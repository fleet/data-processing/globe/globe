/**
 * GLOBE - Ifremer
 */

package gov.nasa.worldwind.ogc.collada;

/**
 * PATCH GLOBE Class added <br>
 * Represents the COLLADA Blinn shader element and provides access to its contents.
 */
public class ColladaBlinn extends ColladaAbstractShader {
	/**
	 * Construct an instance.
	 *
	 * @param namespaceURI the qualifying namespace URI. May be null to indicate no namespace qualification.
	 */
	public ColladaBlinn(String namespaceURI) {
		super(namespaceURI);
	}
}
