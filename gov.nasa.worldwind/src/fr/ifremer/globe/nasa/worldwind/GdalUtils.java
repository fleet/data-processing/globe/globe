/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.nasa.worldwind;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.ogr;
import org.gdal.osr.SpatialReference;

import fr.ifremer.globe.gdal.GdalOsrUtils;
import gov.nasa.worldwind.data.GDAL;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.terrain.LocalElevationModel;

/**
 * Some common utilities.
 */
public class GdalUtils {

	/**
	 * Constructor.
	 */
	public GdalUtils() {
	}

	/**
	 * @return the boundingbox of the raster as [min latitude, max latitude, min longitude, max longitude].
	 */
	public static Geometry makePolygon(ArrayList<ArrayList<LatLon>> segmentsList) {
		// Create polygon
		Geometry polygon = new Geometry(segmentsList.size() > 1 ? ogr.wkbMultiPolygon : ogr.wkbPolygon);
		// One line per segment
		for (ArrayList<LatLon> segments : segmentsList) {
			Geometry line = new Geometry(ogr.wkbLinearRing);
			for (LatLon latlon : segments) {
				line.AddPoint(latlon.getLongitude().getDegrees(), latlon.getLatitude().getDegrees());
			}
			line.CloseRings();
			if (segmentsList.size() > 1) {
				Geometry subPolygon = new Geometry(ogr.wkbPolygon);
				subPolygon.AddGeometry(line);
				polygon.AddGeometry(subPolygon);
			} else {
				polygon.AddGeometry(line);
			}
		}
		return polygon;
	}

	/**
	 * @return the boundingbox of the raster as [min latitude, max latitude, min longitude, max longitude].
	 */
	public static Geometry makeLine(ArrayList<ArrayList<LatLon>> segmentsList) {
		// Create polygon
		Geometry polygon = new Geometry(ogr.wkbPolygon);
		// One line per segment
		Geometry line = new Geometry(ogr.wkbLinearRing);
		for (int i = 0; i < segmentsList.size(); i++) {
			for (int j = 0; j < segmentsList.get(i).size(); j++) {
				line.AddPoint(segmentsList.get(i).get(j).getLongitude().getDegrees(),
						segmentsList.get(i).get(j).getLatitude().getDegrees());
			}
		}
		for (int i = segmentsList.size() - 1; i >= 0; i--) {
			for (int j = segmentsList.get(i).size() - 1; j >= 0; j--) {
				line.AddPoint(segmentsList.get(i).get(j).getLongitude().getDegrees(),
						segmentsList.get(i).get(j).getLatitude().getDegrees());
			}
		}
		polygon.AddGeometry(line);
		return polygon;
	}

	/**
	 * @return a ByteBuffer useable in
	 *         {@link LocalElevationModel#addElevations(ByteBuffer, Sector, int, int, gov.nasa.worldwind.avlist.AVList)}
	 */
	public static ByteBuffer makeByteBuffer(Band band) {
		int columnCount = band.getXSize();
		int lineCount = band.getYSize();

		ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES * lineCount * columnCount);
		DoubleBuffer doubleBuffer = byteBuffer.asDoubleBuffer();

		fr.ifremer.globe.gdal.GdalUtils.readRaster(band, (line, column, value) -> {
			int bufferIndex = line * columnCount + column;
			doubleBuffer.put(bufferIndex, value);
		});

		return byteBuffer;
	}

	/**
	 * @return the BoundingBox of the Dataset as a Sector
	 */
	public static Sector getSector(Dataset dataset) {
		String proj_wkt = dataset.GetProjectionRef();
		if (proj_wkt == null || proj_wkt.isEmpty()) {
			proj_wkt = dataset.GetProjection();
		}
		SpatialReference srs = GdalOsrUtils.SRS_WGS84;
		if (proj_wkt != null && !proj_wkt.isEmpty()) {
			srs = new SpatialReference(proj_wkt);
		}

		GDAL.Area area = new GDAL.Area(srs, dataset);
		return area.getSector();
	}

	/**
	 * @return the Sector as a BoundingBox (latNorth, latSouth, lonWest, lonEast)
	 */
	public static double[] getBoundingBox(Sector sector) {
		return new double[] { sector.getMaxLatitude().degrees, sector.getMinLatitude().degrees,
				sector.getMinLongitude().degrees, sector.getMaxLongitude().degrees };
	}

	/**
	 * @return a list of LatLon with all points of the specified geometry
	 */
	public static List<LatLon> asLatLonList(Geometry geometry, double step) {
		List<LatLon> result = new ArrayList<>();
		double[] point = new double[2];
		int previousIndex = Integer.MIN_VALUE;
		double doubleIndex = 0;
		int index = 0;
		while (index < geometry.GetPointCount()) {
			if (index != previousIndex) {
				geometry.GetPoint_2D(index, point);
				result.add(LatLon.fromDegrees(point[1], point[0]));
				previousIndex = index;
			}
			doubleIndex += step;
			index = (int) Math.round(doubleIndex);
		}
		return result;
	}

}
