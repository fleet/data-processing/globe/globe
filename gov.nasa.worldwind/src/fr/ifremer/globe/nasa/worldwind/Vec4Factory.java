package fr.ifremer.globe.nasa.worldwind;

import java.nio.FloatBuffer;

import gov.nasa.worldwind.geom.Vec4;

public class Vec4Factory {

	/**
	 * Get a {@link Vec4} object inside a coordinates buffer.
	 * @param buffer buffer where the coordinates are
	 * @param position start index where the coordinates need to be load
	 * @return a {@link Vec4} which contains desired coordinates
	 */
	public Vec4 getVec4(FloatBuffer buffer, int position) {
		float x = buffer.get(position);
		float y = buffer.get(position + 1);
		float z = buffer.get(position + 2);
		return new Vec4(x, y, z);
	}
}
