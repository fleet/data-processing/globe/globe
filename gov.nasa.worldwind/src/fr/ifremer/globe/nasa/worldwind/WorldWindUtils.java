/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.nasa.worldwind;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.cache.FileStore;
import gov.nasa.worldwind.data.DataStoreProducer;

/**
 * Utility class
 */
public class WorldWindUtils {

	/** Constructor */
	private WorldWindUtils() {
	}

	/**
	 * Returns a location for installing data. <br>
	 * This attempts to use the first FileStore location marked as an "install" location. <br>
	 * If no install location exists, this falls back to the FileStore's default write location, the same location where
	 * downloaded data is cached.
	 *
	 * @return the default location in the specified FileStore to be used for installing data.
	 */
	public static File getInstallLocation() {
		FileStore fileStore = WorldWind.getDataFileStore();
		for (File location : fileStore.getLocations()) {
			if (fileStore.isInstallLocation(location.getPath())) {
				return location;
			}
		}

		return fileStore.getWriteLocation();
	}

	/** Search the Root folder of WW cache */
	public static File getCacheLocation() {
		File result = getInstallLocation();
		if (result == null || !result.canWrite()) {
			FileStore fileStore = WorldWind.getDataFileStore();
			List<? extends File> locations = fileStore.getLocations();
			for (File location : locations) {
				if (location.canWrite()) {
					result = location;
					break;
				}
			}
		}
		return result;
	}

	/** Search the Root folder of WW cache */
	public static void manageProgression(DataStoreProducer producer, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 10000);
		producer.addPropertyChangeListener(AVKey.PROGRESS, evt -> {
			if (!subMonitor.isCanceled()) {
				if (evt.getOldValue() != null && evt.getNewValue() != null) {
					double oldP = ((Double) evt.getOldValue()).doubleValue();
					double newP = ((Double) evt.getNewValue()).doubleValue();
					subMonitor.worked((int) ((newP - oldP) * 10000d));
				}
			} else {
				producer.stopProduction();
			}
		});
	}

}
