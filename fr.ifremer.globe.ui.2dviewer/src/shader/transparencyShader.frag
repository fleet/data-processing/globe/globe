uniform sampler2D tex;
uniform float min;
uniform float max;
uniform float minTransparency;
uniform float maxTransparency;
uniform float colormap;
uniform float colormapOri;
uniform float width;
uniform float height;
uniform float inverseColormap;
uniform float opacity;

void main()
{
	vec4 colorOri = texture2D(tex, gl_TexCoord[0].st);
	float intensity = 0.0;
	float contrast = 0.0;
	
	float r = 0.0;
	float g = 0.0;
	float b = 0.0;
	
	if(colorOri.a != 0.0)
	{
		if(colormapOri == 0.0) //jet colormap
		{
			if(colorOri.r <= 0.1 && colorOri.g <= 0.1) //index entre 0 et 31
			{
				intensity = (float(colorOri.b) - 0.531250)/(1.0-0.531250)*31.0/255.0;
			}
			else if(colorOri.r <= 0.1 && colorOri.b >= 0.9) //index entre 32 et 95
			{
				intensity = 32.0/255.0 + colorOri.g*(95.0-32.0)/255.0;
			}
			else if(colorOri.g >= 0.9) //index entre 96 et 159
			{
				intensity = 96.0/255.0 + colorOri.r*(159.0-96.0)/255.0;
			}
			else if(colorOri.r >= 0.9 && colorOri.b <= 0.1) //index entre 160 et 223
			{
				intensity = 160.0/255.0 + (1.0 - colorOri.g)*(223.0-160.0)/255.0;
			}
			else //if(colorOri.g <= 0.1 && colorOri.b <= 0.1) //index entre 224 et 255
			{
				intensity = 224.0/255.0 + (1.0 - colorOri.r)/0.5*(255.0-224.0)/255.0;
			}
		}
		else if(colormapOri == 1.0) //catherine
		{
			intensity = colorOri.r;
		}
		else //grayscale
		{
			intensity = colorOri.r;
		}
			
		contrast = (intensity - min)/(max-min);
		
		if(inverseColormap == 1.0)
		{
				contrast = 1.0 - contrast;
		}
		
		// contrast have to be in [min ; max]
		if(contrast < 0.0)
		{
			contrast = 0.0;
		}
		if(contrast > 1.0)
		{
			contrast = 1.0;
		}
		
		if(colormap == 0.0) //jet colormap
		{
			if(contrast <= 31.0/255.0) //index entre 0 et 31
			{
				gl_FragColor.r = 0.0;
				gl_FragColor.g = 0.0;
				gl_FragColor.b = 0.531250 + contrast*(1.0-0.531250)/31.0*255.0;
			}
			else if(contrast <= 95.0/255.0) //index entre 32 et 95
			{
				gl_FragColor.r = 0.0;
				gl_FragColor.g = (contrast-32.0/255.0)*255.0/(95.0-32.0);
				gl_FragColor.b = 1.0;
			}
			else if(contrast <= 159.0/255.0) //index entre 96 et 159
			{
				gl_FragColor.r = (contrast-96.0/255.0)*255.0/(159.0-96.0);
				gl_FragColor.g = 1.0;
				gl_FragColor.b = 1.0 - (contrast-96.0/255.0)*255.0/(159.0-96.0);
			}
			else if(contrast <= 223.0/255.0) //index entre 160 et 223
			{
				gl_FragColor.r = 1.0;
				gl_FragColor.g = 1.0 - (contrast-160.0/255.0)*255.0/(223.0-160.0);
				gl_FragColor.b = 0.0;
			}
			else //index entre 224 et 255
			{
				gl_FragColor.r = 1.0 - (contrast-224.0/255.0)*0.5*255.0/(255.0-224.0);
				gl_FragColor.g = 0.0;
				gl_FragColor.b = 0.0;
			}
		}
		else if(colormap == 1.0) //catherine
		{
			if(contrast <= 35.0/255.0)//R
			{
				r = 0.0391 + contrast*255.0/35.0*(0.0976-0.0391);
			}
			else if(contrast <= 63.0/255.0)//R
			{
				r = 0.1035 + (contrast*255.0-36.0)/(63.0-36.0)*(0.2634-0.1035);
			}
			else if(contrast <= 116.0/255.0)//R
			{
				r = 0.267 + (contrast*255.0-64.0)/(116.0-64.0)*(0.451-0.267);
			}
			else if(contrast <= 160.0/255.0)//R
			{
				r = 0.4634 + (contrast*255.0-117.0)/(160.0-117.0)*(0.9961-0.4634);
			}
			else //R
			{
				r = 0.9944 + (contrast*255.0-161.0)/(255.0-161.0)*(0.8392-0.9944);
			}
			
			if(contrast <= 63.0/255.0)//G
			{
				g = contrast*255.0/63.0*0.7876;
			}
			else if(contrast <= 160.0/255.0)//G
			{
				g = 0.7901 + (contrast*255.0-64.0)/(160.0-64.0)*(0.9882-0.7901);
			}
			else if(contrast <= 206.0/255.0)//G
			{
				g = 0.9842 + (contrast*255.0-161.0)/(206.0-161.0)*(0.8052-0.9842);
			}
			else//G
			{
				g = 0.7933 + (contrast*255.0-206.0)/(255.0-206.0)*(0.2-0.8052);
			}
			
			if(contrast <= 35.0/255.0)//B
			{
				b = 0.4727 + contrast*255.0/35.0*(0.94-0.4727);
			}
			else if(contrast <= 63.0/255.0)//B
			{
				b = 0.942 + (contrast*255.0-36.0)/(63.0-36.0)*(0.9961-0.942);
			}
			else if(contrast <= 116.0/255.0)//B
			{
				b = 0.9891 + (contrast*255.0-64.0)/(116.0-64.0)*(0.6275-0.9891);
			}
			else if(contrast <= 160.0/255.0)//B
			{
				b = 0.6241 + (contrast*255.0-117.0)/(160.0-117.0)*(0.4784-0.6241);
			}
			else if(contrast <= 206.0/255.0)//B
			{
				b = 0.4766 + (contrast*255.0-161.0)/(206.0-161.0)*(0.3902-0.4766);
			}
			else//B
			{
				b = 0.3824 + (contrast*255.0-206.0)/(255.0-206.0)*(0.0-0.3824);
			}
			gl_FragColor.r = r;
			gl_FragColor.g = g;
			gl_FragColor.b = b;
		}
		else //grayscale
		{
			gl_FragColor.r = contrast;
			gl_FragColor.g = contrast;
			gl_FragColor.b = contrast;
		}
		gl_FragColor.a = opacity;	
		
		if((minTransparency != 0.0 && intensity<minTransparency) || (intensity>maxTransparency && maxTransparency != 255.0))	
		{
			gl_FragColor.r = 0.0;
			gl_FragColor.g = 0.0;
			gl_FragColor.b = 0.0;
			gl_FragColor.a = 0.0;
		}
	}
	else
	 {
	   gl_FragColor = colorOri;
	 }
}