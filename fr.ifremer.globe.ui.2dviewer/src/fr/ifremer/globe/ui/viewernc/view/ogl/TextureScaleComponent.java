/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.ogl;

import java.awt.geom.Rectangle2D;
import java.util.function.Supplier;

import jakarta.inject.Inject;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ogl.util.CoordinatesProjection;
import fr.ifremer.globe.ui.texture.TextureDataParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.viewer2d.view.scene.SceneProperties;
import fr.ifremer.globe.ui.viewer2d.view.utils.StringRenderer;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * GL component to draw a color scale bar.
 */
public class TextureScaleComponent implements GLComponent {

	/** Y position on screen to draw the scale bar */
	public static final int ORIGIN_Y = 30;
	/** Height of the scale bar in pixel */
	public static final int BAR_HEIGHT = 12;
	/** Height of the scale bar in pixel */
	public static final int LABEL_WIDTH = 100;

	/** Scene properties computed when drawing */
	@Inject
	@Optional
	private SceneProperties sceneProperties;

	/** Supplier of settings **/
	private final Supplier<TextureDataParameters> settingsSupplier;

	/** Utility instance to draw a Sting on the scene */
	protected final StringRenderer stringRenderer = new StringRenderer();

	/**
	 * Constructor
	 */
	public TextureScaleComponent(Supplier<TextureDataParameters> settingsSupplier) {
		this.settingsSupplier = settingsSupplier;
	}

	/** {@inheritDoc} */
	@Override
	public void dispose(GLAutoDrawable glad) {
		// Nothing to do
	}

	/** {@inheritDoc} */
	@Override
	public void draw(GLAutoDrawable glad) {

		int scalePixelX = sceneProperties.getLeftPadding() + LABEL_WIDTH;
		int scalePixelY = glad.getSurfaceHeight() - ORIGIN_Y;
		int scalePixelWidth = (int) (glad.getSurfaceWidth() - sceneProperties.getLeftPadding()
				- sceneProperties.getRightPadding() - 2d * LABEL_WIDTH);
		if (scalePixelWidth > 100) {
			double[] scaleOrigin = new double[3];
			CoordinatesProjection.fromScreenToScene(glad, scalePixelX, scalePixelY, scaleOrigin);
			double[] oppositePoint = new double[3];
			CoordinatesProjection.fromScreenToScene(glad, scalePixelX + scalePixelWidth, scalePixelY - BAR_HEIGHT,
					oppositePoint);
			double scaleWidth = oppositePoint[0] - scaleOrigin[0];
			double scaleHeight = oppositePoint[1] - scaleOrigin[1];
			TextureDataParameters settings = settingsSupplier.get();

			GL2 gl = glad.getGL().getGL2();
			gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT);
			drawColors(gl, settings, scaleOrigin[0], scaleOrigin[1], scaleWidth, scaleHeight);
			drawBorder(gl, settings, scaleOrigin[0], scaleOrigin[1], scaleWidth, scaleHeight);
			drawLabels(glad, settings, scalePixelX, ORIGIN_Y, scalePixelWidth);
			gl.glPopAttrib();
		}
	}

	private void drawColors(GL2 gl, TextureDataParameters settings, double x, double y, double width, double height) {
		gl.glBegin(GL2GL3.GL_QUADS);
		float[][] colors = ColorMap.getColormap(settings.colorMapIndex);
		int maxQuad = 255;
		for (int i = 0; i < maxQuad; i++) {
			float dx = i / (float) maxQuad;
			int index = i;
			double quadLength = width / maxQuad;
			if (settings.isColorMapReversed) {
				index = 255 - i;
			}
			gl.glColor4d(colors[ColorMap.red][index], colors[ColorMap.green][index], colors[ColorMap.blue][index],
					settings.opacity);
			gl.glVertex3d(x + dx * width, y, settings.opacity);
			gl.glVertex3d(x + dx * width, y + height, settings.opacity);
			gl.glVertex3d(x + dx * width + quadLength, y + height, settings.opacity);
			gl.glVertex3d(x + dx * width + quadLength, y, settings.opacity);
		}
		gl.glEnd();
	}

	private void drawBorder(GL2 gl, TextureDataParameters settings, double x, double y, double scaleBarWidth,
			double scaleBarHeight) {
		// Change color
		float[] previousColor = new float[4];
		gl.glGetFloatv(GL2ES1.GL_CURRENT_COLOR, previousColor, 0);

		gl.glBegin(GL.GL_LINE_STRIP);
		gl.glColor4d(1.0f, 1.0f, 1.0f, settings.opacity);
		gl.glVertex3d(x, y, 1.0);
		gl.glVertex3d(x, y + scaleBarHeight, 1.0);
		gl.glVertex3d(x + scaleBarWidth, y + scaleBarHeight, 1.0);
		gl.glVertex3d(x + scaleBarWidth, y, 1.0);
		gl.glVertex3d(x, y, 1.0);
		gl.glEnd();

		// Restore color
		gl.glColor3fv(previousColor, 0);
	}

	private void drawLabels(GLAutoDrawable glad, TextureDataParameters settings, int x, int y, int scaleBarWidth) {

		String minContrast = NumberUtils.getStringDouble(settings.minContrast);
		Rectangle2D minBounds = stringRenderer.getTextRenderer().getBounds(minContrast);
		y += (int) (BAR_HEIGHT - minBounds.getHeight());
		stringRenderer.draw2DStringOnScene(glad, minContrast, (int) Math.round(x - minBounds.getWidth()) - 3, y);
		String maxContrast = NumberUtils.getStringDouble(settings.maxContrast);
		stringRenderer.draw2DStringOnScene(glad, maxContrast, x + scaleBarWidth + 3, y);

	}

	/** {@inheritDoc} */
	@Override
	public boolean isVisible() {
		return true;
	}

}
