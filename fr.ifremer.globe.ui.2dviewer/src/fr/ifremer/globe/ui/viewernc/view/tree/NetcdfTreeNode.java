/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.tree;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.viewers.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCObject;

/**
 * Abstarct Node for NCObject
 */
public abstract class NetcdfTreeNode<T extends NCObject> extends TreeNode {

	protected static final Logger logger = LoggerFactory.getLogger(NetcdfTreeNode.class);

	/**
	 * Constructor
	 */
	protected NetcdfTreeNode(T value) {
		super(value);
	}

	/** @return wrapped NetcdfGroup */
	@SuppressWarnings("unchecked")
	public T getNetcdfObject() {
		return (T) getValue();
	}

	/** @return the list of attributes of the NcObject */
	public List<NcAttribute> getAttributes() {
		List<NcAttribute> result = new LinkedList<>();
		T ncObject = getNetcdfObject();
		try {
			for (String attName : ncObject.getAttributeNames()) {
				result.add(new NcAttribute(attName, ncObject.getAttributeAsString(attName)));
			}
		} catch (NCException e) {
			logger.warn("Error while reading attributs of {} : {}", ncObject.getPath(), e.getMessage());
		}
		return result;
	}

	/** @return the list of dimensions of the NcObject */
	public abstract List<NCDimension> getDimensions();

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return getNetcdfObject().getPath();
	}
}
