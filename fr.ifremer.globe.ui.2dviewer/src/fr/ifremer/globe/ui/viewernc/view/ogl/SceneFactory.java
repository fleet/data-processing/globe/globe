/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.ogl;

import java.util.function.Consumer;

import jakarta.inject.Inject;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;
import fr.ifremer.globe.ui.viewer2d.view.scene.ISceneComponentProvider;
import fr.ifremer.globe.ui.viewer2d.view.scene.SceneProperties;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewernc.parametersview.TextureDataParametersBearer;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * This class is responsible for creating a Viewer2DScene to display a texture
 */
@Creatable
public class SceneFactory {

	/** Context of the NetcdfViewer part */
	@Inject
	private IEclipseContext context;

	@Inject
	private TextureDataParametersBearer settingsBearer;

	/**
	 * Creates the OGL scene for the specified texture and initialize the context with the necessary components
	 *
	 * @return the AWT Canvas presenting the scene
	 */
	public GLCanvas create(IBasicTextureData texture) {
		GLProfile glprofile = GLProfile.getDefault();
		GLCapabilities caps2 = new GLCapabilities(glprofile);
		GLCanvas canvas = new GLCanvas(caps2);
		context.set(GLCanvas.class, canvas);

		SceneComponentProvider sceneComponentProvider = OsgiUtils.make(SceneComponentProvider.class, context);
		sceneComponentProvider.acceptTexture(texture);
		context.set(ISceneComponentProvider.class, sceneComponentProvider);

		// Initialize Scene properties. No padding, texture takes the whole space
		SceneProperties sceneProperties = OsgiUtils.make(SceneProperties.class, context);
		sceneProperties.setLeftPadding(40);
		sceneProperties.setRightPadding(5);
		sceneProperties.setTopPadding(20);
		sceneProperties.setBottomPadding(sceneProperties.getBottomPadding() + TextureScaleComponent.BAR_HEIGHT);
		sceneProperties.setTickSize(4);

		// Initialize Scene
		Viewer2DScene scene = OsgiUtils.make(Viewer2DScene.class, context);
		// Never equalize axis when displaying a texture with no specific axis
		scene.setEqualizeAxis(false);

		// Hook a listener to refresh the scene when settings change
		canvas.addGLEventListener(new GLEventListener() {
			private Consumer<IBasicTextureData> settingChangedListener = settings -> scene.refresh();

			@Override
			public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
				// Nothing to do
			}

			@Override
			public void init(GLAutoDrawable drawable) {
				settingsBearer.addSettingChangedListener(settingChangedListener);
			}

			@Override
			public void dispose(GLAutoDrawable drawable) {
				settingsBearer.removeSettingChangedListener(settingChangedListener);

			}

			@Override
			public void display(GLAutoDrawable drawable) {
				// Nothing to do
			}
		});

		return canvas;
	}

}
