/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.raw;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.util.slicing.DimensionSlice;

/**
 * Formatter of raw data.
 */
public class RawDataFormatter {

	private static final Logger logger = LoggerFactory.getLogger(RawDataFormatter.class);

	private final NetcdfVariable ncVariable;
	private final DataType rawDataType;
	private final List<DimensionSlice> slicing;
	private final List<ByteBuffer> buffers;

	Function<ByteBuffer, String> formatter = null;
	private int rowCount = 0;

	/**
	 * Constructor
	 */
	public RawDataFormatter(NetcdfVariable ncVariable, List<DimensionSlice> slicing, List<ByteBuffer> buffers) {
		this.ncVariable = ncVariable;
		rawDataType = DataType.getDataTypeFromNCType(ncVariable.getType());
		this.slicing = slicing;
		this.buffers = buffers;
		computeRowCount();
		configureformatter();
	}

	/** Format all buffers */
	public String format() {
		StringBuilder result = new StringBuilder();
		for (ByteBuffer buffer : buffers) {
			buffer.rewind();
			int colCount = buffer.capacity() / rowCount / rawDataType.getSize();
			int rowIndex = 0;
			for (; rowIndex < rowCount && result.length() < 1_000_000; rowIndex++) {
				result.append("{");
				for (int colIndex = 0; colIndex < colCount && result.length() < 1_000_000; colIndex++) {
					if (colIndex > 0) {
						result.append(", ");
					}
					result.append(formatter.apply(buffer));
				}
				result.append("}\n");
			}
		}
		if (result.length() >= 1_000_000) {
			result.insert(0, "Too much data. The content was truncated\n\n");
		}
		return result.toString();
	}

	/**
	 * Initialize formatter
	 */
	private void configureformatter() {
		switch (rawDataType) {
		case BYTE, UBYTE, CHAR:
			formatter = this::formatByteElement;
			break;
		case SHORT, USHORT:
			formatter = this::formatShortElement;
			break;
		case INT, UINT:
			formatter = this::formatIntElement;
			break;
		case FLOAT:
			formatter = this::formatFloatElement;
			break;
		case LONG, ULONG:
			formatter = this::formatLongElement;
			break;
		case DOUBLE:
			formatter = this::formatDoubleElement;
			break;
		default:
			logger.warn("Unhandled variable type {} ", ncVariable.getType());
		}
	}

	/** Compute the number of row to print by buffer */
	private void computeRowCount() {
		if (slicing.isEmpty()) {
			List<NCDimension> shape = ncVariable.getShape();
			rowCount = !shape.isEmpty() ? (int) shape.get(0).getLength() : 0;
		} else if (ncVariable.isVlen()) {
			rowCount = 1;
		} else {
			rowCount = (int) slicing.get(0).count();
		}
	}

	/** Replace the floats at the given index with the given element */
	private String formatFloatElement(ByteBuffer buffer) {
		return String.valueOf(buffer.getFloat());
	}

	/** Replace the doubles at the given index with the given element */
	private String formatDoubleElement(ByteBuffer buffer) {
		return String.valueOf(buffer.getDouble());
	}

	/** Replace the longs at the given index with the given element */
	private String formatLongElement(ByteBuffer buffer) {
		return String.valueOf(buffer.getLong());
	}

	/** Replace the longs at the given index with the given element */
	private String formatIntElement(ByteBuffer buffer) {
		return String.valueOf(buffer.getInt());
	}

	/** Replace the bytes at the given index with the given element */
	private String formatByteElement(ByteBuffer buffer) {
		return String.valueOf(buffer.get());
	}

	/** Replace the shorts at the given index with the given element */
	private String formatShortElement(ByteBuffer buffer) {
		return String.valueOf(buffer.getShort());
	}

}
