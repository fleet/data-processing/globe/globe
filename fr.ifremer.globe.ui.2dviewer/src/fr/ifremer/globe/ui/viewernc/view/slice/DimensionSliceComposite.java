/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.slice;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.netcdf.util.slicing.DimensionSlice;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget.SpinnerWidgetLayout;

/**
 * Composite holding the slicing of NC variable
 */
public class DimensionSliceComposite extends Composite {

	/** MVC view */
	private List<SlicingSpinner> slicingSpinners = List.of();

	private final Consumer<List<DimensionSlice>> slicingConsumer;

	/**
	 * Constructor
	 */
	public DimensionSliceComposite(Composite parent, int style, Consumer<List<DimensionSlice>> slicingConsumer) {
		super(parent, style);
		this.slicingConsumer = slicingConsumer;
		setLayout(new GridLayout());
	}

	/** Change the model of the view */
	public void setSlicing(List<DimensionSlice> slicingModel) {
		GridLayout layout = new GridLayout(slicingModel.size(), false);
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		setLayout(layout);

		// Check first dimension to ensure having a 2D representation
		clear();
		slicingModel.forEach(ds -> slicingSpinners.add(new SlicingSpinner(this, ds)));
		requestLayout();
	}

	/** Clear current slicing */
	public void clear() {
		slicingSpinners.forEach(SlicingSpinner::dispose);
		slicingSpinners = new LinkedList<>();
	}

	/** Return current slicing */
	public List<DimensionSlice> getCurrentSlicing() {
		return slicingSpinners.stream()//
				.map(SlicingSpinner::getDimensionSlice)//
				.toList();
	}

	/** Reacts to the change of the slicing */
	protected void onSlicingChanged() {
		List<DimensionSlice> slicingModel = getCurrentSlicing();
		// Estimate the number of dimension without any slicing
		long unSlicedDimensionCount = slicingModel.stream() //
				.filter(slice -> slice.dimension().getLength() != 1l) //
				.filter(slice -> slice.start() == 0l && slice.count() == slice.dimension().getLength()) //
				.count();

		// No more than 2 unsliced dimension : notify to refresh the view
		if (unSlicedDimensionCount <= 2l) {
			slicingConsumer.accept(slicingModel);
		}
	}

	/**
	 * Spinner to edit the index of slicing for a specific dimension
	 */
	class SlicingSpinner {
		/** Widget */
		private final SpinnerWidget spinner;

		private DimensionSlice dimensionSlice;

		/**
		 * Constructor
		 */
		public SlicingSpinner(Composite parent, DimensionSlice dimensionSlice) {
			this.dimensionSlice = dimensionSlice;
			updateDimensionSlice(dimensionSlice.enabled(), 0l);

			EnabledNumberModel numberModel = new EnabledNumberModel(dimensionSlice.enabled(), dimensionSlice.start());
			spinner = new SpinnerWidget(dimensionSlice.dimension().getName(), "", parent, numberModel, 0,
					SpinnerWidgetLayout.HORIZONTAL);
			GridDataFactory.fillDefaults().grab(true, false).applyTo(spinner);

			int maxValue = (int) dimensionSlice.dimension().getLength() - 1;
			UIUtils.setVisible(spinner, maxValue >= 1);
			spinner.setMinMaxValues(0, maxValue);
			spinner.subscribe(this::onIndexModified);
		}

		/** React to the change of the spinner model */
		protected void onIndexModified(EnabledNumberModel spinnerModel) {
			updateDimensionSlice(spinnerModel.enable, spinnerModel.value.longValue());
			onSlicingChanged();
		}

		/** Update the attribute dimensionSlice when spinner changed */
		private void updateDimensionSlice(boolean sliced, long slicingIndex) {
			if (sliced) {
				dimensionSlice = new DimensionSlice(dimensionSlice.dimension(), slicingIndex, 1l, true);
			} else {
				dimensionSlice = new DimensionSlice(dimensionSlice.dimension());
			}
		}

		/** Disposes of the operating system resources */
		public void dispose() {
			spinner.dispose();
		}

		/**
		 * @return the {@link #dimensionSlice}
		 */
		public DimensionSlice getDimensionSlice() {
			return dimensionSlice;
		}
	}
}
