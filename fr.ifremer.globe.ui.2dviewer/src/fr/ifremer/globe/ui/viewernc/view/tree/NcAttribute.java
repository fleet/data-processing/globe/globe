/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.tree;

/**
 * Classe wrapping information of one attribute of a NcObject
 */
public class NcAttribute {

	public final String name;
	public final String value;

	/**
	 * Constructor
	 */
	public NcAttribute(String name, String value) {
		this.name = name;
		this.value = value;
	}

}
