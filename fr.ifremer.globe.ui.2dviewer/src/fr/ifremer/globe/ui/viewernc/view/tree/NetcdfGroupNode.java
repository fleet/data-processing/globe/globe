/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.tree;

import java.util.List;

import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.FileUtils;

/**
 * Instance of TreeNode to render a NetcdfGroup
 */
public class NetcdfGroupNode extends NetcdfTreeNode<NetcdfGroup> {

	public final String label;

	/**
	 * Constructor
	 */
	public NetcdfGroupNode(NetcdfGroup ncGroup) {
		super(ncGroup);
		label = ncGroup.getName();
	}

	/**
	 * Constructor
	 */
	public NetcdfGroupNode(NetcdfFile ncFile) {
		super(ncFile);
		label = FileUtils.getBaseName(ncFile.getName());
	}

	/** @return the list of dimensions defined of this group */
	@Override
	public List<NCDimension> getDimensions() {
		try {
			return getNetcdfObject().getDimensions();
		} catch (NCException e) {
			logger.warn("Error while reading dimension of {} : {}", getNetcdfObject().getPath(), e.getMessage());
			return List.of();
		}
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return label;
	}
}
