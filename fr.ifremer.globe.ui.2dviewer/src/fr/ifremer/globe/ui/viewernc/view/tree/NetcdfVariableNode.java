/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.tree;

import java.util.List;

import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCDimension;

/**
 * Instance of TreeNode to render a NetcdfVariable
 */
public class NetcdfVariableNode extends NetcdfTreeNode<NetcdfVariable> {

	/**
	 * Constructor
	 */
	public NetcdfVariableNode(NetcdfVariable ncVariable) {
		super(ncVariable);
	}

	/** @return the shape of this variable */
	@Override
	public List<NCDimension> getDimensions() {
		return getNetcdfObject().getShape();
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return getNetcdfObject().getName();
	}

}
