/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.ogl;

import java.util.List;
import java.util.function.Supplier;

import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.texture.TextureDataParameters;
import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.components.CursorLinesComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.FrameComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.RegularHorizontalGridComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.RegularVerticalGridComponent;
import fr.ifremer.globe.ui.viewer2d.view.scene.ISceneComponentProvider;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.texture.BasicTextureCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.texture.TextureComponent;
import fr.ifremer.globe.ui.viewernc.parametersview.TextureDataParametersBearer;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Provider of GLComponent to participate to the scene.<br>
 * These components support the display of textures.
 */
public class SceneComponentProvider implements ISceneComponentProvider {

	/** Context of the NetcdfViewer part */
	@Inject
	protected IEclipseContext context;
	@Inject
	private TextureDataParametersBearer settingsBearer;

	/** Scene injected when SceneFactory'll create it **/
	@Optional
	@Inject
	private Viewer2DScene scene;

	/** True when texture has to be flipped */
	@Inject
	@Named(ContextNames.FLIP_Y_AXIS)
	private boolean flipYAxis;

	/** All GL components participating in texture rendering */
	protected List<GLComponent> glComponents;

	/** Main component, wrapping the ITextureData */
	protected TextureComponent textureComponent;

	/**
	 * Prepare GL components to draw the specified texture
	 */
	public void acceptTexture(IBasicTextureData texture) {
		Supplier<TextureDataParameters> settingsSupplier = () -> settingsBearer.getSettings(texture)
				.orElse(TextureDataParameters.defaultFor(texture));
		textureComponent = new TextureComponent(settingsSupplier);
		ContextInjectionFactory.inject(textureComponent, context);

		CursorLinesComponent cursorLinesComponent = OsgiUtils.make(CursorLinesComponent.class, context);
		cursorLinesComponent.setCoordFormat((x, y) -> formatCursorPosition(x, y, texture));
		// Grid
		var horizontalGrid = OsgiUtils.make(RegularHorizontalGridComponent.class, context);
		horizontalGrid.setUnit("");
		var verticalGrid = OsgiUtils.make(RegularVerticalGridComponent.class, context);
		verticalGrid.setUnit("");

		TextureScaleComponent textureScaleComponent = new TextureScaleComponent(settingsSupplier);
		ContextInjectionFactory.inject(textureScaleComponent, context);
		glComponents = List.of(//
				textureComponent, //
				OsgiUtils.make(FrameComponent.class, context), //
				horizontalGrid, //
				verticalGrid, //
				cursorLinesComponent, //
				textureScaleComponent);

		BasicTextureCharacteristics textureCharacteristics = textureComponent.acceptTexture(texture);
		context.set(ISubjectCharacteristics.class, textureCharacteristics);
	}

	@PreDestroy
	protected void dispose() {
		if (textureComponent != null) {
			textureComponent.dispose();
		}
	}

	/**
	 * Display value at mouse position
	 *
	 * @param x : position of the mouse in the width of the texture. Origin is left
	 * @param y : position of the mouse in the height of the texture. Origin is bottom
	 * @param texture : current displayed texture
	 * @return the formatted label
	 */
	protected String formatCursorPosition(double x, double y, IBasicTextureData texture) {
		if (x >= 0d && y >= 0d) {
			int row = (int) y;
			if (flipYAxis) {
				row = y <= texture.getHeight() ? (int) (texture.getHeight() - y) : -1;
			}

			int col = (int) x;
			if (row >= 0 && col >= 0 && row < texture.getHeight() && col < texture.getWidth()) {
				return String.format(//
						"row = %d    column = %d    value = %.2f", //
						row, col, texture.getFloat(col, row));
			}
		}
		return "";
	}

	/**
	 * @return the {@link #glComponents}
	 */
	@Override
	public List<GLComponent> getGLComponents() {
		return glComponents;
	}
}
