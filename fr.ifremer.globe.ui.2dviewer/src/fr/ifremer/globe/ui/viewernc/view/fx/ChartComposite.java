/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.fx;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.collections.primitives.DoubleList;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.impl.SimpleChartSeries;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 *
 */
public class ChartComposite extends Composite {

	private static final Logger logger = LoggerFactory.getLogger(ChartComposite.class);

	/** Linked chart */
	private FXChart chart;

	/**
	 * Constructor
	 */
	public ChartComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout());

		try {
			// Create charts
			chart = new FXChart(this, "");
		} catch (IOException | FXChartException e) {
			logger.error("Error during chart creation", e);
		}
	}

	/**
	 * Set chart model to a buffer of double
	 */
	public void fitToDoubleData(NetcdfVariable ncVariable, ByteBuffer values) throws NCException {
		values.rewind();
		DoubleList x = new ArrayDoubleList();
		DoubleList y = new ArrayDoubleList();
		int abscissa = 0;
		while (values.hasRemaining()) {
			double value = values.getDouble();
			if (Double.isFinite(value)) {
				x.add(abscissa);
				y.add(value);
			}
			abscissa++;
		}

		chart.setTitle(null);
		chart.setData(List.of(new SimpleChartSeries("", x.toArray(), y.toArray())));
		setYAxisLabelFormatter(ncVariable);

	}

	/**
	 * Set chart model to a buffer of long
	 */
	public void fitToLongData(NetcdfVariable ncVariable, ByteBuffer values) throws NCException {
		values.rewind();
		DoubleList x = new ArrayDoubleList();
		DoubleList y = new ArrayDoubleList();
		int abscissa = 0;
		while (values.hasRemaining()) {
			long value = values.getLong();
			x.add(abscissa);
			y.add(value);
			abscissa++;
		}

		chart.setTitle(null);
		chart.setData(List.of(new SimpleChartSeries("", x.toArray(), y.toArray())));
		setYAxisLabelFormatter(ncVariable);
	}

	/**
	 * Set chart model to a buffer of long
	 */
	public void fitToIntegerData(NetcdfVariable ncVariable, ByteBuffer values) throws NCException {
		values.rewind();
		DoubleList x = new ArrayDoubleList();
		DoubleList y = new ArrayDoubleList();
		int abscissa = 0;
		while (values.hasRemaining()) {
			int value = values.getInt();
			x.add(abscissa);
			y.add(value);
			abscissa++;
		}

		chart.setTitle(null);
		chart.setData(List.of(new SimpleChartSeries("", x.toArray(), y.toArray())));
		setYAxisLabelFormatter(ncVariable);
	}

	/**
	 * Display an error
	 */
	public void fitToErrorInData(String error) {
		chart.setTitle(error);
		chart.setData(List.of(new SimpleChartSeries("", new double[0], new double[0])));

	}

	private void setYAxisLabelFormatter(NetcdfVariable ncVariable) throws NCException {
		if (ncVariable.hasAttribute(CFStandardNames.CF_UNITS)) {
			String name = ncVariable.getName();
			String units = ncVariable.getAttributeText(CFStandardNames.CF_UNITS);

			if (CFStandardNames.CF_DEGREES_NORTH.equals(units)
					&& name.toLowerCase().contains(CFStandardNames.CF_LATITUDE_STD_NAME)) {
				chart.setYAxisLabelFormatter(v -> FormatLatitudeLongitude.latitudeToString(v.doubleValue()));
			} else if (CFStandardNames.CF_DEGREES_EAST.equals(units)
					&& name.toLowerCase().contains(CFStandardNames.CF_LONGITUDE_STD_NAME)) {
				chart.setYAxisLabelFormatter(v -> FormatLatitudeLongitude.longitudeToString(v.doubleValue()));
			} else if (units.toLowerCase().contains(CFStandardNames.CF_UNITS_D)) {
				chart.setYAxisLabelFormatter(v -> NumberUtils.getStringAngle(v.doubleValue()));
			} else if (units.toLowerCase().startsWith("nanoseconds since 1970")) {
				chart.setYAxisTimeLabel();
			} else if (units.toLowerCase().startsWith("nanoseconds since 1601")) {
				chart.setYAxisTimeLabel();
			} else {
				chart.setYAxisLabelFormatter(v -> NumberUtils.getStringDouble(v.doubleValue()));
			}
		} else {
			chart.setYAxisLabelFormatter(v -> NumberUtils.getStringDouble(v.doubleValue()));
		}
	}
}
