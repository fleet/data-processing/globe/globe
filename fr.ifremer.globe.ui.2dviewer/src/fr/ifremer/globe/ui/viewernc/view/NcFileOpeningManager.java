/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view;

import java.util.Optional;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.IPartListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * This class aim at managing the opening (and closing) of the NC file edited in the NetcdfViewer
 */
public class NcFileOpeningManager implements IPartListener {
	private static final Logger logger = LoggerFactory.getLogger(NcFileOpeningManager.class);

	/** Part services */
	private final EPartService partService;
	/** FileInfo of the file to manage */
	private final IFileInfo fileInfo;
	/** NetcdfViewer part */
	private MPart ncViewerPart;
	/** Handler of opened file */
	private NetcdfFile netcdfFile;

	/**
	 * Constructor
	 */
	public NcFileOpeningManager(EPartService partService, IFileInfo fileInfo) {
		this.partService = partService;
		this.fileInfo = fileInfo;
		openFile();
	}

	/** Accept the part to observe */
	public void observePart(MPart mPart) {
		if (ncViewerPart == null) {
			ncViewerPart = mPart;
			partService.addPartListener(this);
		} else {
			logger.debug("A MPart is already observed");
		}
	}

	/** Free ressource when part is closed */
	public void dispose() {
		closeFile();
		partService.removePartListener(this);
	}

	/** Open the file */
	private void openFile() {
		if (netcdfFile == null) {
			try {
				logger.debug("Opening the file {} ", fileInfo.getPath());
				netcdfFile = NetcdfFile.open(fileInfo.getPath(), Mode.readonly);
			} catch (NCException e) {
				logger.warn("Unable to open the file {} ", fileInfo.getPath());
			}
		}
	}

	/** Close the file */
	private void closeFile() {
		if (netcdfFile != null) {
			logger.debug("Closing the file {} ", fileInfo.getPath());
			netcdfFile.close();
			netcdfFile = null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void partActivated(MPart part) {
		if (part == ncViewerPart) {
			openFile();
		}
	}

	/** {@inheritDoc} */
	@Override
	public void partDeactivated(MPart part) {
		if (part == ncViewerPart) {
			closeFile();
		}
	}

	/**
	 * @return a fresh instance of the variable. Useful when the file was reopened.
	 */
	public NetcdfVariable reload(NetcdfVariable ncVariable) {
		NetcdfVariable result = ncVariable;
		if (netcdfFile != null) {
			try {
				result = netcdfFile.getVariable(ncVariable.getPath());
			} catch (NCException e) {
				logger.warn("Unable to reload the variable {} : {}", ncVariable.getPath(), e.getMessage());
			}
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public void partBroughtToTop(MPart part) {
		// Nothing to do
	}

	/** {@inheritDoc} */
	@Override
	public void partHidden(MPart part) {
		// Nothing to do
	}

	/** {@inheritDoc} */
	@Override
	public void partVisible(MPart part) {
		// Nothing to do
	}

	/**
	 * @return the {@link #netcdfFile}
	 */
	public Optional<NetcdfFile> getNetcdfFile() {
		return Optional.ofNullable(netcdfFile);
	}

	/**
	 * @return the {@link #fileInfo}
	 */
	public IFileInfo getFileInfo() {
		return fileInfo;
	}

}
