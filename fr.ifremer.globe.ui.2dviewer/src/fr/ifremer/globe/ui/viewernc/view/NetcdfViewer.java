/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.LinkedList;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Optional;
import java.util.function.LongUnaryOperator;
import java.util.function.ToDoubleFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

import javax.swing.SwingUtilities;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeNode;
import org.eclipse.jface.viewers.TreeNodeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCObject;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.util.slicing.DimensionSlice;
import fr.ifremer.globe.netcdf.util.slicing.VariableSlicerFacade;
import fr.ifremer.globe.ui.texture.basic.BasicTextureData;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewernc.parametersview.TextureDataParametersBearer;
import fr.ifremer.globe.ui.viewernc.view.fx.ChartComposite;
import fr.ifremer.globe.ui.viewernc.view.ogl.SceneFactory;
import fr.ifremer.globe.ui.viewernc.view.raw.RawDataFormatter;
import fr.ifremer.globe.ui.viewernc.view.raw.StringDataFormatter;
import fr.ifremer.globe.ui.viewernc.view.slice.DimensionSliceComposite;
import fr.ifremer.globe.ui.viewernc.view.tree.NetcdfGroupNode;
import fr.ifremer.globe.ui.viewernc.view.tree.NetcdfTreeNode;
import fr.ifremer.globe.ui.viewernc.view.tree.NetcdfVariableNode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 *
 */
public class NetcdfViewer {

	private static final String PLUGIN_ID = "fr.ifremer.globe.ui.2dviewer";

	private static final Logger logger = LoggerFactory.getLogger(NetcdfViewer.class);

	/** Id of the part in fragment.e4xmi */
	public static final String PART_ID = "fr.ifremer.globe.ui.2dviewer.partdescriptor.viewernc";

	/** Default font name */
	public static final String FONT_NAME = JFaceResources.getDefaultFont().getFontData()[0].getName();
	/** Font used for title */
	private static final Font TITLE_FONT = SWTResourceManager.getFont(FONT_NAME, 10, SWT.BOLD);
	/** Font used for title */
	private static final Font SMALL_FONT = SWTResourceManager.getFont(FONT_NAME, 8, SWT.NORMAL);

	@Inject
	@Active
	private IEclipseContext partContext;

	@Inject
	private ESelectionService selectionService;
	@Inject
	private SceneFactory sceneFactory;
	@Inject
	private TextureDataParametersBearer settingsBearer;
	/** Manager of the opening (and closing) of the NC file */
	@Inject
	private NcFileOpeningManager fileManager;

	/**
	 * Current object displayed for the selected variable. This Optional is transmit to the ESelectionService when focus
	 * is gained
	 */
	private Optional<Object> currentRendering = Optional.empty();

	/** Current slicing applied to the current rendering */
	private List<DimensionSlice> currentSlicing = List.of();

	/** Tree */
	private TreeViewer treeVariablesViewer;

	/** Widgets "Filters" */
	private Composite cmpSliceParent;
	private DimensionSliceComposite cmpDimSlice;

	/** Widgets "Variables" */
	private Label lblVariableDesc;
	private Text txtAttributes;
	private Composite cmp2dTools;
	private Button btnRawData;
	private StackLayout varContentLayout;
	private Label lblInfo;
	private Composite cmpOgl;
	private ChartComposite cmpChart;
	private Frame oglFrame;
	private Text txtRawData;

	/** Current displayed variable */
	private Optional<NetcdfVariable> currentVariable = Optional.empty();
	/** True when texture has to be flipped */
	private boolean flipYAxis = false;
	/** True to draw the grid */
	@Inject
	@Named(ContextNames.DRAW_GRID)
	private boolean showGrid;

	/**
	 * Initialize the view part.
	 */
	@PostConstruct
	public void postConstruct(Composite parent, @Named(ContextNames.FLIP_Y_AXIS) boolean flipYAxis) {
		this.flipYAxis = flipYAxis;
		parent.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		createControls(parent);
		displayInfo("Variable content will be displayed here", true);
		showSlicing(false);
		logger.debug("loading file {} in {}", fileManager.getFileInfo().getPath(), getClass().getSimpleName());
		fileManager.getNetcdfFile().ifPresent(netcdfFile -> {
			try {
				NetcdfGroupNode rootNode = new NetcdfGroupNode(netcdfFile);
				loadTreeNodes(netcdfFile, rootNode);
				treeVariablesViewer.setInput(new NetcdfGroupNode[] { rootNode });

				// Select root node
				var tree = treeVariablesViewer.getTree();
				tree.forceFocus();
				tree.getDisplay().asyncExec(() -> {
					if (!tree.isDisposed()) {
						tree.setSelection(tree.getTopItem());
					}
				});
			} catch (NCException e) {
				Messages.openErrorMessage("NetCDF viewer", "File parse error", e);
			}
		});
	}

	/**
	 * Create contents of the view part.
	 */
	private void createControls(Composite parent) {
		SashForm sashForm = new SashForm(parent, SWT.SMOOTH);

		Composite cmpVariables = new Composite(sashForm, SWT.NONE);
		GridLayoutFactory.fillDefaults().margins(0, 0).applyTo(cmpVariables);

		Composite cmpTreeVariables = new Composite(cmpVariables, SWT.NONE);
		cmpTreeVariables.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		TreeColumnLayout treeColumnLayout = new TreeColumnLayout(true);
		cmpTreeVariables.setLayout(treeColumnLayout);

		treeVariablesViewer = new TreeViewer(cmpTreeVariables, SWT.NONE);
		Tree treeVariables = treeVariablesViewer.getTree();
		treeVariables.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		treeVariablesViewer.setContentProvider(new TreeNodeContentProvider());
		treeVariablesViewer.setAutoExpandLevel(2);
		treeVariables.addSelectionListener(
				SelectionListener.widgetSelectedAdapter(e -> onTreeNodeSelection((TreeItem) e.item)));

		TreeViewerColumn treeViewerClmnName = new TreeViewerColumn(treeVariablesViewer, SWT.NONE);
		treeViewerClmnName.setLabelProvider(ColumnLabelProvider.createTextProvider(Object::toString));
		treeColumnLayout.setColumnData(treeViewerClmnName.getColumn(), new ColumnWeightData(150, 150, true));

		Composite cmpSashRight = new Composite(sashForm, SWT.NONE);
		GridLayoutFactory.fillDefaults().margins(0, 0).numColumns(2).equalWidth(false).applyTo(cmpSashRight);

		Label separator = new Label(cmpSashRight, SWT.SEPARATOR | SWT.VERTICAL);
		GridDataFactory.fillDefaults().grab(false, true).applyTo(separator);

		Composite cmpMain = new Composite(cmpSashRight, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(cmpMain);
		GridLayoutFactory.fillDefaults().margins(0, 0).applyTo(cmpMain);

		lblVariableDesc = new Label(cmpMain, SWT.NONE);
		lblVariableDesc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblVariableDesc.setText("Variable content");
		lblVariableDesc.setFont(TITLE_FONT);

		txtAttributes = new Text(cmpMain, SWT.MULTI | SWT.READ_ONLY | SWT.WRAP | SWT.BORDER | SWT.V_SCROLL);
		txtAttributes.setFont(SMALL_FONT);
		txtAttributes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		txtAttributes.setBackground(parent.getBackground());
		txtAttributes.getVerticalBar().setVisible(false);

		cmpSliceParent = new Composite(cmpMain, SWT.NONE);
		cmpSliceParent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		GridLayoutFactory.fillDefaults().numColumns(3).margins(0, 0).applyTo(cmpSliceParent);

		cmpDimSlice = new DimensionSliceComposite(cmpSliceParent, SWT.NONE, this::onSlicingChanged);
		cmpDimSlice.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false, 1, 1));

		cmp2dTools = new Composite(cmpSliceParent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(2).margins(0, 0).applyTo(cmp2dTools);

		Button btnSwapOrigin = new Button(cmp2dTools, SWT.NONE);
		btnSwapOrigin.setImage(ResourceManager.getPluginImage(PLUGIN_ID, "icons/yflip.png"));
		btnSwapOrigin.setToolTipText("Swap origin of texture");
		btnSwapOrigin.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> flipYAxis()));

		Button btnShowGrid = new Button(cmp2dTools, SWT.TOGGLE);
		btnShowGrid.setImage(ResourceManager.getPluginImage(PLUGIN_ID, "icons/showGrid.png"));
		btnShowGrid.setToolTipText("Show grid");
		btnShowGrid.setSelection(showGrid);
		btnShowGrid.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> showGrid()));

		btnRawData = new Button(cmpSliceParent, SWT.NONE);
		morphBtnTo(DataFeature.RAW);
		btnRawData.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> swithBtnRawData()));

		Composite cmpVarContent = new Composite(cmpMain, SWT.NONE);
		varContentLayout = new StackLayout();
		cmpVarContent.setLayout(varContentLayout);
		cmpVarContent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		lblInfo = new Label(cmpVarContent, SWT.WRAP);
		varContentLayout.topControl = lblInfo;

		cmpOgl = new Composite(cmpVarContent, SWT.EMBEDDED);
		cmpOgl.setLayout(new GridLayout(1, false));
		oglFrame = SWT_AWT.new_Frame(cmpOgl);

		cmpChart = new ChartComposite(cmpVarContent, SWT.NONE);

		txtRawData = new Text(cmpVarContent, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		txtRawData.setBackground(parent.getBackground());
		txtRawData.setFont(JFaceResources.getTextFont());

		sashForm.setWeights(20, 80);
	}

	/** Change image and tooltip of the button Raw data */
	private void morphBtnTo(DataFeature feature) {
		if (feature == DataFeature.RAW) {
			btnRawData.setImage(ResourceManager.getPluginImage(PLUGIN_ID, "icons/table.png"));
			btnRawData.setToolTipText("Show raw data");
		} else {
			btnRawData.setImage(ResourceManager.getPluginImage(PLUGIN_ID, "icons/chart.png"));
			btnRawData.setToolTipText("Show graphic");
		}
		btnRawData.setData(feature);
	}

	/** Load sub-nodes of the specified parent */
	private void loadTreeNodes(NetcdfGroup parentGroup, NetcdfGroupNode parentNode) throws NCException {
		List<TreeNode> children = new LinkedList<>();
		for (NetcdfGroup subGroup : parentGroup.getGroups()) {
			var subGroupNode = new NetcdfGroupNode(subGroup);
			children.add(subGroupNode);
			loadTreeNodes(subGroup, subGroupNode);
		}

		for (NetcdfVariable variable : parentGroup.getVariables()) {
			children.add(new NetcdfVariableNode(variable));
		}

		parentNode.setChildren(children.toArray(new TreeNode[children.size()]));
	}

	/** Reacts to change in selection */
	private void onTreeNodeSelection(TreeItem selectedTreeItem) {
		if (ensureFileOpen()) {
			// Cleaning
			cmpDimSlice.clear();
			txtRawData.setText("");

			var netcdfTreeNode = (NetcdfTreeNode<?>) selectedTreeItem.getData();
			NCObject ncObject = netcdfTreeNode.getNetcdfObject();
			if (netcdfTreeNode instanceof NetcdfVariableNode) {
				NetcdfVariable ncVariable = fileManager.reload((NetcdfVariable) ncObject);
				ncObject = ncVariable;

				currentVariable = Optional.of(ncVariable);
				describeVariable(ncVariable);
				displayVariable(ncVariable);
			} else {
				lblVariableDesc.setText(ncObject instanceof NetcdfFile ? "Global attributes"
						: netcdfTreeNode.getNetcdfObject().getPath());
				showSlicing(false);
				currentVariable = Optional.empty();
				displayInfo("", true);
			}
			// Generates list of attribute of the variable
			listAttributes(ncObject);
		}
	}

	/**
	 * Describe the selected variable
	 */
	private void describeVariable(NetcdfVariable ncVariable) {
		DataType rawDataType = DataType.getDataTypeFromNCType(ncVariable.getType());

		// Type + Name + Dimensions
		lblVariableDesc.setText(rawDataType.name().toLowerCase() + " " + ncVariable.getName() + " (" + //
				ncVariable.getShape().stream().map(d -> d.getName() + "=" + d.getLength())
						.collect(Collectors.joining(", "))
				+ //
				")");
	}

	/**
	 * List all attributes like ncDump
	 */
	private void listAttributes(NCObject ncObject) {
		try {
			txtAttributes.getVerticalBar().setVisible(false);
			((GridData) txtAttributes.getLayoutData()).heightHint = SWT.DEFAULT;

			if (ncObject.getAttributeCount() > 0) {
				StringBuilder attDesc = new StringBuilder();
				for (String attName : ncObject.getAttributeNames()) {
					if (attDesc.length() > 0) {
						attDesc.append("\n");
					}
					attDesc.append(attName).append(" = ");
					DataType type = ncObject.getAttributeDataType(attName);
					if (type == DataType.STRING || (type == DataType.CHAR && ncObject.getAttributeSize(attName) != 1)) {
						attDesc.append(ncObject.getAttributeAsString(attName));
					} else if (type == DataType.CHAR) {
						char attValue = ncObject.getAttributeChar(attName);
						if (attValue != 0) // endbuffer char
							attDesc.append(attValue);
					} else if ((type == DataType.INT || type == DataType.SHORT || type == DataType.BYTE)
							&& ncObject.getAttributeSize(attName) > 1) {
						int[] attValue = ncObject.getAttributeIntArray(attName);
						attDesc.append(Arrays.toString(attValue));
					} else if (type != null) {
						attDesc.append(ncObject.getAttributeAsString(attName)).append(" // ")
								.append(type.name().toLowerCase());
					} else {
						attDesc.append(ncObject.getAttributeAsString(attName));
					}
				}

				// describe compression parameters
				if (ncObject instanceof NCVariable ncVariable) {
					if (ncVariable.getCompressionLevel() > 0) {
						attDesc.append("\n").append("compression").append(" = ")
								.append(ncVariable.getCompressionLevel());
						long[] chunksizes = ncVariable.getChunkSizes();
						attDesc.append("\n").append("chunksizes").append(" = ").append(Arrays.toString(chunksizes));
					}
				}

				txtAttributes.setText(attDesc.toString());

				// Limit the height of the Text
				if (ncObject instanceof NCVariable && txtAttributes.computeSize(SWT.DEFAULT, SWT.DEFAULT).y > 100) {
					txtAttributes.getVerticalBar().setVisible(true);
					((GridData) txtAttributes.getLayoutData()).heightHint = 100;
				}
			} else {
				txtAttributes.setText("No attribute");
			}
		} catch (NCException e) {
			txtAttributes.setText("Unable to list attributes");
			logger.warn("Unable to list attributes", e);
		}

		txtAttributes.pack();
		txtAttributes.requestLayout();
	}

	/**
	 * Display variable content in 2D or chart view
	 */
	private void displayVariable(NetcdfVariable ncVariable) {
		List<NCDimension> shape = ncVariable.getShape();
		if (shape.isEmpty() // Scalar variable
				|| (shape.size() == 1 && shape.get(0).getLength() == 1l)) {
			// Only one value
			displaySingleValue(ncVariable);
		} else {
			List<DimensionSlice> slicing = inferCurrentSlicing(ncVariable);
			cmpDimSlice.setSlicing(slicing);
			displaySlicedVariable(ncVariable);
		}
	}

	private void displaySingleValue(NetcdfVariable ncVariable) {
		try {
			String formattedValues = formatRawData(ncVariable);
			displayInfo(formattedValues, true);
		} catch (Exception e) {
			displayInfo("Read error", true);
		}
	}

	/**
	 * Read the content of the variable
	 */
	private ByteBuffer readVariable(DataType targetType, NetcdfVariable ncVariable, List<DimensionSlice> slicing)
			throws NCException, GIOException {
		VariableSlicerFacade variableSlicerFacade = new VariableSlicerFacade();
		ByteBuffer result = variableSlicerFacade.sliceVariable(targetType, ncVariable, slicing)
				.orElseThrow(() -> new GIOException("Unhandled case. Variable " + ncVariable.getName()
						+ " is not readable in " + targetType.name()));

		// Convert dates
		if (targetType == DataType.LONG && ncVariable.hasAttribute(CFStandardNames.CF_UNITS)) {
			String units = ncVariable.getAttributeText(CFStandardNames.CF_UNITS);
			if (units.toLowerCase().startsWith("nanoseconds since 1970")) {
				convertDates(result, DateUtils::nanoSecondToMilli);
			} else if (units.toLowerCase().startsWith("nanoseconds since 1601")) {
				convertDates(result, l -> DateUtils.getDateWithNanosecondsFrom1601(l).getTime());
			}
		}

		return result;
	}

	/**
	 * Read the raw data of the variable
	 */
	private List<ByteBuffer> readRawData(NetcdfVariable ncVariable) throws NCException, GIOException {
		VariableSlicerFacade variableSlicerFacade = new VariableSlicerFacade();
		List<ByteBuffer> result = variableSlicerFacade.sliceRawVariable(ncVariable, inferCurrentSlicing(ncVariable));
		if (result.isEmpty()) {
			throw new GIOException("Unhandled case. Can't read raw data from variable " + ncVariable.getName());
		}
		return result;
	}

	/**
	 * Read the raw data of the variable
	 */
	private String[] readStringData(NetcdfVariable ncVariable) throws NCException {
		var slicing = inferCurrentSlicing(ncVariable);
		long[] start = slicing.stream().mapToLong(DimensionSlice::start).toArray();
		long[] count = slicing.stream().mapToLong(DimensionSlice::count).toArray();
		return ncVariable.get_string(start, count);

	}

	/**
	 * Display variable content in 2D view using OGL
	 */
	private void load2dView(NetcdfVariable ncVariable, List<DimensionSlice> slicing) throws NCException, GIOException {
		logger.debug("Initializing 2D view for variable {}", ncVariable.getName());
		// Defines axis titles by injection
		var titles = slicing.stream().filter(d -> d.count() > 1).map(d -> d.dimension().getName()).toList();
		if (ncVariable.isVlen()) {
			partContext.set(ContextNames.X_AXIS_NAME, !titles.isEmpty() ? titles.get(0) : "");
			partContext.set(ContextNames.Y_AXIS_NAME, "VLEN");
		} else {
			partContext.set(ContextNames.X_AXIS_NAME, titles.size() > 1 ? titles.get(1) : "");
			partContext.set(ContextNames.Y_AXIS_NAME, !titles.isEmpty() ? titles.get(0) : "");
		}

		// Loading buffer
		try {
			ByteBuffer buffer = readVariable(DataType.FLOAT, ncVariable, slicing);
			DoubleSummaryStatistics stats = getDoubleStatistics(buffer, ByteBuffer::getFloat);
			if (stats.getCount() > 0) {
				BasicTextureData texture = ncVariable.isVlen() ? makeVlenTextureData(slicing, buffer)
						: makeTextureData(slicing, buffer);
				settingsBearer.bear(fileManager.getFileInfo().getPath(), ncVariable.getPath(), texture);

				// Change the selection
				setCurrentRendering(texture);

				// AWT work
				SwingUtilities.invokeLater(() -> {
					GLCanvas canvas = sceneFactory.create(texture);
					oglFrame.removeAll();
					oglFrame.add(canvas, BorderLayout.CENTER);
					oglFrame.doLayout();
				});
			} else {
				// No data
				setCurrentRendering(null);
			}
		} catch (GIOException e) {
			// No data
			setCurrentRendering(null);
		}
	}

	/**
	 * Create a BasicTextureData for a VLEN variable
	 */
	private BasicTextureData makeVlenTextureData(List<DimensionSlice> slicing, ByteBuffer buffer) {
		int height = 0;
		int width = 0;
		for (DimensionSlice slice : slicing) {
			if (slice.count() > 1l) {
				width = (int) slice.count();
				height = buffer.capacity() / width / Float.BYTES;
				break;
			}
		}

		return new BasicTextureData(width, height, buffer);
	}

	/**
	 * Create a BasicTextureData
	 */
	private BasicTextureData makeTextureData(List<DimensionSlice> slicing, ByteBuffer buffer) {
		int height = 0;
		int width = 0;
		for (DimensionSlice slice : slicing) {
			if (slice.count() > 1l) {
				if (height == 0) {
					height = (int) slice.count();
				} else {
					width = (int) slice.count();
				}
			}
		}

		return new BasicTextureData(width, height, buffer);
	}

	/**
	 * Display variable content in chart view using JavaFx
	 */
	private void loadChartView(NetcdfVariable ncVariable, List<DimensionSlice> slicing)
			throws NCException, GIOException {
		logger.debug("Initializing chart view for variable {}", ncVariable.getName());
		// Loading buffer
		DataType dataType = ncVariable.getUnpackedDataType();
		if (ncVariable.isVlen() || dataType.isFloatingPoint()) {
			ByteBuffer buffer = readVariable(DataType.DOUBLE, ncVariable, slicing);
			var stats = getDoubleStatistics(buffer, ByteBuffer::getDouble);
			if (stats.getCount() > 0) {
				cmpChart.fitToDoubleData(ncVariable, buffer);
			} else {
				cmpChart.fitToErrorInData("No data.");
			}
		} else if (dataType == DataType.LONG || dataType == DataType.ULONG) {
			ByteBuffer buffer = readVariable(DataType.LONG, ncVariable, slicing);
			var stats = getLongStatistics(buffer, ByteBuffer::getLong);
			if (stats.getCount() > 0) {
				cmpChart.fitToLongData(ncVariable, buffer);
			} else {
				cmpChart.fitToErrorInData("No data.");
			}
		} else if (dataType.isIntegral()) {
			ByteBuffer buffer = readVariable(DataType.INT, ncVariable, slicing);
			var stats = getLongStatistics(buffer, ByteBuffer::getInt);
			if (stats.getCount() > 0) {
				cmpChart.fitToIntegerData(ncVariable, buffer);
			} else {
				cmpChart.fitToErrorInData("No data.");
			}
		} else {
			cmpChart.fitToErrorInData("Data type not suitable for rendering a chart.");
		}
	}

	/**
	 * Display the current variable content in 2D or 3D view depending on the specified slicing
	 */
	private void onSlicingChanged(List<DimensionSlice> slicing) {
		currentSlicing = slicing;
		currentVariable.ifPresent(//
				ncVariable -> lblInfo.getDisplay().syncExec(//
						() -> displaySlicedVariable(ncVariable)));
	}

	/**
	 * Display variable content in 2D or chart view depending on the slicing
	 */
	private void displaySlicedVariable(NetcdfVariable ncVariable) {
		var slicing = inferCurrentSlicing(ncVariable);
		if (ensureFileOpen()) {
			// Clean to force the reload
			txtRawData.setText("");

			// Number of dimensions that can be sliced
			int slicableDimensionCount = (int) slicing.stream().filter(d -> d.dimension().getLength() > 1l).count();
			// Number of dimensions to display.
			int displayDimensionCount = (int) slicing.stream()
					.filter(d -> !d.enabled() && d.dimension().getLength() > 1l).count();
			displayDimensionCount += ncVariable.isVlen() ? 1 : 0;

			try {
				showSlicing(slicableDimensionCount > 1 || ncVariable.isVlen());
				if (displayDimensionCount == 0 && btnRawData.getData() == DataFeature.GRAPHIC) {
					// Can display single value only in raw mode
					loadRawDataView(cmpChart);
					showView(txtRawData);
				} else if (displayDimensionCount == 1) {
					loadChartView(ncVariable, slicing);
					if (btnRawData.getData() == DataFeature.GRAPHIC) {
						loadRawDataView(cmpChart);
						showView(txtRawData);
					} else {
						showView(cmpChart);
					}
				} else if (displayDimensionCount == 2) {
					load2dView(ncVariable, slicing);
					if (currentRendering.isPresent()) {
						if (btnRawData.getData() == DataFeature.GRAPHIC) {
							loadRawDataView(cmpOgl);
							showView(txtRawData);
						} else {
							showView(cmpOgl);
						}
					} else {
						// Keep slicing widget displayed (No data to display can apply to a specific
						// slice only)
						displayInfo("No data to display", false);
					}
				} else if (slicing.isEmpty()) {
					displayInfo("Rendering of the variable " + ncVariable.getName() + " is not possible : no dimension",
							true);

				} else {
					displayInfo("Rendering of the variable " + ncVariable.getName()
							+ " is not possible. Filtering is not suitable", false);
					logger.warn("Unable to display variable {} : slicing not suitable", ncVariable.getName());
				}
			} catch (NCException e) {
				String errorMessage = "Error when rendering the variable " + ncVariable.getName() + ". Abort";
				logger.warn(errorMessage, e);
				displayInfo(errorMessage, true);
			} catch (IllegalArgumentException e) { // See Buffer.createCapacityException
				displayInfo("Not enough memory", true);
			} catch (GException e) {
				displayInfo(e.getMessage(), true);
			}
		}
	}

	/** Browse the buffer and apply a conversion to each date */
	private void convertDates(ByteBuffer buffer, LongUnaryOperator conversion) {
		buffer.rewind();
		while (buffer.hasRemaining()) {
			buffer.mark();
			long date = conversion.applyAsLong(buffer.getLong());
			buffer.reset().putLong(date);
		}
	}

	/** Ensure that the file is opened. If not, display an error message */
	private boolean ensureFileOpen() {
		if (fileManager.getNetcdfFile().isEmpty()) {
			displayInfo("File " + fileManager.getFileInfo().getFilename()
					+ " not available : already opened by an other process/editor.", true);
			return false;
		}
		return true;
	}

	/** setVisible of slicing */
	private void showSlicing(boolean visible) {
		UIUtils.setVisible(cmpDimSlice, visible);
	}

	/**
	 * Display an information. Hide chart/texture
	 */
	private void displayInfo(String info, boolean hideSlicing) {
		lblInfo.setText(info);
		varContentLayout.topControl = lblInfo;
		varContentLayout.topControl.getParent().requestLayout();
		UIUtils.setVisible(cmpSliceParent, !hideSlicing);
	}

	/** Set visible a chart or a texture */
	private void showView(Control control) {
		if (control != null) {
			varContentLayout.topControl = control;
			varContentLayout.topControl.getParent().requestLayout();

			if (control == txtRawData) {
				morphBtnTo(DataFeature.GRAPHIC);
			} else {
				morphBtnTo(DataFeature.RAW);
			}

			UIUtils.setVisible(cmpSliceParent, true);
			UIUtils.setVisible(cmp2dTools, control == cmpOgl);
		}
	}

	/** Switch the origin of the texture */
	private void flipYAxis() {
		flipYAxis = !flipYAxis;
		partContext.set(ContextNames.FLIP_Y_AXIS, flipYAxis);

		if (currentVariable.isPresent()) {
			// Reload the texture to force the creation of new TextureRenderer
			settingsBearer.getTexture(fileManager.getFileInfo().getPath(), currentVariable.get().getPath())
					.ifPresent(textureData -> SwingUtilities.invokeLater(() -> {
						GLCanvas canvas = sceneFactory.create(textureData);
						oglFrame.removeAll();
						oglFrame.add(canvas, BorderLayout.CENTER);
						oglFrame.doLayout();
					}));
		}
	}

	/** Switch the origin of the texture */
	private void showGrid() {
		showGrid = !showGrid;
		partContext.set(ContextNames.DRAW_GRID, showGrid);
		SwingUtilities.invokeLater(() -> {
			if (oglFrame.getComponentCount() > 0)
				((GLCanvas) oglFrame.getComponent(0)).repaint();
		});
	}

	/** Switch the display graphic <-> raw data */
	private void swithBtnRawData() {
		if (btnRawData.getData() == DataFeature.RAW) {
			loadRawDataView(varContentLayout.topControl);
			showView(txtRawData);
		} else {
			// Restore previous display
			showView((Control) txtRawData.getData());
		}
	}

	private void loadRawDataView(Control backControl) {
		if (txtRawData.getText().isEmpty()) {
			BusyIndicator.showWhile(lblVariableDesc.getDisplay(), this::initRawDataText);
		}
		// Memorize previous display for the future switch call
		txtRawData.setData(backControl);
	}

	/** Creates the Text to display raw data */
	private void initRawDataText() {
		if (currentVariable.isPresent()) {
			NetcdfVariable ncVariable = currentVariable.get();
			try {
				String formattedValues = formatRawData(ncVariable);
				txtRawData.setText(formattedValues);
			} catch (NCException | GIOException e) {
				txtRawData.setText("Error occured. Formattage of raw data aborted.");
				logger.warn("Error when displaying raw data of variable " + ncVariable.getName(), e);
			}
		}
	}

	/**
	 * @return A formatted String with all the values contained in the variable
	 */
	private String formatRawData(NetcdfVariable ncVariable) throws NCException, GIOException {
		String formattedValues;
		var slicing = inferCurrentSlicing(ncVariable);
		if (DataType.getDataTypeFromNCType(ncVariable.getType()) == DataType.STRING) {
			String[] data = readStringData(ncVariable);
			StringDataFormatter stringDataFormatter = new StringDataFormatter(data);
			formattedValues = stringDataFormatter.format();
		} else {
			List<ByteBuffer> buffers = readRawData(ncVariable);
			formattedValues = new RawDataFormatter(ncVariable, slicing, buffers).format();
		}
		return formattedValues;
	}

	/**
	 * Log statistics of buffer (min value, max, nb of values, nb of NaN)
	 */
	private DoubleSummaryStatistics getDoubleStatistics(ByteBuffer buffer, ToDoubleFunction<ByteBuffer> convertion) {
		buffer.rewind();
		DoubleSummaryStatistics stats = new DoubleSummaryStatistics();
		int nanCount = 0;
		// Loading variable
		while (buffer.hasRemaining()) {
			double value = convertion.applyAsDouble(buffer);
			if (Double.isNaN(value)) {
				nanCount++;
			} else {
				stats.accept(value);
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug(" ==> Min value : {}", stats.getMin());
			logger.debug(" ==> Max value : {}", stats.getMax());
			logger.debug(" ==> Val count : {}", stats.getCount());
			logger.debug(" ==> NaN count : {}", nanCount);
		}
		return stats;
	}

	/**
	 * Compute statistics of buffer (min value, max, nb of values, nb of NaN)
	 */
	private LongSummaryStatistics getLongStatistics(ByteBuffer buffer, ToLongFunction<ByteBuffer> convertion) {
		LongSummaryStatistics stats = new LongSummaryStatistics();
		buffer.rewind();
		while (buffer.hasRemaining()) {
			stats.accept(convertion.applyAsLong(buffer));
		}

		if (logger.isDebugEnabled()) {
			logger.debug(" ==> Min value : {}", stats.getMin());
			logger.debug(" ==> Max value : {}", stats.getMax());
			logger.debug(" ==> Val count : {}", stats.getCount());
		}
		return stats;
	}

	@PreDestroy
	private void dispose() {
		fileManager.dispose();
	}

	/** Set current rendering as Selection */
	@Focus
	private void focus() {
		selectionService.setSelection(currentRendering.isPresent() ? new StructuredSelection(currentRendering.get())
				: StructuredSelection.EMPTY);
	}

	/**
	 * @param currentRendering the {@link #currentRendering} to set
	 */
	private void setCurrentRendering(Object rendering) {
		currentRendering = Optional.ofNullable(rendering);
		focus();
	}

	/** Feature set of button to swap from Raw data to graphic display */
	private enum DataFeature {
		RAW,
		GRAPHIC
	}

	/** Ensure current slicing is suitable for the variable. If not, prepare and set a new one */
	private List<DimensionSlice> inferCurrentSlicing(NetcdfVariable ncVariable) {
		var defaultSlicing = prepareSlicing(ncVariable);
		// Check if current slicing is compatible with the variable (same dimensions)
		boolean compatibleSlicing = currentSlicing.size() == defaultSlicing.size();
		for (int i = 0; compatibleSlicing && i < currentSlicing.size(); i++) 
			compatibleSlicing = currentSlicing.get(i).dimension().equals(defaultSlicing.get(i).dimension());

		// Current slicing is not compatible with the variable. Use the default one
		if (!compatibleSlicing)
			currentSlicing = defaultSlicing;
		return currentSlicing;
	}

	/**
	 * Prepare the list of dimension used to display the variable.
	 */
	private List<DimensionSlice> prepareSlicing(NetcdfVariable ncVariable) {
		List<NCDimension> shape = ncVariable.getShape();
		if (shape.isEmpty()) {
			// Scalar
			return List.of(new DimensionSlice(null, 0, 1, true));
		}
		int slicableDimensionCount = (int) shape.stream().filter(dim -> dim.getLength() > 1).count();
		slicableDimensionCount += ncVariable.isVlen() ? 1 : 0;

		// Force slicing to ensure that the number of dimensions does not exceed 2.
		int dimensionToSlice = Math.max(slicableDimensionCount - 2, 0);

		List<DimensionSlice> result = new LinkedList<>();
		for (NCDimension dimension : ncVariable.getShape()) {
			if (dimension.getLength() > 1 && dimensionToSlice > 0) {
				// Force slicing
				result.add(new DimensionSlice(dimension, 0, 1, true));
				dimensionToSlice--;
				continue;
			}
			result.add(new DimensionSlice(dimension));
		}
		return result;
	}

}
