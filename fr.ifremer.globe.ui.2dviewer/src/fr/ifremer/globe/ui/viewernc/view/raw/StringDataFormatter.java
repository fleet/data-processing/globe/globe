/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.view.raw;

/**
 * Formatter of String data.
 */
public class StringDataFormatter {

	private final String[] data;

	/**
	 * Constructor
	 */
	public StringDataFormatter(String[] data) {
		this.data = data;
	}

	/** Format all buffers */
	public String format() {
		StringBuilder result = new StringBuilder();
		for (String oneString : data) {
			result.append("{");
			result.append(oneString);
			result.append("}\n");
		}
		if (result.length() >= 1_000_000) {
			result.insert(0, "Too much data. The content was truncated\n\n");
		}
		return result.toString();
	}
}
