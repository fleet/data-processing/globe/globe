package fr.ifremer.globe.ui.viewernc.parametersview;

import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.FloatRange;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.widgets.LabelFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.texture.TextureDataParameters;
import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;

/**
 * Parameter composite for {@link PointCloudLayer}.
 */
public class TextureSettingsComposite extends Composite {

	/** Edited {@link PointCloudLayer} */
	private TextureDataParameters settings;
	private final IBasicTextureData texture;
	private final TextureDataParametersBearer textureDataParametersBearer;

	/**
	 * Constructor
	 */
	public TextureSettingsComposite(Composite parent, IBasicTextureData texture, TextureDataParameters settings,
			TextureDataParametersBearer textureDataParametersBearer) {
		super(parent, SWT.NONE);
		this.settings = settings;
		this.texture = texture;
		this.textureDataParametersBearer = textureDataParametersBearer;
		initializeComposite();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	public void initializeComposite() {
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		// Title
		String filePath = textureDataParametersBearer.getFilePath(texture).orElse("File ?");
		String variable = textureDataParametersBearer.getVariable(texture).orElse("Variable ?");
		String title = "Texture of " + variable + " (" + FilenameUtils.getName(filePath) + ")";
		LabelFactory.newLabel(SWT.NONE)//
				.text(title)//
				.tooltip(title)//
				.layoutData(GridDataFactory.fillDefaults().grab(true, false).create())//
				.create(this);

		// Color
		ColorContrastModel colorContrastModel = createColorContrastModel();
		ColorContrastWidget colorComposite = new ColorContrastWidget(this, colorContrastModel,
				Optional.empty());
		GridDataFactory.fillDefaults().grab(true, false).applyTo(colorComposite);
		colorComposite.subscribe(model -> {
			settings = settings.withColor(model.invertColor(), model.colorMapIndex(), model.contrastMin(),
					model.contrastMax(), model.opacity());
			textureDataParametersBearer.setSettings(texture, settings);
		});

	}

	/**
	 * @return a new ColorContrastModel for the current layer
	 */
	protected ColorContrastModel createColorContrastModel() {
		return new ColorContrastModelBuilder() //
				.setColorMapIndex(settings.colorMapIndex) //
				.setInvertColor(settings.isColorMapReversed) //
				.setContrastMin(settings.minContrast) //
				.setContrastMax(settings.maxContrast)//
				.setOpacity(settings.opacity)//
				.setLimitMin(settings.minThreshold) //
				.setLimitMax(settings.maxThreshold)//
				.setResetMin(settings.minThreshold) //
				.setResetMax(settings.maxThreshold)//
				.build();
	}

}
