/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.parametersview;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import fr.ifremer.globe.ui.texture.TextureDataParameters;
import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.globe.utils.map.TypedMap;

/**
 * Bearer of the all the TextureDataParameters.
 */
public class TextureDataParametersBearer {

	/**
	 * Map to retain all useful links :
	 * <ul>
	 * <li>Variable -> Settings</li>
	 * <li>Texture -> Settings</li>
	 * <li>Texture -> FilePath</li>
	 * <li>Texture -> Variable</li>
	 * <li>Variable -> Texture</li>
	 * </ul>
	 */
	private final TypedMap baseOfKnowledge = new TypedMap();

	/** Listeners on settings changes */
	private final List<Consumer<IBasicTextureData>> listeners = new LinkedList<>();

	/**
	 * Constructor
	 */
	TextureDataParametersBearer() {
		// The only isnstance is created and hold by the Osgi service TextureSettingsCompositeFactory.
	}

	/**
	 * Creates and bear the settings for the specified NcVariable and texture <br>
	 * If settings are already known for the variable, they are reused
	 */
	public void bear(String filePath, String variable, IBasicTextureData texture) {
		TypedKey<TextureDataParameters> keyVar2Settings = getVar2SettingsKey(filePath, variable);
		TextureDataParameters settings = baseOfKnowledge.get(keyVar2Settings).orElse(null);

		// Remove previous links
		TypedKey<IBasicTextureData> keyVar2Texture = getVar2TextureKey(filePath, variable);
		IBasicTextureData oldTexture = baseOfKnowledge.get(keyVar2Texture).orElse(null);
		if (oldTexture != null) {
			var keyTexture2File = getTexture2FileKey(oldTexture);
			var keyTexture2Var = getTexture2VarKey(oldTexture);
			baseOfKnowledge.remove(keyVar2Texture);
			baseOfKnowledge.remove(keyTexture2File);
			baseOfKnowledge.remove(keyTexture2Var);
			baseOfKnowledge.remove(keyVar2Settings);
			baseOfKnowledge.remove(getTexture2SettingsKey(oldTexture));
		}

		// Retain the relations between variable and texture
		baseOfKnowledge.put(getTexture2FileKey(texture).bindValue(filePath));
		baseOfKnowledge.put(getTexture2VarKey(texture).bindValue(variable));
		baseOfKnowledge.put(keyVar2Texture.bindValue(texture));

		// Update links
		if (settings != null) {
			settings = settings.adaptTo(texture);
		} else {
			settings = TextureDataParameters.defaultFor(texture);
		}
		baseOfKnowledge.put(keyVar2Settings.bindValue(settings));
		baseOfKnowledge.put(getTexture2SettingsKey(texture).bindValue(settings));
	}

	/**
	 * @return the settings affected to the texture. May be empty if texture is disposed or created by an other bundle
	 */
	public Optional<TextureDataParameters> getSettings(IBasicTextureData texture) {
		return baseOfKnowledge.get(getTexture2SettingsKey(texture));
	}

	/**
	 * Change the settings affected to the texture.
	 */
	public synchronized void setSettings(IBasicTextureData texture, TextureDataParameters settings) {
		// update setting linked to texture
		baseOfKnowledge.put(getTexture2SettingsKey(texture).bindValue(settings));

		// update setting linked to variable
		baseOfKnowledge.get(getTexture2FileKey(texture))//
				.ifPresent(filePath -> baseOfKnowledge.get(getTexture2VarKey(texture))//
						.ifPresent(variable -> baseOfKnowledge
								.put(getVar2SettingsKey(filePath, variable).bindValue(settings))));

		// Notifying
		listeners.forEach(c -> c.accept(texture));
	}

	/** Key used to retrieve a TextureDataParameters linked to a variable */
	private TypedKey<TextureDataParameters> getVar2SettingsKey(String filePath, String variable) {
		return new TypedKey<>("Variable->Settings:" + filePath + ":" + variable);
	}

	/** Key used to retrieve a TextureDataParameters linked to a texture */
	private TypedKey<TextureDataParameters> getTexture2SettingsKey(IBasicTextureData texture) {
		return new TypedKey<>("Texture->Settings:" + texture.toString());
	}

	/** Key used to retrieve a texture linked to a variable */
	private TypedKey<IBasicTextureData> getVar2TextureKey(String filePath, String variable) {
		return new TypedKey<>("Variable->Texture:" + filePath + ":" + variable);
	}

	/** Key used to retrieve a filePath linked to a texture */
	private TypedKey<String> getTexture2FileKey(IBasicTextureData texture) {
		return new TypedKey<>("Texture->File:" + texture.toString());
	}

	/** Key used to retrieve a variable linked to a texture */
	private TypedKey<String> getTexture2VarKey(IBasicTextureData texture) {
		return new TypedKey<>("Texture->Variable:" + texture.toString());
	}

	/** Ask to be notify when settings change for a texture */
	public synchronized void addSettingChangedListener(Consumer<IBasicTextureData> settingChangedListener) {
		listeners.add(settingChangedListener);
	}

	/** Ask to be no more notify when settings change for a texture */
	public synchronized void removeSettingChangedListener(Consumer<IBasicTextureData> settingChangedListener) {
		listeners.remove(settingChangedListener);
	}

	/** @return the IBasicTextureData texture linked to a variable if any */
	public Optional<IBasicTextureData> getTexture(String filePath, String variable) {
		TypedKey<IBasicTextureData> keyVar2Texture = getVar2TextureKey(filePath, variable);
		return baseOfKnowledge.get(keyVar2Texture);
	}

	/**
	 * @return the file path linked to the texture
	 */
	public Optional<String> getFilePath(IBasicTextureData texture) {
		var keyTexture2File = getTexture2FileKey(texture);
		return baseOfKnowledge.get(keyTexture2File);
	}

	/**
	 * @return the variable path linked to the texture
	 */
	public Optional<String> getVariable(IBasicTextureData texture) {
		var keyTexture2Var = getTexture2VarKey(texture);
		return baseOfKnowledge.get(keyTexture2Var);
	}
}
