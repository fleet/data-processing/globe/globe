package fr.ifremer.globe.ui.viewernc.parametersview;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.texture.TextureDataParameters;
import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;

/**
 * Make a Composite to edit the {@link PointCloudLayer} parameters.
 */
@Component(name = "globe_viewer2d_texture_param_composite_factory", service = { IParametersViewCompositeFactory.class,
		TextureSettingsCompositeFactory.class })
public class TextureSettingsCompositeFactory implements IParametersViewCompositeFactory {

	/** Parameters bearer */
	private final TextureDataParametersBearer textureDataParametersBearer = new TextureDataParametersBearer();

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		if (selection instanceof IBasicTextureData) {
			IBasicTextureData texture = (IBasicTextureData) selection;
			var optSettings = textureDataParametersBearer.getSettings(texture);
			if (optSettings.isPresent()) {
				TextureDataParameters settings = optSettings.get();
				TextureSettingsComposite result = new TextureSettingsComposite(parent, texture, settings,
						textureDataParametersBearer);
				return Optional.of(result);
			}
		}

		// Not concerned
		return Optional.empty();
	}

	/**
	 * @return the {@link #textureDataParametersBearer}
	 */
	public TextureDataParametersBearer getTextureDataParametersBearer() {
		return textureDataParametersBearer;
	}
}
