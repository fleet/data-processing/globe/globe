/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.handlers;

import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewernc.parametersview.TextureDataParametersBearer;
import fr.ifremer.globe.ui.viewernc.parametersview.TextureSettingsCompositeFactory;
import fr.ifremer.globe.ui.viewernc.view.NcFileOpeningManager;
import fr.ifremer.globe.ui.viewernc.view.NetcdfViewer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 *
 */
public class OpenNetcdfViewer extends AbstractNodeHandler {

	/** Id of the command in fragment.e4xmi */
	public static final String COMMAND_ID = "fr.ifremer.globe.ui.2dviewer.command.openncviewer";
	/** Id of the command in fragment.e4xmi */
	public static final String COMMAND_PARAM = "fr.ifremer.globe.ui.2dviewer.commandparameter.filepath";

	@Inject
	private TextureSettingsCompositeFactory textureSettingsCompositeFactory;

	/** {@inheritDoc} */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		Optional<FileInfoNode> optFileInfoNode = getUniqueFileInfoNode(selectionService);
		return optFileInfoNode.isPresent() && optFileInfoNode.get().getFileInfo().getContentType().isNetcdf();
	}

	@CanExecute
	public boolean canExecute(@org.eclipse.e4.core.di.annotations.Optional @Named(COMMAND_PARAM) String filePath,
			ESelectionService selectionService, MUIElement uiElement) {
		return filePath != null || checkExecution(selectionService, uiElement);
	}

	/**
	 * Execute the handler.<br>
	 *
	 * @param filePath if present, open the NetCDF Viewer directly on it.<br>
	 *            Search the selected file in the project explorer
	 */
	@Execute
	public void execute(@org.eclipse.e4.core.di.annotations.Optional @Named(COMMAND_PARAM) String filePath,
			ESelectionService selectionService, EPartService partService, MApplication application,
			EModelService modelService, IEclipseContext parentContext) {
		IFileInfo fileInfo = null;
		if (filePath != null) {
			fileInfo = new BasicFileInfo(filePath);
		} else {
			Optional<FileInfoNode> optFileInfoNode = getUniqueFileInfoNode(selectionService);
			if (optFileInfoNode.isPresent()) {
				fileInfo = optFileInfoNode.get().getFileInfo();
			}
		}

		if (fileInfo != null) {
			launchNcViewer(partService, application, modelService, parentContext, fileInfo);
		}
	}
	
	private static int partCounter = 0;
	private static int getAndIncrementPartCounter() {
		return partCounter++;
	}
	
	/**
	 * Open a NetCDF Viewer on the specified file info
	 */
	public void launchNcViewer(EPartService partService, MApplication application, EModelService modelService,
			IEclipseContext parentContext, IFileInfo fileInfo) {
		// Open the file
		NcFileOpeningManager ncFileManager = new NcFileOpeningManager(partService, fileInfo);
		if (ncFileManager.getNetcdfFile().isEmpty()) {
			Messages.openWarningMessage("NetCDF viewer error",
					"Can not open the file " + ncFileManager.getFileInfo().getFilename()
							+ ". Already opened by an other process/editor or not a NetCDF file.");
			return;
		}
		partService.addPartListener(ncFileManager);

		// Create the part
		String id = NetcdfViewer.PART_ID + getAndIncrementPartCounter() +":" + fileInfo.getFilename();
		MPart existingViewer = partService.findPart(id);
		if (existingViewer == null) {
			BusyIndicator.showWhile(Display.getDefault(), () -> {
				MPart newViewer = partService.createPart(NetcdfViewer.PART_ID);
				initializeContext(parentContext, newViewer, ncFileManager);
				newViewer.setLabel(newViewer.getLabel() + " - " + fileInfo.getFilename());
				newViewer.setElementId(id);
				PartManager.addPartToStack(newViewer, application, modelService, PartManager.RIGHT_STACK_ID);
				PartManager.showPart(partService, newViewer);
			});
		} else {
			if (existingViewer.getContext() == null) {
				// Previously closed.
				initializeContext(parentContext, existingViewer, ncFileManager);
			} else {
				ncFileManager.dispose();
			}
			partService.bringToTop(existingViewer);
		}
	}

	/** Create a dedicated context for the MPart and set values in it */
	private void initializeContext(IEclipseContext parentContext, MPart mPart, NcFileOpeningManager ncFileManager) {
		IEclipseContext viewerContext = parentContext.createChild();
		viewerContext.set(NcFileOpeningManager.class, ncFileManager);
		viewerContext.set(MPart.class, mPart);
		viewerContext.set(TextureDataParametersBearer.class,
				textureSettingsCompositeFactory.getTextureDataParametersBearer());
		viewerContext.set(ContextNames.FLIP_Y_AXIS, false);
		viewerContext.set(ContextNames.DRAW_CURSOR_LINES, false);
		viewerContext.set(ContextNames.DRAW_GRID, true);
		viewerContext.set(ContextNames.RESTRICT_GRID, true);
		mPart.setContext(viewerContext);
		ncFileManager.observePart(mPart);
	}
}
