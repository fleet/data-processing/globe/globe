/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewernc.handlers;

import java.util.Map;

import jakarta.inject.Named;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.CommandManager;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * Handler to open the NetCDF Viewer on any nc file
 */
public class OpenNetcdfViewerWith {
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, CommandManager commandManager,
			IEclipseContext context, EHandlerService handlerService) {
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		dialog.setFilterNames(new String[] { "NetCDF file (*.nc)" });
		dialog.setFilterExtensions(new String[] { "*.nc" });
		if (dialog.open() != null) {
			Command command = commandManager.getCommand(OpenNetcdfViewer.COMMAND_ID);
			ParameterizedCommand parameterizedCommand = ParameterizedCommand.generateCommand(command,
					Map.of(OpenNetcdfViewer.COMMAND_PARAM,
							FilenameUtils.concat(dialog.getFilterPath(), dialog.getFileName())));
			handlerService.executeHandler(parameterizedCommand);
		}
	}

}