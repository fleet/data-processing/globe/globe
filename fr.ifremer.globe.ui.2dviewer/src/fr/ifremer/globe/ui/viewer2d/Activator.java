package fr.ifremer.globe.ui.viewer2d;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.ui.viewer2d.preferences.Viewer2DPreferences;

public class Activator implements BundleActivator {

	private static Viewer2DPreferences param;

	@Override
	public void start(BundleContext context) throws Exception {
		// Initialization of PreferenceComposite system
		PreferenceComposite root = PreferenceRegistry.getInstance().getViewsSettingsNode();
		param = new Viewer2DPreferences(root, "WaterColumn 2D Viewer");
	}

	@Override
	public void stop(BundleContext context) throws Exception {

	}

	public static Viewer2DPreferences get2DPreferences() {
		return param;
	}

}
