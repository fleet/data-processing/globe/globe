package fr.ifremer.globe.ui.viewer2d.preferences;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.ui.workspace.WorkspaceDirectory;

public class Viewer2DPreferences extends PreferenceComposite {

	private BooleanPreferenceAttribute drawLimits;
	private BooleanPreferenceAttribute drawMetrics;
	private BooleanPreferenceAttribute drawCursorMetrics;
	private IntegerPreferenceAttribute markerSize;
	private DirectoryPreferenceAttribute screenshotFolder;
	private BooleanPreferenceAttribute singlePointingMode;
	private BooleanPreferenceAttribute equalizeAxis;
	private BooleanPreferenceAttribute drawZDetection;
	private IntegerPreferenceAttribute zDetectionSize;
	private ColorPreferenceAttribute zDetectionColor;

	public Viewer2DPreferences(PreferenceComposite superNode, String nodeName) {
		super(superNode, nodeName);
		Path defaultDir = FileSystems.getDefault().getPath(WorkspaceDirectory.getInstance().getPath()).toAbsolutePath();
		singlePointingMode = new BooleanPreferenceAttribute("singlePointingMode2D",
				"Toggle single pointing mode (for 2D view)", false);

		drawLimits = new BooleanPreferenceAttribute("drawLimits", "draw limits", true);
		drawMetrics = new BooleanPreferenceAttribute("drawMetrics", "draw metrics", true);
		drawCursorMetrics = new BooleanPreferenceAttribute("drawCursorMetrics", "draw cursor metrics", false);
		markerSize = new IntegerPreferenceAttribute("markerSize", "marker size", 20);
		screenshotFolder = new DirectoryPreferenceAttribute("screenshotFolder", "screenshot folder", defaultDir);
		equalizeAxis = new BooleanPreferenceAttribute("equalizeAxis", "equalize axis scale", false);
		drawZDetection = new BooleanPreferenceAttribute("drawZDetection", "Z Detection", true);
		zDetectionSize = new IntegerPreferenceAttribute("zDetectionSize", "Z Detection point size", 3);
		zDetectionColor = new ColorPreferenceAttribute("zDetectionColor", "Color of Z Detection", GColor.WHITE);

		declareAttribute(drawLimits);
		declareAttribute(drawMetrics);
		declareAttribute(drawCursorMetrics);
		declareAttribute(markerSize);
		declareAttribute(screenshotFolder);
		declareAttribute(singlePointingMode);
		declareAttribute(equalizeAxis);
		declareAttribute(drawZDetection);
		declareAttribute(drawZDetection);
		declareAttribute(zDetectionColor);
		load();

	}

	public int getMarkerSize() {
		return markerSize.getValue();
	}

	public Path getScreenshotFolder() {
		return screenshotFolder.getValue();
	}

	public boolean getDrawLimits() {
		return drawLimits.getValue();
	}

	public boolean getDrawMetrics() {
		return drawMetrics.getValue();
	}

	public void setDrawMetrics(boolean b) {
		drawMetrics.setValue(b, false);
	}

	public boolean getDrawZDetection() {
		return drawZDetection.getValue();
	}

	public void setDrawZDetection(boolean b) {
		drawZDetection.setValue(b, false);
	}

	public int getZDetectionSize() {
		return zDetectionSize.getValue();
	}

	public void setZDetectionSize(int b) {
		zDetectionSize.setValue(b, false);
	}

	public GColor getZDetectionColor() {
		return zDetectionColor.getValue();
	}

	public void setZDetectionColor(GColor b) {
		zDetectionColor.setValue(b, false);
	}

	public boolean getDrawCursorMetrics() {
		return drawCursorMetrics.getValue();
	}

	public void setDrawCursorMetrics(boolean b) {
		drawCursorMetrics.setValue(b, false);
	}

	public void changeScreenshotFolder(String filename) {
		screenshotFolder.setValueAsString(filename, true);
	}

	public boolean isSinglePointingModeEnabled() {
		return singlePointingMode.getValue();
	}

	/**
	 * @return the {@link #equalizeAxis}
	 */
	public boolean getEqualizeAxis() {
		return equalizeAxis.getValue();
	}

	/**
	 * @param equalizeAxis the {@link #equalizeAxis} to set
	 */
	public void setEqualizeAxis(boolean equalizeAxis) {
		this.equalizeAxis.setValue(equalizeAxis, false);
	}
}
