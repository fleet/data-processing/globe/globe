package fr.ifremer.globe.ui.viewer2d.view.scene;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.Viewer2DStatusBar;
import fr.ifremer.globe.ui.viewer2d.view.components.MarkersComponent;
import fr.ifremer.globe.ui.viewer2d.view.texture.TextureParameters;
import fr.ifremer.globe.ui.viewer2d.view.utils.ScreenshotTool;
import fr.ifremer.globe.ui.viewer2d.view.wc.NativeWaterColumn2DParameters;
import fr.ifremer.globe.ui.viewer2d.view.wc.WaterColumn2DParameters;
import fr.ifremer.globe.ui.widget.player.IndexPlayerGetterOperation;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import fr.ifremer.viewer3d.layers.wc.NativeWCVolumicLayer;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;

/**
 * Builder of Viewer2d part.<br>
 * The part in built with a specific Eclipse context containing (key in classe):
 * <ul>
 * <li>IFileInfo : edited file</li>
 * <li>GLCanvas : the AWT component which provides OpenGL rendering support</li>
 * <li>IWWLayer : the current edited WW (water column or texture)</li>
 * <li>WCVolumicLayer (optional) : the WW layer of the edited water column</li>
 * <li>IWWTextureLayer (optional) : the WW layer of the edited water slice</li>
 * <li>IndexedPlayer (optional) : player of the WC</li>
 * <li>IViewer2DComponentProvider : provider of GL component to insert to the scene</li>
 * <li>Viewer2DStatusBar : status bar of the view</li>
 * <li>AbstractGridComponent : GL component of grid</li>
 * <li>MarkersComponent : GL component of markers</li>
 * <li>Viewer2DScene : the scene rendering all GL components</li>
 * <li>SceneProperties : object holding all scene properties</li>
 * <li>ScreenshotTool : utility instance to obtain screenshots of the scene</li>
 * </ul>
 */
public class Viewer2DBuilder {

	protected final IFileInfo fileInfo;
	protected MPart part;
	protected WCVolumicLayer wcVolumicLayer;
	protected NativeWCVolumicLayer nativeWcVolumicLayer;
	protected IWWTextureLayer textureLayer;

	/**
	 * Constructor
	 */
	public Viewer2DBuilder(IFileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}

	/**
	 * @param part the {@link #part} to set
	 */
	public Viewer2DBuilder withPart(MPart part) {
		this.part = part;
		return this;
	}

	/** Add a WC layer */
	public Viewer2DBuilder withWaterColumnVolumicLayer(WCVolumicLayer wcVolumicLayer) {
		this.wcVolumicLayer = wcVolumicLayer;
		return this;
	}

	/** Add a WC layer */
	public Viewer2DBuilder withNativeWaterColumnVolumicLayer(NativeWCVolumicLayer nativeWcVolumicLayer) {
		this.nativeWcVolumicLayer = nativeWcVolumicLayer;
		return this;
	}

	/** Add a Texture layer */
	public Viewer2DBuilder withTextureLayer(IWWTextureLayer textureLayer) {
		this.textureLayer = textureLayer;
		return this;
	}

	public MPart build(EPartService partService) {
		IEclipseContext partContext = part.getContext();
		partContext.set(IFileInfo.class, fileInfo);
		partContext.set(ContextNames.FLIP_Y_AXIS, false);
		partContext.set(ContextNames.DRAW_CURSOR_LINES, true);
		partContext.set(ContextNames.RESTRICT_GRID, false);

		// Prepare a GL component for markers
		OsgiUtils.make(MarkersComponent.class, partContext);

		// Initialize Canvas
		GLCanvas canvas = createCanvas();
		partContext.set(GLCanvas.class, canvas);

		// build a Viewer 2D of a water column
		if (nativeWcVolumicLayer != null) {
			init4NativeWc(partContext);
		} else if (wcVolumicLayer != null) {
			init4Wc(partContext);
		} else if (textureLayer != null) {
			init4Texture(partContext);
		}

		// Status bar
		OsgiUtils.make(Viewer2DStatusBar.class, partContext);

		// Initialize Scene
		OsgiUtils.make(SceneProperties.class, partContext);
		OsgiUtils.make(Viewer2DScene.class, partContext);

		// Initialize screenshot tool
		OsgiUtils.make(ScreenshotTool.class, partContext);

		UIUtils.bindFocus(canvas, partService, part);

		return part;
	}

	/** Initialize components for diplaying a Water Column */
	protected void init4NativeWc(IEclipseContext partContext) {
		// Activates the WC layer
		if (!nativeWcVolumicLayer.isLayerActive()) {
			nativeWcVolumicLayer.activate(true);
		}

		partContext.set(IWWLayer.class, nativeWcVolumicLayer);
		partContext.set(NativeWCVolumicLayer.class, nativeWcVolumicLayer);
		partContext.set(IndexedPlayer.class, nativeWcVolumicLayer.getPlayer());
		OsgiUtils.make(NativeWaterColumn2DParameters.class, ISceneComponentProvider.class, partContext);
	}

	/** Initialize components for diplaying a Water Column */
	protected void init4Wc(IEclipseContext partContext) {
		// Activates the WC layer
		if (!wcVolumicLayer.isLayerActive()) {
			wcVolumicLayer.activate(true);
		}

		partContext.set(IWWLayer.class, wcVolumicLayer);
		partContext.set(WCVolumicLayer.class, wcVolumicLayer);
		partContext.set(IndexedPlayer.class, wcVolumicLayer.getPlayer());
		OsgiUtils.make(WaterColumn2DParameters.class, ISceneComponentProvider.class, partContext);
	}

	/** Initialize components for diplaying a Texture */
	protected void init4Texture(IEclipseContext partContext) {
		partContext.set(IWWLayer.class, textureLayer);
		partContext.set(IWWTextureLayer.class, textureLayer);
		IndexedPlayer player = textureLayer.perform(new IndexPlayerGetterOperation()).getResult().orElse(null);
		if (player != null) {
			partContext.set(IndexedPlayer.class, player);
		}
		OsgiUtils.make(TextureParameters.class, ISceneComponentProvider.class, partContext);
	}

	protected GLCanvas createCanvas() {
		GLProfile glprofile = GLProfile.getDefault();
		GLCapabilities caps2 = new GLCapabilities(glprofile);
		return new GLCanvas(caps2);
	}

}