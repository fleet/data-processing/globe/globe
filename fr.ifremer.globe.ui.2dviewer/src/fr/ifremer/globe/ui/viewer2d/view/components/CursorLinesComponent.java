package fr.ifremer.globe.ui.viewer2d.view.components;

import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.scene.SceneProperties;
import fr.ifremer.globe.ui.viewer2d.view.utils.StringRenderer;
import fr.ifremer.globe.utils.function.BiDoubleFunction;

public class CursorLinesComponent implements GLComponent {

	/** Scene properties computed when drawing */
	@Inject
	@Optional
	protected SceneProperties sceneProperties;

	/** Show grid or not */
	@Inject
	@Named(ContextNames.DRAW_CURSOR_LINES)
	boolean drawCursorLines;

	protected StringRenderer stringRenderer = new StringRenderer();
	protected BiDoubleFunction<String> coordFormat = (x, y) -> String.format("x = %.2f ; y = %.2f", x, y);

	/**
	 * Dispose
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		dispose();
	}

	/**
	 * Draws horizontal and vertical lines pointing on cursor.
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		double[] cursorSceneCoords = sceneProperties.getCursorSceneCoords();
		if (!sceneProperties.contains(cursorSceneCoords)) {
			return;
		}

		GL2 gl = glad.getGL().getGL2();
		gl.glPushMatrix();
		double x = cursorSceneCoords[0];
		double y = cursorSceneCoords[1];

		if (drawCursorLines && Activator.get2DPreferences().getDrawCursorMetrics()) {
			gl.glColor3d(0, 1, 0);
			// vertical line
			gl.glBegin(GL.GL_LINE_STRIP);
			gl.glVertex3d(cursorSceneCoords[0], sceneProperties.getTop(), 1.0);
			gl.glVertex3d(x, sceneProperties.getBottom(), 1.0);
			gl.glEnd();

			// horizontal line
			gl.glBegin(GL.GL_LINE_STRIP);
			gl.glVertex3d(sceneProperties.getLeft(), y, 1.0);
			gl.glVertex3d(sceneProperties.getRight(), y, 1.0);
			gl.glEnd();
		}

		// Text
		stringRenderer.draw2DStringOnScene(glad, coordFormat.apply(x, y), 10, 10);

		gl.glPopMatrix();
	}

	@Override
	public boolean isVisible() {
		return true;
	}

	/**
	 * @param coordFormat the {@link #coordFormat} to set
	 */
	public void setCoordFormat(BiDoubleFunction<String> coordFormat) {
		this.coordFormat = coordFormat;
	}

	@PreDestroy
	protected void dispose() {
		// free any lambda set as format
		coordFormat = null;
	}
}
