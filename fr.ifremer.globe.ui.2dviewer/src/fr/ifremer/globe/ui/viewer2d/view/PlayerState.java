/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view;

/**
 *
 */
public class PlayerState {

	protected final int index;
	protected final long timeNs;

	/**
	 * Constructor
	 */
	public PlayerState(int index, long timeNs) {
		this.index = index;
		this.timeNs = timeNs;
	}

	/**
	 * @return the {@link #index}
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return the {@link #timeNs}
	 */
	public long getTimeNs() {
		return timeNs;
	}

}
