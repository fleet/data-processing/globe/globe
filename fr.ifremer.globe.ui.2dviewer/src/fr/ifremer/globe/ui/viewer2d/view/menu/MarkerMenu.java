/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.menu;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Contextual menu for editing and deleting markers
 */
public class MarkerMenu extends JPopupMenu {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 4954730020699251760L;

	/**
	 * Constructor
	 */
	public MarkerMenu(Runnable editAction, Runnable deleteAction) {
		JMenuItem editItem = new JMenuItem("Edit");
		editItem.setIcon(new ImageIcon(getClass().getResource("/icons/marker.png")));
		editItem.addActionListener(event -> editAction.run());
		add(editItem);

		JMenuItem deleteItem = new JMenuItem("Delete");
		deleteItem.setIcon(new ImageIcon(getClass().getResource("/icons/delete.png")));
		deleteItem.addActionListener(event -> deleteAction.run());
		add(deleteItem);
	}
}
