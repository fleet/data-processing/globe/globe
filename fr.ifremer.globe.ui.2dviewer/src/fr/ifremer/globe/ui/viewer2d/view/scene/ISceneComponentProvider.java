/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.scene;

import java.util.List;

import fr.ifremer.globe.ogl.GLComponent;

/**
 * Parameters used to initialize the Viewer2D.
 */
public interface ISceneComponentProvider {
	/**
	 * @return the components to draw in the scene
	 */
	List<GLComponent> getGLComponents();
}
