package fr.ifremer.globe.ui.viewer2d.view.components;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import javax.swing.SwingUtilities;

import org.eclipse.core.databinding.observable.IChangeListener;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ogl.util.CoordinatesProjection;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.utils.PositionUtils;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.PlayerState;
import fr.ifremer.globe.ui.viewer2d.view.Viewer2DStatusBar;
import fr.ifremer.globe.ui.viewer2d.view.menu.MarkerMenu;
import fr.ifremer.globe.ui.viewer2d.view.scene.SceneProperties;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.opengl.OpenGLUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.markers.MarkerController;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;
import fr.ifremer.viewer3d.layers.markers.WWWaterColumnMarkerLayer;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.AbstractLayer;

public class MarkersComponent implements GLComponent {

	/** Marker controller: used to add markers **/
	@Inject
	MarkerController markerController;

	/** File layer model **/
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;
	private final Consumer<WWFileLayerStoreEvent> fileLayerStoreModelListener = evt -> updateMarkerListeners();

	/** Scene properties computed when drawing */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected SceneProperties sceneProperties;

	/** Status bar **/
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected Viewer2DStatusBar statusBar;

	/** Current edited layer */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private IWWLayer layer;

	/** Canvas **/
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private GLCanvas canvas;

	/** Player **/
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private PlayerState playerState;

	/** Subject displayed in the scene. */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected ISubjectCharacteristics subjectCharacteristics;

	/** List of GLMarker currently displayed */
	protected int currentPlayerIndex = -1;

	/** List of {@link WWMarkerLayer} & theirs listener **/
	private List<WWWaterColumnMarkerLayer> markerLayers = new ArrayList<>();
	private final IChangeListener markerChangeListener = evt -> updateMarkers();
	private final PropertyChangeListener markerPropertyChangeListener = evt -> updateMarkers();

	/** List of {@link IWCMarker} currently displayed */
	protected List<IWCMarker> markers = List.of();

	private Optional<IWCMarker> optMarkerUnderCursor = Optional.empty();
	private Optional<IWCMarker> selectedMarker = Optional.empty();

	@PostConstruct
	public void init() {
		fileLayerStoreModel.addListener(fileLayerStoreModelListener);
		updateMarkerListeners();
	}

	@PreDestroy
	public void destroy() {
		// Remove all listeners
		updateMarkerListeners(List.of());
		fileLayerStoreModel.removeListener(fileLayerStoreModelListener);
	}

	/**
	 * Sets listeners for each {@link WWMarkerLayer}. Removes obsolete listeners.
	 */
	private void updateMarkerListeners() {
		// get all WWMarkerLayers
		List<WWWaterColumnMarkerLayer> newMarkerLayers = fileLayerStoreModel.getLayers(WWWaterColumnMarkerLayer.class)
				.collect(Collectors.toList());
		updateMarkerListeners(newMarkerLayers);
	}

	/**
	 * Sets listeners for each {@link WWMarkerLayer}. Removes obsolete listeners.
	 */
	private void updateMarkerListeners(List<WWWaterColumnMarkerLayer> newMarkerLayers) {

		// remove listener of old marker layer
		markerLayers.forEach(markerLayer -> {
			markerLayer.getIMarkers().removeChangeListener(markerChangeListener);
			markerLayer.removePropertyChangeListener("Enabled", markerPropertyChangeListener);
			markerLayer.removePropertyChangeListener(WWMarkerLayer.SELECTED_MARKER_PROPERTY,
					markerPropertyChangeListener);
			markerLayer.removePropertyChangeListener(WWMarkerLayer.ENABLED_MARKER_PROPERTY,
					markerPropertyChangeListener);
		});

		// refresh markerLayers local list, and add listener
		markerLayers = newMarkerLayers;
		markerLayers.forEach(markerLayer -> {
			markerLayer.getIMarkers().addChangeListener(markerChangeListener);
			markerLayer.addPropertyChangeListener("Enabled", markerPropertyChangeListener);
			markerLayer.addPropertyChangeListener(WWMarkerLayer.SELECTED_MARKER_PROPERTY, markerPropertyChangeListener);
			markerLayer.addPropertyChangeListener(WWMarkerLayer.ENABLED_MARKER_PROPERTY, markerPropertyChangeListener);
		});
	}

	/**
	 * Dispose OpenGL resources
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		/* not used */
	}

	/**
	 * Draws a metric grid
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		if (playerState != null) {

			// First initialization
			if (currentPlayerIndex != playerState.getIndex()) {
				currentPlayerIndex = playerState.getIndex();
				// Player index change : reload the list of markers
				updateMarkers();
			}

			GL2 gl = glad.getGL().getGL2();

			// clear old marker message
			if (optMarkerUnderCursor.isPresent()) {
				statusBar.clearMessage();
			}

			// update current point
			optMarkerUnderCursor = findNearest(glad);
			dragSelectedPoint();

			if (!markers.isEmpty()) {
				int markerSize = Activator.get2DPreferences().getMarkerSize();
				int selectedMarkerSize = (int) (markerSize * 1.5);
				gl.glEnable(GL2.GL_POINT_SMOOTH);
				gl.glPointSize(selectedMarkerSize);

				GColor pointSelectionColor = fr.ifremer.viewer3d.Activator.getPluginParameters()
						.getPointSelectionColor();
				OpenGLUtils.setAsGLColor(gl, pointSelectionColor.getRed(), pointSelectionColor.getGreen(),
						pointSelectionColor.getBlue());

				// draw circle over mouse or if a marker is selected
				var highlightedMarker = optMarkerUnderCursor.isPresent() ? optMarkerUnderCursor
						: markers.stream().filter(m -> m.isEnabled() && m.isSelected()).findFirst();
				if (highlightedMarker.isPresent()) {
					// canvas.requestFocus(); // Useful to activate the key bindings
					gl.glBegin(GL2.GL_POINTS);
					IWCMarker marker = highlightedMarker.get();
					Vec4 sceneCoords = subjectCharacteristics.fromGeographicToScene(PositionUtils.getPosition(marker));
					gl.glVertex3d(sceneCoords.x, sceneCoords.y, 1.0);
					gl.glEnd();
					String typology = marker.getTypology().isPresent()
							? marker.getTypology().get().getGroup() + " " + marker.getTypology().get().getClazz() + " "
							: "";
					statusBar.diplayMessage("Marker " + marker.getID() + " : " + typology + marker.getComment());
				}

				// draw all points
				gl.glPointSize(markerSize);
				gl.glBegin(GL2.GL_POINTS);
				for (IWCMarker marker : markers) {
					if (marker.isEnabled()) {
						Vec4 markerSceneCoords = subjectCharacteristics
								.fromGeographicToScene(PositionUtils.getPosition(marker));
						if (markerSceneCoords.x < sceneProperties.getRightTicks()
								&& markerSceneCoords.x > sceneProperties.getLeftTicks() //
								&& markerSceneCoords.y < sceneProperties.getTopTicks()
								&& markerSceneCoords.y > sceneProperties.getBottomTicks()) {
							GColor color = marker.getColor();
							gl.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());
							gl.glVertex3d(markerSceneCoords.x, markerSceneCoords.y, 1.0);
						}
					}
				}
				gl.glEnd();
				gl.glDisable(GL2.GL_POINT_SMOOTH);
			}
		}
	}

	@Override
	public boolean isVisible() {
		return true;
	}

	public void dragSelectedPoint() {
		if (selectedMarker.isPresent()) {
			double[] sceneCoords = sceneProperties.getCursorSceneCoords();
			if (subjectCharacteristics == null || !subjectCharacteristics.contains(sceneCoords)) {
				return;
			}
			Position position = subjectCharacteristics.fromSceneToGeographic(sceneCoords[0], sceneCoords[1]);
			PositionUtils.setPosition(selectedMarker.get(), position);
			Viewer3D.refreshRequired();
		}
	}

	public Optional<IWCMarker> getSelectedPoint() {
		return selectedMarker;
	}

	// MOUSE EVENTS MANAGEMENT

	public void mouseLeftClicked() {
		double[] sceneCoords = sceneProperties.getCursorSceneCoords();
		if (!optMarkerUnderCursor.isPresent() && subjectCharacteristics != null
				&& subjectCharacteristics.contains(sceneCoords)) {
			// add new marker
			Position pos = subjectCharacteristics.fromSceneToGeographic(sceneCoords[0], sceneCoords[1]);

			Optional<Instant> optDate = Optional.empty();
			if (playerState.getTimeNs() != 0) {
				optDate = Optional.of(Instant.ofEpochMilli(DateUtils.nanoSecondToMilli(playerState.getTimeNs())));
			} else {
				// Get date from texture data (TODO : ugly code : OpenGL picking should be used)
				IWWTextureData textureData = ((IWWTextureLayer) layer).getTextureData();
				int[] rowCol = WWTextureDataUtils.getRowCol(textureData, pos);
				if (rowCol.length == 2 && rowCol[0] != -1 && rowCol[1] != -1)
					optDate = textureData.getDate(rowCol[0], rowCol[1]);
			}

			markerController.addNewWCMarker(pos, layer.getName(), getCurrentSwathIndex(), optDate);
			updateMarkers();
		}
	}

	public void mousePressed(MouseEvent evt) {
		if (SwingUtilities.isLeftMouseButton(evt) && optMarkerUnderCursor.isPresent()) {
			selectedMarker = Optional.of(optMarkerUnderCursor.get());
		}
	}

	public void mouseReleased() {
		if (selectedMarker.isPresent()) {
			selectedMarker = Optional.empty();

			// refresh geographic view
			Viewer3D.refreshRequired();

			// force the update in marker editor
			WWMarkerLayer<?> activeMarkerLayer = markerController.getActiveMarkerLayer();
			if (activeMarkerLayer != null) {
				activeMarkerLayer.getIMarkers().getRealm()
						.exec(() -> activeMarkerLayer.getIMarkers().forceFireChange());
			}
		}
	}

	/** Creates the list of GLMarkers */
	private void updateMarkers() {
		// get marker to display
		markers = markerLayers.stream()
				// filter enabled layers only
				.filter(AbstractLayer::isEnabled)
				// get markers from WWMarkerLayer
				.flatMap(wwMarkerLayer -> wwMarkerLayer.getIMarkers().asList().stream())
				// filter marker to get ones to display
				.filter(marker -> marker.getPing() == getCurrentSwathIndex()
						&& marker.getWaterColumnLayer().equals(layer.getName()))
				.collect(Collectors.toList());

		// Refresh the view
		SwingUtilities.invokeLater(canvas::repaint);
	}

	/**
	 * Looks at the nearest marker from the cursor.
	 */
	private Optional<IWCMarker> findNearest(GLAutoDrawable glad) {
		double minDist = Double.MAX_VALUE;
		IWCMarker nearest = null;
		double[] markerScreenCoords = new double[3];
		double[] cursorSceneCoords = sceneProperties.getCursorSceneCoords();
		double[] cursorScreenCoords = new double[3];

		for (IWCMarker marker : markers) {
			// Geographic -> scene
			Vec4 markerSceneCoords = subjectCharacteristics.fromGeographicToScene(PositionUtils.getPosition(marker));

			// Scene -> screen
			CoordinatesProjection.fromSceneToScreen(glad, markerSceneCoords.x, markerSceneCoords.y, markerSceneCoords.z,
					markerScreenCoords);
			CoordinatesProjection.fromSceneToScreen(glad, cursorSceneCoords[0], cursorSceneCoords[1], 0,
					cursorScreenCoords);

			double dx = Math.abs(cursorScreenCoords[0] - markerScreenCoords[0]);
			double dy = Math.abs(cursorScreenCoords[1] - markerScreenCoords[1]);
			double dist = Math.sqrt(dx * dx + dy * dy);
			if (dist < minDist) {
				nearest = marker;
				minDist = dist;
			}
		}
		return minDist < Activator.get2DPreferences().getMarkerSize() / 2.0 ? Optional.of(nearest) : Optional.empty();
	}

	/**
	 * @return the {@link #markers}
	 */
	public List<IWCMarker> getMakers() {
		return Collections.unmodifiableList(markers);
	}

	/**
	 * @return current swath index
	 */
	public int getCurrentSwathIndex() {
		int swathIndex = playerState.getIndex();
		if (layer instanceof IWWTextureLayer) {
			Optional<Integer> optSwathIndex = WWTextureDataUtils
					.getSwathIndex(((IWWTextureLayer) layer).getTextureData());
			if (optSwathIndex.isPresent()) {
				swathIndex = optSwathIndex.get();
			}
		}
		return swathIndex;

	}

	/** Delete the current selected marker */
	public void deleteSelectedMarker() {
		optMarkerUnderCursor.ifPresent(marker -> fileLayerStoreModel.getLayers(WWWaterColumnMarkerLayer.class)
				.forEach(wcMarkerlayer -> wcMarkerlayer.removeMarker(marker)));
	}

	/** Delete the current selected marker */
	public void editSelectedMarker() {
		optMarkerUnderCursor.ifPresent(marker -> fileLayerStoreModel.getLayers(WWWaterColumnMarkerLayer.class)
				.forEach(wcMarkerlayer -> wcMarkerlayer.editMarker(marker)));
	}

	/** Generates the contextual menu for the selected marker */
	public MarkerMenu generateMarkerMenu() {
		MarkerMenu result = null;
		if (optMarkerUnderCursor.isPresent()) {
			result = new MarkerMenu(this::editSelectedMarker, this::deleteSelectedMarker);
		}
		return result;
	}

}
