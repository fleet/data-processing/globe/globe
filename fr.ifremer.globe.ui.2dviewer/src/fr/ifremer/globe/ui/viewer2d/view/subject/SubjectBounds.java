package fr.ifremer.globe.ui.viewer2d.view.subject;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class SubjectBounds {

	private double minX;
	private double maxX;
	private double minY;
	private double maxY;
	private double width;
	private double height;
	private double centroidX;
	private double centroidY;

	/**
	 * Creates a new instance of {@code Bounds} class.
	 */
	public SubjectBounds(double minX, double maxX, double minY, double maxY) {
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		width = maxX - minX;
		height = maxY - minY;
		centroidX = minX + width / 2d;
		centroidY = minY + height / 2d;
	}

	/**
	 * The x coordinate of the upper-left corner of this {@code Bounds}.
	 *
	 * @return the x coordinate of the upper-left corner
	 *
	 */
	public final double getMinX() {
		return minX;
	}

	/**
	 * The y coordinate of the upper-left corner of this {@code Bounds}.
	 *
	 * @return the y coordinate of the upper-left corner
	 *
	 */
	public final double getMinY() {
		return minY;
	}

	/**
	 * The width of this {@code Bounds}.
	 *
	 * @return the width
	 *
	 */
	public final double getWidth() {
		return width;
	}

	/**
	 * The height of this {@code Bounds}.
	 *
	 * @return the height
	 *
	 */
	public final double getHeight() {
		return height;
	}

	/**
	 * The x coordinate of the lower-right corner of this {@code Bounds}.
	 *
	 * @return the x coordinate of the lower-right corner
	 * @defaultValue {@code minX + width}
	 */
	public final double getMaxX() {
		return maxX;
	}

	/**
	 * The y coordinate of the lower-right corner of this {@code Bounds}.
	 *
	 * @return the y coordinate of the lower-right corner
	 * @defaultValue {@code minY + height}
	 */
	public final double getMaxY() {
		return maxY;
	}

	/**
	 * The maximum z coordinate of this {@code Bounds}.
	 *
	 * @return the maximum z coordinate
	 * @defaultValue {@code minZ + depth}
	 */
	public final double getMaxZ() {
		return maxZ;
	}

	private double maxZ;

	/**
	 * The central x coordinate of this {@code Bounds}.
	 *
	 * @return the central x coordinate
	 * @implSpec This call is equivalent to {@code (getMaxX() + getMinX())/2.0}.
	 * @since 11
	 */
	public final double getCenterX() {
		return (getMaxX() + getMinX()) * 0.5;
	}

	/**
	 * The central y coordinate of this {@code Bounds}.
	 *
	 * @return the central y coordinate
	 * @implSpec This call is equivalent to {@code (getMaxY() + getMinY())/2.0}.
	 * @since 11
	 */
	public final double getCenterY() {
		return (getMaxY() + getMinY()) * 0.5;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/**
	 * @return the {@link #centroidX}
	 */
	public double getCentroidX() {
		return centroidX;
	}

	/**
	 * @param centroidX the {@link #centroidX} to set
	 */
	public void setCentroidX(double centroidX) {
		this.centroidX = centroidX;
	}

	/**
	 * @return the {@link #centroidY}
	 */
	public double getCentroidY() {
		return centroidY;
	}

	/**
	 * @param centroidY the {@link #centroidY} to set
	 */
	public void setCentroidY(double centroidY) {
		this.centroidY = centroidY;
	}

}
