package fr.ifremer.globe.ui.viewer2d.view.texture;

import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.subject.SubjectBounds;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Class use to defines the water column bounds.
 */
public class WWVerticalTextureCharacteristics implements ISubjectCharacteristics {

	private SubjectBounds bounds;
	private TextureXAxis textureXAxis;

	/**
	 * Constructor
	 */
	public WWVerticalTextureCharacteristics(double bottomElevation, double topElevation, TextureXAxis textureXAxis) {
		this.textureXAxis = textureXAxis;
		bounds = new SubjectBounds(textureXAxis.getMin(), textureXAxis.getMax(), topElevation, bottomElevation);
	}

	/**
	 * @return the {@link #bounds}
	 */
	@Override
	public SubjectBounds getBounds() {
		return bounds;
	}

	/**
	 * @return the {@link #textureXAxis}
	 */
	public TextureXAxis getxInterpolatedValues() {
		return textureXAxis;
	}

	@Override
	public Position fromSceneToGeographic(double distance, double immersion) {
		LatLon latLon = textureXAxis.unproject(distance);
		return new Position(latLon, immersion);
	}

	/** {@inheritDoc} */
	@Override
	public Vec4 fromGeographicToScene(Position position) {
		return new Vec4(textureXAxis.project(position), position.elevation);
	}

}
