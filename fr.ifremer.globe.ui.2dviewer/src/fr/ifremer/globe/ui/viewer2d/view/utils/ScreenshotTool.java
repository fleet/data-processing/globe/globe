package fr.ifremer.globe.ui.viewer2d.view.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import jakarta.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.preferences.Viewer2DPreferences;
import fr.ifremer.globe.ui.viewer2d.view.Viewer2DStatusBar;
import fr.ifremer.globe.ui.viewer2d.view.components.MarkersComponent;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;

/**
 * Tool to make screenshot.
 */
public class ScreenshotTool {

	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenshotTool.class);

	@Inject
	private IFileInfo editedFile;
	@Inject
	private Viewer2DStatusBar statusBar;
	@Inject
	private Viewer2DScene scene;
	/** File layer model **/
	@Inject
	private MarkersComponent markersComponent;

	private Optional<File> takeScreenshotFile = Optional.empty();

	/**
	 * Requests a screen shot (defines a file which will be used by process method).
	 */
	public void doScreenShot() {
		// If the screenshot folder setting is not found, ask the user for a folder.
		Viewer2DPreferences pref = Activator.get2DPreferences();
		File folder = pref.getScreenshotFolder().toFile();
		folder.mkdirs();
		if (!folder.isDirectory()) {
			LOGGER.warn("Unable to create screenshot directory: {} ", folder.getAbsolutePath());
			DirectoryDialog dialog = new DirectoryDialog(Display.getCurrent().getActiveShell());
			dialog.setFilterPath(pref.getScreenshotFolder().toString());
			String filename = dialog.open();
			if (filename == null || !new File(filename).isDirectory()) {
				return;
			}
			pref.changeScreenshotFolder(filename);
			pref.save();
		}

		// define file name
		int pingIndex = markersComponent.getCurrentSwathIndex();
		StringBuilder fileNameBuilder = new StringBuilder(editedFile.getBaseName());
		fileNameBuilder.append("_" + pingIndex);
		for (String typologyName : getTypologyNames(markersComponent.getMakers(), pingIndex)) {
			fileNameBuilder.append("_" + typologyName);
		}
		fileNameBuilder.append("_");
		fileNameBuilder.append(getLastScreenshotNumber(folder, fileNameBuilder.toString()) + 1 + ".png");

		takeScreenshotFile = Optional.of(new File(folder, fileNameBuilder.toString()));
		scene.refresh(); // request to refresh to call the process method
	}

	/**
	 * Processes the screenshot if asked. (during draw method: requires glad)
	 */
	public void process(GLAutoDrawable glad) {
		if (takeScreenshotFile.isPresent()) {
			File f = takeScreenshotFile.get();
			takeScreenshotFile = Optional.empty(); // only one screenshot
			AWTGLReadBufferUtil glReadBufferUtil = new AWTGLReadBufferUtil(glad.getGLProfile(), false);
			BufferedImage image = glReadBufferUtil.readPixelsToBufferedImage(glad.getGL(), true);
			try {
				ImageIO.write(image, "png", f);
				LOGGER.info("Screenshot recorded in {} ", f.getAbsolutePath());
				statusBar.diplayMessage(String.format("Screenshot recorded in %s ", f.getAbsolutePath()), 2000);
			} catch (Exception e) {
				LOGGER.error("Screenshot failed: " + e.getMessage(), e);
			}
		}
	}

	/**
	 * @return the last screenshot number from a folder.
	 */
	public static int getLastScreenshotNumber(File folder, String prefix) {
		File[] screenshots = folder.listFiles((f, n) -> n.toLowerCase().endsWith(".png"));
		if (screenshots == null) {
			return 0;
		}

		int result = 0;
		int index = prefix.length();
		for (File f : screenshots) {
			String tmpname = FilenameUtils.removeExtension(f.getName());
			if (tmpname.startsWith(prefix)) {
				try {
					int tmpCount = Integer.parseInt(tmpname.substring(index));
					result = Math.max(tmpCount, result);
				} catch (NumberFormatException ex) {
					// nothing to do
				}
			}
		}
		return result;
	}

	/**
	 * @return a {@link Set} of typology names computed from markers at the specified index.
	 */
	public static Set<String> getTypologyNames(List<IWCMarker> markers, int pingIndex) {
		return markers.stream() //
				.filter(marker -> marker.getPing() == pingIndex && marker.getTypology().isPresent())//
				.map(marker -> marker.getTypology().get())//
				.map(typology -> typology.getClazz().isEmpty() ? typology.getGroup() : typology.getClazz()) //
				.filter(string -> !string.isEmpty()) //
				.collect(Collectors.toSet());
	}

}
