package fr.ifremer.globe.ui.viewer2d.view.texture;

import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.subject.SubjectBounds;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Class use to defines the water column bounds.
 */
public class WWTextureCharacteristics implements ISubjectCharacteristics {

	private SubjectBounds bounds;
	private SubjectBounds geoBounds;
	private IWWTextureData data;

	/**
	 * Constructor
	 */
	public WWTextureCharacteristics(double minX, double maxX, double minY, double maxY) {
		bounds = new SubjectBounds(minX, maxX, minY, maxY);
	}

	public WWTextureCharacteristics(IWWTextureData data) {
		this.data = data;
		var bottomLeft = data.getCoordinates().get(0);
		var topRight = data.getCoordinates().get(data.getCoordinates().size() - 1);
		geoBounds = new SubjectBounds(bottomLeft.getPosition().longitude.degrees,
				topRight.getPosition().longitude.degrees, bottomLeft.getPosition().latitude.degrees,
				topRight.getPosition().latitude.degrees);
		bounds = new SubjectBounds(bottomLeft.getS() * data.getWidth(), topRight.getS() * data.getWidth(),
				bottomLeft.getT() * data.getHeight(), topRight.getT() * data.getHeight());
	}

	/**
	 * @return the {@link #bounds}
	 */
	@Override
	public SubjectBounds getBounds() {
		return bounds;
	}

	@Override
	public Position fromSceneToGeographic(double x, double y) {

		double lon = geoBounds.getMinX() + geoBounds.getWidth() * x / bounds.getWidth();
		double lat = geoBounds.getMinY() + geoBounds.getHeight() * y / bounds.getHeight();

		// find elevation in grid
		int colNext = data.getCoordGridWidth() - 1;
		int rowNext = data.getCoordGridHeight() - 1;

		for (int col = 0; col < data.getCoordGridWidth(); col++) {
			float xCoord = data.getCoordinates().get(col * data.getCoordGridHeight()).getS() * data.getWidth();
			if (xCoord > x) {
				colNext = col;
				break;
			}
		}
		int colPrevious = colNext - 1;
		for (int row = 0; row < data.getCoordGridHeight(); row++) {
			double yCoord = data.getCoordinates().get(colPrevious * data.getCoordGridHeight() + row).getT() * data.getHeight();
			if (yCoord > y) {
				rowNext = row;
				break;
			}
		}
		int rowPrevious = rowNext - 1;

		var bottomLeft = data.getCoordinates().get(colPrevious * data.getCoordGridHeight() + rowPrevious)
				.getPosition();
		var bottomRight = data.getCoordinates().get(colNext * data.getCoordGridHeight() + rowPrevious).getPosition();
		var topLeft = data.getCoordinates().get(colPrevious * data.getCoordGridHeight() + rowNext).getPosition();
		var topRight = data.getCoordinates().get(colNext * data.getCoordGridHeight() + rowNext).getPosition();

		double colCoef = (lon - bottomLeft.longitude.degrees)
				/ (bottomRight.longitude.degrees - bottomLeft.longitude.degrees);
		double rowCoef = (lat - bottomLeft.latitude.degrees) / (topLeft.latitude.degrees - bottomLeft.latitude.degrees);
		var bottom = Position.interpolate(colCoef, bottomLeft, bottomRight);
		var top = Position.interpolate(colCoef, topLeft, topRight);
		var pos = Position.interpolate(rowCoef, bottom, top);
		// add vertical offset
		return new Position(pos.latitude, pos.longitude, pos.elevation + data.getVerticalOffset());
	}

	/** {@inheritDoc} */
	@Override
	public Vec4 fromGeographicToScene(Position position) {
		var x = data.getWidth() * (position.longitude.degrees - geoBounds.getMinX())
				/ (geoBounds.getMaxX() - geoBounds.getMinX());
		var y = data.getHeight() * (position.latitude.degrees - geoBounds.getMinY())
				/ (geoBounds.getMaxY() - geoBounds.getMinY());
		return new Vec4(x, y);
	}

}
