/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.utils;

import java.awt.Font;

import com.jogamp.opengl.GLAutoDrawable;

import com.jogamp.opengl.util.awt.TextRenderer;

import fr.ifremer.globe.ogl.util.CoordinatesProjection;

/**
 * Utility instance to draw a Sting on the scene
 */
public class StringRenderer {

	private TextRenderer textRenderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 12));

	/**
	 * Displays 2D string with scene coordinate (with zoom and translation correction and screen coordinate offset
	 * (dx,dy)).
	 */
	public void draw2DStringOnScene(GLAutoDrawable glad, String text, double x, double y, int dx, int dy) {
		// define text coordinates
		double[] textScreenCoords = new double[3];
		CoordinatesProjection.fromSceneToScreen(glad, x, y, 0, textScreenCoords);
		textScreenCoords[0] += dx;
		// projection in text renderer coordinate system
		textScreenCoords[1] = glad.getSurfaceHeight() - textScreenCoords[1] + dy;

		textRenderer.beginRendering(glad.getSurfaceWidth(), glad.getSurfaceHeight());
		textRenderer.draw(text, (int) textScreenCoords[0], (int) textScreenCoords[1]);
		textRenderer.endRendering();
	}

	public void draw2DStringOnScene(GLAutoDrawable glad, String text, int x, int y) {
		textRenderer.beginRendering(glad.getSurfaceWidth(), glad.getSurfaceHeight());
		textRenderer.draw(text, x, y);
		textRenderer.endRendering();

	}

	/**
	 * @return the {@link #textRenderer}
	 */
	public TextRenderer getTextRenderer() {
		return textRenderer;
	}

}
