/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.texture;

import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.subject.SubjectBounds;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

/**
 * ISubjectCharacteristics describing a IBasicTextureData
 */
public class BasicTextureCharacteristics implements ISubjectCharacteristics {

	private final SubjectBounds bounds;

	/**
	 * Constructor
	 */
	public BasicTextureCharacteristics(IBasicTextureData texture) {
		bounds = new SubjectBounds(0, texture.getWidth(), 0, texture.getHeight());
	}

	/** {@inheritDoc} */
	@Override
	public SubjectBounds getBounds() {
		return bounds;
	}

	/** {@inheritDoc} */
	@Override
	public Position fromSceneToGeographic(double distance, double immersion) {
		return Position.ZERO;
	}

	/** {@inheritDoc} */
	@Override
	public Vec4 fromGeographicToScene(Position position) {
		return Vec4.ZERO;
	}

}
