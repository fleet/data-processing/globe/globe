package fr.ifremer.globe.ui.viewer2d.view.components;

import jakarta.inject.Inject;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLAutoDrawable;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.viewer2d.view.scene.SceneProperties;

/** GLComponent to draw the scene frame */
public class FrameComponent implements GLComponent {

	/** Scene properties computed when drawing */
	@Inject
	@Optional
	protected SceneProperties sceneProperties;

	/**
	 * Dispose
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		/* not used */
	}

	/**
	 * Draws a container, grids and labels.
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		GL2 gl = glad.getGL().getGL2();

		// Change color
		float[] previousColor = new float[4];
		gl.glGetFloatv(GL2ES1.GL_CURRENT_COLOR, previousColor, 0);

		// Clean ticks space
		gl.glColor3f(0f, 0f, 0f);
		gl.glRectd(sceneProperties.getLeftScene(), sceneProperties.getTopScene(), sceneProperties.getRight(),
				sceneProperties.getTop());
		gl.glRectd(sceneProperties.getRightScene(), sceneProperties.getTopScene(), sceneProperties.getRight(),
				sceneProperties.getBottom());
		gl.glRectd(sceneProperties.getRightScene(), sceneProperties.getBottomScene(), sceneProperties.getLeft(),
				sceneProperties.getBottom());
		gl.glRectd(sceneProperties.getLeftScene(), sceneProperties.getBottomScene(), sceneProperties.getLeft(),
				sceneProperties.getTop());

		gl.glColor3f(255f, 255f, 255f);
		gl.glBegin(GL2.GL_LINE_STRIP);
		gl.glVertex3d(sceneProperties.getLeft(), sceneProperties.getTop(), 1.0);
		gl.glVertex3d(sceneProperties.getRight(), sceneProperties.getTop(), 1.0);
		gl.glVertex3d(sceneProperties.getRight(), sceneProperties.getBottom(), 1.0);
		gl.glVertex3d(sceneProperties.getLeft(), sceneProperties.getBottom(), 1.0);
		gl.glVertex3d(sceneProperties.getLeft(), sceneProperties.getTop(), 1.0);
		gl.glEnd();

		// Restore color
		gl.glColor3fv(previousColor, 0);
	}

	@Override
	public boolean isVisible() {
		return true;
	}

}
