package fr.ifremer.globe.ui.viewer2d.view.wc;

import java.nio.FloatBuffer;

import jakarta.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.PlayerState;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import gov.nasa.worldwind.util.OGLStackHandler;

/**
 * This component displays Z detection points.
 */
public class ZDetectionComponent implements GLComponent {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZDetectionComponent.class);

	/** Player **/
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private PlayerState playerState;

	/** Data sources */
	private final WCVolumicLayer wcVolumicLayer;

	/** Current swath selected in the player */
	private int swathIndex = -1;
	/** Coordinates of points to display */
	private FloatBuffer pointBuffer = null;

	/** Component visibility */
	private boolean visible = Activator.get2DPreferences().getDrawZDetection();
	/** Rendering size */
	private int pointSize = Activator.get2DPreferences().getZDetectionSize();
	/** Rendering color */
	private GColor color = Activator.get2DPreferences().getZDetectionColor();

	/**
	 * Constructor
	 */
	public ZDetectionComponent(WCVolumicLayer wcVolumicLayer) throws GIOException {
		this.wcVolumicLayer = wcVolumicLayer;
	}

	/**
	 * Dispose
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		pointBuffer = null;
	}

	/**
	 * Draws the points.
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		if (!isVisible())
			return;

		// First initialization
		if (swathIndex != playerState.getIndex()) {
			swathIndex = playerState.getIndex();
			updateModel();
		}
		if (pointBuffer != null && pointBuffer.capacity() > 0)
			doDraw(glad);

	}

	private void doDraw(GLAutoDrawable glad) {
		GL2 gl = glad.getGL().getGL2();

		// Save opengl states
		OGLStackHandler ogsh = new OGLStackHandler();
		ogsh.pushAttrib(gl, GL2.GL_ENABLE_BIT); // GL_POINT_SMOOTH, GL_BLEND
		float[] colorToRestore = new float[4];
		gl.glGetFloatv(GL2ES1.GL_CURRENT_COLOR, colorToRestore, 0);
		float[] pointSizeToRestore = new float[1];
		gl.glGetFloatv(GL.GL_POINT_SIZE, pointSizeToRestore, 0);

		try {
			gl.glEnable(GL2ES1.GL_POINT_SMOOTH);
			gl.glEnable(GL.GL_BLEND);
			gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
			gl.glPointSize(pointSize);
			gl.glColor3d(color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue());
			gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
			gl.glVertexPointer(3, GL.GL_FLOAT, 0, pointBuffer);
			gl.glDrawArrays(GL.GL_POINTS, 0, pointBuffer.capacity() / 3);
			gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		} finally {
			gl.glPointSize(pointSizeToRestore[0]);
			gl.glColor3fv(colorToRestore, 0);
			ogsh.pop(gl);
		}
	}

	/** Loading model, points to display */
	private void updateModel() {
		pointBuffer = null;

		SounderDataContainer sounderDataContainer = wcVolumicLayer.getContainer();
		if (sounderDataContainer == null)
			return;

		try {
			var csLayers = sounderDataContainer.getCsLayers(CoordinateSystem.SCS);
			var verticalOffset = sounderDataContainer.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
			var statusLayer = sounderDataContainer.getStatusLayer();

			DoubleLayer2D acrossLayer = csLayers.getProjectedY();
			DoubleLayer2D depthLayer = csLayers.getProjectedZ();
			int beamCount = depthLayer.getDimensions().length >= 2 ? (int) depthLayer.getDimensions()[1] : 0;
			float[] points = new float[beamCount * 3];
			int pointCount = 0;
			for (int beamIndex = 0; beamIndex < beamCount; beamIndex++) {
				if (statusLayer.isValid(swathIndex, beamIndex)) {
					double across = acrossLayer.get(swathIndex, beamIndex);
					double immersion = - depthLayer.get(swathIndex, beamIndex) + verticalOffset.get(swathIndex);
					if (Double.isFinite(immersion)) {
						points[3 * pointCount] = (float) across;
						points[3 * pointCount + 1] = (float) immersion;
						points[3 * pointCount + 2] = 1f;
						pointCount++;
					}
				}
			}
			pointBuffer = Buffers.newDirectFloatBuffer(points, 0, pointCount * 3);
			LOGGER.debug("ZDetection loaded. Swath index : {}, Points count : {}", swathIndex, pointCount);
		} catch (GIOException e) {
			LOGGER.warn("ZDetection loading error {}", e.getMessage());
		}
	}

	/**
	 * @return the {@link #visible}
	 */
	@Override
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the {@link #visible} to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the {@link #pointSize}
	 */
	public int getPointSize() {
		return pointSize;
	}

	/**
	 * @param pointSize the {@link #pointSize} to set
	 */
	public void setPointSize(int pointSize) {
		this.pointSize = pointSize;
	}

	/**
	 * @return the {@link #color}
	 */
	public GColor getColor() {
		return color;
	}

	/**
	 * @param color the {@link #color} to set
	 */
	public void setColor(GColor color) {
		this.color = color;
	}

}
