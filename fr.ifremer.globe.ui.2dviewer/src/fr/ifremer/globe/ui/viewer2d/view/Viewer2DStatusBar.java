package fr.ifremer.globe.ui.viewer2d.view;

import java.awt.BorderLayout;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class Viewer2DStatusBar extends JPanel {

	private static final long serialVersionUID = -5551525881288120760L;
	private final JLabel messageText = new JLabel(" ");
	private final JLabel rightDisplay = new JLabel("");
	private final JLabel leftDisplay = new JLabel("");

	/**
	 * Constructor
	 */
	Viewer2DStatusBar() {
		leftDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		rightDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		messageText.setHorizontalAlignment(SwingConstants.CENTER);
		this.setLayout(new BorderLayout());
		this.add(leftDisplay, BorderLayout.WEST);
		this.add(rightDisplay, BorderLayout.CENTER);
		this.add(messageText, BorderLayout.EAST);
	}

	/**
	 * Bounds change. Update the status bar with the current ping
	 */
	public void displayInfo(String leftInfo, String rightInfo) {
		SwingUtilities.invokeLater(() -> {
			leftDisplay.setText(leftInfo);
			rightDisplay.setText(rightInfo);
		});
	}

	/**
	 * Displays a message during the specified delay (ms).
	 */
	public void diplayMessage(String msg, int delay) {
		messageText.setText(msg);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				messageText.setText(" ");
			}
		}, delay);
	}
	
	/**
	 * Displays a message on the right side of status bar.
	 */
	public void diplayMessage(String msg) {
		SwingUtilities.invokeLater(() -> messageText.setText(msg));
	}
	
	/**
	 * Clear the message on the right side of status bar.
	 */
	public void clearMessage() {
		SwingUtilities.invokeLater(() -> messageText.setText(" "));
	}

}
