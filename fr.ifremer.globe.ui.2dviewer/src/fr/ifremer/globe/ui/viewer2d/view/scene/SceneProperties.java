/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.scene;

import com.jogamp.opengl.GLAutoDrawable;

import fr.ifremer.globe.ogl.util.CoordinatesProjection;

/**
 * Simple Pojo holding all scene properties
 */
public class SceneProperties {

	/** Scene coordinates */
	protected double topScene = 0d;
	protected double bottomScene = 0d;
	protected double leftScene = 0d;
	protected double rightScene = 0d;
	/** Space coordinates to draw the subject */
	protected double top = 0d;
	protected double bottom = 0d;
	protected double left = 0d;
	protected double right = 0d;
	/** Space coordinates to draw the ticks */
	protected double topTicks = 0d;
	protected double bottomTicks = 0d;
	protected double leftTicks = 0d;
	protected double rightTicks = 0d;

	/** Mouse position */
	protected int mouseX;
	protected int mouseY;
	protected int mouseClickX;
	protected int mouseClickY;
	private double[] cursorSceneCoords = new double[4];
	private double[] clickedSceneCoords = new double[4];

	/** Constants */
	protected int topPadding = 30;
	protected int leftPadding = 80;
	protected int bottomPadding = 50;
	protected int rightPadding = 30;
	protected int tickSize = 10;

	/** Update all properties */
	public void update(GLAutoDrawable glad) {

		/* @formatter:off
		 *                              topScene
					  	a_____________________________________
						|                                     |
						|       c___________________________  |
						|       | |                         | |
						|  200m |-e                         | |
						|       |                           | |
						|    0m |                           | |
			leftScene   |       |                           | |  rightScene
						| -200m |                           | |
						|       |                         f-| |
						|       |_________________________|_d |
						|          -200m     0m    200m       |
						|                                     |
						|_____________________________________b

						             bottomScene

		 * @formatter:on
		 */

		double[] a = getSceneCoordinates(glad, new int[] { 0, 0 });
		double[] b = getSceneCoordinates(glad, new int[] { glad.getSurfaceWidth(), glad.getSurfaceHeight() });
		double[] c = getSceneCoordinates(glad, new int[] { leftPadding, topPadding });
		double[] d = getSceneCoordinates(glad,
				new int[] { glad.getSurfaceWidth() - rightPadding, glad.getSurfaceHeight() - bottomPadding });
		double[] e = getSceneCoordinates(glad, new int[] { leftPadding + tickSize, topPadding + tickSize });
		double[] f = getSceneCoordinates(glad,
				new int[] { glad.getSurfaceWidth() - rightPadding - tickSize, glad.getSurfaceHeight() - bottomPadding - tickSize });

		topScene = a[1];
		bottomScene = b[1];
		leftScene = a[0];
		rightScene = b[0];
		top = c[1];
		bottom = d[1];
		left = c[0];
		right = d[0];
		topTicks = e[1];
		bottomTicks = f[1];
		leftTicks = e[0];
		rightTicks = f[0];

		// Compute cursor scene coordinates
		CoordinatesProjection.fromScreenToScene(glad, mouseX, mouseY, cursorSceneCoords);
		CoordinatesProjection.fromScreenToScene(glad, mouseClickX, mouseClickY, clickedSceneCoords);
	}

	protected double[] getSceneCoordinates(GLAutoDrawable glad, int[] screenCoordinates) {
		double[] result = new double[3];
		CoordinatesProjection.fromScreenToScene(glad, screenCoordinates[0], screenCoordinates[1], result);
		return result;
	}

	public boolean contains(double[] point) {
		return !Double.isNaN(getTop()) && getLeft() < point[0] && point[0] < getRight() && getBottom() < point[1]
				&& point[1] < getTop();
	}

	public void setMousePosition(int x, int y) {
		mouseX = x;
		mouseY = y;
	}

	public void setMouseClickPosition(int x, int y) {
		setMousePosition(x, y);
		mouseClickX = x;
		mouseClickY = y;
	}

	/**
	 * @return the {@link #topScene}
	 */
	public double getTopScene() {
		return topScene;
	}

	/**
	 * @return the {@link #bottomScene}
	 */
	public double getBottomScene() {
		return bottomScene;
	}

	/**
	 * @return the {@link #leftScene}
	 */
	public double getLeftScene() {
		return leftScene;
	}

	/**
	 * @return the {@link #rightScene}
	 */
	public double getRightScene() {
		return rightScene;
	}

	/**
	 * @return the {@link #top}
	 */
	public double getTop() {
		return top;
	}

	/**
	 * @return the {@link #bottom}
	 */
	public double getBottom() {
		return bottom;
	}

	/**
	 * @return the {@link #left}
	 */
	public double getLeft() {
		return left;
	}

	/**
	 * @return the {@link #right}
	 */
	public double getRight() {
		return right;
	}

	/**
	 * @return the {@link #topTicks}
	 */
	public double getTopTicks() {
		return topTicks;
	}

	/**
	 * @return the {@link #bottomTicks}
	 */
	public double getBottomTicks() {
		return bottomTicks;
	}

	/**
	 * @return the {@link #leftTicks}
	 */
	public double getLeftTicks() {
		return leftTicks;
	}

	/**
	 * @return the {@link #rightTicks}
	 */
	public double getRightTicks() {
		return rightTicks;
	}

	/**
	 * @return the {@link #topPadding}
	 */
	public int getTopPadding() {
		return topPadding;
	}

	/**
	 * @return the {@link #leftPadding}
	 */
	public int getLeftPadding() {
		return leftPadding;
	}

	/**
	 * @return the {@link #bottomPadding}
	 */
	public int getBottomPadding() {
		return bottomPadding;
	}

	/**
	 * @return the {@link #rightPadding}
	 */
	public int getRightPadding() {
		return rightPadding;
	}

	/**
	 * @return the {@link #tickSize}
	 */
	public int getTickSize() {
		return tickSize;
	}

	/**
	 * @param topPadding the {@link #topPadding} to set
	 */
	public void setTopPadding(int topPadding) {
		this.topPadding = topPadding;
	}

	/**
	 * @param leftPadding the {@link #leftPadding} to set
	 */
	public void setLeftPadding(int leftPadding) {
		this.leftPadding = leftPadding;
	}

	/**
	 * @param bottomPadding the {@link #bottomPadding} to set
	 */
	public void setBottomPadding(int bottomPadding) {
		this.bottomPadding = bottomPadding;
	}

	/**
	 * @param rightPadding the {@link #rightPadding} to set
	 */
	public void setRightPadding(int rightPadding) {
		this.rightPadding = rightPadding;
	}

	/**
	 * @param tickSize the {@link #tickSize} to set
	 */
	public void setTickSize(int tickSize) {
		this.tickSize = tickSize;
	}

	/**
	 * @return the {@link #mouseX}
	 */
	public int getMouseX() {
		return mouseX;
	}

	/**
	 * @return the {@link #mouseY}
	 */
	public int getMouseY() {
		return mouseY;
	}

	/**
	 * @return the {@link #mouseClickX}
	 */
	public int getMouseClickX() {
		return mouseClickX;
	}

	/**
	 * @return the {@link #mouseClickY}
	 */
	public int getMouseClickY() {
		return mouseClickY;
	}

	/**
	 * @return the {@link #cursorSceneCoords}
	 */
	public double[] getCursorSceneCoords() {
		return cursorSceneCoords;
	}

	/**
	 * @return the {@link #clickedSceneCoords}
	 */
	public double[] getClickedSceneCoords() {
		return clickedSceneCoords;
	}

}
