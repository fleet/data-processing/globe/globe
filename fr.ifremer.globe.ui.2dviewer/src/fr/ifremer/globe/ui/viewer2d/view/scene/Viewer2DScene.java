package fr.ifremer.globe.ui.viewer2d.view.scene;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import javax.swing.SwingUtilities;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ogl.util.GLUtils;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.components.MarkersComponent;
import fr.ifremer.globe.ui.viewer2d.view.menu.MarkerMenu;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.subject.SubjectBounds;
import fr.ifremer.globe.ui.viewer2d.view.utils.ScreenshotTool;
import fr.ifremer.viewer3d.util.SSVCursor;
import gov.nasa.worldwind.geom.Vec4;

public class Viewer2DScene implements GLEventListener, MouseListener, MouseMotionListener, MouseWheelListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(Viewer2DScene.class);

	private static final double DEFAULT_ZOOM = 1;

	@Inject
	private GLCanvas canvas;
	@Inject
	@Optional
	private ScreenshotTool screenshotTool;
	@Inject
	@Optional
	private MarkersComponent markersComponent;
	/** Scene properties computed when drawing */
	@Inject
	protected SceneProperties sceneProperties;

	/** Subject displayed in the scene. Not inject, use the setter */
	protected SubjectBounds subjectBounds;

	private boolean equalizeAxis = Activator.get2DPreferences().getEqualizeAxis();
	private double zoom = DEFAULT_ZOOM;
	private double savedxtranslation = 0.0;
	private double savedytranslation = 0.0;
	private boolean isDraggingScene;

	private List<GLComponent> components = new ArrayList<>();
	private boolean allowNewPoint;

	/** Width of the scene in meters */
	private double sceneWidth;
	/** Height of the scene in meters */
	private double sceneHeight;
	/** Cartesian coordinates of the center of the scene in meters */
	private Vec4 sceneCenter = new Vec4(0d, 0d);

	/** Horizontal translation apply to the origin */
	private double xTranslation = 0.0;
	/** Vertical translation apply to the origin */
	private double yTranslation = 0.0;
	
	/** parameters used to redirect true float values to unsigned int on GL renderbuffer
	 * 	redirectedValue = floatValue/redirectResolution - redirectMinValue
	 * */ 
	private float redirectMinValue = -1000.f;
	private float redirectResolution = 0.01f;
	private float redirectedValue = Float.NaN;

	/**
	 * Constructor
	 */
	@PostConstruct
	public void postConstruct(ISceneComponentProvider componentProvider, IEclipseContext context) {
		canvas.addGLEventListener(this);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addMouseWheelListener(this);

		// add components to the scene
		components.addAll(componentProvider.getGLComponents());
	}

	public void refresh() {
		SwingUtilities.invokeLater(() -> {
			try {
				canvas.repaint();
			} catch (RuntimeException t) {
				LOGGER.error("Error while refreshing 2D scene : " + t.getMessage(), t);
			}
		});
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// dispose components
		components.forEach(c -> c.dispose(drawable));
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		// not used
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		// Define "clear" color.
		gl.glClearColor(0f, 0f, 0f, 0f);

		// We want a nice perspective.
		gl.glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
	}

	@Override
	public void display(GLAutoDrawable glad) {
		if (subjectBounds == null) {
			return;
		}
		GL2 gl = glad.getGL().getGL2();

		int w = glad.getSurfaceWidth();
		int h = glad.getSurfaceHeight();
		gl.glViewport(0, 0, w, h);

		// clear the color and depth buffer
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// reset projection matrix
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();

		// reset model view matrix
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();

		keepAspectRatio(glad);

		// update scene properties
		sceneProperties.update(glad);

		handleSceneDrag();
		GLUtils.checkGLError(gl);
		
		// redirect components
		redirect(glad);
		
		// draw components
		components.forEach(c -> c.draw(glad));
		

		if (screenshotTool != null)
			screenshotTool.process(glad);
	}

	private void redirect(GLAutoDrawable glad) {
		// redirect components
		components.forEach(c -> c.redirect(glad, redirectResolution, redirectMinValue));
		// get position
		int h = glad.getSurfaceHeight();
		double yInGLCoords = h - sceneProperties.getMouseY() - 1.0;
		ByteBuffer valuesBuffer = Buffers.newDirectByteBuffer(4);
		GL2 gl = glad.getGL().getGL2();
		
		// Read only RGB (not alpha) to retrieve encoded value (from shader)
		gl.glReadPixels(sceneProperties.getMouseX(), (int) yInGLCoords, 1, 1, GL2.GL_RGB, GL.GL_UNSIGNED_BYTE,
				valuesBuffer);

		// decode redirected value
		int rawValue = valuesBuffer.getInt(0);
		redirectedValue = rawValue == 0 ? Float.NaN : rawValue*redirectResolution + redirectMinValue;

		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
	}
	
	/**
	 * Applies transformations to correctly display the scene (zoom, translation,
	 * clipping...)
	 */
	private void keepAspectRatio(GLAutoDrawable glad) {
		GL2 gl = glad.getGL().getGL2();

		double halfWidth = sceneWidth / 2;
		double halfHeight = sceneHeight / 2;
		if (isEqualizeAxis() || sceneHeight == 0.0 || sceneWidth == 0.0) {
			double pixelSizeInMetre = Math.max(sceneHeight / canvas.getHeight(), sceneWidth / canvas.getWidth());
			halfWidth = canvas.getWidth() * pixelSizeInMetre / 2;
			halfHeight = canvas.getHeight() * pixelSizeInMetre / 2;
		}

		// What is the size in meter of the left/right padding difference ?
		double xAxisOffsetRatio = 0.5 * (sceneProperties.leftPadding - sceneProperties.rightPadding)
				/ canvas.getWidth();
		double xAxisOffsetInMeter = sceneWidth * xAxisOffsetRatio;

		gl.glOrtho(-halfWidth * zoom, halfWidth * zoom, -halfHeight * zoom, halfHeight * zoom, -100, 100);
		gl.glTranslated(xTranslation - sceneCenter.x + xAxisOffsetInMeter, yTranslation - sceneCenter.y, 0.0);		
	}

	private void handleSceneDrag() {
		if (isDraggingScene) {
			xTranslation = savedxtranslation + sceneProperties.getCursorSceneCoords()[0]
					- sceneProperties.getClickedSceneCoords()[0];
			yTranslation = savedytranslation + sceneProperties.getCursorSceneCoords()[1]
					- sceneProperties.getClickedSceneCoords()[1];
		}
	}

	public void setAllowNewPoint(boolean b) {
		allowNewPoint = b;
		canvas.setCursor(b ? SSVCursor.getPredefinedCursor(SSVCursor.CURSOR_POINT) : null);
	}

	public void resetView() {
		zoom = DEFAULT_ZOOM;
		sceneWidth = 0.0;
		sceneHeight = 0.0;		
		xTranslation = 0.0;
		yTranslation = 0.0;
		savedxtranslation = 0.0;
		savedytranslation = 0.0;
		setBounds(subjectBounds);
		refresh();
	}

	/**
	 * Sets water column bounds and recomputes scene center and size if necessary.
	 */
	@Inject
	@Optional
	void setBounds(ISubjectCharacteristics newSubject) {
		setBounds(newSubject.getBounds());
	}

	/**
	 * Sets subject bounds and recomputes scene center and size if necessary.
	 */
	@Inject
	@Optional
	void setBounds(SubjectBounds newSubjectBounds) {
		if (newSubjectBounds != null) {
			double sceneMinX = sceneCenter.x - sceneWidth / 2d;
			double sceneMaxX = sceneCenter.x + sceneWidth / 2d;
			double sceneMaxY = sceneCenter.y + sceneHeight / 2d;
			double sceneMinY = sceneCenter.y - sceneHeight / 2d;

			// recompute scene center and size if necessary
			if (subjectBounds == null //
					|| newSubjectBounds.getMinY() < sceneMinY //
					|| newSubjectBounds.getMaxY() > sceneMaxY //
					|| newSubjectBounds.getMinX() < sceneMinX//
					|| newSubjectBounds.getMaxX() > sceneMaxX) {
				// Compute distances in all directions relative to the centroid
				double centroidX = newSubjectBounds.getCentroidX();
				double centroidY = newSubjectBounds.getCentroidY();
				double topDistance = Math.abs(centroidY - newSubjectBounds.getMinY());
				double bottomDistance = Math.abs(newSubjectBounds.getMaxY() - centroidY);
				double leftDistance = Math.abs(centroidX - newSubjectBounds.getMinX());
				double rightDistance = Math.abs(newSubjectBounds.getMaxX() - centroidX);
				sceneWidth = 2.2 * //
						Math.max(leftDistance, rightDistance);
				sceneHeight = 2.5 * //
						Math.max(topDistance, bottomDistance);
				sceneCenter = new Vec4(centroidX, centroidY);
			}
			subjectBounds = newSubjectBounds;

			refresh();
		}
	}

	/**********************************************************************************************************
	 * MOUSE EVENTS MANAGEMENT
	 **********************************************************************************************************/

	@Override
	public void mouseEntered(MouseEvent evt) {
		// not used
	}

	@Override
	public void mouseExited(MouseEvent evt) {
		// not used
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		if (markersComponent != null && evt.getButton() == MouseEvent.BUTTON1 && allowNewPoint) {
			markersComponent.mouseLeftClicked();
			if (Activator.get2DPreferences().isSinglePointingModeEnabled()) {
				allowNewPoint = false;
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1) {
			if (markersComponent != null) {
				markersComponent.mousePressed(evt);
			}
			sceneProperties.setMouseClickPosition(evt.getX(), evt.getY());

			if (markersComponent == null || !markersComponent.getSelectedPoint().isPresent()) {
				isDraggingScene = true;
			}
			refresh();
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1) {
			if (markersComponent != null) {
				markersComponent.mouseReleased();
			}
			if (isDraggingScene) {
				savedxtranslation = xTranslation;
				savedytranslation = yTranslation;
				isDraggingScene = false;
			}
			refresh();
		} else if (!evt.isConsumed() && evt.isPopupTrigger() && markersComponent != null) {
			MarkerMenu markerMenu = markersComponent.generateMarkerMenu();
			if (markerMenu != null) {
				markerMenu.show(evt.getComponent(), evt.getX(), evt.getY());
				evt.consume();
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (SwingUtilities.isLeftMouseButton(evt)) {
			sceneProperties.setMousePosition(evt.getX(), evt.getY());
			refresh();
		}
	}

	@Override
	public void mouseMoved(MouseEvent evt) {
		sceneProperties.setMousePosition(evt.getX(), evt.getY());
		refresh();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		zoom *= e.getWheelRotation() / 10.0 + 1;
		zoom = Math.max(zoom, 0.001);
		zoom = Math.min(zoom, 100.0);
		refresh();
	}

	@PreDestroy
	protected void dispose() {
		canvas.removeGLEventListener(this);
		canvas.removeMouseListener(this);
		canvas.removeMouseMotionListener(this);
		canvas.removeMouseWheelListener(this);

		// canvas.destroy();
		canvas = null;
	}

	/**
	 * @return the {@link #equalizeAxis}
	 */
	public boolean isEqualizeAxis() {
		return equalizeAxis;
	}

	/**
	 * @param equalizeAxis the {@link #equalizeAxis} to set
	 */
	public void setEqualizeAxis(boolean equalizeAxis) {
		this.equalizeAxis = equalizeAxis;
	}

	/**
	 * @return the {@link #redirectedValue}
	 */
	public float getRedirectedValue() {
		return redirectedValue;
	}
	
}
