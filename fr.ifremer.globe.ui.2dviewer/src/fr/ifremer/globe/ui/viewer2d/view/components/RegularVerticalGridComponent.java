package fr.ifremer.globe.ui.viewer2d.view.components;

import java.awt.geom.Rectangle2D;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.scene.SceneProperties;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.utils.StringRenderer;

/** Regular vertical lines of the grid */
public class RegularVerticalGridComponent implements GLComponent {

	/** Subject displayed in the scene. */
	@Inject
	@Optional
	protected ISubjectCharacteristics subjectCharacteristics;

	/**
	 * Title of the axis.
	 */
	@Inject
	@Optional
	@Named(ContextNames.X_AXIS_NAME)
	private String title = "";

	/** Scene properties computed when drawing */
	@Inject
	@Optional
	private SceneProperties sceneProperties;

	/** True when restricting grid to subject */
	@Inject
	@Named(ContextNames.RESTRICT_GRID)
	private boolean restrictToSubject;

	/** Show grid or not */
	@Inject
	@Named(ContextNames.DRAW_GRID)
	boolean showGrid;

	/** Unit of axis to display. By default, meter */
	private String unit = " m";

	/** Utility instance to draw a Sting on the scene */
	private final StringRenderer stringRenderer = new StringRenderer();

	/**
	 * Dispose OpenGL resources
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		/* not used */
	}

	/**
	 * Draws a container, grids and labels.
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		int xShift = getTickSpace((int) (sceneProperties.getRight() - sceneProperties.getLeft()));
		int x = (int) Math.ceil(sceneProperties.getLeft() / xShift) * xShift;
		while (x < sceneProperties.getRight()) {
			if (!restrictToSubject || x >= subjectCharacteristics.getBounds().getMinX()
					&& x <= subjectCharacteristics.getBounds().getMaxX()) {
				drawVerticalLine(glad, x, showGrid);
			}
			x += xShift;
		}

		if (title != null && !title.isEmpty()) {
			drawTitle(glad);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	private void drawVerticalLine(GLAutoDrawable glad, double x, boolean showGrid) {
		GL2 gl = glad.getGL().getGL2();
		gl.glColor3f(255f, 255f, 255f);
		if (showGrid) {
			gl.glBegin(GL.GL_LINE_STRIP);
			gl.glVertex3d(x, sceneProperties.getTop(), 1.0);
			gl.glVertex3d(x, sceneProperties.getBottom(), 1.0);
			gl.glEnd();
		} else {
			gl.glBegin(GL.GL_LINE_STRIP);
			gl.glVertex3d(x, sceneProperties.getTop(), 1.0);
			gl.glVertex3d(x, sceneProperties.getTopTicks(), 1.0);
			gl.glEnd();
			gl.glBegin(GL.GL_LINE_STRIP);
			gl.glVertex3d(x, sceneProperties.getBottom(), 1.0);
			gl.glVertex3d(x, sceneProperties.getBottomTicks(), 1.0);
			gl.glEnd();
		}

		int label = (int) x;
		String value = String.format("%d", label) + getUnit();
		Rectangle2D bounds = stringRenderer.getTextRenderer().getBounds(value);
		stringRenderer.draw2DStringOnScene(glad, value, x, sceneProperties.getBottom(), (int) -bounds.getCenterX(),
				(int) -bounds.getHeight() - 4);
	}

	/** Draw axis title */
	private void drawTitle(GLAutoDrawable glad) {
		Rectangle2D bounds = stringRenderer.getTextRenderer().getBounds(title);
		stringRenderer.draw2DStringOnScene(glad, title, (int) (glad.getSurfaceWidth() - bounds.getWidth() - 5),
				(int) (sceneProperties.getBottomPadding() - bounds.getHeight()) - 5);
	}

	private int getTickSpace(int width) {
		int result = 50000;
		if (width < 10) {
			result = 1;
		} else if (width < 40) {
			result = 5;
		} else if (width < 80) {
			result = 10;
		} else if (width < 160) {
			result = 20;
		} else if (width < 400) {
			result = 50;
		} else if (width < 800) {
			result = 100;
		} else if (width < 1600) {
			result = 200;
		} else if (width < 4000) {
			result = 500;
		} else if (width < 8000) {
			result = 1000;
		} else if (width < 16000) {
			result = 2000;
		} else if (width < 40000) {
			result = 5000;
		} else if (width < 64000) {
			result = 10000;
		} else if (width < 128000) {
			result = 20000;
		}
		return result;
	}

	@Override
	public boolean isVisible() {
		return true;
	}

	/**
	 * @return the {@link #unit}
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit the {@link #unit} to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
}
