/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.subject;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Subject characteristics to display in the scene (water colunm, water slice...).
 */
public interface ISubjectCharacteristics {

	/** @return this object bounds */
	SubjectBounds getBounds();

	/** @return the geographic position computed from scene values */
	Position fromSceneToGeographic(double distance, double immersion);

	/** return the scene coords computed from a geographic position */
	Vec4 fromGeographicToScene(Position position);

	/**
	 * @return true if the point coordinates are inside bounds
	 */
	default boolean contains(double[] coords) {
		// return contains(coords[0], coords[1]);
		return true;
	}

	/**
	 * @return true if the coordinates are inside bounds
	 */
	default boolean contains(double x, double y) {
		// return getLeftDistance() < x && x < getRightDistance() && getBottomElevation() < y && y < getTopElevation();
		return false;
	}

}
