/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.wc;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.wc.DataProducer;
import fr.ifremer.globe.core.model.wc.SlidingBuffer;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.viewer2d.view.PlayerState;
import fr.ifremer.globe.ui.viewer2d.view.Viewer2DStatusBar;
import fr.ifremer.globe.ui.viewer2d.view.components.CursorLinesComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.FrameComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.MarkersComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.RegularHorizontalGridComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.RegularVerticalGridComponent;
import fr.ifremer.globe.ui.viewer2d.view.scene.ISceneComponentProvider;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerData;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import fr.ifremer.viewer3d.layers.wc.render.AbstractSinglePingSamples2D;
import fr.ifremer.viewer3d.layers.wc.render.SinglePingSamples2D;
import gov.nasa.worldwind.geom.Position;
import io.reactivex.disposables.Disposable;

/**
 * Parameters used to initialize the Viewer2D.
 */
public class WaterColumn2DParameters implements ISceneComponentProvider {

	private static final Logger logger = LoggerFactory.getLogger(WaterColumn2DParameters.class);

	@Inject
	protected IEclipseContext context;
	@Inject
	@Optional
	private Viewer2DScene scene;
	@Inject
	protected WCVolumicLayer wcVolumicLayer;
	@Inject
	protected MarkersComponent markersComponent;
	@Inject
	@Optional
	protected Viewer2DStatusBar statusBar;
	@Inject
	@Optional
	protected ISubjectCharacteristics subjectCharacteristics;

	protected SlidingBuffer<AbstractSinglePingSamples2D> buffer;
	/** GL components, over the grid */
	protected List<GLComponent> glComponents;

	/** Lat/Lon formatter from preferences */
	protected ILatLongFormatter latLongFormatter = LatLongFormater.getFormatter();

	protected Disposable playerSubscription;

	/**
	 * Constructor
	 */
	@PostConstruct
	public void postConstruct(IndexedPlayer indexedPlayer) {
		DataProducer<AbstractSinglePingSamples2D> producer = i -> new SinglePingSamples2D(
				wcVolumicLayer.getPingLoader(), i);
		buffer = SlidingBuffer.buildSingle(producer, getSwathCount() - 1);

		CursorLinesComponent cursorLinesComponent = OsgiUtils.make(CursorLinesComponent.class, context);
		cursorLinesComponent.setCoordFormat(this::format);

		glComponents = List.of(//
				new WaterColumnComponent(buffer, wcVolumicLayer::getDisplayParameters), //
				markersComponent, OsgiUtils.make(FrameComponent.class, context), //
				OsgiUtils.make(RegularVerticalGridComponent.class, context), //
				OsgiUtils.make(RegularHorizontalGridComponent.class, context), //
				cursorLinesComponent);

		playerSubscription = indexedPlayer.subscribe(this::loadIndex);

		// WC data are in used in Viewer 2D. Don't desactivate the layer...
		wcVolumicLayer.setInUseInViewer2d(true);

	}

	protected String format(double x, double y) {
		Position position = subjectCharacteristics.fromSceneToGeographic(x, y);
		if (position != null) {
			return String.format(//
					"lon=%s     lat=%s     accross distance = %.2f m     immersion = %.2f m", //
					latLongFormatter.formatLongitude(position.longitude.degrees), //
					latLongFormatter.formatLatitude(position.latitude.degrees), //
					x, //
					y //
			);
		}
		return String.format("accross distance = %.2f m ; immersion = %.2f m", x, y);
	}

	/**
	 * load data for the given index
	 */
	private void loadIndex(IndexedPlayerData indexedPlayerData) {
		logger.debug("Loading WC index {}", indexedPlayerData.currentValue());
		buffer.setCurrent(indexedPlayerData.currentValue());
		Iterator<AbstractSinglePingSamples2D> it = buffer.values().iterator();
		try {
			if (it.hasNext()) {
				AbstractSinglePingSamples2D currentSinglePingSamples2D = it.next();
				if (!currentSinglePingSamples2D.isEmpty()) {
					context.set(PlayerState.class,
							new PlayerState(currentSinglePingSamples2D.getSwathSpatialData().pingIndex,
									currentSinglePingSamples2D.getSwathSpatialData().pingTimeNs));
					context.set(ISubjectCharacteristics.class,
							new WaterColumnCharacteristics(currentSinglePingSamples2D));
				}
				scene.refresh();
			}
		} catch (Exception e) {

			logger.warn("An Exception occurred while refreshing 2DView");
		}
	}

	/** Updade the Status bar */
	@Inject
	@Optional
	protected void updateStatusbar(PlayerState playerState) {
		if (playerState != null && statusBar != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			String leftInfo = "Date : " + sdf.format(DateUtils.nanoSecondToMilli(playerState.getTimeNs()));
			String rightInfo = String.format("Ping : %d / %d", playerState.getIndex(), getSwathCount());
			statusBar.displayInfo(leftInfo, rightInfo);
		}
	}

	/** {@inheritDoc} */
	protected int getSwathCount() {
		return wcVolumicLayer.getInfo().getCycleCount();
	}

	@PreDestroy
	protected void dispose() {
		playerSubscription.dispose();
		wcVolumicLayer.setInUseInViewer2d(false);
	}

	/**
	 * @return the {@link #glComponents}
	 */
	@Override
	public List<GLComponent> getGLComponents() {
		return glComponents;
	}

}
