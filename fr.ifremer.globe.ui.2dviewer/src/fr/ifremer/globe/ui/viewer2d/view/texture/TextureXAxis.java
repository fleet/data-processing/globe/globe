package fr.ifremer.globe.ui.viewer2d.view.texture;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

/**
 * This class handles the projection along the X axis, which is defined in meters.
 */
public class TextureXAxis {

	/** Cache (FIFO) of distance by {@link LatLon} to optimize the project method **/
	private final LinkedHashMap<LatLon, Double> cache = new LinkedHashMap<>(100) {
		private static final long serialVersionUID = 3250365587950941859L;

		@Override
		protected boolean removeEldestEntry(Entry<LatLon, Double> entry) {
			return size() > 100;
		}
	};;

	/**
	 * All coordinates sorted by distance to the first one. (distance = x position)
	 */
	private final TreeMap<Double, IWWTextureCoordinate> coordinateByDistance = new TreeMap<>();

	/**
	 * Constructor
	 */
	public TextureXAxis(Map<IWWTextureCoordinate, Double> distanceByCoordinate) {
		distanceByCoordinate.forEach((coordinate, distance) -> coordinateByDistance.put(distance, coordinate));
	}

	/**
	 * @return texture width
	 */
	public double getTextureWidth() {
		return coordinateByDistance.lastKey() - coordinateByDistance.firstKey();
	}

	/**
	 * @return texture min xCoord
	 */
	public double getMin() {
		return coordinateByDistance.firstKey();
	}

	/**
	 * @return texture max xCoord
	 */
	public double getMax() {
		return coordinateByDistance.lastKey();
	}

	/**
	 * Geographic position to scene X.
	 */
	public double project(LatLon pos) {
		if (cache.containsKey(pos))
			return cache.get(pos);

		var nearestCoordinates = findClosestCoordinates(pos);
		var previous = nearestCoordinates.getFirst();
		var next = nearestCoordinates.getSecond();
		var coeff = dist(previous.getValue().getPosition(), pos)
				/ dist(previous.getValue().getPosition(), next.getValue().getPosition());

		// compute interpolation of x
		var x = previous.getKey() + coeff * (next.getKey() - previous.getKey());

		// cache result
		cache.put(pos, x);

		return x;
	}

	/**
	 * Scene X coordinate to geographic position.
	 */
	public LatLon unproject(double x) {
		if (coordinateByDistance.containsKey(x))
			return new LatLon(coordinateByDistance.get(x).getPosition());

		// Get a position before and after : pass if not found
		Entry<Double, IWWTextureCoordinate> previous = coordinateByDistance.floorEntry(x);
		Entry<Double, IWWTextureCoordinate> next = coordinateByDistance.ceilingEntry(x);
		if (previous == null) {
			previous = next;
			next = coordinateByDistance.higherEntry(next.getKey());
		} else if (next == null) {
			next = previous;
			previous = coordinateByDistance.lowerEntry(next.getKey());
		}

		// Compute interpolated latitude and longitude
		double coeff = !next.getKey().equals(previous.getKey())
				? (x - previous.getKey()) / (next.getKey() - previous.getKey())
				: 0;

		return LatLon.interpolate(coeff, previous.getValue().getPosition(), next.getValue().getPosition());
	}

	/**
	 * @return the two closest coordinates to the provided {@link LatLon}.
	 */
	private Pair<Entry<Double, IWWTextureCoordinate>, Entry<Double, IWWTextureCoordinate>> findClosestCoordinates(
			LatLon latLon) {
		// get the 2 nearest entries
		Entry<Double, IWWTextureCoordinate> entryA = coordinateByDistance.firstEntry();
		Entry<Double, IWWTextureCoordinate> entryB = coordinateByDistance.lastEntry();
		var nearestDist = new double[] { dist(entryA.getValue().getPosition(), latLon),
				dist(entryB.getValue().getPosition(), latLon) };
		for (var entry : coordinateByDistance.entrySet()) {
			var currentDist = dist(entry.getValue().getPosition(), latLon);
			if (nearestDist[0] > currentDist) {
				entryA = entry;
				nearestDist[0] = currentDist;
			} else if (nearestDist[1] > currentDist) {
				entryB = entry;
				nearestDist[1] = currentDist;
			}
		}

		// sort result
		if (entryA.getValue().getS() > entryB.getValue().getS()) {
			var tmp = entryB;
			entryB = entryA;
			entryA = tmp;
		}

		return Pair.of(entryA, entryB);
	}

	/**
	 * Computes distance between two {@link Position}, ignoring elevation.
	 */
	private double dist(LatLon p1, LatLon p2) {
		return LatLon.linearDistance(new LatLon(p1), new LatLon(p2)).degrees;
	}

}
