/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.view.texture;

import java.beans.PropertyChangeListener;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.viewer2d.view.PlayerState;
import fr.ifremer.globe.ui.viewer2d.view.Viewer2DStatusBar;
import fr.ifremer.globe.ui.viewer2d.view.components.CursorLinesComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.FrameComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.MarkersComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.ProjectedVerticalGridComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.RegularHorizontalGridComponent;
import fr.ifremer.globe.ui.viewer2d.view.components.RegularVerticalGridComponent;
import fr.ifremer.globe.ui.viewer2d.view.scene.ISceneComponentProvider;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import gov.nasa.worldwind.geom.Position;
import io.reactivex.disposables.Disposable;

/**
 * Parameters used to initialize the Viewer2D and displaying a Water slice.
 */
public class TextureParameters implements ISceneComponentProvider {

	@Inject
	protected IEclipseContext context;
	@Inject
	protected IWWTextureLayer textureLayer;
	@Inject
	@Optional
	protected Viewer2DStatusBar statusBar;
	@Inject
	@Optional
	protected TextureXAxis xProjectedValues;
	@Inject
	protected MarkersComponent markersComponent;

	/** Scene **/
	@Optional
	@Inject
	private Viewer2DScene scene;

	@Optional
	@Inject
	IndexedPlayer indexedPlayer;

	/** The vertical lines */
	protected ProjectedVerticalGridComponent projectedVerticalGridComponent;
	/** GL components, over the grid */
	protected List<GLComponent> glComponents;

	protected TextureComponent textureComponent;
	protected ISubjectCharacteristics textureCharacteristics;
	protected Disposable playerSubscription;
	protected PropertyChangeListener displayParametersListener;

	/** Lat/Lon formatter from preferences */
	protected ILatLongFormatter latLongFormatter = LatLongFormater.getFormatter();

	/**
	 * Constructor
	 */
	@PostConstruct
	public void postConstruct(@Optional IndexedPlayer indexedPlayer) {
		IWWTextureData textureData = textureLayer.getTextureData();
		textureComponent = new TextureComponent(textureLayer::getDisplayParameters);
		ContextInjectionFactory.inject(textureComponent, context);

		if (textureData.isVerticalTexture()) {
			projectedVerticalGridComponent = OsgiUtils.make(ProjectedVerticalGridComponent.class, context);
			CursorLinesComponent cursorLinesComponent = OsgiUtils.make(CursorLinesComponent.class, context);
			cursorLinesComponent.setCoordFormat(this::formatVertical);
			glComponents = List.of(//
					textureComponent, //
					markersComponent, //
					OsgiUtils.make(FrameComponent.class, context), //
					projectedVerticalGridComponent, //
					OsgiUtils.make(RegularHorizontalGridComponent.class, context), //
					cursorLinesComponent //
			);
		} else {
			// Grid
			var horizontalGrid = OsgiUtils.make(RegularHorizontalGridComponent.class, context);
			horizontalGrid.setUnit("");
			var verticalGrid = OsgiUtils.make(RegularVerticalGridComponent.class, context);
			verticalGrid.setUnit("");
			CursorLinesComponent cursorLinesComponent = OsgiUtils.make(CursorLinesComponent.class, context);
			cursorLinesComponent.setCoordFormat(this::format);
			glComponents = List.of(//
					textureComponent, //
					markersComponent, //
					OsgiUtils.make(FrameComponent.class, context), //
					horizontalGrid, //
					verticalGrid, //
					cursorLinesComponent //
			);
		}
		// React to the change of display parameters (change of texture) and change of index in player
		displayParametersListener = event -> scene.refresh();
		textureLayer.addPropertyChangeListener(IWWTextureLayer.DISPLAY_PARAMETERS_PROPERTY, displayParametersListener);

		if (indexedPlayer == null)
			loadTexture(0); // No player : load the single texture
		else
			playerSubscription = indexedPlayer.getIndexPublisher().subscribe(data -> loadTexture(data.currentValue()));
	}

	/**
	 * Computes the text to display at the bottom of the 2D scene.
	 */
	protected String formatVertical(double x, double y) {
		var texCharacteristics = (WWVerticalTextureCharacteristics) textureCharacteristics;
		if (texCharacteristics != null && xProjectedValues != null) {
			var bounds = texCharacteristics.getBounds();
			if (Double.isFinite(x) && x >= bounds.getMinX() && x <= bounds.getMaxX() && y >= bounds.getMinY()
					&& y <= bounds.getMaxY()) {
				var position = new Position(xProjectedValues.unproject(x), y);
				var sb = new StringBuilder();
				sb.append(String.format(//
						"lon = %s    lat = %s    distance = %.2f m    elevation = %.2f m    value = %.2f", //
						latLongFormatter.formatLongitude(position.longitude.degrees), //
						latLongFormatter.formatLatitude(position.latitude.degrees), //
						x, //
						y, //
						scene.getRedirectedValue()));
				return sb.toString();
			}
		}
		return String.format("elevation = %.2f m", y);
	}

	/**
	 * Computes the text to display at the bottom of the 2D scene.
	 */
	protected String format(double x, double y) {
		var texCharacteristics = (WWTextureCharacteristics) textureCharacteristics;
		if (texCharacteristics != null) {
			var bounds = texCharacteristics.getBounds();
			if (Double.isFinite(x) && Double.isFinite(y) && x >= bounds.getMinX() && x <= bounds.getMaxX()
					&& y >= bounds.getMinY() && y <= bounds.getMaxY()) {
				var position = texCharacteristics.fromSceneToGeographic(x, y);
				var sb = new StringBuilder();
				sb.append(String.format(//
						"lon = %s    lat = %s    elevation = %.2f m    value = %.2f    ", //
						latLongFormatter.formatLongitude(position.longitude.degrees), //
						latLongFormatter.formatLatitude(position.latitude.degrees), //
						position.elevation, scene.getRedirectedValue()));
				return sb.toString();
			}
		}
		return "";
	}

	/**
	 * load data for the given index
	 */
	private void loadTexture(int playerIndex) {
		IWWTextureData data = textureLayer.getTextureData();
		if (data == null || data.getWidth() == 0 || data.getHeight() == 0) {
			textureComponent.clearTextureRenderables();
			return;
		}

		// Send new data to the GL component
		textureCharacteristics = textureComponent.acceptData(data);
		if (textureCharacteristics instanceof WWVerticalTextureCharacteristics)
			context.set(TextureXAxis.class,
					((WWVerticalTextureCharacteristics) textureCharacteristics).getxInterpolatedValues());
		context.set(PlayerState.class, new PlayerState(playerIndex, 0L));
		context.set(ISubjectCharacteristics.class, textureCharacteristics);
	}

	/** Updade the Status bar */
	@Inject
	@Optional
	protected void updateStatusbar(PlayerState playerState) {
		if (indexedPlayer != null && playerState != null && statusBar != null) {
			int index = playerState.getIndex();
			String rightInfo = String.format("%s (index : %d/%d)", indexedPlayer.getLabelProvider().apply(index), index,
					indexedPlayer.getIndexCount());
			statusBar.displayInfo("", rightInfo);
		}
	}

	@PreDestroy
	protected void dispose() {
		if (playerSubscription != null) {
			playerSubscription.dispose();
		}
		if (textureLayer != null) {
			textureLayer.removePropertyChangeListener(IWWTextureLayer.DISPLAY_PARAMETERS_PROPERTY,
					displayParametersListener);
		}
		if (textureComponent != null) {
			textureComponent.dispose();
		}
	}

	/**
	 * @return the {@link #glComponents}
	 */
	@Override
	public List<GLComponent> getGLComponents() {
		return glComponents;
	}
}
