package fr.ifremer.globe.ui.viewer2d.view;

import java.awt.BorderLayout;
import java.awt.Frame;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;

import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;

/**
 * Water column 2D viewer.
 */
public class Viewer2D {

	public static final String PART_ID = "fr.ifremer.globe.ui.viewer2d.partdescriptor";

	Frame frame;

	/**
	 * Creates UI
	 */
	@PostConstruct
	public void createUI(Composite parent, MPart part, IEclipseContext context) {
		part.getContext().set(Viewer2D.class, this);

		final Composite composite = new Composite(parent, SWT.EMBEDDED);
		FormData data = new FormData();
		data.top = UIUtils.FORM_ATTACHMENT_0_0;
		data.bottom = UIUtils.FORM_ATTACHMENT_100_0;
		data.left = UIUtils.FORM_ATTACHMENT_0_0;
		data.right = UIUtils.FORM_ATTACHMENT_100_0;
		composite.setLayoutData(data);

		frame = SWT_AWT.new_Frame(composite);

		// Initialize toolbar buttons
		for (MToolBarElement e : part.getToolbar().getChildren()) {
			if (e.getElementId().equals("fr.ifremer.globe.ui.2dviewer.directtoolitem.showGrid")) {
				((MItem) e).setSelected(Activator.get2DPreferences().getDrawMetrics());
				// Initialize the value in the context for components
				context.set(ContextNames.DRAW_GRID, ((MItem) e).isSelected());
			}
			if (e.getElementId().equals("fr.ifremer.globe.ui.2dviewer.directtoolitem.scale")) {
				((MItem) e).setSelected(Activator.get2DPreferences().getEqualizeAxis());
			}
			if (e.getElementId().equals("fr.ifremer.globe.ui.2dviewer.directtoolitem.cursorline")) {
				((MItem) e).setSelected(Activator.get2DPreferences().getDrawCursorMetrics());
			}
			if (e.getElementId().equals("fr.ifremer.globe.ui.2dviewer.directtoolitem.showzdetection")) {
				((MItem) e).setSelected(Activator.get2DPreferences().getDrawZDetection());
			}
		}
	}

	/**
	 * Set canvas when Viewer2DBuilder creates it
	 */
	@Inject
	@Optional
	void setCanvas(GLCanvas canvas) {
		frame.add(canvas, BorderLayout.CENTER);
	}

	/**
	 * Set canvas when Viewer2DBuilder creates it
	 */
	@Inject
	@Optional
	void setStatusBar(Viewer2DStatusBar statusBar) {
		frame.add(statusBar, BorderLayout.SOUTH);
	}

}
