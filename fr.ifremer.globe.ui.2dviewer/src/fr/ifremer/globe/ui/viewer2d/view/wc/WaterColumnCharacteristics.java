package fr.ifremer.globe.ui.viewer2d.view.wc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.latlon.CoordinateDisplayType;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.ui.viewer2d.view.subject.SubjectBounds;
import fr.ifremer.viewer3d.layers.wc.render.AbstractSinglePingSamples2D;
import fr.ifremer.viewer3d.layers.wc.render.SinglePingSamples2D;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Class use to defines the water column bounds and other characteristics.
 */
public class WaterColumnCharacteristics implements ISubjectCharacteristics {

	private static final Logger logger = LoggerFactory.getLogger(WaterColumnCharacteristics.class);

	private final SubjectBounds bounds;
	private final LatLon leftLatLon;
	private final LatLon rightLatLon;

	/**
	 * Constructor from a {@link SinglePingSamples2D}
	 */
	public WaterColumnCharacteristics(AbstractSinglePingSamples2D singlePingSamples2D) {
		leftLatLon = LatLon.fromDegrees(singlePingSamples2D.getLeftLatLon()[0], singlePingSamples2D.getLeftLatLon()[1]);
		rightLatLon = LatLon.fromDegrees(singlePingSamples2D.getRightLatLon()[0],
				singlePingSamples2D.getRightLatLon()[1]);

		if (logger.isDebugEnabled()) {
			logger.debug("WC positions : left = ({}, {}),  right = ({}, {})", //
					LatLongFormater.latitudeToString(leftLatLon.latitude.degrees, CoordinateDisplayType.DEG_MIN_SEC), //
					LatLongFormater.longitudeToString(leftLatLon.longitude.degrees, CoordinateDisplayType.DEG_MIN_SEC), //
					LatLongFormater.latitudeToString(rightLatLon.latitude.degrees, CoordinateDisplayType.DEG_MIN_SEC), //
					LatLongFormater.longitudeToString(rightLatLon.longitude.degrees, CoordinateDisplayType.DEG_MIN_SEC)//
			);
			logger.debug("WC positions : left = ({}, {}),  right = ({}, {})", //
					LatLongFormater.latitudeToString(leftLatLon.latitude.degrees, CoordinateDisplayType.DECIMAL), //
					LatLongFormater.longitudeToString(leftLatLon.longitude.degrees, CoordinateDisplayType.DECIMAL), //
					LatLongFormater.latitudeToString(rightLatLon.latitude.degrees, CoordinateDisplayType.DECIMAL), //
					LatLongFormater.longitudeToString(rightLatLon.longitude.degrees, CoordinateDisplayType.DECIMAL)//
			);
		}

		bounds = new SubjectBounds(//
				singlePingSamples2D.getMinAccrossDistance(), //
				singlePingSamples2D.getMaxAccrossDistance(), //
				singlePingSamples2D.getTopElevation(), //
				singlePingSamples2D.getBottomElevation());
		bounds.setCentroidX(0d);
		if (logger.isDebugEnabled()) {
			logger.debug("WC bounds : {}", bounds.toString());
		}
	}

	/**********************************************************************************************************
	 * PROJECTION METHODS (Geographic <-> Scene)
	 **********************************************************************************************************/

	/** {@inheritDoc} */
	@Override
	public Position fromSceneToGeographic(double distance, double immersion) {
		double xRelativePosition = (distance - bounds.getMinX()) / bounds.getWidth();
		LatLon result = LatLon.interpolate(xRelativePosition, leftLatLon, rightLatLon);
		return new Position(result, immersion);
	}

	/** {@inheritDoc} */
	@Override
	public Vec4 fromGeographicToScene(Position position) {
		Angle width = LatLon.linearDistance(leftLatLon, rightLatLon);
		Angle distanceToPosition = LatLon.linearDistance(leftLatLon, position);
		double interpolateAmount = distanceToPosition.degrees / width.degrees;
		return new Vec4(bounds.getMinX() + bounds.getWidth() * interpolateAmount, position.elevation);
	}

	// GETTERS

	/**
	 * @return the {@link #bounds}
	 */
	@Override
	public SubjectBounds getBounds() {
		return bounds;
	}

}
