package fr.ifremer.globe.ui.viewer2d.view.components;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.scene.SceneProperties;
import fr.ifremer.globe.ui.viewer2d.view.texture.TextureXAxis;
import fr.ifremer.globe.ui.viewer2d.view.utils.StringRenderer;

/** Redefines RegularGridComponent to manage the x axis with a ProjectededAxisValues */
public class ProjectedVerticalGridComponent implements GLComponent {

	/** Scene properties computed when drawing */
	@Inject
	@Optional
	protected SceneProperties sceneProperties;

	/** Utility instance to draw a Sting on the scene */
	protected StringRenderer stringRenderer = new StringRenderer();

	@Inject
	@Optional
	protected TextureXAxis xAxis;

	/** Show grid or not */
	@Inject
	@Optional
	@Named(ContextNames.DRAW_GRID)
	Boolean drawMetrics;

	/**
	 * Dispose OpenGL resources
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		/* not used */
	}

	/**
	 * Draws a container, grids and labels.
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		boolean showGrid = drawMetrics == Boolean.TRUE;
		double left = !Double.isNaN(sceneProperties.getLeft()) ? sceneProperties.getLeft() : xAxis.getMin();
		double right = !Double.isNaN(sceneProperties.getRight()) ? sceneProperties.getRight() : xAxis.getMax();
		int xShift = getTickSpace((int) (right - left));
		double x = Math.ceil(left / xShift) * xShift;
		while (x <= right) {
			drawVerticalLine(glad, x, showGrid);
			x += xShift;
		}
	}

	/** {@inheritDoc} */
	protected void drawVerticalLine(GLAutoDrawable glad, double x, boolean showGrid) {
		if (!Double.isNaN(x)) {
			GL2 gl = glad.getGL().getGL2();
			gl.glColor3f(255f, 255f, 255f);
			if (showGrid) {
				gl.glBegin(GL2.GL_LINE_STRIP);
				gl.glVertex3d(x, sceneProperties.getTop(), 1.0);
				gl.glVertex3d(x, sceneProperties.getBottom(), 1.0);
				gl.glEnd();
			} else {
				gl.glBegin(GL2.GL_LINE_STRIP);
				gl.glVertex3d(x, sceneProperties.getTop(), 1.0);
				gl.glVertex3d(x, sceneProperties.getTopTicks(), 1.0);
				gl.glEnd();
				gl.glBegin(GL2.GL_LINE_STRIP);
				gl.glVertex3d(x, sceneProperties.getBottom(), 1.0);
				gl.glVertex3d(x, sceneProperties.getBottomTicks(), 1.0);
				gl.glEnd();
			}
			stringRenderer.draw2DStringOnScene(glad, String.format("%.0f m", x), x, sceneProperties.getBottom(), -10,
					-15);
		}
	}

	/**
	 * @param xInterpolatedValues the {@link #xAxis} to set
	 */
	public void setProjectededAxisValues(TextureXAxis xProjectedValues) {
		xAxis = xProjectedValues;
	}

	protected int getTickSpace(int width) {
		int result = 50000;
		if (width < 10) {
			result = 1;
		} else if (width < 40) {
			result = 5;
		} else if (width < 80) {
			result = 10;
		} else if (width < 160) {
			result = 20;
		} else if (width < 400) {
			result = 50;
		} else if (width < 800) {
			result = 100;
		} else if (width < 1600) {
			result = 200;
		} else if (width < 4000) {
			result = 500;
		} else if (width < 8000) {
			result = 1000;
		} else if (width < 16000) {
			result = 2000;
		} else if (width < 40000) {
			result = 5000;
		} else if (width < 64000) {
			result = 10000;
		} else if (width < 128000) {
			result = 20000;
		}
		return result;

	}

	@Override
	public boolean isVisible() {
		return true;
	}

}
