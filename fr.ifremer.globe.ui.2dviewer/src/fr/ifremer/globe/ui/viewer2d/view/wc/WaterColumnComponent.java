package fr.ifremer.globe.ui.viewer2d.view.wc;

import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GLAutoDrawable;

import fr.ifremer.globe.core.model.wc.SlidingBuffer;
import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.wc.render.AbstractSinglePingSamples2D;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRenderer2D;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters;

/**
 * This component displays a 2D water column.
 */
public class WaterColumnComponent implements GLComponent {

	private static final Logger LOGGER = LoggerFactory.getLogger(WaterColumnComponent.class);

	private final VolumicRenderer2D volumicRenderer;
	private final Supplier<VolumicRendererDisplayParameters> displayParametersSupplier;

	/**
	 * Constructor
	 */
	public WaterColumnComponent(SlidingBuffer<AbstractSinglePingSamples2D> buffer,
			Supplier<VolumicRendererDisplayParameters> displayParametersSupplier) {
		volumicRenderer = new VolumicRenderer2D(buffer);
		this.displayParametersSupplier = displayParametersSupplier;
	}

	/**
	 * Dispose
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		volumicRenderer.dispose();
	}

	/**
	 * Draws the water column.
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		try {
			// Enable z- (depth) buffer for hidden surface removal.
			glad.getGL().getGL2().glEnable(GL.GL_DEPTH_TEST);
			glad.getGL().getGL2().glDepthFunc(GL.GL_LEQUAL);

			volumicRenderer.draw(glad.getGL().getGL2(), displayParametersSupplier.get());

			glad.getGL().getGL2().glDisable(GL.GL_DEPTH_TEST);
		} catch (GIOException e) {
			LOGGER.error("Error with draw method", e);
		}
	}
	
	/**
	 * Redirects the water column.
	 */
	@Override
	public void redirect(GLAutoDrawable glad, float resolution, float minValue) {
		try {
			// Enable z- (depth) buffer for hidden surface removal.
			glad.getGL().getGL2().glEnable(GL.GL_DEPTH_TEST);
			glad.getGL().getGL2().glDepthFunc(GL.GL_LEQUAL);

			volumicRenderer.redirect(glad.getGL().getGL2(), displayParametersSupplier.get(), resolution, minValue);

			glad.getGL().getGL2().glDisable(GL.GL_DEPTH_TEST);
		} catch (GIOException e) {
			LOGGER.error("Error with draw method", e);
		}
	}

	@Override
	public boolean isVisible() {
		return true;
	}

}
