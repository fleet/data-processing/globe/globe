package fr.ifremer.globe.ui.viewer2d.view.texture;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import com.jogamp.opengl.GLAutoDrawable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.texture.TextureDataParameters;
import fr.ifremer.globe.ui.texture.TextureDataSlicer;
import fr.ifremer.globe.ui.texture.TextureRenderer;
import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.subject.ISubjectCharacteristics;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.viewer3d.CapacityInspector;
import fr.ifremer.viewer3d.layers.textures.render.TextureRenderable;
import gov.nasa.worldwind.geom.LatLon;

/**
 * This component displays a 2D texture.
 */
public class TextureComponent implements GLComponent {

	private static final Logger LOGGER = LoggerFactory.getLogger(TextureComponent.class);

	/** OpenGL max texture size **/
	private static final int GL_MAX_TEXTURE_SIZE = CapacityInspector.getMaxTextureSize();

	/** Supplier of settings **/
	private final Supplier<TextureDataParameters> settingsSupplier;

	/** List of {@link TextureRenderer} displayed by this component **/
	private final List<TextureRenderer<?>> textureRenderers = new ArrayList<>();

	/** True when texture has to be flipped */
	@Inject
	@Named(ContextNames.FLIP_Y_AXIS)
	private boolean flipYAxis;

	/**
	 * Constructor
	 */
	public TextureComponent(Supplier<TextureDataParameters> settingsSupplier) {
		this.settingsSupplier = settingsSupplier;
	}

	public void dispose() {
		clearTextureRenderables();
	}

	/**
	 * Dispose OpenGL resources
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		dispose();
	}

	/**
	 * Draws the water column.
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		synchronized (textureRenderers) {
			textureRenderers.forEach(textureRenderable -> textureRenderable.draw(glad.getGL().getGL2()));
		}
	}
	
	/**
	 * Redirect the water column.
	 */
	@Override
	public void redirect(GLAutoDrawable glad, float resolution, float minValue) {
		synchronized (textureRenderers) {
			textureRenderers.forEach(textureRenderable -> textureRenderable.redirect(glad.getGL().getGL2(), resolution, minValue));
		}
	}

	@Override
	public boolean isVisible() {
		return true;
	}

	/**
	 * Builds {@link TextureRenderer} with the provided {@link IBasicTextureData}.
	 *
	 * @return {@link BasicTextureCharacteristics}
	 */
	public BasicTextureCharacteristics acceptTexture(IBasicTextureData texture) {
		var start = Instant.now();
		clearTextureRenderables();
		IBasicTextureData[][] textureDataMatrix = new TextureDataSlicer().slice(texture, GL_MAX_TEXTURE_SIZE,
				GL_MAX_TEXTURE_SIZE);
		if (textureDataMatrix.length > 1 || textureDataMatrix[0].length > 1)
			LOGGER.debug("Viewer2D : texture compenent load data : texture split in [row:{},col:{}] parts.",
					textureDataMatrix.length, textureDataMatrix[0].length);

		// creates texture renderables
		synchronized (textureRenderers) {
			for (IBasicTextureData[] textureDataLine : textureDataMatrix) {
				for (IBasicTextureData textureData : textureDataLine) {
					var textureRenderable = new TextureRenderer<>(settingsSupplier);
					textureRenderable.acceptData(textureData, coord -> {
						if (flipYAxis) {
							return new float[] { coord.getX(), textureData.getHeight() - coord.getY(), 0f };
						}
						return new float[] { coord.getX(), coord.getY(), 0f };
					});
					textureRenderers.add(textureRenderable);
				}
			}
		}
		LOGGER.debug("Viewer2D : texture component loaded in {} ", Duration.between(start, Instant.now()));
		return new BasicTextureCharacteristics(texture);
	}

	/**
	 * Builds {@link TextureRenderer} with the provided {@link IWWTextureData}.
	 *
	 * @return {@link ISubjectCharacteristics}
	 */
	public ISubjectCharacteristics acceptData(IWWTextureData data) {
		var start = Instant.now();
		clearTextureRenderables();

		ISubjectCharacteristics textureCharacteristics;
		if (data.isVerticalTexture()) {
			textureCharacteristics = buildVerticalTextureRenderer(data);
		} else {
			textureCharacteristics = buildHorizontalTextureRenderer(data);
		}
		LOGGER.debug("Viewer2D : texture component loaded in {} ", Duration.between(start, Instant.now()));
		return textureCharacteristics;
	}

	private WWVerticalTextureCharacteristics buildVerticalTextureRenderer(IWWTextureData data) {
		// split texture data if necessary (to avoid texture with size over OpenGL limits)
		var textureDataMatrix = WWTextureDataUtils.split(data, GL_MAX_TEXTURE_SIZE, GL_MAX_TEXTURE_SIZE);
		if (textureDataMatrix.length > 1 || (textureDataMatrix.length > 0 && textureDataMatrix[0].length > 1))
			LOGGER.debug("Viewer2D : texture compenent load data : texture split in [row:{},col:{}] parts.",
					textureDataMatrix.length, textureDataMatrix[0].length);


		double distanceMin = data.getLongitudinalOffset();
		// for each texture coordinate : compute distance from the start of the first texture
		var distanceByCoordinate = new HashMap<IWWTextureCoordinate, Double>();
		for (IWWTextureData[] element : textureDataMatrix) {
			// for each row, distance start = 0
			double distance = distanceMin;
			for (IWWTextureData element2 : element) {
				var coordinates = element2.getCoordinates();
				// assume that the new column start at the end of the previous
				var currentTopCoordinate = coordinates.get(0);
				var currentBottomCoordinate = coordinates.get(1);
				distanceByCoordinate.put(currentTopCoordinate, distance);
				distanceByCoordinate.put(currentBottomCoordinate, distance);
				for (int i = 2; i < coordinates.size(); i+=2) {
					var previousTopCoordinate = currentTopCoordinate;
					currentTopCoordinate = coordinates.get(i);
					currentBottomCoordinate = coordinates.get(i+1);
					if (!LatLon.equals(previousTopCoordinate.getPosition(), currentTopCoordinate.getPosition()))
						distance = distance + LatLon.ellipsoidalDistance(previousTopCoordinate.getPosition(),
								currentTopCoordinate.getPosition(), Ellipsoid.WGS84.getHalf_A(),
								Ellipsoid.WGS84.getHalf_B());
					distanceByCoordinate.put(currentTopCoordinate, distance);
					distanceByCoordinate.put(currentBottomCoordinate, distance);
				}
			}
		}

		// build axis with 2D coordinates
		var xAxis = new TextureXAxis(distanceByCoordinate);

		// creates texture renderables
		synchronized (textureRenderers) {
			for (IWWTextureData[] element : textureDataMatrix) {
				for (IWWTextureData element2 : element) {
					var textureRenderable = new TextureRenderer<IWWTextureCoordinate>(settingsSupplier);
					textureRenderable.acceptData(element2, coord -> {
						float x = distanceByCoordinate.get(coord).floatValue();
						float y = (float) coord.getPosition().elevation;
						return new float[] { x, y, 0f };
					});
					textureRenderers.add(textureRenderable);
				}
			}
		}

		// compute minimum and maximum elevation
		var stats = data.getCoordinates().stream().mapToDouble(coord -> coord.getPosition().elevation)
				.summaryStatistics();
		return new WWVerticalTextureCharacteristics(stats.getMax(), stats.getMin(), xAxis);
	}

	private WWTextureCharacteristics buildHorizontalTextureRenderer(IWWTextureData data) {
		// split texture data if necessary (to avoid texture with size over OpenGL limits)
		int maxTextureSize = GL_MAX_TEXTURE_SIZE;
		var textureDataMatrix = WWTextureDataUtils.split(data, maxTextureSize, maxTextureSize);
		if (textureDataMatrix.length > 1 || textureDataMatrix[0].length > 1)
			LOGGER.debug("Viewer2D : texture compenent load data : texture split in [row:{},col:{}] parts.",
					textureDataMatrix.length, textureDataMatrix[0].length);

		// creates texture renderables
		synchronized (textureRenderers) {
			for (int i = 0; i < textureDataMatrix.length; i++) {
				IWWTextureData[] rowElement = textureDataMatrix[i];
				for (int j = 0; j < rowElement.length; j++) {
					IWWTextureData element = rowElement[j];
					var textureRenderable = new TextureRenderer<IWWTextureCoordinate>(settingsSupplier);
					var rowStart = i * maxTextureSize;
					var colStart = j * maxTextureSize;
					textureRenderable.acceptData(element, coord -> {
						float x = colStart + coord.getS() * element.getWidth();
						float y = rowStart + coord.getT() * element.getHeight();
						return new float[] { x, y, 0.f };
					});
					textureRenderers.add(textureRenderable);
				}
			}
		}

		return new WWTextureCharacteristics(data);
	}

	/**
	 * Removes current {@link TextureRenderable}.
	 */
	public void clearTextureRenderables() {
		synchronized (textureRenderers) {
			textureRenderers.forEach(TextureRenderer::dispose);
			textureRenderers.clear();
		}
	}

}
