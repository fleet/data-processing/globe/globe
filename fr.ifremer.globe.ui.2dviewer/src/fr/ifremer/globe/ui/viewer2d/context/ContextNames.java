/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.context;

/**
 * All names of injectable objects stored in the IEclipseContext
 */
public class ContextNames {

	/** Root name of all objects that can be injected */
	public static final String BASE_NAME = "fr.ifremer.globe.ui.viewer2d";

	/** Name of the String name of the x axis */
	public static final String X_AXIS_NAME = BASE_NAME + ".X_AXIS_NAME";

	/** Name of the String name of the y axis */
	public static final String Y_AXIS_NAME = BASE_NAME + ".Y_AXIS_NAME";

	/** Name of the Boolean indicating if y axis has to be flipped */
	public static final String FLIP_Y_AXIS = BASE_NAME + ".FLIP_Y_AXIS";

	/** Name of the Boolean indicating if cursor lines are drawn */
	public static final String DRAW_CURSOR_LINES = BASE_NAME + ".DRAW_CURSOR_LINES";

	/**
	 * Name of the Boolean indicating whether the vertical and horizontal lines of the grid should be restricted to the
	 * subject boundaries
	 */
	public static final String RESTRICT_GRID = BASE_NAME + ".RESTRICT_GRID";

	/** Name of the Boolean indicating if grid may be drawn */
	public static final String DRAW_GRID = BASE_NAME + ".DRAW_GRID";

	/** Name of the String id of the current viewer window */
	public static final String WINDOW_ID = BASE_NAME + ".WINDOW_ID";

	/** Name of the String name of the player bean */
	public static final String PLAYER_BEAN_NAME = BASE_NAME + ".PLAYER_BEAN_NAME";

	/**
	 * Constructor
	 */
	private ContextNames() {
		// private constructor to hide the implicit public one.
	}
}
