package fr.ifremer.globe.ui.viewer2d.handlers;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.jface.widgets.SpinnerFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.view.wc.ZDetectionComponent;

/** Tool control to edit the Z detection point size */
public class ZDetectionSizeHandler {

	@Optional
	@Inject
	/** ZDetectionComponent if deployed in the context */
	private ZDetectionComponent zDetectionComponent;

	@Inject
	@Optional
	/** Scene when available */
	private Viewer2DScene scene;

	@PostConstruct
	public void createControls(Composite parent) {
		Spinner widget = SpinnerFactory.newSpinner(SWT.READ_ONLY | SWT.BORDER)//
				.bounds(1, 15)//
				.tooltip("Point size for Z Detection")//
				.create(parent);
		widget.setSelection(Activator.get2DPreferences().getZDetectionSize());
		widget.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onSizeChanged(widget)));
	}

	/** Point size changed */
	private void onSizeChanged(Spinner widget) {
		if (widget != null) {
			int zDetectionSize = widget.getSelection();
			Activator.get2DPreferences().setZDetectionSize(zDetectionSize);
			Activator.get2DPreferences().save();

			if (zDetectionComponent != null) {
				zDetectionComponent.setPointSize(zDetectionSize);
				if (scene != null)
					scene.refresh();
			}
		}
	}
}