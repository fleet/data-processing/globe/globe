package fr.ifremer.globe.ui.viewer2d.handlers;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.ToolItem;

import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.xml.IWaterColumn2DViewer;
import fr.ifremer.viewer3d.layers.markers.MarkerController;

public class WaterColumn2DAddPointHandler {
	
	@Inject
	MarkerController markerController;

	@Execute
	protected void execute(EPartService partService, @Active MHandledToolItem item, @Optional Viewer2DScene scene) {

		if (scene != null) {
			scene.setAllowNewPoint(item.isSelected());
		} else {
			// Old xml file format
			MPart part = partService.getActivePart();
			Object obj = part.getObject();

			if (obj instanceof IWaterColumn2DViewer) {
				((IWaterColumn2DViewer) obj).setAllowNewPoint(item.isSelected());
			}
		}
		
		// Open marker editor
		if(item.isSelected()) {
			markerController.openMarkerEditor();
		}

		if (Activator.get2DPreferences().isSinglePointingModeEnabled()) {
			Object o = item.getWidget();
			if (o instanceof ToolItem) {
				((ToolItem) o).setSelection(false);
			}
		}
	}
}
