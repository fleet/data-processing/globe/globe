package fr.ifremer.globe.ui.viewer2d.handlers;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;

import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;

public class Viewer2DShowGridHandler {

	@Execute
	protected void execute(Viewer2DScene scene, IEclipseContext context) {
		Boolean drawGrid = (Boolean) context.get(ContextNames.DRAW_GRID);
		// Swith preference
		Activator.get2DPreferences().setDrawMetrics(!drawGrid);
		Activator.get2DPreferences().save();
		// Notify all components
		context.set(ContextNames.DRAW_GRID, !drawGrid);

		// Refresh the view
		scene.refresh();
	}

}
