package fr.ifremer.globe.ui.viewer2d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.ui.viewer2d.view.utils.ScreenshotTool;
import fr.ifremer.globe.ui.viewer2d.xml.IWaterColumn2DViewer;

public class Viewer2DScreenshotHandler {

	@Execute
	protected void execute(EPartService partService, @Optional ScreenshotTool screenshotTool) {
		if (screenshotTool != null) {
			screenshotTool.doScreenShot();
		} else {
			// Old xml file format
			MPart part = partService.getActivePart();
			Object obj = part.getObject();

			if (obj instanceof IWaterColumn2DViewer) {
				((IWaterColumn2DViewer) obj).takeScreenshot();
			}
		}
	}
}