package fr.ifremer.globe.ui.viewer2d.handlers;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.view.wc.ZDetectionComponent;

/** Handler managing the Z detection visibility */
public class Viewer2DShowZDetectionHandler {

	/** ZDetectionComponent if deployed in the context */
	@Optional
	@Inject
	private ZDetectionComponent zDetectionComponent;

	/** Hide the menu item when ZDetectionComponent is not in the context */
	@CanExecute
	private boolean canExecute(MToolItem item) {
		if (zDetectionComponent == null) {
			item.getParent().getChildren().forEach(element -> {
				if (element.getElementId().contains("zdetection"))
					element.setVisible(false);
			});
		}
		return zDetectionComponent != null;
	}

	/** Hide/Show the z-detection points */
	@Execute
	private void execute(Viewer2DScene scene, IEclipseContext context) {
		// Swith preference
		boolean drawZDetection = !zDetectionComponent.isVisible();
		Activator.get2DPreferences().setDrawZDetection(drawZDetection);
		Activator.get2DPreferences().save();

		zDetectionComponent.setVisible(drawZDetection);
		scene.refresh();
	}
}
