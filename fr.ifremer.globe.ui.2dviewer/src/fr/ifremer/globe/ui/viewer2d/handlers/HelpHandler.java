package fr.ifremer.globe.ui.viewer2d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.ui.utils.Messages;

public class HelpHandler {

	@Execute
	public void execute(EPartService partService) {

		StringBuilder sb = new StringBuilder();

		sb.append("2D Viewer commands :\n\n");

		sb.append("Space : play / pause\n\n");
		sb.append("CTRL + Space : reverse play\n\n");
		sb.append("Right arrow : next\n\n");
		sb.append("Left arrow : previous\n\n");
		sb.append("CTRL + S : screenshot\n\n");
		sb.append("Del (or Suppr) : delete selected marker\n");

		// Load new NVI files
		Messages.openInfoMessage("Viewer 2D help", sb.toString());
	}

}
