package fr.ifremer.globe.ui.viewer2d.handlers;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ListDialog;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.ui.viewer2d.view.Viewer2D;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DBuilder;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode.CheckState;
import fr.ifremer.globe.ui.widget.player.IndexPlayerGetterOperation;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.viewer3d.layers.wc.NativeWCVolumicLayer;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import gov.nasa.worldwind.layers.Layer;

/**
 * Handler used to open the water column 2D view.
 */
public class Viewer2DHandler extends AbstractNodeHandler {

	public static final String xmlWcPartId = "fr.ifremer.globe.ui.2dviewer.xml.partdescriptor";
	private static int xmlWcIndex = 0;

	/** Menu type **/
	public static final String WC_TYPE = "WC";
	public static final String TEXTURE_TYPE = "Texture";
	public static final String FLATTEN_WC_TYPE = "WC_flatten";

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement uiElement,
			@Named("fr.ifremer.globe.ui.2dviewer.commandparameter.viewtype") String type) {
		Optional<FileInfoNode> optFileInfoNode = getUniqueFileInfoNode(selectionService);
		ContentType fileContentType = ContentType.UNDEFINED;
		IFileInfo fileInfo = null;
		if (optFileInfoNode.isPresent()) {
			fileInfo = optFileInfoNode.get().getFileInfo();
			fileContentType = fileInfo.getContentType();
		}

		boolean result = WC_TYPE.equals(type) && fileContentType == ContentType.OLD_ECHOGRAM_XML //
				|| TEXTURE_TYPE.equals(type) && fileContentType.equals(ContentType.G3D_NETCDF_4) //
				|| TEXTURE_TYPE.equals(type) && fileContentType.equals(ContentType.SEGY);

		if (!result && fileInfo instanceof ISounderNcInfo sounderNcInfo) {
			// WC for XSF or Sonar Nc file
			if (WC_TYPE.equals(type) || FLATTEN_WC_TYPE.equals(type)) {
				result = sounderNcInfo.getBeamGroupCount() > 0;
			}
			// Texture for Sonar Nc file
			if (TEXTURE_TYPE.equals(type)) {
				result = !sounderNcInfo.getGridGroupNames().isEmpty() // Texture from Grid_data
						|| sounderNcInfo.getWcRxBeamCount() == 1; // Texture from Beam_data
			}
		}
		uiElement.setVisible(result);
		return result;
	}

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application,
			MWindow window, EModelService modelService,
			@Named("fr.ifremer.globe.ui.2dviewer.commandparameter.viewtype") String type,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {

		// get selected FileInfoNode
		getUniqueFileInfoNode(selectionService).ifPresent( //
				fileInfoNode -> {
					ContentType fileContentType = fileInfoNode.getFileInfo().getContentType();
					switch (type) {
					case WC_TYPE:
						if (ContentType.OLD_ECHOGRAM_XML.equals(fileContentType)) {
							executeWc4Xml(partService, application, window, modelService, fileInfoNode);
						} else if (fileContentType.isOneOf(ContentType.XSF_NETCDF_4, ContentType.SONAR_NETCDF_4)) {
							executeWc4Xsf(partService, application, modelService, fileInfoNode);
						}
						break;
					case TEXTURE_TYPE, FLATTEN_WC_TYPE:
						if (fileContentType.isOneOf(ContentType.G3D_NETCDF_4, ContentType.XSF_NETCDF_4,
								ContentType.SONAR_NETCDF_4, ContentType.SEGY)) {
							executeTextureLayer(partService, application, shell, modelService, fileInfoNode);
						}
						break;
					default:
						break;
					}
				});
	}

	public void executeWc4Xml(EPartService partService, MApplication application, MWindow window,
			EModelService modelService, FileInfoNode selectedNode) {
		List<TreeNode> n = selectedNode.getChildren();
		Layer l = ((LayerNode) n.get(0)).getLayer().get();
		if (l instanceof WaterColumnLayer == false) {
			return;
		}

		int partIndex = xmlWcIndex;
		xmlWcIndex++;

		window.getContext().set(WaterColumnLayer.class, (WaterColumnLayer) l);
		window.getContext().set(ContextNames.WINDOW_ID, String.valueOf(partIndex));
		window.getContext().set(ContextNames.PLAYER_BEAN_NAME, selectedNode.getName());

		MPart part = partService.createPart(xmlWcPartId);
		part.setLabel(partIndex + " - " + selectedNode.getName());
		part.setElementId(xmlWcPartId + "." + xmlWcIndex);
		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, part);

	}

	public void executeWc4Xsf(EPartService partService, MApplication application, EModelService modelService,
			FileInfoNode selectedNode) {
		// Search WC layer
		for (TreeNode treeNode : selectedNode.getChildren()) {
			if (treeNode.getState() == CheckState.TRUE && treeNode instanceof LayerNode layerNode
					&& layerNode.getLayer().isPresent()) {
				Layer l = layerNode.getLayer().get();
				if (l instanceof NativeWCVolumicLayer) {
					executeWc4Xsf((NativeWCVolumicLayer) l, partService, application, modelService, selectedNode);
				} else if (l instanceof WCVolumicLayer) {
					executeWc4Xsf((WCVolumicLayer) l, partService, application, modelService, selectedNode);
				}
			}
		}
	}

	private void executeWc4Xsf(WCVolumicLayer wcVolumicLayer, EPartService partService, MApplication application,
			EModelService modelService, FileInfoNode selectedNode) {

		int partIndex = xmlWcIndex;
		xmlWcIndex++;

		// Initialize the part
		MPart part = partService.createPart(Viewer2D.PART_ID);
		part.setLabel(partIndex + " - " + selectedNode.getName());
		part.setElementId(Viewer2D.PART_ID + "." + xmlWcIndex);
		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		partService.showPart(part, PartState.CREATE); // Initialize the Context

		// Build the view

		new Viewer2DBuilder(selectedNode.getFileInfo())//
				.withPart(part)//
				.withWaterColumnVolumicLayer(wcVolumicLayer)//
				.build(partService);

		partService.showPart(part, PartState.VISIBLE);

		// call a first refresh
		wcVolumicLayer.getPlayer().notifyIndex();
	}

	private void executeWc4Xsf(NativeWCVolumicLayer wcVolumicLayer, EPartService partService, MApplication application,
			EModelService modelService, FileInfoNode selectedNode) {

		int partIndex = xmlWcIndex;
		xmlWcIndex++;

		// Initialize the part
		MPart part = partService.createPart(Viewer2D.PART_ID);
		part.setLabel(partIndex + " - " + selectedNode.getName());
		part.setElementId(Viewer2D.PART_ID + "." + xmlWcIndex);
		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		partService.showPart(part, PartState.CREATE); // Initialize the Context

		// Build the view

		new Viewer2DBuilder(selectedNode.getFileInfo())//
				.withPart(part)//
				.withNativeWaterColumnVolumicLayer(wcVolumicLayer)//
				.build(partService);

		partService.showPart(part, PartState.VISIBLE);

		// call a first refresh
		wcVolumicLayer.getPlayer().notifyIndex();
	}

	public void executeTextureLayer(EPartService partService, MApplication application, Shell shell,
			EModelService modelService, FileInfoNode selectedNode) {
		// get Texture layer
		var wWTextureLayers = selectedNode.getChildren().stream() //
				.filter(LayerNode.class::isInstance).map(node -> ((LayerNode) node).getLayer().get()) // get layer node
				.filter(IWWTextureLayer.class::isInstance).map(IWWTextureLayer.class::cast).toList();

		List<IWWTextureLayer> textureLayersToOpen = null;
		if (wWTextureLayers.size() == 1) {
			textureLayersToOpen = wWTextureLayers;
		} else if (wWTextureLayers.size() > 1) {
			ListDialog dialog = new ListDialog(shell) {
				@Override
				public void create() {
					super.create();
					var initialSelection = getInitialElementSelections();
					if (initialSelection != null && !initialSelection.isEmpty()) {
						getTableViewer().reveal(initialSelection.get(0));
					}
				}

				@Override
				protected int getTableStyle() {
					return SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER;
				}

			};
			dialog.setContentProvider(ArrayContentProvider.getInstance());
			dialog.setLabelProvider(new LabelProvider() {

				/** {@inheritDoc} */
				@Override
				public String getText(Object element) {
					return ((IWWTextureLayer) element).getName();
				}
			});
			dialog.setInput(wWTextureLayers);
			dialog.setTitle("Open 2D viewer");
			dialog.setMessage("Select the layers to open");
			dialog.setInitialSelections(
					wWTextureLayers.stream().filter(IWWTextureLayer::isEnabled).collect(Collectors.toList()).toArray());
			if (dialog.open() == Window.OK && dialog.getResult().length > 0) {
				textureLayersToOpen = Arrays.stream(dialog.getResult()).map(IWWTextureLayer.class::cast)
						.collect(Collectors.toList());
			}
		}

		if (textureLayersToOpen != null) {
			for (IWWTextureLayer textureLayerToOpen : textureLayersToOpen) {
				showPartForTextureLayer(partService, application, modelService, selectedNode, textureLayerToOpen);
			}
		}
	}

	/**
	 * Open a Viewer2D part for the specified texture layer
	 */
	protected void showPartForTextureLayer(EPartService partService, MApplication application,
			EModelService modelService, FileInfoNode selectedNode, IWWTextureLayer textureLayerToOpen) {
		xmlWcIndex++;

		// Initialize the part
		MPart part = partService.createPart(Viewer2D.PART_ID);
		part.setLabel(textureLayerToOpen.getName());
		part.setElementId(Viewer2D.PART_ID + "." + xmlWcIndex);
		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		partService.showPart(part, PartState.CREATE); // Initialize the Context
		// Build the view
		new Viewer2DBuilder(selectedNode.getFileInfo())//
				.withPart(part)//
				.withTextureLayer(textureLayerToOpen)//
				.build(partService);

		partService.showPart(part, PartState.VISIBLE);

		// if texture is not yet built : ask for it
		if (textureLayerToOpen.getTextureData() == null) {
			textureLayerToOpen.buildTextureRenderable();
		}

		// call a first refresh
		textureLayerToOpen.perform(new IndexPlayerGetterOperation()).getResult().ifPresent(IndexedPlayer::notifyIndex);
	}

	/** {@inheritDoc} */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return false;
	}
}