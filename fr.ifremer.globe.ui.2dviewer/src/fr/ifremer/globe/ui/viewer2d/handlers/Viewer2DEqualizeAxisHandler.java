/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewer2d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;

import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;

/**
 * Handler to invoke the equalization of the axes of the scene
 */
public class Viewer2DEqualizeAxisHandler {
	@Execute
	protected void execute(Viewer2DScene scene) {
		// Swith preference
		Activator.get2DPreferences().setEqualizeAxis(!scene.isEqualizeAxis());
		Activator.get2DPreferences().save();
		// Notify all components
		scene.setEqualizeAxis(!scene.isEqualizeAxis());
		scene.refresh();
	}
}
