package fr.ifremer.globe.ui.viewer2d.handlers;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.view.wc.ZDetectionComponent;
import fr.ifremer.globe.ui.widget.color.ColorPicker;

/** Tool control to edit the Z detection color */
public class ZDetectionColorHandler {

	@Optional
	@Inject
	/** ZDetectionComponent if deployed in the context */
	private ZDetectionComponent zDetectionComponent;

	@Inject
	@Optional
	/** Scene when available */
	private Viewer2DScene scene;

	@PostConstruct
	public void createControls(Composite parent) {
		Composite cmp = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		cmp.setLayout(layout);

		var navLineColorPicker = new ColorPicker(cmp, SWT.BORDER, Activator.get2DPreferences().getZDetectionColor(),
				this::onColorChanged);
		navLineColorPicker.setToolTipText("Color of Z Detection");
		navLineColorPicker.setLayoutData(GridDataFactory.fillDefaults().hint(15, SWT.DEFAULT).create());
	}

	/** Point size changed */
	private void onColorChanged(GColor color) {
		Activator.get2DPreferences().setZDetectionColor(color);
		Activator.get2DPreferences().save();

		if (zDetectionComponent != null) {
			zDetectionComponent.setColor(color);
			if (scene != null)
				scene.refresh();
		}
	}
}