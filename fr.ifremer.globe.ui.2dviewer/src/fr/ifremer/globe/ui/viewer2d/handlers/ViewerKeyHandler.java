package fr.ifremer.globe.ui.viewer2d.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.ui.viewer2d.view.components.MarkersComponent;
import fr.ifremer.globe.ui.viewer2d.view.scene.Viewer2DScene;
import fr.ifremer.globe.ui.viewer2d.xml.WaterColumn2DViewer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;

public class ViewerKeyHandler {
	@Execute
	protected void execute(EPartService partService,
			@Named("fr.ifremer.globe.ui.2dviewer.commandparameter.keyPressed") String action,
			@Optional IndexedPlayer player, //
			@Optional Viewer2DScene scene, //
			@Optional MarkersComponent markersComponent) {
		if (player != null) {
			switch (action) {
			case "next":
				player.stepForward();
				break;
			case "previous":
				player.stepBackward();
				break;
			case "play":
				player.playPause();
				break;
			case "reverseplay":
				player.revertPlayPause();
				break;
			case "delete":
				if (markersComponent != null) {
					markersComponent.deleteSelectedMarker();
					if (scene != null) {
						scene.refresh();
					}
				}
				break;
			default:
			}
		} else {
			// Old xml file format
			MPart part = partService.getActivePart();
			Object obj = part.getObject();

			if (obj instanceof WaterColumn2DViewer) {
				((WaterColumn2DViewer) obj).handleKeyPress(action);
			}
		}
	}
}
