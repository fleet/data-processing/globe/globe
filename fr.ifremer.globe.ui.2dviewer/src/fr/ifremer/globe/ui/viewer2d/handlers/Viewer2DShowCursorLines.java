package fr.ifremer.globe.ui.viewer2d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.preferences.Viewer2DPreferences;

public class Viewer2DShowCursorLines {
	@Execute
	protected void execute(EPartService partService) {
		Viewer2DPreferences prefs = Activator.get2DPreferences();
		prefs.setDrawCursorMetrics(!prefs.getDrawCursorMetrics());
	}
}
