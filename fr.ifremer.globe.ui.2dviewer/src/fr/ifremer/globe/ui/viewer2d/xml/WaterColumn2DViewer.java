/**
 *
 */
package fr.ifremer.globe.ui.viewer2d.xml;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.viewer2d.context.ContextNames;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.markers.MarkerController;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import fr.ifremer.viewer3d.model.IPlayerSynchronizable;
import fr.ifremer.viewer3d.model.PlayerBean;
import gov.nasa.worldwind.avlist.AVKey;

/**
 * 2D viewer of water column for old xml file format
 */
public class WaterColumn2DViewer implements IWaterColumn2DViewer, IPlayerSynchronizable, PropertyChangeListener {

	private WaterColumn2DScene scene;
	private PlayerBean playerBean = new PlayerBean();
	private boolean isPlayerSynchronized = true;
	private String windowID;
	private WaterColumnLayer layer;
	private WC2DStatusBar statusBar;

	@Inject
	private MPart part;

	@Inject
	EPartService partService;
	@Inject
	MarkerController markerController;

	@Inject
	protected WWFileLayerStoreModel fileLayerStoreModel;
	protected Consumer<WWFileLayerStoreEvent> fileLayerStoreModelListener = this::onFileLayerStoreEvent;

	@Inject
	public WaterColumn2DViewer(WaterColumnLayer l, @Named(ContextNames.PLAYER_BEAN_NAME) String playerBeanName,
			@Named(ContextNames.WINDOW_ID) String id) {
		playerBean.setName(playerBeanName);
		windowID = id;
		layer = l;
		layer.addPropertyChangeListener(this);
		resetPlayerBean();
		playerBean.addPropertyChangeListener(this);
		getMasterPlayerBean().addPropertyChangeListener(this);
	}

	@PostConstruct
	private void postConstruct() {
		fileLayerStoreModel.addListener(fileLayerStoreModelListener);
	}

	@PreDestroy
	private void dispose() {
		layer.removePropertyChangeListener(this);
		fileLayerStoreModel.removeListener(fileLayerStoreModelListener);
	}

	@Override
	public void setWindowID(String windowID) {
		this.windowID = windowID;
	}

	@Override
	public String getWindowID() {
		return windowID;
	}

	@Override
	public PlayerBean getPlayerBean() {
		return playerBean;
	}

	public PlayerBean getActivePlayerBean() {
		if (isPlayerSynchronized()) {
			return getMasterPlayerBean();
		}
		return getPlayerBean();
	}

	@Override
	public PlayerBean getMasterPlayerBean() {
		return layer.getPlayerBean();
	}

	@Override
	public boolean isPlayerSynchronized() {
		return isPlayerSynchronized;
	}

	@PostConstruct
	public void createUI(Composite parent) throws GIOException {
		final Composite composite = new Composite(parent, SWT.EMBEDDED);
		FormData data = new FormData();
		data.top = UIUtils.FORM_ATTACHMENT_0_0;
		data.bottom = UIUtils.FORM_ATTACHMENT_100_0;
		data.left = UIUtils.FORM_ATTACHMENT_0_0;
		data.right = UIUtils.FORM_ATTACHMENT_100_0;
		composite.setLayoutData(data);

		GLProfile glprofile = GLProfile.getDefault();
		GLCapabilities caps2 = new GLCapabilities(glprofile);
		GLCanvas canvas = new GLCanvas(caps2);
		Frame frame = SWT_AWT.new_Frame(composite);
		frame.add(canvas, BorderLayout.CENTER);

		UIUtils.bindFocus(canvas, partService, part);

		scene = new WaterColumn2DScene(layer, canvas, markerController, fileLayerStoreModel);
		canvas.addGLEventListener(scene);
		canvas.addMouseListener(scene);
		canvas.addMouseMotionListener(scene);
		canvas.addMouseWheelListener(scene);

		statusBar = new WC2DStatusBar(layer);
		frame.add(statusBar, BorderLayout.SOUTH);
		statusBar.update(getActivePlayerBean().getPlayNumber());
		scene.refresh(getActivePlayerBean().getPlayNumber());
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String name = event.getPropertyName();
		if (PlayerBean.PROPERTY_PLAY_NUMBER.equals(name)) {
			scene.refresh((Integer) event.getNewValue());
			statusBar.update((Integer) event.getNewValue());
		} else if (AVKey.LAYER.equals(name)) {
			scene.refresh();
		}
	}

	private void resetPlayerBean() {
		playerBean.setIndexList(getMasterPlayerBean().getIndexList());
		playerBean.setPlayIndex(getMasterPlayerBean().getPlayIndex());
		playerBean.setPlayNumber(getMasterPlayerBean().getPlayNumber());
		playerBean.setMaxPlayIndex(getMasterPlayerBean().getMaxPlayIndex());
		playerBean.setMinPlayIndex(getMasterPlayerBean().getMinPlayIndex());
		playerBean.setMaxPlayNumber(getMasterPlayerBean().getMaxPlayNumber());
		playerBean.setMinPlayNumber(getMasterPlayerBean().getMinPlayNumber());
	}

	@Override
	public void takeScreenshot() {
		scene.takeScreenshot();
	}

	@Override
	public void resetView() {
		scene.resetView();
	}

	@Override
	public void setAllowNewPoint(boolean b) {
		scene.setAllowNewPoint(b);
	}

	@Override
	public void handleKeyPress(String action) {
		switch (action) {
		case "next":
			getActivePlayerBean().stepForward();
			break;
		case "previous":
			getActivePlayerBean().stepBackward();
			break;
		case "play":
			getActivePlayerBean().playPause();
			break;
		case "reverseplay":
			getActivePlayerBean().revertPlayPause();
			break;
		case "delete":
			scene.getWall().deleteSelectedMarker();
			break;
		default:
			System.out.println("unhandled key event action : " + action);
		}
	}

	/** React on a layer model changes */
	protected void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		if (event.getState() == FileLayerStoreState.REMOVED && event.getFileLayerStore().getLayers().contains(layer)) {
			Display.getDefault().asyncExec(() -> partService.hidePart(part));
		}
	}

}
