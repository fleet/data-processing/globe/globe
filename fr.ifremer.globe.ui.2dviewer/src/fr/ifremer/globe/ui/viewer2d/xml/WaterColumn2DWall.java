package fr.ifremer.globe.ui.viewer2d.xml;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.swing.SwingUtilities;

import org.eclipse.core.databinding.observable.IChangeListener;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.awt.TextRenderer;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureCoords;

import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ogl.AbstractGLComponent;
import fr.ifremer.globe.ogl.util.CoordinatesProjection;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.utils.PositionUtils;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.preferences.Viewer2DPreferences;
import fr.ifremer.globe.ui.viewer2d.view.menu.MarkerMenu;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.opengl.OpenGLUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.markers.MarkerController;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;
import fr.ifremer.viewer3d.layers.markers.WWWaterColumnMarkerLayer;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import fr.ifremer.viewer3d.model.watercolumn.WaterColumnData;
import fr.ifremer.viewer3d.util.TextureLoader;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

public class WaterColumn2DWall extends AbstractGLComponent {

	private enum Mode {
		NEW_POINT, SELECTION, ALLOW_DRAGGING, DRAGGING_POINT, DRAGGING_SCENE
	}

	private final static double DEFAULT_ZOOM = 1.1;

	private Texture texture;
	private TextureLoader textureCache = new TextureLoader(100);
	private WaterColumnLayer layer;
	private WaterColumnData wcdata;

	/** Marker controller **/
	protected MarkerController markerController;
	/** List of {@link IWCMarker} currently displayed */
	protected List<IWCMarker> makers = List.of();
	/** List of {@link WWMarkerLayer} & theirs listener **/
	protected List<WWWaterColumnMarkerLayer> markerLayers = new ArrayList<>();
	protected final IChangeListener markerListener = evt -> updateMarkers();
	protected final PropertyChangeListener enableLayerListener = evt -> updateMarkers();

	/** Canvas **/
	protected GLCanvas canvas;
	/** File layer model **/
	protected WWFileLayerStoreModel fileLayerStoreModel;
	protected final Consumer<WWFileLayerStoreEvent> fileLayerStoreModelListener = evt -> updateMarkerListeners();

	private int currentPingNumber = 1;
	private boolean allowNewPoint = false;

	private double sceneCoords[] = new double[4];
	private double screenCoords[] = new double[4];
	private double zoom = DEFAULT_ZOOM;
	private double xtranslation = 0.0;
	private double ytranslation = 0.0;
	private double savedxtranslation = 0.0;
	private double savedytranslation = 0.0;

	private int clickedX;
	private int clickedY;
	private int x;
	private int y;
	private Mode mode;
	private Optional<IWCMarker> selectedPoint = Optional.empty();
	private Optional<IWCMarker> currentPoint = Optional.empty();

	// scene size and origin on screen screenCoords
	private double sceneW;
	private double sceneH;
	private double w0;
	private double h0;

	private Viewer2DPreferences pref = null;

	public WaterColumn2DWall(WaterColumnLayer layer, MarkerController markerController,
			WWFileLayerStoreModel fileLayerStoreModel, GLCanvas canvas) {
		this.layer = layer;
		this.markerController = markerController;
		this.fileLayerStoreModel = fileLayerStoreModel;
		this.canvas = canvas;
		wcdata = layer.getDatas();

		pref = Activator.get2DPreferences();
		fileLayerStoreModel.addListener(fileLayerStoreModelListener);
		updateMarkerListeners();
	}

	/**
	 * Dispose OpenGL resources
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		// Remove all listeners
		updateMarkerListeners(List.of());

		textureCache.dispose(glad.getGL());
		fileLayerStoreModel.removeListener(fileLayerStoreModelListener);
	}

	@Override
	public void draw(GLAutoDrawable glad) {
		GL2 gl = glad.getGL().getGL2();

		gl.glPushMatrix();

		// load texture
		int pingIndex = wcdata.getIndexForPing(currentPingNumber);
		String name = wcdata.getImgList().get(pingIndex);

		try {
			texture = textureCache.getTexture(gl, name);
		} catch (IOException e) {
			texture = null;
		}

		if (texture != null) {

			gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT
					| GL2.GL_TEXTURE_BIT | GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

			gl.glEnable(GL.GL_BLEND);
			gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

			keepAspectRatio(glad);

			// render texture on a quad
			gl.glActiveTexture(GL.GL_TEXTURE0);
			texture.bind(gl);
			gl.glEnable(GL.GL_TEXTURE_2D);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);

			double z = 1.0;
			double sizex = 1.0;
			double sizey = 1.0;

			// draw water column
			TextureCoords coords = texture.getImageTexCoords();
			gl.glBegin(GL2.GL_QUADS);

			gl.glTexCoord2f(coords.right(), coords.bottom());
			gl.glVertex3d(sizex, -sizey, z);

			gl.glTexCoord2f(coords.right(), coords.top());
			gl.glVertex3d(sizex, sizey, z);

			gl.glTexCoord2f(coords.left(), coords.top());
			gl.glVertex3d(-sizex, sizey, z);

			gl.glTexCoord2f(coords.left(), coords.bottom());
			gl.glVertex3d(-sizex, -sizey, z);

			gl.glEnd();

			gl.glPopAttrib();

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glDisable(GL.GL_TEXTURE_2D);
		}

		gl.glPopMatrix();
	}

	private void keepAspectRatio(GLAutoDrawable glad) {

		if (texture == null) {
			return;
		}

		GL2 gl = glad.getGL().getGL2();

		// keep aspect ratio
		double windowRatio = glad.getSurfaceWidth() / (double) glad.getSurfaceHeight();
		double ratiox = 1.0;
		double ratioy = 1.0;
		if (windowRatio < texture.getAspectRatio()) {
			ratioy = texture.getWidth() / (double) texture.getHeight() / windowRatio;
		} else {
			ratiox = texture.getHeight() / (double) texture.getWidth() * windowRatio;
		}
		gl.glOrtho(-ratiox * zoom, ratiox * zoom, -ratioy * zoom, ratioy * zoom, -1, 1);
		gl.glTranslated(xtranslation, ytranslation, 0.0);
		double w = glad.getSurfaceWidth();
		double h = glad.getSurfaceHeight();
		sceneW = w / (ratiox * zoom);
		sceneH = h / (ratioy * zoom);
		w0 = (w - sceneW) / 2 + sceneW / 2 * xtranslation;
		h0 = h - (h - sceneH) / 2 + sceneH / 2 * ytranslation;
	}

	/**
	 * draw bounding box, POI and cursor metrics
	 */

	public void drawPoints(GLAutoDrawable glad) {
		GL2 gl = glad.getGL().getGL2();
		gl.glPushMatrix();
		keepAspectRatio(glad);

		// update current point
		currentPoint = findNearestPoint(glad, x, y);

		if (mode == Mode.NEW_POINT) {
			handleNewPoint(glad);
			mode = null;
		} else if (mode == Mode.DRAGGING_POINT) {
			handlePointDrag(glad);
		} else if (mode == Mode.DRAGGING_SCENE) {
			handleSceneDrag(glad);
		}
		int pingIndex = wcdata.getIndexForPing(currentPingNumber);

		if (!makers.isEmpty()) {
			int markerSize = pref.getMarkerSize();
			int selectedMarkerSize = (int) (markerSize * 1.5);
			gl.glEnable(GL2.GL_POINT_SMOOTH);

			gl.glPointSize(selectedMarkerSize);
			GColor pointSelectionColor = fr.ifremer.viewer3d.Activator.getPluginParameters().getPointSelectionColor();
			OpenGLUtils.setAsGLColor(gl, pointSelectionColor.getRed(), pointSelectionColor.getGreen(),
					pointSelectionColor.getBlue());

			// draw mouseover circle
			if (currentPoint.isPresent()) {
				gl.glBegin(GL2.GL_POINTS);
				fromGeographicToScene(PositionUtils.getPosition(currentPoint.get()), sceneCoords);
				gl.glVertex3d(sceneCoords[0], sceneCoords[1], 1.0);
				gl.glEnd();
			}

			// draw selection circle
			if (selectedPoint.isPresent() && mode != null && selectedPoint.get().getPing() == currentPingNumber) {
				canvas.requestFocus(); // Useful to activate the key bindings
				gl.glBegin(GL2.GL_POINTS);
				fromGeographicToScene(PositionUtils.getPosition(selectedPoint.get()), sceneCoords);
				gl.glVertex3d(sceneCoords[0], sceneCoords[1], 1.0);
				gl.glEnd();
			}

			// draw all points
			gl.glPointSize(markerSize);
			gl.glBegin(GL2.GL_POINTS);
			for (IWCMarker marker : makers) {
				fromGeographicToScene(PositionUtils.getPosition(marker), sceneCoords);
				GColor color = marker.getColor();
				gl.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());
				gl.glVertex3d(sceneCoords[0], sceneCoords[1], 1.0);
			}
			gl.glEnd();
			gl.glDisable(GL2.GL_POINT_SMOOTH);
		}
		
		// draw limits
		if (pref.getDrawLimits()) {
			gl.glColor4d(1, 1, 1, 1);
			gl.glBegin(GL2.GL_LINE_LOOP);
			double topElevation = wcdata.getTopElevation(pingIndex);
			double bottomElevation = wcdata.getBottomElevation(pingIndex);
			fromGeographicToScene(wcdata.getLatlon1(pingIndex), topElevation, sceneCoords);
			gl.glVertex3d(sceneCoords[0], sceneCoords[1], 1.0);
			fromGeographicToScene(wcdata.getLatlon2(pingIndex), topElevation, sceneCoords);
			gl.glVertex3d(sceneCoords[0], sceneCoords[1], 1.0);
			fromGeographicToScene(wcdata.getLatlon2(pingIndex), bottomElevation, sceneCoords);
			gl.glVertex3d(sceneCoords[0], sceneCoords[1], 1.0);
			fromGeographicToScene(wcdata.getLatlon1(pingIndex), bottomElevation, sceneCoords);
			gl.glVertex3d(sceneCoords[0], sceneCoords[1], 1.0);
			gl.glEnd();

		}

		if (pref.getDrawCursorMetrics()) {
			// metrics initialization
			double topElevation = wcdata.getTopElevation(pingIndex);
			double bottomElevation = wcdata.getBottomElevation(pingIndex);

			double geoWidth = LatLon.ellipsoidalDistance(wcdata.getLatlon1(pingIndex), wcdata.getLatlon2(pingIndex),
					Ellipsoid.WGS84.getHalf_A(), Ellipsoid.WGS84.getHalf_B());
			double depthScale = (bottomElevation - topElevation) / 2;

			// draw Cursor metrics
			if (pref.getDrawCursorMetrics()) {
				fromScreenToScene(glad, x, y, sceneCoords);
				if (correctCoords(sceneCoords)) {

					// lines
					double sx = sceneCoords[0];
					double sy = sceneCoords[1];
					gl.glColor3d(0, 1, 0);
					gl.glBegin(GL2.GL_LINE_STRIP);
					fromGeographicToScene(wcdata.getLatlon1(pingIndex), topElevation, sceneCoords);
					gl.glVertex3d(sx, sceneCoords[1], 1.0);
					fromGeographicToScene(wcdata.getLatlon1(pingIndex), bottomElevation, sceneCoords);
					gl.glVertex3d(sx, sceneCoords[1], 1.0);
					gl.glEnd();
					gl.glBegin(GL2.GL_LINE_STRIP);
					fromGeographicToScene(wcdata.getLatlon1(pingIndex), 0, sceneCoords);
					gl.glVertex3d(sceneCoords[0], sy, 1.0);
					fromGeographicToScene(wcdata.getLatlon2(pingIndex), 0, sceneCoords);
					gl.glVertex3d(sceneCoords[0], sy, 1.0);
					gl.glEnd();

					// Text
					TextRenderer gltext = getRenderer();
					gltext.beginRendering(glad.getSurfaceWidth(), glad.getSurfaceHeight());
					gltext.draw("cursor depth = " + (int) (topElevation + (-sy + 1) * depthScale)
							+ " m, horizontal length=  " + (int) (sx * geoWidth / 2) + " m", 10, 10);
					gltext.endRendering();
				}
			}
		}
		
		gl.glPopMatrix();
	}

	/**
	 * draw metrics grid
	 *
	 * @param glad
	 */
	public void drawMetrics(GLAutoDrawable glad) {

		GL2 gl = glad.getGL().getGL2();
		gl.glPushMatrix();
		keepAspectRatio(glad);

		if (pref.getDrawMetrics()) {
			// metrics initialization
			int index = wcdata.getIndexForPing(currentPingNumber);
			double topElevation = wcdata.getTopElevation(index);
			double bottomElevation = wcdata.getBottomElevation(index);

			double geoWidth = LatLon.ellipsoidalDistance(wcdata.getLatlon1(index), wcdata.getLatlon2(index),
					Ellipsoid.WGS84.getHalf_A(), Ellipsoid.WGS84.getHalf_B());
			double depthScale = (bottomElevation - topElevation) / 2;

			fromGeographicToScene(wcdata.getLatlon1(index), bottomElevation, sceneCoords);
			double x0 = sceneCoords[0];
			double y0 = sceneCoords[1];
			fromGeographicToScene(wcdata.getLatlon2(index), topElevation, sceneCoords);
			double w = sceneCoords[0] - x0;
			double h = sceneCoords[1] - y0;
			double div = 4;

			// metrics drawing
			gl.glColor3d(1, 1, 1);
			if (pref.getDrawMetrics()) {
				for (int i = 0; i <= div; i++) {
					gl.glBegin(GL2.GL_LINE_STRIP);
					gl.glVertex3d(x0, y0 + h * i / div, 1.0);
					gl.glVertex3d(x0 + w, y0 + h * i / div, 1.0);
					gl.glEnd();
					draw2DStringOnScene(glad, (int) (topElevation + depthScale * 2 * (1 - i / div)) + " m", x0,
							y0 + h * i / div, 32, 4);
					gl.glBegin(GL2.GL_LINE_STRIP);
					gl.glVertex3d(x0 + h * i / div, y0, 1.0);
					gl.glVertex3d(x0 + h * i / div, y0 + h, 1.0);
					gl.glEnd();
					draw2DStringOnScene(glad, (int) (geoWidth * ((i / div) - 0.5)) + " m", x0 + h * i / div, y0, 4, 14);
				}
			}

		}
		gl.glPopMatrix();
	}

	/**
	 * display 2D string on scene coordinate (with zoom and translation correction) with screen coordinate offset
	 * (dx,dy).
	 */
	private void draw2DStringOnScene(GLAutoDrawable glad, String text, double x, double y, int dx, int dy) {
		TextRenderer gltext = getRenderer();
		gltext.beginRendering(glad.getSurfaceWidth(), glad.getSurfaceHeight());
		gltext.draw(text, (int) (w0 + sceneW / 2 * (x + 1)) - dx, (int) (h0 + sceneH / 2 * (y - 1)) - dy);
		gltext.endRendering();
	}

	private void handleSceneDrag(GLAutoDrawable glad) {
		fromScreenToScene(glad, x, y, sceneCoords);
		fromScreenToScene(glad, clickedX, clickedY, screenCoords);
		xtranslation = savedxtranslation + sceneCoords[0] - screenCoords[0];
		ytranslation = savedytranslation + sceneCoords[1] - screenCoords[1];
	}

	private void handlePointDrag(GLAutoDrawable glad) {
		if (selectedPoint.isPresent()) {
			fromScreenToScene(glad, x, y, sceneCoords);
			if (!correctCoords(sceneCoords)) {
				return;
			}
			Position position = fromSceneToGeographic(sceneCoords);
			PositionUtils.setPosition(selectedPoint.get(), position);
			Viewer3D.refreshRequired();
		}
	}

	private void handleNewPoint(GLAutoDrawable glad) {
		if (allowNewPoint) {
			// transform coords to -1, 1 in the opengl scene
			fromScreenToScene(glad, x, y, sceneCoords);
			if (!correctCoords(sceneCoords)) {
				return;
			}

			// create position in geographic view from coords
			Position pos = fromSceneToGeographic(sceneCoords);
			int pingIndex = wcdata.getIndexForPing(currentPingNumber);
			Instant date = Instant.ofEpochMilli(wcdata.getTime(pingIndex));
			markerController.addNewWCMarker(pos, layer.getName(), currentPingNumber, Optional.of(date));
			updateMarkers();

			if (Activator.get2DPreferences().isSinglePointingModeEnabled()) {
				allowNewPoint = false;
			}
		}
	}

	private boolean correctCoords(double[] sceneCoords) {
		for (double d : sceneCoords) {
			if (d > 1 || d < -1) {
				return false;
			}
		}
		return true;
	}

	private Position fromSceneToGeographic(double[] coords) {
		int pingIndex = wcdata.getIndexForPing(currentPingNumber);

		double elev = (coords[1] + 1) / 2;
		double bottomelev = wcdata.getBottomElevation(pingIndex);
		double topelev = wcdata.getTopElevation(pingIndex);
		elev = bottomelev + (topelev - bottomelev) * elev;

		double lat = (coords[0] + 1) / 2;
		double leftlat = wcdata.getLatlon1(pingIndex).latitude.degrees;
		double rightlat = wcdata.getLatlon2(pingIndex).latitude.degrees;
		lat = leftlat + (rightlat - leftlat) * lat;

		double lon = (coords[0] + 1) / 2;
		double leftlon = wcdata.getLatlon1(pingIndex).longitude.degrees;
		double rightlon = wcdata.getLatlon2(pingIndex).longitude.degrees;
		lon = leftlon + (rightlon - leftlon) * lon;

		return Position.fromDegrees(lat, lon, elev);
	}

	private void fromGeographicToScene(Position pos, double[] coords) {
		fromGeographicToScene(pos, pos.elevation, coords);
	}

	private void fromGeographicToScene(LatLon pos, double elev, double[] coords) {
		int pingIndex = wcdata.getIndexForPing(currentPingNumber);

		double bottomelev = wcdata.getBottomElevation(pingIndex);
		double topelev = wcdata.getTopElevation(pingIndex);
		elev = (elev - bottomelev) / (topelev - bottomelev);

		double lat = pos.latitude.degrees;
		double leftlat = wcdata.getLatlon1(pingIndex).latitude.degrees;
		double rightlat = wcdata.getLatlon2(pingIndex).latitude.degrees;
		lat = (lat - leftlat) / (rightlat - leftlat);

		double lon = pos.longitude.degrees;
		double leftlon = wcdata.getLatlon1(pingIndex).longitude.degrees;
		double rightlon = wcdata.getLatlon2(pingIndex).longitude.degrees;
		lon = (lon - leftlon) / (rightlon - leftlon);

		// make average on lat and lon, and put it between -1..1
		coords[0] = lat + lon - 1;

		// put elev from 0..1 to -1..1
		coords[1] = elev * 2 - 1;
	}

	public void setCurrentPing(int pingNumber) {
		if (currentPingNumber != pingNumber) {
			currentPingNumber = pingNumber;
			// Player index change : reload the list of markers
			updateMarkers();
		}
	}

	public void setAllowNewPoint(boolean b) {
		allowNewPoint = b;
	}

	/**
	 * Looks at the nearest marker from the cursor.
	 */
	private Optional<IWCMarker> findNearestPoint(GLAutoDrawable glad, int x2, int y2) {
		double minDist = Double.MAX_VALUE;
		IWCMarker nearest = null;
		double[] markerScreenCoords = new double[3];

		for (IWCMarker marker : makers) {
			// Geographic -> scene
			fromGeographicToScene(PositionUtils.getPosition(marker), sceneCoords);

			// Scene -> screen
			CoordinatesProjection.fromSceneToScreen(glad, sceneCoords[0], sceneCoords[1], 1.0, markerScreenCoords);

			double dx = Math.abs(x2 - markerScreenCoords[0]);
			double dy = Math.abs(y2 - markerScreenCoords[1]);
			double dist = Math.sqrt(dx * dx + dy * dy);
			if (dist < minDist) {
				nearest = marker;
				minDist = dist;
			}
		}
		return minDist < Activator.get2DPreferences().getMarkerSize() / 2.0 ? Optional.of(nearest) : Optional.empty();
	}

	public void mouseClicked(int x, int y) {
		this.x = x;
		this.y = y;
		if (selectedPoint.isEmpty()) {
			mode = Mode.NEW_POINT;
		} else {
			if (mode == Mode.SELECTION) {
				selectedPoint = Optional.empty();
				mode = null;
			} else {
				mode = Mode.SELECTION;
				selectedPoint = Optional.of(currentPoint.get());
			}
		}
	}

	public void mousePressed(int x, int y) {
		clickedX = x;
		clickedY = y;
		this.x = x;
		this.y = y;
		if (currentPoint.isPresent()) {
			mode = Mode.ALLOW_DRAGGING;
			selectedPoint = Optional.ofNullable(currentPoint.get());
		}
	}

	public void mouseMoved(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void mouseDragged(int x, int y) {
		this.x = x;
		this.y = y;
		if (mode == Mode.ALLOW_DRAGGING) {
			mode = Mode.DRAGGING_POINT;
		} else if (mode != Mode.DRAGGING_POINT) {
			mode = Mode.DRAGGING_SCENE;
		}
	}

	public void mouseReleased(int x, int y) {
		if (mode == Mode.DRAGGING_POINT && selectedPoint.isPresent()) {
			// Position tp = selectedPoint.get().getPosition();
			// selectedPoint.get().setPosition(new Position(tp, 0));
			selectedPoint = Optional.empty();
			mode = null;
		} else if (mode == Mode.DRAGGING_SCENE) {
			savedxtranslation = xtranslation;
			savedytranslation = ytranslation;
			mode = null;
			if (selectedPoint.isPresent()) {
				mode = Mode.SELECTION;
			}
		}
	}

	public void resetView() {
		zoom = DEFAULT_ZOOM;
		xtranslation = 0.0;
		ytranslation = 0.0;
		savedxtranslation = 0.0;
		savedytranslation = 0.0;
	}

	public void zoom(int wheelRotation) {
		zoom *= wheelRotation / 10.0 + 1;
		zoom = Math.max(zoom, 0.1);
		zoom = Math.min(zoom, 3.0);
	}

	/**
	 * Sets listeners for each {@link WWMarkerLayer}. Removes obsolete listeners.
	 */
	private void updateMarkerListeners() {
		// get all WWMarkerLayers
		List<WWWaterColumnMarkerLayer> newMarkerLayers = fileLayerStoreModel.getLayers(WWWaterColumnMarkerLayer.class)
				.collect(Collectors.toList());
		updateMarkerListeners(newMarkerLayers);
	}

	/**
	 * Sets listeners for each {@link WWMarkerLayer}. Removes obsolete listeners.
	 */
	private void updateMarkerListeners(List<WWWaterColumnMarkerLayer> newMarkerLayers) {
		// remove listener of old marker layer
		markerLayers.forEach(markerLayer -> {
			markerLayer.getIMarkers().removeChangeListener(markerListener);
			markerLayer.removePropertyChangeListener("Enabled", enableLayerListener);
		});

		// refresh markerLayers local list, and add listener
		markerLayers = newMarkerLayers;
		markerLayers.forEach(markerLayer -> {
			markerLayer.getIMarkers().addChangeListener(markerListener);
			markerLayer.addPropertyChangeListener("Enabled", enableLayerListener);
		});
	}

	/** Creates the list of GLMarkers */
	private void updateMarkers() {
		// get marker to display
		makers = markerLayers.stream()
				// get only enabled layers
				.filter(WWMarkerLayer::isEnabled)
				// get markers from WWMarkerLayer
				.flatMap(wwMarkerLayer -> wwMarkerLayer.getIMarkers().asList().stream())
				// filter marker to get ones to display
				.filter(marker -> marker.getPing() == currentPingNumber
						&& marker.getWaterColumnLayer().equals(layer.getName()))
				.collect(Collectors.toList());

		// Refresh the view
		SwingUtilities.invokeLater(canvas::repaint);
	}

	/**
	 * @return the {@link #makers}
	 */
	public List<IWCMarker> getMakers() {
		return Collections.unmodifiableList(makers);
	}

	/** Delete the current selected marker */
	public void deleteSelectedMarker() {
		var currentSelectedPoint = selectedPoint.get();
		selectedPoint = Optional.empty();
		if (currentSelectedPoint != null) {
			fileLayerStoreModel.getLayers(WWWaterColumnMarkerLayer.class)
					.forEach(wcMarkerlayer -> wcMarkerlayer.removeMarker(currentSelectedPoint));
		}
	}

	/** Delete the current selected marker */
	public void deleteCurrentPoint(IWCMarker marker) {
		fileLayerStoreModel.getLayers(WWWaterColumnMarkerLayer.class)
				.forEach(wcMarkerlayer -> wcMarkerlayer.removeMarker(marker));
	}

	/** Edit the current selected marker */
	public void editCurrentPoint(IWCMarker marker) {
		fileLayerStoreModel.getLayers(WWWaterColumnMarkerLayer.class)
				.forEach(wcMarkerlayer -> wcMarkerlayer.editMarker(marker));
	}

	/** Generates the contextual menu for the selected marker */
	public MarkerMenu generateMarkerMenu() {
		MarkerMenu result = null;
		if (currentPoint.isPresent()) {
			IWCMarker marker = currentPoint.get();
			result = new MarkerMenu(() -> editCurrentPoint(marker), () -> deleteCurrentPoint(marker));
		}
		return result;
	}

}
