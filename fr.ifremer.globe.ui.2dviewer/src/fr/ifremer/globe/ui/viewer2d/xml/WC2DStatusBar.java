package fr.ifremer.globe.ui.viewer2d.xml;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.model.watercolumn.IWaterColumnData;

public class WC2DStatusBar extends JPanel{

	protected final JLabel depthDisplay = new JLabel("");
	protected final JLabel pingDisplay = new JLabel("");
	protected final JLabel dateDisplay = new JLabel("");

	/**
	 *
	 */
	private static final long serialVersionUID = 4707681497521380520L;
	private WaterColumnLayer layer;
	WC2DStatusBar(WaterColumnLayer layer)
	{
		this.layer=layer;

		dateDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		pingDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		depthDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		this.setLayout(new BorderLayout());
		this.add(dateDisplay,BorderLayout.WEST);
		this.add(pingDisplay,BorderLayout.CENTER);
		this.add(depthDisplay, BorderLayout.EAST);

	}



	public void update(final int imageNumber)
	{
		if(layer!=null)
		{
			Runnable r = new Runnable() {
				@Override
				public void run() {
					try {
						updateTooltip( imageNumber);
					} catch (Throwable t) {
						t.printStackTrace();
					}
				}
			};
			SwingUtilities.invokeLater(r);
		}
	}

	private void updateTooltip(int pingNumber) {
		IWaterColumnData wcd = layer.getDatas();
		PlayerBean player=layer.getPlayerBean();
		int pingIndex = wcd.getIndexForPing(pingNumber);

		Double bottom = Math.ceil(wcd.getBottomElevation(pingIndex));
		//Double top = layer.getData().getTopElevation(newIndex);
		depthDisplay.setText("Depth "+bottom.toString());
		StringBuilder sb = new StringBuilder("Ping ");
		sb.append(player.getPlayNumber());
		sb.append("/ ");
		sb.append(player.getMaxPlayNumber());
		sb.append(" (index ");
		sb.append(pingIndex);
		sb.append("/ ");
		if(wcd.getDataCount()>0)
		{
			sb.append(wcd.getDataCount());
		}
		sb.append(")");
		pingDisplay.setText(sb.toString());
		Long date = null;
		if(pingIndex < wcd.getDataCount())
		{
			date=wcd.getTime(pingIndex);
		}
		if (date != null) {

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			dateDisplay.setText("Date "+sdf.format(new Date(date)));
		}
	}
}

