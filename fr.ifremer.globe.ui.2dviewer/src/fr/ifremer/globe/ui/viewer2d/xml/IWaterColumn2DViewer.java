package fr.ifremer.globe.ui.viewer2d.xml;

/**
 * Interface for Water Column 2D viewer
 */
public interface IWaterColumn2DViewer {

	void setAllowNewPoint(boolean b);

	void resetView();

	void takeScreenshot();

	void handleKeyPress(String action);
}
