package fr.ifremer.globe.ui.viewer2d.xml;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;

import fr.ifremer.globe.ogl.Globe3DScene;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.viewer2d.Activator;
import fr.ifremer.globe.ui.viewer2d.preferences.Viewer2DPreferences;
import fr.ifremer.globe.ui.viewer2d.view.menu.MarkerMenu;
import fr.ifremer.globe.ui.viewer2d.view.utils.ScreenshotTool;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.markers.MarkerController;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import fr.ifremer.viewer3d.util.SSVCursor;

public class WaterColumn2DScene extends Globe3DScene
		implements GLEventListener, MouseListener, MouseMotionListener, MouseWheelListener {

	private static final Logger logger = LoggerFactory.getLogger(WaterColumn2DScene.class);

	protected static final String SIMPLE_SHADER_VERT = "/shader/simpleShader.vert";
	protected static final String TRANSPARENCY_SHADER_FRAG = "/shader/transparencyShader.frag";

	private GLCanvas canvas = null;
	private static Map<GL, Integer> shaderprograms = new HashMap<>(4);
	private WaterColumnLayer layer;
	private int currentPingNumber = 1;
	private WaterColumn2DWall wall;
	private boolean takeScreenshot = false;
	private Viewer2DPreferences pref = null;

	public WaterColumn2DScene(WaterColumnLayer layer, GLCanvas canvas, MarkerController markerController,
			WWFileLayerStoreModel fileLayerStoreModel) throws GIOException {
		this.layer = layer;
		this.canvas = canvas;

		wall = new WaterColumn2DWall(layer, markerController, fileLayerStoreModel, canvas);
		// get preferences
		pref = Activator.get2DPreferences();
	}

	@Override
	public void refresh() {
		Runnable r = () -> {
			try {
				canvas.repaint();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		};
		SwingUtilities.invokeLater(r);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		// Enable z- (depth) buffer for hidden surface removal.
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glDepthFunc(GL.GL_LEQUAL);

		// Define "clear" color.
		gl.glClearColor(0f, 0f, 0f, 0f);

		// We want a nice perspective.
		gl.glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);

		initializeShader(gl);
	}

	private void initializeShader(GL2 gl) {

		int shaderProgram = getShaderProgram(gl);

		if (shaderProgram != 0) {
			gl.glUseProgram(shaderProgram);

			int shader_inverseColorMap = gl.glGetUniformLocation(shaderProgram, "inverseColormap");
			int shader_min = gl.glGetUniformLocation(shaderProgram, "min");
			int shader_max = gl.glGetUniformLocation(shaderProgram, "max");
			int shader_minTransparency = gl.glGetUniformLocation(shaderProgram, "minTransparency");
			int shader_maxTransparency = gl.glGetUniformLocation(shaderProgram, "maxTransparency");
			int shader_colormap = gl.glGetUniformLocation(shaderProgram, "colormap");
			int shader_colormapOri = gl.glGetUniformLocation(shaderProgram, "colormapOri");

			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), (float) layer.getOpacity());

			// set uniform variables
			if (layer.isInverseColorMap()) {
				gl.glUniform1f(shader_inverseColorMap, (float) 1.0);
			} else {
				gl.glUniform1f(shader_inverseColorMap, (float) 0.0);
			}
			gl.glUniform1f(shader_min, (float) (layer.getMinContrast() / 255));
			gl.glUniform1f(shader_max, (float) (layer.getMaxContrast() / 255));
			gl.glUniform1f(shader_minTransparency, (float) (layer.getMinTransparency() / 255));
			gl.glUniform1f(shader_maxTransparency, (float) (layer.getMaxTransparency() / 255));
			gl.glUniform1f(shader_colormap, layer.getColorMap());
			gl.glUniform1f(shader_colormapOri, layer.getColorMapOri());
		}
	}

	private int getShaderProgram(GL2 gl) {
		int shaderProgram = 0;
		try {
			if (!shaderprograms.containsKey(gl)) {
				shaderprograms.put(gl, ShaderUtil.createShader(this, gl, SIMPLE_SHADER_VERT, TRANSPARENCY_SHADER_FRAG));
			} else {
				shaderProgram = shaderprograms.get(gl);
			}
		} catch (GIOException e) {
			logger.error("Error reading fragment shader : ", e);
		}
		return shaderProgram;
	}

	@Override
	public void display(GLAutoDrawable glad) {

		GL2 gl = glad.getGL().getGL2();

		int w = glad.getSurfaceWidth();
		int h = glad.getSurfaceHeight();
		gl.glViewport(0, 0, w, h);

		// clear the colour and depth buffer
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// reset projection matrix
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();

		// reset modelview matrix
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();

		wall.setCurrentPing(currentPingNumber);
		// display grid with default shader
		gl.glUseProgram(0);
		wall.drawMetrics(glad);
		// display wall with custom shader
		initializeShader(gl);
		wall.draw(glad);
		// display point with default shader
		gl.glUseProgram(0);
		wall.drawPoints(glad);

		if (takeScreenshot) {
			doTakeScreenshot(glad);
		}
	}

	private void doTakeScreenshot(GLAutoDrawable glad) {
		takeScreenshot = false;
		AWTGLReadBufferUtil glReadBufferUtil = new AWTGLReadBufferUtil(glad.getGLProfile(), false);
		BufferedImage image = glReadBufferUtil.readPixelsToBufferedImage(glad.getGL(), true);

		File f = getScreenshotFile();
		if (f != null) {
			try {
				ImageIO.write(image, "png", f);
				logger.info("Screenshot recorded in " + f.getAbsolutePath());
			} catch (Exception e) {
				logger.error("Screenshot failed: " + e);
			}
		} else {
			logger.error("Screenshot failed: Unable to create the output file");
		}
	}

	@Override
	public void dispose(GLAutoDrawable arg0) {
		wall.dispose(arg0);
	}

	@Override
	public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
		refresh();
	}

	public void refresh(int pingNumber) {
		currentPingNumber = pingNumber;
		refresh();
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1) {
			wall.mouseClicked(evt.getX(), evt.getY());
			refresh();
		}
	}

	@Override
	public void mouseEntered(MouseEvent evt) {
	}

	@Override
	public void mouseExited(MouseEvent evt) {
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1) {
			wall.mousePressed(evt.getX(), evt.getY());
			refresh();
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1) {
			wall.mouseReleased(evt.getX(), evt.getY());
			refresh();
		} else if (!evt.isConsumed() && evt.isPopupTrigger()) {
			MarkerMenu markerMenu = wall.generateMarkerMenu();
			if (markerMenu != null) {
				markerMenu.show(evt.getComponent(), evt.getX(), evt.getY());
				evt.consume();
			}
		}

	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (SwingUtilities.isLeftMouseButton(evt)) {
			wall.mouseDragged(evt.getX(), evt.getY());
			refresh();
		}
	}

	@Override
	public void mouseMoved(MouseEvent evt) {
		wall.mouseMoved(evt.getX(), evt.getY());
		refresh();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		wall.zoom(e.getWheelRotation());
		refresh();
	}

	private File getScreenshotFile() {
		File folder = pref.getScreenshotFolder().toFile();
		folder.mkdirs();
		if (!folder.isDirectory()) {
			logger.warn("Unable to create screenshot directory: " + folder.getAbsolutePath());
			return null;
		}

		String prefix = layer.getInputFile().getName();
		prefix += "_" + currentPingNumber;
		// prefix += "_" + markerStoreNode.getFile().getName();
		for (String typologyName : ScreenshotTool.getTypologyNames(wall.getMakers(), currentPingNumber)) {
			prefix += "_" + typologyName;
		}
		prefix += "_";
		int imgNumber = ScreenshotTool.getLastScreenshotNumber(folder, prefix) + 1;
		return new File(folder, prefix + imgNumber + ".png");
	}

	public void takeScreenshot() {
		takeScreenshot = true;
		// If the screenshot folder setting is not found, ask the user for a
		// folder.
		if (getScreenshotFile() == null) {
			DirectoryDialog dialog = new DirectoryDialog(Display.getCurrent().getActiveShell());
			dialog.setFilterPath(pref.getScreenshotFolder().toString());
			String filename = dialog.open();
			if (filename != null) {
				pref.changeScreenshotFolder(filename);
				pref.save();
			}
		}
		refresh();
	}

	public void setAllowNewPoint(boolean b) {
		wall.setAllowNewPoint(b);
		if (b) {
			canvas.setCursor(SSVCursor.getPredefinedCursor(SSVCursor.CURSOR_POINT));
		} else {
			canvas.setCursor(null);
		}
	}

	public void resetView() {
		wall.resetView();
		refresh();
	}

	/**
	 * @return the {@link #wall}
	 */
	public WaterColumn2DWall getWall() {
		return wall;
	}
}
