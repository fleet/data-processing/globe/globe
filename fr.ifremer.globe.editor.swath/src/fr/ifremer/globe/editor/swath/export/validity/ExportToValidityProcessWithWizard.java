/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.editor.swath.export.validity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.BathyDtmModel;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndexRegistry;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.ValidityMapper;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Export validity flag files using a wizard
 */
public class ExportToValidityProcessWithWizard extends DefaultProcessWithWizard<ExportToValidityWizardModel> {

	public static final String TITLE = "Export to validity file";

	/** SpatialIndex provider */
	@Inject
	@Named(ContextNames.SPATIAL_INDEX_REGISTRY)
	protected SpatialIndexRegistry spatialIndexRegistry;

	/** Values to export */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	@Named(ContextNames.BATHY_DTM_MODEL)
	protected BathyDtmModel dtmModel;

	/**
	 * Constructor
	 */
	public ExportToValidityProcessWithWizard(Shell shell, String name) {
		super(shell, TITLE, ExportToValidityWizardModel::new, ExportToValidityWizard::new);
	}

	/**
	 * @see fr.ifremer.globe.utils.process.GProcess#compute(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) {
		List<File> inputFiles = model.getInputFiles().asList();
		logger.info("Beginning export of {} files", inputFiles.size());
		SubMonitor submonitor = SubMonitor.convert(monitor, "Export to CSV in progress", inputFiles.size() * 100);

		for (File inputFile : model.getInputFiles().asList()) {
			logger.info("Beginning export of {} ", inputFile.getName());
			processTheExport(inputFile, submonitor.split(100), logger);
		}

		String endMessage = String.format("Export validity of %s file%s done.", inputFiles.size(),
				inputFiles.size() > 1 ? "s" : "");
		logger.info(endMessage);
		Messages.openInfoMessage(TITLE, endMessage);
		return Status.OK_STATUS;
	}

	protected void processTheExport(File inputFile, IProgressMonitor monitor, Logger logger) {
		if (!monitor.isCanceled()) {
			Optional<ISounderNcInfo> option = IFileService.grab().getSounderNcInfo(inputFile);
			if (option.isPresent()) {
				ISounderNcInfo sounderNcInfo = option.get();
				File result = outputFileNameComputer.compute(inputFile, model,
						ExportToValidityWizardModel.VALIDITY_EXTENSION);
				try {
					if (shouldBeProcessed(result, logger)) {
						export(sounderNcInfo, result, monitor);
					}
				} catch (GIOException e) {
					logger.error("Error while exporting {} to {} ({})", inputFile.getName(), result.getName(),
							e.getMessage());
					FileUtils.deleteQuietly(result);
				}
			}
			monitor.done();
		}
	}

	public void export(ISounderNcInfo info, File outputFile, IProgressMonitor monitor) throws GIOException {
		monitor.subTask(outputFile.getName());
		// Get data container
		SounderDataContainer dataContainer = null;
		for (SounderDataContainer oneDataContainer : dtmModel.getSounderDataContainers()) {
			if (oneDataContainer.getInfo().getPath().equals(info.getPath())) {
				dataContainer = oneDataContainer;
				break;
			}
		}

		if (dataContainer == null)
			throw new GIOException("File not available : " + info.getPath());

		SpatialIndex spatialIndex = spatialIndexRegistry.get(dataContainer);

		StringBuilder outputStringBuilder = new StringBuilder();
		char[] buffer = null;
		try (BufferedWriter bufferedWriter = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8,
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {

			long totalBeamCount = spatialIndex.getMappings().getSoundingCount();
			SubMonitor submonitor = SubMonitor.convert(monitor, "Processing " + info.getPath(), (int) totalBeamCount);

			LongLayer1D timeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);
			for (long beamIndex = 0; beamIndex < totalBeamCount; beamIndex++) {
				int cycle = spatialIndex.getMappings().getCycleIndex(beamIndex);
				int beam = spatialIndex.getMappings().getBeamIndex(beamIndex);
				long swathTime = DateUtils.nanoSecondToMilli(timeLayer.get(cycle)); // get time in milliseconds
				MappingValidity validity = spatialIndex.getMappings().getValidity(beamIndex);
				outputStringBuilder.append(swathTime);
				outputStringBuilder.append(';');
				outputStringBuilder.append(cycle);
				outputStringBuilder.append(';');
				outputStringBuilder.append(beam);
				outputStringBuilder.append(';');
				outputStringBuilder.append(ValidityMapper.getStatus(validity).getValue());
				outputStringBuilder.append(';');
				outputStringBuilder.append(ValidityMapper.getStatusDetails(validity).getValue());

				// Write StringBuilder efficiently (better than Writer.append(CharSequence))
				if (buffer == null || buffer.length < outputStringBuilder.length()) {
					buffer = new char[outputStringBuilder.length()];
				}
				outputStringBuilder.getChars(0, outputStringBuilder.length(), buffer, 0);
				outputStringBuilder.setLength(0);
				bufferedWriter.write(buffer);
				bufferedWriter.newLine();
				submonitor.worked(1);
			}

		} catch (IOException e) {
			throw GIOException.wrap("Failed to export validity flags as csv file", e);
		}
	}

}
