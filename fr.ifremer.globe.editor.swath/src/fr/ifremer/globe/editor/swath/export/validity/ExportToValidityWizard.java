package fr.ifremer.globe.editor.swath.export.validity;

import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard for exporting mbg/xsf files to validity flag files
 */
public class ExportToValidityWizard extends Wizard {

	/** Model */
	protected ExportToValidityWizardModel exportToValidityWizardModel;

	/** Pages */
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ExportToValidityWizard(ExportToValidityWizardModel convertSounderFilesWizardModel) {
		exportToValidityWizardModel = convertSounderFilesWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {

		IFileService fileService = ContextInitializer.getService(IFileService.class);
		exportToValidityWizardModel.getInputFilesFilterExtensions()
				.addAll(fileService.getFileFilters(ContentType.MBG_NETCDF_3));
		exportToValidityWizardModel.getInputFilesFilterExtensions()
				.addAll(fileService.getFileFilters(ContentType.XSF_NETCDF_4));

		selectOutputParametersPage = new SelectOutputParametersPage(exportToValidityWizardModel);
		selectOutputParametersPage.enableLoadFilesAfter(false);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectOutputParametersPage.isPageComplete();
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		super.dispose();

		exportToValidityWizardModel = null;
		selectOutputParametersPage = null;
	}

}
