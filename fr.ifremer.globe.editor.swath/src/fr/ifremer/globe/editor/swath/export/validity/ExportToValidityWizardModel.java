package fr.ifremer.globe.editor.swath.export.validity;

import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the "Convert sounder files" wizard
 */
public class ExportToValidityWizardModel extends ConvertWizardDefaultModel {

	/** Validity flags extension */
	public static final String VALIDITY_EXTENSION = "validityflags.csv";

	/**
	 * Constructor
	 */
	public ExportToValidityWizardModel() {
		outputFormatExtensions.add(VALIDITY_EXTENSION);
	}
}