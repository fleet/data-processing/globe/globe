package fr.ifremer.globe.editor.swath.application.handlers;

import java.util.List;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.swath.SwathController;
import fr.ifremer.globe.editor.swath.SwathFileInfo;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.projectexplorer.SwathEditorGroupNode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsViewPart;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParametersFactory;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorWizard;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.StoreUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.inject.Inject;

/**
 * Handler used to open the Swath Editor.
 */
public class SwathEditorHandler extends AbstractNodeHandler implements IDataContainerOwner {

	/** logger */
	private static final Logger logger = LoggerFactory.getLogger(SwathEditorHandler.class);

	/** SounderDataContainer provider */
	@Inject
	protected IDataContainerFactory sounderDataContainerFactory;
	@Inject
	protected IProcessService processService;

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService) {
		return checkExecution(selectionService, modelService)
				&& partService.findPart(SoundingsViewPart.PART_ID) == null;
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		Object[] selection = getSelection(selectionService);
		List<FileInfoNode> nodeList = getSelectionAsList(selection, ContentType::isManageableInSwathEditor);
		return nodeList.size() == selection.length;
	}

	@Execute
	public void execute(ESelectionService selectionService) {
		Object[] selection = getSelection(selectionService);
		List<FileInfoNode> nodeList = getSelectionAsList(selection, ContentType::isManageableInSwathEditor);
		List<ISounderNcInfo> infoStores = StoreUtils.getISounderNcInfoFromNodes(nodeList);

		// New context for this edition
		ContextInitializer.prepareNewEdition();
		createView(infoStores);
	}

	/**
	 * Creates the Swath Editor parts and push them into the window at their right place.
	 *
	 * @param editedFiles List of file to open.
	 */
	protected void createView(List<ISounderNcInfo> editedFiles) {
		List<ISounderDataContainerToken> tokens;
		try {
			tokens = sounderDataContainerFactory.bookSounderDataContainers(editedFiles, this);
		} catch (GIOException e) {
			Messages.openWarningMessage("Swath editor",
					"Unable to open this editor because some files are in used.\n\n" + e.getMessage());
			return;
		}
		try {
			// Open a wizard to choose the desired spatial resolution
			SwathEditorParameters parameters = SwathEditorParametersFactory.create(editedFiles);
			SwathEditorWizard wizard = new SwathEditorWizard(parameters);
			WizardDialog dialog = new WizardDialog(Display.getDefault().getActiveShell(), wizard);
			dialog.setPageSize(500, 200);

			if (dialog.open() == 0) {
				processService.runInForeground("Swath Editor initialization", (monitor, l) -> {
					try {
						SwathEditorParameters editedParameters = wizard.getEditedParameters();
						ContextInitializer.setInContext(ContextNames.SWATH_EDITOR_PARAMETERS, editedParameters);
						ContextInitializer.setInContext(ContextNames.SWATH_FILE_INFO,
								ContextInitializer.make(SwathFileInfo.class));
						ContextInitializer.setInContext(ContextNames.SWATH_EDITOR_GROUP_NODE,
								ContextInitializer.make(SwathEditorGroupNode.class));
						SwathController swathControler = ContextInitializer.make(SwathController.class);
						swathControler.initialize(tokens, monitor);
						monitor.done();
						return Status.OK_STATUS;
					} catch (OperationCanceledException e) {
						ContextInitializer.reset();
						return Status.CANCEL_STATUS;
					} catch (GIOException e) {
						cancel(tokens);
						throw e;
					}
				});
			} else {
				cancel(tokens);
			}
		} catch (Exception e) {
			logger.error(String.format("Unable to open a SwathEditor for selected info stores : %s", editedFiles), e);
			Messages.openErrorMessage("An error occured (" + e.getMessage() + ").\nSee logs for more details.", e);
			cancel(tokens);
		}
	}

	private void cancel(List<ISounderDataContainerToken> tokens) {
		tokens.forEach(ISounderDataContainerToken::close);
		ContextInitializer.reset();
	}

}
