/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.application.context;

import fr.ifremer.globe.editor.swath.SwathController;
import fr.ifremer.globe.editor.swath.SwathFileInfo;
import fr.ifremer.globe.editor.swath.model.BathyDtmModel;
import fr.ifremer.globe.editor.swath.model.NavigationModel;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndexRegistry;
import fr.ifremer.globe.editor.swath.ui.geographicview.BeamHunter;
import fr.ifremer.globe.editor.swath.ui.geographicview.SelectionShifter;
import fr.ifremer.globe.editor.swath.ui.projectexplorer.SwathEditorGroupNode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsViewPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.BoundsComputer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.UndoRedoController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector.BiasCorrector;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWQuadSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;

/**
 * All names of injectable objects stored in the IEclipseContext
 */
public class ContextNames {

	/** Root name of all objects that can be injected */
	public static final String PLUGIN_ID = "fr.ifremer.globe.editor.swath";

	/** Name of the instance of {@link SwathEditorParameters} used to prepare the edition */
	public static final String SWATH_EDITOR_PARAMETERS = PLUGIN_ID + ".SWATH_EDITOR_PARAMETERS";

	/** Name of the instance of {@link SwathEditorGroupNode} */
	public static final String SWATH_EDITOR_GROUP_NODE = PLUGIN_ID + ".SWATH_EDITOR_GROUP_NODE";

	/** Name of the instance of {@link SwathFileInfo} */
	public static final String SWATH_FILE_INFO = PLUGIN_ID + ".SWATH_FILE_INFO";

	/** Name of the {@link SpatialIndexRegistry} */
	public static final String SPATIAL_INDEX_REGISTRY = PLUGIN_ID + ".SPATIAL_INDEX_REGISTRY";

	/** Name of the {@link SwathController} */
	public static final String SWATH_CONTROLLER = PLUGIN_ID + ".SWATH_CONTROLLER";

	/** Name of the {@link SoundingsControler} */
	public static final String SOUNDINGS_CONTROLLER = PLUGIN_ID + ".SOUNDINGS_CONTROLLER";

	/** Name of the {@link SoundingsModel} */
	public static final String SOUNDINGS_MODEL = PLUGIN_ID + ".SOUNDINGS_MODEL";

	/** Name of the {@link BeamDisplayMode}, how to color beams in the Soundings View */
	public static final String BEAM_DISPLAY_MODE = PLUGIN_ID + ".BEAM_DISPLAY_MODE";

	/** True to always invalid beams in red whatever the BeamDisplayMode value */
	public static final String KEEP_INVALID_BEAM_IN_RED = PLUGIN_ID + ".KEEP_INVALID_BEAM_IN_RED";

	/** Name of the {@link BathyDtmModel} */
	public static final String BATHY_DTM_MODEL = PLUGIN_ID + ".BATHY_DTM_MODEL";

	/** Name of the {@link NavigationModel} */
	public static final String NAVIGATION_MODEL = PLUGIN_ID + ".NAVIGATION_MODEL";

	/** Name of the {@link SoundingsViewPart} */
	public static final String SWATH_EDITOR_SOUNDINGS = PLUGIN_ID + ".SWATH_EDITOR_SOUNDINGS";

	/** Name of the {@link DtmPreference} */
	public static final String DTM_PREFERENCES = PLUGIN_ID + ".DTM_PREFERENCES";

	/** Name of the {@link UndoRedoController} */
	public static final String UNDO_REDO_CONTROLLER = PLUGIN_ID + ".UNDO_REDO_CONTROLLER";

	/** Name of the {@link SelectionShifter} */
	public static final String SELECTION_SHIFTER = PLUGIN_ID + ".SELECTION_SHIFTER";

	/** Name of the {@link BeamHunter} */
	public static final String BEAM_HUNTER = PLUGIN_ID + ".BEAM_HUNTER";

	/** Name of the {@link BoundsComputer} */
	public static final String BOUNDS_COMPUTER = PLUGIN_ID + ".BOUNDS_COMPUTER";

	/** Name of the {@link ViewPoint} */
	public static final String SOUNDINGS_VIEW_POINT = PLUGIN_ID + ".SOUNDINGS_VIEW_POINT";

	/** Name of the {@link BiasCorrector} */
	public static final String BIAS_CORRECTOR = PLUGIN_ID + ".BIAS_CORRECTOR";

	/** Name of the {@link BathySelectionMode}. Rectangle or Swath selection of DTM */
	public static final String BATHY_SELECTION_MODE = PLUGIN_ID + ".BATHY_SELECTION_MODE";

	/** Name of the property defining the nb of swath to select in selection mode Swath */
	public static final String SWATH_COUNT_IN_SELECTION = PLUGIN_ID + ".SWATH_COUNT_IN_SELECTION";

	/** Name of the {@link IWWQuadSelectionLayer} */
	public static final String WW_QUAD_SELECTION_LAYER = PLUGIN_ID + ".WW_QUAD_SELECTION_LAYER";

	/** Name of the {@link IWWSwathSelectionLayer} */
	public static final String WW_SWATH_SELECTION_LAYER = PLUGIN_ID + ".WW_SWATH_SELECTION_LAYER";

	/**
	 * Constructor
	 */
	private ContextNames() {
		// private constructor to hide the implicit public one.
	}
}
