/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.application.context;

/**
 * Kind of selection in geographic view
 */
public enum BathySelectionMode {
	/** Using IWWQuadSelectionLayer */
	RECTANGLE,
	/** Using IWWSwathLayer */
	SWATH;
}
