/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.application.event;

import org.eclipse.e4.ui.workbench.UIEvents;

import fr.ifremer.globe.editor.swath.ui.soundingsview.action.SoundEditorAction;

/**
 * All topics managed in the swath editor raised by the IEventBroker
 */
public class EventTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/globe/editor/swath";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to alert of a action on the sounding view<br>
	 * Event poster place on the event an instance of {@link SoundEditorAction}
	 */
	public static final String SOUND_ACTION = TOPIC_BASE + UIEvents.TOPIC_SEP + "SOUND_ACTION";

	/**
	 * Topic to alert of the change of the soundings selection<br>
	 * Event poster place on the event an instance of itself.
	 */
	public static final String SOUNDINGS_SELECTION = TOPIC_BASE + UIEvents.TOPIC_SEP + "SOUNDINGS_SELECTION";

	/**
	 * Topic to alert of the update of the bounding box<br>
	 * Event poster place on the event an instance of BoundingBoxes.
	 */
	public static final String BOUNDING_BOXES_UPDATED = TOPIC_BASE + UIEvents.TOPIC_SEP + "BOUNDING_BOXES_UPDATED";

	/**
	 * Topic to alert that status of the model changed and that data or their visibility where updated<br>
	 * Event poster place on the event an instance of SoundingEvent.
	 */
	public static final String SOUNDING_MODEL_CHANGED = TOPIC_BASE + UIEvents.TOPIC_SEP + "SOUNDING_MODEL_CHANGED";

	/**
	 * Topic to notify that some soundings have been designated<br>
	 * Event poster place on the event an instance of File (ShapeFile).
	 */
	public static final String SOUNDING_DESIGNATED = TOPIC_BASE + UIEvents.TOPIC_SEP + "SOUNDING_DESIGNATED";

	/**
	 * Topic to notify that some parameters in the soundings view have been changed Event poster place on the event an
	 * instance of SVDisplayParameterChanged.
	 */
	public static final String SOUNDING_VIEW_PARAMETER_CHANGED = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "SOUNDING_VIEW_PARAMETER_CHANGED";

	/**
	 * Constructor
	 */
	private EventTopics() {
		// private constructor to hide the implicit public one.
	}
}
