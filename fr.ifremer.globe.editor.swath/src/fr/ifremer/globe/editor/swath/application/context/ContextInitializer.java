/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.application.context;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.osgi.service.event.Event;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndexRegistry;
import fr.ifremer.globe.editor.swath.ui.geographicview.BeamHunter;
import fr.ifremer.globe.editor.swath.ui.geographicview.SelectionShifter;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.BoundsComputer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.UndoRedoController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector.BiasCorrector;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.viewer3d.Viewer3D;

/**
 * Application Addon used to initialize the IEclipseContext
 */
public class ContextInitializer {

	/** Static reference of Eclipse context initialized by E4Application */
	protected static IEclipseContext parentEclipseContext;
	/** Child Eclipse context used as current one */
	protected static IEclipseContext eclipseContext;

	/**
	 * Invoked when Globe application is started.<br>
	 * Fill the context with needed components
	 *
	 * @param injectedEclipseContext Eclipse context initialized by E4Application
	 */
	@Inject
	@Optional
	public void startup(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext injectedEclipseContext) {
		startup(injectedEclipseContext);
	}

	/**
	 * Fill the context with needed components and initialize the context
	 */
	public static void startup(IEclipseContext injectedEclipseContext) {
		parentEclipseContext = injectedEclipseContext;
		initializeParentContext();
	}

	/**
	 * Init the {@link #parentEclipseContext} instance.<br>
	 * It registers reference instances of objects used througout all Swath edition
	 */
	protected static void initializeParentContext() {
		fitGlobalServices();
		fitPreferences();
	}

	/** Set services in {@link #parentEclipseContext} */
	protected static void fitGlobalServices() {
		SpatialIndexRegistry spatialIndexRegistry = ContextInjectionFactory.make(SpatialIndexRegistry.class,
				parentEclipseContext);
		parentEclipseContext.set(ContextNames.SPATIAL_INDEX_REGISTRY, spatialIndexRegistry);
	}

	/** Set Preferences in {@link #parentEclipseContext} */
	protected static void fitPreferences() {
		PreferenceComposite root = PreferenceRegistry.getInstance().getEditorsSettingsNode();
		parentEclipseContext.set(ContextNames.DTM_PREFERENCES, new DtmPreference(root));
	}

	/**
	 * Returns the context value associated with the given name
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInContext(String name) {
		// eclipseContext may be null when SE has not been opened yet
		return eclipseContext != null ? (T) eclipseContext.get(name) : null;
	}

	/**
	 * @return the Osgi Service
	 */
	public static <S> S getService(Class<S> clazz) {
		// eclipseContext may be null when SE has not been opened yet
		return eclipseContext != null ? eclipseContext.get(clazz) : null;
	}

	/**
	 * Sets a value to be associated with a given name in this context
	 */
	public static void setInContext(String name, Object value) {
		eclipseContext.set(name, value);
	}

	/**
	 * Removes the given name and any corresponding value from this context
	 */
	static void removeFromContext(String name) {
		eclipseContext.remove(name);
	}

	/**
	 * @return an instance of the specified class and inject it with the context.
	 */
	public static <T> T make(Class<T> clazz) {
		return ContextInjectionFactory.make(clazz, eclipseContext);
	}

	/**
	 * Injects a context into a domain object.
	 */
	public static void inject(Object object) {
		ContextInjectionFactory.inject(object, eclipseContext);
	}

	/** Remove all named objects */
	public static void reset() {
		// Dispose context. All objects are released and @PreDestroy method are invoked on them
		eclipseContext.dispose();
		eclipseContext = null;
	}

	/** Getter of {@link #eclipseContext} */
	public static IEclipseContext getEclipseContext() {
		return eclipseContext;
	}

	/** Prepare an EclipseContext dedicated to the new edition */
	public static void prepareNewEdition() {
		if (eclipseContext == null) {
			eclipseContext = parentEclipseContext.createChild();
			eclipseContext.activate();
			fitServices();
		}
	}

	/** Set services in {@link #eclipseContext} */
	protected static void fitServices() {
		// Could happened in tests
		if (Viewer3D.isUp()) {
			eclipseContext.set(ContextNames.BEAM_HUNTER, make(BeamHunter.class));
			eclipseContext.set(ContextNames.BOUNDS_COMPUTER, make(BoundsComputer.class));
			eclipseContext.set(ContextNames.SELECTION_SHIFTER, make(SelectionShifter.class));
			eclipseContext.set(ContextNames.UNDO_REDO_CONTROLLER, make(UndoRedoController.class));
			eclipseContext.set(ContextNames.SOUNDINGS_VIEW_POINT, make(ViewPoint.class));
		}
		eclipseContext.set(ContextNames.BIAS_CORRECTOR, make(BiasCorrector.class));
		eclipseContext.set(ContextNames.BATHY_SELECTION_MODE, BathySelectionMode.RECTANGLE);

		DtmPreference dtmPreference = getInContext(ContextNames.DTM_PREFERENCES);
		eclipseContext.set(ContextNames.SWATH_COUNT_IN_SELECTION,
				dtmPreference != null ? dtmPreference.getSwathCountInSelection().getValue().intValue() : 1);
	}

}
