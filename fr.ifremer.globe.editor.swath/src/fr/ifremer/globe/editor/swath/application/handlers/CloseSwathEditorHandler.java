/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.application.handlers;

import java.util.concurrent.atomic.AtomicBoolean;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.viewer3d.Viewer3D;

/**
 * Handler to close the Swath editor.
 */
public class CloseSwathEditorHandler extends AbstractNodeHandler {

	/** True when {@link #execute} is running */
	protected AtomicBoolean isClosing = new AtomicBoolean(false);

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return true;
	}

	/**
	 * Close of the Swath editor is required.
	 */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		if (isClosing.compareAndSet(false, true)) {
			// Save preferences
			DtmPreference dtmPreference = ContextInitializer.getInContext(ContextNames.DTM_PREFERENCES);
			dtmPreference.save();

			// Dispose editor
			ContextInitializer.reset();

			// Refresh 3D view
			Viewer3D.refreshRequired();

			isClosing.set(false);
		}
	}

}
