/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath;

import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.projectexplorer.SwathEditorGroupNode;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;

/**
 * IFileInfo identifying the Swath editor
 */
public class SwathFileInfo extends BasicFileInfo {

	@Inject
	@Named(ContextNames.SWATH_EDITOR_PARAMETERS)
	protected SwathEditorParameters parameters;

	protected final List<Property<?>> properties = new ArrayList<>();

	/**
	 * Constructor
	 */
	public SwathFileInfo() {
		super(SwathEditorGroupNode.SWATH_GROUP_NODE_NAME);
	}

	@PostConstruct
	/** After injection... */
	public void postConstruct() {
		geoBox = parameters.lonLatGeoBox;
		initProperties();
	}

	/** Fill the Property list */
	public void initProperties() {
		properties.addAll(Property.build(geoBox));

		// Grid definition
		properties.add(Property.build("Cell size", String.valueOf(parameters.spatialResolution) + " m"));
		properties.add(Property.build("Line count", String.valueOf(parameters.rowCount)));
		properties.add(Property.build("Column count", String.valueOf(parameters.columnCount)));

		// Files
		for (var editedFile : parameters.editedFiles) {
			properties.add(Property.build("File", editedFile.getPath()));
		}
	}

	/**
	 * @return the {@link #properties}
	 */
	@Override
	public List<Property<?>> getProperties() {
		return properties;
	}

}
