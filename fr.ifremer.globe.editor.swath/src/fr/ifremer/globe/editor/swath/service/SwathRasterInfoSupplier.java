/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.editor.swath.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.swath.LayerType;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * RasterInfo supplier used by Parameter View to display the list of available layers
 */
@Component(name = "globe_swath_service_raster_info_supplier", service = IRasterInfoSupplier.class)
public class SwathRasterInfoSupplier implements IRasterInfoSupplier {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RasterInfo> supplyRasterInfos(String filepath) throws GIOException {
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RasterInfo> supplyRasterInfos(IFileInfo fileInfo) {
		if (fileInfo == ContextInitializer.getInContext(ContextNames.SWATH_FILE_INFO)) {
			// SwathEditorParameters parameters = ContextInitializer.getInContext(ContextNames.SWATH_EDITOR_PARAMETERS);
			return Arrays.stream(LayerType.values()).map(dtmLayerType -> {
				RasterInfo rasterInfo = new RasterInfo(this, fileInfo.getPath());
				rasterInfo.setDataType(dtmLayerType.getLabel());
				return rasterInfo;
			}).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

}
