/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.service;

import java.io.IOException;
import java.util.Optional;

import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.swath.SwathController;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.geographicview.DtmElevationModel;
import fr.ifremer.globe.editor.swath.ui.parametersview.SwathElevationParametersComposite;
import fr.ifremer.globe.editor.swath.ui.parametersview.SwathElevationParametersComposite.SwathElevationParametersObserver;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.ElevationNode;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Make a Composite to edit the DtmElevationModel
 */
@Component(name = "globe_editor_swath_elevation_parameters_view_composite_factory", service = IParametersViewCompositeFactory.class)
public class SwathElevationParametersCompositeFactory
		implements IParametersViewCompositeFactory, SwathElevationParametersObserver {

	/** Current flattening overflow */
	protected DtmElevationModel dtmElevationModel;

	/** Current interpolation distance */
	protected int maxDistance = 1;

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		SwathElevationParametersComposite result = null;
		if (selection instanceof ElevationNode) {
			ElevationNode node = (ElevationNode) selection;
			if (node.getElevationModel() instanceof DtmElevationModel) {
				dtmElevationModel = (DtmElevationModel) node.getElevationModel();
				result = new SwathElevationParametersComposite(parent, this);
				result.setOverFlow((int) (dtmElevationModel.getExpand() * 100));
				result.setMaxDistance(maxDistance);
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.swath.ui.parametersview.SwathElevationParametersComposite.SwathElevationParametersObserver#updateOverFlowScale(int)
	 */
	@Override
	public void updateOverFlowScale(int overFlow) {
		if (dtmElevationModel.getExpand() * 100 != overFlow) {
			dtmElevationModel.expandSector(overFlow / 100d);
			IGeographicViewService.grab().refresh();
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.swath.ui.parametersview.SwathElevationParametersComposite.SwathElevationParametersObserver#interpolateElevations(int)
	 */
	@Override
	public void interpolateElevations(int maxDistance) {
		if (this.maxDistance != maxDistance) {
			this.maxDistance = maxDistance;
			IProcessService.grab().runInForeground("Elevation interpolation", (monitor, l) -> {
				try {
					SwathController swathController = ContextInitializer.getInContext(ContextNames.SWATH_CONTROLLER);
					swathController.interpolateElevations(maxDistance, monitor, 100);
				} catch (IOException e) {
					throw new GIOException("Error with elevation interpolation : " + e.getMessage(), e);
				}
				IGeographicViewService.grab().refresh();
				monitor.done();
				return Status.OK_STATUS;
			});
		}
	}

}
