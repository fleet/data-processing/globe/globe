/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.service;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.editor.swath.ui.parametersview.SwathEditorParametersComposite;
import fr.ifremer.globe.editor.swath.ui.projectexplorer.SwathEditorGroupNode;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;

/**
 * Make a Composite to edit the DtmLayer parameters
 */
@Component(name = "globe_editor_swath_parameters_view_composite_factory", service = IParametersViewCompositeFactory.class)
public class SwathEditorParametersCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof SwathEditorGroupNode ? Optional.of(new SwathEditorParametersComposite(parent))
				: Optional.empty();
	}

	// display Swath Editor parameters on top
	@Override
	public int getPriority() {
		return 1;
	}
}
