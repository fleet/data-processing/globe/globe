package fr.ifremer.globe.editor.swath.model;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.swath.LayerType;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndexRegistry;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;

/**
 * This class implements {@link DtmLayer} capabilities but lays on {@link SpatialIndex} capabilities in order to improve
 * data fetching performances. No need to create in-memory DTM layers, we just need to fetch data on-the-fly when
 * DtmSource is constructed or required by the { ClipmapUpdater}.
 *
 * @author apside
 */
public class IndexedDtmLayer {

	/** SpatialIndex provider */
	@Inject
	@Named(ContextNames.SPATIAL_INDEX_REGISTRY)
	protected SpatialIndexRegistry spatialIndexRegistry;

	protected LayerType layerType;

	protected double min;

	protected double max;

	/**
	 * Spécifies data fetcher capability. We use a data fetcher specific for each DTM layer type. For depth layer, the
	 * data fetcher uses the {@link Mappings} object to determine the depth value at {indexInFile} and benefits from the
	 * java NIO buffers performance.
	 */
	public static interface IndexedValueFetcher {

		/**
		 * Fetch a DTM layer's value.
		 *
		 * @param layer A layer containing infoStores.
		 * @param row Cell row.
		 * @param col Cell col.
		 * @return The fetched value.
		 */
		double getValue(IndexedDtmLayer layer, int row, int col);
	}

	/**
	 * Fetcher implementation for depth layer.
	 */
	private class DepthValueFetcher implements IndexedValueFetcher {

		@Override
		public double getValue(IndexedDtmLayer layer, int row, int col) {
			double meanOfInfoStores = layer.getMissingValue();

			double sum = 0;
			int divider = 0;
			for (SounderDataContainer dataContainer : layer.dataContainers) {

				SpatialIndex indexer = spatialIndexRegistry.get(dataContainer);

				if (indexer != null) {
					Mappings mappings = indexer.getMappings();
					int count = indexer.getSoundingCount(col, row);
					if (count > 0) {
						long offset = mappings.getCellOffset(row, col);

						for (int ret = 0; ret < count; ret++) {
							long indexInFile = offset + ret;
							MappingValidity valid = mappings.getValidity(indexInFile);
							if (MappingValidity.isValid(valid)) {
								sum += mappings.getDepth(indexInFile);
								divider++;
							}
						}
					}
				}
			}

			if (divider != 0) {
				sum /= divider;
				meanOfInfoStores = sum;
			}

			return meanOfInfoStores;
		}
	}

	/**
	 * Fetcher implementation for reflectivity layer.
	 */
	private class ReflectivityValueFetcher implements IndexedValueFetcher {

		@Override
		public double getValue(IndexedDtmLayer layer, int row, int col) {
			double meanOfInfoStores = layer.getMissingValue();

			double sum = 0;
			int divider = 0;

			for (SounderDataContainer dataContainer : layer.dataContainers) {

				SpatialIndex indexer = spatialIndexRegistry.get(dataContainer);
				Mappings mappings = indexer.getMappings();

				int count = indexer.getSoundingCount(col, row);
				if (count > 0) {
					long offset = mappings.getCellOffset(row, col);

					for (int ret = 0; ret < count; ret++) {
						long indexInFile = offset + ret;
						MappingValidity valid = mappings.getValidity(indexInFile);
						if (MappingValidity.isValid(valid)) {
							float reflectivity = mappings.getReflectivity(indexInFile);
							sum += reflectivity;
							divider++;
						}
					}
				}
			}

			if (divider != 0) {
				sum /= divider;
				meanOfInfoStores = sum;
				layer.updateMinMax(sum);
			}

			return meanOfInfoStores;
		}
	}

	/**
	 * Fetcher implementation for standard deviation layer.
	 */
	private class StdevValueFetcher implements IndexedValueFetcher {

		@Override
		public double getValue(IndexedDtmLayer layer, int row, int col) {
			double stdev = layer.getMissingValue();

			double squareSum = 0;
			double sum = 0;
			int divider = 0;

			for (SounderDataContainer dataContainer : layer.dataContainers) {

				SpatialIndex indexer = spatialIndexRegistry.get(dataContainer);
				Mappings mappings = indexer.getMappings();

				int count = indexer.getSoundingCount(col, row);
				if (count > 0) {
					long offset = mappings.getCellOffset(row, col);

					for (int ret = 0; ret < count; ret++) {
						long indexInFile = offset + ret;
						MappingValidity valid = mappings.getValidity(indexInFile);
						if (MappingValidity.isValid(valid)) {
							double depth = mappings.getDepth(indexInFile);
							sum += depth;
							squareSum += depth * depth;
							divider++;
						}
					}
				}
			}

			if (divider != 0) {
				double meanDepth = sum / divider;
				stdev = Math.sqrt(squareSum / divider - meanDepth * meanDepth);
				layer.updateMinMax(stdev);
			}

			return stdev;
		}
	}

	/**
	 * Fetcher implementation for MinSoundings layer. Returns the minimum value of a particular cell among all
	 * infoStores.
	 */
	private class MinSoundingsValueFetcher implements IndexedValueFetcher {
		@Override
		public double getValue(IndexedDtmLayer layer, int row, int col) {
			double missingValue = layer.getMissingValue();
			double minOfInfoStores = missingValue;

			for (SounderDataContainer dataContainer : layer.dataContainers) {

				SpatialIndex indexer = spatialIndexRegistry.get(dataContainer);
				Mappings mappings = indexer.getMappings();

				int count = indexer.getSoundingCount(col, row);
				long offset = mappings.getCellOffset(row, col);

				for (int ret = 0; ret < count; ret++) {
					long indexInFile = offset + ret;
					MappingValidity valid = mappings.getValidity(indexInFile);
					if (MappingValidity.isValid(valid)) {
						float minDepth = mappings.getDepth(indexInFile);
						if (minOfInfoStores == missingValue) {
							minOfInfoStores = minDepth;
						} else {
							minOfInfoStores = Math.min(minOfInfoStores, minDepth);
						}
					}
				}
			}

			return minOfInfoStores;
		}
	}

	/**
	 * Fetcher implementation for MaxSoundings layer. Returns the maximum value of a particular cell among all
	 * infoStores.
	 */
	private class MaxSoundingsValueFetcher implements IndexedValueFetcher {

		@Override
		public double getValue(IndexedDtmLayer layer, int row, int col) {
			double missingValue = layer.getMissingValue();
			double maxOfInfoStores = missingValue;

			for (SounderDataContainer dataContainer : layer.dataContainers) {

				SpatialIndex indexer = spatialIndexRegistry.get(dataContainer);
				Mappings mappings = indexer.getMappings();

				int count = indexer.getSoundingCount(col, row);
				if (count > 0) {
					long offset = mappings.getCellOffset(row, col);

					for (int ret = 0; ret < count; ret++) {
						long indexInFile = offset + ret;
						MappingValidity valid = mappings.getValidity(indexInFile);
						if (MappingValidity.isValid(valid)) {
							float maxDepth = mappings.getDepth(indexInFile);
							if (maxOfInfoStores == missingValue) {
								maxOfInfoStores = maxDepth;
							} else {
								maxOfInfoStores = Math.max(maxOfInfoStores, maxDepth);
							}
						}
					}
				}
			}

			return maxOfInfoStores;
		}
	}

	/**
	 * Fetcher implementation for ValidSoundings layer.
	 */
	private class ValidSoundingsValueFetcher implements IndexedValueFetcher {

		@Override
		public double getValue(IndexedDtmLayer layer, int row, int col) {
			double missingValue = layer.getMissingValue();
			double nbValidSoundsPerCell = missingValue;

			int nbValid = 0;
			int totalBeamCount = 0;

			for (SounderDataContainer dataContainer : layer.dataContainers) {

				SpatialIndex indexer = spatialIndexRegistry.get(dataContainer);
				Mappings mappings = indexer.getMappings();

				int count = indexer.getSoundingCount(col, row);
				long offset = mappings.getCellOffset(row, col);

				totalBeamCount += count;

				for (int ret = 0; ret < count; ret++) {
					long indexInFile = offset + ret;
					MappingValidity valid = mappings.getValidity(indexInFile);
					if (MappingValidity.isValid(valid)) {
						nbValid++;
					}
				}
			}

			if (totalBeamCount > 0) {
				nbValidSoundsPerCell = nbValid;
			}

			return nbValidSoundsPerCell;
		}
	}

	/**
	 * Fetcher implementation for ValidSoundings layer.
	 */
	private class InvalidSoundingsValueFetcher implements IndexedValueFetcher {

		@Override
		public double getValue(IndexedDtmLayer layer, int row, int col) {
			double nbInvalidSoundsPerCell = layer.getMissingValue();

			int nbInvalid = 0;
			int totalBeamCount = 0;

			for (SounderDataContainer dataContainer : layer.dataContainers) {

				SpatialIndex indexer = spatialIndexRegistry.get(dataContainer);
				Mappings mappings = indexer.getMappings();

				int count = indexer.getSoundingCount(col, row);
				long offset = mappings.getCellOffset(row, col);

				totalBeamCount += count;

				for (int ret = 0; ret < count; ret++) {
					long indexInFile = offset + ret;
					MappingValidity valid = mappings.getValidity(indexInFile);
					if (MappingValidity.isInvalid(valid)) {
						nbInvalid++;
					}
				}
			}

			if (totalBeamCount > 0) {
				nbInvalidSoundsPerCell = nbInvalid;
			}

			return nbInvalidSoundsPerCell;
		}
	}

	// -------------------------------------------------------------------------------------------

	protected List<SounderDataContainer> dataContainers;
	protected Object sounderInfoOwner;
	protected IndexedValueFetcher valueFetcher;

	/**
	 * Constructor.
	 *
	 * @param name Name of the indexed layer.
	 * @param dataContainers All infoStores to be represented in the DTM viewer.
	 */
	public void initialize(List<SounderDataContainer> containers, LayerType layerType) {
		this.layerType = layerType;
		dataContainers = containers;
		valueFetcher = createValueFetcher(this);

		// Compute min/max
		computeExtrema();
	}

	/**
	 * Recompute min/max layer values.
	 */
	public void computeExtrema() {

		clearExtrema();

		if (LayerType.VSOUNDINGS == layerType || LayerType.ISOUNDINGS == layerType) {
			min = 0;
			max = 0;
			for (SounderDataContainer dataContainer : dataContainers)
				max += dataContainer.getInfo().getTotalRxBeamCount();

		} else if (LayerType.ELEVATION == layerType || LayerType.ELEVATION_MIN == layerType
				|| LayerType.ELEVATION_MAX == layerType) {
			for (SounderDataContainer dataContainer : dataContainers) {

				SpatialIndex index = spatialIndexRegistry.get(dataContainer);

				float minDepth = index.getMappings().getMinDepth();
				float maxDepth = index.getMappings().getMaxDepth();
				updateMinMax(minDepth);
				updateMinMax(maxDepth);
			}
		}
	}

	/**
	 * Instantiates and returns a new data fetcher.
	 *
	 * @param layer The layer for wich to create the fetcher.
	 * @return A new IndexedValueFetcher.
	 */
	private IndexedValueFetcher createValueFetcher(IndexedDtmLayer layer) {
		if (LayerType.BACKSCATTER == layer.layerType) {
			return new ReflectivityValueFetcher();
		} else if (LayerType.STDEV == layer.layerType) {
			return new StdevValueFetcher();
		} else if (LayerType.ELEVATION_MIN == layer.layerType) {
			return new MinSoundingsValueFetcher();
		} else if (LayerType.ELEVATION_MAX == layer.layerType) {
			return new MaxSoundingsValueFetcher();
		} else if (LayerType.VSOUNDINGS == layer.layerType) {
			return new ValidSoundingsValueFetcher();
		} else if (LayerType.ISOUNDINGS == layer.layerType) {
			return new InvalidSoundingsValueFetcher();
		} else {
			return new DepthValueFetcher();
		}
	}

	public void updateMinMax(double value) {
		if (value != Double.NEGATIVE_INFINITY && value != Double.POSITIVE_INFINITY) {
			if (min == getMissingValue() || value < min) {
				min = value;
			}
			if (max == getMissingValue() || value > max) {
				max = value;
			}
		}
	}

	public double get(int line, int col) {
		return valueFetcher.getValue(this, line, col);
	}

	public void clearExtrema() {
		min = getMissingValue();
		max = getMissingValue();
	}

	public double getMissingValue() {
		return layerType.getMissingValue();
	}

}
