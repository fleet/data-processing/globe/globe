package fr.ifremer.globe.editor.swath.model.spatialindex.mapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Definition of validity of detection for the spatial indexed model
 */
public enum MappingValidity {

	/// WARNING IF YOU MODIFY THOSE VALUES, BE SURE TO MODIFY ValidityVertexShader.glsl

	SND_UNKNOWN(-5), // unknow detection flag, will be ignored in this model
	SND_UNVALID_ACQUIS(-4), // unvalid at acquisition or missing
	SND_UNVALID_CONVERSION(-3), // unvalid at acquisition or missing

	SND_UNVALID_AUTO(-2), // unvalidated by automatic
	SND_UNVALID_MANUAL(-1), // unvalidated by operator

	SND_VALID(2);// valid
	protected static final Logger logger = LoggerFactory.getLogger(MappingValidity.class);
	private byte value;

	private MappingValidity(int value) {
		this.value = (byte) value;
	}

	/**
	 * return the byte value for the given enum
	 */
	public byte getValue() {
		return value;
	}

	/**
	 * check if the enum have the same value than the given byte value
	 * 
	 * @param value the value to compare to
	 * @return
	 */
	public boolean isEquals(byte value) {
		return value == this.value;
	}

	/**
	 * return the matching enum for the given byte value
	 */
	public static MappingValidity valueOf(byte b) {
		if (b == SND_UNKNOWN.getValue())
			return SND_UNKNOWN;
		if (b == SND_UNVALID_ACQUIS.getValue())
			return SND_UNVALID_ACQUIS;
		if (b == SND_UNVALID_CONVERSION.getValue())
			return SND_UNVALID_CONVERSION;
		if (b == SND_UNVALID_AUTO.getValue())
			return SND_UNVALID_AUTO;
		if (b == SND_UNVALID_MANUAL.getValue())
			return SND_UNVALID_MANUAL;
		if (b == SND_VALID.getValue())
			return SND_VALID;

		logger.error("Unknown byte value mapping for status detail , mapping to unknown as a default behaviour");
		return SND_UNKNOWN;
	}

	/**
	 * return a String to be displayed to the user
	 */
	public String toUserString() {
		switch (this) {
		case SND_UNVALID_ACQUIS:
			return "Invalid acq.";
		case SND_UNVALID_AUTO:
			return "Invalid auto.";
		case SND_UNVALID_CONVERSION:
			return "Invalid at conversion.";
		case SND_UNVALID_MANUAL:
			return "Invalid manual";
		case SND_VALID:
			return "Valid";
		case SND_UNKNOWN:
		default:
			return "Undefined"; // Status missing

		}
	}

	/**
	 * check if a sounding is valid
	 */
	public static boolean isValid(MappingValidity value) {
		return value == SND_VALID;
	}

	/**
	 * check if a sounding is valid
	 */
	public static boolean isValid(byte value) {
		return value == SND_VALID.getValue();
	}

	/**
	 * check if a sounding is invalid
	 */
	public static boolean isInvalid(byte value) {
		return value >= SND_UNVALID_ACQUIS.getValue() && value <= SND_UNVALID_MANUAL.getValue();
	}

	/**
	 * check if a sounding is invalid
	 */
	public static boolean isInvalid(MappingValidity value) {
		return isInvalid(value.getValue());
	}

	/**
	 * check if a sounding is Unknown
	 */
	public static boolean isUnknown(byte value) {
		return value > SND_VALID.getValue() || value <= SND_UNKNOWN.getValue() || value == 0 || value == 1;
	}

	/**
	 * check if a sounding is Unknown
	 */
	public static boolean isUnknown(MappingValidity value) {
		return isUnknown(value.getValue());
	}

}
