
*File formats for spatial index

GLBeams

GLPoints :

x float
y float
z float
nx float
ny float
nz float
colorR float
colorG float
colorB float
colorA float

Total size = 40

Structure Fichier index


version byte
checksum long

sizes 255*255*long

offset 255*255*long


// organisé par case
cycleId int32 4
beam int16 2
depth float 4
latitude float 4 
longitude float 4
reflectivity double 8
validity byte 1
detection byte 1
editable byte 1

Total size= 29