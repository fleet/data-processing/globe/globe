/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.model.spatialindex.mapping;

import fr.ifremer.globe.editor.swath.model.spatialindex.Coordinates;

/**
 * Hold bathy attributes for one beam
 */
public class Beam {

	protected Coordinates gridCoords = new Coordinates();
	protected int cycleIndex;
	protected int beamIndex;
	protected float x = Float.NaN;
	protected float y = Float.NaN;
	protected float depth = Float.NaN;
	protected float reflectivity = Float.NaN;
	protected MappingValidity valid;
	protected byte detection;
	protected float qualityFactor = Float.NaN;

	/** Constructor */
	public Beam() {
		super();
	}

	/** Getter of {@link #gridCoords} */
	public Coordinates getGridCoords() {
		return gridCoords;
	}

	/** Getter of {@link #cycleIndex} */
	public int getCycleIndex() {
		return cycleIndex;
	}

	/** Setter of {@link #cycleIndex} */
	public void setCycleIndex(int cycleIndex) {
		this.cycleIndex = cycleIndex;
	}

	/** Getter of {@link #beamIndex} */
	public int getBeamIndex() {
		return beamIndex;
	}

	/** Setter of {@link #beamIndex} */
	public void setBeamIndex(int beamIndex) {
		this.beamIndex = beamIndex;
	}

	/** Getter of {@link #depth} */
	public float getDepth() {
		return depth;
	}

	/** Setter of {@link #depth} */
	public void setDepth(float depth) {
		this.depth = depth;
	}

	/** Getter of {@link #reflectivity} */
	public float getReflectivity() {
		return reflectivity;
	}

	/** Setter of {@link #reflectivity} */
	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}

	/** Getter of {@link #valid} */
	public MappingValidity getValid() {
		return valid;
	}

	/** Setter of {@link #valid} */
	public void setValid(MappingValidity valid) {
		this.valid = valid;
	}

	/** Getter of {@link #qualityFactor} */
	public float getQualityFactor() {
		return qualityFactor;
	}

	/** Setter of {@link #qualityFactor} */
	public void setQualityFactor(float qualityFactor) {
		this.qualityFactor = qualityFactor;
	}

	/** Getter of {@link #x} */
	public float getX() {
		return x;
	}

	/** Setter of {@link #x} */
	public void setX(float x) {
		this.x = x;
	}

	/** Getter of {@link #y} */
	public float getY() {
		return y;
	}

	/** Setter of {@link #y} */
	public void setY(float y) {
		this.y = y;
	}

	/** Getter of {@link #detection} */
	public byte getDetection() {
		return detection;
	}

	/** Setter of {@link #detection} */
	public void setDetection(byte detection) {
		this.detection = detection;
	}
}
