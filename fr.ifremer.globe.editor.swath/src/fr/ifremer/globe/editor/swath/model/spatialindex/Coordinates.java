package fr.ifremer.globe.editor.swath.model.spatialindex;
/**
 * Integer 2D Coordinates
 * */
public class Coordinates
{
	public int col;
	public int line;
	
	@Override
	public String toString()
	{
		return "row " +col + "line "+line;
	}
}
