/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.model.spatialindex;

import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;

/**
 * Utility class to process some operation on Coordinates
 */
public class CoordinatesUtils {

	/**
	 * Constructor
	 */
	private CoordinatesUtils() {
	}

	/** project a point in the grid and give it's cell coordinates */
	public static void project(Coordinates coord, float x, float y, SwathEditorParameters parameters) {
		coord.col = (int) Math.floor((x - parameters.mercatorGeoBox.getLeft()) / parameters.spatialResolution);
		coord.line = (int) Math.floor((y - parameters.mercatorGeoBox.getBottom()) / parameters.spatialResolution);
	}

	/** project a point in the grid and give it's cell coordinates */
	public static void project(Coordinates coord, double x, double y, SwathEditorParameters parameters) {
		coord.col = (int) Math.floor((x - parameters.mercatorGeoBox.getLeft()) / parameters.spatialResolution);
		coord.line = (int) Math.floor((y - parameters.mercatorGeoBox.getBottom()) / parameters.spatialResolution);
	}

	/** project a GeoPoint in the grid and give it's cell coordinates */
	public static void project(Coordinates coord, GeoPoint geoPoint, SwathEditorParameters parameters) {
		coord.col = (int) Math
				.floor((geoPoint.getLong() - parameters.mercatorGeoBox.getLeft()) / parameters.spatialResolution);
		coord.line = (int) Math
				.floor((geoPoint.getLat() - parameters.mercatorGeoBox.getBottom()) / parameters.spatialResolution);
	}

	/**
	 * check if the coordinates are inside the grid
	 */
	public static boolean contains(Coordinates coordinates, SwathEditorParameters parameters) {
		return coordinates.col >= 0 && coordinates.col < parameters.columnCount && coordinates.line >= 0
				&& coordinates.line < parameters.rowCount;
	}
}
