package fr.ifremer.globe.editor.swath.model.spatialindex.mapping;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionType;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.globe.utils.array.IIntArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.array.IShortArray;

/**
 * This class manages data buffers.
 */
public class Mappings implements Closeable {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(Mappings.class);

	public static final long OFFSET_INVALID = Long.MIN_VALUE;

	/** length of coordinate data in coordsArray as float */
	public static final int COORD_COUNT = 3;
	/** z index of X attribute in coordsArray. */
	protected static final int X_INDEX = 0;
	/** z index of Y attribute in coordsArray. */
	protected static final int Y_INDEX = 1;
	/** z index of Z attribute in coordsArray. */
	protected static final int DEPTH_INDEX = 2;

	/** Grid has two dimenions */
	public static final int GRID_DIMENSION = 2;
	/** index of line attribute in cellCoordsArray. */
	protected static final int LINE_INDEX = 0;
	/** index of column attribute in cellCoordsArray. */
	protected static final int COL_INDEX = 1;

	/** Swath's edition parameters */
	@Inject
	@Named(ContextNames.SWATH_EDITOR_PARAMETERS)
	protected SwathEditorParameters parameters;

	/** Factory of {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	/** Number of mapped soundings. */
	protected long soundingCount;

	/** Array to store projected geo coords and depth. */
	protected IFloatArray coordsArray;
	/** Array to store projected geo coords and depth. */
	protected IIntArray cellCoordsArray;
	/** Original array to store validity attribute. */
	protected IByteArray originalValidityArray;
	/** Array to store validity attribute. */
	protected IByteArray validityArray;
	/** Array to store reflectivity attribute. */
	protected IFloatArray reflectivityArray;
	/** Array to store detection attribute. */
	protected IByteArray detectionArray;
	/** Array to store detection attribute. */
	protected IFloatArray qualityFactorArray;
	/** Array to store cycleIndex attribute. */
	protected IIntArray cycleIndexArray;
	/** Array to store beamIndex attribute. */
	protected IShortArray beamIndexArray;
	/** Array to store offset of cells in other arrays. */
	protected ILongArray cellOffsetArray;
	/** Array to store the number of beam for each grid coords. */
	protected IIntArray cellBeamCountArray;
	/** Array to store the number min and max. */
	protected IDoubleArray minMaxArray;

	/** Validity file. */
	protected File validityFile;

	/**
	 * Constructor
	 */
	public Mappings() {
		super();
	}

	/**
	 * Creates buffer arrays (based on temporary mapped files).
	 */
	public void init(long totalBeamCount) throws IOException {
		// create arrays
		cycleIndexArray = arrayFactory.makeIntArray(totalBeamCount, "se_cycle_index");
		beamIndexArray = arrayFactory.makeShortArray(totalBeamCount, "se_beam_index");
		coordsArray = arrayFactory.makeFloatArray(totalBeamCount * COORD_COUNT, "se_coords");
		cellCoordsArray = arrayFactory.makeIntArray(totalBeamCount * GRID_DIMENSION, "se_cell_coords");
		validityArray = arrayFactory.makeByteArray(totalBeamCount, "se_validity");
		reflectivityArray = arrayFactory.makeFloatArray(totalBeamCount, "se_reflectivity");
		detectionArray = arrayFactory.makeByteArray(totalBeamCount, "se_detection_array");
		qualityFactorArray = arrayFactory.makeFloatArray(totalBeamCount, "se_quality_factor");
		cellOffsetArray = arrayFactory.makeLongArray(parameters.rowCount, parameters.columnCount, "se_cell_offset");
		cellBeamCountArray = arrayFactory.makeIntArray(parameters.rowCount, parameters.columnCount,
				"se_cell_beam_count");
		minMaxArray = arrayFactory.makeDoubleArray(8);

		// set byte order
		cycleIndexArray.setByteOrder(ByteOrder.nativeOrder());
		beamIndexArray.setByteOrder(ByteOrder.nativeOrder());
		coordsArray.setByteOrder(ByteOrder.nativeOrder());
		cellCoordsArray.setByteOrder(ByteOrder.nativeOrder());
		validityArray.setByteOrder(ByteOrder.nativeOrder());
		reflectivityArray.setByteOrder(ByteOrder.nativeOrder());
		detectionArray.setByteOrder(ByteOrder.nativeOrder());
		qualityFactorArray.setByteOrder(ByteOrder.nativeOrder());
		cellOffsetArray.setByteOrder(ByteOrder.nativeOrder());
		cellBeamCountArray.setByteOrder(ByteOrder.nativeOrder());
		minMaxArray.setByteOrder(ByteOrder.nativeOrder());

		// initialize values
		validityArray.fill(MappingValidity.SND_UNKNOWN.getValue());
		cellOffsetArray.fill(OFFSET_INVALID);
		cellBeamCountArray.fill(0);
		setMinRelectivity(Float.POSITIVE_INFINITY);
		setMaxRelectivity(Float.NEGATIVE_INFINITY);
		setMinDepth(Float.POSITIVE_INFINITY);
		setMaxDepth(Float.NEGATIVE_INFINITY);
		setMinCycleIndex(Integer.MAX_VALUE);
		setMaxCycleIndex(Integer.MIN_VALUE);
		setMinBeamIndex(Short.MAX_VALUE);
		setMaxBeamIndex(Short.MIN_VALUE);
	}

	/**
	 * Adds new data ({@link Beam}).
	 */
	public void addBeam(Beam beam) {
		addBeam(soundingCount, beam);
	}

	/**
	 * Adds new data ({@link Beam}) at the specified position.
	 */
	public void addBeam(long beamPosition, Beam beam) {
		int cycleIndex = beam.getCycleIndex();
		int beamIndex = beam.getBeamIndex();

		cycleIndexArray.putInt(beamPosition, cycleIndex);
		setMinCycleIndex(Math.min(getMinCycleIndex(), cycleIndex));
		setMaxCycleIndex(Math.max(getMaxCycleIndex(), cycleIndex));
		beamIndexArray.putShort(beamPosition, (short) beamIndex);
		setMinBeamIndex((short) Math.min(getMinBeamIndex(), beamIndex));
		setMaxBeamIndex((short) Math.max(getMaxBeamIndex(), beamIndex));

		coordsArray.putFloat(computeIndexForX(beamPosition), beam.getX());
		coordsArray.putFloat(computeIndexForY(beamPosition), beam.getY());
		coordsArray.putFloat(computeIndexForDepth(beamPosition), beam.getDepth());

		cellCoordsArray.putInt(computeIndexForLine(beamPosition), beam.getGridCoords().line);
		cellCoordsArray.putInt(computeIndexForColumn(beamPosition), beam.getGridCoords().col);

		setMinDepth(Math.min(getMinDepth(), beam.getDepth()));
		setMaxDepth(Math.max(getMaxDepth(), beam.getDepth()));

		validityArray.putByte(beamPosition, beam.getValid().getValue());

		reflectivityArray.putFloat(beamPosition, beam.getReflectivity());
		setMinRelectivity(Math.min(getMinRelectivity(), beam.getReflectivity()));
		setMaxRelectivity(Math.max(getMaxRelectivity(), beam.getReflectivity()));

		detectionArray.putByte(beamPosition, beam.getDetection());
		qualityFactorArray.putFloat(beamPosition, beam.getQualityFactor());

		long cellOffset = getCellOffset(beam.getGridCoords().line, beam.getGridCoords().col);
		if (cellOffset == OFFSET_INVALID) {
			cellOffsetArray.putLong(beam.getGridCoords().line, beam.getGridCoords().col, beamPosition);
		}

		cellBeamCountArray.putInt(beam.getGridCoords().line, beam.getGridCoords().col,
				cellBeamCountArray.getInt(beam.getGridCoords().line, beam.getGridCoords().col) + 1);

		soundingCount++;
	}

	/** Load the bathy attributes for one beam */
	public void getBeam(long beamIndex, Beam beam) {
		beam.setCycleIndex(getCycleIndex(beamIndex));
		beam.setBeamIndex(getBeamIndex(beamIndex));
		beam.setX(getX(beamIndex));
		beam.setY(getY(beamIndex));
		beam.setDepth(getDepth(beamIndex));
		beam.setDetection(getDetection(beamIndex));
		beam.setQualityFactor(getQualityFactor(beamIndex));
		beam.setReflectivity(getReflectivity(beamIndex));
		beam.setValid(getValidity(beamIndex));
		beam.getGridCoords().line = getLine(beamIndex);
		beam.getGridCoords().col = getCol(beamIndex);
	}

	/**
	 * @return the number of beam stored in files for the specified grid coords.
	 */
	public int getCellBeamCount(int line, int col) {
		return cellBeamCountArray.getInt(line, col);
	}

	/**
	 * @return the starting offset where soundings are stored in files for the specified grid coords.
	 */
	public long getCellOffset(int line, int col) {
		return cellOffsetArray.getLong(line, col);
	}

	/**
	 * @return the value of a X attribute
	 */
	public float getX(long beamIndex) {
		return coordsArray.getFloat(computeIndexForX(beamIndex));
	}

	/**
	 * @return the value of a X attribute
	 */
	public int getLine(long beamIndex) {
		return cellCoordsArray.getInt(computeIndexForLine(beamIndex));
	}

	/**
	 * set the value of a X attribute
	 */
	public void setX(long beamIndex, float value) {
		coordsArray.putFloat(computeIndexForX(beamIndex), value);
	}

	/**
	 * @return the value of a X attribute
	 */
	public float getX(int line, int col, int beamIndex) {
		float result = Float.NaN;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getX(cellOffset + beamIndex);
		}
		return result;
	}

	/**
	 * @return the value of a Y attribute
	 */
	public float getY(long beamIndex) {
		return coordsArray.getFloat(computeIndexForY(beamIndex));
	}

	/**
	 * @return the value of a X attribute
	 */
	public int getCol(long beamIndex) {
		return cellCoordsArray.getInt(computeIndexForColumn(beamIndex));
	}

	/**
	 * set the value of a Y attribute
	 */
	public void setY(long beamIndex, float value) {
		coordsArray.putFloat(computeIndexForY(beamIndex), value);
	}

	/**
	 * @return the value of a Y attribute
	 */
	public float getY(int line, int col, int cellBeamIndex) {
		float result = Float.NaN;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getY(cellOffset + cellBeamIndex);
		}
		return result;
	}

	/**
	 * @return the value of a depth attribute
	 */
	public float getDepth(long beamIndex) {
		return coordsArray.getFloat(computeIndexForDepth(beamIndex));
	}

	/**
	 * @return set the value of a depth attribute
	 */
	public void setDepth(long beamIndex, float value) {
		coordsArray.putFloat(computeIndexForDepth(beamIndex), value);
	}

	/**
	 * @return the value of a Depth attribute
	 */
	public float getDepth(int line, int col, int cellBeamIndex) {
		float result = Float.NaN;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getDepth(cellOffset + cellBeamIndex);
		}
		return result;
	}

	/**
	 * @return the value of a Validity attribute or Byte.MAX_VALUE if not set
	 */
	public MappingValidity getValidity(long beamIndex) {
		return beamIndex < soundingCount ? MappingValidity.valueOf(validityArray.getByte(beamIndex))
				: MappingValidity.SND_UNKNOWN;
	}

	/**
	 * @return the value of a Validity attribute or Byte.MAX_VALUE if not set
	 */
	public MappingValidity getValidity(int line, int col, int cellBeamIndex) {
		MappingValidity result = MappingValidity.SND_UNKNOWN;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getValidity(cellOffset + cellBeamIndex);
		}
		return result;
	}

	/**
	 * @return the value of a Reflectivity attribute
	 */
	public float getReflectivity(long beamIndex) {
		return reflectivityArray.getFloat(beamIndex);
	}

	/**
	 * @return the value of a Reflectivity attribute
	 */
	public float getReflectivity(int line, int col, int cellBeamIndex) {
		float result = Float.NaN;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getReflectivity(cellOffset + cellBeamIndex);
		}
		return result;
	}

	/**
	 * @return the value of a Detection attribute
	 */
	public byte getDetection(long beamIndex) {
		return detectionArray.getByte(beamIndex);
	}

	/**
	 * @return the value of a Quality Factor attribute
	 */
	public float getQualityFactor(long beamIndex) {
		return qualityFactorArray.getFloat(beamIndex);
	}

	/**
	 * @return the value of a Detection attribute
	 */
	public float getDetection(int line, int col, int cellBeamIndex) {
		float result = Float.NaN;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getDetection(cellOffset + cellBeamIndex);
		}
		return result;
	}

	/**
	 * @return the value of a Quality Factor attribute
	 */
	public float getQualityFactor(int line, int col, int cellBeamIndex) {
		float result = IConstants.MISSING_IFREMER_QUALITY_FACTOR;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getQualityFactor(cellOffset + cellBeamIndex);
		}

		return result;
	}

	/**
	 * @return the value of a CycleIndex attribute
	 */
	public int getCycleIndex(long beamIndex) {
		return cycleIndexArray.getInt(beamIndex);
	}

	/**
	 * @return the value of a CycleIndex attribute
	 */
	public int getCycleIndex(int line, int col, int cellBeamIndex) {
		int result = Integer.MIN_VALUE;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getCycleIndex(cellOffset + cellBeamIndex);
		}
		return result;
	}

	/**
	 * @return the value of a BeamIndex attribute
	 */
	public short getBeamIndex(long beamIndex) {
		return beamIndexArray.getShort(beamIndex);
	}

	/**
	 * @return the value of a CycleIndex attribute
	 */
	public short getBeamIndex(int line, int col, int cellBeamIndex) {
		short result = Short.MIN_VALUE;

		long cellOffset = getCellOffset(line, col);
		if (cellOffset >= 0) {
			result = getBeamIndex(cellOffset + cellBeamIndex);
		}
		return result;
	}

	/**
	 * @return the {@link #soundingCount}
	 */
	public long getSoundingCount() {
		return soundingCount;
	}

	/**
	 * @return the index of the X attribute in coordsArray for the specified sounding index.
	 */
	protected long computeIndexForX(long beamIndex) {
		return beamIndex * COORD_COUNT + X_INDEX;
	}

	/**
	 * @return the index of the Y attribute in coordsArray for the specified sounding index.
	 */
	protected long computeIndexForY(long beamIndex) {
		return beamIndex * COORD_COUNT + Y_INDEX;
	}

	/**
	 * @return the index of the Line attribute in cellCoordsArray for the specified sounding index.
	 */
	protected long computeIndexForLine(long beamIndex) {
		return beamIndex * GRID_DIMENSION + LINE_INDEX;
	}

	/**
	 * @return the index of the Column attribute in cellCoordsArray for the specified sounding index.
	 */
	protected long computeIndexForColumn(long beamIndex) {
		return beamIndex * GRID_DIMENSION + COL_INDEX;
	}

	/**
	 * @return the index of the X attribute in coordsArray for the specified sounding index.
	 */
	protected long computeIndexForDepth(long beamIndex) {
		return beamIndex * COORD_COUNT + DEPTH_INDEX;
	}

	/**
	 * Follow the link.
	 *
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() throws IOException {
		IOUtils.closeQuietly(coordsArray, cellCoordsArray, originalValidityArray, reflectivityArray, detectionArray,
				cycleIndexArray, beamIndexArray, cellOffsetArray, cellBeamCountArray, minMaxArray, qualityFactorArray);

		// Need to dump the ValidityArray because it may have been changed by
		// the Undo/Redo controler.
		if (getValidityFile() != null) {
			validityArray.dump(getValidityFile());
		}
		IOUtils.closeQuietly(validityArray);
	}

	/**
	 * @return the {@link #reflectivityArray}
	 */
	public IFloatArray getReflectivityArray() {
		return reflectivityArray;
	}

	/**
	 * @return the {@link #detectionArray}
	 */
	public IByteArray getDetectionArray() {
		return detectionArray;
	}

	/**
	 * @return the {@link #cycleIndexArray}
	 */
	public IIntArray getCycleIndexArray() {
		return cycleIndexArray;
	}

	/**
	 * @return the {@link #beamIndexArray}
	 */
	public IShortArray getBeamIndexArray() {
		return beamIndexArray;
	}

	/**
	 * @return the {@link #cellOffsetArray}
	 */
	public ILongArray getCellOffsetArray() {
		return cellOffsetArray;
	}

	/**
	 * @return the {@link #cellBeamCountArray}
	 */
	public IIntArray getCellBeamCountArray() {
		return cellBeamCountArray;
	}

	protected void setMinRelectivity(float value) {
		minMaxArray.putDouble(0, value);
	}

	protected void setMaxRelectivity(float value) {
		minMaxArray.putDouble(1, value);
	}

	protected void setMinDepth(float value) {
		minMaxArray.putDouble(2, value);
	}

	protected void setMaxDepth(float value) {
		minMaxArray.putDouble(3, value);
	}

	protected void setMinCycleIndex(int value) {
		minMaxArray.putDouble(4, value);
	}

	protected void setMaxCycleIndex(int value) {
		minMaxArray.putDouble(5, value);
	}

	protected void setMinBeamIndex(short value) {
		minMaxArray.putDouble(6, value);
	}

	protected void setMaxBeamIndex(short value) {
		minMaxArray.putDouble(7, value);
	}

	/**
	 * @return the min reflectivity value.
	 */
	public float getMinRelectivity() {
		return (float) minMaxArray.getDouble(0);
	}

	/**
	 * @return the maximum value of Reflectivity.
	 */
	public float getMaxRelectivity() {
		return (float) minMaxArray.getDouble(1);
	}

	/**
	 * @return the min depth value.
	 */
	public float getMinDepth() {
		return (float) minMaxArray.getDouble(2);
	}

	/**
	 * @return the maximum value of depth.
	 */
	public float getMaxDepth() {
		return (float) minMaxArray.getDouble(3);
	}

	/**
	 * @return the min reflectivity value.
	 */
	public int getMinCycleIndex() {
		return (int) minMaxArray.getDouble(4);
	}

	/**
	 * @return the maximum value of Reflectivity.
	 */
	public int getMaxCycleIndex() {
		return (int) minMaxArray.getDouble(5);
	}

	/**
	 * @return the min depth value.
	 */
	public short getMinBeamIndex() {
		return (short) minMaxArray.getDouble(6);
	}

	/**
	 * @return the maximum value of depth.
	 */
	public short getMaxBeamIndex() {
		return (short) minMaxArray.getDouble(7);
	}

	/**
	 * Indicates if beam has detection.
	 */
	public boolean isAmplitudeDetection(long index) {
		return getDetection(index) == DetectionType.AMPLITUDE.get();
	}

	/**
	 * @return the value of a Validity attribute
	 */
	public void setValidity(long index, MappingValidity value) {
		validityArray.putByte(index, value.getValue());
	}

	/**
	 * @return the {@link #validityArray}
	 */
	public IByteArray getValidityArray() {
		return validityArray;
	}

	/**
	 * @param validityArray the {@link #validityArray} to set
	 */
	public void setValidityArray(IByteArray validityArray) {
		this.validityArray = validityArray;
	}

	/**
	 * @return the {@link #validityFile}
	 */
	public File getValidityFile() {
		return validityFile;
	}

	/**
	 * @return the {@link #qualityFactorArray}
	 */
	public IFloatArray getQualityFactorArray() {
		return qualityFactorArray;
	}

	/**
	 * Getter of coordsArray
	 */
	public IFloatArray getCoordsArray() {
		return coordsArray;
	}

	/**
	 * Setter of coordsArray
	 */
	public void setCoordsArray(IFloatArray coordsArray) {
		this.coordsArray = coordsArray;
	}

	/***
	 * Project the coordinates passed as parameter and set the resulting X/Y/Depth values
	 */
	public void acceptGeographicalCoordinates(long beamIndex, double longitude, double latitude, float depth) {
		if (parameters.projection != null) {
			double[] lonlat = parameters.projection.projectQuietly(longitude, latitude);
			if (lonlat != null) {
				setX(beamIndex, (float) lonlat[0]);
				setY(beamIndex, (float) lonlat[1]);
				setDepth(beamIndex, depth);
			}
		}
	}

}