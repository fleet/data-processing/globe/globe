package fr.ifremer.globe.editor.swath.model.spatialindex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Registry of {@link SpatialIndex}.
 */
public class SpatialIndexRegistry {

	public static final Logger logger = LoggerFactory.getLogger(SpatialIndexRegistry.class);

	/** Indexers */
	private static Map<SounderDataContainer, SpatialIndex> indexers = new HashMap<>();

	/**
	 * Creates {@link SpatialIndex} for each {@link SounderDataContainer}.
	 */
	public void indexSounderFiles(final List<SounderDataContainer> containers, final IProgressMonitor monitor) throws GIOException {
		SubMonitor submonitor = SubMonitor.convert(monitor, "Load input files", containers.size() * 100);
		List<SounderDataContainer> recentlyIndexed = new ArrayList<>();// keep current indexed file (is case of cancel)

		for (final SounderDataContainer container : containers) {
			if (!indexers.containsKey(container)) {
				submonitor.subTask(String.format("Loading '%s'...", container.getInfo().getPath()));
				SpatialIndex newSpatialIndex = ContextInitializer.make(SpatialIndex.class);
				newSpatialIndex.initialize(container);

				// Fill spatial index
				if (newSpatialIndex.indexStore(submonitor.split(100))) {
					indexers.put(container, newSpatialIndex);
					recentlyIndexed.add(container);
				}
			}
			if (monitor.isCanceled()) // Canceled ?
				break;
		}

		if (monitor.isCanceled())
			recentlyIndexed.forEach(this::dispose);

		monitor.done();
	}

	/**
	 * @return the {@link SpatialIndex} associated to the specified {@link SounderDataContainer}.
	 */
	public SpatialIndex get(SounderDataContainer container) {
		return indexers.get(container);
	}

	/**
	 * Disposes the {@link SpatialIndex} associated to the specified container.
	 */
	public void dispose(SounderDataContainer container) {
		if (indexers.containsKey(container))
			indexers.remove(container).dispose();
	}
}
