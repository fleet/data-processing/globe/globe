package fr.ifremer.globe.editor.swath.model;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.swath.LayerType;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndexRegistry;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.ui.utils.Messages;

public class BathyDtmModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(BathyDtmModel.class);

	/** SpatialIndex provider */
	@Inject
	@Named(ContextNames.SPATIAL_INDEX_REGISTRY)
	protected SpatialIndexRegistry spatialIndexRegistry;

	/** Swath's edition parameters */
	@Inject
	@Named(ContextNames.SWATH_EDITOR_PARAMETERS)
	protected SwathEditorParameters parameters;

	/** Selected files */
	protected List<SounderDataContainer> dataContainers;

	/** Container for DTM layers. */
	protected final Map<LayerType, IndexedDtmLayer> layerContainer = new EnumMap<>(LayerType.class);

	/**
	 * Mandatory initialization after construction.
	 */
	public void initialize(List<SounderDataContainer> dataContainers) {
		this.dataContainers = dataContainers;
		Arrays.stream(LayerType.values()).forEach(layer -> layerContainer.put(layer, makeIndexedDtmLayer(layer)));
	}

	protected IndexedDtmLayer makeIndexedDtmLayer(LayerType layerType) {
		IndexedDtmLayer result = ContextInitializer.make(IndexedDtmLayer.class);
		result.initialize(dataContainers, layerType);
		return result;
	}

	/**
	 * Disposes resources retained by the model.
	 */
	@PreDestroy
	void preDestroy() {
		for (SounderDataContainer dataContainer : getSounderDataContainers())
			spatialIndexRegistry.dispose(dataContainer);
	}

	/**
	 * Saves current changes in input files.
	 */
	public boolean doSave(IProgressMonitor monitor) {
		int totalWork = getSounderDataContainers().size();
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Saving files...", totalWork * 100);
		for (SounderDataContainer dc : getSounderDataContainers()) {
			try {
				spatialIndexRegistry.get(dc).updateStore(subMonitor.split(100));
			} catch (Exception e) {
				String errorMsg = "Swath Editor : error occured while saving " + dc.getInfo().getPath() + ": "
						+ e.getMessage();
				Messages.openErrorMessage("Swath Editor error", errorMsg);
				LOGGER.error(errorMsg, e);
				return false;
			}
		}
		return true;
	}

	/** @return list of {@link IndexedDtmLayer} */
	public IndexedDtmLayer getIndexedDtmLayer(LayerType layerType) {
		return layerContainer.get(layerType);
	}

	/** @return input {@link SounderDataContainer} */
	public List<SounderDataContainer> getSounderDataContainers() {
		return dataContainers;
	}

}
