/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.model.spatialindex.mapping;

import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Filler of bathy attributes one Beam instance
 */
public class BeamFiller implements IAbstractBaseLayerOperation {
	/** Indexes of beam (cycle and beam indexes */
	protected int[] indexes;
	/** Instance to fill */
	protected Beam beam;

	/** Constructor */
	public BeamFiller(Beam beam) {
		this.beam = beam;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(DoubleLayer2D layer) throws GIOException {
		if (CompositeCSLayers.PROJ_DETECTION_Z.equals(layer.getName())) {
			beam.setDepth((float) (-layer.get(indexes[0], indexes[1])));
		}
	}

	/** {@inheritDoc} */
	@Override
	public void visit(FloatLayer2D layer) throws GIOException {
		if (BathymetryLayers.DETECTION_BACKSCATTER_R.getName().equals(layer.getName())) {
			beam.setReflectivity(layer.get(indexes[0], indexes[1]));
		} else if (BathymetryLayers.DETECTION_QUALITY_FACTOR.getName().equals(layer.getName())) {
			beam.setQualityFactor(layer.get(indexes[0], indexes[1]));
		}
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ByteLayer2D layer) throws GIOException {
		if (BathymetryLayers.DETECTION_TYPE.getName().equals(layer.getName())) {
			beam.setDetection(layer.get(indexes[0], indexes[1]));
		}
	}

	/** Getter of {@link #indexes} */
	public int[] getIndexes() {
		return indexes;
	}

	/** Setter of {@link #indexes} */
	public void setIndexes(int[] indexes) {
		this.indexes = indexes;
	}
}
