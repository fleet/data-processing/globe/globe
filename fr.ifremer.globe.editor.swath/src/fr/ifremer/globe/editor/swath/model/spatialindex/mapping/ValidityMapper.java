package fr.ifremer.globe.editor.swath.model.spatialindex.mapping;

import java.util.EnumSet;

import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatusDetail;
import fr.ifremer.globe.utils.exception.GIOException;

public class ValidityMapper {

	private ValidityMapper() {
		// utility class
	}

	////// FROM MODEL TO SPATIAL

	/**
	 * get the spatial status validity from model validity
	 */
	public static MappingValidity get(DetectionStatusLayer statusLayer, int iSwath, int iBeam) throws GIOException {

		if (statusLayer.isValid(iSwath, iBeam))
			return MappingValidity.SND_VALID;

		EnumSet<SonarDetectionStatus> status = statusLayer.getStatus(iSwath, iBeam);

		if (status.contains(SonarDetectionStatus.INVALID_ACQUISITION))
			return MappingValidity.SND_UNVALID_ACQUIS;

		if (status.contains(SonarDetectionStatus.INVALID_CONVERSION))
			return MappingValidity.SND_UNVALID_ACQUIS;

		if (status.contains(SonarDetectionStatus.INVALID_SOUNDING_ROW)
				|| status.contains(SonarDetectionStatus.INVALID_SWATH))
			return MappingValidity.SND_UNKNOWN;

		if (status.contains(SonarDetectionStatus.REJECTED)) {
			switch (statusLayer.getDetails(iSwath, iBeam)) {
			case AUTO:
				return MappingValidity.SND_UNVALID_AUTO;
			case DOUBTFULL:
				return MappingValidity.SND_UNKNOWN;
			case MANUAL:
				return MappingValidity.SND_UNVALID_MANUAL;
			case UNKNOWN:
				return MappingValidity.SND_UNKNOWN;
			default:
				break;

			}
		}
		// by default
		return MappingValidity.SND_UNKNOWN;
	}

	//// FROM SPATIAL TO MODEL

	/**
	 * Updates a {@link DetectionStatusLayer} with a byte {@link MappingValidity} value only manual and automatic
	 * modification are supposed to occurs
	 */
	public static void updateStatusLayer(byte mappingValue, DetectionStatusLayer statusLayer, int iSwath, int iBeam)
			throws GIOException {
		switch (MappingValidity.valueOf(mappingValue)) {
		case SND_UNVALID_AUTO:
			statusLayer.enableFlag(iSwath, iBeam, SonarDetectionStatus.REJECTED);
			statusLayer.setDetails(iSwath, iBeam, SonarDetectionStatusDetail.AUTO);
			break;
		case SND_UNVALID_MANUAL:
			statusLayer.enableFlag(iSwath, iBeam, SonarDetectionStatus.REJECTED);
			statusLayer.setDetails(iSwath, iBeam, SonarDetectionStatusDetail.MANUAL);
			break;
		case SND_VALID:
			statusLayer.disableFlag(iSwath, iBeam, SonarDetectionStatus.REJECTED);
			statusLayer.setDetails(iSwath, iBeam, SonarDetectionStatusDetail.UNKNOWN);
			break;
		default:
			break;
		}
	}

	/**
	 * @return the DetectionStatus according to the specified MappingValidity
	 */
	public static SonarDetectionStatus getStatus(MappingValidity validity) {
		// Sound validity
		switch (validity) {
		case SND_UNVALID_MANUAL:
			return SonarDetectionStatus.REJECTED;
		// statusDetails = SonarDetectionStatusDetail.MANUAL.getValue();
		case SND_UNVALID_ACQUIS:
			return SonarDetectionStatus.INVALID_ACQUISITION;
		case SND_UNVALID_AUTO:
			return SonarDetectionStatus.REJECTED;
		// statusDetails = SonarDetectionStatusDetail.AUTO.getValue();
		case SND_UNVALID_CONVERSION:
			return SonarDetectionStatus.INVALID_CONVERSION;
		case SND_UNKNOWN:
			return SonarDetectionStatus.REJECTED;
		case SND_VALID:
		default:
			return SonarDetectionStatus.VALID;
		}
	}

	/**
	 * @return the DetectionStatus according to the specified MappingValidity
	 */
	public static SonarDetectionStatusDetail getStatusDetails(MappingValidity validity) {
		// Sound validity
		switch (validity) {
		case SND_UNVALID_MANUAL:
			return SonarDetectionStatusDetail.MANUAL;
		case SND_UNVALID_AUTO:
			return SonarDetectionStatusDetail.AUTO;
		case SND_UNKNOWN:
		case SND_UNVALID_CONVERSION:
		case SND_VALID:
		case SND_UNVALID_ACQUIS:
		default:
			return SonarDetectionStatusDetail.UNKNOWN;
		}
	}
}
