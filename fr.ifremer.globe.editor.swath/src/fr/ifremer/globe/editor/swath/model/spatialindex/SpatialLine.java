package fr.ifremer.globe.editor.swath.model.spatialindex;

/**
 * Simple class defining a line
 */
public class SpatialLine {
	private static final int INVALID = -1;
	private int line;
	/**
	 * first point
	 */
	private int colStart = INVALID; // first point

	/**
	 * last point of the line (included)
	 */
	private int colEnd = INVALID;

	public SpatialLine(int line) {
		this.line = line;
	}

	/**
	 * add the colonne to the line definition
	 */
	public void push(int col) {
		if (getColStart() == INVALID)
			colStart = col;
		colEnd = col;
	}

	/**
	 * check if the line is empty a line is considered as not empty if its start point is equal to its end point
	 */
	public boolean empty() {
		return getColStart() == INVALID;
	}

	/**
	 * the last column included
	 */
	public int getColEnd() {
		return colEnd;
	}

	public int getColStart() {
		return colStart;
	}

	public int getLine() {
		return line;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " line " + getLine() + " col " + getColStart() + ":" + getColEnd();
	}

}