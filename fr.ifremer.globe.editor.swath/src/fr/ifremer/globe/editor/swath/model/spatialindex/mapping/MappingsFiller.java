/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.model.spatialindex.mapping;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.ILayersConsumer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalkerParameters.AbstractBaseLayersSupplier;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalkerParameters.IndexesPredicate;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.editor.swath.model.spatialindex.Coordinates;
import fr.ifremer.globe.editor.swath.model.spatialindex.CoordinatesUtils;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Used by a LayersWalker to fill a Mappings instance.
 */
public class MappingsFiller implements ILayersConsumer<ISounderNcInfo, SounderDataContainer>,
		AbstractBaseLayersSupplier<ISounderNcInfo, SounderDataContainer>,
		IndexesPredicate<ISounderNcInfo, SounderDataContainer> {

	/** Swath's edition parameters */
	protected SwathEditorParameters parameters;

	/** Mappings to fill */
	protected Mappings mappings;

	/** layer of latitude */
	protected DoubleLayer2D latitudeLayer;
	/** layer of longitude */
	protected DoubleLayer2D longitudeLayer;
	/** layer of status */
	protected DetectionStatusLayer statusLayer;

	/** Reusable object to projecte lon/lat of all beams of one cycle */
	protected double[][] xy;
	/** Reusable object to hold all beam data */
	protected Beam beam = new Beam();
	/** Reusable object to hold all beam data */
	protected BeamFiller beamFiller = new BeamFiller(beam);

	/** Constructor */
	public MappingsFiller(Mappings mappings, SwathEditorParameters parameters) {
		this.mappings = mappings;
		this.parameters = parameters;
	}

	@Override
	public List<AbstractBaseLayer<? extends AbstractBuffer>> apply(SounderDataContainer sounderDataContainer)
			throws GIOException {

		List<AbstractBaseLayer<? extends AbstractBuffer>> result = new ArrayList<>();
		// ampPhaseDetection
		sounderDataContainer.getOptionalLayer(BathymetryLayers.DETECTION_TYPE).ifPresent(result::add);
		// Reflectivity
		sounderDataContainer.getOptionalLayer(BathymetryLayers.DETECTION_BACKSCATTER_R).ifPresent(result::add);
		// qualityFactor
		sounderDataContainer.getOptionalLayer(BathymetryLayers.DETECTION_QUALITY_FACTOR).ifPresent(result::add);

		CompositeCSLayers cSLayers = sounderDataContainer.getCsLayers(CoordinateSystem.FCS);
		latitudeLayer = cSLayers.getProjectedX();
		longitudeLayer = cSLayers.getProjectedY();
		result.add(cSLayers.getProjectedZ());

		statusLayer = sounderDataContainer.getStatusLayer();

		return result;
	}

	/** Process is starting. Initialize attributes */
	@Override
	public <T extends AbstractBaseLayer<?>> void getReady(SounderDataContainer dataContainer, List<T> layers)
			throws GIOException {
		xy = new double[dataContainer.getInfo().getTotalRxBeamCount()][2];
	}

	/** Starting next Swath (levelIndex == 0) or beam (levelIndex == 1) */
	@Override
	public void initiateLevel(int levelIndex, int[] indexes) throws GIOException {
		// For optimization purpose, project all beam of one cycle
		if (levelIndex == 0) {
			for (int i = 0; i < xy.length; i++) {
				xy[i][0] = longitudeLayer.get(indexes[0], i);
				xy[i][1] = latitudeLayer.get(indexes[0], i);
			}
			parameters.projection.projectQuietly(xy);
		} else {
			beam.setCycleIndex(indexes[0]);
			beam.setBeamIndex(indexes[1]);
			beam.setX((float) xy[beam.getBeamIndex()][0]);
			beam.setY((float) xy[beam.getBeamIndex()][1]);
			beam.setValid(ValidityMapper.get(statusLayer, beam.getCycleIndex(), beam.getBeamIndex()));
		}
	}

	/** Loop on layers for the current beam */
	@Override
	public <T extends AbstractBaseLayer<?>> void processLayer(int[] indexes, T layer) throws GIOException {
		beamFiller.setIndexes(indexes);
		layer.process(beamFiller);
	}

	/** @return true to send the current beam to the Mappings */
	@Override
	public boolean test(SounderDataContainer sounderDataContainer, int[] indexes) throws GIOException {
		Coordinates coord = beam.getGridCoords();
		CoordinatesUtils.project(coord, xy[indexes[1]][0], xy[indexes[1]][1], parameters);
		return CoordinatesUtils.contains(coord, parameters) && statusLayer.isDetectionEditable(indexes[0], indexes[1]);
	}

	/** End of the beam filling. */
	@Override
	public void endProcessLayers() throws GIOException {
		mappings.addBeam(beam);
	}
}
