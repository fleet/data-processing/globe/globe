﻿package fr.ifremer.globe.editor.swath.model.spatialindex;

import java.io.IOException;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalker;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalkerParameters;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Beam;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingsFiller;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.ValidityMapper;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.array.IIntArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class contains a {@link Mappings} of data in spatial coordinates (grid column / line).
 */
public class SpatialIndex {

	public static final Logger logger = LoggerFactory.getLogger(SpatialIndex.class);

	/** Swath's edition parameters */
	@Inject
	@Named(ContextNames.SWATH_EDITOR_PARAMETERS)
	protected SwathEditorParameters parameters;

	/** Utility instance to run through layers */
	@Inject
	protected LayersWalker layersWalker;

	/** Utility instance of IArray factory */
	@Inject
	protected IArrayFactory arrayFactory;

	/** Container of mappings */
	protected Mappings mappings;

	protected SounderDataContainer sounderDataContainer;

	/**
	 * Executed after dependency injection is done
	 */
	@PostConstruct
	void postConstruct() {
		mappings = ContextInitializer.make(Mappings.class);
	}

	/**
	 * Build a new {@link SpatialIndex} for a given file, and grid definition
	 */
	public void initialize(SounderDataContainer container) {
		sounderDataContainer = container;
	}

	/**
	 * Disposes the wrapped {@link Mappings}.
	 */
	public void dispose() {
		IOUtils.closeQuietly(mappings);
	}

	/**
	 * Get the number of object in a part of a line
	 *
	 * @param line the line considered
	 * @return the number of soundings in the line part
	 * @throws GIOException
	 */
	public int getSoundingCount(SpatialLine line) {
		int count = 0;
		for (int col = line.getColStart(); col <= line.getColEnd(); col++) {
			count += mappings.getCellBeamCount(line.getLine(), col);
		}
		return count;
	}

	/**
	 * @return sounding count by cell.
	 */
	public int getSoundingCount(int col, int line) {
		return mappings.getCellBeamCount(line, col);
	}

	/**
	 * Computes grid {@link Mappings} from {@link SounderDataContainer}.
	 */
	public boolean indexStore(IProgressMonitor monitor) throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Indexing task", 1_000);

		// Perform the files indexation.
		try (Mappings tmpBuffers = ContextInitializer.make(Mappings.class);
				ILongArray lineOffset = arrayFactory.makeLongArray(parameters.rowCount, parameters.columnCount,
						"se_lineOffset")) {
			// Get data in temporary buffer array ordered by swath / beam
			extractBathyDatas(tmpBuffers, subMonitor.split(450));

			// Put data in buffer array ordered by grid column / line
			computeOffsets(tmpBuffers, lineOffset, subMonitor.split(100));
			reorderMappingsInGridCoordinates(tmpBuffers, lineOffset, subMonitor.split(450));

		} catch (OperationCanceledException e) {
			dispose();
			return false;
		} catch (IOException e) {
			dispose();
			throw new GIOException("IOException : " + e.getMessage(), e);
		}
		return true;
	}

	/**
	 * Extract data from a SounderDataContainer and write them into the temporary Mappings.
	 */
	protected void extractBathyDatas(Mappings bathyData, IProgressMonitor monitor) throws GIOException, IOException {
		int cycleCount = sounderDataContainer.getInfo().getCycleCount();
		int beamCount = sounderDataContainer.getInfo().getTotalRxBeamCount();
		bathyData.init(cycleCount * (long) beamCount);
		MappingsFiller mappingsFiller = new MappingsFiller(bathyData, parameters);
		LayersWalkerParameters<ISounderNcInfo, SounderDataContainer> layerParameters = new LayersWalkerParameters<>(
				sounderDataContainer.getInfo(), mappingsFiller, mappingsFiller);
		layerParameters.setIndexesPredicate(mappingsFiller);
		layersWalker.walk(sounderDataContainer, layerParameters, monitor);
	}

	/**
	 * Compute offsets of each cells.
	 *
	 * At this step, the sounding count by cell is known, so it's possible to evaluate the offset in the future buffers
	 * for each cell.
	 */
	private void computeOffsets(Mappings temporaryMapping, ILongArray result, IProgressMonitor monitor) {
		// offset will be written from the starting point of the sounding area
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Filling offset", parameters.rowCount);
		long currentOffset = 0;
		for (int line = 0; !monitor.isCanceled() && line < parameters.rowCount; line++) {
			for (int col = 0; col < parameters.columnCount; col++) {
				int count = temporaryMapping.getCellBeamCount(line, col);
				result.putLong(line, col, currentOffset);
				currentOffset += count;
			}
			subMonitor.worked(1);
		}
	}

	/**
	 * Reorder data from a {@link Mappings} indexed by swath/beam to a mapping to a new {@link Mappings} indexed by grid
	 * col/line.
	 */
	private void reorderMappingsInGridCoordinates(Mappings tempBathyData, ILongArray lineOffset,
			IProgressMonitor monitor) throws IOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Finalizing offset",
				(int) tempBathyData.getSoundingCount());

		mappings.init(tempBathyData.getSoundingCount());
		try (IIntArray addedSoundings = arrayFactory.makeIntArray(parameters.rowCount, parameters.columnCount,
				"se_addedSoundings")) {

			int lastpercent = 0;// used to increment progress bar
			Beam beam = new Beam();
			for (long absoluteBeamIndex = 0; !monitor.isCanceled()
					&& absoluteBeamIndex < tempBathyData.getSoundingCount(); absoluteBeamIndex++) {

				tempBathyData.getBeam(absoluteBeamIndex, beam);

				int count = addedSoundings.getInt(beam.getGridCoords().line, beam.getGridCoords().col);
				addedSoundings.putInt(beam.getGridCoords().line, beam.getGridCoords().col, count + 1);

				// compute the offset in the mapping in number of object
				long offsetFromStartingPoint = lineOffset.getLong(beam.getGridCoords().line, beam.getGridCoords().col)
						+ count;
				// now copy one data
				mappings.addBeam(offsetFromStartingPoint, beam);
				int percent = (int) (absoluteBeamIndex * 100 / tempBathyData.getSoundingCount());
				if (lastpercent != percent) {
					lastpercent = percent;
				}
				subMonitor.worked(1);
			}
		}
	}

	/**
	 * Write back data in {@link SounderDataContainer} (MBG/XSF files)
	 */
	public void updateStore(IProgressMonitor monitor) throws GException, IOException {
		int cycleCount = sounderDataContainer.getInfo().getCycleCount();
		int beamCount = sounderDataContainer.getInfo().getTotalRxBeamCount();

		try (IByteArray orderedValidityArray = arrayFactory.makeByteArray(cycleCount, beamCount,
				"se_detection_validity")) {
			DetectionStatusLayer statusLayer = sounderDataContainer.getStatusLayer();

			// Temporary array containing validity flag ordered by cycle/beam
			orderedValidityArray.fill(MappingValidity.SND_UNKNOWN.getValue());

			// Filling the temporary array
			long beamIndexCount = mappings.getSoundingCount();
			monitor.beginTask("Preparing...", (int) (beamIndexCount / 1000) + cycleCount);
			for (int i = 0; i < beamIndexCount; i++) {
				short beamIndex = mappings.getBeamIndex(i);
				int cycleIndex = mappings.getCycleIndex(i);
				orderedValidityArray.putByte(cycleIndex, beamIndex, mappings.getValidity(i).getValue());
				if (i > 0 && i % 1000 == 0) {
					monitor.worked(1);
				}
			}

			// browse the temporary array and write all validity flag into the dataStore
			for (int cycleIndex = 0; cycleIndex < cycleCount; cycleIndex++) {
				for (int beamIndex = 0; beamIndex < beamCount; beamIndex++) {
					byte validity = orderedValidityArray.getByte(cycleIndex, beamIndex);
					if (validity != MappingValidity.SND_UNKNOWN.getValue())
						ValidityMapper.updateStatusLayer(validity, statusLayer, cycleIndex, beamIndex);
				}
				monitor.worked(1);
			}

			statusLayer.flush();

			// set "ManualCorrection" flag on
			Optional<ProcessAssessor> processAssessor = sounderDataContainer.getInfo().getProcessAssessor();
			if (processAssessor.isPresent())
				processAssessor.get().heedManualCleaning();

			monitor.done();
		}
	}

	/**
	 * @return the {@link #mappings}
	 */
	public Mappings getMappings() {
		return mappings;
	}

	/**
	 * @return the {@link #info}
	 */
	public ISounderNcInfo getSounderDriverInfo() {
		return sounderDataContainer.getInfo();
	}

}
