package fr.ifremer.globe.editor.swath.model;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderNavigationData;
import fr.ifremer.globe.utils.exception.GIOException;

public class NavigationModel {
	
	private List<INavigationData> navigationDataProxies = new ArrayList<>();

	/**
	 * Mandatory initialization after construction.
	 * @throws GIOException 
	 */
	public void initialize(List<SounderDataContainer> dataContainers) throws GIOException {
		for (SounderDataContainer dataContainer : dataContainers) {
			navigationDataProxies.add(new SounderNavigationData(dataContainer));
		}
	}
	
	public List<INavigationData> getNavigationDataProxies() {
		return navigationDataProxies;
	}

}
