package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geometry.CartesianPoint;
import fr.ifremer.globe.core.model.geometry.OrientedBox;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.Coordinates;
import fr.ifremer.globe.editor.swath.model.spatialindex.CoordinatesUtils;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialLine;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility parsing a grid and return the set of point intersecting this grid works only if the projection choosen is a
 * lat long projection
 *
 * Use JTS library which is a way faster than Gdal for geometry computing
 */
public class GridIntersector {

	public static final Logger logger = LoggerFactory.getLogger(GridIntersector.class);

	/** Swath's edition parameters */
	@Inject
	@Named(ContextNames.SWATH_EDITOR_PARAMETERS)
	protected SwathEditorParameters parameters;

	/**
	 * Create a polygon from a list of points
	 */
	private Polygon createPolygon(List<CartesianPoint> points) {
		GeometryFactory geometryFactory = new GeometryFactory();
		Coordinate[] coord = new Coordinate[points.size() + 1];
		int i = 0;
		for (CartesianPoint geoPoint : points) {
			coord[i] = new Coordinate(geoPoint.getX(), geoPoint.getY());
			i++;
		}
		coord[i] = new Coordinate(points.get(0).getX(), points.get(0).getY());
		LinearRing linear = new GeometryFactory().createLinearRing(coord);
		return new Polygon(linear, null, geometryFactory);
	}

	/**
	 * compute the list of lines intersecting the given geobox
	 *
	 * @param grid the grid definition
	 * @param box the oriented geobox
	 * @return the list of lines {@link SpatialLine} intersecting the geobox
	 * @throws GIOException
	 */
	public List<SpatialLine> intersect(OrientedBox box, SpatialIndex index) {
		// compute min man value so that we can evaluate intersecting mapping coordinates indexes

		double maxX = box.get(0).getX();
		double minX = box.get(0).getX();
		double minY = box.get(0).getY();
		double maxY = box.get(0).getY();
		for (CartesianPoint x : box.getPoints()) {
			maxX = Math.max(maxX, x.getX());
			minX = Math.min(minX, x.getX());
			maxY = Math.max(maxY, x.getY());
			minY = Math.min(minY, x.getY());
		}

		Coordinates coordmin = new Coordinates();
		Coordinates coordmax = new Coordinates();
		CoordinatesUtils.project(coordmin, (float) minX, (float) minY, parameters);
		CoordinatesUtils.project(coordmax, (float) maxX, (float) maxY, parameters);
		Polygon polygon = createPolygon(box.getPoints());
		ArrayList<SpatialLine> instersectingLines = new ArrayList<>(Math.max(coordmax.line - coordmin.line, 1));
		for (int line = Math.max(coordmin.line, 0); line <= coordmax.line && line < parameters.rowCount; line++) {
			SpatialLine myLine = new SpatialLine(line);
			for (int col = Math.max(coordmin.col, 0); col <= coordmax.col && col < parameters.columnCount; col++) {
				// compute the polygon matching the considered cell
				CartesianPoint p1 = new CartesianPoint(
						parameters.spatialResolution * col + parameters.mercatorGeoBox.getLeft(),
						line * parameters.spatialResolution + parameters.mercatorGeoBox.getBottom());
				CartesianPoint p2 = new CartesianPoint(
						parameters.spatialResolution * col + parameters.mercatorGeoBox.getLeft(),
						(line + 1) * parameters.spatialResolution + parameters.mercatorGeoBox.getBottom());
				CartesianPoint p3 = new CartesianPoint(
						parameters.spatialResolution * (col + 1) + parameters.mercatorGeoBox.getLeft(),
						(line + 1) * parameters.spatialResolution + parameters.mercatorGeoBox.getBottom());
				CartesianPoint p4 = new CartesianPoint(
						parameters.spatialResolution * col + parameters.mercatorGeoBox.getLeft(),
						(line + 1) * parameters.spatialResolution + parameters.mercatorGeoBox.getBottom());

				Polygon cell = createPolygon(Arrays.asList(p1, p2, p3, p4));
				if (polygon.intersects(cell)) {
					if (index.getSoundingCount(col, line) > 0) {
						myLine.push(col);
					}

				} else {
					// rejection
					if (!myLine.empty()) {
						// we add at least one point, and this one is no longer valid,
						// since the box is convex, we can deduced that the following points for the current line will
						// not be in box
						break;
					}
				}

			}
			if (!myLine.empty()) {
				instersectingLines.add(myLine);
			}
		}
		// dump the lines content
		if (instersectingLines.isEmpty()) {
			logger.debug("Swath Editor No selected area");
		}
		if (logger.isTraceEnabled()) {
			for (SpatialLine line : instersectingLines) {
				logger.trace(line.toString());
			}
		}
		return instersectingLines;
	}
}
