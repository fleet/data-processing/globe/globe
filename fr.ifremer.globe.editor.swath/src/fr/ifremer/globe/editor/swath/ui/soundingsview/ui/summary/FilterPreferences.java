package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary;

import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.ui.databinding.observable.WritableByte;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableFloat;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;

/**
 * Contains filter preferences of {@link FiltersDialog}.
 */
public class FilterPreferences {

	protected WritableInt minCycleIndex = new WritableInt();
	protected WritableInt maxCycleIndex = new WritableInt();
	protected WritableInt minBeamIndex = new WritableInt();
	protected WritableInt maxBeamIndex = new WritableInt();
	protected WritableFloat minDepth = new WritableFloat();
	protected WritableFloat maxDepth = new WritableFloat();
	protected WritableFloat minReflectivity = new WritableFloat();
	protected WritableFloat maxReflectivity = new WritableFloat();
	protected WritableDouble minLongitude = new WritableDouble();
	protected WritableDouble maxLongitude = new WritableDouble();
	protected WritableDouble minLatitude = new WritableDouble();
	protected WritableDouble maxLatitude = new WritableDouble();
	protected WritableObject<MappingValidity> validity = new WritableObject<>(null);
	protected WritableByte detection = new WritableByte();

	/** Bounds */
	protected int[] cycleIndexRange;
	protected int[] beamIndexRange;
	protected float[] depthRange;
	protected float[] reflectivityRange;
	protected float[] xRange;
	protected float[] yRange;
	protected double[] longitudeRange;
	protected double[] latitudeRange;

	/** Constructor */
	public FilterPreferences() {
		reset();
		clearBounds();
	}

	public void reset() {
		Display.getDefault().syncExec(() -> {
			minCycleIndex.setToNull();
			maxCycleIndex.setToNull();
			minBeamIndex.setToNull();
			maxBeamIndex.setToNull();
			minDepth.setToNull();
			maxDepth.setToNull();
			minReflectivity.setToNull();
			maxReflectivity.setToNull();
			minLongitude.setToNull();
			maxLongitude.setToNull();
			minLatitude.setToNull();
			maxLatitude.setToNull();
			detection.setValue(null);
			validity.set(null);
		});
	}

	/** Getter of {@link #validity} */
	public WritableObject<MappingValidity> getValidity() {
		return validity;
	}

	/** Getter of {@link #minCycleIndex} */
	public WritableInt getMinCycleIndex() {
		return minCycleIndex;
	}

	/** Getter of {@link #maxCycleIndex} */
	public WritableInt getMaxCycleIndex() {
		return maxCycleIndex;
	}

	/** Getter of {@link #minBeamIndex} */
	public WritableInt getMinBeamIndex() {
		return minBeamIndex;
	}

	/** Getter of {@link #maxBeamIndex} */
	public WritableInt getMaxBeamIndex() {
		return maxBeamIndex;
	}

	/** Getter of {@link #minDepth} */
	public WritableFloat getMinDepth() {
		return minDepth;
	}

	/** Getter of {@link #maxDepth} */
	public WritableFloat getMaxDepth() {
		return maxDepth;
	}

	/** Getter of {@link #minReflectivity} */
	public WritableFloat getMinReflectivity() {
		return minReflectivity;
	}

	/** Getter of {@link #maxReflectivity} */
	public WritableFloat getMaxReflectivity() {
		return maxReflectivity;
	}

	/** Getter of {@link #detection} */
	public WritableByte getDetection() {
		return detection;
	}

	/** Getter of {@link #minLongitude} */
	public WritableDouble getMinLongitude() {
		return minLongitude;
	}

	/** Getter of {@link #maxLongitude} */
	public WritableDouble getMaxLongitude() {
		return maxLongitude;
	}

	/** Getter of {@link #minLatitude} */
	public WritableDouble getMinLatitude() {
		return minLatitude;
	}

	/** Getter of {@link #maxLatitude} */
	public WritableDouble getMaxLatitude() {
		return maxLatitude;
	}

	public void setToBounds() {
		minCycleIndex.set(cycleIndexRange[0]);
		maxCycleIndex.set(cycleIndexRange[1]);

		minBeamIndex.set(beamIndexRange[0]);
		maxBeamIndex.set(beamIndexRange[1]);

		minDepth.setValue(Float.isFinite(depthRange[0]) ? depthRange[0] : null);
		maxDepth.setValue(Float.isFinite(depthRange[1]) ? depthRange[1] : null);

		minReflectivity.setValue(Float.isFinite(reflectivityRange[0]) ? reflectivityRange[0] : null);
		maxReflectivity.setValue(Float.isFinite(reflectivityRange[1]) ? reflectivityRange[1] : null);

		minLongitude.setValue(Double.isFinite(longitudeRange[0]) ? longitudeRange[0] : null);
		maxLongitude.setValue(Double.isFinite(longitudeRange[1]) ? longitudeRange[1] : null);

		minLatitude.setValue(Double.isFinite(latitudeRange[0]) ? latitudeRange[0] : null);
		maxLatitude.setValue(Double.isFinite(latitudeRange[1]) ? latitudeRange[1] : null);
	}

	public void clearBounds() {
		cycleIndexRange = new int[] { Integer.MAX_VALUE, Integer.MIN_VALUE };
		beamIndexRange = new int[] { Integer.MAX_VALUE, Integer.MIN_VALUE };
		depthRange = new float[] { Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY };
		reflectivityRange = new float[] { Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY };
		xRange = new float[] { Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY };
		yRange = new float[] { Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY };
		longitudeRange = new double[] { Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY };
		latitudeRange = new double[] { Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY };
	}

	public void push(SummaryItem item) {
		push(cycleIndexRange, item.getCycleIndex());
		push(beamIndexRange, item.getBeamIndex());
		push(depthRange, item.getDepth());
		push(reflectivityRange, item.getReflectivity());
		push(xRange, item.getX());
		push(yRange, item.getY());
		push(longitudeRange, item.getLongitude());
		push(latitudeRange, item.getLatitude());
	}

	protected void push(int[] range, int value) {
		range[0] = Math.min(range[0], value);
		range[1] = Math.max(range[1], value);
	}

	protected void push(float[] range, float value) {
		if (!Float.isNaN(value)) {
			range[0] = Math.min(range[0], value);
			range[1] = Math.max(range[1], value);
		}
	}

	protected void push(double[] range, double value) {
		if (!Double.isNaN(value)) {
			range[0] = Math.min(range[0], value);
			range[1] = Math.max(range[1], value);
		}
	}

}