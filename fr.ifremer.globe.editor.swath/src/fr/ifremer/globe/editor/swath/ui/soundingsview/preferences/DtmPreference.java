package fr.ifremer.globe.editor.swath.ui.soundingsview.preferences;

import java.awt.Color;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DoublePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.triangulation.TriangulationFilteringPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.validator.BeamValidatorPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.biascorrection.BiasCorrectionView;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary.SummaryView;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox.ManageFilesViewPart;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

public class DtmPreference extends PreferenceComposite {
	public static final String DETACHED_VIEW = "Detached";

	private final ColorPreferenceAttribute dtmSelectionColor;
	private final IntegerPreferenceAttribute dtmSelectionLineWidth;
	private final ColorPreferenceAttribute designatedBeamsColor;

	private final IntegerPreferenceAttribute overlap;

	private final IntegerPreferenceAttribute viewSplitRatio;
	private final IntegerPreferenceAttribute swathCountInSelection;
	private final DoublePreferenceAttribute beamSize;

	private final IntegerPreferenceAttribute boundingOverflow;
	private final BooleanPreferenceAttribute metricsText;
	private final IntegerPreferenceAttribute fontSize;
	private final BooleanPreferenceAttribute showOrientedBoundingCube;
	private final BooleanPreferenceAttribute showSoundings;
	private final BooleanPreferenceAttribute showLandmark;
	private final BooleanPreferenceAttribute showStatistics;

	private final ColorPreferenceAttribute foregroundColor;
	private final ColorPreferenceAttribute backgroundColor;
	private final ColorPreferenceAttribute cubeFillColor;
	private final ColorPreferenceAttribute cubeBottomColor;
	private final ColorPreferenceAttribute cubeOutLineColor;

	private final ColorPreferenceAttribute selectionColor;
	private final ColorPreferenceAttribute validColor;
	private final ColorPreferenceAttribute phaseDetectionColor;
	private final ColorPreferenceAttribute amplitudeDetectionColor;
	private final ColorPreferenceAttribute invalidColor;
	private final ColorPreferenceAttribute invalidColorAcq;
	private final EnumPreferenceAttribute preferedView;

	private final DoublePreferenceAttribute zoomFactor;

	// enum for BeamVisibleMode
	private final EnumPreferenceAttribute beamVisibleMode;

	// enum for SoundingsSceneMode
	private final EnumPreferenceAttribute soundingSceneMode;

	// enum for SelectionMode
	private final EnumPreferenceAttribute selectionMode;

	// enum for SelectionMode
	private final BooleanPreferenceAttribute autoRefreshDTM;

	// enum for BeamDisplayMode
	private final EnumPreferenceAttribute beamDisplayMode;
	private final BooleanPreferenceAttribute keepInvalidBeamInRed;

	// min size for opengl buffer
	private final IntegerPreferenceAttribute openglBufferMinSize;

	/** All preferred part stack id where opening views */
	private final PreferenceComposite containerIdPrefs;

	public DtmPreference(PreferenceComposite superNode) {
		super(superNode, "Swath Editor");

		dtmSelectionColor = new ColorPreferenceAttribute("selectionColorInDTM", "Selection color in DTM",
				new GColor(0, 127, 255, 255));
		dtmSelectionLineWidth = new IntegerPreferenceAttribute("selectionLineWifth",
				"Width of the selection lines in DTM", 2);
		designatedBeamsColor = new ColorPreferenceAttribute("designatedBeamsColor", "Designated beams color",
				new GColor(255, 255, 0, 255));
		// overlap of 10% by default
		overlap = new IntegerPreferenceAttribute("overlap", "Overlap (%)", 10);

		viewSplitRatio = new IntegerPreferenceAttribute("0__viewSplitRatio", "View split ratio (0-100)", 50);
		swathCountInSelection = new IntegerPreferenceAttribute("0__swathCountInSelection",
				"Nb of swath in selection (1-99)", 1);
		beamSize = new DoublePreferenceAttribute("0_beamSize", "beam Size", 2D);
		foregroundColor = new ColorPreferenceAttribute("10_colorForeground", "Foreground color (text)",
				new GColor(0, 0, 0, 255));
		backgroundColor = new ColorPreferenceAttribute("11_colorBackground", "Background color",
				new GColor(192, 192, 192, 255));
		selectionColor = new ColorPreferenceAttribute("12_selectionColor", "Selection color",
				new GColor(255, 255, 0, 255));
		validColor = new ColorPreferenceAttribute("13_validColor", "Valid color", new GColor(0, 255, 0, 255));
		invalidColor = new ColorPreferenceAttribute("14_invalidColor", "invalid color", new GColor(255, 0, 0, 255));
		invalidColorAcq = new ColorPreferenceAttribute("15_invalidColorAcqu", "Invalid acquisition color",
				new GColor(255, 128, 0, 255));
		phaseDetectionColor = new ColorPreferenceAttribute("16_phaseColor", "Phase detection color",
				new GColor(0, 255, 0, 255));
		amplitudeDetectionColor = new ColorPreferenceAttribute("17_amColor", "Amplitude detection color",
				new GColor(255, 200, 0, 255));

		showOrientedBoundingCube = new BooleanPreferenceAttribute("21_showOrientedBoundingCube",
				"Show oriented bounding cube", true);
		boundingOverflow = new IntegerPreferenceAttribute("22_boundingOverflow", "Bounding overflow (%)", 10);
		metricsText = new BooleanPreferenceAttribute("22_metricsText", "Show metrics", true);
		fontSize = new IntegerPreferenceAttribute("22_fontSize", "Text font size", 17);
		showSoundings = new BooleanPreferenceAttribute("23_showSoundings", "Show soundings", true);
		showLandmark = new BooleanPreferenceAttribute("24_showLandmark", "Show landmark", true);
		showStatistics = new BooleanPreferenceAttribute("25_showStatistics", "Show statistics", false);

		cubeFillColor = new ColorPreferenceAttribute("31_cubeFillColor", "Grid color", new GColor(128, 128, 128, 255));
		cubeBottomColor = new ColorPreferenceAttribute("32_cubeBottomColor", "Grid bottom color",
				new GColor(192, 192, 192, 255));
		cubeOutLineColor = new ColorPreferenceAttribute("33_cubeOutLineColor", "Grid outline color",
				new GColor(255, 255, 255, 255));

		preferedView = new EnumPreferenceAttribute("40_preferedView", "Prefered View", ViewsType.MainAxis);

		zoomFactor = new DoublePreferenceAttribute("50_zoomFactor", "Zoom factor", 1.);

		beamVisibleMode = new EnumPreferenceAttribute("60_beamVisibleMode", "Beam display mode",
				BeamVisibleMode.VALID_ONLY);

		soundingSceneMode = new EnumPreferenceAttribute("70_sceneMode", "Action mode", SoundingsSceneMode.INVALIDATE);

		selectionMode = new EnumPreferenceAttribute("100_selectionMode", "Selection mode", SelectionMode.POLYGON);
		autoRefreshDTM = new BooleanPreferenceAttribute("105_autoRefreshDTM", "Auto refresh DTM", false);

		beamDisplayMode = new EnumPreferenceAttribute("110_beamDisplayMode", "Display mode",
				BeamDisplayMode.VALID_INVALID);
		keepInvalidBeamInRed = new BooleanPreferenceAttribute("120_keepInvalidBeamInRed", "Keep invalid beam in red",
				false);

		openglBufferMinSize = new IntegerPreferenceAttribute("110", "Opengl buffer min size", 1024);

		declareAttribute(dtmSelectionColor);
		declareAttribute(dtmSelectionLineWidth);
		declareAttribute(designatedBeamsColor);
		declareAttribute(overlap);
		declareAttribute(viewSplitRatio);
		declareAttribute(swathCountInSelection);
		declareAttribute(beamSize);
		declareAttribute(foregroundColor);
		declareAttribute(backgroundColor);
		declareAttribute(selectionColor);
		declareAttribute(validColor);
		declareAttribute(invalidColor);
		declareAttribute(invalidColorAcq);
		declareAttribute(phaseDetectionColor);
		declareAttribute(amplitudeDetectionColor);

		declareAttribute(metricsText);
		declareAttribute(fontSize);
		declareAttribute(showOrientedBoundingCube);
		declareAttribute(boundingOverflow);
		declareAttribute(showSoundings);
		declareAttribute(showLandmark);
		declareAttribute(showStatistics);

		declareAttribute(cubeFillColor);
		declareAttribute(cubeBottomColor);
		declareAttribute(cubeOutLineColor);
		declareAttribute(preferedView);

		declareAttribute(zoomFactor);
		declareAttribute(beamVisibleMode);
		declareAttribute(soundingSceneMode);
		declareAttribute(selectionMode);
		declareAttribute(autoRefreshDTM);

		declareAttribute(beamDisplayMode);
		declareAttribute(keepInvalidBeamInRed);

		declareAttribute(openglBufferMinSize);

		containerIdPrefs = new PreferenceComposite(this, "View containers");
		containerIdPrefs.declareAttribute(new StringPreferenceAttribute("130_" + SummaryView.PART_ID, "Summary view",
				PartManager.RIGHT_STACK_ID));
		containerIdPrefs.declareAttribute(new StringPreferenceAttribute("130_" + BiasCorrectionView.PART_ID,
				"Bias correction view", DETACHED_VIEW));
		containerIdPrefs.declareAttribute(new StringPreferenceAttribute("130_" + TriangulationFilteringPart.PART_ID,
				"Filering by triangulation view", DETACHED_VIEW));
		containerIdPrefs.declareAttribute(new StringPreferenceAttribute("130_" + BeamValidatorPart.PART_ID,
				"Beams validator view", DETACHED_VIEW));
		containerIdPrefs.declareAttribute(new StringPreferenceAttribute("130_" + ManageFilesViewPart.PART_ID,
				"Managed files view", DETACHED_VIEW));

		load();
	}

	public IntegerPreferenceAttribute getOverlap() {
		return overlap;
	}

	/**
	 * @return the {@link #dtmSelectionColor}
	 */
	public Color getDtmSelectionColor() {
		return ColorUtils.convertGColorToAWT(dtmSelectionColor.getValue());
	}

	/**
	 * @return the {@link #dtmSelectionLineWidth}
	 */
	public int getDtmSelectionLineWidth() {
		return dtmSelectionLineWidth.getValue().intValue();
	}

	/**
	 * @return the {@link #designatedBeamsColor}
	 */
	public Color getDesignatedBeamsColor() {
		return ColorUtils.convertGColorToAWT(designatedBeamsColor.getValue());
	}

	public IntegerPreferenceAttribute getViewSplitRatio() {
		return viewSplitRatio;
	}

	public IntegerPreferenceAttribute getSwathCountInSelection() {
		return swathCountInSelection;
	}

	public EnumPreferenceAttribute getBeamDisplayMode() {
		return beamDisplayMode;
	}

	public BooleanPreferenceAttribute getKeepInvalidBeamInRed() {
		return keepInvalidBeamInRed;
	}

	public DoublePreferenceAttribute getZoomFactor() {
		return zoomFactor;
	}

	public EnumPreferenceAttribute getBeamVisibleMode() {
		return beamVisibleMode;
	}

	public EnumPreferenceAttribute getSoundingSceneMode() {
		return soundingSceneMode;
	}

	public EnumPreferenceAttribute getSelectionMode() {
		return selectionMode;
	}

	public BooleanPreferenceAttribute getAutoRefreshDTM() {
		return autoRefreshDTM;
	}

	public ColorPreferenceAttribute getPhaseDetectionColor() {
		return phaseDetectionColor;
	}

	public ColorPreferenceAttribute getAmplitudeDetectionColor() {
		return amplitudeDetectionColor;
	}

	public BooleanPreferenceAttribute getMetricsText() {
		return metricsText;
	}

	public IntegerPreferenceAttribute getFontSize() {
		return fontSize;
	}

	public ColorPreferenceAttribute getCubeFillColor() {
		return cubeFillColor;
	}

	public ColorPreferenceAttribute getCubeBottomColor() {
		return cubeBottomColor;
	}

	public ColorPreferenceAttribute getCubeOutLineColor() {
		return cubeOutLineColor;
	}

	public DoublePreferenceAttribute getBeamSize() {
		return beamSize;
	}

	public ColorPreferenceAttribute getForegroundColor() {
		return foregroundColor;
	}

	public ColorPreferenceAttribute getBackgroundColor() {
		return backgroundColor;
	}

	public ColorPreferenceAttribute getSelectionColor() {
		return selectionColor;
	}

	public ColorPreferenceAttribute getValidColor() {
		return validColor;
	}

	public ColorPreferenceAttribute getInvalidColor() {
		return invalidColor;
	}

	public ColorPreferenceAttribute getInvalidColorAcqu() {
		return invalidColorAcq;
	}

	public EnumPreferenceAttribute getPreferedView() {
		return preferedView;
	}

	public BooleanPreferenceAttribute getStatistics() {
		return showStatistics;
	}

	public IntegerPreferenceAttribute getOpenglBufferMinSize() {
		return openglBufferMinSize;
	}

	public BooleanPreferenceAttribute getOrientedBoundingCube() {
		return showOrientedBoundingCube;
	}

	public BooleanPreferenceAttribute getSoundings() {
		return showSoundings;
	}

	public BooleanPreferenceAttribute getLandmark() {
		return showLandmark;
	}

	/**
	 * Getter of boundingOverflow
	 */
	public IntegerPreferenceAttribute getBoundingOverflow() {
		return boundingOverflow;
	}

	/**
	 * @return the {@link #summaryViewContainerId}
	 */
	public StringPreferenceAttribute getContainerId(String partId) {
		return (StringPreferenceAttribute) containerIdPrefs.getAttribute("130_" + partId);
	}
}
