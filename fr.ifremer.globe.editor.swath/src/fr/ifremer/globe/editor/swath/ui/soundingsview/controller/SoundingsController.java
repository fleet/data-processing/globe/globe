﻿package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Observable;

import javax.swing.SwingUtilities;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.Feature;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogrConstants;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.colt.list.DoubleArrayList;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsComposite;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsViewPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.action.SoundEditorAction;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.BeamsPolygonSelectionComponent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.BufferAssociation;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SelectionMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SoundingsSceneMode;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.ogl.renderer.scene.Camera;
import fr.ifremer.globe.ui.events.swath.ShiftingDirection;
import fr.ifremer.globe.ui.events.swath.SwathEventTopics;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

public class SoundingsController {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(SoundingsController.class);

	private final EPartService partService;
	private final MApplication application;
	private final IEventBroker eventBroker;

	private final SoundingsModel soundingModel;
	private final ViewPoint viewPoint;
	private final UndoRedoController undoRedoController;
	private final DtmPreference dtmPreferences;
	private final BoundsComputer boundsComputer;

	private final BeamLoader loader;
	private final ViewPointController viewPointController;

	/** Controller to manage selection/unselection. */
	private final SelectionController selectionController;
	/** Controller to manage validation/invalidation. */
	private final ValidationController validationController;

	/** Mouse and key listener on SoundingsView. */
	private final KeyAndMouseListener keyAndMouseListener = new KeyAndMouseListener();

	private SoundingsComposite view = null;

	@Inject
	public SoundingsController(EPartService partService, MApplication application, IEventBroker eventBroker,
			@Named(ContextNames.SOUNDINGS_MODEL) SoundingsModel soundingModel,
			@Named(ContextNames.SOUNDINGS_VIEW_POINT) ViewPoint viewPoint,
			@Named(ContextNames.UNDO_REDO_CONTROLLER) UndoRedoController undoRedoController,
			@Named(ContextNames.DTM_PREFERENCES) DtmPreference dtmPreferences,
			@Named(ContextNames.BOUNDS_COMPUTER) BoundsComputer boundsComputer) {
		this.partService = partService;
		this.application = application;
		this.eventBroker = eventBroker;
		this.soundingModel = soundingModel;
		this.viewPoint = viewPoint;
		this.undoRedoController = undoRedoController;
		this.dtmPreferences = dtmPreferences;
		this.boundsComputer = boundsComputer;

		viewPointController = ContextInitializer.make(ViewPointController.class);
		loader = ContextInitializer.make(BeamLoader.class);
		selectionController = ContextInitializer.make(SelectionController.class);
		validationController = ContextInitializer.make(ValidationController.class);

	}

	@PostConstruct
	void postConstruct() {
		// Preferences management
		dtmPreferences.getBeamSize().addObserver((Observable o, Object arg) -> preferencesHasChanged());
	}

	/**
	 * Mandatory initialization after construction
	 * 
	 * @throws GIOException
	 */
	public void initialize(SoundingsComposite view) throws IOException, GIOException {
		// Clean previous Temporary files.
		TemporaryCache.deleteAllFiles(getClass().getSimpleName(), "");
		this.view = view;
	}

	/** Event handler for SOUNDING_MODEL_CHANGED */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onModelChanged(@UIEventTopic(EventTopics.SOUNDING_MODEL_CHANGED) SoundingEvent event) {

		if (soundingModel.getSelectedArea() == null)
			return;

		switch (event) {
		case eDataChanged -> {
			reset();
			boolean boundsChanged = true;
			try {
				boundsChanged = boundsComputer.computeBounds(soundingModel.getSelectedArea());
			} catch (Exception e) {
				Messages.openErrorMessage("Swath Editor : invalid selection", "The selected area is not valid.", e);
				return;
			}
			view.getSoundingsScene().displayChanged(!viewPoint.isLock() && boundsChanged);
			// notify data change
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDataLoaded);
		}
		case eDisplayChanged -> {
			boolean viewPointLocked = viewPoint.isLock();
			boolean boundsChanged = true;
			if (!viewPointLocked) {
				// lock view so that we keep the same point of view if data did not changed
				viewPoint.setLock(true);
				if (soundingModel.getSoundingBB().isAutoScale()) {
					try {
						boundsChanged = boundsComputer.computeBounds(soundingModel.getSelectedArea());
					} catch (Exception e) {
						Messages.openErrorMessage("Swath Editor : invalid selection", "The selected area is not valid.",
								e);
						return;
					}
				}
				// set old value for lock
				viewPoint.setLock(viewPointLocked);
			}
			view.getSoundingsScene().displayChanged(!viewPointLocked && boundsChanged);
			view.refresh();
		}
		case eViewPointChanged -> view.getSoundingsScene().viewPointChanged();
		default -> {
			// Ignored
		}
		}
	}

	public void setExaggeration(boolean exaggeration) {
		viewPointController.updateVerticalExaggeration(exaggeration, true);
	}

	/**
	 * Select the beams designated by the specified model.
	 *
	 * @param designationModels all designated beams.
	 * @param select true to select, false to unselect
	 * @return the number of selected beam
	 */
	public int selectBeams(List<SelectionModel> designationModels, boolean select) {
		getBeamsPolygonSelectionComponent().stopSelection();
		// Wait for current refreshing
		getBeamsPolygonSelectionComponent().waitIfRefreshing();

		// Perform selection
		int result = selectionController.selectDesignatedBeams(designationModels, select);

		// Show the selection on DTM view
		showBeamsDesignation();

		notifyViewHasToBeRefreshed();
		view.lockCamera(false);

		eventBroker.send(EventTopics.SOUNDINGS_SELECTION, null);

		return result;
	}

	/**
	 * Change the valid attribute of the beams designated by the specified model.
	 *
	 * @param designationModels all designated beams.
	 * @param validValue value of validation attribute
	 * @return the number of validated beam
	 */
	public int validateBeams(List<SelectionModel> designationModels, MappingValidity validValue) {
		int result = 0;

		// Wait UndoRedo preparation and other components
		undoRedoController.ensureNextOperationIsPrepared();
		getBeamsPolygonSelectionComponent().stopSelection();
		getBeamsPolygonSelectionComponent().waitIfRefreshing();

		// Perform validation/invalidation
		result = validationController.validateDesignatedBeams(designationModels, validValue);

		if (result > 0) {
			validationHasChanged();
		}

		return result;
	}

	/**
	 * Clear the selection.
	 */
	private void clearSelection() {
		getBeamsPolygonSelectionComponent().reset();
		getBeamsPolygonSelectionComponent().stopSelection();
		selectionController.unselectAllBeams();
		clearBeamsDesignation();
		view.lockCamera(false);
	}

	/** End of selection */
	private void processValidation(boolean onDeleteKeyPressed) {
		getBeamsPolygonSelectionComponent().stopSelection();

		undoRedoController.ensureNextOperationIsPrepared();

		// Wait for current refreshing
		getBeamsPolygonSelectionComponent().waitIfRefreshing();

		SoundingsSceneMode soundingsSceneMode = soundingModel.getDisplayParameters().getSceneMode();
		if (onDeleteKeyPressed) {
			// On Delete key pressed, we invalidate selected beams
			validationController.validateSelectedBeams(MappingValidity.SND_UNVALID_MANUAL);
			clearBeamsDesignation();
		} else if (soundingsSceneMode == SoundingsSceneMode.SELECT) {
			// On selection, we only show the selection on DTM view
			showBeamsDesignation();
		} else {
			// Otherwise, we validate or invalidate the selected beams
			validationController
					.validateSelectedBeams(soundingsSceneMode == SoundingsSceneMode.VALIDATE ? MappingValidity.SND_VALID
							: MappingValidity.SND_UNVALID_MANUAL);
			clearBeamsDesignation();
		}

		notifyViewHasToBeRefreshed();
		view.lockCamera(false);

		eventBroker.send(EventTopics.SOUNDINGS_SELECTION, this);
	}

	private void designatedBeamsInDtmView() {
		try {
			double[][] xyz = extractDesignatedBeamsFromModel();
			var shapeFile = computeBufferedImageForBeamsDesignatedEvent(xyz);
			eventBroker.send(EventTopics.SOUNDING_DESIGNATED, java.util.Optional.ofNullable(shapeFile));
		} catch (GIOException | IOException e) {
			logger.warn("Unable to designate beams (File system full ?)", e);
		}
	}

	/**
	 * Compute buffered image for beams designated event
	 * 
	 * @throws GIOException
	 */
	private File computeBufferedImageForBeamsDesignatedEvent(double[][] xyz) throws IOException, GIOException {
		try {
			TemporaryCache.deleteAllFiles(getClass().getSimpleName(), "");
		} catch (GIOException e) {
			// Not a problem
		}
		return createPointShapeFile(xyz);
	}

	private File createPointShapeFile(double[][] xyz) throws IOException {
		File result = null;
		File shapeFolder = TemporaryCache.createTemporaryDirectory(getClass().getSimpleName(), "");
		DataSource dataSource = GdalUtils.createDataSource(GdalUtils.SHP_DRIVER_NAME, shapeFolder.getAbsolutePath());
		SwathEditorParameters parameters = ContextInitializer.getInContext(ContextNames.SWATH_EDITOR_PARAMETERS);
		SpatialReference input = parameters.projection.getSpatialReference();
		if (dataSource != null) {
			result = new File(shapeFolder, getClass().getSimpleName() + ".shp");
			Geometry setOfPoints = new Geometry(ogrConstants.wkbMultiPoint);
			setOfPoints.AssignSpatialReference(input);
			for (double[] element : xyz) {
				Geometry point = new Geometry(ogrConstants.wkbPoint);
				point.AddPoint(element[0], element[1]);
				setOfPoints.AddGeometry(point);
			}

			// Mercator to LatLon
			setOfPoints.TransformTo(GdalOsrUtils.SRS_WGS84);

			Layer layer = dataSource.CreateLayer(getClass().getSimpleName(), GdalOsrUtils.SRS_WGS84,
					ogrConstants.wkbMultiPoint);
			Feature feature = new Feature(layer.GetLayerDefn());
			feature.SetGeometry(setOfPoints);
			layer.CreateFeature(feature);
			layer.delete();
			dataSource.FlushCache();
			dataSource.delete();
		}
		return result;
	}

	/**
	 * Extracts position of designated beams, and initialize min and max values of latitude and longitude of event
	 */
	private double[][] extractDesignatedBeamsFromModel() {
		DoubleArrayList latitudes = new DoubleArrayList();
		DoubleArrayList longitudes = new DoubleArrayList();
		double minX = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;

		BufferAssociation bufferAssociation = soundingModel.getBeamsAssociation();
		for (int fileIndex = 0; fileIndex < bufferAssociation.size(); fileIndex++) {
			if (soundingModel.getFileAppearances(fileIndex).displayed()) {
				List<LineBuffer> buffers = bufferAssociation.getBuffers(fileIndex);
				for (LineBuffer lineBuffer : buffers) {
					Mappings mappings = lineBuffer.getSpatialIndex().getMappings();
					int nbBeams = lineBuffer.getDetectionCount();
					for (int offset = 0; offset < nbBeams; offset++) {
						long offsetInFile = lineBuffer.getOffsetInFile() + offset;
						SelectionModel dedicatedSelectionModel = selectionController.getSelectionModels()
								.get(fileIndex);
						if (dedicatedSelectionModel.isSelected(offsetInFile)) {
							double x = mappings.getX(offsetInFile);
							double y = mappings.getY(offsetInFile);

							longitudes.add(x);
							latitudes.add(y);

							minX = Math.min(minX, x);
							minY = Math.min(minY, y);
							maxX = Math.max(maxX, x);
							maxY = Math.max(maxY, y);
						}
					}
				}
			}
		}

		double[][] result = new double[latitudes.size()][3];
		for (int i = 0; i < longitudes.size(); i++) {
			result[i][0] = longitudes.get(i);
			result[i][1] = latitudes.get(i);
		}

		return result;
	}

	/** Validity has changed by any algo. */
	public void validationHasChanged() {
		// Otherwise, we validate or unvalidate the selected beams
		showBeamsDesignation();
		notifyViewHasToBeRefreshed();
	}

	/**
	 * Show the selection in DTM View.
	 */
	private void showBeamsDesignation() {
		Display.getDefault()
				.asyncExec(() -> BusyIndicator.showWhile(Display.getDefault(), this::designatedBeamsInDtmView));
	}

	/** Remove the beam designation */
	private void clearBeamsDesignation() {
		eventBroker.send(EventTopics.SOUNDING_DESIGNATED, java.util.Optional.empty());
	}

	/** Ask to refresh the view. */
	public void notifyViewHasToBeRefreshed() {
		// Force the BB computation and the refreshing of sounding view
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDisplayChanged);

		if (soundingModel.getDisplayParameters().isAutoRefreshDTM()) {
			eventBroker.send(SwathEventTopics.REFRESH_DTM, this);
		}
	}

	/**
	 * Perform a Undo/Redo action. Restore the SoundingsView at the specified step.
	 */
	public void restoreView(int actionIndex) {
		getBeamsPolygonSelectionComponent().stopSelection();

		// Wait UndoRedo preparation
		undoRedoController.waitIfPreparing();

		// Wait for current refreshing
		getBeamsPolygonSelectionComponent().waitIfRefreshing();

		undoRedoController.undoUntil(actionIndex);

		notifyViewHasToBeRefreshed();
		view.lockCamera(false);
	}

	/**
	 * Reset all controler. Invoked when model changed.
	 */
	public void reset() {
		clearSelection();
		eventBroker.send(EventTopics.SOUNDINGS_SELECTION, null);
	}

	/** Manage a preference modification. */
	private void preferencesHasChanged() {
		soundingModel.setBeamSize(dtmPreferences.getBeamSize().getValue().intValue());
		view.refresh();
	}

	/**
	 * @return the {@link #view}
	 */
	public SoundingsComposite getView() {
		return view;
	}

	/**
	 * @return the {@link #loader}
	 */
	public BeamLoader getLoader() {
		return loader;
	}

	/**
	 * @return the {@link #viewPointControler}
	 */
	public ViewPointController getViewPointController() {
		return viewPointController;
	}

	/**
	 * @return the BeamsPolygonSelectionComponent view.
	 */
	private BeamsPolygonSelectionComponent getBeamsPolygonSelectionComponent() {
		return view.getSoundingsScene().getBeamsPolygonSelectionComponent();
	}

	public void setCamera(Camera camera) {
		viewPoint.setCamera(camera);
	}

	/** Indicates if selection is ongoing. */
	public boolean isSelecting() {
		return getBeamsPolygonSelectionComponent().isSelecting();
	}

	/** Getter of {@link #keyAndMouseListener} */
	public KeyAndMouseListener getKeyAndMouseListener() {
		return keyAndMouseListener;
	}

	/** Make sure that SoundingsView has the focus (to catch the keyboard actions) */
	@Focus
	private void setFocusOnView() {
		UIUtils.asyncExecSafety(view, () -> {
			view.setFocus();
			SwingUtilities.invokeLater(() -> view.getWindow().getComponent().requestFocus());
		});
	}

	/**
	 * Class managing the polygon selection Do not consume the event when no action was performed (allow
	 * SwathController.mouseReleased to react for example)
	 */
	class KeyAndMouseListener extends MouseAdapter implements KeyListener {
		/** Last mouse position */
		private Point mousePosition = null;

		/** {@inheritDoc} */
		@Override
		public void mouseClicked(MouseEvent e) {
			// we don't use mouse clicked event because these are fired only
			// if we don't move the mouse between mouse pressed and mouse
			// released. This prevent us from selecting area in a fast way
			e.consume();
		}

		/** {@inheritDoc} */
		@Override
		public void mousePressed(MouseEvent e) {
			if (e.isConsumed())
				return;

			// On left click, start selecting Quad or Lasso
			if (e.getButton() == MouseEvent.BUTTON1 && tryStartQuadOrLasso())
				e.consume();

		}

		/** {@inheritDoc} */
		@Override
		public void mouseReleased(MouseEvent e) {
			if (e.isConsumed())
				return;

			// Left click
			if (e.getButton() == MouseEvent.BUTTON1) {
				if (e.getClickCount() == 1) {
					// End of a Quad or Lasso selection
					if (!tryStopQuadOrLasso())
						tryStartOrIncreasePolygon();
					else
						e.consume();
				}
				if (e.getClickCount() == 2 && tryStopPolygon())
					e.consume();
			} else if (e.getButton() == MouseEvent.BUTTON3 && tryRemoveLastPoint())
				e.consume();

		}

		/** {@inheritDoc} */
		@Override
		public void mouseMoved(MouseEvent e) {
			updateMousePosition(e);
			if (tryIncreaseQuadOrLasso())
				e.consume();
		}

		/** {@inheritDoc} */
		@Override
		public void mouseEntered(MouseEvent e) {
			setFocusOnView();
			e.consume();
		}

		/** {@inheritDoc} */
		@Override
		public void mouseDragged(MouseEvent e) {
			// Increase Quad or Lasso selection
			updateMousePosition(e);
			if (tryIncreaseQuadOrLasso())
				e.consume();
		}

		/** {@inheritDoc} */
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.isConsumed())
				return;
			e.consume();

			switch (e.getKeyCode()) {
			case KeyEvent.VK_SPACE:
				// On space, start selecting
				if (!tryStartQuadOrLasso())
					tryStartOrIncreasePolygon();
				break;
			case KeyEvent.VK_ESCAPE, KeyEvent.VK_CANCEL:
				clearSelection();
				notifyViewHasToBeRefreshed();
				break;
			case KeyEvent.VK_ENTER:
				tryStopPolygon();
				break;
			case KeyEvent.VK_LEFT:
				eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.WEST);
				break;
			case KeyEvent.VK_RIGHT:
				eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.EAST);
				break;
			case KeyEvent.VK_UP:
				eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.NORTH);
				break;
			case KeyEvent.VK_DOWN:
				eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.SOUTH);
				break;
			case KeyEvent.VK_DELETE, KeyEvent.VK_BACK_SPACE:
				processValidation(true);
				eventBroker.send(EventTopics.SOUND_ACTION, new SoundEditorAction("manual invalidation"));
				break;
			default:
				break;
			}
		}

		/** {@inheritDoc} */
		@Override
		public void keyTyped(KeyEvent e) {
		}

		/** {@inheritDoc} */
		@Override
		public void keyReleased(KeyEvent e) {
			if (e.isConsumed())
				return;
			e.consume();

			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				// End of a Quad or Lasso selection
				if (tryStopQuadOrLasso())
					return;

			}
		}

		private void alertNewAction() {
			SoundingsSceneMode sceneMode = soundingModel.getDisplayParameters().getSceneMode();
			if (sceneMode == SoundingsSceneMode.INVALIDATE) {
				eventBroker.send(EventTopics.SOUND_ACTION, new SoundEditorAction("manual invalidation"));
			} else if (sceneMode == SoundingsSceneMode.VALIDATE) {
				eventBroker.send(EventTopics.SOUND_ACTION, new SoundEditorAction("manual validation"));
			}
		}

		/**
		 * Add one point to the selection.
		 */
		private void processAddPoint() {
			if (!getBeamsPolygonSelectionComponent().isSelecting()) {
				selectionController.unselectAllBeams();
			}
			getBeamsPolygonSelectionComponent().addPoint(mousePosition);
			view.lockCamera(true);

			// Useful for activate Undo.Redo
			PartUtil.activatePart(application, partService, SoundingsViewPart.PART_ID);
		}

		/** Update the last mouse position */
		private void updateMousePosition(MouseEvent e) {
			if (mousePosition != null) {
				mousePosition.x = e.getX();
				mousePosition.y = e.getY();
			} else {
				mousePosition = e.getPoint();
			}
		}

		/** Try to start drawing a Quad or Lasso : add the first point */
		private boolean tryStartQuadOrLasso() {
			BeamsPolygonSelectionComponent beamsSelector = getBeamsPolygonSelectionComponent();
			if (!beamsSelector.isSelecting() && beamsSelector.getSelectionMode() != SelectionMode.POLYGON) {
				processAddPoint();
				return true;
			}
			return false;
		}

		/** Try to add a point to the current a Quad or Lasso */
		private boolean tryIncreaseQuadOrLasso() {
			BeamsPolygonSelectionComponent beamsSelector = getBeamsPolygonSelectionComponent();
			if (beamsSelector.isSelecting()) {
				beamsSelector.setMousePoint(mousePosition);
				if (beamsSelector.getSelectionMode() == SelectionMode.LASSO) {
					processAddPoint();
					return true;
				}
			}
			return false;
		}

		/** Try to stop drawing a Quad or Lasso : validate the selection */
		private boolean tryStopQuadOrLasso() {
			BeamsPolygonSelectionComponent beamsSelector = getBeamsPolygonSelectionComponent();
			if (beamsSelector.isSelecting() && beamsSelector.getSelectionMode() != SelectionMode.POLYGON) {
				processAddPoint();
				processValidation(false);
				alertNewAction();
				setFocusOnView();
				return true;
			}
			return false;
		}

		/** Try to add a point to the current a Polygon. */
		private void tryStartOrIncreasePolygon() {
			BeamsPolygonSelectionComponent beamsSelector = getBeamsPolygonSelectionComponent();
			if (beamsSelector.getSelectionMode() == SelectionMode.POLYGON)
				processAddPoint();
		}

		/** Try to stop drawing a Quad or Lasso : validate the selection */
		private boolean tryStopPolygon() {
			BeamsPolygonSelectionComponent beamsSelector = getBeamsPolygonSelectionComponent();
			if (beamsSelector.isSelecting() && beamsSelector.getSelectionMode() == SelectionMode.POLYGON) {
				processAddPoint();
				processValidation(false);
				alertNewAction();
				setFocusOnView();
				return true;
			}
			return false;
		}

		/**
		 * Remove the last point of the selection.
		 */
		private boolean tryRemoveLastPoint() {
			BeamsPolygonSelectionComponent beamsSelector = getBeamsPolygonSelectionComponent();
			if (beamsSelector.isSelecting() && beamsSelector.getSelectionMode() == SelectionMode.POLYGON) {
				selectionController.unselectAllBeams();
				beamsSelector.removeLastPoint();
				return true;
			}
			return false;
		}
	}

}
