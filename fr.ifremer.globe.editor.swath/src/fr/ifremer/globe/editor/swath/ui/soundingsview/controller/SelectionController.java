/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.BufferAssociation;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;

/**
 * Controller of the Soundings view to manage the selection/unselection process.
 */
public class SelectionController {

	/** Model. */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	/**
	 * Constructor.
	 */
	public SelectionController() {
		super();
	}

	/**
	 * Perform the selection of some designated beams.
	 * 
	 * @param designationModels designated beams.
	 * @param selected select or unselect
	 */
	public int selectDesignatedBeams(List<SelectionModel> designationModels, boolean selected) {
		int result = 0;
		BufferAssociation bufferAssociation = soundingsModel.getBeamsAssociation();
		for (int fileIndex = 0; fileIndex < bufferAssociation.size(); fileIndex++) {
			SelectionModel designationModel = designationModels.get(fileIndex);
			SelectionModel selectionModel = getSelectionModels().get(fileIndex);
			for (LineBuffer lineBuffer : bufferAssociation.getBuffers(fileIndex)) {
				result += selectBeams(lineBuffer, designationModel, selectionModel, selected);
			}
		}
		return result;
	}

	/**
	 * Perform the selection of some beams in a LineBuffer.
	 * 
	 * @param lineBuffer considered lineBuffer
	 * @param designationModels designated beams.
	 * @param selectionModel Model to update.
	 * @param selected select or unselect
	 */
	protected int selectBeams(LineBuffer lineBuffer, SelectionModel designationModel, SelectionModel selectionModel,
			boolean selected) {
		int result = 0;
		int size = lineBuffer.getDetectionCount();
		for (int i = 0; i < size; i++) {
			long offsetInFile = lineBuffer.getOffsetInFile() + i;
			if (designationModel.isSelected(offsetInFile) && selectionModel.isSelected(offsetInFile) != selected) {
				selectionModel.setSelected(offsetInFile, selected);
				result++;
			}
		}
		return result;
	}

	/**
	 * @return all {@link #SelectionModel}
	 */
	public List<SelectionModel> getSelectionModels() {
		return soundingsModel.getSelectionModels();
	}

	/** Unselect all beams. */
	protected void unselectAllBeams() {
		getSelectionModels().stream().forEach(SelectionModel::resetSelection);
	}

}
