package fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import fr.ifremer.globe.core.model.geometry.OrientedBox;

/**
 * The sounding box of all displayed soundings projected along to axis given by the oriented bounding box passed as
 * parameter in {@link #begin()}
 */
public class OrientedSoundingBoundingBox {

	/**
	 * Axis aligned bounding box, axis are not north and east but are given by the main and secondary axis of selection
	 */
	protected AxisAlignedSoundingBoundingBox boundings = new AxisAlignedSoundingBoundingBox();

	/**
	 * the secondary axis of the bounding box
	 */
	protected Vector2D secondaryAxis;

	/**
	 * the main axis of the bounding box
	 */
	protected Vector2D mainAxis;

	void begin(OrientedBox selection) {
		mainAxis = selection.getMainAxis().normalize();
		secondaryAxis = selection.getSecondaryAxis().normalize();
		boundings.begin();
	}

	void end(double overflow) {
		boundings.end(overflow);
	}

	public void push(double x, double y, double z) {
		double mainAxisDotProduct = mainAxis.getX() * x + mainAxis.getY() * y;
		double secondAxisDotProduct = secondaryAxis.getX() * x + secondaryAxis.getY() * y;

		boundings.push(mainAxisDotProduct, secondAxisDotProduct, z);

	}

	/**
	 * @return the secondaryAxis
	 */
	public Vector2D getSecondaryAxis() {
		return secondaryAxis;
	}

	/**
	 * @return the mainAxis
	 */
	public Vector2D getMainAxis() {
		return mainAxis;
	}

	/**
	 * Axis aligned bounding box, axis are not north and east but are given by the main and secondary axis of selection
	 */
	public AxisAlignedSoundingBoundingBox getAxisBB() {
		return boundings;
	}

	@Override
	public String toString() {
		return "OBB :" + boundings.toString();

	}

}
