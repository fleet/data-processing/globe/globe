package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.OptionalInt;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.processes.biascorrection.BiasCorrection;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList.CorrectionType;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.IBiasSounding;
import fr.ifremer.globe.core.processes.biascorrection.model.LocalContainerProxy;
import fr.ifremer.globe.core.processes.biascorrection.model.SwathBeamLimits;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Apply correction and bias interactively to soundings
 */
public class BiasCorrector {

	protected static Logger logger = LoggerFactory.getLogger(BiasCorrector.class);

	/** model of the soundings view */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingModel;

	/** Eclipse event broker service */
	@Inject
	protected IEventBroker eventBroker;

	private final Map<String, LocalContainerProxy> dataProxyMap = new HashMap<>();
	private final Map<String, Instant> dateMinMap = new HashMap<>();

	private final HashMap<SounderDataContainer, IFloatArray> savedArrays = new HashMap<>();
	private final List<IFloatArray> biasCorrectorArrays = new ArrayList<>();
	private final BiasCorrection process = new BiasCorrection("Swath editor bias corrector");

	private Instant dateMinSelection = Instant.MAX;

	/** Constructor **/
	public BiasCorrector() {
		super();
	}

	/** Clean up environment **/
	public void cleanupProcess() throws IOException {
		dataProxyMap.forEach((name, proxy) -> proxy.dispose());
		dataProxyMap.clear();
		restoreArray();
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDisplayChanged);
	}

	/** Saves the current SE state: clones all position array */
	private void createArray() throws IOException {
		for (SounderDataContainer dataContainer : soundingModel.getSounderDataContainers()) {
			int fileIndex = soundingModel.getBeamsAssociation().indexOf(dataContainer);
			if (fileIndex != -1) {
				SpatialIndex spatialIndex = soundingModel.getBeamsAssociation().getSpatialIndex(fileIndex);
				savedArrays.put(dataContainer, spatialIndex.getMappings().getCoordsArray());
				IFloatArray coordcopy = spatialIndex.getMappings().getCoordsArray().cloneFloatArray();
				biasCorrectorArrays.add(coordcopy);
				spatialIndex.getMappings().setCoordsArray(coordcopy);
			} else
				logger.error("Non consistent model for file : {}", dataContainer.getInfo().getPath());
		}
	}

	/** Restores the previous SE state **/
	private void restoreArray() throws IOException {
		for (Entry<SounderDataContainer, IFloatArray> array : savedArrays.entrySet()) {
			int fileIndex = soundingModel.getBeamsAssociation().indexOf(array.getKey());
			if (fileIndex != -1) {
				SpatialIndex spatialIndex = soundingModel.getBeamsAssociation().getSpatialIndex(fileIndex);
				// reload a previous saved array
				spatialIndex.getMappings().setCoordsArray(array.getValue());
			} else {
				logger.error("Non consistent model for file {}", array.getKey().getInfo().getPath());
			}
		}
		savedArrays.clear();
		// release temporary files used by BiasCorrector
		biasCorrectorArrays.forEach(IFloatArray::dispose);
		biasCorrectorArrays.clear();
	}

	/** @return the minimum date of selection **/
	public Instant getMinDateSelection() {
		return dateMinSelection == Instant.MAX ? Instant.EPOCH : dateMinSelection;
	}

	/** @return the minimum date of selection for a specific file **/
	public Instant getMinDateFileSelection(String fileName) {
		if (dateMinMap.containsKey(fileName)) {
			Instant pingMinDate = dateMinMap.get(fileName);
			return pingMinDate == Instant.MAX ? Instant.EPOCH : pingMinDate;
		} else {
			return Instant.EPOCH;
		}
	}

	/***
	 * setup and prepare environment for soundings corrector (bias and velocity)
	 */
	public void setupProcess() throws IOException {
		cleanupProcess();
		createArray();
		dateMinMap.clear();
		dateMinSelection = Instant.MAX;

		for (int fileIndex = 0; fileIndex < soundingModel.getBeamsAssociation().size(); fileIndex++) {

			final int currentFileIndex = fileIndex;
			try {
				// Optimization ping/beam interval
				int swathMin = Integer.MAX_VALUE;
				int swathMax = -Integer.MAX_VALUE;
				int beamMin = Integer.MAX_VALUE;
				int beamMax = -Integer.MAX_VALUE;

				List<LineBuffer> buffers = soundingModel.getBeamsAssociation().getBuffers(currentFileIndex);
				SounderDataContainer dataContainer = soundingModel.getBeamsAssociation().getFile(currentFileIndex);

				// recompute boundings boxes and update buffers limits
				List<IBiasSounding> soundings = new ArrayList<>();

				boolean processLoadData = false;
				for (LineBuffer line : buffers) {
					Mappings mappedData = line.getSpatialIndex().getMappings();

					// read all data
					long offsetInFile = line.getOffsetInFile();
					int count = line.getDetectionCount();
					for (int i = 0; i < count; i++) {
						int swathIndex = mappedData.getCycleIndex(offsetInFile + i);
						int beamIndex = mappedData.getBeamIndex(offsetInFile + i);
						swathMin = Math.min(swathMin, swathIndex);
						swathMax = Math.max(swathMax, swathIndex);
						beamMin = Math.min(beamMin, beamIndex);
						beamMax = Math.max(beamMax, beamIndex);
						soundings.add(new SoundingInterface(line, (short) beamIndex, swathIndex, i));
						processLoadData = true;
					}
				}

				if (processLoadData) {
					SwathBeamLimits limits = new SwathBeamLimits(swathMin, swathMax, beamMin, beamMax);
					LongLayer1D timeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);

					// Date
					Instant pingMinDate = DateUtils.epochNanoToInstant(timeLayer.get(swathMin));
					String fileName = Paths.get(dataContainer.getInfo().getPath()).getFileName().toString();
					dateMinMap.put(fileName, pingMinDate);
					dateMinSelection = dateMinSelection.isBefore(pingMinDate) ? dateMinSelection : pingMinDate;

					// Create data container proxy
					LocalContainerProxy proxy = new LocalContainerProxy(dataContainer, limits, soundings);
					dataProxyMap.put(dataContainer.getInfo().getPath(), proxy);

				}
			} catch (GIOException e) {
				logger.error("Bias corrector : exception while opening "
						+ soundingModel.getBeamsAssociation().getFile(currentFileIndex).getInfo().getPath(), e);
			}

		}

	}

	/**
	 * Executes the Bias correction, or restore the previous state if there is no correction to apply
	 */
	public void executeProcess(List<CorrectionPoint> points, OptionalInt antennaIndex) throws GIOException {

		// Get corrections for each file
		Map<ISounderNcInfo, CorrectionList> correctionPointsByFile = new HashMap<>();
		for (CorrectionPoint correctionPoint : points) {
			List<ISounderNcInfo> fileList = soundingModel.getBiasCorrectionModel().getInfoStoreList(correctionPoint);
			for (ISounderNcInfo file : fileList) {

				if (!correctionPointsByFile.containsKey(file))
					correctionPointsByFile.put(file, new CorrectionList(CorrectionType.BIAS));

				correctionPointsByFile.get(file).addCorrectionPoint(correctionPoint);
			}
		}

		// Apply correction
		for (Entry<ISounderNcInfo, CorrectionList> entry : correctionPointsByFile.entrySet()) {
			if (dataProxyMap.containsKey(entry.getKey().getPath())) {
				LocalContainerProxy dataProxy = dataProxyMap.get(entry.getKey().getPath());
				dataProxy.startProcess();
				process.processSwath(new NullProgressMonitor(), dataProxy, entry.getValue(), antennaIndex);
				dataProxy.endProcess();
			}
		}

		// Refresh sounding view
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDisplayChanged);
	}

}
