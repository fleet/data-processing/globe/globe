/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.geographicview;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;

/**
 * Interpolates elevations on the specified file
 */
public class DtmElevationInterpolator {

	/**
	 * Constructor
	 */
	public DtmElevationInterpolator() {
	}

	/**
	 * Interpolates elevations of DEPTH layer
	 * 
	 */
	public void interpolateElevations(File file, int maxDistance, IProgressMonitor monitor, int nbTicks)
			throws IOException {
		try (GdalDataset interpolatedDataset = GdalDataset.open(file.getAbsolutePath(), gdalconstConstants.GA_Update)) {

			// Create a Mask band indicating cells to be interpolated (zero valued).
			Band band = interpolatedDataset.get().GetRasterBand(1);
			interpolatedDataset.get().CreateMaskBand(gdalconstConstants.GMF_PER_DATASET);
			Band maskBand = band.GetMaskBand();

			monitor.subTask("Preparing interpolation...");
			// 10% of progress to create the mask
			int ticks10percent = nbTicks / 10;

			// Keep only NoDataValue inside the terrain shape
			int columnCount = interpolatedDataset.get().getRasterXSize();
			int lineCount = interpolatedDataset.get().getRasterYSize();
			float floatMissingValue = (float) GdalUtils.getNoDataValue(band);
			short[] maskLine = new short[columnCount];
			float[] valueLine = new float[columnCount];
			for (int line = 0; line < lineCount; line++) {
				if (band.ReadRaster(0, line, columnCount, 1, valueLine) == GdalUtils.OK) {
					// Compute left side of the line
					int leftSide = 0;
					while (leftSide < columnCount && valueLine[leftSide] == floatMissingValue) {
						leftSide++;
					}
					leftSide += maxDistance;
					// Compute right side of the line
					int rightSide = columnCount - 1;
					while (rightSide > 0 && valueLine[rightSide] == floatMissingValue) {
						rightSide--;
					}
					rightSide -= maxDistance;
					// Mask cell to ignore with a 255 value as mask
					for (int column = 0; column < columnCount; column++) {
						maskLine[column] = (short) (column > leftSide && column < rightSide
								&& valueLine[column] == floatMissingValue ? 0 : 255);
					}
					maskBand.WriteRaster(0, line, columnCount, 1, maskLine);
				}
			}
			monitor.worked(ticks10percent);

			// Compute a value for all empty cells and ignore NoDataValue
			monitor.subTask("Interpolating...");
			gdal.FillNodata(band, maskBand, maxDistance, 0, null,
					GdalUtils.newProgressCallback(monitor, ticks10percent * 8));
			interpolatedDataset.get().FlushCache();

			// Clean
			maskBand.delete();
			band.delete();
			// Delete external mask file created by CreateMaskBand
			getInterpolatedMaskFile(file).delete();

			monitor.subTask("Refreshing layers...");
		}
	}

	/**
	 * @return the file containing interpolaed elevations
	 */
	protected File getInterpolatedMaskFile(File elevationFile) {
		return new File(elevationFile.getParentFile(), elevationFile.getName() + ".msk");
	}

}
