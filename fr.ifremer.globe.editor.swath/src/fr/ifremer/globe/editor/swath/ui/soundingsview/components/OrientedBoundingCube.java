/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.AxisAlignedSoundingBoundingBox;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.OrientedSoundingBoundingBox;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.scene.Camera;

/**
 * Bounding box component.
 */
public class OrientedBoundingCube extends BoundingCube {

	public OrientedBoundingCube(Camera camera) {
		super(camera);
	}

	/**
	 * Prepare the vertice for drawing a cube.
	 */
	@Override
	protected void computeCubeVertice() {
		OrientedSoundingBoundingBox boundingBox = soundingsModel.getSoundingBB().getActiveOBB();
		Vector3D worldCenter = viewPoint.getWorldCenter();
		AxisAlignedSoundingBoundingBox bb = boundingBox.getAxisBB();
		vertice0 = computePosition(bb.getMinX(), bb.getMaxY(), bb.getMaxZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);
		vertice1 = computePosition(bb.getMaxX(), bb.getMaxY(), bb.getMaxZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);
		vertice2 = computePosition(bb.getMaxX(), bb.getMinY(), bb.getMaxZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);
		vertice3 = computePosition(bb.getMinX(), bb.getMinY(), bb.getMaxZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);
		vertice4 = computePosition(bb.getMinX(), bb.getMinY(), bb.getMinZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);
		vertice5 = computePosition(bb.getMinX(), bb.getMaxY(), bb.getMinZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);
		vertice6 = computePosition(bb.getMaxX(), bb.getMaxY(), bb.getMinZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);
		vertice7 = computePosition(bb.getMaxX(), bb.getMinY(), bb.getMinZ(), boundingBox.getMainAxis(),
				boundingBox.getSecondaryAxis(), worldCenter);

		vertices = new float[][] { { (float) vertice0.getX(), (float) vertice0.getY(), (float) vertice0.getZ() },
				{ (float) vertice1.getX(), (float) vertice1.getY(), (float) vertice1.getZ() },
				{ (float) vertice2.getX(), (float) vertice2.getY(), (float) vertice2.getZ() },
				{ (float) vertice3.getX(), (float) vertice3.getY(), (float) vertice3.getZ() },
				{ (float) vertice4.getX(), (float) vertice4.getY(), (float) vertice4.getZ() },
				{ (float) vertice5.getX(), (float) vertice5.getY(), (float) vertice5.getZ() },
				{ (float) vertice6.getX(), (float) vertice6.getY(), (float) vertice6.getZ() },
				{ (float) vertice7.getX(), (float) vertice7.getY(), (float) vertice7.getZ() } };

		Vector3D centerRight = vertice0.add(vertice5).add(vertice3).add(vertice4).divide(4).normalize();
		Vector3D centerTop = vertice0.add(vertice1).add(vertice3).add(vertice2).divide(4).normalize();
		Vector3D centerLeft = vertice1.add(vertice6).add(vertice2).add(vertice7).divide(4).normalize();
		Vector3D centerBottom = vertice4.add(vertice5).add(vertice6).add(vertice7).divide(4).normalize();
		Vector3D centerFront = vertice2.add(vertice3).add(vertice4).add(vertice7).divide(4).normalize();
		Vector3D centerBack = vertice5.add(vertice6).add(vertice1).add(vertice0).divide(4).normalize();
		normales = new Vector3D[] {
				// TOP
				new Vector3D(centerTop.getX(), centerTop.getY(), centerTop.getZ()),
				// RIGHT
				new Vector3D(centerRight.getX(), centerRight.getY(), centerRight.getZ()),
				// BACK
				new Vector3D(centerBack.getX(), centerBack.getY(), centerBack.getZ()),
				// LEFT
				new Vector3D(centerLeft.getX(), centerLeft.getY(), centerLeft.getZ()),
				// FRONT
				new Vector3D(centerFront.getX(), centerFront.getY(), centerFront.getZ()),
				// BOTTOM
				new Vector3D(centerBottom.getX(), centerBottom.getY(), centerBottom.getZ()) };

	}

	Vector3D computePosition(double x, double y, double z,
			org.apache.commons.math3.geometry.euclidean.twod.Vector2D mainAxis,
			org.apache.commons.math3.geometry.euclidean.twod.Vector2D secondaryAxis, Vector3D center) {
		return new Vector3D(x * mainAxis.getX() + y * secondaryAxis.getX() - center.getX(),
				x * mainAxis.getY() + y * secondaryAxis.getY() - center.getY(), z - center.getZ());
	}

}
