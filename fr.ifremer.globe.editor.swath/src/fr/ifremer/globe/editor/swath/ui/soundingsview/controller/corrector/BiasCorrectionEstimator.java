package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.eclipse.e4.core.di.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.processes.biascorrection.model.IBiasSounding;
import fr.ifremer.globe.core.processes.biascorrection.model.LocalContainerProxy;
import fr.ifremer.globe.core.processes.biascorrection.model.SwathBeamLimits;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Estimates bias correction from current detections.
 */
public class BiasCorrectionEstimator {

	protected static Logger logger = LoggerFactory.getLogger(BiasCorrectionEstimator.class);

	/** model of the soundings view */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingModel;

	private final Map<String, LocalContainerProxy> dataProxyMap = new HashMap<>();

	/** Constructor **/
	public BiasCorrectionEstimator() {
		super();
	}

	/** Clean up data **/
	public void cleanupProcess() {
		dataProxyMap.forEach((name, proxy) -> proxy.dispose());
		dataProxyMap.clear();
	}

	/***
	 * Setups and prepares data.
	 */
	public void setupProcess() {
		cleanupProcess();

		for (int fileIndex = 0; fileIndex < soundingModel.getBeamsAssociation().size(); fileIndex++) {

			final int currentFileIndex = fileIndex;
			try {
				// Optimization ping/beam interval
				int swathMin = Integer.MAX_VALUE;
				int swathMax = -Integer.MAX_VALUE;
				int beamMin = Integer.MAX_VALUE;
				int beamMax = -Integer.MAX_VALUE;

				List<LineBuffer> buffers = soundingModel.getBeamsAssociation().getBuffers(currentFileIndex);
				SounderDataContainer dataContainer = soundingModel.getBeamsAssociation().getFile(currentFileIndex);

				// recompute boundings boxes and update buffers limits
				List<IBiasSounding> soundings = new ArrayList<>();

				boolean processLoadData = false;
				for (LineBuffer line : buffers) {
					Mappings mappedData = line.getSpatialIndex().getMappings();
					// read all data
					long offsetInFile = line.getOffsetInFile();
					int count = line.getDetectionCount();
					for (int i = 0; i < count; i++) {
						int swathIndex = mappedData.getCycleIndex(offsetInFile + i);
						int beamIndex = mappedData.getBeamIndex(offsetInFile + i);
						swathMin = Math.min(swathMin, swathIndex);
						swathMax = Math.max(swathMax, swathIndex);
						beamMin = Math.min(beamMin, beamIndex);
						beamMax = Math.max(beamMax, beamIndex);
						soundings.add(new SoundingInterface(line, (short) beamIndex, swathIndex, i));
						processLoadData = true;
					}
				}

				if (processLoadData) {
					SwathBeamLimits limits = new SwathBeamLimits(swathMin, swathMax, beamMin, beamMax);
					dataProxyMap.put(dataContainer.getInfo().getPath(),
							new LocalContainerProxy(dataContainer, limits, soundings));
				}
			} catch (GIOException e) {
				logger.error("Bias corrector : exception while opening "
						+ soundingModel.getBeamsAssociation().getFile(currentFileIndex).getInfo().getPath(), e);
			}
		}
	}

	/**
	 * Executes the bias correction angle to apply between two files.
	 */
	public Pair<Double, String> executeProcess() {
		var logger = new StringBuilder("2D simple regression (x = across distance, y = depth)\n\n");
		try {
			setupProcess();

			List<Double> angles = new ArrayList<Double>();
			for (var dataProxy : dataProxyMap.values()) {
				dataProxy.startProcess();
				angles.add(simpleRegression(dataProxy, logger));
				dataProxy.endProcess();
			}

			double result = Double.NaN;
			if (angles.size() == 2) {
				result = Math.abs(angles.get(0) + angles.get(1)) / 2d;
				logger.append(String.format("Estimated roll bias : %.4f°", result));
			} else {
				logger.append("Roll estimation can be done only with 2 files.");
			}

			return new Pair<>(result, logger.toString());
		} catch (GIOException e) {
			e.printStackTrace();
			return new Pair<>(Double.NaN, "Error while computing estimation : " + e.getMessage());
		}
	}

	/**
	 * Perform simple regression on {@link LocalContainerProxy}.
	 * 
	 * @return y angle compute from result slope.
	 */
	private double simpleRegression(LocalContainerProxy localDataContainer, StringBuilder logger) throws GIOException {
		var simpleRegression = new SimpleRegression();
		localDataContainer.processSounding((swathIndex, beamIndex) -> {
			var x = localDataContainer.getAcrossDistance(swathIndex, beamIndex);
			var y = localDataContainer.getDepth(swathIndex, beamIndex);
			simpleRegression.addData(x, y);
		});
		var slope = simpleRegression.getSlope();
		var angle = Math.toDegrees(Math.atan(slope));
		var limits = localDataContainer.getPingBeamLimits();
		logger.append(
				String.format("File %s\nApply on detection from swath %s to %s, beam %s to %s. Total count = %d.\n",
						localDataContainer.getInfo().getBaseName(), limits.getSwathMin(), limits.getSwathMax(),
						limits.getBeamMin(), limits.getBeamMax(), simpleRegression.getN()));
		logger.append(String.format("Regression line equation : y = %.4f*x + %.2f\n", slope,
				simpleRegression.getIntercept()));
		logger.append(String.format("Angle from y axis : %.4f°\n\n", angle));
		return angle;
	}

}
