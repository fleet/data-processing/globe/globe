/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteOrder;

import jakarta.inject.Inject;

import org.apache.commons.io.IOUtils;

import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Set of beams selected in {@link Mappings}
 */
public class SelectionModel implements Closeable {

	/** 1f mean the beam is selected. */
	public static byte SELECTED_VALUE = NumberUtils.bool2byte(true);
	/** 0f mean the beam is selected. */
	public static byte NOT_SELECTED_VALUE = NumberUtils.bool2byte(false);

	/** Factory of {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	/** Array to store the beam selection. */
	protected IByteArray selectionArray;

	/** Mappings on which the selection is applied. */
	protected Mappings mappings;

	/** Initialize the selection array. */
	public void initSelectionArray(Mappings mappings) throws IOException {
		this.mappings = mappings;
		selectionArray = arrayFactory.makeByteArray(mappings.getSoundingCount());
		selectionArray.setByteOrder(ByteOrder.nativeOrder());
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() {
		IOUtils.closeQuietly(selectionArray);
	}

	/**
	 * Set beam selection flag.
	 */
	public void setSelected(long index, boolean selected) {
		selectionArray.putByte(index, selected ? SELECTED_VALUE : NOT_SELECTED_VALUE);
	}

	/**
	 * Indicates if beam is selected.
	 */
	public boolean isSelected(long index) {
		return selectionArray.getByte(index) == SELECTED_VALUE;
	}

	/** Set all beam unselected. */
	public void resetSelection() {
		selectionArray.fill(NOT_SELECTED_VALUE);
	}

	/**
	 * @return the {@link #selectionArray}
	 */
	public IByteArray getSelectionArray() {
		return selectionArray;
	}

	/**
	 * @return the {@link #mappings}
	 */
	public Mappings getMappings() {
		return mappings;
	}

	/**
	 * @return the total number of selectable beams
	 */
	public long getSize() {
		return mappings.getSoundingCount();
	}
}
