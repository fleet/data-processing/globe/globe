package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.processes.biascorrection.model.CommonCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.FileCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.ICorrectionPointVisitor;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import jakarta.inject.Inject;
import jakarta.inject.Named;

public class BiasCorrectionModel {

	public CorrectionList getCorrectionList() {
		return correctionList;
	}

	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	private CorrectionList correctionList = new CorrectionList();

	public final static String COMMON_CORRECTIONLIST = "commonCorrectionList";

	public BiasCorrectionModel() {
	}

	public void addCorrectionPoint(CorrectionPoint correctionPoint) {
		if (correctionPoint.getClass() == CommonCorrectionPoint.class) {
			CommonCorrectionPoint commonCorrectionPoint = (CommonCorrectionPoint) correctionPoint.clone();
			correctionList.addCorrectionPoint(commonCorrectionPoint);
		} else {
			FileCorrectionPoint fileCorrectionPoint = (FileCorrectionPoint) correctionPoint.clone();
			correctionList.addCorrectionPoint(fileCorrectionPoint);
		}
	}

	public List<ISounderNcInfo> getInfoStoreList(CorrectionPoint point) {
		List<ISounderNcInfo> list = new ArrayList<>();
		ICorrectionPointVisitor visitor = new ICorrectionPointVisitor() {
			@Override
			public void visit(FileCorrectionPoint p) {
				if (p.getFileName() != null)
					for (int fileIdx = 0; fileIdx < soundingsModel.getSounderDataContainers().size(); fileIdx++) {
						SounderDataContainer dataContainer = soundingsModel.getSounderDataContainers().get(fileIdx);
						// Ignore non editable file
						if (soundingsModel.getFileAppearances(fileIdx).editable()
								&& dataContainer.getInfo().getPath().contains(p.getFileName()))
							list.add(dataContainer.getInfo());
					}
			}

			@Override
			public void visit(CommonCorrectionPoint p) {
				for (int fileIdx = 0; fileIdx < soundingsModel.getSounderDataContainers().size(); fileIdx++) {
					SounderDataContainer dataContainer = soundingsModel.getSounderDataContainers().get(fileIdx);
					// Ignore non editable file
					if (soundingsModel.getFileAppearances(fileIdx).editable())
						list.add(dataContainer.getInfo());
				}
			}
		};
		point.accept(visitor);
		return list;
	}

	/**
	 * compute the file list associated with the given correction point
	 */
	public List<String> getFileList(CorrectionPoint point) {
		List<String> fileNameList = new ArrayList<>();
		ICorrectionPointVisitor visitor = new ICorrectionPointVisitor() {
			@Override
			public void visit(FileCorrectionPoint p) {
				fileNameList.add(p.getFileName());
			};

			@Override
			public void visit(CommonCorrectionPoint p) {
				for (SounderDataContainer dataContainer : soundingsModel.getSounderDataContainers()) {
					fileNameList.add(dataContainer.getInfo().getPath());
				}
			};
		};
		point.accept(visitor);
		return fileNameList;
	}

	public void clear() {
		correctionList.getCorrectionPointList().clear();
	}
}
