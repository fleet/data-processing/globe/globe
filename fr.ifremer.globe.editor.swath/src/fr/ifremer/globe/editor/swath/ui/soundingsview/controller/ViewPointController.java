package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;

import fr.ifremer.globe.core.model.geometry.OrientedBox;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.BoundingBoxes;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Update the view point when needed
 */
public class ViewPointController {

	/** Eclipse event broker service */
	@Inject
	protected IEventBroker eventBroker;

	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingModel;

	/** ViewPoint */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_VIEW_POINT)
	protected ViewPoint viewPoint;

	/**
	 * retain last bounding boxes and selection
	 */
	protected BoundingBoxes boxes;

	/**
	 * Recompute view point settings when bounding boxes changed
	 */
	/**
	 * Change of the the soundings selection.
	 */
	@Inject
	@Optional
	void onBoundingBoxesUpdated(@UIEventTopic(EventTopics.BOUNDING_BOXES_UPDATED) BoundingBoxes boundingBoxes) {
		boxes = boundingBoxes;
		if (boxes == null || !viewPoint.isLock())
			// no bounding boxes OR not lock : apply the new bounding boxes
			toMainAxis(false);
		else
			updateView();
	}

	/**
	 * update view center, all data position will be translated relatively to this point
	 */
	private void computeViewCenter() {
		double x = boxes.getActiveOBB().getAxisBB().getMeanX() * boxes.getActiveOBB().getMainAxis().getX()
				+ boxes.getActiveOBB().getAxisBB().getMeanY() * boxes.getActiveOBB().getSecondaryAxis().getX();
		double y = boxes.getActiveOBB().getAxisBB().getMeanX() * boxes.getActiveOBB().getMainAxis().getY()
				+ boxes.getActiveOBB().getAxisBB().getMeanY() * boxes.getActiveOBB().getSecondaryAxis().getY();
		double z = boxes.getActiveOBB().getAxisBB().getMeanZ();

		viewPoint.setWorldCenter(new Vector3D(x, y, z));
	}

	/**
	 * recompute view point based on previous settings and bounding boxes
	 */
	public void toMainAxis(boolean notify) {
		viewPoint.setLock(false);
		computeMainAxisView();
		if (notify)
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eViewPointChanged);
	}

	private void updateView() {
		computeViewCenter();
		double maxLengthAlongMainAxis = Math
				.abs(boxes.getActiveOBB().getAxisBB().getMaxX() - boxes.getActiveOBB().getAxisBB().getMinX());
		viewPoint.setRangeXYZ(maxLengthAlongMainAxis * 1.01);
		computeScalesFactor();
	}

	public void toTopView(boolean notify) {
		viewPoint.setLock(false);
		computeTopView();
		if (notify)
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eViewPointChanged);
	}

	public void lockView(boolean lock, boolean notify) {
		viewPoint.setLock(lock);
		if (notify)
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eViewPointChanged);
	}

	private void computeTopView() {
		OrientedBox selection = soundingModel.getSelectedArea();
		if (selection == null)
			return;
		computeViewCenter();
		viewPoint.getCamera().setTarget(new Vector3D(0, 0, 0));
		Vector2D dir = boxes.getActiveOBB().getSecondaryAxis().normalize();
		viewPoint.getCamera().setUp(new Vector3D(dir.getX(), dir.getY(), 0));

		double maxLengthAlongMainAxis = Math
				.abs(boxes.getActiveOBB().getAxisBB().getMaxX() - boxes.getActiveOBB().getAxisBB().getMinX());
		double maxLengthAlongSecondaryAxis = Math
				.abs(boxes.getActiveOBB().getAxisBB().getMaxY() - boxes.getActiveOBB().getAxisBB().getMinY());

		double mainAxisSizes = Math.max(maxLengthAlongMainAxis, maxLengthAlongSecondaryAxis) * 1.01;
		viewPoint.setRangeXYZ(mainAxisSizes);

		computeScalesFactor();

		viewPoint.getCamera().setEye(new Vector3D(0, 0, computeMaxEyeDistance()));
	}

	/**
	 * compute max eye distance, scale factor should have been updated before
	 */
	private double computeMaxEyeDistance() {
		return new Vector3D(boxes.getActiveBB().getDeltaX(), boxes.getActiveBB().getDeltaY(),
				boxes.getActiveBB().getDeltaZ() * viewPoint.getScalingFactor().getZ()).getMagnitude();
	}

	private void computeMainAxisView() {

		OrientedBox selection = soundingModel.getSelectedArea();
		if (selection == null)
			return;

		// update view center, all data position will be translated relatively to this point
		computeViewCenter();

		// set target to 0,0,0 since coordinates will be shifted by shaders to be relative to the center of the boxes
		viewPoint.getCamera().setTarget(new Vector3D(0, 0, 0));

		viewPoint.getCamera().setUp(new Vector3D(0, 0, 1));

		// now we will project each point of the bounding boxes on both axis and
		// keep their projection values

		double maxLengthAlongMainAxis = Math
				.abs(boxes.getActiveOBB().getAxisBB().getMaxX() - boxes.getActiveOBB().getAxisBB().getMinX());
		viewPoint.setRangeXYZ(maxLengthAlongMainAxis * 1.01);

		computeScalesFactor();
		double d = computeMaxEyeDistance();

		Vector2D view = selection.getSecondaryAxis().normalize().scalarMultiply(d);
		viewPoint.getCamera().setEye(new Vector3D(view.getX(), view.getY(), 0));
	}

	/**
	 * compute scales factors Scale factors is used to fill the entire area for displayed soundings (kinda ideal zoom
	 * factor)
	 */
	private void computeScalesFactor() {
		if (viewPoint.isVerticalExaggeration()) {
			viewPoint.setScalingFactor(1d, viewPoint.getRangeXYZ() / boxes.getActiveBB().getDeltaZ());
		} else {
			viewPoint.setScalingFactor(1d, 1d);
		}
	}

	void updateVerticalExaggeration(boolean exaggeration, boolean notify) {
		viewPoint.setVerticalExaggeration(exaggeration);
		computeScalesFactor();
		if (notify)
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eViewPointChanged);
	}

}
