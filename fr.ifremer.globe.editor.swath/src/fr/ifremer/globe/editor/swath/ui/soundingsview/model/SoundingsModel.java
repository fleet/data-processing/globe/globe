﻿package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;

import fr.ifremer.globe.core.model.geometry.OrientedBox;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.BathyDtmModel;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndexRegistry;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SVDisplayParameterChanged;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.BoundingBoxes;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

public class SoundingsModel {

	/** Current DTM model */
	private final BathyDtmModel dtmModel;

	/** SpatialIndex provider */
	private final SpatialIndexRegistry spatialIndexRegistry;

	/** Eclipse event broker service */
	private final IEventBroker eventBroker;

	/** Selection dtmModel. */
	private final List<SelectionModel> selectionModels = new ArrayList<>();

	private final RotatingColors rotatingColorsMap = new RotatingColors();

	/**
	 * Appearances of files <br>
	 * Same file order than oundingsModel.getInfoStores()
	 */
	private final List<FileAppearance> fileAppearances = new ArrayList<>();

	private DisplayParameters displayParameters;

	private BufferAssociation association = new BufferAssociation();
	/**
	 * The sounding bounding box of displayed soundings
	 */
	private BoundingBoxes soundingBB;

	// model for bias correction
	private BiasCorrectionModel biasCorrectionModel = null;

	// loaded geo zone
	private OrientedBox selectedArea;

	private boolean loaded;

	/** Size of beams. */
	private float beamSize = 1f;

	/**
	 * values bounds
	 */
	ValuesBoundings valueBoundings = new ValuesBoundings();

	/**
	 * Constructor
	 */
	@Inject
	public SoundingsModel(@Named(ContextNames.BATHY_DTM_MODEL) BathyDtmModel dtmModel,
			@Named(ContextNames.SPATIAL_INDEX_REGISTRY) SpatialIndexRegistry spatialIndexRegistry,
			IEventBroker eventBroker) {
		this.dtmModel = dtmModel;
		this.spatialIndexRegistry = spatialIndexRegistry;
		this.eventBroker = eventBroker;
	}

	/**
	 * Mandatory initialization after construction.
	 */
	@PostConstruct
	private void initialize() throws IOException {
		displayParameters = ContextInitializer.make(DisplayParameters.class);
		for (SounderDataContainer dataContainer : dtmModel.getSounderDataContainers()) {
			SpatialIndex spatialindex = spatialIndexRegistry.get(dataContainer);
			association.addFile(dataContainer, spatialindex);

			GColor color = rotatingColorsMap.getColor(dataContainer.getInfo());
			fileAppearances.add(new FileAppearance(true, true, color != null ? color : GColor.WHITE));

			// Model selection initialization
			SelectionModel selectionModel = ContextInitializer.make(SelectionModel.class);
			selectionModel.initSelectionArray(spatialindex.getMappings());
			selectionModel.resetSelection();
			selectionModels.add(selectionModel);
		}

		soundingBB = ContextInitializer.make(BoundingBoxes.class);
		biasCorrectionModel = ContextInitializer.make(BiasCorrectionModel.class);
	}

	/** Event handler for SOUNDING_VIEW_PARAMETER_CHANGED */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void displayParametersChanged(
			@UIEventTopic(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED) SVDisplayParameterChanged event) {
		switch (event) {
		case eVisibleMode:
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDataChanged);
			break;
		default:
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDisplayChanged);
			break;
		}
	}

	/** React to the change of BeamDisplayMode */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onBeamDisplayModeChanged(@Named(ContextNames.BEAM_DISPLAY_MODE) BeamDisplayMode beamDisplayMode) {
		// Refresh the soundings view
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDisplayChanged);
	}

	/** React to the change of BeamDisplayMode */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onKeepInvalidBeamInRedChanged(
			@Named(ContextNames.KEEP_INVALID_BEAM_IN_RED) boolean keepInvalidBeamInRed) {
		// Refresh the soundings view
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDisplayChanged);
	}

	@PreDestroy
	void preDestroy() {
		selectionModels.parallelStream().forEach(SelectionModel::close);
		selectionModels.clear();
	}

	public void setFileStat(FileStat stat, ISounderNcInfo file) {
		statsPerFile.put(file, stat);
	}

	/**
	 * return stat per file of displayed data
	 */
	public FileStat getFileStat(ISounderNcInfo file) {
		return statsPerFile.get(file);
	}

	private HashMap<ISounderNcInfo, FileStat> statsPerFile = new HashMap<>();

	public List<SounderDataContainer> getSounderDataContainers() {
		return dtmModel.getSounderDataContainers();
	}

	/**
	 * Informed that the loadding is starting.
	 *
	 * @param selection
	 */
	public void resetBounds(OrientedBox selection) {
		getSoundingBB().begin(selection);
		valueBoundings.begin();

	}

	/**
	 * Consider a new beam in this model.
	 *
	 * @param reflectivity
	 */
	public void addBeam(int cycleIndex, int beamIndex, float x, float y, float depth, float reflectivity,
			float qualityFactor) {
		getSoundingBB().push(x, y, depth);
		valueBoundings.push(cycleIndex, beamIndex, x, y, depth, reflectivity, qualityFactor);
	}

	/**
	 * Informed that the loading is finished.
	 */
	public void finalizeBounds() {
		getSoundingBB().end();
		valueBoundings.end();
	}

	public boolean hasBeams() {
		return loaded;
	}

	public List<ISounderNcInfo> getInfoStores() {
		return dtmModel.getSounderDataContainers().stream().map(SounderDataContainer::getInfo).toList();
	}

	public BoundingBoxes getSoundingBB() {
		return soundingBB;
	}

	/**
	 * return the beam association (see {@link BufferAssociation})
	 *
	 */
	public BufferAssociation getBeamsAssociation() {
		return association;
	}

	/**
	 * @return the {@link #selectionModels}
	 */
	public List<SelectionModel> getSelectionModels() {
		return selectionModels;
	}

	public DisplayParameters getDisplayParameters() {
		return displayParameters;
	}

	/**
	 * @return the {@link #beamSize}
	 */
	public float getBeamSize() {
		return beamSize;
	}

	/**
	 * @param beamSize the {@link #beamSize} to set
	 */
	public void setBeamSize(float beamSize) {
		this.beamSize = beamSize;
	}

	/**
	 * @return the valueBoundings
	 */
	public ValuesBoundings getValueBoundings() {
		return valueBoundings;
	}

	/**
	 * @return the selectedArea
	 */
	public OrientedBox getSelectedArea() {
		return selectedArea;
	}

	/**
	 * @param selectedArea the selectedArea to set
	 */
	public void setSelectedArea(OrientedBox selectedArea) {
		this.selectedArea = selectedArea;
	}

	/**
	 * @return the biasCorrectionModel
	 */
	public BiasCorrectionModel getBiasCorrectionModel() {
		return biasCorrectionModel;
	}

	/**
	 * All characteristics defining the appearance of a file
	 */
	public static record FileAppearance(boolean displayed, boolean editable, GColor color) {

		/** Duplicates with the specified displayed value */
		public FileAppearance withDisplay(boolean displayed) {
			return new FileAppearance(displayed, editable, color);
		}

		/** Duplicates with the specified editable value */
		public FileAppearance withEditable(boolean editable) {
			return new FileAppearance(displayed, editable, color);
		}

		/** Duplicates with the specified color */
		public FileAppearance withColor(GColor color) {
			return new FileAppearance(displayed, editable, color);
		}
	}

	/**
	 * Return the instance of FileAppearance associated to the file (identified by its index). <br>
	 * Same file order than oundingsModel.getInfoStores()
	 */
	public FileAppearance getFileAppearances(int fileIdx) {
		return fileAppearances.get(fileIdx);
	}

	/** Change the FileAppearance of the file */
	public void changeFileAppearances(int fileIdx, FileAppearance fileAppearance) {
		fileAppearances.set(fileIdx, fileAppearance);
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDisplayChanged);

	}
}
