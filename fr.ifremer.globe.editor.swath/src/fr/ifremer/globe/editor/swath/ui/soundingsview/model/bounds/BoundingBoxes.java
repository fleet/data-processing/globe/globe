package fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds;

import org.eclipse.e4.core.services.events.IEventBroker;

import fr.ifremer.globe.core.model.geometry.OrientedBox;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Compute all the required bounding boxes.<br>
 * The computation is done like this :
 * <p>
 * first begin()
 * <p>
 * then add all soundings
 * <p>
 * the end()
 * <p>
 */
public class BoundingBoxes {

	private final DtmPreference dtmPreferences;
	private final IEventBroker eventBroker;

	private AxisAlignedSoundingBoundingBox boundingBox = new AxisAlignedSoundingBoundingBox();
	private OrientedSoundingBoundingBox orientedBoundingBox = new OrientedSoundingBoundingBox();
	private boolean autoScale = true;

	/**
	 * Constructor
	 */
	@Inject
	public BoundingBoxes(@Named(ContextNames.DTM_PREFERENCES) DtmPreference dtmPreferences, IEventBroker eventBroker) {
		this.dtmPreferences = dtmPreferences;
		this.eventBroker = eventBroker;
	}

	public void begin(OrientedBox selection) {
		boundingBox.begin();
		orientedBoundingBox.begin(selection);
	}

	public void end() {
		int overflow = dtmPreferences.getBoundingOverflow().getValue().intValue();
		boundingBox.end(overflow > 0 ? (overflow / 100d) : 0d);
		orientedBoundingBox.end(overflow > 0 ? (overflow / 100d) : 0d);
		eventBroker.send(EventTopics.BOUNDING_BOXES_UPDATED, this);
	}

	public void push(float x, float y, float z) {
		boundingBox.push(x, y, z);
		orientedBoundingBox.push(x, y, z);
	}

	/** Set minimum value of the Z axis in a manual scalling mode */
	public void setMinZDefinedByUser(Double value) {
		getActiveBB().setMinZDefinedByUser(value);
		getActiveOBB().getAxisBB().setMinZDefinedByUser(value);
	}

	/** Set maximum value of the Z axis in a manual scalling mode */
	public void setMaxZDefinedByUser(Double value) {
		getActiveBB().setMaxZDefinedByUser(value);
		getActiveOBB().getAxisBB().setMaxZDefinedByUser(value);
	}

	/** Set the step value of the Z axis in a manual scalling mode */
	public void setStepZDefinedByUser(Float value) {
		getActiveBB().setStepZDefinedByUser(value);
		getActiveOBB().getAxisBB().setStepZDefinedByUser(value);
	}

	/**
	 * return the active Oriented BoundingBox Active bounding box is set depending on displayParameters
	 */
	public OrientedSoundingBoundingBox getActiveOBB() {
		return orientedBoundingBox;
	}

	/**
	 * return the active BoundingBox Active bounding box is set depending on displayParameters
	 */
	public AxisAlignedSoundingBoundingBox getActiveBB() {
		return boundingBox;
	}

	/** Getter of {@link #autoScale} */
	public boolean isAutoScale() {
		return autoScale;
	}

	/** Setter of {@link #autoScale} */
	public void setAutoScale(boolean autoScale) {
		this.autoScale = autoScale;

		// Reset values
		setStepZDefinedByUser(null);
		setMinZDefinedByUser(null);
		setMaxZDefinedByUser(null);
	}
}
