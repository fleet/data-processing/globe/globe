package fr.ifremer.globe.editor.swath.ui.soundingsview.preferences;

public enum SelectionMode {
	QUAD,
	POLYGON,
	LASSO
}
