/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.awt.Color;
import java.nio.FloatBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.utils.TransformationUtils;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.textures.TextureFloat;

/**
 * Binder to bind a texture on MTU 0.
 */
public class NormalizedTextureBinder extends AShaderBinder {

	/** Number of color in pallette. */
	final static int PALETTE_COLOR_COUNT = 1000;

	public TextureFloat texture = null;

	/** {@inheritDoc} */
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		super.bindUniforms(lineBuffer);

		// Initialize color uniforms
		if (texture != null) {
			texture.bind(GLU.getCurrentGL().getGL2());
		} else {
			createAndBindTextureFloat();
		}

	}

	private void createAndBindTextureFloat() {
		FloatBuffer pixels = FloatBuffer.allocate(PALETTE_COLOR_COUNT * 3);
		for (int i = 0; i < pixels.capacity();) {
			Color color = TransformationUtils
					.getColorFromNormalizedElevation(((double) i) / ((double) PALETTE_COLOR_COUNT) / 3d);
			pixels.put(i++, color.getRed() / 255f);
			pixels.put(i++, color.getGreen() / 255f);
			pixels.put(i++, color.getBlue() / 255f);
		}

		texture = Device2x.createTextureFloatRectangle(pixels, 1, PALETTE_COLOR_COUNT, GL.GL_TEXTURE_2D,
				GL.GL_LINEAR_MIPMAP_LINEAR, GL.GL_LINEAR, GL.GL_RGB, GL.GL_RGB, GL.GL_FLOAT);

	}

}
