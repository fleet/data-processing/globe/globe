package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import java.nio.FloatBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;

import com.jogamp.common.nio.Buffers;

/**
 * Class representing VBO object
 * 
 * @author bvalliere
 */
public class VBO {

	/** Name of the Open GL buffer */
	private int[] bufferName;

	/** Number of elements to render */
	private int elementCount = 0;

	/** Buffer */
	private FloatBuffer buffer;

	/** Size of elements */
	private int elementSize;

	/**
	 * Specifies the expected usage pattern of the data store. The symbolic
	 * constant must be GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY,
	 * GL_STATIC_DRAW, GL_STATIC_READ, GL_STATIC_COPY, GL_DYNAMIC_DRAW,
	 * GL_DYNAMIC_READ, or GL_DYNAMIC_COPY.
	 */
	protected int usage = GL.GL_STATIC_DRAW;

	/**
	 * Constructor.
	 */
	public VBO(int elementSize) {
		this.elementSize = elementSize;
	}

	public void allocateBuffer(int elementCount) {
		if (this.buffer != null) {
			this.buffer.clear();
		}
		this.buffer = Buffers.newDirectFloatBuffer(elementCount * this.elementSize);
	}

	/**
	 * Delete named buffer objects
	 * 
	 * @param gl
	 */
	public void dispose(GL gl) {
		gl.glDeleteBuffers(1, this.bufferName, 0);
	}
	/**
	 * Add a value to buffer
	 * 
	 * @param x
	 */
	public void put(double x) {
		this.buffer.put((float) x);
	}

	/**
	 * Add a values to buffer
	 * 
	 * @param values
	 */
	public void put(float[] values) {
		this.buffer.put(values);
	}

	/**
	 * Generate and bind the buffer
	 * 
	 * @param gl
	 * @param vbo
	 */
	public void bindAndLoad(GL gl1) {
		GL2 gl = gl1.getGL2();
		this.elementCount = this.buffer.capacity() / this.elementSize;
		this.buffer.rewind();

		if (this.bufferName == null) {
			this.bufferName = new int[1];
		} else {
			gl.glDeleteBuffers(1, this.bufferName, 0);
		}

		// Get a valid name
		gl.glGenBuffers(1, this.bufferName, 0);
		// Bind the buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.bufferName[0]);
		// Load the data
		gl.glBufferData(GL.GL_ARRAY_BUFFER, this.buffer.capacity() * Buffers.SIZEOF_FLOAT, this.buffer, getUsage());
	}

	/**
	 * Updates a subset of a buffer object's data store
	 */
	public void update(GL gl1, long offsetInBytes, long sizeInBytes) {
		gl1.getGL2().glBindBuffer(GL.GL_ARRAY_BUFFER, this.bufferName[0]);
		gl1.getGL2().glBufferSubData(GL2.GL_ARRAY_BUFFER, offsetInBytes, sizeInBytes, this.buffer);
	}

	public void drawVertexArray(GL gl1, int glType, int index, int nb) {

		GL2 gl = gl1.getGL2();
		// Enable vertex arrays
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);

		// Set the vertex pointer to the vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.bufferName[0]);
		gl.glVertexPointer(this.elementSize, GL.GL_FLOAT, 0, 0);

		// Rendering
		gl.glDrawArrays(glType, index, nb);

		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
	}
	
	public void position(int position) {
		this.buffer.position(position);
	}
	
	public void drawVertexArrayWithColorArray(GL gl1, int glType, int index, int nb, VBO color) {

		GL2 gl = gl1.getGL2();
		// Enable vertex arrays
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GLPointerFunc.GL_COLOR_ARRAY);

		// Set the vertex pointer to the vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.bufferName[0]);
		gl.glVertexPointer(this.elementSize, GL.GL_FLOAT, 0, 0);
		//Set color pointer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, color.bufferName[0]);
		gl.glColorPointer(this.elementSize, GL.GL_FLOAT, 0, 0);

		// Rendering
		gl.glDrawArrays(glType, index, nb);
		
		//unbind
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_COLOR_ARRAY);
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
	}

	public void drawVertexArrayWithColorArray(GL gl1, int glType, VBO color) {
		drawVertexArrayWithColorArray(gl1, glType, 0, this.elementCount, color);
	}
	

	public int getElementCount() {
		return this.elementCount;
	}

	public int getElementSize() {
		return this.elementSize;
	}

	public int[] getBufferName() {
		return this.bufferName;
	}

	public void clear() {
		this.buffer.clear();
	}

	public float get() {
		return this.buffer.get();
	}

	/**
	 * @return the {@link #usage}
	 */
	public int getUsage() {
		return this.usage;
	}

	/**
	 * @param usage
	 *            the {@link #usage} to set
	 */
	public void setUsage(int usage) {
		this.usage = usage;
	}

	/**
	 * @return the {@link #buffer}
	 */
	public FloatBuffer getBuffer() {
		return this.buffer;
	}
}