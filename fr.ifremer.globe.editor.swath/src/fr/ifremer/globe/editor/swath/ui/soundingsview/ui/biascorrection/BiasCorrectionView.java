package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.biascorrection;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.processes.biascorrection.filereaders.CorrectionFileUtils;
import fr.ifremer.globe.core.processes.biascorrection.model.CommonCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.FileCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.ICorrectionPointVisitor;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsComposite;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector.BiasCorrectionEstimator;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector.BiasCorrector;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.BiasProvider;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

/**
 * Contains a table which displays all attribute data about beams selected in the {@link SoundingsComposite}.
 */
public class BiasCorrectionView {

	private static final Logger LOGGER = LoggerFactory.getLogger(BiasCorrectionView.class);

	public static final String PART_ID = "fr.ifremer.globe.editor.swath.partdescriptor.biasCorrectionView";

	/** Local context objects **/
	private final SoundingsController soundingsControler;
	private final SoundingsModel soundingsModel;
	private final BiasCorrector biasCorrector;
	private final BiasCorrectionEstimator biasCorrectionEstimator;

	private static final String COMMON_SELECTION = "Common";
	private static final String FILE_COLUMN_NAME = "File";
	private static final String DATE_COLUMN_NAME = "Date";
	private static final String VELOCITY_COLUMN_NAME = "Velocity (m/s)";
	private static final String HEADING_COLUMN_NAME = "Heading (°)";
	private static final String MRU_HEADING_COLUMN_NAME = "MRU Heading (°)";
	private static final String ROLL_COLUMN_NAME = "Roll (°)";
	private static final String PITCH_COLUMN_NAME = "Pitch (°)";
	private static final String VERTICAL_COLUMN_NAME = "Vertical offset (m)";
	private static final String ATTITUDE_TIMEDELAY_COLUMN_NAME = "Attitude Time (ms)";
	private static final String VERTICAL_OFFSET_TIMEDELAY_COLUMN_NAME = "Vertical offset Time (ms)";

	public static final int FILE_COLUMN_INDEX = 0;
	public static final int DATE_COLUMN_INDEX = 1;
	public static final int VELOCITY_COLUMN_INDEX = 2;
	public static final int HEADING_COLUMN_INDEX = 3;
	public static final int ROLL_COLUMN_INDEX = 4;
	public static final int PITCH_COLUMN_INDEX = 5;
	public static final int VERTICAL_COLUMN_INDEX = 6;
	public static final int MRU_HEADING_COLUMN_INDEX = 7;
	public static final int ATTITUDE_TIME_COLUMN_INDEX = 8;
	public static final int VERTICAL_OFFSET_TIME_COLUMN_INDEX = 9;
	public static final int REMOVE_LINE_INDEX = 10;

	//
	private CorrectionList correctionList;
	private List<CorrectionPoint> addedCorrectionPoints = new ArrayList<>();

	/** Inner composites **/
	private final Composite parent;
	private TableViewer tableViewer;
	private Group currentCorrectionGroup;
	private ComboViewer filteringAntennaIndex;
	private ComboViewer comboFileViewer;
	private CorrectionPointComposite correctionPointComposite;

	/** Handler subscribed to IEventBroker for PartStack management */
	private EventHandler eventHandler;

	/**
	 * Executed after dependency injection is done to perform any initialization
	 */
	@Inject
	public BiasCorrectionView(IEventBroker eventBroker, Composite parent) {
		soundingsControler = ContextInitializer.getInContext(ContextNames.SOUNDINGS_CONTROLLER);
		soundingsModel = ContextInitializer.getInContext(ContextNames.SOUNDINGS_MODEL);
		biasCorrector = ContextInitializer.getInContext(ContextNames.BIAS_CORRECTOR);
		biasCorrectionEstimator = ContextInitializer.make(BiasCorrectionEstimator.class);

		parent.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		this.parent = parent;
		correctionList = soundingsModel.getBiasCorrectionModel().getCorrectionList();
		initComposites();
		initCurrentCorrectionGroup();
		initTables();
		soundingsControler.getView().setFocus();

		this.eventHandler = PartUtil.onPartStackChanged(PART_ID, eventBroker, this::onPartStackChanged);
	}

	@PreDestroy
	private void dispose(IEventBroker eventBroker) {
		try {
			if (biasCorrector != null)
				biasCorrector.cleanupProcess();
		} catch (IOException e) {
			// Message has already been logged
		}
		eventBroker.unsubscribe(eventHandler);
	}

	public void initComposites() {
		GridLayout gl_parent = new GridLayout(1, false);
		parent.setLayout(gl_parent);

		//
		Group correctionListGroup = new Group(parent, SWT.NONE);
		correctionListGroup.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		GridData gd_correctionListGroup = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_correctionListGroup.heightHint = 104;
		correctionListGroup.setLayoutData(gd_correctionListGroup);
		GridLayout layoutM = new GridLayout(3, false);
		layoutM.marginHeight = 0;
		layoutM.marginWidth = 0;
		correctionListGroup.setLayout(layoutM);
		correctionListGroup.setText("Correction points");

		initTable(correctionListGroup);
		Table table = tableViewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 3, 1));
		tableViewer.getTable().setLinesVisible(true);
		tableViewer.getTable().setHeaderVisible(true);

		var exportCommunButton = new Button(correctionListGroup, SWT.NONE);
		exportCommunButton.setText("Export Common...");
		exportCommunButton.addListener(SWT.Selection, event -> {
			exportCommonCorrectionFile();
			soundingsControler.getView().setFocus();
		});
		var exportIndividualButton = new Button(correctionListGroup, SWT.NONE);
		exportIndividualButton.setText("Export Individual");
		exportIndividualButton.addListener(SWT.Selection, event -> {
			exportIndividualCorrectionFile();
			soundingsControler.getView().setFocus();
		});
		var deleteButton = new Button(correctionListGroup, SWT.NONE);
		deleteButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		deleteButton.setToolTipText("Remove selected correction lines.");
		deleteButton.setText("Remove selected lines");
		deleteButton.addListener(SWT.Selection, event -> {
			int[] selectionIndices = tableViewer.getTable().getSelectionIndices();
			int selectionIndex = 0;
			for (int i = selectionIndices.length - 1; i >= 0; i--) {
				selectionIndex = selectionIndices[i];
				CorrectionPoint point = correctionList.getCorrectionPointList().get(selectionIndex);
				addedCorrectionPoints.remove(point);
				correctionList.getCorrectionPointList().remove(point);
			}
			updateTable();
			preview();
			soundingsControler.getView().setFocus();
		});
	}

	private void initCurrentCorrectionGroup() {
		// current correction
		currentCorrectionGroup = new Group(parent, SWT.NONE);
		currentCorrectionGroup.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		currentCorrectionGroup.setText("Current correction");
		GridData gd_currentCorrectionGroup = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		currentCorrectionGroup.setLayoutData(gd_currentCorrectionGroup);
		GridLayout gl_currentCorrectionGroup = new GridLayout(1, false);
		gl_currentCorrectionGroup.verticalSpacing = 0;
		gl_currentCorrectionGroup.marginHeight = 0;
		currentCorrectionGroup.setLayout(gl_currentCorrectionGroup);

		Composite currentCorrectionComposite = new Composite(currentCorrectionGroup, SWT.NONE);
		GridData gd_currentCorrectionComposite = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_currentCorrectionComposite.widthHint = 658;
		gd_currentCorrectionComposite.heightHint = 35;
		currentCorrectionComposite.setLayoutData(gd_currentCorrectionComposite);
		GridLayout gl_currentCorrectionComposite = new GridLayout(6, false);
		gl_currentCorrectionComposite.verticalSpacing = 0;
		currentCorrectionComposite.setLayout(gl_currentCorrectionComposite);

		Label lblApplyOn = new Label(currentCorrectionComposite, SWT.NONE);
		lblApplyOn.setText("Apply on : ");

		// File
		comboFileViewer = new ComboViewer(currentCorrectionComposite, SWT.READ_ONLY);
		Combo combo = comboFileViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		comboFileViewer.setContentProvider(ArrayContentProvider.getInstance());

		// Antenna
		var cmpAntennaIndex = new Composite(currentCorrectionComposite, SWT.NONE);
		cmpAntennaIndex.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));
		var layoutAntennaIndex = new RowLayout();
		layoutAntennaIndex.center = true;
		cmpAntennaIndex.setLayout(layoutAntennaIndex);
		Label lblAntenna = new Label(cmpAntennaIndex, SWT.NONE);
		lblAntenna.setText("Antenna index :");

		filteringAntennaIndex = new ComboViewer(cmpAntennaIndex);
		filteringAntennaIndex.setContentProvider(ArrayContentProvider.getInstance());
		filteringAntennaIndex.setLabelProvider(LabelProvider
				.createTextProvider(element -> Integer.valueOf(-1).equals(element) ? "All" : element.toString()));
		filteringAntennaIndex.addSelectionChangedListener(e -> preview());

		Button btnNewButton = new Button(currentCorrectionComposite, SWT.NONE);
		btnNewButton.setText("Add");

		Button btnClear = new Button(currentCorrectionComposite, SWT.NONE);
		btnClear.setText("Clear");

		Button btnCompute = new Button(currentCorrectionComposite, SWT.NONE);
		btnCompute.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnCompute.setText("Compute");
		btnCompute.addListener(SWT.Selection, e -> {
			var estimationResult = biasCorrectionEstimator.executeProcess();
			Messages.openInfoMessage("Bias correction estimation", estimationResult.getSecond());
		});

		correctionPointComposite = new CorrectionPointComposite(currentCorrectionGroup, SWT.NONE, this::preview);
		correctionPointComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		correctionPointComposite.setDate(biasCorrector.getMinDateSelection());

		// initialize selection
		onSelectedAreaChanged();

		// hook listeners
		btnClear.addListener(SWT.Selection, e -> {
			correctionPointComposite.clear();
			updateCorrectionPointDate(); // keep date
		});
		btnNewButton.addListener(SWT.Selection, e -> {
			soundingsModel.getBiasCorrectionModel().addCorrectionPoint(getCurrentCorrectionPoint());
			updateTable();
		});
		comboFileViewer.addSelectionChangedListener(e -> {
			updateCorrectionPointDate();
			preview();
		});
	}

	/** Update the number of antennas */
	public void setAntennaCount(int antennaCount) {
		// Model changed ?
		if (filteringAntennaIndex.getCombo().getItemCount() != antennaCount + 1) {
			var antennaModel = new LinkedList<Integer>();
			IntStream.range(-1, antennaCount).forEach(antennaModel::add); // -1 for all antenna
			filteringAntennaIndex.setInput(antennaModel);
			if (filteringAntennaIndex.getSelection().isEmpty()) {
				filteringAntennaIndex.setSelection(new StructuredSelection(Integer.valueOf(-1)));
			}
		}
	}

	/** Return the selected antenna index or OptionalInt.empty when ALL */
	public OptionalInt getAntennaIndex() {
		var selectedAntennaIndex = (Integer) filteringAntennaIndex.getStructuredSelection().getFirstElement();
		return selectedAntennaIndex == null || Integer.valueOf(-1).equals(selectedAntennaIndex) ? OptionalInt.empty()
				: OptionalInt.of(selectedAntennaIndex.intValue());
	}

	private void updateCorrectionPointDate() {
		var targetSelection = (String) comboFileViewer.getStructuredSelection().getFirstElement();
		var date = targetSelection == COMMON_SELECTION ? biasCorrector.getMinDateSelection()
				: biasCorrector.getMinDateFileSelection(targetSelection);
		correctionPointComposite.setDate(date);
		currentCorrectionGroup.setText("Current correction ( " + date + " )");
	}

	private void onSelectedAreaChanged() {
		// update file selection combobox if necessary
		var fileModel = new LinkedList<String>();
		int antennaCount = 0;
		for (ISounderNcInfo sounderNcInfo : soundingsModel.getInfoStores()) {
			if (biasCorrector.getMinDateFileSelection(sounderNcInfo.getFilename()) != Instant.EPOCH) {
				fileModel.add(sounderNcInfo.getFilename());
				antennaCount = Math.max(antennaCount, sounderNcInfo.getAntennaParameters().size());
			}
		}
		fileModel.add(COMMON_SELECTION);

		@SuppressWarnings("unchecked")
		var previousInputs = (List<String>) comboFileViewer.getInput();
		if (previousInputs == null || !fileModel.stream().allMatch(previousInputs::contains)
				|| !previousInputs.stream().allMatch(fileModel::contains)) {
			comboFileViewer.setInput(fileModel);
			comboFileViewer.setSelection(new StructuredSelection(COMMON_SELECTION));
		}

		setAntennaCount(antennaCount);

		updateCorrectionPointDate();
	}

	private CorrectionPoint getCurrentCorrectionPoint() {
		CorrectionPoint correctionPoint = correctionPointComposite.getCurrentCorrectionPoint();
		// set file if selected
		var targetSelection = comboFileViewer.getStructuredSelection().getFirstElement();
		if (targetSelection != COMMON_SELECTION) {
			var targetFileName = (String) comboFileViewer.getStructuredSelection().getFirstElement();
			correctionPoint = new FileCorrectionPoint(correctionPoint, targetFileName);
		}
		return correctionPoint;
	}

	/**
	 * Initialize all tables of the {@link BiasCorrectionDialog}.
	 */
	public void initTables() {
		createTableEditor(tableViewer, correctionList);
		updateTable();
		selectionOnTable();
	}

	/**
	 * Initialize a {@link TableViewer} with columns and editor listener.
	 *
	 * @param isUnique true if only one {@link TableViewer} should be into the parent composite.
	 * @param style style of the {@link Table} contained by the {@link TableViewer}.
	 * @param parent parent composite of {@link TableViewer}
	 * @param timeColumnVisible (true| false)
	 * @return a new {@link TableViewer} which is adapted for the correction files edition
	 */
	public void initTable(Composite parent) {
		tableViewer = new TableViewer(parent, SWT.FULL_SELECTION | SWT.MULTI | SWT.CHECK);

		TableViewerColumn fileColumn = new TableViewerColumn(tableViewer, SWT.LEFT);
		fileColumn.getColumn().setToolTipText("Commun: all files");
		fileColumn.getColumn().setText(FILE_COLUMN_NAME);
		fileColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn dateColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		dateColumn.getColumn().setText(DATE_COLUMN_NAME);
		dateColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn velocityColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		velocityColumn.getColumn().setText(VELOCITY_COLUMN_NAME);
		velocityColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn headingColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		headingColumn.getColumn().setText(HEADING_COLUMN_NAME);
		headingColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn rollColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		rollColumn.getColumn().setText(ROLL_COLUMN_NAME);
		rollColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn pitchColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		pitchColumn.getColumn().setText(PITCH_COLUMN_NAME);
		pitchColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn heaveColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		heaveColumn.getColumn().setText(VERTICAL_COLUMN_NAME);
		heaveColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn mruHeadingColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		mruHeadingColumn.getColumn().setText(MRU_HEADING_COLUMN_NAME);
		mruHeadingColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn attitudeTimeDelayColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		attitudeTimeDelayColumn.getColumn().setText(ATTITUDE_TIMEDELAY_COLUMN_NAME);
		attitudeTimeDelayColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn verticalOffsetTimeDelayColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		verticalOffsetTimeDelayColumn.getColumn().setText(VERTICAL_OFFSET_TIMEDELAY_COLUMN_NAME);
		verticalOffsetTimeDelayColumn.setLabelProvider(new BiasProvider());

		// Colomn to remove a line: a cross will be added for each line
		TableViewerColumn removeLineColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		removeLineColumn.getColumn().setToolTipText("Remove all lines");
		removeLineColumn.getColumn()
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/Red_X.png"));
		removeLineColumn.setLabelProvider(new BiasProvider());
		removeLineColumn.getColumn().addListener(SWT.Selection, event -> {
			correctionList.getCorrectionPointList().clear();
			updateTable();
			addedCorrectionPoints.clear();
			preview();
			soundingsControler.getView().setFocus();
		});

		parent.setRedraw(true);
		parent.layout(true);

	}

	/**
	 * Selection points of table to apply
	 */
	private void selectionOnTable() {
		tableViewer.getTable().addListener(SWT.Selection, event -> {
			TableItem item = (TableItem) event.item;
			// Item checked
			if (event.detail == SWT.CHECK && item.getData() instanceof CorrectionPoint) {
				CorrectionPoint p = (CorrectionPoint) item.getData();
				if (item.getChecked() && !addedCorrectionPoints.contains(p))
					addedCorrectionPoints.add(p);
				else
					addedCorrectionPoints.remove(p);

				preview();
			}
		});

		tableViewer.getTable().addListener(SWT.FocusOut, event -> preview());
		updateTable();
		soundingsControler.getView().setFocus();
	}

	/**
	 * Updates the table content.
	 */
	public void updateTable() {
		if (tableViewer != null && !tableViewer.getTable().isDisposed()) {
			tableViewer.setContentProvider(ArrayContentProvider.getInstance());
			tableViewer.setInput(correctionList.getCorrectionPointList());
			tableViewer.getTable().setRedraw(false);
			for (TableColumn tableColumn : tableViewer.getTable().getColumns()) {
				tableColumn.pack();
			}
			tableViewer.getTable().setRedraw(true);
		}
	}

	private void preview() {
		Display.getDefault().asyncExec(() -> {
			try {
				List<CorrectionPoint> points = new ArrayList<>();
				addedCorrectionPoints.stream().filter(p -> !p.isEmpty()).forEach(points::add);
				points.add(getCurrentCorrectionPoint());
				try {
					biasCorrector.executeProcess(points, getAntennaIndex());
				} catch (GIOException e) {
					Messages.openErrorMessage("Bias correction error", "Error during preview : " + e.getMessage(), e);
				}
			} catch (Exception e) {
				Messages.openErrorMessage("Error Bias editor", "Unable to update dialog", e);
			}
		});
	}

	/**
	 * Create the table editor.
	 */
	protected void createTableEditor(TableViewer tableViewer, CorrectionList correctionList) {
		// Table Editor
		final TableEditor editor = new TableEditor(tableViewer.getTable());
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;

		tableViewer.getTable().addListener(SWT.MouseDown, event -> {

			Point point = new Point(event.x, event.y);
			TableItem tableItem = tableViewer.getTable().getItem(point);

			if (tableItem != null) {
				for (int columnIndex = 0; columnIndex < tableViewer.getTable().getColumnCount(); columnIndex++) {
					Rectangle rectangle = tableItem.getBounds(columnIndex);
					if (rectangle.contains(point)) {
						if (columnIndex < REMOVE_LINE_INDEX) {

							Text text = new Text(tableViewer.getTable(), SWT.NONE);
							text.addListener(SWT.FocusOut,
									new EditorListener(tableItem, text, columnIndex, correctionList
											.getCorrectionPointList().get(tableViewer.getTable().getSelectionIndex())));
							text.addListener(SWT.Traverse,
									new EditorListener(tableItem, text, columnIndex, correctionList
											.getCorrectionPointList().get(tableViewer.getTable().getSelectionIndex())));
							editor.setEditor(text, tableItem, columnIndex);
							text.setText(tableItem.getText(columnIndex));
							text.selectAll();
							text.setFocus();
							return;
						} else {
							Image image = null;
							tableItem.setImage(REMOVE_LINE_INDEX, image);
							TableEditor tableEditor = new TableEditor(tableViewer.getTable());
							Button removeButton = new Button(tableViewer.getTable(), SWT.PUSH);
							removeButton.setToolTipText("Remove line");
							image = ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/Red_X.png");
							removeButton.setImage(image);

							tableEditor.grabHorizontal = true;
							editor.setEditor(removeButton, tableItem, columnIndex);

							removeButton.setFocus();
							removeButton.setSelection(true);
							removeButton.addListener(SWT.FOCUSED, event1 -> {
								int selectionIndex = tableViewer.getTable().getSelectionIndex();
								Iterator<CorrectionPoint> correctionPoint = correctionList.getCorrectionPointList()
										.iterator();
								int i = 0;
								while (correctionPoint.hasNext()) {
									CorrectionPoint point1 = correctionPoint.next();
									if (i == selectionIndex) {
										addedCorrectionPoints.remove(point1);
										correctionPoint.remove();
									}
									i++;
								}
								updateTable();
								preview();
								removeButton.dispose();
							});
						}
						soundingsControler.getView().setFocus();
						return;
					}
				}

			}
		});
	}

	/**
	 * Check if value is correct (long or double).
	 */
	public void checkValidityValue(TableItem tableItem, Text text, int columnIndex, CorrectionPoint correctionPoint) {
		try {
			if (columnIndex == DATE_COLUMN_INDEX) {
				DateUtils.parseDate(text.getText());
			} else {
				Double.parseDouble(text.getText());
			}
			tableItem.setText(columnIndex, text.getText());
			setCorrectionPoint(correctionPoint, tableItem);
			text.dispose();
		} catch (NumberFormatException e) {
			text.dispose();
		}
	}

	/**
	 * Set the given correction point with TableItem data *
	 *
	 * @param correctionPoint data to applied in item
	 * @param item item to modified
	 */
	protected void setCorrectionPoint(CorrectionPoint correctionPoint, TableItem item) {
		correctionPoint.setDate(DateUtils.parseDate(item.getText(DATE_COLUMN_INDEX)).toInstant()); // date
		correctionPoint.setVelocity(Double.parseDouble(item.getText(VELOCITY_COLUMN_INDEX))); // bias
		correctionPoint.setRoll(Double.parseDouble(item.getText(ROLL_COLUMN_INDEX))); // roll
		correctionPoint.setPitch(Double.parseDouble(item.getText(PITCH_COLUMN_INDEX))); // pitch
		correctionPoint.setPlatformVerticalOffset(Double.parseDouble(item.getText(VERTICAL_COLUMN_INDEX))); // heave
		correctionPoint.setHeading(Double.parseDouble(item.getText(HEADING_COLUMN_INDEX))); // heading
		correctionPoint.setMruHeading(Double.parseDouble(item.getText(MRU_HEADING_COLUMN_INDEX))); // MRU heading
		correctionPoint.setAttitudeTimeMs(Double.parseDouble(item.getText(ATTITUDE_TIME_COLUMN_INDEX))); // time
		correctionPoint.setVerticalOffsetTimeMs(Double.parseDouble(item.getText(VERTICAL_OFFSET_TIME_COLUMN_INDEX))); // time
	}

	/**
	 * Listener for the table editor feature.
	 */
	private class EditorListener implements Listener {

		private TableItem tableItem;
		private Text text;
		private int columnIndex;
		private CorrectionPoint correctionPoint;

		public EditorListener(TableItem tableItem, Text text, int columnIndex, CorrectionPoint correctionPoint) {
			super();
			this.tableItem = tableItem;
			this.text = text;
			this.columnIndex = columnIndex;
			this.correctionPoint = correctionPoint;
		}

		@Override
		public void handleEvent(final Event e) {
			switch (e.type) {
			case SWT.FocusOut:
				checkValidityValue(tableItem, text, columnIndex, correctionPoint);
				break;
			case SWT.Traverse:
				switch (e.detail) {
				case SWT.TRAVERSE_RETURN:
					checkValidityValue(tableItem, text, columnIndex, correctionPoint);
					e.doit = false;
					break;
				case SWT.TRAVERSE_ESCAPE:
					text.dispose();
					e.doit = false;
					break;
				default:
				}
				break;
			default:
			}
			soundingsControler.getView().setFocus();

		}
	}

	/**
	 * Catches data of selected tables and format them in a correction file.
	 */
	public void exportIndividualCorrectionFile() {
		HashMap<String, List<CorrectionPoint>> correctionPointPerFile = new HashMap<>();

		ICorrectionPointVisitor visitor = new ICorrectionPointVisitor() {

			@Override
			public void visit(FileCorrectionPoint p) {
				if (!correctionPointPerFile.containsKey(p.getFileName())) {
					correctionPointPerFile.put(p.getFileName(), new ArrayList<CorrectionPoint>());
				}
				correctionPointPerFile.get(p.getFileName()).add(p);
			}
		};

		for (CorrectionPoint point : correctionList.getCorrectionPointList()) {
			point.accept(visitor);
		}
		if (correctionPointPerFile.size() == 0) {
			Messages.openWarningMessage("Empty List", "No individual correction point to export");
		} else {
			DirectoryDialog dialog = new DirectoryDialog(parent.getShell(), SWT.SAVE);
			String directory = dialog.open();
			if (directory != null) {

				for (Entry<String, List<CorrectionPoint>> entry : correctionPointPerFile.entrySet()) {

					CorrectionList correctionListtoExport = new CorrectionList();
					String fileName = entry.getKey();
					for (CorrectionPoint p : entry.getValue()) {
						correctionListtoExport.addCorrectionPoint(p);
					}
					if (correctionListtoExport.getSize() > 0) {
						String outputFilePath = directory + File.separator
								+ new File(FileUtils.changeExtension(fileName, ".cor")).getName();
						try {
							// Write corrections
							if (CorrectionFileUtils.write(outputFilePath, correctionListtoExport)) {
								MessageDialog.openInformation(parent.getShell(), "Export finished",
										"Individual correction files correctly exported in : " + directory);
							}
						} catch (IOException e) {
							Messages.openErrorMessage("Correction file export", "Export failed : " + e.getMessage());
							LOGGER.error("Correction file export failed: " + e.getMessage(), e);
						}
					}
				}
			}
		}
	}

	/**
	 * Catches data of selected tables and format them in a correction file.
	 */
	public void exportCommonCorrectionFile() {
		CorrectionList correctionListtoExport = new CorrectionList();

		ICorrectionPointVisitor visitor = new ICorrectionPointVisitor() {
			@Override
			public void visit(CommonCorrectionPoint p) {
				correctionListtoExport.addCorrectionPoint(p);
			}
		};
		for (CorrectionPoint point : correctionList.getCorrectionPointList()) {
			point.accept(visitor);
		}
		if (correctionListtoExport.getSize() > 0) {
			FileDialog dialog = new FileDialog(parent.getShell(), SWT.SAVE);
			dialog.setFilterNames(new String[] { "Correction file (" + CorrectionFileUtils.EXTENSION + ")" });
			dialog.setFilterExtensions(new String[] { CorrectionFileUtils.EXTENSION });
			if (dialog.open() != null) {
				String filePath = dialog.getFilterPath() + File.separator + dialog.getFileName();
				try {
					// Write corrections
					if (CorrectionFileUtils.write(filePath, correctionListtoExport)) {
						MessageDialog.openInformation(parent.getShell(), "Export finished",
								"Common correction file correctly exported in : " + filePath);
					}
				} catch (IOException e) {
					Messages.openErrorMessage("Correction file export", "Export failed : " + e.getMessage());
					LOGGER.error("Correction file export failed: " + e.getMessage(), e);
				}
			}
		} else {
			Messages.openWarningMessage("Empty List", "No common correction point to export");
		}

	}

	/** Event handler for SOUNDING_MODEL_CHANGED */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onModelChanged(@UIEventTopic(EventTopics.SOUNDING_MODEL_CHANGED) SoundingEvent event) {
		if (event == SoundingEvent.eDataChanged && !parent.isDisposed()) {
			try {
				biasCorrector.setupProcess();
			} catch (IOException e) {
				LOGGER.error("Error with bias corrector setup : " + e.getMessage(), e);
			}
			onSelectedAreaChanged();
		}
	}

	public Rectangle getSizeView() {
		return new Rectangle(16, 16, 1100, 800);
	}

	public boolean isValidCorrection(CorrectionPoint correctionPoint) {
		boolean valid;
		if (!correctionPoint.getDate().equals(Instant.EPOCH) && (!Double.isNaN(correctionPoint.getAttitudeTimeMs())
				|| !Double.isNaN(correctionPoint.getVerticalOffsetTimeMs())
				|| !Double.isNaN(correctionPoint.getVelocity()) || !Double.isNaN(correctionPoint.getHeading())
				|| !Double.isNaN(correctionPoint.getMruHeading()) || !Double.isNaN(correctionPoint.getRoll())
				|| !Double.isNaN(correctionPoint.getPitch())
				|| !Double.isNaN(correctionPoint.getPlatformVerticalOffset())))
			valid = true;
		else
			valid = false;
		return valid;
	}

	/** Subscribe to EventBroker and reacts when a Console view is added to a PartStack */
	private void onPartStackChanged(MPartStack partStack) {
		DtmPreference preference = ContextInitializer.getInContext(ContextNames.DTM_PREFERENCES);
		preference.getContainerId(PART_ID).setValue(partStack.getElementId(), false);
		preference.save();
	}

}
