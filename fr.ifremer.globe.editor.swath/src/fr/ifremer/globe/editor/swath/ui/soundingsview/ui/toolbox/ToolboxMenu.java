package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.editor.swath.application.context.BathySelectionMode;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.BeamVisibleMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SelectionMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SoundingsSceneMode;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Menu which contains all tools of the swath editor.
 */
public class ToolboxMenu extends JPopupMenu {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = -152755826061506687L;

	private final DtmPreference dtmPreferences;
	private final ViewPoint viewPoint;
	private final BathySelectionMode bathySelectionMode;
	private final SoundingsModel soundingsModel;

	/** Object which contains all actions of {@link ToolboxViewPart} buttons. */
	private transient ToolboxActions toolboxActions;

	/** One tool selection is in progress. */
	private AtomicBoolean actionInProgress = new AtomicBoolean(false);

	@Inject
	public ToolboxMenu(@Named(ContextNames.DTM_PREFERENCES) DtmPreference dtmPreferences,
			@Named(ContextNames.SOUNDINGS_VIEW_POINT) ViewPoint viewPoint,
			@Named(ContextNames.SOUNDINGS_MODEL) SoundingsModel soundingsModel,
			@Named(ContextNames.BATHY_SELECTION_MODE) BathySelectionMode bathySelectionMode) {
		this.dtmPreferences = dtmPreferences;
		this.viewPoint = viewPoint;
		this.bathySelectionMode = bathySelectionMode;
		this.soundingsModel = soundingsModel;
	}

	/**
	 * Executed after dependency injection is done
	 */
	@PostConstruct
	void postConstruct() {
		toolboxActions = ContextInitializer.make(ToolboxActions.class);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize(Composite composite) {
		toolboxActions.initialization(actionInProgress);

		JMenu viewsMenu = new JMenu("Views");
		add(viewsMenu);

		JCheckBoxMenuItemWithData exaggerationItem = new JCheckBoxMenuItemWithData("Vertical exageration", composite);
		viewsMenu.add(exaggerationItem);
		exaggerationItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/expand.png")));
		exaggerationItem.addActionListener(e -> toolboxActions.switchExaggeration(exaggerationItem.isSelected()));
		exaggerationItem.setSelected(viewPoint.isVerticalExaggeration());

		JMenuItemWithData axisViewItem = new JMenuItemWithData("Side view", composite);
		viewsMenu.add(axisViewItem);
		axisViewItem.addActionListener(e -> toolboxActions.pushedAxisView());

		JMenuItemWithData topViewItem = new JMenuItemWithData("Top view", composite);
		viewsMenu.add(topViewItem);
		topViewItem.addActionListener(e -> toolboxActions.pushedTopView());

		JCheckBoxMenuItemWithData lockViewItem = new JCheckBoxMenuItemWithData("Lock view", composite);
		viewsMenu.add(lockViewItem);
		lockViewItem.addActionListener(e -> toolboxActions.pushedButtonLocked(lockViewItem.isSelected()));
		lockViewItem.setSelected(viewPoint.isLock());

		JMenu interractorsMenu = new JMenu("Interactors");
		add(interractorsMenu);

		// Bathy selection
		JMenu bathyItem = new JMenu("Bathy selection");
		interractorsMenu.add(bathyItem);

		JCheckBoxMenuItemWithData bathyQuadSelectionItem = new JCheckBoxMenuItemWithData("Rectangle selection",
				composite);
		bathyItem.add(bathyQuadSelectionItem);
		bathyQuadSelectionItem.setSelected(bathySelectionMode == BathySelectionMode.RECTANGLE);
		bathyQuadSelectionItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/quadSelection.png")));
		bathyQuadSelectionItem.addActionListener(
				e -> ContextInitializer.setInContext(ContextNames.BATHY_SELECTION_MODE, BathySelectionMode.RECTANGLE));

		JCheckBoxMenuItemWithData bathySwathSelectionItem = new JCheckBoxMenuItemWithData("Swath selection", composite);
		bathyItem.add(bathySwathSelectionItem);
		bathySwathSelectionItem.setSelected(bathySelectionMode == BathySelectionMode.SWATH);
		bathySwathSelectionItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/swath.png")));
		bathySwathSelectionItem.addActionListener(
				e -> ContextInitializer.setInContext(ContextNames.BATHY_SELECTION_MODE, BathySelectionMode.SWATH));

		// Mode
		JMenu modeItem = new JMenu("Mode");
		interractorsMenu.add(modeItem);

		JCheckBoxMenuItemWithData selectionSceneModeItem = new JCheckBoxMenuItemWithData("Display mode", composite);
		modeItem.add(selectionSceneModeItem);
		selectionSceneModeItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/selection.png")));
		selectionSceneModeItem.addActionListener(new SceneModeMenuItemListener());

		JCheckBoxMenuItemWithData validateSceneModeItem = new JCheckBoxMenuItemWithData("Validate mode (Enter)",
				composite);
		modeItem.add(validateSceneModeItem);
		validateSceneModeItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/on.png")));
		validateSceneModeItem.addActionListener(new SceneModeMenuItemListener());

		JCheckBoxMenuItemWithData invalidateSceneModeItem = new JCheckBoxMenuItemWithData("Invalidate Mode (Del)",
				composite);
		modeItem.add(invalidateSceneModeItem);
		invalidateSceneModeItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/off.png")));
		invalidateSceneModeItem.addActionListener(new SceneModeMenuItemListener());

		JMenu selectionMenu = new JMenu("Selector");
		interractorsMenu.add(selectionMenu);

		JCheckBoxMenuItemWithData quadSelectionModeItem = new JCheckBoxMenuItemWithData("Square selection", composite);
		selectionMenu.add(quadSelectionModeItem);
		quadSelectionModeItem
				.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/selection_rectangular.png")));
		quadSelectionModeItem.addActionListener(new SelectionModeMenuItemListener());

		JCheckBoxMenuItemWithData polygonalSelectionModeItem = new JCheckBoxMenuItemWithData("Polygon selection",
				composite);
		selectionMenu.add(polygonalSelectionModeItem);
		polygonalSelectionModeItem
				.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/selection_polygon.png")));
		polygonalSelectionModeItem.addActionListener(new SelectionModeMenuItemListener());

		JCheckBoxMenuItemWithData lassoSelectionModeItem = new JCheckBoxMenuItemWithData("Lasso selection", composite);
		selectionMenu.add(lassoSelectionModeItem);
		lassoSelectionModeItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/selection_lasso.png")));
		lassoSelectionModeItem.addActionListener(new SelectionModeMenuItemListener());

		JMenu visibilityMenu = new JMenu("Visibility");
		add(visibilityMenu);

		JCheckBoxMenuItemWithData visibilityAllItem = new JCheckBoxMenuItemWithData("Display all points", composite);
		visibilityMenu.add(visibilityAllItem);
		visibilityAllItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/visibilityAll.png")));
		visibilityAllItem.addActionListener(new VisibilityMenuItemListener());

		JCheckBoxMenuItemWithData visibilityValidItem = new JCheckBoxMenuItemWithData("Display validated points",
				composite);
		visibilityMenu.add(visibilityValidItem);
		visibilityValidItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/visibilityValid.png")));
		visibilityValidItem.addActionListener(new VisibilityMenuItemListener());

		JCheckBoxMenuItemWithData visibilityInvalidItem = new JCheckBoxMenuItemWithData("Display invalidated points",
				composite);
		visibilityMenu.add(visibilityInvalidItem);
		visibilityInvalidItem.setIcon(new ImageIcon(this.getClass().getResource("/icons/16/visibilityInvalid.png")));
		visibilityInvalidItem.addActionListener(new VisibilityMenuItemListener());

		JMenu colorModeMenu = new JMenu("Color Mode");
		add(colorModeMenu);

		JCheckBoxMenuItemWithData colorValidInvalidItem = new JCheckBoxMenuItemWithData("Validity", composite);
		colorModeMenu.add(colorValidInvalidItem);
		colorValidInvalidItem.addActionListener(new ColorValidInvalidMenuItemListener());

		JCheckBoxMenuItemWithData colorReflectivityItem = new JCheckBoxMenuItemWithData("Reflectivity", composite);
		colorModeMenu.add(colorReflectivityItem);
		colorReflectivityItem.addActionListener(new ColorValidInvalidMenuItemListener());

		JCheckBoxMenuItemWithData colorFileItem = new JCheckBoxMenuItemWithData("File", composite);
		colorModeMenu.add(colorFileItem);
		colorFileItem.addActionListener(new ColorValidInvalidMenuItemListener());

		JCheckBoxMenuItemWithData colorBeamNumberItem = new JCheckBoxMenuItemWithData("Beam", composite);
		colorModeMenu.add(colorBeamNumberItem);
		colorBeamNumberItem.addActionListener(new ColorValidInvalidMenuItemListener());

		JCheckBoxMenuItemWithData colorPingNumberItem = new JCheckBoxMenuItemWithData("Ping", composite);
		colorModeMenu.add(colorPingNumberItem);
		colorPingNumberItem.addActionListener(new ColorValidInvalidMenuItemListener());

		JCheckBoxMenuItemWithData colorTypeDetectionItem = new JCheckBoxMenuItemWithData("Detection", composite);
		colorModeMenu.add(colorTypeDetectionItem);
		colorTypeDetectionItem.addActionListener(new ColorValidInvalidMenuItemListener());

		JCheckBoxMenuItemWithData colorDepthItem = new JCheckBoxMenuItemWithData("Depth", composite);
		colorModeMenu.add(colorDepthItem);
		colorDepthItem.addActionListener(new ColorValidInvalidMenuItemListener());

		JMenu algorithmMenu = new JMenu("Tools");
		add(algorithmMenu);

		JMenuItemWithData biasCorrectionItem = new JMenuItemWithData("Bias Correction", composite);
		algorithmMenu.add(biasCorrectionItem);
		biasCorrectionItem
				.addActionListener(e -> Display.getDefault().syncExec(() -> toolboxActions.launchBiasCorrection()));

		JMenuItem filtriItem = new JMenuItemWithData("Filter by triangulation", composite);
		algorithmMenu.add(filtriItem);
		filtriItem.addActionListener(
				e -> Display.getDefault().syncExec(() -> toolboxActions.launchTriangulationFiltering()));

		JMenuItemWithData validateItem = new JMenuItemWithData("Validate", composite);
		algorithmMenu.add(validateItem);
		validateItem.addActionListener(e -> Display.getDefault().syncExec(() -> toolboxActions.launchValidating()));

		JMenuItemWithData summaryItem = new JMenuItemWithData("Beams selection summary", composite);
		algorithmMenu.add(summaryItem);
		summaryItem.addActionListener(
				e -> Display.getDefault().syncExec(() -> toolboxActions.launchBeamsSelectionSummary()));

		JMenu widgetMenu = new JMenu("Widgets display");
		add(widgetMenu);

		JCheckBoxMenuItem orientedBoundingCubeItem = new JCheckBoxMenuItem("Metrics box");
		widgetMenu.add(orientedBoundingCubeItem);
		orientedBoundingCubeItem.setSelected(dtmPreferences.getOrientedBoundingCube().getValue());
		orientedBoundingCubeItem.addActionListener(e -> toolboxActions
				.toggleOrientedBoundingCubeDisplay(((JCheckBoxMenuItem) e.getSource()).isSelected()));

		JCheckBoxMenuItem metricsItem = new JCheckBoxMenuItem("Boxes legend");
		widgetMenu.add(metricsItem);
		metricsItem.setSelected(dtmPreferences.getMetricsText().getValue());
		metricsItem.addActionListener(
				e -> toolboxActions.toggleMetricsDisplay(((JCheckBoxMenuItem) e.getSource()).isSelected()));

		JCheckBoxMenuItem soundingsItem = new JCheckBoxMenuItem("Soundings");
		widgetMenu.add(soundingsItem);
		soundingsItem.setSelected(dtmPreferences.getSoundings().getValue());
		soundingsItem.addActionListener(
				e -> toolboxActions.toggleSoundingsDisplay(((JCheckBoxMenuItem) e.getSource()).isSelected()));

		JCheckBoxMenuItem landmarkItem = new JCheckBoxMenuItem("Landmark");
		widgetMenu.add(landmarkItem);
		landmarkItem.setSelected(dtmPreferences.getLandmark().getValue());
		landmarkItem.addActionListener(
				e -> toolboxActions.toggleLandmarkDisplay(((JCheckBoxMenuItem) e.getSource()).isSelected()));

		JCheckBoxMenuItem statisticItem = new JCheckBoxMenuItem("Statistics");
		widgetMenu.add(statisticItem);
		statisticItem.setSelected(dtmPreferences.getStatistics().getValue());
		statisticItem.addActionListener(
				e -> toolboxActions.toggleStatisticsDisplay(((JCheckBoxMenuItem) e.getSource()).isSelected()));

		setInitialSceneState(selectionSceneModeItem, validateSceneModeItem, invalidateSceneModeItem);
		setInitialSelectionMode(quadSelectionModeItem, polygonalSelectionModeItem, lassoSelectionModeItem);
		setInitialVisibilityState(visibilityAllItem, visibilityValidItem, visibilityInvalidItem);
		setInitialColorBeamState(colorValidInvalidItem, colorReflectivityItem, colorFileItem, colorBeamNumberItem,
				colorPingNumberItem, colorTypeDetectionItem, colorDepthItem);
	}

	private class SceneModeMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JCheckBoxMenuItemWithData jCheckBoxMenuItemWithData = (JCheckBoxMenuItemWithData) e.getSource();
			if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
				toogleButtons(jCheckBoxMenuItemWithData);
				soundingsModel.getDisplayParameters().setSceneMode((SoundingsSceneMode) jCheckBoxMenuItemWithData
						.getData(SoundingsSceneMode.class.getSimpleName()));
				toolboxActions.actionInProgress.set(false);
			} else if (e.getSource() instanceof JMenuItemWithData jMenuItemWithData) {
				if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
					toogleButtons(jCheckBoxMenuItemWithData);
					soundingsModel.getDisplayParameters().setSceneMode(
							(SoundingsSceneMode) jMenuItemWithData.getData(SoundingsSceneMode.class.getSimpleName()));
					toolboxActions.actionInProgress.set(false);
				}
			}
		}
	}

	private class SelectionModeMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JCheckBoxMenuItemWithData jCheckBoxMenuItemWithData) {
				if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
					toogleButtons(jCheckBoxMenuItemWithData);
					soundingsModel.getDisplayParameters().setSelectionMode(
							(SelectionMode) jCheckBoxMenuItemWithData.getData(SelectionMode.class.getSimpleName()));
					toolboxActions.actionInProgress.set(false);
				}
			} else if (e.getSource() instanceof JMenuItemWithData jCheckBoxMenuItemWithData) {
				if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
					toogleButtons(jCheckBoxMenuItemWithData);
					soundingsModel.getDisplayParameters().setSelectionMode(
							(SelectionMode) jCheckBoxMenuItemWithData.getData(SelectionMode.class.getSimpleName()));
					toolboxActions.actionInProgress.set(false);
				}
			}
		}
	}

	private class VisibilityMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JCheckBoxMenuItemWithData jCheckBoxMenuItemWithData) {
				if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
					toogleButtons(jCheckBoxMenuItemWithData);
					soundingsModel.getDisplayParameters().setVisibleMode(
							(BeamVisibleMode) jCheckBoxMenuItemWithData.getData(BeamVisibleMode.class.getSimpleName()));
					toolboxActions.actionInProgress.set(false);
				}
			} else if (e.getSource() instanceof JMenuItemWithData jCheckBoxMenuItemWithData) {
				if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
					toogleButtons(jCheckBoxMenuItemWithData);
					soundingsModel.getDisplayParameters().setVisibleMode(
							(BeamVisibleMode) jCheckBoxMenuItemWithData.getData(BeamVisibleMode.class.getSimpleName()));
					toolboxActions.actionInProgress.set(false);
				}
			}
		}
	}

	private class ColorValidInvalidMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JCheckBoxMenuItemWithData jCheckBoxMenuItemWithData) {
				if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
					toogleButtons(jCheckBoxMenuItemWithData);
					soundingsModel.getDisplayParameters().setDisplayMode(
							(BeamDisplayMode) jCheckBoxMenuItemWithData.getData(BeamDisplayMode.class.getSimpleName()));
					toolboxActions.actionInProgress.set(false);
				}
			} else if (e.getSource() instanceof JMenuItemWithData jCheckBoxMenuItemWithData) {
				if (toolboxActions.actionInProgress.compareAndSet(false, true)) {
					toogleButtons(jCheckBoxMenuItemWithData);
					soundingsModel.getDisplayParameters().setDisplayMode(
							(BeamDisplayMode) jCheckBoxMenuItemWithData.getData(BeamDisplayMode.class.getSimpleName()));
					toolboxActions.actionInProgress.set(false);
				}
			}
		}
	}

	/**
	 * Set initial values of scene menu item.
	 */
	private void setInitialSceneState(JCheckBoxMenuItemWithData selectionSceneModeItem,
			JCheckBoxMenuItemWithData validateSceneModeItem, JCheckBoxMenuItemWithData invalidateSceneModeItem) {
		setSceneMenuItemState(selectionSceneModeItem, validateSceneModeItem, invalidateSceneModeItem);
		selectionSceneModeItem.setData(SoundingsSceneMode.class.getSimpleName(), SoundingsSceneMode.SELECT);
		validateSceneModeItem.setData(SoundingsSceneMode.class.getSimpleName(), SoundingsSceneMode.VALIDATE);
		invalidateSceneModeItem.setData(SoundingsSceneMode.class.getSimpleName(), SoundingsSceneMode.INVALIDATE);
	}

	/**
	 * Set initial status of scene menu item.
	 */
	private void setSceneMenuItemState(JCheckBoxMenuItemWithData selectionSceneModeItem,
			JCheckBoxMenuItemWithData validateSceneModeItem, JCheckBoxMenuItemWithData invalidateSceneModeItem) {
		SoundingsSceneMode soundingsSceneMode = (SoundingsSceneMode) dtmPreferences.getSoundingSceneMode().getValue();
		selectionSceneModeItem.setSelected(soundingsSceneMode == SoundingsSceneMode.SELECT);
		validateSceneModeItem.setSelected(soundingsSceneMode == SoundingsSceneMode.VALIDATE);
		invalidateSceneModeItem.setSelected(soundingsSceneMode == SoundingsSceneMode.INVALIDATE);
	}

	/**
	 * Set initial values of selection menu item.
	 */
	private void setInitialSelectionMode(JCheckBoxMenuItemWithData quadSelectionModeItem,
			JCheckBoxMenuItemWithData polygonSelectionModeItem, JCheckBoxMenuItemWithData lassoSelectionModeItem) {
		setSelectedMenuItemMode(quadSelectionModeItem, polygonSelectionModeItem, lassoSelectionModeItem);
		quadSelectionModeItem.setData(SelectionMode.class.getSimpleName(), SelectionMode.QUAD);
		polygonSelectionModeItem.setData(SelectionMode.class.getSimpleName(), SelectionMode.POLYGON);
		lassoSelectionModeItem.setData(SelectionMode.class.getSimpleName(), SelectionMode.LASSO);
	}

	/**
	 * Set initial status of selection menu item.
	 */
	private void setSelectedMenuItemMode(JCheckBoxMenuItemWithData quadSelectionModeItem,
			JCheckBoxMenuItemWithData polygonSelectionModeItem, JCheckBoxMenuItemWithData lassoSelectionModeItem) {
		Enum<?> selectionMode = dtmPreferences.getSelectionMode().getValue();
		quadSelectionModeItem.setSelected(selectionMode == SelectionMode.QUAD);
		polygonSelectionModeItem.setSelected(selectionMode == SelectionMode.POLYGON);
		lassoSelectionModeItem.setSelected(selectionMode == SelectionMode.LASSO);
	}

	/**
	 * Set initial values of visibility menu item.
	 */
	private void setInitialVisibilityState(JCheckBoxMenuItemWithData visibilityAllItem,
			JCheckBoxMenuItemWithData visibilityValidItem, JCheckBoxMenuItemWithData visibilityInvalidItem) {
		setVisibilityMenuItemState(visibilityAllItem, visibilityValidItem, visibilityInvalidItem);
		visibilityAllItem.setData(BeamVisibleMode.class.getSimpleName(), BeamVisibleMode.ALL);
		visibilityValidItem.setData(BeamVisibleMode.class.getSimpleName(), BeamVisibleMode.VALID_ONLY);
		visibilityInvalidItem.setData(BeamVisibleMode.class.getSimpleName(), BeamVisibleMode.INVALID_ONLY);
	}

	/**
	 * Set initial status of visibility menu item.
	 */
	private void setVisibilityMenuItemState(JCheckBoxMenuItemWithData visibilityAllItem,
			JCheckBoxMenuItemWithData visibilityValidItem, JCheckBoxMenuItemWithData visibilityInvalidItem) {
		Enum<?> beamVisibleMode = dtmPreferences.getBeamVisibleMode().getValue();
		visibilityAllItem.setSelected(beamVisibleMode == BeamVisibleMode.ALL);
		visibilityValidItem.setSelected(beamVisibleMode == BeamVisibleMode.VALID_ONLY);
		visibilityInvalidItem.setSelected(beamVisibleMode == BeamVisibleMode.INVALID_ONLY);
	}

	/**
	 * Set initial values of color menu item.
	 */
	private void setInitialColorBeamState(JCheckBoxMenuItemWithData colorValidInvalidItem,
			JCheckBoxMenuItemWithData colorReflectivityItem, JCheckBoxMenuItemWithData colorFileItem,
			JCheckBoxMenuItemWithData colorBeamNumberItem, JCheckBoxMenuItemWithData colorPingNumberItem,
			JCheckBoxMenuItemWithData colorTypeDetectionItem, JCheckBoxMenuItemWithData colorDepthItem) {
		setColorBeamMenuItemState(colorValidInvalidItem, colorReflectivityItem, colorFileItem, colorBeamNumberItem,
				colorPingNumberItem, colorTypeDetectionItem, colorDepthItem);
		colorValidInvalidItem.setData(BeamDisplayMode.class.getSimpleName(), BeamDisplayMode.VALID_INVALID);
		colorReflectivityItem.setData(BeamDisplayMode.class.getSimpleName(), BeamDisplayMode.REFLECTIVITY);
		colorFileItem.setData(BeamDisplayMode.class.getSimpleName(), BeamDisplayMode.FILE);
		colorBeamNumberItem.setData(BeamDisplayMode.class.getSimpleName(), BeamDisplayMode.BEAM_NUMBER);
		colorPingNumberItem.setData(BeamDisplayMode.class.getSimpleName(), BeamDisplayMode.CYCLE_NUMBER);
		colorTypeDetectionItem.setData(BeamDisplayMode.class.getSimpleName(), BeamDisplayMode.DETECTION_TYPE);
		colorDepthItem.setData(BeamDisplayMode.class.getSimpleName(), BeamDisplayMode.DEPTH);
	}

	/**
	 * Set intial values of color menu item.
	 */
	private void setColorBeamMenuItemState(JCheckBoxMenuItemWithData colorValidInvalidItem,
			JCheckBoxMenuItemWithData colorReflectivityItem, JCheckBoxMenuItemWithData colorFileItem,
			JCheckBoxMenuItemWithData colorBeamNumberItem, JCheckBoxMenuItemWithData colorPingNumberItem,
			JCheckBoxMenuItemWithData colorTypeDetectionItem, JCheckBoxMenuItemWithData colorDepthItem) {
		BeamDisplayMode beamDisplayMode = (BeamDisplayMode) dtmPreferences.getBeamDisplayMode().getValue();
		colorValidInvalidItem.setSelected(beamDisplayMode == BeamDisplayMode.VALID_INVALID);
		colorReflectivityItem.setSelected(beamDisplayMode == BeamDisplayMode.REFLECTIVITY);
		colorFileItem.setSelected(beamDisplayMode == BeamDisplayMode.FILE);
		colorBeamNumberItem.setSelected(beamDisplayMode == BeamDisplayMode.BEAM_NUMBER);
		colorPingNumberItem.setSelected(beamDisplayMode == BeamDisplayMode.CYCLE_NUMBER);
		colorTypeDetectionItem.setSelected(beamDisplayMode == BeamDisplayMode.DETECTION_TYPE);
		colorDepthItem.setSelected(beamDisplayMode == BeamDisplayMode.DEPTH);
	}

	/**
	 * Toogle buttons on a selection.
	 */
	private void toogleButtons(JMenuItem selectedJMenuItem) {
		Component[] jMenuItems = selectedJMenuItem.getParent().getComponents();
		for (Component menuItem : jMenuItems)
			if (menuItem instanceof JMenuItem jMenuItem)
				jMenuItem.setSelected(menuItem == selectedJMenuItem);
	}

}
