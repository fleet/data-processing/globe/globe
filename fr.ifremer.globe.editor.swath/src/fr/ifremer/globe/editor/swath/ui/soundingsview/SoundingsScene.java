﻿package fr.ifremer.globe.editor.swath.ui.soundingsview;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicBoolean;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import javax.swing.SwingUtilities;

import org.eclipse.e4.core.di.annotations.Optional;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;

import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.BeamsPolygonSelectionComponent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.BoundingSphere;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.GraphicComponent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.LandmarkComponent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.OrientedBoundingCube;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.SoundingsComponent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.StatisticsComponents;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SelectionMode;
import fr.ifremer.globe.ogl.core.matrices.Matrix4D;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearBuffers;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearState;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.jogl.GraphicsWindowBase;
import fr.ifremer.globe.ogl.renderer.jogl.GraphicsWindowGL2x;
import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.scene.Camera;
import fr.ifremer.globe.ogl.renderer.scene.CameraVisitor;
import fr.ifremer.globe.ogl.renderer.scene.DefaultCameraCreator;
import fr.ifremer.globe.ogl.renderer.scene.OrthographicCamera;
import fr.ifremer.globe.ogl.renderer.scene.PerspectiveCamera;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener;
import fr.ifremer.globe.ogl.scene.CameraBehaviour;
import fr.ifremer.globe.ogl.scene.OrthographicCameraPan;
import fr.ifremer.globe.ogl.scene.PerspectiveCameraPan;
import fr.ifremer.globe.ogl.util.ViewProjectionType;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

public class SoundingsScene implements GraphicsWindowListener {

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;

	/** ViewPoint */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_VIEW_POINT)
	protected ViewPoint viewPoint;

	/** Not inject directly, see {@link #setController} */
	protected SoundingsController controller;

	protected GraphicsWindowBase window;
	protected CameraBehaviour cameraBehavior;
	protected Camera camera;
	protected ContextGL2x context;
	protected SceneState sceneState;
	protected ClearState clearState;
	/** Dirsplayed component */
	protected ArrayList<GraphicComponent> graphicComponents = new ArrayList<>();
	protected LandmarkComponent landmark;
	protected OrientedBoundingCube oboundingCube;
	protected SoundingsComponent soundings;
	/** Polygon selection for beams */
	protected BeamsPolygonSelectionComponent beamsPolygonSelectionComponent;
	protected StatisticsComponents stats;

	/** Observer of preference changes. */
	protected Observer preferencesObserver;
	/** True when preferences have changed. */
	protected AtomicBoolean preferencesHasChanged = new AtomicBoolean(true);

	/**
	 * Constructor.
	 */
	public SoundingsScene() {
		super();
	}

	/**
	 * Constructor.
	 */
	public void initialize(GraphicsWindowGL2x window) {
		this.window = window;
		window.addGraphicsWindowListener(this);
		window.addMouseListener(controller.getKeyAndMouseListener());
		window.addKeyListener(controller.getKeyAndMouseListener());
		window.addMouseMotionListener(controller.getKeyAndMouseListener());

		preferencesObserver = (Observable o, Object arg) -> preferencesHasChanged.set(true);
		dtmPreferences.getBackgroundColor().addObserver(preferencesObserver);
	}

	@Override
	public void init(GLAutoDrawable drawable, GraphicsWindow window) {
		Device2x.initialize();
		context = new ContextGL2x(window.getComponent());
		initializeScene(drawable);

	}

	public void initializeScene(GLAutoDrawable drawable) {

		sceneState = new SceneState(DefaultCameraCreator.create(ViewProjectionType.ORTHOGRAPHIC));

		controller.setCamera(sceneState.getCamera());
		createLightnings();
		updateCameraFromViewPoint();

		createComponents();
		for (GraphicComponent c : graphicComponents) {
			c.init(drawable, context);
		}
		clearState = new ClearState();
		clearState.getBuffers().add(ClearBuffers.DepthBuffer);
		clearState.getBuffers().add(ClearBuffers.StencilBuffer);
		clearState.setColor(new Color(255, 255, 255, 255));
		clearState.getBuffers().add(ClearBuffers.ColorBuffer);

	}

	public void updateCameraFromViewPoint() {
		if (cameraBehavior != null) {
			cameraBehavior.dispose();
		}

		camera = sceneState.getCamera();
		camera.visit(new CameraConfigurator());
		camera.visit(new CameraPanFactory());

	}

	protected void createLightnings() {
		sceneState.setDiffuseIntensity(0.90f);
		sceneState.setSpecularIntensity(0.05f);
		sceneState.setAmbientIntensity(0.05f);
		sceneState.setSunPosition(new Vector3D(200000, 300000, 200000));
	}

	/**
	 * Follow the link.
	 *
	 * @param drawable
	 *
	 * @see fr.ifremer.globe.editor.soundingsviewer.SoundingSceneBase#dispose()
	 */
	public void dispose(Context context) {
		dtmPreferences.deleteObserver(preferencesObserver);

		window.removeGraphicsWindowListener(this);

		for (GraphicComponent c : graphicComponents) {
			c.dispose(context);
		}
	}

	public void displayChanged(boolean updateCamera) {
		soundings.initializeDrawStates();
		if (updateCamera) {
			updateCameraFromViewPoint();
		}

		refresh();
	}

	public void viewPointChanged() {
		updateCameraFromViewPoint();
		refresh();
	}

	public void refresh() {
		SwingUtilities.invokeLater(() -> window.getComponent().repaint());
	}

	public SelectionMode getSelectionMode() {
		return SelectionMode.POLYGON;
	}

	protected void createComponents() {

		oboundingCube = new OrientedBoundingCube(getCamera());
		graphicComponents.add(oboundingCube);
		if (!dtmPreferences.getOrientedBoundingCube().getValue()) {
			oboundingCube.setVisible(false);
		}

		BoundingSphere boundingSphere = new BoundingSphere();
		graphicComponents.add(boundingSphere);
		boundingSphere.setVisible(false);

		soundings = new SoundingsComponent();
		graphicComponents.add(soundings);
		if (!dtmPreferences.getSoundings().getValue()) {
			soundings.setVisible(false);
		}

		beamsPolygonSelectionComponent = new BeamsPolygonSelectionComponent(this::reloadAndRefresh);
		graphicComponents.add(beamsPolygonSelectionComponent);
		beamsPolygonSelectionComponent.setVisible(true);

		/**
		 * landmark destroy all matrix, so be sure to add it at the end
		 */
		landmark = new LandmarkComponent();
		graphicComponents.add(landmark);
		if (!dtmPreferences.getLandmark().getValue()) {
			landmark.setVisible(false);
		}

		stats = new StatisticsComponents();
		graphicComponents.add(stats);
		if (dtmPreferences.getStatistics().getValue()) {
			stats.setVisible(false);
		}
	}

	/** Force to reload all buffers and the require a repaint of the view. */
	protected void reloadAndRefresh() {
		SwingUtilities.invokeLater(() -> {
			// Reload all OpenGl Attributes
			soundings.refreshBuffers();
			// Repaint
			window.getComponent().repaint();
		});
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.soundingsviewer.SoundingSceneBase#render(com.jogamp.opengl.GLAutoDrawable,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void render(GLAutoDrawable drawable, GraphicsWindow windows) {
		if (preferencesHasChanged.compareAndSet(true, false)) {
			getClearState().setColor(ColorUtils.convertGColorToAWT(dtmPreferences.getBackgroundColor().getValue()));
		}

		Context context = window.getContext();
		GL2 gl = drawable.getGL().getGL2();

		context.clear(clearState);

		ViewPoint vp = viewPoint;
		if (vp != null) {
			// Reset the projection matrix
			gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
			gl.glLoadIdentity();
			Matrix4D matrix = sceneState.getProjectionMatrix();
			double[] values = matrix.getReadOnlyColumnMajorValues().clone();

			gl.glLoadMatrixd(values, 0);

			// Reset the modelview matrix
			gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
			gl.glLoadIdentity();
			matrix = sceneState.getModelViewMatrix();
			values = matrix.getReadOnlyColumnMajorValues().clone();
			gl.glLoadMatrixd(values, 0);

			gl.glScaled(vp.getScalingFactor().getX(), vp.getScalingFactor().getY(), vp.getScalingFactor().getZ());

			renderComponents(gl, context, sceneState);
		}
	}

	void renderComponents(GL2 gl, Context context, SceneState sceneState) {
		for (GraphicComponent c : graphicComponents) {
			if (c.isVisible()) {

				// we reset the context to its default values, since some
				// component might have call gl function or change it out of the
				// scene renderer
				context.forceApplyRenderState(new RenderState());
				c.draw(gl, context, sceneState);
			}
		}
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height, GraphicsWindow window) {
		// update the viewport
		// change the aspect ratio
		window.getContext().setViewport(new Rectangle(0, 0, window.getWidth(), window.getHeight()));
		if (sceneState != null) {
			sceneState.getCamera().reshape(window.getWidth(), window.getHeight());
		}

	}

	/**
	 * Set the camera's parameters.
	 */
	class CameraConfigurator implements CameraVisitor {

		/**
		 * Follow the link.
		 *
		 * @see fr.ifremer.globe.ogl.renderer.scene.CameraVisitor#accept(fr.ifremer.globe.ogl.renderer.scene.OrthographicCamera)
		 */
		@Override
		public void accept(OrthographicCamera camera) {
			double range = viewPoint.getRangeXYZ() * 1.3;
			camera.setOrthographics(-range / 2., range / 2., -range / 2., range / 2.);
			camera.setOrthographicNearPlaneDistance(0.01);
			camera.setOrthographicFarPlaneDistance(range * 10);

		}

		/**
		 * Follow the link.
		 *
		 * @see fr.ifremer.globe.ogl.renderer.scene.CameraVisitor#accept(fr.ifremer.globe.ogl.renderer.scene.PerspectiveCamera)
		 */
		@Override
		public void accept(PerspectiveCamera camera) {
			camera.setFieldOfViewY(Math.PI / 10.0);
			camera.setPerspectiveNearPlaneDistance(0.00001); // 0.000001
			camera.setPerspectiveFarPlaneDistance(10000.0);
		}

	}

	/**
	 * Make a CameraBehaviour and initialize the attribute SoundingSceneBase.this._cameraBehavior.
	 */
	class CameraPanFactory implements CameraVisitor {

		/**
		 * Follow the link.
		 *
		 * @see fr.ifremer.globe.ogl.renderer.scene.CameraVisitor#accept(fr.ifremer.globe.ogl.renderer.scene.OrthographicCamera)
		 */
		@Override
		public void accept(OrthographicCamera camera) {
			double maxDisplacement = viewPoint.getRangeXYZ() * 1.3;
			cameraBehavior = new OrthographicCameraPan(camera, window, maxDisplacement);
		}

		/**
		 * Follow the link.
		 *
		 * @see fr.ifremer.globe.ogl.renderer.scene.CameraVisitor#accept(fr.ifremer.globe.ogl.renderer.scene.PerspectiveCamera)
		 */
		@Override
		public void accept(PerspectiveCamera camera) {
			cameraBehavior = new PerspectiveCameraPan(camera, window, 20);
		}

	}

	/**
	 * @return the {@link #beamsPolygonSelectionComponent}
	 */
	public BeamsPolygonSelectionComponent getBeamsPolygonSelectionComponent() {
		return beamsPolygonSelectionComponent;
	}

	/**
	 * @return the landmark
	 */
	public LandmarkComponent getLandmark() {
		return landmark;
	}

	/**
	 * @return the soundings
	 */
	public SoundingsComponent getSoundings() {
		return soundings;
	}

	/**
	 * @return the stats
	 */
	public StatisticsComponents getStats() {
		return stats;
	}

	/**
	 * @return the oboundingCube
	 */
	public OrientedBoundingCube getOboundingCube() {
		return oboundingCube;
	}

	/**
	 * Getter of camera
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * Getter of clearState
	 */
	public ClearState getClearState() {
		return clearState;
	}

	/**
	 * Getter of cameraBehavior
	 */
	public CameraBehaviour getCameraBehavior() {
		return cameraBehavior;
	}

	/** Setter of {@link #controller} */
	@Inject
	@Optional
	void setController(@Named(ContextNames.SOUNDINGS_CONTROLLER) SoundingsController controller) {
		if (controller == null && this.controller != null) {
			window.removeMouseMotionListener(this.controller.getKeyAndMouseListener());
			window.removeMouseListener(this.controller.getKeyAndMouseListener());
			window.removeKeyListener(this.controller.getKeyAndMouseListener());
		}
		this.controller = controller;
	}

}
