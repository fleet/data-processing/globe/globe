/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.GetValueOperation;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.ToStringOperation;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Contains all data needed to fill a line of the {@link SummaryView#tableData}.
 */
public class SummaryItem {
	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(SummaryItem.class);

	/** Attributes */
	protected SounderDataContainer dataContainer;
	protected Mappings mappings;
	protected long offsetInFile;
	protected double longitude;
	protected double latitude;

	/** Constructor */
	public SummaryItem(SounderDataContainer dataContainer, Mappings mappings, long offsetInFile) {
		this.dataContainer = dataContainer;
		this.mappings = mappings;
		this.offsetInFile = offsetInFile;
	}

	/** Getter of {@link #data.cycleIndex} */
	public int getCycleIndex() {
		return mappings.getCycleIndex(offsetInFile);
	}

	/** Getter of {@link #beamIndex} */
	public int getBeamIndex() {
		return mappings.getBeamIndex(offsetInFile);
	}

	/** Getter of {@link #x} */
	public float getX() {
		return mappings.getX(offsetInFile);
	}

	/** Getter of {@link #y} */
	public float getY() {
		return mappings.getY(offsetInFile);
	}

	/** Getter of {@link #depth} */
	public float getDepth() {
		return mappings.getDepth(offsetInFile);
	}

	/** Getter of {@link #reflectivity} */
	public float getReflectivity() {
		return mappings.getReflectivity(offsetInFile);
	}

	/** Getter of {@link #detection} */
	public byte getDetection() {
		return mappings.getDetection(offsetInFile);
	}

	/** Getter of {@link #validity} */
	public MappingValidity getValidity() {
		return mappings.getValidity(offsetInFile);
	}

	/** Getter of {@link #longitude} */
	public double getLongitude() {
		return longitude;
	}

	/** Setter of {@link #longitude} */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/** Getter of {@link #latitude} */
	public double getLatitude() {
		return latitude;
	}

	/** Setter of {@link #latitude} */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/** Getter of the file name */
	public String getFileName() {
		return FilenameUtils.getBaseName(dataContainer.getInfo().getPath());
	}

	/** Getter of {@link #dataContainer} */
	public SounderDataContainer getDataContainer() {
		return dataContainer;
	}

	/** Return the value for the specified layer */
	public Object getValue(GetValueOperation getValueOperation, PredefinedLayers<?> predefinedLayer) {
		Object result = null;

		try {
			int[] indexes = new int[] { mappings.getCycleIndex(offsetInFile), mappings.getBeamIndex(offsetInFile) };
			ILoadableLayer<?> layer = dataContainer.getLayer(predefinedLayer);
			getValueOperation.setIndexes(indexes);
			layer.process(getValueOperation);
			result = getValueOperation.getValue();
		} catch (GIOException e) {
			logger.warn("Unexpected error while converting a layer's value to Object ", e);
		}
		return result;
	}

	/** Return the value as String for the specified layer */
	public String getStringValue(ToStringOperation toStringOperation, PredefinedLayers<?> predefinedLayer) {
		String result = "";
		try {
			int[] indexes = new int[] { mappings.getCycleIndex(offsetInFile), mappings.getBeamIndex(offsetInFile) };
			ILoadableLayer<?> layer = dataContainer.getLayer(predefinedLayer);
			toStringOperation.setIndexes(indexes);
			layer.process(toStringOperation);
			result = toStringOperation.getResult();
		} catch (GIOException e) {
			logger.warn("Unexpected error while converting a layer's value to String ", e);
		}
		return result;
	}
}
