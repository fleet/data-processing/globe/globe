package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

/**
 * Compute values boundings for a set of soundings use case is {@link #begin()}
 * then push the values {@link #push(int, int, float, float, float, float)} then
 * {@link #end()} <br>
 * <br>
 * All Attributes are volatile otherwise some threaded consumers of them (ie
 * Legend in souding view) may not see the values up to date.
 * */
public class ValuesBoundings {
	/** Min cycle index */
	protected volatile int minCycleIndex = Integer.MIN_VALUE;
	/** Max cycle index */
	protected volatile int maxCycleIndex = Integer.MAX_VALUE;
	/** Min cycle index */
	protected volatile int minBeamIndex = Integer.MIN_VALUE;
	/** Max cycle index */
	protected volatile int maxBeamIndex = Integer.MAX_VALUE;
	/** Min cycle index */
	protected volatile float minReflectivity = Float.NEGATIVE_INFINITY;
	/** Max cycle index */
	protected volatile float maxReflectivity = Float.POSITIVE_INFINITY;
	/** Min Depth */
	protected volatile float minDepth = Float.NEGATIVE_INFINITY;
	/** Max Depth */
	protected volatile float maxDepth = Float.POSITIVE_INFINITY;
	/** Min Scalar Quality Factor */
	protected volatile float minQualityFactor = Float.NEGATIVE_INFINITY;
	/** Max Scalar Quality Factor */
	protected volatile float maxQualityFactor = Float.POSITIVE_INFINITY;

	/**
	 * start a computation 
	 * */
	public void begin() {
		this.minCycleIndex = Integer.MAX_VALUE;
		this.maxCycleIndex = Integer.MIN_VALUE;
		this.minBeamIndex = Integer.MAX_VALUE;
		this.maxBeamIndex = Integer.MIN_VALUE;
		this.minReflectivity = Float.POSITIVE_INFINITY;
		this.maxReflectivity = Float.NEGATIVE_INFINITY;
		this.minDepth = Float.POSITIVE_INFINITY;
		this.maxDepth = Float.NEGATIVE_INFINITY;
		this.minQualityFactor = Float.POSITIVE_INFINITY;
		this.maxQualityFactor = Float.NEGATIVE_INFINITY;
	}
	
	/**
	 * end 
	 * */
	public void end()
	{
	}
	
	
	public void push(int cycleIndex, int beamIndex, float x, float y, float depth, float reflectivity, float qualityFactor) {
		this.minCycleIndex = Math.min(this.minCycleIndex, cycleIndex);
		this.maxCycleIndex = Math.max(this.maxCycleIndex, cycleIndex);
		this.minBeamIndex = Math.min(this.minBeamIndex, beamIndex);
		this.maxBeamIndex = Math.max(this.maxBeamIndex, beamIndex);
		if (Float.isFinite(reflectivity)) {
			this.minReflectivity = Math.min(this.minReflectivity, reflectivity);
			this.maxReflectivity = Math.max(this.maxReflectivity, reflectivity);
		}
		this.minDepth = Math.min(this.minDepth, depth);
		this.maxDepth = Math.max(this.maxDepth, depth);
		this.minQualityFactor = Math.min(this.minQualityFactor, qualityFactor);
		this.maxQualityFactor = Math.max(this.maxQualityFactor, qualityFactor);
	}

	/**
	 * @return the {@link #minCycleIndex}
	 */
	public int getMinCycleIndex() {
		return this.minCycleIndex;
	}

	/**
	 * @param minCycleIndex
	 *            the {@link #minCycleIndex} to set
	 */
	public void setMinCycleIndex(int minCycleIndex) {
		this.minCycleIndex = minCycleIndex;
	}

	/**
	 * @return the {@link #maxCycleIndex}
	 */
	public int getMaxCycleIndex() {
		return this.maxCycleIndex;
	}

	/**
	 * @param maxCycleIndex
	 *            the {@link #maxCycleIndex} to set
	 */
	public void setMaxCycleIndex(int maxCycleIndex) {
		this.maxCycleIndex = maxCycleIndex;
	}

	/**
	 * @return the {@link #minBeamIndex}
	 */
	public int getMinBeamIndex() {
		return this.minBeamIndex;
	}

	/**
	 * @param minBeamIndex
	 *            the {@link #minBeamIndex} to set
	 */
	public void setMinBeamIndex(int minBeamIndex) {
		this.minBeamIndex = minBeamIndex;
	}

	/**
	 * @return the {@link #maxBeamIndex}
	 */
	public int getMaxBeamIndex() {
		return this.maxBeamIndex;
	}

	/**
	 * @param maxBeamIndex
	 *            the {@link #maxBeamIndex} to set
	 */
	public void setMaxBeamIndex(int maxBeamIndex) {
		this.maxBeamIndex = maxBeamIndex;
	}

	/**
	 * @return the {@link #minReflectivity}
	 */
	public float getMinReflectivity() {
		return this.minReflectivity;
	}

	/**
	 * @return the {@link #maxReflectivity}
	 */
	public float getMaxReflectivity() {
		return this.maxReflectivity;
	}

	/**
	 * @return the {@link #minDepth}
	 */
	public float getMinDepth() {
		return this.minDepth;
	}

	/**
	 * @return the {@link #maxDepth}
	 */
	public float getMaxDepth() {
		return this.maxDepth;
	}

	/**
	 * @return the {@link #minQualityFactor}
	 */
	public float getMinQualityFactor() {
		return this.minQualityFactor;
	}

	/**
	 * @return the {@link #maxQualityFactor}
	 */
	public float getMaxQualityFactor() {
		return this.maxQualityFactor;
	}

}
