/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.Buffer;
import java.util.function.Supplier;

import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;

/**
 * Instance able to refresh the source buffer of a VertexBuffer.
 */
public class BufferRefresher {
	/** VertexBuffer to refresh. */
	protected VertexBuffer vertexBuffer;
	/** Supplier of ByteBuffer. */
	protected Supplier<Buffer> byteBufferSupplier;
	
	/**
	 * Constructor.
	 */
	public BufferRefresher(VertexBuffer vertexBuffer, Supplier<Buffer> byteBufferSupplier) {
		this.vertexBuffer = vertexBuffer;
		this.byteBufferSupplier = byteBufferSupplier;
	}

	/** Perform the refresh. */
	public void refresh() {
		this.vertexBuffer.refresh(this.byteBufferSupplier.get());
	}
}
