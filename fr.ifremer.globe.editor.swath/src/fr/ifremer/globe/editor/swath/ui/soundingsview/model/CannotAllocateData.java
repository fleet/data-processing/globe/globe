package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import fr.ifremer.globe.utils.exception.GException;

public class CannotAllocateData extends GException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * @see GException#GException(String)
	 * */
	public CannotAllocateData(String message) {
		super(message);
	}
	/**
	 * @see GException#GException(String, Throwable)
	 * */
	public CannotAllocateData(String message, Throwable cause) {
		super(message, cause);
	}
	

}
