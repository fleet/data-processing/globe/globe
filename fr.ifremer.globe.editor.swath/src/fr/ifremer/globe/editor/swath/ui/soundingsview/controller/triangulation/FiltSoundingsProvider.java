/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.triangulation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.core.processes.filtri.model.IFilTriSoundingsProxy;
import fr.ifremer.globe.core.processes.filtri.model.IFiltSoundingsProvider;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.BufferAssociation;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;

/**
 * Provide an iterator of IFilTriSoundings to walk thru the selected beams in the Sounding View.
 */
public class FiltSoundingsProvider implements IFiltSoundingsProvider {

	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	/**
	 * Constructor.
	 */
	public FiltSoundingsProvider() {
		super();
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.process.filtri.model.IFiltSoundingsProvider#getSoundingsIterator(int)
	 */
	@Override
	public Iterator<IFilTriSoundingsProxy> getSoundingsIterator(int maxSoundCount) {
		List<IFilTriSoundingsProxy> allFilTriSoundings = new ArrayList<>();

		BufferAssociation beamsAssociation = soundingsModel.getBeamsAssociation();
		FilTriSoundings filTriSoundings = null;
		for (int fileidx = 0; fileidx < beamsAssociation.size(); fileidx++) {
			boolean readOnly = !soundingsModel.getFileAppearances(fileidx).editable();
			List<LineBuffer> buffers = beamsAssociation.getBuffers(fileidx);
			for (LineBuffer lineBuffer : buffers) {
				if (filTriSoundings == null
						|| filTriSoundings.size() + lineBuffer.getDetectionCount() > maxSoundCount) {
					filTriSoundings = new FilTriSoundings(readOnly);
					allFilTriSoundings.add(filTriSoundings);
				}
				filTriSoundings.add(lineBuffer);
			}
		}

		return allFilTriSoundings.iterator();
	}

}
