package fr.ifremer.globe.editor.swath.ui.soundingsview.ui;

import org.eclipse.e4.core.di.annotations.Execute;

import fr.ifremer.globe.ui.utils.Messages;

/** Display help action and shortcuts */
public class HelpHandler {

	@Execute
	public void execute() {

		String helpText = """
				Swath editor commands :

				View actions :
				 - Right clic and drag : rotate the view
				 - Center clic and drag : shift the view
				 - Scroll wheel : zoom
				 - Arrows : shift Geographic view selection
				 - Ctrl + Z : undo last validation / invalitation
				 - Ctrl + Y : redo last validation / invalitation
				 - Del (or Suppr) : invalitate beams in selection area
				 - Esc : remove the selection area
				 - Ctrl + S : save the modifications in files

				Square or lasso selection tool :
				 - Left clic and drag (or Space) : draw a selection area

				Polygon selection tool :
				 - Left clic (or Space) : add a point to the selection area
				 - Double clic (or Enter) : close and validate the selection area
				 - Right clic : remove last point of the selection area
				""";

		Messages.openInfoMessage("Swath editor help", helpText);
	}

}
