package fr.ifremer.globe.editor.swath.ui.soundingsview.preferences;

public enum ViewsType
{
	Top, 
	DepthLong,
	DepthLat,
	MainAxis,
	SecondaryAxis
}
