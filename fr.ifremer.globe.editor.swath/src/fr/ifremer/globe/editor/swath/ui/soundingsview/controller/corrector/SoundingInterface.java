package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector;

import fr.ifremer.globe.core.processes.biascorrection.model.IBiasSounding;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;

/**
 * Interface to bind a {@link LineBuffer} with the {@link DynamicBiasCorrection} process
 */
class SoundingInterface implements IBiasSounding {
	private short beamNumber;
	private int cycleIndex;
	private Mappings mappedData;
	private long mapIndex;
	LineBuffer line;

	SoundingInterface(LineBuffer buffer, short beamNumber, int cycleIndex, long index) {
		this.beamNumber = beamNumber;
		this.cycleIndex = cycleIndex;
		mapIndex = index + buffer.getOffsetInFile();
		line = buffer;
		mappedData = buffer.getSpatialIndex().getMappings();
	}

	@Override
	public short getBeamNumber() {
		return beamNumber;
	}

	@Override
	public int getSwathNumber() {
		return cycleIndex;
	}

	/**
	 * @see fr.ifremer.globe.process.biascorrection.model.IBiasSounding#setGeographicalCoordinates(double, double,
	 *      double)
	 */
	@Override
	public void setGeographicalCoordinates(double longitude, double latitude, double depth) {
		mappedData.acceptGeographicalCoordinates(mapIndex, longitude, latitude, (float) depth);
	}
}