/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.geographicview;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.terrain.LocalElevationModel;

/**
 * LocalElevationModel used in Swath Editor to represent the ground elevations
 */
public class DtmElevationModel extends LocalElevationModel {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(DtmElevationModel.class);

	/** Sector of the DTM */
	protected Sector dtmBoundingBox;
	/** Nb of column in the DTM */
	protected int columnCount;
	/** Nb of line in the DTM */
	protected int lineCount;
	/** % of extra space around the DTM */
	protected double expand;

	/**
	 * Constructor.
	 */
	public DtmElevationModel(Sector dtmBoundingBox, int columnCount, int lineCount) {
		this.dtmBoundingBox = dtmBoundingBox;
		this.columnCount = columnCount;
		this.lineCount = lineCount;
	}

	/** Expand the sector and fill the elevations with the minimum value */
	public void expandSector(double percent) {
		if (!tiles.isEmpty() && expand != percent) {
			this.expand = percent;
			logger.info("Adding " + (int) (percent * 100) + "% of space aroung Swath DTM");

			// Clear all tiles and restore the main one
			LocalTile mainTile = tiles.get(0);
			tiles.clear();
			tiles.add(mainTile);
			if (percent > 0) {
				// Compute expanded sector
				double cellHeight = dtmBoundingBox.getDeltaLatDegrees() * percent;
				double cellWidth = dtmBoundingBox.getDeltaLonDegrees() * percent;
				Angle minLatitude = dtmBoundingBox.getMinLatitude().subtractDegrees(cellHeight / 2);
				Angle maxLatitude = dtmBoundingBox.getMaxLatitude().addDegrees(cellHeight / 2);
				Angle minLongitude = dtmBoundingBox.getMinLongitude().subtractDegrees(cellWidth / 2);
				Angle maxLongitude = dtmBoundingBox.getMaxLongitude().addDegrees(cellWidth / 2);

				// West
				addFlatElevations(minLatitude, maxLatitude, minLongitude, dtmBoundingBox.getMinLongitude(), (int) (columnCount * percent), lineCount);
				// East
				addFlatElevations(minLatitude, maxLatitude, dtmBoundingBox.getMaxLongitude(), maxLongitude, (int) (columnCount * percent), lineCount);
				// // North
				addFlatElevations(dtmBoundingBox.getMaxLatitude(), maxLatitude, minLongitude, maxLongitude, columnCount, (int) (lineCount * percent));
				// // South
				addFlatElevations(minLatitude, dtmBoundingBox.getMinLatitude(), minLongitude, maxLongitude, columnCount, (int) (lineCount * percent));
			}
		}
	}

	/**
	 * Add elevations to cover the specified sector
	 */
	protected void addFlatElevations(Angle minLatitude, Angle maxLatitude, Angle minLongitude, Angle maxLongitude, int width, int height) {
		Sector sector = new Sector(minLatitude, maxLatitude, minLongitude, maxLongitude);
		AVList parameters = new AVListImpl();
		parameters.setValue(AVKey.DATA_TYPE, AVKey.FLOAT64);
		parameters.setValue(AVKey.ELEVATION_MIN, getMinElevation());
		parameters.setValue(AVKey.ELEVATION_MAX, getMinElevation());

		// Make a ByteBuffer of minimum elevation to cover the sector
		ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES * height * width);
		DoubleBuffer doubleBuffer = byteBuffer.asDoubleBuffer();
		while (doubleBuffer.position() < doubleBuffer.capacity()) {
			doubleBuffer.put(getMinElevation());
		}
		doubleBuffer.rewind();

		addElevations(byteBuffer, sector, width, height, parameters);
	}

	/**
	 * Follow the link.
	 * 
	 * @see gov.nasa.worldwind.terrain.LocalElevationModel#lookupElevation(double,
	 *      double)
	 */
	@Override
	protected Double lookupElevation(double latRadians, double lonRadians) {
		// Replace NoDataValue by min elevation
		Double result = super.lookupElevation(latRadians, lonRadians);
		return result == getMissingDataSignal() ? getMinElevation() : result;
	}

	/**
	 * Follow the link.
	 * 
	 * @see gov.nasa.worldwind.terrain.LocalElevationModel#addElevations(java.io.File)
	 */
	@Override
	public void addElevations(File file) {
		logger.info("Adding elevations to Swath DTM model from " + file.getAbsolutePath());

		tiles.clear();
		try {
			super.addElevations(file);
			if (expand > 0d) {
				expand = 0d;
				expandSector(this.expand);
			}
		} catch (IOException e) {
			logger.warn("Unable to add elevation file " + file.getName() + " : " + e.getMessage());
		}
	}

	/**
	 * @return the {@link #dtmBoundingBox}
	 */
	public Sector getDtmBoundingBox() {
		return dtmBoundingBox;
	}

	/**
	 * @param dtmBoundingBox
	 *            the {@link #dtmBoundingBox} to set
	 */
	public void setDtmBoundingBox(Sector dtmBoundingBox) {
		this.dtmBoundingBox = dtmBoundingBox;
	}

	/**
	 * @return the {@link #columnCount}
	 */
	public int getColumnCount() {
		return columnCount;
	}

	/**
	 * @param columnCount
	 *            the {@link #columnCount} to set
	 */
	public void setColumnCount(int columnCount) {
		this.columnCount = columnCount;
	}

	/**
	 * @return the {@link #lineCount}
	 */
	public int getLineCount() {
		return lineCount;
	}

	/**
	 * @param lineCount
	 *            the {@link #lineCount} to set
	 */
	public void setLineCount(int lineCount) {
		this.lineCount = lineCount;
	}

	/**
	 * @return the {@link #expand}
	 */
	public double getExpand() {
		return expand;
	}

	/**
	 * @param expand
	 *            the {@link #expand} to set
	 */
	public void setExpand(double expand) {
		this.expand = expand;
	}

}
