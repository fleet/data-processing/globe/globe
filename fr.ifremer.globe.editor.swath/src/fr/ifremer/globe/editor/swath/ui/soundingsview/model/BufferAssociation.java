package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.SoftReferenceObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;

/**
 * Maintain a list of buffer associated per file
 * <p>
 * Buffer are stored by their index of addition through {@link #addFile(Object)}
 */
public class BufferAssociation {
	private static final Logger logger = LoggerFactory.getLogger(BufferAssociation.class);

	private ObjectPool<LineBuffer> pool = new SoftReferenceObjectPool<>(new BasePoolableObjectFactory<LineBuffer>() {
		@Override
		public LineBuffer makeObject() throws Exception {
			return new LineBuffer();
		}

		@Override
		public void passivateObject(LineBuffer t) {
			t.reset();
		}
	});

	List<SounderDataContainer> association = new ArrayList<>();
	// the list of buffer per file
	List<List<LineBuffer>> buffers = new ArrayList<>();

	// the spatial index
	List<SpatialIndex> spatialIndex = new ArrayList<>();

	/**
	 * Declare a new File association
	 *
	 * @param key the associated file
	 * @return the association index
	 */
	public int addFile(SounderDataContainer key, SpatialIndex spatialIndex) {
		this.association.add(key);
		this.buffers.add(new ArrayList<>());
		this.spatialIndex.add(spatialIndex);
		return this.association.size() - 1;
	}

	/**
	 * return the index of the given key
	 */
	public int indexOf(SounderDataContainer file) {
		return association.indexOf(file);
	}

	/**
	 * return the file associated for the given index
	 *
	 * @param index the index
	 */
	public SounderDataContainer getFile(int index) {
		return this.association.get(index);
	}

	/**
	 * return the spatial index associated for the given index
	 *
	 * @param index the index
	 */
	public SpatialIndex getSpatialIndex(int index) {
		return this.spatialIndex.get(index);
	}

	/**
	 * return an unmodifiable list of buffer for the given index
	 *
	 * @param index the index
	 */
	public List<LineBuffer> getBuffers(int index) {
		return Collections.unmodifiableList(this.buffers.get(index));
	}

	public void dispose() {
		this.association.clear();

		for (List<LineBuffer> tab : this.buffers) {
			for (LineBuffer buff : tab) {
				try {
					this.pool.returnObject(buff);
				} catch (Exception e) {
					logger.error("Error while releasing object, trying to continue", e);
				}
			}
		}
		this.buffers.clear();

	}

	public int size() {
		return this.association.size();
	}

	/**
	 * free buffer for the given index
	 *
	 * @throws Exception
	 */
	public void releaseBuffers(int index) throws Exception {
		for (LineBuffer buf : this.buffers.get(index)) {
			buf.reset();
			this.pool.returnObject(buf);
		}
		this.buffers.get(index).clear();
	}

	/**
	 * free all buffers
	 */
	public void releaseAllBuffers() {
		for (var lineBuffers : buffers) {
			for (LineBuffer lineBuffer : lineBuffers) {
				lineBuffer.reset();
				try {
					this.pool.returnObject(lineBuffer);
				} catch (Exception e) {
					logger.warn("Error while releasing object : {}", e.getMessage());
				}
			}
			lineBuffers.clear();
		}
	}

	/**
	 * get a new buffer, it will be added to the buffer association
	 *
	 * @param expectedSize
	 * @throws CannotAllocateData
	 */
	public LineBuffer getNewBuffer(int index) throws CannotAllocateData {
		LineBuffer buffer;
		try {
			buffer = this.pool.borrowObject();
		} catch (Exception e) {
			throw new CannotAllocateData("Cannot allocate Data", e);
		}
		this.buffers.get(index).add(buffer);
		return buffer;

	}

	/**
	 * @return the {@link #spatialIndex}
	 */
	public List<SpatialIndex> getSpatialIndex() {
		return this.spatialIndex;
	}
}
