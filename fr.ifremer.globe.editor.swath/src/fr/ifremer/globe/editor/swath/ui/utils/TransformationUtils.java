package fr.ifremer.globe.editor.swath.ui.utils;

import java.awt.Color;

/**
 * Utility class to handle transformation between spatial references.
 */
public class TransformationUtils {

	/**
	 * elevation --> Color
	 * 
	 * Return color from normalized elevation according to CARAIBES colormap
	 */
	public static Color getColorFromNormalizedElevation(double normalizedElevation) {
		double r, g, b;

		if (normalizedElevation <= 0.04f) {
			r = 4.0 * normalizedElevation * 100.0 / 255.0;
			g = 0.0f;
			b = (normalizedElevation * 100.0 + 200.0) / 255.0;
		} else if (normalizedElevation <= 0.1) {
			r = (4.0 * normalizedElevation * 100.0 - 1.0) / 255.0;
			g = 6.0 * normalizedElevation * 100.0 / 255.0;
			b = (normalizedElevation * 100.0 + 200.0) / 255.0;
		} else if (normalizedElevation <= 0.17) {
			r = (4.0 * normalizedElevation * 100.0 - 2.0) / 255.0;
			g = 6.0 * normalizedElevation * 100.0 / 255.0;
			b = (normalizedElevation * 100.0 + 200.0) / 255.0;
		} else if (normalizedElevation <= 0.23) {
			r = (4.0 * normalizedElevation * 100.0 - 3.0) / 255.0;
			g = 6.0 * normalizedElevation * 100.0 / 255.0;
			b = (normalizedElevation * 100.0 + 200.0) / 255.0;
		} else if (normalizedElevation <= 0.26) {
			r = (4.0 * normalizedElevation * 100.0 - 4.0) / 255.0;
			g = 6.0 * normalizedElevation * 100.0 / 255.0;
			b = (normalizedElevation * 100.0 + 200.0) / 255.0;
		} else if (normalizedElevation <= 0.50) {
			r = (-4.0 * normalizedElevation * 100.0 + 200.0) / 255.0;
			g = (4.0 * normalizedElevation * 100.0 + 45.0) / 255.0;
			b = (-10.0 * normalizedElevation * 100.0 + 500.0) / 255.0;
		} else if (normalizedElevation <= 0.73) {
			r = (11.0 * normalizedElevation * 100.0 - 550.0) / 255.0;
			g = 1.0f;
			b = 0.0f;
		} else {
			r = 1.0f;
			g = (-10 * normalizedElevation * 100.0 + 990.0) / 255.0;
			b = 0.0f;
		}
		r = Math.max(0.0, Math.min(1.0, r));
		g = Math.max(0.0, Math.min(1.0, g));
		b = Math.max(0.0, Math.min(1.0, b));
		return new Color((float) r, (float) g, (float) b);
	}

}
