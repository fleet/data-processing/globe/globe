package fr.ifremer.globe.editor.swath.ui.soundingsview.preferences;

/**
 * data displayed on screen valid soundings, invalid soundings , all soundings
 * */
public enum BeamVisibleMode {
	ALL, 
	VALID_ONLY, 
	INVALID_ONLY;
}
