/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

/**
 *
 */
public class SummaryViewerFilter extends ViewerFilter {

	/** Filtering criterias */
	protected final FilterPreferences criterias;

	/** Constructor */
	public SummaryViewerFilter(FilterPreferences criterias) {
		this.criterias = criterias;
	}

	/** {@inheritDoc} */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		SummaryItem item = (SummaryItem) element;
		return (criterias.getMinCycleIndex().isNull() || item.getCycleIndex() >= criterias.getMinCycleIndex().get()) //
				&& (criterias.getMaxCycleIndex().isNull() || item.getCycleIndex() <= criterias.getMaxCycleIndex().get()) //
				&& (criterias.getMinBeamIndex().isNull() || item.getBeamIndex() >= criterias.getMinBeamIndex().get()) //
				&& (criterias.getMaxBeamIndex().isNull() || item.getBeamIndex() <= criterias.getMaxBeamIndex().get()) //
				&& (criterias.getMinDepth().isNull() || item.getDepth() >= criterias.getMinDepth().get()) //
				&& (criterias.getMaxDepth().isNull() || item.getDepth() <= criterias.getMaxDepth().get()) //

				&& (criterias.getMinReflectivity().isNull()
						|| item.getReflectivity() >= criterias.getMinReflectivity().get()) //

				&& (criterias.getMaxReflectivity().isNull()
						|| item.getReflectivity() <= criterias.getMaxReflectivity().get()) //

				&& (Double.isNaN(item.getLongitude()) || criterias.getMinLongitude().isNull()
						|| item.getLongitude() >= criterias.getMinLongitude().get()) //

				&& (Double.isNaN(item.getLongitude()) || criterias.getMaxLongitude().isNull()
						|| item.getLongitude() <= criterias.getMaxLongitude().get()) //

				&& (Double.isNaN(item.getLatitude()) || criterias.getMinLatitude().isNull()
						|| item.getLatitude() >= criterias.getMinLatitude().get()) //

				&& (Double.isNaN(item.getLatitude()) || criterias.getMaxLatitude().isNull()
						|| item.getLatitude() <= criterias.getMaxLatitude().get()) //

				&& (criterias.getValidity().isNull() || item.getValidity() == criterias.getValidity().get()) //
				&& (criterias.getDetection().isNull() || item.getDetection() == criterias.getDetection().value()) //
		;
	}

}
