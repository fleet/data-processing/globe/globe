package fr.ifremer.globe.editor.swath.ui.soundingsview.model;


/**
 * Statistics of displayed data for a file
 * */
public class FileStat
{
	public FileStat(int numberOfSoundings) {
		this.soundingCount=numberOfSoundings;
		}

	public int soundingCount;

	/**
	 * @return the soundingCount
	 */
	public int getSoundingCount() {
		return soundingCount;
	}

	/**
	 * @param soundingCount the soundingCount to set
	 */
	public void setSoundingCount(int soundingCount) {
		this.soundingCount = soundingCount;
	}
};