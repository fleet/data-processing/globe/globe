package fr.ifremer.globe.editor.swath.ui.soundingsview.action;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import fr.ifremer.globe.core.utils.undo.UndoAction;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;

public class SoundEditorAction extends UndoAction {

	/** Soundings model */
	@Inject
	@Named(ContextNames.SOUNDINGS_CONTROLLER)
	protected SoundingsController soundingsControler;

	/** Message (Tooltip) */
	protected String message = "Validate beam(s)";

	/** Index of action used by SoundingsControler */
	protected int actionIndex;

	/**
	 * Constructor.
	 */
	public SoundEditorAction(String message) {
		this.message = message;
		ContextInitializer.inject(this);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.model.undo.UndoAction#executeUndo()
	 */
	@Override
	protected void executeUndo() {
		soundingsControler.restoreView(actionIndex);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.model.undo.UndoAction#executeRedo()
	 */
	@Override
	protected void executeRedo() {
		soundingsControler.restoreView(actionIndex + 1);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.model.undo.IUndoeable#msgInfo()
	 */
	@Override
	public String msgInfo() {
		return message;
	}

	/** Getter of {@link #actionIndex} */
	public int getActionIndex() {
		return actionIndex;
	}

	/** Setter of {@link #actionIndex} */
	public void setActionIndex(int actionIndex) {
		this.actionIndex = actionIndex;
	}

	/** Setter of {@link #message} */
	public void setMessage(String message) {
		this.message = message;
	}
}
