/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.BufferAssociation;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;

/**
 * Controler of the Soundings view to manage the validation/invalidation process.
 */
public class ValidationController {

	/** Model. */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	/**
	 * Constructor.
	 */
	public ValidationController() {
		super();
	}

	/** Perform the validation of selected beams. */
	public void validateSelectedBeams(MappingValidity validValue) {
		BufferAssociation bufferAssociation = soundingsModel.getBeamsAssociation();
		for (int fileIndex = 0; fileIndex < bufferAssociation.size(); fileIndex++) {
			SelectionModel dedicatedSelectionModel = soundingsModel.getSelectionModels().get(fileIndex);
			List<LineBuffer> buffers = bufferAssociation.getBuffers(fileIndex);
			boolean editable = soundingsModel.getFileAppearances(fileIndex).editable();
			buffers.parallelStream().forEach(lineBuffer -> {
				if (editable) {
					validateBeams(lineBuffer, dedicatedSelectionModel, validValue);
				} else {
					// Edition no allowed. Just unselect
					int nbBeams = lineBuffer.getDetectionCount();
					for (int offset = 0; offset < nbBeams; offset++) {
						long offsetInFile = lineBuffer.getOffsetInFile() + offset;
						dedicatedSelectionModel.setSelected(offsetInFile, false);
					}
				}
			});
		}
	}

	/**
	 * Change the valid attribute of the beams designated by the specified model.
	 * 
	 * @param designationModels all designated beams.
	 * @param validValue value of validation attribute
	 * @return the number of validated beam
	 */
	public int validateDesignatedBeams(List<SelectionModel> designationModels, MappingValidity validValue) {
		int result = 0;
		BufferAssociation bufferAssociation = soundingsModel.getBeamsAssociation();
		for (int fileIndex = 0; fileIndex < bufferAssociation.size(); fileIndex++) {
			if (soundingsModel.getFileAppearances(fileIndex).editable()) {
				SelectionModel designationModel = designationModels.get(fileIndex);
				for (LineBuffer lineBuffer : bufferAssociation.getBuffers(fileIndex)) {
					result += validateBeams(lineBuffer, designationModel, validValue);
				}
			}
		}
		return result;
	}

	/** Set validity flag of selected beams. */
	protected int validateBeams(LineBuffer lineBuffer, SelectionModel selectionModel, MappingValidity value) {
		int result = 0;
		Mappings mappings = lineBuffer.getSpatialIndex().getMappings();
		int nbBeams = lineBuffer.getDetectionCount();
		for (int offset = 0; offset < nbBeams; offset++) {
			long offsetInFile = lineBuffer.getOffsetInFile() + offset;
			if (selectionModel.isSelected(offsetInFile)) {
				MappingValidity currentValidity = mappings.getValidity(offsetInFile);
				if (currentValidity != MappingValidity.SND_UNVALID_ACQUIS
						&& currentValidity != MappingValidity.SND_UNKNOWN
						&& currentValidity != MappingValidity.SND_UNVALID_CONVERSION && currentValidity != value) {
					mappings.setValidity(offsetInFile, value);
					result++;
				}
			}
			selectionModel.setSelected(offsetInFile, false);
		}
		return result;
	}
}
