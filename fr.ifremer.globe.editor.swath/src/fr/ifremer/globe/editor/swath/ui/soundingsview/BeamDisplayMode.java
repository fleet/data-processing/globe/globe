package fr.ifremer.globe.editor.swath.ui.soundingsview;

/**
 * How to color beams in Soundings View
 */
public enum BeamDisplayMode {
	VALID_INVALID("Validity"), REFLECTIVITY("Reflectivity"), FILE("File"), DEPTH("Depth"), DETECTION_TYPE(
			"Detection"), BEAM_NUMBER("Beam"), CYCLE_NUMBER("Ping"), QUALITY_FACTOR("Quality");

	private final String label;

	private BeamDisplayMode(String label) {
		this.label = label;
	}

	/**
	 * @return the {@link #label}
	 */
	public String getLabel() {
		return label;
	}
}
