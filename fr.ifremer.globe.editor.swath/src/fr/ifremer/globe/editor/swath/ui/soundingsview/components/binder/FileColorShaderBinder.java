/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

/**
 * Binder for coloring by file.<br>
 * Bind uniforms "u_file_color"
 */
public class FileColorShaderBinder extends AShaderBinder {

	/**
	 * Index of the file. <br>
	 * Same file order than oundingsModel.getInfoStores()
	 */
	private final int fileIdx;
	/** Color to bind */
	private GColor color;

	/**
	 * Constructor.
	 */
	public FileColorShaderBinder(int fileIdx) {
		this.fileIdx = fileIdx;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		Uniform<Vector4F> fileColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms().get("u_file_color");
		fileColorUniform.setValue(
				new Vector4F(color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()));
	}

	/** {@inheritDoc} */
	@Override
	public void clear() {
		// Refresh the color from the model
		color = soundingModel.getFileAppearances(fileIdx).color();
	}
}
