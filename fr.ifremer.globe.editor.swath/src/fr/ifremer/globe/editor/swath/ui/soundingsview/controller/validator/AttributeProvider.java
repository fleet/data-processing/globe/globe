/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.validator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.ifremer.globe.core.processes.detectionfilter.Attributes;
import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;

/**
 * Implementation of {@link IAttributeProvider} working on a {@link Mappings}
 */
public class AttributeProvider implements IAttributeProvider {

	protected Map<PredefinedLayers<?>, Class<?>> dataAttributes = new LinkedHashMap<>();
	protected Map<PredefinedLayers<?>, Class<?>> filterAttributes = new LinkedHashMap<>();

	/**
	 * Constructor.
	 */
	public AttributeProvider() {
		dataAttributes.put(Attributes.VALIDITY, Boolean.class);
		dataAttributes.put(Attributes.SELECTION, Boolean.class);

		filterAttributes.put(Attributes.PING_INDEX, Integer.class);
		filterAttributes.put(Attributes.SELECTION, Boolean.class);
		filterAttributes.put(Attributes.BEAM_INDEX, Integer.class);
		filterAttributes.put(Attributes.DEPTH, Double.class);
		filterAttributes.put(Attributes.VALIDITY, Boolean.class);
		filterAttributes.put(Attributes.REFLECTIVITY, Double.class);
		filterAttributes.put(Attributes.AMPLITUDE_PHASE_DETECTION, Byte.class);
		filterAttributes.put(Attributes.SCALAR_QUALITY_FACTOR, Float.class);
	}

	@Override
	public Class<?> retrieveDataAttributeType(PredefinedLayers<?> layer) {
		return dataAttributes.get(layer);
	}

	/** {@inheritDoc} */
	@Override
	public List<PredefinedLayers<?>> retrieveDataAttributes() {
		return new ArrayList<>(dataAttributes.keySet());
	}

	/** {@inheritDoc} */
	@Override
	public List<PredefinedLayers<?>> retrieveFilterAttributes() {
		return new ArrayList<>(filterAttributes.keySet());
	}

	@Override
	public Class<?> retrieveFilterAttributeType(PredefinedLayers<?> layer) {
		return filterAttributes.get(layer);
	}

}
