package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox;

import javax.swing.JMenuItem;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class JMenuItemWithData extends JMenuItem {

	private static final long serialVersionUID = 1L;
	private Button button;
	private Object object;

	public JMenuItemWithData(String string, Composite composite) {
		super(string);

		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				JMenuItemWithData.this.button = new Button(composite, SWT.NONE);
				JMenuItemWithData.this.button.setVisible(false);
			}
		});
	}

	/**
	 * @return the data
	 */
	public Object getData(String key) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				JMenuItemWithData.this.object=JMenuItemWithData.this.button.getData(key);
			}
		});
		return this.object;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String value, Object data) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				JMenuItemWithData.this.button.setData(value, data);
			}
		});
	}
}