package fr.ifremer.globe.editor.swath.ui.wizard;

import java.text.NumberFormat;
import java.util.Locale;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.utils.UIUtils;

public class SwathEditorWizardPage1 extends WizardPage {

	protected SwathEditorParameters parameters;

	private Composite container;
	private Button forceRecomputeCheckButton;
	private Label spatialResolutionArcsecondsLabel;
	private Text spatialResolutionText;
	private Label dtmDimensionLabel;

	public SwathEditorWizardPage1(SwathEditorParameters parameters) {
		super("SwathEditorWizardPage1");
		this.parameters = parameters;
	}

	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.NULL);
		GridLayout glContainer = new GridLayout(2, false);
		glContainer.marginHeight = 10;
		glContainer.marginWidth = 10;
		glContainer.verticalSpacing = 10;
		glContainer.horizontalSpacing = 10;
		container.setLayout(glContainer);

		Composite resolutionComposite = new Composite(container, SWT.NONE);
		resolutionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		resolutionComposite.setLayout(new GridLayout(3, false));

		Label spatialResolutionLabel = new Label(resolutionComposite, SWT.NONE);
		spatialResolutionLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		spatialResolutionLabel.setText("DTM cell size (m)");

		spatialResolutionText = new Text(resolutionComposite, SWT.BORDER);
		spatialResolutionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		spatialResolutionArcsecondsLabel = new Label(resolutionComposite, SWT.NONE);
		spatialResolutionArcsecondsLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		spatialResolutionArcsecondsLabel.setText(" = ?.?? arcseconds");

		dtmDimensionLabel = new Label(container, SWT.NONE);
		dtmDimensionLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		dtmDimensionLabel.setText("w x h");

		setControl(container);
	}

	public void initializeControls() {

		initializeFloatText(spatialResolutionText, parameters.spatialResolution);

		ModifyListener listener = e -> {
			if (validateFloatText(spatialResolutionText)) {
				SwathEditorWizardPage1.this.setPageComplete(true);
				updateLabels();
			} else {
				SwathEditorWizardPage1.this.setPageComplete(false);
			}
		};

		// initialize field
		listener.modifyText(null);
		spatialResolutionText.addModifyListener(listener);

		initializeFloatText(spatialResolutionText, parameters.spatialResolution);
	}

	public boolean getForceRecompute() {
		return forceRecomputeCheckButton.getSelection();
	}

	/**
	 * Meters.
	 */
	protected double getRawSpatialResolution() {
		try {
			return Double.parseDouble(spatialResolutionText.getText());
		} catch (NumberFormatException e) {
			return Double.NaN;
		}
	}

	/**
	 * Arcsec.
	 */
	public double getConvertedSpatialResolution() {
		try {
			double ARCSEC_IN_METERS = 30.0;
			double spatialResolutionMeters = Double.parseDouble(spatialResolutionText.getText());
			double spatialResolutionArcsec = spatialResolutionMeters / ARCSEC_IN_METERS;
			return spatialResolutionArcsec;
		} catch (NumberFormatException e) {
			return Double.NaN;
		}
	}

	public void updateLabels() {
		dtmDimensionLabel.setVisible(true);

		NumberFormat format = NumberFormat.getNumberInstance(Locale.US);
		format.setMaximumFractionDigits(5);
		double arcseconds = getConvertedSpatialResolution();
		spatialResolutionArcsecondsLabel.setText(" \u2243 " + format.format(arcseconds) + " arcseconds");

		updateDtmDimensionLabel(dtmDimensionLabel, getRawSpatialResolution());

		container.layout(true, true);
	}

	protected void updateDtmDimensionLabel(Label dtmDimensionLabel, double rawSpatialDimension) {
		boolean dimOK = true;
		String strDimensions = "";
		if (rawSpatialDimension > 0) {
			parameters = parameters.duplicateWithSpatialResolution(rawSpatialDimension);
			dimOK = (long) parameters.rowCount * parameters.columnCount * Integer.BYTES < Integer.MAX_VALUE;
			if (dimOK) {
				strDimensions = String.format("DTM size [row,col] : [%d, %d]", parameters.rowCount,
						parameters.columnCount);
			} else {
				strDimensions = "DTM size to large";
			}
		} else {
			dimOK = false;
			strDimensions = "Resolution must be positive";
		}

		if (dimOK)
			dtmDimensionLabel.setForeground(dtmDimensionLabel.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		else
			dtmDimensionLabel.setForeground(dtmDimensionLabel.getDisplay().getSystemColor(SWT.COLOR_RED));

		dtmDimensionLabel.setText(strDimensions);

	}

	public boolean validateInputs() {
		return validateFloatText(spatialResolutionText);
	}

	/**
	 * @return the {@link #parameters}
	 */
	public SwathEditorParameters getParameters() {
		return parameters;
	}

	protected void initializeFloatText(Text text, double value) {
		text.setText(Double.toString(value));
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				validateInputs();
			}
		});
	}

	protected boolean validateFloatText(Text text) {
		IInputValidator validator = UIUtils.getFloatValidator();
		String message = validator.isValid(text.getText());
		if (message == null) {
			try {
				double v = Double.parseDouble(text.getText());
				if (Double.isNaN(v)) {
					message = "DTM cell size has an invalid value";
				}
			} catch (Exception e) {
				message = "DTM cell size has an invalid value";

			}
		}
		return validateMessage(message);
	}

	protected boolean validateMessage(String message) {
		if (message != null) {
			setErrorMessage(getDescription() + "  ERROR: " + message);
		} else {
			setErrorMessage(null);
		}
		return message == null;
	}

}
