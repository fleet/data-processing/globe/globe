/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderProgram;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Set of IShaderBinder. Composite pattern.
 */
public class ShaderBinderComposite extends AShaderBinder {

	/** Vertex Shader program. */
	protected final String vertexShaderSource;

	/** Fragment Shader program. */
	protected final String fragmentShaderSource;

	/** Composites. */
	protected List<AShaderBinder> childrenShaderBinder = new ArrayList<>();

	/**
	 * Constructor.
	 */
	public ShaderBinderComposite(String vertexShaderSource, String fragmentShaderSource,
			AShaderBinder... shaderBinders) {
		this(vertexShaderSource, fragmentShaderSource);
		addShaderBinder(shaderBinders);
	}

	/**
	 * Constructor.
	 */
	public ShaderBinderComposite(String vertexShaderSource, String fragmentShaderSource) {
		this.vertexShaderSource = vertexShaderSource;
		this.fragmentShaderSource = fragmentShaderSource;

	}

	/**
	 * Add some IShaderBinder.
	 */
	public void addShaderBinder(AShaderBinder... shaderBinders) {
		Collections.addAll(childrenShaderBinder, shaderBinders);
	}

	/** {@inheritDoc} */
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		for (AShaderBinder shaderBinder : childrenShaderBinder) {
			shaderBinder.bindUniforms(lineBuffer);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		super.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);

		for (AShaderBinder shaderBinder : childrenShaderBinder) {
			shaderBinder.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);
		}
	}

	/**
	 * @return the {@link #vertexShaderSource}
	 */
	public String getVertexShaderSource() {
		return vertexShaderSource;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#createShaderProgram()
	 */
	@Override
	public ShaderProgram createShaderProgram() throws CouldNotCreateVideoCardResourceException {
		setShaderProgram(Device2x.createShaderProgram(vertexShaderSource, "", fragmentShaderSource, false));
		return getShaderProgram();
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#setShaderProgram(fr.ifremer.globe.ogl.renderer.shaders.ShaderProgram)
	 */
	@Override
	public void setShaderProgram(ShaderProgram shaderProgram) {
		super.setShaderProgram(shaderProgram);
		childrenShaderBinder.stream().forEach((shaderBinder) -> shaderBinder.setShaderProgram(shaderProgram));
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#clear()
	 */
	@Override
	public void clear() {
		super.clear();
		childrenShaderBinder.stream().forEach(AShaderBinder::clear);
	}

}
