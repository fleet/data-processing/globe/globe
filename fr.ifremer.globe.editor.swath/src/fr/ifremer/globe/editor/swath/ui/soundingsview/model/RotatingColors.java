package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import java.util.HashMap;
import java.util.Map;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;

public class RotatingColors {

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;

	private Map<IFileInfo, GColor> colormap = new HashMap<>();
	private ColorProvider colorProvider = new ColorProvider();

	/** Constructor */
	public RotatingColors() {
		ContextInitializer.inject(this);
	}

	public GColor getColor(IFileInfo o) {
		if (!colormap.containsKey(o)) {
			GColor c = colorProvider.get();
			colormap.put(o, c);
			return c;
		}
		return colormap.get(o);
	}
}
