/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.Buffer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Binder for AmplitudePhaseDetection shader.<br>
 * Bind attribute "in_detection"
 */
public class AmplitudePhaseDetectionShaderBinder extends AShaderBinder {

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindUniforms(fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		Uniform<Vector4F> amplitudeColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms()
				.get("amplitude_detection_color");
		if (amplitudeColorUniform != null) {
			GColor amplitudeColor = dtmPreferences.getAmplitudeDetectionColor().getValue();
			amplitudeColorUniform.setValue(new Vector4F(amplitudeColor.getFloatRed(), amplitudeColor.getFloatGreen(),
					amplitudeColor.getFloatBlue(), 1f));
		}
		Uniform<Vector4F> phaseColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms()
				.get("phase_detection_color");
		if (phaseColorUniform != null) {
			GColor phaseColor = dtmPreferences.getPhaseDetectionColor().getValue();
			phaseColorUniform.setValue(
					new Vector4F(phaseColor.getFloatRed(), phaseColor.getFloatGreen(), phaseColor.getFloatBlue(), 1f));
		}
		Uniform<Vector4F> invalidColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms()
				.get("u_invalid_color");
		if (invalidColorUniform != null) {
			GColor invalidColor = dtmPreferences.getInvalidColor().getValue();
			invalidColorUniform.setValue(new Vector4F(invalidColor.getFloatRed(), invalidColor.getFloatGreen(),
					invalidColor.getFloatBlue(), invalidColor.getFloatAlpha()));
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindAttributes(SelectionModel,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes, java.util.function.Consumer)
	 */
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		super.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);

		int offsetInFile = (int) lineBuffer.getOffsetInFile();
		int count = lineBuffer.getDetectionCount();

		Supplier<Buffer> supplier = () -> lineBuffer.getSpatialIndex().getMappings().getDetectionArray()
				.asByteBuffer(offsetInFile, count);
		VertexBuffer amplitudePhaseDetectionVertexBuffer = new VertexBufferGL2x3x(supplier.get(), BufferHint.StaticDraw,
				count * Byte.BYTES);

		VertexBufferAttribute amplitudePhaseDetectionVertexBufferAttribute = new VertexBufferAttribute(
				amplitudePhaseDetectionVertexBuffer, ComponentDatatype.Byte, 1);
		int amplitudePhaseDetectionAttributeLocation = getShaderProgram().getVertexAttributes()
				.get("in_detection_type").getLocation();
		vertexBufferAttributes.set(amplitudePhaseDetectionAttributeLocation,
				amplitudePhaseDetectionVertexBufferAttribute);

		alertVertexBufferCreated(listener, new BufferRefresher(amplitudePhaseDetectionVertexBuffer, supplier));
	}
}
