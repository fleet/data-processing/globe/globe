package fr.ifremer.globe.editor.swath.ui.parametersview;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.editor.swath.SwathFileInfo;
import fr.ifremer.globe.editor.swath.application.context.BathySelectionMode;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.ui.events.swath.ShiftingDirection;
import fr.ifremer.globe.ui.events.swath.SwathEventTopics;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.viewer3d.layers.terrain.parametersview.DtmInfoParametersComposite;

/**
 * Composite used to edit the SwathControler parameters.
 */
public class SwathEditorParametersComposite extends Composite {

	/** Eclipse event broker service */
	@Inject
	@Optional
	protected IEventBroker eventBroker;

	/** Injection context */
	@Inject
	@Optional
	protected IEclipseContext eclipseContext;

	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	@Inject
	@Named(ContextNames.SWATH_FILE_INFO)
	protected SwathFileInfo swathFileInfo;

	protected Button btnNorth;
	protected Button btnWest;
	protected Button btnEast;
	protected Button btnSouth;
	protected Button btnRefreshDtm;

	private Button swathSelectionModeButton;
	private Spinner swathCountInSelectionSpinner;

	private Button rectangleSelectionButton;
	private Button btnAutorefresh;

	/**
	 * Constructor.
	 */
	public SwathEditorParametersComposite(Composite parent) {
		super(parent, SWT.NONE);
		// Injection
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
		initializeComposite();
		hookListener();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	protected void initializeComposite() {
		setLayout(new GridLayout(3, false));

		// move selection zone
		Group grpZoneSelection = new Group(this, SWT.NONE);
		grpZoneSelection.setToolTipText("Tips : use arrow when Sounding View selected");
		grpZoneSelection.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpZoneSelection.setText("Move selection");
		grpZoneSelection.setLayout(new GridLayout(3, false));
		new Label(grpZoneSelection, SWT.NONE);

		btnNorth = new Button(grpZoneSelection, SWT.CENTER);
		btnNorth.setEnabled(true);
		btnNorth.setToolTipText("Allows the shifting of the selection to the north (Shortcut : up arrow)");
		btnNorth.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/1uparrow.png"));
		new Label(grpZoneSelection, SWT.NONE);

		btnWest = new Button(grpZoneSelection, SWT.CENTER);
		btnWest.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		btnWest.setEnabled(true);
		btnWest.setToolTipText("Allows the shifting of the selection to the west (Shortcut : left arrow)");
		btnWest.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/1leftarrow.png"));

		btnSouth = new Button(grpZoneSelection, SWT.CENTER);
		btnSouth.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnSouth.setEnabled(true);
		btnSouth.setToolTipText("Allows the shifting of the selection to the south (Shortcut : down arrow)");
		btnSouth.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/1downarrow.png"));

		btnEast = new Button(grpZoneSelection, SWT.CENTER);
		btnEast.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		btnEast.setEnabled(true);
		btnEast.setToolTipText("Allows the shifting of the selection to the east (Shortcut : right arrow)");
		btnEast.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/1rightarrow.png"));

		// selection mode
		Group grpMode = new Group(this, SWT.NONE);
		grpMode.setLayout(new GridLayout(2, false));
		grpMode.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpMode.setText("Selection mode");

		var selectionMode = ContextInitializer.getInContext(ContextNames.BATHY_SELECTION_MODE);
		swathSelectionModeButton = new Button(grpMode, SWT.TOGGLE);
		GridDataFactory.fillDefaults().hint(30, SWT.DEFAULT).grab(true, false).applyTo(swathSelectionModeButton);
		swathSelectionModeButton.setText("Swaths");
		swathSelectionModeButton.setSelection(selectionMode == BathySelectionMode.SWATH);
		swathSelectionModeButton.addListener(SWT.Selection, evt -> {
			ContextInitializer.setInContext(ContextNames.BATHY_SELECTION_MODE, BathySelectionMode.SWATH);
			toogleButtons(swathSelectionModeButton);
		});
		swathSelectionModeButton.setToolTipText("Enable selection by Swaths");
		swathSelectionModeButton.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/swath.png"));

		Integer swathCountInSelection = ContextInitializer.getInContext(ContextNames.SWATH_COUNT_IN_SELECTION);
		swathCountInSelectionSpinner = new Spinner(grpMode, SWT.BORDER);
		GridDataFactory.fillDefaults().applyTo(swathCountInSelectionSpinner);
		swathCountInSelectionSpinner.setValues(swathCountInSelection.intValue(), 1, 99, 0, 1, 1);
		swathCountInSelectionSpinner.addListener(SWT.Selection, evt -> ContextInitializer
				.setInContext(ContextNames.SWATH_COUNT_IN_SELECTION, swathCountInSelectionSpinner.getSelection()));
		swathCountInSelectionSpinner.setToolTipText("Number of swath by selection");
		swathCountInSelectionSpinner.setEnabled(selectionMode == BathySelectionMode.SWATH);

		rectangleSelectionButton = new Button(grpMode, SWT.TOGGLE);
		GridDataFactory.fillDefaults().span(2, 1).grab(true, false).applyTo(rectangleSelectionButton);
		rectangleSelectionButton.setText("Rectangle");
		rectangleSelectionButton.setSelection(selectionMode == BathySelectionMode.RECTANGLE);
		rectangleSelectionButton.addListener(SWT.Selection, evt -> {
			ContextInitializer.setInContext(ContextNames.BATHY_SELECTION_MODE, BathySelectionMode.RECTANGLE);
			toogleButtons(rectangleSelectionButton);
		});
		rectangleSelectionButton.setToolTipText("Enable rectangular selection");
		rectangleSelectionButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/quadSelection.png"));

		// refresh button
		Group grpDtm = new Group(this, SWT.NONE);
		grpDtm.setLayout(new GridLayout(1, false));
		grpDtm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpDtm.setText("DTM");

		btnRefreshDtm = new Button(grpDtm, SWT.NONE);
		btnRefreshDtm.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnRefreshDtm.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/view_refresh.png"));
		btnRefreshDtm.setText("Refresh");

		btnAutorefresh = new Button(grpDtm, SWT.CHECK);
		btnAutorefresh.setText("Auto-refresh");
		if (soundingsModel != null && soundingsModel.getDisplayParameters() != null)
			btnAutorefresh.setSelection(soundingsModel.getDisplayParameters().isAutoRefreshDTM());

		var dtmParametersComposite = new DtmInfoParametersComposite(getParent(), swathFileInfo);
		dtmParametersComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	}

	/**
	 * add listeners to the buttons
	 */
	public void hookListener() {
		btnNorth.addListener(SWT.Selection,
				e -> eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.NORTH));
		btnWest.addListener(SWT.Selection,
				e -> eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.WEST));
		btnEast.addListener(SWT.Selection,
				e -> eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.EAST));
		btnSouth.addListener(SWT.Selection,
				e -> eventBroker.send(SwathEventTopics.SHIFT_DTM_SELECTION, ShiftingDirection.SOUTH));
		btnRefreshDtm.addListener(SWT.Selection, e -> eventBroker.send(SwathEventTopics.REFRESH_DTM, this));
		btnAutorefresh.addListener(SWT.Selection,
				e -> soundingsModel.getDisplayParameters().setAutoRefreshDTM(btnAutorefresh.getSelection()));
	}

	/** Selection mode changed */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	public void onBathySelectionModeEvent(
			@Named(ContextNames.BATHY_SELECTION_MODE) BathySelectionMode bathySelectionMode) {
		UIUtils.asyncExecSafety(rectangleSelectionButton,
				() -> rectangleSelectionButton.setSelection(bathySelectionMode == BathySelectionMode.RECTANGLE));
		UIUtils.asyncExecSafety(swathSelectionModeButton,
				() -> swathSelectionModeButton.setSelection(bathySelectionMode == BathySelectionMode.SWATH));
		UIUtils.asyncExecSafety(swathCountInSelectionSpinner,
				() -> swathCountInSelectionSpinner.setEnabled(bathySelectionMode == BathySelectionMode.SWATH));
	}

	/** Swath Count In Selection changed */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	public void onSwathCountInSelectionEvent(@Named(ContextNames.SWATH_COUNT_IN_SELECTION) int swathCount) {
		UIUtils.asyncExecSafety(swathCountInSelectionSpinner,
				() -> swathCountInSelectionSpinner.setSelection(swathCount));
	}

	/**
	 * Toogle buttons on a selection.
	 */
	protected void toogleButtons(Button selectedButton) {
		Control[] buttons = selectedButton.getParent().getChildren();
		for (Control control : buttons) {
			if (control instanceof Button button) {
				button.setSelection(control == selectedButton);
			}
		}
	}
}
