package fr.ifremer.globe.editor.swath.ui.soundingsview.preferences;

public enum SoundingsSceneMode {
	SELECT, VALIDATE, INVALIDATE
}
