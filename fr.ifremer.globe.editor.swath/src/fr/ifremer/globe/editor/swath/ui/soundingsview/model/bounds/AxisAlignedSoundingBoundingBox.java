package fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds;

import java.util.Objects;

import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * The sounding box of all displayed soundings This sounding box is axis aligned
 * 
 */
public class AxisAlignedSoundingBoundingBox {
	// extrema
	protected double minZ = 0.0;
	protected double maxZ = 0.0;
	protected double meanZ = 0.0;
	protected double deltaZ = 0.0;
	protected double minX = 0.0;
	protected double maxX = 0.0;
	protected double meanX = 0.0;
	protected double deltaX = 0.0;
	protected double minY = 0.0;
	protected double maxY = 0.0;
	protected double meanY = 0.0;
	protected double deltaY = 0.0;
	protected int soundingsCount = 0;

	// User properties
	protected Double minZDefinedByUser;
	protected Double maxZDefinedByUser;
	protected Float stepZDefinedByUser;

	public int getSoundingsCount() {
		return soundingsCount;
	}

	/** @return true when scale is computed automatically */
	public boolean isAutoScale() {
		return minZDefinedByUser == null || maxZDefinedByUser == null;
	}

	public double getMinZ() {
		if (minZDefinedByUser != null)
			return minZDefinedByUser;
		return minZ;
	}

	public double getMaxZ() {
		if (maxZDefinedByUser != null)
			return maxZDefinedByUser;
		return maxZ;
	}

	public Float getStepZDefunedByUser() {
		return stepZDefinedByUser;
	}

	public void setMinZDefinedByUser(Double value) {
		minZDefinedByUser = value;
	}

	public void setMaxZDefinedByUser(Double value) {
		maxZDefinedByUser = value;
	}

	public void setStepZDefinedByUser(Float f) {
		stepZDefinedByUser = f;
	}

	public double getMinX() {
		return minX;
	}

	public double getMaxX() {
		return maxX;
	}

	public double getMinY() {
		return minY;
	}

	public double getMaxY() {
		return maxY;
	}

	public double getDeltaX() {
		return deltaX;
	}

	public double getDeltaY() {
		return deltaY;
	}

	public double getDeltaZ() {
		return getMaxZ() - getMinZ();
	}

	public double getMeanZ() {
		return (getMaxZ() + getMinZ()) / 2;
	}

	public double getMeanX() {
		return meanX;
	}

	public double getMeanY() {
		return meanY;
	}

	public void begin() {
		minZ = Double.NaN;
		maxZ = Double.NaN;
		minY = Double.NaN;
		maxY = Double.NaN;
		minX = Double.NaN;
		maxX = Double.NaN;
		soundingsCount = 0;
	}

	public void end(double overflow) {
		double increaseX = ((maxX - minX) * 0.5) * overflow;
		maxX += increaseX;
		minX -= increaseX;
		deltaX = maxX - minX;

		double increaseY = ((maxY - minY) * 0.5) * overflow;
		maxY += increaseY;
		minY -= increaseY;
		deltaY = maxY - minY;

		double increaseZ = ((maxZ - minZ) * 0.5) * overflow;
		maxZ += increaseZ;
		minZ -= increaseZ;
		deltaZ = maxZ - minZ;

		meanX = (minX + maxX) / 2;
		meanY = (minY + maxY) / 2;
		meanZ = (minZ + maxZ) / 2;
	}

	public void push(double x, double y, double z) {
		// longitude
		minX = NumberUtils.Min(minX, x);
		maxX = NumberUtils.Max(maxX, x);
		// latitude
		minY = NumberUtils.Min(minY, y);
		maxY = NumberUtils.Max(maxY, y);
		// depth
		minZ = NumberUtils.Min(minZ, z);
		maxZ = NumberUtils.Max(maxZ, z);

		soundingsCount++;
	}

	public void copy(AxisAlignedSoundingBoundingBox other) {
		minZ = other.minZ;
		maxZ = other.maxZ;
		meanZ = other.meanZ;
		deltaZ = other.deltaZ;
		minX = other.minX;
		maxX = other.maxX;
		meanX = other.meanX;
		deltaX = other.deltaX;
		minY = other.minY;
		maxY = other.maxY;
		meanY = other.meanY;
		deltaY = other.deltaY;
		soundingsCount = other.soundingsCount;
	}

	@Override
	public String toString() {
		return "x[" + minX + "," + maxX + "](" + (maxX - minX) + ")" + "y[" + minY + "," + maxY + "](" + (maxY - minY)
				+ ")" + "z[" + minZ + "," + maxZ + "](" + (maxZ - minZ) + ")";

	}

	public Vector3D getCenter() {
		return new Vector3D((getMaxX() - getMinX()) / 2., (getMaxY() - getMinY()) / 2., (getMaxZ() - getMinZ()) / 2.);
	}

	public boolean isEmpty() {
		return soundingsCount == 0 || (deltaX == 0 && deltaY == 0);
	}

	/* Generated code */
	@Override
	public int hashCode() {
		return Objects.hash(deltaX, deltaY, deltaZ, maxX, maxY, maxZ, meanX, meanY, meanZ, minX, minY, minZ,
				soundingsCount);
	}

	/* Generated code */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AxisAlignedSoundingBoundingBox other = (AxisAlignedSoundingBoundingBox) obj;
		return Double.doubleToLongBits(deltaX) == Double.doubleToLongBits(other.deltaX)
				&& Double.doubleToLongBits(deltaY) == Double.doubleToLongBits(other.deltaY)
				&& Double.doubleToLongBits(deltaZ) == Double.doubleToLongBits(other.deltaZ)
				&& Double.doubleToLongBits(maxX) == Double.doubleToLongBits(other.maxX)
				&& Double.doubleToLongBits(maxY) == Double.doubleToLongBits(other.maxY)
				&& Double.doubleToLongBits(maxZ) == Double.doubleToLongBits(other.maxZ)
				&& Double.doubleToLongBits(meanX) == Double.doubleToLongBits(other.meanX)
				&& Double.doubleToLongBits(meanY) == Double.doubleToLongBits(other.meanY)
				&& Double.doubleToLongBits(meanZ) == Double.doubleToLongBits(other.meanZ)
				&& Double.doubleToLongBits(minX) == Double.doubleToLongBits(other.minX)
				&& Double.doubleToLongBits(minY) == Double.doubleToLongBits(other.minY)
				&& Double.doubleToLongBits(minZ) == Double.doubleToLongBits(other.minZ);
	}
}
