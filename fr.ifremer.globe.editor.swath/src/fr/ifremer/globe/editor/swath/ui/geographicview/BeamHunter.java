/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.geographicview;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.NavigationModel;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.event.PositionEvent;
import gov.nasa.worldwind.event.PositionListener;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Beam searcher, according to the mouse position
 */
public class BeamHunter {
	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(BeamHunter.class);

	/** EventBus used by the Swath */
	@Inject
	@Optional
	@Named(ContextNames.NAVIGATION_MODEL)
	protected NavigationModel navigationModel;

	/** Swath's edition parameters */
	@Inject
	@Optional
	@Named(ContextNames.SWATH_EDITOR_PARAMETERS)
	protected SwathEditorParameters parameters;

	/** Service used to access the Nasa view */
	@Inject
	protected IGeographicViewService geographicViewService;

	/** Position listener */
	protected PositionListener positionListener;

	/** Subject that emits a dtm refresh request */
	protected PublishSubject<PositionEvent> refreshPositionSubject = PublishSubject.create();
	/** Subscription on refreshPositionSubject */
	protected Disposable refreshPositionSubscription;

	/**
	 * After construction...
	 */
	@PostConstruct
	public void postConstruct() {
		refreshPositionSubscription = refreshPositionSubject.sample(100l, TimeUnit.MILLISECONDS)
				.observeOn(Schedulers.computation())//
				.subscribe(this::findNereastPoint, //
						error -> logger.warn("Error while refreshing dtm", error)//
				);

		geographicViewService.getWwd().addPositionListener(refreshPositionSubject::onNext);
	}

	@PreDestroy
	void preDestroy() {
		if (positionListener != null) {
			geographicViewService.getWwd().removePositionListener(positionListener);
		}
		refreshPositionSubscription.dispose();
		refreshPositionSubject.unsubscribeOn(Schedulers.computation());
	}

	/**
	 * Get the nearest point on the navigation curve
	 *
	 * @throws GIOException
	 */
	public void findNereastPoint(PositionEvent positionEvent) throws GIOException {
		if (parameters == null || positionEvent.getPosition() == null) {
			geographicViewService.setExtraStatusBarInfo("");
			return;
		}
		double longitude = positionEvent.getPosition().longitude.degrees;
		double latitude = positionEvent.getPosition().latitude.degrees;
		if (!parameters.lonLatGeoBox.contains(longitude, latitude)) {
			geographicViewService.setExtraStatusBarInfo("");
			return;
		}

		long result = 0l;
		double distMin = Double.POSITIVE_INFINITY;
		String nearestLine = null;
		if (navigationModel != null) {
			for (INavigationData navigationDataProxy : navigationModel.getNavigationDataProxies()) {
				for (Integer index : navigationDataProxy) {
					if (navigationDataProxy.isValid(index)) {
						double pLong = navigationDataProxy.getLongitude(index);
						double pLat = navigationDataProxy.getLatitude(index);

						double dist = Math.pow(pLong - longitude, 2) + Math.pow(pLat - latitude, 2);

						if (Double.compare(distMin, dist) > 0) {
							nearestLine = navigationDataProxy.getFileName();
							distMin = dist;
							result = navigationDataProxy.getTime(index);
						}
					}
				}
			}
		}
		if (nearestLine != null)
			updateStatusBar(nearestLine, result);
	}

	protected void updateStatusBar(String filename, long navigationTime) {
		if (navigationTime != 0l) {
			geographicViewService.setExtraStatusBarInfo(FileUtils.getFileShortNameWithExtension(filename) + "  "
					+ DateUtils.formatDate(new Date(navigationTime)));
		} else {
			geographicViewService.setExtraStatusBarInfo("");
		}
	}

}
