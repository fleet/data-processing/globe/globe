package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AmplitudePhaseDetectionShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.BeamNumberShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.BufferRefresher;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.CycleNumberShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.DepthShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.FileColorShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.GreyTextureBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.NormalizedTextureBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.PositionShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.QualityFactorShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.ReflectivityShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.SelectionShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.ShaderBinderComposite;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.ValidityColorsShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.ValidityShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.VisibilityShaderBinder;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.BufferAssociation;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.infrastructure.EmbeddedResources;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.jogl.vertexarray.VertexBufferAttributesGL2x;
import fr.ifremer.globe.ogl.renderer.renderstate.AlphaTestFunction;
import fr.ifremer.globe.ogl.renderer.renderstate.DestinationBlendingFactor;
import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.renderstate.SourceBlendingFactor;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderProgram;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

public class SoundingsComponent extends GraphicComponent {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(SoundingsComponent.class);

	/** List of all BufferRefresher to refresh when model change. */
	protected List<BufferRefresher> vertexBufferRefreshers = new ArrayList<>();

	protected final ArrayList<DrawState> drawStates = new ArrayList<>();
	protected ContextGL2x contextGL2x;

	/** All possible Shader binder indexed in BeamDisplayMode. */
	protected Map<BeamDisplayMode, AShaderBinder> shaderBindersByDisplayMode = new HashedMap<>();

	/**
	 * All possible Shader binder indexed by color (useful when color by file). <br>
	 * Same file order than oundingsModel.getInfoStores()
	 */
	protected List<ShaderBinderComposite> shaderBindersByFile = new ArrayList<>();

	/** A higher-level abstraction for performing OpenGL rendering. */
	protected GLAutoDrawable glAutoDrawable;

	/**
	 * Constructor.
	 */
	public SoundingsComponent() {
		initializeShaderBinders();
	}

	/** Free ressources. */
	@Override
	public void dispose(Context context) {
		disposeDrawStates(context);
		super.dispose(context);
	}

	/** Free ressources. */
	protected void disposeDrawStates(Context context) {
		if (context != null && context.makeCurrent()) {
			// Free previous DrawStates
			drawStates.stream().forEach((drawState) -> drawState.dispose());
			context.release();
		}
		drawStates.clear();
		vertexBufferRefreshers.clear();
	}

	/**
	 * create the component, create drawstate and everything else
	 */
	@Override
	public void init(GLAutoDrawable drawable, ContextGL2x context) {
		glAutoDrawable = drawable;
		contextGL2x = context;
	}

	/** Compute all DrawStates according to the LineBuffers. */
	public void initializeDrawStates() {
		if (contextGL2x.makeCurrent()) {
			disposeDrawStates(contextGL2x);

			try {
				BeamDisplayMode beamDisplayMode = soundingsModel.getDisplayParameters().getDisplayMode();
				if (beamDisplayMode == BeamDisplayMode.FILE) {
					initializeFileDrawStates();
				} else {
					initializeOtherDrawStates();
				}
			} catch (CouldNotCreateVideoCardResourceException e) {
				logger.warn("Unable to create the shader program : ", e);
			} finally {
				contextGL2x.release();
			}
		}
	}

	/**
	 * Compute all DrawStates according to the LineBuffers for a BeamDisplayMode equals to BeamDisplayMode.FILE.
	 */
	protected void initializeFileDrawStates() throws CouldNotCreateVideoCardResourceException {
		// Create a DrawState for each LineBuffer
		BufferAssociation beamsAssociation = soundingsModel.getBeamsAssociation();
		VertexBufferAttributes vertexBufferAttributes = new VertexBufferAttributesGL2x();
		for (int fileidx = 0; fileidx < beamsAssociation.size(); fileidx++) {
			if (soundingsModel.getFileAppearances(fileidx).displayed()) {
				// Which ShaderBinder according expected color.
				ShaderBinderComposite shaderBinder = shaderBindersByFile.get(fileidx);
				// Ask to create the Shader Program
				shaderBinder.clear();
				ShaderProgram shaderProgram = shaderBinder.createShaderProgram();

				List<LineBuffer> buffers = beamsAssociation.getBuffers(fileidx);
				SelectionModel selectionModel = soundingsModel.getSelectionModels().get(fileidx);
				for (LineBuffer lineBuffer : buffers) {
					shaderBinder.bindUniforms(lineBuffer);
					shaderBinder.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes,
							(bufferRefresher) -> vertexBufferRefreshers.add(bufferRefresher));

					VertexArray va = contextGL2x.createVertexArray(vertexBufferAttributes, BufferHint.StaticDraw);

					DrawState drawState = createDrawState(shaderProgram);
					drawState.setVertexArray(va);
					drawStates.add(drawState);
				}
			}
		}
	}

	/**
	 * Compute all DrawStates according to the LineBuffers for a BeamDisplayMode different than BeamDisplayMode.FILE.
	 */
	protected void initializeOtherDrawStates() throws CouldNotCreateVideoCardResourceException {
		// Binders initialization
		AShaderBinder shaderBinder = shaderBindersByDisplayMode
				.get(soundingsModel.getDisplayParameters().getDisplayMode());

		// Ask to create the Shader Program
		shaderBinder.clear();
		ShaderProgram shaderProgram = shaderBinder.createShaderProgram();

		// Create a DrawState for each LineBuffer
		VertexBufferAttributes vertexBufferAttributes = new VertexBufferAttributesGL2x();
		BufferAssociation beamsAssociation = soundingsModel.getBeamsAssociation();
		for (int fileidx = 0; fileidx < beamsAssociation.size(); fileidx++) {
			if (soundingsModel.getFileAppearances(fileidx).displayed()) {
				List<LineBuffer> buffers = beamsAssociation.getBuffers(fileidx);
				SelectionModel selectionModel = soundingsModel.getSelectionModels().get(fileidx);
				for (LineBuffer lineBuffer : buffers) {
					DrawState drawState = createDrawState(shaderProgram);

					shaderBinder.bindUniforms(lineBuffer);
					shaderBinder.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes,
							(bufferRefresher) -> vertexBufferRefreshers.add(bufferRefresher));

					VertexArray va = contextGL2x.createVertexArray(vertexBufferAttributes, BufferHint.StaticDraw);

					drawState.setVertexArray(va);
					drawStates.add(drawState);
				}
			}
		}
	}

	/**
	 * Create a DrawState for the specified.
	 */
	protected DrawState createDrawState(ShaderProgram shaderProgram) {
		DrawState result = new DrawState();
		result.setShaderProgram(shaderProgram);

		// Enable Transparency
		RenderState renderState = new RenderState();
		renderState.getBlending().setEnabled(true);
		renderState.getBlending().setSourceRGBFactor(SourceBlendingFactor.SourceAlpha);
		renderState.getBlending().setDestinationRGBFactor(DestinationBlendingFactor.OneMinusSourceAlpha);
		renderState.getBlending().setSourceAlphaFactor(SourceBlendingFactor.SourceAlpha);
		renderState.getBlending().setDestinationAlphaFactor(DestinationBlendingFactor.OneMinusSourceAlpha);
		renderState.getAlphaTest().setEnabled(false);
		renderState.getAlphaTest().setFunction(AlphaTestFunction.Greater);
		renderState.getAlphaTest().setValue(0.0f);
		renderState.getDepthTest().setEnabled(true);

		result.setRenderState(renderState);

		return result;
	}

	/**
	 * Refreshing Graphic card's memory.
	 */
	public void refreshBuffers() {
		if (contextGL2x.makeCurrent()) {
			try {
				for (BufferRefresher bufferRefresher : vertexBufferRefreshers) {
					bufferRefresher.refresh();
				}
			} finally {
				contextGL2x.release();
			}
		}
	}

	/**
	 * draw the component
	 */
	@Override
	public void draw(GL2 gl, Context context, SceneState sceneState) {
		gl.glPointSize(soundingsModel.getBeamSize());
		for (DrawState drawState : drawStates) {
			context.draw(PrimitiveType.Points, drawState, sceneState);
		}
	}

	/**
	 * Initialize the ShaderBinders
	 */
	protected void initializeShaderBinders() {

		String fragmentShader = getShaderSource("DefaultFragmentShader.glsl");
		String rangeVertexShader = getShaderSource("RangeVertexShader.glsl");
		String textureFragmentShader = getShaderSource("TextureFragmentShader.glsl");

		SelectionShaderBinder selectionShaderBinder = new SelectionShaderBinder();
		selectionShaderBinder.setNotifyCreationOfVertexBuffers(true);

		ValidityShaderBinder validityShaderBinder = new ValidityShaderBinder();
		validityShaderBinder.setNotifyCreationOfVertexBuffers(true);

		VisibilityShaderBinder visibilityShaderBinder = new VisibilityShaderBinder();
		PositionShaderBinder positionShaderBinder = new PositionShaderBinder();
		positionShaderBinder.setNotifyCreationOfVertexBuffers(true);

		NormalizedTextureBinder normalizedTextureBinder = new NormalizedTextureBinder();

		// VALID_INVALID shader
		shaderBindersByDisplayMode.put(BeamDisplayMode.VALID_INVALID,
				new ShaderBinderComposite(getShaderSource("ValidityVertexShader.glsl"), fragmentShader, //
						visibilityShaderBinder, positionShaderBinder, validityShaderBinder,
						new ValidityColorsShaderBinder(), selectionShaderBinder));

		// REFLECTIVITY shader
		shaderBindersByDisplayMode.put(BeamDisplayMode.REFLECTIVITY,
				new ShaderBinderComposite(rangeVertexShader, textureFragmentShader, //
						visibilityShaderBinder, positionShaderBinder, validityShaderBinder, new GreyTextureBinder(),
						new ReflectivityShaderBinder(), selectionShaderBinder));

		// DEPTH shader
		shaderBindersByDisplayMode.put(BeamDisplayMode.DEPTH,
				new ShaderBinderComposite(rangeVertexShader, textureFragmentShader, //
						visibilityShaderBinder, positionShaderBinder, validityShaderBinder, normalizedTextureBinder,
						new DepthShaderBinder(), selectionShaderBinder));

		// Cycle Number shader
		shaderBindersByDisplayMode.put(BeamDisplayMode.CYCLE_NUMBER,
				new ShaderBinderComposite(rangeVertexShader, textureFragmentShader, //
						visibilityShaderBinder, positionShaderBinder, validityShaderBinder, normalizedTextureBinder,
						new CycleNumberShaderBinder(), selectionShaderBinder));

		// Beam Number shader
		shaderBindersByDisplayMode.put(BeamDisplayMode.BEAM_NUMBER,
				new ShaderBinderComposite(rangeVertexShader, textureFragmentShader, //
						visibilityShaderBinder, positionShaderBinder, validityShaderBinder, normalizedTextureBinder,
						new BeamNumberShaderBinder(), selectionShaderBinder));

		// Amplitude Phase Detection shader
		shaderBindersByDisplayMode.put(BeamDisplayMode.DETECTION_TYPE,
				new ShaderBinderComposite(getShaderSource("AmplitudePhaseDetectionVertexShader.glsl"), fragmentShader, //
						visibilityShaderBinder, positionShaderBinder, validityShaderBinder,
						new AmplitudePhaseDetectionShaderBinder(), selectionShaderBinder));

		// REFLECTIVITY shader
		shaderBindersByDisplayMode.put(BeamDisplayMode.QUALITY_FACTOR,
				new ShaderBinderComposite(rangeVertexShader, textureFragmentShader, //
						visibilityShaderBinder, positionShaderBinder, validityShaderBinder, normalizedTextureBinder,
						new QualityFactorShaderBinder(), selectionShaderBinder));

		// BeamDisplayMode.FILE : one shader per file
		List<ISounderNcInfo> infoStores = soundingsModel.getInfoStores();
		for (int fileIdx = 0; fileIdx < infoStores.size(); fileIdx++) {
			shaderBindersByFile.add(//
					new ShaderBinderComposite(//
							getShaderSource("FileVertexShader.glsl"), //
							fragmentShader, //
							visibilityShaderBinder, //
							positionShaderBinder, //
							validityShaderBinder, //
							new FileColorShaderBinder(fileIdx), //
							selectionShaderBinder));
		}
	}

	/**
	 * @return path to Shader.
	 */
	protected String getShaderSource(String vertexShaderName) {

		return EmbeddedResources.getText(this.getClass(), "shader/soundingviewer/" + vertexShaderName);
	}

}
