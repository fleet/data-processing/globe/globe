package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

import org.eclipse.e4.core.di.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;

public abstract class GraphicComponent {
	protected static final Logger logger = LoggerFactory.getLogger(GraphicComponent.class);

	private boolean visible = true;

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;

	/** model of the soundings view */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	/** ViewPoint */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_VIEW_POINT)
	protected ViewPoint viewPoint;

	/** Constructor */
	public GraphicComponent() {
		ContextInitializer.inject(this);
	}

	/**
	 * create the component, create drawstate and everything else
	 */
	public abstract void init(GLAutoDrawable drawable, ContextGL2x context);

	/**
	 * draw the component
	 */
	public abstract void draw(GL2 gl, Context context, SceneState sceneState);

	/**
	 * Component visible ?
	 */
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public void dispose(Context context) {
		soundingsModel = null;
	}
}
