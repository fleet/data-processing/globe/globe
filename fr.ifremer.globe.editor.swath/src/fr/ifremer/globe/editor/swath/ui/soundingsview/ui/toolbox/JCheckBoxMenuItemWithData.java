package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox;

import javax.swing.JCheckBoxMenuItem;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class JCheckBoxMenuItemWithData extends JCheckBoxMenuItem {

	private static final long serialVersionUID = 1L;
	private Button button;
	private Object object;

	public JCheckBoxMenuItemWithData(String string, Composite composite) {
		super(string);

		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				JCheckBoxMenuItemWithData.this.button = new Button(composite, SWT.NONE);
				JCheckBoxMenuItemWithData.this.button.setVisible(false);
			}
		});
	}

	/**
	 * @return the data
	 */
	public Object getData(String key) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				JCheckBoxMenuItemWithData.this.object=JCheckBoxMenuItemWithData.this.button.getData(key);
			}
		});
		return this.object;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String value, Object data) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				JCheckBoxMenuItemWithData.this.button.setData(value, data);
			}
		});
	}
}
