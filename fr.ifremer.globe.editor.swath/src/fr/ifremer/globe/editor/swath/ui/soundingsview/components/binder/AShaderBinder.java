/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.util.function.Consumer;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderProgram;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Abstract class of shader binders.
 */
public abstract class AShaderBinder {

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;

	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingModel;

	/** Indicate if this ShaderBinder informs the creation of VertexBuffer. */
	boolean notifyCreationOfVertexBuffers = false;

	/**
	 * The ShaderProgram built from Vertex and Fragment Shaders managed by this binder.
	 */
	protected ShaderProgram shaderProgram;

	/** Constructor */
	public AShaderBinder() {
		ContextInitializer.inject(this);
	}

	/**
	 * Bind Uniforms.
	 */
	public void bindUniforms(LineBuffer lineBuffer) {
		// Nothing to bind by default
	}

	/**
	 * Bind attributes.
	 * 
	 * @param listener is notified when a {@link VertexBuffer} is created. May be null.
	 */
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		// Nothing to bind by default
	}

	/**
	 * @return the {@link #shaderProgram} @
	 */
	public ShaderProgram createShaderProgram() throws CouldNotCreateVideoCardResourceException {
		return getShaderProgram();
	}

	/** Release all ressources. */
	public void clear() {
		shaderProgram = null;
	}

	/**
	 * @return the {@link #shaderProgram}
	 */
	public ShaderProgram getShaderProgram() {
		return shaderProgram;
	}

	/**
	 * @param shaderProgram the {@link #shaderProgram} to set
	 */
	protected void setShaderProgram(ShaderProgram shaderProgram) {
		this.shaderProgram = shaderProgram;
	}

	/**
	 * @return an initialized ShaderProgram. @
	 */
	protected ShaderProgram initializeShaderProgram() {
		return null;
	}

	/**
	 * Notify the VertexBuffer creation.
	 */
	protected void alertVertexBufferCreated(Consumer<BufferRefresher> listener, BufferRefresher bufferRefresher) {
		if (isNotifyCreationOfVertexBuffers() && listener != null) {
			listener.accept(bufferRefresher);
		}
	}

	/**
	 * @return the {@link #notifyCreationOfVertexBuffers}
	 */
	public boolean isNotifyCreationOfVertexBuffers() {
		return notifyCreationOfVertexBuffers;
	}

	/**
	 * @param notifyCreationOfVertexBuffers the {@link #notifyCreationOfVertexBuffers} to set
	 */
	public void setNotifyCreationOfVertexBuffers(boolean notifyCreationOfVertexBuffers) {
		this.notifyCreationOfVertexBuffers = notifyCreationOfVertexBuffers;
	}

}
