/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.geographicview;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;

import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.geometry.CartesianPoint;
import fr.ifremer.globe.core.model.geometry.OrientedBox;
import fr.ifremer.globe.editor.swath.application.context.BathySelectionMode;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ogl.core.vectors.Vector2D;
import fr.ifremer.globe.ui.events.swath.ShiftingDirection;
import fr.ifremer.globe.ui.events.swath.SwathEventTopics;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWQuadSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import gov.nasa.worldwind.geom.Angle;

/**
 * Manages the shifting of the selection in DTM layer
 *
 */
public class SelectionShifter {

	/** model of the soundings view */
	/** Injected after SoundingsModel is initialized */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingModel;

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;

	@Inject
	@Optional
	@Named(ContextNames.BATHY_SELECTION_MODE)
	protected BathySelectionMode bathySelectionMode;

	@Inject
	protected IGeographicViewService geographicViewService;

	/**
	 * Layer to manage quad selection.
	 */
	protected IWWQuadSelectionLayer quadSelectionLayer;
	protected IWWSwathSelectionLayer swathSelectionLayer;

	protected KeyListener keyListener;

	/**
	 * Hook the key listener on Viewer3D
	 */
	@PostConstruct
	void postConstruct() {
		keyListener = new KeyAdapter() {
			/**
			 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
			 */
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == 'z') {
					shiftDTMSelection(ShiftingDirection.NORTH);
					e.consume();
				} else if (e.getKeyChar() == 's') {
					shiftDTMSelection(ShiftingDirection.SOUTH);
					e.consume();
				} else if (e.getKeyChar() == 'q') {
					shiftDTMSelection(ShiftingDirection.WEST);
					e.consume();
				} else if (e.getKeyChar() == 'd') {
					shiftDTMSelection(ShiftingDirection.EAST);
					e.consume();
				}
			}
		};
		geographicViewService.getWwd().addKeyListener(keyListener);
	}

	@PreDestroy
	void preDestroy() {
		if (keyListener != null) {
			geographicViewService.getWwd().removeKeyListener(keyListener);
		}
	}

	/** Process the shifting selection event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	void onShiftSelectionEvent(@UIEventTopic(SwathEventTopics.SHIFT_DTM_SELECTION) ShiftingDirection direction) {
		if (bathySelectionMode == BathySelectionMode.RECTANGLE) {
			shiftDTMSelection(direction);
		} else {
			shiftSwathSelection(direction);
		}
	}

	/**
	 * Process the shifting of the selection according to the direction
	 */
	protected void shiftSwathSelection(ShiftingDirection direction) {
		swathSelectionLayer
				.shiftSelection(direction == ShiftingDirection.SOUTH || direction == ShiftingDirection.WEST ? -1 : 1);
	}

	/**
	 * Process the shifting of the selection according to the direction
	 */
	protected void shiftDTMSelection(ShiftingDirection direction) {
		OrientedBox currentDTMSelection = soundingModel.getSelectedArea();
		if (currentDTMSelection != null && !quadSelectionLayer.isSelectionArmed()) {
			List<CartesianPoint> listPoints = currentDTMSelection.getPoints();
			CartesianPoint p1 = listPoints.get(0);
			CartesianPoint p2 = listPoints.get(1);
			CartesianPoint p3 = listPoints.get(2);
			CartesianPoint p4 = listPoints.get(3);

			Vector2D pointA = new Vector2D(p1.getX(), p1.getY());
			Vector2D pointB = new Vector2D(p2.getX(), p2.getY());
			Vector2D pointC = new Vector2D(p3.getX(), p3.getY());
			Vector2D pointD = new Vector2D(p4.getX(), p4.getY());

			Vector2D ab = pointB.subtract(pointA);
			Vector2D bc = pointC.subtract(pointB);

			// first find the up direction
			// we compute the projection of vector normalized to find the one
			// the closest to the up direction
			Angle northAngle = geographicViewService.getWwd().getView().getHeading();
			Vector2D north = new Vector2D(northAngle.sin(), northAngle.cos());
			Angle eastAngle = northAngle.subtract(Angle.NEG90);
			Vector2D east = new Vector2D(eastAngle.sin(), eastAngle.cos());
			double abDir = Math.abs(ab.normalize().dot(north));
			double bcDir = Math.abs(bc.normalize().dot(north));

			Vector2D mainAxis = null;
			Vector2D secondAxis = null;
			if (abDir > bcDir) {
				mainAxis = ab;
				secondAxis = bc;
			} else {
				mainAxis = bc;
				secondAxis = ab;
			}
			Vector2D displacement = null;
			// Read values from the configuration
			double overlap = dtmPreferences.getOverlap().getValue();
			switch (direction) {
			case SOUTH:
				displacement = mainAxis.multiply(1. - overlap / 100.);
				if (mainAxis.dot(north) > 0)
					displacement = displacement.multiply(-1);
				break;
			case WEST:
				displacement = secondAxis.multiply(1. - overlap / 100.);
				if (secondAxis.dot(east) < 0)
					displacement = displacement.multiply(-1);
				displacement = displacement.multiply(-1);
				break;
			case EAST:
				displacement = secondAxis.multiply(1. - overlap / 100.);
				if (secondAxis.dot(east) < 0)
					displacement = displacement.multiply(-1);
				break;
			case NORTH:
			default:
				displacement = mainAxis.multiply(1. - overlap / 100.);
				if (mainAxis.dot(north) > 0)
					displacement = displacement.multiply(-1);
				displacement = displacement.multiply(-1);
				break;
			}
			Vector2D newPointA = pointA.add(displacement);
			Vector2D newPointB = pointB.add(displacement);
			Vector2D newPointC = pointC.add(displacement);
			Vector2D newPointD = pointD.add(displacement);

			GeoPoint geoPoint1 = new GeoPoint(newPointA.getX(), newPointA.getY());
			GeoPoint geoPoint2 = new GeoPoint(newPointB.getX(), newPointB.getY());
			GeoPoint geoPoint3 = new GeoPoint(newPointC.getX(), newPointC.getY());
			GeoPoint geoPoint4 = new GeoPoint(newPointD.getX(), newPointD.getY());
			quadSelectionLayer.setSelection(geoPoint1, geoPoint2, geoPoint3, geoPoint4);

			geographicViewService.refresh();
		}
	}

	/**
	 * @return the {@link #quadSelectionLayer}
	 */
	public IWWQuadSelectionLayer getQuadSelectionLayer() {
		return quadSelectionLayer;
	}

	/**
	 * @param quadSelectionLayer the {@link #quadSelectionLayer} to set
	 */
	public void setQuadSelectionLayer(IWWQuadSelectionLayer quadSelectionLayer) {
		this.quadSelectionLayer = quadSelectionLayer;
	}

	/**
	 * @return the {@link #swathSelectionLayer}
	 */
	public IWWSwathSelectionLayer getSwathSelectionLayer() {
		return swathSelectionLayer;
	}

	/**
	 * @param swathSelectionLayer the {@link #swathSelectionLayer} to set
	 */
	public void setSwathSelectionLayer(IWWSwathSelectionLayer swathSelectionLayer) {
		this.swathSelectionLayer = swathSelectionLayer;
	}
}
