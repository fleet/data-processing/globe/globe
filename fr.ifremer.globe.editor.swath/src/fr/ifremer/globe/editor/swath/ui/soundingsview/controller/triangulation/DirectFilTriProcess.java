package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.triangulation;

import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;
import fr.ifremer.globe.core.processes.filtri.model.IFilTriSoundingsProxy;
import fr.ifremer.globe.core.processes.filtri.model.IFiltSoundingsProvider;
import fr.ifremer.globe.core.processes.filtri.process.AbstractFilter;
import fr.ifremer.globe.core.processes.filtri.process.FilterFactory;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering.SimpleParameterDialog;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.service.IAlgoPoolService;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.perfcounter.PerfoCounter;

/**
 * Process to apply the FilTri algo on a specified set of IFilTriSoundings.
 */
public class DirectFilTriProcess implements IProcessFunction {

	// progress tasks weightings
	protected static final int WORK_LOAD = 100;
	protected static final int WORK_SAVE = 100;

	/** Maximum number of sounds in each cycle. */
	public static final int MAX_SOUNDS_BY_CYCLE = 880;

	/** Filtering parameters. */
	protected FiltTriParameters parameters;

	/** Factory of Filter. */
	protected FilterFactory filterFactory = new FilterFactory();

	/** Provider of a set of FiltSoundings */
	protected IFiltSoundingsProvider filtSoundingsProvider;

	/** Parameter dialog. */
	protected SimpleParameterDialog dialog;

	/** How many sounds has been filtered */
	protected int filteredNbSounds;

	/**
	 * Default constructor.
	 */
	public DirectFilTriProcess(Shell parentShell, IFiltSoundingsProvider filtSoundingsProvider) {
		parameters = new FiltTriParameters();
		this.filtSoundingsProvider = filtSoundingsProvider;
		dialog = new SimpleParameterDialog(parameters, parentShell);
	}

	/**
	 * Process.
	 * 
	 * @throws Exception
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException, ProjectionException {
		PerfoCounter perfMain = new PerfoCounter("filtri", "globe.filtri");
		perfMain.start();
		int totalNbSounds = 0;
		filteredNbSounds = 0;

		// Init progression.
		monitor.setTaskName(parameters.getTitle());
		monitor.beginTask("Filtering by triangulation running ...",
				(WORK_LOAD + AbstractFilter.WORK_FILTER + WORK_SAVE));

		Iterator<IFilTriSoundingsProxy> filtSoundingsIterator = filtSoundingsProvider.getSoundingsIterator(
				parameters.getIntProperty(FiltTriParameters.PROPERTY_SLICE_SIZE) * MAX_SOUNDS_BY_CYCLE);

		AbstractFilter filter = filterFactory.getFilter(monitor, getParameters());
		while (filtSoundingsIterator.hasNext() && !monitor.isCanceled()) {
			IFilTriSoundingsProxy sounds = filtSoundingsIterator.next();
			try {
				if (!monitor.isCanceled()) {
					totalNbSounds += sounds.size();
					logger.info("Processing {} sounds", sounds.size());
					filter.apply(sounds);
					filteredNbSounds += filter.getNbFilteredSounds();
				}
			} finally {
				IOUtils.closeQuietly(sounds);
				IAlgoPoolService.grab().resetAll();
			}
		}

		logger.info("Number of invalidated soundings : {} on {}", filteredNbSounds, totalNbSounds);

		perfMain.stop().print();

		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
	}

	/**
	 * @return the {@link #parameters}
	 */
	public FiltTriParameters getParameters() {
		return parameters;
	}

	/**
	 * @return the {@link #dialog}
	 */
	public SimpleParameterDialog getDialog() {
		return dialog;
	}

	/** Getter of {@link #filteredNbSounds} */
	public int getFilteredNbSounds() {
		return filteredNbSounds;
	}
}
