package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;

import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SVDisplayParameterChanged;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.BeamVisibleMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SelectionMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SoundingsSceneMode;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

public class DisplayParameters {

	private BeamVisibleMode visibleMode = BeamVisibleMode.ALL;
	private BeamDisplayMode displayMode = BeamDisplayMode.VALID_INVALID;
	private boolean keepInvalidBeamInRed = false;
	private SelectionMode selectionMode = SelectionMode.QUAD;
	private SoundingsSceneMode sceneMode = SoundingsSceneMode.SELECT;
	private boolean autoRefreshDTM = false;

	/** Eclipse event broker service */
	@Inject
	protected IEventBroker eventBroker;

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;

	@Inject
	protected IEclipseContext eclipseContext;

	/**
	 * Mandatory initialization after construction.
	 */
	@PostConstruct
	protected void initialization() {
		visibleMode = (BeamVisibleMode) dtmPreferences.getBeamVisibleMode().getValue();
		displayMode = (BeamDisplayMode) dtmPreferences.getBeamDisplayMode().getValue();
		keepInvalidBeamInRed = dtmPreferences.getKeepInvalidBeamInRed().getValue();
		sceneMode = (SoundingsSceneMode) dtmPreferences.getSoundingSceneMode().getValue();
		selectionMode = (SelectionMode) dtmPreferences.getSelectionMode().getValue();
		autoRefreshDTM = dtmPreferences.getAutoRefreshDTM().getValue().booleanValue();
	}

	public BeamDisplayMode getDisplayMode() {
		return displayMode;
	}

	public void setDisplayMode(BeamDisplayMode displayMode) {
		if (this.displayMode != displayMode) {
			this.displayMode = displayMode;
			dtmPreferences.getBeamDisplayMode().setValue(displayMode, true);
			dtmPreferences.save();
			eclipseContext.set(ContextNames.BEAM_DISPLAY_MODE, displayMode);
		}
	}

	public void setKeepInvalidBeamInRed(boolean keepInvalidBeamInRed) {
		if (this.keepInvalidBeamInRed != keepInvalidBeamInRed) {
			this.keepInvalidBeamInRed = keepInvalidBeamInRed;
			dtmPreferences.getKeepInvalidBeamInRed().setValue(keepInvalidBeamInRed, true);
			dtmPreferences.save();
			eclipseContext.set(ContextNames.KEEP_INVALID_BEAM_IN_RED, keepInvalidBeamInRed);
		}

	}

	public void setVisibleMode(BeamVisibleMode visibleMode) {
		if (this.visibleMode != visibleMode) {
			this.visibleMode = visibleMode;
			dtmPreferences.getBeamVisibleMode().setValue(visibleMode, true);
			dtmPreferences.save();
			eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eVisibleMode);
		}
	}

	public void setSelectionMode(SelectionMode selectionMode) {
		if (this.selectionMode != selectionMode) {
			this.selectionMode = selectionMode;
			dtmPreferences.getSelectionMode().setValue(selectionMode, true);
			dtmPreferences.save();
			eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eSelectionMode);
		}
	}

	public void setAutoRefreshDTM(boolean autoRefreshDTM) {
		if (this.autoRefreshDTM != autoRefreshDTM) {
			this.autoRefreshDTM = autoRefreshDTM;
			dtmPreferences.getAutoRefreshDTM().setValue(autoRefreshDTM, true);
			dtmPreferences.save();
		}
	}

	public void setSceneMode(SoundingsSceneMode sceneMode) {
		if (this.sceneMode != sceneMode) {
			this.sceneMode = sceneMode;
			dtmPreferences.getSoundingSceneMode().setValue(this.sceneMode, true);
			dtmPreferences.save();
			eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eSceneMode);
		}
	}

	/**
	 * @param boundingCubeStateText the boundingCubeStateText to set
	 */
	public void setMetricsState(boolean boundingCubeStateText) {
		dtmPreferences.getMetricsText().setValue(boundingCubeStateText, true);
		dtmPreferences.save();
		eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eWidgetMode);
	}

	/**
	 * @param orientedBoundingCubeState the orientedBoundingCubeState to set
	 */
	public void setOrientedBoundingCubeState(boolean orientedBoundingCubeState) {
		dtmPreferences.getOrientedBoundingCube().setValue(orientedBoundingCubeState, true);
		dtmPreferences.save();
		eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eWidgetMode);
	}

	/**
	 * @param soundingsState the soundingsState to set
	 */
	public void setSoundingsState(boolean soundingsState) {
		dtmPreferences.getSoundings().setValue(soundingsState, true);
		dtmPreferences.save();
		eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eWidgetMode);
	}

	/**
	 * @param landmarkState the landmarkState to set
	 */
	public void setLandmarkState(boolean landmarkState) {
		dtmPreferences.getLandmark().setValue(landmarkState, true);
		dtmPreferences.save();
		eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eWidgetMode);
	}

	/**
	 * @param statisticsState the statisticsState to set
	 */
	public void setStatisticsState(boolean statisticsState) {
		dtmPreferences.getStatistics().setValue(statisticsState, true);
		dtmPreferences.save();
		eventBroker.send(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED, SVDisplayParameterChanged.eWidgetMode);
	}

	public BeamVisibleMode getVisibleMode() {
		return visibleMode;
	}

	/**
	 * @return the {@link #selectionMode}
	 */
	public SelectionMode getSelectionMode() {
		return selectionMode;
	}

	/**
	 * @return the {@link #autoRefreshDTM}
	 */
	public boolean isAutoRefreshDTM() {
		return autoRefreshDTM;
	}

	/**
	 * @return the {@link #sceneMode}
	 */
	public SoundingsSceneMode getSceneMode() {
		return sceneMode;
	}

	/**
	 * @return the {@link #keepInvalidBeamInRed}
	 */
	public boolean isKeepInvalidBeamInRed() {
		return keepInvalidBeamInRed;
	}
}
