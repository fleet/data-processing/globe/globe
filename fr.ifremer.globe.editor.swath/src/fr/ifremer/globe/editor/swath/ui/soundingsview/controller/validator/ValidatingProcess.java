/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.validator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.LongPredicate;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.processes.detectionfilter.Attributes;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.FilterParameterChecker;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.action.SoundEditorAction;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.BufferAssociation;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Implementation of {@link IFilteringProcess} for the Swath editor.
 */
public class ValidatingProcess implements IProcessFunction {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(ValidatingProcess.class);

	/** Controler. */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_CONTROLLER)
	protected SoundingsController soundingsControler;

	@Inject
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	/** Eclipse event broker service */
	@Inject
	protected IEventBroker eventBroker;

	/** Provider of attributes. */
	protected AttributeProvider attributeProvider;

	private final List<SingleFilterParameters> criterias;
	private final DataParameters dataToSet;

	/**
	 * Constructor.
	 */
	public ValidatingProcess(AttributeProvider attributeProvider, List<SingleFilterParameters> criterias,
			DataParameters dataToSet) {
		this.attributeProvider = attributeProvider;
		this.criterias = criterias;
		this.dataToSet = dataToSet;
		ContextInitializer.inject(this);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.imagery.script.IFilteringProcess#accept(java.util.List,
	 *      fr.ifremer.globe.imagery.script.parameter.DataParameters)
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		int designatedBeamCount = 0;
		List<SelectionModel> designateModels = null;
		try {
			var ranges = new HashMap<PredefinedLayers<?>, Set<DoubleRange>>();
			String error = FilterParameterChecker.checkSelectParameters(criterias, dataToSet.isCheckAllCriterias(),
					attributeProvider, ranges, logger);
			if (!error.isEmpty())
				throw new GIOException("Beams validator - bad criterias :  " + error);

			if (!ranges.isEmpty()) {
				designateModels = designateBeams(ranges, dataToSet.isCheckAllCriterias());
				// Only select beams ?
				if (dataToSet.getSelectedAttribute() == Attributes.SELECTION) {
					designatedBeamCount = soundingsControler.selectBeams(designateModels,
							dataToSet.getValue() == Attributes.SELECTION_YES);
				} else if (dataToSet.getSelectedAttribute() == Attributes.VALIDITY) {
					if (dataToSet.getValue() == Attributes.VALIDITY_VALID) {
						designatedBeamCount = soundingsControler.validateBeams(designateModels,
								MappingValidity.SND_VALID);
						if (designatedBeamCount > 0) {
							eventBroker.send(EventTopics.SOUND_ACTION, new SoundEditorAction("validation process"));
						}
					} else {
						designatedBeamCount = soundingsControler.validateBeams(designateModels,
								MappingValidity.SND_UNVALID_MANUAL);
						if (designatedBeamCount > 0) {
							eventBroker.send(EventTopics.SOUND_ACTION, new SoundEditorAction("invalidation process"));
						}
					}
				}
				int result = designatedBeamCount;
				Display.getDefault().asyncExec(() -> displayResult(dataToSet, result));
			}

			return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
		} catch (IOException e) {
			throw new GIOException("Beams validator error :  " + e.getMessage(), e);
		} finally {
			// Clean
			if (designateModels != null) {
				for (SelectionModel designateModel : designateModels) {
					designateModel.close();
				}
			}
		}
	}

	/**
	 * Designate some beams according to the criterias.
	 *
	 * @return a list of SelectionModel containing designated beams. The list is ordered by file index.
	 */
	protected List<SelectionModel> designateBeams(Map<PredefinedLayers<?>, Set<DoubleRange>> criterias,
			boolean allCriterias) throws IOException {
		List<SelectionModel> result = new ArrayList<>();
		BufferAssociation bufferAssociation = soundingsModel.getBeamsAssociation();
		for (int fileIndex = 0; fileIndex < bufferAssociation.size(); fileIndex++) {
			// Build a specific SelectionModel to identify which beam to manage
			Mappings mappings = bufferAssociation.getSpatialIndex(fileIndex).getMappings();
			SelectionModel selectionModel = soundingsModel.getSelectionModels().get(fileIndex);
			SelectionModel designationModel = ContextInitializer.make(SelectionModel.class);
			designationModel.initSelectionArray(mappings);
			result.add(designationModel);

			List<LineBuffer> buffers = bufferAssociation.getBuffers(fileIndex);
			buffers.stream().forEach(lineBuffer -> processDesignationOneLineBuffer(lineBuffer, selectionModel,
					designationModel, criterias, allCriterias));
		}
		return result;
	}

	/** Select some beams on one LineBuffer according to the criterias. */
	protected void processDesignationOneLineBuffer(LineBuffer lineBuffer, SelectionModel selectionModel,
			SelectionModel designationModel, Map<PredefinedLayers<?>, Set<DoubleRange>> criterias,
			boolean allCriterias) {
		Mappings mappings = lineBuffer.getSpatialIndex().getMappings();

		// First step : select beams according first criteria
		// then : update selected beam (allCriterias = true) OR continue to select beams with other criteria
		// (allCriterias = false)
		boolean checkIfAlreadySelected = false;

		for (var criteria : criterias.entrySet()) {
			PredefinedLayers<?> attribute = criteria.getKey();
			for (DoubleRange range : criteria.getValue()) {
				if (attribute == Attributes.PING_INDEX) {
					processDesignation(lineBuffer, designationModel,
							offsetInFile -> range.containsInteger(mappings.getCycleIndex(offsetInFile)),
							checkIfAlreadySelected);
				} else if (attribute == Attributes.BEAM_INDEX) {
					processDesignation(lineBuffer, designationModel,
							offsetInFile -> range.containsInteger(mappings.getBeamIndex(offsetInFile)),
							checkIfAlreadySelected);
				} else if (attribute == Attributes.DEPTH) {
					processDesignation(lineBuffer, designationModel,
							offsetInFile -> range.containsFloat(mappings.getDepth(offsetInFile)),
							checkIfAlreadySelected);
				} else if (attribute == Attributes.REFLECTIVITY) {
					processDesignation(lineBuffer, designationModel,
							offsetInFile -> range.containsFloat(mappings.getReflectivity(offsetInFile)),
							checkIfAlreadySelected);
				} else if (attribute == Attributes.AMPLITUDE_PHASE_DETECTION) {
					processDesignation(lineBuffer, designationModel,
							offsetInFile -> range.containsInteger(mappings.getDetection(offsetInFile)),
							checkIfAlreadySelected);
				} else if (attribute == Attributes.SCALAR_QUALITY_FACTOR) {
					processDesignation(lineBuffer, designationModel,
							offsetInFile -> range.containsFloat(mappings.getQualityFactor(offsetInFile)),
							checkIfAlreadySelected);
				} else if (attribute == Attributes.SELECTION) {
					boolean selected = range.getMinimumFloat() == NumberUtils.bool2byte(true);
					processDesignation(lineBuffer, designationModel,
							offsetInFile -> selectionModel.isSelected(offsetInFile) == selected,
							checkIfAlreadySelected);
				} else if (attribute == Attributes.VALIDITY) {
					if (range.getMinimumFloat() == NumberUtils.bool2byte(true)) {
						processDesignation(lineBuffer, designationModel,
								offsetInFile -> MappingValidity.isValid(mappings.getValidity(offsetInFile)),
								checkIfAlreadySelected);
					} else {
						processDesignation(lineBuffer, designationModel,
								offsetInFile -> MappingValidity.isInvalid(mappings.getValidity(offsetInFile)),
								checkIfAlreadySelected);
					}
				}

				checkIfAlreadySelected = allCriterias;
			}
		}
	}

	/**
	 * Select some beams on one LineBuffer where attribute ValidBathy is matching with specified range.
	 *
	 * @param unselect true to consider only the selected beams.
	 */
	protected void processDesignation(LineBuffer lineBuffer, SelectionModel designationModel, LongPredicate predicate,
			boolean checkIfAlreadySelected) {
		int nbBeams = lineBuffer.getDetectionCount();
		for (int offset = 0; offset < nbBeams; offset++) {
			long offsetInFile = lineBuffer.getOffsetInFile() + offset;

			// case 1 : select or unselect items which are already selected
			if (checkIfAlreadySelected && designationModel.isSelected(offsetInFile)) {
				designationModel.setSelected(offsetInFile, predicate.test(offsetInFile));
			}

			// case 2 : select all selectable
			if (!checkIfAlreadySelected && predicate.test(offsetInFile)) {
				designationModel.setSelected(offsetInFile, true);
			}
		}
	}

	/**
	 * Show the result
	 */
	protected void displayResult(DataParameters dataToSet, int designatedBeamCount) {
		String message = designatedBeamCount + " beams have been ";
		if (designatedBeamCount == 0) {
			message = "No beam ";
		} else if (designatedBeamCount == 1) {
			message = "One beam has been ";
		}

		if (dataToSet.getSelectedAttribute() == Attributes.SELECTION) {
			boolean valueToSet = dataToSet.getValue() == NumberUtils.bool2byte(true);
			message += valueToSet ? "selected" : "unselected";
		} else {
			// lines in comments to avoid bug : TODO need to be checked!
			// boolean valueToSet = (int) dataToSet.getValue() == MappingValidity.SND_VALID.getValue();
			// message += valueToSet ? "validated" : "invalidated";
			message += (int) dataToSet.getValue() == 1 ? "validated" : "invalidated";
		}
		MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Beams validator", message);
		logger.debug(message);
	}

}
