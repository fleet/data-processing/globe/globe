/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

/**
 * Binder for uniforms "u_min" and "u_max".
 */
public class MinMaxShaderBinder<T> extends AShaderBinder {

	/** Minimum value. */
	protected T minValue;
	/** Maximum value. */
	protected T maxValue;

	protected MinMaxShaderBinder(T minValue, T maxValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		Uniform<T> min = (Uniform<T>) getShaderProgram().getUniforms().get("u_min");
		min.setValue(this.minValue);

		Uniform<T> max = (Uniform<T>) getShaderProgram().getUniforms().get("u_max");
		max.setValue(this.maxValue);
	}
}
