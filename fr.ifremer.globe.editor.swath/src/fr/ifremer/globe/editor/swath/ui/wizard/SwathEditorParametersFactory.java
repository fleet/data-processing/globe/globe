package fr.ifremer.globe.editor.swath.ui.wizard;

import java.util.List;

import fr.ifremer.globe.core.model.dtm.SounderDataDtmGeoboxComputer;
import fr.ifremer.globe.core.model.dtm.SounderDataDtmResolutionComputer;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Factory of SwathEditorParameters
 */
public class SwathEditorParametersFactory {

	/** Constructor */
	private SwathEditorParametersFactory() {
	}

	/**
	 * Initialize a SwathEditorParameters from some sounding files
	 */
	public static SwathEditorParameters create(List<ISounderNcInfo> editedFiles) throws GException {
		SounderDataDtmGeoboxComputer geoboxComputer = new SounderDataDtmGeoboxComputer(editedFiles);
		GeoBox geoBox = geoboxComputer.computeGeoboxWithMask(SonarDetectionStatus.EDITABLE_DETECTION_MASK_BYTE);

		GeoPoint center = geoBox.getCenter();
		Projection mercatorProjection = new Projection(
				StandardProjection.getTransverseMercatorProjectionSettings(center.getLong(), center.getLat()));

		SounderDataDtmResolutionComputer computer = new SounderDataDtmResolutionComputer(editedFiles, mercatorProjection);
		double spatialResolution = computer.computeGridDefaultResolution();
		GeoBox mercatorGeoBox = geoBox.project(mercatorProjection);
		return new SwathEditorParameters(editedFiles, geoBox, mercatorProjection, mercatorGeoBox, spatialResolution);

	}
}
