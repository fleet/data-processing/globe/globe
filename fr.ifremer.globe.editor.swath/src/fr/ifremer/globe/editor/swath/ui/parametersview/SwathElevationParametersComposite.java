package fr.ifremer.globe.editor.swath.ui.parametersview;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

/**
 * Composite that changes contrast and shading of ShaderElevationLayer.
 */
public class SwathElevationParametersComposite extends Composite {

	/** Observer */
	protected SwathElevationParametersObserver swathElevationParametersObserver;

	/** Scale */
	protected Spinner overFlowScale;

	/**
	 * The maximum number of pixels to search in all directions to find values
	 * to interpolate from.
	 */
	protected Spinner maxDistance;

	/**
	 * Constructor.
	 */
	public SwathElevationParametersComposite(Composite parent, SwathElevationParametersObserver swathElevationParametersObserver) {
		super(parent, SWT.NONE);
		this.swathElevationParametersObserver = swathElevationParametersObserver;
		initializeComposite();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	protected void initializeComposite() {
		setLayout(new GridLayout(4, false));

		Label txtFlatteningOverflow = new Label(this, SWT.NONE);
		txtFlatteningOverflow.setText("Flattening overflow");

		overFlowScale = new Spinner(this, SWT.BORDER);
		overFlowScale.setMinimum(0);
		overFlowScale.setMaximum(100);
		overFlowScale.setIncrement(10);
		GridData gd_overFlowScale = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_overFlowScale.minimumWidth = 50;
		gd_overFlowScale.horizontalIndent = 10;
		overFlowScale.setLayoutData(gd_overFlowScale);

		Label label = new Label(this, SWT.NONE);
		label.setText("%");
		new Label(this, SWT.NONE);

		Label lblInterpolation = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblInterpolation.setText("Interpolation");
		GridData gd_lblInterpolation = new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1);
		gd_lblInterpolation.heightHint = 20;
		lblInterpolation.setLayoutData(gd_lblInterpolation);

		Label lblInterpolationDistance = new Label(this, SWT.NONE);
		lblInterpolationDistance.setText("Interpolation distance");

		maxDistance = new Spinner(this, SWT.BORDER);
		maxDistance.setToolTipText("The maximum number of cells to search in all directions to find values to interpolate from");
		GridData gd_maxDistance = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_maxDistance.horizontalIndent = 10;
		maxDistance.setLayoutData(gd_maxDistance);
		maxDistance.setMaximum(100);
		maxDistance.setMinimum(1);
		maxDistance.setSelection(10);

		Label lblCells = new Label(this, SWT.NONE);
		lblCells.setText("Cells");

		Button btnCompute = new Button(this, SWT.NONE);
		GridData gd_btnCompute = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnCompute.horizontalIndent = 10;
		btnCompute.setLayoutData(gd_btnCompute);
		btnCompute.setText("Compute");

		overFlowScale.addListener(SWT.Selection, (event) -> swathElevationParametersObserver.updateOverFlowScale(getOverFlow()));
		btnCompute.addListener(SWT.Selection, (event) -> swathElevationParametersObserver.interpolateElevations(getMaxDistance()));
	}

	/**
	 * Notified when the view changed.
	 */
	public static interface SwathElevationParametersObserver {
		void updateOverFlowScale(int overFlow);

		void interpolateElevations(int maxDistance);
	}

	/**
	 * @return the {@link #overFlowScale}
	 */
	public int getOverFlow() {
		return overFlowScale.getSelection();
	}

	/**
	 * @param overFlowScale
	 *            the {@link #overFlowScale} to set
	 */
	public void setOverFlow(int overFlow) {
		overFlowScale.setSelection(overFlow);
	}

	/**
	 * @return the {@link #maxDistance}
	 */
	public int getMaxDistance() {
		return maxDistance.getSelection();
	}

	/**
	 * @param maxDistance
	 *            the {@link #maxDistance} to set
	 */
	public void setMaxDistance(int maxDistance) {
		this.maxDistance.setSelection(maxDistance);
	}

}
