/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.util.function.Consumer;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Binder for validity colors shader.<br>
 * Bind uniforms "u_valid_color" and "u_invalid_color" and "u_invalid_acquisition_color".
 */
public class ValidityColorsShaderBinder extends AShaderBinder {

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindAttributes(fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes, java.util.function.Consumer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		Uniform<Vector4F> validColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms().get("u_valid_color");
		GColor validColor = dtmPreferences.getValidColor().getValue();
		if (validColorUniform != null)
			validColorUniform.setValue(new Vector4F(validColor.getFloatRed(), validColor.getFloatGreen(),
					validColor.getFloatBlue(), validColor.getFloatAlpha()));

		Uniform<Vector4F> invalidColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms()
				.get("u_invalid_color");
		GColor invalidColor = dtmPreferences.getInvalidColor().getValue();
		if (invalidColorUniform != null)
			invalidColorUniform.setValue(new Vector4F(invalidColor.getFloatRed(), invalidColor.getFloatGreen(),
					invalidColor.getFloatBlue(), invalidColor.getFloatAlpha()));

		Uniform<Vector4F> invalidAcquColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms()
				.get("u_invalid_acquisition_color");
		GColor invalidAcqColor = dtmPreferences.getInvalidColorAcqu().getValue();
		if (invalidAcquColorUniform != null)
			invalidAcquColorUniform.setValue(new Vector4F(invalidAcqColor.getFloatRed(),
					invalidAcqColor.getFloatGreen(), invalidAcqColor.getFloatBlue(), invalidAcqColor.getFloatAlpha()));
	}
}
