/**
 *
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;

/**
 * @author cponcele Wrapper around a line of buffer
 */
public class LineBuffer {

	private SpatialIndex spatialIndex;

	/**
	 * number of object in the line
	 */
	private int detectionCount;

	/**
	 * offset in mapped files
	 */
	private long offsetInFile;

	/**
	 * Configure this line
	 */
	public void init(SpatialIndex spatialIndex, long offsetInFile, int detectionCount) {
		this.spatialIndex = spatialIndex;
		this.detectionCount = detectionCount;
		this.offsetInFile = offsetInFile;
	}

	/** Free linked objects */
	public void reset() {
		spatialIndex = null;
		detectionCount = 0;
		offsetInFile = 0;
	}

	/**
	 * return the number of object currently stored in this buffer
	 */
	public int getDetectionCount() {
		return detectionCount;
	}

	/**
	 * @return the {@link #offsetInFile}
	 */
	public long getOffsetInFile() {
		return offsetInFile;
	}

	/**
	 * @return the {@link #spatialIndex}
	 */
	public SpatialIndex getSpatialIndex() {
		return spatialIndex;
	}
}
