﻿package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.geometry.OrientedBox;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.BathyDtmModel;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialLine;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWQuadSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.WWSwathSelection;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GUnknownException;
import fr.ifremer.globe.utils.perfcounter.PerfoCounter;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Class loading beams and transferring them to opengl compliant buffers
 */
public class BeamLoader {

	private static final Logger logger = LoggerFactory.getLogger(BeamLoader.class);

	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel model;

	/** Current DTM model */
	@Inject
	@Optional
	@Named(ContextNames.BATHY_DTM_MODEL)
	protected BathyDtmModel dtmModel;

	/** Eclipse event broker service */
	@Inject
	protected IEventBroker eventBroker;

	@Inject
	@Named(ContextNames.BOUNDS_COMPUTER)
	protected BoundsComputer boundsComputer;

	/** Subject that emits selections */
	protected PublishSubject<Runnable> selectionSubject = PublishSubject.create();
	protected Disposable disposable;

	GridIntersector intersector;

	/**
	 * Construct a new beam Loader
	 */
	@PostConstruct
	protected void postConstruct() {
		intersector = ContextInitializer.make(GridIntersector.class);
		disposable = selectionSubject//
				.toFlowable(BackpressureStrategy.LATEST)//
				.observeOn(Schedulers.computation(), false, 3)//
				.subscribe(Runnable::run,
						error -> logger.warn("Error while processing a selection : {}", error.getMessage()));
	}

	/**
	 * Dispose
	 */
	@PreDestroy
	protected void preDestroy() {
		if (disposable != null) {
			disposable.dispose();
		}
	}

	/**
	 * load the beams in awt thread
	 */
	public void loadBeams(final OrientedBox zone) {
		load(zone);
	}

	private void load(final OrientedBox selection) {
		PerfoCounter perf = new PerfoCounter("BeamLoader::compute selection");
		perf.start();
		model.setSelectedArea(selection);
		try {
			for (int i = 0; i < model.getBeamsAssociation().size(); i++) {
				load(i, selection);
			}
		} catch (Exception e) {
			Messages.openErrorMessage("Error while loading soundings", e);
		}
		perf.stop();

		// notify data change
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDataChanged);
	}

	private void load(WWSwathSelection swathSelection) {
		GeoBox geobox = swathSelection.geobox();
		model.setSelectedArea(new OrientedBox(geobox.getTopLeft(), geobox.getTopRight(), geobox.getBottomRight(),
				geobox.getBottomLeft()));
		try {
			for (int i = 0; i < model.getBeamsAssociation().size(); i++) {
				SounderDataContainer sounderDataContainer = model.getBeamsAssociation().getFile(i);
				if (sounderDataContainer.getInfo().equals(swathSelection.info())) {
					load(i, swathSelection.firstSwath(), swathSelection.lastSwath());
					break;
				}
			}
		} catch (Exception e) {
			Messages.openErrorMessage("Error while loading soundings", e);
		}

		// notify data change
		eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDataChanged);
	}

	/**
	 * load data for one file, release all buffers, borrow new ones and fill them
	 */
	private void load(int fileIndex, OrientedBox zone) throws GException {
		// remove old buffers
		try {
			model.getBeamsAssociation().releaseBuffers(fileIndex);
		} catch (Exception e) {
			throw new GUnknownException("Exception while loading data", e);
		}

		SpatialIndex spatialIndex = model.getBeamsAssociation().getSpatialIndex(fileIndex);
		List<SpatialLine> lines = intersector.intersect(zone, spatialIndex);
		// Create indexes to the buffer
		for (SpatialLine line : lines) {
			int count = spatialIndex.getSoundingCount(line);
			if (count > 0) {
				LineBuffer currentBuffer = model.getBeamsAssociation().getNewBuffer(fileIndex);
				// expected transfert size in bytes
				currentBuffer.init(spatialIndex,
						spatialIndex.getMappings().getCellOffset(line.getLine(), line.getColStart()), count);
			}
		}
	}

	/**
	 * load data for one file, release all buffers, borrow new ones and fill them
	 */
	private void load(int fileIndex, int firstSwathIdx, int lastSwathIdx) throws GException {
		var bufferAssociation = model.getBeamsAssociation();
		bufferAssociation.releaseAllBuffers();

		SpatialIndex spatialIndex = bufferAssociation.getSpatialIndex(fileIndex);
		Mappings mapping = spatialIndex.getMappings();
		// Browse all detections to find the lines covering the swath
		long offsetInFile = 0;
		int detectionCount = 0;
		for (long detectionIdx = 0; detectionIdx < mapping.getSoundingCount(); detectionIdx++) {
			int swathIdx = mapping.getCycleIndex(detectionIdx);
			if (swathIdx >= firstSwathIdx && swathIdx <= lastSwathIdx) {
				// Swath found
				if (detectionCount == 0) {
					offsetInFile = detectionIdx;
				}
				detectionCount++;
			} else {
				if (detectionCount > 0) {
					// The swath was found
					LineBuffer currentBuffer = bufferAssociation.getNewBuffer(fileIndex);
					currentBuffer.init(spatialIndex, offsetInFile, detectionCount);
					detectionCount = 0;
				}
			}
		}
		if (detectionCount > 0) {
			// The swath was found at the end of the mapping
			LineBuffer currentBuffer = bufferAssociation.getNewBuffer(fileIndex);
			currentBuffer.init(spatialIndex, offsetInFile, detectionCount);
		}
	}

	@Inject
	@Optional
	protected void fitQuadSelectionLayer(
			@Named(ContextNames.WW_QUAD_SELECTION_LAYER) IWWQuadSelectionLayer quadSelectionLayer) {
		quadSelectionLayer.setListener(this::onQuadSelectionDone);
	}

	/**
	 * Selection changed in IWWQuadSelectionLayer
	 */
	protected void onQuadSelectionDone(List<GeoPoint> pointsList) {
		OrientedBox orientedGeoBox = new OrientedBox(pointsList.get(0), pointsList.get(1), pointsList.get(2),
				pointsList.get(3));
		// Do not process empty selection
		if (orientedGeoBox.getSecondaryAxis().getNorm() != 0) {
			selectionSubject.onNext(() -> load(orientedGeoBox));
		}
	}

	@Inject
	@Optional
	protected void fitSwathSelectionLayer(
			@Named(ContextNames.WW_SWATH_SELECTION_LAYER) IWWSwathSelectionLayer swathLayer) {
		swathLayer.setListener(this::onSwathSelectionDone);
	}

	/**
	 * Selection changed in IWWSwathSelectionLayer
	 */
	protected void onSwathSelectionDone(WWSwathSelection swathSelections) {
		selectionSubject.onNext(() -> load(swathSelections));
	}

}
