package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import java.util.List;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.core.model.geometry.OrientedBox;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.FileStat;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.AxisAlignedSoundingBoundingBox;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.BeamVisibleMode;
import jakarta.inject.Inject;
import jakarta.inject.Named;

public class BoundsComputer {

	private java.util.Optional<SoundingsModel> soundingsModel;

	/**
	 * Compute the bounds of the sounding model
	 * 
	 * @return true if limits have changed
	 */
	public boolean computeBounds(OrientedBox selection) {
		if (soundingsModel.isPresent()) {
			SoundingsModel model = soundingsModel.get();

			// Save the current BB for later comparison
			AxisAlignedSoundingBoundingBox currentBB = new AxisAlignedSoundingBoundingBox();
			currentBB.copy(model.getSoundingBB().getActiveBB());
			// reset the bounding boxes
			model.resetBounds(selection);
			addAllBeams(model);
			// end recomputing bounding boxes
			model.finalizeBounds();

			return !currentBB.equals(model.getSoundingBB().getActiveBB());
		}
		return false;
	}

	/** Add all beam to the model, recompute bounding boxes and update buffers limits */
	private void addAllBeams(SoundingsModel model) {
		BeamVisibleMode visibleMode = model.getDisplayParameters().getVisibleMode();
		for (int fileIndex = 0; fileIndex < model.getBeamsAssociation().size(); fileIndex++) {
			int numberOfSoundings = 0; // numberofsounding per file (visible)
			List<LineBuffer> buffers = model.getBeamsAssociation().getBuffers(fileIndex);
			for (LineBuffer line : buffers) {
				Mappings mappedData = line.getSpatialIndex().getMappings();

				// read all data
				long offsetInFile = line.getOffsetInFile();
				int count = line.getDetectionCount();
				for (int i = 0; i < count; i++) {
					MappingValidity validity = mappedData.getValidity(offsetInFile + i);
					if (isVisible(visibleMode, validity)) {
						float x = mappedData.getX(offsetInFile + i);
						float y = mappedData.getY(offsetInFile + i);
						float z = mappedData.getDepth(offsetInFile + i);
						int cycleIndex = mappedData.getCycleIndex(offsetInFile + i);
						int beamIndex = mappedData.getBeamIndex(offsetInFile + i);
						float reflectivity = mappedData.getReflectivity(offsetInFile + i);
						float qualityFactor = mappedData.getQualityFactor(offsetInFile + i);
						model.addBeam(cycleIndex, beamIndex, x, y, z, reflectivity, qualityFactor);
						numberOfSoundings++;
					}

				}
			}
			model.setFileStat(new FileStat(numberOfSoundings),
					model.getBeamsAssociation().getFile(fileIndex).getInfo());
		}
	}

	private boolean isVisible(BeamVisibleMode visibleMode, MappingValidity validity) {
		return switch (visibleMode) {
		case INVALID_ONLY -> MappingValidity.isInvalid(validity);
		case VALID_ONLY -> MappingValidity.isValid(validity);
		case ALL -> !MappingValidity.isUnknown(validity);
		default -> !MappingValidity.isUnknown(validity);
		};
	}

	/**
	 * Inject SoundingsModel when available
	 */
	@Inject
	private void setSoundingsModel(@Optional @Named(ContextNames.SOUNDINGS_MODEL) SoundingsModel model) {
		this.soundingsModel = java.util.Optional.ofNullable(model);
	}
}
