package fr.ifremer.globe.editor.swath.ui.wizard;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.ui.utils.Messages;

/**
 * Wizard for spatial resolution choosing.
 */
public class SwathEditorWizard extends Wizard {

	protected final SwathEditorWizardPage1 spatialResolutionPage;

	/**
	 * Constructor.
	 *
	 * @param stores List of info stores used to establish the global geobox.
	 * @param parameters DTM generation parameters to be updated by the wizard.
	 */
	public SwathEditorWizard(SwathEditorParameters parameters) {
		setNeedsProgressMonitor(true);
		setWindowTitle("DTM generation parameters");
		spatialResolutionPage = new SwathEditorWizardPage1(parameters);
	}

	@Override
	public void addPages() {
		addPage(spatialResolutionPage);

		// delay controls initialization
		Display.getDefault().asyncExec(this::initWizard);

		for (IWizardPage page : getPages()) {
			page.setTitle("DTM generation parameters");
		}
	}

	/**
	 * Initialize controles of the first wizard's page.
	 */
	private void initWizard() {
		spatialResolutionPage.initializeControls();
		spatialResolutionPage.validateInputs();
	}

	@Override
	public boolean performFinish() {
		var parameters = spatialResolutionPage.getParameters();
		// check number of cells
		if (parameters.rowCount < 3 || parameters.columnCount < 3) {
			Messages.openErrorMessage("Error", "DTM layer dimensions are too small.\nPlease select a finer resolution");
			return false;
		}

		return true;
	}

	/**
	 * @return the {@link SwathEditorParameters} resulting of the edition
	 */
	public SwathEditorParameters getEditedParameters() {
		return spatialResolutionPage.getParameters();
	}

}