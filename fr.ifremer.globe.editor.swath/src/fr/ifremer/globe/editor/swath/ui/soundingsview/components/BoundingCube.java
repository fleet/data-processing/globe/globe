/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import java.awt.Color;
import java.awt.Font;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.TextRenderer;

import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.AxisAlignedSoundingBoundingBox;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.BoundingBoxes;
import fr.ifremer.globe.ogl.core.vectors.Vector2D;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.renderstate.AlphaTestFunction;
import fr.ifremer.globe.ogl.renderer.renderstate.DestinationBlendingFactor;
import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.renderstate.SourceBlendingFactor;
import fr.ifremer.globe.ogl.renderer.scene.Camera;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import jakarta.inject.Inject;

/**
 * Bounding box component.
 */
public abstract class BoundingCube extends GraphicComponent {

	/** Number of lines on each face of the cube. */
	public static final int LINE_COUNT = 10;

	/** Face index. */
	public static final int TOP = 0;
	/** Face index. */
	public static final int RIGHT = 1;
	/** Face index. */
	public static final int BACK = 2;
	/** Face index. */
	public static final int LEFT = 3;
	/** Face index. */
	public static final int FRONT = 4;
	/** Face index. */
	public static final int BOTTOM = 5;

	/**
	* 
	* */
	protected Vector3D[] normales = {
			// TOP
			new Vector3D(0, 0, -1),
			// RIGHT
			new Vector3D(-1, 0, 0),
			// BACK
			new Vector3D(0, -1, 0),
			// LEFT
			new Vector3D(1, 0, 0),
			// FRONT
			new Vector3D(0, 1, 0),
			// BOTTOM
			new Vector3D(0, 0, 1) };

	/**
	 * Index of the vertices used to draw this cube :
	 * 
	 * <pre>
	 *    v1------v0
	 *   /|      /|
	 *  v2------v3|
	 *  | |     | |
	 *  | |v6---|-|v5
	 *  |/      |/
	 *  v7------v4
	 * </pre>
	 * 
	 */
	public static final int V0 = 0;
	/** Vertex index. */
	public static final int V1 = 1;
	/** Vertex index. */
	public static final int V2 = 2;
	/** Vertex index. */
	public static final int V3 = 3;
	/** Vertex index. */
	public static final int V4 = 4;
	/** Vertex index. */
	public static final int V5 = 5;
	/** Vertex index. */
	public static final int V6 = 6;
	/** Vertex index. */
	public static final int V7 = 7;

	/** Coord index. */
	public static final int X = 0;
	/** Coord index. */
	public static final int Y = 1;
	/** Coord index. */
	public static final int Z = 2;

	/** Cube's vertices. */
	protected float[][] vertices;

	/** Cube vertex VBO. */
	protected VBO cubeVbo;
	/** Nb float per vertex. */
	protected static final int VERTEX_COMPONENT = 3; // X, Y, Z
	/** Cube color VBO. */
	protected VBO cubeColorVbo;
	/** Nb float per color. */
	protected static final int COLOR_COMPONENT = 3; // R, G, B

	/** Line vertex VBO. */
	protected VBO lineVbo;
	/** Cube color VBO. */
	protected VBO lineColorVbo;
	/** Number of lines per face. */
	int[] lineCountPerFace = { 0, 0, 0, 0, 0, 0 };

	/** All label to display. */
	List<Label> labels = new ArrayList<>();

	/** True to consider a new model. */
	protected AtomicBoolean modelHasChanged = new AtomicBoolean(false);

	/** The sounding box of all displayed soundings */
	protected AxisAlignedSoundingBoundingBox boundingBox;

	/** True when preferences have changed. */
	protected AtomicBoolean preferencesHasChanged = new AtomicBoolean(true);

	/** Observer of preference changes. */
	protected Observer preferencesObserver;

	/** OpenGl's context. */
	protected ContextGL2x context;

	/** Camera of the scene. */
	protected Camera camera;

	/** Medium size renderer (must be created while drawing) */
	protected TextRenderer renderer = null;
	/** Font size */
	protected int fontSize = 12;
	/** Color of the text. */
	protected Color textColor;
	/** True to display the grid text. */
	protected boolean showText;
	/** Axis formatter. */
	protected static final DecimalFormat TICK_FORMAT = new DecimalFormat("0.00 m");

	protected GLAutoDrawable drawable;

	protected Vector3D vertice0;
	protected Vector3D vertice1;
	protected Vector3D vertice2;
	protected Vector3D vertice3;
	protected Vector3D vertice4;
	protected Vector3D vertice5;
	protected Vector3D vertice6;
	protected Vector3D vertice7;

	/**
	 * Constructor.
	 */
	protected BoundingCube(Camera camera) {

		this.camera = camera;

		preferencesObserver = (Observable o, Object arg) -> preferencesHasChanged.set(true);
		dtmPreferences.getCubeFillColor().addObserver(preferencesObserver);
		dtmPreferences.getCubeBottomColor().addObserver(preferencesObserver);
		dtmPreferences.getCubeOutLineColor().addObserver(preferencesObserver);
		dtmPreferences.getMetricsText().addObserver(preferencesObserver);
		dtmPreferences.getFontSize().addObserver(preferencesObserver);
		dtmPreferences.getForegroundColor().addObserver(preferencesObserver);
	}

	/** Free ressources. */
	@Override
	public void dispose(Context context) {
		camera = null;
		super.dispose(context);

		dtmPreferences.getCubeFillColor().deleteObserver(preferencesObserver);
		dtmPreferences.getCubeBottomColor().deleteObserver(preferencesObserver);
		dtmPreferences.getCubeOutLineColor().deleteObserver(preferencesObserver);
		dtmPreferences.getMetricsText().deleteObserver(preferencesObserver);
		dtmPreferences.getFontSize().deleteObserver(preferencesObserver);
		dtmPreferences.getForegroundColor().deleteObserver(preferencesObserver);

		try {
			if (context != null) {

				boolean hasContext = context.makeCurrent();
				cubeVbo.dispose(GLU.getCurrentGL());
				cubeColorVbo.dispose(GLU.getCurrentGL());
				lineColorVbo.dispose(GLU.getCurrentGL());
				if (hasContext)
					context.release();
			}
		} catch (GLException e) {
			logger.warn(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	/**
	 * create the component, create drawstate and everything else
	 */
	@Override
	public void init(GLAutoDrawable drawable, ContextGL2x context) {
		this.drawable = drawable;
		this.context = context;

		// Cube
		cubeVbo = new VBO(VERTEX_COMPONENT);
		cubeVbo.setUsage(GL.GL_DYNAMIC_DRAW);
		cubeVbo.allocateBuffer(36); // 36 vertex for drawing a cube
		cubeVbo.bindAndLoad(drawable.getGL());

		// Cube colors
		cubeColorVbo = new VBO(COLOR_COMPONENT);
		cubeColorVbo.setUsage(GL.GL_DYNAMIC_DRAW);
		cubeColorVbo.allocateBuffer(36); // 36 vertex for drawing a cube
		cubeColorVbo.bindAndLoad(drawable.getGL());

		// Lines
		lineVbo = new VBO(VERTEX_COMPONENT);
		lineVbo.setUsage(GL.GL_DYNAMIC_DRAW);
		lineVbo.allocateBuffer(1024);
		lineVbo.bindAndLoad(drawable.getGL());

		// Line colors
		lineColorVbo = new VBO(COLOR_COMPONENT);
		lineColorVbo.setUsage(GL.GL_DYNAMIC_DRAW);
		lineColorVbo.allocateBuffer(1024);
		lineColorVbo.bindAndLoad(drawable.getGL());

		renderer = new TextRenderer(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize));

	}

	/**
	 * draw the component.
	 */
	@Override
	public void draw(GL2 gl, Context context, SceneState sceneState) {
		if (preferencesHasChanged.compareAndSet(true, false)) {
			loadPreferences(gl);
			labels.clear(); // Force labels computation
		}
		if (modelHasChanged.compareAndSet(true, false)) {
			computeCubeVertice();
			initializeCube(gl);
			initializeLines(gl);
			labels.clear(); // Force labels computation
		}

		// Model no ready
		if (vertices == null) {
			return;
		}

		// Enable Transparency
		RenderState renderState = new RenderState();
		renderState.getBlending().setEnabled(true);
		renderState.getBlending().setSourceRGBFactor(SourceBlendingFactor.SourceAlpha);
		renderState.getBlending().setDestinationRGBFactor(DestinationBlendingFactor.OneMinusSourceAlpha);
		renderState.getBlending().setSourceAlphaFactor(SourceBlendingFactor.SourceAlpha);
		renderState.getBlending().setDestinationAlphaFactor(DestinationBlendingFactor.OneMinusSourceAlpha);
		renderState.getAlphaTest().setEnabled(false);
		renderState.getAlphaTest().setFunction(AlphaTestFunction.Greater);
		renderState.getAlphaTest().setValue(0.0f);
		renderState.getDepthTest().setEnabled(false);

		context.forceApplyRenderState(renderState);

		// bounding cube
		gl.glLineWidth(1f);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);

		int[] viewport = new int[4];
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		double[] mvmatrix = new double[16];
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		double[] projmatrix = new double[16];
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);

		int visibility = computeVisibility();

		for (int face = 0; face < normales.length; face++) {
			if (isFacesVisible(1 << face, visibility)) {
				cubeVbo.drawVertexArrayWithColorArray(gl, GL.GL_TRIANGLES, face * 6, 6, cubeColorVbo);
				int lineVertexIndex = 0;
				for (int i = 0; i < face; i++) {
					lineVertexIndex += lineCountPerFace[i] * 2;
				}
				lineVbo.drawVertexArrayWithColorArray(gl, GL.GL_LINES, lineVertexIndex, lineCountPerFace[face] * 2,
						lineColorVbo);
			}
		}

		gl.glDisable(GL.GL_LINE_SMOOTH);
		// Labels if needed
		if (showText) {
			if (labels.isEmpty()) {
				initializeLabels();
			}
			drawLabels(gl, mvmatrix, projmatrix, viewport, visibility);
		}
	}

	public boolean isFacesVisible(int faces, int visibleFaces) {
		return (visibleFaces & faces) == faces;
	}

	/**
	 * @return true if only one of label faces is visible and it's a face contained in "visibleLabelFaces" list
	 */
	public boolean isFacesVisibleLabel(int faces, int visibleFaces, int visibleLabelFaces) {
		boolean onlyOneFaceVisible = (visibleFaces & faces) != faces && (visibleFaces & faces) != 0;
		return onlyOneFaceVisible && (faces & visibleLabelFaces) != 0;
	}

	/**
	 * Draw the label of the specified face.
	 */
	protected void drawLabels(GL2 gl, double[] mvmatrix, double[] projmatrix, int[] viewport, int visibleFaces) {
		int visibleLabelFaces = computeLabelVisibility();
		GLU glu = new GLU();
		double[] unprojCoord = new double[4];
		for (Label label : labels) {

			if (isFacesVisibleLabel(label.visibleFaces, visibleFaces, visibleLabelFaces)) {
				glu.gluProject(label.position[X], label.position[Y], label.position[Z], mvmatrix, 0, projmatrix, 0,
						viewport, 0, unprojCoord, 0);
				int winX = (int) unprojCoord[0];
				if (label.angle == 0f && winX < viewport[2] / 2) {
					// Left labels are shifted to the left (outside the cube)
					winX -= 95;
				}
				int winY = (int) unprojCoord[1] - fontSize / 2;

				renderer.beginRendering(drawable.getSurfaceWidth(), drawable.getSurfaceHeight());

				gl.glMatrixMode(GL2.GL_MODELVIEW);
				gl.glPushMatrix();
				gl.glTranslatef(winX, winY, 0f);
				gl.glRotated(label.angle, 0f, 0f, 1f);

				renderer.setColor(textColor);
				renderer.setSmoothing(true);
				renderer.draw(label.value, 0, 0);
				renderer.endRendering();
				renderer.flush();

				gl.glPopMatrix();
			}
		}
	}

	/**
	 * Compute the visibility of each face.
	 */
	protected int computeVisibility() {
		int result = 0;
		Vector3D eye = camera.getEye();
		Vector3D view = eye.subtract(camera.getTarget());
		for (int i = 0; i < normales.length; i++) {
			double r = normales[i].dot(view);
			if (r < -0.1) {
				result |= 1 << i;
			}
		}
		return result;
	}

	/**
	 * Compute visibility for labels on each face
	 */
	protected int computeLabelVisibility() {
		int result = 0;
		Vector3D eye = camera.getEye();
		Vector3D view = eye.subtract(camera.getTarget()).normalize();
		for (int i = 0; i < normales.length; i++) {
			double r = normales[i].dot(view);
			if (r < -Math.cos(Math.toRadians(60))) {
				result |= 1 << i;
			}
		}
		return result;
	}

	/**
	 * Change of the the soundings selection.
	 */
	@Inject
	@Optional
	void onBoundingBoxesUpdated(@UIEventTopic(EventTopics.BOUNDING_BOXES_UPDATED) BoundingBoxes boundingBoxes) {
		boundingBox = boundingBoxes.getActiveBB();
		modelHasChanged.set(true);
	}

	/**
	 * Prepare the vertice for drawing a cube.
	 */
	protected abstract void computeCubeVertice();

	/**
	 * Initialize the cube buffer.
	 */
	protected void initializeCube(GL2 gl) {
		FloatBuffer buffer = cubeVbo.getBuffer();
		buffer.rewind();

		// Top : v0-v1-v2 and v0-v1-v2
		addVerticesToBuffer(buffer, vertices[V0], vertices[V1], vertices[V2]);
		addVerticesToBuffer(buffer, vertices[V2], vertices[V3], vertices[V0]);
		// right : v0-v3-v4 and v4-v5-v0
		addVerticesToBuffer(buffer, vertices[V0], vertices[V3], vertices[V4]);
		addVerticesToBuffer(buffer, vertices[V4], vertices[V5], vertices[V0]);
		// back : v0-v5-v6 and v6-v1-v0
		addVerticesToBuffer(buffer, vertices[V0], vertices[V5], vertices[V6]);
		addVerticesToBuffer(buffer, vertices[V6], vertices[V1], vertices[V0]);
		// left : v1-v6-v7 and v7-v2-v1
		addVerticesToBuffer(buffer, vertices[V1], vertices[V6], vertices[V7]);
		addVerticesToBuffer(buffer, vertices[V1], vertices[V7], vertices[V2]);
		// front : v7-v4-v3 and v3-v2-v7
		addVerticesToBuffer(buffer, vertices[V7], vertices[V4], vertices[V3]);
		addVerticesToBuffer(buffer, vertices[V3], vertices[V2], vertices[V7]);
		// bottom : v4-v7-v6 and v6-v5-v4
		addVerticesToBuffer(buffer, vertices[V4], vertices[V7], vertices[V6]);
		addVerticesToBuffer(buffer, vertices[V6], vertices[V5], vertices[V4]);

		buffer.flip();
		cubeVbo.update(gl, 0, (long) buffer.capacity() * Float.BYTES);
	}

	/**
	 * Add some vertice to a buffer.
	 */
	protected void addVerticesToBuffer(FloatBuffer buffer, float[]... vertice) {
		Arrays.stream(vertice).forEach(buffer::put);
	}

	/**
	 * Initialize the lines buffer.
	 */
	protected void initializeLines(GL2 gl) {
		FloatBuffer buffer = lineVbo.getBuffer();
		buffer.clear();

		float depthStep = boundingBox.getStepZDefunedByUser() != null ? boundingBox.getStepZDefunedByUser()
				: (float) computeStep(boundingBox.getDeltaZ());
		float roundedDepthMax = (float) (((Math.floor((boundingBox.getMinZ() + depthStep) / depthStep)) * depthStep)
				- boundingBox.getMeanZ());
		float roundedDepthMin = (float) (((Math.ceil((boundingBox.getMaxZ()) / depthStep)) * depthStep)
				- boundingBox.getMeanZ());

		Arrays.fill(lineCountPerFace, 0);

		Vector3D yDir = vertice0.subtract(vertice1);
		Vector2D yDirection = new Vector2D(yDir.getX(), yDir.getY()).divide(LINE_COUNT);
		Vector3D xDir = vertice2.subtract(vertice1);
		Vector2D xDirection = new Vector2D(xDir.getX(), xDir.getY()).divide(LINE_COUNT);

		Arrays.fill(lineCountPerFace, 0);

		// top
		addLinesToBuffer(TOP, buffer, V0, V1, xDirection);
		addLinesToBuffer(TOP, buffer, V1, V2, yDirection);

		// Right
		addLinesToBuffer(RIGHT, buffer, V0, V5, xDirection);
		addDepthLinesToBuffer(RIGHT, buffer, V0, V3, roundedDepthMin, roundedDepthMax, depthStep);

		// back
		addLinesToBuffer(BACK, buffer, V1, V6, yDirection);
		addDepthLinesToBuffer(BACK, buffer, V1, V0, roundedDepthMin, roundedDepthMax, depthStep);

		// Left
		addLinesToBuffer(LEFT, buffer, V1, V6, xDirection);
		addDepthLinesToBuffer(LEFT, buffer, V2, V1, roundedDepthMin, roundedDepthMax, depthStep);

		// front
		addLinesToBuffer(FRONT, buffer, V2, V7, yDirection);
		addDepthLinesToBuffer(FRONT, buffer, V3, V2, roundedDepthMin, roundedDepthMax, depthStep);

		// bottom
		addLinesToBuffer(BOTTOM, buffer, V6, V5, xDirection);
		addLinesToBuffer(BOTTOM, buffer, V6, V7, yDirection);

		buffer.flip();
		lineVbo.update(gl, 0, (long) buffer.capacity() * Float.BYTES);
	}

	/**
	 * Initialize the lines buffer.
	 */
	protected void initializeLabels() {

		float depthStep = boundingBox.getStepZDefunedByUser() != null ? boundingBox.getStepZDefunedByUser()
				: (float) computeStep(boundingBox.getDeltaZ());
		float roundedDepthMax = (float) (((Math.floor((boundingBox.getMinZ() + depthStep) / depthStep)) * depthStep)
				- boundingBox.getMeanZ());
		float roundedDepthMin = (float) (((Math.ceil((boundingBox.getMaxZ()) / depthStep)) * depthStep)
				- boundingBox.getMeanZ());

		Vector3D yDirLabel = vertice0.subtract(vertice1).divide(LINE_COUNT);
		Vector3D xDirLabel = vertice2.subtract(vertice1).divide(LINE_COUNT);

		// top
		addLabel(vertice0, xDirLabel, (1 << TOP) | (1 << RIGHT), 45f);
		addLabel(vertice1, xDirLabel, (1 << TOP) | (1 << LEFT), 45f);
		addLabel(vertice2, yDirLabel, (1 << TOP) | (1 << FRONT), 45f);
		addLabel(vertice1, yDirLabel, (1 << TOP) | (1 << BACK), 45f);
		// bottom
		addLabel(vertice5, xDirLabel, (1 << BOTTOM) | (1 << RIGHT), -45f);
		addLabel(vertice6, xDirLabel, (1 << BOTTOM) | (1 << LEFT), -45f);
		addLabel(vertice7, yDirLabel, (1 << BOTTOM) | (1 << FRONT), -45f);
		addLabel(vertice6, yDirLabel, (1 << BOTTOM) | (1 << BACK), -45f);

		// Right
		addDepthLabels(boundingBox, V3, roundedDepthMin, roundedDepthMax, depthStep, (1 << RIGHT) | (1 << FRONT));

		// back
		addDepthLabels(boundingBox, V0, roundedDepthMin, roundedDepthMax, depthStep, (1 << RIGHT) | (1 << BACK));

		// Left
		addDepthLabels(boundingBox, V1, roundedDepthMin, roundedDepthMax, depthStep, (1 << LEFT) | (1 << BACK));

		// front
		addDepthLabels(boundingBox, V2, roundedDepthMin, roundedDepthMax, depthStep, (1 << LEFT) | (1 << FRONT));

	}

	/**
	 * Add vertices a the buffer for drawing a line.
	 */
	protected void addLinesToBuffer(int face, FloatBuffer buffer, int vertex1, int vertex2, Vector2D direction) {
		float[] vertexFirst = Arrays.copyOf(vertices[vertex1], 3);
		float[] vertexLast = Arrays.copyOf(vertices[vertex2], 3);

		addVerticesToBuffer(buffer, vertexFirst, vertexLast);
		lineCountPerFace[face]++;

		for (int i = 0; i < LINE_COUNT; i++) {
			vertexFirst[X] = vertexFirst[X] + (float) direction.getX();
			vertexLast[X] = vertexLast[X] + (float) direction.getX();
			vertexLast[Y] = vertexLast[Y] + (float) direction.getY();
			vertexFirst[Y] = vertexFirst[Y] + (float) direction.getY();
			addVerticesToBuffer(buffer, vertexFirst, vertexLast);
			lineCountPerFace[face]++;
		}
	}

	/**
	 * Add longitude labels.
	 */
	protected void addLabel(Vector3D startPoint, Vector3D axis, int associatedFace, float angle) {
		float[] vertex = new float[3];
		vertex[X] = (float) startPoint.getX();
		vertex[Y] = (float) startPoint.getY();
		vertex[Z] = (float) startPoint.getZ();
		int center = LINE_COUNT / 2;
		int distance = (int) axis.getMagnitude();
		for (int i = 0; i < LINE_COUNT - 1; i++) {
			vertex[X] = (float) (vertex[X] + axis.getX());
			vertex[Y] = (float) (vertex[Y] + axis.getY());
			labels.add(new Label(associatedFace, vertex, angle, TICK_FORMAT.format(((center - i - 1) * distance))));
		}
	}

	/**
	 * Add vertices a the buffer for drawing a depth line.
	 */
	protected void addDepthLinesToBuffer(int face, FloatBuffer buffer, int vertex1, int vertex2, float minDepth,
			float maxDepth, float depthStep) {
		float[] vertexFirst = Arrays.copyOf(vertices[vertex1], 3);
		vertexFirst[Z] = maxDepth;
		float[] vertexLast = Arrays.copyOf(vertices[vertex2], 3);
		vertexLast[Z] = maxDepth;
		while (vertexFirst[Z] <= minDepth - depthStep) {
			addVerticesToBuffer(buffer, vertexFirst, vertexLast);

			vertexFirst[Z] = vertexFirst[Z] + depthStep;
			vertexLast[Z] = vertexLast[Z] + depthStep;
			lineCountPerFace[face]++;
		}
	}

	/**
	 * Add depth labels.
	 */
	protected void addDepthLabels(AxisAlignedSoundingBoundingBox boundingBox, int vertexIndex, float minDepth,
			float maxDepth, float depthStep, int faces) {
		float[] vertexFirst = Arrays.copyOf(vertices[vertexIndex], 3);
		vertexFirst[Z] = maxDepth;
		while (vertexFirst[Z] <= minDepth - depthStep) {
			labels.add(new Label(faces, vertexFirst, 0f, TICK_FORMAT.format(vertexFirst[Z] + boundingBox.getMeanZ())));
			vertexFirst[Z] = vertexFirst[Z] + depthStep;
		}
	}

	/** Load all preferences. */
	protected void loadPreferences(GL2 gl) {
		// Cube
		ColorPreferenceAttribute cubeFillColorAttribute = dtmPreferences.getCubeFillColor();
		float[] cubeFillColor = cubeFillColorAttribute.getValue().toRGB();
		ColorPreferenceAttribute cubeBottomColorAttribute = dtmPreferences.getCubeBottomColor();
		float[] cubeBottomColor = cubeBottomColorAttribute.getValue().toRGB();

		FloatBuffer cubeColorbuffer = cubeColorVbo.getBuffer();
		cubeColorbuffer.rewind();
		addColorToFaceOfCube(cubeColorbuffer, cubeBottomColor); // top
		addColorToFaceOfCube(cubeColorbuffer, cubeFillColor); // right
		addColorToFaceOfCube(cubeColorbuffer, cubeFillColor); // back
		addColorToFaceOfCube(cubeColorbuffer, cubeFillColor); // left
		addColorToFaceOfCube(cubeColorbuffer, cubeFillColor); // front
		addColorToFaceOfCube(cubeColorbuffer, cubeBottomColor); // bottom
		cubeColorbuffer.flip();
		cubeColorVbo.update(gl, 0, (long) cubeColorbuffer.capacity() * Float.BYTES);

		// Lines
		ColorPreferenceAttribute cubeOutLineColorAttribute = dtmPreferences.getCubeOutLineColor();
		float[] cubeOutLineColor = cubeOutLineColorAttribute.getValue().toRGB();

		FloatBuffer lineColorbuffer = lineColorVbo.getBuffer();
		lineColorbuffer.rewind();
		for (int i = 0; i < lineColorbuffer.capacity(); i += 3) {
			lineColorbuffer.put(cubeOutLineColor);
		}
		lineColorbuffer.flip();
		lineColorVbo.update(gl, 0, (long) lineColorbuffer.capacity() * Float.BYTES);

		// Text
		ColorPreferenceAttribute textColorAttribute = dtmPreferences.getForegroundColor();
		textColor = ColorUtils.convertGColorToAWT(textColorAttribute.getValue());
		BooleanPreferenceAttribute showCubeTextAttribute = dtmPreferences.getMetricsText();
		showText = showCubeTextAttribute.getValue();

		int lfontSize = dtmPreferences.getFontSize().getValue();
		if (fontSize != lfontSize) {
			fontSize = lfontSize;
			renderer = new TextRenderer(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize));
		}
	}

	/**
	 * Add a color to a face of the cube.
	 */
	protected void addColorToFaceOfCube(FloatBuffer colorbuffer, float[] color) {
		for (int i = 0; i < 6; i++) {
			colorbuffer.put(color);
		}
	}

	protected double computeStep(double delta) {
		double step;
		step = Math.pow(10, Math.floor(Math.log10(delta))) / 2;
		if (delta / step > 10) {
			step = step * 2;
		}
		return step;
	}

	/**
	 * One displayed label.
	 */
	static class Label {

		int visibleFaces;
		/** Position. */
		float[] position;

		/** Value of the label. */
		String value;

		/** Angle. */
		float angle = 0;

		/**
		 * Constructor.
		 */
		public Label(int associatedFace, float[] position, float angle, String value) {
			visibleFaces = associatedFace;
			this.position = Arrays.copyOf(position, position.length);
			this.angle = angle;
			this.value = "  " + value;
		}
	}
}
