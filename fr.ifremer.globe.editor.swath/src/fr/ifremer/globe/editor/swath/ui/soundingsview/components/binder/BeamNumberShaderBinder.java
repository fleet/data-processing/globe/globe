/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.Buffer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Binder for BeamNumber shader.
 */
public class BeamNumberShaderBinder extends AShaderBinder {

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindUniforms(fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel)
	 */
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		// Min and max values
		MinMaxShaderBinder<Float> minMaxShaderBinder = new MinMaxShaderBinder<Float>(
				(float) soundingModel.getValueBoundings().getMinBeamIndex(),
				(float) soundingModel.getValueBoundings().getMaxBeamIndex());
		minMaxShaderBinder.setShaderProgram(getShaderProgram());
		minMaxShaderBinder.bindUniforms(lineBuffer);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindAttributes(SelectionModel,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes, java.util.function.Consumer)
	 */
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		super.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);

		int offsetInFile = (int) lineBuffer.getOffsetInFile();
		int count = lineBuffer.getDetectionCount();

		Supplier<Buffer> supplier = () -> lineBuffer.getSpatialIndex().getMappings().getBeamIndexArray()
				.asShortBuffer(offsetInFile, count);

		VertexBuffer valueVertexBuffer = new VertexBufferGL2x3x(supplier.get(), BufferHint.StaticDraw,
				count * Short.BYTES);
		VertexBufferAttribute valueVertexBufferAttribute = new VertexBufferAttribute(valueVertexBuffer,
				ComponentDatatype.Short, 1);
		int valueAttributeLocation = getShaderProgram().getVertexAttributes().get("in_value").getLocation();
		vertexBufferAttributes.set(valueAttributeLocation, valueVertexBufferAttribute);

		alertVertexBufferCreated(listener, new BufferRefresher(valueVertexBuffer, supplier));
	}

}
