
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.triangulation;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.osgi.service.event.EventHandler;

import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.action.SoundEditorAction;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.UndoRedoController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering.SimpleParameterDialog;
import fr.ifremer.globe.ui.utils.image.Icons;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

public class TriangulationFilteringPart {

	/** Id of the view defined in e4 model */
	public static final String PART_ID = "fr.ifremer.globe.editor.swath.partdescriptor.triangulation_filtering";

	/** Main swt composite */
	private final Composite mainComposite;

	/** Handler subscribed to IEventBroker for PartStack management */
	private final EventHandler eventHandler;

	/**
	 * Post construction, after injection
	 */
	@Inject
	public TriangulationFilteringPart(IEventBroker eventBroker, IProcessService processService, Composite parent) {
		mainComposite = parent;
		this.eventHandler = PartUtil.onPartStackChanged(PART_ID, eventBroker, this::onPartStackChanged);

		FiltSoundingsProvider filtSoundingsProvider = ContextInitializer.make(FiltSoundingsProvider.class);
		DirectFilTriProcess filTriProcess = new DirectFilTriProcess(parent.getShell(), filtSoundingsProvider);

		// Get the parameters dialog, keep only the dialog area
		SimpleParameterDialog parameterDialog = filTriProcess.getDialog();
		Composite dialogArea = (Composite) parameterDialog.createDialogArea(mainComposite);

		// Add a Run button
		Label separator = new Label(dialogArea, SWT.HORIZONTAL | SWT.SEPARATOR);
		GridData separatorGd = new GridData(SWT.FILL, SWT.FILL, true, false);
		separatorGd.verticalIndent = 15;
		separator.setLayoutData(separatorGd);

		Button runButton = new Button(dialogArea, SWT.PUSH);
		GridData runGd = new GridData(SWT.LEFT, SWT.FILL, true, false);
		runGd.verticalIndent = 15;
		runButton.setLayoutData(runGd);
		runButton.setText("run");
		runButton.setImage(Icons.RUN.toImage());
		runButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			BusyIndicator.showWhile(runButton.getDisplay(), () -> parameterDialog.okPressed());
		}));

		// Hook the consumer, for running the filtering with the given parameters
		parameterDialog.setConsumer(parameters -> {
			UndoRedoController undoRedoController = ContextInitializer.getInContext(ContextNames.UNDO_REDO_CONTROLLER);
			undoRedoController.ensureNextOperationIsPrepared();

			GProcess process = processService.createProcess("Filtering by triangulation", filTriProcess);
			process.whenIsDone(event -> {
				if (event.getResult().isOK()) {

					SoundingsController soundingsController = ContextInitializer
							.getInContext(ContextNames.SOUNDINGS_CONTROLLER);
					soundingsController.reset();
					soundingsController.validationHasChanged();
					eventBroker.send(EventTopics.SOUND_ACTION, new SoundEditorAction("triangulation invalitation"));

					String message = String.format("%d sounding%s have been invalidated",
							filTriProcess.getFilteredNbSounds(), filTriProcess.getFilteredNbSounds() > 1 ? "s" : "");

					mainComposite.getDisplay().asyncExec(() -> MessageDialog.openInformation(mainComposite.getShell(),
							parameters.getTitle(), message));
				} else
					// Undo/Redo synchronization
					undoRedoController.cancelLastPreparation();
			});
			processService.runInForeground(process);

		});
	}

	/** Returns the preferred size (in points) of the part. */
	public Point computeSize() {
		return mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
	}

	@PreDestroy
	private void dispose(IEventBroker eventBroker) {
		eventBroker.unsubscribe(eventHandler);
	}

	/** Subscribe to EventBroker and reacts when view is added to a PartStack */
	private void onPartStackChanged(MPartStack partStack) {
		DtmPreference preference = ContextInitializer.getInContext(ContextNames.DTM_PREFERENCES);
		preference.getContainerId(PART_ID).setValue(partStack.getElementId(), false);
		preference.save();
	}

}