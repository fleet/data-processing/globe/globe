/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.Buffer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Binder for validity shader.<br>
 * Bind attribute "in_validity"
 */
public class ValidityShaderBinder extends AShaderBinder {

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindAttributes(SelectionModel,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes, java.util.function.Consumer)
	 */
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		super.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);

		int offsetInFile = (int) lineBuffer.getOffsetInFile();
		int count = lineBuffer.getDetectionCount();

		Supplier<Buffer> supplier = () -> lineBuffer.getSpatialIndex().getMappings().getValidityArray()
				.asByteBuffer(offsetInFile, count);
		VertexBuffer validityVertexBuffer = new VertexBufferGL2x3x(supplier.get(), BufferHint.StaticDraw, count);

		VertexBufferAttribute validityVertexBufferAttribute = new VertexBufferAttribute(validityVertexBuffer,
				ComponentDatatype.Byte, 1);
		int validityAttributeLocation = getShaderProgram().getVertexAttributes().get("in_validity").getLocation();
		vertexBufferAttributes.set(validityAttributeLocation, validityVertexBufferAttribute);

		Uniform<Float> showValidUniformUniform = (Uniform<Float>) getShaderProgram().getUniforms()
				.get("u_keep_invalid_beam_in_red");
		if (showValidUniformUniform != null) {
			boolean keepInvalidBeamInRed = soundingModel.getDisplayParameters().isKeepInvalidBeamInRed();
			showValidUniformUniform.setValue(Float.valueOf(NumberUtils.bool2byte(keepInvalidBeamInRed)));
		}

		Uniform<Vector4F> invalidColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms()
				.get("u_invalid_color");
		if (invalidColorUniform != null) {
			GColor invalidColor = dtmPreferences.getInvalidColor().getValue();
			invalidColorUniform.setValue(new Vector4F(invalidColor.getFloatRed(), invalidColor.getFloatGreen(),
					invalidColor.getFloatBlue(), invalidColor.getFloatAlpha()));
		}
		alertVertexBufferCreated(listener, new BufferRefresher(validityVertexBuffer, supplier));
	}
}
