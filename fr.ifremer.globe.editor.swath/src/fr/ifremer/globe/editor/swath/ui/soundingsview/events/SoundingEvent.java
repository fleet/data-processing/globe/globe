package fr.ifremer.globe.editor.swath.ui.soundingsview.events;

public enum SoundingEvent {
	/** data display configuration changed (color, visibility, exaggeration...) */
	eDisplayChanged,
	/** all the data changed (position, and so one typically a new selection occurred */
	eDataChanged,
	/** data loaded after taking into account the new selection ( after eDataChanged: legend) */
	eDataLoaded,
	/**
	 * a new view point was recomputed
	 */
	eViewPointChanged
}