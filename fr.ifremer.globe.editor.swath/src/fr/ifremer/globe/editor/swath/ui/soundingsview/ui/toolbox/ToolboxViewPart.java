﻿package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.contexts.RunAndTrack;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.widgets.ButtonFactory;
import org.eclipse.jface.widgets.LabelFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SVDisplayParameterChanged;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.BeamVisibleMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SelectionMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SoundingsSceneMode;
import fr.ifremer.globe.ui.utils.UIUtils;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Toolbox for the Soundings view
 */
public class ToolboxViewPart {

	/** Id of the view in fragment model. */
	public static final String PART_ID = "fr.ifremer.globe.editor.swath.partdescriptor.soundingsToolboxView";

	// Injected dependencies
	private final DtmPreference dtmPreferences;
	private final ViewPoint viewPoint;

	/** Object which contains all actions of {@link ToolboxViewPart} buttons. */
	private ToolboxActions toolboxActions;
	/** One tool selection is in progress. */
	private AtomicBoolean actionInProgress;
	/** Items of {@link ToolboxMenu} */
	private Button exaggerationViewButton;
	private Button lockViewButton;

	private Button selectionSceneModeButton;
	private Button validateSceneModeButton;
	private Button invalidateSceneModeButton;

	private Button quadSelectionModeButton;
	private Button polygonalSelectionModeButton;
	private Button lassoSelectionModeButton;

	private Button visibilityAllButton;
	private Button visibilityValidButton;
	private Button visibilityInvalidButton;

	private Button metricsButton;
	private Button orientedBoundingCubeButton;
	private Button soundingsButton;
	private Button landmarkButton;
	private Button statsButton;
	private ExpandItem depthAxisExpanditem;
	private Composite depthAxisComposite;
	private Button btnAutoScale;
	private Text topDepthBoundValue;
	private Label lblMin;
	private Text bottomDepthBoundValue;
	private Label lblMax;
	private Label lblStep;
	private Text stepDepthValue;
	private Label lblM2;
	private Label lblM1;
	private Label lblM3;
	private Button btnNewButton;

	private Combo comboColors;
	private Button keepInvalidRedButton;

	/** True when part is closed */
	private boolean partClosed = false;

	// private Button autoRefreshDTMButton;

	/**
	 * Constructor.
	 */
	@Inject
	public ToolboxViewPart(@Named(ContextNames.DTM_PREFERENCES) DtmPreference dtmPreferences) {
		this.dtmPreferences = dtmPreferences;
		this.viewPoint = ContextInitializer.getInContext(ContextNames.SOUNDINGS_VIEW_POINT);

		actionInProgress = new AtomicBoolean(false);
	}

	/**
	 * Executed after dependency injection is done to perform any initialization
	 */
	@PostConstruct
	void postConstruct(Composite parent) {
		createControls(parent);

		// Track some value change in context
		// Can not manage this behavior with @Inject : at this point, context is not the ContextInitializer one
		ContextInitializer.getEclipseContext().runAndTrack(new RunAndTrack() {
			@Override
			public boolean changed(IEclipseContext context) {
				if (partClosed)
					return false; // Stop tracking the change in context
				if (context.get(ContextNames.BEAM_DISPLAY_MODE) instanceof BeamDisplayMode mode)
					onBeamDisplayModeChanged(mode);
				if (context.get(ContextNames.KEEP_INVALID_BEAM_IN_RED) instanceof Boolean keepInvalidBeamInRed)
					onKeepInvalidBeamInRedChanged(keepInvalidBeamInRed);
				return true; // Call this again if a get value is modified
			}
		});
	}

	@PreDestroy
	void dispose() {
		partClosed = true;
	}

	/** Event handler for SOUNDING_VIEW_PARAMETER_CHANGED */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onDisplayChanged(
			@UIEventTopic(EventTopics.SOUNDING_VIEW_PARAMETER_CHANGED) SVDisplayParameterChanged event) {
		if (event == SVDisplayParameterChanged.eSceneMode)
			setSceneButtonState(selectionSceneModeButton, validateSceneModeButton, invalidateSceneModeButton);
		else if (event == SVDisplayParameterChanged.eSelectionMode)
			setSelectionButtonMode(quadSelectionModeButton, polygonalSelectionModeButton, lassoSelectionModeButton);
		else if (event == SVDisplayParameterChanged.eVisibleMode)
			setVisibilityButtonState(visibilityAllButton, visibilityValidButton, visibilityInvalidButton);
		else if (event == SVDisplayParameterChanged.eWidgetMode)
			setWidgetButtonState(orientedBoundingCubeButton, metricsButton, soundingsButton, landmarkButton,
					statsButton);
	}

	/** React to the change of BeamDisplayMode in context */
	private void onBeamDisplayModeChanged(BeamDisplayMode beamDisplayMode) {
		UIUtils.asyncExecSafety(comboColors, () -> comboColors.select(beamDisplayMode.ordinal()));
		UIUtils.asyncExecSafety(keepInvalidRedButton,
				() -> keepInvalidRedButton.setEnabled(beamDisplayMode != BeamDisplayMode.VALID_INVALID));
	}

	/** React to the change of keepInvalidBeamInRed flag in context */
	private void onKeepInvalidBeamInRedChanged(boolean keepInvalidBeamInRed) {
		UIUtils.asyncExecSafety(keepInvalidRedButton, () -> keepInvalidRedButton.setSelection(keepInvalidBeamInRed));
	}

	/**
	 * Create contents of the view part.
	 */
	private void createControls(Composite parent) {
		toolboxActions = ContextInitializer.make(ToolboxActions.class);
		toolboxActions.initialization(actionInProgress);

		parent.setLayout(new FillLayout(SWT.HORIZONTAL));

		ExpandBar expandBar = new ExpandBar(parent, SWT.V_SCROLL);
		ExpandItem viewExpanditem = new ExpandItem(expandBar, SWT.NONE);
		viewExpanditem.setExpanded(true);
		viewExpanditem.setText("Views");

		Composite viewComposite = new Composite(expandBar, SWT.NONE);
		viewExpanditem.setControl(viewComposite);
		viewComposite.setLayout(new GridLayout(2, true));

		lockViewButton = new Button(viewComposite, SWT.CHECK);
		lockViewButton.setToolTipText("Toogle lock view");
		lockViewButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		lockViewButton.setSelection(viewPoint.isLock());
		lockViewButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(//
				e -> toolboxActions.pushedButtonLocked(lockViewButton.getSelection())));
		lockViewButton.setText("Lock View");

		exaggerationViewButton = new Button(viewComposite, SWT.CHECK);
		exaggerationViewButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		exaggerationViewButton.setText("Vertical exaggeration");
		exaggerationViewButton.setToolTipText("Toggle vertical exaggeration");
		exaggerationViewButton.setSelection(viewPoint.isVerticalExaggeration());
		exaggerationViewButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(//
				e -> {
					toolboxActions.switchExaggeration(exaggerationViewButton.getSelection());
					e.doit = true;
				}));

		Button topViewButton = new Button(viewComposite, SWT.NONE);
		topViewButton.setToolTipText("Toggle top view");
		topViewButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		topViewButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(//
				e -> toolboxActions.pushedTopView()));
		topViewButton.setText("Top View");

		Button axisViewButton = new Button(viewComposite, SWT.NONE);
		axisViewButton.setToolTipText("Toggle axis view");
		axisViewButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		axisViewButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(//
				e -> toolboxActions.pushedAxisView()));
		axisViewButton.setText("Side View");

		viewExpanditem.setHeight(viewExpanditem.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		ExpandItem selectionModeExpandItem = new ExpandItem(expandBar, 0);
		selectionModeExpandItem.setText("Interactors");
		selectionModeExpandItem.setExpanded(true);

		Composite selectionComposite = new Composite(expandBar, SWT.NONE);
		selectionModeExpandItem.setControl(selectionComposite);
		selectionComposite.setLayout(new GridLayout(2, false));

		// Mode
		Group grpMode = new Group(selectionComposite, SWT.NONE);
		grpMode.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpMode.setText("Mode");
		grpMode.setLayout(new GridLayout(3, false));

		selectionSceneModeButton = new Button(grpMode, SWT.TOGGLE);
		selectionSceneModeButton.addSelectionListener(new SceneModeButtonListener());
		selectionSceneModeButton.setToolTipText("Select a beams area for visualization");
		selectionSceneModeButton.setSelection(true);
		selectionSceneModeButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/selection.png"));
		selectionSceneModeButton.setGrayed(true);

		validateSceneModeButton = new Button(grpMode, SWT.TOGGLE);
		validateSceneModeButton.addSelectionListener(new SceneModeButtonListener());
		validateSceneModeButton.setToolTipText("Select an area to validate beams (Enter)");
		validateSceneModeButton.setSelection(true);
		validateSceneModeButton.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/on.png"));
		validateSceneModeButton.setGrayed(true);

		invalidateSceneModeButton = new Button(grpMode, SWT.TOGGLE);
		invalidateSceneModeButton.addSelectionListener(new SceneModeButtonListener());
		invalidateSceneModeButton.setToolTipText("Select an area to invalidate beams (Del)");
		invalidateSceneModeButton.setSelection(true);
		invalidateSceneModeButton.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/off.png"));
		invalidateSceneModeButton.setGrayed(true);

		// Composite autoRefreshDTMComposite = new Composite(grpMode, SWT.NONE);
		// autoRefreshDTMComposite.setLayout(new FillLayout());
		// autoRefreshDTMComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		// autoRefreshDTMButton = new Button(autoRefreshDTMComposite, SWT.TOGGLE);
		// autoRefreshDTMButton.addSelectionListener(new SelectionAdapter() {
		// @Override
		// public void widgetSelected(SelectionEvent event) {
		// toolboxActions.toggleAutoRefreshDTM(autoRefreshDTMButton.getSelection());
		// }
		// });
		// autoRefreshDTMButton
		// .setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/view_refresh.png"));
		// autoRefreshDTMButton.setToolTipText("Auto refresh DTM");

		Group grpSelection = new Group(selectionComposite, SWT.NONE);
		grpSelection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpSelection.setText("Selection");
		grpSelection.setLayout(new GridLayout(3, false));

		quadSelectionModeButton = new Button(grpSelection, SWT.TOGGLE);
		quadSelectionModeButton.addSelectionListener(new SelectionModeButtonListener());
		quadSelectionModeButton.setToolTipText("Square selection tool");
		quadSelectionModeButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/selection_rectangular.png"));

		polygonalSelectionModeButton = new Button(grpSelection, SWT.TOGGLE);
		polygonalSelectionModeButton.addSelectionListener(new SelectionModeButtonListener());
		polygonalSelectionModeButton.setToolTipText("Polygon selection tool");
		polygonalSelectionModeButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/selection_polygon.png"));

		lassoSelectionModeButton = new Button(grpSelection, SWT.TOGGLE);
		lassoSelectionModeButton.addSelectionListener(new SelectionModeButtonListener());
		lassoSelectionModeButton.setToolTipText("Lasso selection tool");
		lassoSelectionModeButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/selection_lasso.png"));

		selectionModeExpandItem.setHeight(selectionModeExpandItem.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		ExpandItem displayExpanditem = new ExpandItem(expandBar, SWT.NONE);
		displayExpanditem.setExpanded(true);
		displayExpanditem.setText("Display");

		Composite displayComposite = new Composite(expandBar, SWT.NONE);
		displayExpanditem.setControl(displayComposite);
		displayComposite.setLayout(new GridLayout(2, false));

		LabelFactory.newLabel(SWT.None)//
				.text("Visibility :")//
				.layoutData(new GridData(SWT.END, SWT.CENTER, false, false, 1, 1))//
				.create(displayComposite);

		Composite visibilityComposite = new Composite(displayComposite, SWT.NONE);
		visibilityComposite.setLayout(GridLayoutFactory.fillDefaults().numColumns(3).margins(0, 0).create());
		visibilityComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		visibilityAllButton = new Button(visibilityComposite, SWT.TOGGLE);
		visibilityAllButton.setToolTipText("Display all beams (with and without validity)");
		visibilityAllButton.addSelectionListener(new VisibilityButtonListener());
		visibilityAllButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/visibilityAll.png"));

		visibilityValidButton = new Button(visibilityComposite, SWT.TOGGLE);
		visibilityValidButton.setToolTipText("Display only valid beams");
		visibilityValidButton.addSelectionListener(new VisibilityButtonListener());
		visibilityValidButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/visibilityValid.png"));

		visibilityInvalidButton = new Button(visibilityComposite, SWT.TOGGLE);
		visibilityInvalidButton.setToolTipText("Display only invalid beams");
		visibilityInvalidButton.addSelectionListener(new VisibilityButtonListener());
		visibilityInvalidButton
				.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/visibilityInvalid.png"));

		LabelFactory.newLabel(SWT.None)//
				.text("Color :")//
				.layoutData(new GridData(SWT.END, SWT.CENTER, false, false, 1, 1))//
				.create(displayComposite);
		Map<BeamDisplayMode, String> allColorTooltips = Map.of(//
				BeamDisplayMode.VALID_INVALID, "Color displayed beams by validity status", //
				BeamDisplayMode.REFLECTIVITY, "Color displayed beams by reflectivity attribute", //
				BeamDisplayMode.FILE, "Color displayed beams by their associated input file", //
				BeamDisplayMode.BEAM_NUMBER, "Color displayed beams by their index", //
				BeamDisplayMode.CYCLE_NUMBER, "Color displayed beams by their pings", //
				BeamDisplayMode.DETECTION_TYPE,
				"Color displayed beams by their detection attribute (amplitude or phase)", //
				BeamDisplayMode.DEPTH, "Color displayed beams by depth attribute", //
				BeamDisplayMode.QUALITY_FACTOR, "Color displayed beams by quality factor attribute"//
		);

		comboColors = new Combo(displayComposite, SWT.READ_ONLY);
		comboColors.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		comboColors.setItems(
				Arrays.stream(BeamDisplayMode.values()).map(BeamDisplayMode::getLabel).toArray(String[]::new));
		comboColors.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			BeamDisplayMode mode = BeamDisplayMode.values()[comboColors.getSelectionIndex()];
			comboColors.setToolTipText(allColorTooltips.get(mode));
			toolboxActions.changeColorMode(mode);
		}));
		BeamDisplayMode beamDisplayMode = (BeamDisplayMode) dtmPreferences.getBeamDisplayMode().getValue();
		comboColors.select(beamDisplayMode.ordinal());

		new Label(displayComposite, SWT.NONE);
		keepInvalidRedButton = new Button(displayComposite, SWT.CHECK);
		keepInvalidRedButton.setText("Keep invalid red");
		keepInvalidRedButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			toolboxActions.changeKeepInvalidRedButton(keepInvalidRedButton.getSelection());
		}));
		keepInvalidRedButton.setSelection(dtmPreferences.getKeepInvalidBeamInRed().getValue());
		keepInvalidRedButton.setEnabled(beamDisplayMode != BeamDisplayMode.VALID_INVALID);

		LabelFactory.newLabel(SWT.None)//
				.text("Size :")//
				.layoutData(new GridData(SWT.END, SWT.CENTER, false, false, 1, 1))//
				.create(displayComposite);

		Scale pointSizeScale = new Scale(displayComposite, SWT.NONE);
		pointSizeScale.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		pointSizeScale.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			toolboxActions.changeBeamSize(pointSizeScale);
			e.doit = true;
		}));
		pointSizeScale.setToolTipText("Change the size of points which represent beams");
		pointSizeScale.setMaximum(10);
		pointSizeScale.setMinimum(1);
		pointSizeScale.setSelection(1);

		ButtonFactory.newButton(SWT.PUSH)//
				.text("Manage Files")//
				.layoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1))//
				.onSelect(e -> toolboxActions.launchManageFiles())//
				.create(displayComposite);

		displayExpanditem.setHeight(displayExpanditem.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		ExpandItem toolsExpandItem = new ExpandItem(expandBar, 0);
		toolsExpandItem.setText("Tools");

		Composite toolsComposite = new Composite(expandBar, SWT.NONE);
		toolsExpandItem.setControl(toolsComposite);
		toolsComposite.setLayout(new GridLayout(1, false));

		Button biasCorrectionButton = new Button(toolsComposite, SWT.NONE);
		biasCorrectionButton.setToolTipText("Open the bias correction tool");
		biasCorrectionButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		biasCorrectionButton.addSelectionListener(
				SelectionListener.widgetSelectedAdapter(e -> toolboxActions.launchBiasCorrection()));
		biasCorrectionButton.setText("Bias Correction");

		Button filTriButton = new Button(toolsComposite, SWT.NONE);
		filTriButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		filTriButton.setText("Filter by triangulation");
		filTriButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toolboxActions.launchTriangulationFiltering();
			}
		});

		Button validateButton = new Button(toolsComposite, SWT.NONE);
		validateButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		validateButton.setText("Validate/Invalidate");
		validateButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toolboxActions.launchValidating();
			}
		});

		Button summaryButton = new Button(toolsComposite, SWT.NONE);
		summaryButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		summaryButton.setToolTipText("Open a summary view of the selected beams");
		summaryButton.setText("Beams selection summary");
		summaryButton.addListener(SWT.Selection, event -> toolboxActions.launchBeamsSelectionSummary());

		Button exportFlagsButton = new Button(toolsComposite, SWT.NONE);
		exportFlagsButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		exportFlagsButton.setToolTipText("Export the validity flags to CSV files");
		exportFlagsButton.setText("Export validity flags");
		exportFlagsButton.addListener(SWT.Selection,
				event -> toolboxActions.exportValidityFlags(exportFlagsButton.getText()));

		toolsExpandItem.setHeight(toolsExpandItem.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		ExpandItem widgetsDisplayExpanditem = new ExpandItem(expandBar, SWT.NONE);
		widgetsDisplayExpanditem.setText("Widgets display");

		Composite widgetsDisplayComposite = new Composite(expandBar, SWT.NONE);
		widgetsDisplayExpanditem.setControl(widgetsDisplayComposite);
		widgetsDisplayComposite.setLayout(new GridLayout(1, false));

		orientedBoundingCubeButton = new Button(widgetsDisplayComposite, SWT.CHECK);
		orientedBoundingCubeButton.setToolTipText("Toggle display of navigation cube which encompasses all the beams");
		orientedBoundingCubeButton.setText("Metrics box");
		orientedBoundingCubeButton.addListener(SWT.Selection,
				event -> toolboxActions.toggleOrientedBoundingCubeDisplay(((Button) event.widget).getSelection()));

		metricsButton = new Button(widgetsDisplayComposite, SWT.CHECK);
		metricsButton.setToolTipText("Toogle display of bounding cubes metrics");
		metricsButton.setText("Boxes legend");
		metricsButton.addListener(SWT.Selection,
				event -> toolboxActions.toggleMetricsDisplay(((Button) event.widget).getSelection()));

		soundingsButton = new Button(widgetsDisplayComposite, SWT.CHECK);
		soundingsButton.setToolTipText("Toggle display of all the beams");
		soundingsButton.setText("Soundings");
		soundingsButton.addListener(SWT.Selection,
				event -> toolboxActions.toggleSoundingsDisplay(((Button) event.widget).getSelection()));

		landmarkButton = new Button(widgetsDisplayComposite, SWT.CHECK);
		landmarkButton.setToolTipText("Toggle display of the compass");
		landmarkButton.setText("Landmark");
		landmarkButton.addListener(SWT.Selection,
				event -> toolboxActions.toggleLandmarkDisplay(((Button) event.widget).getSelection()));

		statsButton = new Button(widgetsDisplayComposite, SWT.CHECK);
		statsButton.setToolTipText("Toggle display of information about your data model selection");
		statsButton.setText("Statitics");
		statsButton.addListener(SWT.Selection,
				event -> toolboxActions.toggleStatisticsDisplay(((Button) event.widget).getSelection()));

		widgetsDisplayExpanditem
				.setHeight(widgetsDisplayExpanditem.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		setInitialSceneState(selectionSceneModeButton, validateSceneModeButton, invalidateSceneModeButton);

		setInitialSelectionMode(quadSelectionModeButton, polygonalSelectionModeButton, lassoSelectionModeButton);
		setInitialVisibilityState(visibilityAllButton, visibilityValidButton, visibilityInvalidButton);
		setWidgetButtonState(orientedBoundingCubeButton, metricsButton, soundingsButton, landmarkButton, statsButton);
		setInitialPointSize(pointSizeScale);

		depthAxisExpanditem = new ExpandItem(expandBar, SWT.NONE);
		depthAxisExpanditem.setText("Depth axis bounds");

		depthAxisComposite = new Composite(expandBar, SWT.NONE);
		depthAxisExpanditem.setControl(depthAxisComposite);
		depthAxisComposite.setLayout(new GridLayout(3, false));

		btnAutoScale = new Button(depthAxisComposite, SWT.CHECK);
		btnAutoScale.setSelection(true);
		btnAutoScale.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		btnAutoScale.setText("Auto scale");
		btnAutoScale.addListener(SWT.Selection, event -> {
			bottomDepthBoundValue.setEnabled(!btnAutoScale.getSelection());
			lblMin.setEnabled(!btnAutoScale.getSelection());
			lblM2.setEnabled(!btnAutoScale.getSelection());

			topDepthBoundValue.setEnabled(!btnAutoScale.getSelection());
			lblMax.setEnabled(!btnAutoScale.getSelection());
			lblM1.setEnabled(!btnAutoScale.getSelection());

			lblStep.setEnabled(!btnAutoScale.getSelection());
			stepDepthValue.setEnabled(!btnAutoScale.getSelection());
			lblM3.setEnabled(!btnAutoScale.getSelection());

			btnNewButton.setEnabled(!btnAutoScale.getSelection());

			toolboxActions.toggleDepthAutoScale();
		});

		lblMax = new Label(depthAxisComposite, SWT.NONE);
		lblMax.setEnabled(false);
		lblMax.setText("Top:");

		topDepthBoundValue = new Text(depthAxisComposite, SWT.BORDER | SWT.RIGHT);
		topDepthBoundValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		topDepthBoundValue.setEnabled(false);

		lblM1 = new Label(depthAxisComposite, SWT.NONE);
		lblM1.setEnabled(false);
		lblM1.setText("m");

		lblMin = new Label(depthAxisComposite, SWT.NONE);
		lblMin.setEnabled(false);
		lblMin.setText("Bottom:");

		bottomDepthBoundValue = new Text(depthAxisComposite, SWT.BORDER | SWT.RIGHT);
		bottomDepthBoundValue.setEnabled(false);
		GridData gbBottomDepthBoundValue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gbBottomDepthBoundValue.widthHint = 78;
		bottomDepthBoundValue.setLayoutData(gbBottomDepthBoundValue);

		lblM2 = new Label(depthAxisComposite, SWT.NONE);
		lblM2.setEnabled(false);
		lblM2.setText("m");

		lblStep = new Label(depthAxisComposite, SWT.NONE);
		lblStep.setEnabled(false);
		lblStep.setText("Step:");

		stepDepthValue = new Text(depthAxisComposite, SWT.BORDER | SWT.RIGHT);
		stepDepthValue.setEnabled(false);
		stepDepthValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		lblM3 = new Label(depthAxisComposite, SWT.NONE);
		lblM3.setEnabled(false);
		lblM3.setText("m");

		btnNewButton = new Button(depthAxisComposite, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toolboxActions.toggleDepthManualScale(bottomDepthBoundValue.getText(), topDepthBoundValue.getText(),
						stepDepthValue.getText());
			}
		});
		btnNewButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		btnNewButton.setText("Apply manual scale");

		depthAxisExpanditem.setHeight(depthAxisExpanditem.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	private class SceneModeButtonListener extends SelectionAdapter {
		@Override
		public void widgetSelected(SelectionEvent e) {
			Button button = (Button) e.widget;
			toolboxActions.changeSceneMode(() -> toogleButtons(button), button);
			e.doit = true;
		}
	}

	private class SelectionModeButtonListener extends SelectionAdapter {
		@Override
		public void widgetSelected(SelectionEvent e) {
			Button button = (Button) e.widget;
			toolboxActions.changeSelectionMode(() -> toogleButtons(button), button);
			e.doit = true;
		}
	}

	private class VisibilityButtonListener extends SelectionAdapter {
		@Override
		public void widgetSelected(SelectionEvent e) {
			Button button = (Button) e.widget;
			toolboxActions.changeVisibility(() -> toogleButtons(button), button);
			e.doit = true;
		}
	}

	/**
	 * Set initial values of scene buttons.
	 */
	private void setInitialSceneState(Button selectionSceneModeButton, Button validateSceneModeButton,
			Button invalidateSceneModeButton) {
		setSceneButtonState(selectionSceneModeButton, validateSceneModeButton, invalidateSceneModeButton);
		selectionSceneModeButton.setData(SoundingsSceneMode.class.getSimpleName(), SoundingsSceneMode.SELECT);
		validateSceneModeButton.setData(SoundingsSceneMode.class.getSimpleName(), SoundingsSceneMode.VALIDATE);
		invalidateSceneModeButton.setData(SoundingsSceneMode.class.getSimpleName(), SoundingsSceneMode.INVALIDATE);
	}

	/**
	 * Set initial status of scene buttons.
	 */
	private void setSceneButtonState(Button selectionSceneModeButton, Button validateSceneModeButton,
			Button invalidateSceneModeButton) {
		Display.getDefault().syncExec(() -> {
			try {
				SoundingsSceneMode soundingsSceneMode = (SoundingsSceneMode) dtmPreferences.getSoundingSceneMode()
						.getValue();
				selectionSceneModeButton.setSelection(soundingsSceneMode == SoundingsSceneMode.SELECT);
				validateSceneModeButton.setSelection(soundingsSceneMode == SoundingsSceneMode.VALIDATE);
				invalidateSceneModeButton.setSelection(soundingsSceneMode == SoundingsSceneMode.INVALIDATE);

				// autoRefreshDTMButton.setSelection(dtmPreferences.getAutoRefreshDTM().getValue().booleanValue());
			} catch (SWTException e) {
				// Case when toolbox is not displayed. No need to update its graphics components status.
			}
		});
	}

	/**
	 * Set initial values of selection buttons.
	 */
	private void setInitialSelectionMode(Button quadSelectionModeButton, Button polygonSelectionModeButton,
			Button lassoSelectionModeButton) {
		setSelectionButtonMode(quadSelectionModeButton, polygonSelectionModeButton, lassoSelectionModeButton);
		quadSelectionModeButton.setData(SelectionMode.class.getSimpleName(), SelectionMode.QUAD);
		polygonSelectionModeButton.setData(SelectionMode.class.getSimpleName(), SelectionMode.POLYGON);
		lassoSelectionModeButton.setData(SelectionMode.class.getSimpleName(), SelectionMode.LASSO);
	}

	/**
	 * Set initial status of selection buttons.
	 */
	private void setSelectionButtonMode(Button quadSelectionModeButton, Button polygonSelectionModeButton,
			Button lassoSelectionModeButton) {
		Display.getDefault().syncExec(() -> {
			try {
				Enum<?> selectionMode = dtmPreferences.getSelectionMode().getValue();
				quadSelectionModeButton.setSelection(selectionMode == SelectionMode.QUAD);
				polygonSelectionModeButton.setSelection(selectionMode == SelectionMode.POLYGON);
				lassoSelectionModeButton.setSelection(selectionMode == SelectionMode.LASSO);
			} catch (SWTException e) {
				// Case when toolbox is not displayed. No need to update its graphics components status.
			}
		});
	}

	/**
	 * Set initial values of visibility buttons.
	 */
	private void setInitialVisibilityState(Button visibilityAllButton, Button visibilityValidButton,
			Button visibilityInvalidButton) {
		setVisibilityButtonState(visibilityAllButton, visibilityValidButton, visibilityInvalidButton);
		visibilityAllButton.setData(BeamVisibleMode.class.getSimpleName(), BeamVisibleMode.ALL);
		visibilityValidButton.setData(BeamVisibleMode.class.getSimpleName(), BeamVisibleMode.VALID_ONLY);
		visibilityInvalidButton.setData(BeamVisibleMode.class.getSimpleName(), BeamVisibleMode.INVALID_ONLY);
	}

	/**
	 * Set initial status of visibility buttons.
	 */
	private void setVisibilityButtonState(Button visibilityAllButton, Button visibilityValidButton,
			Button visibilityInvalidButton) {
		Display.getDefault().syncExec(() -> {
			Enum<?> beamVisibleMode = dtmPreferences.getBeamVisibleMode().getValue();
			try {
				visibilityAllButton.setSelection(beamVisibleMode == BeamVisibleMode.ALL);
				visibilityValidButton.setSelection(beamVisibleMode == BeamVisibleMode.VALID_ONLY);
				visibilityInvalidButton.setSelection(beamVisibleMode == BeamVisibleMode.INVALID_ONLY);
			} catch (SWTException e) {
				// Case when toolbox is not displayed. No need to update its graphics components status.
			}
		});
	}

	/**
	 * Update selection status of all buttons relatives to widget display.
	 */
	public void setWidgetButtonState(Button orientedBoundingCubeButton, Button metricsButtonText,
			Button soundingsButton, Button landmarkButton, Button statsButton) {
		Display.getDefault().syncExec(() -> {
			metricsButtonText.setSelection(dtmPreferences.getMetricsText().getValue());
			orientedBoundingCubeButton.setSelection(dtmPreferences.getOrientedBoundingCube().getValue());
			soundingsButton.setSelection(dtmPreferences.getSoundings().getValue());
			landmarkButton.setSelection(dtmPreferences.getLandmark().getValue());
			statsButton.setSelection(dtmPreferences.getStatistics().getValue());
		});
	}

	/**
	 * Set initial values of color buttons
	 */
	private void setInitialPointSize(Scale pointSizeScale) {
		int beamSize = dtmPreferences.getBeamSize().getValue().intValue();
		pointSizeScale.setSelection(beamSize);
	}

	/**
	 * Toggle buttons on a selection.
	 */
	private void toogleButtons(Button selectedButton) {
		Control[] buttons = selectedButton.getParent().getChildren();
		for (Control control : buttons)
			if (control instanceof Button button)
				button.setSelection(control == selectedButton);
	}

	/** Event handler for SOUNDING_MODEL_CHANGED */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onModelChanged(@UIEventTopic(EventTopics.SOUNDING_MODEL_CHANGED) SoundingEvent event) {
		if (event == SoundingEvent.eViewPointChanged) {
			exaggerationViewButton.setSelection(viewPoint.isVerticalExaggeration());
			lockViewButton.setSelection(viewPoint.isLock());
		}
	}

}
