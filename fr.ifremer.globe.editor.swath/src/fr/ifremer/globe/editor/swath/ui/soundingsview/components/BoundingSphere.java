package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.AxisAlignedSoundingBoundingBox;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;

public class BoundingSphere extends GraphicComponent {

	private GLU glu;
	private GLUquadric obj;

	@Override
	public void init(GLAutoDrawable drawable, ContextGL2x context) {

	}

	@Override
	public void draw(GL2 gl, Context context, SceneState sceneState) {
		glu = new GLU();
		obj = glu.gluNewQuadric();
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);
		AxisAlignedSoundingBoundingBox boundingBox = soundingsModel.getSoundingBB().getActiveBB();
		gl.glPushMatrix();
		gl.glColor3d(0, 0, 0.5);
		gl.glScaled(1. / viewPoint.getScalingFactor().getX(), 1. / viewPoint.getScalingFactor().getY(),
				1. / viewPoint.getScalingFactor().getZ());
		double distanceMax = new Vector3D(boundingBox.getDeltaX() * viewPoint.getScalingFactor().getX(),
				boundingBox.getDeltaY() * viewPoint.getScalingFactor().getY(),
				boundingBox.getDeltaZ() * viewPoint.getScalingFactor().getZ()).getMagnitude();

		glu.gluSphere(obj, distanceMax / 2, 100, 100);
		gl.glPopMatrix();
	}

}
