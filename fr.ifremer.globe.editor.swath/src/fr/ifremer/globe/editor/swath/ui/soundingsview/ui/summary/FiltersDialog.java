package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary;

import java.util.Arrays;
import java.util.function.DoubleFunction;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.nebula.widgets.formattedtext.DoubleFormatter;
import org.eclipse.nebula.widgets.formattedtext.FloatFormatter;
import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.nebula.widgets.formattedtext.IntegerFormatter;
import org.eclipse.nebula.widgets.formattedtext.NumberFormatter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionType;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.ui.databinding.property.FormattedTextProperty;

/**
 * Dialog which allows user to define some filters for the {@link SummaryView#tableData}
 */
public class FiltersDialog extends Dialog {

	/** Constants */
	protected static final String MAX = "Max :";
	protected static final String MIN = "Min :";
	protected static final String FLOAT_FORMAT = "-#.###";
	protected static final String DOUBLE_FORMAT = "-#.#######";
	protected static final String INTEGER_FORMAT = "-#";

	/** Binding context */
	protected DataBindingContext dataBindingContext;

	/** Widgets */
	protected Text minCycleIndexText;
	protected Text maxCycleIndexText;
	protected Text minBeamIndexText;
	protected Text maxBeamIndexText;
	protected Text minDepthText;
	protected Text maxDepthText;
	protected Text minReflectivityText;
	protected Text maxReflectivityText;
	protected ComboViewer validityComboViewer;
	protected Combo validityCombo;
	protected ComboViewer detectionComboViewer;
	protected Combo detectionCombo;
	protected Text minLongitudeText;
	protected Text maxLongitudeText;
	protected Text minLatitudeText;
	protected Text maxLatitudeText;

	/** Model */
	protected final FilterPreferences filterPrefecences;

	/** Constructor */
	public FiltersDialog(Shell parentShell, FilterPreferences filterPrefecences) {
		super(parentShell);
		setShellStyle(SWT.CLOSE);
		this.filterPrefecences = filterPrefecences;
	}

	/** {@inheritDoc} */
	@Override
	protected void configureShell(Shell shell) {
		shell.setImage(ResourceManager.getPluginImage(ContextNames.PLUGIN_ID, "icons/16/games_config_background.png"));
		super.configureShell(shell);
		shell.setText("Filters settings");
	}

	/** {@inheritDoc} */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Apply filter", true);
		createButton(parent, IDialogConstants.ABORT_ID, "Reset fields", false);
		createButton(parent, IDialogConstants.CLIENT_ID, "Bounds", false);
	}

	/** {@inheritDoc} */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.ABORT_ID) {
			filterPrefecences.reset();
			dataBindingContext.updateTargets();
		} else if (buttonId == IDialogConstants.CLIENT_ID) {
			filterPrefecences.setToBounds();
			dataBindingContext.updateTargets();
		}
		super.buttonPressed(buttonId);
	}

	/** {@inheritDoc} */
	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		initializeFilters(parent);
		dataBindingContext = initDataBindings();
		fitFormatters();
		fitBindings();
		dataBindingContext.updateTargets();
		return parent;
	}

	/** Defines all formatters */
	protected void fitFormatters() {
		ILatLongFormatter latLongFormatter = LatLongFormater.getFormatter();
		addFormatter(minCycleIndexText, new IntegerFormatter(INTEGER_FORMAT));
		addFormatter(maxCycleIndexText, new IntegerFormatter(INTEGER_FORMAT));
		addFormatter(minBeamIndexText, new IntegerFormatter(INTEGER_FORMAT));
		addFormatter(maxBeamIndexText, new IntegerFormatter(INTEGER_FORMAT));
		addFormatter(minDepthText, new FloatFormatter(FLOAT_FORMAT));
		addFormatter(maxDepthText, new FloatFormatter(FLOAT_FORMAT));
		addFormatter(minReflectivityText, new FloatFormatter(FLOAT_FORMAT));
		addFormatter(maxReflectivityText, new FloatFormatter(FLOAT_FORMAT));
		addFormatter(minLongitudeText, newLonlatFormatter(latLongFormatter::formatLongitude));
		addFormatter(maxLongitudeText, newLonlatFormatter(latLongFormatter::formatLongitude));
		addFormatter(minLatitudeText, newLonlatFormatter(latLongFormatter::formatLatitude));
		addFormatter(maxLatitudeText, newLonlatFormatter(latLongFormatter::formatLatitude));
	}

	/** Defines all bindings */
	protected void fitBindings() {
		bind(minCycleIndexText, filterPrefecences.getMinCycleIndex(), Integer.class);
		bind(maxCycleIndexText, filterPrefecences.getMaxCycleIndex(), Integer.class);
		bind(minBeamIndexText, filterPrefecences.getMinBeamIndex(), Integer.class);
		bind(maxBeamIndexText, filterPrefecences.getMaxBeamIndex(), Integer.class);
		bind(minDepthText, filterPrefecences.getMinDepth(), Float.class);
		bind(maxDepthText, filterPrefecences.getMaxDepth(), Float.class);
		bind(minReflectivityText, filterPrefecences.getMinReflectivity(), Float.class);
		bind(maxReflectivityText, filterPrefecences.getMaxReflectivity(), Float.class);
		bind(minLongitudeText, filterPrefecences.getMinLongitude(), Double.class);
		bind(maxLongitudeText, filterPrefecences.getMaxLongitude(), Double.class);
		bind(minLatitudeText, filterPrefecences.getMinLatitude(), Double.class);
		bind(maxLatitudeText, filterPrefecences.getMaxLatitude(), Double.class);
	}

	/** Set a NumberFormatter to the Text */
	protected void addFormatter(Text target, NumberFormatter formatter) {
		FormattedText formattedText = new FormattedText(target);
		formatter.setFixedLengths(false, false);
		formattedText.setFormatter(formatter);
	}

	/** Instanciate a NumberFormatter for a Longitude or Latitude value */
	protected NumberFormatter newLonlatFormatter(DoubleFunction<String> latlonToString) {
		return new DoubleFormatter(DOUBLE_FORMAT) {
			@Override
			public String getDisplayString() {
				Double value = (Double) getValue();
				return value != null ? latlonToString.apply(value) : "";
			}
		};
	}

	/** Bind a Text to the model */
	protected <T extends Number> void bind(Text target, WritableValue<T> model, Class<T> type) {
		// Binding
		IObservableValue<T> observeTextMinDepthValueObserveWidget = new FormattedTextProperty<>(type).observe(target);
		dataBindingContext.bindValue(observeTextMinDepthValueObserveWidget, model);
	}

	/**
	 * Initialize the filters area of the {@link SummaryView}.
	 */
	public void initializeFilters(Composite parent) {

		Composite filtersComposite = new Composite(parent, SWT.NONE);
		filtersComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		filtersComposite.setLayout(new GridLayout(2, true));

		Group cycleIndexGroup = new Group(filtersComposite, SWT.NONE);
		cycleIndexGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		cycleIndexGroup.setText("Cycle Index filter");
		cycleIndexGroup.setLayout(new GridLayout(4, false));

		Label minCycleIndexLabel = new Label(cycleIndexGroup, SWT.NONE);
		minCycleIndexLabel.setText(MIN);

		minCycleIndexText = new Text(cycleIndexGroup, SWT.BORDER);
		GridData gdMinCycleIndexText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMinCycleIndexText.minimumWidth = 80;
		minCycleIndexText.setLayoutData(gdMinCycleIndexText);

		Label maxCycleIndexLabel = new Label(cycleIndexGroup, SWT.NONE);
		maxCycleIndexLabel.setText(MAX);

		maxCycleIndexText = new Text(cycleIndexGroup, SWT.BORDER);
		GridData gdMaxCycleIndexText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMaxCycleIndexText.minimumWidth = 80;
		maxCycleIndexText.setLayoutData(gdMaxCycleIndexText);

		Group beamIndexGroup = new Group(filtersComposite, SWT.NONE);
		GridData gdBeamIndexGroup = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gdBeamIndexGroup.minimumWidth = 80;
		beamIndexGroup.setLayoutData(gdBeamIndexGroup);
		beamIndexGroup.setText("Beam Index filter");
		beamIndexGroup.setLayout(new GridLayout(4, false));

		Label minBeamIndexLabel = new Label(beamIndexGroup, SWT.NONE);
		minBeamIndexLabel.setText(MIN);

		minBeamIndexText = new Text(beamIndexGroup, SWT.BORDER);
		GridData gdMinBeamIndexText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMinBeamIndexText.minimumWidth = 80;
		minBeamIndexText.setLayoutData(gdMinBeamIndexText);

		Label maxBeamIndexLabel = new Label(beamIndexGroup, SWT.NONE);
		maxBeamIndexLabel.setText(MAX);

		maxBeamIndexText = new Text(beamIndexGroup, SWT.BORDER);
		GridData gdMaxBeamIndexText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMaxBeamIndexText.minimumWidth = 80;
		maxBeamIndexText.setLayoutData(gdMaxBeamIndexText);

		Group depthGroup = new Group(filtersComposite, SWT.NONE);
		depthGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		depthGroup.setText("Depth filter (m)");
		depthGroup.setLayout(new GridLayout(4, false));

		Label minDepthLabel = new Label(depthGroup, SWT.NONE);
		minDepthLabel.setText(MIN);

		minDepthText = new Text(depthGroup, SWT.BORDER);
		GridData gdMinDepthText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMinDepthText.minimumWidth = 80;
		minDepthText.setLayoutData(gdMinDepthText);

		Label maxDepthLabel = new Label(depthGroup, SWT.NONE);
		maxDepthLabel.setText(MAX);

		maxDepthText = new Text(depthGroup, SWT.BORDER);
		GridData gdMaxDepthText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMaxDepthText.minimumWidth = 80;
		maxDepthText.setLayoutData(gdMaxDepthText);

		Group reflectivityGroup = new Group(filtersComposite, SWT.NONE);
		reflectivityGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		reflectivityGroup.setText("Reflectivity filter");
		reflectivityGroup.setLayout(new GridLayout(4, false));

		Label minReflectivityLabel = new Label(reflectivityGroup, SWT.NONE);
		minReflectivityLabel.setText(MIN);

		minReflectivityText = new Text(reflectivityGroup, SWT.BORDER);
		GridData gdMinReflectivityText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMinReflectivityText.minimumWidth = 80;
		minReflectivityText.setLayoutData(gdMinReflectivityText);

		Label maxReflectivityLabel = new Label(reflectivityGroup, SWT.NONE);
		maxReflectivityLabel.setText(MAX);

		maxReflectivityText = new Text(reflectivityGroup, SWT.BORDER);
		GridData gdMaxReflectivityText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gdMaxReflectivityText.minimumWidth = 80;
		maxReflectivityText.setLayoutData(gdMaxReflectivityText);

		Group longitudeGroup = new Group(filtersComposite, SWT.NONE);
		longitudeGroup.setText("Longitude");
		longitudeGroup.setLayout(new GridLayout(4, false));
		longitudeGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label minLongitudeLabel = new Label(longitudeGroup, SWT.NONE);
		minLongitudeLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		minLongitudeLabel.setText(MIN);

		minLongitudeText = new Text(longitudeGroup, SWT.BORDER);
		GridData gdMinLongitudeText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdMinLongitudeText.minimumWidth = 80;
		minLongitudeText.setLayoutData(gdMinLongitudeText);

		Label maxLongitudeLabel = new Label(longitudeGroup, SWT.NONE);
		maxLongitudeLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		maxLongitudeLabel.setText(MAX);

		maxLongitudeText = new Text(longitudeGroup, SWT.BORDER);
		GridData gdMaxLongitudeText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdMaxLongitudeText.minimumWidth = 80;
		maxLongitudeText.setLayoutData(gdMaxLongitudeText);

		Group latitudePosition = new Group(filtersComposite, SWT.NONE);
		latitudePosition.setLayout(new GridLayout(4, false));
		latitudePosition.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		latitudePosition.setText("Latitude");

		Label minLatitudeLabel = new Label(latitudePosition, SWT.NONE);
		minLatitudeLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		minLatitudeLabel.setText(MIN);

		minLatitudeText = new Text(latitudePosition, SWT.BORDER);
		GridData gdMinLatitudeText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdMinLatitudeText.minimumWidth = 80;
		minLatitudeText.setLayoutData(gdMinLatitudeText);

		Label maxLatitudeLabel = new Label(latitudePosition, SWT.NONE);
		maxLatitudeLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		maxLatitudeLabel.setText(MAX);

		maxLatitudeText = new Text(latitudePosition, SWT.BORDER);
		GridData gdMaxLatitudeText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdMaxLatitudeText.minimumWidth = 80;
		maxLatitudeText.setLayoutData(gdMaxLatitudeText);

		Group validityAndDetectionAndEditableGroup = new Group(filtersComposite, SWT.NONE);
		validityAndDetectionAndEditableGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		validityAndDetectionAndEditableGroup.setLayout(new GridLayout(2, false));

		Label validityLabel = new Label(validityAndDetectionAndEditableGroup, SWT.NONE);
		validityLabel.setText("Validity :");

		validityCombo = new Combo(validityAndDetectionAndEditableGroup, SWT.READ_ONLY);
		validityCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		validityComboViewer = new ComboViewer(validityCombo);
		validityComboViewer.setLabelProvider(new LabelProvider() {

			/** {@inheritDoc} */
			@Override
			public String getText(Object validity) {
				return ((MappingValidity) validity).toUserString();
			}
		});
		validityComboViewer.setContentProvider(new ArrayContentProvider());
		validityComboViewer.setInput(Arrays.asList(MappingValidity.SND_UNVALID_ACQUIS, MappingValidity.SND_UNVALID_AUTO,
				MappingValidity.SND_UNVALID_MANUAL, MappingValidity.SND_UNVALID_CONVERSION, MappingValidity.SND_VALID,
				MappingValidity.SND_UNKNOWN));

		Label detectionLabel = new Label(validityAndDetectionAndEditableGroup, SWT.NONE);
		detectionLabel.setText("Detection :");

		detectionCombo = new Combo(validityAndDetectionAndEditableGroup, SWT.READ_ONLY);
		detectionCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		detectionComboViewer = new ComboViewer(detectionCombo);
		detectionComboViewer.setLabelProvider(new LabelProvider() {
			/** {@inheritDoc} */
			@Override
			public String getText(Object detection) {
				return detection.equals(DetectionType.AMPLITUDE.get()) ? "Amplitude" : "Phase";
			}
		});
		detectionComboViewer.setContentProvider(new ArrayContentProvider());
		detectionComboViewer.setInput(Arrays.asList(DetectionType.AMPLITUDE.get(), DetectionType.PHASE.get()));
	}

	// Generated code

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue<?> observeSingleSelectionValidityCombo = ViewerProperties.singleSelection()
				.observe(validityComboViewer);
		bindingContext.bindValue(observeSingleSelectionValidityCombo, filterPrefecences.getValidity(), null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT));
		//
		IObservableValue<?> observeSelectionValidityComboObserveWidget = ViewerProperties.singleSelection()
				.observe(detectionComboViewer);
		bindingContext.bindValue(observeSelectionValidityComboObserveWidget, filterPrefecences.getDetection(), null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT));
		//
		return bindingContext;
	}
}
