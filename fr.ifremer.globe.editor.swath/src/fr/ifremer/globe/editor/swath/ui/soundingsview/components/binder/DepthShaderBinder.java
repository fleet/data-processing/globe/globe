/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.Buffer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Binder for depth shader.
 */
public class DepthShaderBinder extends AShaderBinder {

	/** Min and max binder. */
	protected MinMaxShaderBinder<Float> minMaxShaderBinder = null;

	/** {@inheritDoc} */
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {

		// Min and max values
		if (minMaxShaderBinder == null) {
			minMaxShaderBinder = new MinMaxShaderBinder<>((float) soundingModel.getSoundingBB().getActiveBB().getMinZ(),
					(float) soundingModel.getSoundingBB().getActiveBB().getMaxZ());
			minMaxShaderBinder.setShaderProgram(getShaderProgram());
		}
		minMaxShaderBinder.bindUniforms(lineBuffer);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindAttributes(SelectionModel,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes, java.util.function.Consumer)
	 */
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		super.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);

		int offsetInFile = (int) lineBuffer.getOffsetInFile() * Mappings.COORD_COUNT;
		int count = lineBuffer.getDetectionCount() * Mappings.COORD_COUNT;

		Supplier<Buffer> supplier = () -> lineBuffer.getSpatialIndex().getMappings().getCoordsArray()
				.asFloatBuffer(offsetInFile, count);
		VertexBuffer valueVertexBuffer = new VertexBufferGL2x3x(supplier.get(), BufferHint.StaticDraw,
				count * Float.BYTES);

		VertexBufferAttribute valueVertexBufferAttribute = new VertexBufferAttribute(valueVertexBuffer,
				ComponentDatatype.Float, 1, false, Float.BYTES * 2, Float.BYTES * 3);
		int valueAttributeLocation = getShaderProgram().getVertexAttributes().get("in_value").getLocation();
		vertexBufferAttributes.set(valueAttributeLocation, valueVertexBufferAttribute);

		alertVertexBufferCreated(listener, new BufferRefresher(valueVertexBuffer, supplier));
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#clear()
	 */
	@Override
	public void clear() {
		super.clear();
		if (minMaxShaderBinder != null) {
			minMaxShaderBinder.clear();
		}
		minMaxShaderBinder = null;
	}

}
