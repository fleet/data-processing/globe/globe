package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import java.awt.Color;
import java.awt.Font;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.awt.TextRenderer;

import fr.ifremer.globe.ogl.core.matrices.Matrix4D;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;

/**
 * Draw the landmark
 * 
 */
public class LandmarkComponent extends GraphicComponent {

	protected GLU glu = new GLU();
	TextRenderer smallRenderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 10));

	@Override
	public void init(GLAutoDrawable drawable, ContextGL2x context) {

	}

	@Override
	public void draw(GL2 gl, Context context, SceneState sceneState) {
		int width = (int) context.getViewport().getWidth();
		int height = (int) context.getViewport().getHeight();
		gl.glViewport(0, 0, width, height);

		// Reset the projection matrix
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();

		// Reset the modelview matrix
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glPushMatrix();

		// On screen position
		gl.glTranslated(-1.0, -1.0, 0.0);
		// keepAspectRatio(gl, w, h);
		float min = Math.min(width, height);
		gl.glOrtho(-width / min, width / min, -height / min, height / min, -1, 1);

		gl.glTranslated(0.2, 0.2, 0.0);

		// Apply camera rotation
		Matrix4D matrix = sceneState.getViewMatrix();
		double[] values = matrix.getRotationMatrix().getReadOnlyColumnMajorValues().clone();
		gl.glMultMatrixd(values, 0);

		gl.glDisable(GL.GL_DEPTH_TEST); // disable Depth Testing

		gl.glPushMatrix();

		// x axis
		drawAxis(gl, width, height, Color.RED, "E");
		// y axis
		gl.glPushMatrix();
		gl.glRotated(90, 0, 0, 1);
		drawAxis(gl, width, height, Color.GREEN, "N");
		gl.glPopMatrix();
		// z axis
		gl.glPushMatrix();
		gl.glRotated(-90, 0, 1, 0);
		drawAxis(gl, width, height, Color.BLUE, "U");
		gl.glPopMatrix();

		gl.glPopMatrix();

		gl.glPopMatrix();

	}

	private void drawAxis(GL gl1, int w, int h, Color color, String text) {
		/** Project / unproject */
		int viewport[] = new int[4];
		double mvmatrix[] = new double[16];
		double projmatrix[] = new double[16];
		double txtCoord[] = new double[4];
		GL2 gl = gl1.getGL2();
		gl.glPushMatrix();

		float size = 0.15f;
		float offset = 0.05f;

		gl.glColor3f(color.getRed(), color.getGreen(), color.getBlue());
		// axe
		gl.glLineWidth(2);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(size, 0.0f, 0.0f);
		gl.glEnd();
		gl.glLineWidth(1);
		// cylinder
		GLUquadric qobj = glu.gluNewQuadric();
		gl.glPushMatrix();
		gl.glTranslatef(size, 0, 0);
		gl.glRotated(90, 0.0f, 1.0f, 0.0f);
		glu.gluCylinder(qobj, size / 15, 0, size / 6, 8, 1);
		gl.glPopMatrix();
		// text
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		glu.gluProject(size + offset, 0f, 0f, mvmatrix, 0, projmatrix, 0, viewport, 0, txtCoord, 0);
		smallRenderer.beginRendering(w, h);
		smallRenderer.setColor(color);
		smallRenderer.draw(text, (int) txtCoord[0] - 2, (int) txtCoord[1] - 2);
		smallRenderer.endRendering();

		gl.glPopMatrix();
	}

}
