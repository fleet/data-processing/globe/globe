package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Rectangle2D;
import java.text.NumberFormat;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.util.awt.TextRenderer;

import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;

public class StatisticsComponents extends GraphicComponent {
	private NumberFormat nf;
	private TextRenderer renderer;

	public StatisticsComponents() {
		nf = NumberFormat.getIntegerInstance();
		renderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 12));
	}

	protected TextRenderer getRenderer() {
		return renderer;
	}

	@Override
	public void init(GLAutoDrawable drawable, ContextGL2x context) {
	}

	@Override
	public void draw(GL2 gl, Context context, SceneState sceneState) {
		if (dtmPreferences.getStatistics().getValue()) {
			int w = (int) context.getViewport().getWidth();
			int h = (int) context.getViewport().getHeight();

			getRenderer().beginRendering(w, h);

			String label = "displayed : ";
			label += nf.format(soundingsModel.getSoundingBB().getActiveBB().getSoundingsCount()) + " points";

			// if (drawable.getAnimator() != null) {
			// label += " FPS : " + drawable.getAnimator().getLastFPS();
			// }
			Rectangle2D rect = getRenderer().getBounds(label);
			getRenderer().setColor(Color.red);
			getRenderer().draw(label, 5, (int) (rect.getHeight() - 5));

			getRenderer().endRendering();
		}
	}
}
