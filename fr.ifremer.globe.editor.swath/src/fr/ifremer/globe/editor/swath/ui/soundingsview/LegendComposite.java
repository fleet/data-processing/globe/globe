/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.function.Supplier;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.window.ToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.FileStat;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel.FileAppearance;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ValuesBoundings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.utils.TransformationUtils;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

/**
 * Swt composite to display the legend of the soundings view.
 */
public class LegendComposite extends Composite {

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingsModel;

	/** Observer of preference changes. */
	protected Observer preferencesObserver;

	/** Formatter */
	protected static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.00");
	/** Formatter */
	protected static final DecimalFormat INTEGER_FORMAT = new DecimalFormat("#0");

	/** UI components. */
	protected Label validLabel;
	protected Label invalidLabel;
	protected Label invalidAcqLabel;
	protected Label validColorLabel;
	protected Label invalidColorLabel;
	protected Label invalidAcqColorLabel;
	protected StackLayout stackLayout = new StackLayout();
	protected Composite validComposite;
	protected Composite depthComposite;
	protected Composite reflectivityComposite;
	protected Composite fileComposite;
	protected Composite beamComposite;
	protected Composite pingComposite;
	protected Composite detectionComposite;
	protected Label lblValidityFrom;
	protected CLabel lblValityColoration;
	protected Label lblValityTo;

	protected Label lblBeamFrom;
	protected CLabel lblBeamColoration;
	protected Label lblBeamTo;
	protected Label lblPingFrom;
	protected CLabel lblPingColoration;
	protected Label lblPingTo;
	protected Label lblDepthFrom;
	protected CLabel lblDepthColoration;
	protected Label lblDepthTo;
	protected Label lblDetectionAmplitudeColor;
	protected Label lblDetectionAmplitude;
	protected Label lblDetectionPhaseColor;
	protected Label lblDetectionPhase;
	protected Label lblDetectionInvalidColor;
	protected Label lblDetectionInvalid;
	private Composite qualityFactorComposite;
	private Label lblQualityFactorFrom;
	private CLabel lblQualityFactorColoration;
	private Label lblQualityFactorTo;

	/**
	 * Constructor.
	 */
	public LegendComposite(Composite parent, int style) {
		super(parent, style);
		ContextInitializer.inject(this);

		createUI();
		// stackLayout.topControl = validComposite;

		initPreferences();
		initLabels();
		refresh();
	}

	/** Free ressources. */
	public void free() {
		dtmPreferences.getBackgroundColor().deleteObserver(preferencesObserver);
		dtmPreferences.getForegroundColor().deleteObserver(preferencesObserver);
		dtmPreferences.getValidColor().deleteObserver(preferencesObserver);
		dtmPreferences.getInvalidColor().deleteObserver(preferencesObserver);
		dtmPreferences.getInvalidColorAcqu().deleteObserver(preferencesObserver);
		dtmPreferences.getBeamDisplayMode().deleteObserver(preferencesObserver);
		dtmPreferences.getAmplitudeDetectionColor().deleteObserver(preferencesObserver);
		dtmPreferences.getPhaseDetectionColor().deleteObserver(preferencesObserver);
		preferencesObserver = null;
	}

	/**
	 * Make UI.<br>
	 * Edit with Window Builder.
	 */
	protected void createUI() {

		setLayout(stackLayout);

		validComposite = new Composite(this, SWT.NONE);
		validComposite.setLayout(new GridLayout(6, false));

		validLabel = new Label(validComposite, SWT.NONE);
		validLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		validLabel.setText("Valid");

		validColorLabel = new Label(validComposite, SWT.BORDER);
		validColorLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		validColorLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
		validColorLabel.setText("   ");

		invalidLabel = new Label(validComposite, SWT.NONE);
		GridData gdInvalidLabel = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gdInvalidLabel.horizontalIndent = 30;
		invalidLabel.setLayoutData(gdInvalidLabel);
		invalidLabel.setText("Invalid");

		invalidColorLabel = new Label(validComposite, SWT.BORDER);
		invalidColorLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		invalidColorLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
		invalidColorLabel.setText("   ");

		invalidAcqLabel = new Label(validComposite, SWT.NONE);
		GridData gdInvalidAcqLabel = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gdInvalidAcqLabel.horizontalIndent = 30;
		invalidAcqLabel.setLayoutData(gdInvalidAcqLabel);
		invalidAcqLabel.setText("Invalid Acquisition");

		invalidAcqColorLabel = new Label(validComposite, SWT.BORDER);
		invalidAcqColorLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		invalidAcqColorLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
		invalidAcqColorLabel.setText("   ");

		reflectivityComposite = new Composite(this, SWT.NONE);
		reflectivityComposite.setLayout(new GridLayout(3, false));

		lblValidityFrom = new Label(reflectivityComposite, SWT.NONE);
		lblValidityFrom.setText("0.00");

		lblValityColoration = new CLabel(reflectivityComposite, SWT.NONE);
		GridData gdLblValityColoration = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdLblValityColoration.horizontalIndent = 10;
		lblValityColoration.setLayoutData(gdLblValityColoration);
		lblValityColoration.setText("");

		lblValityTo = new Label(reflectivityComposite, SWT.NONE);
		GridData gdLblValityTo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblValityTo.horizontalIndent = 10;
		lblValityTo.setLayoutData(gdLblValityTo);
		lblValityTo.setText("0.00");

		fileComposite = new Composite(this, SWT.NONE);
		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
		rowLayout.spacing = 10;
		fileComposite.setLayout(rowLayout);

		createFilesComposite();

		beamComposite = new Composite(this, SWT.NONE);
		beamComposite.setLayout(new GridLayout(3, false));

		lblBeamFrom = new Label(beamComposite, SWT.NONE);
		lblBeamFrom.setText("0");

		lblBeamColoration = new CLabel(beamComposite, SWT.NONE);
		GridData gdLblBeamColoration = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdLblBeamColoration.horizontalIndent = 10;
		lblBeamColoration.setLayoutData(gdLblBeamColoration);
		lblBeamColoration.setText("");

		lblBeamTo = new Label(beamComposite, SWT.NONE);
		GridData gdLblBeamTo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblBeamTo.horizontalIndent = 10;
		lblBeamTo.setLayoutData(gdLblBeamTo);
		lblBeamTo.setText("1");

		pingComposite = new Composite(this, SWT.NONE);
		pingComposite.setLayout(new GridLayout(3, false));

		lblPingFrom = new Label(pingComposite, SWT.NONE);
		lblPingFrom.setText("0");

		lblPingColoration = new CLabel(pingComposite, SWT.NONE);
		GridData gdLblPingColoration = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdLblPingColoration.horizontalIndent = 10;
		lblPingColoration.setLayoutData(gdLblPingColoration);
		lblPingColoration.setText("");

		lblPingTo = new Label(pingComposite, SWT.NONE);
		GridData gdLblPingTo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblPingTo.horizontalIndent = 10;
		lblPingTo.setLayoutData(gdLblPingTo);
		lblPingTo.setText("1");

		detectionComposite = new Composite(this, SWT.NO_REDRAW_RESIZE);
		detectionComposite.setLayout(new GridLayout(6, false));

		lblDetectionAmplitudeColor = new Label(detectionComposite, SWT.NONE);
		lblDetectionAmplitudeColor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		lblDetectionAmplitudeColor.setText("    ");

		lblDetectionAmplitude = new Label(detectionComposite, SWT.NONE);
		lblDetectionAmplitude.setText("Amplitude");

		lblDetectionPhaseColor = new Label(detectionComposite, SWT.NONE);
		GridData gdLblDetectionPhaseColor = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblDetectionPhaseColor.horizontalIndent = 10;
		lblDetectionPhaseColor.setLayoutData(gdLblDetectionPhaseColor);
		lblDetectionPhaseColor.setText("    ");

		lblDetectionPhase = new Label(detectionComposite, SWT.NONE);
		lblDetectionPhase.setText("Phase");

		lblDetectionInvalidColor = new Label(detectionComposite, SWT.NONE);
		GridData gdLblDetectionInvalidColor = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblDetectionInvalidColor.horizontalIndent = 10;
		lblDetectionInvalidColor.setLayoutData(gdLblDetectionInvalidColor);
		lblDetectionInvalidColor.setText("    ");

		lblDetectionInvalid = new Label(detectionComposite, SWT.NONE);
		lblDetectionInvalid.setText("Invalid");

		depthComposite = new Composite(this, SWT.NONE);
		depthComposite.setLayout(new GridLayout(3, false));

		lblDepthFrom = new Label(depthComposite, SWT.NONE);
		lblDepthFrom.setText("0");

		lblDepthColoration = new CLabel(depthComposite, SWT.NONE);
		GridData gdLblDepthColoration = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdLblDepthColoration.horizontalIndent = 10;
		lblDepthColoration.setLayoutData(gdLblDepthColoration);
		lblDepthColoration.setText("");

		lblDepthTo = new Label(depthComposite, SWT.NONE);
		GridData gdLblDepthTo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblDepthTo.horizontalIndent = 10;
		lblDepthTo.setLayoutData(gdLblDepthTo);
		lblDepthTo.setText("1");

		qualityFactorComposite = new Composite(this, SWT.NONE);
		qualityFactorComposite.setLayout(new GridLayout(3, false));

		lblQualityFactorFrom = new Label(qualityFactorComposite, SWT.NONE);
		lblQualityFactorFrom.setText("0");

		lblQualityFactorColoration = new CLabel(qualityFactorComposite, SWT.NONE);
		GridData gdLblQualityFactorColoration = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdLblQualityFactorColoration.horizontalIndent = 10;
		lblQualityFactorColoration.setLayoutData(gdLblQualityFactorColoration);
		lblQualityFactorColoration.setText("");

		lblQualityFactorTo = new Label(qualityFactorComposite, SWT.NONE);
		GridData gdLblQualityFactorTo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblQualityFactorTo.horizontalIndent = 10;
		lblQualityFactorTo.setLayoutData(gdLblQualityFactorTo);
		lblQualityFactorTo.setText("1");

	}

	/** Set initial value and color to labels */
	protected void initLabels() {
		Shell shell = lblValityColoration.getShell();

		lblValityColoration.setBackground(new Color[] { shell.getDisplay().getSystemColor(SWT.COLOR_BLACK),
				shell.getDisplay().getSystemColor(SWT.COLOR_WHITE) }, new int[] { 100 });

		Color[] colors = new Color[100];
		int[] percents = new int[colors.length - 1];
		double percent = 100.0 / colors.length;
		for (int i = 0; i < colors.length; i++) {
			java.awt.Color awtColor = TransformationUtils
					.getColorFromNormalizedElevation((double) i / (double) colors.length);
			colors[i] = ColorUtils.convertAWTToSWT(awtColor);
			if (i > 0) {
				percents[i - 1] = (int) percent;
				percent = Math.min(100.0, percent + 100.0 / colors.length);
			}
		}

		// Tooltips
		ValuesBoundings valueBoundings = soundingsModel.getValueBoundings();
		lblBeamColoration.setBackground(colors, percents);
		lblBeamColoration.addMouseTrackListener(
				new TooltipSupplier(valueBoundings::getMinBeamIndex, valueBoundings::getMaxBeamIndex));
		lblPingColoration.setBackground(colors, percents);
		lblPingColoration.addMouseTrackListener(
				new TooltipSupplier(valueBoundings::getMinCycleIndex, valueBoundings::getMaxCycleIndex));
		lblDepthColoration.setBackground(colors, percents);
		lblDepthColoration
				.addMouseTrackListener(new TooltipSupplier(valueBoundings::getMinDepth, valueBoundings::getMaxDepth));
		lblValityColoration.addMouseTrackListener(
				new TooltipSupplier(valueBoundings::getMinReflectivity, valueBoundings::getMaxReflectivity));
		lblQualityFactorColoration.setBackground(colors, percents);
		lblQualityFactorColoration.addMouseTrackListener(
				new TooltipSupplier(valueBoundings::getMinQualityFactor, valueBoundings::getMaxQualityFactor));

		// Free ressources
		lblBeamColoration.addDisposeListener(disposeEvent -> Arrays.stream(colors).forEach(Color::dispose));
	}

	/** Hook preferences. */
	protected void initPreferences() {
		preferencesObserver = (Observable o, Object arg) -> refresh();

		dtmPreferences.getBackgroundColor().addObserver(preferencesObserver);
		dtmPreferences.getForegroundColor().addObserver(preferencesObserver);
		dtmPreferences.getValidColor().addObserver(preferencesObserver);
		dtmPreferences.getInvalidColor().addObserver(preferencesObserver);
		dtmPreferences.getInvalidColorAcqu().addObserver(preferencesObserver);
		dtmPreferences.getBeamDisplayMode().addObserver(preferencesObserver);
		dtmPreferences.getAmplitudeDetectionColor().addObserver(preferencesObserver);
		dtmPreferences.getPhaseDetectionColor().addObserver(preferencesObserver);
	}

	/** One preference has changed. Refresh the legend. */
	protected void refresh() {
		UIUtils.asyncExecSafety(this, () -> {
			// Background
			setBackground(getPreferedBackground());
			validComposite.setBackground(getPreferedBackground());
			depthComposite.setBackground(getPreferedBackground());
			reflectivityComposite.setBackground(getPreferedBackground());
			fileComposite.setBackground(getPreferedBackground());
			beamComposite.setBackground(getPreferedBackground());
			pingComposite.setBackground(getPreferedBackground());
			detectionComposite.setBackground(getPreferedBackground());

			// Valid/Invalid colors

			validColorLabel.setBackground(ColorUtils.convertGColorToSWT(dtmPreferences.getValidColor().getValue()));
			invalidColorLabel.setBackground(ColorUtils.convertGColorToSWT(dtmPreferences.getInvalidColor().getValue()));
			invalidAcqColorLabel
					.setBackground(ColorUtils.convertGColorToSWT(dtmPreferences.getInvalidColorAcqu().getValue()));

			// Foreground
			validLabel.setForeground(getPreferedForeground());
			validColorLabel.setForeground(getPreferedForeground());
			invalidLabel.setForeground(getPreferedForeground());
			invalidColorLabel.setForeground(getPreferedForeground());
			invalidAcqLabel.setForeground(getPreferedForeground());
			invalidAcqColorLabel.setForeground(getPreferedForeground());
			lblValidityFrom.setForeground(getPreferedForeground());
			lblValityColoration.setForeground(getPreferedForeground());
			lblValityTo.setForeground(getPreferedForeground());
			lblBeamFrom.setForeground(getPreferedForeground());
			lblBeamColoration.setForeground(getPreferedForeground());
			lblBeamTo.setForeground(getPreferedForeground());
			lblPingFrom.setForeground(getPreferedForeground());
			lblPingColoration.setForeground(getPreferedForeground());
			lblPingTo.setForeground(getPreferedForeground());
			lblDetectionAmplitudeColor.setForeground(getPreferedForeground());
			lblDetectionAmplitude.setForeground(getPreferedForeground());
			lblDetectionPhaseColor.setForeground(getPreferedForeground());
			lblDetectionInvalidColor.setForeground(getPreferedForeground());
			lblDetectionInvalid.setForeground(getPreferedForeground());
			lblDepthFrom.setForeground(getPreferedForeground());
			lblDepthTo.setForeground(getPreferedForeground());
			lblDetectionPhase.setForeground(getPreferedForeground());
			lblQualityFactorFrom.setForeground(getPreferedForeground());
			lblQualityFactorTo.setForeground(getPreferedForeground());

			// Beam
			BeamDisplayMode beamDisplayMode = (BeamDisplayMode) dtmPreferences.getBeamDisplayMode().getValue();
			if (beamDisplayMode == BeamDisplayMode.REFLECTIVITY) {
				stackLayout.topControl = reflectivityComposite;
			} else if (beamDisplayMode == BeamDisplayMode.FILE) {
				createFilesComposite();
				stackLayout.topControl = fileComposite;
			} else if (beamDisplayMode == BeamDisplayMode.BEAM_NUMBER) {
				stackLayout.topControl = beamComposite;
			} else if (beamDisplayMode == BeamDisplayMode.DETECTION_TYPE) {
				stackLayout.topControl = detectionComposite;
			} else if (beamDisplayMode == BeamDisplayMode.CYCLE_NUMBER) {
				stackLayout.topControl = pingComposite;
			} else if (beamDisplayMode == BeamDisplayMode.DEPTH) {
				stackLayout.topControl = depthComposite;
			} else if (beamDisplayMode == BeamDisplayMode.QUALITY_FACTOR) {
				stackLayout.topControl = qualityFactorComposite;
			} else {
				stackLayout.topControl = validComposite;
			}

			requestLayout();
			// Detection
			lblDetectionAmplitudeColor.setBackground(
					ColorUtils.convertGColorToSWT(dtmPreferences.getAmplitudeDetectionColor().getValue()));
			lblDetectionPhaseColor
					.setBackground(ColorUtils.convertGColorToSWT(dtmPreferences.getPhaseDetectionColor().getValue()));
			lblDetectionInvalidColor
					.setBackground(ColorUtils.convertGColorToSWT(dtmPreferences.getInvalidColor().getValue()));
		});
	}

	/** Event handler for SOUNDING_MODEL_CHANGED */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onModelChanged(@UIEventTopic(EventTopics.SOUNDING_MODEL_CHANGED) SoundingEvent event) {
		if (event == SoundingEvent.eDataLoaded) {
			Display.getDefault().asyncExec(() -> {
				ValuesBoundings valueBoundings = soundingsModel.getValueBoundings();
				// Reflectivity
				lblValidityFrom.setText(DECIMAL_FORMAT.format(valueBoundings.getMinReflectivity()));
				lblValityTo.setText(DECIMAL_FORMAT.format(valueBoundings.getMaxReflectivity()));
				reflectivityComposite.layout();
				// Files
				createFilesComposite();
				// Beam
				lblBeamFrom.setText(INTEGER_FORMAT.format(valueBoundings.getMinBeamIndex()));
				lblBeamTo.setText(INTEGER_FORMAT.format(valueBoundings.getMaxBeamIndex()));
				beamComposite.layout();
				// Ping
				lblPingFrom.setText(INTEGER_FORMAT.format(valueBoundings.getMinCycleIndex()));
				lblPingTo.setText(INTEGER_FORMAT.format(valueBoundings.getMaxCycleIndex()));
				pingComposite.layout();
				// Depth
				lblDepthFrom.setText(DECIMAL_FORMAT.format(soundingsModel.getSoundingBB().getActiveBB().getMinZ()));
				lblDepthTo.setText(DECIMAL_FORMAT.format(soundingsModel.getSoundingBB().getActiveBB().getMaxZ()));
				depthComposite.layout();
				// Quality factor
				lblQualityFactorFrom.setText(DECIMAL_FORMAT.format(valueBoundings.getMinQualityFactor()));
				lblQualityFactorTo.setText(DECIMAL_FORMAT.format(valueBoundings.getMaxQualityFactor()));
				qualityFactorComposite.layout();
			});
		}
	}

	protected void createFilesComposite() {
		for (Control kid : fileComposite.getChildren()) {
			kid.dispose();
		}

		List<SounderDataContainer> sounderDataContainers = soundingsModel.getSounderDataContainers();
		for (int fileIdx = 0; fileIdx < sounderDataContainers.size(); fileIdx++) {
			SounderDataContainer dataContainer = sounderDataContainers.get(fileIdx);
			FileStat stat = soundingsModel.getFileStat(dataContainer.getInfo());
			if (stat == null || stat.getSoundingCount() <= 0) {
				continue;
			}
			FileAppearance fileAppearances = soundingsModel.getFileAppearances(fileIdx);
			if (!fileAppearances.displayed()) {
				continue;
			}
			GColor color = fileAppearances.color();
			Color swtColor = getPreferedForeground();
			if (color != null) {
				swtColor = new Color(Display.getCurrent(), color.getRed(), color.getGreen(), color.getBlue());
			}

			Label lblFileName = new Label(fileComposite, SWT.NONE);
			lblFileName.setText(new File(dataContainer.getInfo().getPath()).getName());
			lblFileName.setToolTipText(dataContainer.getInfo().getPath());
			lblFileName.setForeground(swtColor);
		}
		fileComposite.requestLayout();
	}

	/**
	 * @return the background color.
	 */
	protected Color getPreferedBackground() {
		return ColorUtils.convertGColorToSWT(dtmPreferences.getBackgroundColor().getValue());
	}

	/**
	 * @return the foreground color.
	 */
	protected Color getPreferedForeground() {
		return ColorUtils.convertGColorToSWT(dtmPreferences.getForegroundColor().getValue());
	}

	static class TooltipSupplier extends MouseTrackAdapter {
		protected Supplier<Number> minValueSupplier;
		protected Supplier<Number> maxValueSupplier;

		/**
		 * Constructor.
		 */
		public TooltipSupplier(Supplier<Number> minValueSupplier, Supplier<Number> maxValueSupplier) {
			this.minValueSupplier = minValueSupplier;
			this.maxValueSupplier = maxValueSupplier;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.swt.events.MouseTrackAdapter#mouseHover(org.eclipse.swt.events.MouseEvent)
		 */
		@Override
		public void mouseHover(MouseEvent e) {
			CLabel label = (CLabel) e.getSource();
			ToolTip toolTip = new ToolTip(label, ToolTip.NO_RECREATE, true) {

				@Override
				protected Composite createToolTipContentArea(Event event, Composite parent) {
					// Create the content area
					Composite composite = new Composite(parent, SWT.NONE);
					Color fg = parent.getDisplay().getSystemColor(SWT.COLOR_INFO_FOREGROUND);
					Color bg = parent.getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND);
					composite.setForeground(fg);
					composite.setBackground(bg);
					Text text = new Text(composite, SWT.READ_ONLY);
					text.setForeground(fg);
					text.setBackground(bg);

					float minValue = minValueSupplier.get().floatValue();
					float maxValue = maxValueSupplier.get().floatValue();

					Point size = label.getSize();
					float value = minValue + (maxValue - minValue) * ((float) e.x / size.x);
					if (minValueSupplier.get() instanceof Integer) {
						text.setText(String.valueOf((int) value));
					} else {
						text.setText(DECIMAL_FORMAT.format(value));
					}

					GridLayoutFactory.fillDefaults().margins(2, 2).generateLayout(composite);
					return composite;
				}
			};
			toolTip.setHideOnMouseDown(false);
			toolTip.setHideDelay(2000);
			toolTip.show(new Point(e.x, e.y - 15));
		}
	}
}
