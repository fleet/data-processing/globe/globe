
package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.osgi.service.event.EventHandler;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel.FileAppearance;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.viewers.CheckBoxCellLabelProvider;
import fr.ifremer.globe.ui.viewers.CheckBoxEditingSupport;
import fr.ifremer.globe.ui.viewers.ColorCellLabelProvider;
import fr.ifremer.globe.ui.viewers.ColorEditingSupport;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

public class ManageFilesViewPart {

	/** Id of the view defined in e4 model */
	public static final String PART_ID = "fr.ifremer.globe.editor.swath.partdescriptor.managefiles";

	/** model of the soundings view */
	private final SoundingsModel soundingModel;

	/** Model of files to display in the table */
	private final List<FileEntry> model = new ArrayList<>();

	private final Composite tableComposite;
	private final TableViewer fileTableViewer;

	/** Handler subscribed to IEventBroker for PartStack management */
	private final EventHandler eventHandler;

	/**
	 * Constructor
	 */
	@Inject
	public ManageFilesViewPart(IEventBroker eventBroker, Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		tableComposite = new Composite(parent, SWT.NONE);
		fileTableViewer = new TableViewer(tableComposite, SWT.FULL_SELECTION);
		soundingModel = ContextInitializer.getInContext(ContextNames.SOUNDINGS_MODEL);

		eventHandler = PartUtil.onPartStackChanged(PART_ID, eventBroker, this::onPartStackChanged);

		createUI();
		initialize();
	}

	@PreDestroy
	private void dispose(IEventBroker eventBroker) {
		eventBroker.unsubscribe(eventHandler);
	}

	private void createUI() {
		tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		TableColumnLayout tableLayout = new TableColumnLayout();
		tableComposite.setLayout(tableLayout);

		Table fileTable = fileTableViewer.getTable();
		fileTable.setHeaderVisible(true);

		TableViewerColumn tableViewerColumnFile = new TableViewerColumn(fileTableViewer, SWT.NONE);
		tableViewerColumnFile
				.setLabelProvider(ColumnLabelProvider.createTextProvider(e -> ((FileEntry) e).fileInfo.getFilename()));
		TableColumn tblclmnFile = tableViewerColumnFile.getColumn();
		tableLayout.setColumnData(tblclmnFile, new ColumnWeightData(55, 150, true));
		tblclmnFile.setText("File");

		TableViewerColumn tableViewerColumnDisplayed = new TableViewerColumn(fileTableViewer, SWT.NONE);
		tableViewerColumnDisplayed.setEditingSupport(//
				new CheckBoxEditingSupport<FileEntry>(fileTableViewer, //
						e -> e.fileAppearance.displayed(), //
						(entry, displayed) -> changeFileAppearances(entry,
								entry.fileAppearance.withDisplay(displayed))));

		tableViewerColumnDisplayed
				.setLabelProvider(new CheckBoxCellLabelProvider<FileEntry>(e -> e.fileAppearance.displayed()));
		TableColumn tblclmnDisplayed = tableViewerColumnDisplayed.getColumn();
		tableLayout.setColumnData(tblclmnDisplayed, new ColumnWeightData(15, 30, true));
		tblclmnDisplayed.setText("Displayed");

		TableViewerColumn tableViewerColumnEditable = new TableViewerColumn(fileTableViewer, SWT.NONE);
		tableViewerColumnEditable.setEditingSupport(//
				new CheckBoxEditingSupport<FileEntry>(fileTableViewer, //
						e -> e.fileAppearance.editable(), //
						(entry, editable) -> changeFileAppearances(entry,
								entry.fileAppearance.withEditable(editable))));
		tableViewerColumnEditable
				.setLabelProvider(new CheckBoxCellLabelProvider<FileEntry>(e -> e.fileAppearance.editable()));
		TableColumn tblclmnEditable = tableViewerColumnEditable.getColumn();
		tableLayout.setColumnData(tblclmnEditable, new ColumnWeightData(15, 30, true));
		tblclmnEditable.setText("Editable");

		TableViewerColumn tableViewerColumnColor = new TableViewerColumn(fileTableViewer, SWT.NONE);
		tableViewerColumnColor.setEditingSupport(new ColorEditingSupport<FileEntry>(fileTableViewer, //
				e -> e.fileAppearance.color(), //
				(entry, color) -> changeFileAppearances(entry, entry.fileAppearance.withColor(color))));
		tableViewerColumnColor.setLabelProvider(new ColorCellLabelProvider<FileEntry>(e -> e.fileAppearance.color()));
		TableColumn tblclmnColor = tableViewerColumnColor.getColumn();
		tableLayout.setColumnData(tblclmnColor, new ColumnWeightData(15, 30, true));
		tblclmnColor.setText("Color");
		fileTableViewer.setContentProvider(ArrayContentProvider.getInstance());

	}

	private void initialize() {
		model.clear();
		int fileIdx = 0;
		for (ISounderNcInfo fileInfo : soundingModel.getInfoStores()) {
			model.add(new FileEntry(fileIdx, fileInfo, soundingModel.getFileAppearances(fileIdx)));
			fileIdx++;
		}
		fileTableViewer.setInput(model);
	}

	/** A FileAppearance has changed. Update inner model and alert SoundingModel */
	private void changeFileAppearances(FileEntry entry, FileAppearance appearance) {
		soundingModel.changeFileAppearances(entry.fileIdx, appearance);
		model.set(model.indexOf(entry), entry.withFileAppearance(appearance));
		fileTableViewer.refresh();
	}

	/**
	 * Returns the <code>Display</code> that is associated with the receiver.
	 */
	public Display getDisplay() {
		return tableComposite.getDisplay();
	}

	/** Subscribe to EventBroker and reacts when view is added to a PartStack */
	private void onPartStackChanged(MPartStack partStack) {
		DtmPreference preference = ContextInitializer.getInContext(ContextNames.DTM_PREFERENCES);
		preference.getContainerId(PART_ID).setValue(partStack.getElementId(), false);
		preference.save();
	}

	public static record FileEntry(int fileIdx, IFileInfo fileInfo, FileAppearance fileAppearance) {
		public FileEntry withFileAppearance(FileAppearance fileAppearance) {
			return new FileEntry(fileIdx, fileInfo, fileAppearance);
		}
	}
}