/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components;

import java.awt.Point;
import java.awt.Polygon;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.BeamVisibleMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SelectionMode;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Polygon selection for beams
 */
public class BeamsPolygonSelectionComponent extends GraphicComponent {
	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(BeamsPolygonSelectionComponent.class);

	@Inject
	@Named(ContextNames.SOUNDINGS_MODEL)
	protected SoundingsModel soundingModel;

	/** Current selection. */
	protected Polygon polygon = new Polygon();

	/** Indicates if selection is performed. */
	protected AtomicBoolean selecting = new AtomicBoolean(false);
	/** Callback to inform that a selection has been made */
	protected Runnable selectionDoneCallback;

	/** Differents stop of the selection process. */
	protected enum BeamSelectionState {
		/** Indicating that a job is sleeping */
		SLEEPING,
		/** Indicating that a job is in development. */
		WAITING,
		/** Indicating that a job is currently running */
		RUNNING
	}

	/** Indicated that a selection highlighting is in progress. */
	protected AtomicReference<BeamSelectionState> selectionProgressState = new AtomicReference<>(
			BeamSelectionState.SLEEPING);

	/** OpenGL rendering target. */
	protected GLAutoDrawable drawable;

	/** Current job for beam selection. */
	protected CompletableFuture<Void> beamsSelectionTask;

	protected int[] viewport = new int[4];
	protected double[] mvmatrix = new double[16];
	protected double[] projmatrix = new double[16];

	/** Selection mode */
	protected SelectionMode selectionMode;
	/** Observer of preference changes. */
	protected Observer selectionModeObserver;

	/** Max number of vertex in VBO */
	protected int maxVertexCount = 100;

	/** Nb float per vertex. */
	protected static final int VERTEX_SIZE = 3; // X, Y, Z,

	/** Vertex VBO. */
	protected VBO vbo;

	/**
	 * Constructor.
	 */
	public BeamsPolygonSelectionComponent(Runnable selectionDoneCallback) {
		this.selectionDoneCallback = selectionDoneCallback;
		EnumPreferenceAttribute selectionModeAttribute = dtmPreferences.getSelectionMode();
		selectionMode = (SelectionMode) selectionModeAttribute.getValue();

		selectionModeObserver = (Observable observable,
				Object arg) -> setSelectionMode((SelectionMode) ((EnumPreferenceAttribute) observable).getValue());
		selectionModeAttribute.addObserver(selectionModeObserver);
	}

	/** Clean up. */
	@Override
	public void dispose(Context context) {

		EnumPreferenceAttribute selectionModeAttribute = dtmPreferences.getSelectionMode();
		selectionModeAttribute.deleteObserver(selectionModeObserver);

		super.dispose(context);

		try {
			if (context != null && context.makeCurrent()) {
				vbo.dispose(GLU.getCurrentGL());
				context.release();
			}

		} catch (GLException e) {
			logger.warn(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	/** Perform the beam selection inside the polygon. */
	protected void selectItemsInsidePolygon() {
		if (polygon.npoints > 2) {
			selectBeamsInsidePolygon();
		}
	}

	/** Perform the beam selection inside the specified polygon. */
	protected void selectBeamsInsidePolygon() {
		BeamSelectionState previousSelectionState = selectionProgressState.getAndSet(BeamSelectionState.WAITING);
		if (previousSelectionState == BeamSelectionState.WAITING) {
			return;
		}

		// Stop the previous selection task.
		if (beamsSelectionTask != null && previousSelectionState == BeamSelectionState.RUNNING) {
			try {
				// Wait for the end of the task
				beamsSelectionTask.get();
				// Previous job finished. selectionProgressState must be
				// SLEEPING
				selectionProgressState.set(BeamSelectionState.WAITING);
			} catch (Exception e) {
				logger.debug("Interrupted while waiting the end of the selection task : {}", e.getMessage());
				return;
			}
		}

		beamsSelectionTask = CompletableFuture.runAsync(buildSelectionTask());
	}

	/**
	 * Make the task to perform the beam selection.
	 */
	protected Runnable buildSelectionTask() {
		Polygon tmpPolygon = new Polygon(polygon.xpoints, polygon.ypoints, polygon.npoints);

		return () -> {
			try {
				if (selectionProgressState.compareAndSet(BeamSelectionState.WAITING, BeamSelectionState.RUNNING)) {
					// Selecting beams
					for (int fileIndex = 0; fileIndex < soundingsModel.getBeamsAssociation().size(); fileIndex++) {
						// Still running ?
						if (selectionProgressState.get() != BeamSelectionState.RUNNING) {
							break;
						}
						List<LineBuffer> buffers = soundingsModel.getBeamsAssociation().getBuffers(fileIndex);
						SelectionModel selectionModel = soundingsModel.getSelectionModels().get(fileIndex);
						buffers.parallelStream().forEach(
								lineBuffer -> selectBeamsOfOneLineBuffer(tmpPolygon, selectionModel, lineBuffer));
					}

					// Notifications
					if (selectionProgressState.get() == BeamSelectionState.RUNNING) {
						selectionDoneCallback.run();
					}
				}
			} finally {
				selectionProgressState.set(BeamSelectionState.SLEEPING);
			}
		};
	}

	/**
	 * Set selection flag to true for the beams belonging to the specified LineBuffer.
	 */
	protected void selectBeamsOfOneLineBuffer(Polygon tmpPolygon, SelectionModel selectionModel,
			LineBuffer lineBuffer) {
		double[] unprojCoord = new double[4];
		Vector3D center = viewPoint.getWorldCenter();
		Mappings mappings = lineBuffer.getSpatialIndex().getMappings();
		int nbBeams = lineBuffer.getDetectionCount();

		// Local GLU to this thread : glu.gluProject not thread safe
		GLU glu = new GLU();

		for (int offset = 0; offset < nbBeams; offset++) {
			// Still running ?
			if (selectionProgressState.get() != BeamSelectionState.RUNNING) {
				break;
			}

			long offsetInFile = lineBuffer.getOffsetInFile() + offset;
			double x = mappings.getX(offsetInFile);
			double y = mappings.getY(offsetInFile);
			double z = mappings.getDepth(offsetInFile);

			// check if visible first
			MappingValidity v = mappings.getValidity(offsetInFile);
			BeamVisibleMode visibleMode = soundingModel.getDisplayParameters().getVisibleMode();
			boolean isVisible = visibleMode == BeamVisibleMode.ALL//
					|| visibleMode == BeamVisibleMode.VALID_ONLY && MappingValidity.isValid(v)//
					|| visibleMode == BeamVisibleMode.INVALID_ONLY && MappingValidity.isInvalid(v);

			if (isVisible && glu.gluProject(x - center.getX(), y - center.getY(), z - center.getZ(), mvmatrix, 0,
					projmatrix, 0, viewport, 0, unprojCoord, 0)) {
				int winX = (int) unprojCoord[0];
				int winY = (int) (viewport[3] - unprojCoord[1]);
				selectionModel.setSelected(offsetInFile, tmpPolygon.contains(winX, winY));
			} else {
				selectionModel.setSelected(offsetInFile, false);
			}
		}
	}

	/**
	 * Add a point to the polygon.
	 *
	 * @param position mouse coords
	 */
	public void addPoint(Point position) {
		if (selecting.compareAndSet(false, true)) {
			reset();
		}
		if (getSelectionMode() == SelectionMode.QUAD) {
			// Quad
			if (polygon.npoints == 0) {
				polygon.addPoint(position.x, position.y);
				polygon.addPoint(position.x, position.y);
				polygon.addPoint(position.x, position.y);
				polygon.addPoint(position.x, position.y);
			} else {
				polygon.xpoints[2] = position.x;
				polygon.ypoints[2] = position.y;
				polygon.xpoints[1] = position.x;
				polygon.ypoints[3] = position.y;
			}
			polygon.invalidate();
		} else {
			// Lasso or Polygon
			if (polygon.npoints == 0) {
				polygon.addPoint(position.x, position.y);
				polygon.addPoint(position.x, position.y);
			} else {
				polygon.addPoint(position.x, position.y);
			}
		}
		selectItemsInsidePolygon();
	}

	/** Initialize selection. */
	public void reset() {
		if (polygon.npoints > 0) {
			polygon.reset();
		}
	}

	/** Removing last point. */
	public void removeLastPoint() {
		if (polygon.npoints == 1) {
			reset();
			selecting.set(false);
		} else if (polygon.npoints > 0) {
			polygon.npoints = polygon.npoints - 1;
			polygon.invalidate();
		}
		selectItemsInsidePolygon();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.GraphicComponent#draw(com.jogamp.opengl.GL2,
	 *      fr.ifremer.globe.ogl.renderer.Context, fr.ifremer.globe.ogl.renderer.scene.SceneState)
	 */
	@Override
	public void draw(GL2 gl, Context context, SceneState sceneState) {
		if (selecting.get()) {
			// Grab the current viewport
			gl.glDisable(GL.GL_DEPTH_TEST);

			gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
			// Grab the Modelview information. The Modelview Matrix
			// determines how the vertices of OpenGL primitives are
			// transformed to eye coordinates.
			gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
			// Get the Projection Matrix. The Projection Matrix transforms
			// vertices in eye coordinates to clip coordinates
			gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);

			// Initialize matrix : 2D drawing
			gl.glPushMatrix();
			gl.glMatrixMode(GL2.GL_MODELVIEW);
			gl.glLoadIdentity();
			gl.glMatrixMode(GL2.GL_PROJECTION);
			gl.glLoadIdentity();

			// Update vertices
			int pointCount = polygon.npoints; // May increase during this function
			FloatBuffer buffer = vbo.getBuffer();
			if (buffer.capacity() <= pointCount * VERTEX_SIZE + VERTEX_SIZE) {
				vbo.allocateBuffer(buffer.capacity() * 2);
				vbo.bindAndLoad(gl);
			}
			buffer.limit(pointCount * VERTEX_SIZE);
			buffer.rewind();
			for (int i = 0; i < pointCount; i++) {
				float x1 = polygon.xpoints[i];
				float y1 = polygon.ypoints[i];
				// Map window coordinates to object coordinates
				float x = 2f * (x1 / context.getViewport().width) - 1f;
				float y = 1f - 2f * (y1 / context.getViewport().height);
				buffer.put(x);
				buffer.put(y);
				buffer.put(1f);
			}
			buffer.flip();
			vbo.update(gl, 0, pointCount * 3l * Buffers.SIZEOF_FLOAT);

			// Draw
			gl.glColor4f(1f, 0f, 0f, 0.6f);
			if (getSelectionMode() != SelectionMode.QUAD)
				gl.glLineWidth(3f);
			vbo.drawVertexArray(gl, GL2.GL_LINE_LOOP, 0, pointCount);

			gl.glPopMatrix();
		}
	}

	/** Indicates if selection is performed. */
	public boolean isSelecting() {
		return selecting.get();
	}

	/** Set the mouse coords when it is moving. */
	public void setMousePoint(Point position) {
		// Update last point
		if (polygon.npoints > 1) {
			if (getSelectionMode() == SelectionMode.QUAD) {
				polygon.xpoints[2] = position.x;
				polygon.ypoints[2] = position.y;
				polygon.xpoints[1] = position.x;
				polygon.ypoints[3] = position.y;
			} else if (getSelectionMode() == SelectionMode.POLYGON) {
				polygon.xpoints[polygon.npoints - 1] = position.x;
				polygon.ypoints[polygon.npoints - 1] = position.y;
			} else {
				// Lasso
				addPoint(position);
			}
		}

		selectItemsInsidePolygon();

		if (isSelecting()) {
			drawable.display();
		}
	}

	/** Indicates if selection is performed. */
	public void stopSelection() {
		selecting.set(false);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.GraphicComponent#init(com.jogamp.opengl.GLAutoDrawable,
	 *      fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x)
	 */
	@Override
	public void init(GLAutoDrawable drawable, ContextGL2x context) {
		this.drawable = drawable;
		vbo = new VBO(VERTEX_SIZE);
		vbo.setUsage(GL.GL_DYNAMIC_DRAW);
		vbo.allocateBuffer(maxVertexCount);
		vbo.bindAndLoad(drawable.getGL());
	}

	public void waitIfRefreshing() {
		if (beamsSelectionTask != null) {
			try {
				beamsSelectionTask.get();
			} catch (Exception e) {
				logger.debug(e.getMessage());
			}
		}

	}

	/**
	 * @return the {@link #drawable}
	 */
	public GLAutoDrawable getDrawable() {
		return drawable;
	}

	/**
	 * @return the {@link #selectionMode}
	 */
	public SelectionMode getSelectionMode() {
		return selectionMode;
	}

	/**
	 * @param selectionMode the {@link #selectionMode} to set
	 */
	public void setSelectionMode(SelectionMode selectionMode) {
		this.selectionMode = selectionMode;
		reset();
	}

}
