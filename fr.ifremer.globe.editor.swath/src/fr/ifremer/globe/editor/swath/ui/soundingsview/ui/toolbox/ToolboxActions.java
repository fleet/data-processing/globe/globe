package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import org.apache.commons.lang.math.NumberUtils;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Widget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.editor.swath.SwathController;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.ui.soundingsview.BeamDisplayMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsComposite;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsViewPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.corrector.BiasCorrector;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.triangulation.TriangulationFilteringPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.validator.BeamValidatorPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.events.SoundingEvent;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.bounds.BoundingBoxes;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.BeamVisibleMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SelectionMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.SoundingsSceneMode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.biascorrection.BiasCorrectionView;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary.SummaryView;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.utils.Messages;
import jakarta.inject.Inject;
import jakarta.inject.Named;

public class ToolboxActions {

	private static final Logger logger = LoggerFactory.getLogger(ToolboxActions.class);

	private final MApplication application;
	private final EModelService modelService;
	private final EPartService partService;
	private final IEventBroker eventBroker;

	private final SoundingsViewPart soundingsView;
	private final SoundingsModel soundingsModel;
	private final SwathController swathControler;
	private final DtmPreference dtmPreferences;
	private final SoundingsController soundingsController;
	private final BiasCorrector biasCorrector;

	/** One tool selection is in progress. */
	protected AtomicBoolean actionInProgress;

	@Inject
	public ToolboxActions(MApplication application, EModelService modelService, EPartService partService,
			IEventBroker eventBroker, //
			@Named(ContextNames.SWATH_EDITOR_SOUNDINGS) SoundingsViewPart soundingsView,
			@Named(ContextNames.SOUNDINGS_MODEL) SoundingsModel soundingsModel,
			@Named(ContextNames.SWATH_CONTROLLER) SwathController swathControler,
			@Named(ContextNames.DTM_PREFERENCES) DtmPreference dtmPreferences,
			@Named(ContextNames.SOUNDINGS_CONTROLLER) SoundingsController soundingsController,
			@Named(ContextNames.BIAS_CORRECTOR) BiasCorrector biasCorrector) {
		this.application = application;
		this.soundingsView = soundingsView;
		this.soundingsModel = soundingsModel;
		this.swathControler = swathControler;
		this.dtmPreferences = dtmPreferences;
		this.soundingsController = soundingsController;
		this.biasCorrector = biasCorrector;
		this.modelService = modelService;
		this.partService = partService;
		this.eventBroker = eventBroker;
	}

	/** Mandatory initialization after construction */
	public void initialization(AtomicBoolean actionInProgress) {
		this.actionInProgress = actionInProgress;
	}

	/**
	 * Exaggeration flag changed...
	 */
	protected void switchExaggeration(boolean exaggeration) {
		soundingsController.setExaggeration(exaggeration);
	}

	protected void pushedAxisView() {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsController.getViewPointController().toMainAxis(true);
			actionInProgress.set(false);
		}
	}

	protected void pushedTopView() {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsController.getViewPointController().toTopView(true);
			actionInProgress.set(false);
		}
	}

	protected void pushedButtonLocked(boolean selected) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsController.getViewPointController().lockView(selected, true);
			actionInProgress.set(false);
		}
	}

	/**
	 * Listener on beam size.
	 */
	protected void changeBeamSize(Scale pointSizeScale) {
		if (actionInProgress.compareAndSet(false, true)) {
			dtmPreferences.getBeamSize().setValue(Double.valueOf(pointSizeScale.getSelection()), true);
			dtmPreferences.save();
			actionInProgress.set(false);
		}
	}

	/**
	 * Listener on visibility change.
	 */
	protected void changeSceneMode(Runnable runnable, Widget selectedWidget) {
		if (actionInProgress.compareAndSet(false, true)) {
			runnable.run();
			soundingsModel.getDisplayParameters().setSceneMode(
					(SoundingsSceneMode) selectedWidget.getData(SoundingsSceneMode.class.getSimpleName()));
			actionInProgress.set(false);
		}
	}

	/**
	 * Listener on selection mode change.
	 */
	protected void changeSelectionMode(Runnable runnable, Widget selectedWidget) {
		if (actionInProgress.compareAndSet(false, true)) {
			runnable.run();
			soundingsModel.getDisplayParameters()
					.setSelectionMode((SelectionMode) selectedWidget.getData(SelectionMode.class.getSimpleName()));
			actionInProgress.set(false);
		}
	}

	/**
	 * Listener on visibility change.
	 */
	protected void changeVisibility(Runnable runnable, Widget selectedWidget) {
		if (actionInProgress.compareAndSet(false, true)) {
			runnable.run();
			soundingsModel.getDisplayParameters()
					.setVisibleMode((BeamVisibleMode) selectedWidget.getData(BeamVisibleMode.class.getSimpleName()));
			actionInProgress.set(false);
		}
	}

	/**
	 * Listener on color mode change.
	 */
	protected void changeColorMode(BeamDisplayMode beamDisplayMode) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsModel.getDisplayParameters().setDisplayMode(beamDisplayMode);
			actionInProgress.set(false);
		}
	}

	protected void changeKeepInvalidRedButton(boolean keepInvalidBeamInRed) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsModel.getDisplayParameters().setKeepInvalidBeamInRed(keepInvalidBeamInRed);
			actionInProgress.set(false);
		}
	}

	/**
	 * Launches the bias correction process.
	 */
	public void launchBiasCorrection() {
		if (actionInProgress.compareAndSet(false, true))
			try {
				biasCorrector.setupProcess();

				MPart biasCorrectionPart = PartManager.createPart(partService, BiasCorrectionView.PART_ID);
				addPartToPartStack(biasCorrectionPart, () -> {
					BiasCorrectionView biasCorrectionView = (BiasCorrectionView) biasCorrectionPart.getObject();
					return biasCorrectionView.getSizeView();
				});
			} catch (IOException e) {
				logger.warn("Unable to launch Bias Correction", e);
			} finally {
				actionInProgress.set(false);
			}
	}

	/**
	 * Launches the Triangulation Filtering process.
	 */
	public void launchTriangulationFiltering() {
		if (actionInProgress.compareAndSet(false, true)) {
			MPart part = partService.findPart(TriangulationFilteringPart.PART_ID);
			if (part != null) {
				partService.bringToTop(part);
			} else {
				MPart filtriMPart = PartManager.createPart(partService, TriangulationFilteringPart.PART_ID);
				addPartToPartStack(filtriMPart, () -> {
					TriangulationFilteringPart filtriPart = (TriangulationFilteringPart) filtriMPart.getObject();
					Point viewSize = filtriPart.computeSize();
					Point cursorLocation = Display.getDefault().getCursorLocation();
					return new Rectangle(cursorLocation.x - viewSize.x, cursorLocation.y - viewSize.y - 20,
							viewSize.x + 25, viewSize.y + 80);
				});
			}
			actionInProgress.set(false);
		}
	}

	/**
	 * Launches the validate/invalidate process.
	 */
	public void launchValidating() {
		if (actionInProgress.compareAndSet(false, true)) {

			MPart part = partService.findPart(BeamValidatorPart.PART_ID);
			MPart validatorMPart;
			if (part != null) {
				partService.bringToTop(part);
			} else {
				validatorMPart = PartManager.createPart(partService, BeamValidatorPart.PART_ID);
				addPartToPartStack(validatorMPart, () -> {
					BeamValidatorPart validatorPart = (BeamValidatorPart) validatorMPart.getObject();

					Point viewSize = validatorPart.computeSize();
					Point cursorLocation = Display.getDefault().getCursorLocation();
					return new Rectangle(cursorLocation.x - viewSize.x, cursorLocation.y - viewSize.y - 20,
							viewSize.x + 25, viewSize.y + 80);
				});
			}
			actionInProgress.set(false);
		}
	}

	/**
	 * Launches the beams selection summary process.
	 */
	public void launchBeamsSelectionSummary() {
		if (actionInProgress.compareAndSet(false, true)) {
			MPart summaryViewPart = PartManager.createPart(partService, SummaryView.PART_ID);
			addPartToPartStack(summaryViewPart, () -> new Rectangle(0, 0, 100, 100));

			SummaryView summaryView = (SummaryView) summaryViewPart.getObject();
			summaryView.initialize();
			actionInProgress.set(false);
		}
	}

	public void launchManageFiles() {
		if (actionInProgress.compareAndSet(false, true))
			try {
				MPart manageFilePart = PartManager.createPart(partService, ManageFilesViewPart.PART_ID);
				addPartToPartStack(manageFilePart, () -> {
					ManageFilesViewPart manageFilesViewPart = (ManageFilesViewPart) manageFilePart.getObject();
					Point cursorLocation = manageFilesViewPart.getDisplay().getCursorLocation();
					return new Rectangle(cursorLocation.x - 500, cursorLocation.y - 200, 500, 200);
				});
			} finally {
				actionInProgress.set(false);
			}
	}

	/**
	 * Launches the export the validity flags to a CSV file.
	 */
	public void exportValidityFlags(String processName) {
		if (actionInProgress.compareAndSet(false, true)) {
			swathControler.exportValidityFlags(processName);
			actionInProgress.set(false);
		}
	}

	/**
	 * Toggles the display status of bounding cube text into the {@link SoundingsComposite}.
	 *
	 * @param widget buttons which triggers the toggle action
	 */
	public void toggleMetricsDisplay(boolean displayStatus) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsView.getSoundingsComposite().getSoundingsScene().refresh();
			soundingsModel.getDisplayParameters().setMetricsState(displayStatus);
			actionInProgress.set(false);
		}
	}

	/**
	 * Toggles the display status of oriented bounding cube into the {@link SoundingsComposite}.
	 *
	 * @param widget buttons which triggers the toggle action
	 */
	public void toggleOrientedBoundingCubeDisplay(boolean displayStatus) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsView.getSoundingsComposite().getSoundingsScene().getOboundingCube().setVisible(displayStatus);
			soundingsView.getSoundingsComposite().getSoundingsScene().refresh();
			soundingsModel.getDisplayParameters().setOrientedBoundingCubeState(displayStatus);
			actionInProgress.set(false);
		}
	}

	/**
	 * Toggles the display status of soundings into the {@link SoundingsComposite}.
	 *
	 * @param widget buttons which triggers the toggle action
	 */
	public void toggleSoundingsDisplay(boolean displayStatus) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsView.getSoundingsComposite().getSoundingsScene().getSoundings().setVisible(displayStatus);
			soundingsView.getSoundingsComposite().getSoundingsScene().refresh();
			soundingsModel.getDisplayParameters().setSoundingsState(displayStatus);
			actionInProgress.set(false);
		}
	}

	/**
	 * Toggles the display status of landmark into the {@link SoundingsComposite}.
	 *
	 * @param widget buttons which triggers the toggle action
	 */
	public void toggleLandmarkDisplay(boolean displayStatus) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsView.getSoundingsComposite().getSoundingsScene().getLandmark().setVisible(displayStatus);
			soundingsView.getSoundingsComposite().getSoundingsScene().refresh();
			soundingsModel.getDisplayParameters().setLandmarkState(displayStatus);
			actionInProgress.set(false);
		}
	}

	/**
	 * Toggles the display status of statistics into the {@link SoundingsComposite}.
	 *
	 * @param widget buttons which triggers the toggle action
	 */
	public void toggleStatisticsDisplay(boolean displayStatus) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsView.getSoundingsComposite().getSoundingsScene().getStats().setVisible(displayStatus);
			soundingsView.getSoundingsComposite().getSoundingsScene().refresh();
			soundingsModel.getDisplayParameters().setStatisticsState(displayStatus);
			actionInProgress.set(false);
		}
	}

	/**
	 * Toggles the display status of statistics into the {@link SoundingsComposite}.
	 *
	 * @param widget buttons which triggers the toggle action
	 */
	public void toggleAutoRefreshDTM(boolean autoRefreshStatus) {
		if (actionInProgress.compareAndSet(false, true)) {
			soundingsModel.getDisplayParameters().setAutoRefreshDTM(autoRefreshStatus);
			if (autoRefreshStatus)
				soundingsController.notifyViewHasToBeRefreshed();
			actionInProgress.set(false);
		}
	}

	/**
	 * Enables auto-scale option for depth axis (reset values defined by user)
	 */
	public void toggleDepthAutoScale() {
		if (actionInProgress.compareAndSet(false, true)) {
			BoundingBoxes boudingBoxes = soundingsModel.getSoundingBB();
			boudingBoxes.setAutoScale(true);
			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDataChanged);
			actionInProgress.set(false);
		}
	}

	/**
	 * Set bounding boxes parameters with values defined by user
	 */
	public void toggleDepthManualScale(String topDepthAxisString, String bottomDepthAxisString,
			String stepValueString) {
		if (actionInProgress.compareAndSet(false, true)) {

			BoundingBoxes boudingBoxes = soundingsModel.getSoundingBB();
			boudingBoxes.setAutoScale(false);

			// Min
			if (NumberUtils.isNumber(topDepthAxisString))
				boudingBoxes.setMinZDefinedByUser(Double.parseDouble(topDepthAxisString));

			// Max
			if (NumberUtils.isNumber(bottomDepthAxisString))
				boudingBoxes.setMaxZDefinedByUser(Double.parseDouble(bottomDepthAxisString));

			// Step
			if (NumberUtils.isNumber(stepValueString))
				// Check if the number of lines will not be big (to avoid buffer
				// overflow exception)
				if (boudingBoxes.getActiveBB().getDeltaZ() / Float.parseFloat(stepValueString) > 100)
					Messages.openErrorMessage("Error ",
							"The defined 'Step' is too small for theses depth axis limits, number of lines will be too large... ");
				else
					boudingBoxes.setStepZDefinedByUser(Float.parseFloat(stepValueString));

			eventBroker.send(EventTopics.SOUNDING_MODEL_CHANGED, SoundingEvent.eDataChanged);

			actionInProgress.set(false);
		}
	}

	/** Add the part to the preferred PartStack. When no PartStack is found, part is considered detached */
	private void addPartToPartStack(MPart part, Supplier<Rectangle> positionSupplier) {
		StringPreferenceAttribute prefContainerId = dtmPreferences.getContainerId(part.getElementId());
		String partStack = prefContainerId.getValue();

		if (!DtmPreference.DETACHED_VIEW.equalsIgnoreCase(partStack)) {
			// Check if stack exists
			List<MPartStack> partStacks = modelService.findElements(application, partStack, MPartStack.class);
			if (partStacks.isEmpty())
				partStack = prefContainerId.getDefaultValue();
		}

		if (partStack.equalsIgnoreCase(DtmPreference.DETACHED_VIEW)) {
			// No preferred and default stack : detach part
			PartManager.showPart(partService, part);
			Rectangle position = positionSupplier.get();
			modelService.detach(part, position.x, position.y, position.width, position.height);
		} else {
			// Add part to preferred or default stack
			PartManager.addPartToStack(part, application, modelService, partStack);
			PartUtil.showPartSashContainer(part);
			PartManager.showPart(partService, part);
		}
	}
}
