package fr.ifremer.globe.editor.swath.ui.wizard;

import java.util.List;

import fr.ifremer.globe.core.model.dtm.SounderDataElevationRasterComputer;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;

public class SwathEditorParameters {

	/** List of opened files in the swath editor */
	public final List<ISounderNcInfo> editedFiles;

	/** Geographical box of all sounding files */
	public final GeoBox lonLatGeoBox;

	/** Mercator projection centered on lonLatGeoBox */
	public final Projection projection;
	/** Mercator GeoBox using projection */
	public final GeoBox mercatorGeoBox;

	/** number of rows */
	public final int rowCount;
	/** number of columns */
	public final int columnCount;
	/** Spatial Resolution in meter of mercatorGeoBox */
	public final double spatialResolution;

	/**
	 * Constructor
	 */
	public SwathEditorParameters(List<ISounderNcInfo> editedFiles, GeoBox geoBox, Projection projection,
			GeoBox mercatorGeoBox, double spatialResolution) {
		this.editedFiles = editedFiles;
		lonLatGeoBox = geoBox;
		this.projection = projection;
		this.mercatorGeoBox = mercatorGeoBox;
		this.spatialResolution = spatialResolution;
		var dims = SounderDataElevationRasterComputer.computeDimensions(mercatorGeoBox, spatialResolution);
		rowCount = dims.getFirst();
		columnCount = dims.getSecond();
	}

	/**
	 * Creates a new intance of SwathEditorParameters.<br>
	 * Compute row and column count according the specified resolution
	 */
	public SwathEditorParameters duplicateWithSpatialResolution(double otherSpatialResolution) {
		return new SwathEditorParameters(editedFiles, lonLatGeoBox, projection, mercatorGeoBox, otherSpatialResolution);
	}

}
