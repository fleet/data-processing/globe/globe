/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.FloatBuffer;

import com.jogamp.opengl.GL;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.ogl.renderer.Device2x;

/**
 * Binder to bind a grey texture on MTU 0.
 */
public class GreyTextureBinder extends AShaderBinder {

	/** Number of color in pallette. */
	static final int PALETTE_COLOR_COUNT = 255;

	/** {@inheritDoc} */
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		super.bindUniforms(lineBuffer);

		// Initialize color uniforms
		FloatBuffer pixels = FloatBuffer.allocate(PALETTE_COLOR_COUNT * 3);
		int index = 0;
		for (int i = 0; i < PALETTE_COLOR_COUNT; i++) {
			pixels.put(index++, i / 255f);
			pixels.put(index++, i / 255f);
			pixels.put(index++, i / 255f);
		}

		Device2x.createTextureFloatRectangle(pixels, 1, PALETTE_COLOR_COUNT, GL.GL_TEXTURE_2D, GL.GL_LINEAR,
				GL.GL_LINEAR, GL.GL_RGB, GL.GL_RGB, GL.GL_FLOAT);
	}

}
