/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.IOUtils;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.action.SoundEditorAction;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.ui.undo.UndoStack;
import fr.ifremer.globe.utils.array.IByteArray;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Undo Redo manager.
 */
public class UndoRedoController implements Closeable {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(UndoRedoController.class);

	/**
	 * Number of possible Arrays in a stack. <br>
	 * We have to store UndoStack.MAX_ACTION validation/invalidation operation.<br>
	 * +1 for the original Array, before the first operation<br>
	 * +1 for le last prepared Array for a future operation
	 */
	protected static final int STACK_CAPACITY = UndoStack.MAX_ACTION + 2;

	/**
	 * Stacks of validity arrays. <br>
	 * First element in a stack is the original IByteArray retrieved from the Mappings. The next elements are the
	 * IByteArray generated for performing validate/invalidate operation. <br>
	 * usedValidityArrayIndex designates the IByteArray in the stack which is actually used in the SoundingsView.
	 */
	protected HashMap<Mappings, List<IByteArray>> validityArrayStacksByMappings = new HashMap<>();

	/**
	 * Model managed by this controller. <br>
	 * Not injected directly. See {@link #initialize}
	 */
	protected SoundingsModel soundingsModel;

	/**
	 * Index of the current active ValidityArrays in the lists of {@link #validityArrayStacksByMappings}.
	 */
	protected int usedValidityArrayIndex = -1;

	/**
	 * Index of the last saved ValidityArrays in the lists of {@link #validityArrayStacksByMappings}.
	 */
	protected int savedValidityArrayIndex = -1;

	/** Running task for preparing future validate/invalidate opration. */
	protected CompletableFuture<Void> preparingTask;

	/**
	 * After a restoration ({@link #undoUntil(int)}), this flag is set to true to force a new preparation.
	 */
	protected AtomicBoolean undoPerformed = new AtomicBoolean(false);

	/** Stack of actions */
	protected UndoStack undoStack;

	/**
	 * Constructor.
	 */
	public UndoRedoController() {
		undoStack = new UndoStack(this, UndoStack.MAX_ACTION);
	}

	@PreDestroy
	void preDestroy() {
		close();
		undoStack.clear();
	}

	/**
	 * Ensure that the preparation of the next operation has been done. If not, launch a new one.
	 */
	public void ensureNextOperationIsPrepared() {
		// Launch a new preparation if last operation was an undo...
		if (undoPerformed.compareAndSet(true, false)) {
			prepareNextOperationAsynchronously();
		}
		waitIfPreparing();
	}

	/**
	 * Waits if necessary for this preparation to complete.
	 */
	public void waitIfPreparing() {
		if (preparingTask != null) {
			try {
				preparingTask.join();
			} catch (CancellationException | CompletionException e) {
				logger.warn(e.getMessage());
			}
		}
	}

	/**
	 * Launch the preparation of the next operation in a thread.
	 */
	public void prepareNextOperationAsynchronously() {
		if (preparingTask == null || preparingTask.isDone()) {
			logger.debug("Launching a preparation task.");
			preparingTask = CompletableFuture.runAsync(() -> {
				cleanUndoneOperations();
				cloneCurrentValidityArray();
			});
		}
	}

	/**
	 * Clean all arrays attached to undone validate/invalidate operation.
	 */
	public void cleanUndoneOperations() {
		// First ValidityArray must be kept
		if (usedValidityArrayIndex >= 0) {
			for (Entry<Mappings, List<IByteArray>> validityArrayStacksEntry : validityArrayStacksByMappings
					.entrySet()) {
				// Closing validityArray of undo operation
				List<IByteArray> validityArrayStack = validityArrayStacksEntry.getValue();
				while (validityArrayStack.size() - 1 > usedValidityArrayIndex) {
					IByteArray validityArray = validityArrayStack.remove(validityArrayStack.size() - 1);
					validityArray.close();
				}
			}
		}
	}

	/**
	 * Prepare the future validate/invalidate operation
	 */
	protected void cloneCurrentValidityArray() {
		try {
			for (Entry<Mappings, List<IByteArray>> validityArrayStacksEntry : validityArrayStacksByMappings
					.entrySet()) {
				// Clone the last and add the copy to the stack
				Mappings mappings = validityArrayStacksEntry.getKey();
				IByteArray copyValidityArray = mappings.getValidityArray().cloneByteArray();
				validityArrayStacksEntry.getValue().add(copyValidityArray);

				// Change ValidityArray into Model. Next validate/invalidate
				// operation will be performed on this instance.
				mappings.setValidityArray(copyValidityArray);
			}

			// Current index will be the index of the new added clone.
			usedValidityArrayIndex++;

			// Now release the oldest and unnecessary array
			for (Entry<Mappings, List<IByteArray>> validityArrayStacksEntry : validityArrayStacksByMappings
					.entrySet()) {
				List<IByteArray> validityArrayStack = validityArrayStacksEntry.getValue();
				if (validityArrayStack.size() > STACK_CAPACITY) {
					IByteArray validityArray = validityArrayStack.set(validityArrayStack.size() - STACK_CAPACITY - 1,
							null);
					if (validityArray != null) {
						IOUtils.closeQuietly(validityArray);
					}
				}
			}
		} catch (IOException e) {
			// Error occured
			preparingTask.completeExceptionally(e);
			logger.warn("Unable to perform the Undo/Redo task : ", e);
			cleanUndoneOperations();
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() {
		closeAllValidityStacks();
	}

	/**
	 * Close all ValidityByteArray but the used one because it's belonging to a Mappings
	 */
	protected void closeAllValidityStacks() {
		int arrayCount = 0;
		for (List<IByteArray> validityStack : validityArrayStacksByMappings.values()) {
			int validityByteArrayCount = validityStack.size();
			for (int i = 0; i < validityByteArrayCount; i++) {
				if (usedValidityArrayIndex != i) {
					IOUtils.closeQuietly(validityStack.get(i));
					arrayCount++;
				}
			}
			validityStack.clear();
		}
		logger.debug("Number of closed ValidityArray {}", arrayCount);
		validityArrayStacksByMappings.clear();
	}

	/**
	 * Intialize this controler.
	 */
	@Inject
	protected void initialize(@Optional @Named(ContextNames.SOUNDINGS_MODEL) SoundingsModel soundingsModel) {
		if (soundingsModel != null) {
			for (SpatialIndex spatialIndex : soundingsModel.getBeamsAssociation().getSpatialIndex()) {
				Mappings mappings = spatialIndex.getMappings();
				ArrayList<IByteArray> validityArrays = new ArrayList<>();
				validityArrays.add(mappings.getValidityArray());
				validityArrayStacksByMappings.put(mappings, validityArrays);
			}
			usedValidityArrayIndex = 0;
			savedValidityArrayIndex = 0;
			prepareNextOperationAsynchronously();
		}
		this.soundingsModel = soundingsModel;
	}

	/**
	 * Restore the validityArrays to the specified context index.
	 */
	public void undoUntil(int actionIndex) {
		undoPerformed.set(true);
		for (Entry<Mappings, List<IByteArray>> validityArrayStacksEntry : validityArrayStacksByMappings.entrySet()) {
			List<IByteArray> validityArrayStack = validityArrayStacksEntry.getValue();
			// Check if specified index is manageable
			if (validityArrayStack.size() > actionIndex) {
				// Change ValidityArray in Mappings
				Mappings mappings = validityArrayStacksEntry.getKey();
				mappings.setValidityArray(validityArrayStack.get(actionIndex));
			} else {
				// Out of bounds. Requirement ignored
				logger.warn("Unable to restore to  ValidityArray at index {} : specified index out of stack bounds",
						actionIndex);
				return;
			}
		}
		usedValidityArrayIndex = actionIndex;
	}

	/**
	 * Mark the current validityArrays as saved.
	 */
	public void markCurentContextSaved() {
		if (undoPerformed.get()) {
			savedValidityArrayIndex = usedValidityArrayIndex;
		} else {
			savedValidityArrayIndex = usedValidityArrayIndex - 1;
		}
	}

	/**
	 * @return true if current context is saved
	 */
	public boolean isCurentContextSaved() {
		if (undoPerformed.get()) {
			return (savedValidityArrayIndex == usedValidityArrayIndex);
		} else {
			return (savedValidityArrayIndex == usedValidityArrayIndex - 1);
		}
	}

	/**
	 * Last operation has been canceled (not undone). The redo will not be possible.
	 */
	public void cancelLastPreparation() {
		if (usedValidityArrayIndex > 0) {
			undoUntil(usedValidityArrayIndex - 1);
		}
	}

	/**
	 * New action has been performed on Sounding view.<br>
	 * Launch the preparation of the next action
	 */
	@Inject
	@Optional
	void onSoundEditorAction(@UIEventTopic(EventTopics.SOUND_ACTION) SoundEditorAction soundEditorAction) {
		soundEditorAction.setActionIndex(usedValidityArrayIndex - 1);
		undoStack.push(soundEditorAction);
		prepareNextOperationAsynchronously();
	}

	/**
	 * @return the {@link #usedValidityArrayIndex}
	 */
	public int getUsedValidityArrayIndex() {
		return usedValidityArrayIndex;
	}

	/** Getter of {@link #undoStack} */
	public UndoStack getUndoStack() {
		return undoStack;
	}

}
