package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.biascorrection;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.DoubleToIntFunction;
import java.util.function.ToDoubleFunction;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.processes.biascorrection.model.CommonCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;

public class CorrectionPointComposite extends Composite {

	private WritableObject<CorrectionPoint> correctionPoint = new WritableObject<>(new CommonCorrectionPoint());

	private List<Runnable> resetFunctions = new ArrayList<>();

	/** Action invoked to inform a change occured of CorrectionPoint */
	private final Runnable doWhenCreationPointChanged;

	public CorrectionPointComposite(Composite parent, int style, Runnable doWhenCreationPointChanged) {
		super(parent, style);
		this.doWhenCreationPointChanged = doWhenCreationPointChanged;
		createUI();
	}

	public void createUI() {
		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);

		resetFunctions.add(createCorrectionSetterWidget("Sound velocity (m/s)", //
				"Sound velocity bias (m/s)", // tooltip
				0.1, new DoubleRange(-99, 99), (cp, value) -> cp.setVelocity(value), CorrectionPoint::getVelocity));

		resetFunctions.add(createCorrectionSetterWidget("Heading (°)", //
				"Platform Heading bias (°)", // tooltip
				0.1, new DoubleRange(-2.5, 2.5), (cp, value) -> cp.setHeading(value), CorrectionPoint::getHeading));

		resetFunctions.add(createCorrectionSetterWidget("Roll (°)", //
				"Platform Roll bias (°)", // tooltip
				0.01, new DoubleRange(-0.25, 0.25), (cp, value) -> cp.setRoll(value), CorrectionPoint::getRoll));

		resetFunctions.add(createCorrectionSetterWidget("Pitch (°)", //
				"Platform Pitch bias (°)", // tooltip
				0.01, new DoubleRange(-0.25, 0.25), (cp, value) -> cp.setPitch(value), CorrectionPoint::getPitch));

		resetFunctions.add(createCorrectionSetterWidget("Vertical offset (m)", //
				"Platform Vertical offset bias (m), positive upwards", // tooltip
				0.1, new DoubleRange(-10, 10), (cp, value) -> cp.setPlatformVerticalOffset(value),
				CorrectionPoint::getPlatformVerticalOffset));
		resetFunctions.add(createCorrectionSetterWidget("MRU Heading (°)", //
				"Motion Reference Unit Heading bias (°) applies roll correction due to misalignment of vertical axis between the attitude sensor(MRU) and the ship reference", // tooltip
				0.1, new DoubleRange(-5.0, 5.0), (cp, value) -> cp.setMruHeading(value),
				CorrectionPoint::getMruHeading));

		Label lblXsfOnly = new Label(this, SWT.NONE);
		lblXsfOnly.setText("Xsf only :");
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		resetFunctions.add(createCorrectionSetterWidget("Attitude Time (ms)", //
				"Attitude time delay (ms) applies heading/pitch/roll from the attitude sensor(MRU)\n at the ping time shifted by the given delay (xsf only)", //tooltip
				10, new DoubleRange(-1000, 1000), (cp, value) -> cp.setAttitudeTimeMs(value),
				CorrectionPoint::getAttitudeTimeMs));

		resetFunctions.add(createCorrectionSetterWidget("Vertical offset Time (ms)", //
				"Vertical offset time delay (ms) applies vertical offset from the attitude sensor(MRU)\n at the ping time shifted by the given delay (xsf only)", //tooltip
				10, new DoubleRange(-1000, 1000), (cp, value) -> cp.setVerticalOffsetTimeMs(value),
				CorrectionPoint::getVerticalOffsetTimeMs));

		correctionPoint.addChangeListener(e -> {
			doWhenCreationPointChanged.run();
		});
	}

	private Runnable createCorrectionSetterWidget(String name, String tooltip, double step, DoubleRange valueRange,
			BiConsumer<CorrectionPoint, Double> valueConsumer, ToDoubleFunction<CorrectionPoint> valueGetter) {
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(5, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		// check button
		Button checkboxButton = new Button(composite, SWT.CHECK);
		GridData gd_checkboxButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_checkboxButton.widthHint = 160;
		checkboxButton.setLayoutData(gd_checkboxButton);
		checkboxButton.setText(name);
		checkboxButton.setToolTipText(tooltip);

		// value spinner
		Spinner valueSpinner = new Spinner(composite, SWT.BORDER);
		GridData gd_valueSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_valueSpinner.widthHint = 40;
		valueSpinner.setLayoutData(gd_valueSpinner);
		valueSpinner.setSelection(getStyle());
		valueSpinner.setDigits(2);
		valueSpinner.setSelection(0);
		DoubleToIntFunction valueSpinnerConverter = value -> (int) Math
				.round(value * Math.pow(10, valueSpinner.getDigits()));
		valueSpinner.setMinimum(-999_999);
		valueSpinner.setMaximum(999_999);

		// hook listeners
		Runnable updateCorrectionPoint = () -> {
			var value = valueSpinner.getSelection() / Math.pow(10, valueSpinner.getDigits());
			// set correction value only if checked
			if (!checkboxButton.getSelection())
				value = Double.NaN;
			else // round value
				value = Math.round(value * Math.pow(10, 2)) / Math.pow(10, 2);

			var newCorrectionPoint = new CommonCorrectionPoint(correctionPoint.get());
			valueConsumer.accept(newCorrectionPoint, value);
			correctionPoint.doSetValue(newCorrectionPoint);
		};

		// value slider
		SliderComposite slider = new SliderComposite(composite, SWT.NONE, valueRange.getMinimumFloat(),
				valueRange.getMaximumFloat(), (float) step, false, value -> {
					valueSpinner.setSelection(valueSpinnerConverter.applyAsInt(value));
					updateCorrectionPoint.run();
				});
		slider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		slider.setValue(0);

		// step
		Label lblStep = new Label(composite, SWT.NONE);
		lblStep.setText("Step :");
		var stepSpinner = new Spinner(composite, SWT.BORDER);
		stepSpinner.setDigits(2);
		stepSpinner.addListener(SWT.Selection, e -> valueSpinner.setIncrement(stepSpinner.getSelection()));
		stepSpinner.setSelection((int) (step * Math.pow(10, stepSpinner.getDigits())));
		valueSpinner.setIncrement(stepSpinner.getSelection());

		valueSpinner.addListener(SWT.Selection, e -> {
			updateCorrectionPoint.run();
			slider.setValue((float) valueGetter.applyAsDouble(correctionPoint.get()));
		});
		Consumer<Boolean> enabledConsumer = enabled -> {
			valueSpinner.setEnabled(enabled);
			stepSpinner.setEnabled(enabled);
			slider.setEnabled(enabled);
		};
		enabledConsumer.accept(false);
		checkboxButton.addListener(SWT.Selection, e -> {
			enabledConsumer.accept(checkboxButton.getSelection());
			updateCorrectionPoint.run();
		});

		// return reset runnable
		return () -> {
			checkboxButton.setSelection(false);
			enabledConsumer.accept(false);
			valueSpinner.setSelection(0);
			slider.setValue(0);
		};
	}

	public CorrectionPoint getCurrentCorrectionPoint() {
		return correctionPoint.get();
	}

	public void clear() {
		resetFunctions.forEach(Runnable::run);
		correctionPoint.doSetValue(new CommonCorrectionPoint());
	}

	public void setDate(Instant date) {
		var newCorrectionPoint = new CommonCorrectionPoint(correctionPoint.get());
		newCorrectionPoint.setDate(date);
		correctionPoint.doSetValue(newCorrectionPoint);
	}

}
