/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.BeamVisibleMode;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Binder for visibility purpose.<br>
 * Bind uniforms "u_show_valid", "u_show_invalid"
 */
public class VisibilityShaderBinder extends AShaderBinder {

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		BeamVisibleMode visibleMode = soundingModel.getDisplayParameters().getVisibleMode();

		Uniform<Float> showValidUniform = (Uniform<Float>) getShaderProgram().getUniforms().get("u_show_valid");
		showValidUniform.setValue(new Float(NumberUtils
				.bool2byte(visibleMode == BeamVisibleMode.ALL || visibleMode == BeamVisibleMode.VALID_ONLY)));

		Uniform<Float> showInvalidUniform = (Uniform<Float>) getShaderProgram().getUniforms().get("u_show_invalid");
		showInvalidUniform.setValue(new Float(NumberUtils
				.bool2byte(visibleMode == BeamVisibleMode.ALL || visibleMode == BeamVisibleMode.INVALID_ONLY)));
	}
}
