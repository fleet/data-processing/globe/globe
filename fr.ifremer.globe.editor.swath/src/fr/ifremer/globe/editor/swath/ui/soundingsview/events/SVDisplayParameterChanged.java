package fr.ifremer.globe.editor.swath.ui.soundingsview.events;

/**
 * Event triggered when display parameters of sounding viewer changed
 */
public enum SVDisplayParameterChanged {
	eVisibleMode, eSceneMode, eSelectionMode, eWidgetMode
}
