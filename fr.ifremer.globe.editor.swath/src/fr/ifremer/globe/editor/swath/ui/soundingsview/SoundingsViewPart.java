package fr.ifremer.globe.editor.swath.ui.soundingsview;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.BathyDtmModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.action.SoundEditorAction;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.UndoRedoController;
import fr.ifremer.globe.ui.IDataUndoable;
import fr.ifremer.globe.ui.events.BaseEventTopics;
import fr.ifremer.globe.ui.events.swath.SwathEventTopics;
import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.undo.StackEvent;
import fr.ifremer.globe.ui.undo.UndoStack;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

/**
 * Sounding view part.
 */
public class SoundingsViewPart implements IDataUndoable, ISaveable {

	private static Logger logger = LoggerFactory.getLogger(SoundingsViewPart.class);

	/** Id of the view in fragment model. */
	public static final String PART_ID = "fr.ifremer.globe.editor.swath.partdescriptor.soundingsView";

	private final MPart part;
	private final UndoRedoController undoRedoController;
	private final BathyDtmModel bathyDtmModel;

	private SoundingsComposite soundingComposite;

	@Inject
	SoundingsViewPart(MPart part) {
		this.part = part;
		this.undoRedoController = ContextInitializer.getInContext(ContextNames.UNDO_REDO_CONTROLLER);
		this.bathyDtmModel = ContextInitializer.getInContext(ContextNames.BATHY_DTM_MODEL);
	}

	/**
	 * Executed after dependency injection is done to perform any initialization
	 */
	@PostConstruct
	void postConstruct(Composite parent) {
		createUI(parent);
	}

	void createUI(final Composite parent) {
		try {
			parent.setLayout(new FillLayout());

			// create the controller
			SoundingsController soundingsController = ContextInitializer.make(SoundingsController.class);
			ContextInitializer.setInContext(ContextNames.SOUNDINGS_CONTROLLER, soundingsController);

			// create the view
			soundingComposite = new SoundingsComposite(parent, SWT.NONE);
			ContextInitializer.inject(soundingComposite);

			soundingsController.initialize(soundingComposite);

			soundingComposite.createUI();
			ContextInitializer.setInContext(ContextNames.SWATH_EDITOR_SOUNDINGS, this);
		} catch (GIOException | IOException e) {
			Messages.openErrorMessage("Writing Exception", e.getMessage());
		}
	}

	@PreDestroy
	public void closePart() {
		// Launch the close command
		try {
			IEclipseContext eclipseContext = ContextInitializer.getEclipseContext();
			if (eclipseContext != null) {
				CommandUtils.executeCommand(eclipseContext, "fr.ifremer.globe.editor.swath.closeSwathEditorCommand");
			}
		} catch (GException e) {
			logger.warn(e.getMessage());
		}
	}

	@Focus
	public boolean setFocus() {
		return soundingComposite.setFocus();
	}

	public SoundingsComposite getSoundingsComposite() {
		return soundingComposite;
	}

	/** {@inheritDoc} */
	@Override
	public UndoStack getUndoStack() {
		return undoRedoController.getUndoStack();
	}

	@Persist
	public void save(IProgressMonitor monitor) {
		doSave(monitor);
	}

	/**
	 * Saves currently edited file.
	 */
	@Override
	public boolean doSave(IProgressMonitor monitor) {
		boolean saved = bathyDtmModel != null && bathyDtmModel.doSave(monitor);
		if (saved) {
			undoRedoController.markCurentContextSaved();
			refreshDirtyFlag();
		}
		return saved;
	}

	@Override
	public void setDirty(boolean dirty) {
		refreshDirtyFlag();
	}

	public void refreshDirtyFlag() {
		if (part != null)
			part.setDirty(isDirty());
	}

	/** Process the dtm refresh event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	public void onDtmRefreshRequestedEvent(@UIEventTopic(SwathEventTopics.IS_DIRTY) Boolean dirty) {
		refreshDirtyFlag();
	}

	/**
	 * New action has been performed on Sounding view : model has changed
	 **/
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	void onSoundEditorAction(@UIEventTopic(EventTopics.SOUND_ACTION) SoundEditorAction soundEditorAction) {
		refreshDirtyFlag();
	}

	@Inject
	@Optional
	void stackChanged(@UIEventTopic(BaseEventTopics.UNDO_STACK) StackEvent e) {
		refreshDirtyFlag();
	}

	/**
	 * Returns whether the contents of this part have changed since the last save operation.
	 */
	@Override
	public boolean isDirty() {
		return !undoRedoController.isCurentContextSaved();
	}

	/**
	 * Repaints the soundings view.
	 */
	public void refresh() {
		soundingComposite.getSoundingsScene().refresh();
	}

	/**
	 * Change of the the soundings selection.
	 */
	@Inject
	@Optional
	void onSoundEditorAction(@UIEventTopic(EventTopics.SOUNDINGS_SELECTION) Event event, MApplication application,
			EPartService partService) {
		// Useful for activate Undo.Redo
		PartUtil.activatePart(application, partService, SoundingsViewPart.PART_ID);
	}

}
