/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.Buffer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Bind attribute "in_selected" and uniform "u_selected_color"
 */
public class SelectionShaderBinder extends AShaderBinder {

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		Uniform<Vector4F> selectedColorUniform = (Uniform<Vector4F>) getShaderProgram().getUniforms()
				.get("u_selected_color");
		GColor selectionColor = dtmPreferences.getSelectionColor().getValue();
		selectedColorUniform.setValue(new Vector4F(selectionColor.getFloatRed(), selectionColor.getFloatGreen(),
				selectionColor.getFloatBlue(), selectionColor.getFloatAlpha()));
	}

	/** {@inheritDoc} */
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		super.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);

		int offsetInFile = (int) lineBuffer.getOffsetInFile();
		int count = lineBuffer.getDetectionCount();

		Supplier<Buffer> supplier = () -> selectionModel.getSelectionArray().asByteBuffer(offsetInFile, count);
		VertexBuffer selectionVertexBuffer = new VertexBufferGL2x3x(supplier.get(), BufferHint.StaticDraw,
				count * Byte.BYTES);

		VertexBufferAttribute selectionVertexBufferAttribute = new VertexBufferAttribute(selectionVertexBuffer,
				ComponentDatatype.Byte, 1);
		int selectionAttributeLocation = getShaderProgram().getVertexAttributes().get("in_selected").getLocation();
		vertexBufferAttributes.set(selectionAttributeLocation, selectionVertexBufferAttribute);

		alertVertexBufferCreated(listener, new BufferRefresher(selectionVertexBuffer, supplier));
	}
}
