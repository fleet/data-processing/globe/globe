/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.projectexplorer;

import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import fr.ifremer.globe.editor.swath.SwathFileInfo;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

/**
 * LayerNode to render ColorScaleLayer.
 */
public class SwathEditorGroupNode extends GroupNode {

	@Inject
	@Named(ContextNames.SWATH_FILE_INFO)
	protected SwathFileInfo swathFileInfo;

	@Inject
	private ProjectExplorerModel projectExplorerModel;
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;
	@Inject
	private ITreeNodeFactory treeNodeFactory;
	// @Inject
	// private ESelectionService selectionService;

	/** Name of the Swath node */
	public static final String SWATH_GROUP_NODE_NAME = "Swath Editor";

	private final Consumer<WWFileLayerStoreEvent> fileLayerStoreModelListener = this::onLayerStoreUpdated;

	/**
	 * Constructor.
	 */
	public SwathEditorGroupNode() {
		super(SWATH_GROUP_NODE_NAME, Icons.SWATH.toImage());
		expanded = true;
	}

	@PostConstruct
	public void postConstruct() {
		projectExplorerModel.add(this);
		fileLayerStoreModel.addListener(fileLayerStoreModelListener);
		projectExplorerModel.setSelection(this);
	}

	@PreDestroy
	public void preDrestroy() {
		fileLayerStoreModel.removeListener(fileLayerStoreModelListener);
	}

	private void onLayerStoreUpdated(WWFileLayerStoreEvent event) {
		if (event.getFileLayerStore().getFileInfo() == swathFileInfo
				&& (event.getState() == FileLayerStoreState.ADDED || event.getState() == FileLayerStoreState.UPDATED)) {
			treeNodeFactory.createOrUpdateLayerNodes(this, event.getFileLayerStore());

			// FIXME : the selection of the node after layer creation doesn't work : the ParametersView doesn't receive
			// the event...
			// projectExplorerModel.setSelection(this);
			// selectionService.setSelection(new StructuredSelection(this));
			IParametersViewService.grab().refresh();
		}
	}

}
