package fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionType;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.GetValueOperation;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.ToStringOperation;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsComposite;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.BufferAssociation;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.ui.TableViewerColumnSorter;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.widget.StackLayout;
import fr.ifremer.globe.utils.number.NumberUtils;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

/**
 * Contains a table which displays all attribute data about beams selected in the {@link SoundingsComposite}.
 */
public class SummaryView {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(SummaryView.class);

	/** Id of the view in fragment model. */
	public static final String PART_ID = "fr.ifremer.globe.editor.swath.partdescriptor.summaryView";

	public static final String CYCLE_INDEX_COLUMN_NAME = "Cycle index";
	public static final String BEAM_INDEX_COLUMN_NAME = "Beam index";
	public static final String LONGITUDE_COLUMN_NAME = "Longitude";
	public static final String LATITUDE_COLUMN_NAME = "Latitude";
	public static final String DEPTH_COLUMN_NAME = "Depth (m)";
	public static final String REFLECTIVITY_COLUMN_NAME = "Reflectivity";
	public static final String VALIDITY_COLUMN_NAME = "Validity flag";
	public static final String DETECTION_COLUMN_NAME = "Detection flag";
	public static final String FILE_COLUMN_NAME = "File";

	private final SoundingsModel soundingsModel;
	private final SwathEditorParameters parameters;

	private Composite tableComposite;
	/** Layers displayed in table as columns */
	private List<PredefinedLayers<?>> diplayedLayers = new ArrayList<>();

	private FilterPreferences filterPreferences = new FilterPreferences();

	private TableViewer dataTableViewer;

	private Composite emptyComposite;

	private StackLayout layout;

	private ILatLongFormatter latLongFormatter = LatLongFormater.getFormatter();

	/** Handler subscribed to IEventBroker for PartStack management */
	private final EventHandler eventHandler;

	/** Constructor */
	@Inject
	public SummaryView(IEventBroker eventBroker, Composite parent) {
		soundingsModel = ContextInitializer.getInContext(ContextNames.SOUNDINGS_MODEL);
		parameters = ContextInitializer.getInContext(ContextNames.SWATH_EDITOR_PARAMETERS);

		eventHandler = PartUtil.onPartStackChanged(PART_ID, eventBroker, this::onPartStackChanged);

		diplayedLayers.add(BathymetryLayers.DETECTION_X);
		diplayedLayers.add(BathymetryLayers.DETECTION_Y);

		createControls(parent);
	}

	@PreDestroy
	private void dispose(IEventBroker eventBroker) {
		eventBroker.unsubscribe(eventHandler);
	}

	/**
	 * Create contents of the view part.
	 */
	public void createControls(Composite parent) {
		createEmptyComposite(parent);
		createTableComposite(parent);
		layout = new StackLayout();
		parent.setLayout(layout);
	}

	public void createEmptyComposite(Composite parent) {
		emptyComposite = new Composite(parent, SWT.NONE);
		emptyComposite.setLayout(new GridLayout(2, false));

		Label questionImage = new Label(emptyComposite, SWT.NONE);
		questionImage.setImage(Display.getDefault().getSystemImage(SWT.ICON_WARNING));

		Label warningLabel = new Label(emptyComposite, SWT.NONE);
		warningLabel.setText("No data to display. Please select an area in the soundings view.");
	}

	/**
	 * Initialize components relatives to the {@link SummaryView#dataTableViewer}.
	 */
	private void createTableComposite(Composite parent) {
		tableComposite = new Composite(parent, SWT.NONE);
		tableComposite.setLayout(new GridLayout(2, false));
		GridDataFactory.fillDefaults().grab(true, true).applyTo(tableComposite);

		Button filtersButton = new Button(tableComposite, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(filtersButton);
		filtersButton.setText("Manage filters");
		filtersButton.addListener(SWT.Selection, event -> {
			FiltersDialog filtersDialog = new FiltersDialog(parent.getShell(), filterPreferences);
			if (filtersDialog.open() == Window.OK) {
				dataTableViewer.getTable().setRedraw(false);
				dataTableViewer.refresh();
				dataTableViewer.getTable().setRedraw(true);
			}
		});

		Button resetButton = new Button(tableComposite, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(resetButton);
		resetButton.setText("Reset filters");
		resetButton.addListener(SWT.Selection, event -> {
			filterPreferences.reset();
			dataTableViewer.getTable().setRedraw(false);
			dataTableViewer.refresh();
			dataTableViewer.getTable().setRedraw(true);
		});

		// Table which displays data relatives to beams.
		Composite composite = new Composite(tableComposite, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).span(2, 1).applyTo(composite);
		dataTableViewer = new TableViewer(composite,
				SWT.BORDER | SWT.VIRTUAL | SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
		Table table = dataTableViewer.getTable();
		GridDataFactory.fillDefaults().grab(true, true).applyTo(table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		// Define columns
		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		composite.setLayout(tableColumnLayout);

		addColumns(dataTableViewer, tableColumnLayout);

		dataTableViewer.setContentProvider(ArrayContentProvider.getInstance());
		dataTableViewer.addFilter(new SummaryViewerFilter(filterPreferences));

	}

	/** Add column definitions to the table */
	private void addColumns(TableViewer dataTableViewer, TableColumnLayout tableColumnLayout) {

		addColumn(dataTableViewer, CYCLE_INDEX_COLUMN_NAME, 70, SummaryItem::getCycleIndex,
				item -> String.valueOf(item.getCycleIndex()), tableColumnLayout);
		addColumn(dataTableViewer, BEAM_INDEX_COLUMN_NAME, 70, SummaryItem::getBeamIndex,
				item -> String.valueOf(item.getBeamIndex()), tableColumnLayout);
		addColumn(dataTableViewer, LONGITUDE_COLUMN_NAME, 90, SummaryItem::getLongitude,
				item -> latLongFormatter.formatLongitude(item.getLongitude()), tableColumnLayout);
		addColumn(dataTableViewer, LATITUDE_COLUMN_NAME, 90, SummaryItem::getLatitude,
				item -> latLongFormatter.formatLatitude(item.getLatitude()), tableColumnLayout);
		addColumn(dataTableViewer, DEPTH_COLUMN_NAME, 70, SummaryItem::getDepth,
				item -> NumberUtils.getStringDouble(item.getDepth()), tableColumnLayout);
		addColumn(dataTableViewer, REFLECTIVITY_COLUMN_NAME, 70, SummaryItem::getReflectivity,
				item -> NumberUtils.getStringDouble(item.getReflectivity()), tableColumnLayout);
		addColumn(dataTableViewer, VALIDITY_COLUMN_NAME, 95, SummaryItem::getValidity,
				item -> item.getValidity().toUserString(), tableColumnLayout);
		addColumn(dataTableViewer, DETECTION_COLUMN_NAME, 90, SummaryItem::getDetection, item -> {
			if (item.getDetection() == DetectionType.AMPLITUDE.get()) {
				return "Amplitude";
			} else if (item.getDetection() == DetectionType.PHASE.get()) {
				return "Phase";
			} else {
				return "";
			}
		}, tableColumnLayout);
		addColumn(dataTableViewer, FILE_COLUMN_NAME, 90, SummaryItem::getFileName, SummaryItem::getFileName,
				tableColumnLayout);

		GetValueOperation getValueOperation = new GetValueOperation();
		ToStringOperation toStringOperation = new ToStringOperation();
		for (PredefinedLayers<?> predefinedLayer : diplayedLayers) {
			addColumn(dataTableViewer, predefinedLayer.getDisplayedName(), 90,
					item -> item.getValue(getValueOperation, predefinedLayer),
					item -> item.getStringValue(toStringOperation, predefinedLayer), tableColumnLayout);
		}
	}

	/** Add one column definition to the table */
	private void addColumn(TableViewer dataTableViewer, String columnName, int width,
			Function<SummaryItem, Object> valueSupplier, Function<SummaryItem, String> textSupplier,
			TableColumnLayout tableColumnLayout) {
		TableViewerColumn column = new TableViewerColumn(dataTableViewer, SWT.NONE);
		column.getColumn().setText(columnName);
		tableColumnLayout.setColumnData(column.getColumn(), new ColumnWeightData(10, width));
		column.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object summaryItem) {
				return textSupplier.apply((SummaryItem) summaryItem);
			}
		});
		new TableViewerColumnSorter(column) {
			@Override
			protected Object getValue(Object summaryItem) {
				return valueSupplier.apply((SummaryItem) summaryItem);
			}
		};
	}

	/**
	 * Fill the {@link SummaryView#tableData}
	 */
	private List<SummaryItem> initializeTableData() {

		// Extracting designated bounds
		List<SummaryItem> result = new ArrayList<>();
		for (int fileIndex = 0; fileIndex < soundingsModel.getBeamsAssociation().size(); fileIndex++) {
			BufferAssociation beamsAssociation = soundingsModel.getBeamsAssociation();
			SounderDataContainer dataContainer = beamsAssociation.getFile(fileIndex);
			List<LineBuffer> buffers = beamsAssociation.getBuffers(fileIndex);

			for (LineBuffer lineBuffer : buffers) {
				Mappings mappings = lineBuffer.getSpatialIndex().getMappings();
				int nbBeams = lineBuffer.getDetectionCount();
				for (int offset = 0; offset < nbBeams; offset++) {
					long offsetInFile = lineBuffer.getOffsetInFile() + offset;
					SelectionModel selectionModel = soundingsModel.getSelectionModels().get(fileIndex);
					if (selectionModel.isSelected(offsetInFile)) {
						SummaryItem item = new SummaryItem(dataContainer, mappings, offsetInFile);
						result.add(item);
					}
				}
			}
		}

		// Compute lon/lat
		try {
			parameters.projection.unproject( //
					index -> result.get(index).getX(), //
					index -> result.get(index).getY(), //
					(index, longitude) -> result.get(index).setLongitude(longitude), //
					(index, latitude) -> result.get(index).setLatitude(latitude), //
					result.size(), new NullProgressMonitor());
		} catch (ProjectionException e) {
			logger.warn("Unproject error : {}", new NullProgressMonitor());
		}

		// Compute bounds
		filterPreferences.clearBounds();
		result.forEach(filterPreferences::push);

		return result;
	}

	/**
	 * Change of the the soundings selection.
	 */
	@Inject
	@Optional
	void onSoundEditorAction(@UIEventTopic(EventTopics.SOUNDINGS_SELECTION) Event event) {
		initialize();
	}

	public void initialize() {
		// check view is not disposed
		if (soundingsModel == null)
			return;

		filterPreferences.reset();
		List<SummaryItem> items = initializeTableData();
		if (items.isEmpty()) {
			layout.setTopControl(emptyComposite);
		} else {
			Display.getDefault().syncExec(() -> dataTableViewer.setInput(items));
			layout.setTopControl(tableComposite);
		}
	}

	/** Subscribe to EventBroker and reacts when view is added to a PartStack */
	private void onPartStackChanged(MPartStack partStack) {
		DtmPreference preference = ContextInitializer.getInContext(ContextNames.DTM_PREFERENCES);
		preference.getContainerId(PART_ID).setValue(partStack.getElementId(), false);
		preference.save();
	}
}
