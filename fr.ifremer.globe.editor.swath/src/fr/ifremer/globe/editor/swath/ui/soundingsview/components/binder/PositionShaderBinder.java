/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder;

import java.nio.Buffer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SelectionModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.ViewPoint;
import fr.ifremer.globe.ogl.core.vectors.Vector3F;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * Binder for uniform "u_center" and attribute "in_position".
 */
public class PositionShaderBinder extends AShaderBinder {

	/** ViewPoint */
	@Inject
	@Optional
	@Named(ContextNames.SOUNDINGS_VIEW_POINT)
	protected ViewPoint viewPoint;

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.swath.ui.soundingsview.components.binder.AShaderBinder#bindAttributes(SelectionModel,
	 *      fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer,
	 *      fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes, java.util.function.Consumer)
	 */
	@Override
	public void bindAttributes(SelectionModel selectionModel, LineBuffer lineBuffer,
			VertexBufferAttributes vertexBufferAttributes, Consumer<BufferRefresher> listener) {
		super.bindAttributes(selectionModel, lineBuffer, vertexBufferAttributes, listener);

		int offsetInFile = (int) lineBuffer.getOffsetInFile() * Mappings.COORD_COUNT;
		int count = lineBuffer.getDetectionCount() * Mappings.COORD_COUNT;

		Supplier<Buffer> supplier = () -> lineBuffer.getSpatialIndex().getMappings().getCoordsArray()
				.asFloatBuffer(offsetInFile, count);
		VertexBuffer positionVertexBuffer = new VertexBufferGL2x3x(supplier.get(), BufferHint.StaticDraw,
				count * Float.BYTES);

		// 3 attributes for vertex position
		VertexBufferAttribute positionVertexBufferAttribute = new VertexBufferAttribute(positionVertexBuffer,
				ComponentDatatype.Float, Mappings.COORD_COUNT);
		int positionAttributeLocation = shaderProgram.getVertexAttributes().get("in_position").getLocation();
		vertexBufferAttributes.set(positionAttributeLocation, positionVertexBufferAttribute);

		alertVertexBufferCreated(listener, new BufferRefresher(positionVertexBuffer, supplier));
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void bindUniforms(LineBuffer lineBuffer) {
		Uniform<Vector3F> centerUniform = (Uniform<Vector3F>) getShaderProgram().getUniforms().get("u_center");
		centerUniform.setValue(viewPoint.getWorldCenter().toVector3F());
	}
}
