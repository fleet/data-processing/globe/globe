package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.validator;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.event.EventHandler;

import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite.FilterComposite;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

/**
 * Part to show the parameters.
 */
public class BeamValidatorPart {

	/** Id of the view defined in e4 model */
	public static final String PART_ID = "fr.ifremer.globe.editor.swath.partdescriptor.beam_validator";

	/** Main swt composite */
	private final Composite mainComposite;

	/** Handler subscribed to IEventBroker for PartStack management */
	private final EventHandler eventHandler;

	/**
	 * Post construction, after injection
	 */
	@Inject
	public BeamValidatorPart(IEventBroker eventBroker, Composite parent) {
		mainComposite = parent;
		this.eventHandler = PartUtil.onPartStackChanged(PART_ID, eventBroker, this::onPartStackChanged);

		parent.setLayout(new GridLayout());

		FilterComposite compo = new FilterComposite(parent, SWT.NONE);
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.widthHint = 800;
		gridData.heightHint = 300;
		compo.setLayoutData(gridData);

		AttributeProvider attributeProvider = new AttributeProvider();
		compo.configure(attributeProvider,
				// create validating process from parameters
				(criterias, dataToSet) -> IProcessService.grab().createProcess("Beam validator",
						new ValidatingProcess(attributeProvider, criterias, dataToSet)));
	}

	@PreDestroy
	private void dispose(IEventBroker eventBroker) {
		eventBroker.unsubscribe(eventHandler);
	}

	/** Returns the preferred size (in points) of the part. */
	public Point computeSize() {
		return mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
	}

	/** Subscribe to EventBroker and reacts when view is added to a PartStack */
	private void onPartStackChanged(MPartStack partStack) {
		DtmPreference preference = ContextInitializer.getInContext(ContextNames.DTM_PREFERENCES);
		preference.getContainerId(PART_ID).setValue(partStack.getElementId(), false);
		preference.save();
	}

}
