﻿package fr.ifremer.globe.editor.swath.ui.soundingsview;

import jakarta.annotation.PreDestroy;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.jogl.GraphicsWindowGL2x;

public class SoundingsComposite extends Composite {

	private SoundingsScene soundingsScene;

	private GraphicsWindowGL2x window;
	private Composite mainComposite;
	private LegendComposite legendComposite;

	public SoundingsComposite(Composite parent, int style) {
		super(parent, style);
	}

	@PreDestroy
	void preDestroy() {
		legendComposite.free();
	}

	public void createUI() {
		// Grid layout for this composite
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginWidth = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginHeight = 0;
		gridLayout.horizontalSpacing = 0;
		setLayout(gridLayout);

		mainComposite = new Composite(this, SWT.EMBEDDED);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		window = Device2x.createWindow(mainComposite);

		soundingsScene = ContextInitializer.make(SoundingsScene.class);
		soundingsScene.initialize(window);

		legendComposite = new LegendComposite(this, SWT.NONE);
		legendComposite.setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 1, 1));

		window.getComponent().addGLEventListener(new GLEventListener() {

			@Override
			public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
				// Unused
			}

			@Override
			public void init(GLAutoDrawable drawable) {
				// Unused
			}

			@Override
			public void dispose(GLAutoDrawable drawable) {
				soundingsScene.dispose(window.getContext());

			}

			@Override
			public void display(GLAutoDrawable drawable) {
				// Unused
			}
		});
	}

	public SoundingsScene getSoundingsScene() {
		return soundingsScene;
	}

	/** Locks the camera. */
	public void lockCamera(boolean lock) {
		soundingsScene.getCameraBehavior().enableDefaultAdaptor(!lock);
	}

	/**
	 * @return the window
	 */
	public GraphicsWindowGL2x getWindow() {
		return window;
	}

	/**
	 * @return the soundingsComposite
	 */
	public Composite getSoundingsComposite() {
		return mainComposite;
	}

	public void refresh() {
		getSoundingsScene().refresh();
		legendComposite.refresh();
	}

}
