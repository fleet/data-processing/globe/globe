package fr.ifremer.globe.editor.swath.ui.soundingsview.model;

import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.scene.Camera;

/**
 * This class gather view points data like
 * <p>
 * the center of the area seen,
 * <p>
 * <li>the eye position</li>
 * <li>all cordinates are in lat/long/meter</li>
 * <li>range used by camera for maximum displacement</li>
 */
public class ViewPoint {
	
	public ViewPoint() {
		setScalingFactorToDefault();
	}

	/**
	 * indicate if the view is locked, ie we keep the camera orientation from one selection to another
	 */
	protected boolean lock;

	/**
	 * the center of worldCenter, coordinates are shifted consequently. It is usefull to bring back coordinates around a
	 * zero values, doing so it avoid float imprecision for big numbers
	 */
	protected Vector3D worldCenter = new Vector3D(0, 0, 0);

	/** Flag "Vertical Exaggeration" */
	protected boolean verticalExaggeration = true;

	protected Camera camera;

	/**
	 * Range is the max allowed for used for displacement and displayed area range is expressed in meters
	 */
	protected double rangeXYZ;

	protected Vector3D eye = new Vector3D(0, 0, 1);

	protected Vector3D target = new Vector3D(0, 0, 0);

	protected double aperture = Math.PI / 10.;

	/**
	 * scaling factor, since by default only lat/long/depth coordinates are supported it is set to have roughly good
	 * ratio between x and y
	 */
	protected Vector3D scalingFactor = new Vector3D(1, 1, 1);

	/**
	 * @return the {@link #worldCenter}
	 */
	public Vector3D getWorldCenter() {
		return worldCenter;
	}

	/**
	 * @param worldCenter the {@link #worldCenter} to set
	 */
	public void setWorldCenter(Vector3D world) {
		worldCenter = world;
	}

	/**
	 * @return the {@link #aperture}
	 */
	public double getAperture() {
		return aperture;
	}

	/**
	 * @param aperture the {@link #aperture} to set
	 */
	public void setAperture(double aperture) {
		this.aperture = aperture;
	}

	/**
	 * @return the {@link #scalingFactor}
	 */
	public Vector3D getScalingFactor() {
		return scalingFactor;
	}

	public void setScalingFactor(double xyScale, double zScale) {
		Vector3D newScalingFactor = new Vector3D(xyScale, xyScale, zScale);
		scalingFactor = newScalingFactor;
	}

	/** Reset the scale factor to the default value. */
	private void setScalingFactorToDefault() {
		scalingFactor = new Vector3D(1, 1, 1);
	}

	public boolean isVerticalExaggeration() {
		return verticalExaggeration;
	}

	public void setVerticalExaggeration(boolean exaggeration) {
		verticalExaggeration = exaggeration;
	}

	/**
	 * @return the {@link #rangeXYZ}
	 */
	public double getRangeXYZ() {
		return rangeXYZ;
	}

	/**
	 * @param range the {@link #range} to set
	 */
	public void setRangeXYZ(double range) {
		rangeXYZ = range;
	}

	/**
	 * @return the lock
	 */
	public boolean isLock() {
		return lock;
	}

	/**
	 * @param lock the lock to set
	 */
	public void setLock(boolean lock) {
		this.lock = lock;
	}

	/**
	 * @return the camera
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * @param camera the camera to set
	 */
	public void setCamera(Camera camera) {
		this.camera = camera;
		this.camera.setUp(new Vector3D(0, 0, 1));
		this.camera.setTarget(new Vector3D(0, 0, 0));
		this.camera.setEye(new Vector3D(0, 0, 1));
	}

}
