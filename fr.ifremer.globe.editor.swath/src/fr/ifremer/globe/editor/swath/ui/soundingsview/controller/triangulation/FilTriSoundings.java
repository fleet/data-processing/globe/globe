/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.swath.ui.soundingsview.controller.triangulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.processes.filtri.model.IFilTriSoundingsProxy;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.LineBuffer;

/**
 * Implementation of IFilTriSoundings to adapt some LineBuffer.
 */
public class FilTriSoundings implements IFilTriSoundingsProxy {

	/** All considered LineBuffer. */
	protected List<LineBuffer> lineBuffers = new ArrayList<>();

	/** All considered LineBuffer. */
	protected int size = 0;

	/** Last claimed index by the Filtri process. */
	protected int lastFilTriIndex = Integer.MAX_VALUE;

	/** Current LineBuffer. */
	protected LineBuffer currentLineBuffer;

	/** Current index of beam in the current LineBuffer. */
	protected long currentOffsetInFile;

	private int currentIndexOfBeamInLineBuffer;

	protected int currentLineBufferIndex;

	/** True when all call to setValid will be ignored. Case of a non editable file */
	private final boolean readOnly;

	public FilTriSoundings(boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * Add one LineBuffer
	 */
	public void add(LineBuffer lineBuffer) {
		this.lineBuffers.add(lineBuffer);
		this.size += lineBuffer.getDetectionCount();
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() throws IOException {
		this.lineBuffers.clear();
	}

	@Override
	public double[] getLonLat(int index) {
		updateLineBufferReference(index);
		Mappings map = this.currentLineBuffer.getSpatialIndex().getMappings();
		return new double[] { map.getX(this.currentOffsetInFile), map.getY(currentOffsetInFile) };
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.process.filtri.model.IFilTriSoundingsProxy#getDepth(int)
	 */
	@Override
	public double getDepth(int index) {
		updateLineBufferReference(index);
		return this.currentLineBuffer.getSpatialIndex().getMappings().getDepth(this.currentOffsetInFile);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.process.filtri.model.IFilTriSoundingsProxy#getValidity(int)
	 */
	@Override
	public byte getValidity(int index) {
		updateLineBufferReference(index);
		MappingValidity validity = this.currentLineBuffer.getSpatialIndex().getMappings()
				.getValidity(this.currentOffsetInFile);
		return validity == MappingValidity.SND_VALID ? IFilTriSoundingsProxy.BYTE_TRUE
				: IFilTriSoundingsProxy.BYTE_FALSE;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.process.filtri.model.IFilTriSoundingsProxy#setValid(int, boolean)
	 */
	@Override
	public void setValid(int index, boolean validity) {
		if (!readOnly) {
			MappingValidity value = validity ? MappingValidity.SND_VALID : MappingValidity.SND_UNVALID_AUTO;
			updateLineBufferReference(index);
			this.currentLineBuffer.getSpatialIndex().getMappings().setValidity(this.currentOffsetInFile, value);
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.process.filtri.model.IFilTriSoundingsProxy#getLastIndex()
	 */
	@Override
	public int getLastIndex() {
		return this.size - 1;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.process.filtri.model.IFilTriSoundingsProxy#size()
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * Compute the LineBuffer and the index inside it.
	 */
	protected void updateLineBufferReference(int index) {
		if (this.lastFilTriIndex != index) {
			// First ?
			if (index == 0) {
				this.currentLineBuffer = this.lineBuffers.get(0);
				this.currentIndexOfBeamInLineBuffer = 0;
				this.currentLineBufferIndex = 0;
			} else if (this.lastFilTriIndex == index - 1) {
				// Next ?
				this.currentIndexOfBeamInLineBuffer++;
				if (this.currentIndexOfBeamInLineBuffer >= this.currentLineBuffer.getDetectionCount()) {
					this.currentIndexOfBeamInLineBuffer = 0;
					this.currentLineBufferIndex++;
					this.currentLineBuffer = this.lineBuffers.get(this.currentLineBufferIndex);
				}
			} else {
				// search which LineBuffer for the specified index
				this.currentLineBufferIndex = 0;
				this.currentLineBuffer = null;
				this.currentIndexOfBeamInLineBuffer = index;
				while (this.currentLineBuffer == null) {
					this.currentLineBuffer = this.lineBuffers.get(this.currentLineBufferIndex);
					if (this.currentLineBuffer.getDetectionCount() <= this.currentIndexOfBeamInLineBuffer) {
						this.currentIndexOfBeamInLineBuffer -= this.currentLineBuffer.getDetectionCount();
						this.currentLineBufferIndex++;
						this.currentLineBuffer = null;
					}
				}
			}

			this.lastFilTriIndex = index;
			this.currentOffsetInFile = this.currentLineBuffer.getOffsetInFile() + this.currentIndexOfBeamInLineBuffer;
		}
	}

}
