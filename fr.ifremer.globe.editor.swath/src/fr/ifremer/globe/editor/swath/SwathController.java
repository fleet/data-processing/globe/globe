﻿package fr.ifremer.globe.editor.swath;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.swath.LayerType;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.swath.application.context.BathySelectionMode;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.application.event.EventTopics;
import fr.ifremer.globe.editor.swath.application.handlers.ShowToolbox;
import fr.ifremer.globe.editor.swath.export.validity.ExportToValidityProcessWithWizard;
import fr.ifremer.globe.editor.swath.model.BathyDtmModel;
import fr.ifremer.globe.editor.swath.model.IndexedDtmLayer;
import fr.ifremer.globe.editor.swath.model.NavigationModel;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndexRegistry;
import fr.ifremer.globe.editor.swath.ui.geographicview.DtmElevationInterpolator;
import fr.ifremer.globe.editor.swath.ui.geographicview.DtmElevationModel;
import fr.ifremer.globe.editor.swath.ui.geographicview.SelectionShifter;
import fr.ifremer.globe.editor.swath.ui.projectexplorer.SwathEditorGroupNode;
import fr.ifremer.globe.editor.swath.ui.soundingsview.SoundingsViewPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.SoundingsController;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.triangulation.TriangulationFilteringPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.controller.validator.BeamValidatorPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.model.SoundingsModel;
import fr.ifremer.globe.editor.swath.ui.soundingsview.preferences.DtmPreference;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.biascorrection.BiasCorrectionView;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.summary.SummaryView;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox.ManageFilesViewPart;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox.ToolboxMenu;
import fr.ifremer.globe.editor.swath.ui.soundingsview.ui.toolbox.ToolboxViewPart;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics;
import fr.ifremer.globe.ui.events.swath.SwathEventTopics;
import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.ui.handler.PartLabel;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWIsobathLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory.RasterFeatures;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWQuadSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWShapefileLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWwLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.WWSwathLayerParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.Renderable;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * This class responsability is to manage creation and life cycle of swath editor views (dtm view and sounding view)
 *
 */
public class SwathController {

	/** Prefix of all files created in Nasa cache */
	public static final String WW_CACHE_PREFIX = "SWATH_EDITOR_";
	/** Id of PartStack defined in fragment.e4xmi */
	public static final String PARTSTACK_SOUNDINGS = "fr.ifremer.globe.editor.swath.partstack.swathSoundings";

	/** Max size of DTM for isobath computing. */
	public static final int ISOBATH_THRESHOLD = 2048;

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(SwathController.class);

	/** Shell of the application. */
	@Inject
	@Named(IServiceConstants.ACTIVE_SHELL)
	private Shell shell;
	@Inject
	private EPartService partService;
	@Inject
	private MApplication application;
	@Inject
	private EModelService modelService;
	@Inject
	private IGeographicViewService geographicViewService;
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;
	@Inject
	private ProjectExplorerModel projectExplorerModel;

	@Inject
	@Named(ContextNames.SWATH_EDITOR_PARAMETERS)
	protected SwathEditorParameters parameters;
	@Inject
	@Named(ContextNames.SWATH_FILE_INFO)
	protected SwathFileInfo swathFileInfo;
	@Inject
	@Named(ContextNames.SWATH_EDITOR_GROUP_NODE)
	protected SwathEditorGroupNode swathEditorGroupNode;

	@Inject
	@Named(ContextNames.DTM_PREFERENCES)
	protected DtmPreference dtmPreferences;

	@Inject
	@Named(ContextNames.SPATIAL_INDEX_REGISTRY)
	private SpatialIndexRegistry spatialIndexRegistry;

	/** Injected after SoundingsModel is initialized */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	@Named(ContextNames.BATHY_DTM_MODEL)
	private BathyDtmModel bathyDtmModel;

	/** Injected after Soundings part is initialized */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	@Named(ContextNames.SWATH_EDITOR_SOUNDINGS)
	private SoundingsViewPart soundingView;
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	@Named(ContextNames.SOUNDINGS_CONTROLLER)
	protected SoundingsController soundingsController;

	/** Osgi Service used to create WW layers */
	@Inject
	protected IWWLayerFactory wwLayerFactory;

	private boolean mouseMoved;

	/** Elevation model computed from elevationDataset */
	protected DtmElevationModel dtmElevationModel;

	/** Selected beams layer */
	protected IWWShapefileLayer selectedBeamsLayer;

	/** Selection Layer */
	protected IWWQuadSelectionLayer quadSelectionLayer;

	/** Selection Layer */
	protected IWWSwathSelectionLayer swathSelectionLayer;

	/** Temporary folder */
	protected File tmpDir;

	/** Current DtmLayerType displayed in the SwathDtmLayer. */
	protected LayerType currentLayerType = LayerType.ELEVATION;
	private List<ISounderDataContainerToken> dataContainerTokens;

	/** Subject that emits a dtm refresh request */
	protected PublishSubject<Event> refreshDtmSubject = PublishSubject.create();
	/** Subscription on refreshDtmSubject */
	protected Disposable refreshDtmSubsciption;
	protected ContextualMenuListener contextualMenuListener;

	/** Kind of selection in geographic view */
	protected BathySelectionMode bathySelectionMode;

	/**
	 * Initialization after constructior.
	 */
	@PostConstruct
	protected void initialization() {
		mouseMoved = false;

		// Clean previous Temporary files.
		fr.ifremer.globe.utils.FileUtils.deleteTempDir(getClass().getSimpleName());
		// Delete Tiles caches
		NasaCache.delete(new File(WW_CACHE_PREFIX));

		if (tmpDir == null) {
			// Create temporary folder
			tmpDir = new File(TemporaryCache.getRootPath(),
					getClass().getSimpleName() + "_" + System.currentTimeMillis());
			tmpDir.mkdirs();
		}
	}

	/**
	 * Preparation of swath controller and opening of the layers
	 */
	public void initialize(List<ISounderDataContainerToken> dataContainerTokens, IProgressMonitor monitor)
			throws GIOException {
		ContextInitializer.setInContext(ContextNames.SWATH_CONTROLLER, this);
		SubMonitor submonitor = SubMonitor.convert(monitor, "Swath editor loading...", 100);
		this.dataContainerTokens = dataContainerTokens;

		List<SounderDataContainer> dataContainers = dataContainerTokens.stream()
				.map(ISounderDataContainerToken::getDataContainer).collect(Collectors.toList());

		prepareModels(dataContainers, submonitor.split(95));
		createParts(dataContainers, submonitor.split(1));
		createLayers(dataContainers, submonitor.split(4));

		// Allow the refreshing of the DTM every 1/2 second
		refreshDtmSubsciption = refreshDtmSubject //
				.sample(1500L, TimeUnit.MILLISECONDS)//
				.toFlowable(BackpressureStrategy.LATEST)//
				.observeOn(Schedulers.io())//
				.subscribe(dtmRefreshRequestedEvent -> refresh(),
						error -> logger.warn("Error while refreshing dtm", error));
	}

	/**
	 * Close the sounding view, remove listener and set to null all class member so that memory can be free
	 */
	@PreDestroy
	void preDestroy() {
		if (soundingView != null) {
			soundingView.getSoundingsComposite().getWindow().removeMouseListener(contextualMenuListener);
			soundingView.getSoundingsComposite().getWindow().removeMouseMotionListener(contextualMenuListener);
		}

		disposeLayers();

		projectExplorerModel.removeElement(swathEditorGroupNode);

		PartUtil.hidePart(application, partService, SoundingsViewPart.PART_ID);
		PartUtil.hidePart(application, partService, ToolboxViewPart.PART_ID);
		PartUtil.hidePart(application, partService, SummaryView.PART_ID);
		PartUtil.hidePart(application, partService, BiasCorrectionView.PART_ID);
		PartUtil.hidePart(application, partService, ManageFilesViewPart.PART_ID);
		PartUtil.hidePart(application, partService, TriangulationFilteringPart.PART_ID);
		PartUtil.hidePart(application, partService, BeamValidatorPart.PART_ID);

		for (ISounderDataContainerToken sounderDataContainerToken : dataContainerTokens) {
			sounderDataContainerToken.close();
			if (bathyDtmModel != null) {
				// TODO GB
				/*
				 * Optional<Long> lastModificationDate = bathyDtmModel
				 * .getLastModificationDate(sounderDataContainer.getInfo()); if (lastModificationDate.isPresent()) {
				 * File sounderDataFile = new File(sounderDataContainer.getInfo().getFilename());
				 * sounderDataFile.setLastModified(lastModificationDate.get().longValue()); }
				 */
			}
		}

		// Delete temporary folder
		if (tmpDir != null)
			FileUtils.deleteQuietly(tmpDir);

		// Delete Tiles caches
		NasaCache.delete(new File(WW_CACHE_PREFIX));

		if (refreshDtmSubsciption != null)
			refreshDtmSubsciption.dispose();

		refreshDtmSubject.unsubscribeOn(Schedulers.io());
		refreshDtmSubject = null;

	}

	/**
	 * Free all layers
	 */
	protected void disposeLayers() {
		fileLayerStoreModel.remove(SwathEditorGroupNode.SWATH_GROUP_NODE_NAME);

		if (quadSelectionLayer != null) {
			geographicViewService.removeLayer(quadSelectionLayer);
			quadSelectionLayer = null;
		}

		if (swathSelectionLayer != null) {
			geographicViewService.removeLayer(swathSelectionLayer);
			swathSelectionLayer = null;
		}

		if (selectedBeamsLayer != null) {
			geographicViewService.removeLayer(selectedBeamsLayer);
			selectedBeamsLayer = null;
		}
	}

	/**
	 * First production of layers to render the DTM into the Nasa view.
	 */
	protected void createLayers(List<SounderDataContainer> dataContainers, IProgressMonitor monitor) {

		var layerStoreOption = generatesLayer(LayerType.ELEVATION, monitor);
		if (layerStoreOption.isPresent()) {
			WWRasterLayerStore layerStore = layerStoreOption.get();

			// Elevation
			dtmElevationModel = new DtmElevationModel(GeoBoxUtils.convert(parameters.lonLatGeoBox),
					parameters.columnCount, parameters.rowCount);
			dtmElevationModel.setName("Elevations");
			dtmElevationModel.addElevations(getLayerTypeFile(LayerType.ELEVATION));
			dtmElevationModel.expandSector(0.5);
			dtmElevationModel.setExtremesCachingEnabled(true);
			geographicViewService.addElevationModel(dtmElevationModel);

			layerStore = layerStore.duplicatesWithElevationModel(dtmElevationModel);

			// Quad selection Layer
			quadSelectionLayer = wwLayerFactory.createQuadSelectionLayer(parameters.projection,
					dtmPreferences.getDtmSelectionColor(), dtmPreferences.getDtmSelectionLineWidth());
			ContextInitializer.setInContext(ContextNames.WW_QUAD_SELECTION_LAYER, quadSelectionLayer);
			quadSelectionLayer.setEnabled(bathySelectionMode == BathySelectionMode.RECTANGLE);

			// Swath selection Layer
			swathSelectionLayer = wwLayerFactory.createSwathLayer("Swath selection",
					parameters.mercatorGeoBox.getProjection(), dtmPreferences.getSwathCountInSelection().getValue(),
					parameters.lonLatGeoBox);
			ContextInitializer.setInContext(ContextNames.WW_SWATH_SELECTION_LAYER, swathSelectionLayer);
			swathSelectionLayer.setEnabled(bathySelectionMode == BathySelectionMode.SWATH);
			swathSelectionLayer.addSwathsFrom(dataContainers);

			SelectionShifter selectionShifter = ContextInitializer.getInContext(ContextNames.SELECTION_SHIFTER);
			selectionShifter.setQuadSelectionLayer(quadSelectionLayer);
			selectionShifter.setSwathSelectionLayer(swathSelectionLayer);

			// Create LayerNodes
			WWFileLayerStore fileLayerStore = new WWFileLayerStore(swathFileInfo);
			fileLayerStore.addLayers(layerStore);
			fileLayerStoreModel.add(SwathEditorGroupNode.SWATH_GROUP_NODE_NAME, fileLayerStore);
			geographicViewService.addLayer(quadSelectionLayer);
			geographicViewService.addLayer(swathSelectionLayer);
		}

		// Focus on zone
		geographicViewService.zoomToSector(GeoBoxUtils.convert(parameters.lonLatGeoBox), 0f);
	}

	/**
	 * Event handler for TOPIC_WW_LAYERS_REQUESTED
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onLayerRequired(@UIEventTopic(UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED) RasterInfo rasterInfo) {
		if (rasterInfo.getFilePath().equals(swathFileInfo.getPath()))
			IProcessService.grab().runInForeground("Layer creation for " + rasterInfo.getDataType(), (monitor, l) -> {
				Optional<LayerType> dtmLayerType = Arrays.stream(LayerType.values())
						.filter(layerType -> layerType.getLabel().equals(rasterInfo.getDataType())).findFirst();
				if (dtmLayerType.isPresent())
					changeLayerType(dtmLayerType.get(), monitor);
				return Status.OK_STATUS;
			});
	}

	/**
	 * Change the current layer type on SwathDtmLayer
	 */
	public void changeLayerType(LayerType layerType, IProgressMonitor monitor) {
		generatesLayer(layerType, monitor).ifPresent(layerStore -> {
			fileLayerStoreModel.addLayersToStore(swathFileInfo,
					layerStore.getLayers().toArray(new IWWLayer[layerStore.getLayers().size()]));
			currentLayerType = layerType;
		});
	}

	/**
	 * Produce the layers to render the DTM into the Nasa view.
	 */
	protected Optional<WWRasterLayerStore> generatesLayer(LayerType layerType, IProgressMonitor monitor) {
		SubMonitor submonitor = SubMonitor.convert(monitor, 100);
		try {
			IndexedDtmLayer indexedDtmLayer = bathyDtmModel.getIndexedDtmLayer(layerType);
			double missingValue = indexedDtmLayer.getMissingValue();

			RasterFeatures dtmInfo = new RasterFeatures(//
					getLayerTypeFile(layerType), //
					layerType.getLabel(), //
					parameters.mercatorGeoBox, //
					parameters.rowCount, //
					parameters.columnCount, //
					missingValue);

			Optional<WWRasterLayerStore> optLayerStore = wwLayerFactory.createRasterLayers(//
					dtmInfo, //
					indexedDtmLayer::get, //
					submonitor.split(70));
			if (optLayerStore.isPresent()) {
				LayerNameSetter layerNameSetter = new LayerNameSetter(layerType);
				optLayerStore.get().getLayers().forEach(layer -> layer.perform(layerNameSetter));
			}
			return optLayerStore;
		} catch (GIOException e) {
			Messages.openErrorMessage("Swath editor", "Unable to change the layer\n" + e.getMessage());
		} catch (OperationCanceledException e) {
			// Operation cancelled
		}
		return Optional.empty();
	}

	/**
	 * Initialize the models
	 */
	protected void prepareModels(List<SounderDataContainer> dataContainers, IProgressMonitor monitor)
			throws GIOException {
		// create the model
		if (monitor.isCanceled())
			return;
		spatialIndexRegistry.indexSounderFiles(dataContainers, monitor);

		if (monitor.isCanceled())
			return;
		BathyDtmModel dtmModel = ContextInitializer.make(BathyDtmModel.class);
		dtmModel.initialize(dataContainers);
		ContextInitializer.setInContext(ContextNames.BATHY_DTM_MODEL, dtmModel);

		if (monitor.isCanceled())
			return;
		NavigationModel navModel = ContextInitializer.make(NavigationModel.class);
		navModel.initialize(dataContainers);
		ContextInitializer.setInContext(ContextNames.NAVIGATION_MODEL, navModel);

		if (monitor.isCanceled())
			return;
		SoundingsModel soundingsModel = ContextInitializer.make(SoundingsModel.class);
		ContextInitializer.setInContext(ContextNames.SOUNDINGS_MODEL, soundingsModel);
		soundingsModel.setBeamSize(dtmPreferences.getBeamSize().getValue().intValue());

		monitor.done();
	}

	/**
	 * create the parts
	 */
	protected void createParts(List<SounderDataContainer> dataContainers, IProgressMonitor monitor) {

		// Define label
		String toolTipLabel = PartLabel.computeToolTipID(dataContainers);

		if (!monitor.isCanceled()) {

			// create SOUNDING PART
			MPart partSounding = PartUtil.createPart(application, partService, SoundingsViewPart.PART_ID);
			partSounding.setLabel(SwathEditorGroupNode.SWATH_GROUP_NODE_NAME);
			partSounding.setTooltip(toolTipLabel);
			Display.getDefault().syncExec(() -> {
				PartManager.addEditableBindingContextToPart(partSounding, application);
				modelService.findElements(application, PARTSTACK_SOUNDINGS, MPartStack.class, null).get(0)
						.setVisible(true);
				PartManager.addPartToStack(partSounding, application, modelService, PARTSTACK_SOUNDINGS);
				PartManager.showPart(partService, IParametersViewService.grab().getPart());
			});
			PartUtil.activatePart(application, partService, partSounding);

			// Display toolbox.
			Display.getDefault().syncExec(() -> {
				try {
					CommandUtils.executeCommand(ContextInitializer.getEclipseContext(),
							ShowToolbox.COMMAND_SHOW_SOUNDINGS_TOOLBOX);
				} catch (GException e) {
					logger.debug("Unable to execute command {} : {}", ShowToolbox.COMMAND_SHOW_SOUNDINGS_TOOLBOX,
							e.getMessage());
				}
			});

			// Create contextual menu
			contextualMenuListener = new ContextualMenuListener();
			soundingView.getSoundingsComposite().getWindow().addMouseListener(contextualMenuListener);
			soundingView.getSoundingsComposite().getWindow().addMouseMotionListener(contextualMenuListener);

			monitor.done();
		}
	}

	/**
	 * Listener for the soundings view contextual menu.
	 */
	class ContextualMenuListener extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			mouseMoved = false;
			e.consume();
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			mouseMoved = true;
			e.consume();
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (!soundingsController.isSelecting() && !e.isConsumed() && e.isPopupTrigger() && !mouseMoved) {
				ToolboxMenu menu = ContextInitializer.make(ToolboxMenu.class);
				menu.initialize(soundingView.getSoundingsComposite().getSoundingsComposite());
				menu.show(e.getComponent(), e.getX(), e.getY());
				e.consume();
			}
		}
	}

	/**
	 * Interpolates elevations of DEPTH layer
	 *
	 * @return the tiff file with interpolated elevations
	 */
	public void interpolateElevations(int maxDistance, IProgressMonitor monitor, int nbTicks) throws IOException {
		// Create a copy of Depth file and open it with gdal
		File depthFile = getLayerTypeFile(LayerType.ELEVATION);
		File interpolatedFile = getInterpolatedFile();
		Files.copy(depthFile.toPath(), interpolatedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		DtmElevationInterpolator dtmElevationInterpolator = new DtmElevationInterpolator();
		dtmElevationInterpolator.interpolateElevations(interpolatedFile, maxDistance, monitor, nbTicks);

		// Updates the elevation model
		dtmElevationModel.addElevations(interpolatedFile);
		monitor.done();
	}

	/**
	 * @return the file containing data of the DtmLayerType
	 */
	public File getLayerTypeFile(LayerType dtmLayerType) {
		return new File(getTmpDir(), WW_CACHE_PREFIX + dtmLayerType.name() + ".tiff");
	}

	/**
	 * @return the WW folder cache containing tiles of the DtmLayerType
	 */
	public String getWWCacheName(LayerType dtmLayerType) {
		return "SWATH_" + dtmLayerType.getLabel();
	}

	/** Process the dtm refresh event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	public void onDtmRefreshRequestedEvent(@UIEventTopic(SwathEventTopics.REFRESH_DTM) Event event) {
		refreshDtmSubject.onNext(event);
	}

	/** Selection mode changed */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	public void onBathySelectionModeEvent(
			@Named(ContextNames.BATHY_SELECTION_MODE) BathySelectionMode newBathySelectionMode) {
		if (newBathySelectionMode != bathySelectionMode) {
			bathySelectionMode = newBathySelectionMode;
			if (quadSelectionLayer != null)
				quadSelectionLayer.setEnabled(bathySelectionMode == BathySelectionMode.RECTANGLE);
			if (swathSelectionLayer != null)
				swathSelectionLayer.setEnabled(bathySelectionMode == BathySelectionMode.SWATH);
		}
	}

	/** Swath Count In Selection changed */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	public void onSwathCountInSelectionEvent(@Named(ContextNames.SWATH_COUNT_IN_SELECTION) int swathCount) {
		if (swathSelectionLayer != null) {
			WWSwathLayerParameters newParam = swathSelectionLayer.getParameters()
					.withSwathsCountInSelection(swathCount);
			swathSelectionLayer.setParameters(newParam);
		}

		dtmPreferences.getSwathCountInSelection().setValue(swathCount, false);
		dtmPreferences.save();
	}

	/**
	 * Beams have been designated from the soundings view
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onBeamsDesignatedEvent(@UIEventTopic(EventTopics.SOUNDING_DESIGNATED) Optional<File> shapeFile) {
		// remove previous selected beam layer
		if (selectedBeamsLayer != null) {
			geographicViewService.removeLayer(selectedBeamsLayer);
			selectedBeamsLayer = null;
		}

		if (shapeFile.isPresent()) {
			selectedBeamsLayer = wwLayerFactory.createShapefileLayer(shapeFile::get);
			if (selectedBeamsLayer != null) {
				PointPlacemarkAttributes att = new PointPlacemarkAttributes();
				att.setLineMaterial(new Material(dtmPreferences.getDesignatedBeamsColor()));
				att.setUsePointAsDefaultImage(true);
				att.setScale(7d);
				if (selectedBeamsLayer instanceof RenderableLayer renderableLayer)
					for (Renderable renderable : renderableLayer.getRenderables())
						if (renderable instanceof PointPlacemark pointPlacemark)
							pointPlacemark.setAttributes(att);
				selectedBeamsLayer.setName("Swath Editor Selected Detections");
				geographicViewService.addLayer(selectedBeamsLayer);
				geographicViewService.refresh();
			}
		}
	}

	/**
	 * Perform the refreshing of all DTM layers.
	 */
	protected void refresh() {
		fileLayerStoreModel.get(SwathEditorGroupNode.SWATH_GROUP_NODE_NAME).map(WWFileLayerStore::getLayers).stream()
				.flatMap(List::stream).filter(IWWTerrainLayer.class::isInstance).map(IWWTerrainLayer.class::cast)
				// for each DTM layer
				.forEach(layer -> {
					var currentLayerType = LayerType.fromLabel(layer.getType()).orElseThrow();
					// Delete XML cache file to force a new generation of tiles
					if (!NasaCache.deleteXml(new File(layer.getDataCacheName().orElseThrow())))
						return; // is delete failed, maybe a refresh is already running : return

					// 1) regenerates the new tiles
					generatesLayer(currentLayerType, new NullProgressMonitor())
							// 2) get the new generated DTM layer
							.map(WWRasterLayerStore::getLayers).stream().flatMap(List::stream) //
							.filter(IWWTerrainLayer.class::isInstance).map(IWWTerrainLayer.class::cast).findFirst() //
							// 3) finds this DTM layer among the layers displayed in geographic view to reload tiles
							.flatMap(newDtmLayer -> fileLayerStoreModel.get(SwathEditorGroupNode.SWATH_GROUP_NODE_NAME)
									.flatMap(swathEditorLayerStore -> swathEditorLayerStore
											.findLayer(newDtmLayer.getName(), IWWTerrainLayer.class)))
							.ifPresentOrElse(geographicViewDtmLayer -> {
								geographicViewDtmLayer.setClearTextureCache(true);
								geographicViewService.refresh();
							}, () -> logger.error("Swath editor DTM not available : {}", currentLayerType));
				});
	}

	/**
	 * @return the file containing interpolaed elevations
	 */
	protected File getInterpolatedFile() {
		return new File(getTmpDir(), LayerType.ELEVATION.name() + "_interpolated.tiff");
	}

	/**
	 * Launches the export the validity flags to a CSV file.
	 */
	public void exportValidityFlags(String processName) {
		ExportToValidityProcessWithWizard exportProcess = new ExportToValidityProcessWithWizard(shell, processName);
		ContextInitializer.inject(exportProcess);

		bathyDtmModel.getSounderDataContainers().stream()
				.forEach(sdc -> exportProcess.getModel().getInputFiles().add(new File(sdc.getInfo().getPath())));

		if (exportProcess.getWizardDialog().open() == Window.OK)
			exportProcess.runInForeground();
	}

	/**
	 * @return the {@link #partService}
	 */
	public EPartService getPartService() {
		return partService;
	}

	/**
	 * @return the {@link #modelService}
	 */
	public EModelService getModelService() {
		return modelService;
	}

	/**
	 * @return the {@link #tmpDir}
	 */
	public File getTmpDir() {
		return tmpDir;
	}

	/**
	 * @param currentLayerType the {@link #currentLayerType} to set
	 */
	public void setCurrentLayerType(LayerType currentLayerType) {
		this.currentLayerType = currentLayerType;
	}

	/** Image properties. */
	public static class ImageProperties {
		/** File */
		protected File imagePath;
		/** Image size */
		protected int height;
		/** Image size */
		protected int width;
		/** Geo sector */
		protected Sector sector;
		/** Min and max values */
		protected DoubleRange minMax;

		/**
		 * @return the {@link #imagePath}
		 */
		public File getImagePath() {
			return imagePath;
		}

		/**
		 * @param imagePath the {@link #imagePath} to set
		 */
		public void setImagePath(File imagePath) {
			this.imagePath = imagePath;
		}

		/**
		 * @return the {@link #height}
		 */
		public int getHeight() {
			return height;
		}

		/**
		 * @param height the {@link #height} to set
		 */
		public void setHeight(int height) {
			this.height = height;
		}

		/**
		 * @return the {@link #width}
		 */
		public int getWidth() {
			return width;
		}

		/**
		 * @param width the {@link #width} to set
		 */
		public void setWidth(int width) {
			this.width = width;
		}

		/**
		 * @return the {@link #sector}
		 */
		public Sector getSector() {
			return sector;
		}

		/**
		 * @param sector the {@link #sector} to set
		 */
		public void setSector(Sector sector) {
			this.sector = sector;
		}

		/**
		 * Follow the link.
		 *
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ImageProperties [height=" + height + ", width=" + width + ", sector=" + sector.toString()
					+ ", imagePath=" + imagePath.getAbsolutePath() + "]";
		}

		/**
		 * @return the {@link #minMax}
		 */
		public DoubleRange getMinMax() {
			return minMax;
		}

		/**
		 * @param minMax the {@link #minMax} to set
		 */
		public void setMinMax(DoubleRange minMax) {
			this.minMax = minMax;
		}

	}

	/** Simple class use to set the name of layers */
	public class LayerNameSetter extends BasicWwLayerOperation {

		protected LayerType dtmLayerType;

		public LayerNameSetter(LayerType dtmLayerType) {
			this.dtmLayerType = dtmLayerType;
		}

		/** {@inheritDoc} */
		@Override
		public BasicWwLayerOperation accept(IWWTerrainLayer layer) {
			layer.setName(dtmLayerType.getLabel());
			layer.addPropertyChangeListener("Enabled", e -> {
				if (layer.isEnabled())
					setCurrentLayerType(dtmLayerType);
			});
			geographicViewService.unPredictedLayer(layer.getName());
			return this;
		}

		/** {@inheritDoc} */
		@Override
		public BasicWwLayerOperation accept(IWWColorScaleLayer layer) {
			layer.setName("Scale " + dtmLayerType.getLabel());
			layer.setScaleLabel(layer.getName());
			geographicViewService.unPredictedLayer(layer.getName());
			return this;
		}

		/** {@inheritDoc} */
		@Override
		public BasicWwLayerOperation accept(IWWIsobathLayer layer) {
			layer.setName("Isobaths");
			geographicViewService.unPredictedLayer(layer.getName());
			return this;
		}
	}
}
