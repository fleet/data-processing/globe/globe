uniform sampler2D texElevation;

void main()
{
	vec4 colorOri = texture2D(texElevation, gl_TexCoord[1].st);

	gl_FragColor.r = 1.0;
	gl_FragColor.g = 1.0;
	gl_FragColor.b = 1.0;
	gl_FragColor.a = 1.0;	
}