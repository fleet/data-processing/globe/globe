#version 120
const float SND_UNVALID_ACQUIS=-3.0;
const float SND_UNVALID_AUTO=-2.0;
const float SND_UNVALID=-1.0;
const float SND_MISSING=0.0;
const float SND_DOUBTFUL=1.0;
const float SND_VALID=2.0;

// Selected color by the vertex shader for the point.
varying vec4 color;
// Value 1.0 if beams is valid. 0.0 otherwise
varying float validity;
// Value 1.0 if beams is selected. 0.0 otherwise
varying float selected;

// Value 1.0 if valid beams have to be shown. 0.0 otherwise
uniform float u_show_valid;
// Value 1.0 if invalid beams have to be shown. 0.0 otherwise
uniform float u_show_invalid;
// Color of selected beams
uniform vec4 u_selected_color;

void main()
{
	if(validity==SND_MISSING )
		discard;
	if(validity<=SND_MISSING && u_show_invalid== 0.0)
		discard;
	if(validity >= SND_MISSING && u_show_valid == 0.0)
		discard;
		
	// Color accroding selection flag
	gl_FragColor = (u_selected_color * selected) + (color * (1.0 - selected));

}
