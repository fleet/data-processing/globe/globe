#version 120

const float SND_VALID=2.0;// valid

uniform vec3 u_center;

// Color of beams
uniform vec4 u_file_color;
// Value 1.0 to keep invalid beam in red. 0.0 otherwise
uniform float u_keep_invalid_beam_in_red;
// Color of beams which have amplitude detection flag
uniform vec4 u_invalid_color;

// Coords of the beam
attribute vec3 in_position;
// Value 1.0 if beams is valid. 0.0 otherwise
attribute float in_validity;
// Value 1.0 if beams is selected. 0.0 otherwise
attribute float in_selected;

// Beam's color color computed by this shader
varying vec4 color;
// Value 1.0 if beams is valid. 0.0 otherwise
varying float validity;
// Value 1.0 if beams is selected. 0.0 otherwise
varying float selected;

void main() {
	// compute position relative to the center
	vec3 position = in_position - u_center;
	gl_Position = gl_ModelViewProjectionMatrix * vec4(position, 1.0);
	
	// Color. Red if invalid and u_keep_invalid_beam_in_red is true
	vec4 force_invalid_color = vec4(lessThan(vec4(in_validity),vec4(SND_VALID))) * u_keep_invalid_beam_in_red;
	color = (u_file_color * (1.0 - force_invalid_color)) + (u_invalid_color * force_invalid_color);
	
	// Informations for the fragment shader
	validity = in_validity;
	selected = in_selected;	
	
}
