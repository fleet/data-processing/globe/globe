#version 120

uniform vec3 u_center;

// Color of valid beams
uniform vec4 u_valid_color;
// Color of invalid beams
uniform vec4 u_invalid_color;

// Color of invalid beams
uniform vec4 u_invalid_acquisition_color;


const float SND_UNVALID_ACQUIS=-3.0;// unvalid at acquisition


const float SND_UNVALID_AUTO=-2.0;// unvalidated by automatic
const float SND_UNVALID_MANUAL=-1.0;// unvalidated by operator
const float SND_VALID=2.0;// valid


// Coords of the beam
attribute vec3 in_position;

attribute float in_validity;
// Value 1.0 if beams is selected. 0.0 otherwise
attribute float in_selected;

// Normalized value of the point.
varying vec4 color;

varying float validity;
// Value 1.0 if beams is selected. 0.0 otherwise
varying float selected;


void main() {
	// compute position relative to the center
	vec3 position = in_position - u_center;
	gl_Position = gl_ModelViewProjectionMatrix * vec4(position, 1.0);
	
	// Color according validity flag


	
	color = (u_valid_color * vec4(greaterThanEqual(vec4(in_validity),vec4(SND_VALID))))
	+ (u_invalid_color*( vec4(equal(vec4(in_validity),vec4(SND_UNVALID_MANUAL))) + vec4(equal( vec4(in_validity),vec4(SND_UNVALID_AUTO) )) ))
	+ (u_invalid_acquisition_color * vec4(lessThanEqual( vec4(in_validity), vec4(SND_UNVALID_ACQUIS) ) ));
	
	// Informations for the fragment shader
	validity = in_validity;
	selected = in_selected;	
}
