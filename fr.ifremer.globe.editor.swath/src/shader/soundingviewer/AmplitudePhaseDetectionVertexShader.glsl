#version 120
const float SND_UNVALID_ACQUIS=-3.0;// unvalid at acquisition
const float SND_UNVALID_AUTO=-2.0;// unvalidated by automatic
const float SND_UNVALID=-1.0;// unvalidated by operator
const float SND_MISSING=0.0;// missing
const float SND_DOUBTFUL=1.0;// doubtful (temporary state) (not used reserved for stuff like cube or esa)
const float SND_VALID=2.0;// valid

const float DETECTION_INVALID=0.0;
const float DETECTION_AMPLITUDE=1.0;
const float DETECTION_PHASE=2.0;

uniform vec3 u_center;

// Color of beams which have amplitude detection flag
uniform vec4 amplitude_detection_color;
// Color of beams which have phase detection flag
uniform vec4 phase_detection_color;
// Color of invalid beams
uniform vec4 u_invalid_color;

// Coords of the beam
attribute vec3 in_position;
// Value 1.0 if beams has amplitude detection. 0.0 otherwise
attribute float in_detection_type;
// Value 1.0 if beams is valid. 0.0 otherwise
attribute float in_validity;
// Value 1.0 if beams is selected. 0.0 otherwise
attribute float in_selected;

// Value 1.0 if beams is valid. 0.0 otherwise
varying float validity;
// Value 1.0 if beams is selected. 0.0 otherwise
varying float selected;
// Beam's color computed by this shader
varying vec4 color;

void main() {
	// compute position relative to the center
	vec3 position = in_position - u_center;
	gl_Position = gl_ModelViewProjectionMatrix * vec4(position, 1.0);

	// Color according amplitude detection flag
	if(in_detection_type == DETECTION_AMPLITUDE)
		color = amplitude_detection_color;
	else if(in_detection_type == DETECTION_PHASE)
		color = phase_detection_color;
	else 
		color = u_invalid_color;
		
	// Color according validity flag
	color = (color * vec4(greaterThanEqual(vec4(in_validity),vec4(SND_DOUBTFUL))))
	+ (u_invalid_color* vec4(lessThanEqual(vec4(in_validity),vec4(SND_MISSING)))  );


	// Informations for the fragment shader
	validity = in_validity;
	selected = in_selected;

}
