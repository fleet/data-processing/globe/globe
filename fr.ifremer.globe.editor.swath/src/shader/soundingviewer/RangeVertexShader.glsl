#version 120

uniform vec3 u_center;

// Min value of attribute
uniform float u_min;
// Max value of attribute
uniform float u_max;

// Coords of the beam
attribute vec3 in_position;
// Value of the attribute (beam number, cycle number...)
attribute float in_value;
// Value 1.0 if beams is valid. 0.0 otherwise
attribute float in_validity;
// Value 1.0 if beams is selected. 0.0 otherwise
attribute float in_selected;

// Normalized value of the point.
varying float value;
// Value 1.0 if beams is valid. 0.0 otherwise
varying float validity;
// Value 1.0 if beams is selected. 0.0 otherwise
varying float selected;

void main() {
	// compute position relative to the center
	vec3 position = in_position - u_center;
	gl_Position = gl_ModelViewProjectionMatrix * vec4(position, 1.0);
	
	// Normalize value
	value = (in_value - u_min) / (u_max - u_min);
	// Informations for the fragment shader
	validity = in_validity;
	selected = in_selected;	
}
