#version 120
const float SND_UNVALID_ACQUIS=-3.0;// unvalid at acquisition
const float SND_UNVALID_AUTO=-2.0;// unvalidated by automatic
const float SND_UNVALID=-1.0;// unvalidated by operator
const float SND_MISSING=0.0;// missing
const float SND_DOUBTFUL=1.0;// doubtful (temporary state) (not used reserved for stuff like cube or esa)
const float SND_VALID=2.0;// valid
// Normalized value of the point.
varying float value;
// Value 1.0 if beams is valid. 0.0 otherwise
varying float validity;
// Value 1.0 if beams is selected. 0.0 otherwise
varying float selected;

// Texture in MTU 0
uniform sampler2D texture_sampler;
// Value 1.0 if valid beams have to be shown. 0.0 otherwise
uniform float u_show_valid;
// Value 1.0 if invalid beams have to be shown. 0.0 otherwise
uniform float u_show_invalid;
// Color of selected beams
uniform vec4 u_selected_color;
// Value 1.0 to keep invalid beam in red. 0.0 otherwise
uniform float u_keep_invalid_beam_in_red;
// Color of beams which have amplitude detection flag
uniform vec4 u_invalid_color;

void main()
{
	if(validity==SND_MISSING )
		discard;
	if(validity<=SND_MISSING && u_show_invalid== 0.0)
		discard;
	if(validity >= SND_MISSING && u_show_valid == 0.0)
		discard;

	vec4 color = texture2D(texture_sampler, vec2(0.0, value));

	// Color. Red if invalid and u_keep_invalid_beam_in_red is true
	vec4 force_invalid_color = vec4(lessThan(vec4(validity),vec4(SND_VALID))) * u_keep_invalid_beam_in_red;
	color = (color * (1.0 - force_invalid_color)) + (u_invalid_color * force_invalid_color);

	// Color according selection flag
	color = (u_selected_color * selected) + (color * (1.0 - selected));
	
	gl_FragColor = color;
}
