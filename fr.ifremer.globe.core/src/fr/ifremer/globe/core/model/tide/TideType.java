package fr.ifremer.globe.core.model.tide;

/**
 * Enumerator to describe tide types.
 */
public enum TideType {
	UNKNWON, PREDICTED, MEASURED, GPS
}
