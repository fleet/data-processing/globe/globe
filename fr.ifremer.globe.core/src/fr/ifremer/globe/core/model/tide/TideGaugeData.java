package fr.ifremer.globe.core.model.tide;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Implementation of {@link ITideData} for data comming from {@link TideGauge}.
 */
public class TideGaugeData implements ITideData {

	/** Date/time formatter **/
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/ddHH:mm:ss")
			.withZone(ZoneOffset.UTC);

	/** Properties **/
	private int idstation;
	private int idsource;
	private double value;
	private String timestamp;

	@Override
	public String toString() {
		return "TideGaugeData [idstation=" + idstation + ", idsource=" + idsource + ", value=" + value + ", timestamp="
				+ timestamp + "]";
	}

	// Getters

	public int getIdstation() {
		return idstation;
	}

	public int getIdsource() {
		return idsource;
	}

	public double getValue() {
		return value;
	}

	public Instant getTimestamp() {
		return ZonedDateTime.parse(timestamp, DATE_TIME_FORMATTER).toInstant();
	}

}
