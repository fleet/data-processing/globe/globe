package fr.ifremer.globe.core.model.tide.impl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import fr.ifremer.globe.core.model.tide.ITideService;
import fr.ifremer.globe.core.model.tide.TideGauge;
import fr.ifremer.globe.core.model.tide.TideGaugeData;
import fr.ifremer.globe.core.model.tide.TideGaugeInfo;
import fr.ifremer.globe.core.model.tide.TideGaugeSource;

/**
 * Implementation of {@link ITideService} with {@link OkHttpClient}.
 */
@Component(name = "globe_model_service_tide", service = ITideService.class)
public class TideService implements ITideService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TideService.class);

	public static final String SHOM_WEB_SERVICE_URL = "https://services.data.shom.fr/maregraphie";
	public static final String TIDE_GAUGES_URL = SHOM_WEB_SERVICE_URL + "/service/tidegauges";
	public static final String TIDE_GAUGE_INFO_URL = SHOM_WEB_SERVICE_URL + "/service/completetidegauge";
	public static final String OBSERVATION_URL = SHOM_WEB_SERVICE_URL + "/observation/json";

	@Override
	public List<TideGauge> getTideGauges() throws IOException {
		var response = sendRequest(TIDE_GAUGES_URL);
		List<TideGauge> result = List.of(new Gson().fromJson(response, TideGauge[].class));
		LOGGER.info("Successful response : {} data received.", result.size());
		return result;
	}

	@Override
	public TideGaugeInfo getTideGaugeInfo(String tideGaugeId) throws IOException {
		var response = sendRequest(TIDE_GAUGE_INFO_URL + "/" + tideGaugeId);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJsonString = gson.toJson(JsonParser.parseString(response));
		return new Gson().fromJson(response, TideGaugeInfo.class).withDetails(prettyJsonString);
	}

	/** class to handle response from observation request **/
	class ObservationJSONResponse {
		TideGaugeData[] data;
	}

	@Override
	public List<TideGaugeData> getObservations(String tideGaugeId, TideGaugeSource source, Instant startDate,
			Instant endDate, Optional<Integer> interval) throws IOException {
		// build request
		StringBuilder url = new StringBuilder(OBSERVATION_URL);
		url.append("/" + tideGaugeId + "?");
		Map<String, String> params = new HashMap<>();
		params.put("sources", source.id);
		params.put("dtStart", startDate.toString());
		params.put("dtEnd", endDate.toString());
		interval.ifPresent(i -> params.put("interval", Integer.toString(i)));
		for (Map.Entry<String, String> entry : params.entrySet()) {
			url.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			url.append("=");
			url.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
			url.append("&");
		}

		// send request
		var response = sendRequest(url.toString());

		// parse response
		List<TideGaugeData> result = List.of(new Gson().fromJson(response, ObservationJSONResponse.class).data);
		LOGGER.info("Successful response : {} data received.", result.size());
		return result;
	}

	/**
	 * Sends HTTP request, and puts response in a {@link String}.
	 */
	private String sendRequest(String url) throws IOException {
		HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
		conn.setConnectTimeout(10000); // not more than 10 seconds to connect
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		LOGGER.info("Send request : {}", conn);

		if (conn.getResponseCode() != 200)
			throw new IOException("GET tide gauges failed : HTTP error code : " + conn.getResponseCode());

		StringBuilder response = new StringBuilder();
		try (Scanner scan = new Scanner(conn.getInputStream())) {
			while (scan.hasNext())
				response.append(scan.next());
		}
		conn.disconnect();
		return response.toString();
	}

}
