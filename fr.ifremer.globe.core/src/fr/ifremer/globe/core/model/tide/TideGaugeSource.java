package fr.ifremer.globe.core.model.tide;

/**
 * Enum of source parameter options for observation request.
 * 
 * @see <a href="https://services.data.shom.fr/support/fr/services/refmar#/Observations/observationsGET"}>
 *      https://services.data.shom.fr/support/fr/services/refmar#/Observations/observationsGET</a>
 */
public enum TideGaugeSource {

	RAW_HIGHT_FREQUENCY("1", "Raw hight frequency"), //
	RAW_DEFERRED_TIME("2", "Raw deferred time"), //
	VALID_DEFERRED_TIME("3", "Valid deferred time"), //
	VALID_HOURS("4", "Valid hours"), //
	RAW_HOURS("5", "Raw hours");

	public final String id;
	public final String label;

	TideGaugeSource(String id, String label) {
		this.id = id;
		this.label = label;
	}

	@Override
	public String toString() {
		return label;
	}

}
