package fr.ifremer.globe.core.model.tide;

import java.time.Instant;
import java.util.Optional;

/**
 * Simple interface to define tide data.
 */
public interface ITideData {

	/**
	 * @return a new {@link ITideData}.
	 */
	public static ITideData build(Instant date, double value) {
		return new ITideData() {
			@Override
			public double getValue() {
				return value;
			}

			@Override
			public Instant getTimestamp() {
				return date;
			}
		};
	}

	/**
	 * @return a new {@link ITideData}.
	 */
	public static ITideData build(Instant date, double value, double longitude, double latitude) {
		return new ITideData() {
			@Override
			public double getValue() {
				return value;
			}

			@Override
			public Instant getTimestamp() {
				return date;
			}

			@Override
			public Optional<Double> getLongitude() {
				return Optional.of(longitude);
			}

			@Override
			public Optional<Double> getLatitude() {
				return Optional.of(latitude);
			}
		};
	}

	/**
	 * @return timestamp
	 */
	Instant getTimestamp();

	/**
	 * @return value of the point (depth in meters).
	 */
	double getValue();

	/**
	 * @return optional longitude
	 */
	default Optional<Double> getLongitude() {
		return Optional.empty();
	}

	/**
	 * @return optional latitude
	 */
	default Optional<Double> getLatitude() {
		return Optional.empty();
	}
}
