package fr.ifremer.globe.core.model.tide;

import com.google.gson.annotations.SerializedName;

public class TideGauge {

	/**
	 * Properties
	 */
	@SerializedName("shom_id")
	private String id;
	private String name;
	private double longitude;
	private double latitude;
	private String state;
	private String reseau;
	private boolean alertCustom;
	private String delai_alerte;

	// GETTERS

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public String getState() {
		return state;
	}

	public String getReseau() {
		return reseau;
	}

	public boolean isAlertCustom() {
		return alertCustom;
	}

	public String getDelai_alerte() {
		return delai_alerte;
	}

	@Override
	public String toString() {
		return "TideGaugeDescription [id=" + id + ", name=" + name + ", longitude=" + longitude + ", latitude="
				+ latitude + ", state=" + state + ", reseau=" + reseau + ", alertCustom=" + alertCustom
				+ ", delai_alerte=" + delai_alerte + "]";
	}

}
