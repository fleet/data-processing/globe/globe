package fr.ifremer.globe.core.model.tide;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * This servicer provides method to get tide data from SHOW web services.
 */
public interface ITideService {

	/**
	 * Returns the service implementation of ISessionService.
	 */
	static ITideService grab() {
		return OsgiUtils.getService(ITideService.class);
	}

	/**
	 * Gets tide available tide gauges from SHOM web services (https://services.data.shom.fr/support/fr/services/)
	 * 
	 * @throws IOException
	 */
	List<TideGauge> getTideGauges() throws IOException;

	/**
	 * Gets tide observation from SHOM web services (https://services.data.shom.fr/support/fr/services/)
	 * 
	 * @param tideGaugeId : "Identifiant Shom du marégraphe"
	 * 
	 * @param sources : "Sources de données demandées. Pour en demander plusieurs, séparer les chiffres par une virgule.
	 *            1 = Brutes haute fréquence, 2 = Brutes temps différé, 3 = Validées temps différé, 4 = Validées
	 *            horaires, 5 = Brutes horaires"
	 * 
	 * @param startDate : "Date et heure de début. La différence entre date de début et date de fin est limitée à 31
	 *            jours."
	 * 
	 * @param endDate : "Date et heure de fin. La différence entre date de début et date de fin est limitée à 31 jours."
	 * 
	 * @param interval : "Intervalle souhaité entre deux mesures : 10 pour une valeur toutes les 10 minutes. Laisser
	 *            vide pour ne pas échantillonner"
	 * 
	 * @return {@link TideGaugeData} computed from parameters.
	 * 
	 * @throws IOException
	 */
	List<TideGaugeData> getObservations(String tideGaugeId, TideGaugeSource sources, Instant startDate, Instant endDate,
			Optional<Integer> interval) throws IOException;

	/**
	 * @return tide gauge properties.
	 */
	TideGaugeInfo getTideGaugeInfo(String tideGaugeId) throws IOException;

}
