package fr.ifremer.globe.core.model.tide;

import java.util.Optional;

import com.google.gson.annotations.SerializedName;

/** Records to store tide gauge properties **/
public record TideGaugeInfo(@SerializedName("shom_id") String shomId, String name, double longitude, double latitude,
		String state, VerticalRef verticalRef, @SerializedName("info_maregraphe") String detailsLink, String details) {

	/** Record to store vertical reference info **/
	public record VerticalRef(@SerializedName("nom_ref") String name, @SerializedName("zero_hydro") String zeroHydro,
			@SerializedName("zh_ref") String zhRef) {
		@Override
		public String toString() {
			return "name="
					+ Optional.ofNullable(name).orElse("unknown") + " ; zero hydro=" + Optional.ofNullable(zeroHydro)
							.filter(zh -> !zh.contains("zero_instrumental_inconnu")).orElse("unknown")
					+ " ; zh_ref=" + Optional.ofNullable(zhRef).orElse("unknown");
		}
	}

	/** Builder with details **/
	public TideGaugeInfo withDetails(String details) {
		return new TideGaugeInfo(shomId, name, longitude, latitude, state, verticalRef, detailsLink, details);
	}

}
