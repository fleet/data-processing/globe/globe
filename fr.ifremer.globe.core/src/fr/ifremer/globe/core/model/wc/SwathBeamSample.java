package fr.ifremer.globe.core.model.wc;
/**
 * A list of sample values for on ping in PingBeam geometry
 * this class is immutable
 * */
public class SwathBeamSample{
	/**
	 * Construct a new PingBeamSample
	 * @param range  the range in sample
	 * @param beam the beam index
	 * @param value the associated value
	 * */
	public SwathBeamSample(int range, int beam, float value,SwathData data) {
		super();
		this.range = range;
		this.beam = beam;
		this.value = value;
		this.data=data;
	}
	public final int range; //sample range in number of samples
	public final int beam; // sample rx beam number
	public final float value; //sample amplitude value
	public final SwathData data;

	
}
