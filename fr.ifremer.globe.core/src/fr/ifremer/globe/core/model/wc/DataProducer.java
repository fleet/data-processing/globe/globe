package fr.ifremer.globe.core.model.wc;

/**
 * produce data to store linked to a ping
 * */
public interface  DataProducer<T> {
	T produce(int pingIndex)  throws Exception ;
}