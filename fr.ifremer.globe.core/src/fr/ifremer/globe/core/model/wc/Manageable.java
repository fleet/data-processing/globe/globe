package fr.ifremer.globe.core.model.wc;

/** 
 * Disposeable interface
 * */
public interface Manageable extends AutoCloseable
{
	@Override
	public void close();
}