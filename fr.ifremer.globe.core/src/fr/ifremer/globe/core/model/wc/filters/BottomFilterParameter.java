package fr.ifremer.globe.core.model.wc.filters;

public class BottomFilterParameter extends BaseFilterParameter {
	public enum ToleranceType {
		SAMPLE, RANGEPERCENT
	}

	private int toleranceAbsolute;
	private double tolerancePercent;
	private double angleCoefficient;
	private ToleranceType type;

	public BottomFilterParameter(boolean enable, int toleranceAbsolute, double tolerancePercent, ToleranceType type,
			double angleCoefficient) {
		super(enable);
		this.setToleranceAbsolute(toleranceAbsolute);
		this.setTolerancePercent(tolerancePercent);
		this.setType(type);
		this.setAngleCoefficient(angleCoefficient);
	}

	public static BottomFilterParameter getDefault() {
		return new BottomFilterParameter(false, 0, 0f, ToleranceType.RANGEPERCENT, 0.1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(getAngleCoefficient());
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + getToleranceAbsolute();
		temp = Double.doubleToLongBits(getTolerancePercent());
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + (getType() == null ? 0 : getType().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BottomFilterParameter other = (BottomFilterParameter) obj;
		if (enable != other.enable)
			return false;
		if (Double.doubleToLongBits(getAngleCoefficient()) != Double.doubleToLongBits(other.getAngleCoefficient())) {
			return false;
		}
		if (getToleranceAbsolute() != other.getToleranceAbsolute()) {
			return false;
		}
		if (Double.doubleToLongBits(getTolerancePercent()) != Double.doubleToLongBits(other.getTolerancePercent())) {
			return false;
		}
		if (getType() != other.getType()) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "BottomFilterParameter [toleranceAbsolute=" + getToleranceAbsolute() + ", tolerancePercent="
				+ getTolerancePercent() + ", angleCoefficient=" + getAngleCoefficient() + ", type=" + getType() + "]";
	}

	/**
	 * @return the {@link #toleranceAbsolute}
	 */
	public int getToleranceAbsolute() {
		return toleranceAbsolute;
	}

	/**
	 * @param toleranceAbsolute the {@link #toleranceAbsolute} to set
	 */
	public void setToleranceAbsolute(int toleranceAbsolute) {
		this.toleranceAbsolute = toleranceAbsolute;
	}

	/**
	 * @return the {@link #tolerancePercent}
	 */
	public double getTolerancePercent() {
		return tolerancePercent;
	}

	/**
	 * @param tolerancePercent the {@link #tolerancePercent} to set
	 */
	public void setTolerancePercent(double tolerancePercent) {
		this.tolerancePercent = tolerancePercent;
	}

	/**
	 * @return the {@link #angleCoefficient}
	 */
	public double getAngleCoefficient() {
		return angleCoefficient;
	}

	/**
	 * @param angleCoefficient the {@link #angleCoefficient} to set
	 */
	public void setAngleCoefficient(double angleCoefficient) {
		this.angleCoefficient = angleCoefficient;
	}

	/**
	 * @return the {@link #type}
	 */
	public ToleranceType getType() {
		return type;
	}

	/**
	 * @param type the {@link #type} to set
	 */
	public void setType(ToleranceType type) {
		this.type = type;
	}

}
