package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.SwathBeamSample;
import fr.ifremer.globe.core.model.wc.SwathData;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Filter that remove side lobe effects <p>
 * compute median value per sector and per range 
 * then threshold values if they are below a the max value minus threshold 
 * */
public class SideLobeFilter implements IFilter {

	private SideLobeFilterParameter parameters;
	private float[] maximas;

	public SideLobeFilter(SideLobeFilterParameter parameters)
	{
		this.parameters=parameters;
	}

	@Override
	public void configure(int pingIdx,SwathData data) throws GIOException {
		maximas = new float[data.matrix.getRowCount()];
		for(int row=0;row<data.matrix.getRowCount();row++)
		{
			float max=Float.NaN;
			for(int col=0;col<data.matrix.getColCount();col++)
			{
				//avoid creating data with no values
				float value=data.matrix.get(row, col,0);
				max=Max(max,value);
			}
			maximas[row]=max;
		}

	}
	/**
	 * Max function, not taking into account NaN values
	 * */
	private float Max(float value,float previous)
	{
		if(Float.isNaN(previous))
			return value;
		else if(Float.isNaN(value))
			return previous;
		else return Math.max(value, previous);
	}
	
	@Override
	public boolean filter(SwathBeamSample sample) {
		return !parameters.isEnable() || reallyFilter(sample);
	}
	/**
	 * Filter sample, return false if filtered, true otherwise
	 */
	private boolean reallyFilter(SwathBeamSample sample) {
		return sample.value > maximas[sample.range]-parameters.getThreshold();
	}

	@Override
	public void setParameters(FilterParameters parameters) {
		this.parameters=parameters.sidelobe;
	}
}
