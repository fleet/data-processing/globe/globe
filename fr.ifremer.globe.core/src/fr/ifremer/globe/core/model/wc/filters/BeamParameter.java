package fr.ifremer.globe.core.model.wc.filters;

public class BeamParameter extends BaseFilterParameter {
	public static final int MINDEFAULTVALUE = 0;
	public static final int MAXDEFAULTVALUE = 800;

	private int minValue = MINDEFAULTVALUE;
	private int maxValue = MAXDEFAULTVALUE;

	public BeamParameter(int minValue, int maxValue, boolean enable) {
		super(enable);
		this.setMinValue(minValue);
		this.setMaxValue(maxValue);
	}

	public static BeamParameter getDefault() {
		return new BeamParameter(MINDEFAULTVALUE, MAXDEFAULTVALUE, false);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getMaxValue();
		result = prime * result + getMinValue();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BeamParameter other = (BeamParameter) obj;
		if(enable != other.enable)
			return false;
		if (getMaxValue() != other.getMaxValue()) {
			return false;
		}
		if (getMinValue() != other.getMinValue()) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "BeamParameter [minValue=" + getMinValue() + ", maxValue=" + getMaxValue() + "]";
	}

	/**
	 * @return the {@link #minValue}
	 */
	public int getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue the {@link #minValue} to set
	 */
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the {@link #maxValue}
	 */
	public int getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the {@link #maxValue} to set
	 */
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

}
