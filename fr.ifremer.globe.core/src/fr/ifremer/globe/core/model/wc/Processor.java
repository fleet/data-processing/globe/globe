package fr.ifremer.globe.core.model.wc;

import fr.ifremer.globe.utils.exception.GIOException;

public interface Processor {
	public void configure(int pingIdx, SwathData data) throws GIOException;
}
