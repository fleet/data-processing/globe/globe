package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.Processor;
import fr.ifremer.globe.core.model.wc.SwathData;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Class holding all sample filters
 */
public class EchoFilters implements Processor {
	private Threshold threshold;
	private BeamFilter beam;
	private DepthFilter depth;
	private SideLobeFilter sidelobe;
	private BottomFilter bottom;
	private SpecularFilter specular;
	private AcrossDistanceFilter across;

	public SpecularFilter getSpecular() {
		return specular;
	}
	
	public BeamFilter getBeam() {
		return beam;
	}

	public BottomFilter getBottom() {
		return bottom;
	}

	public Threshold getThreshold() {
		return threshold;
	}

	public SideLobeFilter getSideLobe() {
		return sidelobe;
	}

	public DepthFilter getDepth() {
		return depth;
	}

	public AcrossDistanceFilter getAcross() {
		return across;
	}

	public EchoFilters(FilterParameters filterParameters) {
		threshold = new Threshold(filterParameters.threshold);
		beam = new BeamFilter(filterParameters.beam);
		depth = new DepthFilter(filterParameters.depth);
		sidelobe = new SideLobeFilter(filterParameters.sidelobe);
		bottom = new BottomFilter(filterParameters.bottom);
		specular = new SpecularFilter(filterParameters.specular);
		across = new AcrossDistanceFilter(filterParameters.acrossDistance);
	}

	public void setParameters(FilterParameters parameters) {
		threshold.setParameters(parameters);
		beam.setParameters(parameters);
		depth.setParameters(parameters);
		sidelobe.setParameters(parameters);
		bottom.setParameters(parameters);
		specular.setParameters(parameters);
		across.setParameters(parameters);
	}

	@Override
	public void configure(int pingIdx, SwathData data) throws GIOException {
		threshold.configure(pingIdx, data);
		beam.configure(pingIdx, data);
		depth.configure(pingIdx, data);
		bottom.configure(pingIdx, data);
		sidelobe.configure(pingIdx, data);
		specular.configure(pingIdx, data);
		across.configure(pingIdx, data);
	}

}
