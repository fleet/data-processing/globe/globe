package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.LatLonSample;
import fr.ifremer.globe.core.model.wc.Processor;
import fr.ifremer.globe.core.model.wc.SwathData;
import fr.ifremer.globe.utils.exception.GIOException;

public interface ISpatialFilter extends Processor {
	/**
	 * Filter sample, return false if filtered, true otherwise
	 * */
	public boolean filter(LatLonSample sample);
	
	public void setParameters(FilterParameters parameters);
	
	public default  void configure(int pingIdx,SwathData data) throws GIOException
	{};
}
