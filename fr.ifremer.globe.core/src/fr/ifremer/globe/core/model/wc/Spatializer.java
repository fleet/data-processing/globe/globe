package fr.ifremer.globe.core.model.wc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import fr.ifremer.globe.core.model.ModelPreferences;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.MbesUtils;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Convert samples in a latitude longitude referential
 */
public class Spatializer implements Processor {

	private List<BeamPoint> indexedMatch = new ArrayList<>();

	private DoubleLoadableLayer1D latitudeLayer;
	private DoubleLoadableLayer1D longitudeLayer;
	private FloatLoadableLayer1D soundspeedLayer;
	private FloatLoadableLayer1D headingLayer;
	private FloatLoadableLayer1D rollLayer;
	private LongLoadableLayer1D timeLayer; // nanoseconds since 1970-01-01 00:00:00Z

	private Optional<FloatLoadableLayer1D> waterlineLayer;
	private Optional<FloatLoadableLayer1D> tx_z_depthLayer;
	private FloatLoadableLayer2D bathy_accross;
	private FloatLoadableLayer2D bathy_along;
	private FloatLoadableLayer2D bathy_pointing_angle;
	private FloatLoadableLayer2D bathy_depth;
	private Optional<ShortLoadableLayer2D> bathy_rxAntennaIndexLayer;
	private Optional<ShortLoadableLayer2D> bathy_txAntennaIndexLayer;
	private ShortLoadableLayer2D bathy_tx_beam_index;

	private FloatLoadableLayer2D wc_pointing_angle_deg;
	private FloatLoadableLayer2D tx_tilt_angle_array;
	private FloatLoadableLayer1D wc_sample_interval;
	private Optional<IntLoadableLayer2D> wc_tx_beam_index = Optional.empty();
	private Optional<IntLoadableLayer2D> wc_tx_transducer_index = Optional.empty();

	// TEMP VARIABLES
	private double shipLatitude;
	private double shipLongitude;
	private double heading;
	private float roll;
	private ArrayList<Double> equivalentTiltAnglePerSector;
	private AntennaInstallationParameters rxParameters;
	private AntennaInstallationParameters txParameters;
	private ArrayList<TreeMap<Double, Double>> acrossInterpolator;

	private SwathSpatialData ping;
	private GeoPoint summit;
	private int scaleFactor; // size of an echo in sample,
	private int bathyBeamCount;
	private int antennaCount;
	private int txBeamCount;
	private int wcBeamCount;
	private double[] headingOffsets;
	private double[] rollOffsets;
	private double[] acrossOffsets;
	private double[] alongOffsets;

	// retain last valid heading seen heading in case of invalid value
	private double last_valid_heading = 0;

	private boolean hasBathymetry = false;
	private boolean debug_print = false;

	private DetectionStatusLayer statusLayer;

	private class GeoPoint {
		private GeoPoint(double latitude, double longitude, double depth) {
			super();
			this.latitude = latitude;
			this.longitude = longitude;
			this.depth = depth;
		}

		public final double latitude;
		public final double longitude;
		public final double depth;
	}

	private class BeamPoint {
		private double samplerate;
		private double equivalentCelerity;
		private double sinEquivalentAngle;
		private double tiltAngleRad;
		private double headingAngleRad;
	}

	Spatializer(SounderDataContainer container, SonarBeamGroup sonarBeamGroup) throws GIOException {
		latitudeLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
		longitudeLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);
		timeLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PING_TIME);
		headingLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
		rollLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
		waterlineLayer = sonarBeamGroup.getOptionalLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);
		tx_z_depthLayer = sonarBeamGroup.getOptionalLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);

		tx_tilt_angle_array = sonarBeamGroup.getLayer(BeamGroup1Layers.TX_BEAM_ROTATION_THETA);

		soundspeedLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);
		wc_pointing_angle_deg = sonarBeamGroup.getLayer(BeamGroup1Layers.RX_BEAM_ROTATION_PHI);
		wc_tx_beam_index = sonarBeamGroup.getOptionalLayer(BeamGroup1Layers.TRANSMIT_BEAM_INDEX);
		wc_tx_transducer_index = sonarBeamGroup.getOptionalLayer(BeamGroup1Layers.TRANSMIT_TRANSDUCER_INDEX);
		wc_sample_interval = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_INTERVAL);

		// check if there is some bathymetry
		hasBathymetry = container.hasLayer(BathymetryLayers.DETECTION_TX_BEAM);
		if (hasBathymetry) {
			bathy_tx_beam_index = container.getLayer(BathymetryLayers.DETECTION_TX_BEAM);
			bathy_rxAntennaIndexLayer = container.getOptionalLayer(BathymetryLayers.DETECTION_RX_TRANSDUCER_INDEX);
			bathy_txAntennaIndexLayer = container.getOptionalLayer(BathymetryLayers.DETECTION_TX_TRANSDUCER_INDEX);
			bathy_accross = container.getLayer(BathymetryLayers.DETECTION_Y);
			bathy_along = container.getLayer(BathymetryLayers.DETECTION_X);
			bathy_pointing_angle = container.getLayer(BathymetryLayers.DETECTION_BEAM_POINTING_ANGLE);
			bathy_depth = container.getLayer(BathymetryLayers.DETECTION_Z);
			statusLayer = container.getStatusLayer();
		}

		rxParameters = container.getInfo().getRxParameters().get(0);
		txParameters = container.getInfo().getTxParameters();
		if (hasBathymetry) {
			bathyBeamCount = container.getInfo().getTotalRxBeamCount();
		}
		antennaCount = container.getInfo().getAntennaParameters().size();
		txBeamCount = container.getInfo().getTotalTxBeamCount();
		wcBeamCount = (int) container.getInfo().getWcRxBeamCount();
		headingOffsets = new double[antennaCount];
		rollOffsets = new double[antennaCount];
		acrossOffsets = new double[antennaCount];
		alongOffsets = new double[antennaCount];
		acrossInterpolator = new ArrayList<TreeMap<Double, Double>>();
		for (int i = 0; i < antennaCount; i++) {
			rollOffsets[i] = container.getInfo().getAntennaParameters().get(i).rolloffset;
			headingOffsets[i] = container.getInfo().getAntennaParameters().get(i).headingoffset;
			acrossOffsets[i] = container.getInfo().getAntennaParameters().get(i).yoffset;
			alongOffsets[i] = container.getInfo().getAntennaParameters().get(i).xoffset;
			acrossInterpolator.add(new TreeMap<>());
		}
	}

	public void configure(int pingIdx, int sampling) throws GIOException {
		// retrieve ship position for this ping to simplify we only care about the first antenna
		shipLatitude = latitudeLayer.get(pingIdx);
		shipLongitude = longitudeLayer.get(pingIdx);
		heading = headingLayer.get(pingIdx);
		if (Double.isNaN(heading)) {
			heading = last_valid_heading;
		} else {
			last_valid_heading = heading;
		}
		roll = rollLayer.get(pingIdx);
		float waterline = 0.f;
		if (waterlineLayer.isPresent()) {
			waterline = waterlineLayer.get().get(pingIdx);
			if (waterlineLayer.get().getFillValue() == waterline || Float.isNaN(waterline))
				waterline = 0.f;
		}
		// swath data are spatialized relative to the surface
		float shipWaterlineFcsDepth = -waterline;
		float txFcsDepth = shipWaterlineFcsDepth;
		if (tx_z_depthLayer.isPresent()) {
			float txDepth = tx_z_depthLayer.get().get(pingIdx);
			if (!Float.isNaN(txDepth))
				txFcsDepth += txDepth;
		}

		scaleFactor = sampling;// size of an echo in sample
		// we only consider the first antenna to get xoffset
		double[] latlon = computeLatLong(rxParameters.xoffset, 0);
		summit = new GeoPoint(latlon[0], latlon[1], txFcsDepth);

		ping = new SwathSpatialData(pingIdx, shipLatitude, shipLongitude, heading, shipWaterlineFcsDepth,
				summit.latitude, summit.longitude, summit.depth, timeLayer.get(pingIdx));
	}

	/**
	 * retrieve all data required for the spatialisation process
	 *
	 * @throws GIOException
	 */
	@Override
	public void configure(int pingIdx, SwathData data) throws GIOException {
		configure(pingIdx, data.sampling);

		ArrayList<DescriptiveStatistics> equivalentTiltAngleRadStats = new ArrayList<>();
		for (int i = 0; i < txBeamCount; i++) {
			equivalentTiltAngleRadStats.add(new DescriptiveStatistics());
		}
		DescriptiveStatistics statsCmCo = new DescriptiveStatistics();
		boolean useBathy = true;
		if (useBathy && hasBathymetry) // beta version, we do not use bathymetry for positionning, instead us constant
										// raytracing
		{
			if (debug_print) {
				System.out.println("bathy : pointing, across, along, elevation");
			}

			for (int beam = 0; beam < bathyBeamCount; beam++) {
				boolean isValidSwath = statusLayer.isValid(pingIdx, beam);
				if (isValidSwath) {
					int rxAntennaIndex = bathy_rxAntennaIndexLayer.isPresent()
							? bathy_rxAntennaIndexLayer.get().get(pingIdx, beam)
							: 0;
					int txAntennaIndex = bathy_txAntennaIndexLayer.isPresent()
							? bathy_txAntennaIndexLayer.get().get(pingIdx, beam)
							: 0;
					double rollOffset = rxAntennaIndex > 0 ? rollOffsets[rxAntennaIndex] : rxParameters.rolloffset;
					double txHeadingOffset = txAntennaIndex > 0 ? headingOffsets[txAntennaIndex]
							: txParameters.headingoffset;
					double acrossOffset = rxAntennaIndex > 0 ? acrossOffsets[rxAntennaIndex] : rxParameters.yoffset;
					double alongOffset = rxAntennaIndex > 0 ? alongOffsets[rxAntennaIndex] : rxParameters.xoffset;

					double pointingAngleDegEarth = bathy_pointing_angle.get(pingIdx, beam) + rollOffset + roll; //
					double across = bathy_accross.get(pingIdx, beam);

					acrossInterpolator.get(rxAntennaIndex).put(pointingAngleDegEarth, across);

					double along = bathy_along.get(pingIdx, beam);
					double bathyDepth = bathy_depth.get(pingIdx, beam);
					short txSector = bathy_tx_beam_index.get(pingIdx, beam);
					double x = across - acrossOffset;
					double y = along - alongOffset;
					double z = bathyDepth - rxParameters.zoffset;

					double rangeBathyMeterRefRx = Math.sqrt(x * x + y * y + z * z);

					// find parameters to fit y = a*x + b
					double a = Math.tan(Math.toRadians(txHeadingOffset));
					double b = y - a * x;
					// cos(heading) = h / b
					double h = b * Math.cos(Math.toRadians(txHeadingOffset));
					double eqTiltAngleRad = Math.atan(h / z);
					equivalentTiltAngleRadStats.get(txSector).addValue(eqTiltAngleRad);// compute an equivalent
																						// tiltangle between Rx antenna
																						// and point detection instead
																						// of computing the two plane
																						// intersection between Tx and
																						// Rx
					double sinEqPointingAngle = x / rangeBathyMeterRefRx;
					double cmOverco = Math.abs( // compute the equivalent celerity over surface celerity
							sinEqPointingAngle / Math.sin(Math.toRadians(pointingAngleDegEarth)));
					statsCmCo.addValue(cmOverco);
					if (debug_print) {
						System.out.println(
								"bathy; " + pointingAngleDegEarth + "; " + across + "; " + along + "; " + bathyDepth);
					}
				}
			}
		} else if (hasBathymetry) // beta version, we do not use bathymetry for positionning, instead us constant
									// raytracing
		{
			for (int beam = 0; beam < bathyBeamCount; beam++) {
				boolean isValidSwath = statusLayer.isValid(pingIdx, beam);
				if (isValidSwath) {
					double along = bathy_along.get(pingIdx, beam);
					double y = along - rxParameters.xoffset;
					double bathyDepth = bathy_depth.get(pingIdx, beam);
					double eqTiltAngleRad = Math.atan(y / bathyDepth);
					short txSector = bathy_tx_beam_index.get(pingIdx, beam);
					equivalentTiltAngleRadStats.get(txSector).addValue(eqTiltAngleRad);// compute an equivalent
																						// tiltangle between Rx antenna
																						// and point detection instead
																						// of computing the two plane
																						// intersection between Tx and
																						// Rx
				}

			}
		}

		double medianCmCo = 1.0;
		if (statsCmCo.getN() != 0) {
			medianCmCo = statsCmCo.getPercentile(50);
		}
		// compute the mean tilt angle per sector
		equivalentTiltAnglePerSector = new ArrayList<>();
		for (int tx_beam = 0; tx_beam < txBeamCount; tx_beam++) {
			DescriptiveStatistics stats = equivalentTiltAngleRadStats.get(tx_beam);
			if (stats.getN() > 0) {
				equivalentTiltAnglePerSector
						.add(Math.toDegrees(equivalentTiltAngleRadStats.get(tx_beam).getPercentile(50)));
			} else {
				// put default tilt angle;
				int txTransducerIndex = 0;
				if (wc_tx_transducer_index.isPresent())
					txTransducerIndex = wc_tx_transducer_index.get().get(pingIdx, tx_beam);
				double headingAngleRad = txTransducerIndex > 0 ? Math.toRadians(headingOffsets[txTransducerIndex])
						: Math.toRadians(txParameters.headingoffset);
				double tanTxTiltAngle = Math.tan(Math.toRadians(tx_tilt_angle_array.get(pingIdx, tx_beam)))
						* Math.cos(headingAngleRad);
				double v = Math.atan(tanTxTiltAngle);
				equivalentTiltAnglePerSector.add(Math.toDegrees(v));
			}
		}
		indexedMatch = new ArrayList<>();

		double minAngle = Double.MAX_VALUE;
		double maxAngle = Double.MIN_VALUE;

		for (int beam = 0; beam < wcBeamCount; beam++) {
			double wcAngleRefAbsoluteDeg = wc_pointing_angle_deg.get(pingIdx, beam);
			minAngle = NumberUtils.Min(minAngle, wcAngleRefAbsoluteDeg);
			maxAngle = NumberUtils.Max(maxAngle, wcAngleRefAbsoluteDeg);
			int txBeamIndex = 0;
			if (wc_tx_beam_index.isPresent())
				txBeamIndex = Math.max(wc_tx_beam_index.get().get(pingIdx, beam), 0); // Nan sometimes
			int txTransducerIndex = 0;
			if (wc_tx_transducer_index.isPresent())
				txTransducerIndex = wc_tx_transducer_index.get().get(pingIdx, txBeamIndex);
			float surfaceSoundSpeed = soundspeedLayer.get(pingIdx);
			double sinEquivalentAngle = Math.sin(Math.toRadians(wcAngleRefAbsoluteDeg)) * medianCmCo;

			BeamPoint point = new BeamPoint();
			point.equivalentCelerity = surfaceSoundSpeed * medianCmCo;
			point.sinEquivalentAngle = sinEquivalentAngle;
			point.tiltAngleRad = Math.toRadians(equivalentTiltAnglePerSector.get(txBeamIndex));
			point.headingAngleRad = txTransducerIndex > 0 ? Math.toRadians(headingOffsets[txTransducerIndex])
					: Math.toRadians(txParameters.headingoffset);
			double sampleRate = 1.0 / wc_sample_interval.get(pingIdx);
			point.samplerate = sampleRate;

			indexedMatch.add(point);
		}
		//

		// recompute faked beam opening angle to ensure there is no z figthing
		double value = Math.abs(minAngle - maxAngle);
		if (value != 0 && ModelPreferences.getInstance().getRecompute_beam_widths().getValue()) {
			value = value / (wcBeamCount - 1);
			for (int i = 0; i < wcBeamCount; i++)
				data.beamwidth_across[i] = (float) (2 * value);
		}

	}

	/**
	 * Process a single sample
	 *
	 * @return
	 * @throws GIOException
	 */
	LatLonSample process(SwathBeamSample sample) {
		BeamPoint point = indexedMatch.get(sample.beam);

		// find X,Z by interpolation between bottomRange detection and this sample
		int rxAntennaIndex = sample.data.rxAntennaIdx[sample.beam];
		double rangeMeter = point.equivalentCelerity * sample.range / (2 * point.samplerate);
		double eqAngleRad = Math.asin(point.sinEquivalentAngle);
		double depthRelTrans = rangeMeter * Math.cos(eqAngleRad) * Math.cos(point.tiltAngleRad);
		double acrossOffset = rxAntennaIndex > 0 ? acrossOffsets[rxAntennaIndex] : rxParameters.yoffset;

		double alongOffset = rxAntennaIndex > 0 ? alongOffsets[rxAntennaIndex] : rxParameters.xoffset;

		double acrossDistance = -rangeMeter * point.sinEquivalentAngle + acrossOffset;

		double alongDistance = +rangeMeter * point.sinEquivalentAngle * Math.tan(point.headingAngleRad)
				+ depthRelTrans * Math.tan(point.tiltAngleRad) * Math.cos(point.headingAngleRad) + alongOffset; //
		double elevation = -depthRelTrans - summit.depth;
		// find lat/long position
		double height = Math.abs(point.equivalentCelerity / (point.samplerate * 2));
		height *= scaleFactor;
		double[] latlon = computeLatLong(alongDistance, acrossDistance);
		if (debug_print && sample.range == sample.data.bottomDetectionRange[sample.beam]) {
			System.out.println("beam; " + alongDistance + "; " + acrossDistance + "; " + elevation);
		}

		return new LatLonSample(latlon[0], latlon[1], acrossDistance, alongDistance, elevation, height, sample, ping);
	}

	/**
	 * compute lat long position
	 *
	 * @return the position lat index 0 and lon index 1
	 */
	private double[] computeLatLong(double xAlong, double yAcross) {
		return MbesUtils.acrossAlongToWGS84LatLong(shipLatitude, shipLongitude, heading, yAcross, xAlong);
	}

	/**
	 * @return the {@link #latitudeLayer}
	 */
	public DoubleLoadableLayer1D getLatitudeLayer() {
		return latitudeLayer;
	}

	/**
	 * @return the {@link #longitudeLayer}
	 */
	public DoubleLoadableLayer1D getLongitudeLayer() {
		return longitudeLayer;
	}

	/**
	 * @return the {@link #headingLayer}
	 */
	public FloatLoadableLayer1D getHeadingLayer() {
		return headingLayer;
	}

	/**
	 * @return the {@link #rxParameters}
	 */
	public AntennaInstallationParameters getRxParameters() {
		return rxParameters;
	}

	/**
	 * @return the {@link #tx_z_depthLayer}
	 */
	public Optional<FloatLoadableLayer1D> getTx_z_depthLayer() {
		return tx_z_depthLayer;
	}

	/**
	 * @return the {@link #ping}
	 */
	public SwathSpatialData getPing() {
		return ping;
	}
}
