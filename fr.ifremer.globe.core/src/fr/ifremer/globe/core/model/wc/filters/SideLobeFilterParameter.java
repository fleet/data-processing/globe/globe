package fr.ifremer.globe.core.model.wc.filters;

/**
 * Parameters for {@link SideLobeFilter}
 */
public class SideLobeFilterParameter extends BaseFilterParameter {
	private float threshold;
	
	public SideLobeFilterParameter(boolean enable, float threshold) {
		super(enable);
		this.setThreshold(threshold);
	}

	public static SideLobeFilterParameter getDefault() {
		return new SideLobeFilterParameter(false, 20.f);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(getThreshold());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SideLobeFilterParameter other = (SideLobeFilterParameter) obj;
		if (enable != other.enable)
			return false;
		if (Float.floatToIntBits(getThreshold()) != Float.floatToIntBits(other.getThreshold())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SideLobeFilterParameter [threshold=" + getThreshold() + "]";
	}

	/**
	 * @return the {@link #threshold}
	 */
	public float getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold the {@link #threshold} to set
	 */
	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}

}
