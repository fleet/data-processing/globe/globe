package fr.ifremer.globe.core.model.wc.filters;

/**
 * Threshold with a tolerance (as an int) associated value
 */
public class ThresholdToleranceParameter extends ThresholdParameter {

	private int tolerance;

	public ThresholdToleranceParameter(int tolerance, double minValue, double maxValue, boolean enable) {
		super(minValue, maxValue, enable);
		this.setTolerance(tolerance);
	}

	public static ThresholdToleranceParameter getDefault() {
		return new ThresholdToleranceParameter(0, minDefaultValue, maxDefaultValue, true);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + getTolerance();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ThresholdToleranceParameter other = (ThresholdToleranceParameter) obj;
		if (enable != other.enable)
			return false;
		if (getTolerance() != other.getTolerance()) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ThresholdToleranceParameter [tolerance=" + getTolerance() + "]";
	}

	/**
	 * @return the {@link #tolerance}
	 */
	public int getTolerance() {
		return tolerance;
	}

	/**
	 * @param tolerance the {@link #tolerance} to set
	 */
	public void setTolerance(int tolerance) {
		this.tolerance = tolerance;
	}

}
