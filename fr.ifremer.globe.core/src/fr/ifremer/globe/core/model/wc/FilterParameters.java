package fr.ifremer.globe.core.model.wc;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.wc.filters.AcrossDistanceParameter;
import fr.ifremer.globe.core.model.wc.filters.BeamParameter;
import fr.ifremer.globe.core.model.wc.filters.BottomFilterParameter;
import fr.ifremer.globe.core.model.wc.filters.DepthParameter;
import fr.ifremer.globe.core.model.wc.filters.SampleParameter;
import fr.ifremer.globe.core.model.wc.filters.SideLobeFilterParameter;
import fr.ifremer.globe.core.model.wc.filters.SpecularFilterParameter;
import fr.ifremer.globe.core.model.wc.filters.ThresholdParameter;
import fr.ifremer.globe.core.model.wc.filters.MultipingParameter;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Container for all filter parameters
 */
public class FilterParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<FilterParameters> SESSION_KEY = new TypedKey<>("Filter_Parameters");

	/** Format names of configuration file */
	public final static String[] CONFIGURATION_FORMAT_NAMES = new String[] { "JSON" };
	/** Format extension of configuration file */
	public final static String[] CONFIGURATION_FORMAT_EXTENSIONS = new String[] { "*.json" };

	private FilterParameters(boolean enabled, ThresholdParameter threshold, BeamParameter beam, SampleParameter sample, DepthParameter depth,
			SideLobeFilterParameter sidelobe, BottomFilterParameter bottom, SamplingParameter sampling, MultipingParameter multiping,
			SpecularFilterParameter specular, AcrossDistanceParameter acrossDistance,
			SpatializationMethodParameter spatializationMethod) {
		super();
		this.enabled = enabled;
		this.threshold = threshold;
		this.beam = beam;
		this.sample = sample;
		this.depth = depth;
		this.sidelobe = sidelobe;
		this.specular = specular;
		this.bottom = bottom;
		this.sampling = sampling;
		this.acrossDistance = acrossDistance;
		this.spatializationMethod = spatializationMethod;
		this.multiping = multiping;
	}

	public final boolean enabled;
	public final AcrossDistanceParameter acrossDistance;
	public final ThresholdParameter threshold;
	public final BeamParameter beam;
	public final SampleParameter sample;
	public final DepthParameter depth;
	public final SideLobeFilterParameter sidelobe;
	public final BottomFilterParameter bottom;
	public final SamplingParameter sampling;
	public final SpecularFilterParameter specular;
	public final SpatializationMethodParameter spatializationMethod;
	public final MultipingParameter multiping;

	public boolean getEnabled() {
		return enabled;
	}

	public AcrossDistanceParameter getAcrossDistance() {
		return acrossDistance != null ? acrossDistance : AcrossDistanceParameter.getDefault();
	}

	public ThresholdParameter getThreshold() {
		return threshold != null ? threshold : ThresholdParameter.getDefault();
	}

	public BeamParameter getBeam() {
		return beam != null ? beam : BeamParameter.getDefault();
	}

	public SampleParameter getSample() {
		return sample != null ? sample : SampleParameter.getDefault();
	}

	public DepthParameter getDepth() {
		return depth != null ? depth : DepthParameter.getDefault();
	}

	public SideLobeFilterParameter getSideLobe() {
		return sidelobe != null ? sidelobe : SideLobeFilterParameter.getDefault();
	}

	public BottomFilterParameter getBottom() {
		return bottom != null ? bottom : BottomFilterParameter.getDefault();
	}

	public SamplingParameter getSampling() {
		return sampling != null ? sampling : SamplingParameter.getDefault();
	}

	public MultipingParameter getMultiping() {
		return multiping != null ? multiping : MultipingParameter.getDefault();
	}
	
	public SpecularFilterParameter getSpecular() {
		return specular != null ? specular : SpecularFilterParameter.getDefault();
	}

	public SpatializationMethodParameter getSpatializationMethod() {
		return spatializationMethod != null ? spatializationMethod : SpatializationMethodParameter.getDefault();
	}

	/**
	 * load FilterParameters from a file
	 *
	 * @param file the file to load
	 * @throws GIOException
	 */
	public static FilterParameters load(File file) throws GIOException {
		try (JsonReader reader = new JsonReader(new FileReader(file))) {
			return new Gson().fromJson(reader, FilterParameters.class);
		} catch (JsonSyntaxException | IOException e) {
			throw new GIOException("Error while parsing file : ", e);
		}
	}

	/**
	 * save FilterParameters to a file
	 *
	 * @param file the file to save
	 * @throws GIOException
	 */
	public void save(File outputFile) throws GIOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		outputFile.getParentFile().mkdirs();
		try (JsonWriter writer = gson.newJsonWriter(new FileWriter(outputFile))) {
			gson.toJson(this, FilterParameters.class, writer);
		} catch (JsonSyntaxException | IOException e) {
			throw new GIOException("Error while writing file : ", e);
		}
	}

	/**
	 * load FilterParameters from a json string
	 *
	 * @param descr the string to load
	 */
	public static FilterParameters fromJson(String descr) {
		return new Gson().fromJson(descr, FilterParameters.class);
	}

	/**
	 * read FilterParameters as a json string
	 */
	public String toJson() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this, FilterParameters.class);
	}

	public static FilterParameters getDefault() {
		return new FilterParameters(false, ThresholdParameter.getDefault(), BeamParameter.getDefault(), SampleParameter.getDefault(),
				DepthParameter.getDefault(), SideLobeFilterParameter.getDefault(), BottomFilterParameter.getDefault(),
				SamplingParameter.getDefault(), MultipingParameter.getDefault(), SpecularFilterParameter.getDefault(),
				AcrossDistanceParameter.getDefault(), SpatializationMethodParameter.getDefault());
	}

	public static FilterParameters getDefault(ISounderNcInfo info) {
		return new FilterParameters(false, ThresholdParameter.getDefault(),
				new BeamParameter(0, (int) Math.max(0, info.getWcRxBeamCount() - 1), false), SampleParameter.getDefault(),
				DepthParameter.getDefault(), SideLobeFilterParameter.getDefault(), BottomFilterParameter.getDefault(),
				SamplingParameter.getDefault(), MultipingParameter.getDefault(), SpecularFilterParameter.getDefault(),
				AcrossDistanceParameter.getDefault(), SpatializationMethodParameter.getDefault());
	}

	public static FilterParameters getDefaultFrom(FilterParameters p) {
		if(p != null) {
			return new FilterParameters(p.getEnabled(), p.getThreshold(), p.getBeam(), p.getSample(), p.getDepth(), p.getSideLobe(),
				p.getBottom(), p.getSampling(), p.getMultiping(), p.getSpecular(), p.getAcrossDistance(), p.getSpatializationMethod());
		} else {
			return getDefault();
		}
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link enabled}
	 */
	public FilterParameters fromEnabled(boolean p) {
		return new FilterParameters(p, threshold, beam, sample, depth, sidelobe, bottom, sampling, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link ThresholdParameter}
	 */
	public FilterParameters from(ThresholdParameter p) {
		return new FilterParameters(enabled, p, beam, sample, depth, sidelobe, bottom, sampling, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link BeamParameter}
	 */
	public FilterParameters from(BeamParameter p) {
		return new FilterParameters(enabled, threshold, p, sample, depth, sidelobe, bottom, sampling, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link SampleParameter}
	 */
	public FilterParameters from(SampleParameter p) {
		return new FilterParameters(enabled, threshold, beam, p, depth, sidelobe, bottom, sampling, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link DepthParameter}
	 */
	public FilterParameters from(DepthParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, p, sidelobe, bottom, sampling, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link SideLobeFilterParameter}
	 */
	public FilterParameters from(SideLobeFilterParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, depth, p, bottom, sampling, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link BottomFilterParameter}
	 */
	public FilterParameters from(BottomFilterParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, depth, sidelobe, p, sampling, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link SamplingParameter}
	 */
	public FilterParameters from(SamplingParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, depth, sidelobe, bottom, p, multiping, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link MultipingParameter}
	 */
	public FilterParameters from(MultipingParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, depth, sidelobe, bottom, sampling, p, specular, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing {@link SpecularFilterParameter}
	 */
	public FilterParameters from(SpecularFilterParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, depth, sidelobe, bottom, sampling, multiping, p, acrossDistance,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing across distance filter parameter
	 */
	public FilterParameters fromAcrossDist(AcrossDistanceParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, depth, sidelobe, bottom, sampling, multiping, specular, p,
				spatializationMethod);
	}

	/**
	 * Construct a new {@link FilterParameters} as a copy of this one, and changing spatialization method parameter
	 */
	public FilterParameters from(SpatializationMethodParameter p) {
		return new FilterParameters(enabled, threshold, beam, sample, depth, sidelobe, bottom, sampling, multiping, specular,
				acrossDistance, p);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Boolean.hashCode(enabled);
		result = prime * result + (acrossDistance == null ? 0 : acrossDistance.hashCode());
		result = prime * result + (beam == null ? 0 : beam.hashCode());
		result = prime * result + (sample == null ? 0 : sample.hashCode());
		result = prime * result + (bottom == null ? 0 : bottom.hashCode());
		result = prime * result + (depth == null ? 0 : depth.hashCode());
		result = prime * result + (sampling == null ? 0 : sampling.hashCode());
		result = prime * result + (multiping == null ? 0 : multiping.hashCode());
		result = prime * result + (sidelobe == null ? 0 : sidelobe.hashCode());
		result = prime * result + (threshold == null ? 0 : threshold.hashCode());
		result = prime * result + (specular == null ? 0 : specular.hashCode());
		result = prime * result + (spatializationMethod == null ? 0 : spatializationMethod.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilterParameters other = (FilterParameters) obj;
		if (enabled != other.enabled)
			return false;
		if (acrossDistance == null) {
			if (other.acrossDistance != null)
				return false;
		} else if (!acrossDistance.equals(other.acrossDistance))
			return false;
		if (beam == null) {
			if (other.beam != null)
				return false;
		} else if (!beam.equals(other.beam))
			return false;
		if (sample == null) {
			if (other.sample != null)
				return false;
		} else if (!sample.equals(other.sample))
			return false;
		if (bottom == null) {
			if (other.bottom != null)
				return false;
		} else if (!bottom.equals(other.bottom))
			return false;
		if (depth == null) {
			if (other.depth != null)
				return false;
		} else if (!depth.equals(other.depth))
			return false;
		if (sampling == null) {
			if (other.sampling != null)
				return false;
		} else if (!sampling.equals(other.sampling))
			return false;
		if (multiping == null) {
			if (other.multiping != null)
				return false;
		} else if (!multiping.equals(other.multiping))
			return false;
		if (sidelobe == null) {
			if (other.sidelobe != null)
				return false;
		} else if (!sidelobe.equals(other.sidelobe))
			return false;
		if (threshold == null) {
			if (other.threshold != null)
				return false;
		} else if (!threshold.equals(other.threshold))
			return false;
		if (specular == null) {
			if (other.specular != null)
				return false;
		} else if (!specular.equals(other.specular))
			return false;
		if (spatializationMethod == null) {
			if (other.spatializationMethod != null)
				return false;
		} else if (!spatializationMethod.equals(other.spatializationMethod))
			return false;
		return true;
	}

	/**
	 * Wrap this bean to a TypedEntry with SESSION_KEY as key
	 */
	public TypedEntry<FilterParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

}
