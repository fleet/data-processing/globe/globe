package fr.ifremer.globe.core.model.wc;

/**
 * data linked to the ping
 * */
public class SwathSpatialData
{
	public final int pingIndex;
	public final double shipLatitudeDeg;
	public final double shipLongitudeDeg;
	public final double shipHeadingDeg;
	public final double shipDepth; //immersion
	public final long pingTimeNs;//nanoseconds since 1970-01-01 00:00:00Z
	
	/**
	 * topPoint is considered to be the rattachment point of water column data, 
	 * it matches the transducer position (if there is only one) all water column is attached to this point
	 * */
	public final double topPointLatitude;
	public final double topPointLongitude;
	public final double topPointDepth;
	public SwathSpatialData(int pingIndex, double shipLatitudeDeg, double shipLongitudeDeg, double shipHeadingDeg,
			double shipDepth, double topPointLatitude, double topPointLongitude, double topPointDepth,long pingTimeNs) {
		super();
		this.pingIndex = pingIndex;
		this.shipLatitudeDeg = shipLatitudeDeg;
		this.shipLongitudeDeg = shipLongitudeDeg;
		this.shipHeadingDeg = shipHeadingDeg;
		this.shipDepth = shipDepth;
		this.topPointLatitude = topPointLatitude;
		this.topPointLongitude = topPointLongitude;
		this.topPointDepth = topPointDepth;
		this.pingTimeNs=pingTimeNs;
	}
	

}