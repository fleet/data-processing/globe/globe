package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.LatLonSample;

public class AcrossDistanceFilter implements ISpatialFilter{

	private AcrossDistanceParameter parameters;

	@Override
	public boolean filter(LatLonSample sample) {
		return !parameters.isEnable()
				|| (parameters.getMinValue() <= sample.acrossDistance && sample.acrossDistance <= parameters.getMaxValue());
	}

	public AcrossDistanceFilter(AcrossDistanceParameter acrossDistance) {
		this.parameters=acrossDistance;
	}

	public void setParameters(FilterParameters parameters) {
		this.parameters = parameters.acrossDistance;
	}
}
