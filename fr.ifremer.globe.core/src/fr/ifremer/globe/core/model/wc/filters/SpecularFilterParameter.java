package fr.ifremer.globe.core.model.wc.filters;

public class SpecularFilterParameter extends BaseFilterParameter {
	private int tolerance;
	private boolean below;

	public SpecularFilterParameter(boolean enable, boolean filterBellow, int tolerance) {
		super(enable);
		this.setFilterBelow(filterBellow);
		this.setTolerance(tolerance);
	}

	public static SpecularFilterParameter getDefault() {
		return new SpecularFilterParameter(false, true, 3);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Boolean.hashCode(below);
		result = prime * result + getTolerance();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SpecularFilterParameter other = (SpecularFilterParameter) obj;
		if (enable != other.enable)
			return false;
		if (below != other.below)
			return false;
		if (getTolerance() != other.getTolerance()) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SpecularFilterParameter [below=" + Boolean.toString(below) + "tolerance=" + getTolerance()+"]";
	}

	/**
	 * @return the {@link #tolerance}
	 */
	public int getTolerance() {
		return tolerance;
	}

	/**
	 * @param tolerance the {@link #tolerance} to set
	 */
	public void setTolerance(int tolerance) {
		this.tolerance = tolerance;
	}


	/**
	 * @param tolerance the {@link #below} to set
	 */
	public void setFilterBelow(boolean bellow) {
		this.below = bellow;
	}
	
	/**
	 * @return the {@link #below}
	 */
	public boolean getFilterBelow() {
		return below;
	}
}
