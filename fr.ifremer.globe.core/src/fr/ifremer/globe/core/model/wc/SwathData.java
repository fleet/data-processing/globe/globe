package fr.ifremer.globe.core.model.wc;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.vlen.Float3DVlenBuffer;

public class SwathData {
	public final Float3DVlenBuffer matrix;
	public final int pingIndex;
	public final int sampling;

	public SwathData(int pingIndex, Float3DVlenBuffer data, int sampling) {
		super();
		this.matrix = data;
		this.pingIndex = pingIndex;
		this.sampling = sampling;
		beamwidth_across = new float[data.getColCount()];
		beamwidth_along = new float[data.getColCount()];
		txSectorIdx = new int[data.getColCount()];
		rxAntennaIdx = new short[data.getColCount()];
		bottomDetectionRange = new int[data.getColCount()];
		wcAngleRefAbsoluteDeg = new double[data.getColCount()];

	}

	public float beamwidth_across[];// One-way beam width at half power down
	public float beamwidth_along[];// One-way beam width at half power down
	public int txSectorIdx[];// One-way beam width at half power down
	public short rxAntennaIdx[];// One-way beam width at half power down
	public int bottomDetectionRange[]; // bottom detection range
	public final double wcAngleRefAbsoluteDeg[]; // beam pointing angle in earth referential
	public int sphereRange; // range of the sphere , ie min value of the bottom ranges
}
