package fr.ifremer.globe.core.model.wc;

import java.util.TreeSet;
import java.util.function.ToDoubleFunction;

import fr.ifremer.globe.utils.exception.runtime.AssumptionFailedException;

/**
 * Collection able to find the nearest element of a value
 * Use a TreeSet as a backend
 * 
 * */
public class NearestFinder<E> {
	TreeSet<E> tree;
	private ToDoubleFunction<E> valueGetter;
	public NearestFinder(ToDoubleFunction<E> valueGetter) {
		tree=new TreeSet<>((a,b)-> (int) Math.signum(valueGetter.applyAsDouble(a)-valueGetter.applyAsDouble(b)));
		this.valueGetter=valueGetter;
	}
	/**
	 * find the nearest value in the TreeSet
	 * */
	public E findNearest(E search)
	{
		//create a fake bathydata for searching in the treeset 
		E higher=tree.ceiling(search);
		E lower=tree.floor(search);
		if(higher==null && lower ==null)
			throw new AssumptionFailedException("No valid bathymetry data available for WC spatial projection computation");
		
		double higherAngularDistance = Double.MAX_VALUE;
		if(higher!=null)
			higherAngularDistance = Math.abs(valueGetter.applyAsDouble(higher)-valueGetter.applyAsDouble(search));
		double lowerAngularDistance= Double.MAX_VALUE;
		if(lower!=null)
			lowerAngularDistance = Math.abs(valueGetter.applyAsDouble(lower)-valueGetter.applyAsDouble(search));
		if(lowerAngularDistance < higherAngularDistance)
		{
			return lower;
		}else {
			return higher;
		}
	}
	/**
	 * see {@link TreeSet#add(Object)}
	 * */
	public void add(E e)
	{
		tree.add(e);
	}
	/**
	 * Returns true if this set contains no elements.
	 * */
	public boolean isEmpty() {
		return tree.isEmpty();
	}
}
