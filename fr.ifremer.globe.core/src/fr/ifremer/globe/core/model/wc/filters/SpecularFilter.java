package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.SwathBeamSample;

public class SpecularFilter implements IFilter {

	private SpecularFilterParameter parameters;

	@Override
	public boolean filter(SwathBeamSample sample) {
		return !parameters.isEnable() || reallyFilter(sample);
	}

	/**
	 * Filter sample, return false if filtered, true otherwise
	 */
	private boolean reallyFilter(SwathBeamSample sample) {
		if (parameters.getFilterBelow()) {
			return sample.range >= sample.data.bottomDetectionRange[sample.beam] - parameters.getTolerance();
		} else {
			return Math.abs(sample.range - sample.data.bottomDetectionRange[sample.beam]) <= parameters.getTolerance();
		}
	}

	public SpecularFilter(SpecularFilterParameter param) {
		this.parameters = param;
	}

	public void setParameters(FilterParameters parameters) {
		this.parameters = parameters.specular;
	}
}
