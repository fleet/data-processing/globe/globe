package fr.ifremer.globe.core.model.wc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.Pair;

/**
 * Maintain a list of data ordered by index Data is maintained in a list based around the current index with a depth in
 * index before and after the current value This class is a sliding buffer of object around a given index value No
 * loading is queried until {@link #setCurrent(int)} method is called
 **/
public class SlidingBuffer<T extends Manageable> {
	Logger logger = LoggerFactory.getLogger(SlidingBuffer.class);
	Map<Integer, T> collection = new ConcurrentSkipListMap<>();

	SlidingBufferSetting settings;

	private int currentIndex = -1;
	private int maxLimit;
	private final int minLimit = 0;
	private DataProducer<T> producer;
	private boolean producing = false;

	/**
	 * build a new Buffer
	 *
	 * @param producer the data producer
	 * @param backLength the size of data in backward direction
	 * @param forwardLength the size of data in forward direction
	 * @param maxLimit the maximum index possible
	 */
	public static <T extends Manageable> SlidingBuffer<T> build(DataProducer<T> producer, int backLength,
			int forwardLenght, int maxLimit) {
		return new SlidingBuffer<>(producer, backLength, forwardLenght, maxLimit);
	}

	/**
	 * build a new Buffer with a storage length of one
	 *
	 * @param producer the data producer
	 * @param maxLimit the maximum index possible , it is not use but never will be any data queried to the data
	 *            producer with an index value higher than this value
	 */
	public static <T extends Manageable> SlidingBuffer<T> buildSingle(DataProducer<T> producer, int maxLimit) {
		return new SlidingBuffer<>(producer, 0, 0, maxLimit);
	}

	private SlidingBuffer(DataProducer<T> producer, int backLength, int forwardLenght, int maxLimit) {
		settings = new SlidingBufferSetting(backLength, forwardLenght);
		this.producer = producer;
		this.maxLimit = maxLimit;
	}

	/**
	 * set the current index to the current value and read/release data accordingly
	 */
	public void setCurrent(int index) {
		currentIndex = index;
		producing = true;
		var minMax = getMinMaxForCurrentIndex(index);
		
		int minIndex = minMax.getFirst();
		int maxIndex = minMax.getSecond();
		
		// add expected object
		for (int i = minIndex; i <= maxIndex && producing; i++) {
			if (!collection.containsKey(i)) {
				try {
					T value = producer.produce(i);
					if (value != null)
						collection.put(i, value);
				} catch (Exception e) {
					logger.error("Error while creating value for index " + i, e);
				}
			}
		}
		// remove obsolete objects
		for (int i = minLimit; i < minIndex; i++) {
			T object = collection.remove(i);
			if (object != null) {
				object.close();
			}
		}
		for (int i = maxIndex + 1; i <= maxLimit; i++) {
			T object = collection.remove(i);
			if (object != null) {
				object.close();
			}
		}
		producing = false;
	}

	/**
	 * get the current index
	 */
	public int getCurrentIndex() {
		return currentIndex;
	}
	
	/** {@inheritDoc} */
	public void dispose() {
		clear();
	}

	/**
	 * Interrupt slide production
	 */
	public void stopProducing() {
		producing = false;
	}

	/**
	 * release all data
	 */
	public void clear() {
		currentIndex = -1;
		collection.values().forEach(T::close);
		collection.clear();
	}

	/**
	 * return a collection copy view of data.
	 */
	public Collection<T> values() {
		return new ArrayList<>(collection.values());
	}

	/**
	 * return the min/max for current index
	 */
	public Pair<Integer,Integer> getMinMaxForCurrentIndex(int index) {
		int minIndex = Math.max(index - settings.backLength, minLimit);
		int maxIndex = Math.min(index + settings.forwardLength, maxLimit);
		if(minIndex == minLimit) {
			maxIndex = Math.min(minLimit + settings.backLength + settings.forwardLength, maxLimit);
		} else if(maxIndex == maxLimit) {
			minIndex = Math.max(maxLimit - settings.backLength - settings.forwardLength, minLimit);
		}
		return new Pair<>(minIndex,maxIndex);
	}
	
	
	/**
	 * return the back length size of the buffer
	 */
	public int getBackLength() {
		return settings.backLength;
	}

	/**
	 * set the back length size of the buffer will NOT immediately trigger a load of the data
	 */
	public void setBackLength(int backLength) {
		settings = settings.fromBackLength(backLength);
		stopProducing();
	}

	/**
	 * return the back length size of the buffer
	 */
	public int getForwardLenght() {
		return settings.getForwardLength();
	}

	/**
	 * set the forward length size of the buffer will NOT immediately trigger a load of the data *
	 */
	public void setForwardLenght(int forwardLenght) {
		settings = settings.fromForwardLenght(forwardLenght);
		stopProducing();
	}

	public SlidingBufferSetting getBufferSettings() {
		return settings;
	}

	public void setBufferSettings(SlidingBufferSetting bufferSettings) {
		settings = bufferSettings;
		stopProducing();
	}
}
