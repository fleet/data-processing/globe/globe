package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.LatLonSample;

public class DepthFilter implements ISpatialFilter {
	private DepthParameter parameters;

	@Override
	public boolean filter(LatLonSample sample) {
		return !parameters.isEnable() || 
				( sample.elevation >= -parameters.getMaxValue() && sample.elevation<=-parameters.getMinValue());
	}

	public DepthFilter(DepthParameter parameters) {
		this.parameters=parameters;
	}
	@Override
	public void setParameters(FilterParameters parameters) {
		this.parameters=parameters.depth;
	}

	
}
