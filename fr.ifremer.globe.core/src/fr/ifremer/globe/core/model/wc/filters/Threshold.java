package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.SwathBeamSample;

public class Threshold implements IFilter{

	private ThresholdParameter parameters;

	@Override
	public boolean filter(SwathBeamSample sample) {
		return !parameters.isEnable() || ( sample.value >= parameters.getMinValue() && sample.value<=parameters.getMaxValue());
	}

	public Threshold(ThresholdParameter param) {
		this.parameters=param;
	}

	public void setParameters(FilterParameters parameters) {
		this.parameters = parameters.threshold;
	}
}
