package fr.ifremer.globe.core.model.wc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.wc.filters.EchoFilters;
import fr.ifremer.globe.core.runtime.datacontainer.layers.Float2DVlenLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.vlen.Float3DVlenBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.signal.SignalUtils;

public class PingLoader {
	private Logger logger = LoggerFactory.getLogger(PingLoader.class);

	private Float2DVlenLayer sample;
	private FloatLoadableLayer2D beamwidthReceiveMajorLayer;
	private FloatLoadableLayer2D beamwidthTxMinor;
	private Optional<IntLoadableLayer2D> txSector = Optional.empty();
	private Optional<IntLoadableLayer1D> rxAntennaIndex = Optional.empty();
	private Optional<FloatLoadableLayer2D> wc_bottom_range = Optional.empty();
	private Optional<FloatLoadableLayer2D> detectionTwoWayTravelTime = Optional.empty();
	private FloatLoadableLayer2D wc_beam_pointing_angle_deg;

	List<Processor> processor = new ArrayList<>();
	private Spatializer spatializer;
	private EchoFilters filters;

	public FilterParameters getFilterParameters() {
		return filterParameters;
	}

	private FilterParameters filterParameters;

	private Sampler sampler;

	private XsfWCPreferences displayPreferences;

	private FloatLoadableLayer2D sample_time_offset;

	private FloatLoadableLayer1D sample_interval;

	private FloatLoadableLayer2D blanking_interval;

	private FloatLoadableLayer1D soundspeed_at_tx;

	private boolean convertToDb;

	public PingLoader(SounderDataContainer container, SonarBeamGroup sonarBeamGroup, FilterParameters filterParameters,
			boolean convertToDb) throws GIOException {
		sample = (Float2DVlenLayer) sonarBeamGroup.getVlenLayer(BeamGroup1Layers.BACKSCATTER_R_VARIABLE_NAME);
		// create default filter parameters
		this.convertToDb = convertToDb;
		displayPreferences = Activator.getXsfWCPreferences();
		this.filterParameters = filterParameters;
		filters = new EchoFilters(filterParameters);
		processor.add(filters);
		spatializer = new Spatializer(container, sonarBeamGroup);
		processor.add(spatializer);
		sampler = new Sampler(container, filterParameters.sampling);

		beamwidthReceiveMajorLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.BEAMWIDTH_RECEIVE_MAJOR);
		beamwidthTxMinor = sonarBeamGroup.getLayer(BeamGroup1Layers.BEAMWIDTH_TRANSMIT_MINOR);
		if (container.hasLayer(BeamGroup1Layers.TRANSMIT_BEAM_INDEX)) {
			txSector = Optional.of(sonarBeamGroup.getLayer(BeamGroup1Layers.TRANSMIT_BEAM_INDEX));
		} else {
			txSector = Optional.empty();
		}
		if (container.hasLayer(BeamGroup1Layers.RECEIVE_TRANSDUCER_INDEX)) {
			rxAntennaIndex = Optional.of(sonarBeamGroup.getLayer(BeamGroup1Layers.RECEIVE_TRANSDUCER_INDEX));
		} else {
			rxAntennaIndex = Optional.empty();
		}

		if (container.hasLayer(BeamGroup1Layers.DETECTED_BOTTOM_RANGE)) {
			wc_bottom_range = Optional.of(sonarBeamGroup.getLayer(BeamGroup1Layers.DETECTED_BOTTOM_RANGE));

			soundspeed_at_tx = sonarBeamGroup.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER); // MANDATORY
			sample_interval = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_INTERVAL); // MANDATORY
			sample_time_offset = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_TIME_OFFSET); // MANDATORY
			blanking_interval = sonarBeamGroup.getLayer(BeamGroup1Layers.BLANKING_INTERVAL); // MANDATORY
			// range is sound_speed_at_transducer*(blanking_interval+bottom_index*sample_interval -
			// sample_time_offset)/2.
		} else if (container.hasLayer(BathymetryLayers.DETECTION_TWO_WAY_TRAVEL_TIME)) { // Reson
			detectionTwoWayTravelTime = Optional
					.of(sonarBeamGroup.getLayer(BathymetryLayers.DETECTION_TWO_WAY_TRAVEL_TIME));
			sample_interval = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_INTERVAL); // MANDATORY
			sample_time_offset = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_TIME_OFFSET); // MANDATORY
			blanking_interval = sonarBeamGroup.getLayer(BeamGroup1Layers.BLANKING_INTERVAL); // MANDATORY
		}
		wc_beam_pointing_angle_deg = sonarBeamGroup.getLayer(BeamGroup1Layers.RX_BEAM_ROTATION_PHI);

	}

	public void setFilterParameters(FilterParameters filterParameters) {
		this.filterParameters = filterParameters;
		sampler.setParameters(filterParameters);
		// update all filters with parameters
		filters.setParameters(filterParameters);
	}

	int currentPingIndex = -1;

	/**
	 * Load a WC ping
	 *
	 * @throws GIOException
	 */
	public synchronized LinkedList<LatLonSample> load(int pingIndex) throws GIOException {
		if (currentPingIndex != pingIndex) {
			currentPingIndex = pingIndex;
			sample.load(pingIndex);
			// update data for amplitude -> db convertions

			Float3DVlenBuffer values = sample.getData();
			if (displayPreferences.isDisplayAmplitudeAsdB() && convertToDb) {
				double offset = displayPreferences.getResonGlobalOffset();
				values.applyTransform(v -> SignalUtils.amplitudeTodB(v - offset + 1));
			}

		}
		SwathData data = new SwathData(pingIndex, sample.getData(), filterParameters.sampling.getSampling());
		int minBottomRange = data.matrix.getRowCount();
		// compute statistics
		for (int col = 0; col < data.matrix.getColCount(); col++) {
			data.wcAngleRefAbsoluteDeg[col] = wc_beam_pointing_angle_deg.get(pingIndex, col);
			if (txSector.isPresent()) {
				data.txSectorIdx[col] = txSector.get().get(pingIndex, col);
			} else {
				data.txSectorIdx[col] = 0;
			}
			if (rxAntennaIndex.isPresent()) {
				data.rxAntennaIdx[col] = (short) rxAntennaIndex.get().get(col);
			} else {
				data.rxAntennaIdx[col] = 0;
			}

			int txSectorIndex = data.txSectorIdx[col];
			int bottomRangeIndex = minBottomRange;

			// beam widths
			data.beamwidth_along[col] = beamwidthTxMinor.get(pingIndex, txSectorIndex);
			data.beamwidth_across[col] = beamwidthReceiveMajorLayer.get(pingIndex, col);

			if (wc_bottom_range.isPresent()) {
				double bottomRangeInMeter = wc_bottom_range.get().get(pingIndex, col);
				double speed = soundspeed_at_tx.get(pingIndex);
				double blanking = blanking_interval.get(pingIndex, col);
				double sample_interval_value = sample_interval.get(pingIndex);
				double time_offset = sample_time_offset.get(pingIndex, txSectorIndex);

				bottomRangeIndex = (int) Math
						.round((2 * bottomRangeInMeter / speed + time_offset - blanking) / sample_interval_value);
				// range is sound_speed_at_transducer*(blanking_interval+bottom_index*sample_interval -
				// sample_time_offset)/2.

			} else if (detectionTwoWayTravelTime.isPresent()) {
				double travelTime = detectionTwoWayTravelTime.get().get(pingIndex, col);
				double blanking = blanking_interval.get(pingIndex, col);
				double sample_interval_value = sample_interval.get(pingIndex);
				double time_offset = sample_time_offset.get(pingIndex, txSectorIndex);
				bottomRangeIndex = (int) Math.round((travelTime + time_offset - blanking) / sample_interval_value);
			}
			data.bottomDetectionRange[col] = bottomRangeIndex;
			if (bottomRangeIndex >= 1) // if valid
			{
				minBottomRange = Math.min(minBottomRange, bottomRangeIndex);
			}
		}
		data.sphereRange = minBottomRange;

		getProcessorAsList().stream().forEach(x -> {
			try {
				x.configure(pingIndex, data);
			} catch (GIOException e) {
				logger.warn("An error occured while configuring WC PingLoader for ping " + pingIndex + " message:"
						+ e.getMessage(), e);
			}
		});
		List<SwathBeamSample> samples = sampler.sample(data);
		// now stream the values to the processes
		return process(samples);
	}

	private List<Processor> getProcessorAsList() {
		return processor;
	}

	/**
	 * Stream the data
	 */
	private LinkedList<LatLonSample> process(List<SwathBeamSample> samples) {
		return samples.parallelStream().filter(filters.getThreshold()::filter) // threshold filter
				.filter(filters.getBeam()::filter) // beam index filter
				.filter(filters.getSpecular()::filter) // specular filter
				.filter(filters.getBottom()::filter).filter(filters.getSideLobe()::filter)
				.map(x -> spatializer.process(x)) // spatialisation
				.filter(filters.getDepth()::filter)// spatial filters
				.filter(filters.getAcross()::filter)// across distance filter
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * @return the {@link #convertToDb}
	 */
	public boolean isConvertToDb() {
		return convertToDb;
	}

	/**
	 * @return the {@link #spatializer}
	 */
	public Spatializer getSpatializer() {
		return spatializer;
	}
}
