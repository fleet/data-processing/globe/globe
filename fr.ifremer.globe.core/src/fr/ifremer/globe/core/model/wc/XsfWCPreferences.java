package fr.ifremer.globe.core.model.wc;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DoublePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;

public class XsfWCPreferences extends PreferenceComposite {

	private BooleanPreferenceAttribute displayAmplitudeAsdB;
	private DoublePreferenceAttribute resonGlobalOffset;
	private IntegerPreferenceAttribute beamGroupDefaultValue;
	private BooleanPreferenceAttribute computeFlattenTextureOnLoading;
		
	public XsfWCPreferences(PreferenceComposite superNode) {
		super(superNode, "XSF Water Column Display preference");
		displayAmplitudeAsdB = new BooleanPreferenceAttribute("displayAmplitudeAsdB",
				"Display WC Amplitude Backscatter data in dB", true);
		resonGlobalOffset = new DoublePreferenceAttribute("resonGlobalOffset",
				"Reson global offset substracted to data when converting to dB", -42.);
		beamGroupDefaultValue = new IntegerPreferenceAttribute("beamGroupDefaultValue", "BeamGroupId used to compute Flatten WC", 0);
		computeFlattenTextureOnLoading = new BooleanPreferenceAttribute("computeFlattenTextureOnLoading",
				"Computes Flatten WC on loading", false);
		
		declareAttribute(displayAmplitudeAsdB);
		declareAttribute(resonGlobalOffset);
		declareAttribute(beamGroupDefaultValue);
		declareAttribute(computeFlattenTextureOnLoading);

		load();

	}

	public boolean isDisplayAmplitudeAsdB() {
		return displayAmplitudeAsdB.getValue();
	}

	public void setDisplayAmplitudeAsdB(boolean displayAmplitudeAsdB) {
		this.displayAmplitudeAsdB.setValue(displayAmplitudeAsdB, false);
		save();
	}

	public void setResonGlobalOffset(double resonGlobalOffset) {
		this.resonGlobalOffset.setValue(resonGlobalOffset, false);
		save();
	}

	public double getResonGlobalOffset() {
		return resonGlobalOffset.getValue();
	}

	public int getBeamGroupDefaultValue() {
		return beamGroupDefaultValue.getValue();
	}
	
	public boolean getComputeFlattenTextureOnLoading() {
		return computeFlattenTextureOnLoading.getValue();
	}
}
