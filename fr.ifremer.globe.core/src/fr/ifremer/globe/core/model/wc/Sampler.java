package fr.ifremer.globe.core.model.wc;

import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Class sample beam echoes values and keeping the maximum value for each
 * 
 * */
public class Sampler {
	private SamplingParameter parameters;
	Sampler(SounderDataContainer container,SamplingParameter parameters) throws GIOException
	{
		this.parameters=parameters;
	}
	public void setParameters(FilterParameters parameters)
	{
		this.parameters=parameters.sampling;
	}
	public List<SwathBeamSample> sample(SwathData data) throws GIOException
	{
		int pingIndex=data.pingIndex;
		List<SwathBeamSample> samples=new LinkedList<>();
		for(int col=0;col<data.matrix.getColCount();col++)
		{
			
			//avoid creating data with no values
			for(int row=0;row<data.matrix.getRowCount();row+=parameters.getSampling())
			{
				float value=data.matrix.get(row, col,0);
				if(!Float.isNaN(value))
				{
					samples.add(new SwathBeamSample(row,col,value,data));
				}
			}
		}
		return samples;
	}

}
