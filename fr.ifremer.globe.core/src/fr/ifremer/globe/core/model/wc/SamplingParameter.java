package fr.ifremer.globe.core.model.wc;

public class SamplingParameter {
	private int sampling;

	public SamplingParameter(int sampling) {
		this.setSampling(sampling);
	}

	public static SamplingParameter getDefault() {
		return new SamplingParameter(1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getSampling();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SamplingParameter other = (SamplingParameter) obj;
		if (getSampling() != other.getSampling()) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SamplingParameter [sampling=" + getSampling() + "]";
	}

	/**
	 * @return the {@link #sampling}
	 */
	public int getSampling() {
		return sampling;
	}

	/**
	 * @param sampling the {@link #sampling} to set
	 */
	public void setSampling(int sampling) {
		this.sampling = sampling;
	}

}
