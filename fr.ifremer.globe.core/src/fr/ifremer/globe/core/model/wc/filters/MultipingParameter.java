package fr.ifremer.globe.core.model.wc.filters;

public class MultipingParameter extends BaseFilterParameter {
	private int index;

	public MultipingParameter(boolean enable, int index) {
		super(enable);
		this.setMultipingIndex(index);
	}

	public static MultipingParameter getDefault() {
		return new MultipingParameter(false, 0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getMultipingIndex();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MultipingParameter other = (MultipingParameter) obj;
		if (enable != other.enable)
			return false;
		if (getMultipingIndex() != other.getMultipingIndex()) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "MultipingParameter [index=" + getMultipingIndex()+"]";
	}

	/**
	 * @return the {@link #index}
	 */
	public int getMultipingIndex() {
		return index;
	}

	/**
	 * @param index the {@link #index} to set
	 */
	public void setMultipingIndex(int index) {
		this.index = index;
	}
}
