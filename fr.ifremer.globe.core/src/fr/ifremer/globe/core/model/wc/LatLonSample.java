package fr.ifremer.globe.core.model.wc;

/**
 * A spatialized wc sample
 * */
public class LatLonSample {
	
	public LatLonSample(double latitude, double longitude, double acrossDistance, double alongDistance, double elevation,double height,
			SwathBeamSample sample,SwathSpatialData ping) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.acrossDistance = acrossDistance;
		this.alongDistance = alongDistance;
		this.elevation = elevation;
		this.echo = sample;
		this.ping=ping;
		this.height=height;
	}
	public final double latitude;
	public final double longitude;
	public final double acrossDistance;
	public final double alongDistance;
	public final double elevation;
	public final double height; //height of an echo (in meters)
	public final SwathBeamSample echo;
	public final SwathSpatialData ping;
	
}
