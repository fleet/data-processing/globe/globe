package fr.ifremer.globe.core.model.wc;

public enum SpatializationMethodParameter {

	DetectionInterpolation, EquivalentCelerity, ConstantCelerity;

	public static SpatializationMethodParameter getDefault() {
		return DetectionInterpolation;
	}

	@Override
	public String toString() {
		return "SpatializationMethodParameter [method=" + super.toString() + "]";
	}
}
