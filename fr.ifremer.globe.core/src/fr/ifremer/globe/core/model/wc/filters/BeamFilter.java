package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.SwathBeamSample;

public class BeamFilter implements IFilter{

	private BeamParameter parameters;

	@Override
	public boolean filter(SwathBeamSample sample) {
		return !parameters.isEnable() || ( sample.beam >= parameters.getMinValue() && sample.beam<=parameters.getMaxValue());
	}

	public BeamFilter(BeamParameter param) {
		this.parameters=param;
	}

	public void setParameters(FilterParameters parameters) {
		this.parameters = parameters.beam;
	}
}
