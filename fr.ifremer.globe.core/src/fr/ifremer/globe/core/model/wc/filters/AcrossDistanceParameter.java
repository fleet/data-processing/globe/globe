package fr.ifremer.globe.core.model.wc.filters;

public class AcrossDistanceParameter extends BaseFilterParameter {
	private float minValue;
	private float maxValue;

	public static final float minDefaultValue = -5000;
	public static final float maxDefaultValue = 5000;

	public AcrossDistanceParameter(double minValue, double maxValue, boolean enable) {
		super(enable);
		this.setMinValue((float) minValue);
		this.setMaxValue((float) maxValue);
	}

	public static AcrossDistanceParameter getDefault() {
		return new AcrossDistanceParameter(minDefaultValue, maxDefaultValue, false);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(getMaxValue());
		result = prime * result + Float.floatToIntBits(getMinValue());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AcrossDistanceParameter other = (AcrossDistanceParameter) obj;
		if (enable != other.enable)
			return false;
		if (Float.floatToIntBits(getMaxValue()) != Float.floatToIntBits(other.getMaxValue())) {
			return false;
		}
		if (Float.floatToIntBits(getMinValue()) != Float.floatToIntBits(other.getMinValue())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Across distance parameter [minValue=" + getMinValue() + ", maxValue=" + getMaxValue() + "]";
	}

	/**
	 * @return the {@link #minValue}
	 */
	public float getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue the {@link #minValue} to set
	 */
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the {@link #maxValue}
	 */
	public float getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the {@link #maxValue} to set
	 */
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

}
