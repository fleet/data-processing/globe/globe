package fr.ifremer.globe.core.model.wc.filters;

/**
 * Base class for filter parameters
 */
public class BaseFilterParameter {
	protected boolean enable;

	public BaseFilterParameter(boolean enable) {
		setEnable(enable);
	}

	/**
	 * @return the {@link #enable}
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * @param enable the {@link #enable} to set
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}

}
