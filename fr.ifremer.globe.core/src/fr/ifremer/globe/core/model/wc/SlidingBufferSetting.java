package fr.ifremer.globe.core.model.wc;

/**
 * Class holding {@link SlidingBuffer} settings
 * */
public class SlidingBufferSetting {
		public final int forwardLength;
		public final int backLength;
		public int getForwardLength() {
			return forwardLength;
		}
		public SlidingBufferSetting(int backLength,int forwardLenght) {
			super();
			this.forwardLength = forwardLenght;
			this.backLength = backLength;
		}

		public SlidingBufferSetting fromBackLength(int backLength) {
			return new SlidingBufferSetting(backLength,forwardLength);
		}
		public SlidingBufferSetting fromForwardLenght(int forwardLenght) {
			return new SlidingBufferSetting(backLength,forwardLenght);
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + backLength;
			result = prime * result + forwardLength;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SlidingBufferSetting other = (SlidingBufferSetting) obj;
			if (backLength != other.backLength)
				return false;
			if (forwardLength != other.forwardLength)
				return false;
			return true;
		}
		@Override
		public String toString() {
			return "BufferSetting [forwardLength=" + forwardLength + ", backLength=" + backLength + "]";
		}
		
		
}
