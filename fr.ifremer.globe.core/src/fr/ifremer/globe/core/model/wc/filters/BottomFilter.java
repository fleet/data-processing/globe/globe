package fr.ifremer.globe.core.model.wc.filters;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.SwathBeamSample;

public class BottomFilter implements IFilter {

	private BottomFilterParameter parameters;

	@Override
	public boolean filter(SwathBeamSample sample) {
		return !parameters.isEnable() || reallyFilter(sample);
	}

	/**
	 * Filter sample, return false if filtered, true otherwise
	 */
	private boolean reallyFilter(SwathBeamSample sample) {
		double alpha = parameters.getAngleCoefficient()
				* (1 - 1 / Math.cos(Math.toRadians(sample.data.wcAngleRefAbsoluteDeg[sample.beam])));
		if (sample.data.bottomDetectionRange[sample.beam] <= 0)
			return true;
		switch (parameters.getType()) {
		case SAMPLE:
			return sample.range <= alpha + sample.data.bottomDetectionRange[sample.beam]
					- parameters.getToleranceAbsolute();
		case RANGEPERCENT:
		default:
			return sample.range <= ((100. + alpha - parameters.getTolerancePercent()) / 100.)
					* sample.data.bottomDetectionRange[sample.beam];

		}
	}

	public BottomFilter(BottomFilterParameter param) {
		this.parameters = param;
	}

	public void setParameters(FilterParameters parameters) {
		this.parameters = parameters.bottom;
	}
}
