/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Generic model to store elements an notifying when model changed
 *
 * @param <K> type of the key of elements in the model
 * @param <T> type of the elements in the model
 * @param <E> kind of event when model changed
 */
public abstract class AbstractModelPublisher<K, T, E> {

	/** Stored elements of E (used of {@link ConcurrentHashMap} to avoid concurrency issues with values() method) */
	protected Map<K, T> elements = new ConcurrentHashMap<>();

	/** All listeners */
	protected Set<Consumer<E>> listeners = new CopyOnWriteArraySet<>();

	protected abstract K getKey(T element);

	/**
	 * Add a element in the model.
	 *
	 * @return false when element is already present in the model
	 */
	public boolean add(K key, T element) {
		boolean result = elements.putIfAbsent(key, element) == null;
		if (result) {
			newAddEvent(element).ifPresent(this::submit);
		}
		return result;
	}

	/** Add a element in the model. */
	public boolean add(T element) {
		return add(getKey(element), element);
	}

	/** Construct an event to inform listeners that a new element has been added */
	protected abstract Optional<E> newAddEvent(T element);

	/** Updates an element in the model. */
	public void update(K key, T element) {
		T previous = elements.put(key, element);
		if (previous == null) {
			newAddEvent(element).ifPresent(this::submit);
		} else {
			newUpdateEvent(element).ifPresent(this::submit);
		}
	}

	/** Updates an element in the model. */
	public void update(T element) {
		update(getKey(element), element);
	}

	/** Construct an event to inform listeners that an element has been updated */
	protected abstract Optional<E> newUpdateEvent(T element);

	/** Remove an element from the cache */
	public Optional<T> remove(K key) {
		var removedElement = Optional.ofNullable(elements.remove(key));
		removedElement.flatMap(this::newRemovedEvent).ifPresent(this::submit);
		return removedElement;
	}

	/** Removes an element */
	public Optional<T> removeElement(T element) {
		return remove(getKey(element));
	}

	/** Construct an event to inform listeners that an element has been updated */
	protected abstract Optional<E> newRemovedEvent(T element);

	/** @return true if model contains this key */
	public boolean contains(K key) {
		return elements.containsKey(key);
	}

	/** @return true if model contains this element */
	public boolean containsElement(T element) {
		return elements.containsValue(element);
	}

	/** @return the element from the model */
	public Optional<T> get(K key) {
		return Optional.ofNullable(elements.get(key));
	}

	/** Accept a new listener */
	public void addListener(Consumer<E> listener) {
		listeners.add(listener);
	}

	/** Accept a new listener */
	public void removeListener(Consumer<E> listener) {
		listeners.remove(listener);
	}

	/** Submit one event */
	public void submit(E event) {
		listeners.forEach(consumer -> consumer.accept(event));
	}

	/** @return all values */
	public List<T> getAll() {
		return new ArrayList<>(elements.values());
	}

	/** @return the first element accepted by provided {@link Predicate} **/
	public Optional<T> find(Predicate<T> predicate) {
		return getAll().stream().filter(predicate::test).findFirst();
	}

}
