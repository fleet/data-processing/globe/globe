/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.geo;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.StandardProjection;

/**
 * his class aim at creating a GeoBox from some differents arguments
 */
public class GeoBoxBuilder {

	/** Min latitude of the resulting GeoBox */
	protected double minY = 90d;
	/** Max latitude of the resulting GeoBox */
	protected double maxY = -90d;
	/** Min longidude of the resulting GeoBox centered on 0th meridian */
	protected double minX0 = 180d;
	/** Max longidude of the resulting GeoBox centered on 0th meridian */
	protected double maxX0 = -180d;
	/** Min longidude of the resulting GeoBox centered on 180th meridian */
	protected double minLongidude180 = 0d;
	/** Max longidude of the resulting GeoBox centered on 180th meridian */
	protected double maxLongidude180 = -360d;

	/** Projection of the geobox to build */
	final Projection projection;

	/**
	 * Constructor
	 */
	public GeoBoxBuilder() {
		this(new Projection(StandardProjection.LONGLAT));
	}

	/**
	 * Constructor
	 */
	public GeoBoxBuilder(Projection projection) {
		this.projection = projection;
		if (!projection.isLongLatProjection()) {
			minY = minX0 = Double.POSITIVE_INFINITY;
			maxY = maxX0 = Double.NEGATIVE_INFINITY;
		}
	}

	/** Add a point to the geobox */
	public GeoBoxBuilder addPoint(double x, double y) {
		if (!(Double.isNaN(x) || Double.isNaN(y))) {
			if (projection.isLongLatProjection()) {
				addLonLat(x, y);
			} else {
				addXY(x, y);
			}
		}
		return this;
	}

	/** Add a point to the geobox */
	protected void addLonLat(double longitude, double latitude) {
		minY = Math.min(minY, latitude);
		maxY = Math.max(maxY, latitude);

		minX0 = Math.min(minX0, longitude);
		maxX0 = Math.max(maxX0, longitude);

		minLongidude180 = Math.min(minLongidude180, longitude >= 0 ? longitude - 360d : longitude);
		maxLongidude180 = Math.max(maxLongidude180, longitude >= 0 ? longitude - 360d : longitude);
	}

	/** Add a point to the geobox */
	protected void addXY(double x, double y) {
		minY = Math.min(minY, y);
		maxY = Math.max(maxY, y);

		minX0 = Math.min(minX0, x);
		maxX0 = Math.max(maxX0, x);
	}

	/** @return the resulting geobox */
	public GeoBox build() {
		var result = new GeoBox(maxY, minY, minX0, maxX0, projection.getProjectionSettings());
		if (projection.isLongLatProjection() && Math.abs(maxX0 - minX0) > Math.abs(maxLongidude180 - minLongidude180)) {
			result.setLeft(minLongidude180 < -180d ? minLongidude180 + 360d : minLongidude180);
			result.setRight(maxLongidude180 < -180d ? maxLongidude180 + 360d : maxLongidude180);
		}
		return result;
	}

}
