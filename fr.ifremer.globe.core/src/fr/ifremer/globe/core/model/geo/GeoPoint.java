/**
 * 
 */
package fr.ifremer.globe.core.model.geo;

import java.io.Serializable;

import fr.ifremer.globe.core.model.geometry.CartesianPoint;

/**
 * Defines a geographical point with latitude/longitude (WGS84 ellsisoid).
 */
public class GeoPoint extends CartesianPoint implements Serializable {

	/** UID */
	private static final long serialVersionUID = -3294092224617283594L;

	public GeoPoint() {
		super();
	}

	/**
	 * create a new geopoint
	 * 
	 * @param longitude
	 * @param latitude
	 * */
	public GeoPoint(double lon, double lat) {
		super(lon,lat);
	}


	/**
	 * create a new geopoint
	 * 
	 * @param longitude
	 * @param latitude
	 * */
	public GeoPoint(GeoPoint p) {
		super(p.getLong(),p.getLat());
	}


	/**
	 * @BUG around -180 degree
	 * @param p_box
	 * @return
	 */
	public boolean isInside(GeoBox p_box) {
		return getLong() <= p_box.getRight() && getLong() >= p_box.getLeft() && getLat() <= p_box.getTop() && getLat() >= p_box.getBottom();
	}

	public double getLong() {
		return getX();
	}

	public void setLong(double lon) {
		setX(lon);
	}

	public double getLat() {
		return getY();
	}

	public void setLat(double lat) {
		setY(lat);
	}

	@Override
	public String toString() {
		return "GeoPoint [getLong()=" + getLong() + ", getLat()=" + getLat() + "]";
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
