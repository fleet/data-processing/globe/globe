package fr.ifremer.globe.core.model.geo;

import java.awt.geom.Line2D;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public final class GeoUtils {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(GeoUtils.class);

	public static final double CIB_CCO_MINUTE_DEGREE = 60.0;

	public static final double CIB_MPC_k_EMODNET_STEP = 0.25;

	/***
	 * scalar products used to find if a point is inside a rectangle
	 *
	 * @param latitudePoint
	 * @param longitudePoint
	 * @param latitudeOrigineRect
	 * @param longitudeOrigineRect
	 * @param latitudeRect1
	 * @param longitudeRect1
	 * @param latitudeRect2
	 * @param longitudeRect2
	 * @return
	 */
	public static boolean insideRect(double latitudePoint, double longitudePoint, double latitudeOrigineRect,
			double longitudeOrigineRect, double latitudeRect1, double longitudeRect1, double latitudeRect2,
			double longitudeRect2) {
		double scalar1 = scalarProduct(latitudeOrigineRect, longitudeOrigineRect, latitudeRect1, longitudeRect1,
				latitudePoint, longitudePoint);

		double scalarRect1 = scalarProduct(latitudeOrigineRect, longitudeOrigineRect, latitudeRect1, longitudeRect1,
				latitudeRect1, longitudeRect1);

		double scalar2 = scalarProduct(latitudeOrigineRect, longitudeOrigineRect, latitudePoint, longitudePoint,
				latitudeRect2, longitudeRect2);

		double scalarRect2 = scalarProduct(latitudeOrigineRect, longitudeOrigineRect, latitudeRect2, longitudeRect2,
				latitudeRect2, longitudeRect2);

		if (0 <= scalar1 && scalar1 <= scalarRect1 && 0 <= scalar2 && scalar2 <= scalarRect2) {
			return true;
		}
		return false;
	}

	private static double scalarProduct(double latitude1, double longitude1, double latitude2, double longitude2,
			double latitude3, double longitude3) {
		double a1, b1, a2, b2, dist1, dist2;

		a1 = latitude2 - latitude1;
		a2 = longitude2 - longitude1;

		b1 = latitude3 - latitude1;
		b2 = longitude3 - longitude1;

		dist1 = Math.sqrt(a1 * a1 + a2 * a2);
		dist2 = Math.sqrt(b1 * b1 + b2 * b2);

		if (dist1 == 0.0 || dist2 == 0.0) {
			return 0;
		}

		// cosinus = (a1*b1+a2*b2) / (dist1*dist2) ;
		return a1 * b1 + a2 * b2;
	}

	/**
	 * Test if 2 boxes overlaps.
	 *
	 * @param box1
	 * @param box2
	 * @return true if boxes overlaps
	 */
	public static boolean overlaps(GeoBox box1, GeoBox box2) {
		if (box1.getTop() < box2.getBottom()) {
			return false;
		}
		if (box2.getTop() < box1.getBottom()) {
			return false;
		}
		if (box1.getRight() < box2.getLeft()) {
			return false;
		}
		if (box2.getRight() < box1.getLeft()) {
			return false;
		}
		return true;
	}

	/**
	 * Test if 1 geoZone and 1 geoBox overlaps.
	 *
	 * @param box1
	 * @param box2
	 * @return true if boxes overlaps
	 */
	public static boolean overlaps(GeoZone zone, GeoBox box) {
		return overlaps(zone, new GeoZone(box));
	}

	/**
	 * Test if 2 zones overlaps.
	 *
	 * @param zone1
	 * @param zone2
	 * @return
	 */
	public static boolean overlaps(GeoZone zone1, GeoZone zone2) {

		boolean result = false;
		for (GeoPoint point : zone1.getPoints()) {
			if (GeoUtils.contains(zone2, point.getLat(), point.getLong())) {
				result = true;
				break;
			}
		}
		if (!result) {
			for (GeoPoint point : zone2.getPoints()) {
				if (GeoUtils.contains(zone1, point.getLat(), point.getLong())) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public static boolean intersects(GeoZone zone, GeoBox box) {
		GeoPoint NW = new GeoPoint(box.getLeft(), box.getTop());
		GeoPoint SW = new GeoPoint(box.getLeft(), box.getBottom());
		GeoPoint SE = new GeoPoint(box.getRight(), box.getBottom());
		GeoPoint NE = new GeoPoint(box.getRight(), box.getTop());
		GeoSegment northSegment = new GeoSegment(NW, NE);
		GeoSegment southSegment = new GeoSegment(SE, SW);
		GeoSegment eastSegment = new GeoSegment(NE, SE);
		GeoSegment westSegment = new GeoSegment(NW, SW);
		return intersects(zone, northSegment) || intersects(zone, southSegment) || intersects(zone, eastSegment)
				|| intersects(zone, westSegment);
	}

	public static boolean intersects(GeoZone zone, GeoSegment geoSegment) {
		boolean result = false;
		List<GeoPoint> points = zone.getPoints();
		if (points.size() > 3) {
			GeoPoint previousPoint = points.get(points.size() - 1);
			for (GeoPoint point : points) {
				if (previousPoint != null) {
					GeoSegment segment = new GeoSegment(previousPoint, point);
					result = intersects(geoSegment, segment);
				}
				if (result) {
					break;
				}
				previousPoint = point;
			}
		}
		return result;
	}

	/**
	 * Test if segment is inside box.
	 *
	 * @param box
	 * @param segment
	 * @return
	 */
	public static boolean contains(GeoBox box, GeoSegment segment) {
		return intersects(box, segment) || contains(box, segment.getPoint1()) || contains(box, segment.getPoint2());
	}

	/**
	 * Test if segment is inside zone.
	 *
	 * @param zone
	 * @param segment
	 * @return
	 */
	public static boolean contains(GeoZone zone, GeoSegment segment) {
		return intersects(zone, segment) || contains(zone, segment.getPoint1()) || contains(zone, segment.getPoint2());
	}

	/**
	 * Test if segment intersects box.
	 *
	 * @param box
	 * @param segment
	 * @return
	 */
	public static boolean intersects(GeoBox box, GeoSegment segment) {
		GeoPoint point1 = segment.getPoint1();
		GeoPoint point2 = segment.getPoint2();
		// test SW-NW segment
		boolean linesIntersect = Line2D.linesIntersect(point1.getLong(), point1.getLat(), point2.getLong(),
				point2.getLat(), box.getLeft(), box.getBottom(), box.getLeft(), box.getTop());
		if (!linesIntersect) {
			// test NW-NE segment
			linesIntersect = Line2D.linesIntersect(point1.getLong(), point1.getLat(), point2.getLong(), point2.getLat(),
					box.getLeft(), box.getTop(), box.getRight(), box.getTop());
		}
		if (!linesIntersect) {
			// test NE-SE segment
			linesIntersect = Line2D.linesIntersect(point1.getLong(), point1.getLat(), point2.getLong(), point2.getLat(),
					box.getRight(), box.getTop(), box.getRight(), box.getBottom());
		}
		if (!linesIntersect) {
			// test SE-SW segment
			linesIntersect = Line2D.linesIntersect(point1.getLong(), point1.getLat(), point2.getLong(), point2.getLat(),
					box.getRight(), box.getBottom(), box.getLeft(), box.getBottom());
		}
		return linesIntersect;
	}

	public static boolean intersects(GeoSegment segment1, GeoSegment segment2) {
		GeoPoint point11 = segment1.getPoint1();
		GeoPoint point12 = segment1.getPoint2();
		GeoPoint point22 = segment2.getPoint2();
		GeoPoint point21 = segment2.getPoint1();
		return Line2D.linesIntersect(point11.getLong(), point11.getLat(), point12.getLong(), point12.getLat(),
				point21.getLong(), point21.getLat(), point22.getLong(), point22.getLat());
	}

	/**
	 * Test if point is inside box.
	 *
	 * @param box
	 * @param point
	 * @return
	 */
	public static boolean contains(GeoBox box, GeoPoint point) {
		if (box.getTop() < point.getLat()) {
			return false;
		}
		if (box.getBottom() > point.getLat()) {
			return false;
		}
		if (box.getRight() < point.getLong()) {
			return false;
		}
		if (box.getLeft() > point.getLong()) {
			return false;
		}
		return true;
	}

	/**
	 * Test if point is inside zone.
	 *
	 * Adapted from {@link java.awt.BasicPolygon.contains}
	 *
	 * @param zone
	 * @param lat
	 * @param lon
	 * @return true if point is inside zone
	 */
	public static boolean contains(GeoZone zone, double lat, double lon) {
		// // Uses ray tracing algorithm to decide whether point is inside
		// // selection.
		// int i, j;
		// List<GeoPoint> points = zone.getPoints();
		// int nvert = points.size();
		// boolean c = false;
		// for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		// if (((points.get(i).getLat() > lat) != (points.get(j).getLat() >
		// lat))
		// && (lon < (points.get(j).getLong() - points.get(i).getLong()) * (lat
		// - points.get(i).getLat()) / (points.get(j).getLat() -
		// points.get(i).getLat()) + points.get(i).getLong())) {
		// c = !c;
		// }
		// }
		// return c;

		List<GeoPoint> points = zone.getPoints();
		int npoints = points.size();
		int hits = 0;

		GeoPoint p = points.get(npoints - 1);
		double lastLon = p.getLong();
		double lastLat = p.getLat();
		double curLon, curLat;

		// Walk the edges of the polygon
		for (int i = 0; i < npoints; lastLon = curLon, lastLat = curLat, i++) {

			p = points.get(i);
			curLon = p.getLong();
			curLat = p.getLat();

			if (curLat == lastLat) {
				continue;
			}

			double leftLon;
			if (curLon < lastLon) {
				if (lon >= lastLon) {
					continue;
				}
				leftLon = curLon;
			} else {
				if (lon >= curLon) {
					continue;
				}
				leftLon = lastLon;
			}

			double test1, test2;
			if (curLat < lastLat) {
				if (lat < curLat || lat >= lastLat) {
					continue;
				}
				if (lon < leftLon) {
					hits++;
					continue;
				}
				test1 = lon - curLon;
				test2 = lat - curLat;
			} else {
				if (lat < lastLat || lat >= curLat) {
					continue;
				}
				if (lon < leftLon) {
					hits++;
					continue;
				}
				test1 = lon - lastLon;
				test2 = lat - lastLat;
			}

			if (test1 < test2 / (lastLat - curLat) * (lastLon - curLon)) {
				hits++;
			}
		}

		return (hits & 1) != 0;
	}

	public static boolean contains(GeoZone zone, GeoPoint point) {
		/**
		 * early rejection if the point is not in the BB, it cannot be in the GeoZone
		 */
		if (!GeoUtils.contains(zone.getBoundingBox(), point)) {
			return false;
		}
		return contains(zone, point.getLat(), point.getLong());
	}

	public static void union(GeoBox geoBox1, GeoBox geoBox2) {
		geoBox1.setTop(Math.max(geoBox1.getTop(), geoBox2.getTop()));
		geoBox1.setBottom(Math.min(geoBox1.getBottom(), geoBox2.getBottom()));
		geoBox1.setRight(Math.max(geoBox1.getRight(), geoBox2.getRight()));
		geoBox1.setLeft(Math.min(geoBox1.getLeft(), geoBox2.getLeft()));
	}

	public static GeoBox reduce(GeoBox geoBox, double geoStep) {
		double westLongitude = geoBox.getLeft() + geoStep;
		double southLatitude = geoBox.getBottom() + geoStep;
		double eastLongitude = geoBox.getRight() - geoStep;
		double northLatitude = geoBox.getTop() - geoStep;
		return new GeoBox(northLatitude, southLatitude, eastLongitude, westLongitude);
	}

	public static void extendWithPoint(GeoBox geoBox, double lon, double lat) {
		double northLatitude = geoBox.getTop();
		double southLatitude = geoBox.getBottom();
		double eastLongitude = geoBox.getRight();
		double westLongitude = geoBox.getLeft();

		geoBox.setTop(Math.max(northLatitude, lat));
		geoBox.setBottom(Math.min(southLatitude, lat));
		geoBox.setRight(Math.max(eastLongitude, lon));
		geoBox.setLeft(Math.min(westLongitude, lon));
	}

	/**
	 * Realigns the bounds so that they are an exact multiple of the given modulo <br>
	 * In case of geographic projection, modulo is expressed in arcmin <br>
	 * In case of projected Geobox, modulo is expressed in meters
	 */
	public static GeoBox realign(GeoBox geoBox, double modulo) {
		GeoBox newGeoBox = new GeoBox(geoBox.getProjection());
		newGeoBox.setTop(upperBound(geoBox.getTop(), modulo));
		newGeoBox.setBottom(lowerBound(geoBox.getBottom(), modulo));
		newGeoBox.setRight(upperBound(geoBox.getRight(), modulo));
		newGeoBox.setLeft(lowerBound(geoBox.getLeft(), modulo));
		return newGeoBox;
	}

	private static double upperBound(double value, double modulo) {
		double valMod = value / modulo;
		// Round when decimals are negligible
		return (Math.abs(valMod - (int) valMod) > 1e-4 ? Math.ceil(valMod) : Math.round(valMod)) * modulo;
	}

	private static double lowerBound(double value, double modulo) {
		double valMod = value / modulo;
		// Round when decimals are negligible
		return (Math.abs(valMod - (int) valMod) > 1e-4 ? Math.floor(valMod) : Math.round(valMod)) * modulo;
	}

	/**
	 * return a list of GeoZones described in KML file in parameter
	 *
	 * @param file
	 * @return
	 */
	public static List<GeoZone> loadGeoZonesFromKML(String file) {
		List<GeoZone> zones = new ArrayList<GeoZone>();
		GeoZone zone = null;
		File f = new File(file);
		if (f.exists()) {
			try {
				DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
				domFactory.setNamespaceAware(true);
				DocumentBuilder builder = domFactory.newDocumentBuilder();
				Document doc = builder.parse(file);
				XPath xpath = XPathFactory.newInstance().newXPath();

				NodeList nodes = (NodeList) xpath.evaluate("//*", doc, XPathConstants.NODESET);

				for (int i = 0; i < nodes.getLength(); i++) {
					if (nodes.item(i).getNodeName().compareTo("Polygon") == 0) {
						if (zone != null) {
							zones.add(zone);
						}
						zone = new GeoZone();
					}
					if (nodes.item(i).getNodeName().compareTo("coordinates") == 0) {
						String[] data = nodes.item(i).getTextContent().split(" ");
						for (String element : data) {
							String[] values = element.split(",");
							zone.addPoint(new GeoPoint(Double.valueOf(values[0]), Double.valueOf(values[1])));
						}
					}
				}
				// we add last zone
				if (zone != null) {
					zones.add(zone);
				}
			} catch (Exception e) {
				logger.warn(String.format("error loading contour file '%s'", file), e);
			}
		}
		return zones;
	}
}
