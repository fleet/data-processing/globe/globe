package fr.ifremer.globe.core.model.geo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Geographic zone defined by a list of points.
 * 
 * @author bvalliere
 */
public class GeoZone implements Serializable {

	/** UID */
	private static final long serialVersionUID = 6289301413156640174L;

	/** Geographical points delimiting this zone */
	private List<GeoPoint> points = new ArrayList<GeoPoint>();

	private GeoBox geobox;

	public List<GeoPoint> getPoints() {
		return points;
	}

	public GeoZone() {
	}

	public GeoZone(GeoBox geoBox) {
		this(geoBox.getTop(), geoBox.getBottom(), geoBox.getRight(), geoBox.getLeft());
	}

	public GeoZone(double northLatitude, double southLatitude, double eastLongitude, double westLongitude) {
		GeoPoint swPoint = new GeoPoint(westLongitude, southLatitude);
		GeoPoint sePoint = new GeoPoint(eastLongitude, southLatitude);
		GeoPoint nePoint = new GeoPoint(eastLongitude, northLatitude);
		GeoPoint nwPoint = new GeoPoint(westLongitude, northLatitude);
		addPoint(swPoint);
		addPoint(sePoint);
		addPoint(nePoint);
		addPoint(nwPoint);
		computeBB();

	}

	@Override
	public GeoZone clone() {
		GeoZone result = new GeoZone();
		for (GeoPoint point : points) {
			result.addPoint(new GeoPoint(point));
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = super.equals(obj);
		if (!result && obj instanceof GeoZone) {
			GeoZone other = (GeoZone) obj;
			if (points.size() != other.points.size()) {
				return false;
			}
			boolean noDiff = true;
			for (int i = 0; i < points.size() && noDiff; i++) {
				GeoPoint p1 = points.get(i);
				GeoPoint p2 = other.points.get(i);
				noDiff &= p1.equals(p2);
			}
			result = noDiff;
		}
		return result;
	}

	public void addPoint(GeoPoint point) {
		points.add(point);
		computeBB();
	}

	public void removeLastPoint(){
		if(points.size()>0)
		{
			points.remove(points.size()-1);
			computeBB();
		}
	}

	/***
	 * recompute the associated bounding box
	 * */
	private void computeBB()
	{
		double westLon = Double.MAX_VALUE;
		double eastLon = -Double.MAX_VALUE;
		double southLat = Double.MAX_VALUE;
		double northLat = -Double.MAX_VALUE;
		for (GeoPoint point : points) {
			westLon = Math.min(westLon, point.getLong());
			eastLon = Math.max(eastLon, point.getLong());
			southLat = Math.min(southLat, point.getLat());
			northLat = Math.max(northLat, point.getLat());
		}
		geobox=new GeoBox(northLat, southLat, eastLon, westLon);

	}
	public GeoBox getBoundingBox() {
		return geobox;
	}

	

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < points.size(); i++) {
			GeoPoint point = points.get(i);
			sb.append("p");
			sb.append(i);
			sb.append(" = ");
			sb.append(point.getLong());
			sb.append(",");
			sb.append(point.getLat());
			sb.append("\n");
		}
		return sb.toString();
	}

	public int size() {
		return points.size();
	}

	public GeoPoint get(int index) {
		return points.get(index);
	}

	/**
	 * Reset zone by clearing all points
	 */
	public void reset() {
		points.clear();
		computeBB();
	}
}
