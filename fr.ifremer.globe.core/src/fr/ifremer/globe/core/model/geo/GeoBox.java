/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.geo;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

import org.apache.commons.math3.util.Precision;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 *
 * Geographic zone defined by the top/bottom/left and right coordinates. <br>
 * <br>
 * In case of a lonlat coordinates,
 * <ul>
 * <li>top is the northiest latitude, beetween ]-90°, 90°]</li>
 * <li>bottom is the southiest latitude, beetween [-90°, 90°[</li>
 * </ul>
 * In case of a lonlat coordinates and a zone spanning the 180th meridian,
 * <ul>
 * <li>left is the westhiest longitude, beetween [0°, 180°]</li>
 * <li>right is the easthiest longitude, beetween [-180°, 0°]</li>
 * </ul>
 * In case of a lonlat coordinates and a zone not spanning the 180th meridian,
 * <ul>
 * <li>left is the westhiest longitude, beetween [-180°, 0°]</li>
 * <li>right is the easthiest longitude, beetween [0°, 180°]</li>
 * </ul>
 */
/**
 *
 */
public class GeoBox {
	/** Empty Geobox */
	public static final GeoBox EMPTY = new GeoBox();

	public static final GeoBox getEmptyGeobox() {
		return new GeoBox(Double.NaN, Double.NaN, Double.NaN, Double.NaN);
	}

	/** Upper ordinate/latitude */
	private double top;
	/** Lower ordinate/latitude */
	private double bottom;
	/** Left abscissa/longitude */
	private double left;
	/** Right abscissa/longitude */
	private double right;

	/** Projection of the coordinates */
	private final ProjectionSettings projection;

	/**
	 * Constructor of a GeoBox with a LONGLAT projection
	 */
	public GeoBox() {
		projection = StandardProjection.LONGLAT;
	}

	/**
	 * Constructor of a GeoBox with a specific projection
	 */
	public GeoBox(ProjectionSettings projection) {
		this.projection = projection == null ? StandardProjection.LONGLAT : projection;
	}

	/**
	 * Copy constructor
	 */
	public GeoBox(GeoBox original) {
		this(original != null ? original.projection : StandardProjection.LONGLAT);
		if (original != null) {
			top = original.top;
			bottom = original.bottom;
			right = original.right;
			left = original.left;
		}
	}

	/**
	 * Constructor of a GeoBox for longitudes and latitudes
	 */
	public GeoBox(double northLatitude, double southLatitude, double eastLongitude, double westLongitude) {
		this();
		top = northLatitude;
		bottom = southLatitude;
		right = eastLongitude;
		left = westLongitude;
	}

	/**
	 * Constructor
	 */
	public GeoBox(double top, double bottom, double left, double right, ProjectionSettings projection) {
		this.top = top;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
		this.projection = projection;
	}

	/**
	 * Constructor
	 *
	 * @param boundingBox NSEW coords as provided by GdalUtils.getBoundingBox
	 */
	public GeoBox(double[] boundingBox) {
		this();
		top = boundingBox[0];
		bottom = boundingBox[1];
		right = boundingBox[2];
		left = boundingBox[3];
	}

	@Override
	public GeoBox clone() {
		return new GeoBox(this);
	}

	public static GeoBox englobe(GeoBox pBox1, GeoBox pBox2) {
		if (pBox1 == null) {
			return pBox2;
		}
		if (pBox2 == null) {
			return pBox1;
		}
		GeoBox box1 = new GeoBox(pBox1);
		GeoBox box2 = new GeoBox(pBox2);

		GeoBox box = new GeoBox();
		box.setTop(Math.max(box1.top, box2.top));
		box.setBottom(Math.min(box1.bottom, box2.bottom));
		box.setRight(Math.max(box1.right, box2.right));
		box.setLeft(Math.min(box1.left, box2.left));
		return box;
	}

	/** @return the union of all GeoBox. may be null */
	public static GeoBox englobe(Collection<GeoBox> geoboxes) {
		GeoBox result = null;
		if (geoboxes != null && !geoboxes.isEmpty()) {
			result = new GeoBox(geoboxes.iterator().next());
			geoboxes.forEach(result::extend);
		}
		return result;
	}

	public boolean contains(GeoBox box) {
		boolean west = left < box.left;
		boolean south = bottom < box.bottom;
		boolean east = right > box.right;
		boolean north = top > box.top;
		return west && south && east && north;
	}

	/**
	 * Determines whether this geobox intersects another geobox's limits.
	 */
	public boolean intersects(GeoBox box) {
		if (box == null || !Objects.equals(getProjection(), box.getProjection())) {
			return false;
		}

		return !(box.right < left || box.left > right || box.top < bottom || box.bottom > top);
	}

	public boolean contains(double x, double y) {
		return x >= left && x <= right && y >= bottom && y <= top;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(top);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(bottom);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(right);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(left);
		result = prime * result + (int) (temp ^ temp >>> 32);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GeoBox other = (GeoBox) obj;
		return Double.compare(top, other.top) == 0 && Double.compare(bottom, other.bottom) == 0
				&& Double.compare(right, other.right) == 0 && Double.compare(left, other.left) == 0;

	}

	public boolean isEmpty() {
		double width = getWidth();
		double height = getHeight();
		if (width == 0 && height == 0) {
			return true;
		} else {
			return Double.isNaN(width) || Double.isNaN(height);
		}
	}

	/**
	 * return the width in meter or degrees
	 */
	public double getWidth() {
		// Simple case
		if (!getProjection().isLongLatProjection() || !isSpanning180Th()) {
			return Math.abs(right - left);
		}

		// Longitudes span 180th meridian
		if (getProjection().isLongLatProjection()) {
			return right - left + 360.0;
		}

		// Projected and spanning 180th meridian
		var mySRS = new Projection(projection);
		var meridian180th = mySRS.projectQuietly(180.0, 0d);
		var result = Math.abs(meridian180th[0] - left);
		meridian180th = mySRS.projectQuietly(-180.0, 0d);
		result += Math.abs(meridian180th[0] - right);

		return result;
	}

	/**
	 * return the height (in meter or degree) of the box
	 */
	public double getHeight() {
		return Math.abs(top - bottom);
	}

	/** @return the center on X axis. */
	public double getCenterX() {
		return left + getWidth() / 2d;
	}

	@Deprecated
	public void setLong(double lon) {
		setBottom(lon - getWidth() / 2.0);
		setTop(lon + getWidth() / 2.0);
	}

	/** @return the center on Y axis. */
	public double getCenterY() {
		return (top + bottom) / 2.0;
	}

	/** Latitude in degrees. */
	@Deprecated
	public void setLat(double lat) {
		setBottom(lat - getHeight() / 2.0);
		setTop(lat + getHeight() / 2.0);
	}

	/** Width in degrees. */
	@Deprecated
	public void setWidth(double width) {
		setLeft(getCenterX() - width / 2.0);
		setRight(getCenterX() + width / 2.0);
	}

	@Deprecated
	public void setHeight(double height) {
		setBottom(getCenterY() - height / 2.0);
		setTop(getCenterY() + height / 2.0);
	}

	@Override
	public String toString() {
		if (getProjection().isLongLatProjection()) {
			ILatLongFormatter formatter = LatLongFormater.getFormatter();
			String n = formatter.formatLatitude(top);
			String s = formatter.formatLatitude(bottom);
			String e = formatter.formatLongitude(right);
			String o = formatter.formatLongitude(left);
			return "[N = " + n + ", S = " + s + ", E = " + e + ", W = " + o + "]";
		} else {
			return "[N = " + top + ", S = " + bottom + ", E = " + right + ", W = " + left + "]";
		}
	}

	/**
	 * Extends this geo box so that it becomes the union of both
	 */
	public void extend(GeoBox other) {
		if (other != null) {
			setTop(Math.max(other.top, top));
			setBottom(Math.min(other.bottom, bottom));
			setRight(Math.max(other.right, right));
			setLeft(Math.min(other.left, left));
		}
	}

	/**
	 * Extends this geo box to the GeoPoint
	 */
	public void extend(GeoPoint point) {
		if (point != null) {
			extend(point.getLong(), point.getLat());
		}
	}

	/**
	 * Extends this geo box to the coords
	 */
	public void extend(double longitude, double latitude) {
		if (Double.isFinite(longitude) && Double.isFinite(latitude)) {
			setTop(Math.max(latitude, top));
			setBottom(Math.min(latitude, bottom));
			setRight(Math.max(longitude, right));
			setLeft(Math.min(longitude, left));
		}
	}

	/**
	 * @return the position of the center of the box
	 */
	public GeoPoint getCenter() {
		GeoPoint result = new GeoPoint((right + left) / 2d, (top + bottom) / 2d);
		if (isSpanning180Th()) {
			result.setLong(180d + result.getLong());
		}
		return result;
	}

	/**
	 * @return true when this GeoBox is spanning the 180th meridian
	 */
	public boolean isSpanning180Th() {
		return left > right;
	}

	/**
	 * Transform this GeoBox to a LonLat one
	 */
	public GeoBox asLonLat() throws ProjectionException {
		if (projection.isLongLatProjection()) {
			return this;
		}
		Projection proj = new Projection(projection);
		double[][] lonlat = new double[2][2];
		lonlat[0][0] = left;
		lonlat[0][1] = bottom;
		lonlat[1][0] = right;
		lonlat[1][1] = top;
		proj.unproject(lonlat);
		return new GeoBox(lonlat[1][1], lonlat[0][1], lonlat[1][0], lonlat[0][0]);
	}

	public GeoBox asLonLatQuietly() {
		try {
			return asLonLat();
		} catch (ProjectionException e) {
			return getEmptyGeobox();
		}
	}

	private double normalizeLongitudeForGdal(double lon) {
		double result = NumberUtils.normalizedDegreesLongitude(lon);
		// Avoid Gdal error "tolerance condition error"
		result = Math.max(result, Math.nextUp(-180.0));
		result = Math.min(result, Math.nextDown(180.0));
		return result;
	}

	private double normalizeLatitudeForGdal(double lat) {
		double result = NumberUtils.normalizedDegreesLatitude(lat);
		// Avoid Gdal error "tolerance condition error"
		result = Math.max(result, Math.nextUp(-90.0));
		result = Math.min(result, Math.nextDown(90.0));
		return result;
	}

	/**
	 * Transform this GeoBox to a LonLat one
	 */
	public GeoBox project(Projection otherProjection) throws ProjectionException {
		if (!projection.isLongLatProjection()) {
			throw new ProjectionException("Can only project a LonLat GeoBox");
		}

		var north = normalizeLatitudeForGdal(getTop());
		var south = normalizeLatitudeForGdal(getBottom());
		var west = normalizeLongitudeForGdal(getLeft());
		var east = normalizeLongitudeForGdal(getRight());

		// 4 points : NW, SW, SE and NE
		double[][] lonlat = { { west, north }, { west, south }, { east, south }, { east, north } };
		otherProjection.project(lonlat);

		return new GeoBox(
				// Top is max(y)
				Arrays.stream(lonlat).mapToDouble(point -> point[1]).max().orElseThrow(),
				// Top is min(y)
				Arrays.stream(lonlat).mapToDouble(point -> point[1]).min().orElseThrow(),
				// Left is min(x)
				Arrays.stream(lonlat).mapToDouble(point -> point[0]).min().orElseThrow(),
				// Right is max(x)
				Arrays.stream(lonlat).mapToDouble(point -> point[0]).max().orElseThrow(),
				// Projection
				otherProjection.getProjectionSettings());
	}

	public GeoBox projectQuietlyAndRound(Projection otherProjection) {
		try {
			var projectedGeoBox = project(otherProjection);
			return new GeoBox(//
					Precision.round(projectedGeoBox.getTop(), 3), //
					Precision.round(projectedGeoBox.getBottom(), 3), //
					Precision.round(projectedGeoBox.getLeft(), 3), //
					Precision.round(projectedGeoBox.getRight(), 3), //
					otherProjection.getProjectionSettings());
		} catch (ProjectionException e) {
			return getEmptyGeobox();
		}
	}

	/** @return a GeoBox as an extent expected by gdal (xmin ymin xmax ymax) */
	public double[] asGdalExtent() {
		return new double[] { //
				left - (isSpanning180Th() ? 360d : 0d), //
				bottom, right, top };

	}

	/**
	 * @return the {@link #projection}
	 */
	public ProjectionSettings getProjection() {
		return projection;
	}

	/**
	 * @return the {@link #top}
	 */
	public double getTop() {
		return top;
	}

	/**
	 * @return the {@link #bottom}
	 */
	public double getBottom() {
		return bottom;
	}

	/**
	 * @return the {@link #left}
	 */
	public double getLeft() {
		return left;
	}

	/**
	 * @return the {@link #right}
	 */
	public double getRight() {
		return right;
	}

	/**
	 * @param top the {@link #top} to set
	 */
	public void setTop(double top) {
		this.top = top;
	}

	/**
	 * @param bottom the {@link #bottom} to set
	 */
	public void setBottom(double bottom) {
		this.bottom = bottom;
	}

	/**
	 * @param left the {@link #left} to set
	 */
	public void setLeft(double left) {
		this.left = left;
	}

	/**
	 * @param right the {@link #right} to set
	 */
	public void setRight(double right) {
		this.right = right;
	}

	/** Return the top left point */
	public GeoPoint getTopLeft() {
		return new GeoPoint(getLeft(), getTop());
	}

	/** Return the bottom left point */
	public GeoPoint getBottomLeft() {
		return new GeoPoint(getLeft(), getBottom());
	}

	/** Return the top right point */
	public GeoPoint getTopRight() {
		return new GeoPoint(getRight(), getTop());
	}

	/** Return the bottom right point */
	public GeoPoint getBottomRight() {
		return new GeoPoint(getRight(), getBottom());
	}

	/** @return true if values (top, bottom, left and right) are finite **/
	public boolean isValid() {
		return Double.isFinite(top) && Double.isFinite(bottom) && Double.isFinite(left) && Double.isFinite(right);
	}

}
