package fr.ifremer.globe.core.model.geo;

import java.io.Serializable;

/**
 * Geographic segment
 * 
 * @author bvalliere
 */
public class GeoSegment implements Serializable {

	/** UID */
	private static final long serialVersionUID = 397820059419208454L;

	private GeoPoint point1;
	private GeoPoint point2;

	public GeoSegment(GeoPoint p1, GeoPoint p2) {
		point1 = p1;
		point2 = p2;
	}

	public GeoPoint getPoint1() {
		return point1;
	}

	public GeoPoint getPoint2() {
		return point2;
	}
}
