package fr.ifremer.globe.core.model.quality;

import java.util.stream.Stream;

public enum NviLegacyQualifierFlags {

	REMOVED((byte) -4),
	DOUBTFUL_UNVALIDATED((byte) -3),
	VALID_UNVALIDATED((byte) -2),
	UNVALID((byte) -1),
	MISSING((byte) 0),
	DOUBTFUL((byte) 1),
	VALID((byte) 2),
	DOUBTFUL_VALIDATED((byte) 3),
	UNVALID_VALIDATED((byte) 4),
	REMOVED_VALIDATED((byte) 5),
	INTERPOLATED((byte) 6),
	MOVED((byte) 7),
	SMOOTHED((byte) 8);

	private final byte value;

	/** Constructor **/
	NviLegacyQualifierFlags(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public static NviLegacyQualifierFlags get(byte value) {
		return Stream.of(values()).filter(type -> type.value == value).findAny()
				.orElse(NviLegacyQualifierFlags.MISSING);
	}

	/**
	 * @return {@link MeasurandQualifierFlags} from {@link NviLegacyQualifierFlags}.
	 */
	public MeasurandQualifierFlags toMeasurandQualifierFlag() {
		return switch (this) {
		case VALID, UNVALID_VALIDATED, REMOVED_VALIDATED, DOUBTFUL_VALIDATED -> MeasurandQualifierFlags.GOOD;
		case UNVALID, REMOVED, DOUBTFUL_UNVALIDATED, VALID_UNVALIDATED -> MeasurandQualifierFlags.BAD;
		case MISSING -> MeasurandQualifierFlags.MISSING;
		case DOUBTFUL -> MeasurandQualifierFlags.ID_UNCERTAIN;
		case INTERPOLATED -> MeasurandQualifierFlags.INTERPOLATED;
		case MOVED, SMOOTHED -> MeasurandQualifierFlags.CHANGED;
		};
	}

	/**
	 * @return {@link NviLegacyQualifierFlags} from {@link MeasurandQualifierFlags}.
	 */
	public static NviLegacyQualifierFlags get(MeasurandQualifierFlags measurandQualifierFlags) {
		return switch (measurandQualifierFlags) {
		case NONE, PROBABLY_GOOD, GOOD, CHANGED, NOMINAL -> VALID;
		case BAD, PROBABLY_BAD, BELOW_DETECTION, BELOW_LOQ, EXCESS -> UNVALID;
		case MISSING -> MISSING;
		case ID_UNCERTAIN -> DOUBTFUL;
		case INTERPOLATED -> INTERPOLATED;
		};
	}

}
