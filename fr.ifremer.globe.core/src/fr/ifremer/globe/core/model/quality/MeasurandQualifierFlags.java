package fr.ifremer.globe.core.model.quality;

import java.util.EnumSet;
import java.util.stream.Stream;

/**
 * 
 * SEADATANET MEASURAND QUALIFIER FLAGS
 * 
 * @see <a href="https://vocab.seadatanet.org/v_bodc_vocab_v2/search.asp?lib=L20">SeaDataNet documentation</a> (click on
 *      "search button" to see all definitions)
 * 
 */
public enum MeasurandQualifierFlags {

	/**
	 * 0 = No quality control procedures have been applied to the data value. This is the initial status for all data
	 * values entering the working archive.
	 **/
	NONE((byte) 0, "no quality control",
			"No quality control procedures have been applied to the data value. This is the initial status for all data values entering the working archive."),
	/**
	 * 1 = Good quality data value that is not part of any identified malfunction and has been verified as consistent
	 * with real phenomena during the quality control process.
	 */
	GOOD((byte) 1, "good value",
			"Good quality data value that is not part of any identified malfunction and has been verified as consistent with real phenomena during the quality control process."),
	/**
	 * 2 = Data value that is probably consistent with real phenomena but this is unconfirmed or data value forming part
	 * of a malfunction that is considered too small to affect the overall quality of the data object of which it is a
	 * part.
	 */
	PROBABLY_GOOD((byte) 2, "good value",
			"Data value that is probably consistent with real phenomena but this is unconfirmed or data value forming part of a malfunction that is considered too small to affect the overall quality of the data object of which it is a part."),
	/**
	 * 3 = Data value recognized as unusual during quality control that forms part of a feature that is probably
	 * inconsistent with real phenomena.
	 */
	PROBABLY_BAD((byte) 3, "probably_bad",
			"Data value recognized as unusual during quality control that forms part of a feature that is probably inconsistent with real phenomena."),
	/**
	 * 4 = An obviously erroneous data value.
	 */
	BAD((byte) 4, "bad", "An obviously erroneous data value."),

	/**
	 * 5 = Data value adjusted during quality control. Best practice strongly recommends that the value before the
	 * change be preserved in the data or its accompanying metadata.
	 */
	CHANGED((byte) 5, "changed value",
			"Data value adjusted during quality control. Best practice strongly recommends that the value before the change be preserved in the data or its accompanying metadata."),

	/**
	 * 6 = The level of the measured phenomenon was less than the limit of detection (LoD) for the method employed to
	 * measure it. The accompanying value is the detection limit for the technique or zero if that value is unknown.
	 */
	BELOW_DETECTION((byte) 6, "value below detection",
			"The level of the measured phenomenon was less than the limit of detection (LoD) for the method employed to measure it. The accompanying value is the detection limit for the technique or zero if that value is unknown."),
	/**
	 * 7 = The level of the measured phenomenon was too large to be quantified by the technique employed to measure it.
	 * The accompanying value is the measurement limit for the technique.
	 */
	EXCESS((byte) 7, "value in excess",
			"The level of the measured phenomenon was too large to be quantified by the technique employed to measure it. The accompanying value is the measurement limit for the technique."),
	/**
	 * 8 = This value has been derived by interpolation from other values in the data object.
	 */
	INTERPOLATED((byte) 8, "interpolated value",
			"This value has been derived by interpolation from other values in the data object."),
	/**
	 * 9 = The data value is missing. There should be no accompanying value in ODV format files. The accompanying value
	 * in SeaDataNet NetCDF data must be the absent data representation specified by the _FillValue parameter attribute
	 * and lie outside the range of data not flagged bad (4) or probably bad (3).
	 */
	MISSING((byte) 9, "missing value",
			"The data value is missing. There should be no accompanying value in ODV format files. The accompanying value in SeaDataNet NetCDF data must be the absent data representation specified by the _FillValue parameter attribute and lie outside the range of data not flagged bad (4) or probably bad (3)."),
	/**
	 * 0xA = There is uncertainty in the description of the measured phenomenon associated with the value such as
	 * chemical species or biological entity.
	 */
	ID_UNCERTAIN((byte) 0xA, "value phenomenon uncertain",
			"There is uncertainty in the description of the measured phenomenon associated with the value such as chemical species or biological entity."),
	/**
	 * 0xB = The data value is a numerical data value that was the intended or targeted value rather than the measured
	 * value (e.g. instrument target depth).
	 */
	NOMINAL((byte) 0xB, "nominal value",
			"The data value is a numerical data value that was the intended or targeted value rather than the measured value (e.g. instrument target depth)."),
	/**
	 * 0xC = The level of the measured phenomenon was less than the limit of quantification (LoQ). The accompanying
	 * value is the limit of quantification for the analytical method.
	 * 
	 * TODO : VALUE SHOULD BE "Q" in the documentation
	 */
	BELOW_LOQ((byte) 0xC, "value below limit of quantification",
			"The level of the measured phenomenon was less than the limit of quantification (LoQ). The accompanying value is the limit of quantification for the analytical method.");

	/** Properties **/
	private final byte value;
	private final String name;
	private final String description;

	/** Constructor **/
	private MeasurandQualifierFlags(byte value, String name, String description) {
		this.value = value;
		this.name = name;
		this.description = description;
	}

	public byte getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isValid() {
		return EnumSet.of(NONE, GOOD, PROBABLY_GOOD, CHANGED, INTERPOLATED).contains(this);
	}

	public static MeasurandQualifierFlags get(byte value) {
		return Stream.of(values()).filter(type -> type.value == value).findAny().orElse(MeasurandQualifierFlags.NONE);
	}

}
