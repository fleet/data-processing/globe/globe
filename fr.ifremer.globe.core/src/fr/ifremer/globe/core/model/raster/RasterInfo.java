/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.raster;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Informations available for a raster
 */
public class RasterInfo implements IInfos {

	/** Kind of raster not yet defined */
	public static final String RASTER_UNDEFINED = "Undefined";
	/** Kind of raster containing floating point numbers */
	public static final String RASTER_RAW = "Raw";
	/** Kind of raster containing elevation relative to sea level */
	public static final String RASTER_ELEVATION = "Elevation";
	/** Kind of raster containing Smoothed elevation relative to sea level, computing with elevation variable */
	public static final String RASTER_ELEVATION_SMOOTHED = "Elevation Smoothed";
	/** Kind of raster containing min elevation value over a cell, relative to Lowest Astronomical Tide */
	public static final String RASTER_ELEVATION_MIN = "Elevation Min";
	/** Kind of raster containing max elevation value over a cell, relative to Lowest Astronomical Tide */
	public static final String RASTER_ELEVATION_MAX = "Elevation Max";
	/** Kind of raster containing Standard Deviation of elevation data over cell */
	public static final String RASTER_STDEV = "STDEV";
	/** Kind of raster containing the number of values used to calculate the resulting elevation over the cell */
	public static final String RASTER_VALUE_COUNT = "Value count";
	/**
	 * Kind of layer containing the number of values matching the cell coordinates but not used in mean elevation
	 * computation, these could be invalid soundings or rejected with filters
	 */
	public static final String RASTER_FILTERED_COUNT = "Filtered sounding";
	/**
	 * Kind of raster containing the indicator of cell processed as extrapolation of the neighbouring cells (absence of
	 * real soundings data)
	 */
	public static final String RASTER_INTERPOLATION_FLAG = "Interpolation Flag";
	/** Kind of raster containing the CDI index of this cell. */
	public static final String RASTER_CDI_INDEX = "CDI Index";
	/** Kind of raster containing backscatter value over a cell */
	public static final String RASTER_BACKSCATTER = "Backscatter";
	public static final String RASTER_MIN_ACROSS_DISTANCE = "Min across distance";
	public static final String RASTER_MAX_ACROSS_DISTANCE = "Max across distance";
	public static final String RASTER_MAX_ACROSS_ANGLE = "Max across angle";

	/** Kind of raster containing a bitfield. See {@link } */
	public static final String RASTER_BITFIELD = "Bitfield";

	/** Size of histogram */
	public static final int HISTOGRAM_SIZE = 20;

	/** Kind of data. May be equals to one RASTER_* constant */
	protected String dataType = RASTER_UNDEFINED;

	/** File type. May be UNDEFINED if this Raster was produced by an editor (ie Swath) */
	protected ContentType contentType = ContentType.UNDEFINED;
	/** File path. */
	protected String filePath;

	/** Raster X Size */
	protected int xSize;
	/** Raster Y Size */
	protected int ySize;
	/** Projection of this raster */
	protected Projection projection;
	/** GeoBox */
	protected GeoBox geoBox;
	/** Min and max values contained in the raster */
	protected DoubleRange minMaxValues;
	/** Mean of values contained in the raster */
	protected double mean = Double.NaN;
	/** Stdev of values contained in the raster */
	protected double stdev = Double.NaN;
	/** Histogram */
	protected IntList histogram = new ArrayIntList(0);
	/** Unit of the min and max values */
	protected String unit;
	/** Missing value */
	protected double noDataValue = Double.NaN;

	/** Gdal "URL" used with a gdal.open() to get directly a gdal.Dataset on this raster */
	protected String gdalLoadingKey;
	/** Index of the gdal.Band of this raster */
	protected int gdalBand = 1;

	/** Instance of IRasterInfoSupplier which created this RasterInfo */
	protected IRasterInfoSupplier supplier;

	/** True when all fields are set */
	protected boolean fullyLoaded = false;

	/** Default palette of color */
	protected Optional<AColorPalette> palette = Optional.empty();

	/**
	 * Constructor
	 */
	public RasterInfo(IRasterInfoSupplier supplier, String filePath) {
		this.supplier = supplier;
		this.filePath = filePath;
	}

	/** {@inheritDoc} */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new LinkedList<>();

		if (getMinMaxValues() != null) {
			result.add(Property.build("Min", NumberUtils.getStringMetersDouble(getMinMaxValues().getMinimumDouble())));
			result.add(Property.build("Max", NumberUtils.getStringMetersDouble(getMinMaxValues().getMaximumDouble())));
		}
		if (Double.isFinite(getMean())) {
			result.add(Property.build("Mean", NumberUtils.getStringMetersDouble(getMean())));
		}
		if (Double.isFinite(getStdev())) {
			result.add(Property.build("Stdev", NumberUtils.getStringMetersDouble(getStdev())));
		}
		return result;
	}

	/**
	 * @return the {@link #dataType}
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * @param dataType the {@link #dataType} to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the {@link #projection}
	 */
	public Projection getProjection() {
		return projection;
	}

	/**
	 * @param projection the {@link #projection} to set
	 */
	public void setProjection(Projection projection) {
		this.projection = projection;
	}

	/**
	 * @return the {@link #gdalLoadingKey}
	 */
	public String getGdalLoadingKey() {
		return gdalLoadingKey;
	}

	/**
	 * @param gdalLoadingKey the {@link #gdalLoadingKey} to set
	 */
	public void setGdalLoadingKey(String gdalLoadingKey) {
		this.gdalLoadingKey = gdalLoadingKey;
	}

	/**
	 * @return the {@link #supplier}
	 */
	public IRasterInfoSupplier getSupplier() {
		return supplier;
	}

	/**
	 * @return the {@link #xSize}
	 */
	public int getXSize() {
		return xSize;
	}

	/**
	 * @param xSize the {@link #xSize} to set
	 */
	public void setXSize(int xSize) {
		this.xSize = xSize;
	}

	/**
	 * @return the {@link #ySize}
	 */
	public int getYSize() {
		return ySize;
	}

	/**
	 * @param ySize the {@link #ySize} to set
	 */
	public void setYSize(int ySize) {
		this.ySize = ySize;
	}

	/**
	 * @return the {@link #geoBox}
	 */
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/**
	 * @param geoBox the {@link #geoBox} to set
	 */
	public void setGeoBox(GeoBox geoBox) {
		this.geoBox = geoBox;
	}

	/**
	 * @return the {@link #minMaxValues}
	 */
	public DoubleRange getMinMaxValues() {
		return minMaxValues;
	}

	/**
	 * @param minMaxValue the {@link #minMaxValues} to set
	 */
	public void setMinMaxValues(DoubleRange minMaxValues) {
		this.minMaxValues = minMaxValues;
	}

	/**
	 * @return the {@link #noDataValue}
	 */
	public double getNoDataValue() {
		return noDataValue;
	}

	/**
	 * @param noDataValue the {@link #noDataValue} to set
	 */
	public void setNoDataValue(double noDataValue) {
		this.noDataValue = noDataValue;
	}

	/**
	 * @return the {@link #fullyLoaded}
	 */
	public boolean isFullyLoaded() {
		return fullyLoaded;
	}

	/**
	 * @param fullyLoaded the {@link #fullyLoaded} to set
	 */
	public void setFullyLoaded(boolean fullyLoaded) {
		this.fullyLoaded = fullyLoaded;
	}

	/**
	 * @return the {@link #unit}
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit the {@link #unit} to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return the {@link #contentType}
	 */
	public ContentType getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the {@link #contentType} to set
	 */
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the {@link #gdalBand}
	 */
	public int getGdalBand() {
		return gdalBand;
	}

	/**
	 * @param gdalBand the {@link #gdalBand} to set
	 */
	public void setGdalBand(int gdalBand) {
		this.gdalBand = gdalBand;
	}

	/**
	 * @return the {@link #filePath}
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @return the {@link #palette}
	 */
	public Optional<AColorPalette> getPalette() {
		return palette;
	}

	/**
	 * @param palette the {@link #palette} to set
	 */
	public void setPalette(Optional<AColorPalette> palette) {
		this.palette = palette;
	}

	/**
	 * @return the {@link #mean}
	 */
	public double getMean() {
		return mean;
	}

	/**
	 * @param mean the {@link #mean} to set
	 */
	public void setMean(double mean) {
		this.mean = mean;
	}

	/**
	 * @return the {@link #stdev}
	 */
	public double getStdev() {
		return stdev;
	}

	/**
	 * @param stdev the {@link #stdev} to set
	 */
	public void setStdev(double stdev) {
		this.stdev = stdev;
	}

	/**
	 * @return the {@link #histogram}
	 */
	public IntList getHistogram() {
		return histogram;
	}

	/**
	 * @param histogram the {@link #histogram} to set
	 */
	public void setHistogram(IntList histogram) {
		this.histogram = histogram;
	}

}
