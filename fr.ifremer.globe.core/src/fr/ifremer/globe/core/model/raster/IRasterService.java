/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.raster;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Raster services
 */
public interface IRasterService {

	/**
	 * Returns the service implementation of IRasterService.
	 */
	static IRasterService grab() {
		return OsgiUtils.getService(IRasterService.class);
	}

	/**
	 * Supply some IRasterInfo on the specified raster file.
	 * 
	 * @return the list of IRasterInfo. Empty list if none
	 */
	List<RasterInfo> getRasterInfos(String filepath);

	/**
	 * Supply some IRasterInfo on the specified IInfoStore.
	 * 
	 * @return the list of IRasterInfo. Empty list if none
	 */
	List<RasterInfo> getRasterInfos(IFileInfo fileInfo);
	
	/**
	 * Supply some IRasterInfo containing elevation data associated to the given raster.
	 * 
	 * @return the elevation RasterInfo if exists
	 */	
	Optional<RasterInfo> getElevationRaster(RasterInfo otherRasterInfo);

	/** @return the instance able to read the specified file */
	Optional<IRasterReader> getRasterReader(RasterInfo rasterInfo);
}
