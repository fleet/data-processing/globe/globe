/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.raster;

import java.util.Optional;

/**
 * Factory of IRasterReader provided by bundles
 */
public interface IRasterReaderFactory {

	/** @return the instance able to read the specified raster */
	Optional<IRasterReader> createRasterReader(RasterInfo rasterInfo);
}
