/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.raster;

import java.io.Closeable;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Service used to read values contained in a raster.
 */
public interface IRasterReader extends Closeable {

	/** Open the file */
	void open(String filePath) throws GIOException;

	/** Get a value from raster */
	double getValue(int row, int col) throws GIOException;

	/**
	 * @return the GeoBox computed from file metadata
	 */
	GeoBox getGeoBox();

	/**
	 * @return the Projection computed from file metadata
	 */
	Projection getProjection() throws ProjectionException;

	/** Getter of {@link #noDataValue} */
	double getNoDataValue();

	/** Getter of min and max values */
	DoubleRange getMinMaxValues();

	/** @return the width of the raster */
	int getRasterXSize();

	/** @return the height of the raster */
	int getRasterYSize();

}
