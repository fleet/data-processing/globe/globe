/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.raster.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.IRasterReader;
import fr.ifremer.globe.core.model.raster.IRasterReaderFactory;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.swath.LayerType;
//import fr.ifremer.globe.editor.swath.model.LayerType;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Some service for managing raster files.
 */
@Component(name = "globe_model_service_raster", service = IRasterService.class)
public class RasterReaderRegistry implements IRasterService {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(RasterReaderRegistry.class);

	/** All registered factories */
	protected List<IRasterInfoSupplier> rasterInfoSuppliers = new ArrayList<>();
	/** All registered factories */
	protected List<IRasterReaderFactory> rasterReaderFactories = new ArrayList<>();

	/**
	 * @see fr.ifremer.globe.model.services.raster.IRasterService#getRasterReader(fr.ifremer.globe.model.services.raster.IRasterInfo)
	 */
	@Override
	public Optional<IRasterReader> getRasterReader(RasterInfo rasterInfo) {
		Optional<IRasterReader> result = Optional.empty();
		for (IRasterReaderFactory rasterReaderFactory : rasterReaderFactories) {
			result = rasterReaderFactory.createRasterReader(rasterInfo);
			if (result.isPresent()) {
				break;
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RasterInfo> getRasterInfos(String filepath) {
		List<RasterInfo> result = new ArrayList<>();
		for (IRasterInfoSupplier rasterInfoSupplier : rasterInfoSuppliers) {
			try {
				result = rasterInfoSupplier.supplyRasterInfos(filepath);
			} catch (GIOException e) {
				// Unable to parse the file
			}
			if (result != null && !result.isEmpty()) {
				break;
			}
		}
		return result != null ? result : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RasterInfo> getRasterInfos(IFileInfo fileInfo) {
		for (IRasterInfoSupplier rasterInfoSupplier : rasterInfoSuppliers) {
			List<RasterInfo> result = rasterInfoSupplier.supplyRasterInfos(fileInfo);
			if (result != null && !result.isEmpty()) {
				return result;
			}
		}
		return Collections.emptyList();
	}

	/** Accept a new IRasterReaderFactory provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addRasterInfoSupplier(IRasterInfoSupplier rasterInfoSupplier) {
		logger.debug("{} registered : {}", IFileInfoSupplier.class.getSimpleName(),
				rasterInfoSupplier.getClass().getName());

		// Is it the ultimate Gdal IRasterInfoSupplier ?
		if (rasterInfoSupplier.getContentTypes().contains(ContentType.RASTER_GDAL)) {
			// This is the default supplier.
			// Put it at the end of the list to let the priority of more specific supplier
			rasterInfoSuppliers.add(rasterInfoSupplier);
		} else {
			rasterInfoSuppliers.add(0, rasterInfoSupplier);
		}
	}

	/** Accept a new IRasterReaderFactory provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addRasterReaderFactory(IRasterReaderFactory rasterReaderFactory) {
		rasterReaderFactories.add(rasterReaderFactory);
	}

	@Override
	public Optional<RasterInfo> getElevationRaster(RasterInfo otherRasterInfo) {
		// retrieve elevation raster if exists (handle SWATH_EDITOR_x layers)
		String layertype = LayerType.fromLabel(otherRasterInfo.getDataType()).orElse(LayerType.ELEVATION).toString();
		String filepath = FilenameUtils.getFullPath(otherRasterInfo.getFilePath());
		String filename = FilenameUtils.getName(otherRasterInfo.getFilePath());
		String elevationFilename = filename.replace(layertype, LayerType.ELEVATION.toString());
		if (!filename.equals(elevationFilename)) {
			String elevationFilepath = FilenameUtils.concat(filepath, elevationFilename);
			return getRasterInfos(elevationFilepath).stream().findFirst();
		}
		return Optional.empty();
	}

}
