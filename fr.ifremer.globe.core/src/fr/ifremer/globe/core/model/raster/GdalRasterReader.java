/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.raster;

import java.util.Arrays;
import java.util.function.IntUnaryOperator;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.osr.SpatialReference;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Class used to browse values of a raster file using gdal capabilities
 */
public class GdalRasterReader implements IRasterReader {

	/** Dataset from input file */
	protected Dataset dataset;
	/** Transformation matrix */
	protected double[] geoTransform;
	/** GeoBox covering the file */
	protected GeoBox geoBox;

	/** Band data from input file */
	protected Band band;
	/** Read row from band */
	protected int currentRowIndex = -1;
	/** Current row data from band */
	protected double[] currentRowValues;
	/** Current row data from band */
	protected double noDataValue;

	/** Min and max depth from input file */
	protected DoubleRange minMaxValues;
	/** Value used to replace the noDataValue contained in the input file */
	protected double missingValue = Double.NaN;

	/** Getter of x index for a specified column */
	protected IntUnaryOperator xGetter = column -> column;
	/** Getter of y index for a specified row */
	protected IntUnaryOperator yGetter = row -> row;

	/**
	 * Open the file and prepare the reading of the first band
	 *
	 * @throws GIOException when IO error occurs
	 */
	@Override
	public void open(String filePath) throws GIOException {
		close();
		dataset = gdal.Open(filePath);
		if (dataset == null) {
			throw new GIOException("Unable to open the file " + FilenameUtils.getName(filePath));
		}

		// Projection
		String projWkt = dataset.GetProjectionRef();
		if (projWkt == null || projWkt.isEmpty()) {
			projWkt = dataset.GetProjection();
		}
		SpatialReference srs = GdalOsrUtils.SRS_WGS84;
		if (projWkt != null && !projWkt.isEmpty()) {
			srs = new SpatialReference(projWkt);
		}
		// BoundingBox
		double[] boundingBox = GdalUtils.getBoundingBox(dataset);
		GdalOsrUtils.tranformBoundingBoxToWGS84(srs, boundingBox);
		geoBox = new GeoBox(boundingBox);
		srs.delete();

		// Adapt grid indexes to geoTransform
		geoTransform = dataset.GetGeoTransform();
		if (geoTransform[1] < 0d) {
			xGetter = column -> dataset.getRasterXSize() - column - 1;
		}
		if (geoTransform[5] < 0d) {
			yGetter = row -> dataset.getRasterYSize() - row - 1;
		}
		currentRowValues = new double[dataset.getRasterXSize()];

		prepare(1);
	}

	/**
	 * Prepare the reading of a band
	 *
	 * @return true preparation done
	 */
	public void prepare(int bandIndex) throws GIOException {
		if (band != null) {
			band.delete();
			band = null;
		}

		if (dataset != null && bandIndex >= 1 && dataset.getRasterCount() >= bandIndex) {
			band = dataset.GetRasterBand(bandIndex);
			if (band != null) {
				minMaxValues = GdalUtils.getMinMaxValues(band, true);
				noDataValue = GdalUtils.getNoDataValue(band);
			}
		}

		if (band == null) {
			throw new GIOException("Bad file format");
		}
	}

	/** {@inheritDoc} */
	@Override
	public void close() {
		if (dataset != null) {
			dataset.delete();
			dataset = null;
		}
		if (band != null) {
			band.delete();
			band = null;
		}
	}

	/**
	 * @return the GeoBox computed from tiff metadata
	 */
	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/**
	 * @return the Projection computed from tiff metadata
	 */
	@Override
	public Projection getProjection() throws ProjectionException {
		Projection result = null;
		String proj4String = dataset.GetProjectionRef();
		if (proj4String != null && !proj4String.isEmpty()) {
			SpatialReference spatialReference = new SpatialReference(proj4String);
			result = Projection.createFromProj4String(spatialReference.ExportToProj4());
		} else {
			result = new Projection(StandardProjection.LONGLAT);
		}

		result.setProjectedGeoBox(getGeoBox());
		return result;
	}

	/** Get a value from raster */
	@Override
	public double getValue(int row, int col) {
		if (currentRowIndex != row) {
			if (band.ReadRaster(0, yGetter.applyAsInt(row), currentRowValues.length, 1,
					currentRowValues) == GdalUtils.OK) {
				currentRowIndex = row;
			} else {
				Arrays.fill(currentRowValues, noDataValue);
			}
		}

		double result = currentRowValues[xGetter.applyAsInt(col)];
		return Double.isNaN(result) || result == noDataValue ? getMissingValue() : result;
	}

	/** Getter of {@link #dataset} */
	public Dataset getDataset() {
		return dataset;
	}

	/** Getter of {@link #geoTransform} */
	public double[] getGeoTransform() {
		return geoTransform;
	}

	/** Getter of {@link #band} */
	public Band getBand() {
		return band;
	}

	/** Getter of {@link #noDataValue} */
	@Override
	public double getNoDataValue() {
		return noDataValue;
	}

	/** Getter of {@link #minMaxValues} */
	@Override
	public DoubleRange getMinMaxValues() {
		return minMaxValues;
	}

	/** Getter of {@link #missingValue} */
	public double getMissingValue() {
		return missingValue;
	}

	/** {@inheritDoc} */
	public int GetLayerCount() {
		return dataset.GetLayerCount();
	}

	/** {@inheritDoc} */
	@Override
	public int getRasterXSize() {
		return dataset.getRasterXSize();
	}

	/** {@inheritDoc} */
	@Override
	public int getRasterYSize() {
		return dataset.getRasterYSize();
	}

	/** Setter of {@link #missingValue} */
	public void setMissingValue(double missingValue) {
		this.missingValue = missingValue;
	}

}
