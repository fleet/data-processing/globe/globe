/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.raster;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Service able to produce some IRasterInfo on a specific kind of raster file
 */
public interface IRasterInfoSupplier {

	/** @return the list of managed file type */
	default Set<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.UNDEFINED);
	}

	/**
	 * Supply some IRasterInfo on the specified raster file.
	 *
	 * @return the list of IRasterInfo. Empty list if none
	 */
	List<RasterInfo> supplyRasterInfos(String filepath) throws GIOException;

	/**
	 * Supply some IRasterInfo on the specified IInfoStore.
	 *
	 * @return the list of IRasterInfo. Empty list if none
	 */
	List<RasterInfo> supplyRasterInfos(IFileInfo fileInfo);
}
