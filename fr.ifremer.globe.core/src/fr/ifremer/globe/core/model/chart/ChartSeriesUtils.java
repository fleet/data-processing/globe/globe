package fr.ifremer.globe.core.model.chart;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math3.exception.OutOfRangeException;

import com.opencsv.CSVWriter;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility methods for {@link IChartSeries}.
 */
public class ChartSeriesUtils {

	/** Static class, no constructor **/
	private ChartSeriesUtils() {
		// nothing
	}

	/**
	 * @return a {@link PolynomialSplineFunction} from this series (allows linear
	 *         interpolation).
	 */
	public static PolynomialSplineFunction getInterpolationFunction(IChartSeries<? extends IChartData> series) {
		var linearInterpolator = new LinearInterpolator();
		var xyArrays = series.getValidXYArrays();
		return linearInterpolator.interpolate(xyArrays[0], xyArrays[1]);
	}

	/**
	 * For each data, if it's possible adds the value of the other series (found by
	 * linear interpolation).
	 */
	public static IChartSeries<? extends IChartData> add(IChartSeries<? extends IChartData> inputSeries,
			IChartSeries<? extends IChartData> otherSeries) {
		PolynomialSplineFunction linearInterpolator = getInterpolationFunction(otherSeries);
		inputSeries.forEach(currentData -> {
			try {
				currentData.setY(currentData.getY() + linearInterpolator.value(currentData.getX()));
			} catch (OutOfRangeException e) {
				// value not outside other series range are ignored
			}
		});
		return inputSeries;
	}

	/**
	 * For each data, if it's possible subtracts the value of the other series
	 * (found by linear interpolation).
	 */
	public static IChartSeries<? extends IChartData> minus(IChartSeries<? extends IChartData> inputSeries,
			IChartSeries<? extends IChartData> otherSeries) {
		PolynomialSplineFunction linearInterpolator = getInterpolationFunction(otherSeries);
		inputSeries.forEach(currentData -> {
			try {
				currentData.setY(currentData.getY() - linearInterpolator.value(currentData.getX()));
			} catch (OutOfRangeException e) {
				// value not outside other series range are ignored
			}
		});
		return inputSeries;
	}

	/**
	 * Applies an offset on each data.
	 */
	public static IChartSeries<? extends IChartData> applyOffset(IChartSeries<? extends IChartData> inputSeries,
			double value) {
		inputSeries.forEach(currentData -> currentData.setY(currentData.getY() + value));
		return inputSeries;
	}

	/**
	 * Saves the {@link IChartSeries} to a CSV file (overwrite if exists).
	 * 
	 * @param inputSeries    : {@link IChartSeries} to save.
	 * @param outputFilePath : destination file path.
	 * @throws GIOException
	 */
	public static void saveSeriesToCsv(IChartSeries<? extends IChartData> inputSeries, String outputFilePath)
			throws GIOException {
		try (Writer writer = new FileWriter(outputFilePath);
				CSVWriter csvWriter = new CSVWriter(writer, ';', CSVWriter.DEFAULT_QUOTE_CHARACTER,
						CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);) {
			// headers
			csvWriter.writeNext(new String[] { "Index", "x", "y", "Description" });
			// data
			inputSeries.forEach(chartData -> {
				csvWriter.writeNext(new String[] { //
						Integer.toString(chartData.getIndex()), //
						Double.toString(chartData.getX()), //
						Double.toString(chartData.getY()), //
						chartData.getTooltip() }, false);
			});
		} catch (IOException e) {
			throw new GIOException("Exception wile saving series '" + inputSeries.getTitle() + "' : " + e.getMessage(),
					e);
		}
	}

	/**
	 * Saves a list of {@link IChartSeries} to a CSV file (overwrite if exists).
	 * 
	 * @param inputSeries    : list {@link IChartSeries} to save. They should be
	 *                       with the same size and x values.
	 * @param outputFilePath : destination file path.
	 * @throws GIOException
	 */
	public static void saveSeriesToCsv(List<IChartSeries<? extends IChartData>> inputSeriesList, String outputFilePath)
			throws GIOException {

		if (inputSeriesList.stream().map(IChartSeries::getSource).distinct().count() > 1)
			throw new GIOException("Input series must have the same source.");

		var xInstant = Instant.ofEpochMilli((long) inputSeriesList.get(0).getData(0).getX());
		boolean xIsTime = xInstant.isAfter(Instant.EPOCH.plus(Duration.ofDays(1))) && xInstant.isBefore(Instant.now());
		boolean hasSectionId = inputSeriesList.get(0).getData(0).getSectionId().isPresent();

		try (Writer writer = new FileWriter(outputFilePath);
				CSVWriter csvWriter = new CSVWriter(writer, ';', CSVWriter.DEFAULT_QUOTE_CHARACTER,
						CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);) {
			// headers
			var headers = new ArrayList<String>(List.of("Index", xIsTime ? "Time" : "x"));
			inputSeriesList.forEach(series -> {
				headers.add(series.getTitle());
				if (hasSectionId)
					headers.add("Section");
			});
			csvWriter.writeNext(headers.toArray(String[]::new));

			// data
			for (int i = 0; i < inputSeriesList.get(0).size(); i++) {
				var firstChartData = inputSeriesList.get(0).getData(i);
				var row = new ArrayList<String>(List.of(Integer.toString(i),
						xIsTime ? Instant.ofEpochMilli((long) firstChartData.getX()).toString()
								: Double.toString(firstChartData.getX())));
				var index = i; // to get final
				boolean hasValidData = inputSeriesList.stream().anyMatch(series -> series.getData(index).isEnabled());
				if (hasValidData) {
					inputSeriesList.forEach(series -> {
						row.add(Double.toString(series.getData(index).getY()));
						series.getData(index).getSectionId().ifPresent(row::add);
					});
					csvWriter.writeNext(row.toArray(String[]::new), false);
				}
			}

		} catch (IOException e) {
			throw new GIOException("Exception wile saving series : " + e.getMessage(), e);
		}
	}

}
