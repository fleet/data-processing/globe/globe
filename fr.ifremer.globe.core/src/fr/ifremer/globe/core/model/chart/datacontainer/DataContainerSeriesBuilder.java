package fr.ifremer.globe.core.model.chart.datacontainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.LoadableChartSeries;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.Group;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiConsumerWithException;
import fr.ifremer.globe.utils.function.BiIntFunctionWithException;
import fr.ifremer.globe.utils.function.FunctionWithException;
import fr.ifremer.globe.utils.function.IntFunctionWithException;
import fr.ifremer.globe.utils.function.TriConsumerWithException;

/**
 * This class provides method to get {@link IChartSeries} from {@link DataContainer}.
 */
public class DataContainerSeriesBuilder implements IAbstractBaseLayerOperation {

	/** {@link IChartSeries} list **/
	private List<LoadableChartSeries> series;

	/** Parameters to build series **/
	private final DataContainer<?> dataContainer;
	private final Predicate<IBaseLayer> layerFilter;
	private final FunctionWithException<Integer, Long, GIOException> indexToTime;

	// Optional list of valid indexes (if some layer data should be ignored)
	private final Optional<List<Integer>> validIndexes;

	private Group currentGroup;

	private DataContainerSeriesSource seriesSource;

	/** Constructor with a list of valid indexes **/
	public DataContainerSeriesBuilder(IDataContainerToken<?> dataContainerToken,
			FunctionWithException<Integer, Long, GIOException> indexToTime, Predicate<IBaseLayer> layerPredicate,
			Optional<List<Integer>> validIndexes) {
		this.dataContainer = dataContainerToken.getDataContainer();
		this.indexToTime = indexToTime;
		this.layerFilter = layerPredicate;
		this.validIndexes = validIndexes;
		this.seriesSource = new DataContainerSeriesSource(dataContainerToken);
	}

	/**
	 * Runs through layers of the provided {@link DataContainer} to create series.
	 */
	public List<LoadableChartSeries> getSeries() throws GIOException {
		if (series == null) {
			series = new ArrayList<>();
			for (var group : dataContainer.getGroups()) {
				currentGroup = group;
				for (var layer : group.getLayers().values())
					if (layerFilter.test(layer))
						layer.process(this);
			}
		}
		return series;
	}

	/**
	 * @return If valid indexes list has been set, return the index in layer.
	 */
	private int seriesToLayerIdx(int seriesIndex) {
		return validIndexes.isPresent() ? validIndexes.get().get(seriesIndex) : seriesIndex;
	}

	/**
	 * @return series title for a specified layer.
	 */
	private String getSeriesTitle(AbstractBaseLayer<?> layer) {
		return (currentGroup != null && !currentGroup.getName().equals("/") ? currentGroup.getName() + "/" : "")
				+ layer.getName() + " [" + layer.getUnit() + "]";
	}

	/**
	 * Creates series from a 1D Layer.
	 */
	private void addSeries(AbstractBaseLayer<?> layer, IntFunctionWithException<Double, GIOException> valueGetter,
			BiConsumerWithException<Integer, Double, GIOException> valueSetter) {
		var title = getSeriesTitle(layer);
		var size = validIndexes.map(List::size).orElse((int) layer.getDimensions()[0]);
		series.add(new DataContainerChartSeries1D<>(title, seriesSource, layer, size,
				i -> (double) indexToTime.apply(seriesToLayerIdx(i)), //
				i -> (double) valueGetter.apply(seriesToLayerIdx(i)), //
				(i, y) -> valueSetter.accept(seriesToLayerIdx(i), y)));
	}

	/**
	 * Creates series from a 2D Layer.
	 */
	private void addSeries(AbstractBaseLayer<?> layer, BiIntFunctionWithException<Double, GIOException> valueGetter,
			TriConsumerWithException<Integer, Integer, Double, GIOException> valueSetter) {
		var title = getSeriesTitle(layer);
		var size = validIndexes.map(List::size).orElse((int) layer.getDimensions()[0]);
		series.add(new DataContainerChartSeries2D<>(title, seriesSource, layer, size, (int) layer.getDimensions()[1],
				i -> (double) indexToTime.apply(seriesToLayerIdx(i)), //
				(i1, i2) -> valueGetter.apply(seriesToLayerIdx(i1), i2), //
				(i1, i2, y) -> valueSetter.accept(seriesToLayerIdx(i1), i2, y)));
	}

	/*
	 * USE OF VISITOR DESIGN PATTERN TO BUILD SERIES
	 */

	@Override
	public void visit(BooleanLayer1D layer) throws GIOException {
		// not managed
	}

	@Override
	public void visit(ByteLayer1D layer) throws GIOException {
		addSeries(layer, i -> (double) layer.get(i), (i, v) -> layer.set(i, v.byteValue()));
	}

	@Override
	public void visit(ShortLayer1D layer) throws GIOException {
		addSeries(layer, i -> (double) layer.get(i), (i, v) -> layer.set(i, v.shortValue()));
	}

	@Override
	public void visit(IntLayer1D layer) throws GIOException {
		addSeries(layer, i -> (double) layer.get(i), (i, v) -> layer.set(i, v.intValue()));
	}

	@Override
	public void visit(LongLayer1D layer) throws GIOException {
		addSeries(layer, i -> (double) layer.get(i), (i, v) -> layer.set(i, v.longValue()));
	}

	@Override
	public void visit(FloatLayer1D layer) throws GIOException {
		addSeries(layer, i -> (double) layer.get(i), (i, v) -> layer.set(i, v.floatValue()));
	}

	@Override
	public void visit(DoubleLayer1D layer) throws GIOException {
		addSeries(layer, layer::get, layer::set);
	}

	@Override
	public void visit(BooleanLayer2D layer) throws GIOException {
		// not managed
	}

	@Override
	public void visit(ByteLayer2D layer) throws GIOException {
		addSeries(layer, (i1, i2) -> (double) layer.get(i1, i2), (i1, i2, v) -> layer.set(i1, i2, v.byteValue()));
	}

	@Override
	public void visit(ShortLayer2D layer) throws GIOException {
		addSeries(layer, (i1, i2) -> (double) layer.get(i1, i2), (i1, i2, v) -> layer.set(i1, i2, v.shortValue()));
	}

	@Override
	public void visit(IntLayer2D layer) throws GIOException {
		addSeries(layer, (i1, i2) -> (double) layer.get(i1, i2), (i1, i2, v) -> layer.set(i1, i2, v.intValue()));
	}

	@Override
	public void visit(LongLayer2D layer) throws GIOException {
		addSeries(layer, (i1, i2) -> (double) layer.get(i1, i2), (i1, i2, v) -> layer.set(i1, i2, v.longValue()));
	}

	@Override
	public void visit(FloatLayer2D layer) throws GIOException {
		addSeries(layer, (i1, i2) -> (double) layer.get(i1, i2), (i1, i2, v) -> layer.set(i1, i2, v.floatValue()));
	}

	@Override
	public void visit(DoubleLayer2D layer) throws GIOException {
		addSeries(layer, layer::get, layer::set);
	}

}
