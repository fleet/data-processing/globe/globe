package fr.ifremer.globe.core.model.chart.impl;

import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiConsumerWithException;
import fr.ifremer.globe.utils.function.IntFunctionWithException;

/**
 * {@link LoadableChartSeries} with 1 dimension.
 */
public class LoadableChartSeries1D extends LoadableChartSeries {

	private final IntFunctionWithException<Double, GIOException> xSupplier;

	private final IntFunctionWithException<Double, GIOException> ySupplier;

	private final BiConsumerWithException<Integer, Double, GIOException> ySetter;

	/**
	 * Constructor
	 */
	protected LoadableChartSeries1D(String name, IChartSeriesDataSource source, int size,
			IntFunctionWithException<Double, GIOException> xGetter,
			IntFunctionWithException<Double, GIOException> yGetter,
			BiConsumerWithException<Integer, Double, GIOException> ySetter) {
		super(name, source, size);
		this.xSupplier = xGetter;
		this.ySupplier = yGetter;
		this.ySetter = ySetter;
	}

	@Override
	public double getX(int index) throws GIOException {
		return xSupplier.apply(index);
	}

	@Override
	public double getY(int index) throws GIOException {
		return ySupplier.apply(index);
	}

	@Override
	public void setY(int index, double y) throws GIOException {
		ySetter.accept(index, y);
	}

}
