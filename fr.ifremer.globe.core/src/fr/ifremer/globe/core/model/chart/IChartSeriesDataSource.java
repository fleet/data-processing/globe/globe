package fr.ifremer.globe.core.model.chart;

import java.nio.file.Path;
import java.util.Optional;

/**
 * Interface to describe the supplier of data used to build {@link IChartSeries} (a file for example).
 */
public interface IChartSeriesDataSource {

	/** {@link IChartSeriesDataSource} to used when the data provider is unknown. **/
	static IChartSeriesDataSource UNKNOWN = () -> "UNKNOWN";

	/**
	 * @return the data source ID.
	 */
	String getId();

	/**
	 * @return an {@link Optional} with the {@link Path} of this data source if exist.
	 */
	default Optional<Path> getFilePath() {
		return Optional.empty();
	}

	/**
	 * @return an {@link Optional} with the file name of this data source if exist.
	 */
	default Optional<String> getFileName() {
		return getFilePath().map(Path::getFileName).map(Path::toString);
	}

}
