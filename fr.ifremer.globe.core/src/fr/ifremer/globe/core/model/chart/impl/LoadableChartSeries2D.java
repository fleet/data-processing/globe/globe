package fr.ifremer.globe.core.model.chart.impl;

import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiIntFunctionWithException;
import fr.ifremer.globe.utils.function.IntFunctionWithException;
import fr.ifremer.globe.utils.function.TriConsumerWithException;

/**
 * {@link LoadableChartSeries} with 2 dimensions.
 */
public abstract class LoadableChartSeries2D extends LoadableChartSeries {

	private final IntFunctionWithException<Double, GIOException> xSupplier;

	private final BiIntFunctionWithException<Double, GIOException> ySupplier;

	private final TriConsumerWithException<Integer, Integer, Double, GIOException> ySetter;

	private final int secondDimSize;

	private int secondIndex;

	/**
	 * Constructor
	 */
	protected LoadableChartSeries2D(String name, IChartSeriesDataSource source, int size, int secondDimSize,
			IntFunctionWithException<Double, GIOException> xGetter,
			BiIntFunctionWithException<Double, GIOException> yGetter,
			TriConsumerWithException<Integer, Integer, Double, GIOException> ySetter) {
		super(name, source, size);
		this.secondDimSize = secondDimSize;
		this.xSupplier = xGetter;
		this.ySupplier = yGetter;
		this.ySetter = ySetter;
	}

	public int getSecondDimSize() {
		return secondDimSize;
	}

	public int getSecondIndex() {
		return secondIndex;
	}

	public void setSecondIndex(int secondIndex) throws GIOException {
		this.secondIndex = secondIndex;
		load();
	}

	@Override
	public double getX(int index) throws GIOException {
		return xSupplier.apply(index);
	}

	@Override
	public double getY(int index) throws GIOException {
		return ySupplier.apply(index, getSecondIndex());
	}

	@Override
	public void setY(int index, double y) throws GIOException {
		ySetter.accept(index, getSecondIndex(), y);
	}

}
