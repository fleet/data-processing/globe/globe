/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.chart.services;

import java.util.List;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Chart service.
 */
public interface IChartService {

	/**
	 * @return the service implementation.
	 */
	static IChartService grab() {
		return OsgiUtils.getService(IChartService.class);
	}

	/**
	 * @return true if the {@link ContentType} is handled by this service.
	 */
	boolean isAccepted(ContentType contenType);

	/**
	 * @return a list of {@link IChartSeries} for the specified file.
	 */
	List<? extends IChartSeries<? extends IChartData>> getChartSeries(String filePath) throws GIOException;

	/**
	 * @return a list of {@link IChartSeries} for the specified {@link IFileInfo}.
	 */
	List<? extends IChartSeries<? extends IChartData>> getChartSeries(IFileInfo info) throws GIOException;

}
