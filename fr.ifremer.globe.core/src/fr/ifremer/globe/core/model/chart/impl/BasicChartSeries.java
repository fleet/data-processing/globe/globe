package fr.ifremer.globe.core.model.chart.impl;

import java.util.Optional;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.model.chart.IChartSeriesStatusHandler;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Abstract base implementation of {@link IChartSeries}.
 */
public abstract class BasicChartSeries<T extends IChartData> implements IChartSeries<T> {

	/** Properties **/
	private final String title;
	private final IChartSeriesDataSource source;
	protected GColor color = GColor.DEFAULT_BLUE;
	private boolean enabled = false;

	/** Optional status handler **/
	private Optional<IChartSeriesStatusHandler> optStatusHandler = Optional.empty();

	/** Constructor **/
	protected BasicChartSeries(String title, IChartSeriesDataSource source) {
		super();
		this.title = title;
		this.source = source;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public IChartSeriesDataSource getSource() {
		return source;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public GColor getColor() {
		return color;
	}

	@Override
	public void setColor(GColor color) {
		this.color = color;
	}

	public void setStatusHandler(IChartSeriesStatusHandler statusHandler) {
		this.optStatusHandler = Optional.of(statusHandler);
	}

	@Override
	public Optional<IChartSeriesStatusHandler> getStatusHandler() {
		return optStatusHandler;
	}

}
