package fr.ifremer.globe.core.model.chart;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * This interface defines methods which have to be implemented by a object to be displayed as a new series.
 */
public interface IChartSeries<T extends IChartData> extends Iterable<T>, Comparable<T> {

	/**
	 * @return title of the series.
	 */
	String getTitle();

	/**
	 * @return the data provider {@link IChartSeriesDataSource}.
	 */
	IChartSeriesDataSource getSource();

	/**
	 * @return count of {@link IChartData} contained in this series.
	 */
	int size();

	/**
	 * @return the {@link IChartData} at the specified index.
	 */
	T getData(int index);

	/**
	 * @return true if this series is enabled.
	 */
	boolean isEnabled();

	/**
	 * Sets the series enabled state.
	 */
	void setEnabled(boolean enabled);

	/**
	 * @return the {@link GColor} of this series.
	 */
	public GColor getColor();

	/**
	 * Sets the {@link GColor} of this series.
	 */
	void setColor(GColor color);

	/**
	 * @return an optional {@link IChartSeriesStatusHandler} which helps to load and save {@link IChartData} status.
	 */
	default Optional<IChartSeriesStatusHandler> getStatusHandler() {
		return Optional.empty();
	}

	/**
	 * Provides an iterator on each {@link IChartData} (see {@link Iterable}).
	 */
	@Override
	public default Iterator<T> iterator() {
		return new Iterator<T>() {
			private int cursor;

			@Override
			public T next() {
				if (this.hasNext()) {
					return getData(cursor++);
				}
				throw new NoSuchElementException();
			}

			@Override
			public boolean hasNext() {
				return cursor < size();
			}
		};
	}

	/**
	 * Compares Series to Data
	 * 
	 * @return data is before series : -1 , data is after series : 1, else : 0
	 */
	@Override
	default int compareTo(IChartData data) {
		if (data.compareTo(getData(0)) < 0)
			return -1;
		if (data.compareTo(getData(size() - 1)) > 0)
			return 1;
		return 0;
	}

	/**
	 * @return new {@link Stream} on {@link IChartData}.
	 */
	default Stream<T> stream() {
		return StreamSupport.stream(spliterator(), false);
	}

	/**
	 * Default method to get x and y arrays from valid {@link IChartData}.
	 */
	public default double[][] getValidXYArrays() {
		List<double[]> validData = stream()//
				.filter(IChartData::isEnabled)//
				.filter(data -> !Double.isNaN(data.getX()) && !Double.isNaN(data.getY()))//
				.map(data -> new double[] { data.getX(), data.getY() }).collect(Collectors.toList());

		double[] x = new double[validData.size()];
		double[] y = new double[validData.size()];
		for (int i = 0; i < validData.size(); i++) {
			x[i] = validData.get(i)[0];
			y[i] = validData.get(i)[1];
		}
		return new double[][] { x, y };
	}

	/**
	 * Default method to set y array to valid {@link IChartData}.
	 */
	public default void setValidYArray(double[] yArray) {
		List<Integer> validIndices = stream()//
				.filter(IChartData::isEnabled)//
				.filter(data -> !Double.isNaN(data.getX()) && !Double.isNaN(data.getY()))//
				.map(IChartData::getIndex).toList();
		for (int i = 0; i < validIndices.size(); i++) {
			getData(validIndices.get(i)).setY(yArray[i]);
		}
	}
}
