package fr.ifremer.globe.core.model.chart.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * {@link IChartSeries} with backed data, provides load and flush methods.
 */
public abstract class LoadableChartSeries extends BasicChartSeries<SimpleChartData> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoadableChartSeries.class);

	private final int size;

	protected final List<SimpleChartData> chartDataList = new ArrayList<>();

	private boolean isDirty;

	/**
	 * Constructor
	 */
	protected LoadableChartSeries(String name, IChartSeriesDataSource source, int size) {
		super(name, source);
		this.size = size;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public SimpleChartData getData(int index) {
		try {
			if (chartDataList.isEmpty())
				load();
			return chartDataList.get(index);
		} catch (GIOException e) {
			LOGGER.error("Error while loading data of series : " + getTitle(), e);
			return new SimpleChartData(this, index, index, Double.NaN);
		}
	}

	protected void load() throws GIOException {
		chartDataList.clear();
		for (int i = 0; i < size(); i++)
			chartDataList.add(new SimpleChartData(this, i, getX(i), getY(i)));
	}

	public void flush() throws GIOException {
		for (var chartData : chartDataList)
			setY(chartData.getIndex(), chartData.getY());
	}

	public boolean isDirty() {
		return isDirty;
	}

	public void setDirty(boolean isDirty) {
		this.isDirty = isDirty;
	}

	protected abstract double getX(int index) throws GIOException;

	protected abstract double getY(int index) throws GIOException;

	protected abstract void setY(int index, double y) throws GIOException;

}
