package fr.ifremer.globe.core.model.chart.impl;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.IntFunction;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.model.chart.IChartSeriesStatusHandler;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.function.BiDoubleFunction;

public class SimpleChartSeriesBuilder {

	private final String title;

	private GColor color = GColor.DEFAULT_BLUE;

	private IChartSeriesDataSource source = IChartSeriesDataSource.UNKNOWN;

	private int size = 0;

	private BiFunction<IChartSeries<SimpleChartData>, Integer, SimpleChartData> dataSupplier;

	private Optional<BiDoubleFunction<String>> optTooltipFormatter = Optional.empty();

	private Optional<IChartSeriesStatusHandler> optStatusHandler = Optional.empty();

	/**
	 * Builder constructor ask for title.
	 */
	public SimpleChartSeriesBuilder(String title) {
		super();
		this.title = title;
	}

	public SimpleChartSeriesBuilder withSource(IChartSeriesDataSource source) {
		this.source = source;
		return this;
	}

	public SimpleChartSeriesBuilder withColor(GColor color) {
		this.color = color;
		return this;
	}

	// SET DATA

	public SimpleChartSeriesBuilder withData(double[] xArray, double[] yArray) {
		size = xArray.length;
		dataSupplier = (series, i) -> new SimpleChartData(series, i, xArray[i], yArray[i]);
		return this;
	}

	public SimpleChartSeriesBuilder withData(List<double[]> inputData) {
		size = inputData.size();
		dataSupplier = (series, i) -> new SimpleChartData(series, i, inputData.get(i)[0], inputData.get(i)[1]);
		return this;
	}

	public SimpleChartSeriesBuilder withData(int size, IntFunction<? extends IChartData> dataSupplier) {
		this.size = size;
		this.dataSupplier = (series, index) -> new SimpleChartData(series, dataSupplier.apply(index));
		return this;
	}

	public SimpleChartSeriesBuilder withData(IChartSeries<? extends IChartData> otherSeries) {
		return withData(otherSeries.size(), otherSeries::getData);
	}

	// TOOLTIP FORMATTER

	public SimpleChartSeriesBuilder withTooltipFormatter(BiDoubleFunction<String> dataTooltipProvider) {
		optTooltipFormatter = Optional.of(dataTooltipProvider);
		return this;
	}

	// STATUS HANDLER

	public SimpleChartSeriesBuilder withStatusHandler(IChartSeriesStatusHandler statusHandler) {
		optStatusHandler = Optional.of(statusHandler);
		return this;
	}

	/**
	 * @return new instance of {@link IChartSeries}
	 */
	public SimpleChartSeries build() {
		var series = new SimpleChartSeries(title, source);
		for (int i = 0; i < size; i++) {
			var data = dataSupplier.apply(series, i);
			optTooltipFormatter.ifPresent(formatter -> data.setTooltip(formatter.apply(data.getX(), data.getY())));
			series.add(data);
		}
		series.setColor(color);
		optStatusHandler.ifPresent(series::setStatusHandler);
		return series;
	}

}