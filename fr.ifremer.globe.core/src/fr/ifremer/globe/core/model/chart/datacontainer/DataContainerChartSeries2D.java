package fr.ifremer.globe.core.model.chart.datacontainer;

import java.io.File;
import java.util.Optional;

import fr.ifremer.globe.core.model.chart.impl.LoadableChartSeries2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiIntFunctionWithException;
import fr.ifremer.globe.utils.function.IntFunctionWithException;
import fr.ifremer.globe.utils.function.TriConsumerWithException;

public class DataContainerChartSeries2D<T extends IBaseLayer> extends LoadableChartSeries2D {

	private final T dataContainerLayer;

	private final boolean isWritable;

	/**
	 * Constructor
	 */
	public DataContainerChartSeries2D(String title, DataContainerSeriesSource source, T layer, int size, int size2,
			IntFunctionWithException<Double, GIOException> xGetter,
			BiIntFunctionWithException<Double, GIOException> yGetter,
			TriConsumerWithException<Integer, Integer, Double, GIOException> ySetter) {
		super(title, source, size, size2, xGetter, yGetter, ySetter);
		this.dataContainerLayer = layer;
		this.isWritable = !source.getDataContainerToken().getOwner().isReadOnly();
	}

	@Override
	public boolean isDirty() {
		return isWritable && super.isDirty();
	}

	@Override
	public void load() throws GIOException {
		if (dataContainerLayer instanceof ILoadableLayer<?> loadableLayer)
			loadableLayer.load();
		super.load();
	}

	@Override
	public void flush() throws GIOException {
		super.flush();
		if (isWritable && dataContainerLayer instanceof ILoadableLayer<?> loadableLayer)
			loadableLayer.flush();
	}

	public Optional<File> getMappedFile() {
		return dataContainerLayer instanceof ILoadableLayer<?> loadableLayer
				? Optional.of(loadableLayer.getData().getLargeArray().getFile())
				: Optional.empty();
	}

}
