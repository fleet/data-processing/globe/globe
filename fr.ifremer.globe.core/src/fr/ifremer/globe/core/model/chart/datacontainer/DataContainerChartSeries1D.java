package fr.ifremer.globe.core.model.chart.datacontainer;

import fr.ifremer.globe.core.model.chart.impl.LoadableChartSeries1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiConsumerWithException;
import fr.ifremer.globe.utils.function.IntFunctionWithException;

public class DataContainerChartSeries1D<T extends IBaseLayer> extends LoadableChartSeries1D {

	private final T dataContainerLayer;

	private final boolean isWritable;

	/**
	 * Constructor
	 */
	public DataContainerChartSeries1D(String title, DataContainerSeriesSource source, T layer, int size,
			IntFunctionWithException<Double, GIOException> xGetter,
			IntFunctionWithException<Double, GIOException> yGetter,
			BiConsumerWithException<Integer, Double, GIOException> ySetter) {
		super(title, source, size, xGetter, yGetter, ySetter);
		this.dataContainerLayer = layer;
		this.isWritable = !source.getDataContainerToken().getOwner().isReadOnly();
	}

	public T getLayer() {
		return dataContainerLayer;
	}

	@Override
	public boolean isDirty() {
		return isWritable && super.isDirty();
	}

	@Override
	public void load() throws GIOException {
		if (dataContainerLayer instanceof ILoadableLayer<?> loadableLayer)
			loadableLayer.load();
		super.load();
	}

	@Override
	public void flush() throws GIOException {
		super.flush();
		if (isWritable && dataContainerLayer instanceof ILoadableLayer<?> loadableLayer)
			loadableLayer.flush();
	}

}
