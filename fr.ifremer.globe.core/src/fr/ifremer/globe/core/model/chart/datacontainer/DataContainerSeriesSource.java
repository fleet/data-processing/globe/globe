package fr.ifremer.globe.core.model.chart.datacontainer;

import java.nio.file.Path;
import java.util.Optional;

import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.model.chart.impl.ChartFileInfoDataSource;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;

public class DataContainerSeriesSource<T extends IDataContainerInfo> extends ChartFileInfoDataSource<T> implements IChartSeriesDataSource, AutoCloseable {

	private final IDataContainerToken<T> dataContainerToken;

	public DataContainerSeriesSource(IDataContainerToken<T> dataContainerToken) {
		super(dataContainerToken.getDataContainer().getInfo());
		this.dataContainerToken = dataContainerToken;
	}

	@Override
	public String getId() {
		return dataContainerToken.getDataContainer().getInfo().getFilename();
	}

	@Override
	public Optional<Path> getFilePath() {
		return Optional.of(Path.of(dataContainerToken.getDataContainer().getInfo().getAbsolutePath()));
	}

	public IDataContainerToken<? extends IFileInfo> getDataContainerToken() {
		return dataContainerToken;
	}

	@Override
	public void close() throws Exception {
		dataContainerToken.close();
	}

}
