package fr.ifremer.globe.core.model.chart.datacontainer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.FunctionWithException;

/**
 * This class provides method to get {@link IChartSeries} from {@link SounderDataContainer}.
 */
@Component(name = "globe_drivers_sounder_file_chart_series_supplier", service = { IChartSeriesSupplier.class })
public class SounderDataContainerSeriesSupplier implements IChartSeriesSupplier, IDataContainerOwner {

	@Override
	public boolean isReadOnly() {
		return true;
	}

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.MBG_NETCDF_3, ContentType.XSF_NETCDF_4, ContentType.SONAR_NETCDF_4);
	}

	@Override
	public List<? extends IChartSeries<? extends IChartData>> getChartSeries(IFileInfo fileInfo) throws GIOException {
		if (fileInfo instanceof ISounderNcInfo sounderFileInfo) {
			var dataContainerToken = IDataContainerFactory.grab().book(sounderFileInfo, this);
			var sounderDataContainer = dataContainerToken.getDataContainer();
			var timeLayer = sounderDataContainer.getLayer(BeamGroup1Layers.PING_TIME);

			// filter to get only layer with time dimension
			Predicate<IBaseLayer> layerFilter = layer -> layer.getDimensions().length > 0
					&& layer.getDimensions()[0] == timeLayer.getDimensions()[0];

			// time supplier
			FunctionWithException<Integer, Long, GIOException> indexToTime = index -> DateUtils
					.nanoSecondToMilli(timeLayer.get(index));

			// keep only valid swath
			var validIndexes = new ArrayList<Integer>();
			if (sounderDataContainer.hasStatusLayer()) {
				DetectionStatusLayer statusLayer = sounderDataContainer.getStatusLayer();
				for (int i = 0; i < sounderFileInfo.getCycleCount(); i++) {
					if (statusLayer.isSwathValid(i))
						validIndexes.add(i);
				}
			}
			Optional<List<Integer>> optValidIndexes = validIndexes.isEmpty()
					|| validIndexes.size() == sounderFileInfo.getCycleCount() ? Optional.empty()
							: Optional.of(validIndexes);

			// build series
			return new DataContainerSeriesBuilder(dataContainerToken, indexToTime, layerFilter, optValidIndexes)
					.getSeries();
		}
		return Collections.emptyList();
	}

}
