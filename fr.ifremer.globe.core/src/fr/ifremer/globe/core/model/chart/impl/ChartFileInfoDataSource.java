package fr.ifremer.globe.core.model.chart.impl;

import java.nio.file.Path;
import java.util.Optional;

import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.model.file.IFileInfo;

/**
 * Implementation of {@link IChartSeriesDataSource} based on a {@link IFileInfo}.
 */
public class ChartFileInfoDataSource<T extends IFileInfo> implements IChartSeriesDataSource {

	private final T fileInfo;

	/**
	 * Constructor
	 */
	public ChartFileInfoDataSource(T fileInfo) {
		this.fileInfo = fileInfo;
	}

	@Override
	public String getId() {
		return fileInfo.getFilename();
	}

	@Override
	public Optional<Path> getFilePath() {
		return Optional.of(Path.of(fileInfo.getPath()));
	}

	public T getFileInfo() {
		return fileInfo;
	}

}
