package fr.ifremer.globe.core.model.chart.impl;

import java.util.Objects;
import java.util.Optional;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Simple implementation of {@link IChartData}.
 */
public class SimpleChartData implements IChartData {

	/** Properties **/
	protected final IChartSeries<? extends IChartData> series;
	private int index;
	private double x;
	private double y;
	private boolean enabled = true;
	private Optional<String> optTooltip = Optional.empty();
	private Optional<String> sectionId = Optional.empty();

	/**
	 * Constructor
	 */
	public SimpleChartData(IChartSeries<? extends IChartData> series, int index, double x, double y) {
		this.series = series;
		this.index = index;
		this.x = x;
		this.y = y;
	}

	/**
	 * Constructor from other {@link IChartData}
	 */
	public SimpleChartData(IChartSeries<? extends IChartData> series, IChartData otherData) {
		this(series, otherData.getIndex(), otherData.getX(), otherData.getY());
		this.enabled = otherData.isEnabled();
		this.optTooltip = Optional.ofNullable(otherData.getTooltip());
		this.sectionId = otherData.getSectionId();
	}

	@Override
	public IChartSeries<? extends IChartData> getSeries() {
		return series;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public void setY(double y) {
		this.y = y;
		if (series instanceof LoadableChartSeries loadableChartSeries)
			loadableChartSeries.setDirty(true);
	}

	@Override
	public Optional<String> getSectionId() {
		return enabled ? sectionId : Optional.empty();
	}

	public void setSectionId(String sectionId) {
		this.sectionId = Optional.of(sectionId);
	}

	@Override
	public GColor getColor() {
		return isEnabled() ? series.getColor() : GColor.LIGHT_GRAY;
	}

	@Override
	public String getTooltip() {
		return optTooltip.orElse(IChartData.super.getTooltip());
	}

	public void setTooltip(String tooltip) {
		this.optTooltip = Optional.of(tooltip);
	}

	@Override
	public boolean isEnabled() {
		return series.getStatusHandler().map(statusHandler -> statusHandler.getStatus(this) == 0).orElse(this.enabled);
	}

	@Override
	public void setEnabled(boolean enabled) {
		series.getStatusHandler().ifPresentOrElse(statusHandler -> statusHandler.setStatus(this, enabled ? 0 : 1),
				() -> this.enabled = enabled);
	}

	@Override
	public int hashCode() {
		return Objects.hash(index, series, x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleChartData other = (SimpleChartData) obj;
		return index == other.index && Objects.equals(series, other.series)
				&& Double.doubleToLongBits(x) == Double.doubleToLongBits(other.x)
				&& Double.doubleToLongBits(y) == Double.doubleToLongBits(other.y);
	}

}
