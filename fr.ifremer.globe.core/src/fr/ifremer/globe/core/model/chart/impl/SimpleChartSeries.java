package fr.ifremer.globe.core.model.chart.impl;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Simple implementation of {@link IChartSeries}.
 * 
 * Use {@link SimpleChartSeriesBuilder} to properly instantiate this class.
 */
public class SimpleChartSeries extends BasicChartSeries<SimpleChartData> {

	/** Properties **/
	private final List<SimpleChartData> data = new ArrayList<>();

	public SimpleChartSeries(String title, IChartSeriesDataSource source) {
		super(title, source);
	}

	public SimpleChartSeries(String title, double[] xArray, double[] yArray) {
		super(title, IChartSeriesDataSource.UNKNOWN);
		for (int i = 0; i < xArray.length; i++)
			add(new SimpleChartData(this, i, xArray[i], yArray[i]));
	}

	public SimpleChartSeries(IChartSeries<? extends IChartData> otherSeries, String title, GColor color) {
		super(title, IChartSeriesDataSource.UNKNOWN);
		otherSeries.forEach(d -> add(new SimpleChartData(this, d.getIndex(), d.getX(), d.getY())));
		setColor(color);
	}

	public SimpleChartSeries(String title, GColor color, List<double[]> inputData) {
		super(title, IChartSeriesDataSource.UNKNOWN);
		for (int i = 0; i < inputData.size(); i++)
			add(new SimpleChartData(this, i, inputData.get(i)[0], inputData.get(i)[1]));
		setColor(color);
	}

	@Override
	public int size() {
		return data.size();
	}

	public void add(SimpleChartData newData) {
		data.add(newData);
	}

	@Override
	public SimpleChartData getData(int index) {
		return data.get(index);
	}

}
