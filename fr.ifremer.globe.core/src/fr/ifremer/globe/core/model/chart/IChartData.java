package fr.ifremer.globe.core.model.chart;

import java.time.Instant;
import java.util.Optional;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * This interface defines the model for a data (= one series'point)
 */
public interface IChartData extends Comparable<IChartData> {

	/**
	 * @return the {@link IChartSeries} which contains this {@link IChartData}.
	 */
	public IChartSeries<? extends IChartData> getSeries();

	public int getIndex();

	public double getX();

	public double getY();

	public default void setY(double d) {
		// not editable by default
	}

	public default boolean isEnabled() {
		return true;
	}

	public default void setEnabled(boolean enabled) {
		// not editable by default
	}

	public GColor getColor();

	public default int getWidth() {
		return 2;
	}

	public default String getTooltip() {
		var x = getX();
		// if x is very large, it is probably an epoch time in milliseconds
		String xLabel = x > 1_000_000_000 ? Instant.ofEpochMilli((long) x).toString() : String.format("%.2f", x);
		String yLabel = String.format("%.2f", getY());
		return xLabel + " : " + yLabel;
	}

	public default double getComparisonValue() {
		return 0;
	}

	/**
	 * Section ID is used to separate a FXChartSeries in several segments . If not specified, the associated segment is
	 * defined by color.
	 */
	public default Optional<String> getSectionId() {
		return Optional.empty();
	}

	/**
	 * Compares to other {@link IChartData}
	 * 
	 * @return negative if this < other, positive if this > other, zero if equal
	 */
	@Override
	public default int compareTo(IChartData otherData) {
		return (int) (getComparisonValue() - otherData.getComparisonValue());
	}

	/**
	 * @return true is this {@link IChartData} is the first of its {@link IChartSeries}.
	 */
	public default boolean isFirst() {
		return getIndex() == 0;
	}

	/**
	 * @return true is this {@link IChartData} is the last of its {@link IChartSeries}.
	 */
	public default boolean isLast() {
		return getIndex() == getSeries().size() - 1;
	}

}
