package fr.ifremer.globe.core.model.chart;

/**
 * Handles status for {@link IChartData}.
 */
public interface IChartSeriesStatusHandler<T extends IChartData> {

	int getStatus(T data);

	void setStatus(T data, int status);

	void flush();

	boolean isDirty();

}
