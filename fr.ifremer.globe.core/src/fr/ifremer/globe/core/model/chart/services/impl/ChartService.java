/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.chart.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.chart.services.IChartService;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link IChartService}.
 */
@Component(name = "globe_model_service_chart", service = IChartService.class)
public class ChartService implements IChartService {

	/** All registered suppliers */
	protected List<IChartSeriesSupplier> chartSeriesSuppliers = new ArrayList<>();

	@Override
	public boolean isAccepted(ContentType contenType) {
		return chartSeriesSuppliers.stream().map(IChartSeriesSupplier::getContentTypes)
				.anyMatch(contentTypes -> contentTypes.contains(contenType));
	}

	@Override
	public List<? extends IChartSeries<? extends IChartData>> getChartSeries(String filePath) throws GIOException {
		var optFileInfo = IFileService.grab().getFileInfo(filePath);
		if (optFileInfo.isPresent())
			return getChartSeries(optFileInfo.get());
		return Collections.emptyList();
	}

	@Override
	public List<? extends IChartSeries<? extends IChartData>> getChartSeries(IFileInfo info) throws GIOException {
		List<IChartSeries<? extends IChartData>> seriesList = new ArrayList<>();
		for (var charSeriesSupplier : chartSeriesSuppliers)
			charSeriesSupplier.getChartSeries(info).forEach(seriesList::add);
		return seriesList;
	}

	/** Accept a new {@link IChartSeriesSupplier} provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addRasterInfoSupplier(IChartSeriesSupplier chartSeriesSupplier) {
		chartSeriesSuppliers.add(chartSeriesSupplier);
	}

}