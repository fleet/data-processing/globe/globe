/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.chart.services;

import java.util.List;
import java.util.Set;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Defines an interface to supply {@link IChartSeries} for an entry file.
 */
public interface IChartSeriesSupplier {

	/**
	 * @return the list of managed file type
	 */
	Set<ContentType> getContentTypes();

	/**
	 * @return a list of {@link IChartSeries} for the specified {@link IFileInfo}.
	 */
	List<? extends IChartSeries<? extends IChartData>> getChartSeries(IFileInfo fileInfo) throws GIOException;

}
