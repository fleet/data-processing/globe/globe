package fr.ifremer.globe.core.model.properties;

public class StringAttributeProperty extends AttributeProperty<String> {

	public StringAttributeProperty(String key, String value) {
		super(key, value);
	}

	@Override
	public String getValueAsString() {
		return getValue() != null ? getValue() : "";
	}

	@Override
	public String formatValue() {
		return getValueAsString();
	}
}
