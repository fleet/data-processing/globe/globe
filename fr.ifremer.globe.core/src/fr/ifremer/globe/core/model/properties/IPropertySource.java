package fr.ifremer.globe.core.model.properties;

import java.util.List;


/** Properties provider interface. */
public interface IPropertySource {
	/** This property source name. */
	public String getName();

	/** Properties list. */
	public List<Property<?>> getProperties();
}
