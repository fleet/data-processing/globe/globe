package fr.ifremer.globe.core.model.properties;

/**
 * Contains attribute property values. An attribute is a key value object containing metadata information about an
 * object
 */
public abstract class AttributeProperty<T> {

	private final String key;
	private final T value;

	protected AttributeProperty(String key, T value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public T getValue() {
		return value;
	}

	public abstract String getValueAsString();

	public abstract String formatValue();

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName());
		sb.append(" : ");
		sb.append(this.getKey());
		sb.append(" -> ");
		sb.append(this.getValueAsString());
		return sb.toString();
	}
}
