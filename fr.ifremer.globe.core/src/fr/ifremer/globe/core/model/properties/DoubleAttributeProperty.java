package fr.ifremer.globe.core.model.properties;

import fr.ifremer.globe.utils.number.NumberUtils;

public class DoubleAttributeProperty extends AttributeProperty<Double> {

	public DoubleAttributeProperty(String key, Double value) {
		super(key, value);
	}

	@Override
	public String getValueAsString() {
		return getValue() != null ? Double.toString(this.getValue()) : "";
	}

	@Override
	public String formatValue() {
		return getValue() != null ? NumberUtils.getStringDouble(this.getValue().doubleValue()) : "";
	}
}
