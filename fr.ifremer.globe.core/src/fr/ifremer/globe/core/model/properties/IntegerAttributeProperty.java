package fr.ifremer.globe.core.model.properties;

public class IntegerAttributeProperty extends AttributeProperty<Integer> {

	public IntegerAttributeProperty(String key, Integer value) {
		super(key, value);
	}

	@Override
	public String getValueAsString() {
		return getValue() != null ? Integer.toString(this.getValue().intValue()) : "";
	}

	@Override
	public String formatValue() {
		return getValueAsString();
	}

}
