package fr.ifremer.globe.core.model.properties;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.math.Range;
import org.apache.commons.lang3.StringUtils;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.globe.utils.units.Units;

/**
 * Contains property values.
 */
public class Property<T> {

	private final AttributeProperty<T> attributeProperty;
	/** May contain a list of sub properties. */
	private List<Property<?>> subProperties = new ArrayList<>();

	public Property(AttributeProperty<T> value) {
		attributeProperty = value;
	}

	public String getKey() {
		return attributeProperty.getKey();
	}

	public T getValue() {
		return attributeProperty.getValue();
	}

	public String getValueAsString() {
		return attributeProperty.getValueAsString();
	}

	public String formatValue() {
		return attributeProperty.formatValue();
	}

	public List<Property<?>> getSubProperties() {
		return Collections.unmodifiableList(subProperties);
	}

	/**
	 * Search the property with the specified key<br>
	 * Can return this or one of the sub-property
	 */
	public Optional<Property<?>> findByKey(String key) {
		if (key.equals(getKey()))
			return Optional.of(this);
		return subProperties.stream().filter(p -> key.equals(p.getKey())).findFirst();
	}

	/** Adds a new sub property. */
	public synchronized void add(Property<?> p) {
		if (p != null) {
			subProperties.add(p);
		}
	}

	@Override
	public String toString() {
		return toString(this, 0);
	}

	/**
	 * Builder to create a property from String value
	 */
	public static Property<String> build(String key, String value) {
		return new Property<>(new StringAttributeProperty(key, value));
	}

	/**
	 * Builder to create a property from String value
	 */
	public static Property<String> build(String key, String value, int max) {
		if (value != null && value.length() > max) {
			String[] lines = WordUtils.wrap(value, max, "\n", false).split("\n");
			return buildDetailed(key, Arrays.asList(lines));
		}
		return build(key, value);
	}

	/**
	 * Builder to create a property from boolean value
	 */
	public static Property<String> build(String key, boolean value) {
		return new Property<>(new StringAttributeProperty(key, Boolean.toString(value)));
	}

	/**
	 * Builder to create a property from Double value
	 */
	public static Property<Double> build(String key, Double value) {
		return new Property<>(new DoubleAttributeProperty(key, value));
	}

	/**
	 * Builder to create a property from Integer value
	 */
	public static Property<Integer> build(String key, Integer value) {
		return new Property<>(new IntegerAttributeProperty(key, value));
	}

	/**
	 * Builder to create a property from int value
	 */
	public static Property<Integer> build(String key, int value) {
		return Property.build(key, Integer.valueOf(value));
	}

	/**
	 * Builder to create a property from Instant value
	 */
	public static Property<String> build(String key, Instant value) {
		return build(key, value.toString());
	}

	/**
	 * Builder to create a properties for a GeoBox
	 */
	public static List<Property<String>> build(GeoBox geoBox) {
		List<Property<String>> result = new ArrayList<>();
		if (geoBox != null) {
			ILatLongFormatter formatter = LatLongFormater.getFormatter();
			result.add(Property.build(IConstants.NorthLatitude, formatter.formatLatitude(geoBox.getTop())));
			result.add(Property.build(IConstants.WestLongitude, formatter.formatLongitude(geoBox.getLeft())));
			result.add(Property.build(IConstants.SouthLatitude, formatter.formatLatitude(geoBox.getBottom())));
			result.add(Property.build(IConstants.EastLongitude, formatter.formatLongitude(geoBox.getRight())));
		}
		return result;
	}

	/**
	 * Builder to create a properties for a Projection
	 */
	public static List<Property<String>> build(Projection projection) {
		List<Property<String>> result = new ArrayList<>();
		if (projection != null) {
			result.add(Property.build(IConstants.Projection, projection.getProj4ProjectionString()));
		}
		return result;
	}

	/**
	 * Builder to create a properties for min and max values
	 */
	public static List<Property<String>> buildMinMax(Range minMaxVal) {
		List<Property<String>> result = new ArrayList<>();
		if (minMaxVal != null) {
			if (Double.isFinite(minMaxVal.getMaximumDouble())) {
				result.add(Property.build("Maximum value",
						NumberUtils.getStringMetersDouble(minMaxVal.getMaximumDouble())));
			}
			if (Double.isFinite(minMaxVal.getMinimumDouble())) {
				result.add(Property.build("Minimum value",
						NumberUtils.getStringMetersDouble(minMaxVal.getMinimumDouble())));
			}
		}
		return result;
	}

	/**
	 * Builder to create a properties for a Spatial resolution
	 */
	public static List<Property<String>> buildSpatialResolution(Projection projection, double spatialResolutionX,
			double spatialResolutionY) {
		List<Property<String>> result = new ArrayList<>();
		if (projection != null) {

			if (ProjectionSettings.isLongLatProjection(projection)) {
				result.add(Property.build(IConstants.SpatialResolutionX,
						NumberUtils.getStringArcSecond(Units.deg2arcSec(spatialResolutionX)) + "'' ("
								+ NumberUtils.scientificNotation(Math.abs(spatialResolutionX)) + "\u00b0)"));
				result.add(Property.build(IConstants.SpatialResolutionY,
						NumberUtils.getStringArcSecond(Units.deg2arcSec(spatialResolutionY)) + "'' ("
								+ NumberUtils.scientificNotation(Math.abs(spatialResolutionY)) + "\u00b0)"));
			} else {
				result.add(Property.build(IConstants.SpatialResolutionX,
						NumberUtils.getStringMetersDouble(spatialResolutionX) + " m"));
				result.add(Property.build(IConstants.SpatialResolutionY,
						NumberUtils.getStringMetersDouble(spatialResolutionY) + " m"));
			}
		} else {
			result.add(Property.build(IConstants.SpatialResolutionX,
					NumberUtils.getStringMetersDouble(spatialResolutionX)));
			result.add(Property.build(IConstants.SpatialResolutionY,
					NumberUtils.getStringMetersDouble(spatialResolutionY)));
		}
		return result;
	}

	/**
	 * Builder to create a properties for min and max values
	 */
	public static List<Property<String>> buildSize(int xSize, int ySize) {
		List<Property<String>> result = new ArrayList<>();
		result.add(Property.build("Size", String.format("%d x %d", xSize, ySize)));
		return result;
	}

	/**
	 * Builder to create a properties for set of string
	 */
	public static Property<String> build(String key, Collection<String> fields) {
		return build(key, StringUtils.join(fields, ','));
	}

	public static Property<String> build(AntennaInstallationParameters antenna) {
		Property<String> prop = Property.build(antenna.type + " antenna [id: " + antenna.antennaID + "]", "");
		prop.add(Property.build("X", Double.toString(antenna.xoffset)));
		prop.add(Property.build("Y", Double.toString(antenna.yoffset)));
		prop.add(Property.build("Z", Double.toString(antenna.zoffset)));
		prop.add(Property.build("heading", Double.toString(antenna.headingoffset)));
		prop.add(Property.build("pich", Double.toString(antenna.pitchoffset)));
		prop.add(Property.build("roll", Double.toString(antenna.rolloffset)));
		return prop;
	}

	/**
	 * Builder to create a property for set of string
	 */
	public static Property<String> buildDetailed(String key, Collection<String> fields) {
		Property<String> result = null;
		if (fields == null || fields.isEmpty()) {
			result = build(key, "");
		} else if (fields.size() == 1) {
			result = build(key, fields.iterator().next());
		} else {
			Iterator<String> it = fields.iterator();
			result = Property.build(key, it.next());
			while (it.hasNext()) {
				result.add(build("", it.next()));
			}
		}
		return result;
	}

	/**
	 * Builder to create a property for map of string
	 */
	public static Property<?> buildDetailed(String key, Map<String, Set<String>> fields) {
		Property<String> result = Property.build(key, "...");
		if (fields != null && !fields.isEmpty()) {
			for (Entry<String, Set<String>> entry : fields.entrySet()) {
				result.add(buildDetailed(entry.getKey(), entry.getValue()));
			}
		}
		return result;
	}

	private static String toString(Property<?> property, int level) {
		StringBuilder result = new StringBuilder(property.getKey()).append(" = ")
				.append(property.attributeProperty.getValueAsString());
		if (property.subProperties.size() > 0) {
			result.append("\n");
			for (Property<?> sub : property.subProperties) {
				result.append(StringUtils.repeat('\t', level)).append("+-- ");
				result.append(toString(sub, level + 1));
			}
		}
		return result.toString();
	}

}
