package fr.ifremer.globe.core.model.file.polygon;

import fr.ifremer.globe.core.model.file.ContentType;

/**
 * FileInfo dedicated to polygon file.
 *
 */
public class GdalGeoJsonFileInfo extends PolygonFileInfo {

	/**
	 * Constructor
	 */
	public GdalGeoJsonFileInfo(String sourcefile) {
		super(sourcefile, ContentType.GEOJSON_GDAL);
	}

}