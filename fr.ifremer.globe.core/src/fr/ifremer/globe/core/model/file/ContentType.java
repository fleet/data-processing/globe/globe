/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.file;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;

import fr.ifremer.globe.core.model.file.pointcloud.IPoint;

/**
 * All possible values describing the content type of the file
 */
public enum ContentType {

	/** Content type of this file is undefined */
	UNDEFINED,

	/** BackScatter Angular Response netcdf **/
	BSAR_NETCDF_4,

	/** This file is an image in GDAL compliant format */
	IMAGE_GDAL,

	/** This file is a raster file in GDAL compliant format */
	RASTER_GDAL,

	/** Vector file managed by GDAL directly */
	VECTOR_GDAL,
	/** Vector file managed by GDAL directly, and containing an editable polygon */
	POLYGON_GDAL,
	/** GeoJson file managed by GDAL directly, and containing polygons and points */
	GEOJSON_GDAL,

	/** This file is a DTM (.nc) in Netcdf-4 format */
	DTM_NETCDF_4,
	/** This file is a GMT (.gmt, .grd) in Netcdf-3 or 4 format */
	GMT,

	/** This file is a DTM (.mnt, .bag, .dtm) in Netcdf-3 format */
	DTM_NETCDF_3,

	/** This file is a echo sounder file (.nc or .xsf) in Netcdf-4 format */
	XSF_NETCDF_4,
	/** This file contains sonar data in netCDF4-formatted (.nc) */
	SONAR_NETCDF_4,
	/** This file is a deprecated echo sounder file (.nc or .xsf) in Netcdf-4 format */
	OLD_XSF_NETCDF_4,

	/** This file is a echo sounder file (.mbg) in Netcdf-3 format */
	MBG_NETCDF_3,

	/** This file is a Navgation file (.nvi and .nvi.nc) in Netcdf-4 format */
	NVI_NETCDF_4,
	NVI_V2_NETCDF_4,

	/** This file is a Sound Velocity file (.vel) in Netcdf-4 format */
	VEL_NETCDF_4,

	/** This file is a Generic Display Format file (.nc) in Netcdf-4 format */
	G3D_NETCDF_4,

	/** NetCDF file from TECHSAS (TECHnical Sensor Acquisition System) */
	TECHSAS_NETCDF,
	TECHSAS_NETCDF_WITH_NAV,
	TECHSAS_NETCDF_GRAVI,
	TECHSAS_NETCDF_MAG,
	TECHSAS_NETCDF_DEPTH,

	/** This file is a text file (.emo) in a "Comma-separated values" format */
	EMO_CSV,

	/** This file bathymetric file (.xyz) in a "Comma-separated values" format */
	BATHIMETRIC_CSV,

	/** This file is a list of makers in a "Comma-separated values" format */
	MARKER_CSV,

	/** This file is a list of navigation points in a "Comma-separated values" format */
	NAVIGATION_CSV,

	/** This file provides a list of {@link IPoint} to display */
	POINT_CLOUD_CSV,

	/** This file is a echo sounder file in a binary Reason format with the extension .s7k */
	SOUNDER_S7K,

	/** This file is a echo sounder file in a binary Kongsberg format with the extension .all */
	SOUNDER_ALL,

	/** This file is a echo sounder file in a binary Kongsberg format with the new extension .kmall */
	SOUNDER_KMALL,

	/** This file is a geographic data file (.kml) in XML format */
	KML_XML,

	/** This file is a text file which contains tide data (tide value by time) **/
	TIDE_CSV,

	/** This file is a text file which contains Bathymetry, Magnetics and Gravity data **/
	MGD77,

	/** This file is a text file which contains position frame from USBL (ultra-short baseline). **/
	USBL,

	/** These files are geospatial vector (.shp) data format **/
	SHAPEFILE,

	/** This file is a journey file in data format **/
	JOURNEY,

	/** This file is a SEG-Y Data Exchange format **/
	SEGY,

	/** Placa files **/
	POLE_TOT,
	COT,
	POLE_INF,
	MTEC,

	/** ADCP files in Oceansite format (NETCDF) */
	ADCP_OCEANSITE,
	/** Acoustic doppler current profiler file (*.STA) */
	ADCP_STA,

	// OLD DEPRECATED FORMATS. TO REMOVED WHEN BECOME OBSOLETED
	OLD_NAV_XML,
	OLD_ELEVATION_XML,
	OLD_DTM_XML,
	OLD_REFLECTIVITY_XML,
	OLD_ECHOGRAM_XML,
	OLD_MULTITEXTURE_XML,
	OLD_PLUMEECHOES_XML,
	OLD_SEISMIC_XML,
	OLD_BATHYMETRY_XML;

	// ENUM SETS

	/** All raw sounder files */
	private static final EnumSet<ContentType> SounderRawFileContentTypes = EnumSet.of(ContentType.SOUNDER_S7K,
			SOUNDER_ALL, SOUNDER_KMALL);

	/** All files containing navigation line */
	private static final EnumSet<ContentType> NavigationContentTypes = EnumSet.of(MBG_NETCDF_3, NVI_NETCDF_4,
			NVI_V2_NETCDF_4, SOUNDER_S7K, SOUNDER_ALL, SOUNDER_KMALL, XSF_NETCDF_4, SONAR_NETCDF_4, NAVIGATION_CSV,
			MGD77, USBL, TECHSAS_NETCDF_WITH_NAV, TECHSAS_NETCDF_DEPTH, TECHSAS_NETCDF_GRAVI, TECHSAS_NETCDF_MAG,
			ADCP_OCEANSITE);

	/** All DTM file types */
	private static final EnumSet<ContentType> DtmContentTypes = EnumSet.of(DTM_NETCDF_3, DTM_NETCDF_4);

	/** All files in Netcdf format */
	private static final EnumSet<ContentType> NetcdfContentTypes = EnumSet.of(MBG_NETCDF_3, DTM_NETCDF_3, DTM_NETCDF_4,
			NVI_NETCDF_4, NVI_V2_NETCDF_4, VEL_NETCDF_4, XSF_NETCDF_4, TECHSAS_NETCDF, TECHSAS_NETCDF_WITH_NAV,
			TECHSAS_NETCDF_DEPTH, TECHSAS_NETCDF_GRAVI, TECHSAS_NETCDF_MAG, G3D_NETCDF_4, SONAR_NETCDF_4, GMT,
			ADCP_OCEANSITE, BSAR_NETCDF_4);

	/** All files view as ISounderNcInfo */
	private static final EnumSet<ContentType> SounderNcInfoContentTypes = EnumSet.of(MBG_NETCDF_3, XSF_NETCDF_4);

	/** All TECHSAS content types **/
	private static final Set<ContentType> TechsasContentTypes = EnumSet.of(TECHSAS_NETCDF, TECHSAS_NETCDF_WITH_NAV,
			TECHSAS_NETCDF_GRAVI, TECHSAS_NETCDF_MAG, TECHSAS_NETCDF_DEPTH);

	// PUBLIC API

	public static final Set<ContentType> getSounderRawFileContentTypes() {
		// return a copy to avoid modification of original set
		return EnumSet.copyOf(SounderRawFileContentTypes);
	}

	public static final Set<ContentType> getNavigationContentTypes() {
		// return a copy to avoid modification of original set
		return EnumSet.copyOf(NavigationContentTypes);
	}

	public static final Set<ContentType> getSounderNcContentType() {
		// return a copy to avoid modification of original set
		return EnumSet.copyOf(SounderNcInfoContentTypes);
	}

	public static final Set<ContentType> getTechsasContentTypes() {
		// return a copy to avoid modification of original set
		return EnumSet.copyOf(TechsasContentTypes);
	}

	/** Return true if this kind of file contains a navigation line */
	public boolean hasNavigation() {
		return NavigationContentTypes.contains(this);
	}

	/** Return true if this kind of file is in Netcdf format */
	public boolean isNetcdf() {
		return NetcdfContentTypes.contains(this);
	}

	/** Return true if this kind of file is in Netcdf format */
	public boolean isDtm() {
		return DtmContentTypes.contains(this);
	}

	/** Content type of the file */
	public boolean isManageableInSwathEditor() {
		return SounderNcInfoContentTypes.contains(this);
	}

	/** Content type of the file */
	public boolean isSounderNcInfo() {
		return SounderNcInfoContentTypes.contains(this);
	}

	/** Content type of the file */
	public boolean isOneOf(ContentType... contentTypes) {
		return Arrays.stream(contentTypes).anyMatch(this::equals);
	}

	/**
	 * @param name one ContentType name
	 * @return the ContentType named by the specified parameter. UNDEFINED if none
	 */
	public static ContentType getByName(String name) {
		ContentType result = UNDEFINED;
		if (name != null && !name.isEmpty()) {
			try {
				result = Enum.valueOf(ContentType.class, name);
			} catch (IllegalArgumentException e) {
				// Let result to UNDEFINED
			}
		}
		return result;
	}

	/**
	 * @return a {@link Set} of {@link ContentType} associated to the provided name.
	 * 
	 *         (Enables to specify an set of {@link ContentType} in PyAT configuration files, and not only one
	 *         {@link ContentType})
	 */
	public static Set<ContentType> getContentTypeSetByName(String name) {
		// get content type directly from Enum values
		var contentType = getByName(name);
		if (contentType != UNDEFINED)
			return Set.of(contentType);

		// get content types from sets
		switch (name) {
		case "TECHSAS":
			return EnumSet.copyOf(TechsasContentTypes);
		default:
			return Set.of(UNDEFINED);
		}
	}

}
