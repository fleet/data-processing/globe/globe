package fr.ifremer.globe.core.model.file.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.gdal.osr.SpatialReference;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gdal.GdalOgrUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Loader of any GDAL file info. Builder of GdalInfo
 */
@Component(//
		name = "globe_model_service_gdal_vector_info_supplier", //
		service = { IFileInfoSupplier.class, IGeographicInfoSupplier.class }, //
		property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer="
				+ FileInfoSupplierPriority.GDAL_VECTOR_PRIORITY }//

)
public class GdalVectorInfoLoader implements IFileInfoSupplier<IFileInfo>, IGeographicInfoSupplier {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalVectorInfoLoader.class);

	/** Content type is undefined : this files are not really managed in Globe */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.UNDEFINED);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath) {
		IFileInfo result = null;
		try {
			result = loadInfo(filePath);
		} catch (GIOException e) {
			logger.warn("Error on {} loading : {}", filePath, e.getMessage());
		}
		return Optional.ofNullable(result);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EnumSet<ContentType> getContentTypes() {
		return CONTENT_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getExtensions() {
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Collections.emptyList();
	}

	/**
	 * @return the GdalInfo of the specified resource. null if filePath is not managed by GDAL
	 * @throws GIOException read failed, error occured
	 */
	protected IFileInfo loadInfo(String filePath) throws GIOException {
		IFileInfo result = null;
		try {
			// Vector ?
			GdalVectorInfo info = new GdalVectorInfo(filePath);
			GdalOgrUtils.readMetadata(info.getPath(), info::fitSpatialReference, info::fitExtent, info::fitGeometry,
					info::fitFieldDefn);
			result = info;
		} catch (IOException e) {
			// May append on GdalOgrUtils.readMetadata when file is not a supported vector file
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Projection> getProjection(IFileInfo fileInfo) {
		if (fileInfo instanceof GdalVectorInfo) {
			return Optional.ofNullable(((GdalVectorInfo) fileInfo).projection);
		}
		return Optional.empty();
	}

	/**
	 * IInfoStore dedicated to GDAL Vector file
	 */
	class GdalVectorInfo implements IFileInfo {

		/** Gdal file */
		protected String sourcefile;
		/** Raster X Size */
		protected String geometryType;
		/** Geo projection */
		protected Projection projection;
		/** GeoBox */
		protected GeoBox geoBox;
		/** Fields and their possible values */
		protected Map<String, Set<String>> fields;

		/**
		 * Constructor
		 */
		public GdalVectorInfo(String sourcefile) {
			this.sourcefile = sourcefile;
		}

		/**
		 * @return the {@link #contentType}
		 */
		@Override
		public ContentType getContentType() {
			return ContentType.VECTOR_GDAL;
		}

		/** Getter of {@link #projection} */
		public Projection getProjection() {
			return projection;
		}

		/** {@inheritDoc} */
		@Override
		public String getPath() {
			return sourcefile;
		}

		/** Accept the extent from the shapefile */
		protected void fitExtent(double[] extent) {
			if (extent != null) {
				if (projection != null && !projection.getProjectionSettings().isLongLatProjection()) {
					try {
						double[] en = projection.unproject(extent[1], extent[3]);
						double[] os = projection.unproject(extent[0], extent[2]);
						geoBox = new GeoBox(en[1], os[1], en[0], os[0]);
					} catch (ProjectionException e) {
						// Error when project
					}
				} else {
					geoBox = new GeoBox(extent[3], extent[2], extent[1], extent[0]);
				}
			}
		}

		/** Accept the SpatialReference from the shapefile */
		protected void fitSpatialReference(SpatialReference spatialReference) {
			if (spatialReference != null) {
				projection = Projection.createFromProj4String(spatialReference.ExportToProj4());
			} else {
				projection = new Projection(StandardProjection.LONGLAT);
			}
		}

		/** Accept the SpatialReference from the shapefile */
		protected void fitGeometry(String geometryType) {
			this.geometryType = geometryType;
		}

		/** Accept the SpatialReference from the shapefile */
		protected void fitFieldDefn(Map<String, Set<String>> fields) {
			this.fields = fields;
		}

		/** Getter of {@link #geometryType} */
		public String getGeometryType() {
			return geometryType;
		}

		/** Getter of {@link #fields} */
		public Map<String, Set<String>> getFields() {
			return fields;
		}

		@Override
		public List<Property<?>> getProperties() {
			List<Property<?>> result = new ArrayList<>();
			result.addAll(Property.build(geoBox));
			result.addAll(Property.build(projection));
			result.add(Property.build("Geometry ", geometryType));
			if (fields != null) {
				result.add(Property.build("Fields ", fields.keySet()));
			}
			return result;
		}

		@Override
		public GeoBox getGeoBox() {
			return geoBox;
		}

	}

}