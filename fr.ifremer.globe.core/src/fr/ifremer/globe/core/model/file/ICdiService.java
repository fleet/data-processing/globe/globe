/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.file;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Services of CDI
 */
public interface ICdiService {

	/**
	 * Returns the service implementation of ICdiService
	 */
	static ICdiService grab() {
		return OsgiUtils.getService(ICdiService.class);
	}

	/** @return all the CDI contains in the specified file */
	List<String> getCdis(String filePath);

	/** @return all the CDI contains in the specified IInfoStore */
	List<String> getCdis(IFileInfo fileInfo);

	/** @return the CDI at the specified position for the given IInfoStore */
	Optional<String> getCdi(IFileInfo fileInfo, GeoPoint position);
}
