/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file;

import java.util.Optional;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;

/**
 * Service able to supply some geographic informations on files
 */
public interface IGeographicInfoSupplier {

	/** @return the Projection contained in the file */
	default Optional<Projection> getProjection(IFileInfo fileInfo) {
		return Optional.empty();
	}

	/** @return the Spatial resolution contained in the file */
	default Optional<SpatialResolution> getSpatialResolution(IFileInfo fileInfo) {
		return Optional.empty();
	}

	/** @return the GeoBox contained in the file */
	default Optional<GeoBox> getGeoBox(IFileInfo fileInfo) {
		return Optional.empty();
	}

	/** Pojo used to hold x and y Spatial resolutions */
	public class SpatialResolution {
		/** X spatial resolutions */
		protected final double xResolution;
		/** Y spatial resolutions */
		protected final double yResolution;

		/**
		 * Constructor
		 */
		public SpatialResolution(double xResolution, double yResolution) {
			this.xResolution = xResolution;
			this.yResolution = yResolution;
		}

		/**
		 * @return the {@link #xResolution}
		 */
		public double getxResolution() {
			return xResolution;
		}

		/**
		 * @return the {@link #yResolution}
		 */
		public double getyResolution() {
			return yResolution;
		}
	}
}
