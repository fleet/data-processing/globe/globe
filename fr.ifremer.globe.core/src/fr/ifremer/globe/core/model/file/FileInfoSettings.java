/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Arguments used to configure the parsing of a file
 */
public class FileInfoSettings extends HashMap<String, String> {

	/** Key of the realm to store the properties used to parse a file */
	public static final String REALM_PROPERTIES_FILE_SETTINGS = "file_settings";

	/**
	 * Serializable ID
	 */
	private static final long serialVersionUID = -3727147743147061512L;

	/** Used keys */
	public static final String KEY_CONTENT_TYPE = "KEY_CONTENT_TYPE";
	public static final String KEY_LOCALE = "KEY_LOCALE";
	public static final String KEY_DELIMITER = "KEY_DELIMITER";
	public static final String KEY_ELEVATION_SCALE_FACTOR = "KEY_ELEVATION_SCALE_FACTOR";
	public static final String KEY_COLUMN_LATITUDE_INDEX = "KEY_COLUMN_LATITUDE_INDEX";
	public static final String KEY_COLUMN_LONGITUDE_INDEX = "KEY_COLUMN_LONGITUDE_INDEX";
	public static final String KEY_COLUMN_ELEVATION_INDEX = "KEY_COLUMN_ELEVATION_INDEX";
	public static final String KEY_ROW_COUNT_TO_SKIP = "KEY_ROW_COUNT_TO_SKIP";
	public static final String KEY_NOT_NUMERIC_COLUMNS = "KEY_NOT_NUMERIC_COLUMNS";
	public static final String KEY_OPTIONAL_COLUMNS = "KEY_OPTIONAL_COLUMNS";

	public static final String KEY_MULTIPLE_VALUES = "KEY_MULTIPLE_VALUES";
	public static final String KEY_VALUE_COLUMNS = "KEY_VALUE_COLUMNS";

	/**
	 * Constructor
	 */
	public FileInfoSettings(ContentType contentType) {
		setContentType(contentType);
	}

	/**
	 * Constructor
	 */
	public FileInfoSettings() {
		super();
	}

	/**
	 * Constructor
	 */
	public FileInfoSettings(Map<String, String> fileSettings) {
		super(fileSettings);
	}

	/**
	 * @return the ContentType
	 */
	public ContentType getContentType() {
		return ContentType.getByName(get(KEY_CONTENT_TYPE));
	}

	/**
	 * @param ContentType to set
	 */
	public void setContentType(ContentType contentType) {
		put(KEY_CONTENT_TYPE, contentType.name());
	}

	/**
	 * @return the {@link #delimiter}
	 */
	public char getDelimiter() {
		var result = getOrDefault(KEY_DELIMITER, ";");
		return result.isEmpty() ? ';' : result.charAt(0);
	}

	/**
	 * @param delimiter the {@link #delimiter} to set
	 */
	public void setDelimiter(char delimiter) {
		put(KEY_DELIMITER, String.valueOf(delimiter));
	}

	/**
	 * @return the {@link #rowCountToSkip}
	 */
	public int getRowCountToSkip() {
		return getInt(KEY_ROW_COUNT_TO_SKIP);
	}

	/**
	 * @param rowCountToSkip the {@link #rowCountToSkip} to set
	 */
	public void setRowCountToSkip(int rowCountToSkip) {
		set(KEY_ROW_COUNT_TO_SKIP, rowCountToSkip);
	}

	/**
	 * @return the {@link #latitudeIndex}
	 */
	public int getLatitudeIndex() {
		return NumberUtils.toInt(get(KEY_COLUMN_LATITUDE_INDEX), 1);
	}

	/**
	 * @return the {@link #latitudeIndex}
	 */
	public int getLatitudeIndex(int defaultValue) {
		return NumberUtils.toInt(get(KEY_COLUMN_LATITUDE_INDEX), defaultValue);
	}

	/**
	 * @param latitudeIndex the {@link #latitudeIndex} to set
	 */
	public void setLatitudeIndex(int latitudeIndex) {
		set(KEY_COLUMN_LATITUDE_INDEX, latitudeIndex);
	}

	/**
	 * @return the {@link #longitudeIndex}
	 */
	public int getLongitudeIndex() {
		return getInt(KEY_COLUMN_LONGITUDE_INDEX);
	}

	/**
	 * @return the {@link #longitudeIndex}
	 */
	public int getLongitudeIndex(int defaultValue) {
		return getInt(KEY_COLUMN_LONGITUDE_INDEX, defaultValue);
	}

	/**
	 * @param longitudeIndex the {@link #longitudeIndex} to set
	 */
	public void setLongitudeIndex(int longitudeIndex) {
		set(KEY_COLUMN_LONGITUDE_INDEX, longitudeIndex);
	}

	/**
	 * @return the {@link #elevationIndex}
	 */
	public int getElevationIndex() {
		return NumberUtils.toInt(get(KEY_COLUMN_ELEVATION_INDEX), 2);
	}

	/**
	 * @return the {@link #elevationIndex}
	 */
	public int getElevationIndex(int defaultValue) {
		return NumberUtils.toInt(get(KEY_COLUMN_ELEVATION_INDEX), defaultValue);
	}

	/**
	 * @param elevationIndex the {@link #elevationIndex} to set
	 */
	public void setElevationIndex(int elevationIndex) {
		set(KEY_COLUMN_ELEVATION_INDEX, elevationIndex);
	}

	/**
	 * Put an integer value
	 */
	public void set(String key, int value) {
		put(key, Integer.toString(value));
	}

	/**
	 * Put an integer value
	 */
	public int getInt(String key) {
		return NumberUtils.toInt(get(key), 0);
	}

	/**
	 * Put an integer value
	 */
	public int getInt(String key, int defaultValue) {
		return NumberUtils.toInt(get(key), defaultValue);
	}

	/**
	 * @return the {@link #locale}
	 */
	public Locale getLocale() {
		String languageTag = get(KEY_LOCALE);
		return languageTag != null ? Locale.forLanguageTag(languageTag) : Locale.US;
	}

	/**
	 * @param locale the {@link #locale} to set
	 */
	public void setLocale(Locale locale) {
		put(KEY_LOCALE, locale.toLanguageTag());
	}

	/**
	 * @return the {@link #elevationScaleFactor}
	 */
	public double getElevationScaleFactor() {
		return Double.parseDouble(getOrDefault(KEY_ELEVATION_SCALE_FACTOR, "1.0"));
	}

	/**
	 * @param elevationScaleFactor the {@link #elevationScaleFactor} to set
	 */
	public void setElevationScaleFactor(double elevationScaleFactor) {
		put(KEY_ELEVATION_SCALE_FACTOR, Double.toString(elevationScaleFactor));
	}

	/**
	 * Add a column that is not numeric
	 */
	public void addNonNumericColumn(String columnName) {
		if (!containsKey(KEY_NOT_NUMERIC_COLUMNS)) {
			put(KEY_NOT_NUMERIC_COLUMNS, columnName);
		} else {
			put(KEY_NOT_NUMERIC_COLUMNS, get(KEY_NOT_NUMERIC_COLUMNS) + "|" + columnName);
		}
	}

	/**
	 * @return all non numeric columns
	 */
	public String[] getNonNumericColumns() {
		var columns = getOrDefault(KEY_NOT_NUMERIC_COLUMNS, "");
		return columns.split("\\|");
	}

	/**
	 * Add a column that is optional
	 */
	public void addOptionalColumn(String columnName) {
		if (!containsKey(KEY_OPTIONAL_COLUMNS)) {
			put(KEY_OPTIONAL_COLUMNS, columnName);
		} else {
			put(KEY_OPTIONAL_COLUMNS, get(KEY_OPTIONAL_COLUMNS) + "|" + columnName);
		}
	}

	/**
	 * @return all optional columns
	 */
	public String[] getOptionalColumns() {
		var columns = getOrDefault(KEY_OPTIONAL_COLUMNS, "");
		return columns.isEmpty() ? new String[0] : columns.split("\\|");
	}

	/**
	 * Indicates if CSV can contains multiple columns of values
	 */
	public void allowMultipleValues(boolean multipleValues) {
		if (multipleValues)
			put(KEY_MULTIPLE_VALUES, String.valueOf(multipleValues));
		else
			remove(KEY_MULTIPLE_VALUES);
	}

	/**
	 * @return true if CSV can contains multiple columns of values
	 */
	public boolean allowsMultipleValues() {
		return containsKey(KEY_MULTIPLE_VALUES);
	}

	/**
	 * @return the set of column indexes where values are present
	 */
	public IntList getValueIndexes() {
		var result = new ArrayIntList();
		if (containsKey(KEY_MULTIPLE_VALUES)) {
			String columns = getOrDefault(KEY_VALUE_COLUMNS, "");
			String[] values = columns.split("\\|");
			for (String value : values) {
				int columnIndex = NumberUtils.toInt(value, -1);
				if (columnIndex >= 0)
					result.add(columnIndex);
			}
		}
		return result;
	}

	/**
	 * Set the list of column indexes where values are present
	 */
	public void setValueIndexes(IntList indexes) {
		if (allowsMultipleValues()) {
			StringBuilder sb = new StringBuilder();
			for (var iter = indexes.iterator(); iter.hasNext();) {
				int index = iter.next();
				if (sb.length() > 0) {
					sb.append("|");
				}
				sb.append(index);
			}
			put(KEY_VALUE_COLUMNS, sb.toString());
		}
	}

}
