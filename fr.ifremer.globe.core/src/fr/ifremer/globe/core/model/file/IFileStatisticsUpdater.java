/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file;

import fr.ifremer.globe.core.model.file.IFileInfo;

/**
 * Service able to upate global statistics on a file
 */
public interface IFileStatisticsUpdater {

	/** Require the computation of statistics and the file uptate */
	void updateStatistics(IFileInfo fileInfo);

}
