package fr.ifremer.globe.core.model.file.pointcloud;

import java.time.temporal.Temporal;
import java.util.Optional;

/**
 * Defines a simple point of point cloud.
 */
public interface IPoint {

	double getLatitude();

	double getLongitude();

	double getElevation();

	/**
	 * Return one of the value loaded from the CSV
	 */
	double getValue(int index);

	/**
	 * Return the number of values for this point.
	 */
	int getValueCount();

	Optional<Temporal> getTime();

	/** Returns the label associated with the point */
	Optional<String> getLabel();

}
