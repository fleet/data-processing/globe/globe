/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file.basic;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.FileUtils;

/**
 * A basic IFileInfoSupplier.
 */
public class BasicFileInfoSupplier<I extends IFileInfo> implements IFileInfoSupplier<I> {

	/** ContentTypes **/
	protected Set<ContentType> contentTypes;
	/** Extension of the file name */
	protected String extension;
	/** Description of the file */
	protected String description;

	/**
	 * Constructor
	 */
	public BasicFileInfoSupplier(String extension, String description, ContentType... contentType) {
		this.extension = extension;
		this.description = description;
		contentTypes = EnumSet.copyOf(Arrays.asList(contentType));
	}

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentTypes() {
		return contentTypes;
	}

	/** {@inheritDoc} */
	@Override
	public List<String> getExtensions() {
		return Collections.singletonList(extension);
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Collections.singletonList(new Pair<>(description, "*." + extension));
	}

	/** {@inheritDoc} */
	@Override
	public Optional<I> getFileInfo(String filePath) {
		return Optional.ofNullable(hasRightExtension(filePath) ? makeFileInfo(filePath) : null);
	}

	/** @return true if file has a correct extension for this supplier */
	public boolean hasRightExtension(String filePath) {
		String fileExtension = FileUtils.getExtension(filePath);
		return fileExtension != null && getExtensions().stream().anyMatch(fileExtension::equalsIgnoreCase);
	}

	/** Instantiates a new IFileInfo */
	@SuppressWarnings("unchecked")
	public I makeFileInfo(String filePath) {
		return (I) new BasicFileInfo(filePath, contentTypes.iterator().next());
	}

}
