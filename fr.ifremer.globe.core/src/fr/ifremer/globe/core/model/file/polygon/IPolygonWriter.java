/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file.polygon;

import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * A polygon writer managing some ContentType
 */
public interface IPolygonWriter {

	/**
	 * Returns the service implementation of IPolygonWriter.
	 */
	static IPolygonWriter grab() {
		return OsgiUtils.getService(IPolygonWriter.class);
	}

	/** Write the polygon in file */
	void save(PolygonFileInfo polygonInfo) throws GException;

	/**
	 * Convert the input vector file to a polygon one in the specified type
	 *
	 * @param filePath openable file with ogr gdal class.
	 * @param targetGdalDriverName name of the target gdal vector driver
	 * @return the resulting polygon file info
	 */
	PolygonFileInfo saveAs(String filePath, String targetFilePath, String targetGdalDriverName) throws GException;

}
