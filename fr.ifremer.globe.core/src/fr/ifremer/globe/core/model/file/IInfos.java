package fr.ifremer.globe.core.model.file;

import java.util.List;

import fr.ifremer.globe.core.model.properties.Property;

/**
 * This interface provides contextual informations that will be displayed by the software in the properties view
 * 
 */
public interface IInfos {

	/**
	 * Returns this layer associative table formed by key / values.
	 * The list returned might or might not be modifiable or a copy of the original list of properties
	 */
	public List<Property<?>> getProperties();


}
