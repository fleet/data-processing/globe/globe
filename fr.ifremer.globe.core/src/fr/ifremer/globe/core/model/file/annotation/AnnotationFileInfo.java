package fr.ifremer.globe.core.model.file.annotation;

import java.util.List;

import org.gdal.ogr.Geometry;
import org.gdal.ogr.ogrConstants;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;

/**
 * FileInfo dedicated to annotation file.
 */
public class AnnotationFileInfo extends BasicFileInfo {

	/** Annotations contained in the file */
	private final List<Annotation> annotations;

	/**
	 * Constructor
	 */
	public AnnotationFileInfo(String sourcefile, List<Annotation> annotations) {
		super(sourcefile, ContentType.GEOJSON_GDAL);
		this.annotations = annotations;
	}

	public List<Annotation> getAnnotations() {
		return annotations;
	}

	/** {@inheritDoc} */
	@Override
	public GeoBox getGeoBox() {
		if (geoBox == null) {
			if (annotations.isEmpty())
				geoBox = GeoBox.EMPTY;
			else {
				for (Annotation annotation : annotations) {
					Geometry geometry = annotation.geometry();
					double[] wesn = new double[4];
					geometry.GetEnvelope(wesn);
					if (geoBox == null)
						geoBox = new GeoBox(wesn[3], wesn[2], wesn[1], wesn[0]);
					else
						geoBox.extend(new GeoBox(wesn[3], wesn[2], wesn[1], wesn[0]));
				}
			}
		}
		return geoBox;
	}

	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();

		int polygon = 0;
		int point = 0;
		for (Annotation annotation : annotations) {
			if (annotation.geometry().GetGeometryType() == ogrConstants.wkbPolygon)
				polygon++;
			else
				point++;
		}

		result.add(Property.build("Annotation count", polygon));
		result.add(Property.build("Marker count", point));

		return result;
	}

}