/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * This class provides event around {@link IFileInfo}.
 */
public class FileInfoEvent {

	/**
	 * Base to build topic strings.
	 */
	public static final String TOPIC_BASE = "fr/ifremer/globe/ui/file";

	/**
	 * UI event topic to inform that some files were selected.
	 *
	 * Associated object : "{@link IFileInfo}...".
	 */
	public static final String EVENT_UI_FILES_SELECTED = TOPIC_BASE + UIEvents.TOPIC_SEP + "selection";

	protected IFileInfo fileInfo;
	protected FileInfoState state;

	/**
	 * Constructor
	 */
	public FileInfoEvent(IFileInfo fileInfo, FileInfoState state) {
		this.fileInfo = fileInfo;
		this.state = state;
	}

	/** Loading State of the file */
	public enum FileInfoState {
		LOADING, // Loading launched
		LOADED, // File completly loaded and layer produced
		UNLOADED, // File unloaded, layers deleted
		RELOADING, // Announce a UNLOADED + LOADED
		REMOVED, // File unloaded, removed from Project Explorer and layers deleted
		ERROR // Error on loading
	}

	/**
	 * @return the {@link #fileInfo}
	 */
	public IFileInfo getfileInfo() {
		return fileInfo;
	}

	/**
	 * @return the {@link #state}
	 */
	public FileInfoState getState() {
		return state;
	}

	@Override
	public String toString() {
		return "FileInfoEvent [fileInfo=" + fileInfo + ", state=" + state + "]";
	}

}
