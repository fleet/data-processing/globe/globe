package fr.ifremer.globe.core.model.file.pointcloud;

import java.util.List;

import fr.ifremer.globe.core.model.file.IFileInfo;

/**
 * Define a {@link IFileInfo} providing {@link IPoint}.
 */
public interface IPointCloudFileInfo extends IFileInfo {

	/**
	 * @return the current {@link IPoint} list.
	 */
	List<? extends IPoint> getPoints();

	/** List of field */
	List<PointCloudFieldInfo> getFieldInfos();

}
