package fr.ifremer.globe.core.model.file.annotation;

import org.gdal.ogr.Geometry;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * An annotation instance made up of geographical points.
 */
public record Annotation(

		/** Label of the annotation */
		String label,

		/** Color of the label. */
		GColor labelColor,

		/** Font of the label. */
		String fontName,

		/** Size of the label. */
		int fontSize,

		/** The shape, point or polygon */
		Geometry geometry,

		/** Indicates the color of the shape's interior. */
		GColor interior,

		/** Indicates the color of the shape's outline. */
		GColor outline,

		/** Indicates the line width (in pixels) used when rendering the shape's outline. */
		int outlineWidth

) {

	/** Custom Constructor */
	public Annotation {
		if (label == null)
			label = "";
		if (labelColor == null)
			labelColor = GColor.LIGHT_GRAY;
		if (fontName == null || fontName.isEmpty())
			fontName = "Arial-PLAIN-20";
		if (interior == null)
			labelColor = GColor.LIGHT_GRAY;
		if (outline == null)
			labelColor = GColor.GRAY;
	}
}