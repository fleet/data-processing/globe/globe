/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.file;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.geo.GeoPoint;

/**
 * Supplier of CDI
 */
public interface ICdiSupplier {

	/** @return the CDI contains in the specified file */
	List<String> getCdis(String filePath);

	/** @return the CDI contains in the specified IInfoStore */
	List<String> getCdis(IFileInfo fileInfo);

	/** @return the CDI at the specified position for the given IInfoStore */
	Optional<String> getCdi(IFileInfo fileInfo, GeoPoint position);

}
