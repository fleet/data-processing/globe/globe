/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file.polygon;

import org.gdal.ogr.Geometry;

import fr.ifremer.globe.core.model.geo.GeoBox;

/**
 * A polygon instance made up of geographical points.
 */
public class BasicPolygon implements IPolygon {

	/** Name of the polygon */
	private String name;
	/** comment on the polygon */
	private String comment = "";

	/** Geometry of the polygon */
	private Geometry geometry;
	/** Surface of the polygon in m² */
	private double surface;
	/** Geobox computed when vertices changed */
	private GeoBox geobox;

	/**
	 * Constructor
	 */
	public BasicPolygon(String name) {
		this.name = name;
		setGeometry(null);
	}

	/**
	 * Constructor
	 */
	public BasicPolygon(String name, Geometry geometry) {
		this.name = name;
		setGeometry(geometry);
	}

	/** {@inheritDoc} */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @return the {@link #geobox}
	 */
	@Override
	public GeoBox getGeobox() {
		return geobox;
	}

	/**
	 * @return the {@link #comment}
	 */
	@Override
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the {@link #comment} to set
	 */
	@Override
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @param name the {@link #name} to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the {@link #surface}
	 */
	@Override
	public double getSurface() {
		return surface;
	}

	/**
	 * @return the {@link #geometry}
	 */
	@Override
	public Geometry getGeometry() {
		return geometry;
	}

	/** {@inheritDoc} */
	@Override
	public void setGeometry(Geometry geometry) {
		if (geometry == null || geometry.IsEmpty()) {
			this.geometry = IPolygon.makeEmptyGeometry();
			geobox = GeoBox.EMPTY;
			surface = 0d;
		} else {
			geometry.CloseRings();
			this.geometry = geometry;
			double[] wesn = new double[4];
			geometry.GetEnvelope(wesn);
			geobox = new GeoBox(wesn[3], wesn[2], wesn[1], wesn[0]);
			surface = IPolygon.computeSurface(this.geometry);
		}
	}

}
