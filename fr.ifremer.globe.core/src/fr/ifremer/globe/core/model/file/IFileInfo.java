package fr.ifremer.globe.core.model.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;

import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;

/**
 * An abstract representation of file pathname
 */
public interface IFileInfo extends IInfos {

	/** Returns the pathname string of this file */
	String getPath();

	/** Content type of the file */
	default ContentType getContentType() {
		return ContentType.UNDEFINED;
	}

	/** Returns the canonical pathname (absolute and unique on the system) on the string of this file */
	default String getAbsolutePath() {
		try {
			return toFile().getCanonicalPath();
		} catch (IOException e) {
			return toFile().getAbsolutePath();
		}
	}

	/**
	 * @return The extension denoted by this pathname
	 */
	default String getExtension() {
		return FilenameUtils.getExtension(getPath());
	}

	/**
	 * @return The name (basename + extension) of the file denoted by this pathname
	 */
	default String getFilename() {
		return FilenameUtils.getName(getPath());
	}

	/**
	 * @return The base name, minus the full path and extension
	 */
	default String getBaseName() {
		return FilenameUtils.getBaseName(getPath());
	}

	/**
	 * @return The pathname string of this pathname's parent (directory)
	 */
	default String getParent() {
		return FilenameUtils.getFullPath(getPath());
	}

	/**
	 * @return The the base name, minus the full path and extension
	 */
	default File toFile() {
		return new File(getPath());
	}

	/**
	 * @return The the base name, minus the full path and extension
	 */
	default Path toPath() {
		return FileSystems.getDefault().getPath(getPath());
	}

	/**
	 * @return data bounding box as GeoBox : may be null for non geographic data.
	 */
	default GeoBox getGeoBox() {
		return null;
	}

	/**
	 * @return start/end dates if exist for the file.
	 */
	default Optional<Pair<Instant, Instant>> getStartEndDate() {
		return Optional.empty();
	}

	/**
	 * @return an IFileInfo for the path with an undefined ContentType
	 */
	static IFileInfo empty(String path) {
		return new BasicFileInfo(path, ContentType.UNDEFINED);
	}
}