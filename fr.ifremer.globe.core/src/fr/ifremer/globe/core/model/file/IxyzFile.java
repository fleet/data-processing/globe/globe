package fr.ifremer.globe.core.model.file;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.primitives.IntList;

public interface IxyzFile {

	String DEPTH = "Depth";

	String LONGITUDE = "Longitude";

	String LATITUDE = "Latitude";

	/**
	 * return the file path of the file
	 */
	String getPath();

	void setDelimiter(char text);

	char getDelimiter();

	void setHeaderIndex(Map<String, Integer> headerIndex);

	Map<String, Integer> getHeaderIndex();

	boolean isDepthPositiveBelowSurface();

	void setDepthPositiveBelowSurface(boolean value);

	/** List of column names that must be present in the file */
	List<String> getMandatoryHeader();

	/**
	 * List of column names that may be present in the file.<br>
	 * By default, there is no optional column
	 */
	default List<String> getOptionalHeader() {
		return List.of();
	}

	void setStartingLine(int value);

	int getStartingLine();

	void setDecimalPointLabel(char s);

	char getDecimalPoint();

	/**
	 * @return {@link Integer#getClass()}, {@link Double#getClass()} or {@link String#getClass()} depending on the
	 *         content type of the column
	 */
	Class<?> getType(String headerName);

	/**
	 * Type of header changed
	 *
	 * @param type {@link Integer#getClass()}, {@link Double#getClass()} or {@link String#getClass()}
	 */
	void setType(String headerName, Class<?> type);

	/** Indicates if the CSV file contains multiple columns with values */
	boolean allowsMultipleValues();

	/** Set the indexes of columns containing values */
	void setValueIndexes(IntList indexes);

}
