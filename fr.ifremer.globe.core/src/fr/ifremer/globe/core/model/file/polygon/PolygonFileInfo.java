package fr.ifremer.globe.core.model.file.polygon;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;

/**
 * FileInfo dedicated to polygon file.
 *
 */
public class PolygonFileInfo extends BasicFileInfo {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(PolygonFileInfo.class);

	/** Polygons contained in the file */
	private final List<IPolygon> polygons = new LinkedList<>();

	/** Polygon contained in the file */
	private String gdalDriverName;

	/**
	 * Constructor
	 */
	public PolygonFileInfo(String sourcefile) {
		this(sourcefile, ContentType.POLYGON_GDAL);
	}

	/**
	 * Constructor
	 */
	public PolygonFileInfo(String sourcefile, ContentType contentType) {
		super(sourcefile, contentType);
	}

	/**
	 * @see fr.ifremer.globe.core.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();
		var polygonProps = Property.build("Polygons (" + polygons.size() + ")", "");
		result.add(polygonProps);
		polygons.stream()//
				.map(p -> Property.build(p.getName(), p.getGeometry().GetPointCount() + " vertices"))//
				.forEach(polygonProps::add);
		return result;
	}

	/**
	 * @return the GDAL driver name. Can be null if the file format has not yet been determined.
	 */
	public String getGdalDriverName() {
		return gdalDriverName;
	}

	/**
	 * @param gdalDriverName the {@link #gdalDriverName} to set
	 */
	public void setGdalDriverName(String gdalDriverName) {
		this.gdalDriverName = gdalDriverName;
	}

	/**
	 * @param path the {@link #path} to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the {@link #polygons}
	 */
	public List<IPolygon> getPolygons() {
		return polygons;
	}

	/** {@inheritDoc} */
	@Override
	public GeoBox getGeoBox() {
		GeoBox result = null;
		if (polygons.isEmpty()) {
			result = GeoBox.EMPTY;
		} else {
			GeoBox geobox = polygons.get(0).getGeobox();
			polygons.forEach(p -> geobox.extend(p.getGeobox()));
			result = geobox;
		}
		return result;
	}

}