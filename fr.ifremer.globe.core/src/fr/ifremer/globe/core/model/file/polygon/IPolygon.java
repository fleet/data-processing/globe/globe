/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file.polygon;

import org.gdal.ogr.Geometry;
import org.gdal.ogr.ogrConstants;
import org.gdal.osr.SpatialReference;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;

/**
 * A polygon instance made up of geographical points.
 */
public interface IPolygon {

	/** Name of the polygon */
	String getName();

	/**
	 * Set the name
	 */
	void setName(String name);

	/** @return the Geometry */
	Geometry getGeometry();

	/** Set a new list of vertex. Expect a Geometry on type wkbPolygon */
	void setGeometry(Geometry geometry);

	/** Compute the Geobox of this polygon */
	GeoBox getGeobox();

	/** Get the surface of the polygon in m² */
	double getSurface();

	/**
	 * @return the comment
	 */
	String getComment();

	/**
	 * Set the comment
	 */
	void setComment(String comment);

	/** Create an linear ring Geometry. Use Geometry.AddPoint_2D to set the LongLat position */
	static Geometry makeEmptyGeometry() {
		Geometry geometry = new Geometry(ogrConstants.wkbLinearRing);
		geometry.AssignSpatialReference(GdalOsrUtils.SRS_WGS84);
		return geometry;
	}

	/**
	 * Create an polygon Geometry with the specified linear ring Geometry. This kind of Geometry is accepted by
	 * setGeometry
	 */
	static Geometry makePolygon(Geometry linearRing) {
		Geometry polygon = new Geometry(ogrConstants.wkbPolygon);
		polygon.AssignSpatialReference(GdalOsrUtils.SRS_WGS84);
		if (linearRing != null) {
			polygon.AddGeometry(linearRing);
			polygon.CloseRings();
		}
		return polygon.Simplify(0d).GetGeometryRef(0);
	}

	/** Compute the surface of the polygon in m² */
	static double computeSurface(Geometry geometry) {
		if (geometry != null && !geometry.IsEmpty()) {
			SpatialReference mercatorSrs = new SpatialReference();
			double[] lonlat = geometry.GetPoint(0);
			mercatorSrs.SetMercator(lonlat[1], lonlat[0], 1d, 0d, 0d);
			Geometry projectedGeometry = geometry.Clone();
			if (projectedGeometry != null) {
				projectedGeometry.CloseRings();
				if (projectedGeometry.TransformTo(mercatorSrs) == GdalUtils.OK) {
					double result = projectedGeometry.GetArea();
					projectedGeometry.delete();
					return result;
				}
			}
		}
		return 0d;
	}

}