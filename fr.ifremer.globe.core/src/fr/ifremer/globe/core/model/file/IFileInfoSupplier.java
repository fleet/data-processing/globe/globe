/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.file;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;

/**
 * Service able to supply some informations on a specific kind of file
 */
public interface IFileInfoSupplier<I extends IFileInfo> {

	/** @return the list of managed file type */
	Set<ContentType> getContentTypes();

	/** @return the file extensions supported by this supplier */
	List<String> getExtensions();

	/**
	 * @return the file extensions supported by this Supplier for the specific ContentType <br>
	 *         By default, suppose that this supplier supportes only one ContentType
	 */
	default List<String> getExtensions(ContentType contentType) {
		return getExtensions();
	}

	/** @return the filtering Pair (Label/extension) supported by this Supplier */
	List<Pair<String, String>> getFileFilters();

	/**
	 * @return the filtering Pair supported by this Supplier for the specific ContentType <br>
	 *         By default, suppose that this supplier supportes only one ContentType
	 */
	default List<Pair<String, String>> getFileFilters(ContentType contentType) {
		return getFileFilters();
	}

	/**
	 * @return the content type corresponding to the specified file. UNDEFINED when this IFileInfoSupplier is not able
	 *         to manage this file
	 */
	default ContentType getContentType(String filePath) {
		Optional<I> infoStore = getFileInfo(filePath);
		ContentType result = infoStore.map(IFileInfo::getContentType).orElse(ContentType.UNDEFINED);
		if (infoStore.isPresent() && infoStore.get() instanceof AutoCloseable fileInfo) {
			try {
				fileInfo.close();
			} catch (Exception e) {
				// logger.debug("Can't close {}", filePath);
			}
		}
		return result;
	}

	/**
	 * @return the IFileInfo corresponding to the specified file. Optional.empty when this IFileInfoSupplier is not able
	 *         to manage this file
	 */
	Optional<I> getFileInfo(String filePath);

	/**
	 * @return the settings to use for parsing the file or {@code Optional.empty()} if none
	 */
	default Optional<FileInfoSettings> evaluateSettings(String filePath) {
		return Optional.empty();
	}

	/**
	 * Specific method called only when loading the session.<br>
	 * No additional information will be asked to the user
	 *
	 * @return the IFileInfo corresponding to the specified file. Optional.empty when this IFileInfoSupplier is not able
	 *         to manage this file
	 */
	default Optional<I> getFileInfoSilently(String filePath) {
		return getFileInfo(filePath);
	}

	/**
	 * @return the IFileInfo corresponding to the specified file using the specified settings
	 */
	default Optional<I> getFileInfo(String filePath, FileInfoSettings settings) {
		return Optional.empty();
	}

	/** @return the layer names contained in this file */
	default List<String> getLayers(String filePath) {
		return Collections.emptyList();
	}

	/**
	 * Returns this file associative table formed by key / values to the specified position.
	 * 
	 * @return Optional.empty() by default : no information at this position for this file
	 */
	default Optional<List<Property<?>>> getProperties(IFileInfo fileInfo, GeoPoint position) {
		return Optional.empty();
	}

	/**
	 * Key used in a IFileInfoSupplier definition to identify the supplier’s priority. <br>
	 * By default, priority is 1. The lowest priority is Integer.MAX_VALUE
	 */
	String PROPERTY_PRIORITY = "PROPERTY_PRIORITY";

}
