/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file;

import java.util.Optional;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.AbstractModelPublisher;
import fr.ifremer.globe.core.model.file.FileInfoEvent.FileInfoState;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

/**
 * Model of loaded IFileInfo in Globe<br>
 * This the cache of all files opened in Globe
 */
@Creatable
@Singleton
public class FileInfoModel extends AbstractModelPublisher<String, IFileInfo, FileInfoEvent> {

	/** Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(FileInfoModel.class);

	private final ISessionService sessionService;

	/**
	 * Constructor
	 */
	@Inject
	public FileInfoModel(ISessionService sessionService) {
		this.sessionService = sessionService;
	}

	@Override
	protected String getKey(IFileInfo fileInfo) {
		return fileInfo.getPath();
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<FileInfoEvent> newAddEvent(IFileInfo fileInfo) {
		return Optional.of(new FileInfoEvent(fileInfo, FileInfoState.LOADING));
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<FileInfoEvent> newUpdateEvent(IFileInfo fileInfo) {
		return Optional.of(new FileInfoEvent(fileInfo, FileInfoState.LOADED));
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<FileInfoEvent> newRemovedEvent(IFileInfo fileInfo) {
		return Optional.of(new FileInfoEvent(fileInfo, FileInfoState.REMOVED));
	}

	public void update(IFileInfo fileInfo, FileInfoState state) {
		if (state == FileInfoState.UNLOADED) {
			unload(fileInfo.getPath());
		} else {
			elements.put(fileInfo.getPath(), fileInfo);
			submit(new FileInfoEvent(fileInfo, state));
		}
	}

	/** Remove the FileInfo from the model but notify as UNLOADED */
	public void unload(String filePath) {
		IFileInfo fileInfo = elements.remove(filePath);
		if (fileInfo != null) {
			submit(new FileInfoEvent(fileInfo, FileInfoState.UNLOADED));
			close(fileInfo);
		} else {
			// submit event even if fileInfo not found
			submit(new FileInfoEvent(new BasicFileInfo(filePath), FileInfoState.UNLOADED));
		}
	}

	/**
	 * Remove the FileInfo from the model but notify as RELOADING
	 * 
	 * @param cleanSession true to remove session parameters of the files
	 */
	public void reload(String filePath, FileInfoState newFileState) {
		// Clean session before reloading
		sessionService.getPropertiesContainer().remove(filePath);
		IFileInfo fileInfo = elements.remove(filePath);
		if (fileInfo != null) {
			submit(new FileInfoEvent(fileInfo, newFileState));
			close(fileInfo);
		}
	}

	/** close file if necessary */
	private void close(IFileInfo fileInfo) {
		if (fileInfo instanceof AutoCloseable autoCloseable) {
			try {
				autoCloseable.close();
			} catch (Exception e) {
				LOGGER.debug("Can't close {}", fileInfo.getPath());
			}
		}
	}

	/**
	 * Remove an element from the cache
	 * 
	 * @return
	 */
	@Override
	public Optional<IFileInfo> remove(String filePath) {
		var optRemovedFileInfo = super.remove(filePath);
		optRemovedFileInfo.ifPresent(removedFileInfo -> {
			newRemovedEvent(removedFileInfo).ifPresent(this::submit);
			close(removedFileInfo);
		});
		return optRemovedFileInfo;
	}

}
