/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.file;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier.SpatialResolution;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Services for files managed by Globe
 */
public interface IFileService {

	/**
	 * Returns the service implementation of IFileService.
	 */
	static IFileService grab() {
		return OsgiUtils.getService(IFileService.class);
	}

	/** @return the content type corresponding to the specified file */
	ContentType getContentType(String filePath);

	/** @return the IFileInfo corresponding to the specified file */
	Optional<IFileInfo> getFileInfo(String filePath);

	/**
	 * @return a list of {@link IFileInfo} corresponding to the specified file paths
	 **/
	default List<IFileInfo> getFileInfos(List<String> filePaths) {
		return filePaths.stream().map(this::getFileInfo).filter(Optional::isPresent).map(Optional::get)
				.collect(Collectors.toList());
	}

	/** @return the ISounderNcInfo for the given file */
	default Optional<ISounderNcInfo> getSounderNcInfo(File file) {
		return getFileInfo(file.getAbsolutePath()).filter(ISounderNcInfo.class::isInstance)
				.map(ISounderNcInfo.class::cast);
	}

	/**
	 * @return the IFileInfo corresponding to the specified file.
	 * @param force true to get a new IFileInfo even if the file has already been loaded with a corresponding IFileInfo
	 *            stored in model. Usefull when we know the file content has changed...
	 */
	Optional<IFileInfo> getFileInfo(String filePath, boolean force);

	/**
	 * The silent mode prevents dialog messages. No additional information will be asked to the user
	 *
	 * @return the IFileInfo corresponding to the specified file
	 *
	 */
	Optional<IFileInfo> getFileInfoSilently(String filePath);

	/**
	 * @return Ask all IFileInfoSuppliers if a FileInfoSettings is possible for the specified filepath
	 */
	Optional<FileInfoSettings> evaluateSettings(String filePath);

	/**
	 * @return the IFileInfo corresponding to the specified file using the specified settings
	 */
	Optional<IFileInfo> getFileInfo(String filePath, FileInfoSettings settings);

	/** @return the file extensions corresponding to the specified file type */
	List<String> getExtensions(ContentType contentType);

	/**
	 * @return the filtering Pair (Label/extension) corresponding to the specified file type
	 */
	List<Pair<String, String>> getFileFilters(ContentType contentType);

	/**
	 * @return the filtering Pair (Label/extension) corresponding to the specified file types
	 */
	List<Pair<String, String>> getFileFilters(ContentType... contentTypes);

	/**
	 * @return the filtering Pair (Label/extension) corresponding to the specified file types
	 */
	List<Pair<String, String>> getFileFilters(Collection<ContentType> contentTypes);

	/** @return the layer names contained in this file */
	List<String> getLayers(String filePath);

	/** @return the Projection contained in the file */
	Optional<Projection> getProjection(IFileInfo fileInfo);

	/** @return the GeoBox contained in the file */
	Optional<GeoBox> getGeoBox(IFileInfo fileInfo);

	/** @return the Projection contained in the file */
	Optional<SpatialResolution> getSpatialResolution(IFileInfo fileInfo);

	/**
	 * Returns this file associative table formed by key / values to the specified position.
	 * 
	 * @return Optional.empty() by default : no information at this position for this file
	 */
	Optional<List<Property<?>>> getProperties(IFileInfo fileInfo, GeoPoint position);

	/** Require the computation of statistics and the file uptate */
	void updateStatistics(IFileInfo fileInfo);

	/** @return true if at least one of provided files is booked */
	boolean isBooked(List<String> filePaths);

}
