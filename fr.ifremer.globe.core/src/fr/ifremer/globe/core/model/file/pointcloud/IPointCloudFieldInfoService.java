/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file.pointcloud;

import java.util.List;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Service able to produce some PointCloudFieldInfo on a specific IFileInfo
 */
public interface IPointCloudFieldInfoService {

	/**
	 * Returns the service implementation of IPointCloudFieldInfoService.
	 */
	static IPointCloudFieldInfoService grab() {
		return OsgiUtils.getService(IPointCloudFieldInfoService.class);
	}

	/** Supply all PointCloudFieldInfo for the specified IFileInfo. Empty list when none */
	List<PointCloudFieldInfo> supplyPointCloudFieldInfos(IFileInfo fileInfo);
}
