/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file.pointcloud;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.function.IntFunction;

/**
 * Field informations of a set of point present in a PointCloud file.
 */
public record PointCloudFieldInfo(

		/** Name of the field. Is the column's header if present in the CSV file */
		String name,

		/** Index of the column in the CSV file. Useful to retrieve values in IPoint */
		int columnIndex,

		/** Statistics */
		double minValue, double maxValue) {

	/** Browse all points, compute statistics and produce the list of fields */
	public static List<PointCloudFieldInfo> gatherFieldInfos(List<? extends IPoint> points,
			IntFunction<String> fieldBaptizer) {
		var result = new ArrayList<PointCloudFieldInfo>();
		int fieldInfoCount = points.isEmpty() ? 0 : points.get(0).getValueCount();

		if (fieldInfoCount == 0)
			// At least generates one field to draw all points with the same color
			result.add(new PointCloudFieldInfo("Default", -1, 0d, 0d));

		for (int i = 0; i < fieldInfoCount; i++) {
			int valueIndex = i;
			DoubleSummaryStatistics s = points.stream()//
					.mapToDouble(point -> point.getValue(valueIndex)) //
					.filter(Double::isFinite) //
					.collect(//
							DoubleSummaryStatistics::new, //
							DoubleSummaryStatistics::accept, //
							DoubleSummaryStatistics::combine);
			if (Double.isFinite(s.getMin()) && Double.isFinite(s.getMax()))
				result.add(
						new PointCloudFieldInfo(fieldBaptizer.apply(valueIndex), valueIndex, s.getMin(), s.getMax()));
			else
				// No valid values
				result.add(new PointCloudFieldInfo(fieldBaptizer.apply(valueIndex), valueIndex, 0d, 0d));
		}
		return result;
	}

}
