/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.file.impl;

import static fr.ifremer.globe.core.model.file.ContentType.UNDEFINED;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.IFileStatisticsUpdater;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier.SpatialResolution;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.utils.Pair;
import jakarta.inject.Inject;

/**
 * Some services for getting managed files informations.
 */
@Component(name = "globe_model_service_file", service = IFileService.class)
public class FileService implements IFileService {

	/** Session service */

	@Reference
	private ISessionService sessionService;

	@Inject
	private FileInfoModel fileInfoModel;

	@Inject
	private IDataContainerFactory dataContainerFactory;

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(FileService.class);

	/** All registered suppliers */
	private List<IFileInfoSupplier<? extends IFileInfo>> fileInfoSuppliers = new ArrayList<>();
	/** Temporary list to store all registered supplier in order to sort them when activate the component */
	private List<Pair<IFileInfoSupplier<? extends IFileInfo>, Integer>> tmpFileInfoSuppliers = new ArrayList<>();

	/** All registered geographic suppliers */
	private List<IGeographicInfoSupplier> geographicInfoSuppliers = new ArrayList<>();

	/** All registered statistics updater */
	private List<IFileStatisticsUpdater> fileStatisticsUpdaters = new ArrayList<>();

	@Activate
	private void postConstruct() {
		tmpFileInfoSuppliers
				.sort(Comparator.comparing(Pair<IFileInfoSupplier<? extends IFileInfo>, Integer>::getSecond));
		fileInfoSuppliers = tmpFileInfoSuppliers.stream().map(Pair::getFirst).collect(Collectors.toList());
		// The list is not necessary any more
		tmpFileInfoSuppliers = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType getContentType(String filePath) {
		Optional<IFileInfo> loadedFileInfo = fileInfoModel.get(filePath);
		if (loadedFileInfo.isPresent())
			return loadedFileInfo.get().getContentType();

		return fileInfoSuppliers.stream().//
				map(supplier -> supplier.getContentType(filePath)).//
				filter(contentType -> contentType != UNDEFINED).//
				findFirst().orElse(UNDEFINED);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath) {
		return getFileInfo(filePath, false);
	}

	@Override
	public Optional<IFileInfo> getFileInfo(String filePath, boolean force) {
		// fileInfoModel may be null in unitary test
		Optional<IFileInfo> result = !force && fileInfoModel != null ? fileInfoModel.get(filePath) : Optional.empty();
		if (result.isEmpty()) {
			// Retrieve the file setting if any
			Map<String, String> fileSettings = sessionService.getPropertiesContainer().get(filePath,
					FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS);
			if (fileSettings.isEmpty()) {
				result = fileInfoSuppliers.stream().//
						flatMap(supplier -> supplier.getFileInfo(filePath).stream()).//
						findFirst().//
						map(IFileInfo.class::cast);
			} else {
				// Settings exists : load file with them
				FileInfoSettings fileInfoSettings = new FileInfoSettings(fileSettings);
				result = getFileInfo(filePath, fileInfoSettings);
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<IFileInfo> getFileInfoSilently(String filePath) {
		Optional<IFileInfo> result = fileInfoModel != null ? fileInfoModel.get(filePath) : Optional.empty();
		if (result.isEmpty()) {
			// Retrieve the file setting if any
			Map<String, String> fileSettings = sessionService.getPropertiesContainer().get(filePath,
					FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS);
			if (fileSettings.isEmpty()) {
				result = fileInfoSuppliers.stream().//
						map(supplier -> supplier.getFileInfoSilently(filePath)).//
						filter(Optional::isPresent).//
						findFirst().//
						orElse(Optional.empty()).//
						map(IFileInfo.class::cast);
			} else {
				// Settings exists : load file with them
				FileInfoSettings fileInfoSettings = new FileInfoSettings(fileSettings);
				result = getFileInfo(filePath, fileInfoSettings);
			}
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		Optional<? extends IFileInfo> fileInfo = fileInfoSuppliers.stream().//
				map(supplier -> supplier.getFileInfo(filePath, settings)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
		// Save the settings in the session
		if (fileInfo.isPresent()) {
			sessionService.getPropertiesContainer().add(filePath, FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS,
					settings);
			return fileInfo.map(IFileInfo.class::cast);
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<FileInfoSettings> evaluateSettings(String filePath) {
		return fileInfoSuppliers.stream().//
				map(supplier -> supplier.evaluateSettings(filePath)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getExtensions(ContentType contentType) {
		return fileInfoSuppliers.stream().//
				filter(supplier -> supplier.getContentTypes().contains(contentType)).//
				map(supplier -> supplier.getExtensions(contentType)).//
				filter(Objects::nonNull).//
				filter(list -> !list.isEmpty()).//
				findFirst().//
				orElse(Collections.emptyList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<String, String>> getFileFilters(ContentType contentType) {
		return fileInfoSuppliers.stream().//
				filter(supplier -> supplier.getContentTypes().contains(contentType)).//
				map(IFileInfoSupplier::getFileFilters).//
				filter(Objects::nonNull).//
				filter(list -> !list.isEmpty()).//
				findFirst().//
				orElse(Collections.emptyList());
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters(ContentType... contentTypes) {
		return getFileFilters(Arrays.stream(contentTypes));
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters(Collection<ContentType> contentTypes) {
		return getFileFilters(contentTypes.stream());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getLayers(String filePath) {
		return fileInfoSuppliers.stream().//
				map(supplier -> supplier.getLayers(filePath)).//
				filter(Objects::nonNull).//
				filter(list -> !list.isEmpty()).//
				findFirst().//
				orElse(Collections.emptyList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Projection> getProjection(IFileInfo fileInfo) {
		return geographicInfoSuppliers.stream().//
				map(supplier -> supplier.getProjection(fileInfo)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<GeoBox> getGeoBox(IFileInfo fileInfo) {
		return geographicInfoSuppliers.stream().//
				map(supplier -> supplier.getGeoBox(fileInfo)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<SpatialResolution> getSpatialResolution(IFileInfo fileInfo) {
		return geographicInfoSuppliers.stream().//
				map(supplier -> supplier.getSpatialResolution(fileInfo)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<List<Property<?>>> getProperties(IFileInfo fileInfo, GeoPoint position) {
		return fileInfoSuppliers.stream().//
				map(supplier -> supplier.getProperties(fileInfo, position)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/** {@inheritDoc} */
	@Override
	public void updateStatistics(IFileInfo fileInfo) {
		fileStatisticsUpdaters.stream().forEach(updater -> updater.updateStatistics(fileInfo));
	}

	@Override
	public boolean isBooked(List<String> filePaths) {
		return filePaths.stream().map(fileInfoModel::get)//
				.filter(Optional::isPresent)//
				.map(Optional::get)//
				.filter(IDataContainerInfo.class::isInstance)//
				.map(IDataContainerInfo.class::cast)//
				.anyMatch(dataContainerFactory::isBooked);
	}

	private List<Pair<String, String>> getFileFilters(Stream<ContentType> contentTypes) {
		List<String> extensions = contentTypes.//
				flatMap(contentType -> getExtensions(contentType).stream()).//
				map(extension -> "*." + extension). //
				collect(Collectors.toList());
		return extensions.isEmpty() ? Collections.emptyList()
				: Collections.singletonList(new Pair<>("Accepted files", StringUtils.join(extensions, ';')));
	}

	/** Accept a new IRasterReaderFactory provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	private void addRasterInfoSupplier(IFileInfoSupplier<IFileInfo> fileInfoSupplier, Map<String, Object> properties) {
		logger.debug("{} registered : {}", IFileInfoSupplier.class.getSimpleName(),
				fileInfoSupplier.getClass().getName());
		Integer priority = (Integer) properties.getOrDefault(FileInfoSupplierPriority.PROPERTY_PRIORITY,
				Integer.valueOf(FileInfoSupplierPriority.DEFAULT_PRIORITY));
		tmpFileInfoSuppliers.add(new Pair<>(fileInfoSupplier, priority));
	}

	/** Accept a new IGeographicInfoSupplier provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	private void addGeographicInfoSupplier(IGeographicInfoSupplier geographicInfoSupplier) {
		logger.debug("{} registered : {}", IGeographicInfoSupplier.class.getSimpleName(),
				geographicInfoSupplier.getClass().getName());
		geographicInfoSuppliers.add(geographicInfoSupplier);
	}

	/** Accept a new IFileStatisticsUpdater provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	private void addFileStatisticsUpdater(IFileStatisticsUpdater fileStatisticsUpdater) {
		logger.debug("{} registered : {}", IFileStatisticsUpdater.class.getSimpleName(),
				fileStatisticsUpdater.getClass().getName());
		fileStatisticsUpdaters.add(fileStatisticsUpdater);
	}

}