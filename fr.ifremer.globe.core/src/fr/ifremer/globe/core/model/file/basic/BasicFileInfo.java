/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.file.basic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;

/**
 * A basic IFileInfo.
 */
public class BasicFileInfo implements IFileInfo {

	/** Path of the file */
	protected String path;

	/** Content type of the file */
	protected ContentType contentType;

	/** Geobox of the file. May be null */
	protected GeoBox geoBox;

	/**
	 * Constructor
	 */
	public BasicFileInfo(String path, ContentType contentType) {
		this.path = path;
		this.contentType = contentType;
	}

	/**
	 * Constructor
	 */
	public BasicFileInfo(String path) {
		this(path, ContentType.UNDEFINED);
	}

	/**
	 * Constructor
	 */
	public BasicFileInfo(File file) {
		this(file.getPath());
	}

	/** {@inheritDoc} */
	@Override
	public String getPath() {
		return path;
	}

	/** {@inheritDoc} */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = new ArrayList<>();
		properties.add(Property.build("File size", FileUtils.byteCountToDisplaySize(toFile().length())));
		GeoBox g = getGeoBox();
		if (g != null) {
			properties.addAll(Property.build(g));
		}
		return properties;
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return contentType;
	}

	/**
	 * @return the {@link #geoBox}
	 */
	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/**
	 * @param geoBox the {@link #geoBox} to set
	 */
	public void setGeoBox(GeoBox geoBox) {
		this.geoBox = geoBox;
	}

}
