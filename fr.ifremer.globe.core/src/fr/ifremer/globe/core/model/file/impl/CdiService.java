/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.file.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import fr.ifremer.globe.core.model.file.ICdiService;
import fr.ifremer.globe.core.model.file.ICdiSupplier;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoPoint;

/**
 * Implementation of ICdiService.
 */
@Component(name = "globe_model_service_cdi", service = ICdiService.class)
public class CdiService implements ICdiService {

	/** All registered suppliers */
	protected List<ICdiSupplier> cdiSuppliers = new ArrayList<>();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCdis(String filePath) {
		return cdiSuppliers.stream().//
				map(supplier -> supplier.getCdis(filePath)).//
				filter(Objects::nonNull).//
				filter(list -> !list.isEmpty()).//
				findFirst().//
				orElse(Collections.emptyList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCdis(IFileInfo fileInfo) {
		return cdiSuppliers.stream().//
				map(supplier -> supplier.getCdis(fileInfo)).//
				filter(Objects::nonNull).//
				filter(list -> !list.isEmpty()).//
				findFirst().//
				orElse(Collections.emptyList());
	}

	@Override
	public Optional<String> getCdi(IFileInfo fileInfo, GeoPoint position) {
		return cdiSuppliers.stream().//
				map(supplier -> supplier.getCdi(fileInfo, position).orElse(null)).//
				filter(Objects::nonNull).//
				findFirst();
	}

	/** Accept a new ICdiSupplier provided by the driver bundles */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addRasterInfoSupplier(ICdiSupplier cdiSupplier) {
		cdiSuppliers.add(cdiSupplier);
	}

}