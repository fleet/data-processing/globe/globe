package fr.ifremer.globe.core.model.marker.impl;

import java.util.Optional;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerTypology;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Basic implementation of {@link IMarker}
 */
public class BasicMarker implements IMarker {

	/** Marker properties **/
	private final String id;
	private double lat;
	private double lon;
	private double elevation;

	private String comment = "";
	private Optional<IMarkerTypology> typology = Optional.empty();

	private Optional<String> layer = Optional.empty();

	// display properties
	private GColor color = new GColor("#eb4934");
	private String shape = "Sphere";
	private float size = 15;
	private boolean isSelected;
	private boolean isEnabled = true;

	/** Vertical offset to apply to this marker's position */
	private float verticalOffset = 0f;

	/**
	 * Constructor : not used directly, but with {@link MarkerBuilder}
	 **/
	BasicMarker(String id, double lat, double lon, double elevation, String layer) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.elevation = elevation;
		this.layer = Optional.ofNullable(layer);
	}

	// GETTERS & SETTERS

	public BasicMarker(String id) {
		this.id = id;
	}

	@Override
	public String getID() {
		return id;
	}

	@Override
	public double getLat() {
		return lat;
	}

	@Override
	public void setLatitude(double lat) {
		this.lat = lat;
	}

	@Override
	public double getLon() {
		return lon;
	}

	@Override
	public void setLongitude(double lon) {
		this.lon = lon;
	}

	@Override
	public double getElevation() {
		return elevation;
	}

	@Override
	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	@Override
	public String getBathyLayer() {
		return layer.orElse("");
	}

	@Override
	public GColor getColor() {
		return color;
	}

	@Override
	public void setColor(GColor color) {
		this.color = color;
	}

	@Override
	public String getShape() {
		return shape;
	}

	@Override
	public void setShape(String shape) {
		this.shape = shape;
	}

	@Override
	public float getSize() {
		return size;
	}

	@Override
	public void setSize(float pointSize) {
		size = pointSize;
	}

	@Override
	public String getComment() {
		return comment;
	}

	@Override
	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public Optional<IMarkerTypology> getTypology() {
		return typology;
	}

	@Override
	public void setTypology(Optional<IMarkerTypology> typology) {
		this.typology = typology;
	}

	@Override
	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	@Override
	public boolean isSelected() {
		return isSelected;
	}

	@Override
	public void setEnabled(boolean enabled) {
		isEnabled = enabled;
	}

	@Override
	public boolean isEnabled() {
		return isEnabled;
	}

	/**
	 * @return the {@link #verticalOffset}
	 */
	@Override
	public float getVerticalOffset() {
		return verticalOffset;
	}

	/**
	 * @param verticalOffset the {@link #verticalOffset} to set
	 */
	@Override
	public void setVerticalOffset(float verticalOffset) {
		this.verticalOffset = verticalOffset;
	}

}
