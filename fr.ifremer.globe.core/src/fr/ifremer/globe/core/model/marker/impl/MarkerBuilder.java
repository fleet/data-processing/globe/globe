package fr.ifremer.globe.core.model.marker.impl;

import java.time.Instant;
import java.util.Optional;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerTypology;
import fr.ifremer.globe.core.utils.color.GColor;

public class MarkerBuilder {

	// mandatory fields
	private final String id;
	private final double lat;
	private final double lon;
	private final double elevation;
	private String layer;
	private IMarkerTypology typology;
	private String comment = "";

	private GColor color;
	private String shape;
	private float size;

	private String waterColumnLayer;
	private int ping;
	private Optional<Instant> dateTime = Optional.empty();
	private Optional<Double> seeFloorElevation = Optional.empty();

	public MarkerBuilder(String id, double lat, double lon, double elevation) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.elevation = elevation;
	}

	public IMarker build() {
		// instanciate a water column marker if possible, else a simple marker
		IMarker result = waterColumnLayer != null
				? new BasicWCMarker(id, lat, lon, elevation, layer, waterColumnLayer, ping, dateTime, seeFloorElevation)
				: new BasicMarker(id, lat, lon, elevation, layer);

		Optional.ofNullable(color).ifPresent(result::setColor);
		Optional.ofNullable(shape).ifPresent(result::setShape);
		Optional.ofNullable(size).ifPresent(result::setSize);
		result.setTypology(Optional.ofNullable(typology));
		Optional.ofNullable(comment).ifPresent(result::setComment);

		return result;
	}

	// Setters

	public MarkerBuilder setLayer(String layer) {
		this.layer = layer;
		return this;
	}

	public MarkerBuilder setColor(GColor color) {
		this.color = color;
		return this;
	}

	public MarkerBuilder setShape(String shape) {
		this.shape = shape;
		return this;
	}

	public MarkerBuilder setSize(float size) {
		this.size = size;
		return this;
	}

	public MarkerBuilder setComment(String comment) {
		this.comment = comment;
		return this;
	}

	public MarkerBuilder setTypology(IMarkerTypology selectedTypology) {
		this.typology = selectedTypology;
		return this;
	}

	public MarkerBuilder setWaterColumnLayerPing(String waterColumnLayer, int ping) {
		this.waterColumnLayer = waterColumnLayer;
		this.ping = ping;
		return this;
	}

	public MarkerBuilder setDateTime(Instant dateTime) {
		this.dateTime = Optional.of(dateTime);
		return this;
	}

	public MarkerBuilder setDepth(double seeFloorElevation) {
		this.seeFloorElevation = Optional.of(seeFloorElevation);
		return this;
	}

}
