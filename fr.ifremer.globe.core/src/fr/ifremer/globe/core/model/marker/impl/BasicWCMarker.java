package fr.ifremer.globe.core.model.marker.impl;

import java.time.Instant;
import java.util.Optional;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IWCMarker;

/**
 * Basic implementation of {@link IMarker}
 */
public class BasicWCMarker extends BasicMarker implements IWCMarker {

	/** Properties (specific for water column) **/
	private final String waterColumnLayer;
	private final int ping;

	// optional properties
	private final Optional<Instant> dateTime;
	private final Optional<Double> seaFloorElevation;

	/**
	 * Constructor : not used directly, but with {@link MarkerBuilder}
	 **/
	BasicWCMarker(String id, double lat, double lon, double elevation, String layer, //
			String waterColumnLayer, int ping, Optional<Instant> dateTime, Optional<Double> seaFloorElevation) {
		super(id, lat, lon, elevation, layer);
		this.waterColumnLayer = waterColumnLayer;
		this.ping = ping;
		this.dateTime = dateTime;
		this.seaFloorElevation = seaFloorElevation;
	}

	// GETTERS & SETTERS

	@Override
	public String getWaterColumnLayer() {
		return waterColumnLayer;
	}

	@Override
	public int getPing() {
		return ping;
	}

	@Override
	public Optional<Instant> getDateTime() {
		return this.dateTime;
	}

	@Override
	public Optional<Double> getSeaFloorElevation() {
		return seaFloorElevation;
	}

}
