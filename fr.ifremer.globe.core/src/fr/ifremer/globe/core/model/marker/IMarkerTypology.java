package fr.ifremer.globe.core.model.marker;

import java.util.Objects;

public interface IMarkerTypology {

	String getGroup();

	String getClazz();

	default boolean equalsOtherTypology(IMarkerTypology other) {
		if (this == other)
			return true;
		if (other == null)
			return false;
		return Objects.equals(getGroup(), other.getGroup()) && Objects.equals(getClazz(), other.getClazz());
	}

}
