package fr.ifremer.globe.core.model.marker;

import java.util.List;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Define a {@link IFileInfo} providing {@link IMarker}.
 */
public interface IMarkerFileInfo<T extends IMarker> extends IFileInfo {
	
	/**
	 * @return the current {@link IMarker} list.
	 */
	List<T> getMarkers();

	/**
	 * Sets the current {@link IMarker} list.
	 */
	void setMarkers(List<T> markers);

	/**
	 * Saves the current {@link IMarker} list.
	 * 
	 * @throws GIOException
	 */
	void save() throws GIOException;

}
