package fr.ifremer.globe.core.model.marker;

import java.util.Optional;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Interface to define a marker.
 */
public interface IMarker {

	public static final String ELEVATION_LONG_NAME = "Height above sea surface";
	public static final String ALTITUDE_LONG_NAME = "Height above sea floor";
	public static final String SEA_FLOOR_ELEVATION_LONG_NAME = "Sea floor elevation";
	public static final String SEA_FLOOT_LAYER_LONG_NAME = "Sea floor layer";

	/** ID **/
	String getID();

	/** Latitude **/
	double getLat();

	void setLatitude(double degrees);

	/** Longitude **/
	double getLon();

	void setLongitude(double degrees);

	/** Elevation **/
	double getElevation();

	void setElevation(double elevation);

	/** Bathymetry layer **/

	String getBathyLayer();

	/** Color **/

	GColor getColor();

	void setColor(GColor color);

	/** Shape **/
	String getShape();

	void setShape(String shape);

	/** Size **/
	float getSize();

	void setSize(float size);

	/** Comment **/
	String getComment();

	void setComment(String text);

	/** Typology (optional) **/
	Optional<IMarkerTypology> getTypology();

	void setTypology(Optional<IMarkerTypology> typology);

	/** Selection **/
	void setSelected(boolean selected);

	boolean isSelected();

	/** Enabled **/
	void setEnabled(boolean enabled);

	boolean isEnabled();

	/** @return the vertical offset applied to this marker's position */
	float getVerticalOffset();

	/**
	 * Specify a vertical offset to apply to this marker's position . <br>
	 * Useful to synchronize the vertical position with the targeted object (ie WC)
	 */
	void setVerticalOffset(float verticalOffset);

}
