package fr.ifremer.globe.core.model.marker;

import java.time.Instant;
import java.util.Optional;

import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Interface to define a water column marker.
 */
public interface IWCMarker extends IMarker {

	/** Depth layer **/
	String getWaterColumnLayer();

	/** Ping **/
	int getPing();

	/** Date (from ping) **/
	Optional<Instant> getDateTime();

	default String getDateString() {
		return getDateTime().isPresent() ? DateUtils.getDateString(getDateTime().get()) : "";
	}

	default String getTimeString() {
		return getDateTime().isPresent() ? DateUtils.getTimeString(getDateTime().get()) : "";
	}

	/** Depth under marker **/
	Optional<Double> getSeaFloorElevation();

	/** @return if possible, retrun a computed altitude ( = marker_elevation - sea_floor_elevation) **/
	default Optional<Double> getAtltitude() {
		return getSeaFloorElevation().isPresent() ? Optional.of(getElevation() - getSeaFloorElevation().get())
				: Optional.empty();
	}

}
