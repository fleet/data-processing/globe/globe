/**
 * 
 */
package fr.ifremer.globe.core.model;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Allows an editor to provide the "save" action.
 * 
 * @author gbourel
 */
public interface ISaveable {

	/**
	 * Returns whether the contents of this part have changed since the last
	 * save operation.
	 */
	public boolean isDirty();

	/**
	 * Saves currently edited file.
	 */
	public boolean doSave(IProgressMonitor monitor);
}