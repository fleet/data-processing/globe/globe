package fr.ifremer.globe.core.model.attitude;

import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This interface provides methods to get attitude data.
 */
public interface IAttitudeData extends Closeable, Iterable<Integer> {

	@Override
	default void close() throws IOException {
		// by default, nothing to do in close method
	}

	String getFileName();

	/**
	 * @return the number of attitude points.
	 */
	int size();

	/**
	 * @return the roll at the specified index.
	 */
	float getRoll(int index) throws GIOException;

	/**
	 * @return the pitch at the specified index.
	 */
	float getPitch(int index) throws GIOException;

	/**
	 * @return the heading at the specified index.
	 */
	float getHeading(int index) throws GIOException;

	/**
	 * @return the vertical offset at the specified index.
	 */
	float getVerticalOffset(int index) throws GIOException;

	/**
	 * @return the time at the specified index.
	 */
	long getEpochTimeNano(int index) throws GIOException;

	/**
	 * Provides an iterator on each index.
	 *
	 * @see Iterable
	 */
	@Override
	default Iterator<Integer> iterator() {
		return IntStream.range(0, size()).iterator();
	}

	/**
	 * @return a new {@link Stream} on indexes.
	 */
	default Stream<Integer> stream() {
		return StreamSupport.stream(spliterator(), false);
	}

}
