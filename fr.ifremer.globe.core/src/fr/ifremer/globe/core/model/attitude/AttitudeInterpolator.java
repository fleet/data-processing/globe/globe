package fr.ifremer.globe.core.model.attitude;

import java.time.Instant;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class computes interpolated values from a {@link IAttitudeData}.
 */
public class AttitudeInterpolator {

	/** Private fields **/
	private final IAttitudeData attitudeData;
	private final TreeMap<Long, Integer> indexesByTime = new TreeMap<>();

	/**
	 * Constructor
	 */
	public AttitudeInterpolator(IAttitudeData attData) throws GIOException {
		super();
		this.attitudeData = attData;
		// Sort attitude indexes by time
		for (int index : attData) {
			indexesByTime.put(attData.getEpochTimeNano(index), index);
		}
	}

	/** Private interface to get value (allows exception) **/
	private interface ValueProvider {
		float get(int value) throws GIOException;
	}

	/**
	 * Does the interpolation.
	 */
	private float getInterpolatedValue(long time, ValueProvider valueProvider) throws GIOException {
		// Get a position before and after : pass if not found
		Entry<Long, Integer> previous = indexesByTime.floorEntry(time);
		Entry<Long, Integer> next = indexesByTime.ceilingEntry(time);
		if (previous == null)
			return valueProvider.get(next.getValue());
		else if (next == null)
			return valueProvider.get(previous.getValue());

		// Compute interpolated attitudes
		float coeff = !next.getKey().equals(previous.getKey())
				? ((float) (time - previous.getKey())) / (next.getKey() - previous.getKey())
				: 0;

		float previousValue = valueProvider.get(previous.getValue());
		float nextValue = valueProvider.get(next.getValue());

		return previousValue + coeff * (nextValue - previousValue);
	}
	
	private float getInterpolatedValue(Instant time, ValueProvider valueProvider) throws GIOException {
		return getInterpolatedValue(DateUtils.instantToEpochNano(time), valueProvider);
	}

	// PUBLIC API

	public float getRoll(long epochTimeNano) throws GIOException {
		return getInterpolatedValue(epochTimeNano, attitudeData::getRoll);
	}

	public float getRoll(Instant instant) throws GIOException {
		return getInterpolatedValue(instant, attitudeData::getRoll);
	}

	public float getPitch(long epochTimeNano) throws GIOException {
		return getInterpolatedValue(epochTimeNano, attitudeData::getPitch);
	}

	public float getPitch(Instant instant) throws GIOException {
		return getInterpolatedValue(instant, attitudeData::getPitch);
	}

	public float getHeading(long epochTimeNano) throws GIOException {
		return getInterpolatedValue(epochTimeNano, attitudeData::getHeading);
	}

	public float getHeading(Instant instant) throws GIOException {
		return getInterpolatedValue(instant, attitudeData::getHeading);
	}

	public float getVerticalOffset(long epochTimeNano) throws GIOException {
		return getInterpolatedValue(epochTimeNano, attitudeData::getVerticalOffset);
	}

	public float getVerticalOffset(Instant instant) throws GIOException {
		return getInterpolatedValue(instant, attitudeData::getVerticalOffset);
	}

}
