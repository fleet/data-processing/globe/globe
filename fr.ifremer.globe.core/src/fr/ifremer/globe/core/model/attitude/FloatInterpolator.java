package fr.ifremer.globe.core.model.attitude;

import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class computes interpolated values from a {@link FloatLoadableLayer1D}.
 */
public class FloatInterpolator {

	/** Private fields **/
	private final TreeMap<Long, Integer> indexesByTime = new TreeMap<>();
	// Source layer
	private FloatBuffer1D sourceValues;

	/**
	 * Constructor
	 */
	public FloatInterpolator(FloatBuffer1D sourceValues, LongLoadableLayer1D sourceTimes) throws GIOException {
		super();
		this.sourceValues = sourceValues;
		// Sort attitude indexes by time
		long size = sourceTimes.getDimensions()[0];
		for (int i = 0; i < size; i++) {
			indexesByTime.put(sourceTimes.get(i), i);
		}
	}

	/**
	 * Does the interpolation.
	 * 
	 * @param time expected in the same unit as the sourceTime layer
	 */
	public float getInterpolatedValue(long time) throws GIOException {
		// Get a position before and after : pass if not found
		Entry<Long, Integer> previous = indexesByTime.floorEntry(time);
		Entry<Long, Integer> next = indexesByTime.ceilingEntry(time);
		if (previous == null)
			return sourceValues.get(next.getValue());
		else if (next == null)
			return sourceValues.get(previous.getValue());

		// Compute interpolated attitudes
		float coeff = !next.getKey().equals(previous.getKey())
				? (float) (((double) (time - previous.getKey())) / (next.getKey() - previous.getKey()))
				: 0.f;

		float previousValue = sourceValues.get(previous.getValue());
		float nextValue = sourceValues.get(next.getValue());

		return previousValue + coeff * (nextValue - previousValue);
	}
}
