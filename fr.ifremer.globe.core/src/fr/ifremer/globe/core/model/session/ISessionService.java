/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.session;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Session service to retrieve or save information on the session
 */
public interface ISessionService {

	/**
	 * Returns the service implementation of ISessionService.
	 */
	static ISessionService grab() {
		return OsgiUtils.getService(ISessionService.class);
	}

	/** Loads default session file **/
	boolean loadSession();

	void loadSession(File sessionFile) throws FileNotFoundException, IOException, ClassNotFoundException;

	/** Save the session in the default file */
	void saveSession();

	void saveSession(boolean relativePath, File destination) throws FileNotFoundException, IOException;

	/** @return the {@link #propertiesContainer} */
	IFilePropertiesContainer getPropertiesContainer();

	/**
	 * Synchronous way to save all parameters of some files in the session. <br>
	 * Useful when files are about to be deleted and all their parameters cleaned up and therefore inaccessible for the
	 * session
	 */
	void saveFilesParameters(List<String> filePaths);

	/**
	 * Synchronous way to remove all parameters from some files in the session. <br>
	 * Useful when files are about to be reloaded in order to force clean up their settings .
	 */
	void removeFilesParameters(List<String> filePaths);

	/** adds a listener that will be notified when the session is about to be saved. */
	void addPreSaveSessionListener(Runnable listener);

	/** Return true if the session contains parameters for the specified layer */
	boolean doesSessionContainsLayer(String filePath, String wwLayerName);

}
