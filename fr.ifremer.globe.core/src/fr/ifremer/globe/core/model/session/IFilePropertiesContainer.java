package fr.ifremer.globe.core.model.session;

import java.beans.PropertyChangeSupport;
import java.util.Map;

import org.apache.commons.beanutils.Converter;

/**
 * A generic container to store any file properties grouped by realm.
 */
public interface IFilePropertiesContainer {

	/**
	 * Returns the entire set of properties linked to a file and a realm<br>
	 * The resulting map has to be set again to trigger the save of the session
	 */
	Map<String, String> get(String filePath, String realm);

	/**
	 * Monitor a JavaBean object to serialize it in the session as a map of properties<br>
	 * The save of the session will be triggered for each detected property change
	 *
	 * @param filePath file path
	 * @param realm realm of the bean
	 * @param bean object respecting the JavaBean specification
	 * @param pcs PropertyChangeSupport used to listen the changes of the bean
	 */
	void monitor(String filePath, String realm, Object bean, PropertyChangeSupport pcs);

	/**
	 * Change the properties linked to a file and a realm <br>
	 * The save of the session will be triggered
	 */
	void add(String filePath, String realm, Map<String, String> properties);

	/** Remove all the properties linked to a file */
	void remove(String filePath);

	/** Add a converter to converter a class to a property like an Enum */
	void addConverter(Converter converter, Class<?> clazz);
}
