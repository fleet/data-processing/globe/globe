package fr.ifremer.globe.core.model;

import fr.ifremer.globe.core.utils.latlon.CoordinateDisplayType;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;

public class ModelPreferences extends PreferenceComposite {
	private static ModelPreferences instance = new ModelPreferences(
			PreferenceRegistry.getInstance().getGlobalSettingsNode(), "Display");

	private IntegerPreferenceAttribute navigationDecimation;
	private BooleanPreferenceAttribute recompute_beam_widths;
	private EnumPreferenceAttribute gridCoordinate;

	public ModelPreferences(PreferenceComposite superNode, String nodeName) {
		super(superNode, nodeName);

		gridCoordinate = new EnumPreferenceAttribute("10_10_grid_coordinate", "Latitude/longitude format",
				CoordinateDisplayType.DEG_MIN_DEC);
		this.declareAttribute(gridCoordinate);

		//
		navigationDecimation = new IntegerPreferenceAttribute("20_navigation_decimation",
				"Navigation decimation percentage", 30);
		this.declareAttribute(navigationDecimation);

		recompute_beam_widths = new BooleanPreferenceAttribute("30",
				"use recomputed beam widths for water column display (avoid holes)", true);
		this.declareAttribute(recompute_beam_widths);

		this.load();

	}

	public BooleanPreferenceAttribute getRecompute_beam_widths() {
		return recompute_beam_widths;
	}

	public IntegerPreferenceAttribute getNavigationDecimation() {
		return navigationDecimation;
	}

	public EnumPreferenceAttribute getGridCoordinate() {
		return gridCoordinate;
	}

	public static ModelPreferences getInstance() {
		return instance;
	}
}
