/**
 * 
 */
package fr.ifremer.globe.core.model;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Interface for deletable elements.
 * 
 * 
 * 
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */
public interface IDeletable {
	/**
	 * Deletes this instance.
	 * 
	 * @param force
	 *            is true don't ask for confirmation
	 * 
	 * @return <code>true</code> if successfully delete, <code>false</code> if
	 *         deletion canceled or failed.
	 */
	public boolean delete(IProgressMonitor monitor);
}
