package fr.ifremer.globe.core.model;

public interface IConstants {
	/***
	 * Definition des noms de meta-attributs
	 */

	/**
	 * Tree node attributs
	 * 
	 */
	public static final String Path = "Path";
	public static final String Name = "Name";

	/**
	 * Default name for standart Attributes
	 */
	public static final String Depth = "Depth";
	public static final String VerticalDepth = "Vertical Depth";
	public static final String Reflectivity = "Reflectivity";
	public static final String Roll = "Roll";
	public static final String Pitch = "Pitch";
	public static final String Tide = "Tide";
	public static final String Heave = "Heave";
	public static final String Immersion = "Immersion";
	public static final String DyDraught = "Draught";
	public static final String SoundVelocity = "Sound Velocity";
	public static final String Speed = "Speed";
	public static final String Heading = "Heading";
	public static final String Validity = "Validity";
	public static final String PingValidity = "Ping Validity";
	public static final String BeamValidity = "Beam Validity";

	public static final String Temperature = "Temperature";
	public static final String Salinity = "Salinity";

	public static final String Navigation = "Navigation";

	/**
	 * Data-centric meta attributs
	 */
	public static final String NorthLatitude = "North Latitude";
	public static final String SouthLatitude = "South Latitude";
	public static final String EastLongitude = "East Longitude";
	public static final String WestLongitude = "West Longitude";

	public static final String StartDate = "Start Date";
	public static final String StartTime = "Start Time";
	public static final String EndDate = "End Date";
	public static final String EndTime = "End Time";

	public static final String History = "History";
	public static final String Ellipsoid = "Ellipsoid";

	public static final String Projection = "Projection";
	public static final String SpatialResolutionX = "X spatial resolution";
	public static final String SpatialResolutionY = "Y spatial resolution";

	/**
	 * Sounder model
	 */
	public static final String EchoSounderModel = "Echo-sounder model";

	/**
	 * Data Parameters
	 */
	public final String Date = "Date";
	public final String Time = "Time";
	public final String Unit = "Unit";

	public final String BeamNbr = "BeamNbr";
	public final String CycleNbr = "CycleNbr";
	public final String AntennaNbr = "AntennaNbr";

	public static final String MinDepthLabel = "Minimum depth (m)";
	public static final String MaxDepthLabel = "Maximum depth (m)";

	/**
	 * NetCDF string dimensions
	 */
	public static final int MB_NAME_LENGTH_VALUE = 20;
	public static final int MB_LABEL_LENGTH_VALUE = 40;
	public static final int MB_LONG_NAME_LENGTH_VALUE = 80;
	public static final int MB_COMMENT_LENGTH_VALUE = 256;
	public static final int MB_INFO_LENGTH_VALUE = 1024;

	public static final String MeterUnit = "m";
	public static final String DegreeUnit = "degree";
	public static final String RadianUnit = "radian";
	public static final String DegreeCelsiusUnit = "degree_Celsius";
	public static final String SpeedUnit = "m/s";
	public static final String SalinityUnit = "PSU";

	public static final byte COMPENSATION_LAYER_MODE_UNKNOWN = 0;  
	public static final byte COMPENSATION_LAYER_MODE_ACTIVE = 1;
	public static final byte COMPENSATION_LAYER_MODE_INACTIVE = 2;

	public static final short MISSING_IFREMER_QUALITY_FACTOR = 0;
	public static final short MISSING_REFLECTIVITY = -64;

	



	

	
	public static byte NOSLOPE = 0; // slope
	public static byte NOBIAS = 0; // sounding bias

	// Frequency plan values
	public static byte NO_FREQUENCY_PLAN = 0; // a single frequency plan
	public static byte FREQUENCY_PLAN_NB_ONE = 1; // frequency plan n 1
	public static byte FREQUENCY_PLAN_NB_TWO = 2; // frequency plan n 2

}
