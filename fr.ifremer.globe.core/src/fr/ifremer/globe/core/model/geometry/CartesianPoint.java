/**
 * 
 */
package fr.ifremer.globe.core.model.geometry;

/**
 * Represents a point in a 2D referential, defined as (x,y)
 */
public class CartesianPoint {



	/** The location of the point in the X direction. */
	protected double x;

	/** The location of the point in the Y direction. */
	protected double y;

	/**
	 * Constructs a point to the origin.
	 */
	public CartesianPoint() {
		x = 0d;
		y = 0d;
	}

	/**
	 * Constructs a point from given x and y values.
	 * 
	 * @param xVal The location of the point in the X direction.
	 * @param yVal The location of the point in the Y direction.
	 */
	public CartesianPoint(double xVal, double yVal) {
		x = xVal;
		y = yVal;
	}

	/**
	 * Getter of x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Setter of x
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Getter of y
	 */
	public double getY() {
		return y;
	}

	/**
	 * Setter of y
	 */
	public void setY(double y) {
		this.y = y;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartesianPoint other = (CartesianPoint) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CartesianPoint [x=" + x + ", y=" + y + "]";
	}
}
