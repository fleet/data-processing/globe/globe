/**
 * 
 */
package fr.ifremer.globe.core.model.geometry;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

/**
 * Class representing an oriented Box. This class is defined by 3 points, the fourth is deduced, it is supposed to be a
 * square
 */
public class OrientedBox {
	/** Geographical points delimiting this zone */
	private List<CartesianPoint> points = new ArrayList<>();

	/**
	 * 
	 */
	private Vector2D mainAxis;
	private Vector2D secondAxis;

	/**
	 * create an Oriented Geobox, ie a geozone with 4 points Note that the 4th could have been deduced.
	 */
	public OrientedBox(CartesianPoint pointA, CartesianPoint pointB, CartesianPoint pointC, CartesianPoint pointD) {
		super();
		points.add(pointA);
		points.add(pointB);
		points.add(pointC);
		points.add(pointD);
		mainAxis = new Vector2D(pointB.getX() - pointA.getX(), pointB.getY() - pointA.getY());
		secondAxis = new Vector2D(pointC.getX() - pointB.getX(), pointC.getY() - pointB.getY());

		// Main axis is the longest one
		if (secondAxis.getNorm() > mainAxis.getNorm()) {
			secondAxis = new Vector2D(pointB.getX() - pointA.getX(), pointB.getY() - pointA.getY());
			mainAxis = new Vector2D(pointC.getX() - pointB.getX(), pointC.getY() - pointB.getY());
		}
	}

	/**
	 * return the vector around the "main axis"
	 * <p>
	 * Main axis is supposed to be between the first and second point
	 */
	public Vector2D getMainAxis() {
		return new Vector2D(mainAxis.toArray());
	}

	/**
	 * return the vector around the "main axis"
	 * <p>
	 * Main axis is supposed to be between the first and second point
	 */
	public Vector2D getSecondaryAxis() {
		return new Vector2D(secondAxis.toArray());
	}

	/**
	 * return the position of the center of the box
	 */
	public Vector2D getCenter() {
		double centerY = 0;
		double centerX = 0;

		for (CartesianPoint point : points) {
			centerY += point.getY();
			centerX += point.getX();
		}
		Vector2D center = new Vector2D(centerX / points.size(), centerY / points.size());
		return center;
	}

	public List<CartesianPoint> getPoints() {
		return points;
	}

	public CartesianPoint get(int i) {
		return points.get(i);
	}

}
