package fr.ifremer.globe.core.model.current;

import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.utils.units.Units;

/**
 * Defines a type of {@link NavigationMetadata} for current
 */
public class CurrentMetadataType {

	/** List of classic {@link NavigationMetadataType} **/
	public static final NavigationMetadataType<Double> HEADING = new NavigationMetadataType<>("Heading", Units.DEGREES,
			0d, 360d);
	public static final NavigationMetadataType<Double> VALUE = new NavigationMetadataType<>("Value", "m/s");

	/**
	 * Constructor
	 */
	private CurrentMetadataType() {
	}
}