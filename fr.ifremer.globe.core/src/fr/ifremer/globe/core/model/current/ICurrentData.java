package fr.ifremer.globe.core.model.current;

import java.io.Closeable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata.NavigationMetadataValue;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * This interface provides methods to get navigation data.
 */
public interface ICurrentData extends Closeable {

	/**
	 * @return the file from which the data originated
	 */
	IFileInfo getFileInfo();

	/** Return the filtering used to procuce this data */
	CurrentFiltering getFiltering();

	/**
	 * @return the number of current vector present in the file.
	 */
	int getTotalVectorCount();

	/**
	 * @return the number of current vector present in this container, after filtering.
	 */
	int getVectorCount();

	/**
	 * @return the min max of the range in this data container.
	 */
	FloatRange getRangeLimits();

	/**
	 * @return the time of a current vector
	 */
	long getVectorTime(int index) throws GIOException;

	/**
	 * @return the time of a current vector
	 */
	Instant getVectorInstant(int index) throws GIOException;

	/**
	 * @return the longitude at the specified index.
	 */
	double getVectorLongitude(int index) throws GIOException;

	/**
	 * @return the latitude at the specified index.
	 */
	double getVectorLatitude(int index) throws GIOException;

	/**
	 * @return the elevation at the specified index.
	 */
	float getVectorElevation(int index) throws GIOException;

	/**
	 * @return the northward absolute current velocity in meter per second
	 */
	float getVectorNorthwardVelocity(int index) throws GIOException;

	/**
	 * @return the eastward absolute current velocity in meter per second
	 */
	float getVectorEastwardVelocity(int index) throws GIOException;

	/**
	 * @return the value of the vector (speed in m/s)
	 */
	default double getVectorValue(int index) throws GIOException {
		double x = getVectorEastwardVelocity(index);
		double y = getVectorNorthwardVelocity(index);
		return Math.sqrt(x * x + y * y);
	}

	/**
	 * @return the heading of the vector
	 */
	default double getVectorHeading(int index) throws GIOException {
		double x = -getVectorEastwardVelocity(index);
		double y = getVectorNorthwardVelocity(index);
		if (y == 0d)
			return x >= 0d ? 90f : 270f;
		double heading = Math.toDegrees(Math.atan(x / y));
		return heading >= 0d ? heading : 360d + heading;
	}

	/**
	 * @return a list of {@link NavigationMetadata}
	 */
	List<NavigationMetadata<Double>> getVectorMetadata();

	/**
	 * @return all {@link NavigationMetadataType} and associated values for the specified index.
	 */
	default List<NavigationMetadataValue<?>> getAllVectorMetadataValues(int index) throws GIOException {
		List<NavigationMetadataValue<?>> result = new ArrayList<>();
		for (var metadata : getVectorMetadata())
			result.add(metadata.getMetadataValue(index));
		return result;
	}

	/**
	 * @return the value of the {@link NavigationMetadataType} at the specified index if exists.
	 */
	@SuppressWarnings("unchecked")
	default <T> Optional<T> getVectorMetadataValue(NavigationMetadataType<T> metadataType, int index)
			throws GIOException {
		for (var metadata : getVectorMetadata()) {
			if (metadata.getType().equals(metadataType))
				return Optional.of((T) metadata.getValue(index));
		}
		return Optional.empty();
	}

	/**
	 * @return a String describing the vector. Useful for tooltips
	 */
	default String getVectorDescription(int vectorIndex) throws GIOException {
		var result = new StringBuilder("[" + vectorIndex + "] " + "\n");
		result.append("Origin = ")//
				.append(FormatLatitudeLongitude.latitudeToString(getVectorLatitude(vectorIndex))).append(" : ")//
				.append(FormatLatitudeLongitude.longitudeToString(getVectorLongitude(vectorIndex))).append(" : ")//
				.append(NumberUtils.getStringMetersDouble(getVectorElevation(vectorIndex))).append(" m\n")//
				.append("Northward velocity = ")
				.append(NumberUtils.getStringMetersDouble(getVectorNorthwardVelocity(vectorIndex))).append(" m/s\n")//
				.append("Eastward velocity = ")
				.append(NumberUtils.getStringMetersDouble(getVectorEastwardVelocity(vectorIndex))).append(" m/s\n")//
				.append("Heading = ").append(NumberUtils.getStringAngle(getVectorHeading(vectorIndex))).append("\n")//
				.append("Value = ").append(NumberUtils.getStringMetersDouble(getVectorValue(vectorIndex)))
				.append(" m/s")//
		;
		return result.toString();
	}
}
