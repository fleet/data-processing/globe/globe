package fr.ifremer.globe.core.model.current;

import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;

/**
 * This record contains the filtering parameters of a ICurrentData.
 */
public record CurrentFiltering(//
		int sampling, //
		Optional<FloatRange> range) {

}
