package fr.ifremer.globe.core.model.velocity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SoundVelocityProfile implements ISoundVelocityProfile {
	/**
	 * date.
	 */
	private Date date;

	/**
	 * Latitude
	 */
	private double latitude = Double.NaN;

	/**
	 * Longitude
	 */
	private double longitude = Double.NaN;

	/**
	 * Profile type
	 */
	private int type = Integer.MAX_VALUE;
	/**
	 * Velocity computation algorithm
	 */
	private String velCompAlgo = "";

	/**
	 * Carrier
	 */
	private String carrier = "";

	/**
	 * sequence number
	 */
	private int sequence = Integer.MAX_VALUE;

	/**
	 * serialNumber
	 */
	private int serialNumber = Integer.MAX_VALUE;

	/**
	 * Profile salinity
	 */
	private float profileSalinity = Float.NaN;

	/**
	 * Profile depth
	 */
	private double profileDepth = Double.NaN;

	/**
	 * Pressure
	 */
	private float pressure = Float.NaN;

	/**
	 * Profile surf air temp.
	 */
	private float surfAirTemp = Float.NaN;

	/**
	 * Profile dry air temp.
	 */
	private float dryAirTemp = Float.NaN;

	/**
	 * Profile Humidity air temp..
	 */
	private float humidAirTemp = Float.NaN;

	/**
	 * TNMG
	 */
	private String TNMG = "";

	/**
	 * Wind direction and strength
	 */
	private String wind = "";

	/**
	 * Swell direction and period
	 */
	private String swell = "";

	/**
	 * Sensor type
	 */
	private String sensorType = "";

	/**
	 * Sample number
	 */
	private int sampleNumber = Integer.MAX_VALUE;

	/**
	 * Profile comment
	 */
	private String profileComment = "";

	/**
	 * soundVelocity.
	 */
	private List<SoundVelocity> soundVelocities;

	public SoundVelocityProfile() {
		super();
		soundVelocities = new ArrayList<>();
	}

	public SoundVelocityProfile(Date date, int profileCounter, int serialNumber, List<Double> velocities,
			List<Double> depths) {
		super();
		soundVelocities = new ArrayList<>();

		this.date = date;
		this.sequence = profileCounter;
		this.serialNumber = serialNumber;
		if (!velocities.isEmpty())
			this.profileDepth = depths.get(velocities.size() - 1);
		int i = 0;
		while (i < velocities.size()) {
			if (velocities.size() == 1 && depths == null) {
				soundVelocities.add(new SoundVelocity(velocities.get(i), 0.0));
				break;
			} else {
				soundVelocities.add(new SoundVelocity(velocities.get(i), depths.get(i)));
			}
			i++;
		}
	}

	/**
	 * 
	 * @param velocities
	 * @param depths: same size as velocities
	 * @param date
	 * @param temperature : same size as velocities
	 * @param salinity: same size as velocities
	 * @param absorption: same size as velocities
	 */

	public SoundVelocityProfile(Date date, int profileCounter, int serialNumber, List<Double> velocities,
			List<Double> depths, List<Double> temperature, List<Double> salinity, List<Double> absorption) {
		super();
		soundVelocities = new ArrayList<>();

		this.date = date;
		this.sequence = profileCounter;
		this.serialNumber = serialNumber;
		if (!velocities.isEmpty())
			this.profileDepth = depths.get(velocities.size() - 1);

		int i = 0;
		while (i < velocities.size()) {
			soundVelocities.add(new SoundVelocity(velocities.get(i), depths.get(i), temperature.get(i), salinity.get(i),
					absorption.get(i)));
			i++;
		}
	}

	public SoundVelocityProfile(Date soundProfileDate, int pingCounter, int serialNumber,
			List<SoundVelocity> soundSpeedValues) {
		this(soundSpeedValues, soundProfileDate);
		this.sequence = pingCounter;
		this.serialNumber = serialNumber;
	}

	public SoundVelocityProfile(List<SoundVelocity> soundVelocities, Date date) {
		super();
		this.date = date;
		this.soundVelocities = soundVelocities;
	}

	public SoundVelocityProfile(double soundVelocity, Date date) {
		super();

		this.date = date;
		soundVelocities = new ArrayList<>();
		soundVelocities.add(new SoundVelocity(soundVelocity));
	}

	@Override
	public String toString() {
		return "SoundVelocityProfile [soundVelocities=" + soundVelocities + "]";
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getLat() {
		return latitude;
	}

	public double getLon() {
		return longitude;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getVelCompAlgo() {
		return velCompAlgo;
	}

	public void setVelCompAlgo(String velCompAlgo) {
		this.velCompAlgo = velCompAlgo;
	}

	public String getCarrier() {
		return carrier;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public float getProfileSalinity() {
		return profileSalinity;
	}

	public void setProfileSalinity(float profileSalinity) {
		this.profileSalinity = profileSalinity;
	}

	public double getProfileDepth() {
		return profileDepth;
	}

	public void setProfileDepth(float profileDepth) {
		this.profileDepth = profileDepth;
	}

	public float getPressure() {
		return pressure;
	}

	public void setPressure(float pressure) {
		this.pressure = pressure;
	}

	public float getSurfAirTemp() {
		return surfAirTemp;
	}

	public void setSurfAirTemp(float surfAirTemp) {
		this.surfAirTemp = surfAirTemp;
	}

	public float getDryAirTemp() {
		return dryAirTemp;
	}

	public void setDryAirTemp(float dryAirTemp) {
		this.dryAirTemp = dryAirTemp;
	}

	public float getHumidAirTemp() {
		return humidAirTemp;
	}

	public void setHumidAirTemp(float humidAirTemp) {
		this.humidAirTemp = humidAirTemp;
	}

	public String getTNMG() {
		return TNMG;
	}

	public void setTNMG(String TNMG) {
		this.TNMG = TNMG;
	}

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public String getSwell() {
		return swell;
	}

	public void setSwell(String swell) {
		this.swell = swell;
	}

	public String getSensorType() {
		return sensorType;
	}

	public void setSensortype(String sensortype) {
		this.sensorType = sensortype;
	}

	public int getSampleNumber() {
		return sampleNumber;
	}

	public void setSampleNumber(int sampleNumber) {
		this.sampleNumber = sampleNumber;
	}

	public String getProfileComment() {
		return profileComment;
	}

	public void setProfileComment(String profileComment) {
		this.profileComment = profileComment;
	}

	public List<SoundVelocity> getSoundVelocities() {
		return soundVelocities;
	}

	public int getNbrLevels() {
		return soundVelocities.size();
	}

	@Override
	public boolean equals(Object obj) {
		boolean ret = false;
		if (soundVelocities instanceof SoundVelocityProfile) {
			SoundVelocityProfile pSsoundVelocityProfile = (SoundVelocityProfile) obj;
			List<SoundVelocity> pSoundVelocities = pSsoundVelocityProfile.getSoundVelocities();
			if (this.soundVelocities.size() == pSoundVelocities.size()) {
				ret = true;
				int i = 0;
				while (i < soundVelocities.size()) {
					SoundVelocity soundVelocity = soundVelocities.get(i);
					SoundVelocity pSoundVelocity = pSoundVelocities.get(i);
					if ((soundVelocity.getDepth().doubleValue() != pSoundVelocity.getDepth().doubleValue())) {
						ret = false;
						break;
					}
					i++;
				}
			}
		}
		return ret;
	}

	public void setSoundVelocities(List<SoundVelocity> soundVelocities) {
		this.soundVelocities = soundVelocities;
	}

	public void setLat(double latitude) {
		this.latitude = latitude;
	}

	public void setLon(double longitude) {
		this.longitude = longitude;
	}

}
