package fr.ifremer.globe.core.model.velocity;

import java.util.Date;
import java.util.List;

public interface ISoundVelocityProfile {

	public Date getDate();

	public void setDate(Date date);

	public double getLat();

	public double getLon();

	public int getType();

	public void setType(int type);

	public String getVelCompAlgo();

	public void setVelCompAlgo(String velCompAlgo);

	public String getCarrier();

	public int getSequence();

	public void setSequence(int sequence);

	public int getSerialNumber();

	public void setSerialNumber(int serialNumber);

	public void setCarrier(String carrier);

	public float getProfileSalinity();

	public void setProfileSalinity(float profileSalinity);

	public double getProfileDepth();

	public void setProfileDepth(float profileDepth);

	public float getPressure();

	public void setPressure(float pressure);

	public float getSurfAirTemp();

	public void setSurfAirTemp(float surfAirTemp);

	public float getDryAirTemp();

	public void setDryAirTemp(float dryAirTemp);

	public float getHumidAirTemp();

	public void setHumidAirTemp(float humidAirTemp);

	public String getTNMG();

	public void setTNMG(String TNMG);

	public String getWind();

	public void setWind(String wind);

	public String getSwell();

	public void setSwell(String swell);

	public String getSensorType();

	public void setSensortype(String sensortype);

	public int getSampleNumber();

	public void setSampleNumber(int sampleNumber);

	public String getProfileComment();

	public void setProfileComment(String profileComment);

	public List<SoundVelocity> getSoundVelocities();

	public int getNbrLevels();

	public boolean equals(Object obj);

	public void setLat(double latitude);

	public void setLon(double longitude);
}
