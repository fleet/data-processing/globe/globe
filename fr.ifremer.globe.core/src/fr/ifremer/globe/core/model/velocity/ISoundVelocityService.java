/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.velocity;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Navigation services
 */
public interface ISoundVelocityService {

	/**
	 * Returns the service implementation of ISoundVelocityService.
	 */
	static ISoundVelocityService grab() {
		return OsgiUtils.getService(ISoundVelocityService.class);
	}

	/**
	 * Supplies a ISoundVelocityData on the specified sound velocity file.
	 *
	 * @return the ISoundVelocityData. Optional.empty if none
	 */
	Optional<ISoundVelocityData> getSoundVelocityData(String filePath);

	/**
	 * Supplies a ISoundVelocityData on the specified IInfoStore.
	 *
	 * @return the ISoundVelocityData. Optional.empty if none
	 */
	Optional<ISoundVelocityData> getSoundVelocityData(IFileInfo info);
}
