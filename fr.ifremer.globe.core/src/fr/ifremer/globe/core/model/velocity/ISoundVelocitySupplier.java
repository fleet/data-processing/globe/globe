/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.velocity;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;

/**
 * 
 */
public interface ISoundVelocitySupplier {

	/**
	 * Supply a ISoundVelocityDataSupplier on the specified of velocity file.
	 * 
	 * @return the ISoundVelocityDataSupplier. Optional.empty if none
	 */
	default Optional<ISoundVelocityData> getSoundVelocityData(String filePath) {
		return IFileService.grab().getFileInfo(filePath).map(fileInfo -> getSoundVelocityData(fileInfo)).get();
	}

	/**
	 * Supply a ISoundVelocityDataProvider on the specified IInfoStore.
	 * 
	 * @return the ISoundVelocityDataProvider. Optional.empty if none
	 */
	Optional<ISoundVelocityData> getSoundVelocityData(IFileInfo fileInfo);

}
