package fr.ifremer.globe.core.model.velocity;

public class SoundVelocity {

	/**
	 * depth.
	 */
	private double depth  = Double.NaN;

	/**
	 * soundVelocity.
	 */
	private double soundVelocity= Double.NaN;

	/**
	 * Température
	 */	
	private double temperature = Double.NaN;

	/**
	 * Salinité 
	 */	
	private double salinity = Double.NaN;

	/**
	 * Absorption
	 */	
	private double absorption  = Double.NaN;

	public SoundVelocity(double soundVelocity) {
		super();
		this.soundVelocity = soundVelocity;
		this.depth = Double.NaN;
		this.temperature = Double.NaN;
		this.salinity = Double.NaN;
		this.absorption = Double.NaN;
	}

	public SoundVelocity(double soundVelocity, double depth) {
		super();
		this.soundVelocity = soundVelocity;
		this.depth = depth;
		this.temperature = Double.NaN;
		this.salinity = Double.NaN;
		this.absorption = Double.NaN;
	}

	public SoundVelocity(double soundVelocity, double depth, double temperature, double salinity, double absorption) {
		super();
		this.soundVelocity = soundVelocity;
		this.depth = depth;
		this.temperature = temperature;
		this.salinity = salinity;
		this.absorption = absorption;
	}

	public double getSoundVelocity() {
		return soundVelocity;
	}

	public Double getDepth() {
		return depth;
	}

	public Double getTemperature() {
		return temperature;
	}
	public Double getSalinity() {
		return salinity;
	}
	public Double getAbsorption() {
		return absorption;
	}

	@Override
	public String toString() {
		return "SoundVelocity [soundVelocity=" + soundVelocity + "]";
	}

}
