/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.velocity;

import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Writer of Sound Velocity (.vel) files
 */
public interface IVelWriter {

	/**
	 * Returns the service implementation of INviWriter.
	 */
	static IVelWriter grab() {
		return OsgiUtils.getService(IVelWriter.class);
	}

	/**
	 * Exports velocity data to a new velocity file.
	 */
	void write(ISoundVelocityData soundVelocityData, String outputFileName)
			throws GIOException;
}
