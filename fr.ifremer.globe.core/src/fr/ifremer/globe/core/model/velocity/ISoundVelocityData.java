package fr.ifremer.globe.core.model.velocity;

import java.util.List;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This interface provides methods to get velocity data.
 */
public interface ISoundVelocityData {
	//// GETTERS

	String getOriginFileName();
	
	List<ISoundVelocityProfile> getSoundVelocityProfiles() throws GIOException;

}
