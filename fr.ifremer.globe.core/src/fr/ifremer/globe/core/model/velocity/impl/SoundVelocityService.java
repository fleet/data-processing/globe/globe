/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.velocity.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityService;
import fr.ifremer.globe.core.model.velocity.ISoundVelocitySupplier;

/**
 * Some services for managing sound velocities.
 */
@Component(name = "globe_model_service_soundvelocity", service = ISoundVelocityService.class)
public class SoundVelocityService implements ISoundVelocityService {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(SoundVelocityService.class);

	/** All registered suppliers */
	protected List<ISoundVelocitySupplier> soundVelocitySuppliers = new ArrayList<>();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ISoundVelocityData> getSoundVelocityData(String filePath) {
		return soundVelocitySuppliers.stream().//
				map(supplier -> supplier.getSoundVelocityData(filePath)).//
				filter(Optional::isPresent).//
				map(Optional::get).//
				findFirst();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ISoundVelocityData> getSoundVelocityData(IFileInfo info) {
		return soundVelocitySuppliers.stream().//
				map(supplier -> supplier.getSoundVelocityData(info)).//
				filter(Optional::isPresent).//
				map(Optional::get).//
				findFirst();
	}

	/** Accept a new ISoundVelocitySupplier provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addSoundVelocitySupplier(ISoundVelocitySupplier supplier) {
		logger.debug("{} registered : {}", ISoundVelocitySupplier.class.getSimpleName(),
				supplier.getClass().getName());

		soundVelocitySuppliers.add(supplier);
	}

}