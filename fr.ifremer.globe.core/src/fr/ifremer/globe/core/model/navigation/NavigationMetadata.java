package fr.ifremer.globe.core.model.navigation;

import java.util.Optional;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class is defined by a {@link NavigationMetadataType} and a {@link MetadataValueSupplier} to get a specific
 * metadata value from an index.
 */
public class NavigationMetadata<T> {

	/**
	 * {@link FunctionalInterface} to get value of type <T> with {@link GIOException}.
	 */
	@FunctionalInterface
	public interface MetadataValueSupplier<T> {
		T getValue(int index) throws GIOException;
	}

	/**
	 * Simple class to bind a {@link NavigationMetadata} and its value.
	 */
	public static record NavigationMetadataValue<T>(NavigationMetadata<T> metadata, T value) {

		public NavigationMetadataType<T> getType() {
			return metadata.getType();
		}

		public Number getValueAsNumber() {
			return getType().parseValueToNumber(value);
		}

		public String getValueAsString() {
			return getType().parseValueToString(value);
		}
	}

	/** Properties **/
	private final NavigationMetadataType<T> type;
	private final MetadataValueSupplier<T> valueSupplier;

	/** Optional min/max values **/
	private Optional<Number> optMinValue = Optional.empty();
	private Optional<Number> optMaxValue = Optional.empty();

	/**
	 * Constructor
	 */
	public NavigationMetadata(NavigationMetadataType<T> metadataType, MetadataValueSupplier<T> valueSupplier) {
		this.type = metadataType;
		this.valueSupplier = valueSupplier;
		this.optMinValue = metadataType.minValue.isPresent()
				? Optional.of(type.parseValueToNumber(metadataType.minValue.get()))
				: Optional.empty();
		this.optMaxValue = metadataType.maxValue.isPresent()
				? Optional.of(type.parseValueToNumber(metadataType.maxValue.get()))
				: Optional.empty();
	}

	/**
	 * @return value of this {@link NavigationMetadata} for the specified index.
	 */
	public NavigationMetadataValue<T> getMetadataValue(int index) throws GIOException {
		var value = valueSupplier.getValue(index);
		return new NavigationMetadataValue<>(this, value);
	}

	// GETTERS

	public NavigationMetadataType<T> getType() {
		return type;
	}

	public Optional<Number> getOptMinValue() {
		return optMinValue;
	}

	public Optional<Number> getOptMaxValue() {
		return optMaxValue;
	}

	public T getValue(int i) throws GIOException {
		return valueSupplier.getValue(i);
	}

}
