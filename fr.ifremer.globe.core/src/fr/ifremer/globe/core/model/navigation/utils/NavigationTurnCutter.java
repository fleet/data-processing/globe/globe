package fr.ifremer.globe.core.model.navigation.utils;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class extends {@link AbstractNavigationCutter} to extract time intervals (= cut lines) without turns.
 */
public class NavigationTurnCutter extends AbstractNavigationCutter {

	private static final Logger LOGGER = LoggerFactory.getLogger(NavigationTurnCutter.class);

	/** Default values **/
	public final static double DEFAULT_HEADING_THRESHOLD = 15;
	public final static Duration DEFAULT_HEADING_INTERVAL = Duration.ofSeconds(60);
	public final static double DEFAULT_SPEED_THRESHOLD = 0.4;
	public final static Duration DEFAULT_SPEED_INTERVAL = Duration.ofSeconds(20);
	public final static Duration DEFAULT_MIN_LINE_DURATION = Duration.ofMinutes(2);
	public final static Mode DEFAULT_MODE = Mode.LAST_VALUE_IN_INTERVAL;
	public final static ObservedVariable DEFAULT_OBSERVED_VARIABLE = ObservedVariable.HEADING;
	public final static boolean DEFAULT_CUT_GAP = false;

	/** Observed variables **/
	public enum ObservedVariable {
		HEADING,
		SPEED,
		HEADING_AND_SPEED
	}

	private ObservedVariable observedVariable = DEFAULT_OBSERVED_VARIABLE;

	/** Modes **/
	public enum Mode {
		LAST_VALUE_IN_INTERVAL,
		AVERAGE_IN_INTERVAL
	}

	private Mode mode = DEFAULT_MODE;

	/** Parameters **/
	private Duration headingInterval = DEFAULT_HEADING_INTERVAL;
	private Duration halfHeadingInterval = headingInterval.dividedBy(2);
	private double headingThreshold = DEFAULT_HEADING_THRESHOLD;
	private Duration speedInterval = DEFAULT_SPEED_INTERVAL;
	private Duration halfSpeedInterval = speedInterval.dividedBy(2);
	private double speedThreshold = DEFAULT_SPEED_THRESHOLD;
	private boolean cutGap = false;

	/**
	 * Constructor
	 */
	public NavigationTurnCutter(INavigationData navigationData) throws GIOException {
		super(navigationData);
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		logger.debug("Compute with option : \n" //
				+ "observedVariable : " + observedVariable + "\n" //
				+ "mode : " + mode + "\n" //
				+ "headingInterval : " + headingInterval + "\n" //
				+ "headingThreshold : " + headingThreshold + "\n" //
				+ "speedInterval : " + speedInterval + "\n" //
				+ "speedThreshold : " + speedThreshold + "\n" //
				+ "cutGap : " + cutGap);

		if (isSpeedObserved() && !navigationData.getMetadataTypes().contains(NavigationMetadataType.SPEED))
			throw new BadParameterException("Unable to compute turns from speed thresholds: speed data missing.");

		if (isHeadingObserved() && !navigationData.getMetadataTypes().contains(NavigationMetadataType.HEADING))
			throw new BadParameterException("Unable to compute turns from heading thresholds: heading data missing.");

		return super.apply(monitor, logger);
	}

	/**
	 * Predicates if a navigation point is in a turn or not.
	 */
	@Override
	public boolean test(Integer index) {
		try {
			Instant currentInstant = navigationData.getInstant(index);
			var startHeading = currentInstant.minus(halfHeadingInterval);
			var endHeading = currentInstant.plus(halfHeadingInterval);
			double beforeHeading = Double.NaN;
			double afterHeading = Double.NaN;
			var startSpeed = currentInstant.minus(halfSpeedInterval);
			var endSpeed = currentInstant.plus(halfSpeedInterval);
			double beforeSpeed = Double.NaN;
			double afterSpeed = Double.NaN;

			switch (mode) {
			case AVERAGE_IN_INTERVAL:
				// get observed average values
				if (isHeadingObserved()) {
					var sliceBefore = indexByInstant.subMap(startHeading, true, currentInstant, true);
					var sliceAfter = indexByInstant.subMap(currentInstant, false, endHeading, true);
					// slice after empty ? if cut gap is false : get the next point to start the slice
					if (sliceAfter.isEmpty() && !cutGap) {
						var from = indexByInstant.higherKey(currentInstant);
						if (from != null)
							sliceAfter = indexByInstant.subMap(from, true, from.plus(halfHeadingInterval), true);
					}
					// compute averages
					beforeHeading = average(sliceBefore.values().stream().mapToDouble(this::getHeading).toArray());
					afterHeading = average(sliceAfter.values().stream().mapToDouble(this::getHeading).toArray());
				}
				if (isSpeedObserved()) {
					var sliceBefore = indexByInstant.subMap(startSpeed, true, currentInstant, true);
					var sliceAfter = indexByInstant.subMap(currentInstant, false, endSpeed, true);
					// slice after empty ? if cut gap is false : get the next point to start the slice
					if (sliceAfter.isEmpty() && !cutGap) {
						var from = indexByInstant.higherKey(currentInstant);
						if (from != null)
							sliceAfter = indexByInstant.subMap(from, true, from.plus(halfSpeedInterval), true);
					}
					// compute averages
					beforeSpeed = sliceBefore.values().stream().mapToDouble(this::getSpeed).summaryStatistics()
							.getAverage();
					afterSpeed = sliceAfter.values().stream().mapToDouble(this::getSpeed).summaryStatistics()
							.getAverage();
				}
				break;
			case LAST_VALUE_IN_INTERVAL:
				if (isHeadingObserved()) {
					// get point just after start
					beforeHeading = Optional.ofNullable(indexByInstant.ceilingEntry(startHeading))
							.filter(before -> before.getKey() != currentInstant)
							// or point just before start if cut gap is false
							.or(() -> cutGap ? Optional.empty()
									: Optional.ofNullable(indexByInstant.lowerEntry(startHeading)))
							.map(Entry::getValue).map(this::getHeading).orElse(Double.NaN);

					// get point just before end
					afterHeading = Optional.ofNullable(indexByInstant.floorEntry(endHeading))
							.filter(before -> before.getKey() != currentInstant)
							// or point just after end if cut gap is false
							.or(() -> cutGap ? Optional.empty()
									: Optional.ofNullable(indexByInstant.higherEntry(endHeading)))
							.map(Entry::getValue).map(this::getHeading).orElse(Double.NaN);
				}
				if (isSpeedObserved()) {
					// get point just after start
					beforeSpeed = Optional.ofNullable(indexByInstant.ceilingEntry(startSpeed))
							.filter(before -> before.getKey() != currentInstant)
							// or point just before start if cut gap is false
							.or(() -> cutGap ? Optional.empty()
									: Optional.ofNullable(indexByInstant.lowerEntry(startSpeed)))
							.map(Entry::getValue).map(this::getSpeed).orElse(Double.NaN);

					// get point just before end
					afterSpeed = Optional.ofNullable(indexByInstant.floorEntry(endSpeed))
							.filter(before -> before.getKey() != currentInstant)
							// or point just after end if cut gap is false
							.or(() -> cutGap ? Optional.empty()
									: Optional.ofNullable(indexByInstant.higherEntry(endSpeed)))
							.map(Entry::getValue).map(this::getSpeed).orElse(Double.NaN);
				}
				break;
			default:
				break;
			}

			// check heading
			if (isHeadingObserved() && !isHeadingVariationUnderThreshold(beforeHeading, afterHeading))
				return false;
			// check speed
			if (isSpeedObserved() && (Double.isNaN(beforeSpeed) || Double.isNaN(afterSpeed)
					|| Math.abs(afterSpeed - beforeSpeed) > speedThreshold))
				return false;
			return true;
		} catch (GIOException e) {
			LOGGER.error("Error while test navigation index : {}", index, e);
		}
		return false;
	}

	/**
	 * Compares heading variation with threshold parameter.
	 * 
	 * @return true if variation is under threshold.
	 */
	private boolean isHeadingVariationUnderThreshold(double heading1, double heading2) {
		if (!Double.isNaN(heading1) && !Double.isNaN(heading2)) {
			var headingVariation = Math.abs(heading2 - heading1);
			if (headingVariation > 180)
				headingVariation = Math.abs(headingVariation - 360);
			return headingVariation < headingThreshold;
		}
		return false;
	}

	private boolean isHeadingObserved() {
		return observedVariable == ObservedVariable.HEADING || observedVariable == ObservedVariable.HEADING_AND_SPEED;
	}

	private boolean isSpeedObserved() {
		return observedVariable == ObservedVariable.SPEED || observedVariable == ObservedVariable.HEADING_AND_SPEED;
	}

	private double getHeading(int index) {
		try {
			return navigationData.getMetadataValue(NavigationMetadataType.HEADING, index).orElseThrow(
					() -> new GIOException("Heading not available for navigation " + navigationData.getFileName()));
		} catch (GIOException e) {
			LOGGER.error("Error get heading index : {}", index, e);
		}
		return Double.NaN;
	}

	private double getSpeed(int index) {
		try {
			return navigationData.getMetadataValue(NavigationMetadataType.SPEED, index).orElseThrow(
					() -> new GIOException("Speed not available for navigation " + navigationData.getFileName()))
					* 0.514444d; // convert knots to m/s
		} catch (GIOException e) {
			LOGGER.error("Error get speed index : {}", index, e);
		}
		return Double.NaN;
	}

	/**
	 * Computes average on heading values.
	 */
	private double average(double[] headings) {
		if (headings.length == 0)
			return Double.NaN;
		headings = Arrays.stream(headings).map(Math::toRadians).toArray();
		var sumSin = Arrays.stream(headings).map(Math::sin).sum();
		var sumCos = Arrays.stream(headings).map(Math::cos).sum();
		return Math.toDegrees(Math.atan2(sumSin, sumCos));
	}

	// GETTERS & SETTERS

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public void setHeadingThreshold(double headingThreshold) {
		this.headingThreshold = headingThreshold;
	}

	public void setHeadingInterval(Duration headingInterval) {
		this.headingInterval = headingInterval;
		this.halfHeadingInterval = headingInterval.dividedBy(2);
	}

	public void setSpeedThreshold(double speedThreshold) {
		this.speedThreshold = speedThreshold;
	}

	public void setSpeedInterval(Duration speedInterval) {
		this.speedInterval = speedInterval;
		this.halfSpeedInterval = speedInterval.dividedBy(2);
	}

	public void setObservedVariable(ObservedVariable observedVariable) {
		this.observedVariable = observedVariable;
	}

	public void setCutGap(boolean cutGap) {
		this.cutGap = cutGap;
	}

}
