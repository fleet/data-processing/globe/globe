package fr.ifremer.globe.core.model.navigation;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Supplier of {@link INavigationData} (throws a {@link GIOException}, it's why the default Supplier functional
 * interface is not used).
 */
@FunctionalInterface
public interface INavigationDataSupplier {

	public INavigationData getNavigationData(boolean readonly) throws GIOException;

}
