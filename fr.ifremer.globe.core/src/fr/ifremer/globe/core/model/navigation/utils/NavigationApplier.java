package fr.ifremer.globe.core.model.navigation.utils;

import java.io.IOException;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to apply a new navigation to a sounder file.
 */
public class NavigationApplier implements IDataContainerOwner {

	@Override
	public String getName() {
		return "navigation applier";
	}

	/**
	 * Applies navigation : update sounder latitude/longitude layers with specified navigation data (and manages
	 * resources). By default, immersion is not applied.
	 */
	public void apply(ISounderNcInfo info, INavigationDataSupplier navigationProvider) throws GIOException {
		apply(info, navigationProvider, false);
	}

	/**
	 * Applies navigation : update sounder latitude/longitude layers with specified navigation data (and manages
	 * resources)
	 */
	public void apply(ISounderNcInfo info, INavigationDataSupplier navigationProvider, boolean applyImmersion)
			throws GIOException {
		// Get required resources and apply the navigation
		try (ISounderDataContainerToken sounderDataContainerToken = IDataContainerFactory.grab().book(info, this);
				INavigationData navigationData = navigationProvider.getNavigationData(true)) {
			apply(sounderDataContainerToken.getDataContainer(), navigationData, applyImmersion);
		} catch (IOException e) {
			throw new GIOException(e.getMessage(), e);
		}

		// set "PositionCorrection" flag on
		Optional<ProcessAssessor> processAssessor = info.getProcessAssessor();
		if (processAssessor.isPresent()) {
			processAssessor.get().heedPositionCorrection();
		}

		IFileService.grab().updateStatistics(info);
	}

	/**
	 * Applies navigation : update sounder latitude/longitude layers with specified navigation data
	 */
	private void apply(SounderDataContainer sounderData, INavigationData navData, boolean applyImmersion)
			throws GIOException {
		// get layers
		LongLayer1D timeLayer = sounderData.getLayer(BeamGroup1Layers.PING_TIME);
		DoubleLoadableLayer1D latitudeLayer = sounderData.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
		DoubleLoadableLayer1D longitudeLayer = sounderData.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);
		FloatLoadableLayer1D txTranducerDepth = sounderData.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
		FloatLoadableLayer1D platformVerticalOffset = sounderData.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

		var navigationInterpolator = new NavigationInterpolator(navData);

		// 1) projection in antenna coordinate system (will allow to recompute detphs after antenna & plateform z
		// edition)
		var coordinateSystemLayer = sounderData.getCsLayers(CoordinateSystem.ACS);

		// 2) for each swath : update latitude, longitude & immersion with interpolated values
		for (var i = 0; i < sounderData.getInfo().getCycleCount(); i++) {
			long swathTime = DateUtils.nanoSecondToMilli(timeLayer.get(i));
			latitudeLayer.set(i, navigationInterpolator.getLatitude(swathTime));
			longitudeLayer.set(i, navigationInterpolator.getLongitude(swathTime));
			if (applyImmersion && navData.getMetadata().stream()
					.anyMatch(metadata -> metadata.getType().equals(NavigationMetadataType.ELEVATION))) {
				// update plateform vertical offset
				var newElevation = (float) navigationInterpolator.getMetadata(NavigationMetadataType.ELEVATION,
						swathTime);
				var elevationOffset = newElevation - platformVerticalOffset.get(i);
				platformVerticalOffset.set(i, newElevation);

				// update antenna z
				txTranducerDepth.set(i, txTranducerDepth.get(i) - elevationOffset);
			}
		}

		// 3) flush from antenna coordinate system (recomputes latitdue, longitude & z depth)
		coordinateSystemLayer.flush();
		sounderData.flush();
	}
}
