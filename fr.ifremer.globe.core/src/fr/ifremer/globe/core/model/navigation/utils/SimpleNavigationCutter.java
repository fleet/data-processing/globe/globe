package fr.ifremer.globe.core.model.navigation.utils;

import java.util.function.BiPredicate;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Simple class which extends {@link AbstractNavigationCutter} with a {@link BiPredicate} passed in constructor.
 */
public class SimpleNavigationCutter extends AbstractNavigationCutter {

	private final BiPredicate<INavigationData, Integer> predicate;

	/**
	 * Constructor
	 */
	public SimpleNavigationCutter(INavigationData navData, BiPredicate<INavigationData, Integer> predicate)
			throws GIOException {
		super(navData);
		this.predicate = predicate;
	}

	@Override
	public boolean test(Integer index) {
		return predicate.test(navigationData, index);
	}

}
