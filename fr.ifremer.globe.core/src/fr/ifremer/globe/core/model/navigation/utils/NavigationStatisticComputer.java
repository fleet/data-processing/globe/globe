package fr.ifremer.globe.core.model.navigation.utils;

import java.time.Instant;
import java.util.LongSummaryStatistics;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.utils.exception.GIOException;

public class NavigationStatisticComputer {

	/** record **/
	public record NavigationStatistics(int size, Instant startTime, Instant endTime, double south, double north,
			double west, double east) {
	}

	/**
	 * Computes statistics of {@link INavigationData}.
	 * 
	 * @return {@link NavigationStatistics}
	 */
	public static NavigationStatistics compute(INavigationData navigationData) throws GIOException {
		var timeStat = new LongSummaryStatistics();
		GeoBoxBuilder geoBoxBuilder = new GeoBoxBuilder();
		for (var i : navigationData) {
			if (navigationData.isValid(i)) {
				timeStat.accept(navigationData.getTime(i));
				geoBoxBuilder.addPoint(navigationData.getLongitude(i), navigationData.getLatitude(i));
			}
		}

		if (timeStat.getCount() == 0l)
			throw new GIOException(
					"Not enough data to compute statistics on navigation : " + navigationData.getFileName());

		GeoBox geobox = geoBoxBuilder.build();
		return new NavigationStatistics(//
				(int) timeStat.getCount(), //
				Instant.ofEpochMilli(timeStat.getMin()), Instant.ofEpochMilli(timeStat.getMax()), //
				// South
				geobox.getBottom(),
				// North
				geobox.getTop(),
				// West
				geobox.getLeft(),
				// East
				geobox.getRight());
	}

}
