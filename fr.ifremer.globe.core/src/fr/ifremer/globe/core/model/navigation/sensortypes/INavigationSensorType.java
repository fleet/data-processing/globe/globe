package fr.ifremer.globe.core.model.navigation.sensortypes;

/**
 * This enumerator defines navigation sensor types.
 */
public interface INavigationSensorType {

	/**
	 * @return the raw value.
	 */
	int getValue();

	/** @return {@link NmeaSensorType} corresponding value. **/
	NmeaSensorType toNmeaSensorType();

	/** @return {@link NviLegacySensorType} corresponding value. **/
	NviLegacySensorType toNviSensorType();

	/**
	 * Defines an operation which can be applied on any {@link INavigationSensorType}.
	 * 
	 * @param <T> : type returned by the operation.
	 */
	interface INavigationSensorTypeOperation<T> {

		T accept(NmeaSensorType nmeaSensorType);

		T accept(NviLegacySensorType nviSensorType);

		T accept(S7KSensorType s7kSensorType);

	}

	/**
	 * Accepts an {@link INavigationSensorTypeOperation}.
	 * 
	 * @param <T> : type of return from the {@link INavigationSensorTypeOperation}.
	 * @param navigationSensorTypeOperation : the operation to apply.
	 * @return the operation passed in parameters.
	 */
	<T> T perform(INavigationSensorTypeOperation<T> navigationSensorTypeOperation);
}
