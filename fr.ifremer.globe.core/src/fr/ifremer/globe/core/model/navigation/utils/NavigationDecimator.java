package fr.ifremer.globe.core.model.navigation.utils;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.utils.algo.DouglasPeucker;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility class to decimate a {@link INavigationData}.
 */
public class NavigationDecimator {

	/**
	 * Applies decimation with the Douglas Peucker algorithm.
	 * 
	 * @return indexes after decimation
	 * @throws GIOException
	 */
	public static List<Integer> decimate(int maxPoints, INavigationData navigationData) throws GIOException {

		AtomicReference<GIOException> exception = new AtomicReference<>();

		// get navigation points as list of array with latitude, longitude and index
		List<double[]> inputPointsAsDouble = navigationData.stream()//
				.map(i -> {
					try {
						if (navigationData.isValid(i)) // keep only valid points
							return new double[] { navigationData.getLatitude(i), navigationData.getLongitude(i), i };
					} catch (GIOException e) {
						exception.compareAndSet(null, e);
					}
					return new double[] {};
				}) //
				.filter(arr -> arr.length == 3 && !Double.isNaN(arr[0]) && !Double.isNaN(arr[1])) //
				.collect(Collectors.toList());

		// fails if an exception has been raised
		if (exception.get() != null)
			throw new GIOException(
					"Navigation decimation failed with at least one exception : " + exception.get().getMessage(),
					exception.get());

		// apply DouglasPeucker : result is an list of array with latitude, longitude and index
		List<double[]> result = DouglasPeucker.performRecursivly(inputPointsAsDouble, 1e-6, maxPoints);

		// return only indexes
		return result.stream().map(doubleArray -> (int) doubleArray[2]).collect(Collectors.toList());
	}

}
