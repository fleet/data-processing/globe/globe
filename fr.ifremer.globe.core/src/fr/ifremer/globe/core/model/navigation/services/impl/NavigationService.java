/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.navigation.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import fr.ifremer.globe.core.io.NavigationSupplierPriority;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.utils.Pair;

/**
 * Some services for managing navigations.
 */
@Component(name = "globe_model_service_navigation", service = INavigationService.class)
public class NavigationService implements INavigationService {

	/** All registered suppliers */
	protected List<INavigationSupplier> navigationSuppliers = new ArrayList<>();
	/** Temporary list to store all registered supplier in order to sort them when activate the component */
	protected List<Pair<INavigationSupplier, Integer>> tmpFileInfoSuppliers = new ArrayList<>();

	@Activate
	protected void postConstruct() {
		tmpFileInfoSuppliers.sort((p1, p2) -> p1.getSecond().compareTo(p2.getSecond()));
		navigationSuppliers = tmpFileInfoSuppliers.stream().map(Pair::getFirst).collect(Collectors.toList());
		// The list is not necessary any more
		tmpFileInfoSuppliers = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataProvider(String filePath) {
		return navigationSuppliers.stream().//
				map(supplier -> supplier.getNavigationDataSupplier(filePath)).//
				filter(Optional::isPresent).//
				map(Optional::get).//
				findFirst();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo info) {
		return navigationSuppliers.stream().//
				map(supplier -> supplier.getNavigationDataSupplier(info)).//
				filter(Optional::isPresent).//
				map(Optional::get).//
				findFirst();
	}

	/** Accept a new INavigationSupplier provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addNavigationSupplier(INavigationSupplier navigationSupplier, Map<String, Object> properties) {
		Integer priority = (Integer) properties.getOrDefault(NavigationSupplierPriority.PROPERTY_PRIORITY,
				Integer.valueOf(NavigationSupplierPriority.DEFAULT_PRIORITY));
		tmpFileInfoSuppliers.add(new Pair<>(navigationSupplier, priority));
		navigationSuppliers.add(navigationSupplier);
	}

}