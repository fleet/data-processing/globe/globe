package fr.ifremer.globe.core.model.navigation;

import java.io.Closeable;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata.NavigationMetadataValue;
import fr.ifremer.globe.core.model.quality.MeasurandQualifierFlags;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This interface provides methods to get navigation data.
 */
public interface INavigationData extends Closeable, Iterable<Integer> {

	@Override
	default void close() throws IOException {
		// by default, nothing to do in close method
	}

	/** Content type of the navigation file */
	default ContentType getContentType() {
		return ContentType.UNDEFINED;
	}

	String getFileName();

	/**
	 * @return the number of navigation point.
	 */
	int size();

	/**
	 * @return the longitude at the specified index.
	 */
	double getLongitude(int index) throws GIOException;

	/**
	 * @return the latitude at the specified index.
	 */
	double getLatitude(int index) throws GIOException;

	// -------------- METADATA -------------/

	/**
	 * @return a list of {@link NavigationMetadata} : defines the metadata for this {@link INavigationData} and how to
	 *         get values.
	 */
	List<NavigationMetadata<?>> getMetadata();

	/**
	 * @return the list of {@link NavigationMetadataType} handled by this {@link INavigationData}.
	 */
	default List<NavigationMetadataType<?>> getMetadataTypes() {
		return getMetadata().stream().map(NavigationMetadata::getType).collect(Collectors.toList());
	}

	/**
	 * @return the value of the {@link NavigationMetadataType} at the specified index if exists.
	 */
	@SuppressWarnings("unchecked")
	default <T> Optional<T> getMetadataValue(NavigationMetadataType<T> metadataType, int index) throws GIOException {
		for (var metadata : getMetadata()) {
			if (metadata.getType().equals(metadataType))
				return Optional.of((T) metadata.getValue(index));
		}
		return Optional.empty();
	}

	/**
	 * @return all {@link NavigationMetadataType} and associated values for the specified index.
	 */
	default List<NavigationMetadataValue<?>> getAllMetadataValues(int index) throws GIOException {
		List<NavigationMetadataValue<?>> result = new ArrayList<>();
		for (var metadata : getMetadata())
			result.add(metadata.getMetadataValue(index));
		return result;
	}

	/**
	 * @return all {@link NavigationMetadataType} and associated values for the specified index.
	 */
	default Optional<NavigationMetadataValue<?>> getNavigationMetadataValue(NavigationMetadataType<?> metadataType,int index) throws GIOException {
		for (var metadata : getMetadata())
		{
			if (metadata.getType().equals(metadataType))
				return Optional.of(metadata.getMetadataValue(index));
		}
		return Optional.empty();
	}
	
	/**
	 * @return all {@link NavigationMetadataType} and associated values for the specified index.
	 */
	default Optional<NavigationMetadata<?>> getNavigationMetadata(NavigationMetadataType<?> metadataType) throws GIOException {
		for (var metadata : getMetadata())
		{
			if (metadata.getType().equals(metadataType))
				return Optional.of(metadata);
		}
		return Optional.empty();
	}
	
	/**
	 * @return {@link MeasurandQualifierFlags} for the specified index.
	 */
	default MeasurandQualifierFlags getQualifierFlag(int index) {
		// NONE by default (qualifier flag is optional)
		return MeasurandQualifierFlags.NONE;
	}

	/**
	 * Sets the {@link MeasurandQualifierFlags} for the specified index.
	 */
	default void setQualifierFlag(int index, MeasurandQualifierFlags flag) {
		// nothing by default (qualifier flag is optional)
	}

	/**
	 * @return true when position at specified index is valid.
	 */
	default boolean isValid(int index) {
		return getQualifierFlag(index).isValid();
	}

	/**
	 * @return {@link GeoBox} computed from valid navigation point.
	 */
	default GeoBox computeGeoBox() throws GIOException {
		GeoBoxBuilder geoBuilder = new GeoBoxBuilder();
		for (int i = 0; i < size(); i++)
			geoBuilder.addPoint(getLongitude(i), getLatitude(i));
		return geoBuilder.build();
	}

	/**
	 * Provides an iterator on each index.
	 *
	 * @see Iterable
	 */
	@Override
	default Iterator<Integer> iterator() {
		return IntStream.range(0, size()).iterator();
	}

	/**
	 * @return a new {@link Stream} on indexes.
	 */
	default Stream<Integer> stream() {
		return StreamSupport.stream(spliterator(), false);
	}

	/**
	 * use getMetadataValue()
	 */
	default long getTime(int index) throws GIOException {
		return getMetadataValue(NavigationMetadataType.TIME, index)
				.orElseThrow(() -> new GIOException("Time not available for navigation " + getFileName()));
	}

	default Instant getInstant(int index) throws GIOException {
		return Instant.ofEpochMilli(getTime(index));
	}

}
