/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.navigation.services;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Navigation services
 */
public interface INavigationService {

	/**
	 * Returns the service implementation of INavigationService.
	 */
	static INavigationService grab() {
		return OsgiUtils.getService(INavigationService.class);
	}

	/**
	 * Supply a INavigationDataProvider on the specified of navigation file.
	 *
	 * @return the INavigationDataProvider. Optional.empty if none
	 */
	Optional<INavigationDataSupplier> getNavigationDataProvider(String filePath);

	/**
	 * Supply a INavigationDataSupplier on the specified IInfoStore.
	 *
	 * @return the INavigationData. Optional.empty if none
	 */
	Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo info);
}
