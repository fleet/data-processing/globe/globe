package fr.ifremer.globe.core.model.navigation;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.primitives.IntList;

import fr.ifremer.globe.core.io.nvi.export.NviExporter;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * {@link INavigationData} with sampled points.
 */
public class SampledNavigationData implements INavigationData {

	private final INavigationData originalNavigation;

	private final IntList samples;

	/**
	 * Constructor
	 */
	public SampledNavigationData(INavigationData originalNavigation, SamplingMode sMode, int sValue)
			throws GIOException {
		this.originalNavigation = originalNavigation;
		samples = NviExporter.applySampling(originalNavigation, sMode, sValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFileName() {
		return originalNavigation.getFileName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return samples.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getTime(int index) throws GIOException {
		return originalNavigation.getTime(samples.get(index));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getLongitude(int index) throws GIOException {
		return originalNavigation.getLongitude(samples.get(index));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getLatitude(int index) throws GIOException {
		return originalNavigation.getLatitude(samples.get(index));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return originalNavigation.getMetadata();
	}

	/**
	 * @return the value of the {@link NavigationMetadataType} at the specified index if exists.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> Optional<T> getMetadataValue(NavigationMetadataType<T> metadataType, int index) throws GIOException {
		for (var metadata : getMetadata()) {
			if (metadata.getType().equals(metadataType))
				return Optional.of((T) metadata.getValue(samples.get(index)));
		}
		return Optional.empty();
	}

}
