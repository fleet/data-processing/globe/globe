package fr.ifremer.globe.core.model.navigation.sensortypes;

import java.util.stream.Stream;

/**
 * This enumerator defines S7K sensor types.
 */
public enum S7KSensorType implements INavigationSensorType {

	NOT_DEFINED(-1, "Not defined"), //
	GPS(0, "GPS"), //
	GPS_DIFF(1, "Differential GPS"), //
	INERTIAL_FROM_GPS_START(2, "Start of inertial positioning system from GPS"), //
	INERTIAL_FROM_DGPS_START(3, "Start of inertial positioning system from DGPS"), //
	INERTIAL_FROM_BOTTOM_CORRELATION_START(4, "Start of inertial positioning system from bottom correlation"), //
	INERTIAL_FROM_BOTTOM_OBJECT_START(5, "Start of inertial positioning from bottom object"), //
	INERTIAL_FROM_INERTIAL_START(6, "Start of inertial positioning from inertial positioning"), //
	INERTIAL_FROM_OPTIONAL_START(7, "Start of inertial positioning from optional data"), //
	INERTIAL_TO_GPS_STOP(8, "Stop of inertial positioning system to GPS"), //
	INERTIAL_TO_DGPS_STOP(9, "Stop of inertial positioning system to DGPS"), //
	INERTIAL_TO_BOTTOM_CORRELATION_STOP(10, "Stop of inertial positioning system to bottom correlation"), //
	INERTIAL_TO_BOTTOM_OBJECT_STOP(11, "Stop of inertial positioning to bottom object"), //
	INERTIAL_TO_INERTIAL_START(12, "Start of inertial positioning to inertial positioning"), //
	INERTIAL_TO_OPTIONAL_START(13, "Start of inertial positioning to optional data"), //
	USER_DEFINED(14, "User defined");

	private String name;
	private int value;

	/** Constructor **/
	private S7KSensorType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	/** Provides sensor type **/
	public static S7KSensorType get(int value) {
		return Stream.of(values()).filter(type -> type.value == value).findAny().orElse(S7KSensorType.GPS);
	}

	/** Provides sensor type **/
	@Override
	public int getValue() {
		return this.value;
	}

	@Override
	public NmeaSensorType toNmeaSensorType() {
		return switch (this) {
		case GPS -> NmeaSensorType.GPS;
		case GPS_DIFF -> NmeaSensorType.GPS_DIFF;
		default -> NmeaSensorType.NOT_DEFINED;
		};
	}

	@Override
	public NviLegacySensorType toNviSensorType() {
		return switch (this) {
		case GPS -> NviLegacySensorType.GPS;
		case GPS_DIFF -> NviLegacySensorType.GPS_DIFF;
		default -> NviLegacySensorType.NOT_DEFINED;
		};
	}

	@Override
	public <T> T perform(INavigationSensorTypeOperation<T> navigationSensorTypeOperation) {
		return navigationSensorTypeOperation.accept(this);
	}

}
