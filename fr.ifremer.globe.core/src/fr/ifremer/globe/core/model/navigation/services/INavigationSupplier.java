/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.navigation.services;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;

/**
 * Instance of service that supports the provituon of navigation data supplier
 */
public interface INavigationSupplier {

	/**
	 * Supply a INavigationDataSupplier on the specified of navigation file.
	 * 
	 * @return the INavigationDataSupplier. Optional.empty if none
	 */
	default Optional<INavigationDataSupplier> getNavigationDataSupplier(String filePath) {
		return IFileService.grab().getFileInfo(filePath).flatMap(this::getNavigationDataSupplier);
	}

	/**
	 * Supply a INavigationDataSupplier on the specified IFileInfo.
	 * 
	 * @return the INavigationDataSupplier. Optional.empty if none
	 */
	Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo);

}
