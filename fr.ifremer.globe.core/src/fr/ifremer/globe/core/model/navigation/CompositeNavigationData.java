package fr.ifremer.globe.core.model.navigation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * {@link INavigationData} composed from several {@link INavigationData}.
 */
public class CompositeNavigationData implements INavigationData {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompositeNavigationData.class);

	/**
	 * {@link FunctionalInterface} to describe a function of {@link INavigationData}.
	 */
	@FunctionalInterface
	private interface NavigationDataFunction<R> {
		R apply(Integer index) throws GIOException;
	}

	/** Map of {@link INavigationData} : key is the "address" (=first index) of {@link INavigationData} */
	private final TreeMap<Integer, INavigationData> navigationDataMap = new TreeMap<>();

	/** Computed size **/
	private int size;

	/**
	 * Builds a {@link CompositeNavigationData} from {@link INavigationDataSupplier}.
	 */
	public static CompositeNavigationData build(List<INavigationDataSupplier> navigationDataSuppliers, boolean readonly)
			throws GIOException {
		var navigationDataList = new ArrayList<INavigationData>();
		for (var supplier : navigationDataSuppliers) {
			try {
				navigationDataList.add(supplier.getNavigationData(readonly));
			} catch (GIOException e) {
				// try to close opened resources
				for (var navigationData : navigationDataList)
					try {
						navigationData.close();
					} catch (IOException e1) {
						LOGGER.error(e1.getMessage(), e1);
					}
				throw e;
			}
		}
		navigationDataList.sort(CompositeNavigationData::navigationDataTimeCompare);
		return new CompositeNavigationData(navigationDataList);
	}

	/**
	 * @return compare navigation by time if present
	 */
	private static int navigationDataTimeCompare(INavigationData value1, INavigationData value2) {
		if(value1.size() > 0 && value2.size() > 0) {
			try {
				Optional<Long> time1 = value1.getMetadataValue(NavigationMetadataType.TIME, 0);
				Optional<Long> time2 = value2.getMetadataValue(NavigationMetadataType.TIME, 0);
				if(time1.isPresent() && time2.isPresent())
					return Long.compare(time1.get(), time2.get());
			} catch (GIOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		} 
		return 0;
	}
	
	/**
	 * Constructor : {@link CompositeNavigationData} is built with several {@link INavigationDataSupplier}.
	 */
	public CompositeNavigationData(List<INavigationData> navigationDataList) {
		for (INavigationData navigationData : navigationDataList) {
			navigationDataMap.put(size, navigationData);
			size += navigationData.size();
		}
	}

	@Override
	public void close() throws IOException {
		for (INavigationData navigationData : navigationDataMap.values())
			navigationData.close();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public String getFileName() {
		return navigationDataMap.values().stream().map(INavigationData::getFileName).collect(Collectors.joining(";"));
	}

	/**
	 * @param <T> : computed value type.
	 * @param index : global index
	 * @param functionProvider : provides the method to call of {@link INavigationData}.
	 * 
	 * @return computed value.
	 */
	private <T> T getValue(int index, Function<INavigationData, NavigationDataFunction<T>> functionProvider)
			throws GIOException {
		var entry = navigationDataMap.floorEntry(index);
		var localIndex = index - entry.getKey();
		return functionProvider.apply(entry.getValue()).apply(localIndex);
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return getValue(index, navigationData -> navigationData::getLatitude);
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return getValue(index, navigationData -> navigationData::getLongitude);
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return navigationDataMap.values().stream().flatMap(navigationData -> navigationData.getMetadata().stream())
				.collect(Collectors.toList());
	}

	@Override
	public <T> Optional<T> getMetadataValue(NavigationMetadataType<T> metadata, int index) throws GIOException {
		return getValue(index, navigationData -> i -> navigationData.getMetadataValue(metadata, i));
	}
}
