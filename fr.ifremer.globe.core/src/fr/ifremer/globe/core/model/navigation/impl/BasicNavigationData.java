package fr.ifremer.globe.core.model.navigation.impl;

import java.util.Collections;
import java.util.List;
import java.util.function.IntToDoubleFunction;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Basic implementation of {@link INavigationData}.
 */
public class BasicNavigationData implements INavigationData {

	private final String name;
	private final int size;
	private final IntToDoubleFunction longitudeSupplier;
	private final IntToDoubleFunction latitudeSupplier;

	/**
	 * Constructor
	 */
	public BasicNavigationData(String name, int size, IntToDoubleFunction longitudeSupplier,
			IntToDoubleFunction latitudeSupplier) {
		super();
		this.name = name;
		this.size = size;
		this.longitudeSupplier = longitudeSupplier;
		this.latitudeSupplier = latitudeSupplier;
	}

	@Override
	public String getFileName() {
		return name;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return latitudeSupplier.applyAsDouble(index);
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return longitudeSupplier.applyAsDouble(index);
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return Collections.emptyList();
	}

}
