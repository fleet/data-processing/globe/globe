package fr.ifremer.globe.core.model.navigation;

import java.time.Instant;
import java.util.IllegalFormatConversionException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import fr.ifremer.globe.core.model.navigation.NavigationMetadata.MetadataValueSupplier;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType;
import fr.ifremer.globe.utils.units.Units;

/**
 * Defines a type of {@link NavigationMetadata}.
 *
 * @param <T> : class of value provided by this {@link NavigationMetadata}.
 */
public class NavigationMetadataType<T> {

	/** List of classic {@link NavigationMetadataType} **/
	public static final NavigationMetadataType<Long> TIME = new NavigationMetadataType<>("Time", "",
			l -> Instant.ofEpochMilli(l).toString());
	public static final NavigationMetadataType<Float> ELEVATION = new NavigationMetadataType<>("Elevation",
			Units.METERS);
	public static final NavigationMetadataType<Float> WATERLINE_TO_CHART_DATUM = new NavigationMetadataType<>(
			"Waterline", Units.METERS);
	public static final NavigationMetadataType<Float> TX_TRANSDUCER_DEPTH = new NavigationMetadataType<>(
			"Transducer depth", "m");
	public static final NavigationMetadataType<Float> ALTITUDE = new NavigationMetadataType<>("Altitude", Units.METERS);
	public static final NavigationMetadataType<Float> HEADING = new NavigationMetadataType<>("Heading", Units.DEGREES,
			0f, 360f);
	public static final NavigationMetadataType<INavigationSensorType> SENSOR_TYPE = new NavigationMetadataType<>(
			"Sensor type") {

		/** {@inheritDoc} */
		@Override
		public Number parseValueToNumber(INavigationSensorType navigationSensorType) {
			return navigationSensorType.getValue();
		}
	};
	public static final NavigationMetadataType<Float> SPEED = new NavigationMetadataType<>("Speed");
	public static final NavigationMetadataType<Integer> QUALITY = new NavigationMetadataType<>("Quality");
	public static final NavigationMetadataType<Integer> SYNTHETIC_MODE = new NavigationMetadataType<>("Synthetic Mode");

	/** Properties **/
	public final String name;
	public final String unit;
	public final Optional<T> minValue;
	public final Optional<T> maxValue;
	private final Function<T, String> valueToString;

	/** Constructors **/

	public NavigationMetadataType(String name, String unit, Optional<T> minValue, Optional<T> maxValue,
			Function<T, String> valueToString) {
		this.name = name;
		this.unit = unit;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.valueToString = valueToString;
	}

	public NavigationMetadataType(String name, String unit, T minValue, T maxValue, Function<T, String> valueToString) {
		this(name, unit, Optional.of(minValue), Optional.of(maxValue), valueToString);
	}

	public NavigationMetadataType(String name, String unit, T minValue, T maxValue) {
		this(name, unit, Optional.of(minValue), Optional.of(maxValue), v -> String.format("%,.2f ", v));
	}

	public NavigationMetadataType(String name, String unit, Function<T, String> valueToString) {
		this(name, unit, Optional.empty(), Optional.empty(), valueToString);
	}

	public NavigationMetadataType(String name) {
		this(name, "", Optional.empty(), Optional.empty(), v -> String.format("%,.2f ", v));
	}

	public NavigationMetadataType(String name, String unit) {
		this(name, unit, Optional.empty(), Optional.empty(), v -> String.format("%,.2f ", v) + unit);
	}

	// Comparators 
	 @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        var type = (NavigationMetadataType<T>) obj;
        return name.equals(type.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
	    
	// GETTERS

	public String getName() {
		return name;
	}

	public String getNameWithUnit() {
		return name.toLowerCase() + (unit.isEmpty() ? "" : " [" + unit + "]");
	}

	/**
	 * Parses the value to {@link Number} (useful to get color values)
	 */
	public Number parseValueToNumber(T value) {
		return (Number) value;
	}

	/**
	 * Parses the value to {@link String}
	 */
	public String parseValueToString(T value) {
		try {
			return valueToString != null ? valueToString.apply(value)
					: String.format("%,.2f", parseValueToNumber(value));
		} catch (IllegalFormatConversionException | ClassCastException ex) {
			return value.toString();
		}
	}

	/**
	 * @return new {@link NavigationMetadata} built with the provided {@link MetadataValueSupplier}.
	 */
	public NavigationMetadata<T> withValueSupplier(MetadataValueSupplier<T> valueSupplier) {
		return new NavigationMetadata<>(this, valueSupplier);
	}

}