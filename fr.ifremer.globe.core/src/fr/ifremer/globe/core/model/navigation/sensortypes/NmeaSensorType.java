package fr.ifremer.globe.core.model.navigation.sensortypes;

import java.util.stream.Stream;

/**
 * This enumerator defines nmea sensor types.
 */
public enum NmeaSensorType implements INavigationSensorType {

	NOT_DEFINED(0, "Not defined"), // 0 - fix not available,
	GPS(1, "GPS"), // GPS fix,
	GPS_DIFF(2, "Differential GPS"), // Differential GPS fix (values above 2 are 2.3 features)
	PPS(3, "Precise GPS"), // PPS fix
	RTK(4, "Real Time Kinematic"), //
	FLOAT_RTK(5, "Float RTK"), //
	ESTIMATED(6, "Dead reckoning"), // estimated (dead reckoning)
	MANUAL(7, "Manual input mode"), //
	EMULATED(8, "Simulation mode"); //

	/** Properties **/
	private String name;
	private int value;

	/** Constructor **/
	private NmeaSensorType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	/** Provides sensor type **/
	public static NmeaSensorType get(int value) {
		return Stream.of(values()).filter(type -> type.value == value).findAny().orElse(NmeaSensorType.NOT_DEFINED);
	}

	/** Provides sensor type **/
	@Override
	public int getValue() {
		return this.value;
	}

	@Override
	public <T> T perform(INavigationSensorTypeOperation<T> navigationSensorTypeOperation) {
		return navigationSensorTypeOperation.accept(this);
	}

	@Override
	public NmeaSensorType toNmeaSensorType() {
		return this;
	}

	@Override
	public NviLegacySensorType toNviSensorType() {
		return switch (this) {
		case GPS -> NviLegacySensorType.GPS;
		case GPS_DIFF -> NviLegacySensorType.GPS_DIFF;
		case PPS -> NviLegacySensorType.GPS_DIFF;
		case RTK -> NviLegacySensorType.CINEMATIC_GPS;
		case FLOAT_RTK -> NviLegacySensorType.RTK_FLOAT;
		default -> NviLegacySensorType.NOT_DEFINED;
		};
	}

}
