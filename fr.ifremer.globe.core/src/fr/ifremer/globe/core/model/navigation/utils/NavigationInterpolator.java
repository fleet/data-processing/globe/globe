package fr.ifremer.globe.core.model.navigation.utils;

import java.time.Instant;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class computes interpolated values from a {@link INavigationData}.
 */
public class NavigationInterpolator {

	/** Private fields **/
	private final INavigationData navigationData;
	private final TreeMap<Long, Integer> indexesByTime = new TreeMap<>();

	/**
	 * Constructor
	 */
	public NavigationInterpolator(INavigationData navData) throws GIOException {
		super();
		this.navigationData = navData;
		// Sort navigation indexes by time (keep only valid positions)
		for (int index : navData) {
			if (navData.isValid(index))
				indexesByTime.put(navData.getTime(index), index);
		}
	}

	/** Private interface to get value (allows exception) **/
	private interface ValueProvider {
		double get(int value) throws GIOException;
	}

	/**
	 * Does the interpolation.
	 */
	private double getInterpolatedValue(long time, ValueProvider valueProvider) throws GIOException {
		// Get a position before and after : pass if not found
		Entry<Long, Integer> previous = indexesByTime.floorEntry(time);
		Entry<Long, Integer> next = indexesByTime.ceilingEntry(time);
		if (previous == null)
			return valueProvider.get(next.getValue());
		else if (next == null)
			return valueProvider.get(previous.getValue());

		// Compute interpolated latitude and longitude
		double coeff = !next.getKey().equals(previous.getKey())
				? ((double) (time - previous.getKey())) / (next.getKey() - previous.getKey())
				: 0;

		double previousValue = valueProvider.get(previous.getValue());
		double nextValue = valueProvider.get(next.getValue());

		return previousValue + coeff * (nextValue - previousValue);
	}

	// PUBLIC API

	public INavigationData getNavigationData() {
		return navigationData;
	}

	public double getLatitude(long time) throws GIOException {
		return getInterpolatedValue(time, navigationData::getLatitude);
	}

	public double getLatitude(Instant instant) throws GIOException {
		return getInterpolatedValue(instant.toEpochMilli(), navigationData::getLatitude);
	}

	public double getLongitude(long time) throws GIOException {
		return getInterpolatedValue(time, navigationData::getLongitude);
	}

	public double getLongitude(Instant instant) throws GIOException {
		return getInterpolatedValue(instant.toEpochMilli(), navigationData::getLongitude);
	}

	public double getMetadata(NavigationMetadataType<Float> metadataType, long time) throws GIOException {
		return getInterpolatedValue(time, index -> navigationData.getMetadataValue(metadataType, index).orElseThrow());
	}

	public double getMetadata(NavigationMetadataType<Float> metadataType, Instant instant) throws GIOException {
		return getInterpolatedValue(instant.toEpochMilli(), index -> navigationData.getMetadataValue(metadataType, index).orElseThrow());
	}

}
