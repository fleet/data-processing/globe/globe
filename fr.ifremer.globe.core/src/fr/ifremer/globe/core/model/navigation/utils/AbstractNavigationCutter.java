package fr.ifremer.globe.core.model.navigation.utils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.function.Predicate;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class computes time intervals by filtering a {@link INavigationData}.
 */
public abstract class AbstractNavigationCutter extends GProcess implements Predicate<Integer> {

	/** Private fields **/
	protected final INavigationData navigationData;
	protected final TreeMap<Instant, Integer> indexByInstant = new TreeMap<>();
	protected List<Pair<Instant, Instant>> cutLines = new ArrayList<>();

	/**
	 * Constructor
	 */
	protected AbstractNavigationCutter(INavigationData navData) throws GIOException {
		super("Navigation cutter");
		this.navigationData = navData;
		// Sort navigation indexes by time (keep only valid positions)
		for (int index : navData)
			if (navData.isValid(index))
				indexByInstant.put(navData.getInstant(index), index);
	}

	/**
	 * Runs the process to filter navigation data and extract valid time intervals (= cut lines).
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		logger.debug("Compute cut line ...");
		cutLines = new ArrayList<Pair<Instant, Instant>>();
		Instant startPeriod = null;
		boolean isPreviousValid = false;

		monitor.beginTask("Compute cut lines...", indexByInstant.size());
		for (var current : indexByInstant.entrySet()) {
			var isCurrentValid = test(current.getValue());

			if (isCurrentValid != isPreviousValid) {
				if (isCurrentValid)
					startPeriod = current.getKey();
				else
					cutLines.add(new Pair<>(startPeriod, indexByInstant.lowerKey(current.getKey())));
			}
			isPreviousValid = isCurrentValid;
			monitor.worked(1);
			if (monitor.isCanceled()) {
				// cancel called by user ?
				cutLines.clear();
				return Status.CANCEL_STATUS;
			}
		}

		if (isPreviousValid)
			cutLines.add(new Pair<>(startPeriod, indexByInstant.lastKey()));

		logger.debug("Result : {} cut lines.", cutLines.size());
		return Status.OK_STATUS;
	}

	// GETTERS & SETTERS

	public List<Pair<Instant, Instant>> getCutLines() {
		return cutLines;
	}

}
