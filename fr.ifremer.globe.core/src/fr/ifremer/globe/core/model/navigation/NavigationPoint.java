package fr.ifremer.globe.core.model.navigation;

import java.time.Instant;

public class NavigationPoint {

	public final double lon;
	public final double lat;
	public final Instant date;

	public NavigationPoint(double lon, double lat, Instant date) {
		super();
		this.lon = lon;
		this.lat = lat;
		this.date = date;
	}

}
