package fr.ifremer.globe.core.model.navigation.sensortypes;

import java.util.stream.Stream;

/**
 * This enumerator defines NVI sensor types.
 */
public enum NviLegacySensorType implements INavigationSensorType {

	NOT_DEFINED(0, "Not defined"), //
	GPS(1, "GPS"), //
	GPS_DIFF(2, "Differential GPS"), //
	TRANSIT(3, "Transit"), //
	MINIRANGER(4, "Miniranger"), //
	SEAFIX(5, "Seafix"), //
	CINEMATIC_GPS(6, "Cinematic GPS RTK"), //
	TRIDENT(7, "Trident"), //
	AXYLE(8, "Axyle"), //
	OPTIC(9, "Optic"), //
	LORAN(10, "Loran"), //
	DECCA(11, "Decca"), //
	SYLEDIS(12, "Syledis"), RTK_FLOAT(13, "RTK Float");

	/** Properties **/
	private String name;
	private int value;

	/** Constructor **/
	private NviLegacySensorType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	/** Provides sensor type **/
	public static NviLegacySensorType get(int value) {
		return Stream.of(values()).filter(type -> type.value == value).findAny().orElse(NviLegacySensorType.NOT_DEFINED);
	}

	@Override
	public int getValue() {
		return this.value;
	}

	@Override
	public NmeaSensorType toNmeaSensorType() {
		return switch (this) {
		case GPS -> NmeaSensorType.GPS;
		case GPS_DIFF -> NmeaSensorType.GPS_DIFF;
		case CINEMATIC_GPS -> NmeaSensorType.RTK;
		case RTK_FLOAT -> NmeaSensorType.FLOAT_RTK;
		default -> NmeaSensorType.NOT_DEFINED;
		};
	}

	@Override
	public NviLegacySensorType toNviSensorType() {
		return this;
	}

	@Override
	public <T> T perform(INavigationSensorTypeOperation<T> navigationSensorTypeOperation) {
		return navigationSensorTypeOperation.accept(this);
	}

}
