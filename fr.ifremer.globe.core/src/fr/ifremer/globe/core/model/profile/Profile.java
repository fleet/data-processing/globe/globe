package fr.ifremer.globe.core.model.profile;

import java.time.Instant;

import fr.ifremer.globe.core.utils.Pair;

/**
 * This class represents a time interval with an ID.
 * 
 * TODO : use {@link Instant} class
 */
public class Profile implements Comparable<Profile> {

	public static final String DEFAULT_PROFILE = "## Default line ##";
	public static final String DEFAULT_ID = "Default line";

	// This is profile date used in line editor
	private long startDate = -1;
	private long endDate = -1;
	private String id = DEFAULT_PROFILE;

	public Profile(long startDate, long endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Profile(String id, long startDate, long endDate) {
		this(startDate, endDate);
		this.id = id;
	}

	@Override
	public int compareTo(Profile o) {
		if (startDate > o.startDate)
			return 1;
		else if (startDate == o.startDate)
			return 0;
		return -1;
	}

	public String getId() {
		return id;
	}

	public long getStartDate() {
		return startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public boolean contains(long date) {
		return date >= startDate && date <= endDate;
	}

	public Pair<Instant, Instant> toInstantPair() {
		return new Pair<Instant, Instant>(Instant.ofEpochMilli(startDate), Instant.ofEpochMilli(endDate));
	}
}
