package fr.ifremer.globe.core.model.profile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.FileFormatException;

public class ProfileUtils {
	/** Date format pattern */
	public static final String DATE_PATTERN = "dd/MM/yyyy"; //$NON-NLS-1$

	/** Time format pattern */
	public static final String TIME_PATTERN = "HH:mm:ss.SSS"; //$NON-NLS-1$

	/**
	 * Writes given profile to buffer
	 */
	public static void writeProfile(BufferedWriter writer, Profile profile) throws IOException {
		Date firstDate = new Date(profile.getStartDate());
		Date lastDate = new Date(profile.getEndDate());
		String firstDateString = DateUtils.formatDate(firstDate, DATE_PATTERN);
		String firstTimeString = DateUtils.formatDate(firstDate, TIME_PATTERN);
		String lastDateString = DateUtils.formatDate(lastDate, DATE_PATTERN);
		String lastTimeString = DateUtils.formatDate(lastDate, TIME_PATTERN);
		String format = "> %s  %s  %s  %s  %s";
		String toString = String.format(format, firstDateString, firstTimeString, lastDateString, lastTimeString,
				profile.getId());
		writer.write(toString);
		writer.newLine();
	}

	/**
	 * Write given profiles to ASCII .cut file
	 * 
	 * @param profiles
	 * @param file
	 * @throws IOException
	 */
	public static void writeProfilesToCutFile(final Collection<Profile> profiles, File file) throws IOException {
		if (file.exists()) {
			file.delete();
		}
		file.getParentFile().mkdirs();
		file.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		Collections.sort((List<Profile>) profiles);
		for (Profile profile : profiles) {
			writeProfile(writer, profile);
		}
		writer.flush();
		writer.close();
	}

	/**
	 * Load lines from given file
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws FileFormatException
	 */
	public static Set<Profile> loadProfilesFromFile(File file) throws IOException, FileFormatException {
		// Using a LinkedHashSet to keep the order in which lines were inserted into the set
		Set<Profile> profiles = new LinkedHashSet<>();
		List<?> rawLines = FileUtils.readLines(file, Charset.defaultCharset());
		for (Object rawline : rawLines) {
			String line = (String) rawline;
			if (!line.trim().isEmpty() && line.startsWith(">")) {
				// line format:
				// >, Date1, Time1, Date2, Time2, id
				String[] tokens = line.split("\\s+", 6);
				if (tokens.length == 6 && tokens[0].equals(">")) {
					String id = tokens[5];
					Date startDate = DateUtils.parseDate(tokens[1] + " " + tokens[2]);
					Date endDate = DateUtils.parseDate(tokens[3] + " " + tokens[4]);
					profiles.add(new Profile(id, startDate.getTime(), endDate.getTime()));
				} else {
					throw new FileFormatException("in file " + file.getName() + ", invalid line format : " + line);
				}
			}
		}
		return profiles;
	}

	/**
	 * Detect if the ping date is in a time interval
	 * 
	 * @param pingDate
	 * @return true if ping is in interval
	 */
	public static boolean isPingInTimeInterval(Instant pingTime, Set<Profile> timeProfileSet,
			Set<Profile> timeProfileContainingPingSet) {
		// Detect if ping is in a time interval
		boolean isPingInTimeInterval = false;
		if (timeProfileSet == null) {
			isPingInTimeInterval = true; // no timeInterval Option
		} else {
			for (Profile profile : timeProfileSet) {
				if (pingTime.toEpochMilli() >= profile.getStartDate() && pingTime.toEpochMilli() <= profile.getEndDate()) {
					isPingInTimeInterval = true;
					timeProfileContainingPingSet.add(profile);
					break;
				}
			}
		}
		return isPingInTimeInterval;
	}

}
