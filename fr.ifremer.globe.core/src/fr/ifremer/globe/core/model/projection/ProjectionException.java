package fr.ifremer.globe.core.model.projection;

import fr.ifremer.globe.utils.exception.GException;

public class ProjectionException extends GException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8205642859455699357L;
	/**
	 * @see ProjectionException#ProjectionException(String)
	 * */
	public ProjectionException(String message) {
		super(message);
	}
	/**
	 * @see ProjectionException#ProjectionException(String, Throwable)
	 * */
	public ProjectionException(String message, Throwable cause) {
		super(message, cause);
	}

}
