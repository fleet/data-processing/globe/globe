package fr.ifremer.globe.core.model.projection;

import org.gdal.osr.SpatialReference;

import fr.ifremer.globe.gdal.GdalUtils;

/**
 * This class contains the projection details name and proj4 string
 */
public record ProjectionSettings(String name, String proj4String) {

	/** Friendly names of projections */
	public static final String LAT_LON = "LatLon";
	public static final String UTM = "UTM";
	public static final String MERCATOR = "Mercator";
	public static final String LAMBERT = "Lambert";
	public static final String LAMBERT_93 = "Lambert 93";
	public static final String LAMBERT_PARIS = "NTF (Paris) / Lambert zone II";
	public static final String LAMBERT_EURO = "France Euro Lambert";
	public static final String EQUIDISTANT = "Equidistant";

	/** Constructor with Proj4 settings. Infer the name */
	public ProjectionSettings(String proj4String) {
		this(getProjName(proj4String), proj4String);
	}

	public static String getProjName(String projString) {
		String name = "Custom";

		if (projString.contains("+proj=longlat")) {
			return LAT_LON;
		} else if (projString.contains("+proj=merc")) {
			name = MERCATOR;
		} else if (projString.contains("+proj=utm")) {
			name = UTM;
		} else if (projString.contains("+proj=eqc")) {
			name = EQUIDISTANT;

		} else if (projString.contains("+proj=lcc")) {
			if (isLambertProjectionName(projString, StandardProjection.LAMBERT_PARIS_PROJ4)) {
				name = LAMBERT_PARIS;
			} else if (isLambertProjectionName(projString, StandardProjection.LAMBERT_EURO_PROJ4)) {
				name = LAMBERT_EURO;
			} else if (isLambertProjectionName(projString, StandardProjection.LAMBERT_93_PROJ4)) {
				name = LAMBERT_93;
			} else {
				name = LAMBERT;
			}
		}
		return name;
	}

	public boolean isLongLatProjection() {
		return proj4String.toLowerCase().contains("+proj=longlat");
	}

	public static boolean isLongLatProjection(Projection projection) {
		if (projection != null) {
			String proj4String = projection.getProjectionSettings().proj4String;
			return proj4String.toLowerCase().contains("+proj=longlat");
		}
		return false;
	}

	/**
	 * Do these this and the other spatial references describe the same system ?
	 */
	public boolean isSame(ProjectionSettings other) {
		if (other == null) {
			return false;
		}
		SpatialReference mySRS = new SpatialReference();
		mySRS.ImportFromProj4(proj4String);
		SpatialReference otherSRS = new SpatialReference();
		otherSRS.ImportFromProj4(other.proj4String);
		boolean result = mySRS.IsSame(otherSRS) == GdalUtils.TRUE;
		mySRS.delete();
		otherSRS.delete();
		return result;
	}

	/**
	 * @return string WKT representation of the projection, with correct fields. Handles MERCATOR 1SP or 2SP.
	 */
	public String getProj4FormattedWkt(boolean convertToMerc1SP) {
		var proj4 = proj4String;
		if (!proj4.contains("+datum=WGS84"))
			proj4 = proj4.replace("+ellps=WGS84", "+ellps=WGS84 +datum=WGS84");
		if (convertToMerc1SP) {
			if (proj4.contains("+proj=merc")) {
				String newProj4 = "";
				String[] patterns = proj4.split("[ ]");
				SpatialReference srs = new SpatialReference();
				srs.ImportFromProj4(proj4);
				for (String p : patterns) {
					if (p.contains("+lat_ts=")) {
						double value = Math.toRadians(Double.parseDouble(p.replace("+lat_ts=", "")));
						// retrieve ellipsoid, use gdal for this double e=0.0818191908426215;
						double f = srs.GetInvFlattening();
						double e1 = Math.sqrt((2 - 1 / f) / f);
						double k_0 = Math.cos(value) / Math.sqrt(1 - Math.pow(e1 * Math.sin(value), 2));
						newProj4 += "+k_0=" + k_0 + " ";
					} else {
						newProj4 += p + " ";
					}
				}
				proj4 = newProj4;
			}
		}
		SpatialReference ref = new SpatialReference();
		ref.ImportFromProj4(proj4);
		return ref.ExportToWkt();
	}

	private static boolean isLambertProjectionName(String projString, String projectionSettingsLambert) {
		boolean ret = false;
		String[] parametersProjString = projString.split(" ");
		String[] parametersProjLambert = projectionSettingsLambert.split(" ");
		if (parametersProjString.length == parametersProjLambert.length) {
			ret = true;
			for (String param : parametersProjString) {
				if (!projectionSettingsLambert.contains(param)) {
					ret = false;
					break;
				}
			}
		}
		return ret;
	}

}
