package fr.ifremer.globe.core.model.projection;

import java.util.Optional;

/**
 * Maintain a list a known projection and their parameters
 */
public class StandardProjection {

	public static final String LAMBERT_PARIS_PROJ4 = "+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +ellps=clrk80ign +pm=paris +towgs84=-168,-60,320,0,0,0,0 +units=m +no_defs"; // EPSG:27572

	public static final String LAMBERT_EURO_PROJ4 = "+proj=lcc +lat_0=52 +lon_0=10 +lat_1=35 +lat_2=65 +x_0=4000000 +y_0=2800000 +ellps=GRS80 +units=m +no_defs"; // EPSG:3034

	public static final String LAMBERT_93_PROJ4 = "+proj=lcc +lat_0=46.5 +lon_0=3 +lat_1=49 +lat_2=44 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"; // EPSG:2154

	public static final String TRANSVERSE_MERCATOR_PROJ4 = "+proj=tmerc +lon_0=%s +lat_0=%s +k_0=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs";

	/** */
	public static final String MERCATOR_LON = "+lon_0";
	public static final String MERCATOR_LAT = "+lat_ts";
	public static final ProjectionSettings MERCATOR = new ProjectionSettings("+proj=merc " + MERCATOR_LON + "=0 "
			+ MERCATOR_LAT + "=0 +k_0=1 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs");
	public static final ProjectionSettings MERCATOR_PATTERN = new ProjectionSettings("+proj=merc " + MERCATOR_LON
			+ "=0 " + MERCATOR_LAT + "= +k_0=1 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs");

	/** */
	public static final ProjectionSettings UTM = new ProjectionSettings(
			"+proj=utm +zone=30 +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
	public static final ProjectionSettings UTM_PATTERN = new ProjectionSettings(
			"+proj=utm +zone= +ellps=WGS84 +datum=WGS84 +units=m +no_defs");

	/** */
	public static final ProjectionSettings LONGLAT = new ProjectionSettings(
			"+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

	/** */
	public static final ProjectionSettings EQUIDISTANT = new ProjectionSettings(
			"+proj=eqc +lat_ts=0 +lat_0=0 +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs");

	/** */
	public static final ProjectionSettings LAMBERT_EURO = new ProjectionSettings(LAMBERT_EURO_PROJ4);

	/** */
	public static final ProjectionSettings LAMBERT_PARIS = new ProjectionSettings(LAMBERT_PARIS_PROJ4);

	/** */
	public static final ProjectionSettings LAMBERT_93 = new ProjectionSettings(LAMBERT_93_PROJ4);

	/** */
	public static final ProjectionSettings CUSTOM = new ProjectionSettings("");

	/** Returns the standard projection found from its name */
	public static Optional<ProjectionSettings> fromFriendlyName(String name) {
		return Optional.ofNullable(switch (name) {
		case ProjectionSettings.LAT_LON -> LONGLAT;
		case ProjectionSettings.EQUIDISTANT -> EQUIDISTANT;
		case ProjectionSettings.LAMBERT_93 -> LAMBERT_93;
		case ProjectionSettings.LAMBERT_PARIS -> LAMBERT_PARIS;
		case ProjectionSettings.LAMBERT_EURO -> LAMBERT_EURO;
		case ProjectionSettings.MERCATOR -> MERCATOR;
		case ProjectionSettings.UTM -> UTM;
		default -> null;
		});
	}

	/**
	 * @return a Transverse Mercator ProjectionSettings
	 * @param lon0 Longitude of projection center
	 * @param latTs Latitude of true scale
	 */
	public static ProjectionSettings getTransverseMercatorProjectionSettings(double lon0, double latTs) {
		return new ProjectionSettings(
				String.format(TRANSVERSE_MERCATOR_PROJ4, String.valueOf(lon0), String.valueOf(latTs)));
	}

	/** Utility class. No useful instance */
	private StandardProjection() {
	}

}
