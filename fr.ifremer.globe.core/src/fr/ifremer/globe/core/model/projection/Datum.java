package fr.ifremer.globe.core.model.projection;

import fr.ifremer.globe.utils.mbes.Ellipsoid;

public enum Datum {
	WGS84(Ellipsoid.WGS84), NAD83(Ellipsoid.GRS80);

	public Ellipsoid ellipsoid;

	Datum(Ellipsoid e) {
		this.ellipsoid = e;
	}
}
