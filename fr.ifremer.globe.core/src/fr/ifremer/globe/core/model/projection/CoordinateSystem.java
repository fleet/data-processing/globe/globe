package fr.ifremer.globe.core.model.projection;

/**
 * Indicate a coordinate system in which sounding data refers
 * <p>
 * <b>Vessel Coordinate System (VCS)</b>
 * <p>
 * Origin of the VCS is the vessel reference point. The VCS is defined according to the right hand rule, 
 * <p>x-axis pointing forward parallel to the vessel main axis.
 * <p> y-axis pointing starboard parallel to the deck plane.
 * <p>z-axis pointing down parallel to the mast. 
 * Rotation of the vessel coordinate system around an axis is defined as positive in the clockwise direction, also according to the right hand rule. 
 * <p>Roll - rotation around the x-axis. 
 * <p>Pitch rotation around the y-axis.
 * <p> Heading - rotation around the z-axis. Heading as input in depth calculations is sensor
 * data referred to true north.
 * 
 * <p><b>Array Coordinate System (ACS) </b><p>
 * Origin of the ACS is at the centre of the array face. 
 * <p>The ACS is defined according to the right hand rule. 
 * <p>x-axis pointing forward along the array. 
 * <p>y-axis pointing starboard along the array plane. 
 * <p>z-axis pointing down orthogonal to the array plane. 
 * <p>
 * <b>Surface Coordinate System (SCS) </b>
 * Origin of the SCS is the vessel reference point at the time of transmission. The SCS is defined according to the right hand rule. 
 * <p> x-axis pointing forward along
 * the horizontal projection of the vessel main axis. 
 * <p>y-axis pointing horizontally to starboard, orthogonal to the
 * horizontal projection of the vessel main axis. 
 * <p> z-axis pointing down along the g-vector. To move SCS into the waterline, use reference point height corrected for roll and pitch at the time of transmission. 
 * 
 * <p>
 * <b>Fixed Coordinate System (FCS) </b>
 * Origo of the FCS is fixed somewhere in the nominal sea surface. The FCS is defined according to the
 * right hand rule. 
 * <p>x-axis pointing north. 
 * <p>y-axis pointing east. 
 * <p>z-axis pointing down along the g-vector.
 * 
 */
public enum CoordinateSystem {

	VCS("VCS"), ACS("ACS"), SCS("SCS"), FCS("FCS");

	private final String name;
	
	
	private CoordinateSystem(String name)
	{
		this.name=name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	
}
