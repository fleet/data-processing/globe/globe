package fr.ifremer.globe.core.model.projection;

import org.gdal.osr.SpatialReference;

public class ESRIWKTConverter {
	
	public static String ESRIWKTtoProj4(String esri_pe_string )
	{
		if (esri_pe_string.contains("Lambert")) {
			if (esri_pe_string.contains("Scale_Factor")) {
				esri_pe_string = esri_pe_string.replace("Lambert_Conformal_Conic", "Lambert_Conformal_Conic_1SP");
			} else {
				esri_pe_string = esri_pe_string.replace("Lambert_Conformal_Conic", "Lambert_Conformal_Conic_2SP");							
			}
		} else if(esri_pe_string.contains("\"Mercator\""))  {
			if (esri_pe_string.contains("Scale_Factor")) {
				esri_pe_string = esri_pe_string.replace("Mercator", "Mercator_1SP");
			} else {
				esri_pe_string = esri_pe_string.replace("Mercator", "Mercator_2SP");							
			}

		} else if(esri_pe_string.contains("Equidistant_Cylindrical"))  {
			esri_pe_string = esri_pe_string.replace("Equidistant_Cylindrical", "Equirectangular");
		}

		//f (!esri_pe_string.contains("Equidistant_Cylindrical") && !esri_pe_string.contains("\"Mercator\"") && !esri_pe_string.contains("Lambert")) {
			SpatialReference ref=new SpatialReference();
			ref.MorphFromESRI();
			ref.ImportFromWkt(esri_pe_string);
			return ref.ExportToProj4();
		//}
		//else
			//return esriWKTToProj4(esri_pe_string);
	}
}
