package fr.ifremer.globe.core.model.projection;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.gdal;
import org.gdal.osr.CoordinateTransformation;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.utils.function.DoubleIndexedConsumer;
import fr.ifremer.globe.utils.function.DoubleIndexedSupplier;
import fr.ifremer.globe.utils.mbes.Ellipsoid;

public class Projection {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(Projection.class);

	private ProjectionSettings projectionSettings = null;
	private Ellipsoid ellipsoid = Ellipsoid.NONE;

	/** Transformation from geographical space to projected space */
	// TODO GB @XStreamOmitField
	private CoordinateTransformation ct;
	/** Transformation from projected space to geographical space */
	// TODO GB @XStreamOmitField
	private CoordinateTransformation ctInverse;

	private double[] projectedNW = new double[2];
	private double[] projectedSW = new double[2];
	private double[] projectedSE = new double[2];
	private double[] projectedNE = new double[2];

	/** Property change support */
	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	/** Name for projection changed property */
	public static final String PROJECTION_CHANGED = "PROJECTION_CHANGED";

	/**
	 * Compute and return a new Spatial Reference
	 */
	public SpatialReference getSpatialReference() {
		return createProjectionSRS(getProj4ProjectionString());
	}

	public Projection(ProjectionSettings projectionSettings) {
		setProjectionSettings(projectionSettings);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public String getProj4ProjectionString() {
		return projectionSettings.proj4String();
	}

	public String getProj4FormattedWkt(boolean convertToMerc1SP) {
		return projectionSettings.getProj4FormattedWkt(convertToMerc1SP);
	}

	public ProjectionSettings getProjectionSettings() {
		return projectionSettings;
	}

	public boolean isLongLatProjection() {
		return projectionSettings.isLongLatProjection();
	}

	public void setProjectionSettings(ProjectionSettings type) {

		projectionSettings = type;
		initializeEllipsoid();
		initializeCt();
	}

	private void initializeEllipsoid() {
		ellipsoid = Ellipsoid.NONE;
		String proj4Projection = getProj4ProjectionString();
		String[] tokens = proj4Projection.split("[ ]");
		for (String token : tokens) {
			if (token.startsWith("+ellps=")) {
				String ellipsoidString = token.substring(7);
				for (Ellipsoid e : Ellipsoid.values()) {
					if (e.getProj4Name().toLowerCase().equals(ellipsoidString.toLowerCase())) {
						ellipsoid = e;
						break;
					}
				}
				break;
			}
		}
		if (ellipsoid == Ellipsoid.NONE) {
			// try to find user defined ellipsoid
			for (String token : tokens) {
				if (token.startsWith("+a=")) {
					ellipsoid = Ellipsoid.USER;
					ellipsoid.setHalf_A(Double.parseDouble(token.substring(3)));
				} else if (token.startsWith("+b=")) {
					ellipsoid = Ellipsoid.USER;
					ellipsoid.setHalf_B(Double.parseDouble(token.substring(3)));
				}
			}
			if (ellipsoid == Ellipsoid.USER) {
				ellipsoid.eccentricity2();
				ellipsoid.flatness();
			}
		}
	}

	public String getERSIProjectionString() {
		SpatialReference srs = new SpatialReference();
		srs.ImportFromProj4(getProj4ProjectionString());
		srs.MorphToESRI();
		return srs.ExportToWkt();
	}

	public void initializeCt() {
		SpatialReference sourceSpatialReference = createGeographicSRS();
		SpatialReference targetSpatialReference = createProjectionSRS(getProj4ProjectionString());
		if (logger.isDebugEnabled()) {
			logger.debug("Creates a CoordinateTransformation :");
			logger.debug(" - Source projection : {}", sourceSpatialReference.ExportToWkt());
			logger.debug(" - Target projection : {}", targetSpatialReference.ExportToWkt());
		}
		ct = new CoordinateTransformation(sourceSpatialReference, targetSpatialReference);
		// useful to determine the selection
		ctInverse = new CoordinateTransformation(targetSpatialReference, sourceSpatialReference);
	}

	/**
	 * Gdal geographic projection
	 *
	 * @return the gdal SpatialReference
	 */
	private SpatialReference createGeographicSRS() {
		return GdalOsrUtils.SRS_WGS84;
	}

	/**
	 * createProjectionSRS
	 *
	 * @param projection
	 * @param ellipsoid
	 * @return gdal projection
	 */
	private SpatialReference createProjectionSRS(String proj4ProjectionString) {
		SpatialReference srs = new SpatialReference();
		srs.ImportFromProj4(proj4ProjectionString);
		return srs;
	}

	public Ellipsoid getEllipsoid() {
		return ellipsoid;
	}

	/**
	 * @return: projectedSW represents the top left corner (North West) of the geographic frame (degree):
	 */
	@Deprecated
	private double[] getProjectedNW() {
		return projectedNW;
	}

	/**
	 * @return: projectedSW represents the bottom left corner (Sud West) of the geographic frame (degree)
	 */
	@Deprecated
	public double[] getProjectedSW() {
		return projectedSW;
	}

	/**
	 * @return: projectedSW represents the bottom right corner (Sud East) of the geographic frame (degree)
	 */
	@Deprecated
	private double[] getProjectedSE() {
		return projectedSE;
	}

	/**
	 * @return: projectedSW represents the top right corner (North East) of the geographic frame (degree)
	 */
	@Deprecated
	private double[] getProjectedNE() {
		return projectedNE;
	}

	/**
	 * Project geo box and extend projected points to reflect projection distortion
	 *
	 * @param box
	 * @param spatialResolution
	 * @throws ProjectionException
	 */
	@Deprecated
	public GeoBox projectAndExtends(GeoBox box, double extension) throws ProjectionException {

		setProjectedGeoBox(box);

		projectedNW[0] -= extension;
		projectedNW[1] += extension;
		projectedSW[0] -= extension;
		projectedSW[1] -= extension;
		projectedSE[0] += extension;
		projectedSE[1] -= extension;
		projectedNE[0] += extension;
		projectedNE[1] += extension;

		double unprj[] = unproject(getMinProjectedX(), getMinProjectedY());
		double unprj2[] = unproject(getMaxProjectedX(), getMaxProjectedY());
		GeoBox extendedGeoBox = new GeoBox(unprj2[1], unprj[1], unprj2[0], unprj[0]);

		return extendedGeoBox;

	}

	@Deprecated
	public void setProjectedGeoBox(GeoBox box) throws ProjectionException {
		/*
		 * Initialization of four corners from the geographic frame (box)
		 */
		projectedNW = project(box.getLeft(), box.getTop());
		projectedSW = project(box.getLeft(), box.getBottom());
		projectedSE = project(box.getRight(), box.getBottom());
		projectedNE = project(box.getRight(), box.getTop());
	}

	@Deprecated
	public double[] decenterAndUnproject(double x, double y) throws ProjectionException {
		double xOffset = getMinProjectedX() + (getMaxProjectedX() - getMinProjectedX()) / 2.0;
		double absoluteX = x + xOffset;
		double yOffset = getMinProjectedY() + (getMaxProjectedY() - getMinProjectedY()) / 2.0;
		double absoluteY = y + yOffset;
		return unproject(absoluteX, absoluteY);
	}

	@Deprecated
	public double[] projectAndCenter(double lon, double lat) throws ProjectionException {
		double[] out = new double[3];
		projectAndCenter(lon, lat, out);
		return out;
	}

	@Deprecated
	public void projectAndCenter(double lon, double lat, double[] out) throws ProjectionException {
		project(out, lon, lat);
		double xOffset = getMinProjectedX() + (getMaxProjectedX() - getMinProjectedX()) / 2.0;
		out[0] -= xOffset;
		double yOffset = getMinProjectedY() + (getMaxProjectedY() - getMinProjectedY()) / 2.0;
		out[1] -= yOffset;
	}

	/***
	 * Project the coordinate passed as parameter
	 *
	 * @param lon the longitude
	 * @param lat the latitude
	 * @return a double array containing the longitude and latitude (in that order) if projection is lat long or the
	 *         transformed coordinates
	 *
	 */
	public double[] project(double lon, double lat) throws ProjectionException {
		double[] result = projectQuietly(lon, lat);
		if (result == null) {
			throw new ProjectionException("GDAL Error: " + gdal.GetLastErrorMsg());
		}
		return result;
	}

	/***
	 * Project the coordinate passed as parameter
	 *
	 * @param lon the longitude
	 * @param lat the latitude
	 * @return a double array containing the longitude and latitude (in that order) if projection is lat long or the
	 *         transformed coordinates.<br>
	 *         Null when projection failed. Invoke {@link gdal#GetLastErrorType} to get the error
	 *
	 */
	public double[] projectQuietly(double lon, double lat) {
		double[] result = null;
		if (getProjectionSettings().isLongLatProjection()) {
			result = new double[2];
			result[0] = lon;
			result[1] = lat;
		} else {
			gdal.ErrorReset();
			double[] projectedCoords = ct.TransformPoint(lon, lat);
			if (gdal.GetLastErrorType() == 0) {
				result = projectedCoords;
			}
		}
		return result;
	}

	public void project(double[] out, double lon, double lat) throws ProjectionException {
		if (getProjectionSettings().isLongLatProjection()) {
			out[0] = lon;
			out[1] = lat;
		} else {
			gdal.ErrorReset();
			ct.TransformPoint(out, lon, lat);
			int lastError = gdal.GetLastErrorType();
			if (lastError != 0) {
				throw new ProjectionException("GDAL Error: " + gdal.GetLastErrorMsg());
			}
		}
	}

	public boolean projectQuietly(double[][] lonlat) {
		boolean result = true;
		if (getProjectionSettings().isLongLatProjection()) {
			// nothing to do
		} else {
			gdal.ErrorReset();
			// GDAL in-place transform
			ct.TransformPoints(lonlat);
			result = gdal.GetLastErrorType() == 0;
		}
		return result;
	}

	/**
	 * Project on point
	 *
	 * @param lonlatDepth three values for longitude/latitude/depth
	 * @return true on success
	 */
	public boolean projectQuietly(double[] lonlatDepth) {
		boolean result = true;
		if (getProjectionSettings().isLongLatProjection()) {
			// nothing to do
		} else {
			gdal.ErrorReset();
			// GDAL in-place transform
			ct.TransformPoint(lonlatDepth);
			result = gdal.GetLastErrorType() == 0;
		}
		return result;
	}

	public void project(double[][] lonlat) throws ProjectionException {
		gdal.ErrorReset();
		projectQuietly(lonlat);
		int lastError = gdal.GetLastErrorType();
		if (lastError != 0) {
			throw new ProjectionException("GDAL Error: " + gdal.GetLastErrorMsg());
		}
	}

	public void project(DoubleIndexedSupplier inLong, DoubleIndexedSupplier inLat, DoubleIndexedConsumer outX,
			DoubleIndexedConsumer outY, int elementCount, IProgressMonitor monitor) throws ProjectionException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, elementCount);

		int bufferSize = Math.min(elementCount, 1024);
		double[][] lonlat = new double[bufferSize][2];
		int xyIndex = 0;
		while (xyIndex < elementCount) {
			for (int i = 0; i < bufferSize; i++) {
				if (xyIndex + i < elementCount) {
					lonlat[i][0] = inLong.getAsDouble(xyIndex + i);
					lonlat[i][1] = inLat.getAsDouble(xyIndex + i);
				} else {
					// Clean buffer to avoid Proj exception
					lonlat[i][0] = 0d;
					lonlat[i][1] = 0d;
				}
			}
			project(lonlat);

			for (int i = 0; i < bufferSize && xyIndex < elementCount; i++, xyIndex++) {
				outX.accept(xyIndex, lonlat[i][0]);
				outY.accept(xyIndex, lonlat[i][1]);
				subMonitor.worked(1);
			}
		}
	}

	public double[] unprojectQuietly(double x, double y) {
		double[] result = null;
		if (getProjectionSettings().isLongLatProjection()) {
			result = new double[] { x, y };
		} else {
			gdal.ErrorReset();
			double[] projectedCoords = ctInverse.TransformPoint(x, y);
			if (gdal.GetLastErrorType() == 0) {
				result = projectedCoords;
			}
		}
		return result;
	}

	public double[] unproject(double x, double y) throws ProjectionException {
		gdal.ErrorReset();
		double[] ret = unprojectQuietly(x, y);
		if (gdal.GetLastErrorType() != 0) {
			throw new ProjectionException("GDAL Error: " + gdal.GetLastErrorMsg());
		}
		return ret;
	}

	public void unproject(double[][] lonlat) throws ProjectionException {
		// GDAL in-place transform
		gdal.ErrorReset();
		ctInverse.TransformPoints(lonlat);
		int lastError = gdal.GetLastErrorType();
		if (lastError != 0) {
			throw new ProjectionException("GDAL Error: " + gdal.GetLastErrorMsg());
		}
	}

	public boolean unprojectQuietly(double[][] lonlat) {
		// GDAL in-place transform
		gdal.ErrorReset();
		ctInverse.TransformPoints(lonlat);
		return gdal.GetLastErrorType() == 0;
	}

	public void unproject(DoubleIndexedSupplier inX, DoubleIndexedSupplier inY, DoubleIndexedConsumer outLong,
			DoubleIndexedConsumer outLat, int elementCount, IProgressMonitor monitor) throws ProjectionException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, elementCount);

		int bufferSize = Math.min(elementCount, 1024);
		double[][] lonlat = new double[bufferSize][2];
		int xyIndex = 0;
		while (xyIndex < elementCount) {
			for (int i = 0; i < bufferSize && xyIndex + i < elementCount; i++) {
				lonlat[i][0] = inX.getAsDouble(xyIndex + i);
				lonlat[i][1] = inY.getAsDouble(xyIndex + i);
			}
			unproject(lonlat);

			for (int i = 0; i < bufferSize && xyIndex < elementCount; i++, xyIndex++) {
				outLong.accept(xyIndex, lonlat[i][0]);
				outLat.accept(xyIndex, lonlat[i][1]);
				subMonitor.worked(1);
			}
		}
	}

	/**
	 *
	 * @return: maximum in X in the projection from geographic frame (box)
	 */
	@Deprecated
	public double getMaxProjectedX() {
		return Math.max(Math.max(getProjectedNW()[0], getProjectedSW()[0]),
				Math.max(getProjectedSE()[0], getProjectedNE()[0]));
	}

	/**
	 *
	 * @return: minimum in X in the projection from geographic frame (box)
	 */
	@Deprecated
	public double getMinProjectedX() {
		return Math.min(Math.min(getProjectedNW()[0], getProjectedSW()[0]),
				Math.min(getProjectedSE()[0], getProjectedNE()[0]));
	}

	/**
	 *
	 * @return: maximum in Y in the projection from geographic frame (box)
	 */
	@Deprecated
	public double getMaxProjectedY() {
		return Math.max(Math.max(getProjectedNW()[1], getProjectedSW()[1]),
				Math.max(getProjectedSE()[1], getProjectedNE()[1]));
	}

	/**
	 *
	 * @return: minimum in Y in the projection from geographic frame (box)
	 */
	@Deprecated
	public double getMinProjectedY() {
		return Math.min(Math.min(getProjectedNW()[1], getProjectedSW()[1]),
				Math.min(getProjectedSE()[1], getProjectedNE()[1]));
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Projection) {
			result = getProj4ProjectionString().equals(((Projection) obj).getProj4ProjectionString());
		} else if (obj instanceof ProjectionSettings) {
			result = getProj4ProjectionString().equals(((ProjectionSettings) obj).proj4String());
		} else if (obj instanceof String) {
			result = getProj4ProjectionString().equals(obj);
		}
		return result;
	}

	public static Projection createFromProj4String(String proj4String) {
		ProjectionSettings projType = new ProjectionSettings(proj4String);
		return new Projection(projType);
	}

	@Deprecated
	public void updateExtremumPoints(double lon, double lat) throws ProjectionException {
		double[] prj = project(lon, lat);
		projectedNW[0] = Math.min(projectedNW[0], prj[0]);
		projectedNW[1] = Math.max(projectedNW[1], prj[1]);
		projectedNE[0] = Math.max(projectedNE[0], prj[0]);
		projectedNE[1] = Math.max(projectedNE[1], prj[1]);
		projectedSE[0] = Math.max(projectedSE[0], prj[0]);
		projectedSE[1] = Math.min(projectedSE[1], prj[1]);
	}
}
