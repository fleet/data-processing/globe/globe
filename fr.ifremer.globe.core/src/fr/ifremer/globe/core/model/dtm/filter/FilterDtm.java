package fr.ifremer.globe.core.model.dtm.filter;

import java.util.Optional;
import java.util.OptionalDouble;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * Groups all elements to filter values of a layer.
 */
public record FilterDtm(//

		String resetLayerName, // Name of layer on which to apply cell reset
		String filterLayerName, // Name of the layer on which to apply the filter condition
		FilteringType type, // Filtering condition type (==, >, <...)
		Optional<String> valueCDI, // CDI value of the condition when filterLayerName is CDI_LAYER
		OptionalDouble valueA, // First value of the condition
		OptionalDouble valueB // Second value of the condition (when between condition)
) {

	public static final String ALL_LAYERS = "All";
	public static final String CDI_LAYER = "CDI";

	public static FilterDtm makeForNumericCondition(String resetLayerName, String filterLayerName, FilteringType type,
			double valueA) {
		return new FilterDtm(resetLayerName, filterLayerName, type, Optional.empty(), OptionalDouble.of(valueA),
				OptionalDouble.empty());
	}

	public static FilterDtm makeForNumericBetweenCondition(String resetLayerName, String filterLayerName, double valueA,
			double valueB) {
		return new FilterDtm(resetLayerName, filterLayerName, FilteringType.between, Optional.empty(),
				OptionalDouble.of(valueA), OptionalDouble.of(valueB));
	}

	public static FilterDtm makeForCDICondition(String resetLayerName, FilteringType type, String valueCDI) {
		return new FilterDtm(resetLayerName, CDI_LAYER, type, Optional.of(valueCDI), OptionalDouble.empty(),
				OptionalDouble.empty());
	}

	/** Reversed method of toString */
	public static FilterDtm parseString(String filterValue) {
		String[] filterElements = filterValue.split("\\^");

		String resetLayerName = filterElements.length > 0 ? filterElements[0] : ALL_LAYERS;
		String filterLayerName = filterElements.length > 1 ? filterElements[1] : CDI_LAYER;
		FilteringType type = filterElements.length > 2 ? FilteringType.resolveName(filterElements[2])
				: FilteringType.equal;

		OptionalDouble valueA = OptionalDouble.empty();
		if (filterElements.length > 3 && !filterElements[3].isEmpty())
			valueA = OptionalDouble.of(NumberUtils.toDouble(filterElements[3], 0d));

		OptionalDouble valueB = OptionalDouble.empty();
		if (filterElements.length > 4 && !filterElements[4].isEmpty())
			valueB = OptionalDouble.of(NumberUtils.toDouble(filterElements[4], 0d));

		Optional<String> valueCDI = Optional.empty();
		if (filterElements.length > 5 && !filterElements[5].isEmpty())
			valueCDI = Optional.of(filterElements[5]);

		return new FilterDtm(resetLayerName, filterLayerName, type, valueCDI, valueA, valueB);
	}

	/**
	 * @return a string representation of the object.
	 */
	@Override
	public String toString() {
		return resetLayerName //
				+ '^'//
				+ filterLayerName //
				+ '^'//
				+ type.getDisplayName() //
				+ '^'//
				+ (valueA.isPresent() ? valueA.getAsDouble() : "")//
				+ '^'//
				+ (valueB.isPresent() ? valueB.getAsDouble() : "")//
				+ '^'//
				+ (valueCDI.isPresent() ? valueCDI.get() : "");//
	}

}
