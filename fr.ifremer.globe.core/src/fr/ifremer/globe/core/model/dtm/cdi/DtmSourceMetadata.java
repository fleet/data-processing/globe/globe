package fr.ifremer.globe.core.model.dtm.cdi;

public record DtmSourceMetadata(int edmoCode, QualityIndicator qualityIndicator, DtmSourceState state) {

	public static DtmSourceMetadata defaultNotFound() {
		return new DtmSourceMetadata(0, QualityIndicator.defaultValue, DtmSourceState.eNotFound);
	}

	public static DtmSourceMetadata defaultNetworkError(int edmoCode) {
		return new DtmSourceMetadata(edmoCode, QualityIndicator.defaultValue, DtmSourceState.eConnectionError);
	}

	public static DtmSourceMetadata defaultXMLFormatError(int edmoCode) {
		return new DtmSourceMetadata(edmoCode, QualityIndicator.defaultValue, DtmSourceState.eXMLParsingError);
	}

	/**
	 * A default value considered as existing (gebco, interpolated)
	 */
	public static DtmSourceMetadata defaultExisting() {
		return new DtmSourceMetadata(0, QualityIndicator.defaultValue, DtmSourceState.eExists);
	}

	/**
	 * A default value considered as existing (gebco, interpolated)
	 */
	public static DtmSourceMetadata defaultBadURL() {
		return new DtmSourceMetadata(0, QualityIndicator.defaultValue, DtmSourceState.eURLParsingError);
	}

}
