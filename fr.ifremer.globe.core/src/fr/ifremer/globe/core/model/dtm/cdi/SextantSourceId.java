package fr.ifremer.globe.core.model.dtm.cdi;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SextantSourceId extends DtmSourceId {
	protected static Logger logger = LoggerFactory.getLogger(SeaDataNetSourceId.class);

	SextantSourceId(String id) throws MalformedURLException {
		super(id, Catalogue.Sextant);
		String urlPattern = "";
		String edmoCodePattern = "";
		String PREFIX = ConstantPattern.SDN + ConstantPattern.CPRD;
		if (id.contains(ConstantPattern.LOCAL)) { // PREFIX LOCAL
			PREFIX += ConstantPattern.LOCAL;
		}
		String myLocal = "LOCAL:LOCAL";
		// hack for bad data
		if (id.contains(myLocal)) {
			PREFIX += ConstantPattern.LOCAL;

		}
		urlPattern = ConstantPattern.KEY_CDITOOL_CPRD_URL; // url for
		// CPRD
		edmoCodePattern = id.substring(PREFIX.length());
		if (edmoCodePattern.contains("_")) {
			edmoCODE = id.substring(PREFIX.length(), id.indexOf('_'));
		} else {
			edmoCODE = id;
		}

		String urlTmp = urlPattern.replace(ConstantPattern.KEY_CDITOOL_EDMOCODE, edmoCODE);
		try {
			String encodedLocalID = URLEncoder.encode(localCDIID, StandardCharsets.UTF_8.displayName());

			displayedURL = new URL(urlTmp.replace(ConstantPattern.KEY_CDITOOL_LOCALID, encodedLocalID));

		} catch (UnsupportedEncodingException e) {
			throw new MalformedURLException("error computing URL " + e.getMessage());
		}
	}

	@Override
	public DtmSourceMetadata retrieve() {
		try {
			String urlPattern = ConstantPattern.KEY_CDITOOL_VALIDATECPRD_URL; // url for

			String urlTmp = urlPattern.replace(ConstantPattern.KEY_CDITOOL_EDMOCODE, edmoCODE);
			String encodedLocalID = URLEncoder.encode(localCDIID, StandardCharsets.UTF_8.displayName());
			URL validateURL = new URL(urlTmp.replace(ConstantPattern.KEY_CDITOOL_LOCALID, encodedLocalID));

			SextantNetXMLParser parser = new SextantNetXMLParser();
			return parser.parse(edmoCODE, validateURL);
		} catch (MalformedURLException | ParserConfigurationException | UnsupportedEncodingException e) {
			logger.error("Error while validating " + this, e);
			return DtmSourceMetadata.defaultBadURL();
		}
	}

	@Override
	public String toHTML() {
		StringBuilder sb = new StringBuilder("CDI message : " + sourceID);
		String link = "";
		link = "<br />Link :<a href=\"" + displayedURL + "\"> " + catalogue + "</a>";
		sb.append("<br />EDMO-CODE = " + edmoCODE);
		sb.append("<br />LOCAL-CDI-ID = " + localCDIID);
		sb.append(link);

		return sb.toString();
	}

}
