package fr.ifremer.globe.core.model.dtm.cdi;

public interface CDIChecker {
	public void checkCDI(CDIValue value);

}
