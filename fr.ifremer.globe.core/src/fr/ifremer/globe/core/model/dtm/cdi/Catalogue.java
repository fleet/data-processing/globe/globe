package fr.ifremer.globe.core.model.dtm.cdi;
public enum Catalogue
{
	Gebco("GEBCO"),
	Sextant("Sextant"), 
	SeaDataNet("SeaDataNet"),
	Undefined("Unknown"), 
	interpolated("Interpolated");

	private final String name;
	Catalogue(String prettyName)
	{
		this.name=prettyName;
	}
	/**display catalogue name*/
	@Override
	public String toString()
	{
		return name;
	}
};
