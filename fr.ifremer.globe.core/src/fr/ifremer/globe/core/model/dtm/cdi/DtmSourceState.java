package fr.ifremer.globe.core.model.dtm.cdi;

/**
 * Indicates if a resources was found on the catalog
 */
public enum DtmSourceState {
	eExists, // data exists was retrieved
	eNotFound, // data was not found in database
	eURLParsingError, // source id was not properly parsed
	eConnectionError, // an error occurred while retrieving metadata
	eXMLParsingError, // xml metadata where not properly parsed
}
