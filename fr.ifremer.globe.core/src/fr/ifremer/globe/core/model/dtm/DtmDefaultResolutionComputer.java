package fr.ifremer.globe.core.model.dtm;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.utils.exception.GException;

public class DtmDefaultResolutionComputer implements IDtmResolutionComputer {

	private Projection projection;

	/**
	 * Constructor.
	 */
	public DtmDefaultResolutionComputer(Projection projection) {
		this.projection = projection;
	}

	@Override
	public void setProjection(Projection projection) {
		this.projection = projection;
	}

	@Override
	public double computeGridDefaultResolution() throws GException {
		if (ProjectionSettings.isLongLatProjection(projection)) {
			return 60 / 16.;
		}

		// disable step computation not working very well and time consuming
		return Double.NaN;
	}
}
