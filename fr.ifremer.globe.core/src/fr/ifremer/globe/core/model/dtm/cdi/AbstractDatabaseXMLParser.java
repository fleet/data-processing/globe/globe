package fr.ifremer.globe.core.model.dtm.cdi;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.ifremer.globe.utils.exception.FileFormatException;

public abstract class AbstractDatabaseXMLParser {
	DocumentBuilder loader;
	XPath path;
	protected static Logger logger = LoggerFactory.getLogger(AbstractDatabaseXMLParser.class);

	public AbstractDatabaseXMLParser() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);

		loader = factory.newDocumentBuilder();
		XPathFactory xpf = XPathFactory.newInstance();
		path = xpf.newXPath();
	}

	/**
	 * check if the given node exists
	 *
	 * @throws XPathExpressionException
	 */
	protected abstract boolean nodeExist(Element root) throws XPathExpressionException;

	/**
	 * parse xml document and retrieve metadata
	 */
	public DtmSourceMetadata parse(String edmoCode, URL validationURL) {

		// Handles several redirections...
		try {
			for (int i = 0; i < 3; i++) {
				HttpURLConnection conn = (HttpURLConnection) validationURL.openConnection();
				// WARNING : automatic redirection between HTTP & HTTPS doesn't work! Manual
				// redirection is required.
				conn.setInstanceFollowRedirects(true);
				conn.setConnectTimeout(5000); // not more than 5 seconds to connect
				var status = conn.getResponseCode();
				if (status == HttpURLConnection.HTTP_OK)
					break; // Valid connection : no redirection needed.
				else if (status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP
						|| status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
					var newUrl = conn.getHeaderField("Location");
					if (newUrl != null) {
						logger.warn("XML parsing : redirection from {} to {}", validationURL, newUrl);
						validationURL = new URL(newUrl);
					} else {
						logger.error("XML parsing : invalid redirection.");
					}
				}
			}
		} catch (IOException ex) {
			logger.warn("XML parsing : error : {} ", ex.getMessage());
		}

		// check if record exists in database
		try (InputStream input = validationURL.openStream()) {

			Document document = loader.parse(input);
			Element root = document.getDocumentElement();
			if (!nodeExist(root)) {
				return DtmSourceMetadata.defaultNotFound();
			}

			// retrieve metadata

			int horizontalAccuracy = QualityIndicator.HORIZONTALDEFAULT;
			int verticalAccuracy = QualityIndicator.VERTICALDEFAULT;
			int surveyType = QualityIndicator.SURVEYDEFAULT;
			int age = QualityIndicator.AGEDEFAULT;
			try {
				verticalAccuracy = parseQIVertical(root);
			} catch (FieldNotFoundException e) {
				logger.warn("{} for url {}", e.getMessage(), validationURL);
			}

			try {
				surveyType = parseQIPurpose(root);
			} catch (FieldNotFoundException e) {
				logger.warn("{} for url {}", e.getMessage(), validationURL);
			}
			try {

				horizontalAccuracy = parseQIHorizonal(root);
			} catch (FieldNotFoundException e) {
				logger.warn("{} for url {}", e.getMessage(), validationURL);
			}
			try {

				LocalDate date = findAgeValue(root);
				age = date.getYear();
			} catch (FieldNotFoundException e) {
				logger.warn("{} for url {}", e.getMessage(), validationURL);
			} catch (DateTimeParseException e) {
				logger.warn("Bad date field format in xml for url {}", validationURL);
			}

			QualityIndicator qc = new QualityIndicator(horizontalAccuracy, verticalAccuracy, surveyType, age);
			return new DtmSourceMetadata(NumberUtils.toInt(edmoCode, 0), qc, DtmSourceState.eExists);

		} catch (XPathExpressionException | SAXException | FileFormatException | NumberFormatException e) {
			logger.warn("Parsing Error on {} : {}", validationURL, e.getMessage());
			return DtmSourceMetadata.defaultXMLFormatError(NumberUtils.toInt(edmoCode, 0));
		} catch (IOException e1) {
			logger.warn("Network Error on {} : {}", validationURL, e1.getMessage());
			return DtmSourceMetadata.defaultNetworkError(NumberUtils.toInt(edmoCode, 0));
		}

	}

	protected int parseQIVertical(Element root)
			throws FileFormatException, XPathExpressionException, FieldNotFoundException {
		String VValue = findQIValue(ConstantPattern.QI_Vertical, root);
		return Integer.valueOf(VValue.trim());
	}

	protected int parseQIHorizonal(Element root)
			throws FileFormatException, XPathExpressionException, FieldNotFoundException {
		String PValue = findQIValue(ConstantPattern.QI_Horizontal, root);
		return Integer.valueOf(PValue.trim());
	}

	protected int parseQIPurpose(Element root)
			throws FileFormatException, XPathExpressionException, FieldNotFoundException {
		String PValue = findQIValue(ConstantPattern.QI_Purpose, root);
		return Integer.valueOf(PValue.trim());
	}

	protected LocalDate findAgeValue(Element root) throws XPathExpressionException, FieldNotFoundException {

		String expression = "//*[local-name()='SDN_DataIdentification']";
		NodeList nodelist = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
		if (nodelist.getLength() == 0) {
			throw new FieldNotFoundException("Missing xml field " + "SDN_DataIdentification");
		}
		Node node = nodelist.item(0);
		String refine = "*[local-name()='aggregationInfo']/*[local-name()='MD_AggregateInformation']/*[local-name()='aggregateDataSetName']/*[local-name()='CI_Citation']/*[local-name()='date']/*[local-name()='CI_Date']/*[local-name()='date']/*[local-name()='Date']";
		nodelist = (NodeList) path.evaluate(refine, node, XPathConstants.NODESET);
		if (nodelist.getLength() == 0) {
			throw new FieldNotFoundException("Cannot find date field");
		}
		node = nodelist.item(0);
		if (node.getChildNodes() == null || node.getChildNodes().getLength() == 0) {
			throw new FieldNotFoundException("Cannot find data field");

		}
		String value = node.getFirstChild().getNodeValue();
		LocalDate date = LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		return date;

	}

	private String findQIValue(String qiField, Element root)
			throws XPathExpressionException, FieldNotFoundException, FileFormatException {
		String expression = "//*[local-name()='CharacterString' and translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"
				+ qiField.toLowerCase() + "']";

		NodeList node = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);

		if (node.getLength() == 0) {
			throw new FieldNotFoundException("Missing xml field " + qiField);
		}
		// take the first node
		Node start = node.item(0);

		// can throw a null pointer exception if xml is does not match what's
		// expected
		try {
			Node parentConformanceResult = start.getParentNode().getParentNode().getParentNode().getParentNode();

			String expressionValue = "*[local-name()='explanation']/*[local-name()='CharacterString']/text()";
			NodeList nodeValueList = (NodeList) path.evaluate(expressionValue, parentConformanceResult,
					XPathConstants.NODESET);
			// int value=Integer.valueOf(nodeValue.getNodeValue());
			if (nodeValueList.getLength() == 0) {
				throw new FileFormatException("Missing xml field " + qiField);
			}
			Node nodeValue = nodeValueList.item(0);
			return nodeValue.getNodeValue();
		} catch (NullPointerException e) {
			throw new FileFormatException("XML format does not match the expected one");
		}
	}
}
