package fr.ifremer.globe.core.model.dtm.cdi;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SeaDataNetXMLParser extends AbstractDatabaseXMLParser {

	public SeaDataNetXMLParser() throws ParserConfigurationException {
		super();
	}
	
	/**
	 * check if the given node exists
	 * @throws XPathExpressionException 
	 * */
	@Override
	public boolean nodeExist(Element root) throws XPathExpressionException
	{
		String expression = "//*[local-name()='error']";
		NodeList node = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
		// record not found in database
		if (node.getLength() > 0) {
			return false;
		}
		return true;
	}
	
}
