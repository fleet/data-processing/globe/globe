package fr.ifremer.globe.core.model.dtm.cdi;
/**
 * Immutable QualityIndicator class
 * 
 * */
public class QualityIndicator {
	public final static int HORIZONTALDEFAULT=0;
	public final static int VERTICALDEFAULT=0;
	public final static int SURVEYDEFAULT=0;
	public final static int AGEDEFAULT=0;
	
	static QualityIndicator defaultValue=new QualityIndicator(HORIZONTALDEFAULT,VERTICALDEFAULT, SURVEYDEFAULT, AGEDEFAULT);
	
	public static QualityIndicator getDefault()
	{
		return defaultValue;
	}
	
	
	int horizontalAccuracy;
	int verticalAccuracy;
	int surveyType;
	int age;
	public int getHorizontalAccuracy() {
		return horizontalAccuracy;
	}
	public int getVerticalAccuracy() {
		return verticalAccuracy;
	}
	public int getSurveyType() {
		return surveyType;
	}
	public int getAge() {
		return age;
	}
	public QualityIndicator(int horizontalAccuracy, int verticalAccuracy, int surveyType, int age) {
		super();
		this.horizontalAccuracy = horizontalAccuracy;
		this.verticalAccuracy = verticalAccuracy;
		this.surveyType = surveyType;
		this.age = age;
	}
	
	
	public static void setDefaultValue(QualityIndicator defaultValue) {
		QualityIndicator.defaultValue = defaultValue;
	}

	@Override
	public String toString()
	{
		return "Date:"+Integer.toString(age)+" Horiz:"+Integer.toString(horizontalAccuracy)+" Vert:"+verticalAccuracy+" Type:"+surveyType;
	}
}
