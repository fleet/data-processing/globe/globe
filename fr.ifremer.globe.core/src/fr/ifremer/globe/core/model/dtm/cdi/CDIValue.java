package fr.ifremer.globe.core.model.dtm.cdi;

public class CDIValue {
	private String input;
	private String value;

	public CDIValue(String input, String modified) {
		this.input = input;
		value = modified;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String inputValue) {
		input = inputValue;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String modifiedValue) {
		value = modifiedValue;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return input != null ? input.hashCode() : super.hashCode();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object other) {
		return other == this || other != null && hashCode() == other.hashCode();
	}
}
