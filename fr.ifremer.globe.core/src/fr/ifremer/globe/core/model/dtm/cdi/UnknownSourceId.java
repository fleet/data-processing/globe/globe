package fr.ifremer.globe.core.model.dtm.cdi;

public class UnknownSourceId extends DtmSourceId {
	UnknownSourceId(String id) {
		super(id, Catalogue.Undefined);
	}

	@Override
	public DtmSourceMetadata retrieve() {
		return DtmSourceMetadata.defaultBadURL();

	}

	@Override
	public String toHTML() {
		StringBuilder sb = new StringBuilder("CDI message : " + sourceID);
		String link = "";
		link = "<br /><b>Source ID syntax error:" + sourceID + "</b>";
		sb.append(link);

		return sb.toString();
	}

}
