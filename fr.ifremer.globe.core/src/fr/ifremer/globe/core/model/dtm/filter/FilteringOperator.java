package fr.ifremer.globe.core.model.dtm.filter;

/**
 * Boolean filter operator.
 */
public enum FilteringOperator {
	AND,
	OR
}
