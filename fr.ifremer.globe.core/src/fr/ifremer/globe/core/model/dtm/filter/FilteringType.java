package fr.ifremer.globe.core.model.dtm.filter;

import java.util.Arrays;

public enum FilteringType {
	equal("equal", "equal", 1), not_equal("not equal", "not_equal", 1), lessThan("less than (x<=a)",
			"less_than",
			1), moreThan("more than (x>=a)", "more_than", 1), between("between (a<=x<=b)", "between", 2), missing("missing", "missing", 0), notMissing("not_missing", "not_missing", 0);// ,
	// notBetween("not between ((x<a or x>b)",
	// true);

	private String displayName;
	private String toStringName;
	private int argsNumber;

	private FilteringType(String name, String toStringName, int argsNumber) {
		this.displayName = name;
		this.toStringName = toStringName;
		this.argsNumber = argsNumber;
	}

	/**
	 * Return the enum named with name, return equal otherwise
	 * 
	 * @param name
	 * @return
	 */
	public static FilteringType resolveName(String name) {
		FilteringType[] values = FilteringType.values();
		for (int i = 0; i < values.length; i++) {
			if (values[i].displayName.equals(name))
				return values[i];
		}
		return equal;
	}

	/**
	 * return all valid displayed names for FilteringType
	 * 
	 * @return
	 */
	public static String[] getDisplayNames() {
		return Arrays.stream(FilteringType.values()).map(FilteringType::getDisplayName).toArray(String[]::new);
	}

	/**
	 * return all valid toString names for FilteringType
	 * 
	 * @return
	 */
	public static String[] getToStringNames() {
		return Arrays.stream(FilteringType.values()).map(FilteringType::getToStringName).toArray(String[]::new);
	}

	public int argsNumber() {
		return argsNumber;
	}

	/**
	 * @return the {@link #toStringName}
	 */
	public String getToStringName() {
		return toStringName;
	}

	/**
	 * @return the {@link #displayName}
	 */
	public String getDisplayName() {
		return displayName;
	}
}