package fr.ifremer.globe.core.model.dtm;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.utils.exception.GException;

public interface IDtmResolutionComputer {

	public abstract double computeGridDefaultResolution() throws GException;

	public abstract void setProjection(Projection projection);
}