package fr.ifremer.globe.core.model.dtm;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Dataset;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.exception.GIOException;

public class SounderDataElevationRasterComputer implements IDataContainerOwner {

	/**
	 * Parameters class
	 */
	public static /* Record */ class RasterComputerParameters {
		public final Projection projection;
		public final GeoBox geoBox;
		public final double spatialResolution;
		public final String outputPath;

		public RasterComputerParameters(Projection projection, GeoBox geoBox, double spatialResolution,
				String outputPath) {
			super();
			this.projection = projection;
			this.geoBox = geoBox;
			this.spatialResolution = spatialResolution;
			this.outputPath = outputPath;
		}
	}

	/** Useful factory class to create array **/
	private final IArrayFactory arrayFactory = IArrayFactory.grab();

	/**
	 * Compute raster dimension from mercator {@link GeoBox} & spatial resolution.
	 */
	public static Pair<Integer, Integer> computeDimensions(GeoBox mercatorGeoBox, double spatialResolution) {
		double verticalResolution = mercatorGeoBox.getHeight();
		double horizontalResolution = mercatorGeoBox.getWidth();

		// limit to 10 decimals to avoid precision problems
		BigDecimal vr = BigDecimal.valueOf(verticalResolution / spatialResolution);
		vr = vr.setScale(10, RoundingMode.FLOOR);
		BigDecimal hr = BigDecimal.valueOf(horizontalResolution / spatialResolution);
		hr = hr.setScale(10, RoundingMode.FLOOR);

		// we use ceil function to ensure that all data in the geobox will be retrieved in dtm
		var rowCount = (int) Math.ceil(vr.doubleValue());
		var columnCount = (int) Math.ceil(hr.doubleValue());
		return new Pair<>(rowCount, columnCount);
	}

	/**
	 * Generates elevation TiFF from {@link IProfile}.
	 */
	public void generateElevationTiFFFile(ISounderNcInfo sounderNcInfo, RasterComputerParameters parameters,
			IProgressMonitor monitor)
			throws ProjectionException, OperationCanceledException, GIOException, IOException {
		// get geobox in projection
		var geoBox = parameters.geoBox.project(parameters.projection);
		// prepare raster parameters
		var dimensions = computeDimensions(geoBox, parameters.spatialResolution);
		var rowCount = dimensions.getFirst();
		var columnCount = dimensions.getSecond();
		Dataset rasterDataset = null;

		try (var token = IDataContainerFactory.grab().book(sounderNcInfo, this)) {
			// prepare layers
			SounderDataContainer dataContainer = token.getDataContainer();
			int swathCount = dataContainer.getInfo().getCycleCount();
			int beamCount = dataContainer.getInfo().getTotalRxBeamCount();
			int detectionCount = swathCount * beamCount;
			SubMonitor subMonitor = SubMonitor.convert(monitor,
					"Generating elevation raster from " + sounderNcInfo.getFilename(), detectionCount * 3);

			CompositeCSLayers cSLayers = dataContainer.getCsLayers(CoordinateSystem.FCS);
			var latitudeLayer = cSLayers.getProjectedX();
			var longitudeLayer = cSLayers.getProjectedY();
			var depthLayer = cSLayers.getProjectedZ();
			var statusLayer = dataContainer.getStatusLayer();

			// get detection position in current projection
			var detectionX = arrayFactory.makeDoubleArray(swathCount, beamCount);
			var detectionY = arrayFactory.makeDoubleArray(swathCount, beamCount);
			for (int swathIdx = 0; swathIdx < swathCount; swathIdx++) {
				for (int beamIdx = 0; beamIdx < beamCount; beamIdx++) {
					if (statusLayer.isValid(swathIdx, beamIdx)) {
						detectionX.putDouble(swathIdx, beamIdx, longitudeLayer.get(swathIdx, beamIdx));
						detectionY.putDouble(swathIdx, beamIdx, latitudeLayer.get(swathIdx, beamIdx));
					}
					subMonitor.worked(1);
				}
			}
			parameters.projection.project(detectionX::getDouble, detectionY::getDouble, detectionX::putDouble,
					detectionY::putDouble, detectionCount, subMonitor.split(1));

			// compute raster values
			var cellDepthArray = arrayFactory.makeFloatArray(rowCount, columnCount);
			// fill with NaN
			for (var r = 0; r < rowCount; r++)
				for (var c = 0; c < columnCount; c++)
					cellDepthArray.putFloat(r, c, Float.NaN);
			var cellDectectionCountArray = arrayFactory.makeIntArray(rowCount, columnCount);

			for (int swathIdx = 0; swathIdx < swathCount; swathIdx++) {
				for (int beamIdx = 0; beamIdx < beamCount; beamIdx++) {
					// keep only valid detection
					if (statusLayer.isValid(swathIdx, beamIdx)) {
						// get detection row & column
						var detectionRow = (int) Math
								.floor((detectionY.getDouble(swathIdx, beamIdx) - geoBox.getBottom())
										/ parameters.spatialResolution);
						var detectionColumn = (int) Math
								.floor((detectionX.getDouble(swathIdx, beamIdx) - geoBox.getLeft())
										/ parameters.spatialResolution);
						var detectionZ = -depthLayer.get(swathIdx, beamIdx);

						// get suitable cell and compute mean with the new detection
						if (0 > detectionRow || detectionRow >= rowCount //
								|| 0 > detectionColumn || detectionColumn >= columnCount)
							continue;

						var previousValue = cellDepthArray.getFloat(detectionRow, detectionColumn);
						if (Float.isNaN(previousValue))
							previousValue = 0;
						var previousDetectionCount = cellDectectionCountArray.getInt(detectionRow, detectionColumn);
						var newDetectionCount = previousDetectionCount + 1;
						cellDectectionCountArray.putInt(detectionRow, detectionColumn, newDetectionCount);
						cellDepthArray.putFloat(detectionRow, detectionColumn,
								(float) (((previousValue * previousDetectionCount) + detectionZ) / newDetectionCount));
					}
					subMonitor.worked(1);
				}
			}

			// generates the result TiFF file
			rasterDataset = GdalUtils.generateTiffFile(//
					rowCount, columnCount, //
					Float.NaN, //
					parameters.projection.getSpatialReference().ExportToWkt(), //
					geoBox.getTop(), geoBox.getBottom(), geoBox.getLeft(), geoBox.getRight(), //
					(row, col) -> cellDepthArray.getFloat((rowCount - 1) - row, col), // inverse row to match Gdal
					parameters.outputPath, //
					GdalUtils.newProgressCallback(subMonitor.split(detectionCount)));
		} finally {
			if (rasterDataset != null)
				rasterDataset.delete();
		}
	}

}
