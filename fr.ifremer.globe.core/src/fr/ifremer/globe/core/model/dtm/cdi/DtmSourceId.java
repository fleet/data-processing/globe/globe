package fr.ifremer.globe.core.model.dtm.cdi;

import java.net.URL;
/**
 * Class handling metadata data for CDI and CPRD ids
 * */
public abstract class DtmSourceId
{
	protected DtmSourceId(String id, Catalogue type) {
		this.sourceID=id;
		this.catalogue=type;
		this.localCDIID= id.substring(id.indexOf('_') + 1);

	}
	@Override
	public String toString()
	{
		return catalogue+": edmo code "+edmoCODE+" local id "+localCDIID;
	}
	// URL used by globe to interactively display information on the give ressource
	URL displayedURL;
	
	
	Catalogue catalogue;
	String edmoCODE = "";
	String localCDIID ="";
	String sourceID = "";
	/**
	 * retrieve metadata information from the web
	 * <p> this can be a long operation and should be started in a separate thread
	 * */
	public abstract DtmSourceMetadata retrieve() ;

	
	/**
	 * retrieve metadata information
	 * */
	public QualityIndicator getMetadata()
	{
		return QualityIndicator.defaultValue;
	}
	
	/**
	 * build html representation of metadata
	 * */
	public abstract String toHTML();

	
	
}