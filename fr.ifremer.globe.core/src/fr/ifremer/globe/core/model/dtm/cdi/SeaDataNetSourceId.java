package fr.ifremer.globe.core.model.dtm.cdi;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeaDataNetSourceId extends DtmSourceId {
	protected static Logger logger = LoggerFactory.getLogger(SeaDataNetSourceId.class);

	SeaDataNetSourceId(String id) throws MalformedURLException {
		super(id, Catalogue.SeaDataNet);
		String PREFIX = "";

		String urlPattern = "";
		String edmoCodePattern = "";
		PREFIX = ConstantPattern.SDN + ConstantPattern.CDI;
		if (id.contains(ConstantPattern.LOCAL)) { // PREFIX LOCAL
			PREFIX += ConstantPattern.LOCAL;
		}

		urlPattern = ConstantPattern.KEY_CDITOOL_CDI_URL; // url for CDI
		edmoCodePattern = id.substring(PREFIX.length());
		if (edmoCodePattern.contains("_")) {
			edmoCODE = id.substring(PREFIX.length(), id.indexOf('_'));
		} else {
			edmoCODE = id;
		}
		String urlTmp = urlPattern.replace(ConstantPattern.KEY_CDITOOL_EDMOCODE, edmoCODE);
		displayedURL = new URL(urlTmp.replace(ConstantPattern.KEY_CDITOOL_LOCALID, localCDIID));

	}

	@Override
	public DtmSourceMetadata retrieve() {
		try {
			String urlPattern = ConstantPattern.KEY_CDITOOL_VALIDATECDI_URL; // url for

			String urlTmp = urlPattern.replace(ConstantPattern.KEY_CDITOOL_EDMOCODE, edmoCODE);
			URL validateURL = new URL(urlTmp.replace(ConstantPattern.KEY_CDITOOL_LOCALID, localCDIID));

			SeaDataNetXMLParser parser = new SeaDataNetXMLParser();
			return parser.parse(edmoCODE, validateURL);
		} catch (MalformedURLException | ParserConfigurationException e) {
			logger.error("Error while validating " + this, e);
			return DtmSourceMetadata.defaultBadURL();
		}
	}

	@Override
	public String toHTML() {
		StringBuilder sb = new StringBuilder("CDI message : " + sourceID);
		String link = "";
		link = "<br />Link :<a href=\"" + displayedURL + "\"> " + catalogue + "</a>";
		sb.append("<br />EDMO-CODE = " + edmoCODE);
		sb.append("<br />LOCAL-CDI-ID = " + localCDIID);
		sb.append(link);

		return sb.toString();
	}

}
