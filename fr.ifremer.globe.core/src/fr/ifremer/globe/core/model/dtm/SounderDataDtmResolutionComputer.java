package fr.ifremer.globe.core.model.dtm;

import java.util.List;
import java.util.Optional;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.VendorSpecificLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Compute a default spatial resolution with mean inter-pings distance. This computer only takes valid pings into
 * account.
 * <p>
 * <strong>Note : The underlying projection is always assumed to be LAT/LONG.</strong>
 */
public class SounderDataDtmResolutionComputer implements IDtmResolutionComputer {

	private List<ISounderNcInfo> sounderNcInfos;
	private Projection projection;

	/**
	 * Constructs a new resolution computer.
	 * 
	 * @param sounderNcInfos list of sounder file infos.
	 */
	public SounderDataDtmResolutionComputer(List<ISounderNcInfo> sounderNcInfos, Projection projection) {
		this.sounderNcInfos = sounderNcInfos;
		this.projection = projection;
	}

	@Override
	public void setProjection(Projection projection) {
		this.projection = projection;
	}

	/**
	 * Compute the spatial resolution for all sounder information given at construct time.
	 * 
	 * @return spatial resolution in meters.
	 */
	@Override
	public double computeGridDefaultResolution() throws GException {
		DescriptiveStatistics desc = new DescriptiveStatistics(sounderNcInfos.size());
		for (ISounderNcInfo info : sounderNcInfos) {
			desc.addValue(compute(info));
		}
		return desc.getMean();
	}

	/**
	 * Compute the mean inter-pings distance in meters and returns.
	 * 
	 * @param info One sounder information.
	 * @return Spatial resolution in meters.
	 */
	private double compute(ISounderNcInfo info) throws ProjectionException, GIOException {

		IDataContainerOwner owner = IDataContainerOwner.generate("fr.ifremer.globe.core.model.dtm.datacontainerowner", true);
		double resolution = 0.0;
		try (ISounderDataContainerToken token = IDataContainerFactory.grab().book(info, owner)) {
			SounderDataContainer container = token.getDataContainer();
			int nbSample = 100;
			SounderData data = new SounderData(container);

			int increment = data.swathCount / 100;
			// check if less than 100 ping
			if (increment == 0) {
				increment = 1;
			}
			// now calculate distances between all couples of consecutive pings
			DescriptiveStatistics statistics = new DescriptiveStatistics(nbSample);

			for (int i = 0; i < data.swathCount; i += increment) {
				Optional<Pair<Integer, Double>> res = distanceBetweenNextPings(data, i);
				if (res.isPresent()) {
					// add computed distance to statistics
					statistics.addValue(res.get().getSecond());
					// increment loop counter to the found ping index
					i = res.get().getFirst();
				}
			}

			// the resolution will be the mean distance between each ping
			resolution = statistics.getPercentile(50);
			if (resolution > 1.0) {
				resolution = Math.round(resolution);
			} else {
				resolution = Math.round(resolution * 100) / 100.;
			}
			// for swath editor, multiply res by 4 in order to have no holes and a meaning full stdev
			resolution *= 4;

			if (Double.isNaN(resolution)) {
				// we use the old CARAIBES COMPUTATION
				projection.setProjectedGeoBox(container.getInfo().getGeoBox());
				double horizontalResolution = projection.getMaxProjectedX() - projection.getMinProjectedX();
				double verticalResolution = projection.getMaxProjectedY() - projection.getMinProjectedY();
				double sr = Math.max(verticalResolution, horizontalResolution);
				// formula from CARAIBES software
				sr *= 3.0 / 2000.0;
				// round precision
				sr = Math.round(sr);
				// threshold
				if (sr < 4) {
					sr = 4;
				}
				resolution = sr;
			}
		}
		if (resolution == 0.0) {
			resolution = 0.01;
		}
		return resolution;
	}

	/**
	 * utility class to prevent access to SounderDataContainer.getLayer
	 */
	private class SounderData {
		private DetectionStatusLayer swathValidityLayer;
		private DoubleLoadableLayer1D latLayer;
		private DoubleLoadableLayer1D longLayer;
		private Optional<FloatLoadableLayer2D> frequencyLayer;
		private Optional<LongLoadableLayer1D> pingLayer;
		private int swathCount;
		private int mediumBeam;

		SounderData(SounderDataContainer container) throws GIOException {
			swathValidityLayer = container.getStatusLayer();
			latLayer = container.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
			longLayer = container.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);

			pingLayer = Optional.empty();
			frequencyLayer = Optional.empty();
			if (container.hasLayer(VendorSpecificLayers.PING_RAW_COUNT)) {
				pingLayer = Optional.of(container.getLayer(VendorSpecificLayers.PING_RAW_COUNT));
			} else if (container.hasLayer(BeamGroup1Layers.TRANSMIT_FREQUENCY_START)) {
				frequencyLayer = Optional.of(container.getLayer(BeamGroup1Layers.TRANSMIT_FREQUENCY_START));
			}

			swathCount = container.getInfo().getCycleCount();
			mediumBeam = container.getInfo().getTotalRxBeamCount() / 2;
		}

	}

	private class Vect2D {
		double x;

		private Vect2D(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}

		double y;

		double dist(Vect2D b) {
			return Math.sqrt((x - b.x) * (x - b.x) + (y - b.y) * (y - b.y));
		}
	}

	/**
	 * find the next two consecutive ping (not swath, real ping) starting from the given index
	 * 
	 * @return A pair with the first ping and the distance between those ping
	 * @throws GIOException
	 */
	private Optional<Pair<Integer, Double>> distanceBetweenNextPings(SounderData data, int start) throws GIOException {
		try {
			Optional<Integer> ret = findNextValidSwath(data, start);
			if (ret.isPresent()) {
				int swathIndex = ret.get();
				Optional<Integer> pair = findPairedSwath(data, swathIndex);
				if (pair.isPresent() && isValidSwath(data, pair.get())) {
					double[] coords = projection.project(data.longLayer.get(swathIndex), data.latLayer.get(swathIndex));
					Vect2D a = new Vect2D(coords[0], coords[1]);
					coords = projection.project(data.longLayer.get(pair.get()), data.latLayer.get(pair.get()));
					Vect2D b = new Vect2D(coords[0], coords[1]);
					double dist = a.dist(b);
					if (dist != Double.NaN && dist != Double.POSITIVE_INFINITY)
						return Optional.of(new Pair<>(swathIndex, dist));
				}
			}
			return Optional.empty();
		} catch (ProjectionException e) {
			throw new GIOException(e.getMessage());
		}
	}

	/**
	 * find the next associated ping starting from this index,
	 * <p>
	 * by associated is meant a swath having a different ping number and the same frequency plane, we do not check if
	 * the swath is valid
	 * 
	 * @return the index of the supposedly next ping
	 * @throws GIOException
	 */
	private Optional<Integer> findPairedSwath(SounderData data, int sourceSwathIdx) throws GIOException {
		if (data.pingLayer.isPresent()) {
			// get ping number
			LongLoadableLayer1D pingLayer = data.pingLayer.get();
			long sourcePing = pingLayer.get(sourceSwathIdx);
			// find next swath with a different ping count
			for (int swathIndex = sourceSwathIdx + 1; swathIndex < data.swathCount; swathIndex++) {
				long ping = pingLayer.get(swathIndex);
				if (ping != sourcePing) {
					return Optional.of(swathIndex);
				}
			}
		} else if (data.frequencyLayer.isPresent()) {
			// get ping number
			FloatLoadableLayer2D frequencyLayer = data.frequencyLayer.get();
			float sourceFreq = frequencyLayer.get(sourceSwathIdx, 0);
			// find next swath in the same frequency plane
			for (int swathIndex = sourceSwathIdx + 1; swathIndex < data.swathCount; swathIndex++) {
				float freq = frequencyLayer.get(swathIndex, 0);
				if (freq == sourceFreq) {
					return Optional.of(swathIndex);
				}
			}
		}
		return Optional.empty();
	}

	private Optional<Integer> findNextValidSwath(SounderData data, int start) throws GIOException {
		for (int swathIndex = start; swathIndex < data.swathCount; swathIndex++) {
			if (isValidSwath(data, swathIndex)) {
				return Optional.of(swathIndex);
			}
		}
		return Optional.empty();
	}

	private boolean isValidSwath(SounderData data, int swathIndex) throws GIOException {
		return data.swathValidityLayer.hasOnlyBeamFlag(swathIndex, data.mediumBeam);
	}

}
