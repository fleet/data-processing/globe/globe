package fr.ifremer.globe.core.model.dtm.cdi;

import java.net.MalformedURLException;
import java.net.URL;

public class GebcoSourceId extends DtmSourceId {
	GebcoSourceId(String id) throws MalformedURLException {
		super(id, Catalogue.Gebco);
		displayedURL = new URL(ConstantPattern.URL_GEBCO);
	}

	@Override
	public DtmSourceMetadata retrieve() {
		return DtmSourceMetadata.defaultExisting();
	}

	@Override
	public String toHTML() {
		StringBuilder sb = new StringBuilder("CDI message : " + sourceID);
		String link = "";
		link = "<br />Link :<a href=\"" + displayedURL + "\"> " + catalogue + "</a>";
		sb.append("<br />EDMO-CODE = " + edmoCODE);
		sb.append("<br />LOCAL-CDI-ID = " + localCDIID);
		sb.append(link);

		return sb.toString();
	}

}
