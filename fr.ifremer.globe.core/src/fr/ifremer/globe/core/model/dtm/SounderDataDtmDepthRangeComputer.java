package fr.ifremer.globe.core.model.dtm;

import java.util.List;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Compute the depth range from sounder detection data. Computed depth range depends on given detection status mask.
 */
public class SounderDataDtmDepthRangeComputer {

	private List<ISounderNcInfo> sounderNcInfos;

	/**
	 * Constructs a new geobox computer.
	 * 
	 * @param sounderNcInfos list of sounder file infos.
	 */
	public SounderDataDtmDepthRangeComputer(List<ISounderNcInfo> sounderNcInfos) {
		this.sounderNcInfos = sounderNcInfos;
	}

	/**
	 * Computes the depth range for all sounder information given at construct time.
	 * 
	 * @return computed depth range for all valid detections.
	 */
	public DoubleRange computeValidDepthRange() throws GIOException {
		return computeDepthRangeWithMask((byte) 0xFF);
	}

	/**
	 * Computes the depth range for all sounder information given at construct time according the given mask.
	 * 
	 * @param detectionStatusMask : SonarDetectionStatus mask used to determine the detection validity
	 * @return computed depth range for detections without any flag set in status mask.
	 * @throws GIOException
	 */
	public DoubleRange computeDepthRangeWithMask(byte detectionStatusMask) throws GIOException {

		double minDepth = Double.POSITIVE_INFINITY;
		double maxDepth = Double.NEGATIVE_INFINITY;
		IDataContainerOwner owner = IDataContainerOwner
				.generate("fr.ifremer.globe.core.model.dtm.depthrangecomputer.datacontainerowner", true);
		for (ISounderNcInfo info : sounderNcInfos) {
			try (ISounderDataContainerToken token = IDataContainerFactory.grab().book(info, owner)) {
				SounderDataContainer dataContainer = token.getDataContainer();
				// Get layers
				DoubleLayer2D depthLayer = dataContainer.getCsLayers(CoordinateSystem.FCS).getProjectedZ();
				DetectionStatusLayer statusLayer = dataContainer.getStatusLayer();

				// Computes statistics
				final int swathSize = dataContainer.getInfo().getCycleCount();
				final int beamSize = dataContainer.getInfo().getTotalRxBeamCount();
				for (int swathIdx = 0; swathIdx < swathSize; swathIdx++) {
					for (int beamIdx = 0; beamIdx < beamSize; beamIdx++) {
						if (statusLayer.isDetectionValidWithMask(swathIdx, beamIdx, detectionStatusMask)) {
							double currentDepth = depthLayer.get(swathIdx, beamIdx);
							minDepth = Math.min(minDepth, currentDepth);
							maxDepth = Math.max(maxDepth, currentDepth);
						}
					}
				}
			}
		}
		return new DoubleRange(minDepth, maxDepth);
	}
}
