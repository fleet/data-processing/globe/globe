package fr.ifremer.globe.core.model.dtm.cdi;

public class InterpolatedMetadata extends DtmSourceId {
	InterpolatedMetadata(String id) {
		super(id, Catalogue.interpolated);
	}

	@Override
	public DtmSourceMetadata retrieve() {
		return DtmSourceMetadata.defaultExisting();
	}

	@Override
	public String toHTML() {
		StringBuilder sb = new StringBuilder("CDI message : " + sourceID);
		String link = "";
		link = "<br /><b>interpolated from source data in neighbouring cells</b>";

		sb.append(link);

		return sb.toString();
	}

}
