package fr.ifremer.globe.core.model.dtm;

import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Compute a Geobox from sounder detection data. Computed geobox depends on given detection status mask.
 */
public class SounderDataDtmGeoboxComputer {

	private List<ISounderNcInfo> sounderNcInfos;

	/**
	 * Constructs a new geobox computer.
	 * 
	 * @param sounderNcInfos list of sounder file infos.
	 */
	public SounderDataDtmGeoboxComputer(List<ISounderNcInfo> sounderNcInfos) {
		this.sounderNcInfos = sounderNcInfos;
	}

	/**
	 * Computes the geobox for all sounder information given at construct time.
	 * 
	 * @return computed geobox for all valid detections.
	 */
	public GeoBox computeValidGeobox() throws GIOException {
		return computeGeoboxWithMask((byte) 0xFF);
	}

	/**
	 * Computes the geobox for all sounder information given at construct time according the given mask.
	 * 
	 * @param detectionStatusMask : SonarDetectionStatus mask used to determine the detection validity
	 * @return computed geobox for detections without any flag set in status mask.
	 * @throws GIOException
	 */
	public GeoBox computeGeoboxWithMask(byte detectionStatusMask) throws GIOException {
		GeoBoxBuilder geoBuilder = new GeoBoxBuilder();
		IDataContainerOwner owner = IDataContainerOwner
				.generate("fr.ifremer.globe.core.model.dtm.geoboxcomputer.datacontainerowner", true);
		for (ISounderNcInfo info : sounderNcInfos) {
			try (ISounderDataContainerToken token = IDataContainerFactory.grab().book(info, owner)) {
				SounderDataContainer dataContainer = token.getDataContainer();
				DoubleLoadableLayer2D lat = dataContainer.getLayer(BathymetryLayers.DETECTION_LATITUDE);
				DoubleLoadableLayer2D lon = dataContainer.getLayer(BathymetryLayers.DETECTION_LONGITUDE);
				DetectionStatusLayer statusLayer = dataContainer.getStatusLayer();
				final int swathSize = (int) lat.getDimensions()[0];
				final int beamSize = (int) lat.getDimensions()[1];
				// compute geobox on with valid swath
				for (int i = 0; i < swathSize; i++) {
					for (int j = 0; j < beamSize; j++) {
						if (statusLayer.isDetectionValidWithMask(i, j, detectionStatusMask)) {
							geoBuilder.addPoint(lon.get(i, j), lat.get(i, j));
						}
					}
				}
			}
		}
		return geoBuilder.build();
	}
}
