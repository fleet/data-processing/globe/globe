package fr.ifremer.globe.core.model.dtm.cdi;

public class ConstantPattern {
	static final String KEY_CDITOOL_EDMOCODE ="#EDMOCODE#";
	static final String KEY_CDITOOL_LOCALID ="#LOCALID#";
	//old URL
	//static final String KEY_CDITOOL_CPRD_URL="http://sextant.ifremer.fr/geonetwork/srv/eng/md.format.html?xsl=mdviewer&style=emodnet&uuid=SDN_CPRD_#EDMOCODE#_#LOCALID#";

	//new URL 2018
	static final String KEY_CDITOOL_CPRD_URL= "https://sextant.ifremer.fr/geonetwork/srv/eng/display#/SDN%5FCPRD%5F#EDMOCODE#%5F#LOCALID#/formatters/xsl-view?css=emodnet-bathymetry&view=emodnetHydrography";
	//static final String KEY_CDITOOL_CPRD_URL="http://www.emodnet-bathymetry.eu/metadata-amp-data/sextant-catalogue-service#/metadata/SDN_CPRD_#EDMOCODE#_#LOCALID#";
	//static final String KEY_CDITOOL_CDI_URL ="http://www.emodnet-bathymetry.eu/v_cdi_v3/print_wfs.asp?edmo=#EDMOCODE#&identifier=#LOCALID#";
	//static final String KEY_CDITOOL_CDI_URL ="https://www.emodnet-bathymetry.eu/report/edmo/#EDMOCODE#/#LOCALID#"; // URL 2019
	static final String KEY_CDITOOL_CDI_URL ="https://cdi-bathymetry.seadatanet.org/report/edmo/#EDMOCODE#/#LOCALID#"; // URL 2023
	
	/**
	 * Validation URL, retrieve metadata
	 * */
	static final String KEY_CDITOOL_VALIDATECPRD_URL ="https://sextant.ifremer.fr/geonetwork/srv/fre/csw?request=GetRecordById&elementSetName=full&service=CSW&version=2.0.2&id=SDN_CPRD_#EDMOCODE#_#LOCALID#&OutputSchema=http://www.isotc211.org/2005/gmd";
	//static String 	KEY_CDITOOL_VALIDATECDI_URL="http://www.emodnet-bathymetry.eu/v_cdi_v3/print.asp?popup=yes&edmo=#EDMOCODE#&identifier=#LOCALID#";

	// URL to retrieve xml metadata
	//static final String KEY_CDITOOL_VALIDATECDI_URL="http://seadatanet.maris2.nl/v_cdi_v3/print_xml.asp?edmo=#EDMOCODE#&identifier=#LOCALID#";
	static final String KEY_CDITOOL_VALIDATECDI_URL ="https://cdi.seadatanet.org/report/edmo/#EDMOCODE#/#LOCALID#/xml"; // URL 2019

	/** PREFIX GEBCO */
	static final String GEBCO = "GEBCO";

	/** URL GEBCO */
	static final String URL_GEBCO = "http://www.gebco.net/data_and_products/gebco_digital_atlas/";

	/** PREFIX SDN */
	static final String SDN = "SDN:";

	/** PREFIX LOCAL ID */
	static final String LOCAL = "LOCAL:";
	//
	/** Catalogue CPRD */
	static final String CPRD = "CPRD:";

	/** Catalogue CDI */
	static final String CDI = "CDI:";
	
	/** QI_Fields*/
	static final String QI_Vertical="QI_Vertical";
	static final String QI_Horizontal="QI_Horizontal";
	static final String QI_Purpose="QI_Purpose";
	
	/** Sextant QI definition**/
	static final String Sextant_QI_Vertical="emodnet-bathymetry.QI.Vertical";
	static final String Sextant_QI_Horizontal="emodnet-bathymetry.QI.Horizontal";
	static final String Sextant_QI_Purpose="emodnet-bathymetry.QI.Purpose";

	static final String QI_Age="";

}
