package fr.ifremer.globe.core.model.dtm.cdi;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.ifremer.globe.utils.exception.FileFormatException;

public class SextantNetXMLParser extends AbstractDatabaseXMLParser {

	public SextantNetXMLParser() throws ParserConfigurationException {
		super();
	}

	/**
	 * check if the given node exists
	 * @throws XPathExpressionException 
	 * */
	@Override
	public boolean nodeExist(Element root) throws XPathExpressionException
	{
		// for sextant node exist if a node MD_Metadata is set
		String expression = "//*[local-name()='MD_Metadata']";
		NodeList node = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
		// record not found in database
		if (node.getLength() > 0) {
			return true;
		}
		return false;
	}

	protected int parseQIVertical(Element root) throws FileFormatException, XPathExpressionException, FieldNotFoundException
	{
		String VValue = findQIValue(ConstantPattern.Sextant_QI_Vertical, root);
		return Integer.valueOf(VValue);
	}
	protected int parseQIHorizonal(Element root) throws FileFormatException, XPathExpressionException, FieldNotFoundException
	{
		String PValue = findQIValue(ConstantPattern.Sextant_QI_Horizontal, root);
		return Integer.valueOf(PValue);
	}
	protected int parseQIPurpose(Element root) throws FileFormatException, XPathExpressionException, FieldNotFoundException
	{
		String PValue = findQIValue(ConstantPattern.Sextant_QI_Purpose, root);
		return Integer.valueOf(PValue);
	}

	protected String findQIValue(String qiField, Element root)
			throws XPathExpressionException, FieldNotFoundException, FileFormatException {
		String expression = "//*[local-name()='CharacterString' and translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='" + qiField.toLowerCase() + "']";

		NodeList node = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);

		if (node.getLength() == 0) {
			throw new FieldNotFoundException("Missing xml field " + qiField);
		}
		// take the first node
		Node start = node.item(0);

		// can throw a null pointer exception if xml is does not match what's
		// expected
		try {
			Node parentConformanceResult = start.getParentNode().getParentNode();

			String expressionValue = "*[local-name()='result']/*[local-name()='DQ_QuantitativeResult']/*[local-name()='value']/*[local-name()='Record']/text()";
			NodeList nodeValueList = (NodeList) path.evaluate(expressionValue, parentConformanceResult,
					XPathConstants.NODESET);
			// int value=Integer.valueOf(nodeValue.getNodeValue());
			if (nodeValueList.getLength() == 0) {
				throw new FileFormatException("Missing xml field " + qiField);
			}
			Node nodeValue = nodeValueList.item(0);
			String value=nodeValue.getNodeValue(); 
			String arrvalue[]=value.trim().split(" ");
			if(arrvalue.length==0)
			{
				throw new FileFormatException(String.format("Missing xml field %s, unable to cast %s to integer" ,qiField,value));
			}
			return arrvalue[0];
		} catch (NullPointerException e) {
			throw new FileFormatException("XML format does not match the expected one");
		}
	}
	@Override
	protected LocalDate findAgeValue(Element root) 
			throws XPathExpressionException, FieldNotFoundException
	{

		String expression = "//*[local-name()='EX_TemporalExtent']";
		NodeList nodelist = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
		if (nodelist.getLength() == 0) {
			throw new FieldNotFoundException("Missing xml field " + "EX_TemporalExtent");
		}	
		Node node=nodelist.item(0);
		String refine="*[local-name()='extent']/*[local-name()='TimePeriod']/*[local-name()='beginPosition']";
		nodelist = (NodeList) path.evaluate(refine, node, XPathConstants.NODESET);
		if (nodelist.getLength() == 0) {
			throw new FieldNotFoundException("Cannot find date field");
		}
		node=nodelist.item(0);
		if(node.getChildNodes()==null || node.getChildNodes().getLength()==0)
		{
			throw new FieldNotFoundException("Cannot find date field");

		}
		String value=node.getFirstChild().getNodeValue();
		try {
			LocalDate date=LocalDateTime.parse(value).toLocalDate();
			return date;
		}
		catch (DateTimeParseException  e) {
			LocalDate date=LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			return date;
		} 
	}

}
