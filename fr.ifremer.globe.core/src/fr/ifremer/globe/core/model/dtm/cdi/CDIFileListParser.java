package fr.ifremer.globe.core.model.dtm.cdi;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;

public class CDIFileListParser {
	private static final Logger logger = LoggerFactory.getLogger(CDIFileListParser.class);

	public List<CDIValue> filter(List<CDIValue> input,List<? extends IFileInfo> infos)
	{
		ArrayList<CDIValue> arr=new ArrayList<CDIValue>();
		for(CDIValue v:input)
		{
			for (IFileInfo i : infos) {
				File file=new File(i.getPath());
				if (file.getName().equals(v.getInput())) {
					arr.add(v);
				}
			}
		}
		return arr;
	}

	/**
	 * 
	 * read CDI text file which lines have this syntax: file.mbg CDI_String
	 * 
	 * fill cdiFiles and cdiValues parameters
	 * 
	 * filter with input mbg files contained in infos
	 * 
	 * @param cdiFilesToLoad
	 * @param cdiFiles
	 * @param cdiValues
	 * @param infos
	 */
	public List<CDIValue> readFile(String cdiFilesToLoad) {
		ArrayList<CDIValue> arr=new ArrayList<CDIValue>();
		try {
			String[] pathes = cdiFilesToLoad.split("[;]");
			if (pathes != null) {
				for (String path : pathes) {
					List<String> lines = FileUtils.readLines(new File(path),Charset.defaultCharset());
					for (String line : lines) {
						line=line.trim(); // remove leading and trailing whitespace 

						//remove comments 
						String[] parsesLines=line.split("[#]|[!]|[;]");
						if(parsesLines.length>0)
						{
							line=parsesLines[0];
							line=line.replace("\"",""); //remove " characters
							// there can be many whitespaces or tab characters...
							String[] tokens = line.split("[ \t][ \t]*");
							if (tokens.length > 1) {
								String file = tokens[0];

								String cdi = tokens[1];

								arr.add(new CDIValue(file, cdi));


							}
						}

					}
				}
			}
		} catch (IOException e) {
			logger.warn("error reading cdi file", e);
		}
		return arr;
	}

	/**
	 * 
	 * read CDI text file which lines have this syntax: file.mbg CDI_String
	 * 
	 * fill cdiFiles and cdiValues parameters
	 * 
	 * filter with input mbg files contained in infos
	 * 
	 * @param cdiFilesToLoad
	 * @param cdiFiles
	 * @param cdiValues
	 * @param infos
	 */
	public void readCDIFiles(String cdiFilesToLoad, List<String> cdiFiles, List<String> cdiValues,
			List<? extends IFileInfo> infos) {
		cdiFiles.clear();
		cdiValues.clear();

		List<CDIValue> l=readFile(cdiFilesToLoad);
		l=filter(l, infos);
		for(CDIValue v:l)
		{
			cdiFiles.add(v.getInput());
			cdiValues.add(v.getValue());
		}
	}
}
