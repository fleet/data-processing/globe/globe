package fr.ifremer.globe.core.model.dtm.cdi;

import fr.ifremer.globe.utils.exception.GException;

@SuppressWarnings("serial")
public class FieldNotFoundException extends GException {

	public FieldNotFoundException(String string) {
		super(string);
	}

}
