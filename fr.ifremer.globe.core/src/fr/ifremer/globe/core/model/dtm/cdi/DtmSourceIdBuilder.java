package fr.ifremer.globe.core.model.dtm.cdi;

import java.net.MalformedURLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DtmSourceIdBuilder {
	
	protected static Logger logger = LoggerFactory.getLogger(DtmSourceIdBuilder.class);

	
	/**
	 * Build a new Metadata given a ID string.
	 * 
	 * @param cdi
	 * @return the message
	 */
	public static DtmSourceId build(String cdi) {
		// careful, invisible char with "Alt + 255" to avoid return

		DtmSourceId ret=null;

		try {

			// if cdi contains PREFIX
			if (cdi.contains(ConstantPattern.GEBCO)) { // GEBCO
				ret=new GebcoSourceId(cdi);

			} else if (cdi.contains(ConstantPattern.SDN)) {
				if (cdi.contains(ConstantPattern.CPRD)) { // PREFIX CPRD
					ret = new SextantSourceId(cdi);
				} else if (cdi.contains(ConstantPattern.CDI)) { // PREFIX CDI
					ret = new SeaDataNetSourceId(cdi);
				} else{
					//unknown type
					ret= new UnknownSourceId(cdi);
				}
			} else if (cdi.toLowerCase().contains("interpolated")){ 
				ret= new InterpolatedMetadata(cdi);
			}
			else {
				ret= new UnknownSourceId(cdi);
			}
		} catch (MalformedURLException e) {
			logger.warn("Cannot compute URL for CDI "+cdi,e);
			ret=new UnknownSourceId(cdi);

		}
		return ret;


	}

}
