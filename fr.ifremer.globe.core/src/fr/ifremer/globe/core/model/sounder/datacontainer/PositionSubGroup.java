/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handle a set of layer being associated with platform position
 */
public class PositionSubGroup {

	public static final String POSITION_GROUP_PATH = "/Platform/Position/";

	/** Container of this attitude_sub_group */
	private final SounderDataContainer container;
	/** Identification of the /Platform/Attitude group to manage */
	private final String groupId;

	/**
	 * Constructor
	 */
	public PositionSubGroup(SounderDataContainer container, String groupId) throws GIOException {
		this.container = container;
		this.groupId = groupId;
	}

	/** @return the layer corresponding to the PredefinedLayer */
	public <U extends ILoadableLayer<?>> U getLayer(PredefinedLayers<U> layer) throws GIOException {
		return container.getLayer(getPredefinedLayer(layer));
	}

	/** @return true if the specified layer is declared within this container */
	public boolean hasLayer(PredefinedLayers<?> layer) {
		return container.hasLayer(getPredefinedLayer(layer));
	}

	/** @return an optional which contains the specified layer or is empty if it does not exist **/
	public <U extends ILoadableLayer<?>> Optional<U> getOptionalLayer(PredefinedLayers<U> layer) throws GIOException {
		return hasLayer(layer) ? Optional.of(getLayer(layer)) : Optional.empty();
	}

	/** @return the layer corresponding to the PredefinedLayer */
	protected <U extends ILoadableLayer<?>> PredefinedLayers<U> getPredefinedLayer(PredefinedLayers<U> layer) {
		return new PredefinedLayers<>(layer.getGroup().replace("position_sub_group", groupId), layer);
	}

	/**
	 * @return the {@link #groupId}
	 */
	public String getGroupId() {
		return groupId;
	}

}
