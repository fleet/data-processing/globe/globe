package fr.ifremer.globe.core.model.sounder.datacontainer;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;

/**
 * {@link IDataContainerToken} for {@link SounderDataContainer}.
 */
public interface ISounderDataContainerToken extends IDataContainerToken<ISounderNcInfo> {

	SounderDataContainer getDataContainer();

}
