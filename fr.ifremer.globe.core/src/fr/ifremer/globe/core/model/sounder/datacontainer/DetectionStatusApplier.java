/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Importer of validity flags contained in a sounder file
 */
public class DetectionStatusApplier implements IDataContainerOwner, IProcessFunction {

	/** Processed file **/
	private final ISounderNcInfo info;

	/** CSV file with detection status flags **/
	private final File inputFile;

	/**
	 * Constructor
	 */
	public DetectionStatusApplier(ISounderNcInfo info, File inputFile) {
		this.info = info;
		this.inputFile = inputFile;
	}

	/**
	 * Extract validity from CSV and set the values to the sounder file
	 */
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		monitor.subTask(inputFile.getName());
		try (ISounderDataContainerToken token = IDataContainerFactory.grab().book(info, this)) {
			SounderDataContainer dataContainer = token.getDataContainer();
			int swathCount = dataContainer.getInfo().getCycleCount();
			int beamCount = dataContainer.getInfo().getTotalRxBeamCount();

			monitor.beginTask("Processing " + info.getPath(), swathCount * beamCount);
			logger.info("Input file : {}", info.getPath());
			logger.info("Validity flags file : {}", inputFile.getPath());

			ByteLoadableLayer2D statusLayer = dataContainer.getLayer(BathymetryLayers.STATUS);
			ByteLoadableLayer2D statusLayerDetails = dataContainer.getLayer(BathymetryLayers.STATUS_DETAIL);
			LongLoadableLayer1D timeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);
			long timeLayerSize = timeLayer.getSize();

			var lineIndex = 0;
			var flagUpdatedCount = 0;
			var timeWarnAlreadyDisplayed = new AtomicBoolean();
			try (var scanner = new Scanner(inputFile)) {
				scanner.useDelimiter("\\D"); // any non-digit character
				while (scanner.hasNext() && !monitor.isCanceled()) {
					if (scanner.hasNextLong()) {// wait for a line which start with a Long (= time)
						var date = scanner.nextLong(); // skip swathTime

						var swathIndex = scanner.nextInt();
						if (swathIndex < 0 || swathIndex >= swathCount)
							throw new GIOException("Invalid swath index at line : " + lineIndex);

						var beamIndex = scanner.nextInt();
						if (beamIndex < 0 || beamIndex >= beamCount)
							throw new GIOException("Invalid beam index at line : " + lineIndex);

						// Check if the same date for the specified index is retrieved from TimeLayer.
						if (Math.abs(date - DateUtils.nanoSecondToMilli(timeLayer.get(swathIndex))) > 1) {
							if (timeWarnAlreadyDisplayed.compareAndSet(false, true)) {
								logger.warn(
										"The pair index-datetime in validity flag file does not match with file content. Target swaths will be retrieved from datetime.");
							}
							// If date does not match the timeLayer, try to get swath index from date.
							long dateTimeNano = DateUtils.milliSecondToNano(date);
							for (int i = 0; i < timeLayerSize; i++) {
								// Keep 1ms precision
								if (Math.abs(timeLayer.get(i) - dateTimeNano) < 1000000) {
									swathIndex = i;
									break;
								}
								if (i == timeLayerSize - 1)
									throw new GIOException("Invalid date at line : " + lineIndex
											+ " (does not match with date of any swath)");
							}
						}

						var status = scanner.nextByte();
						var statusDetails = scanner.nextByte();
						if (statusLayer.get(swathIndex, beamIndex) != status) {
							statusLayer.set(swathIndex, beamIndex, status);
							flagUpdatedCount++;
						}
						if (statusLayerDetails.get(swathIndex, beamIndex) != statusDetails) {
							statusLayerDetails.set(swathIndex, beamIndex, statusDetails);
						}
						monitor.worked(1);
					} else if (lineIndex != 0) { // first line could be the header line
						throw new GIOException("Line not parseable : " + lineIndex);
					}
					scanner.nextLine();
					lineIndex++;
				}
			}
			dataContainer.flush();
			monitor.done();
			logger.info("{} detection status flags updated.", flagUpdatedCount);
			return Status.OK_STATUS;
		} catch (IOException e) {
			throw GIOException.wrap("Failed to import validity flags from a csv file", e);
		}
	}
}
