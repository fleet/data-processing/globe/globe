/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.sounder;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;

/**
 * Supplier of IDataContainerInfo
 */
public interface ISounderNcInfoSupplier<I extends ISounderNcInfo> {

	/** @return the IDataContainerInfo corresponding to the specified file */
	Optional<I> getSounderNcInfo(String filePath);

	/** @return the IDataContainerInfo corresponding to the specified IInfoStore */
	Optional<I> getSounderNcInfo(IFileInfo fileInfo);
}
