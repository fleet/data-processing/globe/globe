/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.model.sounder.datacontainer.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfoSupplier;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoSupplier;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;

/**
 * Some services for getting IDataContainerInfo/ISounderNcInfo informations.
 */
@Component(name = "globe_model_service_data_container_info", service = IDataContainerInfoService.class)
public class DataContainerInfoService implements IDataContainerInfoService {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(DataContainerInfoService.class);

	/** All registered IDataContainerInfoSupplier */
	protected List<IDataContainerInfoSupplier<?>> dataContainerInfoSuppliers = new ArrayList<>();

	/** All registered ISounderNcInfoSupplier */
	protected List<ISounderNcInfoSupplier<?>> sounderNcInfoSuppliers = new ArrayList<>();

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public <I extends IDataContainerInfo> Optional<I> getDataContainerInfo(String filePath) {
		return (Optional<I>) dataContainerInfoSuppliers.stream().//
				map(supplier -> supplier.getDataContainerInfo(filePath)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public <I extends IDataContainerInfo> Optional<I> getDataContainerInfo(IFileInfo fileInfo) {
		return (Optional<I>) dataContainerInfoSuppliers.stream().//
				map(supplier -> supplier.getDataContainerInfo(fileInfo)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public <I extends ISounderNcInfo> Optional<I> getSounderNcInfo(String filePath) {
		return (Optional<I>) sounderNcInfoSuppliers.stream().//
				map(supplier -> supplier.getSounderNcInfo(filePath)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public <I extends ISounderNcInfo> Optional<I> getSounderNcInfo(IFileInfo fileInfo) {
		return (Optional<I>) sounderNcInfoSuppliers.stream().//
				map(supplier -> supplier.getSounderNcInfo(fileInfo)).//
				filter(Optional::isPresent).//
				findFirst().//
				orElse(Optional.empty());
	}

	/** Accept a new IDataContainerInfoSupplier provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addDataContainerInfoSupplier(IDataContainerInfoSupplier<?> supplier) {
		logger.debug("{} registered : {}", IDataContainerInfoSupplier.class.getSimpleName(),
				supplier.getClass().getName());
		dataContainerInfoSuppliers.add(supplier);
	}

	/** Accept a new IDataContainerInfoSupplier provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addSounderNcInfoSupplier(ISounderNcInfoSupplier<?> supplier) {
		logger.debug("{} registered : {}", ISounderNcInfoSupplier.class.getSimpleName(), supplier.getClass().getName());
		sounderNcInfoSuppliers.add(supplier);
	}

}