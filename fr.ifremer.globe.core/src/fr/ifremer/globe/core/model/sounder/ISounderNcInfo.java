package fr.ifremer.globe.core.model.sounder;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderNavigationData;
import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;

/**
 * Base interface for infostore for netcdf file Those are supposed to be writeable and manageable with
 * {@link SounderDataContainer}
 */
public interface ISounderNcInfo extends IDataContainerInfo, INavigationDataSupplier {

	/**
	 * Sounder file properties getters
	 */
	short getEchoSounderIndex();

	String getEchoSounderModel();

	int getCycleCount();

	int getTotalRxBeamCount();

	int getTotalTxBeamCount();

	Date getLastPingDate();

	Date getFirstPingDate();

	AntennaInstallationParameters getTxParameters();

	List<AntennaInstallationParameters> getRxParameters();

	List<AntennaInstallationParameters> getAntennaParameters();

	default List<SensorInstallationParameters> getAttitudeInstallationParameters() {
		return new ArrayList<>();
	}

	/**
	 * Number of Netcdf group /Sonar/Beam_group. Only a Sonar netcdf file may have several beam groups
	 */
	int getBeamGroupCount();

	Optional<String> getPreferedAttitudeGroupId();

	Optional<SensorInstallationParameters> getPreferedAttitudeInstallationParameters();

	Optional<String> getPreferedPositionGroupId();

	@Override
	default Optional<Pair<Instant, Instant>> getStartEndDate() {
		return Optional.of(new Pair<Instant, Instant>(getFirstPingDate().toInstant(), getLastPingDate().toInstant()));
	}

	default Ellipsoid getEllipsoid() {
		return Ellipsoid.WGS84; // WGS84 by default // TODO : to keep here ?
	}

	long getWcRxBeamCount();

	@Override
	default INavigationData getNavigationData(boolean readonly) throws GIOException {
		return new SounderNavigationData(this, readonly);
	}

	void declareLayers(SounderDataContainer container) throws GException;

	@Override
	default void declareLayers(DataContainer<?> container) throws GException {
		declareLayers((SounderDataContainer) container);
	}

	/** @return the ProcessAssessor dedicated to this sounder info */
	default Optional<ProcessAssessor> getProcessAssessor() {
		return Optional.empty();
	}

	/** @return the OptionalLayersAccessor dedicated to this sounder info */
	default Optional<OptionalLayersAccessor> getOptionalLayersAccessor() {
		return Optional.empty();
	}

	/**
	 * The assessor assists a process to finalize the execution.
	 */
	interface OptionalLayersAccessor {
		/** Across angle in degree */
		default Optional<PredefinedLayers<DoubleLoadableLayer2D>> getAcrossAngleLayer() {
			return Optional.empty();
		}

		/** Along angle (Beam azimut) in degree */
		default Optional<PredefinedLayers<DoubleLoadableLayer2D>> getAlongAngleLayer() {
			return Optional.empty();
		}

		/** Vertical depth in meter */
		default Optional<PredefinedLayers<DoubleLoadableLayer2D>> getVerticalDepthLayer() {
			return Optional.empty();
		}
	}

	/**
	 * The assessor assists a process to finalize the execution.
	 */
	interface ProcessAssessor {

		/** Notify this instance so that it heeds the enforcement of the bias correction */
		void heedBiasCorrection(Optional<String> correctionFileName, List<String> correctorsList) throws GIOException;

		/** True when a bias correction has been enforced */
		boolean biasCorrectionEnforced();

		/**
		 * Notify this instance so that it heeds the enforcement of the tide correction
		 *
		 * @param tideType type of tide applied.
		 */
		void heedTideCorrection(Optional<String> correctionFileName, TideType tideType) throws GIOException;

		/** True when a tide correction has been enforced */
		boolean tideCorrectionEnforced();

		/** Notify this instance so that it heeds the enforcement of the position correction */
		void heedPositionCorrection() throws GIOException;

		/** True when a position correction has been enforced */
		boolean positionCorrectionEnforced();

		/** Notify this instance so that it heeds the enforcement of the draught correction (soundings correction) */
		void heedDraughtCorrection(Optional<String> correctionFileName) throws GIOException;

		/** True when a draught correction (soundings correction) has been enforced */
		boolean draughtCorrectionEnforced();

		/** Notify this instance so that it heeds the enforcement of the velocity correction */
		void heedVelocityCorrection() throws GIOException;

		/** True when a velocity correction has been enforced */
		boolean velocityCorrectionEnforced();

		/** Notify this instance so that it heeds the enforcement of the automatic cleaning (FilTri) */
		void heedAutomaticCleaning() throws GIOException;

		/** True when an automatic cleaning has been enforced */
		boolean automaticCleaningEnforced();

		/** Notify this instance so that it heeds the enforcement of the manual cleaning (Swath Editor) */
		void heedManualCleaning() throws GIOException;

		/** True when a manual cleaning has been enforced */
		boolean manualCleaningEnforced();
	}

	/**
	 * @return the list of group named sonar/Grid_group_XXX
	 */
	default List<String> getGridGroupNames() {
		return List.of();
	}

	/**********************************************************************************************************
	 * TODO : obsolete method of the deprecated {@link ISounderDriverInfo}. Defined here as default because
	 * {@link ISounderNcInfo} (mbg/xsf) have to implement the {@link ISounderDriverInfo} interface for the export to the
	 * old DTM model (which will be removed...).
	 **********************************************************************************************************/

	public String getConstructor();

}
