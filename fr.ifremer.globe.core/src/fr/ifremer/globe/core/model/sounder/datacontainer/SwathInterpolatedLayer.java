package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LayerUtility;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handle a set of (times, values) layers interpolated on swath times
 */
public class SwathInterpolatedLayer {
	private final SounderDataContainer dataContainer;

	// Source layers
	private FloatLoadableLayer1D sourceValues;
	private LongLoadableLayer1D sourceTimes;
	private LongLoadableLayer1D interpolatedTimes;

	// Computed layers
	private FloatLayer1D interpolatedValues;

	private int sourceCount;
	private int swathCount;

	/**
	 * Construct a new {@link SwathInterpolatedLayerLoader} once {@link SounderDataContainer} layers have been declared
	 * 
	 * @throws GIOException
	 */
	public SwathInterpolatedLayer(SounderDataContainer container, FloatLoadableLayer1D sourceValues,
			LongLoadableLayer1D sourceTimes) throws GIOException {
		dataContainer = container;
		this.sourceValues = sourceValues;
		this.sourceTimes = sourceTimes;
		
		// source indices ordered by time
		TreeMap<Long, Integer> sourceIndices = new TreeMap<>();
		sourceCount = (int) sourceValues.getDimensions()[0];
		for (int index = 0; index < sourceCount; index++) {
			sourceIndices.put(sourceTimes.get(index), index);
		}

		this.interpolatedTimes = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);

		// Allocate interpolated layer
		swathCount = dataContainer.getInfo().getCycleCount();
		long[] dims = { swathCount };
		interpolatedValues = initInterpolatedLayer(sourceValues, dims);
		for (int swath = 0; swath < swathCount; swath++) {
			long swathDate = interpolatedTimes.get(swath);
			float interpolatedValue = getInterpolatedValue(swathDate, sourceValues, sourceIndices);
			interpolatedValues.set(swath, interpolatedValue);
		}
	}

	/**
	 * Retrieve swath interpolated layer
	 */
	public FloatLayer1D getLayer() {
		return interpolatedValues;
	}

	/**
	 * Apply back interpolated values on source layer
	 * 
	 * @throws GIOException
	 */
	public void flush() throws GIOException {
		// source indices ordered by time
		TreeMap<Long, Integer> swathIndices = new TreeMap<>();
		for (int swath = 0; swath < swathCount; swath++) {
			swathIndices.put(interpolatedTimes.get(swath), swath);
		}
		for (int index = 0; index < sourceCount; index++) {
			long sourceDate = sourceTimes.get(index);
			float sourceValue = getInterpolatedValue(sourceDate, interpolatedValues, swathIndices);
			sourceValues.set(index, sourceValue);
		}
		sourceValues.flush();
	}

	/*
	 * Init interpolated layer
	 */
	private FloatLayer1D initInterpolatedLayer(IBaseLayer sourceLayer, long[] dims) throws GIOException {
		FloatLayer1D interpolatedLayer = new FloatLayer1D(sourceLayer.getName() + "Interpolated",
				"Interpolation of " + sourceLayer.getLongName(), sourceLayer.getUnit());
		LayerUtility.allocate(interpolatedLayer, dims);
		return interpolatedLayer;
	}

	/**
	 * Does the interpolation.
	 */
	private float getInterpolatedValue(long time, FloatLayer1D valueProvider,
			TreeMap<Long, Integer> indicesByTime)
			throws GIOException {
		// Get a position before and after : pass if not found
		Entry<Long, Integer> previous = indicesByTime.floorEntry(time);
		Entry<Long, Integer> next = indicesByTime.ceilingEntry(time);
		if (previous == null)
			return valueProvider.get(next.getValue());
		else if (next == null)
			return valueProvider.get(previous.getValue());

		// Compute interpolated attitudes
		float coeff = !next.getKey().equals(previous.getKey())
				? ((float) (time - previous.getKey())) / (next.getKey() - previous.getKey())
				: 0;

		float previousValue = valueProvider.get(previous.getValue());
		float nextValue = valueProvider.get(next.getValue());

		return previousValue + coeff * (nextValue - previousValue);
	}

}
