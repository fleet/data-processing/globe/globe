package fr.ifremer.globe.core.model.sounder.datacontainer;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class represents a sounder data container.
 */
public final class SounderDataContainer extends DataContainer<ISounderNcInfo> {

	/** Properties */
	private CompositeCSLayers csLayers;
	private DetectionStatusLayer statusLayer;
	private SonarBeamGroup sonarBeamGroup;

	/**
	 * Constructor
	 */
	public SounderDataContainer(ISounderNcInfo info, boolean readOnly) throws GException {
		super(info, readOnly);
	}

	@Override
	public synchronized void dispose() {
		super.dispose();
		if (csLayers != null)
			csLayers.dispose();
	}

	/** @return a layer with CS */
	public CompositeCSLayers getCsLayers(CoordinateSystem cs) throws GIOException {
		if (csLayers == null)
			csLayers = new CompositeCSLayers(this);
		csLayers.projectionIn(cs);
		return csLayers;
	}

	/** @return true if status layer exists into this container **/
	public boolean hasStatusLayer() {
		return statusLayer != null || (hasLayer(BathymetryLayers.STATUS) && hasLayer(BathymetryLayers.STATUS_DETAIL));
	}

	/** @return {@link DetectionStatusLayer} **/
	public DetectionStatusLayer getStatusLayer() throws GIOException {
		if (statusLayer == null)
			statusLayer = new DetectionStatusLayer(this);
		return statusLayer;
	}

	/** @return the current {@link SonarBeamGroup} **/
	public SonarBeamGroup getBeamGroup() throws GIOException {
		return sonarBeamGroup == null ? getBeamGroup(1) : sonarBeamGroup;
	}

	/** @return {@link SonarBeamGroup} **/
	public SonarBeamGroup getBeamGroup(int beamGroupId) throws GIOException {
		if (sonarBeamGroup != null && sonarBeamGroup.getBeamGroupId() == beamGroupId) {
			return sonarBeamGroup;
		}
		sonarBeamGroup = new SonarBeamGroup(this, beamGroupId);
		return sonarBeamGroup;
	}
}
