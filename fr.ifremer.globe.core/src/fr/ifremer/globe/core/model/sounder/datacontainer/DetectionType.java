package fr.ifremer.globe.core.model.sounder.datacontainer;

/**
 * Utility class managing detection types
 */

public enum DetectionType {
	INVALID((byte) 0), //
	AMPLITUDE((byte) 1), //
	PHASE((byte) 2);

	byte value;

	DetectionType(byte v) {
		value = v;
	}

	public byte get() {
		return value;
	}

}