package fr.ifremer.globe.core.model.sounder;

/**
 * class containing installation parameters for antenna
 */
public record SensorInstallationParameters(//
		String sensorId, //
		double xoffset, // x offset of the installation parameter , positive is forward
		double yoffset, // y offset of the installation parameters, positive is starboard
		double zoffset, // z offset of the installation parameters, positive is downwards.
		double rolloffset, // angle roll offset of the installation parameters
		double pitchoffset, // angle pitch offset of the installation parameters
		double headingoffset)// angle heading offset of the installation parameters
{
}
