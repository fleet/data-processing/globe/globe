package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.S7KSensorType;
import fr.ifremer.globe.core.model.quality.MeasurandQualifierFlags;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.position.PositionSubGroupLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.position.position_sub_group.VendorSpecificLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link INavigationData} for sounder files (MBG, XSF)
 */
public class SounderNavigationData implements INavigationData, IDataContainerOwner {

	/** layers **/
	private DoubleLoadableLayer1D latitudeLayer;
	private DoubleLoadableLayer1D longitudeLayer;

	/** Properties **/
	private ISounderDataContainerToken sounderDataContainerToken;
	protected final SounderDataContainer dataContainer;
	protected final List<Integer> indexes = new ArrayList<>();
	protected final Map<Integer, MeasurandQualifierFlags> qualifierFlagMap = new HashMap<>();
	protected boolean isReadOnly = true;

	/** Metadata **/
	protected final List<NavigationMetadata<?>> metadata = new ArrayList<>();
	public static final NavigationMetadataType<Float> SOUND_SPEED_AT_TRANSDUCER_METADATA = new NavigationMetadataType<>(
			"Sound speed at tranducer");

	/** Constructor **/
	public SounderNavigationData(SounderDataContainer dataContainer) throws GIOException {
		this.dataContainer = dataContainer;
		initLayers();
		initMetadata();
	}

	/** Constructor **/
	public SounderNavigationData(ISounderNcInfo info, boolean readonly) throws GIOException {
		isReadOnly = readonly;
		// get data container
		sounderDataContainerToken = IDataContainerFactory.grab().book(info, this);
		dataContainer = sounderDataContainerToken.getDataContainer();
		initLayers();
		initMetadata();
	}

	/**
	 * Retrieves needed layers to provide navigation data.
	 */
	private void initLayers() throws GIOException {
		// Get required layers
		latitudeLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
		longitudeLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);

		// Keep valid swath
		if (dataContainer.hasLayer(BathymetryLayers.STATUS)) {
			DetectionStatusLayer statusLayer = null;
			statusLayer = dataContainer.getStatusLayer();
			// keep valid soundings
			for (int i = 0; i < dataContainer.getInfo().getCycleCount(); i++) {
				if (statusLayer.isSwathValid(i)) {
					indexes.add(i);
				}
			}
		} else {
			// keep valid soundings
			for (int i = 0; i < dataContainer.getInfo().getCycleCount(); i++) {
				indexes.add(i);
			}
		}

	}

	/**
	 * Defines {@link NavigationMetadata} for MBG/XSF navigation.
	 */
	private void initMetadata() throws GIOException {
		// time
		var timeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);
		metadata.add(NavigationMetadataType.TIME
				.withValueSupplier(index -> DateUtils.nanoSecondToMilli(timeLayer.get(indexes.get(index)))));

		// heading
		var headingLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
		metadata.add(NavigationMetadataType.HEADING.withValueSupplier(index -> headingLayer.get(indexes.get(index))));

		// elevation
		var elevationLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
		metadata.add(
				NavigationMetadataType.ELEVATION.withValueSupplier(index -> elevationLayer.get(indexes.get(index))));

		var waterlineLayer = dataContainer.getOptionalLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);
		if (waterlineLayer.isPresent()) {
			metadata.add(NavigationMetadataType.WATERLINE_TO_CHART_DATUM.withValueSupplier(index -> {
				float waterline = waterlineLayer.get().get(indexes.get(index));
				return (waterline == waterlineLayer.get().getFillValue()) ? Float.NaN : waterline;
			}));
		}

		var tx_transducer_depth = dataContainer.getOptionalLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
		if (tx_transducer_depth.isPresent()) {
			// transducer depth
			metadata.add(NavigationMetadataType.TX_TRANSDUCER_DEPTH
					.withValueSupplier(index -> tx_transducer_depth.get().get(indexes.get(index))));
		}

		// sensor type (Xsf only)
		Optional<String> preferredGroupId = dataContainer.getInfo().getPreferedPositionGroupId();
		if (preferredGroupId.isPresent()) {
			PositionSubGroup posGrp = new PositionSubGroup(dataContainer, preferredGroupId.get());
			var nmeaQualityIndicatorLayer = posGrp.getOptionalLayer(VendorSpecificLayers.SENSOR_QUALITY_INDICATOR);
			var s7kPositioningMethodLayer = posGrp.getOptionalLayer(VendorSpecificLayers.POSITIONING_METHOD);

			if (nmeaQualityIndicatorLayer.isPresent() || s7kPositioningMethodLayer.isPresent()) {
				Optional<LongLoadableLayer1D> positionTimeLayer = posGrp.getOptionalLayer(PositionSubGroupLayers.TIME);
				TreeMap<Long, Integer> positionIndexesByTime = new TreeMap<>();

				if (positionTimeLayer.isPresent()) {
					for (int i = 0; i < positionTimeLayer.get().getDimensions()[0]; i++) {
						positionIndexesByTime.put(positionTimeLayer.get().get(i), i);
					}
				}
				metadata.add(NavigationMetadataType.SENSOR_TYPE.withValueSupplier(index -> {
					long time = timeLayer.get(indexes.get(index));
					int positionIndex = Optional.ofNullable(positionIndexesByTime.floorEntry(time))
							.orElse(positionIndexesByTime.firstEntry()).getValue();
					if (nmeaQualityIndicatorLayer.isPresent()) {
						int type = nmeaQualityIndicatorLayer.get().get(positionIndex);
						return NmeaSensorType.get(type);
					} else if (s7kPositioningMethodLayer.isPresent()) {
						int type = s7kPositioningMethodLayer.get().get(positionIndex);
						return S7KSensorType.get(type);
					}
					return NmeaSensorType.NOT_DEFINED;
				}));
			}
		}

		// sound speed at transducer
		var soundSpeedTransducer = dataContainer.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);
		metadata.add(SOUND_SPEED_AT_TRANSDUCER_METADATA
				.withValueSupplier(index -> soundSpeedTransducer.get(indexes.get(index))));

	}

	/** Close method **/
	@Override
	public boolean isReadOnly() {
		return isReadOnly;
	}

	/** Close method **/
	@Override
	public void close() {
		if (sounderDataContainerToken != null)
			sounderDataContainerToken.close();
	}

	//// GETTERS & SETTERS

	@Override
	public String getFileName() {
		return dataContainer.getInfo().getPath();
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return dataContainer.getInfo().getContentType();
	}

	@Override
	public int size() {
		return indexes.size();
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return latitudeLayer.get(indexes.get(index));
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return longitudeLayer.get(indexes.get(index));
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	@Override
	public MeasurandQualifierFlags getQualifierFlag(int index) {
		return qualifierFlagMap.containsKey(indexes.get(index)) ? qualifierFlagMap.get(indexes.get(index))
				: MeasurandQualifierFlags.NONE;
	}

	@Override
	public void setQualifierFlag(int index, MeasurandQualifierFlags flag) {
		qualifierFlagMap.put(indexes.get(index), flag);
	}

}
