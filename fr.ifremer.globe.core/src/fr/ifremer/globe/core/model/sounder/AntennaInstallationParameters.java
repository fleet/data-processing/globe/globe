package fr.ifremer.globe.core.model.sounder;

/**
 * class containing installation parameters for antenna
 */
public class /* Record */ AntennaInstallationParameters {

	public enum Type {
		UNKNOWN, TX, RX, TXRX
	}

	public final Type type;
	public final String antennaID;
	public final double xoffset; // x offset of the installation parameter , positive is forward
	public final double yoffset; // y offset of the installation parameters, positive is starboard
	public final double zoffset; // z offset of the installation parameters, positive is downwards.
	public final double rolloffset; // angle roll offset of the installation parameters
	public final double pitchoffset; // angle pitch offset of the installation parameters
	public final double headingoffset; // angle heading offset of the installation parameters

	public AntennaInstallationParameters(Type type, String antennaID, double xoffset, double yoffset, double zoffset,
			double rolloffset, double pitchoffset, double headingoffset) {
		super();
		this.type = type;
		this.antennaID = antennaID;
		this.xoffset = xoffset;
		this.yoffset = yoffset;
		this.zoffset = zoffset;
		this.rolloffset = rolloffset;
		this.pitchoffset = pitchoffset;
		this.headingoffset = headingoffset;
	}

}
