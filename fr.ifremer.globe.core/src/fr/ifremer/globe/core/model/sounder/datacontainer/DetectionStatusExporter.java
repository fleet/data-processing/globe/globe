package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.exporter.CsvLayerExporter;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.ToStringOperation;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalker;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalkerParameters;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * CsvLayerExporter customizing the depth value and the header for sounder file
 */
public class DetectionStatusExporter extends CsvLayerExporter<ISounderNcInfo, SounderDataContainer>
		implements IProcessFunction {

	private final LayersWalker layersWalker;

	private final ISounderNcInfo info;

	/** Time-stamp of each ping */
	protected LongLayer1D timeLayer;

	/** Constructor */
	public DetectionStatusExporter(LayersWalker layersWalker, ISounderNcInfo info, File outputFile, boolean withHeader,
			char sep) {
		super(outputFile, new ToStringOperation());
		this.layersWalker = layersWalker;
		this.info = info;
		setWithHeader(withHeader);
		setColumnSeparator(sep);
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		logger.info("With header : {}", isWithHeader());
		logger.info("Separator : {}", getColumnSeparator());
		LayersWalkerParameters<ISounderNcInfo, SounderDataContainer> walkerParameters = new LayersWalkerParameters<>(
				info, this::grabValidityFlagsLayers, this);
		layersWalker.walk(walkerParameters, monitor);
		return Status.OK_STATUS;
	}

	/** @return the list of layers to export */
	private List<AbstractBaseLayer<? extends AbstractBuffer>> grabValidityFlagsLayers(
			SounderDataContainer sounderDataContainer) throws GIOException {
		List<AbstractBaseLayer<? extends AbstractBuffer>> result = new ArrayList<>();

		result.add(sounderDataContainer.getLayer(BathymetryLayers.STATUS));
		result.add(sounderDataContainer.getLayer(BathymetryLayers.STATUS_DETAIL));

		return result;
	}

	/** {@inheritDoc} */
	@Override
	public void initiateLevel(int levelIndex, int[] indexes) throws GIOException {
		super.initiateLevel(levelIndex, indexes);
		if (levelIndex == 1) {
			long swathTime = DateUtils.nanoSecondToMilli(timeLayer.get(indexes[0])); // get time in milliseconds
			outputStringBuilder.append(swathTime);
			outputStringBuilder.append(columnSeparator);
			outputStringBuilder.append(indexes[0]);
			outputStringBuilder.append(columnSeparator);
			outputStringBuilder.append(indexes[1]);
			outputStringBuilder.append(columnSeparator);
		}
	}

	/** {@inheritDoc} */
	@Override
	public <T extends AbstractBaseLayer<?>> void getReady(SounderDataContainer dataContainer, List<T> layers)
			throws GIOException {
		if (withHeader) {
			outputStringBuilder.append("time");
			outputStringBuilder.append(columnSeparator);
			outputStringBuilder.append("swath");
			outputStringBuilder.append(columnSeparator);
			outputStringBuilder.append("beam");
			outputStringBuilder.append(columnSeparator);
		}
		super.getReady(dataContainer, layers);
		timeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);

	}

}
