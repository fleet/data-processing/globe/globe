/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Services on sounder files
 */
public interface IDataContainerInfoService {

	/**
	 * Returns the service implementation of IDataContainerInfoService.
	 */
	static IDataContainerInfoService grab() {
		return OsgiUtils.getService(IDataContainerInfoService.class);
	}

	/** @return the ISounderDriverInfo corresponding to the specified file */
	<I extends IDataContainerInfo> Optional<I> getDataContainerInfo(String filePath);

	/** @return the ISounderDriverInfo corresponding to the specified IInfoStore */
	<I extends IDataContainerInfo> Optional<I> getDataContainerInfo(IFileInfo fileInfo);

	/** @return the ISounderDriverInfo corresponding to the specified file */
	<I extends ISounderNcInfo> Optional<I> getSounderNcInfo(String filePath);

	/** @return the ISounderDriverInfo corresponding to the specified IInfoStore */
	<I extends ISounderNcInfo> Optional<I> getSounderNcInfo(IFileInfo fileInfo);
}
