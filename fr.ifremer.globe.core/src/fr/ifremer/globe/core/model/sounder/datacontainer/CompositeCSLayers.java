package fr.ifremer.globe.core.model.sounder.datacontainer;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LayerUtility;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.mbes.MbesUtils;

/**
 * Handle a set of layer being associated with coordinates systems transform right now only across distance, depth, and
 * along distance vectors are taken into account
 */
public class CompositeCSLayers {

	public static final String PROJ_PREFIX = "proj_";
	public static final String PROJ_DETECTION_X = PROJ_PREFIX + BathymetryLayers.DETECTION_X.getName();
	public static final String PROJ_DETECTION_Y = PROJ_PREFIX + BathymetryLayers.DETECTION_Y.getName();
	public static final String PROJ_DETECTION_Z = PROJ_PREFIX + BathymetryLayers.DETECTION_Z.getName();

	private final SounderDataContainer dataContainer;

	// Position source layers
	private final FloatLoadableLayer2D sourceX_SCS;
	private final FloatLoadableLayer2D sourceY_SCS;
	private final FloatLoadableLayer2D sourceZ_SCS;
	private final DoubleLoadableLayer2D sourceX_FCS_lat;
	private final DoubleLoadableLayer2D sourceY_FCS_lon;

	// Others layers
	private final FloatLoadableLayer1D water_line_to_chart_datum;
	private final FloatLoadableLayer1D headingLayer;
	private final DoubleLoadableLayer1D swathLatitudeLayer;
	private final DoubleLoadableLayer1D swathLongitudeLayer;
	private final FloatLoadableLayer1D platformVerticalOffset;
	private final FloatLoadableLayer1D txTransducerZdepth;

	// Projected layers
	private DoubleLayer2D projectedX;
	private DoubleLayer2D projectedY;
	private DoubleLayer2D projectedZ;

	/** current coordinate system **/
	private CoordinateSystem currentCs;

	private int swathCount;
	private int beamCount;

	// antenna level arms
	private double antennaXOffset;
	private double antennaYOffset;

	// getters
	public DoubleLayer2D getProjectedX() {
		return projectedX;
	}

	public DoubleLayer2D getProjectedY() {
		return projectedY;
	}

	public DoubleLayer2D getProjectedZ() {
		return projectedZ;
	}

	public CoordinateSystem getCurrentCs() {
		return currentCs;
	}

	private DoubleLayer2D initProjectedLayer(String name, IBaseLayer sourceLayer, long[] dims) throws GIOException {
		DoubleLayer2D projectedLayer = new DoubleLayer2D(name, "Projection of " + sourceLayer.getLongName(),
				sourceLayer.getUnit());
		LayerUtility.allocate(projectedLayer, dims);
		return projectedLayer;
	}

	/**
	 * construct a new {@link CompositeCSLayers} once {@link SounderDataContainer} layers have been declared
	 * 
	 * @throws GIOException
	 */
	public CompositeCSLayers(SounderDataContainer container) throws GIOException {
		dataContainer = container;

		// Get needed layers
		headingLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
		swathLatitudeLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
		swathLongitudeLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);
		platformVerticalOffset = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
		txTransducerZdepth = dataContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
		water_line_to_chart_datum = dataContainer.getLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);

		// Level arms (used for projection in "ACS")
		AntennaInstallationParameters txParameters = dataContainer.getInfo().getTxParameters();
		if (txParameters != null) {
			antennaXOffset = dataContainer.getInfo().getTxParameters().xoffset;
			antennaYOffset = dataContainer.getInfo().getTxParameters().yoffset;
		} else {
			antennaXOffset = 0d;
			antennaYOffset = 0d;
		}

		// Sources layer in SCS
		sourceX_SCS = dataContainer.getLayer(BathymetryLayers.DETECTION_X);
		sourceY_SCS = dataContainer.getLayer(BathymetryLayers.DETECTION_Y);
		sourceZ_SCS = dataContainer.getLayer(BathymetryLayers.DETECTION_Z);
		sourceX_FCS_lat = dataContainer.getLayer(BathymetryLayers.DETECTION_LATITUDE);
		sourceY_FCS_lon = dataContainer.getLayer(BathymetryLayers.DETECTION_LONGITUDE);

		// Allocate projected layers
		swathCount = dataContainer.getInfo().getCycleCount();
		beamCount = dataContainer.getInfo().getTotalRxBeamCount();
		long[] dims = { swathCount, beamCount };
		projectedX = initProjectedLayer(PROJ_DETECTION_X, sourceX_SCS, dims);
		projectedY = initProjectedLayer(PROJ_DETECTION_Y, sourceY_SCS, dims);
		projectedZ = initProjectedLayer(PROJ_DETECTION_Z, sourceZ_SCS, dims);
	}

	/**
	 * Removes projected layers.
	 */
	protected void dispose() {
		projectedX.dispose();
		projectedY.dispose();
		projectedZ.dispose();
	}

	/**
	 * Does a projection into the specified coordinate system
	 */
	public void projectionIn(CoordinateSystem coordinateSystem) throws GIOException {
		if (coordinateSystem != currentCs) {
			// Change to another coordinate system
			currentCs = coordinateSystem;
			switch (coordinateSystem) {
			case SCS:
				projectIntoSCS();
				break;
			case FCS:
				projectIntoFCS();
				break;
			case ACS:
				projectIntoACS();
				break;
			case VCS:
				throw new GIOException("not implemented");
			}
		}
	}

	/**
	 * Flushes projected layers (update sounder data container source layers)
	 */
	public void flush() throws GIOException {
		if (currentCs == null)
			throw new GIOException("Coordinate system not defined");
		switch (currentCs) {
		case SCS:
			flushFromSCS();
			break;
		case FCS:
			flushFromFCS();
			break;
		case ACS:
			flushFromACS();
			break;
		case VCS:
			throw new GIOException("not implemented");
		}

		// flush source layers
		sourceX_SCS.flush();
		sourceY_SCS.flush();
		sourceZ_SCS.flush();
		sourceX_FCS_lat.flush();
		sourceY_FCS_lon.flush();
	}

	interface IProjection {
		double apply(int swath, int beam) throws GIOException;
	}

	interface IProjectionFlusher {
		void apply(double projectedValue, int swath, int beam) throws GIOException;
	}

	/**
	 * Load the projected layers
	 * 
	 * sourceLayers (SCS) -> projectedLayers (other coordinate system)
	 */
	private void loadProjection(IProjection xProj, IProjection yProj, IProjection zProj) throws GIOException {
		for (int swath = 0; swath < swathCount; swath++)
			for (int beam = 0; beam < beamCount; beam++) {
				projectedX.set(swath, beam, xProj.apply(swath, beam));
				projectedY.set(swath, beam, yProj.apply(swath, beam));
				projectedZ.set(swath, beam, zProj.apply(swath, beam));
			}
	}

	/**
	 * Flush the projected layers
	 * 
	 * projectedLayers (other coordinate system) -> sourceLayers (SCS)
	 */
	private void flushProjection(IProjectionFlusher xProj, IProjectionFlusher yProj, IProjectionFlusher zProj)
			throws GIOException {
		for (int swath = 0; swath < swathCount; swath++)
			for (int beam = 0; beam < beamCount; beam++) {
				xProj.apply(projectedX.get(swath, beam), swath, beam);
				yProj.apply(projectedY.get(swath, beam), swath, beam);
				zProj.apply(projectedZ.get(swath, beam), swath, beam);
			}
	}

	/**
	 * Updates soundings' latitude and longitude
	 */
	public void recomputeSoundingLatLon() throws GIOException {
		for (int iSwath = 0; iSwath < swathCount; iSwath++)
			for (int iBeam = 0; iBeam < beamCount; iBeam++) {
				// Compute lat lon
				double alongDist = sourceX_SCS.get(iSwath, iBeam);
				double acrossDist = sourceY_SCS.get(iSwath, iBeam);

				double heading = headingLayer.get(iSwath);
				double lat = swathLatitudeLayer.get(iSwath);
				double lon = swathLongitudeLayer.get(iSwath);
				Ellipsoid ellipsoid = dataContainer.getInfo().getEllipsoid();

				double[] normRayon = MbesUtils.calcNormRayon(lat, ellipsoid);
				double norm = normRayon[0];
				double r = normRayon[1];

				double sinHead = Math.sin(Math.toRadians(heading));
				double cosHead = Math.cos(Math.toRadians(heading));

				double[] latLon = MbesUtils.acrossAlongToLatitudeLongitude(lon, lat, alongDist, acrossDist, r, sinHead,
						cosHead, norm);

				sourceX_FCS_lat.set(iSwath, iBeam, latLon[0]);
				sourceY_FCS_lon.set(iSwath, iBeam, latLon[1]);
			}
	}

	/// Z translations

	/** z translation SCS -> FCS : z_FCS = z_SCS - platformVerticalOffset - tide - draught **/
	public double zTranslateSCStoFCS(int iSwath, double zSCS) throws GIOException {
		return zSCS - platformVerticalOffset.get(iSwath) - water_line_to_chart_datum.get(iSwath);
	}

	/** z translation FCS -> SCS : z_SCS = z_FCS + platformVerticalOffset + tide + draught **/
	public double zTranslateFCStoSCS(int iSwath, double zFCS) throws GIOException {
		return zFCS + platformVerticalOffset.get(iSwath) + water_line_to_chart_datum.get(iSwath);
	}

	/** z translation SCS -> ACS : z_ACS = z_SCS - txTransducerZdepth - platformVerticalOffset" **/
	public double zTranslateSCStoACS(int iSwath, double zSCS) throws GIOException {
		return zSCS - txTransducerZdepth.get(iSwath) - platformVerticalOffset.get(iSwath);
	}

	/** z translation ACS -> SCS : z_SCS = z_ACS + txTransducerZdepth + platformVerticalOffset **/
	public double zTranslateACStoSCS(int iSwath, double zACS) throws GIOException {
		return zACS + txTransducerZdepth.get(iSwath) + platformVerticalOffset.get(iSwath);
	}

	/** z translation ACS -> FCS : z_FCS = z_ACS + tx_z_depth + tide + draught **/
	public double zTranslateACStoFCS(int iSwath, double zACS) throws GIOException {
		return zTranslateACStoFCS(iSwath, zACS, txTransducerZdepth.get(iSwath));
	}

	/**
	 * z translation ACS -> FCS with a specified txTransducerDepth (useful in Bias Correction when the local immersion
	 * data are changed)
	 */
	public double zTranslateACStoFCS(int iSwath, double zACS, float txTransducerDepth) throws GIOException {
		return zACS + txTransducerDepth - water_line_to_chart_datum.get(iSwath);
	}

	/**********************************************************************************************************
	 * SCS: Surface Coordinate System
	 **********************************************************************************************************/

	/** source layers -> SCS **/
	private void projectIntoSCS() throws GIOException {
		loadProjection(sourceX_SCS::get, sourceY_SCS::get, sourceZ_SCS::get); // directly from source layers
	}

	/** SCS -> source layers **/
	private void flushFromSCS() throws GIOException {
		flushProjection(
				// Flush X projected values
				(xProj, iSwath, iBeam) -> sourceX_SCS.set(iSwath, iBeam, (float) xProj),
				// Flush Y projected values
				(yProj, iSwath, iBeam) -> sourceY_SCS.set(iSwath, iBeam, (float) yProj),
				// Flush Z projected values
				(zProj, iSwath, iBeam) -> sourceZ_SCS.set(iSwath, iBeam, (float) zProj));
		recomputeSoundingLatLon();
	}

	/**********************************************************************************************************
	 * FCS: Fixed Coordinate System
	 **********************************************************************************************************/

	/** source layers -> FCS **/
	private void projectIntoFCS() throws GIOException {
		loadProjection(
				// X projection (directly from source layers)
				sourceX_FCS_lat::get,
				// Y projection (directly from source layers)
				sourceY_FCS_lon::get,
				// Z translation
				(iSwath, iBeam) -> zTranslateSCStoFCS(iSwath, sourceZ_SCS.get(iSwath, iBeam)));
	}

	/** FCS -> source layers **/
	private void flushFromFCS() throws GIOException {
		flushProjection(
				// Flush X projected values
				(xProj, iSwath, iBeam) -> sourceX_FCS_lat.set(iSwath, iBeam, xProj),
				// Flush Y projected values
				(yProj, iSwath, iBeam) -> sourceY_FCS_lon.set(iSwath, iBeam, yProj),
				// Z translation
				(zProj, iSwath, iBeam) -> sourceZ_SCS.set(iSwath, iBeam, (float) zTranslateFCStoSCS(iSwath, zProj)));
		// TODO update source_SCS
	}

	/**********************************************************************************************************
	 * ACS: Antenna Coordinate System
	 **********************************************************************************************************/

	/** source layers -> ACS **/
	private void projectIntoACS() throws GIOException {
		loadProjection(
				// X projection
				(iSwath, iBeam) -> sourceX_SCS.get(iSwath, iBeam) - antennaXOffset,
				// Y projection
				(iSwath, iBeam) -> sourceY_SCS.get(iSwath, iBeam) - antennaYOffset,
				// Z translation
				(iSwath, iBeam) -> zTranslateSCStoACS(iSwath, sourceZ_SCS.get(iSwath, iBeam)));
	}

	/** ACS -> source layers **/
	private void flushFromACS() throws GIOException {
		flushProjection(
				// Flush X projected values
				(xProj, iSwath, iBeam) -> sourceX_SCS.set(iSwath, iBeam, (float) (xProj + antennaXOffset)),
				// Flush Y projected values
				(yProj, iSwath, iBeam) -> sourceY_SCS.set(iSwath, iBeam, (float) (yProj + antennaYOffset)),
				// Z translation
				(zProj, iSwath, iBeam) -> sourceZ_SCS.set(iSwath, iBeam, (float) zTranslateACStoSCS(iSwath, zProj)));
		recomputeSoundingLatLon();
	}
}
