package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.core.model.attitude.AttitudeInterpolator;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LayerUtility;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.VendorSpecificLayers;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.maths.MatrixBuilder;
import fr.ifremer.globe.utils.maths.Ops;
import fr.ifremer.globe.utils.maths.RotationMatrix3x3D;

/**
 * Handle a set of layer being associated with coordinates systems transform angles.
 */
public class DetectionBeamPointingAnglesLayers {
	public static final String BEAM_POINTING_ACROSS_ANGLE_SCS = "beam_across_angle_scs";

	private final SounderDataContainer dataContainer;

	// Source layers
	private FloatLoadableLayer2D detectionBeamPointingAngle;

	// Computed layers
	private FloatLayer2D beamPointingAcrossAngleSCS;

	private int swathCount;
	private int beamCount;

	/**
	 * Retrieve computed across angle layer with reference to SCS.
	 */
	public Optional<FloatLayer2D> getAcrossAngleScsLayer() {
		return Optional.ofNullable(beamPointingAcrossAngleSCS);
	}

	/**
	 * Construct a new {@link DetectionBeamPointingAnglesLayers} once {@link SounderDataContainer} layers have been
	 * declared
	 * 
	 * @throws GIOException
	 */
	public DetectionBeamPointingAnglesLayers(SounderDataContainer container) throws GIOException {
		dataContainer = container;

		// Source layer in ACS
		boolean hasBathy = dataContainer.hasLayer(BathymetryLayers.DETECTION_BEAM_POINTING_ANGLE);
		if (hasBathy) {
			detectionBeamPointingAngle = dataContainer.getLayer(BathymetryLayers.DETECTION_BEAM_POINTING_ANGLE);
			// Allocate projected layer
			swathCount = dataContainer.getInfo().getCycleCount();
			beamCount = dataContainer.getInfo().getTotalRxBeamCount();
			long[] dims = { swathCount, beamCount };
			beamPointingAcrossAngleSCS = initProjectedLayer(BEAM_POINTING_ACROSS_ANGLE_SCS, detectionBeamPointingAngle,
					dims);

			ShortLoadableLayer2D antennaIndexLayer = dataContainer
					.getLayer(BathymetryLayers.DETECTION_RX_TRANSDUCER_INDEX);
			ShortLoadableLayer2D txBeamLayer = dataContainer.getLayer(BathymetryLayers.DETECTION_TX_BEAM);
			Optional<ByteLoadableLayer1D> beamStabilisationLayer = dataContainer
					.getOptionalLayer(BathymetryLayers.DETECTION_BEAM_STABILISATION);
			Optional<FloatLoadableLayer2D> txDelayLayer = dataContainer
					.getOptionalLayer(VendorSpecificLayers.TRANSMIT_TIME_DELAY);
			FloatLoadableLayer2D twoWayTravelTime = dataContainer
					.getLayer(BathymetryLayers.DETECTION_TWO_WAY_TRAVEL_TIME);
			LongLayer1D swathDateLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);

			AttitudeInterpolator attInterpolator = new AttitudeInterpolator(new SounderAttitudeData(dataContainer));

			int antennaCount = dataContainer.getInfo().getAntennaParameters().size();
			int txBeamCount = dataContainer.getInfo().getTotalTxBeamCount();
			
			RotationMatrix3x3D[] rotA2V = new RotationMatrix3x3D[antennaCount];
			for (int i = 0; i < antennaCount; i++) {
				AntennaInstallationParameters params = dataContainer.getInfo().getAntennaParameters().get(i);
				rotA2V[i] = new RotationMatrix3x3D();
				MatrixBuilder.setDegRotationMatrix(params.headingoffset, params.pitchoffset, params.rolloffset,
						rotA2V[i]);
			}
			RotationMatrix3x3D rotV2S = new RotationMatrix3x3D();
			RotationMatrix3x3D rotACS = new RotationMatrix3x3D();
			RotationMatrix3x3D rotVCS = new RotationMatrix3x3D();
			RotationMatrix3x3D rotSCS = new RotationMatrix3x3D();
			
			for (int swath = 0; swath < swathCount; swath++) {	
				boolean stabilised = beamStabilisationLayer.isPresent() && beamStabilisationLayer.get().get(swath) == 1;
				
				long swathDate = swathDateLayer.get(swath);
				for (int beam = 0; beam < beamCount; beam++) {
					if(stabilised) {
						//ME70 case
						beamPointingAcrossAngleSCS.set(swath, beam, detectionBeamPointingAngle.get(swath, beam));
					} else {
						int rxAntennaIndex = antennaIndexLayer.get(swath, beam);
						if (rxAntennaIndex < 0 || rxAntennaIndex >= antennaCount)
							continue; // status invalid
						short txBeamIndex = txBeamLayer.get(swath, beam);
						if (txBeamIndex < 0 || txBeamIndex >= txBeamCount)
							continue; // status invalid
						
						MatrixBuilder.setDegRollRotationMatrix(detectionBeamPointingAngle.get(swath, beam), rotACS);
						Ops.mult(rotA2V[rxAntennaIndex], rotACS, rotVCS);
	
						float txDelay = txDelayLayer.isPresent() ? txDelayLayer.get().get(swath, txBeamIndex) : 0;
						long rxDelayInNanos = (long) (1E+9 * (txDelay + twoWayTravelTime.get(swath, beam)));
						long estimatedRxDate = swathDate + rxDelayInNanos;
	
						MatrixBuilder.setDegRotationMatrix(0.f, attInterpolator.getPitch(estimatedRxDate),
								attInterpolator.getRoll(estimatedRxDate), rotV2S);
	
						Ops.mult(rotV2S, rotVCS, rotSCS);
						float scsRollAngle = (float) Math.toDegrees(Ops.toEulerAngle(rotSCS).getX());
						beamPointingAcrossAngleSCS.set(swath, beam, scsRollAngle);
					}
				}
			}
		}
	}

	private FloatLayer2D initProjectedLayer(String name, IBaseLayer sourceLayer, long[] dims) throws GIOException {
		FloatLayer2D projectedLayer = new FloatLayer2D(name, "Projection of " + sourceLayer.getLongName(),
				sourceLayer.getUnit());
		LayerUtility.allocate(projectedLayer, dims);
		return projectedLayer;
	}

	/**
	 * Remove computed layers.
	 */
	protected void dispose() {
		if (beamPointingAcrossAngleSCS != null)
			beamPointingAcrossAngleSCS.dispose();
	}

}
