package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.core.model.attitude.IAttitudeData;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.SensorInstallationParameters;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.attitude.AttitudeSubGroupLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link IAttitudeData} for sounder files (MBG, XSF)
 */
public class SounderAttitudeData implements IAttitudeData, IDataContainerOwner {

	/** layers **/
	private LongLoadableLayer1D timeLayer;
	private FloatLoadableLayer1D rollLayer;
	private FloatLoadableLayer1D pitchLayer;
	private FloatLoadableLayer1D headingLayer;
	private FloatLoadableLayer1D verticalOffsetLayer;

	/** Properties **/
	private ISounderDataContainerToken sounderDataContainerToken;
	protected final SounderDataContainer dataContainer;
	protected Optional<SensorInstallationParameters> attitudeParameters;

	/** Constructor **/
	public SounderAttitudeData(SounderDataContainer dataContainer) throws GIOException {
		this.dataContainer = dataContainer;
		initLayers();
	}

	/** Constructor **/
	public SounderAttitudeData(ISounderNcInfo info) throws GIOException {
		// get data container
		sounderDataContainerToken = IDataContainerFactory.grab().book(info, this);
		dataContainer = sounderDataContainerToken.getDataContainer();
		initLayers();
	}

	/**
	 * Retrieves needed layers to provide navigation data.
	 */
	private void initLayers() throws GIOException {
		// Get required layers

		Optional<String> preferedGroupId = dataContainer.getInfo().getPreferedAttitudeGroupId();
		if (preferedGroupId.isPresent()) {
			AttitudeSubGroup attitudeGrp = new AttitudeSubGroup(dataContainer, preferedGroupId.get());
			timeLayer = attitudeGrp.getLayer(AttitudeSubGroupLayers.TIME);
			rollLayer = attitudeGrp.getLayer(AttitudeSubGroupLayers.ROLL);
			pitchLayer = attitudeGrp.getLayer(AttitudeSubGroupLayers.PITCH);
			headingLayer = attitudeGrp.getLayer(AttitudeSubGroupLayers.HEADING);
			verticalOffsetLayer = attitudeGrp.getLayer(AttitudeSubGroupLayers.VERTICAL_OFFSET);
			// retrieve installation parameters
			attitudeParameters = dataContainer.getInfo().getPreferedAttitudeInstallationParameters();
		} else {
			timeLayer = dataContainer.getLayer(AttitudeSubGroupLayers.TIME);
			rollLayer = dataContainer.getLayer(AttitudeSubGroupLayers.ROLL);
			pitchLayer = dataContainer.getLayer(AttitudeSubGroupLayers.PITCH);
			headingLayer = dataContainer.getLayer(AttitudeSubGroupLayers.HEADING);
			verticalOffsetLayer = dataContainer.getLayer(AttitudeSubGroupLayers.VERTICAL_OFFSET);
			// retrieve installation parameters
			attitudeParameters = dataContainer.getInfo().getPreferedAttitudeInstallationParameters();
		}
	}

	/** Close method **/
	@Override
	public void close() {
		if (sounderDataContainerToken != null)
			sounderDataContainerToken.close();
	}

	//// GETTERS & SETTERS

	@Override
	public String getFileName() {
		return dataContainer.getInfo().getPath();
	}

	@Override
	public int size() {
		return (int) timeLayer.getDimensions()[0];
	}

	@Override
	public long getEpochTimeNano(int index) throws GIOException {
		return timeLayer.get(index);
	}

	@Override
	public float getRoll(int index) throws GIOException {
		return rollLayer.get(index);
	}

	@Override
	public float getPitch(int index) throws GIOException {
		return pitchLayer.get(index);
	}

	@Override
	public float getHeading(int index) throws GIOException {
		return headingLayer.get(index);
	}

	@Override
	public float getVerticalOffset(int index) throws GIOException {
		return verticalOffsetLayer.get(index);
	}
}
