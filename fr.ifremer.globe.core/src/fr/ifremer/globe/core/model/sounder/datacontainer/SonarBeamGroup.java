/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.core.runtime.datacontainer.Group;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IVlenLoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handle a set of layer being associated with ping based sonar backscatter data and associated metadata
 */
public class SonarBeamGroup {
	public static final String BEAM_GROUP = "Beam_group";
	public static final String BEAM_GROUP_PATH = "/Sonar/" + BEAM_GROUP;

	/** Container of this Beam_group */
	private final SounderDataContainer container;
	/** Netcdf Group of Beam_group[beamGroupId] */
	private final Group ncBeamGroup;
	/** Identification of the /Sonar/Beam_group to manage */
	private final int beamGroupId;


	/**
	 * Constructor
	 */
	public SonarBeamGroup(SounderDataContainer container, int beamGroupId) throws GIOException {
		this.container = container;
		ncBeamGroup = container.getGroup(BEAM_GROUP_PATH + beamGroupId);
		this.beamGroupId = beamGroupId;
	}

	/** @return the layer corresponding to the PredefinedLayer */
	public <U extends ILoadableLayer<?>> U getLayer(PredefinedLayers<U> layer) throws GIOException {
		return container.getLayer(getPredefinedLayer(layer));
	}
	
	/** @return an optional which contains the specified layer or is empty if it does not exist **/
	public <U extends ILoadableLayer<?>> Optional<U> getOptionalLayer(PredefinedLayers<U> layer) throws GIOException {
		return container.getOptionalLayer(getPredefinedLayer(layer));
	}
	
	/** @return true if the specified layer is declared within this container */
	public boolean hasLayer(PredefinedLayers<?> layer) {
		return container.hasLayer(getPredefinedLayer(layer));
	}

	/** @return the layer corresponding to the PredefinedLayer */
	protected <U extends ILoadableLayer<?>> PredefinedLayers<U> getPredefinedLayer(PredefinedLayers<U> layer) {
		var layerOfBeamGroup = beamGroupId > 1 ? new PredefinedLayers<>(BEAM_GROUP_PATH + beamGroupId, layer) : layer;
		if (layer == BeamGroup1Layers.PING_TIME && !container.hasLayer(layerOfBeamGroup))
			// The SONAR-netCDF4 convention :
			// To avoid duplication of timestamp data, a coordinate variable can be used across multiple subgroups.
			// For organisational reasons, it is then recommended that such coordinate variables be located in the
			// /Sonar group.
			layerOfBeamGroup = new PredefinedLayers<U>("/Sonar", layer);
		return layerOfBeamGroup;
	}

	@SuppressWarnings("unchecked")
	public <T extends IVlenLoadableLayer> T getVlenLayer(String name) throws GIOException {
		return (T) ncBeamGroup.getVlenLayer(name);
	}

	/**
	 * @return the {@link #beamGroupId}
	 */
	public int getBeamGroupId() {
		return beamGroupId;
	}

	public String getPath() {
		return BEAM_GROUP_PATH + beamGroupId;
	}

}
