/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.sounder.datacontainer;

import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handle a set of layer being associated with plateform attitude
 */
public class AttitudeSubGroup {

	public static final String ATTITUDE_GROUP_PATH = "/Platform/Attitude/";

	/** Container of this attitude_sub_group */
	private final SounderDataContainer container;
	/** Identification of the /Platform/Attitude group to manage */
	private final String groupId;

	/**
	 * Constructor
	 */
	public AttitudeSubGroup(SounderDataContainer container, String groupId) throws GIOException {
		this.container = container;
		this.groupId = groupId;
	}

	/** @return the layer corresponding to the PredefinedLayer */
	public <U extends ILoadableLayer<?>> U getLayer(PredefinedLayers<U> layer) throws GIOException {
		return container.getLayer(getPredefinedLayer(layer));
	}

	/** @return true if the specified layer is declared within this container */
	public boolean hasLayer(PredefinedLayers<?> layer) {
		return container.hasLayer(getPredefinedLayer(layer));
	}

	/** @return the layer corresponding to the PredefinedLayer */
	protected <U extends ILoadableLayer<?>> PredefinedLayers<U> getPredefinedLayer(PredefinedLayers<U> layer) {
		return new PredefinedLayers<>(layer.getGroup().replace("attitude_sub_group", groupId), layer);
	}


	/**
	 * @return the {@link #groupId}
	 */
	public String getGroupId() {
		return groupId;
	}

}
