/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.model.sounder.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;

/**
 * Supplier of IDataContainerInfo
 */
public interface IDataContainerInfoSupplier<I extends IDataContainerInfo> {

	/** @return the IDataContainerInfo corresponding to the specified file */
	Optional<I> getDataContainerInfo(String filePath);

	/** @return the IDataContainerInfo corresponding to the specified IInfoStore */
	Optional<I> getDataContainerInfo(IFileInfo fileInfo);
}
