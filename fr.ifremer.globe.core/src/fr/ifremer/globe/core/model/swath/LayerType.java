package fr.ifremer.globe.core.model.swath;

import java.util.Optional;
import java.util.stream.Stream;

import fr.ifremer.globe.core.model.raster.RasterInfo;

/** Kind of layer managed in Swath editor */
public enum LayerType {

	ELEVATION(RasterInfo.RASTER_ELEVATION, 32767), //
	ELEVATION_MIN(RasterInfo.RASTER_ELEVATION_MIN, 32767), //
	ELEVATION_MAX(RasterInfo.RASTER_ELEVATION_MAX, 32767), //
	STDEV(RasterInfo.RASTER_STDEV, 32767), //
	BACKSCATTER(RasterInfo.RASTER_BACKSCATTER, 32767), //
	VSOUNDINGS("Nb Valid Sounds/Cell", 2147483647), //
	ISOUNDINGS("Nb Invalid Sounds/Cell", -1);

	protected final String label;
	protected final double missingValue;

	private LayerType(String label, double missingValue) {
		this.label = label;
		this.missingValue = missingValue;
	}

	/**
	 * @return the {@link #label}
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the {@link #missingValue}
	 */
	public double getMissingValue() {
		return missingValue;
	}

	/**
	 * @return the {@link LayerType} with the provided label if exists.
	 */
	public static Optional<LayerType> fromLabel(String label) {
		return Stream.of(values()).filter(value -> value.getLabel().equals(label)).findFirst();
	}
}
