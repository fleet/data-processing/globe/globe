package fr.ifremer.globe.core.processes.filtri.process;

import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.processes.filtri.model.IFilTriSoundingsProxy;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Point_dt;
import fr.ifremer.globe.utils.exception.GIOException;

public class HeightsFilter extends AbstractFilter {

	double heightInvalidLimit;
	int heightIterationNumber;

	public HeightsFilter(IProgressMonitor monitor, double heightInvalidLimit, int heightIterationNumber) {
		super(monitor);
		this.heightInvalidLimit = heightInvalidLimit;
		this.heightIterationNumber = heightIterationNumber;
	}

	@Override
	public void compute(IFilTriSoundingsProxy sounds) throws GIOException {
		this.filteredNbSounds = 0;
		try {
			// progress
			this.taskWork = (WORK_FILTER / this.heightIterationNumber) / 2;
			
			for (int iter = 1; iter <= this.heightIterationNumber; iter++) {
				this.subTask = "Iteration " + iter + "/" + this.heightIterationNumber;
				this.monitor.subTask(this.subTask + " : building triangulation ...");
				updateTriangulation(sounds);
				if (this.monitor.isCanceled()) {
					break;
				}
				invalidate(sounds);
				if (this.monitor.isCanceled()) {
					break;
				}
			}
		} finally {
			clean();
		}
	}

	protected void invalidate(IFilTriSoundingsProxy sounds) throws GIOException {
		this.monitor.subTask(this.subTask + " : computing ...");
		double zOffsetMean = zOffsetMean();

		double validLimit = this.heightInvalidLimit * zOffsetMean;
		int cpt = 0;

		// progress
		int workNb = (int) Math.ceil(this.dt.size() / 100.0);

		Iterator<Point_dt> vIterator = this.dt.verticesIterator();

		this.monitor.subTask(this.subTask + " : filtering ...");
		while (vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();
			if (vertex.info() >= 0 && zOffset(vertex) > validLimit) {
				sounds.setValid(vertex.info(), false);
				this.filteredNbSounds++;
			}

			if (this.taskWork > 0 && cpt++ % workNb == 0) {
				this.monitor.worked(this.taskWork / 100);
			}
			if (this.monitor.isCanceled()) {
				break;
			}
		}
	}

}
