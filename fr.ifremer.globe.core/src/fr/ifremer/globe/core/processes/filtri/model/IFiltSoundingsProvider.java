package fr.ifremer.globe.core.processes.filtri.model;

import java.util.Iterator;

/**
 * Provider of a set of FiltSoundings.
 */
public interface IFiltSoundingsProvider {

	/**
	 * @return an iterator on a set of {@link IFilTriSoundingsProxy}
	 * @param maxSoundCount max number of sounds allowed by iteration.
	 */
	Iterator<IFilTriSoundingsProxy> getSoundingsIterator(int maxSoundCount);

}
