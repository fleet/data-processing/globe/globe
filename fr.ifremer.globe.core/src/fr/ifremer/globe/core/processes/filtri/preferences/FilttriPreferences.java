package fr.ifremer.globe.core.processes.filtri.preferences;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DoublePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

public class FilttriPreferences extends PreferenceComposite {

	public enum FilteringMethod {
		DELAUNAY_HEIGHTS(0, "Delaunay/Heights"),
		DELAUNAY_NORMALS(1, "Delaunay/Normals"),
		DELAUNAY_NEIGHBOURS(2, "Delaunay/Neighbours");

		int value;
		String name;

		FilteringMethod(int value, String name) {
			this.value = value;
			this.name = name;
		}

		public int getValue() {
			return this.value;
		}

		public static FilteringMethod get(String name) {
			FilteringMethod filteringMethod = DELAUNAY_HEIGHTS;
			for (FilteringMethod e : FilteringMethod.values()) {
				if (name.equalsIgnoreCase(e.name)) {
					filteringMethod = FilteringMethod.valueOf(e.name());
					break;
				}
			}
			return filteringMethod;
		}
	}

	private BooleanPreferenceAttribute saveOption;
	private StringPreferenceAttribute filePrefix;
	private DirectoryPreferenceAttribute outputDirectory;
	private EnumPreferenceAttribute filteringMethod;
	private IntegerPreferenceAttribute sliceSize;
	private DoublePreferenceAttribute heightInvalidLimit;
	private IntegerPreferenceAttribute heightIterationNumber;
	private DoublePreferenceAttribute normalHeightInvalidLimit;
	private IntegerPreferenceAttribute normalSelectLimit;
	private DoublePreferenceAttribute normalAngleInvalidLimit;
	private DoublePreferenceAttribute neighbourHeightInvalidLimit;
	private IntegerPreferenceAttribute neighbourSelectLimit;
	private DoublePreferenceAttribute neighnourDistanceInvalidLimit;

	public FilttriPreferences(PreferenceComposite superNode, String nodeName) {
		super(superNode, nodeName);

		saveOption = new BooleanPreferenceAttribute("1- New file option", "New file option", true);
		this.declareAttribute(saveOption);
		Path defaultDir = FileSystems.getDefault().getPath(System.getProperty("user.home")).toAbsolutePath();
		outputDirectory = new DirectoryPreferenceAttribute("2- Output directory", "OutputDirectory", defaultDir);
		this.declareAttribute(outputDirectory);
		filePrefix = new StringPreferenceAttribute("3- New file prefix", "New file prefix", "filttri");
		this.declareAttribute(filePrefix);
		filteringMethod = new EnumPreferenceAttribute("4- Filtering method", "Filtering method",
				FilteringMethod.DELAUNAY_NORMALS);
		this.declareAttribute(filteringMethod);
		PreferenceComposite FilteringHeight = new PreferenceComposite(this, "Filtering heights");
		heightInvalidLimit = new DoublePreferenceAttribute("1-Height coefficient for sounding invalidation",
				"Height coefficient for sounding invalidation", 4.0);
		FilteringHeight.declareAttribute(heightInvalidLimit);
		heightIterationNumber = new IntegerPreferenceAttribute("2-Number of treatments iterations",
				"Number of treatments iterations", 30);
		FilteringHeight.declareAttribute(heightIterationNumber);

		PreferenceComposite FilteringNormals = new PreferenceComposite(this, "Filtering normals");
		normalHeightInvalidLimit = new DoublePreferenceAttribute("1-Height coefficient for first invalidation",
				"Height coefficient for first invalidation", 6.0);
		FilteringNormals.declareAttribute(normalHeightInvalidLimit);
		normalSelectLimit = new IntegerPreferenceAttribute("2-Sounding selection parameter",
				"Sounding selection parameter", 4);
		FilteringNormals.declareAttribute(normalSelectLimit);
		normalAngleInvalidLimit = new DoublePreferenceAttribute("3-Maximum angle between normals",
				"Maximum angle between normals", 60.0);
		FilteringNormals.declareAttribute(normalAngleInvalidLimit);

		PreferenceComposite FilteringNeighBours = new PreferenceComposite(this, "Filtering neighbours");
		neighbourHeightInvalidLimit = new DoublePreferenceAttribute("1-Height coefficient for first invalidation",
				"Height coefficient for first invalidation", 5.0);
		FilteringNeighBours.declareAttribute(neighbourHeightInvalidLimit);
		neighbourSelectLimit = new IntegerPreferenceAttribute("2-Sounding selection parameter",
				"Sounding selection parameter", 10);
		FilteringNeighBours.declareAttribute(neighbourSelectLimit);
		neighnourDistanceInvalidLimit = new DoublePreferenceAttribute("3-Neighbouring limit", "Neighbouring limit(%)",
				70.0);
		FilteringNeighBours.declareAttribute(neighnourDistanceInvalidLimit);

		sliceSize = new IntegerPreferenceAttribute("5-Slice size by default", "Slice size by default", 250);
		this.declareAttribute(sliceSize);

		this.load();
	}

	public BooleanPreferenceAttribute getSaveOption() {
		return saveOption;
	}

	public DirectoryPreferenceAttribute getOutputDirectory() {
		return outputDirectory;
	}

	public StringPreferenceAttribute getFilePrefix() {
		return filePrefix;
	}

	public EnumPreferenceAttribute getFilteringMethod() {
		return filteringMethod;
	}

	public DoublePreferenceAttribute getHeightInvalidLimit() {
		return heightInvalidLimit;
	}

	public IntegerPreferenceAttribute getHeightIterationNumber() {
		return heightIterationNumber;
	}

	public DoublePreferenceAttribute getNormalHeightInvalidLimit() {
		return normalHeightInvalidLimit;
	}

	public IntegerPreferenceAttribute getNormalSelectLimit() {
		return normalSelectLimit;
	}

	public DoublePreferenceAttribute getNormalAngleInvalidLimit() {
		return normalAngleInvalidLimit;
	}

	public DoublePreferenceAttribute getNeighbourHeightInvalidLimit() {
		return neighbourHeightInvalidLimit;
	}

	public IntegerPreferenceAttribute getNeighbourSelectLimit() {
		return neighbourSelectLimit;
	}

	public DoublePreferenceAttribute getNeighnourDistanceInvalidLimit() {
		return neighnourDistanceInvalidLimit;
	}

	public IntegerPreferenceAttribute getSliceSize() {
		return sliceSize;
	}

}
