package fr.ifremer.globe.core.processes.filtri.model;

import java.io.IOException;
import java.util.ArrayList;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatusDetail;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Provides soundings from a {@link SounderDataContainer}
 */
public class SoundingsDataContainerProxy implements IFilTriSoundingsProxy, IDataContainerOwner {

	/** layers **/
	private DoubleLoadableLayer2D latitudeLayer;
	private DoubleLoadableLayer2D longitudeLayer;
	private DetectionStatusLayer validityLayer;
	protected DoubleLayer2D depthLayer;

	/** Properties **/
	protected final ISounderDataContainerToken sounderDataContainerToken;
	protected final SounderDataContainer dataContainer;
	private final Projection projection;
	private ArrayList<Pair<Integer, Integer>> indexes;
	private final int beamCount;

	/** Constructor **/
	public SoundingsDataContainerProxy(ISounderNcInfo info, Projection proj) throws GIOException {
		// Get data container
		this.sounderDataContainerToken = IDataContainerFactory.grab().book(info, this);
		this.dataContainer = sounderDataContainerToken.getDataContainer();

		this.projection = proj;
		this.beamCount = dataContainer.getInfo().getTotalRxBeamCount();
		initLayers();
	}

	/** Initialize layers **/
	protected void initLayers() throws GIOException {
		depthLayer = dataContainer.getCsLayers(CoordinateSystem.FCS).getProjectedZ();
		latitudeLayer = dataContainer.getLayer(BathymetryLayers.DETECTION_LATITUDE);
		longitudeLayer = dataContainer.getLayer(BathymetryLayers.DETECTION_LONGITUDE);
		validityLayer = dataContainer.getStatusLayer();
	}

	/** Indexes valid soundings **/
	public void setLimits(int firstSwath, int lastSwath) throws GIOException {
		indexes = new ArrayList<>();
		for (int iSwath = firstSwath; iSwath < lastSwath; iSwath++) {
			for (int iBeam = 0; iBeam < beamCount; iBeam++) {
				if (validityLayer.isValid(iSwath, iBeam))
					indexes.add(new Pair<>(iSwath, iBeam));
			}
		}
	}

	public void flush() throws GIOException {
		dataContainer.flush();
	}

	@Override
	public void close() throws IOException {
		sounderDataContainerToken.close();
	}

	@Override
	public double[] getLonLat(int index) throws GIOException {
		double lon = longitudeLayer.get(getSwathIndex(index), getBeamIndex(index));
		double lat = latitudeLayer.get(getSwathIndex(index), getBeamIndex(index));
		try {
			return projection.project(lon, lat);
		} catch (ProjectionException e) {
			throw new GIOException("Projection error", e);
		}
	}

	@Override
	public double getDepth(int index) throws GIOException {
		return depthLayer.get(getSwathIndex(index), getBeamIndex(index));
	}

	@Override
	public byte getValidity(int index) throws GIOException {
		return validityLayer.isValid(getSwathIndex(index), getBeamIndex(index)) ? BYTE_TRUE : BYTE_FALSE;
	}

	@Override
	public void setValid(int index, boolean validity) throws GIOException {
		if (!validity) {
			validityLayer.enableFlag(getSwathIndex(index), getBeamIndex(index), SonarDetectionStatus.REJECTED);
			validityLayer.setDetails(getSwathIndex(index), getBeamIndex(index), SonarDetectionStatusDetail.AUTO);
		} else {
			validityLayer.disableFlag(getSwathIndex(index), getBeamIndex(index), SonarDetectionStatus.REJECTED);
			validityLayer.setDetails(getSwathIndex(index), getBeamIndex(index), SonarDetectionStatusDetail.UNKNOWN);
		}
	}

	@Override
	public int getLastIndex() {
		return indexes.size() - 1;
	}

	@Override
	public int size() {
		return indexes.size();
	}

	// Private getters

	private int getSwathIndex(int index) {
		return indexes.get(index).getFirst();
	}

	private int getBeamIndex(int index) {
		return indexes.get(index).getSecond();
	}

}
