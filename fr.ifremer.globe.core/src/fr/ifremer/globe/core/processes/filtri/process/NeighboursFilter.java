package fr.ifremer.globe.core.processes.filtri.process;

import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.processes.filtri.model.IFilTriSoundingsProxy;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Point_dt;
import fr.ifremer.globe.utils.exception.GIOException;

public class NeighboursFilter extends AbstractFilter {

	static final double FILTER_FIRST_Z_COEFF = 0.499;
	static final double FILTER_SECOND_Z_COEFF = 0.501;

	double heightInvalidLimit;
	double selectLimit;
	double distanceInvalidLimit;

	public NeighboursFilter(IProgressMonitor monitor, double heightInvalidLimit, double selectLimit,
			double distanceInvalidLimit) {
		super(monitor);
		this.heightInvalidLimit = heightInvalidLimit;
		this.selectLimit = selectLimit;
		this.distanceInvalidLimit = distanceInvalidLimit;
	}

	@Override
	public void compute(IFilTriSoundingsProxy sounds) throws GIOException {
		this.filteredNbSounds = 0;
		// progress is divided in 5 steps
		this.taskWork = WORK_FILTER / 5;
		try {

			// Invalidate soundings by heights method
			this.monitor.subTask("First invalidation by heights : building triangulation ...");
			triangulate(sounds);
			if (this.monitor.isCanceled()) {
				return;
			}
			this.monitor.subTask("First invalidation by heights : filtering ...");
			invalidateByHeight(sounds);
			if (this.monitor.isCanceled()) {
				return;
			}
			// Invalidate soundings by neighbors method
			this.monitor.subTask("Invalidation by neighbours : building triangulation ...");
			updateTriangulation(sounds);
			if (this.monitor.isCanceled()) {
				return;
			}
			this.monitor.subTask("Invalidation by neighbours : selecting soundings ...");
			select(sounds);
			if (this.monitor.isCanceled()) {
				return;
			}
			this.monitor.subTask("Invalidation by neighbours : filtering ...");
			invalidateByDistance(sounds);
		} finally {
			clean();
		}
	}

	protected void invalidateByHeight(IFilTriSoundingsProxy sounds) throws GIOException {

		double zOffsetMean = zOffsetMean();
		double validLimit = this.heightInvalidLimit * zOffsetMean;
		int cpt = 0;

		// progress step
		int workNb = (int) Math.ceil(this.dt.size() / 100.0);

		Iterator<Point_dt> vIterator = this.dt.verticesIterator();

		while (vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();

			if (vertex.info() >= 0 && zOffset(vertex) > validLimit) {
				sounds.setValid(vertex.info(), false);
				this.filteredNbSounds++;
			}

			if (this.taskWork > 0 && cpt++ % workNb == 0) {
				this.monitor.worked(this.taskWork / 100);
			}
			if (this.monitor.isCanceled()) {
				break;
			}
		}

	}

	protected void select(IFilTriSoundingsProxy sounds) throws GIOException {

		double zOffsetMean = zOffsetMean();
		double validLimit = this.selectLimit * zOffsetMean;
		int cpt = 0;

		// progress step
		int workNb = (int) Math.ceil(this.dt.size() / 100.0);

		double heightPercentage = (this.selectLimit * zOffsetMean) / zMean();

		Iterator<Point_dt> vIterator = this.dt.verticesIterator();
		while (vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();
			if (vertex.info() >= 0) {
				double localCompValue = heightPercentage * zLocalNeighbourMean(vertex);

				if (zOffset(vertex) > validLimit || zLocalOffset(vertex) > localCompValue) {
					sounds.setValid(vertex.info(), false);
				}
			}

			if (this.taskWork > 0 && cpt++ % workNb == 0) {
				this.monitor.worked(this.taskWork / 100);
			}
			if (this.monitor.isCanceled()) {
				break;
			}
		}
	}

	protected void invalidateByDistance(IFilTriSoundingsProxy sounds) throws GIOException {
		int cpt = 0;

		// progress step
		int workNb = (int) Math.ceil(this.dt.size() / 100.0);

		Iterator<Point_dt> vIterator = this.dt.verticesIterator();
		while (vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();
			if (vertex.info() >= 0 && sounds.getValidity(vertex.info()) == IFilTriSoundingsProxy.BYTE_FALSE) {
				if (checkNeighbours(vertex)) {
					this.filteredNbSounds++;
				} else {
					sounds.setValid(vertex.info(), true);
				}
			}

			if (this.taskWork > 0 && cpt++ % workNb == 0) {
				this.monitor.worked(this.taskWork / 100);
			}
			if (this.monitor.isCanceled()) {
				break;
			}

		}
	}

	protected boolean checkNeighbours(Point_dt vertex) {
		boolean isBad = true;
		double[] minmax = new double[2];
		double[] diff = new double[2];
		double zMinMax;
		double compValue;

		// Recuperation de la profondeur mini et maxi des sondes voisines
		zLocalMinMax(vertex, minmax);
		zMinMax = Math.abs(minmax[0] + minmax[1]);

		// Recuperation des 2 differences de profondeur les plus petites avec
		// les sondes voisines
		zLocalTwoNearest(vertex, diff);

		// Premier Test : On teste la profondeur de la sonde douteuse pour
		// prot�ger les pentes fortes.
		if (((zMinMax * FILTER_FIRST_Z_COEFF) < Math.abs(vertex.z()))
				&& ((zMinMax * FILTER_SECOND_Z_COEFF) > Math.abs(vertex.z()))) {
			isBad = false;
		} else {
			// Deuxi�me test: on compare l'ecart le 2eme plus proche voisin a
			// l'ecart avec la moyenne locale ponderee
			compValue = Math.abs(vertex.z() - zLocalMean(vertex, true)) * this.distanceInvalidLimit;
			if (Math.abs(diff[1]) > compValue) {
				isBad = true;
			} else {
				isBad = false;
			}
		}

		return isBad;
	}

}
