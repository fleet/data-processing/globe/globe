package fr.ifremer.globe.core.processes.filtri.process;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.processes.filtri.model.IFilTriSoundingsProxy;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.DelaunayTriangulation;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Edge_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Point_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Vector_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.service.IAlgoPoolService;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.pool.IObjectPool;

public abstract class AbstractFilter {

	// progress tasks weightings
	public static final int WORK_FILTER = 10000;

	protected IProgressMonitor monitor = null;
	DelaunayTriangulation dt = null;
	int filteredNbSounds = 0;
	int taskWork = 0;
	String subTask = "";

	IObjectPool<Point_dt> pointPool = IAlgoPoolService.grab().getPointPool();
	IObjectPool<Vector_dt> vectorPool = IAlgoPoolService.grab().getVectorPool();
	IObjectPool<Edge_dt> edgePool = IAlgoPoolService.grab().getEdgePool();

	public AbstractFilter(IProgressMonitor monitor) {
		this.monitor = monitor;
	}

	/**
	 * Starts of filtering computations, do some test on sounds before compute
	 * @param sounds
	 * @throws GIOException
	 */
	public void apply(IFilTriSoundingsProxy sounds) throws GIOException {
		// Tests if it contains valid soundings to process,
		// if not, there's nothing to do
		if (!sounds.containsValids())
			return;

		compute(sounds);
	}

	/**
	 * Processing.
	 */
	public abstract void compute(IFilTriSoundingsProxy sounds) throws GIOException;

	/**
	 * Triangulation reinitialization.
	 */
	public void reset() {
		if (this.dt != null) {
			this.dt.free();
		}
		this.dt = new DelaunayTriangulation(this.monitor);
	}

	/**
	 * Triangulation of a group of soundings.
	 * 
	 * @param sounds List of soundings Soundings are added to current soudings list
	 */
	protected void triangulate(IFilTriSoundingsProxy sounds) throws GIOException {
		if (this.dt == null) {
			this.dt = new DelaunayTriangulation(this.monitor);
		}

		// progress step
		int workNb = (int) Math.ceil(sounds.size() / 100.0);

		// Initialize triangulation
		double[] minXY = new double[2];
		double[] maxXY = new double[2];

		// Creation of 2 triangles encompassing all sounds
		// to avoid creating new triangles exterior to an existing triangle : bug
		boolean boundsFound = getBounds(sounds, minXY, maxXY);
		if (!boundsFound) {
			throw new GIOException("Error: triangulation not possible, there is no valid sounding.");
		}

		insertPoint(minXY[0], minXY[1], 0.0, -1);
		insertPoint(minXY[0], maxXY[1], 0.0, -1);
		insertPoint(maxXY[0], maxXY[1], 0.0, -1);
		insertPoint(maxXY[0], minXY[1], 0.0, -1);

		int cpt = 0;
		for (int soundIndex = 0; soundIndex <= sounds.getLastIndex(); soundIndex++) {

			if (this.monitor.isCanceled())
				break;
			if (sounds.getValidity(soundIndex) == IFilTriSoundingsProxy.BYTE_TRUE) {
				double[] lonLat = sounds.getLonLat(soundIndex);
				insertPoint(lonLat[0], lonLat[1], sounds.getDepth(soundIndex), soundIndex);
			}
			if (this.monitor.isCanceled())
				return;

			if (this.taskWork > 0 && cpt++ % workNb == 0) {
				this.monitor.worked(this.taskWork / 100);
			}
		}
		if (this.monitor.isCanceled())
			return;
		this.dt.IndexData(20, 20);
		this.dt.initTriangles();
	}

	protected void insertPoint(double x, double y, double z, int soundIndex) {
		Point_dt point = this.pointPool.borrowObject();
		point.initialize(x, y, z, soundIndex);
		if (!this.dt.insertPoint(point)) {
			this.pointPool.returnObject(point);
		}
	}

	/**
	 * Update of triangulation.
	 * 
	 */
	protected void updateTriangulation(IFilTriSoundingsProxy sounds) throws GIOException {
		reset();
		triangulate(sounds);
	}

	protected double zLocalMean(Point_dt vertex, boolean included) {
		int cpt = 0;
		double sumCoef = 0.0;
		double sumZ = 0.0;
		double localMean = 0.0;
		List<Edge_dt> edges = this.dt.findIncidentEdges(vertex);
		for (Edge_dt edge : edges) {
			if (edge.source().info() >= 0) {
				double length = edge.length();
				sumCoef += 1.0 / length;
				sumZ += edge.source().z() / length;
				cpt++;
			}
			this.edgePool.returnObject(edge);
		}
		if (sumCoef != 0.0) {
			localMean = sumZ / sumCoef;
		}
		if (included) {
			localMean = (localMean * cpt + vertex.z()) / (cpt + 1);
		}

		return localMean;
	}

	protected double zOffset(Point_dt vertex) {
		double localMean = zLocalMean(vertex, true);
		return Math.abs(vertex.z() - localMean);
	}

	protected double zMean() {
		int cpt = 0;
		double summ = 0.0;
		Iterator<Point_dt> vIterator = this.dt.verticesIterator();
		while (vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();
			if (vertex.info() >= 0) {
				cpt++;
				summ += vertex.z();
			}
		}

		if (cpt == 0)
			return 0;
		else
			return summ / cpt;
	}

	protected double zOffsetMean() {
		int cpt = 0;
		double summ = 0.0;
		Iterator<Point_dt> vIterator = this.dt.verticesIterator();
		while (vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();
			if (vertex.info() >= 0) {
				cpt++;
				summ += zOffset(vertex);
				if (this.monitor.isCanceled())
					break;
			}
		}
		if (cpt == 0)
			return 0;
		else
			return summ / cpt;
	}

	protected double zLocalNeighbourMean(Point_dt vertex) {
		double summ = 0.0;
		int cpt = 0;
		List<Point_dt> neighbours = this.dt.getNeighbours(vertex);
		for (Point_dt neighbour : neighbours) {
			if (neighbour.info() >= 0) {
				cpt++;
				summ += zLocalMean(neighbour, false);
			}
		}
		if (cpt == 0)
			return 0;
		else
			return summ / cpt;
	}

	protected double zLocalOffset(Point_dt vertex) {
		double zMean = zLocalNeighbourMean(vertex);
		return Math.abs(vertex.z() - zMean);
	}

	protected void zLocalMinMax(Point_dt vertex, double[] minmax) {
		double zMin = vertex.z();
		double zMax = vertex.z();
		List<Point_dt> neighbours = this.dt.getNeighbours(vertex);
		for (Point_dt neighbour : neighbours) {
			if (neighbour.info() >= 0) {
				if (neighbour.z() < zMin) {
					zMin = neighbour.z();
				}
				if (neighbour.z() > zMax) {
					zMax = neighbour.z();
				}
			}
		}
		minmax[0] = zMin;
		minmax[1] = zMax;
	}

	protected void zLocalTwoNearest(Point_dt vertex, double[] diff) {
		double first = Double.MAX_VALUE;
		double second = Double.MAX_VALUE;
		List<Point_dt> neighbours = this.dt.getNeighbours(vertex);
		for (Point_dt neighbour : neighbours) {
			if (neighbour.info() >= 0) {
				double val = Math.abs(neighbour.z() - vertex.z());
				if (val < first) {
					second = first;
					first = val;
				} else if (val < second) {
					second = val;
				}
			}
		}
		if (neighbours.isEmpty()) {
			first = 0.0;
		}
		if (neighbours.size() < 2) {
			second = first;
		}
		diff[0] = first;
		diff[1] = second;
	}

	public int getNbFilteredSounds() {
		return this.filteredNbSounds;
	}

	/**
	 * @return true if bounds have been found
	 * @throws GIOException
	 */
	public boolean getBounds(IFilTriSoundingsProxy sounds, double[] minXY, double[] maxXY) throws GIOException {
		boolean first = true;
		for (int soundIndex = 0; soundIndex <= sounds.getLastIndex(); soundIndex++) {
			if (sounds.getValidity(soundIndex) == IFilTriSoundingsProxy.BYTE_TRUE) {
				double[] lonLat = sounds.getLonLat(soundIndex);
				if (first) {
					minXY[0] = lonLat[0];
					minXY[1] = lonLat[1];
					maxXY[0] = lonLat[0];
					maxXY[1] = lonLat[1];
					first = false;
				} else {
					if (lonLat[0] < minXY[0])
						minXY[0] = lonLat[0];
					if (lonLat[1] < minXY[1])
						minXY[1] = lonLat[1];
					if (lonLat[0] > maxXY[0])
						maxXY[0] = lonLat[0];
					if (lonLat[1] > maxXY[1])
						maxXY[1] = lonLat[1];
				}
			}
		}
		return !first;
	}

	public void clean() {
		this.dt.free();
		this.dt = new DelaunayTriangulation(this.monitor);
	}

}
