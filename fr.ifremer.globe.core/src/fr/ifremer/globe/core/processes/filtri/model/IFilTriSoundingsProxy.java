package fr.ifremer.globe.core.processes.filtri.model;

import java.io.Closeable;

import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Set of beams on which a filtering whill be applying.
 */
public interface IFilTriSoundingsProxy extends Closeable {
	
	/** true in byte. */
	byte BYTE_TRUE = NumberUtils.bool2byte(true);
	/** false in byte. */
	byte BYTE_FALSE = NumberUtils.bool2byte(false);

	/**
	 * This methods is used to filter swaths with enough sounds for triangular filtering
	 * @return true if contains at least 2 valid sounds.
	 */
	default boolean containsValids() throws GIOException {
		int validSounds = 0;
		for (int soundIndex = 0; soundIndex <= getLastIndex(); soundIndex++) {
			if (getValidity(soundIndex) == IFilTriSoundingsProxy.BYTE_TRUE)
				validSounds++;
		}
		return validSounds > 1;
	}
	
	/**
	 * @return longitude and latitude for the given sounding index
	 */
	public double[] getLonLat(int index) throws GIOException;

	/**
	 * @return the value of a Z attribute
	 */
	public double getDepth(int index)throws GIOException;
	/**
	 * @return the value of a validity attribute
	 */
	public byte getValidity(int index)throws GIOException;

	/**
	 * Set the value of a validity attribute
	 */
	public void setValid(int index, boolean validity) throws GIOException;


	/**
	 * @return the {@link #maxIndex}
	 */
	public int getLastIndex();

	/**
	 * @return the number of beam contained.
	 */
	public int size();
}
