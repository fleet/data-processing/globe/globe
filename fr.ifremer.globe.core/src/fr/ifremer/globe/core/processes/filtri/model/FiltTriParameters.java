package fr.ifremer.globe.core.processes.filtri.model;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.processes.ProcessParameters;

/**
 * Filtering by triangulation process.
 * 
 */
public class FiltTriParameters extends ProcessParameters {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parameter file name.
	 */
	public static final String FILTTRI_PARAMETERS_FILE_NAME = "FiltTri.properties";

	/**
	 * New files creation option.
	 */
	public static final String PROPERTY_NEW_FILE_OPTION = "newFileOption";

	/**
	 * new files directory.
	 */
	public static final String PROPERTY_NEW_FILES_DIRECTORY = "newFilesDirectory";
	/**
	 * New file names prefix.
	 */
	public static final String PROPERTY_NEW_FILES_PREFIX = "newFilesPrefix";

	/**
	 * Filtering method.
	 */
	public static final String PROPERTY_FILTERING_OPTION = "filteringOption";

	/**
	 * Height limit for height method.
	 */
	public static final String PROPERTY_HEIGHT_INVALID_LIMIT = "heightInvalidLimit";

	/**
	 * Iteration number for height method.
	 */
	public static final String PROPERTY_HEIGHT_ITERATION_NUMBER = "heightIterationNumber";

	/**
	 * First invalidation parameter for normals method.
	 */
	public static final String PROPERTY_NORMAL_HEIGHT_INVALID_LIMIT = "normalHeightInvalidLimit";

	/**
	 * Selection parameter for normals method.
	 */
	public static final String PROPERTY_NORMAL_SELECT_LIMIT = "normalSelectLimit";

	/**
	 * Limit angle for normals method.
	 */
	public static final String PROPERTY_NORMAL_ANGLE_INVALID_LIMIT = "angleInvalidLimit";

	/**
	 * First invalidation parameter for neighbours method.
	 */
	public static final String PROPERTY_NEIGHBOUR_HEIGHT_INVALID_LIMIT = "neighbourHeightInvalidLimit";

	/**
	 * Selection parameter for neighbours method.
	 */
	public static final String PROPERTY_NEIGHBOUR_SELECT_LIMIT = "neighboursSelectLimit";

	/**
	 * Limit distance for neighbours method.
	 */
	public static final String PROPERTY_NEIGHBOUR_DISTANCE_INVALID_LIMIT = "distanceInvalidLimit";

	/**
	 * Determine the size used to slice input file
	 */
	public static final String PROPERTY_SLICE_SIZE = "sliceSize";
	/**
	 * parameter value for invalidation method. 0 = heights 1 = normals 2 =
	 * neighbours
	 */

	public static final int FILTERING_OPTION_HEIGHT = 0;
	public static final int FILTERING_OPTION_NORMAL = 1;
	public static final int FILTERING_OPTION_NEIGHBOUR = 2;

	public ProjectionSettings projectionSettings;

	private GeoBox geoBox = new GeoBox();
	/*	*//**
			 * projection parameters
			 *
			 *//*
				 * public static final String PROPERTY_PROJECTION_PARAMETERS =
				 * "projectionParameters";
				 */

	/**
	 * Default constructor.
	 */
	public FiltTriParameters() {
		super();
	}

	/**
	 * Read parameters values.
	 * 
	 * @return
	 */
	@Override
	protected void initialize() {
		// read user parameter values
		/*
		 * String paramFileName = getUserFilePath(FILTTRI_PARAMETERS_FILE_NAME); try {
		 * File f = new File(paramFileName); if (!f.exists()) { paramFileName =
		 * getDefaultParameterFilePath("fr.ifremer.globe.process.filttri"); } load(new
		 * FileInputStream(paramFileName)); } catch (IOException e) {
		 * e.printStackTrace(); }
		 */
	}

	public String getTitle() {
		String title = "FiltTri ";

		if (getIntProperty(PROPERTY_FILTERING_OPTION) == FILTERING_OPTION_HEIGHT) {
			title += "by heights method";
		} else if (getIntProperty(PROPERTY_FILTERING_OPTION) == FILTERING_OPTION_NORMAL) {
			title += "by normals method";
		} else if (getIntProperty(PROPERTY_FILTERING_OPTION) == FILTERING_OPTION_NEIGHBOUR) {
			title += "by neighbours method";
		}

		return title;
	}

	/**
	 * Write parameters values.
	 * 
	 * @return
	 */
	/*
	 * public void save() { String paramFileName =
	 * getUserFilePath(FILTTRI_PARAMETERS_FILE_NAME); File f = new
	 * File(paramFileName); try { if (!f.exists()) { f.getParentFile().mkdirs(); }
	 * store(new FileOutputStream(f), null); } catch (IOException e) {
	 * e.printStackTrace(); } }
	 */

	/**
	 * Restore default parameters values.
	 * 
	 * @return
	 */
	/*
	 * public void restoreDefault() { String paramFileName =
	 * getDefaultParameterFilePath("fr.ifremer.globe.process.filttri"); try {
	 * load(new FileInputStream(paramFileName)); } catch (IOException e) {
	 * e.printStackTrace(); } }
	 */

	/**
	 * Set the projection type
	 * 
	 * @param ProjectionSettings paramProjectionSettings
	 */

	public void setProjection(ProjectionSettings paramProjectionSettings) {
		projectionSettings = paramProjectionSettings;
	}

	/**
	 * Get the projection type
	 * 
	 * @return PjectionType
	 */
	public ProjectionSettings getProjection() {
		return projectionSettings;

	}

	public void setGeobox(GeoBox geoBox) {
		this.geoBox = geoBox;

	}

	public GeoBox getGeoBox() {
		return geoBox;
	}

}
