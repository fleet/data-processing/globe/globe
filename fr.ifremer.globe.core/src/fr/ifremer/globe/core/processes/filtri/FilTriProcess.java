package fr.ifremer.globe.core.processes.filtri;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;
import fr.ifremer.globe.core.processes.filtri.model.SoundingsDataContainerProxy;
import fr.ifremer.globe.core.processes.filtri.process.AbstractFilter;
import fr.ifremer.globe.core.processes.filtri.process.FilterFactory;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Process for triangular filtering
 */
public class FilTriProcess {

	public class FilTriProcessResult {
		private int totalNbSounds;
		private int filteredNbSounds;

		FilTriProcessResult(int totalNbSounds, int filteredNbSounds) {
			this.totalNbSounds = totalNbSounds;
			this.filteredNbSounds = filteredNbSounds;
		}

		public int getTotalNbSounds() {
			return totalNbSounds;
		}

		public int getFilteredNbSounds() {
			return filteredNbSounds;
		}
	}

	public SoundingsDataContainerProxy buildSoundings(ISounderNcInfo info, Projection proj) throws GIOException {
		return new SoundingsDataContainerProxy(info, proj);
	}

	public FilTriProcessResult process(File inputFile, FiltTriParameters params, Optional<Logger> logger,
			IProgressMonitor monitor) throws GIOException, IOException {
		return process(inputFile, inputFile, params, logger, monitor);
	}

	public FilTriProcessResult process(File inputFile, File outputFile, FiltTriParameters params,
			Optional<Logger> logger, IProgressMonitor monitor) throws GIOException, IOException {
		FilterFactory filterFactory = new FilterFactory();

		Path wrkPath = Paths.get(outputFile.getAbsolutePath());

		if (!inputFile.getAbsoluteFile().equals(outputFile.getAbsoluteFile())) {
			Files.deleteIfExists(wrkPath);
			FileUtils.copyFile(inputFile, outputFile);
		}

		// Get parameters
		AbstractFilter filter = filterFactory.getFilter(monitor, params);
		Projection proj = new Projection(params.getProjection());
		int sliceSize = params.getIntProperty(FiltTriParameters.PROPERTY_SLICE_SIZE);
		int totalNbSounds = 0;
		int filteredNbSounds = 0;
		ISounderNcInfo info = IDataContainerInfoService.grab().getSounderNcInfo(outputFile.getAbsolutePath())
				.orElse(null);
		if (info != null) {
			final String inputFileName = inputFile.getAbsolutePath();
			final String outputFileName = outputFile.getAbsolutePath();
			logger.ifPresent(l -> l.info("Input file : {}", inputFileName));
			logger.ifPresent(l -> l.info("Output file : {}", outputFileName));

			// Apply filter for each slices
			try (SoundingsDataContainerProxy soundings = buildSoundings(info, proj)) {
				int totalSwath = info.getCycleCount();
				for (int start = 0; start < info.getCycleCount() && !monitor.isCanceled(); start += sliceSize) {
					int end = Math.min(start + sliceSize, totalSwath);
					final int start2 = start;
					logger.ifPresent(l -> l.info("Processing swaths: {} to {} ({})", start2, end, totalSwath));

					soundings.setLimits(start, end);
					filter.apply(soundings);

					totalNbSounds += soundings.size();
					filteredNbSounds += filter.getNbFilteredSounds();
					if (monitor.isCanceled())
						return new FilTriProcessResult(-1, -1);
				}
				soundings.flush();
			}

			final int f1 = filteredNbSounds;
			final int t1 = totalNbSounds;
			logger.ifPresent(l -> l.info("Number of invalidated soundings : {} on {} ", f1, t1));

			// writes data
			Optional<ProcessAssessor> processAssessor = info.getProcessAssessor();
			if (processAssessor.isPresent()) {
				processAssessor.get().heedAutomaticCleaning();
			}
		}
		return new FilTriProcessResult(totalNbSounds, filteredNbSounds);
	}
}
