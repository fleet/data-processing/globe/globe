﻿package fr.ifremer.globe.core.processes.filtri.process;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Factory of reusable Filter.
 */
public class FilterFactory {

	/**
	 * Default constructor.
	 */
	public FilterFactory() {
	}

	/**
	 * Process.
	 */
	public AbstractFilter getFilter(IProgressMonitor monitor, FiltTriParameters filtTriParameters) throws GIOException {
		AbstractFilter result = null;
		if (filtTriParameters.getIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION) == FiltTriParameters.FILTERING_OPTION_HEIGHT) {
			double invalidLimit = filtTriParameters.getDoubleProperty(FiltTriParameters.PROPERTY_HEIGHT_INVALID_LIMIT);
			int iterationNumber = filtTriParameters.getIntProperty(FiltTriParameters.PROPERTY_HEIGHT_ITERATION_NUMBER);
			result = new HeightsFilter(monitor, invalidLimit, iterationNumber);
		} else if (filtTriParameters.getIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION) == FiltTriParameters.FILTERING_OPTION_NORMAL) {
			double heightInvalidLimit = filtTriParameters.getDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_HEIGHT_INVALID_LIMIT);
			double selectLimit = filtTriParameters.getDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_SELECT_LIMIT);
			int iterationNumber = NormalsFilter.NORMAL_FILTER_ITERATION_NUMBER;
			double invalidationNumber = NormalsFilter.NORMAL_FILTER_INVALIDATION_NUMBER;
			double angleInvalidLimit = Math.toRadians(filtTriParameters.getDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_ANGLE_INVALID_LIMIT));
			result = new NormalsFilter(monitor, heightInvalidLimit, selectLimit, iterationNumber, invalidationNumber, angleInvalidLimit);
		} else if (filtTriParameters.getIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION) == FiltTriParameters.FILTERING_OPTION_NEIGHBOUR) {
			double heightInvalidLimit = filtTriParameters.getDoubleProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_HEIGHT_INVALID_LIMIT);
			double selectLimit = filtTriParameters.getDoubleProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_SELECT_LIMIT);
			double distanceInvalidLimit = filtTriParameters.getDoubleProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_DISTANCE_INVALID_LIMIT) / 100.0;
			result = new NeighboursFilter(monitor, heightInvalidLimit, selectLimit, distanceInvalidLimit);
		}
		return result;
	}
}
