package fr.ifremer.globe.core.processes.filtri.process;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.processes.filtri.model.IFilTriSoundingsProxy;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Point_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Vector_dt;
import fr.ifremer.globe.utils.exception.GIOException;

public class NormalsFilter extends AbstractFilter {
	public static final int NORMAL_FILTER_ITERATION_NUMBER = 4;
	public static final double NORMAL_FILTER_INVALIDATION_NUMBER = 0.67;
	
	int iterationNumber;
	double heightInvalidLimit;
	double selectLimit;
	double invalidationNumber;
	double angleInvalidLimit;

	public NormalsFilter(IProgressMonitor monitor, double heightInvalidLimit, double selectLimit, int iterationNumber, double invalidationNumber, double angleInvalidLimit) {
		super(monitor);
		this.heightInvalidLimit = heightInvalidLimit;
		this.selectLimit = selectLimit;
		this.iterationNumber = iterationNumber;
		this.invalidationNumber = invalidationNumber;
		this.angleInvalidLimit = angleInvalidLimit;
	}

	@Override
	public void compute(IFilTriSoundingsProxy sounds) throws GIOException {
		this.filteredNbSounds = 0;
		this.taskWork = WORK_FILTER / (this.iterationNumber * 3 + 2);
		try {

			// Invalidate soundings by heights method
			this.monitor.subTask("First invalidation by heights : building triangulation ...");
			triangulate(sounds);
			this.monitor.subTask("First invalidation by heights : filtering ...");
			this.filteredNbSounds += invalidateByHeight(sounds, this.heightInvalidLimit);

			// Invalidate soundings by normals method
			for (int iter = 1; iter <= this.iterationNumber; iter++) {
				if (this.monitor.isCanceled()) {
					break;
				}
				this.subTask = "Iteration " + iter + "/" + this.iterationNumber;
				this.monitor.subTask(this.subTask + " building triangulation ...");
				updateTriangulation(sounds);
				if (this.monitor.isCanceled()) {
					break;
				}
				this.monitor.subTask(this.subTask + " selecting soundings ...");
				invalidateByHeight(sounds, this.selectLimit);
				if (this.monitor.isCanceled()) {
					break;
				}
				this.monitor.subTask(this.subTask + " filtering ...");
				invalidateByNormals(sounds);
			}
		} finally {
			clean();
		}

	}

	protected int invalidateByHeight(IFilTriSoundingsProxy sounds, double heightInvalidLimit) throws GIOException {
		int filteredNbSounds = 0;

		double zOffsetMean = zOffsetMean();
		double validLimit = heightInvalidLimit * zOffsetMean;
		int cpt = 0;

		double heightPercentage = (heightInvalidLimit * zOffsetMean) / zMean();

		// progress step
		int workNb = (int) Math.ceil(this.dt.size() / 100.0);

		Iterator<Point_dt> vIterator = this.dt.verticesIterator();

		while (vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();
			if (vertex.info() >= 0) {
				double localCompValue = heightPercentage * zLocalNeighbourMean(vertex);

				if (zOffset(vertex) > validLimit || zLocalOffset(vertex) > localCompValue) {
					sounds.setValid(vertex.info(), false);
					filteredNbSounds++;
				}
			}

			if (this.taskWork > 0 && cpt++ % workNb == 0) {
				this.monitor.worked(this.taskWork / 100);
			}
			if(this.monitor.isCanceled())
			{
				break;
			}
		}

		return filteredNbSounds;
	}

	protected void invalidateByNormals(IFilTriSoundingsProxy sounds) throws GIOException {
		int cpt = 0;

		// progress step
		int workNb = (int) Math.ceil(this.dt.size() / 100.0);

		Iterator<Point_dt> vIterator = this.dt.verticesIterator();
		while (!this.monitor.isCanceled() && vIterator.hasNext()) {
			Point_dt vertex = vIterator.next();
			if (vertex.info() >= 0 && sounds.getValidity(vertex.info()) == IFilTriSoundingsProxy.BYTE_FALSE) {
				if (checkFacesNormals(vertex)) {
					this.filteredNbSounds++;
				} else {
					sounds.setValid(vertex.info(), true);
				}
			}

			if (this.taskWork > 0 && cpt++ % workNb == 0) {
				this.monitor.worked(this.taskWork / 100);
			}
		}

	}

	protected boolean checkFacesNormals(Point_dt vertex) {

		List<Vector_dt> normals = this.dt.getNormals(vertex);
		for (Vector_dt normal : normals) {
			if (normal.z() < 0) {
				normal.opposite();
			}
		}
		boolean result = checkNormalsAngles(normals);
		this.vectorPool.returnObject(normals);
		return result;
	}

	protected boolean checkNormalsAngles(List<Vector_dt> normals) {

		int nb = 0;
		int nbInvalid = 0;
		boolean isBad = true;

		for (int idx1 = 0; idx1 < normals.size(); idx1++) {
			Vector_dt v = normals.get(idx1);
			for (int idx2 = idx1 + 1; idx2 < normals.size(); idx2++) {
				double angle = v.getAngle(normals.get(idx2));
				nb++;
				if (angle > this.angleInvalidLimit) {
					nbInvalid++;
				}
			}
		}

		double compValue = this.invalidationNumber * nb;
		if (compValue < nbInvalid) {
			isBad = true;
		} else {
			isBad = false;
		}

		return isBad;
	}

}
