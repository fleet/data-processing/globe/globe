/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.processes.detectionfilter.parameters;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.math.DoubleRange;
import org.slf4j.Logger;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters.Comparator;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Transform a list of SingleFilterParameters to a Map of PredefinedLayers/Range
 */
public class FilterParameterChecker {

	/**
	 * Constructor
	 */
	private FilterParameterChecker() {
	}

	/**
	 * Select all beams corresponding to the criterias.
	 *
	 * @return Error message or "" on success
	 */
	public static String checkSelectParameters(List<SingleFilterParameters> criterias, boolean allCriterias,
			IAttributeProvider attributeProvider, Map<PredefinedLayers<?>, Set<DoubleRange>> resultingCriterias,
			Logger logger) {

		String error = "";
		for (SingleFilterParameters criteria : criterias) {
			PredefinedLayers<?> attribute = criteria.getSelectedAttributes();

			// Check range
			if (criteria.getComparator() == Comparator.between && criteria.getValueA() > criteria.getValueB()) {
				error = "Invalid range for attribute " + attribute.getDisplayedName();
				break;
			}

			Set<DoubleRange> currentRange = Optional.ofNullable(resultingCriterias.get(attribute))
					.orElse(new HashSet<>());
			Class<?> type = attributeProvider.retrieveFilterAttributeType(attribute);
			if (type == Boolean.class) {
				double expectedValue = criteria.getValueA();
				// Two criteria with the same value
				if (allCriterias && !currentRange.isEmpty()
						&& currentRange.stream().noneMatch(range -> range.containsDouble(expectedValue))) {
					error = "Attribute " + attribute.getDisplayedName() + " has contradictory criterias";
					break;
				}
				currentRange.add(new DoubleRange(expectedValue));
				resultingCriterias.put(attribute, currentRange);
			} else {
				// Range of value
				DoubleRange newRange = createRange(criteria, type);
				// Two criteria with different ranges ?
				if (allCriterias && !currentRange.isEmpty()) {
					DoubleRange uniqueRange = currentRange.stream().findFirst().get();
					if (uniqueRange.overlapsRange(newRange)) {
						// Intersection
						currentRange.clear();
						currentRange.add(new DoubleRange(
								Math.max(uniqueRange.getMinimumDouble(), newRange.getMinimumDouble()),
								Math.min(uniqueRange.getMaximumDouble(), newRange.getMaximumDouble())));
						resultingCriterias.put(attribute, currentRange);
					} else {
						error = "Attribute " + attribute.getDisplayedName() + " has contradictory criterias";
						break;
					}
				} else {
					currentRange.add(newRange);
					resultingCriterias.put(attribute, currentRange);
				}
			}
		}

		if (!error.isEmpty()) {
			logger.warn("Bad parameter for filtering process : " + error);
			resultingCriterias.clear();
		}

		for (Map.Entry<PredefinedLayers<?>, Set<DoubleRange>> entry : resultingCriterias.entrySet()) {
			for (DoubleRange range : entry.getValue()) {
				logger.info("Filtering beams on attribute " + entry.getKey().getDisplayedName()
						+ " and range of values [" + range.getMinimumDouble() + "," + range.getMaximumDouble() + "]");
			}
		}

		return error;
	}

	/**
	 * Build a range reflecting the criteria.
	 */
	protected static DoubleRange createRange(SingleFilterParameters criteria, Class<?> type) {
		switch (criteria.getComparator()) {
		case between:
			return new DoubleRange(criteria.getValueA(), criteria.getValueB());
		case equal:
			return new DoubleRange(criteria.getValueA(), criteria.getValueA());
		case less:
			double min = Double.NEGATIVE_INFINITY;
			if (type == Boolean.class) {
				min = NumberUtils.bool2byte(false);
			} else if (type == Byte.class) {
				min = Byte.MIN_VALUE;
			} else if (type == Integer.class) {
				min = Integer.MIN_VALUE;
			} else if (type == Long.class) {
				min = Long.MIN_VALUE;
			} else if (type == Short.class) {
				min = Short.MIN_VALUE;
			}
			return new DoubleRange(min, criteria.getValueA());
		case more:
			double max = Double.POSITIVE_INFINITY;
			if (type == Boolean.class) {
				max = NumberUtils.bool2byte(true);
			} else if (type == Byte.class) {
				max = Byte.MAX_VALUE;
			} else if (type == Integer.class) {
				max = Integer.MAX_VALUE;
			} else if (type == Long.class) {
				max = Long.MAX_VALUE;
			} else if (type == Short.class) {
				max = Short.MAX_VALUE;
			}
			return new DoubleRange(criteria.getValueA(), max);
		}
		return null;
	}

}
