/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.processes.detectionfilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.OptionalLayersAccessor;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionBeamPointingAnglesLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.FilterParameterChecker;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatusDetail;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiIntFunction;

/**
 * Apply the filtering operation on each opened SounderDataContainer
 */
public class FilteringProcess implements IProcessFunction {

	protected List<ISounderNcInfo> sounderInfos;
	protected IAttributeProvider attributeProvider;
	protected List<SingleFilterParameters> criterias;
	protected DataParameters dataToSet;

	/**
	 * Constructor
	 */
	public FilteringProcess(IAttributeProvider attributeProvider, List<SingleFilterParameters> criterias,
			DataParameters dataToSet) {
		this.attributeProvider = attributeProvider;
		this.criterias = criterias;
		this.dataToSet = dataToSet;
	}

	/**
	 * Sets input files
	 */
	public void setInputFiles(List<ISounderNcInfo> sounderInfos) {
		this.sounderInfos = sounderInfos;
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException, OperationCanceledException {
		var subMonitor = SubMonitor.convert(monitor, sounderInfos.size());
		for (ISounderNcInfo sounderInfo : sounderInfos) {
			processFile(sounderInfo, subMonitor.split(1), logger);
		}
		return Status.OK_STATUS;
	}

	/**
	 * Process filtering for a file
	 *
	 * @param fileinfo
	 * @param monitor
	 * @param logger
	 * @throws GIOException
	 */
	public void processFile(ISounderNcInfo sounderInfo, IProgressMonitor monitor, Logger logger) throws GIOException {
		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();
		IDataContainerOwner owner = IDataContainerOwner.generate("FilteringProcessContainerOwner", false);
		try (ISounderDataContainerToken token = dataContainerFactory.book(sounderInfo, owner)) {
			accept(criterias, dataToSet, token.getDataContainer(), monitor, logger);
		}

		Optional<ProcessAssessor> processAssessor = sounderInfo.getProcessAssessor();
		if (processAssessor.isPresent()) {
			processAssessor.get().heedManualCleaning();
		}

		logger.info("Update statistics of {}", sounderInfo.getFilename());
		IFileService.grab().updateStatistics(sounderInfo);
	}

	/** {@inheritDoc} */
	protected void accept(List<SingleFilterParameters> criterias, DataParameters dataToSet,
			SounderDataContainer sounderDataContainer, IProgressMonitor monitor, Logger logger) throws GIOException {
		// monitor not used
		int beamCount = 0;

		var att_ranges = new HashMap<PredefinedLayers<?>, Set<DoubleRange>>();
		FilterParameterChecker.checkSelectParameters(criterias, dataToSet.isCheckAllCriterias(), attributeProvider,
				att_ranges, logger);
		if (!att_ranges.isEmpty()) {
			var layer_ranges = new HashMap<AbstractBaseLayer<?>, Set<DoubleRange>>();

			// Compute ping range
			Set<IntRange> pingRanges = new HashSet<>();
			Set<DoubleRange> doublePingRanges = att_ranges.remove(Attributes.PING_INDEX);
			if (doublePingRanges != null) {
				for (DoubleRange doublePingRange : doublePingRanges) {
					pingRanges.add(new IntRange(//
							Math.max(doublePingRange.getMinimumInteger(), 0), //
							Math.min(sounderDataContainer.getInfo().getCycleCount(),
									doublePingRange.getMaximumInteger())));
				}
			}

			// Compute beam range
			Set<IntRange> beamRanges = new HashSet<>();
			Set<DoubleRange> doubleBeamRanges = att_ranges.remove(Attributes.BEAM_INDEX);
			if (doubleBeamRanges != null) {
				for (DoubleRange doubleBeamRange : doubleBeamRanges) {
					beamRanges.add(new IntRange(//
							Math.max(doubleBeamRange.getMinimumInteger(), 0), //
							Math.min(sounderDataContainer.getInfo().getTotalRxBeamCount(),
									doubleBeamRange.getMaximumInteger())));
				}
			}

			// process across_angle
			Set<DoubleRange> doubleAcrossAngleRanges = att_ranges.remove(Attributes.BEAM_POINTING_ACROSS_ANGLE);
			if (doubleAcrossAngleRanges != null) {
				// Check if optional across_angle layer exists
				Optional<PredefinedLayers<DoubleLoadableLayer2D>> acrossAngleLayer = sounderDataContainer.getInfo()
						.getOptionalLayersAccessor().map(OptionalLayersAccessor::getAcrossAngleLayer)
						.orElse(Optional.empty());
				if (acrossAngleLayer.isEmpty()) {
					// load and map ACROSS_ANGLE Layer
					DetectionBeamPointingAnglesLayers beamAngles = new DetectionBeamPointingAnglesLayers(
							sounderDataContainer);
					Optional<FloatLayer2D> acrossAnglesLayer = beamAngles.getAcrossAngleScsLayer();
					if (acrossAnglesLayer.isPresent())
						layer_ranges.put(acrossAnglesLayer.get(), doubleAcrossAngleRanges);
				} else {
					layer_ranges.put(sounderDataContainer.getLayer(acrossAngleLayer.get()), doubleAcrossAngleRanges);
				}
			}
			for (var entry : att_ranges.entrySet()) {
				layer_ranges.put((AbstractBaseLayer<?>) sounderDataContainer.getLayer(entry.getKey()),
						entry.getValue());
			}
			// Process validity
			if (dataToSet.getSelectedAttribute() == Attributes.VALIDITY) {
				if (dataToSet.getValue() == Attributes.VALIDITY_VALID) {
					beamCount = validateBeams(sounderDataContainer, layer_ranges, dataToSet.isCheckAllCriterias(),
							pingRanges, beamRanges, logger);
				} else {
					beamCount = invalidateBeams(sounderDataContainer, layer_ranges, dataToSet.isCheckAllCriterias(),
							pingRanges, beamRanges, logger);
				}
			}
			logger.info("{} beams processed", beamCount);

			if (beamCount > 0) {
				logger.info("Saving file...");
				sounderDataContainer.flush();
			}
		}
	}

	/** Perform the validation of the beams according the criterias */
	protected int validateBeams(SounderDataContainer sounderDataContainer,
			Map<AbstractBaseLayer<?>, Set<DoubleRange>> ranges, boolean checkAllCriterias, Set<IntRange> pingRanges,
			Set<IntRange> beamRanges, Logger logger) throws GIOException {
		DetectionStatusLayer detectionStatusLayer = sounderDataContainer.getStatusLayer();
		return applyFunction(sounderDataContainer, ranges, checkAllCriterias, pingRanges, beamRanges,
				(swath, beam) -> validateBeams(detectionStatusLayer, swath, beam), logger);
	}

	/** Perform the validation of one beam */
	protected GIOException validateBeams(DetectionStatusLayer detectionStatusLayer, int swath, int beam) {
		try {
			detectionStatusLayer.disableFlag(swath, beam, SonarDetectionStatus.REJECTED);
			detectionStatusLayer.setDetails(swath, beam, SonarDetectionStatusDetail.UNKNOWN);
		} catch (GIOException e) {
			return e;
		} catch (Exception e) {
			return GIOException.wrap("Error on beam ivalidation", e);
		}
		return null;
	}

	/** Perform the invalidation of the beams according the criterias */
	protected int invalidateBeams(SounderDataContainer sounderDataContainer,
			Map<AbstractBaseLayer<?>, Set<DoubleRange>> ranges, boolean checkAllCriterias, Set<IntRange> pingRanges,
			Set<IntRange> beamRanges, Logger logger) throws GIOException {
		DetectionStatusLayer detectionStatusLayer = sounderDataContainer.getStatusLayer();
		return applyFunction(sounderDataContainer, ranges, checkAllCriterias, pingRanges, beamRanges,
				(swath, beam) -> invalidateBeams(detectionStatusLayer, swath, beam), logger);
	}

	/** Perform the invalidation of one beam */
	protected GIOException invalidateBeams(DetectionStatusLayer detectionStatusLayer, int swath, int beam) {
		try {
			detectionStatusLayer.enableFlag(swath, beam, SonarDetectionStatus.REJECTED);
			detectionStatusLayer.setDetails(swath, beam, SonarDetectionStatusDetail.MANUAL);
		} catch (GIOException e) {
			return e;
		} catch (Exception e) {
			return GIOException.wrap("Error on beam invalidation", e);
		}
		return null;
	}

	/** Run over all beams and apply the function on them */
	protected int applyFunction(SounderDataContainer sounderDataContainer,
			Map<AbstractBaseLayer<?>, Set<DoubleRange>> ranges, boolean checkAllCriterias, Set<IntRange> swathRanges,
			Set<IntRange> beamRanges, BiIntFunction<GIOException> function, Logger logger) throws GIOException {
		int result = 0;

		// List filtering layers
		var layers = new ArrayList<Pair<AbstractBaseLayer<?>, Set<DoubleRange>>>();
		for (Entry<AbstractBaseLayer<?>, Set<DoubleRange>> entry : ranges.entrySet()) {
			layers.add(new Pair<>(entry.getKey(), entry.getValue()));
		}

		int lastPercent = 0;
		int swathCount = sounderDataContainer.getInfo().getCycleCount();
		int beamCount = sounderDataContainer.getInfo().getTotalRxBeamCount();
		LayerValueGetter getter = new LayerValueGetter();
		for (int swath = 0; swath < swathCount; swath++) {

			// Progress...
			int percent = swath * 100 / swathCount;
			if (percent >= lastPercent + 10) {
				logger.info("{} %", percent);
				lastPercent = percent;
			}

			// Don’t process the swath when out of range
			getter.setSwath(swath);
			for (int beam = 0; beam < beamCount; beam++) {
				boolean correct = checkAllCriterias;
				if (checkAllCriterias) {
					for (IntRange swathRange : swathRanges) {
						correct &= swathRange.containsInteger(swath);
					}
					for (IntRange beamRange : beamRanges) {
						correct &= beamRange.containsInteger(beam);
					}
					if (!correct)
						continue;
				} else {
					for (IntRange swathRange : swathRanges) {
						correct |= swathRange.containsInteger(swath);
					}
					for (IntRange beamRange : beamRanges) {
						correct |= beamRange.containsInteger(beam);
					}
				}

				getter.setBeam(beam);

				// Verify values in each layers according the criteria
				for (int i = 0; i < layers.size(); i++) {
					// Check if one criteria is enough
					if ((correct && !checkAllCriterias) || (!correct && checkAllCriterias)) {
						break;
					}

					var layer = layers.get(i).getFirst();
					layer.process(getter);
					var value = getter.getValue();

					for (DoubleRange range : layers.get(i).getSecond()) {
						if (checkAllCriterias) {
							correct &= (!Double.isNaN(value) && range.containsDouble(value));
						} else {
							correct |= (!Double.isNaN(value) && range.containsDouble(value));
						}
					}
				}

				// correct => all criterias has been verified
				if (correct) {
					GIOException exception = function.apply(swath, beam);
					if (exception != null) {
						throw exception;
					}
					result++;
				}
			}
		}

		return result;
	}

}
