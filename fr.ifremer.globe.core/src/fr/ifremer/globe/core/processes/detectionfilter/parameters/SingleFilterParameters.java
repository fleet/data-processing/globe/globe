package fr.ifremer.globe.core.processes.detectionfilter.parameters;

import java.util.List;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;

/**
 * Class displaying single filters parameters associated with the composite
 */
public class SingleFilterParameters {
	public SingleFilterParameters(IAttributeProvider attributeProvider) {
		this.attributeProvider = attributeProvider;
		retrieveAvailableAttributes();
		selectedAttribute = 0;
		comparator = Comparator.between;
	}

	IAttributeProvider attributeProvider;

	/** All possible comparators. */
	public enum Comparator {
		between("between ( >= x <=)"), less("less than (<=x)"), more("more than (>=x)"), equal("equal");

		protected String label;

		/**
		 * Constructor.
		 */
		private Comparator(String label) {
			this.label = label;
		}

		/**
		 * Follow the link.
		 *
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return label;
		}
	}

	protected static final String[] availableComparatorText = { Comparator.between.toString(),
			Comparator.less.toString(), Comparator.more.toString(), Comparator.equal.toString() };
	protected double valueA;
	protected double valueB;

	Comparator comparator;
	// the list of available attributes
	protected List<PredefinedLayers<?>> attributeList;

	// the selected attribute
	protected int selectedAttribute;

	public String[] getAvailableComparator() {
		return availableComparatorText;
	}

	public List<PredefinedLayers<?>> getAvailableAttributes() {
		return attributeList;
	}

	protected void retrieveAvailableAttributes() {
		attributeList = attributeProvider.retrieveFilterAttributes();
	}

	public void setComparator(Comparator selection) {
		comparator = selection;
	}

	public Comparator getComparator() {
		return comparator;
	}

	public double getValueA() {
		return valueA;
	}

	public double getValueB() {
		return valueB;
	}

	public void setValueA(String text) {
		valueA = Double.parseDouble(text);
	}

	public void setValueB(String text) {
		valueB = Double.parseDouble(text);
	}

	public int getSelectedAttributeIndex() {
		return selectedAttribute;
	}

	public void setSelectedAttributeIndex(int att) {
		selectedAttribute = att;
	}

	public PredefinedLayers<?> getSelectedAttributes() {
		return attributeList.isEmpty() ? null : attributeList.get(getSelectedAttributeIndex());
	}

	public boolean contains(double value) {

		if (comparator == Comparator.between) {
			return value >= valueA && value <= valueB;
		} else if (comparator == Comparator.less) {
			return value <= valueA;
		} else if (comparator == Comparator.more) {
			return value >= valueA;
		} else if (comparator == Comparator.equal) {
			return value == valueA;
		}
		return true;
	}

}
