package fr.ifremer.globe.core.processes.detectionfilter.parameters;

import java.util.List;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;

/**
 * Class displaying single filters parameters associated with the composite
 */
public class DataParameters {

	IAttributeProvider attributeProvider;

	protected boolean checkAllCriterias = true;

	double value;

	// the list of available attributes
	protected List<PredefinedLayers<?>> attributeList;

	// the selected attribute
	protected int selectedAttribute;

	public DataParameters(IAttributeProvider attributeProvider) {
		setAttributeProvider(attributeProvider);
	}

	public void retrieveAvailableAttributes() {
		attributeList = attributeProvider.retrieveDataAttributes();
	}

	public double getValue() {
		return value;
	}

	public void setValue(String text) {
		value = Double.parseDouble(text);
	}

	public int getSelectedAttributeIndex() {
		return selectedAttribute;
	}

	public void setSelectedAttributeIndex(int att) {
		selectedAttribute = att;
	}

	public List<PredefinedLayers<?>> getAvailableAttributes() {
		return attributeList;
	}

	public PredefinedLayers<?> getSelectedAttribute() {
		return attributeList.isEmpty() ? null : attributeList.get(getSelectedAttributeIndex());
	}

	/**
	 * @return the {@link #checkAllCriterias}
	 */
	public boolean isCheckAllCriterias() {
		return checkAllCriterias;
	}

	/**
	 * @param checkAllCriterias the {@link #checkAllCriterias} to set
	 */
	public void setCheckAllCriterias(boolean checkAllCriterias) {
		this.checkAllCriterias = checkAllCriterias;
	}

	/**
	 * @return the {@link #attributeProvider}
	 */
	public IAttributeProvider getAttributeProvider() {
		return attributeProvider;
	}

	/**
	 * @param attributeProvider the {@link #attributeProvider} to set
	 */
	public void setAttributeProvider(IAttributeProvider attributeProvider) {
		this.attributeProvider = attributeProvider;
		if (attributeProvider != null) {
			retrieveAvailableAttributes();
		}
		selectedAttribute = 0;
	}
}
