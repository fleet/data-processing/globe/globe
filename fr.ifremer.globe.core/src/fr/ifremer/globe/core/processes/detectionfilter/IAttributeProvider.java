/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.processes.detectionfilter;

import java.util.List;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;

/**
 * Provide attributes for {@link DataParameters} and {@link SingleFilterParameters}
 */
public interface IAttributeProvider {

	/**
	 * @return all available attributs needed by {@link DataParameters} .
	 */
	List<PredefinedLayers<?>> retrieveDataAttributes();

	/**
	 * @return the type of the data attribute.
	 */
	Class<?> retrieveDataAttributeType(PredefinedLayers<?> layer);

	/**
	 * @return all available attributs needed by {@link SingleFilterParameters} .
	 */
	List<PredefinedLayers<?>> retrieveFilterAttributes();

	/**
	 * @return the type of the filter attribute .
	 */
	Class<?> retrieveFilterAttributeType(PredefinedLayers<?> layer);
}
