/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.processes.detectionfilter;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IBaseBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.bathymetry.VendorSpecificLayers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 *
 */
public class SounderDataContainerAttributeProvider implements IAttributeProvider {

	private static final Logger logger = LoggerFactory.getLogger(SounderDataContainerAttributeProvider.class);

	protected Map<PredefinedLayers<?>, Class<?>> dataAttributes = new LinkedHashMap<>();
	protected Map<PredefinedLayers<?>, Class<?>> filterAttributes = new LinkedHashMap<>();

	/**
	 * Constructor
	 */
	public SounderDataContainerAttributeProvider(List<SounderDataContainer> sounderDataContainers,
			IProgressMonitor monitor) {
		dataAttributes.put(Attributes.VALIDITY, Boolean.class);

		PredefinedLayers<?>[] allFilterLayer = { //
				BeamGroup1Layers.PING_TIME, //
				BeamGroup1Layers.PLATFORM_HEADING, //
				BeamGroup1Layers.PLATFORM_PITCH, //
				BeamGroup1Layers.PLATFORM_ROLL, //
				BeamGroup1Layers.PLATFORM_LONGITUDE, //
				BeamGroup1Layers.PLATFORM_LATITUDE, //
				BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET, //
				BeamGroup1Layers.TX_TRANSDUCER_DEPTH, //
				BeamGroup1Layers.WATERLINE_TO_CHART_DATUM, //
				BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER, //
				BathymetryLayers.DETECTION_X, //
				BathymetryLayers.DETECTION_Y, //
				BathymetryLayers.DETECTION_Z, //
				BathymetryLayers.DETECTION_LONGITUDE, //
				BathymetryLayers.DETECTION_LATITUDE, //
				BathymetryLayers.DETECTION_TYPE, //
				BathymetryLayers.DETECTION_QUALITY_FACTOR, //
				BathymetryLayers.DETECTION_RX_TRANSDUCER_INDEX, //
				BathymetryLayers.DETECTION_BACKSCATTER_R, //
				BathymetryLayers.MULTIPING_SEQUENCE, //
				BathymetryLayers.STATUS, //
				VendorSpecificLayers.RESON_DETECTION_BRIGHTNESS_FLAG, //
				VendorSpecificLayers.RESON_DETECTION_COLINEARITY_FLAG, //
				VendorSpecificLayers.RESON_DETECTION_NADIR_FILTER_FLAG, //
				// DynamicDraughtLayers.DELTA_DRAUGHT, // -> not necessary aligned
				// TideLayers.TIDE_INDICATIVE, // -> not necessary aligned
		};
		for (SounderDataContainer container : sounderDataContainers) {
			filterAttributes.clear();
			filterAttributes.put(Attributes.PING_INDEX, Integer.class);
			filterAttributes.put(Attributes.BEAM_INDEX, Integer.class);
			filterAttributes.put(Attributes.BEAM_POINTING_ACROSS_ANGLE, Integer.class);
			manageLayer(container, allFilterLayer, filterAttributes, monitor);
			//update allFilterLayer to retrieve the intersection of all dataContainer attributes
			allFilterLayer = filterAttributes.keySet().toArray(PredefinedLayers<?>[]::new);
		}

	}

	protected void manageLayer(SounderDataContainer sounderDataContainer, PredefinedLayers<?>[] possibleLayers,
			Map<PredefinedLayers<?>, Class<?>> resultingMap,
			IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, possibleLayers.length);
		try {
			for (PredefinedLayers<?> predefinedLayers : possibleLayers) {
				if (subMonitor.isCanceled()) {
					return;
				}
				if (sounderDataContainer.hasLayer(predefinedLayers)) {
					ILoadableLayer<? extends IBaseBuffer> layer = sounderDataContainer.getLayer(predefinedLayers);
					subMonitor.worked(1);
					if (layer != null) {
						layer.process(new IAbstractBaseLayerOperation() {

							/** {@inheritDoc} */
							@Override
							public void visit(BooleanLayer1D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Boolean.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(BooleanLayer2D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Boolean.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(ByteLayer1D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Byte.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(ByteLayer2D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Byte.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(ShortLayer1D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Short.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(ShortLayer2D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Short.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(IntLayer1D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Integer.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(IntLayer2D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Integer.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(LongLayer1D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Long.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(LongLayer2D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Long.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(FloatLayer1D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Float.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(FloatLayer2D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Float.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(DoubleLayer1D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Double.class);
							}

							/** {@inheritDoc} */
							@Override
							public void visit(DoubleLayer2D layer) throws GIOException {
								resultingMap.put(predefinedLayers, Double.class);
							}
						});
					}
				}
			}
		} catch (GIOException e) {
			logger.debug("Error while retrieving layers", e);
		}
	}

	@Override
	public Class<?> retrieveDataAttributeType(PredefinedLayers<?> layer) {
		return dataAttributes.get(layer);
	}

	/** {@inheritDoc} */
	@Override
	public List<PredefinedLayers<?>> retrieveDataAttributes() {
		return new ArrayList<>(dataAttributes.keySet());
	}

	/** {@inheritDoc} */
	@Override
	public List<PredefinedLayers<?>> retrieveFilterAttributes() {
		return new ArrayList<>(filterAttributes.keySet());
	}

	@Override
	public Class<?> retrieveFilterAttributeType(PredefinedLayers<?> layer) {
		if (BeamGroup1Layers.PING_TIME == layer) {
			return Date.class;
		} else if (VendorSpecificLayers.RESON_DETECTION_BRIGHTNESS_FLAG == layer || //
				VendorSpecificLayers.RESON_DETECTION_COLINEARITY_FLAG == layer || //
				VendorSpecificLayers.RESON_DETECTION_NADIR_FILTER_FLAG == layer) {
			return Boolean.class;
		}
		return filterAttributes.get(layer);
	}

}
