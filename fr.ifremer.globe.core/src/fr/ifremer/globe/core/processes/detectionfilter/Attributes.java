/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.processes.detectionfilter;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 *
 */
public class Attributes {

	public static final PredefinedLayers<BooleanLoadableLayer2D> VALIDITY = new PredefinedLayers<>("Custom", "VALIDITY",
			"Validity", "", DataKind.continuous, BooleanLoadableLayer2D::new);
	public static final double VALIDITY_UNVALID = NumberUtils.bool2byte(false);
	public static final double VALIDITY_VALID = NumberUtils.bool2byte(true);

	public static final PredefinedLayers<BooleanLoadableLayer2D> SELECTION = new PredefinedLayers<>("Custom",
			"SELECTION", "Selection", "", DataKind.continuous, BooleanLoadableLayer2D::new);
	public static final double SELECTION_YES = NumberUtils.bool2byte(true);
	public static final double SELECTION_NO = NumberUtils.bool2byte(false);

	public static final PredefinedLayers<IntLoadableLayer1D> PING_INDEX = new PredefinedLayers<>("Custom", "PING_INDEX",
			"P:ping index", "", DataKind.continuous, IntLoadableLayer1D::new);
	public static final PredefinedLayers<IntLoadableLayer1D> BEAM_INDEX = new PredefinedLayers<>("Custom", "BEAM_INDEX",
			"RxB:beam Index", "", DataKind.continuous, IntLoadableLayer1D::new);
	public static final PredefinedLayers<DoubleLoadableLayer2D> DEPTH = new PredefinedLayers<>("Custom", "DEPTH",
			"RxB:depth", "", DataKind.continuous, DoubleLoadableLayer2D::new);
	public static final PredefinedLayers<DoubleLoadableLayer2D> BEAM_POINTING_ACROSS_ANGLE = new PredefinedLayers<>("Custom",
			"BEAM_POINTING_ACROSS_ANGLE", "RxB: beam pointing across angle (ref vertical: SCS)", "", DataKind.continuous, DoubleLoadableLayer2D::new);
	public static final PredefinedLayers<DoubleLoadableLayer2D> REFLECTIVITY = new PredefinedLayers<>("Custom",
			"REFLECTIVITY", "RxB:reflectivity", "", DataKind.continuous, DoubleLoadableLayer2D::new);
	public static final PredefinedLayers<BooleanLoadableLayer2D> AMPLITUDE_PHASE_DETECTION = new PredefinedLayers<>(
			"Custom", "AMPLITUDE_PHASE_DETECTION", "RxB: amplitude or phase detection", "", DataKind.continuous,
			BooleanLoadableLayer2D::new);
	public static final PredefinedLayers<FloatLoadableLayer2D> SCALAR_QUALITY_FACTOR = new PredefinedLayers<>("Custom",
			"SCALAR_QUALITY_FACTOR", "RxB:scalar quality Factor", "", DataKind.continuous, FloatLoadableLayer2D::new);

}
