/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.processes.detectionfilter;


import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 *
 */
public class LayerValueGetter implements IAbstractBaseLayerOperation {

	protected int swath;
	protected int beam;
	protected double value = Double.NaN;

	/** {@inheritDoc} */
	@Override
	public void visit(BooleanLayer1D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath ? NumberUtils.bool2byte(layer.get(swath)) : Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ByteLayer1D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath ? layer.get(swath) : Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ShortLayer1D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath ? layer.get(swath) : Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(IntLayer1D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath ? layer.get(swath) : Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(LongLayer1D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath ? layer.get(swath) : Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(FloatLayer1D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath ? layer.get(swath) : Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(DoubleLayer1D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath ? (Double) layer.get(swath) : Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(BooleanLayer2D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath && layer.getDimensions()[1] > beam
				? NumberUtils.bool2byte(layer.getValue(swath, beam))
				: Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ByteLayer2D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath && layer.getDimensions()[1] > beam ? layer.get(swath, beam)
				: Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ShortLayer2D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath && layer.getDimensions()[1] > beam ? layer.get(swath, beam)
				: Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(IntLayer2D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath && layer.getDimensions()[1] > beam ? layer.get(swath, beam)
				: Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(LongLayer2D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath && layer.getDimensions()[1] > beam ? layer.get(swath, beam)
				: Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(FloatLayer2D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath && layer.getDimensions()[1] > beam ? layer.get(swath, beam)
				: Double.NaN;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(DoubleLayer2D layer) throws GIOException {
		value = layer.getDimensions()[0] > swath && layer.getDimensions()[1] > beam ? (Double) layer.get(swath, beam)
				: Double.NaN;
	}

	/**
	 * @return the {@link #swath}
	 */
	public int getSwath() {
		return swath;
	}

	/**
	 * @param swath the {@link #swath} to set
	 */
	public void setSwath(int swath) {
		this.swath = swath;
	}

	/**
	 * @return the {@link #beam}
	 */
	public int getBeam() {
		return beam;
	}

	/**
	 * @param beam the {@link #beam} to set
	 */
	public void setBeam(int beam) {
		this.beam = beam;
	}

	/**
	 * @return the {@link #value}
	 */
	public double getValue() {
		return value;
	}

}
