package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

public class RollCorrector implements IBiasCorrector {

	/** Nb of correction applied */
	int correctionCount = 0;

	@Override
	public void applyBias(IDataProxy dataProxy, int swathIdx, double biasValue, OptionalInt antennaIndex)
			throws GIOException {
		double roll = dataProxy.getRoll(swathIdx) + biasValue;
		applyNewValue(dataProxy, swathIdx, roll, antennaIndex);
	}

	@Override
	public void applyNewValue(IDataProxy dataProxy, int swathIdx, double newValue, OptionalInt antennaIndex)
			throws GIOException {
		double rollBias = newValue - dataProxy.getRoll(swathIdx);

		dataProxy.setRoll(swathIdx, newValue);

		dataProxy.processSounding(beamIdx -> {
			if (antennaIndex.isEmpty() || antennaIndex.getAsInt() == dataProxy.getAntennaIndex(swathIdx, beamIdx)) {
				double depth = dataProxy.getDepth(swathIdx, beamIdx);
				double accrossDist = dataProxy.getAcrossDistance(swathIdx, beamIdx);
				double acrossAngle = dataProxy.getAcrossAngle(swathIdx, beamIdx);
				double distObl = Math.sqrt(depth * depth + accrossDist * accrossDist);
				double alpha = Math.atan2(accrossDist, depth) - Math.toRadians(rollBias);

				accrossDist = distObl * Math.sin(alpha);
				depth = distObl * Math.cos(alpha);
				acrossAngle -= rollBias;

				// Apply modifications
				dataProxy.setDepth(swathIdx, beamIdx, depth);
				dataProxy.setAcrossDistance(swathIdx, beamIdx, accrossDist);
				dataProxy.setAcrossAngle(swathIdx, beamIdx, acrossAngle);

				correctionCount++;
			}
		});
	}

	/** @return the nb of correction applied */
	public int getCorrectionCount() {
		return correctionCount;
	}

	/** set the nb of correction to 0 */
	public void resetCorrectionCount() {
		correctionCount = 0;
	}

}