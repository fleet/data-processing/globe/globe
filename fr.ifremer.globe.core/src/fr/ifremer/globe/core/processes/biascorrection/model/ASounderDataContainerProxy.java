package fr.ifremer.globe.core.processes.biascorrection.model;

import java.io.IOException;
import java.util.Optional;

import fr.ifremer.globe.core.model.attitude.AttitudeInterpolator;
import fr.ifremer.globe.core.model.attitude.FloatInterpolator;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.OptionalLayersAccessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionBeamPointingAnglesLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderAttitudeData;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Abstract class which implements a {@link IDataProxy} based on {@link SounderDataContainer}
 */
public abstract class ASounderDataContainerProxy implements IDataProxy {

	// Sounder data container layers: contain raw data
	protected LongLoadableLayer1D timeLayer;
	protected FloatLoadableLayer1D txZDepth;
	protected FloatLoadableLayer1D swathHeadingLayer;
	protected FloatLoadableLayer1D swathRollLayer;
	protected FloatLoadableLayer1D swathPitchLayer;
	protected FloatLoadableLayer1D swathPlatformVerticalOffsetLayer;
	protected FloatLoadableLayer1D soundSpeedLayer;
	protected DoubleLayer2D depthLayer;
	protected DoubleLayer2D alongDistLayer;
	protected DoubleLayer2D acrossDistLayer;
	protected Optional<DoubleLoadableLayer2D> acrossAngleLayer;
	protected Optional<FloatLayer2D> beamPointingAngleLayerInScs;
	protected Optional<DoubleLoadableLayer2D> alongAngleLayer; // = Azimuth angle
	protected CompositeCSLayers coordinateSystemLayers;
	protected ShortLoadableLayer2D antennaIndexLayer;
	protected Optional<AttitudeInterpolator> attitudeInterpolator;
	protected FloatBuffer1D verticalOffsetValues;
	protected FloatInterpolator platformVerticalOffsetInterpolator;

	/**
	 * Retrieves needed layers.
	 */
	protected void initLayers(SounderDataContainer dataContainer) throws GIOException {
		timeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);

		// Attitude layers
		swathHeadingLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
		swathRollLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
		swathPitchLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);

		swathPlatformVerticalOffsetLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

		// Position layers
		coordinateSystemLayers = dataContainer.getCsLayers(CoordinateSystem.ACS);
		alongDistLayer = coordinateSystemLayers.getProjectedX();
		acrossDistLayer = coordinateSystemLayers.getProjectedY();
		depthLayer = coordinateSystemLayers.getProjectedZ();

		txZDepth = dataContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
		soundSpeedLayer = dataContainer.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);

		OptionalLayersAccessor accessor = dataContainer.getInfo().getOptionalLayersAccessor().orElse(null);
		if (accessor != null) { // MBG
			PredefinedLayers<DoubleLoadableLayer2D> layerRef = accessor.getAcrossAngleLayer().orElse(null);
			acrossAngleLayer = layerRef != null ? dataContainer.getOptionalLayer(layerRef) : Optional.empty();
			layerRef = accessor.getAlongAngleLayer().orElse(null);
			alongAngleLayer = layerRef != null ? dataContainer.getOptionalLayer(layerRef) : Optional.empty();
			beamPointingAngleLayerInScs = Optional.empty();
		} else { // XSF
			acrossAngleLayer = Optional.empty();
			alongAngleLayer = Optional.empty();
			beamPointingAngleLayerInScs = new DetectionBeamPointingAnglesLayers(dataContainer).getAcrossAngleScsLayer();
		}

		antennaIndexLayer = dataContainer.getLayer(BathymetryLayers.DETECTION_RX_TRANSDUCER_INDEX);

		if (dataContainer.getInfo().getPreferedAttitudeGroupId().isPresent()) {
			attitudeInterpolator = Optional.of(new AttitudeInterpolator(new SounderAttitudeData(dataContainer)));
		} else {
			attitudeInterpolator = Optional.empty();
		}
		
		try {
			//make a copy of plaform vertical layer to avoid recursive corrections on source data 
			verticalOffsetValues = swathPlatformVerticalOffsetLayer.getData().copy();
		} catch (IOException e) {
			// rethrow with as GIOException
			throw new GIOException(e.getMessage(), e);
		}
		platformVerticalOffsetInterpolator = new FloatInterpolator(verticalOffsetValues, timeLayer);
	}

	public void dispose() {
		//delete computed layers
		if(verticalOffsetValues != null) {
			verticalOffsetValues.dispose();
			verticalOffsetValues = null;
		}
	}
	
	/**********************************************************************************************************
	 * Getters with time
	 **********************************************************************************************************/

	@Override
	public boolean hasAttitudeInterpolator() {
		return attitudeInterpolator.isPresent();
	}

	@Override
	public double getInterpolatedHeading(long epochTimeNano) throws GIOException {
		return attitudeInterpolator.get().getHeading(epochTimeNano);
	}

	@Override
	public double getInterpolatedRoll(long epochTimeNano) throws GIOException {
		return attitudeInterpolator.get().getRoll(epochTimeNano);
	}

	@Override
	public double getInterpolatedPitch(long epochTimeNano) throws GIOException {
		return attitudeInterpolator.get().getPitch(epochTimeNano);
	}

	@Override
	public double getInterpolatedVerticalOffset(long epochTimeNano) throws GIOException {
		return platformVerticalOffsetInterpolator.getInterpolatedValue(epochTimeNano);
	}

}
