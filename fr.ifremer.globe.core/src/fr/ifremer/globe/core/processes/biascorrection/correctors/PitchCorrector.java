package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

public class PitchCorrector implements IBiasCorrector {

	/** Nb of correction applied */
	int correctionCount = 0;

	@Override
	public void applyBias(IDataProxy dataProxy, int swathIdx, double biasValue, OptionalInt antennaIndex)
			throws GIOException {
		double pitch = dataProxy.getPitch(swathIdx) + biasValue;
		applyNewValue(dataProxy, swathIdx, pitch, antennaIndex);
	}

	@Override
	public void applyNewValue(IDataProxy dataProxy, int swathIdx, double newValue, OptionalInt antennaIndex)
			throws GIOException {
		double pitchBias = newValue - dataProxy.getPitch(swathIdx);

		dataProxy.setPitch(swathIdx, newValue);

		dataProxy.processSounding(beamIdx -> {
			if (antennaIndex.isEmpty() || antennaIndex.getAsInt() == dataProxy.getAntennaIndex(swathIdx, beamIdx)) {
				double depth = dataProxy.getDepth(swathIdx, beamIdx);
				double alongDist = dataProxy.getAlongDistance(swathIdx, beamIdx);
				double accrossDist = dataProxy.getAcrossDistance(swathIdx, beamIdx);
				double azimuthAngle = dataProxy.getAzimutAngle(swathIdx, beamIdx);
				double distObl = Math.sqrt(depth * depth + alongDist * alongDist);
				double alpha = Math.atan2(alongDist, depth) + Math.toRadians(pitchBias);

				// update along distance and depth
				double alongDistCor = distObl * Math.sin(alpha);
				depth = distObl * Math.cos(alpha);

				double azBias = Math.atan2(alongDist, accrossDist) - Math.atan2(alongDistCor, accrossDist);
				azimuthAngle = azimuthAngle + Math.toDegrees(azBias);
				if (azimuthAngle < 0. || azimuthAngle > 360)// Get angle between 0 and 360 degrees
					azimuthAngle = azimuthAngle < 0. ? azimuthAngle % 360 + 360 : azimuthAngle % 360;

				// Apply modifications
				dataProxy.setDepth(swathIdx, beamIdx, depth);
				dataProxy.setAlongDistance(swathIdx, beamIdx, alongDistCor);
				dataProxy.setAzimutAngle(swathIdx, beamIdx, azimuthAngle);

				correctionCount++;
			}
		});

	}

	/** @return the nb of correction applied */
	public int getCorrectionCount() {
		return correctionCount;
	}

	/** set the nb of correction to 0 */
	public void resetCorrectionCount() {
		correctionCount = 0;
	}

}