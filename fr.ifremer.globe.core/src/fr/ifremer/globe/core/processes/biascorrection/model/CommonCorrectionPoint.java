package fr.ifremer.globe.core.processes.biascorrection.model;

/**
 * A correction point in common mode, applied to all the files
 */
public class CommonCorrectionPoint extends CorrectionPoint {

	public CommonCorrectionPoint() {
		super();
	}

	public CommonCorrectionPoint(CorrectionPoint other) {
		super(other);
	}

	@Override
	public void accept(ICorrectionPointVisitor visitor) {
		visitor.visit(this);
	}
}
