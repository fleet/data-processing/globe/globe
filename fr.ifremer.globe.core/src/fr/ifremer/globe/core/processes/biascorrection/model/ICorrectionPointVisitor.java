package fr.ifremer.globe.core.processes.biascorrection.model;

public interface ICorrectionPointVisitor {
	public default void visit(FileCorrectionPoint p) {};
	public default void visit(CommonCorrectionPoint p) {};
}
