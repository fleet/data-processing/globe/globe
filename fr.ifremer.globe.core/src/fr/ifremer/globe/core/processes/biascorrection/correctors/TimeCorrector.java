package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.Optional;
import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

public class TimeCorrector implements IBiasCorrector {

	/** Properties **/
	private Optional<HeadingCorrector> headingCorrector;
	private Optional<RollCorrector> rollCorrector;
	private Optional<PitchCorrector> pitchCorrector;
	private Optional<PlatformVerticalOffsetCorrector> verticalOffsetCorrector;

	/** Nb of correction applied */
	int correctionCount = 0;

	/** Constructor **/
	public TimeCorrector(Optional<HeadingCorrector> headingCorrector, Optional<RollCorrector> rollCorrector,
			Optional<PitchCorrector> pitchCorrector,
			Optional<PlatformVerticalOffsetCorrector> verticalOffsetCorrector) {
		super();
		this.headingCorrector = headingCorrector;
		this.rollCorrector = rollCorrector;
		this.pitchCorrector = pitchCorrector;
		this.verticalOffsetCorrector = verticalOffsetCorrector;
	}

	@Override
	public void applyBias(IDataProxy dataProxy, int swathIdx, double biasValueMs, OptionalInt antennaIndex)
			throws GIOException {
		double newTimeMs = dataProxy.getEpochTimeNano(swathIdx)/1_000_000.0 + biasValueMs;
		applyNewValue(dataProxy, swathIdx, newTimeMs, antennaIndex);
	}

	@Override
	public void applyNewValue(IDataProxy dataProxy, int swathIdx, double newTimeMs, OptionalInt antennaIndex)
			throws GIOException {

		if (dataProxy.hasAttitudeInterpolator()) {
			long oldTimeNano = dataProxy.getEpochTimeNano(swathIdx);
			long newTimeNano = (long) (newTimeMs*1_000_000);
			// Heading Correction
			if (headingCorrector.isPresent()) {
				double headingBias = dataProxy.getInterpolatedHeading(newTimeNano)
						- dataProxy.getInterpolatedHeading(oldTimeNano);
				headingCorrector.get().applyBias(dataProxy, swathIdx, headingBias, antennaIndex);
			}

			// Roll Correction
			if (rollCorrector.isPresent()) {
				double rollBias = dataProxy.getInterpolatedRoll(newTimeNano)
						- dataProxy.getInterpolatedRoll(oldTimeNano);
				rollCorrector.get().applyBias(dataProxy, swathIdx, rollBias, antennaIndex);
			}

			// Pitch Correction
			if (pitchCorrector.isPresent()) {
				double pitchBias = dataProxy.getInterpolatedPitch(newTimeNano)
						- dataProxy.getInterpolatedPitch(oldTimeNano);
				pitchCorrector.get().applyBias(dataProxy, swathIdx, pitchBias, antennaIndex);
			}

			// Vertical offset Correction
			if (verticalOffsetCorrector.isPresent()) {
				double verticalOffsetBias = dataProxy.getInterpolatedVerticalOffset(newTimeNano)
						- dataProxy.getInterpolatedVerticalOffset(oldTimeNano);
				verticalOffsetCorrector.get().applyBias(dataProxy, swathIdx, verticalOffsetBias, antennaIndex);
			}
			correctionCount++;
		}
	}
	
	/** @return the nb of correction applied */
	public int getCorrectionCount() {
		return correctionCount;
	}

	/** set the nb of correction to 0 */
	public void resetCorrectionCount() {
		correctionCount = 0;
	}
}
