package fr.ifremer.globe.core.processes.biascorrection.model;

public interface IBiasSounding {

	short getBeamNumber();

	void setGeographicalCoordinates(double longitude, double latitude, double depth);

	int getSwathNumber();

}