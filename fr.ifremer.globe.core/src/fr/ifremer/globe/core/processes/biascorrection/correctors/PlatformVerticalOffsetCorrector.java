package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

public class PlatformVerticalOffsetCorrector implements IBiasCorrector {

	/** Nb of correction applied */
	int correctionCount = 0;

	@Override
	public void applyBias(IDataProxy dataProxy, int swathIdx, double biasValue, OptionalInt antennaIndex)
			throws GIOException {
		dataProxy.setTxZDepth(swathIdx, dataProxy.getTxZDepth(swathIdx) - biasValue);
		dataProxy.setPlatformVerticalOffset(swathIdx, dataProxy.getPlatformVerticalOffset(swathIdx) + biasValue);
		correctionCount++;
	}

	@Override
	public void applyNewValue(IDataProxy dataProxy, int swathIdx, double newValue, OptionalInt antennaIndex)
			throws GIOException {
		// evaluate old value
		double bias = newValue - dataProxy.getPlatformVerticalOffset(swathIdx);
		// apply bias
		applyBias(dataProxy, swathIdx, bias, antennaIndex);
	}

	/** @return the nb of correction applied */
	public int getCorrectionCount() {
		return correctionCount;
	}

	/** set the nb of correction to 0 */
	public void resetCorrectionCount() {
		correctionCount = 0;
	}

}