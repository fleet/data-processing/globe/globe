package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Class which implements a {@link IBiasCorrector} to rectify Motion Reference Unit vertical misalignment
 */
public class MruHeadingCorrector implements IBiasCorrector {

	/** Nb of correction applied */
	int correctionCount = 0;

	@Override
	public void applyBias(IDataProxy dataProxy, int swathIdx, double biasValue, OptionalInt antennaIndex)
			throws GIOException {
		applyNewValue(dataProxy, swathIdx, biasValue, antennaIndex);
	}

	@Override
	public void applyNewValue(IDataProxy dataProxy, int swathIdx, double newValue, OptionalInt antennaIndex)
			throws GIOException {

		double mruHeadingBias = Math.toRadians(newValue);
		double roll = Math.toRadians(dataProxy.getRoll(swathIdx));
		double pitch = Math.toRadians(dataProxy.getPitch(swathIdx));

		double rollTrue = (Math.sin(roll) - Math.sin(pitch) * Math.tan(mruHeadingBias));
		rollTrue = rollTrue / Math.cos(mruHeadingBias) / (1. + Math.pow(Math.tan(mruHeadingBias), 2.));
		rollTrue = Math.asin(rollTrue);
		roll = Math.toDegrees(rollTrue);

		double rollBias = roll - dataProxy.getRoll(swathIdx);
		dataProxy.setRoll(swathIdx, roll);

		dataProxy.processSounding(beamIdx -> {
			if (antennaIndex.isEmpty() || antennaIndex.getAsInt() == dataProxy.getAntennaIndex(swathIdx, beamIdx)) {
				double depth = dataProxy.getDepth(swathIdx, beamIdx);
				double accrossDist = dataProxy.getAcrossDistance(swathIdx, beamIdx);
				double acrossAngle = dataProxy.getAcrossAngle(swathIdx, beamIdx);
				double distObl = Math.sqrt(depth * depth + accrossDist * accrossDist);
				double alpha = Math.atan2(accrossDist, depth) - Math.toRadians(rollBias);

				accrossDist = distObl * Math.sin(alpha);
				depth = distObl * Math.cos(alpha);
				acrossAngle -= rollBias;

				// Apply modifications
				dataProxy.setDepth(swathIdx, beamIdx, depth);
				dataProxy.setAcrossDistance(swathIdx, beamIdx, accrossDist);
				dataProxy.setAcrossAngle(swathIdx, beamIdx, acrossAngle);

				correctionCount++;
			}
		});
	}

	/** @return the nb of correction applied */
	public int getCorrectionCount() {
		return correctionCount;
	}

	/** set the nb of correction to 0 */
	public void resetCorrectionCount() {
		correctionCount = 0;
	}

}