package fr.ifremer.globe.core.processes.biascorrection.model;

import java.time.Instant;

public abstract class CorrectionPoint implements Cloneable, Comparable<CorrectionPoint> {

	protected Instant date;
	protected double platformVerticalOffset;
	protected double roll;
	protected double pitch;
	protected double attitudeTimeMs;
	protected double verticalOffsetTimeMs;
	protected double heading;
	protected double velocity;
	protected double mruHeading;

	protected int indexInDeletionList;

	public CorrectionPoint() {
		this.date = Instant.EPOCH;
		this.velocity = Double.NaN;
		this.roll = Double.NaN;
		this.pitch = Double.NaN;
		this.attitudeTimeMs = Double.NaN;
		this.verticalOffsetTimeMs = Double.NaN;
		this.heading = Double.NaN;
		this.mruHeading = Double.NaN;
		this.platformVerticalOffset = Double.NaN;
	}

	public CorrectionPoint(CorrectionPoint other) {
		this.date = other.date;
		this.velocity = other.velocity;
		this.roll = other.roll;
		this.pitch = other.pitch;
		this.attitudeTimeMs = other.attitudeTimeMs;
		this.verticalOffsetTimeMs = other.verticalOffsetTimeMs;
		this.heading = other.heading;
		this.mruHeading = other.mruHeading;
		this.platformVerticalOffset = other.platformVerticalOffset;
	}

	@Override
	public Object clone() {
		Object o = null;
		try {
			// On recupere l'instance a renvoyer par l'appel de la methode super.clone()
			o = super.clone();
		} catch (CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous implementons l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// On renvoie le clone
		return o;
	}

	/**
	 * @return
	 */
	public boolean isEmpty() {
		return Double.isNaN(getHeading()) && Double.isNaN(getRoll()) && Double.isNaN(getPitch())
				&& Double.isNaN(getAttitudeTimeMs()) && Double.isNaN(getVerticalOffsetTimeMs())
				&& Double.isNaN(getPlatformVerticalOffset()) && Double.isNaN(getVelocity())
				&& Double.isNaN(getMruHeading());
	}

	/** Functional interface use by interpolation methods **/
	@FunctionalInterface
	public interface ICorrPointValueSupplier {
		public double get(CorrectionPoint correctionPoint);
	}

	/** @return an interpolated value **/
	protected static double getInterpolatedValue(ICorrPointValueSupplier valueSupplier, CorrectionPoint before,
			CorrectionPoint after, Instant timestamp) {

		if (Double.isNaN(valueSupplier.get(before)) && Double.isNaN(valueSupplier.get(after)))
			return Double.NaN; // if no values : return NaN
		else if (Double.isNaN(valueSupplier.get(before))) // if no value before : return after
			return valueSupplier.get(after);
		else if (Double.isNaN(valueSupplier.get(after))) // if no value after: return before
			return valueSupplier.get(before);

		return valueSupplier.get(before) + (valueSupplier.get(after) - valueSupplier.get(before))
				/ (after.getDate().toEpochMilli() - before.getDate().toEpochMilli())
				* (timestamp.toEpochMilli() - before.getDate().toEpochMilli());
	}

	/** @return an interpolated correction point **/
	protected static CorrectionPoint getInterpolatedPoint(CorrectionPoint before, CorrectionPoint after,
			Instant timestamp) {
		CommonCorrectionPoint correctionPoint = new CommonCorrectionPoint();

		correctionPoint.setHeading(getInterpolatedValue(CorrectionPoint::getHeading, before, after, timestamp));
		correctionPoint.setMruHeading(getInterpolatedValue(CorrectionPoint::getMruHeading, before, after, timestamp));
		correctionPoint.setPitch(getInterpolatedValue(CorrectionPoint::getPitch, before, after, timestamp));
		correctionPoint.setRoll(getInterpolatedValue(CorrectionPoint::getRoll, before, after, timestamp));
		correctionPoint
				.setAttitudeTimeMs(getInterpolatedValue(CorrectionPoint::getAttitudeTimeMs, before, after, timestamp));
		correctionPoint.setVerticalOffsetTimeMs(
				getInterpolatedValue(CorrectionPoint::getVerticalOffsetTimeMs, before, after, timestamp));
		correctionPoint.setVelocity(getInterpolatedValue(CorrectionPoint::getVelocity, before, after, timestamp));
		correctionPoint.setPlatformVerticalOffset(
				getInterpolatedValue(CorrectionPoint::getPlatformVerticalOffset, before, after, timestamp));

		return correctionPoint;
	}

	/** @return the sum between two point for a specified variable */
	private static double getConcatenatedValue(ICorrPointValueSupplier valueSupplier, CorrectionPoint p1,
			CorrectionPoint p2) {
		if (!Double.isNaN(valueSupplier.get(p1)) || !Double.isNaN(valueSupplier.get(p2))) {
			double value1 = Double.isNaN(valueSupplier.get(p1)) ? 0 : valueSupplier.get(p1);
			double value2 = Double.isNaN(valueSupplier.get(p2)) ? 0 : valueSupplier.get(p2);
			return value1 + value2;
		}
		return Double.NaN;
	}

	/** @return a new correction point which is the sum of 2 point **/
	public static CommonCorrectionPoint concatenate(CorrectionPoint point1, CorrectionPoint point2) {

		CommonCorrectionPoint correctionPoint = new CommonCorrectionPoint();

		correctionPoint.setHeading(getConcatenatedValue(CorrectionPoint::getHeading, point1, point2));
		correctionPoint.setPitch(getConcatenatedValue(CorrectionPoint::getPitch, point1, point2));
		correctionPoint.setRoll(getConcatenatedValue(CorrectionPoint::getRoll, point1, point2));
		correctionPoint.setAttitudeTimeMs(getConcatenatedValue(CorrectionPoint::getAttitudeTimeMs, point1, point2));
		correctionPoint.setVerticalOffsetTimeMs(
				getConcatenatedValue(CorrectionPoint::getVerticalOffsetTimeMs, point1, point2));
		correctionPoint.setVelocity(getConcatenatedValue(CorrectionPoint::getVelocity, point1, point2));
		correctionPoint.setPlatformVerticalOffset(
				getConcatenatedValue(CorrectionPoint::getPlatformVerticalOffset, point1, point2));
		correctionPoint.setMruHeading(getConcatenatedValue(CorrectionPoint::getMruHeading, point1, point2));
		return correctionPoint;
	}

	/** Getters and setters **/

	public Instant getDate() {
		return this.date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public double getVelocity() {
		return this.velocity;
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}

	public double getRoll() {
		return this.roll;
	}

	public void setRoll(double rollCorrection) {
		this.roll = rollCorrection;
	}

	public double getPitch() {
		return this.pitch;
	}

	public void setPitch(double pitchCorrection) {
		this.pitch = pitchCorrection;
	}

	public double getAttitudeTimeMs() {
		return this.attitudeTimeMs;
	}

	public void setAttitudeTimeMs(double timeCorrection) {
		this.attitudeTimeMs = timeCorrection;
	}

	public double getVerticalOffsetTimeMs() {
		return this.verticalOffsetTimeMs;
	}

	public void setVerticalOffsetTimeMs(double timeCorrection) {
		this.verticalOffsetTimeMs = timeCorrection;
	}

	public double getHeading() {
		return this.heading;
	}

	public void setHeading(double headingCorrection) {
		this.heading = headingCorrection;
	}

	public double getMruHeading() {
		return this.mruHeading;
	}

	public void setMruHeading(double mruHeadingCorrection) {
		this.mruHeading = mruHeadingCorrection;
	}

	@Override
	public int compareTo(CorrectionPoint comparedPoint) {
		return this.getDate().compareTo(comparedPoint.getDate());
	}

	public int getIndexInDeletionList() {
		return this.indexInDeletionList;
	}

	public void setIndexInDeletionList(int indexInDeletionList) {
		this.indexInDeletionList = indexInDeletionList;
	}

	public abstract void accept(ICorrectionPointVisitor visitor);

	public double getPlatformVerticalOffset() {
		return platformVerticalOffset;
	}

	public void setPlatformVerticalOffset(double platformVerticalOffset) {
		this.platformVerticalOffset = platformVerticalOffset;
	}

	@Override
	public String toString() {
		return "CorrectionPoint [date=" + date + ", platform_vertical_offset=" + platformVerticalOffset + ", roll="
				+ roll + ", pitch=" + pitch + ", attitude_time=" + attitudeTimeMs + ", vertical_offset_time="
				+ verticalOffsetTimeMs + ", heading=" + heading + ", mruHeading=" + mruHeading + ", velocity="
				+ velocity + "]";
	}

}
