package fr.ifremer.globe.core.processes.biascorrection.model;


/**
 * Defines limits with a minimum/maximum swath and beam indexes.
 */
public class SwathBeamLimits
{
	private int swathMin;
	private int swathMax;
	private int beamMin;
	private int beamMax;

	public SwathBeamLimits(int swathMin, int swathMax, int beamMin, int beamMax) {
		this.swathMin = swathMin;
		this.swathMax = swathMax;
		this.beamMin = beamMin;
		this.beamMax = beamMax;
	}
	
	public SwathBeamLimits() {
		this.swathMin = -Integer.MAX_VALUE;
		this.swathMax = Integer.MAX_VALUE;
		this.beamMin = -Integer.MAX_VALUE;
		this.beamMax = Integer.MAX_VALUE;
	}

	public boolean isInLimits(int ping, int beam) {
		return isInSwathInterval(ping) && isBeamInBeamInterval(beam);
	}

	/**
	 * Return true if the ping is in ping interval
	 * @param pswath
	 * @return
	 */
	public boolean isInSwathInterval(int pswath) {
		boolean isPingInPingInterval = false;
		if (pswath >= swathMin && pswath <= swathMax) {
			isPingInPingInterval = true;
		}
		return isPingInPingInterval;
	}
	
	/**
	 * Return true if the beam is in ping interval
	 * @param lbeam
	 * @return
	 */
	public boolean isBeamInBeamInterval(int lbeam) {
		boolean isBeamInBeamInterval = false;
		if (lbeam >= beamMin && lbeam <= beamMax) {
			isBeamInBeamInterval = true;
		}
		return isBeamInBeamInterval;
	}

	public int getSwathLenght() {
		return swathMax-swathMin+1;
	}

	public int getBeamLenght() {
		return beamMax-beamMin+1;
	}

	public int getSwathMin() {
		return swathMin;
	}

	public void setSwathMin(int swathMin) {
		this.swathMin = swathMin;
	}

	public int getSwathMax() {
		return swathMax;
	}

	public void setSwathMax(int swathMax) {
		this.swathMax = swathMax;
	}

	public int getBeamMin() {
		return beamMin;
	}

	public void setBeamMin(int beamMin) {
		this.beamMin = beamMin;
	}

	public int getBeamMax() {
		return beamMax;
	}

	public void setBeamMax(int beamMax) {
		this.beamMax = beamMax;
	}
	
	
}