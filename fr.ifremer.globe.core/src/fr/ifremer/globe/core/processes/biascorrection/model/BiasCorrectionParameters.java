package fr.ifremer.globe.core.processes.biascorrection.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.ProcessParameters;

public class BiasCorrectionParameters extends ProcessParameters {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parameter file name.
	 */
	public static final String BIASCORRETION_PARAMETERS_FILE_NAME = "BiasCorrection.properties";

	/**
	 * Correction file name.
	 */
	public static final String PROPERTY_CORRECTION_FILE_NAME = "correctionFilename";

	/**
	 * Apply correction only on detection belonging to this antenna index.
	 */
	private static final String PROPERTY_ANTENNA_INDEX = "antennaIndex";

	public BiasCorrectionParameters() {
		super();
	}

	/**
	 * Read parameters values.
	 *
	 * @return
	 */
	@Override
	protected void initialize() {
		String paramFileName = getUserFilePath(BIASCORRETION_PARAMETERS_FILE_NAME);
		try {
			File f = new File(paramFileName);
			if (f.exists())
				load(new FileInputStream(paramFileName));
			else
				load(getDefaultParameterFilePath("fr.ifremer.globe.core.processes.biascorrection"));

		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Write parameters values.
	 *
	 * @return
	 */
	public void save() {
		String paramFileName = getUserFilePath(BIASCORRETION_PARAMETERS_FILE_NAME);
		File f = new File(paramFileName);
		try {
			if (!f.exists()) {
				f.getParentFile().mkdirs();
			}
			store(new FileOutputStream(f), null);
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Restore default parameters values.
	 *
	 * @return
	 */
	public void restoreDefault() {
		try {
			load(getDefaultParameterFilePath("fr.ifremer.globe.core.processes.biascorrection"));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/** @return the configured antenna index or OptionalInt.empty if none */
	public OptionalInt getAntennaIndex() {
		OptionalInt result = OptionalInt.empty();
		if (getProperty(BiasCorrectionParameters.PROPERTY_ANTENNA_INDEX) != null) {
			result = OptionalInt.of(getIntProperty(BiasCorrectionParameters.PROPERTY_ANTENNA_INDEX));
		}
		return result;
	}

	/** Set antenna index */
	public void setAntennaIndex(Integer antennaIndex) {
		if (antennaIndex != null) {
			setIntProperty(BiasCorrectionParameters.PROPERTY_ANTENNA_INDEX, antennaIndex);
		} else {
			remove(BiasCorrectionParameters.PROPERTY_ANTENNA_INDEX);
		}
	}

	/** @return the Correction filename or OptionalInt.empty if none */
	public Optional<String> getCorrectionFilename() {
		return Optional.ofNullable(getProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME));

	}
}
