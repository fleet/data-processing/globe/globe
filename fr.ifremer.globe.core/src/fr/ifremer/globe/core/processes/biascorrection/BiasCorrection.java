package fr.ifremer.globe.core.processes.biascorrection;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.biascorrection.correctors.HeadingCorrector;
import fr.ifremer.globe.core.processes.biascorrection.correctors.MruHeadingCorrector;
import fr.ifremer.globe.core.processes.biascorrection.correctors.PitchCorrector;
import fr.ifremer.globe.core.processes.biascorrection.correctors.PlatformVerticalOffsetCorrector;
import fr.ifremer.globe.core.processes.biascorrection.correctors.RollCorrector;
import fr.ifremer.globe.core.processes.biascorrection.correctors.SoundVelocityCorrector;
import fr.ifremer.globe.core.processes.biascorrection.correctors.TimeCorrector;
import fr.ifremer.globe.core.processes.biascorrection.filereaders.CorrectionFileUtils;
import fr.ifremer.globe.core.processes.biascorrection.filereaders.CutFileReader;
import fr.ifremer.globe.core.processes.biascorrection.model.BiasCorrectionParameters;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.core.processes.biascorrection.model.SounderDataContainerProxy;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Process to correct bias of a sounder file.
 */
public class BiasCorrection {

	private Logger logger = LoggerFactory.getLogger(BiasCorrection.class);

	private List<ISounderNcInfo> inputFiles;

	// Progression weightings of tasks
	protected static final int WORK_LOAD = 10;
	protected static final int WORK_PROCESS = 100;
	protected static final int WORK_SAVE = 10;

	protected Set<Profile> timeProfileSet; // profile (time interval) from cut

	protected BiasCorrectionParameters m_parameters;
	protected TimeIntervalParameters m_timeItervalParameters;

	// Corrector
	HeadingCorrector headingCorrector = new HeadingCorrector();
	MruHeadingCorrector mruHeadingCorrector = new MruHeadingCorrector();
	PlatformVerticalOffsetCorrector verticalOffsetCorrector = new PlatformVerticalOffsetCorrector();
	PitchCorrector pitchCorrector = new PitchCorrector();
	RollCorrector rollCorrector = new RollCorrector();
	SoundVelocityCorrector velocityCorrector = new SoundVelocityCorrector();
	TimeCorrector attitudeTimeCorrector = new TimeCorrector(Optional.of(headingCorrector), Optional.of(rollCorrector),
			Optional.of(pitchCorrector), Optional.empty());
	TimeCorrector verticalOffsetTimeCorrector = new TimeCorrector(Optional.empty(), Optional.empty(), Optional.empty(),
			Optional.of(verticalOffsetCorrector));

	public static final String CIB_MPC_k_DATE = "date";
	// Bias types
	public static final String CIB_MPC_k_HEADING = "heading";
	public static final String CIB_MPC_k_MRU_HEADING = "mru_heading";
	public static final String CIB_MPC_k_ROLL = "roll";
	public static final String CIB_MPC_k_PITCH = "pitch";
	public static final String CIB_MPC_k_PLATFORM_VERTICAL_OFFSET = "platform_vertical_offset"; // old name (values are
																								// positive downwards)
	public static final String CIB_MPC_k_VERTICAL_OFFSET = "vertical_offset";
	public static final String CIB_MPC_k_TIME = "time";
	public static final String CIB_MPC_k_ATTITUDE_TIME = "attitude_time";
	public static final String CIB_MPC_k_VERTICAL_OFFSET_TIME = "vertical_offset_time";
	public static final String CIB_MPC_k_VELOCITY = "velocity";

	/** Constructor **/
	public BiasCorrection(String title) {
	}

	/** Constructor **/
	public BiasCorrection(BiasCorrectionParameters parameters, TimeIntervalParameters timeItervalParameters) {
		m_parameters = parameters;
		m_timeItervalParameters = timeItervalParameters;
	}

	/**
	 * Sets input files
	 */
	public void setInputFiles(List<ISounderNcInfo> inputFiles) {
		this.inputFiles = inputFiles;
	}

	/**
	 * Sets input files (with a file names) (used by tests)
	 * 
	 * @throws GIOException
	 */
	public void setFileNameList(List<String> fileNameList) throws GIOException {
		List<ISounderNcInfo> inputFilesAsInfo = new ArrayList<>();
		for (String fileName : fileNameList) {
			var sounderFileInfo = IFileService.grab().getFileInfo(fileName).filter(ISounderNcInfo.class::isInstance)
					.map(ISounderNcInfo.class::cast)
					.orElseThrow(() -> new GIOException("Error : can't read input file : " + fileName));
			inputFilesAsInfo.add(sounderFileInfo);
		}
		setInputFiles(inputFilesAsInfo);
	}

	/**
	 * Perform processing. Called by tests
	 */
	public IStatus applyForUnitaryTest(IProgressMonitor monitor, Logger logger) throws GException {
		this.logger = logger;

		monitor.setTaskName("Bias correction");
		monitor.beginTask("Bias correction running ...", inputFiles.size());

		// load all needed configuration files
		logger.info("Correction file : {}",
				m_parameters.getProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME));
		CorrectionList correctionPoints = loadConfiguration(monitor, logger, inputFiles.size());
		if (correctionPoints == null)
			throw new GIOException("No correction point to apply.");
		logger.info("Correction type : {}", correctionPoints.getType());
		logger.info("Correction points : {}", correctionPoints.getSize());

		for (ISounderNcInfo inputFile : inputFiles) {
			monitor.setTaskName("Processing: " + inputFile.getPath());
			logger.info("Processing:  {}", inputFile.getPath());
			try {
				// Process file
				processOnFile(monitor, inputFile, correctionPoints, m_parameters.getAntennaIndex());
			} catch (GIOException e) {
				logger.error("Error while processing file {}: {}", inputFile.getPath(), e.getMessage(), e);
			}

			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			monitor.worked(1);
		}
		return Status.OK_STATUS;

	}

	/**
	 * Load all needed configuration files
	 */
	public CorrectionList loadConfiguration(IProgressMonitor monitor, Logger logger, int numFilesToProcess)
			throws GIOException {
		this.logger = logger;
		CorrectionList correctionPoints = null;
		// Init progression.
		monitor.setTaskName("Bias correction");
		monitor.beginTask("Bias correction running ...", (WORK_LOAD + WORK_PROCESS + WORK_SAVE) * numFilesToProcess);

		// CORRECTION FILE PARAMETERS
		Optional<String> correctionFileName = m_parameters.getCorrectionFilename();
		// load all needed configuration files
		if (correctionFileName.isPresent()) {
			logger.info("Correction file : {}", correctionFileName.get());
			correctionPoints = CorrectionFileUtils.read(correctionFileName.get());
		}
		
		if (monitor.isCanceled())
			throw new OperationCanceledException();

		// TIME INTERVAL PARAMETERS
		if (m_timeItervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION)) {
			// cut file
			String cutFileName = m_timeItervalParameters.getProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME);
			try {
				timeProfileSet = CutFileReader.readCutFile(monitor, cutFileName);
			} catch (FileFormatException e1) {
				throw new GIOException("Error while processing file " + cutFileName, e1);
			}
		} else if (m_timeItervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION)) {
			// interval option
			timeProfileSet = new HashSet<>();
			long startDate = m_timeItervalParameters
					.getLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE);
			long endDate = m_timeItervalParameters.getLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE);
			timeProfileSet.add(new Profile(startDate, endDate));
		}

		return correctionPoints;
	}

	/**
	 * Process correction for a file
	 */
	public void processOnFile(IProgressMonitor monitor, ISounderNcInfo fileInfo, CorrectionList correctionPoints,
			OptionalInt antennaIndex) throws GIOException {

		// Bias correction process
		monitor.setTaskName("Processing : " + fileInfo.getFilename());
		monitor.subTask("Loading data ...");
		int processedSwath = 0;

		// CORRECTION FILE PARAMETERS
		Optional<String> correctionFileName = m_parameters.getCorrectionFilename();
		// Correctors list
		List<String> correctorsList = new ArrayList<String>();
		

		try (SounderDataContainerProxy dataProxy = new SounderDataContainerProxy(fileInfo)) {
			monitor.worked(WORK_LOAD);

			monitor.subTask("Data processing...");
			resetCorrectionCount();
			processedSwath = processSwath(monitor, dataProxy, correctionPoints, antennaIndex);
			logCorrectionCount();

			monitor.subTask("Saving data...");
			dataProxy.saveData(correctionFileName, getActiveCorrectorNameList());

			monitor.worked(WORK_SAVE);
		}

		logger.info("Number of processed cycles : {} / {} ", processedSwath, fileInfo.getCycleCount());
		monitor.subTask("Number of processed cycles : " + processedSwath + " on " + fileInfo.getCycleCount());
	}

	/**
	 * Apply correction for each swath
	 *
	 * @param dataProxy        data provider
	 * @param correctionPoints list of correction to apply
	 */
	public int processSwath(IProgressMonitor monitor, IDataProxy dataProxy, CorrectionList correctionPoints,
			OptionalInt antennaIndex) throws GIOException {

		Set<Profile> timeProfileContainingPingSet = new HashSet<>();

		monitor.subTask("Performing bias correction ... ");
		int workNb = (int) Math.ceil(dataProxy.getInfo().getCycleCount() / 100.0);
		// int swathNb = dataProxy.getPingBeamLimits().getSwathLenght();

		// Loop on swath
		int nbProcessedCycles = dataProxy.processSwath(iSwath -> {

			if (iSwath % workNb == 0) {
				monitor.worked(1);
			}

			Instant swathDate = DateUtils.epochNanoToInstant(dataProxy.getEpochTimeNano(iSwath));

			// Test if swath in in time interval
			if (!ProfileUtils.isPingInTimeInterval(swathDate, timeProfileSet, timeProfileContainingPingSet)) {
				return false;
			}

			// Time correction in first
			double attitudeTimeCorr = correctionPoints.getSuitableValue(swathDate, CorrectionPoint::getAttitudeTimeMs);
			attitudeTimeCorrector.apply(dataProxy, iSwath, attitudeTimeCorr, correctionPoints.getType(), antennaIndex);
			double verticalOffsetTimeCorr = correctionPoints.getSuitableValue(swathDate,
					CorrectionPoint::getVerticalOffsetTimeMs);
			verticalOffsetTimeCorrector.apply(dataProxy, iSwath, verticalOffsetTimeCorr, correctionPoints.getType(),
					antennaIndex);

			// Apply corrections
			double velocityCorr = correctionPoints.getSuitableValue(swathDate, CorrectionPoint::getVelocity);
			velocityCorrector.apply(dataProxy, iSwath, velocityCorr, correctionPoints.getType(), antennaIndex);

			double mruHeadingCorr = correctionPoints.getSuitableValue(swathDate, CorrectionPoint::getMruHeading);
			mruHeadingCorrector.apply(dataProxy, iSwath, mruHeadingCorr, correctionPoints.getType(), antennaIndex);

			double headingCorr = correctionPoints.getSuitableValue(swathDate, CorrectionPoint::getHeading);
			headingCorrector.apply(dataProxy, iSwath, headingCorr, correctionPoints.getType(), antennaIndex);

			double pitchCorr = correctionPoints.getSuitableValue(swathDate, CorrectionPoint::getPitch);
			pitchCorrector.apply(dataProxy, iSwath, pitchCorr, correctionPoints.getType(), antennaIndex);

			double rollCorr = correctionPoints.getSuitableValue(swathDate, CorrectionPoint::getRoll);
			rollCorrector.apply(dataProxy, iSwath, rollCorr, correctionPoints.getType(), antennaIndex);

			double heaveCorr = correctionPoints.getSuitableValue(swathDate, CorrectionPoint::getPlatformVerticalOffset);
			verticalOffsetCorrector.apply(dataProxy, iSwath, heaveCorr, correctionPoints.getType(), antennaIndex);

			// Log progression
			/*
			 * if (iAntenna == 0 && iSwath % (swathNb / 3) == 0) { int progression = (int)
			 * ((iSwath / (double) swathNb) * 100);
			 * logger.info("BiasCorrection processing {} : {} %",
			 * dataProxy.getInfo().getFilename(), progression); }
			 */

			return true;
		});

		logger.debug("BiasCorrection processed on {} ({} swaths) with {} correction point(s)  : ",
				dataProxy.getInfo().getFilename(), nbProcessedCycles, correctionPoints.getSize());
		correctionPoints.getCorrectionPointList().forEach(cp -> logger.debug(cp.toString()));
		return nbProcessedCycles + timeProfileContainingPingSet.size();
	}
	
	private List<String> getActiveCorrectorNameList() {
		List<String> correctorsList = new ArrayList<String>();
		if(velocityCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_VELOCITY);
		if(headingCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_HEADING);
		if(pitchCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_PITCH);
		if(rollCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_ROLL);
		if(verticalOffsetCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_VERTICAL_OFFSET);
		if(mruHeadingCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_MRU_HEADING);
		if(attitudeTimeCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_ATTITUDE_TIME);
		if(verticalOffsetTimeCorrector.getCorrectionCount() > 0)
			correctorsList.add(CIB_MPC_k_VERTICAL_OFFSET_TIME);
		return correctorsList;
	}

	/** set all nb of correction to 0 */
	private void resetCorrectionCount() {
		attitudeTimeCorrector.resetCorrectionCount();
		verticalOffsetTimeCorrector.resetCorrectionCount();
		headingCorrector.resetCorrectionCount();
		mruHeadingCorrector.resetCorrectionCount();
		rollCorrector.resetCorrectionCount();
		pitchCorrector.resetCorrectionCount();
		verticalOffsetCorrector.resetCorrectionCount();
		velocityCorrector.resetCorrectionCount();
	}

	private void logCorrectionCount() {
		logger.debug("{} corrections performed",
				headingCorrector.getCorrectionCount() + mruHeadingCorrector.getCorrectionCount()
						+ rollCorrector.getCorrectionCount() + pitchCorrector.getCorrectionCount()
						+ verticalOffsetCorrector.getCorrectionCount() + velocityCorrector.getCorrectionCount()
						+ attitudeTimeCorrector.getCorrectionCount()
						+ verticalOffsetTimeCorrector.getCorrectionCount());
	}

}