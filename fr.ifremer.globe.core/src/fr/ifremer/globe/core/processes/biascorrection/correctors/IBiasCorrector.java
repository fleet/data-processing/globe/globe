package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList.CorrectionType;
import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Provides methods to apply a bias or absolute correction.
 */
public interface IBiasCorrector {

	/**
	 * Apply a correction
	 *
	 * @param dataProxy : data provider
	 * @param iSwath : swath index
	 * @param corrType : type of correction (bias, absolute...)
	 * @param valueSupplier : value supplier (method to retrieve the correction value)
	 */
	default void apply(IDataProxy dataProxy, int iSwath, double value, CorrectionType corrType,
			OptionalInt antennaIndex) throws GIOException {
		if (!Double.isNaN(value)) {
			if (corrType == CorrectionType.BIAS && value != 0.0)
				applyBias(dataProxy, iSwath, value, antennaIndex);
			else if (corrType == CorrectionType.ABSOLUTE)
				applyNewValue(dataProxy, iSwath, value, antennaIndex);
		}
	}

	/**
	 * Apply a bias correction: computes bias value and calls applyNewValue method
	 */
	void applyBias(IDataProxy dataProxy, int swathIdx, double biasValue, OptionalInt antennaIndex) throws GIOException;

	/**
	 * Apply a new value (refresh all bound variables)
	 */
	void applyNewValue(IDataProxy dataProxy, int swathIdx, double newValue, OptionalInt antennaIndex)
			throws GIOException;

}
