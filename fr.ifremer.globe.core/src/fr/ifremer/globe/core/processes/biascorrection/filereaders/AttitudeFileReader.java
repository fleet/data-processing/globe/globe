package fr.ifremer.globe.core.processes.biascorrection.filereaders;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.exception.GIOException;

public class AttitudeFileReader {

	/** Private constructor **/
	private AttitudeFileReader() {
	}

	public static void readAttitudeFile(IProgressMonitor monitor, String attitudeFile
			/*List<Map<NamedDataStore, LoadedDateTimeStore>> attitudeMapList*/) throws GIOException {
		//FileInfoNode node = ModelRoot.getInstance().findInfoStore(attitudeFile, true);
		//if (node == null)
		//	throw new GIOException(" Error: file doesn't exist: \n" + attitudeFile);

		
		// TODO : recode a simple reader of attitude file !!
		
		
		//IFileInfo fileInfo = node.getFileInfo();
		/*TechsasInfoStore techsasInfo;
		if (infoStore instanceof TechsasInfoStore) {
			techsasInfo = (TechsasInfoStore) infoStore;
		} else {
			monitor.setTaskName("Error : can't read this file as an attitude file : \n" + attitudeFile);
			throw new FileFormatException("Error : can't read this file as an attitude file : \n" + attitudeFile);
		}
		TechsasDriver driver = (TechsasDriver) techsasInfo.getDriver();*

		List<NamedDataStore> dataStorebyType = driver.getDataStorebyType(techsasInfo, IDateTimeStore.class);
		Map<NamedDataStore, LoadedDateTimeStore> dateTimeStoreBasicMap = new HashMap<NamedDataStore, LoadedDateTimeStore>();
		for (NamedDataStore n : dataStorebyType) {
			LoadedDateTimeStore store = (LoadedDateTimeStore) driver.loadDataStore(n, techsasInfo, null);
			dateTimeStoreBasicMap.put(n, store);
		}

		if (dateTimeStoreBasicMap.size() == 0) {
			monitor.setTaskName("Error : attitude file must contains at least one data.");
			throw new FileFormatException("Error : attitude file must contains one data. " + attitudeFile);
		}

		if (dateTimeStoreBasicMap.entrySet().iterator().next().getValue().getDates().size() < 2) {
			monitor.setTaskName("Error : attitude file must contains at least two records.");
			throw new FileFormatException("Error : attitude file must contains at least two records. " + attitudeFile);
		}

		attitudeMapList.add(dateTimeStoreBasicMap);*/
	}

}
