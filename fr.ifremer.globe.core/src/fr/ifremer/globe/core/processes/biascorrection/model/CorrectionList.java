package fr.ifremer.globe.core.processes.biascorrection.model;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint.ICorrPointValueSupplier;

public class CorrectionList {

	/** Correction type **/
	public enum CorrectionType {
		BIAS("biasCorrection"), ABSOLUTE("absoluteCorrection");

		private String name;

		private CorrectionType(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

		/** @returns {@link CorrectionType} matching with the provided name **/
		public static CorrectionType get(String name) {
			for (CorrectionType t : values()) {
				if (t.name.equalsIgnoreCase(name))
					return t;
			}
			return null;
		}
	}

	/** Properties **/
	private List<CorrectionPoint> correctionPointList;
	private CorrectionType type;

	/** Constructor (by default, it's a {@link CorrectionType} BIAS correction) **/
	public CorrectionList() {
		this.correctionPointList = new ArrayList<>();
		this.type = CorrectionType.BIAS;
	}

	/** Constructor with {@link CorrectionType} **/
	public CorrectionList(CorrectionType type) {
		this.correctionPointList = new ArrayList<>();
		this.type = type;
	}

	/** correction type getter */
	public CorrectionType getType() {
		return this.type;
	}

	/** set correction type **/
	public void setType(CorrectionType type) {
		this.type = type;
	}

	/** Adds a correction point **/
	public void addCorrectionPoint(CorrectionPoint correctionPoint) {
		correctionPointList.add(correctionPoint);
	}

	/** Removes a correction point **/
	public void remove(CorrectionPoint correctionPoint) {
		correctionPointList.remove(correctionPoint);
	}

	public List<CorrectionPoint> getCorrectionPointList() {
		return this.correctionPointList;
	}

	public void setCorrectionPointList(List<CorrectionPoint> correctionPointList) {
		this.correctionPointList = correctionPointList;
	}

	public int getSize() {
		return this.correctionPointList.size();
	}

	/** @return correction point at the provided date, or null if does'nt exist **/
	public CorrectionPoint getCorrectionPoint(Instant date) {
		for (CorrectionPoint correctionPoint : this.correctionPointList) {
			if (correctionPoint.getDate().equals(date)) {
				return correctionPoint;
			}
		}
		return null;
	}

	/**
	 * @return a suitable correction value (if the list contains only one point: returns it, else: try to find a point
	 *         by interpolation)
	 **/
	public double getSuitableValue(Instant swathTime, ICorrPointValueSupplier valueSupplier) {

		// Case 1 : only one correction point, return its value
		if (correctionPointList.size() == 1)
			return valueSupplier.get(correctionPointList.get(0));

		// Case 2 : there is a list of correction point: get an interpolated correction value
		CorrectionPoint before = null;
		CorrectionPoint after = null;

		for (CorrectionPoint point : correctionPointList) {
			if (Double.isNaN(valueSupplier.get(point))) // keep only point which has a correction value
				continue;

			if (before != null && before.getDate().equals(point.getDate()))
				before = CorrectionPoint.concatenate(before, point);

			else if (after != null && after.getDate().equals(point.getDate()))
				after = CorrectionPoint.concatenate(after, point);

			else if (point.getDate().compareTo(swathTime) <= 0
					&& (before == null || (point.getDate().until(swathTime, ChronoUnit.NANOS) < before.getDate().until(swathTime, ChronoUnit.NANOS))))
				before = point;
			else if (point.getDate().isAfter(swathTime)
					&& (after == null || (swathTime.until(point.getDate(), ChronoUnit.NANOS) < swathTime.until(after.getDate(), ChronoUnit.NANOS))))
				after = point;
		}

		if (before != null && before.getDate().equals(swathTime)) // If a point exactly matches with the swath, returns it
			return valueSupplier.get(before);
		if (before != null && after != null) // Else try an interpolation
			return CorrectionPoint.getInterpolatedValue(valueSupplier, before, after, swathTime);
		else if (before != null) // Else return before or after if exist
			return valueSupplier.get(before);
		else if (after != null)
			return valueSupplier.get(after);

		return Double.NaN;

	}

	public void sortCorrectionPoints() {
		Collections.sort(this.correctionPointList);
	}

}
