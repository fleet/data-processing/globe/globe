package fr.ifremer.globe.core.processes.biascorrection.filereaders;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

import fr.ifremer.globe.core.processes.biascorrection.BiasCorrection;
import fr.ifremer.globe.core.processes.biascorrection.model.CommonCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList.CorrectionType;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility class to read and write correction files.
 */
public class CorrectionFileUtils {

	/** Constants **/
	public static final String EXTENSION = "*.cor";
	public static final String SEPARATOR = ";";

	/** Private constructor **/
	private CorrectionFileUtils() {
	}

	/**
	 * Reads a correction file.
	 */
	public static CorrectionList read(String filePath) throws GIOException {
		try (Scanner scanner = new Scanner(new File(filePath))) {
			// get correction type from first line
			CorrectionList result = new CorrectionList(CorrectionType.get(scanner.nextLine()));
			// read correction titles
			String[] titles = scanner.nextLine().split(SEPARATOR);

			while (scanner.hasNextLine()) {
				CommonCorrectionPoint correctionPoint = new CommonCorrectionPoint();
				String[] datas = scanner.nextLine().split(SEPARATOR);
				// set date
				correctionPoint.setDate(DateUtils.parseDate(datas[0]).toInstant());
				// set correction values
				for (int j = 1; j < titles.length; j++) {
					String title = titles[j];
					double value = Double.parseDouble(datas[j]);
					if (title.contains(BiasCorrection.CIB_MPC_k_ATTITUDE_TIME))
						correctionPoint.setAttitudeTimeMs(value);
					// be carefull of order to avoid confusion (vertical_offset contained in vertical_offset_time)
					else if (title.contains(BiasCorrection.CIB_MPC_k_VERTICAL_OFFSET_TIME))
						correctionPoint.setVerticalOffsetTimeMs(value);
					// be carefull of order to avoid confusion (heading contained in mru_heading)
					else if (title.contains(BiasCorrection.CIB_MPC_k_MRU_HEADING))
						correctionPoint.setMruHeading(value);
					else if (title.contains(BiasCorrection.CIB_MPC_k_HEADING))
						correctionPoint.setHeading(value);
					else if (title.contains(BiasCorrection.CIB_MPC_k_ROLL))
						correctionPoint.setRoll(value);
					else if (title.contains(BiasCorrection.CIB_MPC_k_PITCH))
						correctionPoint.setPitch(value);
					else if (title.contains(BiasCorrection.CIB_MPC_k_PLATFORM_VERTICAL_OFFSET)) {
						// old format detected : reverse correction sign
						correctionPoint.setPlatformVerticalOffset(-value);
					} else if (title.contains(BiasCorrection.CIB_MPC_k_VERTICAL_OFFSET))
						correctionPoint.setPlatformVerticalOffset(value);
					else if (title.contains(BiasCorrection.CIB_MPC_k_TIME)) {
						correctionPoint.setAttitudeTimeMs(value);
						correctionPoint.setVerticalOffsetTimeMs(value);
					} else if (title.contains(BiasCorrection.CIB_MPC_k_VELOCITY))
						correctionPoint.setVelocity(value);
					else {
						// Error: correction title not recognized
						String expectedColumns = BiasCorrection.CIB_MPC_k_VELOCITY + ", "
								+ BiasCorrection.CIB_MPC_k_HEADING + ", " + BiasCorrection.CIB_MPC_k_ROLL + ", "
								+ BiasCorrection.CIB_MPC_k_PITCH + ", " + BiasCorrection.CIB_MPC_k_VERTICAL_OFFSET
								+ ", " + BiasCorrection.CIB_MPC_k_MRU_HEADING + ", "
								+ BiasCorrection.CIB_MPC_k_ATTITUDE_TIME + ", "
								+ BiasCorrection.CIB_MPC_k_VERTICAL_OFFSET_TIME;
						throw new GIOException(String.format(
								"Correction file not valid, correction not recognized : %s.%nExpected : %s.", titles[j],
								expectedColumns));
					}
				}
				result.addCorrectionPoint(correctionPoint);
			}
			return result;
		} catch (FileNotFoundException e) {
			throw new GIOException("Correction file not found: " + filePath, e);
		} catch (NoSuchElementException e) {
			throw new GIOException("Error while reading correction file : " + e.getMessage(), e);
		}
	}

	/**
	 * Writes a correction file.
	 */
	public static boolean write(String outputFilePath, CorrectionList corrections) throws IOException {
		File file = new File(outputFilePath);
		boolean createNewFile = true;

		// check if file exists
		if (file.exists()) {
			String message = outputFilePath
					+ " already exists.\n\nDo you want to add your corrections to this file or create a new one?";
			// int returnCode = Messages.openSyncQuestionMessage("Warning : file already exists", message,
			// "Add correction(s) to existing file", "Overwrite");
			// if (returnCode == -1) // click on close button : cancel this write operation
			// return false;
			// createNewFile = returnCode == 1;
		}

		try (Writer writer = new FileWriter(file, !createNewFile)) {
			if (createNewFile) {
				// correction type
				writer.append(corrections.getType().toString()).append("\n");
				// correction titles
				writer.append(BiasCorrection.CIB_MPC_k_DATE).append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_VELOCITY + "(m/s)").append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_HEADING + "(°)").append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_ROLL + "(°)").append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_PITCH + "(°)").append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_VERTICAL_OFFSET + "(m)").append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_MRU_HEADING + "(°)").append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_ATTITUDE_TIME + "(ms)").append(SEPARATOR)
						.append(BiasCorrection.CIB_MPC_k_VERTICAL_OFFSET_TIME + "(ms)").append("\n");
			}

			// correction point lines
			for (CorrectionPoint cp : corrections.getCorrectionPointList()) {
				writer.append(String.valueOf(DateUtils.formatDate(Date.from(cp.getDate())))).append(SEPARATOR)
						.append(String.valueOf(cp.getVelocity())).append(SEPARATOR)
						.append(String.valueOf(cp.getHeading())).append(SEPARATOR).append(String.valueOf(cp.getRoll()))
						.append(SEPARATOR).append(String.valueOf(cp.getPitch())).append(SEPARATOR)
						.append(String.valueOf(cp.getPlatformVerticalOffset())).append(SEPARATOR)
						.append(String.valueOf(cp.getMruHeading())).append(SEPARATOR)
						.append(String.valueOf(cp.getAttitudeTimeMs())).append(SEPARATOR)
						.append(String.valueOf(cp.getVerticalOffsetTimeMs())).append("\n");
			}
			writer.flush();
			return true;
		}
	}
}
