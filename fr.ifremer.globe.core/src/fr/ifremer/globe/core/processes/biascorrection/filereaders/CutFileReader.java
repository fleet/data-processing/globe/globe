package fr.ifremer.globe.core.processes.biascorrection.filereaders;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.utils.exception.FileFormatException;

public class CutFileReader {

	/** Private constructor **/
	private CutFileReader() {
	}

	/**
	 * Reads a cute file and returns a time profile set.
	 */
	public static Set<Profile> readCutFile(IProgressMonitor monitor, String cutFileName) throws FileFormatException {
		File cutFile = new File(cutFileName);
		HashSet<Profile> timeProfileSet = new HashSet<>();
		try {
			timeProfileSet.addAll(ProfileUtils.loadProfilesFromFile(cutFile));
		} catch (Exception e) {
			monitor.setTaskName("Error : can't read this file as a cut file : \n" + cutFileName);
			throw new FileFormatException("Error : can't read this file as a cut file : \n" + cutFileName);
		}
		return timeProfileSet;
	}

}
