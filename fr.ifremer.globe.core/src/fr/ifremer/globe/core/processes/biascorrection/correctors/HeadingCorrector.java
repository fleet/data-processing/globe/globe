package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

public class HeadingCorrector implements IBiasCorrector {

	/** Nb of correction applied */
	int correctionCount = 0;

	@Override
	public void applyBias(IDataProxy dataProxy, int swathIndex, double biasValue, OptionalInt antennaIndex)
			throws GIOException {
		double heading = dataProxy.getHeading(swathIndex) + biasValue;
		applyNewValue(dataProxy, swathIndex, heading, antennaIndex);
	}

	@Override
	public void applyNewValue(IDataProxy dataProxy, int swathIdx, double newValue, OptionalInt antennaIndex)
			throws GIOException {
		// Get angle between 0 and 360 degrees
		if (newValue < 0. || newValue > 360)
			newValue = newValue < 0. ? newValue % 360 + 360 : newValue % 360;

		dataProxy.setHeading(swathIdx, newValue);
		correctionCount++;
	}

	/** @return the nb of correction applied */
	public int getCorrectionCount() {
		return correctionCount;
	}

	/** set the nb of correction to 0 */
	public void resetCorrectionCount() {
		correctionCount = 0;
	}

}