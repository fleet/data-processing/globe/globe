package fr.ifremer.globe.core.processes.biascorrection.model;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * {@link IDataProxy} implementation for the {@link SounderDataContainer}.
 */
public class SounderDataContainerProxy extends ASounderDataContainerProxy
		implements AutoCloseable, IDataContainerOwner {

	/** Properties **/
	private final ISounderDataContainerToken sounderDataContainerToken;
	private final SounderDataContainer dataContainer;
	private final SwathBeamLimits limits;

	/**
	 * Constructor: books a {@link SounderDataContainer} for the specified info
	 *
	 * @throws GIOException
	 * @throws BadParameterException
	 */
	public SounderDataContainerProxy(ISounderNcInfo info) throws GIOException {
		sounderDataContainerToken = IDataContainerFactory.grab().book(info, this);
		dataContainer = sounderDataContainerToken.getDataContainer();
		initLayers(dataContainer);
		limits = new SwathBeamLimits(0, dataContainer.getInfo().getCycleCount() - 1, 0,
				dataContainer.getInfo().getTotalRxBeamCount() - 1);
	}

	@Override
	public void close() {
		sounderDataContainerToken.close();
		dispose();
	}

	/**
	 * Flushes data to the file
	 */
	public void saveData(Optional<String> correctionFileName, List<String> correctorsList) throws GIOException {
		dataContainer.getCsLayers(CoordinateSystem.ACS).flush();
		dataContainer.flush();
		sounderDataContainerToken.close(); // dataContainer need to be closed before MbgDriver writes properties

		Optional<ProcessAssessor> processAssessor = getInfo().getProcessAssessor();
		if (processAssessor.isPresent()) {
			IFileService.grab().updateStatistics(getInfo());
			processAssessor.get().heedBiasCorrection(correctionFileName, correctorsList);
		}
	}

	@Override
	public ISounderNcInfo getInfo() {
		return dataContainer.getInfo();
	}

	@Override
	public SwathBeamLimits getPingBeamLimits() {
		return limits;
	}

	/**********************************************************************************************************
	 * Getters
	 **********************************************************************************************************/

	@Override
	public long getEpochTimeNano(int swathIdx) throws GIOException {
		return timeLayer.get(swathIdx);
	}

	@Override
	public double getHeading(int swathIdx) throws GIOException {
		return swathHeadingLayer.get(swathIdx);
	}

	@Override
	public double getRoll(int swathIdx) throws GIOException {
		return swathRollLayer.get(swathIdx);
	}

	@Override
	public double getPitch(int swathIdx) throws GIOException {
		return swathPitchLayer.get(swathIdx);
	}

	@Override
	public double getPlatformVerticalOffset(int swathIdx) throws GIOException {
		return swathPlatformVerticalOffsetLayer.get(swathIdx);
	}

	@Override
	public double getTxZDepth(int swathIdx) throws GIOException {
		return txZDepth.get(swathIdx);
	}

	@Override
	public double getSoundVelocity(int swathIdx) throws GIOException {
		return soundSpeedLayer.get(swathIdx);
	}

	@Override
	public double getDepth(int swathIdx, int beamIdx) throws GIOException {
		return depthLayer.get(swathIdx, beamIdx);
	}

	@Override
	public double getAlongDistance(int swathIdx, int beamIdx) throws GIOException {
		return alongDistLayer.get(swathIdx, beamIdx);
	}

	@Override
	public double getAcrossDistance(int swathIdx, int beamIdx) throws GIOException {
		return acrossDistLayer.get(swathIdx, beamIdx);
	}

	@Override
	public double getAzimutAngle(int swathIdx, int beamIdx) throws GIOException {
		return alongAngleLayer.isPresent() ? alongAngleLayer.get().get(swathIdx, beamIdx) : Double.NaN;
	}

	@Override
	public double getAcrossAngle(int swathIdx, int beamIdx) throws GIOException {
		return acrossAngleLayer.map(layer -> layer.get(swathIdx, beamIdx))//
				.orElse(-beamPointingAngleLayerInScs.map(layer -> (double) layer.get(swathIdx, beamIdx))//
						.orElse(Double.NaN));
	}

	@Override
	public short getAntennaIndex(int swathIdx, int beamIdx) throws GIOException {
		return antennaIndexLayer.get(swathIdx, beamIdx);
	}

	/**********************************************************************************************************
	 * Setters
	 **********************************************************************************************************/

	@Override
	public void setEpochTimeNano(int swathIdx, long pingTime) throws GIOException {
		timeLayer.set(swathIdx, pingTime);
	}

	@Override
	public void setHeading(int swathIdx, double heading) throws GIOException {
		swathHeadingLayer.set(swathIdx, (float) heading);
	}

	@Override
	public void setRoll(int swathIdx, double roll) throws GIOException {
		swathRollLayer.set(swathIdx, (float) roll);
	}

	@Override
	public void setPitch(int swathIdx, double pitch) throws GIOException {
		swathPitchLayer.set(swathIdx, (float) pitch);
	}

	@Override
	public void setPlatformVerticalOffset(int swathIdx, double heave) throws GIOException {
		swathPlatformVerticalOffsetLayer.set(swathIdx, (float) heave);
	}

	@Override
	public void setTxZDepth(int swathIdx, double immersion) throws GIOException {
		txZDepth.set(swathIdx, (float) immersion);
	}

	@Override
	public void setSoundVelocity(int swathIdx, float soundVelocity) throws GIOException {
		soundSpeedLayer.set(swathIdx, soundVelocity);
	}

	@Override
	public void setDepth(int swathIdx, int beamIndex, double depth) throws GIOException {
		depthLayer.set(swathIdx, beamIndex, depth);
	}

	@Override
	public void setAlongDistance(int swathIdx, int beamIndex, double allongDistance) throws GIOException {
		alongDistLayer.set(swathIdx, beamIndex, (float) allongDistance);
	}

	@Override
	public void setAzimutAngle(int swathIdx, int beamIndex, double azimutAngle) throws GIOException {
		if (alongAngleLayer.isPresent())
			alongAngleLayer.get().set(swathIdx, beamIndex, (float) azimutAngle);
	}

	@Override
	public void setAcrossDistance(int swathIdx, int beamIndex, double acrossDistance) throws GIOException {
		acrossDistLayer.set(swathIdx, beamIndex, (float) acrossDistance);
	}

	@Override
	public void setAcrossAngle(int swathIdx, int beamIndex, double acrossAngle) throws GIOException {
		if (acrossAngleLayer.isPresent())
			acrossAngleLayer.get().set(swathIdx, beamIndex, acrossAngle);
		else if (beamPointingAngleLayerInScs.isPresent())
			beamPointingAngleLayerInScs.get().set(swathIdx, beamIndex, (float) -acrossAngle);
	}

}
