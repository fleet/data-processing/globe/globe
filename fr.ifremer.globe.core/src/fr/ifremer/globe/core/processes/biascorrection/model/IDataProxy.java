package fr.ifremer.globe.core.processes.biascorrection.model;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Provides getters and setters for data used by the bias correction.
 */
public interface IDataProxy {

	public ISounderNcInfo getInfo();

	public SwathBeamLimits getPingBeamLimits();

	/** Functional interface to apply a modification for each beam **/
	@FunctionalInterface
	public interface ISwathProccessor {
		public boolean process(int swathIdx) throws GIOException;
	}

	/**
	 * Processes swath
	 *
	 * @throws GIOException
	 */
	public default int processSwath(ISwathProccessor swathProcessor) throws GIOException {
		int swathLength = getPingBeamLimits().getSwathLenght();
		int swathProcessedNb = 0;
		for (int swathIdx = 0; swathIdx < swathLength; swathIdx++) {
			if (swathProcessor.process(swathIdx))
				swathProcessedNb++;
		}
		return swathProcessedNb;
	}

	/** Functional interface to apply a modification for each beam **/
	@FunctionalInterface
	public interface ISoundingProccessor {
		public void process(int beamIdx) throws GIOException;
	}

	/** Functional interface to apply a modification for each beam **/
	@FunctionalInterface
	public interface ISoundingIndexesConsumer {
		public void process(int swathIdx, int beamIdx) throws GIOException;
	}

	/**
	 * Processes each soundings for a specified swath and antenna
	 */
	public default void processSounding(ISoundingProccessor soundingProcessor) throws GIOException {
		int beamLength = getPingBeamLimits().getBeamLenght();
		for (int beamIdx = 0; beamIdx < beamLength; beamIdx++)
			soundingProcessor.process(beamIdx);
	}

	public default void processSounding(ISoundingIndexesConsumer soudingIndexesConsumer) throws GIOException {
		for (int swathIdx = 0; swathIdx < getPingBeamLimits().getSwathLenght(); swathIdx++) {
			for (int beamIdx = 0; beamIdx < getPingBeamLimits().getBeamLenght(); beamIdx++)
				soudingIndexesConsumer.process(swathIdx, beamIdx);
		}
	}

	// Getters
	public long getEpochTimeNano(int swathIdx) throws GIOException;

	public double getHeading(int swathIdx) throws GIOException;

	public double getRoll(int swathIdx) throws GIOException;

	public double getPitch(int swathIdx) throws GIOException;

	public double getPlatformVerticalOffset(int swathIdx) throws GIOException;

	public double getTxZDepth(int swathIdx) throws GIOException;

	public double getDepth(int swathIdx, int beamIdx) throws GIOException;

	public double getAlongDistance(int swathIdx, int beamIdx) throws GIOException;

	public double getAcrossDistance(int swathIdx, int beamIdx) throws GIOException;

	public double getAzimutAngle(int swathIdx, int beamIdx) throws GIOException;

	public double getAcrossAngle(int swathIdx, int beamIdx) throws GIOException;

	public double getSoundVelocity(int swathIdx) throws GIOException;

	public short getAntennaIndex(int swathIdx, int beamIdx) throws GIOException;
	
	// getters with time
	public boolean hasAttitudeInterpolator();

	public double getInterpolatedHeading(long epochTimeNano) throws GIOException;

	public double getInterpolatedRoll(long epochTimeNano) throws GIOException;

	public double getInterpolatedPitch(long epochTimeNano) throws GIOException;

	public double getInterpolatedVerticalOffset(long epochTimeNano) throws GIOException;


	// Setters
	public void setEpochTimeNano(int swathIdx, long pingTime) throws GIOException;

	public void setHeading(int swathIdx, double heading) throws GIOException;

	public void setRoll(int swathIdx, double roll) throws GIOException;

	public void setPitch(int swathIdx, double pitch) throws GIOException;

	public void setPlatformVerticalOffset(int swathIdx, double heave) throws GIOException;

	public void setTxZDepth(int swathIdx, double immersion) throws GIOException;

	public void setSoundVelocity(int swathIdx, float soundVelocity) throws GIOException;

	public void setDepth(int swathIdx, int beamIdx, double depth) throws GIOException;

	public void setAlongDistance(int swathIdx, int beamIdx, double allongDistance) throws GIOException;

	public void setAzimutAngle(int swathIdx, int beamIdx, double azimutAngle) throws GIOException;

	public void setAcrossDistance(int swathIdx, int beamIdx, double acrossDistance) throws GIOException;

	public void setAcrossAngle(int swathIdx, int beamIdx, double acrossAngle) throws GIOException;

}