package fr.ifremer.globe.core.processes.biascorrection.correctors;

import java.util.OptionalInt;

import fr.ifremer.globe.core.processes.biascorrection.model.IDataProxy;
import fr.ifremer.globe.utils.exception.GIOException;

public class SoundVelocityCorrector implements IBiasCorrector {

	/** Nb of correction applied */
	int correctionCount = 0;

	@Override
	public void applyBias(IDataProxy dataProxy, int swathIdx, double biasValue, OptionalInt antennaIndex)
			throws GIOException {
		double pingVelocity = dataProxy.getSoundVelocity(swathIdx);

		dataProxy.processSounding(beamIdx -> {
			if (antennaIndex.isEmpty() || antennaIndex.getAsInt() == dataProxy.getAntennaIndex(swathIdx, beamIdx)) {

				double depth = dataProxy.getDepth(swathIdx, beamIdx);

				double acrossAngle = dataProxy.getAcrossAngle(swathIdx, beamIdx);

				double tan = !Double.isNaN(acrossAngle) ? Math.tan(Math.toRadians(acrossAngle))
						: dataProxy.getAcrossDistance(swathIdx, beamIdx) / depth;

				if (pingVelocity != 0.0) {
					double depthDeviation = depth * tan * tan * biasValue / pingVelocity;
					depth = depth - depthDeviation;
				}

				dataProxy.setDepth(swathIdx, beamIdx, depth);

				correctionCount++;
			}
		});
	}

	@Override
	public void applyNewValue(IDataProxy dataProxy, int swathIdx, double newValue, OptionalInt antennaIndex)
			throws GIOException {
		/**
		 * There is no velocity variable in data model, it 's only possible to apply a bias correction which modifies
		 * the depth values.
		 */
		throw new GIOException("Unable to update velocity: there is no velocity variable in data model");
	}

	/** @return the nb of correction applied */
	public int getCorrectionCount() {
		return correctionCount;
	}

	/** set the nb of correction to 0 */
	public void resetCorrectionCount() {
		correctionCount = 0;
	}

}
