package fr.ifremer.globe.core.processes.biascorrection.model;

public class FileCorrectionPoint extends CorrectionPoint {
	private String fileName;

	public FileCorrectionPoint(String fileName) {
		super();
		this.fileName = fileName;
	}

	public FileCorrectionPoint(FileCorrectionPoint other) {
		super(other);
		this.fileName = other.fileName;
	}
	
	public FileCorrectionPoint(CorrectionPoint other, String filename) {
		super(other);
		this.fileName = filename;
	}

	public String getFileName() {
		return fileName;
	}

	@Override
	public void accept(ICorrectionPointVisitor visitor) {
		visitor.visit(this);
	}
}
