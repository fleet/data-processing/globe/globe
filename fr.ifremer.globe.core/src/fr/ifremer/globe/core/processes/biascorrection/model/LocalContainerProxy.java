package fr.ifremer.globe.core.processes.biascorrection.model;

import java.util.List;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.mbes.MbesUtils;

/**
 * This class is a {@link IDataProxy} for a local container whose data come from a section of a global
 * {@link SounderDataContainer}. This section is delimited by {@link SwathBeamLimits}.
 */
public class LocalContainerProxy extends ASounderDataContainerProxy {

	/** Properties **/
	private final ISounderNcInfo info;
	private final SwathBeamLimits limits;

	/** Needed layers **/
	protected DoubleLoadableLayer1D swathLongitudeLayer;
	protected DoubleLoadableLayer1D swathLatitudeLayer;

	/** Local arrays **/
	// dim: swath
	private long[] time;
	private float[] immersionArray;
	private float[] swathHeadingArray;
	private float[] swathRoll;
	private float[] swathPitch;
	private float[] swathPlatformVerticalOffset;
	private double[] soundSpeed;
	// dim: swath * beam
	private double[][] depth;
	private double[][] alongDistArray;
	private double[][] acrossDistArray;
	private double[][] acrossAngle;
	private double[][] alongAngle;
	private short[][] antennaIndex;

	private List<IBiasSounding> beamtToProcessList;
	private double antennaXOffset;
	private double antennaYOffset;

	/**
	 * Constructor
	 *
	 * @param dataContainer {@link SounderDataContainer} data source
	 * @param limits limits of this data model
	 * @param soundingList reference to beams which need to be updated
	 */
	public LocalContainerProxy(SounderDataContainer dataContainer, SwathBeamLimits limits,
			List<IBiasSounding> soundingList) throws GIOException {

		initLayers(dataContainer);

		this.limits = limits;
		beamtToProcessList = soundingList;
		info = dataContainer.getInfo();
		antennaXOffset = info.getTxParameters().xoffset;
		antennaYOffset = info.getTxParameters().yoffset;
	}

	@Override
	protected void initLayers(SounderDataContainer dataContainer) throws GIOException {
		super.initLayers(dataContainer);
		swathLongitudeLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);
		swathLatitudeLayer = dataContainer.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
	}

	@Override
	public ISounderNcInfo getInfo() {
		return info;
	}

	@Override
	public SwathBeamLimits getPingBeamLimits() {
		return limits;
	}

	/**
	 * Start process: fill local arrays
	 */
	public void startProcess() throws GIOException {
		int swathCapacity = limits.getSwathLenght();
		int beamCapacity = limits.getBeamLenght();

		// initialize arrays
		time = new long[swathCapacity];
		soundSpeed = new double[swathCapacity];
		immersionArray = new float[swathCapacity];
		swathHeadingArray = new float[swathCapacity];
		swathRoll = new float[swathCapacity];
		swathPitch = new float[swathCapacity];
		swathPlatformVerticalOffset = new float[swathCapacity];
		depth = new double[swathCapacity][beamCapacity];
		alongDistArray = new double[swathCapacity][beamCapacity];
		acrossDistArray = new double[swathCapacity][beamCapacity];
		acrossAngle = new double[swathCapacity][beamCapacity];
		alongAngle = new double[swathCapacity][beamCapacity];
		antennaIndex = new short[swathCapacity][beamCapacity];

		// fill arrays
		for (int swathIdx = 0; swathIdx < swathCapacity; swathIdx++) {
			int layerSwathIdx = limits.getSwathMin() + swathIdx;

			for (int beamIdx = 0; beamIdx < beamCapacity; beamIdx++) {
				int layerBeamIdx = limits.getBeamMin() + beamIdx;
				depth[swathIdx][beamIdx] = depthLayer.get(layerSwathIdx, layerBeamIdx);
				alongDistArray[swathIdx][beamIdx] = alongDistLayer.get(layerSwathIdx, layerBeamIdx);
				acrossDistArray[swathIdx][beamIdx] = acrossDistLayer.get(layerSwathIdx, layerBeamIdx);
				if (acrossAngleLayer.isPresent()) { // MBG
					acrossAngle[swathIdx][beamIdx] = acrossAngleLayer.get().get(layerSwathIdx, layerBeamIdx);
				} else if (beamPointingAngleLayerInScs.isPresent()) { // XSF
					acrossAngle[swathIdx][beamIdx] = -beamPointingAngleLayerInScs.get().get(layerSwathIdx, layerBeamIdx);
				}
				if (alongAngleLayer.isPresent())
					alongAngle[swathIdx][beamIdx] = alongAngleLayer.get().get(layerSwathIdx, layerBeamIdx);
				antennaIndex[swathIdx][beamIdx] = antennaIndexLayer.get(layerSwathIdx, layerBeamIdx);

			}

			time[swathIdx] = timeLayer.get(layerSwathIdx);
			soundSpeed[swathIdx] = soundSpeedLayer.get(layerSwathIdx);
			immersionArray[swathIdx] = txZDepth.get(layerSwathIdx);
			swathHeadingArray[swathIdx] = swathHeadingLayer.get(layerSwathIdx);
			swathRoll[swathIdx] = swathRollLayer.get(layerSwathIdx);
			swathPitch[swathIdx] = swathPitchLayer.get(layerSwathIdx);
			swathPlatformVerticalOffset[swathIdx] = swathPlatformVerticalOffsetLayer.get(layerSwathIdx);
		}

	}

	/**
	 * End process method: updates beam position in Swath Editor
	 */
	public void endProcess() throws GIOException {
		for (IBiasSounding beam : beamtToProcessList) {
			int iSwath = beam.getSwathNumber();
			int iLocalSwath = iSwath - limits.getSwathMin();
			int iLocalBeam = beam.getBeamNumber() - limits.getBeamMin();

			double lat = swathLatitudeLayer.get(iSwath);
			double lon = swathLongitudeLayer.get(iSwath);

			// iSwath / iBeam start at 0 in local arrays
			double heading = swathHeadingArray[iLocalSwath];
			double alongDist = alongDistArray[iLocalSwath][iLocalBeam] + antennaXOffset;
			double acrossDist = acrossDistArray[iLocalSwath][iLocalBeam] + antennaYOffset;
			double depthAcs = depth[iLocalSwath][iLocalBeam];

			Ellipsoid ellipsoid = getInfo().getEllipsoid();

			double[] normRayon = MbesUtils.calcNormRayon(lat, ellipsoid);
			double norm = normRayon[0];
			double r = normRayon[1];

			double sinHead = Math.sin(Math.toRadians(heading));
			double cosHead = Math.cos(Math.toRadians(heading));

			double[] latLon = MbesUtils.acrossAlongToLatitudeLongitude(lon, lat, alongDist, acrossDist, r, sinHead,
					cosHead, norm);

			// Get depth in FCS
			double depthFCS = -coordinateSystemLayers.zTranslateACStoFCS(iSwath, depthAcs, immersionArray[iLocalSwath]);

			// Update swath editor soudings
			beam.setGeographicalCoordinates(latLon[1], latLon[0], depthFCS);
		}
	}

	/**********************************************************************************************************
	 * Getters
	 **********************************************************************************************************/

	@Override
	public long getEpochTimeNano(int swathIdx) throws GIOException {
		return time[swathIdx];
	}

	@Override
	public double getHeading(int swathIdx) throws GIOException {
		return swathHeadingArray[swathIdx];
	}

	@Override
	public double getRoll(int swathIdx) throws GIOException {
		return swathRoll[swathIdx];
	}

	@Override
	public double getPitch(int swathIdx) throws GIOException {
		return swathPitch[swathIdx];
	}

	@Override
	public double getPlatformVerticalOffset(int swathIdx) throws GIOException {
		return swathPlatformVerticalOffset[swathIdx];
	}

	@Override
	public double getTxZDepth(int swathIdx) throws GIOException {
		return immersionArray[swathIdx];
	}

	@Override
	public double getSoundVelocity(int swathIdx) throws GIOException {
		return soundSpeed[swathIdx];
	}

	@Override
	public double getDepth(int swathIdx, int beamIdx) throws GIOException {
		return depth[swathIdx][beamIdx];
	}

	@Override
	public double getAlongDistance(int swathIdx, int beamIdx) throws GIOException {
		return alongDistArray[swathIdx][beamIdx];
	}

	@Override
	public double getAcrossDistance(int swathIdx, int beamIdx) throws GIOException {
		return acrossDistArray[swathIdx][beamIdx];
	}

	@Override
	public double getAzimutAngle(int swathIdx, int beamIdx) throws GIOException {
		return alongAngleLayer.isPresent() ? alongAngle[swathIdx][beamIdx] : Double.NaN;
	}

	@Override
	public double getAcrossAngle(int swathIdx, int beamIdx) throws GIOException {
		return acrossAngleLayer.isPresent() || beamPointingAngleLayerInScs.isPresent() ? acrossAngle[swathIdx][beamIdx]
				: Double.NaN;
	}

	@Override
	public short getAntennaIndex(int swathIdx, int beamIdx) throws GIOException {
		return antennaIndex[swathIdx][beamIdx];
	}

	/**********************************************************************************************************
	 * Setters
	 **********************************************************************************************************/

	@Override
	public void setEpochTimeNano(int swathIdx, long value) throws GIOException {
		time[swathIdx] = value;
	}

	@Override
	public void setHeading(int swathIdx, double value) throws GIOException {
		swathHeadingArray[swathIdx] = (float) value;
	}

	@Override
	public void setRoll(int swathIdx, double value) throws GIOException {
		swathRoll[swathIdx] = (float) value;
	}

	@Override
	public void setPitch(int swathIdx, double value) throws GIOException {
		swathPitch[swathIdx] = (float) value;
	}

	@Override
	public void setPlatformVerticalOffset(int swathIdx, double value) throws GIOException {
		swathPlatformVerticalOffset[swathIdx] = (float) value;
	}

	@Override
	public void setTxZDepth(int swathIdx, double value) throws GIOException {
		immersionArray[swathIdx] = (float) value;
	}

	@Override
	public void setSoundVelocity(int swathIdx, float value) throws GIOException {
		soundSpeed[swathIdx] = value;
	}

	@Override
	public void setDepth(int swathIdx, int beamIdx, double value) throws GIOException {
		depth[swathIdx][beamIdx] = value;
	}

	@Override
	public void setAlongDistance(int swathIdx, int beamIdx, double value) throws GIOException {
		alongDistArray[swathIdx][beamIdx] = (float) value;
	}

	@Override
	public void setAcrossDistance(int swathIdx, int beamIdx, double value) throws GIOException {
		acrossDistArray[swathIdx][beamIdx] = (float) value;
	}

	@Override
	public void setAzimutAngle(int swathIdx, int beamIdx, double value) throws GIOException {
		alongAngle[swathIdx][beamIdx] = (float) value;
	}

	@Override
	public void setAcrossAngle(int swathIdx, int beamIdx, double value) throws GIOException {
		acrossAngle[swathIdx][beamIdx] = (float) value;
	}
}
