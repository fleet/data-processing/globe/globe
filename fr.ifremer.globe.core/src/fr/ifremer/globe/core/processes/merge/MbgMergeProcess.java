package fr.ifremer.globe.core.processes.merge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.math.IntRange;
import org.apache.commons.lang.math.LongRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.processes.merge.netcdf.NetcdfMerge;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Version;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to merge MBG files.
 */
public class MbgMergeProcess implements IMergeProcess, IDataContainerOwner {

	private static final Logger STATIC_LOGGER = LoggerFactory.getLogger(MbgMergeProcess.class);

	/** Properties **/
	private final List<MbgInfo> infoStores;
	private String outputFilePath;
	private Optional<LongRange> optTimeInterval = Optional.empty();
	private Logger logger = STATIC_LOGGER;

	/**
	 * Constructor : parameters initialization.
	 */
	public MbgMergeProcess(List<ISounderNcInfo> infoStores) {
		this.infoStores = infoStores.stream().//
				filter(MbgInfo.class::isInstance).//
				map(MbgInfo.class::cast).//
				collect(Collectors.toList());
	}

	@Override
	public String getName() {
		return "cut/merge process";
	}

	/**
	 * Sets the logger
	 */
	@Override
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * Runs the process.
	 */
	@Override
	public void run(IProgressMonitor monitor) throws NCException, IOException, GIOException, BadParameterException {
		List<ISounderDataContainerToken> containerTokens = new ArrayList<>();
		LinkedHashMap<NetcdfFile, Map<NCDimension, IntRange>> inputFilesMap = new LinkedHashMap<>();
		try {
			// Check input files
			String errorMsg = checkInputFiles(infoStores);
			if (!errorMsg.isEmpty()) {
				throw new BadParameterException(errorMsg);
			}

			// check ouput file
			if (infoStores.stream().anyMatch(info -> info.getFilePath().equals(outputFilePath))) {
				throw new BadParameterException("output file can't be one of the input file.");
			}

			// Setup input files map
			for (MbgInfo info : infoStores) {
				// Book container
				ISounderDataContainerToken token = IDataContainerFactory.grab().book(info, this);
				containerTokens.add(token);
				NetcdfFile netcdfFile = info.getFile();
				if (optTimeInterval.isPresent()) {
					Optional<IntRange> swathIndexInterval = getSwathDimensionInTimeInterval(token.getDataContainer(),
							optTimeInterval.get());
					if (swathIndexInterval.isPresent()) {
						Map<NCDimension, IntRange> dimensionSections = new HashMap<>();
						dimensionSections.put(netcdfFile.getDimension("mbCycleNbr"), swathIndexInterval.get());
						inputFilesMap.put(netcdfFile, dimensionSections);
					} else {
						STATIC_LOGGER.debug("File {} not inside the specified time interval : ignored.",
								info.getPath());
					}
				} else {
					inputFilesMap.put(netcdfFile, new HashMap<>());
				}
			}

			if (optTimeInterval.isPresent() && inputFilesMap.isEmpty()) {
				logger.info("No file is time interval.");
				return;
			}

			// Do the merge
			try (NCFile outputFile = NCFile.createNew(Version.netcdf4, outputFilePath).withCompressionDisabled()) {
				NetcdfMerge merge = new NetcdfMerge(inputFilesMap, outputFile);
				merge.setLogger(logger);
				merge.run(monitor);
			}

			// Update mbg output statistics
			updateGlobalAttributes(new MbgInfo(outputFilePath));

		} finally {
			// close opened containers (->files)
			containerTokens.forEach(ISounderDataContainerToken::close);
		}
	}

	/**
	 * Checks input files
	 *
	 * WARNING : INPUT FILES ARE SORTED
	 */
	public static <I extends IFileInfo> String checkInputFiles(List<I> infoStores) {
		@SuppressWarnings("unchecked") // checked by wizard
		List<MbgInfo> mbgInfos = (List<MbgInfo>) infoStores;

		StringBuilder errorMessage = new StringBuilder();
		// sort files
		mbgInfos.sort((mbgInfo1, mbgInfo2) -> mbgInfo1.getFirstPingDate().compareTo(mbgInfo2.getFirstPingDate()));
		// check
		MbgInfo mbgInfo1 = null;
		for (MbgInfo mbgInfo2 : mbgInfos) {
			if (mbgInfo1 != null) {
				String file1 = new File(mbgInfo1.getPath()).getName();
				String file2 = new File(mbgInfo2.getPath()).getName();
				String baseErrMsg = String.format("File %s and %s can't be merged: ", file1, file2);
				if (mbgInfo1.getLastPingDate().compareTo(mbgInfo2.getFirstPingDate()) >= 0) {
					errorMessage.append(baseErrMsg + "dates are not disjoint.\n");
				}
				if (mbgInfo2.getVersion() < MbgConstants.SupportedVersion) {
					errorMessage.append(baseErrMsg + "bad mbg version (" + mbgInfo2.getVersion() + ").\n");
				}
				if (!compareProperty(mbgInfo2.getEchoSounderModel(), mbgInfo1.getEchoSounderModel())) {
					errorMessage.append(baseErrMsg + "echo sounder model are not identical.\n");
				}
				if (mbgInfo2.getTotalRxBeamCount() != mbgInfo1.getTotalRxBeamCount()) {
					errorMessage.append(baseErrMsg + "beam counts are different.\n");
				}
				if (!compareProperty(mbgInfo2.getEllipsoidName(), mbgInfo1.getEllipsoidName())
						|| mbgInfo2.getEllipsoidA() != mbgInfo1.getEllipsoidA()
						|| mbgInfo2.getEllipsoidB() != mbgInfo1.getEllipsoidB()
						|| mbgInfo2.getEllipsoidE2() != mbgInfo1.getEllipsoidE2()
						|| mbgInfo2.getEllipsoidFlatness() != mbgInfo1.getEllipsoidFlatness()) {
					errorMessage.append(baseErrMsg + "ellipsoid are not identical.\n");
				}
			} else if (mbgInfo2.getVersion() < MbgConstants.SupportedVersion) {
				errorMessage.append("File " + mbgInfo2.getPath() + " : Bad mbg version (" + mbgInfo2.getVersion()
						+ "); supproted version >= " + MbgConstants.SupportedVersion + "\n");
			}
			mbgInfo1 = mbgInfo2;
		}
		return errorMessage.toString();
	}

	/**
	 * @return true if property values are equals
	 */
	private static boolean compareProperty(String value1, String value2) {
		return value1 == null && value2 == null || value1 != null && value1.equals(value2);
	}

	/**
	 * Computes start and end swath indexes from a time interval.
	 */
	private Optional<IntRange> getSwathDimensionInTimeInterval(SounderDataContainer container, LongRange timeInterval)
			throws GIOException {

		LongLoadableLayer1D swathTimeLayer = container.getLayer(BeamGroup1Layers.PING_TIME);
		int minIndex = -1;
		int maxIndex = -1;
		for (int i = 0; i < container.getInfo().getCycleCount(); i++) {
			long time = DateUtils.nanoSecondToMilli(swathTimeLayer.get(i)); // get time in milliseconds
			if (timeInterval.getMinimumLong() < time && time <= timeInterval.getMaximumLong()) {
				if (minIndex == -1) {
					minIndex = i;
				}
				maxIndex = i;
			}
		}

		return minIndex == -1 || maxIndex == -1 ? Optional.empty() : Optional.of(new IntRange(minIndex, maxIndex));
	}

	/**
	 * Computes and writes global attributes.
	 */
	private void updateGlobalAttributes(MbgInfo info) throws GIOException {
		IFileService.grab().updateStatistics(info);
	}

	@Override
	public void setOutputFilePath(String path) {
		outputFilePath = path;
	}

	@Override
	public void setTimeInterval(Optional<LongRange> optTimeIntervals) {
		optTimeInterval = optTimeIntervals;
	}
}
