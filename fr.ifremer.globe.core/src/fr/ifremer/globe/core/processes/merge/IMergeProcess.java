package fr.ifremer.globe.core.processes.merge;

import java.io.IOException;
import java.util.Optional;

import org.apache.commons.lang.math.LongRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;

import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Interface of data merge process
 */
public interface IMergeProcess {
	/**
	 * Logger setter
	 * @param logger
	 */
	public void setLogger(Logger logger);
	
	/**
	 * Run the merge process
	 * @param monitor
	 * @throws NCException
	 * @throws IOException
	 * @throws GIOException
	 * @throws BadParameterException
	 */
	public void run(IProgressMonitor monitor) throws NCException, IOException, GIOException, BadParameterException;
	
	/**
	 * Path to the output file of the merge
	 * @param path
	 */
	public void setOutputFilePath(String path);
	
	/**
	 * Time interval for cut process
	 * @param optTimeIntervals
	 */
	public void setTimeInterval( Optional<LongRange> optTimeIntervals);
}
