package fr.ifremer.globe.core.processes.merge;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.math.IntRange;
import org.apache.commons.lang.math.LongRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.info.NviLegacyInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.processes.merge.netcdf.NetcdfMerge;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Version;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;

public class NviMergeProcess implements IMergeProcess {

	private Logger logger = LoggerFactory.getLogger(NviMergeProcess.class);

	private List<NviLegacyInfo> infoStores;
	private String outputFilePath;
	private Optional<LongRange> optTimeInterval;

	/**
	 * Constructor
	 */
	public NviMergeProcess(List<IDataContainerInfo> infoStores) {
		this.infoStores = infoStores.stream().//
				filter(NviLegacyInfo.class::isInstance).//
				map(NviLegacyInfo.class::cast).//
				collect(Collectors.toList());
	}

	/**
	 * Runs the process
	 */
	@Override
	public void run(IProgressMonitor monitor) throws NCException, IOException, GIOException, BadParameterException {
		LinkedHashMap<NetcdfFile, Map<NCDimension, IntRange>> inputFilesMap = new LinkedHashMap<>();
		List<NviLegacyInfo> nviInfoToClose = new ArrayList<>();
		try {
			// check ouput file
			if (infoStores.stream().anyMatch(info -> info.getFilePath().equals(outputFilePath))) {
				throw new BadParameterException("output file can't be one of the input file.");
			}

			// sort files
			infoStores.sort((nviInfo1, nviInfo2) -> nviInfo1.getStartEndDate().get().getFirst()
					.compareTo(nviInfo2.getStartEndDate().get().getFirst()));

			// setup input files map
			for (NviLegacyInfo nviInfo : infoStores) {
				if (optTimeInterval.isPresent()) {
					Optional<IntRange> dimensionInterval = getDimensionInterval(nviInfo, optTimeInterval.get());
					if (dimensionInterval.isPresent()) {
						nviInfo.open(true);
						nviInfoToClose.add(nviInfo);
						Map<NCDimension, IntRange> dimensionSections = new HashMap<>();
						dimensionSections.put(nviInfo.getFile().getDimension("mbPositionNbr"), dimensionInterval.get());
						inputFilesMap.put(nviInfo.getFile(), dimensionSections);
					}
				} else {
					nviInfo.open(true);
					nviInfoToClose.add(nviInfo);
					inputFilesMap.put(nviInfo.getFile(), new HashMap<>());
				}
			}

			// Check if there is files in the specified time interval
			if (optTimeInterval.isPresent() && inputFilesMap.isEmpty()) {
				logger.info("No input file in the time interval: the output file will not be generated.");
				return;
			}

			// do the merge
			try (NCFile outputFile = NCFile.createNew(Version.netcdf4, outputFilePath).withCompressionDisabled()) {
				NetcdfMerge merge = new NetcdfMerge(inputFilesMap, outputFile);
				merge.setLogger(logger);
				merge.run(monitor);
			}
			// update nvi output statistics
			try (var nviInfo = new NviLegacyInfo(outputFilePath)) {
				nviInfo.updateStatistics();
			}

		} finally {
			nviInfoToClose.forEach(NviLegacyInfo::close);
		}
	}

	/**
	 * Computes start and end indexes for the specified time interval.
	 *
	 * @throws IOException
	 */
	private Optional<IntRange> getDimensionInterval(NviLegacyInfo nviInfo, LongRange timeInterval)
			throws GIOException, IOException {
		int startIndex = -1;
		int endIndex = -1;

		try (INavigationData navigationData = nviInfo.getNavigationData(true)) {
			// search startIndex
			for (int i = 0; i < navigationData.size(); i++) {
				long time = navigationData.getTime(i);
				if (time > timeInterval.getMinimumLong()) {
					startIndex = i;
					break;
				}
			}
			// search endIndex
			for (int i = navigationData.size() - 1; 0 <= i; i--) {
				long time = navigationData.getTime(i);
				if (time < timeInterval.getMaximumLong()) {
					endIndex = i;
					break;
				}
			}
		}

		return (startIndex == -1 || endIndex == -1) ? Optional.empty()
				: Optional.of(new IntRange(startIndex, endIndex));
	}

	//// SETTERS

	@Override
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void setOutputFilePath(String path) {
		this.outputFilePath = path;
	}

	@Override
	public void setTimeInterval(Optional<LongRange> optTimeIntervals) {
		this.optTimeInterval = optTimeIntervals;
	}
}
