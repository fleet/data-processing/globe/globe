package fr.ifremer.globe.core.processes.merge.netcdf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.math.IntRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.array.impl.LargeArray;

/**
 * This class allows to merge several {@link NetcdfFile}.
 */
public class NetcdfMerge {

	private static final Logger STATIC_LOGGER = LoggerFactory.getLogger(NetcdfMerge.class);

	/**
	 * Map which provides a range (start-stop indexes) for each pair "File - Dimension"
	 */
	private final Map<NetcdfFile, Map<NCDimension, IntRange>> sectionMapping;
	private final NetcdfFile referenceFile;
	private final NCFile outputFile;
	private Logger logger = STATIC_LOGGER;

	/**
	 * Constructor : defines input parameters.
	 */
	public NetcdfMerge(Map<NetcdfFile, Map<NCDimension, IntRange>> sectionMapping, NCFile outputFile) {
		super();
		this.referenceFile = sectionMapping.keySet().iterator().next();
		this.sectionMapping = sectionMapping;
		this.outputFile = outputFile;
	}
	
	/**
	 * Sets the logger
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * Does the merge.
	 * 
	 * @throws NCException
	 * @throws IOException
	 */
	public void run(IProgressMonitor monitor) throws NCException, IOException {
		logger.info("Start merge...");

		// copy global attributes
		logger.info("Set global attributes...");
		monitor.beginTask("Merge global attributes...", referenceFile.getAttributeNames().length);
		for (String attName : referenceFile.getAttributeNames()) {
			int netcdfType = referenceFile.getAttributeType(attName);
			byte[] attrValue = referenceFile.getAttribute(attName);
			outputFile.addAttribute(attName, attrValue, netcdfType);
			monitor.worked(1);
			if (monitor.isCanceled())
				throw new OperationCanceledException();
		}

		// merge each variable
		logger.info("Merge variables...");
		monitor.beginTask(outputFile.getName() + " : merge variables...", referenceFile.getVariables().size());
		for (NCVariable var : referenceFile.getVariables()) {
			mergeVariable(var.getName());
			monitor.worked(1);
			if (monitor.isCanceled())
				throw new OperationCanceledException();
		}
		logger.info("Merge ended.");
	}

	/**
	 * Creates a new variable which is the result of the merge from input files.
	 */
	private void mergeVariable(String variablePath) throws NCException, IOException {
		logger.info("Merge variable : {}... ", variablePath);
		// get reference variable from reference file
		NetcdfVariable refVariable = referenceFile.getVariable(variablePath);

		// define output variable dimension
		List<NCDimension> newVariableDimensions = new ArrayList<>();
		int valueCount = 1;
		List<NCDimension> referenceDimensions = refVariable.getShape();
		boolean isFirst = true;
		for (NCDimension referenceDimension : referenceDimensions) {
			NCDimension newDimension = getOrCreateDimension(referenceDimension.getName(), isFirst);
			isFirst = false;
			newVariableDimensions.add(newDimension);
			valueCount *= newDimension.getLength();
		}

		// create the new variable
		NCVariable newVariable = outputFile.addVariable(variablePath, refVariable.getType(), newVariableDimensions);

		// add attributes
		for (String attName : refVariable.getAttributeNames())
			newVariable.addAttribute(attName, refVariable.getAttribute(attName), refVariable.getAttributeType(attName));

		// write variable data

		// create temporary buffer
		int dataTypeSize = DataType.getDataTypeFromNCType(newVariable.getType()).getSize();
		try (LargeArray tmpArray = LargeArray.create(valueCount, dataTypeSize, "_netcdf_merge")) {
			ByteBuffer tmpBuffer = tmpArray.getByteBuffer(0);
			
			// fill buffer
			int position = 0;
			for (Entry<NetcdfFile, Map<NCDimension, IntRange>> fileDimensionSection : sectionMapping.entrySet()) {
				NetcdfVariable readVariable = fileDimensionSection.getKey().getVariable(variablePath);
				List<NCDimension> readVariableDims = readVariable.getShape();

				// compute start index and count to read
				long[] readStart = new long[readVariableDims.size()];
				long[] readCount = new long[readVariableDims.size()];

				for (int i = 0; i < readVariableDims.size(); i++) {
					NCDimension dimension = readVariableDims.get(i);
					if (fileDimensionSection.getValue().containsKey(dimension)) {
						readStart[i] = fileDimensionSection.getValue().get(dimension).getMinimumLong();
						readCount[i] = fileDimensionSection.getValue().get(dimension).getMaximumLong() - readStart[i]
								+ 1;
					} else {
						readCount[i] = dimension.getLength();
					}
				}

				readVariable.get_byte(readStart, readCount, tmpBuffer);

				position += (int) Arrays.stream(readCount).reduce(1, (a, b) -> a * b) * dataTypeSize;
				if (position > tmpBuffer.capacity())
					break;
				tmpBuffer.position(position);
			}
			// write buffer
			tmpBuffer.position(0);
			newVariable.put(tmpBuffer);
		}
	}

	/**
	 * Gets or creates a dimension.
	 */
	private NCDimension getOrCreateDimension(String name, boolean isFirst) throws NCException {
		// get dimension
		NCDimension dimension = outputFile.getDimension(name);
		if (dimension != null)
			return dimension;

		// or create it
		long dimensionLength = 0;
		// compute the dimension value from the section mapping
		for (Map<NCDimension, IntRange> map : sectionMapping.values()) {
			for (Entry<NCDimension, IntRange> dimSection : map.entrySet()) {
				if (dimSection.getKey().getName().equals(name))
					dimensionLength += dimSection.getValue().getMaximumLong() - dimSection.getValue().getMinimumLong()
							+ 1;
			}
		}
		// if this dimension is not referenced into the section mapping : get value from reference file
		if (dimensionLength == 0) {
			if (!isFirst) {
				dimensionLength = referenceFile.getDimension(name).getLength();
			} else {
				for (NetcdfFile file : sectionMapping.keySet())
					dimensionLength += file.getDimension(name).getLength();
			}
		}
		logger.info("New dimension : {} [{}]", name, dimensionLength);
		dimension = outputFile.addDimension(name, dimensionLength);

		return dimension;
	}

}
