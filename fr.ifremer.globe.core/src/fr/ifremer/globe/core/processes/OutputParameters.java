package fr.ifremer.globe.core.processes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Sound Velocity Extraction
 * 
 */

public class OutputParameters extends ProcessParameters {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parameter file name.
	 */
	public static final String OUTPUT_PARAMETERS_FILE_NAME = "Output.properties";

	/**
	 * Output directory.
	 */
	public static final String PROPERTY_OUTPUT_DIRECTORY_NAME = "outputDirectoryName";

	/**
	 * Suffixe results
	 */
	public static final String PROPERTY_SUFFIXE_RESULTS = "suffixeResults";

	public OutputParameters(String bundleName) {
		super(bundleName);
	}

	/**
	 * Read parameters values.
	 * 
	 * @return
	 */
	@Override
	protected void initialize() {
		String paramFileName = getUserFilePath(OUTPUT_PARAMETERS_FILE_NAME);
		try {
			File f = new File(paramFileName);
			if (f.exists())
				load(new FileInputStream(paramFileName));
			else
				load(getDefaultParameterFilePath(m_bundleName));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Write parameters values.
	 * 
	 * @return
	 */
	public void save() {
		String paramFileName = getUserFilePath(OUTPUT_PARAMETERS_FILE_NAME);
		File f = new File(paramFileName);
		try {
			if (!f.exists()) {
				f.getParentFile().mkdirs();
			}
			store(new FileOutputStream(f), null);
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Restore default parameters values.
	 * 
	 * @return
	 */
	public void restoreDefault() {
		try {
			load(getDefaultParameterFilePath(m_bundleName));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

}
