package fr.ifremer.globe.core.processes.depthcorrection;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.utils.exception.FileFormatException;

public class CutFileReader {
	
	/**
	 * Constructor
	 */
	private CutFileReader() {
		//Utility class
	}
	
	/**
	 * Reads a cut file
	 */
	public static Set<Profile> read(File cutFile) throws FileFormatException {
		Set<Profile> timeProfileSet = new HashSet<>();
		try {
			timeProfileSet.addAll(ProfileUtils.loadProfilesFromFile(cutFile));
		} catch (IOException e) {
			throw new FileFormatException("Error : can't read this file as a cut file : \n" + cutFile.getName());
		}
		return timeProfileSet;
	}
}
