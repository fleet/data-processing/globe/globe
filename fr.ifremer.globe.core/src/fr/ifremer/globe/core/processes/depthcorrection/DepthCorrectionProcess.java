package fr.ifremer.globe.core.processes.depthcorrection;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.OptionalLayersAccessor;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.sounder.datacontainer.SwathInterpolatedLayer;
import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.TideLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.DynamicDraughtLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Process to correct depth of a sounder file.
 */
public class DepthCorrectionProcess implements IDataContainerOwner {

	/** Process parameters **/
	private final DepthCorrectionType correctionType;
	private final TreeMap<Instant, Double> correctionValues;
	private final Optional<String> correctionFileName;
	private TideType tideType = TideType.UNKNWON;
	private final Optional<Set<Profile>> timeProfileSet;

	/**
	 * Constructor
	 */
	public DepthCorrectionProcess(DepthCorrectionType correctionType, Optional<String> correctionFileName,
			TreeMap<Instant, Double> correctionValues, Optional<Set<Profile>> timeProfileSet) {
		super();
		this.correctionType = correctionType;
		this.correctionFileName = correctionFileName;
		this.correctionValues = correctionValues;
		this.timeProfileSet = timeProfileSet;
	}

	@Override
	public String getName() {
		return "depth correction process";
	}

	/**
	 * Applies the correction to the specified file.
	 */
	public int apply(ISounderNcInfo inputFile, Logger logger) throws GIOException {
		int nbProcessedCycles = 0;

		if (correctionType == DepthCorrectionType.TIDE)
			logger.info("Tide type :  {}", tideType);

		try (ISounderDataContainerToken sounderDataContainerToken = IDataContainerFactory.grab().book(inputFile,
				this)) {
			SounderDataContainer container = sounderDataContainerToken.getDataContainer();
			// Get required layers
			LongLoadableLayer1D swathTimeLayer = container.getLayer(BeamGroup1Layers.PING_TIME);
			FloatLoadableLayer2D depth = container.getLayer(BathymetryLayers.DETECTION_Z);

			// Draught/Tide are not necessary aligned with swath. In that case values have
			// to be interpolated.
			FloatLoadableLayer1D draughtLoadableLayer = container.getLayer(DynamicDraughtLayers.DELTA_DRAUGHT);
			SwathInterpolatedLayer draughtInterpolatedLayer = null;
			FloatLayer1D draught;
			if (container.hasLayer(DynamicDraughtLayers.TIME)) { // Xsf
				draughtInterpolatedLayer = new SwathInterpolatedLayer(container, draughtLoadableLayer,
						container.getLayer(DynamicDraughtLayers.TIME));
				draught = draughtInterpolatedLayer.getLayer();
			} else { // Mbg
				draught = container.getLayer(DynamicDraughtLayers.DELTA_DRAUGHT);
			}

			FloatLoadableLayer1D tideLoadableLayer = container.getLayer(TideLayers.TIDE_INDICATIVE);
			SwathInterpolatedLayer tideInterpolatedLayer = null;
			FloatLayer1D tide;
			if (container.hasLayer(TideLayers.TIME)) { // Xsf
				tideInterpolatedLayer = new SwathInterpolatedLayer(container, tideLoadableLayer,
						container.getLayer(TideLayers.TIME));
				tide = tideInterpolatedLayer.getLayer();
			} else { // Mbg
				tide = container.getLayer(TideLayers.TIDE_INDICATIVE);
			}

			FloatLoadableLayer1D waterline = container.getLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);
			FloatLoadableLayer1D txDepth = container.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
			FloatLoadableLayer1D platformVerticalOffset = container.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

			Optional<DoubleLoadableLayer2D> verDepthLayer = Optional.empty();
			OptionalLayersAccessor accessor = inputFile.getOptionalLayersAccessor().orElse(null);
			if (accessor != null) {
				PredefinedLayers<DoubleLoadableLayer2D> layerRef = accessor.getVerticalDepthLayer().orElse(null);
				verDepthLayer = layerRef != null ? container.getOptionalLayer(layerRef) : Optional.empty();
			}

			double offset = Double.NaN;
			// For each swath
			for (int iSwath = 0; iSwath < inputFile.getCycleCount(); iSwath++) {
				Instant swathTime = DateUtils.epochNanoToInstant(swathTimeLayer.get(iSwath));
				// Detect if ping is in a time interval
				if (!timeProfileSet.isPresent()
						|| ProfileUtils.isPingInTimeInterval(swathTime, timeProfileSet.get(), new HashSet<>())) {

					if (correctionValues.isEmpty()) {
						// reset values
						offset = 0.0;
					} else {
						// Get a position before and after : pass if not found
						Instant timeBefore = correctionValues.floorKey(swathTime);
						Instant timeAfter = correctionValues.ceilingKey(swathTime);

						if (timeBefore == null && correctionType == DepthCorrectionType.DRAUGHT)
							// Extrapolation before
							offset = correctionValues.get(correctionValues.firstKey());
						else if (timeAfter == null && correctionType == DepthCorrectionType.DRAUGHT)
							// Extrapolation after
							offset = correctionValues.get(correctionValues.lastKey());
						else if (timeBefore == null || timeAfter == null)
							// No extrapolation
							continue;
						else {
							// Interpolation
							// Compute interpolated value
							double valueBefore = correctionValues.get(timeBefore);
							double valueAfter = correctionValues.get(timeAfter);

							double coeff = timeAfter.equals(timeBefore) ? 0
									: ((double) (swathTime.toEpochMilli() - timeBefore.toEpochMilli()))
											/ (timeAfter.toEpochMilli() - timeBefore.toEpochMilli());
							offset = valueBefore + coeff * (valueAfter - valueBefore);
						}
					}

					// Write the computed values
					if (!Double.isNaN(offset)) {
						float prevOffset = 0;
						float deltaOffset = 0;
						nbProcessedCycles++;

						switch (correctionType) {
						case NOMINAL_WATERLINE:
							prevOffset = tide.get(iSwath) + draught.get(iSwath);
							deltaOffset = (float) (offset - prevOffset);
							tide.set(iSwath, (float) (offset - draught.get(iSwath)));
							waterline.set(iSwath, tide.get(iSwath));
							break;
						case DRAUGHT:
							// update dependent layers
							prevOffset = draught.get(iSwath);
							deltaOffset = (float) (offset - prevOffset);
							draught.set(iSwath, (float) offset);
							addFloatLayerOffset(platformVerticalOffset, iSwath, deltaOffset);
							addFloatLayerOffset(txDepth, iSwath, -deltaOffset);
							break;
						case TIDE:
							prevOffset = tide.get(iSwath);
							deltaOffset = (float) (offset - prevOffset);
							tide.set(iSwath, (float) offset);
							waterline.set(iSwath, tide.get(iSwath));
							break;
						}
						// vertical depth layers updating (mbg)
						if (verDepthLayer.isPresent()) {
							for (int iAntenna = 0; iAntenna < inputFile.getRxParameters().size(); iAntenna++) {
								double previousValue = verDepthLayer.get().get(iSwath, iAntenna);
								verDepthLayer.get().set(iSwath, iAntenna, previousValue - deltaOffset);
							}
						}
					}
				}
			}

			// Flush layers
			if (draughtInterpolatedLayer != null) {
				draughtInterpolatedLayer.flush();
			} else {
				draughtLoadableLayer.flush();
			}
			if (tideInterpolatedLayer != null) {
				tideInterpolatedLayer.flush();
			} else {
				tideLoadableLayer.flush();
			}
			waterline.flush();
			platformVerticalOffset.flush();
			txDepth.flush();

			if (verDepthLayer.isPresent())
				verDepthLayer.get().flush();
			// Depth need to be updated for mbg
			depth.getData().setDirty(true);
			depth.flush();

			updateFile(inputFile);
		} catch (GException e) {
			throw new GIOException("Unable to process depth correction. File not available : " + inputFile.getPath());
		}

		IFileService.grab().updateStatistics(inputFile);

		return nbProcessedCycles;

	}

	/**
	 * Add an offset to loadable layer
	 * 
	 * @throws GIOException
	 */
	private void addFloatLayerOffset(FloatLoadableLayer1D layer, int index, float offset) throws GIOException {
		float previousValue = layer.get(index);
		layer.set(index, previousValue + offset);
	}

	/**
	 * Update min/max depth attributes and correction flags.
	 */
	private void updateFile(ISounderNcInfo inputFile) throws GIOException {
		Optional<ProcessAssessor> processAssessor = inputFile.getProcessAssessor();
		if (processAssessor.isPresent()) {
			if (correctionType == DepthCorrectionType.TIDE || correctionType == DepthCorrectionType.NOMINAL_WATERLINE) {
				processAssessor.get().heedTideCorrection(correctionFileName, tideType);
			} else if (correctionType == DepthCorrectionType.DRAUGHT) {
				processAssessor.get().heedDraughtCorrection(correctionFileName);
			}
		}
	}

	public void setTideType(TideType tideType) {
		this.tideType = tideType;
	}

}
