package fr.ifremer.globe.core.processes.depthcorrection;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TreeMap;

import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

public class CorrectionFileUtils {

	public static final String defaultSeparators = "[; \t]+";
	protected static String[] fileExtensions = { "*.ttb", "*.txt", "*.dat" };
	protected static String fileExtensions2 = "{ttb,txt,dat}";
	protected static final int minNbColumns = 3;

	/** Private constructor **/
	private CorrectionFileUtils() {
	}

	/**
	 * Reads a tide file.
	 */
	public static TreeMap<Instant, Double> read(String fileName, DepthCorrectionType type) throws GIOException {

		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*." + fileExtensions2);

		Path filePath = Paths.get(fileName);
		if (!matcher.matches(filePath.getFileName())) {
			throw new GIOException("Error : can't read this file as a tide/dynamic draught file : "
					// + filePath
					+ " accepted extensions are .ttb .txt .dat"
					+ "\n Information: The expected format is: \n# Date;Valeur\n"
					+ "DD/MM/YYYY HH:MM:SS.XXX read value [<not read> <not read> etc...]\n Accepted separators are : ; space \\t (tab)");
		}

		TreeMap<Instant, Double> result = new TreeMap<>();
		String line;
		String[] datas;
		Date date;
		try (Scanner scanner = new Scanner(new File(fileName))) {
			while (scanner.hasNextLine()) {
				line = scanner.nextLine().trim();
				// check first character is a digit ( skip comment lines)
				if (!line.isEmpty()) {
					if (Character.isDigit(line.charAt(0))) {
						datas = line.split(defaultSeparators);
						if (datas.length >= minNbColumns) {
							// check first character is a digit ( skip comment lines)
							// set date and value
							date = DateUtils.parseDate(datas[0] + " " + datas[1]);
							if (date == null)
								throw new NumberFormatException("Impossible to parse date " + datas[0] + " " + datas[1]
										+ "Format date must be DD/MM/YYYY HH:MM:SS.XXX ");
							else
								result.put(date.toInstant(), Double.parseDouble(datas[2]));
						}
					}
				}
			}
			if (result.isEmpty()) {
				throw new GIOException("Error : correction file is empty. " + fileName);
			} else if (result.size() < 2 && DepthCorrectionType.DRAUGHT != type) {
				throw new GIOException("Error : tide correction file must contains at least two records. " + fileName);
			}
			return result;
		} catch (FileNotFoundException e) {
			throw new GIOException("Tide file not found: " + filePath, e);
		} catch (NoSuchElementException | NumberFormatException e) {
			throw new GIOException("Error while reading tide file : " + e.getMessage(), e);
		}
	}

	public static String[] getFilterExtensions() {
		List<String> ext = new ArrayList<>();

		ext.add("");
		for (String extension : fileExtensions) {
			ext.add(extension);
			ext.set(0, ext.get(0) + extension + ";");
		}
		ext.add("*.*");
		return ext.toArray(new String[0]);
	}
}
