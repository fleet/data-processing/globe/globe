package fr.ifremer.globe.core.processes.depthcorrection;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Process to correct depth of a sounder file.
 */
public class DepthCorrection implements IProcessFunction {

	private static final String LOGGER_SEPARATOR_LINE = "-----------------------------------------------------";
	private List<ISounderNcInfo> inputFiles;
	private DepthCorrectionProcess tideCorrectionProcess;
	private DepthCorrectionProcess draughtCorrectionProcess;

	/**
	 * Constructor : builds the depth correction process with parameters.
	 */
	public DepthCorrection(DepthCorrectionParameters depthCorrectionParameters,
			TimeIntervalParameters timeItervalParameters, Logger logger) throws GIOException {

		// define time interval
		Optional<Set<Profile>> timeProfileSet = Optional.empty();
		String timeIntervalDescription = "not specified.";
		// cut file provided
		if (timeItervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION)) {
			String cutFileName = timeItervalParameters.getProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME);
			timeProfileSet = Optional.of(CutFileReader.read(new File(cutFileName)));
			timeIntervalDescription = " cut file: " + cutFileName;
		}
		// time interval from manual option
		else if (timeItervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION)) {
			long startDate = timeItervalParameters.getLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE);
			long endDate = timeItervalParameters.getLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE);
			timeProfileSet = Optional.of(new HashSet<>(Arrays.asList(new Profile(startDate, endDate))));
			timeIntervalDescription = " start: " + DateUtils.formatDate(new Date(startDate)) + " - end: "
					+ DateUtils.formatDate(new Date(endDate));
		}

		logger.info("Time interval:  {}", timeIntervalDescription);
		////// DRAUGHT CORRECTION /////////
		// get correction source
		DepthCorrectionSource draughtCorrectionSource = DepthCorrectionSource.valueOf(
				depthCorrectionParameters.getProperty(DepthCorrectionParameters.PROPERTY_DRAUGHT_CORRECTION_SOURCE));
		switch (draughtCorrectionSource) {
		case CORRECTION_FILE:
			// read correction file (.ttb)
			String correctionFileName = depthCorrectionParameters
					.getProperty(DepthCorrectionParameters.PROPERTY_DYNAMIC_DRAUGHT_FILENAME);
			TreeMap<Instant, Double> correctionValues = CorrectionFileUtils.read(correctionFileName, DepthCorrectionType.DRAUGHT);
			logger.info("Draught correction file: {}", correctionFileName);
			// create draught correction process
			draughtCorrectionProcess = new DepthCorrectionProcess(DepthCorrectionType.DRAUGHT,
					Optional.ofNullable(correctionFileName), correctionValues, timeProfileSet);
			break;
		case RESET:
			logger.info("Reset draught correction");
			draughtCorrectionProcess = new DepthCorrectionProcess(DepthCorrectionType.DRAUGHT, Optional.empty(),
					new TreeMap<>(), timeProfileSet);
			break;
		case NONE:
		default:
			// No correction
			break;
		}

		////// TIDE CORRECTION /////////
		// get correction source
		DepthCorrectionSource tideCorrectionSource = DepthCorrectionSource.valueOf(
				depthCorrectionParameters.getProperty(DepthCorrectionParameters.PROPERTY_TIDE_CORRECTION_SOURCE));
		switch (tideCorrectionSource) {
		case CORRECTION_FILE: {
			// read correction file (.ttb)
			String correctionFileName = depthCorrectionParameters
					.getProperty(DepthCorrectionParameters.PROPERTY_TIDE_FILENAME);
			TreeMap<Instant, Double> correctionValues = CorrectionFileUtils.read(correctionFileName, DepthCorrectionType.TIDE);
			logger.info("Tide correction file: {}", correctionFileName);
			// create tide correction process
			tideCorrectionProcess = new DepthCorrectionProcess(DepthCorrectionType.TIDE,
					Optional.ofNullable(correctionFileName), correctionValues, timeProfileSet);
			break;
		}
		case PLATFORM_ELEVATION_FILE: {
			// read correction file (.ttb)
			String correctionFileName = depthCorrectionParameters
					.getProperty(DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_FILENAME);
			Double voffset = Double.valueOf(depthCorrectionParameters
					.getProperty(DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_VOFFSET));
			logger.info("Platform elevation file: {}", correctionFileName);
			TreeMap<Instant, Double> correctionValues = CorrectionFileUtils.read(correctionFileName, DepthCorrectionType.NOMINAL_WATERLINE);
			// translate platform elevation values to nominal waterline elevation values
			correctionValues.forEach((key, value) -> correctionValues.replace(key, value - voffset));
			// create nominal waterline correction process
			tideCorrectionProcess = new DepthCorrectionProcess(DepthCorrectionType.NOMINAL_WATERLINE,
					Optional.ofNullable(correctionFileName), correctionValues, timeProfileSet);
			break;
		}
		case RESET:
			logger.info("Reset tide correction");
			tideCorrectionProcess = new DepthCorrectionProcess(DepthCorrectionType.TIDE, Optional.empty(),
					new TreeMap<>(), timeProfileSet);
			break;
		case NONE:
		default:
			// No correction
			break;
		}
	}

	/**
	 * Sets input files
	 */
	public void setInputFiles(List<ISounderNcInfo> inputFiles) {
		this.inputFiles = inputFiles;
	}

	public void setTideType(TideType tideType) {
		if (tideCorrectionProcess != null)
			this.tideCorrectionProcess.setTideType(tideType);
	}

	/**
	 * Sets input files (with a file names) (used by tests)
	 * 
	 * @throws GIOException
	 */
	public void setFileNameList(List<String> fileNameList) throws GIOException {
		List<ISounderNcInfo> inputFilesAsInfo = new ArrayList<>();
		for (String fileName : fileNameList) {
			var sounderFileInfo = IFileService.grab().getFileInfo(fileName).filter(ISounderNcInfo.class::isInstance)
					.map(ISounderNcInfo.class::cast)
					.orElseThrow(() -> new GIOException("Error : can't read input file : " + fileName));
			inputFilesAsInfo.add(sounderFileInfo);
		}
		setInputFiles(inputFilesAsInfo);
	}

	/**
	 * Perform depth correction on input files.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) {
		logger.info(LOGGER_SEPARATOR_LINE);

		monitor.setTaskName("Depth correction");
		monitor.beginTask("Depth correction running ...", inputFiles.size());

		for (ISounderNcInfo inputFile : inputFiles) {
			monitor.setTaskName("Processing: " + inputFile.getPath());
			logger.info("Processing:  {}", inputFile.getPath());
			try {
				// Process file
				processFile(inputFile, logger);
			} catch (GIOException e) {
				logger.error("Error while processing file {}: {}", inputFile.getPath(), e.getMessage(), e);
			}
			logger.info(LOGGER_SEPARATOR_LINE);

			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			monitor.worked(1);
		}
		return Status.OK_STATUS;
	}

	/**
	 * Process depth correction for a file
	 *
	 * @param fileinfo
	 * @param logger
	 * @throws GIOException
	 */
	public void processFile(ISounderNcInfo sounderInfo, Logger logger) throws GIOException {
		// Apply first draught correction then tide correction to have coherent values
		// in case of tide correction with
		// platform elevation file
		int swathProcessCount = 0;
		if (draughtCorrectionProcess != null) {
			swathProcessCount = draughtCorrectionProcess.apply(sounderInfo, logger);
		}
		if (tideCorrectionProcess != null) {
			swathProcessCount = tideCorrectionProcess.apply(sounderInfo, logger);
		}

		logger.info("{}/{} swaths processed.", swathProcessCount, sounderInfo.getCycleCount());
	}

}
