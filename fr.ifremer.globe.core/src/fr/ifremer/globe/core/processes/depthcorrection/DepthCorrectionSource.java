package fr.ifremer.globe.core.processes.depthcorrection;

/**
 * Enumerator to specify depth correction sources.
 */
public enum DepthCorrectionSource {
	
	NONE("None"), // Keep unchanged
	CORRECTION_FILE("Correction file"), // Correction file (tide / draught)
	PLATFORM_ELEVATION_FILE("Platform elevation file"), // mean platform elevation from compute tide module
	RESET("Reset"); // Reset

	private String name;

	DepthCorrectionSource(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
