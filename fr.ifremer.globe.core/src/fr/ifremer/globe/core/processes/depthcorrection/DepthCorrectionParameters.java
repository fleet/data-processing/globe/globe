package fr.ifremer.globe.core.processes.depthcorrection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import fr.ifremer.globe.core.processes.ProcessParameters;

/**
 * Depth correction process (Cosima).
 * 
 */

public class DepthCorrectionParameters extends ProcessParameters {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parameter file name.
	 */
	public static final String DEPTHCORRECTION_PARAMETERS_FILENAME = "DepthCorrection.properties";

	/**
	 * Tide file name.
	 */
	public static final String PROPERTY_TIDE_FILENAME = "tideFilename";

	/**
	 * Platform elevation file name.
	 */
	public static final String PROPERTY_PLATFORM_ELEVATION_FILENAME = "platformElevationFilename";

	/**
	 * Platform elevation vertical offset. Z coordinate in FCS of the ref point wrt the waterline (positive upward).
	 */
	public static final String PROPERTY_PLATFORM_ELEVATION_VOFFSET = "platformElevationVerticalOffset";

	/**
	 * Dynamic draught file name.
	 */
	public static final String PROPERTY_DYNAMIC_DRAUGHT_FILENAME = "dynamicDraughtFilename";

	/**
	 * Tide correction source.
	 */
	public static final String PROPERTY_TIDE_CORRECTION_SOURCE = "tideCorrectionSource";

	/**
	 * Draught correction source.
	 */
	public static final String PROPERTY_DRAUGHT_CORRECTION_SOURCE = "draughtCorrectionSource";

	public DepthCorrectionParameters() {
		super();
	}

	/**
	 * Read parameters values.
	 * 
	 * @return
	 */
	@Override
	protected void initialize() {
		String paramFileName = getUserFilePath(DEPTHCORRECTION_PARAMETERS_FILENAME);
		try {
			File f = new File(paramFileName);
			if (f.exists())
				load(new FileInputStream(paramFileName));
			else
				load(getDefaultParameterFilePath("fr.ifremer.globe.core.processes.depthcorrection"));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Write parameters values.
	 * 
	 * @return
	 */
	public void save() {
		String paramFileName = getUserFilePath(DEPTHCORRECTION_PARAMETERS_FILENAME);
		File f = new File(paramFileName);
		try {
			if (!f.exists()) {
				f.getParentFile().mkdirs();
			}
			store(new FileOutputStream(f), null);
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Restore default parameters values.
	 * 
	 * @return
	 */
	public void restoreDefault() {
		try {
			load(getDefaultParameterFilePath("fr.ifremer.globe.core.processes.depthcorrection"));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

}
