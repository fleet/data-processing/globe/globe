package fr.ifremer.globe.core.processes.soundvelocitycorrection.model;

import java.util.Optional;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.OptionalLayersAccessor;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionBeamPointingAnglesLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;

public class SounderFileProxy implements IDataProxy, IDataContainerOwner, AutoCloseable {

	private final ISounderDataContainerToken sounderDataContainerToken;
	private final SounderDataContainer sounderDataContainer;

	private final LongLayer1D swathDateLayer;
	private final DoubleLayer1D longitudeLayer;
	private final DoubleLayer1D latitudeLayer;
	private final DoubleLayer2D depthLayer;
	private final FloatLayer2D alongDistLayer;
	private final FloatLayer2D acrossDistLayer;
	private final Optional<DoubleLoadableLayer2D> acrossAngleLayer;
	private final Optional<FloatLayer2D> beamPointingAngleLayer;

	private final FloatLayer1D tx_z_depth;
	private final FloatLoadableLayer2D twoWayTravelTime;

	private final CompositeCSLayers coordinateSystemLayers;

	private final PingBeamLimits limits;
	private final Ellipsoid ellipsoid;

	private FloatLoadableLayer1D surfaceSoundSpeedLayer;
	private ByteLoadableLayer2D swathStatusLayer;

	/**
	 * Constructor
	 */
	public SounderFileProxy(ISounderNcInfo info) throws GIOException {
		// get data container
		sounderDataContainerToken = IDataContainerFactory.grab().book(info, this);
		sounderDataContainer = sounderDataContainerToken.getDataContainer();

		// create Ping / beam limits for proxy
		this.limits = new fr.ifremer.globe.core.processes.soundvelocitycorrection.model.PingBeamLimits(0,
				info.getCycleCount() - 1, 0, info.getTotalRxBeamCount() - 1);

		// retrieve needed layers
		swathDateLayer = sounderDataContainer.getLayer(BeamGroup1Layers.PING_TIME);
		latitudeLayer = sounderDataContainer.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
		longitudeLayer = sounderDataContainer.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);
		alongDistLayer = sounderDataContainer.getLayer(BathymetryLayers.DETECTION_X);
		acrossDistLayer = sounderDataContainer.getLayer(BathymetryLayers.DETECTION_Y);
		surfaceSoundSpeedLayer = sounderDataContainer.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);
		swathStatusLayer = sounderDataContainer.getLayer(BathymetryLayers.STATUS);
		
		// In case of Mbg format, get AcrossAngle layer
		OptionalLayersAccessor accessor = info.getOptionalLayersAccessor().orElse(null);
		if (accessor != null) {
			PredefinedLayers<DoubleLoadableLayer2D> layerRef = accessor.getAcrossAngleLayer().orElse(null);
			acrossAngleLayer = layerRef != null ? sounderDataContainer.getOptionalLayer(layerRef) : Optional.empty();
		} else {
			acrossAngleLayer = Optional.empty();
		}

		// if accrossAngle layer is not available (Xsf) we need additional data to recompute it
		if (acrossAngleLayer.isEmpty()) {
			DetectionBeamPointingAnglesLayers pointingAnglesLayers = new DetectionBeamPointingAnglesLayers(
					sounderDataContainer);
			beamPointingAngleLayer = pointingAnglesLayers.getAcrossAngleScsLayer();
		} else {
			beamPointingAngleLayer = Optional.empty();
		}

		twoWayTravelTime = sounderDataContainer.getLayer(BathymetryLayers.DETECTION_TWO_WAY_TRAVEL_TIME);

		tx_z_depth = sounderDataContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);// immersion

		// Depth : TODO depth should be in Antenna Coordinate System (ACS) (and avoid use of immersion)
		coordinateSystemLayers = sounderDataContainer.getCsLayers(CoordinateSystem.FCS);
		depthLayer = coordinateSystemLayers.getProjectedZ();

		// get ellipsoid
		ellipsoid = sounderDataContainer.getInfo().getEllipsoid();
		ellipsoid.eccentricity2(); // TODO useful ? Got from previous code (MbgProxy)
		ellipsoid.flatness();
	}

	@Override
	public void close() {
		sounderDataContainerToken.close();
	}

	@Override
	public boolean isTobeProcessed(int swathIndex, int beamIndex) {
		return limits.isBeamInBeamInterval(beamIndex);
	}

	@Override
	public void saveData() throws GIOException {
		coordinateSystemLayers.recomputeSoundingLatLon(); // TODO should not be necessary if the coordinate system was
															// ACS!
		coordinateSystemLayers.flush();
		sounderDataContainer.flush();
		sounderDataContainerToken.close(); // dataContainer need to be closed before MbgDriver writes properties

		Optional<ProcessAssessor> processAssessor = sounderDataContainer.getInfo().getProcessAssessor();
		if (processAssessor.isPresent()) {
			processAssessor.get().heedVelocityCorrection();
		}
	}

	@Override
	public ISounderNcInfo getInfo() {
		return sounderDataContainer.getInfo();
	}

	@Override
	public Ellipsoid getEllipsoid() {
		return ellipsoid;
	}

	@Override
	public PingBeamLimits getPingBeamLimits() {
		return limits;
	}

	// GETTERS

	@Override
	public long getEpochTimeNano(int swathIndex) throws GIOException {
		return swathDateLayer.get(swathIndex);
	}

	@Override
	public double getSwathLat(int swathIndex) throws GIOException {
		return latitudeLayer.get(swathIndex);
	}

	@Override
	public double getSwathLon(int swathIndex) throws GIOException {
		return longitudeLayer.get(swathIndex);
	}

	@Override
	public double getImmersion(int swathIndex) throws GIOException {
		return tx_z_depth.get(swathIndex);
	}

	@Override
	public double getDepth(int swathIndex, int beamIndex) throws GIOException {
		if (swathStatusLayer.get(swathIndex, beamIndex)!=0)
			return Double.NaN;
		return -depthLayer.get(swathIndex, beamIndex); // TODO remove "-" after SoundVelocityCorrection update (change
														// of coordinate system...)
	}

	@Override
	public double getAlongDistance(int swathIndex, int beamIndex) throws GIOException {
		return alongDistLayer.get(swathIndex, beamIndex);
	}

	@Override
	public double getAcrossDistance(int swathIndex, int beamIndex) throws GIOException {
		return acrossDistLayer.get(swathIndex, beamIndex);
	}

	@Override
	public double getAcrossAngle(int swathIndex, int beamIndex) throws GIOException {
		if (acrossAngleLayer.isPresent()) {
			return acrossAngleLayer.get().get(swathIndex, beamIndex);
		} else if (beamPointingAngleLayer.isPresent()) {
			return beamPointingAngleLayer.get().get(swathIndex, beamIndex);
		} else {
			return Double.NaN;
		}
	}

	@Override
	public float getTwoWayTravelTime(int swathIndex, int beamIndex) throws GIOException {
		return twoWayTravelTime.get(swathIndex, beamIndex);
	}

	@Override
	public double getSurfaceSoundSpeed(int swathIndex) throws GIOException {
		return surfaceSoundSpeedLayer.get(swathIndex);
	}

	// SETTERS

	@Override
	public void setDepth(int swathIndex, int beamIndex, double depth) throws GIOException {
		depthLayer.set(swathIndex, beamIndex, -depth); // TODO remove "-" after SoundVelocityCorrection update (change
														// of coordinate system...)
	}

	@Override
	public void setAcrossDistance(int swathIndex, int beamIndex, double acrossDistance) throws GIOException {
		acrossDistLayer.set(swathIndex, beamIndex, (float) acrossDistance);
	}
}
