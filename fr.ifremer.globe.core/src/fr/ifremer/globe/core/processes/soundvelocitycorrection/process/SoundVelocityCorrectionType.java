package fr.ifremer.globe.core.processes.soundvelocitycorrection.process;

/**
 * Enumerator to specify depth correction types.
 */
public enum SoundVelocityCorrectionType {
	
	CORRECTION_FROM_MBG("correctionFromMBG"), CORRECTION_FROM_SWATHEDITOR("correctionFromSwathEditor");

	private String name;

	SoundVelocityCorrectionType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
