package fr.ifremer.globe.core.processes.soundvelocitycorrection.model;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;

public interface IDataProxy {

	public boolean isTobeProcessed(int iSwath, int iBeam);

	public void saveData() throws GIOException;

	// Getter

	public ISounderNcInfo getInfo();

	public PingBeamLimits getPingBeamLimits();

	public Ellipsoid getEllipsoid();

	public long getEpochTimeNano(int iSwath) throws GIOException;

	public double getImmersion(int iSwath) throws GIOException;

	public double getSwathLat(int iSwath) throws GIOException;

	public double getSwathLon(int iSwath) throws GIOException;

	public double getDepth(int iSwath, int iBeam) throws GIOException;

	public double getAlongDistance(int iSwath, int iBeam) throws GIOException;

	public double getAcrossDistance(int iSwath, int iBeam) throws GIOException;

	public double getAcrossAngle(int iSwath, int iBeam) throws GIOException;

	public float getTwoWayTravelTime(int iSwath, int iBeam) throws GIOException;

	// Setters

	public void setDepth(int iSwath, int iBeam, double depth) throws GIOException;

	public void setAcrossDistance(int iSwath, int iBeam, double acrossDistance) throws GIOException;

	public double getSurfaceSoundSpeed(int iSwath) throws GIOException;

}