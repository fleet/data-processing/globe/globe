package fr.ifremer.globe.core.processes.soundvelocitycorrection.model;


public class PingBeamLimits
{
	private int _pingMin;
	private int _pingMax;
	private int _beamMin;
	private int _beamMax;

	public PingBeamLimits(int pingMin, int pingMax, int beamMin, int beamMax) {
		_pingMin = pingMin;
		_pingMax = pingMax;
		_beamMin = beamMin;
		_beamMax = beamMax;
	}
	
	public PingBeamLimits() {
		_pingMin = -Integer.MAX_VALUE;
		_pingMax = Integer.MAX_VALUE;
		_beamMin = -Integer.MAX_VALUE;
		_beamMax = Integer.MAX_VALUE;
	}

	public boolean isInLimits(int ping,int beam)
	{
		boolean isPingInPingInterval = false;
		if (isPingInPingInterval(ping) && isBeamInBeamInterval(beam)) {
			isPingInPingInterval = true;
		}
		return isPingInPingInterval;
	}
	
	/**
	 * Return true if the ping is in ping interval
	 * @param p_ping
	 * @return
	 */
	public boolean isPingInPingInterval(int p_ping) {
		boolean isPingInPingInterval = false;
		if (p_ping >= _pingMin && p_ping <= _pingMax) {
			isPingInPingInterval = true;
		}
		return isPingInPingInterval;
	}
	
	/**
	 * Return true if the beam is in ping interval
	 * @param l_beam
	 * @return
	 */
	public boolean isBeamInBeamInterval(int l_beam) {
		boolean isBeamInBeamInterval = false;
		if (l_beam >= _beamMin && l_beam <= _beamMax) {
			isBeamInBeamInterval = true;
		}
		return isBeamInBeamInterval;
	}

	public int getPingLenght() {
		return _pingMax-_pingMin+1;
	}

	public int getBeamLenght() {
		return _beamMax-_beamMin+1;
	}

	public int getPingMin() {
		return _pingMin;
	}

	public void setPingMin(int _pingMin) {
		this._pingMin = _pingMin;
	}

	public int getPingMax() {
		return _pingMax;
	}

	public void setPingMax(int _pingMax) {
		this._pingMax = _pingMax;
	}

	public int getBeamMin() {
		return _beamMin;
	}

	public void setBeamMin(int _beamMin) {
		this._beamMin = _beamMin;
	}

	public int getBeamMax() {
		return _beamMax;
	}

	public void setBeamMax(int _beamMax) {
		this._beamMax = _beamMax;
	}
	
	
}