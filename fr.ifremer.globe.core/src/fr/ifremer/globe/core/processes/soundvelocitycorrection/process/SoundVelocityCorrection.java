package fr.ifremer.globe.core.processes.soundvelocitycorrection.process;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityService;
import fr.ifremer.globe.core.model.velocity.SoundVelocity;
import fr.ifremer.globe.core.model.velocity.SoundVelocityProfile;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.model.IDataProxy;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.model.SoundVelocityCorrectionParameters;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.model.SounderFileProxy;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.sounders.SounderDescription.Constructor;

/**
 * Process to correct sound velocity of a sounder file.
 */
public class SoundVelocityCorrection implements IProcessFunction {

	private Logger logger = LoggerFactory.getLogger(SoundVelocityCorrection.class);

	public static final String NAME = "Sound velocity correction";

	private List<ISounderNcInfo> inputFiles;

	// Progression weightings of tasks
	protected static final int WORK_LOAD = 10;
	protected static final int WORK_PROCESS = 100;
	protected static final int WORK_SAVE = 10;

	protected IProgressMonitor m_monitor;

	protected Set<Profile> timeProfileSet; // profile (time interval) from cut file or manually
	protected Set<Profile> timeProfileContainingPingSet;
	protected List<ISoundVelocityProfile> soundVelocityProfileList = new ArrayList<>();

	protected int m_nbCycles;
	protected int m_nbProcessedCycles;

	protected SoundVelocityCorrectionParameters m_parameters;
	protected TimeIntervalParameters m_timeItervalParameters;

	public SoundVelocityCorrection(SoundVelocityCorrectionParameters parameters,
			TimeIntervalParameters timeItervalParameters, Logger logger) {
		m_parameters = parameters;
		m_timeItervalParameters = timeItervalParameters;
		this.logger = logger;
	}

	public SoundVelocityCorrection(SoundVelocityCorrectionParameters parameters,
			TimeIntervalParameters timeItervalParameters) {
		m_parameters = parameters;
		m_timeItervalParameters = timeItervalParameters;
	}

	/**
	 * Sets input files
	 */
	public void setInputFiles(List<ISounderNcInfo> inputFiles) {
		this.inputFiles = inputFiles;
	}

	/**
	 * Sets input files (with a file names) (used by tests)
	 * 
	 * @throws GIOException
	 */
	public void setFileNameList(List<String> fileNameList) throws GIOException {
		List<ISounderNcInfo> inputFilesAsInfo = new ArrayList<>();
		for (String fileName : fileNameList) {
			Optional<IFileInfo> option = IFileService.grab().getFileInfo(fileName);
			if (option.isPresent()
					&& option.get().getContentType().isOneOf(ContentType.MBG_NETCDF_3, ContentType.XSF_NETCDF_4)) {
				inputFilesAsInfo.add((ISounderNcInfo) option.get());
			} else {
				throw new GIOException("Error : can't read input file : " + fileName);
			}
		}
		setInputFiles(inputFilesAsInfo);
	}

	/**
	 * Get monitor of the process
	 *
	 * @return
	 */
	public IProgressMonitor getMonitor() {
		return m_monitor;
	}

	/**
	 * Perform processing.
	 *
	 * @throws GException
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		this.logger = logger;
		// Loop on input files
		if (inputFiles != null) {
			loadConfiguration(monitor, inputFiles.size());
			for (ISounderNcInfo file : inputFiles) {
				processFile(file);
			}
		}
		return Status.OK_STATUS;
	}

	/**
	 * Retrieves the {@link ISoundVelocityData} for the given file.
	 */
	protected ISoundVelocityData getSoundVelocityData(String filename) throws GIOException {
		Optional<ISoundVelocityData> result = ISoundVelocityService.grab().getSoundVelocityData(filename);
		if (!result.isPresent()) {
			throw new GIOException("Convert to VEL error, input file not correct: " + filename);
		}
		return result.get();
	}

	/**
	 * Loads all needed configuration files.
	 *
	 * @param monitor
	 * @throws GException
	 */
	public void loadConfiguration(IProgressMonitor monitor, int numFilesToProcess) throws GException {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}
		m_monitor = monitor;

		m_nbCycles = 0;
		m_nbProcessedCycles = 0;

		// Init progression.
		monitor.setTaskName("Sound Velocity correction");
		monitor.beginTask("Sound Velocity correction running ...",
				(WORK_LOAD + WORK_PROCESS + WORK_SAVE) * numFilesToProcess);
		String[] velFileNameArray = m_parameters
				.getArrayProperty(SoundVelocityCorrectionParameters.PROPERTY_VEL_FILE_LIST);
		// Read vel files
		for (String velFileName : velFileNameArray) {
			ISoundVelocityData velData = getSoundVelocityData(velFileName);
			soundVelocityProfileList.addAll(velData.getSoundVelocityProfiles());
		}

		if (monitor.isCanceled()) {
			return;
		}

		if (m_timeItervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION)) {// cut file option
			String cutFileName = m_timeItervalParameters.getProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME);
			// Read cut file
			readCutFile(monitor, cutFileName);
		} else if (m_timeItervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION)) {
			// manual interval option
			timeProfileSet = new HashSet<>();
			long startDate = m_timeItervalParameters
					.getLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE);
			long endDate = m_timeItervalParameters.getLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE);
			timeProfileSet.add(new Profile(startDate, endDate));
		} else { // no interval option

		}

	}

	protected void readCutFile(IProgressMonitor monitor, String cutFileName) throws FileFormatException {
		File cutFile = new File(cutFileName);
		timeProfileSet = new HashSet<>();
		try {
			timeProfileSet.addAll(ProfileUtils.loadProfilesFromFile(cutFile));
		} catch (Exception e) {
			monitor.setTaskName("Error : can't read this file as a cut file : \n" + cutFileName);
			throw new FileFormatException("Error : can't read this file as a cut file : \n" + cutFileName);
		}

	}

	/**
	 * Process correction for a file
	 *
	 * @param monitor
	 * @param fileName
	 * @throws GIOException
	 */
	public void processFile(ISounderNcInfo fileInfo) throws GIOException {
		IProgressMonitor monitor = m_monitor;

		try (SounderFileProxy dataProxy = new SounderFileProxy(fileInfo)) {
			monitor.worked(WORK_LOAD);

			monitor.subTask("Data processing...");
			processPings(monitor, dataProxy);

			monitor.subTask("Saving data...");
			dataProxy.saveData();
			IFileService.grab().updateStatistics(fileInfo);

			monitor.worked(WORK_SAVE);
			monitor.subTask("Number of processed cycles : " + m_nbProcessedCycles + " on " + m_nbCycles);
			logger.info("Processed :{} ({} cycles )", fileInfo.getFilename(), m_nbProcessedCycles);
		}
	}

	protected void processPings(IProgressMonitor monitor, IDataProxy dataProxy) throws GIOException {
		int nbProcessedCycles = 0;
		boolean done = false;
		timeProfileContainingPingSet = new HashSet<>();
		int antennaCount = dataProxy.getInfo().getRxParameters().size();
		// progress step
		monitor.subTask("Performing sound velocity correction ... ");
		int workNb = (int) Math.ceil(antennaCount * dataProxy.getInfo().getCycleCount() / 100.0);

		// Loop on pings
		for (int iPing = dataProxy.getPingBeamLimits().getPingMin(); dataProxy.getPingBeamLimits()
				.isPingInPingInterval(iPing) && !done; iPing++) {

			final Instant pingInstant = DateUtils.epochNanoToInstant(dataProxy.getEpochTimeNano(iPing));
			final double surfaceSoundSpeed = dataProxy.getSurfaceSoundSpeed(iPing);

			// Detect if ping is in a time interval and if is in ping min/max
			if (ProfileUtils.isPingInTimeInterval(pingInstant, timeProfileSet, timeProfileContainingPingSet)) {
				// Location of ping
				final double pingLat = dataProxy.getSwathLat(iPing);
				final double pingLon = dataProxy.getSwathLon(iPing);

				// Compute the interpolated soundvelocity profile
				ISoundVelocityProfile interpolatesSoundVelocityProfile = computeInterpolateProfile(dataProxy, pingInstant.toEpochMilli(),
						pingLat, pingLon);

				// Algo CARAIBES, ComputeSoundingDepthsByTimes.cc
				// ***************************************************************************
				// SPG : ComputeSoundingDepthsByTimes
				// ROL : Calcule des profondeurs sondees et des distances transverses pour les
				// faisceaux d un cycle a partir du temps de parcours du signal de l angle
				// d emission
				// Algo CARAIBES, ComputeSoundingDepthsByTimes.cc
				// ***************************************************************************
				int l_nbLevels; // Nombre de niveaux du profil
				List<SoundVelocity> soundVelocities; // Profondeurs et celerit�s des niveaux
				int l_level; // Rang du niveau traite
				int l_levelBegin = 0; // Rang du niveau de depart
				double l_depth; // Profondeur du niveau traite
				double l_cumulated_across_dist; // Distance transverse du niveau traite
				double l_velocity; // Celerite au niveau traite
				double l_sinus; // Sinus angle d'emission
				double l_cosinus; // Sinus angle d'emission
				double l_previousDepth; // Profondeur du niveau precedent
				double l_previousVelocity; // Celerite au niveau precedent
				double l_previousSinus; // Sinus angle d'emission niveau precedent
				double l_previousCosinus; // Cosinus angle d'emission niveau precedent
				double l_distance; // Distance parcourue par le signal
				double l_distanceAccross; // Distance transverse entre niveaux
				double l_timeOb; // Temps de parcour oblique entre niveau
				double l_computedTime; // Temps pour atteindre le niveau courant
				double l_previousComputedTime; // Temps pour atteindre le niveau precedent
				double l_sinAlong2; // Rapport
				double baseImm = dataProxy.getImmersion(iPing); // Immersion
				Map<Integer, Double> correctionAngles = Collections.emptyMap();
				if (Activator.getSoundVelocityCorrPrefs().getCorrectionAngles().getValue()) {
					correctionAngles = ComputeCorrectionAngles(dataProxy, iPing, baseImm);
				}

				// Recuperation des donnees du profil de celerite
				l_nbLevels = interpolatesSoundVelocityProfile.getSoundVelocities().size();
				l_level = 0;
				soundVelocities = interpolatesSoundVelocityProfile.getSoundVelocities();

				// Recherche du 1er niveau d'immersion apres l'immersion de depart
				for (l_level = 0; l_level < l_nbLevels; l_level++) {
					if (baseImm < soundVelocities.get(l_level).getDepth()) {
						if (l_level <= 1) {
							l_levelBegin = 1;
						} else {
							l_levelBegin = l_level;
						}
						break;
					}
				}

				boolean l_processSounding; // Flag de traitement d'une sonde

				for (int iBeam = dataProxy.getPingBeamLimits().getBeamMin(); iBeam <= dataProxy.getPingBeamLimits()
						.getBeamMax(); iBeam++) {
					double depth_origin = dataProxy.getDepth(iPing, iBeam);
					l_processSounding = dataProxy.isTobeProcessed(iPing, iBeam);
					if (l_processSounding) {

						final double oneWayTravelTime = dataProxy.getTwoWayTravelTime(iPing, iBeam) / 2;
						double accrossAngle;
						// double degreeradian = (double)3.141592653589793238/(double)180.0; // CALCUL
						// DE CARAIBES
						if (!correctionAngles.isEmpty()) { // Angle correction
							accrossAngle = Math.toRadians(correctionAngles.get(iBeam)); // Angles emission rad
						} else {
							accrossAngle = Math.toRadians(dataProxy.getAcrossAngle(iPing, iBeam));
						}
						final double alongDistance = dataProxy.getAlongDistance(iPing, iBeam)
								- dataProxy.getInfo().getTxParameters().xoffset;

						// Cas particulier des profondeur a 0
						if (oneWayTravelTime <= 0 || accrossAngle > Math.PI / 2) {
							dataProxy.setDepth(iPing, iBeam, Float.NaN);
						} else {
							l_previousComputedTime = 0;
							l_previousSinus = Math.sin(accrossAngle);
							l_previousCosinus = Math.sqrt(1 - l_previousSinus * l_previousSinus);
							l_cumulated_across_dist = 0;
							l_depth = -dataProxy.getDepth(iPing, iBeam) - baseImm;// depthCaraibes = -depthGlobe
							l_sinAlong2 = alongDistance * alongDistance / l_depth / l_depth;
							l_previousDepth = baseImm;

							l_previousVelocity = surfaceSoundSpeed;// soundVelocities.get(l_levelBegin -
																	// 1).getSoundVelocity();

							// Calcul avec cumul sur chaque niveau
							for (l_level = l_levelBegin; l_level < l_nbLevels; l_level++) {
								// Lecture des valeurs du niveau
								double l_depth_svp = soundVelocities.get(l_level).getDepth();
								l_velocity = soundVelocities.get(l_level).getSoundVelocity();
								// ****************************************************
								// Controle dernier niveau < a la profondeur desire ou
								// dernier niveau atteint.
								// Si PB : on considere la celerite constante du dernier
								// niveau a la profondeur a atteindre
								// ****************************************************
								if ((l_level + 1) >= l_nbLevels || l_velocity == 0 || Double.isNaN(l_velocity)) {
									l_depth_svp = 9999999;
									l_velocity = l_previousVelocity;
								}

								// Calcul du temps de propagation pour le niveau de celerite
								l_sinus = l_previousSinus * l_velocity / l_previousVelocity;

								if (l_sinus >= 1) {
									dataProxy.setDepth(iPing, iBeam, Float.NaN);
									break;
								} else {
									if (l_previousCosinus == 0) {
										dataProxy.setDepth(iPing, iBeam, Float.NaN);
										break;
									} else {
										l_cosinus = Math.sqrt(1 - l_sinus * l_sinus);
										l_distance = (l_depth_svp - l_previousDepth) / l_previousCosinus;

										l_distanceAccross = l_distance * l_previousSinus;
										l_timeOb = l_distance / l_previousVelocity;
										l_computedTime = l_previousComputedTime + l_timeOb;

										if (oneWayTravelTime > l_computedTime) {
											// ********************************************
											// Si temps non atteind : prise en compte du temps
											// de parcours pour le niveau
											// ********************************************
											l_previousComputedTime = l_computedTime;
											l_cumulated_across_dist = l_cumulated_across_dist + l_distanceAccross;
											l_previousDepth = l_depth_svp;
											l_previousVelocity = l_velocity;
											l_previousSinus = l_sinus;
											l_previousCosinus = l_cosinus;
										} else {
											// ********************************************
											// Si temps atteind : calcul des resultats
											// ********************************************
											double l_ratio_f = (oneWayTravelTime - l_previousComputedTime) / l_timeOb;

											double newDepth = l_previousDepth
													+ (l_depth_svp - l_previousDepth) * l_ratio_f;
											/**
											 * + ((SounderDriverInfo) dataProxy.getInfo()).getAntennaOffset()[2];
											 * 
											 * FIXME : deprecated code, to keep here ? antennaOffest = {0,0,0} in
											 * SounderDriverInfo. getAntennaOffset() documentation : Offset a appliquer
											 * au fichier si les données ne sont pas référencées dans le repere navire.
											 * Utilisé par caraibes uniquement et mis à zéro de facon systématique pour
											 * les sondeurs ifremer. Attention ne pas confondre avec la position du
											 * transducteur.
											 */
											newDepth = -newDepth; // depthCaraibes = -depthGlobe

											double acrossDistance = l_cumulated_across_dist
													+ l_distanceAccross * l_ratio_f;
											/**
											 * // add shiftY acrossDistance = acrossDistance + ((SounderDriverInfo)
											 * dataProxy.getInfo()).getAntennaOffset()[1];
											 * 
											 * FIXME : deprecated code, to keep here ? antennaOffest = {0,0,0} in
											 * SounderDriverInfo. getAntennaOffset() documentation : Offset a appliquer
											 * au fichier si les données ne sont pas référencées dans le repere navire.
											 * Utilisé par caraibes uniquement et mis à zéro de facon systématique pour
											 * les sondeurs ifremer. Attention ne pas confondre avec la position du
											 * transducteur.
											 */

											dataProxy.setDepth(iPing, iBeam, newDepth);
											dataProxy.setAcrossDistance(iPing, iBeam, acrossDistance);
											break;
										}
									}
								}
							}
						}
					}
				}

				nbProcessedCycles++;
			}

			if (iPing > dataProxy.getPingBeamLimits().getPingMax()) {
				done = true;
			}

			if (iPing % workNb == 0) {
				monitor.worked(1);
			}

		}

		nbProcessedCycles += timeProfileContainingPingSet.size();
		m_nbProcessedCycles = nbProcessedCycles / dataProxy.getInfo().getRxParameters().size();
	}

	/**
	 * Compute the interpolated SoundVelocity Prolfile
	 *
	 * @param mbgInfo
	 * @param iPing
	 * @param pingDate
	 * @param pingLat
	 * @param pingLon
	 * @param ellipsoid
	 * @return interpolatesSoundVelocityProfile
	 */
	private ISoundVelocityProfile computeInterpolateProfile(IDataProxy dataProxy, long pingTimeMs, double pingLat,
			double pingLon) {
		SoundVelocityProfile interpolatesSoundVelocityProfile = null; // Profile interpol�

		// ***************************************************************************
		// SPG : InterpolateProfile
		// ROL : Interpolation en temps et/ou en distance des profils
		// (Algo CARAIBES, Interpolate.cc)
		// **************************************************************************
		double l_firstNearestProfileDist = 0; // Distance du profil le plus proche au cycle
		double l_secondNearestProfileDist = 0; // Distance du second profil le plus proche au cycle
		int l_firstNearestProfileRange = 0; // Rang du profil le plus proche du cycle
		int l_secondNearestProfileRange = 0; // Rang du deuxieme profil le plus proche du cycle
		int l_profileRange; // Rang du profil traite
		// double depth; // profondeur courante
		float depth; // profondeur courante
		// double soundVelocityValue; // celerit� courante
		float soundVelocityValue; // celerit� courante
		int l_nbLevels; // Nombre de niveaux du profil interpole
		int l_level; // Rang du niveau traite
		double l_ratio1; // Coefficient
		double l_ratio2; // Coefficient

		// Recherche des deux profils les plus proche (Algo CARAIBES, Interpolate.cc)
		if (m_parameters.getBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_TIME_INTERPOLATION_OPTION)) { // sound
			// velocity profil time interpolation
			long l_profileTime; // Date profil traite

			// pour les temps
			if (soundVelocityProfileList.size() == 1) {
				l_firstNearestProfileRange = 0;
				l_firstNearestProfileDist = Math
						.abs((soundVelocityProfileList.get(l_firstNearestProfileRange).getDate().getTime() - pingTimeMs)
								/ 1000.);
				l_secondNearestProfileRange = 0;
				l_secondNearestProfileDist = 0;
			} else {
				l_firstNearestProfileRange = -1;
				l_firstNearestProfileDist = 0;

				for (l_profileRange = 1; l_profileRange < soundVelocityProfileList.size(); l_profileRange++) {
					l_profileTime = soundVelocityProfileList.get(l_profileRange).getDate().getTime();
					// pour les temps
					if (l_profileTime >= pingTimeMs) {
						l_firstNearestProfileRange = l_profileRange - 1;
						l_firstNearestProfileDist = Math
								.abs((soundVelocityProfileList.get(l_firstNearestProfileRange).getDate().getTime()
										- pingTimeMs) / 1000.);
						l_secondNearestProfileRange = l_profileRange;
						l_secondNearestProfileDist = Math.abs((l_profileTime - pingTimeMs) / 1000.);
						break;
					}
				}
				if (l_firstNearestProfileRange == -1) {
					l_firstNearestProfileRange = soundVelocityProfileList.size() - 2;
					l_firstNearestProfileDist = Math.abs(
							(soundVelocityProfileList.get(l_firstNearestProfileRange).getDate().getTime() - pingTimeMs)
									/ 1000.);
					l_secondNearestProfileRange = soundVelocityProfileList.size() - 1;
					l_secondNearestProfileDist = Math.abs(
							(soundVelocityProfileList.get(l_secondNearestProfileRange).getDate().getTime() - pingTimeMs)
									/ 1000.);
				}
			}

			// Calcul du profil interpole (Algo CARAIBES, Interpolate.cc)
			l_nbLevels = soundVelocityProfileList.get(l_firstNearestProfileRange).getSoundVelocities().size();

			l_ratio1 = l_secondNearestProfileDist / (l_firstNearestProfileDist + l_secondNearestProfileDist);
			l_ratio2 = 1 - l_ratio1;

			List<SoundVelocity> soundVelocityList = new ArrayList<>();
			for (l_level = 0; l_level < l_nbLevels; l_level++) {

				double secondNearestProfileDepth; // check if profile have diffenret nbrLevels
				if (l_level < soundVelocityProfileList.get(l_secondNearestProfileRange).getSoundVelocities().size()) {
					secondNearestProfileDepth = soundVelocityProfileList.get(l_secondNearestProfileRange)
							.getSoundVelocities().get(l_level).getDepth();
				} else {
					secondNearestProfileDepth = 0;
				}
				depth = (float) (l_ratio1 * soundVelocityProfileList.get(l_firstNearestProfileRange)
						.getSoundVelocities().get(l_level).getDepth() + l_ratio2 * secondNearestProfileDepth);

				double secondNearestProfileSoundVelocity; // check if profile have diffenret nbrLevels
				if (l_level < soundVelocityProfileList.get(l_secondNearestProfileRange).getSoundVelocities().size()) {
					secondNearestProfileSoundVelocity = soundVelocityProfileList.get(l_secondNearestProfileRange)
							.getSoundVelocities().get(l_level).getSoundVelocity();
				} else {
					secondNearestProfileSoundVelocity = 0;
				}
				soundVelocityValue = (float) (l_ratio1 * soundVelocityProfileList.get(l_firstNearestProfileRange)
						.getSoundVelocities().get(l_level).getSoundVelocity()
						+ l_ratio2 * secondNearestProfileSoundVelocity);

				SoundVelocity soundVelocityObject = new SoundVelocity(soundVelocityValue, depth);
				soundVelocityList.add(soundVelocityObject);
			}
			Date interpolateDate = new Date((long) (l_ratio1
					* soundVelocityProfileList.get(l_firstNearestProfileRange).getDate().getTime()
					+ l_ratio2 * soundVelocityProfileList.get(l_secondNearestProfileRange).getDate().getTime()));

			interpolatesSoundVelocityProfile = new SoundVelocityProfile(soundVelocityList, interpolateDate);

			return interpolatesSoundVelocityProfile;

		} else if (m_parameters.getBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_NEAREST_NEIGHTBOR_OPTION)) { // sound
			// velocity
			// profil
			// close
			// neighboor

			double l_distance; // Distance d un profil au cycle

			// Recherche du profil le plus proche (Algo CARAIBES, Corat.cc)
			for (l_profileRange = 0; l_profileRange < soundVelocityProfileList.size(); l_profileRange++) {

				double profileLat = soundVelocityProfileList.get(l_profileRange).getLat();
				double profileLon = soundVelocityProfileList.get(l_profileRange).getLon();

				// Compute geodeticDistance
				l_distance = dataProxy.getEllipsoid().GetGeodeticDist(pingLat, pingLon, profileLat, profileLon);

				if (l_distance < l_firstNearestProfileDist || l_profileRange == 0) {
					l_firstNearestProfileDist = l_distance;
					l_firstNearestProfileRange = l_profileRange;
				}

			}

			return soundVelocityProfileList.get(l_firstNearestProfileRange);
		}
		return null;
	}

	/**
	 * Fixed angles If Simrad sounder : angular correction due to emission in a non
	 * vertical plane return original angles for the others sounders
	 *
	 * @param info           : metadata of the file
	 * @param cycleContainer : data store
	 * @param cycleIndex:    index of the current cycle in the file
	 * @param immersion      : immersion
	 * @return corrected angles by beam index ; return original angles for the sounder is not Simrad
	 * @throws GIOException
	 */
	private Map<Integer, Double> ComputeCorrectionAngles(final IDataProxy dataProxy, int cycleIndex, double immersion)
			throws GIOException {
		var correctedAcrossAngleByBeamIndex = new HashMap<Integer, Double>();
		for (int beamIndex = dataProxy.getPingBeamLimits().getBeamMin(); beamIndex <= dataProxy.getPingBeamLimits()
				.getBeamMax(); beamIndex++) {
			correctedAcrossAngleByBeamIndex.put(beamIndex, dataProxy.getAcrossAngle(cycleIndex, beamIndex));
		}
		if (dataProxy.getInfo().getConstructor().equalsIgnoreCase(Constructor.Kongsberg.name())) {
			// Simrad
			int midBeam = dataProxy.getInfo().getTotalRxBeamCount() / 2 + 1;
			double correctionFactor = 0.;
			// Port
			for (int beamIndex = 0; beamIndex < midBeam; beamIndex++) {
				double depth = dataProxy.getDepth(cycleIndex, beamIndex);
				double acrossAngle = Math.toRadians(dataProxy.getAcrossAngle(cycleIndex, beamIndex));
				double alongDistance = dataProxy.getAlongDistance(cycleIndex, beamIndex)
						- dataProxy.getInfo().getTxParameters().xoffset;
				double acrossDistance = dataProxy.getAcrossDistance(cycleIndex, beamIndex)
						- dataProxy.getInfo().getTxParameters().yoffset;

				if (!Double.isNaN(depth) && acrossAngle != 0) {
					// Depth - immersion
					double depthCorrected = -depth - immersion;
					double alongDistance2 = Math.pow(alongDistance, 2);

					double beta = Math.atan(acrossDistance / depthCorrected);
					double projected_ground = Math.sqrt(acrossDistance * acrossDistance + alongDistance2);
					double alpha = Math.atan(projected_ground / depthCorrected);

					correctionFactor = alpha / beta;
					correctionFactor = Math.abs(correctionFactor);
					break;
				}
			}
			if (correctionFactor != 0) {
				for (int beamIndex = dataProxy.getPingBeamLimits().getBeamMin(); beamIndex < midBeam; beamIndex++) {
					double acrossAngle = Math.toRadians(dataProxy.getAcrossAngle(cycleIndex, beamIndex));
					correctedAcrossAngleByBeamIndex.put(beamIndex, Math.toDegrees(acrossAngle * correctionFactor));
				}
			}
			correctionFactor = 0.;
			// Starboard
			for (int beamIndex = dataProxy.getPingBeamLimits().getBeamMax(); beamIndex >= midBeam; beamIndex--) {
				double depth = dataProxy.getDepth(cycleIndex, beamIndex);
				double acrossAngle = Math.toRadians(dataProxy.getAcrossAngle(cycleIndex, beamIndex));
				double alongDistance = dataProxy.getAlongDistance(cycleIndex, beamIndex)
						- dataProxy.getInfo().getTxParameters().xoffset;
				double acrossDistance = dataProxy.getAcrossDistance(cycleIndex, beamIndex)
						- dataProxy.getInfo().getTxParameters().yoffset;
				if (!Double.isNaN(depth) && acrossAngle != 0) {
					// Depth - immersion
					double depthCorrected = -depth - immersion;
					double alongDistance2 = Math.pow(alongDistance, 2);

					double beta = Math.atan(acrossDistance / depthCorrected);
					double projected_ground = Math.sqrt(acrossDistance * acrossDistance + alongDistance2);
					double alpha = Math.atan(projected_ground / depthCorrected);

					correctionFactor = alpha / beta;
					correctionFactor = Math.abs(correctionFactor);
					break;
				}
			}
			for (int beamIndex = dataProxy.getPingBeamLimits().getBeamMax(); beamIndex >= midBeam; beamIndex--) {
				double acrossAngle = Math.toRadians(dataProxy.getAcrossAngle(cycleIndex, beamIndex));
				correctedAcrossAngleByBeamIndex.put(beamIndex, Math.toDegrees(acrossAngle * correctionFactor));
			}
		}
		return correctedAcrossAngleByBeamIndex;
	}

}
