package fr.ifremer.globe.core.processes.soundvelocitycorrection.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import fr.ifremer.globe.core.processes.ProcessParameters;

public class SoundVelocityCorrectionParameters extends ProcessParameters {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parameter file name.
	 */
	public static final String SOUNDVELOCITYCORRETION_PARAMETERS_FILE_NAME = "SoundVelocityCorrection.properties";

	/**
	 * Sound Velocity Correction list of file.
	 */
	public static final String PROPERTY_VEL_FILE_LIST = "velFileList";

	/**
	 * Sound Velocity Correction Time interpolation Option.
	 */
	public static final String PROPERTY_TIME_INTERPOLATION_OPTION = "timeInterpolationOption";
	/**
	 * Time Interval Nearest space neighbor Option.
	 */
	public static final String PROPERTY_NEAREST_NEIGHTBOR_OPTION = "nearestNeightborOption";

	public SoundVelocityCorrectionParameters() {
		super();
	}

	/**
	 * Read parameters values.
	 * 
	 * @return
	 */
	@Override
	protected void initialize() {
		String paramFileName = getUserFilePath(SOUNDVELOCITYCORRETION_PARAMETERS_FILE_NAME);
		try {
			File f = new File(paramFileName);
			if (f.exists())
				load(new FileInputStream(paramFileName));
			else
				load(getDefaultParameterFilePath("fr.ifremer.globe.process.soundvelocity"));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Write parameters values.
	 * 
	 * @return
	 */
	public void save() {
		String paramFileName = getUserFilePath(SOUNDVELOCITYCORRETION_PARAMETERS_FILE_NAME);
		File f = new File(paramFileName);
		try {
			if (!f.exists()) {
				f.getParentFile().mkdirs();
			}
			store(new FileOutputStream(f), null);
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Restore default parameters values.
	 * 
	 * @return
	 */
	public void restoreDefault() {

		// noIntervalOption=true
		// manuallyOption=false
		// cutOption=false
		// suffixeResults=-biascorrection

		// String paramFileName = getDefaultParameterFilePath(Activator.getBundleName());
		// try {
		// load(new FileInputStream(paramFileName));
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}

}
