package fr.ifremer.globe.core.processes.soundvelocitycorrection;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;

	public class SoundVelocityCorrectionPreference  extends PreferenceComposite{
		private BooleanPreferenceAttribute correctionAngles;

		public SoundVelocityCorrectionPreference(PreferenceComposite father, String name) {
			super(father, name);
			correctionAngles = new BooleanPreferenceAttribute("correctionAngles", "Compute correction angles", true);
			
			this.declareAttribute(correctionAngles);
			load();
		}

		public BooleanPreferenceAttribute getCorrectionAngles() {
			return correctionAngles;
		}

	}
