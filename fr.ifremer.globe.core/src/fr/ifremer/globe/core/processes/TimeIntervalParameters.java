package fr.ifremer.globe.core.processes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Tide correction process (Cosima).
 * 
 */

public class TimeIntervalParameters extends ProcessParameters {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parameter file name.
	 */
	public static final String TIMEINTERVAL_PARAMETERS_FILE_NAME = "TimeInterval.properties";

	/**
	 * Time Interval CutFile Option.
	 */
	public static final String PROPERTY_CUT_OPTION = "cutOption";
	/**
	 * Time Interval Manually Option.
	 */
	public static final String PROPERTY_MANUALLY_OPTION = "manuallyOption";
	/**
	 * Time Interval No Interval Option.
	 */
	public static final String PROPERTY_NOINTERVAL_OPTION = "noIntervalOption";

	/**
	 * Cut file name for Time Interval Option = CUT.
	 */
	public static final String PROPERTY_CUT_FILE_NAME = "cutFilename";

	/**
	 * Start date time for manually time interval
	 */
	public static final String PROPERTY_STARTDATETIME_VALUE = "startDateTimeValue";

	/**
	 * End date time for manually time interval
	 */
	public static final String PROPERTY_ENDDATETIME_VALUE = "endDateTimeValue";

	public TimeIntervalParameters(String bundleName) {
		super(bundleName);
	}

	/**
	 * Read parameters values.
	 * 
	 * @return
	 */
	@Override
	protected void initialize() {
		String paramFileName = getUserFilePath(TIMEINTERVAL_PARAMETERS_FILE_NAME);
		try {
			File f = new File(paramFileName);
			if (f.exists())
				load(new FileInputStream(paramFileName));
			else
				load(getDefaultParameterFilePath(m_bundleName));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Write parameters values.
	 * 
	 * @return
	 */
	public void save() {
		String paramFileName = getUserFilePath(TIMEINTERVAL_PARAMETERS_FILE_NAME);
		File f = new File(paramFileName);
		try {
			if (!f.exists()) {
				f.getParentFile().mkdirs();
			}
			store(new FileOutputStream(f), null);
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * Restore default parameters values.
	 * 
	 * @return
	 */
	public void restoreDefault() {
		try {
			load(getDefaultParameterFilePath(m_bundleName));
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

}
