package fr.ifremer.globe.core.processes;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.service.datalocation.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessParameters extends Properties {

	public static final Logger LOGGER = LoggerFactory.getLogger(ProcessParameters.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	protected static final String USER_PARAMETERS_DIR_NAME = "userValues";

	protected String m_bundleName;

	/**
	 * Default constructor.
	 */
	public ProcessParameters() {
		super();
		initialize();
	}

	/**
	 * Constructor with bundleName parameter.
	 */
	public ProcessParameters(String bundleName) {
		super();
		m_bundleName = bundleName;
		initialize();
	}

	protected void initialize() {
	}

	static protected String getUserFilePath(String p_userFileName) {
		Location installLocation = Platform.getInstallLocation();
		URL urlLocation = installLocation.getURL();
		String filePath = urlLocation.getPath() + USER_PARAMETERS_DIR_NAME + "/" + p_userFileName;
		return filePath;
	}

	protected static InputStream getDefaultParameterFilePath(String bundleName) throws IOException {
		if (bundleName.contains("biascorrection"))
			return ProcessParameters.class.getResourceAsStream("biascorrection/defaultParameters.properties");
		if (bundleName.contains("depthcorrection"))
			return ProcessParameters.class.getResourceAsStream("depthcorrection/defaultParameters.properties");
		if (bundleName.contains("soundvelocity"))
			return ProcessParameters.class.getResourceAsStream("soundvelocitycorrection/defaultParameters.properties");
		if (bundleName.contains("merge"))
			return ProcessParameters.class.getResourceAsStream("merge/defaultParameters.properties");

		throw new IOException("Can not define defaultParameters.properties for " + bundleName);
	}

	public void setIntProperty(String key, int value) {
		setProperty(key, Integer.toString(value));
	}

	public int getIntProperty(String key) {
		String prop = getProperty(key);
		if (prop != null) {
			return Integer.parseInt(prop);
		} else {
			return 0;
		}
	}

	public void setDoubleProperty(String key, double value) {
		setProperty(key, Double.toString(value));
	}

	public double getDoubleProperty(String key) {
		String prop = getProperty(key);
		if (prop != null) {
			return Double.parseDouble(prop);
		} else {
			return 0.0;
		}
	}

	public void setLongProperty(String key, long value) {
		setProperty(key, Long.toString(value));
	}

	public long getLongProperty(String key) {
		String prop = getProperty(key);
		if (prop != null) {
			return Long.parseLong(prop);
		} else {
			return 0;
		}
	}

	public void setBoolProperty(String key, boolean value) {
		setProperty(key, Boolean.toString(value));
	}

	public boolean getBoolProperty(String key) {
		String prop = getProperty(key);
		if (prop != null) {
			return Boolean.parseBoolean(prop);
		} else {
			return false;
		}
	}

	public List<String> getListProperty(String key) {
		String prop = getProperty(key);
		return Arrays.asList(prop.split(";"));
	}

	public void setArrayProperty(String key, String[] value) {
		String prop = new String();
		for (String s : value) {
			prop += s + ";";
		}
		setProperty(key, prop);
	}

	public String[] getArrayProperty(String key) {
		String prop = getProperty(key);
		if (prop != null) {
			return prop.split(";");
		} else {
			return new String[0];
		}
	}
}
