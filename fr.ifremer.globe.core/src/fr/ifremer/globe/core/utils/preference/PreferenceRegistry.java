package fr.ifremer.globe.core.utils.preference;

import fr.ifremer.globe.core.utils.GlobeVersion;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;

/**
 * This class provide access to the root node of generic preference system.
 */
public class PreferenceRegistry {

	/** Key of the Globe version stored in preference file */
	public static final String GLOBE_VERSION_PREF_KEY = "globeVersion";

	private static PreferenceRegistry instance = new PreferenceRegistry();

	private PreferenceComposite rootnode;
	private PreferenceComposite globalSettingsNode;
	private PreferenceComposite colorsSettingsNode;

	private PreferenceComposite viewsSettingsNode;
	private PreferenceComposite editorsSettingsNode;
	private PreferenceComposite processesSettingsNode;

	/* root node is made once */
	private PreferenceRegistry() {
		rootnode = new PreferenceComposite("fr.ifremer.globe", "master") {
			/** {@inheritDoc} */
			@Override
			protected void write() {
				getEclipsePreferences().put(GLOBE_VERSION_PREF_KEY, GlobeVersion.VERSION);
				super.write();
			}
		};
		globalSettingsNode = new PreferenceComposite(rootnode, "General");
		colorsSettingsNode = new PreferenceComposite(globalSettingsNode, "Colors");
		viewsSettingsNode = new PreferenceComposite(rootnode, "Views");
		editorsSettingsNode = new PreferenceComposite(rootnode, "Editors");
		processesSettingsNode = new PreferenceComposite(rootnode, "Processes");

		int i = 0;
		for (GColor color : GColor.DEFAULT_COLORS) {
			colorsSettingsNode.declareAttribute(
					new ColorPreferenceAttribute(String.valueOf(i), String.format("Color %04d", i), color));
			i++;
		}

	}

	/** Return the version of Globe stored in the preference file */
	public static String getGlobeVersion() {
		return instance.rootnode.getEclipsePreferences().get(GLOBE_VERSION_PREF_KEY, "");
	}

	/**
	 * return an instance of Preference registry
	 *
	 * @return
	 */
	public static PreferenceRegistry getInstance() {
		return instance;
	}

	public PreferenceComposite getRootNode() {
		return rootnode;
	}

	public PreferenceComposite getGlobalSettingsNode() {
		return globalSettingsNode;
	}

	public PreferenceComposite getViewsSettingsNode() {
		return viewsSettingsNode;
	}

	public PreferenceComposite getEditorsSettingsNode() {
		return editorsSettingsNode;
	}

	public PreferenceComposite getProcessesSettingsNode() {
		return processesSettingsNode;
	}

	/**
	 * @return the {@link #colorsSettingsNode}
	 */
	public PreferenceComposite getColorsSettingsNode() {
		return colorsSettingsNode;
	}
}
