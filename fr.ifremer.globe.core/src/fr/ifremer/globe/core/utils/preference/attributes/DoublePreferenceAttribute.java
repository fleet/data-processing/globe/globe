package fr.ifremer.globe.core.utils.preference.attributes;

import org.osgi.service.prefs.Preferences;

public class DoublePreferenceAttribute extends PreferenceAttribute<Double> {

	public DoublePreferenceAttribute(String attributeKey, String displayName, Double defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		setValue(Double.parseDouble(node.get(this.getKey(), Double.valueOf(value).toString())), true);
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), (Double.valueOf(value)).toString());
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(Double.parseDouble(v), notify);

	}

}
