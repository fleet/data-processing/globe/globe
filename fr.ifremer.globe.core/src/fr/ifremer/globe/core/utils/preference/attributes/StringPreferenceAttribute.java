package fr.ifremer.globe.core.utils.preference.attributes;

import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

public class StringPreferenceAttribute extends PreferenceAttribute<String> {
	public StringPreferenceAttribute(String attributeKey, String displayName, String defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		boolean exists;
		try {
			exists = node.nodeExists(node.absolutePath() + this.getKey());
			if (!exists) {
				// System.out.println("node " + node.absolutePath() +
				// " does not exists");
			}
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}

		setValue(node.get(this.getKey(), defaultValue), true);
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), value);
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(v, notify);

	}

}
