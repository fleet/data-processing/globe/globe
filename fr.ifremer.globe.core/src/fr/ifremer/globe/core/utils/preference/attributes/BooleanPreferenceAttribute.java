package fr.ifremer.globe.core.utils.preference.attributes;

import org.osgi.service.prefs.Preferences;

public class BooleanPreferenceAttribute extends PreferenceAttribute<Boolean> {

	public BooleanPreferenceAttribute(String attributeKey, String displayName, Boolean defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		setValue(Boolean.valueOf(node.get(this.getKey(), value.toString())), true);
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), value.toString());
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(Boolean.valueOf(v), notify);

	}

}
