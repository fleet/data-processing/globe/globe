package fr.ifremer.globe.core.utils.preference.attributes;

import java.util.Observable;

import org.osgi.service.prefs.Preferences;

public abstract class PreferenceAttribute<T> extends Observable {
	String attributeKey;
	String displayName;
	T defaultValue;
	T value;

	PreferenceAttribute(String attributeKey, String displayName, T defaultValue) {
		this.attributeKey = attributeKey;
		this.displayName = displayName;
		this.defaultValue = defaultValue;
		this.value = defaultValue;
	}

	public String getKey() {
		return attributeKey;
	}

	public String getDisplayedName() {
		return displayName;
	}

	public abstract void read(Preferences node);

	public abstract void write(Preferences node);

	public T getValue() {
		return value;
	}

	public void setValue(T v, boolean notify) {
		if (!this.value.equals(v)) {
			value = v;
			this.setChanged();
		}
		if (notify) {
			notifyObservers();
		}
	}

	/***
	 * reset value to default value
	 */
	public void resetToDefault() {
		setValue(defaultValue, true);
	}

	/**
	 * Change the value of the attribute from a string representing the attribute value
	 * 
	 * @param v a string representing the attribute value
	 * @param notify
	 */
	public abstract void setValueAsString(String v, boolean notify);

	/** Getter of {@link #defaultValue} */
	public T getDefaultValue() {
		return defaultValue;
	}
}
