package fr.ifremer.globe.core.utils.preference.attributes;

import java.util.Date;

import org.osgi.service.prefs.Preferences;

public class DatePreferenceAttribute extends PreferenceAttribute<Date> {
	public DatePreferenceAttribute(String attributeKey, String displayName, Date defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		setValue(new Date(Long.valueOf(node.get(this.getKey(), String.valueOf(value.getTime())))), true);
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), String.valueOf(value.getTime()));
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(new Date(Long.parseLong(v)), notify);

	}

}
