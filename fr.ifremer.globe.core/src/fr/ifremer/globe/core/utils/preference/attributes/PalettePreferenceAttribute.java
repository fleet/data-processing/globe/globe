package fr.ifremer.globe.core.utils.preference.attributes;

/**
 * Color palette preference
 */
public class PalettePreferenceAttribute extends StringPreferenceAttribute {

	public PalettePreferenceAttribute(String attributeKey, String displayName, String defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}
}
