package fr.ifremer.globe.core.utils.preference.attributes;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.osgi.service.prefs.Preferences;

public class FileListPreferenceAttribute extends PreferenceAttribute<List<File>> {

	public FileListPreferenceAttribute(String attributeKey, String displayName) {
		super(attributeKey, displayName, new ArrayList<>());
	}

	@Override
	public void read(Preferences node) {
		setValueAsString(node.get(this.getKey(), ""), false);
	}

	@Override
	public void write(Preferences node) {
		StringBuilder stringBuilder = new StringBuilder();
		int size = value.size();
		for (int i = 0; i < size; i++) {
			stringBuilder.append(value.get(i));
			if (i < size - 1) {
				stringBuilder.append(File.pathSeparator);
			}
		}
		node.put(getKey(), stringBuilder.toString());
	}

	@Override
	public void setValueAsString(String valueAsString, boolean notify) {
		List<File> result = new ArrayList<>();
		if (valueAsString != null) {
			String[] paths = valueAsString.split(File.pathSeparator);
			Arrays.stream(paths).forEach(path -> result.add(new File(path)));

		}
		setValue(result, notify);
	}

}
