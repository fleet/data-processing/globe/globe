/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.utils.preference.attributes;

import org.osgi.service.prefs.Preferences;

/** Preference representing a number as an int or float, depending of the default value type */
public class NumberPreferenceAttribute extends PreferenceAttribute<Number> {
	public NumberPreferenceAttribute(String attributeKey, String displayName, Number defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	/** {@inheritDoc} */
	@Override
	public void read(Preferences node) {
		if (defaultValue instanceof Integer) {
			setValue(Integer.valueOf(node.get(this.getKey(), value.toString())), true);
		} else {
			setValue(Double.valueOf(node.get(this.getKey(), value.toString())), true);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void write(Preferences node) {
		node.put(getKey(), value.toString());
	}

	/** {@inheritDoc} */
	@Override
	public void setValueAsString(String v, boolean notify) {
		if (defaultValue instanceof Integer) {
			setValue(Integer.valueOf(v), notify);
		} else {
			setValue(Double.valueOf(v), notify);
		}
	}

}
