package fr.ifremer.globe.core.utils.preference.attributes;

import org.osgi.service.prefs.Preferences;

import fr.ifremer.globe.core.utils.color.GColor;

public class ColorPreferenceAttribute extends PreferenceAttribute<GColor> {

	public ColorPreferenceAttribute(String attributeKey, String displayName, GColor defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		String scolor = node.get(this.getKey(), value.toString());
		setValue(GColor.fromString(scolor), true);
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), value.toString());
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(GColor.fromString(v), notify);

	}

}
