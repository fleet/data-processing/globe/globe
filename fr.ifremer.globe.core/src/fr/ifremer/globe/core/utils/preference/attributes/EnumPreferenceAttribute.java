package fr.ifremer.globe.core.utils.preference.attributes;

import org.osgi.service.prefs.Preferences;

public class EnumPreferenceAttribute extends PreferenceAttribute<Enum<?>> {

	public EnumPreferenceAttribute(String attributeKey, String displayName, Enum<?> defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		String stringvalue = node.get(getKey(), value.toString());
		try {
			setValue(Enum.valueOf(value.getDeclaringClass(), stringvalue), true);
		} catch (IllegalArgumentException e) {
			setValue(defaultValue, true);
		}
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), value.toString());
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(Enum.valueOf(value.getDeclaringClass(), v), notify);
	}

}
