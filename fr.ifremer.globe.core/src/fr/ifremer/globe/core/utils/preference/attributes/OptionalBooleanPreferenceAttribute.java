/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.utils.preference.attributes;

/**
 * Kind of boolean preference with a third possible value "Undefined"
 */
public class OptionalBooleanPreferenceAttribute extends EnumPreferenceAttribute {

	public enum OptionalBoolean {
		Yes, No, Undefined
	}

	/**
	 * Constructor
	 */
	public OptionalBooleanPreferenceAttribute(String attributeKey, String displayName) {
		super(attributeKey, displayName, OptionalBoolean.Undefined);
	}

	/** @retun true when no choice has been made */
	public boolean isEmpty() {
		return getValue() == OptionalBoolean.Undefined;
	}

	/** Set a boolean value */
	public void setValue(boolean value) {
		setValue(value ? OptionalBoolean.Yes : OptionalBoolean.No, false);
	}

	/** @retun true when choice is OptionalBoolean.Yes */
	public boolean isTrue() {
		return getValue() == OptionalBoolean.Yes;
	}

	/** @retun true when choice is OptionalBoolean.No */
	public boolean isNo() {
		return getValue() == OptionalBoolean.No;
	}
}
