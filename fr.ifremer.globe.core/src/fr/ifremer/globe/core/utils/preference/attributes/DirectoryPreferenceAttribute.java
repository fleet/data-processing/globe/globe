package fr.ifremer.globe.core.utils.preference.attributes;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.osgi.service.prefs.Preferences;

public class DirectoryPreferenceAttribute extends PreferenceAttribute<Path> {

	public DirectoryPreferenceAttribute(String attributeKey, String displayName, Path defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		setValue(FileSystems.getDefault().getPath(node.get(this.getKey(), value.toString())), true);
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), value.toString());
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(FileSystems.getDefault().getPath(v), notify);
	}

}
