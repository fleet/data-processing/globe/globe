package fr.ifremer.globe.core.utils.preference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

/**
 * Composite is a given node in preference tree. On this node, we can add
 * several attribute node
 * */
public class PreferenceComposite extends Observable implements Observer {
	private static Logger logger = LoggerFactory.getLogger(PreferenceComposite.class);

	// map containing the list of attribute and their values
	private HashMap<String, PreferenceAttribute<?>> map = new HashMap<>();
	private Preferences node;

	/**
	 * Returns all of the keys that have an associated Attribute in this node.
	 * 
	 * @return an array of the keys that have an associated Attribute in this
	 *         node.
	 * */
	public String[] keys() {
		Set<String> in = map.keySet(); //keySet()
		String[] out = new String[in.size()];
		return in.toArray(out);
	}

	public String getAbsolutePath() {
		return node.absolutePath();
	}

	public String getNodeName() {
		return node.name();
	}

	private ArrayList<PreferenceComposite> childs = new ArrayList<>();

	public List<PreferenceComposite> getChildList() {
		return childs;
	}
	
	public PreferenceComposite getChild(String nodeName) {
		return childs.stream().filter(c -> nodeName.equals(c.getNodeName())).findFirst().orElse(null);
	}

	protected PreferenceComposite addChild(PreferenceComposite composite) {
		childs.add(composite);
		return composite;
	}

	/**
	 * create a composite and add it to its father
	 * */
	public PreferenceComposite(PreferenceComposite father, String name) {
		// we must not have 2 children with the same name to ensure data
		// consistency
		if (name.equals(""))
			throw new IllegalArgumentException("PreferenceComposite name is empty!");
		List<PreferenceComposite> childs = father.getChildList();
		for (PreferenceComposite c : childs) {
			if (c.getNodeName().equals(name))
				throw new IllegalArgumentException("PreferenceComposite with the name " + name + " already exist!");

		}
		node = father.node.node(name);
		father.addChild(this);
	}

	/**
	 * Create a stand alone preference composite this should only be used to
	 * create the master node (in PreferenceRegistry)
	 * */
	public PreferenceComposite(String masterNodeName, String name) {
		Preferences preferences = ConfigurationScope.INSTANCE.getNode(masterNodeName);
		node = preferences.node(name);

	}

	/**
	 * Add a declared attribute
	 * */
	public void declareAttribute(PreferenceAttribute<?> att) {
		if (map.containsKey(att.getKey())) {
			logger.error("Preference nodes " + att.getKey() + " already exists, ovewritting it");
		}
		map.put(att.getKey(), att);
		att.addObserver(this);
	}

	public PreferenceAttribute<?> getAttribute(String key) {
		return map.get(key);

	}

	/**
	 * Load data and child data
	 * */
	public void load() {
		for (PreferenceAttribute<?> t : map.values()) {
			t.read(node);
		}
		for (PreferenceComposite child : childs) {
			child.load();
		}
	}

	/**
	 * Save attribute data of the current node
	 */
	public void save() {
		write();
		try {
			node.flush();
		} catch (BackingStoreException e) {
			logger.error("Exception flushing PreferenceParameters preferences", e);

		}
		// recursive save
		for (PreferenceComposite c : childs)
			c.save();
	}

	/**
	 * Write our own attribute and call child's write
	 * */
	protected void write() {
		for (PreferenceAttribute<?> t : map.values()) {
			t.write(node);

		}
		for (PreferenceComposite child : childs) {
			child.write();
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		this.setChanged();
		notifyObservers();
	}

	/**
	 * @return the {@link #node}
	 */
	public Preferences getEclipsePreferences() {
		return node;
	}

}
