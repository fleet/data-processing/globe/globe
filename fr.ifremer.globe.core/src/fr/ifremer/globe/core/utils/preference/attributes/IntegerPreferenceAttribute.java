package fr.ifremer.globe.core.utils.preference.attributes;

import org.osgi.service.prefs.Preferences;

public class IntegerPreferenceAttribute extends PreferenceAttribute<Integer> {
	public IntegerPreferenceAttribute(String attributeKey, String displayName, Integer defaultValue) {
		super(attributeKey, displayName, defaultValue);
	}

	@Override
	public void read(Preferences node) {
		setValue(Integer.valueOf(node.get(this.getKey(), value.toString())), true);
	}

	@Override
	public void write(Preferences node) {
		node.put(getKey(), value.toString());
	}

	@Override
	public void setValueAsString(String v, boolean notify) {
		setValue(Integer.valueOf(v), notify);
	}

}
