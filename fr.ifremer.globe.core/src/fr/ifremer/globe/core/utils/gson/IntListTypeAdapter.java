package fr.ifremer.globe.core.utils.gson;

import java.io.IOException;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
 * GSon adapter to save and restore a IntList
 */
public class IntListTypeAdapter extends TypeAdapter<IntList> {

	@Override
	public void write(JsonWriter out, IntList value) throws IOException {
		out.beginArray();
		for (int i = 0; i < value.size(); i++) {
			out.value(value.get(i));
		}
		out.endArray();
	}

	@Override
	public IntList read(JsonReader in) throws IOException {
		in.beginArray();
		var result = new ArrayIntList();
		while (in.peek() == JsonToken.NUMBER)
			result.add(in.nextInt());
		in.endArray();
		return result;
	}

}
