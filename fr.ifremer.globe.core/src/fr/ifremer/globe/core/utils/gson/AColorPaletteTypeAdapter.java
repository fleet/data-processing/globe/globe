/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.utils.gson;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.core.utils.color.BitColorPalette;

/**
 * Adapt the serialization of a AColorPalette in a JSon format
 */
public class AColorPaletteTypeAdapter implements JsonSerializer<AColorPalette>, JsonDeserializer<AColorPalette> {

	public static final String BIT_COLOR_PALETTE_TYPE = "palette_for_bitfield";

	@Override
	public AColorPalette deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject jsonObject = json.getAsJsonObject();
		String paletteType = jsonObject.get("color_palette_type").getAsString();
		JsonElement palette = jsonObject.get("palette");
		if (BIT_COLOR_PALETTE_TYPE.equals(paletteType)) {
			return context.deserialize(palette, BitColorPalette.class);
		}

		throw new JsonParseException("Unknown palette type: " + paletteType);
	}

	@Override
	public JsonElement serialize(AColorPalette palette, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject result = new JsonObject();
		result.add("color_palette_type", new JsonPrimitive(palette.getType()));
		result.add("palette", context.serialize(palette, palette.getClass()));
		return result;
	}

}
