package fr.ifremer.globe.core.utils.gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/** GSon adapter to save and restore ByteBuffer */
public class GsonByteBufferTypeAdapter extends TypeAdapter<ByteBuffer> {

	/** File containing the ByteBuffer data */
	private final File byteBufferFile;

	/**
	 * Constructor
	 */
	public GsonByteBufferTypeAdapter(File byteBufferFile) {
		this.byteBufferFile = byteBufferFile;
	}

	/**
	 * Write ByteBuffer in file
	 * 
	 * @see com.google.gson.TypeAdapter#write(com.google.gson.stream.JsonWriter, java.lang.Object)
	 */
	@Override
	public void write(JsonWriter out, ByteBuffer value) throws IOException {
		out.value(byteBufferFile.getName());
		try (var bufferFos = new FileOutputStream(byteBufferFile, false)) {
			FileChannel bufferFc = bufferFos.getChannel();
			value.rewind();
			bufferFc.write(value);
			value.rewind();
		}
	}

	/**
	 * Restore ByteBuffer from file
	 * 
	 * @see com.google.gson.TypeAdapter#read(com.google.gson.stream.JsonReader)
	 */
	@Override
	public ByteBuffer read(JsonReader in) throws IOException {
		in.nextString();
		try (var bufferFis = new FileInputStream(byteBufferFile)) {
			FileChannel bufferFc = bufferFis.getChannel();
			ByteBuffer result = ByteBuffer.allocate((int) byteBufferFile.length()).order(ByteOrder.nativeOrder());
			bufferFc.read(result);
			result.rewind();
			return result;
		}
	}
}
