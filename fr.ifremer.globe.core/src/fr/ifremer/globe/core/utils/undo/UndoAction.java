/**
 * 
 */
package fr.ifremer.globe.core.utils.undo;


/**
 * Abstract class for Globe's undoable action.
 * 
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */
public abstract class UndoAction implements IUndoeable {

	private boolean undone = false;

	public boolean isUndone() {
		return undone;
	}

	@Override
	public final void undo() {
		if (undone == true) {
			return;
		}
		undone = true;
		executeUndo();
	}

	@Override
	public final void redo() {
		if (undone == false) {
			return;
		}

		undone = false;
		executeRedo();
	}

	protected abstract void executeUndo();

	protected abstract void executeRedo();

	/**
	 * Dispose associated resources if any
	 */
	public void dispose() {
	}
}
