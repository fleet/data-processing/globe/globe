package fr.ifremer.globe.core.utils.undo;

/**
 * Interface for undoeable actions.
 * 
 * @author gbourel
 */
public interface IUndoeable {

	/**
	 * Returns a short information message used by GUI to show which action may
	 * currently be undone or redone.
	 */
	public String msgInfo();

	public void undo();

	public void redo();
}