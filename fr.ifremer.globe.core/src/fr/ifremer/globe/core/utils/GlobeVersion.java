package fr.ifremer.globe.core.utils;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

public class GlobeVersion {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobeVersion.class);

	public static final String VERSION = "2.5.15";

	public static final String GLOBE_DOWNLOAD_WEBSITE = "https://doi.org/10.17882/70460";

	public static final String GLOBE_AVAILABLE_VERSION_PREF_KEY = "1_globeAvailableVersion";

	private static StringPreferenceAttribute availableVersionPreferenceAttribute;

	/**
	 * Constructor
	 */
	private GlobeVersion() {
		// static utility class
	}

	/**
	 * Adds globe web site & last available version in preferences
	 */
	public static void initPreferences() {
		PreferenceComposite generic = PreferenceRegistry.getInstance().getGlobalSettingsNode();
		generic.declareAttribute(
				new StringPreferenceAttribute("0_globeWebSite", "Globe web site", GlobeVersion.GLOBE_DOWNLOAD_WEBSITE));

		availableVersionPreferenceAttribute = new StringPreferenceAttribute(GLOBE_AVAILABLE_VERSION_PREF_KEY,
				"Version available on web site ", "unknow");
		generic.declareAttribute(availableVersionPreferenceAttribute);
	}

	/**
	 * Gets the available version saved in preference.
	 */
	public static Optional<String> getAvailableVersionSavedInPreferences() {
		if (availableVersionPreferenceAttribute != null) {
			String availableVersionSavedInPreferences = availableVersionPreferenceAttribute.getValue();
			return availableVersionSavedInPreferences.equals("unknow") ? Optional.empty()
					: Optional.of(availableVersionSavedInPreferences);
		}
		return Optional.empty();
	}

	/**
	 * Sets the available version saved in preference.
	 */
	public static void setAvailableVersionSavedInPreferences(String newValue) {
		if (availableVersionPreferenceAttribute != null) {
			availableVersionPreferenceAttribute.setValue(newValue, true);
			PreferenceRegistry.getInstance().getGlobalSettingsNode().save();
		}
	}

	/**
	 * Checks if a new version of Globe is available (by scanning seonoe web site).
	 *
	 * @return an Optional with the new version, or empty if there is not.
	 * @throws IOException
	 */
	public static Optional<String> checkIfNewVersionAvailable() {
		Pattern p = Pattern.compile("Globe Windows V(\\d{1,2}\\.\\d{1,2}\\.\\d{1,2})");
		try {
			// The DOI URL is redirected twice...
			URL url = getRedirectionURL(getRedirectionURL(new URL(GLOBE_DOWNLOAD_WEBSITE)));
			try (Scanner scan = new Scanner(url.openStream())) {
				while (scan.hasNext()) {
					Matcher matcher = p.matcher(scan.nextLine());
					if (matcher.find()) {
						String availableVersion = matcher.group(1);
						// Check if the available version is more recent
						return compareVersions(availableVersion, VERSION) ? Optional.of(availableVersion)
								: Optional.empty();
					}
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Error while checking Globe version : {}", e.getMessage(), e);
		}
		return Optional.empty();
	}

	/**
	 * @return the redirection URL
	 */
	private static URL getRedirectionURL(URL srcURL) throws IOException {
		Pattern pattern = Pattern.compile("<a href=\"(.*?)\">");
		try (Scanner scan = new Scanner(srcURL.openStream())) {
			while (scan.hasNext()) {
				Matcher matcher = pattern.matcher(scan.nextLine());
				if (matcher.find())
					return new URL(matcher.group(1));
			}
		}
		return srcURL;
	}

	/**
	 * @return true is the first version is more recent that the second.
	 *
	 *         Version are expected as : MAJOR.MINOR.RELEASE (= 1.14.2 for example)
	 */
	private static boolean compareVersions(String versionA, String versionB) {
		String[] a = versionA.split("\\.");
		int majorA = Integer.parseInt(a[0]);
		int minorA = Integer.parseInt(a[1]);
		int releaseA = Integer.parseInt(a[2]);
		String[] b = versionB.split("\\.");
		int majorB = Integer.parseInt(b[0]);
		int minorB = Integer.parseInt(b[1]);
		int releaseB = Integer.parseInt(b[2]);

		return majorA > majorB // new major version
				|| (majorA == majorB && minorA > minorB) // new minor version
				|| (majorA == majorB && minorA == minorB && releaseA > releaseB); // new release
	}

}
