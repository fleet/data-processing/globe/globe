/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.utils.color;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.primitives.IntList;

import com.google.gson.annotations.JsonAdapter;

import fr.ifremer.globe.core.utils.gson.AColorPaletteTypeAdapter;

/**
 * Color palette.
 * 
 * It is a sealed class to have control over the hierarchy and thus facilitate serialization in JSon
 * AColorPaletteTypeAdapter
 */
@JsonAdapter(AColorPaletteTypeAdapter.class)
public abstract sealed class AColorPalette permits BitColorPalette {

	/** Defined colors of the palette */
	private final List<Color> colors = new LinkedList<>();
	/** The last generated color map (used by shader) */
	private transient /* Do not serialize */ Optional<IntList> colorMap = Optional.empty();

	/**
	 * Add a color to the palette
	 */
	public void addColor(String key, boolean active, Number value, GColor gColor) {
		colorMap = Optional.empty(); // to force subclass to
		colors.add(new Color(key, active, value, gColor));
	}

	/**
	 * Change the GColor of the specified Color in the palette
	 */
	public void changeColor(Color colorToReplace, GColor gColor) {
		synchronized (colors) {
			int index = colors.indexOf(colorToReplace);
			if (index >= 0) {
				colorMap = Optional.empty(); // to force subclass to
				colors.set(index, colorToReplace.with(gColor));
			}
		}
	}

	/**
	 * Change the activation flag of the specified Color in the palette
	 */
	public void activateColor(Color colorToActivate, boolean activate) {
		synchronized (colors) {
			int index = colors.indexOf(colorToActivate);
			if (index >= 0) {
				colorMap = Optional.empty(); // to force subclass to
				colors.set(index, colorToActivate.with(activate));
			}
		}
	}

	/**
	 * @return the {@link #colors}
	 */
	public List<Color> getColors() {
		synchronized (colors) {
			return colors;
		}
	}

	/** ColorMap as required in Shader */
	public IntList toColorMap() {
		IntList result = colorMap.orElse(null);
		if (result == null) {
			result = generateColorMap();
			colorMap = Optional.of(result);
		}
		return result;
	}

	/** Only subclass can deal with the colormap generation */
	protected abstract IntList generateColorMap();

	/** Return the minimal value of the color */
	public double getMinValue() {
		return colors.stream().mapToDouble(c -> c.value().doubleValue()).min().orElse(0d);
	}

	/** Return the maximal value of the color */
	public double getMaxValue() {
		return colors.stream().mapToDouble(c -> c.value().doubleValue()).max().orElse(0d);
	}

	/** @Return a simple String representing the type of palette */
	public String getType() {
		// Waiting for Pattern Matching for switching Sealed Classes... (preview in java 17)
		if (this instanceof BitColorPalette) {
			return AColorPaletteTypeAdapter.BIT_COLOR_PALETTE_TYPE;
		}
		throw new UnsupportedOperationException(
				"Implementation error, " + this.getClass().getCanonicalName() + " not managed");
	}

	/**
	 * One color of the palette
	 */
	public static record Color(String name, boolean active, Number value, GColor gColor) {

		/** Creates a the same instance of Color with an other GColor */
		public Color with(GColor gColor) {
			return new Color(name, active, value, gColor);
		}

		/** Creates a the same instance of Color with the specified activation flag */
		public Color with(boolean active) {
			return new Color(name, active, value, gColor);
		}
	}

}
