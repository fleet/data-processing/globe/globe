package fr.ifremer.globe.core.utils.color;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.function.Supplier;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;

/**
 * This class handles a list of default colors and provides them one by one.
 */
public class ColorProvider implements Supplier<GColor> {

	/** Defines colors **/
	private class ColorWrapper {

		/** Properties **/
		private GColor color;
		private AtomicBoolean isUsed = new AtomicBoolean();

		/** Constructor **/
		public ColorWrapper(GColor color) {
			this.color = color;
		}
	}

	private List<ColorWrapper> colorWrappers = new ArrayList<>();

	public ColorProvider() {
		PreferenceComposite colorsPreferences = PreferenceRegistry.getInstance().getGlobalSettingsNode()
				.getChild("Colors");
		int colorsCount = colorsPreferences.keys().length;
		for (int i = 0; i < colorsCount; i++) {
			colorWrappers.add(new ColorWrapper((GColor) colorsPreferences.getAttribute(String.valueOf(i)).getValue()));
		}
	}

	/**
	 * @return an available color
	 */
	@Override
	public GColor get() {
		// get an available color
		for (ColorWrapper colorWrapper : colorWrappers) {
			if (colorWrapper.isUsed.compareAndSet(false, true)) {
				return colorWrapper.color;
			}
		}
		// no color available: release all
		for (ColorWrapper colorWrapper : colorWrappers)
			colorWrapper.isUsed.set(false);
		return get();
	}

	public List<GColor> getColors() {
		return colorWrappers.stream().map(wrapper -> wrapper.color).toList();
	}

	public Optional<GColor> getColor(Predicate<GColor> filter) {
		return getColors().stream().filter(filter).findFirst();
	}

}
