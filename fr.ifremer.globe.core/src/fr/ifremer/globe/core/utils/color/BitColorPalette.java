/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.utils.color;

import java.util.stream.IntStream;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;

/**
 * Color palette for BitField.<br>
 * Each contained Color have :
 * <ul>
 * <li>name : name of the bit</li>
 * <li>value : the mask of bit with only one bit equals to 1</li>
 * </ul>
 * For an integer value, the colour will be that of the most significant bit
 */
public final class BitColorPalette extends AColorPalette {

	/** {@inheritDoc} */
	@Override
	protected IntList generateColorMap() {

		// index of the most significant bit
		int bitMax = getColors().stream()//
				.mapToInt(c -> c.value().intValue())//
				.max().orElse(0);

		int nbOfColors = bitMax << 1;
		IntList result = new ArrayIntList(nbOfColors);
		// Color when no bit is set, value is 0
		int color0 = getColors().stream()//
				.filter(c -> c.value().intValue() == 0 && c.active())//
				.map(Color::gColor)//
				.findFirst().orElse(GColor.TRANSPARENT).toColorMap();
		IntStream.range(0, nbOfColors).forEach(i -> result.add(color0));

		for (int shift = 0; shift < 8; shift++) {
			final int bit = 1 << shift;
			if (bit <= bitMax) {
				GColor color = getColors().stream()//
						.filter(c -> c.value().intValue() == bit && c.active())//
						.map(Color::gColor)//
						.findFirst().orElse(null);
				if (color != null) {
					IntStream.range(0, nbOfColors).forEach(i -> {
						if ((i & bit) == bit) {
							result.set(i, color.toColorMap());
						}
					});
				}
			}
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public double getMinValue() {
		int result = (int) super.getMinValue();
		return result << 1;
	}

	/** {@inheritDoc} */
	@Override
	public double getMaxValue() {
		int result = (int) super.getMaxValue();
		return result > 0 ? (result << 1) - 1 : 0;
	}

}
