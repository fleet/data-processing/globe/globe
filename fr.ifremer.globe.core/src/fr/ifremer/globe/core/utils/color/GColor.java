package fr.ifremer.globe.core.utils.color;

import static java.util.Map.entry;

import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A basic color class, agnostic of SWT, AWT, JavaFX It must not contain any
 * reference of graphic's class.
 */
public class GColor {

	/**
	 * Default colors (used by editors...)
	 */
	public static final GColor DEFAULT_BLUE = new GColor("#1A75AE"); // blue
	public static final GColor CORAL = new GColor("#FF7F50"); // coral
	public static final GColor DEFAULT_GREEN = new GColor("#4CAF50"); //
	public static final GColor CRIMSON = new GColor("#DC143C"); // crimson
	public static final GColor NAVY = new GColor("#000080"); // navy
	public static final GColor BROWN = new GColor("#A52A2A"); // brown
	public static final GColor DEFAULT_DARK_GREEN = new GColor("#006400"); // dark green
	public static final GColor MEDIUM_ORCHID = new GColor("#BA55D3"); // medium orchid
	public static final GColor DARK_CYAN = new GColor("#008B8B"); // dark cyan
	public static final GColor GOLD = new GColor("#FFD700"); // gold
	// "#228B22", // forest green
	// "#DB7093", // pale violet red
	// "#00CED1 ", // dark turquoise
	// "#DAA520", // golden rod
	// "#2F4F4F", // dark slate gray
	// "#D8BFD8", // thistle
	// "#4682B4", // steel blue
	// "#FF6347", // tomato
	// "#8FBC8F", // dark sea green
	// "#EE82EE", // violet

	public static final GColor LIGHT_GRAY = new GColor("#E0E0E0");
	public static final GColor GRAY = new GColor("#9E9E9E");
	public static final GColor DARK_GRAY = new GColor("#616161");

	public static final GColor LIGHT_RED = new GColor("#E57373");
	public static final GColor RED = new GColor("#F44336");
	public static final GColor DARK_RED = new GColor("#D32F2F");

	public static final GColor LIGHT_ORANGE = new GColor("#FFB74D");
	public static final GColor ORANGE = new GColor("#FF9800");
	public static final GColor DARK_ORANGE = new GColor("#F57C00");

	public static final GColor LIGHT_YELLOW = new GColor("#FFF176");
	public static final GColor YELLOW = new GColor("#FFEB3B");
	public static final GColor DARK_YELLOW = new GColor("#FBC02D");

	public static final GColor LIGHT_BLUE = new GColor("#64B5F6");
	public static final GColor BLUE = new GColor("#2196F3");
	public static final GColor DARK_BLUE = new GColor("#1976D2");

	public static final GColor LIGHT_GREEN = new GColor("#81C784");
	public static final GColor GREEN = new GColor("#4CAF50");
	public static final GColor DARK_GREEN = new GColor("#388E3C");

	public static final GColor WHITE = new GColor("#FFFFFF");
	public static final GColor BLACK = new GColor("#000000");
	public static final GColor PINK = new GColor("#FFAFAF");
	public static final GColor MAGENTA = new GColor("#FF00FF");
	public static final GColor FUCHSIA = new GColor("#FD3F92");
	public static final GColor TRANSPARENT = new GColor(0, 0, 0, 0);

	public static final GColor[] DEFAULT_COLORS = { BLUE, CORAL, DEFAULT_GREEN, CRIMSON, NAVY, BROWN, DARK_GREEN,
			MEDIUM_ORCHID, DARK_CYAN, GOLD };

	public static final GColor[] CONTRAST_PALETTE = { GColor.WHITE, GColor.BLUE, GColor.GREEN, GColor.FUCHSIA,
			GColor.RED, GColor.YELLOW, GColor.CORAL, GColor.CRIMSON, GColor.GOLD };

	/** Color properties (red, green, blue, alpha = transparency) **/
	private int r;
	private int g;
	private int b;
	private int a;

	/**
	 * Constructor
	 */
	public GColor(int r, int g, int b) {
		this(r, g, b, 255);
	}

	/**
	 * Constructor
	 */
	public GColor(int r, int g, int b, int a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/**
	 * Constructor with CSS Code
	 */
	public GColor(String colorString) {
		colorString = colorString.replace("#", ""); // remove #
		colorString = colorString.replace("0x", ""); // remove 0x
		if (colorString.length() >= 6) {
			r = Integer.parseInt(colorString.substring(0, 2), 16);
			g = Integer.parseInt(colorString.substring(2, 4), 16);
			b = Integer.parseInt(colorString.substring(4, 6), 16);
			a = colorString.length() == 8 ? Integer.parseInt(colorString.substring(6, 8), 16) : 255;
		}
	}

	public int getRed() {
		return this.r;
	}

	public int getBlue() {
		return this.b;

	}

	public int getGreen() {
		return this.g;
	}

	public int getAlpha() {
		return this.a;
	}

	public float getFloatRed() {
		return this.r / 255.f;
	}

	public float getFloatBlue() {
		return this.b / 255f;

	}

	public float getFloatGreen() {
		return this.g / 255f;
	}

	public float getFloatAlpha() {
		return this.a / 255f;
	}

	/**
	 * convert to an array of RGB.
	 */
	public float[] toRGB() {
		return new float[] { getRed() / 255f, getGreen() / 255f, getBlue() / 255f };
	}

	/**
	 * convert to int as expected by ColorMap in shaders.
	 */
	public int toColorMap() {
		return a * 256 * 256 * 256 + r * 256 * 256 + g * 256 + b;
	}

	@Override
	public String toString() {
		return r + "," + g + "," + b;
	}

	/**
	 * @return GColor from string (accepts #FFFFFFFF format, or rgb(255,255,255) or
	 *         rgba(255,255,255,1.0)...).
	 */
	public static Optional<GColor> parseString(String colorString) {
		// Try to parse hex color code (#AARRGGBB or 0xAARRGGBB).
		Matcher matcher = Pattern.compile("(?:0x|#)([a-fA-F\\d]{2})([a-fA-F\\d]{2})([a-fA-F\\d]{2})([a-fA-F\\d]{2})?")
				.matcher(colorString);
		if (matcher.matches()) {
			int offset = 0;
			int alpha = 255;
			if(matcher.group(4) != null) {
				// Case with alpha in first group
				offset = 1;
				alpha = Integer.parseInt(matcher.group(1), 16);
			}
			int red = Integer.parseInt(matcher.group(offset + 1), 16);
			int green = Integer.parseInt(matcher.group(offset + 2), 16);
			int blue = Integer.parseInt(matcher.group(offset + 3), 16);
			return Optional.of(new GColor(red, green, blue, alpha));
		}

		// Try to parse rgb(r,g,b) or rgba(r,g,b,a).
		matcher = Pattern.compile("rgba?\\((\\d{1,3}),\\s*(\\d{1,3}),\\s*(\\d{1,3}),?\\s*(\\d{1}\\.\\d{1,2})?\\)")
				.matcher(colorString);
		if (matcher.matches()) {
			int red = Integer.parseInt(matcher.group(1));
			int green = Integer.parseInt(matcher.group(2));
			int blue = Integer.parseInt(matcher.group(3));
			String group4 = matcher.group(4);
			int alpha = group4 != null
					? group4.contains(".") ? Math.round(Float.parseFloat(group4) * 255) : Integer.parseInt(group4)
					: 255;
			return Optional.of(new GColor(red, green, blue, alpha));
		}
		
		System.err.println("COLOR NOT PARSED : " + colorString);

		return Optional.empty();
	}

	/**
	 * @return CSS color code like #ffffff
	 */
	public String toCSSString() {
		int rgba = (r & 0xFF) << 24 | (g & 0xFF) << 16 | (b & 0xFF) << 8 | (a & 0xFF);
		return String.format("#%08x", rgba);
	}

	public String toAABBGGRR() {
		int rgba = (a & 0xFF) << 24 | (b & 0xFF) << 16 | (g & 0xFF) << 8 | (r & 0xFF);
		return String.format("%08x", rgba);
	}

	/**
	 * @deprecated use parseString()
	 */
	@Deprecated
	public static GColor fromString(String c) {
		GColor result = null;
		String[] split = c.split(",");
		if (split.length == 3) {
			int r = Integer.parseInt(split[0]);
			int g = Integer.parseInt(split[1]);
			int b = Integer.parseInt(split[2]);
			result = new GColor(r, g, b);
		}
		return result;
	}

	/**
	 * Parse CSS color code like #ffffff
	 */
	public static GColor fromCSSCode(String c) {
		int red = Integer.parseInt(c.substring(1, 3), 16);
		int green = Integer.parseInt(c.substring(3, 5), 16);
		int blue = Integer.parseInt(c.substring(5, 7), 16);
		return new GColor(red, green, blue, 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + a;
		result = prime * result + b;
		result = prime * result + g;
		result = prime * result + r;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GColor other = (GColor) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		if (g != other.g)
			return false;
		if (r != other.r)
			return false;
		return true;
	}

	// Typology util methods

	// typology deprecated colors (colors are now named with CSS tag)
	private static final Map<String, GColor> TYPOLOGY_COLORS = Map.ofEntries(entry("yellow", new GColor(255, 255, 0)),
			entry("red", new GColor(255, 0, 0)), entry("black", new GColor(0, 0, 0)),
			entry("brown", new GColor(165, 42, 42)), entry("teal", new GColor(0, 128, 128)),
			entry("purple", new GColor(128, 0, 128)), entry("blue", new GColor(0, 0, 255)),
			entry("gray", new GColor(128, 128, 128)), entry("white", new GColor(255, 255, 255)),
			entry("orange", new GColor(255, 165, 0)), entry("green", new GColor(0, 128, 0)),
			entry("pink", new GColor(255, 68, 136)));

	public static GColor fromTypologyColor(String typologyColor) {
		return typologyColor.startsWith("#") ? //
				new GColor(typologyColor) // color in CSS format (#fffffff...)
				: TYPOLOGY_COLORS.get(typologyColor.toLowerCase()); // old deprecated color name
	}

}
