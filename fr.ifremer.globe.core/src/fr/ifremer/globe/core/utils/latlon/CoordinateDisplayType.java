package fr.ifremer.globe.core.utils.latlon;

public enum CoordinateDisplayType {
	DECIMAL,
	DEG_MIN_SEC,
	DEG_MIN_DEC
}
