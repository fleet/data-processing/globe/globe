package fr.ifremer.globe.core.utils.latlon;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class DecimalFormatter implements ILatLongFormatter {
	private static DecimalFormatter instance = new DecimalFormatter();
	private static String DEGREE = "°";
	
	private DecimalFormatter() {}
	
	public static DecimalFormatter getInstance() {
		return instance;
	}

	@Override
	public String formatLatitude(double latitude) {
		StringBuilder builder = new StringBuilder();
		DecimalFormat format = new DecimalFormat();
		
		format.setRoundingMode(RoundingMode.HALF_DOWN);
		format.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
		format.applyPattern("#0.0000000");
		builder.append(format.format(latitude));
		
		builder.append(DEGREE);
		
		return builder.toString();
	}

	@Override
	public Double parseLatitude(String latitude) {
		Double res;
		
		try {
			res = Double.parseDouble(latitude.substring(0, latitude.length() - 1));
		} catch (NumberFormatException e) {
			return null;
		}
		
		return res;
	}

	@Override
	public String formatLongitude(double longitude) {
		StringBuilder builder = new StringBuilder();
		DecimalFormat format = new DecimalFormat();
		
		format.setRoundingMode(RoundingMode.HALF_DOWN);
		format.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
		format.applyPattern("#0.0000000");
		builder.append(format.format(longitude));
		
		builder.append(DEGREE);
		
		return builder.toString();
	}

	@Override
	public Double parseLongitude(String longitude) {
		Double res;
		
		try {
			res = Double.parseDouble(longitude.substring(0, longitude.length() - 1));
		} catch (NumberFormatException e) {
			return null;
		}
		
		return res;
	}

}
