package fr.ifremer.globe.core.utils.latlon;

public interface ILatLongFormatter {
	public String formatLatitude(double latitude);
	public Double parseLatitude(String latitude);
	
	public String formatLongitude(double longitude);
	public Double parseLongitude(String longitude);
}
