package fr.ifremer.globe.core.utils.latlon;

/**
 * 
 * Class use to format and display latitude longitude in Globe user interface
 * */
public class FormatLatitudeLongitude {
	private FormatLatitudeLongitude() {}
	
	public static String longitudeToString(double longitude) {
		ILatLongFormatter formatter = LatLongFormater.getFormatter();
		
		return formatter.formatLongitude(longitude);

	}
	public static String latitudeToString(double latitude) {
		ILatLongFormatter formatter = LatLongFormater.getFormatter();
		
		return formatter.formatLatitude(latitude);
	}
}
