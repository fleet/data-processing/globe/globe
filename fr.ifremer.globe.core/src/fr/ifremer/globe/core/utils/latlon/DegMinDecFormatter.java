package fr.ifremer.globe.core.utils.latlon;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class provides the format services.
 */
public final class DegMinDecFormatter implements ILatLongFormatter {
	private static final String MINUTE_DECIMAL_PATTERN = "00.000";
	private static final String LATITUDE_DEGREE_PATTERN = "00";
	private static final String LONGITUDE_DEGREE_PATTERN = "000";
	private static final Pattern LATITUDE_PATTERN = Pattern.compile("^(N|S) +(\\d{1,2}) +(\\d{1,2}\\.\\d{1,}) *$");
	private static final Pattern LONGITUDE_PATTERN = Pattern.compile("^(W|E) +(\\d{1,3}) +(\\d{1,2}\\.\\d{1,}) *$");
	
	private static DegMinDecFormatter instance = new DegMinDecFormatter();

	private DegMinDecFormatter() {}

	public static DegMinDecFormatter getInstance() {
		return instance;
	}
	
	/**
	 * Format a latitude (in degrees)
	 * 
	 * @param latitude
	 *            the latitude (in degrees)
	 * @return the formatted latitude
	 */
	public String formatLatitude(double latitude) {
		StringBuilder builder = new StringBuilder();
		
		char hemisphere = latitude >= 0 ? 'N' : 'S';
		builder.append(hemisphere);
		builder.append(" ");		

		double absLat = Math.abs(latitude);
		int degrees = (int) absLat;
		double minutes = (absLat - degrees) * 60;
		if (minutes > 59.9995) {
			degrees++;
			minutes = 0;
		}
		
		DecimalFormat format = new DecimalFormat();
		format.setRoundingMode(RoundingMode.HALF_DOWN);
		format.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
		format.applyPattern(LATITUDE_DEGREE_PATTERN);
		builder.append(format.format(degrees));
		builder.append(" "); //$NON-NLS-1$
		format.applyPattern(MINUTE_DECIMAL_PATTERN);
		builder.append(format.format(minutes));
		return builder.toString();
	}

	/**
	 * Parse a latitude text.
	 * 
	 * @param latString
	 *            the latitude text
	 * @return the latitude (in degrees), or <code>null</code> if the parsing failed.
	 */
	public Double parseLatitude(String latString) {
		Matcher m = LATITUDE_PATTERN.matcher(latString);
		
		if (m.find()) {
			double sign = m.group(1).equalsIgnoreCase("N") ? 1 : -1;
			int degree = Integer.parseInt(m.group(2));
			double minutes = Double.parseDouble(m.group(3));
			
			return sign * (degree + minutes / 60);
		}
		
		return null;
	}

	/**
	 * Format a longitude (in degrees)
	 * 
	 * @param longitude
	 *            the longitude (in degrees)
	 * @return the formatted longitude
	 */
	public String formatLongitude(double longitude) {
		StringBuilder builder = new StringBuilder();
		
		char hemisphere = longitude >= 0 ? 'E' : 'W';
		builder.append(hemisphere);
		builder.append(" ");

		double absLon = Math.abs(longitude);
		int degrees = (int) absLon;
		double minutes = (absLon - degrees) * 60;
		if (minutes > 59.9995) {
			degrees++;
			minutes = 0;
		}
		
		DecimalFormat format = new DecimalFormat();
		format.setRoundingMode(RoundingMode.HALF_DOWN);
		format.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
		format.applyPattern(LONGITUDE_DEGREE_PATTERN);
		builder.append(format.format(degrees));
		builder.append(" ");
		format.applyPattern(MINUTE_DECIMAL_PATTERN);
		builder.append(format.format(minutes));
		return builder.toString();
	}

	/**
	 * Parse a longitude text
	 * 
	 * @param lonString
	 *            the longitude text
	 * @return the longitude (in degrees), <code>null</code> if the parsing has
	 *         been failed.
	 */
	public Double parseLongitude(String lonString) {
		Matcher m = LONGITUDE_PATTERN.matcher(lonString);
		
		if (m.find()) {
			double sign = m.group(1).equalsIgnoreCase("E") ? 1 : -1;
			int degree = Integer.parseInt(m.group(2));
			double minutes = Double.parseDouble(m.group(3));
			
			return sign * (degree + minutes / 60);
		}
		
		return null;
	}
}
