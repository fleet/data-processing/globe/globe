package fr.ifremer.globe.core.utils.latlon;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DegMinSecFormatter implements ILatLongFormatter {

	private static final Pattern LATITUDE_PATTERN = Pattern.compile("^(N|S) +(\\d{1,2})° *(\\d{1,2})' *(\\d{1,2})''$");
	private static final Pattern LONGITUDE_PATTERN = Pattern.compile("^(W|E) +(\\d{1,3})° *(\\d{1,2})' *(\\d{1,2})''$");

	private static final String DEGREE = "°";
	private static final String MINUTE = "'";
	private static final String SECOND = "''";

	private static DegMinSecFormatter instance = new DegMinSecFormatter();

	private DegMinSecFormatter() {
	}

	public static DegMinSecFormatter getInstance() {
		return instance;
	}

	/**
	 * Format a latitude (in degrees)
	 *
	 * @param latitude the latitude (in degrees)
	 * @return the formatted latitude
	 */
	@Override
	public String formatLatitude(double latitude) {
		return doubleToDegMinSec(latitude, true);
	}

	/**
	 * Parse a latitude text.
	 *
	 * @param latString the latitude text
	 * @return the latitude (in degrees), or <code>null</code> if the parsing failed.
	 */
	@Override
	public Double parseLatitude(String latString) {
		Matcher m = LATITUDE_PATTERN.matcher(latString);

		if (m.find()) {
			double sign = m.group(1).equalsIgnoreCase("N") ? 1 : -1;
			int degree = Integer.parseInt(m.group(2));
			double minutes = Double.parseDouble(m.group(3));
			double seconds = Double.parseDouble(m.group(4));

			return sign * (degree + minutes / 60 + seconds / 3600);
		}

		return null;
	}

	/**
	 * Format a longitude (in degrees)
	 *
	 * @param longitude the longitude (in degrees)
	 * @return the formatted longitude
	 */
	@Override
	public String formatLongitude(double longitude) {
		return doubleToDegMinSec(longitude, false);
	}

	/**
	 * Parse a longitude text
	 *
	 * @param lonString the longitude text
	 * @return the longitude (in degrees), <code>null</code> if the parsing has been failed.
	 */
	@Override
	public Double parseLongitude(String lonString) {
		Matcher m = LONGITUDE_PATTERN.matcher(lonString);

		if (m.find()) {
			double sign = m.group(1).equalsIgnoreCase("E") ? 1 : -1;
			int degree = Integer.parseInt(m.group(2));
			double minutes = Double.parseDouble(m.group(3));
			double seconds = Double.parseDouble(m.group(4));

			return sign * (degree + minutes / 60 + seconds / 3600);
		}

		return null;
	}

	/**
	 * Parse a double value to degree-minute-second format.
	 */
	private String doubleToDegMinSec(double doubleValue, boolean isLatitude) {
		StringBuilder builder = new StringBuilder();

		if (isLatitude) {
			builder.append(doubleValue >= 0 ? "N" : "S");
		} else {
			builder.append(doubleValue >= 0 ? "E" : "W");
		}
		builder.append(" ");
		doubleValue = Math.abs(doubleValue);

		int degrees = (int) doubleValue;
		double minSec = (doubleValue - degrees) * 60;
		int minutes = (int) minSec;

		int seconds;
		double delta = minSec - minutes;

		if (Math.abs(Math.round(delta) - delta) < 0.000001) {
			minutes = (int) Math.round(minSec);
			seconds = 0;
		} else {
			seconds = (int) Math.round((minSec - minutes) * 60);
		}

		builder.append(String.format(isLatitude ? "%02d" : "%03d", degrees));
		builder.append(DEGREE);
		builder.append(String.format("%02d", minutes));
		builder.append(MINUTE);
		builder.append(String.format("%02d", seconds));
		builder.append(SECOND);

		return builder.toString();
	}
}
