package fr.ifremer.globe.core.utils.latlon;

import fr.ifremer.globe.core.model.ModelPreferences;

/**
 * 
 * Class use to format and display latitude longitude in Globe user interface
 */
public class LatLongFormater {

	private LatLongFormater() {
	}

	public static ILatLongFormatter getFormatter(CoordinateDisplayType display) {
		switch (display) {
		case DEG_MIN_DEC:
			return DegMinDecFormatter.getInstance();
		default:
		case DECIMAL:
			return DecimalFormatter.getInstance();
		case DEG_MIN_SEC:
			return DegMinSecFormatter.getInstance();
		}
	}

	public static String latitudeToString(double latitude) {
		return getFormatter().formatLatitude(latitude);
	}

	public static String latitudeToString(double latitude, CoordinateDisplayType display) {
		ILatLongFormatter formatter = getFormatter(display);
		return formatter.formatLatitude(latitude);
	}

	public static String longitudeToString(double longitude) {
		return getFormatter().formatLongitude(longitude);
	}

	public static String longitudeToString(double longitude, CoordinateDisplayType display) {
		ILatLongFormatter formatter = getFormatter(display);
		return formatter.formatLongitude(longitude);
	}

	public static ILatLongFormatter getFormatter() {
		CoordinateDisplayType display = (CoordinateDisplayType) ModelPreferences.getInstance().getGridCoordinate()
				.getValue();
		return getFormatter(display);
	}

}
