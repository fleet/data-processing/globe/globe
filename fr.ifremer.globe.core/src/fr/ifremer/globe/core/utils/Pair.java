
package fr.ifremer.globe.core.utils;

/**
 * Class for objects pair.
 *
 */
public class Pair<U, V> {
	private final U first;
	private final V second;

	public Pair(U o1, V o2) {
		this.first = o1;
		this.second = o2;
	}

	public U getFirst() {
		return first;
	}

	public V getSecond() {
		return second;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pair<?, ?>)) {
			return false;
		}
		Pair<?, ?> p = (Pair<?, ?>) obj;
		return same(p.first, this.first) && same(p.second, this.second);
	}

	@Override
	public int hashCode() {
		return (first == null ? 0 : first.hashCode()) + (second == null ? 0 : second.hashCode());
	}

	public static boolean same(Object o1, Object o2) {
		return o1 == null ? o2 == null : o1.equals(o2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{");
		sb.append(first);
		sb.append(", ");
		sb.append(second);
		sb.append("}");
		return sb.toString();
	}

	/** Return an instance of Pair */
	public static <U, V> Pair<U, V> of(U o1, V o2) {
		return new Pair<>(o1, o2);
	}

}
