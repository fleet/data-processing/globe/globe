package fr.ifremer.globe.core.runtime.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Utility Globe class to run external processes.
 */
public class GSystem {

	/** PyAT log tags **/
	private static final String PYAT_INFO_TAG = "[ INFO ]  ";
	private static final String PYAT_DEBUG_TAG = "[ DEBUG ]  ";
	private static final String PYAT_WARNING_TAG = "[ WARNING ]  ";
	private static final String PYAT_ERROR_TAG = "[ ERROR ]  ";

	/**
	 * Runs a simple command.
	 */
	public static List<String> run(String command) throws IOException {
		List<String> outputs = new LinkedList<>();
		final Process process = Runtime.getRuntime().exec(command);
		try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
			String read;
			while ((read = br.readLine()) != null) {
				outputs.add(read);
			}
		}
		return outputs;
	}

	/**
	 * Runs command with environment variables, logger and monitor.
	 */
	public static IStatus run(List<String> programAndArgs, Map<String, String> envs, IProgressMonitor monitor,
			Logger logger) throws InterruptedException, IOException {
		IStatus result = Status.OK_STATUS;
		// Create process
		ProcessBuilder builder = new ProcessBuilder(programAndArgs);
		builder.environment().putAll(envs);
		builder.redirectErrorStream(true);

		// Launch process
		final Process process = builder.start();

		// Logs
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
			String line;
			while ((line = reader.readLine()) != null && !monitor.isCanceled()) {
				if (line.contains("Warning 1: Recode from UTF-8 to CP_ACP failed with the error"))
					continue;// remove Gdal warning for recode

				// remove date for logs (there is already timestamp with Logger)
				line = line.replaceAll("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2},\\d{3}:", "");

				// remove PyAT tags, and use the suitable logger function
				if (line.contains(PYAT_DEBUG_TAG)) {
					// debug
					String log = line.replace(PYAT_DEBUG_TAG, "");
					logger.debug(log);
				} else if (line.contains(PYAT_WARNING_TAG)) {
					// warning
					String log = line.replace(PYAT_WARNING_TAG, "");
					logger.warn(log);
				} else if (line.contains(PYAT_ERROR_TAG)) {
					// error
					String log = line.replace(PYAT_ERROR_TAG, "");
					logger.error(log);
					result = new Status(IStatus.ERROR, OsgiUtils.getBundleName(GSystem.class), log);
				} else {
					// info
					String log = line.replace(PYAT_INFO_TAG, "");
					logger.info(log);
					
					//TODO (to get a feedback when running in foreground)
					monitor.setTaskName(log);
				}
			}
		} finally {
			if (monitor.isCanceled())
				process.destroyForcibly();
		}

		if (monitor.isCanceled())
			return Status.CANCEL_STATUS;

		int exitCode = process.waitFor();
		if (exitCode != 0) {
			String errMsg = "Process failed ? Exit not equals 0, exit code : " + exitCode;
			result = new Status(IStatus.ERROR, OsgiUtils.getBundleName(GSystem.class), "exit code : " + exitCode);
			logger.error(errMsg);
		}

		return result;
	}

}
