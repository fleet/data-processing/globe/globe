package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;

/** Params for smoother service */
public record SmootherParams(//
		File inputFile, //
		File outputFile, //
		String type, //
		String algorithm, //
		int windowSize, //
		int order, //
		double criticalFreq, //
		double samplingFreq//
) {

}
