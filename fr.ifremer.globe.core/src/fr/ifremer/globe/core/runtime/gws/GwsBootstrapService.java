package fr.ifremer.globe.core.runtime.gws;

import fr.ifremer.globe.core.runtime.gws.event.GwsState;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/** GWS server bootstrap services */
public interface GwsBootstrapService {
	/**
	 * Returns the service implementation.
	 */
	static GwsBootstrapService grab() {
		return OsgiUtils.getService(GwsBootstrapService.class);
	}
	
	/** Start the server GWS. */
	void startServing();
	
	/** Loads driver from GWS. **/
	void startDriverServices();

	/** Stop the server GWS. */
	void stopServing();

	/** Return the state of the server */
	GwsState getServerState();
}
