package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;
import java.util.List;

public record MergeXsfParams(List<File> inXsfFiles, List<File> outCutXsfFiles, File cutFile, boolean overwrite) {
}
