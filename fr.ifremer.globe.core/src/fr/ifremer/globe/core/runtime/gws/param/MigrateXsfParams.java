package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;
import java.util.List;

public record MigrateXsfParams(//
		List<File> inXsfFiles, //
		List<File> outXsfFiles, //
		List<File> refXsfFiles, // Empty list if referenceFolder is provided
		File referenceFolder, // May be null if refXsfFiles is provided
		boolean overwrite) {
}
