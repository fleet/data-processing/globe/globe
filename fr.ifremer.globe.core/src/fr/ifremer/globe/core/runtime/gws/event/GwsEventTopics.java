/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.gws.event;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * All topics managed in the GWS plugin
 */
public class GwsEventTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/globe/gws";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to inform that the state of the server has changed<br>
	 * Event poster place on the event an instance of GwsState
	 */
	public static final String TOPIC_SERVER_STATE = TOPIC_BASE + UIEvents.TOPIC_SEP + "SERVER_STATE";

	/**
	 * Constructor
	 */
	private GwsEventTopics() {
		// private constructor to hide the implicit public one.
	}
}
