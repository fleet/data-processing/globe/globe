package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;
import java.util.List;

/** Params for tide estimation service */
public record EstimateTideParams(//
		List<File> inputFiles, //
		File outputFile, //
		File modelDir, //
		boolean enableFiltering, //
		boolean displayMatplot, //
		boolean predictiveMode, //
		String referenceSurface, //
		double referenceAltitude//
) {

}
