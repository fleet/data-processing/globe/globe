package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;

public record ExportToXsfParams(//
		File in, // Raw file to convert or folder of raw files
		File out, // Destination folder for converted files
		boolean overwrite,
		// Metadata
		String xsfKeywords, //
		String xsfLicense, //
		String xsfRights, //
		String xsfSummary, //
		String xsfTitle, //
		boolean ignoreWC) {
}
