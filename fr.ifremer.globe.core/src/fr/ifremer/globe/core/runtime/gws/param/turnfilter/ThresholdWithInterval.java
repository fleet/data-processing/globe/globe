package fr.ifremer.globe.core.runtime.gws.param.turnfilter;

import java.time.Duration;

public record ThresholdWithInterval(Duration interval, double threshold) {
}
