package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.navigation.INavigationData;

public record ExportToNviParams(
		/** List of String (file path). Used to set up the source filenames of the NVI files */
		List<File> inputFiles,
		/** List of INavigationData to write in output files. If absent, open the input files */
		Optional<List<INavigationData>> navigationData,
		/**
		 * if present, indicates how to distribute all merged navigation points in nvi.nc files. Useful when a single
		 * INavigationData needs to be split into several nvi.nc files
		 */
		Optional<int[]> navPointsInFiles,
		/** List of NVI files to generate. If empty, this list is set up by Wizard */
		Optional<List<File>> ouputFiles,
		/** true to overwrite existing output files */
		Optional<Boolean> overwrite,
		/** If present, indicates where in the project explorer to load output file at the end of the export */
		Optional<String> whereToLoadOutputFiles) {
}
