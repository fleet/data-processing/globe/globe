package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;

/** Params for Shom harmonic tide prediction service */
public record PredictShomHarmonicTideParams(//
		File dataFile, // Input data file file (.csv)
		File outputFile, // Output file (.csv)
		File harmonicFile // Harmonic constant file (.har)
) {

}
