package fr.ifremer.globe.core.runtime.gws.param.turnfilter;

/**
 * Filter methods
 */
public enum TurnFilterMethods {
	STD("Standard deviation", "std"),
	WEIGHTED_STD("Weighted standard deviation", "weighted_std"),
	GAP("Diff between furthest values", "gap"),
	MEAN("Mean before vs after", "mean");

	public final String label;
	public final String value;

	TurnFilterMethods(String label, String value) {
		this.label = label;
		this.value = value;
	}
}
