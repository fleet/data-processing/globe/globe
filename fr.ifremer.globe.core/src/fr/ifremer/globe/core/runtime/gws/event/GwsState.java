package fr.ifremer.globe.core.runtime.gws.event;

/**
 * State of GWS server
 */
public enum GwsState {
	NOT_AVAILABLE,
	STARTING,
	RUNNING
}
