package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;

public record ExportToMbgParams(//
		File in, // Raw file to convert or folder of raw files
		File out, // Destination file or folder for converted files
		boolean overwrite,
		// Metadata
		String mbgCdi, //
		String mbgReference, //
		String mbgShipName, //
		String mbgSurveyName) {
}
