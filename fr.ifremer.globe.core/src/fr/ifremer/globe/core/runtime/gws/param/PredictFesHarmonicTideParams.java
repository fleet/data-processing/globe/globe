package fr.ifremer.globe.core.runtime.gws.param;

import java.io.File;

/** Params for FES2014 harmonic tide prediction service */
public record PredictFesHarmonicTideParams(//
		File dataFile, // Input data file file (.csv)
		File outputFile, // Output file (.csv)
		File modelDir // FES model input directory file
) {

}
