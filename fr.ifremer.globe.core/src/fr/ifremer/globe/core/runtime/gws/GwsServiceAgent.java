package fr.ifremer.globe.core.runtime.gws;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.slf4j.Logger;

import fr.ifremer.globe.core.runtime.gws.param.EstimateTideParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToMbgParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToNviParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.MergeXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.MigrateXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.PredictFesHarmonicTideParams;
import fr.ifremer.globe.core.runtime.gws.param.PredictShomHarmonicTideParams;
import fr.ifremer.globe.core.runtime.gws.param.SmootherParams;
import fr.ifremer.globe.core.runtime.gws.param.turnfilter.TurnFilterParams;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Allow access to services defined in GWS
 */
public interface GwsServiceAgent {
	/**
	 * Opens help page of the service.
	 */
	void openHelp(String serviceName);

	/**
	 * Apply a mask to a MBG file and produce the cut lines using a Shapefile (edited in the wizard)
	 * 
	 * @return the cut lines (.cut file format)
	 */
	List<String> computeCutFileFromMbgMask(List<File> mbgFiles) throws GIOException;

	/**
	 * Converts files in NVI format
	 */
	IStatus exportToNvi(ExportToNviParams params, IProgressMonitor monitor, Logger logger) throws GException;

	/** Convert S7K/ALL files to XSF */
	IStatus exportToXsf(ExportToXsfParams params, IProgressMonitor monitor, Logger logger) throws GException;

	/** Convert S7K/ALL files to MBG */
	IStatus exportToMbg(ExportToMbgParams params, IProgressMonitor monitor, Logger logger) throws GException;

	/** Cut/Merge XSF files */
	IStatus mergeXsf(MergeXsfParams params, IProgressMonitor monitor, Logger logger) throws GException;

	/** Migrate XSF files */
	IStatus migrateXsf(MigrateXsfParams params, IProgressMonitor monitor, Logger logger) throws GException;

	/** Navigation tide estimation */
	IStatus estimateNavigationTide(EstimateTideParams params, IProgressMonitor monitor, Logger logger)
			throws GException;

	/** XSF tide estimation */
	IStatus estimateXsfNmeaTide(EstimateTideParams params, IProgressMonitor monitor, Logger logger) throws GException;

	/** Shom harmonic tide prediction service */
	IStatus predictShomHarmonicTide(PredictShomHarmonicTideParams params, IProgressMonitor monitor, Logger logger)
			throws GException;

	/** FES2014 harmonic tide prediction service */
	IStatus predictFesHarmonicTide(PredictFesHarmonicTideParams params, IProgressMonitor monitor, Logger logger)
			throws GException;

	/** Smoother service */
	IStatus smoother(SmootherParams params, IProgressMonitor monitor, Logger logger) throws GException;

	/** Turn filter service */
	IStatus filterTurn(TurnFilterParams params, IProgressMonitor monitor, Logger logger) throws GException;
}
