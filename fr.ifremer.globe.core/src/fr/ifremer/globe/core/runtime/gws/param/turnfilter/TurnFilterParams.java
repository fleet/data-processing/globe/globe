package fr.ifremer.globe.core.runtime.gws.param.turnfilter;

import java.io.File;
import java.time.Duration;
import java.util.Optional;

/**
 * Parameters record
 */
public record TurnFilterParams(//
		File inputFile, //
		File outputFile, //
		TurnFilterMethods method, //
		Optional<ThresholdWithInterval> headingParameters, //
		Optional<ThresholdWithInterval> speedParameters, //
		Optional<Duration> minimumDuration//
) {

}
