package fr.ifremer.globe.core.runtime.job;

import fr.ifremer.globe.utils.exception.GException;

/**
 * {@link GException} during {@link GProcess}.
 */
public class GProcessException extends GException {

	private static final long serialVersionUID = -665340254046131352L;

	GProcessException(String message) {
		super(message);
	}

}
