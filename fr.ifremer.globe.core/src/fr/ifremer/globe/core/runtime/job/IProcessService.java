package fr.ifremer.globe.core.runtime.job;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

public interface IProcessService {

	/**
	 * Returns the service implementation.
	 */
	static IProcessService grab() {
		return OsgiUtils.getService(IProcessService.class);
	}

	// METHODS TO IMPLEMENT

	/**
	 * @return new {@link GProcess}.
	 */
	GProcess createProcess(String name, IProcessFunction function);

	/**
	 * Runs a {@link GProcess} in foreground.
	 * 
	 * @param process : {@link GProcess} to run.
	 */
	void runInForeground(GProcess process);

	/**
	 * Runs a {@link GProcess} in background.
	 * 
	 * @param process : {@link GProcess} to run.
	 * @param openConsoleView : if true, a console view is opened to display logs.
	 */
	void runInBackground(GProcess process, boolean openConsoleView);

	// OTHER DEFAULT METHODS TO EXTEND THE PUBLIC API

	/**
	 * Runs a {@link IProcessFunction} in foreground (with dialog).
	 */
	default void runInForeground(String name, Runnable runnable) {
		runInForeground(createProcess(name, IProcessFunction.with(runnable)));
	}

	/**
	 * Runs a {@link IProcessFunction} in foreground (with dialog).
	 */
	default void runInForeground(String name, IProcessFunction function) {
		runInForeground(createProcess(name, function));
	}

	/**
	 * Runs a {@link IProcessFunction} in background.
	 */
	default void runInBackground(String name, Runnable runnable) {
		runInBackground(createProcess(name, IProcessFunction.with(runnable)), false);
	}

	/**
	 * Runs a {@link IProcessFunction} in background.
	 */
	default void runInBackground(String name, IProcessFunction function) {
		runInBackground(createProcess(name, function), false);
	}

	/**
	 * Runs a {@link IProcessFunction} in background, and displays logs in {@link IProcessConsole}.
	 */
	default void runInBackground(String name, IProcessFunction function, boolean openConsoleView) {
		runInBackground(createProcess(name, function), openConsoleView);
	}

}
