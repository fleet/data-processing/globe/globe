package fr.ifremer.globe.core.runtime.job;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Process class which extends the {@link Job} class. It allows to run a process which can access to a logger and a
 * monitor, and throw {@link GException}.
 */
public abstract class GProcess extends Job {

	/** Atomic counter used to get a unique ID per process */
	private static final AtomicInteger ID_COUNTER = new AtomicInteger(0);

	/** Properties **/
	protected Logger logger = getLogger();

	/** {@link Consumer} called when the process throw an exception. */
	private Optional<Consumer<Throwable>> exceptionReactor = Optional.empty();

	/**
	 * A specific consumer is used to call listener for the end of running method. !! The standard {@link Job}
	 * addJobListener method can't be used because the {@link IProcessService} implementation call directly 'run()'
	 * method and not 'schedule()'...
	 */
	private Consumer<IJobChangeEvent> doneReactor;

	/**
	 * Constructor
	 */
	protected GProcess(String name) {
		super(String.format("%s #%d", name, ID_COUNTER.incrementAndGet()));
	}

	/**
	 * Method called by {@link GProcess}.
	 * 
	 * @param monitor : {@link IProgressMonitor} used to display the progress of the activity.
	 * @param logger : {@link Logger} of the {@link GProcess} (allows to display log in console view...).
	 * 
	 * @return the result {@link IStatus} of the process.
	 * @throws GException
	 */
	protected abstract IStatus apply(IProgressMonitor monitor, Logger logger) throws GException;

	/**
	 * Runs this process in foreground with the {@link IProcessService}.
	 */
	public void runInForeground() {
		IProcessService.grab().runInForeground(this);
	}

	/**
	 * Runs this process in background with the {@link IProcessService}.
	 */
	public void runInBackground(boolean openConsoleView) {
		IProcessService.grab().runInBackground(this, openConsoleView);
	}

	/**
	 * Runs the process.
	 */
	@Override
	public IStatus run(IProgressMonitor monitor) {
		Instant start = Instant.now();
		getLogger().info("{} start...", getName());

		IStatus result = Status.CANCEL_STATUS;
		try {
			// run the process
			result = apply(monitor, getLogger());

			// log result
			Duration duration = Duration.between(start, Instant.now());
			switch (result.getSeverity()) {
			case IStatus.OK:
				getLogger().info("{} done in {}.", getName(), duration);
				break;
			case IStatus.CANCEL:
				getLogger().info("{} cancelled (duration : {}).", getName(), duration);
				break;
			case IStatus.ERROR:
				var exception = Optional.ofNullable(result.getException())
						.orElse(new GProcessException(result.getMessage()));
				exceptionReactor.ifPresent(c -> c.accept(exception));
				getLogger().error("{} error occurred :{} (duration : {}).", getName(), result.getMessage(), duration);
				break;
			default:
				getLogger().info("{} ended in state : {} (duration : {}).", getName(), result.getMessage(), duration);
			}
		} catch (OperationCanceledException e) {
			getLogger().info("{} cancelled (duration : {}).", getName(), Duration.between(start, Instant.now()));
			return Status.CANCEL_STATUS;
		} catch (GException e1) {
			getLogger().error("{} error occurred : {}.", getName(), e1.getMessage(), e1);
			exceptionReactor.ifPresent(c -> c.accept(e1));
			return new Status(IStatus.ERROR, OsgiUtils.getBundleName(GProcess.class), e1.getMessage());
		} catch (Exception e2) {
			getLogger().error("{} unexpected error occurred : {}.", getName(), e2.getMessage(), e2);
			exceptionReactor.ifPresent(c -> c.accept(e2));
			return new Status(IStatus.ERROR, OsgiUtils.getBundleName(GProcess.class), e2.getMessage());
		}

		if (doneReactor != null) {
			final IStatus resultf = result;
			doneReactor.accept(new IJobChangeEvent() {
				@Override
				public IStatus getResult() {
					return resultf;
				}

				@Override
				public IStatus getJobGroupResult() {
					return null;
				}

				@Override
				public Job getJob() {
					return GProcess.this;
				}

				@Override
				public long getDelay() {
					return -1;
				}
			});
		}

		return result;
	}

	/**
	 * Sets a method to run when the process is done.
	 */
	public void whenIsDone(Consumer<IJobChangeEvent> doneReactor) {
		this.doneReactor = doneReactor;

		// addJobChangeListener can't be used because the {@link IProcessService} implementation call directly 'run()'
		// method and not 'schedule()'...
		// addJobChangeListener(new JobChangeAdapter() {
		// @Override public void done(IJobChangeEvent event) {
		// doneReactor.accept(event); }
		// });
	}

	/**
	 * Sets a method to run when an exepction occurs.
	 */
	public void whenException(Consumer<Throwable> exceptionConsumer) {
		this.exceptionReactor = Optional.of(exceptionConsumer);
	}

	/**
	 * @return process logger.
	 */
	public Logger getLogger() {
		if (logger == null) // logger can be null if GProcess has been created by deserialization
			logger = LoggerFactory.getLogger(GProcess.class.getName() + "_" + getName());
		return logger;
	}

}
