package fr.ifremer.globe.core.runtime.job;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;

import fr.ifremer.globe.utils.exception.GException;

/**
 * {@link FunctionalInterface} run by {@link GProcess}.
 */
@FunctionalInterface
public interface IProcessFunction {

	/**
	 * Method called by {@link GProcess}.
	 *
	 * @param monitor : {@link IProgressMonitor} used to display the progress of the activity.
	 * @param logger : {@link Logger} of the {@link GProcess} (allows to display log in console view...).
	 *
	 * @return the result {@link IStatus} of the process.
	 * @throws GException
	 */
	IStatus apply(IProgressMonitor monitor, Logger logger) throws GException;

	/** Creates a simple IProcessFunction to execute a Runnable in a process */
	static IProcessFunction with(Runnable runnable) {
		return (IProgressMonitor monitor, Logger logger) -> {
			runnable.run();
			return Status.OK_STATUS;
		};
	}

	/** Joins several {@link IProcessFunction} in one **/
	static IProcessFunction join(List<IProcessFunction> functions) {
		return (IProgressMonitor monitor, Logger logger) -> {
			var result = Status.OK_STATUS;
			for (var function : functions) {
				logger.info(" "); // separate processes
				var functionResult = function.apply(monitor, logger);
				result = functionResult != Status.OK_STATUS ? functionResult : result;
			}
			logger.info(" ");
			return result;
		};
	}
}
