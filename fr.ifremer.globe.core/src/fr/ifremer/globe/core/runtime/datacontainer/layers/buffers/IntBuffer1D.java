package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class IntBuffer1D extends AbstractBuffer {

	public IntBuffer1D(int length, String name) throws GIOException {
		super(int.class, Integer.BYTES, length, name);
	}

	/** Constructor used for copy **/
	private IntBuffer1D(IntBuffer1D source) throws IOException {
		super(source);
	}

	@Override
	public IntBuffer1D copy() throws IOException {
		return new IntBuffer1D(this);
	}

	public int get(int index) {
		return backedBuffer.getInt(index);
	}

	public void set(int index, int value) {
		setDirty(true);
		backedBuffer.putInt(index, value);
	}

}
