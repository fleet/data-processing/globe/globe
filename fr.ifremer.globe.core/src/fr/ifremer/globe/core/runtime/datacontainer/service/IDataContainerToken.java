package fr.ifremer.globe.core.runtime.datacontainer.service;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;

/**
 * Token : unmutable object returned after a booking. Allows to access to the {@link DataContainer}.
 */
public interface IDataContainerToken<U extends IDataContainerInfo> extends AutoCloseable {

	/** Close without exception **/
	@Override
	void close();

	DataContainer<U> getDataContainer();

	IDataContainerOwner getOwner();

}
