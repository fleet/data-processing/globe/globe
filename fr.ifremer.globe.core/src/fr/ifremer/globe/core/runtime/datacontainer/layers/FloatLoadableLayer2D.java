package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class FloatLoadableLayer2D extends FloatLayer2D implements ILoadableLayer<FloatBuffer2D> {

	private boolean isLoaded;

	public FloatLoadableLayer2D(String name, String longName, String unit, ILayerLoader<FloatBuffer2D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<FloatBuffer2D> loader;

	@Override
	public ILayerLoader<FloatBuffer2D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
