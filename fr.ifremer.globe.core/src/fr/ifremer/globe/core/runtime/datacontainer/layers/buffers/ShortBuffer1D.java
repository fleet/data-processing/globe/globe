package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class ShortBuffer1D extends AbstractBuffer {

	public ShortBuffer1D(int length, String name) throws GIOException {
		super(short.class, Short.BYTES, length, name);
	}

	/** Constructor used for copy **/
	private ShortBuffer1D(ShortBuffer1D source) throws IOException {
		super(source);
	}

	@Override
	public ShortBuffer1D copy() throws IOException {
		return new ShortBuffer1D(this);
	}

	public short get(int index) {
		return backedBuffer.getShort(index);
	}

	public void set(int index, short value) {
		setDirty(true);
		backedBuffer.putShort(index, value);
	}

}
