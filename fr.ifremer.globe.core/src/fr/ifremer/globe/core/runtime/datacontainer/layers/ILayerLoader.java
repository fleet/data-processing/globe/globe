package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Interface handling loading/flushing operation for a {@link ILoadableLayer}
 */
public interface ILayerLoader<T extends AbstractBuffer> {

	/**
	 * Loads data in the buffer. Implementation is delegate to the {@link ILayerManager}
	 * 
	 * @throws GIOException
	 */
	@SuppressWarnings("unchecked")
	default void load(ILoadableLayer<T> layer) throws GIOException {
		// Allocate buffer if it's not already done
		if (layer.getData() == null)
			LayerUtility.allocate((AbstractBaseLayer<? extends AbstractBuffer>) layer, getDimensions());

		// Call load method
		if (!layer.isLoaded())
			layer.setLoaded(load(layer.getData()));
	}

	/**
	 * Flushes data from the buffer to the file. Implementation is delegate to the {@link ILayerManager}
	 * 
	 * @throws GIOException
	 */
	default void flush(ILoadableLayer<T> layer) throws GIOException {
		if (layer.isLoaded() && layer.getData().isDirty()) {
			boolean flushResult = flush(layer.getData());
			layer.getData().setDirty(!flushResult);
		}
	}

	/**
	 * @return layer dimensions
	 * @throws GIOException
	 */
	long[] getDimensions() throws GIOException;

	/**
	 * @return layer fill value as float
	 */
	default float getFloatFillValue() {
		return Float.NaN;
	}
	

	/**
	 * Fill the buffer with data.
	 * 
	 * @param buffer to fill
	 * @return true if successful
	 * @throws GIOException
	 */
	boolean load(T buffer) throws GIOException;

	/**
	 * Flush the specified buffer.
	 * 
	 * @param buffer
	 * @return true if successful
	 * @throws GIOException
	 */
	boolean flush(T buffer) throws GIOException;
}
