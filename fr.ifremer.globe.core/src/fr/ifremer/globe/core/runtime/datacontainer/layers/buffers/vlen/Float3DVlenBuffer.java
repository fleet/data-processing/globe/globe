package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.vlen;

import java.nio.ByteBuffer;
import java.util.function.DoubleUnaryOperator;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IBaseBuffer;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.utils.function.FloatIndexedConsumer;
import fr.ifremer.globe.utils.function.FloatIndexedSupplier;

/**
 * class storing data for a vlen buffer<br>
 * data is stored in a 1D buffer, it is accessible as a 2D array
 */
public class Float3DVlenBuffer implements IBaseBuffer {

	private int rowCount;
	private int colCount;
	private int zCount;

	private ByteBuffer values;
	/** Type of data in ByteBuffer */
	private DataType dataType;
	/** Way to access to a float in the values ByteBuffer */
	private FloatIndexedSupplier valueGetter;
	/** Way to put to a float in the values ByteBuffer */
	private FloatIndexedConsumer valueSetter;

	public Float3DVlenBuffer(DataType dataType) {
		this.dataType = dataType;

		colCount = 0;
		rowCount = 0;
		zCount = 0;
	}

	/**
	 * Apply a transformation to all data in the buffer
	 */
	public void applyTransform(DoubleUnaryOperator f) {
		int len = values.capacity() / dataType.getSize();
		values.position(0);
		for (int i = 0; i < len; i++) {
			double v = getFloat(i);
			v = f.applyAsDouble(v);
			putFloat(i, (float) v);
		}
	}

	public void setBuffer(ByteBuffer values, DataType dataType, int rowCount, int colCount, int zCount) {
		this.rowCount = rowCount;
		this.colCount = colCount;
		this.zCount = zCount;
		this.values = values;
		this.dataType = dataType;

		switch (dataType) {
		case BYTE:
		case UBYTE:
			valueGetter = this.values::get;
			valueSetter = (i, v) -> this.values.put(i, (byte) v);
			break;
		case CHAR:
			valueGetter = this.values::getChar;
			valueSetter = (i, v) -> this.values.putChar(i, (char) v);
			break;
		case SHORT:
		case USHORT:
			valueGetter = this.values::getShort;
			valueSetter = (i, v) -> this.values.putShort(i, (short) v);
			break;
		case INT:
		case UINT:
			valueGetter = this.values::getInt;
			valueSetter = (i, v) -> this.values.putInt(i, (int) v);
			break;
		case LONG:
		case ULONG:
			valueGetter = this.values::getLong;
			valueSetter = (i, v) -> this.values.putLong(i, (long) v);
			break;
		case FLOAT:
			valueGetter = this.values::getFloat;
			valueSetter = this.values::putFloat;
			break;
		case DOUBLE:
			valueGetter = i -> (float) this.values.getDouble(i);
			valueSetter = this.values::putDouble;
			break;

		default:
			break;
		}
	}

	public float get(int row, int col, int z) {
		return getFloat(z * rowCount * colCount + col * rowCount + row);
	}

	/** @return the value as float of one element of the buffer */
	protected float getFloat(int elementIndex) {
		if (values == null || elementIndex > values.capacity() / dataType.getSize()) {
			return Float.NaN;
		}
		return valueGetter.getAsFloat(elementIndex * dataType.getSize());
	}

	/** Set the value of one element of the buffer */
	protected void putFloat(int elementIndex, float value) {
		valueSetter.accept(elementIndex * dataType.getSize(), value);
	}

	@Override
	public String toString() {
		int maxRow = Math.min(rowCount, 3);
		int maxLen = Math.min(colCount, 2);
		var preview = new StringBuilder(System.lineSeparator());
		for (int row = 0; row < maxRow; row++) {
			for (int col = 0; col < maxLen; col++) {
				preview.append(get(row, col, 0)).append(" ");
			}
			preview.append("...").append(System.lineSeparator());
		}
		preview.append("...").append(System.lineSeparator());
		return String.format("Matrix [%d,%d,%d] (%s)", rowCount, colCount, zCount, preview.toString());
	}

	public int getRowCount() {
		return rowCount;
	}

	public int getColCount() {
		return colCount;
	}

}
