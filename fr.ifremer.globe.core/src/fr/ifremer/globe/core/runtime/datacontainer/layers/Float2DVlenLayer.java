package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.NetcdfVlenLayerManager;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.vlen.Float3DVlenBuffer;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * A layer of float variable length values
 *
 */
public class Float2DVlenLayer implements IVlenLoadableLayer {

	private final NetcdfVlenLayerManager m;
	private final String name;
	private final String longName;
	private final DataType dataType;
	private final String unit;
	private final long[] dimensions;

	private Float3DVlenBuffer buffer;
	private boolean isLoaded;

	/** Constructor **/
	public Float2DVlenLayer(String name, String longName, String unit, DataType dataType, NetcdfVlenLayerManager m,
			long[] dimensions) {
		this.name = name;
		this.longName = longName;
		this.unit = unit;
		this.dataType = dataType;
		this.m = m;
		isLoaded = false;
		this.dimensions = dimensions;
	}

	@Override
	public void load(int swathIdx) throws GIOException {
		try {
			m.load(swathIdx, buffer);
		} catch (NCException e) {
			throw new GIOException(String.format("Unable to load swath (%d) cause : %s", swathIdx, e.getMessage()), e);
		}
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void load() throws GIOException {
		if (!isLoaded) {
			isLoaded = true;
			buffer = new Float3DVlenBuffer(dataType);
		}
	}

	/** Getters **/

	public Float3DVlenBuffer getData() {
		return buffer;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getLongName() {
		return longName;
	}

	@Override
	public String getUnit() {
		return unit;
	}

	@Override
	public long[] getDimensions() {
		return dimensions;
	}

	/**
	 * @return the {@link #m}
	 */
	public NetcdfVlenLayerManager getLayerManager() {
		return m;
	}

}
