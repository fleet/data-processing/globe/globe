package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleBuffer3D extends AbstractBuffer {

	public DoubleBuffer3D(int xSize, int ySize, int zSize, String name) throws GIOException {
		super(double.class, Double.BYTES, xSize, ySize, zSize, name);
	}

	/** Constructor used for copy **/
	private DoubleBuffer3D(DoubleBuffer3D source) throws IOException {
		super(source);
	}

	@Override
	public DoubleBuffer3D copy() throws IOException {
		return new DoubleBuffer3D(this);
	}

	public double get(int x, int y, int z) {
		return backedBuffer.getDouble(x, y, z);
	}

	public void set(int x, int y, int z, double value) {
		setDirty(true);
		backedBuffer.putDouble(x, y, z, value);
	}

}
