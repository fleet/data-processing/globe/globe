package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class ShortBuffer2D extends AbstractBuffer {

	public ShortBuffer2D(int xSize, int ySize, String name) throws GIOException {
		super(short.class, Short.BYTES, xSize, ySize, name);
	}

	/** Constructor used for copy **/
	private ShortBuffer2D(ShortBuffer2D source) throws IOException {
		super(source);
	}

	@Override
	public ShortBuffer2D copy() throws IOException {
		return new ShortBuffer2D(this);
	}

	public short get(int row, int col) {
		return backedBuffer.getShort(row, col);
	}

	public void set(int row, int col, short value) {
		setDirty(true);
		backedBuffer.putShort(row, col, value);
	}

}
