package fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar;

import java.util.EnumSet;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides an API to manage status layers (status flags and details)
 */
public class DetectionStatusLayer {

	/** Raw status layer */
	private final ByteLoadableLayer2D statusLayer;
	private final ByteLoadableLayer2D detailsLayer;

	/** Constructor **/
	public DetectionStatusLayer(SounderDataContainer container) throws GIOException {
		this.statusLayer = container.getLayer(BathymetryLayers.STATUS);
		this.detailsLayer = container.getLayer(BathymetryLayers.STATUS_DETAIL);
	}

	/**
	 * @return true is status or details layer are dirty
	 */
	public boolean isDirty() {
		return statusLayer.getData().isDirty() || detailsLayer.getData().isDirty();
	}

	/**
	 * Flush status and details layers
	 */
	public void flush() throws GIOException {
		statusLayer.flush();
		detailsLayer.flush();
	}

	//// GETTERS

	/**
	 * @return true if the detection is valid
	 * 
	 * @param iSwath swath index
	 * @param iBeam beam index
	 */
	public boolean isValid(int iSwath, int iBeam) throws GIOException {
		return SonarDetectionStatus.isValid(statusLayer.get(iSwath, iBeam));
	}

	/**
	 * @return true if one of the swath's detection is not flagged {@link SonarDetectionStatus} INVALID_SWATH.
	 * 
	 * @param iSwath swath index
	 */
	public boolean isSwathValid(int iSwath) throws GIOException {
		for (int iBeam = 0; iBeam < statusLayer.getDimensions()[1]; iBeam++) {
			if (!SonarDetectionStatus.getStatus(statusLayer.get(iSwath, iBeam))
					.contains(SonarDetectionStatus.INVALID_SWATH))
				return true;
		}
		return false;
	}

	/**
	 * @return true if the specified flag is active for the detection
	 * 
	 * @param iSwath swath index
	 * @param iBeam beam index
	 */
	public boolean isFlagEnabled(int iSwath, int iBeam, SonarDetectionStatus flag) throws GIOException {
		EnumSet<SonarDetectionStatus> status = SonarDetectionStatus.getStatus(statusLayer.get(iSwath, iBeam));
		return status.contains(flag);
	}

	/**
	 * @return status details
	 * 
	 * @param iSwath swath index
	 * @param iBeam beam index
	 */
	public EnumSet<SonarDetectionStatus> getStatus(int iSwath, int iBeam) throws GIOException {
		return SonarDetectionStatus.getStatus(statusLayer.get(iSwath, iBeam));
	}

	/**
	 * @return status details
	 * 
	 * @param iSwath swath index
	 * @param iBeam beam index
	 */
	public SonarDetectionStatusDetail getDetails(int iSwath, int iBeam) throws GIOException {
		return SonarDetectionStatusDetail.valueOf(detailsLayer.get(iSwath, iBeam));
	}

	/**
	 * check if a given status if able to be modified by classical detection algorithm such as the swath editor, filtri
	 * 
	 * @return true if the detection is not invalidated by one following status: INVALID_SWATH, INVALID_SOUNDING_ROW,
	 *         INVALID_CONVERSION
	 */
	public boolean isDetectionEditable(int iSwath, int iBeam) throws GIOException {
		return (statusLayer.get(iSwath, iBeam) & SonarDetectionStatus.EDITABLE_DETECTION_MASK_BYTE) == 0;
	}

	/**
	 * return true if none of the detection ping beam, conversion validity are set
	 */
	public boolean hasOnlyBeamFlag(int iSwath, int iBeam) throws GIOException {
		byte status = statusLayer.get(iSwath, iBeam);
		return (status & SonarDetectionStatus.INVALID_SWATH.mask) == 0 && // check swath validity
				(status & SonarDetectionStatus.INVALID_SOUNDING_ROW.mask) == 0 && // check sounding row validity
				(status & SonarDetectionStatus.INVALID_ACQUISITION.mask) == 0 && // check acquisition validity
				(status & SonarDetectionStatus.INVALID_CONVERSION.mask) == 0; // check conversion validity
	}

	/**
	 * check if a detection status is valid according given mask
	 * 
	 * @return true if the detection is has not masked flag
	 */
	public boolean isDetectionValidWithMask(int iSwath, int iBeam, byte mask) throws GIOException {
		byte status = statusLayer.get(iSwath, iBeam);
		return (status & mask) == 0; // check masked status validity
	}

	//// SETTERS

	/**
	 * Set a specified status flag on
	 * 
	 * @param iSwath swath index
	 * @param iBeam beam index
	 * @param flag flag to set on
	 */
	public void enableFlag(int iSwath, int iBeam, SonarDetectionStatus flag) throws GIOException {
		EnumSet<SonarDetectionStatus> status = SonarDetectionStatus.getStatus(statusLayer.get(iSwath, iBeam));

		if (flag == SonarDetectionStatus.VALID) // if valid: all flags are disabled (status = 0)
			status.clear();

		status.add(flag);
		statusLayer.set(iSwath, iBeam, SonarDetectionStatus.getValue(status));
	}

	/**
	 * Set a specified status flag off
	 * 
	 * @param iSwath swath index
	 * @param iBeam beam index
	 * @param flag flag to set off
	 */
	public void disableFlag(int iSwath, int iBeam, SonarDetectionStatus flag) throws GIOException {
		EnumSet<SonarDetectionStatus> status = SonarDetectionStatus.getStatus(statusLayer.get(iSwath, iBeam));
		status.remove(flag);
		statusLayer.set(iSwath, iBeam, SonarDetectionStatus.getValue(status));
	}

	/**
	 * Set status details
	 * 
	 * @param iSwath swath index
	 * @param iBeam beam index
	 */
	public void setDetails(int iSwath, int iBeam, SonarDetectionStatusDetail details) throws GIOException {
		detailsLayer.set(iSwath, iBeam, details.getValue());
	}

}
