package fr.ifremer.globe.core.runtime.datacontainer;

import java.util.EnumSet;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer3D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.Float2DVlenLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerBuilder;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IVlenLoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer3D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLoadableLayer2D;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.exception.runtime.AssumptionFailedException;

/**
 * Factory which provides methods to declare layers for a specific container.
 */
public class NetcdfLayerDeclarer {
	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(NetcdfLayerDeclarer.class);

	/**
	 * Declares a container layer for each XsfVariables of a specified file
	 *
	 * @throws GIOException
	 */
	public void declareLayers(NetcdfFile file, DataContainer<?> container, IGroupProvider groupProvider,
			IPredefinedLayerProvider predefinedLayerProvider) throws NCException, GIOException {
		declareLayers(file, file, container, groupProvider, predefinedLayerProvider);
	}

	/**
	 * Declares a container layer for each XsfVariables (recursive call)
	 *
	 * @throws GIOException
	 */
	protected void declareLayers(NetcdfGroup group, NetcdfFile file, DataContainer<?> container,
			IGroupProvider groupProvider, IPredefinedLayerProvider predefinedLayerProvider)
			throws NCException, GIOException {

		for (NetcdfVariable var : group.getVariables()) {
			// filter variable lenght variable
			if (!var.isVlen()) {
				ILayerLoader layerLoader = new NetcdfLayerLoader<>(file, var);
				Optional<PredefinedLayers> preLayer = predefinedLayerProvider.providePredefinedLayer(var);

				try {
					// Predefined layer is defined for this variable and not already used
					if (preLayer.isPresent() && !container.hasLayer(preLayer.get())) {
						container.declareLayer(preLayer.get(), layerLoader);
					} else {
						ILayerBuilder<?> builder = getLayerBuilder(var);
						if (builder == null) {
							continue;
						}

						// read long name
						String longName = var.getName();
						try {
							if (var.hasAttribute(CFStandardNames.CF_LNG_NAME)) {
								longName = var.getAttributeAsString(CFStandardNames.CF_LNG_NAME);
							}
						} catch (NCException e) {
							logger.error("Error reading attribute '" + CFStandardNames.CF_LNG_NAME + "' attribut of "
									+ var.getName() + " : " + e.getMessage(), e);
						}

						// read unit
						String units = "";
						try {
							if (var.hasAttribute(CFStandardNames.CF_UNITS)) {
								units = var.getAttributeAsString(CFStandardNames.CF_UNITS);
							}
						} catch (NCException e) {
							logger.error("Error reading attribute '" + CFStandardNames.CF_UNITS + "' attribut of "
									+ var.getName() + " : " + e.getMessage(), e);
						}

						// declare layer
						container.declareLayer(groupProvider.provideGroupName(group),
								builder.build(var.getName(), longName, units, layerLoader));
					}
				} catch (NCException | TooHighDimension e) {
					// not supported datatype, file ignore it
				}
			} else {
				// declare variable lenght variables,
				NetcdfVlenLayerManager layerManager = new NetcdfVlenLayerManager(file, (NetcdfVlenVariable) var);
				IVlenLoadableLayer layer = createVLenLayer((NetcdfVlenVariable) var, layerManager);
				if (layer != null) {
					container.declareLayer(groupProvider.provideGroupName(group), layer);
				}
			}
		}

		for (NetcdfGroup childGroup : group.getGroups()) {
			declareLayers(childGroup, file, container, groupProvider, predefinedLayerProvider);
		}
	}

	private IVlenLoadableLayer createVLenLayer(NetcdfVlenVariable var, NetcdfVlenLayerManager layerManager)
			throws NCException {
		long[] dims = new long[var.getShape().size()];
		int i = 0;
		for (NCDimension d : var.getShape()) {
			dims[i] = d.getLength();
			i++;
		}
		if (!var.isVlen()) {
			throw new AssumptionFailedException(String.format("Var %s is not a variable Length array", var.getName()));
		}
		String varName = var.getName();
		DataType dataType = var.getUnpackedDataType();
		var possibleDataTypes = EnumSet.of(DataType.BYTE, DataType.UBYTE, DataType.CHAR, DataType.SHORT,
				DataType.USHORT, DataType.INT, DataType.UINT, DataType.LONG, DataType.ULONG, DataType.FLOAT,
				DataType.DOUBLE);
		if (!possibleDataTypes.contains(dataType)) {
			logger.debug("Unsupported VLEN data type {} of variable {}", dataType.name(), varName);
			return null;
		}
		return new Float2DVlenLayer(varName, varName, "", dataType, layerManager, dims);
	}

	/**
	 * @return the layer class corresponding to an XsfVariable
	 */
	public static ILayerBuilder getLayerBuilder(NetcdfVariable var) throws NCException {
		int dim = var.getShape().size();
		if (dim > 3) {
			logger.debug("Not supported shape requested for variable " + var.getName());
			return null;
		}
		if (!var.isVlen()) {
			return getLayerBuilder(dim, var.getUnpackedDataType());
		}
		return null;
	}

	public static ILayerBuilder getLayerBuilder(int dim, DataType type) {
		switch (type) {
		case BOOLEAN:
			return dim == 0 || dim == 1 ? BooleanLoadableLayer1D::new : BooleanLoadableLayer2D::new;
		case BYTE:
			return dim == 0 || dim == 1 ? ByteLoadableLayer1D::new : ByteLoadableLayer2D::new;
		case CHAR:
			return dim == 0 || dim == 1 ? ShortLoadableLayer1D::new : ShortLoadableLayer2D::new;
		case UBYTE:
		case SHORT:
			return dim == 0 || dim == 1 ? ShortLoadableLayer1D::new : ShortLoadableLayer2D::new;
		case USHORT:
		case INT:
			return switch (dim) {
			case 0 | 1 -> IntLoadableLayer1D::new;
			case 2 -> IntLoadableLayer2D::new;
			case 3 -> IntLoadableLayer3D::new;
			default -> null;
			};
		case UINT:
		case LONG:
		case ULONG:
			return dim == 0 || dim == 1 ? LongLoadableLayer1D::new : LongLoadableLayer2D::new;
		case FLOAT:
			return dim == 0 || dim == 1 ? FloatLoadableLayer1D::new : FloatLoadableLayer2D::new;
		case DOUBLE:
			return switch (dim) {
			case 0 | 1 -> DoubleLoadableLayer1D::new;
			case 2 -> DoubleLoadableLayer2D::new;
			case 3 -> DoubleLoadableLayer3D::new;
			default -> null;
			};
		default:
			return null;
		}
	}

	public static Class<?> getLayerClass(int dim, DataType type) {
		switch (type) {
		case BOOLEAN:
			return dim == 0 || dim == 1 ? BooleanLoadableLayer1D.class : BooleanLoadableLayer2D.class;
		case BYTE:
			return dim == 0 || dim == 1 ? ByteLoadableLayer1D.class : ByteLoadableLayer2D.class;
		case CHAR:
			return dim == 0 || dim == 1 ? ShortLoadableLayer1D.class : ShortLoadableLayer2D.class;
		case UBYTE:
		case SHORT:
			return dim == 0 || dim == 1 ? ShortLoadableLayer1D.class : ShortLoadableLayer2D.class;
		case USHORT:
		case INT:
			return dim == 0 || dim == 1 ? IntLoadableLayer1D.class : IntLoadableLayer2D.class;
		case UINT:
		case LONG:
		case ULONG:
			return dim == 0 || dim == 1 ? LongLoadableLayer1D.class : LongLoadableLayer2D.class;
		case FLOAT:
			return dim == 0 || dim == 1 ? FloatLoadableLayer1D.class : FloatLoadableLayer2D.class;
		case DOUBLE:
			return switch (dim) {
			case 0 | 1 -> DoubleLoadableLayer1D.class;
			case 2 -> DoubleLoadableLayer2D.class;
			case 3 -> DoubleLoadableLayer3D.class;
			default -> null;
			};
		default:
			return null;
		}
	}
}
