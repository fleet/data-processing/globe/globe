package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.BooleanBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class BooleanLoadableLayer2D extends BooleanLayer2D implements ILoadableLayer<BooleanBuffer2D> {

	private boolean isLoaded;

	public BooleanLoadableLayer2D(String name, String longName, String unit, ILayerLoader<BooleanBuffer2D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<BooleanBuffer2D> loader;

	@Override
	public ILayerLoader<BooleanBuffer2D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
