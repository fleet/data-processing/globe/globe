package fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 * detailed status for rejected sounding (byte field of status)
 * <ul>manual rejected manually by operator</ul>
 * <ul>rejected automatically</ul>
 * <ul>doubtfull (typically cube algorithm)</ul>
 * <ul>unknown rejected by another reason</ul>
 */

public enum SonarDetectionStatusDetail {
	UNKNOWN((byte)0),
	AUTO((byte)1),
	DOUBTFULL((byte)2),
	MANUAL((byte) 3);
	
	private byte value;
	protected static final Logger logger = LoggerFactory.getLogger(SonarDetectionStatusDetail.class);

	private SonarDetectionStatusDetail(byte value)
	{
		this.value=value;
	}
	/**
	 * return the byte value for the given enum
	 * */
	public byte getValue() {
	   return value;
	}
	
	/**
	 * return the matching enum for the given byte value
	 * 
	 * 
	 * */
	public static SonarDetectionStatusDetail valueOf(byte b) 
	{
		for(SonarDetectionStatusDetail d:values()) {
			if(d.getValue() == b)
				return d;
		}
		logger.error("Unknown byte value mapping for status detail , mapping to unknown as a default behaviour");
		return UNKNOWN;
	}
}
