package fr.ifremer.globe.core.runtime.datacontainer;

import java.nio.ByteOrder;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.TypeMatcher;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class is the implementation of {@link ILayerLoader} for NETCDF files.
 * 
 * <p>
 * It's an interface between an {@link NetcdfVariable} (of the NETCDF API) and an {@link AbstractLayer} (of the
 * {@link SounderDataContainer}).
 * </p>
 */
public class NetcdfLayerLoader<T extends AbstractBuffer> implements ILayerLoader<T> {

	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(NetcdfLayerLoader.class);

	/** Properties **/
	private final NetcdfFile netcdfFile;
	private final NetcdfVariable netcdfVar;

	/** Constructor **/
	public NetcdfLayerLoader(NetcdfFile file, String variablePath) throws NCException, GIOException {
		netcdfFile = file;
		netcdfVar = netcdfFile.getVariable(variablePath);
		if (netcdfVar == null)
			throw new GIOException("NETCDF variable not found : " + variablePath);
	}

	public NetcdfLayerLoader(NetcdfFile file, NetcdfVariable layer) {
		netcdfFile = file;
		netcdfVar = layer;
	}

	public NetcdfVariable getNetcdfVar() {
		return netcdfVar;
	}

	@Override
	public long[] getDimensions() throws GIOException {
		List<NCDimension> netcdfDims = netcdfVar.getShape();
		if (netcdfDims == null)
			throw new GIOException("Error while getting " + netcdfVar.getName() + " dimensions from netcdf file");
		if (netcdfDims.size() > 3)
			throw new TooHighDimension("Unsupported variable " + netcdfVar.getName() + " with dimension > 3");
		if (netcdfDims.isEmpty()) {
			// scalar variable
			return new long[] { 1 };
		}
		long[] result = new long[netcdfDims.size()];
		for (int i = 0; i < result.length; i++)
			result[i] = netcdfDims.get(i).getLength();
		return result;
	}

	@Override
	public boolean load(T array) throws GIOException {
		try {
			array.getLargeArray().setByteOrder(ByteOrder.nativeOrder());
			netcdfVar.read(getDimensions(), array.getLargeArray().getByteBuffer(0),
					TypeMatcher.javaToNc(array.getDataType()), Optional.empty());
			return true;
		} catch (NCException e) {
			throw new GIOException("Error while loading " + netcdfVar.getName() + " from netcdf file", e);
		}
	}

	@Override
	public boolean flush(T array) throws GIOException {
		try {
			netcdfVar.write(getDimensions(), array.getLargeArray().getByteBuffer(0),
					TypeMatcher.javaToNc(array.getDataType()), Optional.empty());
			netcdfFile.synchronize();
		} catch (NCException e) {
			throw new GIOException("Error while loading " + netcdfVar.getName() + " from netcdf file", e);
		}
		return true;
	}

	@Override
	public float getFloatFillValue() {
		return netcdfVar.getFloatFillValue();
	}
}
