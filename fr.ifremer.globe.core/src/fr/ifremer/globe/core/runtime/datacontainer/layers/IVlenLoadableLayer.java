package fr.ifremer.globe.core.runtime.datacontainer.layers;

import org.apache.commons.lang.NotImplementedException;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Interfaces for layer containing vlen data, loading and unloading is set per swath
 */
public interface IVlenLoadableLayer extends IBaseLayer  {
	
	/**
	 * load data for the given swath Id
	 * @throws GIOException 
	 * */
	public void load(int swathIdx) throws GIOException;
	
	/**
	 * Flushes data from the buffer to the file. Implementation is delegate to the {@link ILayerManager}
	 * 
	 * @throws GIOException
	 */
	public default void flush() throws GIOException {
		throw new NotImplementedException();
	}
	/**
	 * check is a layer is loaded
	 * */
	public boolean isLoaded() ;
	
	
	/**
	 * load a layer
	 * initialize storages   
	 * */
	public void load() throws GIOException;
}
