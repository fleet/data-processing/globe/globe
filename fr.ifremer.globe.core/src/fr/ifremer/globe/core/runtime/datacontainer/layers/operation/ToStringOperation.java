/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer.layers.operation;

import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer2D;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Visitor of layer to format its values as String in a abstract way.
 */
public class ToStringOperation implements IAbstractBaseLayerOperation {

	/** Current indexes row/column/... */
	protected int[] indexes;

	/** Resulting value of the conversion */
	protected StringBuilder outputStringBuilder;

	/** Constructor */
	public ToStringOperation() {
		outputStringBuilder = new StringBuilder();
	}

	/** Constructor */
	public ToStringOperation(StringBuilder outputStringBuilder) {
		this.outputStringBuilder = outputStringBuilder;
	}

	/** {@inheritDoc} */
	@Override
	public void visit(BooleanLayer2D layer) throws GIOException {
		outputStringBuilder.append(layer.getValue(indexes[0], indexes[1]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ByteLayer2D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0], indexes[1]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(DoubleLayer2D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0], indexes[1]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(FloatLayer2D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0], indexes[1]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(IntLayer2D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0], indexes[1]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(LongLayer2D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0], indexes[1]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ShortLayer2D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0], indexes[1]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(BooleanLayer1D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ByteLayer1D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(DoubleLayer1D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(FloatLayer1D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(IntLayer1D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(LongLayer1D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0]));
	}

	/** {@inheritDoc} */
	@Override
	public void visit(ShortLayer1D layer) throws GIOException {
		outputStringBuilder.append(layer.get(indexes[0]));
	}

	/** Getter of {@link #outputStringBuilder} */
	public StringBuilder getOutputStringBuilder() {
		return outputStringBuilder;
	}

	/** @return the result of the operation */
	public String getResult() {
		String result = outputStringBuilder.toString();
		outputStringBuilder.setLength(0);
		return result;
	}

	/** Getter of {@link #indexes} */
	public int[] getIndexes() {
		return indexes;
	}

	/** Setter of {@link #indexes} */
	public void setIndexes(int[] indexes) {
		this.indexes = indexes;
	}

}
