package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ShortBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class ShortLayer1D extends AbstractBaseLayer<ShortBuffer1D> {

	public ShortLayer1D(String name, String longName, String unit) {
		super(name, longName, unit, ShortBuffer1D::new);
	}

	public short get(int index) {
		return buffer.get(index);
	}

	public void set(int index, short value) {
		buffer.set(index, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}

}
