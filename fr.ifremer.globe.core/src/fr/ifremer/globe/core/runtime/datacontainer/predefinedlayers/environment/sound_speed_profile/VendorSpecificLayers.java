package fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.sound_speed_profile;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLoadableLayer1D;

/** Class generated by PredefinedLayersGenerator **/
public class VendorSpecificLayers {

	private VendorSpecificLayers() {}

	public static final String GROUP = "/Environment/Sound_speed_profile/Vendor_specific";

	/** Layer: CONDUCTIVITY **/
	public static final String CONDUCTIVITY_VARIABLE_NAME ="conductivity";
	//public static final PredefinedLayers<FloatVlenLoadableLayer1D> CONDUCTIVITY = new PredefinedLayers<>(GROUP,"conductivity","conductivity","S/m", DataKind.continuous, FloatVlenLoadableLayer1D::new);

	/** Layer: CONDUCTIVITY_FLAG **/
	public static final String CONDUCTIVITY_FLAG_VARIABLE_NAME ="conductivity_flag";
	public static final PredefinedLayers<ByteLoadableLayer1D> CONDUCTIVITY_FLAG = new PredefinedLayers<>(GROUP,"conductivity_flag","Conductivity flag","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: FREQUENCY **/
	public static final String FREQUENCY_VARIABLE_NAME ="frequency";
	public static final PredefinedLayers<FloatLoadableLayer1D> FREQUENCY = new PredefinedLayers<>(GROUP,"frequency","frequency","Hz", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: POSITION_FLAG **/
	public static final String POSITION_FLAG_VARIABLE_NAME ="position_flag";
	public static final PredefinedLayers<ByteLoadableLayer1D> POSITION_FLAG = new PredefinedLayers<>(GROUP,"position_flag","Position flag","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: PRESSURE **/
	public static final String PRESSURE_VARIABLE_NAME ="pressure";
	//public static final PredefinedLayers<FloatVlenLoadableLayer1D> PRESSURE = new PredefinedLayers<>(GROUP,"pressure","Pressure","p", DataKind.continuous, FloatVlenLoadableLayer1D::new);

	/** Layer: PRESSURE_FLAG **/
	public static final String PRESSURE_FLAG_VARIABLE_NAME ="pressure_flag";
	public static final PredefinedLayers<ByteLoadableLayer1D> PRESSURE_FLAG = new PredefinedLayers<>(GROUP,"pressure_flag","Pressure flag","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: SAMPLE_CONTENT_VALIDITY **/
	public static final String SAMPLE_CONTENT_VALIDITY_VARIABLE_NAME ="sample_content_validity";
	public static final PredefinedLayers<ShortLoadableLayer1D> SAMPLE_CONTENT_VALIDITY = new PredefinedLayers<>(GROUP,"sample_content_validity","","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: SAMPLE_RATE **/
	public static final String SAMPLE_RATE_VARIABLE_NAME ="sample_rate";
	public static final PredefinedLayers<FloatLoadableLayer1D> SAMPLE_RATE = new PredefinedLayers<>(GROUP,"sample_rate","Sample rate","", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: SENSOR_FORMAT **/
	public static final String SENSOR_FORMAT_VARIABLE_NAME ="sensor_format";
	//public static final PredefinedLayers<StringLoadableLayer1D> SENSOR_FORMAT = new PredefinedLayers<>(GROUP,"sensor_format","Sensor format","", DataKind.continuous, StringLoadableLayer1D::new);

	/** Layer: SOUND_VELOCITY_ALGORITHM **/
	public static final String SOUND_VELOCITY_ALGORITHM_VARIABLE_NAME ="sound_velocity_algorithm";
	public static final PredefinedLayers<ByteLoadableLayer1D> SOUND_VELOCITY_ALGORITHM = new PredefinedLayers<>(GROUP,"sound_velocity_algorithm","Sound velocity algorithm","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: SOUND_VELODICY_SOURCE_FLAG **/
	public static final String SOUND_VELODICY_SOURCE_FLAG_VARIABLE_NAME ="sound_velodicy_source_flag";
	public static final PredefinedLayers<ByteLoadableLayer1D> SOUND_VELODICY_SOURCE_FLAG = new PredefinedLayers<>(GROUP,"sound_velodicy_source_flag","Sound velocity sourceg flag","", DataKind.continuous, ByteLoadableLayer1D::new);
}
