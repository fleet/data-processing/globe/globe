/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer.layers.walker;

import java.util.List;
import java.util.stream.IntStream;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalkerParameters.AbstractBaseLayersSupplier;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * /** Walks over {@link AbstractBaseLayer} to perform a processing on each value.<br>
 * {@link AbstractBaseLayer} to walk on are provided by a {@link AbstractBaseLayersSupplier}.<br>
 * Each value are consumed by a {@link ILayersConsumer}.<br>
 *
 * Usage example :
 * 
 * <pre>
 * {@code
 *     	Optional<IInfoStore> option = InfoStoreUtils.getInfoStore(inputFile);
 *      if (option.isPresent()) {
 *         IInfoStore info = option.get();
 *         try (ILayersConsumer consumer = new ... ) {
 *            AbstractBaseLayersSupplier supplier = new ...
 *            LayersWalkerParameters parameters = new LayersWalkerParameters(info, supplier, consumer);
 *            
 *            // Optional initializations
 *            parameters.setBookOwner(...); // Owner used with IDataContainerFactory.book 
 *            parameters.setIndexesPredicate(...); // Which indexes to consume 
 *            
 *            layersWalker.walk(parameters, monitor);
 *         }
 *      }
 * }
 * </pre>
 *
 */
@Creatable
public class LayersWalker {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(LayersWalker.class);

	/** Factory of the sounder data container */
	@Inject
	protected IDataContainerFactory containerFactory;

	/**
	 * Constructor
	 */
	public LayersWalker() {
		super();
	}

	/**
	 * Run the walking process. DataContainer is booked if necessary
	 * 
	 * @throws GIOException when IOException occurs
	 * @throws OperationCanceledException when user canceled the process
	 */
	public <U extends IDataContainerInfo, T extends DataContainer<U>> void walk(
			LayersWalkerParameters<U, T> walkerParameters, IProgressMonitor monitor) throws GIOException {
		logger.info("Walking process begin");

		try (IDataContainerToken<U> token = containerFactory.book(walkerParameters.getInfo(),
				walkerParameters.getBookOwner())) {
			T dataContainer = (T) token.getDataContainer();
			walk(dataContainer, walkerParameters, monitor);
		}
		logger.info("Walking process end");
	}

	/**
	 * Run the walking process on the specified DataContainer.
	 * 
	 * @throws GIOException when IOException occurs
	 * @throws OperationCanceledException when user canceled the process
	 */
	public <U extends IDataContainerInfo, T extends DataContainer<U>> void walk(T dataContainer,
			LayersWalkerParameters<U, T> walkerParameters, IProgressMonitor monitor) throws GIOException {
		List<AbstractBaseLayer<?>> layers = walkerParameters.getLayersSupplier().apply(dataContainer);
		int[] dimensions = checkDimensions(layers);
		walkerParameters.getLayersConsumer().open();
		walkerParameters.getLayersConsumer().getReady(dataContainer, layers);
		IterateData<U, T> iterateData = new IterateData<>(dimensions, dataContainer, layers,
				SubMonitor.convert(monitor, dimensions[0]));
		iterateDimensions(walkerParameters, iterateData, 0);
		walkerParameters.getLayersConsumer().conclude();
	}

	/**
	 * Iterates over indexes recursively
	 * 
	 * @throws GIOException when IOException occurs
	 * @throws OperationCanceledException when user canceled the process
	 */
	protected <U extends IDataContainerInfo, T extends DataContainer<U>> void iterateDimensions(
			LayersWalkerParameters<U, T> layersWalkerParameters, IterateData<U, T> iterateData, int dimensionIndex)
			throws GIOException {
		// Iterates over each index of the current dimension
		for (int index = 0; index < iterateData.dimensions[dimensionIndex]; index++) {
			iterateData.indexes[dimensionIndex] = index;
			ILayersConsumer<U, T> consumer = layersWalkerParameters.getLayersConsumer();
			// If current dimension is the deepest -> apply the process to each suitable index
			if (dimensionIndex == iterateData.dimensions.length - 1) {
				if (layersWalkerParameters.getIndexesPredicate().test(iterateData.sounderDataContainer,
						iterateData.indexes)) {
					iterateLastDimension(consumer, iterateData, dimensionIndex);
				}
			}
			// Else : go to a deeper dimension
			else {
				consumer.initiateLevel(dimensionIndex, iterateData.indexes);
				iterateDimensions(layersWalkerParameters, iterateData, dimensionIndex + 1);
				consumer.endLevel(dimensionIndex, iterateData.indexes);
			}
			if (dimensionIndex == 0) {
				if (iterateData.monitor.isCanceled()) {
					throw new OperationCanceledException();
				}
				iterateData.monitor.worked(1);
			}
		}
	}

	public <U extends IDataContainerInfo, T extends DataContainer<U>> void iterateLastDimension(
			ILayersConsumer<U, T> consumer, IterateData<U, T> iterateData, int dimensionIndex) throws GIOException {
		consumer.initiateLevel(dimensionIndex, iterateData.indexes);
		// Not using the foreach syntax to avoid several MO of ArrayList.ListItr intances...
		for (int i = 0; i < iterateData.layers.size(); i++) {
			consumer.processLayer(iterateData.indexes, iterateData.layers.get(i));
		}
		consumer.endProcessLayers();
		consumer.endLevel(dimensionIndex, iterateData.indexes);
	}

	/** Check all layer's dimensions */
	protected <L extends AbstractBaseLayer<?>> int[] checkDimensions(List<L> layers) {
		// Check dimensions
		int[] result = null;
		for (L layer : layers) {
			long[] layerDim = layer.getDimensions();
			if (result == null) {
				int[] shape = new int[layerDim.length];
				IntStream.range(0, layerDim.length).forEach(index -> shape[index] = (int) layerDim[index]);
				result = shape;
			} else {
				Assert.isTrue(result.length == layerDim.length, "All layers must have the same dimenion");
				for (int i = 0; i < result.length; i++) {
					Assert.isTrue(result[i] == (int) layerDim[i], "All layers must have the same dimenion");
				}
			}
		}
		return result;
	}

	/** Data used to group all parameters used by {@link #iterateDimensions} */
	protected static class IterateData<U extends IDataContainerInfo, T extends DataContainer<U>> {
		protected int[] dimensions;
		protected int[] indexes;
		protected T sounderDataContainer;
		protected List<AbstractBaseLayer<?>> layers;
		protected IProgressMonitor monitor;

		/** Constructor */
		public IterateData(int[] dimensions, T sounderDataContainer, List<AbstractBaseLayer<?>> layers,
				IProgressMonitor monitor) {
			this.dimensions = dimensions;
			indexes = new int[dimensions.length];
			this.sounderDataContainer = sounderDataContainer;
			this.layers = layers;
			this.monitor = monitor;
		}
	}

}