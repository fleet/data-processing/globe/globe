package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class IntBuffer3D extends AbstractBuffer {

	public IntBuffer3D(int xSize, int ySize, int zSize, String name) throws GIOException {
		super(int.class, Integer.BYTES, xSize, ySize, zSize, name);
	}

	/** Constructor used for copy **/
	private IntBuffer3D(IntBuffer3D source) throws IOException {
		super(source);
	}

	@Override
	public IntBuffer3D copy() throws IOException {
		return new IntBuffer3D(this);
	}

	public int get(int x, int y, int z) {
		return backedBuffer.getInt(x, y, z);
	}

	public void set(int x, int y, int z, int value) {
		setDirty(true);
		backedBuffer.putInt(x, y, z, value);
	}
}
