package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.core.runtime.datacontainer.BufferAllocator;
import fr.ifremer.globe.utils.array.impl.LargeArray;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Base class for buffer handling layer data
 */
public abstract class AbstractBuffer implements IBaseBuffer {

	protected LargeArray backedBuffer;
	private boolean isDirty;
	Class<?> dataType;

	/**
	 * Constructor 1D
	 */
	protected AbstractBuffer(Class<?> dataType, int elementSizeInBytes, int xSize, String name) throws GIOException {
		this.dataType = dataType;
		backedBuffer = BufferAllocator.allocateBuffer1D(xSize, elementSizeInBytes, name);
	}

	/**
	 * Constructor 2D
	 */
	protected AbstractBuffer(Class<?> dataType, int elementSizeInBytes, int xSize, int ySize, String name)
			throws GIOException {
		this.dataType = dataType;
		backedBuffer = BufferAllocator.allocateBuffer2D(xSize, ySize, elementSizeInBytes, name);
	}

	/**
	 * Constructor 3D
	 */
	protected AbstractBuffer(Class<?> dataType, int elementSizeInBytes, int xSize, int ySize, int zSize, String name)
			throws GIOException {
		this.dataType = dataType;
		backedBuffer = BufferAllocator.allocateBuffer3D(xSize, ySize, zSize, elementSizeInBytes, name);
	}

	/** Constructor used by copy methods **/
	protected AbstractBuffer(AbstractBuffer source) throws IOException {
		this.isDirty = source.isDirty;
		this.dataType = source.dataType;
		this.backedBuffer = source.backedBuffer.cloneArray();
	}

	/**
	 * @return the java data type stored in this buffer
	 */
	public Class<?> getDataType() {
		return dataType;
	}

	/**
	 * Indicates that the buffer has been modified and need to be flushed/saved
	 */
	public boolean isDirty() {
		return isDirty;
	}

	/**
	 * mark the buffer as modified
	 */
	public void setDirty(boolean isDirty) {
		this.isDirty = isDirty;
	}

	/**
	 * @return the {@link LargeArray} storing data
	 */
	public LargeArray getLargeArray() {
		return backedBuffer;
	}

	/**
	 * Replaces the backed buffer ({@link LargeArray} storing data)
	 */
	public void replaceLargeArray(LargeArray newLargeArray) {
		backedBuffer.dispose();
		backedBuffer = newLargeArray;
	}

	public void dispose() {
		backedBuffer.dispose();
	}

	/**
	 * @return a copy of the current buffer
	 */
	public abstract AbstractBuffer copy() throws IOException;

}
