package fr.ifremer.globe.core.runtime.datacontainer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.vlen.Float3DVlenBuffer;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable;
import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable.VlenWrapper;
import fr.ifremer.globe.netcdf.api.operation.FloatUnaryOperator;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class is the implementation of {@link ILayerManager} for NETCDF files.
 *
 * <p>
 * It's an interface between an {@link NetcdfVariable} (of the NETCDF API) and an {@link AbstractLayer} (of the
 * {@link SounderDataContainer}).
 * </p>
 */
public class NetcdfVlenLayerManager {

	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(NetcdfVlenLayerManager.class);

	/** Properties **/
	private final NetcdfFile netcdfFile;
	private final NetcdfVlenVariable netcdfVar;

	public NetcdfVlenLayerManager(NetcdfFile file, NetcdfVlenVariable variable) throws NCException {
		netcdfFile = file;
		netcdfVar = variable;
		// retrieve expected variables and fail if not found
		if (netcdfVar == null) {
			throw new NCException("NETCDF variable not found ");
		}

	}

	public Map<String, String> getAttributes() throws GIOException {
		try {
			return netcdfVar.getAttributes();
		} catch (NCException e) {
			throw new GIOException("Error while getting " + netcdfVar.getName() + " attritbutes from netcdf file", e);
		}
	}

	/**
	 * load data along the dimension index and first the first set of subbeams and fill results in the given buffer
	 */
	public void load(int index, Float3DVlenBuffer array) throws NCException {
		// ensure that we are bidimensionnal
		long secondDim = netcdfVar.getShape().get(1).getLength();
		long[] start = { index, 0, 0 };
		long[] count = { 1, secondDim, 1 };
		// we read only one set of data along the third dimension
		Vlen_t[] values = netcdfVar.get_vlen(start, count);
		int maxLen = 0;
		for (Vlen_t v : values) {
			maxLen = Math.max(maxLen, v.len);
		}
		// allocate a destination buffer
		ByteBuffer matrix = ByteBuffer.allocateDirect((int) (maxLen * secondDim * Float.BYTES));
		matrix.order(ByteOrder.nativeOrder());
		matrix.rewind();
		VlenWrapper w = netcdfVar.getVlenWrapper();
		FloatUnaryOperator transform = netcdfVar.getOperation();
		for (int col = 0; col < secondDim; col++) {
			Vlen_t v = values[col];
			// fill matrix with values
			for (int row = 0; row < v.len; row++) {
				matrix.putFloat(transform.applyAsFloat(w.get(v, row)));
			}
			// Fill remaining with Nan
			for (int row = v.len; row < maxLen; row++) {
				matrix.putFloat(Float.NaN);
			}
		}
		netcdfVar.free_vlen(values);
		array.setBuffer(matrix, DataType.FLOAT, maxLen, (int) secondDim, 1);

	}

	/**
	 * @return the {@link #netcdfVar}
	 */
	public NetcdfVlenVariable getNetcdfVariable() {
		return netcdfVar;
	}

}
