package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.LongBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class LongLoadableLayer2D extends LongLayer2D implements ILoadableLayer<LongBuffer2D> {

	private boolean isLoaded;

	public LongLoadableLayer2D(String name, String longName, String unit, ILayerLoader<LongBuffer2D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<LongBuffer2D> loader;

	@Override
	public ILayerLoader<LongBuffer2D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
