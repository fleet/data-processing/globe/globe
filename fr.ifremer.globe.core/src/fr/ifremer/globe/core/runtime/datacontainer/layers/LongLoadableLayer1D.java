package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.LongBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class LongLoadableLayer1D extends LongLayer1D implements ILoadableLayer<LongBuffer1D> {

	private boolean isLoaded;

	public LongLoadableLayer1D(String name, String longName, String unit, ILayerLoader<LongBuffer1D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<LongBuffer1D> loader;

	@Override
	public ILayerLoader<LongBuffer1D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
