package fr.ifremer.globe.core.runtime.datacontainer.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link IDataContainerFactory}.
 */
@Component(name = "DataContainerFactory", service = IDataContainerFactory.class)
public class DataContainerFactory implements IDataContainerFactory {

	public static final Logger LOGGER = LoggerFactory.getLogger(DataContainerFactory.class);

	/**
	 * Token : unmutable object returned after a booking. Allows to access to the {@link DataContainer}.
	 * 
	 * candidate for Java 16 "records" ... ?
	 */
	private class DataContainerToken<U extends IDataContainerInfo> implements IDataContainerToken<U> {

		/** Properties **/
		protected final IDataContainerOwner owner;
		protected final U fileInfo;
		protected final DataContainer<U> dataContainer;

		/** Constructor **/
		public DataContainerToken(IDataContainerOwner owner, U fileInfo, DataContainer<U> dataContainer) {
			this.owner = owner;
			this.fileInfo = fileInfo;
			this.dataContainer = dataContainer;
			tokens.add(this);
		}

		@Override
		public void close() {
			synchronized (tokens) {
				LOGGER.debug("Container released: {} by {}", fileInfo.getFilename(), owner.getName());
				tokens.remove(this);
				// if data container is no more used: dispose it
				if (!isBooked(fileInfo)) {
					LOGGER.debug("Container no longer used : dispose.");
					dataContainer.dispose();
				}
			}
		}

		@Override
		public DataContainer<U> getDataContainer() {
			return dataContainer;
		}

		@Override
		public IDataContainerOwner getOwner() {
			return owner;
		}
	}

	/**
	 * {@link DataContainerToken} for {@link SounderDataContainer}.
	 */
	private class SounderDataContainerToken extends DataContainerToken<ISounderNcInfo>
			implements ISounderDataContainerToken {

		public SounderDataContainerToken(IDataContainerOwner owner, ISounderNcInfo file, SounderDataContainer dc) {
			super(owner, file, dc);
		}

		@Override
		public SounderDataContainer getDataContainer() {
			return (SounderDataContainer) dataContainer;
		}
	}

	/** Token list */
	private List<DataContainerToken<?>> tokens = Collections.synchronizedList(new ArrayList<>());

	@SuppressWarnings("unchecked")
	@Override
	public synchronized <U extends IDataContainerInfo> DataContainerToken<U> book(U info, IDataContainerOwner owner)
			throws GIOException {
		synchronized (tokens) {
			// if possible book a SounderDataContainer
			if (info instanceof ISounderNcInfo)
				return (DataContainerToken<U>) book((ISounderNcInfo) info, owner);

			// get tokens for the target file
			List<DataContainerToken<U>> relativeTokens = tokens.stream()
					.filter(token -> token.fileInfo.getAbsolutePath().equals(info.getAbsolutePath()))
					.map(DataContainerToken.class::cast).collect(Collectors.toList());

			// check if a matching token already exist
			Optional<DataContainerToken<U>> result = relativeTokens.stream().filter(t -> t.owner == owner).findFirst();
			if (result.isPresent())
				return result.get();

			// check if container is not already booked by an owner not "read only"
			if (!owner.isReadOnly() && !relativeTokens.isEmpty())
				throw new GIOException(
						"File " + info.getFilename() + " not available : already opened by an other process/editor : "
								+ relativeTokens.get(0).owner.getName());

			// else : compute a new token
			LOGGER.debug("Container booked: {} by {} (owner count = {})", info.getFilename(), owner.getName(),
					relativeTokens.size() + 1);
			DataContainer<U> dataContainer;
			try {
				dataContainer = relativeTokens.isEmpty() ? new DataContainer<>(info, owner.isReadOnly())
						: relativeTokens.get(0).getDataContainer();
			} catch (GException e) {
				throw new GIOException("Error with data container creation : " + e.getMessage(), e);
			}
			return new DataContainerToken<>(owner, info, dataContainer);
		}
	}

	@Override
	public synchronized SounderDataContainerToken book(ISounderNcInfo info, IDataContainerOwner owner)
			throws GIOException {
		// get tokens for the target file
		synchronized (tokens) {
			List<SounderDataContainerToken> relativeTokens = tokens.stream().filter(token -> token.fileInfo == info)//
					.map(SounderDataContainerToken.class::cast).collect(Collectors.toList());

			// check if a matching token already exist
			var result = relativeTokens.stream().filter(t -> t.owner == owner).findFirst();
			if (result.isPresent())
				return result.get();

			// check if container is not already booked or by an owner not "read only" OR for any other container for
			// write access
			if (!owner.isReadOnly() && !relativeTokens.isEmpty()) { // || relativeTokens.stream().anyMatch(token ->
																	// !token.owner.isReadOnly())
				throw new GIOException(
						"File " + info.getFilename() + " not available : already opened by an other process/editor : "
								+ relativeTokens.get(0).owner.getName());
			}

			// else : compute a new token
			LOGGER.debug("Container booked: {} by {} (owner count = {})", info.getFilename(), owner.getName(),
					relativeTokens.size() + 1);
			SounderDataContainer dataContainer;
			try {
				dataContainer = relativeTokens.isEmpty() ? new SounderDataContainer(info, owner.isReadOnly())
						: relativeTokens.get(0).getDataContainer();
			} catch (Exception e) {
				throw new GIOException("Error with data container creation : " + e.getMessage(), e);
			}
			return new SounderDataContainerToken(owner, info, dataContainer);
		}
	}

	@Override
	public <U extends IDataContainerInfo> List<IDataContainerToken<U>> book(List<U> infoStores,
			IDataContainerOwner owner) throws GIOException {
		List<IDataContainerToken<U>> containers = new ArrayList<>();
		for (U info : infoStores) {
			try {
				containers.add(book(info, owner));
			} catch (GIOException e) {
				// if one booking has failed, close containers already opened
				containers.forEach(IDataContainerToken::close);
				throw e;
			}
		}
		return containers;
	}

	@Override
	public List<ISounderDataContainerToken> bookSounderDataContainers(List<ISounderNcInfo> infoStores,
			IDataContainerOwner owner) throws GIOException {
		List<ISounderDataContainerToken> containers = new ArrayList<>();
		for (ISounderNcInfo info : infoStores) {
			try {
				containers.add(book(info, owner));
			} catch (GIOException e) {
				// if one booking has failed, close containers already opened
				containers.forEach(IDataContainerToken::close);
				throw e;
			}
		}
		return containers;
	}

	@Override
	public boolean isBooked(IDataContainerInfo info) {
		synchronized (tokens) {
			return tokens.stream().anyMatch(token -> token.fileInfo == info);
		}
	}

	@Override
	public boolean isBooked(List<? extends IDataContainerInfo> infos) {
		synchronized (tokens) {
			return infos.stream().anyMatch(this::isBooked);
		}
	}

}
