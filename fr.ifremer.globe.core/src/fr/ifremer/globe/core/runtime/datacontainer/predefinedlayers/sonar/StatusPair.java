package fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar;

/**
 * {@link SonarDetectionStatus} and {@link SonarDetectionStatusDetail} pair
 */
public class StatusPair {

	byte status;
	byte detail;

	public StatusPair(byte status, byte detail) {
		super();
		this.status = status;
		this.detail = detail;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public byte getDetail() {
		return detail;
	}

	public void setDetail(byte detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "StatusPair [status=0x" + Integer.toHexString(status) + ", detail=0x" + Integer.toHexString(detail) + "]";
	}

	// Equals/hashCode overriding

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusPair other = (StatusPair) obj;
		if (detail != other.detail)
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + detail;
		result = prime * result + status;
		return result;
	}

}


