package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer3D;
import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleLoadableLayer3D extends DoubleLayer3D implements ILoadableLayer<DoubleBuffer3D> {

	private boolean isLoaded;

	public DoubleLoadableLayer3D(String name, String longName, String unit, ILayerLoader<DoubleBuffer3D> l)
			throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<DoubleBuffer3D> loader;

	@Override
	public ILayerLoader<DoubleBuffer3D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
