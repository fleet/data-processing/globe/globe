package fr.ifremer.globe.core.runtime.datacontainer.service;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;

/**
 * This interface must be implemented by any client of {@link IDataContainerFactory} service which want to book a
 * {@link DataContainer}.
 */
public interface IDataContainerOwner {

	/**
	 * @return the name of this owner, by default the class name.
	 */
	default String getName() {
		return this.getClass().getSimpleName();
	}

	/**
	 * Defines if the access is "read only". If true, the {@link DataContainer} can be booked even if its already owned.
	 * 
	 * By default the owner is NOT "read only".
	 * 
	 * Be careful, there is no check after the booking ! A {@link DataContainer} layer can be updated after a book with
	 * a "read only" owner!! This option is just an help to avoid several booking from "writers", but not a security
	 * guaranty! (this should required an important update of {@link DataContainer} & layers ...).
	 */
	default boolean isReadOnly() {
		return false;
	}

	/** Gererate an owner */
	static IDataContainerOwner generate() {
		return new IDataContainerOwner() {
		};
	}

	/** Gererate an owner with the specified read only option */
	static IDataContainerOwner generate(String name, boolean isReadOnly) {
		return new IDataContainerOwner() {

			@Override
			public String getName() {
				return name;
			}

			@Override
			public boolean isReadOnly() {
				return isReadOnly;
			}
		};
	}
}
