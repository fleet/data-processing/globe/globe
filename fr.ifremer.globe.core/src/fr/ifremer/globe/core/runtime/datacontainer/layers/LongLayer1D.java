package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.LongBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class LongLayer1D extends AbstractBaseLayer<LongBuffer1D> {

	public LongLayer1D(String name, String longName, String unit) {
		super(name, longName, unit, LongBuffer1D::new);
	}

	public long get(int index) {
		return buffer.get(index);
	}

	public void set(int index, long value) {
		buffer.set(index, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}

}
