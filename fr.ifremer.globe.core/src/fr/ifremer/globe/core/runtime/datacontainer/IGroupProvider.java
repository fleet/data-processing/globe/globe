/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer;

import fr.ifremer.globe.netcdf.api.NetcdfGroup;

/**
 * Provides layer names
 */
public interface IGroupProvider {

	/**
	 * @return the name of the layer for the specified group
	 */
	String provideGroupName(NetcdfGroup group);
}
