/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer.layers.walker;

import java.util.List;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.exception.GIOException;

/** Data used to group all parameters used by {@link LayersWalker#walk} */
public class LayersWalkerParameters<U extends IDataContainerInfo, T extends DataContainer<U>> implements IDataContainerOwner {

	/** Info store of the layered file */
	protected U info;
	/** Instance in charge of supplying all layers to walk on */
	protected AbstractBaseLayersSupplier<U, T> layersSupplier;
	/** Optional predicate to filter some unwanted indexes */
	protected IndexesPredicate<U, T> indexesPredicate = (u, t) -> true;
	/** Consumer of layers. This instance will be call for each values of each supplied layer */
	protected ILayersConsumer<U, T> layersConsumer;
	/** Owner for booking the DataContainer */
	protected IDataContainerOwner bookOwner = this;

	/** Constructor */
	public LayersWalkerParameters(U info, AbstractBaseLayersSupplier<U, T> layersSupplier,
			ILayersConsumer<U, T> layerConsumer) {
		this.info = info;
		this.layersSupplier = layersSupplier;
		this.layersConsumer = layerConsumer;
	}

	/** Getter of {@link #info} */
	public U getInfo() {
		return info;
	}

	/** Getter of {@link #layersSupplier} */
	public AbstractBaseLayersSupplier<U, T> getLayersSupplier() {
		return layersSupplier;
	}

	/** Getter of {@link #indexesPredicate} */
	public IndexesPredicate<U, T> getIndexesPredicate() {
		return indexesPredicate;
	}

	/** Setter of {@link #indexesPredicate} */
	public void setIndexesPredicate(IndexesPredicate<U, T> indexesPredicate) {
		this.indexesPredicate = indexesPredicate;
	}

	/** Getter of {@link #layersConsumer} */
	public ILayersConsumer<U, T> getLayersConsumer() {
		return layersConsumer;
	}

	/** Getter of {@link #bookOwner} */
	public IDataContainerOwner getBookOwner() {
		return bookOwner;
	}

	/** Setter of {@link #bookOwner} */
	public void setBookOwner(IDataContainerOwner bookOwner) {
		this.bookOwner = bookOwner;
	}

	@FunctionalInterface
	public interface IndexesPredicate<U extends IDataContainerInfo, T extends DataContainer<U>> {
		boolean test(T sounderDataContainer, int[] indexes) throws GIOException;
	}

	@FunctionalInterface
	public interface AbstractBaseLayersSupplier<U extends IDataContainerInfo, T extends DataContainer<U>> {
		List<AbstractBaseLayer<? extends AbstractBuffer>> apply(T sounderDataContainer) throws GIOException;
	}

}
