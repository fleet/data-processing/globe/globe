package fr.ifremer.globe.core.runtime.datacontainer;

import java.io.Closeable;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Interface implemented by file which could be managed by a {@link DataContainer}
 */
public interface IDataContainerInfo extends IFileInfo, Closeable {

	/**
	 * Open the file
	 *
	 * @throws GIOException
	 */
	public void open(boolean readOnly) throws GIOException;

	/**
	 * Declares data container's layers
	 */
	public void declareLayers(DataContainer<?> container) throws GException;

}
