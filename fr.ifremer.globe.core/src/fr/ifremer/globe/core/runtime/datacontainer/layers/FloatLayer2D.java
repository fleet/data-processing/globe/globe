package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class FloatLayer2D extends AbstractBaseLayer<FloatBuffer2D> {

	public FloatLayer2D(String name, String longName, String unit) {
		super(name, longName, unit, FloatBuffer2D::new);
	}

	public float get(int row, int col) {
		return buffer.get(row, col);
	}

	public void set(int row, int col, float value) {
		buffer.set(row, col, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
