package fr.ifremer.globe.core.runtime.datacontainer;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IVlenLoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class represents a generic data container.
 *
 * <p>
 * The container is composed of groups which contain layers. A layer is the representation of a specific variable of a
 * sounder data file (previously called "attribute" in old containers).
 * </p>
 */
public class DataContainer<T extends IDataContainerInfo> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataContainer.class);

	/** Data container file info **/
	protected final T info;

	/** Properties */
	private Map<String, Group> groups = new HashMap<>();

	/**
	 * Constructor
	 */
	public DataContainer(T info, boolean readOnly) throws GException {
		this.info = info;
		this.info.open(readOnly);
		info.declareLayers(this);
		LOGGER.debug("Container created : {} with {} layers.", info.getFilename(), getLayerCount());
	}

	/**
	 * Dispose method (remove temporary files)
	 */
	public synchronized void dispose() {
		LOGGER.debug("Container disposed : {} ", info.getFilename());
		for (Group g : groups.values())
			g.getLayers().values().forEach(ILoadableLayer::dispose);
		IOUtils.closeQuietly(info);
	}

	/** @return the linked ISounderNcInfo */
	public T getInfo() {
		return info;
	}

	/** @return true if the specified group is declared within this container */
	public boolean hasGroup(String group) {
		return groups.containsKey(group);
	}

	/** Get a group by name, if it doesn't exist, it's created. */
	private Group getOrCreateGroup(String groupName) {
		if (!groups.containsKey(groupName))
			groups.put(groupName, new Group(groupName));
		return groups.get(groupName);
	}

	/**
	 * @return the specified group
	 * @throws GIOException if there is no corresponding group
	 */
	public Group getGroup(String groupName) throws GIOException {
		if (!groups.containsKey(groupName))
			throw new GIOException("Invalid group access: group " + groupName + " not declared in the container");
		return groups.get(groupName);
	}

	/**
	 * @return an optional which contains the specified group or is empty if it does not exist
	 */
	public Optional<Group> getOptionalGroup(String groupName) {
		return groups.containsKey(groupName) ? Optional.of(groups.get(groupName)) : Optional.empty();
	}

	/**
	 * @return an unmodifiable collection of groups
	 */
	public Collection<Group> getGroups() {
		return Collections.unmodifiableCollection(groups.values());
	}

	/**
	 * Applies {@link IAbstractBaseLayerOperation} on each layer of this {@link DataContainer}.
	 */
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		for (var group : groups.values())
			for (var layer : group.getLayers().values())
				layer.process(operation);
	}

	/**
	 * Applies {@link IAbstractBaseLayerOperation} on each layer accepted by the filter.
	 */
	public void process(Predicate<IBaseLayer> filter, IAbstractBaseLayerOperation operation) throws GIOException {
		for (var group : groups.values())
			for (var layer : group.getLayers().values())
				if (filter.test(layer))
					layer.process(operation);
	}
	
	/** @return true if the specified layer is declared within this container */
	public boolean hasLayer(PredefinedLayers<?> layer) {
		return groups.containsKey(layer.getGroup()) ? groups.get(layer.getGroup()).hasLayer(layer.getName()) : false;
	}

	/** @return true if the specified layer is declared within this container */
	public boolean hasLayer(String group, String name) {
		return groups.containsKey(group) ? groups.get(group).hasLayer(name) : false;
	}

	/** @return the layer corresponding to the PredefinedLayer */
	public <U extends ILoadableLayer<?>> U getLayer(PredefinedLayers<U> layer) throws GIOException {
		return getGroup(layer.getGroup()).getLayer(layer.getName());
	}

	/** @return an optional which contains the specified layer or is empty if it does not exist **/
	public <U extends ILoadableLayer<?>> Optional<U> getOptionalLayer(PredefinedLayers<U> layer) throws GIOException {
		return hasLayer(layer) ? Optional.of(getLayer(layer)) : Optional.empty();
	}

	/** @return a layer from a specified group */
	public <U extends ILoadableLayer<?>> U getLayer(String group, String name) throws GIOException {
		return getGroup(group).getLayer(name);
	}

	/**
	 * Creates and adds a new layer in the specified group, if the group does not exist: it's created
	 *
	 * @param groupName group's name
	 * @param layer the layer to add
	 * @throws GIOException
	 */
	public void declareLayer(String groupName, ILoadableLayer<?> layer) throws GIOException {
		getOrCreateGroup(groupName).addLayer(layer);
		// logger.info("[{}] new layer declared: {}", groupName, layer.getName());
	}

	/**
	 * Creates and adds a new layer in the specified group, if the group does not exist: it's created
	 *
	 * @param groupName group's name
	 * @param layer the layer to add
	 * @throws GIOException
	 */
	public void declareLayer(String groupName, IVlenLoadableLayer layer) throws GIOException {
		getOrCreateGroup(groupName).addLayer(layer);
	}

	/**
	 * Declares a predefined layer (generics force to specify a predefined layer and a loader associated with the same
	 * buffer type)
	 *
	 * @param predefinedLayer layer structure already known
	 * @param loader manager used to handle the layer (reads / writes data...)
	 * @throws GIOException
	 */
	public <U extends AbstractBuffer, V extends ILoadableLayer<U>> void declareLayer(
			PredefinedLayers<V> predefinedLayer, ILayerLoader<U> loader) throws GIOException {
		V layer = predefinedLayer.buildLayer(loader);
		getOrCreateGroup(predefinedLayer.getGroup()).addLayer(layer);
		// logger.info("[{}] predefined layer declared: {} (native variable: {})", predefinedLayer.getGroup(),
		// predefinedLayer.getName(), predefinedLayer.getName());
	}

	/** Flush all the container */
	public void flush() throws GIOException {
		for (Group g : groups.values())
			g.flush();
	}

	/**
	 * @return the count of layer managed by the data conatainer.
	 */
	public int getLayerCount() {
		return groups.values().stream().mapToInt(group -> group.getLayers().size()).sum();
	}
}
