package fr.ifremer.globe.core.runtime.datacontainer;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerBuilder;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IBaseBuffer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Defines a list of predefined layers, which can be used to declare or get layers from a {@link SounderDataContainer}.
 *
 * To avoid "rawtypes" warning: PredefinedLayers<T extends AbstractBuffer, U extends ILoadableLayer<T>>... but this
 * evolution needs to redefine predefined layer, and could affect the code readability
 */
public class PredefinedLayers<T extends ILoadableLayer<? extends IBaseBuffer>> {

	/** Properties **/
	private final String group;
	private final String name;
	private final String displayedName;
	private final String unit;
	private final DataKind dataType;
	public final ILayerBuilder builder;

	/** Constructor: set properties */
	public PredefinedLayers(String grp, String name, String dispName, String unit, DataKind dataType,
			ILayerBuilder builder) {
		this.group = grp;
		this.name = name;
		this.displayedName = dispName;
		this.unit = unit;
		this.dataType = dataType;
		this.builder = builder;
	}

	/** Constructor without unit **/
	public PredefinedLayers(String grp, String name, String dispName, DataKind dataType, ILayerBuilder builder) {
		this(grp, name, dispName, "", dataType, builder);
	}

	/** Constructor: creates the same layer in another group **/
	public PredefinedLayers(String grp, PredefinedLayers<T> origin) {
		this(grp, origin.name, origin.displayedName, origin.unit, origin.dataType, origin.builder);
	}

	/** name getter **/
	public String getName() {
		return name;
	}

	/**
	 * compute and return the complete path in netcdf for this Predefined Layer
	 */
	public String getPath() {
		return group.endsWith("/") ? group + name : group + "/" + name;
	}

	/** group getter **/
	public String getGroup() {
		return group;
	}

	/**
	 * Creates a layer from the selected predefined layer enumerator and a {@link ILayerManager}.
	 */
	public T buildLayer(ILayerLoader<? extends AbstractBuffer> l) throws GIOException {
		return (T) builder.build(name, displayedName, unit, l);
	}

	/** Getter of {@link #displayedName} */
	public String getDisplayedName() {
		return displayedName;
	}

	/**
	 * @return the {@link #unit}
	 */
	public String getUnit() {
		return unit;
	}
}
