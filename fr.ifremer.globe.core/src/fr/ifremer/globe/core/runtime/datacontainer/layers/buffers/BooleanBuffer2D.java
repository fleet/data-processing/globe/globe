package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class BooleanBuffer2D extends AbstractBuffer {

	public BooleanBuffer2D(int xSize, int ySize, String name) throws GIOException {
		super(boolean.class, Byte.BYTES, xSize, ySize, name);
	}

	/** Constructor used for copy **/
	private BooleanBuffer2D(BooleanBuffer2D source) throws IOException {
		super(source);
	}

	@Override
	public BooleanBuffer2D copy() throws IOException {
		return new BooleanBuffer2D(this);
	}

	public boolean getValue(int row, int col) {
		return backedBuffer.getBoolean(row, col);
	}

	public void setValue(int row, int col, boolean value) {
		setDirty(true);
		backedBuffer.putBoolean(row, col, value);
	}

}
