package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility class for LayerManagement
 * 
 * */
public class LayerUtility {
	/**
	 * Utility class
	 * */
	private LayerUtility() {
	}

	/**
	 * Allocate a layer, and prevent final users to see allocation methods 
	 * @throws GIOException 
	 * */
	public static void allocate( AbstractBaseLayer<? extends AbstractBuffer>  layer, long[] dims) throws GIOException
	{
		layer.allocateBuffer(dims);
	}
}
