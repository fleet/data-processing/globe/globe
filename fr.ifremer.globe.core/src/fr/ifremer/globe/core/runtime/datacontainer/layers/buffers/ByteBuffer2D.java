package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class ByteBuffer2D extends AbstractBuffer {

	public ByteBuffer2D(int xSize, int ySize, String name) throws GIOException {
		super(byte.class, Byte.BYTES, xSize, ySize, name);
	}

	/** Constructor used for copy **/
	private ByteBuffer2D(ByteBuffer2D source) throws IOException {
		super(source);
	}

	@Override
	public ByteBuffer2D copy() throws IOException {
		return new ByteBuffer2D(this);
	}

	public byte get(int row, int col) {
		return backedBuffer.getByte(row, col);
	}

	public void set(int row, int col, byte value) {
		setDirty(true);
		backedBuffer.putByte(row, col, value);
	}

}
