/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.netcdf.api.NetcdfVariable;

/**
 * Provides specific predefined layers
 */
public interface IPredefinedLayerProvider {

	/**
	 * Provides the predefined layers for the specified variable
	 * 
	 * @return the instance of PredefinedLayers or empty optional if none
	 */
	Optional<PredefinedLayers> providePredefinedLayer(NetcdfVariable variable);

}
