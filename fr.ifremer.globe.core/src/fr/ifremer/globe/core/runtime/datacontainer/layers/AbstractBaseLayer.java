package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Base class for a layer in datamodel
 */
public abstract class AbstractBaseLayer<T extends AbstractBuffer> implements IBaseLayer {

	/** Value of empty dimension */
	public static final long[] EMPTY_DIMENSIONS = new long[0];

	/** Properties **/
	protected String name;
	private String longName;
	private String unit;
	protected T buffer;
	protected long[] dimensions;

	/** Buffer builder interfaces **/

	interface BufferBuilder<T> {
		T build(long[] dims) throws GIOException;
	}

	interface IBuffer1DBuilder<T> {
		T build(int xSize, String name) throws GIOException;
	}

	interface IBuffer2DBuilder<T> {
		T build(int xSize, int ySize, String name) throws GIOException;
	}

	interface IBuffer3DBuilder<T> {
		T build(int xSize, int ySize, int zSize, String name) throws GIOException;
	}

	private final BufferBuilder<T> bufferBuilder;

	/** Constructors **/

	private AbstractBaseLayer(String name, String longName, String unit, BufferBuilder<T> bufferBuilder) {
		this.name = name;
		this.longName = longName;
		this.unit = unit;
		this.dimensions = EMPTY_DIMENSIONS;
		this.bufferBuilder = bufferBuilder;
	}

	protected AbstractBaseLayer(String name, String longName, String unit, IBuffer1DBuilder<T> buffer1DBuilder) {
		this(name, longName, unit, dims -> {
			if (dims == null || dims.length != 1)
				throw new GIOException("Invalid dimension (layer: " + name + ")");
			return buffer1DBuilder.build((int) dims[0], name);
		});
	}

	protected AbstractBaseLayer(String name, String longName, String unit, IBuffer2DBuilder<T> buffer2DBuilder) {
		this(name, longName, unit, dims -> {
			if (dims == null || dims.length != 2)
				throw new GIOException("Invalid dimension (layer: " + name + ")");
			return buffer2DBuilder.build((int) dims[0], (int) dims[1], name);
		});
	}

	protected AbstractBaseLayer(String name, String longName, String unit, IBuffer3DBuilder<T> buffer3DBuilder) {
		this(name, longName, unit, dims -> {
			if (dims == null || dims.length != 3)
				throw new GIOException("Invalid dimension (layer: " + name + ")");
			return buffer3DBuilder.build((int) dims[0], (int) dims[1], (int) dims[2], name);
		});
	}

	/** Getters **/

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getLongName() {
		return longName;
	}

	@Override
	public String getUnit() {
		return unit;
	}

	public T getData() {
		return buffer;
	}

	/** Allocate a buffer (buffer size depends of the layer dimensions and data type) */
	protected void allocateBuffer(long[] dims) throws GIOException {
		buffer = bufferBuilder.build(dims);
		setDimensions(dims);
	}

	/**
	 * Release the buffer (deletes the temporary file)
	 */
	public void dispose() {
		if (buffer != null)
			buffer.dispose();
	}

	@Override
	public String toString() {
		return name + " (class: " + getClass().getSimpleName() + "; unit: " + unit + "; size: "
				+ ((buffer != null && buffer.getLargeArray().getByteBuffer(0) != null)
						? buffer.getLargeArray().getByteBuffer(0).limit() + " bytes)"
						: "buffer not allocated)");
	}

	/** Perform the operation on this instance of layer */
	public abstract void process(IAbstractBaseLayerOperation operation) throws GIOException;

	/** Getter of {@link #dimension} */
	@Override
	public long[] getDimensions() {
		return dimensions;
	}

	/** Setter of {@link #dimension} */
	protected void setDimensions(long[] dims) {
		this.dimensions = dims;
	}

}
