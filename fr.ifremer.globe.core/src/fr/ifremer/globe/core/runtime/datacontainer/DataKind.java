package fr.ifremer.globe.core.runtime.datacontainer;

public enum DataKind {
	continuous, discrete
}
