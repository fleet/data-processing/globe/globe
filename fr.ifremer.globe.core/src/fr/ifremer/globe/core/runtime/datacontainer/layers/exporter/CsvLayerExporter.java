/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer.layers.exporter;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.apache.commons.io.IOUtils;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.ToStringOperation;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.ILayersConsumer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Specific ILayerConsumer to exporter layers in CSV file format.
 */
public class CsvLayerExporter<I extends IDataContainerInfo, C extends DataContainer<I>>
		implements ILayersConsumer<I, C>, Closeable {

	/** Csv file */
	protected File outputFile;
	/** True to write a preliminary header */
	protected boolean withHeader = true;
	/** Character used to separate values */
	protected char columnSeparator = ';';
	/** Opened file writer */
	protected BufferedWriter writer;
	/** Buffer used to optimize the String creation */
	protected StringBuilder outputStringBuilder;
	/** Layer visitor to serialize value as string */
	protected ToStringOperation toStringOperation;
	/** Buffer used to write in the BufferedWriter */
	protected char[] buffer;

	/** Constructor */
	public CsvLayerExporter(File outputFile, ToStringOperation toStringOperation) {
		this.outputFile = outputFile;
		outputStringBuilder = toStringOperation.getOutputStringBuilder();
		this.toStringOperation = toStringOperation;
	}

	/** Constructor */
	public CsvLayerExporter(File outputFile) {
		this.outputFile = outputFile;
		outputStringBuilder = new StringBuilder();
		toStringOperation = new ToStringOperation(outputStringBuilder);
	}

	/** {@inheritDoc} */
	@Override
	public void open() throws GIOException {
		try {
			writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE,
					StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			throw GIOException.wrap("Error during CSV opening : " + e.getMessage(), e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public <T extends AbstractBaseLayer<?>> void getReady(C dataContainer, List<T> layers) throws GIOException {
		if (withHeader && !layers.isEmpty()) {
			for (T layer : layers) {
				outputStringBuilder.append(getHeader(layer));
				outputStringBuilder.append(columnSeparator);
			}
			outputStringBuilder.deleteCharAt(outputStringBuilder.length() - 1);
			try {
				writer.append(outputStringBuilder);
				writer.newLine();
			} catch (IOException e) {
				throw GIOException.wrap("Error during CSV writing : " + e.getMessage(), e);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void initiateLevel(int levelIndex, int[] indexes) throws GIOException {
		outputStringBuilder.setLength(0);
	}

	/** {@inheritDoc} */
	@Override
	public <T extends AbstractBaseLayer<?>> void processLayer(int[] indexes, T layer) throws GIOException {
		toStringOperation.setIndexes(indexes);
		layer.process(toStringOperation);
		outputStringBuilder.append(columnSeparator);
	}

	/** {@inheritDoc} */
	@Override
	public void endProcessLayers() throws GIOException {
		if (outputStringBuilder.length() > 0) {
			try {
				// Much more efficient than writer.append(outputStringBuilder)
				loadBuffer();
				writer.write(buffer, 0, outputStringBuilder.length() - 1); // - 1 to delete last column separator
				writer.newLine();
			} catch (IOException e) {
				throw GIOException.wrap("Error during CSV writing : " + e.getMessage(), e);
			}
			outputStringBuilder.setLength(0);
		}
	}

	/** Load characters from the StringBuilder into the buffer */
	protected void loadBuffer() {
		if (buffer == null || buffer.length < outputStringBuilder.length()) {
			buffer = new char[outputStringBuilder.length()];
		}
		outputStringBuilder.getChars(0, outputStringBuilder.length(), buffer, 0);
	}

	/** {@inheritDoc} */
	@Override
	public void close() {
		buffer = null;
		IOUtils.closeQuietly(writer);
	}

	/** @return the header for the specified layer */
	public <T extends AbstractBaseLayer<?>> String getHeader(T layer) {
		return layer.getLongName();
	}

	/** Getter of {@link #withHeader} */
	public boolean isWithHeader() {
		return withHeader;
	}

	/** Setter of {@link #withHeader} */
	public void setWithHeader(boolean withHeader) {
		this.withHeader = withHeader;
	}

	/** Getter of {@link #columnSeparator} */
	public char getColumnSeparator() {
		return columnSeparator;
	}

	/** Setter of {@link #columnSeparator} */
	public void setColumnSeparator(char columnSeparator) {
		this.columnSeparator = columnSeparator;
	}

	/** Getter of {@link #toStringOperation} */
	public ToStringOperation getToStringOperation() {
		return toStringOperation;
	}

	/** Getter of {@link #outputFile} */
	public File getOutputFile() {
		return outputFile;
	}
}
