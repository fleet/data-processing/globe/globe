package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ByteBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class ByteLayer2D extends AbstractBaseLayer<ByteBuffer2D> {

	public ByteLayer2D(String name, String longName, String unit) {
		super(name, longName, unit, ByteBuffer2D::new);
	}

	public byte get(int row, int col) {
		return buffer.get(row, col);
	}

	public void set(int row, int col, byte value) {
		buffer.set(row, col, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
