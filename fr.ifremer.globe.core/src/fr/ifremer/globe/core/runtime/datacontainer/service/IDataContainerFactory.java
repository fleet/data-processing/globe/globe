package fr.ifremer.globe.core.runtime.datacontainer.service;

import java.util.List;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * This interface provides methods to book {@link DataContainer} & {@link SounderDataContainer}.
 */
public interface IDataContainerFactory {

	/**
	 * Returns the service implementation of IDataContainerFactory.
	 */
	static IDataContainerFactory grab() {
		return OsgiUtils.getService(IDataContainerFactory.class);
	}

	/**
	 * Books a container associated to the given {@link IDataContainerInfo}, if no one exist, creates a new one.
	 * 
	 * @param owner owner of the Container. Used to check the caller rights on the expected Container.
	 * @param info {@link IDataContainerInfo} associated to the container
	 */
	<U extends IDataContainerInfo> IDataContainerToken<U> book(U info, IDataContainerOwner owner) throws GIOException;

	/**
	 * Books a container associated to the given {@link ISounderNcInfo}, if no one exist, creates a new one.
	 * 
	 * @param owner owner of the Container. Used to check the caller rights on the expected Container.
	 * @param info {@link IDataContainerInfo} associated to the container
	 */
	ISounderDataContainerToken book(ISounderNcInfo info, IDataContainerOwner owner) throws GIOException;

	/**
	 * Books all the container associated to the given {@link IDataContainerInfo}.
	 */
	<U extends IDataContainerInfo> List<IDataContainerToken<U>> book(List<U> infoStores, IDataContainerOwner owner)
			throws GIOException;

	/**
	 * Books all the container associated to the given {@link ISounderNcInfo}.
	 */
	List<ISounderDataContainerToken> bookSounderDataContainers(List<ISounderNcInfo> infoStores,
			IDataContainerOwner owner) throws GIOException;

	/**
	 * @return true if the specified {@link IDataContainerInfo} is booked.
	 */
	boolean isBooked(IDataContainerInfo infoStore);

	/**
	 * @return true if at least one {@link IDataContainerInfo} of the list is booked.
	 */
	boolean isBooked(List<? extends IDataContainerInfo> infoStores);

}