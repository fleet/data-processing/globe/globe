package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ByteBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class ByteLoadableLayer2D extends ByteLayer2D implements ILoadableLayer<ByteBuffer2D> {

	private boolean isLoaded;

	public ByteLoadableLayer2D(String name, String longName, String unit, ILayerLoader<ByteBuffer2D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<ByteBuffer2D> loader;

	@Override
	public ILayerLoader<ByteBuffer2D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
