package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleLayer2D extends AbstractBaseLayer<DoubleBuffer2D> {

	public DoubleLayer2D(String name, String longName, String unit) {
		super(name, longName, unit, DoubleBuffer2D::new);
	}

	public double get(int row, int col) {
		return buffer.get(row, col);
	}

	public void set(int row, int col, double value) {
		buffer.set(row, col, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
