package fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.position.position_sub_group;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;

/** Class generated by PredefinedLayersGenerator **/
public class NmeaLayers {

	private NmeaLayers() {}

	public static final String GROUP = "/Platform/Position/position_sub_group/NMEA";

	/** Layer: NMEA_DATAGRAM **/
	public static final String NMEA_DATAGRAM_VARIABLE_NAME ="NMEA_datagram";
	//public static final PredefinedLayers<StringLoadableLayer1D> NMEA_DATAGRAM = new PredefinedLayers<>(GROUP,"NMEA_datagram","NMEA datagram","", DataKind.continuous, StringLoadableLayer1D::new);

	/** Layer: TIME **/
	public static final String TIME_VARIABLE_NAME ="time";
	public static final PredefinedLayers<LongLoadableLayer1D> TIME = new PredefinedLayers<>(GROUP,"time","Timestamps for NMEA datagrams","nanoseconds since 1970-01-01 00:00:00Z", DataKind.continuous, LongLoadableLayer1D::new);
}
