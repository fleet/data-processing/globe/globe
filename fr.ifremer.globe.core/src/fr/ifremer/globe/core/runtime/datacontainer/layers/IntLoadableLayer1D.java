package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IntBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class IntLoadableLayer1D extends IntLayer1D implements ILoadableLayer<IntBuffer1D> {

	private boolean isLoaded;

	public IntLoadableLayer1D(String name, String longName, String unit, ILayerLoader<IntBuffer1D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<IntBuffer1D> loader;

	@Override
	public ILayerLoader<IntBuffer1D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
