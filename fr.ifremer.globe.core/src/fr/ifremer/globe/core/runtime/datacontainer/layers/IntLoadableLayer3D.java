package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IntBuffer3D;
import fr.ifremer.globe.utils.exception.GIOException;

public class IntLoadableLayer3D extends IntLayer3D implements ILoadableLayer<IntBuffer3D> {

	private boolean isLoaded;

	public IntLoadableLayer3D(String name, String longName, String unit, ILayerLoader<IntBuffer3D> l)
			throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<IntBuffer3D> loader;

	@Override
	public ILayerLoader<IntBuffer3D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
