package fr.ifremer.globe.core.runtime.datacontainer;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.datacontainer.exception.LayerNotFoundException;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IVlenLoadableLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Sounder data container group: contains a list of layers
 */
public class Group {
	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(Group.class);

	/** Properties **/
	private String name;
	private Map<String, ILoadableLayer<?>> layers;
	private Map<String, IVlenLoadableLayer> vlenLayers;

	/**
	 * Protected constructor: group are instantiated only when layers are declared
	 **/
	protected Group(String name) {
		this.name = name;
		layers = new HashMap<>();
		vlenLayers = new HashMap<>();
	}

	/** Name getter **/
	public String getName() {
		return name;
	}

	/** Layer list getter **/
	public Map<String, ILoadableLayer<?>> getLayers() {
		return layers;
	}

	/** @return true if the layer exists in this group */
	public boolean hasLayer(String name) {
		return layers.containsKey(name);
	}

	/** @return true if the layer exists in this group */
	public <T extends ILoadableLayer<?>> boolean hasLayer(PredefinedLayers<T> layer) {
		return hasLayer(layer.getName());
	}

	/**
	 * @return a specific layer, and load it if needed
	 * @throws GIOException if layer not declared in the group
	 */
	@SuppressWarnings("unchecked")
	public <T extends ILoadableLayer<?>> T getLayer(String name) throws GIOException, LayerNotFoundException {
		T layer = (T) layers.get(name);

		if (layer == null) {
			throw new LayerNotFoundException(
					"Invalid layer access: layer " + name + " not declared in the group " + this.name);
		}

		if (!layer.isLoaded()) {
			layer.load();
		}

		return layer;
	}

	/**
	 * @return a predefined layer, and load it if needed
	 * @throws GIOException if layer not declared in the group
	 */
	public <T extends ILoadableLayer<?>> T getLayer(PredefinedLayers<T> layer) throws GIOException {
		return getLayer(layer.getName());
	}

	/**
	 * list of declared vlen layers names
	 */
	public Map<String, IVlenLoadableLayer> getVlenLayers() {
		return vlenLayers;
	}

	@SuppressWarnings("unchecked")
	public <T extends IVlenLoadableLayer> T getVlenLayer(String name) throws GIOException, LayerNotFoundException {
		IVlenLoadableLayer layer = vlenLayers.get(name);
		if (layer == null) {
			throw new LayerNotFoundException(
					"Invalid layer access: layer " + name + " not declared in the group " + this.name);
		}
		if (!layer.isLoaded()) {
			layer.load();
		}

		return (T) layer;
	}

	/**
	 * Add a new layer
	 *
	 * @throws GIOException
	 **/
	protected void addLayer(ILoadableLayer<?> layer) throws GIOException {
		if (hasLayer(layer.getName())) {
			throw new GIOException(
					String.format("Group %s : can't add layer already present (%s).", getName(), layer.getName()));
		}
		layers.put(layer.getName(), layer);
	}

	/**
	 * Add a new vlen layer
	 *
	 * @throws GIOException
	 **/
	protected void addLayer(IVlenLoadableLayer layer) throws GIOException {
		if (hasLayer(layer.getName())) {
			throw new GIOException(
					String.format("Group %s : can't add layer already present (%s).", getName(), layer.getName()));
		}
		vlenLayers.put(layer.getName(), layer);
	}

	/** Launch a flush for each layer contained in this group **/
	protected void flush() throws GIOException {
		for (ILoadableLayer<?> l : layers.values()) {
			l.flush();
		}
	}

}
