package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ShortBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class ShortLayer2D extends AbstractBaseLayer<ShortBuffer2D> {

	public ShortLayer2D(String name, String longName, String unit) {
		super(name, longName, unit, ShortBuffer2D::new);
	}

	public short get(int row, int col) {
		return buffer.get(row, col);
	}

	public void set(int row, int col, short value) {
		buffer.set(row, col, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}
}
