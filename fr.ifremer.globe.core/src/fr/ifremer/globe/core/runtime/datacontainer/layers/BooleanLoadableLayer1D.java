package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.BooleanBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class BooleanLoadableLayer1D extends BooleanLayer1D implements ILoadableLayer<BooleanBuffer1D> {

	private boolean isLoaded;

	public BooleanLoadableLayer1D(String name, String longName, String unit, ILayerLoader<BooleanBuffer1D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<BooleanBuffer1D> loader;

	@Override
	public ILayerLoader<BooleanBuffer1D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
