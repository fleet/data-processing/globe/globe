package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.BooleanBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 **/
public class BooleanLayer1D extends AbstractBaseLayer<BooleanBuffer1D> {

	public BooleanLayer1D(String name, String longName, String unit) {
		super(name, longName, unit, BooleanBuffer1D::new);
	}

	public boolean get(int index) {
		return buffer.get(index);
	}

	public void set(int index, boolean value) {
		buffer.set(index, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}

}
