package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Interfaces for layer having to capacity to be loaded and flushed individually
 */
public interface ILoadableLayer<T extends AbstractBuffer> extends IBaseLayer {

	/**
	 * return the {@link ILayerLoader} instance associated with the layer
	 */
	public ILayerLoader<T> getLoader();

	public boolean isLoaded();

	public void setLoaded(boolean b);

	/**
	 * Flushes data from the buffer to the file
	 * 
	 * @throws GIOException
	 */
	public default void flush() throws GIOException {
		getLoader().flush(this);
	}

	/**
	 * Loads data from the file to the buffer
	 * 
	 * @throws GIOException
	 */
	public default void load() throws GIOException {
		getLoader().load(this);
	}

	/**
	 * @return the {@link AbstractBuffer}
	 */
	public T getData();

	/**
	 * Dispose associated buffer
	 */
	public void dispose();

	/** Perform the operation on this instance of layer */
	public abstract void process(IAbstractBaseLayerOperation operation) throws GIOException;

}