package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IntBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class IntLoadableLayer2D extends IntLayer2D implements ILoadableLayer<IntBuffer2D> {

	private boolean isLoaded;

	public IntLoadableLayer2D(String name, String longName, String unit, ILayerLoader<IntBuffer2D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<IntBuffer2D> loader;

	@Override
	public ILayerLoader<IntBuffer2D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
