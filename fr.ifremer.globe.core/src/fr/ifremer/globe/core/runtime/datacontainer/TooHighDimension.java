package fr.ifremer.globe.core.runtime.datacontainer;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Unsupported dimension size
 * */
@SuppressWarnings("serial")
public class TooHighDimension extends GIOException {
	public TooHighDimension(String paramString) {
		super(paramString);
	}

}
