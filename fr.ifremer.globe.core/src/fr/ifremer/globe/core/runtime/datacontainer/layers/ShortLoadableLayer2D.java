package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ShortBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class ShortLoadableLayer2D extends ShortLayer2D implements ILoadableLayer<ShortBuffer2D> {

	private boolean isLoaded;

	public ShortLoadableLayer2D(String name, String longName, String unit, ILayerLoader<ShortBuffer2D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<ShortBuffer2D> loader;

	@Override
	public ILayerLoader<ShortBuffer2D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
