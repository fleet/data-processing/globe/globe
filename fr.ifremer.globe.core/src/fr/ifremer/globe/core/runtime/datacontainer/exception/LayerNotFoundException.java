/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer.exception;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Exception raised when trying to access a layer that does not exist
 */
@SuppressWarnings("serial")
public class LayerNotFoundException extends GIOException {

	/**
	 * Constructor
	 */
	public LayerNotFoundException(String paramString) {
		super(paramString);
	}

	/**
	 * Constructor
	 */
	public LayerNotFoundException(String string, Throwable e) {
		super(string, e);
	}

}
