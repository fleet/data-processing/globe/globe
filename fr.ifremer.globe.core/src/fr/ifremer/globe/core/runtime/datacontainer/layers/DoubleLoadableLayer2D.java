package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class DoubleLoadableLayer2D extends DoubleLayer2D implements ILoadableLayer<DoubleBuffer2D> {

	private boolean isLoaded;

	public DoubleLoadableLayer2D(String name, String longName, String unit, ILayerLoader<DoubleBuffer2D> l) throws GIOException {
		super(name, longName, unit);
		loader = l;
		dimensions = loader.getDimensions();
	}

	ILayerLoader<DoubleBuffer2D> loader;

	@Override
	public ILayerLoader<DoubleBuffer2D> getLoader() {
		return loader;
	}

	@Override
	public boolean isLoaded() {
		return isLoaded;
	}

	@Override
	public void setLoaded(boolean b) {
		isLoaded = b;
	}

}
