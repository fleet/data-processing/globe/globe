package fr.ifremer.globe.core.runtime.datacontainer.layers;

import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.BooleanBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;

public class BooleanLayer2D extends AbstractBaseLayer<BooleanBuffer2D> {

	public BooleanLayer2D(String name, String longName, String unit) {
		super(name, longName, unit, BooleanBuffer2D::new);
	}

	public boolean getValue(int row, int col) {
		return buffer.getValue(row, col);
	}

	public void setValue(int row, int col, boolean value) {
		buffer.setValue(row, col, value);
	}

	/** {@inheritDoc} */
	@Override
	public void process(IAbstractBaseLayerOperation operation) throws GIOException {
		operation.visit(this);
	}

}
