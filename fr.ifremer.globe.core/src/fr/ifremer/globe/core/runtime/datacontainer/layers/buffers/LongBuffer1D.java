package fr.ifremer.globe.core.runtime.datacontainer.layers.buffers;

import java.io.IOException;

import fr.ifremer.globe.utils.exception.GIOException;

public class LongBuffer1D extends AbstractBuffer {

	public LongBuffer1D(int length, String name) throws GIOException {
		super(long.class, Long.BYTES, length, Long.BYTES, name);
	}

	/** Constructor used for copy **/
	private LongBuffer1D(LongBuffer1D source) throws IOException {
		super(source);
	}

	@Override
	public LongBuffer1D copy() throws IOException {
		return new LongBuffer1D(this);
	}

	public long get(int index) {
		return backedBuffer.getLong(index);
	}

	public void set(int index, long value) {
		setDirty(true);
		backedBuffer.putLong(index, value);
	}

}
