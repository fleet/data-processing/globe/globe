package fr.ifremer.globe.core.runtime.datacontainer.layers;

import java.util.stream.LongStream;

public interface IBaseLayer {

	/** name getter **/
	String getName();

	/** longName getter **/
	String getLongName();

	/** unit getter **/
	String getUnit();

	/** Getter of dimensions */
	long[] getDimensions();

	/** Getter of size **/
	default long getSize() {
		return LongStream.of(getDimensions()).reduce(Math::multiplyExact).orElse(0);
	}
}