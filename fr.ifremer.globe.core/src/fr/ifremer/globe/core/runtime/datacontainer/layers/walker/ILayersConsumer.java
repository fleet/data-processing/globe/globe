/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.datacontainer.layers.walker;

import java.io.Closeable;
import java.util.List;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Writer of Layer's values into a file.
 */
public interface ILayersConsumer<I extends IDataContainerInfo, C extends DataContainer<I>> extends Closeable {

	/** Open some resources if necessary */
	default void open() throws GIOException {
		// Do nothing by default
	}

	/** Advise which layers will be processed */
	default <T extends AbstractBaseLayer<?>> void getReady(C dataContainer, List<T> layers) throws GIOException {
		// Do nothing by default
	}

	/** Initiate a new level (may be 0 or 1 for 2D layers) */
	default void initiateLevel(int levelIndex, int[] indexes) throws GIOException {
		// Do nothing by default
	}

	/**
	 * Process the writing of the values for the specified AbstractBaseLayer. Considered Swath and Beam have been
	 * initiated previously
	 */
	default <T extends AbstractBaseLayer<?>> void processLayer(int[] indexes, T layer) throws GIOException {
		// Do nothing by default
	}

	/**
	 * Advise the end of the layer's process
	 */
	default void endProcessLayers() throws GIOException {
		// Do nothing by default
	}

	/** Advise the end of the level */
	default void endLevel(int levelIndex, int[] indexes) throws GIOException {
		// Do nothing by default
	}

	/** Finalize the end of writing of all values */
	default void conclude() throws GIOException {
		// Do nothing by default
	}

	/** {@inheritDoc} */
	@Override
	default void close() {
		// Do nothing by default
	}

}
