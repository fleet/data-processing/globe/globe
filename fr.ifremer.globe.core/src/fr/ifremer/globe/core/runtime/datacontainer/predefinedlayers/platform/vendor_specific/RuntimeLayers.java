package fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.vendor_specific;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLoadableLayer1D;

/** Class generated by PredefinedLayersGenerator **/
public class RuntimeLayers {

	private RuntimeLayers() {}

	public static final String GROUP = "/Platform/Vendor_specific/runtime";

	/** Layer: ABSORPTION_COEFFICIENT **/
	public static final String ABSORPTION_COEFFICIENT_VARIABLE_NAME ="absorption_coefficient";
	public static final PredefinedLayers<FloatLoadableLayer1D> ABSORPTION_COEFFICIENT = new PredefinedLayers<>(GROUP,"absorption_coefficient","Absorption Coefficient","", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: BEAM_SPACING **/
	public static final String BEAM_SPACING_VARIABLE_NAME ="beam_spacing";
	public static final PredefinedLayers<ShortLoadableLayer1D> BEAM_SPACING = new PredefinedLayers<>(GROUP,"beam_spacing","Beam Spacing","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: BSP_STATUS **/
	public static final String BSP_STATUS_VARIABLE_NAME ="bsp_status";
	public static final PredefinedLayers<ShortLoadableLayer1D> BSP_STATUS = new PredefinedLayers<>(GROUP,"bsp_status","BSP Status","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: DUAL_SWATH_MODE **/
	public static final String DUAL_SWATH_MODE_VARIABLE_NAME ="dual_swath_mode";
	public static final PredefinedLayers<ByteLoadableLayer1D> DUAL_SWATH_MODE = new PredefinedLayers<>(GROUP,"dual_swath_mode","Dual swath mode","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: DUROTONG_SPEED **/
	public static final String DUROTONG_SPEED_VARIABLE_NAME ="durotong_speed";
	public static final PredefinedLayers<DoubleLoadableLayer1D> DUROTONG_SPEED = new PredefinedLayers<>(GROUP,"durotong_speed","Durotong Speed","", DataKind.continuous, DoubleLoadableLayer1D::new);

	/** Layer: FILTER_IDENTIFIER **/
	public static final String FILTER_IDENTIFIER_VARIABLE_NAME ="filter_identifier";
	public static final PredefinedLayers<ShortLoadableLayer1D> FILTER_IDENTIFIER = new PredefinedLayers<>(GROUP,"filter_identifier","Filter Identifier","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: FILTER_IDENTIFIER2 **/
	public static final String FILTER_IDENTIFIER2_VARIABLE_NAME ="filter_identifier2";
	public static final PredefinedLayers<ShortLoadableLayer1D> FILTER_IDENTIFIER2 = new PredefinedLayers<>(GROUP,"filter_identifier2","Filter Identifier 2","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: FREQUENCY_MODE **/
	public static final String FREQUENCY_MODE_VARIABLE_NAME ="frequency_mode";
	public static final PredefinedLayers<IntLoadableLayer1D> FREQUENCY_MODE = new PredefinedLayers<>(GROUP,"frequency_mode","Frequency Mode","", DataKind.continuous, IntLoadableLayer1D::new);

	/** Layer: HILO_FREQUENCY_ABSORPTION_COEF **/
	public static final String HILO_FREQUENCY_ABSORPTION_COEF_VARIABLE_NAME ="hilo_frequency_absorption_coef";
	public static final PredefinedLayers<ShortLoadableLayer1D> HILO_FREQUENCY_ABSORPTION_COEF = new PredefinedLayers<>(GROUP,"hilo_frequency_absorption_coef","HiLo frequency absorption coefficient ratio","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: MAXIMUM_DEPTH **/
	public static final String MAXIMUM_DEPTH_VARIABLE_NAME ="maximum_depth";
	public static final PredefinedLayers<IntLoadableLayer1D> MAXIMUM_DEPTH = new PredefinedLayers<>(GROUP,"maximum_depth","Maximum Depth","", DataKind.continuous, IntLoadableLayer1D::new);

	/** Layer: MAXIMUM_PORT_COVERAGE **/
	public static final String MAXIMUM_PORT_COVERAGE_VARIABLE_NAME ="maximum_port_coverage";
	public static final PredefinedLayers<ShortLoadableLayer1D> MAXIMUM_PORT_COVERAGE = new PredefinedLayers<>(GROUP,"maximum_port_coverage","Maximum Port Coverage","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: MAXIMUM_PORT_SWATH_WIDTH **/
	public static final String MAXIMUM_PORT_SWATH_WIDTH_VARIABLE_NAME ="maximum_port_swath_width";
	public static final PredefinedLayers<IntLoadableLayer1D> MAXIMUM_PORT_SWATH_WIDTH = new PredefinedLayers<>(GROUP,"maximum_port_swath_width","Maximum port swath width","", DataKind.continuous, IntLoadableLayer1D::new);

	/** Layer: MAXIMUM_STARBOARD_COVERAGE **/
	public static final String MAXIMUM_STARBOARD_COVERAGE_VARIABLE_NAME ="maximum_starboard_coverage";
	public static final PredefinedLayers<ShortLoadableLayer1D> MAXIMUM_STARBOARD_COVERAGE = new PredefinedLayers<>(GROUP,"maximum_starboard_coverage","Maximum Starboard Coverage","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: MAXIMUM_STARBOARD_SWATH_WIDTH **/
	public static final String MAXIMUM_STARBOARD_SWATH_WIDTH_VARIABLE_NAME ="maximum_starboard_swath_width";
	public static final PredefinedLayers<IntLoadableLayer1D> MAXIMUM_STARBOARD_SWATH_WIDTH = new PredefinedLayers<>(GROUP,"maximum_starboard_swath_width","Maximum Starboard Swath","", DataKind.continuous, IntLoadableLayer1D::new);

	/** Layer: MINIMUM_DEPTH **/
	public static final String MINIMUM_DEPTH_VARIABLE_NAME ="minimum_depth";
	public static final PredefinedLayers<IntLoadableLayer1D> MINIMUM_DEPTH = new PredefinedLayers<>(GROUP,"minimum_depth","Minimum Depth","", DataKind.continuous, IntLoadableLayer1D::new);

	/** Layer: MODE **/
	public static final String MODE_VARIABLE_NAME ="mode";
	public static final PredefinedLayers<ShortLoadableLayer1D> MODE = new PredefinedLayers<>(GROUP,"mode","Mode","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: MODE2 **/
	public static final String MODE2_VARIABLE_NAME ="mode2";
	public static final PredefinedLayers<ShortLoadableLayer1D> MODE2 = new PredefinedLayers<>(GROUP,"mode2","Mode 2","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: OPERATOR_STATION_STATUS **/
	public static final String OPERATOR_STATION_STATUS_VARIABLE_NAME ="operator_station_status";
	public static final PredefinedLayers<ShortLoadableLayer1D> OPERATOR_STATION_STATUS = new PredefinedLayers<>(GROUP,"operator_station_status","Operator Station Status","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: PING_MODE **/
	public static final String PING_MODE_VARIABLE_NAME ="ping_mode";
	public static final PredefinedLayers<ByteLoadableLayer1D> PING_MODE = new PredefinedLayers<>(GROUP,"ping_mode","Mode","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: PROCESSING_UNIT_STATUS **/
	public static final String PROCESSING_UNIT_STATUS_VARIABLE_NAME ="processing_unit_status";
	public static final PredefinedLayers<ShortLoadableLayer1D> PROCESSING_UNIT_STATUS = new PredefinedLayers<>(GROUP,"processing_unit_status","Processing Unit Status","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: PULSE_LENGTH_MODE **/
	public static final String PULSE_LENGTH_MODE_VARIABLE_NAME ="pulse_length_mode";
	public static final PredefinedLayers<ByteLoadableLayer1D> PULSE_LENGTH_MODE = new PredefinedLayers<>(GROUP,"pulse_length_mode","Pulse length Mode (mode2)","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: RECEIVER_BANDWITH **/
	public static final String RECEIVER_BANDWITH_VARIABLE_NAME ="receiver_bandwith";
	public static final PredefinedLayers<FloatLoadableLayer1D> RECEIVER_BANDWITH = new PredefinedLayers<>(GROUP,"receiver_bandwith","Receiver Bandwidth","", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: RECEIVER_BEAMWIDTH **/
	public static final String RECEIVER_BEAMWIDTH_VARIABLE_NAME ="receiver_beamwidth";
	public static final PredefinedLayers<FloatLoadableLayer1D> RECEIVER_BEAMWIDTH = new PredefinedLayers<>(GROUP,"receiver_beamwidth","Receiver Beamwidth","", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: RECEIVER_FIXED_GAIN **/
	public static final String RECEIVER_FIXED_GAIN_VARIABLE_NAME ="receiver_fixed_gain";
	public static final PredefinedLayers<ShortLoadableLayer1D> RECEIVER_FIXED_GAIN = new PredefinedLayers<>(GROUP,"receiver_fixed_gain","Receiver fixed gain setting","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: RUNTIME_RAW_COUNT **/
	public static final String RUNTIME_RAW_COUNT_VARIABLE_NAME ="runtime_raw_count";
	public static final PredefinedLayers<IntLoadableLayer1D> RUNTIME_RAW_COUNT = new PredefinedLayers<>(GROUP,"runtime_raw_count","Number of runtime datagrams","", DataKind.continuous, IntLoadableLayer1D::new);

	/** Layer: RUNTIME_TXT **/
	public static final String RUNTIME_TXT_VARIABLE_NAME ="runtime_txt";
	//public static final PredefinedLayers<StringLoadableLayer1D> RUNTIME_TXT = new PredefinedLayers<>(GROUP,"runtime_txt","runtime txt","", DataKind.continuous, StringLoadableLayer1D::new);

	/** Layer: SONAR_HEAD_OR_TRANSCEIVER_STATUS **/
	public static final String SONAR_HEAD_OR_TRANSCEIVER_STATUS_VARIABLE_NAME ="sonar_head_or_transceiver_status";
	public static final PredefinedLayers<ShortLoadableLayer1D> SONAR_HEAD_OR_TRANSCEIVER_STATUS = new PredefinedLayers<>(GROUP,"sonar_head_or_transceiver_status","SonarHead or Transceiver Status","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: SOURCE_SOUND_SPEED_TRANSDUCER **/
	public static final String SOURCE_SOUND_SPEED_TRANSDUCER_VARIABLE_NAME ="source_sound_speed_transducer";
	public static final PredefinedLayers<ShortLoadableLayer1D> SOURCE_SOUND_SPEED_TRANSDUCER = new PredefinedLayers<>(GROUP,"source_sound_speed_transducer","Source of sound speed at transducer","", DataKind.continuous, ShortLoadableLayer1D::new);

	/** Layer: TIME **/
	public static final String TIME_VARIABLE_NAME ="time";
	public static final PredefinedLayers<LongLoadableLayer1D> TIME = new PredefinedLayers<>(GROUP,"time","Time-stamp of each ping","nanoseconds since 1970-01-01 00:00:00Z", DataKind.continuous, LongLoadableLayer1D::new);

	/** Layer: TVG_LAW_CROSSOVER_ANGLE **/
	public static final String TVG_LAW_CROSSOVER_ANGLE_VARIABLE_NAME ="TVG_law_crossover_angle";
	public static final PredefinedLayers<FloatLoadableLayer1D> TVG_LAW_CROSSOVER_ANGLE = new PredefinedLayers<>(GROUP,"TVG_law_crossover_angle","TVG law crossover angle in degrees","", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: TX_ALONG_TILT **/
	public static final String TX_ALONG_TILT_VARIABLE_NAME ="tx_along_tilt";
	public static final PredefinedLayers<FloatLoadableLayer1D> TX_ALONG_TILT = new PredefinedLayers<>(GROUP,"tx_along_tilt","Transmit Along Tilt","", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: TX_BEAMWIDTH **/
	public static final String TX_BEAMWIDTH_VARIABLE_NAME ="tx_beamwidth";
	public static final PredefinedLayers<FloatLoadableLayer1D> TX_BEAMWIDTH = new PredefinedLayers<>(GROUP,"tx_beamwidth","Transmit Beamwidth","", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: TX_POWER_RE_MAXIMUM **/
	public static final String TX_POWER_RE_MAXIMUM_VARIABLE_NAME ="tx_power_re_maximum";
	public static final PredefinedLayers<ByteLoadableLayer1D> TX_POWER_RE_MAXIMUM = new PredefinedLayers<>(GROUP,"tx_power_re_maximum","Transmit power re maximum","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: TX_PULSE_LENGTH **/
	public static final String TX_PULSE_LENGTH_VARIABLE_NAME ="tx_pulse_length";
	public static final PredefinedLayers<DoubleLoadableLayer1D> TX_PULSE_LENGTH = new PredefinedLayers<>(GROUP,"tx_pulse_length","Transmit Pulse Length","", DataKind.continuous, DoubleLoadableLayer1D::new);

	/** Layer: YAW_PITCH_STABILIZATION_MODE **/
	public static final String YAW_PITCH_STABILIZATION_MODE_VARIABLE_NAME ="yaw_pitch_stabilization_mode";
	public static final PredefinedLayers<ShortLoadableLayer1D> YAW_PITCH_STABILIZATION_MODE = new PredefinedLayers<>(GROUP,"yaw_pitch_stabilization_mode","Yaw and pitch stabilization mode","", DataKind.continuous, ShortLoadableLayer1D::new);
}
