package fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.attitude.attitude_sub_group.vendor_specific;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;

/** Class generated by PredefinedLayersGenerator **/
public class NetworkAttitudeVelocity110Layers {

	private NetworkAttitudeVelocity110Layers() {}

	public static final String GROUP = "/Platform/Attitude/attitude_sub_group/Vendor_specific/network_attitude_velocity_110";

	/** Layer: ATTITUDE_RAW_COUNT **/
	public static final String ATTITUDE_RAW_COUNT_VARIABLE_NAME ="attitude_raw_count";
	public static final PredefinedLayers<IntLoadableLayer1D> ATTITUDE_RAW_COUNT = new PredefinedLayers<>(GROUP,"attitude_raw_count","Number of position datagrams","", DataKind.continuous, IntLoadableLayer1D::new);

	/** Layer: DATAGRAM_TIME **/
	public static final String DATAGRAM_TIME_VARIABLE_NAME ="datagram_time";
	public static final PredefinedLayers<LongLoadableLayer1D> DATAGRAM_TIME = new PredefinedLayers<>(GROUP,"datagram_time","Timestamps for  datagram","nanoseconds since 1970-01-01 00:00:00Z", DataKind.continuous, LongLoadableLayer1D::new);

	/** Layer: HEADING **/
	public static final String HEADING_VARIABLE_NAME ="heading";
	public static final PredefinedLayers<FloatLoadableLayer1D> HEADING = new PredefinedLayers<>(GROUP,"heading","heading angle","degree", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: HEAVE **/
	public static final String HEAVE_VARIABLE_NAME ="heave";
	public static final PredefinedLayers<FloatLoadableLayer1D> HEAVE = new PredefinedLayers<>(GROUP,"heave","Platform vertical offset from nominal (heave)","m", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: PITCH **/
	public static final String PITCH_VARIABLE_NAME ="pitch";
	public static final PredefinedLayers<FloatLoadableLayer1D> PITCH = new PredefinedLayers<>(GROUP,"pitch","pitch angle","degree", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: ROLL **/
	public static final String ROLL_VARIABLE_NAME ="roll";
	public static final PredefinedLayers<FloatLoadableLayer1D> ROLL = new PredefinedLayers<>(GROUP,"roll","roll angle","degree", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: SENSOR_STATUS **/
	public static final String SENSOR_STATUS_VARIABLE_NAME ="sensor_status";
	public static final PredefinedLayers<ByteLoadableLayer1D> SENSOR_STATUS = new PredefinedLayers<>(GROUP,"sensor_status","sensor system descriptor showing which sensor the data is derived from, see .all format specification","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: SYSTEM_SERIAL_NUMBER **/
	public static final String SYSTEM_SERIAL_NUMBER_VARIABLE_NAME ="system_serial_number";
	public static final PredefinedLayers<IntLoadableLayer1D> SYSTEM_SERIAL_NUMBER = new PredefinedLayers<>(GROUP,"system_serial_number","System Serial Number","", DataKind.continuous, IntLoadableLayer1D::new);
}
