package fr.ifremer.globe.core.runtime.ipc.service;

/**
 *
 */
public interface IGlobeIpcClient {

	public String getVersion();

	public String requestsLoading(String filePath);
}
