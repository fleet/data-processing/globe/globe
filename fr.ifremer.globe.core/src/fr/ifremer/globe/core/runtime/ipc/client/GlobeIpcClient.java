/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.client;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.runtime.ipc.message.IpcMessage;
import fr.ifremer.globe.core.runtime.ipc.message.IpcMessageType;
import fr.ifremer.globe.core.runtime.ipc.preference.IpcPreferences;
import fr.ifremer.globe.core.runtime.ipc.service.IGlobeIpcClient;

@Component(name = "globe_ipc_client", service = IGlobeIpcClient.class)
public class GlobeIpcClient extends IpcClient<IpcMessage> implements IGlobeIpcClient {

	/** IPC preferences */
	@Reference
	private IpcPreferences preferences;

	@Override
	public String getVersion() {
		IpcMessage ipcMessage = new IpcMessage(IpcMessageType.VERSION);
		return sendMessage(ipcMessage);
	}

	@Override
	public String requestsLoading(String filePath) {
		IpcMessage ipcMessage = new IpcMessage(IpcMessageType.LOAD_FILE, filePath);
		return sendMessage(ipcMessage);
	}

	private String sendMessage(IpcMessage ipcMessage) {
		return sendMessage(preferences.getIpcPort().getValue(), ipcMessage);
	}

}
