/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.service;

import java.util.Optional;

import fr.ifremer.globe.core.runtime.ipc.message.IpcMessage;

/**
 *
 */
public interface IpcMessageReceiver {

	/**
	 * Ask the receiver to process the message. Returns
	 * <UL>
	 * <LI>Optional.empty when this receiver is not concerned by the message.</LI>
	 * <LI>IpcConstants.REPLY_OK when message was process successfully</LI>
	 * <LI>any String describing the result.</LI>
	 * </UL>
	 */
	Optional<String> accept(IpcMessage message);
}
