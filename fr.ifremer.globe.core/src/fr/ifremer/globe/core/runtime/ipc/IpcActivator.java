/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import fr.ifremer.globe.core.runtime.ipc.preference.IpcPreferences;

/**
 * Activator of the plugin
 */
public class IpcActivator extends Plugin {

	private static IpcActivator instance;

	public static IpcActivator getInstance() {
		return instance;
	}

	private ServiceRegistration<IpcPreferences> preferenceRegistration;

	public IpcActivator() {
		instance = this;
	}

	/** {@inheritDoc} */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		preferenceRegistration = context.registerService(IpcPreferences.class, new IpcPreferences(), null);
	}

	/** {@inheritDoc} */
	@Override
	public void stop(BundleContext context) throws Exception {
		if (preferenceRegistration != null) {
			preferenceRegistration.unregister();
		}
	}

}
