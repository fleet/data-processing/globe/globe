/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.server;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.runtime.ipc.message.IpcMessage;
import fr.ifremer.globe.core.runtime.ipc.message.IpcMessageType;
import fr.ifremer.globe.core.runtime.ipc.service.IpcMessageReceiver;
import fr.ifremer.globe.core.utils.GlobeVersion;

/**
 *
 */
@Component(name = "globe_ipc_version_message_receiver", service = IpcMessageReceiver.class)
public class VersionMessageReceiver implements IpcMessageReceiver {

	/** {@inheritDoc} */
	@Override
	public Optional<String> accept(IpcMessage message) {
		return message.type == IpcMessageType.VERSION ? Optional.of(GlobeVersion.VERSION) : Optional.empty();
	}

}
