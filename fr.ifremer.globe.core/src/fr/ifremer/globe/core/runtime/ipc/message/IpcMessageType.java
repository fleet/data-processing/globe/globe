/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.message;

/**
 * Kind of message
 */
public enum IpcMessageType {
	VERSION, LOAD_FILE;
}
