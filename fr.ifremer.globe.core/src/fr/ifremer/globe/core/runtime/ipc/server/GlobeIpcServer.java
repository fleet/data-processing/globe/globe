/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.server;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import fr.ifremer.globe.core.runtime.ipc.message.IpcConstants;
import fr.ifremer.globe.core.runtime.ipc.message.IpcMessage;
import fr.ifremer.globe.core.runtime.ipc.preference.IpcPreferences;
import fr.ifremer.globe.core.runtime.ipc.service.IGlobeIpcServer;
import fr.ifremer.globe.core.runtime.ipc.service.IpcMessageReceiver;

/**
 * Implementation of IGlobeIpcServer.
 */

@Component(name = "globe_ipc_server", service = IGlobeIpcServer.class)
public class GlobeIpcServer implements IGlobeIpcServer {

	private static final Logger logger = LoggerFactory.getLogger(GlobeIpcServer.class);

	/** IPC preferences */
	@Reference
	private IpcPreferences preferences;

	private Thread serverThread;
	private ZContext context;
	private final List<IpcMessageReceiver> messageReceivers = new LinkedList<>();

	/**
	 * Start the server
	 */
	@Activate
	protected void activate() {
		startServer();
		if (isAlive()) {
			preferences.addObserver((o, a) -> restartServer());
		}
	}

	private void restartServer() {
		preferences.save();
		stopServer();
		startServer();
	}

	private void startServer() {
		try {
			logger.debug("Try to start Globe IPC server.");

			// Socket to talk to clients
			context = new ZContext();
			ZMQ.Socket socket = context.createSocket(SocketType.REP);
			if (socket.bind("tcp://*:" + preferences.getIpcPort().getValue())) {
				serverThread = new Thread(() -> receiveMessages(socket), "Globe IPC");
				serverThread.start();
				logger.info("Globe IPC server started on port {}", preferences.getIpcPort().getValue());
			}
		} catch (ZMQException e) {
			if (e.getErrorCode() == zmq.ZError.EADDRINUSE) {
				logger.debug("An instance of Globe IPC server is already running.");
			} else {
				logger.warn("Unable to start IPC server {}", e.getMessage());
			}
		}
	}

	@Deactivate
	private void stopServer() {
		if (isAlive()) {
			serverThread.interrupt();
			context.close();
			logger.info("Globe IPC server stopped.");
		}
	}

	/**
	 * @return true if server is alive
	 */
	@Override
	public boolean isAlive() {
		return serverThread != null && serverThread.isAlive();
	}

	private void receiveMessages(ZMQ.Socket socket) {
		while (!Thread.currentThread().isInterrupted()) {
			try {
				String message = socket.recvStr(0);
				IpcMessage ipcMessage = new Gson().fromJson(message, IpcMessage.class);
				if (ipcMessage != null) {
					logger.info("Globe IPC server - received {}", ipcMessage.type);
					for (IpcMessageReceiver messageReceiver : messageReceivers) {
						Optional<String> optReply = messageReceiver.accept(ipcMessage);
						if (optReply.isPresent()) {
							String reply = optReply.get();
							socket.send(reply, 0);
							break;
						}
					}
				}
			} catch (ZMQException e) {
				if (e.getErrorCode() != zmq.ZError.EINTR) {
					logger.warn("Error while processing Ipc message : {}", e.getMessage(), e);
				} else {
					logger.warn("Ipc server interruped");
				}
			} catch (JsonParseException e) {
				logger.warn("Error while processing Ipc message : {}", e.getMessage(), e);
				socket.send(IpcConstants.REPLY_ERROR, 0);
			}
		}
	}

	/** Accept a new IpcMessageReceiver */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addIpcMessageReceiver(IpcMessageReceiver messageReceiver) {
		logger.debug("{} registered : {}", GlobeIpcServer.class.getSimpleName(), messageReceiver.getClass().getName());
		messageReceivers.add(messageReceiver);
	}

}
