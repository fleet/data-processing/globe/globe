/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.preference;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;

/**
 *
 */
public class IpcPreferences extends PreferenceComposite {

	private final IntegerPreferenceAttribute ipcPort;

	/**
	 * Constructor
	 */
	public IpcPreferences() {
		super(PreferenceRegistry.getInstance().getRootNode(), "Processus communication");
		ipcPort = new IntegerPreferenceAttribute("ipc_port", "Port", 5555);
		declareAttribute(ipcPort);

		load();
	}

	/**
	 * @return the {@link #ipcPort}
	 */
	public IntegerPreferenceAttribute getIpcPort() {
		return ipcPort;
	}

}
