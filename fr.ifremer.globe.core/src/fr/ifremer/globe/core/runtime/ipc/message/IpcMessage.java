/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.message;

/**
 *
 */
public class IpcMessage {

	public final IpcMessageType type;
	public final String argument;

	/**
	 * Constructor
	 */
	public IpcMessage(IpcMessageType type, String message) {
		this.type = type;
		this.argument = message;
	}

	/**
	 * Constructor
	 */
	public IpcMessage(IpcMessageType type) {
		this(type, "");
	}
}
