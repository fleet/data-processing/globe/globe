/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import com.google.gson.Gson;

import fr.ifremer.globe.core.runtime.ipc.message.IpcConstants;

/**
 * Client for Inter Protocol Communication with ZMQ.
 */
public class IpcClient<T> {

	private static final Logger logger = LoggerFactory.getLogger(IpcClient.class);

	/** Timeout for receive response from server */
	public static final int TIMEOUT_MS = 100;

	protected String sendMessage(int port, T ipcMessage) {
		return sendMessage(port, ipcMessage, TIMEOUT_MS);
	}

	/**
	 * Sends a message to IPC server
	 * 
	 * @param timeOut Timeout for receive operation in milliseconds. Default -1 (infinite)
	 * @return the response. IpcConstants.REPLY_TIMEOUT on timeout
	 */
	protected String sendMessage(int port, T ipcMessage, int timeOut) {
		String reply = "";
		try (ZContext context = new ZContext()) {
			logger.debug("Connecting to Globe IPC server");
			// Socket to talk to server
			try (ZMQ.Socket socket = context.createSocket(SocketType.REQ)) {
				if (timeOut != -1)
					socket.setReceiveTimeOut(timeOut);
				if (socket.connect("tcp://localhost:" + port)) {
					var jsonMessage = new Gson().toJson(ipcMessage);
					logger.debug("Requesting {}", jsonMessage);
					socket.send(new Gson().toJson(ipcMessage), 0);
					reply = socket.recvStr(0);
					if (reply == null) {
						logger.debug("No response from the IPC server (timeout) ", reply);
						reply = IpcConstants.REPLY_TIMEOUT;
					} else
						logger.debug("IPC server replies {}", reply);
				} else {
					logger.debug("IPC server did not respond", reply);
				}
			}
		}
		return reply;
	}

}
