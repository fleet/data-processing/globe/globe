package fr.ifremer.globe.core.runtime.ipc.service;

/**
 * Osgi service of the Inter-process communication between Globe and Pyat
 */

public interface IGlobeIpcServer {

	/**
	 * @return true if server is alive
	 */
	boolean isAlive();

	/**
	 * Adds a message receiver to the server. <br>
	 * Automatically invoked by Osgi when a class declares implementing a service IpcMessageReceiver. <br>
	 * May be invoked manually when using Osgi injection process is not possible.
	 */
	void addIpcMessageReceiver(IpcMessageReceiver messageReceiver);
}
