/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.runtime.ipc.message;

/**
 *
 */
public class IpcConstants {

	public static final String REPLY_TIMEOUT = "IPC_TIMEOUT";
	public static final String REPLY_ERROR = "IPC_ERROR";
	public static final String REPLY_OK = "IPC_OK";

	/**
	 * Constructor
	 */
	private IpcConstants() {
	}

}
