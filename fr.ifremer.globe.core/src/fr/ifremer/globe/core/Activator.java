package fr.ifremer.globe.core;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.csv.CsvPreferences;
import fr.ifremer.globe.core.model.wc.XsfWCPreferences;
import fr.ifremer.globe.core.processes.filtri.preferences.FilttriPreferences;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.SoundVelocityCorrectionPreference;
import fr.ifremer.globe.core.runtime.ipc.message.IpcConstants;
import fr.ifremer.globe.core.runtime.ipc.preference.IpcPreferences;
import fr.ifremer.globe.core.runtime.ipc.service.IGlobeIpcClient;
import fr.ifremer.globe.core.runtime.ipc.service.IGlobeIpcServer;
import fr.ifremer.globe.core.utils.GlobeVersion;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

public class Activator implements BundleActivator {

	private final static Logger LOGGER = LoggerFactory.getLogger(Activator.class);
	private ServiceRegistration<IpcPreferences> preferenceRegistration;

	protected static XsfWCPreferences xsfWCPreferences;
	protected ServiceRegistration<XsfWCPreferences> xsfPreferenceRegistration;

	protected ServiceRegistration<CsvPreferences> csvPreferencesRegistration;
	private static GeotiffExportPreference geotiffExportPreference;

	private static FilttriPreferences filtriparam;

	private static SoundVelocityCorrectionPreference param;

	/** Bundle's execution context within the Eclipse Framework. */
	private static BundleContext context;

	public static BundleContext getContext() {
		return context;
	}

	@Override
	public void start(BundleContext bundleContext) throws Exception {
		LOGGER.info("Starting Globe version " + GlobeVersion.VERSION);
		context = bundleContext;
		// IPC preferences
		preferenceRegistration = context.registerService(IpcPreferences.class, new IpcPreferences(), null);
		// CLI
		parseCommandLine();
		// IPC server
		launchIpcServer();

		GlobeVersion.initPreferences();

		PreferenceComposite rootNode = PreferenceRegistry.getInstance().getViewsSettingsNode();
		xsfWCPreferences = new XsfWCPreferences(rootNode);
		xsfPreferenceRegistration = context.registerService(XsfWCPreferences.class, xsfWCPreferences, null);
		csvPreferencesRegistration = context.registerService(CsvPreferences.class,
				new CsvPreferences(rootNode, "Import ascii file"), null);

		PreferenceComposite root = PreferenceRegistry.getInstance().getProcessesSettingsNode();
		param = new SoundVelocityCorrectionPreference(root, "SoundVelocityCorrection");

		// plugin general preference
		filtriparam = new FilttriPreferences(root, "Filtering");

		PreferenceComposite generic = PreferenceRegistry.getInstance().getGlobalSettingsNode();
		geotiffExportPreference = new GeotiffExportPreference(generic);

	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
		if (xsfPreferenceRegistration != null)
			xsfPreferenceRegistration.unregister();

		if (csvPreferencesRegistration != null)
			csvPreferencesRegistration.unregister();
		if (preferenceRegistration != null)
			preferenceRegistration.unregister();
	}

	public static FilttriPreferences getFilttriPreferences() {
		return filtriparam;
	}

	public static SoundVelocityCorrectionPreference getSoundVelocityCorrPrefs() {
		return param;
	}

	public static XsfWCPreferences getXsfWCPreferences() {
		return xsfWCPreferences;
	}

	public static GeotiffExportPreference getGeotiffExportPref() {
		return geotiffExportPreference;
	}

	public static CommandLine getCommandLine() throws ParseException {
		String[] arguments = getArguments();
		Options cliOptions = createArgumentOptions();
		return new DefaultParser().parse(cliOptions, arguments);

	}

	/**
	 * Parsing of the command line
	 * 
	 * @return the file to load, present in the command line
	 */
	private void parseCommandLine() {
		try {
			CommandLine commandLine = getCommandLine();
			if (commandLine.hasOption("help"))
				printHelp();

			if (commandLine.hasOption("version"))
				printVersion();

			String workspace = commandLine.getOptionValue("workspace");
			if (workspace != null)
				setWorkspace(workspace);

			String fileToLoad = commandLine.getOptionValue("load");
			if (fileToLoad != null)
				loadFile(fileToLoad);

		} catch (ParseException e) {
			System.err.println("Error while parsing command line : " + e.getMessage());
		}

	}

	private static Options createArgumentOptions() {
		Options result = new Options();

		// Common options
		result.addOption(
				Option.builder().longOpt("help").required(false).hasArg(false).desc("display this help text").build());
		result.addOption(Option.builder().longOpt("version").required(false).hasArg(false)
				.desc("display the version of Globe").build());
		result.addOption(Option.builder().longOpt("workspace").required(false).hasArg(true)
				.desc("data location for this session").build());
		result.addOption(Option.builder().longOpt("load").required(false).hasArg(true)
				.desc("launch Globe and load the specified file in the project explorer").build());

		Option conversionOption = Option.builder().longOpt("convert").required(false).hasArg(false)
				.desc("launch a MBG/XSF conversion").build();
		result.addOption(conversionOption);

		return result;
	}

	/**
	 * Set the workspace. Can not used -data CLI switch because only one globe can work of a workspace at a time
	 * (workspace considered locked and Globe start failed)
	 */
	private void setWorkspace(String workspace) {
		try {
			Location instanceLoc = Platform.getInstanceLocation();
			instanceLoc.set(new URL("file", null, workspace), false);
		} catch (Exception e) {
			System.err.println("Unable to set the workspace to '" + workspace + "' : " + e.getMessage());
		}
	}

	/** Manage --version option */
	private void printVersion() {
		System.out.println("Globe version: " + GlobeVersion.VERSION);
		System.exit(0);
	}

	/**
	 * Manage --help option
	 */
	private void printHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.setOptionComparator(null);
		formatter.printHelp("globe", createArgumentOptions(), true);
		System.exit(0);
	}

	/** Launches globe and loads a file passed in argument */
	private void loadFile(String filePath) {
		File fileToLoad = new File(filePath);
		if (fileToLoad.isFile()) {
			try {
				IGlobeIpcClient ipcClient = OsgiUtils.getService(context, IGlobeIpcClient.class);
				String reply = ipcClient.requestsLoading(filePath);
				if (IpcConstants.REPLY_OK.equals(reply)) {
					System.out.println("Loading requested");
					System.exit(0);
				} else if (IpcConstants.REPLY_TIMEOUT.equals(reply)) {
					// On Timeout, launch UI and load the file.
					LOGGER.debug("Launching UI");
				} else {
					System.err.println(reply);
					System.exit(1);
				}
			} catch (Exception e) {
				e.printStackTrace(System.err);
				System.exit(1);
			}
		}
	}

	private static String[] getArguments() {
		ServiceReference<?> ser = context.getServiceReference(EnvironmentInfo.class);
		EnvironmentInfo envInfo = (EnvironmentInfo) context.getService(ser);
		String[] arguments = envInfo.getNonFrameworkArgs();
		if (arguments.length > 1 && "-product".equalsIgnoreCase(arguments[0]))
			arguments = Arrays.copyOfRange(arguments, 2, arguments.length);
		if (arguments != null)
			return arguments;
		return new String[0];

	}

	/**
	 * @param arguments : command line arguments
	 * @param value : searched argument
	 * @return list of searched argument parameters
	 */
	private List<String> filteredArguments(List<String> arguments, String value) {
		int startIndex = 0;
		for (String argument : arguments) {
			if (argument.compareToIgnoreCase(value) == 0)
				break;
			else
				startIndex++;
		}
		return arguments.subList(startIndex + 1, arguments.size());
	}

	private void launchIpcServer() {
		IGlobeIpcServer ipcServer = OsgiUtils.getService(context, IGlobeIpcServer.class);
		if (ipcServer == null) {
			LOGGER.warn("Globe IPC server missing");
			return;
		}
		if (ipcServer.isAlive()) {
			IGlobeIpcClient ipcClient = OsgiUtils.getService(context, IGlobeIpcClient.class);
			LOGGER.info("Current version of running Globe {}", ipcClient.getVersion());
		}
	}

}
