package fr.ifremer.globe.core;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.NumberPreferenceAttribute;

/**
 * Preference node indicating if we use GDAL or geotool for DTM export
 */
public class GeotiffExportPreference extends PreferenceComposite {

	public enum Framework {
		GDAL, Geotool, Mimosa2Compliance
	}

	public enum MercatorConvert {
		Yes, No
	}

	private EnumPreferenceAttribute library;
	private EnumPreferenceAttribute convertMercator;
	private NumberPreferenceAttribute noDataValue;

	public GeotiffExportPreference(PreferenceComposite father) {
		super(father, "Geotiff export framework");
		library = new EnumPreferenceAttribute("framework", "Framework to use for dtm to geotiff export",
				Framework.GDAL);
		convertMercator = new EnumPreferenceAttribute("ConvertMercator", "Auto convert Mercator_2SP to Mercator_1SP",
				MercatorConvert.No);
		// no more framework option, just 3 options :
		// - GDAL with Mercator SP2
		// - GDAL with Mercator SP1
		// - MIMOSA == GeoTool with Mercator SP2
		// declareAttribute(library);
		
		noDataValue = new NumberPreferenceAttribute("NoDataValue", "Default value used for missing data", -99999.0);
		declareAttribute(convertMercator);
		declareAttribute(noDataValue);
		load();
	}

	public EnumPreferenceAttribute getLibrary() {
		return library;
	}

	public EnumPreferenceAttribute getConvertMercator() {
		return convertMercator;
	}

	public NumberPreferenceAttribute getNoDataValue() {
		return noDataValue;
	}
}
