package fr.ifremer.globe.core.io.gdal.info;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier.SpatialResolution;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.raster.RasterInfo;

/**
 * IInfoStore dedicated to Gdal raster file.<br>
 * Possible ContentType : IMAGE_GDAL, RASTER_GDAL <br>
 * Gather all raster file metadata
 *
 */
public class GdalFileInfo implements IFileInfo {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(GdalFileInfo.class);

	/** Raster file */
	protected String sourcefile;

	/** Content type. */
	protected ContentType rasterType = ContentType.IMAGE_GDAL;

	/** Raster X Size */
	protected int xSize;
	/** Raster Y Size */
	protected int ySize;
	/** Geo projection */
	protected Projection projection;
	/** GeoBox */
	protected GeoBox geoBox;
	/** Spatial resolutions */
	SpatialResolution spatialResolution;

	/** All embeded raster in this file */
	protected List<RasterInfo> rasterInfos = new ArrayList<>();

	/**
	 * Constructor
	 */
	public GdalFileInfo(String sourcefile) {
		this.sourcefile = sourcefile;
	}

	/** Getter of {@link #projection} */
	public Projection getProjection() {
		return projection;
	}

	/** Setter of {@link #projection} */
	protected void setProjection(Projection projection) {
		this.projection = projection;
		try {
			projection.setProjectedGeoBox(geoBox);
		} catch (ProjectionException e) {
			logger.debug("Projection error : ", e);
		}

	}

	/** Getter of {@link #xSize} */
	public int getxSize() {
		return xSize;
	}

	/** Setter of {@link #xSize} */
	protected void setxSize(int xSize) {
		this.xSize = xSize;
	}

	/** Getter of {@link #ySize} */
	public int getySize() {
		return ySize;
	}

	/** Setter of {@link #ySize} */
	protected void setySize(int ySize) {
		this.ySize = ySize;
	}

	/** Setter of {@link #geoBox} */
	protected void setGeoBox(GeoBox geoBox) {
		this.geoBox = geoBox;
	}

	/** {@inheritDoc} */
	@Override
	public String getPath() {
		return sourcefile;
	}

	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/**
	 * @see fr.ifremer.globe.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new ArrayList<>();
		result.add(Property.build("Type", rasterType == ContentType.IMAGE_GDAL ? "Image" : "Raster"));

		result.addAll(Property.build(geoBox));
		result.addAll(Property.build(projection));
		result.addAll(Property.buildSpatialResolution(projection, spatialResolution.getxResolution(),
				spatialResolution.getyResolution()));
		result.addAll(Property.buildSize(xSize, ySize));

		Optional<RasterInfo> elevationRasterInfo = getRasterInfo(RasterInfo.RASTER_ELEVATION);
		if (elevationRasterInfo.isPresent()) {
			result.addAll(Property.buildMinMax(elevationRasterInfo.get().getMinMaxValues()));
		}

		return result;
	}

	/** Add a RasterInfo to the list of embeded RasterInfo */
	void add(RasterInfo rasterInfo) {
		rasterInfos.add(rasterInfo);
	}

	/**
	 * @return the {@link #rasterInfos}
	 */
	public List<RasterInfo> getRasterInfos() {
		return rasterInfos;
	}

	public Optional<RasterInfo> getRasterInfo(String rasterElevation) {
		return rasterInfos.stream().filter(rasterInfo -> rasterElevation.equalsIgnoreCase(rasterInfo.getDataType()))
				.findFirst();
	}

	/**
	 * @return true if this info contains some Rasters. False when it's an image.
	 */
	public boolean hasRasters() {
		return !rasterInfos.isEmpty();
	}

	/**
	 * @return the {@link #rasterType}
	 */
	@Override
	public ContentType getContentType() {
		return rasterType;
	}

	/**
	 * @param contentType the {@link #rasterType} to set
	 */
	public void setContentType(ContentType contentType) {
		rasterType = contentType;
		rasterInfos.stream().forEach(rasterInfo -> rasterInfo.setContentType(contentType));
	}
}