package fr.ifremer.globe.core.io.gdal.info.json;

import com.google.gson.annotations.JsonAdapter;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Part of GeoJson file, defining a rgba color
 */
@JsonAdapter(RgbaAdapter.class)
public record Rgba(GColor gcolor) {

	public static final Rgba DEFAULT = new Rgba(GColor.DARK_GRAY);

}
