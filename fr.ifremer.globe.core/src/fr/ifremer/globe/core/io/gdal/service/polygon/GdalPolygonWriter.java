/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.gdal.service.polygon;

import java.util.Optional;

import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.Driver;
import org.gdal.ogr.Feature;
import org.gdal.ogr.FeatureDefn;
import org.gdal.ogr.FieldDefn;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.gdal.ogr.ogrConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.Gson;

import fr.ifremer.globe.core.io.gdal.info.GdalPolygonInfoSupplier;
import fr.ifremer.globe.core.io.gdal.info.GdalPolygonInfoSupplier.GdalDescription;
import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.IPolygonWriter;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Writer using GDAL to serialize polygons
 */
@Component(name = "globe_drivers_gdal_polygon_writer", service = IPolygonWriter.class)
public class GdalPolygonWriter implements IPolygonWriter {

	@Reference
	private GdalPolygonInfoSupplier polygonSupplier;

	/** {@inheritDoc} */
	@Override
	public void save(PolygonFileInfo polygonInfo) throws GException {
		if (polygonInfo.toFile().exists() && !GdalUtils.deleteFile(polygonInfo.getPath())) {
			throw new GIOException("The file exists and cannot be overwritten : " + polygonInfo.getPath());
		}
		writeFile(polygonInfo);
	}

	/** {@inheritDoc} */
	@Override
	public PolygonFileInfo saveAs(String filePath, String targetFilePath, String targetGdalDriverName)
			throws GException {
		Optional<PolygonFileInfo> polygonInfo = polygonSupplier.getFileInfo(filePath, false);
		if (polygonInfo.isEmpty() || polygonInfo.get().getPolygons().isEmpty()) {
			throw new GIOException("Unparsable file. \n" + filePath + " does not contains polygon");
		}

		PolygonFileInfo result = new PolygonFileInfo(targetFilePath);
		result.setGdalDriverName(targetGdalDriverName);
		result.getPolygons().addAll(polygonInfo.get().getPolygons());
		save(result);

		return result;
	}

	/**
	 * Write the specified geometry to a file using the Gdal driver
	 */
	private void writeFile(PolygonFileInfo polygonInfo) throws GIOException {
		Driver driver = ogr.GetDriverByName(polygonInfo.getGdalDriverName());
		if (driver == null) {
			throw new GIOException("Driver GDAL '" + polygonInfo.getGdalDriverName() + "' not available");
		}

		DataSource dataSource = driver.CreateDataSource(polygonInfo.getAbsolutePath());
		String errorMessage = "Failed to create the file '" + polygonInfo.getFilename() + "'";
		if (dataSource == null) {
			throw new GIOException(errorMessage);
		}

		try {
			Layer outputLayer = dataSource.CreateLayer(polygonInfo.getBaseName(), GdalOsrUtils.SRS_WGS84,
					ogrConstants.wkbPolygon);

			// Fields
			String descFieldName = GdalPolygonInfoSupplier.getDescriptionFieldName(driver.getName());
			outputLayer.CreateField(new FieldDefn(descFieldName, ogrConstants.OFTString));
			for (IPolygon polygon : polygonInfo.getPolygons()) {
				createFeature(polygon, outputLayer, descFieldName, errorMessage);
			}

			dataSource.FlushCache();

		} finally {
			dataSource.delete();
		}
	}

	/** Creates a Feature to store the the polygon */
	private void createFeature(IPolygon polygon, Layer outputLayer, String descFieldName, String errorMessage)
			throws GIOException {
		Geometry geometry = new Geometry(ogrConstants.wkbPolygon);
		geometry.AddGeometry(polygon.getGeometry().Clone());

		// Get the output Layer's Feature Definition
		FeatureDefn featureDefn = outputLayer.GetLayerDefn();
		// create a new feature
		Feature feature = new Feature(featureDefn);

		// Set new geometry
		if (feature.SetGeometry(geometry) == gdalconstConstants.CE_None) {
			// Set the feature on layer
			GdalDescription description = new GdalDescription(polygon.getName(), polygon.getComment());
			feature.SetField(descFieldName, new Gson().toJson(description));
			if (outputLayer.CreateFeature(feature) != gdalconstConstants.CE_None) {
				throw new GIOException(errorMessage + ". CreateFeature failed");
			}
		} else {
			throw new GIOException(errorMessage + ". SetGeometry failed");
		}
	}
}
