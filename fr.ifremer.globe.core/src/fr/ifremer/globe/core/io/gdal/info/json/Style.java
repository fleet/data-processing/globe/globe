package fr.ifremer.globe.core.io.gdal.info.json;

/**
 * 
 * <pre>
 *                            
        "_style": {
            "fill": {
                "color": "#ff0000"
            },
            "stroke": {
                "color": "#ff0000",
                "width": 1
            },
            "text": {
                "text": "Test point",
                "font": "14px Calibri,sans-serif",
                "fill": {
                    "color": "#ff0000"
                },
                "width": 14
            },
            "image": {
                "radius": 7,
                "fill": {
                    "color": "#ff0000"
                }
            }
        }
 * </pre>
 */
public record Style(Fill fill, Stroke stroke, Text text, Image image) {

	public static final Style DEFAULT = new Style(Fill.DEFAULT, Stroke.DEFAULT, Text.DEFAULT, Image.DEFAULT);

	/** Custom Constructor : no null reference */
	public Style {
		if (fill == null)
			fill = Fill.DEFAULT;
		if (stroke == null)
			stroke = Stroke.DEFAULT;
		if (text == null)
			text = Text.DEFAULT;
		if (image == null)
			image = Image.DEFAULT;
	}

}
