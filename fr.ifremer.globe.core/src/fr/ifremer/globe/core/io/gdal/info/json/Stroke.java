package fr.ifremer.globe.core.io.gdal.info.json;

/**
 * 
 * <pre>
 *                            
	    "stroke": {
	        "color": "#ff0000",
	        "width": 1
	    },
 * </pre>
 */
public record Stroke(Rgba color, int width) {

	public static final Stroke DEFAULT = new Stroke(Rgba.DEFAULT, 2);

	/** Custom Constructor : no null reference */
	public Stroke {
		if (color == null)
			color = Rgba.DEFAULT;
	}

}
