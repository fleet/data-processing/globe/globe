package fr.ifremer.globe.core.io.gdal.info.json;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Part of GeoJson file, defining the fill color
 * 
 * <pre>
 *                            
        "fill": {
            "color": "#b7b7ebff"
        }
 * </pre>
 */
public record Fill(Rgba color) {

	public static final Fill DEFAULT = new Fill(new Rgba(new GColor("#E0E0E080")));

	/** Custom Constructor : no null reference */
	public Fill {
		if (color == null || color == Rgba.DEFAULT)
			color = DEFAULT.color;
	}

}
