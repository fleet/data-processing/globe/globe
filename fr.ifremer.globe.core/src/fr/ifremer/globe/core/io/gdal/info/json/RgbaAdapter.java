package fr.ifremer.globe.core.io.gdal.info.json;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 *
 */
class RgbaAdapter extends TypeAdapter<Rgba> {
	@Override
	public void write(JsonWriter out, Rgba color) throws IOException {
		// Never used
	}

	@Override
	public Rgba read(JsonReader in) throws IOException {
		return GColor.parseString(in.nextString()).map(Rgba::new).orElse(Rgba.DEFAULT);
	}
}
