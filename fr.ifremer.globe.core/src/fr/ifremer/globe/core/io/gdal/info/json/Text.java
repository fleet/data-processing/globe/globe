package fr.ifremer.globe.core.io.gdal.info.json;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Part of GeoJson file, defining a text
 * 
 * <pre>
 *                            
"text": {
    "text": "Test point",
    "font": "Arial-PLAIN-14",
    "fill": {
        "color": "#ff0000"
    },
    "width": 14
}
 * </pre>
 */
public record Text(String text, String font, Fill fill, int width) {

	public static final Text DEFAULT = new Text("", "Arial-PLAIN-14", new Fill(new Rgba(GColor.CRIMSON)), 14);

	/** Custom Constructor : no null reference */
	public Text {
		if (text == null)
			text = "";
		if (font == null)
			font = "Arial-PLAIN-14";
		if (fill == null || fill.color() == Fill.DEFAULT.color())
			fill = DEFAULT.fill;
	}

}
