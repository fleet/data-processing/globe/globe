package fr.ifremer.globe.core.io.gdal.info;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.gdal.ogr.DataSource;
import org.gdal.ogr.Feature;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import fr.ifremer.globe.core.io.gdal.info.json.Style;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.annotation.Annotation;
import fr.ifremer.globe.core.model.file.annotation.AnnotationFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Loader JSON file, parsed with GDAL. Builder of GdalGeoJsonFileInfo
 */
@Component(//
		name = "globe_drivers_gdal_geojson_info_supplier", //
		service = { IFileInfoSupplier.class, GdalGeoJsonInfoSupplier.class })
public class GdalGeoJsonInfoSupplier extends BasicFileInfoSupplier<AnnotationFileInfo> {

	/** Logger */
	private static Logger logger = LoggerFactory.getLogger(GdalGeoJsonInfoSupplier.class);

	/**
	 * Constructor
	 */
	public GdalGeoJsonInfoSupplier() {
		super("json", "GeoJson (*.json)", ContentType.GEOJSON_GDAL);
	}

	/** Instantiates a new GdalGeoJsonFileInfo */
	@Override
	public AnnotationFileInfo makeFileInfo(String filePath) {
		DataSource datasource = null;
		try {
			datasource = ogr.Open(filePath, false);
			if (datasource == null) {
				// Unreadable file
				return null;
			}

			List<Annotation> annotations = new LinkedList<>();
			for (int iLayer = 0; iLayer < datasource.GetLayerCount(); iLayer++) {
				Layer layer = datasource.GetLayer(iLayer);
				if (layer != null) {
					layer.ResetReading(); // In case readings have been done before
					annotations.addAll(readFeatures(layer));
					layer.delete();
				}
			}

			if (annotations.isEmpty())
				return null;

			return new AnnotationFileInfo(filePath, annotations);
		} finally {
			if (datasource != null)
				datasource.delete();
		}
	}

	/**
	 * Browse feature and read Polygons
	 */
	private List<Annotation> readFeatures(Layer layer) {
		List<Annotation> result = new LinkedList<>();
		Feature feature = layer.GetNextFeature();
		while (feature != null) {
			result.addAll(readFeature(feature));
			feature.delete();
			feature = layer.GetNextFeature();
		}
		return result;
	}

	/**
	 * Read Polygons in the feature
	 */
	private List<Annotation> readFeature(Feature feature) {
		List<Annotation> result = new LinkedList<>();
		Style style = null;
		if (feature.IsFieldSetAndNotNull("_style")) {
			String field = feature.GetFieldAsString("_style");
			try {
				style = new Gson().fromJson(field, Style.class);
			} catch (JsonParseException e) {
				logger.debug("Field _style can not be parsed '{}'", field);
			}
		}

		parseGeometry(feature.GetGeometryRef(), style != null ? style : Style.DEFAULT)//
				.ifPresent(result::add);

		return result;
	}

	/**
	 * @return the polygon contains in the geometry
	 */
	private Optional<Annotation> parseGeometry(Geometry geometry, Style style) {

		if (geometry == null)
			return Optional.empty();

		// Extract label
		String label = style.text().text();
		GColor labelColor = style.text().fill().color().gcolor();
		String fontName = style.text().font();
		int fontSize = style.text().width();

		// Extract interior
		GColor interior = style.fill().color().gcolor();

		// Extract interior
		GColor outline = style.stroke().color().gcolor();

		// Extract outline width or radius
		int outlineWidth = style.stroke().width();

		// Single point : shape is a point
		if (geometry.GetPointCount() == 1) {
			outlineWidth = style.image().radius();
			interior = style.image().fill().color().gcolor();
			outline = GColor.TRANSPARENT;
		}

		Geometry clonedGeometry = geometry.Clone();
		geometry.delete();

		return Optional.of(
				new Annotation(label, labelColor, fontName, fontSize, clonedGeometry, interior, outline, outlineWidth));
	}

}