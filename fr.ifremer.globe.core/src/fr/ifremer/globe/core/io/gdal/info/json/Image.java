package fr.ifremer.globe.core.io.gdal.info.json;

/**
 * Part of GeoJson file, defining an image
 * 
 * <pre>
 *                            
        "image": {
            "radius": 7,
            "fill": {
                "color": "#ff0000"
            }
        }
 * </pre>
 */
public record Image(int radius, Fill fill) {

	public static final Image DEFAULT = new Image(0, Fill.DEFAULT);

	/** Custom Constructor : no null reference */
	public Image {
		if (fill == null)
			fill = Fill.DEFAULT;
	}

}
