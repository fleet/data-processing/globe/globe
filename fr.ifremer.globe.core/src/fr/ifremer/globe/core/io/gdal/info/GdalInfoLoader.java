package fr.ifremer.globe.core.io.gdal.info;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.io.FilenameUtils;
import org.gdal.gdal.Band;
import org.gdal.gdal.ColorTable;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.osr.SpatialReference;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.color.BitColorPalette;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Loader of Gdal file info. Builder of GdalRasterInfo
 */
@Component(//
		name = "globe_drivers_gdal_file_info_supplier", //
		service = { GdalInfoLoader.class, IFileInfoSupplier.class, IRasterInfoSupplier.class,
				IGeographicInfoSupplier.class })
public class GdalInfoLoader implements IFileInfoSupplier<GdalFileInfo>, IRasterInfoSupplier, IGeographicInfoSupplier {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalInfoLoader.class);

	/** Supported file type */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.IMAGE_GDAL,
			ContentType.RASTER_GDAL);

	/** Extensions **/
	protected List<String> extensions = null;

	/** File filters */
	protected List<Pair<String, String>> fileFilters;

	/**
	 * @see fr.ifremer.globe.model.services.raster.IRasterInfoSupplier#supplyRasterInfos(java.lang.String)
	 */
	@Override
	public List<RasterInfo> supplyRasterInfos(String filePath) throws GIOException {
		Optional<GdalFileInfo> gdalFileInfo = makeFileInfo(filePath);
		return gdalFileInfo.map(GdalFileInfo::getRasterInfos).orElse(Collections.emptyList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RasterInfo> supplyRasterInfos(IFileInfo fileInfo) {
		return fileInfo instanceof GdalFileInfo info ? info.getRasterInfos() : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<GdalFileInfo> getFileInfo(String filePath) {
		return Optional.empty(); // Need a FileInfoSettings to know if elevation are present or not
	}

	/**
	 * Try to parse the specified file and create a GdalFileInfo
	 */
	public Optional<GdalFileInfo> makeFileInfo(String filePath) {
		GdalFileInfo result = null;
		try {
			result = loadInfo(filePath);
		} catch (GIOException e) {
			logger.warn("Error on {} loading : {}", filePath, e.getMessage());
		}
		return Optional.ofNullable(result);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<GdalFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		Optional<GdalFileInfo> result = makeFileInfo(filePath);
		if (result.isPresent()) {
			GdalFileInfo fileInfo = result.get();
			String dataType = settings.getOrDefault("DataType", RasterInfo.RASTER_RAW);
			fileInfo.getRasterInfos().forEach(rasterInfo -> rasterInfo.setDataType(dataType));
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EnumSet<ContentType> getContentTypes() {
		return CONTENT_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getExtensions() {
		if (extensions == null) {
			Set<String> allGdalExtensions = IntStream.range(0, gdal.GetDriverCount())//
					.mapToObj(gdal::GetDriver)//
					.filter(driver -> "YES".equalsIgnoreCase(driver.GetMetadataItem("DCAP_RASTER")))
					.map(driver -> driver.GetMetadataItem(gdalconstConstants.GDAL_DMD_EXTENSIONS))//
					.filter(Objects::nonNull) //
					.filter(extension -> !extension.isEmpty()) //
					.flatMap(extension -> Arrays.stream(extension.split(" "))) //
					.collect(Collectors.toSet());
			extensions = new ArrayList<>(allGdalExtensions);
		}
		return extensions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		if (fileFilters == null) {
			String filters = getExtensions().stream().//
					map(extension -> "*." + extension).//
					collect(Collectors.joining(";"));

			fileFilters = Arrays.asList(new Pair<>("Gdal Raster", filters));
		}
		return fileFilters;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Projection> getProjection(IFileInfo fileInfo) {
		if (fileInfo instanceof GdalFileInfo info) {
			return Optional.ofNullable(info.getProjection());
		}
		return Optional.empty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<SpatialResolution> getSpatialResolution(IFileInfo fileInfo) {
		if (fileInfo instanceof GdalFileInfo info) {
			return Optional.ofNullable(info.spatialResolution);
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<GeoBox> getGeoBox(IFileInfo fileInfo) {
		if (fileInfo instanceof GdalFileInfo info) {
			return Optional.ofNullable(info.getGeoBox());
		}
		return Optional.empty();
	}

	/**
	 * @return the GdalRasterInfo of the specified resource. null if filePath is not managed by Gdal
	 * @throws GIOException read failed, error occured
	 */
	protected GdalFileInfo loadInfo(String filePath) throws GIOException {
		GdalFileInfo result = null;
		try (GdalDataset dataset = GdalDataset.open(filePath)) {
			if (dataset.isPresent()) {
				result = loadInfo(filePath, dataset.get());
			}
		}
		return result;
	}

	/**
	 * @return the GdalRasterInfo of the specified opened resource.
	 * @throws GIOException read failed, error occured
	 */
	protected GdalFileInfo loadInfo(String filePath, Dataset dataset) throws GIOException {
		GdalFileInfo result = new GdalFileInfo(filePath);

		result.setxSize(dataset.getRasterXSize());
		result.setySize(dataset.getRasterYSize());

		// Projection
		String projWkt = dataset.GetProjectionRef();
		if (projWkt == null || projWkt.isEmpty()) {
			projWkt = dataset.GetProjection();
		}
		SpatialReference srs = GdalOsrUtils.SRS_WGS84;
		if (projWkt != null && !projWkt.isEmpty()) {
			srs = new SpatialReference(projWkt);
		}

		// Spatial resolution
		double[] resolution = GdalUtils.computeSpatialResolution(dataset);
		if (resolution[0] <= 0d || resolution[1] <= 0d) {
			throw new GIOException(FilenameUtils.getName(result.sourcefile) + " - bad spatial resolution");
		}
		result.spatialResolution = new SpatialResolution(resolution[0], resolution[1]);

		// BoundingBox
		double[] boundingBox = GdalUtils.getBoundingBox(dataset);
		GdalOsrUtils.tranformBoundingBoxToWGS84(srs, boundingBox);
		result.setGeoBox(new GeoBox(boundingBox));
		result.setProjection(Projection.createFromProj4String(srs.ExportToProj4()));
		srs.delete();

		int bandCount = dataset.getRasterCount();
		Band band = bandCount > 0 ? dataset.GetRasterBand(1) : null;
		if (band != null) {
			int colorInterpretation = band.GetColorInterpretation();
			int dataType = band.getDataType();
			// Check if image, raster or bitfield
			String description = band.GetDescription();
			// Well-known tif of bitfield ?
			if (bandCount == 1 && description != null && description.startsWith("bitfield")) {
				result.setContentType(ContentType.RASTER_GDAL);
				createBitfieldRasterInfo(band, result);
			} else if (colorInterpretation == gdalconstConstants.GCI_Undefined || //
					dataType != gdalconstConstants.GDT_Byte && dataType != gdalconstConstants.GDT_UInt16
							&& dataType != gdalconstConstants.GDT_UInt32) {
				createRasterInfos(band, result);
				// Other raster
				for (int i = 2; i <= bandCount; i++) {
					band = dataset.GetRasterBand(i);
					createRasterInfos(band, result);
					band.delete();
				}
				result.setContentType(ContentType.RASTER_GDAL);
			}
			band.delete();
		} else {
			throw new GIOException(FilenameUtils.getName(result.sourcefile) + " has no data");
		}

		return result;
	}

	/**
	 * Create an instance of RasterInfo
	 */
	private RasterInfo makeRasterInfo(Band band, GdalFileInfo gdalFileInfo) throws GIOException {
		RasterInfo rasterInfo = new RasterInfo(this, gdalFileInfo.getPath());
		rasterInfo.setContentType(gdalFileInfo.getContentType());
		rasterInfo.setGdalLoadingKey(gdalFileInfo.getPath());
		rasterInfo.setGdalBand(band.GetBand());
		rasterInfo.setGeoBox(gdalFileInfo.getGeoBox());
		rasterInfo.setProjection(gdalFileInfo.getProjection());
		rasterInfo.setXSize(band.getXSize());
		rasterInfo.setYSize(band.getYSize());
		rasterInfo.setNoDataValue(GdalUtils.getNoDataValue(band));
		rasterInfo.setMinMaxValues(GdalUtils.getMinMaxValues(band));
		if (rasterInfo.getMinMaxValues() == null) {
			throw new GIOException("No valid value found in layer '" + rasterInfo.getDataType() + "'");
		}

		rasterInfo.setUnit("");
		rasterInfo.setFullyLoaded(true);
		return rasterInfo;
	}

	/**
	 * Add an instance of RasterInfo (datatype RASTER_UNDEFINED) to the GdalFileInfo. Will be promoted to ELEVATION or
	 * RAW later
	 */
	protected void createRasterInfos(Band band, GdalFileInfo gdalFileInfo) throws GIOException {
		RasterInfo rasterInfo = makeRasterInfo(band, gdalFileInfo);
		// Undefined for the moment. RasterInfo.RASTER_RAW or RasterInfo.RASTER_ELEVATION later...
		String description = band.GetDescription();
		if (description != null && !description.isEmpty()) {
			rasterInfo.setDataType(description);
		}
		gdalFileInfo.add(rasterInfo);
	}

	/**
	 * Add an instance of RasterInfo (datatype RASTER_RAW) to the GdalFileInfo for a Bitfield raster
	 */
	protected void createBitfieldRasterInfo(Band band, GdalFileInfo gdalFileInfo) throws GIOException {
		RasterInfo rasterInfo = makeRasterInfo(band, gdalFileInfo);
		gdalFileInfo.add(rasterInfo);

		rasterInfo.setDataType(RasterInfo.RASTER_BITFIELD);
		BitColorPalette palette = new BitColorPalette();
		rasterInfo.setPalette(Optional.of(palette));

		// Extract name of bits
		String description = band.GetDescription();
		List<String> bitfields = new LinkedList<>();
		if (description != null && description.startsWith("bitfield=")) {
			String[] bits = description.substring(9).split(",");
			for (String bit : bits) {
				bit = "None".equalsIgnoreCase(bit) ? "" : bit;
				bitfields.add(0, "None".equalsIgnoreCase(bit) ? "" : bit);
			}
		}

		ColorTable colorTable = band.GetColorTable();
		// Case no bit set, value 0
		GColor gColor = GColor.CONTRAST_PALETTE[0];
		if (colorTable.GetCount() > 0) {
			var color = colorTable.GetColorEntry(0);
			gColor = new GColor(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
		}
		palette.addColor("No value", true, 0, gColor);

		// Complete the palette with the color of each bit
		if (!bitfields.isEmpty()) {
			for (int shift = 0; shift < 8; shift++) {
				if (shift < bitfields.size() && !bitfields.get(shift).isEmpty()) {
					String bitfield = bitfields.get(shift);
					int bitValue = 1 << shift;
					gColor = GColor.CONTRAST_PALETTE[shift + 1];
					if (bitValue < colorTable.GetCount()) {
						var color = colorTable.GetColorEntry(bitValue);
						gColor = new GColor(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
					}
					palette.addColor(bitfield, true, bitValue, gColor);
				}
			}
		}
	}

}