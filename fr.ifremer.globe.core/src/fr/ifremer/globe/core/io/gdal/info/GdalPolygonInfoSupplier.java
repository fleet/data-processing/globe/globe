package fr.ifremer.globe.core.io.gdal.info;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.Feature;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.gdal.osr.SpatialReference;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.polygon.BasicPolygon;
import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gdal.GdalOgrUtils;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;

/**
 * Loader of Gdal file info. Builder of GdalRasterInfo
 */
@Component(//
		name = "globe_drivers_gdal_polygon_info_supplier", //
		service = { IFileInfoSupplier.class, GdalPolygonInfoSupplier.class }, property = {
				FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer="
						+ FileInfoSupplierPriority.EXTENDED_POLYGON_PRIORITY })
public class GdalPolygonInfoSupplier implements IFileInfoSupplier<PolygonFileInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalPolygonInfoSupplier.class);

	/** Expected value of the field called GdalField.GLOBETYPE */
	public static final String GLOBETYPE_FIELD_VAL = ContentType.POLYGON_GDAL.name();

	/** Max nb of polygon allowed in a file */
	private static final int MAX_POLYGON_COUNT = 1000;

	/** Supported file type */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.POLYGON_GDAL);

	/** Extensions **/
	protected List<String> extensions = null;

	/** File filters */
	protected List<Pair<String, String>> fileFilters;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentTypes() {
		return CONTENT_TYPES;
	}

	/** {@inheritDoc} */
	@Override
	public List<String> getExtensions() {
		if (extensions == null) {
			Set<String> allGdalExtensions = List
					.of(GdalUtils.GEOJSON_DRIVER_NAME, GdalUtils.KML_DRIVER_NAME, GdalUtils.SHP_DRIVER_NAME)//
					.stream() //
					.map(gdal::GetDriverByName)//
					.map(driver -> driver.GetMetadataItem(gdalconstConstants.GDAL_DMD_EXTENSIONS))//
					.filter(Objects::nonNull) //
					.filter(extension -> !extension.isEmpty()) //
					.flatMap(extension -> Arrays.stream(extension.split(" "))) //
					.collect(Collectors.toSet());
			extensions = new ArrayList<>(allGdalExtensions);
		}
		return extensions;
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		if (fileFilters == null) {
			String filters = getExtensions().stream().//
					map(extension -> "*." + extension).//
					collect(Collectors.joining(";"));

			fileFilters = Arrays.asList(new Pair<>("Polygon", filters));
		}
		return fileFilters;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<PolygonFileInfo> getFileInfo(String filePath) {
		if (getExtensions().contains(FilenameUtils.getExtension(filePath))) {
			return getFileInfo(filePath, true);
		}
		return Optional.empty();
	}

	/**
	 * Load polygon contained in the specified file and build a PolygonFileInfo
	 *
	 * @param withGlobeTypeOnly when true, accept only file with {@link GdalField#GLOBETYPE} set
	 */
	public Optional<PolygonFileInfo> getFileInfo(String filePath, boolean withGlobeTypeOnly) {
		DataSource datasource = null;
		try {
			datasource = ogr.Open(filePath, false);
			if (datasource == null) {
				// Unreadable file
				return Optional.empty();
			}

			PolygonFileInfo result = new PolygonFileInfo(filePath);
			result.setGdalDriverName(datasource.GetDriver().getName());
			boolean fileEmpty = true;
			for (int iLayer = 0; iLayer < datasource.GetLayerCount(); iLayer++) {
				Layer layer = datasource.GetLayer(iLayer);
				if (layer != null) {
					fileEmpty = fileEmpty && layer.GetFeatureCount() == 0l;
					layer.ResetReading(); // In case readings have been done before
					readFeatures(layer, result, withGlobeTypeOnly);
					layer.delete();
				}
			}

			Object[] metadata = new Object[3];
			Map<String, Set<String>> fields = new HashMap<>();
			GdalOgrUtils.readMetadata(datasource, //
					spatialReference -> metadata[0] = spatialReference, //
					extent -> metadata[1] = extent, //
					geometry -> metadata[2] = geometry, //
					fields::putAll);

			if (!fileEmpty && result.getPolygons().isEmpty()) {
				// Features exist but no suitable polygon was found : bad file
				return Optional.empty();
			}

			// File may be empty (no feature). This is not an error
			return Optional.of(result);

		} catch (IOException e) {
			logger.warn("Error while reading file {} : {}", filePath, e.getMessage());
		} finally {
			if (datasource != null) {
				datasource.delete();
			}
		}
		return Optional.empty();
	}

	/**
	 * Browse feature and read Polygons
	 */
	private void readFeatures(Layer layer, PolygonFileInfo fileInfo, boolean withGlobeTypeOnly) {
		Feature feature = layer.GetNextFeature();
		while (feature != null && fileInfo.getPolygons().size() <= MAX_POLYGON_COUNT) {
			readFeature(fileInfo, feature, withGlobeTypeOnly);
			feature.delete();
			feature = layer.GetNextFeature();
		}
	}

	/**
	 * Read Polygons in the feature
	 */
	private void readFeature(PolygonFileInfo fileInfo, Feature feature, boolean withGlobeTypeOnly) {
		GdalDescription description = null;
		if (withGlobeTypeOnly) {
			String descriptionField = getDescriptionFieldName(fileInfo.getGdalDriverName());
			if (feature.IsFieldSetAndNotNull(descriptionField)) {
				var descriptionString = feature.GetFieldAsString(descriptionField);
				try {
					description = new Gson().fromJson(feature.GetFieldAsString(descriptionField),
							GdalDescription.class);
				} catch (JsonParseException ex) {
					logger.warn("Error while parsing description field of KML as json : {} ", ex.getMessage());
					description = new GdalDescription("Description", descriptionString);
				}
			}
		} else {
			description = new GdalDescription("Polygon", "");
		}

		if (description != null) {
			var geometry = feature.GetGeometryRef();
			if (geometry != null) {
				fileInfo.getPolygons().addAll(parseGeometry(feature, geometry, description));
			}

		}
	}

	/** Accept the extent from the shapefile */
	protected void fitExtent(PolygonFileInfo info, double[] extent) {
		if (extent != null) {
			info.setGeoBox(new GeoBox(new double[] { //
					Math.max(extent[2], extent[3]), //
					Math.min(extent[2], extent[3]), //
					Math.max(extent[0], extent[1]), //
					Math.min(extent[0], extent[1])//
			}));
		}
	}

	/**
	 * @return the polygon contains in the geometry
	 */
	private Collection<? extends IPolygon> parseGeometry(Feature feature, Geometry geometry,
			GdalDescription description) {
		List<IPolygon> result = new LinkedList<>();
		for (int i = 0; i < geometry.GetGeometryCount() && result.size() <= MAX_POLYGON_COUNT; i++) {
			Geometry subGeometry = geometry.GetGeometryRef(i);
			// At least 4 points in a polygon
			if (subGeometry != null && subGeometry.GetPointCount() > 3) {
				// Only LonLat
				SpatialReference srs = subGeometry.GetSpatialReference();
				if (srs != null && srs.IsProjected() == 1
						&& subGeometry.TransformTo(GdalOsrUtils.SRS_WGS84) != GdalUtils.OK) {
					// Transformation failed
					subGeometry.delete();
					continue;
				}

				BasicPolygon polygon = new BasicPolygon(description.name, subGeometry.Clone());
				polygon.setComment(description.comment);

				result.add(polygon);
				subGeometry.delete();
			}

			result.addAll(parseGeometry(feature, subGeometry, description));

		}
		return result;
	}

	/** Provide the name of the description field */
	public static String getDescriptionFieldName(String gdalDriverName) {
		String descriptionFieldName = "globe_description";
		return Map.of(//
				GdalUtils.GEOJSON_DRIVER_NAME, descriptionFieldName, //
				GdalUtils.KML_DRIVER_NAME, "description", // Only Description is compliant for kml
				GdalUtils.SHP_DRIVER_NAME, descriptionFieldName.substring(0, 10)// shp max lenght is 10
		).getOrDefault(gdalDriverName, descriptionFieldName);
	}

	/** Gdal Fields describing a polygon and serializable in GSon */
	public static class GdalDescription {
		/** Name of polygon */
		public final String name;
		/** Comment on polygon */
		public final String comment;

		/**
		 * Constructor
		 */
		public GdalDescription(String name, String comment) {
			super();
			this.name = name;
			this.comment = comment;
		}
	}
}