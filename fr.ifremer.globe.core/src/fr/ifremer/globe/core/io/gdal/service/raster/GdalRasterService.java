/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.gdal.service.raster;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.gdal.info.GdalInfoLoader;
import fr.ifremer.globe.core.model.raster.GdalRasterReader;
import fr.ifremer.globe.core.model.raster.IRasterReader;
import fr.ifremer.globe.core.model.raster.IRasterReaderFactory;
import fr.ifremer.globe.core.model.raster.RasterInfo;

/**
 * IRasterReaderFactory to create a IRasterReader for Gdal files
 */
@Component(name = "globe_drivers_gdal_raster_reader_factory", service = IRasterReaderFactory.class)
public class GdalRasterService implements IRasterReaderFactory {

	/**
	 * Loader of Gdal file
	 */
	@Reference
	protected GdalInfoLoader gdalInfoLoader = new GdalInfoLoader();

	/**
	 * @see fr.ifremer.globe.model.services.raster.IRasterReaderFactory#createRasterReader(fr.ifremer.globe.model.services.raster.RasterInfo)
	 */
	@Override
	public Optional<IRasterReader> createRasterReader(RasterInfo rasterInfo) {
		IRasterReader result = null;
		if (rasterInfo.getSupplier() == gdalInfoLoader) {
			result = new GdalRasterReader();
		}
		return Optional.ofNullable(result);
	}

}
