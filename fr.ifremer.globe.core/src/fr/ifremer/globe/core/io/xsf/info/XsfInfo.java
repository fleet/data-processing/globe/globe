/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.xsf.info;

import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfUtils;
import fr.ifremer.globe.core.io.xsf.writer.XsfProcessAssessor;
import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.SensorInstallationParameters;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * XsfInfo: this class provides informations about an XSF file.
 */
public class XsfInfo implements ISounderNcInfo {

	private static final Logger logger = LoggerFactory.getLogger(XsfInfo.class);

	// minimum XSF version model, if not will need to convert again data
	private static final double XSF_MIN_SUPPORTED_VERSION = 0.3;

	/** file properties **/
	private NetcdfFile file;
	private final String filePath;
	private final AtomicBoolean isOpen = new AtomicBoolean();

	/** XSF info attributes */
	private String version;
	private GeoBox geobox;
	private String echoSounderModel = "Not defined";
	private short echoSounderIndex = -1;
	private String constructor = "Unknown";
	private int swathCount;
	private Date firstSwathDate;
	private Date lastSwathDate;
	private Optional<Range> minMaxDepth = Optional.empty();
	private List<AntennaInstallationParameters> antennas = new ArrayList<>();
	private long wcRxBeamCount;
	private int bathyRxBeamCount;
	private int txBeamCount;
	private Property<String> xsfRawAttributes;
	private byte automaticCleaning = 0;
	private byte manualCleaning = 0;
	private byte velocityCorrection = 0;
	private byte biasCorrection = 0;
	private byte tideCorrection = 0;
	private byte positionCorrection = 0;
	private byte draughtCorrection = 0;
	private byte backscatterCorrection = 0;
	private List<String> biasCorrectors = new ArrayList<>();

	private List<SensorInstallationParameters> attitudeInstallationParameters = new ArrayList<>();

	/** Default groupId in /Platform/Attitude */
	private String preferedAttitudeGroupId;
	private int preferedAttitudeGroupIndex = -1;

	/** Default groupId in /Platform/Position */
	private String preferedPositionGroupId;

	protected List<String> history = new ArrayList<>();

	/** Constructor */
	public XsfInfo(String filePath) throws GIOException {
		this.filePath = filePath;
		new XsfInfoReader().readAttributes(this);
	}

	/** {@inheritDoc} */
	@Override
	public void open(boolean readonly) throws GIOException {
		try {
			if (isOpen().compareAndSet(false, true)) {
				setFile(NetcdfFile.open(filePath, readonly ? Mode.readonly : Mode.readwrite));
			}
		} catch (NCException e) {
			isOpen.set(false);
			throw new GIOException("Error while opening NetcdfFile", e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void close() {
		if (!IDataContainerFactory.grab().isBooked(this) && isOpen.compareAndSet(true, false)) {
			file.close();
		}
	}

	/** @return {@link AtomicBoolean} which defined file opening state **/
	public AtomicBoolean isOpen() {
		return isOpen;
	}

	// TODO set this method generic for xsf and mbg
	@Override
	public void declareLayers(SounderDataContainer container) throws GException {
		NetcdfLayerDeclarer netcdfLayerDeclarer = new NetcdfLayerDeclarer();
		netcdfLayerDeclarer.declareLayers(getFile(), container, NetcdfGroup::getPath,
				XsfToPredefinedLayerMap::providePredefinedLayer);
	}

	/**********************************************************************************************************
	 * Getters / Setters
	 **********************************************************************************************************/

	public NetcdfFile getFile() {
		return file;
	}

	public void setFile(NetcdfFile file) {
		this.file = file;
	}

	@Override
	public String getPath() {
		return filePath;
	}

	public String getXsfVersion() {
		return version;
	}

	protected void setXsfVersion(String version) {
		this.version = version;
	}

	@Override
	public int getCycleCount() {
		return swathCount;
	}

	protected void setCycleCount(int swathCount) {
		this.swathCount = swathCount;
	}

	@Override
	public GeoBox getGeoBox() {
		return geobox;
	}

	protected void setGeoBox(GeoBox geoBox) {
		geobox = geoBox;
	}

	@Override
	public String getEchoSounderModel() {
		return echoSounderModel;
	}

	protected void setEchoSounderModel(String echoSounderModel) {
		this.echoSounderModel = echoSounderModel;
	}

	@Override
	public short getEchoSounderIndex() {
		return echoSounderIndex;
	}

	protected void setEchoSounderIndex(short echoSounderIndex) {
		this.echoSounderIndex = echoSounderIndex;
	}

	@Override
	public Date getFirstPingDate() {
		return firstSwathDate;
	}

	protected void setFirstSwathDate(Date firstSwathDate) {
		this.firstSwathDate = firstSwathDate;
	}

	@Override
	public Date getLastPingDate() {
		return lastSwathDate;
	}

	protected void setLastSwathDate(Date lastSwathDate) {
		this.lastSwathDate = lastSwathDate;
	}

	protected void setMinMaxDepth(float minDepth, float maxDepth) {
		minMaxDepth = Optional.of(new FloatRange(minDepth, maxDepth));
	}

	@Override
	public long getWcRxBeamCount() {
		return wcRxBeamCount;
	}

	protected void setWcRxBeamCount(long wcRxBeamCount) {
		this.wcRxBeamCount = wcRxBeamCount;
	}

	@Override
	public int getTotalTxBeamCount() {
		return txBeamCount;
	}

	protected void setTotalTxBeamCount(int txBeamCount) {
		this.txBeamCount = txBeamCount;
	}

	@Override
	public int getTotalRxBeamCount() {
		return bathyRxBeamCount;
	}

	protected void setTotalRxBeamCount(int rxBeamCount) {
		this.bathyRxBeamCount = rxBeamCount;
	}

	public byte getAutomaticCleaning() {
		return automaticCleaning;
	}

	public void setAutomaticCleaning(byte automaticCleaning) {
		this.automaticCleaning = automaticCleaning;
	}

	public byte getManualCleaning() {
		return manualCleaning;
	}

	public void setManualCleaning(byte manualCleaning) {
		this.manualCleaning = manualCleaning;
	}

	public byte getBiasCorrection() {
		return biasCorrection;
	}

	public void setBiasCorrection(byte biasCorrection) {
		this.biasCorrection = biasCorrection;
	}

	public List<String> getBiasCorrectors() {
		return biasCorrectors;
	}

	public void setBiasCorrectors(List<String> biasCorrectors) {
		this.biasCorrectors = biasCorrectors;
	}

	public void addBiasCorrectors(List<String> biasCorrectors) {
		Set<String> uniqueList = new HashSet<>(this.biasCorrectors);
		uniqueList.addAll(biasCorrectors);
		this.biasCorrectors = new ArrayList<>(uniqueList);
	}

	public byte getVelocityCorrection() {
		return velocityCorrection;
	}

	public void setVelocityCorrection(byte velocityCorrection) {
		this.velocityCorrection = velocityCorrection;
	}

	public byte getTideCorrection() {
		return tideCorrection;
	}

	public void setTideCorrection(byte tideCorrection) {
		this.tideCorrection = tideCorrection;
	}

	public byte getPositionCorrection() {
		return positionCorrection;
	}

	public void setPositionCorrection(byte positionCorrection) {
		this.positionCorrection = positionCorrection;
	}

	public byte getDraughtCorrection() {
		return draughtCorrection;
	}

	public void setDraughtCorrection(byte draughtCorrection) {
		this.draughtCorrection = draughtCorrection;
	}

	public byte getBackscatterCorrection() {
		return backscatterCorrection;
	}

	public void setBackscatterCorrection(byte backscatterCorrection) {
		this.backscatterCorrection = backscatterCorrection;
	}

	public List<String> getHistory() {
		return history;
	}

	public void setHistory(List<String> history) {
		this.history = history;
	}

	/**********************************************************************************************************
	 * ANTENNAS
	 **********************************************************************************************************/

	@Override
	public AntennaInstallationParameters getTxParameters() {
		List<AntennaInstallationParameters> txAntennas = getTxParametersList();
		return txAntennas.isEmpty() ? null : txAntennas.get(0);
	}

	protected List<AntennaInstallationParameters> getTxParametersList() {
		return antennas.stream().filter(ant -> (ant.type == AntennaInstallationParameters.Type.TX
				|| ant.type == AntennaInstallationParameters.Type.TXRX)).toList();
	}

	@Override
	public List<AntennaInstallationParameters> getRxParameters() {
		return antennas.stream().filter(ant -> (ant.type == AntennaInstallationParameters.Type.RX
				|| ant.type == AntennaInstallationParameters.Type.TXRX)).toList();
	}

	@Override
	public List<AntennaInstallationParameters> getAntennaParameters() {
		return antennas;
	}

	/**********************************************************************************************************
	 * SENSORS
	 **********************************************************************************************************/

	@Override
	public List<SensorInstallationParameters> getAttitudeInstallationParameters() {
		return attitudeInstallationParameters;
	}

	@Override
	public Optional<String> getPreferedAttitudeGroupId() {
		return Optional.ofNullable(preferedAttitudeGroupId);
	}

	@Override
	public Optional<SensorInstallationParameters> getPreferedAttitudeInstallationParameters() {
		List<SensorInstallationParameters> attitudes = getAttitudeInstallationParameters();
		if (preferedAttitudeGroupIndex >= 0 && preferedAttitudeGroupIndex < attitudes.size()) {
			return Optional.of(attitudes.get(preferedAttitudeGroupIndex));
		} else if (!attitudes.isEmpty()) {
			return Optional.of(attitudes.get(0));
		} else {
			return Optional.empty();
		}
	}

	public void setPreferedAttitudeGroupId(String groupId) {
		preferedAttitudeGroupId = groupId;
	}

	public void setPreferedAttitudeGroupIndex(int groupIndex) {
		preferedAttitudeGroupIndex = groupIndex;
	}

	@Override
	public Optional<String> getPreferedPositionGroupId() {
		return Optional.ofNullable(preferedPositionGroupId);
	}

	public void setPreferedPositionGroupId(String groupId) {
		preferedPositionGroupId = groupId;
	}

	/**********************************************************************************************************
	 *
	 **********************************************************************************************************/

	/**
	 * Properties getter (provides data to display in the properties view)
	 */
	@Override
	public List<Property<?>> getProperties() {

		List<Property<?>> properties = new ArrayList<>();

		// FILE PROPERTIES
		properties.add(Property.build("Xsf version", getXsfVersion()));
		properties.add(Property.build(IConstants.EchoSounderModel, getEchoSounderModel()));

		// MIN / MAX DEPTHS
		if (minMaxDepth.isPresent()) {
			Property<String> rangeProps = Property.build("Depth", "");
			Property.buildMinMax(minMaxDepth.get()).forEach(rangeProps::add);
			properties.add(rangeProps);
		} else {
			properties.add(Property.build("Depth", "Computation in progress..."));
			new XsfInfoReader().updateProperties(this);
		}

		properties.addAll(Property.build(getGeoBox()));

		// START / END DATES
		properties.add(Property.build("Start Date", DateUtils.getStringDate(getFirstPingDate())));
		properties.add(Property.build("End Date", DateUtils.getStringDate(getLastPingDate())));

		// ELLIPSOID
		properties.add(Property.build("Ellipsoid", getEllipsoid().getEllipsName()));

		// DETECTIONS
		properties.add(Property.build("Swath count", Integer.toString(swathCount)));
		properties.add(Property.build("RxBeam count", Integer.toString(bathyRxBeamCount)));
		properties.add(Property.build("TxBeam count", Integer.toString(txBeamCount)));

		if (getWcRxBeamCount() > 0) {
			Property<String> wc = Property.build("Water column", "");
			properties.add(wc);
			wc.add(Property.build("WC RxBeam count", Long.toString(getWcRxBeamCount())));
		} else {
			Property<String> wc = Property.build("Water column", "No Data");
			properties.add(wc);
		}

		// INSTALLATION ANTENNA PROPERTIES
		Property<String> installation = Property.build("Installation Lever Arm", "");
		properties.add(installation);
		antennas.forEach(antenna -> installation.add(Property.build(antenna)));

		// XSF file attributes
		if (xsfRawAttributes != null) {
			properties.add(xsfRawAttributes);
		}

		// History
		Property<String> historyProp = Property.build("History", "");
		history.forEach(v -> {
			// check if first element of string is an Instant
			Instant historyInstant = null;
			int firstSpaceIndex = v.indexOf(" ");
			if (firstSpaceIndex > 0) {
				try {
					historyInstant = Instant.parse(v.subSequence(0, firstSpaceIndex));
				} catch (DateTimeParseException e) {
					// no instant in first place
				}
			}
			if (historyInstant != null) {
				historyProp.add(Property.build(DateUtils.getDateString(historyInstant) + " "
						+ DateUtils.getTimeString(historyInstant) + " GMT", v.substring(firstSpaceIndex + 1)));
			} else {
				historyProp.add(Property.build("", v));
			}

		});
		properties.add(historyProp);

		// processing status
		Property<String> processingSatusProp = Property.build("Processing status", "");
		processingSatusProp.add(Property.build("Automatic cleaning", XsfUtils.flagToString(automaticCleaning)));
		processingSatusProp.add(Property.build("Manual cleaning", XsfUtils.flagToString(manualCleaning)));
		processingSatusProp.add(Property.build("Velocity correction", XsfUtils.flagToString(velocityCorrection)));
		Property<String> biasCorrectionProp = Property.build("Bias correction", XsfUtils.flagToString(biasCorrection));
		processingSatusProp.add(biasCorrectionProp);
		// correctors
		getBiasCorrectors().forEach(corrector -> biasCorrectionProp
				.add(Property.build(corrector, XsfUtils.flagToString(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON))));

		processingSatusProp.add(Property.build("Tide correction", XsfUtils.flagToString(tideCorrection)));
		processingSatusProp.add(Property.build("Position correction", XsfUtils.flagToString(positionCorrection)));
		processingSatusProp.add(Property.build("Draught correction", XsfUtils.flagToString(draughtCorrection)));
		processingSatusProp.add(Property.build("Backscatter correction", XsfUtils.flagToString(backscatterCorrection)));
		properties.add(processingSatusProp);

		properties.add(Property.build("Available Layers", ""));

		return properties;
	}

	/**
	 *
	 * @return True if XSF file has a supported version
	 */
	public static boolean checkVersion(String xsfVersion) {
		double versionNumber = 0;
		if (xsfVersion != null) {
			xsfVersion = xsfVersion.trim();
			if (!xsfVersion.isEmpty()) {
				versionNumber = Double.valueOf(xsfVersion);
			}
		}
		return versionNumber >= XSF_MIN_SUPPORTED_VERSION;
	}

	@Override
	public ContentType getContentType() {
		return ContentType.XSF_NETCDF_4;
	}

	@Override
	public Optional<ProcessAssessor> getProcessAssessor() {
		return Optional.of(new XsfProcessAssessor(this));
	}

	@Override
	public String getConstructor() {
		return constructor;
	}

	protected void setConstructor(String constructor) {
		this.constructor = constructor;
	}

	protected void setXsfRawAttributes(Property<String> xsfRawAttributes) {
		this.xsfRawAttributes = xsfRawAttributes;
	}

	/** {@inheritDoc} */
	@Override
	public int getBeamGroupCount() {
		// A XSF has only one /Sonar/Beam_group
		return 1;
	}
}
