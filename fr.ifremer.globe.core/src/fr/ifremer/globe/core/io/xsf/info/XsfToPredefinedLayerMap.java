package fr.ifremer.globe.core.io.xsf.info;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.PlatformLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.SoundSpeedProfileLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.SurfaceSoundSpeedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.TideLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.DynamicDraughtLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.attitude.AttitudeSubGroupLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.position.PositionSubGroupLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.VendorSpecificLayers;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;

/**
 * This class binds XSF variables to predefined layers.
 *
 */
public class XsfToPredefinedLayerMap {
	private static final Logger logger = LoggerFactory.getLogger(XsfToPredefinedLayerMap.class);

	private XsfToPredefinedLayerMap() {
	}

	private static final Map<String, PredefinedLayers<?>> map = new HashMap<>();

	private static final String[] sensorGroups = { "Attitude", "Position" };

	/**
	 * @return an optional which contains the predefined layer bound to the specified XSF path, or an empty optional if
	 *         no predefined layer has been found.
	 */
	public static Optional<PredefinedLayers> providePredefinedLayer(NetcdfVariable variable) {
		String variablePath = variable.getPath();

		/*
		 * In XSF files, sensors groups (attitude, position...) are different in function of sensor number and their id.
		 * (it's possible to have several 'position' group for example, if there is several sensors...) To handle all
		 * cases, the predefined layers are pushed in a group whose name is computed from the sensor id.
		 */
		for (String grp : sensorGroups) {
			if (variablePath.contains("/" + grp + "/")) {
				String[] splitedPath = variablePath.split("/");
				int grpIndex = 0;
				while (!splitedPath[grpIndex].contains(grp)) {
					grpIndex++;
				}
				String id = splitedPath[grpIndex + 1];
				String templateSubgroup = grp.toLowerCase() + "_sub_group";
				PredefinedLayers<?> templateLayer = map
						.get(variablePath.replace("/" + id + "/", "/" + templateSubgroup + "/"));
				if (templateLayer != null)
					return Optional.of(
							new PredefinedLayers(templateLayer.getGroup().replace(templateSubgroup, id), templateLayer));
				break;
			}
		}

		return Optional.ofNullable(map.get(variablePath));
	}

	static void fillMap(PredefinedLayers<?> layer) {
		map.put(layer.getGroup() + "/" + layer.getName(), layer);

	}

	static void declareLayers(Class<?> target) {
		Field[] declaredFields = target.getDeclaredFields();
		List<Field> staticFields = new ArrayList<>();
		for (Field field : declaredFields) {
			if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
				staticFields.add(field);
				try {
					if (PredefinedLayers.class.isInstance(field.get(null))) {
						PredefinedLayers<?> layer = (PredefinedLayers<?>) field.get(null);
						fillMap(layer);
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					logger.error("Error while declaring field " + field.getName(), e);
				}
			}
		}
	}

	static {
		// TODO to update with current model
		// Platform (antennas)
		declareLayers(PlatformLayers.class);

		// Position
		declareLayers(PositionSubGroupLayers.class);
		declareLayers(VendorSpecificLayers.class);
		// Attitude
		declareLayers(AttitudeSubGroupLayers.class);
		// Environment
		declareLayers(SurfaceSoundSpeedLayers.class);
		declareLayers(SoundSpeedProfileLayers.class);
		declareLayers(DynamicDraughtLayers.class);
		declareLayers(TideLayers.class);

		// Beam
		declareLayers(BeamGroup1Layers.class);

		declareLayers(BathymetryLayers.class);
	}

}
