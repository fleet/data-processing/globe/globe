/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.xsf.writer;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.ProvenanceGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.core.io.xsf.info.XsfInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.utils.GlobeVersion;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * XsfInfo process assessor
 */
public class XsfProcessAssessor implements ProcessAssessor {

	private static final Logger logger = LoggerFactory.getLogger(XsfProcessAssessor.class);

	/** Managed XsfInfo */
	protected XsfInfo xsfInfo;

	/**
	 * Constructor
	 */
	public XsfProcessAssessor(XsfInfo xsfInfo) {
		this.xsfInfo = xsfInfo;
	}

	@Override
	public void heedBiasCorrection(Optional<String> correctionFileName, List<String> correctorsList) throws GIOException {
		xsfInfo.setBiasCorrection(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON);
		xsfInfo.addBiasCorrectors(correctorsList);
		if (correctionFileName.isPresent() && !correctorsList.isEmpty()) {
			addHistoryLine("Bias correction (ref: "+ FilenameUtils.getName(correctionFileName.get()) + "; correctors: " + correctorsList.toString() + ")");
		} else {
			addHistoryLine("Bias correction");
		}
		writeProperties();
	}

	@Override
	public boolean biasCorrectionEnforced() {
		return xsfInfo.getBiasCorrection() == XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON;
	}

	@Override
	public void heedTideCorrection(Optional<String> correctionFileName, TideType tideType) throws GIOException {
		if (correctionFileName.isPresent()) {
			xsfInfo.setTideCorrection(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON);
			addHistoryLine("Tide correction (type: " + tideType.toString() + "; ref: "
					+ FilenameUtils.getName(correctionFileName.get()) + ")");
		} else {
			xsfInfo.setTideCorrection(XsfConstants.ATT_PROCESSING_STATUS_FLAG_OFF);
			addHistoryLine("Reset tide correction");
		}
		writeProperties();
	}

	@Override
	public boolean tideCorrectionEnforced() {
		return xsfInfo.getTideCorrection() == XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON;
	}

	@Override
	public void heedDraughtCorrection(Optional<String> correctionFileName) throws GIOException {
		if (correctionFileName.isPresent()) {
			xsfInfo.setDraughtCorrection(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON);
			addHistoryLine("Draught correction (ref: " + FilenameUtils.getName(correctionFileName.get()) + ")");
		} else {
			xsfInfo.setDraughtCorrection(XsfConstants.ATT_PROCESSING_STATUS_FLAG_OFF);
			addHistoryLine("Reset draught correction");
		}
		writeProperties();
	}

	@Override
	public boolean draughtCorrectionEnforced() {
		return xsfInfo.getDraughtCorrection() == XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON;
	}

	@Override
	public void heedVelocityCorrection() throws GIOException {
		xsfInfo.setVelocityCorrection(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON);
		addHistoryLine("Velocity correction");
		writeProperties();
	}

	@Override
	public boolean velocityCorrectionEnforced() {
		return xsfInfo.getVelocityCorrection() == XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON;
	}

	@Override
	public void heedAutomaticCleaning() throws GIOException {
		xsfInfo.setAutomaticCleaning(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON);
		addHistoryLine("Automatic cleaning");
		writeProperties();
	}

	@Override
	public boolean automaticCleaningEnforced() {
		return xsfInfo.getAutomaticCleaning() == XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON;
	}

	@Override
	public void heedManualCleaning() throws GIOException {
		xsfInfo.setManualCleaning(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON);
		addHistoryLine("Manual cleaning");
		writeProperties();
	}

	@Override
	public boolean manualCleaningEnforced() {
		return xsfInfo.getManualCleaning() == XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON;
	}

	@Override
	public void heedPositionCorrection() throws GIOException {
		xsfInfo.setPositionCorrection(XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON);
		addHistoryLine("Position correction");
		writeProperties();
	}

	@Override
	public boolean positionCorrectionEnforced() {
		return xsfInfo.getPositionCorrection() == XsfConstants.ATT_PROCESSING_STATUS_FLAG_ON;
	}

	private void addHistoryLine(String processDescription) {
		xsfInfo.getHistory().add( //
				Instant.now().truncatedTo(ChronoUnit.SECONDS).toString() //
						+ " " + processDescription //
						+ " with Globe " + GlobeVersion.VERSION + "/Ifremer");
	}

	/***
	 * write the properties in the file
	 */
	private void writeProperties() throws GIOException {
		try {
			xsfInfo.open(false);
			NetcdfFile ncfile = xsfInfo.getFile();
			ncfile.redef();
			// Write processing status as JSON Object
			String processingStatus = "";
			if (ncfile.hasAttribute(RootGrp.ATT_PROCESSING_STATUS)) {
				processingStatus = ncfile.getAttributeAsString(RootGrp.ATT_PROCESSING_STATUS);
			}
			JsonObject jsonObject = new JsonObject();
			try {
				JsonElement jsonElem = new JsonParser().parse(processingStatus);
				if (jsonElem.isJsonObject()) {
					jsonObject = jsonElem.getAsJsonObject();
				}
			} catch (JsonSyntaxException e) {
				logger.error("error when parsing processing_status : previous status will be overwritten");
			}
			// set automaticCleaning
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_AUTOMATIC_CLEANING,
					xsfInfo.getAutomaticCleaning());
			// set manualCleaning
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_MANUAL_CLEANING, xsfInfo.getManualCleaning());
			// set velocityCorrection
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_VELOCITY_CORRECTION,
					xsfInfo.getVelocityCorrection());
			// get biasCorrection
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_BIAS_CORRECTION, xsfInfo.getBiasCorrection());
			// get tideCorrection
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_TIDE_CORRECTION, xsfInfo.getTideCorrection());
			// get positionCorrection
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_POSITION_CORRECTION,
					xsfInfo.getPositionCorrection());
			// get soundingsCorrection
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_DRAUGHT_CORRECTION,
					xsfInfo.getDraughtCorrection());
			// get soundingsCorrection
			jsonObject.addProperty(XsfConstants.ATT_PROCESSING_STATUS_BACKSCATTER_CORRECTION,
					xsfInfo.getBackscatterCorrection());
			// get biasCorrectors
			List<String> biasCorrectors = xsfInfo.getBiasCorrectors();
			if(!biasCorrectors.isEmpty()) {
				JsonArray jsonArray = new JsonArray();
				biasCorrectors.forEach(jsonArray::add);
				jsonObject.add(XsfConstants.ATT_PROCESSING_STATUS_BIAS_CORRECTORS, jsonArray);
			}

			ncfile.deleteAttribute(RootGrp.ATT_PROCESSING_STATUS);
			ncfile.addAttribute(RootGrp.ATT_PROCESSING_STATUS, jsonObject.toString());
			ncfile.enddef();

			// Write history
			NetcdfGroup provenance = ncfile.getGroup(ProvenanceGrp.GROUP_NAME);
			provenance.deleteAttribute(ProvenanceGrp.ATT_HISTORY);

			String[] values = xsfInfo.getHistory().toArray(String[]::new);
			provenance.addAttributeTextArray(ProvenanceGrp.ATT_HISTORY,values);

			ncfile.synchronize();

		} catch (NCException e) {
			throw new GIOException("write properties error : " + e.getMessage(), e);
		} finally {
			xsfInfo.close();
		}
	}

}
