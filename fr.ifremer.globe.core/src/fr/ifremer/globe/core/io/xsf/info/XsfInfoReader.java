package fr.ifremer.globe.core.io.xsf.info;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.jobs.JobGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.AttitudeGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PlatformGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PositionGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.ProvenanceGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters.Type;
import fr.ifremer.globe.core.model.sounder.SensorInstallationParameters;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderNavigationData;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.PlatformLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to read an XSF file.
 */
public class XsfInfoReader {

	private final Logger logger = LoggerFactory.getLogger(XsfInfoReader.class);

	/** Group of all GProcess launched to update properties of XSFInfo. Only one GProcess at a time */
	private JobGroup statisticsJobGroup = new JobGroup("Computing statistics jobs", 1, 0);

	/**
	 * Reads file to get needed attributes
	 */
	public void readAttributes(XsfInfo info) throws GIOException {
		IDataContainerFactory containerFactory = IDataContainerFactory.grab();
		IDataContainerOwner owner = IDataContainerOwner.generate(getClass().getName(), true);
		try (ISounderDataContainerToken dataContainerToken = containerFactory.book(info, owner)) {
			readVersion(info);
			readSounderModel(info);
			readConstructor(info);
			readSwathCount(info);
			readBeamCount(info, dataContainerToken.getDataContainer());
			readGeobox(info, dataContainerToken.getDataContainer());
			readFirstLastDate(info, dataContainerToken.getDataContainer());
			readWcDims(info);
			readXsfRawAttributes(info);
			readProcessingStatus(info);
			readAntennaProperties(info, dataContainerToken.getDataContainer());
			readSensorProperties(info, dataContainerToken.getDataContainer());
			readHistory(info);
			readPlatformGroupAttributes(info, dataContainerToken.getDataContainer());
		}
	}

	/**********************************************************************************************************
	 * Attribute readers
	 **********************************************************************************************************/

	/** Reads XSF version **/
	private void readVersion(XsfInfo info) throws GIOException {
		try {
			if (info.getFile().hasAttribute(XsfConstants.ATT_VERSION)) {
				String version = info.getFile().getAttributeAsString(XsfConstants.ATT_VERSION);
				if (!XsfInfo.checkVersion(version)) {
					throw new GIOException("Bad XSF version : " + version);
				}
				info.setXsfVersion(version);
			} else {
				throw new GIOException("Not a XSF file");
			}
		} catch (NCException e) {
			info.setXsfVersion("not found");
			logger.error("XSFInfo loading: error with xsf version: " + e.getMessage(), e);
		}
	}

	/** Reads sounder model **/
	private void readSounderModel(XsfInfo info) {
		try {
			NetcdfGroup sonar = info.getFile().getGroup(SonarGrp.GROUP_NAME);
			if (sonar == null)
				return;
			String name = sonar.getAttributeAsString(SonarGrp.ATT_SONAR_MODEL);
			if (name != null && !name.isBlank() && !name.isEmpty()) {
				info.setEchoSounderModel(name);
			}
		} catch (Exception e) {
			logger.error("XSFInfo loading: error with sounder model: " + e.getMessage(), e);
		}
	}

	/** Reads constructor **/
	private void readConstructor(XsfInfo info) {
		try {
			NetcdfGroup sonar = info.getFile().getGroup(SonarGrp.GROUP_NAME);
			if (sonar == null)
				return;
			String name = sonar.getAttributeAsString(SonarGrp.ATT_SONAR_MANUFACTURER);
			if (name != null && !name.isBlank() && !name.isEmpty()) {
				info.setConstructor(name);
			}
		} catch (Exception e) {
			logger.error("XSFInfo while retrieving SonarGrp.ATT_SONAR_MANUFACTURER " + e.getMessage(), e);
		}
	}

	/** Reads swath count **/
	private void readSwathCount(XsfInfo info) {
		try {
			info.setCycleCount((int) info.getFile()
					.getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.PING_TIME_DIM_NAME).getLength());
		} catch (NCException e) {
			info.setCycleCount(-1);
			logger.error("XSFInfo loading: error with swath count: " + e.getMessage(), e);
		}
	}

	/** Reads swath count **/
	private void readBeamCount(XsfInfo info, SounderDataContainer dataContainer) {
		// RX
		try {
			if (dataContainer.hasLayer(BathymetryLayers.DETECTION_X)) {
				info.setTotalRxBeamCount((int) info.getFile()
						.getDimension(BathymetryLayers.GROUP + "/" + BathymetryGrp.DETECTION_DIM_NAME).getLength());
			} else {
				info.setTotalRxBeamCount(0);
			}
		} catch (NCException e) {
			info.setTotalRxBeamCount(0);
			logger.error("XSFInfo loading: error with rxBeam count: " + e.getMessage(), e);
		}
		// TX
		try {
			info.setTotalTxBeamCount((int) info.getFile()
					.getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.TX_BEAM_DIM_NAME).getLength());
		} catch (NCException e) {
			info.setTotalTxBeamCount(0);
			logger.error("XSFInfo loading: error with txBeam count: " + e.getMessage(), e);
		}
	}

	/**
	 * Reads Geobox from the navigation data. Navigation data is obtained using only valid swaths.
	 * 
	 * @throws GIOException
	 **/
	private void readGeobox(XsfInfo info, SounderDataContainer dataContainer) throws GIOException {
		// we need to compute a geobox. we're gonna take it from the navigation data
		@SuppressWarnings("resource") // closed in caller
		SounderNavigationData navData = new SounderNavigationData(dataContainer);
		info.setGeoBox(navData.computeGeoBox());
	}

	/**
	 * Reads first and last swath date
	 * 
	 * @throws GIOException
	 **/
	private void readFirstLastDate(XsfInfo info, SounderDataContainer dataContainer) throws GIOException {
		try {
			LongLoadableLayer1D swathTimeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);
			int swathCount = info.getCycleCount();
			long minTime = swathTimeLayer.get(0);
			long maxTime = swathTimeLayer.get(swathCount - 1);
			info.setFirstSwathDate(new Date(DateUtils.nanoSecondToMilli(minTime)));
			info.setLastSwathDate(new Date(DateUtils.nanoSecondToMilli(maxTime)));
		} catch (DateTimeParseException e) {
			logger.error("XSFInfo loading: error with swath dates: " + e.getMessage(), e);
		}
	}

	/**
	 * If the RasterInfo is not yet fully loaded, open the file with Gdal and computes the statistics of the raster
	 */
	public void updateProperties(XsfInfo info) {
		GProcess statisticsJob = IProcessService.grab().createProcess("Computing statistics for " + info.getFilename(),
				IProcessFunction.with(() -> {
					logger.debug("Computing statistics for {}", info.getFilename());
					IDataContainerFactory containerFactory = IDataContainerFactory.grab();
					IDataContainerOwner owner = IDataContainerOwner.generate(getClass().getName(), true);
					try (ISounderDataContainerToken dataContainerToken = containerFactory.book(info, owner)) {
						SounderDataContainer dataContainer = dataContainerToken.getDataContainer();
						updateMinMaxDepth(info, dataContainer);
					} catch (GIOException e) {
						logger.warn("Error while computing properties of XSF " + info.getFilename(), e);
					}
				}));

		statisticsJob.setUser(false);
		statisticsJob.setJobGroup(statisticsJobGroup);
		statisticsJob.schedule(10L);
	}

	/**
	 * Open the XSF file, compute some statistics and update the properties of the specified XSF info.S
	 **/
	private void updateMinMaxDepth(XsfInfo info, SounderDataContainer dataContainer) throws GIOException {
		DoubleSummaryStatistics depthStat = new DoubleSummaryStatistics();
		try {
			DoubleLayer2D depthLayer = dataContainer.getCsLayers(CoordinateSystem.FCS).getProjectedZ();
			DetectionStatusLayer statusLayer = dataContainer.getStatusLayer();
			int swathCount = info.getCycleCount();
			int beamCount = info.getTotalRxBeamCount();
			for (int swathIdx = 0; swathIdx < swathCount; swathIdx++) {
				for (int beamIdx = 0; beamIdx < beamCount; beamIdx++) {
					if (statusLayer.isValid(swathIdx, beamIdx)) {
						double depth = depthLayer.get(swathIdx, beamIdx);
						if (Double.isFinite(depth))
							depthStat.accept(depthLayer.get(swathIdx, beamIdx));
					}
				}
			}
		} finally {
			info.setMinMaxDepth((float) depthStat.getMin(), (float) depthStat.getMax());
		}
	}

	/** Reads antennas **/
	private void readAntennaProperties(XsfInfo info, SounderDataContainer dataContainer) {
		List<AntennaInstallationParameters> allAntennas = info.getAntennaParameters();
		allAntennas.clear();
		try {
			int antennaCount = (int) info.getFile()
					.getDimension(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.TRANSDUCER_DIM_NAME).getLength();

			ByteLayer1D type = dataContainer.getLayer(PlatformLayers.TRANSDUCER_FUNCTION);
			FloatLayer1D x = dataContainer.getLayer(PlatformLayers.TRANSDUCER_OFFSET_X);
			FloatLayer1D y = dataContainer.getLayer(PlatformLayers.TRANSDUCER_OFFSET_Y);
			FloatLayer1D z = dataContainer.getLayer(PlatformLayers.TRANSDUCER_OFFSET_Z);
			FloatLayer1D xRotation = dataContainer.getLayer(PlatformLayers.TRANSDUCER_ROTATION_X);
			FloatLayer1D yRotation = dataContainer.getLayer(PlatformLayers.TRANSDUCER_ROTATION_Y);
			FloatLayer1D zRotation = dataContainer.getLayer(PlatformLayers.TRANSDUCER_ROTATION_Z);

			String[] antennaIds = null;
			NCVariable antennaVar = info.getFile()
					.getVariable(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.TRANSDUCER_IDS);
			if (antennaVar != null) {
				antennaIds = antennaVar.get_string(new long[] { 0 }, new long[] { antennaCount });
			}
			for (int i = 0; i < antennaCount; i++) {
				// Transducer serial number OR identification name => may be not be an integer
				var id = Integer.toString(i);
				try {
					id = antennaIds != null ? antennaIds[i] : Integer.toString(i);
				} catch (NumberFormatException e) {
				}

				var antennaType = Type.UNKNOWN;
				if (type.get(i) == (byte) 0)
					antennaType = Type.RX;
				else if (type.get(i) == (byte) 1)
					antennaType = Type.TX;
				else if (type.get(i) == (byte) 3)
					antennaType = Type.TXRX;

				var antenna = new AntennaInstallationParameters(//
						antennaType, id, //
						x.get(i), y.get(i), z.get(i), //
						xRotation.get(i), yRotation.get(i), zRotation.get(i));
				allAntennas.add(antenna);

				if (antenna.type == Type.UNKNOWN) {
					throw new GIOException("Unknown antenna type : id = " + id);
				}
			}
		} catch (GException e) {
			logger.error("XSFInfo loading: error antennas: " + e.getMessage(), e);
		}
	}

	/** Reads antennas **/
	private void readSensorProperties(XsfInfo info, SounderDataContainer dataContainer) {
		// MRU SENSORS
		List<SensorInstallationParameters> allMrus = info.getAttitudeInstallationParameters();
		allMrus.clear();
		try {
			int mruCount = (int) info.getFile().getDimension(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.MRU_DIM_NAME)
					.getLength();

			FloatLayer1D x = dataContainer.getLayer(PlatformLayers.MRU_OFFSET_X);
			FloatLayer1D y = dataContainer.getLayer(PlatformLayers.MRU_OFFSET_Y);
			FloatLayer1D z = dataContainer.getLayer(PlatformLayers.MRU_OFFSET_Z);
			FloatLayer1D xRotation = dataContainer.getLayer(PlatformLayers.MRU_ROTATION_X);
			FloatLayer1D yRotation = dataContainer.getLayer(PlatformLayers.MRU_ROTATION_Y);
			FloatLayer1D zRotation = dataContainer.getLayer(PlatformLayers.MRU_ROTATION_Z);

			String[] mruIds = null;
			NCVariable mruVar = info.getFile().getVariable(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.MRU_IDS);
			if (mruVar != null) {
				mruIds = mruVar.get_string(new long[] { 0 }, new long[] { mruCount });
			}
			for (int i = 0; i < mruCount; i++) {
				// Transducer serial number OR identification name => may be not be an integer
				var id = Integer.toString(i);
				try {
					id = mruIds != null ? mruIds[i] : Integer.toString(i);
				} catch (NumberFormatException e) {
				}
				var mru = new SensorInstallationParameters(//
						id, //
						x.get(i), y.get(i), z.get(i), //
						xRotation.get(i), yRotation.get(i), zRotation.get(i));
				allMrus.add(mru);
			}
		} catch (GException e) {
			logger.error("XSFInfo loading: error reading mrus: " + e.getMessage(), e);
		}
	}

	/** Reads WC beam / sample count **/
	private void readWcDims(XsfInfo info) {
		try {
			if (info.getFile().getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.BEAM_DIM_NAME) != null
					&& info.getFile().getVariable(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.BACKSCATTER_R) != null) {
				info.setWcRxBeamCount(info.getFile()
						.getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.BEAM_DIM_NAME).getLength());
			}
		} catch (NCException e) {
			logger.error("XSFInfo loading: error parsing WC dimensions: " + e.getMessage(), e);
		}
	}

	/** Reads XSF group attributes **/
	private void readXsfRawAttributes(XsfInfo info) {
		try {
			Property<String> xsfRawAttributes = Property.build("XSF File raw properties", "");
			info.getFile().getGlobalAttributs().forEach((k, v) -> xsfRawAttributes.add(Property.build(k, v)));
			info.setXsfRawAttributes(xsfRawAttributes);
		} catch (NCException e) {
			logger.error("Error during XSF file attributs retrievement: " + e.getMessage(), e);
		}
	}

	/** Reads XSF root group processing_status **/
	private void readProcessingStatus(XsfInfo info) {
		try {
			if (info.getFile().hasAttribute(RootGrp.ATT_PROCESSING_STATUS)) {
				String processingStatus = info.getFile().getAttributeAsString(RootGrp.ATT_PROCESSING_STATUS);
				// parse processing_status attribute as a JSON object
				JsonElement jsonElem = JsonParser.parseString(processingStatus);
				if (jsonElem.isJsonObject()) {
					JsonObject jsonObject = jsonElem.getAsJsonObject();

					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_AUTOMATIC_CLEANING))
							.ifPresent(elem -> info.setAutomaticCleaning(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_MANUAL_CLEANING))
							.ifPresent(elem -> info.setManualCleaning(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_VELOCITY_CORRECTION))
							.ifPresent(elem -> info.setVelocityCorrection(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_BIAS_CORRECTION))
							.ifPresent(elem -> info.setBiasCorrection(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_TIDE_CORRECTION))
							.ifPresent(elem -> info.setTideCorrection(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_POSITION_CORRECTION))
							.ifPresent(elem -> info.setPositionCorrection(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_DRAUGHT_CORRECTION))
							.ifPresent(elem -> info.setDraughtCorrection(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_BACKSCATTER_CORRECTION))
							.ifPresent(elem -> info.setBackscatterCorrection(elem.getAsByte()));
					Optional.ofNullable(jsonObject.get(XsfConstants.ATT_PROCESSING_STATUS_BIAS_CORRECTORS))
							.ifPresent(elem -> info.setBiasCorrectors(
									elem.getAsJsonArray().asList().stream().map(JsonElement::getAsString).toList()));
				}
			}
		} catch (NCException | JsonSyntaxException e) {
			logger.error("XSFInfo loading: error with xsf processing status: " + e.getMessage(), e);
		}
	}

	/** Reads History **/
	private void readHistory(XsfInfo info) {
		try {
			NetcdfGroup provenance = info.getFile().getGroup(ProvenanceGrp.GROUP_NAME);
			if (provenance != null) {
				String[] historyArray = provenance.getAttributeTextArray(ProvenanceGrp.ATT_HISTORY);
				if (historyArray != null) {
					info.history = new ArrayList<>(Arrays.asList(historyArray));
				}
			}
		} catch (NCException e) {
			logger.error("XSFInfo loading: error parsing history: " + e.getMessage(), e);
		}
	}

	/**
	 * Reads BeamGroup attributes
	 * 
	 * @throws GIOException
	 **/
	private void readPlatformGroupAttributes(XsfInfo info, SounderDataContainer dataContainer) throws GIOException {
		SonarBeamGroup beamGroup = dataContainer.getBeamGroup();
		try {
			if (info.getFile().hasAttribute(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_MRU)) {
				int mruIndex = info.getFile()
						.getAttributeInt(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_MRU);
				NCVariable mruVar = info.getFile().getVariable(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.MRU_IDS);
				if (mruVar != null) {
					String[] mruIds = mruVar.get_string(new long[] { mruIndex }, new long[] { 1 });
					// check group validity
					if (info.getFile().getGroup(
							PlatformGrp.GROUP_NAME + "/" + AttitudeGrp.GROUP_NAME + "/" + mruIds[0]) != null) {
						info.setPreferedAttitudeGroupId(mruIds[0]);
						info.setPreferedAttitudeGroupIndex(mruIndex);
					}
				}
			}

		} catch (NCException e) {
			// warn only to be able to pass tests with old corrupted files
			logger.warn("XSFInfo loading: error parsing beamGroup prefered MRU: " + e.getMessage(), e);
		}

		// fill preferred attitude group with first group if not found
		if (info.getPreferedAttitudeGroupId().isEmpty()) {
			try {
				NetcdfGroup attitudeGroup = info.getFile()
						.getGroup(PlatformGrp.GROUP_NAME + "/" + AttitudeGrp.GROUP_NAME);
				info.setPreferedAttitudeGroupId(attitudeGroup.getGroups().get(0).getName());
				info.setPreferedAttitudeGroupIndex(0);
			} catch (NCException e) {
				logger.warn("XSFInfo loading: error parsing first attitude subgroup: " + e.getMessage(), e);
			}

		}

		try {
			if (info.getFile().hasAttribute(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_POSITION)) {
				int posIndex = info.getFile()
						.getAttributeInt(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_POSITION);
				NCVariable posVar = info.getFile().getVariable(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.POSITION_IDS);
				if (posVar != null) {
					String[] posIds = posVar.get_string(new long[] { posIndex }, new long[] { 1 });
					info.setPreferedPositionGroupId(posIds[0]);
					// check group validity
					if (info.getFile()
							.getGroup(PlatformGrp.GROUP_NAME + "/" + PositionGrp.GROUP_NAME + "/" + posIds[0]) != null)
						info.setPreferedPositionGroupId(posIds[0]);
				}
			}
		} catch (NCException e) {
			// warn only to be able to pass tests with old corrupted files
			logger.warn("XSFInfo loading: error parsing beamGroup prefered Position: " + e.getMessage(), e);
		}

		// fill preferred position group with first group if not found
		if (info.getPreferedPositionGroupId().isEmpty()) {
			try {
				NetcdfGroup positionGroup = info.getFile()
						.getGroup(PlatformGrp.GROUP_NAME + "/" + PositionGrp.GROUP_NAME);
				info.setPreferedPositionGroupId(positionGroup.getGroups().get(0).getName());
			} catch (NCException e) {
				logger.warn("XSFInfo loading: error parsing first position subgroup: " + e.getMessage(), e);
			}
		}
	}
}
