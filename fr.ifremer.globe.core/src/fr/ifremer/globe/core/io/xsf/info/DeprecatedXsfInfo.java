/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.xsf.info;

import java.util.List;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * DeprecatedXsfInfo: this class provides informations about an old XSF file.
 */
public class DeprecatedXsfInfo extends BasicFileInfo {

	private String version;

	/** Constructor */
	public DeprecatedXsfInfo(String filePath) {
		super(filePath);
		try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readonly)) {
			if (file.hasAttribute(XsfConstants.ATT_VERSION)) {
				this.version = file.getAttributeAsString(XsfConstants.ATT_VERSION);
			}
		} catch (NCException e) {
			// Ignored
		}
	}

	String getXsfVersion() {
		return version;
	}

	@Override
	public List<Property<?>> getProperties() {

		// BASIC FILE PROPERTIES
		List<Property<?>> properties = super.getProperties();

		// XSF FILE PROPERTIES
		properties.add(Property.build("Xsf version", getXsfVersion() + " (deprecated : please regenerate)"));
		return properties;
	}

	@Override
	public ContentType getContentType() {
		return ContentType.OLD_XSF_NETCDF_4;
	}
}
