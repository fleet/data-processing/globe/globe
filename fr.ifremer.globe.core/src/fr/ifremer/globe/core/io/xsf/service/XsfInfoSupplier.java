/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.xsf.service;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.core.io.xsf.info.XsfInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfoSupplier;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * Service providing informations on XSF files
 */
@Component(name = "globe_drivers_xsf_file_info_supplier", service = { IFileInfoSupplier.class,
		ISounderNcInfoSupplier.class, INavigationSupplier.class, XsfInfoSupplier.class })
public class XsfInfoSupplier
		implements IFileInfoSupplier<XsfInfo>, ISounderNcInfoSupplier<XsfInfo>, INavigationSupplier {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(XsfInfoSupplier.class);

	/** Supported XSF type */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.XSF_NETCDF_4);

	/** Extensions **/
	public static final String EXTENSION1 = "nc";
	public static final String EXTENSION2 = "xsf";
	protected static final List<String> EXTENSIONS = Arrays.asList(EXTENSION1, EXTENSION2);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<XsfInfo> getFileInfo(String filePath) {
		XsfInfo result = null;
		try {
			if (isXsfFile(filePath)) {
				result = new XsfInfo(filePath);
			}
		} catch (Exception e) {
			// Not a Xsf file or bad version
			logger.warn("XSF file not readable (maybe is depreacted (format under development)) : {}", filePath, e);
		}
		return Optional.ofNullable(result);
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return CONTENT_TYPES;
	}

	@Override
	public List<String> getExtensions() {
		return EXTENSIONS;
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Arrays.asList(//
				new Pair<>("Xsf (*." + EXTENSION1 + ", *." + EXTENSION2 + ")", "*." + EXTENSION1 + ";*." + EXTENSION2));
	}

	/**
	 * @return true if filePath point to a xsf file
	 */
	public boolean isXsfFile(String filePath) {
		boolean isNetcdf = EXTENSIONS.contains(FilenameUtils.getExtension(filePath).toLowerCase());
		if (isNetcdf) {
			try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readonly)) {
				// A XSF file must have a /Sonar/Beam_group1/Bathymetry group unlike a Sonar Netcdf file
				return file.hasAttribute(XsfConstants.ATT_VERSION) && file.getGroup(BathymetryLayers.GROUP) != null;
			} catch (NCException e) {
				logger.debug("Open file error : {}", e.getMessage());
			}
		}
		return false;
	}

	@Override
	public Optional<XsfInfo> getSounderNcInfo(String filePath) {
		return getFileInfo(filePath);
	}

	@Override
	public Optional<XsfInfo> getSounderNcInfo(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof XsfInfo ? (XsfInfo) fileInfo : null);
	}

	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof XsfInfo ? (XsfInfo) fileInfo : null);
	}

}