/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.xsf.service;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfConstants;
import fr.ifremer.globe.core.io.xsf.info.DeprecatedXsfInfo;
import fr.ifremer.globe.core.io.xsf.info.XsfInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * Service providing informations on XSF files
 */
@Component(name = "globe_drivers_deprecated_xsf_file_info_supplier", service = { IFileInfoSupplier.class,
		DeprecatedXsfInfoSupplier.class })
public class DeprecatedXsfInfoSupplier
		implements IFileInfoSupplier<DeprecatedXsfInfo> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(DeprecatedXsfInfoSupplier.class);

	/** Supported XSF type */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.OLD_XSF_NETCDF_4);

	/** Extensions **/
	public static final String EXTENSION1 = "nc";
	public static final String EXTENSION2 = "xsf";
	protected static final List<String> EXTENSIONS = Arrays.asList(EXTENSION1, EXTENSION2);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<DeprecatedXsfInfo> getFileInfo(String filePath) {
		DeprecatedXsfInfo result = null;
		try {
			if (isDeprecatedXsfFile(filePath)) {
				result = new DeprecatedXsfInfo(filePath);
			}
		} catch (Exception e) {
			// Not a Xsf file or bad version
			logger.warn("XSF file not readable(format under development) : {}", filePath, e);
		}
		return Optional.ofNullable(result);
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return CONTENT_TYPES;
	}

	@Override
	public List<String> getExtensions() {
		return EXTENSIONS;
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Arrays.asList(//
				new Pair<>("Xsf (*." + EXTENSION1 + ", *." + EXTENSION2 + ")", "*." + EXTENSION1 + ";*." + EXTENSION2));
	}

	/**
	 * @return true if filePath point to a xsf file
	 */
	public boolean isDeprecatedXsfFile(String filePath) {
		boolean isNetcdf = EXTENSIONS.contains(FilenameUtils.getExtension(filePath).toLowerCase());
		if (isNetcdf) {
			try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readonly)) {
				// A XSF file must have a /Sonar/Beam_group1/Bathymetry group unlike a Sonar Netcdf file
				boolean isXsfFile = file.hasAttribute(XsfConstants.ATT_VERSION)
						&& file.getGroup(BathymetryLayers.GROUP) != null;
				if (isXsfFile) {
					String version = file.getAttributeAsString(XsfConstants.ATT_VERSION);
					return !XsfInfo.checkVersion(version);
				}
			} catch (NCException e) {
				// Ignored
			}
		}
		return false;
	}

}