package fr.ifremer.globe.core.io.g3d;

import java.util.Optional;

/**
 * This class defines a data layer in a G3D file.
 */
public class G3dDataLayer {

	public final String name;
	public final String displayName;
	public final Optional<Float> contrastMin;
	public final Optional<Float> contrastMax;
	public final Optional<String> colorName;

	public G3dDataLayer(String name, Optional<String> displayName, Optional<Float> contrastMin,
			Optional<Float> contrastMax, Optional<String> colorName) {
		super();
		this.name = name;
		this.displayName = displayName.orElse(name);
		this.contrastMin = contrastMin;
		this.contrastMax = contrastMax;
		this.colorName = colorName;
	}

}
