/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.g3d.info;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.io.g3d.G3dConstants;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * Service providing informations on Globe 3D NetCDF files.
 */
@Component(//
		name = "globe_drivers_g3d_file_info_supplier", //
		service = { IFileInfoSupplier.class, G3dInfoSupplier.class }, //
		property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer=" + FileInfoSupplierPriority.G3D_PRIORITY })
public class G3dInfoSupplier implements IFileInfoSupplier<G3dInfo> {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(G3dInfoSupplier.class);

	/** Supported GDF type */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.G3D_NETCDF_4);

	/** Extensions **/
	public static final String EXTENSION1 = "nc";
	protected static final List<String> EXTENSIONS = Arrays.asList(EXTENSION1);

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("resource") // the return G3dInfo is closeable
	@Override
	public Optional<G3dInfo> getFileInfo(String filePath) {
		if (filePath.endsWith(".g3d.nc")) {
			NetcdfFile file = null;
			try {
				file = NetcdfFile.open(filePath, Mode.readonly);
				if (file.hasAttribute(G3dConstants.DATASET_TYPE_ATT))
					return Optional.of(new G3dInfo(file));
				else {
					file.close();
				}
			} catch (NCException e) {
				LOGGER.error("Error while opening file {} : {}", filePath, e.getMessage(), e);
				if (file != null)
					file.close();
			}
		}
		return Optional.empty();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EnumSet<ContentType> getContentTypes() {
		return CONTENT_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getExtensions() {
		return EXTENSIONS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Arrays.asList(new Pair<>("Gdf (*." + EXTENSION1 + ")", "*." + EXTENSION1));
	}
}