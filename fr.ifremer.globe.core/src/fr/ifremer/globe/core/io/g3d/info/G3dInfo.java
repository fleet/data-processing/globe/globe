package fr.ifremer.globe.core.io.g3d.info;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.io.g3d.G3dConstants;
import fr.ifremer.globe.core.io.g3d.G3dDataLayer;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Info about Globe 3D NetCDF files (.g3d.nc).
 */
public class G3dInfo implements IFileInfo, AutoCloseable {

	/** NetCDF file **/
	private final NetcdfFile netcdfFile;

	/** Dataset type (Fly texture, ground texture or point cloud) **/
	private String datasetType;

	/** Slice number **/
	private int sliceCount;

	/** List of {@link G3dDataLayer} **/
	private final List<G3dDataLayer> dataLayers = new ArrayList<>();

	/** History field **/
	private String history;

	/** Computed {@link GeoBox} **/
	private GeoBox geoBox;

	/** List of date grab of each netcdf group */
	private List<Instant> netcdfGroupDates = List.of();

	/**
	 * Constructor
	 */
	public G3dInfo(NetcdfFile netcdfFile) throws NCException {
		this.netcdfFile = netcdfFile;
		load();
	}

	@Override
	public void close() {
		netcdfFile.close();
	}

	/**
	 * Opens and reads the file to retrieve needed information.
	 */
	private void load() throws NCException {
		// type
		datasetType = netcdfFile.getAttributeText(G3dConstants.DATASET_TYPE_ATT);

		// slice number
		netcdfFile.getAttributeText(G3dConstants.DATASET_TYPE_ATT);

		if (netcdfFile.hasAttribute(G3dConstants.SLICE_COUNT)) {
			sliceCount = netcdfFile.getAttributeInt(G3dConstants.SLICE_COUNT);
		} else {
			sliceCount = netcdfFile.getGroups().size();
		}
		// datalayer variables & names
		int datalayerCount = (int) netcdfFile.getDimension(G3dConstants.DATALAYER_COUNT_DIM).getLength();
		NetcdfVariable datalayerNameVar = netcdfFile.getVariable(G3dConstants.DATALAYER_VARIABLE_NAME_VAR);

		// optional variables (display...)
		Optional<NetcdfVariable> displayNameVar = Optional
				.ofNullable(netcdfFile.getVariable(G3dConstants.DATALAYER_DISPLAY_NAME_VAR));
		Optional<NetcdfVariable> contrastMinVar = Optional
				.ofNullable(netcdfFile.getVariable(G3dConstants.DATALAYER_CONTRAST_MIN_VAR));
		Optional<NetcdfVariable> contrastMaxVar = Optional
				.ofNullable(netcdfFile.getVariable(G3dConstants.DATALAYER_CONTRAST_MAX_VAR));
		Optional<NetcdfVariable> colorPaletteVar = Optional
				.ofNullable(netcdfFile.getVariable(G3dConstants.DATALAYER_COLOR_PALETTE_VAR));

		for (int i = 0; i < datalayerCount; i++) {
			String name = getStringFromNcVariable(datalayerNameVar, i);

			// mask layer is ignored
			if (name.equals("mask")) {
				continue;
			}

			// optional parameters for each datalayer
			Optional<String> optDisplayName = displayNameVar.isPresent()
					? Optional.of(getStringFromNcVariable(displayNameVar.get(), i))
					: Optional.empty();

			Optional<Float> optContrastMin = contrastMinVar.isPresent()
					? Optional.of(contrastMinVar.get().get_float(new long[] { i }, new long[] { 1 })[0])
					: Optional.empty();

			Optional<Float> optContrastMax = contrastMaxVar.isPresent()
					? Optional.of(contrastMaxVar.get().get_float(new long[] { i }, new long[] { 1 })[0])
					: Optional.empty();

			Optional<String> optColorPalette = colorPaletteVar.isPresent()
					? Optional.of(getStringFromNcVariable(colorPaletteVar.get(), i))
					: Optional.empty();

			dataLayers.add(new G3dDataLayer(name, optDisplayName, optContrastMin, optContrastMax, optColorPalette));
		}

		// history
		history = netcdfFile.getAttributeText(G3dConstants.HISTORY_ATT);
	}

	/**
	 * Get {@link String} from {@link NetcdfVariable} (handles String variable and char array (Sonar Scope))
	 */
	private String getStringFromNcVariable(NetcdfVariable var, int index) throws NCException {
		long[] datalayerDisplayNamesDims = var.getDimensions();
		// variable type = string
		if (datalayerDisplayNamesDims.length == 1) {
			return var.get_string(new long[] { index }, new long[] { 1 })[0];
		} else {
			long charArrayLength = datalayerDisplayNamesDims[1];
			byte[] charBytes = var.get_byte(new long[] { index, 0 }, new long[] { 1, charArrayLength });
			return new String(charBytes, StandardCharsets.UTF_8).trim();
		}
	}

	/**
	 * Provides properties displayed in "Properties" view.
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new ArrayList<>();
		result.add(Property.build("Dataset type", datasetType));
		result.add(Property.build("Slices", sliceCount));
		result.add(
				Property.build("Datalayers", dataLayers.stream().map(properties -> properties.displayName).toList()));

		Optional<Pair<Instant, Instant>> startEndDate = getStartEndDate();
		if (startEndDate.isPresent()) {
			result.add(Property.build("Start Date", DateUtils.getDateTimeString(startEndDate.get().getFirst())));
			result.add(Property.build("End Date", DateUtils.getDateTimeString(startEndDate.get().getSecond())));
		}

		result.addAll(Property.build(geoBox));
		result.add(Property.build("History", history));

		return result;
	}

	//// GETTERS & SETTERS

	@Override
	public String getPath() {
		return netcdfFile.getName();
	}

	@Override
	public ContentType getContentType() {
		return ContentType.G3D_NETCDF_4; // useful? Can't be retrieved from info Supplier ?
	}

	public String getDatasetType() {
		return datasetType;
	}

	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	public void setGeoBox(GeoBox geobox) {
		geoBox = geobox;
	}

	public int getSliceNumber() {
		return sliceCount;
	}

	public void setSliceNumber(int sliceNumber) {
		this.sliceCount = sliceNumber;
	}

	public List<G3dDataLayer> getDataLayers() {
		return dataLayers;
	}

	public NetcdfGroup getNetcdfFile() {
		return netcdfFile;
	}

	/** @return true when this G3D contains Polar Echograms */
	public boolean containsPolarEchograms() {
		// TODO Find a better way to determine the contains....
		return getFilename().contains("PolarEchograms");
	}

	/**
	 * @return the {@link #netcdfGroupDates}
	 */
	public List<Instant> getNetcdfGroupDates() {
		return netcdfGroupDates;
	}

	/**
	 * @param netcdfGroupDates the {@link #netcdfGroupDates} to set
	 */
	public void setNetcdfGroupDates(List<Instant> netcdfGroupDates) {
		this.netcdfGroupDates = netcdfGroupDates;
	}

	/**
	 * @return start/end dates if exist for the file.
	 */
	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		if (netcdfGroupDates.isEmpty())
			return Optional.empty();
		return Optional.of(Pair.of(netcdfGroupDates.get(0), netcdfGroupDates.get(netcdfGroupDates.size() - 1)));
	}

}
