package fr.ifremer.globe.core.io.g3d;

/**
 * Globe 3D netCDF constants
 */
public class G3dConstants {

	private G3dConstants() {
		// Constants class
	}

	/** NetCDF attribute names **/
	public static final String DATASET_TYPE_ATT = "dataset_type";
	public static final String DATASET_TYPE_FLY_TEXTURE = "FlyTexture";
	public static final String DATASET_TYPE_ELEVATION_MAPPED_TEXTURE = "ElevationMappedTexture";

	public static final String HISTORY_ATT = "history";
	public static final String SLICE_COUNT = "slice_count";
	public static final String VERTICAL_OFFSET_ATT = "vertical_offset";
	public static final String UNITS_ATT = "units";
	public static final String NAME_ATT = "long_name";
	public static final String DATE_ATT = "time";

	/** NetCDF variable names **/
	public static final String DATALAYER_VARIABLE_NAME_VAR = "datalayer_variable_name";
	public static final String DATALAYER_DISPLAY_NAME_VAR = "datalayer_display_name";
	public static final String DATALAYER_CONTRAST_MIN_VAR = "datalayer_contrast_min";
	public static final String DATALAYER_CONTRAST_MAX_VAR = "datalayer_contrast_max";
	public static final String DATALAYER_COLOR_PALETTE_VAR = "datalayer_color_palette";

	public static final String TIME_VAR = "time";
	public static final String LATITUDE_VAR = "latitude";
	public static final String LONGITUDE_VAR = "longitude";
	public static final String LAT_VAR = "lat";
	public static final String LON_VAR = "lon";
	public static final String ELEVATION_VAR = "elevation";
	public static final String POSITION_COL_INDEX_VAR = "column_index";

	/** NetCDF dimensions **/
	public static final String DATALAYER_COUNT_DIM = "datalayer_count";
	public static final String LENGTH_DIM = "length";
	public static final String HEIGHT_DIM = "height";
	public static final String POSITION_DIM = "position";
	public static final String VECTOR_DIM = "vector";
}
