package fr.ifremer.globe.core.io.adcp;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Info about ADCP files in Oceansite format (NETCDF).
 */
public class AdcpOceansiteInfo extends BasicFileInfo implements IDataContainerInfo, INavigationDataSupplier {

	private NetcdfFile ncFile;
	private final AtomicBoolean isOpen = new AtomicBoolean();

	/** Attributes of the root group */
	private final List<Property<String>> globalAttributes = new LinkedList<>();

	/** Size of the dimension N_DATE_TIME */
	private int positionCount;

	/** Size of the dimension N_LEVEL */
	private int levelCount;

	/** Date of reference for Julian days (from variable REFERENCE_DATE_TIME) */
	private Instant referenceDate;

	/** Date of reference for Julian days (from variable REFERENCE_DATE_TIME) */
	private Pair<Instant, Instant> startEndDate;

	/**
	 * Constructor
	 */
	public AdcpOceansiteInfo(String path) {
		super(path, ContentType.ADCP_OCEANSITE);
	}

	/**
	 * Open the file
	 */
	@Override
	public synchronized void open(boolean readOnly) throws GIOException {
		try {
			if (isOpen.compareAndSet(false, true)) {
				ncFile = NetcdfFile.open(getPath(), readOnly ? Mode.readonly : Mode.readwrite);
			}
		} catch (NCException e) {
			isOpen.set(false);
			throw new GIOException("Error while opening NVI file : " + e.getMessage(), e);
		}
	}

	/**
	 * Close the file
	 */
	@Override
	public synchronized void close() {
		if (!IDataContainerFactory.grab().isBooked(this) && isOpen.compareAndSet(true, false))
			ncFile.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void declareLayers(DataContainer<?> container) throws GException {
		var map = AdcpOceansiteConstants.LAYERS.stream()
				.collect(Collectors.toMap(PredefinedLayers::getPath, Function.identity()));
		new NetcdfLayerDeclarer().declareLayers(ncFile, container, group -> AdcpOceansiteConstants.GROUP,
				variable -> Optional.ofNullable(map.get(variable.getPath())));
	}

	/**
	 * @return the container of ADCP data
	 */
	public AdcpOceansiteData getAdcpOceansiteData(boolean readonly) throws GIOException {
		return new AdcpOceansiteData(this, readonly);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public INavigationData getNavigationData(boolean readonly) throws GIOException {
		return new AdcpOceansiteData(this, readonly);
	}

	/**
	 * @return the {@link #globalAttributs}
	 */
	public List<Property<String>> getGlobalAttributes() {
		return globalAttributes;
	}

	/**
	 * @return the {@link #globalAttributs}
	 */
	@Override
	public List<Property<?>> getProperties() {
		var result = super.getProperties();

		result.add(Property.build("Position count", positionCount));
		result.add(Property.build("Level count", levelCount));

		getStartEndDate().ifPresent(dates -> {
			Property<String> datesProps = Property.build("Dates", "");
			datesProps.add(Property.build("From",
					DateUtils.getDateString(dates.getFirst()) + " " + DateUtils.getTimeString(dates.getFirst())));
			datesProps.add(Property.build("To",
					DateUtils.getDateString(dates.getSecond()) + " " + DateUtils.getTimeString(dates.getSecond())));
			result.add(datesProps);
		});

		Property<String> globalAttribute = Property.build("Global properties", "");
		globalAttributes.forEach(globalAttribute::add);
		result.add(globalAttribute);
		return result;
	}

	/**
	 * @return the {@link #ncFile}
	 */
	public NetcdfFile getNcFile() {
		return ncFile;
	}

	/**
	 * @return the {@link #positionCount}
	 */
	public int getPositionCount() {
		return positionCount;
	}

	/**
	 * @param positionCount the {@link #positionCount} to set
	 */
	protected void setPositionCount(int positionCount) {
		this.positionCount = positionCount;
	}

	/**
	 * @return the {@link #referenceDate}
	 */
	public Instant getReferenceDate() {
		return referenceDate;
	}

	/**
	 * @param referenceDate the {@link #referenceDate} to set
	 */
	protected void setReferenceDate(Instant referenceDate) {
		this.referenceDate = referenceDate;
	}

	/**
	 * @return the {@link #startEndDate}
	 */
	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		return Optional.ofNullable(startEndDate);
	}

	/**
	 * @param startEndDate the {@link #startEndDate} to set
	 */
	protected void setStartEndDate(Pair<Instant, Instant> startEndDate) {
		this.startEndDate = startEndDate;
	}

	/**
	 * @return the {@link #levelCount}
	 */
	public int getLevelCount() {
		return levelCount;
	}

	/**
	 * @param levelCount the {@link #levelCount} to set
	 */
	protected void setLevelCount(int levelCount) {
		this.levelCount = levelCount;
	}
}
