package fr.ifremer.globe.core.io.adcp;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import org.apache.commons.lang.math.FloatRange;

import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Info about ADCP (acoustic doppler current profiler) files (*.STA).
 */
public class AdcpInfo extends BasicFileInfo {

	public static final String SAMPLING_PROPERTY = "sampling";
	public static final String MIN_RANGE_PROPERTY = "min_range";
	public static final String MAX_RANGE_PROPERTY = "max_range";

	/** Current informations. Empty until current data are not loaded */
	private Optional<ICurrentData> currentData = Optional.empty();
	/** Navigation informations */
	private INavigationData navigationData;

	private int vectorCount;
	private FloatRange rangeLimits;

	/** Date of reference for Julian days (from variable REFERENCE_DATE_TIME) */
	private Pair<Instant, Instant> startEndDate;

	/** Filter of current data */
	private final BiFunction<AdcpInfo, CurrentFiltering, Optional<ICurrentData>> currentDataFilter;

	/**
	 * Constructor
	 */
	public AdcpInfo(String path, BiFunction<AdcpInfo, CurrentFiltering, Optional<ICurrentData>> currentDataFilter) {
		super(path, ContentType.ADCP_STA);
		this.currentDataFilter = currentDataFilter;
	}

	/**
	 * @return the nb of vector present in the file
	 */
	public int getVectorCount() {
		return vectorCount;
	}

	/**
	 * @return the {@link #rangeThreshold}
	 */
	public FloatRange getRangeLimits() {
		return rangeLimits;
	}

	/**
	 * @return the {@link #globalAttributs}
	 */
	@Override
	public List<Property<?>> getProperties() {
		var result = super.getProperties();

		if (currentData.isPresent())
			result.add(Property.build("Vector count", getVectorCount()));
		else
			result.add(Property.build("No vector available at present", "Waiting for toolbox..."));

		FloatRange rangeThreshold = getRangeLimits();
		if (rangeThreshold != null) {
			Property<String> rangeProps = Property.build("Range limits", "");
			Property.buildMinMax(rangeThreshold).forEach(rangeProps::add);
			result.add(rangeProps);
		}
		getStartEndDate().ifPresent(dates -> {
			Property<String> datesProps = Property.build("Dates", "");
			datesProps.add(Property.build("From",
					DateUtils.getDateString(dates.getFirst()) + " " + DateUtils.getTimeString(dates.getFirst())));
			datesProps.add(Property.build("To",
					DateUtils.getDateString(dates.getSecond()) + " " + DateUtils.getTimeString(dates.getSecond())));
			result.add(datesProps);
		});

		return result;
	}

	/** Build a FileInfoSettings to save ADCP properties in session */
	public static FileInfoSettings computeFileInfoSettings(CurrentFiltering filtering) {
		FileInfoSettings result = new FileInfoSettings(
				Map.of(AdcpInfo.SAMPLING_PROPERTY, String.valueOf(filtering.sampling())));
		filtering.range().ifPresent(floatRange -> {
			result.put(AdcpInfo.MIN_RANGE_PROPERTY, floatRange.getMinimumNumber().toString());
			result.put(AdcpInfo.MAX_RANGE_PROPERTY, floatRange.getMaximumNumber().toString());
		});
		result.setContentType(ContentType.ADCP_STA);
		return result;
	}

	/**
	 * Apply the filtering and update the current data.
	 * 
	 * @return true on success
	 */
	public boolean filterCurrentData(CurrentFiltering filtering) {
		Optional<ICurrentData> newCurrentData = this.currentDataFilter.apply(this, filtering);
		newCurrentData.ifPresent(this::setCurrentData);
		return newCurrentData.isPresent();
	}

	/**
	 * @return the {@link #currentData}
	 */
	public Optional<ICurrentData> getCurrentData() {
		return currentData;
	}

	/**
	 * @param currentData the {@link #currentData} to set
	 */
	public void setCurrentData(ICurrentData currentData) {
		this.currentData = Optional.ofNullable(currentData);
	}

	/**
	 * @return the {@link #startEndDate}
	 */
	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		return Optional.ofNullable(startEndDate);
	}

	/**
	 * @param startEndDate the {@link #startEndDate} to set
	 */
	public void setStartEndDate(Pair<Instant, Instant> startEndDate) {
		this.startEndDate = startEndDate;
	}

	/**
	 * @return the {@link #navigationData}
	 */
	public INavigationData getNavigationData() {
		return navigationData;
	}

	/**
	 * @param vectorCount the {@link #vectorCount} to set
	 */
	public void setVectorCount(int vectorCount) {
		this.vectorCount = vectorCount;
	}

	/**
	 * @param rangeLimits the {@link #rangeLimits} to set
	 */
	public void setRangeLimits(FloatRange rangeLimits) {
		this.rangeLimits = rangeLimits;
	}

	/**
	 * @param navigationData the {@link #navigationData} to set
	 */
	public void setNavigationData(INavigationData navigationData) {
		this.navigationData = navigationData;
	}

}
