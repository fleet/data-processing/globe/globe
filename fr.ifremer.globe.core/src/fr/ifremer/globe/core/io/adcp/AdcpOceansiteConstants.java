package fr.ifremer.globe.core.io.adcp;

import java.util.List;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.utils.units.Units;

/**
 * Constants of ADCP files in Oceansite format
 */
public class AdcpOceansiteConstants {

	private AdcpOceansiteConstants() {
		// Constants class
	}

	/** NetCDF dimension names **/
	public static final String DIM_DATE_TIME = "N_DATE_TIME";
	public static final String DIM_LEVEL = "N_LEVEL";

	/** Only one layer group **/
	public static final String GROUP = "/";

	/** NetCDF attribute names **/
	public static final String CONVENTIONS_ATTRIBUT = "CONVENTIONS";
	public static final String CONVENTIONS_OCEANSITE = "OceanSite dictionary";
	public static final String CONVENTION_ATTRIBUT = "CONVENTION";

	/** NetCDF variable names **/
	public static final String VAR_REFERENCE_DATE_TIME = "REFERENCE_DATE_TIME";

	/** Data container layers of navigation **/

	public static final PredefinedLayers<DoubleLoadableLayer1D> JULIAN_DAYS = new PredefinedLayers<>(GROUP, "JULD",
			"Julian days relative to REFERENCE_DATE_TIME", "", DataKind.continuous, DoubleLoadableLayer1D::new);

	public static final PredefinedLayers<DoubleLoadableLayer1D> LATITUDE = new PredefinedLayers<>(GROUP, "LATITUDE",
			"Latitude of each location", Units.DEGREES, DataKind.continuous, DoubleLoadableLayer1D::new);

	public static final PredefinedLayers<DoubleLoadableLayer1D> LONGITUDE = new PredefinedLayers<>(GROUP, "LONGITUDE",
			"Longitude of each location", Units.DEGREES, DataKind.continuous, DoubleLoadableLayer1D::new);

	public static final PredefinedLayers<FloatLoadableLayer1D> HEADING = new PredefinedLayers<>(GROUP, "HDG",
			"Ship heading", Units.DEGREES, DataKind.continuous, FloatLoadableLayer1D::new);

	public static final PredefinedLayers<FloatLoadableLayer1D> PITCH = new PredefinedLayers<>(GROUP, "PTCH",
			"Ship pitch", Units.DEGREES, DataKind.continuous, FloatLoadableLayer1D::new);

	public static final PredefinedLayers<FloatLoadableLayer1D> ROLL = new PredefinedLayers<>(GROUP, "ROLL", "Ship roll",
			Units.DEGREES, DataKind.continuous, FloatLoadableLayer1D::new);

	/** Data container layers of current **/

	public static final PredefinedLayers<FloatLoadableLayer1D> DEPTH = new PredefinedLayers<>(GROUP, "DEPH",
			"Depth of bin center", Units.METERS, DataKind.continuous, FloatLoadableLayer1D::new);
	public static final PredefinedLayers<FloatLoadableLayer2D> NORTHWARD_VELOCITY = new PredefinedLayers<>(GROUP,
			"VVEL_ADCP", "Northward absolute ADCP current velocity", "meter per second", DataKind.continuous,
			FloatLoadableLayer2D::new);
	public static final PredefinedLayers<FloatLoadableLayer2D> EASTWARD_VELOCITY = new PredefinedLayers<>(GROUP,
			"UVEL_ADCP", "Eastward absolute ADCP current velocity", "meter per second", DataKind.continuous,
			FloatLoadableLayer2D::new);

	/** All data container layers to declare **/
	public static final List<PredefinedLayers<?>> LAYERS = List.of(JULIAN_DAYS, LATITUDE, LONGITUDE, HEADING, PITCH,
			ROLL, DEPTH, NORTHWARD_VELOCITY, EASTWARD_VELOCITY);

}
