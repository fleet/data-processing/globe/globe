/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.adcp;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Service providing informations on ADCP files in Oceansite format.
 */
@Component(name = "globe_drivers_adcp_oceansite_file_info_supplier", //
		service = { IFileInfoSupplier.class, INavigationSupplier.class })
public class AdcpOceansiteInfoSupplier extends BasicFileInfoSupplier<AdcpOceansiteInfo> implements INavigationSupplier {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(AdcpOceansiteInfoSupplier.class);

	/** Extensions **/
	public static final String EXTENSION_NC = "nc";

	/**
	 * Constructor
	 */
	public AdcpOceansiteInfoSupplier() {
		super(EXTENSION_NC, "ADCP Oceansite", ContentType.ADCP_OCEANSITE);
	}

	/**
	 * Make an instance of AdcpOceansiteInfo for the specified file
	 */
	@Override
	public AdcpOceansiteInfo makeFileInfo(String filePath) {
		try (AdcpOceansiteInfo info = new AdcpOceansiteInfo(filePath)) {
			info.open(true);
			if (isAdcpOceansiteFile(info)) {
				loadGlobalAttributes(info);
				loadReferenceDate(info);
				loadDimensions(info);
				computeGeobox(info);
				return info;
			}
		} catch (NCException | GIOException e) {
			LOGGER.error("Error while opening file {} : {}", filePath, e.getMessage(), e);
		}
		return null;
	}

	/**
	 * Read the Date of reference for all Julian days described in the file
	 */
	private void loadReferenceDate(AdcpOceansiteInfo info) throws NCException { // Default value and format (
		String format = "yyyyMMddHHmmss";
		String value = "19500101000000";

		NetcdfVariable dateVar = info.getNcFile().getVariable(AdcpOceansiteConstants.VAR_REFERENCE_DATE_TIME);
		if (dateVar != null) {
			byte[] cars = dateVar.get_byte(new long[] { 0l }, new long[] { dateVar.getShape().get(0).getLength() });
			if (cars[0] != 0 && cars.length == format.length())
				value = new String(cars);
		}

		info.getGlobalAttributes().add(Property.build(AdcpOceansiteConstants.VAR_REFERENCE_DATE_TIME, value));
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneOffset.UTC);
		info.setReferenceDate(ZonedDateTime.parse(value, dateTimeFormatter).toInstant());
	}

	/**
	 * Browses the navigation points for computing Geobox and start/end dates
	 */
	private void computeGeobox(AdcpOceansiteInfo info) throws GIOException {
		INavigationData navigationData = info.getNavigationData(true);
		info.setGeoBox(navigationData.computeGeoBox());
		info.setStartEndDate(
				Pair.of(navigationData.getInstant(0), navigationData.getInstant(info.getPositionCount() - 1)));
	}

	/**
	 * Read Netcdf dimensions
	 */
	private void loadDimensions(AdcpOceansiteInfo info) throws NCException {
		NCDimension dateDim = info.getNcFile().getDimension(AdcpOceansiteConstants.DIM_DATE_TIME);
		if (dateDim != null) {
			info.setPositionCount((int) dateDim.getLength());
		}
		NCDimension levalDim = info.getNcFile().getDimension(AdcpOceansiteConstants.DIM_LEVEL);
		if (levalDim != null) {
			info.setLevelCount((int) levalDim.getLength());
		}
	}

	/** Chech if the file is an ADCP Oceansite one */
	private boolean isAdcpOceansiteFile(AdcpOceansiteInfo info) throws NCException {
		if (info.getNcFile().hasAttribute(AdcpOceansiteConstants.CONVENTIONS_ATTRIBUT)) {
			var conventions = info.getNcFile().getAttributeAsString(AdcpOceansiteConstants.CONVENTIONS_ATTRIBUT);
			return AdcpOceansiteConstants.CONVENTIONS_OCEANSITE.equalsIgnoreCase(conventions);
		}
		return false;
	}

	/** Reads Sonar root group attributes **/
	public void loadGlobalAttributes(AdcpOceansiteInfo info) {
		try {
			info.getNcFile().getGlobalAttributs()
					.forEach((k, v) -> info.getGlobalAttributes().add(Property.build(k, v)));
		} catch (NCException e) {
			LOGGER.error("Error during ADCP Oceansite attributs retrievement: " + e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof AdcpOceansiteInfo info ? info : null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(String filePath) {
		return getFileInfo(filePath).map(INavigationDataSupplier.class::cast);
	}
}