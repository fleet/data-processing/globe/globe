package fr.ifremer.globe.core.io.adcp;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;

import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.current.CurrentMetadataType;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.units.Units;

/**
 * {@link INavigationDataSupplier} implementation for {@link AdcpOceansiteInfo}.
 */
public class AdcpOceansiteData implements INavigationData, ICurrentData, IDataContainerOwner {

	/** Adcp file currently read */
	private final AdcpOceansiteInfo info;

	/** Token received after booking the file */
	private final IDataContainerToken<AdcpOceansiteInfo> dataContainerToken;

	/** Indicates if this IDataContainerOwner is in read mode only */
	private final boolean isReadOnly;

	/** List of metadata for vectors */
	private final List<NavigationMetadata<Double>> vectorMetada = new ArrayList<>();

	// Loaded Retcdf Layers
	private final DoubleLoadableLayer1D julianDays;
	private final DoubleLoadableLayer1D latitudes;
	private final DoubleLoadableLayer1D longitudes;
	private final FloatLoadableLayer1D headings;
	private final FloatLoadableLayer1D pitch;
	private final FloatLoadableLayer1D roll;
	private final FloatLoadableLayer1D elevation;
	private final FloatLoadableLayer2D northwardNelocity;
	private final FloatLoadableLayer2D eastwardNelocity;

	/** Metadata on navigation position **/
	private final List<NavigationMetadata<?>> metadata = List.of(//
			NavigationMetadataType.TIME.withValueSupplier(this::getTime), //
			new NavigationMetadataType<Float>("Heading", Units.DEGREES, -360f, 360f)
					.withValueSupplier(this::getHeading), //
			new NavigationMetadataType<Float>("Pitch", Units.DEGREES, -360f, 360f).withValueSupplier(this::getPitch), //
			new NavigationMetadataType<Float>("Roll", Units.DEGREES, -360f, 360f).withValueSupplier(this::getRoll)//
	);

	/** Constructor **/
	public AdcpOceansiteData(AdcpOceansiteInfo info, boolean readonly) throws GIOException {
		this.info = info;
		this.isReadOnly = readonly;
		dataContainerToken = IDataContainerFactory.grab().book(info, this);
		DataContainer<AdcpOceansiteInfo> dataContainer = dataContainerToken.getDataContainer();

		// Loading layers
		julianDays = dataContainer.getLayer(AdcpOceansiteConstants.JULIAN_DAYS);
		latitudes = dataContainer.getLayer(AdcpOceansiteConstants.LATITUDE);
		longitudes = dataContainer.getLayer(AdcpOceansiteConstants.LONGITUDE);
		headings = dataContainer.getLayer(AdcpOceansiteConstants.HEADING);
		pitch = dataContainer.getLayer(AdcpOceansiteConstants.PITCH);
		roll = dataContainer.getLayer(AdcpOceansiteConstants.ROLL);
		elevation = dataContainer.getLayer(AdcpOceansiteConstants.DEPTH);
		northwardNelocity = dataContainer.getLayer(AdcpOceansiteConstants.NORTHWARD_VELOCITY);
		eastwardNelocity = dataContainer.getLayer(AdcpOceansiteConstants.EASTWARD_VELOCITY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFileName() {
		return info.getFilename();
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public IFileInfo getFileInfo() {
		return info;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return info.getPositionCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getTime(int index) throws GIOException {
		return getInstant(index).toEpochMilli();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Instant getInstant(int index) throws GIOException {
		long msSinceRefDate = (long) (julianDays.get(index) * (24l * 60l * 60l * 1000l));
		return info.getReferenceDate().plusMillis(msSinceRefDate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getLongitude(int index) throws GIOException {
		return longitudes.get(index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getLatitude(int index) throws GIOException {
		return latitudes.get(index);
	}

	/** @return the heading value at the specified position index */
	private float getHeading(int index) throws GIOException {
		return headings.get(index);
	}

	/** @return the pitch value at the specified position index */
	private float getPitch(int index) throws GIOException {
		return pitch.get(index);
	}

	/** @return the roll value at the specified position index */
	private float getRoll(int index) throws GIOException {
		return roll.get(index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isReadOnly() {
		return isReadOnly;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTotalVectorCount() {
		return info.getPositionCount() * info.getLevelCount();
	}

	@Override
	public int getVectorCount() {
		return getTotalVectorCount(); // No filtering for ADCP Oceansite
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getVectorTime(int index) throws GIOException {
		return getTime(index / info.getLevelCount());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Instant getVectorInstant(int index) throws GIOException {
		return getInstant(index / info.getLevelCount());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getVectorLongitude(int index) throws GIOException {
		return getLongitude(index / info.getLevelCount());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getVectorLatitude(int index) throws GIOException {
		return getLatitude(index / info.getLevelCount());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getVectorElevation(int index) throws GIOException {
		return elevation.get(index % info.getLevelCount());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getVectorNorthwardVelocity(int index) throws GIOException {
		int timeIndex = index / info.getLevelCount();
		int levelIndex = index % info.getLevelCount();
		return northwardNelocity.get(timeIndex, levelIndex);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getVectorEastwardVelocity(int index) throws GIOException {
		int timeIndex = index / info.getLevelCount();
		int levelIndex = index % info.getLevelCount();
		return eastwardNelocity.get(timeIndex, levelIndex);
	}

	@Override
	public List<NavigationMetadata<Double>> getVectorMetadata() {
		if (vectorMetada.isEmpty()) {
			vectorMetada.add(CurrentMetadataType.HEADING.withValueSupplier(this::getVectorHeading));
			vectorMetada.add(CurrentMetadataType.VALUE.withValueSupplier(this::getVectorValue));
		}
		return vectorMetada;
	}

	/** {@inheritDoc} */
	@Override
	public CurrentFiltering getFiltering() {
		return new CurrentFiltering(0, Optional.empty()); // No filter available
	}

	@Override
	public FloatRange getRangeLimits() {
		return new FloatRange(0f);
	}
}
