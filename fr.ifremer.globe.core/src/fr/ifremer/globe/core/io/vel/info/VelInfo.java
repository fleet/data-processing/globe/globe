package fr.ifremer.globe.core.io.vel.info;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.core.model.velocity.SoundVelocity;
import fr.ifremer.globe.core.model.velocity.SoundVelocityProfile;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides information about a soundvelocity file (.vel).
 */
public class VelInfo implements IFileInfo, ISoundVelocityData {

	private static final Logger LOGGER = LoggerFactory.getLogger(VelInfo.class);

	/** Path & file **/
	private final String filePath;

	/** Velocity info attributes */
	private GeoBox geobox;
	private Instant startDate;
	private Instant endDate;
	private int profileCounter;
	private List<ISoundVelocityProfile> soundVelocityProfiles;

	/**
	 * Constructor
	 */
	public VelInfo(String filePath) {
		this.filePath = filePath;
		load();
	}

	/**
	 * Loads properties from the {@link NetcdfFile}.
	 */
	public void load() {
		try (NetcdfFile ncFile = NetcdfFile.open(filePath, Mode.readonly)) {
			// start / end dates
			int velDate = ncFile.getAttributeInt(VelocityConstants.StartDate);
			int velTime = ncFile.getAttributeInt(VelocityConstants.StartTime);
			setStartDate(DateUtils.getInstantFromJulian(velDate, velTime));
			velDate = ncFile.getAttributeInt(VelocityConstants.EndDate);
			velTime = ncFile.getAttributeInt(VelocityConstants.EndTime);
			setEndDate(DateUtils.getInstantFromJulian(velDate, velTime));

			// geographic box
			double north = ncFile.getAttributeDouble(VelocityConstants.NorthLatitude);
			double south = ncFile.getAttributeDouble(VelocityConstants.SouthLatitude);
			double east = ncFile.getAttributeDouble(VelocityConstants.EastLongitude);
			double west = ncFile.getAttributeDouble(VelocityConstants.WestLongitude);
			setGeoBox(new GeoBox(north, south, east, west));

			// profiles number
			setProfileCounter(ncFile.getAttributeInt(VelocityConstants.ProfileCounter));

			soundVelocityProfiles = readSoundSpeedProfiles(ncFile);
		} catch (NCException | GIOException e) {
			LOGGER.error("Exception reading VEL file: " + filePath, e);
		}
	}

	/**
	 * Reads {@link SoundVelocityProfile} from the specified {@link NetcdfFile}.
	 * 
	 * @param allInfo : input file (.all)
	 * 
	 * @return a list of {@link ISoundVelocityProfile}.
	 * @throws NCException
	 * @throws UnsupportedSounderException
	 */
	public static List<ISoundVelocityProfile> readSoundSpeedProfiles(NetcdfFile ncFile)
			throws GIOException, NCException {
		LOGGER.debug("Extraction of sound speed profiles from {}... ", ncFile.getName());
		List<ISoundVelocityProfile> list = new ArrayList<>();
		int nbProfiles = ncFile.getAttributeInt(VelocityVariables.nbrProfil);

		// Day
		NetcdfVariable varDate = ncFile.getVariable(VelocityVariables.date);
		ILayerLoader layerLoader = new NetcdfLayerLoader<>(ncFile, varDate);
		IntLoadableLayer1D layerDate = new IntLoadableLayer1D(VelocityVariables.date, "Date", "d", layerLoader);
		layerDate.load();

		// Time
		NetcdfVariable varTime = ncFile.getVariable(VelocityVariables.time);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varTime);
		IntLoadableLayer1D layerTime = new IntLoadableLayer1D(VelocityVariables.time, "Time", "deg", layerLoader);
		layerTime.load();

		// Profile date
		LongLoadableLayer1D profileDate = new LongLoadableLayer1D("profileDate", "profileDate",
				"milliseconds since January 1, 1970, 00:00:00 GMT", new ProfilesTimeLayerLoader(layerDate, layerTime));
		profileDate.load();

		// Latitude
		NetcdfVariable varLatitude = ncFile.getVariable(VelocityVariables.ordinate);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varLatitude);
		DoubleLoadableLayer1D layerLatitude = new DoubleLoadableLayer1D(VelocityVariables.ordinate, "Latitude", "deg",
				layerLoader);
		layerLatitude.load();

		// Longitude
		NetcdfVariable varLongitude = ncFile.getVariable(VelocityVariables.abscissa);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varLongitude);
		DoubleLoadableLayer1D layerLongitude = new DoubleLoadableLayer1D(VelocityVariables.abscissa, "Longitude", "deg",
				layerLoader);
		layerLongitude.load();

		// Number of levels
		NetcdfVariable varNbLevels = ncFile.getVariable(VelocityVariables.nbrLevel);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varNbLevels);
		IntLoadableLayer1D layerNbLevels = new IntLoadableLayer1D(VelocityVariables.nbrLevel, "Number of levels", "",
				layerLoader);
		layerNbLevels.load();
		double levelsMissingValue = varNbLevels.getUnpackedMissingValue();

		// Type of profile
		NetcdfVariable varType = ncFile.getVariable(VelocityVariables.type);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varType);
		IntLoadableLayer1D layerType = new IntLoadableLayer1D(VelocityVariables.type, "Type of profile", "",
				layerLoader);
		layerType.load();

		// Depth
		NetcdfVariable varDepth = ncFile.getVariable(VelocityVariables.depth);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varDepth);
		DoubleLoadableLayer2D layerDepth = new DoubleLoadableLayer2D(VelocityVariables.depth, "Depth", "m",
				layerLoader);
		layerDepth.load();
		Double depthMissingValue = varDepth.getUnpackedMissingValue();

		// Velocity
		NetcdfVariable varSoundVelocity = ncFile.getVariable(VelocityVariables.soundVelocity);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varSoundVelocity);
		DoubleLoadableLayer2D layerSoundVelocity = new DoubleLoadableLayer2D(VelocityVariables.soundVelocity,
				"Sound velocity", "m/s", layerLoader);
		layerSoundVelocity.load();
		Double soundVelocityMissingValue = varSoundVelocity.getUnpackedMissingValue();

		// Temperature
		NetcdfVariable varTemperature = ncFile.getVariable(VelocityVariables.temperature);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varTemperature);
		DoubleLoadableLayer2D layerTemperature = new DoubleLoadableLayer2D(VelocityVariables.temperature, "Temperature",
				"degreeC", layerLoader);
		layerTemperature.load();
		Double temperatureMissingValue = varTemperature.getUnpackedMissingValue();

		// Salinity
		NetcdfVariable varSalinity = ncFile.getVariable(VelocityVariables.salinity);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varSalinity);
		DoubleLoadableLayer2D layerSalinity = new DoubleLoadableLayer2D(VelocityVariables.salinity, "Salinity", "%",
				layerLoader);
		layerSalinity.load();
		Double salinityMissingValue = varSalinity.getUnpackedMissingValue();

		// Absorption
		NetcdfVariable varAbsorption = ncFile.getVariable(VelocityVariables.absorption);
		layerLoader = new NetcdfLayerLoader<>(ncFile, varAbsorption);
		DoubleLoadableLayer2D layerAbsorption = new DoubleLoadableLayer2D(VelocityVariables.absorption, "Absorption",
				"dB/km", layerLoader);
		layerAbsorption.load();
		Double AbsorptionMissingValue = varAbsorption.getUnpackedMissingValue();

		for (int profileNumber = 0; profileNumber < nbProfiles; profileNumber++) {
			List<SoundVelocity> soundVelocityList = new ArrayList<>();
			int nbLevels = layerNbLevels.get(profileNumber);
			for (int levelNumber = 0; levelNumber < nbLevels; levelNumber++) {
				Double depth = layerDepth.get(profileNumber, levelNumber);
				depth = depth.compareTo(depthMissingValue) == 0 ? Double.NaN : depth;

				Double soundVelocity = layerSoundVelocity.get(profileNumber, levelNumber);
				soundVelocity = soundVelocity.compareTo(soundVelocityMissingValue) == 0 ? Double.NaN : soundVelocity;

				Double temperature = layerTemperature.get(profileNumber, levelNumber);
				temperature = soundVelocity.compareTo(temperatureMissingValue) == 0 ? Double.NaN : temperature;

				Double salinity = layerSalinity.get(profileNumber, levelNumber);
				salinity = salinity.compareTo(salinityMissingValue) == 0 ? Double.NaN : salinity;

				Double absorption = layerAbsorption.get(profileNumber, levelNumber);
				absorption = ((Double) (layerAbsorption.get(profileNumber, levelNumber)))
						.compareTo(AbsorptionMissingValue) == 0 ? Double.NaN : absorption;

				if (levelNumber == 0 || (!Double.isNaN(depth) && !Double.isNaN(soundVelocity))) { // Filter empty
																									// data
					SoundVelocity soundVelocityObject = new SoundVelocity(soundVelocity, depth, temperature, salinity,
							absorption);
					soundVelocityList.add(soundVelocityObject);
				}
			}

			SoundVelocityProfile profile = new SoundVelocityProfile(soundVelocityList,
					new Date(profileDate.get(profileNumber)));
			profile.setLat(layerLatitude.get(profileNumber));
			profile.setLon(layerLongitude.get(profileNumber));
			list.add(profile);
		}

		return list;
	}

	/**
	 * @return VEL properties
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = new ArrayList<>();

		properties.add(Property.build("Start Date", startDate != null ? startDate.toString() : "Unknown"));
		properties.add(Property.build("End Date", endDate != null ? endDate.toString() : "Unknown"));

		properties.add(Property.build("Profiles counter", Integer.toString(profileCounter)));

		if (geobox != null) {
			properties.add(Property.build("North Latitude",
					FormatLatitudeLongitude.latitudeToString(geobox.getTop())));
			properties.add(Property.build("South Latitude",
					FormatLatitudeLongitude.latitudeToString(geobox.getBottom())));
			properties.add(Property.build("West Longitude",
					FormatLatitudeLongitude.longitudeToString(geobox.getLeft())));
			properties.add(Property.build("East Longitude",
					FormatLatitudeLongitude.longitudeToString(geobox.getRight())));
		}

		return properties;
	}

	/**********************************************************************************************************
	 * Getters / Setters
	 **********************************************************************************************************/

	@Override
	public ContentType getContentType() {
		return ContentType.VEL_NETCDF_4;
	}

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public GeoBox getGeoBox() {
		return geobox;
	}

	public void setGeoBox(GeoBox geoBox) {
		this.geobox = geoBox;
	}

	public final void setStartDate(Instant start) {
		this.startDate = start;
	}

	public final void setEndDate(Instant end) {
		this.endDate = end;
	}

	@Override
	public final Optional<Pair<Instant, Instant>> getStartEndDate() {
		return startDate != null && endDate != null ? Optional.of(Pair.of(startDate, endDate)) : Optional.empty();
	}

	public int getProfileCounter() {
		return profileCounter;
	}

	public void setProfileCounter(int count) {
		this.profileCounter = count;
	}

	@Override
	public String getOriginFileName() {
		return getFilename();
	}

	@Override
	public List<ISoundVelocityProfile> getSoundVelocityProfiles() throws GIOException {
		return soundVelocityProfiles;
	}

}