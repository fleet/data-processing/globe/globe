package fr.ifremer.globe.core.io.vel.info;

import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.DataType;

/**
 * NVI constants
 */
public class VelocityConstants {
	
	private VelocityConstants() {
		// Constants class
	}
	
	public static final Short ACTUAL_VERSION = 210;
	public static final String MODULE_NAME = "Velocity export";
	public static final String CLASSE = "MB_VELOCITY";
	

	
	public static final String TIME_REFERENCE_ATTRIBUTE = "Julian date for 1970/01/01 = 2 440 588";
	public static final double ELLIPSOID_A = 6378137.0;
	public static final double ELLIPSOID_INVF = 0.003352810664747574;
	public static final double ELLIPSOID_E2 = 0.006694379990141441;

	public static final String DIM_PROFILE_NBR = "mbProfilMaxNbr";
	public static final String DIM_LEVELS_NBR = "mbLevelMaxNbr";
	public static final String DIM_NAME_LENGTH = "mbNameLength";
	public static final String DIM_COMMENT_LENGTH = "mbCommentLength";

	public static final String Version = "mbVersion";
	public static final String Name = "mbName";
	public static final String GLOBAL_ATT_CLASSE = "mbClasse";
	public static final String Level = "mbLevel";
	public static final String NbrHistoryRec = "mbNbrHistoryRec";
	public static final String TimeReference = "mbTimeReference";
	public static final String StartDate = "mbStartDate";
	public static final String StartTime = "mbStartTime";
	public static final String EndDate = "mbEndDate";
	public static final String EndTime = "mbEndTime";
	public static final String NorthLatitude = "mbNorthLatitude";
	public static final String SouthLatitude = "mbSouthLatitude";
	public static final String EastLongitude = "mbEastLongitude";
	public static final String WestLongitude = "mbWestLongitude";
	public static final String Meridian180 = "mbMeridian180";
	public static final String GeoDictionnary = "mbGeoDictionnary";
	public static final String GeoRepresentation = "mbGeoRepresentation";
	public static final String GeodesicSystem = "mbGeodesicSystem";
	public static final String EllipsoidName = "mbEllipsoidName";
	public static final String EllipsoidA = "mbEllipsoidA";
	public static final String EllipsoidInvF = "mbEllipsoidInvF";
	public static final String EllipsoidE2 = "mbEllipsoidE2";
	public static final String ProjType = "mbProjType";
	public static final String ProjParameterValue = "mbProjParameterValue";
	public static final String ProjParameterCode = "mbProjParameterCode";
	public static final String CDI = "mbCDI";
	public static final String ProfileCounter = "mbNbrProfil";
	
	public static final String ATTRIBUTE_TYPE = "type";
	public static final String ATTRIBUTE_LONG_NAME = "long_name";
	public static final String ATTRIBUTE_NAME_CODE = "name_code";
	public static final String ATTRIBUTE_UNITS = CFStandardNames.CF_UNITS;
	public static final String ATTRIBUTE_UNIT_CODE = "unit_code";
	public static final String ATTRIBUTE_ADD_OFFSET = "add_offset";
	public static final String ATTRIBUTE_SCALE_FACTOR = "scale_factor";
	public static final String ATTRIBUTE_MIN = "minimum";
	public static final String ATTRIBUTE_MAX = "maximum";
	public static final String ATTRIBUTE_VALID_MIN = "valid_minimum";
	public static final String ATTRIBUTE_VALID_MAX = "valid_maximum";
	public static final String ATTRIBUTE_MISSING_VALUE = "missing_value";
	public static final String ATTRIBUTE_FORMAT_C = "format_C";
	public static final String ATTRIBUTE_ORIENTATION = "orientation";
	public static final String ATTRIBUTE_FLAG_VALUES = "flag_values";
	public static final String ATTRIBUTE_FLAG_MEANINGS = "flag_meanings";


	public static final String NAME_DATE = "mbDate";
	public static final DataType DATA_TYPE_DATE = DataType.INT;
	public static final String TYPE_DATE = "integer";
	public static final String LONG_NAME_DATE = "Date of cycle";
	public static final String NAME_CODE_DATE = "MB_PROFILE_DATE";
	public static final String UNITS_DATE = "Julian_date";
	public static final String UNIT_CODE_DATE = "MB_JULIAN_DATE";
	public static final int ADD_OFFSET_DATE = 2440588;
	public static final int SCALE_FACTOR_DATE = 1;
	public static final int MINIMUM_DATE = -25567;
	public static final int MAXIMUM_DATE = 47482;
	public static final int VALID_MINIMUM_DATE = -25567;
	public static final int VALID_MAXIMUM_DATE = 47482;
	public static final int MISSING_VALUE_DATE = 2147483647;
	public static final String FORMAT_C_DATE = "%d";
	public static final String ORIENTATION_DATE = "direct";
	
	public static final String NAME_TIME = "mbTime";
	public static final DataType DATA_TYPE_TIME = DataType.INT;
	public static final String TYPE_TIME = "integer";
	public static final String LONG_NAME_TIME = "Time of cycle";
	public static final String NAME_CODE_TIME = "MB_PROFILE_TIME";
	public static final String UNITS_TIME = "ms";
	public static final String UNIT_CODE_TIME = "MB_MS";
	public static final int ADD_OFFSET_TIME = 0;
	public static final int SCALE_FACTOR_TIME = 1;
	public static final int MINIMUM_TIME = 0;
	public static final int MAXIMUM_TIME = 86399999;
	public static final int VALID_MINIMUM_TIME = 0;
	public static final int VALID_MAXIMUM_TIME = 86399999;
	public static final int MISSING_VALUE_TIME = -2147483648;
	public static final String FORMAT_C_TIME = "%d";
	public static final String ORIENTATION_TIME = "direct";

	public static final String NAME_LATITUDE = "mbOrdinate";
	public static final DataType DATA_TYPE_LATITUDE = DataType.INT;
	public static final String TYPE_LATITUDE = "real";
	public static final String LONG_NAME_LATITUDE = "Latitude";
	public static final String NAME_CODE_LATITUDE = "MB_PROFILE_LATITUDE";
	public static final String UNITS_LATITUDE = "degree";
	public static final String UNIT_CODE_LATITUDE = "MB_DEGREE";
	public static final double ADD_OFFSET_LATITUDE = 0.0;
	public static final double SCALE_FACTOR_LATITUDE = 0.00000005;
	public static final double MINIMUM_LATITUDE = -1.8E9;
	public static final double MAXIMUM_LATITUDE = 1.8E9;
	public static final int VALID_MINIMUM_LATITUDE = -1800000000;
	public static final int VALID_MAXIMUM_LATITUDE = 1800000000;
	public static final int MISSING_VALUE_LATITUDE = -2147483648;
	public static final String FORMAT_C_LATITUDE = "%f";
	public static final String ORIENTATION_LATITUDE = "direct";

	public static final String NAME_LONGITUDE = "mbAbscissa";
	public static final DataType DATA_TYPE_LONGITUDE = DataType.INT;
	public static final String TYPE_LONGITUDE = "real";
	public static final String LONG_NAME_LONGITUDE = "Longitude";
	public static final String NAME_CODE_LONGITUDE = "MB_PROFILE_LONGITUDE";
	public static final String UNITS_LONGITUDE = "degree";
	public static final String UNIT_CODE_LONGITUDE = "MB_DEGREE";
	public static final double ADD_OFFSET_LONGITUDE = 0.0;
	public static final double SCALE_FACTOR_LONGITUDE = 0.0000001;
	public static final double MINIMUM_LONGITUDE = -1.8E9;
	public static final double MAXIMUM_LONGITUDE = 1.8E9;
	public static final int VALID_MINIMUM_LONGITUDE = -1800000000;
	public static final int VALID_MAXIMUM_LONGITUDE = 1800000000;
	public static final int MISSING_VALUE_LONGITUDE = -2147483648;
	public static final String FORMAT_C_LONGITUDE = "%f";
	public static final String ORIENTATION_LONGITUDE = "direct";
	
	public static final String NBRLEVELS = "mbNbrLevel";
	public static final DataType DATA_TYPE_NBRLEVEL = DataType.SHORT;
	public static final String TYPE_NBRLEVEL = "integer";
	public static final String LONG_NAME_NBRLEVEL = "Number of levels";
	public static final String NAME_CODE_NBRLEVEL = "MB_LEVEL_NUMBER";
	public static final String UNITS_NBRLEVEL = "";
	public static final String UNIT_CODE_NBRLEVEL = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_NBRLEVEL = 0;
	public static final int SCALE_FACTOR_NBRLEVEL = 1;
	public static final int MINIMUM_NBRLEVEL = 1;
	public static final int MAXIMUM_NBRLEVEL = 65535;
	public static final int VALID_MINIMUM_NBRLEVEL = 1;
	public static final int VALID_MAXIMUM_NBRLEVEL = 65535;
	public static final int MISSING_VALUE_NBRLEVEL = 0;
	public static final String FORMAT_C_NBRLEVEL = "%d";
	public static final String ORIENTATION_NBRLEVEL = "direct";
	
	public static final String TYPE = "mbType";
	public static final DataType DATA_TYPE_TYPE = DataType.CHAR;
	public static final String TYPE_TYPE = "integer"; 
	public static final String LONG_NAME_TYPE = "Type of profile";
	public static final String NAME_CODE_TYPE = "MB_PROFILE_TYPE";
	public static final String UNITS_TYPE = "";
	public static final String UNIT_CODE_TYPE = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_TYPE = 0;
	public static final int SCALE_FACTOR_TYPE = 1;
	public static final int MINIMUM_TYPE = 1;
	public static final int MAXIMUM_TYPE = 255;
	public static final int VALID_MINIMUM_TYPE = 1;
	public static final int VALID_MAXIMUM_TYPE = 255;
	public static final int MISSING_VALUE_TYPE = 0;
	public static final String FORMAT_C_TYPE = "%d";
	public static final String ORIENTATION_TYPE = "direct";
	
	public static final String VELCOMPALGO = "mbVelCompAlgo";
	public static final DataType DATA_TYPE_VELCOMPALGO = DataType.CHAR;
	public static final String TYPE_VELCOMPALGO = "string";
	public static final String LONG_NAME_VELCOMPALGO = "Velocity computation algorithm";
	public static final String NAME_CODE_VELCOMPALGO = "MB_PROFILE_COMP_ALGO";
	
	public static final String CARRIER = "mbCarrier";
	public static final DataType DATA_TYPE_CARRIER = DataType.CHAR;
	public static final String TYPE_CARRIER = "string";
	public static final String LONG_NAME_CARRIER = "Carrier";
	public static final String NAME_CODE_CARRIER = "MB_PROFILE_CARRIER";
	
	public static final String SEQUENCE = "mbSequence";
	public static final DataType DATA_TYPE_SEQUENCE = DataType.SHORT;
	public static final String TYPE_SEQUENCE = "integer";
	public static final String LONG_NAME_SEQUENCE = "Sequence";
	public static final String NAME_CODE_SEQUENCE = "MB_PROFILE_SEQUENCE";
	public static final String UNITS_SEQUENCE = "";
	public static final String UNIT_CODE_SEQUENCE = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_SEQUENCE = 0;
	public static final int SCALE_FACTOR_SEQUENCE = 1;
	public static final int MINIMUM_SEQUENCE = 1;
	public static final int MAXIMUM_SEQUENCE = 65535;
	public static final int VALID_MINIMUM_SEQUENCE = 1;
	public static final int VALID_MAXIMUM_SEQUENCE = 65535;
	public static final int MISSING_VALUE_SEQUENCE = 0;
	public static final String FORMAT_C_SEQUENCE = "%d";
	public static final String ORIENTATION_SEQUENCE = "direct";
	
	public static final String SERIALNUMBER = "mbSerialNumber";
	public static final DataType DATA_TYPE_SERIALNUMBER = DataType.CHAR;
	public static final String TYPE_SERIALNUMBER = "string";
	public static final String LONG_NAME_SERIALNUMBER = "Serial number";
	public static final String NAME_CODE_SERIALNUMBER = "MB_PROFILE_SN";
	
	public static final String PROFILESALINITY = "mbProfileSalinity";
	public static final DataType DATA_TYPE_PROFILESALINITY = DataType.SHORT;
	public static final String TYPE_PROFILESALINITY = "real";
	public static final String LONG_NAME_PROFILESALINITY = "Salinity";
	public static final String NAME_CODE_PROFILESALINITY = "MB_PROFILE_SALINITY";
	public static final String UNITS_PROFILESALINITY = "0/00";
	public static final String UNIT_CODE_PROFILESALINITY = "MB_PM";
	public static final double ADD_OFFSET_PROFILESALINITY = 0.0;
	public static final double SCALE_FACTOR_PROFILESALINITY = 0.01;
	public static final double MINIMUM_PROFILESALINITY = 0.;
	public static final double MAXIMUM_PROFILESALINITY = 4500.;
	public static final int VALID_MINIMUM_PROFILESALINITY = 0;
	public static final int VALID_MAXIMUM_PROFILESALINITY = 4500;
	public static final int MISSING_VALUE_PROFILESALINITY = 32767;
	public static final String FORMAT_C_PROFILESALINITY = "%.2f";
	public static final String ORIENTATION_PROFILESALINITY = "direct";
	
	public static final String PROFILEDEPTH = "mbProfileDepth";
	public static final DataType DATA_TYPE_PROFILEDEPTH = DataType.INT;
	public static final String TYPE_PROFILEDEPTH = "real";
	public static final String LONG_NAME_PROFILEDEPTH = "Depth";
	public static final String NAME_CODE_PROFILEDEPTH = "MB_PROFILE_DEPTH";
	public static final String UNITS_PROFILEDEPTH = "m";
	public static final String UNIT_CODE_PROFILEDEPTH = "MB_M";
	public static final double ADD_OFFSET_PROFILEDEPTH = 0.0;
	public static final double SCALE_FACTOR_PROFILEDEPTH = 0.01;
	public static final double MINIMUM_PROFILEDEPTH = 0.0;
	public static final double MAXIMUM_PROFILEDEPTH = 2.147483646E9;
	public static final int VALID_MINIMUM_PROFILEDEPTH = 0;
	public static final int VALID_MAXIMUM_PROFILEDEPTH = 2147483646;
	public static final int MISSING_VALUE_PROFILEDEPTH = Integer.MAX_VALUE;
	public static final String FORMAT_C_PROFILEDEPTH = "%.2f";
	public static final String ORIENTATION_PROFILEDEPTH = "direct";
	
	public static final String PRESSURE = "mbPressure";
	public static final DataType DATA_TYPE_PRESSURE = DataType.INT;
	public static final String TYPE_PRESSURE = "real";
	public static final String LONG_NAME_PRESSURE = "Pressure";
	public static final String NAME_CODE_PRESSURE = "MB_PROFILE_PRESSURE";
	public static final String UNITS_PRESSURE = "";
	public static final String UNIT_CODE_PRESSURE = "MB_NOT_DEFINED";
	public static final double ADD_OFFSET_PRESSURE = 0.0;
	public static final double SCALE_FACTOR_PRESSURE = 0.01;
	public static final double MINIMUM_PRESSURE = 0.0;
	public static final double MAXIMUM_PRESSURE = 2.147483646E9;
	public static final int VALID_MINIMUM_PRESSURE = 0;
	public static final int VALID_MAXIMUM_PRESSURE = 2147483646;
	public static final int MISSING_VALUE_PRESSURE = Integer.MAX_VALUE;
	public static final String FORMAT_C_PRESSURE = "%.2f";
	public static final String ORIENTATION_PRESSURE = "direct";
	
	public static final String SURFAIRTEMP = "mbSurfAirTemp";
	public static final DataType DATA_TYPE_SURFAIRTEMP = DataType.SHORT;
	public static final String TYPE_SURFAIRTEMP = "real";
	public static final String LONG_NAME_SURFAIRTEMP = "Surface air temperature";
	public static final String NAME_CODE_SURFAIRTEMP = "MB_PROFILE_SURF_TEMP";
	public static final String UNITS_SURFAIRTEMP = "degreeC";
	public static final String UNIT_CODE_SURFAIRTEMP = "MB_DEGREE_CENT";
	public static final double ADD_OFFSET_SURFAIRTEMP = 0.0;
	public static final double SCALE_FACTOR_SURFAIRTEMP = 0.01;
	public static final double MINIMUM_SURFAIRTEMP = -500.0;
	public static final double MAXIMUM_SURFAIRTEMP = 4500.0;
	public static final int VALID_MINIMUM_SURFAIRTEMP = -500;
	public static final int VALID_MAXIMUM_SURFAIRTEMP = 4500;
	public static final int MISSING_VALUE_SURFAIRTEMP = 32767;
	public static final String FORMAT_C_SURFAIRTEMP = "%.2f";
	public static final String ORIENTATION_SURFAIRTEMP = "direct";
	
	public static final String DRYAIRTEMP = "mbDryAirTemp";
	public static final DataType DATA_TYPE_DRYAIRTEMP = DataType.SHORT;
	public static final String TYPE_DRYAIRTEMP = "real";
	public static final String LONG_NAME_DRYAIRTEMP = "Dry air temperature";
	public static final String NAME_CODE_DRYAIRTEMP = "MB_PROFILE_DRY_TEMP";
	public static final String UNITS_DRYAIRTEMP = "degreeC";
	public static final String UNIT_CODE_DRYAIRTEMP = "MB_DEGREE_CENT";
	public static final double ADD_OFFSET_DRYAIRTEMP = 0.0;
	public static final double SCALE_FACTOR_DRYAIRTEMP = 0.01;
	public static final double MINIMUM_DRYAIRTEMP = -500.0;
	public static final double MAXIMUM_DRYAIRTEMP = 4500.0;
	public static final int VALID_MINIMUM_DRYAIRTEMP = -500;
	public static final int VALID_MAXIMUM_DRYAIRTEMP = 4500;
	public static final int MISSING_VALUE_DRYAIRTEMP = 32767;
	public static final String FORMAT_C_DRYAIRTEMP = "%.2f";
	public static final String ORIENTATION_DRYAIRTEMP = "direct";
	
	public static final String HUMIDAIRTEMP = "mbHumidAirTemp";
	public static final DataType DATA_TYPE_HUMIDAIRTEMP = DataType.SHORT;
	public static final String TYPE_HUMIDAIRTEMP = "real";
	public static final String LONG_NAME_HUMIDAIRTEMP = "Humid air temperature";
	public static final String NAME_CODE_HUMIDAIRTEMP = "MB_PROFILE_HUMID_TEMP";
	public static final String UNITS_HUMIDAIRTEMP = "degreeC";
	public static final String UNIT_CODE_HUMIDAIRTEMP = "MB_DEGREE_CENT";
	public static final double ADD_OFFSET_HUMIDAIRTEMP = 0.0;
	public static final double SCALE_FACTOR_HUMIDAIRTEMP = 0.01;
	public static final double MINIMUM_HUMIDAIRTEMP = -500.0;
	public static final double MAXIMUM_HUMIDAIRTEMP = 4500.0;
	public static final int VALID_MINIMUM_HUMIDAIRTEMP = -500;
	public static final int VALID_MAXIMUM_HUMIDAIRTEMP = 4500;
	public static final int MISSING_VALUE_HUMIDAIRTEMP = 32767;
	public static final String FORMAT_C_HUMIDAIRTEMP = "%.2f";
	public static final String ORIENTATION_HUMIDAIRTEMP = "direct";
		
	public static final String TNMG = "mbTNMG";
	public static final DataType DATA_TYPE_TNMG = DataType.CHAR;
	public static final String TYPE_TNMG = "string";
	public static final String LONG_NAME_TNMG = "TNMG";
	public static final String NAME_CODE_TNMG = "MB_PROFILE_TNMG";
	
	public static final String WIND = "mbWind";
	public static final DataType DATA_TYPE_WIND = DataType.CHAR;
	public static final String TYPE_WIND = "string";
	public static final String LONG_NAME_WIND = "Wind direction and strength";
	public static final String NAME_CODE_WIND = "MB_PROFILE_WIND";
	
	public static final String SWELL = "mbSwell";
	public static final DataType DATA_TYPE_SWELL = DataType.CHAR;
	public static final String TYPE_SWELL = "string";
	public static final String LONG_NAME_SWELL = "Swell direction and period";
	public static final String NAME_CODE_SWELL = "MB_PROFILE_SWELL";
	
	public static final String SENSORTYPE = "mbSensortype";
	public static final DataType DATA_TYPE_SENSORTYPE = DataType.CHAR;
	public static final String TYPE_SENSORTYPE = "string";
	public static final String LONG_NAME_SENSORTYPE = "Sensor type";
	public static final String NAME_CODE_SENSORTYPE = "MB_PROFILE_SENSOR_TYPE";

	
	public static final String SAMPLENUMBER = "mbSampleNumber";
	public static final DataType DATA_TYPE_SAMPLENUMBER = DataType.SHORT;
	public static final String TYPE_SAMPLENUMBER = "integer";
	public static final String LONG_NAME_SAMPLENUMBER = "Sample number";
	public static final String NAME_CODE_SAMPLENUMBER = "MB_PROFILE_SAMPLE_NB";
	public static final String UNITS_SAMPLENUMBER = "";
	public static final String UNIT_CODE_SAMPLENUMBER = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_SAMPLENUMBER = 0;
	public static final int SCALE_FACTOR_SAMPLENUMBER = 1;
	public static final int MINIMUM_SAMPLENUMBER = 1;
	public static final int MAXIMUM_SAMPLENUMBER = 65535;
	public static final int VALID_MINIMUM_SAMPLENUMBER = 1;
	public static final int VALID_MAXIMUM_SAMPLENUMBER = 65535;
	public static final int MISSING_VALUE_SAMPLENUMBER = 0;
	public static final String FORMAT_C_SAMPLENUMBER = "%d";
	public static final String ORIENTATION_SAMPLENUMBER = "direct";
	
	public static final String PROFILECOMMENT = "mbProfileComment";
	public static final DataType DATA_TYPE_PROFILECOMMENT = DataType.CHAR;
	public static final String TYPE_PROFILECOMMENT = "string";
	public static final String LONG_NAME_PROFILECOMMENT = "Profile comment";
	public static final String NAME_CODE_PROFILECOMMENT = "MB_PROFILE_COMMENT";
	
	public static final String DEPTH = "mbDepth";
	public static final DataType DATA_TYPE_DEPTH = DataType.INT;
	public static final String TYPE_DEPTH = "real";
	public static final String LONG_NAME_DEPTH = "Layer depth";
	public static final String NAME_CODE_DEPTH = "MB_PROFILE_DEPTH";
	public static final String UNITS_DEPTH = "m";
	public static final String UNIT_CODE_DEPTH = "MB_M";
	public static final double ADD_OFFSET_DEPTH = 0.0;
	public static final double SCALE_FACTOR_DEPTH = 0.01;
	public static final double MINIMUM_DEPTH = 0.0;
	public static final double MAXIMUM_DEPTH = 2.147483646E9;
	public static final int VALID_MINIMUM_DEPTH = 0;
	public static final int VALID_MAXIMUM_DEPTH = 2147483646;
	public static final int MISSING_VALUE_DEPTH = Integer.MAX_VALUE;
	public static final String FORMAT_C_DEPTH = "%.2f";
	public static final String ORIENTATION_DEPTH = "direct";
	
	public static final String SOUNDVELOCITY = "mbSoundVelocity";
	public static final DataType DATA_TYPE_SOUNDVELOCITY = DataType.SHORT;
	public static final String TYPE_SOUNDVELOCITY = "real";
	public static final String LONG_NAME_SOUNDVELOCITY = "Sound velocity";
	public static final String NAME_CODE_SOUNDVELOCITY = "MB_PROFILE_VELOCITY";
	public static final String UNITS_SOUNDVELOCITY = "m/s";
	public static final String UNIT_CODE_SOUNDVELOCITY = "MB_MS";
	public static final double ADD_OFFSET_SOUNDVELOCITY = 1400.0;
	public static final double SCALE_FACTOR_SOUNDVELOCITY = 0.01;
	public static final double MINIMUM_SOUNDVELOCITY = 0.0;
	public static final double MAXIMUM_SOUNDVELOCITY = 32767.0;
	public static final int VALID_MINIMUM_SOUNDVELOCITY = 0;
	public static final int VALID_MAXIMUM_SOUNDVELOCITY = 32767;
	public static final int MISSING_VALUE_SOUNDVELOCITY = -32768;
	public static final String FORMAT_C_SOUNDVELOCITY = "%.2f";
	public static final String ORIENTATION_SOUNDVELOCITY = "direct";
	
	public static final String TEMPERATURE = "mbTemperature";
	public static final DataType DATA_TYPE_TEMPERATURE = DataType.SHORT;
	public static final String TYPE_TEMPERATURE = "real";
	public static final String LONG_NAME_TEMPERATURE = "Temperature";
	public static final String NAME_CODE_TEMPERATURE = "MB_PROFILE_TEMPERATURE";
	public static final String UNITS_TEMPERATURE = "degreeC";
	public static final String UNIT_CODE_TEMPERATURE = "MB_DEGREE_CENT";
	public static final double ADD_OFFSET_TEMPERATURE = 0.0;
	public static final double SCALE_FACTOR_TEMPERATURE = 0.01;
	public static final double MINIMUM_TEMPERATURE = -500.0;
	public static final double MAXIMUM_TEMPERATURE = 4500.0;
	public static final int VALID_MINIMUM_TEMPERATURE = -500;
	public static final int VALID_MAXIMUM_TEMPERATURE = 4500;
	public static final int MISSING_VALUE_TEMPERATURE = 32767;
	public static final String FORMAT_C_TEMPERATURE = "%.2f";
	public static final String ORIENTATION_TEMPERATURE = "direct";
	
	public static final String SALINITY = "mbSalinity"; // 
	public static final DataType DATA_TYPE_SALINITY = DataType.SHORT;
	public static final String TYPE_SALINITY = "real";
	public static final String LONG_NAME_SALINITY = "Salinity";
	public static final String NAME_CODE_SALINITY = "MB_PROFILE_SALINITY";
	public static final String UNITS_SALINITY = "0/00";
	public static final String UNIT_CODE_SALINITY = "MB_PM";
	public static final double ADD_OFFSET_SALINITY = 0.0;
	public static final double SCALE_FACTOR_SALINITY = 0.01;
	public static final double MINIMUM_SALINITY = 0.0;
	public static final double MAXIMUM_SALINITY = 4500.0;
	public static final int VALID_MINIMUM_SALINITY = 0;
	//min and max in  short values: variable written as a short value an not as an unsigned short values
	public static final int VALID_MAXIMUM_SALINITY = 4500;   
	public static final int MISSING_VALUE_SALINITY = 32767;  // before 65535 not in short values
	public static final String FORMAT_C_SALINITY = "%.2f";
	public static final String ORIENTATION_SALINITY = "direct";
	
	public static final String ABSORPTION = "mbAbsorption";
	public static final DataType DATA_TYPE_ABSORPTION = DataType.SHORT;
	public static final String TYPE_ABSORPTION = "real";
	public static final String LONG_NAME_ABSORPTION = "Absorption coefficient";
	public static final String NAME_CODE_ABSORPTION = "MB_PROFILE_ABSORPTION";
	public static final String UNITS_ABSORPTION = "dB/km";
	public static final String UNIT_CODE_ABSORPTION = "MB_DBKM";
	public static final double ADD_OFFSET_ABSORPTION = 0.0;
	public static final double SCALE_FACTOR_ABSORPTION = 0.01;
	public static final double MINIMUM_ABSORPTION = 0.0;
	public static final double MAXIMUM_ABSORPTION = 20000.0;
	public static final int VALID_MINIMUM_ABSORPTION = 0;
	public static final int VALID_MAXIMUM_ABSORPTION = 20000;
	public static final int MISSING_VALUE_ABSORPTION = 32767; // before 65535 not in short values
	public static final String FORMAT_C_ABSORPTION = "%.2f";
	public static final String ORIENTATION_ABSORPTION = "direct";
	

}
