package fr.ifremer.globe.core.io.vel.export.writers;

import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This interface provides methods to define and fill an netcdf variable
 */
public interface IVelVariableWriter {
	
	

	/**
	 * Declares the variable
	 */
	//public void define(NetcdfFile file, List<NCDimension> dim) throws NCException;
	public void define(NetcdfFile file) throws NCException;
	

	/**
	 * Writes data in the variable
	 */
	public void writeData(ISoundVelocityData soundVelocityData)
			throws NCException, GIOException;

	/**
	 * Optional method to write global attributes
	 */
	public default void writeGlobalAttributes(NetcdfFile file) throws NCException {
	}
}
