package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesDateTimeWriter implements IVelVariableWriter {

	private static final DataType BUFFER_TYPE = DataType.INT;

	/** Properties **/
	private NetcdfVariable timeVariable;
	private NetcdfVariable dateVariable;

	private long maxDateTime;
	private long minDateTime = Long.MAX_VALUE;
	
	private NCDimension dim;

	public ProfilesDateTimeWriter(NCDimension dimNbrProfiles) {
		dim = dimNbrProfiles;
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		// DATE
		dateVariable = file.addVariable(VelocityConstants.NAME_DATE, DataType.INT, Arrays.asList(dim));
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_DATE);
		dateVariable.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_DATE);

		// TIME
		timeVariable = file.addVariable(VelocityConstants.NAME_TIME, DataType.INT, Arrays.asList(dim));
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_TIME);
		timeVariable.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_TIME);
	}

	@Override
	public void writeData(ISoundVelocityData soundVelocityData)
			throws NCException, GIOException {

		List<ISoundVelocityProfile> listSoundVelocityProfile = soundVelocityData.getSoundVelocityProfiles();
		// Initialize buffers
		ByteBuffer dateBuffer = ByteBuffer.allocate(listSoundVelocityProfile.size() * BUFFER_TYPE.getSize());
		dateBuffer.order(ByteOrder.nativeOrder());

		ByteBuffer timeBuffer = ByteBuffer.allocate(listSoundVelocityProfile.size() * BUFFER_TYPE.getSize());
		timeBuffer.order(ByteOrder.nativeOrder());

		// Fill buffers
		int[] dateAndTime;
		// Date of the first profile
		Date profileDate = listSoundVelocityProfile.get(0).getDate();
		minDateTime = profileDate.getTime();
		// Date of the last profile
		profileDate = listSoundVelocityProfile.get(listSoundVelocityProfile.size() - 1).getDate();
		maxDateTime = profileDate.getTime();

		for (ISoundVelocityProfile profile : listSoundVelocityProfile) {
			dateAndTime = DateUtils.getDateTime(profile.getDate().getTime());
			// ADD OFFSET ISSUE : DEFINED BUT NOT USED IN PREVIOUS VELOCITY WRITER!!
			dateBuffer.putInt((int) (dateAndTime[0] + VelocityConstants.ADD_OFFSET_DATE));
			timeBuffer.putInt(dateAndTime[1]);
		}

		// Write
		dateVariable.write(new long[] { listSoundVelocityProfile.size() }, dateBuffer, BUFFER_TYPE, Optional.empty());
		timeVariable.write(new long[] { listSoundVelocityProfile.size() }, timeBuffer, BUFFER_TYPE, Optional.empty());
	}

	/**
	 * Writes start and end date/time global attributes
	 */
	@Override
	public void writeGlobalAttributes(NetcdfFile file) throws NCException {
		int[] dateAndTime = DateUtils.getDateTime(minDateTime);
		file.addAttribute(VelocityConstants.StartDate, (int) (dateAndTime[0] + VelocityConstants.ADD_OFFSET_DATE));
		file.addAttribute(VelocityConstants.StartTime, dateAndTime[1]);

		dateAndTime = DateUtils.getDateTime(maxDateTime);
		file.addAttribute(VelocityConstants.EndDate, (int) (dateAndTime[0] + VelocityConstants.ADD_OFFSET_DATE));
		file.addAttribute(VelocityConstants.EndTime, dateAndTime[1]);
	}

}
