package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesSerialNumberWriter extends SimpleVariableWriter {

	//private NCDimension dimCommentLength;

	/** Constructor **/
	public ProfilesSerialNumberWriter(NCDimension dimNbrProfiles, NCDimension dimCommentLength) {
		super(VelocityConstants.SERIALNUMBER, VelocityConstants.DATA_TYPE_SERIALNUMBER, DataType.CHAR, Arrays.asList(dimNbrProfiles, dimCommentLength));
		//this.dimCommentLength = dimCommentLength;
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_SERIALNUMBER);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_SERIALNUMBER);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_SERIALNUMBER);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		//buffer.put(java.util.Arrays.copyOf(Integer.toString(profile.getSerialNumber()).getBytes(), (int) dimCommentLength.getLength()));
		buffer.put(java.util.Arrays.copyOf(Integer.toString(profile.getSerialNumber()).getBytes(), (int) var.getDimensions()[1]));

	}
}
