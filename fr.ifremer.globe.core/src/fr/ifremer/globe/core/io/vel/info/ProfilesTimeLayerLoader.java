/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.core.io.vel.info;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.LongBuffer1D;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Computes the swath time layer of the sounder data container
 */
public class ProfilesTimeLayerLoader implements ILayerLoader<LongBuffer1D> {

	/** Properties **/
	private final long[] shape;
	private final IntLoadableLayer1D dateLayer;
	private final IntLoadableLayer1D timeLayer;
	private final AtomicBoolean loading = new AtomicBoolean(false);

	/**
	 * Constructor
	 */
	public ProfilesTimeLayerLoader(IntLoadableLayer1D dateLayer, IntLoadableLayer1D timeLayer) {
		this.shape = timeLayer.getDimensions();
		this.dateLayer = dateLayer;
		this.timeLayer = timeLayer;
	}

	@Override
	public long[] getDimensions() throws GIOException {
		return new long[] { shape[0] };
	}

	@Override
	public boolean load(LongBuffer1D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			for (var i = 0; i < shape[0]; i++)
				buffer.set(i, DateUtils.getEpochMilliFromJulian(dateLayer.get(i), timeLayer.get(i)));
		}
		loading.set(false);
		return true;
	}

	@Override
	public boolean flush(LongBuffer1D buffer) throws GIOException {
		return false;
	}

}
