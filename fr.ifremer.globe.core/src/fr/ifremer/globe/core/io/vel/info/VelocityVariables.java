package fr.ifremer.globe.core.io.vel.info;

/**
 * Class containing all the strings (variable names, flags, ...)
 * @author pmahoudo
 *
 */
public class VelocityVariables {
	
	// Dimension
	public static final String historyRecNbr = "mbHistoryRecNbr";
	public static final String nameLength = "mbNameLength";
	public static final String commentLength = "mbCommentLength";
	public static final String profilMaxNbr = "mbProfilMaxNbr";
	public static final String levelMaxNbr = "mbLevelMaxNbr";
	
	// Global attributes
	public static final String version = "mbVersion";
	public static final String name = "mbName";
	public static final String classe = "mbClasse";
	public static final String level = "mbLevel";
	public static final String nbrHistoryRec = "mbNbrHistoryRec";
	public static final String timeReference = "mbTimeReference";
	public static final String startDate = "mbStartDate";
	public static final String startTime = "mbStartTime";
	public static final String endDate = "mbEndDate";
	public static final String endTime = "mbEndTime";
	public static final String northLatitude = "mbNorthLatitude";
	public static final String southLatitude = "mbSouthLatitude";
	public static final String eastLongitude = "mbEastLongitude";
	public static final String westLongitude = "mbWestLongitude";
	public static final String meridian180 = "mbMeridian180";
	public static final String geodesicSystem = "mbGeodesicSystem";
	public static final String ellipsoidName = "mbEllipsoidName";
	public static final String ellipsoidA = "mbEllipsoidA";
	public static final String ellipsoidInvF = "mbEllipsoidInvF";
	public static final String ellipsoidE2  = "mbEllipsoidE2 ";
	public static final String projType = "mbProjType";
	public static final String projParameterValue = "mbProjParameterValue";
	public static final String projParameterCode= "mbProjParameterCode";
	public static final String CDI = "mbCDI";
	public static final String nbrProfil = "mbNbrProfil";
	
	// Variables
	public static final String histDate = "mbHistDate";
	public static final String histTime = "mbHistTime";
	public static final String histCode = "mbHistCode";
	public static final String histAutor = "mbHistAutor";
	public static final String histModule = "mbHistModule";
	public static final String histComment = "mbHistComment";
	public static final String date = "mbDate";
	public static final String time = "mbTime";
	public static final String ordinate = "mbOrdinate";
	public static final String abscissa = "mbAbscissa";
	public static final String nbrLevel = "mbNbrLevel";
	public static final String type = "mbType";
	public static final String velCompAlgo = "mbVelCompAlgo";
	public static final String carrier = "mbCarrier";
	public static final String sequence = "mbSequence";
	public static final String serialNumber = "mbSerialNumber";
	public static final String profileSalinity = "mbProfileSalinity";
	public static final String profileDepth = "mbProfileDepth";
	public static final String pressure = "mbPressure";
	public static final String surfAirTemp = "mbSurfAirTemp";
	public static final String dryAirtemp = "mbDryAirTemp";
	public static final String humidAirTemp = "mbHumidAirTemp";
	public static final String tnmg = "mbTNMG";
	public static final String wind = "mbWind";
	public static final String swell = "mbSwell";
	public static final String sensortype = "mbSensortype";
	public static final String sampleNumber = "mbSampleNumber";
	public static final String profileComment = "mbProfileComment";
	public static final String depth = "mbDepth";
	public static final String soundVelocity = "mbSoundVelocity";
	public static final String temperature = "mbTemperature";
	public static final String salinity = "mbSalinity";
	public static final String absorption = "mbAbsorption";
	

}
