package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesSalinityWriter extends SimpleVariableWriter {

	private float missingValue;

	/** Constructor **/
	public ProfilesSalinityWriter(NCDimension dimNbrProfiles) {
		super(VelocityConstants.PROFILESALINITY, VelocityConstants.DATA_TYPE_PROFILESALINITY, DataType.FLOAT, Arrays.asList(dimNbrProfiles));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_SALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_PROFILESALINITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_PROFILESALINITY);
		
		missingValue = (float) (VelocityConstants.MISSING_VALUE_PROFILESALINITY * VelocityConstants.SCALE_FACTOR_SALINITY + VelocityConstants.ADD_OFFSET_SALINITY);

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		if (Float.isNaN(profile.getProfileSalinity())) 
			buffer.putFloat(missingValue);
		else 
			buffer.putFloat(profile.getProfileSalinity());
	}
}
