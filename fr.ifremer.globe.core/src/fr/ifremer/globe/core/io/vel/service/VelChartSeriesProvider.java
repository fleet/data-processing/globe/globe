package fr.ifremer.globe.core.io.vel.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.vel.info.VelInfo;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.BasicChartSeries;
import fr.ifremer.globe.core.model.chart.impl.ChartFileInfoDataSource;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartData;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides method to get {@link IChartSeries} from {@link VelInfo}.
 */
@Component(name = "globe_drivers_vel_chart_series_supplier", service = { IChartSeriesSupplier.class })
public class VelChartSeriesProvider implements IChartSeriesSupplier {

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.VEL_NETCDF_4);
	}

	/**
	 * @return a list of {@link IChartSeries}
	 */
	@Override
	public List<SoundVelocityProfileChartSeries> getChartSeries(IFileInfo fileInfo) throws GIOException {
		if (fileInfo instanceof VelInfo) {
			var VelInfo = (VelInfo) fileInfo;
			return VelInfo.getSoundVelocityProfiles().stream()
					.map(svp -> new SoundVelocityProfileChartSeries(VelInfo, svp)).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	/**
	 * Implementation of {@link IChartSeries} for {@link ISoundVelocityProfile}.
	 */
	public static class SoundVelocityProfileChartSeries extends BasicChartSeries<IChartData> {

		private final List<SimpleChartData> chartDataList = new ArrayList<>();

		/**
		 * Constructor
		 */
		public SoundVelocityProfileChartSeries(VelInfo velInfo, ISoundVelocityProfile soundVelocityProfile) {
			super("Sound velocity profile " + soundVelocityProfile.getDate().toInstant(),
					new ChartFileInfoDataSource<VelInfo>(velInfo));
			AtomicInteger index = new AtomicInteger();
			soundVelocityProfile.getSoundVelocities().forEach(soundVelocity -> {
				var chartData = new SimpleChartData(this, index.get(), soundVelocity.getSoundVelocity(),
						-soundVelocity.getDepth());
				chartDataList.add(chartData);
			});
		}

		@Override
		public int size() {
			return chartDataList.size();
		}

		@Override
		public IChartData getData(int index) {
			return chartDataList.get(index);
		}

	}

}
