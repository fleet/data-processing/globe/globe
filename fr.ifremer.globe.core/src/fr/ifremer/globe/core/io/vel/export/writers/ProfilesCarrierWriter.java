package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesCarrierWriter extends SimpleVariableWriter {
	private NCDimension dimCommentLength;


	/** Constructor **/
	public ProfilesCarrierWriter(NCDimension dimNbrProfiles, NCDimension dimCommentLength) {
		super(VelocityConstants.CARRIER, VelocityConstants.DATA_TYPE_CARRIER, DataType.CHAR, Arrays.asList(dimNbrProfiles, dimCommentLength));
		this.dimCommentLength = dimCommentLength;
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_CARRIER);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_CARRIER);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_CARRIER);

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		//The array must be the size of the second dimension (comment length)
		buffer.put(Arrays.copyOf(profile.getCarrier().getBytes(), (int) dimCommentLength.getLength()));		
	}

}
