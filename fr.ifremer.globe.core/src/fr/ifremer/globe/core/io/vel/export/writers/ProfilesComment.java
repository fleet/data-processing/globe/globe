package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesComment extends SimpleVariableWriter {

	/** Constructor **/
	public ProfilesComment(NCDimension dimNbrProfiles, NCDimension dimCommentLength) {
		super(VelocityConstants.PROFILECOMMENT, VelocityConstants.DATA_TYPE_PROFILECOMMENT, DataType.CHAR,
				Arrays.asList(dimNbrProfiles, dimCommentLength));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_PROFILECOMMENT);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_PROFILECOMMENT);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_PROFILECOMMENT);

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		// The array must be the size of the second dimension (comment length)
		buffer.put(Arrays.copyOf(profile.getProfileComment().getBytes(), (int) var.getDimensions()[1]));

	}

}
