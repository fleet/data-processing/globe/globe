package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public abstract class SimpleVariableWriter implements IVelVariableWriter {

	/** Properties **/
	private final String name;
	protected final DataType dataType;
	protected final DataType bufferType; // = unpacked data type (before scale factor and add offset)
	protected NetcdfVariable var;
	private List<NCDimension> dim;

	/** Constructor **/
	protected SimpleVariableWriter(String name, DataType dataType, DataType bufferType, List<NCDimension> dimensions) {
		this.name = name;
		this.dataType = dataType;
		this.bufferType = bufferType;
		this.dim = dimensions;
	}
	
	@Override
	public void define(NetcdfFile file) throws NCException {
		var = file.addVariable(name, dataType, dim);
	}

	@Override
	public void writeData(ISoundVelocityData soundVelocityData)
			throws NCException, GIOException {
		long[] arrayDim = var.getDimensions();
		int valueCount = (int) Arrays.stream(arrayDim).reduce(1, (a, b) -> {
			return a * b;
		});
		ByteBuffer buffer = ByteBuffer.allocate(valueCount * bufferType.getSize());
		buffer.order(ByteOrder.nativeOrder());

		for (ISoundVelocityProfile profile : soundVelocityData.getSoundVelocityProfiles())
			fillBufferToWrite(buffer, profile);
		
		var.write(arrayDim , buffer, bufferType, Optional.empty());
	}

	/**
	 * Fills the buffer which will be used by the write operation
	 */
	protected abstract void fillBufferToWrite(ByteBuffer bb, ISoundVelocityProfile profile)
			throws NCException, GIOException;
	
}
