package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.core.model.velocity.SoundVelocity;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class LevelsSoundVelocityValuesWriter extends SimpleVariableWriter {

	private double missingValue;

	/** Constructor **/
	public LevelsSoundVelocityValuesWriter(NCDimension dimNbrProfiles, NCDimension dimNbrLevels) {
		super(VelocityConstants.SOUNDVELOCITY, VelocityConstants.DATA_TYPE_SOUNDVELOCITY, DataType.DOUBLE, Arrays.asList(dimNbrProfiles, dimNbrLevels));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_SOUNDVELOCITY);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_SOUNDVELOCITY);
		
		missingValue = VelocityConstants.MISSING_VALUE_SOUNDVELOCITY * VelocityConstants.SCALE_FACTOR_SOUNDVELOCITY + VelocityConstants.ADD_OFFSET_SOUNDVELOCITY;

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		for (SoundVelocity velocity : profile.getSoundVelocities()) {
			if (Double.isNaN(velocity.getSoundVelocity()))
				buffer.putDouble(missingValue);
			else				
				buffer.putDouble(velocity.getSoundVelocity());
		}

	}

}
