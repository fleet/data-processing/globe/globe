package fr.ifremer.globe.core.io.vel.service;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.vel.export.VelExporter;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.IVelWriter;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * This class provides methods to generate sound velocity file (.vel)
 */
@Component(name = "globe_drivers_vel_writer", service = IVelWriter.class)
public class VelWriter implements IVelWriter {

	/**
	 * Returns the service implementation of IVelWriter.
	 */
	static IVelWriter grab() {
		return OsgiUtils.getService(IVelWriter.class);
	}

	/** {@inheritDoc} */
	@Override
	public void write(ISoundVelocityData soundVelocityData, String outputFilePath) throws GIOException {
		VelExporter.export(soundVelocityData, outputFilePath);
	}
}
