package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.io.vel.info.HistoryConstants;
import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class defines and write history variables.
 */
public class HistoryWriter implements IVelVariableWriter {

	/** History variables **/
	private NetcdfVariable dateVar;
	private NetcdfVariable timeVar;
	private NetcdfVariable codeVar;
	private NetcdfVariable autorVar;
	private NetcdfVariable moduleVar;
	private NetcdfVariable commentVar;

	@Override
	public void define(NetcdfFile file) throws NCException {
		// Add history dimensions
		NCDimension historyRecNbr = file.addDimension(HistoryConstants.DIM_RECORD_NUMBER,
				HistoryConstants.DIM_RECORD_NUMBER_VALUE);
		NCDimension nameLength = file.addDimension(HistoryConstants.DIM_NAME_LENGTH,
				HistoryConstants.DIM_NAME_LENGTH_VALUE);
		NCDimension commentLength = file.addDimension(HistoryConstants.DIM_COMMENT_LENGTH,
				HistoryConstants.DIM_COMMENT_LENGTH_VALUE);

		List<NCDimension> dimRecNbr = Arrays.asList(historyRecNbr);
		List<NCDimension> dimRecNbrNameLength = Arrays.asList(historyRecNbr, nameLength);
		List<NCDimension> dimRecNbrCommentLength = Arrays.asList(historyRecNbr, commentLength);

		// DATE
		dateVar = file.addVariable(HistoryConstants.DATE_VALUE, DataType.INT, dimRecNbr);
		dateVar.addAttribute(HistoryConstants.TYPE, HistoryConstants.TYPE_DATE);
		dateVar.addAttribute(HistoryConstants.LONG_NAME, HistoryConstants.LONG_NAME_DATE);
		dateVar.addAttribute(HistoryConstants.NAME_CODE, HistoryConstants.NAME_CODE_DATE);
		dateVar.addAttribute(HistoryConstants.UNITS, HistoryConstants.UNITS_DATE);
		dateVar.addAttribute(HistoryConstants.UNIT_CODE, HistoryConstants.UNIT_CODE_DATE);
		dateVar.addAttribute(HistoryConstants.ADD_OFFSET, HistoryConstants.ADD_OFFSET_DATE);
		dateVar.addAttribute(HistoryConstants.SCALE_FACTOR, HistoryConstants.SCALE_FACTOR_DATE);
		dateVar.addAttribute(HistoryConstants.MINIMUM, HistoryConstants.MINIMUM_DATE);
		dateVar.addAttribute(HistoryConstants.MAXIMUM, HistoryConstants.VALID_MAXIMUM_DATE);
		dateVar.addAttribute(HistoryConstants.VALID_MINIMUM, HistoryConstants.VALID_MINIMUM_DATE);
		dateVar.addAttribute(HistoryConstants.VALID_MAXIMUM, HistoryConstants.VALID_MAXIMUM_DATE);
		dateVar.addAttribute(HistoryConstants.MISSING_VALUE, HistoryConstants.MISSING_VALUE_DATE);
		dateVar.addAttribute(HistoryConstants.FORMAT_C, HistoryConstants.FORMAT_C_DATE);
		dateVar.addAttribute(HistoryConstants.ORIENTATION, HistoryConstants.ORIENTATION_DATE);

		// TIME
		timeVar = file.addVariable(HistoryConstants.TIME_VALUE, DataType.INT, dimRecNbr);
		timeVar.addAttribute(HistoryConstants.TYPE, HistoryConstants.TYPE_TIME);
		timeVar.addAttribute(HistoryConstants.LONG_NAME, HistoryConstants.LONG_NAME_TIME);
		timeVar.addAttribute(HistoryConstants.NAME_CODE, HistoryConstants.NAME_CODE_TIME);
		timeVar.addAttribute(HistoryConstants.UNITS, HistoryConstants.UNITS_TIME);
		timeVar.addAttribute(HistoryConstants.UNIT_CODE, HistoryConstants.UNIT_CODE_TIME);
		timeVar.addAttribute(HistoryConstants.ADD_OFFSET, HistoryConstants.ADD_OFFSET_TIME);
		timeVar.addAttribute(HistoryConstants.SCALE_FACTOR, HistoryConstants.SCALE_FACTOR_TIME);
		timeVar.addAttribute(HistoryConstants.MINIMUM, HistoryConstants.MINIMUM_TIME);
		timeVar.addAttribute(HistoryConstants.MAXIMUM, HistoryConstants.VALID_MAXIMUM_TIME);
		timeVar.addAttribute(HistoryConstants.VALID_MINIMUM, HistoryConstants.VALID_MINIMUM_TIME);
		timeVar.addAttribute(HistoryConstants.VALID_MAXIMUM, HistoryConstants.VALID_MAXIMUM_TIME);
		timeVar.addAttribute(HistoryConstants.MISSING_VALUE, HistoryConstants.MISSING_VALUE_TIME);
		timeVar.addAttribute(HistoryConstants.FORMAT_C, HistoryConstants.FORMAT_C_TIME);
		timeVar.addAttribute(HistoryConstants.ORIENTATION, HistoryConstants.ORIENTATION_TIME);

		// CODE
		codeVar = file.addVariable(HistoryConstants.CODE_VALUE, DataType.CHAR, dimRecNbr);
		codeVar.addAttribute(HistoryConstants.TYPE, HistoryConstants.TYPE_CODE);
		codeVar.addAttribute(HistoryConstants.LONG_NAME, HistoryConstants.LONG_NAME_CODE);
		codeVar.addAttribute(HistoryConstants.NAME_CODE, HistoryConstants.NAME_CODE_CODE);
		codeVar.addAttribute(HistoryConstants.UNITS, HistoryConstants.UNITS_CODE);
		codeVar.addAttribute(HistoryConstants.UNIT_CODE, HistoryConstants.UNIT_CODE_CODE);
		codeVar.addAttribute(HistoryConstants.ADD_OFFSET, HistoryConstants.ADD_OFFSET_CODE);
		codeVar.addAttribute(HistoryConstants.SCALE_FACTOR, HistoryConstants.SCALE_FACTOR_CODE);
		codeVar.addAttribute(HistoryConstants.MINIMUM, HistoryConstants.MINIMUM_CODE);
		codeVar.addAttribute(HistoryConstants.MAXIMUM, HistoryConstants.VALID_MAXIMUM_CODE);
		codeVar.addAttribute(HistoryConstants.VALID_MINIMUM, HistoryConstants.VALID_MINIMUM_CODE);
		codeVar.addAttribute(HistoryConstants.VALID_MAXIMUM, HistoryConstants.VALID_MAXIMUM_CODE);
		codeVar.addAttribute(HistoryConstants.MISSING_VALUE, HistoryConstants.MISSING_VALUE_CODE);
		codeVar.addAttribute(HistoryConstants.FORMAT_C, HistoryConstants.FORMAT_C_CODE);
		codeVar.addAttribute(HistoryConstants.ORIENTATION, HistoryConstants.ORIENTATION_CODE);

		// AUTHOR
		autorVar = file.addVariable(HistoryConstants.AUTOR_VALUE, DataType.CHAR, dimRecNbrNameLength);
		autorVar.addAttribute(HistoryConstants.TYPE, HistoryConstants.TYPE_AUTOR);
		autorVar.addAttribute(HistoryConstants.LONG_NAME, HistoryConstants.LONG_NAME_AUTOR);
		autorVar.addAttribute(HistoryConstants.NAME_CODE, HistoryConstants.NAME_CODE_AUTOR);

		// MODULE
		moduleVar = file.addVariable(HistoryConstants.MODULE_VALUE, DataType.CHAR, dimRecNbrNameLength);
		moduleVar.addAttribute(HistoryConstants.TYPE, HistoryConstants.TYPE_MODULE);
		moduleVar.addAttribute(HistoryConstants.LONG_NAME, HistoryConstants.LONG_NAME_MODULE);
		moduleVar.addAttribute(HistoryConstants.NAME_CODE, HistoryConstants.NAME_CODE_MODULE);

		// COMMENT
		commentVar = file.addVariable(HistoryConstants.COMMENT_VALUE, DataType.CHAR, dimRecNbrCommentLength);
		commentVar.addAttribute(HistoryConstants.TYPE, HistoryConstants.TYPE_COMMENT);
		commentVar.addAttribute(HistoryConstants.LONG_NAME, HistoryConstants.LONG_NAME_COMMENT);
		commentVar.addAttribute(HistoryConstants.NAME_CODE, HistoryConstants.NAME_CODE_COMMENT);
	}

	@Override
	public void writeData(ISoundVelocityData soundVelocityData)
			throws NCException, GIOException {

		int[] currentDateTime = DateUtils.getJulianDateTime(Instant.now());
		// DATE
		ByteBuffer buffer = ByteBuffer.allocate(DataType.INT.getSize()).order(ByteOrder.nativeOrder());
		buffer.putInt(currentDateTime[0]);
		dateVar.write(new long[] { 1 }, buffer, DataType.INT, Optional.empty());

		// TIME
		buffer = ByteBuffer.allocate(DataType.INT.getSize()).order(ByteOrder.nativeOrder());
		buffer.putInt(currentDateTime[1]);
		timeVar.write(new long[] { 1 }, buffer, DataType.INT, Optional.empty());

		// CODE
		// Do nothing

		// AUTHOR
		buffer = ByteBuffer.allocate(HistoryConstants.DIM_NAME_LENGTH_VALUE * DataType.CHAR.getSize())
				.order(ByteOrder.nativeOrder());
		String author = trimToSize(System.getProperty("user.name"), HistoryConstants.DIM_NAME_LENGTH_VALUE);
		buffer.put(author.getBytes());
		autorVar.write(new long[] { 1, HistoryConstants.DIM_NAME_LENGTH_VALUE }, buffer, DataType.CHAR,
				Optional.empty());

		// MODULE
		buffer = ByteBuffer.allocate(HistoryConstants.DIM_NAME_LENGTH_VALUE * DataType.CHAR.getSize())
				.order(ByteOrder.nativeOrder());
		buffer.put(VelocityConstants.MODULE_NAME.getBytes());
		moduleVar.write(new long[] { 1, HistoryConstants.DIM_NAME_LENGTH_VALUE }, buffer, DataType.CHAR,
				Optional.empty());

		// COMMENT
		buffer = ByteBuffer.allocate(HistoryConstants.DIM_COMMENT_LENGTH_VALUE * DataType.CHAR.getSize())
				.order(ByteOrder.nativeOrder());
		String comment = trimToSize("Origin file : " + soundVelocityData.getOriginFileName(),
				HistoryConstants.DIM_COMMENT_LENGTH_VALUE);
		buffer.put(comment.getBytes());
		commentVar.write(new long[] { 1, HistoryConstants.DIM_COMMENT_LENGTH_VALUE }, buffer, DataType.CHAR,
				Optional.empty());
	}

	/**
	 * Writes start and end date/time global attributes
	 */
	@Override
	public void writeGlobalAttributes(NetcdfFile file) throws NCException {
		file.addAttribute(VelocityConstants.NbrHistoryRec, (short) 1);
	}

	private String trimToSize(String name, int size) {
		return name.substring(0, Math.min(name.length(), size));
	}

}
