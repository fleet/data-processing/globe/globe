package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesLongitudeWriter extends SimpleVariableWriter {

	/** Properties **/
	private double max = Double.NaN;
	private double min = Double.NaN;

	/** Constructor **/
	public ProfilesLongitudeWriter(NCDimension dimNbrProfiles) {
		super(VelocityConstants.NAME_LONGITUDE, VelocityConstants.DATA_TYPE_LONGITUDE, DataType.DOUBLE, Arrays.asList(dimNbrProfiles));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_LONGITUDE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_LONGITUDE);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		double value = profile.getLon();
		if (!Double.isNaN(value)) { 
			max = Double.isNaN(max) ? value : Math.max(max, value);
			min = Double.isNaN(min) ? value : Math.min(min, value);
		} 
		buffer.putDouble(value);
	}
	@Override
	public void writeGlobalAttributes(NetcdfFile file) throws NCException {
		file.addAttribute(VelocityConstants.EastLongitude, max);
		file.addAttribute(VelocityConstants.WestLongitude, min);
	}
}
