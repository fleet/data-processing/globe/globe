package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesSurfAirTempWriter extends SimpleVariableWriter {
	float missingValue;

	/** Constructor **/
	public ProfilesSurfAirTempWriter(NCDimension dimNbrProfiles) {
		super(VelocityConstants.SURFAIRTEMP, VelocityConstants.DATA_TYPE_SURFAIRTEMP, DataType.FLOAT, Arrays.asList(dimNbrProfiles));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_SURFAIRTEMP);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_SURFAIRTEMP);
		
		missingValue = (float) (VelocityConstants.MISSING_VALUE_SURFAIRTEMP * VelocityConstants.SCALE_FACTOR_SURFAIRTEMP + VelocityConstants.ADD_OFFSET_SURFAIRTEMP);

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		if (Float.isNaN(profile.getSurfAirTemp()))
			buffer.putFloat(missingValue);
		else
			buffer.putFloat(profile.getSurfAirTemp());
	}

}
