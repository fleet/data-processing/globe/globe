package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesPressureWriter extends SimpleVariableWriter {


	private double missingValue;

	/** Constructor **/
	public ProfilesPressureWriter(NCDimension dimNbrProfiles) {
		super(VelocityConstants.PRESSURE, VelocityConstants.DATA_TYPE_PRESSURE, DataType.DOUBLE, Arrays.asList(dimNbrProfiles));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_PRESSURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_PRESSURE);
		
		missingValue =  VelocityConstants.MISSING_VALUE_PRESSURE * VelocityConstants.SCALE_FACTOR_PRESSURE + VelocityConstants.ADD_OFFSET_PRESSURE;

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		if (Float.isNaN(profile.getPressure())) 
			buffer.putDouble(missingValue);
		else
			buffer.putDouble(profile.getPressure());
	}

}
