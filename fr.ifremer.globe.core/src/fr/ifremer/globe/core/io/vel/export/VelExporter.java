package fr.ifremer.globe.core.io.vel.export;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.io.vel.export.writers.HistoryWriter;
import fr.ifremer.globe.core.io.vel.export.writers.IVelVariableWriter;
import fr.ifremer.globe.core.io.vel.export.writers.LevelsAbsorptionValuesWriter;
import fr.ifremer.globe.core.io.vel.export.writers.LevelsDepthValuesWriter;
import fr.ifremer.globe.core.io.vel.export.writers.LevelsSalinityValuesWriter;
import fr.ifremer.globe.core.io.vel.export.writers.LevelsSoundVelocityValuesWriter;
import fr.ifremer.globe.core.io.vel.export.writers.LevelsTemperatureValuesWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesCarrierWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesComment;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesDateTimeWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesDepthWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesDryAirTempWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesHumidityAirTempWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesLatitudeWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesLongitudeWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesNbrLevelsWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesPressureWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesSalinityWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesSampleNumberWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesSensorTypeWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesSequenceWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesSerialNumberWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesSurfAirTempWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesSwellWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesTNMGWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesTypeWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesVelCompAlgoWriter;
import fr.ifremer.globe.core.io.vel.export.writers.ProfilesWindWriter;
import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Version;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

public class VelExporter {

	/** Private constructor **/
	private VelExporter() {
	}

	/**
	 * Export velocity data to an new velocity file
	 */
	public static boolean export(ISoundVelocityData soundVelocityData, String outputFilePath) throws GIOException {

		// Creates file
		try (NetcdfFile file = new NetcdfFile(NCFile.createNew(Version.netcdf4, outputFilePath).withCompressionDisabled())) {
			List<ISoundVelocityProfile> listSoundVelocityProfile = soundVelocityData.getSoundVelocityProfiles();
			// Write position dimension
			NCDimension dimNbrProfiles = file.addDimension(VelocityConstants.DIM_PROFILE_NBR,
					listSoundVelocityProfile.size());

			Optional<ISoundVelocityProfile> optionalProfile = listSoundVelocityProfile.stream()
					.max(Comparator.comparing(ISoundVelocityProfile::getNbrLevels));
			if (!optionalProfile.isPresent())
				throw new GIOException("No valid profile");

			NCDimension dimNbrLevels = file.addDimension(VelocityConstants.DIM_LEVELS_NBR,
					optionalProfile.get().getNbrLevels());
			NCDimension dimNameLength = file.addDimension(VelocityConstants.DIM_NAME_LENGTH, 20);
			NCDimension dimCommentLength = file.addDimension(VelocityConstants.DIM_COMMENT_LENGTH, 256);

			// Writers list
			IVelVariableWriter[] varWriters = { new HistoryWriter(),
					new ProfilesDateTimeWriter(dimNbrProfiles), new ProfilesLatitudeWriter(dimNbrProfiles),
					new ProfilesLongitudeWriter(dimNbrProfiles), new ProfilesNbrLevelsWriter(dimNbrProfiles),
					new ProfilesTypeWriter(dimNbrProfiles),
					new ProfilesVelCompAlgoWriter(dimNbrProfiles, dimCommentLength),
					new ProfilesCarrierWriter(dimNbrProfiles, dimCommentLength),
					new ProfilesSequenceWriter(dimNbrProfiles),
					new ProfilesSerialNumberWriter(dimNbrProfiles, dimCommentLength),
					new ProfilesSalinityWriter(dimNbrProfiles), new ProfilesDepthWriter(dimNbrProfiles),
					new ProfilesPressureWriter(dimNbrProfiles), new ProfilesSurfAirTempWriter(dimNbrProfiles),
					new ProfilesDryAirTempWriter(dimNbrProfiles), new ProfilesHumidityAirTempWriter(dimNbrProfiles),
					new ProfilesTNMGWriter(dimNbrProfiles, dimNameLength),
					new ProfilesWindWriter(dimNbrProfiles, dimNameLength),
					new ProfilesSwellWriter(dimNbrProfiles, dimNameLength),
					new ProfilesSensorTypeWriter(dimNbrProfiles, dimNameLength),
					new ProfilesSampleNumberWriter(dimNbrProfiles),
					new ProfilesComment(dimNbrProfiles, dimCommentLength),
					new LevelsDepthValuesWriter(dimNbrProfiles, dimNbrLevels),
					new LevelsSoundVelocityValuesWriter(dimNbrProfiles, dimNbrLevels),
					new LevelsTemperatureValuesWriter(dimNbrProfiles, dimNbrLevels),
					new LevelsSalinityValuesWriter(dimNbrProfiles, dimNbrLevels),
					new LevelsAbsorptionValuesWriter(dimNbrProfiles, dimNbrLevels) };

			// Define and write variables
			for (IVelVariableWriter writer : varWriters)
				writer.define(file);

			for (IVelVariableWriter writer : varWriters) {
				writer.writeData(soundVelocityData);
			}

			// Write global attributes
			writeGlobalAttributes(file, varWriters, listSoundVelocityProfile.size());

			file.synchronize();

		} catch (GIOException | NCException e) {
			throw new GIOException("Error while creating/writing velocity file " + e.getMessage(), e);
		}
		return true;
	}

	/**
	 * Writes global attributes
	 */
	private static void writeGlobalAttributes(NetcdfFile file, IVelVariableWriter[] varWriters, int pointNbr)
			throws NCException {
		// Pass in definition mode
		file.redef();

		file.addAttribute(VelocityConstants.Version, VelocityConstants.ACTUAL_VERSION);
		file.addAttribute(VelocityConstants.Name, FileUtils.getFileShortName(file.getName()));
		file.addAttribute(VelocityConstants.GLOBAL_ATT_CLASSE, VelocityConstants.CLASSE);
		file.addAttribute(VelocityConstants.Level, (short) 0);
		file.addAttribute(VelocityConstants.NbrHistoryRec, (short) 1);

		file.addAttribute(VelocityConstants.TimeReference, VelocityConstants.TIME_REFERENCE_ATTRIBUTE);
		// Computes some global attributes with writers( Geographic and date/time limits...)
		for (IVelVariableWriter writer : varWriters)
			writer.writeGlobalAttributes(file);

		// Ellipsoid

		file.addAttribute(VelocityConstants.EllipsoidName, "");
		file.addAttribute(VelocityConstants.EllipsoidA, 0.);
		file.addAttribute(VelocityConstants.EllipsoidInvF, 0.);
		file.addAttribute(VelocityConstants.EllipsoidE2, 0.);

		// Meridian 180 & geodesic system

		file.addAttribute(VelocityConstants.Meridian180, "");
		file.addAttribute(VelocityConstants.GeodesicSystem, "");

		// Projection
		file.addAttribute(VelocityConstants.ProjType, (short) -1);
		double[] value = { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };

		file.addAttribute(VelocityConstants.ProjParameterValue, value);
		file.addAttribute(VelocityConstants.ProjParameterCode, "");

		file.addAttribute(VelocityConstants.CDI, "");
		file.addAttribute(VelocityConstants.ProfileCounter, pointNbr);

		file.enddef();
	}
}
