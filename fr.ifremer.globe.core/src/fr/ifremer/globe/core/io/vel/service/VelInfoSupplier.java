package fr.ifremer.globe.core.io.vel.service;

import java.util.EnumSet;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.vel.info.VelInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocitySupplier;

@Component(name = "globe_vel_service_info_supplier", service = { IFileInfoSupplier.class,
		ISoundVelocitySupplier.class })
public class VelInfoSupplier extends BasicFileInfoSupplier<VelInfo> implements ISoundVelocitySupplier {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(VelInfoSupplier.class);

	/** Supported DTM type */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.VEL_NETCDF_4);

	/**
	 * Constructor
	 */
	public VelInfoSupplier() {
		super("vel", "Sound velocity file (*.vel)", ContentType.VEL_NETCDF_4);
	}

	@Override
	public VelInfo makeFileInfo(String filePath) {
		return new VelInfo(filePath);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ISoundVelocityData> getSoundVelocityData(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof VelInfo ? (VelInfo) fileInfo : null);
	}
}
