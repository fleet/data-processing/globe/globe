package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ProfilesDepthWriter extends SimpleVariableWriter {

	private double missingValue;

	/** Constructor **/
	public ProfilesDepthWriter(NCDimension dimNbrProfiles) {
		super(VelocityConstants.PROFILEDEPTH, VelocityConstants.DATA_TYPE_PROFILEDEPTH, DataType.DOUBLE, Arrays.asList(dimNbrProfiles));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_PROFILEDEPTH);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_PROFILEDEPTH);
		
		missingValue = VelocityConstants.MISSING_VALUE_PROFILEDEPTH * VelocityConstants.SCALE_FACTOR_PROFILEDEPTH + VelocityConstants.ADD_OFFSET_PROFILEDEPTH;

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		if (Double.isNaN(profile.getProfileDepth())) 
			buffer.putDouble(missingValue);
		else
		    buffer.putDouble(profile.getProfileDepth());
	}

}
