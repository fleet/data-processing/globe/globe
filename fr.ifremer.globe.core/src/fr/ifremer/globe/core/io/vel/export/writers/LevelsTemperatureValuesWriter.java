package fr.ifremer.globe.core.io.vel.export.writers;

import java.nio.ByteBuffer;
import java.util.Arrays;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.core.model.velocity.SoundVelocity;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class LevelsTemperatureValuesWriter extends SimpleVariableWriter {

	private double missingValue;

	/** Constructor **/
	public LevelsTemperatureValuesWriter(NCDimension dimNbrProfiles, NCDimension dimNbrLevels) {
		super(VelocityConstants.TEMPERATURE, VelocityConstants.DATA_TYPE_TEMPERATURE, DataType.DOUBLE, Arrays.asList(dimNbrProfiles, dimNbrLevels));
	}

	@Override
	public void define(NetcdfFile file) throws NCException {
		super.define(file);
		var.addAttribute(VelocityConstants.ATTRIBUTE_TYPE, VelocityConstants.TYPE_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_LONG_NAME, VelocityConstants.LONG_NAME_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_NAME_CODE, VelocityConstants.NAME_CODE_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNITS, VelocityConstants.UNITS_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_UNIT_CODE, VelocityConstants.UNIT_CODE_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ADD_OFFSET, VelocityConstants.ADD_OFFSET_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_SCALE_FACTOR, VelocityConstants.SCALE_FACTOR_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MIN, VelocityConstants.MINIMUM_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MAX, VelocityConstants.MAXIMUM_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MIN, VelocityConstants.VALID_MINIMUM_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_VALID_MAX, VelocityConstants.VALID_MAXIMUM_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_MISSING_VALUE, VelocityConstants.MISSING_VALUE_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_FORMAT_C, VelocityConstants.FORMAT_C_TEMPERATURE);
		var.addAttribute(VelocityConstants.ATTRIBUTE_ORIENTATION, VelocityConstants.ORIENTATION_TEMPERATURE);
		
		missingValue = VelocityConstants.MISSING_VALUE_TEMPERATURE * VelocityConstants.SCALE_FACTOR_TEMPERATURE + VelocityConstants.ADD_OFFSET_TEMPERATURE;

	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, ISoundVelocityProfile profile) throws GIOException {
		for (SoundVelocity velocity : profile.getSoundVelocities()) {
			if (Double.isNaN(velocity.getTemperature()))
				buffer.putDouble(missingValue);
			else	
				buffer.putDouble(velocity.getTemperature());
		}

	}

}
