/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.bsar;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;

/**
 * Service providing informations on BackScatter Angular Response (.bsar.nc)
 * NetCDF files.
 */
@Component(name = "globe_drivers_bsar_file_info_supplier", //
		service = { IFileInfoSupplier.class, BsarInfoSupplier.class })
public class BsarInfoSupplier implements IFileInfoSupplier<BsarInfo> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(BsarInfoSupplier.class);

	/** Extensions **/
	public static final String EXTENSION_BSAR_NC = "bsar.nc";
	public static final List<String> EXTENSIONS = List.of(EXTENSION_BSAR_NC);

	@Override
	public Optional<BsarInfo> getFileInfo(String filePath) {
		BsarInfo result = null;
		try {
			if (filePath.endsWith(EXTENSION_BSAR_NC)) {
				result = new BsarInfo(filePath);
			}
		} catch (Exception e) {
			// Not a Bsar file or bad version
			logger.warn("Bsar file not readable (maybe is depreacted (format under development)) : {}", filePath, e);
		}
		return Optional.ofNullable(result);
	}

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.BSAR_NETCDF_4);
	}

	@Override
	public List<String> getExtensions() {
		return EXTENSIONS;
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		var extensions = String.join(";", EXTENSIONS.stream().map(ext -> "*." + ext).toList());
		return Arrays.asList(new Pair<>("BackScatter Angular Response file (*." + extensions + ")", extensions));
	}

}