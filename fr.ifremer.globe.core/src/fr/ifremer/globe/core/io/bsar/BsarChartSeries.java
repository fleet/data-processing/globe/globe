package fr.ifremer.globe.core.io.bsar;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.datacontainer.DataContainerSeriesSource;
import fr.ifremer.globe.core.model.chart.impl.LoadableChartSeries;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartData;
import fr.ifremer.globe.core.runtime.datacontainer.Group;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer3D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer3D;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * {@link IChartSeries} with backed data, provides load and flush methods.
 */
public class BsarChartSeries extends LoadableChartSeries {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(BsarChartSeries.class);

	protected class BsarChartData extends SimpleChartData {
		final int valueCount;

		protected BsarChartData(IChartSeries<? extends IChartData> series, int index, double angle, double bs,
				int valueCount) {
			super(series, index, angle, bs);
			this.valueCount = valueCount;
		}

		@Override
		public GColor getColor() {
			if (isEnabled()) {
				return series.getColor();
			}
			return GColor.LIGHT_GRAY;
		}

		@Override
		public void setEnabled(boolean enabled) {
			if (enabled != isEnabled()) {
				super.setEnabled(enabled);
				setDirty(true);
			}
		}
	}

	protected class BsarIncidenceChartData extends BsarChartData {
		public BsarIncidenceChartData(IChartSeries<? extends IChartData> series, int index, double angle, double bs,
				int valueCount) {
			super(series, index, angle, bs, valueCount);
			// Incidence data
			setSectionId("INCIDENCE");
			setTooltip(String.format("incidence angle = %.2f°; bs = %.2fdB; valueCount = %d", //
					angle, bs, valueCount));
		}
	}

	protected class BsarTransmissionChartData extends BsarChartData {
		final int rxAntenna;
		final int txBeam;

		public BsarTransmissionChartData(IChartSeries<? extends IChartData> series, int index, double angle, double bs,
				int rxAntenna, int txBeam, int valueCount) {
			super(series, index, angle, bs, valueCount);
			this.txBeam = txBeam;
			this.rxAntenna = rxAntenna;
			// Transmission data
			setSectionId("TX_BEAM_" + txBeam + ":RX_ANTENNA_" + rxAntenna);
			setTooltip(String.format(
					"transmission angle = %.2f°; bs = %.2fdB; rxAntenna = %d; txBeam = %d; valueCount = %d", //
					angle, bs, rxAntenna, txBeam, valueCount));
		}

		@Override
		public GColor getColor() {
			if (isEnabled()) {
				var seriesColor = series.getColor();
				javafx.scene.paint.Color jfxColor = javafx.scene.paint.Color.rgb(seriesColor.getRed(),
						seriesColor.getGreen(), seriesColor.getBlue());
				double hueShift = 165.0 * rxAntenna + 30.0 * (txBeam + 1);
				double saturationFactor = 1.0;
				double brightnessFactor = 1.0;
				double opacityFactor = 1.0;
				var newColor = jfxColor.deriveColor(hueShift, saturationFactor, brightnessFactor, opacityFactor);
				return new GColor((int) Math.round(newColor.getRed() * 255),
						(int) Math.round(newColor.getGreen() * 255), (int) Math.round(newColor.getBlue() * 255));
			}
			return GColor.LIGHT_GRAY;
		}

	}

	private final Optional<Group> transmissionGroup;
	private List<Integer> rxAntennaIndices;
	private List<Integer> txBeamIndices;
	private List<Integer> transmissionAngleIndices;

	private final Group incidenceGroup;
	private List<Integer> incidenceAngleIndices;

	/**
	 * Constructor
	 * 
	 * @throws GIOException
	 */
	public BsarChartSeries(DataContainerSeriesSource source, String mode) throws GIOException {
		super(mode, source, 0);
		this.incidenceGroup = source.getDataContainerToken().getDataContainer()
				.getGroup("/" + mode + "/" + BsarConstants.INCIDENCE_SUBGROUP);
		this.transmissionGroup = source.getDataContainerToken().getDataContainer()
				.getOptionalGroup("/" + mode + "/" + BsarConstants.TRANSMISSION_SUBGROUP);
		load();
	}

	@Override
	public int size() {
		return chartDataList.size();
	}

	@Override
	public SimpleChartData getData(int index) {
		return chartDataList.get(index);
	}

	@Override
	protected void load() throws GIOException {
		// First incidence then transmission : important to keep that order for good
		// data indexing
		loadIncidence();
		loadTransmission();
	}

	protected void loadIncidence() throws GIOException {

		// get transmission layers
		var angleLayer = (DoubleLoadableLayer1D) incidenceGroup.getLayer(BsarConstants.ANGLE_VAR);
		var meanBsLayer = (DoubleLoadableLayer1D) incidenceGroup.getLayer(BsarConstants.MEAN_BS_VAR);
		var valueCountLayer = (IntLoadableLayer1D) incidenceGroup.getLayer(BsarConstants.VALUE_COUNT_VAR);

		long[] dims = meanBsLayer.getDimensions();
		int angleCount = (int) dims[0];
		incidenceAngleIndices = new ArrayList<>();
		for (int angleIndex = 0; angleIndex < angleCount; angleIndex++) {
			var angle = angleLayer.get(angleIndex);
			var bs = meanBsLayer.get(angleIndex);
			if (!Double.isNaN(bs) && angle < 90 && angle > -90) {
				// add incidence chart data
				incidenceAngleIndices.add(angleIndex);
				chartDataList.add(new BsarIncidenceChartData(this, chartDataList.size(), //
						angle, //
						bs, //
						valueCountLayer.get(angleIndex)));
			}
		}
	}

	protected void loadTransmission() throws GIOException {
		if(transmissionGroup.isPresent()) {
			// get transmission layers
			var angleLayer = (DoubleLoadableLayer1D) transmissionGroup.get().getLayer(BsarConstants.ANGLE_VAR);
			var meanResidualBsLayer = (DoubleLoadableLayer3D) transmissionGroup
					.get().getLayer(BsarConstants.MEAN_RESIDUAL_BS_VAR);
			var rxAntennaLayer = (IntLoadableLayer1D) transmissionGroup.get().getLayer(BsarConstants.RX_ANTENNA_VAR);
			var txBeamLayer = (IntLoadableLayer1D) transmissionGroup.get().getLayer(BsarConstants.TX_BEAM_VAR);
			var valueCountLayer = (IntLoadableLayer3D) transmissionGroup.get().getLayer(BsarConstants.VALUE_COUNT_VAR);
	
			long[] dims = meanResidualBsLayer.getDimensions();
			int rxAntennaCount = (int) dims[0];
			int txBeamCount = (int) dims[1];
			int angleCount = (int) dims[2];
			rxAntennaIndices = new ArrayList<>();
			txBeamIndices = new ArrayList<>();
			transmissionAngleIndices = new ArrayList<>();
	
			for (int rxAntennaIndex = 0; rxAntennaIndex < rxAntennaCount; rxAntennaIndex++) {
				for (int txBeamIndex = 0; txBeamIndex < txBeamCount; txBeamIndex++) {
					for (int angleIndex = 0; angleIndex < angleCount; angleIndex++) {
						var angle = angleLayer.get(angleIndex);
						var bs = meanResidualBsLayer.get(rxAntennaIndex, txBeamIndex, angleIndex);
						if (!Double.isNaN(bs) && angle < 90 && angle > -90) {
							// add transmission chart data
							rxAntennaIndices.add(rxAntennaIndex);
							txBeamIndices.add(txBeamIndex);
							transmissionAngleIndices.add(angleIndex);
							chartDataList.add(new BsarTransmissionChartData(this, chartDataList.size(), //
									angle, //
									bs, //
									rxAntennaLayer.get(rxAntennaIndex), //
									txBeamLayer.get(txBeamIndex), //
									valueCountLayer.get(rxAntennaIndex, txBeamIndex, angleIndex)));
						}
					}
				}
			}
		}
	}

	@Override
	public void flush() throws GIOException {
		// get incidence layers
		var incidenceMeanBsLayer = (DoubleLoadableLayer1D) incidenceGroup.getLayer(BsarConstants.MEAN_BS_VAR);

		// get transmission layers
		Optional<DoubleLoadableLayer3D> transmissionMeanResidualBsLayer = transmissionGroup.isPresent() ? Optional.of((DoubleLoadableLayer3D) transmissionGroup
				.get().getLayer(BsarConstants.MEAN_RESIDUAL_BS_VAR)) : Optional.empty();

		// Apply new values or Nan if invalid
		for (var chartData : chartDataList) {
			int charDataIndex = chartData.getIndex();
			double value = chartData.isEnabled() ? chartData.getY() : Double.NaN;
			if (charDataIndex < incidenceAngleIndices.size()) {
				// incidence chart data
				int index = charDataIndex;
				int angleIndex = incidenceAngleIndices.get(index);
				incidenceMeanBsLayer.set(angleIndex, value);
			} else if (transmissionMeanResidualBsLayer.isPresent()){
				// transmission chart data
				int index = charDataIndex - incidenceAngleIndices.size();
				int rxAntennaIndex = rxAntennaIndices.get(index);
				int txBeamIndex = txBeamIndices.get(index);
				int angleIndex = transmissionAngleIndices.get(index);
				transmissionMeanResidualBsLayer.get().set(rxAntennaIndex, txBeamIndex, angleIndex, value);
			}
		}
		incidenceMeanBsLayer.flush();
		if (transmissionMeanResidualBsLayer.isPresent())
			transmissionMeanResidualBsLayer.get().flush();
	}

	@Override
	protected double getX(int index) throws GIOException {
		return chartDataList.get(index).getX();
	}

	@Override
	protected double getY(int index) throws GIOException {
		return chartDataList.get(index).getY();
	}

	@Override
	protected void setY(int index, double y) throws GIOException {
		// not used
	}
}
