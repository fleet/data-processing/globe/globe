package fr.ifremer.globe.core.io.bsar;

/**
 * Globe 3D netCDF constants
 */
public class BsarConstants {

	private BsarConstants() {
		// Constants class
	}

	/** NetCDF attribute names **/
	public static final String TITLE_ATTRIBUTE = "title";
	public static final String VERSION_ATTRIBUTE = "bs_angular_response_version";
	public static final String SOUNDER_TYPE_ATTRIBUTE = "sounder_type";
	public static final String MODE_SERIALIZED_ATTRIBUTE = "mode_serialized";
	public static final String USE_SVP_ATTRIBUTE = "use_sound_velocity_profiles";
	public static final String USE_SNIPPETS_ATTRIBUTE = "use_snippets";
	public static final String USE_INSONIFIED_AREA_ATTRIBUTE = "use_insonified_area";
	public static final String REMOVE_CALIBRATION_ATTRIBUTE = "remove_calibration";

	/** Group names **/
	public static final String INCIDENCE_SUBGROUP = "by_incidence_angle";
	public static final String TRANSMISSION_SUBGROUP = "by_transmission_angle";
	
	/** Variable names **/
	public static final String MEAN_BS_VAR = "mean_bs";
	public static final String MEAN_RESIDUAL_BS_VAR = "mean_residual_bs";
	public static final String VALUE_COUNT_VAR = "value_count";
	public static final String ANGLE_VAR = "angle";
	public static final String TX_BEAM_VAR = "tx_beam";
	public static final String RX_ANTENNA_VAR = "rx_antenna";
	
	public static final double BSAR_MIN_SUPPORTED_VERSION = 0.2;

}
