package fr.ifremer.globe.core.io.bsar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to read an BSAR file.
 */
public class BsarInfoReader {

	private final Logger logger = LoggerFactory.getLogger(BsarInfoReader.class);

	/**
	 * Reads file to get needed attributes
	 */
	public void readAttributes(BsarInfo info) throws GIOException {
		IDataContainerFactory containerFactory = IDataContainerFactory.grab();
		IDataContainerOwner owner = IDataContainerOwner.generate(getClass().getName(), true);
		try (IDataContainerToken<BsarInfo> dataContainerToken = containerFactory.book(info, owner)) {
			readVersion(info);
			readSounderType(info);
			readAdditionalAttributes(info);
			readModes(info);
		}
	}

	/**********************************************************************************************************
	 * Attribute readers
	 **********************************************************************************************************/

	/** Reads BSAR version **/
	private void readVersion(BsarInfo info) throws GIOException {
		try {
			if (info.getFile().hasAttribute(BsarConstants.VERSION_ATTRIBUTE)) {
				String version = info.getFile().getAttributeAsString(BsarConstants.VERSION_ATTRIBUTE);
				if (!BsarInfo.checkVersion(version)) {
					throw new GIOException("Bad BSAR version : " + version);
				}
				info.setBsarVersion(version);
			} else {
				throw new GIOException("Not a BSAR file");
			}
		} catch (NCException e) {
			info.setBsarVersion("not found");
			logger.error("BsarInfo loading: error with bsar version: " + e.getMessage(), e);
		}
	}

	/** Reads sounder type **/
	private void readSounderType(BsarInfo info) {
		try {
			if (info.getFile().hasAttribute(BsarConstants.SOUNDER_TYPE_ATTRIBUTE)) {
				String type = info.getFile().getAttributeAsString(BsarConstants.SOUNDER_TYPE_ATTRIBUTE);
				info.setSounderType(type);
			}
		} catch (Exception e) {
			logger.error("BsarInfo loading: error with bsar model: " + e.getMessage(), e);
		}
	}

	/** Reads additional properties **/
	private void readAdditionalAttributes(BsarInfo info) {
		try {
			Map<String, String> attributes = new HashMap<>();
			if (info.getFile().hasAttribute(BsarConstants.USE_SNIPPETS_ATTRIBUTE)) {
				String att = info.getFile().getAttributeAsString(BsarConstants.USE_SNIPPETS_ATTRIBUTE);
				attributes.put(BsarConstants.USE_SNIPPETS_ATTRIBUTE, att);
			}
			if (info.getFile().hasAttribute(BsarConstants.USE_INSONIFIED_AREA_ATTRIBUTE)) {
				String att = info.getFile().getAttributeAsString(BsarConstants.USE_INSONIFIED_AREA_ATTRIBUTE);
				attributes.put(BsarConstants.USE_INSONIFIED_AREA_ATTRIBUTE, att);
			}
			if (info.getFile().hasAttribute(BsarConstants.USE_SVP_ATTRIBUTE)) {
				String att = info.getFile().getAttributeAsString(BsarConstants.USE_SVP_ATTRIBUTE);
				attributes.put(BsarConstants.USE_SVP_ATTRIBUTE, att);
			}
			if (info.getFile().hasAttribute(BsarConstants.REMOVE_CALIBRATION_ATTRIBUTE)) {
				String att = info.getFile().getAttributeAsString(BsarConstants.REMOVE_CALIBRATION_ATTRIBUTE);
				attributes.put(BsarConstants.REMOVE_CALIBRATION_ATTRIBUTE, att);
			}
			info.setAttributes(attributes);
		} catch (Exception e) {
			logger.error("BsarInfo loading: error with bsar model: " + e.getMessage(), e);
		}
	}

	/** Reads sounder type **/
	private void readModes(BsarInfo info) {
		try {
			// List available modes
			List<String> modes = new ArrayList<>();
			for (var group : info.getFile().getGroups()) {
				modes.add(group.getName());
			}
			info.setModes(modes);
		} catch (Exception e) {
			logger.error("BsarInfo loading: error with bsar model: " + e.getMessage(), e);
		}
	}

}
