package fr.ifremer.globe.core.io.bsar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.datacontainer.DataContainerSeriesSource;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides method to get {@link IChartSeries} from {@link BsarInfo}.
 */
@Component(name = "globe_drivers_bsar_chart_series_supplier", service = { IChartSeriesSupplier.class })
public class BsarChartSeriesProvider implements IChartSeriesSupplier, IDataContainerOwner {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(BsarChartSeriesProvider.class);

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.BSAR_NETCDF_4);
	}

	@Override
	public List<? extends IChartSeries<? extends IChartData>> getChartSeries(IFileInfo fileInfo) throws GIOException {
		if (fileInfo instanceof BsarInfo bsarInfo) {
			IDataContainerToken<BsarInfo> dataContainerToken = IDataContainerFactory.grab().book(bsarInfo, this);
			var source = new DataContainerSeriesSource(dataContainerToken);

			List<BsarChartSeries> seriesList = new ArrayList<>();
			for (String mode : bsarInfo.getModes()) {
				seriesList.add(new BsarChartSeries(source, mode));
			}
			return seriesList;
		}
		return Collections.emptyList();
	}
}
