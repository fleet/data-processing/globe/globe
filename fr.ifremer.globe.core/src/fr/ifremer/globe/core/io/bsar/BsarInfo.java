package fr.ifremer.globe.core.io.bsar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Info about BackScatter Angular Response NetCDF files (.bsar.nc).
 */
public class BsarInfo implements IDataContainerInfo {

	/** Path & file **/
	private final String filePath;
	private NetcdfFile ncFile;
	private final AtomicBoolean isOpen = new AtomicBoolean();
	private List<String> modes = new ArrayList<>();
	private String sounderType;
	private String version;

	// Additional attributes
	private Map<String, String> attributes = new HashMap<>();

	/**
	 * Constructor
	 *
	 */
	public BsarInfo(String filePath) throws GIOException {
		this.filePath = filePath;
		new BsarInfoReader().readAttributes(this);
	}

	/**
	 * Open the file
	 */
	@Override
	public synchronized void open(boolean readonly) throws GIOException {
		try {
			if (isOpen.compareAndSet(false, true)) {
				ncFile = NetcdfFile.open(filePath, readonly ? Mode.readonly : Mode.readwrite);
			}
		} catch (NCException e) {
			isOpen.set(false);
			throw new GIOException("Error while opening BSAR file : " + e.getMessage(), e);
		}
	}

	/**
	 * Close the file
	 */
	@Override
	public synchronized void close() {
		if (!IDataContainerFactory.grab().isBooked(this) && isOpen.compareAndSet(true, false))
			ncFile.close();
	}

	@Override
	public void declareLayers(DataContainer<?> container) throws GException {
		new NetcdfLayerDeclarer().declareLayers(ncFile, container, NetcdfGroup::getPath, variable -> Optional.empty());
	}

	/**
	 *
	 * @return True if XSF file has a supported version
	 */
	public static boolean checkVersion(String bsarVersion) {
		double versionNumber = 0;
		if (bsarVersion != null) {
			bsarVersion = bsarVersion.trim();
			if (!bsarVersion.isEmpty()) {
				versionNumber = Double.valueOf(bsarVersion);
			}
		}
		return versionNumber >= BsarConstants.BSAR_MIN_SUPPORTED_VERSION;
	}

	/**********************************************************************************************************
	 * Getters / Setters
	 **********************************************************************************************************/

	public NetcdfFile getFile() {
		return ncFile;
	}

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public ContentType getContentType() {
		return ContentType.BSAR_NETCDF_4;
	}

	@Override
	public List<Property<?>> getProperties() {
		var properties = new ArrayList<Property<?>>();
		properties.add(Property.build("Description", "Backscatter Angular Response File (.bsar.nc)"));
		properties.add(Property.build("Bsar version", this.version));
		properties.add(Property.build("Sounder type", this.sounderType));
		properties.add(Property.buildDetailed("Modes", this.modes));
		attributes.forEach((k, v) -> properties.add(Property.build(k, v)));
		return properties;
	}

	public List<String> getModes() {
		return this.modes;
	}

	public void setModes(List<String> modes) {
		this.modes = modes;
	}

	public String getBsarVersion() {
		return version;
	}

	protected void setBsarVersion(String version) {
		this.version = version;
	}

	public String getSounderType() {
		return sounderType;
	}

	protected void setSounderType(String type) {
		this.sounderType = type;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	protected void setAttributes(Map<String, String> att) {
		this.attributes = att;
	}

}
