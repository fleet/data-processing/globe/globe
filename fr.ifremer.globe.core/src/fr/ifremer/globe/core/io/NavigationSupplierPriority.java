/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io;

/**
 * Constant class of IFileInfoSupplier priority.<br>
 * This constants are used by INavigationService to sort INavigationSupplier  
 */
public abstract class NavigationSupplierPriority {

	/**
	 * Key used in a INavigationSupplier definition to identify the supplier’s priority. <br>
	 * By default, priority is 10. The lowest priority is Integer.MAX_VALUE
	 */
	public static final String PROPERTY_PRIORITY = "NavigationSupplierPriority.PROPERTY_PRIORITY";

	/**
	 * Default priority of all supplier<br>
	 * Use when {@code property = { NavigationSupplierPriority.PROPERTY_PRIORITY + ":Integer="...} } is not defined on
	 * these supplier
	 */
	public static final int DEFAULT_PRIORITY = 10;

	/**
	 * Priority of polygon supplier<br>
	 * This supplier has priority over other navigation supplier (Xsf, Mbg, nvi...)
	 */
	public static final int EXTENDED_GWS_PRIORITY = 1;

	/**
	 * Constructor
	 */
	private NavigationSupplierPriority() {
	}

}
