package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public abstract class SimpleVariableWriter implements INviVariableWriter {

	/** Properties **/
	private final String name;
	protected final DataType dataType;
	protected final DataType bufferType; // = unpacked data type (before scale factor and add offset)
	protected NetcdfVariable var;

	/** Constructor **/
	public SimpleVariableWriter(String name, DataType dataType, DataType bufferType) {
		this.name = name;
		this.dataType = dataType;
		this.bufferType = bufferType;
	}

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		var = file.addVariable(name, dataType, Arrays.asList(dim));
	}

	@Override
	public void writeData(INavigationData dataProxy, List<Integer> indexesToExport)
			throws NCException, GIOException {
		ByteBuffer buffer = ByteBuffer.allocate(indexesToExport.size() * bufferType.getSize());
		buffer.order(ByteOrder.nativeOrder());

		for (int i : indexesToExport)
			fillBufferToWrite(buffer, dataProxy, i);

		var.write(new long[] { indexesToExport.size() }, buffer, bufferType, Optional.empty());
	}

	/**
	 * Fills the buffer which will be used by the write operation
	 */
	protected abstract void fillBufferToWrite(ByteBuffer bb, INavigationData dP, int index) throws GIOException;
}
