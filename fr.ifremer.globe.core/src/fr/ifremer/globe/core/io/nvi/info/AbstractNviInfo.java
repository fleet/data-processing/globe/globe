package fr.ifremer.globe.core.io.nvi.info;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides information about a navigation file (.nvi or .nvi.nc).
 */
public abstract class AbstractNviInfo implements IDataContainerInfo {

	/** Path & file **/
	protected final String filePath;
	protected NetcdfFile ncFile;
	private final AtomicBoolean isOpen = new AtomicBoolean();
	private final ContentType contentType;

	/** NVI info attributes */
	protected GeoBox geobox;
	protected Instant startDate;
	protected Instant endDate;
	protected int positionNbr;

	/**
	 * Constructor
	 */
	protected AbstractNviInfo(String filePath, ContentType contentType) {
		this.filePath = filePath;
		this.contentType = contentType;
		load();
	}

	/**
	 * Open the file
	 */
	@Override
	public synchronized void open(boolean readOnly) throws GIOException {
		try {
			if (isOpen.compareAndSet(false, true)) {
				ncFile = NetcdfFile.open(filePath, readOnly ? Mode.readonly : Mode.readwrite);
			}
		} catch (NCException e) {
			isOpen.set(false);
			throw new GIOException("Error while opening NVI file : " + e.getMessage(), e);
		}
	}

	/**
	 * Close the file
	 */
	@Override
	public synchronized void close() {
		if (!IDataContainerFactory.grab().isBooked(this) && isOpen.compareAndSet(true, false))
			ncFile.close();
	}

	/**
	 * Loads properties from the {@link NetcdfFile}.
	 */
	protected abstract void load();

	/**
	 * @return NVI properties
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = new ArrayList<>();
		properties.add(Property.build("Navigation points", Integer.toString(positionNbr)));
		properties.add(Property.build("Start Date", startDate != null ? startDate.toString() : "Unknown"));
		properties.add(Property.build("End Date", endDate != null ? endDate.toString() : "Unknown"));
		Optional.ofNullable(geobox).ifPresent(gb -> properties.addAll(Property.build(geobox)));
		return properties;
	}

	/**********************************************************************************************************
	 * Getters / Setters
	 **********************************************************************************************************/

	@Override
	public ContentType getContentType() {
		return contentType;
	}

	public String getFilePath() {
		return filePath;
	}

	public NetcdfFile getFile() {
		return ncFile;
	}

	@Override
	public String getPath() {
		return ncFile != null ? ncFile.getName() : filePath;
	}

	@Override
	public GeoBox getGeoBox() {
		return geobox;
	}

	public void setGeoBox(GeoBox geoBox) {
		this.geobox = geoBox;
	}

	public final void setStartDate(Instant start) {
		this.startDate = start;
	}

	public final void setEndDate(Instant end) {
		this.endDate = end;
	}

	@Override
	public final Optional<Pair<Instant, Instant>> getStartEndDate() {
		return startDate != null && endDate != null ? Optional.of(Pair.of(startDate, endDate)) : Optional.empty();
	}

	public int getPositionNbr() {
		return positionNbr;
	}

	public void setPositionNbr(int length) {
		this.positionNbr = length;
	}
}