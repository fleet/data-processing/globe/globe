package fr.ifremer.globe.core.io.nvi.info;

import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.DataType;

/**
 * NVI constants
 */
public class NviConstants {
	
	private NviConstants() {
		// Constants class
	}
	
	public static final Short ACTUAL_VERSION = 210;
	public static final String EXTENSION = "nvi";
	public static final String MODULE_NAME = "NVI export";
	public static final String CLASSE = "MB_NAVIGATION";
	
	public static final String TIME_REFERENCE_ATTRIBUTE = "Julian date for 1970/01/01 = 2 440 588";
	public static final double ELLIPSOID_A = 6378137.0;
	public static final double ELLIPSOID_INVF = 0.003352810664747574;
	public static final double ELLIPSOID_E2 = 0.006694379990141441;

	public static final byte VALIDITY_FLAG_VALUE = 2;
	public static final byte INVALIDITY_FLAG_VALUE = -2;
	public static final float INVALIDITY_FLAG_VALUE_FLOAT = -2.0f;

	public static final String DIM_POSITION_NBR = "mbPositionNbr";

	public static final String Version = "mbVersion";
	public static final String Name = "mbName";
	public static final String GLOBAL_ATT_CLASSE = "mbClasse";
	public static final String Level = "mbLevel";
	public static final String NbrHistoryRec = "mbNbrHistoryRec";
	public static final String TimeReference = "mbTimeReference";
	public static final String StartDate = "mbStartDate";
	public static final String StartTime = "mbStartTime";
	public static final String EndDate = "mbEndDate";
	public static final String EndTime = "mbEndTime";
	public static final String NorthLatitude = "mbNorthLatitude";
	public static final String SouthLatitude = "mbSouthLatitude";
	public static final String EastLongitude = "mbEastLongitude";
	public static final String WestLongitude = "mbWestLongitude";
	public static final String Meridian180 = "mbMeridian180";
	public static final String GeoDictionnary = "mbGeoDictionnary";
	public static final String GeoRepresentation = "mbGeoRepresentation";
	public static final String GeodesicSystem = "mbGeodesicSystem";
	public static final String EllipsoidName = "mbEllipsoidName";
	public static final String EllipsoidA = "mbEllipsoidA";
	public static final String EllipsoidInvF = "mbEllipsoidInvF";
	public static final String EllipsoidE2 = "mbEllipsoidE2";
	public static final String ProjType = "mbProjType";
	public static final String ProjParameterValue = "mbProjParameterValue";
	public static final String ProjParameterCode = "mbProjParameterCode";
	public static final String Ship = "mbShip";
	public static final String Survey = "mbSurvey";
	public static final String Reference = "mbReference";
	public static final String CDI = "mbCDI";
	public static final String PointCounter = "mbPointCounter";

	public static final String ATTRIBUTE_TYPE = "type";
	public static final String ATTRIBUTE_LONG_NAME = "long_name";
	public static final String ATTRIBUTE_NAME_CODE = "name_code";
	public static final String ATTRIBUTE_UNITS = CFStandardNames.CF_UNITS;
	public static final String ATTRIBUTE_UNIT_CODE = "unit_code";
	public static final String ATTRIBUTE_ADD_OFFSET = "add_offset";
	public static final String ATTRIBUTE_SCALE_FACTOR = "scale_factor";
	public static final String ATTRIBUTE_MIN = "minimum";
	public static final String ATTRIBUTE_MAX = "maximum";
	public static final String ATTRIBUTE_VALID_MIN = "valid_minimum";
	public static final String ATTRIBUTE_VALID_MAX = "valid_maximum";
	public static final String ATTRIBUTE_MISSING_VALUE = "missing_value";
	public static final String ATTRIBUTE_FORMAT_C = "format_C";
	public static final String ATTRIBUTE_ORIENTATION = "orientation";
	public static final String ATTRIBUTE_FLAG_VALUES = "flag_values";
	public static final String ATTRIBUTE_FLAG_MEANINGS = "flag_meanings";

	public static final String NAME_LATITUDE = "mbOrdinate";
	public static final DataType DATA_TYPE_LATITUDE = DataType.INT;
	public static final String TYPE_LATITUDE = "real";
	public static final String LONG_NAME_LATITUDE = "Latitude";
	public static final String NAME_CODE_LATITUDE = "MB_POSITION_LATITUDE";
	public static final String UNITS_LATITUDE = "degree";
	public static final String UNIT_CODE_LATITUDE = "MB_DEGREE";
	public static final double ADD_OFFSET_LATITUDE = 0.0;
	public static final double SCALE_FACTOR_LATITUDE = 0.00000005;
	public static final double MINIMUM_LATITUDE = -1.8E9;
	public static final double MAXIMUM_LATITUDE = 1.8E9;
	public static final int VALID_MINIMUM_LATITUDE = -1800000000;
	public static final int VALID_MAXIMUM_LATITUDE = 1800000000;
	public static final int MISSING_VALUE_LATITUDE = -2147483648;
	public static final String FORMAT_C_LATITUDE = "%f";
	public static final String ORIENTATION_LATITUDE = "direct";

	public static final String NAME_LONGITUDE = "mbAbscissa";
	public static final DataType DATA_TYPE_LONGITUDE = DataType.INT;
	public static final String TYPE_LONGITUDE = "real";
	public static final String LONG_NAME_LONGITUDE = "Longitude";
	public static final String NAME_CODE_LONGITUDE = "MB_POSITION_LONGITUDE";
	public static final String UNITS_LONGITUDE = "degree";
	public static final String UNIT_CODE_LONGITUDE = "MB_DEGREE";
	public static final double ADD_OFFSET_LONGITUDE = 0.0;
	public static final double SCALE_FACTOR_LONGITUDE = 0.0000001;
	public static final double MINIMUM_LONGITUDE = -1.8E9;
	public static final double MAXIMUM_LONGITUDE = 1.8E9;
	public static final int VALID_MINIMUM_LONGITUDE = -1800000000;
	public static final int VALID_MAXIMUM_LONGITUDE = 1800000000;
	public static final int MISSING_VALUE_LONGITUDE = -2147483648;
	public static final String FORMAT_C_LONGITUDE = "%f";
	public static final String ORIENTATION_LONGITUDE = "direct";

	public static final String NAME_ALTITUDE = "mbAltitude";
	public static final DataType DATA_TYPE_ALTITUDE = DataType.SHORT;
	public static final String TYPE_ALTITUDE = "real";
	public static final String LONG_NAME_ALTITUDE = "Altitude";
	public static final String NAME_CODE_ALTITUDE = "MB_POSITION_ALTITUDE";
	public static final String UNITS_ALTITUDE = "m";
	public static final String UNIT_CODE_ALTITUDE = "MB_M";
	public static final double ADD_OFFSET_ALTITUDE = 0.0;
	public static final double SCALE_FACTOR_ALTITUDE = 0.01;
	public static final double MINIMUM_ALTITUDE = -32767.0;
	public static final double MAXIMUM_ALTITUDE = 32767.0;
	public static final int VALID_MINIMUM_ALTITUDE = -32767;
	public static final int VALID_MAXIMUM_ALTITUDE = 32767;
	public static final int MISSING_VALUE_ALTITUDE = -32768;
	public static final String FORMAT_C_ALTITUDE = "%.2f";
	public static final String ORIENTATION_ALTITUDE = "direct";

	public static final String NAME_TIME = "mbTime";
	public static final DataType DATA_TYPE_TIME = DataType.INT;
	public static final String TYPE_TIME = "integer";
	public static final String LONG_NAME_TIME = "Time";
	public static final String NAME_CODE_TIME = "MB_POSITION_TIME";
	public static final String UNITS_TIME = "ms";
	public static final String UNIT_CODE_TIME = "MB_MS";
	public static final int ADD_OFFSET_TIME = 0;
	public static final int SCALE_FACTOR_TIME = 1;
	public static final int MINIMUM_TIME = 0;
	public static final int MAXIMUM_TIME = 86399999;
	public static final int VALID_MINIMUM_TIME = 0;
	public static final int VALID_MAXIMUM_TIME = 86399999;
	public static final int MISSING_VALUE_TIME = -2147483648;
	public static final String FORMAT_C_TIME = "%d";
	public static final String ORIENTATION_TIME = "direct";

	public static final String NAME_DATE = "mbDate";
	public static final DataType DATA_TYPE_DATE = DataType.INT;
	public static final String TYPE_DATE = "integer";
	public static final String LONG_NAME_DATE = "Date";
	public static final String NAME_CODE_DATE = "MB_POSITION_DATE";
	public static final String UNITS_DATE = "Julian_date";
	public static final String UNIT_CODE_DATE = "MB_JULIAN_DATE";
	public static final int ADD_OFFSET_DATE = 2440588;
	public static final int SCALE_FACTOR_DATE = 1;
	public static final int MINIMUM_DATE = -25567;
	public static final int MAXIMUM_DATE = 47482;
	public static final int VALID_MINIMUM_DATE = -25567;
	public static final int VALID_MAXIMUM_DATE = 47482;
	public static final int MISSING_VALUE_DATE = 2147483647;
	public static final String FORMAT_C_DATE = "%d";
	public static final String ORIENTATION_DATE = "direct";

	public static final String NAME_IMMERSION = "mbImmersion";
	public static final DataType DATA_TYPE_IMMERSION = DataType.SHORT;
	public static final String TYPE_IMMERSION = "real";
	public static final String LONG_NAME_IMMERSION = "Immersion";
	public static final String NAME_CODE_IMMERSION = "MB_POSITION_IMMERSION";
	public static final String UNITS_IMMERSION = "m";
	public static final String UNIT_CODE_IMMERSION = "MB_M";
	public static final double ADD_OFFSET_IMMERSION = 0.0;
	public static final double SCALE_FACTOR_IMMERSION = 0.2;
	public static final double MINIMUM_IMMERSION = -32767.0;
	public static final double MAXIMUM_IMMERSION = 32767.0;
	public static final int VALID_MINIMUM_IMMERSION = -32767;
	public static final int VALID_MAXIMUM_IMMERSION = 32767;
	public static final int MISSING_VALUE_IMMERSION = -32768;
	public static final String FORMAT_C_IMMERSION = "%.2f";
	public static final String ORIENTATION_IMMERSION = "direct";

	public static final String NAME_HEADING = "mbHeading";
	public static final DataType DATA_TYPE_HEADING = DataType.INT;
	public static final String TYPE_HEADING = "real";
	public static final String LONG_NAME_HEADING = "Ship heading";
	public static final String NAME_CODE_HEADING = "MB_POSITION_HEADING";
	public static final String UNITS_HEADING = "degree";
	public static final String UNIT_CODE_HEADING = "MB_DEGREE";
	public static final double ADD_OFFSET_HEADING = 0.0;
	public static final double SCALE_FACTOR_HEADING = 0.01;
	public static final double MINIMUM_HEADING = 1.0;
	public static final double MAXIMUM_HEADING = 35999.0;
	public static final int VALID_MINIMUM_HEADING = 1;
	public static final int VALID_MAXIMUM_HEADING = 35999;
	public static final int MISSING_VALUE_HEADING = 65535;
	public static final String FORMAT_C_HEADING = "%.2f";
	public static final String ORIENTATION_HEADING = "direct";

	public static final String NAME_SPEED = "mbSpeed";
	public static final DataType DATA_TYPE_SPEED = DataType.SHORT;
	public static final String TYPE_SPEED = "real";
	public static final String LONG_NAME_SPEED = "Vessel speed";
	public static final String NAME_CODE_SPEED = "MB_POSITION_SPEED";
	public static final String UNITS_SPEED = "knot";
	public static final String UNIT_CODE_SPEED = "MB_KNOT";
	public static final double ADD_OFFSET_SPEED = 0.0;
	public static final double SCALE_FACTOR_SPEED = 0.01;
	public static final double MINIMUM_SPEED = -32767.0;
	public static final double MAXIMUM_SPEED = 32767.0;
	public static final int VALID_MINIMUM_SPEED = -32767;
	public static final int VALID_MAXIMUM_SPEED = 32767;
	public static final int MISSING_VALUE_SPEED = -32768;
	public static final String FORMAT_C_SPEED = "%.2f";
	public static final String ORIENTATION_SPEED = "direct";

	public static final String NAME_PTYPE = "mbPType";
	public static final DataType DATA_TYPE_PTYPE = DataType.CHAR;
	public static final String TYPE_PTYPE = "integer";
	public static final String LONG_NAME_PTYPE = "PType of captor";
	public static final String NAME_CODE_PTYPE = "MB_POSITION_TYPE";
	public static final String UNITS_PTYPE = "";
	public static final String UNIT_CODE_PTYPE = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_PTYPE = 0;
	public static final int SCALE_FACTOR_PTYPE = 1;
	public static final int MINIMUM_PTYPE = 1;
	public static final int MAXIMUM_PTYPE = 255;
	public static final int VALID_MINIMUM_PTYPE = 1;
	public static final int VALID_MAXIMUM_PTYPE = 255;
	public static final int MISSING_VALUE_PTYPE = 0;
	public static final String FORMAT_C_PTYPE = "%d";
	public static final String ORIENTATION_PTYPE = "direct";

	public static final String NAME_PQUALITY = "mbPQuality";
	public static final DataType DATA_TYPE_PQUALITY = DataType.CHAR;
	public static final String TYPE_PQUALITY = "integer";
	public static final String LONG_NAME_PQUALITY = "Factor of quality";
	public static final String NAME_CODE_PQUALITY = "MB_POSITION_QUALITY";
	public static final String UNITS_PQUALITY = "";
	public static final String UNIT_CODE_PQUALITY = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_PQUALITY = 0;
	public static final int SCALE_FACTOR_PQUALITY = 1;
	public static final int MINIMUM_PQUALITY = 1;
	public static final int MAXIMUM_PQUALITY = 255;
	public static final int VALID_MINIMUM_PQUALITY = 1;
	public static final int VALID_MAXIMUM_PQUALITY = 255;
	public static final int MISSING_VALUE_PQUALITY = 0;
	public static final String FORMAT_C_PQUALITY = "%d";
	public static final String ORIENTATION_PQUALITY = "direct";

	public static final String NAME_PFLAG = "mbPFlag";
	public static final DataType DATA_TYPE_PFLAG = DataType.CHAR;
	public static final String TYPE_PFLAG = "integer";
	public static final String LONG_NAME_PFLAG = "Flag of position";
	public static final String NAME_CODE_PFLAG = "MB_POSITION_Flag";
	public static final String UNITS_PFLAG = "";
	public static final String UNIT_CODE_PFLAG = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_PFLAG = 0;
	public static final int SCALE_FACTOR_PFLAG = 1;
	public static final int MINIMUM_PFLAG = -127;
	public static final int MAXIMUM_PFLAG = 127;
	public static final int VALID_MINIMUM_PFLAG = -127;
	public static final int VALID_MAXIMUM_PFLAG = 127;
	public static final int MISSING_VALUE_PFLAG = -128;
	public static final String FORMAT_C_PFLAG = "%d";
	public static final String ORIENTATION_PFLAG = "direct";
	public static final String FLAG_VALUES_PFLAG = "-4B, -3B, -2B, -1B, 0B, 1B, 2B, 3B, 4B, 5B, 6B, 7B, 8B";
	// public static final ArrayByte.D1 FLAG_VALUES_PFLAG; //TODO define if it's important to keep a byte array for falg values ?
	public static final String FLAG_MEANINGS_PFLAG = "removed doubtful_unvalidated valid_unvalidated unvalid missing doubtful valid doubtful_validated unvalid_validated removed_validated interpolated moved smoothed";

	/*
	 * static final { FLAG_VALUES_PFLAG = new ArrayByte.D1(13); FLAG_VALUES_PFLAG.set(0, (byte) -4);
	 * FLAG_VALUES_PFLAG.set(1, (byte) -3); FLAG_VALUES_PFLAG.set(2, (byte) -2); FLAG_VALUES_PFLAG.set(3, (byte) -1);
	 * FLAG_VALUES_PFLAG.set(4, (byte) 0); FLAG_VALUES_PFLAG.set(5, (byte) 1); FLAG_VALUES_PFLAG.set(6, (byte) 2);
	 * FLAG_VALUES_PFLAG.set(7, (byte) 3); FLAG_VALUES_PFLAG.set(8, (byte) 4); FLAG_VALUES_PFLAG.set(9, (byte) 5);
	 * FLAG_VALUES_PFLAG.set(10, (byte) 6); FLAG_VALUES_PFLAG.set(11, (byte) 7); FLAG_VALUES_PFLAG.set(12, (byte) 8); }
	 */
}
