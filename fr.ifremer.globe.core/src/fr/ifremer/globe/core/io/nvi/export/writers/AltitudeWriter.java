package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class AltitudeWriter extends SimpleVariableWriter {

	/** Constructor **/
	public AltitudeWriter() {
		super( NviConstants.NAME_ALTITUDE, NviConstants.DATA_TYPE_ALTITUDE,  DataType.FLOAT);
	}

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		super.define(file, dim);
		var.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_ALTITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_ALTITUDE);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, INavigationData dataProxy, int index) throws GIOException {
		buffer.putFloat(dataProxy.getMetadataValue(NavigationMetadataType.ALTITUDE, index).orElse(Float.NaN));
	}

}
