package fr.ifremer.globe.core.io.nvi.info;

import java.util.List;

import fr.ifremer.globe.core.io.nvi.datacontainer.NavigationLayers;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;

/**
 * NVI V2 constants
 */
public class NviV2Constants {

	/* Global attributes */
	public static final String NVI_CONVENTION_NAME = "nvi_convention_name";
	public static final String NVI_CONVENTION_VERSION = "nvi_convention_version";

	public static final List<PredefinedLayers<?>> NAVIGATION_LAYERS = List.of(NavigationLayers.COURSE_OVER_GROUND,
			NavigationLayers.HEADING, NavigationLayers.HEADING_RATE, NavigationLayers.HEIGHT_ABOVE_REFERENCE_ELLIPSOID,
			NavigationLayers.LATITUDE, NavigationLayers.LONGITUDE, NavigationLayers.PITCH, NavigationLayers.PITCH_RATE,
			NavigationLayers.POSITION_QUALITY_INDICATOR, NavigationLayers.QUALITY_FLAG, NavigationLayers.ROLL,
			NavigationLayers.ROLL_RATE, NavigationLayers.SPEED_OVER_GROUND, NavigationLayers.SPEED_RELATIVE,
			NavigationLayers.TIME, NavigationLayers.VERTICAL_OFFSET);

	private NviV2Constants() {
		// Constants class
	}

}
