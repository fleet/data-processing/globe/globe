package fr.ifremer.globe.core.io.nvi.datacontainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.io.nvi.info.NviV2Info;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.core.model.navigation.utils.NavigationStatisticComputer;
import fr.ifremer.globe.core.model.quality.MeasurandQualifierFlags;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link INavigationData} for NVI : provides access to navigation data.
 */
public class NviV2Navigation implements INavigationData, IDataContainerOwner {

	protected final IDataContainerToken<NviV2Info> dataContainerToken;
	protected final DataContainer<NviV2Info> dataContainer;
	protected final NviV2Info info;
	private boolean isReadOnly = true;

	/** layers **/
	private final LongLoadableLayer1D timeLayer;
	private final DoubleLoadableLayer1D latitudeLayer;
	private final DoubleLoadableLayer1D longitudeLayer;
	private final FloatLoadableLayer1D headingLayer;
	private final FloatLoadableLayer1D heightAboveReferenceEllipsoid;
	private final FloatLoadableLayer1D verticalOffsetLayer;
	private final ByteLoadableLayer1D qualityFlagLayer;
	private final Optional<ByteLoadableLayer1D> positionQualityIndicatorLayer;
	private final Optional<FloatLoadableLayer1D> speedOverGroundLayer;

	/** Metadata */
	private final List<NavigationMetadata<?>> metadata = new ArrayList<>();

	/** Constructor */
	public NviV2Navigation(NviV2Info info, boolean readonly) throws GIOException {
		this.info = info;
		this.isReadOnly = readonly;
		dataContainerToken = IDataContainerFactory.grab().book(info, this);
		dataContainer = dataContainerToken.getDataContainer();

		// Get required layers
		timeLayer = dataContainer.getLayer(NavigationLayers.TIME);
		latitudeLayer = dataContainer.getLayer(NavigationLayers.LATITUDE);
		longitudeLayer = dataContainer.getLayer(NavigationLayers.LONGITUDE);
		headingLayer = dataContainer.getLayer(NavigationLayers.HEADING);
		verticalOffsetLayer = dataContainer.getLayer(NavigationLayers.VERTICAL_OFFSET);
		heightAboveReferenceEllipsoid = dataContainer.getLayer(NavigationLayers.HEIGHT_ABOVE_REFERENCE_ELLIPSOID);
		qualityFlagLayer = dataContainer.getLayer(NavigationLayers.QUALITY_FLAG);
		// Get optional layers
		speedOverGroundLayer = dataContainer.getOptionalLayer(NavigationLayers.SPEED_OVER_GROUND);
		positionQualityIndicatorLayer = dataContainer.getOptionalLayer(NavigationLayers.POSITION_QUALITY_INDICATOR);
		initMetadata();
		
	}

	/**
	 * Defines {@link NavigationMetadata} for NviV2 navigation.
	 */
	private void initMetadata() throws GIOException {
		metadata.add(NavigationMetadataType.TIME.withValueSupplier(this::getTime));
		metadata.add(NavigationMetadataType.HEADING.withValueSupplier(this::getHeading));
		metadata.add(NavigationMetadataType.ELEVATION.withValueSupplier(this::getElevation));
		metadata.add(NavigationMetadataType.ALTITUDE.withValueSupplier(this::getAltitude));
		if(positionQualityIndicatorLayer.isPresent())
			metadata.add(NavigationMetadataType.SENSOR_TYPE.withValueSupplier(this::getSensorType));
		if(speedOverGroundLayer.isPresent())
			metadata.add(NavigationMetadataType.SPEED.withValueSupplier(this::getSpeed));
	}

	@Override
	public boolean isReadOnly() {
		return isReadOnly;
	}

	/** Close method **/
	@Override
	public void close() {
		if (dataContainerToken != null)
			dataContainerToken.close();
	}

	/**
	 * Flush
	 */
	public void flush() throws GIOException {
		dataContainer.flush();
		updateStatistics();
	}

	/** Updates the statistics and write the result in the NVI file */
	public void updateStatistics() throws GIOException {
		var stats = NavigationStatisticComputer.compute(this);
		info.setPositionNbr(stats.size());
		info.setGeoBox(new GeoBox(stats.north(), stats.south(), stats.east(), stats.west()));
		info.setStartDate(stats.startTime());
		info.setEndDate(stats.endTime());
	}

	@Override
	public int size() {
		return (int) latitudeLayer.getDimensions()[0];
	}

	@Override
	public String getFileName() {
		return dataContainer.getInfo().getPath();
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return dataContainer.getInfo().getContentType();
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return latitudeLayer.get(index);
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return longitudeLayer.get(index);
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	@Override
	public long getTime(int index) throws GIOException {
		return DateUtils.epochNanoToInstant(timeLayer.get(index)).toEpochMilli();
	}

	private float getHeading(int index) {
		return headingLayer.get(index);
	}

	private float getElevation(int index) {
		return verticalOffsetLayer.get(index);
	}

	private float getAltitude(int index) {
		return heightAboveReferenceEllipsoid.get(index);
	}

	private float getSpeed(int index) {
		if (speedOverGroundLayer.isPresent())
			return speedOverGroundLayer.get().get(index);
		else
			return Float.NaN;
	}

	private INavigationSensorType getSensorType(int index) {
		if (positionQualityIndicatorLayer.isPresent())
			return NmeaSensorType.get(positionQualityIndicatorLayer.get().get(index));
		else
			return NmeaSensorType.NOT_DEFINED;
	}

	@Override
	public MeasurandQualifierFlags getQualifierFlag(int index) {
		return MeasurandQualifierFlags.get(qualityFlagLayer.get(index));
	}

	@Override
	public void setQualifierFlag(int index, MeasurandQualifierFlags flag) {
		qualityFlagLayer.set(index, flag.getValue());
	}

}
