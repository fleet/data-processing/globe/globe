/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.nvi.service;

import java.util.List;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Writer of NVI files
 */
public interface INviWriter {

	/**
	 * Returns the service implementation of INviWriter.
	 */
	static INviWriter grab() {
		return OsgiUtils.getService(INviWriter.class);
	}

	/**
	 * Write on disk the changes made of the specified data if they match a NVI file
	 */
	void flush(INavigationData navigationData) throws GIOException;

	/**
	 * Exports navigation data
	 */
	void write(INavigationData navigationData, String outputFilePath) throws GIOException;

	/**
	 * Export navigation data to an new NVI file
	 */
	void write(INavigationData navigationData, String outputFilePath, List<Integer> indexesToExport)
			throws GIOException;

	/**
	 * Sampling modes
	 */
	public enum SamplingMode {
		NONE, TIME, SOUNDINGS;
	}

	/**
	 * Samples and exports navigation data to a new NVI file
	 */
	void write(INavigationDataSupplier source, String outputFilePath, SamplingMode sMode, int sValue)
			throws GIOException;
}
