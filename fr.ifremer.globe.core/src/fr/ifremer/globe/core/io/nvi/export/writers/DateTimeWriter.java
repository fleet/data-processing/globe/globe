package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

public class DateTimeWriter implements INviVariableWriter {

	private static final DataType BUFFER_TYPE = DataType.INT;

	/** Properties **/
	private NetcdfVariable timeVariable;
	private NetcdfVariable dateVariable;

	private long maxDateTime;
	private long minDateTime = Long.MAX_VALUE;

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		// DATE
		dateVariable = file.addVariable(NviConstants.NAME_DATE, DataType.INT, Arrays.asList(dim));
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_DATE);
		dateVariable.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_DATE);

		// TIME
		timeVariable = file.addVariable(NviConstants.NAME_TIME, DataType.INT, Arrays.asList(dim));
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_TIME);
		timeVariable.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_TIME);
	}

	@Override
	public void writeData(INavigationData dataProxy, List<Integer> indexesToExport)
			throws NCException, GIOException {

		// Initialize buffers
		ByteBuffer dateBuffer = ByteBuffer.allocate(indexesToExport.size() * BUFFER_TYPE.getSize());
		dateBuffer.order(ByteOrder.nativeOrder());

		ByteBuffer timeBuffer = ByteBuffer.allocate(indexesToExport.size() * BUFFER_TYPE.getSize());
		timeBuffer.order(ByteOrder.nativeOrder());

		// Fill buffers
		int[] dateAndTime;
		for (int i : indexesToExport) {
			long dateTime = dataProxy.getTime(i);
			// TODO ? if (dataProxy.getValidity(i) != NviConstants.INVALIDITY_FLAG_VALUE) {
				maxDateTime = Math.max(maxDateTime, dateTime);
				minDateTime = Math.min(minDateTime, dateTime);
			//}

			dateAndTime = DateUtils.getDateTime(dateTime);
			// ADD OFFSET ISSUE : DEFINED BUT NOT USED IN PREVIOUS NVI WRITER!!
			dateBuffer.putInt((int) (dateAndTime[0] + NviConstants.ADD_OFFSET_DATE));
			timeBuffer.putInt(dateAndTime[1]);
		}

		// Write
		dateVariable.write(new long[] { indexesToExport.size() }, dateBuffer, BUFFER_TYPE, Optional.empty());
		timeVariable.write(new long[] { indexesToExport.size() }, timeBuffer, BUFFER_TYPE, Optional.empty());
	}

	/**
	 * Writes start and end date/time global attributes
	 */
	@Override
	public void writeGlobalAttributes(NetcdfFile file) throws NCException {
		int[] dateAndTime = DateUtils.getDateTime(minDateTime);
		file.addAttribute(NviConstants.StartDate, (int) (dateAndTime[0] + NviConstants.ADD_OFFSET_DATE));
		file.addAttribute(NviConstants.StartTime, dateAndTime[1]);

		dateAndTime = DateUtils.getDateTime(maxDateTime);
		file.addAttribute(NviConstants.EndDate, (int) (dateAndTime[0] + NviConstants.ADD_OFFSET_DATE));
		file.addAttribute(NviConstants.EndTime, dateAndTime[1]);
	}

}
