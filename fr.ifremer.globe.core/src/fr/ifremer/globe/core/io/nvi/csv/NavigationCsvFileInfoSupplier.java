/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.nvi.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.BeanVerifier;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.io.csv.ICsvFileInfoSupplier;
import fr.ifremer.globe.core.io.csv.parsing.CsvTimestampedBean;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.utils.csv.CsvUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Loader of {@link NavigationCsvFileInfo}.
 */
@Component(name = "globe_drivers_navigation_csv_file_info_supplier", service = { ICsvFileInfoSupplier.class,
		INavigationSupplier.class })
public class NavigationCsvFileInfoSupplier
		implements ICsvFileInfoSupplier, INavigationSupplier, BeanVerifier<NavigationPointCsv> {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(NavigationCsvFileInfoSupplier.class);

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return ContentType.NAVIGATION_CSV;
	}

	/**
	 * A Navigation file may have the columns longitude, latitude, immersion, cap, date, time
	 */
	@Override
	public Optional<FileInfoSettings> evaluatesSettings(String filePath, List<String> firstLines) {
		FileInfoSettings result = new FileInfoSettings(getContentType());
		result.setElevationScaleFactor(1d);
		result.setRowCountToSkip(1);
		result.setLocale(Locale.US);

		String line = firstLines.get(0);
		var sep = CsvUtils.guessDelimiter(line);
		int[] colIndex = new int[NavigationPointCsv.COLUMNS.size()];
		Arrays.fill(colIndex, -1);
		if (sep.isPresent()) {
			char delimiter = sep.get();
			result.setDelimiter(delimiter);

			// Search columns index
			var columns = CsvUtils.parseLine(firstLines.get(0), delimiter);
			for (int i = 0; i < NavigationPointCsv.COLUMNS.size(); i++)
				colIndex[i] = CsvUtils.guessColumnIndex(columns, NavigationPointCsv.COLUMNS.get(i));
		}

		for (int i = 0; i < NavigationPointCsv.COLUMNS.size(); i++)
			result.set(NavigationPointCsv.COLUMNS.get(i), colIndex[i] >= 0 ? colIndex[i] : -2);

		// Add optional columns (Only longitude and latitude are mandatory)
		CsvTimestampedBean.getAllHeaders().forEach(result::addNonNumericColumn);
		CsvTimestampedBean.getAllHeaders().forEach(result::addOptionalColumn);
		result.addOptionalColumn(NavigationPointCsv.COL_HEADING);
		result.addOptionalColumn(NavigationPointCsv.COL_ELEVATION);

		return Optional.of(result);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ICsvFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		NavigationCsvFileInfo result = null;

		// Extract column names with new CSV marker
		try {
			// OpenCsv strategy : mapping each attributes to a column index
			var strategy = new NavigationMappingStrategy(settings);
			var errors = new ArrayList<CsvException>();

			List<NavigationPointCsv> beans = //
					new CsvToBeanBuilder<NavigationPointCsv>(new FileReader(new File(filePath)))//
							.withMappingStrategy(strategy)//
							.withSeparator(settings.getDelimiter())//
							.withSkipLines(settings.getRowCountToSkip()) //
							.withVerifier(this)//
							.withExceptionHandler(e -> {
								logger.error("Error while parsing navigation CSV at line " + e.getLineNumber() + " : "
										+ e.getMessage(), e);
								errors.add(e);
								return e;
							}) //
							.build()//
							.parse();

			beans.forEach(
					navigationPoint -> navigationPoint.setImmersionScaleFactor(settings.getElevationScaleFactor()));

			result = new NavigationCsvFileInfo(filePath, beans, errors);
		} catch (RuntimeException | FileNotFoundException ex) {
			// Something went wrong : CSV not acceptable
			logger.warn("{} was unable to parse the file {} : {}", getClass().getSimpleName(), filePath,
					ex.getMessage());
		}
		return Optional.ofNullable(result);

	}

	/** {@inheritDoc} */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return fileInfo instanceof NavigationCsvFileInfo navfileInfo ? Optional.of(navfileInfo) : Optional.empty();
	}

	@Override
	public boolean verifyBean(NavigationPointCsv bean) throws CsvConstraintViolationException {
		try {
			bean.parseDateTime();
		} catch (DateTimeParseException | GIOException e) {
			throw new CsvConstraintViolationException(e, e.getMessage());
		}
		return true;
	}

}