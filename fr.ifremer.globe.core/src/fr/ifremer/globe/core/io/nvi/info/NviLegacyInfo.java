package fr.ifremer.globe.core.io.nvi.info;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.datacontainer.NviLayers;
import fr.ifremer.globe.core.io.nvi.datacontainer.NviLegacyNavigation;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides information about a navigation file (.nvi).
 */
public class NviLegacyInfo extends AbstractNviInfo implements INavigationDataSupplier {

	private static final Logger LOGGER = LoggerFactory.getLogger(NviLegacyInfo.class);

	/**
	 * Constructor
	 */
	public NviLegacyInfo(String filePath) {
		super(filePath, ContentType.NVI_NETCDF_4);
	}

	@Override
	public NviLegacyNavigation getNavigationData(boolean readonly) throws GIOException {
		return new NviLegacyNavigation(this, readonly);
	}

	/**
	 * Loads properties from the {@link NetcdfFile}.
	 */
	@Override
	public void load() {
		try {
			open(true);

			// start / end dates
			int nviDate = ncFile.getAttributeInt(NviConstants.StartDate);
			int nviTime = ncFile.getAttributeInt(NviConstants.StartTime);
			setStartDate(DateUtils.getInstantFromJulian(nviDate, nviTime));
			nviDate = ncFile.getAttributeInt(NviConstants.EndDate);
			nviTime = ncFile.getAttributeInt(NviConstants.EndTime);
			setEndDate(DateUtils.getInstantFromJulian(nviDate, nviTime));

			// geographic box
			double north = ncFile.getAttributeDouble(NviConstants.NorthLatitude);
			double south = ncFile.getAttributeDouble(NviConstants.SouthLatitude);
			double east = ncFile.getAttributeDouble(NviConstants.EastLongitude);
			double west = ncFile.getAttributeDouble(NviConstants.WestLongitude);
			setGeoBox(new GeoBox(north, south, east, west));

			// position number
			setPositionNbr(ncFile.getAttributeInt(NviConstants.PointCounter));

		} catch (NCException | GIOException e) {
			LOGGER.error("Exception reading NVI file: " + filePath, e);
		} finally {
			close();
		}
	}

	/**
	 * Writes properties in the {@link NetcdfFile}.
	 */
	public void flush() throws GIOException {
		try {
			open(false);
			ncFile.redef();

			// start / end dates
			int[] dateAndTime = DateUtils.getDateTime(startDate.toEpochMilli());
			ncFile.addAttribute(NviConstants.StartDate, dateAndTime[0] + NviConstants.ADD_OFFSET_DATE);
			ncFile.addAttribute(NviConstants.StartTime, dateAndTime[1]);
			dateAndTime = DateUtils.getDateTime(endDate.toEpochMilli());
			ncFile.addAttribute(NviConstants.EndDate, dateAndTime[0] + NviConstants.ADD_OFFSET_DATE);
			ncFile.addAttribute(NviConstants.EndTime, dateAndTime[1]);

			// geographic box
			ncFile.addAttribute(NviConstants.NorthLatitude, geobox.getTop());
			ncFile.addAttribute(NviConstants.SouthLatitude, geobox.getBottom());
			ncFile.addAttribute(NviConstants.EastLongitude, geobox.getRight());
			ncFile.addAttribute(NviConstants.WestLongitude, geobox.getLeft());

			// position number
			ncFile.addAttribute(NviConstants.PointCounter, positionNbr);

			ncFile.enddef();
		} catch (NCException e) {
			throw new GIOException("Error while flushing NVI file: " + filePath, e);
		} finally {
			close();
		}
	}

	/**
	 * Declares NVI layers to set in {@link DataContainer}.
	 */
	@Override
	public void declareLayers(DataContainer<?> container) throws GException {
		Map<String, PredefinedLayers<?>> map = new HashMap<>();

		// Get NVI predefined layer by reflection
		Field[] declaredFields = NviLayers.class.getDeclaredFields();
		List<Field> staticFields = new ArrayList<>();
		for (Field field : declaredFields) {
			if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
				staticFields.add(field);
				try {
					if (PredefinedLayers.class.isInstance(field.get(null))) {
						PredefinedLayers<?> layer = (PredefinedLayers<?>) field.get(null);
						// Define a new NVI predefined layer from the specified class
						map.put("/" + layer.getName(), layer);
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					LOGGER.error("Error while declaring field " + field.getName(), e);
				}
			}
		}
		NetcdfLayerDeclarer netcdfLayerDeclarer = new NetcdfLayerDeclarer();
		netcdfLayerDeclarer.declareLayers(ncFile, container, group -> NviLayers.GROUP,
				variable -> Optional.ofNullable(map.get(variable.getPath())));
	}

	/**
	 * Computes and writes global attributes.
	 */
	public void updateStatistics() throws GIOException {
		try (var nav = getNavigationData(false)) {
			nav.updateStatistics();
		}
	}

}