package fr.ifremer.globe.core.io.nvi.export;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;

import fr.ifremer.globe.core.io.nvi.export.writers.AltitudeWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.DateTimeWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.HeadingWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.HistoryWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.INviVariableWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.ImmersionWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.LatitudeWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.LongitudeWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.QualityWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.SpeedWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.TypeWriter;
import fr.ifremer.globe.core.io.nvi.export.writers.ValidityFlagWriter;
import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Version;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to generate navigation file (.nvi)
 */
public class NviExporter {

	/** Constructor **/
	private NviExporter() {
		// utility class
	}

	//// PUBLIC API

	/**
	 * Exports navigation data
	 */
	public static boolean export(INavigationData navigationData, String outputFilePath) throws GIOException {
		// Export all points
		List<Integer> indexesToExport = IntStream.range(0, navigationData.size()).boxed().collect(Collectors.toList());
		return export(navigationData, outputFilePath, indexesToExport);
	}

	/**
	 * Samples and exports navigation data to a new NVI file
	 */
	public static boolean export(INavigationDataSupplier source, String outputFilePath, SamplingMode sMode, int sValue)
			throws GIOException {
		try (INavigationData navigationData = source.getNavigationData(true)) {
			IntList indexesToExport = applySampling(navigationData, sMode, sValue);
			return export(navigationData, outputFilePath, IntStream.of(indexesToExport.toArray()).boxed().toList());
		} catch (IOException e) {
			throw new GIOException(e.getMessage(), e); // Converts IOException in GIOException
		}
	}

	/**
	 * Samples navigation data
	 */
	public static IntList applySampling(INavigationData navigationData, SamplingMode sMode, int sValue)
			throws GIOException {
		// Compute indexes to export
		var result = new ArrayIntList();
		switch (sMode) {
		// Soundings sampling
		case SOUNDINGS:
			int newSounding = sValue;
			for (int i = 0; i < navigationData.size(); i++) {
				if (newSounding == sValue) {
					newSounding = 0;
					result.add(i);
				}
				newSounding++;
			}
			break;
		// Time sampling
		case TIME:
			long lastSecond = navigationData.getTime(0);
			result.add(0);
			for (int i = 0; i < navigationData.size(); i++) {
				long currentSecond = navigationData.getTime(i);
				if (currentSecond - lastSecond >= 1000 * sValue) {
					result.add(i);
					lastSecond = currentSecond;
				}
			}
			break;
		// None
		case NONE:
		default:
			IntStream.range(0, navigationData.size()).forEach(result::add);
			break;
		}

		return result;
	}

	//// PRIVATE METHODS

	/**
	 * Export navigation data to an new NVI file
	 */
	public static boolean export(INavigationData navigationData, String outputFilePath, List<Integer> indexesToExport)
			throws GIOException {

		// List of {@link INviVariableWriter} used to write the file
		INviVariableWriter[] varWriters = { new AltitudeWriter(), new DateTimeWriter(), new HeadingWriter(),
				new HistoryWriter(), new ImmersionWriter(), new LatitudeWriter(), new LongitudeWriter(),
				new ValidityFlagWriter(), new QualityWriter(), new TypeWriter(), new SpeedWriter() };

		// if output already file exists, it will be overwrite
		try {
			Files.deleteIfExists(new File(outputFilePath).toPath());
		} catch (IOException e1) {
			throw new GIOException("Export to NVI : error while overiting output file : " + e1.getMessage(), e1);
		}

		// Creates file
		try (NetcdfFile file = new NetcdfFile(
				NCFile.createNew(Version.netcdf4, outputFilePath).withCompressionDisabled())) {

			// Write position dimension
			NCDimension dim = file.addDimension(NviConstants.DIM_POSITION_NBR, indexesToExport.size());

			// Define and write variables
			for (INviVariableWriter writer : varWriters)
				writer.define(file, dim);

			for (INviVariableWriter writer : varWriters)
				writer.writeData(navigationData, indexesToExport);

			// Write global attributes
			writeGlobalAttributes(file, varWriters, indexesToExport.size());

			file.synchronize();

		} catch (GIOException | NCException e) {
			throw new GIOException("Error during nvi file export: " + e.getMessage(), e);
		}
		return true;
	}

	private static final int COMMENT_ATT_LENGTH = 256;
	private static final int NAME_ATT_LENGTH = 20;

	/**
	 * Writes global attributes
	 */
	private static void writeGlobalAttributes(NetcdfFile file, INviVariableWriter[] varWriters, int pointNbr)
			throws NCException {
		// Pass in definition mode
		file.redef();

		file.addAttribute(NviConstants.Version, NviConstants.ACTUAL_VERSION);// NVI version
		file.addAttribute(NviConstants.GLOBAL_ATT_CLASSE, NviConstants.CLASSE);
		file.addAttribute(NviConstants.Level, (short) 0);
		file.addAttribute(NviConstants.TimeReference, NviConstants.TIME_REFERENCE_ATTRIBUTE);
		file.addAttribute(NviConstants.Name, file.getName());
		file.addAttribute(NviConstants.Ship, createString("", COMMENT_ATT_LENGTH));
		file.addAttribute(NviConstants.Survey, createString("", COMMENT_ATT_LENGTH));
		file.addAttribute(NviConstants.Reference, createString("", COMMENT_ATT_LENGTH));
		file.addAttribute(NviConstants.CDI, createString("", 100));

		// Computes some global attributes with writers( Geographic and date/time limits...)
		for (INviVariableWriter writer : varWriters)
			writer.writeGlobalAttributes(file);

		// Ellipsoid
		file.addAttribute(NviConstants.EllipsoidName, "WGS-84");
		file.addAttribute(NviConstants.EllipsoidA, NviConstants.ELLIPSOID_A);
		file.addAttribute(NviConstants.EllipsoidInvF, NviConstants.ELLIPSOID_INVF);
		file.addAttribute(NviConstants.EllipsoidE2, NviConstants.ELLIPSOID_E2);

		// Meridian 180 & geodesic system
		file.addAttribute(NviConstants.Meridian180, (char) 32);
		file.addAttribute(NviConstants.GeodesicSystem, createString("", NAME_ATT_LENGTH));

		// Projection
		file.addAttribute(NviConstants.ProjType, (short) 0);
		double[] projParameterValue = { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };
		file.addAttribute(NviConstants.ProjParameterValue, projParameterValue);
		file.addAttribute(NviConstants.ProjParameterCode, createString("", COMMENT_ATT_LENGTH));

		file.addAttribute(NviConstants.PointCounter, pointNbr);

		// TODO to removed when test updated
		file.addAttribute("mbGeoDictionnary", "");
		file.addAttribute("mbGeoRepresentation", "");

		file.enddef();
	}

	public static String createString(String value, int length) {
		StringBuilder sbString = new StringBuilder(length);
		sbString.append(value);
		for (int i = 0; i < (length - value.length()); i++)
			sbString.append(' ');
		return sbString.toString();
	}
}
