package fr.ifremer.globe.core.io.nvi.datacontainer;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;

/** NVI layers of {@link DataContainer} **/
public class NviLayers {

	private NviLayers() {
	}

	public static final String GROUP = "/";

	/** Layer: DATE **/
	public static final PredefinedLayers<IntLoadableLayer1D> DATE = new PredefinedLayers<>(GROUP, NviConstants.NAME_DATE,
			NviConstants.LONG_NAME_DATE,NviConstants.UNITS_DATE, DataKind.continuous, IntLoadableLayer1D::new);
	

	/** Layer: TIME **/
	public static final PredefinedLayers<IntLoadableLayer1D> TIME = new PredefinedLayers<>(GROUP, NviConstants.NAME_TIME,
			NviConstants.LONG_NAME_TIME,NviConstants.UNITS_TIME, DataKind.continuous, IntLoadableLayer1D::new);
	

	/** Layer: LATITUDE **/
	public static final PredefinedLayers<DoubleLoadableLayer1D> LATITUDE = new PredefinedLayers<>(GROUP, NviConstants.NAME_LATITUDE,
			NviConstants.LONG_NAME_LATITUDE,NviConstants.UNITS_LATITUDE, DataKind.continuous, DoubleLoadableLayer1D::new);
	

	/** Layer: LONGITUDE **/
	public static final PredefinedLayers<DoubleLoadableLayer1D> LONGITUDE = new PredefinedLayers<>(GROUP, NviConstants.NAME_LONGITUDE,
			NviConstants.LONG_NAME_LONGITUDE,NviConstants.UNITS_LONGITUDE, DataKind.continuous, DoubleLoadableLayer1D::new);
	

	/** Layer: ALTITUDE **/
	public static final PredefinedLayers<FloatLoadableLayer1D> ALTITUDE = new PredefinedLayers<>(GROUP, NviConstants.NAME_ALTITUDE,
			NviConstants.LONG_NAME_ALTITUDE,NviConstants.UNITS_ALTITUDE, DataKind.continuous, FloatLoadableLayer1D::new);
	

	/** Layer: IMMERSION **/
	public static final PredefinedLayers<FloatLoadableLayer1D> IMMERSION = new PredefinedLayers<>(GROUP, NviConstants.NAME_IMMERSION,
			NviConstants.LONG_NAME_IMMERSION,NviConstants.UNITS_IMMERSION, DataKind.continuous, FloatLoadableLayer1D::new);
	

	/** Layer: HEADING **/
	public static final PredefinedLayers<FloatLoadableLayer1D> HEADING = new PredefinedLayers<>(GROUP, NviConstants.NAME_HEADING,
			NviConstants.LONG_NAME_HEADING,NviConstants.UNITS_HEADING, DataKind.continuous, FloatLoadableLayer1D::new);
	

	/** Layer: SPEED **/
	public static final PredefinedLayers<FloatLoadableLayer1D> SPEED = new PredefinedLayers<>(GROUP, NviConstants.NAME_SPEED,
			NviConstants.LONG_NAME_SPEED,NviConstants.UNITS_SPEED, DataKind.continuous, FloatLoadableLayer1D::new);
	

	/** Layer: POISTION_TYPE **/
	public static final PredefinedLayers<ByteLoadableLayer1D> P_TYPE = new PredefinedLayers<>(GROUP, NviConstants.NAME_PTYPE,
			NviConstants.LONG_NAME_PTYPE,NviConstants.UNITS_PTYPE, DataKind.continuous, ByteLoadableLayer1D::new);
	

	/** Layer: POSITION_QUALITY **/
	public static final PredefinedLayers<ByteLoadableLayer1D> P_QUALITY = new PredefinedLayers<>(GROUP, NviConstants.NAME_PQUALITY,
			NviConstants.LONG_NAME_PQUALITY,NviConstants.UNITS_PQUALITY, DataKind.continuous, ByteLoadableLayer1D::new);
	

	/** Layer: POSITION_FLAG **/
	public static final PredefinedLayers<ByteLoadableLayer1D> VALIDITY_FLAG = new PredefinedLayers<>(GROUP, NviConstants.NAME_PFLAG,
			NviConstants.LONG_NAME_PFLAG,NviConstants.UNITS_PFLAG, DataKind.continuous, ByteLoadableLayer1D::new);

}
