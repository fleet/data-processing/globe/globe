package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class SpeedWriter extends SimpleVariableWriter {

	/** Constructor **/
	public SpeedWriter() {
		super(NviConstants.NAME_SPEED, NviConstants.DATA_TYPE_SPEED, DataType.FLOAT);
	}

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		super.define(file, dim);
		var.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_SPEED);
		var.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_SPEED);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, INavigationData dataProxy, int index) throws GIOException {
		buffer.putFloat(dataProxy.getMetadataValue(NavigationMetadataType.SPEED, index).orElse(Float.NaN));
	}

}
