package fr.ifremer.globe.core.io.nvi.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.info.NviV2Info;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoSupplier;

@Component(name = "globe_nvi_v2_service_info_supplier", service = { IFileInfoSupplier.class, INavigationSupplier.class,
		IDataContainerInfoSupplier.class })
public class NviV2InfoSupplier extends BasicFileInfoSupplier<NviV2Info>
		implements IDataContainerInfoSupplier<NviV2Info>, INavigationSupplier {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(NviV2InfoSupplier.class);

	/**
	 * Constructor
	 */
	public NviV2InfoSupplier() {
		super("nvi.nc", "Navigation file (*.nvi.nc)", ContentType.NVI_V2_NETCDF_4);
	}

	@Override
	public NviV2Info makeFileInfo(String filePath) {
		return new NviV2Info(filePath);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof NviV2Info nviInfo ? nviInfo : null);
	}

	/**
	 * Implementation of {@link INavigationSupplier} to provide an {@link INavigationDataSupplier} from a nvi file.
	 *
	 * @return {@link INavigationDataSupplier}
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(String filePath) {
		return getFileInfo(filePath).map(INavigationDataSupplier.class::cast);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<NviV2Info> getDataContainerInfo(String filePath) {
		return getFileInfo(filePath);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<NviV2Info> getDataContainerInfo(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof NviV2Info nviInfo ? nviInfo : null);
	}
}
