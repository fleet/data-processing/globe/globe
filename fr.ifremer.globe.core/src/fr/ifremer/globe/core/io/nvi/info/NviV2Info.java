package fr.ifremer.globe.core.io.nvi.info;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.datacontainer.NavigationLayers;
import fr.ifremer.globe.core.io.nvi.datacontainer.NviV2Navigation;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.utils.NavigationStatisticComputer;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides information about a navigation file (.nvi.nc).
 */
public class NviV2Info extends AbstractNviInfo implements INavigationDataSupplier {

	private static final Logger LOGGER = LoggerFactory.getLogger(NviV2Info.class);

	/**
	 * Constructor
	 */
	public NviV2Info(String filePath) {
		super(filePath, ContentType.NVI_V2_NETCDF_4);
	}

	@Override
	public NviV2Navigation getNavigationData(boolean readonly) throws GIOException {
		return new NviV2Navigation(this, readonly);
	}

	/**
	 * Loads properties from the {@link NetcdfFile}.
	 */
	@Override
	public void load() {
		try (var navigationData = getNavigationData(true)) {
			var stats = NavigationStatisticComputer.compute(navigationData);
			setPositionNbr(stats.size());
			setGeoBox(new GeoBox(stats.north(), stats.south(), stats.east(), stats.west()));
			setStartDate(stats.startTime());
			setEndDate(stats.endTime());
		} catch (GIOException e) {
			LOGGER.error("Exception reading NVI file: " + filePath, e);
		}
	}

	/**
	 * Declares NVI layers to set in {@link DataContainer}.
	 */
	@Override
	public void declareLayers(DataContainer<?> container) throws GException {
		var map = NviV2Constants.NAVIGATION_LAYERS.stream()
				.collect(Collectors.toMap(PredefinedLayers::getPath, Function.identity()));
		new NetcdfLayerDeclarer().declareLayers(ncFile, container, group -> NavigationLayers.GROUP,
				variable -> Optional.ofNullable(map.get(variable.getPath())));
	}
}