package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.quality.NviLegacyQualifierFlags;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ValidityFlagWriter extends SimpleVariableWriter {

	/** Constructor **/
	public ValidityFlagWriter() {
		super(NviConstants.NAME_PFLAG, NviConstants.DATA_TYPE_PFLAG, DataType.INT);
	}

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		super.define(file, dim);
		var.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_FLAG_VALUES, NviConstants.FLAG_VALUES_PFLAG);
		var.addAttribute(NviConstants.ATTRIBUTE_FLAG_MEANINGS, NviConstants.FLAG_MEANINGS_PFLAG);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, INavigationData dataProxy, int index) throws GIOException {
		// parse quality flag to legacy flag values
		buffer.putInt(NviLegacyQualifierFlags.get(dataProxy.getQualifierFlag(index)).getValue());
	}

}
