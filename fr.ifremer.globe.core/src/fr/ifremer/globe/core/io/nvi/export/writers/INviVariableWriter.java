package fr.ifremer.globe.core.io.nvi.export.writers;

import java.util.List;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This interface provides methods to define and fill an netcdf variable
 */
public interface INviVariableWriter {

	/**
	 * Declares the variable
	 */
	public void define(NetcdfFile file, NCDimension dim) throws NCException;

	/**
	 * Writes data in the variable
	 */
	public void writeData(INavigationData dataProxy, List<Integer> indexesToExport)
			throws NCException, GIOException;

	/**
	 * Optional method to write global attributes
	 */
	public default void writeGlobalAttributes(NetcdfFile file) throws NCException {
	}
}
