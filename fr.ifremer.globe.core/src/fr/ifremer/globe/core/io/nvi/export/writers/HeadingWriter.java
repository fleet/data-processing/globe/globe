package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class HeadingWriter extends SimpleVariableWriter {

	/** Constructor **/
	public HeadingWriter() {
		super(NviConstants.NAME_HEADING, NviConstants.DATA_TYPE_HEADING, DataType.FLOAT);
	}

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		super.define(file, dim);
		var.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_HEADING);
		var.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_HEADING);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, INavigationData dataProxy, int index) throws GIOException {
		buffer.putFloat(dataProxy.getMetadataValue(NavigationMetadataType.HEADING, index).orElse(Float.NaN));
	}

}
