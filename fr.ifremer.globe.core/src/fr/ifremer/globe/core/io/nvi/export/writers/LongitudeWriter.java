package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class LongitudeWriter extends SimpleVariableWriter {

	/** Properties **/
	private GeoBoxBuilder geoBoxBuilder = new GeoBoxBuilder();

	/** Constructor **/
	public LongitudeWriter() {
		super(NviConstants.NAME_LONGITUDE, NviConstants.DATA_TYPE_LONGITUDE, DataType.DOUBLE);
	}

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		super.define(file, dim);
		var.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_LONGITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_LONGITUDE);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, INavigationData dataProxy, int index) throws GIOException {
		double value = dataProxy.getLongitude(index);
		if (!Double.isNaN(value)) { // TODO ? && dataProxy.getValidity(index) != NviConstants.INVALIDITY_FLAG_VALUE) {
			geoBoxBuilder.addPoint(value, 0d);
		}
		buffer.putDouble(value);
	}

	@Override
	public void writeGlobalAttributes(NetcdfFile file) throws NCException {
		var geobox = geoBoxBuilder.build();
		file.addAttribute(NviConstants.EastLongitude, geobox.getRight());
		file.addAttribute(NviConstants.WestLongitude, geobox.getLeft());
	}
}
