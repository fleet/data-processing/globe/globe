package fr.ifremer.globe.core.io.nvi.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.info.NviLegacyInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoSupplier;

@Component(name = "globe_nvi_legacy_service_info_supplier", service = { IFileInfoSupplier.class,
		INavigationSupplier.class, IDataContainerInfoSupplier.class })
public class NviLegacyInfoSupplier extends BasicFileInfoSupplier<NviLegacyInfo>
		implements IDataContainerInfoSupplier<NviLegacyInfo>, INavigationSupplier {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(NviLegacyInfoSupplier.class);

	/**
	 * Constructor
	 */
	public NviLegacyInfoSupplier() {
		super("nvi", "Navigation file (*.nvi)", ContentType.NVI_NETCDF_4);
	}

	@Override
	public NviLegacyInfo makeFileInfo(String filePath) {
		return new NviLegacyInfo(filePath);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof NviLegacyInfo nviInfo ? nviInfo : null);
	}

	/**
	 * Implementation of {@link INavigationSupplier} to provide an {@link INavigationDataSupplier} from a nvi file.
	 *
	 * @return {@link INavigationDataSupplier}
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(String filePath) {
		return getFileInfo(filePath).map(INavigationDataSupplier.class::cast);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<NviLegacyInfo> getDataContainerInfo(String filePath) {
		return getFileInfo(filePath);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<NviLegacyInfo> getDataContainerInfo(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof NviLegacyInfo nviInfo ? nviInfo : null);
	}
}
