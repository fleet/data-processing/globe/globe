package fr.ifremer.globe.core.io.nvi.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.csv.IWellKnownCsvFileInfoSupplier;
import fr.ifremer.globe.core.io.csv.parsing.CsvTimestampedBean;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.FileUtils;

/**
 * Supplier of {@link IFileInfo} for DelphiNS files.<br>
 * A DelphiNS file has a well known format and can be parsed without any FileInfoSettings.
 */
@Component(name = "globe_drivers_delphins_csv_file_info_supplier", service = IWellKnownCsvFileInfoSupplier.class)
public class DelphinsInfoSupplier implements IWellKnownCsvFileInfoSupplier {

	public static final String DELPHINS_EXTENSION = "txt";

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return ContentType.NAVIGATION_CSV;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath) {
		Optional<FileInfoSettings> settings = evaluateSettings(filePath);
		if (settings.isPresent()) {
			NavigationCsvFileInfoSupplier navCsvFileInfoSupplier = new NavigationCsvFileInfoSupplier();
			return navCsvFileInfoSupplier.getFileInfo(filePath, settings.get()).map(IFileInfo.class::cast);
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<FileInfoSettings> evaluateSettings(String filePath) {
		if (isDelphiNSFile(filePath)) {
			FileInfoSettings result = new FileInfoSettings(ContentType.NAVIGATION_CSV);
			result.setRowCountToSkip(1);
			result.setDelimiter('\t');
			result.set(CsvTimestampedBean.DATE_HEADER, 0);
			result.set(CsvTimestampedBean.TIME_HEADER, 1);
			result.set(NavigationPointCsv.COL_LAT, 2);
			result.set(NavigationPointCsv.COL_LON, 3);
			result.set(NavigationPointCsv.COL_ELEVATION, 4);
			result.setLocale(Locale.US);
			result.setElevationScaleFactor(-1);
			return Optional.of(result);
		}

		return Optional.empty();
	}

	/** Check if the specified file contains the DelphiNS header */
	private boolean isDelphiNSFile(String filePath) {
		var fileExtension = FileUtils.getExtension(filePath);
		if (fileExtension.equalsIgnoreCase(DELPHINS_EXTENSION)) {

			try (var reader = new BufferedReader(new FileReader(filePath, StandardCharsets.UTF_8))) {
				String firstLine = reader.readLine();
				return "Date\tHeure\tLatitude\tLongitude\tImmersion".equalsIgnoreCase(firstLine);
			} catch (IOException | UncheckedIOException e) {
				// Not a DelphiNS file or not in UTF8
			}
		}
		return false;
	}

}