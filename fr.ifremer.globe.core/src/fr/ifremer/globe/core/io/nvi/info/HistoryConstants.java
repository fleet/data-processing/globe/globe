package fr.ifremer.globe.core.io.nvi.info;

import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;

/**
 * Constants of history netcdf variables
 */
public class HistoryConstants {

	private HistoryConstants() {
	}

	public static final String DIM_RECORD_NUMBER = "mbHistoryRecNbr";
	public static final int DIM_RECORD_NUMBER_VALUE = 20;
	public static final String DIM_NAME_LENGTH = "mbNameLength";
	public static final int DIM_NAME_LENGTH_VALUE = 20;
	public static final String DIM_COMMENT_LENGTH = "mbCommentLength";
	public static final int DIM_COMMENT_LENGTH_VALUE = 256;

	public static final String DATE = "History Date";
	public static final String DATE_VALUE = "mbHistDate";

	public static final String TIME = "History Time";
	public static final String TIME_VALUE = "mbHistTime";

	public static final String CODE = "History Code";
	public static final String CODE_VALUE = "mbHistCode";

	public static final String AUTOR = "History Author";
	public static final String AUTOR_VALUE = "mbHistAutor";

	public static final String MODULE = "History Module";
	public static final String MODULE_VALUE = "mbHistModule";

	public static final String COMMENT = "History Comment";
	public static final String COMMENT_VALUE = "mbHistComment";

	public static final String TYPE = "type";
	public static final String LONG_NAME = "long_name";
	public static final String NAME_CODE = "name_code";
	public static final String UNITS = CFStandardNames.CF_UNITS;
	public static final String UNIT_CODE = "unit_code";
	public static final String ADD_OFFSET = "add_offset";
	public static final String SCALE_FACTOR = "scale_factor";
	public static final String MINIMUM = "minimum";
	public static final String MAXIMUM = "maximum";
	public static final String VALID_MINIMUM = "valid_minimum";
	public static final String VALID_MAXIMUM = "valid_maximum";
	public static final String MISSING_VALUE = "missing_value";
	public static final String FORMAT_C = "format_C";
	public static final String ORIENTATION = "orientation";

	public static final String TYPE_TIME = "integer";
	public static final String LONG_NAME_TIME = "History time (UT)";
	public static final String NAME_CODE_TIME = "MB_HISTORY_TIME";
	public static final String UNITS_TIME = "ms";
	public static final String UNIT_CODE_TIME = "MB_MS";
	public static final int ADD_OFFSET_TIME = 0;
	public static final int SCALE_FACTOR_TIME = 1;
	public static final int MINIMUM_TIME = 0;
	public static final int MAXIMUM_TIME = 86399999;
	public static final int VALID_MINIMUM_TIME = 0;
	public static final int VALID_MAXIMUM_TIME = 86399999;
	public static final int MISSING_VALUE_TIME = -2147483648;
	public static final String FORMAT_C_TIME = "%d";
	public static final String ORIENTATION_TIME = "direct";

	public static final String TYPE_DATE = "integer";
	public static final String LONG_NAME_DATE = "History date";
	public static final String NAME_CODE_DATE = "MB_HISTORY_DATE";
	public static final String UNITS_DATE = "Julian_date";
	public static final String UNIT_CODE_DATE = "MB_JULIAN_DATE";
	public static final int ADD_OFFSET_DATE = 2440588;
	public static final int SCALE_FACTOR_DATE = 1;
	public static final int MINIMUM_DATE = -25567;
	public static final int MAXIMUM_DATE = 47482;
	public static final int VALID_MINIMUM_DATE = -25567;
	public static final int VALID_MAXIMUM_DATE = 47482;
	public static final int MISSING_VALUE_DATE = 2147483647;
	public static final String FORMAT_C_DATE = "%d";
	public static final String ORIENTATION_DATE = "direct";

	public static final String TYPE_AUTOR = "string";
	public static final String LONG_NAME_AUTOR = "History autor";
	public static final String NAME_CODE_AUTOR = "MB_HISTORY_AUTOR";

	public static final String TYPE_CODE = "integer";
	public static final String LONG_NAME_CODE = "History code";
	public static final String NAME_CODE_CODE = "MB_HISTORY_CODE";
	public static final String UNITS_CODE = "";
	public static final String UNIT_CODE_CODE = "MB_NOT_DEFINED";
	public static final int ADD_OFFSET_CODE = 0;
	public static final int SCALE_FACTOR_CODE = 1;
	public static final int MINIMUM_CODE = 1;
	public static final int MAXIMUM_CODE = 255;
	public static final int VALID_MINIMUM_CODE = 1;
	public static final int VALID_MAXIMUM_CODE = 255;
	public static final int MISSING_VALUE_CODE = 0;
	public static final String FORMAT_C_CODE = "%d";
	public static final String ORIENTATION_CODE = "direct";

	public static final String TYPE_COMMENT = "string";
	public static final String LONG_NAME_COMMENT = "History comment";
	public static final String NAME_CODE_COMMENT = "MB_HISTORY_COMMENT";

	public static final String TYPE_MODULE = "string";
	public static final String LONG_NAME_MODULE = "History module";
	public static final String NAME_CODE_MODULE = "MB_HISTORY_MODULE";
}
