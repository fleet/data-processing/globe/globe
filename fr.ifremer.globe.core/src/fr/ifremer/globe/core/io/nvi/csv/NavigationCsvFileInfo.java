/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.nvi.csv;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * FileInfo dedicated to navigation CSV files.
 */
public class NavigationCsvFileInfo extends BasicFileInfo
		implements ICsvFileInfo, INavigationData, INavigationDataSupplier {

	/** All markers */
	private final List<NavigationPointCsv> navigationPoints;

	/** Metadata **/
	private final List<NavigationMetadata<?>> metadata;

	/** Errors while reading CSV file **/
	private final List<CsvException> parsingErrors;

	/**
	 * Constructor
	 * 
	 * @param errors
	 */
	public NavigationCsvFileInfo(String csvFilePath, List<NavigationPointCsv> navigationPoints,
			List<CsvException> errors) {
		super(csvFilePath, ContentType.NAVIGATION_CSV);
		this.navigationPoints = navigationPoints;
		this.metadata = List.of(
				NavigationMetadataType.TIME
						.withValueSupplier(index -> navigationPoints.get(index).getTime().toEpochMilli()),
				NavigationMetadataType.ELEVATION
						.withValueSupplier(index -> (float) navigationPoints.get(index).getElevation()),
				NavigationMetadataType.HEADING
						.withValueSupplier(index -> (float) navigationPoints.get(index).getHeading()));
		this.parsingErrors = errors;
		computeBoundingBox();

	}

	/** {@inheritDoc} */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = super.getProperties();
		properties.add(Property.build("Number of points", navigationPoints.size()));
		if (!navigationPoints.isEmpty()) {
			properties.add(Property.build("Start date", navigationPoints.get(0).getTime().toString()));
			properties.add(
					Property.build("End date", navigationPoints.get(navigationPoints.size() - 1).getTime().toString()));
		}
		if (!parsingErrors.isEmpty())
			properties.add(Property.build("Parsing errors", parsingErrors.size()));
		return properties;
	}

	/**
	 * @return the {@link #markers}
	 */
	public List<NavigationPointCsv> getNavigationPoints() {
		return navigationPoints;
	}

	/**
	 * Sets the navigation point list (and update statistics).
	 */
	private void computeBoundingBox() {
		GeoBoxBuilder geoBoxBuilder = new GeoBoxBuilder();
		navigationPoints.forEach(point -> geoBoxBuilder.addPoint(point.getLongitude(), point.getLatitude()));
		geoBox = geoBoxBuilder.build();
	}

	@Override
	public INavigationData getNavigationData(boolean readonly) throws GIOException {
		return this;
	}

	@Override
	public int size() {
		return navigationPoints.size();
	}

	@Override
	public String getFileName() {
		return getFilename(); // TODO: get only one "getFilename" method
	}

	@Override
	public double getLatitude(int index) {
		return navigationPoints.get(index).getLatitude();
	}

	@Override
	public double getLongitude(int index) {
		return navigationPoints.get(index).getLongitude();
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	@Override
	public List<CsvException> getParsingErrors() {
		return parsingErrors;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		if (!navigationPoints.isEmpty()) {
			Instant start = navigationPoints.get(0).getTime();
			Instant end = navigationPoints.get(navigationPoints.size() - 1).getTime();
			return Optional.of(Pair.of(start, end));
		}
		return super.getStartEndDate();
	}
}
