package fr.ifremer.globe.core.io.nvi.datacontainer;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;

/** Class generated by PredefinedLayersGenerator **/
public class NavigationLayers {

	private NavigationLayers() {}

	public static final String GROUP = "/Navigation";

	/** Layer: COURSE_OVER_GROUND **/
	public static final String COURSE_OVER_GROUND_VARIABLE_NAME ="course_over_ground";
	public static final PredefinedLayers<FloatLoadableLayer1D> COURSE_OVER_GROUND = new PredefinedLayers<>(GROUP,"course_over_ground","course","degree", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: HEADING **/
	public static final String HEADING_VARIABLE_NAME ="heading";
	public static final PredefinedLayers<FloatLoadableLayer1D> HEADING = new PredefinedLayers<>(GROUP,"heading","heading(true)","degree", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: HEADING_RATE **/
	public static final String HEADING_RATE_VARIABLE_NAME ="heading_rate";
	public static final PredefinedLayers<FloatLoadableLayer1D> HEADING_RATE = new PredefinedLayers<>(GROUP,"heading_rate","heading rate","degree/s", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: HEIGHT_ABOVE_REFERENCE_ELLIPSOID **/
	public static final String HEIGHT_ABOVE_REFERENCE_ELLIPSOID_VARIABLE_NAME ="height_above_reference_ellipsoid";
	public static final PredefinedLayers<FloatLoadableLayer1D> HEIGHT_ABOVE_REFERENCE_ELLIPSOID = new PredefinedLayers<>(GROUP,"height_above_reference_ellipsoid","height above reference ellipsoid","m", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: LATITUDE **/
	public static final String LATITUDE_VARIABLE_NAME ="latitude";
	public static final PredefinedLayers<DoubleLoadableLayer1D> LATITUDE = new PredefinedLayers<>(GROUP,"latitude","latitude","degrees_north", DataKind.continuous, DoubleLoadableLayer1D::new);

	/** Layer: LONGITUDE **/
	public static final String LONGITUDE_VARIABLE_NAME ="longitude";
	public static final PredefinedLayers<DoubleLoadableLayer1D> LONGITUDE = new PredefinedLayers<>(GROUP,"longitude","longitude","degrees_east", DataKind.continuous, DoubleLoadableLayer1D::new);

	/** Layer: PITCH **/
	public static final String PITCH_VARIABLE_NAME ="pitch";
	public static final PredefinedLayers<FloatLoadableLayer1D> PITCH = new PredefinedLayers<>(GROUP,"pitch","pitch","arc_degree", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: PITCH_RATE **/
	public static final String PITCH_RATE_VARIABLE_NAME ="pitch_rate";
	public static final PredefinedLayers<FloatLoadableLayer1D> PITCH_RATE = new PredefinedLayers<>(GROUP,"pitch_rate","pitch rate","degree/s", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: POSITION_QUALITY_INDICATOR **/
	public static final String POSITION_QUALITY_INDICATOR_VARIABLE_NAME ="position_quality_indicator";
	public static final PredefinedLayers<ByteLoadableLayer1D> POSITION_QUALITY_INDICATOR = new PredefinedLayers<>(GROUP,"position_quality_indicator","Sensor quality indicator","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: QUALITY_FLAG **/
	public static final String QUALITY_FLAG_VARIABLE_NAME ="quality_flag";
	public static final PredefinedLayers<ByteLoadableLayer1D> QUALITY_FLAG = new PredefinedLayers<>(GROUP,"quality_flag","quality flag","", DataKind.continuous, ByteLoadableLayer1D::new);

	/** Layer: ROLL **/
	public static final String ROLL_VARIABLE_NAME ="roll";
	public static final PredefinedLayers<FloatLoadableLayer1D> ROLL = new PredefinedLayers<>(GROUP,"roll","roll","arc_degree", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: ROLL_RATE **/
	public static final String ROLL_RATE_VARIABLE_NAME ="roll_rate";
	public static final PredefinedLayers<FloatLoadableLayer1D> ROLL_RATE = new PredefinedLayers<>(GROUP,"roll_rate","roll rate","degree/s", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: SPEED_OVER_GROUND **/
	public static final String SPEED_OVER_GROUND_VARIABLE_NAME ="speed_over_ground";
	public static final PredefinedLayers<FloatLoadableLayer1D> SPEED_OVER_GROUND = new PredefinedLayers<>(GROUP,"speed_over_ground","speed over ground","m/s", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: SPEED_RELATIVE **/
	public static final String SPEED_RELATIVE_VARIABLE_NAME ="speed_relative";
	public static final PredefinedLayers<FloatLoadableLayer1D> SPEED_RELATIVE = new PredefinedLayers<>(GROUP,"speed_relative","speed relative to water","m/s", DataKind.continuous, FloatLoadableLayer1D::new);

	/** Layer: TIME **/
	public static final String TIME_VARIABLE_NAME ="time";
	public static final PredefinedLayers<LongLoadableLayer1D> TIME = new PredefinedLayers<>(GROUP,"time","Time-stamp of each ping","nanoseconds since 1970-01-01 00:00:00Z", DataKind.continuous, LongLoadableLayer1D::new);

	/** Layer: VERTICAL_OFFSET **/
	public static final String VERTICAL_OFFSET_VARIABLE_NAME ="vertical_offset";
	public static final PredefinedLayers<FloatLoadableLayer1D> VERTICAL_OFFSET = new PredefinedLayers<>(GROUP,"vertical_offset","vertical offset from nominal","m", DataKind.continuous, FloatLoadableLayer1D::new);
}
