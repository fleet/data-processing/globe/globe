/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.nvi.csv;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import fr.ifremer.globe.core.io.csv.parsing.CsvTimestampedBean;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Navigation point built from CSV file.
 */
public class NavigationPointCsv extends CsvTimestampedBean {

	public static final String COL_LAT = "latitude";
	public static final String COL_LON = "longitude";
	public static final String COL_ELEVATION = "elevation";
	public static final String COL_HEADING = "heading";

	public static final List<String> COLUMNS = List.of(COL_LAT, COL_LON, COL_ELEVATION, COL_HEADING);

	private Instant instant;
	private double latitude;
	private double longitude;
	private double elevation = Double.NaN;
	private double elevationScaleFactor = 1d;
	private double heading = Double.NaN;

	public void parseDateTime() throws DateTimeException, GIOException {
		this.instant = getTemporal()//
				.filter(LocalDateTime.class::isInstance).map(LocalDateTime.class::cast) //
				.map(dateTime -> dateTime.toInstant(ZoneOffset.UTC)) //
				.orElseThrow(() -> new GIOException("Date and time are not available"));
	}

	public Instant getTime() {
		return instant;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getElevation() {
		return elevationScaleFactor * elevation;
	}

	public double getHeading() {
		return heading;
	}

	public void setImmersionScaleFactor(double elevationScaleFactor) {
		this.elevationScaleFactor = elevationScaleFactor;
	}

	@Override
	public String toString() {
		return "NavigationPointCsv [instant=" + instant + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", elevation=" + elevation + ", elevationScaleFactor=" + elevationScaleFactor + ", heading=" + heading
				+ "]";
	}

}
