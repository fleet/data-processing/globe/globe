package fr.ifremer.globe.core.io.nvi.datacontainer;

import java.util.List;

import fr.ifremer.globe.core.io.nvi.info.NviLegacyInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NviLegacySensorType;
import fr.ifremer.globe.core.model.navigation.utils.NavigationStatisticComputer;
import fr.ifremer.globe.core.model.quality.MeasurandQualifierFlags;
import fr.ifremer.globe.core.model.quality.NviLegacyQualifierFlags;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link INavigationData} for NVI : provides access to navigation data.
 */
public class NviLegacyNavigation implements INavigationData, IDataContainerOwner {

	protected final IDataContainerToken<NviLegacyInfo> dataContainerToken;
	protected final DataContainer<NviLegacyInfo> dataContainer;
	private final NviLegacyInfo info;
	private boolean isReadOnly = true;

	/** layers **/
	private final IntLoadableLayer1D dateLayer;
	private final IntLoadableLayer1D timeLayer;
	private final DoubleLoadableLayer1D latitudeLayer;
	private final DoubleLoadableLayer1D longitudeLayer;
	private final FloatLoadableLayer1D headingLayer;
	private final FloatLoadableLayer1D altitudeLayer;
	private final FloatLoadableLayer1D immersionLayer;
	private final FloatLoadableLayer1D speedLayer;
	private final ByteLoadableLayer1D validityFlagLayer;
	private final ByteLoadableLayer1D typeLayer;
	private final ByteLoadableLayer1D qualityLayer;

	/** Metadata **/
	private final List<NavigationMetadata<?>> metadata = List.of(
			NavigationMetadataType.TIME.withValueSupplier(this::getTime2),
			NavigationMetadataType.HEADING.withValueSupplier(this::getHeading),
			NavigationMetadataType.SPEED.withValueSupplier(this::getSpeed),
			NavigationMetadataType.SENSOR_TYPE.withValueSupplier(this::getSensorType),
			NavigationMetadataType.ELEVATION.withValueSupplier(this::getElevation),
			NavigationMetadataType.ALTITUDE.withValueSupplier(this::getAltitude),
			NavigationMetadataType.QUALITY.withValueSupplier(this::getQuality));

	/** Constructor **/
	public NviLegacyNavigation(NviLegacyInfo info, boolean readonly) throws GIOException {
		this.info = info;
		this.isReadOnly = readonly;
		dataContainerToken = IDataContainerFactory.grab().book(info, this);
		dataContainer = dataContainerToken.getDataContainer();

		// Get required layers
		dateLayer = dataContainer.getLayer(NviLayers.DATE);
		timeLayer = dataContainer.getLayer(NviLayers.TIME);
		latitudeLayer = dataContainer.getLayer(NviLayers.LATITUDE);
		longitudeLayer = dataContainer.getLayer(NviLayers.LONGITUDE);
		headingLayer = dataContainer.getLayer(NviLayers.HEADING);
		immersionLayer = dataContainer.getLayer(NviLayers.IMMERSION);
		altitudeLayer = dataContainer.getLayer(NviLayers.ALTITUDE);
		speedLayer = dataContainer.getLayer(NviLayers.SPEED);
		validityFlagLayer = dataContainer.getLayer(NviLayers.VALIDITY_FLAG);
		typeLayer = dataContainer.getLayer(NviLayers.P_TYPE);
		qualityLayer = dataContainer.getLayer(NviLayers.P_QUALITY);
	}

	@Override
	public boolean isReadOnly() {
		return isReadOnly;
	}

	/** Close method **/
	@Override
	public void close() {
		if (dataContainerToken != null)
			dataContainerToken.close();
	}

	/**
	 * Flush
	 */
	public void flush() throws GIOException {
		dataContainer.flush();
		updateStatistics();
	}

	/** Updates the statistics and write the result in the NVI file */
	public void updateStatistics() throws GIOException {
		var stats = NavigationStatisticComputer.compute(this);
		info.setPositionNbr(stats.size());
		info.setGeoBox(new GeoBox(stats.north(), stats.south(), stats.east(), stats.west()));
		info.setStartDate(stats.startTime());
		info.setEndDate(stats.endTime());
		info.flush();
	}

	@Override
	public int size() {
		return (int) latitudeLayer.getDimensions()[0];
	}

	//// GETTERS & SETTERS

	@Override
	public String getFileName() {
		return dataContainer.getInfo().getPath();
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return dataContainer.getInfo().getContentType();
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return latitudeLayer.get(index);
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return longitudeLayer.get(index);
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	private long getTime2(int index) {
		return DateUtils.getEpochMilliFromJulian(dateLayer.get(index), timeLayer.get(index));
	}

	private float getHeading(int index) {
		return headingLayer.get(index);
	}

	private float getElevation(int index) {
		return -immersionLayer.get(index);
	}

	private float getAltitude(int index) {
		return altitudeLayer.get(index);
	}

	private float getSpeed(int index) {
		return speedLayer.get(index);
	}

	private int getQuality(int index) {
		return qualityLayer.get(index);
	}

	private INavigationSensorType getSensorType(int index) {
		return NviLegacySensorType.get(typeLayer.get(index));
	}

	@Override
	public MeasurandQualifierFlags getQualifierFlag(int index) {
		return NviLegacyQualifierFlags.get(validityFlagLayer.get(index)).toMeasurandQualifierFlag();
	}

	@Override
	public void setQualifierFlag(int index, MeasurandQualifierFlags flag) {
		validityFlagLayer.set(index, NviLegacyQualifierFlags.get(flag).getValue());
	}

}
