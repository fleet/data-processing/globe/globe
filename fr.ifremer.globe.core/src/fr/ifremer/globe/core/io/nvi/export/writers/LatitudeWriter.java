package fr.ifremer.globe.core.io.nvi.export.writers;

import java.nio.ByteBuffer;

import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class LatitudeWriter extends SimpleVariableWriter {

	/** Properties **/
	private double max = Double.NaN;
	private double min = Double.NaN;

	/** Constructor **/
	public LatitudeWriter() {
		super(NviConstants.NAME_LATITUDE, NviConstants.DATA_TYPE_LATITUDE, DataType.DOUBLE);
	}

	@Override
	public void define(NetcdfFile file, NCDimension dim) throws NCException {
		super.define(file, dim);
		var.addAttribute(NviConstants.ATTRIBUTE_TYPE, NviConstants.TYPE_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_LONG_NAME, NviConstants.LONG_NAME_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_NAME_CODE, NviConstants.NAME_CODE_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_UNITS, NviConstants.UNITS_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_UNIT_CODE, NviConstants.UNIT_CODE_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_ADD_OFFSET, NviConstants.ADD_OFFSET_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_SCALE_FACTOR, NviConstants.SCALE_FACTOR_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MIN, NviConstants.MINIMUM_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MAX, NviConstants.MAXIMUM_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MIN, NviConstants.VALID_MINIMUM_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_VALID_MAX, NviConstants.VALID_MAXIMUM_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_MISSING_VALUE, NviConstants.MISSING_VALUE_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_FORMAT_C, NviConstants.FORMAT_C_LATITUDE);
		var.addAttribute(NviConstants.ATTRIBUTE_ORIENTATION, NviConstants.ORIENTATION_LATITUDE);
	}

	@Override
	protected void fillBufferToWrite(ByteBuffer buffer, INavigationData dataProxy, int index) throws GIOException {
		double value = dataProxy.getLatitude(index);
		if (!Double.isNaN(value)){ //TODO ? && dataProxy.getValidity(index) != NviConstants.INVALIDITY_FLAG_VALUE) {
			max = Double.isNaN(max) ? value : Math.max(max, value);
			min = Double.isNaN(min) ? value : Math.min(min, value);
		}
		buffer.putDouble(value);
	}

	@Override
	public void writeGlobalAttributes(NetcdfFile file) throws NCException {
		file.addAttribute(NviConstants.NorthLatitude, max);
		file.addAttribute(NviConstants.SouthLatitude, min);
	}
}
