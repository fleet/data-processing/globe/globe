package fr.ifremer.globe.core.io.nvi.csv;

import fr.ifremer.globe.core.io.csv.parsing.CsvTimestampedMappingStrategy;
import fr.ifremer.globe.core.model.file.FileInfoSettings;

/**
 * Strategy of mapping where column's position is map to NavigationPointCsv field
 */
public class NavigationMappingStrategy extends CsvTimestampedMappingStrategy<NavigationPointCsv> {

	/**
	 * Constructor
	 */
	public NavigationMappingStrategy(FileInfoSettings settings) {
		super(NavigationPointCsv.COLUMNS, settings);
		setType(NavigationPointCsv.class);
	}
}
