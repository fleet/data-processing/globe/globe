package fr.ifremer.globe.core.io.nvi.service.impl;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.nvi.datacontainer.NviLegacyNavigation;
import fr.ifremer.globe.core.io.nvi.datacontainer.NviV2Navigation;
import fr.ifremer.globe.core.io.nvi.export.NviExporter;
import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * This class provides methods to generate navigation file (.nvi)
 */
@Component(name = "globe_drivers_nvi_writer", service = INviWriter.class)
public class NviWriter implements INviWriter {

	/**
	 * Returns the service implementation of INviWriter.
	 */
	static INviWriter grab() {
		return OsgiUtils.getService(INviWriter.class);
	}

	/** {@inheritDoc} */
	@Override
	public void flush(INavigationData navigationData) throws GIOException {
		if (navigationData instanceof NviLegacyNavigation legacyNavigation) {
			legacyNavigation.flush();
		} else if (navigationData instanceof NviV2Navigation v2Navigation) {
			v2Navigation.flush();
		}
	}

	/** {@inheritDoc} */
	@Override
	public void write(INavigationData navigationData, String outputFilePath) throws GIOException {
		NviExporter.export(navigationData, outputFilePath);
	}

	/** {@inheritDoc} */
	@Override
	public void write(INavigationDataSupplier source, String outputFilePath, SamplingMode sMode, int sValue)
			throws GIOException {
		NviExporter.export(source, outputFilePath, sMode, sValue);
	}

	/** {@inheritDoc} */
	@Override
	public void write(INavigationData navigationData, String outputFilePath, List<Integer> indexesToExport)
			throws GIOException {
		NviExporter.export(navigationData, outputFilePath, indexesToExport);
	}
}
