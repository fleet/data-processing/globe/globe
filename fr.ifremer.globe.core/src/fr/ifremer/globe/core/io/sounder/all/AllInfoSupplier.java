/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.sounder.all;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;

/**
 * Service providing informations on Simrad all files
 */
@Component(name = "globe_drivers_all_file_info_supplier", service = { IFileInfoSupplier.class })
public class AllInfoSupplier implements IFileInfoSupplier<AllInfo> {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(AllInfoSupplier.class);

	/** Extensions **/
	public static final String EXTENSION = "all";

	@Override
	public Optional<AllInfo> getFileInfo(String filePath) {
		if (filePath.endsWith("." + EXTENSION)) {
			try {
				return Optional.of(new AllInfo(filePath));
			} catch (Exception e) {
				LOGGER.error("Error while opening file {} : {}", filePath, e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(AllInfo.CONTENT_TYPE);
	}

	@Override
	public List<String> getExtensions() {
		return List.of(EXTENSION);
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Arrays.asList(new Pair<>("Simrad file (*." + EXTENSION + ")", "*." + EXTENSION));
	}

}