package fr.ifremer.globe.core.io.sounder.s7k;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1003;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1015;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7K1003InvidivualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.S7KSensorType;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.units.Units;

/**
 * This class provides navigation data for an S7K file.
 */
public class S7kNavigation implements INavigationData {

	private final S7kInfo fileInfo;
	private final S7KFile s7kFile;
	private final List<DatagramPosition> navigationDatagramPointers = new ArrayList<>();
	private final DatagramBuffer navigationDatagramBuffer = new DatagramBuffer();
	private final List<DatagramPosition> positionDatagramPointers = new ArrayList<>();
	private final DatagramBuffer positionDatagramBuffer = new DatagramBuffer();
	private int currentNavigationDatagramIndex = -1;
	private boolean navigationInSyncWithPosition = false;

	/** Metadata **/
	private final List<NavigationMetadata<?>> metadata = List.of(
			NavigationMetadataType.TIME.withValueSupplier(this::getTime2),
			NavigationMetadataType.ELEVATION.withValueSupplier(this::getElevation),
			NavigationMetadataType.HEADING.withValueSupplier(this::getHeading),
			NavigationMetadataType.SPEED.withValueSupplier(this::getSpeed),
			NavigationMetadataType.SENSOR_TYPE.withValueSupplier(this::getSensorType));

	/**
	 * Constructor
	 */
	public S7kNavigation(S7kInfo fileInfo) throws GIOException {
		this.fileInfo = fileInfo;
		try {
			s7kFile = new S7KFile(fileInfo.getPath());
			// Get navigation values
			getNavigationDatagramPointers(s7kFile);
			// Get position values
			getPositionDatagramPointers(s7kFile);
			navigationInSyncWithPosition = (navigationDatagramPointers.size() == positionDatagramPointers.size());
		} catch (IOException e) {
			throw new GIOException(e.getMessage(), e);
		}
	}

	/** Close method **/
	@Override
	public void close() throws IOException {
		s7kFile.close();
	}

	/**
	 * @return pointers on navigation datagram. TODO sort datagram by time ?
	 */
	private void getNavigationDatagramPointers(S7KFile fileMetaData) {
		for (Entry<Long, S7KIndividualSensorMetadata> sensorMetadata : fileMetaData.get1015Metadata().getEntries()) {
			for (Entry<Long, DatagramPosition> posentry : sensorMetadata.getValue().getDatagrams()) {
				navigationDatagramPointers.add(posentry.getValue());
			}
			// retrieve positions from first sensor only
			if (!navigationDatagramPointers.isEmpty())
				break;
		}
	}

	/**
	 * @return pointers on position datagram
	 */
	private void getPositionDatagramPointers(S7KFile fileMetaData) {
		// get
		for (Entry<Long, S7K1003InvidivualSensorMetadata> sensorMetadata : fileMetaData.get1003Metadata()
				.getEntries()) {
			for (Entry<Long, DatagramPosition> posentry : sensorMetadata.getValue().getDatagrams()) {
				positionDatagramPointers.add(posentry.getValue());
			}
			// retrieve positions from first sensor only
			if (!positionDatagramPointers.isEmpty())
				break;
		}
	}

	/**
	 * Reads, if necessary, the position datagram at the specified index.
	 */
	private void readNavigationDatagram(int index) throws GIOException {
		try {
			if (currentNavigationDatagramIndex != index) {
				DatagramPosition pointer = navigationDatagramPointers.get(index);
				DatagramReader.readDatagram(pointer.getFile(), navigationDatagramBuffer, pointer.getSeek());
				if (navigationInSyncWithPosition) {
					pointer = positionDatagramPointers.get(index);
					DatagramReader.readDatagram(pointer.getFile(), positionDatagramBuffer, pointer.getSeek());
				}
				currentNavigationDatagramIndex = index;
			}
		} catch (IOException e) {
			throw new GIOException("e", e);
		}
	}

	@Override
	public int size() {
		return navigationDatagramPointers.size();
	}

	@Override
	public String getFileName() {
		return fileInfo.getPath();
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return fileInfo.getContentType();
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		readNavigationDatagram(index);
		return Math.toDegrees(S7K1015.getLatitude(navigationDatagramBuffer).get());
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		readNavigationDatagram(index);
		return Math.toDegrees(S7K1015.getLongitude(navigationDatagramBuffer).get());
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	private long getTime2(int index) throws GIOException {
		readNavigationDatagram(index);
		return S7K1015.getTimeMilli(navigationDatagramBuffer);
	}

	private float getHeading(int index) throws GIOException {
		readNavigationDatagram(index);
		return (float) Math.toDegrees(S7K1015.getHeading(navigationDatagramBuffer).get());
	}

	private float getElevation(int index) throws GIOException {
		readNavigationDatagram(index);
		return S7K1015.getVesselHeight(navigationDatagramBuffer).get();
	}

	private float getSpeed(int index) throws GIOException {
		readNavigationDatagram(index);
		return Units.metterSecondsToKnots(S7K1015.getSpeedOverGround(navigationDatagramBuffer).get());
	}

	private INavigationSensorType getSensorType(int index) throws GIOException {
		if (navigationInSyncWithPosition) {
			readNavigationDatagram(index);
			return S7KSensorType.get(S7K1003.getPositioningMethod(positionDatagramBuffer).getByte());
		}
		return S7KSensorType.NOT_DEFINED;
	}
}
