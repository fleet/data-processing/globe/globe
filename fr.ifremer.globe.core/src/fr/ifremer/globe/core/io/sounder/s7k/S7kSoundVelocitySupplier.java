package fr.ifremer.globe.core.io.sounder.s7k;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1010;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7K1010.CTDSample;
import fr.ifremer.globe.api.xsf.converter.s7k.datagram.S7KBaseDatagram;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KIndividualSensorMetadata;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.utils.NavigationInterpolator;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.core.model.velocity.ISoundVelocitySupplier;
import fr.ifremer.globe.core.model.velocity.SoundVelocity;
import fr.ifremer.globe.core.model.velocity.SoundVelocityProfile;
import fr.ifremer.globe.utils.algo.VelocityAlgorithm;
import fr.ifremer.globe.utils.exception.GIOException;

@Component(name = "globe_drivers_s7k_soundvelocity_supplier", service = { ISoundVelocitySupplier.class })
public class S7kSoundVelocitySupplier implements ISoundVelocitySupplier {

	/** Logger **/
	private static final Logger logger = LoggerFactory.getLogger(S7kSoundVelocitySupplier.class);

	public Optional<ISoundVelocityData> getSoundVelocityData(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof S7kInfo ? new ISoundVelocityData() {

			private List<ISoundVelocityProfile> profiles;

			@Override
			public String getOriginFileName() {
				return fileInfo.getAbsolutePath();
			}

			@Override
			public List<ISoundVelocityProfile> getSoundVelocityProfiles() throws GIOException {
				if (null == profiles) {
					profiles = readSoundSpeedProfiles((S7kInfo) fileInfo);
				}
				return profiles;
			}

			/**
			 * Reads {@link ISoundVelocityProfile} from the specified {@link S7kInfo}.
			 * 
			 * @param s7kInfo : input file (.s7k)
			 * 
			 * @return a list of {@link SoundVelocityProfile}.
			 * @throws UnsupportedSounderException
			 */
			private List<ISoundVelocityProfile> readSoundSpeedProfiles(S7kInfo s7kInfo) throws GIOException {
				logger.debug("Extraction of sound speed profiles from {}... ", s7kInfo.getFilename());
				List<ISoundVelocityProfile> soundVelocityProfiles = new ArrayList<>();
				DatagramBuffer datagramBuffer = new DatagramBuffer();

				// Open nav & file
				try (S7kNavigation nav = new S7kNavigation(s7kInfo); //
						S7KFile s7kFile = new S7KFile(s7kInfo.getAbsolutePath())) {

					NavigationInterpolator navigationInterpolator = new NavigationInterpolator(nav);

					for (Entry<Long, S7KIndividualSensorMetadata> sensorMetadata : s7kFile.get1010Metadata()
							.getEntries()) {
						for (Entry<Long, DatagramPosition> posentry : sensorMetadata.getValue().getDatagrams()) {
							DatagramPosition pointer = posentry.getValue();
							DatagramReader.readDatagram(pointer.getFile(), datagramBuffer, pointer.getSeek());

							// metadata (serial number, counter, date...)
							Date date = new Date(S7KBaseDatagram.getTimeMilli(datagramBuffer));

							// lat/lon are not actually provided by S7k1010 (always = 0), so they are computed from
							// navigation
							double lat = navigationInterpolator.getLatitude(date.getTime());
							double lon = navigationInterpolator.getLongitude(date.getTime());

							boolean isDepth = S7K1010.getPressureFlag(datagramBuffer).getU() == 1;
							boolean isSalinity = S7K1010.getConductivityFlag(datagramBuffer).getU() == 1;

							// get sound speed values
							List<SoundVelocity> soundSpeedValues = new ArrayList<>();
							for (CTDSample cTDSample : S7K1010.getSmples(datagramBuffer)) {

								double depth = isDepth ? cTDSample.pressure_depth : //
								// convert pascal to depth
								VelocityAlgorithm.calculDepthFromPressure(0.0001d * cTDSample.pressure_depth, lat);

								double salinity = isSalinity ? cTDSample.condictivity_salinity : Double.NaN;

								soundSpeedValues.add(new SoundVelocity(cTDSample.sound_velocity, depth,
										cTDSample.water_temperature, salinity, cTDSample.absorption));
							}

							SoundVelocityProfile profile = new SoundVelocityProfile(date, 0, 0, soundSpeedValues);
							profile.setLat(lat);
							profile.setLon(lon);
							profile.setSensortype("Datagram 1010 (.s7k file) : CTD");
							soundVelocityProfiles.add(profile);
						}
					}

				} catch (IOException e) {
					throw new GIOException(e.getMessage(), e);
				}

				logger.debug(" {} sound profile(s) extracted.", soundVelocityProfiles.size());
				return soundVelocityProfiles;
			}

		} : null);
	}

}
