package fr.ifremer.globe.core.io.sounder.kmall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.number.NumberUtils;

public class KmallInfo implements IFileInfo {

	/** Content type **/
	public final static ContentType CONTENT_TYPE = ContentType.SOUNDER_KMALL;

	/** File path **/
	private final String filePath;

	/** Computed {@link GeoBox} **/
	private GeoBox geoBox;

	/** Properties **/
	private Date minDate;
	private Date maxDate;
	private double depthMin;
	private double depthMax;
	private String modelNumber;
	private int rxAntennaCount;
	private int txAntennaCount;
	private int swathCount;
	private int detectionCount;
	private int wcBeamCount;
	private long maxSeabedSampleCount;

	/**
	 * Constructor
	 * 
	 * @throws IOException
	 */
	public KmallInfo(String filePath) throws IOException {
		this.filePath = filePath;
		try (KmallFile fileMetaData = new KmallFile(filePath)) {
			modelNumber = fileMetaData.getModelNumber();
			geoBox = new GeoBox(fileMetaData.getLatMax(), fileMetaData.getLatMin(), fileMetaData.getLonMax(),
					fileMetaData.getLonMin());
			minDate = new Date(fileMetaData.getMinDate());
			maxDate = new Date(fileMetaData.getMaxDate());
			depthMin = fileMetaData.getDepthMin();
			depthMax = fileMetaData.getDepthMax();
			rxAntennaCount = fileMetaData.getRxAntennaNb();
			txAntennaCount = fileMetaData.getTxAntennaNb();
			swathCount = fileMetaData.getSwathCount();
			detectionCount = fileMetaData.getDetectionCount();
			wcBeamCount = fileMetaData.getWcBeamCount();
			maxSeabedSampleCount = fileMetaData.getMaxSeabedSampleCount();
		}
	}

	/**
	 * Provides properties displayed in "Properties" view.
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new ArrayList<>();
		result.add(Property.build("Model", modelNumber));
		result.add(Property.build("Date start", DateUtils.getStringDate(minDate)));
		result.add(Property.build("Date end", DateUtils.getStringDate(maxDate)));
		result.addAll(Property.build(geoBox));
		result.add(Property.build("Minimum depth", NumberUtils.getStringMetersDouble(depthMin) + " m"));
		result.add(Property.build("Maximum depth", NumberUtils.getStringMetersDouble(depthMax) + " m"));
		result.add(Property.build("Rx antenna", rxAntennaCount));
		result.add(Property.build("Tx antenna", txAntennaCount));
		result.add(Property.build("Swath count", swathCount));
		result.add(Property.build("Detection count", detectionCount));
		result.add(Property.build("Water column beam count", wcBeamCount));
		result.add(Property.build("Max seabed samples count", (int) maxSeabedSampleCount));
		return result;
	}

	//// GETTERS & SETTERS

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public ContentType getContentType() {
		return CONTENT_TYPE;
	}

	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	public void setGeoBox(GeoBox geobox) {
		this.geoBox = geobox;
	}

}
