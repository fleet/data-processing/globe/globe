/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.sounder.s7k;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;

/**
 * Service providing informations on s7k files
 */
@Component(name = "globe_drivers_s7k_file_info_supplier", service = { IFileInfoSupplier.class })
public class S7kInfoSupplier implements IFileInfoSupplier<S7kInfo> {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(S7kInfoSupplier.class);

	/** Extensions **/
	public static final String EXTENSION = "s7k";

	@Override
	public Optional<S7kInfo> getFileInfo(String filePath) {
		if (filePath.endsWith(EXTENSION)) {
			try {
				return Optional.of(new S7kInfo(filePath));
			} catch (Exception e) {
				LOGGER.error("Error while opening file {} : {}", filePath, e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(S7kInfo.CONTENT_TYPE);
	}

	@Override
	public List<String> getExtensions() {
		return List.of(EXTENSION);
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Arrays.asList(new Pair<>("Reason file (*." + EXTENSION + ")", "*." + EXTENSION));
	}

}