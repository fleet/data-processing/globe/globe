package fr.ifremer.globe.core.io.sounder.all;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.utils.date.DateUtils;

public class AllInfo implements IFileInfo {
	
	/** Content type **/
	public final static ContentType CONTENT_TYPE = ContentType.SOUNDER_ALL;

	/** File path **/
	private final String filePath;

	/** Computed {@link GeoBox} **/
	private GeoBox geoBox;

	/** Dates **/
	private Date minDate;
	private Date maxDate;

	/**
	 * Constructor
	 * @throws UnsupportedSounderException 
	 */
	public AllInfo(String filePath) throws IOException, UnsupportedSounderException {
		this.filePath = filePath;
		try (AllFile fileMetaData = new AllFile(filePath)) {
			double north = fileMetaData.getPosition().getLatMax();
			double south = fileMetaData.getPosition().getLatMin();
			double east = fileMetaData.getPosition().getLonMax();
			double west = fileMetaData.getPosition().getLonMin();
			geoBox = new GeoBox(north, south, east, west);
			minDate = new Date(fileMetaData.getMinDate());
			maxDate = new Date(fileMetaData.getMaxDate());
		}
	}

	/**
	 * Provides properties displayed in "Properties" view.
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new ArrayList<>();
		result.add(Property.build("Date start", DateUtils.getStringDate(minDate)));
		result.add(Property.build("Date end", DateUtils.getStringDate(maxDate)));
		result.addAll(Property.build(geoBox));
		return result;
	}

	//// GETTERS & SETTERS

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public ContentType getContentType() {
		return CONTENT_TYPE; // useful? Can't be retrieved from info Supplier ?
	}

	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	public void setGeoBox(GeoBox geobox) {
		this.geoBox = geobox;
	}

}
