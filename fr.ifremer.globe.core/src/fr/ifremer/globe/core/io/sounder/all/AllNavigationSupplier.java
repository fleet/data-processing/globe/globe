package fr.ifremer.globe.core.io.sounder.all;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;

@Component(name = "globe_drivers_all_navigation_supplier", service = { INavigationSupplier.class })
public class AllNavigationSupplier implements INavigationSupplier {

	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof AllInfo ? accessMode -> new AllNavigation((AllInfo) fileInfo) : null);
	}

}
