package fr.ifremer.globe.core.io.sounder.all;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.position.Position;
import fr.ifremer.globe.api.xsf.converter.common.datagram.BaseDatagram;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.units.Units;

/**
 * This class provides navigation data for a SIMRAD file (.all).
 */
public class AllNavigation implements INavigationData {

	private final AllInfo fileInfo;
	private final AllFile allFile;
	private final List<DatagramPosition> positionDatagramPointers;
	private final DatagramBuffer datagramBuffer;
	private int currentPositionDatagramIndex = -1;

	/** Metadata **/
	private final List<NavigationMetadata<?>> metadata = List.of(
			NavigationMetadataType.TIME.withValueSupplier(this::getTime2),
			NavigationMetadataType.HEADING.withValueSupplier(this::getHeading),
			NavigationMetadataType.SPEED.withValueSupplier(this::getSpeed),
			NavigationMetadataType.SENSOR_TYPE.withValueSupplier(this::getSensorType));

	/**
	 * Constructor
	 */
	public AllNavigation(AllInfo fileInfo) throws GIOException {
		this.fileInfo = fileInfo;
		try {
			allFile = new AllFile(fileInfo.getPath());
			datagramBuffer = new DatagramBuffer(allFile.getByteOrder());
			positionDatagramPointers = getPositionDatagramPointers(allFile);
		} catch (IOException | UnsupportedSounderException e) {
			throw new GIOException(e.getMessage(), e);
		}
	}

	@Override
	public void close() throws IOException {
		allFile.close();
	}

	/**
	 * @return pointers on position datagram.
	 */
	private List<DatagramPosition> getPositionDatagramPointers(AllFile fileMetaData) throws IOException {
		// get
		TreeMap<Long, DatagramPosition> positionDatagramPointersMap = new TreeMap<>();

		for (IndividualSensorMetadata sensorMetadata : fileMetaData.getPosition().getSensors().values()) {
			for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
				final DatagramPosition pos = posentry.getValue();
				DatagramReader.readDatagramAt(pos.getFile(), datagramBuffer, pos.getSeek());
				positionDatagramPointersMap.put(BaseDatagram.getEpochTime(datagramBuffer.byteBufferData), pos);
			}
			//retrieve positions from first sensor only
			if(!positionDatagramPointersMap.isEmpty()) break;
		}
		return new ArrayList<>(positionDatagramPointersMap.values());
	}

	/**
	 * Reads the position datagram for the specified index.
	 */
	private void readPositionDatagram(int index) throws GIOException {
		try {
			if (currentPositionDatagramIndex != index) {
				DatagramPosition pointer = positionDatagramPointers.get(index);
				DatagramReader.readDatagramAt(pointer.getFile(), datagramBuffer, pointer.getSeek());
				currentPositionDatagramIndex = index;
			}
		} catch (IOException e) {
			throw new GIOException(e.getMessage(), e);
		}
	}

	@Override
	public int size() {
		return positionDatagramPointers.size();
	}

	@Override
	public String getFileName() {
		return fileInfo.getPath();
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return fileInfo.getContentType();
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		readPositionDatagram(index);
		return Position.getLatitude(datagramBuffer.byteBufferData).get() / 20000000.;
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		readPositionDatagram(index);
		return Position.getLongitude(datagramBuffer.byteBufferData).get() / 10000000.;
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	private long getTime2(int index) throws GIOException {
		readPositionDatagram(index);
		return Position.getEpochTime(datagramBuffer.byteBufferData);
	}

	private INavigationSensorType getSensorType(int index) throws GIOException {
		readPositionDatagram(index);
		return NmeaSensorType.get(Position.getSensorQualityIndicator(datagramBuffer.byteBufferData).get());
	}

	private float getHeading(int index) throws GIOException {
		readPositionDatagram(index);
		return (float) (Position.getCourseOverGround(datagramBuffer.byteBufferData).getU() / 100d);
	}

	private float getSpeed(int index) throws GIOException {
		readPositionDatagram(index);
		return Units.metterSecondsToKnots(Position.getSpeedOverGround(datagramBuffer.byteBufferData).getU() * 0.01f);
	}

}
