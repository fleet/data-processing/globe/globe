package fr.ifremer.globe.core.io.sounder.all;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.all.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.all.datagram.metadata.AllFile;
import fr.ifremer.globe.api.xsf.converter.all.datagram.soundspeedprofile.SoundSpeedProfile;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.common.exceptions.UnsupportedSounderException;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.core.model.velocity.ISoundVelocitySupplier;
import fr.ifremer.globe.core.model.velocity.SoundVelocity;
import fr.ifremer.globe.core.model.velocity.SoundVelocityProfile;
import fr.ifremer.globe.utils.exception.GIOException;

@Component(name = "globe_drivers_all_soundvelocity_supplier", service = { ISoundVelocitySupplier.class })
public class AllSoundVelocitySupplier implements ISoundVelocitySupplier {

	/** Logger **/
	private static final Logger logger = LoggerFactory.getLogger(AllSoundVelocitySupplier.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ISoundVelocityData> getSoundVelocityData(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof AllInfo ? new ISoundVelocityData() {
			private List<ISoundVelocityProfile> profiles;

			@Override
			public String getOriginFileName() {
				return fileInfo.getAbsolutePath();
			}

			@Override
			public List<ISoundVelocityProfile> getSoundVelocityProfiles() throws GIOException {
				if (null == profiles) {
					profiles = readSoundSpeedProfiles((AllInfo) fileInfo);
				}
				return profiles;
			}

			/**
			 * Reads {@link SoundVelocityProfile} from the specified {@link AllInfo}.
			 * 
			 * @param allInfo : input file (.all)
			 * 
			 * @return a list of {@link SoundVelocityProfile}.
			 * @throws UnsupportedSounderException
			 */
			private List<ISoundVelocityProfile> readSoundSpeedProfiles(AllInfo allInfo) throws GIOException {
				logger.debug("Extraction of sound speed profiles from {}... ", allInfo.getFilename());
				List<ISoundVelocityProfile> soundVelocityProfiles = new ArrayList<>();

				try (AllFile allFile = new AllFile(allInfo.getAbsolutePath())) {
					DatagramBuffer buffer = new DatagramBuffer(allFile.getByteOrder());

					// for each datagram: build a sound velocity profile
					for (Entry<SimpleIdentifier, DatagramPosition> posentry : allFile.getSoundSpeedProfile()
							.getDatagrams()) {
						final DatagramPosition pos = posentry.getValue();
						DatagramReader.readDatagramAt(pos.getFile(), buffer, pos.getSeek());

						// metadata (serial number, counter, date...)
						int serialNumber = SoundSpeedProfile.getSerialNumber(buffer.byteBufferData).getU();
						int pingCounter = SoundSpeedProfile.getCounter(buffer.byteBufferData).getU();

						Date soundProfileDate = new Date(
								SoundSpeedProfile.getProfilEpochTime(allFile, buffer.byteBufferData));
						// get sound speed values
						List<SoundVelocity> soundSpeedValues = new ArrayList<>();
						for (int i = 0; i < SoundSpeedProfile.getNumberEntries(buffer.byteBufferData).getU(); i++) {
							double depth = SoundSpeedProfile.getDepth(buffer.byteBufferData, i).getU() * 0.01d
									* SoundSpeedProfile.getDepthResolution(buffer.byteBufferData).getU();
							double soundSpeed = SoundSpeedProfile.getSoundSpeedProfile(buffer.byteBufferData, i).getU()
									* 0.1d;
							soundSpeedValues.add(new SoundVelocity(soundSpeed, depth));
						}
						soundVelocityProfiles.add(new SoundVelocityProfile(soundProfileDate, pingCounter, serialNumber,
								soundSpeedValues));
					}
				} catch (IOException | UnsupportedSounderException e) {
					throw new GIOException(e.getMessage(), e);
				}

				logger.debug(" {} sound profile(s) extracted.", soundVelocityProfiles.size());
				return soundVelocityProfiles;
			}

		} : null);
	}
}
