package fr.ifremer.globe.core.io.sounder;

import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class computes the global attributes of a sounder file (Mbg, Xsf...).
 */
public class SounderGlobalAttributesComputer {

	/**
	 * Computes and writes global attributes.
	 */
	public ComputedAttributes compute(SounderDataContainer container) throws GIOException {

		// Get layers
		DoubleLoadableLayer2D detectionLatitudeLayer = container.getLayer(BathymetryLayers.DETECTION_LATITUDE);
		DoubleLoadableLayer2D detectionLongitudeLayer = container.getLayer(BathymetryLayers.DETECTION_LONGITUDE);
		DoubleLayer2D depthLayer = container.getCsLayers(CoordinateSystem.FCS).getProjectedZ();
		LongLoadableLayer1D swathTimeLayer = container.getLayer(BeamGroup1Layers.PING_TIME);
		DetectionStatusLayer statusLayer = container.getStatusLayer();

		// Computes statistics
		ComputedAttributes result = new ComputedAttributes();
		for (int swathIdx = 0; swathIdx < container.getInfo().getCycleCount(); swathIdx++) {
			boolean isSwathValid = false;
			for (int beamIdx = 0; beamIdx < container.getInfo().getTotalRxBeamCount(); beamIdx++) {
				if (statusLayer.isValid(swathIdx, beamIdx)) {
					isSwathValid = true;

					double currentDepth = depthLayer.get(swathIdx, beamIdx);
					if (!Double.isNaN(currentDepth)) {
						result.minDepth = Math.min(result.minDepth, currentDepth);
						result.maxDepth = Math.max(result.maxDepth, currentDepth);
					}

					double currentLatitude = detectionLatitudeLayer.get(swathIdx, beamIdx);
					double currentLongitude = detectionLongitudeLayer.get(swathIdx, beamIdx);
					if (!Double.isNaN(currentLatitude) && !Double.isNaN(currentLongitude)) {
						result.south = Math.min(result.south, currentLatitude);
						result.north = Math.max(result.north, currentLatitude);
						result.west = Math.min(result.west, currentLongitude);
						result.east = Math.max(result.east, currentLongitude);
					}
				}
			}
			if (isSwathValid) {
				long currentTime = swathTimeLayer.get(swathIdx);
				result.minTime = Math.min(result.minTime, currentTime);
				result.maxTime = Math.max(result.maxTime, currentTime);
			}
		}

		return result;
	}

	// waiting for "record" of java 14...
	public static class ComputedAttributes {
		public long minTime = Long.MAX_VALUE;
		public long maxTime = Long.MIN_VALUE;
		public double minDepth = Double.POSITIVE_INFINITY;
		public double maxDepth = Double.NEGATIVE_INFINITY;
		public double south = Double.POSITIVE_INFINITY;
		public double north = Double.NEGATIVE_INFINITY;
		public double west = Double.POSITIVE_INFINITY;
		public double east = Double.NEGATIVE_INFINITY;
	}

}
