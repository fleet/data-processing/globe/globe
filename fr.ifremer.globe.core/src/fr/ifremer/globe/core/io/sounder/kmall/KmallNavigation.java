package fr.ifremer.globe.core.io.sounder.kmall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.ifremer.globe.api.xsf.converter.all.datagram.IndividualSensorMetadata;
import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SPO;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.units.Units;

/**
 * This class provides navigation data for a Kongsberg file (.kmall).
 */
public class KmallNavigation implements INavigationData {

	private final KmallInfo fileInfo;
	private final KmallFile kmallFile;
	private final List<DatagramPosition> positionDatagramPointers;
	private final DatagramBuffer datagramBuffer = new DatagramBuffer();
	private int currentPositionDatagramIndex = -1;

	/** Metadata **/
	private final List<NavigationMetadata<?>> metadata = List.of(
			NavigationMetadataType.TIME.withValueSupplier(this::getTime2),
			NavigationMetadataType.HEADING.withValueSupplier(this::getHeading),
			NavigationMetadataType.SPEED.withValueSupplier(this::getSpeed),
			NavigationMetadataType.SENSOR_TYPE.withValueSupplier(this::getSensorType));

	/**
	 * Constructor
	 */
	public KmallNavigation(KmallInfo fileInfo) throws GIOException {
		this.fileInfo = fileInfo;
		try {
			kmallFile = new KmallFile(fileInfo.getPath());
			positionDatagramPointers = getPositionDatagramPointers(kmallFile);
		} catch (IOException e) {
			throw new GIOException(e.getMessage(), e);
		}
	}

	@Override
	public void close() {
		kmallFile.close();
	}

	/**
	 * @return pointers on position datagrams sorted by time.
	 */
	private List<DatagramPosition> getPositionDatagramPointers(KmallFile fileMetaData) throws IOException {
		TreeMap<Long, DatagramPosition> positionDatagramPointersMap = new TreeMap<>();
		for (IndividualSensorMetadata sensorMetadata : fileMetaData.getSPOMetadata().getSensors().values()) {
			for (Entry<SimpleIdentifier, DatagramPosition> posentry : sensorMetadata.getDatagrams()) {
				var datagramPosition = posentry.getValue();
				DatagramReader.readDatagram(datagramPosition.getFile(), datagramBuffer, datagramPosition.getSeek());
				var time = SPO.getEpochTimeMilli(datagramBuffer.byteBufferData);
				positionDatagramPointersMap.put(time, datagramPosition);
			}
			//retrieve positions from first sensor only
			if(!positionDatagramPointersMap.isEmpty()) break;
		}
		return new ArrayList<>(positionDatagramPointersMap.values());
	}

	private void readPositionDatagram(int index) throws GIOException {
		try {
			if (currentPositionDatagramIndex != index) {
				DatagramPosition pointer = positionDatagramPointers.get(index);
				DatagramReader.readDatagram(pointer.getFile(), datagramBuffer, pointer.getSeek());
				currentPositionDatagramIndex = index;
			}
		} catch (IOException e) {
			throw new GIOException(e.getMessage(), e);
		}
	}

	@Override
	public int size() {
		return positionDatagramPointers.size();
	}

	@Override
	public String getFileName() {
		return fileInfo.getPath();
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return fileInfo.getContentType();
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		readPositionDatagram(index);
		return SPO.getCorrectedLat_deg(datagramBuffer.byteBufferData).get();
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		readPositionDatagram(index);
		return SPO.getCorrectedLong_deg(datagramBuffer.byteBufferData).get();
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

	private long getTime2(int index) throws GIOException {
		readPositionDatagram(index);
		return SPO.getEpochTimeMilli(datagramBuffer.byteBufferData);
	}

	private float getHeading(int index) throws GIOException {
		readPositionDatagram(index);
		return SPO.getCourseOverGround_deg(datagramBuffer.byteBufferData).get();
	}

	private float getSpeed(int index) throws GIOException {
		readPositionDatagram(index);
		return Units.metterSecondsToKnots(SPO.getSpeedOverGround_mPerSec(datagramBuffer.byteBufferData).get());
	}

	private INavigationSensorType getSensorType(int index) throws GIOException {
		readPositionDatagram(index);
		return NmeaSensorType.get(SPO.getSensorQualityIndicator(datagramBuffer.byteBufferData).getU());
	}

}
