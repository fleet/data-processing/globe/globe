package fr.ifremer.globe.core.io.sounder.kmall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.datagram.DatagramPosition;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramBuffer;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.DatagramReader;
import fr.ifremer.globe.api.xsf.converter.kmall.datagram.SVP;
import fr.ifremer.globe.api.xsf.converter.kmall.metadata.KmallFile;
import fr.ifremer.globe.api.xsf.util.SimpleIdentifier;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityProfile;
import fr.ifremer.globe.core.model.velocity.ISoundVelocitySupplier;
import fr.ifremer.globe.core.model.velocity.SoundVelocity;
import fr.ifremer.globe.core.model.velocity.SoundVelocityProfile;
import fr.ifremer.globe.utils.exception.GIOException;

@Component(name = "globe_drivers_kmall_soundvelocity_supplier", service = { ISoundVelocitySupplier.class })
public class KmallSoundVelocitySupplier implements ISoundVelocitySupplier {

	/** Logger **/
	private static final Logger logger = LoggerFactory.getLogger(KmallSoundVelocitySupplier.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ISoundVelocityData> getSoundVelocityData(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof KmallInfo ? new ISoundVelocityData() {
			private List<ISoundVelocityProfile> profiles;

			@Override
			public String getOriginFileName() {
				return fileInfo.getAbsolutePath();
			}

			@Override
			public List<ISoundVelocityProfile> getSoundVelocityProfiles() throws GIOException {
				if (null == profiles) {
					profiles = readSoundSpeedProfiles((KmallInfo) fileInfo);
				}
				return profiles;
			}

			/**
			 * Reads {@link SoundVelocityProfile} from the specified {@link KmallInfo}.
			 * 
			 * @param kmallInfo : input file (.kmall)
			 * 
			 * @return a list of {@link SoundVelocityProfile}.
			 * @throws GIOException
			 */
			private List<ISoundVelocityProfile> readSoundSpeedProfiles(KmallInfo kmallInfo) throws GIOException {
				logger.debug("Extraction of sound speed profiles from {}... ", kmallInfo.getFilename());
				List<ISoundVelocityProfile> soundVelocityProfiles = new ArrayList<>();

				try (KmallFile kmallFile = new KmallFile(kmallInfo.getAbsolutePath())) {
					DatagramBuffer datagramBuffer = new DatagramBuffer();

					// for each datagram: build a sound velocity profile
					for (Entry<SimpleIdentifier, DatagramPosition> posentry : kmallFile.getSVPMetadata()
							.getDatagrams()) {
						final DatagramPosition pos = posentry.getValue();
						DatagramReader.readDatagram(pos.getFile(), datagramBuffer, pos.getSeek());

						// metadata (date...)
						Date soundProfileDate = new Date(
								SVP.getEpochTimeMilli(datagramBuffer.byteBufferData));
						// get sound speed values
						List<SoundVelocity> soundSpeedValues = new ArrayList<>();
						for (int i = 0; i < SVP.getSamplesCount(datagramBuffer.byteBufferData)
								.getU(); i++) {
							double depth = SVP.getDepth_m(datagramBuffer.byteBufferData, i).get();
							double soundSpeed = SVP.getSoundVelocity_mPerSec(datagramBuffer.byteBufferData, i).get();
							soundSpeedValues.add(new SoundVelocity(soundSpeed, depth));
						}
						soundVelocityProfiles.add(new SoundVelocityProfile(soundSpeedValues, soundProfileDate));
					}
				} catch (IOException e) {
					throw new GIOException(e.getMessage(), e);
				}

				logger.debug(" {} sound profile(s) extracted.", soundVelocityProfiles.size());
				return soundVelocityProfiles;
			}

		} : null);
	}
}
