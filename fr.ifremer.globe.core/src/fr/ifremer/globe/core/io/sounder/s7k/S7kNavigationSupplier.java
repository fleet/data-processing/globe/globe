package fr.ifremer.globe.core.io.sounder.s7k;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;

@Component(name = "globe_drivers_s7k_navigation_supplier", service = { INavigationSupplier.class })
public class S7kNavigationSupplier implements INavigationSupplier {

	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof S7kInfo ? accessMode -> new S7kNavigation((S7kInfo) fileInfo) : null);
	}

}
