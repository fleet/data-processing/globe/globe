package fr.ifremer.globe.core.io.sounder.s7k;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ifremer.globe.api.xsf.converter.s7k.metadata.S7KFile;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;

public class S7kInfo implements IFileInfo {

	/** Content type **/
	public static final ContentType CONTENT_TYPE = ContentType.SOUNDER_S7K;

	/** File path **/
	private final String filePath;

	/** Computed {@link GeoBox} **/
	private GeoBox geoBox;

	/** Dates **/
	private Date minDate;
	private Date maxDate;

	/**
	 * Constructor
	 */
	public S7kInfo(String filePath) throws IOException {
		this.filePath = filePath;
		try (S7KFile s7kFile = new S7KFile(filePath)) {
			// geobox
			double north = s7kFile.getStats().getLat().getMax();
			double south = s7kFile.getStats().getLat().getMin();
			double east = s7kFile.getStats().getLon().getMax();
			double west = s7kFile.getStats().getLon().getMin();
			geoBox = new GeoBox(north, south, east, west);

			// dates
			minDate = new Date(s7kFile.getStats().getDate().getMin());
			maxDate = new Date(s7kFile.getStats().getDate().getMax());
		}
	}

	/**
	 * Provides properties displayed in "Properties" view.
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new ArrayList<>();
		result.add(Property.build("Date start", minDate.toString()));
		result.add(Property.build("Date end", maxDate.toString()));
		result.addAll(Property.build(geoBox));
		return result;
	}

	//// GETTERS & SETTERS

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public ContentType getContentType() {
		return CONTENT_TYPE; // useful? Can't be retrieved from info Supplier ?
	}

	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	public void setGeoBox(GeoBox geobox) {
		this.geoBox = geobox;
	}

}
