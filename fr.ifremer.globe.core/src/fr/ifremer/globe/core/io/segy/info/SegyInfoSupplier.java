/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.segy.info;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;
import no.petroware.seismicio.segy.SegyFile;
import no.petroware.seismicio.segy.SegyFileReader;

/**
 * Loader of Seg-y file info.
 */
@Component(name = "globe_drivers_segy_file_info_supplier", service = { SegyInfoSupplier.class,
		IFileInfoSupplier.class })
public class SegyInfoSupplier extends BasicFileInfoSupplier<SegyFileInfo> {

	/** Logger. */
	private static final Logger logger = LoggerFactory.getLogger(SegyInfoSupplier.class);

	/**
	 * Constructor
	 */
	public SegyInfoSupplier() {
		super("sgy", "SEG-Y file", ContentType.SEGY);
	}

	@Override
	public List<String> getExtensions() {
		return List.of("sgy", "segy", "seg");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return List.of(Pair.of("sgy", "SEG-Y file (*.sgy)"), Pair.of("segy", "SEG-Y file (*.segy)"));
	}

	/** {@inheritDoc} */
	@Override
	public SegyFileInfo makeFileInfo(String filePath) {
		SegyFileInfo result = null;

		// Open with Seismicio
		SegyFileReader reader = new SegyFileReader(new File(filePath));
		try {
			SegyFile segyFile = reader.read(false);
			if (segyFile.getFileHeader().getNSamples() > 1) {
				segyFile = reader.read(true);
				result = new SegyFileInfo(filePath, segyFile);
			} else
				logger.warn("Unsupported Segy file format {}", filePath);

		} catch (IOException e) {
			logger.warn("Unreadable Segy file {} : {}", filePath, e.getMessage());
		}

		return result;
	}

}