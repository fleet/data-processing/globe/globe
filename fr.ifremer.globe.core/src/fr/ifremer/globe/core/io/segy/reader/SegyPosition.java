package fr.ifremer.globe.core.io.segy.reader;

/**
 * One position extracted from a Segy file
 */
public record SegyPosition(

		/** The latitude of the point. */
		double lat,

		/** The longitude of the point. */
		double lon,

		/** Top elevation of the point. */
		double topElevation,

		/** Bottom elevation of the point. */
		double bottomElevation) {
}
