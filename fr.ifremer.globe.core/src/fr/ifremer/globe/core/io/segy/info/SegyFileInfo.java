/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.segy.info;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;

import fr.ifremer.globe.core.io.segy.reader.SEGYUtils;
import fr.ifremer.globe.core.io.segy.reader.SegyPosition;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import no.petroware.seismicio.segy.SegyFile;
import no.petroware.seismicio.segy.SegyFileHeader.LengthUnit;
import no.petroware.seismicio.segy.SegyTrace;
import no.petroware.seismicio.segy.SegyTraceHeader;
import no.petroware.seismicio.segy.SegyTraceHeader.CoordinateUnit;

/**
 * IFileInfo of a Seg-Y file
 */
public class SegyFileInfo extends BasicFileInfo {

	/** Feet to meter */
	public static final double FEET_TO_METER = 0.3048;
	/** Default velocity in m/s */
	public static final double DEFAULT_WEATHERING_VELOCITY = 1500d;

	/** Nb of trace in file */
	private final int traceCount;
	/** Nb of sample per trace */
	private final int samplesPerTrace;

	/** Extracted positions */
	private final List<SegyPosition> points;

	/** Values contained in the file */
	private final double[][] samples;

	/** Min value in samples */
	private final double minValue;
	/** Max value in samples */
	private final double maxValue;

	/**
	 * Constructor
	 */
	public SegyFileInfo(String path, SegyFile segyFile) {
		super(path, ContentType.SEGY);
		this.traceCount = segyFile.getNTraces();
		this.samplesPerTrace = segyFile.getNSamplesPerTrace();
		this.points = new ArrayList<>(traceCount);
		samples = new double[traceCount][samplesPerTrace];

		// Switch from DDDMMSS.ss to DDDMMSSss with a x100 scale factor
		var latLonScaleFactor = segyFile.getTraces().stream().map(SegyTrace::getHeader)
				.anyMatch(h -> h.getX() != (int) h.getX() || h.getY() != (int) h.getY()) ? 100.0 : 1.0;

		double measurementScalar = segyFile.getFileHeader().getLengthUnit() == LengthUnit.FEET ? FEET_TO_METER : 1d;
		DoubleSummaryStatistics valuesStat = new DoubleSummaryStatistics();
		for (var traceIndex = 0; traceIndex < traceCount; traceIndex++) {
			SegyTrace trace = segyFile.getTrace(traceIndex);
			for (var sampleIndex = 0; sampleIndex < samplesPerTrace; sampleIndex++) {
				if (trace.getValue(sampleIndex) instanceof Number value) {
					samples[traceIndex][sampleIndex] = value.doubleValue();
					valuesStat.accept(value.doubleValue());
				} else
					samples[traceIndex][sampleIndex] = Double.NaN;
			}

			// Get latitude and longitude
			SegyTraceHeader traceHeader = trace.getHeader();
			CoordinateUnit coordinateUnit = traceHeader.getCoordinateUnit();
			double lon = SEGYUtils.convertXY(traceHeader.getX() * latLonScaleFactor, coordinateUnit);
			double lat = SEGYUtils.convertXY(traceHeader.getY() * latLonScaleFactor, coordinateUnit);

			// compute weathering velocity in m/s
			double weatheringVelocity = traceHeader.getWeatheringVelocity();
			weatheringVelocity = weatheringVelocity != 0 ? weatheringVelocity * measurementScalar
					: DEFAULT_WEATHERING_VELOCITY;
			double sampleInterval = traceHeader.getSampleInterval() / 1e6; // Interval in µs

			// Compute top and bottom elevation
			double topElevation = -(traceHeader.getSourceDepth() + traceHeader.getWaterDepthSource())
					* measurementScalar;
			double bottomElevation = topElevation
					- Math.abs(traceHeader.getNSamples() * sampleInterval * weatheringVelocity);
			this.points.add(new SegyPosition(lat, lon, topElevation, bottomElevation));

		}

		// Determine min and max values
		this.minValue = valuesStat.getMin();
		this.maxValue = valuesStat.getMax();

		// Determine Geobox
		DoubleSummaryStatistics lats = points.stream().mapToDouble(SegyPosition::lat).summaryStatistics();
		DoubleSummaryStatistics lons = points.stream().mapToDouble(SegyPosition::lon).summaryStatistics();
		GeoBox geoBox = new GeoBox(lats.getMax(), lats.getMin(), lons.getMax(), lons.getMin());
		setGeoBox(geoBox);
	}

	/** {@inheritDoc} */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();
		result.add(Property.build("Minimum sample value", getMinValue()));
		result.add(Property.build("Maximum sample value", getMaxValue()));

		if (points != null && !points.isEmpty()) {
			result.add(Property.build("Bottom elevation",
					points.stream().mapToDouble(SegyPosition::bottomElevation).min().orElse(0d)));
			result.add(Property.build("Top elevation",
					points.stream().mapToDouble(SegyPosition::topElevation).min().orElse(0d)));
		}

		return result;
	}

	/**
	 * @return the {@link #points}
	 */
	public List<SegyPosition> getPoints() {
		return points;
	}

	/**
	 * @return
	 * @see no.petroware.seismicio.segy.SegyFile#getNTraces()
	 */
	public int getNTraces() {
		return traceCount;
	}

	/**
	 * @return
	 * @see no.petroware.seismicio.segy.SegyFile#getNSamplesPerTrace()
	 */
	public int getNSamplesPerTrace() {
		return samplesPerTrace;
	}

	/**
	 * @return the value of one sample of one trace
	 */
	public double getValue(int trace, int sample) {
		return this.samples[trace][sample];
	}

	/**
	 * @return the {@link #minValue}
	 */
	public double getMinValue() {
		return minValue;
	}

	/**
	 * @return the {@link #maxValue}
	 */
	public double getMaxValue() {
		return maxValue;
	}

}
