package fr.ifremer.globe.core.io.segy.reader;

import no.petroware.seismicio.segy.SegyTraceHeader.CoordinateUnit;

/**
 * Some utility methods on a SEGY file.
 */
public class SEGYUtils {
	public static final double SECONDOFARC_TO_DEGREES = 1d / 3600d;

	/** Convert a latitude/longitude in a SEGY format to degrees */
	public static double convertXY(double value, CoordinateUnit coordinateUnit) {
		return switch (coordinateUnit) {
		case DMS -> dmsToDecimal(value);
		case ARCSECONDS -> value * SECONDOFARC_TO_DEGREES;
		default -> value;
		};
	}

	/**
	 * Convert a packed DMS value (DDDMMSSss) into decimal degrees.
	 */
	public static double dmsToDecimal(double angle) {
		double sign = angle < 0.0 ? -1 : 1;
		angle = Math.abs(angle);
		double degrees = Math.floor(angle / 1e6);
		double minutes = Math.floor(angle % 1e6 / 1e4);
		double seconds = angle % 1e4 / 1e2;
		return sign * (degrees + minutes / 60.0 + seconds / 3600.0); // Degrees
	}

}
