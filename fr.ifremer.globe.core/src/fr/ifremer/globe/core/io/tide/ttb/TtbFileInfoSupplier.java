package fr.ifremer.globe.core.io.tide.ttb;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.tide.TideFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.tide.ITideData;

/**
 * Supplier of {@link TideFileInfo} from *.ttb files.<br>
 * A ttb file has a well known format and can be parsed without any FileInfoSettings.
 */
@Component(name = "globe_drivers_ttb_csv_file_info_supplier", service = IFileInfoSupplier.class)
public class TtbFileInfoSupplier extends BasicFileInfoSupplier<TideFileInfo> {

	/** Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(TtbFileInfoSupplier.class);

	public static final String EXTENSION_TTB = "ttb";

	private static final Pattern PATTERN = Pattern
			.compile("(\\d+/\\d+/\\d+)\\s+(\\d+:\\d+:\\d+.?\\d*)\\s+(-?\\d+.?\\d*).*");

	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss[.SSS]")
			.withZone(ZoneOffset.UTC);

	/**
	 * Constructor
	 */
	public TtbFileInfoSupplier() {
		super(EXTENSION_TTB, "Ttb file", ContentType.TIDE_CSV);
	}

	/**
	 * Reads Ttb file.
	 */
	@Override
	public TideFileInfo makeFileInfo(String filePath) {
		List<ITideData> tideData = new ArrayList<>();
		try {
			List<String> lines = Files.readAllLines(Paths.get(filePath));
			for (int i = 0; i < lines.size(); i++) {
				try {
					var m = PATTERN.matcher(lines.get(i));
					if (!m.matches() || m.groupCount() != 3) {
						if (i == 0)
							continue; // jump first line if contains headers
						throw new IOException("Pattern doesn't match.");
					}
					Instant date = ZonedDateTime.parse(m.group(1) + " " + m.group(2), DATE_TIME_FORMATTER).toInstant();
					double value = Double.parseDouble(m.group(3).replace(",", "."));
					tideData.add(ITideData.build(date, value));
				} catch (IOException | NumberFormatException | DateTimeParseException e) {
					LOGGER.error("Error with file " + filePath + " : line " + (i + 1) + " not parsable : '"
							+ lines.get(i) + "'; expected format: dd/MM/yyyy HH:mm:ss\tvalue", e);
					return null;
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error while reading Ttb file : {} ", filePath, e);
			return null;
		}
		return new TideFileInfo(filePath, tideData);
	}
}
