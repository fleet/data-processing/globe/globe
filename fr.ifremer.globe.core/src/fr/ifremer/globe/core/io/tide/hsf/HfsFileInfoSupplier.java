package fr.ifremer.globe.core.io.tide.hsf;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.tide.TideFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.tide.ITideData;

/**
 * Supplier of {@link TideFileInfo} from *.hfs files (SHOM tide file).<br>
 */
@Component(name = "globe_drivers_hfs_file_info_supplier", service = IFileInfoSupplier.class)
public class HfsFileInfoSupplier extends BasicFileInfoSupplier<TideFileInfo> {

	/** Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(HfsFileInfoSupplier.class);

	public static final String EXTENSION_HSF = "hfs";

	private static final Pattern PATTERN = Pattern.compile("(\\d+-\\d+-\\d+\\s+\\d+:\\d+:\\d+)\\s+(.+)");

	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
			.withZone(ZoneOffset.UTC);

	/**
	 * Constructor
	 */
	public HfsFileInfoSupplier() {
		super(EXTENSION_HSF, "Shom tide file", ContentType.TIDE_CSV);
	}

	/**
	 * Reads hsf file.
	 */
	@Override
	public TideFileInfo makeFileInfo(String filePath) {
		List<ITideData> tideData = new ArrayList<>();
		try {
			List<String> lines = Files.readAllLines(Paths.get(filePath));
			for (int i = 0; i < lines.size(); i++) {
				try {
					var m = PATTERN.matcher(lines.get(i));
					if (!m.matches() || m.groupCount() != 2)
						throw new IOException("Pattern doesn't match.");
					Instant date = ZonedDateTime.parse(m.group(1), DATE_TIME_FORMATTER).toInstant();
					double value = Double.parseDouble(m.group(2).replace(",", "."));
					tideData.add(ITideData.build(date, value));
				} catch (IOException | NumberFormatException | DateTimeParseException e) {
					LOGGER.error("Error with file " + filePath + " : line " + (i + 1) + " not parsable : '"
							+ lines.get(i) + "'; expected format: dd/MM/yyyy HH:mm:ss\tvalue", e);
					return null;
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error while reading Ttb file : {} ", filePath, e);
			return null;
		}
		return new TideFileInfo(filePath, tideData);
	}
}
