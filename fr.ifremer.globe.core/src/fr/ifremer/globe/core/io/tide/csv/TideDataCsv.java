package fr.ifremer.globe.core.io.tide.csv;

import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import fr.ifremer.globe.core.model.tide.ITideData;

public class TideDataCsv implements ITideData {

	/** Attributes **/
	protected String date;
	protected double latitude = Double.NaN;
	protected double longitude = Double.NaN;
	protected double tide;

	// GETTERS

	@Override
	public Instant getTimestamp() {
		try {
			return Instant.parse(date);
		} catch (DateTimeParseException e) {
			return Instant.now();
		}
	}

	@Override
	public double getValue() {
		return tide;
	}

	@Override
	public Optional<Double> getLatitude() {
		return Double.isNaN(latitude) ? Optional.empty() : Optional.of(latitude);
	}

	@Override
	public Optional<Double> getLongitude() {
		return Double.isNaN(longitude) ? Optional.empty() : Optional.of(longitude);
	}
}
