/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.tide.csv;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import com.opencsv.ICSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.bean.exceptionhandler.ExceptionHandlerIgnore;
import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.model.navigation.NavigationPoint;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to write CSV for tide process input.
 */
public class InputTideProcessCsvFileWriter {

	static final char SEPARATOR = ';';

	/** CSV columns (must be same than the {@link CsvData} properties ) **/
	private static final String[] COLUMNS = new String[] { "date", "longitude", "latitude" };

	/** Data for one CSV line. */
	@SuppressWarnings("unused")
	private static class CsvData {
		String date;
		double longitude;
		double latitude;

		public CsvData(Instant instant, double longitude, double latittude) {
			this.date = instant.toString();
			this.longitude = longitude;
			this.latitude = latittude;
		}
	}

	/**
	 * Writes a CSV marker files.
	 */
	public static void write(String filePath, List<NavigationPoint> navigationPoints) throws GIOException {
		// parse to CsvMarkers
		List<CsvData> csvData = navigationPoints.stream()//
				.map(navigationPoint -> new CsvData(navigationPoint.date, navigationPoint.lon, navigationPoint.lat))
				.collect(Collectors.toList());

		// OpenCsv strategy : mapping each attributes to a column index
		var strategy = new ColumnPositionMappingStrategy<CsvData>();
		strategy.setColumnMapping(COLUMNS);
		strategy.setType(CsvData.class);

		try (Writer writer = new FileWriter(filePath)) {
			// headers
			writer.write(String.join(";", COLUMNS) + ICSVWriter.RFC4180_LINE_END);

			StatefulBeanToCsv<CsvData> statefulBeanToCsv = new StatefulBeanToCsvBuilder<CsvData>(writer)//
					.withExceptionHandler(new ExceptionHandlerIgnore())//
					.withMappingStrategy(strategy) //
					.withOrderedResults(true) //
					.withSeparator(SEPARATOR) //
					.withLineEnd(ICSVWriter.RFC4180_LINE_END) //
					.withApplyQuotesToAll(false) //
					.build();
			statefulBeanToCsv.write(csvData);

		} catch (CsvException | IOException e) {
			throw new GIOException("Error : " + e.getMessage(), e);
		}
	}

}
