/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.tide.csv;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.io.csv.ICsvFileInfoSupplier;
import fr.ifremer.globe.core.io.tide.TideFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.csv.CsvUtils;

/**
 * Loader of {@link TideDataCsv}.
 */
@Component(name = "globe_drivers_tide_csv_file_info_supplier", service = { ICsvFileInfoSupplier.class,
		TideCsvFileInfoSupplier.class })
public class TideCsvFileInfoSupplier implements ICsvFileInfoSupplier {

	public static final String COL_DATE = "date";
	public static final String COL_LON = "longitude";
	public static final String COL_LAT = "latitude";
	public static final String COL_TIDE = "tide";
	public static final List<String> COLUMNS = List.of(COL_DATE, COL_LON, COL_LAT, COL_TIDE);

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(TideCsvFileInfoSupplier.class);

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return ContentType.TIDE_CSV;
	}

	/**
	 * A tide file may have the columns longitude, latitude, date & tide.
	 */
	@Override
	public Optional<FileInfoSettings> evaluatesSettings(String filePath, List<String> firstLines) {
		FileInfoSettings result = new FileInfoSettings(getContentType());
		result.setElevationScaleFactor(1d);
		result.setRowCountToSkip(1);
		result.setLocale(Locale.US);

		String line = firstLines.get(0);
		var sep = CsvUtils.guessDelimiter(line);
		int[] colIndex = new int[COLUMNS.size()];
		Arrays.fill(colIndex, -1);
		if (sep.isPresent()) {
			char delimiter = sep.get();
			result.setDelimiter(delimiter);

			// Search columns index
			var columns = CsvUtils.parseLine(firstLines.get(0), delimiter);
			for (int i = 0; i < COLUMNS.size(); i++) {
				colIndex[i] = CsvUtils.guessColumnIndex(columns, COLUMNS.get(i));
			}
		}

		for (int i = 0; i < COLUMNS.size(); i++) {
			result.set(COLUMNS.get(i), colIndex[i] >= 0 ? colIndex[i] : -2);
		}

		// Add optional columns
		result.addOptionalColumn(COL_LON);
		result.addOptionalColumn(COL_LAT);
		result.addNonNumericColumn(COL_DATE);

		return Optional.of(result);
	}

	/**
	 * @return {@link TideFileInfo} by parsing file with default settings.
	 */
	public Optional<TideFileInfo> getFileInfo(String filePath) {
		var settings = evaluatesSettings(filePath, FileUtils.readLines(filePath, 5)).get();
		return getFileInfo(filePath, settings).map(TideFileInfo.class::cast);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ICsvFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		TideFileInfo result = null;

		// Extract column names with new CSV marker
		try (var fileReader = new FileReader(new File(filePath))) {
			// OpenCsv strategy : mapping each attributes to a column index
			var strategy = new ColumnPositionMappingStrategy<TideDataCsv>();
			var columnMapping = new String[20];
			Arrays.fill(columnMapping, "dummy");
			// Mapping column index => attribute name of csv bean
			for (String element : COLUMNS) {
				int colIndex = settings.getInt(element, -1);
				if (colIndex >= 0 && colIndex < columnMapping.length) {
					columnMapping[colIndex] = element;
				}
			}
			strategy.setColumnMapping(columnMapping);
			strategy.setType(TideDataCsv.class);
			var errors = new ArrayList<CsvException>();

			List<TideDataCsv> beans = //
					new CsvToBeanBuilder<TideDataCsv>(fileReader)//
							.withMappingStrategy(strategy)//
							.withSeparator(settings.getDelimiter())//
							.withExceptionHandler(e -> {
								logger.error("Error while parsing navigation CSV at line " + e.getLineNumber() + " : "
										+ e.getMessage(), e);
								errors.add(e);
								return e;
							}) //
							.withSkipLines(settings.getRowCountToSkip()) //
							.build()//
							.parse();

			result = new TideFileInfo(filePath, beans.stream().map(ITideData.class::cast).collect(Collectors.toList()),
					errors);
		} catch (RuntimeException | IOException ex) {
			// Something went wrong : CSV not acceptable
			logger.warn("{} was unable to parse the file {} : {}", getClass().getSimpleName(), filePath,
					ex.getMessage());
		}
		return Optional.ofNullable(result);
	}
}