package fr.ifremer.globe.core.io.tide.ttb;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.number.NumberUtils;

public class TtbFileWriter {

	/** Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(TtbFileWriter.class);

	/** File formatting properties **/
	public static final String SEPARATOR = "\t";

	/** Private constructor : utility class which provides only static methods **/
	private TtbFileWriter() {
		//
	}

	/**
	 * Writes a CSV tide file.
	 */
	public static void write(String filePath, List<ITideData> tideData) throws GIOException {
		LOGGER.info("Write tide file (.ttb) {} with {} data...", filePath, tideData.size());
		if (!filePath.endsWith(TtbFileInfoSupplier.EXTENSION_TTB))
			throw new GIOException("Invalid file path : .ttb extension required");

		if (!tideData.isEmpty()) {
			try (Writer writer = new FileWriter(filePath)) {
				for (ITideData data : tideData) {
					writer.write(String.format("%s%s%s%n",
							TtbFileInfoSupplier.DATE_TIME_FORMATTER.format(data.getTimestamp()), SEPARATOR,
							NumberUtils.getStringDouble(data.getValue())));
				}
			} catch (IOException e) {
				throw new GIOException("Error while writing tide file (.ttb) : " + e.getMessage(), e);
			}
		} else {
			throw new GIOException("Tide file can't be generated without data.");
		}
	}

}
