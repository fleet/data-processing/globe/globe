package fr.ifremer.globe.core.io.tide;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.tide.ttb.TtbFileInfoSupplier;
import fr.ifremer.globe.core.io.tide.ttb.TtbFileWriter;
import fr.ifremer.globe.core.model.chart.ChartSeriesUtils;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.BasicChartSeries;
import fr.ifremer.globe.core.model.chart.impl.ChartFileInfoDataSource;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartData;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Service to create {@link IChartSeries} from {@link TideFileInfo}.
 */
@Component(name = "globe_drivers_tide_chart_series_supplier", service = { IChartSeriesSupplier.class })
public class TideChartSeriesSupplier implements IChartSeriesSupplier {

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.TIDE_CSV);
	}

	@Override
	public List<TideChartSeries> getChartSeries(IFileInfo fileInfo) {
		return fileInfo instanceof TideFileInfo ? List.of(new TideChartSeries((TideFileInfo) fileInfo))
				: Collections.emptyList();
	}

	/**
	 * Implementation of {@link IChartSeries} from {@link TideFileInfo}.
	 */
	class TideChartSeries extends BasicChartSeries<IChartData> {

		private final List<SimpleChartData> chartData = new ArrayList<>();

		/** Constructor **/
		public TideChartSeries(TideFileInfo tideFileInfo) {
			super(tideFileInfo.getBaseName(), new ChartFileInfoDataSource<TideFileInfo>(tideFileInfo));
			for (int i = 0; i < tideFileInfo.getData().size(); i++) {
				ITideData tideData = tideFileInfo.getData().get(i);
				chartData
						.add(new SimpleChartData(this, i, tideData.getTimestamp().toEpochMilli(), tideData.getValue()));
			}
		}

		@Override
		public int size() {
			return chartData.size();
		}

		@Override
		public IChartData getData(int index) {
			return chartData.get(index);
		}

		public void save(String outputFilePath) throws GIOException {
			// ask a "classic" save if output is not .ttb
			if (!FilenameUtils.getExtension(outputFilePath).equals(TtbFileInfoSupplier.EXTENSION_TTB)) {
				ChartSeriesUtils.saveSeriesToCsv(this, outputFilePath);
				return;
			}

			// compute ITideData from IChartData
			List<ITideData> tideData = stream()//
					.filter(IChartData::isEnabled)// filter to keep only validated data
					.map(chartData -> new ITideData() {
						@Override
						public Instant getTimestamp() {
							return Instant.ofEpochMilli((long) chartData.getX());
						}

						@Override
						public double getValue() {
							return chartData.getY();
						}
					}).collect(Collectors.toList());

			TtbFileWriter.write(outputFilePath, tideData);
		}

	}

}
