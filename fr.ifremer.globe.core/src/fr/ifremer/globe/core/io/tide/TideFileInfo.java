package fr.ifremer.globe.core.io.tide;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.core.utils.Pair;

/**
 * Tide file info.
 */
public class TideFileInfo extends BasicFileInfo implements ICsvFileInfo {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(TideFileInfo.class);

	/** Tide data */
	private final List<ITideData> data;

	/** Start / end dates */
	private Instant startDate = null;
	private Instant endDate = null;

	/** Errors while reading CSV file **/
	private final List<CsvException> parsingErrors;

	/**
	 * Constructor
	 */
	public TideFileInfo(String filePath, List<ITideData> tideData) {
		this(filePath, tideData, Collections.emptyList());
	}

	/**
	 * Constructor
	 */
	public TideFileInfo(String filePath, List<ITideData> tideData, List<CsvException> errors) {
		super(filePath, ContentType.TIDE_CSV);
		this.data = tideData;
		this.parsingErrors = errors;
		for (ITideData currentData : data) {
			Instant date = currentData.getTimestamp();
			if (startDate == null || date.compareTo(startDate) < 0) {
				startDate = date;
			}
			if (endDate == null || date.compareTo(endDate) > 0) {
				endDate = date;
			}
		}
	}

	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		return startDate != null && endDate != null ? Optional.of(Pair.of(startDate, endDate)) : Optional.empty();
	}

	public List<ITideData> getData() {
		return data;
	}

	/**
	 * @see fr.ifremer.globe.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();
		result.add(Property.build("Number of points", data.size()));
		result.add(Property.build("Start date", startDate.toString()));
		result.add(Property.build("End date", endDate.toString()));
		return result;
	}

	@Override
	public List<CsvException> getParsingErrors() {
		return parsingErrors;
	}

}