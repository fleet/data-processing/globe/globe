/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.LongBuffer1D;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Computes the swath time layer of the sounder data container
 */
public class TimeLayerLoader implements ILayerLoader<LongBuffer1D> {

	/** Properties **/
	private final long[] mbgShape;
	private final IntLoadableLayer2D mbgDateLayer;
	private final IntLoadableLayer2D mbgTimeLayer;
	private final AtomicBoolean loading = new AtomicBoolean(false);
	private final int[] antennaIndexes;

	/**
	 * Constructor
	 */
	public TimeLayerLoader(IntLoadableLayer2D dateLayer, IntLoadableLayer2D timeLayer, int[] antennaIndexes) {
		this.mbgShape = timeLayer.getDimensions();
		this.mbgDateLayer = dateLayer;
		this.mbgTimeLayer = timeLayer;
		this.antennaIndexes = antennaIndexes;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return new long[] { mbgShape[0] };
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(LongBuffer1D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			for (int iSwath = 0; iSwath < mbgShape[0]; iSwath++) {
				int datePart = mbgDateLayer.get(iSwath, antennaIndexes[iSwath]);
				int timePart = mbgTimeLayer.get(iSwath, antennaIndexes[iSwath]);
				long swathTime = DateUtils.milliSecondToNano(DateUtils.getEpochMilliFromJulian(datePart, timePart));
				buffer.set(iSwath, swathTime);
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean flush(LongBuffer1D buffer) throws GIOException {
		return false;
	}

}
