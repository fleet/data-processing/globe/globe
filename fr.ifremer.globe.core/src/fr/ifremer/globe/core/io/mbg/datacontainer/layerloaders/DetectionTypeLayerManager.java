package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionType;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ByteBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.bathymetry.VendorSpecificLayers;
import fr.ifremer.globe.utils.exception.GIOException;

public class DetectionTypeLayerManager implements ILayerLoader<ByteBuffer2D> {

	protected long[] shape;
	protected SounderDataContainer sounderDataContainer;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	protected MbgInfo mbgInfo;

	/**
	 * Constructor
	 */
	public DetectionTypeLayerManager(SounderDataContainer sounderDataContainer) {
		this.mbgInfo = (MbgInfo) sounderDataContainer.getInfo();
		this.sounderDataContainer = sounderDataContainer;
		this.shape = new long[] { mbgInfo.getCycleCount(), mbgInfo.getTotalRxBeamCount() };
	}

	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(ByteBuffer2D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			processLoading(buffer);
			loading.set(false);
			buffer.setDirty(false);
		}
		return true;
	}

	protected void processLoading(ByteBuffer2D buffer) throws GIOException {
		FloatLayer2D mbQuality = sounderDataContainer.getLayer(VendorSpecificLayers.DETECTION_CONSTRUCTOR_QUALITY_FACTOR);

		for (int cycleIndex = 0; cycleIndex < shape[0]; cycleIndex++) {
			for (int beamIndex = 0; beamIndex < shape[1]; beamIndex++) {
				int qualityValue = (int) mbQuality.get(cycleIndex, beamIndex);
				buffer.set(cycleIndex, beamIndex,
						(byte) (qualityValue <= 127 ? DetectionType.AMPLITUDE.get() : DetectionType.PHASE.get()));
			}
		}
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean flush(ByteBuffer2D buffer) throws GIOException {
		// we never modify this value
		return true;
	}
}
