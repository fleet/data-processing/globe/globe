package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IntBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class SwathIntLayerLoader implements ILayerLoader<IntBuffer1D> {

	/** Properties **/
	private final long[] mbgShape;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	private final IntLoadableLayer2D mbgLayer;
	private final int scaleFactor;
	private final int offset;
	private final int[] antennaIndexes;

	/** Constructors **/
	public SwathIntLayerLoader(IntLoadableLayer2D mbgLayer, int scaleFactor, int offset, int[] antennaIndexes)
			throws GIOException {
		this.mbgLayer = mbgLayer;
		this.mbgShape = mbgLayer.getLoader().getDimensions();
		this.scaleFactor = scaleFactor;
		this.offset = offset;
		this.antennaIndexes = antennaIndexes;
	}

	public SwathIntLayerLoader(IntLoadableLayer2D mbgLayer, int[] antennaIndexes) throws GIOException {
		this(mbgLayer, 1, 0, antennaIndexes); // by default scaleFactor = 1 and offset = 0
	}

	@Override
	public long[] getDimensions() throws GIOException {
		return new long[] { mbgShape[0] };
	}

	/** Load the model layer buffer with the MBG layer **/
	@Override
	public boolean load(IntBuffer1D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			for (int iSwath = 0; iSwath < mbgShape[0]; iSwath++) {
				buffer.set(iSwath, (int) (mbgLayer.get(iSwath, antennaIndexes[iSwath]) * scaleFactor + offset));
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	/** Flush the MBG layer with values from the corresponding model layer **/
	@Override
	public boolean flush(IntBuffer1D buffer) throws GIOException {
		for (int iSwath = 0; iSwath < mbgShape[0]; iSwath++) {
			for (int iAntenna = 0; iAntenna < mbgShape[1]; iAntenna++)
				mbgLayer.set(iSwath, iAntenna, (buffer.get(iSwath) - offset) / scaleFactor);
		}
		mbgLayer.flush();
		return false;
	}

}
