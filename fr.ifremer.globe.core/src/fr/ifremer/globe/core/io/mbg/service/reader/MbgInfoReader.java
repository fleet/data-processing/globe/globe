package fr.ifremer.globe.core.io.mbg.service.reader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters.Type;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.sounders.SounderDescription;
import fr.ifremer.globe.utils.sounders.SoundersLibrary;

/**
 * This class provides methods to read an MBG file.
 */
public class MbgInfoReader {

	private final Logger logger = LoggerFactory.getLogger(MbgInfoReader.class);

	/**
	 * Reads the Netcdf file and fills the {@link MbgInfo}
	 */
	public void readFile(MbgInfo info, IProgressMonitor monitor) throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 12);
		try {
			info.open(true);
			subMonitor.worked(1);
			readDimensions(info);
			subMonitor.worked(1);
			readFileAttributs(info);
			subMonitor.worked(1);
			readMinMaxDepth(info);
			subMonitor.worked(1);
			readGeoBox(info);
			subMonitor.worked(1);
			readFirsLastDate(info);
			subMonitor.worked(1);
			readSounderAndAntennaAttributs(info);
			subMonitor.worked(1);
			readProjectionAndEllipsoid(info);
			subMonitor.worked(1);
			readBiasAndCorrections(info);
			subMonitor.worked(1);
			readShipAttributs(info);
			subMonitor.worked(1);
			readVRUOffsetAndDelay(info);
			subMonitor.worked(1);
			readHistory(info);
		} finally {
			info.close();
		}
		subMonitor.done();
	}

	/** Reads file attributes **/
	private void readFileAttributs(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			info.setMbgName(ncfile.getAttributeText(MbgConstants.Name));
			info.setVersion(ncfile.getAttributeInt(MbgConstants.Version));
			info.setMbgClass(ncfile.getAttributeText(MbgConstants.Classe));
			info.setLevel(ncfile.getAttributeShort(MbgConstants.Level));
			info.setMbgClass(ncfile.getAttributeText(MbgConstants.Classe));
			info.setTimeReference(ncfile.getAttributeText(MbgConstants.TimeReference));
			info.setCdi(ncfile.getAttributeText(MbgConstants.CDI));
		} catch (NCException e) {
			logger.error("MbgInfo reading: error file attributs: " + e.getMessage(), e);
		}
	}

	/** Reads Geobox **/
	private void readGeoBox(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			double north = ncfile.getAttributeDouble(MbgConstants.NorthLatitude);
			double south = ncfile.getAttributeDouble(MbgConstants.SouthLatitude);
			double east = ncfile.getAttributeDouble(MbgConstants.EastLongitude);
			double west = ncfile.getAttributeDouble(MbgConstants.WestLongitude);
			info.setGeoBox(new GeoBox(north, south, east, west));
		} catch (NCException e) {
			logger.error("MbgInfo reading: error with geobox: " + e.getMessage(), e);
		}
	}

	/** Reads min and max depth **/
	private void readMinMaxDepth(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			info.setMinDepth(-ncfile.getAttributeFloat(MbgConstants.MinDepth));
			info.setMaxDepth(-ncfile.getAttributeFloat(MbgConstants.MaxDepth));
		} catch (NCException e) {
			logger.error("MbgInfo reading: error with min/max depth: " + e.getMessage(), e);
		}
	}

	/** Reads first and last dates **/
	private void readFirsLastDate(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			int mbgDate = ncfile.getAttributeInt(MbgConstants.StartDate);
			int mbgTime = ncfile.getAttributeInt(MbgConstants.StartTime);
			info.setFirstPingDate(DateUtils.getDateFromJulian(mbgDate, mbgTime));

			mbgDate = ncfile.getAttributeInt(MbgConstants.EndDate);
			mbgTime = ncfile.getAttributeInt(MbgConstants.EndTime);
			info.setLastPingDate(DateUtils.getDateFromJulian(mbgDate, mbgTime));
		} catch (NCException e) {
			logger.error("MbgInfo reading: error with dates: " + e.getMessage(), e);
		}
	}

	/** Reads ship attributes **/
	private void readShipAttributs(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			info.setShipName(ncfile.getAttributeText(MbgConstants.Ship));
			info.setSurveyname(ncfile.getAttributeText(MbgConstants.Survey));
			info.setReferencePoint(ncfile.getAttributeText(MbgConstants.Reference));

			// Sounder model
			short sounderIndex = (short) ncfile.getAttributeInt(MbgConstants.Sounder);
			info.setEchoSounderModel(SoundersLibrary.get().getFirstSounderDescription(sounderIndex).getName(),
					sounderIndex);

		} catch (NCException e) {
			logger.error("MbgInfo reading: error ship attributs: " + e.getMessage(), e);
		}
	}

	/** Reads projection and ellipsoid attributes **/
	private void readProjectionAndEllipsoid(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			info.setMeridien180(ncfile.getAttributeText(MbgConstants.Meridian180));
			info.setGeodesicSystem(ncfile.getAttributeText(MbgConstants.GeodesicSystem));

			// Ellipsoid
			String ellipsName = ncfile.getAttributeText(MbgConstants.EllipsoidName);
			int indexOf;
			if ((indexOf = ellipsName.indexOf(' ')) != -1)
				ellipsName = ellipsName.substring(0, indexOf);
			info.setEllipsoidName(ellipsName);

			// Define ellipsoid from file
			Ellipsoid ellipsoid = Ellipsoid.get(ellipsName);
			if (ellipsoid == Ellipsoid.USER) {
				double a = ncfile.getAttributeDouble(MbgConstants.EllipsoidA);
				info.setEllipsoidA(a);
				double e2 = ncfile.getAttributeDouble(MbgConstants.EllipsoidE2);
				info.setEllipsoidE2(e2);
				double flatness = ncfile.getAttributeDouble(MbgConstants.EllipsoidInvF);
				info.setEllipsoidFlatness(flatness);
				info.setEllipsoidB(Ellipsoid.computeHalf_B(a, e2));
			} else {
				info.setEllipsoidA(ellipsoid.getHalf_A());
				info.setEllipsoidB(ellipsoid.getHalf_B());
				info.setEllipsoidFlatness(ellipsoid.getFlatness());
				info.setEllipsoidE2(ellipsoid.getEccentricity2());
			}

			// Projection
			info.setProjType((short) ncfile.getAttributeInt(MbgConstants.ProjType));
			info.setProjParameterValue(ncfile.getAttributeDoubleArray(MbgConstants.ProjParameterValue));
			info.setProjParameterCode(ncfile.getAttributeText(MbgConstants.ProjParameterCode));

		} catch (NCException e) {
			logger.error("MbgInfo reading: error with projection / ellipsoid: " + e.getMessage(), e);
		}
	}

	/** Reads bias and corrections **/
	private void readBiasAndCorrections(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			// Bias
			info.setHeadingBias(ncfile.getAttributeDouble(MbgConstants.HeadingBias));
			info.setRollBias(ncfile.getAttributeDouble(MbgConstants.RollBias));
			info.setPitchBias(ncfile.getAttributeDouble(MbgConstants.PitchBias));
			info.setHeaveBias(ncfile.getAttributeDouble(MbgConstants.HeaveBias));

			// Bias correction
			if(ncfile.hasAttribute(MbgConstants.BiasCorrectionRef))
				info.setBiasCorrectionRef(ncfile.getAttributeText(MbgConstants.BiasCorrectionRef));

			// Corrections
			info.setBiasCorrection((byte) ncfile.getAttributeChar(MbgConstants.BiasCorrection));
			info.setTideCorrection((byte) ncfile.getAttributeChar(MbgConstants.TideCorrection));
			info.setPositionCorrection((byte) ncfile.getAttributeChar(MbgConstants.PositionCorrection));
			info.setDraughtCorrection((byte) ncfile.getAttributeChar(MbgConstants.DraughtCorrection));
			info.setVelocityCorrection((byte) ncfile.getAttributeChar(MbgConstants.VelocityCorrection));
		
			// Reflectivity
			if (ncfile.hasAttribute(MbgConstants.ImReflectivityOrigin))
				info.setImReflecivityOrigin((byte) ncfile.getAttributeChar(MbgConstants.ImReflectivityOrigin));

			// Nav
			info.setNavType(ncfile.getAttributeShort(MbgConstants.NavType));
			info.setNavRef(ncfile.getAttributeText(MbgConstants.NavRef));

			// Tide & draught
			info.setTideType(ncfile.getAttributeShort(MbgConstants.TideType));
			info.setTideRef(ncfile.getAttributeText(MbgConstants.TideRef));
			info.setDraught(ncfile.getAttributeDouble(MbgConstants.Draught));

			// Cleaning
			info.setAutomaticCleaning((byte) ncfile.getAttributeChar(MbgConstants.AutomaticCleaning));
			info.setManualCleaning((byte) ncfile.getAttributeChar(MbgConstants.ManualCleaning));

		} catch (NCException e) {
			logger.error("MbgInfo reading: error bias and corrections attributes: " + e.getMessage(), e);
		}
	}

	/** Reads sounder and antenna attributes **/
	private void readSounderAndAntennaAttributs(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			// Sounder model
			short sounderIndex = (short) ncfile.getAttributeInt(MbgConstants.Sounder);
			SounderDescription sounderDesc = SoundersLibrary.get().getFirstSounderDescription(sounderIndex);
			info.setEchoSounderModel(sounderDesc.getName(), sounderIndex);
			info.setSounderOffset(ncfile.getAttributeDoubleArray(MbgConstants.SounderOffset));
			info.setSounderDelay(ncfile.getAttributeDouble(MbgConstants.SounderDelay));

			double[] antennaOffset = ncfile.getAttributeDoubleArray(MbgConstants.AntennaOffset);
			info.setShiftAlongNavigation(antennaOffset[0]);
			info.setShiftAcrossNavigation(antennaOffset[1]);
			info.setShiftVerticalNavigation(antennaOffset[2]);

			// installation parameters
			info.setRawInstallParameters(ncfile.getAttributeText(MbgConstants.InstallParameters).trim());

			// serial number
			int serialNumber = ncfile.getAttributeInt((MbgConstants.SerialNumber));
			var txAntennaId = Integer.toString(serialNumber);
			var txAntennaXOffset = 0d;
			var txAntennaYOffset = 0d;
			var txAntennaZOffset = 0d;
			var txAntennaRolloffset = 0d;
			var txAntennaPitchoffset = 0d;
			var txAntennaHeadingoffset = 0d;

			String installParamString = info.getRawInstallParameters();
			if (!installParamString.isEmpty()) {
				int modelNumber = sounderDesc.getConstructorModelID();

				// for mbg, the string is field in case of simrad files, we can then try to retrieve parameters back
				InstallationParameters param = new InstallationParameters();
				param.fromKString(installParamString, modelNumber, serialNumber,
						10000 + serialNumber /** secondary number not saved, compute an artificial one **/
				);

				txAntennaXOffset = param.getTxXOffset();
				txAntennaYOffset = param.getTxYOffset();
				txAntennaZOffset = param.getTxZOffset();
				txAntennaRolloffset = param.getTxRollOffset();
				txAntennaPitchoffset = param.getTxPitchOffset();
				txAntennaHeadingoffset = param.getTxHeadingOffset();

				// port antenna if two antennas or main antenna is only one
				var rxAntenna = new AntennaInstallationParameters(Type.RX, //
						param.getRxPortAntennaID(), //
						param.getRxPortXOffset(), //
						param.getRxPortYOffset(), //
						param.getRxPortZOffset(), //
						param.getRxPortRollOffset(), //
						param.getRxPortPitchOffset(), //
						param.getRxPortHeadingOffset());
				info.addAntenna(rxAntenna);

				int antennaRxCount = (int) ncfile.getDimension(MbgConstants.ANTENNA_NUMBER).getLength();
				if (antennaRxCount == 2) {
					var rxAntennaStarboard = new AntennaInstallationParameters(Type.RX, //
							param.getRxStarboardAntennaID(), //
							param.getRxStarboardXOffset(), //
							param.getRxStarboardYOffset(), //
							param.getRxStarboardZOffset(), //
							param.getRxStarboardRollOffset(), //
							param.getRxStarboardPitchOffset(), //
							param.getRxStarboardHeadingOffset());
					info.addAntenna(rxAntennaStarboard);
				}
			} else {
				// add default antenna matching the Antenna number count
				int antennaRxCount = (int) ncfile.getDimension(MbgConstants.ANTENNA_NUMBER).getLength();
				for (int rxAntenna = 0; rxAntenna < antennaRxCount; rxAntenna++) {
					AntennaInstallationParameters rxAntennat = new AntennaInstallationParameters(Type.RX,
							Integer.toString(rxAntenna), 0d, 0d, 0d, 0d, 0d, 0d);
					info.addAntenna(rxAntennat);
				}
			}

			if (ncfile.hasAttribute(MbgConstants.mbTxAntennaLeverArm)) {
				double[] sounderOffset = ncfile.getAttributeDoubleArray(MbgConstants.mbTxAntennaLeverArm);
				txAntennaXOffset = sounderOffset[0];
				txAntennaYOffset = sounderOffset[1];
				txAntennaZOffset = sounderOffset[2];
			}

			var txAntenna = new AntennaInstallationParameters(Type.TX, txAntennaId, //
					txAntennaXOffset, txAntennaYOffset, txAntennaZOffset, //
					txAntennaRolloffset, txAntennaPitchoffset, txAntennaHeadingoffset);
			info.addAntenna(txAntenna);

			info.setAntennaDelay(ncfile.getAttributeDouble(MbgConstants.AntennaDelay));

		} catch (NCException e) {
			logger.error("MbgInfo reading: error sounder / antenna  attributs: " + e.getMessage(), e);
		}
	}

	/** Reads VRU offset / delay **/
	private void readVRUOffsetAndDelay(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			info.setVRUOffset(ncfile.getAttributeDoubleArray(MbgConstants.VRUOffset));
			info.setVRUDelay(ncfile.getAttributeDouble(MbgConstants.VRUDelay));
		} catch (NCException e) {
			logger.error("MbgInfo reading: error with VRU Offset and delay : " + e.getMessage(), e);
		}
	}

	/** Reads dimensions from NetCDF input file */
	private void readDimensions(MbgInfo info) throws GIOException {
		NetcdfFile ncfile = info.getFile();
		try {
			if (ncfile.getDimension(MbgConstants.CYCLE_NUMBER) == null) {
				throw new GIOException("Bad file format");
			}
			info.setSwathCount((int) ncfile.getDimension(MbgConstants.CYCLE_NUMBER).getLength());
			info.setBeamCount((int) ncfile.getDimension(MbgConstants.BEAM_NUMBER).getLength());

			info.setVelocityProfileCount((int) ncfile.getDimension(MbgConstants.VELOCITY_PROFILE_NUMBER).getLength());

			// Antennas
			/*
			 * int antennaCount = (int) ncfile.getDimension(MbgConstants.ANTENNA_NUMBER).getLength(); ByteBuffer bb =
			 * ByteBuffer.allocate(antennaCount * DataType.INT.getSize()).order(ByteOrder.nativeOrder());
			 * ncfile.getVariable(MbgConstants.Beam).read(new long[] { antennaCount }, bb, DataType.INT,
			 * Optional.empty()); bb.rewind(); for (Integer i = 0; i < antennaCount; i++) { int beamCount = bb.getInt();
			 * // TODO 28/08/20 //info.pushRxAntenna(new NamedAntennaId(i + 1), beamCount); //info.pushTxAntenna(new
			 * NamedAntennaId(i + 1), beamCount); }
			 */
		} catch (NCException e) {
			logger.error("MbgInfo reading: error with dimensions: " + e.getMessage(), e);
		}
	}

	/** Reads dimensions from NetCDF input file */
	private void readHistory(MbgInfo info) {
		NetcdfFile ncfile = info.getFile();
		try {
			short nistoryRecNb = ncfile.getAttributeShort(MbgConstants.NbrHistoryRec);
			info.setNbHistoryRec(nistoryRecNb);

			NetcdfVariable dateVariable = ncfile.getVariable(MbgConstants.HistDate);
			ByteBuffer dateBuffer = ByteBuffer.allocate(nistoryRecNb * DataType.INT.getSize())
					.order(ByteOrder.nativeOrder());
			dateVariable.read(new long[] { nistoryRecNb }, dateBuffer, DataType.INT, Optional.empty());

			NetcdfVariable timeVariable = ncfile.getVariable(MbgConstants.HistTime);
			ByteBuffer timeBuffer = ByteBuffer.allocate(nistoryRecNb * DataType.INT.getSize())
					.order(ByteOrder.nativeOrder());
			timeVariable.read(new long[] { nistoryRecNb }, timeBuffer, DataType.INT, Optional.empty());

			int nameLength = (int) ncfile.getDimension(MbgConstants.NameLength).getLength();
			NetcdfVariable authorVariable = ncfile.getVariable(MbgConstants.HistAutor);
			ByteBuffer autorBuffer = ByteBuffer.allocate(nistoryRecNb * nameLength * DataType.CHAR.getSize())
					.order(ByteOrder.nativeOrder());
			authorVariable.read(new long[] { nistoryRecNb, nameLength }, autorBuffer, DataType.CHAR, Optional.empty());

			StringBuilder sb = new StringBuilder();
			int date = dateBuffer.getInt(nistoryRecNb - 1);
			int time = timeBuffer.getInt(nistoryRecNb - 1);
			sb.append(DateUtils.getDateFromJulian(date, time));
			sb.append(",\t");
			String autor = StandardCharsets.UTF_8.decode(autorBuffer).toString();
			sb.append(autor);
			sb.append("\n");

			info.setHistory(sb.toString());
		} catch (NCException e) {
			logger.error("MbgInfo reading: error with history: " + e.getMessage(), e);
		}
	}
}
