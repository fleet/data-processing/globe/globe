package fr.ifremer.globe.core.io.mbg.datacontainer;


/**
 * Class holding enum validity flags definitions for MBG
 * */
public interface MbgValidityConstants {

	// Ping flag values
	public static byte PNG_UNVALID_ACQUIS = -3; // unvalid at acquisition
	public static byte PNG_UNVALID_AUTO = -2; // unvalidated by automatic
	public static byte PNG_UNVALID = -1; // unvalidated by operator
	public static byte PNG_MISSING = 0; // missing
	public static byte PNG_VALID = 2; // valid
	public static byte PNG_UNVALID_VALIDATED = 4; // unvalid validated
	public static byte PNG_MOVED = 5; // modified

	// Antenna flag values
	public static byte ANT_UNVALID = -1; // unvalid
	public static byte ANT_MISSING = 0; // missing
	public static byte ANT_VALID = 1; // valid

	// Beam flag values
	public static byte BEA_UNVALID = -1; // unvalid
	public static byte BEA_MISSING = 0; // missing
	public static byte BEA_VALID = 2; // valid


	public enum SoundingValidity
	{
		SND_UNVALID_ACQUIS(-3), // unvalid at acquisition
		SND_UNVALID_AUTO(-2),// unvalidated by automatic
		SND_UNVALID(-1),// unvalidated by operator
		SND_MISSING(0),// missing
		SND_DOUBTFUL(1),// doubtful (temporary state) (not used reserved for stuff like cube or esa)
		SND_VALID(2);// valid
		private byte value;
		private SoundingValidity(int value)
		{
			this.value=(byte)value;
		}
		/**
		 * return the byte value for the given enum
		 * */
		public byte getValue() {
			return value;
		}

		private static SoundingValidity continuousValues[]; //Values stored by index
		private static  int minValue=Byte.MAX_VALUE;

		static {
			int max=Byte.MIN_VALUE;
			for(SoundingValidity instance:SoundingValidity.values())
			{
				minValue=Math.min(instance.getValue(),minValue);
				max=Math.max(instance.getValue(),max);
			}

			continuousValues=new SoundingValidity[max-minValue+1];
			for(SoundingValidity instance:SoundingValidity.values())
			{
				continuousValues[instance.getValue()-minValue]=instance;
			}
		}
		/**
		 * return the matching enum given its byte value
		 * */
		public static SoundingValidity get(byte value)
		{
			if(value<minValue) return SND_MISSING;
			if(value-minValue>continuousValues.length-1) return SoundingValidity.SND_MISSING;
			return continuousValues[value-minValue];
		}

		public static boolean isValid(byte value)
		{
			return value >=SND_DOUBTFUL.getValue();
		}
		public static boolean isMissing(byte value)
		{
			return value == SND_MISSING.getValue();
		}
	}
}
