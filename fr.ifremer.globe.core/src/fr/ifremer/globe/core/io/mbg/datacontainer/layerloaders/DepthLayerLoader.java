package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.utils.exception.GIOException;

public class DepthLayerLoader implements ILayerLoader<FloatBuffer2D> {

	private final long[] shape;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	private final SounderDataContainer sdc;

	/**
	 * Constructor
	 */
	public DepthLayerLoader(SounderDataContainer sounderDataContainer) throws GIOException {
		this.sdc = sounderDataContainer;
		this.shape = sdc.getLayer(MbgPredefinedLayers.DEPTH).getLoader().getDimensions();
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * Load the depth layer with the mbg depth, tide, dynamic draught and tx_z_depth
	 * 
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(FloatBuffer2D buffer) throws GIOException {
		DoubleLoadableLayer2D mbgDepth = sdc.getLayer(MbgPredefinedLayers.DEPTH);
		FloatLoadableLayer1D platformVertOffsetLayer = sdc.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

		FloatLoadableLayer1D waterlineLayer = sdc.getLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);

		if (loading.compareAndSet(false, true)) {
			for (int iSwath = 0; iSwath < shape[0]; iSwath++) {
				float tide = waterlineLayer.get(iSwath);
				float platformVertOffset = platformVertOffsetLayer.get(iSwath);
				for (int iBeam = 0; iBeam < shape[1]; iBeam++) {
					// "model_depth = depth_fcs + tide + platform_vert_offset"
					double depthScs = mbgDepth.get(iSwath, iBeam) + platformVertOffset + tide;
					buffer.set(iSwath, iBeam, (float) depthScs);
				}
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	/**
	 * Flush the mbg depth layer with values from the suitable coordinate system
	 * 
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean flush(FloatBuffer2D buffer) throws GIOException {

		DoubleLoadableLayer2D mbgFcsLayer = sdc.getLayer(MbgPredefinedLayers.DEPTH);
		DoubleLayer2D modelFcsLayer = sdc.getCsLayers(CoordinateSystem.FCS).getProjectedZ();

		for (int iSwath = 0; iSwath < shape[0]; iSwath++)
			for (int iBeam = 0; iBeam < shape[1]; iBeam++)
				mbgFcsLayer.set(iSwath, iBeam, modelFcsLayer.get(iSwath, iBeam));

		mbgFcsLayer.flush();
		return false; // return false: we want to keep this datagram "dirty" (because the depth could be modified only
						// in fixed coordinate system (see heave bias correction), the flush is systematically done)
	}

}
