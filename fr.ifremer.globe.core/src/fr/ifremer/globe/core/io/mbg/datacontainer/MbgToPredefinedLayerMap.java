package fr.ifremer.globe.core.io.mbg.datacontainer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.bathymetry.VendorSpecificLayers;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;

/**
 * This class binds variables to predefined layers.
 *
 */
public class MbgToPredefinedLayerMap {

	private MbgToPredefinedLayerMap() {
	}

	private static final Map<String, PredefinedLayers<?>> map = new HashMap<>();

	/**
	 * @return an optional which contains the predefined layer bound to the specified MBG path, or an empty optional if
	 *         no predefined layer has been found.
	 */
	public static Optional<PredefinedLayers> providePredefinedLayer(NetcdfVariable variable) {
		return Optional.ofNullable(map.get(variable.getPath()));
	}

	static {

		/** MBG variables directly mapped with model layers **/

		// map.put("/mbReflectivity", BathymetryLayers.DETECTION_BACKSCATTER);
		map.put("/mbSQuality", VendorSpecificLayers.DETECTION_CONSTRUCTOR_QUALITY_FACTOR);
		map.put("/mbQuality", BathymetryLayers.DETECTION_QUALITY_FACTOR);

		/** MBG Layers **/

		map.put("/mbRange", MbgPredefinedLayers.RANGE);
		map.put("/mbVelProfilDate", MbgPredefinedLayers.VEL_PROFIL_DATE);
		map.put("/mbTransmissionHeave", MbgPredefinedLayers.TRANSMISSION_HEAVE);
		map.put("/mbFrequency", MbgPredefinedLayers.FREQUENCY_PLANE);
		map.put("/mbVelProfilIdx", MbgPredefinedLayers.VEL_PROFIL_IDX);
		map.put("/mbSonarFrequency", MbgPredefinedLayers.SONAR_FREQUENCY);
		map.put("/mbTransmitPowerReMax", MbgPredefinedLayers.TRANSMIT_POWER_RE_MAX);
		map.put("/mbAlongSlope", MbgPredefinedLayers.ALONG_SLOPE);
		map.put("/mbAbsorptionCoefficient", MbgPredefinedLayers.ABSORPTION_COEFFICIENT);
		map.put("/mbMaxStarboardWidth", MbgPredefinedLayers.MAX_STARBOARD_WIDTH);
		map.put("/mbAlongDistance", MbgPredefinedLayers.ALONG_DISTANCE);
		map.put("/mbTime", MbgPredefinedLayers.CYCLE_TIME);
		map.put("/mbReceiveBeamwidth", MbgPredefinedLayers.RECEIVE_BEAMWIDTH);
		map.put("/mbDistanceScale", MbgPredefinedLayers.DISTANCE_SCALE);
		map.put("/mbSamplingRate", MbgPredefinedLayers.SAMPLING_RATE);
		map.put("/mbTransmitBeamwidth", MbgPredefinedLayers.TRANSMIT_BEAM_WIDTH);
		map.put("/mbHistCode", MbgPredefinedLayers.HIST_CODE);
		map.put("/mbCompensationLayerMode", MbgPredefinedLayers.COMPENSATION_LAYER_MODE);
		map.put("/mbDepth", MbgPredefinedLayers.DEPTH);
		map.put("/mbReflectivity", MbgPredefinedLayers.REFLECTIVITY);
		map.put("/mbSFlag", MbgPredefinedLayers.SOUNDING_FLAG);
		map.put("/mbTransmitPulseLength", MbgPredefinedLayers.TRANSMIT_PULSE_LENGTH);
		map.put("/mbAcrossDistance", MbgPredefinedLayers.ACROSS_DISTANCE);
		map.put("/mbReceiveBandwidth", MbgPredefinedLayers.RECEIVE_BANDWIDTH);
		map.put("/mbAFlag", MbgPredefinedLayers.ANTENNA_FLAG);
		map.put("/mbHistModule", MbgPredefinedLayers.HIST_MODULE);
		map.put("/mbHiLoAbsorptionRatio", MbgPredefinedLayers.HILO_ABSORPTION_RATIO);
		map.put("/mbVelProfilRef", MbgPredefinedLayers.VEL_PROFIL_REF);
		map.put("/mbReferenceDepth", MbgPredefinedLayers.REFERENCE_DEPTH);
		map.put("/mbAzimutBeamAngle", MbgPredefinedLayers.AZIMUT_ANGLE);
		map.put("/mbBSPStatus", MbgPredefinedLayers.BSP_STATUS);
		map.put("/mbSoundingBias", MbgPredefinedLayers.SOUNDING_BIAS);
		map.put("/mbAntenna", MbgPredefinedLayers.ANTENNA_INDEX);
		map.put("/mbRoll", MbgPredefinedLayers.ROLL);
		map.put("/mbCQuality", MbgPredefinedLayers.CYCLE_QUALITY_FACTOR);
		map.put("/mbSoundVelocity", MbgPredefinedLayers.SOUND_VELOCITY);
		map.put("/mbHistTime", MbgPredefinedLayers.HIST_TIME);
		map.put("/mbParamMinimumDepth", MbgPredefinedLayers.PARAM_MIN_DEPTH);
		map.put("/mbCFlag", MbgPredefinedLayers.CYCLE_FLAG);
		map.put("/mbFilterIdentifier", MbgPredefinedLayers.FILTER_IDENTIFIER);
		map.put("/mbDurotongSpeed", MbgPredefinedLayers.DUROTONG_SPEED);
		map.put("/mbSLengthOfDetection", MbgPredefinedLayers.S_LENGTH_OF_DETECTION);
		map.put("/mbHistAutor", MbgPredefinedLayers.HIST_AUTOR);
		map.put("/mbTVGLawCrossoverAngle", MbgPredefinedLayers.TVG_LAW_CROSSOVER_ANGLE);
		map.put("/mbMaxStarboardCoverage", MbgPredefinedLayers.MAX_STARBOARD_COVERAGE);
		map.put("/mbAcrossSlope", MbgPredefinedLayers.ACROSS_SLOPE);
		map.put("/mbOrdinate", MbgPredefinedLayers.LATITUDE);
		map.put("/mbHeading", MbgPredefinedLayers.HEADING_SHIP);
		map.put("/mbInterlacing", MbgPredefinedLayers.INTERLACING);
		map.put("/mbMaxPortCoverage", MbgPredefinedLayers.MAX_PORT_COVERAGE);
		map.put("/mbBFlag", MbgPredefinedLayers.BEAM_FLAG);
		map.put("/mbProcessingUnitStatus", MbgPredefinedLayers.PROCESSING_UNIT_STATUS);
		map.put("/mbHistDate", MbgPredefinedLayers.HIST_DATE);
		map.put("/mbBeam", MbgPredefinedLayers.BEAM_NUMBER);
		map.put("/mbMaxPortWidth", MbgPredefinedLayers.MAX_PORT_WIDTH);
		map.put("/mbDate", MbgPredefinedLayers.CYCLE_DATE);
		map.put("/mbReceptionHeave", MbgPredefinedLayers.RECEPTION_HEAVE);
		map.put("/mbHistComment", MbgPredefinedLayers.HIST_COMMENT);
		map.put("/mbVelProfilTime", MbgPredefinedLayers.VEL_PROFIL_TIME);
		map.put("/mbBeamSpacing", MbgPredefinedLayers.BEAM_SPACING);
		map.put("/mbSounderMode", MbgPredefinedLayers.SOUNDER_MODE);
		map.put("/mbTransVelocitySource", MbgPredefinedLayers.TRANSDUCER_VELOCITY_SOURCE);
		map.put("/mbAcrossBeamAngle", MbgPredefinedLayers.ACROSS_ANGLE);
		map.put("/mbDynamicDraught", MbgPredefinedLayers.DYNAMIC_DRAUGHT);
		map.put("/mbTide", MbgPredefinedLayers.TIDE);
		map.put("/mbVerticalDepth", MbgPredefinedLayers.VERTICAL_DEPTH);
		map.put("/mbPitch", MbgPredefinedLayers.PITCH);
		map.put("/mbReceiverFixedGain", MbgPredefinedLayers.RECEIVER_FIXED_GAIN);
		map.put("/mbYawPitchStabMode", MbgPredefinedLayers.YAW_PITCH_STAB_MODE);
		map.put("/mbAbscissa", MbgPredefinedLayers.LONGITUDE);
		map.put("/mbOperatorStationStatus", MbgPredefinedLayers.OPERATOR_STATION_STATUS);
		map.put("/mbParamMaximumDepth", MbgPredefinedLayers.PARAM_MAX_DEPTH);
		map.put("/mbCycle", MbgPredefinedLayers.CYCLE_NUMBER);
		map.put("/mbSonarStatus", MbgPredefinedLayers.SONAR_STATUS);
	}

}
