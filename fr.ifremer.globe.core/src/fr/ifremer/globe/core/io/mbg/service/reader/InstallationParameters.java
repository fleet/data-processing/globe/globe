package fr.ifremer.globe.core.io.mbg.service.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.IConstants;

class InstallationParameters {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstallationParameters.class);

	int secondaryserialNumber = 0;

	double txXOffset = 0;
	double txYOffset = 0;
	double txZOffset = 0;

	double txRollOffset = 0;
	double txPitchOffset = 0;
	double txHeadingOffset = 0;

	double rxStarboardXOffset = 0;
	double rxStarboardYOffset = 0;
	double rxStarboardZOffset = 0;

	double rxPortXOffset = 0;
	double rxPortYOffset = 0;
	double rxPortZOffset = 0;

	double rxStarboardRollOffset = 0;
	double rxStarboardPitchOffset = 0;
	double rxStarboardHeadingOffset = 0;

	double rxPortRollOffset = 0;
	double rxPortPitchOffset = 0;
	double rxPortHeadingOffset = 0;

	String rxPortAntennaID = null;
	String rxStarboardAntennaID = null;
	String txAntennaID = null;

	Short SISVersion = 0;

	int signOfAngles = -1;
	byte layerCompensationFlag = IConstants.COMPENSATION_LAYER_MODE_ACTIVE;

	public int portSerialNumber;

	private int modelNumber;

	public int getSecondaryserialNumber() {
		return secondaryserialNumber;
	}

	public double getTxXOffset() {
		return txXOffset;
	}

	public double getTxYOffset() {
		return txYOffset;
	}

	public double getTxZOffset() {
		return txZOffset;
	}

	public double getTxRollOffset() {
		return txRollOffset;
	}

	public double getTxPitchOffset() {
		return txPitchOffset;
	}

	public double getTxHeadingOffset() {
		return txHeadingOffset;
	}

	public double getRxStarboardXOffset() {
		return rxStarboardXOffset;
	}

	public double getRxStarboardYOffset() {
		return rxStarboardYOffset;
	}

	public double getRxStarboardZOffset() {
		return rxStarboardZOffset;
	}

	public double getRxPortXOffset() {
		return rxPortXOffset;
	}

	public double getRxPortYOffset() {
		return rxPortYOffset;
	}

	public double getRxPortZOffset() {
		return rxPortZOffset;
	}

	public double getRxStarboardRollOffset() {
		return rxStarboardRollOffset;
	}

	public double getRxStarboardPitchOffset() {
		return rxStarboardPitchOffset;
	}

	public double getRxStarboardHeadingOffset() {
		return rxStarboardHeadingOffset;
	}

	public double getRxPortRollOffset() {
		return rxPortRollOffset;
	}

	public double getRxPortPitchOffset() {
		return rxPortPitchOffset;
	}

	public double getRxPortHeadingOffset() {
		return rxPortHeadingOffset;
	}

	public String getRxPortAntennaID() {
		return rxPortAntennaID;
	}

	public String getRxStarboardAntennaID() {
		return rxStarboardAntennaID;
	}

	public String getTxAntennaID() {
		return txAntennaID;
	}

	public Short getSISVersion() {
		return SISVersion;
	}

	public int getSignOfAngles() {
		return signOfAngles;
	}

	public byte getLayerCompensationFlag() {
		return layerCompensationFlag;
	}

	/**
	 * return the antenna roll installation offset given the antenna serial ID
	 */
	public double getRollOffset(int serialNumber) {

		if (serialNumber == getPortSerialNumber()) {

			return getRxPortRollOffset();
		} else if (serialNumber == getStarboadSerialNumber()) {

			return getRxStarboardRollOffset();
		} else {
			LOGGER.error("Simrad installation parameter : unknown antenna " + serialNumber);
			return 0;
		}
	}

	public int getPortSerialNumber() {
		return portSerialNumber;
	}

	public int getStarboadSerialNumber() {
		return getSecondaryserialNumber();
	}

	/**
	 * decode and fill installation parameters from a kongsberg formatted string
	 */
	public void fromKString(String parametersString, int modelNumber, int portSerialNumber, int startBoadSerialNumber) {
		this.portSerialNumber = portSerialNumber;
		secondaryserialNumber = startBoadSerialNumber;
		// set default values as they can be not defined in installation parameters string
		txAntennaID = Integer.toString(portSerialNumber);
		rxPortAntennaID = Integer.toString(portSerialNumber);
		rxStarboardAntennaID = Integer.toString(startBoadSerialNumber);

		this.modelNumber = modelNumber;
		String[] parametersList = parametersString.split(",");
		// process the data to find and interpret usefull datagram

		for (String param : parametersList) {

			// ajout offset antenne emission

			if (param.startsWith("S1X")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					txXOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S1Y")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					txYOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S1Z")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					txZOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S1R")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					txRollOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S1P")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					txPitchOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S1H")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					txHeadingOffset = Double.parseDouble(data[1]);
					if (txHeadingOffset > 170. && txHeadingOffset < 190.)
						signOfAngles = 1;
				}
			}

			// ajout offset antenne babord

			// find port roll offset, this start with S2R=
			if (param.startsWith("S2R")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxPortRollOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S2X")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxPortXOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S2Y")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxPortYOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S2Z")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxPortZOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S2P")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxPortPitchOffset = Double.parseDouble(data[1]);
				}
			}

			/*
			 * else if (param.startsWith("S2H")) { String[] data = param.split("="); if (data.length > 1) { //
			 * Transducer heading in degrees double starboardAntennaHeading = Double.parseDouble(data[1]); // If the
			 * transducer heading is around 180 the sign of angles must not be reversed (EM2040 Europe) if
			 * (starboardAntennaHeading > 170. && starboardAntennaHeading < 190.) signOfAngles = 1; }
			 */
			// SHC doesn't exist always

			// changer le code car confusion entre starboard et port !!!! (ancien code juste au dessus en commentaire)
			else if (param.startsWith("S2H")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxPortHeadingOffset = Double.parseDouble(data[1]);
					// If the transducer heading is around 180 the sign of angles must not be reversed (EM2040 Europe)
					if (rxPortHeadingOffset > 170. && rxPortHeadingOffset < 190.)
						signOfAngles = 1;
				}
			}

			// ajout offset antenne tribord

			else if (param.startsWith("S3X")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxStarboardXOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S3Y")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxStarboardYOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S3Z")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxStarboardZOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S3R")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxStarboardRollOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S3P")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxStarboardPitchOffset = Double.parseDouble(data[1]);
				}
			}

			else if (param.startsWith("S3H")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxStarboardHeadingOffset = Double.parseDouble(data[1]);
					if (rxStarboardHeadingOffset > 170. && rxStarboardHeadingOffset < 190.)
						signOfAngles = 1;
				}
			}

			// autre

			else if (param.startsWith("OSV")) {
				if (param.contains("SIS")) {
					String[] data = param.split("SIS ");
					if (data.length > 1) {
						try {
							SISVersion = Short.valueOf(data[1].replaceAll("\\.", ""));
						} catch (NumberFormatException e) {
							SISVersion = -1;
						}
					}
				} else {
					String[] data = param.split("=");
					if (data.length > 1) {
						try {
							SISVersion = Short.valueOf((data[1].split(" "))[0].replaceAll("\\.", ""));
						} catch (NumberFormatException e) {
							SISVersion = -1;
						}
					}
				}
			} else if (param.startsWith("SHC")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					byte value = Byte.parseByte(data[1]);
					if (value == 0) {
						layerCompensationFlag = IConstants.COMPENSATION_LAYER_MODE_ACTIVE;
					} else if (value == 1) {
						layerCompensationFlag = IConstants.COMPENSATION_LAYER_MODE_INACTIVE;
					}
				}

			}

			else if (param.startsWith("TXS")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					txAntennaID = data[1];
				}
			}

			else if (param.startsWith("R1S")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxPortAntennaID = data[1];
				}
			}

			else if (param.startsWith("R2S")) {
				String[] data = param.split("=");
				if (data.length > 1) {
					rxStarboardAntennaID = data[1];
				}
			}

		}
		if (modelNumber == 850) // ME70 , only one transducer for emission an reception
		{
			rxPortHeadingOffset = txHeadingOffset;
			rxPortPitchOffset = txPitchOffset;
			rxPortRollOffset = txRollOffset;
			rxPortXOffset = txXOffset;
			rxPortYOffset = txYOffset;
			rxPortZOffset = txZOffset;
		}

	}

	public int getModelNumber() {
		return modelNumber;
	}
}
