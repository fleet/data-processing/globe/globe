/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ShortBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Computes the detection_rx_antenna layer of the sounder data container
 */
public class DetectionRxTransducerIndexLayerLoader implements ILayerLoader<ShortBuffer2D> {

	/** Properties **/
	private final long[] shape;
	private final SounderDataContainer sdc;
	private final AtomicBoolean loading = new AtomicBoolean(false);

	/**
	 * Constructor
	 */
	public DetectionRxTransducerIndexLayerLoader(SounderDataContainer sounderDataContainer) {
		MbgInfo mbgInfo = (MbgInfo) sounderDataContainer.getInfo();
		this.shape = new long[] { mbgInfo.getCycleCount(), mbgInfo.getTotalRxBeamCount() };
		this.sdc = sounderDataContainer;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(ShortBuffer2D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			IntLoadableLayer1D mbgAntenna = sdc.getLayer(MbgPredefinedLayers.ANTENNA_INDEX);
			for (int iSwath = 0; iSwath < shape[0]; iSwath++) {
				for (int iBeam = 0; iBeam < shape[1]; iBeam++) {
					buffer.set(iSwath, iBeam, (short) mbgAntenna.get(iBeam));
				}
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean flush(ShortBuffer2D buffer) throws GIOException {
		return false;
	}

}
