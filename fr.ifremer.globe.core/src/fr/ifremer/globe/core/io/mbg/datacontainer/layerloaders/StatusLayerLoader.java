package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.io.mbg.datacontainer.MbgValidityConstants.SoundingValidity;
import fr.ifremer.globe.core.io.mbg.datacontainer.ValidityMapper;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.ByteBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.StatusPair;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Manages status layers and validity flags
 */
public class StatusLayerLoader implements ILayerLoader<ByteBuffer2D> {

	/** Properties **/
	private final long[] shape;
	private final AtomicBoolean loading = new AtomicBoolean(false);
	private final AtomicBoolean writing = new AtomicBoolean(false);
	private final MbgInfo mbgInfo;
	private final SounderDataContainer sounderDataContainer;
	private final IntLoadableLayer2D mbgSoundingFlag;
	private final IntLoadableLayer2D mbgCycleValidityLayer;
	private final IntLoadableLayer1D mbgAntennaValidityLayer;
	private final IntLoadableLayer1D mbgBeamValidityLayer;
	private final IntLoadableLayer1D mbgBeamAntennaIdxLayer;

	/** Constructor */
	public StatusLayerLoader(SounderDataContainer sounderDataContainer) throws GIOException {
		mbgInfo = (MbgInfo) sounderDataContainer.getInfo();
		this.sounderDataContainer = sounderDataContainer;
		mbgAntennaValidityLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.ANTENNA_FLAG);
		mbgBeamValidityLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.BEAM_FLAG);
		mbgBeamAntennaIdxLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.ANTENNA_INDEX);
		mbgCycleValidityLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.CYCLE_FLAG);
		mbgSoundingFlag = sounderDataContainer.getLayer(MbgPredefinedLayers.SOUNDING_FLAG);
		shape = new long[] { mbgInfo.getCycleCount(), mbgInfo.getTotalRxBeamCount() };
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(ByteBuffer2D buffer) throws GIOException {
		// Ensure that the process loading is done only one time
		if (loading.compareAndSet(false, true)) {
			ByteLoadableLayer2D statusLayer = sounderDataContainer.getLayer(BathymetryLayers.STATUS);
			ByteLoadableLayer2D statusLayerDetails = sounderDataContainer.getLayer(BathymetryLayers.STATUS_DETAIL);

			StatusPair statusPair = new StatusPair((byte) 0, (byte) 0);
			for (int iSwath = 0; iSwath < shape[0]; iSwath++) {
				for (int iBeam = 0; iBeam < shape[1]; iBeam++) {

					// Get mbg values
					int antennaIdx = mbgBeamAntennaIdxLayer.get(iBeam);
					int pingFlag = mbgCycleValidityLayer.get(iSwath, antennaIdx);
					int beamFlag = mbgBeamValidityLayer.get(iBeam);
					int antennaFlag = mbgAntennaValidityLayer.get(antennaIdx);
					SoundingValidity mbgSoundValidity = SoundingValidity
							.get((byte) (mbgSoundingFlag.get(iSwath, iBeam)));

					// Set model status value
					ValidityMapper.getStatus(pingFlag, beamFlag, antennaFlag, mbgSoundValidity, statusPair);
					statusLayer.set(iSwath, iBeam, statusPair.getStatus());
					statusLayerDetails.set(iSwath, iBeam, statusPair.getDetail());
				}
			}
			loading.set(false);
			buffer.setDirty(false);

		}
		return true;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 *
	 *      N.B. : antenna flag is not flushed (because not managed by the model)
	 */
	@Override
	public boolean flush(ByteBuffer2D buffer) throws GIOException {
		// Ensure that the flush process is done only one time
		if (writing.compareAndSet(false, true)) {
			ByteLoadableLayer2D statusLayer = sounderDataContainer.getLayer(BathymetryLayers.STATUS);
			ByteLoadableLayer2D statusLayerDetails = sounderDataContainer.getLayer(BathymetryLayers.STATUS_DETAIL);

			for (int iSwath = 0; iSwath < shape[0]; iSwath++) {
				for (int iBeam = 0; iBeam < shape[1]; iBeam++) {
					byte status = statusLayer.get(iSwath, iBeam);
					byte statusDetail = statusLayerDetails.get(iSwath, iBeam);
					int antennaIdx = mbgBeamAntennaIdxLayer.get(iBeam);

					// Cycle flag
					if (iBeam == 0) // cycle checking can be only done in one beam
						mbgCycleValidityLayer.set(iSwath, antennaIdx, ValidityMapper.getPingValidity(status));

					// Beam flag
					if (iSwath == 0) // beam checking can be only done with one swath
						mbgBeamValidityLayer.set(iBeam, ValidityMapper.getDetectionBeamValidity(status));

					// Sounding flag (we consider that missing sounding are never modified)
					SoundingValidity mbgValidity = ValidityMapper.getSoundingValidity(status, statusDetail);
					if (mbgValidity != SoundingValidity.SND_MISSING)
						mbgSoundingFlag.set(iSwath, iBeam, mbgValidity.getValue());
				}
			}

			mbgSoundingFlag.flush();
			statusLayer.getData().setDirty(false);
			statusLayerDetails.getData().setDirty(false);
			writing.set(false);
		}
		return true;
	}

}
