package fr.ifremer.globe.core.io.mbg.info;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.NullProgressMonitor;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgUtils;
import fr.ifremer.globe.core.io.mbg.datacontainer.MbgLayerDeclarer;
import fr.ifremer.globe.core.io.mbg.datacontainer.MbgOptionalLayersAccessor;
import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.io.mbg.datacontainer.MbgToPredefinedLayerMap;
import fr.ifremer.globe.core.io.mbg.service.reader.MbgInfoReader;
import fr.ifremer.globe.core.io.mbg.writer.MbgProcessAssessor;
import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.SensorInstallationParameters;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.globe.utils.sounders.SounderDescription;
import fr.ifremer.globe.utils.sounders.SoundersLibrary;

/**
 * MbgInfo: this class provides informations about an MBG file.
 */
public class MbgInfo implements ISounderNcInfo {

	/** file properties **/
	private final String filePath;
	private final AtomicBoolean isOpen = new AtomicBoolean();
	private NetcdfFile file;

	/** MBG properties **/
	private GeoBox geobox;
	private String sounderModel;
	private short sounderIndex;
	private int swathCount;
	private int beamCount;
	private Date firstSwathDate;
	private Date lastSwathDate;
	private double minDepth;
	private double maxDepth;
	private ArrayList<AntennaInstallationParameters> antennas = new ArrayList<>();

	private int mbgVersion = -1;
	private String mbgName = "";
	private String mbgClass = "";
	private short level = -1;
	private short nbHistoryRec = -1;
	private String timeReference = "";
	private String meridien180 = "";
	private String geodesicSystem = "";
	private String ellipsoidName = "";
	private double ellipsoidA = -1;
	private double ellipsoidB = -1;
	private double ellipsoidE2 = -1;
	private double ellipsoidFlatness = -1;
	private short projType = -1;
	private double[] projParameterValue = { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };
	private String projParameterCode = " ";
	private String shipName = "None";
	private String surveyname = "None";
	private String referencePoint = "None";
	private String cdi = "None";
	private double shiftAlongNavigation = 999999.0;
	private double shiftAcrossNavigation = 999999.0;
	private double shiftVerticalNavigation = 999999.0;
	private double antennaDelay = 999999;
	private double[] sounderOffset = { 999999., 999999., 999999. };
	private double sounderDelay = 999999.0;
	private double[] VRUOffset = { 999999., 999999., 999999. };
	private double VRUDelay = 999999.;
	private short navType = 9999;
	private String navRef = "None";
	private short tideType = 0;
	private String tideRef = "None";
	private String biasCorrectionRef = "None";
	private double headingBias = 999999.;
	private double rollBias = 999999.;
	private double pitchBias = 999999.;
	private double heaveBias = 999999.;
	private double draught = 999999.;
	private byte automaticCleaning = 0;
	private byte manualCleaning = 0;
	private byte velocityCorrection = 0;
	private byte biasCorrection = 0;
	private byte tideCorrection = 0;
	private byte positionCorrection = 0;
	private byte draughtCorrection = 0;
	private byte imReflecivityOrigin = 0;
	private String rawInstallParameters;
	private int velocityProfileCount;
	private String history;

	/**
	 * Protected as this constructor shouldn't be called directly.
	 */
	public MbgInfo(String filePath) throws GIOException {
		this.filePath = filePath;
		new MbgInfoReader().readFile(this, new NullProgressMonitor());
	}

	/** @return {@link AtomicBoolean} which defined file opening state **/
	protected AtomicBoolean isOpen() {
		return isOpen;
	}

	@Override
	public void open(boolean readOnly) throws GIOException {
		try {
			if (isOpen().compareAndSet(false, true)) {
				setFile(NetcdfFile.open(getFilePath(), readOnly ? Mode.readonly : Mode.readwrite));
			}
		} catch (NCException e) {
			isOpen.set(false);
			throw new GIOException("Error while opening MBG File", e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void close() {
		if (!IDataContainerFactory.grab().isBooked(this) && isOpen.compareAndSet(true, false)) {
			file.close();
		}
	}

	/**
	 * Declares layers
	 */
	@Override
	public void declareLayers(SounderDataContainer container) throws GException {
		NetcdfLayerDeclarer netcdfLayerDeclarer = new NetcdfLayerDeclarer();
		netcdfLayerDeclarer.declareLayers(file, container, group -> MbgPredefinedLayers.GROUP,
				MbgToPredefinedLayerMap::providePredefinedLayer);
		MbgLayerDeclarer mbgLayerDeclarer = new MbgLayerDeclarer();
		mbgLayerDeclarer.declareLayers(this, container);
	}

	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = new ArrayList<>();
		properties.add(Property.build("Ship name", shipName));
		properties.add(Property.build("Survey name", surveyname));
		properties.add(Property.build(IConstants.EchoSounderModel, sounderModel));
		properties.add(Property.build("Mbg version", Integer.toString(mbgVersion)));

		properties.add(Property.build("Swath count", Integer.toString(swathCount)));
		properties.add(Property.build("Beam count", Integer.toString(beamCount)));

		properties.add(Property.build("Start Date", DateUtils.getStringDate(firstSwathDate)));
		properties.add(Property.build("End Date", DateUtils.getStringDate(lastSwathDate)));

		properties
				.add(Property.build("North latitude", FormatLatitudeLongitude.latitudeToString(getGeoBox().getTop())));
		properties.add(
				Property.build("West longitude", FormatLatitudeLongitude.longitudeToString(getGeoBox().getLeft())));
		properties.add(
				Property.build("South latitude", FormatLatitudeLongitude.latitudeToString(getGeoBox().getBottom())));
		properties.add(
				Property.build("East longitude", FormatLatitudeLongitude.longitudeToString(getGeoBox().getRight())));

		properties.add(Property.build(IConstants.MinDepthLabel, NumberUtils.getStringDouble(-minDepth)));
		properties.add(Property.build(IConstants.MaxDepthLabel, NumberUtils.getStringDouble(-maxDepth)));

		// INSTALLATION ANTENNA PROPERTIES
		Property<String> installation = Property.build("Installation Lever Arm", "");
		properties.add(installation);
		antennas.forEach(antenna -> installation.add(Property.build(antenna)));

		properties.add(Property.build("Ellipsoid", ellipsoidName));
		properties.add(Property.build(" a, b (meters)", getEllipsoidA() + " , " + getEllipsoidB()));
		properties.add(Property.build(" e2, 1/flatness", getEllipsoidE2() + " , " + 1 / getEllipsoidFlatness()));

		properties.add(Property.build("Reference point", referencePoint));
		properties.add(Property.build("CDI (Common Data Index)", cdi));

		properties.add(Property.build("History", getHistory()));

		properties.add(Property.build("Automatic cleaning", MbgUtils.flagToString(automaticCleaning)));
		properties.add(Property.build("Manual cleaning", MbgUtils.flagToString(manualCleaning)));
		properties.add(Property.build("Velocity correction", MbgUtils.flagToString(velocityCorrection)));
		String biasCorrectionString = MbgUtils.flagToString(biasCorrection);
		if (biasCorrection == MbgConstants.MB_TRT_FLAG_ON) {
			biasCorrectionString += " (ref: " + biasCorrectionRef + ")";
		}
		properties.add(Property.build("Bias correction", biasCorrectionString));
		String tideCorrectionString = MbgUtils.flagToString(tideCorrection);
		if (tideCorrection == MbgConstants.MB_TRT_FLAG_ON) {
			tideCorrectionString += " (type: " + MbgUtils.getTideType(tideType) + "; ref: " + tideRef + ")";
		}
		properties.add(Property.build("Tide correction", tideCorrectionString));
		properties.add(Property.build("Navigation correction", MbgUtils.flagToString(positionCorrection)));
		properties.add(Property.build("Draught correction", MbgUtils.flagToString(draughtCorrection)));
		if (imReflecivityOrigin == MbgConstants.MB_TRT_FLAG_ON) {
			properties.add(Property.build("IM reflectivity imported", MbgUtils.flagToString(imReflecivityOrigin)));
		}

		return properties;
	}

	/**********************************************************************************************************
	 * Getters / Setters
	 **********************************************************************************************************/

	@Override
	public String getPath() {
		return filePath;
	}

	public NetcdfFile getFile() {
		return file;
	}

	protected void setFile(NetcdfFile file) {
		this.file = file;
	}

	public String getFilePath() {
		return filePath;
	}

	@Override
	public short getEchoSounderIndex() {
		return sounderIndex;
	}

	@Override
	public String getEchoSounderModel() {
		return sounderModel;
	}

	@Override
	public String getConstructor() {
		SounderDescription desc = SoundersLibrary.get().getFirstSounderDescription(getEchoSounderIndex());
		return desc.getCompany().name();
	}

	public void setEchoSounderModel(String sounderModel, short sounderIndex) {
		this.sounderModel = sounderModel;
		this.sounderIndex = sounderIndex;
	}

	@Override
	public int getCycleCount() {
		return swathCount;
	}

	public void setSwathCount(int swathCount) {
		this.swathCount = swathCount;
	}

	@Override
	public int getTotalRxBeamCount() {
		return beamCount;
	}

	public void setBeamCount(int beamCount) {
		this.beamCount = beamCount;
	}

	@Override
	public GeoBox getGeoBox() {
		return geobox;
	}

	@Override
	public Date getFirstPingDate() {
		return firstSwathDate;
	}

	public void setFirstPingDate(Date date) {
		firstSwathDate = date;
	}

	@Override
	public Date getLastPingDate() {
		return lastSwathDate;
	}

	public void setLastPingDate(Date date) {
		lastSwathDate = date;
	}

	public double getMinDepth() {
		return minDepth;
	}

	public void setMinDepth(double depth) {
		minDepth = depth;
	}

	public double getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(double depth) {
		maxDepth = depth;
	}

	public void setGeoBox(GeoBox geoBox) {
		geobox = geoBox;
	}

	public int getVersion() {
		return mbgVersion;
	}

	public void setVersion(int s) {
		mbgVersion = s;
	}

	public String getMbgName() {
		return mbgName;
	}

	public void setMbgName(String name) {
		mbgName = name;
	}

	public String getMbgClass() {
		return mbgClass;
	}

	public void setMbgClass(String mbgClass) {
		this.mbgClass = mbgClass;
	}

	public short getLevel() {
		return level;
	}

	public void setLevel(short level) {
		this.level = level;
	}

	public short getNbHistoryRec() {
		return nbHistoryRec;
	}

	public void setNbHistoryRec(short nbHistoryRec) {
		this.nbHistoryRec = nbHistoryRec;
	}

	public String getTimeReference() {
		return timeReference;
	}

	public void setTimeReference(String timeReference) {
		this.timeReference = timeReference;
	}

	public String getMeridien180() {
		return meridien180;
	}

	public void setMeridien180(String meridien180) {
		this.meridien180 = meridien180;
	}

	public String getGeodesicSystem() {
		return geodesicSystem;
	}

	public String getEllipsoidName() {
		return ellipsoidName;
	}

	public void setEllipsoidName(String ellipsoidName) {
		this.ellipsoidName = ellipsoidName;
	}

	public double getEllipsoidA() {
		return ellipsoidA;
	}

	public void setEllipsoidA(double ellipsoidA) {
		this.ellipsoidA = ellipsoidA;
	}

	public void setEllipsoidB(double ellipsoidB) {
		this.ellipsoidB = ellipsoidB;
	}

	public double getEllipsoidB() {
		return ellipsoidB;
	}

	public double getEllipsoidE2() {
		return ellipsoidE2;
	}

	public void setEllipsoidE2(double ellipsoidE2) {
		this.ellipsoidE2 = ellipsoidE2;
	}

	public double getEllipsoidFlatness() {
		return ellipsoidFlatness;
	}

	public void setEllipsoidFlatness(double ellipsoidFlatness) {
		this.ellipsoidFlatness = ellipsoidFlatness;
	}

	public void setGeodesicSystem(String geodesicSystem) {
		this.geodesicSystem = geodesicSystem;
	}

	public short getProjType() {
		return projType;
	}

	public void setProjType(short projType) {
		this.projType = projType;
	}

	public double[] getProjParameterValue() {
		return projParameterValue;
	}

	public void setProjParameterValue(double[] projParameterValue) {
		this.projParameterValue = projParameterValue;
	}

	public String getProjParameterCode() {
		return projParameterCode;
	}

	public void setProjParameterCode(String projParameterCode) {
		this.projParameterCode = projParameterCode;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getSurveyname() {
		return surveyname;
	}

	public void setSurveyname(String surveyname) {
		this.surveyname = surveyname;
	}

	public String getReferencePoint() {
		return referencePoint;
	}

	public void setReferencePoint(String referencePoint) {
		this.referencePoint = referencePoint;
	}

	public String getCdi() {
		return cdi;
	}

	public void setCdi(String cdi) {
		this.cdi = cdi;
	}

	public double getShiftAlongNavigation() {
		return shiftAlongNavigation;
	}

	public void setShiftAlongNavigation(double shiftAlongNavigation) {
		this.shiftAlongNavigation = shiftAlongNavigation;
	}

	public double getShiftAcrossNavigation() {
		return shiftAcrossNavigation;
	}

	public void setShiftAcrossNavigation(double shiftAcrossNavigation) {
		this.shiftAcrossNavigation = shiftAcrossNavigation;
	}

	public double getShiftVerticalNavigation() {
		return shiftVerticalNavigation;
	}

	public void setShiftVerticalNavigation(double shiftVerticalNavigation) {
		this.shiftVerticalNavigation = shiftVerticalNavigation;
	}

	public double getAntennaDelay() {
		return antennaDelay;
	}

	public void setAntennaDelay(double antennaDelay) {
		this.antennaDelay = antennaDelay;
	}

	public double[] getVRUOffset() {
		return VRUOffset;
	}

	public void setVRUOffset(double[] VRUOffset) {
		this.VRUOffset = VRUOffset;
	}

	public double getVRUDelay() {
		return VRUDelay;
	}

	public void setVRUDelay(double VRUDelay) {
		this.VRUDelay = VRUDelay;
	}

	public double[] getSounderOffset() {
		return sounderOffset;
	}

	public void setSounderOffset(double[] sounderOffset) {
		this.sounderOffset = sounderOffset;
	}

	public double getSounderDelay() {
		return sounderDelay;
	}

	public void setSounderDelay(double sounderDelay) {
		this.sounderDelay = sounderDelay;
	}

	public short getNavType() {
		return navType;
	}

	public void setNavType(short navType) {
		this.navType = navType;
	}

	public String getNavRef() {
		return navRef;
	}

	public void setNavRef(String navRef) {
		this.navRef = navRef;
	}

	public short getTideType() {
		return tideType;
	}

	public void setTideType(short tideType) {
		this.tideType = tideType;
	}

	public String getTideRef() {
		return tideRef;
	}

	public void setTideRef(String tideRef) {
		this.tideRef = tideRef;
	}

	public void setHeadingBias(double headingBias) {
		this.headingBias = headingBias;
	}

	public double getHeadingBias() {
		return headingBias;
	}

	public void setRollBias(double rollBias) {
		this.rollBias = rollBias;
	}

	public double getRollBias() {
		return rollBias;
	}

	public void setPitchBias(double pitchBias) {
		this.pitchBias = pitchBias;
	}

	public double getPitchBias() {
		return pitchBias;
	}

	public void setHeaveBias(double heaveBias) {
		this.heaveBias = heaveBias;
	}

	public double getHeaveBias() {
		return heaveBias;
	}

	public void setDraught(double draught) {
		this.draught = draught;
	}

	public double getDraught() {
		return draught;
	}

	public byte getAutomaticCleaning() {
		return automaticCleaning;
	}

	public void setAutomaticCleaning(byte automaticCleaning) {
		this.automaticCleaning = automaticCleaning;
	}

	public byte getManualCleaning() {
		return manualCleaning;
	}

	public void setManualCleaning(byte manualCleaning) {
		this.manualCleaning = manualCleaning;
	}

	public byte getBiasCorrection() {
		return biasCorrection;
	}

	public void setBiasCorrection(byte biasCorrection) {
		this.biasCorrection = biasCorrection;
	}

	public String getBiasCorrectionRef() {
		return biasCorrectionRef;
	}

	public void addBiasCorrectionRef(String biasCorrectionRef) {
		this.biasCorrectionRef = this.biasCorrectionRef + "," + biasCorrectionRef;
	}

	public void setBiasCorrectionRef(String biasCorrectionRef) {
		this.biasCorrectionRef = biasCorrectionRef;
	}

	public byte getVelocityCorrection() {
		return velocityCorrection;
	}

	public void setVelocityCorrection(byte velocityCorrection) {
		this.velocityCorrection = velocityCorrection;
	}

	public byte getTideCorrection() {
		return tideCorrection;
	}

	public void setTideCorrection(byte tideCorrection) {
		this.tideCorrection = tideCorrection;
	}

	public byte getPositionCorrection() {
		return positionCorrection;
	}

	public void setPositionCorrection(byte positionCorrection) {
		this.positionCorrection = positionCorrection;
	}

	public byte getDraughtCorrection() {
		return draughtCorrection;
	}

	public void setDraughtCorrection(byte draughtCorrection) {
		this.draughtCorrection = draughtCorrection;
	}

	public byte getImReflectivityOrigin() {
		return imReflecivityOrigin;
	}

	public void setImReflecivityOrigin(byte imReflecivityOrigin) {
		this.imReflecivityOrigin = imReflecivityOrigin;
	}

	public void setVelocityProfileCount(int velocityProfileCount) {
		this.velocityProfileCount = velocityProfileCount;
	}

	public int getVelocityProfileCount() {
		return velocityProfileCount;
	}

	public String getRawInstallParameters() {
		return rawInstallParameters;
	}

	public void setRawInstallParameters(String rawInstallParameters) {
		this.rawInstallParameters = rawInstallParameters;
	}

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	@Override
	public long getWcRxBeamCount() {
		// No water column data in MBG
		return 0;
	}

	@Override
	public int getTotalTxBeamCount() {
		// useless for mbg (used by water column spatializer)
		return -1;
	}

	@Override
	public Ellipsoid getEllipsoid() {
		// Ellipsoid by default : correct ?
		return Ellipsoid.WGS84;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType getContentType() {
		return ContentType.MBG_NETCDF_3;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ProcessAssessor> getProcessAssessor() {
		return Optional.of(new MbgProcessAssessor(this));
	}

	/** {@inheritDoc} */
	@Override
	public Optional<OptionalLayersAccessor> getOptionalLayersAccessor() {
		return Optional.of(new MbgOptionalLayersAccessor());
	}

	/**********************************************************************************************************
	 * ANTENNAS
	 **********************************************************************************************************/

	@Override
	public AntennaInstallationParameters getTxParameters() {
		List<AntennaInstallationParameters> txAntennas = getTxParametersList();
		return txAntennas.isEmpty() ? null : txAntennas.get(0);
	}

	protected List<AntennaInstallationParameters> getTxParametersList() {
		return antennas.stream().filter(ant -> (ant.type == AntennaInstallationParameters.Type.TX
				|| ant.type == AntennaInstallationParameters.Type.TXRX)).collect(Collectors.toList());
	}

	@Override
	public List<AntennaInstallationParameters> getRxParameters() {
		return antennas.stream().filter(ant -> (ant.type == AntennaInstallationParameters.Type.RX
				|| ant.type == AntennaInstallationParameters.Type.TXRX)).collect(Collectors.toList());
	}

	@Override
	public List<AntennaInstallationParameters> getAntennaParameters() {
		return antennas;
	}

	public void addAntenna(AntennaInstallationParameters antenna) {
		antennas.add(antenna);
	}

	public double getFirstPingDepth() {
		return getMinDepth();
	}

	/** {@inheritDoc} */
	@Override
	public int getBeamGroupCount() {
		// A MBG has no /Sonar/Beam_group
		return 0;
	}

	@Override
	public Optional<String> getPreferedAttitudeGroupId() {
		// A MBG has no /Sonar/Platform
		return Optional.empty();
	}

	@Override
	public Optional<SensorInstallationParameters> getPreferedAttitudeInstallationParameters() {
		// A MBG has no /Sonar/Platform
		return Optional.empty();
	}

	@Override
	public Optional<String> getPreferedPositionGroupId() {
		// A MBG has no /Sonar/Platform
		return Optional.empty();
	}

}
