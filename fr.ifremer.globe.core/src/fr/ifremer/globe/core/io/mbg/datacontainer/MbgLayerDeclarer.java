package fr.ifremer.globe.core.io.mbg.datacontainer;

import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.AcrossAlongLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.BeamLatLonLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.DepthLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.DetectionRxTransducerIndexLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.DetectionTwoWayTravelTimeLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.DetectionTypeLayerManager;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.PlatformVerticalOffsetLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.ReceiveTransducerIndexLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.ReflectivityLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.ResonQualityFlagsLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.StatusLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.SwathDoubleLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.SwathFloatLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.SwathLongLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.SwathShortLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.TimeLayerLoader;
import fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders.WaterLineLayerLoader;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.TideLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.DynamicDraughtLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.VendorSpecificLayers;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.sounders.SounderDescription.Constructor;

/**
 * Declares specifics layers of mbg in sounder data container
 */
public class MbgLayerDeclarer {

	/**
	 * Declares layers of the sounder data container
	 *
	 * @throws GIOException
	 */
	public void declareLayers(MbgInfo mbgSounderInfo, SounderDataContainer sdc) throws GIOException {

		// Gets required MBG layers
		DoubleLoadableLayer2D mbgLatitude = sdc.getLayer(MbgPredefinedLayers.LATITUDE);
		DoubleLoadableLayer2D mbgLongitude = sdc.getLayer(MbgPredefinedLayers.LONGITUDE);
		DoubleLoadableLayer2D mbgHeadingShip = sdc.getLayer(MbgPredefinedLayers.HEADING_SHIP);
		DoubleLoadableLayer2D mbgPitch = sdc.getLayer(MbgPredefinedLayers.PITCH);
		DoubleLoadableLayer2D mbgRoll = sdc.getLayer(MbgPredefinedLayers.ROLL);
		DoubleLoadableLayer2D mbgRefDepth = sdc.getLayer(MbgPredefinedLayers.REFERENCE_DEPTH);
		DoubleLoadableLayer2D mbgDraught = sdc.getLayer(MbgPredefinedLayers.DYNAMIC_DRAUGHT);
		DoubleLoadableLayer2D mbgTide = sdc.getLayer(MbgPredefinedLayers.TIDE);
		IntLoadableLayer2D acrossDistMbg = sdc.getLayer(MbgPredefinedLayers.ACROSS_DISTANCE);
		IntLoadableLayer2D alongDistMbg = sdc.getLayer(MbgPredefinedLayers.ALONG_DISTANCE);
		DoubleLoadableLayer2D distScaleMbg = sdc.getLayer(MbgPredefinedLayers.DISTANCE_SCALE);
		IntLoadableLayer2D mbgTime = sdc.getLayer(MbgPredefinedLayers.CYCLE_TIME);
		IntLoadableLayer2D mbgDate = sdc.getLayer(MbgPredefinedLayers.CYCLE_DATE);
		DoubleLoadableLayer2D mbgSoundVelocity = sdc.getLayer(MbgPredefinedLayers.SOUND_VELOCITY);
		IntLoadableLayer1D mbgAntennaIndex = sdc.getLayer(MbgPredefinedLayers.ANTENNA_INDEX);
		IntLoadableLayer2D mbgFrequency = sdc.getLayer(MbgPredefinedLayers.FREQUENCY_PLANE);

		// Array with the first valid antenna index for each swath, or 0 if no one valid.
		IntLayer2D mbgCycleFlag = sdc.getLayer(MbgPredefinedLayers.CYCLE_FLAG);
		long[] dims = mbgCycleFlag.getDimensions();
		int[] antennaIndexes = new int[(int) dims[0]];
		for (int iSwath = 0; iSwath < dims[0]; iSwath++) {
			for (int iAntenna = 0; iAntenna < dims[1]; iAntenna++) {
				if (mbgCycleFlag.get(iSwath, iAntenna) == MbgValidityConstants.PNG_VALID) {
					antennaIndexes[iSwath] = iAntenna;
					break;
				}
			}
		}

		// Declares model layers

		// Time
		sdc.declareLayer(BeamGroup1Layers.PING_TIME, new TimeLayerLoader(mbgDate, mbgTime, antennaIndexes));

		// Platform
		sdc.declareLayer(BeamGroup1Layers.PLATFORM_LATITUDE, new SwathDoubleLayerLoader(mbgLatitude, antennaIndexes));
		sdc.declareLayer(BeamGroup1Layers.PLATFORM_LONGITUDE, new SwathDoubleLayerLoader(mbgLongitude, antennaIndexes));
		sdc.declareLayer(BeamGroup1Layers.PLATFORM_HEADING, new SwathFloatLayerLoader(mbgHeadingShip, antennaIndexes));
		sdc.declareLayer(BeamGroup1Layers.PLATFORM_PITCH, new SwathFloatLayerLoader(mbgPitch, antennaIndexes));
		sdc.declareLayer(BeamGroup1Layers.PLATFORM_ROLL, new SwathFloatLayerLoader(mbgRoll, antennaIndexes));
		sdc.declareLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH, new SwathFloatLayerLoader(mbgRefDepth, antennaIndexes));

		sdc.declareLayer(VendorSpecificLayers.PING_RAW_COUNT,
				new SwathLongLayerLoader(sdc.getLayer(MbgPredefinedLayers.CYCLE_NUMBER), antennaIndexes));
		// Tide, draught, sound speed
		sdc.declareLayer(DynamicDraughtLayers.DELTA_DRAUGHT, new SwathFloatLayerLoader(mbgDraught, antennaIndexes));
		sdc.declareLayer(TideLayers.TIDE_INDICATIVE, new SwathFloatLayerLoader(mbgTide, antennaIndexes));
		sdc.declareLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM, new WaterLineLayerLoader(sdc));
		sdc.declareLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER,
				new SwathFloatLayerLoader(mbgSoundVelocity, antennaIndexes));

		// Detection X, Y, Z
		sdc.declareLayer(BathymetryLayers.DETECTION_Y,
				new AcrossAlongLayerLoader(acrossDistMbg, distScaleMbg, mbgAntennaIndex));
		sdc.declareLayer(BathymetryLayers.DETECTION_X,
				new AcrossAlongLayerLoader(alongDistMbg, distScaleMbg, mbgAntennaIndex));
		sdc.declareLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET, new PlatformVerticalOffsetLayerLoader(sdc));

		DepthLayerLoader depthLoader = new DepthLayerLoader(sdc);
		sdc.declareLayer(BathymetryLayers.DETECTION_Z, depthLoader);

		// Reflectivity
		if (sdc.hasLayer(MbgPredefinedLayers.REFLECTIVITY)) {
			sdc.declareLayer(BathymetryLayers.DETECTION_BACKSCATTER_R, new ReflectivityLayerLoader(sdc));
		}

		// Detection longitude / latitude
		BeamLatLonLayerLoader beamLatLonLoader = new BeamLatLonLayerLoader(sdc);
		sdc.declareLayer(BathymetryLayers.DETECTION_LONGITUDE, beamLatLonLoader);
		sdc.declareLayer(BathymetryLayers.DETECTION_LATITUDE, beamLatLonLoader);

		// Status
		StatusLayerLoader statusLayerLoader = new StatusLayerLoader(sdc);
		sdc.declareLayer(BathymetryLayers.STATUS, statusLayerLoader);
		sdc.declareLayer(BathymetryLayers.STATUS_DETAIL, statusLayerLoader);

		// Antenna
		sdc.declareLayer(BathymetryLayers.DETECTION_RX_TRANSDUCER_INDEX,
				new DetectionRxTransducerIndexLayerLoader(sdc));

		// Detection type
		DetectionTypeLayerManager qc = new DetectionTypeLayerManager(sdc);
		sdc.declareLayer(BathymetryLayers.DETECTION_TYPE, qc);

		// Reson Quality flags
		if (mbgSounderInfo.getConstructor().equals(Constructor.Reson.name())) {
			ResonQualityFlagsLayerLoader resonLoader = new ResonQualityFlagsLayerLoader(sdc);
			sdc.declareLayer(
					fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.bathymetry.VendorSpecificLayers.RESON_DETECTION_BRIGHTNESS_FLAG,
					resonLoader);
			sdc.declareLayer(
					fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.bathymetry.VendorSpecificLayers.RESON_DETECTION_COLINEARITY_FLAG,
					resonLoader);
			sdc.declareLayer(
					fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.bathymetry.VendorSpecificLayers.RESON_DETECTION_NADIR_FILTER_FLAG,
					resonLoader);
		}

		// Detection two way travel time
		DetectionTwoWayTravelTimeLayerLoader travelTimeLoader = new DetectionTwoWayTravelTimeLayerLoader(sdc);
		sdc.declareLayer(BathymetryLayers.DETECTION_TWO_WAY_TRAVEL_TIME, travelTimeLoader);

		// Frequency plane
		SwathShortLayerLoader multipingLayerLoader = new SwathShortLayerLoader(mbgFrequency, antennaIndexes);
		sdc.declareLayer(BathymetryLayers.MULTIPING_SEQUENCE, multipingLayerLoader);

		// Antenna index
		sdc.declareLayer(BeamGroup1Layers.RECEIVE_TRANSDUCER_INDEX, new ReceiveTransducerIndexLayerLoader(sdc));
	}
}
