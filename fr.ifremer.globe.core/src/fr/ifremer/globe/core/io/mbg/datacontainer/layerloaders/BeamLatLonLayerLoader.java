/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.mbes.MbesUtils;

/**
 * Computes detection latitude / longitude
 */
public class BeamLatLonLayerLoader implements ILayerLoader<DoubleBuffer2D> {

	protected long[] shape;
	protected MbgInfo mbgInfo;
	protected SounderDataContainer sounderDataContainer;
	protected Ellipsoid ellipsoid;
	protected AtomicBoolean loading = new AtomicBoolean(false);

	/**
	 * Constructor
	 */
	public BeamLatLonLayerLoader(SounderDataContainer sounderDataContainer) {
		this.mbgInfo = (MbgInfo) sounderDataContainer.getInfo();
		this.shape = new long[] { mbgInfo.getCycleCount(), mbgInfo.getTotalRxBeamCount() };
		this.sounderDataContainer = sounderDataContainer;
		this.ellipsoid = mbgInfo.getEllipsoid();
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(DoubleBuffer2D buffer) throws GIOException {
		// Ensure that the process loading is done only one time
		if (loading.compareAndSet(false, true)) {
			processLoading();
			loading.set(false);
			buffer.setDirty(false);
		}
		return true;
	}

	/**
	 * Computes beams latitudes and longitudes, updates latitude and longitude layers of bathy group
	 */
	protected void processLoading() throws GIOException {
		DoubleLoadableLayer2D beamLatitudeLayer = sounderDataContainer.getLayer(BathymetryLayers.DETECTION_LATITUDE);
		DoubleLoadableLayer2D beamLongitudeLayer = sounderDataContainer.getLayer(BathymetryLayers.DETECTION_LONGITUDE);
		FloatLoadableLayer1D headingLayer = sounderDataContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
		FloatLoadableLayer2D alongDistanceLayer = sounderDataContainer.getLayer(BathymetryLayers.DETECTION_X);
		FloatLoadableLayer2D acrossDistanceLayer = sounderDataContainer.getLayer(BathymetryLayers.DETECTION_Y);

		IntLoadableLayer1D mbgBeamAntennaIdxLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.ANTENNA_INDEX);
		DoubleLoadableLayer2D mbgLatitudeLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.LATITUDE);
		DoubleLoadableLayer2D mbgLongitudeLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.LONGITUDE);

		double[] latlong = new double[2];
		for (int cycleIndex = 0; cycleIndex < shape[0]; cycleIndex++) {
			for (int beamIndex = 0; beamIndex < shape[1]; beamIndex++) {
				int antennaIdx = mbgBeamAntennaIdxLayer.get(beamIndex);
				double[] normAndR = MbesUtils.calcNormRayon(mbgLatitudeLayer.get(cycleIndex, antennaIdx),
						ellipsoid.getEccentricity2(), ellipsoid.getHalf_A());

				double norm = normAndR[0];
				double rayon = normAndR[1];

				double heading = headingLayer.get(cycleIndex);

				double sinHeading = Math.sin(Math.toRadians(heading));
				double cosHeading = Math.cos(Math.toRadians(heading));

				float alongDistance = alongDistanceLayer.get(cycleIndex, beamIndex);
				double acrossDistance = acrossDistanceLayer.get(cycleIndex, beamIndex);

				MbesUtils.acrossAlongToLatitudeLongitude(latlong, mbgLongitudeLayer.get(cycleIndex, antennaIdx),
						mbgLatitudeLayer.get(cycleIndex, antennaIdx), alongDistance, acrossDistance, rayon,
						sinHeading, cosHeading, norm);

				beamLatitudeLayer.set(cycleIndex, beamIndex, latlong[0]);
				beamLongitudeLayer.set(cycleIndex, beamIndex, latlong[1]);
			}
		}
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean flush(DoubleBuffer2D buffer) throws GIOException {
		return false;
	}
}
