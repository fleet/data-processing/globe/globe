/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Computes the across or along layer of the sounder data container
 */
public class AcrossAlongLayerLoader implements ILayerLoader<FloatBuffer2D> {

	protected long[] shape;
	protected final AtomicBoolean loading = new AtomicBoolean(false);
	private final IntLoadableLayer2D mbgAlongOrAcross;
	private final DoubleLoadableLayer2D distanceScaleLayer;
	private final IntLoadableLayer1D mbgAntennaIndexLayer;

	/** Constructor */
	public AcrossAlongLayerLoader(IntLoadableLayer2D mbgAlongOrAcross, DoubleLoadableLayer2D distanceScaleLayer,
			IntLoadableLayer1D mbgAntennaIndexLayer) throws GIOException {
		this.mbgAlongOrAcross = mbgAlongOrAcross;
		this.shape = mbgAlongOrAcross.getLoader().getDimensions();
		this.distanceScaleLayer = distanceScaleLayer;
		this.mbgAntennaIndexLayer = mbgAntennaIndexLayer;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(FloatBuffer2D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			for (int cycleIndex = 0; cycleIndex < shape[0]; cycleIndex++) {
				for (int beamIndex = 0; beamIndex < shape[1]; beamIndex++) {
					int antennaIndex = mbgAntennaIndexLayer.get(beamIndex);
					double distanceScale = distanceScaleLayer.get(cycleIndex, antennaIndex);
					buffer.set(cycleIndex, beamIndex,
							(float) (mbgAlongOrAcross.get(cycleIndex, beamIndex) * distanceScale));
				}
			}
			loading.set(false);
			buffer.setDirty(false);
		}
		return true;
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean flush(FloatBuffer2D buffer) throws GIOException {
		for (int cycleIndex = 0; cycleIndex < shape[0]; cycleIndex++) {
			for (int beamIndex = 0; beamIndex < shape[1]; beamIndex++) {
				int antennaIndex = mbgAntennaIndexLayer.get(beamIndex);
				double distanceScale = distanceScaleLayer.get(cycleIndex, antennaIndex);
				mbgAlongOrAcross.set(cycleIndex, beamIndex,
						(int) Math.round(buffer.get(cycleIndex, beamIndex) / distanceScale));
			}
		}
		mbgAlongOrAcross.flush();
		return true;

	}

}
