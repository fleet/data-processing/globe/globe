package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.maths.MatrixBuilder;
import fr.ifremer.globe.utils.maths.Ops;
import fr.ifremer.globe.utils.maths.RotationMatrix3x3D;
import fr.ifremer.globe.utils.maths.Vector3D;

public class PlatformVerticalOffsetLayerLoader implements ILayerLoader<FloatBuffer1D> {

	/** Properties **/

	protected SounderDataContainer sounderDataContainer;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	protected MbgInfo mbgInfo;
	protected long[] shape;

	/** Constructors **/

	public PlatformVerticalOffsetLayerLoader(SounderDataContainer sounderDataContainer) throws GIOException {
		this.sounderDataContainer = sounderDataContainer;
		this.mbgInfo = (MbgInfo) sounderDataContainer.getInfo();
		this.shape = new long[] { mbgInfo.getCycleCount() };

	}

	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/** Load the model layer buffer with the MBG layer **/
	@Override
	public boolean load(FloatBuffer1D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			processLoading(buffer);
		}
		loading.set(false);
		buffer.setDirty(false);
		return true;
	}

	/** Flush the MBG layer with values from the corresponding model layer **/
	@Override
	public boolean flush(FloatBuffer1D buffer) throws GIOException {

		return false;
	}

	/**
	 * compute platform_vertical_offset
	 */
	protected void processLoading(FloatBuffer1D buffer) throws GIOException {
		RotationMatrix3x3D attitudeMatrix = new RotationMatrix3x3D();
		Vector3D txAntennaSCS = new Vector3D(mbgInfo.getTxParameters().xoffset, mbgInfo.getTxParameters().yoffset,
				mbgInfo.getTxParameters().zoffset);

		Vector3D txAntennaSCS_swath = new Vector3D();

		FloatLoadableLayer1D platformPitch = sounderDataContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);
		FloatLoadableLayer1D platformRoll = sounderDataContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
		FloatLoadableLayer1D txZDepthLayer = sounderDataContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
		for (int swathIndex = 0; swathIndex < shape[0]; swathIndex++) {
			float roll = platformRoll.get(swathIndex);
			float pitch = platformPitch.get(swathIndex);
			float tx_z_depth = txZDepthLayer.get(swathIndex);
			MatrixBuilder.setDegRotationMatrix(0, pitch, roll, attitudeMatrix);
			Ops.mult(attitudeMatrix, txAntennaSCS, txAntennaSCS_swath);
			buffer.set(swathIndex, (float) (txAntennaSCS_swath.getZ() - tx_z_depth));
		}
	}
}
