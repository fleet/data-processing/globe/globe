package fr.ifremer.globe.core.io.mbg.service;

import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.io.mbg.writer.MbgInfoWriter;
import fr.ifremer.globe.utils.exception.GIOException;

public class MbgFlagCleaner extends MbgInfoWriter {

	/**
	 * Replaces invalid flags by "0".
	 */
	public void cleanFlags(MbgInfo mbg) throws GIOException {
		cleanFlag(mbg::getAutomaticCleaning, mbg::setAutomaticCleaning);
		cleanFlag(mbg::getManualCleaning, mbg::setManualCleaning);
		cleanFlag(mbg::getVelocityCorrection, mbg::setVelocityCorrection);
		cleanFlag(mbg::getBiasCorrection, mbg::setBiasCorrection);
		cleanFlag(mbg::getTideCorrection, mbg::setTideCorrection);
		cleanFlag(mbg::getPositionCorrection, mbg::setPositionCorrection);
		cleanFlag(mbg::getDraughtCorrection, mbg::setDraughtCorrection);
		writeProperties(mbg);
	}

	private void cleanFlag(Supplier<Byte> flagGetter, Consumer<Byte> flagSetter) {
		if (flagGetter.get() != MbgConstants.MB_TRT_FLAG_ON)
			flagSetter.accept(MbgConstants.MB_TRT_FLAG_OFF);
	}
}
