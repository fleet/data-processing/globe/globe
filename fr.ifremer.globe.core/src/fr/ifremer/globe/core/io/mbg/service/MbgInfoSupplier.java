package fr.ifremer.globe.core.io.mbg.service;

import java.util.Optional;
import java.util.TreeMap;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfoSupplier;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Service providing informations on MBG files
 */
@Component(name = "globe_drivers_mbg_file_info_supplier", service = { IFileInfoSupplier.class,
		ISounderNcInfoSupplier.class, INavigationSupplier.class, MbgInfoSupplier.class })
public class MbgInfoSupplier extends BasicFileInfoSupplier<MbgInfo>
		implements ISounderNcInfoSupplier<MbgInfo>, INavigationSupplier {

	private static final Logger logger = LoggerFactory.getLogger(MbgInfoSupplier.class);

	/**
	 * Constructor
	 */
	public MbgInfoSupplier() {
		super("mbg", "Caraibes MBG (*.mbg)", ContentType.MBG_NETCDF_3);
	}

	@Override
	public MbgInfo makeFileInfo(String filePath) {
		MbgInfo result = null;
		if (isValid(filePath)) {
			try {
				result = new MbgInfo(filePath);
			} catch (GIOException e) {
				// Not a Mbg file
				result = null;
			}
		}
		return result;
	}

	private boolean isValid(String filePath) {
		try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readonly)) {
			return isValidMbgFile(file);
		} catch (NCException e) {
			logger.error("Error while opening MBG File {} : {}", filePath, e.getMessage(), e);
			return false;
		}
	}

	/***
	 * Tests validity of the specified file
	 */
	public boolean isValidMbgFile(NetcdfFile ncFile) throws NCException {
		StringBuilder message = new StringBuilder();
		int nbOfError = 0;
		String classe = ncFile.getAttributeAsString(MbgConstants.Classe);
		if (!classe.contentEquals(MbgConstants.MBGClasse)) {
			message.append("not a MBG file");
		} else {
			Short fileVersion = ncFile.getAttributeShort(MbgConstants.Version.toString());
			if (fileVersion < MbgConstants.SupportedVersion) {
				message.append("bad MBG Version: ").append(fileVersion).append(" / ").append(MbgConstants.ActualVersion)
						.append(" expected");
			} else {
				if (ncFile.getDimension("CIB_BLOCK_DIM") != null) {
					message.append("\n CIB_BLOCK_DIM is present: number of unlimited pings"
							+ "\n Update it by MbgUpd(CARAIBES) in the Globe toolbox ");
					nbOfError++;
				}
				if (ncFile.getDimension(MbgConstants.BEAM_NUMBER) == null) {
					message.append(MbgConstants.BEAM_NUMBER + " is missing");
					nbOfError++;
				}
				if (ncFile.getDimension(MbgConstants.CYCLE_NUMBER) == null) {
					message.append(MbgConstants.CYCLE_NUMBER + " is missing");
					nbOfError++;
				}
				TreeMap<String, String> GlobalAttributsInFile = ncFile.getGlobalAttributs();
				for (String attg : MbgConstants.GlobalAttributes) {
					if (GlobalAttributsInFile.get(attg) == null) {
						message.append("\n" + attg + " is missing");
						nbOfError++;
						if (nbOfError > 20) {
							message.append("\nmore.....");
							break;
						}
					}
				}
			}
		}
		if (message.length() > 0) {
			logger.warn("{} is not a valid MBG : {}", ncFile.getName(), message);
		}
		return message.length() == 0;
	}

	@Override
	public Optional<MbgInfo> getSounderNcInfo(String filePath) {
		return getFileInfo(filePath);
	}

	@Override
	public Optional<MbgInfo> getSounderNcInfo(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof MbgInfo ? (MbgInfo) fileInfo : null);
	}

	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof MbgInfo ? (MbgInfo) fileInfo : null);
	}

}