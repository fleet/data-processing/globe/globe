package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer2D;
import fr.ifremer.globe.utils.exception.GIOException;

public class DetectionTwoWayTravelTimeLayerLoader implements ILayerLoader<FloatBuffer2D> {

	/** Properties **/
	private final long[] mbgShape;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	private final DoubleLoadableLayer2D mbgLayer;

	/** Constructors **/
	public DetectionTwoWayTravelTimeLayerLoader(SounderDataContainer sounderDataContainer) throws GIOException {
		this.mbgLayer = sounderDataContainer.getLayer(MbgPredefinedLayers.RANGE);
		this.mbgShape = mbgLayer.getLoader().getDimensions();
	}

	@Override
	public long[] getDimensions() throws GIOException {
		return mbgShape;
	}

	/** Load the model layer buffer with the MBG layer **/
	@Override
	public boolean load(FloatBuffer2D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			for (int iSwath = 0; iSwath < mbgShape[0]; iSwath++) {
				for (int iBeam = 0; iBeam < mbgShape[1]; iBeam++) {
					buffer.set(iSwath, iBeam, (float) (mbgLayer.get(iSwath, iBeam) * 2));
				}
			}
		}
		buffer.setDirty(false);
		loading.set(false);
		return true;
	}

	/** Flush the MBG layer with values from the corresponding model layer **/
	@Override
	public boolean flush(FloatBuffer2D buffer) throws GIOException {
		// we never modify this value
		return true;
	}

}
