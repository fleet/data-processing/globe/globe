package fr.ifremer.globe.core.io.mbg.writer;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Provides methods to write MBG file attributes.
 */
public class MbgInfoWriter {

	private String trimToSize(String name, int size) {
		return name.substring(0, Math.min(name.length(), size));
	}

	/***
	 * write the properties in the file
	 */
	public void writeProperties(MbgInfo info) throws GIOException {
		try {
			info.open(false);
			NetcdfFile ncfile = info.getFile();
			ncfile.redef();

			ncfile.addAttribute(MbgConstants.Ship, trimToSize(info.getShipName(), IConstants.MB_COMMENT_LENGTH_VALUE));
			ncfile.addAttribute(MbgConstants.Survey,
					trimToSize(info.getSurveyname(), IConstants.MB_COMMENT_LENGTH_VALUE));
			ncfile.addAttribute(MbgConstants.Reference,
					trimToSize(info.getReferencePoint(), IConstants.MB_COMMENT_LENGTH_VALUE));
			ncfile.addAttribute(MbgConstants.CDI, trimToSize(info.getCdi(), IConstants.MB_LONG_NAME_LENGTH_VALUE));
			ncfile.addAttribute(MbgConstants.Sounder, info.getEchoSounderIndex());
			ncfile.addAttribute(MbgConstants.Version, (short) info.getVersion());

			// Start - end date/time
			int[] dayMs = DateUtils.getJulianDateTime(info.getFirstPingDate());
			ncfile.addAttribute(MbgConstants.StartDate, dayMs[0]);
			ncfile.addAttribute(MbgConstants.StartTime, dayMs[1]);

			dayMs = DateUtils.getJulianDateTime(info.getLastPingDate());
			ncfile.addAttribute(MbgConstants.EndDate, dayMs[0]);
			ncfile.addAttribute(MbgConstants.EndTime, dayMs[1]);

			ncfile.addAttribute(MbgConstants.EllipsoidName,
					trimToSize(info.getEllipsoidName(), IConstants.MB_COMMENT_LENGTH_VALUE));
			ncfile.addAttribute(MbgConstants.EllipsoidA, info.getEllipsoidA());
			ncfile.addAttribute(MbgConstants.EllipsoidInvF, info.getEllipsoidFlatness());
			ncfile.addAttribute(MbgConstants.EllipsoidE2, info.getEllipsoidE2());

			ncfile.addAttribute(MbgConstants.TideType, info.getTideType());
			ncfile.addAttribute(MbgConstants.TideRef, trimToSize(info.getTideRef(),
					Math.min(info.getTideRef().length(), IConstants.MB_COMMENT_LENGTH_VALUE)));

			ncfile.addAttribute(MbgConstants.BiasCorrectionRef, trimToSize(info.getBiasCorrectionRef(),
					Math.min(info.getBiasCorrectionRef().length(), IConstants.MB_COMMENT_LENGTH_VALUE)));

			// flag values (defined as char)
			// to prevent a write bug, attributes are removed before
			ncfile.deleteAttribute(MbgConstants.AutomaticCleaning);
			ncfile.addAttribute(MbgConstants.AutomaticCleaning, (char) info.getAutomaticCleaning());

			ncfile.deleteAttribute(MbgConstants.ManualCleaning);
			ncfile.addAttribute(MbgConstants.ManualCleaning, (char) info.getManualCleaning());

			ncfile.deleteAttribute(MbgConstants.VelocityCorrection);
			ncfile.addAttribute(MbgConstants.VelocityCorrection, (char) info.getVelocityCorrection());

			ncfile.deleteAttribute(MbgConstants.BiasCorrection);
			ncfile.addAttribute(MbgConstants.BiasCorrection, (char) info.getBiasCorrection());

			ncfile.deleteAttribute(MbgConstants.TideCorrection);
			ncfile.addAttribute(MbgConstants.TideCorrection, (char) info.getTideCorrection());

			ncfile.deleteAttribute(MbgConstants.PositionCorrection);
			ncfile.addAttribute(MbgConstants.PositionCorrection, (char) info.getPositionCorrection());

			ncfile.deleteAttribute(MbgConstants.DraughtCorrection);
			ncfile.addAttribute(MbgConstants.DraughtCorrection, (char) info.getDraughtCorrection());

			ncfile.addAttribute(MbgConstants.CycleCounter, info.getCycleCount());

			ncfile.enddef();
			ncfile.synchronize();
		} catch (NCException e) {
			throw new GIOException("write properties error : " + e.getMessage(), e);
		} finally {
			info.close();
		}
	}

	/**
	 * writes depth min max values
	 */
	public void updateMinMaxDepth(MbgInfo info) throws GIOException {
		try {
			info.open(false);
			NetcdfFile ncfile = info.getFile();
			ncfile.redef();
			ncfile.addAttribute(MbgConstants.MinDepth, -info.getMinDepth());
			ncfile.addAttribute(MbgConstants.MaxDepth, -info.getMaxDepth());
			ncfile.enddef();
		} catch (NCException e) {
			throw new GIOException("trying to write properties to netcdf", e);
		} finally {
			info.close();
		}
	}

	/**
	 * Writes geographic limits
	 */
	public void writeGeoBox(MbgInfo info) throws GIOException {
		try {
			info.open(false);
			NetcdfFile ncfile = info.getFile();
			ncfile.redef();
			GeoBox geoBox = info.getGeoBox();
			ncfile.addAttribute(MbgConstants.NorthLatitude, geoBox.getTop());
			ncfile.addAttribute(MbgConstants.SouthLatitude, geoBox.getBottom());
			ncfile.addAttribute(MbgConstants.EastLongitude, geoBox.getRight());
			ncfile.addAttribute(MbgConstants.WestLongitude, geoBox.getLeft());
			ncfile.enddef();
		} catch (NCException e) {
			throw new GIOException("Error trying to write Geobox properties to netcdf", e);
		} finally {
			info.close();
		}
	}

}
