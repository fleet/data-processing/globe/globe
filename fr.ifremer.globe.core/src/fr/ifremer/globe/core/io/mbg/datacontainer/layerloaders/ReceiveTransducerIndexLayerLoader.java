/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.IntBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 *
 */
public class ReceiveTransducerIndexLayerLoader implements ILayerLoader<IntBuffer1D> {

	private final long[] shape;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	private final SounderDataContainer sdc;

	/**
	 * Constructor
	 */
	public ReceiveTransducerIndexLayerLoader(SounderDataContainer sounderDataContainer) throws GIOException {
		sdc = sounderDataContainer;
		shape = sdc.getLayer(MbgPredefinedLayers.ANTENNA_INDEX).getLoader().getDimensions();
	}

	/** {@inheritDoc} */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/** {@inheritDoc} */
	@Override
	public boolean load(IntBuffer1D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			IntLoadableLayer1D mbgAntennaIndex = sdc.getLayer(MbgPredefinedLayers.ANTENNA_INDEX);
			for (int iBeam = 0; iBeam < shape[0]; iBeam++) {
				buffer.set(iBeam, mbgAntennaIndex.get(iBeam));
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	/** {@inheritDoc} */
	@Override
	public boolean flush(IntBuffer1D buffer) throws GIOException {
		return true; // This layer is read only
	}

}
