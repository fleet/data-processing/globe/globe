package fr.ifremer.globe.core.io.mbg.datacontainer;

import java.util.EnumSet;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgValidityConstants.SoundingValidity;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatusDetail;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.StatusPair;

/**
 * Utility class creating a mapping between model detection status {@link SonarDetectionStatus},model status detail
 * {@link SonarDetectionStatusDetail} and MBG constant {@link SoundingValidity}
 */
public class ValidityMapper {

	/** empty constructor: utility class */
	private ValidityMapper() {
	}

	///// FROM MODEL TO MBG

	/**
	 * retrieve the matching {@link SoundingValidity} from the model
	 * 
	 * @param status the status store in model
	 * @param detail the status detail from model
	 */
	public static SoundingValidity getSoundingValidity(byte status, byte detail) {

		EnumSet<SonarDetectionStatus> set = SonarDetectionStatus.getStatus(status);

		// INVALID_SOUNDING_ROW (=beam_flag) and INVALID_SWATH (=cycle flag) are not used to defined the sounding
		// validity
		set.remove(SonarDetectionStatus.INVALID_SOUNDING_ROW);
		set.remove(SonarDetectionStatus.INVALID_SWATH);

		if (set.contains(SonarDetectionStatus.INVALID_ACQUISITION))
			return SoundingValidity.SND_UNVALID_ACQUIS;

		if (set.contains(SonarDetectionStatus.INVALID_CONVERSION))
			return SoundingValidity.SND_MISSING;

		if (set.isEmpty() || set.contains(SonarDetectionStatus.VALID))
			return SoundingValidity.SND_VALID;

		if (set.contains(SonarDetectionStatus.REJECTED)) {
			switch (SonarDetectionStatusDetail.valueOf(detail)) {
			case AUTO:
				return SoundingValidity.SND_UNVALID_AUTO;
			case DOUBTFULL:
				return SoundingValidity.SND_DOUBTFUL;
			case MANUAL:
				return SoundingValidity.SND_UNVALID;
			case UNKNOWN:
				return SoundingValidity.SND_UNVALID;
			default:
				break;
			}
		}

		// by default
		return SoundingValidity.SND_MISSING;
	}

	/**
	 * compute the MBG Ping flag values from the model
	 */
	public static int getPingValidity(byte status) {
		EnumSet<SonarDetectionStatus> set = SonarDetectionStatus.getStatus(status);
		return set.contains(SonarDetectionStatus.INVALID_SWATH) ? MbgValidityConstants.PNG_UNVALID
				: MbgValidityConstants.PNG_VALID;
	}

	/**
	 * compute the MBG Beam flag values from the model
	 */
	public static int getDetectionBeamValidity(byte status) {
		EnumSet<SonarDetectionStatus> set = SonarDetectionStatus.getStatus(status);
		return set.contains(SonarDetectionStatus.INVALID_SOUNDING_ROW) ? MbgValidityConstants.BEA_UNVALID
				: MbgValidityConstants.BEA_VALID;
	}

	/////////// FROM MBG TO MODEL
	/**
	 * @return the model status corresponding to the specified ping/beam/antenna/sound mbg flags
	 */
	public static void getStatus(int pingFlag, int beamFlag, int antennaFlag, SoundingValidity validity,
			StatusPair result) {
		byte status = SonarDetectionStatus.VALID.getValue();

		byte statusDetails = 0;

		// ping flag
		if (pingFlag < MbgValidityConstants.PNG_VALID)
			status = (byte) (status | SonarDetectionStatus.INVALID_SWATH.getValue());

		// beam flag
		if (beamFlag < MbgValidityConstants.BEA_VALID)
			status = (byte) (status | SonarDetectionStatus.INVALID_SOUNDING_ROW.getValue());

		// no antenna flag, invalid all detection instead
		if (antennaFlag < MbgValidityConstants.ANT_VALID) {
			status = (byte) (status | SonarDetectionStatus.REJECTED.getValue());
			statusDetails = SonarDetectionStatusDetail.UNKNOWN.getValue();
		}

		// Sound validity
		switch (validity) {
		case SND_VALID:
			// nothing to do
			break;
		case SND_DOUBTFUL:
			status = (byte) (status | SonarDetectionStatus.REJECTED.getValue());
			statusDetails = SonarDetectionStatusDetail.DOUBTFULL.getValue();
			break;
		case SND_UNVALID:
			status = (byte) (status | SonarDetectionStatus.REJECTED.getValue());
			statusDetails = SonarDetectionStatusDetail.MANUAL.getValue();
			break;
		case SND_UNVALID_ACQUIS:
			status = (byte) (status | SonarDetectionStatus.INVALID_ACQUISITION.getValue());
			break;
		case SND_UNVALID_AUTO:
			status = (byte) (status | SonarDetectionStatus.REJECTED.getValue());
			statusDetails = SonarDetectionStatusDetail.AUTO.getValue();
			break;
		case SND_MISSING:
			status = (byte) (status | SonarDetectionStatus.INVALID_CONVERSION.getValue());
			break;
		default:
			break;
		}

		result.setStatus(status);
		result.setDetail(statusDetails);
	}

	/**
	 * @return the model status corresponding to the specified ping/beam/antenna/sound mbg flags
	 */
	public static StatusPair getStatus(int pingFlag, int beamFlag, int antennaFlag, SoundingValidity validity) {
		StatusPair result = new StatusPair((byte) 0, (byte) 0);
		getStatus(pingFlag, beamFlag, antennaFlag, validity, result);
		return result;
	}
}
