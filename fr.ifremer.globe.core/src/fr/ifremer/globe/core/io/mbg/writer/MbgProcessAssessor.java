/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.writer;

import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;
import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * MbgInfo process assessor
 */
public class MbgProcessAssessor implements ProcessAssessor {
	/** Managed MbgInfo */
	protected MbgInfo mbgInfo;

	/**
	 * Constructor
	 */
	public MbgProcessAssessor(MbgInfo mbgInfo) {
		this.mbgInfo = mbgInfo;
	}

	/** {@inheritDoc} */
	@Override
	public void heedBiasCorrection(Optional<String> correctionFileName, List<String> correctorsList) throws GIOException {
		if(correctionFileName.isPresent()) {
			if(MbgConstants.MB_TRT_FLAG_ON == mbgInfo.getBiasCorrection()) {
				mbgInfo.addBiasCorrectionRef(FilenameUtils.getName(correctionFileName.get()));		
			} else {
				mbgInfo.setBiasCorrectionRef(FilenameUtils.getName(correctionFileName.get()));
			}
		}
		mbgInfo.setBiasCorrection(MbgConstants.MB_TRT_FLAG_ON);
		MbgInfoWriter mbgInfoWriter = new MbgInfoWriter();
		mbgInfoWriter.writeProperties(this.mbgInfo);
		mbgInfoWriter.updateMinMaxDepth(this.mbgInfo);
	}

	/** {@inheritDoc} */
	@Override
	public boolean biasCorrectionEnforced() {
		return mbgInfo.getBiasCorrection() == MbgConstants.MB_TRT_FLAG_ON;
	}

	/** {@inheritDoc} */
	@Override
	public void heedTideCorrection(Optional<String> correctionFileName, TideType tideType) throws GIOException {
		if (correctionFileName.isPresent()) {
			mbgInfo.setTideCorrection(MbgConstants.MB_TRT_FLAG_ON);

			switch (tideType) {
			case GPS:
				mbgInfo.setTideType(MbgConstants.MB_FLAG_GPS_TIDE);
				break;
			case MEASURED:
				mbgInfo.setTideType(MbgConstants.MB_FLAG_MESURED_TIDE);
				break;
			case PREDICTED:
				mbgInfo.setTideType(MbgConstants.MB_FLAG_PREDICTED_TIDE);
				break;
			case UNKNWON:
			default:
				mbgInfo.setTideType(MbgConstants.MB_FLAG_NO_TIDE);
				break;
			}
			mbgInfo.setTideRef(correctionFileName.get());
		} else {
			mbgInfo.setTideCorrection(MbgConstants.MB_TRT_FLAG_OFF);
			mbgInfo.setTideType(MbgConstants.MB_FLAG_NO_TIDE);
			mbgInfo.setTideRef("None");
		}

		var mbgInfoWriter = new MbgInfoWriter();
		mbgInfoWriter.writeProperties(this.mbgInfo);
		mbgInfoWriter.updateMinMaxDepth(this.mbgInfo);
	}

	/** {@inheritDoc} */
	@Override
	public boolean tideCorrectionEnforced() {
		return mbgInfo.getTideCorrection() == MbgConstants.MB_TRT_FLAG_ON;
	}

	/** {@inheritDoc} */
	@Override
	public void heedPositionCorrection() throws GIOException {
		MbgInfoWriter mbgInfoWriter = new MbgInfoWriter();
		mbgInfo.setPositionCorrection(MbgConstants.MB_TRT_FLAG_ON);
		mbgInfoWriter.writeProperties(this.mbgInfo);
	}

	/** {@inheritDoc} */
	@Override
	public boolean positionCorrectionEnforced() {
		return mbgInfo.getPositionCorrection() == MbgConstants.MB_TRT_FLAG_ON;
	}

	/** {@inheritDoc} */
	@Override
	public void heedDraughtCorrection(Optional<String> correctionFileName) throws GIOException {
		MbgInfoWriter mbgInfoWriter = new MbgInfoWriter();
		if (correctionFileName.isPresent()) {
			mbgInfo.setDraughtCorrection(MbgConstants.MB_TRT_FLAG_ON);
		} else {
			mbgInfo.setDraughtCorrection(MbgConstants.MB_TRT_FLAG_OFF);
		}
		mbgInfoWriter.writeProperties(this.mbgInfo);
		mbgInfoWriter.updateMinMaxDepth(this.mbgInfo);
	}

	/** {@inheritDoc} */
	@Override
	public boolean draughtCorrectionEnforced() {
		return mbgInfo.getDraughtCorrection() == MbgConstants.MB_TRT_FLAG_ON;
	}

	/** {@inheritDoc} */
	@Override
	public void heedAutomaticCleaning() throws GIOException {
		mbgInfo.setAutomaticCleaning(MbgConstants.MB_TRT_FLAG_ON);
		MbgInfoWriter mbgInfoWriter = new MbgInfoWriter();
		mbgInfoWriter.writeProperties(this.mbgInfo);
	}

	/** {@inheritDoc} */
	@Override
	public boolean automaticCleaningEnforced() {
		return mbgInfo.getAutomaticCleaning() == MbgConstants.MB_TRT_FLAG_ON;
	}

	/** {@inheritDoc} */
	@Override
	public void heedVelocityCorrection() throws GIOException {
		mbgInfo.setVelocityCorrection(MbgConstants.MB_TRT_FLAG_ON);
		MbgInfoWriter mbgInfoWriter = new MbgInfoWriter();
		mbgInfoWriter.writeProperties(mbgInfo);
	}

	/** {@inheritDoc} */
	@Override
	public boolean velocityCorrectionEnforced() {
		return mbgInfo.getVelocityCorrection() == MbgConstants.MB_TRT_FLAG_ON;
	}

	/** {@inheritDoc} */
	@Override
	public boolean manualCleaningEnforced() {
		return mbgInfo.getManualCleaning() == MbgConstants.MB_TRT_FLAG_ON;
	}

	@Override
	public void heedManualCleaning() throws GIOException {
		mbgInfo.setManualCleaning(MbgConstants.MB_TRT_FLAG_ON);
		new MbgInfoWriter().writeProperties(this.mbgInfo);
	}

}
