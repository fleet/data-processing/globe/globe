package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.DoubleBuffer1D;
import fr.ifremer.globe.utils.exception.GIOException;

public class SwathDoubleLayerLoader implements ILayerLoader<DoubleBuffer1D> {

	private final long[] mbgShape;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	private final DoubleLoadableLayer2D mbgLayer;
	private final int[] antennaIndexes;

	/** Constructor **/
	public SwathDoubleLayerLoader(DoubleLoadableLayer2D mbgLayer, int[] antennaIndexes) throws GIOException {
		this.mbgLayer = mbgLayer;
		this.mbgShape = mbgLayer.getLoader().getDimensions();
		this.antennaIndexes = antennaIndexes;
	}

	@Override
	public long[] getDimensions() throws GIOException {
		return new long[] { mbgShape[0] };
	}

	/** Load the model layer buffer with the MBG layer **/
	@Override
	public boolean load(DoubleBuffer1D buffer) throws GIOException {
		if (loading.compareAndSet(false, true)) {
			for (int iSwath = 0; iSwath < mbgShape[0]; iSwath++) {
				buffer.set(iSwath, mbgLayer.get(iSwath, antennaIndexes[iSwath]));
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	/** Flush the MBG layer with values from the corresponding model layer **/
	@Override
	public boolean flush(DoubleBuffer1D buffer) throws GIOException {
		for (int iSwath = 0; iSwath < mbgShape[0]; iSwath++) {
			for (int iAntenna = 0; iAntenna < mbgShape[1]; iAntenna++)
				mbgLayer.set(iSwath, iAntenna, buffer.get(iSwath));
		}
		mbgLayer.flush();
		return false;
	}

}
