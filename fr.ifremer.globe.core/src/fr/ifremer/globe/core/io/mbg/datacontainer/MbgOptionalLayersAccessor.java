/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.datacontainer;

import java.util.Optional;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.OptionalLayersAccessor;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;

/**
 * Optional layers assumed in MBG.
 */
public class MbgOptionalLayersAccessor implements OptionalLayersAccessor {

	/** {@inheritDoc} */
	@Override
	public Optional<PredefinedLayers<DoubleLoadableLayer2D>> getAcrossAngleLayer() {
		return Optional.of(MbgPredefinedLayers.ACROSS_ANGLE);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<PredefinedLayers<DoubleLoadableLayer2D>> getAlongAngleLayer() {
		return Optional.of(MbgPredefinedLayers.AZIMUT_ANGLE);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<PredefinedLayers<DoubleLoadableLayer2D>> getVerticalDepthLayer() {
		return Optional.of(MbgPredefinedLayers.VERTICAL_DEPTH);
	}

}
