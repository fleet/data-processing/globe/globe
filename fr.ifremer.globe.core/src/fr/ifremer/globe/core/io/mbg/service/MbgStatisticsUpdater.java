package fr.ifremer.globe.core.io.mbg.service;

import java.util.Date;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.io.mbg.writer.MbgInfoWriter;
import fr.ifremer.globe.core.io.sounder.SounderGlobalAttributesComputer;
import fr.ifremer.globe.core.io.sounder.SounderGlobalAttributesComputer.ComputedAttributes;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileStatisticsUpdater;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class computes the global attributes of a MBG file.
 */
@Component(name = "globe_drivers_mbg_file_statistics_updater", service = IFileStatisticsUpdater.class)
public class MbgStatisticsUpdater implements IFileStatisticsUpdater {

	private static final Logger logger = LoggerFactory.getLogger(MbgStatisticsUpdater.class);

	/** {@inheritDoc} */
	@Override
	public void updateStatistics(IFileInfo fileInfo) {
		if (fileInfo.getContentType() == ContentType.MBG_NETCDF_3 && fileInfo instanceof MbgInfo) {
			try {
				updateStatistics((MbgInfo) fileInfo);
			} catch (GIOException e) {
				logger.warn("Can't update statistics on file {} : {} ", fileInfo.getPath(), e.getMessage());
			}
		}
	}

	/** Updates the statistics and write the result in the Mbg file */
	protected void updateStatistics(MbgInfo info) throws GIOException {
		// Book file
		try (ISounderDataContainerToken token = IDataContainerFactory.grab().book(info,
				IDataContainerOwner.generate())) {

			ComputedAttributes attributes = new SounderGlobalAttributesComputer().compute(token.getDataContainer());
			// Update info
			info.setGeoBox(new GeoBox(attributes.north, attributes.south, attributes.east, attributes.west));
			info.setMinDepth(-attributes.minDepth);
			info.setMaxDepth(-attributes.maxDepth);
			info.setFirstPingDate(new Date(DateUtils.nanoSecondToMilli(attributes.minTime)));
			info.setLastPingDate(new Date(DateUtils.nanoSecondToMilli(attributes.maxTime)));
		}

		// Write properties in file
		MbgInfoWriter infoWriter = new MbgInfoWriter();
		infoWriter.writeProperties(info);
		infoWriter.writeGeoBox(info);
		infoWriter.updateMinMaxDepth(info);
	}

}
