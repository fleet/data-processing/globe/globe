package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.TideLayers;
import fr.ifremer.globe.utils.exception.GIOException;

public class WaterLineLayerLoader implements ILayerLoader<FloatBuffer1D> {

	private final long[] shape;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	private final SounderDataContainer sdc;
	private MbgInfo mbgInfo;

	/**
	 * Constructor
	 */
	public WaterLineLayerLoader(SounderDataContainer sounderDataContainer) throws GIOException {
		this.sdc = sounderDataContainer;
		this.mbgInfo = (MbgInfo) sounderDataContainer.getInfo();
		this.shape =  new long[] { mbgInfo.getCycleCount()};
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * Load the depth layer with the mbg depth, tide, dynamic draught and tx_z_depth
	 * 
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(FloatBuffer1D buffer) throws GIOException {
		FloatLoadableLayer1D tideLayer = sdc.getLayer(TideLayers.TIDE_INDICATIVE);

		if (loading.compareAndSet(false, true)) {
			for (int iSwath = 0; iSwath < shape[0]; iSwath++) {
				float tide = tideLayer.get(iSwath);
				// "model_depth = depth_fcs + tide + platform_vert_offset"
				double waterlineToChartDatum = tide;
				buffer.set(iSwath, (float) waterlineToChartDatum);
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	@Override
	public boolean flush(FloatBuffer1D buffer) throws GIOException {
		return true;
	}

	
}
