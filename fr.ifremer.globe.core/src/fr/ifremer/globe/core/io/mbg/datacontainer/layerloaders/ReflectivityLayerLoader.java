package fr.ifremer.globe.core.io.mbg.datacontainer.layerloaders;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgPredefinedLayers;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.FloatBuffer2D;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

public class ReflectivityLayerLoader implements ILayerLoader<FloatBuffer2D> {

	private final long[] shape;
	protected AtomicBoolean loading = new AtomicBoolean(false);
	private final SounderDataContainer sdc;

	/**
	 * Constructor
	 */
	public ReflectivityLayerLoader(SounderDataContainer sounderDataContainer) throws GIOException {
		this.sdc = sounderDataContainer;
		this.shape = sdc.getLayer(MbgPredefinedLayers.REFLECTIVITY).getLoader().getDimensions();
	}

	/**
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#getDimensions()
	 */
	@Override
	public long[] getDimensions() throws GIOException {
		return shape;
	}

	/**
	 * Load the depth layer with the mbg depth, tide, dynamic draught and tx_z_depth
	 * 
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#load(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean load(FloatBuffer2D buffer) throws GIOException {
		// MissingValue
		
		double  missingValue = 0;
		
		try {
			MbgInfo mbgInfo = ((MbgInfo) (this.sdc.getInfo()));
			NetcdfVariable variable = mbgInfo.getFile().getVariable(MbgPredefinedLayers.REFLECTIVITY.getName());
		    missingValue = variable.getAttributeInt("missing_value");

	        double scaleFactor = variable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
	        double  addOffset = variable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
	        missingValue = (int) (missingValue * scaleFactor + addOffset);

		} catch (NCException e) {
			throw new GIOException(e.getMessage());
		}
		DoubleLoadableLayer2D mbgReflectivity = sdc.getLayer(MbgPredefinedLayers.REFLECTIVITY);

		if (loading.compareAndSet(false, true)) {
			for (int iSwath = 0; iSwath < shape[0]; iSwath++) {
				for (int iBeam = 0; iBeam < shape[1]; iBeam++) {
					double reflectivity = mbgReflectivity.get(iSwath, iBeam);
					if (reflectivity == missingValue) 
						reflectivity = Short.MAX_VALUE;
					buffer.set(iSwath, iBeam, (float) reflectivity);
				}
			}
		}
		loading.set(false);
		buffer.setDirty(false);

		return true;
	}

	/**
	 * Flush the mbg depth layer with values from the suitable coordinate system
	 * 
	 * @see fr.ifremer.globe.model.sounderdatacontainer.ILayerManager#flush(fr.ifremer.globe.utils.array.impl.LargeArray)
	 */
	@Override
	public boolean flush(FloatBuffer2D buffer) throws GIOException {
		return false; 
	}

}
