/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.gmt.info;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.Collections;
import java.util.DoubleSummaryStatistics;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.IntRange;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.math.Range;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.util.slicing.DimensionSlice;
import fr.ifremer.globe.netcdf.util.slicing.VariableSlicerFacade;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiIntToFloatFunction;

/**
 * Supplier of GMT file info <br>
 * GMT stores 2-D grids as COARDS-compliant netCDF files.
 */
@Component(name = "globe_drivers_gmt_netcdf4_file_info_supplier", service = { GmtFileInfoSupplier.class,
		IFileInfoSupplier.class, IGeographicInfoSupplier.class, IRasterInfoSupplier.class }, //
		property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer=" + FileInfoSupplierPriority.GMT_PRIORITY }//
)
public class GmtFileInfoSupplier extends BasicFileInfoSupplier<GmtFileInfo>
		implements IGeographicInfoSupplier, IRasterInfoSupplier {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(GmtFileInfoSupplier.class);

	/**
	 * Constructor
	 */
	public GmtFileInfoSupplier() {
		super("gmt", "GMT (*.gmt)", ContentType.GMT);
	}

	@Override
	public List<String> getExtensions() {
		return List.of("gmt", "grd");
	}

	@Override
	public GmtFileInfo makeFileInfo(String filePath) {
		GmtFileInfo result = null;
		try {
			result = loadInfo(filePath);
		} catch (GIOException e) {
			logger.debug("Not a GMT file {} : {}", filePath, e.getMessage());
		}
		return result;
	}

	/**
	 * @return the GmtFileInfo of the specified resource. null if filePath is not a Gmt
	 * @throws GIOException read failed, error occured
	 */
	public synchronized GmtFileInfo loadInfo(String filePath) throws GIOException {
		GmtFileInfo result = null;
		try (NetcdfFile ncfile = NetcdfFile.open(filePath, Mode.readonly)) {
			if (isCompliantGmt5(ncfile)) {
				result = makeGmt5FileInfo(filePath, ncfile);
			} else {
				result = makeGmt4FileInfo(filePath, ncfile);
			}

			double n = result.getyMinMax().getMaximumDouble() + result.getSpatialResolution().getyResolution() / 2d;
			double s = result.getyMinMax().getMinimumDouble() - result.getSpatialResolution().getyResolution() / 2d;
			double e = result.getxMinMax().getMaximumDouble() + result.getSpatialResolution().getxResolution() / 2d;
			double w = result.getxMinMax().getMinimumDouble() - result.getSpatialResolution().getxResolution() / 2d;
			result.setGeoBox(new GeoBox(n, s, e, w));
		} catch (Exception e) {
			logger.debug("Not a GMT file {} : {} ", filePath, e.getMessage());
		}
		return result;
	}

	/**
	 * Try to make a GmtFileInfo for an old GMT file
	 */
	private GmtFileInfo makeGmt4FileInfo(String filePath, NetcdfFile ncfile) throws NCException, GIOException {
		GmtFileInfo result = new GmtFileInfo(filePath);

		result.setxMinMax(readMinMaxVariale(ncfile, "x_range"));
		result.setyMinMax(readMinMaxVariale(ncfile, "y_range"));
		result.setzMinMax(readMinMaxVariale(ncfile, "z_range"));

		Range dim = readMinMaxVariale(ncfile, "dimension");
		result.setxSize(dim.getMinimumInteger());
		result.setySize(dim.getMaximumInteger());

		Range res = readMinMaxVariale(ncfile, "spacing");
		result.setSpatialResolution(new SpatialResolution(res.getMinimumDouble(), res.getMaximumDouble()));

		return result;
	}

	private Range readMinMaxVariale(NetcdfFile ncfile, String variableName) throws NCException {
		var zVar = ncfile.getVariable(variableName);
		if (zVar.getType() == Nc4prototypes.NC_FLOAT) {
			float[] val = zVar.get_float(new long[] { 0 }, new long[] { 2 });
			return new DoubleRange(val[0], val[1]);
		} else if (zVar.getType() == Nc4prototypes.NC_DOUBLE) {
			double[] val = zVar.get_double(new long[] { 0 }, new long[] { 2 });
			return new DoubleRange(val[0], val[1]);
		} else if (zVar.getType() == Nc4prototypes.NC_INT) {
			int[] val = zVar.get_int(new long[] { 0 }, new long[] { 2 });
			return new IntRange(val[0], val[1]);
		}
		throw new NCException("Unexpected type for variable " + variableName);
	}

	/**
	 * Try to make a GmtFileInfo for an old GMT file
	 */
	private GmtFileInfo makeGmt5FileInfo(String filePath, NetcdfFile ncfile) throws NCException, GIOException {
		GmtFileInfo result = new GmtFileInfo(filePath);
		result.flipRaster(true);

		DoubleBuffer x = readDoubleVariable(ncfile, "x");
		result.setxSize(x.capacity());
		double xMin = x.get(0);
		double xMax = x.get(x.capacity() - 1);
		result.setxMinMax(new DoubleRange(xMin, xMax));

		DoubleBuffer y = readDoubleVariable(ncfile, "y");
		result.setySize(y.capacity());
		double yMin = y.get(0);
		double yMax = y.get((y.capacity() - 1));
		result.setyMinMax(new DoubleRange(yMin, yMax));

		FloatBuffer z = readFloatVariable(ncfile, "z");
		DoubleSummaryStatistics zStats = new DoubleSummaryStatistics();
		z.rewind();
		while (z.hasRemaining()) {
			float value = z.get();
			if (Float.isFinite(value))
				zStats.accept(-value);
		}
		result.setzMinMax(new FloatRange(zStats.getMin(), zStats.getMax()));

		double xRes = x.get(1) - xMin;
		double yRes = (y.get(1) - yMin);
		result.setSpatialResolution(new SpatialResolution(xRes, yRes));

		return result;
	}

	/**
	 * Check if this netcdf file is a GMT compliant file
	 */
	private boolean isCompliantGmt5(NetcdfFile ncfile) throws NCException {
		String conventions = ncfile.hasAttribute("Conventions") ? ncfile.getAttributeAsString("Conventions") : null;
		if ("COARDS".equals(conventions))
			return true;

		String version = ncfile.hasAttribute("GMT_version") ? ncfile.getAttributeAsString("GMT_version") : null;
		if (version != null) {
			int dot = version.indexOf('.');
			if (dot > 0) {
				version = version.substring(0, dot);
			}
			return NumberUtils.toInt(version, 0) >= 5;
		}

		return false;
	}

	/**
	 * Return a ByteBuffer containing the values of the specified variable and type
	 */
	private ByteBuffer readVariable(NetcdfFile ncfile, String variableName, DataType targetType)
			throws NCException, GIOException {
		NetcdfVariable variable = ncfile.getVariable(variableName);
		VariableSlicerFacade slicerFacade = new VariableSlicerFacade();
		if (variable != null) {
			ByteBuffer buffer = slicerFacade
					.sliceVariable(targetType, variable, DimensionSlice.fromDimensions(variable.getShape()))
					.orElseThrow(() -> new GIOException("Unable to read " + variable + " variable"));
			buffer.rewind();
			return buffer;
		} else {
			throw new GIOException(variable + " variable not found");
		}
	}

	/**
	 * Return a FloatBuffer containing the float values of the specified variable
	 */
	private FloatBuffer readFloatVariable(NetcdfFile ncfile, String variableName) throws NCException, GIOException {
		return readVariable(ncfile, variableName, DataType.FLOAT).asFloatBuffer();
	}

	/**
	 * Return a DoubleBuffer containing the float values of the specified variable
	 */
	private DoubleBuffer readDoubleVariable(NetcdfFile ncfile, String variableName) throws NCException, GIOException {
		return readVariable(ncfile, variableName, DataType.DOUBLE).asDoubleBuffer();
	}

	@Override
	public List<RasterInfo> supplyRasterInfos(String filepath) throws GIOException {
		return getFileInfo(filepath).map(this::supplyRasterInfos).orElse(Collections.emptyList());
	}

	@Override
	public List<RasterInfo> supplyRasterInfos(IFileInfo fileInfo) {
		if (fileInfo instanceof GmtFileInfo gmtFileInfo) {
			RasterInfo result = new RasterInfo(this, gmtFileInfo.getPath()) {

				/** {@inheritDoc} */
				@Override
				public String getGdalLoadingKey() {
					return extractElevations(gmtFileInfo, new NullProgressMonitor());
				}
			};
			result.setContentType(ContentType.RASTER_GDAL);
			result.setDataType(RasterInfo.RASTER_ELEVATION);
			result.setGdalBand(1);
			result.setGeoBox(gmtFileInfo.getGeoBox());
			result.setMinMaxValues(new DoubleRange(gmtFileInfo.getzMinMax().getMinimumDouble(),
					gmtFileInfo.getzMinMax().getMaximumDouble()));
			result.setNoDataValue(Float.NaN);
			result.setProjection(gmtFileInfo.getProjection());
			result.setXSize(gmtFileInfo.getxSize());
			result.setYSize(gmtFileInfo.getySize());
			return List.of(result);
		}
		return List.of();
	}

	/**
	 * @return a Gdal dataset on a tif file containing elevations of the specified gmt file
	 */

	/**
	 * @return a Gdal dataset on a tif file containing elevations of the specified gmt file
	 */
	public String extractElevations(GmtFileInfo fileInfo, IProgressMonitor monitor) {
		try (NetcdfFile ncfile = NetcdfFile.open(fileInfo.getPath(), Mode.readonly)) {
			FloatBuffer zBuffer = readFloatVariable(ncfile, "z");
			BiIntToFloatFunction valueSupplier = (line, column) -> {
				line = fileInfo.flipRaster() ? fileInfo.getySize() - line - 1 : line;
				float value = zBuffer.get(line * fileInfo.getxSize() + column);
				// Elevations are positive down in GMT
				return Float.isFinite(value) ? -value : Float.NaN;
			};

			GeoBox geoBox = fileInfo.getGeoBox();
			String filePath = GdalUtils.generateInMemoryFilePath();
			GdalUtils.generateTiffFile(fileInfo.getySize(), fileInfo.getxSize(), Float.NaN, geoBox.getTop(),
					geoBox.getBottom(), geoBox.getLeft(), geoBox.getRight(), valueSupplier, filePath,
					GdalUtils.newProgressCallback(monitor));
			return filePath;
		} catch (NCException | GIOException e) {
			logger.warn("Error while extracting z layer of GMT file " + fileInfo.getBaseName(), e);
		}
		return null;
	}

}