/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.gmt.info;

import java.util.List;

import org.apache.commons.lang.math.Range;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier.SpatialResolution;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.model.properties.Property;

/**
 * IFileInfo dedicated to GMT file
 */
public class GmtFileInfo extends BasicFileInfo {

	/** Shape of NcVariable X */
	private int xSize;
	/** Shape of NcVariable X */
	private int ySize;
	/** MinMax values in NcVariable X */
	private Range xMinMax;
	/** MinMax values in NcVariable Y */
	private Range yMinMax;
	/** MinMax values in NcVariable Z */
	private Range zMinMax;

	/** Spatial resolutions */
	private SpatialResolution spatialResolution;

	/** True when origin of raster is left/up (true since GMT 5) */
	private boolean flipRaster;

	/** Geo projection */
	private final Projection projection;

	/**
	 * Constructor
	 */
	public GmtFileInfo(String sourcefile) {
		super(sourcefile, ContentType.GMT);
		projection = new Projection(StandardProjection.LONGLAT);
	}

	/**
	 * @see fr.ifremer.globe.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();
		result.addAll(Property.build(projection));
		result.addAll(Property.buildSpatialResolution(projection, spatialResolution.getxResolution(),
				spatialResolution.getyResolution()));
		result.addAll(Property.buildSize(getxSize(), getySize()));
		if (getzMinMax() != null) {
			result.addAll(Property.buildMinMax(getzMinMax()));
		}

		return result;
	}

	/**
	 * @return the {@link #spatialResolution}
	 */
	public SpatialResolution getSpatialResolution() {
		return spatialResolution;
	}

	/**
	 * @param spatialResolution the {@link #spatialResolution} to set
	 */
	public void setSpatialResolution(SpatialResolution spatialResolution) {
		this.spatialResolution = spatialResolution;
	}

	/**
	 * @return the {@link #projection}
	 */
	public Projection getProjection() {
		return projection;
	}

	/**
	 * @return the {@link #xMinMax}
	 */
	public Range getxMinMax() {
		return xMinMax;
	}

	/**
	 * @param xMinMax the {@link #xMinMax} to set
	 */
	public void setxMinMax(Range xMinMax) {
		this.xMinMax = xMinMax;
	}

	/**
	 * @return the {@link #yMinMax}
	 */
	public Range getyMinMax() {
		return yMinMax;
	}

	/**
	 * @param yMinMax the {@link #yMinMax} to set
	 */
	public void setyMinMax(Range yMinMax) {
		this.yMinMax = yMinMax;
	}

	/**
	 * @return the {@link #zMinMax}
	 */
	public Range getzMinMax() {
		return zMinMax;
	}

	/**
	 * @param zMinMax the {@link #zMinMax} to set
	 */
	public void setzMinMax(Range zMinMax) {
		this.zMinMax = zMinMax;
	}

	/**
	 * @return the {@link #xSize}
	 */
	public int getxSize() {
		return xSize;
	}

	/**
	 * @param xSize the {@link #xSize} to set
	 */
	public void setxSize(int xSize) {
		this.xSize = xSize;
	}

	/**
	 * @return the {@link #ySize}
	 */
	public int getySize() {
		return ySize;
	}

	/**
	 * @param ySize the {@link #ySize} to set
	 */
	public void setySize(int ySize) {
		this.ySize = ySize;
	}

	/**
	 * @return the {@link #flipRaster}
	 */
	public boolean flipRaster() {
		return flipRaster;
	}

	/**
	 * @param flipRaster the {@link #flipRaster} to set
	 */
	public void flipRaster(boolean flipRaster) {
		this.flipRaster = flipRaster;
	}

}
