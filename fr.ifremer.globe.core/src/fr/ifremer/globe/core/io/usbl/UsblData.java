package fr.ifremer.globe.core.io.usbl;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.ifremer.globe.core.utils.latlon.CoordinateDisplayType;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;

/**
 * Model class to represent an USBL data (position).
 */
public class UsblData {

	private static final ILatLongFormatter LAT_LON_FORMATER = LatLongFormater
			.getFormatter(CoordinateDisplayType.DEG_MIN_DEC);

	private static final Pattern PTSAG_FRAME_PATTERN = Pattern.compile(
			".*\\$PTSAG;#(\\d+);(\\d{2})(\\d{2})(\\d{2}).(\\d+);(\\d{1,2});(\\d{1,2});(\\d{4});(\\d+);(\\d{2})(\\d{2}.\\d+);([N|S]);(\\d{3})(\\d{2}.\\d+);([W|E]);([0-9A-F]);(\\d+.\\d+);([0|1|2]);(\\d+.\\d+)\\*.+");

	/** USBL data fields **/
	private final int id;
	private final int beaconOrShipId;
	private final Instant datetime;
	private final double latitude;
	private final double longitude;
	private boolean isValidPosition;
	private final float elevation;
	private String frame;

	/** Constructor **/
	private UsblData(int id, int beaconOrShipId, Instant datetime, double latitude, double longitude,
			boolean isValidPosition, float depth, String frame) {
		super();
		this.id = id;
		this.beaconOrShipId = beaconOrShipId;
		this.datetime = datetime;
		this.latitude = latitude;
		this.longitude = longitude;
		this.isValidPosition = isValidPosition;
		this.elevation = depth;
		this.frame = frame;
	}

	/// Getters / Setters

	public int getId() {
		return id;
	}

	/**
	 * @return true for Beacon data (computed from "Beacon / ship ID" NMEA field (0: ship, 1 to 128 : beacon))
	 */
	public boolean isBeacon() {
		return 1 <= beaconOrShipId && beaconOrShipId <= 128;
	}

	public boolean isShip() {
		return beaconOrShipId == 0;
	}

	public boolean isValid() {
		return isValidPosition;
	}

	public void setValid(boolean validity) {
		if (isValidPosition != validity) {
			this.isValidPosition = validity;

			// Update NMEA frame with new validity
			var fields = frame.split(";");
			fields[11] = isValidPosition ? "F" : "1";
			frame = String.join(";", fields);
		}
	}

	public Instant getDatetime() {
		return datetime;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public float getElevation() {
		return elevation;
	}

	public String getFrame() {
		return frame;
	}

	/**
	 * @return true if the provided frame is recognized as PTSAG.
	 */
	public static boolean accept(String frame) {
		return PTSAG_FRAME_PATTERN.matcher(frame).matches();
	}

	/**
	 * Builds {@link UsblData} from NMEA $PTSAG frame.
	 */
	public static UsblData parse(String frame) throws IOException {
		Matcher matcher = PTSAG_FRAME_PATTERN.matcher(frame);
		if (matcher.matches()) {
			try {
				// ID : Acoustic recurrence n°
				int id = Integer.parseInt(matcher.group(1));

				// Date time
				int hours = Integer.parseInt(matcher.group(2));
				int min = Integer.parseInt(matcher.group(3));
				int sec = Integer.parseInt(matcher.group(4));
				int ms = Integer.parseInt(matcher.group(5));
				int day = Integer.parseInt(matcher.group(6));
				int month = Integer.parseInt(matcher.group(7));
				int year = Integer.parseInt(matcher.group(8));
				Instant dateTime = LocalDateTime.of(year, month, day, hours, min, sec, ms * 1_000_000)
						.toInstant(ZoneOffset.UTC);

				// Beacon / ship ID (0: ship, 1 to 128 : beacon)
				int beaconorShipId = Integer.parseInt(matcher.group(9));

				// Latitude
				String latDegrees = matcher.group(10);
				String latMinutes = matcher.group(11);
				String northOrSouth = matcher.group(12);
				double latitude = LAT_LON_FORMATER.parseLatitude(northOrSouth + " " + latDegrees + " " + latMinutes);

				// Longitude
				String lonDegrees = matcher.group(13);
				String lonMinutes = matcher.group(14);
				String eastOrWest = matcher.group(15);
				double longitude = LAT_LON_FORMATER.parseLongitude(eastOrWest + " " + lonDegrees + " " + lonMinutes);

				// Position validity
				boolean isPositionValid = matcher.group(16).equals("F");

				// Depth
				int depthValidity = Integer.parseInt(matcher.group(18));
				float depth = switch (depthValidity) {
				case 1 -> Float.parseFloat(matcher.group(17)); // Calculated
				case 2 -> Float.parseFloat(matcher.group(19)); // Sensor
				default -> Float.NaN; // 0 = "No depth"
				};

				return new UsblData(id, beaconorShipId, dateTime, latitude, longitude, isPositionValid, -depth, frame);
			} catch (Exception e) {
				throw new IOException("Error while parsing frame : " + frame, e);
			}
		} else {
			throw new IOException("Error while parsing frame (not recognized as PTSAG frame): " + frame);
		}
	}

}
