/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.usbl;

import java.time.Instant;

import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Settings of USBL file.
 */
public record UsblSettings(
		/** Metadata **/
		String survey, String description,
		/** Target position */
		boolean displayTarget, double targetLatitude, double targetLongitude, int targetRadius, //
		/** Landing area parameters */
		boolean displayLanding, boolean displayAllPointsBeforeLanding, long landingDateTimeEpochMilli,
		int durationBeforeLandingInSeconds, //
		float opacity) {

	public UsblSettings copyWith(String survey, String description) {
		return new UsblSettings(survey, description, displayTarget, targetLatitude, targetLongitude, targetRadius,
				displayLanding, displayAllPointsBeforeLanding, landingDateTimeEpochMilli,
				durationBeforeLandingInSeconds, opacity);
	}

	public UsblSettings copyWith(
			/** Target position */
			boolean displayTarget, double targetLatitude, double targetLongitude, int targetRadius, //
			/** Landing area parameters */
			boolean displayLanding, boolean displayAllPointsBeforeLanding, long landingDateTimeEpochMilli,
			int durationBeforeLandingInSeconds, //
			float opacity) {
		return new UsblSettings(survey, description, displayTarget, targetLatitude, targetLongitude, targetRadius,
				displayLanding, displayAllPointsBeforeLanding, landingDateTimeEpochMilli,
				durationBeforeLandingInSeconds, opacity);

	}

	/**
	 * Landing date time is store as long EpochMilli to avoid serialization issue during session save/restore.
	 * 
	 * @return
	 */
	public Instant landingDateTime() {
		return Instant.ofEpochMilli(landingDateTimeEpochMilli);
	}

	/**
	 * @return true if data is inside the period before landing.
	 */
	public boolean isDataBeforeLanding(UsblData usblData) {
		var start = landingDateTime().minusSeconds(durationBeforeLandingInSeconds());
		var end = landingDateTime();
		var datetime = usblData.getDatetime();
		return (datetime.isAfter(start) && datetime.isBefore(end)) || datetime.equals(start) || datetime.equals(end);
	}

	/** Key used to save this bean in session */
	public static final TypedKey<UsblSettings> SESSION_KEY = new TypedKey<>("usbl_layer_parameters");

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<UsblSettings> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

}
