package fr.ifremer.globe.core.io.usbl;

import java.io.IOException;
import java.util.List;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * {@link INavigationDataSupplier} implementation for {@link UsblFileInfo}.
 */
public class UsblNavigation implements INavigationDataSupplier {
	public final static NavigationMetadataType<Integer> IS_BEFORE_LANDING = new NavigationMetadataType<>(
			"Before landing (=1)", "");

	private final UsblFileInfo fileInfo;

	/** Constructor **/
	public UsblNavigation(UsblFileInfo mgdFileInfo) {
		this.fileInfo = mgdFileInfo;
	}

	@Override
	public INavigationData getNavigationData(boolean readonly) {
		return new UsblNavigationData(fileInfo);
	}

	/**
	 * {@link INavigationData} implementation from {@link UsblFileInfo}.
	 */
	private class UsblNavigationData implements INavigationData {

		private final List<UsblData> data;
		private final List<NavigationMetadata<?>> metadata;

		/** Constructor **/
		protected UsblNavigationData(UsblFileInfo fileInfo) {
			// Keep only valid beacon position.
			data = fileInfo.getBeaconData().stream().filter(UsblData::isValid).toList();
			metadata = List.of(
					NavigationMetadataType.TIME
							.withValueSupplier(index -> data.get(index).getDatetime().toEpochMilli()),
					NavigationMetadataType.ELEVATION.withValueSupplier(index -> data.get(index).getElevation()),
					IS_BEFORE_LANDING.withValueSupplier(
							index -> fileInfo.getSettings().isDataBeforeLanding(data.get(index)) ? 1 : 0));
		}

		@Override
		public void close() throws IOException {
			// nothing to do
		}

		@Override
		public int size() {
			return data.size();
		}

		@Override
		public String getFileName() {
			return fileInfo.getFilename();
		}

		@Override
		public ContentType getContentType() {
			return fileInfo.getContentType();
		}

		@Override
		public double getLatitude(int index) throws GIOException {
			return data.get(index).getLatitude();
		}

		@Override
		public double getLongitude(int index) throws GIOException {
			return data.get(index).getLongitude();
		}

		@Override
		public List<NavigationMetadata<?>> getMetadata() {
			return metadata;
		}

	}

}
