package fr.ifremer.globe.core.io.usbl;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Supplier of {@link UsblFileInfo} files.
 */
@Component(name = "globe_drivers_usbl_file_info_supplier", //
		service = { UsblFileInfoSupplier.class, IFileInfoSupplier.class, INavigationSupplier.class }, property = {
				FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer=" + FileInfoSupplierPriority.USBL_PRIORITY })
public class UsblFileInfoSupplier extends BasicFileInfoSupplier<UsblFileInfo> implements INavigationSupplier {

	/** Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(UsblFileInfoSupplier.class);

	public static final String EXTENSION_USBL = "csv";

	/**
	 * Constructor
	 */
	public UsblFileInfoSupplier() {
		super(EXTENSION_USBL, "USBL file", ContentType.USBL);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<UsblFileInfo> getFileInfo(String filePath) {
		return Optional.ofNullable(accept(filePath) ? makeFileInfo(filePath) : null);
	}

	private Stream<String> readFile(String filePath) throws IOException {
		return Files.lines(Paths.get(filePath));
	}

	public boolean accept(String filePath) {
		try {
			return hasRightExtension(filePath) && readFile(filePath).findFirst().filter(UsblData::accept).isPresent();
		} catch (IOException | UncheckedIOException e) {
			return false;
		}
	}

	@Override
	public UsblFileInfo makeFileInfo(String filePath) {
		int i = 1;
		try {
			var data = new ArrayList<UsblData>();
			for (var line : readFile(filePath).toList()) {
				data.add(UsblData.parse(line));
				i++;
			}

			if (data.isEmpty()) {
				LOGGER.error(
						"Error while reading USBL file : {} , no valid data (with valid position and beacon flag).",
						filePath);
				return null;
			}

			return new UsblFileInfo(filePath, data);
		} catch (IOException | GIOException e) {
			LOGGER.error("Error while reading USBL file (ligne {}) : {} ", i, filePath, e);
			return null;
		}
	}

	/**
	 * @return {@link INavigationDataSupplier} for {@link UsblFileInfo}.
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return fileInfo instanceof UsblFileInfo usblFileInfo ? Optional.of(new UsblNavigation(usblFileInfo))
				: Optional.empty();
	}

}
