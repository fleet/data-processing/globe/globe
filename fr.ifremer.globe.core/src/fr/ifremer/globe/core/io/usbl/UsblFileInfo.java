package fr.ifremer.globe.core.io.usbl;

import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * USBL (ultra-short baseline) file info.
 */
public class UsblFileInfo extends BasicFileInfo {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(UsblFileInfo.class);

	/** USBL data */
	private final List<UsblData> data;

	/** Start / end dates */
	private Optional<Instant> optStartDate = Optional.empty();
	private Optional<Instant> optEndDate = Optional.empty();

	/** Settings **/
	private UsblSettings settings;

	/**
	 * Constructor
	 */
	public UsblFileInfo(String filePath, List<UsblData> data) throws GIOException {
		super(filePath, ContentType.USBL);
		this.data = data;

		// compute statistics
		for (UsblData currentData : getBeaconData()) {
			Instant date = currentData.getDatetime();
			if (optStartDate.isEmpty() || date.compareTo(optStartDate.get()) < 0)
				optStartDate = Optional.of(date);
			if (optEndDate.isEmpty() || date.compareTo(optEndDate.get()) > 0)
				optEndDate = Optional.of(date);
		}

		geoBox = new UsblNavigation(this).getNavigationData(true).computeGeoBox();

		// Compute default USBL layer parameters (used to display target and landing areas)

		// Suppose target == ship position
		var shipData = data.stream().filter(UsblData::isShip).toList();
		var latTarget = shipData.stream().mapToDouble(UsblData::getLatitude).average().orElse(Double.NaN);
		var lonTarget = shipData.stream().mapToDouble(UsblData::getLongitude).average().orElse(Double.NaN);

		// Suppose landing = beacon deepest position
		var landingDatetime = getBeaconData().stream()
				.sorted((d1, d2) -> Float.compare(d1.getElevation(), d2.getElevation())).findFirst()
				.map(UsblData::getDatetime).orElseThrow();

		this.settings = new UsblSettings("", "", //
				true, latTarget, lonTarget, 25, // target
				true, true, landingDatetime.toEpochMilli(), 60, // landing
				0.40f // opacity
		);
	}

	public List<UsblData> getAllData() {
		return data;
	}

	public List<UsblData> getBeaconData() {
		return data.stream().filter(UsblData::isBeacon).toList();
	}

	public List<UsblData> getDataBeforeLanding() {
		return data.stream().filter(UsblData::isBeacon).filter(UsblData::isValid).filter(settings::isDataBeforeLanding).toList();
	}

	public double getAverageLatitudeBeforeLanding() {
		return getDataBeforeLanding().stream().mapToDouble(UsblData::getLatitude).average().orElse(Double.NaN);
	}

	public double getAverageLongitudeBeforeLanding() {
		return getDataBeforeLanding().stream().mapToDouble(UsblData::getLongitude).average().orElse(Double.NaN);
	}

	public int size() {
		return data.size();
	}

	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		return optStartDate.isPresent() && optEndDate.isPresent()
				? Optional.of(Pair.of(optStartDate.get(), optEndDate.get()))
				: Optional.empty();
	}

	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();
		result.add(Property.build("Number of points", data.size()));
		optStartDate.ifPresent(startDat -> result.add(Property.build("Start date", startDat)));
		optEndDate.ifPresent(endDate -> result.add(Property.build("End date", endDate)));
		result.add(Property.build("Survey", settings.survey()));
		result.add(Property.build("Description", settings.description()));
		return result;
	}

	public boolean flush() {
		try {
			Files.write(toPath(), data.stream().map(UsblData::getFrame).toList());
			return true;
		} catch (IOException e) {
			logger.error("Error while writing USBL file {} : {}", getFilename(), e.getMessage(), e);
			return false;
		}
	}

	// Getters & setters

	public UsblSettings getSettings() {
		return settings;
	}

	public void setSettings(UsblSettings usblSettings) {
		this.settings = usblSettings;
	}

}