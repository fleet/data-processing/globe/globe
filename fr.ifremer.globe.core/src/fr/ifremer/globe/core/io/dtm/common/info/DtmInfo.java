/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.dtm.common.info;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier.SpatialResolution;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.raster.RasterInfo;

/**
 * IInfoStore dedicated to dtm file.<br>
 * Gather all dtm file metadata
 *
 */
public class DtmInfo extends BasicFileInfo {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(DtmInfo.class);

	/** History attribute */
	private String history;

	/** Geo projection */
	private Projection projection;
	/** Spatial resolutions */
	private SpatialResolution spatialResolution;
	/** Transformation matrix to get geographic coordinates from raster grid coordinates */
	private double[] geoTransform;

	/** All embedded raster in this Dtm file */
	private List<RasterInfo> rasterInfos = new ArrayList<>();

	/** All embedded CDI in this Dtm file */
	private List<String> cdis = new ArrayList<>();

	/** All embedded layers in this Dtm file */
	private List<String> layers = new ArrayList<>();

	/** Computer able to initialize tho statistics of a RasterInfo */
	private final Consumer<RasterInfo> rasterInfoStatisticsComputer;

	/**
	 * Constructor
	 */
	public DtmInfo(String sourcefile, ContentType contentType, Consumer<RasterInfo> rasterInfoStatisticsComputer) {
		super(sourcefile, contentType);
		this.rasterInfoStatisticsComputer = rasterInfoStatisticsComputer;
	}

	/** Getter of {@link #projection} */
	public Projection getProjection() {
		return projection;
	}

	/** Setter of {@link #projection} */
	public void setProjection(Projection projection) {
		this.projection = projection;
		try {
			projection.setProjectedGeoBox(geoBox);
		} catch (ProjectionException e) {
			logger.debug("Projection error : ", e);
		}

	}

	/**
	 * @see fr.ifremer.globe.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();

		result.addAll(Property.build(projection));

		RasterInfo elevationRaster = getRasterInfo(RasterInfo.RASTER_ELEVATION).orElse(null);
		if (elevationRaster != null) {
			result.addAll(Property.buildSpatialResolution(projection, spatialResolution.getxResolution(),
					spatialResolution.getyResolution()));
			result.addAll(Property.buildSize(elevationRaster.getXSize(), elevationRaster.getYSize()));
			result.addAll(Property.buildMinMax(elevationRaster.getMinMaxValues()));

			// List all rasters and their statistics
			var layersProps = Property.build("Layers", "");
			for (RasterInfo rasterInfo : rasterInfos) {
				var layerProps = Property.build(rasterInfo.getDataType(), "");
				layersProps.add(layerProps);
				if (rasterInfo.isFullyLoaded()) {
					rasterInfo.getProperties().forEach(layerProps::add);
				} else {
					// Require the statistics computation
					rasterInfoStatisticsComputer.accept(rasterInfo);
					layerProps.add(Property.build("Statistics", "Computation in progress..."));
				}
			}
			result.add(layersProps);
		}

		result.add(Property.buildDetailed("Linked ID values", getCdis()));
		if (history != null) {
			result.add(Property.build("History", history, 30));
		}

		return result;
	}

	/** Add a RasterInfo to the list of embeded RasterInfo */
	public void add(RasterInfo rasterInfo) {
		rasterInfos.add(rasterInfo);
	}

	/**
	 * @return the {@link #rasterInfos}
	 */
	public List<RasterInfo> getRasterInfos() {
		return rasterInfos;
	}

	public Optional<RasterInfo> getRasterInfo(String rasterElevation) {
		return rasterInfos.stream().filter(rasterInfo -> rasterElevation.equalsIgnoreCase(rasterInfo.getDataType()))
				.findFirst();
	}

	/**
	 * @return all CDIs
	 */
	public List<String> getCdis() {
		return cdis;
	}

	/**
	 * @return all Layers
	 */
	public List<String> getLayers() {
		return layers;
	}

	/**
	 * @return the {@link #history}
	 */
	public String getHistory() {
		return history;
	}

	/**
	 * @param history the {@link #history} to set
	 */
	public void setHistory(String history) {
		this.history = history;
	}

	/**
	 * @return the {@link #spatialResolution}
	 */
	public SpatialResolution getSpatialResolution() {
		return spatialResolution;
	}

	/**
	 * @param spatialResolution the {@link #spatialResolution} to set
	 */
	public void setSpatialResolution(SpatialResolution spatialResolution) {
		this.spatialResolution = spatialResolution;
	}

	/**
	 * @return the {@link #geoTransform}
	 */
	public double[] getGeoTransform() {
		return geoTransform;
	}

	public void setGeoTransform(double[] geoTransform) {
		this.geoTransform = geoTransform;

	}

}
