/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.dtm.netcdf4.service.raster;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.dtm.netcdf4.info.DtmInfoSupplier;
import fr.ifremer.globe.core.model.raster.GdalRasterReader;
import fr.ifremer.globe.core.model.raster.IRasterReader;
import fr.ifremer.globe.core.model.raster.IRasterReaderFactory;
import fr.ifremer.globe.core.model.raster.RasterInfo;

/**
 * IRasterReaderFactory to create a IRasterReader for Dtm File
 */
@Component(name = "globe_drivers_dtm_netcdf4_raster_reader_factory", service = IRasterReaderFactory.class)
public class DtmRasterService implements IRasterReaderFactory {

	/**
	 * Loader of DTM
	 */
	@Reference
	protected DtmInfoSupplier dtmInfoLoader;

	@Override
	public Optional<IRasterReader> createRasterReader(RasterInfo rasterInfo) {
		IRasterReader result = null;
		if (rasterInfo.getSupplier() == dtmInfoLoader) {
			result = new GdalRasterReader();
		}
		return Optional.ofNullable(result);
	}

}
