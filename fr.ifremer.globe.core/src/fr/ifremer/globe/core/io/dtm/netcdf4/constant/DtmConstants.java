/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.dtm.netcdf4.constant;

/**
 * Class containing all naming, convention for 'new" dtm format, this one is inspired from Gebco dtm format
 */
public class DtmConstants {

	/** Version attribute name **/
	public static final String VERSION_ATTRIBUTE_NAME = "dtm_convention_version";

	/** Kind of layer containing longitudes Degrees east */
	public static final String LAYER_LAT_NAME = "lat";
	/** Kind of layer containing latitudes Degrees north */
	public static final String LAYER_LON_NAME = "lon";
	/** Kind of layer containing elevation relative to sea level */
	public static final String LAYER_ELEVATION = "elevation";
	/** Kind of layer containing Smoothed elevation relative to sea level, computing with elevation variable */
	public static final String LAYER_ELEVATION_SMOOTHED = "elevation_smoothed";
	/** Kind of layer containing min elevation value over a cell, relative to Lowest Astronomical Tide */
	public static final String LAYER_ELEVATION_MIN = "elevation_min";
	/** Kind of layer containing max elevation value over a cell, relative to Lowest Astronomical Tide */
	public static final String LAYER_ELEVATION_MAX = "elevation_max";
	/** Kind of layer containing Standard Deviation of elevation data over cell */
	public static final String LAYER_STDEV = "stdev";
	/** Kind of layer containing the number of values used to calculate the resulting elevation over the cell */
	public static final String LAYER_VALUE_COUNT = "value_count";
	/**
	 * Kind of layer containing the number of values matching the cell coordinates but not used in mean elevation
	 * computation, these could be invalid soundings or rejected with filters
	 */
	public static final String LAYER_FILTERED_COUNT = "filtered_sounding";
	/**
	 * Kind of layer containing the indicator of cell processed as extrapolation of the neighbouring cells (absence of
	 * real soundings data)
	 */
	public static final String LAYER_INTERPOLATION_FLAG = "interpolation_flag";
	/** Kind of layer containing backscatter value over a cell */
	public static final String LAYER_BACKSCATTER = "backscatter";
	public static final String LAYER_MIN_ACROSS_DISTANCE = "min_across_distance";
	public static final String LAYER_MAX_ACROSS_DISTANCE = "max_across_distance";
	public static final String LAYER_MAX_ACROSS_ANGLE = "max_across_angle";

	/** Kind of layer containing the CDI index of this cell. matching CDI information is retrieved from LAYER_CDI */
	public static final String LAYER_CDI_INDEX = "cdi_index";
	/** Kind of layer containing the ID of related CDI metadata record */
	public static final String LAYER_CDI = "cdi_reference";

	/**
	 * Constructor
	 */
	private DtmConstants() {
	}
}
