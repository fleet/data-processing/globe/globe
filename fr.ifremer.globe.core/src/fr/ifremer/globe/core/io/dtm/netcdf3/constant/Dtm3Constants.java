/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.dtm.netcdf3.constant;

/**
 * Class containing all naming, convention for "old" dtm format
 */
public class Dtm3Constants {

	/** Version attribute name **/
	public static final String VERSION_ATTRIBUTE_NAME = "mbVersion";

	/** Kind of layer containing depth relative to sea level */
	public static final String LAYER_DEPTH = "DEPTH";
	/** Kind of layer containing the ID of related CDI metadata record */
	public static final String LAYER_CDI_INDEX = "CDI_INDEX";
	/** Kind of layer containing backscatter value over a cell */
	public static final String LAYER_REFLECTIVITY = "REFLECTIVITY";
	/** Kind of layer containing min elevation value over a cell */
	public static final String LAYER_MIN_SOUNDING = "MIN_SOUNDING";
	/** Kind of layer containing max elevation value over a cell */
	public static final String LAYER_MAX_SOUNDING = "MAX_SOUNDING";
	/** Kind of layer containing Smoothed depeth relative to sea level, computing with depth variable */
	public static final String LAYER_DEPTH_SMOOTH = "DEPTH_SMOOTH";
	/** Kind of layer containing the number of values used to calculate the resulting elevation over the cell */
	public static final String LAYER_VSOUNDINGS = "VSOUNDINGS";
	/**
	 * Kind of layer containing the number of values matching the cell coordinates but not used in mean elevation
	 * computation, these could be invalid soundings or rejected with filters
	 */
	public static final String LAYER_FILTERED_COUNT = "ISOUNDINGS";
	/** Kind of layer containing Standard Deviation of elevation data over cell */
	public static final String LAYER_STDEV = "STDEV";

	/** Kind of layer containing longitudes Degrees east */
	public static final String LAYER_LAT_NAME = "lat";
	/** Kind of layer containing latitudes Degrees north */
	public static final String LAYER_LON_NAME = "lon";
	/** Kind of layer containing the CDI index of this cell. matching CDI information is retrieved from LAYER_CDI */
	public static final String LAYER_CDI = "CDI";
	/**
	 * Kind of layer containing the indicator of cell processed as extrapolation of the neighbouring cells (absence of
	 * real soundings data)
	 */
	public static final String LAYER_INTERPOLATION_FLAG = "INTERPOLATION_FLAG";

	public static final String LAYER_MIN_ACROSS_DISTANCE = "MIN_ACROSS_DISTANCE";
	public static final String LAYER_MAX_ACROSS_DISTANCE = "MAX_ACROSS_DISTANCE";

	/**
	 * Constructor
	 */
	private Dtm3Constants() {
	}
}
