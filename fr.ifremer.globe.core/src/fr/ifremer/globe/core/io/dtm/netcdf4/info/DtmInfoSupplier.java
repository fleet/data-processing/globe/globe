/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.dtm.netcdf4.info;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.math.DoubleRange;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.dtm.common.info.AbstractDtmInfoSupplier;
import fr.ifremer.globe.core.io.dtm.common.info.DtmInfo;
import fr.ifremer.globe.core.io.dtm.netcdf4.constant.DtmConstants;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.ICdiSupplier;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Supplier of DTM file info specific for DTM in netcdf 4 format
 */
@Component(name = "globe_drivers_dtm_netcdf4_file_info_supplier", service = { DtmInfoSupplier.class,
		IFileInfoSupplier.class, IRasterInfoSupplier.class, ICdiSupplier.class, IGeographicInfoSupplier.class })
public class DtmInfoSupplier extends AbstractDtmInfoSupplier {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(DtmInfoSupplier.class);

	/** Supported DTM must have a value for the global attribute between 0 and 1.0 */
	protected static final DoubleRange SUPPORTED_VERSION = new DoubleRange(0d, 1d);

	/**
	 * Constructor
	 */
	public DtmInfoSupplier() {
		super("nc", "Dtm (*.nc)", ContentType.DTM_NETCDF_4);
	}

	@Override
	public List<String> getExtensions() {
		return List.of("dtm.nc", "nc");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return List.of(new Pair<>("Dtm (*.dtm.nc)", "*.dtm.nc"), new Pair<>("Dtm (*.nc)", "*.nc"));
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<String> getLayerName(String rasterName) {
		switch (rasterName) {
		case RasterInfo.RASTER_ELEVATION:
			return Optional.of(DtmConstants.LAYER_ELEVATION);
		case RasterInfo.RASTER_BACKSCATTER:
			return Optional.of(DtmConstants.LAYER_BACKSCATTER);
		case RasterInfo.RASTER_ELEVATION_MIN:
			return Optional.of(DtmConstants.LAYER_ELEVATION_MIN);
		case RasterInfo.RASTER_ELEVATION_MAX:
			return Optional.of(DtmConstants.LAYER_ELEVATION_MAX);
		case RasterInfo.RASTER_ELEVATION_SMOOTHED:
			return Optional.of(DtmConstants.LAYER_ELEVATION_SMOOTHED);
		case RasterInfo.RASTER_VALUE_COUNT:
			return Optional.of(DtmConstants.LAYER_VALUE_COUNT);
		case RasterInfo.RASTER_FILTERED_COUNT:
			return Optional.of(DtmConstants.LAYER_FILTERED_COUNT);
		case RasterInfo.RASTER_STDEV:
			return Optional.of(DtmConstants.LAYER_STDEV);
		case RasterInfo.RASTER_CDI_INDEX:
			return Optional.of(DtmConstants.LAYER_CDI_INDEX);
		case RasterInfo.RASTER_INTERPOLATION_FLAG:
			return Optional.of(DtmConstants.LAYER_INTERPOLATION_FLAG);
		case RasterInfo.RASTER_MIN_ACROSS_DISTANCE:
			return Optional.of(DtmConstants.LAYER_MIN_ACROSS_DISTANCE);
		case RasterInfo.RASTER_MAX_ACROSS_DISTANCE:
			return Optional.of(DtmConstants.LAYER_MAX_ACROSS_DISTANCE);
		case RasterInfo.RASTER_MAX_ACROSS_ANGLE:
			return Optional.of(DtmConstants.LAYER_MAX_ACROSS_ANGLE);
		default:
			return Optional.of(rasterName);
		}

	}

	/** {@inheritDoc} */
	@Override
	protected Optional<String> getRasterName(String layerName) {
		switch (layerName) {
		case DtmConstants.LAYER_ELEVATION:
			return Optional.of(RasterInfo.RASTER_ELEVATION);
		case DtmConstants.LAYER_BACKSCATTER:
			return Optional.of(RasterInfo.RASTER_BACKSCATTER);
		case DtmConstants.LAYER_ELEVATION_MIN:
			return Optional.of(RasterInfo.RASTER_ELEVATION_MIN);
		case DtmConstants.LAYER_ELEVATION_MAX:
			return Optional.of(RasterInfo.RASTER_ELEVATION_MAX);
		case DtmConstants.LAYER_ELEVATION_SMOOTHED:
			return Optional.of(RasterInfo.RASTER_ELEVATION_SMOOTHED);
		case DtmConstants.LAYER_VALUE_COUNT:
			return Optional.of(RasterInfo.RASTER_VALUE_COUNT);
		case DtmConstants.LAYER_FILTERED_COUNT:
			return Optional.of(RasterInfo.RASTER_FILTERED_COUNT);
		case DtmConstants.LAYER_STDEV:
			return Optional.of(RasterInfo.RASTER_STDEV);
		case DtmConstants.LAYER_CDI_INDEX:
			return Optional.of(RasterInfo.RASTER_CDI_INDEX);
		case DtmConstants.LAYER_INTERPOLATION_FLAG:
			return Optional.of(RasterInfo.RASTER_INTERPOLATION_FLAG);
		case DtmConstants.LAYER_MIN_ACROSS_DISTANCE:
			return Optional.of(RasterInfo.RASTER_MIN_ACROSS_DISTANCE);
		case DtmConstants.LAYER_MAX_ACROSS_DISTANCE: 
			return Optional.of(RasterInfo.RASTER_MAX_ACROSS_DISTANCE);
		case DtmConstants.LAYER_MAX_ACROSS_ANGLE:
			return Optional.of(RasterInfo.RASTER_MAX_ACROSS_ANGLE);
		default:
			return Optional.of(layerName);
		}
	}

	/** Grab attributes from file and set values on DtmInfo */
	@Override
	protected void fillAttributes(NCFile ncfile, DtmInfo result) {
		try {
			String history = ncfile.getAttributeAsString("history");
			result.setHistory(history);
		} catch (NCException e) {
			// Attribute ignored
		}
	}

	/**
	 * @return true when file's version is supported
	 */
	@Override
	protected boolean isSupported(NCFile ncfile) throws NCException {
		boolean result = false;
		if (ncfile.hasAttribute(DtmConstants.VERSION_ATTRIBUTE_NAME)) {
			try {
				double numVersion = Double
						.parseDouble(ncfile.getAttributeAsString(DtmConstants.VERSION_ATTRIBUTE_NAME));
				result = SUPPORTED_VERSION.containsDouble(numVersion)
						&& ncfile.getVariable(DtmConstants.LAYER_ELEVATION) != null;
			} catch (NumberFormatException e) {
				// Bad version number
			}
		}
		return result;
	}

	/**
	 * Load CDIs
	 */
	@Override
	protected void addCdis(DtmInfo dtmInfo, NCFile ncfile) throws NCException {
		NCVariable cdiVariable = ncfile.getVariable(DtmConstants.LAYER_CDI);
		if (cdiVariable != null) {
			long cdiCount = cdiVariable.getShape().get(0).getLength();
			for_all_cdi: for (long cdiIndex = 0; cdiIndex < cdiCount; cdiIndex += 1024) {
				long[] start = { cdiIndex };
				long[] count = { Math.min(1024, cdiCount - cdiIndex) };
				//
				String[] cdis = cdiVariable.get_string(start, count);
				for (int i = 0; i < cdis.length; i++) {
					if (cdis[i] == null || cdis[i].isEmpty()) {
						break for_all_cdi;
					}
					dtmInfo.getCdis().add(cdis[i]);
				}
			}
		}
	}

}