/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.dtm.netcdf3.info;

import java.util.Optional;

import org.apache.commons.lang.math.DoubleRange;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.dtm.common.info.AbstractDtmInfoSupplier;
import fr.ifremer.globe.core.io.dtm.common.info.DtmInfo;
import fr.ifremer.globe.core.io.dtm.netcdf3.constant.Dtm3Constants;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.ICdiSupplier;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Supplier of DTM file info specific for DTM in netcdf 4 format
 */
@Component(name = "globe_drivers_dtm_netcdf3_file_info_supplier", service = { IFileInfoSupplier.class,
		IRasterInfoSupplier.class, ICdiSupplier.class, IGeographicInfoSupplier.class })
public class Dtm3InfoSupplier extends AbstractDtmInfoSupplier {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(Dtm3InfoSupplier.class);

	/** Supported DTM must have a value for the global attribute between 2 and 2.1 */
	protected static final DoubleRange SUPPORTED_VERSION = new DoubleRange(200d, 210d);

	/**
	 * Constructor
	 */
	public Dtm3InfoSupplier() {
		super("dtm", "Dtm (*.dtm)", ContentType.DTM_NETCDF_3);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<String> getLayerName(String rasterName) {
		switch (rasterName) {
		case RasterInfo.RASTER_ELEVATION:
			return Optional.of(Dtm3Constants.LAYER_DEPTH);
		case RasterInfo.RASTER_BACKSCATTER:
			return Optional.of(Dtm3Constants.LAYER_REFLECTIVITY);
		case RasterInfo.RASTER_ELEVATION_MIN:
			return Optional.of(Dtm3Constants.LAYER_MIN_SOUNDING);
		case RasterInfo.RASTER_ELEVATION_MAX:
			return Optional.of(Dtm3Constants.LAYER_MAX_SOUNDING);
		case RasterInfo.RASTER_ELEVATION_SMOOTHED:
			return Optional.of(Dtm3Constants.LAYER_DEPTH_SMOOTH);
		case RasterInfo.RASTER_VALUE_COUNT:
			return Optional.of(Dtm3Constants.LAYER_VSOUNDINGS);
		case RasterInfo.RASTER_FILTERED_COUNT:
			return Optional.of(Dtm3Constants.LAYER_FILTERED_COUNT);
		case RasterInfo.RASTER_STDEV:
			return Optional.of(Dtm3Constants.LAYER_STDEV);
		case RasterInfo.RASTER_CDI_INDEX:
			return Optional.of(Dtm3Constants.LAYER_CDI);
		case RasterInfo.RASTER_INTERPOLATION_FLAG:
			return Optional.of(Dtm3Constants.LAYER_INTERPOLATION_FLAG);
		case RasterInfo.RASTER_MIN_ACROSS_DISTANCE:
			return Optional.of(Dtm3Constants.LAYER_MIN_ACROSS_DISTANCE);
		case RasterInfo.RASTER_MAX_ACROSS_DISTANCE:
			return Optional.of(Dtm3Constants.LAYER_MAX_ACROSS_DISTANCE);
		default:
			return Optional.empty();
		}

	}

	/** {@inheritDoc} */
	@Override
	protected Optional<String> getRasterName(String layerName) {
		switch (layerName) {
		case Dtm3Constants.LAYER_DEPTH:
			return Optional.of(RasterInfo.RASTER_ELEVATION);
		case Dtm3Constants.LAYER_REFLECTIVITY:
			return Optional.of(RasterInfo.RASTER_BACKSCATTER);
		case Dtm3Constants.LAYER_MIN_SOUNDING:
			return Optional.of(RasterInfo.RASTER_ELEVATION_MIN);
		case Dtm3Constants.LAYER_MAX_SOUNDING:
			return Optional.of(RasterInfo.RASTER_ELEVATION_MAX);
		case Dtm3Constants.LAYER_DEPTH_SMOOTH:
			return Optional.of(RasterInfo.RASTER_ELEVATION_SMOOTHED);
		case Dtm3Constants.LAYER_VSOUNDINGS:
			return Optional.of(RasterInfo.RASTER_VALUE_COUNT);
		case Dtm3Constants.LAYER_FILTERED_COUNT:
			return Optional.of(RasterInfo.RASTER_FILTERED_COUNT);
		case Dtm3Constants.LAYER_STDEV:
			return Optional.of(RasterInfo.RASTER_STDEV);
		case Dtm3Constants.LAYER_CDI:
			return Optional.of(RasterInfo.RASTER_CDI_INDEX);
		case Dtm3Constants.LAYER_INTERPOLATION_FLAG:
			return Optional.of(RasterInfo.RASTER_INTERPOLATION_FLAG);
		case Dtm3Constants.LAYER_MIN_ACROSS_DISTANCE:
			return Optional.of(RasterInfo.RASTER_MIN_ACROSS_DISTANCE);
		case Dtm3Constants.LAYER_MAX_ACROSS_DISTANCE: 
			return Optional.of(RasterInfo.RASTER_MAX_ACROSS_DISTANCE);
		default:
			return Optional.of(layerName);
		}
	}
	/** Grab attributes from file and set values on DtmInfo */
	@Override
	protected void fillAttributes(NCFile ncfile, DtmInfo result) {
		// Nothing to do
	}

	/**
	 * @return true when file's version is supported
	 */
	@Override
	protected boolean isSupported(NCFile ncfile) throws NCException {
		boolean result = false;
		if (ncfile.hasAttribute(Dtm3Constants.VERSION_ATTRIBUTE_NAME)) {
			double numVersion = ncfile.getAttributeDouble(Dtm3Constants.VERSION_ATTRIBUTE_NAME);
			result = SUPPORTED_VERSION.containsDouble(numVersion)
					&& ncfile.getVariable(Dtm3Constants.LAYER_DEPTH) != null;
		}
		return result;
	}

	/**
	 * Load CDIs
	 */
	@Override
	protected void addCdis(DtmInfo dtmInfo, NCFile ncfile) throws NCException {
		NCVariable cdiVariable = ncfile.getVariable(Dtm3Constants.LAYER_CDI_INDEX);
		if (cdiVariable != null) {
			long cdiCount = cdiVariable.getShape().get(0).getLength();
			for (long cdiIndex = 0; cdiIndex < cdiCount; cdiIndex++) {
				long[] start = { cdiIndex, 0 };
				long[] count = { 1, cdiVariable.getShape().get(1).getLength() };
				//
				byte[] cars = cdiVariable.get_byte(start, count);
				if (cars[0] == 0) {
					break;
				}
				dtmInfo.getCdis().add(new String(cars));
			}
		}
	}

}