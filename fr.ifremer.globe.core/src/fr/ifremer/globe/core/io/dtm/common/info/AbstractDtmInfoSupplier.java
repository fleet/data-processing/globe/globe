/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.dtm.common.info;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.jobs.JobGroup;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.ICdiSupplier;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Template method for supplying DTM file info. Builder of DtmInfo <br>
 * Common implementation for DTM file in netcdf 3 or 4 format
 */
public abstract class AbstractDtmInfoSupplier extends BasicFileInfoSupplier<DtmInfo>
		implements IRasterInfoSupplier, ICdiSupplier, IGeographicInfoSupplier {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(AbstractDtmInfoSupplier.class);

	private IProcessService processService = IProcessService.grab();
	private JobGroup statisticsJobGroup = new JobGroup("Computing statistics jobs", 1, 0);

	/** Kind of DTM managed */
	protected final ContentType dtmType;

	/**
	 * Constructor
	 */
	protected AbstractDtmInfoSupplier(String extension, String description, ContentType contentType) {
		super(extension, description, contentType);
		dtmType = contentType;
	}

	/** @return true when file's version is supported */
	protected abstract boolean isSupported(NCFile ncfile) throws NCException;

	/** Grab attributes from file and set values on DtmInfo */
	protected abstract void fillAttributes(NCFile ncfile, DtmInfo result);

	/** @return the name of the layer corresponding to the name of the raster */
	protected abstract Optional<String> getLayerName(String rasterName);

	/** @return the name of the raster corresponding to the name of the layer */
	protected abstract Optional<String> getRasterName(String layerName);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RasterInfo> supplyRasterInfos(String filePath) throws GIOException {
		List<RasterInfo> result = null;
		Optional<DtmInfo> dtmInfo = getFileInfo(filePath);
		if (dtmInfo.isPresent()) {
			result = dtmInfo.get().getRasterInfos();
		}
		return result != null ? result : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RasterInfo> supplyRasterInfos(IFileInfo fileInfo) {
		return fileInfo instanceof DtmInfo dtmInfo ? dtmInfo.getRasterInfos() : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCdis(String filePath) {
		List<String> result = null;
		if (hasRightExtension(filePath)) {
			try {
				DtmInfo dtmInfo = loadInfo(filePath);
				if (dtmInfo != null) {
					result = dtmInfo.getCdis();
				}
			} catch (GIOException e) {
				// Not a DTM file
			}
		}
		return result != null ? result : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCdis(IFileInfo fileInfo) {
		return getCdis(fileInfo.getPath());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<String> getCdi(IFileInfo fileInfo, GeoPoint position) {
		Optional<List<Property<?>>> propOption = getProperties(fileInfo, position);
		if (propOption.isPresent()) {
			List<Property<?>> properties = propOption.get();
			return properties.stream()//
					.map(p -> p.findByKey(RasterInfo.RASTER_CDI_INDEX))//
					.filter(Optional::isPresent).map(Optional::get) //
					.map(Property::getValueAsString)//
					.findFirst();
		}
		return Optional.empty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getLayers(String filePath) {
		List<String> result = Collections.emptyList();
		if (hasRightExtension(filePath)) {
			try {
				DtmInfo dtmInfo = loadInfo(filePath);
				if (dtmInfo != null) {
					result = dtmInfo.getLayers();
				}
			} catch (GIOException e) {
				// Not a DTM file
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Projection> getProjection(IFileInfo fileInfo) {
		if (fileInfo instanceof DtmInfo dtmInfo) {
			return Optional.ofNullable(dtmInfo.getProjection());
		}
		return Optional.empty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<SpatialResolution> getSpatialResolution(IFileInfo fileInfo) {
		if (fileInfo instanceof DtmInfo dtmInfo) {
			return Optional.ofNullable(dtmInfo.getSpatialResolution());
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<GeoBox> getGeoBox(IFileInfo fileInfo) {
		if (fileInfo instanceof DtmInfo dtmInfo) {
			return Optional.ofNullable(dtmInfo.getGeoBox());
		}
		return Optional.empty();
	}

	/**
	 * @return the DtmInfo of the specified resource. null if filePath is not a dtm
	 * @throws GIOException read failed, error occured
	 */
	public synchronized DtmInfo loadInfo(String filePath) throws GIOException {
		final DtmInfo result = new DtmInfo(filePath, dtmType, this::loadRasterInfoFully);
		List<String> allLayerNames = List.of();
		try (NCFile ncfile = NCFile.open(filePath, Mode.readonly)) {
			if (!isSupported(ncfile)) {
				return null;
			}
			fillAttributes(ncfile, result);
			addLayers(result, ncfile);
			addCdis(result, ncfile);

			// loading other layers
			allLayerNames = result.getLayers();
		} catch (Exception e) {
			logger.debug(e.getMessage());
			return null;
		}
		allLayerNames.forEach(layerName -> addRasterInfo(layerName, result));
		return result;
	}

	/**
	 * Load projection, SpatialReference and BoundingBox
	 */
	protected void loadSpatialnfo(Dataset dataset, DtmInfo dtmInfo) {

		// Spatial resolution
		double[] resolution = GdalUtils.computeSpatialResolution(dataset);
		dtmInfo.setSpatialResolution(new SpatialResolution(resolution[0], resolution[1]));

		// BoundingBox
		SpatialReference srs = GdalUtils.getSpatialReference(dataset);
		double[] boundingBox = GdalUtils.getBoundingBox(dataset);
		GdalOsrUtils.tranformBoundingBoxToWGS84(srs, boundingBox);
		dtmInfo.setGeoBox(new GeoBox(boundingBox));
		dtmInfo.setProjection(Projection.createFromProj4String(srs.ExportToProj4()));
		dtmInfo.setGeoTransform(dataset.GetGeoTransform());
		srs.delete();
	}

	protected void addRasterInfo(String layerName, DtmInfo dtmInfo) {
		getRasterName(layerName).ifPresent(rasterName -> {
			String subset = computeSubsetName(dtmInfo.getPath(), layerName);
			try (var gdalDataset = GdalDataset.open(subset)) {
				Dataset dataset = gdalDataset.orElse(null);
				if (dataset != null) {
					logger.info("Adding raster {}", layerName);
					if (RasterInfo.RASTER_ELEVATION.equalsIgnoreCase(rasterName)) {
						loadSpatialnfo(dataset, dtmInfo);
					}
					RasterInfo resultingRasterInfo = newRasterInfo(rasterName, layerName, dataset, dtmInfo, subset);
					dtmInfo.add(resultingRasterInfo);
				} else
					logger.info("No raster {} in {}", layerName, dtmInfo.getPath());
			}
		});
	}

	/**
	 * If the RasterInfo is not yet fully loaded, open the file with Gdal and computes the statistics of the raster
	 */
	private void loadRasterInfoFully(RasterInfo rasterInfo) {
		if (rasterInfo.isFullyLoaded() || rasterInfo.getContentType() == ContentType.DTM_NETCDF_3)
			return;

		GProcess statisticsJob = processService.createProcess(
				"Computing statistics for " + rasterInfo.getGdalLoadingKey(), IProcessFunction.with(() -> {
					//operation realized by a previous job
					if (rasterInfo.isFullyLoaded())
							return;
					logger.debug("Computing statistics for rasterInfo.getGdalLoadingKey()");
					try (var gdalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey())) {
						gdalDataset.computeStatistics(RasterInfo.HISTOGRAM_SIZE).ifPresent(statistics -> {
							rasterInfo.setMinMaxValues(new DoubleRange(statistics.min(), statistics.max()));
							rasterInfo.setMean(statistics.mean());
							rasterInfo.setStdev(statistics.stddev());
							rasterInfo.setHistogram(statistics.histogram());
						});
						rasterInfo.setFullyLoaded(true);
					}
				}));

		statisticsJob.setUser(false);
		statisticsJob.setJobGroup(statisticsJobGroup);
		statisticsJob.schedule(10L);
	}

	/**
	 * Load CDIs
	 */
	protected abstract void addCdis(DtmInfo dtmInfo, NCFile ncfile) throws NCException;

	/**
	 * Load layers names
	 */
	protected void addLayers(DtmInfo dtmInfo, NCFile ncfile) throws NCException {
		for (String expectedLayer : getAllRasterNames()) {
			var optLayer = getLayerName(expectedLayer);
			if (optLayer.isPresent()) {
				NCVariable oneVariable = ncfile.getVariable(optLayer.get());
				if (oneVariable != null) {
					List<NCDimension> shape = oneVariable.getShape();
					if (shape != null && shape.size() == 2) {
						dtmInfo.getLayers().add(optLayer.get());
					}
				}
			}
		}
	}

	/**
	 * Create an instance of RasterInfo
	 * 
	 * @param statistics Statistics computed by gdal of the raster
	 */
	protected RasterInfo newRasterInfo(String dataType, String dtmLayer, Dataset dataset, DtmInfo dtmInfo,
			String subset) {
		RasterInfo result = new RasterInfo(this, dtmInfo.getPath());
		result.setDataType(dataType);
		result.setContentType(dtmInfo.getContentType());
		result.setGdalLoadingKey(subset);
		result.setGeoBox(dtmInfo.getGeoBox());
		result.setProjection(dtmInfo.getProjection());
		result.setXSize(dataset.getRasterXSize());
		result.setYSize(dataset.getRasterYSize());
		result.setContentType(dtmType);
		String unit = dataset.GetMetadataItem(dtmLayer + "#units");
		result.setUnit(unit != null ? unit : "");
		return result;
	}

	/** Compute virtual filename on a specific layer */
	public String computeSubsetName(String filePath, String layerName) {
		return "netcdf:\"" + filePath + "\":" + layerName;
	}

	@Override
	public DtmInfo makeFileInfo(String filePath) {
		DtmInfo result = null;
		try {
			result = loadInfo(filePath);
		} catch (GIOException e) {
			// Not a DTM file
		}
		return result;
	}

	/**
	 * Returns this layer associative table formed by key / values to the specified position.
	 * 
	 * @return Optional.empty() by default : no information at this position for this layer
	 */
	@Override
	public Optional<List<Property<?>>> getProperties(IFileInfo fileInfo, GeoPoint position) {
		List<Property<?>> result = new LinkedList<>();

		if (fileInfo instanceof DtmInfo dtmInfo) {
			// Reading the CDI index in the file
			try (var ncfile = NetcdfFile.open(dtmInfo.getPath(), Mode.readonly)) {
				// Projects the position if necessary
				double[] xyz = new double[] { position.getLong(), position.getLat(), 0d };
				dtmInfo.getProjection().projectQuietly(xyz);

				// Transformation matrix to get raster grid coordinates from geographic coordinates
				double[] transform = gdal.InvGeoTransform(dtmInfo.getGeoTransform());
				// Computes the grid coordinates (lineIndex and columnIndex) from the position
				GdalUtils.transform(xyz, transform);
				long columnIndex = (long) Math.floor(xyz[0]);
				long lineIndex = (long) Math.floor(xyz[1]);

				for (String rasterName : getAllRasterNames()) {
					Optional<String> layerName = getLayerName(rasterName);
					if (layerName.isEmpty())
						continue;
					// Make the property for the layer
					Optional<Property<?>> property = buildProperty(dtmInfo, ncfile, columnIndex, lineIndex, rasterName,
							layerName.get());
					property.ifPresent(result::add);
				}

			} catch (NCException e) {
				logger.debug("Netcdf error while reading file {} : {}", dtmInfo.getPath(), e.getMessage());
			}
		}
		return result.isEmpty() ? Optional.empty() : Optional.of(result);
	}

	/** Read a number to the specified layer/position and create a Property with it */
	private Optional<Property<?>> buildProperty(DtmInfo dtmInfo, NetcdfFile ncfile, long columnIndex, long lineIndex,
			String rasterName, String layerName) throws NCException {
		OptionalDouble value = readNumberInFile(ncfile, layerName, lineIndex, columnIndex);
		if (value.isEmpty())
			return Optional.empty();

		Property<?> result = switch (rasterName) {
		// CDI
		case RasterInfo.RASTER_CDI_INDEX -> {
			if (!dtmInfo.getCdis().isEmpty()) {
				int cdiIndex = (int) value.getAsDouble();
				if (cdiIndex >= 0 && cdiIndex < dtmInfo.getCdis().size())
					yield Property.build(rasterName, dtmInfo.getCdis().get(cdiIndex));
			}
			yield null;
		}
		// Integer
		case RasterInfo.RASTER_VALUE_COUNT, RasterInfo.RASTER_FILTERED_COUNT -> Property.build(rasterName,
				(int) value.getAsDouble());
		// Boolean
		case RasterInfo.RASTER_INTERPOLATION_FLAG -> Property.build(rasterName, value.getAsDouble() != 0d);
		// Float
		default -> Property.build(rasterName, value.getAsDouble());
		};

		return Optional.ofNullable(result);
	}

	/** Read a any number in the specified variable (convert it to a double regardless of the variable type) */
	private OptionalDouble readNumberInFile(NetcdfFile ncfile, String varName, long lineIndex, long columnIndex)
			throws NCException {
		NetcdfVariable variable = ncfile.getVariable(varName);
		if (variable != null) {
			// Must reverse lineIndex : in Netcdf, origin is lower/left
			long[] dimensions = variable.getDimensions();
			lineIndex = dimensions[0] - lineIndex - 1l;
			if (columnIndex >= 0 && columnIndex < dimensions[1] && lineIndex >= 0 && lineIndex < dimensions[0]) {
				float[] values = variable.get_float(new long[] { lineIndex, columnIndex }, new long[] { 1l, 1l });
				if (values != null && values.length > 0)
					return OptionalDouble.of(values[0]);
			}
		}
		return OptionalDouble.empty();
	}

	/**
	 * @return the list of all {@link RasterInfo} constants
	 */
	private List<String> getAllRasterNames() {
		return List.of(RasterInfo.RASTER_ELEVATION, RasterInfo.RASTER_BACKSCATTER, RasterInfo.RASTER_ELEVATION_MIN,
				RasterInfo.RASTER_ELEVATION_MAX, RasterInfo.RASTER_ELEVATION_SMOOTHED, RasterInfo.RASTER_VALUE_COUNT,
				RasterInfo.RASTER_FILTERED_COUNT, RasterInfo.RASTER_STDEV, RasterInfo.RASTER_CDI_INDEX,
				RasterInfo.RASTER_INTERPOLATION_FLAG, RasterInfo.RASTER_MIN_ACROSS_DISTANCE,
				RasterInfo.RASTER_MAX_ACROSS_DISTANCE, RasterInfo.RASTER_MAX_ACROSS_DISTANCE,
				RasterInfo.RASTER_MAX_ACROSS_ANGLE);
	}

}