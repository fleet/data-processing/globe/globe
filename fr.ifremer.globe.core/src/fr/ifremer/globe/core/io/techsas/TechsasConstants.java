package fr.ifremer.globe.core.io.techsas;

import java.util.List;

import fr.ifremer.globe.core.runtime.datacontainer.DataKind;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;

/**
 * Globe 3D netCDF constants
 */
public class TechsasConstants {

	private TechsasConstants() {
		// Constants class
	}

	/** NetCDF attribute names **/
	public static final String TITLE_ATTRIBUT = "title";
	public static final String TITLE_TECHSAS = "Technical and Scientific sensors Acquisition System";
	public static final String TITLE_TECHSAS_ABBR = "TECHSAS";
	public static final String SOURCE_TYPE = "frame_sourcetype";
	public static final String SOURCE_TYPE_GRAVI = "gravi";
	public static final String SOURCE_TYPE_MAG = "mag";
	public static final String SOURCE_TYPE_DEPTH = "depth";

	/** Only one layer group **/
	public static final String GROUP = "/";

	/** Variable names **/
	public static final String TIME_VAR = "time";
	public static final String HEADING_VAR = "heading";
	public static final String SPEED_VAR = "speed";
	public static final String SPEED_VAR_2 = "gndspeed";

	// depth files (.depth)
	public static final String DEPTH_VAR = "snd";

	// gravity files (.gravi/.gravi.nc)
	public static final String GRAVITY_VAR = "gravity";
	public static final String FREE_AIR_ANOMALY = "freeair";
	public static final String COMPUTED_CORRECTED_GRAVITY_VAR = "computed_corrected_gravity";
	public static final String COMPUTED_FREE_AIR_ANOMALY_VAR = "computed_free_air_anomaly";

	// magnetism files (.mag/.mag.nc)
	public static final String MAG_VAR = "mag";
	public static final String COMPUTED_THEORETIC_MAGNETISM_VAR = "computed_theoretic_magnetism";
	public static final String COMPUTED_MAGNETIC_ANOMALY = "computed_magnetic_anomaly";

	// temperatue files (.ths)
	public static final String TEMPERATURE_VAR = "intaketemp";

	/** Data container layers **/

	// time
	public static final PredefinedLayers<DoubleLoadableLayer1D> TIME = new PredefinedLayers<>(GROUP, TIME_VAR, "Time",
			"ms", DataKind.continuous, DoubleLoadableLayer1D::new);

	// latitude
	public static final PredefinedLayers<DoubleLoadableLayer1D> LATITUDE = new PredefinedLayers<>(GROUP, "lat",
			"Latitude", "degree", DataKind.continuous, DoubleLoadableLayer1D::new);

	// 2 types of longitude : with name "lon" or "long"
	public static final PredefinedLayers<DoubleLoadableLayer1D> LONGITUDE_1 = new PredefinedLayers<>(GROUP, "lon",
			"Longitude", "degree", DataKind.continuous, DoubleLoadableLayer1D::new);

	// longitude for .nav
	public static final PredefinedLayers<DoubleLoadableLayer1D> LONGITUDE_2 = new PredefinedLayers<>(GROUP, "long",
			"Longitude", "degree", DataKind.continuous, DoubleLoadableLayer1D::new);

	// heading
	public static final PredefinedLayers<FloatLoadableLayer1D> HEADING = new PredefinedLayers<>(GROUP, HEADING_VAR,
			"Heading", "degree", DataKind.continuous, FloatLoadableLayer1D::new);

	// speed
	public static final PredefinedLayers<FloatLoadableLayer1D> SPEED = new PredefinedLayers<>(GROUP, SPEED_VAR, "Speed",
			"knot", DataKind.continuous, FloatLoadableLayer1D::new);

	// speed for .nav
	public static final PredefinedLayers<FloatLoadableLayer1D> SPEED_2 = new PredefinedLayers<>(GROUP, SPEED_VAR_2,
			"Speed", "knot", DataKind.continuous, FloatLoadableLayer1D::new);

	// status
	public static final PredefinedLayers<ByteLoadableLayer1D> STATUS = new PredefinedLayers<>(GROUP, "status",
			"invalidity flag :if one of the byte field is set (value != 0) the data are considered as invalid", "",
			DataKind.continuous, ByteLoadableLayer1D::new);

	// TODO : specify attributes of the status netcdf variable
	// status.addAttribute("flag_meanings", "rejected invalid_acquis invalid_conversion invalid_swath
	// invalid_sounding_row ");
	// status.addAttribute("flag_mask", "1b,2b,4b,8b,16b,32b,64b");
	// status.addAttribute("comment", "invalidity flag for soundings data, if one of the byte field is set (value != 0)
	// the data are considered as invalid for the given reasons : rejected (rejected by user operation, detailed reason
	// is given by the detailed status field), invalid_acquisition (marked invalid by sounder), invalid_conversion
	// (tagged as invalid during file creation, typically missing datagram), invalid_swath (swath is invalidated
	// typically by the user), invalid_sounding_row (range of sounding invalidated along sounding_row dimension)");
	//

	/** Data container layers **/
	public static final List<PredefinedLayers<?>> LAYERS = List.of(TIME, LATITUDE, LONGITUDE_1, LONGITUDE_2, HEADING,
			SPEED);

}
