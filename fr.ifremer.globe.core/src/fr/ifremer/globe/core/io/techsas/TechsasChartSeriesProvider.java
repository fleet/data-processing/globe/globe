package fr.ifremer.globe.core.io.techsas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesStatusHandler;
import fr.ifremer.globe.core.model.chart.datacontainer.DataContainerSeriesBuilder;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerLoader;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.FunctionWithException;

/**
 * This class provides method to get {@link IChartSeries} from {@link TechsasInfo}.
 */
@Component(name = "globe_drivers_techsas_chart_series_supplier", service = { IChartSeriesSupplier.class })
public class TechsasChartSeriesProvider implements IChartSeriesSupplier, IDataContainerOwner {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(TechsasChartSeriesProvider.class);

	/** Default series to display **/
	private static final List<String> DEFAULT_SERIES_TO_DISPLAY = List.of(
			TechsasConstants.COMPUTED_FREE_AIR_ANOMALY_VAR, TechsasConstants.COMPUTED_MAGNETIC_ANOMALY,
			TechsasConstants.DEPTH_VAR, TechsasConstants.GRAVITY_VAR, TechsasConstants.MAG_VAR);

	@Override
	public Set<ContentType> getContentTypes() {
		return ContentType.getTechsasContentTypes();
	}

	@Override
	public List<? extends IChartSeries<? extends IChartData>> getChartSeries(IFileInfo fileInfo) throws GIOException {
		if (fileInfo instanceof TechsasInfo techsasInfo) {
			var dataContainerToken = IDataContainerFactory.grab().book(techsasInfo, this);
			var dataContainer = dataContainerToken.getDataContainer();
			var timeLayer = dataContainerToken.getDataContainer().getLayer(TechsasConstants.TIME);

			// filter to get only layer with time dimension
			Predicate<IBaseLayer> layerFilter = layer -> !layer.getName().contains(TechsasConstants.STATUS.getName())
					&& layer.getDimensions().length > 0 && layer.getDimensions()[0] == timeLayer.getDimensions()[0];

			// time supplier
			FunctionWithException<Integer, Long, GIOException> indexToTime = index -> DateUtils
					.daysFrom1899_12_30ToInstant(timeLayer.get(index)).toEpochMilli();

			// keep only valid times
			var validIndexes = new ArrayList<Integer>();
			var timeLayerLength = timeLayer.getDimensions()[0];
			for (int i = 0; i < timeLayerLength; i++) {
				if (timeLayer.get(i) > 0)
					validIndexes.add(i);
			}
			Optional<List<Integer>> optValidIndexes = validIndexes.size() == timeLayerLength ? Optional.empty()
					: Optional.of(validIndexes);

			// build series
			var seriesList = new DataContainerSeriesBuilder(dataContainerToken, indexToTime, layerFilter,
					optValidIndexes).getSeries();

			// select the series to display by default
			for (var defaultSeries : DEFAULT_SERIES_TO_DISPLAY) {
				var seriesToSelect = seriesList.stream().filter(s -> s.getTitle().contains(defaultSeries)).findFirst();
				if (seriesToSelect.isPresent()) {
					seriesToSelect.get().setEnabled(true);
					break;
				}
			}

			// load validity flag and define "save" method
			var statusHandler = new StatusHandler(dataContainer, validIndexes);
			for (var series : seriesList) {
				// checkMinMax(dataContainer, series); // disabled to avoid the loading of each series
				series.setStatusHandler(statusHandler);
			}
			return seriesList;
		}

		return Collections.emptyList();
	}

	/**
	 * Data status handler
	 */
	private class StatusHandler implements IChartSeriesStatusHandler<IChartData> {

		private final TechsasInfo techsasFileInfo;

		private final List<Integer> validIndexes;

		private int[] statusArray;

		private boolean isDirty = false;

		public StatusHandler(DataContainer<TechsasInfo> dataContainer, List<Integer> validIndexes) {
			statusArray = new int[validIndexes.size()];
			techsasFileInfo = dataContainer.getInfo();
			this.validIndexes = validIndexes;
			load(dataContainer);
		}

		@Override
		public int getStatus(IChartData data) {
			return statusArray[data.getIndex()];
		}

		@Override
		public void setStatus(IChartData data, int status) {
			if (statusArray[data.getIndex()] != status) {
				statusArray[data.getIndex()] = status;
				isDirty = true;
			}
		}

		/**
		 * Loads status from NetCDF variable.
		 */
		private void load(DataContainer<TechsasInfo> dataContainer) {
			try {
				if (dataContainer.hasLayer(TechsasConstants.STATUS)) {
					var flagLayer = dataContainer.getLayer(TechsasConstants.STATUS);
					for (int i = 0; i < validIndexes.size(); i++)
						statusArray[i] = flagLayer.get(validIndexes.get(i));
				}
			} catch (GIOException e) {
				LOGGER.error("Error while loading validity flags : " + e.getMessage(), e);
			}
		}

		@Override
		public boolean isDirty() {
			return isDirty;
		}

		/**
		 * Flushes status array to NetCDF file.
		 */
		@Override
		public void flush() {
			boolean isAlreayOpened = IDataContainerFactory.grab().isBooked(techsasFileInfo);

			IDataContainerToken<TechsasInfo> dataContainerToken = null;
			try {
				dataContainerToken = IDataContainerFactory.grab().book(techsasFileInfo,
						TechsasChartSeriesProvider.this);
				var dataContainer = dataContainerToken.getDataContainer();
				var ncFile = techsasFileInfo.getNcFile();

				// create status layer if doesn't exist
				if (!dataContainer.hasLayer(TechsasConstants.STATUS)) {
					var dims = ncFile.getVariable(TechsasConstants.TIME_VAR).getShape();
					ncFile.redef();
					var ncVariable = ncFile.addVariable(TechsasConstants.STATUS.getName(), DataType.BYTE, dims);
					dataContainer.declareLayer(TechsasConstants.STATUS,
							new NetcdfLayerLoader<>(ncFile, ncVariable.getPath()));
					ncFile.enddef();
				}

				// edit layer
				var flagLayer = dataContainer.getLayer(TechsasConstants.STATUS);
				for (int i = 0; i < statusArray.length; i++)
					flagLayer.set(validIndexes.get(i), (byte) statusArray[i]);
				flagLayer.flush();
				isDirty = false;
			} catch (NCException | GIOException e) {
				LOGGER.error("Error while saving validity flags : " + e.getMessage(), e);
			} finally {
				// close file only if it was not previously opened
				if (!isAlreayOpened)
					Optional.ofNullable(dataContainerToken).ifPresent(IDataContainerToken::close);
			}

		}

	}

	/**
	 * Loads validity flags.
	 */
	private void checkMinMax(DataContainer<TechsasInfo> dataContainer, IChartSeries<? extends IChartData> series) {
		var baseLayerName = series.getTitle().split(" ")[0];
		try {
			NetcdfVariable ncVar = ((NetcdfLayerLoader<?>) dataContainer.getLayer("/", baseLayerName).getLoader())
					.getNetcdfVar();
			Optional<Double> minValue = ncVar.getValidMin();
			Optional<Double> maxValue = ncVar.getValidMax();
			if (minValue.isPresent() && maxValue.isPresent()) {
				for (var chartData : series) {
					if (chartData.getY() < minValue.get() || chartData.getY() > maxValue.get()) {
						// set value to NaN to be not displayed
						// chartData.setY(Double.NaN);
						// set flag to false
						chartData.setEnabled(false);
					}
				}
			}
		} catch (NCException | GIOException e) {
			LOGGER.error("Error while loading validity flags : " + e.getMessage(), e);
		}
	}

}
