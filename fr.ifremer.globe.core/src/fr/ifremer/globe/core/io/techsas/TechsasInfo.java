package fr.ifremer.globe.core.io.techsas;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Info about TECHSAS NetCDF files.
 */
public class TechsasInfo implements IDataContainerInfo {

	/** Path & file **/
	private final String filePath;
	private NetcdfFile ncFile;
	private final AtomicBoolean isOpen = new AtomicBoolean();
	private final ContentType contentType;

	/** NetCDF file global attributes **/
	private int timeDimension;
	private TreeMap<String, String> globalAttributes;

	/** True if the TECHSAS file contains position variables (lat,lon...) **/
	private boolean isGeolocated;
	/** Start / end dates */
	private Instant startDate = null;
	private Instant endDate = null;

	/** Geobox of the file. May be null */
	private GeoBox geoBox = null;

	/**
	 * Constructor
	 *
	 * @throws GIOException
	 */
	public TechsasInfo(String filePath, ContentType contentType) throws GIOException {
		this.filePath = filePath;
		this.contentType = contentType;
		load();
	}

	/**
	 * Open the file
	 */
	@Override
	public synchronized void open(boolean readonly) throws GIOException {
		try {
			if (isOpen.compareAndSet(false, true)) {
				ncFile = NetcdfFile.open(filePath, readonly ? Mode.readonly : Mode.readwrite);
			}
		} catch (NCException e) {
			isOpen.set(false);
			throw new GIOException("Error while opening NVI file : " + e.getMessage(), e);
		}
	}

	/**
	 * Close the file
	 */
	@Override
	public synchronized void close() {
		if (!IDataContainerFactory.grab().isBooked(this) && isOpen.compareAndSet(true, false))
			ncFile.close();
	}

	/**
	 * Opens and reads the file to retrieve needed information.
	 *
	 * @throws GIOException
	 */
	private void load() throws GIOException {
		try {
			open(true);
			globalAttributes = ncFile.getGlobalAttributs();
			NCDimension ncTimeDimension = ncFile.getDimension("time");
			if (ncTimeDimension != null) {
				timeDimension = (int) ncTimeDimension.getLength();
			}
			if (timeDimension == 0) {
				throw new GIOException("Invalid time dimension");
			}

			// Loading start/end dates
			NetcdfVariable timeVar = ncFile.getVariable("time");
			if (timeVar != null) {
				startDate = DateUtils
						.daysFrom1899_12_30ToInstant(timeVar.get_double(new long[] { 0l }, new long[] { 1l })[0]);
				endDate = DateUtils.daysFrom1899_12_30ToInstant(
						timeVar.get_double(new long[] { timeDimension - 1 }, new long[] { 1l })[0]);
			}

			// Load position data.
			isGeolocated = TechsasInfoSupplier.isGeolocatedFile(ncFile);
			if (isGeolocated) {
				try (TechsasNavigationData navData = new TechsasNavigationData(this)) {
					geoBox = navData.computeGeoBox();
				}
			}
		} catch (NCException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	@Override
	public void declareLayers(DataContainer<?> container) throws GException {
		Map<String, PredefinedLayers<?>> map = TechsasConstants.LAYERS.stream()
				.collect(Collectors.toMap(PredefinedLayers::getPath, Function.identity()));
		new NetcdfLayerDeclarer().declareLayers(ncFile, container, group -> TechsasConstants.GROUP,
				variable -> Optional.ofNullable(map.get(variable.getPath())));
	}

	/**
	 * Provides properties displayed in "Properties" view.
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new ArrayList<>();
		result.add(Property.build("Time dimension", timeDimension));
		result.add(Property.build("Geolocated", isGeolocated));
		if (startDate != null)
			result.add(Property.build("Start date", startDate.toString()));
		if (endDate != null)
			result.add(Property.build("End date", endDate.toString()));
		if (globalAttributes != null)
			globalAttributes.forEach((key, value) -> result.add(Property.build(key, value)));
		return result;
	}

	//// GETTERS & SETTERS

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public ContentType getContentType() {
		return contentType;
	}

	public NetcdfFile getNcFile() {
		return ncFile;
	}

	public boolean isGeolocated() {
		return isGeolocated;
	}

	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		return startDate != null && endDate != null ? Optional.of(Pair.of(startDate, endDate)) : Optional.empty();
	}

}
