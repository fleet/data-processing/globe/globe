/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.techsas;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Service providing informations on TECHSAS NetCDF files.
 */
@Component(name = "globe_drivers_techsas_file_info_supplier", //
		service = { IFileInfoSupplier.class, TechsasInfoSupplier.class })
public class TechsasInfoSupplier implements IFileInfoSupplier<TechsasInfo> {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(TechsasInfoSupplier.class);

	/** Extensions **/
	public static final String EXTENSION_NC = "nc";
	public static final String EXTENSION_ADCP = "adcp";
	public static final String EXTENSION_ATT = "att";
	public static final String EXTENSION_BATSND = "batsnd";
	public static final String EXTENSION_DEPTH = "depth";
	public static final String EXTENSION_ENG = "eng";
	public static final String EXTENSION_FBOX = "fbox";
	public static final String EXTENSION_FLOW = "flow";
	public static final String EXTENSION_FLUO = "fluo";
	public static final String EXTENSION_GPS = "gps";
	public static final String EXTENSION_GRAVI = "gravi";
	public static final String EXTENSION_GYR = "gyr";
	public static final String EXTENSION_HSS = "hss";
	public static final String EXTENSION_INFO = "info";
	public static final String EXTENSION_KHXDR = "khxdr";
	public static final String EXTENSION_LOCH = "loch";
	public static final String EXTENSION_MAG = "mag";
	public static final String EXTENSION_MBS = "mbs";
	public static final String EXTENSION_MET = "met";
	public static final String EXTENSION_NAV = "nav";
	public static final String EXTENSION_QAL = "qal";
	public static final String EXTENSION_QCF = "qcf";
	public static final String EXTENSION_SUBNAV = "subnav";
	public static final String EXTENSION_THS = "ths";
	public static final String EXTENSION_WINCH = "winch";
	protected static final List<String> EXTENSIONS = List.of(EXTENSION_NC, EXTENSION_ADCP, EXTENSION_ATT,
			EXTENSION_BATSND, EXTENSION_DEPTH, EXTENSION_ENG, EXTENSION_FBOX, EXTENSION_FLOW, EXTENSION_FLUO,
			EXTENSION_GPS, EXTENSION_GRAVI, EXTENSION_INFO, EXTENSION_KHXDR, EXTENSION_GYR, EXTENSION_HSS,
			EXTENSION_LOCH, EXTENSION_MAG, EXTENSION_MBS, EXTENSION_MET, EXTENSION_NAV, EXTENSION_QAL, EXTENSION_QCF,
			EXTENSION_SUBNAV, EXTENSION_THS, EXTENSION_WINCH);

	@Override
	public Optional<TechsasInfo> getFileInfo(String filePath) {
		return getTechsasContentType(filePath).map(contentType -> {
			try {
				return new TechsasInfo(filePath, contentType);
			} catch (GIOException e) {
				LOGGER.error("Error while opening TECHSAS file {} : {}", filePath, e.getMessage(), e);
			}
			return null;
		});
	}

	/**
	 * @return TECHSAS content type or empty if it is not recognized as a TECHSAS file.
	 */
	private Optional<ContentType> getTechsasContentType(String filePath) {
		if (EXTENSIONS.stream().anyMatch(filePath::endsWith)) {
			try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readonly)) {
				if (file.hasAttribute(TechsasConstants.TITLE_ATTRIBUT)) {
					// Check if it is a TECHSAS file.
					var title = file.getAttributeAsString(TechsasConstants.TITLE_ATTRIBUT);
					var isTechsasFile = TechsasConstants.TITLE_TECHSAS.equalsIgnoreCase(title)
							|| title.contains(TechsasConstants.TITLE_TECHSAS_ABBR);
					var isGeolocated = isGeolocatedFile(file);
					if (!isTechsasFile)
						return Optional.empty();

					// Define specific content type from file's attributes (only if the file contains position data...).
					if (file.hasAttribute(TechsasConstants.SOURCE_TYPE) && isGeolocated) {
						var sourceType = file.getAttributeAsString(TechsasConstants.SOURCE_TYPE);
						switch (sourceType) {
						case TechsasConstants.SOURCE_TYPE_DEPTH:
							return Optional.of(ContentType.TECHSAS_NETCDF_DEPTH);
						case TechsasConstants.SOURCE_TYPE_GRAVI:
							return Optional.of(ContentType.TECHSAS_NETCDF_GRAVI);
						case TechsasConstants.SOURCE_TYPE_MAG:
							return Optional.of(ContentType.TECHSAS_NETCDF_MAG);
						}
					}

					return isGeolocated ? Optional.of(ContentType.TECHSAS_NETCDF_WITH_NAV) : Optional.of(ContentType.TECHSAS_NETCDF);
				}
			} catch (NCException e) {
				LOGGER.error("Error while opening file {} : {}", filePath, e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	public static boolean isGeolocatedFile(NetcdfFile ncFile) throws NCException {
		return ncFile.getVariables().stream().anyMatch(variable -> variable.getName().equals("lat"));
	}

	@Override
	public Set<ContentType> getContentTypes() {
		return ContentType.getTechsasContentTypes();
	}

	@Override
	public List<String> getExtensions() {
		return EXTENSIONS;
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		var extensions = String.join(";", EXTENSIONS.stream().map(ext -> "*." + ext).toList());
		return Arrays.asList(new Pair<>("TECHSAS (*." + extensions + ")", extensions));
	}

}