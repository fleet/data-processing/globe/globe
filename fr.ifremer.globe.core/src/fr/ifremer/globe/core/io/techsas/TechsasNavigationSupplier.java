package fr.ifremer.globe.core.io.techsas;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;

/**
 * Implementation of {@link INavigationSupplier} for {@link TechsasInfo}.
 */
@Component(name = "globe_drivers_techsas_navigation_supplier", service = { INavigationSupplier.class })
public class TechsasNavigationSupplier implements INavigationSupplier {

	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		if (fileInfo instanceof TechsasInfo && ((TechsasInfo) fileInfo).isGeolocated())
			return Optional.of(accessMode -> new TechsasNavigationData((TechsasInfo) fileInfo));
		return Optional.empty();
	}

}
