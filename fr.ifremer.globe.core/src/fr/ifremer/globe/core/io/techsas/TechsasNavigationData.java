package fr.ifremer.globe.core.io.techsas;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link INavigationData} for extended TECHSAS files
 */
public class TechsasNavigationData implements INavigationData {

	/** TECHSAS {@link NavigationMetadataType} **/
	public static final NavigationMetadataType<Double> COMPUTED_CORRECTED_GRAVITY = new NavigationMetadataType<>(
			"Computed corrected gravity", "mgal");
	public static final NavigationMetadataType<Double> COMPUTED_FAA = new NavigationMetadataType<>(
			"Computed free air anomaly", "mgal");
	public static final NavigationMetadataType<Float> DEPTH = new NavigationMetadataType<>("Depth", "m");
	public static final NavigationMetadataType<Float> GRAVITY = new NavigationMetadataType<>("Gravity", "mgal");
	public static final NavigationMetadataType<Float> MAGNETISM = new NavigationMetadataType<>("Magnetic field",
			"gamma");
	public static final NavigationMetadataType<Double> MAGNETIC_ANOMALY = new NavigationMetadataType<>(
			"Magnetic anomaly", "gamma");
	public static final NavigationMetadataType<Float> WATER_TEMPERATURE = new NavigationMetadataType<>(
			"Water temperature", "degree celsius");

	/** layers **/
	private final DoubleLoadableLayer1D latitudeLayer;
	private final DoubleLoadableLayer1D longitudeLayer;

	/** Properties **/
	private final IDataContainerToken<TechsasInfo> sounderDataContainerToken;

	/** Metadata **/
	private final List<NavigationMetadata<?>> metadata = new ArrayList<>();

	/** Constructor **/
	public TechsasNavigationData(TechsasInfo info) throws GIOException {
		sounderDataContainerToken = IDataContainerFactory.grab().book(info,
				IDataContainerOwner.generate(TechsasNavigationData.class.getSimpleName(), true));
		var dataContainer = sounderDataContainerToken.getDataContainer();
		latitudeLayer = dataContainer.getLayer(TechsasConstants.LATITUDE);
		longitudeLayer = dataContainer.hasLayer(TechsasConstants.LONGITUDE_1)
				? dataContainer.getLayer(TechsasConstants.LONGITUDE_1)
				: dataContainer.getLayer(TechsasConstants.LONGITUDE_2);

		// get layer with same dimension than latitude to associate its values with the navigation
		dataContainer.process(layer -> layer.getDimensions()[0] == latitudeLayer.getDimensions()[0],
				new IAbstractBaseLayerOperation() {

					@Override
					public void visit(ByteLayer1D layer) throws GIOException {
						metadata.add(getMetadata(layer));
					}

					@Override
					public void visit(FloatLayer1D layer) throws GIOException {
						metadata.add(getMetadata(layer));
					}

					@Override
					public void visit(DoubleLayer1D layer) throws GIOException {
						metadata.add(getMetadata(layer));
					}
				});
	}

	/** Close method **/
	@Override
	public void close() {
		if (sounderDataContainerToken != null)
			sounderDataContainerToken.close();
	}

	/**
	 * @return the {@link NavigationMetadata} associated to the provided layer.
	 */
	private NavigationMetadata<?> getMetadata(ByteLayer1D layer) {
		// try to find known metadata
		var metadataType = new NavigationMetadataType<Integer>(layer.getName());
		return metadataType.withValueSupplier(i -> {
			// loads layers if needed
			if (layer instanceof ILoadableLayer && !((ILoadableLayer<?>) layer).isLoaded())
				((ILoadableLayer<?>) layer).load();
			return (int) layer.get(i);
		});
	}

	/**
	 * @return the {@link NavigationMetadata} associated to the provided layer.
	 */
	private NavigationMetadata<?> getMetadata(FloatLayer1D layer) {
		// try to find known metadata
		NavigationMetadataType<Float> metadataType = switch (layer.getName()) {
		case TechsasConstants.DEPTH_VAR -> DEPTH;
		case TechsasConstants.GRAVITY_VAR -> GRAVITY;
		case TechsasConstants.HEADING_VAR -> NavigationMetadataType.HEADING;
		case TechsasConstants.MAG_VAR -> MAGNETISM;
		case TechsasConstants.SPEED_VAR, TechsasConstants.SPEED_VAR_2 -> NavigationMetadataType.SPEED;
		case TechsasConstants.TEMPERATURE_VAR -> WATER_TEMPERATURE;
		default -> new NavigationMetadataType<Float>(layer.getName());
		};

		return metadataType.withValueSupplier(i -> {
			// loads layers if needed
			if (layer instanceof ILoadableLayer && !((ILoadableLayer<?>) layer).isLoaded())
				((ILoadableLayer<?>) layer).load();
			return layer.get(i);
		});
	}

	/**
	 * @return the {@link NavigationMetadata} associated to the provided layer.
	 */
	private NavigationMetadata<?> getMetadata(DoubleLayer1D layer) {
		// specific case of Time variable
		if (layer.getName().equals(TechsasConstants.TIME_VAR)) {
			return NavigationMetadataType.TIME.withValueSupplier(i -> {
				if (layer instanceof ILoadableLayer && !((ILoadableLayer<?>) layer).isLoaded())
					((ILoadableLayer<?>) layer).load();
				return DateUtils.daysFrom1899_12_30ToInstant(layer.get(i)).toEpochMilli();
			});
		}

		// try to find known metadata
		NavigationMetadataType<Double> metadataType = switch (layer.getName()) {
		case TechsasConstants.COMPUTED_CORRECTED_GRAVITY_VAR -> COMPUTED_CORRECTED_GRAVITY;
		case TechsasConstants.COMPUTED_FREE_AIR_ANOMALY_VAR -> COMPUTED_FAA;
		case TechsasConstants.COMPUTED_MAGNETIC_ANOMALY -> MAGNETIC_ANOMALY;
		default -> new NavigationMetadataType<Double>(layer.getName(), layer.getUnit());
		};

		// else build default Metadata
		return metadataType.withValueSupplier(i -> {
			// loads layers if needed
			if (layer instanceof ILoadableLayer && !((ILoadableLayer<?>) layer).isLoaded())
				((ILoadableLayer<?>) layer).load();
			return layer.get(i);
		});
	}

	//// GETTERS & SETTERS

	@Override
	public String getFileName() {
		return sounderDataContainerToken.getDataContainer().getInfo().getPath();
	}

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return sounderDataContainerToken.getDataContainer().getInfo().getContentType();
	}

	@Override
	public int size() {
		return (int) latitudeLayer.getDimensions()[0];
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return latitudeLayer.get(index);
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return longitudeLayer.get(index);
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

}
