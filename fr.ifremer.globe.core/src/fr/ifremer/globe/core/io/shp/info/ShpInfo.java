package fr.ifremer.globe.core.io.shp.info;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.model.properties.Property;

/**
 * LayerStore dedicated to Shape file.
 *
 */
public class ShpInfo extends BasicFileInfo {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(ShpInfo.class);

	/** Kind of geometry */
	protected String geometryType;
	/** Geo projection */
	protected Projection projection;

	/** Fields and their possible values */
	protected Map<String, Set<String>> fields;

	/**
	 * Constructor
	 */
	public ShpInfo(String sourcefile) {
		super(sourcefile, ContentType.SHAPEFILE);
	}

	/**
	 * @see fr.ifremer.globe.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();

		result.addAll(Property.build(projection));
		result.add(Property.build("Geometry", geometryType));
		result.add(Property.buildDetailed("Fields", fields));

		return result;
	}

	/** Getter of {@link #projection} */
	public Projection getProjection() {
		return projection;
	}

	/** Accept the extent from the shapefile */
	protected void fitExtent(double[] extent) {
		if (extent != null) {
			try {
				double[] en = projection.unproject(extent[1], extent[3]);
				double[] os = projection.unproject(extent[0], extent[2]);
				setGeoBox(new GeoBox(en[1], os[1], en[0], os[0]));
			} catch (ProjectionException e) {
				logger.debug("Error Projection.setProjectedGeoBox : ");
			}
		}
	}

	/** Accept the SpatialReference from the shapefile */
	protected void fitSpatialReference(SpatialReference spatialReference) {
		if (spatialReference != null) {
			projection = Projection.createFromProj4String(spatialReference.ExportToProj4());
		} else {
			projection = new Projection(StandardProjection.LONGLAT);
		}
	}

	/** Accept the SpatialReference from the shapefile */
	protected void fitGeometry(String geometryType) {
		if (geometryType != null) {
			setGeometryType(geometryType);
		}
	}

	/** Accept the SpatialReference from the shapefile */
	protected void fitFieldDefn(Map<String, Set<String>> fields) {
		this.fields = fields;
	}

	/** Getter of {@link #geometryType} */
	public String getGeometryType() {
		return geometryType;
	}

	/** Setter of {@link #geometryType} */
	protected void setGeometryType(String geometryType) {
		this.geometryType = geometryType;
	}

	/** Getter of {@link #fields} */
	public Map<String, Set<String>> getFields() {
		return fields;
	}

}