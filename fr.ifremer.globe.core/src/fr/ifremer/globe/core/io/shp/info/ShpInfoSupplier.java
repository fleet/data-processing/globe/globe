package fr.ifremer.globe.core.io.shp.info;

import java.io.IOException;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.gdal.GdalOgrUtils;

@Component(name = "globe_drivers_shp_file_info_supplier", //
		service = { IFileInfoSupplier.class, ShpInfoSupplier.class }, //
		property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer=" + FileInfoSupplierPriority.SHP_PRIORITY })
public class ShpInfoSupplier extends BasicFileInfoSupplier<ShpInfo> {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(ShpInfoSupplier.class);

	/**
	 * Constructor
	 */
	public ShpInfoSupplier() {
		super("shp", "Shape file", ContentType.SHAPEFILE);
	}

	/** {@inheritDoc} */
	@Override
	public ShpInfo makeFileInfo(String filePath) {
		ShpInfo result = null;
		try {
			result = loadInfo(filePath);
		} catch (IOException e) {
			logger.warn("Error on {} loading : {}", filePath, e.getMessage());
		}
		return result;
	}

	/**
	 * @return the ShpInfo of the specified resource. null if filePath is not a SHP
	 * @throws IOException read failed, error occured
	 */
	protected ShpInfo loadInfo(String resource) throws IOException {
		ShpInfo result = new ShpInfo(resource);
		GdalOgrUtils.readMetadata(result.getPath(), result::fitSpatialReference, result::fitExtent, result::fitGeometry,
				result::fitFieldDefn);
		return result;
	}
}
