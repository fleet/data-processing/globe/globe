package fr.ifremer.globe.core.io.mgd;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * MGD77 file info (MARINE GEOPHYSICAL DATA EXCHANGE FORMAT - "MGD77" (Bathymetry, Magnetics, and Gravity).
 */
public class MgdFileInfo extends BasicFileInfo {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(MgdFileInfo.class);

	/** Tide data */
	private final List<MgdData> data;

	/** Start / end dates */
	private Instant startDate = null;
	private Instant endDate = null;

	/**
	 * Constructor
	 */
	public MgdFileInfo(String filePath, List<MgdData> mgdData) throws GIOException {
		super(filePath, ContentType.MGD77);
		this.data = mgdData;

		// compute statistics
		for (MgdData currentData : data) {
			Instant date = currentData.time;
			if (startDate == null || date.compareTo(startDate) < 0)
				startDate = date;
			if (endDate == null || date.compareTo(endDate) > 0)
				endDate = date;
		}

		geoBox = new MgdNavigation(this).getNavigationData(true).computeGeoBox();
	}

	public List<MgdData> getData() {
		return data;
	}

	public int size() {
		return data.size();
	}

	@Override
	public Optional<Pair<Instant, Instant>> getStartEndDate() {
		return startDate != null && endDate != null ? Optional.of(Pair.of(startDate, endDate)) : Optional.empty();
	}

	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();
		result.add(Property.build("Number of points", data.size()));
		result.add(Property.build("Start date", startDate.toString()));
		result.add(Property.build("End date", endDate.toString()));
		return result;
	}

}