package fr.ifremer.globe.core.io.mgd;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public class MgdData {

	private final static Pattern UNUSED_PATTERN = Pattern.compile("\\+?9*");

	/** Fields (public & finals (wait for java16 records...) **/

	public final int recordType;
	public final String surveyIdentifier;
	public final Instant time;
	public final double latitude;
	public final double longitude;

	// magnetic fields
	public final double mag;
	public final double residualMagnetics;

	// gravity fields
	public final double gravity;
	public final double eotvosCorrection;
	public final double freeAirAnomaly;

	// depth
	public final double depth;

	/**
	 * Constructor : parses one line of MGD77 file.
	 */
	public MgdData(String dataLine) {
		recordType = MgdDataFields.DATA_RECORD_TYPE.extract(dataLine, Integer::parseInt, -1);
		surveyIdentifier = MgdDataFields.SURVEY_IDENTIFIER.extract(dataLine, s -> s, "NaN");

		latitude = MgdDataFields.LATITUDE.extract(dataLine, s -> Double.parseDouble(s) / 100000.0, Double.NaN);
		longitude = MgdDataFields.LONGITUDE.extract(dataLine, s -> Double.parseDouble(s) / 100000.0, Double.NaN);
		time = MgdDataFields.DATE_TIME.extract(dataLine, this::parseDateTime, null);

		gravity = MgdDataFields.OBSERVED_GRAVITY.extract(dataLine, s -> parseDouble(s, 0.1d), Double.NaN);
		eotvosCorrection = MgdDataFields.EOTVOS_CORRECTION.extract(dataLine, s -> parseDouble(s, 0.1d), Double.NaN);
		freeAirAnomaly = MgdDataFields.FREE_AIR_ANOMALY.extract(dataLine, s -> parseDouble(s, 0.1d), Double.NaN);

		depth = MgdDataFields.BATHYMETRY_CORRECTED_DEPTH.extract(dataLine, s -> parseDouble(s, 0.1d), Double.NaN);

		mag = MgdDataFields.MAGNETICS_TOTAL_FIELD_1.extract(dataLine, s -> parseDouble(s, 0.1d), Double.NaN);
		residualMagnetics = MgdDataFields.MAGNETICS_RESIDUAL_FIELD.extract(dataLine, s -> parseDouble(s, 0.1d),
				Double.NaN);
	}

	/**
	 * Parses string as double (check the "unused pattern" of MGD = +99999)
	 */
	private double parseDouble(String string, double scaleFactor) {
		if (UNUSED_PATTERN.matcher(string).matches() && UNUSED_PATTERN.matcher(string).regionEnd() == string.length())
			return Double.NaN;
		return Double.parseDouble(string) * scaleFactor;
	}

	/**
	 * Parses date time from MGD data line.
	 */
	private Instant parseDateTime(String stringValue) {
		var localDate = LocalDate.parse(stringValue.substring(0, 8), DateTimeFormatter.BASIC_ISO_DATE);
		var localTime = LocalTime.of(Integer.parseInt(stringValue.substring(8, 10)),
				Integer.parseInt(stringValue.substring(10, 15)) / 1000);
		return LocalDateTime.of(localDate, localTime).toInstant(ZoneOffset.UTC);
	}

	public boolean isValid() {
		return !Double.isNaN(latitude) && !Double.isNaN(longitude) && time != null;
	}

}
