package fr.ifremer.globe.core.io.mgd;

import java.util.function.Function;

import org.apache.commons.lang.StringUtils;

/**
 * Fields of MARINE GEOPHYSICAL DATA EXCHANGE FORMAT - "MGD77" (Bathymetry, Magnetics, and Gravity).
 */
public enum MgdDataFields {

	DATA_RECORD_TYPE(1, "Data record type : set to '5' for data record."),

	SURVEY_IDENTIFIER(2, 9,
			"Survey identifier supplied by the contributing organization, else given by NGDC in a manner which represents the data."),

	TIME_ZONE_CORRECTION(10, 12,
			"Corrects time (in characters 13-27) to GMT when added: equals zero when time is GMT. Timezone normally falls between -13 and +12 inclusively."),

	DATE_TIME(13, 27, "Date time : YYYYMMDD + HOUR + MINUTES x 1000"),

	LATITUDE(28, 35, "Latitude x 100000; + = North; - = South; Between -9000000 and 9000000"),

	LONGITUDE(36, 44, "Longitude x 100000; + = East; - = West; Between -18000000 and 18000000"),

	POSITION_TYPE_CODE(45, "Position type code : indicates how lat/lon was obtained: "//
			+ "1 = Observed fix; "//
			+ "3 = Interpolated; "//
			+ "9 = Unspecified"),

	BATHYMETRY_2_WAY_TRAVELTIME(46, 51, "Bathymetry 2 way traveltime in ten-thousandths of seconds.	"
			+ "Corrected for transducer depth and	other such corrections, especially in shallow water"),

	BATHYMETRY_CORRECTED_DEPTH(52, 57, "Bathymetry corrected depth in tenths of meters."),

	BATHYMETRIC_CORRECTION_CODE(58, 59,
			"Bathymetric correction code : details the procedure used for determining the sound velocity correction to depth: "//
					+ "01-55 Matthews' Zones with zone; " //
					+ "59 Matthews' Zones, no zone; "//
					+ "60 S. Kuwahara Formula; " //
					+ "61 Wilson Formula; " //
					+ "62 Del Grosso Formula; " //
					+ "63 Carter's Tables; " //
					+ "88 Other (see Add. Doc.); " //
					+ "99 Unspecified"),

	BATHYMETRIC_TYPE_CODE(60, "Bathymetric type code : indicates how the data record's bathymetric value was obtained: " //
			+ "1 = Observed; "//
			+ "3 = Interpolated (Header Seq. 12); "//
			+ "9 = Unspecified"),

	MAGNETICS_TOTAL_FIELD_1(61, 66,
			"Magnetics total field for 1st sensor in tenths of nanoteslas (gammas). For leading sensor. Use this field for single sensor."),

	MAGNETICS_TOTAL_FIELD_2(67, 72,
			"Magnetics total field for 2nd sensor in tenths of nanoteslas (gammas). For trailing sensor."),

	MAGNETICS_RESIDUAL_FIELD(73, 78,
			"Magnetics residual field In tenths of nanoteslas (gammas). The reference field used is in Header Seq. 13."),

	SENSOR_FOR_RESIDUAL_FIELD(79, "Sensor for residual field : "//
			+ "1 = 1st or leading sensor; "//
			+ "2 = 2nd or trailing sensor; "//
			+ "9 = Unspecified"),

	MAGNETICS_DIURNAL_CORRECTION(80, 84,
			"Magnetics diurnal correction in tenths of nanoteslas (gammas). (In nanoteslas) if 9-filled (i.e., set to +9999),"
					+ " total and residual fields are assumed to be uncorrected;"
					+ " if used, total and residuals are assumed to have been already corrected."),

	DEPTH_OR_ALTITUDE_OF_MAGNETICS_SENSOR(85, 90,
			"Depth or altitude of magnetics sensor in meters. + = Below sealevel - = Above sealevel"),

	OBSERVED_GRAVITY(91, 97, "Observed gravitry in tenths of milligals. Corrected for Eotvos, drift, and tares."),

	EOTVOS_CORRECTION(98, 103, "Eotvos correciton in tenths of milligals. E = 7.5 V cos phi sin alpha + 0.0042 V*V"),

	FREE_AIR_ANOMALY(104, 108,
			"Free air anomaly in tenths of milligals. Free-air Anomaly = G(observed) - G(theoretical)"),

	SEISMIC_LINE_NUMBER(109, 113, "Seismic line number used for cross referencing with seismic data."),

	SEISMIC_SHOT_POINT_NUMBER(114, 119, "Seismic shot point number."),

	QUALITY_CODE_FOR_NAVIGATION(120, "Quality code for navigation : "//
			+ "5 = Suspected, by the originating institution; "//
			+ "6 = Suspected, by the data center; "//
			+ "9 = No identifiable problem	found");

	/** Properties **/
	private final int startIndex;
	private final int stopIndex;
	public final String description;

	/** Constructor (indexes expected as described in MGD77 documentation) **/
	private MgdDataFields(int startIndex, int stopIndex, String description) {
		this.startIndex = startIndex - 1; // index from MGD documentation start at 1
		this.stopIndex = stopIndex;
		this.description = description;
	}

	/** Constructor with only one index **/
	private MgdDataFields(int startIndex, String description) {
		this(startIndex, startIndex + 1, description);
	}

	/**
	 * Extracts the part of the MGD77 data line of this field.
	 */
	public <T> T extract(String line, Function<String, T> stringValueConsumer, T invalidValue) {
		var stringValue = line.substring(startIndex, stopIndex);
		// invalid fields are filled with '9'
		boolean isValid = StringUtils.countMatches(stringValue.replace('+', '9'), "9") != stringValue.length();
		return isValid ? stringValueConsumer.apply(stringValue) : invalidValue;
	}

}
