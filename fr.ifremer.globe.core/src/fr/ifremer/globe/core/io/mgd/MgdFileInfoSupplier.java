package fr.ifremer.globe.core.io.mgd;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Supplier of {@link MgdFileInfo} from *.mgd77 files.
 */
@Component(name = "globe_drivers_mgd_file_info_supplier", //
		service = { IFileInfoSupplier.class, INavigationSupplier.class })
public class MgdFileInfoSupplier extends BasicFileInfoSupplier<MgdFileInfo> implements INavigationSupplier {

	/** Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(MgdFileInfoSupplier.class);

	public static final String EXTENSION_MGD = "mgd77";

	/**
	 * Constructor
	 */
	public MgdFileInfoSupplier() {
		super(EXTENSION_MGD, "MGD77 file", ContentType.MGD77);
	}

	/**
	 * Reads MGD file.
	 */
	@Override
	public MgdFileInfo makeFileInfo(String filePath) {
		List<MgdData> mgdData = new ArrayList<>();
		try {
			List<String> lines = Files.readAllLines(Paths.get(filePath), StandardCharsets.ISO_8859_1);
			for (int i = 25; i < lines.size(); i++) {
				var dataLine = new MgdData(lines.get(i));
				if (dataLine.isValid())
					mgdData.add(dataLine);
			}
			return new MgdFileInfo(filePath, mgdData);
		} catch (IOException | GIOException e) {
			LOGGER.error("Error while reading MGD77 file : {} ", filePath, e);
			return null;
		}
	}

	/**
	 * @return {@link INavigationDataSupplier} for {@link MgdFileInfo}.
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		return fileInfo instanceof MgdFileInfo ? Optional.of(new MgdNavigation((MgdFileInfo) fileInfo))
				: Optional.empty();
	}

}
