package fr.ifremer.globe.core.io.mgd;

import java.io.IOException;
import java.util.List;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.units.Units;

/**
 * {@link INavigationDataSupplier} implementation for {@link MgdFileInfo}.
 */
public class MgdNavigation implements INavigationDataSupplier {

	/** Constants **/
	public static final NavigationMetadataType<Double> DEPTH = new NavigationMetadataType<>("Depth", Units.METERS);
	public static final NavigationMetadataType<Double> GRAVITY = new NavigationMetadataType<>("Gravity",
			Units.MILLIGAL);
	public static final NavigationMetadataType<Double> EOTVOS_CORRECTION = new NavigationMetadataType<>(
			"Eotvos correction", Units.MILLIGAL);
	public static final NavigationMetadataType<Double> FREE_AIR_ANOMALY = new NavigationMetadataType<>(
			"Free air anomaly", Units.MILLIGAL);
	public static final NavigationMetadataType<Double> MAGNETICS = new NavigationMetadataType<>("Magnetism",
			Units.GAMMA);
	public static final NavigationMetadataType<Double> MAG_RESIDUAL_FIELD = new NavigationMetadataType<>(
			"Magnetics residual field", Units.GAMMA);

	private final MgdFileInfo mgdFileInfo;

	private final INavigationData navigationData;

	/** Constructor **/
	public MgdNavigation(MgdFileInfo mgdFileInfo) {
		this.mgdFileInfo = mgdFileInfo;
		this.navigationData = new MgdNavigationData();
	}

	@Override
	public INavigationData getNavigationData(boolean readonly) {
		return navigationData;
	}

	/**
	 * {@link INavigationData} implementation from {@link MgdFileInfo}.
	 */
	private class MgdNavigationData implements INavigationData {

		private final List<MgdData> data = mgdFileInfo.getData();

		/** Metadata **/
		private final List<NavigationMetadata<?>> metadata = List.of(
				NavigationMetadataType.TIME.withValueSupplier(index -> data.get(index).time.toEpochMilli()),
				DEPTH.withValueSupplier(index -> data.get(index).depth),
				EOTVOS_CORRECTION.withValueSupplier(index -> data.get(index).eotvosCorrection),
				FREE_AIR_ANOMALY.withValueSupplier(index -> data.get(index).freeAirAnomaly),
				GRAVITY.withValueSupplier(index -> data.get(index).gravity),
				MAGNETICS.withValueSupplier(index -> data.get(index).mag), //
				MAG_RESIDUAL_FIELD.withValueSupplier(index -> data.get(index).residualMagnetics));

		@Override
		public void close() throws IOException {
			// nothing to do
		}

		@Override
		public int size() {
			return mgdFileInfo.size();
		}

		@Override
		public String getFileName() {
			return mgdFileInfo.getFilename();
		}

		@Override
		public ContentType getContentType() {
			return mgdFileInfo.getContentType();
		}

		@Override
		public double getLatitude(int index) throws GIOException {
			return data.get(index).latitude;
		}

		@Override
		public double getLongitude(int index) throws GIOException {
			return data.get(index).longitude;
		}

		@Override
		public List<NavigationMetadata<?>> getMetadata() {
			return metadata;
		}

	}

}
