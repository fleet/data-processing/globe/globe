package fr.ifremer.globe.core.io.mgd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.ToDoubleFunction;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.BasicChartSeries;
import fr.ifremer.globe.core.model.chart.impl.ChartFileInfoDataSource;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartData;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;

/**
 * This class provides method to get {@link IChartSeries} from {@link MgdFileInfo}.
 */
@Component(name = "globe_drivers_mgd77_chart_series_supplier", service = { IChartSeriesSupplier.class })
public class Mgd77ChartSeriesProvider implements IChartSeriesSupplier {

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.MGD77);
	}

	/**
	 * @return a builder of {@link IChartSeries} for {@link MgdFileInfo}.
	 */
	@Override
	public List<MgdChartSeries> getChartSeries(IFileInfo fileInfo) {
		if (fileInfo instanceof MgdFileInfo mgdFileInfo) {
			var source = new ChartFileInfoDataSource<MgdFileInfo>(mgdFileInfo);
			return List.of(//
					new MgdChartSeries(source, "Gravity", data -> data.gravity),
					new MgdChartSeries(source, "Depth", data -> data.depth),
					new MgdChartSeries(source, "Eotvos Correction", data -> data.eotvosCorrection),
					new MgdChartSeries(source, "Free air anomaly", data -> data.freeAirAnomaly),
					new MgdChartSeries(source, "Magnetism (field 1)", data -> data.mag),
					new MgdChartSeries(source, "Residual magnetism", data -> data.residualMagnetics));
		}
		return Collections.emptyList();
	}

	/**
	 * Implementation of {@link IChartSeries} for a MGB77 variable.
	 */
	public static class MgdChartSeries extends BasicChartSeries<IChartData> {

		private final List<SimpleChartData> chartDataList = new ArrayList<>();

		private static final String SECTION_ID = "SECTION_";

		/**
		 * Constructor
		 */
		public MgdChartSeries(ChartFileInfoDataSource<MgdFileInfo> source, String title,
				ToDoubleFunction<MgdData> valueProvider) {
			super(title, source);
			var index = 0;
			var currentSectionIndex = 0;
			var currentSectionId = SECTION_ID + currentSectionIndex;
			var previousIsNaN = false;

			for (var mgdData : source.getFileInfo().getData()) {
				double value = valueProvider.applyAsDouble(mgdData);

				// update section ID (use to cut the chart series in several segments if NaN values)
				if (previousIsNaN && !Double.isNaN(value))
					currentSectionId = SECTION_ID + currentSectionIndex++;
				previousIsNaN = Double.isNaN(value);

				// create IChartData
				if (!Double.isNaN(value)) {
					var chartData = new SimpleChartData(this, index, mgdData.time.toEpochMilli(), value);
					chartData.setSectionId(currentSectionId);
					chartDataList.add(chartData);
					index++;
				}
			}
		}

		@Override
		public int size() {
			return chartDataList.size();
		}

		@Override
		public IChartData getData(int index) {
			return chartDataList.get(index);
		}

	}

}
