/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.marker.info;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.io.marker.csv.CsvMarker;
import fr.ifremer.globe.core.io.marker.csv.CsvMarkerFileWriter;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * FileInfo dedicated to Marker files
 */
public class MarkerFileInfo<T extends IMarker> extends BasicFileInfo implements IMarkerFileInfo<T> {

	/** All markers */
	protected List<T> markers;

	/**
	 * Constructor
	 */
	public MarkerFileInfo(String csvFilePath, List<T> markers) {
		super(csvFilePath, ContentType.MARKER_CSV);
		setMarkers(markers);
	}

	/**
	 * Constructor (builds an empty list of markers).
	 */
	public MarkerFileInfo(String csvFilePath) {
		this(csvFilePath, List.of());
	}

	protected String getTypeName() {
		return "Terrain";
	}

	/** {@inheritDoc} */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = new ArrayList<>();
		properties.add(Property.build("Number of points", markers.size()));
		properties.add(Property.build("Type", getTypeName()));
		properties.addAll(super.getProperties());
		return properties;
	}

	/**
	 * @return the {@link #markers}
	 */
	public List<T> getMarkers() {
		return markers;
	}

	@Override
	public void setMarkers(List<T> markers) {
		this.markers = markers;
		double north = Double.NaN;
		double south = Double.NaN;
		double east = Double.NaN;
		double west = Double.NaN;
		for (IMarker marker : markers) {
			north = Double.isNaN(north) ? marker.getLat() : Math.max(north, marker.getLat());
			south = Double.isNaN(south) ? marker.getLat() : Math.min(south, marker.getLat());
			east = Double.isNaN(east) ? marker.getLon() : Math.max(east, marker.getLon());
			west = Double.isNaN(west) ? marker.getLon() : Math.min(west, marker.getLon());
		}
		geoBox = new GeoBox(north, south, east, west);
	}

	@Override
	public void save() throws GIOException {
		CsvMarkerFileWriter<T, CsvMarker> writer = new CsvMarkerFileWriter<>(CsvMarker::new, CsvMarker.class,
				CsvMarker.COLUMNS);
		writer.write(getPath(), markers);
	}

}
