/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.marker.info;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.csv.IWellKnownCsvFileInfoSupplier;
import fr.ifremer.globe.core.io.marker.csv.CsvMarker;
import fr.ifremer.globe.core.io.marker.csv.CsvMarkerFileReader;
import fr.ifremer.globe.core.io.marker.csv.WCCsvMarker;
import fr.ifremer.globe.core.io.marker.csv.old.OldCsvMarkerReader;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.utils.FileUtils;

/**
 * Supplier of {@link IFileInfo} for markers files with *.csv extension.<br>
 * Markers files have a well known format and can be parsed without any FileInfoSettings.
 */
@Component(name = "globe_drivers_marker_csv_file_info_supplier", service = IWellKnownCsvFileInfoSupplier.class)
public class MarkerInfoSupplier implements IWellKnownCsvFileInfoSupplier {

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return ContentType.MARKER_CSV;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath) {
		if (!"csv".equalsIgnoreCase(FileUtils.getExtension(filePath))) {
			return Optional.empty();
		}

		// reader water column csv
		CsvMarkerFileReader<WCCsvMarker, IWCMarker> wCReader = new CsvMarkerFileReader<>(WCCsvMarker::toModelMarker,
				WCCsvMarker.class);
		Optional<List<IWCMarker>> wCMarkers = wCReader.read(filePath);
		if (wCMarkers.isPresent()) {
			return Optional.of(new WCMarkerFileInfo(filePath, wCMarkers.get()));
		}

		// reader : classic marker csv
		CsvMarkerFileReader<CsvMarker, IMarker> reader = new CsvMarkerFileReader<>(CsvMarker::toModelMarker,
				CsvMarker.class);
		Optional<List<IMarker>> markers = reader.read(filePath);
		if (markers.isPresent()) {
			return Optional.of(new MarkerFileInfo<>(filePath, markers.get()));
		}

		// try to parse the old CSV marker format
		markers = new OldCsvMarkerReader().read(filePath);
		if (markers.isPresent()) {
			// old water column markers
			if (markers.stream().allMatch(IWCMarker.class::isInstance)) {
				List<IWCMarker> wCOldMarkers = markers.stream().map(IWCMarker.class::cast).collect(Collectors.toList());
				return Optional.of(new WCMarkerFileInfo(filePath, wCOldMarkers));
			}
			// old terrain markers
			return Optional.of(new MarkerFileInfo<>(filePath, markers.get()));
		}

		// no markers found : return null
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<FileInfoSettings> evaluateSettings(String filePath) {
		return Optional.empty();
	}

}