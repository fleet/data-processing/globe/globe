/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.marker.info;

import java.util.List;

import fr.ifremer.globe.core.io.marker.csv.CsvMarkerFileWriter;
import fr.ifremer.globe.core.io.marker.csv.WCCsvMarker;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * FileInfo dedicated to Marker files
 */
public class WCMarkerFileInfo extends MarkerFileInfo<IWCMarker> {

	/**
	 * Constructor
	 */
	public WCMarkerFileInfo(String csvFilePath, List<IWCMarker> markers) {
		super(csvFilePath, markers);
	}

	/**
	 * Constructor (builds an empty list of markers).
	 */
	public WCMarkerFileInfo(String csvFilePath) {
		super(csvFilePath);
	}
	
	@Override
	protected String getTypeName() {
		return "Water column";
	}

	@Override
	public void save() throws GIOException {
		CsvMarkerFileWriter<IWCMarker, WCCsvMarker> writer = new CsvMarkerFileWriter<>(
				WCCsvMarker::new, WCCsvMarker.class, WCCsvMarker.COLUMNS);
		writer.write(getPath(), markers);
	}

}
