/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.marker.csv;

import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvNumber;

import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.model.marker.impl.MarkerBuilder;

/**
 * Water column CSV marker : extends the {@link CsvMarker} to add water column properties.
 */
public class WCCsvMarker extends CsvMarker {

	public static final String COL_LAYER = "layer";
	public static final String COL_PING = "ping";
	public static final String COL_DATE = "date";
	public static final String COL_TIME = "time";
	public static final String COL_HEIGHT_ABOVE_SEA_FLOOR = "height_above_sea_floor";
	public static final String COL_SEA_FLOOR_ELEVATION = "sea_floor_elevation";

	public static final List<String> COLUMNS = Arrays.asList(COL_ID, COL_LAYER, COL_PING, COL_LAT, COL_LON, COL_LAT_DMD,
			COL_LON_DMD, COL_HEIGHT_ABOVE_SEA_SURFACE, COL_HEIGHT_ABOVE_SEA_FLOOR, COL_SEA_FLOOR_ELEVATION,
			COL_SEA_FLOOR_LAYER, COL_DATE, COL_TIME, COL_MARKER_COLOR, COL_MARKER_SIZE, COL_MARKER_SHAPE, COL_GROUP,
			COL_CLASS, COL_COMMENT);

	@CsvBindByName(column = COL_LAYER)
	protected String layer;

	@CsvBindByName(column = COL_PING, required = true)
	protected int ping;

	@CsvBindByName(column = COL_DATE)
	protected String date;

	@CsvBindByName(column = COL_TIME)
	protected String time;

	// Height above seafloor
	@CsvBindByName(column = COL_HEIGHT_ABOVE_SEA_FLOOR, locale = "en", required = true)
	@CsvNumber(value = "#.###")
	protected double altitude;

	// Depth at the marker position
	@CsvBindByName(column = COL_SEA_FLOOR_ELEVATION, locale = "en", required = true)
	@CsvNumber(value = "#.###")
	protected double depth;

	public WCCsvMarker() {
		super();
	}

	public WCCsvMarker(IWCMarker marker) {
		super(marker);
		// water column properties
		this.layer = marker.getWaterColumnLayer();
		this.ping = marker.getPing();
		this.date = marker.getDateString();
		this.time = marker.getTimeString();
		this.altitude = marker.getAtltitude().orElse(Double.NaN);
		this.depth = marker.getSeaFloorElevation().orElse(Double.NaN);
	}

	@Override
	public IWCMarker toModelMarker() {
		// get builder, filled with basic marker attributes
		MarkerBuilder markerBuilder = getMarkerBuilder();

		// add watercolumn attributes
		markerBuilder.setWaterColumnLayerPing(layer, ping).setDepth(depth);

		// try to parse date time
		Optional<Instant> optDateTime = Optional.empty();
		try {
			optDateTime = Optional.of(Instant.parse(date + "T" + time + "Z"));
		} catch (DateTimeParseException e) {
		}
		optDateTime.ifPresent(markerBuilder::setDateTime);

		return (IWCMarker) markerBuilder.build();
	}

}
