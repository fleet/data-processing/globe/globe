/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.marker.csv.old;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.opencsv.bean.CsvBindByName;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerTypology;
import fr.ifremer.globe.core.model.marker.impl.MarkerBuilder;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Default implementation of IMarker
 */
public class OldCsvMarker {

	public static final String COL_ID = "id";
	public static final String COL_TYPE = "type";
	public static final String COL_LAT = "lat deg";
	public static final String COL_LAT_2 = "lat(deg)";
	public static final String COL_LON = "lon deg";
	public static final String COL_LON_2 = "lon(deg)";
	public static final String COL_LAT_DMD = "lat DMD";
	public static final String COL_LON_DMD = "lon DMD";
	public static final String COL_IMMERSION = "immersion";
	public static final String COL_REF = "ref";
	public static final String COL_LAYER = "layer";
	public static final String COL_PING = "ping";
	public static final String COL_DATE = "date";
	public static final String COL_TIME = "time";
	public static final String COL_TOPPOINT = "topPoint";
	public static final String COL_LINE = "line";
	public static final String COL_BOTTOMPOINT = "bottomPoint";
	public static final String COL_POINTCOLOR = "pointColor";
	public static final String COL_POINTSHAPE = "pointShape";
	public static final String COL_POINTSIZE = "pointSize";
	public static final String COL_GROUP = "group";
	public static final String COL_CLASS = "class";
	public static final String COL_COMMENT = "comment";

	protected static final List<String> COLUMNS = Arrays.asList(COL_ID, COL_TYPE, COL_LAT, COL_LON, COL_LAT_DMD,
			COL_LON_DMD, COL_IMMERSION, COL_REF, COL_LAYER, COL_PING, COL_DATE, COL_TIME, COL_TOPPOINT, COL_LINE,
			COL_BOTTOMPOINT, COL_POINTCOLOR, COL_POINTSHAPE, COL_POINTSIZE, COL_GROUP, COL_CLASS, COL_COMMENT);

	public static final String TYPE_POINT = "POINT";
	public static final String TYPE_LINE = "LINE";

	@CsvBindByName(column = COL_ID, required = true)
	protected int id;
	@CsvBindByName(column = COL_TYPE)
	protected String type;
	@CsvBindByName(column = COL_LAT, locale = "en")
	protected double lat = Double.NaN;
	@CsvBindByName(column = COL_LON, locale = "en")
	protected double lon = Double.NaN;
	@CsvBindByName(column = COL_LAT_2, locale = "en")
	protected double lat2 = Double.NaN;
	@CsvBindByName(column = COL_LON_2, locale = "en")
	protected double lon2 = Double.NaN;
	@CsvBindByName(column = COL_LAT_DMD)
	protected String latDMD;
	@CsvBindByName(column = COL_LON_DMD)
	protected String lonDMD;
	@CsvBindByName(column = COL_IMMERSION, locale = "en", required = true)
	protected double immersion;
	@CsvBindByName(column = COL_REF)
	protected String ref;
	@CsvBindByName(column = COL_LAYER)
	protected String layer;
	@CsvBindByName(column = COL_PING)
	protected int ping;
	@CsvBindByName(column = COL_DATE)
	protected String date;
	@CsvBindByName(column = COL_TIME)
	protected String time;
	@CsvBindByName(column = COL_TOPPOINT, required = true)
	protected boolean topPoint;
	@CsvBindByName(column = COL_LINE, required = true)
	protected boolean line;
	@CsvBindByName(column = COL_BOTTOMPOINT, required = true)
	protected boolean bottomPoint;
	@CsvBindByName(column = COL_POINTCOLOR)
	protected String pointColor;
	@CsvBindByName(column = COL_POINTSHAPE)
	protected String pointShape;
	@CsvBindByName(column = COL_POINTSIZE, required = true)
	protected int pointSize;
	@CsvBindByName(column = COL_GROUP)
	protected String group;
	@CsvBindByName(column = COL_CLASS)
	protected String clazz;
	@CsvBindByName(column = COL_COMMENT)
	protected String comment;

	/**
	 * Builds a {@link IMarker} from this bean.
	 */
	public IMarker toModelMarker() {
		double latitude = Double.isNaN(lat) ? lat2 :lat;
		double longitude = Double.isNaN(lon) ? lon2 :lon;
		MarkerBuilder markerBuilder = new MarkerBuilder(Integer.toString(id), latitude, longitude, immersion);
		// display
		markerBuilder.setSize(pointSize != 0 ? pointSize / 3 : 15);
		// color
		markerBuilder.setColor(GColor.fromTypologyColor(pointColor));
		// shape
		String shape = "Sphere";
		if (pointShape != null) {
			switch ("pointShape") {
			case "plain":
				shape = "Shpere";
				break;
			// TODO parse others shapes...
			}
		}
		markerBuilder.setShape(shape);
		// typology
		markerBuilder.setTypology(new IMarkerTypology() {
			@Override
			public String getGroup() {
				return group;
			}

			@Override
			public String getClazz() {
				return clazz;
			}
		});

		// comment
		markerBuilder.setComment(comment);

		// if exists... add watercolun values
		if (!layer.equals("-")) {
			markerBuilder.setWaterColumnLayerPing(layer, ping);

			// try to parse date time
			Optional<Instant> optDateTime = Optional.empty();
			try {
				optDateTime = Optional
						.of(LocalDateTime.parse(date + " " + time, DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss"))
								.toInstant(ZoneOffset.UTC));
			} catch (DateTimeParseException e) {

			}
			optDateTime.ifPresent(markerBuilder::setDateTime);
		}

		return markerBuilder.build();
	}

	public String getRef() {
		return ref;
	}

}
