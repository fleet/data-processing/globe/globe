/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.marker.csv;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.opencsv.CSVWriter;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.bean.exceptionhandler.ExceptionHandlerIgnore;
import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to write CSV marker files.
 */
public class CsvMarkerFileWriter<T extends IMarker, U extends CsvMarker> {

	private final Function<T, U> modelToCsv;
	private final Class<U> csvMarkerClass;
	private final List<String> columns;
	
	public static <T extends IMarker> void writ(String filePath, List<T> markers) throws GIOException {
		getWriter(markers).write(filePath, markers);
	}

	public static CsvMarkerFileWriter getWriter(List<? extends IMarker> markers) {
		if(markers instanceof List<?>) {
			
		}
		if (markers.get(0) instanceof IWCMarker) {
			// get a writer for water column markers
			return new CsvMarkerFileWriter<IWCMarker, WCCsvMarker>(WCCsvMarker::new, WCCsvMarker.class,
					WCCsvMarker.COLUMNS);
		} else
			// get a class writer
			return new CsvMarkerFileWriter<>(CsvMarker::new, CsvMarker.class, CsvMarker.COLUMNS);
	}

	public CsvMarkerFileWriter(Function<T, U> modelToCsv, Class<U> csvMarkerClass, List<String> columns) {
		this.modelToCsv = modelToCsv;
		this.csvMarkerClass = csvMarkerClass;
		this.columns = columns.stream().map(String::toUpperCase).collect(Collectors.toList());
	}

	/**
	 * Writes a CSV marker files.
	 */
	public void write(String filePath, List<T> markers) throws GIOException {
		// parse to CsvMarkers
		List<U> csvMarkers = markers.stream().map(modelToCsv::apply).collect(Collectors.toList());

		HeaderColumnNameMappingStrategy<U> strategy = new HeaderColumnNameMappingStrategy<>();
		Comparator<String> colCompare = (col1, col2) -> Integer.compare(columns.indexOf(col1.toUpperCase()),
				columns.indexOf(col2.toUpperCase()));
		strategy.setColumnOrderOnWrite(colCompare);
		strategy.setType(csvMarkerClass);

		try (Writer writer = new FileWriter(filePath)) {
			if (!markers.isEmpty()) {
				StatefulBeanToCsv<U> statefulBeanToCsv = new StatefulBeanToCsvBuilder<U>(writer)//
						.withExceptionHandler(new ExceptionHandlerIgnore())//
						.withMappingStrategy(strategy) //
						.withOrderedResults(true) //
						.withSeparator(CsvMarker.SEPARATOR) //
						.build();
				statefulBeanToCsv.write(csvMarkers);
			} else {
				// Headers only
				try (CSVWriter csvWriter = new CSVWriter(writer, CsvMarker.SEPARATOR, CSVWriter.DEFAULT_QUOTE_CHARACTER,
						CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
					csvWriter.writeNext(columns.toArray(new String[columns.size()]));
				}
			}
		} catch (CsvException | IOException e) {
			throw new GIOException("Error : " + e.getMessage(), e);
		}
	}

}
