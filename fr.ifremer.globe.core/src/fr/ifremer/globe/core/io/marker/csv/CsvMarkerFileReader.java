package fr.ifremer.globe.core.io.marker.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.exceptionhandler.ExceptionHandlerIgnore;

import fr.ifremer.globe.core.model.marker.IMarker;

/**
 * This class provides methods to read CSV marker files.
 */
public class CsvMarkerFileReader<T, U extends IMarker> {
	final Function<T, U> csvToModel;
	final Class<T> csvMarkerClass;

	public CsvMarkerFileReader(Function<T, U> csvToModel, Class<T> csvMarkerClass) {
		this.csvToModel = csvToModel;
		this.csvMarkerClass = csvMarkerClass;
	}

	/**
	 * @return an {@link Optional} which contains a list of {@link IMarker} if the file has been parsed.
	 */
	public Optional<List<U>> read(String filePath) {
		try {
			// read CSV file
			HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
			strategy.setType(csvMarkerClass);
			List<T> beans = //
					new CsvToBeanBuilder<T>(new FileReader(new File(filePath)))//
							.withMappingStrategy(strategy)//
							.withSeparator(CsvMarker.SEPARATOR)//
							// Bad markers are ignored
							.withExceptionHandler(new ExceptionHandlerIgnore()).build()//
							.parse();

			// convert to marker
			List<U> markers = beans.stream().map(csvToModel::apply).collect(Collectors.toList());

			return Optional.of(markers);
		} catch (RuntimeException | FileNotFoundException e) {
			// Something went wrong : input file not readable
		}
		return Optional.empty();
	}

}
