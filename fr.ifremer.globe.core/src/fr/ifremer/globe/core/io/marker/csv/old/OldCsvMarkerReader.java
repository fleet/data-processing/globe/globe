package fr.ifremer.globe.core.io.marker.csv.old;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.exceptionhandler.ExceptionHandlerIgnore;

import fr.ifremer.globe.core.model.marker.IMarker;

/**
 * This class provides methods to read CSV marker files.
 */
public class OldCsvMarkerReader {

	/**
	 * @return an {@link Optional} which contains a list of {@link IMarker} if the file has been parsed.
	 */
	public Optional<List<IMarker>> read(String filePath) {
		try {
			// read CSV file
			HeaderColumnNameMappingStrategy<OldCsvMarker> strategy = new HeaderColumnNameMappingStrategy<>();
			strategy.setType(OldCsvMarker.class);
			List<OldCsvMarker> beans = new CsvToBeanBuilder<OldCsvMarker>(new FileReader(new File(filePath)))//
					.withSeparator(',') //
					.withMappingStrategy(strategy) //
					.withExceptionHandler(new ExceptionHandlerIgnore())// Bad markers are ignored
					.build()//
					.parse();

			// convert to marker
			List<IMarker> markers = beans.stream()//
					.filter(csvMarker -> csvMarker.getRef().equalsIgnoreCase("absolute"))// keep only "Absolute" points
					.map(OldCsvMarker::toModelMarker) //
					.collect(Collectors.toList());

			return Optional.of(markers);
		} catch (RuntimeException | FileNotFoundException e) {
			// Something went wrong : input file not readable
		}

		return Optional.empty();
	}

}
