/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.marker.csv;

import java.util.Arrays;
import java.util.List;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvNumber;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerTypology;
import fr.ifremer.globe.core.model.marker.impl.MarkerBuilder;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.units.Units;

/**
 * CSV marker.
 */
public class CsvMarker {

	static final char SEPARATOR = ';';

	public static final String COL_ID = "id";
	public static final String COL_LAT = "latitude_deg";
	public static final String COL_LON = "longitude_deg";
	public static final String COL_LAT_DMD = "latitude_DMD";
	public static final String COL_LON_DMD = "longitude_DMD";
	public static final String COL_HEIGHT_ABOVE_SEA_SURFACE = "height_above_sea_surface";
	public static final String COL_SEA_FLOOR_LAYER = "sea_floor_layer";
	public static final String COL_MARKER_COLOR = "marker_color";
	public static final String COL_MARKER_SIZE = "marker_size";
	public static final String COL_MARKER_SHAPE = "marker_shape";
	public static final String COL_GROUP = "group";
	public static final String COL_CLASS = "class";
	public static final String COL_COMMENT = "comment";

	public static final List<String> COLUMNS = Arrays.asList(COL_ID, COL_LAT, COL_LON, COL_LAT_DMD, COL_LON_DMD,
			COL_HEIGHT_ABOVE_SEA_SURFACE, COL_SEA_FLOOR_LAYER, COL_MARKER_COLOR, COL_MARKER_SIZE, COL_MARKER_SHAPE,
			COL_GROUP, COL_CLASS, COL_COMMENT);

	@CsvBindByName(column = COL_ID, required = true)
	protected String id;

	@CsvBindByName(column = COL_LAT, locale = "en", required = true)
	@CsvNumber(value = "#.#######")
	protected double lat;

	@CsvBindByName(column = COL_LON, locale = "en", required = true)
	@CsvNumber(value = "#.#######")
	protected double lon;

	@CsvBindByName(column = COL_LAT_DMD)
	protected String latDMD;

	@CsvBindByName(column = COL_LON_DMD)
	protected String lonDMD;

	// Height below sea surface
	@CsvBindByName(column = COL_HEIGHT_ABOVE_SEA_SURFACE, locale = "en", required = true)
	@CsvNumber(value = "#.###")
	protected double elevation;

	@CsvBindByName(column = COL_MARKER_COLOR)
	protected String color;

	@CsvBindByName(column = COL_MARKER_SIZE, required = true)
	protected float pointSize;

	@CsvBindByName(column = COL_MARKER_SHAPE, required = true)
	protected String shape;

	@CsvBindByName(column = COL_GROUP)
	protected String group;

	@CsvBindByName(column = COL_CLASS)
	protected String clazz;

	@CsvBindByName(column = COL_COMMENT)
	protected String comment;

	@CsvBindByName(column = COL_SEA_FLOOR_LAYER)
	protected String bathyLayer;

	public CsvMarker() {

	}

	public CsvMarker(IMarker marker) {
		this.id = marker.getID();
		this.lat = marker.getLat();
		this.lon = marker.getLon();
		this.latDMD = Units.degreestoDMString(marker.getLat()).replace("°", " ").replace("\u2019", "");
		this.lonDMD = Units.degreestoDMString(marker.getLon()).replace("°", " ").replace("\u2019", "");
		this.elevation = marker.getElevation();
		this.bathyLayer = marker.getBathyLayer();
		this.color = marker.getColor().toCSSString();
		this.pointSize = marker.getSize();
		this.shape = marker.getShape();
		this.comment = marker.getComment();
		marker.getTypology().ifPresent(typology -> {
			this.clazz = typology.getClazz();
			this.group = typology.getGroup();
		});
	}

	public IMarker toModelMarker() {
		return getMarkerBuilder().build();
	}

	protected MarkerBuilder getMarkerBuilder() {
		var markerBuilder = new MarkerBuilder(id, lat, lon, elevation) // position
				// bathy layer
				.setLayer(bathyLayer)
				// display
				.setColor(new GColor(color)).setSize(pointSize).setShape(shape)
				// comment
				.setComment(comment);

		// typology
		if ((group != null && !group.isEmpty()) || (clazz != null && !clazz.isEmpty()))
			markerBuilder.setTypology(new IMarkerTypology() {
				@Override
				public String getGroup() {
					return group;
				}

				@Override
				public String getClazz() {
					return clazz;
				}
			});
		return markerBuilder;
	}

}
