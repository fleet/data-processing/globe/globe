package fr.ifremer.globe.core.io.sonar.griddeddata;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * {@link INavigationData} for gridded data.
 */
public class SonarNcGriddedDataNavigation implements INavigationData, IDataContainerOwner {

	/** File **/
	private final SonarNcInfo sonarNcInfo;

	/** Navigation data **/
	private final List<NavigationMetadata<?>> metadata = new ArrayList<>();

	/** Layers & buffers **/
	private int pingDim;
	private int rangeDim;
	private ByteBuffer latBuffer;
	private ByteBuffer lonBuffer;

	/**
	 * Constructor
	 */
	public SonarNcGriddedDataNavigation(SonarNcInfo sonarNcInfo) throws GIOException {
		this.sonarNcInfo = sonarNcInfo;
		try {
			// open and read position variables
			sonarNcInfo.open(true);
			var group = sonarNcInfo.getFile().getGroup(sonarNcInfo.inferGridGroupName(0));
			pingDim = (int) group.getDimension(SonarNcGriddedDataConstants.PING_DIM).getLength();
			rangeDim = (int) group.getDimension(SonarNcGriddedDataConstants.RANGE_DIM).getLength();
			int txBeamDim = (int) group.getDimension(SonarNcGriddedDataConstants.TX_BEAM_DIM).getLength();

			NetcdfVariable latVariable = group.getVariable(SonarNcGriddedDataConstants.CELL_LATITUDE_VAR);
			latBuffer = ByteBuffer.allocateDirect(pingDim * rangeDim * Double.BYTES).order(ByteOrder.nativeOrder());
			latVariable.read(new long[] { pingDim, rangeDim, 1 }, latBuffer, DataType.DOUBLE, Optional.empty());

			NetcdfVariable lonVariable = group.getVariable(SonarNcGriddedDataConstants.CELL_LONGITUDE_VAR);
			lonBuffer = ByteBuffer.allocateDirect(pingDim * rangeDim * Double.BYTES).order(ByteOrder.nativeOrder());
			lonVariable.read(new long[] { pingDim, rangeDim, 1 }, lonBuffer, DataType.DOUBLE, Optional.empty());

			var timeVariable = group.getVariable(SonarNcGriddedDataConstants.CELL_PING_TIME_VAR);
			var timeBuffer = ByteBuffer.allocateDirect(pingDim * txBeamDim * Long.BYTES).order(ByteOrder.nativeOrder());
			timeVariable.read(new long[] { 0, 0 }, new long[] { pingDim, 1 }, timeBuffer, DataType.LONG,
					Optional.empty());
			metadata.add(NavigationMetadataType.TIME.withValueSupplier(i -> timeBuffer.getLong(i * Long.BYTES) * 1000));
		} catch (NCException e) {
			throw new GIOException("Error while reading navigation varriable : " + e.getMessage(), e);
		} finally {
			sonarNcInfo.close();
		}
	}

	@Override
	public String getFileName() {
		return sonarNcInfo.getFilename();
	}

	@Override
	public int size() {
		return pingDim;
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return lonBuffer.getDouble(index * rangeDim * Double.BYTES);
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return latBuffer.getDouble(index * rangeDim * Double.BYTES);
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

}
