package fr.ifremer.globe.core.io.sonar.info;

import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.PlatformGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.SonarGrp;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters.Type;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderNavigationData;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.EnvironmentLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.PlatformLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.GridGroup1Layers;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to read a Sonar Netcdf file.
 */
public class SonarNcInfoReader {

	public final Logger logger = LoggerFactory.getLogger(SonarNcInfoReader.class);

	/**********************************************************************************************************
	 * Attribute readers
	 **********************************************************************************************************/

	/** Reads convension version **/
	public void readVersion(SonarNcInfo info) throws GIOException {
		try {
			if (info.getFile().hasAttribute(RootGrp.ATT_SONAR_CONVENTION_NAME)
					&& info.getFile().hasAttribute(RootGrp.ATT_SONAR_CONVENTION_VERSION)) {
				String convention = info.getFile().getAttributeAsString(RootGrp.ATT_SONAR_CONVENTION_NAME);
				if (!"SONAR-netCDF4".equalsIgnoreCase(convention)) {
					logger.error("SonarNcInfo loading: error Sonar convension version: " + convention);
					info.setSonarConventionVersion("[ERROR: invalid version] " + convention);
				}
				String version = info.getFile().getAttributeAsString(RootGrp.ATT_SONAR_CONVENTION_VERSION);
				info.setSonarConventionVersion(version);
			} else {
				throw new GIOException("Not a Sonar Netcdf file");
			}
		} catch (NCException e) {
			info.setSonarConventionVersion("not found");
			logger.error("SonarNcInfo loading: error Sonar convension version: " + e.getMessage(), e);
		}
	}

	/** Reads constructor **/
	public void readConstructor(SonarNcInfo info) {
		try {
			NetcdfGroup sonar = info.getFile().getGroup(SonarGrp.GROUP_NAME);
			if (sonar == null) {
				return;
			}
			if (sonar.hasAttribute(SonarGrp.ATT_SONAR_MANUFACTURER)) {
				String name = sonar.getAttributeAsString(SonarGrp.ATT_SONAR_MANUFACTURER);
				if (name != null && !name.isBlank() && !name.isEmpty()) {
					info.setConstructor(name);
				}
			}
		} catch (Exception e) {
			logger.error("SonarNcInfo while retrieving SonarGrp.ATT_SONAR_MANUFACTURER " + e.getMessage(), e);
		}
	}

	/** Reads swath count **/
	public void readSwathCount(SonarNcInfo info) {
		try {
			var pingTimeDim = info.getFile()
					.getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.PING_TIME_DIM_NAME);
			if (pingTimeDim != null) {
				info.setCycleCount((int) pingTimeDim.getLength());
			}
		} catch (NCException e) {
			info.setCycleCount(-1);
			logger.error("SonarNcInfo loading: error with swath count: " + e.getMessage(), e);
		}
	}

	/** Return the value of one dimension on grid group */
	public int readGridGroupDim(SonarNcInfo info, int gridGroupIndex, String dimName) {
		int result = 0;
		try {
			NCDimension dimension = info.getFile()
					.getDimension(info.inferGridGroupName(gridGroupIndex) + "/" + dimName);
			if (dimension != null) {
				result = (int) dimension.getLength();
			}
		} catch (NCException e) {
			logger.warn("SonarNcInfo loading: error grid group dimension : {}", e.getMessage());
		}
		return result;
	}

	/** Reads Beam group count **/
	public void readSubGroupCount(SonarNcInfo info) {
		Pattern gridPattern = Pattern.compile("(Grid.+)\\d+", Pattern.CASE_INSENSITIVE);

		NetcdfFile netcdfFile = info.getFile();
		int beamGroupCount = 0;
		try {
			var sonarGrp = netcdfFile.getGroup("/Sonar");
			if (sonarGrp != null) {
				var sorarSubGroups = sonarGrp.getGroups();
				for (NetcdfGroup sonarSubGroup : sorarSubGroups) {
					var groupName = sonarSubGroup.getName();
					if (groupName.startsWith(SonarBeamGroup.BEAM_GROUP)) {
						beamGroupCount++;
					} else {
						Matcher gridMatcher = gridPattern.matcher(groupName);
						if (gridMatcher.matches()) {
							info.getGridGroupNames().add(groupName);
						}
					}
				}
			}
			info.setBeamGroupCount(beamGroupCount);
		} catch (NCException e) {
			logger.warn("SonarNcInfo loading: error with sonar sub group count: {}", e.getMessage());
		}
	}

	/** Reads layer /sonar/Grid_group_1/frequency or /sonar/Beam_group_XXX/frequency **/
	public void readFrequencies(SonarNcInfo info, SounderDataContainer dataContainer) {
		try {
			if (info.getGridGroupCount() > 0) {
				var gridFrequencylayer = new PredefinedLayers<>("/Sonar/" + info.getGridGroupNames().get(0),
						GridGroup1Layers.FREQUENCY);
				if (dataContainer.hasLayer(gridFrequencylayer)) {
					info.setFrequencyUnit(gridFrequencylayer.getUnit());
					FloatLoadableLayer1D frequencies = dataContainer.getLayer(gridFrequencylayer);
					for (int i = 0; i < frequencies.getDimensions()[0]; i++)
						info.getFrequencies().add(frequencies.get(i));
				}
			}

			if (info.getFrequencies().isEmpty()) {
				// Search frequencies en /Sonar/Beam_group_XXX/Frequency
				for (int i = 0; i < info.getBeamGroupCount(); i++) {
					var beamFrequencyLayer = new PredefinedLayers<>(
							BeamGroup1Layers.GROUP.replace("1", String.valueOf(i + 1)), BeamGroup1Layers.FREQUENCY);
					if (dataContainer.hasLayer(beamFrequencyLayer)) {
						FloatLoadableLayer1D frequencyLayer = dataContainer.getLayer(beamFrequencyLayer);
						info.setFrequencyUnit(frequencyLayer.getUnit());
						if (frequencyLayer.getDimensions()[0] >= 1) {
							info.getFrequencies().add(frequencyLayer.get(0));
						}
					}
				}
			}

			// Search frequencies en /Environment/frequency
			if (info.getFrequencies().isEmpty() && dataContainer.hasLayer(EnvironmentLayers.FREQUENCY)) {
				FloatLoadableLayer1D frequencies = dataContainer.getLayer(EnvironmentLayers.FREQUENCY);
				info.setFrequencyUnit(frequencies.getUnit());
				for (int i = 0; i < frequencies.getDimensions()[0]; i++)
					info.getFrequencies().add(frequencies.get(i));
			}
		} catch (GIOException e) {
			logger.warn("SonarNcInfo loading: error while loading frequencies layer : {}", e.getMessage());
		}
	}

	/**
	 * Reads Geobox from the navigation data. Navigation data is obtained using only valid swaths.
	 *
	 * @throws GIOException
	 **/
	public void readGeobox(SonarNcInfo info, SounderDataContainer dataContainer) throws GIOException {
		// we need to compute a geobox. we're gonna take it from the navigation data
		SounderNavigationData navData = new SounderNavigationData(dataContainer);
		info.setGeoBox(navData.computeGeoBox());
	}

	/**
	 * Reads first and last swath date
	 *
	 * @throws GIOException
	 **/
	public void readFirstLastDate(SonarNcInfo info, SounderDataContainer dataContainer) throws GIOException {
		try {
			LongLoadableLayer1D swathTimeLayer = dataContainer.getLayer(BeamGroup1Layers.PING_TIME);
			int swathCount = info.getCycleCount();
			long minTime = swathTimeLayer.get(0);
			long maxTime = swathTimeLayer.get(swathCount - 1);
			String unit = swathTimeLayer.getUnit();
			// Default unit is "nanoseconds since 1970-01-01 00:00:00Z"
			if (unit == null || unit.isEmpty() || unit.contains("1970")) {
				info.setFirstSwathDate(new Date(DateUtils.nanoSecondToMilli(minTime)));
				info.setLastSwathDate(new Date(DateUtils.nanoSecondToMilli(maxTime)));
			} else {
				// Possible unit in a Sonar Netcdf : "nanoseconds since 1601-01-01 00:00:00Z"
				info.setFirstSwathDate(DateUtils.getDateWithNanosecondsFrom1601(minTime));
				info.setLastSwathDate(DateUtils.getDateWithNanosecondsFrom1601(maxTime));
			}
		} catch (DateTimeParseException e) {
			logger.error("SonarNcInfo loading: error with swath dates: " + e.getMessage(), e);
		}
	}

	/** Reads antennas **/
	public void readAntennaProperties(SonarNcInfo info, SounderDataContainer dataContainer) {
		List<AntennaInstallationParameters> allAntennas = info.getAntennaParameters();
		allAntennas.clear();
		try {
			var transducerDim = info.getFile()
					.getDimension(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.TRANSDUCER_DIM_NAME);
			if (transducerDim == null) {
				return;
			}
			int antennaCount = (int) transducerDim.getLength();

			ByteLayer1D type = dataContainer.getLayer(PlatformLayers.TRANSDUCER_FUNCTION);
			FloatLayer1D x = dataContainer.getLayer(PlatformLayers.TRANSDUCER_OFFSET_X);
			FloatLayer1D y = dataContainer.getLayer(PlatformLayers.TRANSDUCER_OFFSET_Y);
			FloatLayer1D z = dataContainer.getLayer(PlatformLayers.TRANSDUCER_OFFSET_Z);
			FloatLayer1D xRotation = dataContainer.getLayer(PlatformLayers.TRANSDUCER_ROTATION_X);
			FloatLayer1D yRotation = dataContainer.getLayer(PlatformLayers.TRANSDUCER_ROTATION_Y);
			FloatLayer1D zRotation = dataContainer.getLayer(PlatformLayers.TRANSDUCER_ROTATION_Z);

			String[] antennaIds = null;
			NCVariable antennaVar = info.getFile()
					.getVariable(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.TRANSDUCER_IDS);
			if (antennaVar != null) {
				antennaIds = antennaVar.get_string(new long[] { 0 }, new long[] { antennaCount });
			}
			for (int i = 0; i < antennaCount; i++) {
				// Transducer serial number OR identification name => may be not be an integer
				var id = Integer.toString(i);
				try {
					id = antennaIds != null ? antennaIds[i] : Integer.toString(i);
				} catch (NumberFormatException e) {
				}

				var antennaType = Type.UNKNOWN;
				if (type.get(i) == (byte) 0)
					antennaType = Type.RX;
				else if (type.get(i) == (byte) 1)
					antennaType = Type.TX;
				else if (type.get(i) == (byte) 3)
					antennaType = Type.TXRX;

				var antenna = new AntennaInstallationParameters(//
						antennaType, id, //
						x.get(i), y.get(i), z.get(i), //
						xRotation.get(i), yRotation.get(i), zRotation.get(i));

				allAntennas.add(antenna);

				if (antenna.type == Type.UNKNOWN) {
					throw new GIOException("Unknown antenna type : id = " + id);
				}
			}
		} catch (GException e) {
			logger.error("SonarNcInfo loading: error antennas: " + e.getMessage(), e);
		}
	}

	/** Reads WC beam / sample count **/
	public void readWcDims(SonarNcInfo info) {
		try {
			if (info.getFile().getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.BEAM_DIM_NAME) != null) {
				info.setWcRxBeamCount(info.getFile()
						.getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.BEAM_DIM_NAME).getLength());
			}
		} catch (NCException e) {
			logger.error("SonarNcInfo loading: error parsing WC dimensions: " + e.getMessage(), e);
		}
	}

	/** Reads Sonar root group attributes **/
	public void readRawAttributes(SonarNcInfo info) {
		try {
			Property<String> rawAttributes = Property.build("Raw properties", "");
			info.getFile().getGlobalAttributs().forEach((k, v) -> rawAttributes.add(Property.build(k, v)));
			info.setRawAttributes(rawAttributes);
		} catch (NCException e) {
			logger.error("Error during SonarNc file attributs retrievement: " + e.getMessage(), e);
		}
	}

	/**
	 * Reads BeamGroup attributes
	 * 
	 * @throws GIOException
	 **/
	public void readBeamGroupAttributes(SonarNcInfo info, SounderDataContainer dataContainer) throws GIOException {
		SonarBeamGroup beamGroup = dataContainer.getBeamGroup();
		try {
			if (info.getFile().hasAttribute(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_MRU)) {
				int mruIndex = info.getFile()
						.getAttributeInt(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_MRU);
				NCVariable mruVar = info.getFile().getVariable(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.MRU_IDS);
				if (mruVar != null) {
					String[] mruIds = mruVar.get_string(new long[] { mruIndex }, new long[] { 1 });
					info.setPreferedAttitudeGroupId(mruIds[0]);
				}
			}
		} catch (NCException e) {
			logger.error("XSFInfo loading: error parsing beamGroup prefered MRU: " + e.getMessage(), e);
		}

		try {
			if (info.getFile().hasAttribute(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_POSITION)) {
				int posIndex = info.getFile()
						.getAttributeInt(beamGroup.getPath() + "/" + BeamGroup1Grp.ATT_PREFERRED_POSITION);
				NCVariable posVar = info.getFile().getVariable(PlatformGrp.GROUP_NAME + "/" + PlatformGrp.POSITION_IDS);
				if (posVar != null) {
					String[] posIds = posVar.get_string(new long[] { posIndex }, new long[] { 1 });
					info.setPreferedPositionGroupId(posIds[0]);
				}
			}
		} catch (NCException e) {
			logger.error("XSFInfo loading: error parsing beamGroup prefered Position: " + e.getMessage(), e);
		}
	}
}
