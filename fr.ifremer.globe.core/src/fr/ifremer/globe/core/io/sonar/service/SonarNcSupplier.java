/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.sonar.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.core.io.sonar.griddeddata.SonarNcGriddedDataNavigation;
import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.io.sonar.info.SonarNcInfoReader;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfoSupplier;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;

/**
 * Service providing informations on XSF files
 */
@Component(name = "globe_drivers_sonar_nc_file_info_supplier", service = { IFileInfoSupplier.class,
		ISounderNcInfoSupplier.class, INavigationSupplier.class })
public class SonarNcSupplier extends BasicFileInfoSupplier<SonarNcInfo>
		implements IFileInfoSupplier<SonarNcInfo>, ISounderNcInfoSupplier<SonarNcInfo>, INavigationSupplier {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(SonarNcSupplier.class);

	/**
	 * Constructor
	 */
	public SonarNcSupplier() {
		super("nc", "Sonar Netcdf (*.nc)", ContentType.SONAR_NETCDF_4);
	}

	/** {@inheritDoc} */
	@Override
	public SonarNcInfo makeFileInfo(String filePath) {
		SonarNcInfo result = null;
		try {
			if (isSonarNcFile(filePath)) {
				result = new SonarNcInfo(filePath);
				var reader = new SonarNcInfoReader();
				IDataContainerFactory containerFactory = IDataContainerFactory.grab();
				IDataContainerOwner owner = IDataContainerOwner.generate(getClass().getName(), true);
				try (ISounderDataContainerToken dataContainerToken = containerFactory.book(result, owner)) {
					reader.readVersion(result);
					reader.readConstructor(result);
					reader.readSwathCount(result);
					reader.readSubGroupCount(result);
					reader.readFrequencies(result, dataContainerToken.getDataContainer());
					if (result.getCycleCount() > 0) {
						reader.readGeobox(result, dataContainerToken.getDataContainer());
						reader.readFirstLastDate(result, dataContainerToken.getDataContainer());
						reader.readWcDims(result);
						reader.readBeamGroupAttributes(result, dataContainerToken.getDataContainer());
					}
					reader.readRawAttributes(result);
					reader.readAntennaProperties(result, dataContainerToken.getDataContainer());
				}
			}
		} catch (Exception e) {
			// Not a Sonar Netcdf file
			logger.warn("Sonar Netcdf file {} not readable : {}", filePath, e.getMessage());
			result = null;
		}
		return result;
	}

	/**
	 * @return true if filePath point to a Sonar Netcdf file
	 */
	public boolean isSonarNcFile(String filePath) {
		try (NetcdfFile file = NetcdfFile.open(filePath, Mode.readonly)) {
			// A Sonar file is a Netcdf4 with a "sonar_convention_version" attribut
			// and no /Sonar/Beam_group1/Bathymetry group unlike a XSF file
			return file.hasAttribute(RootGrp.ATT_SONAR_CONVENTION_VERSION)
					&& file.getGroup(BathymetryLayers.GROUP) == null;
		} catch (NCException e) {
			// Ignored
		}
		return false;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		if (fileInfo instanceof SonarNcInfo) {
			var sonarNcInfo = (SonarNcInfo) fileInfo;
			if (sonarNcInfo.getBeamGroupCount() > 0)
				return Optional.of(sonarNcInfo);
			// gridded data
			if (sonarNcInfo.getGridGroupCount() > 0)
				return Optional.of(accessMode -> new SonarNcGriddedDataNavigation((SonarNcInfo) fileInfo));
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<SonarNcInfo> getSounderNcInfo(String filePath) {
		return getFileInfo(filePath);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<SonarNcInfo> getSounderNcInfo(IFileInfo fileInfo) {
		return Optional.ofNullable(fileInfo instanceof SonarNcInfo ? (SonarNcInfo) fileInfo : null);
	}

}