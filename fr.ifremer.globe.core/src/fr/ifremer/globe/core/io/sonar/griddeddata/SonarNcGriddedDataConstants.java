package fr.ifremer.globe.core.io.sonar.griddeddata;

/**
 * Constants for gridded data.
 */
public class SonarNcGriddedDataConstants {

	// dimensions
	public static final String PING_DIM = "ping_axis";
	public static final String RANGE_DIM = "range_axis";
	public static final String TX_BEAM_DIM = "tx_beam";
	
	// groups
	public static final String SONAR_GRP = "Sonar";

	// variables
	public static final String CELL_LATITUDE_VAR = "cell_latitude";
	public static final String CELL_LONGITUDE_VAR = "cell_longitude";
	public static final String CELL_DEPTH_VAR = "cell_depth";
	public static final String CELL_PING_TIME_VAR = "cell_ping_time";

}
