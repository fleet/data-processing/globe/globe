/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.sonar.info;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BathymetryGrp;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.BeamGroup1Grp;
import fr.ifremer.globe.core.io.sonar.griddeddata.SonarNcGriddedDataConstants;
import fr.ifremer.globe.core.io.xsf.info.XsfToPredefinedLayerMap;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.SensorInstallationParameters;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfLayerDeclarer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * SonarNcInfo: this class provides informations about Sonar Netcdf file.
 */
public class SonarNcInfo implements ISounderNcInfo {

	private static final Logger logger = LoggerFactory.getLogger(SonarNcInfo.class);

	/** file properties **/
	private NetcdfFile file;
	private final String filePath;
	/**
	 * How many times open() was called. Closing is only possible when this counter drops to zero. <br>
	 * Useful because the file can be opened and closed in multiple threads
	 */
	private final AtomicInteger openCount = new AtomicInteger(0);

	/** Sonar Netcdf info attributes */
	private String sonarConventionVersion;
	private GeoBox geobox;
	private String constructor = "Unknown";

	/** Number of swath. Value of the dimension /Sonar/Beam_group1/ping_time */
	private int swathCount;
	/** Number of /Sonar/Beam_group */
	private int beamGroupCount = 0;

	/** Default groupId in /Platform/Attitude */
	private String preferedAttitudeGroupId;
	/** Default groupId in /Platform/Position */
	private String preferedPositionGroupId;

	/** List of /Sonar/Grid_group */
	private List<String> gridGroupNames = new LinkedList<>();
	/**
	 * List of Frequencies in variables /Sonar/Grid_group_1/Frequency on /Sonar/Beam_group_XXX/Frequency. <br>
	 * May content Float.NaN when frequency is no available for a group
	 */
	private List<Float> frequencies = new LinkedList<>();
	/** Unit of frequencies */
	private String frequencyUnit = "";

	private Date firstSwathDate;
	private Date lastSwathDate;
	private ArrayList<AntennaInstallationParameters> antennas = new ArrayList<>();
	private long wcRxBeamCount;
	private Property<String> rawAttributes;

	/** Constructor */
	public SonarNcInfo(String filePath) {
		this.filePath = filePath;
	}

	/** {@inheritDoc} */
	@Override
	public synchronized void open(boolean readonly) throws GIOException {
		try {
			if (openCount.getAndIncrement() == 0) {
				setFile(NetcdfFile.open(filePath, readonly ? Mode.readonly : Mode.readwrite));
			}
		} catch (NCException e) {
			openCount.decrementAndGet();
			throw new GIOException("Error while opening NetcdfFile", e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public synchronized void close() {
		if (!IDataContainerFactory.grab().isBooked(this) && openCount.decrementAndGet() == 0) {
			file.close();
		}
	}

	@Override
	public void declareLayers(SounderDataContainer container) throws GException {
		NetcdfLayerDeclarer netcdfLayerDeclarer = new NetcdfLayerDeclarer();
		netcdfLayerDeclarer.declareLayers(getFile(), container, NetcdfGroup::getPath,
				XsfToPredefinedLayerMap::providePredefinedLayer);
	}

	/**********************************************************************************************************
	 * Getters / Setters
	 **********************************************************************************************************/

	public NetcdfFile getFile() {
		return file;
	}

	public void setFile(NetcdfFile file) {
		this.file = file;
	}

	@Override
	public String getPath() {
		return filePath;
	}

	public String getSonarConventionVersion() {
		return sonarConventionVersion;
	}

	protected void setSonarConventionVersion(String sonarConventionVersion) {
		this.sonarConventionVersion = sonarConventionVersion;
	}

	@Override
	public int getCycleCount() {
		return swathCount;
	}

	protected void setCycleCount(int swathCount) {
		this.swathCount = swathCount;
	}

	@Override
	public GeoBox getGeoBox() {
		return geobox;
	}

	public void setGeoBox(GeoBox geoBox) {
		geobox = geoBox;
	}

	@Override
	public String getEchoSounderModel() {
		return null;
	}

	@Override
	public short getEchoSounderIndex() {
		return -1;
	}

	@Override
	public Date getFirstPingDate() {
		return firstSwathDate;
	}

	protected void setFirstSwathDate(Date firstSwathDate) {
		this.firstSwathDate = firstSwathDate;
	}

	@Override
	public Date getLastPingDate() {
		return lastSwathDate;
	}

	protected void setLastSwathDate(Date lastSwathDate) {
		this.lastSwathDate = lastSwathDate;
	}

	@Override
	public long getWcRxBeamCount() {
		return wcRxBeamCount;
	}

	protected void setWcRxBeamCount(long wcRxBeamCount) {
		this.wcRxBeamCount = wcRxBeamCount;
	}

	@Override
	public int getTotalTxBeamCount() {
		try {
			return (int) file.getDimension(BeamGroup1Layers.GROUP + "/" + BeamGroup1Grp.TX_BEAM_DIM_NAME).getLength();
		} catch (NCException e) {
			logger.error("SonarNcInfo: getTotalTxBeamCount() method has failed: " + e.getMessage(), e);
			return 0;
		}
	}

	@Override
	public int getTotalRxBeamCount() {
		try {
			return (int) file.getDimension(BathymetryLayers.GROUP + "/" + BathymetryGrp.DETECTION_DIM_NAME).getLength();
		} catch (NCException e) {
			logger.error("SonarNcInfo: getTotalRxBeamCount() method has failed: " + e.getMessage(), e);
			return 0;
		}
	}

	/**********************************************************************************************************
	 * ANTENNAS
	 **********************************************************************************************************/

	@Override
	public AntennaInstallationParameters getTxParameters() {
		List<AntennaInstallationParameters> txAntennas = getTxParametersList();
		return txAntennas.isEmpty() ? null : txAntennas.get(0);
	}

	protected List<AntennaInstallationParameters> getTxParametersList() {
		return antennas.stream().filter(ant -> (ant.type == AntennaInstallationParameters.Type.TX
				|| ant.type == AntennaInstallationParameters.Type.TXRX)).collect(Collectors.toList());
	}

	@Override
	public List<AntennaInstallationParameters> getRxParameters() {
		return antennas.stream().filter(ant -> (ant.type == AntennaInstallationParameters.Type.RX
				|| ant.type == AntennaInstallationParameters.Type.TXRX)).collect(Collectors.toList());
	}

	@Override
	public List<AntennaInstallationParameters> getAntennaParameters() {
		return antennas;
	}

	/**********************************************************************************************************
	 *
	 **********************************************************************************************************/

	/**
	 * Properties getter (provides data to display in the properties view)
	 */
	@Override
	public List<Property<?>> getProperties() {

		List<Property<?>> properties = new ArrayList<>();

		// FILE PROPERTIES
		properties.add(Property.build("Sonar convention version", getSonarConventionVersion()));
		if (getCycleCount() > 0) {
			properties.add(Property.build("Nb of swath", getCycleCount()));
		}
		if (getBeamGroupCount() > 0) {
			properties.add(Property.build("Nb of beam group", getBeamGroupCount()));
		}
		if (getGridGroupCount() > 0) {
			properties.add(Property.build("Nb of grid group", getGridGroupCount()));
		}

		if (getGeoBox() != null) {
			properties.addAll(Property.build(getGeoBox()));
		}

		// START / END DATES
		if (getFirstPingDate() != null) {
			properties.add(Property.build("Start Date", DateUtils.getStringDate(getFirstPingDate())));
		}
		if (getLastPingDate() != null) {
			properties.add(Property.build("End Date", DateUtils.getStringDate(getLastPingDate())));
		}

		if (getWcRxBeamCount() > 0) {
			Property<String> WC = Property.build("Water column", "");
			properties.add(WC);
			WC.add(Property.build("WC rxBeam count", Long.toString(getWcRxBeamCount())));
		} else {
			Property<String> WC = Property.build("Water column", "No Data");
			properties.add(WC);
		}

		if (!frequencies.isEmpty()) {
			Property<String> frequenciesProp = Property.build("Frequencies", "");
			frequencies.forEach(
					freq -> frequenciesProp.add(Property.build("", String.valueOf(freq) + " " + frequencyUnit)));
			properties.add(frequenciesProp);
		}

		// INSTALLATION ANTENNA PROPERTIES
		if (!antennas.isEmpty()) {
			Property<String> installation = Property.build("Installation Lever Arm", "");
			properties.add(installation);
			antennas.forEach(antenna -> installation.add(Property.build(antenna)));
		}
		// Sonar file attributes
		if (rawAttributes != null) {
			properties.add(rawAttributes);
		}

		return properties;
	}

	@Override
	public ContentType getContentType() {
		return ContentType.SONAR_NETCDF_4;
	}

	@Override
	public String getConstructor() {
		return constructor;
	}

	/** Compute the name of the grid group */
	public String inferGridGroupName(int gridGroupIndex) {
		return "/" + SonarNcGriddedDataConstants.SONAR_GRP + "/" + getGridGroupNames().get(gridGroupIndex);
	}

	protected void setConstructor(String constructor) {
		this.constructor = constructor;
	}

	protected void setRawAttributes(Property<String> xsfRawAttributes) {
		rawAttributes = xsfRawAttributes;
	}

	/**
	 * @return the {@link #beamGroupCount}
	 */
	@Override
	public int getBeamGroupCount() {
		return beamGroupCount;
	}

	/**
	 * @param beamGroupCount the {@link #beamGroupCount} to set
	 */
	public void setBeamGroupCount(int beamGroupCount) {
		this.beamGroupCount = beamGroupCount;
	}

	@Override
	public Optional<String> getPreferedAttitudeGroupId() {
		return Optional.ofNullable(preferedAttitudeGroupId);
	}

	public void setPreferedAttitudeGroupId(String groupId) {
		preferedAttitudeGroupId = groupId;
	}

	@Override
	public Optional<SensorInstallationParameters> getPreferedAttitudeInstallationParameters() {
		// AttitudeInstallationParameters uesd only for bathymetry bias correction
		return Optional.empty();
	}

	@Override
	public Optional<String> getPreferedPositionGroupId() {
		return Optional.ofNullable(preferedPositionGroupId);
	}

	public void setPreferedPositionGroupId(String groupId) {
		preferedPositionGroupId = groupId;
	}

	/**
	 * @return the {@link #gridGroupCount}
	 */
	public int getGridGroupCount() {
		return gridGroupNames.size();
	}

	/**
	 * @return the {@link #gridGroupNames}
	 */
	@Override
	public List<String> getGridGroupNames() {
		return gridGroupNames;
	}

	/**
	 * @return the {@link #frequencies}
	 */
	public List<Float> getFrequencies() {
		return frequencies;
	}

	/**
	 * @return the {@link #frequencyUnit}
	 */
	public String getFrequencyUnit() {
		return frequencyUnit;
	}

	/**
	 * @param frequencyUnit the {@link #frequencyUnit} to set
	 */
	public void setFrequencyUnit(String frequencyUnit) {
		this.frequencyUnit = frequencyUnit;
	}

}
