/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io;

/**
 * Constant class of IFileInfoSupplier priority.<br>
 * This constants are used by IFileService to sort IFileInfoSupplier  
 */
public abstract class FileInfoSupplierPriority {

	/**
	 * Key used in a IFileInfoSupplier definition to identify the supplier’s priority. <br>
	 * By default, priority is 1. The lowest priority is Integer.MAX_VALUE
	 */
	public static final String PROPERTY_PRIORITY = "FileInfoSupplierPriority.PROPERTY_PRIORITY";

	/**
	 * Default priority of all supplier<br>
	 * Use when {@code property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer="...} } is not defined on
	 * thes supplier
	 */
	public static final int DEFAULT_PRIORITY = 1;

	/**
	 * Priority of polygon supplier<br>
	 * This supplier has priority over other vector file supplier (Shp, Kml...)
	 */
	public static final int EXTENDED_POLYGON_PRIORITY = 0;

	/**
	 * Priority of KML_XML supplier<br>
	 * Priority is 0 to be first, and avoid a loading from DTM/XSF drivers...
	 */
	public static final int G3D_PRIORITY = 0;

	/**
	 * Priority of SHP supplier <br>
	 * This supplier has less priority over other vector file supplier (Polygon...)
	 */
	public static final int SHP_PRIORITY = 500;

	/**
	 * Priority of KML_XML supplier <br>
	 * This supplier has less priority over other vector file supplier (Polygon...)
	 */
	public static final int KML_XML_PRIORITY = 500;

	/**
	 * Priority of CSV supplier in extended bundle <br>
	 * The supplier kicks in when all the others CSV have failed
	 */
	public static final int EXTENDED_CSV_PRIORITY = 1000;

	/**
	 * Priority of GMT supplier<br>
	 * Priority is less than DTM (DTM know how to identify its managed files)...
	 */
	public static final int GMT_PRIORITY = 5000;

	/**
	 * Priority of Gdal Raster supplier in extended bundle <br>
	 * Ultimate raster supplier, just before GDAL_VECTOR_PRIORITY
	 */
	public static final int EXTENDED_GDAL_RASTER_PRIORITY = 10000;

	/**
	 * Priority of Gdal Raster supplier in extended bundle <br>
	 * Ultimate raster supplier
	 */
	public static final int GDAL_VECTOR_PRIORITY = EXTENDED_GDAL_RASTER_PRIORITY + 1;

	/**
	 * Priority of USBL supplier <br>
	 * Priority is 0 to be first, before generic CSV.
	 */
	public static final int USBL_PRIORITY = 0;

	/**
	 * Priority of USBL Extended supplier <br>
	 * If not loaded from the session, this supplier will show a dialog window to get some parameters from user.
	 */
	public static final int USBL_EXTENDED_PRIORITY = -1;

	/**
	 * Constructor
	 */
	private FileInfoSupplierPriority() {
	}

}
