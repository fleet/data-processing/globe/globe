/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.pointcloud;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.IntConsumer;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.io.csv.ICsvFileInfoSupplier;
import fr.ifremer.globe.core.io.csv.parsing.CsvTimestampedBean;
import fr.ifremer.globe.core.io.pointcloud.csv.PointCloudCsv;
import fr.ifremer.globe.core.io.pointcloud.csv.PointCloudMappingStrategy;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.IPointCloudFieldInfoService;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.utils.csv.CsvUtils;

/**
 * Loader of Markers file info.
 */
@Component(name = "globe_drivers_pointcloud_csv_file_info_supplier", service = { ICsvFileInfoSupplier.class,
		IPointCloudFieldInfoService.class })
public class PointCloudFileInfoSupplier implements ICsvFileInfoSupplier, IPointCloudFieldInfoService {

	/** Logger */
	static final Logger logger = LoggerFactory.getLogger(PointCloudFileInfoSupplier.class);

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return ContentType.POINT_CLOUD_CSV;
	}

	/**
	 * A PointCloud file must have at least the columns Longitude, Latitude, Depth and a value
	 */
	@Override
	public Optional<FileInfoSettings> evaluatesSettings(String filePath, List<String> firstLines) {
		FileInfoSettings result = new FileInfoSettings(getContentType());
		result.setElevationScaleFactor(1d);
		result.setRowCountToSkip(1);
		result.setLocale(Locale.US);

		String line = firstLines.get(0);
		var sep = CsvUtils.guessDelimiter(line);
		PointCloudCsv.COLUMNS.forEach(column -> result.set(column, -2));
		if (sep.isPresent()) {
			char delimiter = sep.get();
			result.setDelimiter(delimiter);

			// Search index of well-known column
			var columns = CsvUtils.parseLine(firstLines.get(0), delimiter);
			for (String column : PointCloudCsv.COLUMNS) {
				int colIndex = CsvUtils.guessColumnIndex(columns, column);
				if (colIndex >= 0)
					result.set(column, colIndex);
			}

			result.allowMultipleValues(true);
		}

		// Add optional columns (Only longitude and latitude are mandatory)
		result.addOptionalColumn(PointCloudCsv.COL_ELEV);
		CsvTimestampedBean.getAllHeaders().forEach(result::addNonNumericColumn);
		CsvTimestampedBean.getAllHeaders().forEach(result::addOptionalColumn);
		result.addOptionalColumn(PointCloudCsv.COL_LABEL);
		result.addNonNumericColumn(PointCloudCsv.COL_LABEL);

		return Optional.of(result);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ICsvFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		PointCloudFileInfo result = null;
		try {
			// OpenCsv strategy : mapping each attributes to a column index
			var strategy = new PointCloudMappingStrategy(settings);
			var errors = new ArrayList<CsvException>();

			List<? extends PointCloudCsv> points = //
					new CsvToBeanBuilder<PointCloudCsv>(new FileReader(new File(filePath)))//
							.withMappingStrategy(strategy)//
							.withSeparator(settings.getDelimiter())//
							.withExceptionHandler(e -> {
								logger.error(
										"Error while parsing CSV at line " + e.getLineNumber() + " : " + e.getMessage(),
										e);
								errors.add(e);
								return e;
							}) //
							.build()//
							.parse();

			var fieldInfos = PointCloudFieldInfo.gatherFieldInfos(points, strategy::getFieldName);
			result = new PointCloudFileInfo(filePath, points, fieldInfos, errors);
		} catch (ArrayIndexOutOfBoundsException ex) {
			logger.warn("Unable to parse the csv file {} : bad column definition", filePath);
		} catch (FileNotFoundException ex) {
			logger.warn("Unable to parse the csv file " + filePath + ex.getMessage());
		} catch (RuntimeException ex) {
			logger.warn("Unable to parse the csv file " + filePath, ex);
		}
		return Optional.ofNullable(result);
	}

	/**
	 * Call the setter with this index of searchedColumn in the columns list
	 */
	protected void setProperty(List<String> columns, String searchedColumn, IntConsumer setter) {
		for (int i = 0; i < columns.size(); i++)
			if (columns.get(i).startsWith(searchedColumn)) {
				setter.accept(i);
				return;
			}
	}

	/** {@inheritDoc} */
	@Override
	public List<PointCloudFieldInfo> supplyPointCloudFieldInfos(IFileInfo fileInfo) {
		return fileInfo instanceof PointCloudFileInfo ? ((PointCloudFileInfo) fileInfo).getFieldInfos() : List.of();
	}

}