/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.pointcloud.csv;

import java.time.temporal.Temporal;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.collections.primitives.DoubleList;

import fr.ifremer.globe.core.io.csv.parsing.CsvTimestampedBean;
import fr.ifremer.globe.core.model.file.pointcloud.IPoint;

/**
 * {@link IPoint} built from CSV file.
 */
public class PointCloudCsv extends CsvTimestampedBean implements IPoint {

	// Column header in the file
	public static final String COL_LAT = "latitude";
	public static final String COL_LON = "longitude";
	public static final String COL_ELEV = "elevation";
	public static final String COL_LABEL = "label";
	public static final List<String> COLUMNS = List.of(COL_LAT, COL_LON, COL_ELEV, COL_LABEL);

	private double latitude;
	private double longitude;
	private double elevation = 0d;

	private String label;

	/** Values */
	private DoubleList values = new ArrayDoubleList();

	@Override
	public double getLatitude() {
		return latitude;
	}

	@Override
	public double getLongitude() {
		return longitude;
	}

	@Override
	public double getElevation() {
		return elevation;
	}

	@Override
	public Optional<Temporal> getTime() {
		return getTemporal();
	}

	/** Bean representing a PointCloudCsv with positive depth */
	public static class PointCloudCsvWithPositiveElevation extends PointCloudCsv {

		/** {@inheritDoc} */
		@Override
		public double getElevation() {
			return -super.getElevation();
		}
	}

	/** {@inheritDoc} */
	@Override
	public double getValue(int index) {
		return index >= 0 && values.size() > index ? values.get(index) : 0d;
	}

	/** {@inheritDoc} */
	@Override
	public int getValueCount() {
		return values.size();
	}

	/** Set a value at the specified index */
	public void setValue(int index, double value) {
		// Ensure list capacity
		while (values.size() <= index)
			values.add(Double.NaN);

		values.set(index, value);
	}

	/**
	 * @return the {@link #label}
	 */
	@Override
	public Optional<String> getLabel() {
		return Optional.ofNullable(label != null && !label.isEmpty() ? label : null);
	}
}
