package fr.ifremer.globe.core.io.pointcloud;

import java.util.Collections;
import java.util.List;

import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.IPoint;
import fr.ifremer.globe.core.model.file.pointcloud.IPointCloudFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;

public class PointCloudFileInfo extends BasicFileInfo implements IPointCloudFileInfo, ICsvFileInfo {

	/** Point list */
	private final List<? extends IPoint> points;

	/** List of field */
	private final List<PointCloudFieldInfo> fieldInfos;

	/** Errors while parsing CSV file **/
	private final List<CsvException> parsingErrors;

	/**
	 * Constructor. Useful when several fields are present by point
	 */
	public PointCloudFileInfo(String csvFilePath, List<? extends IPoint> points, List<PointCloudFieldInfo> fieldInfos) {
		this(csvFilePath, points, fieldInfos, Collections.emptyList());
	}

	/**
	 * Constructor. Useful when several fields are present by point
	 */
	public PointCloudFileInfo(String csvFilePath, List<? extends IPoint> points, List<PointCloudFieldInfo> fieldInfos,
			List<CsvException> parsingErrors) {
		super(csvFilePath, ContentType.POINT_CLOUD_CSV);
		this.points = points;
		this.fieldInfos = fieldInfos;
		this.parsingErrors = parsingErrors;
		geoBox = computeGeobox(points);
	}

	/** {@inheritDoc} */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = super.getProperties();
		properties.add(Property.build("Number of points", points.size()));
		if (fieldInfos.size() == 1) {
			properties.add(Property.build("Min value", fieldInfos.get(0).minValue()));
			properties.add(Property.build("Max value", fieldInfos.get(0).maxValue()));
		} else if (fieldInfos.size() > 1) {
			var values = Property.build("Values", "");
			properties.add(values);
			for (var fieldInfo : fieldInfos) {
				var minmax = Property.build(fieldInfo.name(), "");
				values.add(minmax);
				minmax.add(Property.build("Min value", fieldInfo.minValue()));
				minmax.add(Property.build("Max value", fieldInfo.maxValue()));
			}
		} else
			properties.add(Property.build("Values", "No column of values selected"));

		return properties;
	}

	/** Browse all points and compute the encompassing GeoBox */
	private GeoBox computeGeobox(List<? extends IPoint> points) {
		double north = Double.NaN;
		double south = Double.NaN;
		double east = Double.NaN;
		double west = Double.NaN;

		for (IPoint point : points) {
			north = Double.isNaN(north) ? point.getLatitude() : Math.max(north, point.getLatitude());
			south = Double.isNaN(south) ? point.getLatitude() : Math.min(south, point.getLatitude());
			east = Double.isNaN(east) ? point.getLongitude() : Math.max(east, point.getLongitude());
			west = Double.isNaN(west) ? point.getLongitude() : Math.min(west, point.getLongitude());

		}
		return new GeoBox(north, south, east, west);
	}

	/**
	 * @return the {@link #points}
	 */
	@Override
	public List<? extends IPoint> getPoints() {
		return points;
	}

	/** {@inheritDoc} */
	@Override
	public List<PointCloudFieldInfo> getFieldInfos() {
		return fieldInfos;
	}

	@Override
	public List<CsvException> getParsingErrors() {
		return parsingErrors;
	}

}
