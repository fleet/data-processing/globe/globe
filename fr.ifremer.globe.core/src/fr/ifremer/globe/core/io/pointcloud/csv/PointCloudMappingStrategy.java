/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.pointcloud.csv;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Map;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.opencsv.exceptions.CsvValidationException;

import fr.ifremer.globe.core.io.csv.parsing.CsvTimestampedMappingStrategy;
import fr.ifremer.globe.core.io.pointcloud.csv.PointCloudCsv.PointCloudCsvWithPositiveElevation;
import fr.ifremer.globe.core.model.file.FileInfoSettings;

/**
 * Strategy of mapping where column's position is map to PointCloudCsv's field
 */
public class PointCloudMappingStrategy extends CsvTimestampedMappingStrategy<PointCloudCsv> {
	/** Logger */
	static final Logger logger = LoggerFactory.getLogger(PointCloudMappingStrategy.class);

	/** Mapping column index to value index */
	private final IntList columnToValueIndex = new ArrayIntList();
	/** How to parse values */
	private final NumberFormat valueFormat;
	/** How to parse values */
	private final int numberOfLinesToSkip;

	/** Headers of the file. Empty when numberOfLinesToSkip == 0 */
	private String[] headers = {};

	/**
	 * Constructor
	 */
	public PointCloudMappingStrategy(FileInfoSettings settings) {
		super(PointCloudCsv.COLUMNS, settings);
		valueFormat = NumberFormat.getInstance(settings.getLocale());
		numberOfLinesToSkip = settings.getRowCountToSkip();
		initValueIndex(settings);

		setType(settings.getElevationScaleFactor() < 0 ? PointCloudCsvWithPositiveElevation.class
				: PointCloudCsv.class);

	}

	/** Redefine super.setFieldValue to capture values */
	@Override
	protected void setFieldValue(Map<Class<?>, Object> beanTree, String stringValue, int column)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, CsvConstraintViolationException,
			CsvValidationException {

		// Is it a value ? (ie PointCloudCsv.values attribute)
		if (column < columnToValueIndex.size()) {
			int valueIndex = columnToValueIndex.get(column);
			if (valueIndex >= 0) {
				lock.lock(); // block until condition holds
				PointCloudCsv pointCloudCsv = (PointCloudCsv) beanTree.get(getType());
				try {
					double value = valueFormat.parse(stringValue).doubleValue();
					pointCloudCsv.setValue(valueIndex, value);
				} catch (Exception e) {
					// Bad value
					pointCloudCsv.setValue(valueIndex, Double.NaN);
				} finally {
					lock.unlock();
				}
				return; // Job done
			}
		}

		super.setFieldValue(beanTree, stringValue, column);
	}

	/**
	 * Initialize index of column containing values
	 */
	private void initValueIndex(FileInfoSettings settings) {
		int valueIndex = 0;
		var valueIndexes = settings.getValueIndexes();

		// Old session format (before Globe 1.20.8)
		// Only one value was possible. Index of column is hold by key "value"
		if (valueIndexes.isEmpty() && settings.containsKey("value"))
			valueIndexes.add(settings.getInt("value"));

		for (var iter = valueIndexes.iterator(); iter.hasNext();) {
			int column = iter.next();
			// Ensure list capacity
			while (columnToValueIndex.size() <= column)
				columnToValueIndex.add(-1);

			if (column >= 0) {
				columnToValueIndex.set(column, valueIndex);
				valueIndex++;
			}
		}
	}

	/**
	 * There is no header per se for this mapping strategy, but this method checks the first line to determine how many
	 * fields are present and adjusts its field map accordingly.
	 */
	@Override
	public void captureHeader(CSVReader reader) throws IOException {
		super.captureHeader(reader);
		// Consume the first lines to grab column headers
		if (numberOfLinesToSkip > 0) {
			headers = reader.peek();
			reader.skip(numberOfLinesToSkip);
		}
	}

	/** Return the name of a field. Match the header of the linked column */
	public String getFieldName(int fieldIndex) {
		// search the column of the expected field
		int columnIndex = columnToValueIndex.indexOf(fieldIndex);
		if (columnIndex >= 0 && columnIndex < headers.length) {
			String header = headers[columnIndex];
			if (header != null && !header.isBlank())
				return header.strip();
		}
		return "value " + fieldIndex;
	}
}
