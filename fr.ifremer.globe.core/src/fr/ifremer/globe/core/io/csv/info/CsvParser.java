/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv.info;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.ParsePosition;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;

/**
 * Parser of CSV file to extract GeoBox and min/max elevations.
 */
public class CsvParser {

	/** Extract values from CSV */
	public boolean parse(CsvFileInfo fileInfo, CsvParserSettings settings) {
		boolean result = false;
		long fileSize = fileInfo.toFile().length();
		GeoBoxBuilder geoBoxBuilder = new GeoBoxBuilder();
		double[] elevations = { Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY };
		// Small file ?
		if (fileSize < 100L * FileUtils.ONE_MB) {
			extractData(fileInfo, geoBoxBuilder, elevations, settings);
		} else {
			estimateData(fileInfo, geoBoxBuilder, elevations, settings, fileSize / 100);
			fileInfo.setEstimated(true);
		}
		var geobox = geoBoxBuilder.build();
		result = Double.isFinite(geobox.getTop()) && Double.isFinite(elevations[0]);
		if (result) {
			fileInfo.setGeoBox(geobox);
			fileInfo.setElevations(new DoubleRange(elevations[0], elevations[1]));
		}
		return result;
	}

	/** Read some lines in the file to estimate the geobox */
	protected void estimateData(BasicFileInfo fileInfo, GeoBoxBuilder geoBoxBuilder, double[] elevations,
			CsvParserSettings settings, long step) {
		ByteBuffer byteBuffer = ByteBuffer.allocate((int) FileUtils.ONE_KB);
		try (SeekableByteChannel byteChannel = Files.newByteChannel(fileInfo.toPath())) {
			while (byteChannel.read(byteBuffer) == byteBuffer.capacity()) {
				byteBuffer.rewind();

				String[] lines = StandardCharsets.UTF_8.decode(byteBuffer).toString().split("\n");
				Arrays.stream(lines, 1, lines.length - 1) //
						.map(s -> settings.delimiter() == '…' ? s.replaceAll("\\s+", "…") : s) // Manage whitespaces
						.forEach(line -> extractData(line, geoBoxBuilder, elevations, settings));

				// Jump to next line
				byteChannel.position(byteChannel.position() + step);
				byteBuffer.rewind();
			}
		} catch (IOException e) {
			// Something went wrong
		}
	}

	/** Read all lines in the file to computi the geobox */
	protected void extractData(BasicFileInfo fileInfo, GeoBoxBuilder geoBoxBuilder, double[] elevations,
			CsvParserSettings settings) {
		try (BufferedReader bufferedReader = Files.newBufferedReader(fileInfo.toPath())) {
			String line = bufferedReader.readLine();
			int lineCount = 0;
			while (line != null) {
				lineCount++;
				if (lineCount > settings.lineToSkip()) {
					if (settings.delimiter() == '…') {
						line = line.replaceAll("\\s+", "…"); // Manage whitespaces
					}
					extractData(line, geoBoxBuilder, elevations, settings);
				}
				line = bufferedReader.readLine();
			}
		} catch (IOException e) {
			// Something went wrong
		}
	}

	protected void extractData(String line, GeoBoxBuilder geoBoxBuilder, double[] elevations, CsvParserSettings settings) {
		ParsePosition parsePosition = new ParsePosition(0);
		initPosition(parsePosition, line, settings, settings.longitudeIndex());
		Number longitude = settings.numberFormat().parse(line, parsePosition);
		initPosition(parsePosition, line, settings, settings.latitudeIndex());
		Number latitude = settings.numberFormat().parse(line, parsePosition);
		initPosition(parsePosition, line, settings, settings.elevationIndex());
		Number elevation = settings.numberFormat().parse(line, parsePosition);
		if (longitude != null && latitude != null && elevation != null) {
			geoBoxBuilder.addPoint(longitude.doubleValue(), latitude.doubleValue());
			double depth = elevation.doubleValue() * settings.elevationScaleFactor();
			elevations[0] = Math.min(elevations[0], depth);
			elevations[1] = Math.max(elevations[1], depth);
		}
	}

	private void initPosition(ParsePosition parsePosition, String line, CsvParserSettings settings, int colIndex) {
		parsePosition.setIndex(0);
		for (int i = 0; i < colIndex; i++) {
			parsePosition.setIndex(line.indexOf(settings.delimiter(), parsePosition.getIndex()) + 1);
		}
	}

}