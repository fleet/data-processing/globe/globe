/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;

/**
 * Kind of {@link ICsvFileInfoSupplier} managing well known csv format. <br>
 * Brings a default implementation to all the classes managing csv files whose format is fixed.
 */
public interface IWellKnownCsvFileInfoSupplier {

	/** @return the managed file type */
	ContentType getContentType();

	/**
	 * In case of a well known CSV, this supplier opens the file and tries to parse it to create an IFileInfo<br>
	 * This method is called only with files with the expected extensions ({@link #getExtensions()})
	 *
	 * @return the IFileInfo corresponding to the specified file
	 */
	<I extends IFileInfo> Optional<I> getFileInfo(String filePath);

	/**
	 * @return the settings to use for parsing the file or {@code Optional.empty()} none
	 */
	Optional<FileInfoSettings> evaluateSettings(String filePath);
}
