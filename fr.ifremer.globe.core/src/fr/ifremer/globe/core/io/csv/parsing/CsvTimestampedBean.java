/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv.parsing;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Base bean for all CSV pojo with date/time column
 */
public abstract class CsvTimestampedBean {

	/** Logger */
	static final Logger logger = LoggerFactory.getLogger(CsvTimestampedBean.class);

	/**
	 * List all possible Date Time format.
	 */
	public static final String DATE_TIME_HEADER = "date time";
	private static final List<DateTimeFormatter> COLUMN_TO_DATE_TIME_FORMATTER = List.of(//
			DateTimeFormatter.ISO_DATE_TIME.withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ISO_INSTANT.withZone(ZoneOffset.UTC), //
			DateUtils.LOCAL_DATE_TIME_MILLIS.withZone(ZoneOffset.UTC), //
			DateUtils.LOCAL_DATE_TIME_MILLIS_Z.withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_1.toPattern()).withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_2.toPattern()).withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_3.toPattern()).withZone(ZoneOffset.UTC), //
			DateUtils.DATE_TIME_FR.withZone(ZoneOffset.UTC), //
			DateUtils.DATE_TIME_US.withZone(ZoneOffset.UTC)//
	);

	/**
	 * List all possible Date format.
	 */
	public static final String DATE_HEADER = "date";
	private static final List<DateTimeFormatter> COLUMN_TO_DATE_FORMATTER = List.of(//
			DateTimeFormatter.ISO_DATE.withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ISO_LOCAL_DATE.withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ofPattern(DateUtils.DATE_PATTERN).withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ofPattern(DateUtils.FR_DATE_FORMAT).withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ofPattern(DateUtils.US_DATE_FORMAT).withZone(ZoneOffset.UTC)//
	);

	/**
	 * List all possible Time format.
	 */
	public static final String TIME_HEADER = "time";
	private static final List<DateTimeFormatter> COLUMN_TO_TIME_FORMATTER = List.of(//
			DateUtils.LOCAL_TIME.withZone(ZoneOffset.UTC), //
			DateUtils.LOCAL_TIME_MILLIS.withZone(ZoneOffset.UTC), //
			DateUtils.LOCAL_TIME_MILLIS_Z.withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ISO_TIME.withZone(ZoneOffset.UTC), //
			DateTimeFormatter.ISO_LOCAL_TIME.withZone(ZoneOffset.UTC), //
			DateUtils.TIME.withZone(ZoneOffset.UTC)//
	);

	/** Return all possible headers for date/time values */
	public static List<String> getAllHeaders() {
		List<String> result = new ArrayList<>();
		result.add(DATE_TIME_HEADER);
		result.add(DATE_HEADER);
		result.add(TIME_HEADER);
		return result;
	}

	/**
	 * Attribute to receive date time parsed with one of COLUMN_TO_DATE_TIME_FORMATTER
	 */
	private LocalDateTime dateTime;
	/**
	 * Attribute to receive date only parsed with one of COLUMN_TO_DATE_FORMATTER
	 */
	private LocalDate date;
	/**
	 * Attribute to receive time only parsed with one of COLUMN_TO_TIME_FORMATTER
	 */
	private LocalTime time;

	/** Infer the Temporal using the received values */
	public Optional<Temporal> getTemporal() {
		if (dateTime != null)
			return Optional.of(dateTime);

		// No timestamp for this bean
		if (date == null && time == null)
			return Optional.empty();

		// Date + Time
		if (date != null && time != null) {
			dateTime = date.atTime(time);
			return Optional.of(dateTime);
		}

		// Date only
		if (date != null)
			return Optional.of(date);

		// Time only
		return Optional.of(time);
	}

	/** Search a suitable DateTimeFormatter for the specified value */
	public Optional<DateTimeFormatter> inferDateTimeFormatter(String header, String stringValue)
			throws DateTimeParseException {
		if (DATE_TIME_HEADER.equals(header))
			return inferDateTimeFormatter(stringValue, COLUMN_TO_DATE_TIME_FORMATTER);
		if (DATE_HEADER.equals(header))
			return inferDateTimeFormatter(stringValue, COLUMN_TO_DATE_FORMATTER);
		if (TIME_HEADER.equals(header))
			return inferDateTimeFormatter(stringValue, COLUMN_TO_TIME_FORMATTER);
		return Optional.empty();
	}

	/**
	 * Search a suitable DateTimeFormatter among a list to parse the specified value The formatter is expected by
	 * parseInstant for all instance of CsvTimestampedBean generated for the CSV
	 */
	private Optional<DateTimeFormatter> inferDateTimeFormatter(String stringValue,
			List<DateTimeFormatter> columnToDateTimeFormatter) {
		for (DateTimeFormatter dateTimeFormatter : columnToDateTimeFormatter) {
			try {
				dateTimeFormatter.parse(stringValue);
				return Optional.of(dateTimeFormatter);
			} catch (Exception e) {
				// Wrong formatter
			}
		}
		return Optional.empty();
	}

	/** Parse the specified value using the formatter returned by inferDateTimeFormatter */
	public void parseInstant(String header, String stringValue, DateTimeFormatter formatter)
			throws DateTimeParseException {
		if (DATE_TIME_HEADER.equals(header))
			dateTime = formatter.parse(stringValue, LocalDateTime::from);
		else if (DATE_HEADER.equals(header))
			date = formatter.parse(stringValue, LocalDate::from);
		else if (TIME_HEADER.equals(header))
			time = formatter.parse(stringValue, LocalTime::from);
	}

}
