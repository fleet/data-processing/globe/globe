package fr.ifremer.globe.core.io.csv;

import java.util.Map;

import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

public class CsvPreferences extends PreferenceComposite {

	public enum DecimalPointList {
		DOT(0, "Dot \".\""), COMMA(1, "Comma \",\"");

		int value;
		String name;

		DecimalPointList(int value, String name) {
			this.value = value;
			this.name = name;
		}

		public int getValue() {
			return value;
		}

		public static DecimalPointList get(String name) {
			DecimalPointList decimalSeparatorChoice = DOT;
			for (DecimalPointList e : DecimalPointList.values()) {
				if (name.equalsIgnoreCase(e.name)) {
					decimalSeparatorChoice = DecimalPointList.valueOf(e.name());
					break;
				}
			}
			return decimalSeparatorChoice;
		}
	}

	private final static String DEFAULT_DELIMITER = ",";
	private final static boolean DEFAULT_DEPTH_POSITIVE_BELOW_SURFACE = true;
	private final static Integer DEFAULT_RAW_COUNT_TO_SKIP = 0;

	private final static Integer DEFAULT_LATITUDE_COLUMN_INDEX = 1;
	private final static Integer DEFAULT_LONGITUDE_COLUMN_INDEX = 2;
	private final static Integer DEFAULT_DEPTH_COLUMN_INDEX = 3;

	private StringPreferenceAttribute delimiter;
	private BooleanPreferenceAttribute isDepthPositiveBelowSurface;
	private IntegerPreferenceAttribute rowCountToSkip;
	private IntegerPreferenceAttribute latitudeColumnIndex;
	private IntegerPreferenceAttribute longitudeColumnIndex;
	private IntegerPreferenceAttribute depthColumnIndex;
	private EnumPreferenceAttribute decimalPoint;

	public CsvPreferences(PreferenceComposite superNode, String nodeName) {
		super(superNode, nodeName);

		delimiter = new StringPreferenceAttribute("1-delimiter", "Delimiter", DEFAULT_DELIMITER);
		this.declareAttribute(delimiter);

		isDepthPositiveBelowSurface = new BooleanPreferenceAttribute("2-isDepthPositiveBelowSurface",
				"Is depth positive below surface?", DEFAULT_DEPTH_POSITIVE_BELOW_SURFACE);
		this.declareAttribute(isDepthPositiveBelowSurface);

		rowCountToSkip = new IntegerPreferenceAttribute("3-rowCountToSkip", "Row count to skip",
				DEFAULT_RAW_COUNT_TO_SKIP);
		this.declareAttribute(rowCountToSkip);

		latitudeColumnIndex = new IntegerPreferenceAttribute("4-latitude", "Latitude", DEFAULT_LATITUDE_COLUMN_INDEX);
		this.declareAttribute(latitudeColumnIndex);

		longitudeColumnIndex = new IntegerPreferenceAttribute("5-longitude", "Longitude",
				DEFAULT_LONGITUDE_COLUMN_INDEX);
		this.declareAttribute(longitudeColumnIndex);

		depthColumnIndex = new IntegerPreferenceAttribute("6-depth", "Depth", DEFAULT_DEPTH_COLUMN_INDEX);
		this.declareAttribute(depthColumnIndex);

		decimalPoint = new EnumPreferenceAttribute("7- DecimalPoint", "Decimal Point", DecimalPointList.DOT);
		this.declareAttribute(decimalPoint);

		load();

	}

	public String getDelimiter() {
		return delimiter.getValue();
	}

	public void setDelimiter(String s) {
		delimiter.setValue(s, false);
		save();
	}

	public Boolean getIsDepthPositiveBelowSurface() {
		return isDepthPositiveBelowSurface.getValue();
	}

	public void setIsDepthPositiveBelowSurface(boolean s) {
		isDepthPositiveBelowSurface.setValue(s, false);
		save();
	}

	public int getRowCountToSkip() {
		return rowCountToSkip.getValue();
	}

	public void setRowCountToSkip(int s) {
		rowCountToSkip.setValue(s, false);
		save();
	}

	public int getLatitudeColumnIndex() {
		return latitudeColumnIndex.getValue();
	}

	public void setLatitudeColumnIndex(int s) {
		latitudeColumnIndex.setValue(s, false);
		save();
	}

	public int getLongitudeColumnIndex() {
		return longitudeColumnIndex.getValue();
	}

	public void setLongitudeColumnIndex(int s) {
		longitudeColumnIndex.setValue(s, false);
		save();
	}

	public int getDepthColumnIndex() {
		return depthColumnIndex.getValue();
	}

	public void setDepthColumnIndex(int s) {
		depthColumnIndex.setValue(s, false);
		save();
	}

	public void setColumnsNamesIndex(Map<String, Integer> headerIndexMap) {
		latitudeColumnIndex.setValue(headerIndexMap.get(IxyzFile.LATITUDE) + 1, false);
		longitudeColumnIndex.setValue(headerIndexMap.get(IxyzFile.LONGITUDE) + 1, false);
		depthColumnIndex.setValue(headerIndexMap.get(IxyzFile.DEPTH) + 1, false);
		save();
	}

	public int getDecimalPoint() {
		return ((DecimalPointList) decimalPoint.getValue()).getValue();
	}

	public void setDecimalSeparator(String decimalSeparatorLabel) {
		decimalPoint.setValue(DecimalPointList.get(decimalSeparatorLabel), false);
	}

}
