/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;

/**
 * There is four kinds of :
 * <ul>
 * <li>well known csv format. These files can be parsed without any {@link FileInfoSettings}. In that case, a subclass
 * have to implement {@link IWellKnownCsvFileInfoSupplier}</li>
 * <li>navigation file . In that case, an subclass have to implement {@link #getFileInfo(String, FileInfoSettings)} and
 * {@link #guessSettingsAdapter(String, List)}</li>
 * </ul>
 */
public interface ICsvFileInfoSupplier {

	/** @return the managed file type */
	ContentType getContentType();

	/**
	 * Analyse first line of the file to define the parse settings.
	 *
	 * @return the settings or {@code Optional.empty()} if the file is not suitable for this supplier
	 */
	Optional<FileInfoSettings> evaluatesSettings(String filePath, List<String> firstLines);

	/**
	 * Used when session is restored and a FileInfoSettings was saved for the file
	 *
	 * @return the {@link ICsvFileInfo} corresponding to the specified file using the specified settings
	 */
	<I extends ICsvFileInfo> Optional<I> getFileInfo(String filePath, FileInfoSettings settings);

}
