/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv.info;

import java.util.List;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;

/**
 * FileInfo dedicated to CSV files
 */
public class CsvFileInfo extends BasicFileInfo implements ICsvFileInfo {

	/** Elevations min/max */
	protected DoubleRange elevations;

	/** True when values are estimated */
	protected boolean estimated = false;

	/**
	 * Constructor
	 */
	public CsvFileInfo(String path, ContentType contentType) {
		super(path, contentType);
	}

	/** {@inheritDoc} */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> properties = super.getProperties();
		if (elevations != null) {
			properties.addAll(Property.buildMinMax(elevations));
		}
		if (estimated) {
			properties.add(Property.build("Estimated values", "Yes"));
		}
		return properties;
	}

	/**
	 * @return the {@link #estimated}
	 */
	public boolean isEstimated() {
		return estimated;
	}

	/**
	 * @param estimated the {@link #estimated} to set
	 */
	public void setEstimated(boolean estimated) {
		this.estimated = estimated;
	}

	/**
	 * @return the {@link #elevations}
	 */
	public DoubleRange getElevations() {
		return elevations;
	}

	/**
	 * @param elevations the {@link #elevations} to set
	 */
	public void setElevations(DoubleRange elevations) {
		this.elevations = elevations;
	}

}
