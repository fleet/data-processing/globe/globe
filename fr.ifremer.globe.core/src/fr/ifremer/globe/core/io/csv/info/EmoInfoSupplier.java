/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv.info;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.csv.IWellKnownCsvFileInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.FileUtils;

/**
 * Supplier of {@link IFileInfo} for *.emo files.<br>
 * A emo file has a well known format and can be parsed without any FileInfoSettings.
 */
@Component(name = "globe_drivers_emo_csv_file_info_supplier", service = IWellKnownCsvFileInfoSupplier.class)
public class EmoInfoSupplier implements IWellKnownCsvFileInfoSupplier {

	public static final String EMO_EXTENSION = "emo";

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return ContentType.EMO_CSV;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath) {
		return evaluateSettings(filePath).map(infoSettings -> {
			var csvFileInfo = new CsvFileInfo(filePath, ContentType.EMO_CSV);
			var settings = new CsvParserSettings(//
					infoSettings.getRowCountToSkip(), //
					infoSettings.getDelimiter(), //
					infoSettings.getLongitudeIndex(), //
					infoSettings.getLatitudeIndex(), //
					infoSettings.getElevationIndex(), //
					NumberFormat.getInstance(infoSettings.getLocale()), //
					infoSettings.getElevationScaleFactor() //
			);
			new CsvParser().parse(csvFileInfo, settings);
			return csvFileInfo;
		});
	}

	/** {@inheritDoc} */
	@Override
	public Optional<FileInfoSettings> evaluateSettings(String filePath) {
		if (isEmoFile(filePath)) {
			FileInfoSettings result = new FileInfoSettings(ContentType.EMO_CSV);
			result.setRowCountToSkip(0);
			result.setDelimiter(';');
			result.setLongitudeIndex(0);
			result.setLatitudeIndex(1);
			result.set("Min elevation", 2);
			result.set("Max elevation", 3);
			result.setElevationIndex(4);
			result.set("Std dev", 5);
			result.set("Value count", 6);
			result.set("Interpolation flag", 7);
			result.set("Elevation smoothed", 8);
			result.set("Smoothed mean difference", 9);
			result.set("CDI", 10);
			result.set("CPRD", 11);
			result.setLocale(Locale.US);
			result.setElevationScaleFactor(-1);
			return Optional.of(result);
		}

		return Optional.empty();
	}

	private boolean isEmoFile(String filePath) {
		var fileExtension = FileUtils.getExtension(filePath);
		if (fileExtension.equalsIgnoreCase(EMO_EXTENSION))
			return true;
		else if (fileExtension.equalsIgnoreCase("csv") || fileExtension.equalsIgnoreCase("txt")) {
			try (var lines = Files.lines(Paths.get(filePath))) {
				return lines.findFirst().filter(firstLine -> firstLine.split(";").length == 12).isPresent();
			} catch (IOException | UncheckedIOException e) {
				// Not a text file or not in UTF8
			}
		}
		return false;
	}

}