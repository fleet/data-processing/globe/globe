package fr.ifremer.globe.core.io.csv;

import java.util.Collections;
import java.util.List;

import com.opencsv.exceptions.CsvException;

import fr.ifremer.globe.core.model.file.IFileInfo;

public interface ICsvFileInfo extends IFileInfo {

	/**
	 * @return {@link CsvException} raised during file parsing.
	 */
	default List<CsvException> getParsingErrors() {
		return Collections.emptyList();
	}

}
