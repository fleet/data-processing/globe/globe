/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv.info;

import java.text.NumberFormat;

/** Settings used by CsvParser to decrypt a CSV file */
public class /* record */ CsvParserSettings {

	private final int latitudeIndex;
	private final int longitudeIndex;
	private final int elevationIndex;
	private final char delimiter;
	private final NumberFormat numberFormat;
	private final int lineToSkip;
	private final double elevationScaleFactor;

	/**
	 * Constructor
	 */
	public CsvParserSettings(int lineToSkip, char delimiter, int longitudeIndex, int latitudeIndex, int elevationIndex,
			NumberFormat numberFormat, double elevationScaleFactor) {
		this.latitudeIndex = latitudeIndex;
		this.longitudeIndex = longitudeIndex;
		this.elevationIndex = elevationIndex;
		this.delimiter = delimiter;
		this.numberFormat = numberFormat;
		this.lineToSkip = lineToSkip;
		this.elevationScaleFactor = elevationScaleFactor;
	}

	/**
	 * @return the {@link #latitudeIndex}
	 */
	public int latitudeIndex() {
		return latitudeIndex;
	}

	/**
	 * @return the {@link #longitudeIndex}
	 */
	public int longitudeIndex() {
		return longitudeIndex;
	}

	/**
	 * @return the {@link #elevationIndex}
	 */
	public int elevationIndex() {
		return elevationIndex;
	}

	/**
	 * @return the {@link #delimiter}
	 */
	public char delimiter() {
		return delimiter;
	}

	/**
	 * @return the {@link #numberFormat}
	 */
	public NumberFormat numberFormat() {
		return numberFormat;
	}

	/**
	 * @return the {@link #lineToSkip}
	 */
	public int lineToSkip() {
		return lineToSkip;
	}

	/**
	 * @return the {@link #elevationScaleFactor}
	 */
	public double elevationScaleFactor() {
		return elevationScaleFactor;
	}
}
