/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv.parsing;

import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.opencsv.exceptions.CsvValidationException;

import fr.ifremer.globe.core.model.file.FileInfoSettings;

/**
 * Strategy of mapping where column's position is map to CsvTimestampedBean's field
 */
public class CsvTimestampedMappingStrategy<T extends CsvTimestampedBean> extends ColumnPositionMappingStrategy<T> {

	/** Logger */
	static final Logger logger = LoggerFactory.getLogger(CsvTimestampedMappingStrategy.class);

	/**
	 * Because OpenCsv is multi-threaded, use a mutex to protect the access of the {@link NumberFormat#parse(String)}
	 * and {@link PointCloudCsv#setValue)}
	 */
	protected final ReentrantLock lock = new ReentrantLock();

	/** Formatters used to parse a date, time and date time */
	private final Map<String, DateTimeFormatter> dateTimeFormatters = new HashMap<>();

	/**
	 * Constructor
	 * 
	 * @param headers the list of headers expected in the CSV file without date/time ones
	 */
	public CsvTimestampedMappingStrategy(List<String> headers, FileInfoSettings settings) {
		initColumMapping(headers, settings);
		dateTimeFormatters.put(CsvTimestampedBean.DATE_TIME_HEADER, null);
		dateTimeFormatters.put(CsvTimestampedBean.DATE_HEADER, null);
		dateTimeFormatters.put(CsvTimestampedBean.TIME_HEADER, null);
	}

	/**
	 * Prepare the column mapping of the specified headers.<br>
	 * Add the mapping for date/time column.
	 * 
	 * @param headers the list of headers expected in the CSV file without date/time ones
	 */
	private void initColumMapping(List<String> headers, FileInfoSettings settings) {
		var columnMapping = new ArrayList<String>();
		// Mapping column index => attribute name of PointCloudCsv bean
		Stream.concat(//
				headers.stream(), //
				CsvTimestampedBean.getAllHeaders().stream())//
				.forEach(header -> {
					int colIndex = settings.getInt(header, -1);
					if (colIndex >= 0) {
						// Ensure list capacity
						while (columnMapping.size() <= colIndex)
							columnMapping.add("dummy");
						columnMapping.set(colIndex, header);
					}
				});
		setColumnMapping(columnMapping.toArray(new String[columnMapping.size()]));
	}

	/** Redefine super.setFieldValue to capture date time values */
	@Override
	protected void setFieldValue(Map<Class<?>, Object> beanTree, String stringValue, int column)
			throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, CsvConstraintViolationException,
			CsvValidationException {

		// Is it a date time ?
		String header = headerIndex.getByPosition(column);
		if (dateTimeFormatters.containsKey(header)) {
			lock.lock(); // block until condition holds
			try {
				if (beanTree.get(getType()) instanceof CsvTimestampedBean timestampedBean) {
					DateTimeFormatter dateTimeFormatter = dateTimeFormatters.computeIfAbsent(header,
							k -> timestampedBean.inferDateTimeFormatter(k, stringValue).orElse(null));
					if (dateTimeFormatter != null)
						timestampedBean.parseInstant(header, stringValue, dateTimeFormatter);
				}
			} catch (DateTimeParseException e) {
				if (logger.isDebugEnabled())
					logger.debug("Unparsable date/time {}", stringValue);
			} finally {
				lock.unlock();
			}
			return; // Job done
		}

		super.setFieldValue(beanTree, stringValue, column);
	}
}
