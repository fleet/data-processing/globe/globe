/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.csv.info;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.io.csv.ICsvFileInfoSupplier;
import fr.ifremer.globe.core.io.csv.IWellKnownCsvFileInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;

/**
 * IFileInfoSupplier managing all ICsvFileInfoSupplier.
 */
@Component(name = "globe_drivers_csv_file_info_supplier", service = { CsvInfoSupplier.class, IFileInfoSupplier.class })
public class CsvInfoSupplier implements IFileInfoSupplier<IFileInfo> {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(CsvInfoSupplier.class);

	/** All suppliers of CSV files */
	protected List<ICsvFileInfoSupplier> csvFileInfoSuppliers = new ArrayList<>();
	/** Temporary list to store all registered supplier in order to sort them when activate the component */
	protected List<Pair<ICsvFileInfoSupplier, Integer>> tmpFileInfoSuppliers = new ArrayList<>();
	/** All suppliers of CSV files */
	protected List<IWellKnownCsvFileInfoSupplier> wellKnownCsvFileInfoSuppliers = new ArrayList<>();

	@Activate
	protected void postConstruct() {
		tmpFileInfoSuppliers.sort((p1, p2) -> p1.getSecond().compareTo(p2.getSecond()));
		csvFileInfoSuppliers = tmpFileInfoSuppliers.stream().map(Pair::getFirst).collect(Collectors.toList());
		// The list is not necessary any more
		tmpFileInfoSuppliers = null;
	}

	/**
	 * @return always Optional.empty() because this Supplier is only able to produce a IFileInfo from a FileInfoSettings
	 */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath) {
		for (IWellKnownCsvFileInfoSupplier csvFileInfoSupplier : wellKnownCsvFileInfoSuppliers) {
			var result = csvFileInfoSupplier.getFileInfo(filePath);
			if (result.isPresent()) {
				return result;
			}
		}

		// No supplier was able to parse the file.
		return Optional.empty();
	}

	/**
	 * @return the settings to use for parsing the file or {@code Optional.empty()} none
	 */
	@Override
	public Optional<FileInfoSettings> evaluateSettings(String filePath) {
		for (IWellKnownCsvFileInfoSupplier csvFileInfoSupplier : wellKnownCsvFileInfoSuppliers) {
			var result = csvFileInfoSupplier.evaluateSettings(filePath);
			if (result.isPresent()) {
				return result;
			}
		}
		return Optional.empty();
	}

	/**
	 * Used when
	 * <ul>
	 * <li>session is restored and no FileInfoSettings has been saved for the specified file</li>
	 * <li>file is added to list of input files by a Wizard</li>
	 * </ul>
	 */
	@Override
	public Optional<IFileInfo> getFileInfoSilently(String filePath) {
		// This is the case where the session is restored with a CSV file which can be parsed without FileInfoSettings
		var result = getFileInfo(filePath);

		// Not a WellKnownCsv ?
		if (result.isEmpty() && hasRightExtension(filePath)) {
			// By default, guess this is a BATHIMETRIC csv
			result = Optional.of(new CsvFileInfo(filePath, ContentType.BATHIMETRIC_CSV));
		}

		return result;
	}

	/** Used when session is restored */
	@Override
	public Optional<IFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		ContentType contentType = settings.getContentType();
		if (!getContentTypes().contains(contentType)) {
			// No ContentType
			return Optional.empty();
		}

		ICsvFileInfoSupplier csvFileInfoSupplier = csvFileInfoSuppliers.stream() //
				.filter(supplier -> supplier.getContentType() == contentType)//
				.findFirst()//
				.orElse(null);
		if (csvFileInfoSupplier == null) {
			// Not a CSV file
			return Optional.empty();
		}

		return csvFileInfoSupplier.getFileInfo(filePath, settings).map(IFileInfo.class::cast);
	}

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentTypes() {
		Set<ContentType> csvContentTypes = csvFileInfoSuppliers.stream().map(ICsvFileInfoSupplier::getContentType)
				.collect(Collectors.toSet());
		csvContentTypes.addAll(wellKnownCsvFileInfoSuppliers.stream().map(IWellKnownCsvFileInfoSupplier::getContentType)
				.collect(Collectors.toSet()));
		return csvContentTypes;
	}

	/** {@inheritDoc} */
	@Override
	public List<String> getExtensions() {
		return List.of("csv", "txt", "xyz", "emo");
	}

	/** {@inheritDoc} */
	@Override
	public List<String> getExtensions(ContentType contentType) {
		if (contentType == ContentType.BATHIMETRIC_CSV) {
			return List.of("csv", "txt", "xyz");
		}
		if (contentType == ContentType.EMO_CSV) {
			return List.of("emo");
		}
		return List.of("csv", "txt");
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		var allExtensions = getExtensions().stream().//
				map(extension -> "*." + extension). //
				collect(Collectors.toSet());
		return Collections.singletonList(new Pair<>("CSV files", StringUtils.join(allExtensions, ';')));
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters(ContentType contentType) {
		var allExtensions = StringUtils.join(//
				getExtensions().stream().//
						map(extension -> "*." + extension). //
						collect(Collectors.toList()), //
				";");
		String filterName = "Bathymetric CSV file";
		if (contentType == ContentType.NAVIGATION_CSV) {
			filterName = "Navigation CSV file";
		} else if (contentType == ContentType.POINT_CLOUD_CSV) {
			filterName = "Point cloud CSV file";
		}
		return List.of(Pair.of(filterName, allExtensions));
	}

	/** Accept a new ICsvFileInfoSupplier provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addCsvFileInfoSupplier(ICsvFileInfoSupplier csvFileInfoSupplier, Map<String, Object> properties) {
		logger.debug("{} registered : {}", ICsvFileInfoSupplier.class.getSimpleName(),
				csvFileInfoSupplier.getClass().getName());
		Integer priority = (Integer) properties.getOrDefault(FileInfoSupplierPriority.PROPERTY_PRIORITY,
				Integer.valueOf(1));
		tmpFileInfoSuppliers.add(Pair.of(csvFileInfoSupplier, priority));
	}

	/** Accept a new IWellKnownCsvFileInfoSupplier provided by the driver bundle */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE)
	public void addWellKnownCsvFileInfoSupplier(IWellKnownCsvFileInfoSupplier wellKnownCsvFileInfoSupplier) {
		logger.debug("{} registered : {}", IWellKnownCsvFileInfoSupplier.class.getSimpleName(),
				wellKnownCsvFileInfoSupplier.getClass().getName());
		wellKnownCsvFileInfoSuppliers.add(wellKnownCsvFileInfoSupplier);
	}

	/**
	 * @return the {@link #csvFileInfoSuppliers}
	 */
	public List<ICsvFileInfoSupplier> getCsvFileInfoSuppliers() {
		return csvFileInfoSuppliers;
	}

	/** @return true if file has a correct extension for this supplier */
	public boolean hasRightExtension(String filePath) {
		return getExtensions().contains(FilenameUtils.getExtension(filePath));
	}

}