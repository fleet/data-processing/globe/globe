/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.core.io.csv.info;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.csv.CsvPreferences;
import fr.ifremer.globe.core.io.csv.CsvPreferences.DecimalPointList;
import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.io.csv.ICsvFileInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.utils.csv.CsvUtils;

/**
 * Loader of XYZ file info.
 */
@Component(name = "globe_drivers_bathymetric_csv_file_info_supplier", service = ICsvFileInfoSupplier.class)
public class BathymetricCsvInfoSupplier implements ICsvFileInfoSupplier {
	/** Csv csvPreferences */
	@Reference
	protected CsvPreferences csvPreferences;

	/** {@inheritDoc} */
	@Override
	public ContentType getContentType() {
		return ContentType.BATHIMETRIC_CSV;
	}

	/**
	 * A XYZ file may have the columns Longitude, Latitude, Altitude/Sonde
	 */
	@Override
	public Optional<FileInfoSettings> evaluatesSettings(String filePath, List<String> firstLines) {

		String line = firstLines.get(0);
		var sep = CsvUtils.guessDelimiter(line);
		if (sep.isPresent()) {
			FileInfoSettings result = new FileInfoSettings(getContentType());
			result.setElevationScaleFactor(-1d);
			result.setRowCountToSkip(1);
			result.setLocale(Locale.US);
			char delimiter = sep.get();
			result.setDelimiter(delimiter);

			// Search columns
			var columns = CsvUtils.parseLine(firstLines.get(0), delimiter);
			int lonIndex = CsvUtils.guessLongitudeIndex(columns);
			result.set(FileInfoSettings.KEY_COLUMN_LONGITUDE_INDEX, lonIndex >= 0 ? lonIndex : -2);
			int latIndex = CsvUtils.guessLatitudeIndex(columns);
			result.set(FileInfoSettings.KEY_COLUMN_LATITUDE_INDEX, latIndex >= 0 ? latIndex : -2);
			int elevIndex = CsvUtils.guessElevationIndex(columns);
			result.set(FileInfoSettings.KEY_COLUMN_ELEVATION_INDEX, elevIndex >= 0 ? elevIndex : -2);
			return Optional.of(result);
		}

		return Optional.of(loadPreferences());
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ICsvFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		CsvParserSettings parserConfiguration = new CsvParserSettings(settings.getRowCountToSkip(), settings.getDelimiter(),
				settings.getLongitudeIndex(), settings.getLatitudeIndex(), settings.getElevationIndex(),
				NumberFormat.getInstance(settings.getLocale()), settings.getElevationScaleFactor());
		CsvFileInfo result = new CsvFileInfo(filePath, ContentType.BATHIMETRIC_CSV);
		if (new CsvParser().parse(result, parserConfiguration)) {
			saveCsvPreferences(parserConfiguration);
		} else {
			// Parse error
			result = null;
		}
		return Optional.ofNullable(result);
	}

	/** Specific code for CSV files : load settings from csvPreferences */
	protected FileInfoSettings loadPreferences() {
		var result = new FileInfoSettings();
		result.setDelimiter(csvPreferences.getDelimiter().isEmpty() ? ';' : csvPreferences.getDelimiter().charAt(0));
		result.setElevationScaleFactor(csvPreferences.getIsDepthPositiveBelowSurface().booleanValue() ? -1d : 1d);
		result.setRowCountToSkip(csvPreferences.getRowCountToSkip());
		result.setLongitudeIndex(csvPreferences.getLongitudeColumnIndex() - 1);
		result.setLatitudeIndex(csvPreferences.getLatitudeColumnIndex() - 1);
		result.setElevationIndex(csvPreferences.getDepthColumnIndex() - 1);
		var decimalPoint = DecimalPointList.values()[csvPreferences.getDecimalPoint()];
		result.setLocale(decimalPoint == DecimalPointList.COMMA ? Locale.FRANCE : Locale.US);
		return result;
	}

	/** Specific code for CSV files : save settings in csvPreferences */
	protected void saveCsvPreferences(CsvParserSettings parserConfiguration) {
		// Field delimiter
		csvPreferences.setDelimiter(String.valueOf(parserConfiguration.delimiter()));
		// Depth sign
		csvPreferences.setIsDepthPositiveBelowSurface(parserConfiguration.elevationScaleFactor() < 0d);
		// Nb lines to skip
		csvPreferences.setRowCountToSkip(parserConfiguration.lineToSkip());
		// Column Order values
		csvPreferences.setColumnsNamesIndex(Map.of(//
				IxyzFile.LONGITUDE, parserConfiguration.longitudeIndex(), //
				IxyzFile.LATITUDE, parserConfiguration.latitudeIndex(), //
				IxyzFile.DEPTH, parserConfiguration.elevationIndex()//
		));
		// Decimal point
		csvPreferences.setDecimalSeparator(
				NumberFormat.getInstance(Locale.US).equals(parserConfiguration.numberFormat()) ? "." : ",");
		csvPreferences.save();
	}
}