

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.placa3d.tecGenerator.ElevationReaderAdaptorTest;
import fr.ifremer.globe.placa3d.tectonic.MultiLayerTecFileReaderTest;
import fr.ifremer.globe.placa3d.tectonic.MultiLayerTecFileWriterTest;
import fr.ifremer.globe.placa3d.tectonic.TectonicUtilsTest;
import fr.ifremer.globe.utils.cache.TemporaryCache;

@RunWith(Suite.class)
@SuiteClasses({  MultiLayerTecFileWriterTest.class,MultiLayerTecFileReaderTest.class, TectonicUtilsTest.class, ElevationReaderAdaptorTest.class })
public class MvnTestSuite {
	@AfterClass
	public static void cleanUp() {
		TemporaryCache.cleanAllFiles();
	}
}
