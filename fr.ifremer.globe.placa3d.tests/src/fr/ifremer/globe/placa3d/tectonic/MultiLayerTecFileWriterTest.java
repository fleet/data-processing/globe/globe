package fr.ifremer.globe.placa3d.tectonic;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.TectonicGrid;
import fr.ifremer.globe.placa3d.data.TectonicLayer;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.placa3d.writer.MultiLayerTecFileWriter;
import fr.ifremer.globe.utils.test.GlobeTestUtil;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

public class MultiLayerTecFileWriterTest {

	private static Logger logger = LoggerFactory.getLogger(GlobeTestUtil.class);

	@Test
	public void testWriteFile() throws Exception {

		File file = new File(GlobeTestUtil.getTestDataPath(), "file/generated/Africa.mtec");
		write(file);

	}

	public void write(File file) throws Exception {
		TectonicModel model = new TectonicModel();

		// Age intervals of plate's texture/elevation visibilities
		double b1 = MagneticAnomalies.getInstance().getDoubleAnomaly("ano6");
		double e1 = MagneticAnomalies.getInstance().getDoubleAnomaly("ano7");

		double b2 = MagneticAnomalies.getInstance().getDoubleAnomaly("ano13");
		double e2 = MagneticAnomalies.getInstance().getDoubleAnomaly("ano17");

		List<TectonicLayer> tectonicLayers = new ArrayList<>();
		tectonicLayers.add(new TectonicLayer("Layer 1", b1, e1));
		tectonicLayers.add(new TectonicLayer("Layer 2", b2, e2));

		Sector sector = new Sector(Angle.fromDegrees(15), Angle.fromDegrees(17), Angle.fromDegrees(40),
				Angle.fromDegrees(42));
		TectonicPlateModel p1 = model.addPlate("Africa", tectonicLayers, sector);

		PoleRotations rotations = new PoleRotations();
		rotations.addRotation(new Rotation(LatLon.fromDegrees(40.0, 15.0), 12, b1, e1));
		rotations.addRotation(new Rotation(LatLon.fromDegrees(30.0, 68.0), 5, b2, e2));
		TectonicMovement movement = new TectonicMovement(file.getName());
		movement.setPoleRotation(rotations);
		p1.setMovement(movement);

		double[][] z = new double[][] { { 0.0, 0.0, 0.0, 0.0, 100.0, 100.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 100.0, 100.0, 100.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 100.0, 100.0, 100.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 100.0, 100.0, 100.0, 100.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 0.0, 0.0 },
				{ 0.0, 0.0, 100.0, 120.0, 150.0, 150.0, 120.0, 100.0, 0.0, 0.0 },
				{ 0.0, 0.0, 100.0, 120.0, 150.0, 150.0, 120.0, 100.0, 0.0, 0.0 },
				{ 0.0, 0.0, 100.0, 120.0, 150.0, 150.0, 120.0, 100.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 100.0, 100.0, 100.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 100.0, 100.0, 0.0, 0.0, 0.0, 0.0 } };
		double[][] mask = new double[][] { { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 } };

		double[][] tex = new double[10][10];
		fillWithRGBValues(tex, 10, 10);

		TectonicGrid elevation = new TectonicGrid(p1.getGeoBox(), z);
		p1.setElevationGrid(elevation);

		TectonicGrid texture = new TectonicGrid(p1.getGeoBox(), tex);
		p1.setTextureGrid(texture);

		TectonicGrid maskGrid = new TectonicGrid(p1.getGeoBox(), mask);
		p1.setMaskingGrid(maskGrid);

		p1.setZrange(new DoubleRange(0.0, 150.0));

		logger.info("file = " + file.getAbsolutePath());
		MultiLayerTecFileWriter writer = new MultiLayerTecFileWriter(file);
		logger.info("Writer = " + writer);
		try {

			// **************************************************************************
			// LE CODE SUIVANT N'EST PAS DU CODE DESTINE A ETRE REPRODUIT EN PRODUCTION
			//
			// L'écriture d'un TectonicModel n'est normalement pas prise en charge.
			//
			// Ce code est un outil, utilisé dans des conditions spéciales : l'âge du
			// TectonicModel évolue par incréments et ne revient jamais en arrière (ce
			// qui rendrait caduque le procédé d'écriture).
			// **************************************************************************

			logger.info("Opening writer = " + writer);
			writer.open();
			logger.info("Writing Metadata writer = " + writer);
			writer.writeMetadata(model);

			// On crée un player qui va ensuite faire évoluer l'age par incréments de 1 million d'années
			TectonicPlayerModel player = new TectonicPlayerModel();
			player.addPropertyChangeListener(evt -> {
				if (TectonicPlayerModel.PROPERTY_DATEINDEX.equals(evt.getPropertyName())) {
					model.setAge((Integer) evt.getNewValue());
				}
			});

			// A chaque changement d'ère, on écrit les grilles de données associées dans le fichier
			List<TectonicPlateModel> plates = model.getPlates();
			for (TectonicPlateModel plate : plates) {

				// writer.writePlateGrids(plate, plate.getMetadata().getAgeLayersIntervals().get(0), 0);

				plate.getPropertyChangeSupport().addPropertyChangeListener(evt -> {

					try {
						Integer currentAgeInterval = (Integer) evt.getNewValue();
						if (TectonicPlateModel.PROPERTY_CURRENT_AGE_INTERVAL.equals(evt.getPropertyName())) {
							if (currentAgeInterval.intValue() >= 0) {
								DoubleRange range = plate.getMetadata().getTectonicLayers()
										.get(currentAgeInterval.intValue()).getAge();
								writer.writePlateGrids(plate, range, currentAgeInterval.intValue());
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						Assert.fail(e.getMessage());
					}
				});
			}

			player.setMinDateIndex(Activator.getPreferences().getMinimumAge().getValue());
			player.setMaxDateIndex(Activator.getPreferences().getMaximumAge().getValue());
			player.setPlaying(true);

			for (int i = Activator.getPreferences().getMinimumAge().getValue(); i < Activator.getPreferences()
					.getMaximumAge().getValue(); i++) {
				player.jumpToNextDate();
			}
			player.setPlaying(false);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw e;
			}
		}
	}

	private void fillWithRGBValues(double[][] tex, int width, int height) {

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				int r = (int) (x / ((double) width - 1) * 255);
				int g = (int) ((double) (x * y) / (double) ((width - 1) * (height - 1)) * 255);
				int b = (int) (x / ((double) width - 1) * 255);

				Color color = new Color(r, g, b, 0);
				tex[x][y] = color.getRGB();
			}
		}
	}

}
