package fr.ifremer.globe.placa3d.tectonic;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.placa3d.util.TectonicUtils;
import gov.nasa.worldwind.geom.LatLon;

public class TectonicUtilsTest {

	@Test
	public void testFitTrans() {
		
		// Data test from code Fortant
		LatLon r1 = LatLon.fromDegrees(-8.22354794, -34.9636459);
		LatLon r2 = LatLon.fromDegrees(0.414565176, 9.38087368);
		
		// Computed rotation
		Rotation computedRotation = TectonicUtils.fitTrans(r1, r2);
		// Expected rotation
		Rotation expectedRotation = new Rotation(LatLon.fromDegrees(77.9050751, -82.5541306), 45.029476, 0, 0);
		
		// Compare latitude
		Assert.assertEquals(expectedRotation.getLatLon().getLatitude().getDegrees(), computedRotation.getLatLon().getLatitude().getDegrees(), 0.00001);
		// Compare longitude
		Assert.assertEquals(expectedRotation.getLatLon().getLongitude().getDegrees(), computedRotation.getLatLon().getLongitude().getDegrees(), 0.00001);
		// Compare angle
		Assert.assertEquals(expectedRotation.getAngle(), computedRotation.getAngle(), 0.00001);
	}

	@Test
	public void testFitRot() {

		// Data test from code Fortant
		LatLon r1 = LatLon.fromDegrees(0.213026777, 9.58243847);
		LatLon r2 = LatLon.fromDegrees(-27.3095894, -6.74441051);
		LatLon r3 = LatLon.fromDegrees(-27.1303577, 15.4278488);
		
		// Computed rotation
		Rotation computedRotation = TectonicUtils.fitRot(r1, r2, r3);
		// Expected rotation
		Rotation expectedRotation = new Rotation(LatLon.fromDegrees(0.213026777, 9.58243847), 39.5632248, 0, 0);
		
		// Compare latitude
		Assert.assertEquals(expectedRotation.getLatLon().getLatitude().getDegrees(), computedRotation.getLatLon().getLatitude().getDegrees(), 0.00001);
		// Compare longitude
		Assert.assertEquals(expectedRotation.getLatLon().getLongitude().getDegrees(), computedRotation.getLatLon().getLongitude().getDegrees(), 0.00001);
		// Compare angle
		Assert.assertEquals(expectedRotation.getAngle(), computedRotation.getAngle(), 0.00001);
	}

}
