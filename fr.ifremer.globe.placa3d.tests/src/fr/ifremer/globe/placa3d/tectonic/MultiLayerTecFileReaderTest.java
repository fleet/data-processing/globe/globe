package fr.ifremer.globe.placa3d.tectonic;

import java.io.File;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateMetadata;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.reader.MultiLayerTecFileReader;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class MultiLayerTecFileReaderTest {

	@Test
	public void testReadFile() throws Exception {

		File file = new File(GlobeTestUtil.getTestDataPath(), "file/generated/Africa.hdf5");

		MultiLayerTecFileWriterTest writer = new MultiLayerTecFileWriterTest();
		writer.write(file);

		TectonicModel model = new TectonicModel();
		MultiLayerTecFileReader reader = new MultiLayerTecFileReader(file);

		try {
			reader.open();

			reader.loadMetadata(model);

			Assert.assertEquals(0.0, model.getAge(), 0.0);
			Assert.assertEquals(1, model.getPlates().size());

			double firstAge = MagneticAnomalies.getInstance().getDoubleAnomaly("ano6");
			model.setAge(firstAge);

			List<TectonicPlateModel> plates = model.getPlates();
			for (TectonicPlateModel plate : plates) {

				TectonicPlateMetadata metadata = plate.getMetadata();

				// Name, geobox and movement are not null
				Assert.assertNotNull(metadata.getPlateName());
				Assert.assertNotNull(metadata.getGeoBox());
				Assert.assertNotNull(metadata.getMovement());

				// Grids are not loaded yet
				Assert.assertTrue(!plate.hasGrids());
				Assert.assertNull(plate.getMaskingGrid());
				Assert.assertNull(plate.getElevationGrid());
				Assert.assertNull(plate.getTextureGrid());

				// Load all plate's grids
				reader.readPlate(model, metadata.getPlateIndex(), null, true, false);
				Assert.assertFalse(!plate.hasGrids());
				Assert.assertNotNull(plate.getMaskingGrid());
				Assert.assertNotNull(plate.getElevationGrid());
				Assert.assertNotNull(plate.getTextureGrid());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
		}
	}
}
