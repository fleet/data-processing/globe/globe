package fr.ifremer.globe.placa3d.morphing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.util.Timer;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainMorphing {

	public static class Canvas extends JPanel {

		private static final long serialVersionUID = 1L;

		static int xpoints[] = { 262, 260, 258, 257, 255, 253, 252, 251, 250, 249, 248, 248, 248, 248, 247, 246, 245, 244, 242, 241, 241, 240, 239, 239, 238, 237, 236, 236, 235, 234, 234, 233, 232,
				231, 230, 229, 229, 229, 228, 227, 225, 225, 224, 224, 224, 224, 224, 224, 223, 222, 222, 221, 220, 218, 214, 212, 211, 212, 212, 211, 210, 209, 206, 203, 200, 197, 195, 192, 190,
				188, 186, 184, 183, 181, 178, 176, 173, 168, 166, 165, 165, 164, 160, 160, 157, 155, 154, 154, 152, 151, 149, 147, 146, 144, 143, 143, 143, 143, 143, 144, 144, 144, 143, 143, 144,
				144, 144, 145, 144, 143, 144, 144, 145, 146, 146, 145, 145, 145, 144, 143, 142, 141, 140, 140, 140, 140, 140, 141, 141, 141, 141, 140, 139, 138, 138, 136, 135, 134, 133, 132, 132,
				132, 132, 131, 129, 127, 125, 124, 124, 123, 124, 123, 122, 121, 121, 119, 116, 114, 111, 109, 108, 106, 104, 102, 101, 100, 99, 97, 95, 93, 93, 93, 91, 90, 90, 89, 87, 87, 86, 85,
				84, 82, 82, 82, 82, 81, 81, 82, 81, 80, 80, 80, 79, 79, 78, 77, 77, 77, 76, 75, 75, 75, 75, 74, 74, 74, 73, 73, 72, 72, 71, 71, 71, 71, 70, 70, 71, 71, 71, 71, 71, 71, 70, 70, 70, 70,
				70, 69, 69, 69, 69, 69, 69, 69, 70, 70, 70, 70, 69, 69, 69, 68, 68, 68, 68, 68, 68, 68, 69, 68, 67, 67, 67, 67, 68, 68, 69, 70, 70, 71, 69, 68, 67, 65, 65, 64, 64, 64, 64, 63, 63, 63,
				63, 62, 61, 60, 58, 57, 56, 56, 55, 54, 52, 50, 49, 47, 46, 46, 44, 42, 40, 35, 35, 32, 32, 33, 34, 34, 35, 36, 38, 39, 41, 43, 45, 46, 46, 46, 45, 44, 43, 43, 40, 37, 36, 34, 32, 30,
				29, 27, 26, 25, 24, 24, 24, 24, 23, 24, 23, 21, 20, 20, 18, 18, 17, 16, 15, 15, 14, 14, 13, 12, 10, 10, 10, 10, 9, 8, 6, 5, 3, 1, 0, 1, 2, 4, 4, 3, 3, 5, 7, 9, 11, 14, 16, 20, 24, 29,
				31, 34, 36, 38, 39, 41, 44, 48, 50, 53, 55, 57, 59, 60, 62, 66, 69, 70, 73, 74, 77, 79, 81, 83, 84, 86, 87, 89, 91, 92, 93, 93, 93, 92, 92, 92, 94, 95, 100, 104, 107, 111, 113, 116,
				117, 118, 120, 122, 124, 126, 129, 131, 134, 137, 140, 142, 145, 147, 149, 153, 158, 162, 166, 172, 174, 177, 180, 184, 188, 191, 195, 199, 207, 213, 220, 227, 234, 239, 242, 246,
				248, 253, 258, 261, 266, 270, 273, 275, 278, 280, 284, 286, 287, 289, 291, 294, 294, 295, 296, 295, 296, 297, 298, 299, 300, 302, 304, 306, 308, 309, 310, 310, 310, 310, 310, 309,
				309, 308, 307, 305, 303, 303, 302, 301, 300, 300, 299, 298, 298, 297, 297, 296, 296, 292, 287, 281, 277, 275, 272, 268, 263, 262 };

		static int ypoints[] = { 136, 134, 131, 130, 131, 131, 130, 129, 129, 131, 132, 134, 135, 136, 137, 139, 140, 140, 137, 139, 140, 140, 141, 140, 140, 140, 140, 141, 141, 141, 142, 142, 142,
				142, 142, 140, 141, 142, 142, 142, 143, 141, 141, 141, 140, 139, 138, 137, 137, 138, 139, 140, 141, 142, 144, 144, 147, 150, 151, 153, 154, 156, 159, 160, 160, 162, 163, 165, 168,
				170, 173, 174, 175, 176, 178, 181, 183, 186, 188, 190, 192, 193, 194, 195, 195, 196, 196, 198, 200, 201, 200, 200, 201, 203, 205, 207, 209, 211, 212, 212, 213, 214, 215, 216, 218,
				219, 221, 222, 221, 222, 222, 223, 224, 226, 226, 228, 228, 230, 233, 235, 237, 240, 242, 244, 247, 248, 250, 252, 253, 255, 255, 255, 255, 255, 255, 255, 257, 258, 260, 262, 264,
				265, 265, 266, 266, 267, 268, 268, 270, 272, 272, 273, 274, 275, 275, 277, 277, 275, 272, 268, 266, 261, 255, 251, 247, 243, 242, 239, 238, 235, 234, 233, 228, 224, 222, 220, 218,
				215, 213, 211, 211, 209, 208, 207, 206, 205, 204, 203, 203, 203, 203, 202, 201, 201, 199, 199, 198, 197, 196, 194, 193, 191, 189, 188, 187, 185, 184, 182, 180, 179, 178, 176, 176,
				175, 174, 173, 171, 170, 169, 168, 167, 167, 167, 168, 167, 166, 165, 164, 163, 162, 161, 159, 159, 157, 156, 154, 153, 151, 149, 148, 148, 147, 147, 147, 146, 145, 144, 143, 142,
				141, 141, 140, 139, 139, 137, 136, 136, 136, 136, 135, 135, 135, 135, 135, 135, 134, 135, 137, 138, 139, 140, 142, 143, 145, 147, 147, 148, 148, 149, 149, 150, 150, 150, 150, 150,
				150, 150, 149, 148, 147, 144, 140, 139, 137, 135, 134, 134, 135, 135, 134, 134, 134, 134, 133, 131, 129, 128, 127, 127, 128, 129, 129, 130, 130, 130, 130, 129, 129, 127, 125, 124,
				122, 122, 122, 122, 121, 120, 120, 121, 120, 120, 120, 120, 119, 118, 117, 115, 113, 112, 111, 111, 110, 110, 108, 107, 106, 105, 105, 104, 104, 104, 104, 104, 106, 97, 87, 81, 73,
				66, 59, 52, 45, 42, 40, 40, 37, 34, 30, 26, 22, 20, 18, 17, 17, 17, 16, 15, 12, 11, 10, 9, 9, 9, 10, 10, 10, 10, 10, 9, 9, 8, 6, 5, 4, 1, 0, 0, 0, 1, 2, 3, 7, 7, 10, 11, 12, 12, 12,
				13, 14, 14, 15, 16, 17, 20, 21, 22, 24, 27, 28, 31, 33, 36, 39, 41, 43, 45, 48, 51, 53, 55, 57, 57, 59, 59, 61, 61, 63, 64, 64, 65, 64, 65, 65, 66, 66, 66, 66, 66, 66, 66, 65, 66, 66,
				67, 67, 66, 66, 64, 63, 62, 60, 60, 59, 61, 63, 65, 66, 68, 69, 70, 71, 74, 76, 78, 80, 82, 84, 86, 88, 91, 92, 94, 96, 99, 101, 102, 104, 108, 109, 111, 114, 116, 118, 120, 121, 123,
				124, 127, 129, 130, 132, 133, 135, 136, 136, 136, 136, 136, 136

		};


		Polygon startShape = new Polygon(xpoints, ypoints, ypoints.length);
		Polygon endShape = new StarPolygon(150, 150, 150, 20, 12);

		// Polygon startShape = new StarPolygon(200, 200, 100, 20, 8);
		// Polygon endShape = new Polygon(new int[] { 100, 400, 400, 100 }, new
		// int[] { 100, 100, 300, 300 }, 4);
		Morphing2D morphing2D = new Morphing2D(endShape, startShape);

		private Image offScreenImageDrawed = null;
		private Graphics offScreenGraphicsDrawed = null;
		private Timer timer = new Timer();
		double morph = 0.0;
		double step = 0.01;

		public Canvas() {
			timer.schedule(new AutomataTask(), 0, 50);
			this.setPreferredSize(new Dimension(800, 600));
			this.setBackground(Color.white);


		}

		/**
		 * Use double buffering.
		 * 
		 * @see java.awt.Component#update(java.awt.Graphics)
		 */
		@Override
		public void update(Graphics g) {
			paint(g);
		}

		/**
		 * Draw this generation.
		 * 
		 * @see java.awt.Component#paint(java.awt.Graphics)
		 */
		@Override
		public void paint(final Graphics g) {

			final Dimension d = getSize();
			if (offScreenImageDrawed == null) {
				// Double-buffer: clear the offscreen image.
				offScreenImageDrawed = createImage(d.width, d.height);
			}
			offScreenGraphicsDrawed = offScreenImageDrawed.getGraphics();
			offScreenGraphicsDrawed.setColor(Color.white);
			offScreenGraphicsDrawed.fillRect(0, 0, d.width, d.height);
			// Paint Offscreen //
			renderOffScreen(offScreenImageDrawed.getGraphics());
			g.drawImage(offScreenImageDrawed, 0, 0, null);

			// File outputfile = new File("d:/tmp/Morp_" + (int) (morph * 100) +
			// ".jpg");
			// if (!outputfile.exists()) {
			// try {
			// ImageIO.write((BufferedImage) offScreenImageDrawed, "jpg",
			// outputfile);
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// }
		}

		public void renderOffScreen(final Graphics g) {
			// g.setColor(Color.GREEN);
			// ((Graphics2D) g).draw(startShape);
			// g.setColor(Color.RED);
			// ((Graphics2D) g).draw(endShape);
			g.setColor(Color.BLUE);
			((Graphics2D) g).draw(morphing2D);
		}

		private class AutomataTask extends java.util.TimerTask {
			@Override
			public void run() {
				// Run thread on event dispatching thread
				if (!EventQueue.isDispatchThread()) {
					EventQueue.invokeLater(this);
				} else {
					if (morphing2D != null) {
						morph += step;
						if (morph >= 1.0 || morph <= 0.0) {
							step = -step;
						}
						morphing2D.setMorphing(morph);

						if (Canvas.this != null) {
							Canvas.this.repaint();
						}
					}
				}

			} // End of Run
		}

		@SuppressWarnings("serial")
		class StarPolygon extends Polygon {
			public StarPolygon(int x, int y, int r, int innerR, int vertexCount) {
				this(x, y, r, innerR, vertexCount, 0);
			}

			public StarPolygon(int x, int y, int r, int innerR, int vertexCount, double startAngle) {
				super(getXCoordinates(x, y, r, innerR, vertexCount, startAngle), getYCoordinates(x, y, r, innerR, vertexCount, startAngle), vertexCount * 2);
			}

		}

		protected static int[] getXCoordinates(int x, int y, int r, int innerR, int vertexCount, double startAngle) {
			int res[] = new int[vertexCount * 2];
			double addAngle = 2 * Math.PI / vertexCount;
			double angle = startAngle;
			double innerAngle = startAngle + Math.PI / vertexCount;
			for (int i = 0; i < vertexCount; i++) {
				res[i * 2] = (int) Math.round(r * Math.cos(angle)) + x;
				angle += addAngle;
				res[i * 2 + 1] = (int) Math.round(innerR * Math.cos(innerAngle)) + x;
				innerAngle += addAngle;
			}
			return res;
		}

		protected static int[] getYCoordinates(int x, int y, int r, int innerR, int vertexCount, double startAngle) {
			int res[] = new int[vertexCount * 2];
			double addAngle = 2 * Math.PI / vertexCount;
			double angle = startAngle;
			double innerAngle = startAngle + Math.PI / vertexCount;
			for (int i = 0; i < vertexCount; i++) {
				res[i * 2] = (int) Math.round(r * Math.sin(angle)) + y;
				angle += addAngle;
				res[i * 2 + 1] = (int) Math.round(innerR * Math.sin(innerAngle)) + y;
				innerAngle += addAngle;
			}
			return res;
		}

	}

	public static void main(final String[] args) {
		final JFrame frame = new JFrame("Apside - Morphing demo") {
			private static final long serialVersionUID = 1L;

			@Override
			public void processWindowEvent(java.awt.event.WindowEvent e) {
				super.processWindowEvent(e);
				if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
					System.exit(-1);
				}
			}
		};
		frame.setPreferredSize(new Dimension(800, 600));
		frame.setBackground(Color.white);
		frame.add(new Canvas());
		frame.pack();
		frame.setVisible(true);

	}

} // End of the Class //