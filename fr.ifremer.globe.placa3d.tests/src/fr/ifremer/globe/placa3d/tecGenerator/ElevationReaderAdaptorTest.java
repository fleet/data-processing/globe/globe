/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.tecGenerator;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.internal.workbench.E4Workbench;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.globe.placa3d.application.context.ContextInitializer;
import fr.ifremer.globe.placa3d.reader.raster.RasterReaderFactory;
import fr.ifremer.globe.utils.test.GlobeTestUtil;
import fr.ifremer.viewer3d.data.reader.raster.RasterReader;
import gov.nasa.worldwind.geom.Sector;

/**
 * Test class of {@link #MultiLayerTecFileWriter}
 */
public class ElevationReaderAdaptorTest {

	/** File to test. */
	public static final String DTM_FILE = GlobeTestUtil.getTestDataPath() + "/file/placa/DTM/dtm.dtm.nc";
	/** Zone file. */
	public static final String DTM_COT_FILE = GlobeTestUtil.getTestDataPath() + "/file/placa/DTM/dtm.cot";

	/** Zone file. */
	public static final String GMT_COT_FILE = GlobeTestUtil.getTestDataPath() + "/file/placa/GMT/gmt.cot";
	/** GMT5 file to test. */
	public static final String GMT5_FILE = GlobeTestUtil.getTestDataPath() + "/file/placa/GMT/gmt5.grd";

	@Inject
	RasterReaderFactory rasterReaderFactory;

	/**
	 * Set up the IEclipseContext
	 */
	@SuppressWarnings("restriction")
	@Before
	public void setUp() {
		IEclipseContext testContext = E4Workbench.getServiceContext();
		ContextInitializer.activate(testContext);
		ContextInitializer.inject(this);
	}

	/**
	 * Test a grid computation of a DTM file.
	 */
	@Test
	public void testDTMFile() throws Exception {

		// Readers
		RasterReader terrainReader = rasterReaderFactory.getDtmReader(DTM_FILE);
		ElevationReaderAdaptor elevationReaderAdaptor = new ElevationReaderAdaptor(terrainReader);
		FileCotReader cotReader = new FileCotReader();
		cotReader.setFileNameList(new String[] { DTM_COT_FILE });
		Sector out = cotReader.computeBoundingBox();
		// Extractor
		ElevationExtractor elevationExtractor = new ElevationExtractor(elevationReaderAdaptor, cotReader);
		elevationExtractor.computeExtraction(out, new NullProgressMonitor());

		checkResult(elevationExtractor,
				new double[] { 43.64607476963131, 43.66238829054906, -1.5133360798439794, -1.5035437820856887 }, 833,
				363, 245353, -221.64, -39.33);
	}

	/**
	 * Test a grid computation of a GMT V4 file.
	 */
	@Test
	public void testGMT5File() throws Exception {

		// Readers
		RasterReader terrainReader = rasterReaderFactory.getGmtReader(GMT5_FILE);
		ElevationReaderAdaptor elevationReaderAdaptor = new ElevationReaderAdaptor(terrainReader);
		FileCotReader cotReader = new FileCotReader();
		cotReader.setFileNameList(new String[] { GMT_COT_FILE });
		Sector out = cotReader.computeBoundingBox();

		// Extractor
		ElevationExtractor elevationExtractor = new ElevationExtractor(elevationReaderAdaptor, cotReader);
		elevationExtractor.computeExtraction(out, new NullProgressMonitor());

		// GMT file read correctly ?
		Assert.assertEquals(1921, terrainReader.getRasterAttributes().getLines());
		Assert.assertEquals(0.0016, terrainReader.getRasterAttributes().getLineSpacing(), 0.0001);
		Assert.assertEquals(2221, terrainReader.getRasterAttributes().getColumns());
		Assert.assertEquals(0.0016, terrainReader.getRasterAttributes().getColumnSpacing(), 0.0001);
		Assert.assertEquals(0d, terrainReader.getRasterAttributes().getDepthOffset(), 0d);
		Assert.assertEquals(1d, terrainReader.getRasterAttributes().getDepthScale(), 0d);
		Assert.assertEquals(Double.NaN, terrainReader.getRasterAttributes().getMissingValue(), 0d);
		Assert.assertEquals(0d, terrainReader.getRasterAttributes().getValidMin(), 0d);
		Assert.assertEquals(3615.37, terrainReader.getRasterAttributes().getValidMax(), 0.01d);

		checkResult(elevationExtractor,
				new double[] { 40.510338645842204, 43.385427941563876, 2.848034686513416, 6.012743531831967 }, 1725,
				1898, 1602507, 0.0, 3615.37);
	}

	/**
	 * Check the computation.
	 */
	protected void checkResult(ElevationExtractor elevationExtractor, double[] expectedBoundingBox,
			int expectedLineCount, int expectedColumnCount, int expectedEmptyCellCount, double expectedMinElevation,
			double expectedMaxElevation) throws Exception {

		// Check boundingBox
		Assert.assertArrayEquals(expectedBoundingBox, elevationExtractor.getBoundingBox(), 0.0);

		// Check read grid
		IElevationReaderAdaptor elevationReaderAdaptor = elevationExtractor.getElevationReader();
		Assert.assertEquals(expectedLineCount, elevationReaderAdaptor.getNbOfLines());
		Assert.assertEquals(expectedColumnCount, elevationReaderAdaptor.getNbOfColumns());
		double[][] elevations = elevationExtractor.getElevationMatrix();
		Assert.assertEquals(expectedLineCount, elevations.length);
		Assert.assertEquals(expectedColumnCount, elevations[0].length);

		// Check values
		double minElevation = Double.POSITIVE_INFINITY;
		double maxElevation = Double.NEGATIVE_INFINITY;
		int emptyCellCount = 0;
		for (int line = 0; line < elevations.length; line++) {
			for (int column = 0; column < elevations[line].length; column++) {
				double elevation = elevations[line][column];
				if (elevation == TecGeneratorUtil.MISSING_VALUE) {
					emptyCellCount++;
				} else {
					minElevation = Math.min(minElevation, elevation);
					maxElevation = Math.max(maxElevation, elevation);
				}
			}
		}
		Assert.assertEquals(expectedEmptyCellCount, emptyCellCount);
		Assert.assertEquals(expectedMinElevation, minElevation, 0.01);
		Assert.assertEquals(expectedMaxElevation, maxElevation, 0.01);
	}
}
