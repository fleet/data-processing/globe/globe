package fr.ifremer.globe.editor.animation.util;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import gov.nasa.worldwind.geom.Position;

public class AnimationUtils {

	/** @return The Position with a applied VerticalExaggeration */
	public static Position applyVerticalExaggeration(Position position) {
		Position result = position;
		double verticalExaggeration = IGeographicViewService.grab().getWwd().getSceneController()
				.getVerticalExaggeration();
		if (verticalExaggeration != 1d) {
			result = new Position(position.getLatitude(), position.getLongitude(),
					position.getAltitude() * verticalExaggeration);
		}
		return result;
	}

	/** @return The Position with a unapplied VerticalExaggeration */
	public static Position unapplyVerticalExaggeration(Position position) {
		Position result = position;
		double verticalExaggeration = IGeographicViewService.grab().getWwd().getSceneController()
				.getVerticalExaggeration();
		if (verticalExaggeration != 1d) {
			result = new Position(position.getLatitude(), position.getLongitude(),
					position.getAltitude() * (1d / verticalExaggeration));
		}
		return result;
	}
}
