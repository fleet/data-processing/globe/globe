/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.util;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.geotools.geometry.jts.JTS;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.editor.animation.model.GoTowards;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.model.JourneyViewPoint;
import gov.nasa.worldwind.animation.AnimationSupport;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.globes.Earth;

/**
 * Compute a smoothed List of GoTowards.
 */
public class SmoothJourneyComputer {
	public final static Logger logger = LoggerFactory.getLogger(SmoothJourneyComputer.class);

	/**
	 * Constructor
	 */
	public SmoothJourneyComputer() {
	}

	public void smooth(JourneyFile journeyFile) {
		List<GoTowards> journeyPointsList = journeyFile.getJourneyPointsList();
		if (!journeyPointsList.isEmpty()) {
			double fit = journeyFile.getFit();
			// If we cross the date line, we have to split the journey alongside it, smooth each segment, and then put
			// it back together
			if (crossDateLine(journeyPointsList)) {
				List<List<GoTowards>> splitJourneyPoints = repeatLocationsAroundDateline(journeyPointsList);

				List<List<GoTowards>> smoothedSplitJourneyPoints = new ArrayList<>();
				for (List<GoTowards> segment : splitJourneyPoints) {
					smoothedSplitJourneyPoints.add(smoothGoTowards(segment, fit));
				}
				journeyFile.setSmoothedJourneyPointsList(fuse(smoothedSplitJourneyPoints));
			}
			// Otherwise smooth it directly
			else {
				journeyFile.setSmoothedJourneyPointsList(smoothGoTowards(journeyPointsList, fit));
			}
			journeyFile.setSmooth(true);
		}
	}

	/**
	 * Smooth the given List of GoTowards
	 */
	protected List<GoTowards> smoothGoTowards(List<GoTowards> originalGoTowards, double fit) {
		logger.debug(String.format("Smoothing a list of GoTowards (%d elements)", originalGoTowards.size()));

		List<SmoothGoTowards> SmoothGoTowardsList = smoothWithJts(originalGoTowards, fit);
		int nbSteps = computeNbSteps(originalGoTowards, SmoothGoTowardsList);
		patchRawSmoothedList(originalGoTowards, SmoothGoTowardsList, nbSteps);
		interpolateBehavior(SmoothGoTowardsList, nbSteps);
		List<GoTowards> result = new ArrayList<>(SmoothGoTowardsList);

		logger.debug(String.format("Resulting smoothed list has %d elements", SmoothGoTowardsList.size()));

		return result;
	}

	/**
	 * Calculate the linear interpolation for the altitude between two original GoTowards
	 */
	protected void interpolateBehavior(List<SmoothGoTowards> smoothedGoTowards, int nbSteps) {
		int step = 1;
		for (int i = 1; i < smoothedGoTowards.size(); i++) {
			SmoothGoTowards smoothGoTowards = smoothedGoTowards.get(i);
			JourneyViewPoint journeyViewPoint = smoothGoTowards.getJourneyViewPoint();

			JourneyViewPoint fromJourneyViewPoint = smoothGoTowards.getFromOriginalGoTowards().getJourneyViewPoint();
			JourneyViewPoint toJourneyViewPoint = smoothGoTowards.getToOriginalGoTowards().getJourneyViewPoint();

			double amount = (double) step / nbSteps;
			journeyViewPoint.setAltitude(AnimationSupport.mixDouble(amount, fromJourneyViewPoint.getAltitude(),
					toJourneyViewPoint.getAltitude()));
			journeyViewPoint
					.setHeading(Angle.mix(amount, fromJourneyViewPoint.getHeading(), toJourneyViewPoint.getHeading()));
			journeyViewPoint
					.setPitch(Angle.mix(amount, fromJourneyViewPoint.getPitch(), toJourneyViewPoint.getPitch()));

			step = step == nbSteps ? 1 : step + 1;
		}
	}

	/**
	 * Use Jts to smooth the journey
	 */
	protected void patchRawSmoothedList(List<GoTowards> originalGoTowards, List<SmoothGoTowards> smoothedGoTowards,
			int nbSteps) {

		// SmoothGoTowards previousNewGoTowards = null;
		GoTowards fromOriginalGoTowards = null;

		ListIterator<SmoothGoTowards> smoothedIterator = smoothedGoTowards.listIterator();
		for (GoTowards toOriginalGoTowards : originalGoTowards) {

			do {
				SmoothGoTowards oneSmoothedGoTowards = smoothedIterator.next();
				oneSmoothedGoTowards.setFromOriginalGoTowards(fromOriginalGoTowards);
				oneSmoothedGoTowards.setToOriginalGoTowards(toOriginalGoTowards);

				if (fromOriginalGoTowards != null) {
					oneSmoothedGoTowards.setMovingTime(fromOriginalGoTowards.getMovingTime() / nbSteps);
				}

				// get values from toOriginalGoTowards
				JourneyViewPoint toOriginalJourneyViewPoint = toOriginalGoTowards.getJourneyViewPoint();
				JourneyViewPoint smoothedJourneyViewPoint = oneSmoothedGoTowards.getJourneyViewPoint();
				smoothedJourneyViewPoint.setName(toOriginalJourneyViewPoint.getName());
				// altitude is NaN when the GoTowards has been inserted by the smoothing process
				if (Double.isNaN(smoothedJourneyViewPoint.getAltitude())) {
					smoothedJourneyViewPoint.setAltitude(toOriginalJourneyViewPoint.getAltitude());
				} else {
					oneSmoothedGoTowards.setDelay(toOriginalGoTowards.getDelay());
					break;
				}
			} while (smoothedIterator.hasNext());
			fromOriginalGoTowards = toOriginalGoTowards;
		}
	}

	/**
	 * Use Jts to creates a list of SmoothGoTowards to represent a smoothed journey of the specified list of GoTowards.
	 */
	protected List<SmoothGoTowards> smoothWithJts(List<GoTowards> originalGoTowards, double fit) {
		Coordinate[] originalCoordinate = new Coordinate[originalGoTowards.size()];
		for (int i = 0; i < originalGoTowards.size(); i++) {
			JourneyViewPoint journeyViewPoint = originalGoTowards.get(i).getJourneyViewPoint();
			originalCoordinate[i] = new Coordinate(journeyViewPoint.getLatitudeDegrees(),
					journeyViewPoint.getLongitudeDegrees(), journeyViewPoint.getAltitude());
		}
		GeometryFactory factory = new GeometryFactory();
		LineString lineLatLon = factory.createLineString(originalCoordinate);
		lineLatLon = (LineString) JTS.smooth(lineLatLon, fit);
		Coordinate[] smoothCoordinates = lineLatLon.getCoordinates();
		// Convert Jts Coordinates to GoTowards objects
		List<SmoothGoTowards> result = new ArrayList<>(smoothCoordinates.length);
		for (Coordinate smoothCoordinate : smoothCoordinates) {
			SmoothGoTowards newGoTowards = new SmoothGoTowards();
			JourneyViewPoint journeyViewPoint = new JourneyViewPoint("Smoth-" + System.currentTimeMillis(),
					smoothCoordinate.x, smoothCoordinate.y, smoothCoordinate.z, 0d, 0d);
			newGoTowards.setJourneyViewPoint(journeyViewPoint);
			newGoTowards.setDelay(0);
			newGoTowards.setMovingTime(0L);
			result.add(newGoTowards);
		}
		return result;
	}

	/** How many smoothedGoTowards for each originalGoTowards */
	protected int computeNbSteps(List<GoTowards> originalGoTowards, List<SmoothGoTowards> smoothedGoTowards) {
		return (smoothedGoTowards.size() - 1) / (originalGoTowards.size() - 1);
	}

	protected boolean crossDateLine(List<GoTowards> ljvp) {
		for (int i = 0; i < ljvp.size() - 1; i++) {
			GoTowards jvp1 = ljvp.get(i);
			GoTowards jvp2 = ljvp.get(i + 1);
			if (crossDateLine(jvp1, jvp2)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Split the journey view point list alongside the anti-meridian ( date line )
	 */
	protected List<List<GoTowards>> repeatLocationsAroundDateline(List<GoTowards> locations) {
		List<List<GoTowards>> splitJvpListList = new ArrayList<>();
		List<GoTowards> sectJvpList = new ArrayList<>();
		for (int i = 0; i < locations.size(); i++) {
			GoTowards jvp1 = locations.get(i);
			if (i < locations.size() - 1) {
				GoTowards jvp2 = locations.get(i + 1);
				if (crossDateLine(jvp1, jvp2)) {
					Pair<GoTowards, GoTowards> dateLineJvp = getDateLineJourneyPointIntersection(jvp1, jvp2);

					sectJvpList.add(dateLineJvp.getFirst());
					splitJvpListList.add(sectJvpList);
					sectJvpList = new ArrayList<>();
					sectJvpList.add(dateLineJvp.getSecond());
				} else {
					sectJvpList.add(jvp1);
				}
			} else {
				sectJvpList.add(jvp1);
			}
		}
		splitJvpListList.add(sectJvpList);
		return splitJvpListList;

	}

	/**
	 * Fuse a list of list, considering that each first element of a list is identical to the last one of the previous
	 * list
	 */
	protected List<GoTowards> fuse(List<List<GoTowards>> splitedList) {

		List<GoTowards> fusedList = new ArrayList<>();
		for (int i = 0; i < splitedList.size(); i++) {
			int a = 1;
			if (i == 0) {
				a = 0;
			}
			for (int j = a; j < splitedList.get(i).size(); j++) {
				fusedList.add(splitedList.get(i).get(j));
			}
		}

		return fusedList;
	}

	protected boolean crossDateLine(GoTowards goTowards1, GoTowards goTowards2) {

		// A segment cross the line if end pos have different longitude signs
		// and are more than 180 degrees longitude apart
		JourneyViewPoint jvp1 = goTowards1.getJourneyViewPoint();
		JourneyViewPoint jvp2 = goTowards2.getJourneyViewPoint();
		if (Math.signum(jvp1.getLongitudeDegrees()) != Math.signum(jvp2.getLongitudeDegrees())) {
			double delta = Math.abs(jvp1.getLongitudeDegrees() - jvp2.getLongitudeDegrees());
			if (delta > 180 && delta < 360) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the interpolated journey points on each side of the crossing of the date line / anti-meridian
	 */
	protected Pair<GoTowards, GoTowards> getDateLineJourneyPointIntersection(GoTowards goTowards1,
			GoTowards goTowards2) {

		JourneyViewPoint jvp1 = goTowards1.getJourneyViewPoint();
		JourneyViewPoint jvp2 = goTowards2.getJourneyViewPoint();
		// y = mx + b case after normalizing negative angles.
		// retrieve eye latitude on [jvp1,jvp2] intersection with the anti-meridian.
		double eyeLon1 = jvp1.getLongitudeDegrees() < 0 ? jvp1.getLongitudeDegrees() + 360 : jvp1.getLongitudeDegrees();
		double eyeLon2 = jvp2.getLongitudeDegrees() < 0 ? jvp2.getLongitudeDegrees() + 360 : jvp2.getLongitudeDegrees();

		double med = 180;
		double slope = (jvp2.getLatitudeDegrees() - jvp1.getLatitudeDegrees()) / (eyeLon2 - eyeLon1);
		double eyeLat = jvp1.getLatitudeDegrees() + slope * (med - eyeLon1);

		// checking anti-meridian crossing direction, so that we can put each point on their respective side
		int sign = 0;
		if (jvp1.getLongitudeDegrees() < 0) {
			sign = -1;
		} else {
			sign = 1;
		}
		// getting the interpolation ratio based on ellipsoidal distance between the two journey points
		double dist1 = LatLon.ellipsoidalDistance(new LatLon(jvp1.getLatitude(), jvp1.getLongitude()),
				new LatLon(jvp2.getLatitude(), jvp2.getLongitude()), Earth.WGS84_EQUATORIAL_RADIUS,
				Earth.WGS84_POLAR_RADIUS);

		double distT = LatLon.ellipsoidalDistance(new LatLon(jvp1.getLatitude(), jvp1.getLongitude()),
				LatLon.fromDegrees(eyeLat, med), Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS);
		double ratio = dist1 / distT;
		// computing interpolation for all other view points paramaters
		double eyeAlt = jvp1.getAltitude() + ratio * (jvp1.getAltitude() - jvp2.getAltitude());

		double pitch = jvp1.getPitchDegrees() + ratio * (jvp1.getPitchDegrees() - jvp2.getPitchDegrees());
		double heading = jvp1.getHeadingDegrees() + ratio * (jvp1.getHeadingDegrees() - jvp2.getHeadingDegrees());

		JourneyViewPoint jvpI1 = new JourneyViewPoint(jvp1.getName() + " antimeridian intersection", eyeLat, sign * 180,
				eyeAlt, pitch, heading);
		JourneyViewPoint jvpI2 = new JourneyViewPoint(jvp2.getName() + " antimeridian intersection", eyeLat,
				-1 * sign * 180, eyeAlt, pitch, heading);

		GoTowards goTowards3 = new GoTowards();
		goTowards1.setJourneyViewPoint(jvpI1);
		GoTowards goTowards4 = new GoTowards();
		goTowards1.setJourneyViewPoint(jvpI2);
		Pair<GoTowards, GoTowards> pair = new Pair<>(goTowards3, goTowards4);
		return pair;
	}

	public static class SmoothGoTowards extends GoTowards {
		protected GoTowards fromOriginalGoTowards;
		protected GoTowards toOriginalGoTowards;

		/**
		 * Getter of fromOriginalGoTowards
		 */
		public GoTowards getFromOriginalGoTowards() {
			return fromOriginalGoTowards;
		}

		/**
		 * Setter of fromOriginalGoTowards
		 */
		public void setFromOriginalGoTowards(GoTowards fromOriginalGoTowards) {
			this.fromOriginalGoTowards = fromOriginalGoTowards;
		}

		/**
		 * Getter of toOriginalGoTowards
		 */
		public GoTowards getToOriginalGoTowards() {
			return toOriginalGoTowards;
		}

		/**
		 * Setter of toOriginalGoTowards
		 */
		public void setToOriginalGoTowards(GoTowards toOriginalGoTowards) {
			this.toOriginalGoTowards = toOriginalGoTowards;
		}

	}
}
