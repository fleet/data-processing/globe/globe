package fr.ifremer.globe.editor.animation.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JourneyUtils {
	public final static Logger logger = LoggerFactory.getLogger(JourneyUtils.class);
	public static final String journeyFileExt = "journey";
	public static final String fileType = "JOURNEY";
	public static final String ICON_JOURNEY_16 = "/icons/journey-16.png";

}
