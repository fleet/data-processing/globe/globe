/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.model;

import fr.ifremer.globe.editor.animation.util.AnimationUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

/***
 * Define point of views </br>
 * go methods allowing to retrieve previous view positions
 *
 * @author mmorvan
 *
 */
public class JourneyViewPoint {

	private String name;
	private Angle pitch;
	private Angle heading;
	private Position position;

	/**
	 * Constructor
	 */
	public JourneyViewPoint(String name, boolean init) {
		this.name = name;
		if (init) {
			init();
		}
	}

	/**
	 * Constructor
	 */
	public JourneyViewPoint(JourneyViewPoint pointOfView) {
		this.name = pointOfView.getName();

		this.position = new Position(pointOfView.getPosition().getLatitude(), pointOfView.getPosition().getLongitude(),
				pointOfView.getPosition().getElevation());
		this.pitch = pointOfView.getPitch();
		this.heading = pointOfView.getHeading();
	}

	/**
	 * Constructor
	 */
	public JourneyViewPoint(String name, double eyeLat, double eyeLon, double eyeAlt, double pitch, double heading) {

		this.name = name;
		this.position = Position.fromDegrees(eyeLat, eyeLon, eyeAlt);
		this.pitch = Angle.fromDegrees(pitch);
		this.heading = Angle.fromDegrees(heading);
	}

	/**
	 *
	 */
	public void init() {
		WorldWindow wwd = IGeographicViewService.grab().getWwd();
		View view = wwd.getView();
		pitch = view.getPitch();
		heading = view.getHeading();

		// In orbit view the saved position is composed of the center (=target) lat/lon and a zoom value.
		// This values are used to retrieved the position with a goTo => view.setEyePosition()
		// see BasicOrbitView.setEyePosition()
		if (view instanceof BasicOrbitView) {
			Position centerPosition = ((BasicOrbitView) view).getCenterPosition();
			double zoom = ((BasicOrbitView) view).getZoom();
			position = new Position(centerPosition.getLatitude(), centerPosition.getLongitude(), zoom);
		} else {
			position = AnimationUtils.unapplyVerticalExaggeration(view.getCurrentEyePosition());
		}
	}

	public double getPitchDegrees() {
		return pitch.getDegrees();
	}

	public double getHeadingDegrees() {
		return heading.getDegrees();
	}

	public Angle getLatitude() {
		return position.getLatitude();
	}

	public double getLatitudeDegrees() {
		return position.getLatitude().getDegrees();
	}

	public Angle getLongitude() {
		return position.getLongitude();
	}

	public double getLongitudeDegrees() {
		return position.getLongitude().getDegrees();
	}

	public double getAltitude() {
		return position.getAltitude();
	}

	public void setLatitude(double latitude) {
		this.position = new Position(Angle.fromDegrees(latitude), position.getLongitude(), position.getElevation());
	}

	public void setLongitude(double longitude) {
		this.position = new Position(position.getLatitude(), Angle.fromDegrees(longitude), position.getElevation());
	}

	public void setAltitude(double altitude) {
		this.position = new Position(position.getLatitude(), position.getLongitude(), altitude);
	}

	@Override
	public String toString() {
		return name + ":" + position.toString() + ":" + pitch + ":" + heading;
	}

	/**
	 * Getter of name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter of name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter of position
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * Setter of position
	 */
	public void setPosition(Position position) {
		this.position = position;
	}

	/**
	 * Getter of pitch
	 */
	public Angle getPitch() {
		return pitch;
	}

	/**
	 * Setter of pitch
	 */
	public void setPitch(Angle pitch) {
		this.pitch = pitch;
	}

	/**
	 * Getter of heading
	 */
	public Angle getHeading() {
		return heading;
	}

	/**
	 * Setter of heading
	 */
	public void setHeading(Angle heading) {
		this.heading = heading;
	}

}
