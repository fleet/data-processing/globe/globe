﻿/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

public class JourneyFile extends Observable {

	protected static Logger logger = LoggerFactory.getLogger(JourneyFile.class);
	/** Name of this Journey. */
	protected String name;
	/** Path of the journey file. */
	protected File path;
	protected double fit = 0;
	protected boolean capture = false;
	public boolean smooth = false;

	/***
	 * List of Points of View available for the journey
	 */
	protected List<JourneyViewPoint> pointsOfView = new ArrayList<>();

	/***
	 * List of {@link JourneyViewPoint} defining the Journey ( taken from the points of view )
	 */
	protected List<GoTowards> journeyPointsList = new ArrayList<>();
	protected List<GoTowards> smoothedJourneyPointsList = new ArrayList<>();

	/**
	 * Constructor.
	 */
	public JourneyFile(String name, File path) {
		this.name = name;
		this.path = path;
		this.pointsOfView = new ArrayList<>();
		this.journeyPointsList = new ArrayList<>();
	}

	/***
	 * Journey defined by a list of {@link JourneyViewPoint} Delays define the time spent at {@link JourneyViewPoint}
	 */
	public JourneyFile(String name, File path, List<JourneyViewPoint> pointsOfView, List<GoTowards> journeyPointsList,
			boolean smooth, double fit) {
		this.pointsOfView = pointsOfView;
		this.name = name;
		this.path = path;
		this.journeyPointsList = journeyPointsList;
		this.smooth = smooth;
		this.fit = fit;
	}

	public void displayCurrentCameraValue() {

		BasicOrbitView view = (BasicOrbitView) IGeographicViewService.grab().getWwd().getView();
		logger.debug("Eye    pos   : " + view.getEyePosition().toString());
		logger.debug("Center pos   : " + view.getCenterPosition().toString());
	}

	/**
	 * @param pointOfView
	 */
	public void addPointOfView(JourneyViewPoint pointOfView) {
		pointsOfView.add(pointOfView);
	}

	/***
	 * Remove a {@link JourneyViewPoint} and the corresponding delay
	 *
	 * @param index
	 */
	public boolean removePointOfView(int index) {
		JourneyViewPoint journeyViewPoint = this.pointsOfView.get(index);
		for (GoTowards goTowards : journeyPointsList) {
			if (goTowards.getJourneyViewPoint() == journeyViewPoint) {
				return false;
			}
		}

		this.pointsOfView.remove(index);
		return true;
	}

	/**
	 * @return smoothed positions if smooth is on, regular position otherwise
	 */
	public List<Position> getJourneyPositionToDisplay() {
		var pointList = smooth ? smoothedJourneyPointsList : journeyPointsList;
		return pointList.stream().map(point -> point.getJourneyViewPoint().getPosition()).collect(Collectors.toList());
	}

	/**
	 * @return
	 */
	public List<JourneyViewPoint> getPointsOfView() {
		return pointsOfView;
	}

	/**
	 * @param pov
	 */
	public void updatePointsOfView(List<JourneyViewPoint> newPointsOfView) {
		for (int i = 0; i < Math.min(pointsOfView.size(), newPointsOfView.size()); i++) {
			pointsOfView.get(i).setPosition(newPointsOfView.get(i).getPosition());
			pointsOfView.get(i).setHeading(newPointsOfView.get(i).getHeading());
			pointsOfView.get(i).setPitch(newPointsOfView.get(i).getPitch());
		}
	}

	/***
	 *
	 * @return Journey's name as displayed in UI
	 */
	public String getName() {
		return name;
	}

	/***
	 *
	 * @return list of {@link JourneyViewPoint}
	 */
	public List<GoTowards> getJourneyPointsListToDisplay() {
		if (smooth) {
			return smoothedJourneyPointsList;
		} else {
			return journeyPointsList;
		}
	}

	/***
	 *
	 * @return list of {@link JourneyViewPoint}
	 */
	public List<GoTowards> getJourneyPointsList() {
		return journeyPointsList;
	}

	/**
	 * @param jrnPntList
	 */
	public void setJourneyPointsList(List<GoTowards> jrnPntList) {
		journeyPointsList = jrnPntList;
	}

	public double getFit() {
		return fit;
	}

	public void setFit(double value) {
		this.fit = value;
	}

	public boolean getSmooth() {
		return smooth;
	}

	public void setSmooth(boolean value) {
		this.smooth = value;
	}

	public synchronized boolean getCapture() {
		return this.capture;
	}

	public void setCapture(boolean capture) {
		synchronized (this) {
			this.capture = capture;
		}
		setChanged();
		notifyObservers();
	}

	/**
	 * Getter of path
	 */
	public File getPath() {
		return path;
	}

	/**
	 * Setter of path
	 */
	public void setPath(File path) {
		this.path = path;
	}

	/**
	 * Getter of smoothedJourneyPointsList
	 */
	public List<GoTowards> getSmoothedJourneyPointsList() {
		return smoothedJourneyPointsList;
	}

	/**
	 * Setter of smoothedJourneyPointsList
	 */
	public void setSmoothedJourneyPointsList(List<GoTowards> smoothedJourneyPointsList) {
		this.smoothedJourneyPointsList = smoothedJourneyPointsList;
	}

	/**
	 * Setter of name
	 */
	public void setName(String name) {
		this.name = name;
	}

}
