/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.model;

/**
 * Simple Pojo to hold parameter of a movement towards a ViewPoint
 */
public class GoTowards {

	/** ViewPoint to reach */
	protected JourneyViewPoint journeyViewPoint = null;
	/** Waiting time before jumping to the next point */
	protected float delay = 0f;
	/** Time in seconds of the movement before reaching the JourneyViewPoint */
	protected float movingTime = 1f;
	/** True when joining the next point in a tangent way */
	protected boolean tangent = false;

	/**
	 * Constructor
	 */
	public GoTowards() {
	}

	/**
	 * Constructor
	 */
	public GoTowards(JourneyViewPoint journeyViewPoint, float delay, float movingTime, boolean tangent) {
		this.journeyViewPoint = journeyViewPoint;
		this.delay = delay;
		this.movingTime = movingTime;
		this.tangent = tangent;
	}

	/**
	 * Constructor
	 */
	public GoTowards(GoTowards goTowards) {
		this.journeyViewPoint = goTowards.journeyViewPoint;
		this.delay = goTowards.delay;
		this.movingTime = goTowards.movingTime;
		this.tangent = goTowards.tangent;
	}

	/**
	 * Getter of journeyViewPoint
	 */
	public JourneyViewPoint getJourneyViewPoint() {
		return journeyViewPoint;
	}

	/**
	 * Setter of journeyViewPoint
	 */
	public void setJourneyViewPoint(JourneyViewPoint journeyViewPoint) {
		this.journeyViewPoint = journeyViewPoint;
	}

	/**
	 * Getter of delay
	 */
	public float getDelay() {
		return delay;
	}

	/**
	 * Setter of delay
	 */
	public void setDelay(float delay) {
		this.delay = delay;
	}

	/**
	 * Getter of movingTime
	 */
	public float getMovingTime() {
		return movingTime;
	}

	/**
	 * Setter of movingTime
	 */
	public void setMovingTime(float movingTime) {
		this.movingTime = movingTime;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return journeyViewPoint.hashCode();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj instanceof GoTowards) {
			return journeyViewPoint.equals(((GoTowards) obj).journeyViewPoint);
		}
		return super.equals(obj);
	}

	/**
	 * Getter of tangent
	 */
	public boolean isTangent() {
		return tangent;
	}

	/**
	 * Setter of tangent
	 */
	public void setTangent(boolean tangent) {
		this.tangent = tangent;
	}
}
