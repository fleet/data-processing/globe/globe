package fr.ifremer.globe.editor.animation.info;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.util.JourneyUtils;

/**
 * IInfoStore dedicated to Journey file.
 */
public class JourneyInfo extends BasicFileInfo {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(JourneyInfo.class);

	/** Journey file data */
	protected JourneyFile journeyFile;

	/**
	 * Constructor
	 */
	public JourneyInfo(String filepath, JourneyFile journeyFile) {
		super(filepath, ContentType.JOURNEY);
		this.journeyFile = journeyFile;
	}

	/**
	 * @see fr.ifremer.globe.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = super.getProperties();
		result.add(Property.build("File type", JourneyUtils.fileType));
		return result;
	}

	/**
	 * @return the {@link #journeyFile}
	 */
	public JourneyFile getJourneyFile() {
		return journeyFile;
	}

}