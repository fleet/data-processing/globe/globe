/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.player;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import javax.swing.SwingUtilities;

import fr.ifremer.globe.editor.animation.application.context.ContextNames;
import fr.ifremer.globe.editor.animation.model.GoTowards;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.model.JourneyViewPoint;
import fr.ifremer.globe.editor.animation.recorder.VideoRecorder;
import fr.ifremer.globe.editor.animation.util.AnimationUtils;
import fr.ifremer.globe.ffmpeg.FfmpegUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Angle;

/**
 *
 */
public class JourneyPlayer implements PropertyChangeListener {

	protected AtomicReference<JourneyFile> journeyFile = new AtomicReference<>();
	protected boolean loop = false;

	/** Current JourneyAnimator animating the View */
	protected JourneyAnimator journeyAnimator;

	@Inject
	@Named(ContextNames.VIDEO_RECORDER)
	protected VideoRecorder videoRecorder;

	/**
	 * Constructor
	 */
	public JourneyPlayer() {
	}

	/**
	 * Start this journey
	 */
	public void startJourney(JourneyFile journeyFile, boolean recording) {
		WorldWindow wwd = IGeographicViewService.grab().getWwd();
		if (wwd != null && journeyFile != null && this.journeyFile.compareAndSet(null, journeyFile)) {
			// JourneyUtils.switchToFlyView();

			View view = wwd.getView();
			List<GoTowards> allGoTowards = journeyFile.getSmooth() ? journeyFile.getSmoothedJourneyPointsList()
					: journeyFile.getJourneyPointsList();
			if (allGoTowards.isEmpty())
				return;

			// useledd? TODO to remove ??
			// SwingUtilities.invokeLater(() -> goTo(allGoTowards.get(0)));

			// Recording ?
			if (recording)
				videoRecorder.startRecording(wwd);

			long timeToMove = computeTimeToMove(allGoTowards);
			journeyAnimator = new JourneyAnimator(view, timeToMove, allGoTowards);
			view.addAnimator(journeyAnimator);
			view.addPropertyChangeListener(AVKey.VIEW, this);
			SwingUtilities.invokeLater(() -> view.firePropertyChange(AVKey.VIEW, null, view));
		}
	}

	/**
	 * Start this journey
	 */
	public void goTo(JourneyViewPoint journeyViewPoint) {
		if (journeyViewPoint != null && this.journeyFile.get() == null) {
			WorldWindow wwd = IGeographicViewService.grab().getWwd();
			if (wwd != null) {
				// JourneyUtils.switchToFlyView();
				View view = wwd.getView();
				view.setEyePosition(AnimationUtils.applyVerticalExaggeration(journeyViewPoint.getPosition()));
				view.setHeading(journeyViewPoint.getHeading());
				view.setPitch(journeyViewPoint.getPitch());
				view.setRoll(Angle.ZERO);
				SwingUtilities.invokeLater(() -> wwd.redraw());
			}
		}
	}

	public void goTo(GoTowards goTowards) {
		if (goTowards != null) {
			goTo(goTowards.getJourneyViewPoint());
		}
	}

	/**
	 * Stop currently started journey
	 */
	public void stopJourney() {
		JourneyFile journeyFile = this.journeyFile.getAndSet(null);
		if (journeyFile != null) {
			WorldWindowGLCanvas wwd = IGeographicViewService.grab().getWwd();
			if (wwd != null) {
				View view = wwd.getView();
				view.stopMovement();
				view.removePropertyChangeListener(AVKey.VIEW, this);
				if (videoRecorder.isRecording()) {
					videoRecorder.stopRecording();
					FfmpegUtils.encodeVideo(journeyFile.getName(), videoRecorder.getVideoCapturePath(),
							videoRecorder.getFramerate());
				}
			}
		}
	}

	/**
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		View view = (View) evt.getNewValue();
		if (!view.isAnimating()) {
			if (loop) {
				restartJourney();
			} else {
				stopJourney();
			}
		}
	}

	protected void restartJourney() {
		WorldWindow wwd = IGeographicViewService.grab().getWwd();
		if (wwd != null && this.journeyFile.get() != null && journeyAnimator != null) {
			startJourney(this.journeyFile.getAndSet(null), false);
		}
	}

	protected long computeTimeToMove(List<GoTowards> allGoTowards) {
		long timeToMove = 0;
		for (GoTowards goTowards : allGoTowards) {
			timeToMove += goTowards.getDelay() * 1000f + Math.max(1f, goTowards.getMovingTime()) * 1000f;
		}
		return timeToMove;
	}

	/**
	 * Getter of loop
	 */
	public boolean isLoop() {
		return loop;
	}

	/**
	 * Setter of loop
	 */
	public void setLoop(boolean loop) {
		this.loop = loop;
	}

	/**
	 * Getter of videoRecorder
	 */
	public VideoRecorder getVideoRecorder() {
		return videoRecorder;
	}

}
