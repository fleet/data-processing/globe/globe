package fr.ifremer.globe.editor.animation.player;

import java.util.List;

import fr.ifremer.globe.editor.animation.model.GoTowards;
import fr.ifremer.globe.editor.animation.model.JourneyViewPoint;
import fr.ifremer.globe.editor.animation.util.AnimationUtils;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.animation.AngleAnimator;
import gov.nasa.worldwind.animation.CompoundAnimator;
import gov.nasa.worldwind.animation.PositionAnimator;
import gov.nasa.worldwind.animation.ScheduledInterpolator;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.util.PropertyAccessor.AngleAccessor;
import gov.nasa.worldwind.view.ViewPropertyAccessor;
import gov.nasa.worldwind.view.ViewUtil;

public class JourneyAnimator extends CompoundAnimator {

	protected List<GoTowards> allGoTowards;

	protected PositionAnimator eyePosAnimator;
	protected BehaviorAnimator headingAnimator;
	protected BehaviorAnimator pitchAnimator;
	protected AngleAnimator rollAnimator;

	protected double[] sequence;
	protected int currentSequenceIndex = 0;
	protected double[] waitUntil;

	/**
	 * Constructor
	 */
	public JourneyAnimator(View view, long timeToMove, List<GoTowards> allGoTowards) {
		super(new ScheduledInterpolator(timeToMove));
		this.allGoTowards = allGoTowards;

		computeSequence(timeToMove);
		this.eyePosAnimator = ViewUtil.createEyePositionAnimator(view, timeToMove, view.getCurrentEyePosition(),
				view.getCurrentEyePosition());
		this.headingAnimator = new BehaviorAnimator(10d, view.getHeading(), view.getHeading(),
				ViewPropertyAccessor.createHeadingAccessor(view));
		this.pitchAnimator = new BehaviorAnimator(10d, view.getPitch(), view.getPitch(),
				ViewPropertyAccessor.createPitchAccessor(view));
		this.rollAnimator = new AngleAnimator(null, Angle.ZERO, Angle.ZERO,
				ViewPropertyAccessor.createRollAccessor(view));
		prepareAnimators();
		setAnimators(this.eyePosAnimator, this.headingAnimator, this.pitchAnimator, this.rollAnimator);
	}

	protected void computeSequence(long timeToMove) {
		this.sequence = new double[allGoTowards.size()];
		this.waitUntil = new double[allGoTowards.size()];
		long timeOfSequence = 0l;
		int goTowardsIndex = 0;
		for (int sequenceIndex = 0; sequenceIndex < this.sequence.length; sequenceIndex++) {
			this.sequence[sequenceIndex] = (double) timeOfSequence / timeToMove;
			GoTowards goTowards = allGoTowards.get(goTowardsIndex);
			this.waitUntil[sequenceIndex] = (double) (timeOfSequence + goTowards.getDelay() * 1000) / timeToMove;
			timeOfSequence += goTowards.getDelay() * 1000 + Math.max(goTowards.getMovingTime(), 1f) * 1000;
			goTowardsIndex++;
		}
	}

	/**
	 * @see gov.nasa.worldwind.animation.CompoundAnimator#setImpl(double)
	 */
	@Override
	protected void setImpl(double interpolant) {
		// must consider the next sequence ?
		int nextSequenceIndex = currentSequenceIndex + 1;
		if (nextSequenceIndex + 1 < sequence.length && sequence[nextSequenceIndex] <= interpolant) {
			currentSequenceIndex = nextSequenceIndex;
			prepareAnimators();
		}

		// Wait ?
		if (Double.isNaN(interpolant) || interpolant < waitUntil[currentSequenceIndex]) {
			return;
		}

		// finish ?
		if (interpolant >= 1d || currentSequenceIndex + 1 >= allGoTowards.size()) {
			stop();
			return;
		}

		double movingTime = this.sequence[currentSequenceIndex + 1] - this.waitUntil[currentSequenceIndex];
		super.setImpl(1d - ((this.sequence[currentSequenceIndex + 1] - interpolant) / movingTime));
	}

	protected void prepareAnimators() {
		GoTowards fromGoTowards = allGoTowards.get(currentSequenceIndex);
		boolean tangent = fromGoTowards.isTangent();
		GoTowards toGoTowards = allGoTowards.get(currentSequenceIndex + 1);

		JourneyViewPoint fromJourneyViewPoint = fromGoTowards.getJourneyViewPoint();
		JourneyViewPoint toJourneyViewPoint = toGoTowards.getJourneyViewPoint();

		// Eye Position
		Position fromPosition = AnimationUtils.applyVerticalExaggeration(fromJourneyViewPoint.getPosition());
		Position toPosition = AnimationUtils.applyVerticalExaggeration(toJourneyViewPoint.getPosition());
		eyePosAnimator.setBegin(fromPosition);
		eyePosAnimator.setEnd(toPosition);
		eyePosAnimator.start();

		// Heading
		Angle fromHeading = tangent && currentSequenceIndex > 0 ? headingAnimator.getEnd()
				: fromJourneyViewPoint.getHeading();
		Angle toHeading = toJourneyViewPoint.getHeading();
		if (tangent) {
			Angle computedToHeading = LatLon.greatCircleAzimuth(fromPosition, toPosition);
			toHeading = Double.isNaN(computedToHeading.getDegrees()) ? toHeading : computedToHeading;
		}
		headingAnimator.setBegin(fromHeading);
		headingAnimator.setEnd(toHeading);
		headingAnimator.setRatio(
				tangent && toGoTowards.getMovingTime() >= 1f ? toGoTowards.getMovingTime() * 1000f / 250f : 1d);
		headingAnimator.start();

		// Pitch
		Angle fromPitch = tangent && currentSequenceIndex > 0 ? pitchAnimator.getEnd()
				: fromJourneyViewPoint.getPitch();
		Angle toPitch = toJourneyViewPoint.getPitch();
		if (tangent) {
			double distance = LatLon.linearDistance(fromPosition, toPosition).radians * Earth.WGS84_EQUATORIAL_RADIUS;
			Angle computedToPitch = Angle.POS90
					.subtract(Angle.atan((fromPosition.elevation - toPosition.elevation) / distance));
			toPitch = Double.isNaN(computedToPitch.getDegrees()) ? toPitch : computedToPitch;
		}
		pitchAnimator.setBegin(fromPitch);
		pitchAnimator.setEnd(toPitch);
		pitchAnimator.setRatio(
				tangent && toGoTowards.getMovingTime() >= 1f ? toGoTowards.getMovingTime() * 1000f / 250f : 1d);
		pitchAnimator.start();

	}

	static public class BehaviorAnimator extends AngleAnimator {
		/**
		 * The angle the animation begins at.
		 */
		protected double ratio;

		public BehaviorAnimator(double ratio, Angle begin, Angle end, AngleAccessor propertyAccessor) {
			super(null, begin, end, propertyAccessor);
			this.ratio = ratio;
		}

		@Override
		protected void setImpl(double interpolant) {
			super.setImpl(Math.min(interpolant * ratio, 1d));
		}

		/**
		 * Getter of ratio
		 */
		public double getRatio() {
			return ratio;
		}

		/**
		 * Setter of ratio
		 */
		public void setRatio(double ratio) {
			this.ratio = ratio;
		}

	}
}
