/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.services.layer;

import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.editor.animation.info.JourneyInfo;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.services.info.JourneyInfoLoader;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Service providing Nasa layer for any Gdal Raster file
 */
@Component(name = "globe_animation_layer_provider", service = IWWLayerProvider.class)
public class JourneyLayerProvider extends BasicWWLayerProvider<JourneyInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(JourneyLayerProvider.class);

	/** Loader of journey file info */
	@Reference
	protected JourneyInfoLoader journeyInfoLoader;

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return journeyInfoLoader.getContentTypes();
	}

	/**
	 * Create the layers to represent the Journey file in the 3D Viewer
	 */
	@Override
	protected void createLayers(JourneyInfo journeyInfo, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException {
		IWWNavigationLayer layer = layerFactory
				.createNavigationLayer(accessMode -> new JourneyNavigation(journeyInfo.getJourneyFile()));
		layer.setValue(JourneyInfo.class.getName(), journeyInfo);
		layerStore.addLayers(layer);
		monitor.done();
	}

	static class JourneyNavigation implements INavigationData {

		/** Metadata **/
		private final List<NavigationMetadata<?>> metadata = List
				.of(NavigationMetadataType.ELEVATION.withValueSupplier(this::getElevation));

		JourneyFile journeyFile;

		/**
		 * Constructor
		 */
		public JourneyNavigation(JourneyFile journeyFile) {
			this.journeyFile = journeyFile;

		}

		@Override
		public int size() {
			return journeyFile.getJourneyPositionToDisplay().size();
		}

		@Override
		public String getFileName() {
			return journeyFile.getName();
		}

		@Override
		public ContentType getContentType() {
			return ContentType.JOURNEY;
		}

		@Override
		public double getLatitude(int index) throws GIOException {
			return journeyFile.getJourneyPositionToDisplay().get(index).getLatitude().degrees;
		}

		@Override
		public double getLongitude(int index) throws GIOException {
			return journeyFile.getJourneyPositionToDisplay().get(index).getLongitude().degrees;
		}

		@Override
		public List<NavigationMetadata<?>> getMetadata() {
			return metadata;
		}

		private float getElevation(int index) throws GIOException {
			// approximation of elevation : the altitude property of journey position is the "zoom" value and not the
			// true altitude : see JourneyViewPoint init()
			// TODO : get true elevation value from JourneyViewPoint
			return (float) journeyFile.getJourneyPositionToDisplay().get(index).getAltitude() / 2.52f;
		}

	}

}