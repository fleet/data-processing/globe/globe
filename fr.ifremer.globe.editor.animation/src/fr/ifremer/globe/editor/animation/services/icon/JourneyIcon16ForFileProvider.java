/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.services.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.editor.animation.util.JourneyUtils;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a Gdal file.
 */
@Component(name = "globe_animation_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class JourneyIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public JourneyIcon16ForFileProvider() {
		super(JourneyUtils.ICON_JOURNEY_16, ContentType.JOURNEY);
	}

}