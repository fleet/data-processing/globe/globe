package fr.ifremer.globe.editor.animation.services.info;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.DomDriver;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.editor.animation.info.JourneyInfo;
import fr.ifremer.globe.editor.animation.model.GoTowards;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.model.JourneyViewPoint;
import fr.ifremer.globe.editor.animation.services.info.JourneyInfoLoader.FileData.GoTowardsData;
import fr.ifremer.globe.editor.animation.services.info.JourneyInfoLoader.FileData.JourneyData;
import fr.ifremer.globe.editor.animation.services.info.JourneyInfoLoader.FileData.JourneysData;
import fr.ifremer.globe.editor.animation.services.info.JourneyInfoLoader.FileData.ViewPointData;
import fr.ifremer.globe.editor.animation.util.JourneyUtils;
import fr.ifremer.globe.editor.animation.util.SmoothJourneyComputer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Loader of Gdal file info. Builder of GdalRasterInfo
 */
@Component(name = "globe_animation_journey_info_supplier", service = { IFileInfoSupplier.class,
		JourneyInfoLoader.class })
public class JourneyInfoLoader extends BasicFileInfoSupplier<JourneyInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(JourneyInfoLoader.class);

	/**
	 * Constructor
	 */
	public JourneyInfoLoader() {
		super(JourneyUtils.journeyFileExt, "Globe journey file", ContentType.JOURNEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JourneyInfo makeFileInfo(String filePath) {
		JourneyInfo result = null;
		try {
			result = new JourneyInfo(filePath, load(filePath));
		} catch (GIOException e) {
			logger.warn("Error on {} loading : {}", filePath, e.getMessage());
		}
		return result;
	}

	/***
	 * Load JourneyFile from .journey xml file
	 */
	public JourneyFile load(String path) throws GIOException {
		logger.info("Loading JourneyFile " + path);
		XStream xstream = configureXstream();

		File fileToLoad = new File(path);
		FileData fileData = null;
		try {
			fileData = (FileData) xstream.fromXML(fileToLoad);
		} catch (Exception e) {
			throw new GIOException("Error while loading JourneyFile " + fileToLoad.getName(), e);
		}
		String journeyName = FilenameUtils.removeExtension(fileToLoad.getName());

		JourneyFile journeyFile = new JourneyFile(journeyName, fileToLoad);
		journeyFile.setSmooth(fileData.journeysData.journeyData.smooth);
		journeyFile.setFit(fileData.journeysData.journeyData.fit);
		if (fileData.viewPoints != null) {
			for (ViewPointData viewPointData : fileData.viewPoints) {
				JourneyViewPoint journeyViewPoint = new JourneyViewPoint(viewPointData.name, viewPointData.eyeLatitude,
						viewPointData.eyeLongitude, viewPointData.eyeAltitude, viewPointData.pitch,
						viewPointData.heading);
				journeyFile.getPointsOfView().add(journeyViewPoint);
			}
		}
		if (fileData.journeysData.journeyData.viewPoints != null) {
			for (GoTowardsData goTowardsData : fileData.journeysData.journeyData.viewPoints) {
				JourneyViewPoint journeyViewPoint = journeyFile.getPointsOfView().get(goTowardsData.index);
				GoTowards goTowards = new GoTowards(journeyViewPoint, goTowardsData.delay, goTowardsData.movingTime,
						goTowardsData.tangent);
				journeyFile.getJourneyPointsList().add(goTowards);
			}
		}

		if (journeyFile.getSmooth()) {
			SmoothJourneyComputer smoothJourneyComputer = new SmoothJourneyComputer();
			smoothJourneyComputer.smooth(journeyFile);
		}

		return journeyFile;

	}

	/***
	 * Save JourneyFile to .journey xml file
	 *
	 * @param path
	 */
	public void save(JourneyFile journeyFile) {
		logger.info("Saving JourneyFile " + journeyFile.getPath().getName());

		FileData fileData = new FileData();
		fileData.journeysData = new JourneysData();
		fileData.journeysData.journeyData = new JourneyData();
		fileData.journeysData.journeyData.smooth = journeyFile.getSmooth();
		fileData.journeysData.journeyData.fit = journeyFile.getFit();

		for (JourneyViewPoint journeyViewPoint : journeyFile.getPointsOfView()) {
			ViewPointData viewPointData = new ViewPointData();
			viewPointData.name = journeyViewPoint.getName();
			viewPointData.eyeLatitude = journeyViewPoint.getLatitudeDegrees();
			viewPointData.eyeLongitude = journeyViewPoint.getLongitudeDegrees();
			viewPointData.eyeAltitude = journeyViewPoint.getAltitude();
			viewPointData.heading = journeyViewPoint.getHeadingDegrees();
			viewPointData.pitch = journeyViewPoint.getPitchDegrees();
			fileData.viewPoints.add(viewPointData);
		}

		for (GoTowards goTowards : journeyFile.getJourneyPointsList()) {
			GoTowardsData goTowardsData = new GoTowardsData();
			goTowardsData.name = goTowards.getJourneyViewPoint().getName();
			goTowardsData.index = journeyFile.getPointsOfView().indexOf(goTowards.getJourneyViewPoint());
			goTowardsData.delay = goTowards.getDelay();
			goTowardsData.movingTime = goTowards.getMovingTime();
			goTowardsData.tangent = goTowards.isTangent();
			fileData.journeysData.journeyData.viewPoints.add(goTowardsData);
		}

		XStream xstream = configureXstream();
		try (FileOutputStream fileOutputStream = new FileOutputStream(journeyFile.getPath())) {
			fileOutputStream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".getBytes("UTF-8"));
			xstream.toXML(fileData, fileOutputStream);
		} catch (IOException e) {
			logger.warn("Error while saving JourneyFile " + journeyFile.getPath().getName(), e);
		}
	}

	protected XStream configureXstream() {
		XStream xstream = new XStream(new DomDriver());
		xstream.setClassLoader(getClass().getClassLoader());
		xstream.processAnnotations(FileData.class);
		xstream.processAnnotations(JourneysData.class);
		xstream.processAnnotations(JourneyData.class);
		xstream.processAnnotations(ViewPointData.class);
		xstream.addImplicitCollection(JourneyData.class, "viewPoints", "ViewPoint", GoTowardsData.class);
		xstream.ignoreUnknownElements();
		xstream.allowTypeHierarchy(JourneyInfoLoader.class);
		xstream.allowTypesByWildcard(new String[] { JourneyInfoLoader.class.getPackageName() + ".*" });
		return xstream;
	}

	@XStreamAlias("Data")
	static class FileData {
		@XStreamAlias("PointsOfView")
		public List<ViewPointData> viewPoints = new ArrayList<>();
		@XStreamAlias("Journeys")
		public JourneysData journeysData;

		static class JourneysData {
			@XStreamAlias("Journey")
			public JourneyData journeyData;
		}

		static class JourneyData {
			@XStreamAlias("Fit")
			public double fit = 0;
			@XStreamAlias("Smooth")
			public boolean smooth = false;
			public List<GoTowardsData> viewPoints = new ArrayList<>();
		}

		@XStreamAlias("ViewPoint")
		static class ViewPointData {
			@XStreamAlias("Name")
			public String name;
			@XStreamAlias("EyeLatitude")
			private double eyeLatitude;
			@XStreamAlias("EyeLongitude")
			private double eyeLongitude;
			@XStreamAlias("EyeAltitude")
			private double eyeAltitude;
			@XStreamAlias("Roll")
			private double roll;
			@XStreamAlias("Pitch")
			private double pitch;
			@XStreamAlias("Heading")
			private double heading;
		}

		static class GoTowardsData {
			@XStreamAlias("Name")
			public String name;
			@XStreamAlias("Index")
			public int index = 0;
			@XStreamAlias("Delay")
			public float delay = 0f;
			@XStreamAlias("MovingTime")
			public float movingTime = 1f;
			@XStreamAlias("Tangent")
			public boolean tangent = false;
		}
	}

}