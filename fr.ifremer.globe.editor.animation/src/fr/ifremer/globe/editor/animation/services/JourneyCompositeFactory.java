/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.services;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.editor.animation.info.JourneyInfo;
import fr.ifremer.globe.editor.animation.ui.parameter.JourneyComposite;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Provide the parameter Composite of a ShpLayer
 */
@Component(name = "globe_animation_journey_composite_factory", service = IParametersViewCompositeFactory.class)
public class JourneyCompositeFactory implements IParametersViewCompositeFactory {

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof FileInfoNode fileInfoNode
				&& fileInfoNode.getFileInfo() instanceof JourneyInfo journeyInfo
						? Optional.of(new JourneyComposite(parent, journeyInfo))
						: Optional.empty();
	}

}
