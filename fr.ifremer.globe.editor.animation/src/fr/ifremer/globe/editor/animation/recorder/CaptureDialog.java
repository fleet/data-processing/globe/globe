package fr.ifremer.globe.editor.animation.recorder;

import java.io.File;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ffmpeg.model.Framerate;
import fr.ifremer.globe.ui.utils.Messages;

public class CaptureDialog extends Dialog {
	protected VideoRecorder videoRecorder;

	protected Text videoPathText;
	protected ComboViewer inputFramerateCombo;

	public CaptureDialog(Shell parentShell, VideoRecorder videoRecorder) {
		super(parentShell);
		this.videoRecorder = videoRecorder;
	}

	/**
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Capture settings");
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		File videoCapturePath = !videoPathText.getText().isEmpty() ? new File(videoPathText.getText()) : null;
		// if path or file name are invalid
		if (videoCapturePath == null || !videoCapturePath.isDirectory()) {
			Messages.openToolTip(videoPathText, "Invalid path");
		} else {
			// Allow the dialog to close
			videoRecorder.setVideoCapturePath(videoCapturePath);
			videoRecorder.setFramerate(
					(Framerate) ((IStructuredSelection) inputFramerateCombo.getSelection()).getFirstElement());
			super.okPressed();
		}
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite result = (Composite) super.createDialogArea(parent);
		Composite innerComposite = new Composite(result, SWT.None);
		innerComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		innerComposite.setLayout(new GridLayout(4, false));
		// The text fields will grow with the size of the dialog
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;

		// dir browser for the path of the video
		Label lab = new Label(innerComposite, SWT.NONE);
		lab.setText("Images directory :");
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		data.horizontalSpan = 1;
		lab.setLayoutData(data);

		videoPathText = new Text(innerComposite, SWT.BORDER);
		data = new GridData(GridData.FILL_HORIZONTAL);
		videoPathText.setLayoutData(data);
		videoPathText.setText(getVideoCapturePath() != null ? getVideoCapturePath().getAbsolutePath() : "");
		videoPathText.setEditable(false);

		Button browseButton = new Button(innerComposite, SWT.PUSH);
		browseButton.setText("Browse...");
		data = new GridData(GridData.HORIZONTAL_ALIGN_END);
		data.horizontalSpan = 2;
		browseButton.setLayoutData(data);
		browseButton.addListener(SWT.Selection, (event) -> {
			DirectoryDialog dlg = new DirectoryDialog(browseButton.getShell());
			if (getVideoCapturePath() != null) {
				dlg.setFilterPath(getVideoCapturePath().getAbsolutePath());
			}
			dlg.setText("Directory");
			dlg.setMessage("Select the video directory");
			String path = dlg.open();
			if (path != null) {
				videoPathText.setText(path);
			}
		});

		// input framerate spinner
		Label inFpsLab = new Label(innerComposite, SWT.NONE);
		inFpsLab.setText("Capture frequency : ");
		data = new GridData(SWT.BEGINNING);
		data.horizontalSpan = 2;

		inputFramerateCombo = new ComboViewer(innerComposite, SWT.READ_ONLY);
		inputFramerateCombo.setContentProvider(ArrayContentProvider.getInstance());
		inputFramerateCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Framerate) element).getDescription();
			}
		});
		inputFramerateCombo.setInput(Framerate.values());
		inputFramerateCombo.setSelection(new StructuredSelection(getFramerate()));

		return result;
	}

	/**
	 * Getter of videoCapturePath
	 */
	public File getVideoCapturePath() {
		return videoRecorder.getVideoCapturePath();
	}

	/**
	 * Getter of framerate
	 */
	public Framerate getFramerate() {
		return videoRecorder.getFramerate();
	}

}
