/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.recorder;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;

import fr.ifremer.globe.ffmpeg.FfmpegUtils;
import fr.ifremer.globe.ffmpeg.model.Framerate;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.BufferedImageSerializer;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.event.RenderingEvent;
import gov.nasa.worldwind.event.RenderingListener;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Get snapshots of NWW view
 */
public class VideoRecorder implements RenderingListener {

	private static Logger logger = LoggerFactory.getLogger(VideoRecorder.class);

	protected File videoCapturePath;
	protected Framerate framerate = Framerate.T25;

	protected AtomicReference<WorldWindow> currentWorldWindow = new AtomicReference<>();
	protected volatile BufferedImage currentBufferedImage;

	protected Disposable snapshotObserver;

	public boolean isRecording() {
		return snapshotObserver != null;
	}

	public void startRecording(WorldWindow worldWindow) {

		if (currentWorldWindow.compareAndSet(null, worldWindow)) {
			worldWindow.addRenderingListener(this);
		}

		snapshotObserver = Observable.interval(this.framerate.getFps(), TimeUnit.MILLISECONDS).forEach((index) -> {
			try {
				BufferedImage bufferedImage = currentBufferedImage;
				if (bufferedImage != null) {
					BufferedImageSerializer.write(bufferedImage,
							FfmpegUtils.getRawFile(this.videoCapturePath, index.intValue()));
				}
			} catch (IOException e) {
				logger.warn("Video recording error", e);
				Messages.openErrorMessage("Video recording error",
						String.format("Video recording failed (%s)", e.getMessage()));
				snapshotObserver.dispose();
			}
		});

		emitVideoRecorderEvent();
	}

	public void stopRecording() {
		WorldWindow worldWindow = currentWorldWindow.getAndSet(null);
		if (worldWindow != null) {
			worldWindow.removeRenderingListener(this);

			snapshotObserver.dispose();
			snapshotObserver = null;

			currentWorldWindow.set(null);
			currentBufferedImage = null;

			emitVideoRecorderEvent();
		}
	}

	/**
	 * @see gov.nasa.worldwind.event.RenderingListener#stageChanged(gov.nasa.worldwind.event.RenderingEvent)
	 */
	@Override
	public void stageChanged(RenderingEvent event) {
		if (currentWorldWindow.get() != null && event.getStage().equals(RenderingEvent.AFTER_BUFFER_SWAP)) {
			GLAutoDrawable drawable = (GLAutoDrawable) event.getSource();
			AWTGLReadBufferUtil glReadBufferUtil = new AWTGLReadBufferUtil(drawable.getGLProfile(), false);
			currentBufferedImage = glReadBufferUtil.readPixelsToBufferedImage(drawable.getGL(), true);
		}
	}

	/**
	 * Getter of videoCapturePath
	 */
	public File getVideoCapturePath() {
		return videoCapturePath;
	}

	/**
	 * Setter of videoCapturePath
	 */
	public void setVideoCapturePath(File videoCapturePath) {
		this.videoCapturePath = videoCapturePath;
	}

	/**
	 * Getter of framerate
	 */
	public Framerate getFramerate() {
		return framerate;
	}

	/**
	 * Setter of framerate
	 */
	public void setFramerate(Framerate framerate) {
		this.framerate = framerate;
	}

	private void emitVideoRecorderEvent() {
		var eventBroker = ContextInitializer.getInContext(IEventBroker.class);
		eventBroker.post(VideoRecorderEvent.TOPIC, new VideoRecorderEvent(this));
	}

	/**
	 * Video recorder event
	 **/
	public static record VideoRecorderEvent(VideoRecorder videoRecorder) implements BaseEvent {
		public static final String TOPIC = "fr/ifremer/globe/editor/animation/recorder/videorecorder";
	}

}
