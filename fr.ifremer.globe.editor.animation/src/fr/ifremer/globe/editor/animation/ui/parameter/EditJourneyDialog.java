package fr.ifremer.globe.editor.animation.ui.parameter;

import java.text.DecimalFormat;
import java.util.List;

import javax.swing.SwingUtilities;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import fr.ifremer.globe.editor.animation.model.GoTowards;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.model.JourneyViewPoint;
import fr.ifremer.globe.editor.animation.util.AnimationUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.viewers.CellModifierAdapter;
import fr.ifremer.globe.ui.widget.SpinnerCellEditor;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWindow;

/***
 * Dialog used to add points of view to a journey
 *
 *
 */
public class EditJourneyDialog extends Dialog {

	/** Translated value of tangent */
	protected String tangentLabel = "Tangent";

	protected JourneyFile jrnFile;

	/***
	 * Journey's list of {@link GoTowards}
	 */
	protected List<GoTowards> goTowards;

	/***
	 *
	 */
	protected List<JourneyViewPoint> availableViewPoints;

	protected TableItem selectedItem;

	protected DecimalFormat timeFormat = new DecimalFormat();
	protected TableViewer goTowardsViewer;
	protected Spinner totalSpinner;

	/***
	 * Dialog used to define a {@link Journey}</br>
	 * A list of {@link ViewPoint} and the corresponding delays is defined</br>
	 * Those {@link ViewPoint} are selected between availablesViewPoints
	 */
	public EditJourneyDialog(Shell parentShell, JourneyFile jrnFile, List<GoTowards> editedGoTowards) {
		super(parentShell);
		this.jrnFile = jrnFile;
		this.availableViewPoints = jrnFile.getPointsOfView();
		this.goTowards = editedGoTowards;
		timeFormat.setGroupingUsed(true);
		timeFormat.setGroupingSize(3);
	}

	/**
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Edit points and associated Delays/Moving time");
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		Composite innerComposite = new Composite(composite, SWT.None);
		innerComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		innerComposite.setLayout(new GridLayout(4, false));

		createPointOfViewComposite(innerComposite);
		createTotalTimeComposite(innerComposite);
		new Label(innerComposite, SWT.NONE);
		return composite;

	}

	protected void createTotalTimeComposite(Composite parent) {
		// 2 empty cells
		new Label(parent, SWT.RIGHT);
		new Label(parent, SWT.RIGHT);

		Composite composite = new Composite(parent, SWT.None);
		composite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		composite.setLayout(new GridLayout(2, false));

		Label label = new Label(composite, SWT.RIGHT);
		label.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		label.setText("Total moving time");
		totalSpinner = new Spinner(composite, SWT.BORDER | SWT.READ_ONLY);

		totalSpinner.setTextLimit(10);
		updateTotalSpinner();
		Listener totalSpinnerListener = new Listener() {
			@Override
			public void handleEvent(Event event) {
				totalSpinner.removeListener(SWT.Verify, this);
				incrementeMovingTime();
				goTowardsViewer.getTable().getDisplay().asyncExec(() -> goTowardsViewer.refresh());
				totalSpinner.addListener(SWT.Verify, this);
			}
		};
		totalSpinner.addListener(SWT.Verify, totalSpinnerListener);
	}

	protected void incrementeMovingTime() {
		int totalMovingTime = (int) goTowards.stream().mapToDouble(GoTowards::getMovingTime).sum();
		if (totalSpinner.getSelection() != totalMovingTime) {
			float increment = totalSpinner.getSelection() > totalMovingTime ? 1f : -1f;
			float total = 0f;
			for (GoTowards oneGoTowards : goTowards) {
				oneGoTowards.setMovingTime(Math.max(Math.min(oneGoTowards.getMovingTime() + increment, 60f), 1f));
				total += oneGoTowards.getMovingTime();
			}
			int newtotalMovingTime = (int) total;
			setSelection(totalSpinner, newtotalMovingTime);
		}
	}

	protected void createPointOfViewComposite(Composite parent) {
		// Table of available ViewPoint
		TableViewer availablesTableViewer = new TableViewer(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		availablesTableViewer.setContentProvider(ArrayContentProvider.getInstance());
		availablesTableViewer.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		availablesTableViewer.getTable().setHeaderVisible(true);
		availablesTableViewer.getTable().setLinesVisible(true);

		TableViewerColumn availableTableViewerColumn = new TableViewerColumn(availablesTableViewer, SWT.NONE);
		availableTableViewerColumn.getColumn().setWidth(150);
		availableTableViewerColumn.getColumn().setText("Point of View");
		availableTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((JourneyViewPoint) element).getName();
			}
		});
		availablesTableViewer.setInput(availableViewPoints);
		availablesTableViewer.getControl().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// Button of selection
		Composite selectComposite = new Composite(parent, SWT.NONE);
		selectComposite.setLayout(new GridLayout(1, false));
		Button selectionButton = new Button(selectComposite, SWT.PUSH);
		selectionButton.setToolTipText("Add the selected points of view");
		selectionButton.setImage(ImageResources.getImage("icons/double_right.png", getClass()));

		goTowardsViewer = new TableViewer(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		goTowardsViewer.setContentProvider(ArrayContentProvider.getInstance());
		Table selectedTable = goTowardsViewer.getTable();
		selectedTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		selectedTable.setHeaderVisible(true);
		selectedTable.setLinesVisible(true);
		goTowardsViewer.addSelectionChangedListener(
				(e) -> goTo((GoTowards) ((IStructuredSelection) e.getSelection()).getFirstElement()));

		defineColumns(goTowardsViewer);

		Menu selectedTableMenu = new Menu(selectedTable);
		selectedTable.setMenu(selectedTableMenu);

		MenuItem setAllTangentSubmenu = new MenuItem(selectedTableMenu, SWT.CASCADE);
		setAllTangentSubmenu.setText("Set all tangent flags to ...");

		Menu menu_1 = new Menu(setAllTangentSubmenu);
		setAllTangentSubmenu.setMenu(menu_1);

		MenuItem setAllOnMenuItem = new MenuItem(menu_1, SWT.NONE);
		setAllOnMenuItem.setImage(ImageResources.getImage("icons/tangent.png", getClass()));
		setAllOnMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setAllTangentFlags(true);
			}
		});
		setAllOnMenuItem.setText("On");

		MenuItem setAllOffMenuItem = new MenuItem(menu_1, SWT.NONE);
		setAllOffMenuItem.setImage(ImageResources.getImage("icons/notTangent.png", getClass()));
		setAllOffMenuItem.setText("Off");
		setAllOffMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setAllTangentFlags(false);
			}
		});

		MenuItem reverseMenuItem = new MenuItem(selectedTableMenu, SWT.NONE);
		reverseMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				reverseAllTangentFlags();
			}
		});
		reverseMenuItem.setText("Reverse all tangent flags");
		goTowardsViewer.setInput(goTowards);

		// Sort Point of View
		Composite sortComposite = new Composite(parent, SWT.NONE);
		sortComposite.setLayout(new GridLayout(1, false));
		availablesTableViewer.getControl().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// DATA Edition
		// select Point of View
		selectionButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) availablesTableViewer.getSelection();
				if (!selection.isEmpty()) {
					for (Object journeyViewPointObject : selection.toList()) {
						JourneyViewPoint journeyViewPoint = (JourneyViewPoint) journeyViewPointObject;
						GoTowards newGoTowards = new GoTowards();
						newGoTowards.setJourneyViewPoint(journeyViewPoint);
						newGoTowards.setDelay(0f);
						newGoTowards.setMovingTime(1f);
						goTowards.add(newGoTowards);
					}
					goTowardsViewer.setInput(goTowards);
				}
			}
		});

		// remove button
		Button removeButton = new Button(sortComposite, SWT.PUSH);
		removeButton.setToolTipText("Remove");
		removeButton.setImage(ImageResources.getImage("icons/edit_delete.png", getClass()));
		removeButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		removeButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) goTowardsViewer.getSelection();
				if (!selection.isEmpty()) {
					goTowards.removeAll(selection.toList());
					goTowardsViewer.setInput(goTowards);
				}
			}
		});

		// top button
		Button topButton = new Button(sortComposite, SWT.PUSH);
		topButton.setToolTipText("Top");
		topButton.setImage(ImageResources.getImage("icons/double_up.png", getClass()));
		topButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		topButton.addListener(SWT.Selection, new Listener() {
			@SuppressWarnings("unchecked")
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) goTowardsViewer.getSelection();
				if (!selection.isEmpty()) {
					goTowards.removeAll(selection.toList());
					goTowards.addAll(0, selection.toList());
					goTowardsViewer.setInput(goTowards);
				}
			}
		});

		// up button
		Button upButton = new Button(sortComposite, SWT.PUSH);
		upButton.setToolTipText("Up");
		upButton.setImage(ImageResources.getImage("icons/arrow_up.png", getClass()));
		upButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		upButton.addListener(SWT.Selection, new Listener() {
			@SuppressWarnings("unchecked")
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) goTowardsViewer.getSelection();
				if (!selection.isEmpty()) {
					int selectionIndex = selectedTable.getSelectionIndex();
					if (selectionIndex > 0) {
						goTowards.removeAll(selection.toList());
						goTowards.addAll(selectionIndex - 1, selection.toList());
						goTowardsViewer.setInput(goTowards);
					}
				}
			}
		});

		// down button
		Button downButton = new Button(sortComposite, SWT.PUSH);
		downButton.setToolTipText("Down");
		downButton.setImage(ImageResources.getImage("icons/arrow_down.png", getClass()));
		downButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		downButton.addListener(SWT.Selection, new Listener() {
			@SuppressWarnings("unchecked")
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) goTowardsViewer.getSelection();
				if (!selection.isEmpty()) {
					int selectionIndex = selectedTable.getSelectionIndex();
					if (selectionIndex + 1 < goTowards.size()) {
						goTowards.removeAll(selection.toList());
						goTowards.addAll(selectionIndex + 1, selection.toList());
						goTowardsViewer.setInput(goTowards);
					}
				}
			}
		});

		// down button
		Button bottomButton = new Button(sortComposite, SWT.PUSH);
		bottomButton.setToolTipText("Bottom");
		bottomButton.setImage(ImageResources.getImage("icons/double_down.png", getClass()));
		bottomButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		bottomButton.addListener(SWT.Selection, new Listener() {
			@SuppressWarnings("unchecked")
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) goTowardsViewer.getSelection();
				if (!selection.isEmpty()) {
					goTowards.removeAll(selection.toList());
					goTowards.addAll(selection.toList());
					goTowardsViewer.setInput(goTowards);
				}
			}
		});
	}

	/** Global method to reverse the tangent flags on all GoTowards instances */
	protected void reverseAllTangentFlags() {
		for (GoTowards oneGoTowards : goTowards) {
			oneGoTowards.setTangent(!oneGoTowards.isTangent());
		}
		goTowardsViewer.refresh();
	}

	/** Global method to set the tangent flags on all GoTowards instances */
	protected void setAllTangentFlags(boolean tangent) {
		for (GoTowards oneGoTowards : goTowards) {
			oneGoTowards.setTangent(tangent);
		}
		goTowardsViewer.refresh();
	}

	protected void defineColumns(TableViewer selectedTableViewer) {
		TableViewerColumn selectedNameTableViewerColumn = new TableViewerColumn(selectedTableViewer, SWT.NONE);
		selectedNameTableViewerColumn.getColumn().setWidth(150);
		selectedNameTableViewerColumn.getColumn().setText("Point of View");
		selectedNameTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((GoTowards) element).getJourneyViewPoint().getName();
			}
		});

		TableViewerColumn tangentTableViewerColumn = new TableViewerColumn(selectedTableViewer, SWT.CENTER);
		tangentTableViewerColumn.getColumn().setWidth(50);
		tangentTableViewerColumn.getColumn().setText(tangentLabel);
		tangentTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((GoTowards) element).isTangent() ? "On" : null;
			}
		});

		selectedTableViewer.setColumnProperties(new String[] { null, tangentLabel, null, null });
		selectedTableViewer.setCellModifier(new CellModifierAdapter() {

			@Override
			public boolean canModify(Object element, String property) {
				if (tangentLabel.equals(property)) {
					((GoTowards) element).setTangent(!((GoTowards) element).isTangent());
					selectedTableViewer.refresh(element, false);
				}
				return false;
			}
		});

		TableViewerColumn selectedDelayTableViewerColumn = new TableViewerColumn(selectedTableViewer, SWT.RIGHT);
		selectedDelayTableViewerColumn.getColumn().setWidth(70);
		selectedDelayTableViewerColumn.getColumn().setText("Delay (s)");
		selectedDelayTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return timeFormat.format(((GoTowards) element).getDelay());
			}
		});

		selectedDelayTableViewerColumn.setEditingSupport(new EditingSupport(selectedTableViewer) {
			@Override
			protected void setValue(Object element, Object value) {
				((GoTowards) element).setDelay(((Number) value).floatValue());
				selectedTableViewer.refresh(element);
			}

			@Override
			protected Object getValue(Object element) {
				return Integer.valueOf((int) (((GoTowards) element).getDelay()));
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return new SpinnerCellEditor(selectedTableViewer.getTable(), 0, 60, 1);
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});

		TableViewerColumn selectedMovingTimeTableViewerColumn = new TableViewerColumn(selectedTableViewer, SWT.RIGHT);
		selectedMovingTimeTableViewerColumn.getColumn().setWidth(70);
		selectedMovingTimeTableViewerColumn.getColumn().setText("Move (s)");
		selectedMovingTimeTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return timeFormat.format(((GoTowards) element).getMovingTime());
			}
		});

		selectedMovingTimeTableViewerColumn.setEditingSupport(new EditingSupport(selectedTableViewer) {
			@Override
			protected void setValue(Object element, Object value) {
				((GoTowards) element).setMovingTime(((Number) value).floatValue());
				selectedTableViewer.refresh(element);
				updateTotalSpinner();
			}

			@Override
			protected Object getValue(Object element) {
				return Integer.valueOf((int) (((GoTowards) element).getMovingTime()));
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return new SpinnerCellEditor(selectedTableViewer.getTable(), 1, 60, 1);
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});

	}

	protected void updateTotalSpinner() {
		totalSpinner.setMinimum(goTowards.size());
		totalSpinner.setMaximum(goTowards.size() * 60);
		totalSpinner.setIncrement(goTowards.size());
		setSelection(totalSpinner, (int) goTowards.stream().mapToDouble(GoTowards::getMovingTime).sum());
	}

	/**
	 * Sets the selection to the spinner
	 */
	protected void setSelection(Spinner spinner, int value) {
		if (!spinner.isDisposed()) {
			spinner.getDisplay().asyncExec(() -> {
				if (!spinner.isDisposed()) {
					spinner.setSelection(value);
				}
			});
		}
	}

	/** Sets the geographic position of the eye. */
	protected void goTo(GoTowards goTowards) {
		if (goTowards != null) {
			JourneyViewPoint journeyViewPoint = goTowards.getJourneyViewPoint();
			WorldWindow wwd = IGeographicViewService.grab().getWwd();
			View view = (wwd != null) ? wwd.getView() : null;
			if (view != null) {
				view.setEyePosition(AnimationUtils.applyVerticalExaggeration(journeyViewPoint.getPosition()));
				view.setHeading(journeyViewPoint.getHeading());
				view.setPitch(journeyViewPoint.getPitch());
				SwingUtilities.invokeLater(() -> wwd.redraw());
			}
		}
	}

	/***
	 * Journey's list of {@link GoTowards}
	 */
	public List<GoTowards> getJourneyPoints() {
		return goTowards;
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return getShell().computeSize(SWT.DEFAULT, 300, true);
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}
}
