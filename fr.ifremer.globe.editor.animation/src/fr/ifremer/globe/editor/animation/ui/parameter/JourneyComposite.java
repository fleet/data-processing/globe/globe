package fr.ifremer.globe.editor.animation.ui.parameter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.editor.animation.application.context.ContextInitializer;
import fr.ifremer.globe.editor.animation.application.context.ContextNames;
import fr.ifremer.globe.editor.animation.info.JourneyInfo;
import fr.ifremer.globe.editor.animation.model.GoTowards;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.model.JourneyViewPoint;
import fr.ifremer.globe.editor.animation.player.JourneyPlayer;
import fr.ifremer.globe.editor.animation.recorder.CaptureDialog;
import fr.ifremer.globe.editor.animation.recorder.VideoRecorder.VideoRecorderEvent;
import fr.ifremer.globe.editor.animation.services.info.JourneyInfoLoader;
import fr.ifremer.globe.editor.animation.util.SmoothJourneyComputer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.preference.NasaWorldWindDetailHint;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Parameter composite for Journey parameters
 */
public class JourneyComposite extends Composite {
	protected Logger logger = LoggerFactory.getLogger(JourneyComposite.class);

	protected JourneyInfo journeyInfo;
	protected JourneyInfoLoader journeyInfoLoader;

	protected String videoCaptureCurrentPath = "";
	protected Combo pointsOfViewCombo;
	protected Table currentPoVData;
	protected Button bVideoCapture;

	/** How to format an altitude. */
	protected final DecimalFormat altitudeFormat = new DecimalFormat("#.#");
	/** Formatter of angles. */
	protected final DecimalFormat angleFormat = new DecimalFormat("#.#");

	protected JourneyPlayer journeyPlayer;

	/** Subject that emits a save request to {@code JourneyFileReader} */
	protected PublishSubject<JourneyFile> savePublishSubject = PublishSubject.create();
	/** Subject that emits a smooth computation request to {@code SmoothJourneyComputer} */
	protected PublishSubject<JourneyFile> smoothPublishSubject = PublishSubject.create();

	/**
	 * Constructor.
	 *
	 * @param selection
	 */
	public JourneyComposite(Composite parent, JourneyInfo journeyInfo) {
		super(parent, SWT.None);
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
		this.journeyInfo = journeyInfo;
		this.journeyPlayer = ContextInitializer.getInContext(ContextNames.PLAYER);
		this.journeyInfoLoader = ContextInitializer.getInContext(JourneyInfoLoader.class);
		initialize();

		// Save the file every 2 seconds if needed
		var saveDisposable = savePublishSubject.sample(2, TimeUnit.SECONDS).observeOn(Schedulers.io()).subscribe(
				(journeyFile) -> journeyInfoLoader.save(journeyFile),
				(error) -> logger.warn("Error while saving JourneyFile", error));

		// Smooth the file every second if needed
		var smoothDisposable = smoothPublishSubject.sample(1, TimeUnit.SECONDS).observeOn(Schedulers.computation())
				.subscribe((journeyFile) -> smooth(journeyFile),
						(error) -> logger.warn("Error while smoothing JourneyFile", error));

		// unsubscribe on dispose
		addDisposeListener(evt -> {
			savePublishSubject.unsubscribeOn(Schedulers.io());
			saveDisposable.dispose();
			smoothPublishSubject.unsubscribeOn(Schedulers.computation());
			smoothDisposable.dispose();
		});
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	protected void initialize() {

		DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
		decimalFormatSymbols.setDecimalSeparator('\u00B0');
		angleFormat.setDecimalFormatSymbols(decimalFormatSymbols);
		angleFormat.setDecimalSeparatorAlwaysShown(true);

		JourneyFile jrnFile = journeyInfo.getJourneyFile();
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		Group toolsGroup = new Group(this, SWT.NONE);
		toolsGroup.setText("Tools Panel");
		toolsGroup.setLayout(new GridLayout(1, false));
		toolsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		createPoVGroup(toolsGroup, jrnFile);
		createJourneyPointsGroup(toolsGroup, jrnFile);
		createJourneyGroup(toolsGroup, jrnFile);
		createDetailHintGroup(toolsGroup);
	}

	protected void createPoVGroup(Group toolsGroup, JourneyFile jrnFile) {

		Group pointsOfViewGroup = new Group(toolsGroup, SWT.NONE);
		pointsOfViewGroup.setText("Available points of view");
		pointsOfViewGroup.setLayout(new GridLayout(5, false));
		pointsOfViewGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		pointsOfViewCombo = new Combo(pointsOfViewGroup, SWT.READ_ONLY);
		pointsOfViewCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// INIT COMBO with Point of View already defined
		List<JourneyViewPoint> pointsOfView = jrnFile.getPointsOfView();
		for (JourneyViewPoint point : pointsOfView) {
			pointsOfViewCombo.add(point.getName());
			// Display the first
			pointsOfViewCombo.select(0);
		}
		pointsOfViewCombo.select(0);
		pointsOfViewCombo.addListener(SWT.Selection, event -> createViewPointTable(pointsOfViewGroup, jrnFile));
		// ADD a new View Point to the list of available points of view
		Button addPoVButton = new Button(pointsOfViewGroup, SWT.PUSH);
		addPoVButton.setImage(ImageResources.getImage("/icons/pinpoint-16.png", getClass()));
		addPoVButton.setToolTipText("Add current view to avalaible points of view");
		addPoVButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		addPoVButton.addListener(SWT.Selection, event -> {
			InputDialog dialog = new InputDialog(Display.getCurrent().getActiveShell(), "New point of View", "Name : ",
					"", null);
			if (dialog.open() == Window.OK) {
				String name = dialog.getValue();
				// add view point to view points list

				// JourneyUtils.switchToFlyView();
				jrnFile.addPointOfView(new JourneyViewPoint(name, true));
				// add view point to combo list
				pointsOfViewCombo.add(name);
				pointsOfViewCombo.select(pointsOfViewCombo.getItemCount() - 1);
				createViewPointTable(pointsOfViewGroup, jrnFile);
				savePublishSubject.onNext(jrnFile);
			}
		});
		// EDIT currently selected view point
		Button editPoVButton = new Button(pointsOfViewGroup, SWT.PUSH);
		editPoVButton.setImage(ImageResources.getImage("/icons/edit-16.png", getClass()));
		editPoVButton.setToolTipText("Edit points of view");
		editPoVButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		editPoVButton.addListener(SWT.Selection, event -> {
			// JourneyUtils.switchToFlyView();
			List<JourneyViewPoint> journeyViewPoints = new ArrayList<>();
			jrnFile.getPointsOfView()
					.forEach((journeyViewPoint) -> journeyViewPoints.add(new JourneyViewPoint(journeyViewPoint)));
			EditPointOfViewDialog dialog = new EditPointOfViewDialog(Display.getCurrent().getActiveShell(),
					journeyViewPoints);
			if (dialog.open() == Window.OK) {
				jrnFile.updatePointsOfView(journeyViewPoints);
				if (jrnFile.getSmooth()) {
					smoothPublishSubject.onNext(jrnFile);
				} else {
					reloadNavigationLayer();
				}
				createViewPointTable(pointsOfViewGroup, jrnFile);
				savePublishSubject.onNext(jrnFile);
			}
		});

		// REMOVE a view point from the available PoV list if it isn't used in
		// the journey
		Button removePoVButton = new Button(pointsOfViewGroup, SWT.PUSH);
		removePoVButton.setImage(ImageResources.getImage("/icons/edit_delete.png", getClass()));
		removePoVButton.setToolTipText("Remove from available points of view");
		removePoVButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		removePoVButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (pointsOfViewCombo.getItemCount() > 0) {
					int idx = pointsOfViewCombo.getSelectionIndex();

					if (MessageDialog.openConfirm(removePoVButton.getShell(), "Warning",
							"This will remove the point of view")) {
						boolean canRemove = jrnFile.removePointOfView(idx);
						if (canRemove) {
							pointsOfViewCombo.remove(idx);
							pointsOfViewCombo.select(0);
							createViewPointTable(pointsOfViewGroup, jrnFile);
							savePublishSubject.onNext(jrnFile);
						} else {
							MessageDialog.openWarning(removePoVButton.getShell(), "Warning",
									"Currently in use by " + jrnFile.getName() + ". Delete in used view points first.");
						}
					}
				}
			}
		});

		// GO TO
		Button goToPoVButton = new Button(pointsOfViewGroup, SWT.PUSH);
		goToPoVButton.setImage(ImageResources.getImage("/icons/go-jump.png", getClass()));
		goToPoVButton.setToolTipText("Go To");
		goToPoVButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		goToPoVButton.addListener(SWT.Selection,
				event -> this.journeyPlayer.goTo(pointsOfView.get(pointsOfViewCombo.getSelectionIndex())));

		createViewPointTable(pointsOfViewGroup, jrnFile);

	}

	protected void createJourneyPointsGroup(Group toolsGroup, JourneyFile jrnFile) {

		Group journeyPointsGroup = new Group(toolsGroup, SWT.NONE);
		journeyPointsGroup.setText("Current journey view points");
		journeyPointsGroup.setLayout(new GridLayout(5, false));
		journeyPointsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		ComboViewer journeyPointsCombo = new ComboViewer(journeyPointsGroup, SWT.READ_ONLY);
		journeyPointsCombo.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		journeyPointsCombo.setContentProvider(ArrayContentProvider.getInstance());
		journeyPointsCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((GoTowards) element).getJourneyViewPoint().getName();
			}
		});
		journeyPointsCombo.setInput(jrnFile.getJourneyPointsList());
		journeyPointsCombo.getCombo().select(0);

		// ADD selected point of view to the journey with delay
		Button editJourneyVButton = new Button(journeyPointsGroup, SWT.PUSH);
		editJourneyVButton.setImage(ImageResources.getImage("/icons/edit_journey-16.png", getClass()));
		editJourneyVButton.setToolTipText("Edit the journey points and associated delays ");
		editJourneyVButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		editJourneyVButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				List<GoTowards> editedGoTowards = new ArrayList<>();
				jrnFile.getJourneyPointsList().forEach((goTowards) -> editedGoTowards.add(new GoTowards(goTowards)));

				EditJourneyDialog dialog = new EditJourneyDialog(Display.getCurrent().getActiveShell(), jrnFile,
						editedGoTowards);
				if (dialog.open() == Window.OK) {
					jrnFile.setJourneyPointsList(editedGoTowards);

					GoTowards selectedGoTowards = getSelectedGoTowards(journeyPointsCombo);
					journeyPointsCombo.setInput(editedGoTowards);
					journeyPointsCombo.getCombo().select(Math.max(0, editedGoTowards.indexOf(selectedGoTowards)));

					if (jrnFile.getSmooth()) {
						smoothPublishSubject.onNext(jrnFile);
					} else {
						reloadNavigationLayer();
					}
					savePublishSubject.onNext(jrnFile);
				}
			}
		});

		// GO TO
		Button gotoJourneyPoint = new Button(journeyPointsGroup, SWT.PUSH);
		gotoJourneyPoint.setImage(ImageResources.getImage("/icons/go-jump.png", getClass()));
		gotoJourneyPoint.setToolTipText("Go To");
		gotoJourneyPoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		gotoJourneyPoint.addListener(SWT.Selection,
				event -> this.journeyPlayer.goTo(getSelectedGoTowards(journeyPointsCombo)));

	}

	protected GoTowards getSelectedGoTowards(ComboViewer comboViewer) {
		return (GoTowards) ((IStructuredSelection) comboViewer.getSelection()).getFirstElement();
	}

	protected void createDetailHintGroup(Group parent) {
		Group group = new Group(parent, SWT.NONE);
		group.setText("Scene detail");
		group.setLayout(new FillLayout());
		group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));

		NasaWorldWindDetailHint nasaWorldWindDetailHint = SSVConfiguration.getConfiguration()
				.getNasaWorldWindDetailHint();
		nasaWorldWindDetailHint.initializeView(group, SWT.NONE);
	}

	protected void createJourneyGroup(Group parent, JourneyFile jrnFile) {

		Group journeyGroup = new Group(parent, SWT.NONE);
		journeyGroup.setText("Journey");
		journeyGroup.setLayout(new GridLayout(1, false));
		journeyGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Composite magnetoComposite = new Composite(journeyGroup, SWT.NONE);
		magnetoComposite.setLayout(new GridLayout(4, false));
		magnetoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		// Start journey
		Button startJourney = new Button(magnetoComposite, SWT.PUSH);
		startJourney.setImage(ImageResources.getImage("/icons/media-playback-start.png", getClass()));
		startJourney.setToolTipText("Start");
		startJourney.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		startJourney.addListener(SWT.Selection,
				event -> journeyPlayer.startJourney(jrnFile, bVideoCapture.getSelection()));

		// Stop journey
		Button stopJourney = new Button(magnetoComposite, SWT.PUSH);
		stopJourney.setImage(ImageResources.getImage("/icons/media-playback-pause.png", getClass()));
		stopJourney.setToolTipText("Stop");
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gridData.horizontalIndent = 10;
		stopJourney.setLayoutData(gridData);
		stopJourney.addListener(SWT.Selection, event -> journeyPlayer.stopJourney());

		// loop
		Button checkLoop = new Button(magnetoComposite, SWT.TOGGLE);
		checkLoop.setImage(ImageResources.getImage("/icons/media-playback-loop.png", getClass()));
		checkLoop.setToolTipText("Loop");
		checkLoop.setSelection(false);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gridData.horizontalIndent = 10;
		gridData.widthHint = 40;
		checkLoop.setLayoutData(gridData);
		checkLoop.addListener(SWT.Selection, event -> journeyPlayer.setLoop(checkLoop.getSelection()));

		// Video capture check box
		bVideoCapture = new Button(magnetoComposite, SWT.TOGGLE);
		bVideoCapture.setImage(ImageResources.getImage("/icons/camera.png", getClass()));
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gridData.widthHint = 40;
		bVideoCapture.setLayoutData(gridData);
		bVideoCapture.setToolTipText("Capture video when playing");

		Composite actionComposite = new Composite(journeyGroup, SWT.NONE);
		actionComposite.setLayout(new GridLayout(3, false));
		actionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		// smooth
		Button smoothButton = new Button(actionComposite, SWT.TOGGLE);
		smoothButton.setImage(ImageResources.getImage("/icons/smooth-16.png", getClass()));
		smoothButton.setText("Smooth");
		gridData = new GridData(SWT.CENTER, SWT.TOP, false, false);
		smoothButton.setLayoutData(gridData);
		smoothButton.setSelection(jrnFile.getSmooth());

		// smooth fit spinner
		Spinner fitSpinner = new Spinner(actionComposite, SWT.BORDER);
		gridData = new GridData(SWT.CENTER, SWT.FILL, false, false);
		fitSpinner.setLayoutData(gridData);
		fitSpinner.setMinimum(0);
		fitSpinner.setMaximum(100);
		int value = 100 - (int) (jrnFile.getFit() * 100);
		fitSpinner.setSelection(value);
		fitSpinner.setIncrement(1);
		fitSpinner.setToolTipText("0 (tight) to 100 (loose)");
		fitSpinner.addListener(SWT.Selection, event -> {
			int selection = fitSpinner.getSelection();
			double fitValue = selection / 100d;
			jrnFile.setFit(1d - fitValue);
			smoothPublishSubject.onNext(jrnFile);
			savePublishSubject.onNext(jrnFile);
		});

		fitSpinner.setEnabled(jrnFile.getSmooth());

		smoothButton.addListener(SWT.Selection, event -> {
			jrnFile.setSmooth(smoothButton.getSelection());
			fitSpinner.setEnabled(smoothButton.getSelection());

			if (smoothButton.getSelection()) {
				smoothPublishSubject.onNext(jrnFile);
			} else {
				reloadNavigationLayer();
			}
			savePublishSubject.onNext(jrnFile);
		});

		bVideoCapture.addListener(SWT.Selection, event -> {
			// get the directory if videoCapture check box checked
			if (bVideoCapture.getSelection()) {
				CaptureDialog captureDialog = new CaptureDialog(bVideoCapture.getShell(),
						journeyPlayer.getVideoRecorder());
				if (captureDialog.open() != Window.OK) {
					bVideoCapture.setSelection(false);
				}
			}
		});
	}

	protected void reloadNavigationLayer() {
		IWWLayerService.grab().getFileLayerStoreModel().get(journeyInfo).map(WWFileLayerStore::getLayers)
				.ifPresent(list -> list.stream()//
						.filter(IWWNavigationLayer.class::isInstance).map(IWWNavigationLayer.class::cast)
						.forEach(IWWNavigationLayer::reload));

		WorldWindow wwd = Viewer3D.getWwd();
		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();
			view.firePropertyChange(AVKey.VIEW, null, view);
		}
	}

	protected void smooth(JourneyFile journeyFile) {
		SmoothJourneyComputer smoothJourneyComputer = new SmoothJourneyComputer();
		smoothJourneyComputer.smooth(journeyFile);
		reloadNavigationLayer();
	}

	protected void createViewPointTable(Group parent, JourneyFile jrnFile) {
		if (currentPoVData != null) {
			currentPoVData.removeAll();
		} else {
			currentPoVData = new Table(parent, SWT.NO_SCROLL | SWT.BORDER);
			currentPoVData.setRedraw(false); // Wait the end of contruction to draw the table
			currentPoVData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 5, 1));
			TableColumn columnData = new TableColumn(currentPoVData, SWT.LEFT);
			TableColumn columnValues = new TableColumn(currentPoVData, SWT.LEFT);
			columnValues.setResizable(true);
			columnValues.setWidth(Integer.MAX_VALUE);
			columnData.setText("Data");
			columnValues.setText("Value");
			currentPoVData.setHeaderVisible(true);
			currentPoVData.setLinesVisible(true);
		}

		TableItem nameItem = new TableItem(currentPoVData, SWT.NONE);
		nameItem.setText(0, "Name");
		TableItem latItem = new TableItem(currentPoVData, SWT.NONE);
		latItem.setText(0, "Latitude");
		TableItem longItem = new TableItem(currentPoVData, SWT.NONE);
		longItem.setText(0, "Longitude");
		TableItem altItem = new TableItem(currentPoVData, SWT.NONE);
		altItem.setText(0, "Altitude");
		TableItem pitchItem = new TableItem(currentPoVData, SWT.NONE);
		pitchItem.setText(0, "Pitch");
		TableItem headingItem = new TableItem(currentPoVData, SWT.NONE);
		headingItem.setText(0, "Heading");

		if (pointsOfViewCombo.getSelectionIndex() != -1) {
			JourneyViewPoint selectPoV = jrnFile.getPointsOfView().get(pointsOfViewCombo.getSelectionIndex());
			nameItem.setText(1, selectPoV.getName());
			latItem.setText(1, "" + FormatLatitudeLongitude.latitudeToString(selectPoV.getLatitudeDegrees()));
			longItem.setText(1, "" + FormatLatitudeLongitude.longitudeToString(selectPoV.getLongitudeDegrees()));
			altItem.setText(1, "" + altitudeFormat.format(selectPoV.getAltitude()));
			pitchItem.setText(1, angleFormat.format(selectPoV.getPitchDegrees()));
			headingItem.setText(1, angleFormat.format(selectPoV.getHeadingDegrees()));
		}

		parent.getDisplay().asyncExec(() -> {
			if (!currentPoVData.isDisposed()) {
				// Pack first column
				currentPoVData.getColumn(0).pack();
				int firstColumnWidth = currentPoVData.getColumn(0).getWidth();
				// Compute size of second column
				int totalAreaWdith = currentPoVData.getClientArea().width;
				int lineWidth = currentPoVData.getGridLineWidth();
				int secondColumnWidth = totalAreaWdith - (firstColumnWidth + lineWidth);
				currentPoVData.getColumn(1).setWidth(secondColumnWidth);

				currentPoVData.setRedraw(true); // Table can be drawn now
				currentPoVData.redraw();
			}
		});
	}

	@Inject
	@Optional
	public void onVideoRecorderEvent(@UIEventTopic(VideoRecorderEvent.TOPIC) VideoRecorderEvent event) {
		if (!bVideoCapture.isDisposed()) {
			bVideoCapture.setEnabled(!event.videoRecorder().isRecording());
			if (!event.videoRecorder().isRecording())
				bVideoCapture.setSelection(false);
		}
	}

}
