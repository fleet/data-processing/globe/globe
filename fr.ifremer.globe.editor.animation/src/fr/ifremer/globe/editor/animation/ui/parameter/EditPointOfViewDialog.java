package fr.ifremer.globe.editor.animation.ui.parameter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.function.DoubleConsumer;
import java.util.function.ObjDoubleConsumer;
import java.util.function.ToDoubleFunction;

import javax.swing.SwingUtilities;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;

import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.editor.animation.model.JourneyViewPoint;
import fr.ifremer.globe.editor.animation.util.AnimationUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.widget.SpinnerCellEditor;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Earth;

/**
 * Dialog to edit some JourneyViewPoint. All modifications are reflected in NWW view
 */
public class EditPointOfViewDialog extends Dialog {

	/** All JourneyViewPoint to edit */
	protected List<JourneyViewPoint> pointOfViews;
	/** Nasa View */
	protected View view;
	/** How to format an altitude. */
	protected final DecimalFormat altitudeFormat = new DecimalFormat("#.#");
	/** Formatter of angles. */
	protected final DecimalFormat angleFormat = new DecimalFormat("#.#");

	/** TableViewer created to edit all JourneyViewPoint */
	protected TableViewer journeyViewPointTableViewer;

	/**
	 * Constructor
	 */
	public EditPointOfViewDialog(Shell shell, List<JourneyViewPoint> pointOfViews) {
		super(shell);
		this.pointOfViews = pointOfViews;
		WorldWindow wwd = IGeographicViewService.grab().getWwd();
		this.view = (wwd != null) ? wwd.getView() : null;

		DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
		decimalFormatSymbols.setDecimalSeparator('\u00B0');
		angleFormat.setDecimalFormatSymbols(decimalFormatSymbols);
		angleFormat.setDecimalSeparatorAlwaysShown(true);
	}

	/**
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Edit values of points of view");
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		journeyViewPointTableViewer = new TableViewer(composite,
				SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		journeyViewPointTableViewer.setContentProvider(ArrayContentProvider.getInstance());
		Table selectedTable = journeyViewPointTableViewer.getTable();
		selectedTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		selectedTable.setHeaderVisible(true);
		selectedTable.setLinesVisible(true);

		defineColumns(journeyViewPointTableViewer);
		journeyViewPointTableViewer.setInput(pointOfViews);

		journeyViewPointTableViewer.addSelectionChangedListener(
				(e) -> goTo((JourneyViewPoint) ((IStructuredSelection) e.getSelection()).getFirstElement()));
		return composite;
	}

	/** Sets the geographic position of the eye. */
	protected void goTo(JourneyViewPoint journeyViewPoint) {
		view.setEyePosition(AnimationUtils.applyVerticalExaggeration(journeyViewPoint.getPosition()));
		view.setHeading(journeyViewPoint.getHeading());
		view.setPitch(journeyViewPoint.getPitch());
		SwingUtilities.invokeLater(() -> IGeographicViewService.grab().getWwd().redraw());
	}

	/** Defines the columns of the TableViewer */
	protected void defineColumns(TableViewer journeyViewPointTableViewer) {
		defineNameColumn(journeyViewPointTableViewer);
		defineLatitudeColumn(journeyViewPointTableViewer);
		defineLongitudeColumn(journeyViewPointTableViewer);
		defineAltitudeColumn(journeyViewPointTableViewer);
		definePitchColumn(journeyViewPointTableViewer);
		defineHeadingColumn(journeyViewPointTableViewer);
	}

	/** Defines the name column of the TableViewer */
	protected void defineNameColumn(TableViewer journeyViewPointTableViewer) {
		TableViewerColumn nameTableViewerColumn = new TableViewerColumn(journeyViewPointTableViewer, SWT.NONE);
		nameTableViewerColumn.getColumn().setWidth(150);
		nameTableViewerColumn.getColumn().setText("Point of View");
		nameTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((JourneyViewPoint) element).getName();
			}
		});
	}

	/** Defines the latitude column of the TableViewer */
	protected void defineLatitudeColumn(TableViewer journeyViewPointTableViewer) {
		TableViewerColumn latitudeTableViewerColumn = new TableViewerColumn(journeyViewPointTableViewer, SWT.RIGHT);
		latitudeTableViewerColumn.getColumn().setWidth(100);
		latitudeTableViewerColumn.getColumn().setText("Latitude");
		latitudeTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return FormatLatitudeLongitude.latitudeToString(((JourneyViewPoint) element).getLatitudeDegrees());
			}
		});

		latitudeTableViewerColumn.setEditingSupport(new DoubleEditingSupport(-72d, 88d, 0.01d, 5,
				(journeyViewPoint) -> journeyViewPoint.getLatitudeDegrees(),
				(journeyViewPoint, latitude) -> journeyViewPoint.setLatitude(latitude),
				(latitude) -> view.setEyePosition(Position.fromDegrees(latitude,
						view.getEyePosition().getLongitude().getDegrees(), view.getEyePosition().getAltitude()))));
	}

	/** Defines the longitude column of the TableViewer */
	protected void defineLongitudeColumn(TableViewer journeyViewPointTableViewer) {
		TableViewerColumn longitudeTableViewerColumn = new TableViewerColumn(journeyViewPointTableViewer, SWT.RIGHT);
		longitudeTableViewerColumn.getColumn().setWidth(100);
		longitudeTableViewerColumn.getColumn().setText("Longitude");
		longitudeTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return FormatLatitudeLongitude.longitudeToString(((JourneyViewPoint) element).getLongitudeDegrees());
			}
		});

		longitudeTableViewerColumn.setEditingSupport(new DoubleEditingSupport(-180d, 180d, 0.01d, 5,
				(journeyViewPoint) -> journeyViewPoint.getLongitudeDegrees(),
				(journeyViewPoint, longitude) -> journeyViewPoint.setLongitude(longitude),
				(longitude) -> view
						.setEyePosition(Position.fromDegrees(view.getEyePosition().getLatitude().getDegrees(),
								longitude, view.getEyePosition().getAltitude()))));
	}

	/** Defines the altitude column of the TableViewer */
	protected void defineAltitudeColumn(TableViewer journeyViewPointTableViewer) {
		TableViewerColumn altitudeTableViewerColumn = new TableViewerColumn(journeyViewPointTableViewer, SWT.RIGHT);
		altitudeTableViewerColumn.getColumn().setWidth(80);
		altitudeTableViewerColumn.getColumn().setText("Altitude");
		altitudeTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return altitudeFormat.format(((JourneyViewPoint) element).getAltitude());
			}
		});

		altitudeTableViewerColumn.setEditingSupport(new DoubleEditingSupport(Earth.ELEVATION_MIN, 9000000d, 1d, 1,
				(journeyViewPoint) -> journeyViewPoint.getAltitude(),
				(journeyViewPoint, altitude) -> journeyViewPoint.setAltitude(altitude),
				(altitude) -> view.setEyePosition(Position.fromDegrees(view.getEyePosition().getLatitude().getDegrees(),
						view.getEyePosition().getLongitude().getDegrees(), altitude))));
	}

	/** Defines the pitch column of the TableViewer */
	protected void definePitchColumn(TableViewer journeyViewPointTableViewer) {
		TableViewerColumn pitchTableViewerColumn = new TableViewerColumn(journeyViewPointTableViewer, SWT.RIGHT);
		pitchTableViewerColumn.getColumn().setWidth(80);
		pitchTableViewerColumn.getColumn().setText("Pitch");
		pitchTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return angleFormat.format(((JourneyViewPoint) element).getPitchDegrees());
			}
		});

		pitchTableViewerColumn.setEditingSupport(
				new DoubleEditingSupport(0d, 180d, 0.1d, 1, (journeyViewPoint) -> journeyViewPoint.getPitchDegrees(),
						(journeyViewPoint, pitch) -> journeyViewPoint.setPitch(Angle.fromDegrees(pitch)),
						(pitch) -> view.setPitch(Angle.fromDegrees(pitch))));
	}

	/** Defines the heading column of the TableViewer */
	protected void defineHeadingColumn(TableViewer journeyViewPointTableViewer) {
		TableViewerColumn headingTableViewerColumn = new TableViewerColumn(journeyViewPointTableViewer, SWT.RIGHT);
		headingTableViewerColumn.getColumn().setWidth(80);
		headingTableViewerColumn.getColumn().setText("Heading");
		headingTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return angleFormat.format(((JourneyViewPoint) element).getHeadingDegrees());
			}
		});

		headingTableViewerColumn.setEditingSupport(new DoubleEditingSupport(-180d, 180d, 0.1d, 1,
				(journeyViewPoint) -> journeyViewPoint.getHeadingDegrees(),
				(journeyViewPoint, heading) -> journeyViewPoint.setHeading(Angle.fromDegrees(heading)),
				(heading) -> view.setHeading(Angle.fromDegrees(heading))));

	}

	/** Support for cell editing of any double property using a spinner */
	protected class DoubleEditingSupport extends EditingSupport {
		protected ToDoubleFunction<JourneyViewPoint> valueSupplier;
		protected ObjDoubleConsumer<JourneyViewPoint> valueConsumer;
		protected DoubleConsumer nwwSetter;
		int minimum;
		int maximum;
		int increment;
		int digits;

		public DoubleEditingSupport(double minimum, double maximum, double increment, int digits,
				ToDoubleFunction<JourneyViewPoint> valueSupplier, ObjDoubleConsumer<JourneyViewPoint> valueConsumer,
				DoubleConsumer nwwSetter) {
			super(journeyViewPointTableViewer);
			this.minimum = (int) (minimum * Math.pow(10d, digits));
			this.maximum = (int) (maximum * Math.pow(10d, digits));
			this.increment = (int) (increment * Math.pow(10d, digits));
			this.digits = digits;
			this.valueConsumer = valueConsumer;
			this.valueSupplier = valueSupplier;
			this.nwwSetter = nwwSetter;
		}

		/**
		 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
		 */
		@Override
		protected void setValue(Object element, Object value) {
			valueConsumer.accept((JourneyViewPoint) element, ((Number) value).doubleValue() / Math.pow(10d, digits));
			journeyViewPointTableViewer.refresh(element);
		}

		/**
		 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
		 */
		@Override
		protected Object getValue(Object element) {
			return Integer
					.valueOf((int) (valueSupplier.applyAsDouble((JourneyViewPoint) element) * Math.pow(10d, digits)));
		}

		/**
		 * @see org.eclipse.jface.viewers.EditingSupport#getCellEditor(java.lang.Object)
		 */
		@Override
		protected CellEditor getCellEditor(Object element) {
			SpinnerCellEditor result = new SpinnerCellEditor(journeyViewPointTableViewer.getTable(), SWT.WRAP, minimum,
					maximum, increment, digits);

			Spinner spinner = (Spinner) result.getControl();
			spinner.addListener(SWT.Selection, e -> {
				// ----------------------/
				// nwwSetter.accept(((spinner.getSelection()) / Math.pow(10d, digits)));
				// SwingUtilities.invokeLater(() -> Viewer3D.getWwd().redraw());
				// TODO: temporary fix to allow value edit and view refresh : if validated, the nwwSetter could be
				// removed
				valueConsumer.accept((JourneyViewPoint) element,
						((Number) spinner.getSelection()).doubleValue() / Math.pow(10d, digits));
				goTo((JourneyViewPoint) element);
				// ----------------------/
			});
			return result;
		}

		/**
		 * @see org.eclipse.jface.viewers.EditingSupport#canEdit(java.lang.Object)
		 */
		@Override
		protected boolean canEdit(Object element) {
			return true;
		}
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return getShell().computeSize(SWT.DEFAULT, 260, true);
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}

}
