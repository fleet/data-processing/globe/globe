package fr.ifremer.globe.editor.animation.application.handlers;

import java.io.File;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.editor.animation.model.JourneyFile;
import fr.ifremer.globe.editor.animation.services.info.JourneyInfoLoader;
import fr.ifremer.globe.editor.animation.util.JourneyUtils;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;

public class NewJourneyFileHandler {

	protected String defaultDirectory = "";

	@Inject
	protected JourneyInfoLoader journeyInfoLoader;

	@Inject
	protected IFileLoadingService fileLoadingService;

	@CanExecute
	public boolean canExecute(@Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection) {
		return structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof FolderNode;
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {

		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setFilterNames(new String[] { journeyInfoLoader.getFileFilters().get(0).getFirst() });
		dialog.setFilterExtensions(new String[] { journeyInfoLoader.getFileFilters().get(0).getSecond() });
		dialog.setFilterPath(defaultDirectory);
		String filePath = dialog.open();
		if (filePath != null) {
			defaultDirectory = dialog.getFilterPath();
			if (!FilenameUtils.isExtension(filePath, JourneyUtils.journeyFileExt)) {
				filePath = filePath + "." + JourneyUtils.journeyFileExt;
			}
			File journeyFile = new File(filePath);
			JourneyFile jrnFile = new JourneyFile(journeyFile.getName(), journeyFile);
			journeyInfoLoader.save(jrnFile);

			ITreeNodeFactory.grab().createFileInfoNode(new BasicFileInfo(filePath));
			fileLoadingService.load(filePath);
		}
	}
}
