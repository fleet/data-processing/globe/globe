/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.application.context;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.osgi.service.event.Event;

import fr.ifremer.globe.editor.animation.player.JourneyPlayer;
import fr.ifremer.globe.editor.animation.recorder.VideoRecorder;

/**
 * Application Addon used to initialize the IEclipseContext
 */
public class ContextInitializer {

	/** Static reference of Eclipse context initialized by E4Application */
	protected static IEclipseContext eclipseContext;

	/**
	 * Invoked when Globe application is started.<br>
	 * Fill the context with needed components
	 *
	 * @param injectedEclipseContext Eclipse context initialized by E4Application
	 */
	@Inject
	@Optional
	public void activate(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext injectedEclipseContext) {
		activate(injectedEclipseContext);
	}

	/**
	 * Fill the context with needed components and initialize the context
	 */
	public static void activate(IEclipseContext eclipseContext) {
		setEclipseContext(eclipseContext);
		init();
	}

	/**
	 * Init the singleton instance, create by DI, it registers reference instances of objects used througout the
	 * animation
	 */
	protected static void init() {
		eclipseContext.set(ContextNames.VIDEO_RECORDER,
				ContextInjectionFactory.make(VideoRecorder.class, eclipseContext));
		eclipseContext.set(ContextNames.PLAYER, ContextInjectionFactory.make(JourneyPlayer.class, eclipseContext));
	}

	/**
	 * Getter of {@link #eclipseContext}
	 */
	public static IEclipseContext getEclipseContext() {
		return eclipseContext;
	}

	/**
	 * Setter of {@link #eclipseContext}
	 */
	public static void setEclipseContext(IEclipseContext newEclipseContext) {
		eclipseContext = newEclipseContext;
	}

	/**
	 * Returns the context value associated with the given name
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInContext(String name) {
		return (T) getEclipseContext().get(name);
	}

	/**
	 * Returns the context value associated with the given class
	 */
	public static <T> T getInContext(Class<T> clazz) {
		return getEclipseContext().get(clazz);
	}

	/**
	 * Sets a value to be associated with a given name in this context
	 */
	public static void setInContext(String name, Object value) {
		getEclipseContext().set(name, value);
	}

	/**
	 * Removes the given name and any corresponding value from this context
	 */
	public static void removeFromContext(String name) {
		getEclipseContext().remove(name);
	}
}
