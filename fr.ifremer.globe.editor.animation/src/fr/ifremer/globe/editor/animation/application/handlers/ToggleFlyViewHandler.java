package fr.ifremer.globe.editor.animation.application.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.editor.animation.application.context.ContextNames;
import fr.ifremer.globe.editor.animation.player.JourneyPlayer;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.viewer3d.view.GlobeFlyView;
import fr.ifremer.viewer3d.view.SSVOrbitView;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.view.BasicView;

public class ToggleFlyViewHandler {

	private Logger logger = LoggerFactory.getLogger(ToggleFlyViewHandler.class);

	/** Current item... */
	protected static MHandledToolItem handledToolItem = null;

	protected SSVOrbitView orbitView = null;
	protected GlobeFlyView flyView = null;

	@CanExecute
	public boolean canExecute(MHandledToolItem item) {
		handledToolItem = item;
		return true;
	}

	@Execute
	public void execute(@Optional @Active MHandledToolItem item,
			@Named(ContextNames.PLAYER) JourneyPlayer journeyPlayer) {
		handledToolItem = item;

		logger.debug("ToggleFlyView Handler");
		journeyPlayer.stopJourney();

		WorldWindowGLCanvas wwd = IGeographicViewService.grab().getWwd();
		boolean switchToFlyView = item.isSelected();
		BasicView currentView = (BasicView) wwd.getView();
		if (switchToFlyView) { // To FlyView
			if (orbitView == null) {
				orbitView = (SSVOrbitView) currentView;
			}

			if (flyView == null || orbitView.isPointOfViewHasChanged()) {
				flyView = new GlobeFlyView(currentView);
			} else {
				orbitView = (SSVOrbitView) currentView;
			}
			flyView.setPointOfViewHasChanged(false);
			IGeographicViewService.grab().getWwd().setView(flyView);
		} else { // To OrbitView
			if (flyView == null) {
				flyView = (GlobeFlyView) currentView;
			}

			if (orbitView == null || flyView.isPointOfViewHasChanged()) {
				orbitView = new SSVOrbitView(currentView);
			} else {
				flyView = (GlobeFlyView) currentView;
			}
			orbitView.setPointOfViewHasChanged(false);
			IGeographicViewService.grab().getWwd().setView(orbitView);
		}
	}

	/**
	 * Activates or desactivates the ToolItem (execute() is not invoked)
	 */
	public static void active(boolean active) {
		if (handledToolItem != null) {
			Display.getDefault().asyncExec(() -> handledToolItem.setSelected(active));
		}

	}

}
