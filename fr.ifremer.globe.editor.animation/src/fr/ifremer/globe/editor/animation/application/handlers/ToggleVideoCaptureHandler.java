package fr.ifremer.globe.editor.animation.application.handlers;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.editor.animation.application.context.ContextNames;
import fr.ifremer.globe.editor.animation.recorder.CaptureDialog;
import fr.ifremer.globe.editor.animation.recorder.VideoRecorder;
import fr.ifremer.globe.editor.animation.recorder.VideoRecorder.VideoRecorderEvent;
import fr.ifremer.globe.ffmpeg.FfmpegUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.Messages;

public class ToggleVideoCaptureHandler {

	protected static Logger logger = LoggerFactory.getLogger(ToggleVideoCaptureHandler.class);

	protected MHandledToolItem handledToolItem;

	@Inject
	@Optional
	@Named(ContextNames.VIDEO_RECORDER)
	protected VideoRecorder videoRecorder;

	@Execute
	public void execute(Shell shell, @Optional @Active MHandledToolItem handledToolItem) {
		logger.debug("ToggleVideoCapture Handler");

		// first call
		this.handledToolItem = handledToolItem;

		if (handledToolItem.isSelected()) {
			CaptureDialog captureDialog = new CaptureDialog(shell, videoRecorder);
			if (captureDialog.open() == Window.OK) {

				if (FfmpegUtils.capturedFilesExists(captureDialog.getVideoCapturePath())
						&& Messages.openSyncQuestionMessage("Video capture",
								"The selected folder already contains some screenshots.\nDo you only want to encode a video with this files ?")) {
					handledToolItem.setSelected(false);
					encodeVideo(shell);
				} else {
					videoRecorder.startRecording(IGeographicViewService.grab().getWwd());
				}
			} else {
				handledToolItem.setSelected(false);
			}

		} else {
			encodeVideo(shell);
		}
	}

	/** Ask to encode images. */
	protected void encodeVideo(Shell shell) {
		videoRecorder.stopRecording();
		FfmpegUtils.encodeVideo(shell, "Capture", videoRecorder.getVideoCapturePath(), videoRecorder.getFramerate());
	}

	@Inject
	@Optional
	public void onVideoRecorderEvent(@UIEventTopic(VideoRecorderEvent.TOPIC) VideoRecorderEvent event) {
		if (handledToolItem != null && !event.videoRecorder().isRecording()) {
			handledToolItem.setSelected(false);
		}
	}
}
