/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.animation.application.context;

/**
 * All names of injectable objects stored in the IEclipseContext
 */
public class ContextNames {

	/** Root name of all objects that can be injected */
	public static final String BASE_NAME = "fr.ifremer.globe.editor.animation";

	/** Name of the JourneyPlayer */
	public static final String PLAYER = BASE_NAME + ".player";
	/** Name of the VideoRecorder */
	public static final String VIDEO_RECORDER = BASE_NAME + ".video.recorder";

	/**
	 * Constructor
	 */
	private ContextNames() {
		// private constructor to hide the implicit public one.
	}
}
