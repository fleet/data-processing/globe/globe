uniform float size;
uniform float percentArray[16];
uniform int colorRedArray[16];
uniform int colorGreenArray[16];
uniform int colorBlueArray[16];

void main()
{
	float pi = 3.14;

	//copy value to avoid to modify a uniform value
	float size_tmp = size;

	//avoid the marker out of the area
	if (size_tmp > 0.5) {
		size_tmp = 0.5;
	}

	float x = gl_TexCoord[0].s - size_tmp;
	float y = gl_TexCoord[0].t - size_tmp;
	if(gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s > 1.0 || gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t > 1.0)
	{
		gl_FragColor.a = 0.0;
	} else {
		float radius = sqrt(x * x + y * y);
		//first draw circle
		if(radius < size_tmp)
		{
			//percent => compute angle between center and pixel
			float teta = atan(y,-x);
			teta = degrees(teta);
			if(teta<0.0) {
				teta = teta + 360.0;
			}
			//antitrigonometric, vetrical (camembert)
			float percentTeta = (360.0-(teta))/360.0;
			//surround with black circle

			float dataSumm = 0.0;
			for (int i = 0; i<16; i++) {
				if (percentArray[i] != 0.0) { //check if data have a percent
					dataSumm += percentArray[i]; 
					if (percentTeta < dataSumm) {
						gl_FragColor.r = float(colorRedArray[i])/255.0;
						gl_FragColor.g = float(colorGreenArray[i])/255.0;
						gl_FragColor.b =  float(colorBlueArray[i])/255.0;
						gl_FragColor.a = 1.0;

						break;
					}
				}
			}

		} else {
			gl_FragColor.a = 0.0;
		}
	}
}