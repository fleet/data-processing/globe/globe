package fr.ifremer.viewer3d;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import gov.nasa.worldwind.BasicSceneController;
import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.geom.Intersection;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.TiledImageLayer;
import gov.nasa.worldwind.layers.Earth.BMNGOneImage;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.airspaces.editor.AirspaceEditorUtil;
import gov.nasa.worldwind.terrain.SectorGeometry;
import gov.nasa.worldwind.util.Logging;

public class Viewer3DSceneController extends BasicSceneController {
	protected String vertex_shader = "/shader/elevationShader.vert";
	protected String fragment_shader = "/shader/blackToTransparentShader.frag";

	private static Map<GL, Integer> shaderprograms = new HashMap<GL, Integer>(4);
	private boolean init = false;
	private boolean displayorHideTerrain = true;

	private Optional<GeoBox> displayedGeoBox=Optional.empty();

	private Position computeIntersection(double x, double y)
	{
		Line ray = dc.getView().computeRayFromScreenPoint(x, y);
		Intersection[] intersections = getTerrain().intersect(ray);
		if (intersections != null)
		{
			Vec4 point = AirspaceEditorUtil.nearestIntersectionPoint(ray, intersections);
			if (point != null)
			{
				Position pos = getModel().getGlobe().computePositionFromPoint(point);
				return pos;
			}
		}
		return null;
	}
	/***
	 * draw layers set opacity transparent if pixel color is black for MS
	 * Virtual Earth
	 * 
	 * @param dc
	 */
	@Override
	protected void draw(DrawContext dc) {

		// PATCH GLOBE - BEGIN - Code added
		Position cornerA = computeIntersection(dc.getView().getViewport().getMinX(),
				dc.getView().getViewport().getMinY());
		Position cornerB = computeIntersection(dc.getView().getViewport().getMaxX(),
				dc.getView().getViewport().getMinY());
		Position cornerC = computeIntersection(dc.getView().getViewport().getMaxX(),
				dc.getView().getViewport().getMaxY());
		Position cornerD = computeIntersection(dc.getView().getViewport().getMinX(),
				dc.getView().getViewport().getMaxY());
		displayedGeoBox = Optional.empty();
		if (cornerA != null && cornerB != null && cornerC != null && cornerD != null) {
			double northLatitude = Math.max(cornerA.getLatitude().getDegrees(), cornerB.getLatitude().getDegrees());
			northLatitude = Math.max(northLatitude, cornerC.getLatitude().getDegrees());
			northLatitude = Math.max(northLatitude, cornerD.getLatitude().getDegrees());
			double southLatitude = Math.min(cornerA.getLatitude().getDegrees(), cornerB.getLatitude().getDegrees());
			southLatitude = Math.min(northLatitude, cornerC.getLatitude().getDegrees());
			southLatitude = Math.min(northLatitude, cornerD.getLatitude().getDegrees());

			double eastLongitude = Math.min(cornerA.getLongitude().getDegrees(), cornerB.getLongitude().getDegrees());
			eastLongitude = Math.max(eastLongitude, cornerC.getLongitude().getDegrees());
			eastLongitude = Math.max(eastLongitude, cornerD.getLongitude().getDegrees());

			double westLongitude = Math.max(cornerA.getLongitude().getDegrees(), cornerB.getLongitude().getDegrees());
			westLongitude = Math.min(westLongitude, cornerC.getLongitude().getDegrees());
			westLongitude = Math.min(westLongitude, cornerD.getLongitude().getDegrees());

			GeoBox box = new GeoBox(northLatitude, southLatitude, eastLongitude, westLongitude);
			displayedGeoBox = Optional.of(box);
		}

		if (!init) {
			init = true;
			try {
				shaderprograms.put(dc.getGL(), ShaderUtil.createShader(this, dc.getGL(), vertex_shader, fragment_shader));
			} catch (GIOException e) {
				e.printStackTrace();
			}
		}
		// PATCH GLOBE - END - Code added

		try {
			// Draw the layers.
			if (dc.getLayers() != null) {
				for (Layer layer : dc.getLayers()) {
					try {
						if (layer != null) {
							dc.setCurrentLayer(layer);

							// hide terrain layers when asked
							if (displayorHideTerrain || !(layer instanceof TiledImageLayer
									|| layer instanceof ShaderElevationLayer || layer instanceof BMNGOneImage)) {
								dc.setCurrentLayer(layer);
								// transparency shader for virtual earth
								if (layer.getName().contains("Virtual Earth")) {
									int shaderprogram = shaderprograms.get(dc.getGL());
									dc.getGL().getGL2().glUseProgram(shaderprogram);
									try {
										layer.render(dc);
									} finally {
										// inhibit shader
										dc.getGL().getGL2().glUseProgram(0);
									}
								} else {
									layer.render(dc);
								}
							}
						}
					} catch (Exception e) {
						String message = Logging.getMessage("SceneController.ExceptionWhileRenderingLayer",
								layer != null ? layer.getClass().getName() : Logging.getMessage("term.unknown"));
						Logging.logger().log(Level.SEVERE, message, e);
						if (layer != null) {
							layer.setEnabled(false);
							Messages.openErrorMessage(message,
									"Layer '" + layer.getName() + "' caused an error. It is disabled for security", e);
						}
						// Don't abort; continue on to the next layer.
					}
				}

				dc.setCurrentLayer(null);
			}

			// Draw the deferred/ordered surface renderables.
			this.drawOrderedSurfaceRenderables(dc);

			if (this.isDeferOrderedRendering())
				return;

			if (this.screenCreditController != null)
				this.screenCreditController.render(dc);

			// Draw the deferred/ordered renderables.
			dc.setOrderedRenderingMode(true);
			// dc.applyGroupingFilters();
			dc.applyClutterFilter();
			while (dc.peekOrderedRenderables() != null) {
				try {
					dc.pollOrderedRenderables().render(dc);
				} catch (Exception e) {
					Logging.logger().log(Level.WARNING,
							Logging.getMessage("BasicSceneController.ExceptionDuringRendering"), e);
				}
			}
			dc.setOrderedRenderingMode(false);

			// Draw the diagnostic displays.
			if (dc.getSurfaceGeometry() != null && dc.getModel() != null && (dc.getModel().isShowWireframeExterior()
					|| dc.getModel().isShowWireframeInterior() || dc.getModel().isShowTessellationBoundingVolumes())) {
				Model model = dc.getModel();

				float[] previousColor = new float[4];
				GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
				gl.glGetFloatv(GL2.GL_CURRENT_COLOR, previousColor, 0);

				for (SectorGeometry sg : dc.getSurfaceGeometry()) {
					if (model.isShowWireframeInterior() || model.isShowWireframeExterior())
						sg.renderWireframe(dc, model.isShowWireframeInterior(), model.isShowWireframeExterior());

					if (model.isShowTessellationBoundingVolumes()) {
						gl.glColor3d(1, 0, 0);
						sg.renderBoundingVolume(dc);
					}
				}

				gl.glColor4fv(previousColor, 0);
			}
		} catch (Throwable e) {
			Logging.logger().log(Level.SEVERE, Logging.getMessage("BasicSceneController.ExceptionDuringRendering"), e);
		}
	}

	public boolean isDisplayorHideTerrain() {
		return displayorHideTerrain;
	}

	public void setDisplayorHideTerrain() {
		this.displayorHideTerrain = !displayorHideTerrain;
	}
	public Optional<GeoBox> getDisplayedGeoBox() {
		return displayedGeoBox;
	}


}
