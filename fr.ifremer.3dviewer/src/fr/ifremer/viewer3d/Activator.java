package fr.ifremer.viewer3d;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.viewer3d.preference.IsobathsPreference;
import fr.ifremer.viewer3d.preference.PalettesPreference;
import fr.ifremer.viewer3d.preference.Viewer3DPreferences;
import gov.nasa.worldwind.Configuration;
import jakarta.inject.Inject;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "fr.ifremer.3dviewer"; //$NON-NLS-1$

	// The shared instance
	private static Activator INSTANCE;

	private static BundleContext bundleContext;

	/** Fabrique de {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	// plugin preferences
	private static Viewer3DPreferences param;

	public static Viewer3DPreferences getPluginParameters() {
		return param;
	}

	/** Preferences of the isobaths */
	private static IsobathsPreference isobathsPreference;
	private static PalettesPreference palettesPreference;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		INSTANCE = this;
		super.start(context);
		Activator.bundleContext = context;

		ContextInjectionFactory.inject(this, EclipseContextFactory.getServiceContext(Activator.getBundleContext()));

		// plugin general preference
		PreferenceRegistry preferences = PreferenceRegistry.getInstance();
		param = new Viewer3DPreferences(preferences.getViewsSettingsNode(), "3D Viewer");

		// preferences
		isobathsPreference = new IsobathsPreference(preferences.getGlobalSettingsNode());
		palettesPreference = new PalettesPreference(preferences.getColorsSettingsNode());

		Configuration.insertConfigurationDocument("config/3DViewer.xml");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		INSTANCE = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return INSTANCE;
	}

	/**
	 * Accesseur de arrayFactory
	 */
	public IArrayFactory getArrayFactory() {
		return arrayFactory;
	}

	/**
	 * Accesseur de arrayFactory.<br>
	 * Invoked by DS to inject the service defined in OSGI_INF/ArrayFactoryRegistry.xml
	 */
	public void setArrayFactory(IArrayFactory arrayFactory) {
		INSTANCE.arrayFactory = arrayFactory;
	}

	/**
	 * @return the {@link #isobathsPreference}
	 */
	public static IsobathsPreference getIsobathsPreference() {
		return isobathsPreference;
	}

	/**
	 * @param isobathsPreference the {@link #isobathsPreference} to set
	 */
	public static void setIsobathsPreference(IsobathsPreference isobathsPreference) {
		Activator.isobathsPreference = isobathsPreference;
	}

	/**
	 * @return the {@link #bundleContext}
	 */
	public static BundleContext getBundleContext() {
		return bundleContext;
	}

	/**
	 * @return the {@link #palettesPreference}
	 */
	public static PalettesPreference getPalettesPreference() {
		return palettesPreference;
	}
}
