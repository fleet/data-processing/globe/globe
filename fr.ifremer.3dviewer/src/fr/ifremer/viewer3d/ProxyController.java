package fr.ifremer.viewer3d;

import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import jakarta.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.util.conf.ProxyConf;
import fr.ifremer.viewer3d.util.conf.ProxyConf.ProxyMode;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.util.Logging;

@Creatable
@Singleton
public class ProxyController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProxyController.class);

	private static final String OFFLINE_SITE_TEST = "https://wwz.ifremer.fr/";

	/** System proxy configuration. */
	private String systemProxyHost = null;
	/** System proxy configuration. */
	private String systemProxyPort = null;
	/** System proxy configuration. */
	private String systemProxyType = "Proxy.Type.Direct";

	/**
	 * Detects system proxy configuration.
	 */
	public void detectProxy() {
		// auto-detection from system settings
		System.setProperty("java.net.useSystemProxies", "true");
		try {
			List<Proxy> l = ProxySelector.getDefault().select(new URI("http://www.nasa.network.com/"));

			// returns only proxies allowing access to www.nasa.network.com
			// use only first one
			for (Proxy proxy : l) {
				Logging.logger().finer("Proxy hostname : " + proxy.type());
				switch (proxy.type()) {
				case HTTP:
					systemProxyType = "Proxy.Type.Http";
					break;
				case SOCKS:
					systemProxyType = "Proxy.Type.SOCKS";
					break;
				case DIRECT:
					systemProxyType = "Proxy.Type.Direct";
				default:

					break;
				}
				InetSocketAddress addr = (InetSocketAddress) proxy.address();
				if (addr == null) {
					Logging.logger().finer("No Proxy");
				} else {
					systemProxyHost = addr.getHostName();
					systemProxyPort = String.valueOf(addr.getPort());
					Logging.logger().finer("Proxy hostname : " + addr.getHostName());
					Logging.logger().finer("Proxy port : " + addr.getPort());
				}
			}
		} catch (URISyntaxException e) {
			Logging.logger().severe("Error during proxy detection " + e);
		}
	}

	/**
	 * Initialize proxy settings.
	 *
	 * @param autodetect if autodetection is true other parameters are ignored
	 * @param host Host name or ip address. If null => auto-detection.
	 * @param port Proxy port (eg. Squid default port is 3128). May be null
	 * @param type Default is Proxy.Type.Direct (no proxy). May be null.
	 */
	public void initProxy(final ProxyConf pc) {
		if (pc == null) {
			return;
		}

		String host = null;
		String port = null;
		String type = "Proxy.Type.DIRECT";

		if (ProxyMode.NO_PROXY.equals(pc.getMode())) {
			// no proxy
			System.setProperty("java.net.useSystemProxies", "false");
			host = null;
			port = null;
			type = "Proxy.Type.Direct";
		} else if (ProxyMode.MANUAL.equals(pc.getMode())) {
			// manual configuration
			System.setProperty("java.net.useSystemProxies", "false");
			host = pc.getHost();
			port = pc.getPort();
			type = pc.getType();
			System.setProperty("http.proxyHost", host);
			System.setProperty("http.proxyPort", port);
			System.setProperty("https.proxyHost", host);
			System.setProperty("https.proxyPort", port);

			if (!pc.getLogin().equals("")) {
				Authenticator.setDefault(new Authenticator() {
					@Override
					public PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(pc.getLogin(), pc.getPassword().toCharArray());
					}
				});

				System.setProperty("http.proxyUser", pc.getLogin());
				System.setProperty("http.proxyPassword", pc.getPassword());
			}

		} else if (ProxyMode.SYSTEM_PROXY.equals(pc.getMode())) {
			System.setProperty("java.net.useSystemProxies", "true");

			host = systemProxyHost;
			port = systemProxyPort;
			type = systemProxyType;

			Authenticator authenticator = new Authenticator() {

				@Override
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("user", "password".toCharArray());
				}
			};
			Authenticator.setDefault(authenticator);
		}
		if (host != null && host != "") {
			Configuration.setValue(AVKey.URL_PROXY_HOST, host);
		} else {
			Configuration.removeKey(AVKey.URL_PROXY_HOST);
		}
		if (port != null && port != "") {
			Configuration.setValue(AVKey.URL_PROXY_PORT, port);
		} else {
			Configuration.removeKey(AVKey.URL_PROXY_PORT);
		}
		if (type != null) {
			Configuration.setValue(AVKey.URL_PROXY_TYPE, type);
		}

		// if online checks configuration
		if (!WorldWind.isOfflineMode()) {
			try {
				if (pc.getHost() == null) {
					new URL(OFFLINE_SITE_TEST).openConnection().connect();
				} else {
					int proxyPort = Configuration.getIntegerValue(AVKey.URL_PROXY_PORT);
					String proxyHost = Configuration.getStringValue(AVKey.URL_PROXY_HOST);
					SocketAddress addr = new InetSocketAddress(proxyHost, proxyPort);
					new URL(OFFLINE_SITE_TEST).openConnection(new Proxy(Proxy.Type.HTTP, addr)).connect();
				}

			} catch (Exception e) {
				MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Internet connection failed",
						"Internet connection failed you have been switched to offline mode.");
				LOGGER.warn("Error while retrieve worldwind web site", e);
				WorldWind.setOfflineMode(true);
				return;
			}
		}
	}

}
