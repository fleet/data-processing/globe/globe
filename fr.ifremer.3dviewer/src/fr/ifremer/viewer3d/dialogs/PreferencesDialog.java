package fr.ifremer.viewer3d.dialogs;

import java.util.Optional;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import fr.ifremer.viewer3d.ProxyController;
import fr.ifremer.viewer3d.preference.NasaWorldWindDetailHint;
import fr.ifremer.viewer3d.preference.NasaWorldWindDetailHint.DetailHintComposite;
import fr.ifremer.viewer3d.util.conf.ProxyConf;
import fr.ifremer.viewer3d.util.conf.ProxyConf.ProxyMode;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import gov.nasa.worldwind.WorldWind;

public class PreferencesDialog extends Dialog {

	private final ProxyController proxyController;
	
	// SWT Widgets
	private Button workOfflineButton;
	private Button noProxyButton;
	private Button systemProxyButton;
	private Button manualProxyButton;
	private Label hostLabel;
	private Text hostText;
	private Label portLabel;
	private Spinner portSpinner;
	private Label userLabel;
	private Text userText;
	private Label passwordLabel;
	private Text passwordText;
	private Label cacheSizeLabel;
	private Text cacheSizeText;
	private Label gpuCacheSizeLabel;
	private Text gpuCacheSizeText;
	protected DetailHintComposite detailHintComposite;

	public PreferencesDialog(Shell parentShell, ProxyController proxyController) {
		super(parentShell);
		this.proxyController = proxyController;
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			WorldWind.setOfflineMode(isOffline());
			// reloads configuration
			SSVConfiguration configuration = SSVConfiguration.getConfiguration();
			ProxyConf pc = configuration.getProxy();
			configuration.setOfflineMode(isOffline());
			pc.setLOD(detailHintComposite.getTerrainDetailHint());
			pc.setImageLevelOfDetail(detailHintComposite.getImageDetailHint());
			if (isManualProxy()) {
				pc.setMode(ProxyMode.MANUAL);
				pc.setHost(getHost());
				pc.setPort(String.valueOf(getPort()));
				pc.setType("Proxy.Type.Http");
				pc.setLogin(userText.getText());
				pc.setPassword(passwordText.getText());

			} else {
				if (isNoProxy()) {
					pc.setMode(ProxyMode.NO_PROXY);
				} else {
					pc.setMode(ProxyMode.SYSTEM_PROXY);
				}
				pc.setHost(null);
				pc.setPort(null);
				pc.setType(null);
			}
			proxyController.initProxy(pc);
			getCacheSize().ifPresent(configuration::setCacheSize);
			getGpuCacheSize().ifPresent(configuration::setGpuCacheSize);
			configuration.save();
		} else {
		}
		super.buttonPressed(buttonId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Preferences");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(2, false));

		Label labelImage = new Label(composite, SWT.NONE);
		labelImage.setImage(ImageResources.getImage("/icons/preferences.png", getClass()));
		labelImage.setLayoutData(new GridData(GridData.FILL_HORIZONTAL, GridData.CENTER, true, false, 1, 3));
		labelImage.setFont(parent.getFont());

		Group group = new Group(composite, SWT.NONE);
		group.setText("Scene detail");
		group.setLayout(new FillLayout());
		group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));

		NasaWorldWindDetailHint nasaWorldWindDetailHint = SSVConfiguration.getConfiguration()
				.getNasaWorldWindDetailHint();
		detailHintComposite = nasaWorldWindDetailHint.initializeView(group, SWT.NONE);

		Group connexionGroup = new Group(composite, SWT.NONE);
		connexionGroup.setText("Connexion");
		GridLayout layout = new GridLayout(4, false);
		layout.marginHeight = 10;
		layout.verticalSpacing = 10;
		connexionGroup.setLayout(layout);
		connexionGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		workOfflineButton = new Button(connexionGroup, SWT.CHECK);
		noProxyButton = new Button(connexionGroup, SWT.RADIO);
		systemProxyButton = new Button(connexionGroup, SWT.RADIO);
		manualProxyButton = new Button(connexionGroup, SWT.RADIO);
		hostLabel = new Label(connexionGroup, SWT.NONE);
		hostText = new Text(connexionGroup, SWT.BORDER);
		portLabel = new Label(connexionGroup, SWT.NONE);
		portSpinner = new Spinner(connexionGroup, SWT.BORDER);
		userLabel = new Label(connexionGroup, SWT.NONE);
		userText = new Text(connexionGroup, SWT.BORDER);
		passwordLabel = new Label(connexionGroup, SWT.NONE);
		passwordText = new Text(connexionGroup, SWT.BORDER);

		workOfflineButton.setText("Work offline");
		workOfflineButton.setSelection(WorldWind.isOfflineMode());
		workOfflineButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				updateEnableWidgets();
			}
		});
		workOfflineButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 4, 1));

		noProxyButton.setText("No proxy");
		noProxyButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				updateEnableWidgets();
			}
		});
		noProxyButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 4, 1));

		systemProxyButton.setText("Use system proxy settings");
		systemProxyButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				updateEnableWidgets();
			}
		});
		systemProxyButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 4, 1));

		manualProxyButton.setText("Manual proxy configuration :");
		manualProxyButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				updateEnableWidgets();
			}
		});
		manualProxyButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 4, 1));

		hostLabel.setText("Host : ");
		hostLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		hostText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		portLabel.setText("Port : ");
		portLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		portSpinner.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		portSpinner.setMaximum(9999);

		userLabel.setText("User : ");

		userText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		passwordLabel.setText("Password : ");

		passwordText.setEchoChar('*');
		passwordText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		Group cacheGroup = new Group(composite, SWT.NONE);
		cacheGroup.setText("Memory Cache");
		GridLayout memoryLayout = new GridLayout(4, true);
		memoryLayout.marginHeight = 10;
		memoryLayout.verticalSpacing = 10;
		cacheGroup.setLayout(memoryLayout);
		cacheGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		cacheSizeLabel = new Label(cacheGroup, SWT.NONE);
		cacheSizeLabel.setText("RAM size for tiling 3D Viewer process ( in GB): ");
		cacheSizeLabel.setToolTipText("This indicator is the limit size for the internal process for tiling terrains.");
		cacheSizeLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));
		cacheSizeText = new Text(cacheGroup, SWT.BORDER);
		cacheSizeText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		gpuCacheSizeLabel = new Label(cacheGroup, SWT.NONE);
		gpuCacheSizeLabel.setText("Texture cache size ( in GB): ");
		gpuCacheSizeLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));
		gpuCacheSizeText = new Text(cacheGroup, SWT.BORDER);
		gpuCacheSizeText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// init GUI values
		initValues();

		// update widget
		updateEnableWidgets();

		// errorMessageText = new Text(composite, SWT.READ_ONLY | SWT.WRAP);
		// errorMessageText.setLayoutData(new GridData(GridData.FILL,
		// GridData.CENTER, true, false, 2, 1));
		// errorMessageText.setBackground(errorMessageText.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		// setErrorMessage(errorMessage);

		return composite;
	}

	/**
	 * Init GUI values.
	 */
	private void initValues() {
		SSVConfiguration configuration = SSVConfiguration.getConfiguration();
		ProxyConf pc = configuration.getProxy();
		if (pc == null) {
			systemProxyButton.setSelection(true);
		} else {

			boolean offline = WorldWind.isOfflineMode();
			workOfflineButton.setSelection(offline);

			if (ProxyMode.NO_PROXY.equals(pc.getMode())) {
				noProxyButton.setSelection(true);
			} else if (ProxyMode.SYSTEM_PROXY.equals(pc.getMode())) {
				systemProxyButton.setSelection(true);
			} else if (ProxyMode.MANUAL.equals(pc.getMode())) {
				manualProxyButton.setSelection(true);
				String host = (pc.getHost() != null) ? pc.getHost() : "";
				hostText.setText(host);
				int portNb = Integer.parseInt(pc.getPort());
				try {
					portNb = Integer.valueOf(pc.getPort());
				} catch (NumberFormatException e) {

				}
				portSpinner.setSelection(portNb);
				this.userText.setText(pc.getLogin());
				this.passwordText.setText(pc.getPassword());
			}
		}
		cacheSizeText.setText(String.valueOf(configuration.getCacheSize() / Math.pow(10, 9)));
		gpuCacheSizeText.setText(String.valueOf(configuration.getGpuCacheSize() / Math.pow(10, 9)));
	}

	/**
	 * Updates enable state of input widgets.
	 */
	private void updateEnableWidgets() {
		boolean enabled = true;
		boolean manualSelected = manualProxyButton.getSelection();

		// if work offline selected : disable all widgets
		if (workOfflineButton.getSelection()) {
			enabled = false;
			manualSelected = false;
		}

		noProxyButton.setEnabled(enabled);
		systemProxyButton.setEnabled(enabled);
		manualProxyButton.setEnabled(enabled);

		hostLabel.setEnabled(manualSelected);
		hostText.setEnabled(manualSelected);
		portLabel.setEnabled(manualSelected);
		portSpinner.setEnabled(manualSelected);
		passwordLabel.setEnabled(manualSelected);
		passwordText.setEnabled(manualSelected);
		userLabel.setEnabled(manualSelected);
		userText.setEnabled(manualSelected);
	}

	/**
	 * Returns true if "work offline" button is selected.
	 */
	public boolean isOffline() {
		return workOfflineButton.getSelection();
	}

	/**
	 * Returns true if "no proxy" button is selected.
	 */
	public boolean isNoProxy() {
		return noProxyButton.getSelection();
	}

	/**
	 * Returns true if "system proxy" button is selected.
	 */
	public boolean isSystemProxy() {
		return systemProxyButton.getSelection();
	}

	/**
	 * Returns true if "manual proxy" button is selected.
	 */
	public boolean isManualProxy() {
		return manualProxyButton.getSelection();
	}

	public String getHost() {
		return hostText.getText();
	}

	public int getPort() {
		int port = 3128;
		try {
			port = portSpinner.getSelection();
		} catch (NumberFormatException e) {
		}
		return port;
	}

	public Optional<Long> getCacheSize() {
		try {
			return Optional.of(Math.round(Double.parseDouble(cacheSizeText.getText()) * Math.pow(10, 9)));
		} catch (NumberFormatException ex) {
			return Optional.empty();
		}
	}

	public Optional<Long> getGpuCacheSize() {
		try {
			return Optional.of(Math.round(Double.parseDouble(gpuCacheSizeText.getText()) * Math.pow(10, 9)));
		} catch (NumberFormatException ex) {
			return Optional.empty();
		}
	}

}
