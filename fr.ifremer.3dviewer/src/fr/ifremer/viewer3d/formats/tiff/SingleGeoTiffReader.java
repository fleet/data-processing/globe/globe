package fr.ifremer.viewer3d.formats.tiff;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SingleGeoTiffReader extends GeotiffReader {

	public SingleGeoTiffReader(File geotiffFile) throws IOException {
		super(geotiffFile);
	}
	/**
	 * read geotiff file Header for a single file or for all tiff files in the same repository
	 * @throws IOException 
	 * */
	@Override
	protected void readFileHeader(String sourceFilename) throws IOException
	{
		this.tiffIFDs = null;
		metadata = null;
		this.sourceFile = new RandomAccessFile(sourceFilename, "r");
		this.theChannel = this.sourceFile.getChannel();

		this.tiffReader = new TIFFReader(this.theChannel,sourceFilename);

		readTiffHeaders();
	}
}
