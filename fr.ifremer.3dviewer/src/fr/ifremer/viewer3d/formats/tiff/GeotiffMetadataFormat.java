/*
 * Copyright (C) 2011 United States Government as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */
package fr.ifremer.viewer3d.formats.tiff;

import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadataFormatImpl;

/**
 * @author brownrigg
 * @version $Id: GeotiffMetadataFormat.java,v 1.3 2012/11/12 15:09:18 bvalliere
 *          Exp $
 */

public class GeotiffMetadataFormat extends IIOMetadataFormatImpl {

	public GeotiffMetadataFormat() {
		super(null, 0);
	}

	@Override
	public boolean canNodeAppear(String elementName, ImageTypeSpecifier imageType) {
		return false; // To change body of implemented methods use File |
						// Settings | File Templates.
	}
}
