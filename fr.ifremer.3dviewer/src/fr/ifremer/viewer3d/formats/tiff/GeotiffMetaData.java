/*
 * Copyright (C) 2011 United States Government as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */
package fr.ifremer.viewer3d.formats.tiff;

import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;

import org.w3c.dom.Node;

/**
 * @author brownrigg
 * @version $Id: GeotiffMetaData.java,v 1.3 2012/11/12 15:09:18 bvalliere Exp $
 */

public class GeotiffMetaData extends IIOMetadata {

	@Override
	public boolean isReadOnly() {
		return false; // To change body of implemented methods use File |
						// Settings | File Templates.
	}

	@Override
	public Node getAsTree(String formatName) {
		return null; // To change body of implemented methods use File |
						// Settings | File Templates.
	}

	@Override
	public void mergeTree(String formatName, Node root) throws IIOInvalidTreeException {
		// To change body of implemented methods use File | Settings | File
		// Templates.
	}

	@Override
	public void reset() {
		// To change body of implemented methods use File | Settings | File
		// Templates.
	}
}
