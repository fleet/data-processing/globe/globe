package fr.ifremer.viewer3d.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * Defines a swing file filter using a file extensions list.
 * 
 * @see #addAllowedExtensions(String)
 * @author G. Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class MultiFileFilter extends FileFilter {

	/** List of allowed file extensions. */
	protected List<String> extensions = new ArrayList<String>();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription() {
		return DicoBundle.getString("ALL_KNOWN_FILE_FORMATS"); //$NON-NLS-1$
	}

	/**
	 * Add a new file extension allowed by this filter. <br/>
	 * The extension may be provided with or without the dot.
	 * 
	 * @param value
	 *            the file extension to add
	 */
	public void addAllowedExtensions(String[] value) {
		if (value != null) {
			for (int i = 0; i < value.length; i++) {
				String extension = value[i];
				if (extension != null && !extensions.contains(extension)) {
					extensions.add(extension);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean accept(File file) {
		if (file != null) {
			if (file.isDirectory()) {
				return true;
			}
			String filename = file.getName();
			for (String extension : extensions) {
				if (filename.endsWith(extension)) {
					return true;
				}
			}
		}
		return false;
	}
}
