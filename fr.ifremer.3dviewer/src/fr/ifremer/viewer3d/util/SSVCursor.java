/**
 * 
 */
package fr.ifremer.viewer3d.util;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

/**
 * Custom cursors for SonarScope-3DViewer.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class SSVCursor {

	/**
	 * Curseur pour la selection d'une zone.
	 * 
	 * @see #getPredefinedCursor(int)
	 */
	public final static int CURSOR_ZONE = 0;

	/**
	 * Curseur pour la selection d'un point.
	 * 
	 * @see #getPredefinedCursor(int)
	 */
	public final static int CURSOR_POINT = 1;

	private final static String IMG_ZONE_CURSOR = "/config/cursors/cursor-zone.gif";
	private final static String IMG_POINT_CURSOR = "/config/cursors/cursor-point.gif";

	public static Cursor cursorZone;
	public static Cursor cursorPoint;

	/**
	 * Charge le curseur à partir du type passé en paramètre.
	 * 
	 * @param cursorType
	 * @return
	 */
	public static Cursor getPredefinedCursor(int cursorType) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Point hotSpot = new Point(0, 0);
		Cursor value = null;
		switch (cursorType) {
		case CURSOR_ZONE:
			synchronized (IMG_ZONE_CURSOR) {
				if (cursorZone == null) {
					Image imgZone = toolkit.getImage(SSVCursor.class.getResource(IMG_ZONE_CURSOR));
					cursorZone = toolkit.createCustomCursor(imgZone, hotSpot, "SelectZone");
				}
			}
			value = cursorZone;
			break;
		case CURSOR_POINT:
			synchronized (IMG_POINT_CURSOR) {
				if (cursorPoint == null) {
					Image imgPt = toolkit.getImage(SSVCursor.class.getResource(IMG_POINT_CURSOR));
					cursorPoint = toolkit.createCustomCursor(imgPt, hotSpot, "SelectPoint");
				}
			}
			value = cursorPoint;
			break;
		}

		return value;
	}

}
