package fr.ifremer.viewer3d.util.contextmenu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Content definition for context menus.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class ContextMenuInfo {
	private String title;
	private String subTitle;
	private List<ContextMenuItemInfo> menuItems;

	public ContextMenuInfo(String title) {
		this(title, null);
	}

	public ContextMenuInfo(String title, Collection<ContextMenuItemInfo> menuItems) {
		this.title = title;
		this.subTitle = null;
		if (menuItems != null) {
			this.menuItems = new ArrayList<ContextMenuItemInfo>(menuItems);
		} else {
			this.menuItems = new ArrayList<ContextMenuItemInfo>();
		}
	}

	public String getTitle() {
		return this.title;
	}

	public String getSubTitle() {
		return this.subTitle;
	}

	public List<ContextMenuItemInfo> getItems() {
		return this.menuItems;
	}

	/**
	 * Returns this menu info content as HTML.
	 */
	public String getHTMLContent() {
		StringBuilder sb = new StringBuilder("<html>");
		if (this.title != null) {
			sb.append("<b><font color=\"");
			sb.append(getTitleColor());
			sb.append("\">");
			sb.append(this.title);
			sb.append("</font></b><br/>");
		}
		for (ContextMenuItemInfo cmii : this.menuItems) {
			// sb.append("<p>");
			sb.append(cmii.getHTMLContent());
			sb.append("<br/>");
		}
		sb.append("</html>");
		return sb.toString();
	}

	/**
	 * Returns title color in HTML format (eg. "#664400").
	 */
	public String getTitleColor() {
		// TODO configuration
		return "#664400";
	}

	/**
	 * Appends a new item to this menu.
	 * 
	 * @param item
	 *            the item to add
	 */
	public void addItem(ContextMenuItemInfo item) {
		this.menuItems.add(item);
	}
}