package fr.ifremer.viewer3d.util.contextmenu;

import gov.nasa.worldwind.event.SelectEvent;

/**
 * Definition of one context menu item.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class ContextMenuItemInfo {
	/** This menu item's label. */
	protected String text;
	/**
	 * Associated action name.
	 * 
	 * @see #setAction(String)
	 */
	protected ContextMenuItemAction action;
	/**
	 * @see ContextMenuItemInfo#isHighlighted()
	 */
	private boolean highlighted = false;

	/**
	 * Ctor.
	 * 
	 * @param displayString
	 *            the menu item's label
	 */
	public ContextMenuItemInfo(String displayString) {
		this.text = displayString;
	}

	/**
	 * Returns this item label.
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * Returns this menu item content HTML formated.
	 * 
	 * @return
	 */
	public String getHTMLContent() {
		if (action == null) {
			return this.text;
		}

		StringBuilder sb = new StringBuilder();
		if (highlighted) {
			sb.append("<b>");
		}
		sb.append("<a href=\"");
		sb.append(this.action.getName());
		sb.append("\">");
		sb.append(this.text);
		sb.append("</a>");
		if (highlighted) {
			sb.append("</b>");
		}
		return sb.toString();
	}

	/**
	 * Returns this item associated action.
	 * 
	 * @see #setAction(String)
	 */
	public ContextMenuItemAction getAction() {
		return this.action;
	}

	/**
	 * Define action associated with this menu item.
	 * <p>
	 * This action triggers a {@link SelectEvent} and the action may be
	 * retrieved using <i>getValue(AVKey.URL)</i> on the event top object.
	 * </p>
	 * 
	 * @param action
	 *            the action to define
	 */
	public void setAction(ContextMenuItemAction action) {
		this.action = action;
	}

	/**
	 * @see #isHighlighted()
	 */
	public void setHighlighted(boolean value) {
		this.highlighted = value;
	}

	/**
	 * Returns <code>true</code> if this item is currently highlighted (ie. item
	 * enabled and mouse pointer over this item).
	 */
	public boolean isHighlighted() {
		return this.highlighted;
	}
}