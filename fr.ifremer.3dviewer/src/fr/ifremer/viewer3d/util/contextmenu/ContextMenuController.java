package fr.ifremer.viewer3d.util.contextmenu;

import java.awt.Color;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;

import org.eclipse.e4.core.services.events.IEventBroker;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import fr.ifremer.viewer3d.render.SSVContextMenu;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.render.PointPlacemark;

public class ContextMenuController implements SelectListener {

	public static final String CONTEXT_MENU_INFO_KEY = "CONTEXT_MENU_INFO_KEY";

	public static final String CONTEXT_MENU_EVENT_TOPIC = "CONTEXT_MENU_EVENT_TOPIC";

	public static final String CONTEXT_MENU_PICKED_OBJECT_KEY = "CONTEXT_MENU_PICKED_OBJECT_KEY";
	

	/** Layer for context menu. */
	private final RenderableLayer contextMenuLayer = new RenderableLayer();

	/**
	 * Constructor
	 */
	public ContextMenuController() {
		// nothing to do
	}

	/**
	 * Used for highlight / unhighlight.
	 */
	protected PointPlacemark lastPickedPlacemark = null;

	/**
	 * The controlled context menu. <code>Null</code> when not shown.
	 */
	private SSVContextMenu contextMenu;

	@Override
	public void selected(SelectEvent event) {
		var contextMenuPickedObject = getContextMenuPickedObject(event);
		String evtAction = event.getEventAction();
		// if menu is already active
		if (isMenuActive()) {
			// get optional URL
			Optional<String> optUrl = contextMenuPickedObject.isPresent()
					&& contextMenuPickedObject.get().getValue(AVKey.TEXT) != null
							? Optional.ofNullable((String) contextMenuPickedObject.get().getValue(AVKey.URL))
							: Optional.empty();

			// highlight selected item
			contextMenu.getInfo().getItems().forEach(menuItem -> menuItem.setHighlighted(optUrl.isPresent()
					&& menuItem.getAction() != null && optUrl.get().equals(menuItem.getAction().getName())));
			contextMenu.setText(contextMenu.getInfo().getHTMLContent());

			// send events on left click
			if (evtAction.equals(SelectEvent.LEFT_CLICK) && optUrl.isPresent() && !optUrl.get().isEmpty()) {
				Object source = contextMenu.getSource();
				hideContextMenu();
				OsgiUtils.getService(IEventBroker.class).send(CONTEXT_MENU_EVENT_TOPIC,
						new ContextMenuEvent(optUrl.get(), source));
				Optional.ofNullable(event.getMouseEvent()).ifPresent(MouseEvent::consume);
			}

			// hide menu after clicks
			if (evtAction.equals(SelectEvent.LEFT_CLICK) || evtAction.equals(SelectEvent.RIGHT_PRESS)
					|| evtAction.equals(SelectEvent.RIGHT_CLICK) ||
					// or if the cursor moves from the menu or the picked object
					(evtAction.equals(SelectEvent.ROLLOVER) && event.getObjects().stream().map(PickedObject::getObject)
							.noneMatch(obj -> obj == contextMenu
									|| obj == contextMenu.getValue(CONTEXT_MENU_PICKED_OBJECT_KEY)))) {
				hideContextMenu();
			}
		}

		if (event.getEventAction().equals(SelectEvent.ROLLOVER)) {
			highlight(event.getTopObject());
		} else if (event.getEventAction().equals(SelectEvent.RIGHT_CLICK) && contextMenu == null) {
			showContextMenu(event);
		}
	}

	/** Returns true if the context menu is currently shown. */
	private boolean isMenuActive() {
		return contextMenu != null;
	}

	private void highlight(Object o) {
		if (this.lastPickedPlacemark == o) {
			return; // same thing selected
		}

		// Turn off highlight if on.
		if (this.lastPickedPlacemark != null) {
			this.lastPickedPlacemark.setHighlighted(false);
			this.lastPickedPlacemark = null;
		}

		// Turn on highlight if object selected.
		if (o instanceof PointPlacemark) {
			this.lastPickedPlacemark = (PointPlacemark) o;
			this.lastPickedPlacemark.setHighlighted(true);
		}
	}

	@SuppressWarnings("unchecked")
	protected void showContextMenu(SelectEvent event) {
		ContextMenuInfo menuInfo = null;

		Object o = event.getTopObject();
		if (o instanceof AVList) { // Uses an AVList in order to be applicable to all shapes.
			AVList params = (AVList) o;
			menuInfo = (ContextMenuInfo) params.getValue(CONTEXT_MENU_INFO_KEY);
		} else if (o instanceof Supplier<?> && ((Supplier<?>) o).get() instanceof ContextMenuInfo) {
			menuInfo = ((Supplier<ContextMenuInfo>) o).get();
		}

		Object src = event.getSource();
		if (menuInfo != null && src instanceof WorldWindow) {
			WorldWindow ww = (WorldWindow) src;
			int height = ww.getView().getViewport().height;
			Point location = event.getPickPoint();
			location.y = height - location.y - 10;
			location.x -= 16;
			contextMenu = new SSVContextMenu(menuInfo, location, o);
			contextMenu.getAttributes().setDrawOffset(new Point(40, 0));
			contextMenu.getAttributes().setBorderColor(Color.BLACK);
			contextMenu.getAttributes().setBorderWidth(2);
			contextMenu.getAttributes().setCornerRadius(5);
			contextMenu.getAttributes().setOpacity(0.8);
			contextMenu.getAttributes().setInsets(new Insets(8, 10, 0, 10));
			contextMenu.setValue(CONTEXT_MENU_PICKED_OBJECT_KEY, event.getTopPickedObject().getObject());
			contextMenuLayer.addRenderable(contextMenu);
			IGeographicViewService.grab().addLayer(contextMenuLayer);
			IGeographicViewService.grab().refresh();
		}
	}

	private void hideContextMenu() {
		if (contextMenu != null) {
			contextMenuLayer.removeRenderable(contextMenu);
			contextMenu = null;
			IGeographicViewService.grab().removeLayer(contextMenuLayer);
			IGeographicViewService.grab().refresh();
		}
	}

	/**
	 * @return the {@link PickedObject} over context menu from an event.
	 */
	private Optional<PickedObject> getContextMenuPickedObject(SelectEvent event) {
		return Optional.ofNullable(event.getObjects()).stream()//
				.flatMap(Collection::stream)//
				.filter(pickedObject -> pickedObject.getObject() == contextMenu)//
				.map(PickedObject.class::cast).findFirst();
	}

}
