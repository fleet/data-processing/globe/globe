package fr.ifremer.viewer3d.util.contextmenu;

/**
 * The ContextMenuItemAction responds to user selection of a context menu item.
 * <p>
 * When the parent menu item is selected, an EventBus event is triggered using
 * {@link ContextMenuEvent} class with this action name.
 * </p>
 */
public class ContextMenuItemAction {
	/** This action name. */
	private final String actionName;

	public ContextMenuItemAction(String actionName) {
		this.actionName = actionName;
	}

	/** Returns action's name. */
	public String getName() {
		return this.actionName;
	}
}