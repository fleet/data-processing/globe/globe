package fr.ifremer.viewer3d.util.contextmenu;

/**
 * When a context menu action is triggered, a new event using this class is
 * thrown.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 * 
 * @see ContextMenuItemAction
 */
public class ContextMenuEvent {
	private final String actionName;

	private final Object source;

	public ContextMenuEvent(final String actionName, final Object sourceObject) {
		this.actionName = actionName;
		this.source = sourceObject;
	}

	/** Returns action's name. */
	public String getName() {
		return this.actionName;
	}

	/** Return source object for this event. */
	public Object getSource() {
		return this.source;
	}
}
