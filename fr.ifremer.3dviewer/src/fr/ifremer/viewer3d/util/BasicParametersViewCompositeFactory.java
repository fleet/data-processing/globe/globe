/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.util;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode.CheckState;

/**
 * Template method for factory of Composite for the ParametersView <br>
 * Pattern to make a Composite for a FileInfoNode or one of its sub layer node.
 *
 * @param <L> : Expected type of layer in LayerNode
 */
public abstract class BasicParametersViewCompositeFactory<L extends IWWLayer>
		implements IParametersViewCompositeFactory {

	/** Expected Class of layer in LayerNode */
	private final Class<L> layerClass;
	/** Expected ContentType in FileInfoNode */
	private final ContentType contentType;

	/**
	 * Constructor
	 */
	protected BasicParametersViewCompositeFactory(ContentType contentType, Class<L> layerClass) {
		this.contentType = contentType;
		this.layerClass = layerClass;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		if (selection instanceof FileInfoNode) {
			return getComposite((FileInfoNode) selection, parent);
		} else if (selection instanceof LayerNode) {
			return getComposite((LayerNode) selection, parent);
		} else if (layerClass.isInstance(selection)) {
			return Optional.of(makeComposite(parent, layerClass.cast(selection)));
		}

		// Not concerned
		return Optional.empty();
	}

	/** Composite for a FileInfoNode */
	public Optional<Composite> getComposite(FileInfoNode selection, Composite parent) {
		if (selection.getFileInfo().getContentType() == contentType) {
			FileInfoNode fileInfoNode = selection;
			for (TreeNode subNode : fileInfoNode.getChildren()) {
				if (subNode instanceof LayerNode && subNode.getState() == CheckState.TRUE) {
					var result = getComposite(subNode, parent);
					if (result.isPresent())
						return result;
				}
			}
			// No layer currently selected
			return Optional.of(new Composite(parent, SWT.NONE));
		}
		return Optional.empty();
	}

	/** Composite for a LayerNode */
	public Optional<Composite> getComposite(LayerNode layerNode, Composite parent) {
		if (layerNode.getLayer().isPresent() && layerClass.isInstance(layerNode.getLayer().get())) {
			return Optional.of(makeComposite(parent, layerClass.cast(layerNode.getLayer().get())));
		}
		return Optional.empty();
	}

	/** Delegate construction of the Composite to the subclass */
	protected abstract Composite makeComposite(Composite parent, L layer);

}
