package fr.ifremer.viewer3d.util;

import javax.swing.SwingUtilities;

import fr.ifremer.viewer3d.Viewer3D;

/***
 * force refresh of OpenGL view after IHM input
 * 
 * @author MORVAN
 * 
 */
public class RefreshOpenGLView {
	private static boolean oneWay = true;

	public static void fullRefresh() {
		refresh();
		redraw();
	}

	public static void redraw() {
		try {
			// Synchronize Main and EDT Thread to avoid time lag effects in layer refresh.
			if( !SwingUtilities.isEventDispatchThread()) {
				SwingUtilities.invokeLater(()->Viewer3D.getWwd().redraw());
			} else {
				Viewer3D.getWwd().redraw();
			}
		} catch (Exception e) {
			// Never mind
		}
	}
	protected static void refresh() {
		// Force SimplePathWall to recompute the model. Is it really usefull ?	
		if (oneWay) {
			Viewer3D.getWwd().getSceneController().setVerticalExaggeration(Viewer3D.getWwd().getSceneController().getVerticalExaggeration() + 0.00000000000000001);
		} else {
			Viewer3D.getWwd().getSceneController().setVerticalExaggeration(Viewer3D.getWwd().getSceneController().getVerticalExaggeration() - 0.00000000000000001);
		}
		oneWay = !oneWay;
	}
}
