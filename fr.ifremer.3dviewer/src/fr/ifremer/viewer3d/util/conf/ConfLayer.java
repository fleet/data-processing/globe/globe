/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import java.io.File;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Configuration values for any map layer.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@XStreamAlias("MapLayer")
public class ConfLayer {

	/** Layer name. */
	private String name;

	/** Layer name. */
	public String getName() {
		return name;
	}

	/** Layer name. */
	public void setName(String value) {
		name = value;
	}

	/** If this layer is currently enabled. */
	private boolean enabled;

	/** If this layer is currently enabled. */
	public boolean getEnabled() {
		return enabled;
	}

	/** If this layer is currently enabled. */
	public void setEnabled(boolean value) {
		enabled = value;
	}

	/** Data for this layer. */
	private Object data;

	/** Data for this layer */
	public Object getData() {
		return data;
	}

	/** Data for this layer */
	public void setData(Object value) {
		data = value;
	}

	/** File used to load this layer. */
	private File inputFile;

	/** File used to load this layer. */
	public File getInputFile() {
		return inputFile;
	}

	/** File used to load this layer. */
	public void setInputFile(File value) {
		this.inputFile = value;
	}

	/** Standalone layer (doesn't provide file input). */
	private boolean standalone;

	/** Standalone layer (doesn't provide file input). */
	public void setStandalone(boolean standalone) {
		this.standalone = standalone;
	}

	/** Standalone layer (doesn't provide file input). */
	public boolean isStandalone() {
		return standalone;
	}

	private String layerClass;

	public void setLayerClass(String layerClass) {
		this.layerClass = layerClass;
	}

	public String getLayerClass() {
		return layerClass;
	}

}
