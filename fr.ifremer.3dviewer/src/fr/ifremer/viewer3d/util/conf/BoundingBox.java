/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;

/**
 * Configuration values for a bounding box.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class BoundingBox {
	/**
	 * The location of the eye in geographic coordinates.
	 */
	private Position eyePosition;

	/**
	 * The location of the eye in geographic coordinates.
	 */
	public Position getEyePosition() {
		return eyePosition;
	}

	/**
	 * The location of the eye in geographic coordinates.
	 */
	public void setEyePosition(Position value) {
		eyePosition = value;
	}

	/**
	 * Get the center position of the OrbitView. The center position is used as
	 * the point about which the heading and pitch rotate. It is defined by the
	 * intersection of a ray from the eye position through the center of the
	 * viewport with the surface of the globe.
	 */
	private Position centerPosition;

	/**
	 * Get the center position of the OrbitView. The center position is used as
	 * the point about which the heading and pitch rotate. It is defined by the
	 * intersection of a ray from the eye position through the center of the
	 * viewport with the surface of the globe.
	 */
	public Position getCenterPosition() {
		return centerPosition;
	}

	/**
	 * Get the center position of the OrbitView. The center position is used as
	 * the point about which the heading and pitch rotate. It is defined by the
	 * intersection of a ray from the eye position through the center of the
	 * viewport with the surface of the globe.
	 */
	public void setCenterPosition(Position value) {
		centerPosition = value;
	}

	/**
	 * Pitch in degrees.
	 */
	private Angle pitch;

	/**
	 * Pitch in degrees.
	 */
	public Angle getPitch() {
		return pitch;
	}

	/**
	 * Pitch in degrees.
	 */
	public void setPitch(Angle value) {
		pitch = value;
	}

}
