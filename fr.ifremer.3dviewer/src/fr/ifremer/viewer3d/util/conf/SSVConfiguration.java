/*
 * @License@
 */
package fr.ifremer.viewer3d.util.conf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.data.MyTiledElevationProducer;
import fr.ifremer.viewer3d.data.MyTiledImageProducer;
import fr.ifremer.viewer3d.multidata.TiledMultiElevationProducer;
import fr.ifremer.viewer3d.multidata.TiledMultiTextureProducer;
import fr.ifremer.viewer3d.preference.NasaWorldWindDetailHint;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.cache.GpuResourceCache;
import gov.nasa.worldwind.util.Logging;

/**
 * Configuration values for SonarScope-3DViewer.
 * <p>
 * This is the main class for configuration and it provides method for loading and storing SonarScope-3DViewer's main
 * configuration such as GUI preferences, default layers activation ie. preferences other than user (session) data.
 * </p>
 * 
 * @see SessionConfiguration
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@XStreamAlias("SonarScope3D-Configuration")
public class SSVConfiguration {

	/**
	 * UI settings values.
	 */
	private UISettings uiSettings;

	/**
	 * Http proxy configuration values.
	 */
	private ProxyConf proxy;

	/**
	 * RAM size for tiling 3D Viewer process. This indicator is the limit size for the internal process for tiling
	 * terrains.
	 */
	private long cacheSize = 900_000_000;

	private long gpuCacheSize = 900_000_000;

	/** Manager of Detail Hints */
	@XStreamOmitField
	private NasaWorldWindDetailHint nasaWorldWindDetailHint;

	/**
	 * Http proxy configuration values.
	 */
	public ProxyConf getProxy() {
		if (proxy == null) {
			proxy = new ProxyConf();
		}
		return this.proxy;
	}

	/**
	 * Http proxy configuration values.
	 */
	public void setProxy(ProxyConf value) {
		this.proxy = value;
	}

	/**
	 * UI settings values.
	 */
	public UISettings getUISettings() {
		return this.uiSettings;
	}

	/**
	 * UI settings values.
	 */
	public void setuiSettings(UISettings value) {
		this.uiSettings = value;
	}

	/**
	 * Map layers configuration.
	 */
	private List<ConfLayer> mapLayers;

	/**
	 * Map layers configuration.
	 */
	public List<ConfLayer> getLayers() {
		if (this.mapLayers == null) {
			this.mapLayers = new ArrayList<>();
		}
		return this.mapLayers;
	}

	/**
	 * Map layers configuration.
	 */
	public void setLayers(List<ConfLayer> value) {
		this.mapLayers = value;
	}

	/**
	 * offline mode
	 */
	private boolean offlineMode;

	/** Configuration instance. */
	private static SSVConfiguration instance = null;

	/**
	 * @param offlineMode the offlineMode to set
	 */
	public void setOfflineMode(boolean offlineMode) {
		this.offlineMode = offlineMode;
	}

	/**
	 * @return the offlineMode
	 */
	public boolean isOfflineMode() {
		return this.offlineMode;
	}

	/**
	 * SonarScope-3DViewer configuration properties.
	 * 
	 * @see #getConfiguration()
	 * @see #readResolve()
	 */
	private SSVConfiguration() {
		this.uiSettings = new UISettings();
		this.mapLayers = new ArrayList<>();
	}

	public long getCacheSize() {
		if (cacheSize == 0) { // prevent problem of save
			cacheSize = MyTiledElevationProducer.CACHE_CAPACITY;
		}
		return cacheSize;
	}

	public void setCacheSize(long cacheSize) {
		this.cacheSize = cacheSize;

		MyTiledElevationProducer.CACHE_CAPACITY = cacheSize;
		MyTiledImageProducer.setCACHE_CAPACITY(cacheSize);
		TiledMultiElevationProducer.setCACHE_CAPACITY(cacheSize);
		TiledMultiTextureProducer.setCACHE_CAPACITY(cacheSize);

	}

	public long getGpuCacheSize() {
		if (gpuCacheSize == 0)
			gpuCacheSize = Configuration.getLongValue(AVKey.TEXTURE_CACHE_SIZE);
		return gpuCacheSize;
	}

	/**
	 * Sets the {@link GpuResourceCache} capacity (AVKey.TEXTURE_CACHE_SIZE).
	 * 
	 * @param gpuCacheSize
	 */
	public void setGpuCacheSize(long gpuCacheSize) {
		this.gpuCacheSize = gpuCacheSize;
		Configuration.setValue(AVKey.TEXTURE_CACHE_SIZE, gpuCacheSize);
		Optional.ofNullable(Viewer3D.getWwd())//
				.ifPresent(wwd -> Optional.ofNullable(wwd.getGpuResourceCache())//
						.ifPresent(cache -> cache.setCapacity(gpuCacheSize)));
	}

	/**
	 * Loads properties from default SonarScope-3DViewer configuration file in user home, if already loaded returns
	 * configuration instance.
	 * 
	 * @return loaded instance or null if loading fails.
	 */
	public static SSVConfiguration getConfiguration() {
		synchronized (SSVConfiguration.class) {
			if (instance == null) {
				File file = getConfigurationFile();
				SSVConfiguration conf = load(file);
				if (conf == null) {
					conf = new SSVConfiguration();
				}
				instance = conf;
				conf.nasaWorldWindDetailHint = new NasaWorldWindDetailHint();
				instance.save();

			}
		}

		return instance;
	}

	/**
	 * Loads properties from a SonarScope-3DViewer configuration file.
	 * 
	 * @return loaded instance or null if loading fails.
	 */
	protected static SSVConfiguration load(File file) {
		if (file != null && file.exists()) {
			try {
				// loads data from XML
				XStream xstream = new XStream();
				xstream.autodetectAnnotations(true);
				xstream.processAnnotations(SSVConfiguration.class);
				// required for osgi plugins (has its own classloader)
				xstream.setClassLoader(SSVConfiguration.class.getClassLoader());

				xstream.allowTypes(new Class[] { SSVConfiguration.class, ConfLayer.class });
				InputStream fis = new FileInputStream(file);
				SSVConfiguration value = (SSVConfiguration) xstream.fromXML(fis);
				return value;

			} catch (XStreamException e) {
				Logging.logger().severe("Error during configuration loading " + file.getAbsolutePath());
			} catch (FileNotFoundException e) {
				Logging.logger().severe("File not found during configuration loading " + file.getAbsolutePath());
			}
		} else {
			Logging.logger().fine("Configuration file doesn't exist yet.");
		}

		return null;
	}

	/**
	 * Saves properties to default SonarScope-3DViewer properties file to user home directory.
	 */
	public void save() {
		File file = getConfigurationFile();
		save(file);
	}

	/**
	 * Saves properties to default SonarScope-3DViewer properties file into specified file.
	 * 
	 * @param file the file where configuration should be written.
	 */
	protected void save(File file) {
		if (file != null) {
			XStream xstream = new XStream();
			xstream.autodetectAnnotations(true);

			String xml = xstream.toXML(this);

			try {
				FileWriter fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(xml);
				bw.close();
			} catch (IOException e) {
				Logging.logger().severe("Error while writing configuration file " + file.getPath());
			}
		}
	}

	/**
	 * Returns SSV configuration directory absolute path (defaults in user home).
	 */
	public static String configurationDirectory() {
		String home = System.getProperty("user.home");
		String dirName = ".globe/";
		if (home != null) {
			dirName = home + "/" + dirName;
		}
		return dirName;
	}

	/**
	 * Returns configuration file.
	 */
	private static File getConfigurationFile() {
		// find configuration directory in user home
		String dirName = configurationDirectory();
		File dir = new File(dirName);
		boolean exists = dir.exists();
		// if this directory doesn't exist yet creates it
		if (!exists) {
			exists = dir.mkdir();
		}
		if (exists) {
			String fileName = dirName + "/preferences.properties";
			return new File(fileName);
		} else {
			Logging.logger().warning("Can't find configuration directory" + dirName);
		}
		return null;
	}

	/**
	 * Getter of nasaWorldWindDetailHint
	 */
	public NasaWorldWindDetailHint getNasaWorldWindDetailHint() {
		return nasaWorldWindDetailHint;
	}

}
