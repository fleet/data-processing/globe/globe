/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import java.util.HashMap;
import java.util.Map;

/**
 * Configuration values for UI settings.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class UISettings {
	/** Default path for measure height profile. */
	public static final String EXPORT_PROFILE_PATH = "UISettings.EXPORT_PROFILE_PATH";

	/**
	 * Configuration for terrain profile.
	 */
	private TerrainProfileSettings terrainProfile;

	/**
	 * Map of default UI string values.
	 */
	private Map<String, String> defaultValues = new HashMap<String, String>();

	/**
	 * Ctor.
	 */
	public UISettings() {
	}

	/**
	 * Configuration for terrain profile.
	 */
	public TerrainProfileSettings getterrainProfile() {
		return this.terrainProfile;
	}

	/**
	 * Configuration for terrain profile.
	 */
	public void setTerrainProfile(TerrainProfileSettings value) {
		this.terrainProfile = value;
	}

	/**
	 * Ensure defaultValues isn't null for previous versions compatibility.
	 */
	private Map<String, String> getDefaultValues() {
		if (this.defaultValues == null) {
			this.defaultValues = new HashMap<String, String>();
		}
		return this.defaultValues;
	}

	/**
	 * Get default value associated with provided key (may be null).
	 */
	public String getDefault(String key) {
		return getDefaultValues().get(key);
	}

	/**
	 * Define default value associated with provided key (may be null).
	 * 
	 * @param key
	 *            the key used to access this value
	 * @param value
	 *            the value itself
	 */
	public void setDefault(String key, String value) {
		if (key == null) {
			return;
		}
		getDefaultValues().put(key, value);
	}
}
