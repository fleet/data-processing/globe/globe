/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Configuration values for macro layers.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@XStreamAlias("MacroLayer")
public class ConfMacroLayer {

	/**
	 * Ctor.
	 */
	public ConfMacroLayer() {
		this.layers = new ArrayList<ConfLayer>();
	}

	/**
	 * Name for this macro layer.
	 */
	private String name;

	/**
	 * Name for this macro layer.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Name for this macro layer.
	 */
	public void setName(String value) {
		name = value;
	}

	/**
	 * List of layers included in this macro layer.
	 */
	private List<ConfLayer> layers;

	/**
	 * List of layers included in this macro layer.
	 */
	public List<ConfLayer> getLayers() {
		return layers;
	}

	/**
	 * List of layers included in this macro layer.
	 */
	public void setlayers(List<ConfLayer> value) {
		layers = value;
	}

}
