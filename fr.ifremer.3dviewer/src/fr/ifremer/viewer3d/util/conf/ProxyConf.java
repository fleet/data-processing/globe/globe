/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Http proxy configuration values.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@XStreamAlias("proxy")
public class ProxyConf {

	/**
	 * Define level of detail, will be use as the value/100 of detailHint in all BasicElevationModel
	 */
	private double levelOfDetail = 0;

	/**
	 * Define level of detail, will be use as the value/100 of detailHint in all TiledImage
	 */
	private double imageLevelOfDetail = 0;

	public double getLOD() {
		return this.levelOfDetail;
	}

	public void setLOD(double value) {
		this.levelOfDetail = value;
	}

	/**
	 * Define available proxy modes.
	 */
	public enum ProxyMode {
		NO_PROXY, SYSTEM_PROXY, MANUAL
	}

	/**
	 * Proxy definition mode.
	 */
	private ProxyMode mode = ProxyMode.SYSTEM_PROXY;

	/**
	 * Proxy definition mode.
	 */
	public ProxyMode getMode() {
		return this.mode;
	}

	/**
	 * Proxy definition mode.
	 */
	public void setMode(ProxyMode value) {
		this.mode = value;
	}

	/**
	 * Host name or ip address.
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	private String host;

	private String login = "";
	private String password = "";

	/**
	 * Host name or ip address.
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	public String getHost() {
		return this.host;
	}

	/**
	 * Host name or ip address.
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	public void setHost(String value) {
		this.host = value;
	}

	/**
	 * Proxy port (eg. Squid default port is 3128).
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	private String port;

	/**
	 * Proxy port (eg. Squid default port is 3128).
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	public String getPort() {
		return this.port;
	}

	/**
	 * Proxy port (eg. Squid default port is 3128).
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	public void setPort(String value) {
		this.port = value;
	}

	/**
	 * Proxy type Proxy.Type.Http, Proxy.Type.SOCKS, Proxy.Type.Direct (default : no proxy).
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	private String type;

	/**
	 * Proxy type Proxy.Type.Http, Proxy.Type.SOCKS, Proxy.Type.Direct (default : no proxy).
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Proxy type Proxy.Type.Http, Proxy.Type.SOCKS, Proxy.Type.Direct (default : no proxy).
	 * <p>
	 * This value is only used if configuration mode is manual.
	 * </p>
	 */
	public void setType(String value) {
		this.type = value;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Getter of imageLevelOfDetail
	 */
	public double getImageLevelOfDetail() {
		return imageLevelOfDetail;
	}

	/**
	 * Setter of imageLevelOfDetail
	 */
	public void setImageLevelOfDetail(double imageLevelOfDetail) {
		this.imageLevelOfDetail = imageLevelOfDetail;
	}

}
