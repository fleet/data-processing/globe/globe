/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Configuration values for custom layers.
 * <p>
 * This configuration contains values defining custom layers pointing to a WMS
 * server an all its sub layers.
 * </p>
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@XStreamAlias("CustomLayer")
public class CustomLayer extends ConfLayer {
	/**
	 * WMS Server address. Eg. http://www.geosignal.org/cgi-bin/wmsmap?
	 */
	private String serverAddress;

	/**
	 * WMS Server address. Eg. http://www.geosignal.org/cgi-bin/wmsmap?
	 */
	public String getServerAddress() {
		return serverAddress;
	}

	/**
	 * WMS Server address. Eg. http://www.geosignal.org/cgi-bin/wmsmap?
	 */
	public void setServerAddress(String value) {
		serverAddress = value;
	}

	/**
	 * Server name (ie. label).
	 */
	private String serverName;

	/**
	 * Server name (ie. label).
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * Server name (ie. label).
	 */
	public void setServerName(String value) {
		serverName = value;
	}

	/**
	 * List of layers defined in this WMS server.
	 */
	private List<ConfLayer> layers;

	/**
	 * List of layers defined in this WMS server.
	 */
	public List<ConfLayer> getLayers() {
		return layers;
	}

	/**
	 * List of layers defined in this WMS server.
	 */
	public void setLayers(List<ConfLayer> value) {
		layers = value;
	}

}
