/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Configuration for terrain profile layer.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@XStreamAlias("TerrainProfile")
public class TerrainProfileSettings {

	/**
	 * If this layer is enabled (ie. visible).
	 */
	private boolean enabled;

	/**
	 * If this layer is enabled (ie. visible).
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * If this layer is enabled (ie. visible).
	 */
	public void setEnabled(boolean value) {
		enabled = value;
	}

	/**
	 * If this layer is minimized.
	 */
	private boolean minimized;

	/**
	 * If this layer is minimized.
	 */
	public boolean isMinimized() {
		return minimized;
	}

	/**
	 * If this layer is minimized.
	 */
	public void setMinimized(boolean value) {
		minimized = value;
	}

	/**
	 * If this layer is maximized.
	 */
	private boolean maximized;

	/**
	 * If this layer is maximized.
	 */
	public boolean isMaximized() {
		return maximized;
	}

	/**
	 * If this layer is maximized.
	 */
	public void setMaximized(boolean value) {
		maximized = value;
	}

}
