/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.conf;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.util.Logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;

/**
 * This class handle configuration for session data.
 * <p>
 * It contains session configuration data : loaded data such as bathymetry,
 * plume data, custom layers.
 * </p>
 * 
 * @see SSVConfiguration
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class SessionConfiguration {

	public static class Zone {
		private String name;
		/** Remarkable points. */
		private List<Position> points = new ArrayList<Position>();

		public void addPoint(Position pos) {
			if (pos != null) {
				points.add(pos);
			}
		}

		public List<Position> getPoints() {
			return points;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	/** Macro layers configuration. */
	private List<ConfMacroLayer> macroLayers;

	/** List of data files imported in this session. */
	private List<File> importedFiles;

	// /** List of data files imported in this session. */
	// private List<WMSServerDefinition> customLayers;

	/** Current view bounding box. */
	private BoundingBox boundingBox;

	/** Remarkable points. */
	private List<Position> points = new ArrayList<Position>();

	/** Remarkable zones. */
	private List<Zone> zones = new ArrayList<Zone>();

	public void addPoint(Position pos) {
		if (pos != null) {
			points.add(pos);
		}
	}

	public List<Position> getPoints() {
		return points;
	}

	public void addZone(Zone zone) {
		if (zone != null) {
			zones.add(zone);
		}
	}

	public List<Zone> getZones() {
		return zones;
	}

	/**
	 * Session configuration properties.
	 * 
	 * @see #load()
	 */
	public SessionConfiguration() {
		this.importedFiles = new ArrayList<File>();
		this.macroLayers = new ArrayList<ConfMacroLayer>();
		this.boundingBox = new BoundingBox();
		// this.customLayers = new ArrayList<WMSServerDefinition>();
	}

	/**
	 * Macro layers configuration.
	 */
	public List<ConfMacroLayer> getMacroLayers() {
		if (macroLayers == null) {
			macroLayers = new ArrayList<ConfMacroLayer>();
		}
		return macroLayers;
	}

	/**
	 * Returns imported files list as an unmodifiable list.
	 */
	public List<File> getImportedFiles() {
		return Collections.unmodifiableList(importedFiles);
	}

	/** Add a new file to imported files list. */
	public void addImportedFile(File file) {
		this.importedFiles.add(file);
	}

	/**
	 * Macro layers configuration.
	 */
	public void setMacroLayers(List<ConfMacroLayer> value) {
		macroLayers = value;
	}

	/**
	 * Current view bounding box.
	 */
	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	/**
	 * Current view bounding box.
	 */
	public void setBoundingBox(BoundingBox value) {
		boundingBox = value;
	}

	/**
	 * Loads session configuration from a SonarScope-3DViewer session file.
	 * 
	 * @return loaded instance or null if loading fails.
	 */
	public static SessionConfiguration load(File file) {
		if (file != null && file.exists()) {
			try {
				// loads data from XML
				XStream xstream = new XStream();
				xstream.autodetectAnnotations(true);
				xstream.processAnnotations(SessionConfiguration.class);
				// required for osgi plugins (has its own classloader)
				xstream.setClassLoader(SessionConfiguration.class.getClassLoader());

				InputStream fis = new FileInputStream(file);
				SessionConfiguration value = (SessionConfiguration) xstream.fromXML(fis);
				return value;

			} catch (XStreamException e) {
				Logging.logger().severe("Error during session configuration loading " + file.getAbsolutePath());
			} catch (FileNotFoundException e) {
				Logging.logger().severe("File not found during session configuration loading " + file.getAbsolutePath());
			}
		} else {
			Logging.logger().fine("Configuration file doesn't exist yet.");
		}

		return null;
	}

	/**
	 * Saves session configuration into specified file.
	 * 
	 * @param file
	 *            the file where session configuration should be written.
	 */
	public void save(File file) {
		if (file != null) {
			XStream xstream = new XStream();
			xstream.autodetectAnnotations(true);

			String xml = xstream.toXML(this);

			try {
				FileWriter fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(xml);
				bw.close();
			} catch (IOException e) {
				Logging.logger().severe("Error while writing session configuration file " + file.getPath());
			}
		}
	}

	// public void addCustomMacroLayer(WMSServerDefinition cml) {
	// if (cml != null) {
	// customLayers.add(cml);
	// }
	// }
	//
	// public List<WMSServerDefinition> getCustomLayers() {
	// if (customLayers != null)
	// return Collections.unmodifiableList(customLayers);
	// return null;
	// }
}
