/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.binary;

/**
 * Warper providing methods to iterate over a byte-buffer.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public interface BufferIterator<T> {

	/**
	 * Relative get method. Reads the value at this buffer's current position,
	 * and then increments the position.
	 * 
	 * @return the value at the buffer's current position
	 */
	public T get();

	/**
	 * Returns total buffer size.
	 * 
	 * @return total buffer size.
	 */
	public int size();
}
