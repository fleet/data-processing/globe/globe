/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.binary;

import java.nio.DoubleBuffer;
import java.security.InvalidParameterException;

/**
 * Return double values from a {@link DoubleBuffer}.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class DDBufferIterator implements BufferIterator<Double> {

	/**
	 * The byte buffer to scan.
	 */
	private final DoubleBuffer buffer;

	/**
	 * Ctor.
	 * 
	 * @param buffer
	 *            the byte buffer to scan.
	 */
	public DDBufferIterator(DoubleBuffer buffer) {
		if (buffer == null) {
			throw new InvalidParameterException("null buffer");
		}
		this.buffer = buffer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double get() {
		return buffer.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return buffer.capacity();
	}

}
