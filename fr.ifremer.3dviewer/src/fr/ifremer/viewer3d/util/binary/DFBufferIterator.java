/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.binary;

import java.nio.FloatBuffer;
import java.security.InvalidParameterException;

/**
 * Return double values from a {@link FloatBuffer}.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class DFBufferIterator implements BufferIterator<Double> {

	/**
	 * The byte buffer to scan.
	 */
	private final FloatBuffer buffer;

	/**
	 * Ctor.
	 * 
	 * @param buffer
	 *            the byte buffer to scan.
	 */
	public DFBufferIterator(FloatBuffer buffer) {
		if (buffer == null) {
			throw new InvalidParameterException("null buffer");
		}
		this.buffer = buffer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double get() {
		return (double) buffer.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return buffer.capacity();
	}

}
