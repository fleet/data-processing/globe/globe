/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.binary;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.model.echoes.EndianType;
import fr.ifremer.viewer3d.model.echoes.StorageType;

/**
 * Utility class providing binary reading methods.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class BinaryUtil {

	/**
	 * Read binary file and return result as a double values iterator.
	 * <p>
	 * This method use little-endian order.
	 * </p>
	 * 
	 * @param file
	 *            the file to read
	 * @param storageType
	 *            the storageType for the binary values
	 * @return the double iterator
	 * @throws IOException
	 *             if an error occurs during file reading
	 */
	public static BufferIterator<Double> readBinFile(File file, StorageType storageType) throws GIOException {
		return readBinFile(file, null, StorageType.DOUBLE.equals(storageType));
	}

	public static BufferIterator<Double> readBinFile(File file, fr.ifremer.viewer3d.model.info.StorageType storageType) throws GIOException {
		return readBinFile(file, null, fr.ifremer.viewer3d.model.info.StorageType.DOUBLE.equals(storageType));
	}

	/**
	 * Read binary file and return result as a double values iterator.
	 * 
	 * @param file
	 *            the file to read
	 * @param endianness
	 *            endianness of binary values
	 * @param doubleStorageType
	 *            true if binary values are stored as double values (otherwise
	 *            float is assumed)
	 * @return the double iterator
	 * @throws IOException
	 *             if an error occurs during file reading
	 */
	public static BufferIterator<Double> readBinFile(File file, EndianType endianness, boolean doubleStorageType) throws GIOException {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			FileChannel fc = fis.getChannel();
			int size = (int) fc.size();
			// allocate inside VM heap
			ByteBuffer buffer = ByteBuffer.allocate(size);
			for (int count = 0; count >= 0 && buffer.hasRemaining();) {
				count = fc.read(buffer);
			}
			buffer.flip();
			buffer.rewind();
			if (endianness == null || EndianType.LITTLE_ENDIAN.equals(endianness)) {
				buffer.order(ByteOrder.LITTLE_ENDIAN);
			} else {
				buffer.order(ByteOrder.BIG_ENDIAN);
			}

			BufferIterator<Double> it = null;
			if (doubleStorageType) {
				it = new DDBufferIterator(buffer.asDoubleBuffer());
			} else {
				it = new DFBufferIterator(buffer.asFloatBuffer());
			}
			return it;
		} catch (IOException e) {
			throw new GIOException ("Error bin file ", e);
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				throw new GIOException ("Error close bin file ", e);
			}
		}
	}

	/**
	 * Read binary file and return result as a list of FloatBuffer.
	 * 
	 * @param file
	 *            the file to read
	 * @param nbSlices
	 *            the number of slices stored in the file (defines the returned
	 *            list's size)
	 * @param endianness
	 *            endianness of binary values
	 * @return list of FloatBuffer
	 * @throws IOException
	 *             if an error occurs during file reading
	 */
	public static List<FloatBuffer> binFileToFloatBuffer(File file, EndianType endianness, int nbSlices) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		BufferedInputStream bfis = new BufferedInputStream(fis);
		try {
			int sizeofBuffers = (int) (fis.getChannel().size() / nbSlices);
			byte[] bufferArray = new byte[sizeofBuffers];
			ArrayList<ByteBuffer> tmpListBufs = new ArrayList<ByteBuffer>();
			int bytesRead = 0;
			// we read all the file
			while ((bytesRead = bfis.read(bufferArray)) != -1) {
				byte[] inBufferArray = Arrays.copyOfRange(bufferArray, 0, bytesRead);
				ByteBuffer inbuffer = ByteBuffer.wrap(inBufferArray, 0, bytesRead);
				tmpListBufs.add(inbuffer);
			}
			// reordering the buffers depending on the endianness
			Iterator<ByteBuffer> it;
			if (endianness == null || EndianType.LITTLE_ENDIAN.equals(endianness)) {
				it = tmpListBufs.listIterator();
				while (it.hasNext()) {
					it.next().order(ByteOrder.LITTLE_ENDIAN);
				}
			} else {
				it = tmpListBufs.listIterator();
				while (it.hasNext()) {
					it.next().order(ByteOrder.BIG_ENDIAN);
				}
			}
			// creating the returnList
			it = tmpListBufs.listIterator();
			List<FloatBuffer> returnListBufs = new ArrayList<FloatBuffer>();
			while (it.hasNext()) {
				returnListBufs.add(it.next().asFloatBuffer());
			}
			// we clear the temporary list of buffers to free some memory
			tmpListBufs.clear();

			return returnListBufs;
		} finally {
			fis.close();
			bfis.close();
		}
	}
}
