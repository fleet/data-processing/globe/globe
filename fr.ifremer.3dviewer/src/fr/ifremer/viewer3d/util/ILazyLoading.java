/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util;

import java.io.File;

/**
 * Interface for "lazy loadable" objects.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public interface ILazyLoading {

	/**
	 * Property value for initialization.
	 */
	public static final String PROPERTY_INITIALIZED = "initialized";

	/**
	 * Returns input file.
	 */
	public File getInputFile();

	/**
	 * Returns true if this instance is already loaded.
	 * 
	 * @return true if this instance is already loaded.
	 */
	public boolean isInitialized();

	/**
	 * Initialize this instance.
	 * <p>
	 * If initialization succeed a property event with
	 * {@link #PROPERTY_INITIALIZED} name should be fired.
	 * </p>
	 */
	public void initialize();
}
