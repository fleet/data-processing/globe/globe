/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util;

import java.io.File;
import java.util.Map;

/**
 * This interface allows any object to provide a list of files defining it.
 * <p>
 * Eg. SonarScope-3DViewer layers defined by both an XML index file and binary
 * data files.
 * </p>
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public interface IFiles {

	/**
	 * Return a map of files defining this object.
	 * <p>
	 * This map contains defining files as keys and a "recursive" boolean value
	 * defining if this file or directory is fully owned by current object (true
	 * value) or may be shared (false value).<br>
	 * Therefore during deletion if any file or directory is fully owned by
	 * current object it should be recursively deleted.
	 * </p>
	 * 
	 * @return a map of files defining this object.
	 */
	public Map<File, Boolean> getFiles();

}
