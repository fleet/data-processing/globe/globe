package fr.ifremer.viewer3d.util;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * File filter for SonarScope-3DViewer session files (.w3d).
 */
public final class ExtensionFileFilter extends FileFilter {

	private final String extension;
	private final String description;

	public ExtensionFileFilter(String extension, String description) {
		this.extension = extension;
		this.description = description;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public boolean accept(File f) {
		if (f != null) {
			if (f.isDirectory()) {
				return true;
			}

			String name = f.getName();
			if (name != null && name.endsWith(this.extension)) {
				return true;
			}
		}
		return false;
	}
}