package fr.ifremer.viewer3d.util;

import java.nio.FloatBuffer;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

/**
 * Class representing VBO object
 */
public class VBO {

	/** Name of the Open GL buffer */
	private int[] bufferName;

	/** Number of elements to render */
	private int elementCount = 0;

	/** Buffer */
	private FloatBuffer buffer;

	/** Size of elements */
	private int elementSize;

	public VBO(int elementSize) {
		this.elementSize = elementSize;
	}

	public void allocateBuffer(int elementCount) {
		if (buffer != null) {
			buffer.clear();
		}
		buffer = Buffers.newDirectFloatBuffer(elementCount * elementSize);
		this.elementCount = elementCount;
	}

	/**
	 * Add a value to buffer
	 */
	public void put(double x) {
		buffer.put((float) x);
	}

	/**
	 * Add a value to buffer
	 */
	public void put(float value) {
		buffer.put(value);
	}

	/**
	 * Add a values to buffer
	 * 
	 * @param values
	 */
	public void put(float[] values) {
		buffer.put(values);
	}

	/**
	 * Generate and bind the buffer
	 */
	public void bindAndLoad(GL gl1) {
		if (bufferName == null) {
			// Get a valid name
			GL2 gl = gl1.getGL2();
			bufferName = new int[1];
			gl.glGenBuffers(1, bufferName, 0);

			// Bind the buffer
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, bufferName[0]);
			// Load the data
			buffer.rewind();
			gl.glBufferData(GL.GL_ARRAY_BUFFER, (long) buffer.capacity() * Buffers.SIZEOF_FLOAT, buffer,
					GL.GL_STATIC_DRAW);
			// unbind the buffer
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		}
	}

	/**
	 * remove the buffer from OpenGL
	 */
	public void delete(GL gl) {
		if (bufferName != null) {
			if (gl != null) {
				gl.glDeleteBuffers(1, bufferName, 0);
			}
			bufferName = null;
		}
	}

	public void position(int position) {
		buffer.position(position);
	}

	public int getElementCount() {
		return elementCount;
	}

	public int getElementSize() {
		return elementSize;
	}

	public int[] getBufferName() {
		return bufferName;
	}

	public void clear() {
		if (buffer != null)
			buffer.clear();
	}

	public float get() {
		return buffer.get();
	}

	public void rewind() {
		buffer.rewind();
	}

	/**
	 * @return the {@link #buffer}
	 */
	public FloatBuffer getBuffer() {
		return buffer;
	}
}