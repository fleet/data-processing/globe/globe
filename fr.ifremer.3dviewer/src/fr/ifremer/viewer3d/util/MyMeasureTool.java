package fr.ifremer.viewer3d.util;

import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import fr.ifremer.viewer3d.layers.background.MyTerrainProfileLayer;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import fr.ifremer.viewer3d.util.conf.UISettings;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.layers.TerrainProfileLayer;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Path;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.SurfaceCircle;
import gov.nasa.worldwind.render.SurfaceEllipse;
import gov.nasa.worldwind.render.SurfacePolygon;
import gov.nasa.worldwind.render.SurfaceQuad;
import gov.nasa.worldwind.render.SurfaceSquare;
import gov.nasa.worldwind.util.measure.LengthMeasurer;
import gov.nasa.worldwind.util.measure.MeasureTool;

public class MyMeasureTool extends MeasureTool {

	protected Logger logger = LoggerFactory.getLogger(MyMeasureTool.class);

	public final static String[] FORMAT_NAMES = new String[] { "Comma Separated Values" };
	public final static String[] FORMAT_EXTENSIONS = new String[] { "*.csv" };

	public static final String ELEV_COLUMN = "elev";
	public static final String LON_COLUMN = "lon(deg)";
	public static final String LAT_COLUMN = "lat(deg)";

	private boolean selected = false;
	protected Color directLineColor = Color.PINK;
	protected Path directLine;
	private MyTerrainProfileLayer profile;

	public MyMeasureTool(WorldWindow wwd) {
		super(wwd);
	}

	public void reset() {
		if (getLine() != null) {
			if (getLine().getMeasurer() != null) {
				// hack to #clearCachedValues
				LengthMeasurer lm = getLine().getMeasurer();
				double d2 = lm.getLengthTerrainSamplingSteps();
				lm.setLengthTerrainSamplingSteps(d2 + 1.0);
				lm.setLengthTerrainSamplingSteps(d2);
			}
		}
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	@Override
	public Position addControlPoint() {
		if (positions != null && positions.size() > 1) {
			initProfile();
			profile.setPathPositions(positions);
		}
		return super.addControlPoint();
	}

	private void initProfile() {
		if (profile == null) {
			profile = new MyTerrainProfileLayer();
			profile.setPosition(AVKey.SOUTHEAST);
			profile.setBorderWidth(20);
			profile.setEventSource(wwd);
			profile.setFollow(TerrainProfileLayer.FOLLOW_PATH);
			profile.setStartLatLon(LatLon.fromDegrees(0, -10));
			profile.setEndLatLon(LatLon.fromDegrees(0, 65));
			wwd.getModel().getLayers().add(profile);
			// EventBus.publish(new MeasureProfileEvent(this.profile, true));
		}
	}

	@Override
	public void clear() {
		if (profile != null) {
			wwd.getModel().getLayers().remove(profile);
			// EventBus.publish(new MeasureProfileEvent(this.profile, false));
			profile = null;
		}

		super.clear();
	}

	public void removeControlPoint(ControlPoint ctrlPoint) {
		Position currentLastPosition = null;

		if (positions.size() == 0 || ctrlPoint == null) {
			return;
		}

		currentLastPosition = ctrlPoint.getPosition();
		int i = 0;
		for (Renderable controlPoint : getControlPoints()) {
			if (controlPoint == ctrlPoint) {
				positions.remove(i);
			}
			i++;
		}

		controlPoints.remove(ctrlPoint);

		controlPointsLayer.setRenderables(controlPoints);
		// Update screen shapes
		updateMeasureShape();
		this.firePropertyChange(EVENT_POSITION_REMOVE, currentLastPosition, null);
		wwd.redraw();
	}

	@Override
    protected void updateMeasureShape() {
        // Update line
        if (this.measureShapeType.equals(SHAPE_LINE) || this.measureShapeType.equals(SHAPE_PATH)) {
            if (this.positions.size() > 1 && this.line == null) {
                // Init path
                this.line = new Path();
                setFollowTerrain(this.isFollowTerrain());
                this.line.setPathType(this.getPathType());
                var attrs = new BasicShapeAttributes();
                attrs.setOutlineWidth(this.getLineWidth());
                attrs.setOutlineMaterial(new Material(this.getLineColor()));
                this.line.setAttributes(attrs);
                this.shapeLayer.addRenderable(this.line);

// GLOBE PATCH : BEGIN
                // Init path
                this.directLine = new Path();
				List<Position> positions = new ArrayList<>();
				for (Position position : line.getPositions()) {
					positions.add(new Position(position.getLatitude(), position.getLongitude(), 0.0));
				}
				this.directLine.setPositions(positions);
                this.directLine.setFollowTerrain(false);
                this.directLine.setPathType(this.getPathType());
                attrs = new BasicShapeAttributes();
                attrs.setOutlineWidth(this.getLineWidth());
                attrs.setOutlineMaterial(new Material(directLineColor));
                this.directLine.setAttributes(attrs);
                this.shapeLayer.addRenderable(this.directLine);
// GLOBE PATCH : END
            }
            if (this.positions.size() < 2 && this.line != null) {
                // Remove line if less then 2 positions
                this.shapeLayer.removeRenderable(this.line);
                this.line = null;
// GLOBE PATCH : BEGIN
                this.shapeLayer.removeRenderable(this.directLine);
                this.directLine = null;
// GLOBE PATCH : END
            }
            // Update current line
            if (this.positions.size() > 1 && this.line != null) {
                this.line.setPositions(this.positions);
            }

            if (this.surfaceShape != null) {
                // Remove surface shape if necessary
                this.shapeLayer.removeRenderable(this.surfaceShape);
                this.surfaceShape = null;
            }
        } // Update polygon
        else if (this.measureShapeType.equals(SHAPE_POLYGON)) {
            if (this.positions.size() >= 4 && this.surfaceShape == null) {
                // Init surface shape
                this.surfaceShape = new SurfacePolygon(this.positions);
                ShapeAttributes attr = new BasicShapeAttributes();
                attr.setInteriorMaterial(new Material(this.getFillColor()));
                attr.setInteriorOpacity(this.getFillColor().getAlpha() / 255d);
                attr.setOutlineMaterial(new Material(this.getLineColor()));
                attr.setOutlineOpacity(this.getLineColor().getAlpha() / 255d);
                attr.setOutlineWidth(this.getLineWidth());
                this.surfaceShape.setAttributes(attr);
                this.shapeLayer.addRenderable(this.surfaceShape);
            }
            if (this.positions.size() <= 3 && this.surfaceShape != null) {
                // Remove surface shape if only three positions or less - last is same as first
                this.shapeLayer.removeRenderable(this.surfaceShape);
                this.surfaceShape = null;
            }
            if (this.surfaceShape != null) {
                // Update current shape
                ((SurfacePolygon) this.surfaceShape).setLocations(this.positions);
            }
            // Remove line if necessary
            if (this.line != null) {
                this.shapeLayer.removeRenderable(this.line);
                this.line = null;
// GLOBE PATCH : BEGIN
                this.shapeLayer.removeRenderable(this.directLine);
                this.directLine = null;
// GLOBE PATCH : END
            }
        } // Update regular shape
        else if (this.isRegularShape()) {
            if (this.shapeCenterPosition != null && this.shapeRectangle != null && this.surfaceShape == null) {
                // Init surface shape
                switch (this.measureShapeType) {
                    case SHAPE_QUAD:
                        this.surfaceShape = new SurfaceQuad(this.shapeCenterPosition,
                                this.shapeRectangle.width, this.shapeRectangle.height, this.shapeOrientation);
                        break;
                    case SHAPE_SQUARE:
                        this.surfaceShape = new SurfaceSquare(this.shapeCenterPosition,
                                this.shapeRectangle.width);
                        break;
                    case SHAPE_ELLIPSE:
                        this.surfaceShape = new SurfaceEllipse(this.shapeCenterPosition,
                                this.shapeRectangle.width / 2, this.shapeRectangle.height / 2, this.shapeOrientation,
                                this.shapeIntervals);
                        break;
                    case SHAPE_CIRCLE:
                        this.surfaceShape = new SurfaceCircle(this.shapeCenterPosition,
                                this.shapeRectangle.width / 2, this.shapeIntervals);
                        break;
                    default:
                        break;
                }

                ShapeAttributes attr = new BasicShapeAttributes();
                attr.setInteriorMaterial(new Material(this.getFillColor()));
                attr.setInteriorOpacity(this.getFillColor().getAlpha() / 255d);
                attr.setOutlineMaterial(new Material(this.getLineColor()));
                attr.setOutlineOpacity(this.getLineColor().getAlpha() / 255d);
                attr.setOutlineWidth(this.getLineWidth());
                this.surfaceShape.setAttributes(attr);
                this.shapeLayer.addRenderable(this.surfaceShape);
            }
            if (this.shapeRectangle == null && this.surfaceShape != null) {
                // Remove surface shape if not defined
                this.shapeLayer.removeRenderable(this.surfaceShape);
                this.surfaceShape = null;
                this.positions.clear();
            }
            if (this.surfaceShape != null) {
                // Update current shape
                if (this.measureShapeType.equals(SHAPE_QUAD) || this.measureShapeType.equals(SHAPE_SQUARE)) {
                    ((SurfaceQuad) this.surfaceShape).setCenter(this.shapeCenterPosition);
                    ((SurfaceQuad) this.surfaceShape).setSize(this.shapeRectangle.width, this.shapeRectangle.height);
                    ((SurfaceQuad) this.surfaceShape).setHeading(this.shapeOrientation);
                }
                if (this.measureShapeType.equals(SHAPE_ELLIPSE) || this.measureShapeType.equals(SHAPE_CIRCLE)) {
                    ((SurfaceEllipse) this.surfaceShape).setCenter(this.shapeCenterPosition);
                    ((SurfaceEllipse) this.surfaceShape).setRadii(this.shapeRectangle.width / 2,
                            this.shapeRectangle.height / 2);
                    ((SurfaceEllipse) this.surfaceShape).setHeading(this.shapeOrientation);
                }
                // Update position from shape list with zero elevation
                updatePositionsFromShape();
            }
            // Remove line if necessary
            if (this.line != null) {
                this.shapeLayer.removeRenderable(this.line);
                this.line = null;
// GLOBE PATCH : BEGIN
                this.shapeLayer.removeRenderable(this.directLine);
                this.directLine = null;
// GLOBE PATCH : END
           }
        }
		
// GLOBE PATCH : BEGIN
		if (line != null) {
			List<Position> positions = new ArrayList<>();

			for (Position position : line.getPositions()) {
				positions.add(new Position(position.getLatitude(), position.getLongitude(), 0.0));
			}
			directLine.setPositions(positions);
		}
// GLOBE PATCH : END

    }
	public double getDirectLength() {
		Globe globe = wwd.getModel().getGlobe();

		if (directLine != null) {
			return directLine.getLength(globe);
		}

		if (surfaceShape != null) {
			return surfaceShape.getPerimeter(globe);
		}

		return -1;
	}

	public Color getDirectLineColor() {
		return directLineColor;
	}

	public void export() {
		FileDialog dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.SAVE);
		dialog.setFilterNames(MyMeasureTool.FORMAT_NAMES);
		dialog.setFilterExtensions(MyMeasureTool.FORMAT_EXTENSIONS);

		// loads default path from configuration
		SSVConfiguration conf = SSVConfiguration.getConfiguration();
		if (conf != null && conf.getUISettings() != null) {
			String path = conf.getUISettings().getDefault(UISettings.EXPORT_PROFILE_PATH);
			if (path != null) {
				File f = new File(path);
				dialog.setFilterPath(f.getParent());
			}
		}

		String filename = dialog.open();
		if (filename != null) {
			File file = new File(filename);
			exportTo(file);

			// save current directory as default directory
			if (conf != null && conf.getUISettings() != null) {
				conf.getUISettings().setDefault(UISettings.EXPORT_PROFILE_PATH, file.getPath());
			}
		}
	}

	private void exportTo(File file) {
		try {
			FileWriter fw = new FileWriter(file);
			CSVWriter csvwriter = new CSVWriter(fw);

			// header
			String[] headerColumns = new String[] { "key point lat(deg).", "key point lon(deg)." };
			csvwriter.writeNext(headerColumns);
			List<? extends LatLon> positions = profile.getPathPositions();
			if (positions != null) {
				String[] content = new String[2];
				for (LatLon pos : positions) {
					content[0] = String.valueOf(pos.latitude.degrees);
					content[1] = String.valueOf(pos.longitude.degrees);
					csvwriter.writeNext(content);
				}
			}

			// column title
			String[] columns = new String[] { LAT_COLUMN, LON_COLUMN, ELEV_COLUMN };
			csvwriter.writeNext(columns);

			// content
			Position[] list = profile.getPositions();
			if (list != null) {
				String[] content = new String[3];
				for (Position pos : list) {
					content[0] = String.valueOf(pos.latitude.degrees);
					content[1] = String.valueOf(pos.longitude.degrees);
					content[2] = String.valueOf(pos.getElevation());
					csvwriter.writeNext(content);
				}
			}
			csvwriter.flush();
			csvwriter.close();

		} catch (IOException e) {
			logger.error("Error during CSV export.", e);
		}

	}

	public void importProfile() {
		FileDialog dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
		dialog.setFilterNames(MyMeasureTool.FORMAT_NAMES);
		dialog.setFilterExtensions(MyMeasureTool.FORMAT_EXTENSIONS);

		SSVConfiguration conf = SSVConfiguration.getConfiguration();
		// loads default path from configuration
		if (conf != null && conf.getUISettings() != null) {
			String path = conf.getUISettings().getDefault(UISettings.EXPORT_PROFILE_PATH);
			if (path != null) {
				File f = new File(path);
				if (f != null) {
					dialog.setFileName(path);
				}
			}
		}
		String filename = dialog.open();
		if (filename != null) {
			// get selected file
			File file = new File(filename);
			loadProfile(file);

			// saves default path
			if (file != null && conf != null && conf.getUISettings() != null) {
				conf.getUISettings().setDefault(UISettings.EXPORT_PROFILE_PATH, file.getAbsolutePath());
			}
		}
	}

	private void loadProfile(File file) {
		if (file == null) {
			return;
		}
		try {
			FileReader fr = new FileReader(file);
			CSVReader csvReader = new CSVReader(fr);
			// read header line
			String[] line = csvReader.readNext();
			// key points
			line = csvReader.readNext();
			clear();
			reset();
			while (line != null && line.length == 2) {
				String lat = line[0];
				String lon = line[1];
				// // remove trailing degree char
				// lat = lat.substring(0, lat.length() - 1);
				// lon = lon.substring(0, lon.length() - 1);
				Position pos = Position.fromDegrees(Double.valueOf(lat), Double.valueOf(lon));
				positions.add(pos);
				line = csvReader.readNext();
			}
			if (positions != null && positions.size() > 1) {
				initProfile();

			}
			if (profile != null) {
				profile.setPositions(positions.toArray(new Position[0]));
				profile.setPathPositions(positions);
				for (int i = 0; i < positions.size(); i++) {
					addControlPoint(positions.get(i), CONTROL_TYPE_LOCATION_INDEX, i);
				}
				updateMeasureShape();
			}
			csvReader.close();
		} catch (Exception e) {
			logger.error("Cannot read profile CSV file.", e);
		}

	}

	public void exit() {
		this.clear();
		this.setSelected(false);
		this.updateAnnotation(null);
		this.setArmed(false);
	}

}
