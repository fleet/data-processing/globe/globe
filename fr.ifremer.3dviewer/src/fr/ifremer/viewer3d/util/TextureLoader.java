package fr.ifremer.viewer3d.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.globe.core.utils.Pair;

public class TextureLoader {

	private final LinkedHashMap<Pair<GL, String>, Texture> textureMap = new LinkedHashMap<>();
	private final int maxSize;

	public TextureLoader(int maxSize) {
		this.maxSize = maxSize;
	}

	/**
	 * Dispose method to free resources used by textures. (! Avoid memory leak!)
	 */
	public void dispose(GL gl) {
		SwingUtilities.invokeLater(() -> {
			if (gl.getContext().makeCurrent() != GLContext.CONTEXT_NOT_CURRENT) {
				try {
					for (Texture t : textureMap.values()) {
						t.destroy(gl);
					}
				} finally {
					gl.getContext().release();
				}
			}
		});
	}

	public Texture getTexture(GL gl, String imgFile) throws IOException {
		Pair<GL, String> key = new Pair<>(gl, imgFile);
		Texture result = textureMap.get(key);
		if (result == null) {
			File file = new File(imgFile);
			if (file.exists()) {
				BufferedImage image = null;
				GLProfile glprofile = GLProfile.getDefault();
				try {
					image = ImageIO.read(file);
					result = AWTTextureIO.newTexture(glprofile, image, false);
					textureMap.put(key, result);
				} finally {
					if (image != null) {
						image.flush();
					}
				}
			}
		}

		if (textureMap.size() > maxSize) {
			textureMap.remove(textureMap.keySet().iterator().next()).destroy(gl);
		}

		return result;

	}

}
