
package fr.ifremer.viewer3d.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Permet la validation d'un fichier XML.
 * 
 * @author Guillaume Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class XSDValidator {

	private static final Logger logger = LoggerFactory.getLogger(XSDValidator.class);

	/**
	 * Methode principale du validateur.
	 * 
	 * @param args
	 *            les arguments de la ligne de commande :
	 *            <ul>
	 *            <li>-f xmlFileName le nom du fichier xml � valider
	 *            <li>-s schema le nom du schema W3C pour la validation du xml
	 *            </ul>
	 */
	public static boolean valid(InputStream xmlFile, InputStream schemaFile) {
		if (xmlFile == null) {
			return false;
		}
		if (schemaFile == null) {
			throw new InvalidParameterException("Cannot valid against null XML schema");
		}
		boolean value = true;

		try {
			MyErrorHandler errorHandler = new MyErrorHandler();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);


			DocumentBuilder analyseur = factory.newDocumentBuilder();
			analyseur.setErrorHandler(errorHandler);
			Document document = analyseur.parse(xmlFile);

			// si un schema est specifié => validation
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			schemaFactory.setErrorHandler(errorHandler);

			InputSource xsdInput = new InputSource(schemaFile);
			SAXSource source = new SAXSource(xsdInput);

			Schema schema = schemaFactory.newSchema(source);
			Validator validateur = schema.newValidator();

			validateur.validate(new DOMSource(document));

			if (errorHandler.getNbErrors() > 0) {
				logger.error(errorHandler.getNbErrors() + " errors founds");
				value = false;
			}
		} catch (ParserConfigurationException pce) {
			logger.error("Erreur de configuration du parseur");
			value = false;
		} catch (SAXException e) {
			logger.debug("File is not compliant with the schema");
			value = false;
		} catch (IOException e) {
			logger.error("File IO error",e);
			value = false;
		}

		return value;
	}

	/**
	 * XML error handler.
	 */
	public static class MyErrorHandler implements ErrorHandler {

		/**
		 * Error counter.
		 */
		private int _nbErrors = 0;

		public int getNbErrors() {
			return _nbErrors;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void fatalError(SAXParseException e) throws SAXException {
			_nbErrors++;
			logger.error("XSDValidation Fatal Error",e);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void error(SAXParseException e) throws SAXException {
			_nbErrors++;
			logger.error("XSDValidation Error",e);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void warning(SAXParseException e) throws SAXException {
			System.out.println("AVERTISSEMENT");
			logger.warn("XSDValidation Error",e);
		}
	}

}
