package fr.ifremer.viewer3d.util;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Synchronized LRU cache using soft references.
 * 
 * @author gbourel
 * 
 * @param <K>
 *            keys (kept using strong references)
 * @param <V>
 *            values (kept using soft references)
 */
public class LRUSoftCache<K, V> {
	/** Internal map. */
	private LinkedHashMap<K, SoftReference<V>> map;
	/** Max cache size (number of objects). */
	private int cacheSize;

	/**
	 * Ctor.
	 * 
	 * @param cacheSize
	 *            Max cache size (number of objects)
	 */
	public LRUSoftCache(int cacheSize) {
		this.cacheSize = cacheSize;
		int hashTableCapacity = (int) Math.ceil(cacheSize / 0.75) + 1;
		map = new LinkedHashMap<K, SoftReference<V>>(hashTableCapacity, 0.75f, true) {
			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			/**
			 * Returns true if the current map size is greater than NewLRUCache, wich means
			 * that the cache is full and we should drop the oldest entry.
			 */
			protected boolean removeEldestEntry(Map.Entry<K, SoftReference<V>> eldest) {
				return size() > LRUSoftCache.this.cacheSize;
			}
		};
	}

	/**
	 * Retrieves an entry from this cache.<br>
	 * The retrieved entry becomes the most recently used (MRU) entry.
	 */
	public V get(K key) {
		V value = null;
		if (key != null) {
			synchronized (this) {
				SoftReference<V> ref = map.get(key);
				if (ref == null) {
					map.remove(key);
				} else {
					value = ref.get();
					if (value == null) {
						map.remove(key);
					}
				}
			}
		}
		return value;
	}

	/**
	 * Retrieves a reference on an entry.<br>
	 * The retrieved entry becomes the most recently used (MRU) entry.
	 */
	public synchronized SoftReference<V> getRef(K key) {
		return map.get(key);
	}

	/**
	 * Adds an entry to this cache. The new entry becomes the most recently used
	 * (MRU) entry. If an entry with the specified key already exists in the
	 * cache, it is replaced by the new entry. If the cache is full, the least
	 * recently used (LRU) entry is removed from the cache.
	 * 
	 * @param key
	 *            the key with which the specified value is to be associated.
	 * @param value
	 *            a value to be associated with the specified key.
	 */
	public synchronized void put(K key, V value) {
		if (key == null) {
			return;
		}
		if (value == null) {
			return;
		}
		synchronized (this) {
			map.put(key, new SoftReference<V>(value));
		}
	}

	/** Removes the element referenced by provided key. */
	public synchronized void remove(K key) {
		map.remove(key);
	}

	/** Removes the element referenced by provided key. */
	public synchronized void removeLast() {
		// map.keySet().
	}

	/** Clears the cache. */
	public synchronized void clear() {
		map.clear();
	}

	/**
	 * Returns the number of entries currently in the cache.
	 * <p>
	 * <em>Warning :</em> this method doesn't check for soft references that may
	 * have been garbage collected.
	 * </p>
	 */
	public synchronized int size() {
		return map.size();
	}

	/**
	 * Returns an unmodifiable Set that contains a copy of all cache entries.
	 */
	public synchronized Set<Map.Entry<K, SoftReference<V>>> getAll() {
		return Collections.unmodifiableSet(map.entrySet());
	}

}
