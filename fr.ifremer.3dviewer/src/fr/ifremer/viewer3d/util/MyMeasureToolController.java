package fr.ifremer.viewer3d.util;

import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.pick.PickedObjectList;
import gov.nasa.worldwind.util.measure.MeasureTool;
import gov.nasa.worldwind.util.measure.MeasureTool.ControlPoint;
import gov.nasa.worldwind.util.measure.MeasureToolController;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

public class MyMeasureToolController extends MeasureToolController implements KeyListener {

	protected static final char ADD_POINT_CHAR = 'p';

	protected static final String ADD_CONTROL_POINT = "MyMeasureToolController.AddControlPoint";
	protected static final String REMOVE_CONTROL_POINT = "MyMeasureToolController.RemoveControlPoint";

	private Map<String, Cursor> actionCursorMap = new HashMap<String, Cursor>();

	public MyMeasureToolController() {
		super();
		Viewer3D.getWwd().getInputHandler().addKeyListener(this);
		this.getActionCursorMap().put(ADD_CONTROL_POINT, Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		this.getActionCursorMap().put(REMOVE_CONTROL_POINT, Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e == null) {
			return;
		}

		if (e.getKeyChar() == ADD_POINT_CHAR) {
			if (this.getMeasureTool() instanceof MyMeasureTool) {
				if (((MyMeasureTool) this.getMeasureTool()).isSelected()) {
					this.updateCursor(e);
					this.setArmed(true);
					((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
				}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e == null) {
			return;
		}
		if (e.getKeyChar() == ADD_POINT_CHAR) {
			if (this.getMeasureTool() instanceof MyMeasureTool) {
				if (((MyMeasureTool) this.getMeasureTool()).isSelected()) {
					this.updateCursor(e);
					this.setArmed(false);
					((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// no key binding
	}

	@Override
	public void mousePressed(MouseEvent mouseEvent) {
		if (this.isArmed() && this.isUseRubberBand() && mouseEvent.getButton() == MouseEvent.BUTTON1) {
			//Handle left click
			if ((mouseEvent.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
				if (!mouseEvent.isControlDown()) {
					this.setActive(true);
					measureTool.addControlPoint();
					if (measureTool.getControlPoints().size() == 1) {
						measureTool.addControlPoint(); // Simulate a second
						// click
					}
					// Set the rubber band target to the last control point or
					// the relevant control for regular shapes.
					if (measureTool.isRegularShape()) {
						String initControl = measureTool.getShapeInitialControl(measureTool.getWwd().getCurrentPosition());
						rubberBandTarget = measureTool.getControlPoint(initControl);
					} else {
						rubberBandTarget = (MeasureTool.ControlPoint) measureTool.getControlPoints().get(measureTool.getControlPoints().size() - 1);
					}
					measureTool.firePropertyChange(MeasureTool.EVENT_RUBBERBAND_START, null, null);
				} else if (measureTool instanceof MyMeasureTool) {
					((MyMeasureTool) measureTool).removeControlPoint(getTopOwnedControlPointAtCurrentPosition());
				}
			}
			mouseEvent.consume();
		} else if (this.isArmed() && this.isUseRubberBand() && mouseEvent.getButton() == MouseEvent.BUTTON3) {
			//Handle right click
			if ((mouseEvent.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0) {
				((MyMeasureTool) measureTool).removeControlPoint(getTopOwnedControlPointAtCurrentPosition());
			}
			mouseEvent.consume();
		} else if (!this.isArmed() && mouseEvent.getButton() == MouseEvent.BUTTON1 && mouseEvent.isAltDown()) {
			if (!this.measureTool.isRegularShape()) {
				this.setMoving(true);
				this.movingTarget = this.lastPickedObject;
			}
			mouseEvent.consume();
		}
	}

	protected void updateCursor(InputEvent e) {
		// Include this test to ensure any derived implementation performs it.
		if (e == null || e.getComponent() == null) {
			return;
		}

		Cursor cursor = this.getCursorFor(e);
		e.getComponent().setCursor(cursor);
		e.getComponent().repaint();
	}

	protected Cursor getCursorFor(InputEvent e) {
		// If we're actively engaged in some action, then return the cursor
		// associated with that action. Otherwise
		// return the cursor representing the action that would be invoked (if
		// the user pressed the mouse) given the
		// curent modifiers and pick list.

		if (e == null) {
			return null;
		}

		// Include this test to ensure any derived implementation performs it.
		if (this.getMeasureTool() == null) {
			return null;
		}

		String action = this.isActive() ? null : this.getPotentialActionFor(e);
		return this.getActionCursorMap().get(action);
	}

	protected String getPotentialActionFor(InputEvent e) {
		// ???
		if (e instanceof KeyEvent) {
			if (((KeyEvent) e).getKeyChar() == ADD_POINT_CHAR) {
				if (e.isControlDown()) {
					return REMOVE_CONTROL_POINT;
				} else {
					return ADD_CONTROL_POINT;
				}
			}
		}

		return null;
	}

	protected MyMeasureTool getTopOwnedMeasureToolAtCurrentPosition() {
		if (this.getMeasureTool() == null) {
			return null;
		}

		Object obj = this.getTopPickedObject();
		if (this.getMeasureTool().getLine() != obj) {
			return null;
		}

		return (MyMeasureTool) obj;
	}

	protected ControlPoint getTopOwnedControlPointAtCurrentPosition() {
		if (this.getMeasureTool() == null) {
			return null;
		}

		Object obj = this.getTopPickedObject();
		if (!(obj instanceof ControlPoint)) {
			return null;
		}

		if (this.getMeasureTool() != (((ControlPoint) obj).getParent())) {
			return null;
		}

		return (ControlPoint) obj;
	}

	protected Object getTopPickedObject() {
		if (Viewer3D.getWwd() == null) {
			return null;
		}

		PickedObjectList pickedObjects = Viewer3D.getWwd().getObjectsAtCurrentPosition();
		if (pickedObjects == null || pickedObjects.getTopPickedObject() == null || pickedObjects.getTopPickedObject().isTerrain()) {
			return null;
		}

		return pickedObjects.getTopPickedObject().getObject();
	}

	public void setActionCursorMap(Map<String, Cursor> actionCursorMap) {
		this.actionCursorMap = actionCursorMap;
	}

	public Map<String, Cursor> getActionCursorMap() {
		return actionCursorMap;
	}

	@Override
	public void selected(SelectEvent event) {
		if (isArmed()) {
			super.selected(event);
		}
	}
}
