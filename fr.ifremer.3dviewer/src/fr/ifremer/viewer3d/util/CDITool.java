package fr.ifremer.viewer3d.util;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceId;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceIdBuilder;
import fr.ifremer.globe.core.model.file.ICdiService;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.pick.PickedObjectList;
import gov.nasa.worldwind.render.Annotation;
import gov.nasa.worldwind.render.GlobeAnnotation;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import jakarta.inject.Inject;

@Creatable
public class CDITool extends MouseAdapter {

	protected static final Logger logger = LoggerFactory.getLogger(CDITool.class);

	public static final String NO_CDI_MESSAGE = "No CDI data in this area";

	@Inject
	protected IFileService fileService;
	@Inject
	protected ICdiService cdiService;
	@Inject
	protected WWFileLayerStoreModel wWFileLayerStoreModel;
	@Inject
	protected IGeographicViewService geographicViewService;

	/** A Subject that emits Position to currently subscribed observers (this::searchCdi) */
	protected PublishSubject<Position> computePublisher = PublishSubject.create();
	/** Observer subscribed to mouseEventPublisher */
	protected Disposable computeSubscriber;

	/** Listener to select or highlight annotations on rollover */
	private SelectListener listener;

	/**
	 * Annotation layer
	 */
	private AnnotationLayer layer;

	/**
	 * Current Annotation
	 */
	private GlobeAnnotation annotation;

	boolean selected;

	/**
	 * Constructor
	 */
	public CDITool() {
		super();
	}

	/**
	 * CDI tool activation. Called by the E4 handler to begin the display of CDI in the tooltip.
	 */
	public void hookListeners() {
		computeSubscriber = computePublisher.toFlowable(BackpressureStrategy.LATEST).observeOn(Schedulers.io())
				.subscribe(this::searchCdi);

		Viewer3D.getWwd().getInputHandler().addMouseListener(this);
		setupSelectListener();

		// Annotation Layer
		layer = new AnnotationLayer();
		layer.setPickEnabled(true);
		IGeographicViewService.grab().addLayerBeforeCompass(layer);
	}

	/**
	 * Cleans the references of this instance
	 */
	public void exit() {
		if (computeSubscriber != null) {
			computeSubscriber.dispose();
			computeSubscriber = null;
		}

		// remove AnntotationLayer
		Viewer3D.getModel().getLayers().remove(layer);
		layer.removeAllAnnotations();
		annotation = null;
		layer = null;

		// remove mouseListener
		Viewer3D.getWwd().getInputHandler().removeMouseListener(this);

		// remove listener
		Viewer3D.getWwd().removeSelectListener(listener);
		listener = null;

	}

	// Handle single click for removing control points
	@Override
	public void mouseClicked(MouseEvent mouseEvent) {
		if (mouseEvent.getButton() == MouseEvent.BUTTON1 && !mouseEvent.isControlDown()) {
			// current position
			Position curPos = Viewer3D.getWwd().getCurrentPosition();
			if (curPos == null) {
				return;
			}

			// annotation
			if (annotation == null) {
				annotation = new GlobeAnnotation("Searching CDI...", curPos);
				annotation.getAttributes().setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
				annotation.setPickEnabled(true);
				annotation.setAltitudeMode(WorldWind.CLAMP_TO_GROUND);
				layer.addAnnotation(annotation);
			}

			// default message
			annotation.setText("Searching CDI...");
			annotation.setPosition(curPos);
			mouseEvent.consume();
			Viewer3D.getWwd().redraw();

			computePublisher.onNext(curPos);
		}
	}

	/**
	 * Generic algo for computing and displaying the CDI for any file
	 */
	protected void searchCdi(Position curPos) {
		var layers = new ArrayList<>(geographicViewService.getLayers());
		Collections.reverse(layers); // Layers are sorted from background to front : reverse the list

		var message = layers.stream()//
				// keep only IWWLayer & get file info
				.filter(IWWLayer.class::isInstance).map(IWWLayer.class::cast).filter(IWWLayer::isEnabled)//
				.map(wWFileLayerStoreModel::getFileInfo).filter(Optional::isPresent).map(Optional::get)
				// compute CDI
				.map(fileInfo -> cdiService.getCdi(fileInfo,
						new GeoPoint(curPos.getLongitude().getDegrees(), curPos.getLatitude().getDegrees())))
				.filter(Optional::isPresent).map(Optional::get)//
				// build message to display from first CDI found
				.map(this::buildMessage).findFirst().orElse(NO_CDI_MESSAGE);

		// display message
		annotation.setText(message);
		geographicViewService.refresh();
	}

	/**
	 * Build the message in the GlobeAnntotation.
	 */
	private String buildMessage(String cdi) {
		// careful, invisible char with "Alt + 255" to avoid return

		DtmSourceId info = DtmSourceIdBuilder.build(cdi);
		return info.toHTML();

	}

	/**
	 * Gdal geographic projection.
	 *
	 * @return the gdal SpatialReference
	 */
	public SpatialReference createGeographicSRS() {
		SpatialReference srs = new SpatialReference();
		srs.ImportFromProj4("+proj=latlong +datum=WGS84 +no_defs");
		return srs;
	}

	/**
	 * Add a selectedListener linking url with annotation
	 */
	private void setupSelectListener() {
		listener = event -> {
			if (event.hasObjects() && event.getTopObject() instanceof Annotation) {
				// Handle cursor change on hyperlink
				if (event.getTopPickedObject().getValue(AVKey.URL) != null) {
					Viewer3D.getFrame().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				} else {
					Viewer3D.getFrame().setCursor(Cursor.getDefaultCursor());
				}
			}

			// Select/unselect on left click on annotations
			if (event.getEventAction().equals(SelectEvent.LEFT_CLICK)) {
				if (event.hasObjects()) {
					if (event.getTopObject() instanceof Annotation) {
						// Check for text or url
						PickedObject po = event.getTopPickedObject();
						if (po.getValue(AVKey.TEXT) != null && po.getValue(AVKey.URL) != null) {
							// Check for url command
							String url = (String) po.getValue(AVKey.URL);
							if (!org.eclipse.swt.program.Program.launch(url)) {

								String osName = System.getProperty("os.name");
								String[] cmd = null;

								if (osName != null && osName.contains("Linux")) {
									cmd = new String[2];
									cmd[0] = "/usr/bin/firefox";
									cmd[1] = url;
									try {
										Runtime.getRuntime().exec(cmd);
									} catch (IOException e) {
										logger.info(e.getMessage());
									}
								}
							}
						}
					}
				}
			} else if (event.getEventAction().equals(SelectEvent.DRAG_END)
					|| event.getEventAction().equals(SelectEvent.DRAG)) {
				// We missed any roll-over events while dragging, so
				// highlight any under the cursor now,
				// or de-highlight the dragged shape if it's no longer under
				// the cursor.
				if (event.getEventAction().equals(SelectEvent.DRAG_END)) {
					PickedObjectList pol = Viewer3D.getWwd().getObjectsAtCurrentPosition();
					if (pol != null) {
						Viewer3D.getFrame().repaint();
					}
				}
			}
		};
		// Add a select listener to select or highlight annotations on rollover
		Viewer3D.getWwd().addSelectListener(listener);
	}

}
