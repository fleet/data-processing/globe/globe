package fr.ifremer.viewer3d.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides utility methods allowing access to private fields.
 */
public class PrivateAccessor {

	private static Logger logger = LoggerFactory.getLogger(PrivateAccessor.class);

	/**
	 * @param o
	 *            instance contenant l'attribut
	 * @param fieldName
	 *            nom de l'attribut
	 * @return l'attribut ou <code>null</code> si il n'est pas trouv�
	 */
	public static Object getField(Object o, String fieldName) {
		// Assert.assertNotNull(o);
		// Assert.assertNotNull(fieldName);

		return getField(o, fieldName, o.getClass());
	}

	public static Object getField(Object o, String fieldName, Class<?> c) {
		// Assert.assertNotNull(c);
		// Assert.assertNotNull(fieldName);
		// Assert.assertNotNull(o);

		final Field[] fields = c.getDeclaredFields();
		for (int i = 0; i < fields.length; ++i) {
			if (fieldName.equals(fields[i].getName())) {
				try {
					fields[i].setAccessible(true);
					return fields[i].get(o);
				} catch (IllegalAccessException ex) {
					logger.error("IllegalAccessException accessing " + fieldName, ex);
				}
			}
		}
		logger.warn("Field '" + fieldName + "' not found");
		return null;
	}

	/**
	 * Allows to set a private field value.
	 * 
	 * @param o
	 *            this instance containing the field to set
	 * @param fieldName
	 *            the field name as a string
	 * @param value
	 *            the new value to set
	 * @return <code>true</code> if setting succeed, <code>false</code>.
	 *         otherwise.
	 */
	public static boolean setField(Object o, String fieldName, Object value) {
		// Assert.assertNotNull(o);
		// Assert.assertNotNull(fieldName);

		return setField(o, fieldName, o.getClass(), value);
	}

	public static boolean setField(Object o, String fieldName, Class<?> c, Object value) {
		// Assert.assertNotNull(c);
		// Assert.assertNotNull(fieldName);
		// Assert.assertNotNull(o);

		final Field[] fields = FieldUtils.getAllFields(c);
		for (int i = 0; i < fields.length; ++i) {
			if (fieldName.equals(fields[i].getName())) {
				try {
					fields[i].setAccessible(true);
					fields[i].set(o, value);
					return true;
				} catch (IllegalAccessException ex) {
					logger.error("IllegalAccessException accessing " + fieldName, ex);
				}
			}
		}
		logger.warn("Field '" + fieldName + "' not found");
		return false;
	}

	/**
	 * Invoque la m�thode de l'objet sp�cifi� m�me si celle-ci est priv�e.
	 * 
	 * @param o
	 *            instance de l'objet
	 * @param methodName
	 *            nom de la m�thode a appeler
	 * @param args
	 *            arguments de la m�thode
	 * @return <code>null</code> ou param�tre de retour de la m�thode
	 */
	public static Object invokeMethod(Object o, String methodName, Object... args) {
		// Assert.assertNotNull(o);
		Class<?> c = o.getClass();
		while (c != null) {
			final Method[] methods = c.getDeclaredMethods();
			int nbArgs = (args == null) ? 0 : args.length;

			for (Method m : methods) {
				if (methodName.equals(m.getName()) && m.getParameterTypes().length == nbArgs) {
					try {
						m.setAccessible(true);
						return m.invoke(o, args);
					} catch (IllegalAccessException ex) {
						logger.error("IllegalAccessException accessing " + methodName, ex);
					} catch (IllegalArgumentException e) {
						logger.error("IllegalArgumentException accessing " + methodName, e);
					} catch (InvocationTargetException e) {
						logger.error("InvocationTargetException accessing " + methodName, e);
					}
				}
			}

			// if method wasn't found try parent object
			c = c.getSuperclass();
		}
		logger.warn("Field '" + methodName + "' not found");
		return null;
	}

	/**
	 * Invoke static method.
	 * 
	 * @param c
	 *            Class containing static method
	 * @param methodName
	 *            static method name
	 * @param args
	 *            if this method requires arguments
	 * @return the returned object
	 */
	public static Object invokeStatic(Class<?> c, String methodName, Object... args) {
		// Assert.assertNotNull(c);
		while (c != null) {
			final Method[] methods = c.getDeclaredMethods();
			int nbArgs = (args == null) ? 0 : args.length;

			for (Method m : methods) {
				if (methodName.equals(m.getName()) && m.getParameterTypes().length == nbArgs) {
					try {
						m.setAccessible(true);
						return m.invoke(null, args);
					} catch (IllegalAccessException ex) {
						logger.error("IllegalAccessException accessing " + methodName, ex);
					} catch (IllegalArgumentException e) {
						logger.error("IllegalArgumentException accessing " + methodName, e);
					} catch (InvocationTargetException e) {
						logger.error("InvocationTargetException accessing " + methodName, e);
					}
				}
			}

			// if method wasn't found try parent object
			c = c.getSuperclass();
		}
		logger.warn("Field '" + methodName + "' not found");
		return null;
	}
}
