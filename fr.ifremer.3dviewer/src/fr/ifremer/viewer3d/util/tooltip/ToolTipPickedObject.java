package fr.ifremer.viewer3d.util.tooltip;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import gov.nasa.worldwind.geom.Position;

/**
 * Record representing a picking action on a layer.<br>
 * The purpose of this action is to display a tooltip on the geographic view for the specified file and position.
 */
public record ToolTipPickedObject(IWWLayer layer, IFileInfo fileInfo) {

	/**
	 * Combines the properties provided by the file at a given position in a tooltip.
	 */
	public Optional<String> getTooltip(Position position) {
		Optional<List<Property<?>>> propOption = IFileService.grab().getProperties(fileInfo,
				new GeoPoint(position.longitude.degrees, position.latitude.degrees));
		if (propOption.isPresent()) {
			List<Property<?>> properties = propOption.get();
			StringBuilder sb = new StringBuilder(layer.getName());

			for (Property<?> property : properties) {
				if (RasterInfo.RASTER_CDI_INDEX.equals(property.getKey()))
					sb.append(System.lineSeparator()).append("CDI message : ").append(property.getValueAsString());
				else
					sb.append(System.lineSeparator()).append(property.getKey()).append(" : ")
							.append(property.formatValue());
			}

			return Optional.of(sb.toString());
		}
		return Optional.empty();
	}
}
