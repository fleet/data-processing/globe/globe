package fr.ifremer.viewer3d.util.tooltip;

import java.awt.Point;
import java.util.Optional;

import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.viewer3d.layers.markers.render.WWMarker;
import fr.ifremer.viewer3d.layers.wc.render.IWCPickedObject;
import fr.ifremer.viewer3d.render.ToolTipAnnotation;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.render.Annotation;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

/**
 * This class displays an {@link AnnotationLayer} in the geographic view.
 */
@Creatable
@Singleton
public class ToolTipController implements SelectListener {

	private final IGlobalCursorService globalCursorService;

	/** Layer to display the tooltip **/
	private final AnnotationLayer layer = new AnnotationLayer();

	/** Last top object & event pick point. **/
	private Point currentPickPoint;

	/** Constructor **/
	@Inject
	public ToolTipController(IGlobalCursorService globalCursorService) {
		this.globalCursorService = globalCursorService;
		layer.setPickEnabled(false);
	}

	@Override
	public void selected(SelectEvent event) {
		// check if pick point has changed
		if (event.getPickPoint() == null || event.getPickPoint().equals(currentPickPoint))
			return;
		currentPickPoint = event.getPickPoint();

		// RectangularTessellator always produced a PickedObject with a position
		Optional<Position> currentPosition = Optional.empty();
		if (event.getObjects() != null)
			currentPosition = event.getObjects().stream()//
					.map(PickedObject::getObject)//
					.filter(Position.class::isInstance)//
					.map(Position.class::cast)//
					.findFirst();

		var topObject = event.getTopObject();
		// if cursor => submit it to IGlobalCursorService to handle display
		Optional<GlobalCursor> globalCursor = getCursor(topObject);
		if (globalCursor.isPresent())
			globalCursorService.submit(globalCursor.get());
		else {
			// submit empty cursor it to IGlobalCursorService to clear display
			globalCursorService.submitEmpty();
			// else : display the text if exists
			getTooltipText(topObject, currentPosition).ifPresentOrElse(this::showToolTip, this::hideToolTip);
		}
	}

	private Optional<String> getTooltipText(Object pickedObject, Optional<Position> currentPosition) {
		if (currentPosition.isPresent() && pickedObject instanceof ToolTipPickedObject terrainPickedObject)
			return terrainPickedObject.getTooltip(currentPosition.get());
		else if (pickedObject instanceof AVList aVList) {
			if (aVList.hasKey(AVKey.ROLLOVER_TEXT))
				return Optional.of(aVList.getStringValue(AVKey.ROLLOVER_TEXT));
		} else if (pickedObject instanceof IWCPickedObject wcPickedObject)
			return wcPickedObject.getTooltip();
		else if (pickedObject instanceof WWMarker)
			return ((WWMarker<?>) pickedObject).getTooltip();
		return Optional.empty();
	}

	private Optional<GlobalCursor> getCursor(Object pickedObject) {
		return Optional.ofNullable(pickedObject).filter(AVList.class::isInstance).map(AVList.class::cast)
				.map(aVList -> (GlobalCursor) aVList.getValue(GlobalCursor.AV_KEY));
	}

	private void showToolTip(String text) {
		hideToolTip();
		layer.addAnnotation(new ToolTipAnnotation(text.replace("\\n", "\n")));
	}

	// PUBLIC API

	public AnnotationLayer getLayer() {
		return layer;
	}

	public void showToolTip(String text, Position position) {
		hideToolTip();
		var annotation = new ToolTipAnnotation(text);
		annotation.setPosition(position);
		layer.addAnnotation(annotation);
	}

	public void hideToolTip() {
		layer.getAnnotations().forEach(Annotation::dispose);
		layer.removeAllAnnotations();
	}
}
