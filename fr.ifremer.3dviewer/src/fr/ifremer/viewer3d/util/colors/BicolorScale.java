/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.colors;

import java.awt.Color;
import java.security.InvalidParameterException;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * A gray color scale composed of shades of grey.
 * <p>
 * The 0.0 input is mapped to black and 1.0 input is mapped to white.
 * <p>
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class BicolorScale implements IColorScale {

	/**
	 * Start color for this scale.
	 */
	private final float[] start;

	/**
	 * End color for this scale.
	 */
	private final float[] end;

	/**
	 * Length of the colors array (may be 3 for RGB or 4 for RGBA).
	 */
	private final int length;

	/**
	 * Creates a new color scale from start color to end color.
	 * <p>
	 * This scale can handle the translucence (alpha) value.
	 * </p>
	 * 
	 * @param start
	 *            the start color for this scale (RGB or RGBA)
	 * @param end
	 *            the destination color (RGB or RGBA)
	 * @throws InvalidParameterException
	 *             if the start or the end array are not valid or if the length
	 *             of the start array isn't the same as the end array.
	 */
	public BicolorScale(float[] start, float[] end) {
		if (start == null || start.length < 3) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INVALID_START_ARRAY_MESSAGE")); //$NON-NLS-1$
		}
		if (end == null || end.length < 3) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INVALID_DEST_ARRAY_MESSAGE")); //$NON-NLS-1$
		}
		if (start.length != end.length) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INCONSISTENT_VALUES_MESSAGE")); //$NON-NLS-1$
		}
		this.start = start;
		this.end = end;
		this.length = Math.min(start.length, 4);
	}

	/**
	 * Creates a new color scale from start color to end color.
	 * <p>
	 * This scale can handle the translucence (alpha) value.
	 * </p>
	 * 
	 * @param start
	 *            the start color for this scale
	 * @param end
	 *            the destination color
	 * @throws InvalidParameterException
	 *             if the start or the end array is null.
	 */
	public BicolorScale(Color start, Color end) {
		if (start == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INVALID_START_ARRAY_MESSAGE")); //$NON-NLS-1$
		}
		if (end == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INVALID_DEST_ARRAY_MESSAGE")); //$NON-NLS-1$
		}
		this.start = new float[] { start.getRed() / 255.0f, start.getGreen() / 255.0f, start.getBlue() / 255.0f, start.getAlpha() / 255.0f };
		this.end = new float[] { end.getRed() / 255.0f, end.getGreen() / 255.0f, end.getBlue() / 255.0f, end.getAlpha() / 255.0f };
		this.length = 4;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void computeColor(float value, float[] color) {
		if (color == null || color.length < 3) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INVALID_COLOR_MESSAGE")); //$NON-NLS-1$
		}
		float v2 = 1.0f - value;
		int n = Math.min(color.length, this.length);
		for (int i = 0; i < n; i++) {
			color[i] = start[i] * v2 + end[i] * value;
		}
	}
}
