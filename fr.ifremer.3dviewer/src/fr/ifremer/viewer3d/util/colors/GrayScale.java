/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.colors;

import java.security.InvalidParameterException;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * A gray color scale composed of shades of grey.<br/>
 * This scale doesn't change translucence (alpha) value.
 * <p>
 * The 0.0 input is mapped to black and 1.0 input is mapped to white.
 * <p>
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class GrayScale implements IColorScale {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void computeColor(float value, float[] color) {
		if (color == null || color.length < 3) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INVALID_COLOR_MESSAGE"));
		}

		color[0] = value;
		color[1] = value;
		color[2] = value;
	}

}
