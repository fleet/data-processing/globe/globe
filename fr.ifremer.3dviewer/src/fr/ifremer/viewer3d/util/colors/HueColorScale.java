/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.colors;

import java.security.InvalidParameterException;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * A color scale based on hue value from blue to red (through green).<br/>
 * This scale doesn't change translucence (alpha) value.
 * <p>
 * The 0.0 input is mapped to blue and the 1.0 input is mapped to red.
 * <p>
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class HueColorScale implements IColorScale {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void computeColor(float value, float[] color) {
		if (color == null || color.length < 3) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_BICOLORSCALE_INVALID_COLOR_MESSAGE"));
		}

		// hsl scale from blue to red (through green)
		// h from 0.66 to 0.0 ; s = 1.0 ; l = 0.5
		double h = 2.0 / 3.0 - value * 2.0 / 3.0;
		double tempr = h + 1.0 / 3.0;
		if (tempr > 1.0) {
			tempr -= 1.0;
		}
		double tempg = h;
		double tempb = h - 1.0 / 3.0;
		if (tempb < 0) {
			tempb += 1.0;
		}

		// red value
		if (6.0 * tempr < 1.0) {
			color[0] = (float) (6.0 * tempr);
		} else if (2.0 * tempr < 1.0) {
			color[0] = 1.0f;
		} else if (3.0 * tempr < 2.0) {
			color[0] = (float) (((2.0 / 3.0) - tempr) * 6.0);
		} else {
			color[0] = 0.0f;
		}
		// green value
		if (6.0 * tempg < 1.0) {
			color[1] = (float) (6.0 * tempg);
		} else if (2.0 * tempg < 1.0) {
			color[1] = 1.0f;
		} else if (3.0 * tempg < 2.0) {
			color[1] = (float) (((2.0 / 3.0) - tempg) * 6.0);
		} else {
			color[1] = 0.0f;
		}
		// blue value
		if (6.0 * tempb < 1.0) {
			color[2] = (float) (6.0 * tempb);
		} else if (2.0 * tempb < 1.0) {
			color[2] = 1.0f;
		} else if (3.0 * tempr < 2.0) {
			color[2] = (float) (((2.0 / 3.0) - tempb) * 6.0);
		} else {
			color[2] = 0.0f;
		}
	}

}
