/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.colors;

import java.security.InvalidParameterException;

/**
 * This interface provides methods to get a RGB color from a linear input value.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public interface IColorScale {

	/**
	 * Compute color from input value.
	 * 
	 * @param value
	 *            input value, range [0.0;1.0]
	 * @param color
	 *            [inout] result colors (RGB). This array length must be at
	 *            least 3. Colors are returned as float values : 0.0,0.0,0.0 is
	 *            black and 1.0,1.0,1.0 is white.
	 * @throws InvalidParameterException
	 *             if the color array isn't correctly initialized
	 */
	public void computeColor(float value, float[] color);
}
