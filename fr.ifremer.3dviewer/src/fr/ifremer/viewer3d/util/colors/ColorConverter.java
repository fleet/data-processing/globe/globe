/*
 * @License@ 
 */
package fr.ifremer.viewer3d.util.colors;

import gov.nasa.worldwind.util.Logging;

import java.awt.Color;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * This is a JAXB color converter. The color string pattern is three float
 * values surrounded by brackets (eg. [0.5 0.1 0.75]). Color order is Red, Green
 * and Blue.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class ColorConverter {

	/**
	 * Parse string and return color value. If the string cannot be parsed black
	 * color is returned.
	 * 
	 * @param value
	 *            the string to parse
	 * @return the parsed color or black if value cannot be parsed
	 */
	public static Color parseStringToColor(String value) {
		if (value != null && value.length() > 2) {
			String[] values = value.substring(1, value.length() - 1).split("\\s+"); //$NON-NLS-1$
			if (values != null && values.length == 3) {
				try {
					float r = Float.valueOf(values[0]);
					float g = Float.valueOf(values[1]);
					float b = Float.valueOf(values[2]);

					Color color = new Color(r, g, b);
					return color;
				} catch (NumberFormatException e) {
					Logging.logger().warning(DicoBundle.getString("KEY_COLORCONVERTER_INVALID_VALUE") + e.getMessage()); //$NON-NLS-1$
				}
			}
			Logging.logger().warning(DicoBundle.getString("KEY_COLORCONVERTER_INVALID_FORMAT")); //$NON-NLS-1$
		}
		return Color.BLACK;
	}

	/**
	 * Return the provided color as its string value.
	 * 
	 * @param value
	 *            the color to convert to string
	 * @return the value's string representation or [0 0 0] (ie. black color
	 *         representation) if value is null.
	 */
	public static String printColorToString(Color value) {
		if (value != null) {
			StringBuilder sb = new StringBuilder("["); //$NON-NLS-1$
			sb.append(value.getRed() / 255.0f);
			sb.append(" "); //$NON-NLS-1$
			sb.append(value.getGreen() / 255.0f);
			sb.append(" "); //$NON-NLS-1$
			sb.append(value.getBlue() / 255.0f);
			sb.append("]"); //$NON-NLS-1$

			return sb.toString();
		}

		return "[0 0 0]"; //$NON-NLS-1$
	}

}
