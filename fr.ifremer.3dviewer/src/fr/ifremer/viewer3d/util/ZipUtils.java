/*
 * @License@
 */

package fr.ifremer.viewer3d.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.Adler32;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.CheckedOutputStream;
import java.util.zip.Checksum;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility methods for zip files.
 */
final public class ZipUtils {

	private static Logger logger = LoggerFactory.getLogger(ZipUtils.class);

	/** Compression buffer size. */
	public static final int BUFFER_SIZE = 2048;
	/** Null checksum : no check. */
	public static final Checksum CHECK_NONE = null;
	/** Checksum using Adler32 algorithm. */
	public static final Checksum CHECK_ADLER32 = new Adler32();
	/** Checksum using CRC32 algorithm. */
	public static final Checksum CHECK_CRC32 = new CRC32();

	/** Utility class : no instance. */
	private ZipUtils() {
	};

	/**
	 * Extract specified archive into its directory. Previoulsy existing files are
	 * overwritten.
	 * 
	 * @param file the archive file to extract.
	 */
	public static final void unzip(File file) throws IOException {
		String path = file.getAbsolutePath();
		unzip(file, path.substring(0, path.lastIndexOf(File.separatorChar)));
	}

	/**
	 * Extract specified archive into its directory using specified checksum.
	 * Previoulsy existing files are overwritten.
	 * 
	 * @param file     the archive file to extract.
	 * @param checksum the checksum to use during unzip.
	 */
	public static final void unzip(File file, Checksum checksum) throws IOException {
		String path = file.getAbsolutePath();
		unzip(file, path.substring(0, path.lastIndexOf(File.separatorChar)), checksum);
	}

	/**
	 * Extract specified archive into destination directory. Previoulsy existing
	 * files are overwritten.
	 * 
	 * @param file            the archive file to extract
	 * @param destinationPath destination directory.
	 */
	public static final void unzip(File file, String destinationPath) throws IOException {
		unzip(file, destinationPath, true, CHECK_NONE);
	}

	/**
	 * Extract specified archive into destination directory using specified
	 * chacksum. Previoulsy existing files are overwritten.
	 * 
	 * @param file            the archive file to extract
	 * @param destinationPath destination directory
	 * @param checksum        the checksum to use during unzip
	 */
	public static final void unzip(File file, String destinationPath, Checksum checksum) throws IOException {
		unzip(file, destinationPath, true, checksum);
	}

	/**
	 * Extract specified archive into destination directory using specified
	 * chacksum. Allows to choose if existing files should be overwritten.
	 * 
	 * @param file            the archive file to extract
	 * @param destinationPath destination directory
	 * @param force           if true existing files will be overwritten.
	 * @param checksum        the checksum to use during unzip
	 */
	public static final void unzip(File file, String destinationPath, boolean force, Checksum checksum)
			throws IOException {
		ZipFile zipfile = new ZipFile(file);
		byte[] buf = new byte[BUFFER_SIZE];
		FileInputStream fis = new FileInputStream(zipfile.getName());
		CheckedInputStream cis = (checksum == null) ? null : new CheckedInputStream(fis, checksum);
		BufferedInputStream bis = new BufferedInputStream((cis != null) ? cis : fis);
		ZipInputStream zis = new ZipInputStream(bis);
		ZipEntry entry = null;
		try {
			while ((entry = zis.getNextEntry()) != null) {
				File f = new File(destinationPath + File.separatorChar + entry.getName());
				if (entry.isDirectory()) {
					if (!f.exists()) {
						f.mkdirs();
					}
					continue;
				} else {
					File parent = f.getParentFile();
					if (!parent.exists()) {
						parent.mkdirs();
					}
				}
				if (f.exists()) {
					if (force) {
						f.delete();
					} else {
						continue;
					}
				}
				f.createNewFile();
				FileOutputStream fos = new FileOutputStream(f);
				BufferedOutputStream bos = new BufferedOutputStream(fos, BUFFER_SIZE);
				int nbRead;
				try {
					while ((nbRead = zis.read(buf)) > 0) {
						bos.write(buf, 0, nbRead);
					}
				} finally {
					bos.flush();
					bos.close();
					fos.close();
				}
			}
		} finally {
			zis.close();
			bis.close();
			if (cis != null) {
				cis.close();
			}
			fis.close();
			zipfile.close();
		}
	}

	/**
	 * Zip specified files into a new archive.
	 * 
	 * @param zipFile the zip archive where files will be compressed. This file must
	 *                already exists.
	 * @param files   files to archive.
	 */
	public static final void zip(File zipFile, File... files) throws IOException {
		zip(zipFile, Deflater.BEST_COMPRESSION, files);
	}

	/**
	 * Zip specified files into a new archive using specified compression level.
	 * 
	 * @param zipFile the zip archive where files will be compressed. This file must
	 *                already exists.
	 * @param level   compression level.
	 * @param files   files to archive.
	 */
	public static final void zip(File zipFile, int level, File... files) throws IOException {
		zip(zipFile, level, CHECK_NONE, files);
	}

	/**
	 * Zip specified files into a new archive using specified checksum.
	 * 
	 * @param zipFile  the zip archive where files will be compressed. This file
	 *                 must already exists.
	 * @param checksum checksum to use.
	 * @param files    files to archive.
	 */
	public static final void zip(File zipFile, Checksum checksum, File... files) throws IOException {
		zip(zipFile, Deflater.BEST_COMPRESSION, checksum, files);
	}

	/**
	 * Zip specified files into a new archive using specified checksum and
	 * compression level.
	 * 
	 * @param zipFile  the zip archive where files will be compressed. This file
	 *                 must already exists.
	 * @param level    compression level
	 * @param checksum checksum to use.
	 * @param files    files to archive.
	 */
	public static final void zip(File zipFile, int level, Checksum checksum, File... files)
			throws FileNotFoundException, IOException {
		FileOutputStream fos = new FileOutputStream(zipFile);
		CheckedOutputStream cos = (checksum == null) ? null : new CheckedOutputStream(fos, checksum);
		BufferedOutputStream bos = new BufferedOutputStream((cos != null) ? cos : fos);
		ZipOutputStream zos = new ZipOutputStream(bos);
		zos.setMethod(ZipOutputStream.DEFLATED);
		zos.setLevel(level);
		try {
			for (File f : files) {
				String path = f.getCanonicalPath();
				path = path.substring(0, path.lastIndexOf(File.separator));
				HashMap<ZipEntry, File> entries = createEntries(path, "", f);
				byte[] buf = new byte[BUFFER_SIZE];
				for (Map.Entry<ZipEntry, File> e : entries.entrySet()) {
					if (e.getValue().length() == 0) {
						ZipEntry zipEntry = e.getKey();
						zipEntry.setSize(0);
						zipEntry.setCrc(0);
						zipEntry.setMethod(ZipOutputStream.STORED);
						zos.closeEntry();
						zos.putNextEntry(e.getKey());
						continue;
					}
					zos.putNextEntry(e.getKey());
					FileInputStream fis = new FileInputStream(e.getValue());
					BufferedInputStream bis = new BufferedInputStream(fis, BUFFER_SIZE);
					int nbRead;
					try {
						while ((nbRead = bis.read(buf)) > 0) {
							zos.write(buf, 0, nbRead);
						}
					} finally {
						zos.closeEntry();
						bis.close();
						fis.close();
					}
				}
			}
			zos.finish();
		} finally {
			zos.close();
			bos.close();
			if (cos != null) {
				cos.close();
			}
			fos.close();
		}
	}

	private static final HashMap<ZipEntry, File> createEntries(String path, String parentPath, File... files)
			throws IOException {
		path += path.endsWith(File.separator) ? "" : File.separator;
		HashMap<ZipEntry, File> map = new HashMap<ZipEntry, File>();
		for (File f : files) {
			if (f.isDirectory()) {
				String entrypath = parentPath.replace(File.separatorChar, '/') + f.getName();
				entrypath += entrypath.endsWith("/") ? "" : "/";
				HashMap<ZipEntry, File> temp = createEntries(path, entrypath, f.listFiles());
				map.put(new ZipEntry(entrypath), f);
				for (Map.Entry<ZipEntry, File> e : temp.entrySet()) {
					map.put(e.getKey(), e.getValue());
				}
			} else {
				String key = parentPath.concat(f.getName()).replace(File.separatorChar, '/');
				map.put(new ZipEntry(key), f);
			}
		}
		return map;
	}

	public static File unzipDataInCache(File inputFile, File dataFile) throws GIOException {
		// Test présence des données
		// File parent = inputFile.getParentFile();
		// File item = new File(parent.getAbsolutePath()+"/"+
		// infoXML.getSignals().getItems().get(0).getFileName());
		// File parentItem = item.getParentFile(); // data
		File fileStoreLocation = WorldWindUtils.getInstallLocation(); // cache
		File dataTemp = new File(fileStoreLocation.getAbsolutePath() + "/" + NasaCache.getCacheSubDirectory() + "/"
				+ dataFile.getName());// data
		// du
		// cache
		File inputFileTemp = new File(fileStoreLocation.getAbsolutePath() + "/" + NasaCache.getCacheSubDirectory() + "/"
				+ inputFile.getName());// inputFile
		// du
		// cache
		File itemZip = new File(dataFile.getAbsoluteFile() + ".zip"); // data.zip

		if (dataFile.exists()) {// Test présence des données à coté du xml

		} else if (dataTemp.exists() && inputFileTemp.exists()) {// Test
			// présence
			// des
			// données
			// dans le
			// cache
			logger.debug("chargement données dans le chache...");
			inputFile = inputFileTemp;
		} else if (itemZip.exists()) {// Test présence des données zippé à coté
			// du xml
			try {
				logger.debug("décompression des données dans le cache...");
				ZipUtils.unzip(itemZip,
						fileStoreLocation.getAbsolutePath() + "/" + NasaCache.getCacheSubDirectory() + "/"); // décompression
				// dans
				// /tmp
				File fileCopy = new File(inputFileTemp.getAbsolutePath()); // nouveau
				// inputFile
				// dans
				// /tmp
				FileUtils.copy(inputFile, fileCopy); // copie du inputFile
				inputFile = fileCopy;
			} catch (Exception e) {
				logger.warn("Error accessing data cache directory", e);
			}
		}
		return inputFile;
	}

}