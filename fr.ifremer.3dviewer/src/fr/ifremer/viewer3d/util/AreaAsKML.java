package fr.ifremer.viewer3d.util;

import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.render.airspaces.editor.AirspaceControlPoint;
import gov.nasa.worldwind.render.airspaces.editor.AirspaceEditor;
import gov.nasa.worldwind.render.airspaces.editor.PolygonEditor;
import gov.nasa.worldwind.util.Logging;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * ??? A supprimer ?
 * 
 * @author ?
 */
public class AreaAsKML {

	private List<AirspaceControlPoint> points;
	private String layerName;
	private AirspaceEditor editor;

	public AreaAsKML(List<AirspaceControlPoint> points, String layerName, AirspaceEditor editor) {
		this.points = points;
		this.layerName = layerName;
		this.editor = editor;
	}

	/**
	 * Saves kml into specified file.
	 * 
	 * @param file
	 * 
	 */
	public void saveAreaAsKML(File file) {
		if (file != null) {

			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = factory.newDocumentBuilder();
				Document doc = docBuilder.newDocument();

				saveKML(doc);

				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");

				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(file);

				transformer.transform(source, result);

			} catch (Exception e) {
				Logging.logger().severe("Error while writing area file " + file.getPath());
			}
		}
	}

	private void saveKML(Document document) {
		String root = "kml";
		Element rootElement = document.createElement(root);
		rootElement.setAttribute("xmlns", "http://earth.google.com/kml/2.2");
		document.appendChild(rootElement);

		// node document
		Element doc = document.createElement("Document");
		rootElement.appendChild(doc);

		// node Visibility
		Element visibility = document.createElement("Visibility");
		doc.appendChild(visibility);

		// node DocumentSource
		Element documentSrc = document.createElement("DocumentSource");
		doc.appendChild(documentSrc);

		// node Placemark
		Element placemark = document.createElement("Placemark");
		doc.appendChild(placemark);

		// node name
		Element name = document.createElement("name");
		name.setTextContent(layerName);
		placemark.appendChild(name);

		// node Style
		Element style = document.createElement("Style");
		style.setAttribute("id", layerName);
		placemark.appendChild(style);

		// node PolyStyle
		Element polyStyle = document.createElement("PolyStyle");
		style.appendChild(polyStyle);

		// node color
		Element color = document.createElement("color");
		color.setTextContent("775555ee");
		polyStyle.appendChild(color);

		// node outline
		Element outline = document.createElement("outline");
		outline.setTextContent("0");
		polyStyle.appendChild(outline);

		// node MultiGeometry
		Element multiGeometry = document.createElement("MultiGeometry");
		placemark.appendChild(multiGeometry);

		saveArea(document, multiGeometry);
	}

	private void saveArea(Document document, Element multiGeometry) {

		// node Polygon
		Element polygon = document.createElement("Polygon");
		multiGeometry.appendChild(polygon);

		// node altitudeMode
		Element altitudeMode = document.createElement("altitudeMode");
		altitudeMode.setTextContent("absolute");
		polygon.appendChild(altitudeMode);

		// node outerBoundaryIs
		Element outerBoundaryIs = document.createElement("outerBoundaryIs");
		polygon.appendChild(outerBoundaryIs);

		// node LinearRing
		Element linearRing = document.createElement("LinearRing");
		outerBoundaryIs.appendChild(linearRing);

		// node coordinates
		Element coordinates = document.createElement("coordinates");
		if (editor instanceof PolygonEditor) {
			List<LatLon> locationList = ((PolygonEditor) editor).getPolygon().getLocations();
			String coordinate = "";
			for (int i = 0; i < points.size(); i++) {
				if (((PolygonEditor) editor).getPolygon().getAltitudes()[points.get(i).getAltitudeIndex()] != 0) {
					coordinate += "\n" + locationList.get(points.get(i).getLocationIndex()).getLongitude().getDegrees() + ", "
							+ locationList.get(points.get(i).getLocationIndex()).getLatitude().getDegrees() + ", "
							+ (((PolygonEditor) editor).getPolygon().getAltitudes()[points.get(i).getAltitudeIndex()] + 10.0);// 10m
				}
			}
			// au
			// dessus
			// du
			// sol
			// afin
			// que
			// la
			// zone
			// soit
			// visible
			coordinates.setTextContent(coordinate);
			linearRing.appendChild(coordinates);
		}

	}
}
