package fr.ifremer.viewer3d.util.processing;

import java.beans.PropertyChangeListener;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Sector;

/***
 * Interface used for moving sector Used mainly to simulate tectonic movement
 * 
 * @author MORVAN
 * 
 */
public interface ISectorMovementProcessing {

	public static final String PROPERTY_DATE_INDEX = "date";

	void addPropertyChangeListener(PropertyChangeListener pcl);

	void removePropertyChangeListener(PropertyChangeListener pcl);

	/***
	 * computes the new position Useful for tectonic movement for exemple
	 * 
	 * @param s
	 */
	public Sector move(Sector s);

	/**
	 * computes the new position as LaLon
	 * 
	 * @param latitude
	 * @param longitude
	 * @param angle
	 */
	public LatLon move(Angle latitude, Angle longitude);

	/***
	 * computes the previous position Useful for tectonic movement for exemple
	 * 
	 * @param s
	 */
	public Sector moveInv(Sector s);

	public LatLon move(LatLon latLon);

	public Sector move(Sector s, int indexDate);

	public LatLon move(LatLon latLon, int indexDate);

	public Sector moveInv(Sector s, int indexDate);

	public LatLon moveInv(LatLon latLon);

	public LatLon moveInv(LatLon latLon, int indexDate);

	/***
	 * 
	 * @return the Matrix 4*4 defining the movement
	 */
	public Matrix getMovement();

	public Matrix getMovementInv(int indexDate);

	/***
	 * 
	 * @return the Matrix 4*4 defining the inverse movement
	 */
	public Matrix getMovementInv();

	public Matrix getMovement(int indexDate);

	/***
	 * 
	 * @return the current index
	 */
	public int getDateIndex();

	/***
	 * 
	 * @return true if movement is defined
	 */
	public boolean hasMovement();

	public ISectorMovementProcessing getReferentialMovement();

}
