package fr.ifremer.viewer3d.view;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.view.BasicView;
import gov.nasa.worldwind.view.firstperson.BasicFlyView;
import gov.nasa.worldwind.view.firstperson.FlyViewInputHandler;
import gov.nasa.worldwind.view.firstperson.FlyViewLimits;

public class GlobeFlyView extends BasicFlyView {

	/** true when view has changed */
	protected boolean pointOfViewHasChanged = false;

	public GlobeFlyView(BasicView view) {
		final double GLOBE_DEFAULT_MIN_ELEVATION = -12_000_000;
		final double GLOBE_DEFAULT_MAX_ELEVATION = DEFAULT_MAX_ELEVATION * 10; /** use 40 km as max */

		this.viewInputHandler = new FlyViewInputHandler();
		this.viewLimits = new FlyViewLimits();
		this.viewLimits.setPitchLimits(DEFAULT_MIN_PITCH, DEFAULT_MAX_PITCH);
		this.viewLimits.setEyeElevationLimits(GLOBE_DEFAULT_MIN_ELEVATION, GLOBE_DEFAULT_MAX_ELEVATION);
		this.setValues(view.copy());
		this.eyePosition = view.getCurrentEyePosition();
		this.lastEyePosition = view.getEyePosition();

		this.lastEyePoint = view.getEyePoint();
		this.heading = view.getHeading();
		this.pitch = view.getPitch();
		this.roll = view.getRoll();
		this.lastUpVector = view.getUpVector();
		this.lastForwardVector = view.getCurrentForwardVector();
		this.dc = view.getDC();
		this.globe = view.getGlobe();
		this.detectCollisions = true;
		this.farClipDistance = view.getFarClipDistance();
		this.fieldOfView = view.getFieldOfView();
		this.frustum = view.getFrustum();
		this.hadCollisions = true;
		this.horizonDistance = view.getHorizonDistance();
		this.lastFrustumInModelCoords = view.getFrustumInModelCoordinates();
		this.modelview = view.getModelviewMatrix();
		this.nearClipDistance = view.getNearClipDistance();
		this.projection = view.getProjectionMatrix();
		this.viewport = view.getViewport();
		this.viewStateID = view.getViewStateID();
		this.updateModelViewStateID();
	}

	public void update() {
		this.updateModelViewStateID();

	}

	@Override
	public String toString() {

		String str = "Eye position :" + this.getEyePosition().toString() + ", pitch :" + this.getPitch() + ", heading :"
				+ this.getHeading();
		return str;

	}

	/**
	 * @see gov.nasa.worldwind.view.firstperson.BasicFlyView#setEyePosition(gov.nasa.worldwind.geom.Position)
	 */
	@Override
	public void setEyePosition(Position eyePosition) {
		super.setEyePosition(eyePosition);
		pointOfViewHasChanged = true;
	}

	/**
	 * @see gov.nasa.worldwind.view.firstperson.BasicFlyView#setHeading(gov.nasa.worldwind.geom.Angle)
	 */
	@Override
	public void setHeading(Angle heading) {
		super.setHeading(heading);
		pointOfViewHasChanged = true;
	}

	/**
	 * @see gov.nasa.worldwind.view.firstperson.BasicFlyView#setPitch(gov.nasa.worldwind.geom.Angle)
	 */
	@Override
	public void setPitch(Angle pitch) {
		super.setPitch(pitch);
		pointOfViewHasChanged = true;
	}

	/**
	 * @see gov.nasa.worldwind.view.BasicView#setRoll(gov.nasa.worldwind.geom.Angle)
	 */
	@Override
	public void setRoll(Angle roll) {
		super.setRoll(roll);
		pointOfViewHasChanged = true;
	}

	/**
	 * Getter of pointOfViewHasChanged
	 */
	public boolean isPointOfViewHasChanged() {
		return pointOfViewHasChanged;
	}

	/**
	 * Setter of pointOfViewHasChanged
	 */
	public void setPointOfViewHasChanged(boolean pointOfViewHasChanged) {
		this.pointOfViewHasChanged = pointOfViewHasChanged;
	}

}
