/*
 * @License@
 */
package fr.ifremer.viewer3d.view.orbit;

import java.awt.event.MouseEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.animation.AnimationController;
import gov.nasa.worldwind.animation.RotateToAngleAnimator;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.ViewInputAttributes;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.view.BasicView;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import gov.nasa.worldwind.view.orbit.OrbitView;
import gov.nasa.worldwind.view.orbit.OrbitViewInputHandler;

/**
 * This input handler overrides {@link OrbitViewInputHandler} in order to handle under or above ground specific
 * behaviors.
 */
public class OrbitView3DInputHandler extends OrbitViewInputHandler {

	private Logger logger = LoggerFactory.getLogger(OrbitView3DInputHandler.class);

	/**
	 * This flag is <code>true</code> if the current view position is under ground.
	 */
	private boolean _underGround = true;

	/** Enables the globe rotation with the mouse */
	protected boolean enabledMouseDragged = true;

	public OrbitView3DInputHandler() {
		super();

		logger.debug("Current input handler : OrbitView3DInputHandler");

		// TODO : inertie
		// ActionAttributes att =
		// getAttributes().getActionAttributes(ViewInputAttributes.DEVICE_MOUSE,
		// ViewInputAttributes.VIEW_ROTATE);
		// if(att != null){
		// att.setSmoothingValue(0.95);
		// }
		// att =
		// getAttributes().getActionAttributes(ViewInputAttributes.DEVICE_MOUSE,
		// ViewInputAttributes.VIEW_HORIZONTAL_TRANSLATE);
		// if(att != null){
		// att.setSmoothingValue(0.95);
		// }
	}

	protected class CollisionAwarePitchAccessor3D extends CollisionAwarePitchAccessor {

		public CollisionAwarePitchAccessor3D(OrbitView orbitView) {
			super(orbitView);
			// allows underground visualization
			orbitView.setDetectCollisions(false);
		}

		/**
		 * Sets the pitch property of this accessor's view to the specified value. If the value is null, setting the
		 * view's pitch causes a surface collision, or setting the view's pitch causes an exception, this returns false.
		 * Otherwise this returns true.
		 *
		 * @param value the value to set as this view's pitch property.
		 *
		 * @return true if the pitch property was successfully set, and false otherwise.
		 */
		@Override
		public boolean setAngle(Angle value) {
			if (value == null) {
				return false;
			}

			// If the view supports surface collision detection, then clear the
			// view's collision flag prior to
			// making any property changes.
			if (orbitView.isDetectCollisions()) {
				orbitView.hadCollisions();
			}

			boolean collision = false;
			try {
				orbitView.setPitch(value);
				Globe globe = orbitView.getGlobe();
				Position position = globe.computePositionFromPoint(orbitView.getCurrentEyePoint());
				double verticalExaggeration = ((BasicView) orbitView).getDC().getVerticalExaggeration();

				double elevation = 0.0;
				elevation = globe.getElevation(position.getLatitude(), position.getLongitude()) * verticalExaggeration;
				double height = position.getElevation() - elevation;
				if (height >= 0) {
					if (_underGround) {
						collision = true;
						_underGround = false;
						Viewer3D.fitUnderGround(_underGround);
					}
				} else {
					if (!_underGround) {
						collision = true;
						_underGround = true;
						Viewer3D.fitUnderGround(_underGround);
					}
				}
			} catch (Exception e) {
				String message = Logging.getMessage("generic.ExceptionWhileChangingView");

				Logging.logger().log(java.util.logging.Level.SEVERE, message, e);
				return false;
			}

			// If the view supports surface collision detection, then return
			// false if the collision flag is set,
			// otherwise return true.
			boolean flag = !orbitView.isDetectCollisions() || !collision;
			return flag;
		}
	}

	@Override
	protected void changePitch(BasicOrbitView view, AnimationController animControl, Angle change,
			ViewInputAttributes.ActionAttributes attrib) {
		view.computeAndSetViewCenterIfNeeded();

		double smoothing = attrib.getSmoothingValue();
		if (!attrib.isEnableSmoothing() || !this.isEnableSmoothing()) {
			smoothing = 0.0;
		}

		if (smoothing == 0.0) {
			if (animControl.get(VIEW_ANIM_PITCH) != null) {
				animControl.remove(VIEW_ANIM_PITCH);
			}
			Angle newPitch = computeNewPitch(view, view.getPitch().add(change));
			view.setPitch(newPitch);
		} else {
			RotateToAngleAnimator angleAnimator = (RotateToAngleAnimator) animControl.get(VIEW_ANIM_PITCH);

			if (angleAnimator == null || !angleAnimator.hasNext()) {
				// Create an angle animator which tilts the view to the specified new pitch. If this changes causes the
				// view to collide with the surface, this animator is set to stop. We enable this behavior by using a
				// {@link #CollisionAwarePitchAccessor} angle accessor and setting the animator's stopOnInvalidState
				// property to 'true'.
				Angle newPitch = computeNewPitch(view, view.getPitch().add(change));
				angleAnimator = new RotateToAngleAnimator(view.getPitch(), newPitch, smoothing,
						new CollisionAwarePitchAccessor3D(view));
				angleAnimator.setStopOnInvalidState(true);
				animControl.put(VIEW_ANIM_PITCH, angleAnimator);
			} else {
				Angle newPitch = computeNewPitch(view, angleAnimator.getEnd().add(change));
				angleAnimator.setEnd(newPitch);
			}

			angleAnimator.start();
		}

		view.firePropertyChange(AVKey.VIEW, null, view);
	}

	/**
	 * @see gov.nasa.worldwind.awt.BasicViewInputHandler#handleMouseDragged(java.awt.event.MouseEvent)
	 */
	@Override
	protected void handleMouseDragged(MouseEvent e) {
		if (enabledMouseDragged) {
			super.handleMouseDragged(e);
		}
	}

	/**
	 * @return the {@link #enabledMouseDragged}
	 */
	public boolean isEnabledMouseDragged() {
		return enabledMouseDragged;
	}

	/**
	 * @param enabledMouseDragged the {@link #enabledMouseDragged} to set
	 */
	public void setEnabledMouseDragged(boolean enabledMouseDragged) {
		this.enabledMouseDragged = enabledMouseDragged;
	}

}
