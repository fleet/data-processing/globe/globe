/*
 * @License@
 */
package fr.ifremer.viewer3d.view;

import com.jogamp.opengl.GL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Frustum;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.view.BasicView;
import gov.nasa.worldwind.view.ViewUtil;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import gov.nasa.worldwind.view.orbit.OrbitViewInputSupport;
import gov.nasa.worldwind.view.orbit.OrbitViewLimits;

/**
 * BasicOrbitView override to allow SonarScope-3DViewer specific behavior such as its own clipping plane distance.
 *
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class SSVOrbitView extends BasicOrbitView {

	private Logger logger = LoggerFactory.getLogger(SSVOrbitView.class);

	/** true when view has changed */
	protected boolean pointOfViewHasChanged = false;

	public SSVOrbitView() {

		logger.debug("Current view : SSVOrbitView");
		//AnnotationProcessor.process(this);
		setDetectCollisions(false);

		OrbitViewLimits ovl = getOrbitViewLimits();
		if (ovl != null) {
			// overrides pitch angle limits in order to allow beneath ground
			// vizualisation
			ovl.setPitchLimits(Angle.ZERO, Angle.POS180);
		}
	}

	public SSVOrbitView(BasicView view) {
		logger.debug("Current view : SSVOrbitView");
		//AnnotationProcessor.process(this);
		setDetectCollisions(false);

		OrbitViewLimits ovl = getOrbitViewLimits();
		if (ovl != null) {
			// overrides pitch angle limits in order to allow beneath ground
			// vizualisation
			ovl.setPitchLimits(Angle.ZERO, Angle.POS180);
		}
		this.setEyePosition(view.getEyePosition());
		try {
			this.setCenterPosition(view.getCenterPosition());
		} catch (NullPointerException npe) {

		}
		this.setPitch(view.getPitch());
		this.setHeading(view.getHeading());
	}

	@Override
	public String toString() {

		String str = "Eye position :" + this.getEyePosition().toString() + ", pitch :" + this.getPitch() + ", heading :"
				+ this.getHeading() + ", zoom : " + this.getZoom();
		return str;

	}

	/**
	 * Computes the near clipping plane distance. This method is overridden to allow a closer clipping plane.
	 */
	@Override
	protected double computeNearDistance(Position eyePosition) {
		double near = 0;
		if (eyePosition != null && this.dc != null) {
			double elevation = ViewUtil.computeElevationAboveSurface(this.dc, eyePosition);
			double tanHalfFov = this.fieldOfView.tanHalfAngle();
			near = elevation / (8 * Math.sqrt(2 * tanHalfFov * tanHalfFov + 1));
		}
		return near < MINIMUM_NEAR_DISTANCE ? MINIMUM_NEAR_DISTANCE : near;
	}

	/**
	 * Overridden to fix far clipping distance when using projected earth.
	 */
	@Override
	protected double computeFarDistance(Position eyePosition) {
		if (this.dc.is2DGlobe()) {
			return super.computeFarDistance(eyePosition);
		}

		double far = 0;
		if (eyePosition != null) {
			far = computeHorizonDistance(eyePosition);
		}

		return far;
	}

	@Override
	protected double computeHorizonDistance() {
		double horizon = 0;
		Position eyePos = computeEyePositionFromModelview();
		if (eyePos != null) {
			horizon = computeHorizonDistance(eyePos);
		}

		return horizon;
	}

	@Override
	protected void doApply(DrawContext dc) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (dc.getGL() == null) {
			String message = Logging.getMessage("nullValue.DrawingContextGLIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}
		if (dc.getGlobe() == null) {
			String message = Logging.getMessage("nullValue.DrawingContextGlobeIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		// Update DrawContext and Globe references.
		this.dc = dc;
		this.globe = this.dc.getGlobe();
		// ========== modelview matrix state ==========//
		// Compute the current modelview matrix.
		this.modelview = OrbitViewInputSupport.computeTransformMatrix(this.globe, this.center, this.heading, this.pitch,
				this.roll, this.zoom);
		if (this.modelview == null) {
			this.modelview = Matrix.IDENTITY;
		}
		// Compute the current inverse-modelview matrix.
		this.modelviewInv = this.modelview.getInverse();
		if (this.modelviewInv == null) {
			this.modelviewInv = Matrix.IDENTITY;
		}

		// ========== projection matrix state ==========//
		// Get the current OpenGL viewport state.
		int[] viewportArray = new int[4];
		this.dc.getGL().glGetIntegerv(GL.GL_VIEWPORT, viewportArray, 0);
		this.viewport = new java.awt.Rectangle(viewportArray[0], viewportArray[1], viewportArray[2], viewportArray[3]);
		// Compute the current clip plane distances.
		this.nearClipDistance = computeNearClipDistance();
		this.farClipDistance = computeFarClipDistance();

		// BRL FIXME calcul far clip distance
		if (this.farClipDistance < 0.01f) {
			this.farClipDistance = 100000.0f;
		}

		// Compute the current viewport dimensions.
		double viewportWidth = this.viewport.getWidth() <= 0.0 ? 1.0 : this.viewport.getWidth();
		double viewportHeight = this.viewport.getHeight() <= 0.0 ? 1.0 : this.viewport.getHeight();
		// Compute the current projection matrix.
		this.projection = Matrix.fromPerspective(this.fieldOfView, viewportWidth, viewportHeight, this.nearClipDistance,
				this.farClipDistance);
		// Compute the current frustum.
		this.frustum = Frustum.fromPerspective(this.fieldOfView, (int) viewportWidth, (int) viewportHeight,
				this.nearClipDistance, this.farClipDistance);

		// ========== load GL matrix state ==========//
		loadGLViewState(dc, this.modelview, this.projection);

		// ========== after apply (GL matrix state) ==========//
		afterDoApply();
	}

	@Override
	public Position getEyePosition() {
		if (this.lastEyePosition == null) {
			this.lastEyePosition = computeEyePositionFromModelview();
		}
		return this.lastEyePosition;
	}

	/*@EventSubscriber(eventClass = CollisionDetectionBehaviorEvent.class)
	public void onGroundBehaviorEvent(CollisionDetectionBehaviorEvent evt) {
		setDetectCollisions(evt.isDetectCollision());
	}*/

	/**
	 * @see gov.nasa.worldwind.view.firstperson.BasicFlyView#setEyePosition(gov.nasa.worldwind.geom.Position)
	 */
	@Override
	public void setEyePosition(Position eyePosition) {
		super.setEyePosition(eyePosition);
		pointOfViewHasChanged = true;
	}

	/**
	 * @see gov.nasa.worldwind.view.firstperson.BasicFlyView#setHeading(gov.nasa.worldwind.geom.Angle)
	 */
	@Override
	public void setHeading(Angle heading) {
		super.setHeading(heading);
		pointOfViewHasChanged = true;
	}

	/**
	 * @see gov.nasa.worldwind.view.firstperson.BasicFlyView#setPitch(gov.nasa.worldwind.geom.Angle)
	 */
	@Override
	public void setPitch(Angle pitch) {
		super.setPitch(pitch);
		pointOfViewHasChanged = true;
	}

	/**
	 * @see gov.nasa.worldwind.view.orbit.BasicOrbitView#setZoom(double)
	 */
	@Override
	public void setZoom(double zoom) {
		super.setZoom(zoom);
		pointOfViewHasChanged = true;
	}

	/**
	 * @see gov.nasa.worldwind.view.BasicView#setRoll(gov.nasa.worldwind.geom.Angle)
	 */
	@Override
	public void setRoll(Angle roll) {
		super.setRoll(roll);
		pointOfViewHasChanged = true;
	}

	/**
	 * Getter of pointOfViewHasChanged
	 */
	public boolean isPointOfViewHasChanged() {
		return pointOfViewHasChanged;
	}

	/**
	 * Setter of pointOfViewHasChanged
	 */
	public void setPointOfViewHasChanged(boolean pointOfViewHasChanged) {
		this.pointOfViewHasChanged = pointOfViewHasChanged;
	}

}
