package fr.ifremer.viewer3d;

import fr.ifremer.viewer3d.geom.IMovableActivator;
import gov.nasa.worldwind.Movable;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.drag.Draggable;
import gov.nasa.worldwind.event.DragSelectEvent;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.geom.Intersection;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.util.Logging;

import java.awt.Point;

/**
 * Class extracted from {@link Dragger} Implements dragging for {@link Movable}
 * class with respect to the {@link IMovableActivator} interface that allow to
 * activate/desactivate movement
 * */
public class Dragger implements SelectListener {
	private final WorldWindow wwd;
	private boolean dragging = false;
	private boolean useTerrain = true;

	private Point dragRefCursorPoint;
	private Vec4 dragRefObjectPoint;
	private double dragRefAltitude;

	public Dragger(WorldWindow wwd) {
		if (wwd == null) {
			String msg = Logging.getMessage("nullValue.WorldWindow");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		this.wwd = wwd;
	}

	public Dragger(WorldWindow wwd, boolean useTerrain) {
		if (wwd == null) {
			String msg = Logging.getMessage("nullValue.WorldWindow");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		this.wwd = wwd;
		this.setUseTerrain(useTerrain);
	}

	public boolean isUseTerrain() {
		return useTerrain;
	}

	public void setUseTerrain(boolean useTerrain) {
		this.useTerrain = useTerrain;
	}

	public boolean isDragging() {
		return this.dragging;
	}

	@Override
	public void selected(SelectEvent event) {
		if (event == null) {
			String msg = Logging.getMessage("nullValue.EventIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (event.getEventAction().equals(SelectEvent.DRAG_END)) {
			this.dragging = false;
			event.consume();
		} else if (event.getEventAction().equals(SelectEvent.DRAG)) {
			DragSelectEvent dragEvent = (DragSelectEvent) event;
			Object topObject = dragEvent.getTopObject();
			if (topObject == null) {
				return;
			}

			// ignore drag if object are not movable
			if (!(topObject instanceof Movable)) {
				return;
			}

			if (topObject instanceof IMovableActivator && !((IMovableActivator) topObject).isMovable()) {
				return;
			}
			if (topObject instanceof Draggable && !((Draggable) topObject).isDragEnabled()) {
				return;
			}
			//

			Movable dragObject = (Movable) topObject;
			View view = wwd.getView();
			Globe globe = wwd.getModel().getGlobe();

			// Compute dragged object ref-point in model coordinates.
			// Use the Icon and Annotation logic of elevation as offset above
			// ground when below max elevation.
			Position refPos = dragObject.getReferencePosition();
			if (refPos == null) {
				return;
			}

			Vec4 refPoint = globe.computePointFromPosition(refPos);

			if (!this.isDragging()) // Dragging started
			{
				// Save initial reference points for object and cursor in screen
				// coordinates
				// Note: y is inverted for the object point.
				this.dragRefObjectPoint = view.project(refPoint);
				// Save cursor position
				this.dragRefCursorPoint = dragEvent.getPreviousPickPoint();
				// Save start altitude
				this.dragRefAltitude = globe.computePositionFromPoint(refPoint).getElevation();
			}

			// Compute screen-coord delta since drag started.
			int dx = dragEvent.getPickPoint().x - this.dragRefCursorPoint.x;
			int dy = dragEvent.getPickPoint().y - this.dragRefCursorPoint.y;

			// Find intersection of screen coord (refObjectPoint + delta) with
			// globe.
			double x = this.dragRefObjectPoint.x + dx;
			double y = event.getMouseEvent().getComponent().getSize().height - this.dragRefObjectPoint.y + dy - 1;
			Line ray = view.computeRayFromScreenPoint(x, y);
			Position pickPos = null;
			// Use intersection with sphere at reference altitude.
			Intersection inters[] = globe.intersect(ray, this.dragRefAltitude);
			if (inters != null) {
				pickPos = globe.computePositionFromPoint(inters[0].getIntersectionPoint());
			}

			if (pickPos != null) {
				// Intersection with globe. Move reference point to the
				// intersection point,
				// but maintain current altitude.
				Position p = new Position(pickPos, dragObject.getReferencePosition().getElevation());
				dragObject.moveTo(p);
			}
			this.dragging = true;
			event.consume();
		}
	}
}
