package fr.ifremer.viewer3d.gamepad;

import javax.swing.SwingUtilities;

import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.view.firstperson.BasicFlyView;
import gov.nasa.worldwind.view.orbit.OrbitView;

public class FlyingModel {
	protected WorldWindowGLCanvas wwd;

	protected double zoomStep = .8;

	protected double panfactor = .1;

	/**
	 * Set the zoom distance factor. Doubling this value will double the zooming speed. Negating it will reverse the
	 * zooming direction. Default value is .8.
	 *
	 * @param value the zooming distance factor.
	 */
	public void setZoomIncrement(double value) {
		this.zoomStep = value;
	}

	/**
	 * Get the zooming distance factor.
	 *
	 * @return the zooming distance factor.
	 */
	public double getZoomIncrement() {
		return this.zoomStep;
	}

	public void incrementPan() {
		panfactor *= 1.5;
	}

	public void decrementPan() {
		panfactor /= 1.5;
	}

	public FlyingModel(WorldWindowGLCanvas wwd) {
		this.wwd = wwd;
	}

	// Update view settings from control panel in a 'first person' perspective
	public void updateViewPosition(final GamePadEvent event) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				updateView(event);
			}
		});
	}

	private void updateView(GamePadEvent event) {
		if (wwd.getView() instanceof OrbitView) {
			OrbitView view = (OrbitView) this.wwd.getView();
			updateOrbitView(event, view);
		} else if (wwd.getView() instanceof BasicFlyView) {
			BasicFlyView view = (BasicFlyView) this.wwd.getView();
			updateFlyView(event, view);
		}
		this.wwd.redraw();
	}

	private double forwardFactor = 50000, headingFactor = 2, pitchFactor = 2;

	void updateFlyView(GamePadEvent event, BasicFlyView view) {
		view.stopAnimations();

		double elevationCoeff = computeCoeffFromElevation(view);
		if (event.getCommand().isButtonPanDec()) {
			this.decrementPan();
		}
		if (event.getCommand().isButtonPanInc()) {
			this.incrementPan();
		}

		double elevationMovement = computeElevationToAdd(view, event.getCommand().getZ());

		Vec4 eyeXYZ = view.getEyePoint();
		Vec4 eyeForward = view.getForwardVector();
		double forwardValue = -event.getCommand().getLy() * forwardFactor * elevationCoeff;
		System.err.println(event.getCommand().getLy() + " " + forwardValue + " " + elevationCoeff);
		Vec4 newEyeXYZ = eyeXYZ.add3(eyeForward.multiply3(forwardValue));
		Position newEyePosition = Viewer3D.getWwd().getModel().getGlobe().computePositionFromPoint(newEyeXYZ);
		newEyePosition = new Position(newEyePosition.getLatitude(), newEyePosition.getLongitude(),
				newEyePosition.elevation + elevationMovement);

		view.setHeading(view.getHeading().add(Angle.fromDegrees(event.getCommand().getRx() * headingFactor)));

		// Turn around if passing by a pole - TODO: better handling of the pole
		// crossing situation
		if (this.isPathCrossingAPole(newEyePosition, view.getEyePosition()))
			view.setHeading(Angle.POS180.subtract(view.getHeading()));
		view.setPitch(view.getPitch().add(Angle.fromDegrees(event.getCommand().getRy() * pitchFactor)));
		view.setEyePosition(newEyePosition);

	}

	void updateOrbitView(GamePadEvent event, OrbitView view) {
		// Stop iterators first
		view.stopAnimations();

		if (event.getCommand().isButtonPanDec()) {
			this.decrementPan();
		}
		if (event.getCommand().isButtonPanInc()) {
			this.incrementPan();
		}

		// Set view heading, pitch and fov

		view.setHeading(view.getHeading().add(Angle.fromRadians(event.getCommand().getHeading())));
		view.setRoll(view.getRoll().add(Angle.fromRadians(event.getCommand().getRoll())));
		view.setPitch(view.getPitch().add(Angle.fromRadians(event.getCommand().getPitch())));

		// Go some distance in the control mouse direction
		Angle heading = view.getHeading();

		Angle vect = Angle.fromXY(-event.getCommand().getY(), event.getCommand().getX());
		double angularPane = computeAngularAmount(view);

		Angle distance = Angle.fromRadians(Math.sqrt(event.getCommand().getY() * event.getCommand().getY()
				+ event.getCommand().getX() * event.getCommand().getX()) * angularPane);

		Angle direction = heading.add(vect);
		LatLon newViewCenter = LatLon.greatCircleEndPosition(view.getCenterPosition(), direction, distance);
		// Turn around if passing by a pole - TODO: better handling of the pole
		// crossing situation
		if (this.isPathCrossingAPole(newViewCenter, view.getCenterPosition()))
			view.setHeading(Angle.POS180.subtract(view.getHeading()));

		if (event.getCommand().getZ() != 0) {
			view.setZoom(computeNewZoom(view, 1 * event.getCommand().getZ()));
		}

		// Set new center pos
		view.setCenterPosition(new Position(newViewCenter, view.getCenterPosition().getElevation()));

	}

	protected boolean isPathCrossingAPole(LatLon p1, LatLon p2) {
		return Math.abs(p1.getLongitude().degrees - p2.getLongitude().degrees) > 20
				&& Math.abs(p1.getLatitude().degrees - 90 * Math.signum(p1.getLatitude().degrees)) < 10;
	}

	protected double computeNewZoom(OrbitView view, double amount) {
		double coeff = 0.05;
		double change = coeff * amount;
		double logZoom = view.getZoom() != 0 ? Math.log(view.getZoom()) : 0;
		// Zoom changes are treated as logarithmic values. This accomplishes two things:
		// 1) Zooming is slow near the globe, and fast at great distances.
		// 2) Zooming in then immediately zooming out returns the viewer to the same zoom value.
		return Math.exp(logZoom + change);
	}

	double computeAngularAmount(OrbitView view) {
		// Compute globe angular distance depending on eye altitude
		Position eyePos = view.getEyePosition();
		Position center = view.getCenterPosition();
		double groundelevation = Math.min(center.getElevation(), eyePos.getElevation());
		double radius = view.getGlobe().getRadiusAt(eyePos);

		double minValue = 0.5 * (180.0 / (Math.PI * radius)); // Minimum change
		// ~0.5 meters
		double maxValue = 1.0; // Maximum change ~1 degree
		// Compute an interpolated value between minValue and maxValue, using (eye altitude)/(globe radius) as the
		// interpolant.
		// Interpolation is performed on an exponential curve, to keep the value from increasing too quickly as eye
		// altitude increases.
		double a = eyePos.getElevation() / radius;
		if (groundelevation < 0) {
			radius += groundelevation; // we reduce the earth radius
			// under the sea evaluate a new coefficient
			a = (eyePos.getElevation() - groundelevation) / (radius);
		}

		a = (a < 0 ? 0 : (a > 1 ? 1 : a));
		double expBase = 2.0; // Exponential curve parameter.
		double value = panfactor
				* (minValue + (maxValue - minValue) * ((Math.pow(expBase, a) - 1.0) / (expBase - 1.0)));

		return value;
	}

	double computeElevationToAdd(BasicFlyView view, double input) {

		double minValue = 5; // metres
		double maxValue = 200000;
		double minElevationThreshold = -10000;
		double maxElevationThreshold = 4000000;
		double currentElevation = view.getEyePosition().getAltitude();
		if (currentElevation < minElevationThreshold) {
			return input * minValue;
		} else if (currentElevation > maxElevationThreshold) {
			return input * maxValue;
		} else {
			double coeff = (currentElevation - minElevationThreshold) / (maxElevationThreshold - minElevationThreshold);
			double elevToAdd = minValue + coeff * (maxValue - minValue);
			return input * elevToAdd;
		}
	}

	double computeForward() {
		return 0;
	}

	double computeCoeffFromElevation(BasicFlyView view) {

		Position eyePos = view.getEyePosition();
		double groundelevation = eyePos.getElevation();
		double radius = view.getGlobe().getRadiusAt(eyePos);

		double minValue = 0.5 * (180.0 / (Math.PI * radius)); // Minimum change
		// ~0.5 meters
		double maxValue = 1.0; // Maximum change ~1 degree
		// Compute an interpolated value between minValue and maxValue, using (eye altitude)/(globe radius) as the
		// interpolant.
		// Interpolation is performed on an exponential curve, to keep the value from increasing too quickly as eye
		// altitude increases.
		double a = eyePos.getElevation() / radius;
		if (groundelevation < 0) {
			radius += groundelevation; // we reduce the earth radius
			// under the sea evaluate a new coefficient
			a = (eyePos.getElevation() - groundelevation) / (radius);
		}

		a = (a < 0 ? 0 : (a > 1 ? 1 : a));
		double expBase = 2.0; // Exponential curve parameter.
		double value = panfactor
				* (minValue + (maxValue - minValue) * ((Math.pow(expBase, a) - 1.0) / (expBase - 1.0)));

		return value * 50;
	}

}
