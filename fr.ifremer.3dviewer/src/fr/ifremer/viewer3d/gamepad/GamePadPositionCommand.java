package fr.ifremer.viewer3d.gamepad;

public class GamePadPositionCommand {
	double validThreshold=0;	
	/**In radians*/
	double heading,pitch,roll;

	double x,y,z;
	double lx,ly,rx,ry;
	boolean buttonPanInc=false;
	boolean buttonPanDec=false;

	GamePadPositionCommand()
	{
		heading=0;
		pitch=0;
		roll=0;
		x=0;
		y=0;
		z=0;
		lx=ly=rx=ry=0;
		buttonPanInc=false;
		buttonPanDec=false;
	}
	public boolean isButtonPanInc() {
		return buttonPanInc;
	}
	public void setButtonPanInc(boolean buttonPanInc) {
		this.buttonPanInc = buttonPanInc;
	}
	public boolean isButtonPanDec() {
		return buttonPanDec;
	}
	public void setButtonPanDec(boolean buttonPanDec) {
		this.buttonPanDec = buttonPanDec;
	}

	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getZ() {
		return z;
	}
	public void setZ(double z) {
		this.z = z;
	}
	public double getHeading() {

		return heading;
	}

	public double getLx() {
		return lx;
	}	
	public void setLx(double lx) {
		this.lx = lx;
	}
	public double getLy() {
		return ly;
	}	
	public void setLy(double ly) {
		this.ly = ly;
	}
	public double getRx() {
		return rx;
	}	
	public void setRx(double rx) {
		this.rx = rx;
	}
	public double getRy() {
		return ry;
	}	
	public void setRy(double ry) {
		this.ry = ry;
	}

	public void setHeading(double value) {

		this.heading = value;
	}
	public double getPitch() {
		return pitch;
	}
	public void setPitch(double value) {

		this.pitch = value;
	}
	public double getRoll() {
		return roll;
	}
	public void setRoll(double value) {

		this.roll = value;
	}

	/**
	 * check if displacement is not empty, and the command shall be published
	 * */
	public boolean valid() {
		return Math.abs(heading)> validThreshold || Math.abs(pitch)>validThreshold || Math.abs(roll)>validThreshold 
				||Math.abs(x)> validThreshold || Math.abs(y)>validThreshold || Math.abs(z)>validThreshold
				||Math.abs(lx)>validThreshold || Math.abs(ly)>validThreshold|| Math.abs(rx)>validThreshold|| Math.abs(ry)>validThreshold
				||buttonPanDec || buttonPanInc 
				;
	}
}
