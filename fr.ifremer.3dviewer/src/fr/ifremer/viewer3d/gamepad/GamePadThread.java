package fr.ifremer.viewer3d.gamepad;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.Activator;
import net.java.games.input.Controller;

/***
 * Class handling gamepad and generating GamePadPosition command
 * 
 */
public class GamePadThread {
	private static final int SLEEPTIME = 100;
	private static final int DETECTION_POLLING = 1000;

	private double rotationSensibility = 1 * Math.PI / 40;

	/** threshold in absolute value for gamepad detection */
	double threshold = 0.2;

	Thread thread;
	private static Logger logger = LoggerFactory.getLogger(GamePadThread.class);

	private final FlyingModel flyingModel;

	public GamePadThread(FlyingModel flyingModel) {
		this.flyingModel = flyingModel;
	}

	public void start() {
		Runnable runnable = new Runnable() {
			JInputJoystick joystick = null;

			@Override
			public void run() {
				try {
					while (true) {
						/** first try to detect a joystick */
						while (joystick == null) {
							// try to connect
							try {
								if (Activator.getPluginParameters().getEnableGamePadDetection().getValue() == true) {
									joystick = new JInputJoystick(Controller.Type.STICK, Controller.Type.GAMEPAD);
									if (!joystick.isControllerConnected()) {
										joystick.dispose();
										joystick = null;
										Thread.sleep(DETECTION_POLLING);
									} else {
										logger.info(joystick.getControllerName() + " connected");
									}
								} else {
									Thread.sleep(DETECTION_POLLING);

								}

							} catch (ReflectiveOperationException e) {
								joystick = null;
								Thread.sleep(DETECTION_POLLING);
							}
						}

						if (joystick.isControllerConnected()) {
							publishGamePadEvent(joystick);
						} else {

							logger.info(joystick.getControllerName() + " deconnected");
							joystick = null;

						}
						Thread.sleep(SLEEPTIME);

					}
				} catch (InterruptedException e) {
				}

			}
		};
		thread = new Thread(runnable);
		thread.start();
	}

	public void publishGamePadEvent(JInputJoystick joystick) {

		// compute gamepad positions
		GamePadPositionCommand command = new GamePadPositionCommand();
		joystick.pollController();

		double heading = joystick.getXRotationValue();
		double pitch = joystick.getYRotationValue();
		// z axis is null somehow
		// double roll=joystick.getZRotationValue();

		if (Math.abs(heading) > threshold)
			command.setHeading(heading * rotationSensibility);
		// if(Math.abs(roll)>threshold)
		// command.setRoll(roll*rotationSensibility);
		if (Math.abs(pitch) > threshold)
			command.setPitch(pitch * rotationSensibility);

		double x = joystick.getXAxisValue();
		double y = joystick.getYAxisValue();
		double z = joystick.getZAxisValue();
		if (Math.abs(x) > threshold)
			command.setX(x);
		if (Math.abs(z) > threshold)
			command.setZ(z);
		if (Math.abs(y) > threshold)
			command.setY(y);

		if (joystick.getButtonValue(joystick.getLBButtonIndex())) // lb button
			command.setButtonPanDec(true);
		if (joystick.getButtonValue(joystick.getRBButtonIndex())) // lr button
			command.setButtonPanInc(true);

		double lx = joystick.getX_LeftJoystick_Value();
		double ly = joystick.getY_LeftJoystick_Value();

		double rx = joystick.getX_RightJoystick_Value();
		double ry = joystick.getY_RightJoystick_Value();

		if (Math.abs(lx) > threshold)
			command.setLx(lx);
		if (Math.abs(ly) > threshold)
			command.setLy(ly);
		if (Math.abs(rx) > threshold)
			command.setRx(rx);
		if (Math.abs(ry) > threshold)
			command.setRy(ry);

		if (command.valid()) {
			flyingModel.updateViewPosition(new GamePadEvent(command));
		}
	}

	public void stop() {
		thread.interrupt();
	}
}
