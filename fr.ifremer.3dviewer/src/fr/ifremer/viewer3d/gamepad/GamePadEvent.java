package fr.ifremer.viewer3d.gamepad;

import fr.ifremer.globe.ui.events.BaseEvent;

public class GamePadEvent implements BaseEvent {
	private GamePadPositionCommand command;

	public GamePadPositionCommand getCommand() {
		return command;
	}

	GamePadEvent(GamePadPositionCommand command)
	{
		this.command=command;
	}
}
