/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.application.context;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.osgi.service.event.Event;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.model.wc.filters.BottomFilterParameter.ToleranceType;
import fr.ifremer.globe.utils.bean.EnumTypeConverter;
import fr.ifremer.viewer3d.gui.wcmarker.PoiModel;
import fr.ifremer.viewer3d.layers.globalcursor.GlobalCursorLayer;
import fr.ifremer.viewer3d.layers.sync.annotation.WWAnnotationLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.current.WWCurrentLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.dtm.WWTerrainLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.dtm.WWTerrainLayerWithPaletteSynchronizer;
import fr.ifremer.viewer3d.layers.sync.marker.WWMarkerLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.navigation.WWNavigationLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.player.IndexedPlayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.pointcloud.WWPointCloudLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.polygon.WWPolygonLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWComputableTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWMultiTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWPlayableMultiTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWPlayableTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.wc.WWWaterColumnVolumicSynchronizer;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters.Rendering;

/**
 * Application Addon used to initialize the IEclipseContext
 */
public class ContextInitializer {

	/** Static reference of Eclipse context initialized by E4Application */
	protected static IEclipseContext eclipseContext;

	/**
	 * Invoked when Globe application is started.<br>
	 * Fill the context with needed components
	 *
	 * @param injectedEclipseContext Eclipse context initialized by E4Application
	 */
	@Inject
	@Optional
	public void activate(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext injectedEclipseContext) {
		activate(injectedEclipseContext);
	}

	/**
	 * Fill the context with needed components and initialize the context
	 */
	public static void activate(IEclipseContext eclipseContext) {
		setEclipseContext(eclipseContext);
		init();
	}

	/**
	 * Init the singleton instance, create by DI, it registers reference instances of objects used througout the bundle
	 */
	protected static void init() {
		configureSessionService();

		getEclipseContext().set(PoiModel.class, make(PoiModel.class));
		// Creates all synchronizers of layers
		getEclipseContext().set(WWTerrainLayerSynchronizer.class, make(WWTerrainLayerSynchronizer.class));
		getEclipseContext().set(WWTerrainLayerWithPaletteSynchronizer.class,
				make(WWTerrainLayerWithPaletteSynchronizer.class));
		getEclipseContext().set(WWPointCloudLayerSynchronizer.class, make(WWPointCloudLayerSynchronizer.class));
		getEclipseContext().set(WWWaterColumnVolumicSynchronizer.class, make(WWWaterColumnVolumicSynchronizer.class));
		getEclipseContext().set(WWComputableTextureSynchronizer.class, make(WWComputableTextureSynchronizer.class));
		getEclipseContext().set(WWMultiTextureSynchronizer.class, make(WWMultiTextureSynchronizer.class));
		getEclipseContext().set(WWPlayableMultiTextureSynchronizer.class,
				make(WWPlayableMultiTextureSynchronizer.class));
		getEclipseContext().set(WWPlayableTextureSynchronizer.class, make(WWPlayableTextureSynchronizer.class));
		getEclipseContext().set(WWTextureSynchronizer.class, make(WWTextureSynchronizer.class));
		getEclipseContext().set(WWNavigationLayerSynchronizer.class, make(WWNavigationLayerSynchronizer.class));
		getEclipseContext().set(IndexedPlayerSynchronizer.class, make(IndexedPlayerSynchronizer.class));
		getEclipseContext().set(WWMarkerLayerSynchronizer.class, make(WWMarkerLayerSynchronizer.class));
		getEclipseContext().set(WWPolygonLayerSynchronizer.class, make(WWPolygonLayerSynchronizer.class));
		getEclipseContext().set(WWCurrentLayerSynchronizer.class, make(WWCurrentLayerSynchronizer.class));
		getEclipseContext().set(WWAnnotationLayerSynchronizer.class, make(WWAnnotationLayerSynchronizer.class));

		getEclipseContext().set(GlobalCursorLayer.class, make(GlobalCursorLayer.class));

		// load session (done here after context initialization)
		getInContext(ISessionService.class).loadSession();
	}

	/** Add some converters to ISessionService */
	protected static void configureSessionService() {
		ISessionService sessionService = getInContext(ISessionService.class);
		sessionService.getPropertiesContainer().addConverter(new EnumTypeConverter(Rendering.Point), Rendering.class);
		sessionService.getPropertiesContainer().addConverter(new EnumTypeConverter(ToleranceType.SAMPLE),
				ToleranceType.class);
	}

	/**
	 * Getter of {@link #eclipseContext}
	 */
	public static IEclipseContext getEclipseContext() {
		return eclipseContext;
	}

	/**
	 * Setter of {@link #eclipseContext}
	 */
	public static void setEclipseContext(IEclipseContext newEclipseContext) {
		eclipseContext = newEclipseContext;
	}

	/**
	 * Returns the context value associated with the given name
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInContext(String name) {
		return (T) getEclipseContext().get(name);
	}

	public static <T> T getInContext(Class<T> clazz) {
		return getEclipseContext().get(clazz);
	}

	/**
	 * Sets a value to be associated with a given name in this context
	 */
	public static void setInContext(String name, Object value) {
		getEclipseContext().set(name, value);
	}

	/**
	 * Removes the given name and any corresponding value from this context
	 */
	public static void removeFromContext(String name) {
		getEclipseContext().remove(name);
	}

	/**
	 * @return an instance of the specified class and inject it with the context.
	 */
	public static <T> T make(Class<T> clazz) {
		return ContextInjectionFactory.make(clazz, eclipseContext);
	}

	/**
	 * Injects a context into a domain object.
	 */
	public static void inject(Object object) {
		ContextInjectionFactory.inject(object, getEclipseContext());
	}

	/**
	 * Un-injects the context from the object. Usefull to remove listener on EventBroker
	 */
	public static void uninject(Object object) {
		ContextInjectionFactory.uninject(object, getEclipseContext());
	}
}
