package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.IDataDriver;
import fr.ifremer.viewer3d.layers.xml.AbstractMultiTextureTiledImageLayer;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import fr.ifremer.viewer3d.multidata.SubLayerParameters;
import fr.ifremer.viewer3d.multidata.TiledMultiElevationProducer;
import fr.ifremer.viewer3d.multidata.TiledMultiTextureProducer;
import fr.ifremer.viewer3d.terrain.MovableInterpolateElevationModel;
import fr.ifremer.viewer3d.terrain.MultiElevationModel;
import fr.ifremer.viewer3d.terrain.SubElevation;
import fr.ifremer.viewer3d.util.XSDValidator;
import fr.ifremer.viewer3d.util.ZipUtils;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWXML;

/**
 * MultiTexture files (.ifr or .xml) loading </br>
 * MultiTexture cache generation through {@link TiledMultiTextureProducer} and {@link TiledMultiElevationProducer} </br>
 * {@link MultiTextureLayer} and {@link MultiElevationModel} generation</br>
 * </br>
 * Warning : {@link MultiTextureLayer}'s {@link SubLayer} must be synchronize</br>
 * ie : DTM gridding algorithms must be the same for all SubLayers
 *
 * @author mmorvan
 */

@Component(name = "globe_viewer3d_multi_texture_loader", service = { IIcon16ForFileProvider.class, IDataDriver.class,
		IFileInfoSupplier.class })
public class MultiTextureLoader implements ILoader, //
		IDataDriver, // BRIDGE !
		IIcon16ForFileProvider, IFileInfoSupplier<BridgeFileInfo> {

	public static final String EXTENSION_XML = ".xml"; //$NON-NLS-1$
	public static final String EXTENSION_IFR = ".ifr"; //$NON-NLS-1$

	/** XML schema (XSD) file name for this loader input data. */
	public static final String SCHEMA_FILENAME = "/multitexture-1.0.xsd";

	private String type;
	private String groupID;
	private String unit;
	private double valMin;
	private double valMax;

	private String permanentDirectory;
	private String subLayerDirectory;

	private MultiElevationModel elevationModel = null;
	private MultiTextureLayer layer = null;

	private File fileStoreLocation = null;

	protected double resolution;
	private boolean recomputeCache = true;
	private File sourceFile;

	// sector of data
	private Sector sector = null;

	// zero tile delta
	// must be equals for all sublayers
	private LatLon zeroTileDelta = null;

	/** Logger for this class. */
	private static Logger logger = LoggerFactory.getLogger(ElevationLoader.class);

	private static List<Pair<String, String>> extensions = new ArrayList<>();
	static {
		extensions.add(new Pair<>("MultiTexture", "*" + EXTENSION_XML));
		extensions.add(new Pair<>("MultiTexture", "*" + EXTENSION_IFR));
	}

	@Override
	public FileNameExtensionFilter getFileFilter() {
		return new FileNameExtensionFilter("MultiTexture", EXTENSION_XML, EXTENSION_IFR);
	}

	@Override
	public boolean accept(String resource) {
		// TODO estimate size of each data to compare with the cache memory (see
		// ElevationLoader)

		File file = new File(resource);
		if (file != null && file.exists()) {
			String name = file.getName();
			// case zipped data
			if (name != null && FileUtils.getFileExtension(file) != null
					&& FileUtils.getFileExtension(file).equals("ifr")) {
				// unzip first
				File dataFile = file.getParentFile(); // data
				try {
					ZipUtils.unzip(file, dataFile.getPath());
				} catch (IOException e1) {
					logger.warn("Error unzipping ifr file ", e1);
				} catch (Exception e) {
					logger.warn("Error unzipping ifr file ", e);
				}

				// file should be the directory of uncompressed data
				name = file.getAbsolutePath();
				name = name.substring(0, name.length() - 3) + "xml";
			}
			if (name != null && name.contains("xml")) {
				try {
					String path = name;
					File toRead = file;
					if (file.getName().endsWith("ifr")) {
						toRead = new File(path);
					}
					FileInputStream fis = new FileInputStream(toRead);
					InputStream schema = getClass().getResourceAsStream(SCHEMA_FILENAME);
					try {
						return XSDValidator.valid(fis, schema);
					} catch (Exception e) {
						// elevation_v2 could be not valid
						try {
							fis.close();
						} catch (IOException e1) {
							logger.warn("Error unzipping ifr file ", e);
						}
						return false;
					}
				} catch (FileNotFoundException e) {
					return false;
				}
			}
		}
		return false;
	}

	@Override
	public synchronized List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor monitor) throws Exception {
		elevationModel = null;
		layer = null;
		fileStoreLocation = null;

		zeroTileDelta = null;

		// sector of data
		sector = null;

		// cache path
		sourceFile = file;
		File dir = null;
		if (sourceFile.getName().endsWith(EXTENSION_IFR)) {
			String path = file.getAbsolutePath();
			file = new File(path.substring(0, path.length() - 3) + "xml");
			path = path.replace("\\", "/");
			dir = new File(path.substring(0, path.length() - 4));
		}
		fileStoreLocation = WorldWindUtils.getCacheLocation();

		List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(1);

		// Create MultiData Layer through XML HEADER
		// parameters of the layer
		// load subLayers
		XMLReader(file.getPath());

		// add MultiDataLayer to layer store
		layers.add(new Pair<Layer, ElevationModel>(layer, elevationModel));

		// delete decompressed directory if needed
		if (dir != null) {
			file.delete();
			org.apache.commons.io.FileUtils.deleteDirectory(dir);
		}
		monitor.done();
		return layers;
	}

	/***
	 * Read the parameters of the Layer Load SubLayers
	 *
	 * @param filepath
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @return List of SubLayers
	 */
	private void XMLReader(String filepath) throws ParserConfigurationException, SAXException, GIOException {

		File file = new File(filepath);
		String dirPath = FileUtils.getParent(file).getAbsolutePath();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc;
		try {
			doc = db.parse(file);
		} catch (IOException e1) {
			logger.error("Parse error ", e1);
			throw new GIOException(" parse error ", e1);
		}
		doc.getDocumentElement().normalize();

		NodeList parse_name = doc.getElementsByTagName("LayerName"); //$NON-NLS-1$
		NodeList parse_directory = doc.getElementsByTagName("PermanentDirectory"); //$NON-NLS-1$
		NodeList parse_resolution = doc.getElementsByTagName("Latitude"); //$NON-NLS-1$
		NodeList parse_resolution_exception = doc.getElementsByTagName("Resolution"); //$NON-NLS-1$
		NodeList parse_dateMin = doc.getElementsByTagName("DateMin"); //$NON-NLS-1$
		NodeList parse_dateMax = doc.getElementsByTagName("DateMax"); //$NON-NLS-1$
		NodeList parse_dateStep = doc.getElementsByTagName("DateStep"); //$NON-NLS-1$

		String name = ((Element) parse_name.item(0)).getChildNodes().item(0).getNodeValue();

		int dateMin = Integer.valueOf(((Element) parse_dateMin.item(0)).getChildNodes().item(0).getNodeValue());

		int dateMax = Integer.valueOf(((Element) parse_dateMax.item(0)).getChildNodes().item(0).getNodeValue());

		int dateStep = Integer.valueOf(((Element) parse_dateStep.item(0)).getChildNodes().item(0).getNodeValue());

		layer = new MultiTextureLayer(name, dateMin, dateMax, dateStep);
		// create associated elevation model
		// elevationModel = new MultiElevationModel(layer);

		permanentDirectory = ((Element) parse_directory.item(0)).getChildNodes().item(0).getNodeValue();

		try {
			// the resolution is defined by the dLat (longitude depends upon
			// latitude)
			layer.setResolution(
					Double.valueOf(((Element) parse_resolution.item(0)).getChildNodes().item(0).getNodeValue()));
		} catch (Exception e) {
			try {
				layer.setResolution(Double.valueOf(
						((Element) parse_resolution_exception.item(0)).getChildNodes().item(0).getNodeValue()));
			} catch (Exception e2) {
				layer.setResolution(100);// par defaut on met une resolution
				// faible
			}
		}

		try {
			double minLatitude = Double.valueOf(
					((Element) doc.getElementsByTagName("South").item(0)).getChildNodes().item(0).getNodeValue());
			double maxLatitude = Double.valueOf(
					((Element) doc.getElementsByTagName("North").item(0)).getChildNodes().item(0).getNodeValue());
			double maxLongitude = Double.valueOf(
					((Element) doc.getElementsByTagName("East").item(0)).getChildNodes().item(0).getNodeValue());
			double minLongitude = Double.valueOf(
					((Element) doc.getElementsByTagName("West").item(0)).getChildNodes().item(0).getNodeValue());

			sector = Sector.fromDegrees(minLatitude, maxLatitude, minLongitude, maxLongitude);

		} catch (Exception e) {

		}

		NodeList subLayers = doc.getElementsByTagName("SubLayer");
		if (subLayers == null) {
			logger.error("Could not read MultiData file");
		}

		// for each SubLayer
		for (int index = 0; index < subLayers.getLength(); index++) {
			Node node = subLayers.item(index);

			// create the SubLayer
			XMLSubLayer(node, layer, dirPath);
		}
	}

	/***
	 * Create and read the SubLayer parameters
	 *
	 * @param node
	 * @param layer
	 * @param path
	 * @return the list of textures indexed by date
	 */
	private void XMLSubLayer(Node node, MultiTextureLayer layer, String path) {
		if (node == null || layer == null) {
			logger.error("Could not read SubLayer's MultiData file");
		}

		// sub layer parameters
		String subLayerName = ((Element) node).getElementsByTagName("SubLayerName").item(0).getChildNodes().item(0)
				.getNodeValue();
		type = ((Element) node).getElementsByTagName("Type").item(0).getChildNodes().item(0).getNodeValue();
		groupID = ((Element) node).getElementsByTagName("GroupID").item(0).getChildNodes().item(0).getNodeValue();
		unit = ((Element) node).getElementsByTagName("Unit").item(0).getChildNodes().item(0).getNodeValue();
		valMin = Double
				.valueOf(((Element) node).getElementsByTagName(AbstractMultiTextureTiledImageLayer.XML_KEY_MinValue)
						.item(0).getChildNodes().item(0).getNodeValue());
		valMax = Double
				.valueOf(((Element) node).getElementsByTagName(AbstractMultiTextureTiledImageLayer.XML_KEY_MaxValue)
						.item(0).getChildNodes().item(0).getNodeValue());
		subLayerDirectory = ((Element) node).getElementsByTagName("SubLayerDirectory").item(0).getChildNodes().item(0)
				.getNodeValue();

		// RGB data => shader color map disabled
		boolean isRGB = Boolean
				.valueOf(((Element) node).getElementsByTagName("IsRGB").item(0).getChildNodes().item(0).getNodeValue());

		// time indexed?
		// list of date indexed textures
		NodeList subLayerTextures = ((Element) node).getElementsByTagName("DateIndex");
		boolean isTimeIndexed = true;
		if (subLayerTextures == null || subLayerTextures.getLength() == 0 || subLayerTextures.item(0) == null
				|| !subLayerTextures.item(0).hasChildNodes()) {
			isTimeIndexed = false;
		}

		// create sub layer
		SubLayerParameters parameters = layer.getLayerParameters().getSubLayerParameters(type);

		SubLayer subLayer = new SubLayer(subLayerName, subLayerDirectory, type, groupID, unit, valMin, valMax, isRGB,
				isTimeIndexed, parameters);
		// add sub layer to layer
		layer.addSubLayer(subLayer);

		SubElevation subElevation = null;
		// if bathymetry => elevation
		if (subLayer.getType().equals("Bathymetry")) {
			subElevation = new SubElevation(subLayer.getName());
			if (elevationModel == null) {
				elevationModel = new MultiElevationModel(layer, subElevation);
				elevationModel.setCurrentSubElevation(subElevation);
			} else {
				elevationModel.addSubElevation(subElevation);
			}

			// // int elevationIndex =
			// elevationModel.getSubElevationList().indexOf(layer.getCurrentSubLayer());
			// int elevationIndex =
			// elevationModel.getSubElevationList().get(layer.getSubLayers().indexOf(layer.getCurrentSubLayer())));
			//
			//
			// if (elevationIndex != -1) {
			// elevationModel.setCurrentSubElevation(elevationModel.getSubElevationList().get(elevationIndex));
			// }
		}

		// if null texture is not date indexed
		// load it directly
		if (!isTimeIndexed) {
			XMLSubLayerTexture(node, subLayer, subElevation, path, false);
		} else {
			// for each date indexed texture
			for (int index = 0; index < subLayerTextures.getLength(); index++) {
				Node textureNode = subLayerTextures.item(index);

				// create the SubLayer
				XMLSubLayerTexture(textureNode, subLayer, subElevation, path, true);
			}
		}
	}

	/***
	 * load date indexed texture of the subLayer
	 *
	 * @param node
	 * @param subLayer
	 * @param path
	 * @param timeIndexed
	 */
	private void XMLSubLayerTexture(Node node, SubLayer subLayer, SubElevation subElevation, String path,
			boolean timeIndexed) {
		if (node == null || subLayer == null) {
			logger.error("Could not read SubLayer's texture MultiData file");
		}

		// read date
		int length = 1;
		if (timeIndexed) {
			length = ((Element) node).getElementsByTagName("Date").getLength();
		}

		for (int index = 0; index < length; index++) {
			// -1 is for non time indexed
			int dateIndex = -1;
			if (timeIndexed) {
				dateIndex = Integer.valueOf(((Element) node).getElementsByTagName("Date").item(index).getChildNodes()
						.item(0).getNodeValue());
			}

			// path of texture directory
			String dirPath = path + "/" + permanentDirectory + "/" + subLayerDirectory;
			if (timeIndexed) {
				dirPath += "/" + dateIndex;
			}

			// name of the pyramid
			String name = subLayer.getName() + "_" + subLayer.getType();
			if (timeIndexed) {
				name += "_" + dateIndex;
			}

			// path of the cache
			String cacheName = NasaCache.getCacheSubDirectory() + "/" + permanentDirectory + "/" + subLayerDirectory;
			if (timeIndexed) {
				cacheName += "/" + dateIndex + "/";
			}

			// if bathymetry => load elevation
			if (subElevation != null) {
				TiledMultiElevationProducer producer = new TiledMultiElevationProducer(recomputeCache);

				importMultiElevation(name, cacheName, dirPath, dateIndex, fileStoreLocation, producer, subElevation);
				producer = null;
			}

			// always load texture
			TiledMultiTextureProducer textureProducer = new TiledMultiTextureProducer(unit, valMin, valMax,
					recomputeCache);

			importMultiTexture(name, cacheName, dirPath, dateIndex, fileStoreLocation, textureProducer, subLayer);
			textureProducer = null;
		}
	}

	protected void importMultiElevation(String displayName, String cacheName, String dataPath, int dateIndex,
			File fileStoreLocation, TiledMultiElevationProducer producer, SubElevation subElevation) {

		// Create a parameter list which defines where the elevation data is
		// imported, the name associated with it,
		// and default extreme elevations for the ElevationModel using the
		// minimum and maximum elevations on
		// Earth.
		AVList params = new AVListImpl();
		params.setValue(AVKey.FILE_STORE_LOCATION, fileStoreLocation.getAbsolutePath());
		params.setValue(AVKey.DATA_CACHE_NAME, cacheName);
		params.setValue(AVKey.DATASET_NAME, displayName);
		params.setValue(AVKey.NAME, displayName);
		params.setValue(AVKey.SECTOR, sector);
		// same zero tule delta for each sublayer
		if (zeroTileDelta != null) {
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, zeroTileDelta);
		}

		// Create a TiledElevationProducer to transforms the source elevation
		// data to a pyramid of elevation tiles in
		// the World Wind Java cache format.

		try {
			// Configure the TiledElevationProducer with the parameter list and
			// the elevation data source.
			producer.setStoreParameters(params);
			producer.offerDataSource(dataPath, null);
			// Import the source elevation data into the FileStore by converting
			// it to the World Wind Java cache format.
			producer.startProduction();

			// retrieve zero tile delta
			if (zeroTileDelta == null) {
				LatLon retrieve = (LatLon) producer.getProductionParameters().getValue(AVKey.LEVEL_ZERO_TILE_DELTA);
				if (retrieve != null) {
					zeroTileDelta = retrieve;
				}
			}

			// clean level 1 elevation cache (temp files)
			TemporaryCache.deleteAllFiles("elevationFile", ".txt");

			// layer.setLevels(producer.getLevels());
		} catch (Exception e) {
			producer.removeProductionState();
			logger.error("An error occured during tiles production", e);
			return;
		}

		// Extract the data configuration document from the production results.
		// If production sucessfully completed, the
		// TiledElevationProducer should always contain a document in the
		// production results, but we test the results
		// anyway.
		Iterable<?> results = producer.getProductionResults();
		if (results == null || results.iterator() == null || !results.iterator().hasNext()) {
			return;
		}

		Object o = results.iterator().next();
		if (o == null || !(o instanceof Document)) {
			return;
		}

		// levelSet = new LevelSet(producer.getStoreParameters());
		// Construct an ElevationModel by passing the data configuration
		// document to an ElevationModelFactory.

		// Overrides default factory behavior in order easily change the
		// elevation model
		subElevation.add(dateIndex, new MovableInterpolateElevationModel((Document) o, null));
	}

	private void importMultiTexture(String name, String cacheName, String dirPath, int dateIndex,
			File fileStoreLocation, TiledMultiTextureProducer producer, SubLayer subLayer) {
		// Create a parameter list that specifies where the image is imported
		// to, and the name associated with it.
		AVList params = new AVListImpl();
		params.setValue(AVKey.FILE_STORE_LOCATION, fileStoreLocation.getAbsolutePath());
		params.setValue(AVKey.DATA_CACHE_NAME, cacheName);
		params.setValue(AVKey.DATASET_NAME, name);
		params.setValue(AVKey.SECTOR, sector);
		// same zero tule delta for each sublayer
		if (zeroTileDelta != null) {
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, zeroTileDelta);
		}

		try {
			// Configure the TiledImageProducer with the parameter list and the
			// image source.
			producer.setStoreParameters(params);
			producer.offerDataSource(dirPath, null);
			// Import the source image into the FileStore by converting it to
			// the World Wind Java cache format.
			producer.startProduction();
		} catch (Exception e) {
			producer.removeProductionState();
			logger.error("Error converting filestore" + " to World Wind cache format", e);
			return;
		}

		// retrieve zero tile delta
		if (zeroTileDelta == null) {
			LatLon retrieve = (LatLon) producer.getProductionParameters().getValue(AVKey.LEVEL_ZERO_TILE_DELTA);
			if (retrieve != null) {
				zeroTileDelta = retrieve;
			}
		}

		// Extract the data configuration document from the production results.
		// If production sucessfully completed, the
		// TiledImageProducer should always contain a document in the production
		// results, but we test the results
		// anyway.
		Iterable<?> results = producer.getProductionResults();
		if (results == null || results.iterator() == null || !results.iterator().hasNext()) {
			return;
		}

		Object o = results.iterator().next();
		if (o == null || !(o instanceof Document)) {
			return;
		}

		// add texture to subLayer
		subLayer.addTexture(dateIndex, cacheName);

		// add levelSet to layer
		AVList metadata = getParamsFromDocument(((Document) o).getDocumentElement(), null);
		// display first subLayer first
		if (layer.getLevels() == null) {
			layer.setLevelSet(new LevelSet(metadata));
		}

		// clean level 1 image cache (temp files)
		try {
			TemporaryCache.deleteAllFiles("bufferedImage", ".png");
		} catch (GIOException e1) {
			logger.error("temp image file not destroyed", e1);
		}
	}

	protected static AVList getParamsFromDocument(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		getTiledImageLayerConfigParams(domElement, params);
		// setFallbacks(params);

		return params;
	}

	public static AVList getTiledImageLayerConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		XPath xpath = WWXML.makeXPath();

		// Common layer properties.
		AbstractLayer.getLayerConfigParams(domElement, params);

		// LevelSet properties.
		DataConfigurationUtils.getLevelSetConfigParams(domElement, params);

		// Service properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.SERVICE_NAME, "Service/@serviceName", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE,
				"RetrievePropertiesFromService", xpath);

		// Image format properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.IMAGE_FORMAT, "ImageFormat", xpath);
		WWXML.checkAndSetStringParam(domElement, params, AVKey.TEXTURE_FORMAT, "TextureFormat", xpath);
		WWXML.checkAndSetUniqueStringsParam(domElement, params, AVKey.AVAILABLE_IMAGE_FORMATS,
				"AvailableImageFormats/ImageFormat", xpath);

		// Optional behavior properties.
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.FORCE_LEVEL_ZERO_LOADS, "ForceLevelZeroLoads", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETAIN_LEVEL_ZERO_TILES, "RetainLevelZeroTiles", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_MIP_MAPS, "UseMipMaps", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_TRANSPARENT_TEXTURES, "UseTransparentTextures",
				xpath);
		WWXML.checkAndSetDoubleParam(domElement, params, AVKey.DETAIL_HINT, "DetailHint", xpath);
		WWXML.checkAndSetColorArrayParam(domElement, params, AVKey.TRANSPARENCY_COLORS, "TransparencyColors/Color",
				xpath);

		// Retrieval properties. Convert the Long time values to Integers,
		// because BasicTiledImageLayer is expecting
		// Integer values.
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_CONNECT_TIMEOUT,
				"RetrievalTimeouts/ConnectTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_READ_TIMEOUT,
				"RetrievalTimeouts/ReadTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT,
				"RetrievalTimeouts/StaleRequestLimit/Time", xpath);

		return params;
	}

	public int getSizeListLayers() {
		return 1;
	}

	@Override
	public List<Pair<String, String>> extensions() {
		return extensions;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param resource if this resource refers to a known {@link Layer} or {@link ElevationModel} instance it look for
	 *            the matching icon, otherwise it returns default elevation icon.
	 */
	@Override
	public String getImage(Object resource) {
		if (resource instanceof ElevationModel) {
			return "icons/16/elevation.png";
		} else if (resource instanceof Layer) {
			return "icons/16/texture.png";
		}
		return "icons/16/dtm16.png";
	}

	@Override
	public List<Property<?>> getProperties(String f) throws GIOException {
		List<Property<?>> properties = new ArrayList<>();
		properties.addAll(Property.build(//
				new GeoBox(sector.getMaxLatitude().degrees, sector.getMinLatitude().degrees,
						sector.getMaxLongitude().degrees, sector.getMinLongitude().degrees)));
		properties.add(Property.build("Type", type));
		properties.add(Property.build("Maximum value", valMax));
		properties.add(Property.build("Minimum value", valMin));
		return properties;
	}

	@Override
	public int getPriority() {
		return DEFAULT_PRIORITY;
	}

	/* @formatter:off
	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	@formatter:on */
	@Override
	public ContentType getContentType(String resource) {
		return accept(resource) ? ContentType.OLD_MULTITEXTURE_XML : ContentType.UNDEFINED;
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.OLD_MULTITEXTURE_XML);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(getImage(null), getClass()));
	}

	@Override
	public List<String> getExtensions() {
		return Arrays.asList("xml", "ifr");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return extensions();
	}

	@Override
	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		return Optional
				.ofNullable(accept(filePath) ? new BridgeFileInfo(filePath, getContentType(filePath), this) : null);
	}

}
