package fr.ifremer.viewer3d.loaders;

import java.util.HashMap;

import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import fr.ifremer.viewer3d.layers.xml.MultiTextureTile;
import fr.ifremer.viewer3d.multidata.SubLayerParameters;

/***
 * Defines a list of terrain texture indexed by date<br/>
 * Containing informations needed for displaying (data type, unit, min, max)<br/>
 * Used in {@link MultiTextureLayer} and {@link MultiTextureTile}<br/>
 * to defined the texture<br/>
 * 
 * @author MORVAN
 * 
 */
public class SubLayer {

	/***
	 * name of the subLayer as displayed in the IHM
	 */
	private String name;

	/***
	 * cache (pyramid) name of the subLayer
	 */
	private String cacheName;

	/***
	 * type of data (bathymetry, reflectivity, ...)
	 */
	private String type;

	/***
	 * Association of SubLayer between which Operation are performed
	 */
	private String groupID;

	/***
	 * unit of data (m, dB, ...)
	 */
	private String unit;

	/***
	 * value minimum for this texture (in unit)
	 */
	private double valMin;

	/***
	 * value maximum for this texture (in unit)
	 */
	private double valMax;

	protected boolean _isRGB = false;

	private boolean isTimeIndexed = false;

	/***
	 * SubLayer parameters common to MultiTextureLayer's SubLayers with same
	 * type
	 */
	private SubLayerParameters parameters = null;

	/***
	 * Defines a list of terrain texture indexed by date Containing informations
	 * needed for displaying (data type, unit, min, max) and parameters of
	 * display (contrast, shading)
	 * 
	 * @param subLayerName
	 * @param cacheName
	 * @param type
	 * @param unit
	 * @param valMin
	 * @param valMax
	 * @param isRGB
	 * @param parameters
	 */
	public SubLayer(String subLayerName, String cacheName, String type, String groupID, String unit, double valMin, double valMax, boolean isRGB, boolean isTimeIndexed, SubLayerParameters parameters) {
		this.name = subLayerName;
		this.cacheName = cacheName;
		this.groupID = groupID;
		this.type = type;
		this.unit = unit;
		this.valMin = valMin;
		this.valMax = valMax;
		this._isRGB = isRGB;
		this.isTimeIndexed = isTimeIndexed;

		this.parameters = parameters;

		parameters.setMinContrast(valMin);
		parameters.setMaxContrast(valMax);
		parameters.setMinTransparency(valMin);
		parameters.setMaxTransparency(valMax);
	}

	/***
	 * texture indexed by date index (date set by Parent
	 * {@link MultiTextureLayer} )
	 */
	private HashMap<Integer, String> textures = new HashMap<Integer, String>();

	public String getTexture(int dateIndex) {
		if (isTimeIndexed) {
			return textures.get(dateIndex);
		} else {
			return textures.get(0);
		}
	}

	public void setTextures(HashMap<Integer, String> textures) {
		this.textures = textures;
	}

	public void addTexture(int dateIndex, String texturePath) {
		if (isTimeIndexed) {
			this.textures.put(dateIndex, texturePath);
		} else {
			this.textures.put(0, texturePath);
		}
	}

	/***
	 * name of the subLayer as displayed in the IHM name (type)
	 */
	public String getName() {
		return name + " (" + type + ")";
	}

	/***
	 * type of data (bathymetry, reflectivity, ...)
	 */
	public String getCacheName() {
		return cacheName;
	}

	/***
	 * Association of SubLayer between which Operation are performed
	 * 
	 * @return groupID
	 */
	public String getGroupID() {
		return groupID;
	}

	/***
	 * value minimum of this texture (in unit)
	 * 
	 * @param valMin
	 */
	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	/***
	 * value minimum of this texture (in unit)
	 * 
	 * @return valMin
	 */
	public double getValMin() {
		return valMin;
	}

	/***
	 * value maximum of this texture (in unit)
	 * 
	 * @param valMax
	 */
	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	/***
	 * value maximum of this texture (in unit)
	 * 
	 * @return valMax
	 */
	public double getValMax() {
		return valMax;
	}

	/***
	 * Useful for IHM purpose
	 * 
	 * @param texture
	 *            type (reflectivity, bathymetry, ..)
	 */
	public void setType(String type) {
		this.type = type;
	}

	/***
	 * Useful for IHM purpose
	 * 
	 * @return texture type (reflectivity, bathymetry, ..)
	 */
	public String getType() {
		return type;
	}

	/***
	 * Useful for IHM purpose
	 * 
	 * @param texture
	 *            unit (m, dB, ..)
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/***
	 * Useful for IHM purpose
	 * 
	 * @return texture unit (m, dB, ..)
	 */
	public String getUnit() {
		return unit;
	}

	public void setIsRGB(boolean _isRGB) {
		this._isRGB = _isRGB;
	}

	public boolean isRGB() {
		return this._isRGB;
	}

	/***
	 * Parameters common to all SubLayers of this layer with the same type
	 * 
	 * @return SubLayerParameters
	 */
	public SubLayerParameters getParameters() {
		return parameters;
	}

	/***
	 * update parameters common to all SubLayers of this layer with the same
	 * type
	 * 
	 * @param the
	 *            {@link SubLayerParameters}
	 */
	public void setParameters(SubLayerParameters param) {
		parameters = param;
	}

	/***
	 * 
	 * @return true if time indexed
	 */
	public boolean isTimeIndexed() {
		return isTimeIndexed;
	}
}
