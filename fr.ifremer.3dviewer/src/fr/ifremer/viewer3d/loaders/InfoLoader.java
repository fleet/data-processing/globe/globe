/*
 * @License@
 */
package fr.ifremer.viewer3d.loaders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.security.AnyTypePermission;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.IDataDriver;
import fr.ifremer.viewer3d.layers.MyAbstractLayerParameter;
import fr.ifremer.viewer3d.layers.ReflectivityLayer;
import fr.ifremer.viewer3d.layers.Seismic3DLayer;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import fr.ifremer.viewer3d.model.DepthWallLine;
import fr.ifremer.viewer3d.model.PathWallLines;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.model.ReflectivityNavData;
import fr.ifremer.viewer3d.model.Seismic3DData;
import fr.ifremer.viewer3d.model.info.Dimensions;
import fr.ifremer.viewer3d.model.info.Images;
import fr.ifremer.viewer3d.model.info.Info;
import fr.ifremer.viewer3d.model.info.Item;
import fr.ifremer.viewer3d.model.info.LabelEnumConverter;
import fr.ifremer.viewer3d.model.info.Signals;
import fr.ifremer.viewer3d.model.info.Strings;
import fr.ifremer.viewer3d.util.ZipUtils;
import fr.ifremer.viewer3d.util.binary.BinaryUtil;
import fr.ifremer.viewer3d.util.binary.BufferIterator;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Loader for SonarScope-3DViewer V2 input files.
 *
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
@Component(name = "globe_view3d_loaders_infoloader", service = { IDataDriver.class, IIcon16ForFileProvider.class,
		IFileInfoSupplier.class })
public class InfoLoader implements ILoader, //
		IDataDriver, // BRIDGE !
		IIcon16ForFileProvider, IFileInfoSupplier<BridgeFileInfo> {

	/**
	 * XML schema (XSD) file name for this loader input data.
	 */
	public static final String SCHEMA_FILENAME = "/info-2.0.xsd";

	/**
	 * Extension for echoes definition files.
	 */
	public static final String EXTENSION = "xml"; //$NON-NLS-1$

	private static final String LAT_TOP = "LatitudeTop"; //$NON-NLS-1$

	private static final String LON_TOP = "LongitudeTop"; //$NON-NLS-1$

	private static final String LAT_BOT = "LatitudeBottom"; //$NON-NLS-1$

	private static final String LON_BOT = "LongitudeBottom"; //$NON-NLS-1$

	private static final String DEPTH_TOP = "DepthTop"; //$NON-NLS-1$

	private static final String DEPTH_BOT = "DepthBottom"; //$NON-NLS-1$

	private static final String INDEX_NAME = "iSlice"; //$NON-NLS-1$

	private static final String HEAVE = "Heave";

	private static final String TIDE = "Tide";

	private static final String SLICE_NAME = "SliceName.txt";

	private static final Object LONGITUDE_NAME = "Longitude";

	private static final Object LATITUDE_NAME = "Latitude";

	private static final Object Z_NAME = "z";

	private static final Object T_NAME = "t";

	/**
	 * Logger for this class.
	 */
	private static Logger logger = LoggerFactory.getLogger(InfoLoader.class);

	/**
	 * Extension for IDataDriver
	 */
	private static List<Pair<String, String>> extensions = new ArrayList<>();
	static {
		extensions.add(new Pair<>("Reflectivity (*.xml)", "*.xml"));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileNameExtensionFilter getFileFilter() {
		return new FileNameExtensionFilter("Reflectivity (.xml)", //$NON-NLS-1$
				EXTENSION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean accept(String resource) {
		File file = new File(resource);
		if (file != null) {
			String name = file.getName();
			if (name != null && name.endsWith(EXTENSION)) {
				// TODO use weak references map for data caching
				try {
					Info obj = readXmlFile(file);
					if (obj != null) {
						return true;
					}
				} catch (CannotResolveClassException e) {
					return false; // not an acceptable file
				} catch (Exception e) {
					logger.error("Exception while reading " + EXTENSION + " file '{}' : {} ", resource, e.getMessage(),
							e);
				}
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor monitor) throws Exception {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		if (file == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_NAVIGATIONLOADER_ERROR_MESSAGE")); //$NON-NLS-1$
		}

		try {
			Info infoXML = readXmlFile(file);
			subMonitor.worked(20);

			if (infoXML != null) {

				// Test présence des données
				File parent = file.getParentFile();
				File item = new File(
						parent.getAbsolutePath() + "/" + infoXML.getSignals().getItems().get(0).getFileName());
				File dataFile = item.getParentFile(); // data
				file = ZipUtils.unzipDataInCache(file, dataFile);

				ReflectivityNavData data = null;
				PlayerBean bean = null;
				List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(4);
				switch (infoXML.getSignature2()) {
				case Seismic3D:
					Seismic3DData sd = new Seismic3DData(file);
					PlayerBean pb = new PlayerBean();
					Seismic3DLayer slayer = new Seismic3DLayer(pb, sd);
					load3DSeismicData(file, slayer, infoXML, subMonitor.split(60));
					layers.add(new Pair<Layer, ElevationModel>(slayer, null));
					break;
				case ImagesAlongNavigation:
				case WCAlongNavigation:
				case SismicAlongNav:
				case SeismicAlongNav:
					bean = new PlayerBean();
					data = new ReflectivityNavData(file);
					addInfos(infoXML, data.getInfos());
					ReflectivityNavData navData = loadNavBinaryFile(file, infoXML, data, subMonitor.split(60));

					ReflectivityLayer layer = new ReflectivityLayer(bean, navData, //
							file.getName(), // avoid unique id to correctly reload session (and 'enable' state)
							// infoXML.getName() + "_" + System.currentTimeMillis(), // +Time to make sure the name is
							// unique
							infoXML.getSignature2(), true /*** do not use mipmap since they interpolate colors */
							, infoXML);
					layer.initialize();
					layer.setValue(MyAbstractLayerParameter.INTERPOLATION,
							MyAbstractLayerParameter.INTERPOLATION_NEAREST);
					layers.add(new Pair<Layer, ElevationModel>(layer, null));
					break;
				}
				return layers;
			}
		} catch (FileNotFoundException e) {
			logger.error("File not found", e);
		} catch (CannotResolveClassException e) {
			logger.error("Cannot parse such file", e);
		}
		monitor.done();
		return null;
	}

	private void addInfos(Info infoXML, AbstractInfos data) {
		data.addInfo("Signature1", infoXML.getSignature1());
		data.addInfo("Signature2", infoXML.getSignature2());
		data.addInfo("FormatVersion", infoXML.getFormatVersion());
		data.addInfo("Name", infoXML.getName());
		data.addInfo("ExtractedFrom", infoXML.getExtractedFrom());
		data.addInfo("Survey", infoXML.getSurvey());
		data.addInfo("Vessel", infoXML.getVessel());
		data.addInfo("Sounder", infoXML.getSounder());
		data.addInfo("ChiefScientist", infoXML.getChiefScientist());
		// navData.addInfo("DepthTop", reflectivityNav.getDepthTop());
		// navData.addInfo("DepthBottom", reflectivityNav.getDepthBottom());
		data.addInfo("Pings", infoXML.getDimensions().getNbPings());
		data.addInfo("Rows", infoXML.getDimensions().getNbRows());
		data.addInfo("Slices", infoXML.getDimensions().getNbSlices());
		data.addInfo("MiddleSlice", infoXML.getMiddleSlice());

	}

	/**
	 * Load data from binary data files.
	 *
	 * @param inputFile the input XML "index" file
	 * @return
	 * @throws IOException if an exception occurs during file access
	 */
	protected ReflectivityNavData loadNavBinaryFile(File inputFile, Info xmlData, ReflectivityNavData data,
			IProgressMonitor monitor) throws IOException {

		// mis en commentaire car non utilisée pour l'instant
		/*
		 * BufferIterator<Double> dateIt = null; BufferIterator<Double> hourIt = null; BufferIterator<Double>
		 * pingNumberIt = null; BufferIterator<Double> latNadirIt = null; BufferIterator<Double> lonNadirIt = null;
		 * BufferIterator<Double> acrossDistanceIt = null;
		 */

		BufferIterator<Double> latTopIt = null;
		BufferIterator<Double> lonTopIt = null;
		BufferIterator<Double> latBotIt = null;
		BufferIterator<Double> lonBotIt = null;
		BufferIterator<Double> depthTopIt = null;
		BufferIterator<Double> depthBotIt = null;
		BufferIterator<Double> heaveIt = null;
		BufferIterator<Double> tideIt = null;
		BufferIterator<Double> indexIt = null;
		BufferedReader sliceNameReader = null;

		// List<FloatBuffer> reflectivityBuffers = null;

		try {
			if (xmlData != null) {

				data.setName(xmlData.getName());
				data.setSoftware(xmlData.getSignature1());

				Signals signals = xmlData.getSignals();
				List<Item> signalItems = signals.getItems();

				Images images = xmlData.getImages();
				List<Item> imagesItems = images.getItems();

				Strings strings = xmlData.getStrings();
				List<Item> stringsItems = null;
				if (strings != null) {
					stringsItems = strings.getItems();
				}

				List<Item> allItems = signalItems;
				allItems.addAll(imagesItems);
				if (stringsItems != null) {
					allItems.addAll(stringsItems);
				}
				Dimensions dim = xmlData.getDimensions();
				int nbPings = dim.getNbPings();
				int nbSlices = dim.getNbSlices();

				if (allItems != null) {
					File parent = inputFile.getParentFile();
					String basePath = parent.getAbsolutePath() + "/"; //$NON-NLS-1$
					SubMonitor subMonitor = SubMonitor.convert(monitor, allItems.size());
					for (Item item : allItems) {
						String filename = item.getFileName().trim();
						// replace backslashes by slashes for portability
						filename = filename.replaceAll("\\\\([^ ])", "/$1");
						File file = new File(basePath + filename);

						data.addBinaryFile(file);
						// add data directory if it doesn't exist yet

						// find parent directory
						int idx = filename.indexOf("/");
						int idx2 = filename.indexOf("\\");
						if (idx == -1 || idx2 != -1 && idx2 < idx) {
							idx = idx2;
						}
						if (idx != -1) {
							File directory = new File(basePath + filename.substring(0, idx));
							if (!data.getBinaryFiles().contains(directory)) {
								data.addBinaryFile(directory);
							}
						}
						// read binary data
						// mis en commentaire car non utilisée pour l'instant
						/*
						 * if (DATE_NAME.equals(item.getName())) { dateIt = BinaryUtil.readBinFile(file,
						 * item.getStorage()); } else if (HOUR_NAME.equals(item.getName())) { hourIt =
						 * BinaryUtil.readBinFile(file, item.getStorage()); } else if
						 * (PING_NUMBER.equals(item.getName())) { pingNumberIt = BinaryUtil.readBinFile(file,
						 * item.getStorage()); } else if (LAT_NADIR.equals(item.getName())) { latNadirIt =
						 * BinaryUtil.readBinFile(file, item.getStorage()); } else if (LON_NADIR.equals(item.getName()))
						 * { lonNadirIt = BinaryUtil.readBinFile(file, item.getStorage()); } else if
						 * (ACROSS_DISTANCE.equals(item.getName())) { acrossDistanceIt = BinaryUtil.readBinFile(file,
						 * item.getStorage()); } else
						 */
						if (LAT_TOP.equals(item.getName())) {
							latTopIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (LON_TOP.equals(item.getName())) {
							lonTopIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (LON_BOT.equals(item.getName())) {
							lonBotIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (LAT_BOT.equals(item.getName())) {
							latBotIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (DEPTH_TOP.equals(item.getName())) {
							depthTopIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (DEPTH_BOT.equals(item.getName())) {
							depthBotIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (HEAVE.equals(item.getName())) {
							heaveIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (TIDE.equals(item.getName())) {
							tideIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (INDEX_NAME.equals(item.getName())) {
							indexIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (item.getFileName().trim().endsWith(SLICE_NAME)) {
							// lecture du fichier texte
							try {
								sliceNameReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
							} catch (Exception e) {
								System.out.println(e.toString());
							}
						}
						subMonitor.worked(1);
					}
				}

				if (latTopIt != null) {

					// ajout sliceName pour tooltip
					if (sliceNameReader != null) {
						data.setSliceNameList(new ArrayList<String>());
						String ligne;
						while ((ligne = sliceNameReader.readLine()) != null) {
							data.addSliceName(ligne);
						}
						sliceNameReader.close();
					}

					// per ping values
					List<Double> depthTopList = null;
					List<Double> depthBotList = null;
					List<Double> heaveList = null;
					List<Double> tideList = null;
					List<Double> latTopList = null;
					List<Double> lonTopList = null;
					List<Double> latBotList = null;
					List<Double> lonBotList = null;
					for (int pingNb = 0; pingNb < nbPings; pingNb++) {
						// if DEPTH_TOP <Dim>1, nbPings</Dim>
						if (depthTopIt != null && depthTopIt.size() == nbPings) { // if
							// <Dim>1,
							// nbPings</Dim>
							if (depthTopList == null) {
								depthTopList = new ArrayList<>(nbPings);
							}
							depthTopList.add(depthTopIt.get());
						}
						// if DEPTH_BOT <Dim>1, nbPings</Dim>
						if (depthBotIt != null && depthBotIt.size() == nbPings) {
							if (depthBotList == null) {
								depthBotList = new ArrayList<>(nbPings);
							}
							depthBotList.add(depthBotIt.get());

						}
						// if HEAVE <Dim>1, nbPings</Dim>
						if (heaveIt != null && heaveIt.size() == nbPings) { // if
							// <Dim>1,
							// nbPings</Dim>
							if (heaveList == null) {
								heaveList = new ArrayList<>(nbPings);
							}
							heaveList.add(heaveIt.get());
						}
						// if TIDE <Dim>1, nbPings</Dim>
						if (tideIt != null && tideIt.size() == nbPings) { // if
							// <Dim>1,
							// nbPings</Dim>
							if (tideList == null) {
								tideList = new ArrayList<>(nbPings);
							}
							tideList.add(tideIt.get());
						}
						// if LAT_TOP <Dim>1, nbPings</Dim>
						if (latTopIt != null && latTopIt.size() == nbPings) { // if
							// <Dim>1,
							// nbPings</Dim>
							if (latTopList == null) {
								latTopList = new ArrayList<>(nbPings);
							}
							latTopList.add(latTopIt.get());
						}
						// if LON_TOP <Dim>1, nbPings</Dim>
						if (lonTopIt != null && lonTopIt.size() == nbPings) { // if
							// <Dim>1,
							// nbPings</Dim>
							if (lonTopList == null) {
								lonTopList = new ArrayList<>(nbPings);
							}
							lonTopList.add(lonTopIt.get());
						}
						// if LAT_BOT <Dim>1, nbPings</Dim>
						if (latBotIt != null && latBotIt.size() == nbPings) { // if
							// <Dim>1,
							// nbPings</Dim>
							if (latBotList == null) {
								latBotList = new ArrayList<>(nbPings);
							}
							latBotList.add(latBotIt.get());
						}
						// if LON_BOT <Dim>1, nbPings</Dim>
						if (lonBotIt != null && lonBotIt.size() == nbPings) {
							if (lonBotList == null) {
								lonBotList = new ArrayList<>(nbPings);
							}
							lonBotList.add(lonBotIt.get());
						}
					}

					// for slice values
					for (int sliceNb = 0; sliceNb < nbSlices; sliceNb++) {
						PathWallLines navLine = null;
						if (depthBotList != null) { // if DEPTH_BOT <Dim>1,
							// nbPings</Dim>
							if (latBotIt != null && lonBotIt != null) {
								navLine = new PathWallLines();
							} else {
								navLine = new DepthWallLine();
							}
						} else if (depthBotIt != null) { // if DEPTH_BOT
							// <Dim>nbSlices,
							// nbPings</Dim>
							if (latBotIt != null && lonBotIt != null) {
								navLine = new PathWallLines();
							} else { // if not DEPTH_BOT
								navLine = new DepthWallLine();
							}
						} else { // if not DEPTH_BOT
							navLine = new PathWallLines();
						}
						navLine.setName("slice " + sliceNb);

						if (indexIt != null) {
							int idx = indexIt.get().intValue();
							data.addIndex(idx); // permet de retrouver le slice
							// en fonction de l'index
							data.addIndexMap(idx, sliceNb); // permet de
							// retrouver l'index
							// en fonction du
							// slice
						} else {
							data.addIndex(sliceNb);
							data.addIndexMap(sliceNb, sliceNb);
						}
						// per slice, per ping values
						for (int pingNb = 0; pingNb < nbPings; pingNb++) {
							// LatLon Depth TOP
							Double latTop = 0.0;
							Double lonTop = 0.0;
							Double altTop = 0.0;
							Double heave = 0.0;
							Double tide = 0.0;
							// LatTop
							if (latTopList != null && latTopList.size() == nbPings) {// if
								// LAT_TOP
								// <Dim>1,
								// nbPings</Dim>
								latTop = latTopList.get(pingNb);
							} else if (latTopList == null && latTopIt.size() == nbPings * nbSlices) {// if
								// LAT_TOP
								// <Dim>nbSlices,
								// nbPings</Dim>
								latTop = latTopIt.get();
							}
							// LonTop
							if (lonTopList != null && lonTopList.size() == nbPings) {// if
								// LON_TOP
								// <Dim>1,
								// nbPings</Dim>
								lonTop = lonTopList.get(pingNb);
							} else if (lonTopList == null && lonTopIt.size() == nbPings * nbSlices) {// if
								// LON_TOP
								// <Dim>nbSlices,
								// nbPings</Dim>
								lonTop = lonTopIt.get();
							}
							// DepthTop
							if (depthTopList != null && depthTopList.size() == nbPings) {// if
								// DEPTH_TOP
								// <Dim>1,
								// nbPings</Dim>
								altTop = depthTopList.get(pingNb);
							} else if (depthTopIt != null && depthTopIt.size() == nbPings * nbSlices) {// if
								// DEPTH_TOP
								// <Dim>nbSlices,
								// nbPings</Dim>
								altTop = depthTopIt.get();
							}

							// Heave
							if (heaveList != null && heaveList.size() == nbPings) { // if
								// HEAVE
								// <Dim>1,
								// nbPings</Dim>
								heave = heaveList.get(pingNb);
							} else if (heaveIt != null && heaveIt.size() == nbPings * nbSlices) { // if
								// HEAVE
								// <Dim>nbSlices,
								// nbPings</Dim>
								heave = heaveIt.get();
							}
							// Tide
							if (tideList != null && tideList.size() == nbPings) { // if
								// TIDE
								// <Dim>1,
								// nbPings</Dim>
								tide = tideList.get(pingNb);
							} else if (tideIt != null && tideIt.size() == nbPings * nbSlices) { // if
								// TIDE
								// <Dim>nbSlices,
								// nbPings</Dim>
								tide = tideIt.get();
							}
							altTop = altTop + heave + tide;

							LatLon llTop = LatLon.fromDegrees(latTop, lonTop);
							Position topPoint = new Position(llTop, altTop);
							Position bottomPoint = null;
							Double altBot = 0.0;

							// LatLon Depth BOT
							// DepthBot
							if (depthBotList != null && depthBotList.size() == nbPings) {// if
								// DEPTH_BOT
								// <Dim>1,
								// nbPings</Dim>
								altBot = depthBotList.get(pingNb);
							} else if (depthBotIt != null && depthBotIt.size() == nbPings * nbSlices) {// if
								// DEPTH_BOT
								// <Dim>nbSlices,
								// nbPings</Dim>
								altBot = depthBotIt.get();
							}
							altBot = altBot + heave + tide;

							Double latBot = latTop;
							Double lonBot = lonTop;
							if (latBotIt != null && lonBotIt != null) {
								// LatBot
								if (latBotList != null && latBotList.size() == nbPings) { // if
									// LAT_BOT
									// <Dim>1,
									// nbPings</Dim>
									latBot = latBotList.get(pingNb);
								} else if (latBotList == null && latBotIt.size() == nbPings * nbSlices) {// if
									// LAT_BOT
									// <Dim>nbSlices,
									// nbPings</Dim>
									latBot = latBotIt.get();
								}
								// LonBot
								if (lonBotList != null && lonBotList.size() == nbPings) {// if
									// LON_BOT
									// <Dim>1,
									// nbPings</Dim>
									lonBot = lonBotList.get(pingNb);
								} else if (lonBotList == null && lonBotIt.size() == nbPings * nbSlices) {// if
									// LON_BOT
									// <Dim>nbSlices,
									// nbPings</Dim>
									lonBot = lonBotIt.get();
								}

							}
							LatLon llBot = LatLon.fromDegrees(latBot, lonBot);
							bottomPoint = new Position(llBot, altBot);
							navLine.addPoint(topPoint, bottomPoint);
						}

						data.addLine(sliceNb, navLine);
					}
					depthTopIt = null;
					depthBotIt = null;
					// on libère un peu de mémoire en vidant la list de buffer
					// reflectivityBuffers.clear();
				}
			}
		} catch (Exception e) {
			logger.error("Error during loading", e);
		}

		return data;
	}

	/**
	 * Loads 3DSeimic data and images.
	 */
	private void load3DSeismicData(File seismicFile, Seismic3DLayer layer, Info echos, IProgressMonitor monitor)
			throws GIOException {
		if (echos == null) {
			return;
		}

		// retrieve number of pings
		Dimensions dim = echos.getDimensions();
		Seismic3DData sd = layer.getData();
		int nbx = dim.getNx();
		int nby = dim.getNy();
		int nbz = dim.getNz();
		sd.setNx(nbx);
		sd.setNy(nby);
		sd.setNz(nbz);

		layer.setName(echos.getName());

		// if at least one ping
		if (nbz > 0) {
			Signals signals = echos.getSignals();
			List<Item> signalItems = signals.getItems();
			if (signalItems != null) {
				BufferIterator<Double> zIt = null;
				BufferIterator<Double> tIt = null;

				// png directory
				String imgDir = null;

				File parent = seismicFile.getParentFile();
				String basePath = parent.getAbsolutePath() + "/"; //$NON-NLS-1$
				for (Item item : signalItems) {
					if (imgDir == null) {
						String name = item.getFileName();
						if (name != null) {
							String[] str = name.split("\\\\|/"); //$NON-NLS-1$
							if (str != null && str.length > 0) {
								imgDir = str[0];
							}
						}
					}

					String filename = item.getFileName().trim();
					// replace backslashes by slashes for portability
					filename = filename.replaceAll("\\\\([^ ])", "/$1");
					File file = new File(basePath + filename);

					if (Z_NAME.equals(item.getName())) {
						zIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (T_NAME.equals(item.getName())) {
						tIt = BinaryUtil.readBinFile(file, item.getStorage());
					}
				}

				if (zIt == null || zIt.size() != nbz) {
					logger.warn(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_STARBOARD_LATITUDE")); //$NON-NLS-1$
				}
				if (tIt == null || tIt.size() != nbz) {
					logger.warn(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_STARBOARD_LONGITUDE")); //$NON-NLS-1$
				}

				sd.setImgDir(imgDir);
				for (int i = 0; i < nbz; i++) {
					Double z = zIt.get();
					Double t = tIt.get();

					sd.addZ(z);
					sd.addT(t);
				}
			}

			Images images = echos.getImages();
			List<Item> imagesItems = images.getItems();
			if (imagesItems != null) {
				BufferIterator<Double> lonIt = null;
				BufferIterator<Double> latIt = null;

				File parent = seismicFile.getParentFile();
				String basePath = parent.getAbsolutePath() + "/"; //$NON-NLS-1$
				for (Item item : imagesItems) {

					String filename = item.getFileName().trim();
					// replace backslashes by slashes for portability
					filename = filename.replaceAll("\\\\([^ ])", "/$1");
					File file = new File(basePath + filename);

					if (LONGITUDE_NAME.equals(item.getName())) {
						lonIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (LATITUDE_NAME.equals(item.getName())) {
						latIt = BinaryUtil.readBinFile(file, item.getStorage());
					}
				}

				if (latIt == null || latIt.size() != nbz) {
					logger.warn(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_PORT_LATITUDE")); //$NON-NLS-1$
				}
				if (lonIt == null || lonIt.size() != nbz) {
					logger.warn(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_PORT_LONGITUDE")); //$NON-NLS-1$
				}

				LatLon latlon[][] = new LatLon[nbx][nby];
				for (int i = 0; i < nbx; i++) {
					for (int j = 0; j < nby; j++) {
						Double lat = latIt.get();
						Double lon = lonIt.get();
						latlon[i][j] = LatLon.fromDegrees(lat, lon);
					}
				}
				sd.setLatlon(latlon);
			}
			PlayerBean bean = layer.getPlayerBean();
			// bean.setIndexList(sd.getIndexList());
			bean.setMinPlayIndex(1);
			bean.setPlayIndex(1);
			bean.setMaxPlayIndex(nbz);
			// sd.setPositions(positions);
		}

		monitor.done();
	}

	/**
	 * Read info xml data file.
	 *
	 * @param file the input file
	 */
	public Info readXmlFile(File file) throws CannotResolveClassException, IOException {
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.processAnnotations(Info.class);
		// required for osgi plugins (has its own classloader)
		xstream.setClassLoader(Info.class.getClassLoader());
		// xstream.registerConverter(new Signature2Converter());
		xstream.registerConverter(new LabelEnumConverter());

		xstream.addPermission(AnyTypePermission.ANY);
		InputStream fis = new FileInputStream(file);
		Object value = xstream.fromXML(fis);
		fis.close();
		return (Info) value;
	}

	@Override
	public List<Pair<String, String>> extensions() {
		return extensions;
	}

	@Override
	public String getImage(Object resource) {
		return "icons/16/echogramLong.png";
	}

	@Override
	public int getPriority() {
		return DEFAULT_PRIORITY;
	}

	/* @formatter:off
 	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	@formatter:on */

	@Override
	public ContentType getContentType(String resource) {
		return accept(resource) ? ContentType.OLD_REFLECTIVITY_XML : ContentType.UNDEFINED;
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.OLD_REFLECTIVITY_XML);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(getImage(null), getClass()));
	}

	@Override
	public List<String> getExtensions() {
		return Collections.singletonList("xml");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return extensions();
	}

	@Override
	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		return Optional
				.ofNullable(accept(filePath) ? new BridgeFileInfo(filePath, getContentType(filePath), this) : null);
	}

}
