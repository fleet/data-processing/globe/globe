/*
 * @License@
 */
package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.util.List;

import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.viewer3d.util.ILazyLoading;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * This interface define methods for input file loading.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public interface ILoader {

	/**
	 * Returns a file filter for files allowed by this loader.
	 * <p>
	 * This file filter is meant to be used in files GUI and therefore should be
	 * quite fast.
	 * </p>
	 * 
	 * @return a file filter for files allowed by this loader.
	 */
	public FileNameExtensionFilter getFileFilter();

	/**
	 * Returns true if this loader is able to read provided file.
	 * <p>
	 * This method does a deep check before file loading in order to avoid
	 * irrelevant errors during actual loading.
	 * </p>
	 * 
	 * @param file
	 *            the file absolute path to load
	 * @return true if the file can be loaded, false otherwise
	 */
	public boolean accept(String file);

	/**
	 * Pre-load file content : creates an empty layer, if returned layer
	 * implements {@link ILazyLoading} .
	 * 
	 * @param file
	 *            the file to load
	 * @param progress
	 *            un éventuel suivi de la progression
	 * @return initialized layers (it may also returns an elevation models).
	 * @throws Exception
	 *             if any error occurs during reading
	 */
	List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor progress) throws Exception;
}
