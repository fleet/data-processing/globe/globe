/*
 * @License@
 */
package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.data.DataType;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.IDataDriver;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyBasicTiledImageLayer;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTiledImageLayer;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.terrain.SSVElevationModel;
import fr.ifremer.viewer3d.util.ZipUtils;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.util.LevelSet;

/**
 * Loader for bathymetry files.
 */
@Component(name = "globe_viewer3d_old_xml_loader", service = { IIcon16ForFileProvider.class, IDataDriver.class,
		IFileInfoSupplier.class })
public class XMLLoader implements ILoader, //
		IDataDriver, // BRIDGE !
		IIcon16ForFileProvider, IFileInfoSupplier<BridgeFileInfo> {

	private Logger logger = LoggerFactory.getLogger(XMLLoader.class);

	public static final String FORMAT_BIL = AVKey.FLOAT32;// INT16;//

	/** Extension for XML (!?) files. */
	public static final String EXTENSION = DicoBundle.getString("KEY_XMLLOADER_FILE_EXTENSION"); //$NON-NLS-1$

	public static final String ELEVATION = DataType.elevation.name();
	public static final String TEXTURE = DataType.texture.name();

	private static List<Pair<String, String>> extensions;

	static {
		List<Pair<String, String>> list = new ArrayList<>();
		list.add(new Pair<>("Bathymetry (*.xml)", "*" + EXTENSION));
		extensions = Collections.unmodifiableList(list);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileNameExtensionFilter getFileFilter() {
		return new FileNameExtensionFilter(DicoBundle.getString("KEY_XMLLOADER_FILTER_MESSAGE"), EXTENSION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean accept(String resource) {
		File file = new File(resource);
		if (file != null) {
			String name = file.getName();
			if (name != null) {
				if (name.endsWith(EXTENSION)) {
					if ((name.contains("_texture") || name.contains("_elevation")) && !name.contains("_texture_v2")
							&& !name.contains("_elevation_v2")) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor monitor) throws Exception {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		if (file == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_XMLLOADER_ERROR_MESSAGE")); //$NON-NLS-1$
		}

		String[] tab = XMLReader(file.getAbsolutePath());
		subMonitor.worked(10);

		// Test présence des données
		File parent = FileUtils.getParent(file);
		File item = new File(parent.getAbsolutePath() + "/" + tab[9]);
		File dataFile = item.getParentFile(); // data
		file = ZipUtils.unzipDataInCache(file, dataFile);

		return viewImage(tab, file, subMonitor.split(90));
	}

	/**
	 * ?
	 *
	 * @param filepath path of file to load ?
	 * @return ?
	 * @throws ParserConfigurationException ?
	 * @throws SAXException ?
	 * @throws IOException ?
	 */
	public String[] XMLReader(String filepath) throws ParserConfigurationException, SAXException, IOException {
		int lenght = 12;
		String[] tab = new String[lenght];

		File file = new File(filepath);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		doc.getDocumentElement().normalize();

		NodeList NLname = doc.getElementsByTagName("Name"); //$NON-NLS-1$
		String name = ((Element) NLname.item(0)).getChildNodes().item(0).getNodeValue();
		tab[0] = name;

		NodeList North = doc.getElementsByTagName("North"); //$NON-NLS-1$
		String N = ((Element) North.item(0)).getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
		tab[1] = N;

		NodeList South = doc.getElementsByTagName("South"); //$NON-NLS-1$
		String S = ((Element) South.item(0)).getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
		tab[2] = S;

		NodeList East = doc.getElementsByTagName("East"); //$NON-NLS-1$
		String E = ((Element) East.item(0)).getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
		tab[3] = E;

		NodeList West = doc.getElementsByTagName("West"); //$NON-NLS-1$
		String W = ((Element) West.item(0)).getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
		tab[4] = W;

		NodeList NLlztsd = doc.getElementsByTagName("LevelZeroTileSizeDegrees"); //$NON-NLS-1$
		String lztsd = ((Element) NLlztsd.item(0)).getChildNodes().item(0).getNodeValue();
		tab[5] = lztsd;

		NodeList NLnblevels = doc.getElementsByTagName("NumberLevels"); //$NON-NLS-1$
		String nblevels = ((Element) NLnblevels.item(0)).getChildNodes().item(0).getNodeValue();
		tab[6] = nblevels;

		NodeList NLtsp = doc.getElementsByTagName("TextureSizePixels"); //$NON-NLS-1$
		String sizePixels = ((Element) NLtsp.item(0)).getChildNodes().item(0).getNodeValue();
		tab[7] = sizePixels;

		NodeList NLfileExtension = doc.getElementsByTagName("ImageFileExtension"); //$NON-NLS-1$
		String fileExtension = ((Element) NLfileExtension.item(0)).getChildNodes().item(0).getNodeValue();
		tab[8] = fileExtension;

		NodeList Ipath = doc.getElementsByTagName("PermanentDirectory"); //$NON-NLS-1$
		String path = ((Element) Ipath.item(0)).getChildNodes().item(0).getNodeValue();
		tab[9] = path;

		NodeList Dresol = doc.getElementsByTagName("Resolution"); //$NON-NLS-1$
		try {
			NodeList children = Dresol.item(0).getChildNodes();
			for (int index = 0; index < children.getLength(); index++) {
				Node n = children.item(index);
				if ("Latitude".equals(n.getNodeName())) {
					tab[10] = n.getChildNodes().item(0).getNodeValue();
				} else if ("Longitude".equals(n.getNodeName())) {
					tab[11] = n.getChildNodes().item(0).getNodeValue();
				}
			}
		} catch (Exception e) {
			tab[10] = "100";// par defaut on met une resolution très faible
			tab[11] = "100";// par defaut on met une resolution très faible
		}

		return tab;
	}

	/**
	 * ?
	 *
	 * @param tab ???
	 * @param dataPath if data files referenced are relative, this path is use as reference path.
	 * @throws IOException
	 */
	@SuppressWarnings({ "unused", "deprecation" })
	public List<Pair<Layer, ElevationModel>> viewImage(String[] tab, File dataFile, IProgressMonitor monitor)
			throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		String name = tab[0];
		String dataPath = null;
		if (dataFile != null) {
			dataPath = dataFile.getAbsolutePath();
		}

		MyTiledImageLayer btil;

		Double North = Double.parseDouble(tab[1]);
		Double South = Double.parseDouble(tab[2]);
		Double East = Double.parseDouble(tab[3]);
		Double West = Double.parseDouble(tab[4]);
		Double lztsd = Double.parseDouble(tab[5]);
		int nblevels = Integer.parseInt(tab[6]);
		int tileSize = Integer.parseInt(tab[7]);
		String fileExtension = tab[8];
		String filePath = tab[9];
		// only keeps latitude resolution
		Double resolution = Double.parseDouble(tab[10]);
		File path = null;
		if (isAbsolute(filePath)) {
			path = new File(filePath);
		} else {
			if (dataPath != null) {
				File parent = new File(dataPath);
				if (!parent.isDirectory()) {
					parent = FileUtils.getParent(parent);
					dataPath = parent.getAbsolutePath() + "/" + filePath;
				}
				path = new File(parent.getAbsolutePath() + File.separator + filePath);
			}
		}
		if (path == null) {
			return null;
		}

		WorldWind.getDataFileStore().addLocation(path.getParent(), true);
		AVList avList = new AVListImpl();

		Sector sector = new Sector(Angle.fromDegrees(South), Angle.fromDegrees(North), Angle.fromDegrees(West),
				Angle.fromDegrees(East));

		avList.setValue(AVKey.SECTOR, sector);
		LatLon lzsd = new LatLon(Angle.fromDegrees(lztsd), Angle.fromDegrees(lztsd));

		if (dataPath != null) {
			avList.setValue(AVKey.FILE_STORE_LOCATION, dataPath);
		}
		avList.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, lzsd);
		avList.setValue(AVKey.NUM_LEVELS, nblevels);
		avList.setValue(AVKey.NUM_EMPTY_LEVELS, 0);
		avList.setValue(AVKey.LEVEL_NAME, name);
		avList.setValue(AVKey.TILE_WIDTH, tileSize);
		avList.setValue(AVKey.TILE_HEIGHT, tileSize);
		avList.setValue(AVKey.TILE_DELTA, lzsd);
		avList.setValue(AVKey.DATASET_NAME, name);
		avList.setValue(AVKey.FORMAT_SUFFIX, "." + fileExtension); //$NON-NLS-1$

		avList.setValue(AVKey.DATA_CACHE_NAME, filePath);

		LevelSet levelSet = new LevelSet(avList);

		subMonitor.worked(10);

		if (name != null) {
			if (name.contains("_elevation")) {
				btil = new ShaderElevationLayer(dataFile, levelSet);
				((ShaderElevationLayer) btil).setResolution(resolution);
			}
			// shader texture are coded on 24bits, old texture on 8bits
			/*
			 * else if (name.endsWith("_texture")) { btil = new ShaderElevationLayer(dataFile, levelSet);
			 * ((ShaderElevationLayer)btil).setResolution(resolution); }
			 */else {
				btil = new MyBasicTiledImageLayer(dataFile, levelSet);
			}
			btil.setBinFolder(new File(dataFile.getParentFile(), name));
			btil.setNetworkRetrievalEnabled(false);
			btil.setForceLevelZeroLoads(false);
			btil.setRetainLevelZeroTiles(false);

			if (name.endsWith("_elevation")) {
				name = name.substring(0, name.length() - 10);
			} else if (name.endsWith("_texture")) {
				name = name.substring(0, name.length() - 8);
			} else {
				// btil.setName(name);
			}

			BasicElevationModel bem = null;

			boolean recursive = true;
			String[] extensions = new String[] { "bil" };
			File directory = path;

			// creates an elevation model only if elevation model exists
			if (directory != null) {
				Collection<?> col = org.apache.commons.io.FileUtils.listFiles(directory, extensions, recursive);
				subMonitor.worked(40);
				if (col != null && !col.isEmpty()) {
					avList.setValue(AVKey.BYTE_ORDER, AVKey.LITTLE_ENDIAN);
					avList.setValue(AVKey.DETAIL_HINT, 0.0);
					avList.setValue(AVKey.ELEVATION_MIN, -11000.0);
					avList.setValue(AVKey.ELEVATION_MAX, 9000.0);
					avList.setValue(AVKey.PIXEL_TYPE, FORMAT_BIL);
					avList.setValue(AVKey.FORMAT_SUFFIX, ".bil"); //$NON-NLS-1$

					bem = new SSVElevationModel(avList);
				}
			}

			List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(1);
			layers.add(new Pair<Layer, ElevationModel>(btil, bem)); // btil
			subMonitor.done();
			return layers;
		}
		subMonitor.done();

		return null;
	}

	/**
	 * Return true if provided file path is an absolute path.
	 *
	 * @param filePath the file path to check
	 * @return true if provided file path is an absolute path, false otherwise.
	 */
	private boolean isAbsolute(String filePath) {
		if (filePath == null) {
			return false;
		}

		if (filePath.startsWith("/")) {
			return true;
		}
		if (filePath.length() > 1 && filePath.charAt(1) == ':') {
			return true;
		}

		return false;
	}

	@Override
	public List<Pair<String, String>> extensions() {
		return extensions;
	}

	@Override
	public String getImage(Object resource) {
		return "icons/16/elevation.png";
	}

	@Override
	public int getPriority() {
		return DEFAULT_PRIORITY;
	}

	/* @formatter:off
	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	@formatter:on */
	@Override
	public ContentType getContentType(String resource) {
		return accept(resource) ? ContentType.OLD_BATHYMETRY_XML : ContentType.UNDEFINED;
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.OLD_BATHYMETRY_XML);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(getImage(null), getClass()));
	}

	@Override
	public List<String> getExtensions() {
		return Collections.singletonList("xml");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return extensions();
	}

	@Override
	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		return Optional
				.ofNullable(accept(filePath) ? new BridgeFileInfo(filePath, getContentType(filePath), this) : null);
	}

}
