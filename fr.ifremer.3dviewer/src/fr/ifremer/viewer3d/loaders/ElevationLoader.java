package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.ui.utils.cache.NasaCache.CacheContext;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.exception.GUnknownException;
import fr.ifremer.viewer3d.data.FileProjectionCacheService;
import fr.ifremer.viewer3d.data.IBoundedMinMax;
import fr.ifremer.viewer3d.data.MyTiledElevationProducer;
import fr.ifremer.viewer3d.data.MyTiledImageProducer;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.IDataDriver;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTiledImageLayer;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.terrain.InterpolateElevationModel;
import fr.ifremer.viewer3d.util.ZipUtils;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.SurfaceImageLayer;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * chargement de fichiers geotiff ou dtm creation d'un cache (pyramide)
 *
 * @author mmorvan
 */
@Component(name = "globe_viewer3d_elevation_loader_data_driver", service = { IIcon16ForFileProvider.class,
		IDataDriver.class, IFileInfoSupplier.class })
public class ElevationLoader implements ILoader, //
		IDataDriver, // BRIDGE !
		IIcon16ForFileProvider, IFileInfoSupplier<BridgeFileInfo> {

	/** Logger for this class. */
	private static Logger logger = LoggerFactory.getLogger(ElevationLoader.class);

	public static final int DEFAULT_MIN_ELEVATION = -20000;
	public static final int DEFAULT_MAX_ELEVATION = 20000;

	public static final String EXTENSION1 = ".xml";

	// public static final String EXTENSION3 = ".mbg"; //$NON-NLS-1$
	public static final String EXTENSION_TEXTURE = "_texture_v2"; //$NON-NLS-1$
	public static final String EXTENSION_ELEVATION = "_elevation_v2"; //$NON-NLS-1$
	public static final String EXTENSION_UNKNOWN = "_unknownTex_v2"; //$NON-NLS-1$

	@Reference
	protected IWWElevationModelFactory elevationModelFactory;

	public class Context implements IBoundedMinMax {
		/** The data field to read. */
		public String dataField = IConstants.Depth.toUpperCase();

		public String type;
		public String unit;
		public double globalMin;
		public double globalMax;
		public double valMin;
		public double valMax;

		public boolean[] mapped;
		public String[] name;
		public String[] PermanentDirectory;
		public String[] terrainMapped;
		public String[] ImageFileExtension;
		public LevelSet levelSet;
		public ShaderElevationLayer[] elevationLayer;

		public double[] resolution;
		public boolean recomputeCache = true;
		private double[] dateLineTrick;

		public void setDataField(String dataField) {
			this.dataField = dataField;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getUnit() {
			return unit;
		}

		public void setUnit(String unit) {
			this.unit = unit;
		}

		@Override
		public double getValGlobaleMin() {
			return globalMin;
		}

		@Override
		public void setValGlobaleMin(double globalMin) {
			this.globalMin = globalMin;
		}

		@Override
		public double getValGlobaleMax() {
			return globalMax;
		}

		@Override
		public void setValGlobaleMax(double globalMax) {
			this.globalMax = globalMax;
		}

		@Override
		public double getValMin() {
			return valMin;
		}

		@Override
		public void setValMin(double valMin) {
			this.valMin = valMin;
		}

		@Override
		public double getValMax() {
			return valMax;
		}

		@Override
		public void setValMax(double valMax) {
			this.valMax = valMax;
		}

		public boolean[] getMapped() {
			return mapped;
		}

		public void setMapped(boolean[] mapped) {
			this.mapped = mapped;
		}

		public String[] getName() {
			return name;
		}

		public void setName(String[] name) {
			this.name = name;
		}

		public String[] getPermanentDirectory() {
			return PermanentDirectory;
		}

		public void setPermanentDirectory(String[] permanentDirectory) {
			PermanentDirectory = permanentDirectory;
		}

		public String[] getTerrainMapped() {
			return terrainMapped;
		}

		public void setTerrainMapped(String[] terrainMapped) {
			this.terrainMapped = terrainMapped;
		}

		public String[] getImageFileExtension() {
			return ImageFileExtension;
		}

		public void setImageFileExtension(String[] imageFileExtension) {
			ImageFileExtension = imageFileExtension;
		}

		public LevelSet getLevelSet() {
			return levelSet;
		}

		public void setLevelSet(LevelSet levelSet) {
			this.levelSet = levelSet;
		}

		public ShaderElevationLayer[] getElevationLayer() {
			return elevationLayer;
		}

		public void setElevationLayer(ShaderElevationLayer[] elevationLayer) {
			this.elevationLayer = elevationLayer;
		}

		public double[] getResolution() {
			return resolution;
		}

		public void setResolution(double[] resolution) {
			this.resolution = resolution;
		}

		public boolean isRecomputeCache() {
			return recomputeCache;
		}

		public void setRecomputeCache(boolean recomputeCache) {
			this.recomputeCache = recomputeCache;
		}

		public String getDataField() {
			return dataField;
		}

		public void reset() {
			type = null;
			globalMin = 0;
			globalMax = 0;
			valMin = 0;
			valMax = 0;
		}

		public double[] getDateLineTrick() {
			return dateLineTrick;
		}

		public void setDateLineTrick(double[] dateLineTrick) {
			this.dateLineTrick = dateLineTrick;
		}
	}

	public ElevationLoader() {
		// System.err.println("Elevation Loader Constructor");
	}

	// private DtmDriver dtmDriver = DtmDriver.grab();

	@Override
	public FileNameExtensionFilter getFileFilter() {
		return new FileNameExtensionFilter("Elevation", EXTENSION1);// , DtmDriver.EXTENSION_DTM1);
	}

	@Override
	public boolean accept(String resource) {
		if (resource == null) {
			return false;
		}

		File file = new File(resource);
		if (!file.exists()) {
			return false;
		}
		Context context = new Context();
		context.reset();

		boolean extensionAccepted = false;
		long sizeOfData = 0;

		if (file.getName().endsWith(EXTENSION_TEXTURE + EXTENSION1)
				|| file.getName().endsWith(EXTENSION_ELEVATION + EXTENSION1)
				|| file.getName().endsWith(EXTENSION_UNKNOWN + EXTENSION1)) {

			extensionAccepted = true;

			// try to compute the size of data
			try {
				// parse the xml to find the data files
				XMLReader(file.getPath(), context);

				File parent = null;
				try {
					parent = FileUtils.getParent(file);
				} catch (GIOException e) {
					logger.error("Error to get parent file ", e);
				}

				for (int i = 0; i < context.name.length; i++) {
					String path = parent.getPath();
					path = path + "/" + context.PermanentDirectory[i];
					File sourceFile = new File(
							path + "/" + context.name[i] + EXTENSION_ELEVATION + "." + context.ImageFileExtension[i]);

					sizeOfData += sourceFile.length();
				}
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} /*
			 * else if (dtmDriver.accept(resource)) {
			 * 
			 * extensionAccepted = true;
			 * 
			 * sizeOfData += file.length(); }
			 */
		// else if (file.getName().endsWith(GMTRasterAttributesFiller.FILE_EXTENSION)) {
		// extensionAccepted = true;
		// sizeOfData += file.length();
		// }
		else if (file.isDirectory()) {

			extensionAccepted = true;
		}

		if (extensionAccepted) {
			// compare cache memory with size of data
			if (sizeOfData <= SSVConfiguration.getConfiguration().getCacheSize()) {
				return true;
			} else {
				// not enough cache memory
				Display.getDefault()
						.asyncExec(() -> MessageDialog.openError(Display.getCurrent().getActiveShell(),
								"Insufficient cache memory",
								"Please increase the size of the cache memory in preferences menu"));
				return false;
			}
		} else {
			// extension not accepted
			return false;
		}

	}

	@Override
	public List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor monitor) throws Exception {

		int nbTicks = 1;
		File nasaCacheRootFolder = WorldWindUtils.getCacheLocation();
		// for (DtmLayerType dtmLayerType : DtmLayerType.values()) {
		// String cacheName = guessNasaCacheFolder(file.getName(), dtmLayerType.name());
		// if (dtmLayerType != DtmLayerType.DEPTH && new File(nasaCacheRootFolder,
		// cacheName).exists()) {
		// nbTicks++;
		// }
		// }

		SubMonitor subMonitor = SubMonitor.convert(monitor, nbTicks);

		List<Pair<Layer, ElevationModel>> result = new ArrayList<>();

		// Layer Depth is always loaded
		Context context = new Context();
		// context.dataField = DtmLayerType.DEPTH.name();
		result.addAll(load(file, context, subMonitor.split(1)));
		//
		// // Other layers
		// for (DtmLayerType dtmLayerType : DtmLayerType.values()) {
		// String cacheName = guessNasaCacheFolder(file.getName(), dtmLayerType.name());
		// // Check if a cache folder exists for the layer type
		// if (dtmLayerType != DtmLayerType.DEPTH && new File(nasaCacheRootFolder,
		// cacheName).exists()) {
		// // A cache folder exists, so reload the layer
		// context = new Context();
		// context.dataField = dtmLayerType.name();
		// result.addAll(load(file, context, subMonitor.split(1)));
		// }
		// }

		subMonitor.done();
		return result;
	}

	/** Compute the subfolder of WW cache to store specified layer */
	protected String guessNasaCacheFolder(String displayName, String type) {
		return NasaCache.getCacheSubDirectory() + "/" + WWIO.replaceIllegalFileNameCharacters(displayName) + "_texFrom"
				+ type;
	}

	public List<Pair<Layer, ElevationModel>> load(File inputFile, Context context, IProgressMonitor monitor)
			throws Exception {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		if (inputFile.getName().endsWith(EXTENSION_TEXTURE + EXTENSION1)
				|| inputFile.getName().endsWith(EXTENSION_ELEVATION + EXTENSION1)
				|| inputFile.getName().endsWith(EXTENSION_UNKNOWN + EXTENSION1)) {

			XMLReader(inputFile.getPath(), context);

			// Test présence des données
			File parent = inputFile.getParentFile();
			File item = new File(parent.getAbsolutePath() + "/" + context.PermanentDirectory[0]);
			File dataFile = item.getParentFile(); // data
			inputFile = ZipUtils.unzipDataInCache(inputFile, dataFile);

		} else {
			context.name = new String[1];
			context.name[0] = inputFile.getName();
			context.terrainMapped = new String[1];
			context.terrainMapped[0] = "true";
			context.resolution = new double[1];
			context.resolution[0] = 100;
			context.mapped = new boolean[1];
			context.mapped[0] = true;
			context.elevationLayer = new ShaderElevationLayer[1];
		}
		int size = 2;
		List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(size);

		// sonar data : utilise les shaders pour le contrast et l'ombrage
		if (!inputFile.getName().endsWith(EXTENSION_UNKNOWN + EXTENSION1)) {
			File parent = FileUtils.getParent(inputFile);
			String path = parent.getPath();

			ElevationModel em = null;

			File sourceFile = null;
			File fileStoreLocation = WorldWindUtils.getCacheLocation();
			logger.debug("File store location : {}", fileStoreLocation.getAbsolutePath());

			if (inputFile.getName().endsWith(EXTENSION_ELEVATION + EXTENSION1)) {
				path = path + "/" + context.PermanentDirectory[0];
				sourceFile = new File(
						path + "/" + context.name[0] + EXTENSION_ELEVATION + "." + context.ImageFileExtension[0]);
			} else if (inputFile.getName().endsWith(EXTENSION_TEXTURE + EXTENSION1)) {
				path = path + "/" + context.PermanentDirectory[0];
				sourceFile = new File(
						path + "/" + context.name[0] + EXTENSION_TEXTURE + "." + context.ImageFileExtension[0]);
			} /*
				 * else if (inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM1) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM2) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM3) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM4) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM5) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM6) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM7) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM8) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM9) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM11) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM12) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM13) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM14) ||
				 * inputFile.getName().endsWith(DtmDriver.EXTENSION_DTM15)) { sourceFile = new File(path + "/" +
				 * context.name[0]); }
				 */
			// else if (inputFile.getName().endsWith(GMTRasterAttributesFiller.FILE_EXTENSION)) {
			// sourceFile = new File(path + "/" + context.name[0]);
			//
			// }

			// Load Elevation model for Depth layer only
			// if (DtmLayerType.DEPTH.name().equals(context.dataField) && sourceFile != null
			// && sourceFile.exists()
			// && !inputFile.getName().endsWith(EXTENSION_TEXTURE + EXTENSION1)) {
			// em = importElevationData(context.name[0], sourceFile, fileStoreLocation,
			// context, inputFile,
			// subMonitor.split(90));
			// if (em == null) {
			// return layers;
			// }
			// }

			if (sourceFile != null && sourceFile.exists()) {
				// data without xml are considered as bathymetry
				if (context.type == null) {
					if (context.dataField != null) {
						context.type = context.dataField;
					} else {
						context.type = "Bathymetry";
					}
					context.unit = "m";
					context.globalMin = DEFAULT_MIN_ELEVATION;
					context.globalMax = 2000000; // ??
					context.valMin = 0;
					context.valMax = 0;

					if (context.type.equals("uncertainty")) {
						context.unit = "";
						context.globalMin = -200;
						context.globalMax = 200;
					}
				}
				context.elevationLayer[0] = importSurfaceImage(sourceFile, context.name[0], context.type, sourceFile,
						fileStoreLocation, context.dataField, context);
				context.elevationLayer[0].setResolution(context.resolution[0]);
				layers.add(new Pair<Layer, ElevationModel>(context.elevationLayer[0], em));

				// // Isobaths layer : TODO should be provided by a service
				// if (em != null) {
				// String inputFilePath = inputFile.getAbsolutePath();
				//
				// // DEM file supplier
				// if (dtmDriver.accept(inputFilePath)) {
				// Supplier<File> demFileSupplier = () -> {
				// // Generate TIFF from DTM file
				// DtmInfo dtmInfo = dtmDriver.readDriverInfo(inputFilePath);
				// Projection dtmProjection = dtmInfo.getProjection();
				// DtmLayer depthLayer = dtmDriver.loadDataStore(IDtmConstants.DepthLayer,
				// dtmInfo);
				// Dataset dataset = null;
				// Dataset reprojectedDataset = null;
				// File tmpNotProjDemFile = null;
				// try {
				// dataset = gdal.Open("netcdf:\"" + inputFilePath + "\":DEPTH");
				// if (dataset == null) {
				// logger.warn("Unable to open the DTM file with gdal.");
				// return null;
				// }
				// GeoBox projectedGeoBox = new GeoBox(GdalUtils.getBoundingBox(dataset));
				// dataset.delete(); // Close the file
				//
				// tmpNotProjDemFile = TemporaryCache.getTemporaryFile("DEM_" +
				// dtmInfo.getPath(),
				// ".tiff");
				// File demFile = TemporaryCache.getTemporaryFile("DEM_PROJ_" +
				// dtmInfo.getPath(),
				// ".tiff");
				//
				// depthLayer.loadZone(depthLayer.getTotalZone());
				//
				// dataset = DtmLayerToTiffSerializer.serializeDtmModel(depthLayer,
				// dtmProjection,
				// projectedGeoBox, tmpNotProjDemFile, subMonitor.split(100));
				//
				// // Reprojection to lat / lon
				// reprojectedDataset = new GdalWarp(dataset, demFile)
				// .addTargetProjection(new
				// SpatialReference(osrConstants.SRS_WKT_WGS84_LAT_LONG)).run();
				//
				// return demFile;
				//
				// } catch (GIOException | IOException e) {
				// logger.error("Error while computing DEM .tiff : " + inputFilePath, e);
				// } finally {
				// if (dataset != null) {
				// dataset.delete();
				// }
				// if (reprojectedDataset != null) {
				// reprojectedDataset.delete();
				// }
				// if (tmpNotProjDemFile != null) {
				// TemporaryCache.delete(tmpNotProjDemFile);
				// }
				// }
				//
				// return null;
				// };
				//
				// IWWIsobathLayer isobathLayer = IWWLayerFactory.grab().createIsobathLayer(//
				// context.name[0], //
				// GeoBoxUtils.convert(context.elevationLayer[0].getSector()), //
				// demFileSupplier);
				// if (isobathLayer != null) {
				// layers.add(new Pair<>(isobathLayer, null));
				// }
				//
				// }
				// }

				var scaleLayer = IWWLayerFactory.grab().createColorScaleLayer(context.elevationLayer[0],
						context.getName()[0]);
				layers.add(new Pair<Layer, ElevationModel>(scaleLayer, null));

				if (logger.isDebugEnabled()) {
					logger.debug(context.elevationLayer[0].getName() + " texture created");
				}
			} else if (em != null) {
				context.elevationLayer[0] = new ShaderElevationLayer(inputFile, context.levelSet);
				context.elevationLayer[0].setEnabled(true);
				context.elevationLayer[0].setBinFolder(new File(fileStoreLocation, context.name[0]));
				context.elevationLayer[0].setNetworkRetrievalEnabled(false);
				context.elevationLayer[0].setForceLevelZeroLoads(false);
				context.elevationLayer[0].setRetainLevelZeroTiles(false);
				context.elevationLayer[0].setResolution(context.resolution[0]);

				layers.add(new Pair<Layer, ElevationModel>(context.elevationLayer[0], em));
				if (logger.isDebugEnabled()) {
					logger.debug(context.elevationLayer[0].getName() + " elevation created");
				}

				var scaleLayer = IWWLayerFactory.grab().createColorScaleLayer(context.elevationLayer[0],
						context.getName()[0]);
				layers.add(new Pair<Layer, ElevationModel>(scaleLayer, null));

				context.elevationLayer[0].getContrastControler().getModel().retainLayers(layers,
						context.elevationLayer[0].getType());
			}
			// elevation and image are loaded, remove projection cache file if
			// it exists
			FileProjectionCacheService.getInstance().remove(inputFile.toPath());

		} else {
			// donnees non modifiables par les shaders
			SurfaceImageLayer layer = new SurfaceImageLayer();
			for (String imageFile : context.name) {
				layer.addImage(imageFile);
			}
			layers.add(new Pair<Layer, ElevationModel>(layer, null));
		}

		subMonitor.done();
		return layers;
	}

	public void XMLCacheReader(String filepath, Context context)
			throws ParserConfigurationException, SAXException, IOException {
		File file = new File(filepath);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		doc.getDocumentElement().normalize();

		NodeList parse_unit = doc.getElementsByTagName("Unit");
		NodeList parse_globalMin = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_GlobalMinValue);
		NodeList parse_globalMax = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_GlobalMaxValue);
		NodeList parse_valMin = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_MinValue);
		NodeList parse_valMax = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_MaxValue);

		try {
			context.unit = ((Element) parse_unit.item(0)).getChildNodes().item(0).getNodeValue();
		} catch (Exception e) {
			context.unit = "m"; // par défaut
		}
		try {
			context.globalMin = Double
					.valueOf(((Element) parse_globalMin.item(0)).getChildNodes().item(0).getNodeValue());
		} catch (Exception e) {
			context.globalMin = DEFAULT_MIN_ELEVATION; // par défaut
		}
		try {
			context.globalMax = Double
					.valueOf(((Element) parse_globalMax.item(0)).getChildNodes().item(0).getNodeValue());
		} catch (Exception e) {
			context.globalMax = DEFAULT_MAX_ELEVATION; // par défaut
		}
		try {
			context.valMin = Double.valueOf(((Element) parse_valMin.item(0)).getChildNodes().item(0).getNodeValue());
		} catch (Exception e) {
			context.valMin = DEFAULT_MIN_ELEVATION; // par défaut
		}
		try {
			context.valMax = Double.valueOf(((Element) parse_valMax.item(0)).getChildNodes().item(0).getNodeValue());
		} catch (Exception e) {
			context.valMax = DEFAULT_MAX_ELEVATION; // par défaut
		}
	}

	public void XMLReader(String filepath, Context context)
			throws ParserConfigurationException, SAXException, IOException {

		File file = new File(filepath);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		doc.getDocumentElement().normalize();

		NodeList parse_type = doc.getElementsByTagName("Type"); //$NON-NLS-1$
		NodeList parse_unit = doc.getElementsByTagName("Unit"); //$NON-NLS-1$
		NodeList parse_globalMin = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_GlobalMinValue); // $NON-NLS-1$
		NodeList parse_globalMax = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_GlobalMaxValue); // $NON-NLS-1$
		NodeList parse_valMin = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_MinValue); // $NON-NLS-1$
		NodeList parse_valMax = doc.getElementsByTagName(MyTiledImageLayer.XML_KEY_MaxValue); // $NON-NLS-1$

		try {
			context.setType(((Element) parse_type.item(0)).getChildNodes().item(0).getNodeValue());
		} catch (Exception e) {
			context.setType("Bathymetry"); // par défaut
		}
		try {
			context.setUnit(((Element) parse_unit.item(0)).getChildNodes().item(0).getNodeValue());
		} catch (Exception e) {
			context.setUnit("m"); // par défaut
		}
		try {
			context.setValGlobaleMin(
					Double.valueOf(((Element) parse_globalMin.item(0)).getChildNodes().item(0).getNodeValue()));
		} catch (Exception e) {
			context.setValGlobaleMin(DEFAULT_MIN_ELEVATION); // par défaut
		}
		try {
			context.setValGlobaleMax(
					Double.valueOf(((Element) parse_globalMax.item(0)).getChildNodes().item(0).getNodeValue()));
		} catch (Exception e) {
			context.setValGlobaleMax(DEFAULT_MAX_ELEVATION); // par défaut
		}
		try {
			context.setValMin(Double.valueOf(((Element) parse_valMin.item(0)).getChildNodes().item(0).getNodeValue()));
		} catch (Exception e) {
			context.setValMin(DEFAULT_MIN_ELEVATION);
		}
		try {
			context.setValMax(Double.valueOf(((Element) parse_valMax.item(0)).getChildNodes().item(0).getNodeValue()));
		} catch (Exception e) {
			context.setValMax(DEFAULT_MAX_ELEVATION); // par défaut
		}
		NodeList parse_name = doc.getElementsByTagName("Name"); //$NON-NLS-1$
		NodeList parse_terrain = doc.getElementsByTagName("TerrainMapped"); //$NON-NLS-1$ ;
		NodeList parse_image = doc.getElementsByTagName("ImageFileExtension"); //$NON-NLS-1$
		NodeList parse_directory = doc.getElementsByTagName("PermanentDirectory"); //$NON-NLS-1$
		NodeList parse_resolution = doc.getElementsByTagName("Latitude"); //$NON-NLS-1$
		NodeList parse_resolution_exception = doc.getElementsByTagName("Resolution"); //$NON-NLS-1$

		context.setName(new String[parse_name.getLength()]);
		context.setTerrainMapped(new String[parse_name.getLength()]);
		context.setImageFileExtension(new String[parse_name.getLength()]);
		context.setPermanentDirectory(new String[parse_name.getLength()]);
		context.setResolution(new double[parse_name.getLength()]);
		context.setMapped(new boolean[parse_name.getLength()]);
		context.setDateLineTrick(new double[parse_name.getLength()]);
		context.setElevationLayer(new ShaderElevationLayer[parse_name.getLength()]);

		for (int i = 0; i < parse_name.getLength(); i++) {
			context.getName()[i] = ((Element) parse_name.item(i)).getChildNodes().item(0).getNodeValue();

			context.getTerrainMapped()[i] = ((Element) parse_terrain.item(i)).getChildNodes().item(0).getNodeValue();
			context.getImageFileExtension()[i] = ((Element) parse_image.item(i)).getChildNodes().item(0).getNodeValue();
			context.getPermanentDirectory()[i] = ((Element) parse_directory.item(i)).getChildNodes().item(0)
					.getNodeValue();

			try {
				// the resolution is defined by the dLat (longitude depends upon
				// latitude)

				context.getResolution()[i] = Double
						.valueOf(((Element) parse_resolution.item(i)).getChildNodes().item(0).getNodeValue());
			} catch (Exception e) {
				try {
					context.getResolution()[i] = Double.valueOf(
							((Element) parse_resolution_exception.item(i)).getChildNodes().item(0).getNodeValue());
				} catch (Exception e2) {
					context.getResolution()[i] = 100;// par defaut on met une
					// resolution faible
				}
			}

			try {
				double maxLongitude = Double.valueOf(((Element) doc.getElementsByTagName("West").item(0))
						.getChildNodes().item(i).getChildNodes().item(0).getNodeValue());

				if (maxLongitude > 180) {
					double minLongitude = Double.valueOf(((Element) doc.getElementsByTagName("East").item(0))
							.getChildNodes().item(i).getChildNodes().item(0).getNodeValue());
					context.getDateLineTrick()[i] = minLongitude + 180;
				}
			} catch (Exception e) {
				context.getDateLineTrick()[i] = 0;// par defaut pas de
				// translation pour pasage
				// de la date line
			}

			if (context.getTerrainMapped()[i].compareTo("true") == 0) {
				context.getMapped()[i] = true;
			} else {
				context.getMapped()[i] = false;
			}
		}

	}

	protected ElevationModel importElevationData(String displayName, File elevationSource, File fileStoreLocation,
			Context context, File sourceFile, IProgressMonitor monitor) {

		// Create a unique cache name which defines the FileStore path to the
		// imported elevations.
		CacheContext cacheContext = NasaCache.create(sourceFile, fileStoreLocation, context.name[0]);

		boolean askRecomputeCache = true;

		context.recomputeCache = NasaCache.cacheIsNull(cacheContext.getXmlParametersFile());

		if (askRecomputeCache) {
			switch (NasaCache.askForRecomputeCache(sourceFile, cacheContext)) {
			case NasaCache.TO_UPDATE:
				context.recomputeCache = true;
				NasaCache.delete(sourceFile);
				NasaCache.createHistoryXMLFile(cacheContext, sourceFile);
				break;
			case NasaCache.UP_TO_DATE:
				context.recomputeCache = false;
				break;
			default:
				logger.debug("Cache doesn't exist.");
				context.recomputeCache = true;
				break;
			}
		}

		MyTiledElevationProducer producer = new MyTiledElevationProducer(context.recomputeCache);

		// Create a parameter list which defines where the elevation data is
		// imported, the name associated with it,
		// and default extreme elevations for the ElevationModel using the
		// minimum and maximum elevations on
		// Earth.
		AVList params = new AVListImpl();
		params.setValue(AVKey.FILE_STORE_LOCATION, fileStoreLocation.getAbsolutePath());
		params.setValue(AVKey.DATA_CACHE_NAME, cacheContext.getCacheDirectory());
		params.setValue(AVKey.DATASET_NAME, displayName);
		params.setValue(AVKey.NAME, displayName);

		// Create a TiledElevationProducer to transforms the source elevation
		// data to a pyramid of elevation tiles in
		// the World Wind Java cache format.

		try {
			// Configure the TiledElevationProducer with the parameter list and
			// the elevation data source.
			producer.setStoreParameters(params);
			producer.offerDataSource(elevationSource, null);
			WorldWindUtils.manageProgression(producer, monitor);
			// Import the source elevation data into the FileStore by converting
			// it to the World Wind Java cache format.
			producer.startProduction();

			// layer.setLevels(producer.getLevels());
		} catch (IllegalArgumentException e) { // NNW encapsulate some errors in
			// an IllegalArgumentException
			producer.removeProductionState();
			Messages.openErrorMessage("Tiles production error", e.getMessage());
			return null;
		} catch (Exception e) {
			producer.removeProductionState();
			logger.error("An error occured during tiles production for file " + sourceFile, e);
			Messages.openErrorMessage("An error occured during tiles production for file " + sourceFile,
					"An error occured during tiles production for file see console for details");
			return null;
		}

		context.setLevelSet(producer.getLevels());

		// Extract the data configuration document from the production results.
		// If production sucessfully completed, the
		// TiledElevationProducer should always contain a document in the
		// production results, but we test the results
		// anyway.
		Iterable<?> results = producer.getProductionResults();
		if (results == null || results.iterator() == null || !results.iterator().hasNext()) {
			return null;
		}

		Object o = results.iterator().next();
		if (o == null || !(o instanceof Document)) {
			return null;
		}

		InterpolateElevationModel model = (InterpolateElevationModel) elevationModelFactory
				.createMovableInterpolateElevationModel(((Document) o).getDocumentElement());
		return model;
	}

	protected ShaderElevationLayer importSurfaceImage(File sourceFile, String displayName, String type,
			Object imageSource, File fileStoreLocation, String dataField, Context context) throws GUnknownException {
		// Create a unique cache name that specifies the FileStore path to the
		// imported image.
		String cacheName = guessNasaCacheFolder(displayName, type);

		// Create a parameter list that specifies where the image is imported
		// to, and the name associated with it.
		AVList params = new AVListImpl();
		params.setValue(AVKey.FILE_STORE_LOCATION, fileStoreLocation.getAbsolutePath());
		params.setValue(AVKey.DATA_CACHE_NAME, cacheName);
		params.setValue(AVKey.DATASET_NAME, displayName);

		File file_tex_xml = new File(fileStoreLocation.getPath() + "/" + cacheName + "/" + displayName + ".xml");
		if (!NasaCache.cacheIsNull(file_tex_xml)) {
			if (context.valMin == 0.0 && context.valMax == 0.0) {
				try {
					XMLCacheReader(file_tex_xml.getAbsolutePath(), context);
				} catch (ParserConfigurationException e) {
					logger.error("Error during XML cache read", e);
				} catch (SAXException e) {
					logger.error("Error during XML cache read", e);
				} catch (IOException e) {
					logger.error("Error during XML cache read", e);
				}
			}
		}

		context.recomputeCache = NasaCache.cacheIsNull(file_tex_xml);

		// Create a TiledImageProducer to transforms the source image to a
		// pyramid of images tiles in the World Wind
		// Java cache format.
		MyTiledImageProducer producer = new MyTiledImageProducer(dataField, context.unit, context.globalMin,
				context.globalMax, context.valMin, context.valMax);
		producer.setRecomputeCache(context.recomputeCache);

		try {
			// Configure the TiledImageProducer with the parameter list and the
			// image source.
			producer.setStoreParameters(params);
			producer.offerDataSource(imageSource, params);
			// Import the source image into the FileStore by converting it to
			// the World Wind Java cache format.
			producer.startProduction();
			/**
			 * update max and min values, in case of netcdf these are equals to min and max values availaibles
			 */
			context.globalMax = producer.getValGlobaleMax();
			context.globalMin = producer.getValGlobaleMin();
		} catch (Exception e) {
			producer.removeProductionState();
			logger.error("Error converting filestore" + " to World Wind cache format", e);
			throw new GUnknownException("Error converting filestore" + " to World Wind cache format", e);
		}

		// Extract the data configuration document from the production results.
		// If production sucessfully completed, the
		// TiledImageProducer should always contain a document in the production
		// results, but we test the results
		// anyway.
		Document result = null;
		Iterable<?> results = producer.getProductionResults();
		if (results != null && results.iterator() != null && results.iterator().hasNext()) {
			result = (Document) results.iterator().next();
		}

		if (result == null) {
			result = WWXML.openDocument(file_tex_xml);
		}

		ShaderElevationLayer layer = null;
		if (result != null) {
			Element domElement = result.getDocumentElement();
			AVList layerParams = DataConfigurationUtils.getLevelSetConfigParams(domElement, null);
			layer = new ShaderElevationLayer(sourceFile, result, new LevelSet(layerParams), context.type);

			// RGB data => shader disabled
			if (type.equals("RGB")) {
				layer.setIsRGB(true);
			}
			layer.setEnabled(true);
		}
		return layer;
	}

	@Override
	public List<Pair<String, String>> extensions() {
		List<Pair<String, String>> extensions = new ArrayList<>();
		// extensions.add(new Pair<>("GMT geographic grd (*" + GMTRasterAttributesFiller.FILE_EXTENSION + ")",
		// "*" + GMTRasterAttributesFiller.FILE_EXTENSION));

		return extensions;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param resource if this resource refers to a known {@link Layer} or {@link ElevationModel} instance it look for
	 *            the matching icon, otherwise it returns default elevation icon.
	 */
	@Override
	public String getImage(Object resource) {
		if (resource instanceof ElevationModel) {
			return "icons/16/elevation.png";
		} else if (resource instanceof Layer) {
			return "icons/16/texture.png";
		}
		return "icons/16/dtm16.png";// dtmDriver.getImage(resource);
	}

	// public LayerStore initData(String resource, IProgressMonitor monitor) throws GIOException {
	// if (resource == null) {
	// return null;
	// }
	// //FileInfoNode node = IProjectExplorerService.grab().findInfoStore(resource, false);
	//// if (node != null) {
	//// IFileInfo is = node.getFileInfo();
	////
	//// if (is instanceof LayerStore) {
	//// return (LayerStore) is;
	//// }
	//// }
	//
	// File f = new File(resource);
	// try {
	// List<Pair<Layer, ElevationModel>> data = load(f, monitor);
	// if (data != null && !data.isEmpty()) {
	// // if (f.getName().endsWith(DtmDriver.EXTENSION_DTM1) ||
	// // f.getName().endsWith(DtmDriver.EXTENSION_DTM2)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM3)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM4)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM5)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM6)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM7)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM8)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM9)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM11)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM12)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM13)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM14)
	// // || f.getName().endsWith(DtmDriver.EXTENSION_DTM15)) {
	// // DtmStore store = new DtmStore(this, f, data);
	// // if (store.getDtmInfo() != null) {
	// // readDTMFile(store);
	// // return store;
	// // } else {
	// // return null;
	// // }
	// // }
	// if (f.getName().endsWith(GMTRasterAttributesFiller.FILE_EXTENSION)) {
	// GRDStore store = new GRDStore(f, data);
	// readGRDFile(store);
	// return store;
	// } else {
	// return new LayerStore(f, data);
	// }
	// }
	// } catch (Exception e) {
	// Messages.openErrorMessage("Error loading " + resource, e);
	// }
	//
	// return null;
	// }

	// private void readGRDFile(GRDStore store) throws GException {
	// if (store.getPath() == null) {
	// return;
	// }
	//
	// RasterAttributes terrainAttributes = new RasterAttributes(store.getPath());
	// GMTRasterAttributesFiller bf = new GMTRasterAttributesFiller(terrainAttributes);
	// bf.fill();
	//
	// store.setNbLines(terrainAttributes.getLines());
	// store.setNbColumns(terrainAttributes.getColumns());
	//
	// // store.setGeoBox(new GeoBox(bf.getNorthLatitude(),
	// // bf.getSouthLatitude(), bf.getEastLongitude(),
	// // bf.getWestLongitude()));
	// store.setGeoBox(terrainAttributes.getGeoBox());
	//
	// terrainAttributes.dispose();
	// }

	// private void readDTMFile(DtmStore store) {
	// if (store.getPath() == null) {
	// return;
	// }
	//
	// NetcdfFile ncfile = null;
	// try {
	// ncfile = NetcdfFile.open(store.getPath());
	// store.setNbLines(ncfile.findDimension(IDtmConstants.DIM_LINES).getLength());
	// store.setNbColumns(ncfile.findDimension(IDtmConstants.DIM_COLUMNS).getLength());
	//
	// if (store != null) {
	// initializeDTMProperties(ncfile, store);
	//
	// store.setGeoBox(new GeoBox(
	// ncfile.findGlobalAttribute(IDtmConstants.NorthLatitude.getValue()).getNumericValue()
	// .floatValue(),
	// ncfile.findGlobalAttribute(IDtmConstants.SouthLatitude.getValue()).getNumericValue()
	// .floatValue(),
	// ncfile.findGlobalAttribute(IDtmConstants.EastLongitude.getValue()).getNumericValue()
	// .floatValue(),
	// ncfile.findGlobalAttribute(IDtmConstants.WestLongitude.getValue()).getNumericValue()
	// .floatValue()));
	// }
	// } catch (Exception e) {
	// logger.error("Error during netCDF reading", e);
	// } finally {
	// if (null != ncfile) {
	// try {
	// ncfile.close();
	// } catch (IOException ioe) {
	// logger.error("trying to close " + store.getPath(), ioe);
	// }
	// }
	// }
	// }

	// @SuppressWarnings("deprecation")
	// private void initializeDTMProperties(NetcdfFile ncfile, DtmStore store) {
	// if (store == null || ncfile == null) {
	// return;
	// }
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.Sounder.getValue())) {
	// store.setEchoSounderModel(SoundersLibrary.get()
	// .getFirstSounderDescription(
	// ncfile.findGlobalAttribute(IDtmConstants.Sounder.getValue()).getNumericValue().shortValue())
	// .getName());
	// }
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.ProjType.getValue())) {
	// store.setProjType(
	// ncfile.findGlobalAttribute(IDtmConstants.ProjType.getValue()).getNumericValue().intValue());
	// }
	//
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.EllipsoidName.getValue())) {
	// store.setEllipsoidName(
	// ncfile.findGlobalAttribute(IDtmConstants.EllipsoidName.getValue()).getStringValue().toString());
	// }
	//
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.EllipsoidA.getValue())) {
	// double a =
	// ncfile.findGlobalAttribute(IDtmConstants.EllipsoidA.getValue()).getNumericValue().doubleValue();
	// store.setEllipsoidA(a);
	// }
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.EllipsoidInvF.getValue())) {
	// double fl =
	// ncfile.findGlobalAttribute(IDtmConstants.EllipsoidInvF.getValue()).getNumericValue()
	// .doubleValue();
	// store.setEllipsoidFlatness(fl);
	// }
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.EllipsoidE2.getValue())) {
	// double e2 =
	// ncfile.findGlobalAttribute(IDtmConstants.EllipsoidE2.getValue()).getNumericValue()
	// .doubleValue();
	// store.setEllipsoidE2(e2);
	// }
	//
	// if (NcUtils.containsGlobalAttribute(ncfile, IDtmConstants.Xmin.getValue())) {
	// double xMin =
	// ncfile.findGlobalAttribute(IDtmConstants.Xmin.getValue()).getNumericValue().doubleValue();
	// store.setXMin(xMin);
	// }
	// if (NcUtils.containsGlobalAttribute(ncfile, IDtmConstants.Ymin.getValue())) {
	// double yMin =
	// ncfile.findGlobalAttribute(IDtmConstants.Ymin.getValue()).getNumericValue().doubleValue();
	// store.setYMin(yMin);
	// }
	//
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.SpatialResolutionX.getValue())) {
	// double xSize =
	// ncfile.findGlobalAttribute(IDtmConstants.SpatialResolutionX.getValue()).getNumericValue()
	// .doubleValue();
	// store.setXSize(xSize);
	// }
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.SpatialResolutionY.getValue())) {
	// double ySize =
	// ncfile.findGlobalAttribute(IDtmConstants.SpatialResolutionY.getValue()).getNumericValue()
	// .doubleValue();
	// store.setYSize(ySize);
	// }
	//
	// if (NcUtils.containsGlobalAttribute(ncfile,
	// IDtmConstants.ProjectionParametersCodes.getValue())) {
	//
	// String readParamCodes =
	// ncfile.findGlobalAttribute(IDtmConstants.ProjectionParametersCodes.getValue())
	// .getStringValue();
	// String paramCodes[] = readParamCodes.split(",");
	// Array readParamValues =
	// ncfile.findGlobalAttribute(IDtmConstants.ProjectionParametersValues.getValue())
	// .getValues();
	// Class<?> a = readParamValues.getElementType();
	// if (a == double.class) {
	// double paramValues[] = new double[(int) readParamValues.getSize()];
	// int nb = 0;
	// while (readParamValues.hasNext()) {
	// paramValues[nb++] = readParamValues.nextDouble();
	// }
	// store.setProjParams(paramCodes, paramValues);
	//
	// }
	// }
	//
	// for (Variable v : ncfile.getVariables()) {
	// for (Dimension d : v.getDimensions()) {
	// if (d.getName().equals(IDtmConstants.DIM_LINES)) {
	// for (Dimension dd : v.getDimensions()) {
	// if (dd.getName().equals(IDtmConstants.DIM_COLUMNS)) {
	// store.addLayer(v.getFullName());
	// }
	// }
	// }
	// }
	// }
	//
	// store.setHistory(readHistory(ncfile));
	//
	// }

	// private String readHistory(NetcdfFile ncFile) {
	// // NbrHistoryRec
	// StringBuilder sb = new StringBuilder();
	// Number n =
	// ncFile.findGlobalAttribute(IDtmConstants.NbrHistoryRec.getValue()).getNumericValue();
	// if (n != null) {
	// try {
	// int size = n.intValue();
	// int dateOffset = 0;
	// Array arrDate = null;
	// Array arrTime = null;
	// String[] authors = null;
	// Variable vdate = ncFile.findVariable("mbHistDate");
	// Variable vtime = ncFile.findVariable("mbHistTime");
	// Variable vauthor = ncFile.findVariable("mbHistAutor");
	// if (vdate != null) {
	// Attribute att = vdate.findAttribute("add_offset");
	// if (att != null) {
	// dateOffset = att.getNumericValue().intValue();
	// }
	// arrDate = vdate.read();
	// }
	// if (vtime != null) {
	// arrTime = vtime.read();
	// }
	// if (vauthor != null) {
	// Array arrAuthor = vauthor.read();
	// if (arrAuthor != null) {
	// String s = arrAuthor.toString();
	// authors = s.split(",");
	// }
	// }
	// // ncfile.write("mbHistDate", date);
	// // ncfile.write("mbHistTime", time);
	// // ncfile.writeStringData("mbHistAutor", name);
	// if (arrDate != null && arrTime != null && authors != null) {
	// for (int i = 0; i < size; i++) {
	// int date = dateOffset + arrDate.getInt(i);
	// int time = arrTime.getInt(i);
	// sb.append(dateToString(date, time));
	// if (i < authors.length) {
	// sb.append(",\t");
	// sb.append(authors[i]);
	// }
	// if (i + 1 < size) {
	// sb.append("\n");
	// }
	// }
	// }
	// } catch (IOException e) {
	// logger.error("Error reading MBG history", e);
	// }
	// }
	// return sb.toString();
	// }

	private String dateToString(int date, int time) {
		long referenceDate = 2440588;
		long refdate = (date - referenceDate) * 24 * 3600 * 1000;
		refdate += time;
		Date julian = new Date(refdate);

		final DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		return formatter.format(julian);
	}

	@Override
	public List<Property<?>> getProperties(String ressource) {
		// if (ressource.endsWith(".dtm")) {
		// return dtmDriver.getProperties(ressource);
		// }
		File f = new File(ressource);
		List<Property<?>> properties = new ArrayList<>();
		properties.add(Property.build(IConstants.Name, f.getName()));
		properties.add(Property.build(IConstants.Path, f.getAbsolutePath()));
		return properties;
	}

	@Override
	public int getPriority() {
		return 90;
	}

	/* @formatter:off
	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	@formatter:on */
	@Override
	public ContentType getContentType(String resource) {
		if (accept(resource)) {
			return resource.endsWith(EXTENSION1) ? ContentType.OLD_ELEVATION_XML : ContentType.UNDEFINED;
		}
		return ContentType.UNDEFINED;
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.OLD_ELEVATION_XML);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(getImage(null), getClass()));
	}

	@Override
	public List<String> getExtensions() {
		// return Collections.singletonList(GMTRasterAttributesFiller.FILE_EXTENSION);
		return List.of();
	}

	@Override
	public List<String> getExtensions(ContentType contentType) {
		// return Collections.singletonList(GMTRasterAttributesFiller.FILE_EXTENSION);
		return List.of();
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Arrays.asList(//
				// new Pair<>("GMT geographic grd (*" + GMTRasterAttributesFiller.FILE_EXTENSION + ")",
				// "*" + GMTRasterAttributesFiller.FILE_EXTENSION), //
				new Pair<>("DTM (*.dtm)", "*.dtm") //
		);
	}

	@Override
	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		return Optional
				.ofNullable(accept(filePath) ? new BridgeFileInfo(filePath, getContentType(filePath), this) : null);
	}

}
