/*
 * @License@
 */
package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.impl.BasicNavigationData;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.ANavLine;
import fr.ifremer.globe.ui.NavTimedLine;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.IDataDriver;
import fr.ifremer.viewer3d.model.NavData;
import fr.ifremer.viewer3d.model.echoes.StorageType;
import fr.ifremer.viewer3d.model.navigation.Dimensions;
import fr.ifremer.viewer3d.model.navigation.Line;
import fr.ifremer.viewer3d.model.navigation.Nav;
import fr.ifremer.viewer3d.model.navigation.Signals;
import fr.ifremer.viewer3d.util.ZipUtils;
import fr.ifremer.viewer3d.util.binary.BinaryUtil;
import fr.ifremer.viewer3d.util.binary.BufferIterator;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Loader for navigation files.
 * <p>
 * Loads data definition from navigation XML definition file and associated binary files.
 * </p>
 *
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@Component(name = "globe_view3d_loaders_navigationloader", service = { IDataDriver.class, IIcon16ForFileProvider.class,
		IFileInfoSupplier.class })
@Deprecated
public class NavigationLoader implements ILoader, //
		IDataDriver, // BRIDGE !
		IIcon16ForFileProvider, IFileInfoSupplier<BridgeFileInfo> {

	/**
	 * Extension for echoes definition files. Value : {@value}
	 */
	public static final String EXTENSION = ".xml"; //$NON-NLS-1$

	/**
	 * Name of the XML element for points per line count. Value : {@value}
	 */
	public static final String NB_POINTS_NAME = "nbPoints"; //$NON-NLS-1$

	/**
	 * Name for the longitude XML item. Value : {@value}
	 */
	public static final String LONGITUDE_NAME = "Longitude"; //$NON-NLS-1$

	/**
	 * Name for the latitude XML item. Value : {@value}
	 */
	public static final String LATITUDE_NAME = "Latitude"; //$NON-NLS-1$

	/**
	 * Name for the depth XML item. Value : {@value}
	 */
	public static final String DEPTH_NAME = "Depth"; //$NON-NLS-1$

	/**
	 * Name for the navigation file. Value : {@value}
	 */
	public static final String NAME = "name"; //$NON-NLS-1$

	/**
	 * Software info. Value : {@value}
	 */
	public static final String SOFTWARE = "Software"; //$NON-NLS-1$

	/*
	 * Add M.antoine 20/08/2010
	 */
	/**
	 * Name for the date XML item. Value : {@value}
	 */
	private static final String DATE_NAME = "Date"; //$NON-NLS-1$

	/**
	 * Name for the hour XML item. Value : {@value}
	 */
	private static final String HOUR_NAME = "Hour"; //$NON-NLS-1$

	/*
	 * End add M.antoine 20/08/2010
	 */

	/**
	 * Collada info. Value : {@value}
	 */
	public static final String COLLADA = "Collada"; //$NON-NLS-1$

	/**
	 * Logger for this class.
	 */
	private static Logger logger = LoggerFactory.getLogger(NavigationLoader.class);

	private static List<Pair<String, String>> extensions = new ArrayList<>();

	static {
		extensions.add(new Pair<>("Navigation (*.xml)", "*" + EXTENSION));
	}

	/**
	 * Current navigation data for this layer.
	 */
	private NavData navData = null;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileNameExtensionFilter getFileFilter() {
		return new FileNameExtensionFilter(DicoBundle.getString("KEY_NAVIGATIONLOADER_FILTER_MESSAGE"), //$NON-NLS-1$
				EXTENSION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean accept(String resource) {
		File file = new File(resource);
		if (file != null) {
			String name = file.getName();
			if (name != null && name.endsWith(EXTENSION)) {

				// TODO use xsd validation

				try {
					JAXBContext jc = JAXBContext.newInstance(Nav.class.getPackageName(), Nav.class.getClassLoader());
					Unmarshaller u = jc.createUnmarshaller();
					FileInputStream fis = new FileInputStream(file);
					Object obj = u.unmarshal(fis);
					if (obj != null && obj instanceof Nav) {
						return true;
					}
				} catch (Exception e) {
					logger.warn("Jaxb error : {}", e.getMessage());
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor monitor) throws Exception {
		if (file == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_NAVIGATIONLOADER_ERROR_MESSAGE")); //$NON-NLS-1$
		}

		navData = loadBinaryNavFile(file, monitor);

		List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(1);
		List<ANavLine<? extends Position>> lines = navData.getLines();

		for (ANavLine<? extends Position> line : lines) {
			INavigationData navigationData = new BasicNavigationData(line.getName(), line.size(),
					i -> line.getPoints().get(i).getLongitude().degrees,
					i -> line.getPoints().get(i).getLatitude().degrees);
			IWWNavigationLayer navigationLayer = IWWLayerFactory.grab()
					.createNavigationLayer(accessMode -> navigationData);
			navigationLayer.setShortName(line.getName());

			layers.add(new Pair<Layer, ElevationModel>(navigationLayer, null));
		}

		List<Pair<Layer, ElevationModel>> lp = new ArrayList<>();

		layers.addAll(lp);

		return layers;
	}

	/**
	 * Load echoes from binary data files.
	 *
	 * @param inputFile the input XML "index" file
	 * @return loaded plume bean or null if an error occurs
	 * @throws IOException if an exception occurs during file access
	 * @throws JAXBException if an exception occurs during XML parsing
	 */
	private NavData loadBinaryNavFile(File inputFile, IProgressMonitor monitor) throws JAXBException, GIOException {

		JAXBContext jc = JAXBContext.newInstance(Nav.class.getPackageName(), Nav.class.getClassLoader());
		Unmarshaller u = jc.createUnmarshaller();

		NavData data = null;

		FileInputStream fis;
		try {
			fis = new FileInputStream(inputFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			throw new GIOException("File not found", e);
		}
		try {
			Object obj = u.unmarshal(fis);
			if (obj != null && obj instanceof Nav) {
				Nav nav = (Nav) obj;

				// Test présence des données
				File parent = inputFile.getParentFile();
				File itemFileName = new File(
						parent.getAbsolutePath() + "/" + nav.getLine().get(0).getSignals().get(0).getFileName());
				File dataFile = itemFileName.getParentFile(); // data
				inputFile = ZipUtils.unzipDataInCache(inputFile, dataFile);

				data = new NavData();
				data.setFile(inputFile);
				data.setName(nav.getName());
				data.setSoftware(nav.getSoftware());
				data.setCollada(nav.getCollada());

				// if at least one line
				List<Line> lines = nav.getLine();
				if (lines != null) {
					for (Line line : lines) {
						// retrieve number of point for this line
						int nbPoints = 0;
						Dimensions dim = line.getDimensions();
						if (dim != null) {
							for (Object o : dim.getContent()) {
								if (nbPoints == 0 && o instanceof JAXBElement<?>) {
									JAXBElement<?> elt = (JAXBElement<?>) o;
									if (elt != null && NB_POINTS_NAME.equals(elt.getName().toString())) {
										Object value = elt.getValue();
										if (value != null) {
											try {
												nbPoints = Integer.valueOf(value.toString());
											} catch (NumberFormatException nfe) {
												logger.warn(DicoBundle
														.getString("KEY_NAVIGATIONLOADER_ERROR_MESSAGE_ECHOES") //$NON-NLS-1$
														+ value.toString());
											}
										}
									}
								}
							}
						}

						List<Signals> signals = line.getSignals();
						if (signals != null) {
							BufferIterator<Double> latIt = null;
							BufferIterator<Double> lonIt = null;
							BufferIterator<Double> depthIt = null;
							/*
							 * Add M.antoine 20/08/2010
							 */
							BufferIterator<Double> dateIt = null;
							BufferIterator<Double> hourIt = null;
							/*
							 * end add M.antoine 20/08/2010
							 */

							parent = FileUtils.getParent(inputFile);
							String basePath = parent.getAbsolutePath() + "/"; //$NON-NLS-1$
							for (Signals item : signals) {
								String filename = item.getFileName().trim();
								// replace backslashes by slashes for
								// portability
								filename = filename.replaceAll("\\\\([^ ])", "/$1");

								File file = new File(basePath + filename);

								data.addBinaryFile(file);

								// add data directory if it doesn't exist yet

								// find parent directory
								int idx = filename.indexOf("/");
								int idx2 = filename.indexOf("\\");
								if (idx == -1 || idx2 != -1 && idx2 < idx) {
									idx = idx2;
								}
								if (idx != -1) {
									File directory = new File(basePath + filename.substring(0, idx));
									if (!data.getBinaryFiles().contains(directory)) {
										data.addBinaryFile(directory);
									}
								}

								// read binary data
								StorageType type = item.getStorage();
								if (LONGITUDE_NAME.equals(item.getName())) {
									// retrieve longitudes
									lonIt = BinaryUtil.readBinFile(file, type);
								} else if (LATITUDE_NAME.equals(item.getName())) {
									// retrieve latitudes
									latIt = BinaryUtil.readBinFile(file, type);
								} else if (DEPTH_NAME.equals(item.getName())) {
									// retrieve latitudes
									depthIt = BinaryUtil.readBinFile(file, type);
								}
								/*
								 * Add M.antoine 20/08/2010
								 */
								else if (DATE_NAME.equals(item.getName())) {
									dateIt = BinaryUtil.readBinFile(file, type == null ? StorageType.DOUBLE : type);
								} else if (HOUR_NAME.equals(item.getName())) {
									hourIt = BinaryUtil.readBinFile(file, type == null ? StorageType.DOUBLE : type);
								}
								/*
								 * end add M.antoine 20/08/2010
								 */
								else if (COLLADA.equals(item.getName())) {

								}

							}
							/*
							 * Add M.antoine 20/08/2010
							 */
							if (dateIt == null || dateIt.size() != nbPoints) {
								logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_DATE")); //$NON-NLS-1$
							}
							if (hourIt == null || hourIt.size() != nbPoints) {
								logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_HOUR")); //$NON-NLS-1$
							}
							/*
							 * end add M.antoine 20/08/2010
							 */

							// should check iterators
							if (latIt == null) {
								logger.warn("Unable to read latitude");
							} else if (lonIt == null) {
								logger.warn("Unable to read longitude");
							} else {
								NavTimedLine navLine = new NavTimedLine();
								navLine.setName(line.getName());
								navLine.setCollada(nav.getCollada());
								for (int i = 0; i < nbPoints; i++) {
									Double lat = latIt.get();
									Double lon = lonIt.get();
									Double alt = 0.0;
									if (depthIt != null) {
										alt = depthIt.get();
									}
									LatLon ll = LatLon.fromDegrees(lat, lon);
									Double date = dateIt.get();
									Double hour = 0.0;
									if (hourIt != null) {
										hour = hourIt.get();
									}
									navLine.addPointTime(new Position(ll, alt), date.doubleValue(), hour.doubleValue());

								}

								data.addLine(navLine);
							}
						}
					}
				}
			}

			// now sort data
			data.sort();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				throw new GIOException("Error close file ", e);
			}
		}

		monitor.done();
		return data;
	}

	/**
	 * Current navigation data for this loader.
	 */
	public NavData getNavData() {
		return navData;
	}

	@Override
	public List<Pair<String, String>> extensions() {
		return extensions;
	}

	@Override
	public String getImage(Object resource) {
		return "icons/16/navigation.png";
	}

	@Override
	public int getPriority() {
		return DEFAULT_PRIORITY;
	}

	/*
	 * @formatter:off
 	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	 * @formatter:on
	 */
	@Override
	public ContentType getContentType(String resource) {
		return accept(resource) ? ContentType.OLD_NAV_XML : ContentType.UNDEFINED;
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.OLD_NAV_XML);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(getImage(null), getClass()));
	}

	@Override
	public List<String> getExtensions() {
		return Collections.singletonList("xml");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return extensions();
	}

	@Override
	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		return Optional
				.ofNullable(accept(filePath) ? new BridgeFileInfo(filePath, getContentType(filePath), this) : null);
	}

}
