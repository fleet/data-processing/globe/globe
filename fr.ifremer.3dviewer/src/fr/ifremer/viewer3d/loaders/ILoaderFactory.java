package fr.ifremer.viewer3d.loaders;

import java.io.File;

/**
 * Factory used to instantiate loaders. One loader is usually dedicated to one file ; so you must instantiate a new loader for each file to load.
 *  
 * @author apside
 */
public interface ILoaderFactory<T extends ILoader> {

	/**
	 * Creates a new loader for the file. The loader is assumed to accept the file given as parameter.
	 * 
	 * @param file File to load.
	 * @return A new loader instance.
	 * 
	 * @see ILoader#accept(String)
	 */
	T newLoaderInstance(File file);
}
