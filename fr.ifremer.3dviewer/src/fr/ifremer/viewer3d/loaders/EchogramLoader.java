/*
 * @License@
 */
package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Formatter;
import java.util.List;
import java.util.Optional;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.IDataDriver;
import fr.ifremer.viewer3d.deprecated.JulianDay;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
//import fr.ifremer.viewer3d.layers.WaterColumnLayer;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.model.echograms.Dimensions;
import fr.ifremer.viewer3d.model.echograms.Item;
import fr.ifremer.viewer3d.model.echograms.PolarEchograms;
import fr.ifremer.viewer3d.model.echograms.Signals;
import fr.ifremer.viewer3d.model.watercolumn.WaterColumnData;
import fr.ifremer.viewer3d.util.XSDValidator;
import fr.ifremer.viewer3d.util.ZipUtils;
import fr.ifremer.viewer3d.util.binary.BinaryUtil;
import fr.ifremer.viewer3d.util.binary.BufferIterator;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.util.Logging;

/**
 * Loader for echograms files.
 *
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@Component(name = "globe_view3d_loaders_echogramloader", service = { IDataDriver.class, IIcon16ForFileProvider.class,
		IFileInfoSupplier.class })
public class EchogramLoader implements ILoader, //
		IDataDriver, // BRIDGE !
		IIcon16ForFileProvider, IFileInfoSupplier<BridgeFileInfo> {

	private static final String EXTENSION = "xml";

	/** XML schema (XSD) file name for this loader input data. */
	public static final String SCHEMA_FILENAME = "/echograms-1.0.xsd";

	/** Name of the XML element for pings' number. {@value} */
	private static final String NB_PINGS_NAME = "nbPings"; //$NON-NLS-1$

	/** Name for the port longitude XML item. {@value} */
	private static final String LONGITUDE_PORT_NAME = "lonBab"; //$NON-NLS-1$

	/** Name for the port latitude XML item. {@value} */
	private static final String LATITUDE_PORT_NAME = "latBab"; //$NON-NLS-1$

	/** Name for the starboard longitude XML item. {@value} */
	private static final String LONGITUDE_STARBOARD_NAME = "lonTri"; //$NON-NLS-1$

	/** Name for the starboard latitude XML item. {@value} */
	private static final String LATITUDE_STARBOARD_NAME = "latTri"; //$NON-NLS-1$

	/** Name for the center longitude XML item. {@value} */
	private static final String LONGITUDE_CENTER_NAME = "lonCentre"; //$NON-NLS-1$

	/** Name for the center latitude XML item. {@value} */
	private static final String LATITUDE_CENTER_NAME = "latCentre"; //$NON-NLS-1$

	/*
	 * Add M.antoine 12/04/2010
	 */
	/** Name for the date XML item. {@value} */
	private static final String DATE_NAME = "Date"; //$NON-NLS-1$

	/** Name for the hour XML item. {@value} */
	private static final String HOUR_NAME = "Hour"; //$NON-NLS-1$

	/*
	 * End add M.antoine 12/04/2010
	 */

	/**
	 * Name for the altitude XML item. It matches echogram's bottom elevation. * *
	 * {@value}
	 */
	private static final String ALT_NAME = "Depth"; //$NON-NLS-1$

	/**
	 * Name for the immersion XML item. It matches echogram's top elevation. * * *
	 * {@value}
	 */
	private static final String IMMERSION_NAME = "Immersion"; //$NON-NLS-1$

	/** Name for the ping index XML item.{@value} */
	private static final String INDEX_NAME = "iPing"; //$NON-NLS-1$

	/** Logger for this class. */
	private static Logger logger = LoggerFactory.getLogger(EchogramLoader.class);

	private static List<Pair<String, String>> extensions = new ArrayList<>();

	static {
		extensions.add(new Pair<>("Polar echograms (*.xml)", "*." + EXTENSION));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileNameExtensionFilter getFileFilter() {
		return new FileNameExtensionFilter(DicoBundle.getString("KEY_WATERCOLUMNFILEFILTER_MESSAGE"), "txt", EXTENSION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean accept(String resource) {
		if (resource == null || !resource.endsWith(EXTENSION)) {
			return false;
		}

		File file = new File(resource);
		if (file != null) {
			String name = file.getName();
			if (name != null) {
				try {
					FileInputStream fis = new FileInputStream(file);
					InputStream schema = getClass().getResourceAsStream(SCHEMA_FILENAME);
					logger.info("Schema is :" + SCHEMA_FILENAME);
					return XSDValidator.valid(fis, schema);
				} catch (FileNotFoundException e) {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor progress) throws Exception {
		if (file == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_ECHOGRAMLOADER_ERROR_MESSAGE")); //$NON-NLS-1$
		}

		SubMonitor subMonitor = SubMonitor.convert(progress, 100);

		PolarEchograms polarEchograms = readXmlFile(file);
		subMonitor.worked(10);

		if (polarEchograms != null) {

			// Test présence des données
			File parent = FileUtils.getParent(file);
			File item = new File(
					parent.getAbsolutePath() + "/" + polarEchograms.getSignals().getItem().get(0).getFileName());
			File dataFile = item.getParentFile(); // data
			file = ZipUtils.unzipDataInCache(file, dataFile);

			WaterColumnData wcd = new WaterColumnData(file);

			wcd.getInfos().addInfo("Name", polarEchograms.getName());
			wcd.getInfos().addInfo("Survey", polarEchograms.getSurvey());
			wcd.getInfos().addInfo("Vessel", polarEchograms.getVessel());
			wcd.getInfos().addInfo("Sounder", polarEchograms.getSounder());
			wcd.getInfos().addInfo("Chief scientist", polarEchograms.getChiefScientist());
			wcd.getInfos().addInfo("Tag", polarEchograms.getDataTag());
			wcd.getInfos().addInfo("Software", polarEchograms.getSoftware());
			wcd.getInfos().addInfo("Extracted from", polarEchograms.getExtractedFrom());
			wcd.getInfos().addInfo("Format version", String.valueOf(polarEchograms.getFormatVersion()));
			wcd.getInfos().addInfo("Export directory", polarEchograms.getNomDirExport());
			wcd.getInfos().addInfo("Kml Name", polarEchograms.getKmlName());
			subMonitor.worked(10);

			String beanName = file.getName();
			PlayerBean playerBean = new PlayerBean(beanName);
			WaterColumnLayer layer = new WaterColumnLayer(playerBean, wcd);
			subMonitor.worked(10);

			loadBinaryEchogramData(file, layer, polarEchograms, subMonitor.split(60));
			layer.initialize();

			List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(1);
			layers.add(new Pair<Layer, ElevationModel>(layer, null));
			return layers;

		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public void fullLoad(File file, WaterColumnLayer layer) throws Exception {
		if (file != null && layer != null) {
			PolarEchograms polarEchograms = readXmlFile(file);
			loadBinaryEchogramData(file, layer, polarEchograms, null);
		}
	}

	/**
	 * Read water column xml data file.
	 *
	 * @param waterColumnFile the input file
	 */
	public PolarEchograms readXmlFile(File waterColumnFile) {
		if (waterColumnFile == null) {
			logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_WC_FILE_NULL_MESSAGE")); //$NON-NLS-1$
			return null;
		}
		if (!waterColumnFile.exists()) {
			logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_WC_FILE_NOT_FOUND_MESSAGE") //$NON-NLS-1$
					+ waterColumnFile.getAbsolutePath());
			return null;
		}

		try {
			JAXBContext jc = JAXBContext.newInstance(PolarEchograms.class.getPackageName(),
					PolarEchograms.class.getClassLoader());
			Unmarshaller u = jc.createUnmarshaller();

			FileInputStream fis = new FileInputStream(waterColumnFile);
			try {
				Object obj = u.unmarshal(fis);
				if (obj != null && obj instanceof PolarEchograms) {
					return (PolarEchograms) obj;
				}
			} finally {
				fis.close();
			}
		} catch (FileNotFoundException e) {
			String message = Logging.getMessage("generic.FileNotFound", //$NON-NLS-1$
					waterColumnFile.getName());
			Logging.logger().severe(message);
		} catch (IOException e) {
			String message = Logging.getMessage(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_PROB_PARSING_MESSAGE"), //$NON-NLS-1$
					waterColumnFile.getName());
			Logging.logger().severe(message);
		} catch (JAXBException e) {
			logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_JAXB_EXCEPTION_MESSAGE") //$NON-NLS-1$
					+ e.getMessage());
		}

		return null;
	}

	/**
	 * Loads all binary data and images.
	 * {@link #loadBinaryEchogramData(File, WaterColumnLayer, PolarEchograms)}
	 *
	 * @param waterColumnFile input file
	 * @param wcd             data to fill
	 * @param echos           echograms to display
	 * @throws IOException
	 */
	private void loadBinaryEchogramData(File waterColumnFile, WaterColumnLayer layer, PolarEchograms echos,
			IProgressMonitor progress) throws GIOException {
		if (echos == null) {
			return;
		}

		// retrieve number of pings
		int nbPings = 0;
		Dimensions dim = echos.getDimensions();
		if (dim != null) {
			for (Object o : dim.getContent()) {
				if (nbPings == 0 && o instanceof JAXBElement<?>) {
					JAXBElement<?> elt = (JAXBElement<?>) o;
					if (elt != null && NB_PINGS_NAME.equals(elt.getName().toString())) {
						Object value = elt.getValue();
						if (value != null) {
							try {
								nbPings = Integer.valueOf(value.toString());
							} catch (NumberFormatException nfe) {
								logger.warn(
										DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_PARSE_PINGS_IMPOSSIBLE_MESSAGE") //$NON-NLS-1$
												+ value.toString());
							}
						}
					}
				}
			}
		}

		// if at least one ping
		if (nbPings > 0) {
			Signals signals = echos.getSignals();
			if (signals != null) {
				BufferIterator<Double> lonPortIt = null;
				BufferIterator<Double> latPortIt = null;
				BufferIterator<Double> lonStarIt = null;
				BufferIterator<Double> latStarIt = null;
				BufferIterator<Double> latCenterIt = null;
				BufferIterator<Double> lonCenterIt = null;
				/*
				 * Add M.antoine 12/04/2010
				 */
				BufferIterator<Double> dateIt = null;
				BufferIterator<Double> hourIt = null;
				/*
				 * end add M.antoine 12/04/2010
				 */
				BufferIterator<Double> altIt = null;
				BufferIterator<Double> immersionIt = null;
				BufferIterator<Double> indexIt = null;

				// png directory
				String imgDir = null;

				File parent = FileUtils.getParent(waterColumnFile);
				String basePath = parent.getAbsolutePath() + "/"; //$NON-NLS-1$
				for (Item item : signals.getItem()) {
					if (imgDir == null) {
						String name = item.getFileName();
						if (name != null) {
							String[] str = name.split("\\\\|/"); //$NON-NLS-1$
							if (str != null && str.length > 0) {
								imgDir = str[0];
							}
						}
					}

					String filename = item.getFileName().trim();
					// replace backslashes by slashes for portability
					filename = filename.replaceAll("\\\\([^ ])", "/$1");
					File file = new File(basePath + filename);

					if (LONGITUDE_PORT_NAME.equals(item.getName())) {
						lonPortIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (LATITUDE_PORT_NAME.equals(item.getName())) {
						latPortIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (LONGITUDE_STARBOARD_NAME.equals(item.getName())) {
						lonStarIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (LATITUDE_STARBOARD_NAME.equals(item.getName())) {
						latStarIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (LONGITUDE_CENTER_NAME.equals(item.getName())) {
						lonCenterIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (LATITUDE_CENTER_NAME.equals(item.getName())) {
						latCenterIt = BinaryUtil.readBinFile(file, item.getStorage());
						/*
						 * Add M.antoine 12/04/2010
						 */
					} else if (DATE_NAME.equals(item.getName())) {
						dateIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (HOUR_NAME.equals(item.getName())) {
						hourIt = BinaryUtil.readBinFile(file, item.getStorage());
						/*
						 * end add M.antoine 12/04/2010
						 */

					} else if (ALT_NAME.equals(item.getName())) {
						altIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (IMMERSION_NAME.equals(item.getName())) {
						immersionIt = BinaryUtil.readBinFile(file, item.getStorage());
					} else if (INDEX_NAME.equals(item.getName())) {
						indexIt = BinaryUtil.readBinFile(file, item.getStorage());
					}
				}

				if (latPortIt == null || latPortIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_PORT_LATITUDE")); //$NON-NLS-1$
				}
				if (lonPortIt == null || lonPortIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_PORT_LONGITUDE")); //$NON-NLS-1$
				}
				if (latStarIt == null || latStarIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_STARBOARD_LATITUDE")); //$NON-NLS-1$
				}
				if (lonStarIt == null || lonStarIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_STARBOARD_LONGITUDE")); //$NON-NLS-1$
				}
				if (latCenterIt == null || latStarIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_CENTER_LATITUDE")); //$NON-NLS-1$
				}
				if (lonCenterIt == null || lonStarIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_CENTER_LONGITUDE")); //$NON-NLS-1$
				}
				/*
				 * Add M.antoine 12/04/2010
				 */
				if (dateIt == null || dateIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_DATE")); //$NON-NLS-1$
				}
				if (hourIt == null || hourIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_HOUR")); //$NON-NLS-1$
				}
				/*
				 * end add M.antoine 12/04/2010
				 */

				if (altIt == null || altIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_ELEVATION")); //$NON-NLS-1$
				}
				if (immersionIt == null || immersionIt.size() != nbPings) {
					logger.warn(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_IMMERSION")); //$NON-NLS-1$
				}
				if (indexIt == null || indexIt.size() != nbPings) {
					logger.error(DicoBundle.getString("KEY_WATERCOLUMNFILEREADER_INVALID_INDEX")); //$NON-NLS-1$
				}

				WaterColumnData wcd = layer.getDatas();

				wcd.setImgDir(imgDir);
				List<Position> positions = new ArrayList<>(nbPings);
				List<String> imgList = new ArrayList<>(nbPings);
				SubMonitor subMonitor = SubMonitor.convert(progress, nbPings);
				for (int i = 0; i < nbPings; i++) {

					Double latPort = latPortIt.get();
					Double lonPort = lonPortIt.get();
					Double latStar = latStarIt.get();
					Double lonStar = lonStarIt.get();
					Double alt = altIt.get();
					Double top = null;
					if (immersionIt != null) {
						top = immersionIt.get();
					}
					int idx = indexIt.get().intValue();
					Double lonCenter = lonCenterIt.get();
					Double latCenter = latCenterIt.get();

					wcd.addLatlon1(LatLon.fromDegrees(latPort, lonPort));
					wcd.addLatlon2(LatLon.fromDegrees(latStar, lonStar));

					if (top != null) {
						wcd.addTopElevation(top);
					}
					wcd.addBottomElevation(alt);
					wcd.addIndex(idx); // permet de retrouver le ping en
					// fonction de l'index
					wcd.addIndexMap(idx, i); // permet de retrouver l'index en
					// fonction du ping

					/*
					 * Add M.antoine 12/04/2010
					 */
					Double date = dateIt.get();
					Double hour = hourIt.get();
					JulianDay jd = new JulianDay(date);
					wcd.addTime((long) (jd.getMilliSeconds() + hour));
					LatLon ll = LatLon.fromDegrees(latCenter, lonCenter);
					wcd.addLatLonCenter(ll);
					/*
					 * end add M.antoine 12/04/2010
					 */

					imgList.add(getImageFileName(imgDir, idx, parent));
					positions.add(new Position(LatLon.fromDegrees(latCenter, lonCenter), 0.0));

					subMonitor.worked(1);
				}

				PlayerBean bean = layer.getPlayerBean();
				bean.setIndexList(wcd.getIndexList());

				bean.setPlayIndex(0);
				bean.setMinPlayIndex(0);
				bean.setMaxPlayIndex(wcd.getIndexList().size() - 1);

				wcd.setPositions(positions);
				wcd.setImgList(imgList);
			}
		}
	}

	/**
	 * Returns image file name.
	 *
	 * @param imgDir      image directory name
	 * @param imageNumber the image index
	 * @param dir         the image base directory
	 * @return the image file name or null if not found
	 */
	private String getImageFileName(String imgDir, int imageNumber, File dir) {
		Formatter formatter = new Formatter();
		String fileName = null;
		if (dir != null) {
			int dirNumber = imageNumber / 100;
			fileName = formatter.format("%s/%s/%03d/%05d", dir //$NON-NLS-1$
					.getAbsolutePath(), imgDir, dirNumber, imageNumber)
					+ DicoBundle.getString("KEY_WATERCOLUMNLAYER_FILE_EXTENSION"); //$NON-NLS-1$
		}
		formatter.close();

		return fileName;
	}

	@Override
	public List<Pair<String, String>> extensions() {
		return extensions;
	}

	@Override
	public String getImage(Object resource) {
		return "icons/16/polarEcho.png";
	}

	@Override
	public List<Property<?>> getProperties(String resource) {

		return new ArrayList<>();
	}

	@Override
	public int getPriority() {
		return DEFAULT_PRIORITY;
	}

	/* @formatter:off
	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	@formatter:on */
	@Override
	public ContentType getContentType(String resource) {
		return accept(resource) ? ContentType.OLD_ECHOGRAM_XML : ContentType.UNDEFINED;
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.OLD_ECHOGRAM_XML);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(getImage(null), getClass()));
	}

	@Override
	public List<String> getExtensions() {
		return Collections.singletonList("xml");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return extensions();
	}

	@Override
	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		return Optional
				.ofNullable(accept(filePath) ? new BridgeFileInfo(filePath, getContentType(filePath), this) : null);
	}

}
