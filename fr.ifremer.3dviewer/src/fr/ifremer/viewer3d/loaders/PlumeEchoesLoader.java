/*
 * @License@
 */
package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.IDataDriver;
import fr.ifremer.viewer3d.layers.plume.PlumeLayer;
import fr.ifremer.viewer3d.model.Echo;
import fr.ifremer.viewer3d.model.PlumeBean;
import fr.ifremer.viewer3d.model.echoes.Dimensions;
import fr.ifremer.viewer3d.model.echoes.Echoes;
import fr.ifremer.viewer3d.model.echoes.Item;
import fr.ifremer.viewer3d.model.echoes.Signals;
import fr.ifremer.viewer3d.util.XSDValidator;
import fr.ifremer.viewer3d.util.ZipUtils;
import fr.ifremer.viewer3d.util.binary.BinaryUtil;
import fr.ifremer.viewer3d.util.binary.BufferIterator;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Loader for plume echoes data files.
 * <p>
 * Loads data definition from Echoes XML file and associated binary files.
 * </p>
 *
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
@Component(name = "globe_view3d_loaders_plumeechoesloader", service = { IDataDriver.class, IIcon16ForFileProvider.class,
			IFileInfoSupplier.class })
public class PlumeEchoesLoader implements ILoader, //
		IDataDriver, // BRIDGE !
		IIcon16ForFileProvider, IFileInfoSupplier<BridgeFileInfo> {

	/**
	 * XML schema (XSD) file name for this loader input data.
	 */
	public static final String SCHEMA_FILENAME = "/echoes-1.0.xsd";

	/**
	 * Extension for echoes definition files.
	 */
	public static final String EXTENSION = ".xml"; //$NON-NLS-1$

	/**
	 * Name of the XML element for echoes' number. {@value}
	 */
	public static final String NB_ECHOS_NAME = "nbEchoes"; //$NON-NLS-1$

	/**
	 * Name for the longitude XML item. {@value}
	 */
	public static final String LONGITUDE_NAME = "Longitude"; //$NON-NLS-1$

	/**
	 * Name for the latitude XML item. {@value}
	 */
	public static final String LATITUDE_NAME = "Latitude"; //$NON-NLS-1$

	/**
	 * Name for the altitude XML item.{@value}
	 */
	public static final String ALT_NAME = "Z"; //$NON-NLS-1$

	/**
	 * Name for the ping indice XML item.{@value}
	 */
	public static final String PING_NAME = "iPing"; //$NON-NLS-1$

	/**
	 * Name for the energy XML item.{@value}
	 */
	public static final String ENERGY_NAME = "Energie"; //$NON-NLS-1$

	/**
	 * Name for the total energy XML item.{@value}
	 */
	public static final String TOTAL_ENERGY_NAME = "EnergieTotale"; //$NON-NLS-1$

	// FT5440 Add missing echo parameter
	/**
	 * Name for the date XML item.{@value}
	 */
	public static final String DATE_NAME = "Date"; //$NON-NLS-1$

	/**
	 * Name for the hour XML item.{@value}
	 */
	public static final String HOUR_NAME = "Hour"; //$NON-NLS-1$

	/**
	 * Name for the XCoor XML item.{@value}
	 */
	public static final String XCOOR_NAME = "XCoor"; //$NON-NLS-1$

	/**
	 * Name for the YCOOR XML item.{@value}
	 */
	public static final String YCOOR_NAME = "YCoor"; //$NON-NLS-1$

	/**
	 * Name for the Angle XML item.{@value}
	 */
	public static final String ANGLE_NAME = "Angle"; //$NON-NLS-1$

	/**
	 * Name for the RangeInSamples XML item.{@value}
	 */
	public static final String RANGE_IN_SAMPLE_NAME = "RangeInSamples"; //$NON-NLS-1$

	/**
	 * Name for the AcrossDistance XML item.{@value}
	 */
	public static final String ACROSS_DISTANCE_NAME = "AcrossDistance"; //$NON-NLS-1$

	/**
	 * Name for the Celerite XML item.{@value}
	 */
	public static final String CELERITY_NAME = "Celerite"; //$NON-NLS-1$

	/**
	 * Logger for this class.
	 */
	private static Logger logger = LoggerFactory.getLogger(PlumeEchoesLoader.class);

	private static List<Pair<String, String>> extensions = new ArrayList<>();

	static {
		extensions.add(new Pair<>("Echoes (*.xml)", "*" + EXTENSION));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileNameExtensionFilter getFileFilter() {
		return new FileNameExtensionFilter(DicoBundle.getString("KEY_PLUMEECHOESLOADER_FILTER_MESSAGE"), //$NON-NLS-1$
				"*" + EXTENSION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean accept(String resource) {
		File file = new File(resource);
		if (file != null) {
			String name = file.getName();
			if (name != null && name.endsWith(EXTENSION)) {
				try {
					FileInputStream fis = new FileInputStream(file);
					InputStream schema = getClass().getResourceAsStream(SCHEMA_FILENAME);
					logger.info("Schema is :" + SCHEMA_FILENAME);
					return XSDValidator.valid(fis, schema);
				} catch (FileNotFoundException e) {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor monitor) throws Exception {
		if (file == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_PLUMEECHOESLOADER_ERROR_MESSAGE")); //$NON-NLS-1$
		}

		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		Echoes echoes = getEchoesData(file);
		PlumeBean data = new PlumeBean();

		// REQ-IHM-004 - Markers Filtering - Initialize PlumeBean filters
		if (echoes != null) {
			data.setSignalfilters(echoes.getSignalFilters());
		}

		PlumeLayer layer = new PlumeLayer(data);

		subMonitor.worked(40);
		if (echoes != null) {
			// Test presence des donnees
			File parent = file.getParentFile();
			File item = new File(parent.getAbsolutePath() + "/" + echoes.getSignals().getItem().get(0).getFileName());
			File dataFile = item.getParentFile(); // data
			file = ZipUtils.unzipDataInCache(file, dataFile);

			data.setFile(file);
			layer.setName(echoes.getName());

			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.SurveyInfo"), echoes.getSurvey()); //$NON-NLS-1$
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.VesselInfo"), echoes.getVessel()); //$NON-NLS-1$
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.SounderInfo"), echoes.getSounder()); //$NON-NLS-1$
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.ChiefScientistInfo"), //$NON-NLS-1$
					echoes.getChiefScientist());
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.TagInfo"), echoes.getDataTag()); //$NON-NLS-1$
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.SoftwareInfo"), echoes.getSoftware()); //$NON-NLS-1$
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.ExtractedInfo"), //$NON-NLS-1$
					echoes.getExtractedFrom());
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.FormatInfo"), String.valueOf(echoes //$NON-NLS-1$
					.getFormatVersion()));
			layer.getInfos().addInfo(DicoBundle.getString("PlumeEchoesLoader.ExportInfo"), echoes.getNomDirExport()); //$NON-NLS-1$

			layer.setBaseForm(echoes.getBaseForm());

			// if (full) {
			loadBinaryPlumeFile(file, layer, echoes, subMonitor.split(60));
			// }
		}

		List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(1);
		layers.add(new Pair<Layer, ElevationModel>(layer, null));
		return layers;
	}

	/**
	 * {@inheritDoc}
	 */
	public void fullLoad(File file, PlumeLayer layer) throws Exception {
		if (file != null && layer != null) {
			Echoes echoes = getEchoesData(file);
			loadBinaryPlumeFile(file, layer, echoes, null);
		}
	}

	/**
	 * Load echoes from binary data files.
	 *
	 * @param inputFile the input XML "index" file
	 * @param layer owner of loaded data
	 * @param echoes meta data from XML definition
	 * @return loaded plume bean or null if an error occurs
	 * @throws IOException if an exception occurs during file access
	 * @throws JAXBException if an exception occurs during XML parsing
	 */
	private PlumeLayer loadBinaryPlumeFile(File inputFile, PlumeLayer layer, Echoes echoes, IProgressMonitor monitor)
			throws JAXBException, GIOException {
		if (echoes == null) {
			return null;
		}
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		PlumeBean plume = layer.getPlume();

		// retrieve number of echoes
		int nbEchoes = 0;
		Dimensions dim = echoes.getDimensions();
		if (dim != null) {
			for (Object o : dim.getContent()) {
				if (nbEchoes == 0 && o instanceof JAXBElement<?>) {
					JAXBElement<?> elt = (JAXBElement<?>) o;
					if (elt != null && NB_ECHOS_NAME.equals(elt.getName().toString())) {
						Object value = elt.getValue();
						if (value != null) {
							try {
								nbEchoes = Integer.valueOf(value.toString());
							} catch (NumberFormatException nfe) {
								logger.warn(DicoBundle.getString("KEY_PLUMEECHOESLOADER_ERROR_MESSAGE_ECHOES") //$NON-NLS-1$
										+ value.toString());
							}
						}
					}
				}
			}
		}

		subMonitor.worked(5);

		// if at least one echo
		if (nbEchoes > 0) {
			Signals signals = echoes.getSignals();
			if (signals != null) {
				BufferIterator<Double> latIt = null;
				BufferIterator<Double> lonIt = null;
				BufferIterator<Double> altIt = null;
				BufferIterator<Double> energyIt = null;
				BufferIterator<Double> pingIt = null;
				BufferIterator<Double> totalEnergyIt = null;
				// FT5440
				BufferIterator<Double> dateIt = null;
				BufferIterator<Double> hourIt = null;
				BufferIterator<Double> xCoorIt = null;
				BufferIterator<Double> yCoorIt = null;
				BufferIterator<Double> angleIt = null;
				BufferIterator<Double> rangeInSampleIt = null;
				BufferIterator<Double> acrossdistanceIt = null;
				BufferIterator<Double> celeriteIt = null;

				File parent = FileUtils.getParent(inputFile);
				String basePath = parent.getAbsolutePath() + "/"; //$NON-NLS-1$
				for (Item item : signals.getItem()) {
					String filename = item.getFileName().trim();
					// replace backslashes by slashes for portability
					filename = filename.replaceAll("\\\\([^ ])", "/$1");
					File file = new File(basePath + filename);

					plume.addBinaryFile(file);

					if (file.exists()) {
						if (LONGITUDE_NAME.equals(item.getName())) {
							// retrieve longitudes
							lonIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (LATITUDE_NAME.equals(item.getName())) {
							// retrieve latitudes
							latIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (ALT_NAME.equals(item.getName())) {
							// retrieve altitude values
							altIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (ENERGY_NAME.equals(item.getName())) {
							// retrieve energy values
							energyIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (PING_NAME.equals(item.getName())) {
							// retrieve energy values
							pingIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (TOTAL_ENERGY_NAME.equals(item.getName())) {
							// retrieve energy values
							totalEnergyIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (DATE_NAME.equals(item.getName())) {
							// retrieve date values
							dateIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (HOUR_NAME.equals(item.getName())) {
							// retrieve hour values
							hourIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (XCOOR_NAME.equals(item.getName())) {
							// retrieve xCoor values
							xCoorIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (YCOOR_NAME.equals(item.getName())) {
							// retrieve yCoor values
							yCoorIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (ANGLE_NAME.equals(item.getName())) {
							// retrieve angle values
							angleIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (RANGE_IN_SAMPLE_NAME.equals(item.getName())) {
							// retrieve range values
							rangeInSampleIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (ACROSS_DISTANCE_NAME.equals(item.getName())) {
							// retrieve distance values
							acrossdistanceIt = BinaryUtil.readBinFile(file, item.getStorage());
						} else if (CELERITY_NAME.equals(item.getName())) {
							// retrieve celerite values
							celeriteIt = BinaryUtil.readBinFile(file, item.getStorage());
						}
					}
				}

				// should check iterators
				if (pingIt == null) {
					logger.error(DicoBundle.getString("PlumeEchoesLoader.PingLoadingError")); //$NON-NLS-1$
				}
				if (energyIt == null) {
					logger.error(DicoBundle.getString("PlumeEchoesLoader.EnergyLoadingError")); //$NON-NLS-1$
				}

				for (int i = 1; i < nbEchoes; i++) {
					subMonitor.worked(85);
					Double lat = latIt.get();
					Double lon = lonIt.get();
					Double alt = altIt.get();
					Double energy = 0.0;

					if (energyIt != null) {
						energy = energyIt.get();
					}
					LatLon ll = LatLon.fromDegrees(lat, lon);

					Echo echo = new Echo();
					echo.setPosition(new Position(ll, alt));
					echo.setEnergy(energy.floatValue());
					if (pingIt != null) {
						echo.setPing(pingIt.get().intValue());
					}
					if (totalEnergyIt != null) {
						echo.setTotalEnergy(totalEnergyIt.get().floatValue());
					}

					// FT5440
					if (dateIt != null) {
						echo.setDate(dateIt.get().intValue());
					}
					if (hourIt != null) {
						echo.setHour(hourIt.get().longValue());
					}
					if (xCoorIt != null) {
						echo.setXCoordinate(xCoorIt.get().floatValue());
					}
					if (yCoorIt != null) {
						echo.setYCoordinate(yCoorIt.get().floatValue());
					}
					if (angleIt != null) {
						echo.setAngle(angleIt.get().floatValue());
					}
					if (rangeInSampleIt != null) {
						echo.setRangeInSample(rangeInSampleIt.get().floatValue());
					}
					if (acrossdistanceIt != null) {
						echo.setAcrossDistance(acrossdistanceIt.get().floatValue());
					}
					if (celeriteIt != null) {
						echo.setCelerity(celeriteIt.get().floatValue());
					}

					plume.addEchoe(echo);
				}

			}
		}
		subMonitor.done();
		plume.setFile(inputFile);

		return layer;
	}

	/**
	 * Load XML definition.
	 *
	 * @param inputFile input XML file
	 */
	private Echoes getEchoesData(File inputFile) throws JAXBException, FileNotFoundException {
		JAXBContext jc = JAXBContext.newInstance(Echoes.class.getPackageName(), Echoes.class.getClassLoader());
		Unmarshaller u = jc.createUnmarshaller();
		FileInputStream fis = new FileInputStream(inputFile);
		Object obj = u.unmarshal(fis);
		if (obj instanceof Echoes) {
			return (Echoes) obj;
		}

		return null;
	}

	@Override
	public List<Pair<String, String>> extensions() {
		return extensions;
	}

	@Override
	public String getImage(Object resource) {
		return "icons/16/bubbles.png";
	}

	@Override
	public int getPriority() {
		return DEFAULT_PRIORITY;
	}

	/* @formatter:off
	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	@formatter:on */
	@Override
	public ContentType getContentType(String resource) {
		return accept(resource) ? ContentType.OLD_PLUMEECHOES_XML : ContentType.UNDEFINED;
	}

	@Override
	public EnumSet<ContentType> getContentTypes() {
		return EnumSet.of(ContentType.OLD_PLUMEECHOES_XML);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(getImage(null), getClass()));
	}

	@Override
	public List<String> getExtensions() {
		return Collections.singletonList("xml");
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return extensions();
	}

	@Override
	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		return Optional
				.ofNullable(accept(filePath) ? new BridgeFileInfo(filePath, getContentType(filePath), this) : null);
	}

}
