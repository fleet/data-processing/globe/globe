package fr.ifremer.viewer3d.events;

/**
 * Evènement de définition de l'apparition du profile sous l'outil de mesure.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class MeasureProfileEvent {

	private final Object src;

	private final boolean active;

	public MeasureProfileEvent(Object source, boolean active) {
		this.active = active;
		this.src = source;
	}

	public boolean isActive() {
		return this.active;
	}

	public Object getSource() {
		return this.src;
	}
}
