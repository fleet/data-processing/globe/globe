package fr.ifremer.viewer3d.events;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.events.BaseEvent;

/**
 * This event is used to open the Preferences dialog. You can pre-select the desired node.
 */
public class OpenPreferencesDialogEvent implements BaseEvent {

	protected PreferenceComposite selectedNode;

	protected PreferenceAttribute<?> selectedAttribute;

	/**
	 * Ask to open the preferences dialog.
	 */
	public OpenPreferencesDialogEvent() {
	}

	/**
	 * Ask to open the preferences dialog and to select a node.
	 * 
	 * @param node
	 *            Node to be selected after the preferences dialog is open.
	 */
	public OpenPreferencesDialogEvent(PreferenceComposite node) {
		this.selectedNode = node;
	}

	/**
	 * Ask to open the preferences dialog and to select a node.
	 * 
	 * @param node
	 *            Node to be selected after the preferences dialog is open.
	 * @param selectedAttribute
	 *            Attribute to select.
	 */
	public OpenPreferencesDialogEvent(PreferenceComposite node, PreferenceAttribute<?> selectedAttribute) {
		this.selectedNode = node;
		this.selectedAttribute = selectedAttribute;
	}

	/**
	 * @return The node to be selected after the preferences dialog is open.
	 */
	public PreferenceComposite getSelectedNode() {
		return this.selectedNode;
	}

	/**
	 * @return The attribute to be selected after the preferences dialog is open.
	 */
	public PreferenceAttribute<?> getSelectedAttribute() {
		return this.selectedAttribute;
	}
}
