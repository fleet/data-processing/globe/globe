package fr.ifremer.viewer3d.wms;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerBaseNode;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.WMSLayerComponent;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;

public class WMSLayerNode extends LayerBaseNode {

	@XStreamOmitField
	WMSLayerComponent layerdefinition;

	public WMSLayerNode(String name, WMSLayerComponent layerdefinition) {
		super(name, Icons.LAYER.toImage());
		this.layerdefinition = layerdefinition;
	}

	public void declareLayer() {
		/**
		 * Add the layer to the 3DViewer
		 */
		LayerList layers = Viewer3D.getModel().getLayers();
		layers.addAll(layerdefinition.getLayers());
		setSavedCheckedState(layerdefinition.getLayers().stream().allMatch(Layer::isEnabled));
	}

	@Override
	public CheckState getState() {
		if (layerdefinition != null) {
			List<Layer> layers = layerdefinition.getLayers();
			if (layers != null && !layers.isEmpty()) {
				if (layers.get(0).isEnabled()) {
					return CheckState.TRUE;
				}
			}
		}
		return CheckState.FALSE;
	}

	@Override
	public void setLayerEnabled(boolean enabled) {
		if (layerdefinition != null) {
			for (Layer l : layerdefinition.getLayers()) {
				l.setEnabled(enabled);
			}
		}
	}

	@Override
	public void delete() {
		if (Viewer3D.getModel() != null) {
			LayerList layers = Viewer3D.getModel().getLayers();
			layers.removeAll(layerdefinition.getLayers());
		}
	}

}
