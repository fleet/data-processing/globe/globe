package fr.ifremer.viewer3d.wms;

import java.security.InvalidParameterException;
import java.util.List;

import fr.ifremer.viewer3d.layers.WMSLayerComponent;

/**
 * Implements Globe's data store for map layers.
 */
public class WMSStore  {

	/** Key used to retrieve all layers from WMS server. */
	public static final String ALL_LAYERS = "WMSStore_all_layers";
	// /** Key used to retrieve current layers. */
	// public static final String CURRENT_LAYERS = "WMSStore_current_layers";

	private final WMSDriver driver;
	private final String source;

	/** List of all WMS layers for this server. */
	private List<WMSLayerComponent> wmsLayers;

	private String label = "WMS";

	public WMSStore(final String url, final List<WMSLayerComponent> allLayers) {
		if (url == null) {
			throw new InvalidParameterException("URL cannot be null");
		}
		if (allLayers == null) {
			throw new InvalidParameterException("Data (List<WMSLayerComponents>) cannot be null");
		}

		driver = new WMSDriver();
		source = url;
		wmsLayers = allLayers;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPath() {
		return source;
	}

	// @Override
	// public void setFilename(String filename) {
	// throw new NotImplementedException();
	//
	// }

	/**
	 * Returns layers for this WMS server.
	 *
	 * @param params
	 *            <ul>
	 *            <li>CURRENT_LAYERS (default) : returns layers currently chosen by user</li>
	 *            <li>ALL_LAYERS (default) : returns all layers from WMS server</li>
	 *            </ul>
	 */
	public List<WMSLayerComponent> getAllLayer() {
		return wmsLayers;
	}

	/*
	 * public void setCurrentLayers(List<WMSLayerComponent> selectedList) { // remove old layers LayerList layers =
	 * Viewer3D.getModel().getLayers(); if (currentLayers != null) { for (WMSLayerComponent comp : currentLayers) {
	 * layers.removeAll(comp.getLayers()); } } currentLayers = selectedList; if (currentLayers != null) { for
	 * (WMSLayerComponent comp : currentLayers) { layers.addAll(comp.getLayers()); } } }
	 */
	/*
	 * @Override public boolean delete(IProgressMonitor monitor) {
	 *
	 * LayerList layers = Viewer3D.getModel().getLayers();
	 *
	 * if (currentLayers != null) { for (WMSLayerComponent comp : currentLayers) { layers.removeAll(comp.getLayers()); }
	 * }
	 *
	 * return true; }
	 */

}
