package fr.ifremer.viewer3d.wms;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.WMSLayerComponent;
import gov.nasa.worldwind.Factory;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.ogc.wms.WMSCapabilities;
import gov.nasa.worldwind.ogc.wms.WMSLayerCapabilities;
import gov.nasa.worldwind.ogc.wms.WMSLayerStyle;
import gov.nasa.worldwind.util.WWUtil;

/**
 * IDataDriver implementation for WMS servers.
 */
public class WMSDriver {

	protected Logger logger = LoggerFactory.getLogger(WMSDriver.class);

	private WMSStore store = null;

	/**
	 * TimeOut value = 30sec.
	 */
	public static final int TIMEOUT = 30000;

	/**
	 * WMS Data for method listSubLayers.
	 */
	private List<WMSLayerComponent> WMSData;

	public WMSStore initData(final String resource, IProgressMonitor monitor) {
		if (store == null) {

			Thread thread = new Thread(() -> {
				try {
					// long running task
					listSubLayers(Viewer3D.getWwd(), resource, "test");
				} catch (Exception e) {
					logger.error("Error loading WMS data", e);
				}
			});
			thread.start();

			// TimeOut if listSubLayers() lasts 30sec
			long endTimeMillis = System.currentTimeMillis() + TIMEOUT;
			while (thread.isAlive()) {
				if (System.currentTimeMillis() > endTimeMillis) {
					// set an error flag
					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Error", "Request timed out");
					break;
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException t) {
				}
			}

			if (WMSData != null) {
				store = new WMSStore(resource, WMSData);
			}
		}
		return store;
	}

	private static class LayerInfo {
		protected WMSCapabilities caps;
		protected AVListImpl params = new AVListImpl();

		// protected String getTitle() {
		// return this.params.getStringValue(AVKey.DISPLAY_NAME);
		// }

		// protected String getName() {
		// return this.params.getStringValue(AVKey.LAYER_NAMES);
		// }

		// protected String getAbstract() {
		// return this.params.getStringValue(AVKey.LAYER_ABSTRACT);
		// }
	}

	protected LayerInfo createLayerInfo(WMSCapabilities caps, WMSLayerCapabilities layerCaps, WMSLayerStyle style) {
		// Create the layer info specified by the layer's capabilities entry and
		// the selected style.

		LayerInfo linfo = new LayerInfo();
		linfo.caps = caps;
		linfo.params = new AVListImpl();
		linfo.params.setValue(AVKey.LAYER_NAMES, layerCaps.getName());
		if (style != null) {
			linfo.params.setValue(AVKey.STYLE_NAMES, style.getName());
		}
		String abs = layerCaps.getLayerAbstract();
		if (!WWUtil.isEmpty(abs)) {
			linfo.params.setValue(AVKey.LAYER_ABSTRACT, abs);
		}

		linfo.params.setValue(AVKey.DISPLAY_NAME, makeTitle(caps, linfo));

		return linfo;
	}

	protected String makeTitle(WMSCapabilities caps, LayerInfo layerInfo) {
		String layerNames = layerInfo.params.getStringValue(AVKey.LAYER_NAMES);
		String styleNames = layerInfo.params.getStringValue(AVKey.STYLE_NAMES);
		String[] lNames = layerNames.split(",");
		String[] sNames = styleNames != null ? styleNames.split(",") : null;

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < lNames.length; i++) {
			if (sb.length() > 0) {
				sb.append(", ");
			}

			String layerName = lNames[i];
			WMSLayerCapabilities lc = caps.getLayerByName(layerName);
			String layerTitle = lc.getTitle();
			sb.append(layerTitle != null ? layerTitle : layerName);

			if (sNames == null || sNames.length <= i) {
				continue;
			}

			String styleName = sNames[i];
			WMSLayerStyle style = lc.getStyleByName(styleName);
			if (style == null) {
				continue;
			}

			sb.append(" : ");
			String styleTitle = style.getTitle();
			sb.append(styleTitle != null ? styleTitle : styleName);
		}

		return sb.toString();
	}

	// need a void method for TimeOut thread
	public void listSubLayers(final WorldWindow wwd, final String server, final String name) throws Exception {

		WMSData = new ArrayList<>();

		WMSCapabilities caps = WMSCapabilities.retrieve(new URI(server.trim()));
		caps.parse();

		// Gather up all the named layers and make a world wind layer for
		// each.
		final List<WMSLayerCapabilities> namedLayerCaps = caps.getNamedLayers();

		for (WMSLayerCapabilities layerCaps : namedLayerCaps) {

			Set<WMSLayerStyle> styles = layerCaps.getStyles();

			List<LayerInfo> layersInfos = new ArrayList<>();

			WMSLayerComponent components = new WMSLayerComponent(wwd);

			if (styles == null || styles.size() == 0) {
				layersInfos.add(createLayerInfo(caps, layerCaps, null));
			} else {
				for (WMSLayerStyle style : styles) {
					layersInfos.add(createLayerInfo(caps, layerCaps, style));
				}
			}

			for (LayerInfo layerInfo : layersInfos) {
				components.add(createComponent(layerInfo.caps, layerInfo.params));
			}

			WMSData.add(components);
		}
	}

	protected Object createComponent(WMSCapabilities caps, AVList params) {
		AVList configParams = params.copy(); // Copy to insulate changes from
		// the caller.

		// Some wms servers are slow, so increase the timeouts and limits used
		// by world wind's retrievers.
		configParams.setValue(AVKey.URL_CONNECT_TIMEOUT, 30000);
		configParams.setValue(AVKey.URL_READ_TIMEOUT, 30000);
		configParams.setValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, 60000);

		try {
			String factoryKey = getFactoryKeyForCapabilities(caps);
			Factory factory = (Factory) WorldWind.createConfigurationComponent(factoryKey);
			return factory.createFromConfigSource(caps, params);
		} catch (Exception e) {
			// Ignore the exception, and just return null.
		}

		return null;
	}

	protected String getFactoryKeyForCapabilities(WMSCapabilities caps) {
		boolean hasApplicationBilFormat = false;

		Set<String> formats = caps.getImageFormats();
		for (String s : formats) {
			if (s.contains("application/bil")) {
				hasApplicationBilFormat = true;
				break;
			}
		}

		return hasApplicationBilFormat ? AVKey.ELEVATION_MODEL_FACTORY : AVKey.LAYER_FACTORY;
	}

}
