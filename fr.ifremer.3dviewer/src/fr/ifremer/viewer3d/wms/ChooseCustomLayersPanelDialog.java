/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ChooseSUBCustomPaneldIALOG.java
 *
 * Created on 14 avr. 2010, 17:15:44
 */

package fr.ifremer.viewer3d.wms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.WMSLayerComponent;
import gov.nasa.worldwind.layers.LayerList;

/**
 *
 */
public class ChooseCustomLayersPanelDialog extends Dialog {

	private WMSServerNode node;

	private String _macroLayerLabel;

	// private String address;

	private org.eclipse.swt.widgets.List layersList;

	/** Creates new form ChooseSUBCustomPaneldIALOG */
	public ChooseCustomLayersPanelDialog(Shell parent) {
		super(parent);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			loadWMSLayer();
		}
		super.buttonPressed(buttonId);
	}

	private void loadWMSLayer() {
		if (node != null) {
			WMSStore store = node.getStore();
			if (store != null) {
				List<WMSLayerComponent> components = store.getAllLayer();

				List<WMSLayerComponent> selectedList = new ArrayList<>();

				for (int idx : layersList.getSelectionIndices()) {
					selectedList.add(components.get(idx));
				}

				store.setLabel(_macroLayerLabel);
				List<WMSLayerComponent> list2add = LayersFiltering(selectedList);

				if (list2add != null) {
					ArrayList<TreeNode> nodeAdded = new ArrayList<>();
					for (WMSLayerComponent component : list2add) {
						WMSLayerNode layernode = new WMSLayerNode(component.getName(), component);
						layernode.declareLayer();
						nodeAdded.add(node.addChild(layernode));
					}
				}
			}
		}
	}

	private List<WMSLayerComponent> LayersFiltering(List<WMSLayerComponent> selectedList) {
		List<WMSLayerComponent> ret = new ArrayList<>();

		// remove old layers
		LayerList layers = Viewer3D.getModel().getLayers();
		if (selectedList != null) {

			String layerName = null;
			Boolean alreadyLoaded = false;
			for (WMSLayerComponent comp : selectedList) {

				alreadyLoaded = false;
				for (gov.nasa.worldwind.layers.Layer layer : layers) {
					layerName = layer.getName();
					if (layerName.equals(comp.getName())) {
						alreadyLoaded = true;
						break;
					}
				}

				if (alreadyLoaded) {
					// WMS layer already load in 3DViewer
					MessageDialog.openInformation(Display.getDefault().getActiveShell(), "Layer already loaded",
							"the wms layer: \"" + layerName + "\" is already loaded in your workspace.");
				} else {
					ret.add(comp);
				}
			}
		}
		return ret;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Layers selection");
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control c = super.createButtonBar(parent);
		getButton(OK).setEnabled(false);
		return c;
	}

	private void updateList() {
		if (layersList != null && node != null) {
			layersList.removeAll();
			List<String> names = new ArrayList<>();

			WMSStore store = node.getStore();
			if (store != null) {
				List<WMSLayerComponent> components = store.getAllLayer();

				// sort components by name (ignore case)
				Collections.sort(components, (c1, c2) -> {
					if (c1.getName() != null && c2.getName() != null) {
						return c1.getName().compareToIgnoreCase(c2.getName());
					} else if (c1.getName() != null) {
						return 1;
					}
					return -1;
				});

				for (WMSLayerComponent component : components) {
					names.add(component.getName());
				}
				for (String name : names) {
					if (name == null) {
						layersList.add("Unknown");
					} else {
						layersList.add(name);
					}
				}
			}
		}
	}

	public void setMacroLayerLabel(String label) {
		_macroLayerLabel = label;
	}

	// public void setAddress(String value) {
	// address = value;
	// }

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, false));

		Group group = new Group(composite, SWT.NONE);
		group.setText("Layers list");
		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 10;
		layout.verticalSpacing = 10;
		group.setLayout(layout);
		group.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		layersList = new org.eclipse.swt.widgets.List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1);
		gridData.heightHint = 500;
		layersList.setLayoutData(gridData);
		updateList();

		layersList.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				loadWMSLayer();
				ChooseCustomLayersPanelDialog.super.buttonPressed(IDialogConstants.OK_ID);
			}

			@Override
			public void mouseDown(MouseEvent e) {
				getButton(OK).setEnabled(true);
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
		});

		return composite;
	}

	public void setNode(WMSServerNode node) {
		this.node = node;
		updateList();
	}

}
