package fr.ifremer.viewer3d.wms;

import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

public class WMSServerNode extends GroupNode {

	WMSStore store;

	public WMSServerNode(String name, WMSStore store, WMSDriver driver) {
		super(name, Icons.WMS.toImage());
		this.store = store;
	}

	public void setStore(WMSStore store) {
		this.store = store;
	}

	/**
	 * @return the {@link #store}
	 */
	public WMSStore getStore() {
		return store;
	}
}
