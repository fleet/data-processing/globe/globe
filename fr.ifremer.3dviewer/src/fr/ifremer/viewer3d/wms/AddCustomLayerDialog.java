/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * AddCustomLayerDialog.java
 *
 * Created on 19 févr. 2010, 10:09:48
 */

package fr.ifremer.viewer3d.wms;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.viewer3d.deprecated.IErrorHandler;

/**
 *
 */
public class AddCustomLayerDialog extends Dialog implements IErrorHandler {

	private IErrorHandler errorHandler = null;

	private final static String IFREMER_SERVER_ADDRESS = "http://www.ifremer.fr/services/wms1";
	private final static String IFREMER_LABEL = "Ifremer";
	private final static String BRGM_SERVER_ADDRESS = "http://geoservices.brgm.fr/geologie";
	private final static String BRGM_LABEL = "BRGM";
	private final static String ONEGEOLOGY_SERVER_ADDRESS = "http://www.seismicportal.eu/geoserver/ows";
	private final static String ONE_GEOLOGY_LABEL = "OneGeology Europe";
	// private final static String EMODNET_SERVER_ADDRESS =
	// "http://geoservice.maris2.nl/wms/seadatanet/cdi_v2/simorc";
	// private final static String EMODNET_LABEL = "EMODnet";
	private final static String EMODNET_HYDROGRAPHY_SERVER_ADDRESS = "http://geoservice.maris2.nl/wms/seadatanet/cdi_v2/emodnet/hydrography";
	private final static String EMODNET_HYDROGRAPHY_LABEL = "EMODnet Hydrography";
	// private final static String SEADATANET_SERVER_ADDRESS =
	// "http://geoservice.maris2.nl/wms/seadatanet/cdi_v2/seadatanet";
	// private final static String SEADATANET_LABEL = "SeaDataNet";

	private String layerNameTemp;
	private String serverAdddressTemp;

	private Combo serverAddress;
	private Text layerName;
	private WMSDriver driver;

	private boolean layerNameFilled = false;
	private boolean serverAddressFilled = false;

	private GroupNode wMSGroupNode;

	/** Creates new form AddCustomLayerDialog */
	public AddCustomLayerDialog(Shell parentShell, GroupNode wMSGroupNode) {
		super(parentShell);
		this.wMSGroupNode = wMSGroupNode;
		driver = new WMSDriver();
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			String address = serverAddress.getText();

			if (address != null && !address.equals("")) { //$NON-NLS-1$

				// here find if a WSMServerNode already exists with the same address
				WMSServerNode node = findServerNode(wMSGroupNode, address);
				WMSStore store = null;
				if (node != null) {
					store = node.getStore();
					if (store == null) {
						store = driver.initData(address, new NullProgressMonitor());
						node.setStore(store);
					}
				}
				// if not, create it
				else {
					store = driver.initData(address, new NullProgressMonitor());
					node = new WMSServerNode(layerName.getText(), store, driver);
					wMSGroupNode.addChild(node);
				}
				if (store != null) {

					if (store.getAllLayer().isEmpty()) {
						MessageDialog.openError(Display.getCurrent().getActiveShell(), "Error",
								DicoBundle.getString("ADD_CUSTOM_LAYER_ERROR") + " (" + serverAddress.getText() + ")");
					} else {
						ChooseCustomLayersPanelDialog subPanelDial = new ChooseCustomLayersPanelDialog(
								getParentShell());
						subPanelDial.setNode(node);
						subPanelDial.setMacroLayerLabel(layerName.getText());
						// subPanelDial.setAddress(address);
						subPanelDial.open();

					}
				}
			}
		}
		super.buttonPressed(buttonId);
	}

	private WMSServerNode findServerNode(GroupNode root, String url) {
		if (root instanceof WMSServerNode && ((WMSServerNode) root).getStore() != null) {
			if (((WMSServerNode) root).getStore().getPath().equals(url)) {
				return (WMSServerNode) root;
			}
		}
		for (TreeNode child : root.getChildren()) {
			if (child instanceof GroupNode) {
				WMSServerNode nodefound = findServerNode((GroupNode) child, url);
				if (nodefound != null) {
					return nodefound;
				}
			}
		}
		return null;

	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(DicoBundle.getString("CUSTOM_LAYER_DIALOG_TITLE"));
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control c = super.createButtonBar(parent);
		getButton(OK).setEnabled(false);
		return c;
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		errorHandler = this;

		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, false));

		Group paramGroup = new Group(composite, SWT.NONE);
		paramGroup.setText(DicoBundle.getString("CUSTOM_LAYER_DIALOG_SERVER_PARAMETERS"));
		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 10;
		layout.verticalSpacing = 10;
		paramGroup.setLayout(layout);
		paramGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		Label serverLabel = new Label(paramGroup, SWT.NONE);
		serverLabel.setText(DicoBundle.getString("CUSTOM_LAYER_DIALOG_SERVER_ADDRESS"));
		serverLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		serverAddress = new Combo(paramGroup, SWT.BORDER);
		serverAddress.add(IFREMER_SERVER_ADDRESS);
		serverAddress.add(BRGM_SERVER_ADDRESS);
		serverAddress.add(ONEGEOLOGY_SERVER_ADDRESS);
		// serverAddress.add(EMODNET_SERVER_ADDRESS);
		serverAddress.add(EMODNET_HYDROGRAPHY_SERVER_ADDRESS);
		// serverAddress.add(SEADATANET_SERVER_ADDRESS);

		serverAddress.setFocus();
		GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1);
		gridData.widthHint = 300;
		serverAddress.setLayoutData(gridData);

		Label layerLabel = new Label(paramGroup, SWT.NONE);
		layerLabel.setText(DicoBundle.getString("CUSTOM_LAYER_DIALOG_LAYER_NAME"));
		layerLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		layerName = new Text(paramGroup, SWT.BORDER);
		layerName.setText("");
		gridData = new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1);
		gridData.widthHint = 300;
		layerName.setLayoutData(gridData);

		Text helpLabel = new Text(composite, SWT.MULTI);
		helpLabel.setEditable(false);
		helpLabel.setBackground(serverLabel.getBackground());
		helpLabel.setText(DicoBundle.getString("CUSTOM_LAYER_DIALOG_TEXT"));
		helpLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		serverAddress.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (serverAddress.getText().equals(IFREMER_SERVER_ADDRESS)) {
					layerName.setText(IFREMER_LABEL);
					layerNameFilled = true;
				} else if (serverAddress.getText().equals(BRGM_SERVER_ADDRESS)) {
					layerName.setText(BRGM_LABEL);
					layerNameFilled = true;
				} else if (serverAddress.getText().equals(ONEGEOLOGY_SERVER_ADDRESS)) {
					layerName.setText(ONE_GEOLOGY_LABEL);
					layerNameFilled = true;
					// } else if (serverAddress.getText().equals(
					// EMODNET_SERVER_ADDRESS)) {
					// layerName.setText(EMODNET_LABEL);
					// layerNameFilled = true;
					// } else if (serverAddress.getText().equals(
					// SEADATANET_SERVER_ADDRESS)) {
					// layerName.setText(SEADATANET_LABEL);
					// layerNameFilled = true;
				} else if (serverAddress.getText().equals(EMODNET_HYDROGRAPHY_SERVER_ADDRESS)) {
					layerName.setText(EMODNET_HYDROGRAPHY_LABEL);
					layerNameFilled = true;
				}
			}
		});

		layerName.addModifyListener(e -> onModificationLayerName(layerName.getText(), e));

		serverAddress.addModifyListener(e -> onModificationServerAdress(serverAddress.getText(), e));

		return composite;
	}

	/**
	 * @param Object
	 * @param Event
	 */
	public void onModificationLayerName(String layerName, ModifyEvent event) {
		if (errorHandler == null) {
			return;
		}

		if (layerName != null) {
			layerNameTemp = layerName;
			if (layerNameTemp == null || layerNameTemp.isEmpty()) {
				layerNameFilled = false;
				errorHandler.setErrorMessage("Layer name must be specified");
				return;
			}

			String invalid = FileUtils.checkFilename(layerNameTemp);
			if (invalid != null) {
				layerNameFilled = false;
				errorHandler.setErrorMessage("Invalid character in layer name : " + invalid);
				return;
			}
		}
		layerNameFilled = true;
		errorHandler.setErrorMessage(null);
	}

	/**
	 * @param Object
	 * @param Event
	 */
	public void onModificationServerAdress(String serverAddress, ModifyEvent event) {
		if (errorHandler == null) {
			return;
		}

		if (serverAddress != null) {
			serverAdddressTemp = serverAddress;
			if (serverAdddressTemp == null || serverAdddressTemp.isEmpty()) {
				serverAddressFilled = false;
				errorHandler.setErrorMessage("server adress must be specified");
				return;
			}

			try {
				new URL(serverAddress);
			} catch (MalformedURLException e) {
				serverAddressFilled = false;
				errorHandler.setErrorMessage("Invalid URL: " + e.getMessage());
				return;
			}
		}
		serverAddressFilled = true;
		errorHandler.setErrorMessage(null);
	}

	@Override
	public void setErrorMessage(String newErrorMessage) {
		getButton(OK).setEnabled(
				(newErrorMessage == null || newErrorMessage.isEmpty()) && serverAddressFilled && layerNameFilled);
	}
}
