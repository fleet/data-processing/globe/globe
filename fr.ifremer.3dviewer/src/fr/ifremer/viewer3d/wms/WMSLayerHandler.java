package fr.ifremer.viewer3d.wms;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

public class WMSLayerHandler extends AbstractNodeHandler {

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return SelectionUtils.getUniqueSelection(selectionService, GroupNode.class,
				groupNode -> groupNode == projectExplorerModel.getwMSGroupNode()).isPresent();
	}

	@Execute
	public void execute(@Active Shell shell) {
		new AddCustomLayerDialog(shell, projectExplorerModel.getwMSGroupNode()).open();
	}
}