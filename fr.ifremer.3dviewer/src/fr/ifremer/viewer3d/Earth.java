/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d;

import fr.ifremer.viewer3d.elevation.RootElevationModel;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.globes.EllipsoidalGlobe;

/**
 * Specific EllipsoidalGlobe implementation to set ElevationModel at construction.<br>
 * Ensure that the ElevationModel used in Globe in always a RootElevationModel.
 */
public class Earth extends EllipsoidalGlobe {

	/**
	 * Constructor<br>
	 * Same constructor than gov.nasa.worldwind.globes.Earth but with the RootElevationModel initialization.
	 */
	public Earth() {
		super(gov.nasa.worldwind.globes.Earth.WGS84_EQUATORIAL_RADIUS,
				gov.nasa.worldwind.globes.Earth.WGS84_POLAR_RADIUS, gov.nasa.worldwind.globes.Earth.WGS84_ES,
				// Replace the default WW elevation model by a RootElevationModel
				new RootElevationModel(EllipsoidalGlobe.makeElevationModel(AVKey.EARTH_ELEVATION_MODEL_CONFIG_FILE,
						"config/Earth/EarthElevations2.xml")));
	}

}
