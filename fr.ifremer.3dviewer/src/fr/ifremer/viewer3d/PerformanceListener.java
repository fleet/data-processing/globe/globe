package fr.ifremer.viewer3d;

import java.util.LinkedList;

import jakarta.inject.Inject;

import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.event.RenderingEvent;
import gov.nasa.worldwind.event.RenderingListener;
import gov.nasa.worldwind.util.PerformanceStatistic;

public class PerformanceListener {

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	private RenderingListener renderListener;
	private WorldWindowGLCanvas windows;

	PerformanceListener(WorldWindowGLCanvas windows) {
		ContextInitializer.inject(this);
		this.windows = windows;
	}

	public void start() {
		if (windows != null && renderListener == null) {
			windows.setPerFrameStatisticsKeys(PerformanceStatistic.ALL_STATISTICS_SET);
			renderListener = event -> {
				if (event.getStage().equals(RenderingEvent.AFTER_BUFFER_SWAP)) {
					LinkedList<Property<?>> proplist = new LinkedList<>();
					for (PerformanceStatistic performance : Viewer3D.getWwd().getPerFrameStatistics())
						proplist.add(Property.build(performance.getDisplayString(), performance.getValue().toString()));
					projectExplorerModel.getBackgroundNode().setProperties(proplist);
				}
			};
			windows.addRenderingListener(renderListener);
		}
	}

	public void stop() {
		if (windows != null && renderListener != null)
			windows.removeRenderingListener(renderListener);
	}

}
