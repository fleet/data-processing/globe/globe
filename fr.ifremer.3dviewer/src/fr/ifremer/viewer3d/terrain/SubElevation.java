package fr.ifremer.viewer3d.terrain;

import java.util.HashMap;

import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;

/***
 * Container of {@link MovableInterpolateElevationModel} indexed by date
 * Interpolation could be done between 2 date indexed elevation models Operation
 * could be performed
 * 
 * @author MORVAN
 * 
 */
public class SubElevation {

	/***
	 * Elevation models indexed by date (date set by Parent
	 * {@link MultiTextureLayer} )
	 */
	private HashMap<Integer, MovableInterpolateElevationModel> elevations = new HashMap<Integer, MovableInterpolateElevationModel>();

	/***
	 * name as displayed in IHM
	 */
	@SuppressWarnings("unused")
	private String name;

	public SubElevation(String name) {
		this.name = name;
	}

	/***
	 * add an elevation model indexed by date
	 * 
	 * @param dateIndex
	 * @param movableInterpolateElevationModel
	 */
	public void add(int dateIndex, MovableInterpolateElevationModel movableInterpolateElevationModel) {
		elevations.put(dateIndex, movableInterpolateElevationModel);
	}

	/***
	 * 
	 * @param dateIndex
	 * @return the elevation corresponding to the dateIndex
	 */
	public MovableInterpolateElevationModel getElevation(int dateIndex) {
		if (elevations.size() == 1) {
			elevations.get(0);
		}
		return elevations.get(dateIndex);
	}

}
