package fr.ifremer.viewer3d.terrain;

import java.net.URL;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.Tile;

/**
 * Overrides {@link BasicElevationModel} in order to handle non standard path
 * for tiles : see
 */
public class SSVElevationModel extends BasicElevationModel {

	public SSVElevationModel(AVList params) {
		super(params);
	}

	@Override
	protected void downloadElevations(Tile tile) {
		// ugly hack : check another local path
		final URL url = this.getDataFileStore().findFile(getSpecialPath(tile), false);
		if (tile instanceof ElevationTile && url != null && !this.isFileExpired(tile, url, this.getDataFileStore())) {
			try {
				if (this.loadElevations((ElevationTile) tile, url)) {
					this.levels.unmarkResourceAbsent(tile);
					this.firePropertyChange(AVKey.ELEVATION_MODEL, null, this);
				} else {
					// Assume that something's wrong with the file and delete
					// it.
					this.getDataFileStore().removeFile(url);
					this.levels.markResourceAbsent(tile);
					String message = Logging.getMessage("generic.DeletedCorruptDataFile", url);
					Logging.logger().info(message);
				}
			} catch (Exception e) {
				String msg = Logging.getMessage("ElevationModel.ExceptionRequestingElevations", tile.toString());
				Logging.logger().log(java.util.logging.Level.FINE, msg, e);
			}
		} else {
			super.downloadElevations(tile);
		}
	}

	/** Returns specific file path for some SonarScope tiles. */
	private String getSpecialPath(Tile tile) {
		String path = tile.getLevel().getPath() + "/" + String.format("%04d", tile.getRow()) + "/" + String.format("%04d", tile.getRow()) + "_" + String.format("%04d", tile.getColumn());
		if (!tile.getLevel().isEmpty()) {
			path += tile.getLevel().getFormatSuffix();
		}

		return path;
	}
}
