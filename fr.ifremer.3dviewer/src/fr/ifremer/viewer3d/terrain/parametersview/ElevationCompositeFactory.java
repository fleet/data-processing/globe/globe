package fr.ifremer.viewer3d.terrain.parametersview;

import java.util.Optional;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.ElevationNode;
import fr.ifremer.viewer3d.terrain.MultiElevationModel;
import gov.nasa.worldwind.globes.ElevationModel;

/**
 * Provide the parameter Composite of a ShpLayer
 */
@Component(name = "globe_parameters_view_elevation_composite_factory", service = IParametersViewCompositeFactory.class)
public class ElevationCompositeFactory implements IParametersViewCompositeFactory {

	/** Constructor */
	public ElevationCompositeFactory() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		Composite result = null;
		if (selection instanceof ElevationNode) {
			ElevationModel em = ((ElevationNode) selection).getElevationModel();
			if (em != null) {
				Composite mainComposite = new Composite(parent, SWT.NONE);
				result = mainComposite;
				GridLayoutFactory.fillDefaults().applyTo(mainComposite);
				if (!(em instanceof MultiElevationModel)) {
					// creates GUI's composites for world wind elevation model :
					// no OSGI service used here as there is way less elevation
					// model kinds than layers' : directly creates this GUI is
					// therefore a lot easier
					if (em instanceof IWWElevationModel) {
						var interpolationComposite = new InterpolationComposite(mainComposite, SWT.NONE,
								(IWWElevationModel) em);
						GridDataFactory.fillDefaults().grab(true, false).applyTo(interpolationComposite);
					}

					IGeographicViewService.grab().getExtensionParameters(em).ifPresent(extensionParameters -> {
						var extensionComposite = new ElevationModelExtensionComposite(mainComposite, em,
								extensionParameters);
						GridDataFactory.fillDefaults().grab(true, false).applyTo(extensionComposite);
					});

				}
			}

		}

		return Optional.ofNullable(result);
	}

}
