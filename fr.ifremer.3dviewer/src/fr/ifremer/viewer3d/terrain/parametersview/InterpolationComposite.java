package fr.ifremer.viewer3d.terrain.parametersview;

import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModel;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModel.ElevationModelParameters;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget;

/**
 * Composite for interpolation widget.
 */
public class InterpolationComposite extends Composite {

	public InterpolationComposite(Composite parent, int style, IWWElevationModel elevationModel) {
		super(parent, style);

		setLayout(new FillLayout());
		var interpolateModel = new EnabledNumberModel(elevationModel.getElevationModelParameters().interpolate,
				elevationModel.getElevationModelParameters().offset);
		var interpolateWidget = new SpinnerWidget("Missing data interpolation", this, interpolateModel, 2);
		interpolateWidget.subscribe(model -> {
			elevationModel
					.setElevationModelParameters(new ElevationModelParameters(model.enable, model.value.doubleValue()));
			IGeographicViewService.grab().refresh();
		});
		interpolateWidget.getSpinner().setToolTipText("Offset");
	}

}
