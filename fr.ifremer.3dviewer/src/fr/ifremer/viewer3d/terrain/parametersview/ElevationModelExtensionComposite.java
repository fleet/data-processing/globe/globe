/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.terrain.parametersview;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.service.geographicview.ExtensionParameters;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget.SpinnerWidgetLayout;
import gov.nasa.worldwind.globes.ElevationModel;

/**
 * Composite for extension widget in %
 */
public class ElevationModelExtensionComposite extends Composite {

	private IGeographicViewService geographicViewService = IGeographicViewService.grab();

	public ElevationModelExtensionComposite(Composite parent, ElevationModel elevationModel,
			ExtensionParameters elevationParam) {
		super(parent, SWT.NONE);
		setLayout(new FillLayout());

		// Init model
		var extensionModel = new EnabledNumberModel(elevationParam.extensionEnabled(),
				elevationParam.extensionPercent());

		// Creates widget
		var interpolateWidget = new SpinnerWidget("Extend elevation model", "% of extension", this, extensionModel, 0,
				SpinnerWidgetLayout.VERTICAL);
		interpolateWidget.setMinMaxValues(0f, 100f);
		interpolateWidget.subscribe(model -> {
			geographicViewService.applyExtensionParameters(elevationModel,
					new ExtensionParameters(model.enable, model.value.floatValue()));
			geographicViewService.refresh();
		});
		interpolateWidget.getSpinner().setToolTipText("Offset");
	}

}
