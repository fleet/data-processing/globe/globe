package fr.ifremer.viewer3d.terrain;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.ifremer.viewer3d.layers.IMovableLayer;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.cache.MemoryCache;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.Tile;
import gov.nasa.worldwind.util.TileKey;

/***
 * Adds movement functionality to {@link InterpolateElevationModel}.
 *
 * @author MORVAN
 *
 */
public class MovableInterpolateElevationModel extends InterpolateElevationModel implements IMovableLayer {

	/***
	 * movement to simulate tectonic for example
	 */
	private ISectorMovementProcessing movement = null;

	public MovableInterpolateElevationModel(Document dom, AVList params) {
		super(dom, params);
	}

	public MovableInterpolateElevationModel(Element element, AVList params) {
		super(element, params);
	}

	@Override
	public ISectorMovementProcessing getMovement() {
		return movement;
	}

	@Override
	public void setMovement(ISectorMovementProcessing movement) {
		this.movement = movement;
	}

	@Override
	public Sector move(Sector st) {
		if (movement != null) {
			return movement.move(st);
		} else {
			return st;
		}
	}

	@Override
	public LatLon move(LatLon latLon) {
		if (movement != null) {
			return movement.move(latLon);
		} else {
			return latLon;
		}
	}

	@Override
	public Sector moveInv(Sector st) {
		if (movement != null) {
			return movement.moveInv(st);
		} else {
			return st;
		}
	}

	@Override
	public LatLon moveInv(LatLon latLon) {
		if (movement != null) {
			return movement.moveInv(latLon);
		} else {
			return latLon;
		}
	}

	/***
	 * considers whether the sector intersects considering the rotation due to tectonic movement
	 */
	@Override
	public int intersects(Sector sector) {
		return super.intersects(moveInv(sector));
	}

	@Override
	public double getBestResolution(Sector sector) {
		// WW 2.1.0 : sector may now be null...
		return sector != null ? super.getBestResolution(move(sector)) : super.getBestResolution(sector);
	}

	@Override
	public double getUnmappedElevation(Angle latitude, Angle longitude) {

		double elev = super.getUnmappedElevation(latitude, longitude);

		if (elev == this.getMissingDataSignal()) { // -32768 = missing value, no
			// offset!
			return elev;
		} else {
			return elev + getElevationModelParameters().offset;
		}
	}

	@Override
	protected double getElevations(Sector sector, List<? extends LatLon> latlons, double targetResolution,
			double[] buffer, boolean mapMissingData) {
		if (sector == null) {
			String msg = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (latlons == null) {
			String msg = Logging.getMessage("nullValue.LatLonListIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (buffer == null) {
			String msg = Logging.getMessage("nullValue.ElevationsBufferIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (buffer.length < latlons.size()) {
			String msg = Logging.getMessage("ElevationModel.ElevationsBufferTooSmall", latlons.size());
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		Sector moved = moveInv(sector);
		// usual sector could be process the NWW way
		/*
		 * if(!(moved instanceof MovableSector)) return super.getElevations(sector, latlons, targetResolution, buffer,
		 * mapMissingData);
		 */

		Level targetLevel = this.getTargetLevel(moved, targetResolution);
		if (targetLevel == null) {
			return Double.MAX_VALUE;
		}

		Elevations elevations = null;
		try {
			elevations = this.getElevations(moved, this.levels, targetLevel.getLevelNumber());
		} catch (Exception e) {
			// TODO null pointer occur at pole
			// find why
			Angle lat = moved.getMinLatitude();
			Angle lon = moved.getMinLongitude();
			System.out.print("latitude : " + lat.degrees + " longitude : " + lon.degrees);
		}
		if (elevations == null) {
			return Double.MAX_VALUE;
		}

		for (int i = 0; i < latlons.size(); i++) {
			LatLon ll = latlons.get(i);
			if (ll == null) {
				continue;
			}

			Double value = Double.valueOf(0);
			try {
				// considers the rotation for movable sector
				LatLon movedLatLon = moveInv(ll);
				value = elevations.getElevation(movedLatLon.getLatitude(), movedLatLon.getLongitude());
			} catch (Exception e) {
				// TODO null pointer occur at pole
				// find why
				Angle lat = ll.getLatitude();
				Angle lon = ll.getLongitude();
				System.out.print("latitude : " + lat.degrees + " longitude : " + lon.degrees);
			}

			if (this.isTransparentValue(value)) {
				continue;
			}

			// If an elevation at the given location is available, write that
			// elevation to the destination buffer.
			// If an elevation is not available but the location is within the
			// elevation model's coverage, write the
			// elevation models extreme elevation at the location. Do nothing if
			// the location is not within the
			// elevation model's coverage.
			LatLon prev = move(ll);
			if (value != null && value != this.getMissingDataSignal()) {
				buffer[i] = value + getElevationModelParameters().offset;
			} else if (contains(prev.getLatitude(), prev.getLongitude())) {
				if (value == null) {
					buffer[i] = getExtremeElevations(sector)[0];
				} else if (mapMissingData && value == this.getMissingDataSignal()) {
					buffer[i] = this.getMissingDataReplacement();
				}
			}
		}

		return elevations.getAchievedResolution();
	}

	@Override
	protected Elevations getElevations(Sector requestedSector, LevelSet levelSet, int targetLevelNumber) {
		// Compute the intersection of the requested sector with the LevelSet's
		// sector.
		// The intersection will be used to determine which Tiles in the
		// LevelSet are in the requested sector.
		Sector sector = requestedSector.intersection(levelSet.getSector());

		Level targetLevel = levelSet.getLevel(targetLevelNumber);
		LatLon delta = targetLevel.getTileDelta();
		LatLon origin = levelSet.getTileOrigin();
		final int nwRow = Tile.computeRow(delta.getLatitude(), sector.getMaxLatitude(), origin.getLatitude());
		final int nwCol = Tile.computeColumn(delta.getLongitude(), sector.getMinLongitude(), origin.getLongitude());
		final int seRow = Tile.computeRow(delta.getLatitude(), sector.getMinLatitude(), origin.getLatitude());
		final int seCol = Tile.computeColumn(delta.getLongitude(), sector.getMaxLongitude(), origin.getLongitude());

		java.util.TreeSet<ElevationTile> tiles = new java.util.TreeSet<>(new Comparator<ElevationTile>() {
			@Override
			public int compare(ElevationTile t1, ElevationTile t2) {
				if (t2.getLevelNumber() == t1.getLevelNumber() && t2.getRow() == t1.getRow()
						&& t2.getColumn() == t1.getColumn()) {
					return 0;
				}

				// Higher-res levels compare lower than lower-res
				return t1.getLevelNumber() > t2.getLevelNumber() ? -1 : 1;
			}
		});
		ArrayList<TileKey> requested = new ArrayList<>();

		boolean missingTargetTiles = false;
		boolean missingLevelZeroTiles = false;
		for (int row = seRow; row <= nwRow; row++) {
			for (int col = nwCol; col <= seCol; col++) {
				TileKey key = new TileKey(targetLevel.getLevelNumber(), row, col, targetLevel.getCacheName());
				ElevationTile tile = this.getTileFromMemory(key);
				if (tile != null) {
					tiles.add(tile);
					continue;
				}

				missingTargetTiles = true;
				this.requestTile(key);

				// Determine the fallback to use. Simultaneously determine a
				// fallback to request that is
				// the next resolution higher than the fallback chosen, if any.
				// This will progressively
				// refine the display until the desired resolution tile arrives.
				TileKey fallbackToRequest = null;
				TileKey fallbackKey;
				int fallbackRow = row;
				int fallbackCol = col;
				for (int fallbackLevelNum = key.getLevelNumber() - 1; fallbackLevelNum >= 0; fallbackLevelNum--) {
					fallbackRow /= 2;
					fallbackCol /= 2;
					fallbackKey = new TileKey(fallbackLevelNum, fallbackRow, fallbackCol,
							this.levels.getLevel(fallbackLevelNum).getCacheName());

					tile = this.getTileFromMemory(fallbackKey);
					if (tile != null) {
						if (!tiles.contains(tile)) {
							tiles.add(tile);
						}
						break;
					} else {
						if (fallbackLevelNum == 0) {
							missingLevelZeroTiles = true;
						}
						fallbackToRequest = fallbackKey; // keep track of lowest
						// level to request
					}
				}

				if (fallbackToRequest != null) {
					if (!requested.contains(fallbackToRequest)) {
						this.requestTile(fallbackToRequest);
						requested.add(fallbackToRequest); // keep track to avoid
						// overhead of
						// duplicte requests
					}
				}
			}
		}

		Elevations elevations;

		if (missingLevelZeroTiles || tiles.isEmpty()) {
			// Double.MAX_VALUE is a signal for no in-memory tile for a given
			// region of the sector.
			elevations = new Elevations(this, Double.MAX_VALUE);
			elevations.setTiles(tiles);
		} else if (missingTargetTiles) {
			// Use the level of the the lowest resolution found to denote the
			// resolution of this elevation set.
			// The list of tiles is sorted first by level, so use the level of
			// the list's last entry.
			elevations = new Elevations(this, tiles.last().getLevel().getTexelSize());
			elevations.setTiles(tiles);
		} else {
			elevations = new Elevations(this, tiles.last().getLevel().getTexelSize());

			// Compute the elevation extremes now that the sector is fully
			// resolved
			if (tiles != null && tiles.size() > 0) {
				elevations.setTiles(tiles);
				double[] extremes = elevations.getExtremes(requestedSector);
				if (extremes != null) {
					this.getExtremesLookupCache().add(requestedSector, extremes, 16);
				}
			}
		}

		// Check tile expiration. Memory-cached tiles are checked for expiration
		// only when an explicit, non-zero expiry
		// time has been set for the elevation model. If none has been set, the
		// expiry times of the model's individual
		// levels are used, but only for tiles in the local file cache, not
		// tiles in memory. This is to avoid incurring
		// the overhead of checking expiration of in-memory tiles, a very rarely
		// used feature.
		if (this.getExpiryTime() > 0 && this.getExpiryTime() < System.currentTimeMillis()) {
			this.checkElevationExpiration(tiles);
		}

		return elevations;
	}

	@Override
	public boolean contains(Angle latitude, Angle longitude) {
		// if (latitude == null || longitude == null) {
		// String msg = Logging.getMessage("nullValue.AngleIsNull");
		// Logging.logger().severe(msg);
		// throw new IllegalArgumentException(msg);
		// }

		return this.levels.getSector().contains(latitude, longitude);
		// return this.levels.getSector().contains(latitude, longitude);
	}

	/**
	 * Internal class to hold collections of elevation tiles that provide elevations for a specific sector.
	 */
	protected static class Elevations extends BasicElevationModel.Elevations {
		protected Elevations(MovableInterpolateElevationModel elevationModel, double achievedResolution) {
			super(elevationModel, achievedResolution);
		}

		public void setTiles(TreeSet<ElevationTile> tiles) {
			this.tiles = tiles;
		}

		@Override
		public Double getElevation(Angle latitude, Angle longitude) {
			return super.getElevation(latitude, longitude);
		}

		protected double getAchievedResolution() {
			return achievedResolution;
		}

		@Override
		protected double[] getExtremes(Sector s) {
			return super.getExtremes(s);
		}
	}

	@Override
	public MemoryCache getMemoryCache() {
		return super.getMemoryCache();
	}
}
