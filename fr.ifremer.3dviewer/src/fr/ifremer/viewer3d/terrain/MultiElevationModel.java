package fr.ifremer.viewer3d.terrain;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;
import javax.xml.xpath.XPath;

import org.geotools.coverage.processing.Operations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jogamp.common.nio.Buffers;

import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.BasicMemoryCache;
import gov.nasa.worldwind.cache.FileStore;
import gov.nasa.worldwind.cache.MemoryCache;
import gov.nasa.worldwind.exception.WWRuntimeException;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.ogc.wms.WMSCapabilities;
import gov.nasa.worldwind.terrain.AbstractElevationModel;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.util.AbsentResourceList;
import gov.nasa.worldwind.util.BufferWrapper;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.RestorableSupport;
import gov.nasa.worldwind.util.SessionCacheUtils;
import gov.nasa.worldwind.util.Tile;
import gov.nasa.worldwind.util.TileKey;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/***
 * Elevation model containing multiple date indexed elevation models Multiple
 * {@link SubElevation} could be defined {@link Operations} could be performed
 * for those SubElevations Date is set by Parent {@link MultiTextureLayer}
 * 
 * @author MORVAN
 * 
 */
public class MultiElevationModel extends AbstractElevationModel {
	/** Logger for this class. */
	private static Logger logger = LoggerFactory.getLogger(MultiElevationModel.class);

	/***
	 * Parent layer, used to define current date
	 */
	private MultiTextureLayer layer = null;

	protected String elevationDataType = AVKey.INT16;
	protected String elevationDataByteOrder = AVKey.LITTLE_ENDIAN;
	protected double detailHint = 0.0;
	protected final Object fileLock = new Object();
	protected int extremesLevel = -1;
	protected BufferWrapper extremes = null;
	protected MemoryCache extremesLookupCache;
	// Model resource properties.
	protected ScheduledExecutorService resourceRetrievalService;
	protected AbsentResourceList absentResources;
	protected static final int RESOURCE_ID_OGC_CAPABILITIES = 1;
	protected static final int DEFAULT_MAX_RESOURCE_ATTEMPTS = 3;
	protected static final int DEFAULT_MIN_RESOURCE_CHECK_INTERVAL = (int) 6e5; // 10
	// minutes

	private SubElevation current = null;

	/***
	 * List of {@link SubElevation} which are indexed by name and the date of
	 * the {@link MultiTextureLayer}
	 */
	private List<SubElevation> subElevations = new ArrayList<SubElevation>();

	private static double missingData = 1.607574272451149E-7;
	private static double missingData2 = 1.604447709846706E-7;
	private static double missingData3 = 1.5648910774462428E-6;

	/***
	 * At least one {@link SubElevation} is needed to define a
	 * MultiElevationModel {@link MultiTextureLayer} is used to define the
	 * current date
	 * 
	 * @param layer
	 * @param subElevation
	 */
	public MultiElevationModel(MultiTextureLayer layer, SubElevation subElevation) {
		this.layer = layer;
		this.setName(layer.getName());
		layer.setElevation(this);
		addSubElevation(subElevation);
	}

	/***
	 * Multiple {@link SubElevation} could be defined Operations could be
	 * performed between those SubElevation
	 * 
	 * @param subElevation
	 */
	public void addSubElevation(SubElevation subElevation) {
		if (subElevation == null) {
			return;
		}

		subElevations.add(subElevation);
	}

	// /Begin BasicElevationModel params are defined in SubElevation

	@Override
	/***
	 * Max elevation of the current {@link SubElevation}
	 */
	public double getMaxElevation() {
		double value = missingDataValue;
		for (SubElevation subElevation : subElevations) {
			// if several dates
			if (layer.getDateMin() != layer.getDateMax()) {
				current = subElevation;
			}
			try {
				value = subElevation.getElevation(layer.getDateIndex()).getMaxElevation();
				if (value > -11000 && value < 11000 && value != missingData && value != missingData2 && value != missingData3) {
					if (value != 9000.0) {
						return value;
					}
				}
			} catch (Exception e) {
				logger.error("SubElevation is not available");
			}
		}
		return missingDataValue;
	}

	@Override
	/***
	 * Min elevation of the current {@link SubElevation}
	 */
	public double getMinElevation() {
		double value = missingDataValue;
		for (SubElevation subElevation : subElevations) {
			// if several dates
			if (layer.getDateMin() != layer.getDateMax()) {
				current = subElevation;
			}
			try {
				value = subElevation.getElevation(layer.getDateIndex()).getMinElevation();
				if (value > -11000 && value < 11000 && value != missingData && value != missingData2 && value != missingData3) {
					if (value != 9000.0) {
						return value;
					}
				}
			} catch (Exception e) {
				logger.error("SubElevation is not available");
			}
		}
		return missingDataValue;
	}

	// /End BasicElevationModel params are defined in SubElevation

	public LevelSet getLevels() {
		return layer.getLevels();
	}

	@Override
	public int intersects(Sector sector) {
		if (getLevels().getSector().contains(sector)) {
			return 0;
		}

		return getLevels().getSector().intersects(sector) ? 1 : -1;
	}

	@Override
	public boolean contains(Angle latitude, Angle longitude) {
		if (latitude == null || longitude == null) {
			String msg = Logging.getMessage("nullValue.AngleIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		return getLevels().getSector().contains(latitude, longitude);
	}

	@Override
	public void dispose() {
		WorldWind.removePropertyChangeListener(WorldWind.SHUTDOWN_EVENT, this);
		this.stopResourceRetrieval();
	}

	@Override
	public Object setValue(String key, Object value) {
		// Offer it to the level set
		if (this.getLevels() != null) {
			this.getLevels().setValue(key, value);
		}

		return super.setValue(key, value);
	}

	@Override
	public Object getValue(String key) {
		Object value = super.getValue(key);

		return value != null ? value : this.getLevels().getValue(key); // see if
		// the
		// level
		// set
		// has
		// it
	}

	protected static void setFallbacks(AVList params) {
		if (params.getValue(AVKey.TILE_WIDTH) == null) {
			params.setValue(AVKey.TILE_WIDTH, 150);
		}

		if (params.getValue(AVKey.TILE_HEIGHT) == null) {
			params.setValue(AVKey.TILE_HEIGHT, 150);
		}

		if (params.getValue(AVKey.FORMAT_SUFFIX) == null) {
			params.setValue(AVKey.FORMAT_SUFFIX, ".bil");
		}

		if (params.getValue(AVKey.NUM_LEVELS) == null) {
			params.setValue(AVKey.NUM_LEVELS, 2);
		}

		if (params.getValue(AVKey.NUM_EMPTY_LEVELS) == null) {
			params.setValue(AVKey.NUM_EMPTY_LEVELS, 0);
		}
	}

	protected MemoryCache getMemoryCache() {
		if (current != null) {
			return current.getElevation(layer.getDateIndex()).getMemoryCache();
		}
		return null;
	}

	protected int getExtremesLevel() {
		return extremesLevel;
	}

	protected BufferWrapper getExtremes() {
		return extremes;
	}

	@Override
	public double getBestResolution(Sector sector) {
		if (sector == null) {
			return getLevels().getLastLevel().getTexelSize();
		}

		Level level = getLevels().getLastLevel(sector);
		return level != null ? level.getTexelSize() : Double.MAX_VALUE;
	}

	@Override
	public double getDetailHint(Sector sector) {
		return this.detailHint;
	}

	public void setDetailHint(double hint) {
		this.detailHint = hint;
	}

	public String getElevationDataType() {
		return this.elevationDataType;
	}

	public void setElevationDataType(String dataType) {
		if (dataType == null) {
			String message = Logging.getMessage("nullValue.DataTypeIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		this.elevationDataType = dataType;
	}

	public String getElevationDataByteOrder() {
		return this.elevationDataByteOrder;
	}

	public void setByteOrder(String byteOrder) {
		if (byteOrder == null) {
			String message = Logging.getMessage("nullValue.ByteOrderIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		this.elevationDataByteOrder = byteOrder;
	}

	protected boolean isFileExpired(Tile tile, java.net.URL fileURL, FileStore fileStore) {
		if (!WWIO.isFileOutOfDate(fileURL, tile.getLevel().getExpiryTime())) {
			return false;
		}

		// The file has expired. Delete it.
		fileStore.removeFile(fileURL);
		String message = Logging.getMessage("generic.DataFileExpired", fileURL);
		Logging.logger().fine(message);
		return true;
	}

	// Read elevations from the file cache. Don't be confused by the use of a
	// URL here: it's used so that files can
	// be read using System.getResource(URL), which will draw the data from a
	// jar file in the classpath.

	protected BufferWrapper readElevations(URL url) throws IOException {
		try {
			ByteBuffer byteBuffer;
			synchronized (this.fileLock) {
				byteBuffer = WWIO.readURLContentToBuffer(url);
			}

			// Setup parameters to instruct BufferWrapper on how to interpret
			// the ByteBuffer.
			AVList bufferParams = new AVListImpl();
			bufferParams.setValue(AVKey.DATA_TYPE, this.elevationDataType);
			bufferParams.setValue(AVKey.BYTE_ORDER, this.elevationDataByteOrder);
			return BufferWrapper.wrap(byteBuffer, bufferParams);
		} catch (java.io.IOException e) {
			Logging.logger().log(java.util.logging.Level.SEVERE, "ElevationModel.ExceptionReadingElevationFile", url.toString());
			throw e;
		}
	}

	protected static ByteBuffer convertImageToElevations(ByteBuffer buffer, String contentType) throws IOException {
		File tempFile = TemporaryCache.createTemporaryFile("wwj-", WWIO.makeSuffixForMimeType(contentType));
		try {
			WWIO.saveBuffer(buffer, tempFile);
			BufferedImage image = ImageIO.read(tempFile);
			ByteBuffer byteBuffer = Buffers.newDirectByteBuffer(image.getWidth() * image.getHeight() * 2);
			byteBuffer.order(java.nio.ByteOrder.LITTLE_ENDIAN);
			ShortBuffer bilBuffer = byteBuffer.asShortBuffer();

			WritableRaster raster = image.getRaster();
			int[] samples = new int[raster.getWidth() * raster.getHeight()];
			raster.getSamples(0, 0, raster.getWidth(), raster.getHeight(), 0, samples);
			for (int sample : samples) {
				bilBuffer.put((short) sample);
			}

			return byteBuffer;
		} finally {
			if (tempFile != null) {
				// noinspection ResultOfMethodCallIgnored
				tempFile.delete();
			}
		}
	}

	protected void determineExtremes(double value, double extremes[]) {
		if (value == this.getMissingDataSignal()) {
			value = this.getMissingDataReplacement();
		}

		if (value < extremes[0]) {
			extremes[0] = value;
		}

		if (value > extremes[1]) {
			extremes[1] = value;
		}
	}

	@Override
	public double getUnmappedElevation(Angle latitude, Angle longitude) {
		double value = missingDataValue;
		for (SubElevation subElevation : subElevations) {
			if (subElevation.getElevation(layer.getDateIndex()) != null) {
				value = subElevation.getElevation(layer.getDateIndex()).getUnmappedElevation(latitude, longitude);
				if (value > -11000 && value < 11000 && value != missingData && value != missingData2 && value != missingData3) {
					if (value != 9000.0) {
						return value;
					}
				}
			}
		}
		return missingDataValue;
	}

	@Override
	public double getElevations(Sector sector, List<? extends LatLon> latlons, double targetResolution, double[] buffer) {
		double value = missingDataValue;
		for (SubElevation subElevation : subElevations) {
			// if several dates
			if (layer.getDateMin() != layer.getDateMax()) {
				current = subElevation;
			}
			value = subElevation.getElevation(layer.getDateIndex()).getElevations(sector, latlons, targetResolution, buffer);
			if (value > -11000 && value < 11000 && value != missingData && value != missingData2 && value != missingData3) {
				if (value != 9000.0) {
					return value;
				}
			}
		}
		return missingDataValue;
	}

	@Override
	public double getUnmappedElevations(Sector sector, List<? extends LatLon> latlons, double targetResolution, double[] buffer) {
		double value = missingDataValue;

		// if several dates or mosaicing
		if (layer.getDateMin() != layer.getDateMax() || layer.getLayerParameters().isDisplaySameType()) {
			for (SubElevation subElevation : subElevations) {
				current = subElevation;
				value = subElevation.getElevation(layer.getDateIndex()).getUnmappedElevations(sector, latlons, targetResolution, buffer);
				if (value > -11000 && value < 11000 && value != missingData && value != missingData2 && value != missingData3) {
					if (value != 9000.0) {
						return value;
					}
				}
			}
			return missingDataValue;
		} else {
			{
				value = current.getElevation(layer.getDateIndex()).getUnmappedElevations(sector, latlons, targetResolution, buffer);
				if (value > -11000 && value < 11000 && value != missingData && value != missingData2 && value != missingData3) {
					if (value != 9000.0) {
						return value;
					}
				}
			}
			return missingDataValue;
		}
	}

	protected Level getTargetLevel(Sector sector, double targetSize) {
		Level lastLevel = getLevels().getLastLevel(sector); // finest resolution
		// available
		if (lastLevel == null) {
			return null;
		}

		if (lastLevel.getTexelSize() >= targetSize) {
			return lastLevel; // can't do any better than this
		}

		for (Level level : getLevels().getLevels()) {
			if (level.getTexelSize() <= targetSize) {
				return !level.isEmpty() ? level : null;
			}

			if (level == lastLevel) {
				break;
			}
		}

		return lastLevel;
	}

	@Override
	public double[] getExtremeElevations(Angle latitude, Angle longitude) {
		return null;
	}

	@Override
	public double[] getExtremeElevations(Sector sector) {
		if (sector == null) {
			String message = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		try {
			double[] extremes = (double[]) this.getExtremesLookupCache().getObject(sector);
			if (extremes != null) {
				return new double[] { extremes[0], extremes[1] }; // return
				// defensive
				// copy
			}

			if (this.extremesLevel < 0 || this.extremes == null) {
				return new double[] { this.getMinElevation(), this.getMaxElevation() };
			}

			// Compute the extremes from the extreme-elevations file, but don't
			// cache them. Only extremes computed from
			// fully resolved elevation tiles are cached. This ensures that the
			// extreme values accurately reflect the
			// extremes of the sector, which is critical for bounding volume
			// creation and thereby performance.
			extremes = this.computeExtremeElevations(sector);
			if (extremes != null) {
				this.getExtremesLookupCache().add(sector, extremes, 16);
			}

			// Return a defensive copy of the array to prevent the caller from
			// modifying the cache contents.
			return extremes != null ? new double[] { extremes[0], extremes[1] } : null;
		} catch (Exception e) {
			String message = Logging.getMessage("BasicElevationModel.ExceptionDeterminingExtremes", sector);
			Logging.logger().log(java.util.logging.Level.WARNING, message, e);

			return new double[] { this.getMinElevation(), this.getMaxElevation() };
		}
	}

	public void loadExtremeElevations(String extremesFileName) {
		if (extremesFileName == null) {
			String message = Logging.getMessage("nullValue.ExtremeElevationsFileName");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		InputStream is = null;
		try {
			is = this.getClass().getResourceAsStream("/" + extremesFileName);
			if (is == null) {
				// Look directly in the file system
				File file = new File(extremesFileName);
				if (file.exists()) {
					is = new FileInputStream(file);
				} else {
					Logging.logger().log(java.util.logging.Level.WARNING, "BasicElevationModel.UnavailableExtremesFile", extremesFileName);
				}
			}

			if (is == null) {
				return;
			}

			// The level the extremes were taken from is encoded as the last
			// element in the file name
			String[] tokens = extremesFileName.substring(0, extremesFileName.lastIndexOf(".")).split("_");
			this.extremesLevel = Integer.parseInt(tokens[tokens.length - 1]);
			if (this.extremesLevel < 0) {
				this.extremes = null;
				Logging.logger().log(java.util.logging.Level.WARNING, "BasicElevationModel.UnavailableExtremesLevel", extremesFileName);
				return;
			}

			AVList bufferParams = new AVListImpl();
			bufferParams.setValue(AVKey.DATA_TYPE, AVKey.INT16);
			bufferParams.setValue(AVKey.BYTE_ORDER, AVKey.BIG_ENDIAN); // Extremes
			// are
			// always
			// saved
			// in
			// JVM
			// byte
			// order
			this.extremes = BufferWrapper.wrap(WWIO.readStreamToBuffer(is, true), bufferParams); // Read
			// extremes
			// to
			// a
			// direct
			// ByteBuffer.
		} catch (FileNotFoundException e) {
			Logging.logger().log(java.util.logging.Level.WARNING, Logging.getMessage("BasicElevationModel.ExceptionReadingExtremeElevations", extremesFileName), e);
			this.extremes = null;
			this.extremesLevel = -1;
			this.extremesLookupCache = null;
		} catch (IOException e) {
			Logging.logger().log(java.util.logging.Level.WARNING, Logging.getMessage("BasicElevationModel.ExceptionReadingExtremeElevations", extremesFileName), e);
			this.extremes = null;
			this.extremesLevel = -1;
			this.extremesLookupCache = null;
		} finally {
			WWIO.closeStream(is, extremesFileName);

			// Clear the extreme elevations lookup cache.
			if (this.extremesLookupCache != null) {
				this.extremesLookupCache.clear();
			}
		}
	}

	protected double[] computeExtremeElevations(Sector sector) {
		return null;
	}

	/**
	 * Returns the memory cache used to cache extreme elevation computations,
	 * initializing the cache if it doesn't yet exist. This is an instance level
	 * cache: each instance of BasicElevationModel has its own instance of an
	 * extreme elevations lookup cache.
	 * 
	 * @return the memory cache associated with the extreme elevations
	 *         computations.
	 */
	protected synchronized MemoryCache getExtremesLookupCache() {
		// Note that the extremes lookup cache does not belong to the WorldWind
		// memory cache set, therefore it will not
		// be automatically cleared and disposed when World Wind is shutdown.
		// However, since the extremes lookup cache
		// is a local reference to this elevation model, it will be reclaimed by
		// the JVM garbage collector when this
		// elevation model is reclaimed by the GC.

		if (this.extremesLookupCache == null) {
			// Default cache size holds 1250 min/max pairs. This size was
			// experimentally determined to hold enough
			// value lookups to prevent cache thrashing.
			long size = Configuration.getLongValue(AVKey.ELEVATION_EXTREMES_LOOKUP_CACHE_SIZE, 20000L);
			this.extremesLookupCache = new BasicMemoryCache((long) (0.85 * size), size);
		}

		return this.extremesLookupCache;
	}

	public ByteBuffer generateExtremeElevations(int levelNumber) {
		return null;
		// long waitTime = 1000;
		// long timeout = 10 * 60 * 1000;
		//
		// ElevationModel.Elevations elevs;
		// BasicElevationModel em = new EarthElevationModel();
		//
		// double delta = 20d / Math.pow(2, levelNumber);
		//
		// int numLats = (int) Math.ceil(180 / delta);
		// int numLons = (int) Math.ceil(360 / delta);
		//
		// System.out.printf("Building extreme elevations for layer %d, num lats %d, num lons %d\n",
		// levelNumber, numLats, numLons);
		//
		// ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2 * 2 * numLats *
		// numLons);
		// ShortBuffer buffer = byteBuffer.asShortBuffer();
		// buffer.rewind();
		//
		// Level level = getLevels().getLevel(levelNumber);
		// for (int j = 0; j < numLats; j++)
		// {
		// double lat = -90 + j * delta;
		// for (int i = 0; i < numLons; i++)
		// {
		// double lon = -180 + i * delta;
		// Sector s = Sector.fromDegrees(lat, lat + delta, lon, lon + delta);
		// long startTime = System.currentTimeMillis();
		// while ((elevs = em.getElevations(s, level)) == null)
		// {
		// try
		// {
		// Thread.sleep(waitTime);
		// }
		// catch (InterruptedException e)
		// {
		// e.printStackTrace();
		// }
		// if (System.currentTimeMillis() - startTime >= timeout)
		// break;
		// }
		//
		// if (elevs == null)
		// {
		// System.out.printf("null elevations for (%f, %f) %s\n", lat, lon, s);
		// continue;
		// }
		//
		//
		// double[] extremes = elevs.getExtremes();
		// if (extremes != null)
		// {
		// System.out.printf("%d:%d, (%f, %f) min = %f, max = %f\n", j, i, lat,
		// lon, extremes[0], extremes[1]);
		// buffer.put((short) extremes[0]).put((short) extremes[1]);
		// }
		// else
		// System.out.printf("no extremes for (%f, %f)\n", lat, lon);
		// }
		// }
		//
		// return (ByteBuffer) buffer.rewind();
	}

	//
	// public final int getTileCount(Sector sector, int resolution)
	// {
	// if (sector == null)
	// {
	// String msg = Logging.getMessage("nullValue.SectorIsNull");
	// Logging.logger().severe(msg);
	// throw new IllegalArgumentException(msg);
	// }
	//
	// // Collect all the elevation tiles intersecting the input sector. If a
	// desired tile is not curently
	// // available, choose its next lowest resolution parent that is available.
	// final Level targetLevel = getLevels().getLevel(resolution);
	//
	// LatLon delta = getLevels().getLevel(resolution).getTileDelta();
	// final int nwRow = Tile.computeRow(delta.getLatitude(),
	// sector.getMaxLatitude());
	// final int nwCol = Tile.computeColumn(delta.getLongitude(),
	// sector.getMinLongitude());
	// final int seRow = Tile.computeRow(delta.getLatitude(),
	// sector.getMinLatitude());
	// final int seCol = Tile.computeColumn(delta.getLongitude(),
	// sector.getMaxLongitude());
	//
	// return (1 + (nwRow - seRow) * (1 + seCol - nwCol));
	// }

	// **************************************************************//
	// ******************** Non-Tile Resource Retrieval ***********//
	// **************************************************************//

	/**
	 * Retrieves any non-tile resources associated with this ElevationModel,
	 * either online or in the local filesystem, and initializes properties of
	 * this ElevationModel using those resources. This returns a key indicating
	 * the retrieval state:
	 * {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_SUCCESSFUL}
	 * indicates the retrieval succeeded,
	 * {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_ERROR} indicates
	 * the retrieval failed with errors, and <code>null</code> indicates the
	 * retrieval state is unknown. This method may invoke blocking I/O
	 * operations, and therefore should not be executed from the rendering
	 * thread.
	 * 
	 * @return {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_SUCCESSFUL}
	 *         if the retrieval succeeded,
	 *         {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_ERROR} if
	 *         the retrieval failed with errors, and <code>null</code> if the
	 *         retrieval state is unknown.
	 */
	protected String retrieveResources() {
		// This ElevationModel has no construction parameters, so there is no
		// description of what to retrieve. Return a
		// key indicating the resources have been successfully retrieved, though
		// there is nothing to retrieve.
		AVList params = (AVList) this.getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (params == null) {
			String message = Logging.getMessage("nullValue.ConstructionParametersIsNull");
			Logging.logger().warning(message);
			return AVKey.RETRIEVAL_STATE_SUCCESSFUL;
		}

		// This ElevationModel has no OGC Capabilities URL in its construction
		// parameters. Return a key indicating the
		// resources have been successfully retrieved, though there is nothing
		// to retrieve.
		URL url = DataConfigurationUtils.getOGCGetCapabilitiesURL(params);
		if (url == null) {
			String message = Logging.getMessage("nullValue.CapabilitiesURLIsNull");
			Logging.logger().warning(message);
			return AVKey.RETRIEVAL_STATE_SUCCESSFUL;
		}

		// The OGC Capabilities resource is marked as absent. Return null
		// indicating that the retrieval was not
		// successful, and we should try again later.
		if (this.absentResources.isResourceAbsent(RESOURCE_ID_OGC_CAPABILITIES)) {
			return null;
		}

		// Get the service's OGC Capabilities resource from the session cache,
		// or initiate a retrieval to fetch it in
		// a separate thread.
		// SessionCacheUtils.getOrRetrieveSessionCapabilities() returns null if
		// it initiated a
		// retrieval, or if the OGC Capabilities URL is unavailable.
		//
		// Note that we use the URL's String representation as the cache key. We
		// cannot use the URL itself, because
		// the cache invokes the methods Object.hashCode() and Object.equals()
		// on the cache key. URL's implementations
		// of hashCode() and equals() perform blocking IO calls. World Wind does
		// not perform blocking calls during
		// rendering, and this method is likely to be called from the rendering
		// thread.
		WMSCapabilities caps;
		if (this.isNetworkRetrievalEnabled()) {
			caps = SessionCacheUtils.getOrRetrieveSessionCapabilities(url, WorldWind.getSessionCache(), url.toString(), this.absentResources, RESOURCE_ID_OGC_CAPABILITIES, null, null);
		} else {
			caps = SessionCacheUtils.getSessionCapabilities(WorldWind.getSessionCache(), url.toString(), url.toString());
		}

		// The OGC Capabilities resource retrieval is either currently running
		// in another thread, or has failed. In
		// either case, return null indicating that that the retrieval was not
		// successful, and we should try again
		// later.
		if (caps == null) {
			return null;
		}

		// We have sucessfully retrieved this ElevationModel's OGC Capabilities
		// resource. Intialize this ElevationModel
		// using the Capabilities document, and return a key indicating the
		// retrieval has succeeded.
		this.initFromOGCCapabilitiesResource(caps, params);

		return AVKey.RETRIEVAL_STATE_SUCCESSFUL;
	}

	/**
	 * Initializes this ElevationModel's expiry time property from the specified
	 * WMS Capabilities document and parameter list describing the WMS layer
	 * names associated with this ElevationModel. This method is thread safe; it
	 * synchronizes changes to this ElevationModel by wrapping the appropriate
	 * method calls in {@link SwingUtilities#invokeLater(Runnable)}.
	 * 
	 * @param caps
	 *            the WMS Capabilities document retrieved from this
	 *            ElevationModel's WMS server.
	 * @param params
	 *            the parameter list describing the WMS layer names associated
	 *            with this ElevationModel.
	 * 
	 * @throws IllegalArgumentException
	 *             if either the Capabilities or the parameter list is null.
	 */
	protected void initFromOGCCapabilitiesResource(WMSCapabilities caps, AVList params) {
		if (caps == null) {
			String message = Logging.getMessage("nullValue.CapabilitiesIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			String message = Logging.getMessage("nullValue.ParametersIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		String[] names = DataConfigurationUtils.getOGCLayerNames(params);
		if (names == null || names.length == 0) {
			return;
		}

		final Long expiryTime = caps.getLayerLatestLastUpdateTime( names);
		if (expiryTime == null) {
			return;
		}

		// Synchronize changes to this ElevationModel with the Event Dispatch
		// Thread.
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MultiElevationModel.this.setExpiryTime(expiryTime);
				MultiElevationModel.this.firePropertyChange(AVKey.ELEVATION_MODEL, null, MultiElevationModel.this);
			}
		});
	}

	/**
	 * Returns a boolean value indicating if this ElevationModel should retrieve
	 * any non-tile resources, either online or in the local filesystem, and
	 * initialize itself using those resources.
	 * 
	 * @return <code>true</code> if this ElevationModel should retrieve any
	 *         non-tile resources, and <code>false</code> otherwise.
	 */
	protected boolean isRetrieveResources() {
		AVList params = (AVList) this.getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (params == null) {
			return false;
		}

		Boolean b = (Boolean) params.getValue(AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE);
		return b != null && b;
	}

	/**
	 * Starts retrieving non-tile resources associated with this ElevationModel
	 * in a non-rendering thread. By default, this schedules a task immediately
	 * to retrieve those resources, and then every 10 seconds thereafter until
	 * the retrieval succeeds.
	 * <p/>
	 * If this method is invoked while any non-tile resource tasks are running
	 * or pending, this cancels any pending tasks (but allows any running tasks
	 * to finish).
	 */
	protected void startResourceRetrieval() {
		// Configure an AbsentResourceList with the specified number of max
		// retrieval attempts, and the smallest
		// possible min attempt interval. We specify a small attempt interval
		// because the resource retrieval service
		// itself schedules the tasks at our specified interval. We therefore
		// want to bypass AbsentResourceLists's
		// internal timing scheme.
		this.absentResources = new AbsentResourceList(DEFAULT_MAX_RESOURCE_ATTEMPTS, 1);

		// Stop any pending resource retrieval tasks.
		if (this.resourceRetrievalService != null) {
			this.resourceRetrievalService.shutdown();
		}

		// Schedule a task to retrieve non-tile resources immediately, then at
		// intervals thereafter.
		Runnable task = this.createResourceRetrievalTask();
		String taskName = Logging.getMessage("BasicElevationModel.ResourceRetrieverThreadName", this.getName());
		this.resourceRetrievalService = DataConfigurationUtils.createResourceRetrievalService(taskName);
		this.resourceRetrievalService.scheduleAtFixedRate(task, 0, DEFAULT_MIN_RESOURCE_CHECK_INTERVAL, TimeUnit.MILLISECONDS);
	}

	/**
	 * Cancels any pending non-tile resource retrieval tasks, and allows any
	 * running tasks to finish.
	 */
	protected void stopResourceRetrieval() {
		if (this.resourceRetrievalService != null) {
			this.resourceRetrievalService.shutdownNow();
			this.resourceRetrievalService = null;
		}
	}

	/**
	 * Returns a Runnable task which retrieves any non-tile resources associated
	 * with a specified ElevationModel in it's run method. This task is used by
	 * the ElevationModel to schedule periodic resource checks. If the task's
	 * run method throws an Exception, it will no longer be scheduled for
	 * execution. By default, this returns a reference to a new
	 * {@link ResourceRetrievalTask}.
	 * 
	 * @return Runnable who's run method retrieves non-tile resources.
	 */
	protected Runnable createResourceRetrievalTask() {
		return new ResourceRetrievalTask(this);
	}

	/**
	 * ResourceRetrievalTask retrieves any non-tile resources associated with
	 * this ElevationModel in it's run method.
	 */
	protected static class ResourceRetrievalTask implements Runnable {
		protected MultiElevationModel em;

		/**
		 * Constructs a new ResourceRetrievalTask, but otherwise does nothing.
		 * 
		 * @param em
		 *            the BasicElevationModel who's non-tile resources should be
		 *            retrieved in the run method.
		 * 
		 * @throws IllegalArgumentException
		 *             if the elevation model is null.
		 */
		public ResourceRetrievalTask(MultiElevationModel em) {
			if (em == null) {
				String message = Logging.getMessage("nullValue.ElevationModelIsNull");
				Logging.logger().severe(message);
				throw new IllegalArgumentException(message);
			}

			this.em = em;
		}

		/**
		 * Returns the elevation model who's non-tile resources are retrieved by
		 * this ResourceRetrievalTask
		 * 
		 * @return the elevation model who's non-tile resources are retireved.
		 */
		public MultiElevationModel getElevationModel() {
			return this.em;
		}

		/**
		 * Retrieves any non-tile resources associated with the specified
		 * ElevationModel, and cancels any pending retrieval tasks if the
		 * retrieval succeeds, or if an exception is thrown during retrieval.
		 */
		@Override
		public void run() {
			try {
				this.retrieveResources();
			} catch (Throwable t) {
				this.handleUncaughtException(t);
			}
		}

		/**
		 * Invokes {@link BasicElevationModel#retrieveResources()}, and cancels
		 * any pending retrieval tasks if the call returns
		 * {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_SUCCESSFUL}.
		 */
		protected void retrieveResources() {
			String state = this.em.retrieveResources();

			if (state != null && state.equals(AVKey.RETRIEVAL_STATE_SUCCESSFUL)) {
				this.em.stopResourceRetrieval();
			}
		}

		/**
		 * Logs a message describing the uncaught exception thrown during a call
		 * to run, and cancels any pending retrieval tasks.
		 * 
		 * @param t
		 *            the uncaught exception.
		 */
		protected void handleUncaughtException(Throwable t) {
			String message = Logging.getMessage("BasicElevationModel.ExceptionRetrievingResources", this.em.getName());
			Logging.logger().log(java.util.logging.Level.FINE, message, t);

			this.em.stopResourceRetrieval();
		}
	}

	// **************************************************************//
	// ******************** Configuration *************************//
	// **************************************************************//

	/**
	 * Creates a configuration document for a BasicElevationModel described by
	 * the specified params. The returned document may be used as a construction
	 * parameter to {@link gov.nasa.worldwind.terrain.BasicElevationModel}.
	 * 
	 * @param params
	 *            parameters describing a BasicElevationModel.
	 * 
	 * @return a configuration document for the BasicElevationModel.
	 */
	public static Document createBasicElevationModelConfigDocument(AVList params) {
		Document doc = WWXML.createDocumentBuilder(true).newDocument();

		Element root = WWXML.setDocumentElement(doc, "ElevationModel");
		// Note: no type attribute denotes the default elevation model, which
		// currently is BasicElevationModel.
		WWXML.setIntegerAttribute(root, "version", 1);

		createBasicElevationModelConfigElements(params, root);

		return doc;
	}

	/**
	 * Appends BasicElevationModel configuration parameters as elements to the
	 * specified context. This appends elements for the following parameters:
	 * <table>
	 * <th>
	 * <td>Parameter</td>
	 * <td>Element Path</td>
	 * <td>Type</td></th>
	 * <tr>
	 * <td>{@link AVKey#SERVICE_NAME}</td>
	 * <td>Service/@serviceName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#IMAGE_FORMAT}</td>
	 * <td>ImageFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#AVAILABLE_IMAGE_FORMATS}</td>
	 * <td>AvailableImageFormats/ImageFormat</td>
	 * <td>String array</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#DATA_TYPE}</td>
	 * <td>DataType/@type</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#BYTE_ORDER}</td>
	 * <td>ByteOrder</td>
	 * <td>DataType/@byteOrder</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#ELEVATION_EXTREMES_FILE}</td>
	 * <td>ExtremeElevations/FileName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#ELEVATION_MAX}</td>
	 * <td>ExtremeElevations/@max</td>
	 * <td>Double</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#ELEVATION_MIN}</td>
	 * <td>ExtremeElevations/@min</td>
	 * <td>Double</td>
	 * </tr>
	 * </table>
	 * This also writes common elevation model and LevelSet configuration
	 * parameters by invoking
	 * {@link gov.nasa.worldwind.terrain.AbstractElevationModel#createElevationModelConfigElements(gov.nasa.worldwind.avlist.AVList, org.w3c.dom.Element)}
	 * and
	 * {@link DataConfigurationUtils#createLevelSetConfigElements(gov.nasa.worldwind.avlist.AVList, org.w3c.dom.Element)}
	 * .
	 * 
	 * @param params
	 *            the key-value pairs which define the BasicElevationModel
	 *            configuration parameters.
	 * @param context
	 *            the XML document root on which to append BasicElevationModel
	 *            configuration elements.
	 * 
	 * @return a reference to context.
	 * 
	 * @throws IllegalArgumentException
	 *             if either the parameters or the context are null.
	 */
	public static Element createBasicElevationModelConfigElements(AVList params, Element context) {
		if (params == null) {
			String message = Logging.getMessage("nullValue.ParametersIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (context == null) {
			String message = Logging.getMessage("nullValue.ContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		XPath xpath = WWXML.makeXPath();

		// Common elevation model properties.
		AbstractElevationModel.createElevationModelConfigElements(params, context);

		// LevelSet properties.
		DataConfigurationUtils.createLevelSetConfigElements(params, context);

		// Service properties.
		// Try to get the SERVICE_NAME property, but default to "WWTileService".
		String s = AVListImpl.getStringValue(params, AVKey.SERVICE_NAME, "WWTileService");
		if (s != null && s.length() > 0) {
			// The service element may already exist, in which case we want to
			// append to it.
			Element el = WWXML.getElement(context, "Service", xpath);
			if (el == null) {
				el = WWXML.appendElementPath(context, "Service");
			}
			WWXML.setTextAttribute(el, "serviceName", s);
		}

		WWXML.checkAndAppendBooleanElement(params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE, context, "RetrievePropertiesFromService");

		// Image format properties.
		WWXML.checkAndAppendTextElement(params, AVKey.IMAGE_FORMAT, context, "ImageFormat");

		Object o = params.getValue(AVKey.AVAILABLE_IMAGE_FORMATS);
		if (o != null && o instanceof String[]) {
			String[] strings = (String[]) o;
			if (strings.length > 0) {
				// The available image formats element may already exists, in
				// which case we want to append to it, rather
				// than create entirely separate paths.
				Element el = WWXML.getElement(context, "AvailableImageFormats", xpath);
				if (el == null) {
					el = WWXML.appendElementPath(context, "AvailableImageFormats");
				}
				WWXML.appendTextArray(el, "ImageFormat", strings);
			}
		}

		// Data type properties.
		if (params.getValue(AVKey.DATA_TYPE) != null || params.getValue(AVKey.BYTE_ORDER) != null) {
			Element el = WWXML.getElement(context, "DataType", null);
			if (el == null) {
				el = WWXML.appendElementPath(context, "DataType");
			}

			s = params.getStringValue(AVKey.DATA_TYPE);
			if (s != null && s.length() > 0) {
				s = WWXML.dataTypeAsText(s);
				if (s != null && s.length() > 0) {
					WWXML.setTextAttribute(el, "type", s);
				}
			}

			s = params.getStringValue(AVKey.BYTE_ORDER);
			if (s != null && s.length() > 0) {
				s = WWXML.byteOrderAsText(s);
				if (s != null && s.length() > 0) {
					WWXML.setTextAttribute(el, "byteOrder", s);
				}
			}
		}

		// Elevation data properties.
		Element el = WWXML.appendElementPath(context, "ExtremeElevations");
		WWXML.checkAndAppendTextElement(params, AVKey.ELEVATION_EXTREMES_FILE, el, "FileName");

		Double d = AVListImpl.getDoubleValue(params, AVKey.ELEVATION_MAX);
		if (d != null) {
			WWXML.setDoubleAttribute(el, "max", d);
		}

		d = AVListImpl.getDoubleValue(params, AVKey.ELEVATION_MIN);
		if (d != null) {
			WWXML.setDoubleAttribute(el, "min", d);
		}

		return context;
	}

	/**
	 * Parses BasicElevationModel parameters from a specified DOM document. This
	 * writes output as key-value pairs to params. If a parameter from the XML
	 * document already exists in params, that parameter is ignored. Supported
	 * key and parameter names are:
	 * <table>
	 * <th>
	 * <td>Parameter</td>
	 * <td>Element Path</td>
	 * <td>Type</td></th>
	 * <tr>
	 * <td>{@link AVKey#SERVICE_NAME}</td>
	 * <td>Service/@serviceName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#IMAGE_FORMAT}</td>
	 * <td>ImageFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#AVAILABLE_IMAGE_FORMATS}</td>
	 * <td>AvailableImageFormats/ImageFormat</td>
	 * <td>String array</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#DATA_TYPE}</td>
	 * <td>DataType/@type</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#BYTE_ORDER}</td>
	 * <td>DataType/@byteOrder</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#ELEVATION_EXTREMES_FILE}</td>
	 * <td>ExtremeElevations/FileName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#ELEVATION_MAX}</td>
	 * <td>ExtremeElevations/@max</td>
	 * <td>Double</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#ELEVATION_MIN}</td>
	 * <td>ExtremeElevations/@min</td>
	 * <td>Double</td>
	 * </tr>
	 * </table>
	 * This also parses common elevation model and LevelSet configuration
	 * parameters by invoking
	 * {@link gov.nasa.worldwind.terrain.AbstractElevationModel#getElevationModelConfigParams(org.w3c.dom.Element, gov.nasa.worldwind.avlist.AVList)}
	 * and
	 * {@link gov.nasa.worldwind.util.DataConfigurationUtils#getLevelSetConfigParams(org.w3c.dom.Element, gov.nasa.worldwind.avlist.AVList)}
	 * .
	 * 
	 * @param domElement
	 *            the XML document root to parse for BasicElevationModel
	 *            configuration parameters.
	 * @param params
	 *            the output key-value pairs which recieve the
	 *            BasicElevationModel configuration parameters. A null reference
	 *            is permitted.
	 * 
	 * @return a reference to params, or a new AVList if params is null.
	 * 
	 * @throws IllegalArgumentException
	 *             if the document is null.
	 */
	public static AVList getBasicElevationModelConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		XPath xpath = WWXML.makeXPath();

		// Common elevation model properties.
		AbstractElevationModel.getElevationModelConfigParams(domElement, params);

		// LevelSet properties.
		DataConfigurationUtils.getLevelSetConfigParams(domElement, params);

		// Service properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.SERVICE_NAME, "Service/@serviceName", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE, "RetrievePropertiesFromService", xpath);

		// Image format properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.IMAGE_FORMAT, "ImageFormat", xpath);
		WWXML.checkAndSetUniqueStringsParam(domElement, params, AVKey.AVAILABLE_IMAGE_FORMATS, "AvailableImageFormats/ImageFormat", xpath);

		// Data type properties.
		if (params.getValue(AVKey.DATA_TYPE) == null) {
			String s = WWXML.getText(domElement, "DataType/@type", xpath);
			if (s != null && s.length() > 0) {
				s = WWXML.parseDataType(s);
				if (s != null && s.length() > 0) {
					params.setValue(AVKey.DATA_TYPE, s);
				}
			}
		}

		if (params.getValue(AVKey.BYTE_ORDER) == null) {
			String s = WWXML.getText(domElement, "DataType/@byteOrder", xpath);
			if (s != null && s.length() > 0) {
				s = WWXML.parseByteOrder(s);
				if (s != null && s.length() > 0) {
					params.setValue(AVKey.BYTE_ORDER, s);
				}
			}
		}

		// Elevation data properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.ELEVATION_EXTREMES_FILE, "ExtremeElevations/FileName", xpath);
		WWXML.checkAndSetDoubleParam(domElement, params, AVKey.ELEVATION_MAX, "ExtremeElevations/@max", xpath);
		WWXML.checkAndSetDoubleParam(domElement, params, AVKey.ELEVATION_MIN, "ExtremeElevations/@min", xpath);

		return params;
	}

	protected void writeConfigurationFile(FileStore fileStore) {
		// TODO: configurable max attempts for creating a configuration file.

		try {
			AVList configParams = this.getConfigurationParams(null);
			this.writeConfigurationParams(configParams, fileStore);
		} catch (Exception e) {
			String message = Logging.getMessage("generic.ExceptionAttemptingToWriteConfigurationFile");
			Logging.logger().log(java.util.logging.Level.SEVERE, message, e);
		}
	}

	protected void writeConfigurationParams(AVList params, FileStore fileStore) {
		// Determine what the configuration file name should be based on the
		// configuration parameters. Assume an XML
		// configuration document type, and append the XML file suffix.
		String fileName = DataConfigurationUtils.getDataConfigFilename(params, ".xml");
		if (fileName == null) {
			String message = Logging.getMessage("nullValue.FilePathIsNull");
			Logging.logger().severe(message);
			throw new WWRuntimeException(message);
		}

		// Check if this component needs to write a configuration file. This
		// happens outside of the synchronized block
		// to improve multithreaded performance for the common case: the
		// configuration file already exists, this just
		// need to check that it's there and return. If the file exists but is
		// expired, do not remove it - this
		// removes the file inside the synchronized block below.
		if (!this.needsConfigurationFile(fileStore, fileName, params, false)) {
			return;
		}

		synchronized (this.fileLock) {
			// Check again if the component needs to write a configuration file,
			// potentially removing any existing file
			// which has expired. This additional check is necessary because the
			// file could have been created by
			// another thread while we were waiting for the lock.
			if (!this.needsConfigurationFile(fileStore, fileName, params, true)) {
				return;
			}

			this.doWriteConfigurationParams(fileStore, fileName, params);
		}
	}

	protected void doWriteConfigurationParams(FileStore fileStore, String fileName, AVList params) {
		java.io.File file = fileStore.newFile(fileName);
		if (file == null) {
			String message = Logging.getMessage("generic.CannotCreateFile", fileName);
			Logging.logger().severe(message);
			throw new WWRuntimeException(message);
		}

		Document doc = this.createConfigurationDocument(params);
		WWXML.saveDocumentToFile(doc, file.getPath());

		String message = Logging.getMessage("generic.ConfigurationFileCreated", fileName);
		Logging.logger().fine(message);
	}

	protected boolean needsConfigurationFile(FileStore fileStore, String fileName, AVList params, boolean removeIfExpired) {
		long expiryTime = this.getExpiryTime();
		if (expiryTime <= 0) {
			expiryTime = AVListImpl.getLongValue(params, AVKey.EXPIRY_TIME, 0L);
		}

		return !DataConfigurationUtils.hasDataConfigFile(fileStore, fileName, removeIfExpired, expiryTime);
	}

	protected AVList getConfigurationParams(AVList params) {
		if (params == null) {
			params = new AVListImpl();
		}

		// Gather all the construction parameters if they are available.
		AVList constructionParams = (AVList) this.getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (constructionParams != null) {
			params.setValues(constructionParams);
		}

		// Gather any missing LevelSet parameters from the LevelSet itself.
		DataConfigurationUtils.getLevelSetConfigParams(this.getLevels(), params);

		// Gather any missing parameters about the elevation data. These values
		// must be available for consumers of the
		// model configuration to property interpret the cached elevation files.
		// While the elevation model assumes
		// default values when these properties are missing, a different system
		// does not know what those default values
		// should be, and thus cannot assume anything about the value of these
		// properties.

		if (params.getValue(AVKey.BYTE_ORDER) == null) {
			params.setValue(AVKey.BYTE_ORDER, this.getElevationDataByteOrder());
		}

		if (params.getValue(AVKey.DATA_TYPE) == null) {
			params.setValue(AVKey.DATA_TYPE, this.getElevationDataType());
		}

		if (params.getValue(AVKey.MISSING_DATA_SIGNAL) == null) {
			params.setValue(AVKey.MISSING_DATA_SIGNAL, this.getMissingDataSignal());
		}

		return params;
	}

	protected Document createConfigurationDocument(AVList params) {
		return createBasicElevationModelConfigDocument(params);
	}

	// **************************************************************//
	// ******************** Restorable Support ********************//
	// **************************************************************//

	@Override
	public String getRestorableState() {
		// We only create a restorable state XML if this elevation model was
		// constructed with an AVList.
		AVList constructionParams = (AVList) this.getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (constructionParams == null) {
			return null;
		}

		RestorableSupport rs = RestorableSupport.newRestorableSupport();
		// Creating a new RestorableSupport failed. RestorableSupport logged the
		// problem, so just return null.
		if (rs == null) {
			return null;
		}

		this.doGetRestorableState(rs, null);
		return rs.getStateAsXml();
	}

	@Override
	public void restoreState(String stateInXml) {
		String message = Logging.getMessage("RestorableSupport.RestoreRequiresConstructor");
		Logging.logger().severe(message);
		throw new UnsupportedOperationException(message);
	}

	protected void doGetRestorableState(RestorableSupport rs, RestorableSupport.StateObject context) {
		AVList constructionParams = (AVList) this.getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (constructionParams != null) {
			for (Map.Entry<String, Object> avp : constructionParams.getEntries()) {
				this.getRestorableStateForAVPair(avp.getKey(), avp.getValue(), rs, context);
			}
		}

		rs.addStateValueAsString(context, "ElevationModel.Name", this.getName());
		rs.addStateValueAsDouble(context, "ElevationModel.MissingDataFlag", this.getMissingDataSignal());
		rs.addStateValueAsDouble(context, "ElevationModel.MissingDataValue", this.getMissingDataReplacement());
		rs.addStateValueAsBoolean(context, "ElevationModel.NetworkRetrievalEnabled", this.isNetworkRetrievalEnabled());
		rs.addStateValueAsDouble(context, "ElevationModel.MinElevation", this.getMinElevation());
		rs.addStateValueAsDouble(context, "ElevationModel.MaxElevation", this.getMaxElevation());
		rs.addStateValueAsString(context, "BasicElevationModel.DataType", this.getElevationDataType());
		rs.addStateValueAsString(context, "BasicElevationModel.DataByteOrder", this.getElevationDataByteOrder());

		// We'll write the detail hint attribute only when it's a nonzero value.
		if (this.detailHint != 0.0) {
			rs.addStateValueAsDouble(context, "BasicElevationModel.DetailHint", this.detailHint);
		}

		RestorableSupport.StateObject so = rs.addStateObject(context, "avlist");
		for (Map.Entry<String, Object> avp : this.getEntries()) {
			this.getRestorableStateForAVPair(avp.getKey(), avp.getValue(), rs, so);
		}
	}

	@Override
	public void getRestorableStateForAVPair(String key, Object value, RestorableSupport rs, RestorableSupport.StateObject context) {
		if (value == null) {
			return;
		}

		if (key.equals(AVKey.CONSTRUCTION_PARAMETERS)) {
			return;
		}

		if (value instanceof LatLon) {
			rs.addStateValueAsLatLon(context, key, (LatLon) value);
		} else if (value instanceof Sector) {
			rs.addStateValueAsSector(context, key, (Sector) value);
		} else {
			super.getRestorableStateForAVPair(key, value, rs, context);
		}
	}

	protected void doRestoreState(RestorableSupport rs, RestorableSupport.StateObject context) {
		String s = rs.getStateValueAsString(context, "ElevationModel.Name");
		if (s != null) {
			this.setName(s);
		}

		Double d = rs.getStateValueAsDouble(context, "ElevationModel.MissingDataFlag");
		if (d != null) {
			this.setMissingDataSignal(d);
		}

		d = rs.getStateValueAsDouble(context, "ElevationModel.MissingDataValue");
		if (d != null) {
			this.setMissingDataReplacement(d);
		}

		Boolean b = rs.getStateValueAsBoolean(context, "ElevationModel.NetworkRetrievalEnabled");
		if (b != null) {
			this.setNetworkRetrievalEnabled(b);
		}

		// Look for the elevation data type using the current property name
		// "BasicElevationModel.DataType", or the the
		// old property name "BasicElevationModel.DataPixelType" if a property
		// with the current name does not exist.
		s = rs.getStateValueAsString(context, "BasicElevationModel.DataType");
		if (s == null) {
			s = rs.getStateValueAsString(context, "BasicElevationModel.DataPixelType");
		}
		if (s != null) {
			this.setElevationDataType(s);
		}

		s = rs.getStateValueAsString(context, "BasicElevationModel.DataByteOrder");
		if (s != null) {
			this.setByteOrder(s);
		}

		d = rs.getStateValueAsDouble(context, "BasicElevationModel.DetailHint");
		if (d != null) {
			this.setDetailHint(d);
		}

		// Intentionally omitting "ElevationModel.MinElevation" and
		// "ElevationModel.MaxElevation" because they are final
		// properties only configurable at construction.

		RestorableSupport.StateObject so = rs.getStateObject(context, "avlist");
		if (so != null) {
			RestorableSupport.StateObject[] avpairs = rs.getAllStateObjects(so, "");
			if (avpairs != null) {
				for (RestorableSupport.StateObject avp : avpairs) {
					if (avp != null) {
						this.doRestoreStateForObject(rs, avp);
					}
				}
			}
		}
	}

	protected void doRestoreStateForObject(RestorableSupport rs, RestorableSupport.StateObject so) {
		if (so == null) {
			return;
		}

		// Map the old PIXEL_TYPE AVKey constant to the new DATA_TYPE constant.
		if ("gov.nasa.worldwind.avkey.PixelType".equals(so.getName())) {
			this.setValue(AVKey.DATA_TYPE, so.getValue());
		} else {
			this.setValue(so.getName(), so.getValue());
		}
	}

	protected static AVList restorableStateToParams(String stateInXml) {
		if (stateInXml == null) {
			String message = Logging.getMessage("nullValue.StringIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		RestorableSupport rs;
		try {
			rs = RestorableSupport.parse(stateInXml);
		} catch (Exception e) {
			// Parsing the document specified by stateInXml failed.
			String message = Logging.getMessage("generic.ExceptionAttemptingToParseStateXml", stateInXml);
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message, e);
		}

		AVList params = new AVListImpl();
		restoreStateForParams(rs, null, params);
		return params;
	}

	protected static void restoreStateForParams(RestorableSupport rs, RestorableSupport.StateObject context, AVList params) {
		StringBuilder sb = new StringBuilder();

		String s = rs.getStateValueAsString(context, AVKey.DATA_CACHE_NAME);
		if (s != null) {
			params.setValue(AVKey.DATA_CACHE_NAME, s);
		}

		s = rs.getStateValueAsString(context, AVKey.SERVICE);
		if (s != null) {
			params.setValue(AVKey.SERVICE, s);
		}

		s = rs.getStateValueAsString(context, AVKey.DATASET_NAME);
		if (s != null) {
			params.setValue(AVKey.DATASET_NAME, s);
		}

		s = rs.getStateValueAsString(context, AVKey.FORMAT_SUFFIX);
		if (s != null) {
			params.setValue(AVKey.FORMAT_SUFFIX, s);
		}

		Integer i = rs.getStateValueAsInteger(context, AVKey.NUM_EMPTY_LEVELS);
		if (i != null) {
			params.setValue(AVKey.NUM_EMPTY_LEVELS, i);
		}

		i = rs.getStateValueAsInteger(context, AVKey.NUM_LEVELS);
		if (i != null) {
			params.setValue(AVKey.NUM_LEVELS, i);
		}

		i = rs.getStateValueAsInteger(context, AVKey.TILE_WIDTH);
		if (i != null) {
			params.setValue(AVKey.TILE_WIDTH, i);
		}

		i = rs.getStateValueAsInteger(context, AVKey.TILE_HEIGHT);
		if (i != null) {
			params.setValue(AVKey.TILE_HEIGHT, i);
		}

		Long lo = rs.getStateValueAsLong(context, AVKey.EXPIRY_TIME);
		if (lo != null) {
			params.setValue(AVKey.EXPIRY_TIME, lo);
		}

		LatLon ll = rs.getStateValueAsLatLon(context, AVKey.LEVEL_ZERO_TILE_DELTA);
		if (ll != null) {
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, ll);
		}

		ll = rs.getStateValueAsLatLon(context, AVKey.TILE_ORIGIN);
		if (ll != null) {
			params.setValue(AVKey.TILE_ORIGIN, ll);
		}

		Sector sector = rs.getStateValueAsSector(context, AVKey.SECTOR);
		if (sector != null) {
			params.setValue(AVKey.SECTOR, sector);
		}

		Double d = rs.getStateValueAsDouble("ElevationModel.MinElevation");
		if (d != null) {
			params.setValue(AVKey.ELEVATION_MIN, d);
		} else {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append("term.minElevation");
		}

		d = rs.getStateValueAsDouble("ElevationModel.MaxElevation");
		if (d != null) {
			params.setValue(AVKey.ELEVATION_MAX, d);
		} else {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append("term.maxElevation");
		}

		if (sb.length() > 0) {
			String message = Logging.getMessage("BasicElevationModel.InvalidDescriptorFields", sb.toString());
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}
	}

	@Override
	public double getLocalDataAvailability(Sector requestedSector, Double targetResolution) {
		if (requestedSector == null) {
			String msg = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		// Compute intersection of the requested sector and the sector covered
		// by the elevation model.
		LevelSet levelSet = this.getLevels();
		Sector sector = requestedSector.intersection(levelSet.getSector());

		// If there is no intersection there is no data to retrieve
		if (sector == null) {
			return 1d;
		}

		Level targetLevel = targetResolution != null ? this.getTargetLevel(sector, targetResolution) : levelSet.getLastLevel();

		// Count all the tiles intersecting the input sector.
		long numLocalTiles = 0;
		long numMissingTiles = 0;
		LatLon delta = targetLevel.getTileDelta();
		LatLon origin = levelSet.getTileOrigin();
		final int nwRow = Tile.computeRow(delta.getLatitude(), sector.getMaxLatitude(), origin.getLatitude());
		final int nwCol = Tile.computeColumn(delta.getLongitude(), sector.getMinLongitude(), origin.getLongitude());
		final int seRow = Tile.computeRow(delta.getLatitude(), sector.getMinLatitude(), origin.getLatitude());
		final int seCol = Tile.computeColumn(delta.getLongitude(), sector.getMaxLongitude(), origin.getLongitude());

		for (int row = nwRow; row >= seRow; row--) {
			for (int col = nwCol; col <= seCol; col++) {
				TileKey key = new TileKey(targetLevel.getLevelNumber(), row, col, targetLevel.getCacheName());
				Sector tileSector = levelSet.computeSectorForKey(key);
				Tile tile = new Tile(tileSector, targetLevel, row, col);
				if (!this.isTileLocalOrAbsent(tile)) {
					++numMissingTiles;
				} else {
					++numLocalTiles;
				}
			}
		}

		return numLocalTiles > 0 ? numLocalTiles / (double) (numLocalTiles + numMissingTiles) : 0d;
	}

	protected boolean isTileLocalOrAbsent(Tile tile) {
		if (this.getLevels().isResourceAbsent(tile)) {
			return true; // tile is absent
		}

		URL url = this.getDataFileStore().findFile(tile.getPath(), false);

		return url != null && !this.isFileExpired(tile, url, this.getDataFileStore());
	}

	/***
	 * set the elevation to be rendered
	 * 
	 * @param currentSubElevation
	 */
	public void setCurrentSubElevation(SubElevation currentSubElevation) {
		if (currentSubElevation != null) {
			this.current = currentSubElevation;
		}
	}

	public List<SubElevation> getSubElevationList() {
		return subElevations;
	}

	/** {@inheritDoc} */
	@Override
	public void setExtremesCachingEnabled(boolean enabled) {
	}

	/** {@inheritDoc} */
	@Override
	public boolean isExtremesCachingEnabled() {
		return false;
	}

}
