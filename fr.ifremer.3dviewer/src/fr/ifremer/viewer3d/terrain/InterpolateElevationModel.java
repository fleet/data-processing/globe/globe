/**
 *
 */
package fr.ifremer.viewer3d.terrain;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Optional;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModel;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.util.LRUSoftCache;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.util.BufferWrapper;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.TileKey;

/**
 * Adds interpolation functionality to {@link BasicElevationModel}.
 */
public class InterpolateElevationModel extends BasicElevationModel implements IWWElevationModel {

	/** Key used to save the enabled state in session */
	public static final TypedKey<Boolean> ENABLED_SESSION_KEY = new TypedKey<>("enabled");

	/** Parameters of a IWWElevationModel */
	ElevationModelParameters elevationModelParameters;

	private final NearestInterpolator interpolator;

	public InterpolateElevationModel(Document dom, AVList params) {
		this(dom.getDocumentElement(), params);
	}

	public InterpolateElevationModel(Element element, AVList params) {
		super(element, params);
		interpolator = new NearestInterpolator(getMissingDataSignal());
		elevationModelParameters = new ElevationModelParameters(false, 0d);
	}

	/** Overrode to allows interpolation of missing data. */
	@Override
	protected double lookupElevation(Angle latitude, Angle longitude, final ElevationTile tile) {
		if (!elevationModelParameters.interpolate) {
			try {
				return super.lookupElevation(latitude, longitude, tile);
			} catch (Exception e) {
				// TODO find why exception occur after some movement
				return 0;
			}
		}

		BufferWrapper elevations = interpolator.getElevations(tile);
		Sector sector = tile.getSector();
		final int tileHeight = tile.getHeight();
		final int tileWidth = tile.getWidth();
		final double sectorDeltaLat = sector.getDeltaLat().radians;
		final double sectorDeltaLon = sector.getDeltaLon().radians;
		final double dLat = sector.getMaxLatitude().radians - latitude.radians;
		final double dLon = longitude.radians - sector.getMinLongitude().radians;
		final double sLat = dLat / sectorDeltaLat;
		final double sLon = dLon / sectorDeltaLon;

		int j = (int) ((tileHeight - 1) * sLat);
		int i = (int) ((tileWidth - 1) * sLon);
		int k = j * tileWidth + i;

		try {
			double eLeft = elevations.getDouble(k);
			double eRight = i < tileWidth - 1 ? elevations.getDouble(k + 1) : eLeft;

			if (getMissingDataSignal() == eLeft || getMissingDataSignal() == eRight) {
				return getMissingDataSignal();
			}

			double dw = sectorDeltaLon / (tileWidth - 1);
			double dh = sectorDeltaLat / (tileHeight - 1);
			double ssLon = (dLon - i * dw) / dw;
			double ssLat = (dLat - j * dh) / dh;

			double eTop = eLeft + ssLon * (eRight - eLeft);

			if (j < tileHeight - 1 && i < tileWidth - 1) {
				eLeft = elevations.getDouble(k + tileWidth);
				eRight = elevations.getDouble(k + tileWidth + 1);

				if (getMissingDataSignal() == eLeft || getMissingDataSignal() == eRight) {
					return getMissingDataSignal();
				}
			}

			double eBot = eLeft + ssLon * (eRight - eLeft);
			return eTop + ssLat * (eBot - eTop);
		} catch (Exception e) {
			String message = "invalid elevation index";
			Logging.logger().severe(message);
		}
		return 0;
	}

	/**
	 * Interpolator implementation using nearest algorithm.
	 * <p>
	 * For each elevation value this algorithm checks for missing data flag. When missing data is detected it counts
	 * valid points among the 8 neighbors points. If at least {@link #minNeighbors} are valid it replace the current
	 * invalid value by an average value of surrounding values.<br/>
	 * In order to propagate the newly create valid values, this algorithm may be repeated.
	 * </p>
	 */
	protected static class NearestInterpolator {

		protected final double missingDataSignal;

		/** Minimum number of valid data neighbors. */
		protected int minNeighbors = 4;
		/** Number of algorithm passes. */
		protected int passes = 5;

		/** 128 values cache : around 10MB needed. */
		private LRUSoftCache<String, BufferWrapper> cache = new LRUSoftCache<String, BufferWrapper>(128);

		public NearestInterpolator(double missingDataSignal) {
			this.missingDataSignal = missingDataSignal;
		}

		public BufferWrapper getElevations(ElevationTile tile) {
			BufferWrapper bw = cache.get(tile.getPath());
			if (bw == null) {
				bw = createBuffer(tile.getElevations(), tile.getHeight(), tile.getWidth());
				cache.put(tile.getPath(), bw);
			}
			return bw;
		}

		protected BufferWrapper createBuffer(BufferWrapper elevations, int height, int width) {
			BufferWrapper interpolated = elevations.copyOf(height * width);
			for (int i = 0; i < passes; i++) {

				// iterates over the entire buffer
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						int idx = x * width + y;
						// if invalid value
						if (interpolated.getFloat(idx) == missingDataSignal) {
							// gets 8 values surrounding
							int nbValues = 0;
							double sum = 0.0;
							float val = 0.0f;
							if (x > 0) {
								if (y > 0) {
									val = interpolated.getFloat((x - 1) * width + y - 1);
									if (val != missingDataSignal) {
										nbValues++;
										sum += val;
									}
								}
								val = interpolated.getFloat((x - 1) * width + y);
								if (val != missingDataSignal) {
									nbValues++;
									sum += val;
								}
								if (y < height - 1) {
									val = interpolated.getFloat((x - 1) * width + y + 1);
									if (val != missingDataSignal) {
										nbValues++;
										sum += val;
									}
								}
							}
							if (y > 0) {
								val = interpolated.getFloat(x * width + y - 1);
								if (val != missingDataSignal) {
									nbValues++;
									sum += val;
								}
							}
							if (y < height - 1) {
								val = interpolated.getFloat(x * width + y + 1);
								if (val != missingDataSignal) {
									nbValues++;
									sum += val;
								}
							}
							if (x < width - 1) {
								if (y > 0) {
									val = interpolated.getFloat((x + 1) * width + y - 1);
									if (val != missingDataSignal) {
										nbValues++;
										sum += val;
									}
								}
								val = interpolated.getFloat((x + 1) * width + y);
								if (val != missingDataSignal) {
									nbValues++;
									sum += val;
								}
								if (y < height - 1) {
									val = interpolated.getFloat((x + 1) * width + y + 1);
									if (val != missingDataSignal) {
										nbValues++;
										sum += val;
									}
								}
							}
							// interpolated.putFloat(idx, 24.0f);

							if (nbValues >= minNeighbors) {
								interpolated.putFloat(idx, (float) (sum / nbValues));
							}
						}

					}
				}
			}
			return interpolated;
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see gov.nasa.worldwind.terrain.BasicElevationModel#getLevelZeroTiles()
	 */
	@Override
	public Map<TileKey, ElevationTile> getLevelZeroTiles() {
		return super.getLevelZeroTiles();
	}

	/**
	 * @return the {@link #elevationModelParameters}
	 */
	@Override
	public ElevationModelParameters getElevationModelParameters() {
		return elevationModelParameters;
	}

	/**
	 * @param elevationModelParameters the {@link #elevationModelParameters} to set
	 */
	@Override
	public void setElevationModelParameters(ElevationModelParameters elevationModelParameters) {
		this.elevationModelParameters = elevationModelParameters;
		ISessionService.grab().saveSession();
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(//
				elevationModelParameters.toTypedEntry(), //
				ENABLED_SESSION_KEY.bindValue(isEnabled()));
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(ElevationModelParameters.SESSION_KEY).ifPresent(p -> elevationModelParameters = p);
		parameters.get(ENABLED_SESSION_KEY).ifPresent(super::setEnabled);
	}

	/** {@inheritDoc} */
	@Override
	protected BufferWrapper makeTiffElevations(URL url) throws IOException, URISyntaxException {
		File tileFile = new File(url.toURI());
		try (var gdalDataset = GdalDataset.open(tileFile.getPath())) {
			// Tile with 4 bands (elevations/slope/cos aspect/sin aspect)
			if (gdalDataset.getBandCount() == 4) {
				Optional<ByteBuffer> elevations = gdalDataset.read(1, Float.class);
				if (elevations.isPresent()) {
					return BufferWrapper.wrap(elevations.get(), AVKey.FLOAT32, null /* Let the same byteOrder */ );
				}
			}
		}
		// Default implementation
		return super.makeTiffElevations(url);
	}

	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		ISessionService.grab().saveSession();
	}

}
