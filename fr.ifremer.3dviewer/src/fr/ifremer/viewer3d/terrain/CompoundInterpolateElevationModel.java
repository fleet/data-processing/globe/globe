/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.terrain;

import org.apache.commons.lang.NotImplementedException;

import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModel;
import fr.ifremer.globe.utils.map.TypedMap;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.terrain.CompoundElevationModel;

/**
 * Set of IWWElevationModel sharing the same parameters
 */
public class CompoundInterpolateElevationModel extends CompoundElevationModel implements IWWElevationModel {

	@Override
	public TypedMap describeParametersForSession() {
		return ((IWWElevationModel) elevationModels.get(0)).describeParametersForSession();
	}

	@Override
	public void setSessionParameter(TypedMap parameters) {
		elevationModels.stream()//
				.map(IWWElevationModel.class::cast) //
				.forEach(em -> em.setSessionParameter(parameters));
	}

	@Override
	public ElevationModelParameters getElevationModelParameters() {
		return ((IWWElevationModel) elevationModels.get(0)).getElevationModelParameters();
	}

	@Override
	public void setElevationModelParameters(ElevationModelParameters parameters) {
		elevationModels.stream()//
				.map(IWWElevationModel.class::cast) //
				.forEach(em -> em.setElevationModelParameters(parameters));
	}

	/** {@inheritDoc} */
	@Override
	public void addElevationModel(ElevationModel em) {
		if (!(em instanceof IWWElevationModel)) {
			throw new NotImplementedException();
		}
		super.addElevationModel(em);
	}

	/** {@inheritDoc} */
	@Override
	public void addElevationModel(int index, ElevationModel em) {
		if (!(em instanceof IWWElevationModel)) {
			throw new NotImplementedException();
		}
		super.addElevationModel(index, em);
	}

}
