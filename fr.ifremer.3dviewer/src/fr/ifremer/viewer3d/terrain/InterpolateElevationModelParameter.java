package fr.ifremer.viewer3d.terrain;

import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.layers.LayerParameters.AbstractLayerParameters;

public class InterpolateElevationModelParameter extends AbstractLayerParameters {

	private final BooleanPreferenceAttribute interpolateElevationModelPrefAttribut = Activator.getPluginParameters()
			.getInterpolateElevationModel();

	private boolean interpolate = interpolateElevationModelPrefAttribut.getValue();

	/***
	 * used for layers comparison
	 */
	private double offset = 0;

	public void load(InterpolateElevationModelParameter other) {
		interpolate = other.interpolate;
		offset = other.offset;
		visibility = other.visibility;
	}

	/**
	 * @return the interpolate
	 */
	public boolean isInterpolate() {
		return interpolate;
	}

	/**
	 * @param interpolate the interpolate to set
	 */
	public void setInterpolate(boolean interpolate) {
		pcs.firePropertyChange("interpolate", this.interpolate, this.interpolate = interpolate);
		interpolateElevationModelPrefAttribut.setValue(interpolate, false);
		Activator.getPluginParameters().save();
	}

	/**
	 * @return the offset
	 */
	public double getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(double offset) {
		pcs.firePropertyChange("offset", this.offset, this.offset = offset);
	}

}
