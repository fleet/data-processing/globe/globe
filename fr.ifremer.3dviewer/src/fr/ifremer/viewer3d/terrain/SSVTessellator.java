package fr.ifremer.viewer3d.terrain;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.terrain.RectangularTessellator;

/**
 * Redefine {@link RectangularTessellator} to manage skirt refresh and underground display.
 */
public class SSVTessellator extends RectangularTessellator {

	/**
	 * True if currently under ground. The under ground rendering removes tile skirts and uses front face culling.
	 */
	private boolean underground = false;

	@Override
	public void setMakeTileSkirts(boolean makeTileSkirts) {
		// useful for forcing the computation of skirt vertices
		WorldWind.getMemoryCache(CACHE_ID).clear();

		super.setMakeTileSkirts(makeTileSkirts);
	}

	/** True if currently under ground. */
	public void setUnderground(boolean value) {
		this.underground = value;
	}

	@Override
	protected long render(DrawContext dc, RectTile tile, int numTextureUnits) {
		GL2 gl = dc.getGL().getGL2();

		if (underground) {
			gl.glPushAttrib(GL.GL_CULL_FACE_MODE);
			gl.glCullFace(GL.GL_FRONT);
		}

		long value = super.render(dc, tile, numTextureUnits);

		if (underground) {
			gl.glPopAttrib();
		}

		return value;
	}
}
