package fr.ifremer.viewer3d.render;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureCoords;

import fr.ifremer.viewer3d.util.TextureLoader;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;

/**
 * This class holds information for a texture and provides utilities for binding
 * and initializing textures from ByteBuffers.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class MemoryMappedTexture {

	Logger logger = LoggerFactory.getLogger(MemoryMappedTexture.class);

	// last image data if it could be kept
	//private SoftReference<ByteBuffer> lastImageData;
	private boolean useMipMaps;
	private boolean useAnisotropy = true;

	/** One texture instance per context. */
	protected Map<GL, TextureLoader> textureMap = new HashMap<>(4);

	protected int width;
	protected int height;
	protected TextureCoords texCoords;
	protected boolean textureInitialized = false;
	protected boolean textureInitializationFailed = false;

	private String imageFile;

	/**
	 * Constructs a texture object from an image source.
	 * <p/>
	 * The texture's image source is opened, if a file, only when the texture is
	 * displayed. If the texture is not displayed the image source is not read.
	 * 
	 * @param imageSource
	 *            the source of the image, either a file path {@link String} or
	 *            a {@link BufferedImage}.
	 * @param useMipMaps
	 *            Indicates whether to generate and use mipmaps for the image.
	 * 
	 * @throws IllegalArgumentException
	 *             if the <code>imageSource</code> is null.
	 */
	public MemoryMappedTexture(boolean useMipMaps) {
		initialize(useMipMaps);
	}

	/**
	 * Constructs a texture object.
	 * <p/>
	 * The texture's image source is opened, if a file, only when the texture is
	 * displayed. If the texture is not displayed the image source is not read.
	 * 
	 * @param imageSource
	 *            the source of the image, either a file path {@link String} or
	 *            a {@link BufferedImage}.
	 * 
	 * @throws IllegalArgumentException
	 *             if the <code>imageSource</code> is null.
	 */
	public MemoryMappedTexture() {
		this(false);
	}

	protected void initialize(boolean useMipMaps) {
		this.useMipMaps = useMipMaps;
	}

	public void dispose() {
		for(Entry<GL, TextureLoader> entry : textureMap.entrySet()) {
			entry.getValue().dispose(entry.getKey());
		}
	}

	// public int getWidth(DrawContext dc) {
	// if (!this.textureInitialized) {
	// this.initializeTexture(dc);
	// }
	//
	// return width;
	// }
	//
	// public int getHeight(DrawContext dc) {
	// if (!this.textureInitialized) {
	// this.initializeTexture(dc);
	// }
	//
	// return height;
	// }

	/**
	 * Indicates whether the texture creates and uses mipmaps.
	 * 
	 * @return true if mipmaps are used, false if not.
	 */
	public boolean isUseMipMaps() {
		return useMipMaps;
	}

	public TextureCoords getTexCoords() {
		return texCoords;
	}

	public boolean isTextureCurrent(DrawContext dc) {
		return true;
	}

	/**
	 * Indicates whether texture anisotropy is applied to the texture when
	 * rendered.
	 * 
	 * @return useAnisotropy true if anisotropy is to be applied, otherwise
	 *         false.
	 */
	public boolean isUseAnisotropy() {
		return useAnisotropy;
	}

	/**
	 * Specifies whether texture anisotropy is applied to the texture when
	 * rendered.
	 * 
	 * @param useAnisotropy
	 *            true if anisotropy is to be applied, otherwise false.
	 */
	public void setUseAnisotropy(boolean useAnisotropy) {
		this.useAnisotropy = useAnisotropy;
	}

	public boolean isTextureInitializationFailed() {
		return textureInitializationFailed;
	}

	public boolean bind(DrawContext dc) {
		return bind1(dc);
	}

	public boolean bind1(DrawContext dc) {

		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		TextureLoader loader = textureMap.get(dc.getGL());
		if (loader == null) {
			loader = new TextureLoader(100);
			textureMap.put(dc.getGL(), loader);
		}

		Texture texture = null;
		try {
			texture = loader.getTexture(dc.getGL(), imageFile);	
		} catch (IOException e) {
			logger.warn("Error reading image data", e);
		}

		if (texture != null) {
			texture.bind(dc.getGL());

			boolean useMipMapFilter = this.useMipMaps && texture.isUsingAutoMipmapGeneration();

			GL2 gl = dc.getGL().getGL2();
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, useMipMapFilter ? GL.GL_LINEAR_MIPMAP_LINEAR : GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

			if (this.isUseAnisotropy() && useMipMapFilter) {
				double maxAnisotropy = dc.getGLRuntimeCapabilities().getMaxTextureAnisotropy();
				if (dc.getGLRuntimeCapabilities().isUseAnisotropicTextureFilter() && maxAnisotropy >= 2.0) {
					gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT, (float) maxAnisotropy);
				}
			}

			this.width = texture.getWidth();
			this.height = texture.getHeight();
			this.texCoords = texture.getImageTexCoords();
		}

		return texture != null;
	}

	// public boolean bind0(DrawContext dc) {
	//
	// if (dc == null) {
	// String message = Logging.getMessage("nullValue.DrawContextIsNull");
	// Logging.logger().severe(message);
	// throw new IllegalStateException(message);
	// }
	// Texture texture = textureMap.get(dc.getGL());
	// try {
	// if (texture == null) {
	// texture = this.initializeTexture(dc);
	// if (texture != null) {
	// textureMap.put(dc.getGL(), texture);
	// return true; // texture was bound during initialization.
	// }
	// }
	//
	// if (textureBindingNeeded) {
	// if (imageData != null) {
	// // synchronized (imageData) {
	// if (td != null) {
	// td.flush();
	// td = null;
	// }
	// try {
	// GLProfile glprofile = GLProfile.getDefault();
	//
	// // 2014/08/26 - bvalliere : Fix performance issue when
	// // playing water column and scrolling 3D view
	//
	// // TODO : new code to activate.
	// // maybe hold BufferedImage's in MyAbstractWallLayer
	// // instead of ByteBuffer's
	//
	// File file = new File(imageFile);
	// BufferedImage image = null;
	// try {
	// image = ImageIO.read(file);
	// texture = AWTTextureIO.newTexture(glprofile, image, false);
	// textureMap.put(dc.getGL(), texture);
	// textureBindingNeeded = false;
	// // td = AWTTextureIO.newTextureData(glprofile,
	// // image, this.useMipMaps);
	// // texture.updateImage(dc.getGL(), td);
	// } finally {
	// if (image != null) {
	// image.flush();
	// }
	// }
	//
	// // original code
	// // imageData.rewind();
	// // td = TextureIO.newTextureData(glprofile,
	// // byteInputStream(imageData), this.useMipMaps, "png");
	// // texture.updateImage(dc.getGL(), td);
	//
	// } catch (IOException e) {
	// logger.warn("Error reading image data", e);
	// }
	// lastImageData = new SoftReference<ByteBuffer>(imageData);
	// imageData = null;
	// // }
	// }
	// }
	//
	// if (texture != null) {
	// texture.bind(dc.getGL());
	//
	// boolean useMipMapFilter = this.useMipMaps &&
	// texture.isUsingAutoMipmapGeneration();
	//
	// GL2 gl = dc.getGL().getGL2();
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
	// useMipMapFilter ? GL.GL_LINEAR_MIPMAP_LINEAR : GL.GL_LINEAR);
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
	// GL.GL_LINEAR);
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S,
	// GL.GL_CLAMP_TO_EDGE);
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T,
	// GL.GL_CLAMP_TO_EDGE);
	//
	// if (this.isUseAnisotropy() && useMipMapFilter) {
	// double maxAnisotropy =
	// dc.getGLRuntimeCapabilities().getMaxTextureAnisotropy();
	// if (dc.getGLRuntimeCapabilities().isUseAnisotropicTextureFilter() &&
	// maxAnisotropy >= 2.0) {
	// gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT,
	// (float) maxAnisotropy);
	// }
	// }
	// }
	//
	// // if (texture != null && this.width == 0 && this.height == 0) {
	// this.width = texture.getWidth();
	// this.height = texture.getHeight();
	// this.texCoords = texture.getImageTexCoords();
	// // }
	// } catch (Exception e) {
	// logger.error("Error during image binding ", e);
	// }
	// return texture != null;
	// }

	/**
	 * Returns an input stream for a ByteBuffer. The read() methods use the
	 * relative ByteBuffer get() methods.
	 */
	public static InputStream byteInputStream(final ByteBuffer buf) {
		return new InputStream() {
			@Override
			public synchronized int read() throws IOException {
				if (!buf.hasRemaining()) {
					return -1;
				}
				return buf.get();
			}

			@Override
			public synchronized int read(byte[] bytes, int off, int len) throws IOException { // Read
				// only
				// what's
				// left
				len = Math.min(len, buf.remaining());
				buf.get(bytes, off, len);
				return len;
			}
		};
	}

	// protected Texture initializeTexture(DrawContext dc) {
	// if (dc == null) {
	// String message = Logging.getMessage("nullValue.DrawContextIsNull");
	// logger.error(message);
	// throw new IllegalStateException(message);
	// }
	//
	// if (this.textureInitializationFailed) {
	// return null;
	// }
	//
	// Texture t = null;
	// try {
	// boolean haveMipMapData = false;
	//
	// ByteBuffer imgData = imageData;
	// if (imgData == null && lastImageData != null) {
	// imgData = lastImageData.get();
	// }
	//
	// if (imgData != null) {
	// try {
	// imgData.rewind();
	// GLProfile glprofile = GLProfile.getDefault();
	// td = TextureIO.newTextureData(glprofile, byteInputStream(imageData),
	// this.useMipMaps, "png");
	//
	// // td = AWTTextureIO.newTextureData(glprofile, bi,
	// // this.useMipMaps);
	// t = TextureIO.newTexture(td);
	// haveMipMapData = td.getMipmapData() != null;
	// lastImageData = new SoftReference<ByteBuffer>(imgData);
	// imageData = null;
	// } catch (Exception e) {
	// logger.error("layers.TextureLayer.ExceptionAttemptingToReadTextureFile",
	// e);
	// this.textureInitializationFailed = true;
	// return null;
	// }
	// } else {
	// logger.debug("Missing image data");
	// }
	//
	// if (t == null) // In case JOGL TextureIO returned null
	// {
	// Logging.logger().log(java.util.logging.Level.SEVERE,
	// "generic.TextureUnreadable");
	// this.textureInitializationFailed = true;
	// return null;
	// }
	//
	// // Textures with the same path are assumed to be identical textures,
	// // so
	// // key the texture id off the
	// // image source.
	// t.bind(dc.getGL());
	//
	// // Enable the appropriate mip-mapping texture filters if the caller
	// // has
	// // specified that mip-mapping should be
	// // enabled, and the texture itself supports mip-mapping.
	// boolean useMipMapFilter = this.useMipMaps && (haveMipMapData ||
	// t.isUsingAutoMipmapGeneration());
	//
	// GL2 gl = dc.getGL().getGL2();
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
	// useMipMapFilter ? GL.GL_LINEAR_MIPMAP_LINEAR : GL.GL_LINEAR);
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
	// GL.GL_LINEAR);
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S,
	// GL.GL_CLAMP_TO_EDGE);
	// gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T,
	// GL.GL_CLAMP_TO_EDGE);
	//
	// if (this.isUseAnisotropy() && useMipMapFilter) {
	// double maxAnisotropy =
	// dc.getGLRuntimeCapabilities().getMaxTextureAnisotropy();
	// if (dc.getGLRuntimeCapabilities().isUseAnisotropicTextureFilter() &&
	// maxAnisotropy >= 2.0) {
	// gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT,
	// (float) maxAnisotropy);
	// }
	// }
	//
	// this.width = t.getWidth();
	// this.height = t.getHeight();
	// this.texCoords = t.getImageTexCoords();
	// this.textureInitialized = true;
	// } catch (Exception e) {
	// logger.error("Error during texture initialization", e);
	// }
	// return t;
	// }

	// public void setTextureData(ByteBuffer data) {
	// this.imageData = data;
	// this.textureBindingNeeded = true;
	// }

	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}


}
