package fr.ifremer.viewer3d.render;

import java.util.function.Supplier;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;

import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.render.DrawContext;

/** ??? */
public class VolatilHideableTexturedWall extends VolatilTexturedWall {

	private boolean hide = false;

	public boolean isHide() {
		return hide;
	}

	public void setHide(boolean hide) {
		this.hide = hide;
	}

	public VolatilHideableTexturedWall(MyAbstractLayer parent, LatLon location1, LatLon location2, double bottomElevation, double topElevation,Supplier<Object> pickObjectBuilder) {
		super(parent, location1, location2, bottomElevation, topElevation,pickObjectBuilder);
	}

	@Override
	public void render(DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		double subFieldOpacity = 80.0 / 100.0;
		try {
			if (!dc.isPickingMode()) {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
						| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
						// depth
						// func
						| GL2.GL_TEXTURE_BIT // for texture env
						| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

				// Enable blending using white premultiplied by the current
				// opacity.
				double opacity = dc.getCurrentLayer() != null ? this.getOpacity() * dc.getCurrentLayer().getOpacity() : this.getOpacity();
				gl.glColor4d(opacity, opacity, opacity, opacity);
				gl.glEnable(GL.GL_BLEND);
				gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);

				// Bind texture
				gl.glEnable(GL.GL_TEXTURE_2D);
				this.texture.bind(dc);
				// this.texture.applyInternalTransform(dc);

				// Set the interpolation mode to NEAREST to avoid problems with
				// pixels
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
			} else {
				gl.glPushAttrib(GL2.GL_POLYGON_BIT);
			}

			gl.glPolygonMode(GL.GL_FRONT, GL2GL3.GL_FILL);

			this.draw(dc);
		} finally {
			gl.glPopAttrib();
		}
		if (!hide) {
			try {
				/*
				 * gl.glDepthMask(false);
				 * gl.glDisable(GL.GL_DEPTH_TEST);//05/08/10
				 */
				if (!dc.isPickingMode()) {
					gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
							| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
							// depth
							// func
							| GL2.GL_TEXTURE_BIT // for texture env
							| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

					// Enable blending using white premultiplied by the current
					// opacity.
					double opacity = dc.getCurrentLayer() != null ? this.getOpacity() * dc.getCurrentLayer().getOpacity() : this.getOpacity();
					gl.glColor4d(opacity * subFieldOpacity, opacity * subFieldOpacity, opacity * subFieldOpacity, opacity * subFieldOpacity);
					gl.glEnable(GL.GL_BLEND);
					gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);

					// Bind texture
					gl.glEnable(GL.GL_TEXTURE_2D);
					this.texture.bind(dc);
					// this.texture.applyInternalTransform(dc);
				} else {
					gl.glPushAttrib(GL2.GL_POLYGON_BIT);
				}

				gl.glPolygonMode(GL.GL_FRONT, GL2GL3.GL_FILL);

				this.draw(dc);

			} finally {
				gl.glPopAttrib();
			}
		}
		/*
		 * gl.glEnable(GL.GL_DEPTH_TEST);//05/08/10 gl.glDepthMask(true);
		 */
		// gl.glUseProgram(0);
	}

}
