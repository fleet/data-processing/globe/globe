package fr.ifremer.viewer3d.render;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;

import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.util.OGLStackHandler;
import gov.nasa.worldwind.util.OGLUtil;

/** Renderable to draw a rectangle */
public class ScreenRectangle implements OrderedRenderable {

	private static final Color DEFAULT_INTERIOR_COLOR = new Color(255, 255, 255, 64);
	private static final Color DEFAULT_BORDER_COLOR = Color.WHITE;

	/** The current drawn rectangle */
	private final Rectangle rect;
	private final Point startPoint;
	private final Point endPoint;
	private final OGLStackHandler oglStackHandler = new OGLStackHandler();

	private Color interiorColor;
	private Color borderColor;

	/**
	 * Constructor
	 */
	public ScreenRectangle() {
		rect = new Rectangle();
		startPoint = new Point();
		endPoint = new Point();
	}

	public boolean hasSelection() {
		return !rect.isEmpty();
	}

	public Rectangle getSelection() {
		return rect;
	}

	public void startSelection(Point point) {
		startPoint.setLocation(point);
		endPoint.setLocation(point);
		rect.setRect(point.x, point.y, 0, 0);
	}

	public void endSelection(Point point) {
		endPoint.setLocation(point);

		// Compute the selection's extremes along the x axis.
		double minX;
		double maxX;
		if (startPoint.x < endPoint.x) {
			minX = startPoint.x;
			maxX = endPoint.x;
		} else {
			minX = endPoint.x;
			maxX = startPoint.x;
		}

		// Compute the selection's extremes along the y axis. The selection is defined in AWT screen coordinates, so
		// the origin is in the upper left corner and the y axis points down.
		double minY;
		double maxY;
		if (startPoint.y < endPoint.y) {
			minY = startPoint.y;
			maxY = endPoint.y;
		} else {
			minY = endPoint.y;
			maxY = startPoint.y;
		}

		// If only one of the selection rectangle's dimensions is zero, then the selection is either a horizontal or
		// vertical line. In this case, we set the zero dimension to 1 because both dimensions must be nonzero to
		// perform a selection.
		if (minX == maxX && minY < maxY)
			maxX = minX + 1;
		if (minY == maxY && minX < maxX)
			minY = maxY - 1;

		rect.setRect(minX, maxY, maxX - minX, maxY - minY);
	}

	public void clearSelection() {
		startPoint.setLocation(0, 0);
		endPoint.setLocation(0, 0);
		rect.setRect(0, 0, 0, 0);
	}

	public void setInteriorColor(Color color) {
		interiorColor = color;
	}

	public Color getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(Color color) {
		borderColor = color;
	}

	@Override
	public double getDistanceFromEye() {
		return 0; // Screen rectangle is drawn on top of other ordered renderables, except other screen objects.
	}

	@Override
	public void pick(DrawContext dc, Point pickPoint) {
		// Intentionally left blank. SelectionRectangle is not pickable.
	}

	@Override
	public void render(DrawContext dc) {
		if (dc.isOrderedRenderingMode())
			drawOrderedRenderable(dc);
		else
			makeOrderedRenderable(dc);
	}

	protected void makeOrderedRenderable(DrawContext dc) {
		if (hasSelection())
			dc.addOrderedRenderable(this);
	}

	protected void drawOrderedRenderable(DrawContext dc) {
		int attrs = GL.GL_COLOR_BUFFER_BIT // For blend enable, alpha enable, blend func, alpha func.
				| GL2.GL_CURRENT_BIT // For current color.
				| GL.GL_DEPTH_BUFFER_BIT; // For depth test disable.

		Rectangle viewport = dc.getView().getViewport();
		Rectangle selection = getSelection();

		GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
		oglStackHandler.pushAttrib(gl, attrs);
		oglStackHandler.pushClientAttrib(gl, GLPointerFunc.GL_VERTEX_ARRAY);
		try {
			// Configure the modelview-projection matrix to transform vertex points from screen rectangle
			// coordinates to clip coordinates without any perspective transformation. We offset the rectangle by
			// 0.5 pixels to ensure that the line loop draws a line without a 1-pixel gap between the line's
			// beginning and its end. We scale by (width - 1, height - 1) to ensure that only the actual selected
			// area is filled. If we scaled by (width, height), GL line rasterization would fill one pixel beyond
			// the actual selected area.
			oglStackHandler.pushProjectionIdentity(gl);
			gl.glOrtho(0, viewport.getWidth(), 0, viewport.getHeight(), -1, 1); // l, r, b, t, n, f
			oglStackHandler.pushModelviewIdentity(gl);
			gl.glTranslated(0.5, 0.5, 0.0);
			gl.glTranslated(selection.getX(), viewport.getHeight() - selection.getY(), 0);
			gl.glScaled(selection.getWidth() - 1, selection.getHeight() - 1, 1);

			// Disable the depth test and enable blending so this screen rectangle appears on top of the existing
			// framebuffer contents.
			gl.glDisable(GL.GL_DEPTH_TEST);
			gl.glEnable(GL.GL_BLEND);
			OGLUtil.applyBlending(gl, false); // SelectionRectangle does not use pre-multiplied colors.

			// Draw this screen rectangle's interior as a filled quadrilateral.
			Color c = interiorColor != null ? interiorColor : DEFAULT_INTERIOR_COLOR;
			gl.glColor4ub((byte) c.getRed(), (byte) c.getGreen(), (byte) c.getBlue(), (byte) c.getAlpha());
			dc.drawUnitQuad();

			// Draw this screen rectangle's border as a line loop. This assumes the default line width of 1.0.
			c = getBorderColor() != null ? getBorderColor() : DEFAULT_BORDER_COLOR;
			gl.glColor4ub((byte) c.getRed(), (byte) c.getGreen(), (byte) c.getBlue(), (byte) c.getAlpha());
			dc.drawUnitQuadOutline();
		} finally {
			oglStackHandler.pop(gl);
		}
	}
}