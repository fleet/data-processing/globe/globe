package fr.ifremer.viewer3d.render;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLProfile;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.geom.MovableSector;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTextureTile;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.GeographicSurfaceTileRenderer;
import gov.nasa.worldwind.render.SurfaceImage;
import gov.nasa.worldwind.render.SurfaceTile;
import gov.nasa.worldwind.terrain.SectorGeometry;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.OGLUtil;

/**
 * Renderer for image elevation tiles. This renderer uses the "elevationShader16.frag" fragment shader.
 */
public class ShaderSurfaceTileRenderer extends GeographicSurfaceTileRenderer {

	protected ShaderElevationLayer layer;
	protected float cellSizeShadingFactor = 10000 * 300; //
	protected double alphaFilter = 1.2;
	protected int lookup = 1;
	protected int width;
	protected int height;
	protected int level;

	public ShaderSurfaceTileRenderer(ShaderElevationLayer shaderElevationLayer) {
		layer = shaderElevationLayer;
	}

	@Override
	public void renderTiles(DrawContext dc, Iterable<? extends SurfaceTile> tiles) {
		if (tiles == null) {
			String message = Logging.getMessage("nullValue.TileIterableIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		GL2 gl = dc.getGL().getGL2();

		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
				| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
				// depth
				// func
				| GL2.GL_TRANSFORM_BIT);

		try {
			gl.glEnable(GL.GL_DEPTH_TEST);
			gl.glDepthFunc(GL.GL_LEQUAL);

			gl.glEnable(GL2ES1.GL_ALPHA_TEST);
			gl.glAlphaFunc(GL.GL_GREATER, 0.0f);

			gl.glEnable(GL.GL_BLEND);
			gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

			int shaderProgram = useShader(gl);

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glEnable(GL.GL_TEXTURE_2D);
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glPushMatrix();
			if (!dc.isPickingMode()) {
				gl.glTexEnvi(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, GL2ES1.GL_MODULATE);
			} else {
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, GL2ES1.GL_COMBINE);
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_SRC0_RGB, GL2ES1.GL_PREVIOUS);
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_COMBINE_RGB, GL.GL_REPLACE);
			}

			int numTexUnitsUsed = 1;
			dc.getSurfaceGeometry().beginRendering(dc);

			// For each current geometry tile, find the intersecting image tiles
			// and render the geometry
			// tile once for each intersecting image tile.
			Transform transform = new Transform();
			for (SectorGeometry sg : dc.getSurfaceGeometry()) {
				Iterable<SurfaceTile> tilesToRender = getIntersectingTiles(dc, sg, tiles);
				if (tilesToRender == null) {
					continue;
				}
				try {
					sg.beginRendering(dc, numTexUnitsUsed); // TODO: wrap in

					// set reference center
					// RenderInfo info = moveSectorGemotry(dc, sg);
					// dc.getView().setReferenceCenter(dc,
					// info.getReferenceCenter());
				} catch (Throwable e) {
					Logging.logger().log(Level.SEVERE, "SurfaceTileRenderer begin rendering", e);
				}

				// Pre-load info to compute the texture transform
				this.preComputeTextureTransform(dc, sg, transform);

				for (SurfaceTile tile : tilesToRender) {
					gl.glActiveTexture(GL.GL_TEXTURE0);

					if (tile.bind(dc)) {
						gl.glMatrixMode(GL.GL_TEXTURE);
						gl.glLoadIdentity();
						tile.applyInternalTransform(dc, true);

						List<Sector> sectorsToRender = new ArrayList<Sector>();
						Sector st = tile.getSector();
						st = layer.move(st);
						Sector s = st;
						if (st instanceof MovableSector) {
							sectorsToRender = ((MovableSector) st).getEnglobingSector();
						} else {
							sectorsToRender.add(st);
						}
						Sector sectorTmp = sg.getSector();
						for (Sector sr : sectorsToRender) {

							if (!sr.intersects(sectorTmp)) {
								continue;
							}
							// Determine and apply texture transform to map
							// image
							// tile into geometry tile's texture space
							this.computeTextureTransform(dc, tile, transform, sr);

							gl.glScaled(transform.HScale, transform.VScale, 1d);
							gl.glTranslated(transform.HShift, transform.VShift, 0d);

							// Render the geometry tile
							if (tile instanceof MyTextureTile) {
								MyTextureTile textureTile = (MyTextureTile) tile;
								Texture texture = textureTile.getTexture(dc.getTextureCache());

								if (texture != null) {
									// this cellSize parameter allow to convert with some factor the texel size
									// (radians) to something more like a meter in order to compare it with the
									// gradients computed in shader
									gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "cellSize"),
											(float) (cellSizeShadingFactor * textureTile.getLevel().getTexelSize()));
									int shader_width = gl.glGetUniformLocation(shaderProgram, "width");
									int shader_height = gl.glGetUniformLocation(shaderProgram, "height");

									gl.glUniform1f(shader_width, ((MyTextureTile) tile).getWidth());
									gl.glUniform1f(shader_height, ((MyTextureTile) tile).getHeight());

									shaderParametersForMovement(gl, shaderProgram, tile, s, sr, transform);

									if (!Viewer3D.isInterpolate()) {
										// nearest interpolation
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
									} else {
										// linear interpolation
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
										gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
									}

									sg.renderMultiTexture(dc, numTexUnitsUsed);
								}
							} else if (tile instanceof SurfaceImage) {
								SurfaceImage surfaceImage = (SurfaceImage) tile;
								int width = (int) surfaceImage.getValue(AVKey.WIDTH);
								int height = (int) surfaceImage.getValue(AVKey.HEIGHT);
								Sector sector = surfaceImage.getSector();
								double texelSize = sector.getDeltaLatRadians() / height;
								gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "cellSize"),
										(float) (10000 * texelSize));
								int shader_width = gl.glGetUniformLocation(shaderProgram, "width");
								int shader_height = gl.glGetUniformLocation(shaderProgram, "height");

								gl.glUniform1f(shader_width, width);
								gl.glUniform1f(shader_height, height);

								shaderParametersForMovement(gl, shaderProgram, tile, s, sr, transform);

								if (!Viewer3D.isInterpolate()) {
									// nearest interpolation
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								} else {
									// linear interpolation
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								}

								sg.renderMultiTexture(dc, numTexUnitsUsed);
							} else {
								sg.renderMultiTexture(dc, numTexUnitsUsed);
							}
						}
					}
				}
				try {
					sg.endRendering(dc);
				} catch (Throwable e) {
					Logging.logger().log(Level.SEVERE, "SurfaceTileRenderer begin rendering", e);
				}

			}
		} catch (Exception e) {
			Logging.logger().log(Level.SEVERE,
					Logging.getMessage("generic.ExceptionWhileRenderingLayer", this.getClass().getName()), e);
		} finally {
			dc.getSurfaceGeometry().endRendering(dc);

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glPopMatrix();
			gl.glDisable(GL.GL_TEXTURE_2D);

			gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, OGLUtil.DEFAULT_TEX_ENV_MODE);
			if (dc.isPickingMode()) {
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_SRC0_RGB, OGLUtil.DEFAULT_SRC0_RGB);
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_COMBINE_RGB, OGLUtil.DEFAULT_COMBINE_RGB);
			}

			gl.glPopAttrib();

			// disable shader
			gl.glUseProgram(0);

			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
		}
	}

	protected TextureData initTexture(TextureData textureDataOriginale, int width, int height) {

		byte[] ba = new byte[width * height];
		byte value = (byte) 0xff;
		for (int i = 0; i < ba.length; i++) {
			ba[i] = value;
		}
		ByteBuffer textureBytes = Buffers.newDirectByteBuffer(ba);

		GLProfile glprofile = GLProfile.getDefault();
		TextureData textureData = new TextureData(glprofile, GL.GL_RGBA, width, height, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, false, false, false, textureBytes.rewind(), null);

		return textureData;
	}

	// copy from GeographicSurfaceTileRenderer and SurfaceTileRenderer because
	// of private accessor
	public static class Transform {
		public double HScale;
		public double VScale;
		public double HShift;
		public double VShift;
	}

	protected void preComputeTextureTransform(DrawContext dc, SectorGeometry sg, Transform t) {
		Sector st = sg.getSector();
		PrivateAccessor.setField(this, "sgWidth", GeographicSurfaceTileRenderer.class, st.getDeltaLonRadians());
		PrivateAccessor.setField(this, "sgHeight", GeographicSurfaceTileRenderer.class, st.getDeltaLatRadians());
		PrivateAccessor.setField(this, "sgMinWE", GeographicSurfaceTileRenderer.class, st.getMinLongitude().radians);
		PrivateAccessor.setField(this, "sgMinSN", GeographicSurfaceTileRenderer.class, st.getMinLatitude().radians);
	}

	protected Sector computeTextureTransform(DrawContext dc, SurfaceTile tile, Transform t, Sector st) {
		double sgHeight = (Double) PrivateAccessor.getField(this, "sgHeight", GeographicSurfaceTileRenderer.class);
		double sgWidth = (Double) PrivateAccessor.getField(this, "sgWidth", GeographicSurfaceTileRenderer.class);
		double sgMinSN = (Double) PrivateAccessor.getField(this, "sgMinSN", GeographicSurfaceTileRenderer.class);
		double sgMinWE = (Double) PrivateAccessor.getField(this, "sgMinWE", GeographicSurfaceTileRenderer.class);

		double tileWidth = st.getDeltaLonRadians();
		double tileHeight = st.getDeltaLatRadians();

		double minLon = st.getMinLongitude().radians;
		double minLat = st.getMinLatitude().radians;

		double vShift = -(minLat - sgMinSN) / sgHeight;
		double hShift = -(minLon - sgMinWE) / sgWidth;

		t.VScale = tileHeight > 0 ? sgHeight / tileHeight : 1;
		t.HScale = tileWidth > 0 ? sgWidth / tileWidth : 1;

		t.VShift = vShift;
		t.HShift = hShift;

		return st;
	}

	@Override
	protected Iterable<SurfaceTile> getIntersectingTiles(DrawContext dc, SectorGeometry sg,
			Iterable<? extends SurfaceTile> tiles) {
		ArrayList<SurfaceTile> intersectingTiles = null;
		boolean intersect = false;
		List<Sector> englobingSectors;

		for (SurfaceTile tile : tiles) {
			// sector is rotated by tectonic movement
			Sector s = layer.move(tile.getSector());
			if (s instanceof MovableSector) {
				Sector sectorTmp = sg.getSector();

				englobingSectors = ((MovableSector) s).getEnglobingSector();
				intersect = false;
				for (Sector seng : englobingSectors) {
					if (seng.intersects(sectorTmp)) {
						intersect = true;
					}
				}
				if (!intersect) {
					continue;
				}
			} else if (!s.intersectsInterior(sg.getSector())) {
				continue;
			}

			if (intersectingTiles == null) {
				// case is no intersecting tiles
				intersectingTiles = new ArrayList<SurfaceTile>();
			}

			intersectingTiles.add(tile);
		}

		return intersectingTiles; // will be null if no intersecting tiles
	}

	public ShaderElevationLayer getLayer() {
		return layer;
	}

	public void setLayer(ShaderElevationLayer layer) {
		this.layer = layer;
	}

	protected int useShader(GL gl1) {
		GL2 gl = gl1.getGL2();
		// RGB data => shader disabled
		if (layer == null || layer.getShaderprograms(gl) == 0) {
			return -1;
		}

		TerrainParameters textureParameters = layer.getTextureParameters();
		int shaderProgram = layer.getShaderprograms(gl);
		gl.glUseProgram(shaderProgram);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "scale"), (float) layer.getScaleFactor());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "offset"), 0f/* (float) layer.getOffset() */);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "reference"), (float) textureParameters.maxTextureValue);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), (float) textureParameters.opacity);

		if (layer.isRGB()) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "isRGB"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "isRGB"), (float) 0.0);
		}

		if (textureParameters.reverseColorMap) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "inverseColormap"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "inverseColormap"), (float) 0.0);
		}

		float min_val = (float) ((textureParameters.minContrast - layer.getValGlobaleMin())
				/ (layer.getValGlobaleMax() - layer.getValGlobaleMin()));
		float max_val = (float) ((textureParameters.maxContrast - layer.getValGlobaleMin())
				/ (layer.getValGlobaleMax() - layer.getValGlobaleMin()));

		// contrast
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "min"), min_val);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "max"), max_val);

		if (textureParameters.transparencyEnabled) {
			min_val = (float) ((textureParameters.minTransparency - layer.getOffset() - layer.getValGlobaleMin())
					/ (layer.getValGlobaleMax() - layer.getValGlobaleMin()));
			max_val = (float) ((textureParameters.maxTransparency - layer.getOffset() - layer.getValGlobaleMin())
					/ (layer.getValGlobaleMax() - layer.getValGlobaleMin()));
		} else {
			min_val = 0f;
			max_val = 1f;
		}

		// highlight
		if (layer.isHighlighted()) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "highlight"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "highlight"), (float) 0.0);
		}

		// transparency contrast
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minTransparency"), min_val);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxTransparency"), max_val);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "globalMin"),
				(float) (layer.getValMin() + layer.getOffset()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "globalMax"),
				(float) (layer.getValMax() + layer.getOffset()));

		float[][] colormapBuffer = ColorMap.getColormap(textureParameters.colorMap);
		int colorMapLength = 256;
		gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "colorMapLength"), colorMapLength);

		// colormap in one array for effficiency
		// and because shader can't handle to many parameters
		int[] colormap = new int[256];
		for (int index = 0; index < colorMapLength; index++) {
			colormap[index] = (int) (colormapBuffer[ColorMap.red][index] * 255) * 256 * 256
					+ (int) (colormapBuffer[ColorMap.green][index] * 255) * 256
					+ (int) (colormapBuffer[ColorMap.blue][index] * 255);
		}

		gl.glUniform1iv(gl.glGetUniformLocation(shaderProgram, "colormap"), colorMapLength, colormap, 0);

		if (layer.isUseOmbrage()) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrage"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrage"), (float) 0.0);
		}

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "width"), getWidth());
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "height"), getHeight());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "azimuth"), (float) textureParameters.azimuth);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "zenith"), (float) textureParameters.zenith);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageExaggeration"),
				(float) layer.getOmbrageExaggeration());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "resolution"), (float) layer.getResolution());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "toEnergy"), (float) 0.0);
		// // conversion db energy
		// if (layer.isDbToEnergy()) {
		// gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "toEnergy"), (float) (1.0));
		// } else {
		// }

		// log de la color map
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "logColormap"), (float) 0.0);

		if (textureParameters.useLogarithmicShading) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageLogarithmique"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageLogarithmique"), (float) 0.0);
		}

		if (textureParameters.useGradient) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "gradient"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "gradient"), (float) 0.0);
		}

		return shaderProgram;
	}

	/***
	 * Transfer parameters to GPU to compute movement</br>
	 *
	 * @param gl
	 * @param shaderProgram
	 * @param tile : the beginning position
	 * @param boundingBox : the bounding box on which the texture is binded
	 * @param transform
	 */
	protected void shaderParametersForMovement(GL gl1, int shaderProgram, SurfaceTile tile, Sector boundingBox,
			Sector sr, Transform transform) {
		ISectorMovementProcessing movement = layer.getMovement();

		GL2 gl = gl1.getGL2();

		// TODO add condition : the first position must not be computed
		if (movement != null && boundingBox != null && boundingBox instanceof MovableSector) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "computeMovement"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "computeMovement"), (float) 0.0);
			// no need to transfer unneeded parameters
			return;
		}

		// System.out.println("enter shader sector : " +
		// boundingBox.toString());
		boundingBox = sr;
		// System.out.println("bounding shader sector : " +
		// boundingBox.toString()+ "\n");

		Matrix m = movement.getMovementInv();

		// Check if referential movement exists
		if (movement.getReferentialMovement() != null) {
			m = m.multiply(movement.getReferentialMovement().getMovement(movement.getDateIndex()));
		}

		// deplacement Matrix
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m11"), (float) m.m11);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m12"), (float) m.m12);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m13"), (float) m.m13);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m14"), (float) m.m14);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m21"), (float) m.m21);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m22"), (float) m.m22);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m23"), (float) m.m23);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m24"), (float) m.m24);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m31"), (float) m.m31);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m32"), (float) m.m32);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m33"), (float) m.m33);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m34"), (float) m.m34);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m41"), (float) m.m41);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m42"), (float) m.m42);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m43"), (float) m.m43);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m44"), (float) m.m44);

		// texture bounding box
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "latitudeSouth"),
				(float) boundingBox.getMinLatitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "latitudeNorth"),
				(float) boundingBox.getMaxLatitude().degrees);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "longitudeWest"),
				(float) boundingBox.getMinLongitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "longitudeEast"),
				(float) boundingBox.getMaxLongitude().degrees);

		// original position
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLatitudeSouth"),
				(float) tile.getSector().getMinLatitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLatitudeNorth"),
				(float) tile.getSector().getMaxLatitude().degrees);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLongitudeWest"),
				(float) tile.getSector().getMinLongitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLongitudeEast"),
				(float) tile.getSector().getMaxLongitude().degrees);
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWidth() {
		return width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public void clearCache() {
	}
}
