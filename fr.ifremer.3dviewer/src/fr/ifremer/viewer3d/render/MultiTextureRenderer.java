package fr.ifremer.viewer3d.render;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import fr.ifremer.viewer3d.layers.xml.MultiTextureTile;
import fr.ifremer.viewer3d.layers.xml.Operation;
import fr.ifremer.viewer3d.layers.xml.OperationOnTexture;
import fr.ifremer.viewer3d.loaders.SubLayer;
import fr.ifremer.viewer3d.multidata.SubLayerParameters;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.GeographicSurfaceTileRenderer;
import gov.nasa.worldwind.render.SurfaceTile;
import gov.nasa.worldwind.terrain.SectorGeometry;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.OGLUtil;
import jogamp.opengl.glu.nurbs.Curve;

/***
 * Render of visible {@link MultiTextureTile} with {@link Operation} defined
 * between texture and/or {@link Curve}. <br/>
 * Designed for {@link MultiTextureLayer} rendering
 * 
 * @author MORVAN
 * 
 */
public class MultiTextureRenderer extends GeographicSurfaceTileRenderer {
	protected Logger logger = LoggerFactory.getLogger(MultiTextureRenderer.class);

	/***
	 * layer containing the {@link SubLayer} to be rendered and defining the
	 * date
	 */
	MultiTextureLayer layer;

	private double alphaFilter = 1.2;
	private int lookup = 1;
	private int width;
	private int height;
	private int level;

	public MultiTextureRenderer(MultiTextureLayer multiTextureLayer) {
		layer = multiTextureLayer;
	}

	@SuppressWarnings("unused")
	public void renderTiles(DrawContext dc, Iterable<? extends SurfaceTile> tiles, int indexDate) {
		if (tiles == null) {
			String message = Logging.getMessage("nullValue.TileIterableIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		GL2 gl = dc.getGL().getGL2();

		// use shader
		int shaderProgram = useShader(gl);

		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
				| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
				// depth
				// func
				| GL2.GL_TRANSFORM_BIT);

		int i = 0;
		// list of textures which are displayed
		List<SubLayer> subLayerstoBeRendered = layer.getSubLayersToBeRendered();
		// list of textures used for filtering
		Pair<Operation, List<SubLayer>> toFilter = layer.getSubLayersForFiltering();
		List<SubLayer> subLayersforFiltering = toFilter.getSecond();
		// list of textures used for filtering
		Pair<Operation, List<SubLayer>> secondFilter = layer.getSubLayersForSecondFiltering();
		List<SubLayer> subLayersforSecondFiltering = secondFilter.getSecond();

		// Pass Operation's parameters if needed
		Operation op = toFilter.getFirst();
		if (op != null && op instanceof OperationOnTexture) {
			OperationOnTexture opt = ((OperationOnTexture) op);
			float min_val = (opt.getMin() - opt.getValMin()) / (opt.getValMax() - opt.getValMin());
			float max_val = (opt.getMax() - opt.getValMin()) / (opt.getValMax() - opt.getValMin());

			// min max for filtering
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "filterMinValue"), min_val);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "filterMaxValue"), max_val);
		}

		// Pass second Operation's parameters if needed
		Operation sop = secondFilter.getFirst();
		if (sop != null && sop instanceof OperationOnTexture) {
			OperationOnTexture opt = ((OperationOnTexture) sop);
			float min_val = (opt.getMin() - opt.getValMin()) / (opt.getValMax() - opt.getValMin());
			float max_val = (opt.getMax() - opt.getValMin()) / (opt.getValMax() - opt.getValMin());

			// min max for filtering
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "secondFilterMinValue"), min_val);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "secondFilterMaxValue"), max_val);
		}

		// subLayers needed in current Operation
		int numTexUnitsUsed = subLayerstoBeRendered.size() + subLayersforFiltering.size() + subLayersforSecondFiltering.size();

		// Check number of texture units available
		IntBuffer value = IntBuffer.allocate(1);
		gl.glGetIntegerv(GL2ES2.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, value);
		int nbTextureUnitAvailables = value.get(0);

		try {
			// active alpha blending
			gl.glEnable(GL.GL_DEPTH_TEST);
			gl.glDepthFunc(GL.GL_LEQUAL);

			gl.glEnable(GL2ES1.GL_ALPHA_TEST);
			gl.glAlphaFunc(GL.GL_GREATER, 0.0f);

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glEnable(GL.GL_TEXTURE_2D);
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glPushMatrix();

			// Active all texture unit needed
			i = 0;
			int j = 1;
			for (SubLayer subLayer : subLayerstoBeRendered) {
				if (i > 0) {
					if (subLayer != null) {
						// don't allocate texture unit if not needed
						gl.glActiveTexture(GL.GL_TEXTURE0 + j);
						j++;
						gl.glEnable(GL.GL_TEXTURE_2D);
						gl.glMatrixMode(GL.GL_TEXTURE);
						gl.glPushMatrix();
					}
				}
				i++;
			}

			// does a filter is used?
			if (subLayersforFiltering.size() != 0) {
				gl.glUniform1f(gl.glGetUniformLocation(layer.getShaderprograms(gl), "useTextureFilter"), (float) 1.0);
			} else {
				gl.glUniform1f(gl.glGetUniformLocation(layer.getShaderprograms(gl), "useTextureFilter"), (float) 0.0);
			}

			// does a second filter is used?
			if (subLayersforSecondFiltering.size() != 0) {
				gl.glUniform1f(gl.glGetUniformLocation(layer.getShaderprograms(gl), "useTextureSecondFilter"), (float) 1.0);
			} else {
				gl.glUniform1f(gl.glGetUniformLocation(layer.getShaderprograms(gl), "useTextureSecondFilter"), (float) 0.0);
			}

			int nbTex = j;
			// if filters, enabling texture units
			for (SubLayer subLayer : subLayersforFiltering) {
				if (subLayer != null) {
					// don't allocate texture unit if not needed
					gl.glActiveTexture(GL.GL_TEXTURE0 + j);
					j++;
					gl.glEnable(GL.GL_TEXTURE_2D);
					gl.glMatrixMode(GL.GL_TEXTURE);
					gl.glPushMatrix();
				}
			}

			for (SubLayer subLayer : subLayersforSecondFiltering) {
				if (subLayer != null) {
					// don't allocate texture unit if not needed
					gl.glActiveTexture(GL.GL_TEXTURE0 + j);
					j++;
					gl.glEnable(GL.GL_TEXTURE_2D);
					gl.glMatrixMode(GL.GL_TEXTURE);
					gl.glPushMatrix();
				}
			}

			dc.getSurfaceGeometry().beginRendering(dc);

			// For each current geometry tile, find the intersecting image tiles
			// and render the geometry
			// tile once for each intersecting image tile.
			Transform transform = new Transform();
			for (SectorGeometry sg : dc.getSurfaceGeometry()) {
				Iterable<SurfaceTile> tilesToRender = this.getIntersectingTiles(dc, sg, tiles);
				if (tilesToRender == null) {
					continue;
				}
				try {
					sg.beginRendering(dc, numTexUnitsUsed); // TODO: wrap in

					// set reference center
					// RenderInfo info = moveSectorGemotry(dc, sg);
					// dc.getView().setReferenceCenter(dc,
					// info.getReferenceCenter());
				} catch (Throwable e) {
					Logging.logger().log(Level.SEVERE, "SurfaceTileRenderer begin rendering", e);
				}

				// Pre-load info to compute the texture transform
				this.preComputeTextureTransform(dc, sg, transform);

				for (SurfaceTile tile : tilesToRender) {
					if (tile instanceof MultiTextureTile) {
						MultiTextureTile textureTile = (MultiTextureTile) tile;

						// Determine and apply texture transform to map image
						// tile into geometry tile's texture space
						Sector tileSector = this.computeTextureTransform(dc, textureTile, transform);

						// TODO Check maximum texture units available through
						// nbTextureUnitAvailables
						// divide rendering to match number of textures
						// available

						int k = 0;
						// pass SubLayers ' texture to Texture units
						// corresponding texture unit is passed to the shader
						for (SubLayer subLayer : subLayerstoBeRendered) {
							gl.glActiveTexture(GL.GL_TEXTURE0 + k);
							if (textureTile.bind(dc, subLayer, indexDate)) {
								// bind texture unit
								int pos = gl.glGetUniformLocation(layer.getShaderprograms(gl), "Texture" + k);
								gl.glUniform1i(pos, k);
								k++;

								gl.glMatrixMode(GL.GL_TEXTURE);
								gl.glLoadIdentity();
								textureTile.applyInternalTransform(dc, subLayer, true, indexDate);

								gl.glScaled(transform.HScale, transform.VScale, 1d);
								gl.glTranslated(transform.HShift, transform.VShift, 0d);

								// /change interpolation mode for displayin
								// texture pixels
								// useful to check texture definition
								// (scientific purpose)
								if (!Viewer3D.isInterpolate()) {
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								} else {
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								}
							}
						}

						// filter
						int l = 0;
						SubLayer subLayer;
						// pass operation SubLayers ' texture to Texture units
						if (subLayersforFiltering.size() != 0) {
							for (SubLayer s : subLayerstoBeRendered) {
								// case filtering SubLayer is an allgroupID one
								if (subLayersforFiltering.size() == 1) {
									subLayer = subLayersforFiltering.get(0);
								} else {
									subLayer = subLayersforFiltering.get(l);
								}

								// do not rebind if not needed
								if (subLayersforFiltering.size() != 1 || l == 0) {
									gl.glActiveTexture(GL.GL_TEXTURE0 + k);
									textureTile.bind(dc, subLayer, indexDate);
								}

								// bind texture unit
								int pos = gl.glGetUniformLocation(layer.getShaderprograms(gl), "TextureFilter" + l);
								gl.glUniform1i(pos, k);

								// do not increment for allgroupID SubLayer
								// they use the same texture unit
								if (!(subLayer.getGroupID().equals(MultiTextureLayer.allGroupId)) || l == subLayerstoBeRendered.size() - 1) {
									k++;
								}
								l++;

								gl.glMatrixMode(GL.GL_TEXTURE);
								gl.glLoadIdentity();
								tile.applyInternalTransform(dc, true);

								gl.glScaled(transform.HScale, transform.VScale, 1d);
								gl.glTranslated(transform.HShift, transform.VShift, 0d);

								// /change interpolation mode for displaying
								// texture pixels
								// useful to check texture definition
								// (scientific purpose)
								// the same interpolation is used for displayed
								// textures and operation textures
								if (!Viewer3D.isInterpolate()) {
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								} else {
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								}

							}
						}

						// second filter
						int m = 0;
						// pass operation SubLayers ' texture to Texture units
						if (subLayersforSecondFiltering.size() != 0) {
							for (SubLayer s : subLayerstoBeRendered) {
								// case filtering SubLayer is an allgroupID one
								if (subLayersforSecondFiltering.size() == 1) {
									subLayer = subLayersforSecondFiltering.get(0);
								} else {
									subLayer = subLayersforSecondFiltering.get(m);
								}

								// do not rebind if not needed
								if (subLayersforSecondFiltering.size() != 1 || m == 0) {
									gl.glActiveTexture(GL.GL_TEXTURE0 + k);
									textureTile.bind(dc, subLayer, indexDate);
								}

								// bind texture unit
								int pos = gl.glGetUniformLocation(layer.getShaderprograms(gl), "TextureSecondFilter" + m);
								gl.glUniform1i(pos, k);

								// do not increment for allgroupID SubLayer
								// they use the same texture unit
								if (!(subLayer.getGroupID().equals(MultiTextureLayer.allGroupId)) || m == subLayerstoBeRendered.size() - 1) {
									k++;
								}
								m++;

								gl.glMatrixMode(GL.GL_TEXTURE);
								gl.glLoadIdentity();
								tile.applyInternalTransform(dc, true);

								gl.glScaled(transform.HScale, transform.VScale, 1d);
								gl.glTranslated(transform.HShift, transform.VShift, 0d);

								// /change interpolation mode for displaying
								// texture pixels
								// useful to check texture definition
								// (scientific purpose)
								// the same interpolation is used for displayed
								// textures and operation textures
								if (!Viewer3D.isInterpolate()) {
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								} else {
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
									gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
								}

							}
						}

						gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "width"), ((MultiTextureTile) tile).getWidth());
						gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "height"), ((MultiTextureTile) tile).getHeight());

						// nb texture used in shader
						// should be numTexUnitsUsed or 2*numTexUnitsUsed if
						// filter is used
						gl.glUniform1f(gl.glGetUniformLocation(layer.getShaderprograms(gl), "nbTexture"), nbTex);

						// cellSize
						gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "cellSize"), (float) (10000 * textureTile.getLevel().getTexelSize()));

						sg.renderMultiTexture(dc, numTexUnitsUsed);
					}
				}
				try {
					sg.endRendering(dc);
				} catch (Throwable e) {
					Logging.logger().log(Level.SEVERE, "SurfaceTileRenderer begin rendering", e);
				}

			}
		} catch (Exception e) {
			Logging.logger().log(Level.SEVERE, Logging.getMessage("generic.ExceptionWhileRenderingLayer", this.getClass().getName()), e);
		} finally {
			dc.getSurfaceGeometry().endRendering(dc);

			for (int ii = 0; ii < numTexUnitsUsed; ii++) {
				// reverse indice
				// because pop should be symmetric of push
				gl.glActiveTexture(GL.GL_TEXTURE0 + numTexUnitsUsed - ii - 1);
				gl.glMatrixMode(GL.GL_TEXTURE);

				gl.glPopMatrix();
				gl.glDisable(GL.GL_TEXTURE_2D);
				i++;
			}

			gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, OGLUtil.DEFAULT_TEX_ENV_MODE);

			gl.glPopAttrib();

			// disable shader
			gl.glUseProgram(0);

			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
		}
	}

	protected TextureData initTexture(TextureData textureDataOriginale, int width, int height) {
		ByteBuffer textureBytes = Buffers.newDirectByteBuffer(width * height);
		for (int i = 0; i < textureBytes.capacity(); i++) {
			textureBytes.put((byte) 0xff);
		}

		GLProfile glprofile = GLProfile.getDefault();
		TextureData textureData = new TextureData(glprofile, GL.GL_RGBA, width, height, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, false, false, false, textureBytes.rewind(), null);

		return textureData;
	}

	protected static void fillShortBuffer(ShortBuffer buffer, short value) {
		for (int i = 0; i < buffer.capacity(); i++) {
			buffer.put(value);
		}
	}

	// copy from GeographicSurfaceTileRenderer and SurfaceTileRenderer because
	// of private accessor
	public static class Transform {
		public double HScale;
		public double VScale;
		public double HShift;
		public double VShift;
		public double rotationDegrees;
	}

	protected void preComputeTextureTransform(DrawContext dc, SectorGeometry sg, Transform t) {
		Sector st = sg.getSector();
		PrivateAccessor.setField(this, "sgWidth", GeographicSurfaceTileRenderer.class, st.getDeltaLonRadians());
		PrivateAccessor.setField(this, "sgHeight", GeographicSurfaceTileRenderer.class, st.getDeltaLatRadians());
		PrivateAccessor.setField(this, "sgMinWE", GeographicSurfaceTileRenderer.class, st.getMinLongitude().radians);
		PrivateAccessor.setField(this, "sgMinSN", GeographicSurfaceTileRenderer.class, st.getMinLatitude().radians);
	}

	protected Sector computeTextureTransform(DrawContext dc, SurfaceTile tile, Transform t) {
		Sector st = tile.getSector();

		double tileWidth = st.getDeltaLonRadians();
		double tileHeight = st.getDeltaLatRadians();

		double minLon = st.getMinLongitude().radians;
		double minLat = st.getMinLatitude().radians;

		double sgHeight = (Double) PrivateAccessor.getField(this, "sgHeight", GeographicSurfaceTileRenderer.class);

		double sgWidth = (Double) PrivateAccessor.getField(this, "sgWidth", GeographicSurfaceTileRenderer.class);

		double sgMinSN = (Double) PrivateAccessor.getField(this, "sgMinSN", GeographicSurfaceTileRenderer.class);
		double sgMinWE = (Double) PrivateAccessor.getField(this, "sgMinWE", GeographicSurfaceTileRenderer.class);

		double vShift = -(minLat - sgMinSN) / sgHeight;
		double hShift = -(minLon - sgMinWE) / sgWidth;

		t.VScale = tileHeight > 0 ? sgHeight / tileHeight : 1;
		t.HScale = tileWidth > 0 ? sgWidth / tileWidth : 1;

		t.VShift = vShift;
		t.HShift = hShift;
		t.rotationDegrees = 0;

		return st;
	}

	@Override
	protected Iterable<SurfaceTile> getIntersectingTiles(DrawContext dc, SectorGeometry sg, Iterable<? extends SurfaceTile> tiles) {
		ArrayList<SurfaceTile> intersectingTiles = null;

		for (SurfaceTile tile : tiles) {
			if (!tile.getSector().intersectsInterior(sg.getSector())) {
				continue;
			}

			if (intersectingTiles == null) {
				// case is no intersecting tiles
				intersectingTiles = new ArrayList<SurfaceTile>();
			}

			intersectingTiles.add(tile);
		}

		return intersectingTiles; // will be null if no intersecting tiles
	}

	// ///////////////////////////////////////////

	/***
	 * initialize shader program with rendered {@link SubLayer}'s
	 * {@link SubLayerParameters}
	 * 
	 * @param gl
	 * @return shaderProgram ID
	 */
	protected int useShader(GL gl1) {
		GL2 gl = gl1.getGL2();
		if (layer == null) {
			return -1;
		}

		SubLayer subLayer = layer.getCurrentSubLayer();
		SubLayerParameters parameters = layer.getLayerParameters().getSubLayerParameters(subLayer.getType());
		// RGB data => shader disabled
		if (subLayer == null || subLayer.isRGB() || layer.getShaderprograms(gl) == 0) {
			return -1;
		}

		int shaderProgram = layer.getShaderprograms(gl);
		gl.glUseProgram(shaderProgram);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "offset"), (float) parameters.getOffset());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), (float) layer.getOpacity());

		if (parameters.isInverseColorMap()) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "inverseColormap"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "inverseColormap"), (float) 0.0);
		}

		float min_val = (float) ((parameters.getMinContrast() - subLayer.getValMin() - 1.0) / (subLayer.getValMax() - subLayer.getValMin()));
		float max_val = (float) ((parameters.getMaxContrast() - subLayer.getValMin() - 1.0) / (subLayer.getValMax() - subLayer.getValMin()));

		// contrast
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "min"), min_val);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "max"), max_val);

		min_val = (float) ((parameters.getMinTransparency() - subLayer.getValMin()) / (subLayer.getValMax() - subLayer.getValMin()));
		max_val = (float) ((parameters.getMaxTransparency() - subLayer.getValMin()) / (subLayer.getValMax() - subLayer.getValMin()));

		// transparency contrast
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minTransparency"), min_val);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxTransparency"), max_val);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "globalMin"), (float) subLayer.getValMin());
		// for cast purpose
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "globalMax"), (float) (subLayer.getValMax() + 1.0));

		float[][] colormapBuffer = ColorMap.getColormap(ColorMap.getColorMapIntIndex(parameters.getColorMap()));
		int colorMapLength = 256;
		gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "colorMapLength"), colorMapLength);

		// colormap in one array for efficiency
		// and because shader can't handle too many parameters
		int[] colormap = new int[256];
		for (int index = 0; index < colorMapLength; index++) {
			colormap[index] = (int) (colormapBuffer[ColorMap.red][index] * 255) * 256 * 256 + (int) (colormapBuffer[ColorMap.green][index] * 255) * 256
					+ (int) (colormapBuffer[ColorMap.blue][index] * 255);
		}

		gl.glUniform1iv(gl.glGetUniformLocation(shaderProgram, "colormap"), colorMapLength, colormap, 0);

		if (parameters.isUseOmbrage()) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrage"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrage"), (float) 0.0);
		}

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "azimuth"), (float) (parameters.getAzimuth()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "zenith"), (float) (parameters.getZenith()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageExaggeration"), (float) (parameters.getOmbrageExaggeration()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "resolution"), (float) (layer.getResolution()));

		// log de la color map
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "logColormap"), (float) (0.0));

		// filtrages
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "filterName"), parameters.getFilterName());

		// filtrage de l elevation
		// gaussien
		if (parameters.getFilterName() == 0.0)// "gauss"
		{
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "sigmaGauss"), (float) (parameters.getSigmaGauss()));
		}
		// alpha-lineaire ou lee
		else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "filterWidth"), (float) (parameters.getFilterWidth()));
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "filterHeight"), (float) (parameters.getFilterHeight()));
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "Lookup"), lookup);

			if (parameters.getFilterName() == 1) // "alphaLineaire"
			{
				gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "alphaFilter"), (float) (alphaFilter));
			}
		}

		if (parameters.getUseOmbrageLogarithmique()) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageLogarithmique"), (float) (1.0));
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageLogarithmique"), (float) (0.0));
		}

		if (parameters.getUseGradient()) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "gradient"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "gradient"), (float) 0.0);
		}

		return shaderProgram;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWidth() {
		return width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

}
