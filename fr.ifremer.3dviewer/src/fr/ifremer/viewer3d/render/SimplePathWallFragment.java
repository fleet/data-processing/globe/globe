package fr.ifremer.viewer3d.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.globe.ogl.renderer.jogl.buffers.FloatVertexBuffer;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.MyAbstractLayerParameter;
import fr.ifremer.viewer3d.model.PathWallLines;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.render.BasicWWTexture;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.util.Logging;

/**
 * Affiche une texture verticale sous un chemin.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
@SuppressWarnings("unused")
public class SimplePathWallFragment implements OrderedRenderable {
	protected static final String SIMPLE_SHADER_VERT = "/shader/simpleShader.vert";
	protected static final String TRANSPARENCY_SHADER_FRAG = "/shader/transparencyShaderColorMap.frag";
	protected static final String ALPHA_SHADER_FRAG = "/shader/alphaShader.frag";

	/** Path locations. */
	protected PathWallLines lines;

	/** Current drawn texture. */
	protected BasicWWTexture texture;

	/** Current opacity. */
	protected double opacity = 1;

	/** Number of points : top (points in the navigation line) and matching bottom ones (points on the floor line). */
	protected int nbNavLinePoints;

	protected boolean recomputeModel;

	/** For picking. */
	protected PickSupport pickSupport = new PickSupport();

	/**
	 * Render shader program. One shader program instance needed for each OGL context.
	 */
	protected Map<GL, Integer> shaderPrograms = new HashMap<GL, Integer>(4);

	protected final Logger logger = LoggerFactory.getLogger(SimplePathWallFragment.class);

	/**
	 * Distance from draw context's "eye" to this textured wall.
	 * 
	 * @see #updateEyeDistance(DrawContext)
	 */
	protected double eyeDistance;

	/** The layer where the SimplePathWall is rendered onto. */
	protected final MyAbstractLayer layer;

	/** Reference center. */
	protected Vec4 referenceCenter;

	/** Points buffer (x,y,z). CPU-RAM buffer intended to be transferred into GPU-RAM. */
	protected FloatVertexBuffer pointsBuffer = new FloatVertexBuffer(3);

	/** Texture buffer (u,v). CPU-RAM buffer intended to be transferred into GPU-RAM. */
	protected FloatVertexBuffer texBuffer = new FloatVertexBuffer(2);

	protected ISectorMovementProcessing movement;

	protected double verticalExaggeration;

	protected BufferedImage imageSource;

	protected boolean isUseMipMaps;

	protected Object lastInterpolation;

	protected boolean isInitialized;

	class ColorPalette{
		// Argument about color palette of this renderer
		protected Texture paletteTexture;
		private BufferedImage paletteImage;
		protected int currentPalette;
		protected boolean currentPaletteInverted;
		/** Generated ID of the color palette shader program. */
	}
	private int[] texturesId;
	ColorPalette palette=new ColorPalette();



	/**
	 * Builds a new SimplePathWall with a source image as texture.
	 * 
	 * @param imageSource
	 *            Source image.
	 * @param line
	 *            Navigation line.
	 * @param layer
	 *            Layer where the object is rendered onto.
	 * @param useMipMaps
	 *            <code>true</code> to use mipmaps automatic generation.
	 */
	public SimplePathWallFragment(BufferedImage imageSource, PathWallLines line, MyAbstractLayer layer, boolean useMipMaps) {

		this.eyeDistance = 100000.0;
		this.recomputeModel = true;
		this.imageSource = imageSource;
		this.isUseMipMaps = useMipMaps;

		if (imageSource == null) {
			String message = Logging.getMessage("nullValue.ImageSource");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (line == null) {
			String message = Logging.getMessage("nullValue.LatLonIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		this.lastInterpolation = MyAbstractLayerParameter.INTERPOLATION_LINEAR;
		this.lines = line;
		this.layer = layer;
		this.movement = layer.getMovement();
		this.verticalExaggeration = 1.0;
	}

	/**
	 * Ensures the object is well initialized (ie. that dependencies on external environment is established, like listeners on NWW window).
	 */
	protected void ensureInitialized() {

		if (!isInitialized) {

			this.verticalExaggeration = Viewer3D.getWwd().getSceneController().getVerticalExaggeration();

			this.layer.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if (MyAbstractLayer.PROTERTY_MOVEMENT.equals(evt.getPropertyName())) {
						ISectorMovementProcessing oldMovement = (ISectorMovementProcessing) evt.getOldValue();
						if (oldMovement != null) {
							oldMovement.removePropertyChangeListener(this);
						}
						ISectorMovementProcessing newMovement = (ISectorMovementProcessing) evt.getNewValue();
						if (newMovement != null && newMovement != movement) {
							movement = newMovement;
							movement.addPropertyChangeListener(this);
						}
						recomputeModel = true;
					}
					if (evt.getSource() == movement && ISectorMovementProcessing.PROPERTY_DATE_INDEX.equals(evt.getPropertyName())) {
						recomputeModel = true;
					}
					if (evt.getPropertyName().equals(MyAbstractLayer.PROPERTY_OFFSET)) {
						recomputeModel = true;
					}
				}
			});

			Viewer3D.getWwd().getSceneController().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					onChangeVerticalExaggeration(evt);
				}
			});
			isInitialized = true;
		}
	}

	/**
	 * Reacts to any change on the layer's movement.
	 * 
	 * @param evt
	 *            The original PropertyChangeEvent.
	 */
	protected void onChangeVerticalExaggeration(PropertyChangeEvent evt) {
		if (AVKey.VERTICAL_EXAGGERATION.equals(evt.getPropertyName())) {
			double oldExageration = ((Double) evt.getOldValue()).doubleValue();
			double newExageration = ((Double) evt.getNewValue()).doubleValue();
			if (verticalExaggeration != newExageration) {
				verticalExaggeration = newExageration;

				// Don't recompute model for minimal modifications of vertical exaggeration
				if (((int) Math.abs(oldExageration - newExageration)) > 0) {
					recomputeModel = true;
				}
			}
		}
	}

	/**
	 * Unbind the texture from the cache and then re-creates it with mipmaps property or not.
	 * 
	 * @param dc
	 *            The current drawing context.
	 * @param useMipMaps
	 *            <code>true</code> to create the texture with mipmaps capability.
	 */
	protected void refreshTexture(DrawContext dc, boolean useMipMaps) {
		if (this.texture != null && this.imageSource != null) {
			dc.getTextureCache().remove(this.imageSource);
		}
		this.texture = new BasicWWTexture(this.imageSource, useMipMaps);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void render(DrawContext dc) {

		GL2 gl = dc.getGL().getGL2();

		// Ensure the SimplePathWall has been fully initialized before the first rendering
		if(!isInitialized)
		{
			ensureInitialized();
			// Generate an ID to the buffer texture
			this.texturesId = new int[2];
			gl.glGenTextures(2, this.texturesId, 0);
		}
		ensureInitialized();

		// Compute the interpolation mode
		boolean useMipMaps = this.isUseMipMaps;

		Object layerInterpolationParameter = layer.getValue(MyAbstractLayerParameter.INTERPOLATION);
		if (lastInterpolation != layerInterpolationParameter && (lastInterpolation == null || !lastInterpolation.equals(layerInterpolationParameter))) {
			lastInterpolation = layerInterpolationParameter;
			recomputeModel = true;
		}
		if (lastInterpolation == null || MyAbstractLayerParameter.INTERPOLATION_NEAREST.equals(lastInterpolation)) {
			useMipMaps = false;
		}
		if (recomputeModel || this.texture == null) {
			refreshTexture(dc, useMipMaps);
		}

		try {
			if (!dc.isPickingMode()) {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
						| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for depth func
						| GL2.GL_TEXTURE_BIT // for texture env
						| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

				// Enable blending using white premultiplied by the current opacity.
				double opacity = dc.getCurrentLayer() != null ? this.layer.getOpacity() * dc.getCurrentLayer().getOpacity() : this.layer.getOpacity();
				gl.glColor4d(opacity, opacity, opacity, opacity);
				gl.glEnable(GL2.GL_BLEND);
				gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);



				// Bind texture
				this.bindTexture(dc);

				initializeColorPalette(gl);

				if (useMipMaps && texture.isUseMipMaps()) {
					int interpolation = GL2.GL_NEAREST_MIPMAP_NEAREST;
					if (MyAbstractLayerParameter.INTERPOLATION_LINEAR.equals(layerInterpolationParameter)) {
						interpolation = GL2.GL_LINEAR_MIPMAP_LINEAR;
					}
					gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, interpolation);
					gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
					gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAX_LEVEL, 10);
				} else {
					int interpolation = GL2.GL_NEAREST;
					gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, interpolation);
					gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST); // only GL2.GL_NEAREST or GL2.GL_LINEAR

				}
				gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_EDGE);
				gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_EDGE);

			} else {
				gl.glPushAttrib(GL2.GL_POLYGON_BIT);
			}

			gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);

			this.draw(dc);

		} finally {
			if (!dc.isPickingMode()) {
				gl.glPopAttrib();
			}
		}
	}

	/**
	 * Binds the texture in the OpenGL context. If the texture has been uncached by NWW, it will be reloaded and that operation may take a long time as your source image is big.
	 * 
	 * @param dc
	 *            Drawing c ontext.
	 * @param gl
	 */
	protected void bindTexture(DrawContext dc) {
		dc.getGL().glEnable(GL.GL_TEXTURE_2D);
		this.texture.bind(dc);
	}

	/**
	 * Computes or recomputes the model (ie. all points of the navigation line,
	 * top and bottom ones). This method should be called as rarely as possible.
	 * 
	 * @param dc
	 *            Drawing context.
	 */
	protected void computeModel(DrawContext dc) {
		pointsBuffer.clear();

		// conversion des latlon en points
		List<Position> posList = new ArrayList<Position>(lines.size());
		List<Position> bottomPosList = new ArrayList<Position>(lines.size());

		double ve = verticalExaggeration;

		for (int x = 0; x < lines.size(); x++) {
			Position top = lines.getTop(x);
			if (top != null && movement != null) {
				LatLon move = movement.move(top);
				top = new Position(move, top.elevation);
			}

			Position bot = lines.getBottom(x);
			if (bot != null && movement != null) {
				LatLon move = movement.move(bot);
				bot = new Position(move, bot.elevation);
			}

			// Point haut
			posList.add(new Position(top, (layer.getOffset() + top.elevation) * ve));
			bottomPosList.add(new Position(bot, (bot.getElevation() + layer.getOffset()) * ve));
		}

		nbNavLinePoints = posList.size();

		float[][][] points = new float[nbNavLinePoints][2][3];

		// calcul de la matrice
		referenceCenter = null;
		for (int x = 0; x < nbNavLinePoints; x++) {

			// first point is used as reference center
			Vec4 mpoint = dc.getGlobe().computePointFromPosition(posList.get(x));
			if (referenceCenter == null) {
				referenceCenter = mpoint;
			}
			mpoint = mpoint.subtract3(referenceCenter);

			points[x][0][0] = (float) mpoint.x;
			points[x][0][1] = (float) mpoint.y;
			points[x][0][2] = (float) mpoint.z;

			mpoint = dc.getGlobe().computePointFromPosition(bottomPosList.get(x));
			mpoint = mpoint.subtract3(referenceCenter);

			points[x][1][0] = (float) mpoint.x;
			points[x][1][1] = (float) mpoint.y;
			points[x][1][2] = (float) mpoint.z;
		}

		pointsBuffer.allocate(nbNavLinePoints * 2);
		texBuffer.allocate(nbNavLinePoints * 2);

		for (int x = 0; x < nbNavLinePoints; x = x + 1) {

			// calculs des coordonnées de la texture
			float float_x = (float) x / (nbNavLinePoints - 1);
			float float_y = 0;
			float float_xb = (float) (x + 1) / (nbNavLinePoints);
			pointsBuffer.put(points[x][0]);
			pointsBuffer.put(points[x][1]);

			texBuffer.put(float_x);
			texBuffer.put(float_y);

			texBuffer.put(float_x);
			texBuffer.put(1);

			// texBuffer.put(float_xb);
			// texBuffer.put(float_y);
			//
			// texBuffer.put(float_xb);
			// texBuffer.put(1);
		}

		GL2 gl = dc.getGL().getGL2();

		pointsBuffer.sendToGpu(gl);
		texBuffer.sendToGpu(gl);
	}

	protected void initializeColorPalette(GL2 gl) {


		if (this.palette.paletteImage == null || this.palette.currentPalette != (int) this.layer.getColorMap() || this.palette.currentPaletteInverted != this.layer.isInverseColorMap()) {
			// Reset the variables which defines the condition just before
			this.palette.currentPalette = (int) this.layer.getColorMap();
			this.palette.currentPaletteInverted = this.layer.isInverseColorMap();
			// Build the buffered image which will represent the choosen palette
			float[][] colors = ColorMap.getColormap(this.palette.currentPalette);
			int width = 100;
			int height = 5;
			this.palette.paletteImage = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(width, height);
			// Define a shader program for color palette generation
			Graphics2D g2 = (Graphics2D) palette.paletteImage.getGraphics();
			// Fill the buffered image with the color defined by the choosen palette
			for (int i = 0; i < width; i++) {
				int index = Math.round(i / (float) width * 256);	
				int r = (int) (colors[ColorMap.red][index] * 255);
				int g = (int) (colors[ColorMap.green][index] * 255);
				int b = (int) (colors[ColorMap.blue][index] * 255);
				// Use OpenGL instruction to draw a vertival line with the desired color
				g2.setColor(new Color(r, g, b));
				g2.drawLine(i, 0, i, height);
			}
			// Dispose the shader program used for color palette generation
			g2.dispose();
			// Generation of a texture from the buffered image.
			this.palette.paletteTexture = AWTTextureIO.newTexture(gl.getGLProfile(), this.palette.paletteImage, false);
		}
	}

	protected void draw(DrawContext dc) {
		float float_x, float_y, float_xb, float_yb;

		GL2 gl = dc.getGL().getGL2();

		// calcul du modÃ¨le
		if (recomputeModel) {
			computeModel(dc);
		}

		if (referenceCenter != null) {
			dc.getView().pushReferenceCenter(dc, referenceCenter);
		}

		try {
			if (dc.isPickingMode()) {
				Color color = dc.getUniquePickColor();
				int colorCode = color.getRGB();
				this.pickSupport.addPickableObject(colorCode, this.layer);

				gl.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());
			}

			int shaderProgram = 0;

			if (layer.isUseColorMap() || layer.isUseContrast()) {

				if (shaderPrograms.containsKey(gl)) {
					shaderProgram = shaderPrograms.get(gl);
				} else {
					shaderProgram = ShaderUtil.createShader(this, gl, SIMPLE_SHADER_VERT, TRANSPARENCY_SHADER_FRAG);
					if (shaderProgram != 0) {
						shaderPrograms.put(gl, shaderProgram);
					}
				}

				if (shaderProgram != 0) {
					gl.glUseProgram(shaderProgram);

					// set uniform variables

					// Opacity
					gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), (float) layer.getOpacity());

					//Inverse color map
					gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "inverseColormap"),
							layer.isInverseColorMap() ? (float) 1.0 : (float) 0.0);

					//Contrast 
					float minContrastFloat = (float) ((layer.getMinContrast()- layer.getMinTextureValue())/(layer.getMaxTextureValue() - layer.getMinTextureValue()));
					float maxContrastFloat = (float) ((layer.getMaxContrast()- layer.getMinTextureValue())/(layer.getMaxTextureValue() - layer.getMinTextureValue()));
					gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minContrastValue"), minContrastFloat);
					gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxContrastValue"), maxContrastFloat);

					//Transparency (= filtering threshold) 
					float minTransparencyFloat = (float) ((layer.getMinTransparency()- layer.getMinTextureValue())/(layer.getMaxTextureValue() - layer.getMinTextureValue()));
					float maxTransparencyFloat = (float) ((layer.getMaxTransparency()- layer.getMinTextureValue())/(layer.getMaxTextureValue() - layer.getMinTextureValue()));

					gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minTransparency"), minTransparencyFloat);
					gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxTransparency"), maxTransparencyFloat);
					gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "colormapOri"), layer.getColorMapOri());

					dc.getGL().glEnable(GL.GL_TEXTURE_2D);

					gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "texture"), 0);
					gl.glActiveTexture(GL2.GL_TEXTURE0);
					this.texture.bind(dc);

					// Initialize a uniform which will manage the color palette
					gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "palette"), 1);
					gl.glActiveTexture(GL2.GL_TEXTURE1);
					gl.glBindTexture(GL2.GL_TEXTURE_2D, texturesId[1]);
					this.palette.paletteTexture.bind(gl);
				}

			}

			// Enable vertex arrays
			gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
			gl.glEnableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

			// Set the vertex pointer to the vertex buffer
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, pointsBuffer.getBufferName());
			gl.glVertexPointer(pointsBuffer.getVerticeSize(), GL.GL_FLOAT, 0, 0);

			// texture
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, texBuffer.getBufferName());
			gl.glTexCoordPointer(texBuffer.getVerticeSize(), GL.GL_FLOAT, 0, 0);
			// Rendering
			gl.glDrawArrays(GL.GL_TRIANGLE_STRIP, 0, nbNavLinePoints * 2 );

			// Initialize buffer
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

			unbindPalette(gl);
			// Clean context
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
			gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
			gl.glDisableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

			if (shaderProgram != 0) {
				gl.glUseProgram(0);
			}

			if (referenceCenter != null) {
				dc.getView().popReferenceCenter(dc);
			}
		} catch (GIOException e) {
			logger.error("Error reading fragment shader", e);
		}

		recomputeModel = false;
	}
	private void unbindPalette(GL2 gl) {
		// Unbind color texture
		if (palette.paletteTexture != null) {
			palette.paletteTexture.disable(gl);
		}		
	}


	/**
	 * Updates distance from draw context's "eye" to this textured wall, needed by {@link OrderedRenderable} behavior.
	 * 
	 * @param dc
	 *            current draw context
	 */
	public void updateEyeDistance(DrawContext dc) {
	}

	@Override
	public double getDistanceFromEye() {
		return eyeDistance;
	}

	@Override
	public void pick(DrawContext dc, Point pickPoint) {
		if( texture != null) {
			this.pickSupport.clearPickList();
			this.pickSupport.beginPicking(dc);
			try {
				draw(dc);
			} finally {
				this.pickSupport.endPicking(dc);
				this.pickSupport.resolvePick(dc, pickPoint, this.layer);
			}
		}
	}

	/**
	 * @return the {@link #recomputeModel}
	 */
	public boolean isRecomputeModel() {
		return recomputeModel;
	}

	/**
	 * @param recomputeModel the {@link #recomputeModel} to set
	 */
	public void setRecomputeModel(boolean recomputeModel) {
		this.recomputeModel = recomputeModel;
	}
}