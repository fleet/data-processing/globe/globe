package fr.ifremer.viewer3d.render;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.utils.exception.runtime.NotImplementedException;
import fr.ifremer.viewer3d.geom.IMovableActivator;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.Polyline;

/**
 * Renderer for GeoBox
 */
public class GeoBoxRenderer extends Polyline implements IMovableActivator {

	/**
	 * Constructor
	 */
	public GeoBoxRenderer(GeoBox geoBox) {
		super();
		List<Position> points = new ArrayList<>();
		points.add(Position.fromDegrees(geoBox.getBottom(), geoBox.getLeft()));
		points.add(Position.fromDegrees(geoBox.getTop(), geoBox.getLeft()));
		points.add(Position.fromDegrees(geoBox.getTop(), geoBox.getRight()));
		points.add(Position.fromDegrees(geoBox.getBottom(), geoBox.getRight()));
		this.setPositions(points);
		this.setFollowTerrain(true);
		this.setClosed(true);
	}

	@Override
	public boolean isMovable() {
		return false;
	}

	@Override
	public void setMovable(boolean value) {
		throw new NotImplementedException();// not movable
	}
}