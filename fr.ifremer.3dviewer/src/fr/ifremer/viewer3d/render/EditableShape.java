package fr.ifremer.viewer3d.render;

import gov.nasa.worldwind.geom.Position;

public interface EditableShape {

	/**
	 * Define new location for point at index idx.
	 * 
	 * @param idx
	 *            the point index to move
	 * @param pos
	 *            the new point location.
	 */
	public void setLocation(int idx, Position pos);

	public void setEditing(boolean value);
}
