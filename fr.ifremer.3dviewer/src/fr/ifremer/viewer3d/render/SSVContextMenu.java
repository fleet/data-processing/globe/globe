/**
 * 
 */
package fr.ifremer.viewer3d.render;

import fr.ifremer.viewer3d.util.contextmenu.ContextMenuInfo;
import gov.nasa.worldwind.render.ScreenAnnotation;

import java.awt.Point;

/**
 * Context menu implementation based on OGL rendering.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class SSVContextMenu extends ScreenAnnotation {

	private final ContextMenuInfo data;

	private final Object source;

	/**
	 * Creates a new OGL context menu.
	 * 
	 * @param info
	 *            the menu content
	 * @param position
	 *            the menu location
	 * @param source
	 *            the source object on which this context menu apply
	 */
	public SSVContextMenu(ContextMenuInfo info, Point position, Object source) {
		super(info.getHTMLContent(), position);
		this.data = info;
		this.source = source;
	}

	/**
	 * Returns context menu data content.
	 */
	public ContextMenuInfo getInfo() {
		return this.data;
	}

	public Object getSource() {
		return this.source;
	}
}
