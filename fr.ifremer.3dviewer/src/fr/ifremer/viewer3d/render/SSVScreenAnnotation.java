package fr.ifremer.viewer3d.render;

import gov.nasa.worldwind.render.AnnotationAttributes;
import gov.nasa.worldwind.render.ScreenAnnotation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Etends la classe ScreenAnnotation pour ajouter des fonctionnalité de
 * selection et de "mode d'utilisation" courant.
 * <p>
 * Affiche uniquement des icons de taille 64x64.
 * </p>
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class SSVScreenAnnotation extends ScreenAnnotation {

	/**
	 * Map used to store radio buttons behavior groups for screen annotations.
	 * 
	 * @see #setRadioGroup(String)
	 */
	protected static Map<String, List<SSVScreenAnnotation>> radioGroups = new HashMap<String, List<SSVScreenAnnotation>>();

	/** Small (default) icon size. */
	private Dimension smallSize = new Dimension(32, 32);
	private double smallScale = 0.5;
	/** Medium (highlighted) icon size. */
	private Dimension mediumSize = new Dimension(48, 48);
	private double mediumScale = 0.75;

	/**
	 * This widget radio group, may be null is it doesn't belong to any radio
	 * group.
	 * 
	 * @see #setRadioGroup(String)
	 */
	private String radioGroup = null;

	/**
	 * True if currently selected.
	 * 
	 */
	private boolean selected = false;

	/** List of actions who runs on this annotation selection. */
	private List<Runnable> actions = new ArrayList<Runnable>();

	/**
	 * {@inheritDoc}
	 * 
	 * @param imgSrc
	 *            image source file, must be 64x64
	 */
	public SSVScreenAnnotation(String text, Point position, AnnotationAttributes defaults, String imgSrc) {
		super(text, position, defaults);
		getAttributes().setImageSource(imgSrc);
		getAttributes().setImageScale(smallScale);
	}

	public boolean isSelected() {
		return this.selected;
	}

	/**
	 * Returns radio group name for this screen annotation radio behavior.
	 * 
	 * @see SSVScreenAnnotation#setRadioGroup(String)
	 */
	public String getRadioGroup() {
		return this.radioGroup;
	}

	/**
	 * Define radio group name for this screen annotation radio behavior.
	 * <p>
	 * Each group is uniquely identified by its name as a string. When any
	 * widget in a group is selected all other widgets belonging to this group
	 * are automatically unselected.
	 * </p>
	 * <p>
	 * Only one radio group is allowed per screen annotation.
	 * </p>
	 */
	public void setRadioGroup(String group) {
		synchronized (radioGroups) {
			// removes from another group if it exists
			if (this.radioGroup != null) {
				List<SSVScreenAnnotation> last = radioGroups.get(group);
				if (last != null) {
					last.remove(this);
					this.radioGroup = null;
				}
			}

			if (group != null) {
				List<SSVScreenAnnotation> g = radioGroups.get(group);
				if (g == null) {
					g = new ArrayList<SSVScreenAnnotation>();
					radioGroups.put(group, g);
				}
				g.add(this);
				this.radioGroup = group;
			}
		}
	}

	/**
	 * Place le widget en surbrillance ou non.
	 * 
	 * @param highlighted
	 */
	public void highlight(boolean highlighted) {
		// if already selected, nothing to do
		if (selected) {
			return;
		}

		if (highlighted) {
			getAttributes().setImageOpacity(1);
			getAttributes().setSize(mediumSize);
			getAttributes().setImageScale(mediumScale);
		} else {
			getAttributes().setImageOpacity(-1);
			getAttributes().setSize(smallSize);
			getAttributes().setImageScale(smallScale);
		}
	}

	/**
	 * Selectionne le widget.
	 * 
	 * @param value
	 */
	public void select(boolean value) {
		if (this.selected == value) {
			return;
		}
		// radio group behavior
		if (value && this.radioGroup != null) {
			List<SSVScreenAnnotation> group = radioGroups.get(this.radioGroup);
			for (SSVScreenAnnotation annotation : group) {
				annotation.select(false);
			}
		}

		this.selected = value;
		if (value) {
			getAttributes().setImageOpacity(1);
			getAttributes().setSize(mediumSize);
			getAttributes().setImageScale(mediumScale);
			getAttributes().setBorderColor(Color.GREEN);
			getAttributes().setBorderWidth(1.0);
		} else {
			getAttributes().setImageOpacity(-1);
			getAttributes().setSize(smallSize);
			getAttributes().setImageScale(smallScale);
			getAttributes().setBorderWidth(0.0);
		}
	}

	/**
	 * Ajoute une action à la liste des actions réalisées par ce widget.
	 */
	public void addAction(Runnable action) {
		if (!actions.contains(action)) {
			actions.add(action);
		}
	}

	/**
	 * Exécute les actions associées à ce widget.
	 */
	public void runActions() {
		for (Runnable r : actions) {
			r.run();
		}
	}
}
