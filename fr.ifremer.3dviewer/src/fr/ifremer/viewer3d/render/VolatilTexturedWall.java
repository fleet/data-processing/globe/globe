package fr.ifremer.viewer3d.render;

import java.awt.Color;
import java.awt.Point;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GL2GL3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.texture.TextureCoords;

import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Triangle;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.util.Logging;

/**
 * This renderable draws a vertical wall textured from modifiable bytebuffer
 * data ({@link #updateData(ByteBuffer)}).
 * <p>
 * It implements {@link OrderedRenderable} interface due to its translucent
 * appearance : multiples instance of this class must be sorted before
 * rendering.
 * </p>
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class VolatilTexturedWall implements OrderedRenderable {
	boolean enableInterpolationForWaterColumn=false;

	private final Logger logger = LoggerFactory.getLogger(VolatilTexturedWall.class);

	protected static final String SIMPLE_SHADER_VERT = "/shader/simpleShader.vert";
	protected static final String TRANSPARENCY_SHADER_FRAG = "/shader/transparencyShader.frag";
	protected static final String ALPHA_SHADER_FRAG = "/shader/alphaShader.frag";

	/**
	 * Two locations of location points : starboard (0) and port (1).
	 */
	private LatLon[] locations;
	/**
	 * Elevations for this texture { bottom elevation, top elevation }.
	 */
	private double[] elevations;
	/**
	 * Current texture.
	 */
	protected MemoryMappedTexture texture;
	/**
	 * This textured wall opacity.
	 * <p>
	 * Even with an opacity value to 1.0 some texels could be translucent as
	 * with factor is applied on input RGBA images.
	 * </p>
	 */
	private double opacity = 1.0;

	/** For picking. */
	private PickSupport pickSupport = new PickSupport();

	/**
	 * Distance from draw context's "eye" to this textured wall.
	 * 
	 * @see #updateEyeDistance(DrawContext)
	 */
	private double eyeDistance;

	/**
	 * Render shader program. One shader program instance needed for each OGL
	 * context.
	 */
	private Map<GL, Integer> shaderPrograms = new HashMap<GL, Integer>(4);

	/**
	 * Shader programs for picking. One shader program instance needed for each
	 * OGL context.
	 */
	private Map<GL, Integer> pickPrograms = new HashMap<GL, Integer>(4);

	/** Parent layer. */
	private final MyAbstractLayer layer;

	private Supplier<Object> pickObjectBuilder;

	/**
	 * Ctor.
	 * 
	 * @param bb
	 *            input image bytebuffer
	 * @param location1
	 *            starboard location
	 * @param location2
	 *            port location
	 * @param bottomElevation
	 *            bottom elevation (m)
	 * @param topElevation
	 *            top elevation (m)
	 */
	public VolatilTexturedWall(MyAbstractLayer parent, LatLon location1, LatLon location2, double bottomElevation, double topElevation,Supplier<Object> pickObjectBuilder) {

		if (location1 == null || location2 == null) {
			String message = Logging.getMessage("nullValue.LatLonIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		this.layer = parent;

		this.locations = new LatLon[] { location1, location2 };
		this.elevations = new double[] { bottomElevation, topElevation };

		boolean mipmaps = false;
		this.texture = new MemoryMappedTexture(mipmaps);
		this.pickObjectBuilder = pickObjectBuilder;
	}

	public void dispose() {
		texture.dispose();
	}

	/**
	 * Opacity factor [0.0, 1.0].
	 */
	public double getOpacity() {
		return this.opacity;
	}

	/**
	 * Opacity factor [0.0, 1.0].
	 */
	public void setOpacity(double opacity) {
		this.opacity = opacity;
	}

	public LatLon getLocation1() {
		return this.locations[0];
	}

	public LatLon getLocation2() {
		return this.locations[1];
	}

	public void setLocation1(LatLon latlon) {
		this.locations[0] = latlon;
	}

	public void setLocation2(LatLon latlon) {
		this.locations[1] = latlon;
	}

	public double getBottomElevation() {
		return this.elevations[0] + layer.getOffset();
	}

	public double getTopElevation() {
		return this.elevations[1] + layer.getOffset();
	}

	public void setBottomElevation(double elevation) {
		this.elevations[0] = elevation;
	}

	public void setTopElevation(double elevation) {
		this.elevations[1] = elevation;
	}

	public void updateImageFile(String imageFile) {
		this.texture.setImageFile(imageFile);
	}

	@Override
	public void render(DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		int shaderProgram = 0;

		try {
			if (shaderPrograms.containsKey(gl)) {
				shaderProgram = shaderPrograms.get(gl);
			} else {
				shaderProgram = ShaderUtil.createShader(this, gl, SIMPLE_SHADER_VERT, TRANSPARENCY_SHADER_FRAG);
				if (shaderProgram != 0) {
					shaderPrograms.put(gl, shaderProgram);
				}
			}

			if (shaderProgram != 0) {
				gl.glUseProgram(shaderProgram);

				int shader_inverseColorMap = gl.glGetUniformLocation(shaderProgram, "inverseColormap");
				int shader_min = gl.glGetUniformLocation(shaderProgram, "min");
				int shader_max = gl.glGetUniformLocation(shaderProgram, "max");
				int shader_minTransparency = gl.glGetUniformLocation(shaderProgram, "minTransparency");
				int shader_maxTransparency = gl.glGetUniformLocation(shaderProgram, "maxTransparency");
				int shader_colormap = gl.glGetUniformLocation(shaderProgram, "colormap");
				int shader_colormapOri = gl.glGetUniformLocation(shaderProgram, "colormapOri");

				gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), (float) layer.getOpacity());

				int shader_width = gl.glGetUniformLocation(shaderProgram, "width");
				int shader_height = gl.glGetUniformLocation(shaderProgram, "height");

				// set uniform variables
				if (layer.isInverseColorMap()) {
					gl.glUniform1f(shader_inverseColorMap, (float) 1.0);
				} else {
					gl.glUniform1f(shader_inverseColorMap, (float) 0.0);
				}
				gl.glUniform1f(shader_min, (float) (layer.getMinContrast() / 255));
				gl.glUniform1f(shader_max, (float) (layer.getMaxContrast() / 255));
				gl.glUniform1f(shader_minTransparency, (float) (layer.getMinTransparency() / 255));
				gl.glUniform1f(shader_maxTransparency, (float) (layer.getMaxTransparency() / 255));
				gl.glUniform1f(shader_colormap, layer.getColorMap());
				gl.glUniform1f(shader_colormapOri, layer.getColorMapOri());

				gl.glUniform1f(shader_width, this.texture.width);
				gl.glUniform1f(shader_height, this.texture.height);
			}
		} catch (GIOException e) {
			logger.error("Error reading fragment shader", e);
		}
		try {
			if (!dc.isPickingMode()) {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
						| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
						// depth
						// func
						| GL2.GL_TEXTURE_BIT // for texture env
						| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

				// Enable blending using white premultiplied by the current
				// opacity.
				double opacity = dc.getCurrentLayer() != null ? this.getOpacity() * dc.getCurrentLayer().getOpacity() : this.getOpacity();
				gl.glColor4d(opacity, opacity, opacity, opacity);
				gl.glEnable(GL.GL_BLEND);
				gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

				// Bind texture
				gl.glEnable(GL.GL_TEXTURE_2D);
				this.texture.bind(dc);
				// this.texture.applyInternalTransform(dc);


			} else {
				gl.glPushAttrib(GL2.GL_POLYGON_BIT);
			}

			gl.glPolygonMode(GL.GL_FRONT, GL2GL3.GL_FILL);

			this.draw(dc);
		} finally {
			gl.glPopAttrib();
		}

		gl.glEnable(GL.GL_DEPTH_TEST);// 05/08/10
		gl.glDepthMask(true);

		if (shaderProgram != 0) {
			gl.glUseProgram(0);
		}
	}

	/**
	 * Renders this textured wall on specified draw context.
	 * 
	 * @param dc
	 *            textured context to draw onto.
	 */
	protected void draw(DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();

		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
				| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
				// depth
				// func
				| GL2.GL_TEXTURE_BIT // for texture env
				| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

		// Enable blending using white premultiplied by the current
		// opacity.
		double opacity = dc.getCurrentLayer() != null ? this.getOpacity() * dc.getCurrentLayer().getOpacity() : this.getOpacity();
		gl.glColor4d(opacity, opacity, opacity, opacity);
		gl.glEnable(GL.GL_BLEND);
		// OpenGL Transparency Mode for this wall
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2ES1.GL_ALPHA_TEST);
		gl.glAlphaFunc(GL.GL_GREATER, 0.001f);

		// Bind texture
		gl.glEnable(GL.GL_TEXTURE_2D);
		this.texture.bind(dc);
		// /change interpolation mode for displayin
		// texture pixels
		// useful to check texture definition
		// (scientific purpose)
		if (!enableInterpolationForWaterColumn || !Viewer3D.isInterpolate()) {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
		} else {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
		}

		double ve = dc.getVerticalExaggeration();
		TextureCoords coords = this.texture.getTexCoords();
		// considers tectonic movement
		LatLon loc0 = LatLon.fromDegrees(this.locations[0].getLatitude().getDegrees(), this.locations[0].getLongitude().getDegrees());
		LatLon loc1 = LatLon.fromDegrees(this.locations[1].getLatitude().getDegrees(), this.locations[1].getLongitude().getDegrees());

		if (coords != null) {
			Vec4 reference = dc.getGlobe().computePointFromPosition(loc0, getBottomElevation() * ve);
			Vec4 p1 = new Vec4(0.0, 0.0, 0.0);
			Vec4 p2 = dc.getGlobe().computePointFromPosition(loc0, getTopElevation() * ve);
			p2 = p2.subtract3(reference);
			Vec4 p3 = dc.getGlobe().computePointFromPosition(loc1, getBottomElevation() * ve);
			p3 = p3.subtract3(reference);
			Vec4 p4 = dc.getGlobe().computePointFromPosition(loc1, getTopElevation() * ve);
			p4 = p4.subtract3(reference);

			dc.getView().pushReferenceCenter(dc, reference);
			gl.glBegin(GL.GL_TRIANGLE_STRIP);

			gl.glTexCoord2d(coords.left(), coords.bottom());
			gl.glVertex3d(p1.x, p1.y, p1.z);

			gl.glTexCoord2d(coords.left(), coords.top());
			gl.glVertex3d(p2.x, p2.y, p2.z);

			gl.glTexCoord2d(coords.right(), coords.bottom());
			gl.glVertex3d(p3.x, p3.y, p3.z);

			gl.glTexCoord2d(coords.right(), coords.top());
			gl.glVertex3d(p4.x, p4.y, p4.z);

			gl.glEnd();
			dc.getView().popReferenceCenter(dc);
		}

		gl.glPopAttrib();
	}

	/**
	 * Updates distance from draw context's "eye" to this textured wall, needed
	 * by {@link OrderedRenderable} behavior.
	 * 
	 * @param dc
	 *            current draw context
	 */
	public void updateEyeDistance(DrawContext dc) {
		double distance = 1.0;
		if (dc != null && dc.getView() != null) {

			double elevation = (this.elevations[1] + this.elevations[0]) / 2.0;
			double ve = dc.getVerticalExaggeration();
			Vec4 point0 = dc.getGlobe().computePointFromPosition(this.locations[0], elevation * ve);
			Vec4 point1 = dc.getGlobe().computePointFromPosition(this.locations[1], elevation * ve);

			Vec4 eyePoint = dc.getView().getEyePoint();
			double d0 = eyePoint.distanceTo3(point0);
			double d1 = eyePoint.distanceTo3(point1);
			distance = Math.min(d0, d1);
		}
		this.eyeDistance = distance;
	}

	/**
	 * This eye distance is {@inheritDoc}
	 * 
	 */
	@Override
	public double getDistanceFromEye() {
		return this.eyeDistance;
	}

	public int getWidth() {
		if (texture != null) {
			return texture.width;
		}
		return 0;
	}

	public int getHeight() {
		if (texture != null) {
			return texture.height;
		}
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pick(DrawContext dc, Point pickPoint) {
		this.pickSupport.clearPickList();
		this.pickSupport.beginPicking(dc);

		Color color = dc.getUniquePickColor();
		int colorCode = color.getRGB();

		// compute pick position on this texture
		double ve = dc.getVerticalExaggeration();
		Line ray = dc.getView().computeRayFromScreenPoint(pickPoint.getX(), pickPoint.getY());
		Vec4 point0 = dc.getGlobe().computePointFromPosition(this.locations[0], getBottomElevation() * ve);
		Vec4 point1 = dc.getGlobe().computePointFromPosition(this.locations[0], getTopElevation() * ve);
		Vec4 point2 = dc.getGlobe().computePointFromPosition(this.locations[1], getBottomElevation() * ve);
		Vec4 point3 = dc.getGlobe().computePointFromPosition(this.locations[1], getTopElevation() * ve);
		Triangle triangle = new Triangle(point0, point1, point2);
		Vec4 inter = triangle.intersect(ray);
		if (inter == null) {
			triangle = new Triangle(point1, point2, point3);
			inter = triangle.intersect(ray);
		}
		Position pickPosition = null;
		if (inter != null) {
			pickPosition = dc.getGlobe().computePositionFromPoint(inter);
		}
		this.pickSupport.addPickableObject(colorCode, pickObjectBuilder.get(), pickPosition, false);

		GL2 gl = dc.getGL().getGL2();

		gl.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());

		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
				| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
				// depth
				// func
				| GL2.GL_TEXTURE_BIT // for texture env
				| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

		// Bind texture
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
		this.texture.bind(dc);

		int pickShader = 0;
		if (pickPrograms.containsKey(gl)) {
			pickShader = pickPrograms.get(gl);
		} else {
			try {
				pickShader = ShaderUtil.createShader(this, gl, SIMPLE_SHADER_VERT, ALPHA_SHADER_FRAG);
				pickPrograms.put(gl, pickShader);
			} catch (GIOException e) {
				logger.error("Unable to create picking shader.", e);
			}
		}

		if (pickShader != 0) {
			gl.glUseProgram(pickShader);
		}

		TextureCoords coords = this.texture.getTexCoords();
		if (coords != null) {
			Vec4 reference = dc.getGlobe().computePointFromPosition(this.locations[0], getBottomElevation() * ve);
			Vec4 p1 = new Vec4(0.0, 0.0, 0.0);
			Vec4 p2 = dc.getGlobe().computePointFromPosition(this.locations[0], getTopElevation() * ve);
			p2 = p2.subtract3(reference);
			Vec4 p3 = dc.getGlobe().computePointFromPosition(this.locations[1], getBottomElevation() * ve);
			p3 = p3.subtract3(reference);
			Vec4 p4 = dc.getGlobe().computePointFromPosition(this.locations[1], getTopElevation() * ve);
			p4 = p4.subtract3(reference);

			dc.getView().pushReferenceCenter(dc, reference);
			gl.glBegin(GL.GL_TRIANGLE_STRIP);

			gl.glTexCoord2d(coords.left(), coords.bottom());
			gl.glVertex3d(p1.x, p1.y, p1.z);

			gl.glTexCoord2d(coords.left(), coords.top());
			gl.glVertex3d(p2.x, p2.y, p2.z);

			gl.glTexCoord2d(coords.right(), coords.bottom());
			gl.glVertex3d(p3.x, p3.y, p3.z);

			gl.glTexCoord2d(coords.right(), coords.top());
			gl.glVertex3d(p4.x, p4.y, p4.z);

			gl.glEnd();
			dc.getView().popReferenceCenter(dc);
		}

		if (pickShader != 0) {
			gl.glUseProgram(0);
		}

		gl.glPopAttrib();

		this.pickSupport.endPicking(dc);
		this.pickSupport.resolvePick(dc, pickPoint, this.layer);
	}

	protected static Vec4 getVertex3(int position, DoubleBuffer vertices) {
		double[] compArray = new double[3];
		vertices.position(3 * position);
		vertices.get(compArray, 0, 3);
		return Vec4.fromArray3(compArray, 0);
	}


}
