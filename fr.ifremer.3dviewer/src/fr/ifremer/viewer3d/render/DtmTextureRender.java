package fr.ifremer.viewer3d.render;

import fr.ifremer.viewer3d.layers.DtmTextureLayer;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.SurfaceImage;
import gov.nasa.worldwind.util.Logging;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2GL3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.nio.Buffers;

/**
 * Surface renderer for images computed from MNT files.
 * 
 * @see MNTTextureLayer
 */
public class DtmTextureRender extends SurfaceImage {

	private Logger logger = LoggerFactory.getLogger(DtmTextureRender.class);

	private static final String FRAGMENT_SHADER = "/shader/mntcharlie.frag";
	private static final String VERTEX_SHADER = "/shader/elevationShader16.vert";

	/** One shader program instance needed for each OGL context. */
	private Map<GL, Integer> shaderPrograms = new HashMap<GL, Integer>(4);

	/**
	 * Renders a single image tile from a local image source.
	 * 
	 * @param imageSource
	 *            either the file path to a local image or a
	 *            <code>BufferedImage</code> reference.
	 * @param sector
	 *            the sector covered by the image.
	 */
	public DtmTextureRender(Object imageSource, Sector sector) {
		if (imageSource == null) {
			String message = Logging.getMessage("nullValue.ImageSource");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (sector == null) {
			String message = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		this.setImageSource(imageSource, sector);
	}

	@Override
	public void render(DrawContext dc) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (dc.isPickingMode() && !this.isPickEnabled()) {
			return;
		}

		if (!this.getSector().intersects(dc.getVisibleSector())) {
			return;
		}

		if (this.sourceTexture == null) {
			return;
		}

		GL2 gl = dc.getGL().getGL2();
		try {
			if (!dc.isPickingMode()) {
				double opacity = dc.getCurrentLayer() != null ? this.getOpacity() * dc.getCurrentLayer().getOpacity() : this.getOpacity();

				if (opacity < 1) {
					gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT | GL2.GL_CURRENT_BIT);
					// Enable blending using white premultiplied by the current
					// opacity.
					gl.glColor4d(opacity, opacity, opacity, opacity);
				} else {
					gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT);
				}
				gl.glEnable(GL.GL_BLEND);
				gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
			} else {
				gl.glPushAttrib(GL2.GL_POLYGON_BIT);
			}

			gl.glPolygonMode(GL.GL_FRONT, GL2GL3.GL_FILL);
			gl.glEnable(GL.GL_CULL_FACE);
			gl.glCullFace(GL.GL_BACK);

			if (!shaderPrograms.containsKey(gl)) {
				initialize(gl);
			}
			int _shaderProgram = shaderPrograms.get(gl);

			// utilisation des shaders
			if (_useOmbrage || _useColorMap || _useContrast) {

				gl.glUseProgram(_shaderProgram);
				int shader_inverseColorMap = gl.glGetUniformLocation(_shaderProgram, "inverseColormap");
				/*
				 * int shader_min = gl.glGetUniformLocation(_shaderProgram,
				 * "min"); int shader_max =
				 * gl.glGetUniformLocation(_shaderProgram, "max"); int
				 * shader_globalMin = gl.glGetUniformLocation(_shaderProgram,
				 * "globalMin"); int shader_globalMax =
				 * gl.glGetUniformLocation(_shaderProgram, "globalMax");
				 */
				int shader_colormap = gl.glGetUniformLocation(_shaderProgram, "colormap");
				int shader_ombrage = gl.glGetUniformLocation(_shaderProgram, "ombrage");
				int shader_gradient = gl.glGetUniformLocation(_shaderProgram, "gradient");

				int shader_azimuth = gl.glGetUniformLocation(_shaderProgram, "azimuth");
				int shader_zenith = gl.glGetUniformLocation(_shaderProgram, "zenith");
				int shader_ombrageExaggeration = gl.glGetUniformLocation(_shaderProgram, "ombrageExaggeration");
				int shader_cellSize = gl.glGetUniformLocation(_shaderProgram, "cellSize");

				int shader_width = gl.glGetUniformLocation(_shaderProgram, "width");
				int shader_height = gl.glGetUniformLocation(_shaderProgram, "height");

				int shader_opacity = gl.glGetUniformLocation(_shaderProgram, "opacity");

				gl.glUniform1f(shader_opacity, (float) opacity);

				if (_inverseColorMap) {
					gl.glUniform1f(shader_inverseColorMap, (float) 1.0);
				} else {
					gl.glUniform1f(shader_inverseColorMap, (float) 0.0);
				}

				// float min_val =
				// (float)((_minTextureContrast+20000.0d)/40000.0d*256.0d/255.0d);
				// float max_val =
				// (float)((_maxTextureContrast+20000.0d)/40000.0d*256.0d/255.0d);
				// gl.glUniform1f(shader_min, min_val);
				// gl.glUniform1f(shader_max, max_val);
				/*
				 * gl.glUniform1f(shader_min, (float)_minTextureContrast);
				 * gl.glUniform1f(shader_max, (float)_maxTextureContrast);
				 * 
				 * gl.glUniform1f(shader_globalMin, (float)
				 * _globaleMinTextureContrast); gl.glUniform1f(shader_globalMax,
				 * (float) _globaleMaxTextureContrast);
				 */

				gl.glUniform1f(shader_colormap, _colormap);
				if (_useOmbrage) {
					gl.glUniform1f(shader_ombrage, (float) 1.0);
				} else {
					gl.glUniform1f(shader_ombrage, (float) 0.0);
				}

				gl.glUniform1f(shader_azimuth, (float) (_azimuth));
				gl.glUniform1f(shader_zenith, (float) (_zenith));
				gl.glUniform1f(shader_ombrageExaggeration, (float) (_ombrageExaggeration));
				// TODO cellsize
				gl.glUniform1f(shader_cellSize, (10000));
				// gl.glUniform1f(shader_cellSize,
				// (float)(10000*textureTile.getLevel().getTexelSize()));

				if (_useGradient) {
					gl.glUniform1f(shader_gradient, (float) 1.0);
				} else {
					gl.glUniform1f(shader_gradient, (float) 0.0);
				}

				gl.glUniform1f(shader_width, width);
				gl.glUniform1f(shader_height, height);

				int shader_use = gl.glGetUniformLocation(_shaderProgram, "use");
				if (!_useOmbrage && !_useColorMap && !_useContrast) {
					gl.glUniform1f(shader_use, (float) 0.0);
				} else {
					gl.glUniform1f(shader_use, (float) 1.0);
				}
			}

			dc.getGeographicSurfaceTileRenderer().renderTiles(dc, this.thisList);

			// inihib shader
			if (_useOmbrage || _useColorMap || _useContrast) {
				gl.glUseProgram(0);
			}
		} finally {
			gl.glPopAttrib();
		}
	}

	public void setUseContrast(boolean _useContrast) {
		this._useContrast = _useContrast;
	}

	public boolean isUseContrast() {
		return _useContrast;
	}

	public void setMinTextureContrast(double _minTextureContrast) {
		this._minTextureContrast = _minTextureContrast;
	}

	public double getMinTextureContrast() {
		return _minTextureContrast;
	}

	public void setMaxTextureContrast(double _maxTextureContrast) {
		this._maxTextureContrast = _maxTextureContrast;
	}

	public double getMaxTextureContrast() {
		return _maxTextureContrast;
	}

	public void setUseColorMap(boolean _useColorMap) {
		this._useColorMap = _useColorMap;
	}

	public boolean isUseColorMap() {
		return _useColorMap;
	}

	public void setColormap(float _colormap) {
		this._colormap = _colormap;
	}

	public float getColormap() {
		return _colormap;
	}

	public void setInverseColorMap(boolean _inverseColorMap) {
		this._inverseColorMap = _inverseColorMap;
	}

	public boolean isInverseColorMap() {
		return _inverseColorMap;
	}

	public void setUseOmbrage(boolean _useOmbrage) {
		this._useOmbrage = _useOmbrage;
	}

	public boolean isUseOmbrage() {
		return _useOmbrage;
	}

	public void setAzimuth(double _azimuth) {
		this._azimuth = _azimuth;
	}

	public double getAzimuth() {
		return _azimuth;
	}

	public void setZenith(double _zenith) {
		this._zenith = _zenith;
	}

	public double getZenith() {
		return _zenith;
	}

	public void setOmbrageExaggeration(double _exaggeration) {
		this._ombrageExaggeration = _exaggeration;
	}

	public double getOmbrageExaggeration() {
		return _ombrageExaggeration;
	}

	public void setMinTextureValue(double _minTextureValue) {
		this._minTextureValue = _minTextureValue;
	}

	public double getMinTextureValue() {
		return _minTextureValue;
	}

	public void setMaxTextureValue(double _maxTextureValue) {
		this._maxTextureValue = _maxTextureValue;
	}

	public double getMaxTextureValue() {
		return _maxTextureValue;
	}

	public void setMinRecommandeValue(double _minRecommandeValue) {
		this._minRecommandeValue = _minRecommandeValue;
	}

	public double getMinRecommandeValue() {
		return _minRecommandeValue;
	}

	public void setMaxRecommandeValue(double _maxRecommandeValue) {
		this._maxRecommandeValue = _maxRecommandeValue;
	}

	public double getMaxRecommandeValue() {
		return _maxRecommandeValue;
	}

	public void setIsRGB(boolean _isRGB) {
		this._isRGB = _isRGB;
	}

	public boolean isRGB() {
		return _isRGB;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWidth() {
		return width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	public void setUseGradient(boolean _useGradient) {
		this._useGradient = _useGradient;
	}

	public boolean isUseGradient() {
		return _useGradient;
	}

	// contrast
	private boolean _useContrast;
	private double _minTextureContrast;
	private double _maxTextureContrast;
	@SuppressWarnings("unused")
	private double _globaleMinTextureContrast;
	@SuppressWarnings("unused")
	private double _globaleMaxTextureContrast;

	// colormap
	private boolean _useColorMap = true;
	// default colormap caraibes
	private float _colormap = 3;
	private boolean _inverseColorMap = false;

	// ombrage
	private boolean _useOmbrage = false;
	private boolean _useGradient = false;

	private double _azimuth = 90.0;
	private double _zenith = 0;
	private double _ombrageExaggeration = 1;

	// texture
	private double _minTextureValue;
	private double _maxTextureValue;
	private double _minRecommandeValue;
	private double _maxRecommandeValue;
	private boolean _isRGB;

	private int width;
	private int height;
	private double opacity = 1.0;

	public void setGlobalMin(double valMin) {
		_globaleMinTextureContrast = valMin;

	}

	public void setGlobalMax(double valMax) {
		_globaleMaxTextureContrast = valMax;

	}

	@Override
	public void setOpacity(double opacity) {
		this.opacity = opacity;
	}

	private void initialize(GL gl) {
		// Creation Shader
		try {
			if (!shaderPrograms.containsKey(gl)) {
				shaderPrograms.put(gl, createShader(gl));
			}
		} catch (IOException e) {
			logger.error("Error reading fragment shader : ", e);
		}
	}

	private int createShader(GL gl1) throws IOException {
		GL2 gl = gl1.getGL2();
		boolean isShaderSupported = (gl.isFunctionAvailable("glCreateShader") && gl.isFunctionAvailable("glShaderSource") && gl.isFunctionAvailable("glCompileShader")
				&& gl.isFunctionAvailable("glCreateProgram") && gl.isFunctionAvailable("glAttachShader") && gl.isFunctionAvailable("glLinkProgram") && gl.isFunctionAvailable("glValidateProgram") && gl
				.isFunctionAvailable("glUseProgram"));

		if (isShaderSupported) {
			String shaderFile = null;
			shaderFile = VERTEX_SHADER;

			BufferedReader brv = getShaderReader(shaderFile);
			shaderFile = FRAGMENT_SHADER;

			BufferedReader brf = getShaderReader(shaderFile);

			if (brv == null || brf == null) {
				return 0;
			}

			int vertID = gl.glCreateShader(GL2ES2.GL_VERTEX_SHADER);
			int fragID = gl.glCreateShader(GL2ES2.GL_FRAGMENT_SHADER);

			// FIXME BRL pourquoi ce 800 !!!???
			String[] vertSource = new String[800];

			for (int i = 0; i < vertSource.length; ++i) {
				vertSource[i] = "";
			}

			int[] length = new int[1];
			String line;
			int k = 0;
			while ((line = brv.readLine()) != null) {
				vertSource[k] = line + "\n";
				k++;
			}

			int[] vertLengths = new int[vertSource.length];
			for (int i = 0; i < vertLengths.length; ++i) {
				vertLengths[i] = vertSource[i].length();
			}

			length[0] = vertSource.length;
			gl.glShaderSource(vertID, vertSource.length, vertSource, vertLengths, 0);
			gl.glCompileShader(vertID);

			// debug compilation vertShader
			int statusVertexShader[] = new int[1];
			gl.glGetShaderiv(vertID, GL2ES2.GL_COMPILE_STATUS, IntBuffer.wrap(statusVertexShader));
			if (statusVertexShader[0] == GL.GL_FALSE) {
				int infoLogLenght[] = new int[1];
				gl.glGetShaderiv(vertID, GL2ES2.GL_INFO_LOG_LENGTH, IntBuffer.wrap(infoLogLenght));
				ByteBuffer infoLog = Buffers.newDirectByteBuffer(infoLogLenght[0]);
				gl.glGetShaderInfoLog(vertID, infoLogLenght[0], null, infoLog);
				byte[] infoBytes = new byte[infoLogLenght[0]];
				infoLog.get(infoBytes);
				String out = new String(infoBytes);
				System.out.println("Vertex shader error:\n" + out);
				if (logger.isDebugEnabled()) {
					logger.debug(out);
				}
			}

			// FIXME arrrgghhh
			String fragSource[] = new String[800];

			for (int i = 0; i < fragSource.length; ++i) {
				fragSource[i] = "";
			}

			k = 0;
			while ((line = brf.readLine()) != null) {
				fragSource[k] = line + "\n";
				k++;
			}

			int[] fragLengths = new int[fragSource.length];
			for (int i = 0; i < fragLengths.length; ++i) {
				fragLengths[i] = fragSource[i].length();
			}

			length[0] = 0;
			gl.glShaderSource(fragID, fragSource.length, fragSource, fragLengths, 0);
			gl.glCompileShader(fragID);

			// debug compilation fragShader
			int statusFragmentShader[] = new int[1];
			gl.glGetShaderiv(fragID, GL2ES2.GL_COMPILE_STATUS, IntBuffer.wrap(statusFragmentShader));
			if (statusFragmentShader[0] == GL.GL_FALSE) {
				int infoLogLenght[] = new int[1];
				gl.glGetShaderiv(fragID, GL2ES2.GL_INFO_LOG_LENGTH, IntBuffer.wrap(infoLogLenght));
				ByteBuffer infoLog = Buffers.newDirectByteBuffer(infoLogLenght[0]);
				gl.glGetShaderInfoLog(fragID, infoLogLenght[0], null, infoLog);
				byte[] infoBytes = new byte[infoLogLenght[0]];
				infoLog.get(infoBytes);
				String out = new String(infoBytes);
				System.out.println("Fragment shader error:\n" + out);
				if (logger.isDebugEnabled()) {
					logger.debug(out);
				}
			}

			int shaderprogram = gl.glCreateProgram();
			gl.glAttachShader(shaderprogram, vertID);
			gl.glAttachShader(shaderprogram, fragID);

			gl.glLinkProgram(shaderprogram);
			gl.glValidateProgram(shaderprogram);

			// debug compilation linker Shader
			int statusLinker[] = new int[1];
			gl.glGetProgramiv(shaderprogram, GL2ES2.GL_LINK_STATUS, IntBuffer.wrap(statusLinker));
			if (statusLinker[0] == GL.GL_FALSE) {
				int infoLogLenght[] = new int[1];
				gl.glGetShaderiv(shaderprogram, GL2ES2.GL_INFO_LOG_LENGTH, IntBuffer.wrap(infoLogLenght));
				ByteBuffer infoLog = Buffers.newDirectByteBuffer(infoLogLenght[0]);
				gl.glGetProgramInfoLog(shaderprogram, infoLogLenght[0], null, infoLog);
				byte[] infoBytes = new byte[infoLogLenght[0]];
				infoLog.get(infoBytes);
				String out = new String(infoBytes);
				System.out.println("Compilation Linker Shader error:\n" + out);
				if (logger.isDebugEnabled()) {
					logger.debug(out);
				}
			}

			return shaderprogram;
		}
		return 0;
	}

	/** Only returns a buffered reader from a filename in this classpath. */
	private BufferedReader getShaderReader(final String shaderFile) {
		BufferedReader brv;
		InputStream is = DtmTextureLayer.class.getResourceAsStream(shaderFile);
		if (is == null) {
			Logging.logger().severe("Cannot read shader file " + shaderFile);
			return null;
		}
		brv = new BufferedReader(new InputStreamReader(is));
		return brv;
	}
}
