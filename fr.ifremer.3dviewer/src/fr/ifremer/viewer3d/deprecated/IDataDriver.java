/* */
package fr.ifremer.viewer3d.deprecated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.reflect.FieldUtils;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Generic interface for any Globe's data reader.
 *
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */
@Deprecated
public interface IDataDriver {

	default Set<ContentType> getContentTypes() {
		return Collections.emptySet();
	}

	default ContentType getContentType(String resource) {
		return ContentType.UNDEFINED;
	}

	/**
	 * Returns true if this driver is able to read provided resource.
	 *
	 * @param resource usually a file's name, but may be an URL or any reference, depending upon the driver's
	 *            implementation.
	 */
	boolean accept(String resource);

	/**
	 * Return available extensions for this driver.
	 * <p>
	 * Return value is a list of all file types known by this driver. Each {@link Pair} is composed by a human readable
	 * format definition (eg. "Scalable Vector Graphics") and the associated extension (eg. "*.svg")
	 * </p>
	 */
	List<Pair<String, String>> extensions();

	/**
	 * Returns an icon for provided resource directly usable with an image constructor.
	 * <p>
	 * The resource parameter allows to retrieve an icon for different views. E.g. it may be a File instance to retrieve
	 * an icon for a file viewer or a geographic layer instance for a geographic viewer. As the allowed types are up to
	 * each driver's implementation, their implementation should provide specific description for allowed types.
	 * </p>
	 * <p>
	 * A basic implementation may be : <code>
	 * URL fileUrl = Platform.getBundle("fr.ifremer.globe.myplugin").getEntry("/");<br/>
	 * String filename = FileLocator.resolve(fileUrl).getFile() + "/icons/myIcon.png"
	 * </code>
	 * </p>
	 *
	 * @param resource basically a file's name, but may be an URL or any reference to an instance. Allowed types depends
	 *            on each driver implementation.
	 */
	String getImage(Object resource);

	/**
	 * Dispose ressources associated with the given infostore
	 */
	default void dispose(IInfos infostore) {
	}

	/**
	 * Return a list of meta-data properties for provided file. Despite to {@link #getMetaData()} who provides meta-data
	 * for computations, this list purpose is read only user information. Therefore it should only contains human
	 * readable data.
	 *
	 * @param resource usually a file's name, but may be an URL or any reference, depending upon the driver's
	 *            implementation.
	 * @throws GIOException
	 */
	default List<Property<?>> getProperties(String resource) throws GIOException {
		return new ArrayList<>();
	}

	int DEFAULT_PRIORITY = 100;

	/**
	 * return priority for driver, driver will be sorted by priority and call to
	 * {@link DataDriverRegistry#selectDataDriver(String)} will return the driver with the highest (smallest) priority
	 */
	default int getPriority() {
		return DEFAULT_PRIORITY;
	}

	/**
	 * Access to the IDataDriver hold on a IInfoStore as as field called driver <br>
	 * This is a temporary way to access to IDataDriver until IDataDriver and IInfoStore are removed
	 */
	static IDataDriver getDriverOn(IFileInfo info) {
		if (info != null) {
			try {
				return (IDataDriver) FieldUtils.readField(info, "driver", true);
			} catch (Exception e) {
				// No driver on this IInfoStore or not a T
			}
		}
		return null;
	}

}
