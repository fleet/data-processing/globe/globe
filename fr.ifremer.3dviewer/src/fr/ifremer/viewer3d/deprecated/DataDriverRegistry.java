package fr.ifremer.viewer3d.deprecated;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

@Deprecated
public class DataDriverRegistry {
	private List<IDataDriver> drivers = new LinkedList<>();
	protected Logger logger = LoggerFactory.getLogger(DataDriverRegistry.class);

	public void addDataDriver(IDataDriver creator) {
		logger.debug("driver registered " + creator);
		drivers.add(creator);
		sortByPriority();
	}

	public void removeDataDriver(IDataDriver creator) {
		drivers.remove(creator);
	}

	private void sortByPriority() {
		Collections.sort(drivers, (arg0, arg1) -> arg0.getPriority() - arg1.getPriority());
	}

	public <T extends IDataDriver> T getDriver(Class<T> driverClass) {
		if (driverClass != null) {
			for (IDataDriver driver : drivers) {
				if (driverClass.isAssignableFrom(driver.getClass())) {
					return driverClass.cast(driver);
				}
			}
		}
		return null;
	}

	/**
	 * Returns the unmodifiable list of available files drivers.
	 *
	 * @see DataDriverRegistry#selectDataDriver(File)
	 */
	public List<IDataDriver> getDrivers() {
		return Collections.unmodifiableList(drivers);
	}

	/** Find data driver associated with provided file. */
	public IDataDriver selectDataDriver(File file) {
		if (file != null) {
			return selectDataDriver(file.getAbsolutePath());
		}
		return null;
	}

	/** Find data driver associated with provided path. */
	@SafeVarargs
	public final IDataDriver selectDataDriver(String path, Class<? extends IDataDriver>... allowedDrivers) {

		List<IDataDriver> result = new ArrayList<>();

		if (path != null) {
			for (IDataDriver driver : drivers) {
				if (allowedDrivers.length != 0) {
					for (Class<? extends IDataDriver> clazz : allowedDrivers) {
						if (clazz.isInstance(driver) && driver.accept(path)) {
							result.add(driver);
						}
					}
				} else if (driver.accept(path)) {
					result.add(driver);
				}
			}
		}

		if (result.size() > 1)

		{
			StringBuilder conflicts = new StringBuilder(path).append("\n");
			for (IDataDriver driver : result) {
				conflicts.append("  --> ").append(driver.getClass().getName()).append(" [priority=")
						.append(driver.getPriority()).append("]\n");
			}

			// Order by priority
			Collections.sort(result, (o1, o2) -> (o1.getPriority() - o2.getPriority()));

			logger.debug("Drivers conflict : " + conflicts.toString() + "\n ==> Driver with highest priority is "
					+ result.iterator().next().getClass().getName());
		}

		Iterator<IDataDriver> iterator = result.iterator();
		return iterator.hasNext() ? iterator.next() : null;
	}

	public static DataDriverRegistry getInstance() {
		return OsgiUtils.getService(DataDriverRegistry.class);
	}

	/* @formatter:off
	  ___     _    _
	 | _ )_ _(_)__| |__ _ ___
	 | _ \ '_| / _` / _` / -_)
	 |___/_| |_\__,_\__, \___|
	                |___/
	@formatter:on */

	public Set<ContentType> getContentTypes() {
		Set<ContentType> result = drivers.stream() //
				.flatMap(driver -> driver.getContentTypes().stream())//
				.collect(Collectors.toSet());
		return result;
	}

	public Optional<BridgeFileInfo> getFileInfo(String filePath) {
		BridgeFileInfo result = null;
		for (IDataDriver driver : drivers) {
			if (driver.accept(filePath)) {
				result = new BridgeFileInfo(filePath, driver.getContentType(filePath), driver);
				break;
			}
		}
		return Optional.ofNullable(result);
	}

}
