package fr.ifremer.viewer3d.deprecated;

import java.util.Collections;
import java.util.List;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.utils.exception.GIOException;

public class BridgeFileInfo extends BasicFileInfo {

	IDataDriver driver;

	public BridgeFileInfo(String path, ContentType contentType, IDataDriver driver) {
		super(path, contentType);
		this.driver = driver;
	}

	@Override
	public List<Property<?>> getProperties() {
		try {
			return driver.getProperties(path);
		} catch (GIOException e) {
			return Collections.emptyList();
		}
	}

	/**
	 * @return the {@link #driver}
	 */
	public IDataDriver getDriver() {
		return driver;
	}
}