/**
 * 
 */
package fr.ifremer.viewer3d.deprecated;

/**
 * Handles error messages.
 * 
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */
public interface IErrorHandler {

	public void setErrorMessage(String msg);
}
