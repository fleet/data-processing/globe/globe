/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.elevation;

import gov.nasa.worldwind.geom.Sector;

/**
 * An area around which the elevations are flattened
 */
public class ExtensionArea {

	/** Flattening area */
	private final Sector sector;

	/** % of extra space around the sector */
	private final float expandingPercent;

	/**
	 * Constructor
	 */
	public ExtensionArea(Sector sector, float expandingPercent) {
		this.sector = sector;
		this.expandingPercent = expandingPercent;
	}

	/**
	 * @return the {@link #sector}
	 */
	public Sector sector() {
		return sector;
	}

	/**
	 * @return the {@link #expandingPercent}
	 */
	public float expandingPercent() {
		return expandingPercent;
	}

}
