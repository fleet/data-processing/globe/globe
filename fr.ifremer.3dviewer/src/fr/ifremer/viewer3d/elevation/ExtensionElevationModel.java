/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.elevation;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.ui.service.geographicview.ExtensionParameters;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.terrain.AbstractElevationModel;

/**
 * An ElevationModel that extends the elevations of an other ElevationModel
 */
public class ExtensionElevationModel extends AbstractElevationModel {

	/** Targeted Elevation Model */
	private final ElevationModel targetElevationModel;
	/** Current parameters */
	private ExtensionParameters extensionParameters;

	/** Area affected by extension */
	private Sector extensionSector;
	private boolean spanning180 = false;

	/** Remember the last productid grid */
	private Optional<Grid> lastProductedGrid = Optional.empty();

	/**
	 * Constructor
	 */
	public ExtensionElevationModel(ElevationModel anElevationModel, ExtensionParameters extensionParameters) {
		targetElevationModel = anElevationModel;
		setName("Extension_" + anElevationModel.getName());
		fitElevationParameters(extensionParameters);
	}

	/** Apply the expanding percent to the original sector */
	public void fitElevationParameters(ExtensionParameters extensionParameters) {
		this.extensionParameters = extensionParameters;
		setEnabled(this.extensionParameters.extensionEnabled());
		var originalSector = getOriginalSector();
		extensionSector = GeoBoxUtils.adaptIfSpanning180Th(originalSector);
		if (extensionParameters.extensionEnabled() && extensionParameters.extensionPercent() > 0f) {
			double width = extensionSector.getDeltaLonDegrees();
			width += width * (extensionParameters.extensionPercent() / 100f);
			double height = extensionSector.getDeltaLatDegrees();
			height += height * (extensionParameters.extensionPercent() / 100f);
			var center = extensionSector.getCentroid();
			extensionSector = Sector.fromDegrees( //
					center.latitude.degrees - height / 2d, //
					center.latitude.degrees + height / 2d, //
					center.longitude.degrees - width / 2d, //
					center.longitude.degrees + width / 2d//
			);
		}
		spanning180 = extensionSector.getMaxLongitude().degrees > 180d;
	}

	/** ElevationModel origin area affected by extension */
	private Sector getOriginalSector() {
		return (Sector) targetElevationModel.getValue(AVKey.SECTOR);
	}

	/** {@inheritDoc} */
	@Override
	public double getMaxElevation() {
		return targetElevationModel.getMaxElevation();
	}

	/** {@inheritDoc} */
	@Override
	public double getMinElevation() {
		return targetElevationModel.getMinElevation();
	}

	/** {@inheritDoc} */
	@Override
	public double[] getExtremeElevations(Angle latitude, Angle longitude) {
		return targetElevationModel.getExtremeElevations(latitude, longitude);
	}

	/** {@inheritDoc} */
	@Override
	public double[] getExtremeElevations(Sector sector) {
		return targetElevationModel.getExtremeElevations(sector);
	}

	/** {@inheritDoc} */
	@Override
	public double getElevations(Sector sector, List<? extends LatLon> latlons, double targetResolution,
			double[] buffer) {
		return doGetElevations(sector, latlons, targetResolution, buffer, true);
	}

	/** {@inheritDoc} */
	@Override
	public double getUnmappedElevations(Sector sector, List<? extends LatLon> latlons, double targetResolution,
			double[] buffer) {
		return doGetElevations(sector, latlons, targetResolution, buffer, false);
	}

	/** {@inheritDoc} */
	@Override
	public int intersects(Sector sector) {
		sector = adaptsSector(sector);
		// 0 if the elevation model fully contains the sector
		if (extensionSector.contains(sector)) {
			return 0;
		}

		return extensionSector.intersects(sector) ? //
				1 : // the elevation model intersects the sector but does not fully contain it
				-1; // the sector does not intersect the elevation model.
	}

	/** {@inheritDoc} */
	@Override
	public boolean contains(Angle latitude, Angle longitude) {
		longitude = longitude.degrees < 0d && extensionSector.getMaxLongitude().degrees > 180d
				? Angle.POS360.add(longitude)
				: longitude;
		return extensionSector.contains(latitude, longitude);
	}

	/** {@inheritDoc} */
	@Override
	public double getBestResolution(Sector sector) {
		return targetElevationModel.getBestResolution(sector);
	}

	/**
	 * Used when picking.
	 *
	 * @return the elevation from the last produced grid if any
	 */
	@Override
	public double getUnmappedElevation(Angle latitude, Angle longitude) {
		double result = getMissingDataSignal();
		if (lastProductedGrid.isPresent()) {
			var grid = lastProductedGrid.get();
			result = grid.getElevation(latitude, longitude);
			result = result == grid.noElevationValue ? getMissingDataSignal() : result;
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public void setExtremesCachingEnabled(boolean enabled) {
		// caching not allowed
	}

	/** {@inheritDoc} */
	@Override
	public boolean isExtremesCachingEnabled() {
		return false;
	}

	/** {@inheritDoc} */
	@Override
	public double getMissingDataSignal() {
		return targetElevationModel.getMissingDataSignal();
	}

	/** {@inheritDoc} */
	@Override
	public double getMissingDataReplacement() {
		return targetElevationModel.getMissingDataReplacement();
	}

	/**
	 * @return the {@link #extensionParameters}
	 */
	public ExtensionParameters getElevationParameters() {
		return extensionParameters;
	}

	/**
	 * Returns the elevations of a collection of locations <br>
	 * When mapMissingData is true, replaces any elevation values corresponding to the missing data signal with the
	 * elevation model's missing data replacement value.
	 */
	protected double doGetElevations(Sector sector, List<? extends LatLon> latlons, double targetResolution,
			double[] buffer, boolean mapMissingData) {

		// Creates a grid covering the sector
		double noElevationValue = mapMissingData ? getMissingDataReplacement() : getMissingDataSignal();
		Grid grid = new Grid(sector, noElevationValue, targetResolution, targetElevationModel);
		grid.fillEmptyCells();
		lastProductedGrid = Optional.of(grid);

		// Fill buffer
		double minElevation = targetElevationModel.getMinElevation();
		if (minElevation == 0d) {
			minElevation = buffer[0];
		}
		updatesBuffer(latlons, buffer, grid, minElevation);

		return targetResolution;
	}

	/**
	 * Uptade the elevation in the buffer at specified latlons.
	 */
	protected void updatesBuffer(List<? extends LatLon> latlons, double[] buffer, Grid grid, double defaultElevation) {
		// Process all positions in remainingMappedCell
		for (int i = 0, size = latlons.size(); i < size; i++) {
			LatLon latlon = latlons.get(i);
			if (contains(latlon.latitude, latlon.longitude)) {
				double elevation = grid.getElevation(latlon.latitude, latlon.longitude);
				if (elevation != grid.noElevationValue) {
					// Elevation defined for this cell
					buffer[i] = elevation;
				} else {
					buffer[i] = defaultElevation;
				}
			}
		}
	}

	/** Grid of elevations over a sector and a specific resolution */
	static class Grid {

		/** Original sector */
		public final Sector sector;

		/** Value of elevation when cell is empty */
		public final double noElevationValue;
		/** Width and height of the grid */
		public final int gridSize;
		/** Cell's width in degrees */
		public final double cellSize;

		/** The grid values */
		public final double[][] elevations;

		/**
		 * Constructor
		 *
		 * @param targetResolution is equals to sector.getDeltaLatRadians() / density
		 */
		public Grid(Sector sector, double noElevationValue, double targetResolution,
				ElevationModel referenceElevationModel) {
			this.sector = sector;
			this.noElevationValue = noElevationValue;

			gridSize = (int) Math.round(sector.getDeltaLatRadians() / targetResolution) + 1;
			cellSize = sector.getDeltaLatDegrees() / (gridSize - 1);
			elevations = new double[gridSize][gridSize];
			Angle longitude = sector.getMinLongitude();
			for (int cellX = 0; cellX < gridSize; cellX++) {
				Angle latitude = sector.getMinLatitude();
				for (int cellY = 0; cellY < gridSize; cellY++) {
					double elevation = referenceElevationModel.getUnmappedElevation(latitude, longitude);
					if (elevation != referenceElevationModel.getMissingDataSignal()) {
						elevations[cellX][cellY] = elevation;
					} else {
						elevations[cellX][cellY] = noElevationValue;
					}
					latitude = latitude.addDegrees(cellSize);
				}
				longitude = longitude.addDegrees(cellSize);
			}
		}

		/**
		 * Fill the empty cell with the mean elevation of neighboring cells
		 */
		protected void fillEmptyCells() {
			for (int i = 0; i < 10; i++) {
				for (int cellX = 0; cellX < gridSize; cellX++) {
					for (int cellY = 0; cellY < gridSize; cellY++) {
						if (elevations[cellX][cellY] == noElevationValue) {
							double elevation = computeMeanElevation(cellX, cellY);
							if (elevation != noElevationValue) {
								elevations[cellX][cellY] = elevation;
							}
						}
					}
				}
			}
		}

		/**
		 * @return the mean elevation for a cell.
		 */
		public double computeMeanElevation(int x, int y) {
			// Look in neighboring cells
			int minX = Math.max(x - 1, 0);
			int maxX = Math.min(x + 2, gridSize);
			int minY = Math.max(y - 1, 0);
			int maxY = Math.min(y + 2, gridSize);

			int count = 0;
			double result = 0d;
			for (x = minX; x < maxX; x++) {
				for (y = minY; y < maxY; y++) {
					if (elevations[x][y] != noElevationValue) {
						result += elevations[x][y];
						count++;
					}
				}
			}
			return count > 1 ? result / count : noElevationValue;
		}

		/**
		 * @return the elevation of a position. noElevationValue if position is out of the grid
		 */
		public double getElevation(Angle latitude, Angle longitude) {
			int x = (int) Math.round((longitude.degrees - sector.getMinLongitude().degrees) / cellSize);
			int y = (int) Math.round((latitude.degrees - sector.getMinLatitude().degrees) / cellSize);
			if (x >= 0 && x < gridSize && y >= 0 && y < gridSize) {
				return elevations[x][y];
			}
			return noElevationValue;
		}

	}

	/** {@inheritDoc} */
	@Override
	public boolean isEnabled() {
		return super.isEnabled() && targetElevationModel.isEnabled();
	}

	private Sector adaptsSector(Sector sector) {
		if (spanning180) {
			// When spanning 180, extensionSector have longitude between [0, 360]
			var minLongitude = sector.getMinLongitude();
			minLongitude = minLongitude.degrees < 0d ? Angle.POS360.add(minLongitude) : minLongitude;
			var maxLongitude = sector.getMaxLongitude();
			maxLongitude = maxLongitude.degrees < 0d ? Angle.POS360.add(maxLongitude) : maxLongitude;
			sector = new Sector(sector.getMinLatitude(), sector.getMaxLatitude(), minLongitude, maxLongitude);
		}
		return sector;
	}

}
