/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.elevation;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import fr.ifremer.globe.ui.service.geographicview.ExtensionParameters;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.terrain.CompoundElevationModel;

/**
 *
 * Stack of elevation models. Priority is given to layers :
 * <ul>
 * <li>models created by globe (DTM, Swath Editor...)</li>
 * <li>a model that extend the elevations for a given sector</li>
 * <li>the default elevation built by Nasa WW</li>
 * </ul>
 */
public class RootElevationModel extends CompoundElevationModel {

	/**
	 * Primitive elevation model created by WW (using configuration {@link AVKey#EARTH_ELEVATION_MODEL_CONFIG_FILE})<br>
	 * This is the last chance layer to find an elevation
	 */
	private final ElevationModel wwElevationModel;

	/**
	 * All ElevationModel that extend other ElevationModel
	 */
	private final CompoundElevationModel compoundExtensionElevationModel;

	/** Registry of ExtensionElevationModel associated to their ElevationModel */
	private final Map<ElevationModel, ExtensionElevationModel> extensionElevationModels = new HashMap<>();

	/**
	 * Constructor
	 */
	public RootElevationModel(ElevationModel wwElevationModel) {
		this.wwElevationModel = wwElevationModel;
		compoundExtensionElevationModel = new CompoundElevationModel();

		setName(RootElevationModel.class.getName());

		// use super.addElevationModel to avoid adding ExtensionElevationModel
		super.addElevationModel(this.wwElevationModel);
		super.addElevationModel(compoundExtensionElevationModel);
	}

	/** Returns true if this model contains the specified ElevationModel or an ElevationModel with the same name */
	@Override
	public boolean containsElevationModel(ElevationModel anElevationModel) {
		boolean result = super.containsElevationModel(anElevationModel);
		if (!result) {
			// Checks that no model with the same name exists
			result = getElevationModels().stream()
					.anyMatch(em -> Objects.equals(em.getName(), anElevationModel.getName()));
		}
		return result;
	}

	/**
	 * Adds an elevation to this compound elevation model<br>
	 * Avoids adding an ElevationModel twice<br>
	 * This method is redefined to associate a ExtensionElevationModel to any new ElevationModel
	 */
	@Override
	public void addElevationModel(ElevationModel anElevationModel) {
		// Sanity check
		if (anElevationModel == null) {
			return;
		}
		// Checks that the elevation model has not already been added
		if (containsElevationModel(anElevationModel)) {
			return;
		}

		super.addElevationModel(anElevationModel);
		manageExtensionModel(anElevationModel);
	}

	/** {@inheritDoc} */
	@Override
	public void addElevationModel(int index, ElevationModel anElevationModel) {
		// Sanity check
		if (anElevationModel == null) {
			return;
		}
		// Checks that the elevation model has not already been added
		if (containsElevationModel(anElevationModel)) {
			return;
		}

		super.addElevationModel(index, anElevationModel);
		manageExtensionModel(anElevationModel);
	}

	/** {@inheritDoc} */
	@Override
	public void removeElevationModel(ElevationModel anElevationModel) {
		// Sanity check
		if (anElevationModel == null) {
			return;
		}
		super.removeElevationModel(anElevationModel);
		var extensionElevationModel = extensionElevationModels.get(anElevationModel);
		if (extensionElevationModel != null) {
			compoundExtensionElevationModel.removeElevationModel(extensionElevationModel);
		}
	}

	/**
	 * Adds a ExtensionElevationModel to extend area around the specified ElevationModel
	 */
	protected void manageExtensionModel(ElevationModel anElevationModel) {
		// Only ElevationModel model with a defined sector can be extended
		if (anElevationModel.getValue(AVKey.SECTOR) != null) {
			ExtensionParameters extensionParameters = new ExtensionParameters();
			ExtensionElevationModel extensionElevationModel = new ExtensionElevationModel(anElevationModel,
					extensionParameters);
			extensionElevationModels.put(anElevationModel, extensionElevationModel);
			compoundExtensionElevationModel.addElevationModel(extensionElevationModel);
		}
	}

	/** Provides the settings currently applied to the elevation model */
	public Optional<ExtensionParameters> getElevationParameters(ElevationModel elevationModel) {
		var extensionElevationModel = extensionElevationModels.get(elevationModel);
		return Optional
				.ofNullable(extensionElevationModel != null ? extensionElevationModel.getElevationParameters() : null);
	}

	/** Changes the settings of the elevation model */
	public void applyElevationParameters(ElevationModel elevationModel, ExtensionParameters extensionParameters) {
		var extensionElevationModel = extensionElevationModels.get(elevationModel);
		if (extensionElevationModel != null) {
			extensionElevationModel.fitElevationParameters(extensionParameters);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected void sortElevationModels() {
		// This feature was introduced in WW 2.1.0
		// Allow to get elevations from less to most detailed model
		// Unfortunately, default WW elevation model (cached in Earth/EarthElevationModel.bil16)
		// is very detailed (11 levels) and may overwrite other model as DTM's one.
	}

}
