package fr.ifremer.viewer3d.multidata;

import java.io.IOException;

import fr.ifremer.viewer3d.data.MyByteBufferRaster;
import fr.ifremer.viewer3d.formats.tiff.BaselineTiff;
import fr.ifremer.viewer3d.formats.tiff.GeoTiff;
import fr.ifremer.viewer3d.formats.tiff.GeotiffMultiReader;
import fr.ifremer.viewer3d.formats.tiff.GeotiffReader;
import fr.ifremer.viewer3d.formats.tiff.Tiff;
import fr.ifremer.viewer3d.formats.tiff.TiffIFDEntry;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.Logging;

/***
 * Decodes Geotiff as elevation
 * 
 * @see GeotiffReader
 * @see GeotiffMultiElevationRasterReader
 * @author MORVAN
 * 
 */
public class GeotiffMultiElevationReader extends GeotiffMultiReader {

	public GeotiffMultiElevationReader(String sourceFilename) throws IOException {
		super(sourceFilename);
	}

	// protected TextureData textureData;

	/**
	 * Returns true if georeferencing information was found in this file.
	 * <p/>
	 * Note: see getGeoKeys() for determining if projection information is
	 * contained in the file.
	 * 
	 * @return Returns true if the file contains georeferencing information;
	 *         false otherwise.
	 * 
	 * @throws java.io.IOException
	 *             if data type is not supported or unknown
	 */

	@Override
	protected void repackageGeoReferencingTags() throws IOException {
		for (int i = 0; i < this.getNumImages(); i++) {
			TiffIFDEntry[] ifd = tiffIFDs.get(i);
			AVList values = this.metadata.get(i);

			values.setValue(AVKey.FILE_NAME, this.path);
			// after we read all data, we have evrything as BIG_ENDIAN
			values.setValue(AVKey.BYTE_ORDER, AVKey.BIG_ENDIAN);

			BaselineTiff tiff = BaselineTiff.extract(ifd, this.tiffReader);

			if (null == tiff) {
				String message = Logging.getMessage("GeotiffReader.BadGeotiff");
				Logging.logger().severe(message);
				throw new IOException(message);
			}

			if (tiff.width == Tiff.Undefined) {
				String message = Logging.getMessage("generic.InvalidWidth", tiff.width);
				Logging.logger().severe(message);
				throw new IOException(message);
			}
			values.setValue(AVKey.WIDTH, tiff.width);

			if (tiff.height == Tiff.Undefined) {
				String message = Logging.getMessage("generic.InvalidHeight", tiff.height);
				Logging.logger().severe(message);
				throw new IOException(message);
			}
			values.setValue(AVKey.HEIGHT, tiff.height);

			int sampleFormat = (null != tiff.sampleFormat) ? tiff.sampleFormat[0] : Tiff.Undefined;
			int bitsPerSample = (null != tiff.bitsPerSample) ? tiff.bitsPerSample[0] : Tiff.Undefined;

			if (null != tiff.displayName) {
				values.setValue(AVKey.DISPLAY_NAME, tiff.displayName);
			}

			if (null != tiff.imageDescription) {
				values.setValue(AVKey.DESCRIPTION, tiff.imageDescription);
			}

			if (null != tiff.softwareVersion) {
				values.setValue(AVKey.VERSION, tiff.softwareVersion);
			}

			if (null != tiff.dateTime) {
				values.setValue(AVKey.DATE_TIME, tiff.dateTime);
			}

			if (tiff.photometric == Tiff.Photometric.Color_RGB) {
				values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
				values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.COLOR);
				values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
			} else if (tiff.photometric == Tiff.Photometric.CMYK) {
				values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
				values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.COLOR);
				values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
			} else if (tiff.photometric == Tiff.Photometric.Color_Palette) {
				values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
				values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.COLOR);
				values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
			} else if (tiff.samplesPerPixel == Tiff.SamplesPerPixel.MONOCHROME) { // Tiff.Photometric.Grayscale_BlackIsZero
				// or
				// Tiff.Photometric.Grayscale_WhiteIsZero
				if (sampleFormat == Tiff.SampleFormat.SIGNED) {
					values.setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
					if (bitsPerSample == Short.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT16);
					} else if (bitsPerSample == Byte.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
					} else if (bitsPerSample == Integer.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT32);
					}
				} else if (sampleFormat == Tiff.SampleFormat.IEEEFLOAT) {
					values.setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
					if (bitsPerSample == Float.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
					}
				} else if (sampleFormat == Tiff.SampleFormat.UNSIGNED) {
					values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
					values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.GRAYSCALE);
					if (bitsPerSample == Short.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT16);
					} else if (bitsPerSample == Byte.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
					} else if (bitsPerSample == Integer.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT32);
					}
				}
			}

			if (!values.hasKey(AVKey.PIXEL_FORMAT) || !values.hasKey(AVKey.DATA_TYPE)) {
				String message = Logging.getMessage("Geotiff.UnsupportedDataTypeRaster", tiff.toString());
				Logging.logger().severe(message);
				// throw new IOException(message);
			}

			// geo keys
			for (TiffIFDEntry entry : ifd) {
				try {
					switch (entry.tag) {
					case GeoTiff.Tag.GDAL_NODATA:
						Double d = Double.parseDouble(this.tiffReader.readString(entry));
						values.setValue(AVKey.MISSING_DATA_SIGNAL, d);
						break;

					case Tiff.Tag.MIN_SAMPLE_VALUE:
						values.setValue(AVKey.ELEVATION_MIN, entry.getAsDouble());
						break;

					case Tiff.Tag.MAX_SAMPLE_VALUE:
						values.setValue(AVKey.ELEVATION_MAX, entry.getAsDouble());
						break;

					case GeoTiff.Tag.MODEL_PIXELSCALE:
						this.gc.setModelPixelScale(entry.getDoubles());
						break;

					case GeoTiff.Tag.MODEL_TIEPOINT:
						this.gc.addModelTiePoints(entry.getDoubles());
						break;

					case GeoTiff.Tag.MODEL_TRANSFORMATION:
						this.gc.setModelTransformation(entry.getDoubles());
						break;

					case GeoTiff.Tag.GEO_KEY_DIRECTORY:
						this.gc.setGeokeys(entry.getShorts());
						break;

					case GeoTiff.Tag.GEO_DOUBLE_PARAMS:
						this.gc.setDoubleParams(entry.getDoubles());
						break;

					case GeoTiff.Tag.GEO_ASCII_PARAMS:
						this.gc.setAsciiParams(this.tiffReader.readBytes(entry));
						break;
					}
				} catch (Exception e) {
					Logging.logger().finest(e.toString());
				}
			}

			this.processGeoKeys(i);
		}
	}

	@Override
	@SuppressWarnings("unused")
	public DataRaster doRead(int imageIndex) throws IOException {
		checkImageIndex(imageIndex);
		AVList values = this.metadata.get(imageIndex);

		// Extract the various IFD tags we need to read this image. We want to
		// loop over the tag set once, instead
		// multiple times if we simply used our general getByTag() method.

		long[] stripOffsets = null;
		byte[][] cmap = null;
		long[] stripCounts = null;

		boolean tiffDifferencing = false;

		TiffIFDEntry[] ifd = this.tiffIFDs.get(imageIndex);

		BaselineTiff tiff = BaselineTiff.extract(ifd, this.tiffReader);

		if (null == tiff) {
			String message = Logging.getMessage("GeotiffReader.BadGeotiff");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		if (tiff.width <= 0) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.width, "width", Tiff.Tag.IMAGE_WIDTH);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.height <= 0) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.height, "height", Tiff.Tag.IMAGE_LENGTH);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.samplesPerPixel <= Tiff.Undefined) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.samplesPerPixel, "samplesPerPixel", Tiff.Tag.SAMPLES_PER_PIXEL);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.photometric <= Tiff.Photometric.Undefined || tiff.photometric > Tiff.Photometric.YCbCr) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.photometric, "PhotoInterpretation", Tiff.Tag.PHOTO_INTERPRETATION);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.rowsPerStrip <= Tiff.Undefined) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.rowsPerStrip, "RowsPerStrip", Tiff.Tag.ROWS_PER_STRIP);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.planarConfig != Tiff.PlanarConfiguration.PLANAR && tiff.planarConfig != Tiff.PlanarConfiguration.CHUNKY) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.planarConfig, "PhotoInterpretation", Tiff.Tag.PHOTO_INTERPRETATION);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		for (TiffIFDEntry entry : ifd) {
			try {
				switch (entry.tag) {
				case Tiff.Tag.STRIP_OFFSETS:
					stripOffsets = entry.getAsLongs();
					break;

				case Tiff.Tag.STRIP_BYTE_COUNTS:
					stripCounts = entry.getAsLongs();
					break;

				case Tiff.Tag.COLORMAP:
					cmap = this.tiffReader.readColorMap(entry);
					break;
				}
			} catch (IOException e) {
				Logging.logger().finest(e.toString());
			}
		}

		if (null == stripOffsets || 0 == stripOffsets.length) {
			String message = Logging.getMessage("GeotiffReader.MissingRequiredTag", "StripOffsets");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		if (null == stripCounts || 0 == stripCounts.length) {
			String message = Logging.getMessage("GeotiffReader.MissingRequiredTag", "StripCounts");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		TiffIFDEntry notToday = getByTag(ifd, Tiff.Tag.COMPRESSION);
		boolean lzwCompressed = false;
		if (notToday != null && notToday.asLong() == Tiff.Compression.LZW) {
			lzwCompressed = true;
			TiffIFDEntry predictorEntry = getByTag(ifd, Tiff.Tag.TIFF_PREDICTOR);
			if ((predictorEntry != null) && (predictorEntry.asLong() != 0)) {
				tiffDifferencing = true;
			}
		} else if (notToday != null && notToday.asLong() != Tiff.Compression.NONE) {
			String message = Logging.getMessage("GeotiffReader.CompressionFormatNotSupported");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		notToday = getByTag(ifd, Tiff.Tag.TILE_WIDTH);
		if (notToday != null) {
			String message = Logging.getMessage("GeotiffReader.NoTiled");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		long offset = stripOffsets[0];
		// int sampleFormat = (null != tiff.sampleFormat) ? tiff.sampleFormat[0]
		// : Tiff.Undefined;
		// int bitsPerSample = (null != tiff.bitsPerSample) ?
		// tiff.bitsPerSample[0] : Tiff.Undefined;

		if (values.getValue(AVKey.PIXEL_FORMAT) == AVKey.ELEVATION) {
			double minSampleValue = Double.MIN_VALUE;
			double maxSampleValue = Double.MAX_VALUE;
			double nodataSignal = Short.MIN_VALUE;

			float[][] data = this.tiffReader.readPlanarFloat32(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

			int next = 0;
			// FloatBuffer buff = FloatBuffer.allocate(tiff.width*tiff.height);
			/*
			 * for (int y = 0; y < tiff.height; y++) { for (int x = 0; x <
			 * tiff.width; x++) { double value = data[0][next++]; //value =
			 * (value > maxSampleValue || value < minSampleValue) ? nodataSignal
			 * : value; //value = 0.0; raster.setDoubleAtPosition(y, x, value);
			 * //buff.put((float)value) ; } }
			 */

			MyByteBufferRaster raster = new MyByteBufferRaster(tiff.width, tiff.height, (Sector) values.getValue(AVKey.SECTOR), values);
			raster.write(data);

			if (raster.hasKey(AVKey.ELEVATION_MIN) && raster.hasKey(AVKey.ELEVATION_MAX)) {
				minSampleValue = (Double) raster.getValue(AVKey.ELEVATION_MIN);
				maxSampleValue = (Double) raster.getValue(AVKey.ELEVATION_MAX);

				if (raster.hasKey(AVKey.MISSING_DATA_SIGNAL)) {
					nodataSignal = (Double) raster.getValue(AVKey.MISSING_DATA_SIGNAL);
				} else {
					raster.setValue(AVKey.MISSING_DATA_SIGNAL, nodataSignal);
				}
			}
			raster.setTransparentValue(-32768.0);
			return raster;
		}

		String message = Logging.getMessage("Geotiff.UnsupportedDataTypeRaster", tiff.toString());
		Logging.logger().severe(message);
		throw new IOException(message);
	}

}
