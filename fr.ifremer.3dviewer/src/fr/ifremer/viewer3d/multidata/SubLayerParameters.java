package fr.ifremer.viewer3d.multidata;

import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import fr.ifremer.viewer3d.loaders.SubLayer;

/***
 * Parameters common to a {@link MultiTextureLayer}'s list of {@link SubLayer}
 * with the same type
 * 
 * @author MORVAN
 * 
 */
public class SubLayerParameters {
	// contrast
	protected double minTextureContrast;
	protected double maxTextureContrast;
	protected double minTransparency;
	protected double maxTransparency;

	// colormap
	protected String colormap;
	protected boolean inverseColorMap;
	protected String colormapori;

	// ombrage
	protected boolean useOmbrage;
	protected boolean useGradient;
	protected double azimuth;
	protected double zenith;
	protected double ombrageExaggeration = 1.0; // (0 in log10)

	// filter
	protected int filterName = 0;// gauss
	protected double filterHeight = 1.0;
	protected double filterWidth = 1.0;
	protected double sigmaGauss = 0.0;

	// texture
	protected boolean useOmbrageLogarithmique = false;

	public SubLayerParameters(String type) {
		if (type.equals("Reflectivity")) {
			setColorMap("GRAY");
		} else {
			setColorMap("GMTjet");
		}
	}

	/***
	 * level of contrast to retrieve after load session 0 : min max 1 : 0.5% 2 :
	 * 1% 3 : custom
	 */
	private Integer contrastLevel = 0;

	/***
	 * level of contrast to retrieve after load session 0 : min max 1 : 0.5% 2 :
	 * 1% 3 : custom
	 */
	public Integer getContrastLevel() {
		return contrastLevel;
	}

	/***
	 * level of contrast to retrieve after load session 0 : min max 1 : 0.5% 2 :
	 * 1% 3 : custom
	 */
	public void setContrastLevel(Integer contrastLevel) {
		this.contrastLevel = contrastLevel;
	}

	/***
	 * offset used to modify data displaying for comparison between layers
	 */
	protected double offset = 0.0;

	// contrast

	public void setMinContrast(double minContrast) {
		this.minTextureContrast = minContrast;
	}

	public double getMinContrast() {
		return this.minTextureContrast;
	}

	public void setMaxContrast(double maxContrast) {
		this.maxTextureContrast = maxContrast;
	}

	public double getMaxContrast() {
		return this.maxTextureContrast;
	}

	/**
	 * @return the colormap
	 */
	public String getColorMap() {
		return colormap;
	}

	/**
	 * @param colormap
	 *            the colormap to set
	 */
	public void setColorMap(String colormap) {
		this.colormap = colormap;
	}

	/**
	 * @return the colormapori
	 */
	public String getColorMapOri() {
		return colormapori;
	}

	/**
	 * @param colormapori
	 *            the colormapori to set
	 */
	public void setColorMapOri(String colormapori) {
		this.colormapori = colormapori;
	}

	// ombrage

	public void setUseOmbrage(boolean useOmbrage) {
		this.useOmbrage = useOmbrage;
	}

	public boolean isUseOmbrage() {
		return this.useOmbrage;
	}

	public void setAzimuth(double azimuth) {
		this.azimuth = azimuth;
	}

	public double getAzimuth() {
		return this.azimuth;
	}

	public void setZenith(double zenith) {
		this.zenith = zenith;
	}

	public double getZenith() {
		return this.zenith;
	}

	public void setOmbrageExaggeration(double exaggeration) {
		this.ombrageExaggeration = exaggeration;
	}

	public double getOmbrageExaggeration() {
		return this.ombrageExaggeration;
	}

	public void setInverseColorMap(boolean inverseColorMap) {
		this.inverseColorMap = inverseColorMap;
	}

	public boolean isInverseColorMap() {
		return this.inverseColorMap;
	}

	public void setUseGradient(boolean useGradient) {
		this.useGradient = useGradient;

	}

	public boolean getUseGradient() {
		return this.useGradient;

	}

	public void setUseOmbrageLogarithmique(boolean useOmbrageLogarithmique) {
		this.useOmbrageLogarithmique = useOmbrageLogarithmique;
	}

	public boolean getUseOmbrageLogarithmique() {
		return this.useOmbrageLogarithmique;
	}

	public void setSigmaGauss(double sigma) {
		this.sigmaGauss = sigma;
	}

	public double getSigmaGauss() {
		return sigmaGauss;
	}

	public int getFilterName() {
		return filterName;
	}

	public void setFilterName(int filterName) {
		this.filterName = filterName;
	}

	public double getFilterHeight() {
		return filterHeight;
	}

	public void setFilterHeight(double filterHeight) {
		this.filterHeight = filterHeight;
	}

	public double getFilterWidth() {
		return filterWidth;
	}

	public void setFilterWidth(double filterWidth) {
		this.filterWidth = filterWidth;
	}

	public double getOffset() {
		return offset;
	}

	public void setOffset(double offset) {
		this.offset = offset;
	}

	/***
	 * value min before transparency (kind of transparent contrast)
	 * 
	 * @param minTransparency
	 */
	public void setMinTransparency(double minTransparency) {
		this.minTransparency = minTransparency;

	}

	/***
	 * value min before transparency (kind of transparent contrast)
	 * 
	 * @return minTransparency
	 */
	public double getMinTransparency() {
		return this.minTransparency;
	}

	/***
	 * value max before transparency (kind of transparent contrast)
	 * 
	 * @param maxTransparency
	 */
	public void setMaxTransparency(double maxTransparency) {
		this.maxTransparency = maxTransparency;

	}

	/***
	 * value max before transparency (kind of transparent contrast)
	 * 
	 * @return maxTransparency
	 */
	public double getMaxTransparency() {
		return this.maxTransparency;
	}

}
