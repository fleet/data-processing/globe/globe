package fr.ifremer.viewer3d.multidata;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.viewer3d.data.MyCachedDataRaster;
import fr.ifremer.viewer3d.data.MyImageIORasterWriter;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTiledImageLayer;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.BasicMemoryCache;
import gov.nasa.worldwind.data.BufferWrapperRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.DataRasterReader;
import gov.nasa.worldwind.data.DataRasterWriter;
import gov.nasa.worldwind.data.TiledImageProducer;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.Tile;

/***
 * Texture cache generator for {@link MultiTextureLayer}
 * 
 * @see TiledImageProducer
 * @see GeotiffMultiTextureRasterReader
 * @author MORVAN
 * 
 */
public class TiledMultiTextureProducer extends TiledImageProducer {

	private static long CACHE_CAPACITY = 900000000;

	private Logger logger = LoggerFactory.getLogger(TiledMultiTextureProducer.class);

	private LevelSet levels;
	public static int DEFAULT_TILE_WIDTH_AND_HEIGHT = 512;
	protected static int DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT = 512;
	/** 3000 pixels. */
	public static final int DEFAULT_TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD = 3000;
	public static final double DEFAULT_LEVEL_ZERO_TILE_DELTA = 36d;

	private double valGlobaleMin = -20000.0;
	private double valGlobaleMax = 20000.0;
	private double valMin = 0.0;
	private double valMax = 0.0;
	private String unit = "m";
	private boolean recomputeCache = true;

	private static DataRasterReader[] readers = new DataRasterReader[] { new GeotiffMultiTextureRasterReader() };

	@Override
	protected DataRasterWriter[] getDataRasterWriters() {
		return new DataRasterWriter[] {
		// Configure the ImageIO writer to disable writing of georeference
		// files. Georeferencing files are
		// redundant for tiled images. The image format is defined in the data
		// configuration file, and each
		// tile's georeferencing information is implicit in the tile structure.
		new MyImageIORasterWriter(false) };
	}

	public TiledMultiTextureProducer(String unit, double valMin, double valMax, boolean recomputeCache) {
		// FIXME : Kludge to fix input files greater than default cache size
		// (which leads to out of memory exceptions)
		// 2 : DEFAULT_WRITE_THREAD_POOL_SIZE
		super(new BasicMemoryCache((long) (0.6 * CACHE_CAPACITY), CACHE_CAPACITY), 2);
		this.unit = unit;
		this.valGlobaleMin = valMin;
		this.valGlobaleMax = valMax;
		this.valMin = valMin;
		this.valMax = valMax;

		this.recomputeCache = recomputeCache;

		for (int i = 0; i < readers.length; i++) {
			if (readers[i] instanceof GeotiffMultiTextureRasterReader) {
				((GeotiffMultiTextureRasterReader) (readers[i])).setValGlobaleMin(valGlobaleMin);
				((GeotiffMultiTextureRasterReader) (readers[i])).setValGlobaleMax(valGlobaleMax);
				((GeotiffMultiTextureRasterReader) (readers[i])).setValMin(valMin);
				((GeotiffMultiTextureRasterReader) (readers[i])).setValMax(valMax);
			}
		}
	}

	@Override
	protected void assembleDataSource(Object source, AVList params) throws Exception {
		List<DataRaster> rasters;
		Iterable<DataRaster> dr = this.getDataRasters();
		if (dr instanceof List) {
			rasters = ((List<DataRaster>) dr);
		} else {
			throw new InvalidParameterException();
		}

		if (source instanceof DataRaster) {
			rasters.add((DataRaster) source);

		} else {
			// only geotiff could be read
			DataRasterReader reader = readers[0];
			rasters.add(new MyCachedDataRaster(source, params, reader, this.getCache()));
		}
	}

	@Override
	protected DataRaster drawDescendants(LevelSet levelSet, Tile tile, AVList params) throws java.io.IOException {
		DataRaster tileRaster = null;
		boolean hasDescendants = false;

		Iterable<DataRaster> rasters = this.getDataRasters();

		// Recursively create sub-tile rasters.
		Tile[] subTiles = this.createSubTiles(tile, levelSet.getLevel(tile.getLevelNumber() + 1));
		DataRaster[] subRasters = new DataRaster[subTiles.length];
		DataRaster[] data;
		MyCachedDataRaster cached;
		DataRaster subRaster;
		for (int index = 0; index < subTiles.length; index++) {
			// If the sub-tile does not intersect the level set, then skip that
			// sub-tile.
			if (subTiles[index].getSector().intersects(levelSet.getSector())) {
				boolean created = false;
				for (DataRaster r : rasters) {
					if (created) {
						break;
					}
					if (r instanceof MyCachedDataRaster) {
						cached = (MyCachedDataRaster) r;
						data = cached.getDataRasters();
						for (DataRaster d : data) {
							// Does the tile intersect at least one input file?
							if (subTiles[index].getSector().intersects(d.getSector())) {
								// Recursively create the sub-tile raster.
								subRaster = this.createTileRaster(levelSet, subTiles[index], params);
								// If creating the sub-tile raster fails, then
								// skip that sub-tile.
								if (subRaster != null) {
									subRasters[index] = subRaster;
									hasDescendants = true;
									created = true;
									break;
								}
							}
						}
					}
				}
			}
		}

		// Exit if the caller has instructed us to stop production.
		if (this.isStopped()) {
			return null;
		}

		// If any of the sub-tiles successfully created a data raster, then we
		// potentially create this tile's raster,
		// then write the sub-tiles to disk.
		if (hasDescendants) {
			// If this tile's level is not empty, then create and render the
			// tile's raster.
			if (!tile.getLevel().isEmpty()) {
				// Create the tile's raster.
				tileRaster = this.createDataRaster(tile.getLevel().getTileWidth(), tile.getLevel().getTileHeight(), tile.getSector(), params);

				for (int index = 0; index < subTiles.length; index++) {
					if (subRasters[index] != null) {
						// Render the sub-tile raster to this this tile raster.
						subRasters[index].drawOnTo(tileRaster);
						// Write the sub-tile raster to disk.
						this.installTileRasterLater(levelSet, subTiles[index], subRasters[index], params);
					}
				}
			}
		}

		// Make the sub-tiles and sub-rasters available for garbage collection.
		for (int index = 0; index < subTiles.length; index++) {
			subTiles[index] = null;
			subRasters[index] = null;
		}
		// noinspection UnusedAssignment
		subTiles = null;
		// noinspection UnusedAssignment
		subRasters = null;

		return tileRaster;
	}

	@Override
	protected DataRasterReader[] getDataRasterReaders() {
		return readers;
	}

	@Override
	protected void installLevelSet(LevelSet levelSet, AVList params) throws java.io.IOException {
		// Exit if the caller has instructed us to stop production.
		if (this.isStopped()) {
			return;
		}

		// Setup the progress parameters.
		this.calculateTileCount(levelSet, params);
		this.startProgress();

		Sector sector = levelSet.getSector();
		Level level = levelSet.getFirstLevel();

		Angle dLat = level.getTileDelta().getLatitude();
		Angle dLon = level.getTileDelta().getLongitude();
		Angle latOrigin = levelSet.getTileOrigin().getLatitude();
		Angle lonOrigin = levelSet.getTileOrigin().getLongitude();
		int firstRow = Tile.computeRow(dLat, sector.getMinLatitude(), latOrigin);
		int firstCol = Tile.computeColumn(dLon, sector.getMinLongitude(), lonOrigin);
		int lastRow = Tile.computeRow(dLat, sector.getMaxLatitude(), latOrigin);
		int lastCol = Tile.computeColumn(dLon, sector.getMaxLongitude(), lonOrigin);

		int row, col;
		Angle p2, t1, t2;
		Tile tile;
		DataRaster tileRaster;
		buildLoop: {
			Angle p1 = Tile.computeRowLatitude(firstRow, dLat, latOrigin);
			for (row = firstRow; row <= lastRow; row++) {
				p2 = p1.add(dLat);
				t1 = Tile.computeColumnLongitude(firstCol, dLon, lonOrigin);
				for (col = firstCol; col <= lastCol; col++) {
					// Exit if the caller has instructed us to stop production.
					Thread.yield();
					if (this.isStopped()) {
						break buildLoop;
					}

					t2 = t1.add(dLon);

					tile = new Tile(new Sector(p1, p2, t1, t2), level, row, col);
					tileRaster = this.createTileRaster(levelSet, tile, params);
					// Write the top-level tile raster to disk.
					if (tileRaster != null) {
						this.installTileRasterLater(levelSet, tile, tileRaster, params);
					}

					t1 = t2;
				}
				p1 = p2;
			}
		}
	}

	@Override
	protected DataRaster drawDataSources(LevelSet levelSet, Tile tile, Iterable<DataRaster> dataRasters, AVList params) throws java.io.IOException {
		DataRaster tileRaster = null;

		// Find the data sources that intersect this tile and intersect the
		// LevelSet sector.
		java.util.ArrayList<DataRaster> intersectingRasters = new java.util.ArrayList<DataRaster>();
		for (DataRaster raster : dataRasters) {
			if (raster.getSector().intersects(tile.getSector()) && raster.getSector().intersects(levelSet.getSector())) {
				intersectingRasters.add(raster);
			}
		}

		// If any data sources intersect this tile, and the tile's level is not
		// empty, then we attempt to read those
		// sources and render them into this tile.
		if (!intersectingRasters.isEmpty() && !tile.getLevel().isEmpty()) {
			// Create the tile raster to render into.
			tileRaster = this.createDataRaster(tile.getLevel().getTileWidth(), tile.getLevel().getTileHeight(), tile.getSector(), params);
			// Render each data source raster into the tile raster.
			for (DataRaster raster : intersectingRasters) {
				raster.drawOnTo(tileRaster);

				if (raster instanceof MyCachedDataRaster) {
					if (valMin == 0.0 && valMax == 0.0) {
						valMin = ((MyCachedDataRaster) raster).getValMin();
						valMax = ((MyCachedDataRaster) raster).getValMax();
					}
				}
			}
		}

		// Make the data rasters available for garbage collection.
		intersectingRasters.clear();
		// noinspection UnusedAssignment
		intersectingRasters = null;

		return tileRaster;
	}

	@Override
	protected void doStartProduction(AVList parameters) throws Exception {
		// Copy production parameters to prevent changes to caller's reference.
		this.productionParams = parameters.copy();
		this.initProductionParameters(this.productionParams);

		// Assemble the source data rasters.
		this.assembleDataRasters();

		// Initialize the level set parameters, and create the level set.
		this.initLevelSetParameters(this.productionParams);
		LevelSet levelSet = new LevelSet(this.productionParams);
		// Install the each tiles of the LevelSet.
		if (recomputeCache) {
			this.installLevelSet(levelSet, this.productionParams);
		}
		levelSet.setValues(productionParams);
		setLevels(levelSet);

		// Wait for concurrent tasks to complete.
		this.waitForInstallTileTasks();

		if (recomputeCache) {
			// Clear the raster cache.
			this.getCache().clear();
		}

		// Install the data descriptor for this tiled raster set.
		this.installConfigFile(this.productionParams);
	}

	@Override
	protected LatLon computeRasterPixelSize(DataRaster raster) {
		// Compute the raster's pixel dimension in latitude and longitude. In
		// this computation a pixel is assumed to
		// cover a finite area.
		return LatLon.fromDegrees(raster.getSector().getDeltaLatDegrees() / raster.getHeight(), raster.getSector().getDeltaLonDegrees() / raster.getWidth());
	}

	@Override
	protected LatLon computeSmallestPixelSize(Iterable<? extends DataRaster> rasters) {
		// Find the smallest pixel dimensions in the given rasters.
		double smallestLat = Double.MAX_VALUE;
		double smallestLon = Double.MAX_VALUE;
		for (DataRaster raster : rasters) {
			LatLon curSize = this.computeRasterPixelSize(raster);
			if (smallestLat > curSize.getLatitude().degrees) {
				smallestLat = curSize.getLatitude().degrees;
			}
			if (smallestLon > curSize.getLongitude().degrees) {
				smallestLon = curSize.getLongitude().degrees;
			}
		}
		return LatLon.fromDegrees(smallestLat, smallestLon);
	}

	@Override
	protected void initLevelSetParameters(AVList params) {
		int largeThreshold = Configuration.getIntegerValue(AVKey.TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD, DEFAULT_TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD);
		boolean isDataSetLarge = this.isDataSetLarge(getDataRasters(), largeThreshold);

		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		if (sector == null) {
			// Compute a sector that bounds the data rasters. Make sure the
			// sector does not exceed the limits of
			// latitude and longitude.
			sector = this.computeBoundingSector(getDataRasters());

			if (sector != null) {
				sector = sector.intersection(Sector.FULL_SPHERE);
			}
			params.setValue(AVKey.SECTOR, sector);
		}

		Integer tileWidth = (Integer) params.getValue(AVKey.TILE_WIDTH);
		if (tileWidth == null) {
			tileWidth = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_WIDTH, tileWidth);
		}

		Integer tileHeight = (Integer) params.getValue(AVKey.TILE_HEIGHT);
		if (tileHeight == null) {
			tileHeight = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_HEIGHT, tileHeight);
		}

		LatLon rasterTileDelta = this.computeRasterTileDelta(tileWidth, tileHeight, getDataRasters());
		LatLon desiredLevelZeroDelta = this.computeDesiredTileDelta(sector);

		Integer numLevels = (Integer) params.getValue(AVKey.NUM_LEVELS);
		if (numLevels == null) {
			// If the data set is large, then use compute a number of levels for
			// the full pyramid. Otherwise use a
			// single level.
			numLevels = this.computeNumLevels(desiredLevelZeroDelta, rasterTileDelta);
			params.setValue(AVKey.NUM_LEVELS, numLevels);
		}

		Integer numEmptyLevels = (Integer) params.getValue(AVKey.NUM_EMPTY_LEVELS);
		if (numEmptyLevels == null) {
			numEmptyLevels = 0;
			params.setValue(AVKey.NUM_EMPTY_LEVELS, numEmptyLevels);
		}

		LatLon levelZeroTileDelta = (LatLon) params.getValue(AVKey.LEVEL_ZERO_TILE_DELTA);
		if (levelZeroTileDelta == null) {
			double scale = Math.pow(2d, numLevels - 1);
			levelZeroTileDelta = LatLon.fromDegrees(scale * rasterTileDelta.getLatitude().degrees, scale * rasterTileDelta.getLongitude().degrees);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);
		}

		LatLon tileOrigin = (LatLon) params.getValue(AVKey.TILE_ORIGIN);
		if (tileOrigin == null) {
			tileOrigin = new LatLon(sector.getMinLatitude(), sector.getMinLongitude());
			params.setValue(AVKey.TILE_ORIGIN, tileOrigin);
		}

		// If the default or caller-specified values define a level set that
		// does not fit in the limits of latitude
		// and longitude, then we re-define the level set parameters using
		// values known to fit in those limits.
		/*
		 * if (!this.isWithinLatLonLimits(sector, levelZeroTileDelta,
		 * tileOrigin)) { String message =
		 * "TiledRasterProducer: native tiling is outside lat/lon limits. Falling back to default tiling."
		 * ; Logging.logger().warning(message);
		 * 
		 * levelZeroTileDelta = LatLon.fromDegrees(
		 * DEFAULT_LEVEL_ZERO_TILE_DELTA, DEFAULT_LEVEL_ZERO_TILE_DELTA);
		 * params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);
		 * 
		 * tileOrigin = new LatLon(Angle.NEG90, Angle.NEG180);
		 * params.setValue(AVKey.TILE_ORIGIN, tileOrigin);
		 * 
		 * numLevels = this.computeNumLevels(levelZeroTileDelta,
		 * rasterTileDelta); params.setValue(AVKey.NUM_LEVELS, numLevels);
		 * 
		 * numEmptyLevels = 0; params.setValue(AVKey.NUM_EMPTY_LEVELS,
		 * numEmptyLevels); }
		 */
	}

	@Override
	protected LatLon computeRasterTileDelta(int tileWidth, int tileHeight, Iterable<? extends DataRaster> rasters) {
		LatLon pixelSize = this.computeSmallestPixelSize(rasters);
		// Compute the tile size in latitude and longitude, given a raster's
		// sector and dimension, and the tile
		// dimensions. In this computation a pixel is assumed to cover a finite
		// area.
		double latDelta = tileHeight * pixelSize.getLatitude().degrees;
		double lonDelta = tileWidth * pixelSize.getLongitude().degrees;
		return LatLon.fromDegrees(latDelta, lonDelta);
	}

	/**
	 * Returns a Layer configuration document which describes the tiled imagery
	 * produced by this TiledImageProducer. The document's contents are based on
	 * the configuration document for a TiledImageLayer, except this document
	 * describes an offline dataset. This returns null if the parameter list is
	 * null, or if the configuration document cannot be created for any reason.
	 * 
	 * @param params
	 *            the parameters which describe a Layer configuration document's
	 *            contents.
	 * 
	 * @return the configuration document, or null if the parameter list is null
	 *         or does not contain the required parameters.
	 */
	@Override
	protected Document createConfigDoc(AVList params) {
		AVList configParams = params.copy();

		// Determine a default display name if none exists.
		if (configParams.getValue(AVKey.DISPLAY_NAME) == null) {
			configParams.setValue(AVKey.DISPLAY_NAME, params.getValue(AVKey.DATASET_NAME));
		}

		// Set the SERVICE_NAME and NETWORK_RETRIEVAL_ENABLED parameters to
		// indicate this dataset is offline.
		configParams.setValue(AVKey.SERVICE_NAME, "Offline");
		configParams.setValue(AVKey.NETWORK_RETRIEVAL_ENABLED, Boolean.FALSE);

		// Set the texture format to DDS. If the texture data is already in DDS
		// format, this parameter is benign.
		configParams.setValue(AVKey.TEXTURE_FORMAT, DEFAULT_TEXTURE_FORMAT);

		// Set the USE_MIP_MAPS, and USE_TRANSPARENT_TEXTURES parameters to
		// true. The imagery produced by
		// TiledImageProducer is best viewed with mipMapping enabled, and
		// texture transparency enabled. These parameters
		// tell the consumer of this imagery to enable these features during
		// rendering.
		configParams.setValue(AVKey.USE_MIP_MAPS, Boolean.TRUE);
		configParams.setValue(AVKey.USE_TRANSPARENT_TEXTURES, Boolean.TRUE);

		// Bathyscope contrast properties
		configParams.setValue(MyTiledImageLayer.XML_KEY_UNIT, unit);
		configParams.setValue(MyTiledImageLayer.XML_KEY_GlobalMinValue, Double.toString(valGlobaleMin));
		configParams.setValue(MyTiledImageLayer.XML_KEY_GlobalMaxValue, Double.toString(valGlobaleMax));
		configParams.setValue(MyTiledImageLayer.XML_KEY_MinValue, Double.toString(valMin));
		configParams.setValue(MyTiledImageLayer.XML_KEY_MaxValue, Double.toString(valMax));

		// Return a configuration file for a TiledImageLayer. TiledImageLayer is
		// the standard WWJ component which
		// consumes and renders tiled imagery.
		return MyTiledImageLayer.createTiledImageLayerConfigDocument(configParams);
	}

	@Override
	protected String validateDataSource(Object source, AVList params) {
		// TiledElevationProducer does not accept null data sources.
		if (source == null) {
			return Logging.getMessage("nullValue.SourceIsNull");
		}

		// TiledElevationProducer accepts BufferWrapperRaster as a data source.
		// If the data source is a DataRaster, then
		// check that it's a BufferWrapperRaster.
		if (source instanceof DataRaster) {
			DataRaster raster = (DataRaster) source;

			if (!(raster instanceof BufferWrapperRaster)) {
				return Logging.getMessage("TiledRasterProducer.UnrecognizedDataSource", raster);
			}

			String s = this.validateDataSourceParams(raster, String.valueOf(raster));
			if (s != null) {
				return s;
			}
		}
		// For any other data source, attempt to find a reader for the data
		// source. If the reader know's the data
		// source's raster type, then check that it's elevation data.
		else {
			// only geotiff can be read
			DataRasterReader reader = readers[0];
			if (reader == null) {
				return Logging.getMessage("TiledRasterProducer.UnrecognizedDataSource", source);
			}

			// Copy the parameter list to insulate changes from the caller.
			params = (params != null) ? params.copy() : new AVListImpl();

			try {
				reader.readMetadata(source, params);
			} catch (IOException e) {
				return Logging.getMessage("TiledRasterProducer.ExceptionWhileReading", source, e.getMessage());
			}

			String s = this.validateDataSourceParams(params, String.valueOf(source));
			if (s != null) {
				return s;
			}
		}

		return null;
	}

	public void setLevels(LevelSet levels) {
		this.levels = levels;
	}

	public LevelSet getLevels() {
		return levels;
	}

	public double getValGlobaleMin() {
		return valGlobaleMin;
	}

	public void setValGlobaleMin(double valGlobaleMin) {
		this.valGlobaleMin = valGlobaleMin;
	}

	public double getValGlobaleMax() {
		return valGlobaleMax;
	}

	public void setValGlobaleMax(double valGlobaleMax) {
		this.valGlobaleMax = valGlobaleMax;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getValMin() {
		return valMin;
	}

	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	public double getValMax() {
		return valMax;
	}

	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	// correction non generation du cache
	@Override
	protected void installTileRasterLater(final LevelSet levelSet, final Tile tile, final DataRaster tileRaster, final AVList params) {
		try {
			installTileRaster(tile, tileRaster, params);
			// Dispose the data raster.
			if (tileRaster instanceof Disposable) {
				((Disposable) tileRaster).dispose();
			}
		} catch (IOException e) {
			logger.error("Error during tile installation", e);
		}

	}

	public static long getCACHE_CAPACITY() {
		return CACHE_CAPACITY;
	}

	public static void setCACHE_CAPACITY(long cACHE_CAPACITY) {
		CACHE_CAPACITY = cACHE_CAPACITY;
	}

}
