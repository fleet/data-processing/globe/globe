package fr.ifremer.viewer3d.multidata;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.GeotiffRasterReader;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;

import java.io.File;

/***
 * Decodes Geotiffs of a given directory for texture cache
 * 
 * @see GeotiffRasterReader
 * @see GeotiffMultiTextureReader
 * @see TiledMultiTextureProducer
 * @author MORVAN
 * 
 */
public class GeotiffMultiTextureRasterReader extends GeotiffRasterReader {
	private double valGlobaleMin = -20000;
	private double valGlobaleMax = 20000;
	private double valMin = 0;
	private double valMax = 0;
	private double dateLineTrick = 0;

	public GeotiffMultiTextureRasterReader() {
		super();
	}

	@Override
	protected boolean doCanRead(Object source, AVList params) {
		// this raster reader is specific to TiledMultiTextureProducer
		// no need to rechecks
		return true;
	}

	@Override
	protected boolean canReadSuffix(Object source) {
		// If the source has no path, we cannot return failure, so return that
		// the test passed.
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			return true;
		}

		// If the source is a directory
		// return true is one of the XML binary file
		File file = new File(path);
		if (file.isDirectory()) {
			return true;
		}
		return false;
	}

	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("DataRaster.CannotRead", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		AVList metadata = new AVListImpl();
		if (null != params) {
			metadata.setValues(params);
		}

		GeotiffMultiTextureReader reader = null;
		DataRaster[] rasters = null;
		try {
			this.readMetadata(source, metadata);

			reader = new GeotiffMultiTextureReader(path, valGlobaleMin, valGlobaleMax);
			reader.copyMetadataTo(metadata);

			rasters = reader.readDataRaster();
			valMin = reader.getValMin();
			valMax = reader.getValMax();

			if (null != rasters) {
				String[] keysToCopy = new String[] { AVKey.SECTOR };
				for (DataRaster raster : rasters) {
					WWUtil.copyValues(metadata, raster, keysToCopy, false);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return rasters;
	}

	@Override
	protected void doReadMetadata(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("nullValue.PathIsNull", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		GeotiffMultiTextureReader reader = null;
		try {
			reader = new GeotiffMultiTextureReader(path, valGlobaleMin, valGlobaleMax);
			reader.copyMetadataTo(params);

			// date line trick
			dateLineTrick = reader.getDateLineTrick();

			boolean isGeoTiff = reader.isGeotiff(0);
			if (!isGeoTiff && params.hasKey(AVKey.WIDTH) && params.hasKey(AVKey.HEIGHT)) {
				int[] size = new int[2];

				size[0] = (Integer) params.getValue(AVKey.WIDTH);
				size[1] = (Integer) params.getValue(AVKey.HEIGHT);

				params.setValue(WorldFile.WORLD_FILE_IMAGE_SIZE, size);

				WorldFile.readWorldFiles(source, params);

				Object o = params.getValue(AVKey.SECTOR);
				if (o == null || !(o instanceof Sector)) {
					ImageUtil.calcBoundingBoxForUTM(params);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	/*
	 * protected DataRaster[] doRead(Object source, AVList params) throws
	 * java.io.IOException { String path = WWIO.getSourcePath(source); if (path
	 * == null) { String message = Logging.getMessage("DataRaster.CannotRead",
	 * source); Logging.logger().severe(message); throw new
	 * java.io.IOException(message); }
	 * 
	 * GeotiffReader reader = null;
	 * 
	 * FileSeekableStream stream = null; try { stream = new
	 * FileSeekableStream(new File(path)); } catch (IOException e) {
	 * e.printStackTrace(); System.exit(0); } ParameterBlock param = new
	 * ParameterBlock(); param.add(stream);
	 * 
	 * TIFFDecodeParam decodeParam = new TIFFDecodeParam();
	 * decodeParam.setDecodePaletteAsShorts(true);
	 * 
	 * RenderedOp image1 = JAI.create("tiff", param);
	 * java.awt.image.BufferedImage image = image1.getAsBufferedImage();
	 * 
	 * //reader = new GeotiffReader(path); //java.awt.image.BufferedImage image
	 * = reader.read(); textureData = TextureIO.newTextureData(image, true);
	 * image = ImageUtil.toCompatibleImage(image);
	 * 
	 * ByteBuffer buff = convertImageData(image); DataRaster[] rasters = {new
	 * ByteBufferRaster(image.getWidth(), image.getHeight(),
	 * (Sector)params.getValue(AVKey.SECTOR), buff, params)};
	 * 
	 * 
	 * return rasters; }
	 */

	// /**
	// * Convert the buffered image to a texture
	// *
	// * @param bufferedImage
	// * The image to convert to a texture
	// * @param texture
	// * The texture to store the data into
	// * @return A buffer containing the data
	// */
	// private ByteBuffer convertImageData(BufferedImage bufferedImage) {
	// ByteBuffer imageBuffer = null;
	//
	// if (bufferedImage.getData() != null) {
	// Raster raster = bufferedImage.getData();
	// if (raster.getDataBuffer() != null) {
	// DataBuffer buffer = raster.getDataBuffer();
	// int[] data = ((DataBufferInt) buffer).getData();
	// imageBuffer = ByteBuffer.allocateDirect(4 * data.length);
	// imageBuffer.order(ByteOrder.nativeOrder());
	// for (int i = 0; i < data.length; i++) {
	// imageBuffer.putInt(data[i]);
	// }
	// imageBuffer.flip();
	// }
	// }
	//
	// return imageBuffer;
	// }

	public double getValGlobaleMin() {
		return valGlobaleMin;
	}

	public void setValGlobaleMin(double valGlobaleMin) {
		this.valGlobaleMin = valGlobaleMin;
	}

	public double getValGlobaleMax() {
		return valGlobaleMax;
	}

	public void setValGlobaleMax(double valGlobaleMax) {
		this.valGlobaleMax = valGlobaleMax;
	}

	public double getValMin() {
		return valMin;
	}

	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	public double getValMax() {
		return valMax;
	}

	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	public double getDateLineTrick() {
		return dateLineTrick;
	}

	public void setDateLineTrick(double dateLineTrick) {
		this.dateLineTrick = dateLineTrick;
	}
}
