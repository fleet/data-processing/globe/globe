package fr.ifremer.viewer3d.multidata;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.GeotiffRasterReader;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;

import java.io.File;

/***
 * Decodes Geotiffs of a given directory for elevation cache
 * 
 * @see GeotiffRasterReader
 * @see GeotiffMultiElevationReader
 * @see TiledMultiElevationProducer
 * @author MORVAN
 * 
 */
public class GeotiffMultiElevationRasterReader extends GeotiffRasterReader {

	public GeotiffMultiElevationRasterReader() {
		super();
	}

	@Override
	protected boolean doCanRead(Object source, AVList params) {
		// this raster reader is specific to TiledMultiElevationProducer
		// no need to rechecks
		return true;
	}

	@Override
	protected boolean canReadSuffix(Object source) {
		// If the source has no path, we cannot return failure, so return that
		// the test passed.
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			return true;
		}

		// If the source is a directory
		// return true is one of the XML binary file
		File file = new File(path);
		if (file.isDirectory()) {
			return true;
		}
		return false;
	}

	@Override
	protected void doReadMetadata(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("nullValue.PathIsNull", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		GeotiffMultiElevationReader reader = null;
		try {
			reader = new GeotiffMultiElevationReader(path);
			reader.copyMetadataTo(params);

			boolean isGeoTiff = reader.isGeotiff(0);
			if (!isGeoTiff && params.hasKey(AVKey.WIDTH) && params.hasKey(AVKey.HEIGHT)) {
				int[] size = new int[2];

				size[0] = (Integer) params.getValue(AVKey.WIDTH);
				size[1] = (Integer) params.getValue(AVKey.HEIGHT);

				params.setValue(WorldFile.WORLD_FILE_IMAGE_SIZE, size);

				WorldFile.readWorldFiles(source, params);

				Object o = params.getValue(AVKey.SECTOR);
				if (o == null || !(o instanceof Sector)) {
					ImageUtil.calcBoundingBoxForUTM(params);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("DataRaster.CannotRead", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		AVList metadata = new AVListImpl();
		if (null != params) {
			metadata.setValues(params);
		}

		GeotiffMultiElevationReader reader = null;
		DataRaster[] rasters = null;
		try {
			this.readMetadata(source, metadata);

			reader = new GeotiffMultiElevationReader(path);
			reader.copyMetadataTo(metadata);

			rasters = reader.readDataRaster();

			if (null != rasters) {
				String[] keysToCopy = new String[] { AVKey.SECTOR };
				for (DataRaster raster : rasters) {
					WWUtil.copyValues(metadata, raster, keysToCopy, false);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return rasters;
	}
}
