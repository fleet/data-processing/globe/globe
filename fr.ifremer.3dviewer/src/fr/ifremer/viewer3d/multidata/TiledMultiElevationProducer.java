package fr.ifremer.viewer3d.multidata;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.viewer3d.data.MyCachedDataRaster;
import fr.ifremer.viewer3d.data.MyReadableDataRaster;
import fr.ifremer.viewer3d.data.MyTiledImageProducer;
import fr.ifremer.viewer3d.terrain.MultiElevationModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.BasicMemoryCache;
import gov.nasa.worldwind.data.BufferWrapperRaster;
import gov.nasa.worldwind.data.ByteBufferRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.DataRasterReader;
import gov.nasa.worldwind.data.TiledElevationProducer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.Tile;

/***
 * Elevation cache generator for {@link MultiElevationModel}
 * 
 * @see TiledElevationProducer
 * @see GeotiffMultiElevationRasterReader
 * @author MORVAN
 * 
 */
public class TiledMultiElevationProducer extends TiledElevationProducer {

	private static long CACHE_CAPACITY = 900000000;

	private Logger logger = LoggerFactory.getLogger(TiledMultiElevationProducer.class);

	private LevelSet levels;
	private List<MyReadableDataRaster> intersectingRasters;
	private Integer numLevels;
	private boolean recomputeCache = true;

	protected static final int DEFAULT_TILE_WIDTH_AND_HEIGHT = 512;
	protected static final int DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT = 512;

	private static DataRasterReader[] readers = new DataRasterReader[] { new GeotiffMultiElevationRasterReader() };

	public TiledMultiElevationProducer(boolean recomputeCache) {
		// FIXME : Kludge to fix input files greater than default cache size
		// (which leads to out of memory exceptions)
		// 2 : DEFAULT_WRITE_THREAD_POOL_SIZE
		super(new BasicMemoryCache((long) (0.6 * CACHE_CAPACITY), CACHE_CAPACITY), 2);
		this.recomputeCache = recomputeCache;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void initLevelSetParameters(AVList params) {
		int largeThreshold = Configuration.getIntegerValue(AVKey.TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD, MyTiledImageProducer.DEFAULT_TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD);
		boolean isDataSetLarge = this.isDataSetLarge(getDataRasters(), largeThreshold);

		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		if (sector == null) {
			// Compute a sector that bounds the data rasters. Make sure the
			// sector does not exceed the limits of
			// latitude and longitude.
			sector = this.computeBoundingSector(getDataRasters());
			if (sector != null) {
				sector = sector.intersection(Sector.FULL_SPHERE);
			}
			params.setValue(AVKey.SECTOR, sector);
		}

		Integer tileWidth = (Integer) params.getValue(AVKey.TILE_WIDTH);
		if (tileWidth == null) {
			tileWidth = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_WIDTH, tileWidth);
		}

		Integer tileHeight = (Integer) params.getValue(AVKey.TILE_HEIGHT);
		if (tileHeight == null) {
			tileHeight = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_HEIGHT, tileHeight);
		}

		LatLon rasterTileDelta = this.computeRasterTileDelta(tileWidth, tileHeight, getDataRasters());
		LatLon desiredLevelZeroDelta = this.computeDesiredTileDelta(sector);

		numLevels = (Integer) params.getValue(AVKey.NUM_LEVELS);
		if (numLevels == null) {
			// If the data set is large, then use compute a number of levels for
			// the full pyramid. Otherwise use a
			// single level.
			numLevels = this.computeNumLevels(desiredLevelZeroDelta, rasterTileDelta);
			params.setValue(AVKey.NUM_LEVELS, numLevels);
		}

		Integer numEmptyLevels = (Integer) params.getValue(AVKey.NUM_EMPTY_LEVELS);
		if (numEmptyLevels == null) {
			numEmptyLevels = 0;
			params.setValue(AVKey.NUM_EMPTY_LEVELS, numEmptyLevels);
		}

		LatLon levelZeroTileDelta = (LatLon) params.getValue(AVKey.LEVEL_ZERO_TILE_DELTA);
		if (levelZeroTileDelta == null) {
			double scale = Math.pow(2d, numLevels - 1);
			levelZeroTileDelta = LatLon.fromDegrees(scale * rasterTileDelta.getLatitude().degrees, scale * rasterTileDelta.getLongitude().degrees);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);
		}

		LatLon tileOrigin = (LatLon) params.getValue(AVKey.TILE_ORIGIN);
		if (tileOrigin == null) {
			tileOrigin = new LatLon(sector.getMinLatitude(), sector.getMinLongitude());
			params.setValue(AVKey.TILE_ORIGIN, tileOrigin);
		}

		params.setValue(AVKey.BYTE_ORDER, AVKey.LITTLE_ENDIAN);
		params.setValue(AVKey.DETAIL_HINT, 0.0);
		params.setValue(AVKey.ELEVATION_MIN, -11000.0);
		params.setValue(AVKey.ELEVATION_MAX, 9000.0);
		params.setValue(AVKey.PIXEL_TYPE, AVKey.FLOAT32);
		params.setValue(AVKey.FORMAT_SUFFIX, ".bil"); //$NON-NLS-1$

		if (logger.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append(sector.toString());
			sb.append("\nTile size : ");
			sb.append(tileWidth);
			sb.append("x");
			sb.append(tileHeight);
			sb.append("\nLevels : ");
			sb.append(numLevels);
			logger.debug(sb.toString());
		}

		// If the default or caller-specified values define a level set that
		// does not fit in the limits of latitude
		// and longitude, then we re-define the level set parameters using
		// values known to fit in those limits.
		/*
		 * if (!this.isWithinLatLonLimits(sector, levelZeroTileDelta, tileOrigin)) { String message = "TiledRasterProducer: native tiling is outside lat/lon limits. Falling back to default tiling." ;
		 * Logging.logger().warning(message);
		 * 
		 * levelZeroTileDelta = LatLon.fromDegrees( MyTiledImageProducer.DEFAULT_LEVEL_ZERO_TILE_DELTA, MyTiledImageProducer.DEFAULT_LEVEL_ZERO_TILE_DELTA);
		 * params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);
		 * 
		 * tileOrigin = new LatLon(Angle.NEG90, Angle.NEG180); params.setValue(AVKey.TILE_ORIGIN, tileOrigin);
		 * 
		 * numLevels = this.computeNumLevels(levelZeroTileDelta, rasterTileDelta); params.setValue(AVKey.NUM_LEVELS, numLevels);
		 * 
		 * numEmptyLevels = 0; params.setValue(AVKey.NUM_EMPTY_LEVELS, numEmptyLevels); }
		 */
	}

	@Override
	protected void doStartProduction(AVList parameters) throws Exception {
		// Copy production parameters to prevent changes to caller's reference.
		this.productionParams = parameters.copy();
		this.initProductionParameters(this.productionParams);

		// Assemble the source data rasters.
		this.assembleDataRasters();

		// Initialize the level set parameters, and create the level set.
		this.initLevelSetParameters(this.productionParams);
		LevelSet levelSet = new LevelSet(this.productionParams);
		// Install the each tiles of the LevelSet.
		if (recomputeCache) {
			this.installLevelSet(levelSet, this.productionParams);
		}
		levelSet.setValues(productionParams);
		setLevels(levelSet);

		// Wait for concurrent tasks to complete.
		this.waitForInstallTileTasks();

		if (recomputeCache) {
			// Clear the raster cache.
			this.getCache().clear();
		}
		// Install the data descriptor for this tiled raster set.
		this.installConfigFile(this.productionParams);
	}

	@Override
	protected DataRaster drawDataSources(LevelSet levelSet, Tile tile, Iterable<DataRaster> dataRasters, AVList params) throws java.io.IOException {
		DataRaster tileRaster = null;

		// Find the data sources that intersect this tile and intersect the
		// LevelSet sector.
		java.util.ArrayList<DataRaster> intersectingRasters = new java.util.ArrayList<DataRaster>();
		for (DataRaster raster : dataRasters) {
			if (raster.getSector().intersects(tile.getSector()) && raster.getSector().intersects(levelSet.getSector())) {
				intersectingRasters.add(raster);
			}
		}

		// If any data sources intersect this tile, and the tile's level is not
		// empty, then we attempt to read those
		// sources and render them into this tile.
		if (!intersectingRasters.isEmpty() && !tile.getLevel().isEmpty()) {
			// Create the tile raster to render into.
			tileRaster = this.createDataRaster(tile.getLevel().getTileWidth(), tile.getLevel().getTileHeight(), tile.getSector(), params);
			// Render each data source raster into the tile raster.
			for (DataRaster raster : intersectingRasters) {
				raster.drawOnTo(tileRaster);
			}
		}

		// Make the data rasters available for garbage collection.
		intersectingRasters.clear();
		// noinspection UnusedAssignment
		intersectingRasters = null;

		return tileRaster;
	}

	@Override
	protected DataRaster drawDescendants(LevelSet levelSet, Tile tile, AVList params) throws java.io.IOException {
		DataRaster tileRaster = null;
		boolean hasDescendants = false;

		Iterable<DataRaster> rasters = this.getDataRasters();

		// Recursively create sub-tile rasters.
		Tile[] subTiles = this.createSubTiles(tile, levelSet.getLevel(tile.getLevelNumber() + 1));
		DataRaster[] subRasters = new DataRaster[subTiles.length];
		DataRaster[] data;
		MyCachedDataRaster cached;
		DataRaster subRaster;
		for (int index = 0; index < subTiles.length; index++) {
			// If the sub-tile does not intersect the level set, then skip that
			// sub-tile.
			if (subTiles[index].getSector().intersects(levelSet.getSector())) {
				boolean created = false;
				for (DataRaster r : rasters) {
					if (created) {
						break;
					}
					if (r instanceof MyCachedDataRaster) {
						cached = (MyCachedDataRaster) r;
						data = cached.getDataRasters();
						for (DataRaster d : data) {
							// Does the tile intersect at least one input file?
							if (subTiles[index].getSector().intersects(d.getSector())) {
								// Recursively create the sub-tile raster.
								subRaster = this.createTileRaster(levelSet, subTiles[index], params);
								// If creating the sub-tile raster fails, then
								// skip that sub-tile.
								if (subRaster != null) {
									subRasters[index] = subRaster;
									hasDescendants = true;
									created = true;
									break;
								}
							}
						}
					}
				}
			}
		}

		// Exit if the caller has instructed us to stop production.
		if (this.isStopped()) {
			return null;
		}

		// If any of the sub-tiles successfully created a data raster, then we
		// potentially create this tile's raster,
		// then write the sub-tiles to disk.
		if (hasDescendants) {
			// If this tile's level is not empty, then create and render the
			// tile's raster.
			if (!tile.getLevel().isEmpty()) {
				// Create the tile's raster.
				tileRaster = this.createDataRaster(tile.getLevel().getTileWidth(), tile.getLevel().getTileHeight(), tile.getSector(), params);

				for (int index = 0; index < subTiles.length; index++) {
					if (subRasters[index] != null) {
						// Render the sub-tile raster to this this tile raster.
						subRasters[index].drawOnTo(tileRaster);
						// Write the sub-tile raster to disk.
						this.installTileRasterLater(levelSet, subTiles[index], subRasters[index], params);
					}
				}
			}
		}

		// Make the sub-tiles and sub-rasters available for garbage collection.
		for (int index = 0; index < subTiles.length; index++) {
			subTiles[index] = null;
			subRasters[index] = null;
		}
		// noinspection UnusedAssignment
		subTiles = null;
		// noinspection UnusedAssignment
		subRasters = null;

		return tileRaster;
	}

	@Override
	protected String validateDataSource(Object source, AVList params) {
		// TiledElevationProducer does not accept null data sources.
		if (source == null) {
			return Logging.getMessage("nullValue.SourceIsNull");
		}

		// TiledElevationProducer accepts BufferWrapperRaster as a data source.
		// If the data source is a DataRaster, then
		// check that it's a BufferWrapperRaster.
		if (source instanceof DataRaster) {
			DataRaster raster = (DataRaster) source;

			if (!(raster instanceof BufferWrapperRaster)) {
				return Logging.getMessage("TiledRasterProducer.UnrecognizedDataSource", raster);
			}

			String s = this.validateDataSourceParams(raster, String.valueOf(raster));
			if (s != null) {
				return s;
			}
		}
		// For any other data source, attempt to find a reader for the data
		// source. If the reader know's the data
		// source's raster type, then check that it's elevation data.
		else {
			// only geotiff can be read
			DataRasterReader reader = readers[0];
			if (reader == null) {
				return Logging.getMessage("TiledRasterProducer.UnrecognizedDataSource", source);
			}

			// Copy the parameter list to insulate changes from the caller.
			params = (params != null) ? params.copy() : new AVListImpl();

			try {
				reader.readMetadata(source, params);
			} catch (IOException e) {
				return Logging.getMessage("TiledRasterProducer.ExceptionWhileReading", source, e.getMessage());
			}

			String s = this.validateDataSourceParams(params, String.valueOf(source));
			if (s != null) {
				return s;
			}
		}

		return null;
	}

	@Override
	protected LatLon computeRasterTileDelta(int tileWidth, int tileHeight, Iterable<? extends DataRaster> rasters) {
		LatLon pixelSize = this.computeSmallestPixelSize(rasters);
		// Compute the tile size in latitude and longitude, given a raster's
		// sector and dimension, and the tile
		// dimensions. In this computation a pixel is assumed to cover a finite
		// area.
		double latDelta = tileHeight * pixelSize.getLatitude().degrees;
		double lonDelta = tileWidth * pixelSize.getLongitude().degrees;
		return LatLon.fromDegrees(latDelta, lonDelta);
	}

	@Override
	protected LatLon computeRasterPixelSize(DataRaster raster) {
		// Compute the raster's pixel dimension in latitude and longitude. In
		// this computation a pixel is assumed to
		// cover a finite area.
		return LatLon.fromDegrees(raster.getSector().getDeltaLatDegrees() / raster.getHeight(), raster.getSector().getDeltaLonDegrees() / raster.getWidth());
	}

	@Override
	protected LatLon computeSmallestPixelSize(Iterable<? extends DataRaster> rasters) {
		// Find the smallest pixel dimensions in the given rasters.
		double smallestLat = Double.MAX_VALUE;
		double smallestLon = Double.MAX_VALUE;
		for (DataRaster raster : rasters) {
			LatLon curSize = this.computeRasterPixelSize(raster);
			if (smallestLat > curSize.getLatitude().degrees) {
				smallestLat = curSize.getLatitude().degrees;
			}
			if (smallestLon > curSize.getLongitude().degrees) {
				smallestLon = curSize.getLongitude().degrees;
			}
		}
		return LatLon.fromDegrees(smallestLat, smallestLon);
	}

	@Override
	protected DataRaster createDataRaster(int width, int height, Sector sector, AVList params) {
		// Create a BIL elevation raster to hold the tile's data.
		AVList bufferParams = new AVListImpl();
		bufferParams.setValue(AVKey.DATA_TYPE, params.getValue(AVKey.DATA_TYPE));
		bufferParams.setValue(AVKey.BYTE_ORDER, params.getValue(AVKey.BYTE_ORDER));
		ByteBufferRaster bufferRaster = new ByteBufferRaster(width, height, sector, bufferParams);

		// Clear the raster with the missing data replacment.
		// This code expects the string
		// "gov.nasa.worldwind.avkey.MissingDataValue", which now corresponds to
		// the key
		// MISSING_DATA_REPLACEMENT.
		Object o = params.getValue(AVKey.MISSING_DATA_REPLACEMENT);
		if (o != null && o instanceof Double) {
			Double missingDataValue = (Double) o;
			bufferRaster.fill(missingDataValue);
			bufferRaster.setTransparentValue(missingDataValue);
		}

		return bufferRaster;
	}

	@Override
	protected void assembleDataSource(Object source, AVList params) throws Exception {
		List<DataRaster> rasters;
		Iterable<DataRaster> dr = this.getDataRasters();
		if (dr instanceof List) {
			rasters = ((List<DataRaster>) dr);
		} else {
			throw new InvalidParameterException();
		}

		if (source instanceof DataRaster) {
			rasters.add((DataRaster) source);

		} else {
			DataRasterReader reader = this.getReaderFactory().findReaderFor(source, params, this.getDataRasterReaders());
			rasters.add(new MyCachedDataRaster(source, params, reader, this.getCache()));
		}
	}

	@Override
	protected DataRasterReader[] getDataRasterReaders() {
		return readers;
	}

	public void setIntersectingRasters(java.util.ArrayList<MyReadableDataRaster> intersectingRasters) {
		this.intersectingRasters = intersectingRasters;
	}

	public List<MyReadableDataRaster> getIntersectingRasters() {
		return intersectingRasters;
	}

	public void setLevels(LevelSet levels) {
		this.levels = levels;
	}

	public LevelSet getLevels() {
		return levels;
	}

	public void setNumLevels(Integer numLevels) {
		this.numLevels = numLevels;
	}

	public Integer getNumLevels() {
		return numLevels;
	}

	/**
	 * Returns an ElevationModel configuration document which describes the tiled elevation data produced by this TiledElevationProducer. The document's contents are based on the configuration
	 * document for a basic ElevationModel, except this document describes an offline dataset. This returns null if the parameter list is null, or if the configuration document cannot be created for
	 * any reason.
	 * 
	 * @param params
	 *            the parameters which describe an ElevationModel configuration document's contents.
	 * 
	 * @return the configuration document, or null if the parameter list is null or does not contain the required parameters.
	 */
	@Override
	protected Document createConfigDoc(AVList params) {
		AVList configParams = params.copy();

		// Determine a default display name if none exists.
		if (configParams.getValue(AVKey.DISPLAY_NAME) == null) {
			configParams.setValue(AVKey.DISPLAY_NAME, params.getValue(AVKey.DATASET_NAME));
		}

		// Set the SERVICE_NAME and NETWORK_RETRIEVAL_ENABLED parameters to
		// indicate this dataset is offline.
		if (configParams.getValue(AVKey.SERVICE_NAME) == null) {
			configParams.setValue(AVKey.SERVICE_NAME, AVKey.SERVICE_NAME_OFFLINE);
		}

		configParams.setValue(AVKey.NETWORK_RETRIEVAL_ENABLED, Boolean.FALSE);

		// TiledElevationProducer and the DataRaster classes use
		// MISSING_DATA_REPLACEMENT to denote the missing data
		// value, whereas all other WWWJ code uses MISSING_DATA_SIGNAL. Replace
		// the MISSING_DATA_REPLACEMENT with the
		// MISSING_DATA_SIGNAL here to ensure this discrapancy is limited to
		// data installation code.
		configParams.removeKey(AVKey.MISSING_DATA_REPLACEMENT);
		configParams.setValue(AVKey.MISSING_DATA_SIGNAL, params.getValue(AVKey.MISSING_DATA_REPLACEMENT));

		// If we able to successfully compute extreme values for this elevation
		// data, then set the extreme elevation
		// values ELEVATION_MIN and ELEVATION_MAX. If the extremes array is null
		// or has length less than 2, the imported
		// elevations are either empty or contain only missing data values. In
		// either case we cannot determine the
		// extreme values.
		if (this.extremes != null && this.extremes.length >= 2) {
			configParams.setValue(AVKey.ELEVATION_MIN, this.extremes[0]);
			configParams.setValue(AVKey.ELEVATION_MAX, this.extremes[1]);
		}

		// Return a configuration file for a BasicElevationModel.
		// BasicElevationModel is the standard WWJ component
		// which consumes tiled elevation data.

		return BasicElevationModel.createBasicElevationModelConfigDocument(configParams);
	}

	// correction non generation du cache
	@Override
	protected void installTileRasterLater(final LevelSet levelSet, final Tile tile, final DataRaster tileRaster, final AVList params) {
		try {
			installTileRaster(tile, tileRaster, params);
			// Dispose the data raster.
			if (tileRaster instanceof Disposable) {
				((Disposable) tileRaster).dispose();
			}
		} catch (IOException e) {
			logger.error("Error during tile installation", e);
		}

	}

	public static long getCACHE_CAPACITY() {
		return CACHE_CAPACITY;
	}

	public static void setCACHE_CAPACITY(long cACHE_CAPACITY) {
		CACHE_CAPACITY = cACHE_CAPACITY;
	}

}
