package fr.ifremer.viewer3d.multidata;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.WritableRaster;
import java.io.IOException;

import fr.ifremer.viewer3d.formats.tiff.BaselineTiff;
import fr.ifremer.viewer3d.formats.tiff.GeoCodec;
import fr.ifremer.viewer3d.formats.tiff.GeoTiff;
import fr.ifremer.viewer3d.formats.tiff.GeotiffMultiReader;
import fr.ifremer.viewer3d.formats.tiff.GeotiffReader;
import fr.ifremer.viewer3d.formats.tiff.Tiff;
import fr.ifremer.viewer3d.formats.tiff.TiffIFDEntry;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;

/***
 * Decodes Texture Geotiff as RGB or 24 bits
 * 
 * @see GeotiffReader
 * @see GeotiffMultiTextureRasterReader
 * @author MORVAN
 * 
 */
public class GeotiffMultiTextureReader extends GeotiffMultiReader {

	private double valGlobaleMin = -20000.0;
	private double valGlobaleMax = 20000.0;
	private double valMin = 2000000000000.0;
	private double valMax = -2000000000000.0;
	private double dateLineTrick = 0;
	private static double tmpdateLineTrick = 0;

	public GeotiffMultiTextureReader(String sourceFilename, double min, double max) throws IOException {
		super(sourceFilename);
		valGlobaleMin = min;
		valGlobaleMax = max;
		dateLineTrick = tmpdateLineTrick;
		tmpdateLineTrick = 0;
	}

	/**
	 * Returns true if georeferencing information was found in this file.
	 * <p/>
	 * Note: see getGeoKeys() for determining if projection information is
	 * contained in the file.
	 * 
	 * @return Returns true if the file contains georeferencing information;
	 *         false otherwise.
	 * 
	 * @throws java.io.IOException
	 *             if data type is not supported or unknown
	 */

	@Override
	protected void repackageGeoReferencingTags() throws IOException {
		for (int i = 0; i < this.getNumImages(); i++) {
			TiffIFDEntry[] ifd = tiffIFDs.get(i);
			AVList values = this.metadata.get(i);

			values.setValue(AVKey.FILE_NAME, this.path);
			// after we read all data, we have evrything as BIG_ENDIAN
			values.setValue(AVKey.BYTE_ORDER, AVKey.BIG_ENDIAN);

			BaselineTiff tiff = BaselineTiff.extract(ifd, this.tiffReader);

			if (null == tiff) {
				String message = Logging.getMessage("GeotiffReader.BadGeotiff");
				Logging.logger().severe(message);
				throw new IOException(message);
			}

			if (tiff.width == Tiff.Undefined) {
				String message = Logging.getMessage("generic.InvalidWidth", tiff.width);
				Logging.logger().severe(message);
				throw new IOException(message);
			}
			values.setValue(AVKey.WIDTH, tiff.width);

			if (tiff.height == Tiff.Undefined) {
				String message = Logging.getMessage("generic.InvalidHeight", tiff.height);
				Logging.logger().severe(message);
				throw new IOException(message);
			}
			values.setValue(AVKey.HEIGHT, tiff.height);

			int sampleFormat = (null != tiff.sampleFormat) ? tiff.sampleFormat[0] : Tiff.Undefined;
			int bitsPerSample = (null != tiff.bitsPerSample) ? tiff.bitsPerSample[0] : Tiff.Undefined;

			if (null != tiff.displayName) {
				values.setValue(AVKey.DISPLAY_NAME, tiff.displayName);
			}

			if (null != tiff.imageDescription) {
				values.setValue(AVKey.DESCRIPTION, tiff.imageDescription);
			}

			if (null != tiff.softwareVersion) {
				values.setValue(AVKey.VERSION, tiff.softwareVersion);
			}

			if (null != tiff.dateTime) {
				values.setValue(AVKey.DATE_TIME, tiff.dateTime);
			}

			if (tiff.photometric == Tiff.Photometric.Color_RGB) {
				values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
				values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.COLOR);
				values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
			} else if (tiff.photometric == Tiff.Photometric.CMYK) {
				values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
				values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.COLOR);
				values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
			} else if (tiff.photometric == Tiff.Photometric.Color_Palette) {
				values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
				values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.COLOR);
				values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
			} else if (tiff.samplesPerPixel == Tiff.SamplesPerPixel.MONOCHROME) { // Tiff.Photometric.Grayscale_BlackIsZero
				// or
				// Tiff.Photometric.Grayscale_WhiteIsZero
				if (sampleFormat == Tiff.SampleFormat.SIGNED) {
					values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
					if (bitsPerSample == Short.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT16);
					} else if (bitsPerSample == Byte.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
					} else if (bitsPerSample == Integer.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT32);
					}
				} else if (sampleFormat == Tiff.SampleFormat.IEEEFLOAT) {
					values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
					if (bitsPerSample == Float.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
					}
				} else if (sampleFormat == Tiff.SampleFormat.UNSIGNED) {
					values.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
					values.setValue(AVKey.IMAGE_COLOR_FORMAT, AVKey.GRAYSCALE);
					if (bitsPerSample == Short.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT16);
					} else if (bitsPerSample == Byte.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT8);
					} else if (bitsPerSample == Integer.SIZE) {
						values.setValue(AVKey.DATA_TYPE, AVKey.INT32);
					}
				}
			}

			if (!values.hasKey(AVKey.PIXEL_FORMAT) || !values.hasKey(AVKey.DATA_TYPE)) {
				String message = Logging.getMessage("Geotiff.UnsupportedDataTypeRaster", tiff.toString());
				Logging.logger().severe(message);
				// throw new IOException(message);
			}

			// geo keys
			for (TiffIFDEntry entry : ifd) {
				try {
					switch (entry.tag) {
					case GeoTiff.Tag.GDAL_NODATA:
						Double d = Double.parseDouble(this.tiffReader.readString(entry));
						values.setValue(AVKey.MISSING_DATA_SIGNAL, d);
						break;

					case Tiff.Tag.MIN_SAMPLE_VALUE:
						values.setValue(AVKey.ELEVATION_MIN, entry.getAsDouble());
						break;

					case Tiff.Tag.MAX_SAMPLE_VALUE:
						values.setValue(AVKey.ELEVATION_MAX, entry.getAsDouble());
						break;

					case GeoTiff.Tag.MODEL_PIXELSCALE:
						this.gc.setModelPixelScale(entry.getDoubles());
						break;

					case GeoTiff.Tag.MODEL_TIEPOINT:
						this.gc.addModelTiePoints(entry.getDoubles());
						break;

					case GeoTiff.Tag.MODEL_TRANSFORMATION:
						this.gc.setModelTransformation(entry.getDoubles());
						break;

					case GeoTiff.Tag.GEO_KEY_DIRECTORY:
						this.gc.setGeokeys(entry.getShorts());
						break;

					case GeoTiff.Tag.GEO_DOUBLE_PARAMS:
						this.gc.setDoubleParams(entry.getDoubles());
						break;

					case GeoTiff.Tag.GEO_ASCII_PARAMS:
						this.gc.setAsciiParams(this.tiffReader.readBytes(entry));
						break;
					}
				} catch (Exception e) {
					Logging.logger().finest(e.toString());
				}
			}

			this.processGeoKeys(i);
		}
	}

	@Override
	@SuppressWarnings("unused")

	public DataRaster doRead(int imageIndex) throws IOException {
		checkImageIndex(imageIndex);
		AVList values = this.metadata.get(imageIndex);

		// Extract the various IFD tags we need to read this image. We want to
		// loop over the tag set once, instead
		// multiple times if we simply used our general getByTag() method.

		long[] stripOffsets = null;
		byte[][] cmap = null;
		long[] stripCounts = null;

		boolean tiffDifferencing = false;

		TiffIFDEntry[] ifd = this.tiffIFDs.get(imageIndex);

		BaselineTiff tiff = BaselineTiff.extract(ifd, this.tiffReader);

		if (null == tiff) {
			String message = Logging.getMessage("GeotiffReader.BadGeotiff");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		if (tiff.width <= 0) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.width, "width", Tiff.Tag.IMAGE_WIDTH);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.height <= 0) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.height, "height", Tiff.Tag.IMAGE_LENGTH);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.samplesPerPixel <= Tiff.Undefined) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.samplesPerPixel, "samplesPerPixel", Tiff.Tag.SAMPLES_PER_PIXEL);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.photometric <= Tiff.Photometric.Undefined || tiff.photometric > Tiff.Photometric.YCbCr) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.photometric, "PhotoInterpretation", Tiff.Tag.PHOTO_INTERPRETATION);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.rowsPerStrip <= Tiff.Undefined) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.rowsPerStrip, "RowsPerStrip", Tiff.Tag.ROWS_PER_STRIP);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.planarConfig != Tiff.PlanarConfiguration.PLANAR && tiff.planarConfig != Tiff.PlanarConfiguration.CHUNKY) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.planarConfig, "PhotoInterpretation", Tiff.Tag.PHOTO_INTERPRETATION);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		for (TiffIFDEntry entry : ifd) {
			try {
				switch (entry.tag) {
				case Tiff.Tag.STRIP_OFFSETS:
					stripOffsets = entry.getAsLongs();
					break;

				case Tiff.Tag.STRIP_BYTE_COUNTS:
					stripCounts = entry.getAsLongs();
					break;

				case Tiff.Tag.COLORMAP:
					cmap = this.tiffReader.readColorMap(entry);
					break;
				}
			} catch (IOException e) {
				Logging.logger().finest(e.toString());
			}
		}

		if (null == stripOffsets || 0 == stripOffsets.length) {
			String message = Logging.getMessage("GeotiffReader.MissingRequiredTag", "StripOffsets");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		if (null == stripCounts || 0 == stripCounts.length) {
			String message = Logging.getMessage("GeotiffReader.MissingRequiredTag", "StripCounts");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		TiffIFDEntry notToday = getByTag(ifd, Tiff.Tag.COMPRESSION);
		boolean lzwCompressed = false;
		if (notToday != null && notToday.asLong() == Tiff.Compression.LZW) {
			lzwCompressed = true;
			TiffIFDEntry predictorEntry = getByTag(ifd, Tiff.Tag.TIFF_PREDICTOR);
			if ((predictorEntry != null) && (predictorEntry.asLong() != 0)) {
				tiffDifferencing = true;
			}
		} else if (notToday != null && notToday.asLong() != Tiff.Compression.NONE) {
			String message = Logging.getMessage("GeotiffReader.CompressionFormatNotSupported");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		notToday = getByTag(ifd, Tiff.Tag.TILE_WIDTH);
		if (notToday != null) {
			String message = Logging.getMessage("GeotiffReader.NoTiled");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		long offset = stripOffsets[0];
		// int sampleFormat = (null != tiff.sampleFormat) ? tiff.sampleFormat[0]
		// : Tiff.Undefined;
		// int bitsPerSample = (null != tiff.bitsPerSample) ?
		// tiff.bitsPerSample[0] : Tiff.Undefined;

		BufferedImage rgbImage = null;

		// case rgb
		boolean isRGB = false;
		if (values.getValue(AVKey.DATA_TYPE) == AVKey.INT8) {
			isRGB = true;
			byte[][] image = this.tiffReader.readPixelInterleaved8(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts);

			rgbImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_INT_ARGB);
			WritableRaster wrRaster = rgbImage.getRaster();

			int next = 0;
			int r, g, b;
			for (int y = 0; y < tiff.height; y++) {
				for (int x = 0; x < tiff.width; x++) {
					r = 0xFF & (image[0][next++]);
					g = 0xFF & (image[0][next++]);
					b = 0xFF & (image[0][next++]);

					// no black data => transparency issues
					if (r == 0 && g == 0 && b == 0) {
						r = 1;
						g = 1;
						b = 1;
					}

					wrRaster.setSample(x, y, 0, r);
					wrRaster.setSample(x, y, 1, g);
					wrRaster.setSample(x, y, 2, b);
					wrRaster.setSample(x, y, 3, (255));
				}
			}
		} else if (values.getValue(AVKey.DATA_TYPE) == AVKey.INT16 && tiff.samplesPerPixel == 1) {
			short[] image = this.tiffReader.read16bitPixelInterleavedImage(imageIndex, tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

			BufferedImage buffImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_INT_RGB);
			rgbImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_INT_RGB);
			ColorSpace rgb = ColorSpace.getInstance(ColorSpace.CS_LINEAR_RGB);
			WritableRaster wrRaster = buffImage.getRaster();

			int next = 0;
			for (int y = 0; y < tiff.height; y++) {
				for (int x = 0; x < tiff.width; x++) {
					wrRaster.setSample(x, y, 0, 2 * image[next] - ((char) (2 * image[next] / 256) * 256));
					wrRaster.setSample(x, y, 1, ((char) (2 * image[next++] / 256)));
					// Logging.logger().warning("original value" +
					// Integer.toString(image[next++]));
					/*
					 * Logging.logger().warning("value" +
					 * Integer.toString(0xFFFF & (int) (image[next++])));
					 */
				}
			}
			ColorConvertOp theOp = new ColorConvertOp(rgb, null);
			theOp.filter(buffImage, rgbImage);
		} else if (values.getValue(AVKey.DATA_TYPE) == AVKey.INT16 && tiff.samplesPerPixel > 1) {
			short[] image = this.tiffReader.read16bitPixelInterleavedImage(imageIndex, tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

			rgbImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_INT_RGB);
			WritableRaster wrRaster = rgbImage.getRaster();

			int next = 0;
			for (int y = 0; y < tiff.height; y++) {
				for (int x = 0; x < tiff.width; x++) {
					wrRaster.setSample(x, y, 0, image[next++]);
				}
			}
		} else if (values.getValue(AVKey.DATA_TYPE) == AVKey.FLOAT32) {
			float[][] data = this.tiffReader.readPlanarFloat32(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

			int next = 0;
			rgbImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_INT_ARGB);
			WritableRaster wrRaster = rgbImage.getRaster();

			int value;
			char red, green, blue;
			for (int y = 0; y < tiff.height; y++) {
				for (int x = 0; x < tiff.width; x++) {
					if (data[0][next] == -32768.0)// transparency
					{
						wrRaster.setSample(x, y, 0, (0));
						wrRaster.setSample(x, y, 1, (0));
						wrRaster.setSample(x, y, 2, (0));
						wrRaster.setSample(x, y, 3, (0));
						next++;
					} else {
						if (valMin > data[0][next]) {
							valMin = data[0][next];
						}
						if (valMax < data[0][next]) {
							valMax = data[0][next];
						}
						// valGlobaleMax+1.0 for cast purpose
						value = (int) ((data[0][next++] - valGlobaleMin) / (valGlobaleMax + 1.0 - valGlobaleMin) * 256 * 256 * 256);// elevation
						// 24bits
						red = (char) (value / 256 / 256);// first 8 bits
						green = (char) ((value - red * 256 * 256) / 256);// next
						// 8bits
						blue = (char) ((value - red * 256 * 256 - green * 256));
						// value = (value > maxSampleValue || value <
						// minSampleValue) ? nodataSignal : value;
						// value = 0.0;
						wrRaster.setSample(x, y, 0, (red));
						wrRaster.setSample(x, y, 1, (green));
						wrRaster.setSample(x, y, 2, (blue));
						wrRaster.setSample(x, y, 3, (255));
					}
				}
			}
		}

		if (null == rgbImage) {
			String message = Logging.getMessage("Geotiff.UnsupportedDataTypeRaster", tiff.toString());
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		rgbImage = ImageUtil.toCompatibleImage(rgbImage);
		final BufferedImageRaster r = (BufferedImageRaster) BufferedImageRaster.wrap(rgbImage, values);
		// r.setRGB(isRGB);
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				// r.dispose(true);
			}
		});
		t.start();

		// wait to help write picture
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return r;

	}

	@Override
	protected void processGeoKeys(int imageIndex) throws IOException {
		this.checkImageIndex(imageIndex);

		AVList values = this.metadata.get(imageIndex);

		if (null == values || null == this.gc || !this.gc.hasGeoKey(GeoTiff.GeoKey.ModelType) || !values.hasKey(AVKey.WIDTH) || !values.hasKey(AVKey.HEIGHT)) {
			return;
		}

		int width = (Integer) values.getValue(AVKey.WIDTH);
		int height = (Integer) values.getValue(AVKey.HEIGHT);

		// geo-tiff spec requires the VerticalCSType to be present for
		// elevations (but ignores its value)
		if (this.gc.hasGeoKey(GeoTiff.GeoKey.VerticalCSType)) {
			values.setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
		}

		if (this.gc.hasGeoKey(GeoTiff.GeoKey.VerticalUnits)) {
			int[] v = this.gc.getGeoKeyAsInts(GeoTiff.GeoKey.VerticalUnits);
			int units = (null != v && v.length > 0) ? v[0] : GeoTiff.Undefined;

			if (units == GeoTiff.Unit.Linear.Meter) {
				values.setValue(AVKey.ELEVATION_UNIT, AVKey.UNIT_METER);
			} else if (units == GeoTiff.Unit.Linear.Foot) {
				values.setValue(AVKey.ELEVATION_UNIT, AVKey.UNIT_FOOT);
			}
		}

		if (this.gc.hasGeoKey(GeoTiff.GeoKey.RasterType)) {
			int[] v = this.gc.getGeoKeyAsInts(GeoTiff.GeoKey.RasterType);
			int rasterType = (null != v && v.length > 0) ? v[0] : GeoTiff.Undefined;

			if (rasterType == GeoTiff.RasterType.RasterPixelIsArea) {
				values.setValue(AVKey.RASTER_PIXEL, AVKey.RASTER_PIXEL_IS_AREA);
			} else if (rasterType == GeoTiff.RasterType.RasterPixelIsPoint) {
				values.setValue(AVKey.RASTER_PIXEL, AVKey.RASTER_PIXEL_IS_POINT);
			}
		}

		if (this.gc.hasGeoKey(GeoTiff.GeoKey.GeogAngularUnits)) {
			// int[] v = this.gc.getGeoKeyAsInts(
			// GeoTiff.GeoKey.GeogAngularUnits );
			// int unit = ( null != v && v.length > 0 ) ? v[0] :
			// GeoTiff.Undefined;
			//
			// if( unit == GeoTiff.Unit.Angular.Angular_Degree )
			// else if( unit == GeoTiff.Unit.Angular.Angular_Radian )
		}

		// AVKey.PROJECTION_DATUM Optional,
		// AVKey.PROJECTION_DESC Optional,
		// AVKey.PROJECTION_NAME Optional,
		// AVKey.PROJECTION_UNITS Optional,

		int gtModelTypeGeoKey = GeoTiff.ModelType.Undefined;

		if (this.gc.hasGeoKey(GeoTiff.GeoKey.ModelType)) {
			int[] gkValues = this.gc.getGeoKeyAsInts(GeoTiff.GeoKey.ModelType);
			if (null != gkValues && gkValues.length > 0) {
				gtModelTypeGeoKey = gkValues[0];
			}
		}

		if (gtModelTypeGeoKey == GeoTiff.ModelType.Geographic) {
			values.setValue(AVKey.COORDINATE_SYSTEM, AVKey.COORDINATE_SYSTEM_GEOGRAPHIC);

			int epsg = GeoTiff.GCS.Undefined;

			if (this.gc.hasGeoKey(GeoTiff.GeoKey.GeographicType)) {
				int[] gkValues = this.gc.getGeoKeyAsInts(GeoTiff.GeoKey.GeographicType);
				if (null != gkValues && gkValues.length > 0) {
					epsg = gkValues[0];
				}
			}

			if (epsg != GeoTiff.GCS.Undefined) {
				values.setValue(AVKey.PROJECTION_EPSG_CODE, epsg);
			}

			// TODO Assumes WGS84(4326)- should we check for this ?

			double[] bbox = this.gc.getBoundingBox(width, height);
			// trick for date line
			//
			if (bbox[2] > 180) {
				tmpdateLineTrick = bbox[0] + 180;
				bbox[2] -= tmpdateLineTrick;
				bbox[0] -= tmpdateLineTrick;
			}
			values.setValue(AVKey.SECTOR, Sector.fromDegrees(bbox[3], bbox[1], bbox[0], bbox[2]));
			values.setValue(AVKey.ORIGIN, LatLon.fromDegrees(bbox[1], bbox[0]));

			// if min latitude is not in latitudes list, we consider a new entry
			// for height
			// pictures are considered side to side (no space, no overlay)
			if (!latitudes.contains(bbox[3])) {
				latitudes.add(bbox[3]);
				heights.add(height);
			}
			if (!longitudes.contains(bbox[0])) {
				longitudes.add(bbox[0]);
				widths.add(width);
			}

			if (bbox[3] < minLatitude) {
				minLatitude = bbox[3];
			}
			if (bbox[0] < minLongitude) {
				minLongitude = bbox[0];
			}
			if (bbox[1] > maxLatitude) {
				maxLatitude = bbox[1];
			}
			if (bbox[2] > maxLongitude) {
				maxLongitude = bbox[2];
			}
		} else if (gtModelTypeGeoKey == GeoTiff.ModelType.Projected) {
			values.setValue(AVKey.COORDINATE_SYSTEM, AVKey.COORDINATE_SYSTEM_PROJECTED);

			int projection = GeoTiff.PCS.Undefined;
			String hemi;
			int zone;

			int[] vals = null;
			if (this.gc.hasGeoKey(GeoTiff.GeoKey.Projection)) {
				vals = this.gc.getGeoKeyAsInts(GeoTiff.GeoKey.Projection);
			} else if (this.gc.hasGeoKey(GeoTiff.GeoKey.ProjectedCSType)) {
				vals = this.gc.getGeoKeyAsInts(GeoTiff.GeoKey.ProjectedCSType);
			}

			if (null != vals && vals.length > 0) {
				projection = vals[0];
			}

			if (projection != GeoTiff.PCS.Undefined) {
				values.setValue(AVKey.PROJECTION_EPSG_CODE, projection);
			}

			// TODO read more GeoKeys and GeoKeyDirectoryTag values

			/*
			 * from
			 * http://www.remotesensing.org/geotiff/spec/geotiff6.html#6.3.3.2
			 * UTM (North) Format: 160zz UTM (South) Format: 161zz
			 */
			if ((projection >= 16100) && (projection <= 16199)) // UTM Zone
				// South
			{
				hemi = AVKey.SOUTH;
				zone = projection - 16100;
			} else if ((projection >= 16000) && (projection <= 16099)) // UTM
				// Zone
				// North
			{
				hemi = AVKey.NORTH;
				zone = projection - 16000;
			} else if ((projection >= 26900) && (projection <= 26999)) // UTM :
				// NAD83
			{
				hemi = AVKey.NORTH;
				zone = projection - 26900;
			} else if ((projection >= 32201) && (projection <= 32260)) // UTM :
				// WGS72
				// N
			{
				hemi = AVKey.NORTH;
				zone = projection - 32200;
			} else if ((projection >= 32301) && (projection <= 32360)) // UTM :
				// WGS72
				// S
			{
				hemi = AVKey.SOUTH;
				zone = projection - 32300;
			} else if ((projection >= 32401) && (projection <= 32460)) // UTM :
				// WGS72BE
				// N
			{
				hemi = AVKey.NORTH;
				zone = projection - 32400;
			} else if ((projection >= 32501) && (projection <= 32560)) // UTM :
				// WGS72BE
				// S
			{
				hemi = AVKey.SOUTH;
				zone = projection - 32500;
			} else if ((projection >= 32601) && (projection <= 32660)) // UTM :
				// WGS84
				// N
			{
				hemi = AVKey.NORTH;
				zone = projection - 32600;
			} else if ((projection >= 32701) && (projection <= 32760)) // UTM :
				// WGS84
				// S
			{
				hemi = AVKey.SOUTH;
				zone = projection - 32700;
			} else {
				String message = Logging.getMessage("generic.UnknownProjection", projection);
				Logging.logger().severe(message);
				// throw new IOException(message);
				return;
			}

			double pixelScaleX = this.gc.getModelPixelScaleX();
			double pixelScaleY = Math.abs(this.gc.getModelPixelScaleY());

			// dump "world file" values into values
			values.setValue(AVKey.PROJECTION_HEMISPHERE, hemi);
			values.setValue(AVKey.PROJECTION_ZONE, zone);
			values.setValue(WorldFile.WORLD_FILE_X_PIXEL_SIZE, pixelScaleX);
			values.setValue(WorldFile.WORLD_FILE_Y_PIXEL_SIZE, -pixelScaleY);

			// shift to center
			GeoCodec.ModelTiePoint[] tps = this.gc.getTiePoints();
			if (null != tps && tps.length > imageIndex) {
				GeoCodec.ModelTiePoint tp = tps[imageIndex];

				double xD = tp.getX() + (pixelScaleX / 2d);
				double yD = tp.getY() - (pixelScaleY / 2d);

				values.setValue(WorldFile.WORLD_FILE_X_LOCATION, xD);
				values.setValue(WorldFile.WORLD_FILE_Y_LOCATION, yD);
			}

			Sector s = ImageUtil.calcBoundingBoxForUTM(values);
			values.setValue(AVKey.SECTOR, s);
		} else {
			String msg = Logging.getMessage("Geotiff.UnknownGeoKeyValue", gtModelTypeGeoKey, GeoTiff.GeoKey.ModelType);
			Logging.logger().severe(msg);
			// throw new IOException(msg);
		}
	}

	public double getValGlobaleMin() {
		return valGlobaleMin;
	}

	public void setValGlobaleMin(double valGlobaleMin) {
		this.valGlobaleMin = valGlobaleMin;
	}

	public double getValGlobaleMax() {
		return valGlobaleMax;
	}

	public void setValGlobaleMax(double valGlobaleMax) {
		this.valGlobaleMax = valGlobaleMax;
	}

	public double getValMin() {
		return valMin;
	}

	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	public double getValMax() {
		return valMax;
	}

	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	public double getDateLineTrick() {
		return dateLineTrick;
	}

	public void setDateLineTrick(double dateLineTrick) {
		this.dateLineTrick = dateLineTrick;
	}

}
