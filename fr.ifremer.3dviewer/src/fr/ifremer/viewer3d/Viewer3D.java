package fr.ifremer.viewer3d;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;

import fr.ifremer.globe.ui.events.BaseEventTopics;
import fr.ifremer.globe.ui.events.PlayerEvent;
import fr.ifremer.globe.ui.events.PlayerEvent.EPlayerAction;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.viewer3d.gamepad.FlyingModel;
import fr.ifremer.viewer3d.gamepad.GamePadThread;
import fr.ifremer.viewer3d.gui.SSVStatusBar;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.background.MyTerrainProfileLayer;
import fr.ifremer.viewer3d.layers.background.WWLatLonGraticuleLayer;
import fr.ifremer.viewer3d.layers.terrain.contrast.TerrainContrastController;
import fr.ifremer.viewer3d.layers.xml.AbstractMultiTextureTiledImageLayer;
import fr.ifremer.viewer3d.selection.WWLayerSelectionController;
import fr.ifremer.viewer3d.terrain.SSVTessellator;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;
import fr.ifremer.viewer3d.util.conf.ConfLayer;
import fr.ifremer.viewer3d.util.conf.ProxyConf;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import fr.ifremer.viewer3d.util.conf.TerrainProfileSettings;
import fr.ifremer.viewer3d.util.conf.UISettings;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuController;
import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.awt.ViewInputAttributes;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.layers.CompassLayer;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.ViewControlsLayer;
import gov.nasa.worldwind.layers.ViewControlsSelectListener;
import gov.nasa.worldwind.layers.placename.PlaceNameLayer;
import gov.nasa.worldwind.render.Highlightable;
import gov.nasa.worldwind.terrain.Tessellator;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import swingintegration.example.EmbeddedSwingComposite;
import swingintegration.example.Platform;

/**
 * Main 3DViewer part which basically is the whole earth visualization.
 */
public class Viewer3D {

	private static final Logger LOGGER = LoggerFactory.getLogger(Viewer3D.class);

	public static final String PART_ID = "fr.ifremer.globe.geographicview";

	@Inject
	private IEventBroker eventBroker;
	@Inject
	private WWLayerSelectionController layerSelectionController;
	@Inject
	private ProxyController proxyController;
	@Inject
	private TerrainContrastController terrainContrastController;
	@Inject
	private ProjectExplorerModel projectExplorerModel;
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Worldwind model **/
	private final BasicModel model = new BasicModel();

	/** Background layers **/
	private final LayerList backgroundLayers = new LayerList();
	private final MyTerrainProfileLayer terrainProfileLayer = new MyTerrainProfileLayer();
	@Inject
	private WWLatLonGraticuleLayer latLonGraticuleLayer;

	private static Viewer3D currentViewer3D;

	/** WorldWindowGLCanvas. */
	private WorldWindowGLCanvas wwd;

	/** Configuration for this frame. */
	private SSVConfiguration configuration;

	/*** terrain textures are interpolated */
	private static boolean interpolate = true;

	/** terrain textures are in high resolution */
	private boolean highResolution = false;

	private PerformanceListener performanceListerner;

	private EmbeddedSwingComposite embeddedComponent;

	/**
	 * The property change support of the Viewer3D class
	 */
	private static PropertyChangeSupport pcs = new PropertyChangeSupport(Viewer3D.class);

	/**
	 * Property used when the interpolation is changed.
	 */
	public static final String INTERPOLATION_EVENT = "interpolation_event";

	/**
	 * Property used when the resolution is changed.
	 */
	public static final String RESOLUTION_EVENT = "resolution_event";

	private GamePadThread gamepadControler;
	private FlyingModel viewPositionUpdater;
	protected JLabel extraStatusBarLabel;

	/** Last highlighted object */
	protected Highlightable lastHighlightObject;

	@Inject
	public Viewer3D() {
		Platform.initLAF();
		currentViewer3D = this;
	}

	@Focus
	public void focus() {
		embeddedComponent.setFocus();
	}

	@PreDestroy
	public void predispose() {
		saveConfiguration();
		gamepadControler.stop();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@PostConstruct
	public void createPartControl(Composite parent) {
		parent.setLayoutData(new GridData(GridData.FILL_BOTH));
		proxyController.detectProxy();
		initWorldWindLayerModel();
		loadConfiguration();

		if (Platform.isMac()) // bug workaround for mac platform (if not it freezes)
			GLProfile.getMaximum(true);

		embeddedComponent = new EmbeddedSwingComposite(parent, SWT.NONE) {

			@Override
			protected JComponent createSwingComponent() {
				var panel = new JPanel(new java.awt.BorderLayout());

				// Add the WWJ 3D OpenGL Canvas to the Swing Panel
				panel.add(wwd, BorderLayout.CENTER);

				// create status bar
				extraStatusBarLabel = new JLabel();
				var statusBar = new SSVStatusBar();
				JPanel statusBarPanel = new JPanel();
				BorderLayout borderLayout = new BorderLayout();
				borderLayout.setHgap(10);
				statusBarPanel.setLayout(borderLayout);
				statusBarPanel.add(extraStatusBarLabel, BorderLayout.WEST);
				statusBarPanel.add(statusBar, BorderLayout.CENTER);
				panel.add(statusBarPanel, BorderLayout.PAGE_END);
				statusBar.setEventSource(wwd);

				startPerformanceRecord();

				// initializations post OpenGL Init
				wwd.addGLEventListener(new GLEventListener() {
					@Override
					public void reshape(GLAutoDrawable p, int i1, int i2, int i3, int i4) {
						/* not used */ }

					@Override
					public void init(GLAutoDrawable paramGLAutoDrawable) {
						new CapacityInspector().dump();
					}

					@Override
					public void dispose(GLAutoDrawable paramGLAutoDrawable) {
						/* not used */ }

					@Override
					public void display(GLAutoDrawable paramGLAutoDrawable) {
						/* not used */ }
				});

				getDisplay().asyncExec(() -> {
					final Rectangle clientArea = parent.getClientArea();
					embeddedComponent.setSize(clientArea.width, clientArea.height);
				});
				createGamePadControl();
				return panel;
			}
		};
		embeddedComponent.setLayoutData(new GridData(GridData.FILL_BOTH));
		embeddedComponent.populate();
	}

	/**
	 * Initializes WW model with default layers
	 */
	private void initWorldWindLayerModel() {
		wwd = new WorldWindowGLCanvas();

		// insert specific background layers (others background layers are defined in worldwin.layers.xml)
		insertBeforeCompass(latLonGraticuleLayer);
		terrainProfileLayer.setEnabled(false); // disable terrain profile layer by default
		insertBeforeCompass(terrainProfileLayer);
		backgroundLayers.addAll(model.getLayers());
		// add background nodes in Project Explorer
		backgroundLayers.stream().map(LayerNode::new).forEach(projectExplorerModel::addBackgroundNode);

		wwd.setModel(model);

		// add listeners
		wwd.getInputHandler().addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				processKeyEvent(e.getKeyChar(), e.isControlDown());
			}
		});
		wwd.addSelectListener(this::highlight);
		wwd.addSelectListener(new Dragger(wwd));
		wwd.addSelectListener(new ContextMenuController());
		wwd.addSelectListener(new ViewControlsSelectListener(wwd,
				(ViewControlsLayer) model.getLayers().getLayersByClass(ViewControlsLayer.class).get(0)));
		wwd.addMouseListener(layerSelectionController);

		// Disable the auto center globe when left click
		ViewInputAttributes attrs = wwd.getView().getViewInputHandler().getAttributes();
		attrs.getActionMap(ViewInputAttributes.DEVICE_MOUSE).getActionAttributes(ViewInputAttributes.VIEW_MOVE_TO)
				.setMouseActionListener(null);
	}

	/**
	 * Dump performance records, make a sync redraw for this, all data will be added to the properties of the main nasa
	 * node
	 */
	public void startPerformanceRecord() {
		if (performanceListerner == null)
			performanceListerner = new PerformanceListener(wwd);
		performanceListerner.start();
	}

	public void stopPerformanceRecord() {
		performanceListerner.stop();
	}

	private void loadConfiguration() {
		configuration = SSVConfiguration.getConfiguration();
		if (configuration != null) {
			// WARNING proxy detection must be done before frame creation
			if (!configuration.isOfflineMode())
				proxyController.initProxy(configuration.getProxy());

			// init the cache memory size
			configuration.setCacheSize(configuration.getCacheSize());
			configuration.setGpuCacheSize(configuration.getGpuCacheSize());

			WorldWind.setOfflineMode(configuration.isOfflineMode());

			// specific initialization for terrain profile layer
			MyTerrainProfileLayer tpl = terrainProfileLayer;
			tpl.setEventSource(wwd);
			UISettings settings = configuration.getUISettings();
			if (settings != null) {
				TerrainProfileSettings tp = settings.getterrainProfile();
				if (tp != null) {
					tpl.setIsMinimized(tp.isMinimized());
					tpl.setIsMaximized(tp.isMaximized());
				}
			}

			// load layers enable state from configuration
			for (var confLayer : configuration.getLayers())
				Optional.ofNullable(backgroundLayers.getLayerByName(confLayer.getName()))
						.ifPresent(layer -> layer.setEnabled(confLayer.getEnabled()));

			// Apply detail hint to loaded maps
			ProxyConf pc = configuration.getProxy();
			configuration.getNasaWorldWindDetailHint().applyImageDetailHint(pc.getImageLevelOfDetail());
			configuration.getNasaWorldWindDetailHint().applyTerrainDetailHint(pc.getLOD());
		}
	}

	public void saveConfiguration() {
		if (configuration != null) {
			configuration.setOfflineMode(WorldWind.isOfflineMode());

			// terrain profile layer parameters
			UISettings settings = configuration.getUISettings();
			if (settings != null) {
				TerrainProfileSettings tp = settings.getterrainProfile();
				if (tp == null) {
					tp = new TerrainProfileSettings();
					settings.setTerrainProfile(tp);
				}
				tp.setEnabled(terrainProfileLayer.isEnabled());
				tp.setMinimized(terrainProfileLayer.getIsMinimized());
				tp.setMaximized(terrainProfileLayer.getIsMaximized());
			}

			// default map layers activation
			configuration.getLayers().clear();
			backgroundLayers.stream().map(backgroundLayer -> {
				var confLayer = new ConfLayer();
				confLayer.setEnabled(backgroundLayer.isEnabled());
				confLayer.setName(backgroundLayer.getName());
				return confLayer;
			}).forEach(configuration.getLayers()::add);

			configuration.save();
		}
	}

	/***
	 * Inserts additional layer before {@link CompassLayer}.
	 */
	public void insertBeforeCompass(Layer layer) {
		LayerList layers = model.getLayers();
		var compassLayerIndex = layers.stream().filter(CompassLayer.class::isInstance).mapToInt(layers::indexOf)
				.findFirst().orElse(0);
		layers.add(compassLayerIndex, layer);
	}

	/**
	 * @return current Viewer3D instance.
	 */
	public static Viewer3D getCurrentViewer3D() {
		return currentViewer3D;
	}

	public WorldWindowGLCanvas getCurrentWwd() {
		return wwd;
	}

	/**
	 * @return current Frame.
	 */
	public static Frame getFrame() {
		Frame result = null;
		Viewer3D currentViewer3D = getCurrentViewer3D();
		if (currentViewer3D != null) {
			synchronized (currentViewer3D) {
				result = currentViewer3D.embeddedComponent.getFrame();
			}
		}
		return result;
	}

	public void refreshAll() {
		RefreshOpenGLView.fullRefresh();
	}

	/**
	 * @return true when Viewer3D is up and operational.
	 */
	public static boolean isUp() {
		return getCurrentViewer3D() != null;
	}

	/**
	 * Handles short key events
	 * 
	 * @param key : key typed by user
	 * @param isCtrlDown
	 */
	private void processKeyEvent(char key, boolean isCtrlDown) {
		switch (Character.toLowerCase(key)) { // ignore case
		// show / hide terrain
		case 't' -> {
			if (wwd.getSceneController() instanceof Viewer3DSceneController viewer3DSceneController)
				viewer3DSceneController.setDisplayorHideTerrain();
		}
		// show / hide layers within geobox view
		case 'h', 's' -> {
			if (wwd.getSceneController() instanceof Viewer3DSceneController viewer3DSceneController)
				viewer3DSceneController.getDisplayedGeoBox().map(fileLayerStoreModel::getLayersWithinGeoBox)//
						.stream().flatMap(List::stream)//
						.forEach(wwLayerInView -> wwLayerInView.setEnabled(key == 's'));
		}
		// show / hide graticule layer
		case 'g' -> {
			// Search the LayerNode of the graticule to change its checked state (thus change the layer enabled flag */
			Optional<LayerNode> layerNode = projectExplorerModel.getBackgroundNode().getChildNode(LayerNode.class,
					n -> n.getLayer().filter(latLonGraticuleLayer::equals).isPresent());
			layerNode.ifPresent(node -> node.setChecked(!latLonGraticuleLayer.isEnabled()));
		}
		// resolution
		case '$' -> {
			setHighResolution(); // reloads configuration
			pcs.firePropertyChange(RESOLUTION_EVENT, !highResolution, highResolution);
		}
		// interpolation
		case 'i' -> {
			interpolateTerrain();
			pcs.firePropertyChange(INTERPOLATION_EVENT, !interpolate, interpolate);
		}
		// contrast
		case 'c' -> terrainContrastController.computeContrast();
		case 'm' -> terrainContrastController.resetContrast();
		// debug : wire frames, volumes...
		case 'w' -> {
			var showExterior = !model.isShowWireframeInterior() && !model.isShowTessellationBoundingVolumes();
			var showInterior = !model.isShowWireframeInterior() && model.isShowWireframeExterior();
			var showTessellationBoundingVolumes = model.isShowWireframeInterior() && model.isShowWireframeExterior();
			model.setShowWireframeExterior(showExterior);
			model.setShowWireframeInterior(showInterior);
			model.setShowTessellationBoundingVolumes(showTessellationBoundingVolumes);
		}
		// player
		case ' ' -> eventBroker.post(BaseEventTopics.TOPIC_BASE,
				new PlayerEvent(isCtrlDown ? EPlayerAction.REVERT_PLAY_PAUSE : EPlayerAction.PLAY_PAUSE));
		case 'o' -> eventBroker.post(BaseEventTopics.TOPIC_BASE, new PlayerEvent(EPlayerAction.STEP_FORWARD));
		case 'p' -> eventBroker.post(BaseEventTopics.TOPIC_BASE, new PlayerEvent(EPlayerAction.STEP_BACKWARD));
		}
		refreshRequired();
	}

	/***
	 *
	 * @return true if texture are interpolated
	 */
	public static boolean isInterpolate() {
		return interpolate;
	}

	/***
	 * Change interpolation mode Nearest / Linear
	 */
	public static void interpolateTerrain() {
		interpolate = !interpolate;
		getWwd().redraw();
	}

	/***
	 *
	 * @return true if high resolution is activated
	 */
	public boolean isHighResolution() {
		return highResolution;
	}

	/***
	 * Change high resolution state
	 */
	public void setHighResolution() {
		highResolution = !highResolution;
		SSVConfiguration configuration = SSVConfiguration.getConfiguration();
		ProxyConf pc = configuration.getProxy();
		configuration.getNasaWorldWindDetailHint().applyImageDetailHint(pc.getImageLevelOfDetail());
		configuration.getNasaWorldWindDetailHint().applyTerrainDetailHint(pc.getLOD());

		MyAbstractLayer.setHighQualityDisplayOfTexture(highResolution);
		AbstractMultiTextureTiledImageLayer.setHighQualityDisplayOfTexture(highResolution);
		refreshRequired();
	}

	public static void addListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public static void removeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	// deprecated static accessors

	public static BasicModel getModel() {
		return currentViewer3D.model;
	}

	public FlyingModel getViewPositionUpdater() {
		return viewPositionUpdater;
	}

	/**
	 * @param extraStatusBarLabel the {@link #extraStatusBarLabel} to set
	 */
	public void setExtraStatusBarLabel(String extraStatusBarInformation) {
		extraStatusBarLabel.setText(extraStatusBarInformation);
	}

	/** Postpone a refresh */
	public static void refreshRequired() {
		if (isUp())
			SwingUtilities.invokeLater(() -> getWwd().redraw());
	}

	/** Insert the layer into the layer list just before the placenames. */
	public static void insertBeforePlacenames(Layer layer) {
		int compassPosition = 0;
		LayerList layers = getModel().getLayers();
		for (Layer l : layers) {
			if (l instanceof PlaceNameLayer) {
				compassPosition = layers.indexOf(l);
			}
		}
		layers.add(compassPosition, layer);
	}

	/** Highlight an {@link Highlightable} when mouse roll over it */
	protected void highlight(SelectEvent event) {
		try {
			if (SelectEvent.ROLLOVER.equals(event.getEventAction())) {
				Object o = event.getTopObject();
				if (!Objects.equals(lastHighlightObject, o)) {
					// Turn off highlight if on.
					if (lastHighlightObject != null) {
						lastHighlightObject.setHighlighted(false);
						lastHighlightObject = null;
					}

					// Turn on highlight if object selected.
					if (o instanceof Highlightable highlightable) {
						lastHighlightObject = highlightable;
						lastHighlightObject.setHighlighted(true);
					}
				}
			}
		} catch (Exception e) {
			// Wrap the handler in a try/catch to keep exceptions from bubbling up
			LOGGER.warn(e.getMessage());
		}
	}

	public static void fitUnderGround(boolean underGround) {
		Tessellator t = getModel().getGlobe().getTessellator();
		t.setMakeTileSkirts(!underGround);
		if (t instanceof SSVTessellator sSVTessellator)
			sSVTessellator.setUnderground(underGround);
		getWwd().redraw();
	}

	public static WorldWindowGLCanvas getWwd() {
		return getCurrentViewer3D().getCurrentWwd();
	}

	public void createGamePadControl() {
		viewPositionUpdater = new FlyingModel(wwd);
		gamepadControler = new GamePadThread(viewPositionUpdater);
		gamepadControler.start();
	}
}
