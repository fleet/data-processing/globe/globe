//REQ-IHM-004 - Markers Filtering
/**
 * UI Class for Min/Max Spinner filter creation
 * This class construct the filter object 
 */
package fr.ifremer.viewer3d.gui;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeSupport;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.viewer3d.layers.plume.filter.MinMaxFilter;
import fr.ifremer.viewer3d.model.PlumeBean;

public class MinMaxFilterUI extends GenericFilterUI {

	private Button activator;
	private Spinner minSpinner;
	private Spinner maxSpinner;
	private Button minButton;
	private Button maxButton;
	private float oldMinValue;
	private float oldMaxValue;
	private static boolean minMaxTitle = false;
	// FT5440 Add precision for value displaying (0 for integer, 3 for float
	// values).
	private int precision;
	private float scale;
	// FT5426 add reset menu
	private Menu resetMinMenu;
	private Menu resetMaxMenu;
	private MenuItem resetMinMenuItem;
	private MenuItem resetMaxMenuItem;
	int iMinValue;
	int iMaxValue;

	public MinMaxFilterUI(Group group, String label, String signal, PropertyChangeSupport pcs, int precision, MinMaxFilter genericFilter) {
		super(group, label, signal, pcs);

		// Set precision for Spinner
		this.precision = precision;
		this.scale = (float) Math.pow(10.0f, this.precision);

		// Contruct user interface
		initUI();

		// Construct filter object
		filterObject = genericFilter;

	}

	private void initUI() {
		if (!minMaxTitle) {
			final Composite nullComposite = new Composite(group, SWT.NONE);
			final Label minLabel = new Label(group, SWT.NONE);
			final Label maxLabel = new Label(group, SWT.NONE);

			minLabel.setText("Min :");
			maxLabel.setText("Max :");

			nullComposite.setLayout(new GridLayout(1, false));
			nullComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			minLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			maxLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			minMaxTitle = true;
		}

		// Widgets Creation
		activator = new Button(group, SWT.CHECK);
		minButton = new Button(group, SWT.CHECK);
		minSpinner = new Spinner(group, SWT.NONE);
		maxSpinner = new Spinner(group, SWT.NONE);
		maxButton = new Button(group, SWT.CHECK);

		// Widgets initialisation
		activator.setSelection(false);
		minSpinner.setEnabled(false);
		maxSpinner.setEnabled(false);
		minButton.setEnabled(false);
		maxButton.setEnabled(false);
		minSpinner.setDigits(precision);
		maxSpinner.setDigits(precision);

		// FT5426 Spinner popup menus
		resetMinMenu = new Menu(Display.getDefault().getActiveShell(), SWT.POP_UP);
		resetMinMenuItem = new MenuItem(resetMinMenu, SWT.PUSH);
		resetMinMenuItem.setText("Reset");
		resetMinMenuItem.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				minSpinner.setSelection(iMinValue);
				minSpinner.notifyListeners(SWT.Selection, null);
			}
		});

		resetMaxMenu = new Menu(Display.getDefault().getActiveShell(), SWT.POP_UP);
		resetMaxMenuItem = new MenuItem(resetMaxMenu, SWT.PUSH);
		resetMaxMenuItem.setText("Reset");
		resetMaxMenuItem.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				maxSpinner.setSelection(iMaxValue);
				maxSpinner.notifyListeners(SWT.Selection, null);
			}
		});

		minSpinner.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {

				float newMinValue = minSpinner.getSelection() / scale;
				((MinMaxFilter) filterObject).setMinValue(newMinValue);
				_pcs.firePropertyChange(PlumeBean.PROPERTY_SIGNALFILTER, oldMinValue, newMinValue);
				oldMinValue = newMinValue;
				updateCustomStep();
			}
		});

		minSpinner.addListener(SWT.MouseDown, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (event.button == MouseEvent.BUTTON3) {
					Rectangle bounds = minSpinner.getBounds();
					Point point = group.toDisplay(bounds.x, bounds.y + bounds.height);
					resetMinMenu.setLocation(point);
					resetMinMenu.setVisible(true);
				}
			}
		});

		maxSpinner.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				float newMaxValue = maxSpinner.getSelection() / scale;
				((MinMaxFilter) filterObject).setMaxValue(newMaxValue);
				_pcs.firePropertyChange(PlumeBean.PROPERTY_SIGNALFILTER, oldMaxValue, newMaxValue);
				oldMaxValue = newMaxValue;
				updateCustomStep();
			}
		});

		maxSpinner.addListener(SWT.MouseDown, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (event.button == MouseEvent.BUTTON3) {
					Rectangle bounds = maxSpinner.getBounds();
					Point point = group.toDisplay(bounds.x, bounds.y + bounds.height);
					resetMaxMenu.setLocation(point);
					resetMaxMenu.setVisible(true);
				}
			}
		});

		activator.setText(label);

		activator.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				boolean active = activator.getSelection();
				filterObject.setActivity(active);
				minSpinner.setEnabled(active);
				maxSpinner.setEnabled(active);
				minButton.setEnabled(active);
				maxButton.setEnabled(active);

				_pcs.firePropertyChange(PlumeBean.PROPERTY_SIGNALFILTER, !active, active);
			}
		});

		minButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {

				boolean active = activator.getSelection();
				((MinMaxFilter) filterObject).setMinSaturation(minButton.getSelection());

				_pcs.firePropertyChange(PlumeBean.PROPERTY_SIGNALFILTER, !active, active);
			}
		});

		maxButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {

				boolean active = activator.getSelection();
				((MinMaxFilter) filterObject).setMaxSaturation(maxButton.getSelection());

				_pcs.firePropertyChange(PlumeBean.PROPERTY_SIGNALFILTER, !active, active);
			}
		});

		oldMinValue = minSpinner.getSelection();
		oldMaxValue = maxSpinner.getSelection();

		activator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		minButton.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true, false));
		minSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		maxSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		minButton.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
	}

	public void setMinMaxInterval(float minValue, float maxValue) {
		((MinMaxFilter) filterObject).setMinValue(minValue);
		((MinMaxFilter) filterObject).setMaxValue(maxValue);

		iMinValue = (int) (minValue * scale);
		iMaxValue = (int) (maxValue * scale);

		minSpinner.setMaximum(iMaxValue);
		minSpinner.setMinimum(iMinValue);
		minSpinner.setSelection(iMinValue);

		maxSpinner.setMaximum(iMaxValue);
		maxSpinner.setMinimum(iMinValue);
		maxSpinner.setSelection(iMaxValue);

		updateCustomStep();
	}

	/**
	 * Update the custom step for a couple of min and max spinners.
	 * 
	 * @param min
	 *            The min spinner
	 * @param max
	 *            The max spinner
	 */
	private void updateCustomStep() {
		double customStep = Math.abs(maxSpinner.getSelection() - minSpinner.getSelection()) / 100.0f;
		minSpinner.setIncrement((int) customStep);
		maxSpinner.setIncrement((int) customStep);
		minSpinner.setPageIncrement((int) customStep);
		maxSpinner.setPageIncrement((int) customStep);
	}

}
