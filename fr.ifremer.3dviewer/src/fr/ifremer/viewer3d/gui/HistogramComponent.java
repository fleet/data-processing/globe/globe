package fr.ifremer.viewer3d.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JComponent;

import fr.ifremer.viewer3d.layers.ContrastShadeHistogram;

public class HistogramComponent extends JComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int width;
	protected int height;
	protected int histogramSize = 255;

	public HistogramComponent() {
		this.width = 180;
		this.height = 100;

	}

	@Override
	public void paint(Graphics g) {
		if (ContrastShadeHistogram.getHistogram() == null) {
			// attendre la création de l'histogramme
			this.repaint();
		} else {
			int[] histogram = ContrastShadeHistogram.getHistogram();
			int borneMin = (int) ContrastShadeHistogram.getStaticMin();
			int borneMax = (int) ContrastShadeHistogram.getStaticMax();

			// bordure de l'histo
			g.setColor(Color.black);
			g.drawLine(0, height, 255 * width / histogramSize, height);

			g.drawLine(0, height, 0, 0);
			g.drawLine(255 * width / histogramSize, height, 255 * width / histogramSize, 0);

			g.setColor(Color.gray);
			g.drawLine(51 * width / histogramSize, height, 51 * width / histogramSize, 0);
			g.drawLine(102 * width / histogramSize, height, 102 * width / histogramSize, 0);
			g.drawLine(153 * width / histogramSize, height, 153 * width / histogramSize, 0);
			g.drawLine(204 * width / histogramSize, height, 204 * width / histogramSize, 0);

			// on determine le max de lhisto
			int max = 0;
			for (int i = 0; i < 256; i++) {
				if (histogram[i] > max) {
					max = histogram[i];
				}
			}
			max = max / height;
			if (max != 0) {
				// histo
				g.setColor(Color.darkGray);
				for (int i = 0; i < 256; i++) {
					g.drawLine(i * width / histogramSize, height - histogram[i] / max, (i + 1) * width / histogramSize, height - histogram[i] / max);
					if (i != 255) {
						g.drawLine((i + 1) * width / histogramSize, height - histogram[i] / max, (i + 1) * width / histogramSize, height - histogram[i + 1] / max);
					}
				}
			}
			g.setFont(new Font("Serif", Font.PLAIN, 10));
			g.drawString(String.valueOf(borneMin), 0, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 1 + borneMin), 51 * width / histogramSize - 5, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 2 + borneMin), 102 * width / histogramSize - 10, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 3 + borneMin), 153 * width / histogramSize - 10, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 4 + borneMin), 204 * width / histogramSize - 10, height + 10);
			g.drawString(String.valueOf(borneMax), 255 * width / histogramSize - 10, height + 10);
		}
	}

	/**
	 * Return the ideal size that the knob would like to be.
	 * 
	 * @return the preferred size of the JKnob.
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width + 30, height + 10);
	}

	/**
	 * Return the minimum size that the knob would like to be. This is the same
	 * size as the preferred size so the knob will be of a fixed size.
	 * 
	 * @return the minimum size of the JKnob.
	 */
	@Override
	public Dimension getMinimumSize() {
		return new Dimension(width + 30, height + 10);
	}
}
