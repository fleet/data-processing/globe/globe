package fr.ifremer.viewer3d.gui;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.ui.utils.Messages;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.StatusBar;
import gov.nasa.worldwind.util.WWMath;

/**
 * Overrides WWJ StatusBar with SonarScope3D's specific data.
 */
public class SSVStatusBar extends StatusBar {

	private static final long serialVersionUID = 1L;

	public SSVStatusBar() {
		super();
		manageOnlineMode();
	}

	@Override
	protected String makeCursorElevationDescription(double metersElevation) {
		String s;
		String elev = Logging.getMessage("term.Elev");
		if (UNIT_IMPERIAL.equals(getElevationUnit())) {
			s = String.format(elev + " %,7d feet", (int) (WWMath.convertMetersToFeet(metersElevation)));
		} else {
			// Default to metric units.
			s = String.format("%s %,7.2f %s", elev, metersElevation, "meters");
		}
		return s;
	}

	@Override
	protected String makeAngleDescription(String label, Angle angle) {
		switch (label) {
		case "Lat":
			return FormatLatitudeLongitude.latitudeToString(angle.degrees);
		case "Lon":
			return FormatLatitudeLongitude.longitudeToString(angle.degrees);
		default:
			return super.makeAngleDescription(label, angle);
		}
	}

	/**
	 * Hook a listener on the network status in status bar.<br>
	 * Switch to mode "on line" when user click on "No network" label.
	 */
	private void manageOnlineMode() {
		// At this point, JLabel heartBeat is created and text is "Downloading". See super constructor.
		for (int i = 0; i < getComponentCount(); i++) {
			Component childComponent = getComponent(i);
			if (childComponent instanceof JLabel childLabel
					&& Logging.getMessage("term.Downloading").equals(childLabel.getText())) {
				// Label "Downloading" found. Hook a click listener
				childLabel.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						e.consume();
						// Reacts only when "No Network"
						if (WorldWind.isOfflineMode() && Messages.openSyncQuestionMessage("No network",
								"Connection to retrieve background layer is disbaled. Try to switch to 'online' mode ?",
								"Try connection", "Cancel") == 0)
							WorldWind.setOfflineMode(false);
					}
				});
			}
		}
	}

}
