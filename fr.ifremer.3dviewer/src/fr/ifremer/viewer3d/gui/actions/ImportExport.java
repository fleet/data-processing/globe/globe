package fr.ifremer.viewer3d.gui.actions;

import fr.ifremer.viewer3d.util.conf.SessionConfiguration.Zone;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.util.Logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.filechooser.FileFilter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Superclass for import/export actions.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public abstract class ImportExport extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** File extension for import/export files. */
	public final static String S3D_EXT = ".s3d";

	/**
	 * File filter for s3d data files.
	 */
	public static final class S3DFileFilter extends FileFilter {

		private static String description = "SonarScope data files (" + S3D_EXT + ")";

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}

				String name = f.getName();
				if (name != null && name.endsWith(S3D_EXT)) {
					return true;
				}
			}
			return false;
		}
	}

	@XStreamAlias("Layer")
	public static class LayerDef {
		private List<LayerDef> children = new LinkedList<LayerDef>();

		private String macroLayerName;

		private String filename;

		protected LayerDef() {
		}

		public LayerDef(String macroLayerName, String filename) {
			this();
			this.macroLayerName = macroLayerName;
			this.filename = filename;
		}

		public String getMacroLayerName() {
			return macroLayerName;
		}

		public String getFilename() {
			return filename;
		}

		/**
		 * List of children layers.
		 * <p>
		 * Returned list isn't modifiable.
		 * </p>
		 * 
		 * @see #addLayer(LayerDef)
		 */
		public List<LayerDef> getLayers() {
			return Collections.unmodifiableList(children);
		}

		/**
		 * Add a new layer to this layers map.
		 * 
		 * @param layer
		 *            layer file name
		 */
		public void addLayer(LayerDef layer) {
			if (layer != null && !children.contains(layer)) {
				this.children.add(layer);
			}
		}
	}

	/**
	 * Class for layer configuration.
	 */
	@XStreamAlias("LayerConfiguration")
	public static class LayerConfiguration extends LayerDef {

		/**
		 * Extension for this file.
		 */
		public static final String EXTENSION = ".idx";

		/**
		 * Loads configuration from a configuration file.
		 * 
		 * @return loaded instance or null if loading fails.
		 */
		public static LayerConfiguration load(File file) {
			if (file != null && file.exists()) {
				try {
					// loads data from XML
					XStream xstream = new XStream();
					xstream.autodetectAnnotations(true);
					xstream.processAnnotations(LayerConfiguration.class);
					// required for osgi plugins (has its own classloader)
					xstream.setClassLoader(LayerConfiguration.class.getClassLoader());

					InputStream fis = new FileInputStream(file);
					LayerConfiguration value = (LayerConfiguration) xstream.fromXML(fis);
					return value;

				} catch (XStreamException e) {
					Logging.logger().severe("Error during layers configuration loading " + file.getAbsolutePath());
				} catch (FileNotFoundException e) {
					Logging.logger().severe("File not found during layers configuration loading " + file.getAbsolutePath());
				}
			} else {
				Logging.logger().fine("Configuration file doesn't exist yet.");
			}

			return null;
		}

		/**
		 * Saves this configuration into specified file.
		 * 
		 * @param file
		 *            the file where session configuration should be written.
		 */
		public void save(File file) {
			if (file != null) {
				XStream xstream = new XStream();
				xstream.autodetectAnnotations(true);

				String xml = xstream.toXML(this);

				try {
					FileWriter fw = new FileWriter(file);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(xml);
					bw.close();
				} catch (IOException e) {
					Logging.logger().severe("Error while writing layers configuration file " + file.getPath());
				}
			}
		}

		/** Remarkable points. */
		private List<Position> points = new ArrayList<Position>();

		/** Remarkable zones. */
		private List<Zone> zones = new ArrayList<Zone>();

		public void addPoint(Position pos) {
			if (pos != null) {
				points.add(pos);
			}
		}

		public List<Position> getPoints() {
			return points;
		}

		public void addZone(Zone zone) {
			if (zone != null) {
				zones.add(zone);
			}
		}

		public List<Zone> getZones() {
			return zones;
		}
	}

	/**
	 * Ctor.
	 * 
	 * @param name
	 *            action name
	 * @param icon
	 *            action icon
	 */
	public ImportExport(String name, Icon icon) {
		super(name, icon);
	}
}