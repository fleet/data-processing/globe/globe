package fr.ifremer.viewer3d.gui.actions;

import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class CameraTranslationAction implements PropertyChangeListener {

	private static Position initialCenterPos;
	private static double initialElevation;
	private static Double cameraElevation = 1000.0;

	public static final String PROPERTY_ENDANIMATING = "endAnimating";

	public CameraTranslationAction() {
		init();
	}

	public static void activeCameraElevation(boolean active) {
		WorldWindow wwd = Viewer3D.getWwd();
		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();

			if (active) {
				// Stop iterators first
				view.stopAnimations();

				// Save initial parameters
				initialCenterPos = view.getCenterPosition();
				initialElevation = view.getCenterPosition().getElevation();
			} else {
				// Stop iterators first
				view.stopAnimations();
				view.setCenterPosition(initialCenterPos);
			}
		}
	}

	public static void changeElevation2(int elevation) {
		WorldWindow wwd = Viewer3D.getWwd();
		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();

			// Stop iterators first
			view.stopAnimations();
			initialCenterPos = view.getCenterPosition();
			double newElevation = view.getCenterPosition().getElevation();

			// TEST
			cameraElevation = newElevation + elevation;
			System.out.println("Camera elevation: " + cameraElevation);
			// FIN TEST

			Position newCenterPos = new Position(initialCenterPos.getLatitude(), initialCenterPos.getLongitude(),
					newElevation + elevation);
			view.setCenterPosition(newCenterPos);

			wwd.redraw();

			view.firePropertyChange(CameraTranslationAction.PROPERTY_ENDANIMATING, null, null);
		}
	}

	public static String getCameraElevation() {
		return cameraElevation.toString();
	}

	public static void go(Angle latitude, Angle longitude, Angle heading, Angle pitch, double altitude) {
		WorldWindow wwd = Viewer3D.getWwd();
		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();

			double correctAltitude = altitude * (2 - Math.sin((((90 - pitch.getDegrees()) * Math.PI) / 180.0f)));
			Position pos = new Position(latitude, longitude, correctAltitude);

			view.addPanToAnimator(pos, heading, pitch, pos.getElevation(), 10000, true);
		}
	}

	public static void changeElevation(int elevation) {
		WorldWindow wwd = Viewer3D.getWwd();
		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();

			// Stop iterators first
			view.stopAnimations();
			initialCenterPos = view.getCenterPosition();

			Position newCenterPos = new Position(initialCenterPos.getLatitude(), initialCenterPos.getLongitude(),
					initialElevation + elevation);
			view.setCenterPosition(newCenterPos);

			wwd.redraw();
		}

	}

	public void init() {
		WorldWindow wwd = Viewer3D.getWwd();
		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();
			view.addPropertyChangeListener(this);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event != null) {
			WorldWindow wwd = Viewer3D.getWwd();
			if (wwd.getView() instanceof BasicOrbitView) {
				BasicOrbitView view = (BasicOrbitView) wwd.getView();

				if (event.getPropertyName().equals(AVKey.VIEW)) {
					if (!view.isAnimating()) {
						view.firePropertyChange(CameraTranslationAction.PROPERTY_ENDANIMATING, null, null);
					}
				} else if (event.getPropertyName().equals(CameraTranslationAction.PROPERTY_ENDANIMATING)) {
					// System.out.println("Fin Animation");

					// TEST
					// Position eyePos = view.getCurrentEyePosition();
					//
					// // while(Math.round(eyePos.getAltitude()) !=
					// Math.round(altitude))
					// // {
					// // if(eyePos.getAltitude() < altitude)
					// // correctAltitude += 1;
					// // else
					// // correctAltitude -= 1;
					// //
					// // view.addPanToAnimator(pos, heading, pitch,
					// correctAltitude, 2000, true);
					//
					// System.out.println("Altitude = " + eyePos.getAltitude());
					//
					// if(Math.round(eyePos.getAltitude()) <
					// Math.round(newAltitude))
					// {
					// view.addPanToAnimator(eyePos, view.getHeading(),
					// view.getPitch(), eyePos.getAltitude() + 1.0f, 100, true);
					//
					// }
					// if(Math.round(eyePos.getAltitude()) >
					// Math.round(newAltitude))
					// {
					// view.addPanToAnimator(eyePos, view.getHeading(),
					// view.getPitch(), eyePos.getAltitude() - 1.0f, 100, true);
					// }
					// }
					// FIN TEST
				}
			}
		}
	}
}
