/*
 * @License@
 */
package fr.ifremer.viewer3d.gui.actions;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import gov.nasa.worldwind.wms.WMSTiledImageLayer;

/**
 * This action listener provides methods to zoom to layers.
 * <p>
 * The layers should implement {@link ITarget} or it could be a {@link WMSTiledImageLayer}.
 * </p>
 *
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 * 
 * @deprecated use {@link IGeographicViewService} methods instead.
 */
@Deprecated
public class GoToActionUtil {

	/**
	 * Limit of the zoom elevation.
	 */
	private static final double elevationLimit = 10000;

	public static void zoomToSector(ITarget target) {
		if (target != null) {
			Sector sector = target.getSector();
			if (sector != null && !sector.equals(Sector.EMPTY_SECTOR)) {
				zoomToSector(sector);
			}
		}
	}

	public static void zoomToSector(Sector s) {

		WorldWindow wwd = Viewer3D.getWwd();

		if (s == null) {
			return;
		}

		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();

			LatLon latLon = GeoBoxUtils.getCentroid(s);

			// Stop all animations on the view, and start a 'pan to' animation.
			view.stopAnimations();

			LatLon[] corners = s.getCorners();
			double dist = LatLon.ellipsoidalDistance(corners[0], corners[2], 6378137, 6356752.3); // consts
			// from
			// wikipedia
			double elevation = dist;
			if (Double.isNaN(elevation)) {
				elevation = 1000;
			}

			view.addPanToAnimator(new Position(latLon, 0), view.getHeading(), view.getPitch(), elevation);
		}
	}

	/**
	 * zooms to a desired position. If the elevation is less than 10 000 then the elevation doesn't change. If it is
	 * higher than it zooms at an elevation of 10 000m.
	 *
	 * @param latlon
	 * @param wwd
	 */
	public static void zoomToPosition(LatLon latlon, WorldWindow wwd) {

		double elevation;

		if (latlon == null) {
			return;
		}

		if (wwd.getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) wwd.getView();
			if (wwd.getView().getCurrentEyePosition().getElevation() < elevationLimit) {
				elevation = wwd.getView().getCurrentEyePosition().getElevation();
			} else {
				elevation = elevationLimit;
			}

			// Stop all animations on the view, and start a 'pan to' animation.
			view.stopAnimations();
			// LRT has changed the elevation value from 100000 to 10000
			// (2010/01/25)
			view.addPanToAnimator(new Position(latlon, 0), view.getHeading(), view.getPitch(), elevation);
		}
	}

	public static void zoomToPosition(Position p) {
		double elevation;
		if (Viewer3D.getWwd().getView() instanceof BasicOrbitView) {
			BasicOrbitView view = (BasicOrbitView) Viewer3D.getWwd().getView();
			if (Viewer3D.getWwd().getView().getCurrentEyePosition().getElevation() < elevationLimit) {
				elevation = Viewer3D.getWwd().getView().getCurrentEyePosition().getElevation();
			} else {
				elevation = elevationLimit;
			}

			// Stop all animations on the view, and start a 'pan to' animation.
			view.stopAnimations();
			view.addPanToAnimator(p, view.getHeading(), view.getPitch(), elevation);
		}
	}

}
