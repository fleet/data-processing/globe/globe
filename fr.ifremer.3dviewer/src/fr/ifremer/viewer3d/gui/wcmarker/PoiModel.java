package fr.ifremer.viewer3d.gui.wcmarker;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import jakarta.inject.Inject;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWaterColumnVolumicLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWwLayerOperation;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.Result;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import io.reactivex.subjects.PublishSubject;

/**
 *
 * this class contains all the WC Point Of Interest object for water column
 * <p>
 * they can be serialized/read from a file
 */
public class PoiModel {

	/** Format names of configuration file */
	@XStreamOmitField
	public static final String[] CONFIGURATION_FORMAT_NAMES = new String[] { "Extensible Markup Language", "XML" };
	@XStreamOmitField
	/** Format extension of configuration file */
	public static final String[] CONFIGURATION_FORMAT_EXTENSIONS = new String[] { "*.xml" };

	/***
	 * Map maintaining links between file name and associated WCVolumicLayers
	 */
	@XStreamOmitField
	private HashMap<String, WCVolumicLayer> map = new HashMap<>();

	/**
	 * {@link PublisherSubject} for event in this composite
	 */
	PublishSubject<PoiList> publisher = PublishSubject.create();

	public PublishSubject<PoiList> getPublisher() {
		return publisher;
	}

	/**
	 * the current list of point of interest
	 */
	private PoiList current = new PoiList();

	public void add(PoiData value) {
		current.add(value);
		publishCurrentState();
	}

	/**
	 * load FilterParameters from a file
	 *
	 * @param file the file to load
	 * @throws GIOException
	 */
	public void load(File file) throws GIOException {

		XStream xstream = new XStream();
		xstream.ignoreUnknownElements();
		xstream.setClassLoader(getClass().getClassLoader());
		try (ObjectInputStream in = xstream.createObjectInputStream(new FileReader(file))) {
			Object o = in.readObject();
			PoiList read = (PoiList) o;
			current = read;
			publisher.onNext(current);

		} catch (Exception e) {
			throw new GIOException("Error while parsing file" + e.getMessage(), e);
		}
	}

	/**
	 * load FilterParameters from a file
	 *
	 * @param file the file to load
	 * @throws GIOException
	 */
	public void save(File outputFile) throws GIOException {
		XStream xstream = new XStream();
		// read the file
		xstream.autodetectAnnotations(true);
		xstream.ignoreUnknownElements();
		try (ObjectOutputStream out = xstream.createObjectOutputStream(new FileWriter(outputFile), "WC_POI");) {
			out.writeObject(current);
		} catch (IOException e) {
			throw new GIOException("Error while saving file " + e.getMessage(), e);
		}
	}

	/**
	 * publish the current PoiList
	 */
	public void publishCurrentState() {
		publisher.onNext(current);
	}

	/**
	 * Remove a PoiData from the model
	 */
	public void remove(PoiData data) {
		current.poi.remove(data);
		publishCurrentState();
	}

	/**
	 * retrieve and load the given point of interest
	 */
	public Result retrieve(PoiData data) {
		String shortFileName = data.shortFileName;
		WCVolumicLayer layer = map.get(shortFileName);

		if (layer == null) {
			// cannot find layer
			return Result.failed("Cannot find a wc layer associated with " + shortFileName);
		}
		if (layer.isLayerActive()) {
			Result res = layer.activate(true);
			if (!res.isSuccess()) {
				return Result.failed("Failed to activate WC layer (" + res.getErrorMessage() + ")");
			}
		}
		// continue
		layer.load(data.player, data.display, data.filters, data.buffer);
		return Result.success();
	}

	/** React on a layer model changes */
	protected void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		if (event.getState() == FileLayerStoreState.ADDED || event.getState() == FileLayerStoreState.UPDATED) {
			onLayersUpdated(event.getFileLayerStore().getLayers());
		} else if (event.getState() == FileLayerStoreState.REMOVED) {
			onLayersDeleted(event.getFileLayerStore().getLayers());
		}
	}

	protected void onLayersUpdated(List<IWWLayer> layers) {
		for (IWWLayer layer : layers) {
			if (layer != null) {
				layer.perform(new BasicWwLayerOperation() {
					/** {@inheritDoc} */
					@Override
					public BasicWwLayerOperation accept(IWWWaterColumnVolumicLayer wwWcLayer) {
						WCVolumicLayer wclayer = (WCVolumicLayer) wwWcLayer;
						String fileName = wclayer.getInfo().getPath();
						String shortName = FileUtils.getFileShortNameWithExtension(fileName);
						map.put(shortName, wclayer);
						return this;
					}
				});
			}
		}
	}

	protected void onLayersDeleted(List<IWWLayer> layers) {
		for (IWWLayer layer : layers) {
			if (layer != null) {
				layer.perform(new BasicWwLayerOperation() {
					/** {@inheritDoc} */
					@Override
					public BasicWwLayerOperation accept(IWWWaterColumnVolumicLayer wwWcLayer) {
						WCVolumicLayer wclayer = (WCVolumicLayer) wwWcLayer;
						String fileName = wclayer.getInfo().getPath();
						String shortName = FileUtils.getFileShortNameWithExtension(fileName);
						map.remove(shortName);
						return this;
					}
				});
			}
		}
	}

	static class PoiList implements Serializable {
		/**
		 */
		private static final long serialVersionUID = -6351461078919865347L;
		private List<PoiData> poi = new ArrayList<>();

		protected List<PoiData> get() {
			return Collections.unmodifiableList(poi);
		}

		public void add(PoiData value) {
			poi.add(value);
		}
	}

	/**
	 * @param fileLayerStoreModel the {@link #fileLayerStoreModel} to set
	 */
	@Inject
	public void fitFileLayerStoreModel(WWFileLayerStoreModel fileLayerStoreModel) {
		fileLayerStoreModel.addListener(this::onFileLayerStoreEvent);
	}

}
