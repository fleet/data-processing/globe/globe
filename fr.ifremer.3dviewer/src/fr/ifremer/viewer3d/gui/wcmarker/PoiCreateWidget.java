package fr.ifremer.viewer3d.gui.wcmarker;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.viewer3d.application.context.ContextInitializer;

public class PoiCreateWidget extends TitleAreaDialog {

	protected Object result;
	protected Shell shell;
	private Text txtComment;
	private PoiData initial;
	private Text textId;
	private Text textTypo;

	/**
	 * Create the dialog.
	 *
	 * @param parent
	 * @param style
	 */
	public PoiCreateWidget(Shell parent, PoiData sourceData) {
		super(parent);
		initial = sourceData;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Add new WC Point Of Interest");
	}

	/**
	 * Create contents of the dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		area.setLayout(new GridLayout(1, false));
		area.setLayoutData(new GridData(GridData.FILL_BOTH));

		Label lblNewLabel = new Label(area, SWT.NONE);
		lblNewLabel.setText(initial.shortFileName);

		Composite composite = new Composite(area, SWT.NONE);
		composite.setLayout(new GridLayout(2, true));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));

		Label lblNewLabel_1 = new Label(composite, SWT.NONE);
		lblNewLabel_1.setText("Id");

		textId = new Text(composite, SWT.BORDER);
		textId.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblNewLabel_2 = new Label(composite, SWT.NONE);
		lblNewLabel_2.setText("Typo");

		textTypo = new Text(composite, SWT.BORDER);
		textTypo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		txtComment = new Text(composite, SWT.BORDER | SWT.MULTI);
		txtComment.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 2, 1));
		txtComment.setText("comments");

		return parent;
	}

	@Override
	protected void okPressed() {
		ContextInitializer.getInContext(PoiModel.class)
				.add(initial.from(textTypo.getText(), textId.getText(), txtComment.getText()));
		super.okPressed();
	}
}
