package fr.ifremer.viewer3d.gui.wcmarker;

import java.io.File;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.gui.wcmarker.PoiModel.PoiList;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;

public class PoiView {

	public static final String ID = "fr.ifremer.viewer3d.gui.wcPoi.PoiView"; //$NON-NLS-1$
	private TableViewer tableViewer;
	private Disposable observer;

	@Inject
	private PoiModel poiModel;

	public class PoiDataLegendProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public String getColumnText(Object element, int columnIndex) {
			if (element instanceof PoiData) {
				PoiData data = (PoiData) element;
				switch (columnIndex) {
				case 0:
					return data.shortFileName;
				case 1:
					return data.typology;
				case 2:
					return data.id;
				case 3:
					return data.comment;
				default:
					return "";
				}
			}
			return "";
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
	}

	@PreDestroy
	public void dispose() {
		observer.dispose();
	}

	/**
	 * Create contents of the view part.
	 *
	 * @param parent
	 */
	@PostConstruct
	public void postConstruct(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1, false));

		Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayout(new GridLayout(2, false));
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		tableViewer = new TableViewer(composite_1, SWT.BORDER);
		Table table_1 = tableViewer.getTable();
		table_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableViewer.getTable().setLinesVisible(true);
		tableViewer.getTable().setHeaderVisible(true);

		tableViewer.getTable().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.DEL) {
					// retrieve selected element
					PoiData selection = getSelectedData();
					if (selection != null) {
						poiModel.remove(selection);
					}
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		TableViewerColumn fileNameColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		fileNameColumn.getColumn().setText("file name");

		TableViewerColumn typoColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		typoColumn.getColumn().setText("typo");

		TableViewerColumn idColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		idColumn.getColumn().setText("Id");

		TableViewerColumn commentColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		commentColumn.getColumn().setText("comment");

		tableViewer.setLabelProvider(new PoiDataLegendProvider());
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());

		Composite composite = new Composite(composite_1, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1));

		Group grpPoiMngt = new Group(composite, SWT.NONE);
		grpPoiMngt.setText("POI mngt");
		grpPoiMngt.setLayout(new GridLayout(1, false));

		Button btnRetrieveButton = new Button(grpPoiMngt, SWT.NONE);
		btnRetrieveButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnRetrieveButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PoiData data = getSelectedData();
				if (data != null) {
					poiModel.retrieve(data);
				}
			}
		});
		btnRetrieveButton.setText("retrieve");

		Button btnDelete = new Button(grpPoiMngt, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PoiData data = getSelectedData();
				if (data != null) {
					poiModel.remove(data);
				}
			}

		});
		btnDelete.setText("delete");

		Group grpFileMngt = new Group(composite, SWT.NONE);
		grpFileMngt.setText("file mngt");
		grpFileMngt.setLayout(new GridLayout(1, false));

		Button btnSave = new Button(grpFileMngt, SWT.NONE);
		btnSave.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(container.getShell(), SWT.SAVE);

				dialog.setFilterNames(PoiModel.CONFIGURATION_FORMAT_NAMES);
				dialog.setFilterExtensions(PoiModel.CONFIGURATION_FORMAT_EXTENSIONS);

				final String path = dialog.open();

				// Create survey from xml session file
				if (path != null) {
					File outputFile = new File(path);
					try {
						poiModel.save(outputFile);
					} catch (GIOException e1) {
						MessageDialog.openError(container.getShell(), "Error saving file" + outputFile,
								"Error saving file : (error message:" + e1.getMessage() + ")");
					}

				} else {
					return;
				}

			}
		});
		btnSave.setText("save file");

		Button btnLoad = new Button(grpFileMngt, SWT.NONE);
		btnLoad.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnLoad.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(container.getShell(), SWT.OPEN);

				dialog.setFilterNames(PoiModel.CONFIGURATION_FORMAT_NAMES);
				dialog.setFilterExtensions(PoiModel.CONFIGURATION_FORMAT_EXTENSIONS);

				final String path = dialog.open();

				// Create survey from xml session file
				if (path != null) {
					File outputFile = new File(path);
					try {
						poiModel.load(outputFile);
					} catch (GIOException e1) {
						MessageDialog.openError(container.getShell(), "Error while loading file" + outputFile,
								"error message:" + e1.getMessage() + ")");
					}

				} else {
					return;
				}
			}
		});
		btnLoad.setText("load file");

		observer = poiModel.getPublisher().toFlowable(BackpressureStrategy.LATEST).subscribe(model -> {
			populate(model);
		});
		poiModel.publishCurrentState();

	}

	private PoiData getSelectedData() {
		// retrieve selected element
		ISelection selectedElement = tableViewer.getSelection();
		if (selectedElement instanceof StructuredSelection) {
			StructuredSelection sel = (StructuredSelection) selectedElement;

			if (sel.getFirstElement() instanceof PoiData) {
				PoiData data = (PoiData) sel.getFirstElement();
				return data;
			}
		}
		return null;
	}

	private void populate(PoiList current) {
		tableViewer.setInput(current.get());
		for (TableColumn tableColumn : tableViewer.getTable().getColumns()) {
			tableColumn.pack();
		}

	}
}
