package fr.ifremer.viewer3d.gui.wcmarker;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.SlidingBufferSetting;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerData;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters;
/**
 * A point of interest in water column
 * contains data needed to be able to reload it and be able to display it again
 * */
public class PoiData {

	String shortFileName;

	String typology;

	String id;

	String comment;

	public final VolumicRendererDisplayParameters display;

	public final IndexedPlayerData player;

	public final FilterParameters filters;

	public final SlidingBufferSetting buffer;


	public PoiData(String fileName, String typology, String id, String comment,
			VolumicRendererDisplayParameters display, IndexedPlayerData player, FilterParameters filters, SlidingBufferSetting buffer
			) {
		super();
		this.shortFileName = FileUtils.getFileShortNameWithExtension(fileName);
		this.typology = typology;
		this.id = id;
		this.comment = comment;
		this.display = display;
		this.player = player;
		this.filters = filters;
		this.buffer=buffer;
	}



	/**
	 * return a new {@link PoiData} as a copy of this one, and a new typology,id and comment
	 * */
	public PoiData from(String typology, String id, String newComment)
	{
		return new PoiData(shortFileName, typology,id,newComment, display, player, filters,buffer);
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buffer == null) ? 0 : buffer.hashCode());
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((display == null) ? 0 : display.hashCode());
		result = prime * result + ((filters == null) ? 0 : filters.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		result = prime * result + ((shortFileName == null) ? 0 : shortFileName.hashCode());
		result = prime * result + ((typology == null) ? 0 : typology.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoiData other = (PoiData) obj;
		if (buffer == null) {
			if (other.buffer != null)
				return false;
		} else if (!buffer.equals(other.buffer))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (display == null) {
			if (other.display != null)
				return false;
		} else if (!display.equals(other.display))
			return false;
		if (filters == null) {
			if (other.filters != null)
				return false;
		} else if (!filters.equals(other.filters))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		if (shortFileName == null) {
			if (other.shortFileName != null)
				return false;
		} else if (!shortFileName.equals(other.shortFileName))
			return false;
		if (typology == null) {
			if (other.typology != null)
				return false;
		} else if (!typology.equals(other.typology))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "PoiData [shortFileName=" + shortFileName + ", typology=" + typology + ", id=" + id + ", comment="
				+ comment + ", display=" + display + ", player=" + player + ", filters=" + filters + ", buffer="
				+ buffer + "]";
	}










}
