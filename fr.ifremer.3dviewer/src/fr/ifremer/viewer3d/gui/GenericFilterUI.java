//REQ-IHM-004 - Markers Filtering
/**
 * Base class for all filter UI class
 */

package fr.ifremer.viewer3d.gui;

import java.beans.PropertyChangeSupport;

import org.eclipse.swt.widgets.Group;

import fr.ifremer.viewer3d.layers.plume.filter.GenericFilter;

public abstract class GenericFilterUI {

	protected Group group;
	protected String label;
	protected String name;
	protected GenericFilter filterObject;

	protected transient PropertyChangeSupport _pcs;

	public GenericFilterUI(Group group, String label, String name, PropertyChangeSupport pcs) {
		this.group = group;
		this.label = label;
		this._pcs = pcs;
		this.name = name;
		filterObject = null;
	}

	public GenericFilter getFilterObject() {
		return filterObject;
	}
}
