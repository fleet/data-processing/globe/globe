package fr.ifremer.viewer3d.gui.markereditor;

import java.util.List;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.viewer3d.layers.markers.MarkerController;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;

/**
 * Handler to open the {@link MarkerEditor}.
 */
public class MarkerEditorHandler extends AbstractNodeHandler {

	/** Id of command defined in fragment.e4xmi */
	public static final String COMMAND_OPEN_MARKER_EDITOR = "fr.ifremer.globe.ui.projectexplorer.command.openWithMarkerEditor";

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		Object[] selection = getSelection(selectionService);
		List<FileInfoNode> nodeList = getSelectionAsList(selection,
				contentType -> contentType == ContentType.MARKER_CSV);
		return selection.length == 1 && nodeList.size() == selection.length;
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Execute
	@SuppressWarnings("restriction")
	public void execute(ECommandService commandService, ESelectionService selectionService,
			MarkerController markerController) {

		Object[] selection = getSelection(selectionService);
		if (selection != null) {
			List<FileInfoNode> nodeList = getSelectionAsList(selection,
					contentType -> contentType == ContentType.MARKER_CSV);
			if (selection.length == 1 && nodeList.size() == selection.length) {
				markerController.setActiveMarkerLayer(
						(WWMarkerLayer) ((LayerNode) nodeList.get(0).getChildren().get(0)).getLayer().get());
			}
		}

		Display.getDefault().asyncExec(() -> {
			// Lookup commmand with its ID
			Command command = commandService.getCommand("fr.ifremer.3dviewer.command.showmarkereditor");
			try {
				command.executeWithChecks(new ExecutionEvent());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
	}
}
