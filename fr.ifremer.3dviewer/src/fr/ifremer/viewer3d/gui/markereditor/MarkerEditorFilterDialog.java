package fr.ifremer.viewer3d.gui.markereditor;

import java.util.List;
import java.util.Optional;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.typology.model.Typology;
import fr.ifremer.globe.typology.ui.TypologyCombobox;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;

public class MarkerEditorFilterDialog extends Dialog {

	/** Inputs **/
	private final TableViewer tableViewer;
	private final WWMarkerLayer<? extends IMarker> markerLayer;

	/** Inner composites **/
	private Text textComment;
	private Button typologyCheckbox;
	private Button commentCheckbox;
	private TypologyCombobox comboViewerTypology;
	private Button btnSetEnabled;
	private Button btnUseColorOf;
	private ColorPicker colorPicker;

	/**
	 * Creates the dialog.
	 */
	public MarkerEditorFilterDialog(TableViewer tableViewer, WWMarkerLayer<?> markerLayer) {
		super(Display.getDefault().getActiveShell());
		setShellStyle(SWT.CLOSE | SWT.APPLICATION_MODAL | SWT.RESIZE);
		this.markerLayer = markerLayer;
		this.tableViewer = tableViewer;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Marker filter");
		shell.setSize(600, 350);
	}

	/**
	 * Creates contents of the dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout glContainer = new GridLayout(1, false);
		glContainer.marginWidth = 10;
		glContainer.marginHeight = 10;
		container.setLayout(glContainer);

		Group grpFilterOptions = new Group(container, SWT.NONE);
		grpFilterOptions.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpFilterOptions.setLayout(new GridLayout(2, false));
		grpFilterOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpFilterOptions.setText("Filter options");

		typologyCheckbox = new Button(grpFilterOptions, SWT.CHECK);
		typologyCheckbox.setText("Typologie");
		comboViewerTypology = new TypologyCombobox(grpFilterOptions, SWT.NONE | SWT.READ_ONLY, typology -> {
		});
		Combo combo = comboViewerTypology.getCombo();
		combo.setEnabled(false);
		typologyCheckbox.addListener(SWT.Selection, e -> combo.setEnabled(typologyCheckbox.getSelection()));
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		commentCheckbox = new Button(grpFilterOptions, SWT.CHECK);
		commentCheckbox.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		commentCheckbox.setText("Comment");
		textComment = new Text(grpFilterOptions, SWT.BORDER | SWT.MULTI);
		textComment.setEnabled(false);
		commentCheckbox.addListener(SWT.Selection, e -> textComment.setEnabled(commentCheckbox.getSelection()));
		textComment.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

		Group grpApply = new Group(container, SWT.NONE);
		grpApply.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpApply.setLayout(new GridLayout(3, false));
		grpApply.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpApply.setText("Apply on filtered markers");

		btnSetEnabled = new Button(grpApply, SWT.RADIO);
		btnSetEnabled.setText("Enable");
		btnSetEnabled.setSelection(true);

		Button btnSetDisabled = new Button(grpApply, SWT.RADIO);
		btnSetDisabled.setText("Disable");

		Button btnApplyFilter = new Button(grpApply, SWT.NONE);
		btnApplyFilter.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		btnApplyFilter.setText("Apply");
		btnApplyFilter.addListener(SWT.Selection, e -> applyEnable());

		// Set color
		Group grpSetColor = new Group(container, SWT.NONE);
		grpSetColor.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpSetColor.setLayout(new GridLayout(4, false));
		grpSetColor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpSetColor.setText("Apply color on filtered markers");

		btnUseColorOf = new Button(grpSetColor, SWT.RADIO);
		btnUseColorOf.setText("from Typology");
		btnUseColorOf.setSelection(true);

		Button btnColorPicker = new Button(grpSetColor, SWT.RADIO);
		btnColorPicker.setSelection(false);

		colorPicker = new ColorPicker(grpSetColor, SWT.BORDER, null, color -> {
			btnColorPicker.setSelection(true);
			btnUseColorOf.setSelection(false);
		});

		GridData gd_uniColorPicker = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_uniColorPicker.heightHint = 27;
		gd_uniColorPicker.widthHint = 132;
		colorPicker.setLayoutData(gd_uniColorPicker);

		// init typo combobox & color picker
		Optional.ofNullable(Typologies.getInstance().getSelectedTypology()).ifPresent(selectedTypology -> {
			comboViewerTypology.setSelection(new StructuredSelection(selectedTypology));
			if (btnUseColorOf.getSelection())
				colorPicker.setColor(selectedTypology.getGColor());
		});

		Button btnSetColor = new Button(grpSetColor, SWT.NONE);
		btnSetColor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		btnSetColor.setText("Apply");
		btnSetColor.addListener(SWT.Selection, e -> applyColor());

		return container;
	}

	/**
	 * @return filtered markers.
	 */
	private List<IMarker> getFilteredMarkers() {
		Optional<Typology> selectedTypology = comboViewerTypology.getSelectedTypology();
		return markerLayer.getIMarkers().stream()//
				.filter(marker -> //
				// check typology
				(!typologyCheckbox.getSelection() //
						|| selectedTypology.filter(selectedTypo -> marker.getTypology()
								.filter(selectedTypo::equalsOtherTypology).isPresent()).isPresent() //
						|| selectedTypology.get().getName().equals("NONE") && marker.getTypology().isEmpty())
						// check comment
						&& (!commentCheckbox.getSelection() || marker.getComment().contains(textComment.getText())))
				.map(IMarker.class::cast).toList();
	}

	/**
	 * Enables/disables filtered markers.
	 */
	private void applyEnable() {
		markerLayer.setMarkersEnabled(getFilteredMarkers(), btnSetEnabled.getSelection());
		markerLayer.refresh();
		tableViewer.refresh();
	}

	/**
	 * Apply color on filtered markers.
	 */
	private void applyColor() {
		getFilteredMarkers().forEach(marker -> {
			if (!btnUseColorOf.getSelection()) {
				marker.setColor(colorPicker.getColor());
			} else if (marker.getTypology().isPresent()) {
				// find typology
				var typo = Optional.ofNullable(Typologies.getInstance().findTypology(marker.getTypology().get()));
				// set color
				if (typo.isPresent()) {
					marker.setColor(typo.get().getGColor());
				}
			}
		});
		markerLayer.refresh();
		tableViewer.refresh();
	}

}
