package fr.ifremer.viewer3d.gui.markereditor;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.ui.TableViewerColumnSorter;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.PositionUtils;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;
import fr.ifremer.viewer3d.preference.Viewer3DPreferences;

public class MarkerEditorTableComposite<T extends IMarker> extends Composite {

	private static final Image CHECK_ICON = ResourceManager.getPluginImage("fr.ifremer.3dviewer", "icons/16/check.png");
	private static final Image UNCHECK_ICON = ResourceManager.getPluginImage("fr.ifremer.3dviewer",
			"icons/16/uncheck.png");

	/** Widgets **/
	private TableViewer tableViewer;

	/** Marker layer used to fill the table **/
	private WWMarkerLayer<T> markerLayer;

	/** Last time user clicked on checkbox **/
	private Instant lastClickOnCheckbox;

	/**
	 * Constructor
	 */
	public MarkerEditorTableComposite(Composite parent, int style, WWMarkerLayer<T> markerLayer) {
		super(parent, style);
		this.markerLayer = markerLayer;
		createUI();
	}

	/**
	 * Builds graphical interface.
	 */
	private void createUI() {
		setLayout(new GridLayout(9, false));

		Label lblLines = new Label(this, SWT.NONE);
		lblLines.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 7, 1));
		lblLines.setText("Markers");
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		// create table
		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		GridData gdTable = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdTable.horizontalSpan = 9;
		gdTable.widthHint = 350;
		table.setLayoutData(gdTable);
		tableViewer.setContentProvider(new ObservableListContentProvider<IMarker>());

		// create columns
		buildColmuns();
		tableViewer.setInput(markerLayer.getIMarkers());

		// set column order
		try {
			int[] order = getSavedColumnOrder();
			if (order.length > 1)
				tableViewer.getTable().setColumnOrder(order);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// hide/display columns
		TableColumn[] columns = tableViewer.getTable().getColumns();
		for (int columnIndex : getHiddenColumns()) {
			TableColumn col = columns[columnIndex];
			col.setWidth(0);
			col.setResizable(false);
		}

		// contextual menu : useful to select which columns to display or not
		Menu contextMenu = new Menu(tableViewer.getTable());
		tableViewer.getTable().setMenu(contextMenu);
		for (TableColumn col : tableViewer.getTable().getColumns()) {
			createMenuItem(contextMenu, col);
		}

		tableViewer.addDoubleClickListener(event -> {
			// avoid opening after click on checkbox
			if (lastClickOnCheckbox != null && Duration.between(lastClickOnCheckbox, Instant.now()).toSeconds() < 2)
				return;
			openMarkerEditDialog(((IStructuredSelection) event.getSelection()).getFirstElement());
			tableViewer.refresh();
		});

		Listener removeSelection = e -> {
			List<IMarker> markersToRemove = new ArrayList<>();
			for (TableItem item : table.getSelection())
				markersToRemove.add((IMarker) item.getData());
			markerLayer.getIMarkers().removeAll(markersToRemove);
		};
		// Add a listener to remove selected lines with "Delete" key
		table.addListener(SWT.KeyDown, e -> {
			if (e.keyCode == SWT.DEL)
				removeSelection.handleEvent(e);
		});

		table.addListener(SWT.Selection, e -> {
			var selectedMarkers = Stream.of(table.getSelection()).map(TableItem::getData).map(IMarker.class::cast)
					.toList();
			markerLayer.selectMarkers(selectedMarkers);
			Viewer3D.refreshRequired();
		});

		table.addListener(SWT.FocusOut, e -> {
			markerLayer.getIMarkers().asList().forEach(m -> m.setSelected(false));
			Viewer3D.refreshRequired();
		});

		// buttons
		Button btnEditr = new Button(this, SWT.NONE);
		btnEditr.setToolTipText("Edit selected marker");
		btnEditr.setText("Edit");
		btnEditr.addListener(SWT.Selection,
				e -> openMarkerEditDialog(tableViewer.getStructuredSelection().getFirstElement()));

		Button btnGoto = new Button(this, SWT.NONE);
		btnGoto.setToolTipText("Zoom on selected marker");
		btnGoto.setText("Go to");
		btnGoto.addListener(SWT.Selection, e -> {
			Object selection = tableViewer.getStructuredSelection().getFirstElement();
			if (selection != null)
				IGeographicViewService.grab().zoomTo(PositionUtils.getPosition((IMarker) selection));
		});

		Label sepatator = new Label(this, SWT.SEPARATOR);
		GridData gdSeperator = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdSeperator.heightHint = 25;
		sepatator.setLayoutData(gdSeperator);

		Button btnRemove = new Button(this, SWT.NONE);
		btnRemove.setText("Remove");
		btnRemove.setToolTipText("Remove selected markers");
		btnRemove.addListener(SWT.Selection, removeSelection);

		Button btnRemoveAll = new Button(this, SWT.NONE);
		btnRemoveAll.setText("Remove all");
		btnRemoveAll.setToolTipText("Remove all markers");
		btnRemoveAll.addListener(SWT.Selection, e -> markerLayer.getIMarkers().clear());

		Label sepatator2 = new Label(this, SWT.SEPARATOR);
		GridData gdSeperator2 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdSeperator2.heightHint = 25;
		sepatator2.setLayoutData(gdSeperator2);

		Button btnEnableAll = new Button(this, SWT.NONE);
		btnEnableAll.setToolTipText("Enable all markers");
		btnEnableAll.setText("Enable all");
		btnEnableAll.addListener(SWT.Selection, e -> {
			var markers = markerLayer.getIMarkers().stream().map(IMarker.class::cast).toList();
			markerLayer.setMarkersEnabled(markers, true);
			tableViewer.refresh();
		});

		Button btnDisabledAll = new Button(this, SWT.NONE);
		btnDisabledAll.setToolTipText("Disable all markers");
		btnDisabledAll.setText("Disable all");
		btnDisabledAll.addListener(SWT.Selection, e -> {
			var markers = markerLayer.getIMarkers().stream().map(IMarker.class::cast).toList();
			markerLayer.setMarkersEnabled(markers, false);
			tableViewer.refresh();
		});

		Button btnFilter = new Button(this, SWT.NONE);
		btnFilter.setText("Filter...");
		btnFilter.addListener(SWT.Selection, e -> new MarkerEditorFilterDialog(tableViewer, markerLayer).open());
	}

	/**
	 * Builds columns.
	 */
	protected void buildColmuns() {
		buildCheckboxColumn(IMarker::isEnabled, Optional.of("Enable"));
		buildColumn("ID", 40, IMarker::getID);
		buildColumn("Latitude", 120, marker -> LatLongFormater.latitudeToString(marker.getLat()));
		buildColumn("Longitude", 120, marker -> LatLongFormater.longitudeToString(marker.getLon()));
		buildColumn("Elevation (m)", IMarker.ELEVATION_LONG_NAME, 100,
				marker -> NumberUtils.getStringDouble(marker.getElevation()));
		buildColumn("Group", "Typology group", 120,
				marker -> marker.getTypology().isPresent() ? marker.getTypology().get().getGroup() : "");
		buildColumn("Class", "Typology class", 120,
				marker -> marker.getTypology().isPresent() ? marker.getTypology().get().getClazz() : "");
		buildColumn("Layer", 200, IMarker::getBathyLayer);
		buildColumn("Comment", 250, IMarker::getComment);

	}

	protected void buildColumn(String title, int size, Function<T, String> labelProvider) {
		buildColumn(title, Optional.empty(), size, labelProvider);
	}

	protected void buildColumn(String title, String tooltip, int size, Function<T, String> labelProvider) {
		buildColumn(title, Optional.of(tooltip), size, labelProvider);
	}

	@SuppressWarnings("unchecked")
	protected void buildColumn(String title, Optional<String> tooltip, int size, Function<T, String> labelProvider) {
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		tableViewerColumn.setLabelProvider(ColumnLabelProvider.createTextProvider(e -> labelProvider.apply((T) e)));

		TableColumn tableColumn = tableViewerColumn.getColumn();
		tableColumn.setWidth(size);
		tableColumn.setText(title);
		tableColumn.setMoveable(true);
		tooltip.ifPresent(tableColumn::setToolTipText);

		// sort
		new TableViewerColumnSorter(tableViewerColumn);

		// move listener
		tableColumn.addListener(SWT.Move, evt -> {
			Integer[] order = ArrayUtils.toObject(tableColumn.getParent().getColumnOrder());
			savePreference(getPreferenceNode().getColumnsOrder(), StringUtils.join(order, ","));
		});

	}

	@SuppressWarnings("unchecked")
	protected void buildCheckboxColumn(Function<T, Boolean> stateProvider, Optional<String> tooltip) {
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);

		// label provider is an image provider
		tableViewerColumn.setLabelProvider(ColumnLabelProvider.createTextImageProvider( //
				e -> "", // empty text
				e -> stateProvider.apply((T) e) ? CHECK_ICON : UNCHECK_ICON));

		tableViewerColumn.setEditingSupport(new CheckBoxColumnEditingSupport(tableViewer));

		TableColumn tableColumn = tableViewerColumn.getColumn();
		tableColumn.setWidth(25);
		tooltip.ifPresent(tableColumn::setToolTipText);
	}

	public class CheckBoxColumnEditingSupport extends EditingSupport {

		private TableViewer tableViewer;

		public CheckBoxColumnEditingSupport(TableViewer viewer) {
			super(viewer);
			this.tableViewer = viewer;
		}

		@Override
		protected CellEditor getCellEditor(Object o) {
			return new CheckboxCellEditor(null, SWT.CHECK);
		}

		@Override
		protected boolean canEdit(Object o) {
			return true;
		}

		@Override
		protected Object getValue(Object o) {
			return ((IMarker) o).isEnabled();
		}

		@Override
		protected void setValue(Object element, Object value) {
			lastClickOnCheckbox = Instant.now();
			var selectedMarkers = markerLayer.getIMarkers().stream().map(IMarker.class::cast)
					.filter(IMarker::isSelected).toList();
			var selectedItems = List.of(tableViewer.getTable().getItems()).stream()
					.filter(item -> selectedMarkers.contains(item.getData())).toList();
			if (selectedMarkers.contains(element)) {
				markerLayer.setMarkersEnabled(selectedMarkers, (Boolean) value);
				tableViewer.getTable().setSelection(selectedItems.toArray(new TableItem[0]));
			} else {
				markerLayer.setMarkersEnabled(List.of((IMarker) element), (Boolean) value);
			}
			tableViewer.refresh();
		}
	}

	private void createMenuItem(Menu parent, final TableColumn column) {
		final MenuItem itemName = new MenuItem(parent, SWT.CHECK);
		itemName.setText(column.getText());
		itemName.setSelection(column.getResizable());
		itemName.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (itemName.getSelection()) {
					column.setResizable(true);
					column.pack();
				} else {
					column.setWidth(0);
					column.setResizable(false);
				}
				saveHiddenColumns(column.getParent().getColumns());
			}

			private void saveHiddenColumns(TableColumn[] columns) {
				ArrayList<String> hiddenColumns = new ArrayList<>();
				for (int i = 0; i < columns.length; i++) {
					if (isColumnHidden(columns[i])) {
						hiddenColumns.add(String.valueOf(i));
					}
				}
				String hidden = StringUtils.join(hiddenColumns, ",");
				savePreference(getPreferenceNode().getHiddenColumns(), hidden);
			}
		});
	}

	private boolean isColumnHidden(TableColumn col) {
		return col.getWidth() == 0 && !col.getResizable();
	}

	/** Preferences **/

	private Viewer3DPreferences getPreferenceNode() {
		return Activator.getPluginParameters();
	}

	private int[] getPreferenceColumns(StringPreferenceAttribute attribut) {
		String pref = attribut.getValue();
		if ("".equals(pref)) {
			return new int[0];
		}
		String[] spref = pref.split(",");
		int[] result = new int[spref.length];
		for (int i = 0; i < spref.length; i++) {
			result[i] = Integer.parseInt(spref[i]);
		}
		return result;
	}

	private int[] getSavedColumnOrder() {
		return getPreferenceColumns(getPreferenceNode().getColumnsOrder());
	}

	private int[] getHiddenColumns() {
		return getPreferenceColumns(getPreferenceNode().getHiddenColumns());
	}

	private void savePreference(StringPreferenceAttribute attribut, String value) {
		attribut.setValue(value, true);
		getPreferenceNode().save();
	}

	private void openMarkerEditDialog(Object selection) {
		if (selection != null && selection instanceof IMarker marker)
			new MarkerEditDialog(marker).open();
	}
}
