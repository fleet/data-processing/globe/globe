package fr.ifremer.viewer3d.gui.markereditor;

import java.util.Optional;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerTypology;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.typology.model.Typology;
import fr.ifremer.globe.typology.ui.TypologyCombobox;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.markers.render.WWMarkerShape;

public class MarkerEditDialog extends Dialog {

	private final IMarker marker;
	private Text textComment;
	private Button btnUseTypologyColor;
	private ColorPicker colorPicker;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public MarkerEditDialog(IMarker selection) {
		super(Display.getDefault().getActiveShell());
		setShellStyle(SWT.CLOSE | SWT.APPLICATION_MODAL | SWT.RESIZE);
		this.marker = selection;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Edit marker (ID: " + marker.getID() + ")");
		shell.setSize(700, 600);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID)
			marker.setComment(textComment.getText());
		super.buttonPressed(buttonId);
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout glContainer = new GridLayout(1, false);
		glContainer.marginWidth = 10;
		glContainer.marginHeight = 10;
		container.setLayout(glContainer);

		Group grpPosition = new Group(container, SWT.NONE);
		grpPosition.setLayout(new GridLayout(6, false));
		grpPosition.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpPosition.setText("Position");

		// latitude
		Label lblLatitude = new Label(grpPosition, SWT.NONE);
		lblLatitude.setText("Latitude : ");
		Label lblLatitudeValue = new Label(grpPosition, SWT.NONE);
		lblLatitudeValue.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblLatitudeValue.setText(LatLongFormater.latitudeToString(marker.getLat()));

		// longitude
		Label lblLongitude = new Label(grpPosition, SWT.NONE);
		lblLongitude.setText("Longitude : ");
		Label lblLongitudeValue = new Label(grpPosition, SWT.NONE);
		lblLongitudeValue.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblLongitudeValue.setText(LatLongFormater.longitudeToString(marker.getLon()));

		// elevation
		Label lblDepth1 = new Label(grpPosition, SWT.NONE);
		lblDepth1.setText("Elevation :");
		Label lblUndefined42 = new Label(grpPosition, SWT.NONE);
		lblUndefined42.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblUndefined42.setText(NumberUtils.getStringDouble(marker.getElevation()) + " m");

		// Water column properties (optional: only for IWCMarker)
		if (marker instanceof IWCMarker) {
			Label lblAltitude = new Label(grpPosition, SWT.NONE);
			lblAltitude.setText("Altitude :");
			Label lblAltitudeValue = new Label(grpPosition, SWT.NONE);
			Optional<Double> optAltitude = ((IWCMarker) marker).getAtltitude();
			lblAltitudeValue.setText(optAltitude.isPresent() ? //
					NumberUtils.getStringDouble(optAltitude.get()) + " m" : "undefined");
			new Label(grpPosition, SWT.NONE);
			new Label(grpPosition, SWT.NONE);
			new Label(grpPosition, SWT.NONE);
			new Label(grpPosition, SWT.NONE);

			Group grpWaterColumn = new Group(container, SWT.NONE);
			grpWaterColumn.setLayout(new GridLayout(4, false));
			grpWaterColumn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			grpWaterColumn.setText("Water column");

			Label lblLayer = new Label(grpWaterColumn, SWT.NONE);
			lblLayer.setText("Layer : ");
			Label lblWCLayerValue = new Label(grpWaterColumn, SWT.NONE);
			lblWCLayerValue.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
			lblWCLayerValue.setText(((IWCMarker) marker).getWaterColumnLayer());

			Label lblPing = new Label(grpWaterColumn, SWT.NONE);
			lblPing.setText("Ping: ");
			Label lblPingValue = new Label(grpWaterColumn, SWT.NONE);
			lblPingValue.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
			lblPingValue.setText(Integer.toString(((IWCMarker) marker).getPing()));

			Label lblDate = new Label(grpWaterColumn, SWT.NONE);
			lblDate.setText("Date :");
			Label lblLayerDate = new Label(grpWaterColumn, SWT.NONE);
			lblLayerDate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
			lblLayerDate.setText(((IWCMarker) marker).getDateString());

			Label lblTime = new Label(grpWaterColumn, SWT.NONE);
			lblTime.setText("Time :");
			Label lblTimeValue = new Label(grpWaterColumn, SWT.NONE);
			lblTimeValue.setText(((IWCMarker) marker).getTimeString());
		}

		// bathymetry
		Group grpBahtymetry = new Group(container, SWT.NONE);
		grpBahtymetry.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpBahtymetry.setText("Bahtymetry");
		grpBahtymetry.setLayout(new GridLayout(4, false));

		// layer
		Label lblDepthLayer = new Label(grpBahtymetry, SWT.NONE);
		lblDepthLayer.setText("Layer : ");
		Label lblLayerValue = new Label(grpBahtymetry, SWT.NONE);
		lblLayerValue.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblLayerValue.setText(marker.getBathyLayer());

		if (marker instanceof IWCMarker) {
			Label lblDepth = new Label(grpBahtymetry, SWT.NONE);
			lblDepth.setText("Depth :");
			Label lblDepthValue = new Label(grpBahtymetry, SWT.NONE);
			lblDepthValue.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
			Optional<Double> optSeeFloorElevation = ((IWCMarker) marker).getSeaFloorElevation();
			lblDepthValue.setText(optSeeFloorElevation.isPresent() ? //
					NumberUtils.getStringDouble(optSeeFloorElevation.get()) + " m" : "undefined");
		}

		Group grpDescription = new Group(container, SWT.NONE);
		grpDescription.setLayout(new GridLayout(2, false));
		grpDescription.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpDescription.setText("Description");

		// Typology
		Label lblTypology = new Label(grpDescription, SWT.NONE);
		lblTypology.setText("Typology :");
		ComboViewer comboViewerTypology = new TypologyCombobox(grpDescription, SWT.NONE | SWT.READ_ONLY, typology -> {
			typology.ifPresent(t -> marker.setTypology(Optional.of((IMarkerTypology) t)));
			if (typology.isPresent() && btnUseTypologyColor != null && btnUseTypologyColor.getSelection()) {
				colorPicker.setColor(typology.get().getGColor());
				marker.setColor(typology.get().getGColor());
				Viewer3D.refreshRequired();
			}
		});
		Combo combo = comboViewerTypology.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		if (marker.getTypology().isPresent()) {
			Typology markerTypology = Typologies.getInstance().findTypology(marker.getTypology().get());
			if (markerTypology != null)
				comboViewerTypology.setSelection(new StructuredSelection(markerTypology));
		}

		// Comment
		Label lblComment = new Label(grpDescription, SWT.NONE);
		lblComment.setText("Comment :");
		new Label(grpDescription, SWT.NONE);
		textComment = new Text(grpDescription, SWT.BORDER | SWT.MULTI);
		textComment.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		textComment.setText(marker.getComment());

		Group grpDisplay = new Group(container, SWT.NONE);
		grpDisplay.setLayout(new GridLayout(6, false));
		grpDisplay.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpDisplay.setText("Display");

		// color
		Label lblColor = new Label(grpDisplay, SWT.NONE);
		lblColor.setText("Color : ");
		colorPicker = new ColorPicker(grpDisplay, SWT.BORDER, marker.getColor(), color -> {
			btnUseTypologyColor.setSelection(false);
			marker.setColor(color);
			Viewer3D.refreshRequired();
		});
		GridData gdColorPicker = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdColorPicker.heightHint = 28;
		gdColorPicker.widthHint = 113;
		colorPicker.setLayoutData(gdColorPicker);

		// size
		Label lblNewLabel = new Label(grpDisplay, SWT.NONE);
		lblNewLabel.setText("Size : ");
		Spinner pointSizeSpinner = new Spinner(grpDisplay, SWT.BORDER);
		pointSizeSpinner.setDigits(2);
		pointSizeSpinner.setMinimum(Integer.MIN_VALUE);
		pointSizeSpinner.setMaximum(Integer.MAX_VALUE);
		pointSizeSpinner.setSelection((int) (marker.getSize()*Math.pow(10,pointSizeSpinner.getDigits())));
		pointSizeSpinner.addListener(SWT.Selection, event -> {
			marker.setSize((float)(pointSizeSpinner.getSelection()/Math.pow(10,pointSizeSpinner.getDigits())));
			Viewer3D.refreshRequired();
		});

		// shape
		Label lblShape = new Label(grpDisplay, SWT.NONE);
		lblShape.setText("Shape");
		ComboViewer comboViewerShape = new ComboViewer(grpDisplay, SWT.NONE | SWT.READ_ONLY);
		Combo comboShape = comboViewerShape.getCombo();
		GridData gdComboShape = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdComboShape.widthHint = 120;
		comboShape.setLayoutData(gdComboShape);
		comboViewerShape.setContentProvider(ArrayContentProvider.getInstance());
		comboViewerShape.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((WWMarkerShape) element).getName();
			}
		});
		comboViewerShape.setInput(WWMarkerShape.values());
		comboViewerShape.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			marker.setShape(((WWMarkerShape) selection.getFirstElement()).getName());
			Viewer3D.refreshRequired();
		});
		comboViewerShape.setSelection(new StructuredSelection(WWMarkerShape.fromName(marker.getShape()).get()));

		btnUseTypologyColor = new Button(grpDisplay, SWT.CHECK);
		btnUseTypologyColor.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnUseTypologyColor.setText("Use typology color");
		btnUseTypologyColor.addListener(SWT.Selection, e -> {
			// if a typology is selected: get color from
			marker.getTypology().ifPresent(markerTypology -> Optional
					.ofNullable(Typologies.getInstance().findTypology(markerTypology)).ifPresent(typology -> {
						colorPicker.setColor(typology.getGColor());
						marker.setColor(typology.getGColor());
						Viewer3D.refreshRequired();
					}));
		});
		// button is selected if marker color is the same of typology color
		Optional<Typology> optTypology = marker.getTypology().isPresent()
				? Optional.ofNullable(Typologies.getInstance().findTypology(marker.getTypology().get()))
				: Optional.empty();
		btnUseTypologyColor.setSelection(optTypology.isPresent() && //
				marker.getColor().equals(optTypology.get().getGColor()));

		new Label(grpDisplay, SWT.NONE);
		new Label(grpDisplay, SWT.NONE);
		new Label(grpDisplay, SWT.NONE);
		new Label(grpDisplay, SWT.NONE);

		return container;
	}
}
