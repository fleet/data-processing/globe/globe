package fr.ifremer.viewer3d.gui.markereditor;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.marker.info.MarkerFileInfo;
import fr.ifremer.globe.core.io.marker.info.WCMarkerFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.typology.events.TypologiesChangedEvent;
import fr.ifremer.globe.typology.handlers.ShowTypologyEditorHandler;
import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.typology.model.Typology;
import fr.ifremer.globe.typology.ui.TypologyCombobox;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.markers.MarkerController;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;
import fr.ifremer.viewer3d.layers.markers.WWWaterColumnMarkerLayer;
import fr.ifremer.viewer3d.layers.markers.render.WWMarkerShape;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import fr.ifremer.viewer3d.util.conf.UISettings;
import swingintegration.example.Platform;

public class MarkerEditor {

	/** Part properties **/
	public static final String PART_ID = "fr.ifremer.viewer3d.gui.markereditor";
	private MPart part;

	private static final Logger LOGGER = LoggerFactory.getLogger(WWMarkerLayer.class);

	@Inject
	WWFileLayerStoreModel fileLayerStoreModel;

	@Inject
	MarkerController markerController;

	private ComboViewer comboMarkerFileViewer;
	private MarkerEditorTableComposite<? extends IMarker> markerEditorTableComposite;

	// FileLayerStore model listener (watch load of marker files)
	private Consumer<WWFileLayerStoreEvent> fileLayerStoreEventListener = evt -> refreshMarkerFileList();

	// Marker dirty listener
	private IChangeListener markerLayerIsDirtyListener = evt -> part
			.setDirty(markerController.getActiveMarkerLayer().isDirty().get());

	private String newMarkerFileToSelect;
	private Text text;
	private ColorPicker colorPicker;
	private Button btnUseColorOf;
	private TypologyCombobox comboViewerTypology;

	@Inject
	public MarkerEditor(Composite parent) {
	}

	/**
	 * Executed after dependency injection is done to perform any initialization
	 */
	@PostConstruct
	void postConstruct(@org.eclipse.e4.core.di.annotations.Optional Composite parent, MApplication application,
			EPartService partService) {
		part = PartUtil.findPart(application, partService, PART_ID);
		createUI(parent);
		fileLayerStoreModel.addListener(fileLayerStoreEventListener);
		if (markerController.getActiveMarkerLayer() != null)
			comboMarkerFileViewer.setSelection(new StructuredSelection(markerController.getActiveMarkerLayer()));
	}

	@PreDestroy
	private void dispose() {
		fileLayerStoreModel.removeListener(fileLayerStoreEventListener);
	}

	private void createUI(Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		Group grpFile = new Group(parent, SWT.NONE);
		grpFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpFile.setText("File");
		grpFile.setLayout(new GridLayout(4, false));

		// marker file selection
		comboMarkerFileViewer = new ComboViewer(grpFile, SWT.READ_ONLY);
		Combo combo = comboMarkerFileViewer.getCombo();
		GridData gd_combo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_combo.widthHint = 400;
		combo.setLayoutData(gd_combo);

		Button btnNew = new Button(grpFile, SWT.NONE);
		btnNew.setText("Create or load file...");
		btnNew.addListener(SWT.Selection, e -> loadOrCreateMarkerFile());

		Button btnSave = new Button(grpFile, SWT.NONE);
		btnSave.setImage(ResourceManager.getPluginImage("fr.ifremer.3dviewer", "icons/16/document-save.png"));
		btnSave.setText("Save");
		btnSave.setToolTipText("Save markers in CSV file");
		btnSave.addListener(SWT.Selection, e -> markerController.getActiveMarkerLayer().save());

		Button btnSaveAs = new Button(grpFile, SWT.NONE);
		btnSaveAs.setImage(ResourceManager.getPluginImage("fr.ifremer.3dviewer", "icons/16/document-save-as.png"));
		btnSaveAs.setText("Save as...");
		btnSaveAs.setToolTipText("Save markers in other CSV file...");
		btnSaveAs.addListener(SWT.Selection, e -> markerController.getActiveMarkerLayer().saveAs());

		comboMarkerFileViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboMarkerFileViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof WWWaterColumnMarkerLayer)
					return ((WWWaterColumnMarkerLayer) element).getMarkerFileInfo().getFilename() + " (water column)";
				if (element instanceof WWMarkerLayer<?>)
					return ((WWMarkerLayer<?>) element).getMarkerFileInfo().getFilename() + " (terrain)";
				return super.getText(element);
			}
		});
		comboMarkerFileViewer.addSelectionChangedListener(event -> {
			// remove previous dirty listener
			if (markerController.getActiveMarkerLayer() != null)
				markerController.getActiveMarkerLayer().isDirty().removeChangeListener(markerLayerIsDirtyListener);

			if (markerEditorTableComposite != null)
				markerEditorTableComposite.dispose();

			Object selection = ((IStructuredSelection) event.getSelection()).getFirstElement();
			if (selection != null) {
				WWMarkerLayer<?> selectedLayer = null;
				if (selection instanceof WWWaterColumnMarkerLayer) {
					selectedLayer = (WWWaterColumnMarkerLayer) selection;
					markerEditorTableComposite = new WCMarkerEditorTableComposite(parent, SWT.FILL,
							(WWWaterColumnMarkerLayer) selectedLayer);
					markerController.setActiveMarkerLayer(selectedLayer);
				} else {
					selectedLayer = (WWMarkerLayer<IMarker>) selection;
					markerEditorTableComposite = new MarkerEditorTableComposite<>(parent, SWT.FILL, selectedLayer);
					markerController.setActiveMarkerLayer(selectedLayer);
				}
				markerEditorTableComposite.moveBelow(grpFile);
				markerEditorTableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				part.setDirty(selectedLayer != null && selectedLayer.isDirty().get());
				selectedLayer.isDirty().addChangeListener(markerLayerIsDirtyListener);
			}

			parent.layout();

		});
		refreshMarkerFileList();

		Group grpAddNewMarker = new Group(parent, SWT.NONE);
		grpAddNewMarker.setLayout(new GridLayout(4, false));
		grpAddNewMarker.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpAddNewMarker.setText("Add new marker options");

		// Typology
		Group grpTypology = new Group(grpAddNewMarker, SWT.NONE);
		grpTypology.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpTypology.setText("Typology");
		grpTypology.setLayout(new GridLayout(3, false));
		comboViewerTypology = new TypologyCombobox(grpTypology, SWT.NONE | SWT.READ_ONLY, typology -> {
			Typologies.getInstance().setSelectedTypology(typology.orElse(null));
			if (typology.isPresent() && btnUseColorOf.getSelection())
				colorPicker.setColor(typology.get().getGColor());
		});

		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		comboViewerTypology.getCombo().setLayoutData(gridData);

		Button btnSelectTypologies = new Button(grpTypology, SWT.NONE);
		btnSelectTypologies.setText("Select");
		// open a window to select typology
		btnSelectTypologies.addListener(SWT.Selection,
				// synchronize window & combo viewer
				e -> Typologies.launchTypologySelector().addSelectionChangedListener(event -> {
					Optional<Typology> typology = Optional.ofNullable(Typologies.getInstance().getSelectedTypology());
					typology.ifPresent(t -> comboViewerTypology.setSelection(new StructuredSelection(t)));
				}));

		Button btnEditTypologies = new Button(grpTypology, SWT.NONE);
		btnEditTypologies.setText("Edit");
		btnEditTypologies.addListener(SWT.Selection,
				// synchronize window & combo viewer
				e -> openTypologyEditor());

		// Color
		Group grpColor = new Group(grpAddNewMarker, SWT.NONE);
		grpColor.setLayout(new GridLayout(2, false));
		grpColor.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		grpColor.setText("Color");

		btnUseColorOf = new Button(grpColor, SWT.CHECK);
		btnUseColorOf.setText("from Typology");
		btnUseColorOf.addListener(SWT.Selection, e -> {
			markerController.setUseTypologyColor(btnUseColorOf.getSelection());
			// if a typology is selected: get color from
			Optional.ofNullable(Typologies.getInstance().getSelectedTypology()).ifPresent(t -> {
				colorPicker.setColor(t.getGColor());
				markerController.setSelectionColor(t.getGColor());
			});
		});
		btnUseColorOf.setSelection(markerController.isUseTypologyColor());

		colorPicker = new ColorPicker(grpColor, SWT.BORDER, markerController.getSelectionColor(), color -> {
			btnUseColorOf.setSelection(false);
			markerController.setUseTypologyColor(false);
			markerController.setSelectionColor(color);
		});

		GridData gd_uniColorPicker = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_uniColorPicker.heightHint = 27;
		gd_uniColorPicker.widthHint = 60;
		colorPicker.setLayoutData(gd_uniColorPicker);

		// init typo combobox & color picker
		Optional.ofNullable(Typologies.getInstance().getSelectedTypology()).ifPresent(selectedTypology -> {
			comboViewerTypology.setSelection(new StructuredSelection(selectedTypology));
			if (btnUseColorOf.getSelection())
				colorPicker.setColor(selectedTypology.getGColor());
		});

		// Shape
		Group grpDisplay = new Group(grpAddNewMarker, SWT.NONE);
		grpDisplay.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpDisplay.setText("Shape");
		grpDisplay.setLayout(new GridLayout(4, false));

		ComboViewer comboViewerShape = new ComboViewer(grpDisplay, SWT.NONE | SWT.READ_ONLY);
		Combo comboShape = comboViewerShape.getCombo();
		GridData gd_comboShape = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		comboShape.setLayoutData(gd_comboShape);
		comboViewerShape.setContentProvider(ArrayContentProvider.getInstance());
		comboViewerShape.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((WWMarkerShape) element).getName();
			}
		});
		comboViewerShape.setInput(WWMarkerShape.values());
		comboViewerShape.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			markerController.setSelectionShape((WWMarkerShape) selection.getFirstElement());
		});
		comboViewerShape.setSelection(new StructuredSelection(markerController.getSelectionShape()));

		// Size
		Label lblSize = new Label(grpDisplay, SWT.NONE);
		lblSize.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblSize.setText("Size");
		Spinner pointSizeSpinner = new Spinner(grpDisplay, SWT.BORDER);
		pointSizeSpinner.setDigits(2);
		GridData gd_pointSizeSpinner = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_pointSizeSpinner.widthHint = 35;
		pointSizeSpinner.setLayoutData(gd_pointSizeSpinner);
		pointSizeSpinner.setMinimum(Integer.MIN_VALUE);
		pointSizeSpinner.setMaximum(Integer.MAX_VALUE);
		pointSizeSpinner
				.setSelection((int) (markerController.getSelectionSize() * Math.pow(10, pointSizeSpinner.getDigits())));
		pointSizeSpinner.addListener(SWT.Selection, event -> markerController.setSelectionSize(
				(float) (pointSizeSpinner.getSelection() / (Math.pow(10, pointSizeSpinner.getDigits())))));

		Group grpComment = new Group(grpAddNewMarker, SWT.NONE);
		grpComment.setLayout(new GridLayout(1, false));
		grpComment.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpComment.setText("Comment");

		text = new Text(grpComment, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		text.setText(markerController.getSelectionComment());
		text.addModifyListener(event -> markerController.setSelectionComment(text.getText()));
	}

	/**
	 * Handles Eclipse events (see {@link IEventBroker}) of topic {@link TypologiesChangedEvent}.
	 */
	@org.eclipse.e4.core.di.annotations.Optional
	@Inject
	private void onTypologiesChanged(@UIEventTopic(TypologiesChangedEvent.TOPIC_TYPOLOGIES_CHANGED) Object event) {
		if (comboViewerTypology != null) {
			comboViewerTypology.updateTypologies();
		}
	}

	/**
	 * Opens the typology editor
	 */
	public void openTypologyEditor() {
		try {
			CommandUtils.executeCommand(ContextInitializer.getEclipseContext(), ShowTypologyEditorHandler.COMMAND_ID);
		} catch (GException e) {
			LOGGER.error("Error while opening Typologies Editor : {}", e.getMessage(), e);
		}
	}

	private void refreshMarkerFileList() {
		List<WWMarkerLayer> markerLayers = fileLayerStoreModel.getAll().stream()//
				.filter(layerStore -> layerStore.getFileInfo().getContentType() == ContentType.MARKER_CSV)
				.map(layerStore -> layerStore.getLayers().stream().filter(layer -> layer instanceof WWMarkerLayer)
						.findFirst())
				.filter(Optional::isPresent).map(optLayer -> (WWMarkerLayer) optLayer.get())
				.collect(Collectors.toList());

		Display.getDefault().syncExec(() -> {
			// if current active marker layer has been unload -> unselect it
			WWMarkerLayer selection = (WWMarkerLayer) comboMarkerFileViewer.getStructuredSelection().getFirstElement();
			if (selection != null && !markerLayers.contains(selection))
				comboMarkerFileViewer.setSelection(null);

			// sort marker file by alphabetical order
			markerLayers.sort((l1, l2) -> l1.getName().compareTo(l2.getName()));
			comboMarkerFileViewer.setInput(markerLayers);

			// select marker file just loaded or created
			if (newMarkerFileToSelect != null) {
				markerLayers.stream()
						.filter(layer -> layer.getMarkerFileInfo().getAbsolutePath().equals(newMarkerFileToSelect))
						.findFirst().ifPresent(layer -> {
							comboMarkerFileViewer.setSelection(new StructuredSelection(layer));
							newMarkerFileToSelect = null;
						});
			} else if (markerController.getActiveMarkerLayer() != null) {
				// continue to display active marker layer (TODO improve by setting the active marker as an observable)
				comboMarkerFileViewer.setSelection(new StructuredSelection(markerController.getActiveMarkerLayer()));
			}
		});
	}

	private void loadOrCreateMarkerFile() {
		FileDialog dialog;
		if (Platform.isMac()) {
			dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.SAVE);
		} else {
			dialog = new FileDialog(Display.getCurrent().getActiveShell());
		}
		dialog.setText("Select or create a marker file (.csv)");
		dialog.setFilterNames(new String[] { "Marker file (*.csv)" });
		dialog.setFilterExtensions(new String[] { "*.csv" });
		dialog.setFileName("markers.csv");

		// loads default path from configuration
		SSVConfiguration conf = SSVConfiguration.getConfiguration();
		if (conf != null && conf.getUISettings() != null) {
			String path = conf.getUISettings().getDefault(UISettings.EXPORT_PROFILE_PATH);
			if (path != null) {
				File f = new File(path);
				dialog.setFilterPath(f.getParent());
			}
		}

		String filename = dialog.open();
		if (filename != null) {

			// force csv extension
			if (!filename.endsWith(".csv"))
				filename += ".csv";

			MarkerFileInfo fileInfo;
			try {
				// if new file: create it with save method
				if (!new File(filename).exists()) {
					int returnCode = Messages.openSyncQuestionMessage("New marker file",
							"Wich kind of marker file is it ?", "Terrain", "Water column");
					switch (returnCode) {
					case 0: // terrain : classic marker file
						fileInfo = new MarkerFileInfo<>(filename);// .save();
						fileInfo.save();
						break;
					case 1: // water column marker file
						fileInfo = new WCMarkerFileInfo(filename);// .save();
						fileInfo.save();
						break;
					default: // cancel selected
						return;
					}
					ITreeNodeFactory.grab().createFileInfoNode(fileInfo);
				}
			} catch (GIOException ex) {
				Messages.openErrorMessage("Error : " + ex.getMessage(), ex);
			}
			// ITreeNodeFactory.grab().createFileInfoNode(fileInfo);
			IFileLoadingService.grab().load(filename);
			newMarkerFileToSelect = filename;
		}
	}

	@Persist
	public boolean save(IProgressMonitor monitor) {
		return markerController.getActiveMarkerLayer().save();
	}
}
