package fr.ifremer.viewer3d.gui.markereditor;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;

/**
 * Table to display {@link IWCMarker}.
 */
public class WCMarkerEditorTableComposite extends MarkerEditorTableComposite<IWCMarker> {

	/**
	 * Constructor
	 */
	public WCMarkerEditorTableComposite(Composite parent, int style, WWMarkerLayer<IWCMarker> markerLayer) {
		super(parent, style, markerLayer);
	}

	/**
	 * Builds colmuns (specific of Water Column markers).
	 */
	@Override
	protected void buildColmuns() {
		// create columns
		buildCheckboxColumn(IMarker::isEnabled, Optional.of("Enable"));
		buildColumn("ID", 35, IWCMarker::getID);
		buildColumn("Layer", 100, IWCMarker::getWaterColumnLayer);
		buildColumn("Ping", 50, marker -> Integer.toString(marker.getPing()));
		buildColumn("Date", 100, IWCMarker::getDateString);
		buildColumn("Time", 100, IWCMarker::getTimeString);
		buildColumn("Latitude", 100, marker -> LatLongFormater.latitudeToString(marker.getLat()));
		buildColumn("Longitude", 100, marker -> LatLongFormater.longitudeToString(marker.getLon()));
		buildColumn("Elevation", IMarker.ELEVATION_LONG_NAME, 80,
				marker -> NumberUtils.getStringDouble(marker.getElevation()));
		buildColumn("Altitude", IMarker.ALTITUDE_LONG_NAME, 80,
				marker -> marker.getAtltitude().isPresent() ? NumberUtils.getStringDouble(marker.getAtltitude().get())
						: "");
		buildColumn("Sea floor", IMarker.SEA_FLOOR_ELEVATION_LONG_NAME, 80,
				marker -> marker.getSeaFloorElevation().isPresent()
						? NumberUtils.getStringDouble(marker.getSeaFloorElevation().get())
						: "");
		buildColumn("Sea floor layer", 80, IWCMarker::getBathyLayer);
		buildColumn("Group", "Typology group", 100,
				marker -> marker.getTypology().isPresent() ? marker.getTypology().get().getGroup() : "");
		buildColumn("Class", "Typology class", 100,
				marker -> marker.getTypology().isPresent() ? marker.getTypology().get().getClazz() : "");
		buildColumn("Comment", 200, IMarker::getComment);
	}

}
