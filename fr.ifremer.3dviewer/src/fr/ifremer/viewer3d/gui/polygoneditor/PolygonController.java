/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.gui.polygoneditor;

import java.awt.Cursor;
import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.gdal.ogr.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.polygon.BasicPolygon;
import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.IPolygonWriter;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.wizard.IWizardBuilder;
import fr.ifremer.globe.ui.service.wizard.IWizardService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.DefaultSelectOutputParametersPageModel;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.viewer3d.gui.polygoneditor.handler.ShowPolygonEditorHandler;
import fr.ifremer.viewer3d.util.SSVCursor;
import swingintegration.example.Platform;

/**
 * This class handles the polygon management.
 */
@Creatable
@Singleton
public class PolygonController {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(PolygonController.class);

	/** 3D view service */
	@Inject
	private IGeographicViewService geographicViewService;

	/** Polygon file service */
	@Inject
	private IPolygonWriter polygonWriter;

	/** Factory of layers */
	@Inject
	private IWWLayerFactory layerFactory;

	/** Builder of wizard */
	@Inject
	private IWizardService wizardService;

	/** Model of loaded WWFileLayerStore in the geographic view */
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Shell of the application. */
	@Inject
	@Named(IServiceConstants.ACTIVE_SHELL)
	private Shell shell;

	/** Service to load files in Globe. */
	@Inject
	private IFileLoadingService fileLoadingService;

	/** Event broker service */
	@Inject
	private IEventBroker eventBroker;

	/** In memory PolygonFileInfo. The polygons in this file are not persistent. */
	private final PolygonFileInfo inMemoryPolygonFileInfo = new PolygonFileInfo("No file");
	/** IWWPolygonLayer used to edit inMemoryPolygonFileInfo polygons */
	private IWWPolygonLayer inMemoryPolygonLayer;

	/** Flag to avoid triggering an infinite event loop */
	private final AtomicBoolean isFiringCurrentPolygon = new AtomicBoolean(false);

	/** List of dirty files */
	private final Set<PolygonFileInfo> dirtyFiles = new HashSet<>();

	/** Possible extension */
	public enum PolygonContentType {
		KML("kml", "KML - Keyhole Markup Language", GdalUtils.KML_DRIVER_NAME), //
		GEOJSON("geojson", "GeoJSON - Geographic JSON", GdalUtils.GEOJSON_DRIVER_NAME), //
		SHAPEFILE("shp", "Shapefile - ESRI Shapefile / DBF", GdalUtils.SHP_DRIVER_NAME);

		public final String extension;
		public final String label;
		public final String gdalDriverName;

		private PolygonContentType(String extension, String label, String gdalDriverName) {
			this.extension = extension;
			this.label = label;
			this.gdalDriverName = gdalDriverName;
		}
	}

	/** Current edited polygon file */
	private PolygonFileInfo currentPolygonFile = inMemoryPolygonFileInfo;
	/** Current edited polygon */
	private Optional<IPolygon> editedPolygon = Optional.empty();
	/** True when polygon is currently in creation */
	private AtomicBoolean creationInProgress = new AtomicBoolean();

	/** Listener on layer model */
	private final Consumer<WWFileLayerStoreEvent> fileLayerStoreListener = this::onFileLayerStoreEvent;

	/**
	 * Add a new empty polygon to the current file and start editing it.
	 */
	public void createNewPolygon() {
		// A polygon is already in creation ?
		if (creationInProgress.compareAndSet(false, true)) {
			logger.info("Creating a new polygon");

			int polygonIndex = 1;
			IPolygon newPolygon = null;
			while (newPolygon == null) {
				String polygonName = "Polygon_" + polygonIndex;
				if (currentPolygonFile.getPolygons().stream().noneMatch(p -> p.getName().equals(polygonName))) {
					newPolygon = new BasicPolygon("Polygon_" + polygonIndex);
				}
				polygonIndex++;
			}
			currentPolygonFile.getPolygons().add(newPolygon);
			editedPolygon = Optional.of(newPolygon);
			firePolygonFileHasChanged(currentPolygonFile);

			// update cursor
			geographicViewService.getWwd().setCursor(SSVCursor.getPredefinedCursor(SSVCursor.CURSOR_ZONE));
		}
	}

	/** Cancellation of the edition required */
	public void cancelEdition() {
		if (creationInProgress.compareAndSet(true, false)) {
			// Cancel creation
			editedPolygon.ifPresent(p -> {
				if (currentPolygonFile.getPolygons().remove(p)) {
					editedPolygon = Optional.empty();
					firePolygonFileHasChanged(currentPolygonFile);
				}
			});
			geographicViewService.getWwd().setCursor(Cursor.getDefaultCursor());
		} else {
			// Cancel update
			editedPolygon.ifPresent(p -> {
				editedPolygon = Optional.empty();
				firePolygonFileHasChanged(currentPolygonFile);
			});
		}
	}

	/**
	 * Validate the creation of the polygon
	 */
	public synchronized void finalizeTheCreation(Geometry geometry) {
		if (isValidGeometry(geometry) && IPolygon.computeSurface(geometry) > 0.1
				&& creationInProgress.compareAndSet(true, false)) {
			editedPolygon.ifPresent(//
					p -> {
						logger.info("Finalizing the creation of polygon : {} ", p.getName());
						updateEditedPolygon(IPolygon.makePolygon(geometry));
					});

			// update cursor
			geographicViewService.getWwd().setCursor(Cursor.getDefaultCursor());
		}
	}

	/** Save of the current polygon is required */
	public void save() {
		try {
			if (currentPolygonFile != inMemoryPolygonFileInfo) {
				logger.info("Saving polygon file {} ", currentPolygonFile.getPath());
				polygonWriter.save(currentPolygonFile);
				setDirty(currentPolygonFile, false);
			}
		} catch (Exception e) {
			Messages.openErrorMessage("Polygon save error", e.getMessage());
		}
	}

	/** Save the current polygon in a other file */
	public void saveAs() {
		try {
			PolygonFileInfo originalPolygonFileInfo = currentPolygonFile;
			PolygonFileInfo polygonToSave = new PolygonFileInfo("polygons");
			polygonToSave.getPolygons().addAll(currentPolygonFile.getPolygons());
			if (saveNewPolygon(polygonToSave)) {
				// Clean the in memory file after the save
				if (originalPolygonFileInfo == getInMemoryPolygonFileInfo()) {
					originalPolygonFileInfo.getPolygons().clear();
					setDirty(originalPolygonFileInfo, false);
					eventBroker.send(PolygonTopics.TOPIC_POLYGON_FILE_CHANGED, originalPolygonFileInfo);
				}
			}
		} catch (Exception e) {
			Messages.openErrorMessage("Polygon save error", e.getMessage());
		}
	}

	/** Save all dirty files */
	public void saveAllDirty() {
		for (PolygonFileInfo polygonFileInfo : dirtyFiles) {
			if (polygonFileInfo != getInMemoryPolygonFileInfo()) {
				try {
					polygonWriter.save(polygonFileInfo);
				} catch (GException e) {
					logger.warn("Error when saving file " + polygonFileInfo.getPath(), e);
				}
			}
		}

		// Clean dirty flags
		dirtyFiles.clear();
		if (!getInMemoryPolygonFileInfo().getPolygons().isEmpty()) {
			dirtyFiles.add(getInMemoryPolygonFileInfo());
		}

		eventBroker.send(PolygonTopics.TOPIC_IS_DIRTY, isDirty());
	}

	/** Load a new polygon file and edit it */
	public void loadOrCreateOneFile() {
		FileDialog dialog = new FileDialog(shell, Platform.isMac() ? SWT.SAVE : SWT.APPLICATION_MODAL);
		dialog.setText("Select or create a polygon file");
		dialog.setFilterNames(Arrays.stream(PolygonContentType.values()).map(e -> e.label).toArray(String[]::new));
		dialog.setFilterExtensions(
				Arrays.stream(PolygonContentType.values()).map(e -> "*." + e.extension).toArray(String[]::new));
		dialog.setFileName("polygon.kml");
		String filename = dialog.open();
		if (filename != null) {
			if (!new File(filename).exists()) {
				// Force extension
				PolygonContentType fileType = dialog.getFilterIndex() >= 0
						? PolygonContentType.values()[dialog.getFilterIndex()]
						: PolygonContentType.KML;
				filename = FilenameUtils.removeExtension(filename) + "." + fileType.extension;
				logger.info("Creating an empty polygon file {} ", filename);
				PolygonFileInfo newPolygonFile = new PolygonFileInfo(filename);
				newPolygonFile.setGdalDriverName(fileType.gdalDriverName);
				try {
					polygonWriter.save(newPolygonFile);
					setCurrentPolygonFile(new File(filename));
				} catch (GException e) {
					Messages.openInfoMessage("Creation aborted", "Unable to create the new file : \n" + e.getMessage());
				}
			} else {
				setCurrentPolygonFile(new File(filename));
			}
		}
	}

	/**
	 * Creates a new polygon file
	 *
	 * @return true if file was correctly saved
	 */
	private boolean saveNewPolygon(PolygonFileInfo fileInfo) {
		WizardModel model = new WizardModel();
		model.getInputFiles().add(new File(FileUtils.getUserDirectory(), fileInfo.getBaseName() + ".kml"));
		if (openSaveWizard(model, "Save polygon as") && !model.getOutputFiles().isEmpty()) {
			File outputFile = model.getOutputFiles().get(0);
			if (outputFile.exists() && model.getOverwriteExistingFiles().isFalse()) {
				Messages.openInfoMessage("Save aborted", "File exists and overwriting is not allowed");
				return false;
			}

			fileInfo.setPath(outputFile.getPath());
			fileInfo.setGdalDriverName(model.getSelectedFileExtension().gdalDriverName);
			logger.info("Creating new polygon file {} ", fileInfo.getPath());
			try {
				polygonWriter.save(fileInfo);
				if (model.getLoadFilesAfter().isTrue()) {
					// Request the loading
					fileLoadingService.reload(outputFile);
				}
				return true;
			} catch (GException e) {
				Messages.openInfoMessage("Save error", e.getMessage());
			}
		}
		return false;
	}

	/** Open a wizard to save the polygon */
	public boolean openSaveWizard(WizardModel model, String title) {
		IWizardBuilder wizardBuilder = wizardService.newWizardBuilder();
		wizardBuilder.addToGenericPage(title, //
				"Choose the file type to save the polygon", //
				"File type", //
				model.getFileType(), //
				null, // No Evaluator (no Pyat function)
				PolygonContentType.KML.label, //
				PolygonContentType.SHAPEFILE.label, //
				PolygonContentType.GEOJSON.label);

		wizardBuilder.addSelectOutputFilesPage(title, model.getInputFiles(), model.getOutputFormatExtensions(),
				model.getOutputFiles(), model.getLoadFilesAfter(), model.getWhereToloadFiles(),
				model.getOverwriteExistingFiles());

		Wizard wizard = wizardBuilder.build();
		wizard.setWindowTitle(title);
		WizardDialog wizardDialog = new WizardDialog(shell, wizard);
		wizardDialog.setTitle(title);
		wizardDialog.setMinimumPageSize(620, 300);
		return wizardDialog.open() == Window.OK;
	}

	/** Request the loading of the file and set the resulting polygon as current edited one */
	private void setCurrentPolygonFile(File file) {
		// Reload the file and edit it at once
		// Wait for the new polygon by listening the layer model
		fileLayerStoreModel.removeListener(fileLayerStoreListener);
		fileLayerStoreModel.addListener(fileLayerStoreListener);
		// Request the loading
		fileLoadingService.reload(file);
	}

	/** Request to edition of the specified polygon */
	public void requestPolygonEdition(PolygonFileInfo polygonFileInfo, IPolygon polygon) {
		if (polygon != null && !isCreationInProgress()) {
			if (currentPolygonFile != polygonFileInfo) {
				ShowPolygonEditorHandler.showView();
				requestPolygonFileEdition(polygonFileInfo);
			}
			if (currentPolygonFile.getPolygons().contains(polygon)) {
				// Swith to the other polygon
				logger.info("Editing polygon '{}' ", polygon.getName());
				editedPolygon = Optional.of(polygon);
			}
			firePolygonHasChanged(polygon);
		}
	}

	/** Ask the controller to delete all polygons of current file */
	public void requestPolygonsDeletion() {
		logger.info("Revoving all polygons from file");
		currentPolygonFile.getPolygons().clear();
		editedPolygon = Optional.empty();
		creationInProgress.set(false);
		firePolygonFileHasChanged(currentPolygonFile);
		setDirty(currentPolygonFile, true);
	}

	/** Ask the controller to delete the current edited polygon */
	public void requestPolygonDeletion(PolygonFileInfo polygonFile, IPolygon polygon) {
		if (polygonFile.getPolygons().remove(polygon)) {
			logger.info("Revoving polygon {} from file {}", polygon.getName(), polygonFile.getFilename());
			firePolygonFileHasChanged(polygonFile);
			setDirty(polygonFile, true);
		}
	}

	/** Listener of FileLayerStore. Waiting for the newly created polygon */
	private void onFileLayerStoreEvent(WWFileLayerStoreEvent e) {
		if (e.getState() == FileLayerStoreState.ADDED
				&& e.getFileLayerStore().getFileInfo() instanceof PolygonFileInfo) {
			requestPolygonFileEdition((PolygonFileInfo) e.getFileLayerStore().getFileInfo());
			fileLayerStoreModel.removeListener(fileLayerStoreListener);
		}
	}

	/**
	 * @return the current edited PolygonFileInfo.
	 */
	public PolygonFileInfo getCurrentPolygonFile() {
		return currentPolygonFile;
	}

	/**
	 * Request to edit the specified file
	 */
	public void requestPolygonFileEdition(PolygonFileInfo polygonFile) {
		polygonFile = polygonFile != null ? polygonFile : inMemoryPolygonFileInfo;
		logger.info("Editing file '{}' ", currentPolygonFile.getPath());
		if (!polygonFile.equals(currentPolygonFile)) {
			currentPolygonFile = polygonFile;
			editedPolygon = Optional.empty();
			firePolygonFileHasChanged(currentPolygonFile);
		}
	}

	/**
	 * Modify the edited polygon by setting a new Geometry
	 */
	public synchronized void updateEditedPolygon(Geometry geometry) {
		// Sanity check
		if (geometry == null) {
			return;
		}
		// Bad Geometry are only acceptable during creation
		if (isCreationInProgress() || isValidGeometry(geometry)) {
			editedPolygon.ifPresent(polygon -> {
				polygon.setGeometry(geometry);
				firePolygonHasChanged(polygon);
				setDirty(currentPolygonFile, true);
			});
		}
	}

	/** Return true if geometry is acceptable and can be set to a polygon */
	private boolean isValidGeometry(Geometry geometry) {
		return geometry != null && geometry.GetPointCount() > 3;

	}

	/**
	 * Returns true when the controller is busy, a polygon is being edited
	 */
	public boolean isDirty() {
		return !dirtyFiles.isEmpty();
	}

	/**
	 * Returns true when the controller a polygon is currently in creation
	 */
	public boolean isCreationInProgress() {
		return creationInProgress.get();
	}

	/**
	 * Send an event to deliver the current edited file
	 */
	private void firePolygonFileHasChanged(PolygonFileInfo polygonFile) {
		if (isFiringCurrentPolygon.compareAndSet(false, true)) {
			eventBroker.send(PolygonTopics.TOPIC_POLYGON_FILE_CHANGED, polygonFile);
			isFiringCurrentPolygon.set(false);
		}
	}

	/**
	 * Send an event to inform that the specifiel polygon has changed
	 */
	private void firePolygonHasChanged(IPolygon polygon) {
		eventBroker.send(PolygonTopics.TOPIC_POLYGON_CHANGED, polygon);
	}

	/**
	 * Mark current edited file as dirty or not
	 */
	private void setDirty(PolygonFileInfo polygonFile, boolean dirty) {
		// Only POLYGON_GDAL files can be updated (not GEOJSON_GDAL ones)
		if (polygonFile.getContentType() == ContentType.POLYGON_GDAL && (!dirty || !dirtyFiles.contains(polygonFile))) {
			if (dirty) {
				dirtyFiles.add(polygonFile);
			} else {
				dirtyFiles.remove(polygonFile);
			}
			eventBroker.send(PolygonTopics.TOPIC_IS_DIRTY, isDirty());
		}
	}

	/** Model used by the save wizard */
	public static class WizardModel extends DefaultSelectOutputParametersPageModel {

		private final WritableString fileType = new WritableString(PolygonContentType.KML.label);

		/**
		 * Constructor
		 */
		public WizardModel() {
			// Init default values
			getOutputFormatExtensions().add(PolygonContentType.KML.extension);
			getOverwriteExistingFiles().set(true);
			getLoadFilesAfter().set(true);

			// React to the change of output format
			fileType.addChangeListener(e -> getOutputFormatExtensions().set(0, getSelectedFileExtension().extension));
		}

		/** Translate the choosen file type to a FileExtension */
		public PolygonContentType getSelectedFileExtension() {
			var currentFileType = fileType.get();
			for (var aFileExtension : PolygonContentType.values()) {
				if (aFileExtension.label.equals(currentFileType))
					return aFileExtension;
			}
			return PolygonContentType.KML;
		}

		/**
		 * @return the {@link #fileType}
		 */
		public WritableString getFileType() {
			return fileType;
		}
	}

	/**
	 * @return the {@link #editedPolygon}
	 */
	public Optional<IPolygon> getEditedPolygon() {
		return editedPolygon;
	}

	/** Modify the name of the polygon */
	public void changePolygonName(IPolygon polygon, String name) {
		if (name != null && !name.isEmpty() && !name.equals(polygon.getName())) {
			polygon.setName(name);
			setDirty(currentPolygonFile, true);
			firePolygonHasChanged(polygon);
		}
	}

	/** Modify the comment of the polygon */
	public void changePolygonComment(IPolygon polygon, String comment) {
		if (comment != null && !comment.equals(polygon.getComment())) {
			polygon.setComment(comment);
			setDirty(currentPolygonFile, true);
			firePolygonHasChanged(polygon);
		}
	}

	/**
	 * @return the {@link #inMemoryPolygonFileInfo}
	 */
	public PolygonFileInfo getInMemoryPolygonFileInfo() {
		return inMemoryPolygonFileInfo;
	}

	/**
	 * @return true if file have to be saved
	 */
	public boolean isDirtyFile(PolygonFileInfo file) {
		return dirtyFiles.contains(file);
	}

	/** Activate the controller. Means that polygon tool is up. */
	public void activate() {
		// Initialize the in memory layer for the first call
		if (inMemoryPolygonLayer == null) {
			inMemoryPolygonLayer = layerFactory.createPolygonLayer(inMemoryPolygonFileInfo);
			geographicViewService.addLayer(inMemoryPolygonLayer);
		}
	}

	/** Reset the controller. Means that polygon tool is down. */
	public void deactivate() {
		if (inMemoryPolygonLayer != null) {
			geographicViewService.removeLayer(inMemoryPolygonLayer);
			inMemoryPolygonLayer.dispose();
			inMemoryPolygonLayer = null;
		}
		geographicViewService.getWwd().setCursor(Cursor.getDefaultCursor());
		editedPolygon = Optional.empty();
		currentPolygonFile = inMemoryPolygonFileInfo;
		firePolygonFileHasChanged(inMemoryPolygonFileInfo);
	}

}
