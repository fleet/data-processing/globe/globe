/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.gui.polygoneditor;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * All topics in the polygon management through the IEventBroker
 */
public class PolygonTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/viewer3d/gui/polygoneditor";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to inform that the specified polygon file changed (Polygon added, removed...).<br>
	 * Object passed is a PolygonFileInfo
	 */
	public static final String TOPIC_POLYGON_FILE_CHANGED = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_POLYGON_FILE_CHANGED";

	/**
	 * Topic to inform that a polygon has changed.<br>
	 * Object passed is a IPolygon.
	 */
	public static final String TOPIC_POLYGON_CHANGED = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_POLYGON_CHANGED";

	/**
	 * Topic to inform that the polygon has changed and may be saved.<br>
	 * Object passed Boolean.
	 */
	public static final String TOPIC_IS_DIRTY = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_IS_DIRTY";

	/** Constructor */
	private PolygonTopics() {
		// private constructor to hide the implicit public one.
	}

}
