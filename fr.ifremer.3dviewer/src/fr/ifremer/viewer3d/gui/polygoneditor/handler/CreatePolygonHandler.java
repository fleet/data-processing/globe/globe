/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.gui.polygoneditor.handler;

import jakarta.inject.Inject;

import org.eclipse.core.commands.CommandManager;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarElement;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.osgi.service.event.Event;

import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonController;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonTopics;
import fr.ifremer.viewer3d.handlers.EnableMarkersHandler;

/**
 * Handler triggering the polygon management
 */
public class CreatePolygonHandler {

	/** Command id defined in E4 model */
	public static final String COMMAND_ID = "fr.ifremer.3dviewer.command.addpolygons";
	/** ToolItem id defined in E4 model */
	public static final String TOOL_ITEM_ID = "fr.ifremer.3dviewer.handledtoolitem.addpolygons";

	/** Controller of polygon */
	@Inject
	private PolygonController polygonController;

	/** Service of E4 model */
	@Inject
	private EModelService modelService;

	/** Application */
	@Inject
	private EPartService partService;

	/**
	 * Invoke when user click on Polygon tool or on a other radio button in the toolbar
	 */
	@Execute
	protected void execute(@Active MToolItem toolItem, CommandManager commandManager) {
		// Execution from Polygon toolItem ?
		if (TOOL_ITEM_ID.equals(toolItem.getElementId())) {
			if (toolItem.isSelected()) {
				disarmMarkers(toolItem.getParent(), commandManager);
				polygonController.createNewPolygon();
				ShowPolygonEditorHandler.showView();
			} else {
				polygonController.cancelEdition();
			}
		} else {
			// Direct execution of the command to force marker disarming
			polygonController.cancelEdition();
		}
	}

	/**
	 * Disarming the marker tool
	 */
	private void disarmMarkers(MElementContainer<MUIElement> toolbar, CommandManager commandManager) {
		MUIElement markerToolItem = modelService.find(EnableMarkersHandler.TOOL_ITEM_ID, toolbar);
		if (markerToolItem instanceof MItem) {
			MItem mItem = (MItem) markerToolItem;
			if (mItem.isSelected()) {
				mItem.setSelected(false);
				CommandUtils.executeCommand(commandManager, EnableMarkersHandler.COMMAND_ID);
			}
		}
	}

	/** Handles the TOPIC_POLYGON_FILE_CHANGED event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPolygonFileChanged(@UIEventTopic(PolygonTopics.TOPIC_POLYGON_FILE_CHANGED) Event event) {
		unselectToolbarItem();
	}

	/** Handles the TOPIC_POLYGON_CHANGED event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPolygonChanged(@UIEventTopic(PolygonTopics.TOPIC_POLYGON_CHANGED) Event event) {
		unselectToolbarItem();
	}

	/**
	 * @return the MToolBarElement of polygon
	 */
	private MToolBarElement findPolygonToolBarElement() {
		return partService.findPart(Viewer3D.PART_ID)//
				.getToolbar()//
				.getChildren().stream() //
				.filter(e -> TOOL_ITEM_ID.equals(e.getElementId()))//
				.findFirst()//
				.orElseThrow();
	}

	/**
	 * Unselect the MToolBarElement of polygon
	 */
	private void unselectToolbarItem() {
		if (!polygonController.isCreationInProgress()) {
			MToolBarElement toolBarElement = findPolygonToolBarElement();
			((MItem) toolBarElement).setSelected(false);
		}
	}

}
