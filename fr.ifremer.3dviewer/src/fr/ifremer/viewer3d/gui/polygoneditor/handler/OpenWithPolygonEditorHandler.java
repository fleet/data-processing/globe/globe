/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.gui.polygoneditor.handler;

import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.core.commands.CommandManager;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonController;

/**
 * Open the polygon editor for a file
 */
public class OpenWithPolygonEditorHandler extends AbstractNodeHandler {

	/** Id of command defined in fragment.e4xmi */
	public static final String COMMAND_ID = "fr.ifremer.3dviewer.command.openwithpolygoneditor";

	/** Controller of polygon */
	@Inject
	private PolygonController polygonController;
	@Inject
	private IGeographicViewService geographicViewService;

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement uiElement) {
		return checkExecution(selectionService, uiElement);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !polygonController.isCreationInProgress() && !geographicViewService.isMarkersEnabled()
				&& findSelectedPolygon(selectionService).isPresent();
	}

	@Execute
	public void execute(ESelectionService selectionService, CommandManager commandManager) {
		findSelectedPolygon(selectionService).ifPresent(polygonFile -> {
			geographicViewService.zoomToGeobox(polygonFile.getGeoBox());
			ShowPolygonEditorHandler.showView();
			polygonController.requestPolygonFileEdition(polygonFile);
		});
	}

	/** Search the PolygonFileInfo in the selection if any */
	protected Optional<PolygonFileInfo> findSelectedPolygon(ESelectionService selectionService) {
		Object[] selection = getSelection(selectionService);
		if (selection.length != 1) {
			return Optional.empty();
		}

		List<FileInfoNode> nodeList = getSelectionAsList(selection,
				contentType -> contentType == ContentType.POLYGON_GDAL);
		return nodeList.stream() //
				.map(FileInfoNode::getFileInfo) //
				.filter(PolygonFileInfo.class::isInstance) //
				.map(PolygonFileInfo.class::cast) //
				.findFirst();
	}
}
