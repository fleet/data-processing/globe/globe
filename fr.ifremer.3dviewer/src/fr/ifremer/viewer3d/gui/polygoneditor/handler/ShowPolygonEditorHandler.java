package fr.ifremer.viewer3d.gui.polygoneditor.handler;

import org.eclipse.core.commands.CommandManager;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonEditor;

/**
 * Open the polygon editor
 */
public class ShowPolygonEditorHandler {

	/** Id of command defined in fragment.e4xmi */
	public static final String COMMAND_ID = "fr.ifremer.3dviewer.command.showpolygoneditor";

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application,
			EModelService modelService) {
		MPart polygonEditor = PartUtil.findPart(application, partService, PolygonEditor.PART_ID);
		PartUtil.showPartSashContainer(polygonEditor);
		polygonEditor.setVisible(true);
		partService.showPart(polygonEditor, PartState.ACTIVATE);
		partService.activate(polygonEditor, true);
	}

	/** Required the command execution to show the view */
	public static void showView() {
		Display.getDefault().asyncExec(() -> CommandUtils
				.executeCommand(ContextInitializer.getEclipseContext().get(CommandManager.class), COMMAND_ID));
	}

}
