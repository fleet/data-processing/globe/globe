/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.gui.polygoneditor.handler;

import java.io.File;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.polygon.IPolygonWriter;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonController;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonController.WizardModel;

/**
 * Launch the wizard to convert a file to a polygon file
 */
public class ConvertToPolygonFileHandler extends AbstractNodeHandler {

	/** Id of command defined in fragment.e4xmi */
	public static final String COMMAND_ID = "fr.ifremer.3dviewer.command.convertToPolygonFile";
	/** Id of command defined in fragment.e4xmi */
	public static final String PARAM_ID = "fr.ifremer.3dviewer.commandparameter.convertToPolygonFile.filePath";

	/** Polygon file service */
	@Inject
	private IPolygonWriter polygonWriter;
	/** Polygon controller */
	@Inject
	private PolygonController polygonController;
	/** Service to load files in Globe. */
	@Inject
	private IFileLoadingService fileLoadingService;

	@CanExecute
	private boolean canExecute(//
			@org.eclipse.e4.core.di.annotations.Optional @Named(PARAM_ID) String filePath, //
			@org.eclipse.e4.core.di.annotations.Optional MUIElement menuItem, //
			ESelectionService selectionService) {
		// Command invoked from menu ?
		if (menuItem != null) {
			return checkExecution(selectionService, menuItem);
		}
		// Command directly invoked with a file in parameter ?
		return filePath != null;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return findSelectedPolygon(selectionService).isPresent();
	}

	/** Search the gdal vector file in the selection if any */
	protected Optional<IFileInfo> findSelectedPolygon(ESelectionService selectionService) {
		List<FileInfoNode> nodeList = getSelectionAsList(getSelection(selectionService),
				contentType -> contentType.isOneOf(ContentType.KML_XML, ContentType.SHAPEFILE));
		return Optional.ofNullable(nodeList.size() == 1 ? nodeList.get(0).getFileInfo() : null);
	}

	@Execute
	private void execute(//
			@org.eclipse.e4.core.di.annotations.Optional @Named(PARAM_ID) String filePath, //
			ESelectionService selectionService) {
		// Command invoked from menu ?
		try {
			if (filePath == null) {
				Optional<IFileInfo> vectorFileInfo = findSelectedPolygon(selectionService);
				if (vectorFileInfo.isPresent()) {
					filePath = vectorFileInfo.get().getPath();
				}
			}

			if (filePath != null) {
				WizardModel model = new WizardModel();
				model.getInputFiles().add(new File(filePath));
				if (polygonController.openSaveWizard(model, "Export file as")) {
					File outputFile = model.getOutputFiles().get(0);
					if (outputFile.exists() && model.getOverwriteExistingFiles().isFalse()) {
						Messages.openInfoMessage("Convert aborted", "File exists and overwriting is not allowed");
						return;
					}
					PolygonFileInfo result = polygonWriter.saveAs(model.getInputFiles().get(0).getPath(),
							outputFile.getPath(), model.getSelectedFileExtension().gdalDriverName);
					if (result.toFile().exists() && model.getLoadFilesAfter().isTrue()) {
						fileLoadingService.reload(result.getPath());
					}
				}
			}
		} catch (GException e) {
			Messages.openErrorMessage("Convertion failed", e.getMessage());
		}
	}

}
