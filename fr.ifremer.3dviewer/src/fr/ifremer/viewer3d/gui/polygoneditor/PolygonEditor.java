/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.gui.polygoneditor;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.widgets.ButtonFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.utils.UIUtils;

/**
 * View for listing loaded polygons and editing them
 */
public class PolygonEditor {

	/** Part properties **/
	public static final String PART_ID = "fr.ifremer.3dviewer.part.polygonview";

	/** Controller of polygon */
	@Inject
	private PolygonController controller;

	/** Model of loaded WWFileLayerStore in the geographic view */
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** MPart of this editor */
	@Inject
	@Active
	private MPart mPart;

	/** Listener of FileLayer model */
	private final Consumer<WWFileLayerStoreEvent> fileLayerStoreEventListener;

	/** Widgets **/
	private ComboViewer comboPolygonFileViewer;
	private PolygonTableComposite polygonTableComposite;
	private Button btnSave;
	private Button btnNewFile;

	/**
	 * Constructor
	 */
	public PolygonEditor() {
		fileLayerStoreEventListener = evt -> {
			// Ensure in main thread
			Display display = Display.getDefault();
			if (display.getThread() == Thread.currentThread()) {
				refreshPolygonFileList();
			} else {
				display.asyncExec(this::refreshPolygonFileList);
			}
		};
	}

	/**
	 * Executed after dependency injection is done to perform any initialization
	 */
	@PostConstruct
	void postConstruct(@org.eclipse.e4.core.di.annotations.Optional Composite parent, EPartService partService) {
		createUI(parent);

		controller.activate();
		onPolygonFileChanged(controller.getCurrentPolygonFile());

		setDirty(controller.isDirty());
		fileLayerStoreModel.addListener(fileLayerStoreEventListener);
	}

	/** Close the view */
	@PreDestroy
	private void preDestroy(EPartService partService) {
		fileLayerStoreModel.removeListener(fileLayerStoreEventListener);
		controller.deactivate();
	}

	/**
	 * Creating SWT presentation
	 */
	private void createUI(Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		Group grpFile = new Group(parent, SWT.NONE);
		grpFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpFile.setText("File");
		grpFile.setLayout(new GridLayout(4, false));

		// Polygon file selection
		comboPolygonFileViewer = new ComboViewer(grpFile, SWT.READ_ONLY);
		GridDataFactory.fillDefaults().hint(400, SWT.DEFAULT).applyTo(comboPolygonFileViewer.getCombo());

		btnNewFile = ButtonFactory.newButton(SWT.PUSH)//
				.text("Create or load file...") //
				.tooltip("Create a new polygon file") //
				.onSelect(e -> controller.loadOrCreateOneFile()) //
				.create(grpFile);

		btnSave = ButtonFactory.newButton(SWT.PUSH)//
				.image(ResourceManager.getPluginImage("fr.ifremer.3dviewer", "icons/16/document-save.png")) //
				.text("Save") //
				.tooltip("Save polygon file") //
				.enabled(false) //
				.onSelect(e -> save()) //
				.create(grpFile);

		ButtonFactory.newButton(SWT.PUSH)//
				.image(ResourceManager.getPluginImage("fr.ifremer.3dviewer", "icons/16/document-save-as.png")) //
				.text("Save as...") //
				.tooltip("Save polygon as...") //
				.onSelect(e -> controller.saveAs()) //
				.create(grpFile);

		comboPolygonFileViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboPolygonFileViewer.setLabelProvider(LabelProvider.createTextProvider(p -> {
			PolygonFileInfo fileInfo = (PolygonFileInfo) p;
			return controller.isDirtyFile(fileInfo) ? fileInfo.getPath() + " *" : fileInfo.getPath();
		}));
		comboPolygonFileViewer
				.addSelectionChangedListener(e -> controller.requestPolygonFileEdition(getSelectedFileInfo()));

		// Table of polygons
		Label lblLines = new Label(parent, SWT.NONE);
		lblLines.setText("Polygons");

		polygonTableComposite = new PolygonTableComposite(parent, SWT.NONE, controller);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(polygonTableComposite);
	}

	/**
	 * @return the current selected polygon file. Null if none
	 */
	private PolygonFileInfo getSelectedFileInfo() {
		return (PolygonFileInfo) comboPolygonFileViewer.getStructuredSelection().getFirstElement();
	}

	/** Handles the TOPIC_POLYGON_FILE_CHANGED event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPolygonFileChanged(
			@UIEventTopic(PolygonTopics.TOPIC_POLYGON_FILE_CHANGED) PolygonFileInfo polygonFile) {
		if (!Objects.equals(getSelectedFileInfo(), polygonFile)) {
			refreshPolygonFileList();
		} else {
			refreshPolygonList();
		}

		refreshState();
	}

	/** Handles the TOPIC_POSITIONS_CHANGED event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onDirtyChanged(@UIEventTopic(PolygonTopics.TOPIC_IS_DIRTY) boolean dirty) {
		if (!btnSave.isDisposed()) {
			refreshState();
			comboPolygonFileViewer.refresh();
		}
	}

	/** Handles the TOPIC_POLYGON_CHANGED event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPolygonChanged(@UIEventTopic(PolygonTopics.TOPIC_POLYGON_CHANGED) IPolygon polygon) {
		UIUtils.asyncExecSafety(comboPolygonFileViewer, () -> {
			refreshState();
			polygonTableComposite.refresh();
			polygonTableComposite.selectPolygon(polygon);
		});
	}

	/** Refresh the states of all buttons */
	private void refreshState() {
		if (!btnNewFile.isDisposed()) {
			setDirty(controller.isDirty());
			btnNewFile.setEnabled(!controller.isCreationInProgress());

			PolygonFileInfo currentFileInfo = getSelectedFileInfo();
			btnSave.setEnabled(controller.isDirtyFile(currentFileInfo)
					&& !Objects.equals(controller.getInMemoryPolygonFileInfo(), currentFileInfo));
		}
	}

	/**
	 * Save the current edited file
	 */
	private void save() {
		controller.save();
		refreshState();
	}

	/**
	 * Save the work before closing the view
	 */
	@Persist
	private void saveBeforeClosing() {
		controller.saveAllDirty();
	}

	/** Reload the list of polygon file from the model */
	private void refreshPolygonFileList() {
		if (!comboPolygonFileViewer.getControl().isDisposed()) {
			var polygonFiles = fileLayerStoreModel.getAll().stream()//
					.map(WWFileLayerStore::getFileInfo) //
					.filter(PolygonFileInfo.class::isInstance) //
					.map(PolygonFileInfo.class::cast)//
					.collect(Collectors.toList());

			// sort polygon file by alphabetical order
			polygonFiles.sort((p1, p2) -> p1.getFilename().compareTo(p2.getFilename()));

			// Add the In memory polygon file
			polygonFiles.add(0, controller.getInMemoryPolygonFileInfo());

			// Configure combo
			comboPolygonFileViewer.setInput(polygonFiles);
			comboPolygonFileViewer.setSelection(new StructuredSelection(controller.getCurrentPolygonFile()));

			refreshPolygonList();
		}
	}

	/** Reload the list of the polygon from the current file */
	private void refreshPolygonList() {
		var polygonFile = controller.getCurrentPolygonFile();
		if (!polygonTableComposite.isDisposed()) {
			polygonTableComposite.fitToPolygons(polygonFile);
		}
		var polygonOpt = controller.getEditedPolygon();
		selectPolygon(polygonOpt.orElse(null));
	}

	/** Change the current selected polygon if different */
	private void selectPolygon(IPolygon polygon) {
		polygonTableComposite.selectPolygon(polygon);
	}

	/** Set dirty flag on Part */
	private void setDirty(boolean dirty) {
		mPart.setDirty(dirty);
	}

}
