/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.gui.polygoneditor;

import java.util.Objects;
import java.util.function.Function;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.ui.TableViewerColumnSorter;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.viewers.StringEditingSupport;

/**
 * TableViewer to represent the list of polygon
 */
public class PolygonTableComposite extends Composite {

	/** Controller */
	private final PolygonController controller;

	/** Widgets **/
	private TableViewer tableViewer;

	/**
	 * Constructor
	 */
	public PolygonTableComposite(Composite parent, int style, PolygonController controller) {
		super(parent, style);
		this.controller = controller;
		createUI();
	}

	/** Change the content of the table to fit to positions of the specified polygon */
	public void fitToPolygons(PolygonFileInfo polygonFile) {
		if (!tableViewer.getTable().isDisposed()) {
			var polygons = polygonFile.getPolygons();
			tableViewer.setInput(polygons);
			tableViewer.refresh();

			if (!polygons.isEmpty()) {
				controller.requestPolygonEdition(polygonFile, polygons.get(0));
			}
		}
	}

	/**
	 * Build UI.
	 */
	private void createUI() {
		GridLayoutFactory.fillDefaults().numColumns(4).margins(0, 0).applyTo(this);

		// create table
		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION | SWT.SINGLE);
		Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		GridDataFactory.fillDefaults().grab(true, true).span(4, 1).applyTo(table);
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		table.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			if (!tableViewer.isCellEditorActive()) {
				controller.requestPolygonEdition(controller.getCurrentPolygonFile(), getSelectPolygon());
			}
		}));

		// create columns
		buildColmuns();

		// Add a listener to remove selected lines with "Delete" key
		Listener removeSelection = e -> controller.requestPolygonDeletion(controller.getCurrentPolygonFile(),
				getSelectPolygon());
		table.addListener(SWT.KeyDown, e -> {
			if (e.keyCode == SWT.DEL)
				removeSelection.handleEvent(e);
		});

		// buttons
		Button btnGoto = new Button(this, SWT.NONE);
		btnGoto.setToolTipText("Zoom on selected polygon");
		btnGoto.setText("Go to");
		btnGoto.addListener(SWT.Selection, e -> {
			IPolygon polygon = (IPolygon) tableViewer.getStructuredSelection().getFirstElement();
			if (polygon != null) {
				IGeographicViewService.grab().zoomToGeobox(polygon.getGeobox());
			}
		});

		Button btnAdd = new Button(this, SWT.NONE);
		btnAdd.setText("Add");
		btnAdd.setToolTipText("Enter the polygon creation mode");
		btnAdd.addListener(SWT.Selection, e -> controller.createNewPolygon());

		Button btnRemove = new Button(this, SWT.NONE);
		btnRemove.setText("Remove");
		btnRemove.setToolTipText("Remove selected polygon");
		btnRemove.addListener(SWT.Selection, removeSelection);

		Button btnRemoveAll = new Button(this, SWT.NONE);
		btnRemoveAll.setText("Remove all");
		btnRemoveAll.setToolTipText("Remove all polygons");
		btnRemoveAll.addListener(SWT.Selection, e -> controller.requestPolygonsDeletion());
	}

	/**
	 * Builds columns.
	 */
	private void buildColmuns() {
		ILatLongFormatter formatter = LatLongFormater.getFormatter();
		TableViewerColumn nameTVC = buildColumn("Name", 200, IPolygon::getName);
		nameTVC.setEditingSupport(
				new StringEditingSupport<IPolygon>(tableViewer, IPolygon::getName, controller::changePolygonName));

		buildColumn(IConstants.NorthLatitude, 120,
				p -> !p.getGeobox().isEmpty() ? formatter.formatLatitude(p.getGeobox().getTop()) : "");
		buildColumn(IConstants.WestLongitude, 100,
				p -> !p.getGeobox().isEmpty() ? formatter.formatLongitude(p.getGeobox().getLeft()) : "");
		buildColumn(IConstants.SouthLatitude, 100,
				p -> !p.getGeobox().isEmpty() ? formatter.formatLatitude(p.getGeobox().getBottom()) : "");
		buildColumn(IConstants.EastLongitude, 100,
				p -> !p.getGeobox().isEmpty() ? formatter.formatLongitude(p.getGeobox().getRight()) : "");
		buildColumn("Surface", 100, p -> {
			String result = "";
			double surface = p.getSurface();
			if (surface > 1_000_000d) {
				result = String.format("%.2f km²", surface / 1_000_000d);
			} else if (surface > 0d) {
				result = String.format("%.2f m²", surface);
			}
			return result;
		});

		TableViewerColumn commentTVC = buildColumn("Comment", 200, IPolygon::getComment);
		commentTVC.setEditingSupport(new StringEditingSupport<IPolygon>(tableViewer, IPolygon::getComment,
				controller::changePolygonComment));
	}

	private TableViewerColumn buildColumn(String title, int size, Function<IPolygon, String> labelProvider) {
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		tableViewerColumn
				.setLabelProvider(ColumnLabelProvider.createTextProvider(e -> labelProvider.apply((IPolygon) e)));

		TableColumn tableColumn = tableViewerColumn.getColumn();
		tableColumn.setWidth(size);
		tableColumn.setText(title);
		tableColumn.setMoveable(true);

		// sort
		new TableViewerColumnSorter(tableViewerColumn);

		return tableViewerColumn;
	}

	/** @return the current selected polygon or null when none */
	public IPolygon getSelectPolygon() {
		return (IPolygon) tableViewer.getStructuredSelection().getFirstElement();
	}

	/** Change the current selected polygon if different */
	public void selectPolygon(IPolygon polygon) {
		if (!Objects.equals(getSelectPolygon(), polygon)) {
			tableViewer.setSelection(polygon != null ? new StructuredSelection(polygon) : StructuredSelection.EMPTY);
		}
	}

	/** Refresh the table. List of polygon may have changed */
	public void refresh() {
		if (!tableViewer.getTable().isDisposed()) {
			tableViewer.refresh();
		}
	}

}
