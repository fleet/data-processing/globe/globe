//REQ-IHM-004 - Markers Filtering

package fr.ifremer.viewer3d.gui;

import java.beans.PropertyChangeSupport;

import org.eclipse.swt.widgets.Group;

import fr.ifremer.viewer3d.layers.plume.filter.GenericFilter;
import fr.ifremer.viewer3d.layers.plume.filter.MinMaxFilter;

public class FilterUIFactory {

	public static GenericFilterUI createFilterUI(String filterType, Group group, String label, String signal, PropertyChangeSupport pcs, int precision, GenericFilter genericFilter) {
		GenericFilterUI filter = null;

		if (genericFilter instanceof MinMaxFilter) {
			filter = new MinMaxFilterUI(group, label, signal, pcs, precision, (MinMaxFilter) genericFilter);
		}

		return filter;
	}

}