package fr.ifremer.viewer3d.tile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.OperationCanceledException;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.ProgressCallback;
import org.gdal.gdal.TranslateOptions;
import org.gdal.gdal.WarpOptions;
import org.gdal.gdal.gdal;
import org.gdal.osr.osrConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.programs.GdalWarp;
import fr.ifremer.globe.gdal.programs.Photometric;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Tile;

/**
 * Retiles a tiff file and build a tiled pyramid
 */
public class GdalTileProducer {

	/** Logger for this class. */
	private static Logger logger = LoggerFactory.getLogger(GdalTileProducer.class);

	/** Tile producer delegator */
	protected IDelegator delegator;

	/** Constructor */
	public GdalTileProducer(IDelegator delegator) {
		this.delegator = delegator;
	}

	/** Let me produce the tiles... */
	public void tilePyramid(LevelSet levelSet) throws IOException {
		logger.info("Start the tile pyramid production");
		long startTime = System.currentTimeMillis();

		// Loop over each level of the pyramid
		ArrayList<Level> levels = levelSet.getLevels();

		AtomicReference<IOException> exception = new AtomicReference<>();
		levels.stream().forEach(level -> {
			GdalDataset inputDataset = delegator.openInputFile();
			inputDataset.ifPresentOrElse(//
					dataset -> {
						try {
							tilePyramidLevel(levelSet, level, dataset);
						} catch (IOException e) {
							exception.set(e);
						}
					}, //
					() -> exception.set(new IOException(gdal.GetLastErrorMsg())));
		});

		if (exception.get() != null) {
			throw exception.get();
		}

		logger.info("End of tile pyramid producting in {} ms", System.currentTimeMillis() - startTime);
	}

	/**
	 * Produce the tiles for the specified pyramid level
	 *
	 * @param LevelSet of the level
	 * @param level Level to consider
	 * @param inputDataset Dataset opened on the tif file to tile
	 */
	protected void tilePyramidLevel(LevelSet levelSet, Level level, Dataset inputDataset) throws IOException {
		logger.info("Start the tiling level {} ", level.getLevelName());
		long levelStartTime = System.currentTimeMillis();

		Sector sector = levelSet.getSector();

		Angle dLat = level.getTileDelta().getLatitude();
		Angle dLon = level.getTileDelta().getLongitude();
		Angle latOrigin = levelSet.getTileOrigin().getLatitude();
		Angle lonOrigin = levelSet.getTileOrigin().getLongitude();
		int firstRow = Tile.computeRow(dLat, sector.getMinLatitude(), latOrigin);
		int firstCol = Tile.computeColumn(dLon, sector.getMinLongitude(), lonOrigin);
		int lastRow = Tile.computeRow(dLat, sector.getMaxLatitude(), latOrigin);
		int lastCol = Tile.computeColumn(dLon, sector.getMaxLongitude(), lonOrigin);

		Angle p1 = Tile.computeRowLatitude(firstRow, dLat, latOrigin);
		for (int row = firstRow; row <= lastRow; row++) {
			logger.debug("Producing row {} for level {}", level.getLevelName(), row);
			long rowStartTime = System.currentTimeMillis();
			Angle p2 = p1.add(dLat);
			Angle t1 = Tile.computeColumnLongitude(firstCol, dLon, lonOrigin);
			for (int col = firstCol; col <= lastCol; col++) {
				Angle t2 = t1.add(dLon);

				Tile tile = new Tile(new Sector(p1, p2, t1, t2), level, row, col);
				if (".png".equals(level.getFormatSuffix())) {
					produceOneImageTile(inputDataset, level, tile);
				} else {
					produceOneTerrainTile(inputDataset, level, tile);
				}
				delegator.oneTileProduced();
				t1 = t2;
			}
			logger.info("End of row's producting ({}) for level {} in {} ms", row, level.getLevelName(),
					System.currentTimeMillis() - rowStartTime);
			p1 = p2;
		}

		logger.info("End of the level's tiling in {} ms", System.currentTimeMillis() - levelStartTime);
	}

	/**
	 * Compute an elevation tiff suitable for the level.
	 *
	 * @param inputDataset Dataset get from the input file
	 * @param level Level to consider
	 * @param tile tile to produce
	 */
	protected void produceOneTerrainTile(Dataset inputDataset, Level level, Tile tile) throws IOException {
		File outputFile = new File(delegator.getProductionParams().getStringValue(AVKey.FILE_STORE_LOCATION),
				tile.getPath());
		outputFile.getParentFile().mkdirs();
		GdalWarp gdalWarp = new GdalWarp(inputDataset, outputFile.getPath());
		gdalWarp.addMetadata(false);
		gdalWarp.addTargetProjection(GdalOsrUtils.SRS_WGS84);
		gdalWarp.addOverwrite(true);
		gdalWarp.addGeobox(GeoBoxUtils.asGdalExtent(tile.getSector()));
		gdalWarp.addTargetType("Float32");
		gdalWarp.addTargetSize(tile.getWidth(), tile.getHeight());
		if (inputDataset.getRasterCount() >= 3) {
			gdalWarp.addPhotometric(Photometric.RGB); // to avoid WW warning
			gdalWarp.addTargetNoData(GdalTiledTerrainProducer.NO_DATA_VALUE);
		} else {
			// TODO. To remove at the deletion of GdalTiledElevationProducer
			gdalWarp.addTargetNoData(-32768f);
		}
		try (GdalDataset result = GdalDataset.of(gdalWarp.run())) {
			if (result.isEmpty()) {
				if (!delegator.isCancelled()) {
					throw new IOException(gdal.GetLastErrorMsg());
				} else {
					throw new OperationCanceledException();
				}
			}
			// Delete empty tile
			result.deleteOnClose(result.isBandEmpty(1));
		}
	}

	/**
	 * Compute an image PNG suitable for the level.
	 *
	 * @param inputDataset Dataset get from the input file
	 * @param level Level to consider
	 * @param tile tile to produce
	 */
	protected void produceOneImageTile(Dataset inputDataset, Level level, Tile tile) throws IOException {
		Dataset expandedDataset = null;
		String expandedFile = null;

		// Specific case : tif with palette
		if (inputDataset.getRasterCount() == 1) {
			expandedFile = "/vsimem/rgba/" + FilenameUtils.removeExtension(tile.getPath()) + ".tif";
			expandedDataset = expandPaletteImage(inputDataset, level, tile, expandedFile);
		}

		// Unproject tile
		String tileFile = "/vsimem/" + FilenameUtils.removeExtension(tile.getPath()) + ".tif";
		Dataset tileDataset = unprojectAndSizeTile(expandedDataset != null ? expandedDataset : inputDataset, tile,
				tileFile);

		// Write PNG
		File pngFile = translateImageTileToPNG(tileDataset, level, tile);
		// Delete geo extra file
		FileUtils.deleteQuietly(new File(pngFile.getAbsolutePath() + ".aux.xml"));

		// clean
		tileDataset.delete();
		gdal.Unlink(tileFile);
		if (expandedFile != null) {
			gdal.Unlink(expandedFile);
		}
		if (expandedDataset != null) {
			expandedDataset.delete();
		}

	}

	/**
	 * Expose the dataset with 1 band with a color table as a dataset with 4 (RGBA) bands.
	 *
	 * @param inputDataset Dataset get from the input file
	 * @param level Level to consider
	 * @param tile tile to produce
	 * @param outputFile where to write the result
	 * @return the Gdal dataset opened on the outputFile
	 */
	protected Dataset expandPaletteImage(Dataset inputDataset, Level level, Tile tile, String outputFile)
			throws IOException {
		Sector sector = tile.getSector();
		Vector<String> options = new Vector<>();
		options.add("-ot");
		options.add("Byte");
		options.add("-projwin"); // Set georeferenced extents of output file
		options.add(String.valueOf(sector.getMinLongitude().degrees));
		options.add(String.valueOf(sector.getMaxLatitude().degrees));
		options.add(String.valueOf(sector.getMaxLongitude().degrees));
		options.add(String.valueOf(sector.getMinLatitude().degrees));
		options.add("-projwin_srs"); // SRS in which to interpret the coordinates given with -projwin
		options.add(osrConstants.SRS_WKT_WGS84_LAT_LONG);
		options.add("-expand");
		options.add("rgba");

		TranslateOptions translateOptions = new TranslateOptions(options);
		Dataset result = gdal.Translate(outputFile, inputDataset, translateOptions);
		translateOptions.delete();

		if (result == null) {
			if (!delegator.isCancelled()) {
				throw new IOException(gdal.GetLastErrorMsg());
			} else {
				throw new OperationCanceledException();
			}
		}

		return result;
	}

	/**
	 * Unproject the dataset and into a 512x512 image tile.
	 *
	 * @param inputDataset Dataset get from the input file
	 * @param tile tile to produce
	 * @param outputFile where to write the result
	 * @return the Gdal dataset opened on the outputFile
	 */
	protected Dataset unprojectAndSizeTile(Dataset inputDataset, Tile tile, String outputFile) throws IOException {
		Sector sector = tile.getSector();
		Vector<String> options = new Vector<>();
		options.add("-t_srs"); // Set target spatial reference
		options.add(osrConstants.SRS_WKT_WGS84_LAT_LONG);
		options.add("-te"); // Set georeferenced extents of output file
		options.add(String.valueOf(sector.getMinLongitude().degrees));
		options.add(String.valueOf(sector.getMinLatitude().degrees));
		options.add(String.valueOf(sector.getMaxLongitude().degrees));
		options.add(String.valueOf(sector.getMaxLatitude().degrees));
		options.add("-te_srs"); // SRS in which to interpret the coordinates given with -te
		options.add(osrConstants.SRS_WKT_WGS84_LAT_LONG);
		options.add("-ts"); // Set output file size in pixels and lines.
		options.add(String.valueOf(tile.getWidth()));
		options.add(String.valueOf(tile.getHeight()));
		options.add("-nomd"); // Do not copy metadata.

		// RGB without alpha ?
		if (inputDataset.GetRasterCount() == 3) {
			options.add("-dstalpha"); // Create an output alpha band to identify nodata.
			options.add("-srcnodata"); // Set nodata masking values for input bands.
			options.add("0"); // Black as nodata values.
			options.add("0");
			options.add("0");
		}

		WarpOptions warpOptions = new WarpOptions(options);
		Dataset result = gdal.Warp(outputFile, new Dataset[] { inputDataset }, warpOptions, newProgressCallback());
		warpOptions.delete();
		if (result == null) {
			if (!delegator.isCancelled()) {
				throw new IOException(gdal.GetLastErrorMsg());
			} else {
				throw new OperationCanceledException();
			}
		}
		result.FlushCache();

		return result;
	}

	/**
	 * Export the tile as a PNG image.
	 *
	 * @param inputDataset Dataset get from the input file
	 * @param level Level to consider
	 * @param tile tile to produce
	 * @return the Gdal dataset opened on the outputFile
	 */
	protected File translateImageTileToPNG(Dataset inputDataset, Level level, Tile tile) throws IOException {
		File result = new File(delegator.getProductionParams().getStringValue(AVKey.FILE_STORE_LOCATION),
				tile.getPath());
		result.getParentFile().mkdirs();

		Vector<String> options = new Vector<>();
		options.add("-ot");
		options.add("Byte");
		options.add("-of");
		options.add("PNG");

		TranslateOptions translateOptions = new TranslateOptions(options);
		Dataset tileDataset = gdal.Translate(result.getAbsolutePath(), inputDataset, translateOptions);
		translateOptions.delete();

		if (tileDataset == null) {
			if (!delegator.isCancelled()) {
				throw new IOException(gdal.GetLastErrorMsg());
			} else {
				throw new OperationCanceledException();
			}
		}

		tileDataset.delete();

		return result;
	}

	/**
	 * @return the number of horizontal tiles to cover the level
	 */
	protected int computeXTilesCount(LevelSet levelSet, Level level) {
		Sector sector = levelSet.getSector();
		Angle dLon = level.getTileDelta().getLongitude();
		Angle lonOrigin = levelSet.getTileOrigin().getLongitude();
		int firstCol = Tile.computeColumn(dLon, sector.getMinLongitude(), lonOrigin);
		int lastCol = Tile.computeColumn(dLon, sector.getMaxLongitude(), lonOrigin);
		return lastCol - firstCol + 1;
	}

	/**
	 * @return the number of horizontal tiles to cover the level
	 */
	protected int computeYTilesCount(LevelSet levelSet, Level level) {
		Sector sector = levelSet.getSector();
		Angle dLat = level.getTileDelta().getLatitude();
		Angle latOrigin = levelSet.getTileOrigin().getLatitude();
		int firstRow = Tile.computeRow(dLat, sector.getMinLatitude(), latOrigin);
		int lastRow = Tile.computeRow(dLat, sector.getMaxLatitude(), latOrigin);
		return lastRow - firstRow + 1;
	}

	/**
	 * Compute the width of the level
	 *
	 * @param LevelSet of the level
	 * @param level Level to consider
	 * @return width in pixels
	 */
	protected int computeLevelWidth(LevelSet levelSet, Level level) {
		Sector sector = levelSet.getSector();

		Angle dLon = level.getTileDelta().getLongitude();
		Angle lonOrigin = levelSet.getTileOrigin().getLongitude();
		int firstCol = Tile.computeColumn(dLon, sector.getMinLongitude(), lonOrigin);
		int lastCol = Tile.computeColumn(dLon, sector.getMaxLongitude(), lonOrigin);

		return (lastCol - firstCol + 1) * level.getTileWidth();
	}

	/** Tile producer delegator, the calling object */
	public interface IDelegator {

		/** @return the Dataset opened of the tif file to tile */
		GdalDataset openInputFile();

		/** @return all production parameters */
		AVList getProductionParams();

		/** Inform that one tile has been produced */
		void oneTileProduced();

		/** @return true if cancel is required by the user */
		boolean isCancelled();
	}

	/**
	 * @return ProgressCallback to cancel current process if needed
	 */
	public ProgressCallback newProgressCallback() {
		return new ProgressCallback() {
			/**
			 * Follow the link.
			 *
			 * @see org.gdal.gdal.ProgressCallback#run(double, java.lang.String)
			 */
			@Override
			public int run(double complete, String message) {
				return delegator.isCancelled() ? 0 : 1;
			}
		};
	}

}
