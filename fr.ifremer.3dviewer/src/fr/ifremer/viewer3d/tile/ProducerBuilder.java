/**
 * GLOBE - Ifremer
 */

package fr.ifremer.viewer3d.tile;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.math.Range;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.ui.service.worldwind.tile.ITilesProducerBuilder;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;

/**
 * Builder of IImageProducer
 */
abstract class ProducerBuilder implements ITilesProducerBuilder {

	/** Cache suffix */
	protected final String cacheSuffix;

	/** Folder of Nasa cache */
	protected File tilesLocation = null;
	/** Folder of tiles */
	protected String datasetName = null;
	/** Number of level. Minimum 1. When > 1, generates a pyramid */
	protected int numLevels = 0;
	/** Min and max values in thi original raster (for computing palette) */
	protected Range minMax = null;
	/** Unit of min and max values */
	protected String unit = null;
	/** GeoBox */
	protected GeoBox geoBox = null;

	/**
	 * Constructor
	 */
	protected ProducerBuilder(String cacheSuffix) {
		this.cacheSuffix = cacheSuffix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Document> run(IProgressMonitor progress) throws GIOException {
		if (geoBox == null || !geoBox.isSpanning180Th()) {
			return List.of(performOneTilePyramid(progress));
		} else {
			return performTwoTilePyramids(progress);
		}
	}

	/** Generates 2 tile pyramids (east and west) */
	private List<Document> performTwoTilePyramids(IProgressMonitor progress) throws GIOException {
		var westGeobox = new GeoBox(geoBox.getTop(), geoBox.getBottom(), 180d, geoBox.getLeft());
		var westdatasetName = datasetName + "_West";
		var eastGeobox = new GeoBox(geoBox.getTop(), geoBox.getBottom(), geoBox.getRight(), -180d);
		var eastdatasetName = datasetName + "_East";

		SubMonitor subMonitor = SubMonitor.convert(progress, 200);
		List<Document> result = new LinkedList<>();

		geoBox = eastGeobox;
		datasetName = eastdatasetName;
		result.add(performOneTilePyramid(subMonitor.split(100)));

		geoBox = westGeobox;
		datasetName = westdatasetName;
		result.add(performOneTilePyramid(subMonitor.split(100)));
		progress.done();

		return result;
	}

	/** Generates a single tile pyramid */
	private Document performOneTilePyramid(IProgressMonitor progress) throws GIOException {
		AVList parameters = new AVListImpl();
		if (numLevels > 0) {
			parameters.setValue(AVKey.NUM_LEVELS, numLevels);
		}

		if (tilesLocation != null) {
			parameters.setValue(AVKey.FILE_STORE_LOCATION, tilesLocation.getAbsolutePath());
		} else {
			parameters.setValue(AVKey.FILE_STORE_LOCATION, WorldWindUtils.getCacheLocation().getAbsolutePath());
		}

		if (datasetName == null) {
			datasetName = String.valueOf(System.currentTimeMillis());
		}
		parameters.setValue(AVKey.DATA_CACHE_NAME, NasaCache.getCacheDirectory(datasetName, cacheSuffix));
		parameters.setValue(AVKey.DATASET_NAME, datasetName);

		if (minMax != null) {
			parameters.setValue(AVKey.ELEVATION_MIN, minMax.getMinimumNumber().toString());
			parameters.setValue(AVKey.ELEVATION_MAX, minMax.getMaximumNumber().toString());
		}
		if (unit != null) {
			parameters.setValue("Unit", unit);
		}
		if (geoBox != null) {
			parameters.setValue(AVKey.SECTOR, GeoBoxUtils.convert(geoBox));
		}

		return perform(parameters, progress);
	}

	/** Process delegation */
	abstract Document perform(AVList parameters, IProgressMonitor progress) throws GIOException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder setTilesLocation(File tilesLocation) {
		this.tilesLocation = tilesLocation;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder setDatasetName(String datasetName) {
		this.datasetName = datasetName;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder setNumLevels(int numLevels) {
		this.numLevels = numLevels;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder setMinMax(Range minMax) {
		this.minMax = minMax;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder setUnit(String unit) {
		this.unit = unit;
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public ITilesProducerBuilder setGeoBox(GeoBox geoBox) {
		this.geoBox = geoBox;
		return this;
	}

}
