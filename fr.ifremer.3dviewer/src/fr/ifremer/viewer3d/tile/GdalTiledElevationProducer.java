/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.tile;

import java.io.File;
import java.io.IOException;

import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.viewer3d.tile.GdalTileProducer.IDelegator;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.TiledElevationProducer;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.LevelSet;

/**
 * Specific TiledElevationProducer to produce an elevation tiles pyramid in a Tif format
 */
@Deprecated(forRemoval = true)
public class GdalTiledElevationProducer extends TiledElevationProducer implements IDelegator {

	/** File to tile */
	protected File elevationFile;

	/** Constructor */
	public GdalTiledElevationProducer() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public void offerDataSource(Object elevationFile, AVList params) {
		super.offerDataSource(elevationFile, null);
		this.elevationFile = (File) elevationFile;
	}

	/** {@inheritDoc} */
	@Override
	protected void initLevelSetParameters(AVList params) {
		super.initLevelSetParameters(params);
		// Custom sector ?
		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		if (sector != null && sector.getMaxLongitude().degrees == 180d) {
			initLevelSetParametersTo180thMeridian(params);
		}
	}

	/**
	 * Adapt the production paramaters for a sector reaching the 180th meridian <br>
	 * Tile origin is computed to reach the 180th meridian at the last tile
	 */
	protected void initLevelSetParametersTo180thMeridian(AVList params) {

		Integer tileWidth = (Integer) params.getValue(AVKey.TILE_WIDTH);
		Integer tileHeight = (Integer) params.getValue(AVKey.TILE_HEIGHT);
		LatLon rasterTileDelta = computeRasterTileDelta(tileWidth, tileHeight, getDataRasters());
		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		LatLon desiredLevelZeroDelta = computeDesiredTileDelta(sector);

		boolean isDataSetLarge = isDataSetLarge(getDataRasters(), 3000);
		int numLevels = isDataSetLarge ? computeNumLevels(desiredLevelZeroDelta, rasterTileDelta) : 1;
		params.setValue(AVKey.NUM_LEVELS, numLevels);

		double scale = Math.pow(2d, numLevels - 1d);
		LatLon levelZeroTileDelta = LatLon.fromDegrees(scale * rasterTileDelta.getLatitude().degrees,
				scale * rasterTileDelta.getLongitude().degrees);
		params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);

		double tilesCount = Math.ceil(sector.getDeltaLonDegrees() / rasterTileDelta.getLongitude().degrees);
		double origineLon = 180d - tilesCount * rasterTileDelta.getLongitude().degrees;

		LatLon tileOrigin = new LatLon(sector.getMinLatitude(), Angle.fromDegrees(origineLon));
		params.setValue(AVKey.TILE_ORIGIN, tileOrigin);
	}

	/** {@inheritDoc} */
	@Override
	protected void initProductionParameters(AVList params) {
		super.initProductionParameters(params);

		// We want tiles in tif format (not bil)
		params.setValue(AVKey.FORMAT_SUFFIX, ".tif");
		// Store elevation as float
		params.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
	}

	/** Let me produce the tiles... */
	@Override
	protected void installLevelSet(LevelSet levelSet, AVList params) throws IOException {
		// Initialize progression as super
		calculateTileCount(levelSet, params);
		startProgress();

		// Let GDalTileProducer producing the tile instead of WW implementation
		GdalTileProducer delegate = new GdalTileProducer(this);
		try {
			delegate.tilePyramid(levelSet);
		} catch (IOException e) {
			stopProduction();
			throw e;
		}
	}

	/** {@inheritDoc} */
	@Override
	public GdalDataset openInputFile() {
		return GdalDataset.open(elevationFile.getAbsolutePath());
	}

	/** {@inheritDoc} */
	@Override
	public synchronized void oneTileProduced() {
		updateProgress();
	}

	/** {@inheritDoc} */
	@Override
	public AVList getProductionParams() {
		return productionParams;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isCancelled() {
		return isStopped();
	}

}
