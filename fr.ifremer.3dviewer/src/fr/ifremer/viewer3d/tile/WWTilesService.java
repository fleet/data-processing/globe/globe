/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.tile;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.ui.service.worldwind.tile.ITilesProducerBuilder;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.TiledRasterProducer;
import gov.nasa.worldwind.util.WWIO;

/**
 * Implementation of IWWTilesInstaller service
 */
@Component(name = "globe_viewer3d_tiles_service", service = IWWTilesService.class)
public class WWTilesService implements IWWTilesService {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(WWTilesService.class);

	public static final String INAGE_CACHE_SUFFIX = "_rgb";
	public static final String RASTER_CACHE_SUFFIX = "_float";
	@Deprecated(forRemoval = true)
	public static final String ELEVATION_CACHE_SUFFIX = "_elevation";

	/** @return how to present the file name in Nasa */
	@Override
	public String computeDisplayName(File file) {
		return WWIO.replaceIllegalFileNameCharacters(FilenameUtils.getBaseName(file.getName()));
	}

	@Override
	public List<File> getCacheFolders(String displayName) {
		File rootCacheFolder = new File(WorldWindUtils.getCacheLocation(), NasaCache.GLOBE_CACHE);
		String[] subFolders = rootCacheFolder.list((dir, name) -> name.startsWith(displayName));
		return subFolders != null ? Arrays.stream(subFolders).map(subFolder -> new File(rootCacheFolder, subFolder))
				.collect(Collectors.toList()) : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder makeImageProducerBuilder(File rgbFile) {
		return new ProducerBuilder(INAGE_CACHE_SUFFIX) {
			@Override
			Document perform(AVList parameters, IProgressMonitor progress) throws GIOException {
				progress.subTask("Building a tiled pyramid for the texture");
				return produceTiles(new GdalTiledImageProducer(), rgbFile, parameters, progress);
			}
		};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder makeElevationProducerBuilder(File demFile) {
		return new ProducerBuilder(ELEVATION_CACHE_SUFFIX) {
			@Override
			Document perform(AVList parameters, IProgressMonitor progress) throws GIOException {
				progress.subTask("Building a tiled pyramid for the elevation");
				return produceTiles(new GdalTiledElevationProducer(), demFile, parameters, progress);
			}
		};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITilesProducerBuilder makeTerrainProducerBuilder(GdalDataset gdalDataset, GdalDataset elevationDataset) {
		return new ProducerBuilder(RASTER_CACHE_SUFFIX) {
			@Override
			Document perform(AVList parameters, IProgressMonitor progress) throws GIOException {
				SubMonitor subMonitor = SubMonitor.convert(progress, 100);
				progress.subTask("Building a tiled pyramid for the raster");
				var producer = new GdalTiledTerrainProducer(subMonitor.split(40));
				return produceTiles(producer, new Pair<>(gdalDataset,elevationDataset), parameters, subMonitor.split(60));
			}
		};
	}

	/**
	 * Invoke the producer to build tiled pyramid levels
	 *
	 * @param producer delelagate producer
	 * @param dataSource File or GdalDataset
	 * @param storeParameters all parameters expected by the producer
	 * @param progress monitor of progression
	 * @return the ressuling XML document
	 * @throws GIOException production failed, error occured
	 */
	protected Document produceTiles(TiledRasterProducer producer, Object dataSource, AVList storeParameters,
			IProgressMonitor progress) throws GIOException {
		String displayName = storeParameters.getStringValue(AVKey.DATASET_NAME);
		try {
			// Configure the TiledImageProducer with the parameter list and the image source.
			producer.setStoreParameters(storeParameters);
			producer.offerDataSource(dataSource, null);
			WorldWindUtils.manageProgression(producer, progress);
			// Install the imagery.
			producer.startProduction();
		} catch (OperationCanceledException e) {
			producer.removeProductionState(); // Clean up on failure.
			throw e;
		} catch (Exception e) {
			logger.error("Error while creating the tiles pyramid of " + displayName, e);
		}

		if (progress.isCanceled()) {
			producer.removeProductionState(); // Clean up on failure.
			throw new OperationCanceledException();
		}

		// Extract the data configuration document from the installed results. If the installation successfully
		// completed, the TiledImageProducer should always contain a document in the production results, but test
		// the results anyway.
		Iterable<?> results = producer.getProductionResults();
		Document result = null;
		if (results != null && results.iterator() != null && results.iterator().hasNext()) {
			Object o = results.iterator().next();
			if (o instanceof Document) {
				result = (Document) o;
			}
		}

		if (result == null) {
			producer.removeProductionState(); // Clean up on failure.
			throw new GIOException("Unabled to produce the tiles pyramid for " + displayName + "\nLoading aborted");
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public File computeImageCacheDocument(String displayName) {
		String cacheFolder = NasaCache.getCacheDirectory(displayName, INAGE_CACHE_SUFFIX);
		return new File(WorldWindUtils.getCacheLocation(), cacheFolder + '/' + displayName + ".xml");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public File computeElevationCacheDocument(String displayName) {
		String cacheFolder = NasaCache.getCacheDirectory(displayName, ELEVATION_CACHE_SUFFIX);
		return new File(WorldWindUtils.getCacheLocation(), cacheFolder + '/' + displayName + ".xml");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<File> computeRasterCacheDocument(String displayName, GeoBox geoBox) {
		List<File> result = new LinkedList<>();
		String cacheFolder = NasaCache.getCacheDirectory(displayName, RASTER_CACHE_SUFFIX);
		if (geoBox.isSpanning180Th()) {
			result.add(new File(WorldWindUtils.getCacheLocation(), cacheFolder + '/' + displayName + "_west.xml"));
			result.add(new File(WorldWindUtils.getCacheLocation(), cacheFolder + '/' + displayName + "_east.xml"));
		} else {
			result.add(new File(WorldWindUtils.getCacheLocation(), cacheFolder + '/' + displayName + ".xml"));
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public void clearCache(File file) {
		String displayName = computeDisplayName(file);
		getCacheFolders(displayName).stream().forEach(FileUtils::deleteQuietly);
	}

}
