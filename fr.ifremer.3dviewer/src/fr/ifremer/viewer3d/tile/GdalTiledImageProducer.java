package fr.ifremer.viewer3d.tile;

import java.io.File;
import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTiledImageLayer;
import fr.ifremer.viewer3d.tile.GdalTileProducer.IDelegator;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.TiledImageProducer;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.WWXML;

/**
 * Retiles a RGB file and build a tiled pyramid using GDAL
 */
public class GdalTiledImageProducer extends TiledImageProducer implements IDelegator {

	/** File to tile */
	protected File rgbaFile;

	/** Constructor */
	public GdalTiledImageProducer() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public void offerDataSource(Object rgbaFile, AVList params) {
		super.offerDataSource(rgbaFile, null);
		this.rgbaFile = (File) rgbaFile;
	}

	/** {@inheritDoc} */
	@Override
	protected LatLon computeDesiredTileDelta(Sector sector) {
		return super.computeDesiredTileDelta(GeoBoxUtils.adaptIfSpanning180Th(sector));
	}

	/** {@inheritDoc} */
	@Override
	protected void initLevelSetParameters(AVList params) {
		super.initLevelSetParameters(params);
		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		if (GeoBoxUtils.isSpanning180Th(sector)) {
			// Size of tiles if degrees
			LatLon levelZeroTileDelta = (LatLon) params.getValue(AVKey.LEVEL_ZERO_TILE_DELTA);
			double deltaLon = levelZeroTileDelta.longitude.degrees;

			// Number of tiles before 180th meridian
			double originLon = sector.getMinLongitude().degrees;
			double nbTiles = Math.ceil((180.0 - originLon) / deltaLon);
			// Shift the tile's origin to fit with the 180th meridian
			originLon = 180.0 - nbTiles * deltaLon;
			params.setValue(AVKey.TILE_ORIGIN, new LatLon(sector.getMinLatitude(), Angle.fromDegrees(originLon)));
		}
	}

	/** Let me produce the tiles... */
	@Override
	protected void installLevelSet(LevelSet levelSet, AVList params) throws IOException {
		// Initialize progression as super
		calculateTileCount(levelSet, params);
		startProgress();

		// Let GDalTileProducer producing the tile instead of WW implementation
		GdalTileProducer delegate = new GdalTileProducer(this);
		try {
			delegate.tilePyramid(levelSet);
		} catch (IOException e) {
			stopProduction();
			throw e;
		}
	}

	/**
	 * Returns a Layer configuration document. <BR>
	 * Redefined to add attributes expected by the DtmLayer
	 */
	@Override
	protected Document createConfigDoc(AVList params) {
		Document result = super.createConfigDoc(params);

		Element context = result.getDocumentElement();
		Element el = WWXML.appendElementPath(context, "BathyscopeContrast");
		WWXML.checkAndAppendTextElement(params, MyTiledImageLayer.XML_KEY_UNIT, el, MyTiledImageLayer.XML_KEY_UNIT);
		WWXML.checkAndAppendTextElement(params, AVKey.ELEVATION_MIN, el, MyTiledImageLayer.XML_KEY_GlobalMinValue);
		WWXML.checkAndAppendTextElement(params, AVKey.ELEVATION_MAX, el, MyTiledImageLayer.XML_KEY_GlobalMaxValue);
		WWXML.checkAndAppendTextElement(params, AVKey.ELEVATION_MIN, el, MyTiledImageLayer.XML_KEY_MinValue);
		WWXML.checkAndAppendTextElement(params, AVKey.ELEVATION_MAX, el, MyTiledImageLayer.XML_KEY_MaxValue);

		return result;
	}

	/** {@inheritDoc} */
	@Override
	public GdalDataset openInputFile() {
		return GdalDataset.open(rgbaFile.getAbsolutePath());
	}

	/** {@inheritDoc} */
	@Override
	public synchronized void oneTileProduced() {
		updateProgress();
	}

	/** {@inheritDoc} */
	@Override
	public AVList getProductionParams() {
		return productionParams;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isCancelled() {
		return isStopped();
	}

}
