/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.tile;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.function.DoubleUnaryOperator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Dataset;
import org.gdal.osr.SpatialReference;
import org.gdal.osr.osrConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.dataset.GdalVirtualDataset;
import fr.ifremer.globe.gdal.programs.GdalAspect;
import fr.ifremer.globe.gdal.programs.GdalSlope;
import fr.ifremer.globe.gdal.programs.GdalWarp;
import fr.ifremer.globe.gdal.programs.GdalWarp.ResamplingMethod;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.viewer3d.tile.GdalTileProducer.IDelegator;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.TiledElevationProducer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.util.LevelSet;

/**
 * Specific TiledElevationProducer to produce a raster tiles pyramid in a Tif format
 */
public class GdalTiledTerrainProducer extends TiledElevationProducer implements IDelegator {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalTiledTerrainProducer.class);

	/** No data value used in tiles */
	public static final Double NO_DATA_VALUE = Double.valueOf(-Float.MAX_VALUE);

	/** Dataset used to produce the tiles. Tiff containing 4 bands (values/slope/cos aspect/sin aspect) */
	protected GdalDataset tilableDataset = GdalDataset.empty();

	/** All opened dataset, to closed at the end of the process */
	protected List<GdalDataset> openedDataset = new LinkedList<>();

	/** Progress monitor */
	private final IProgressMonitor progress;

	/** Constructor */
	public GdalTiledTerrainProducer(IProgressMonitor progress) {
		this.progress = progress;
	}

	/** {@inheritDoc} */
	@Override
	public void offerDataSource(Object source, AVList params) {

		Object sourceTexture = source;
		Object sourceElevation = source;
		if (source instanceof Pair<?, ?> compound) {
			sourceTexture = compound.getFirst();
			sourceElevation = compound.getSecond();
			// check elevation source
			if (!(sourceElevation instanceof GdalDataset inputDataset && inputDataset.isPresent()
					&& inputDataset.getFile().isPresent())) {
				sourceElevation = sourceTexture;
			}
		}

		if (sourceTexture instanceof GdalDataset inputDataset && inputDataset.isPresent()
				&& inputDataset.getFile().isPresent()) {
			try {
				var subMonitor = SubMonitor.convert(progress, 100);
				// Set the projectedDataset attribute
				var lonlatDataset = generatesLonLatFile(inputDataset, subMonitor.split(20));
				var elevationDataset = (sourceTexture == sourceElevation) ? lonlatDataset
						: generatesLonLatFile((GdalDataset) sourceElevation, subMonitor.split(20));
				File lonlatFile = lonlatDataset.getFile().orElseThrow();
				// Set the tilableDataset attribute
				generatesAspectAndSlopeFile(lonlatDataset, elevationDataset, subMonitor.split(80));

				super.offerDataSource(lonlatFile, params);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} else {
			logger.error("Input dataset is empty. Tiling process aborted");
		}
	}

	@Override
	protected String validateDataSource(Object source, AVList params) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	protected void initProductionParameters(AVList params) {

		// We want tiles in tif format (not bil)
		params.setValue(AVKey.FORMAT_SUFFIX, ".tif");

		// Store values as float
		params.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
		params.setValue(AVKey.MISSING_DATA_REPLACEMENT, NO_DATA_VALUE);

		super.initProductionParameters(params);

		params.setValue(AVKey.IMAGE_FORMAT, "image/tiff");
		params.setValue(AVKey.AVAILABLE_IMAGE_FORMATS, new String[] { "image/tiff" });
	}

	/**
	 * Overridden to initialize this producer's extreme elevations prior to creating and installing elevation tiles.
	 *
	 * @param parameters the installation parameters.
	 *
	 * @throws Exception if production fails for any reason.
	 */
	@Override
	protected void doStartProduction(AVList parameters) throws Exception {
		try {
			super.doStartProduction(parameters);
		} finally {
			tilableDataset.close();
			openedDataset.forEach(GdalDataset::close);
		}
	}

	/** Let me produce the tiles... */
	@Override
	protected void installLevelSet(LevelSet levelSet, AVList params) throws IOException {
		// Initialize progression as super
		calculateTileCount(levelSet, params);
		startProgress();

		// Let GDalTileProducer producing the tile instead of WW implementation
		GdalTileProducer delegate = new GdalTileProducer(this);
		try {
			delegate.tilePyramid(levelSet);
		} catch (IOException e) {
			stopProduction();
			throw e;
		}
	}

	@Override
	protected LatLon computeRasterTileDelta(int tileWidth, int tileHeight, Iterable<? extends DataRaster> rasters) {
		LatLon pixelSize = this.computeSmallestPixelSize(rasters);
		// Compute the tile size in latitude and longitude, given a raster's sector and dimension, and the tile
		// dimensions. In this computation a pixel is assumed to cover a finite area.
		double latDelta = tileHeight * pixelSize.getLatitude().degrees;
		double lonDelta = tileWidth * pixelSize.getLongitude().degrees;
		return LatLon.fromDegrees(latDelta, lonDelta);
	}

	@Override
	protected LatLon computeRasterPixelSize(DataRaster raster) {
		// Compute the raster's pixel dimension in latitude and longitude. In this computation a pixel is assumed to
		// cover a finite area.
		return LatLon.fromDegrees(raster.getSector().getDeltaLatDegrees() / raster.getHeight(),
				raster.getSector().getDeltaLonDegrees() / raster.getWidth());
	}

	/**
	 * Use gdal to produce file with a lonlat coordinate system
	 */
	private GdalDataset generatesLonLatFile(GdalDataset inputDataset, IProgressMonitor monitor) throws IOException {
		SpatialReference srs = inputDataset.getSpatialReference();
		Dataset dataset = inputDataset.get();

		// FIXME : In memory wraping doesn't work with .dtm.nc
		// GdalWarp gdalWarp = new GdalWarp(dataset, GdalUtils.generateInMemoryFilePath());
		GdalWarp gdalWarp = new GdalWarp(dataset,
				TemporaryCache.createTemporaryFile("Wrap_Wgs84_", ".tif").getAbsolutePath());
		gdalWarp.addTargetNoData(NO_DATA_VALUE);
		gdalWarp.addTargetType("Float32");
		gdalWarp.addMetadata(false);
		gdalWarp.addProgressMonitor(monitor);
		if (srs.IsProjected() == 1) {
			gdalWarp.addTargetProjection(GdalOsrUtils.SRS_WGS84);

			// resampling with 'near' method for byte, short, integer or long; else use 'bilinear'
			var dataType = inputDataset.getBandFeatures().getDataType();
			var resamplingMethod = (dataType == Byte.class || dataType == Short.class || dataType == Integer.class
					|| dataType == Long.class) ? ResamplingMethod.NEAR : ResamplingMethod.BILINEAR;
			gdalWarp.addResamplingMethod(resamplingMethod);

			// In case of Mercator projection (or any projection defining standard_parallel_1),
			// retrieve reference lat/lon to generate aligned grid on it
			Projection proj = Projection.createFromProj4String(srs.ExportToWkt());
			double lon0 = srs.GetProjParm(osrConstants.SRS_PP_CENTRAL_MERIDIAN, NO_DATA_VALUE);
			double lat0 = srs.GetProjParm(osrConstants.SRS_PP_STANDARD_PARALLEL_1, NO_DATA_VALUE);

			if (lon0 == NO_DATA_VALUE || lat0 == NO_DATA_VALUE) {
				// General case, generate a raster of the same size as the source
				gdalWarp.addTargetSize(inputDataset.getWidth(), inputDataset.getHeight());
			} else {
				// Mercator case
				// estimate target resolution projecting the first cell next to the reference lon/lat
				double[] xy = proj.projectQuietly(lon0, lat0);
				double[] lonlat1 = proj.unprojectQuietly(xy[0], xy[1]);
				double[] lonlat2 = proj.unprojectQuietly(xy[0] + inputDataset.getResolution().xRes(),
						xy[1] + inputDataset.getResolution().yRes());
				gdalWarp.addTargetResolution(Math.abs(lonlat2[0] - lonlat1[0]), Math.abs(lonlat2[1] - lonlat1[1]));
				// force alignment of cells with gdalwarp "-tap"
				gdalWarp.addTargetAlignedPixels(true);
			}
		}
		var result = GdalDataset.of(gdalWarp.run());
		// FIXME : In memory wraping doesn't work with .dtm.nc
		// result.deleteOnClose(false);
		result.deleteOnClose(true);
		openedDataset.add(result);
		return result;
	}

	/**
	 * Use gdal to produce the Aspect and Slope Tiff files
	 */
	private void generatesAspectAndSlopeFile(GdalDataset lonlatDataset, GdalDataset elevationlonlatDataset,
			IProgressMonitor monitor) throws IOException {
		var subMonitor = SubMonitor.convert(monitor, 100);

		// Getting dataset of input values (elevations...)
		Dataset inputRasterDataset = lonlatDataset.get();
		Dataset inputElevationDataset = elevationlonlatDataset.get();

		// Computes aspect ie, the azimuth that slopes are facing
		// 0° means East, 90° North, 180° West, 270° South.
		var cosAspectFile = TemporaryCache.createTemporaryFile("Cosinus_Aspect", ".tif");
		GdalAspect gdalAspect = new GdalAspect(inputElevationDataset, cosAspectFile);
		gdalAspect.setComputeEdges();
		gdalAspect.setZeroForFlat();
		gdalAspect.setTrigonometric();
		GdalDataset cosAspectDataset = gdalAspect.run(subMonitor.split(30));
		cosAspectDataset.deleteOnClose(true);
		openedDataset.add(cosAspectDataset);

		// Duplicates the aspectFile before computing cosinus (for computing sinus later)
		var sinAspectFile = TemporaryCache.createTemporaryFile("Sinus_Aspect", ".tif");
		GdalDataset sinAspectDataset = cosAspectDataset.copy(sinAspectFile.getPath());
		sinAspectDataset.deleteOnClose(true);
		openedDataset.add(sinAspectDataset);

		// Computing cosinus of aspects
		logger.debug("Computing cosinus of aspect angles");
		cosAspectDataset.resample(1, (buffer, noDataValue) -> computeAspect(buffer, noDataValue, Math::cos));

		// Computing sinus of aspects
		logger.debug("Computing sinus of aspect angles");
		sinAspectDataset.resample(1, (buffer, noDataValue) -> computeAspect(buffer, noDataValue, Math::sin));

		// Computes slope, in °. 0° means flat
		var slopeFile = TemporaryCache.createTemporaryFile("Slope", ".tif");
		GdalSlope gdalSlope = new GdalSlope(inputElevationDataset, slopeFile);
		gdalSlope.setComputeEdges();
		Sector sector = (Sector) getStoreParameters().getValue(AVKey.SECTOR);
		gdalSlope.setScale(LatLon.ellipsoidalDistance(sector.getCentroid(),
				LatLon.fromDegrees(sector.getCentroid().latitude.degrees, sector.getCentroid().longitude.degrees + 1d),
				Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS));
		GdalDataset slopeDataset = gdalSlope.run(subMonitor.split(30));
		slopeDataset.deleteOnClose(true);
		openedDataset.add(slopeDataset);

		// Merge Values/Slope/Aspect(cos)/Aspect(sin) into one virtual dataset
		// As if is was a single tif with 4 bands
		GdalVirtualDataset gdalVirtualDataset = new GdalVirtualDataset();
		gdalVirtualDataset.setSeparate();
		gdalVirtualDataset.setSrcNoData(NO_DATA_VALUE, NO_DATA_VALUE, NO_DATA_VALUE, NO_DATA_VALUE);
		gdalVirtualDataset.addDataset(inputRasterDataset);
		slopeDataset.ifPresent(gdalVirtualDataset::addDataset);
		cosAspectDataset.ifPresent(gdalVirtualDataset::addDataset);
		sinAspectDataset.ifPresent(gdalVirtualDataset::addDataset);
		tilableDataset = gdalVirtualDataset.run();
		tilableDataset.deleteOnClose(true);

		monitor.done();
	}

	/** Browse the buffer of aspect and compute a cos or sin for each value */
	private void computeAspect(ByteBuffer buffer, double noDataValue, DoubleUnaryOperator cosOrSin) {
		FloatBuffer floatBuffer = buffer.asFloatBuffer();
		for (int i = 0; i < floatBuffer.capacity(); i++) {
			double aspect = floatBuffer.get(i);
			if (aspect != noDataValue) {
				floatBuffer.put(i, (float) cosOrSin.applyAsDouble(Math.toRadians(aspect)));
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public GdalDataset openInputFile() {
		return tilableDataset;
	}

	/** {@inheritDoc} */
	@Override
	public synchronized void oneTileProduced() {
		updateProgress();
	}

	/** {@inheritDoc} */
	@Override
	public AVList getProductionParams() {
		return productionParams;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isCancelled() {
		return isStopped();
	}

}
