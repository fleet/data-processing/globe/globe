//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.01.05 à 10:40:05 AM CET 
//


package fr.ifremer.viewer3d.model.echoes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Software"/>
 *         &lt;element ref="{}DataTag"/>
 *         &lt;element ref="{}Name" minOccurs="0"/>
 *         &lt;element ref="{}nomDirExport"/>
 *         &lt;element ref="{}FormatVersion"/>
 *         &lt;element ref="{}ExtractedFrom"/>
 *         &lt;element ref="{}Survey"/>
 *         &lt;element ref="{}Vessel"/>
 *         &lt;element ref="{}Sounder"/>
 *         &lt;element ref="{}ChiefScientist"/>
 *         &lt;element ref="{}BaseForm" minOccurs="0"/>
 *         &lt;element ref="{}Dimensions"/>
 *         &lt;element ref="{}SignalFilters" minOccurs="0"/>
 *         &lt;element ref="{}Signals"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "software",
    "dataTag",
    "name",
    "nomDirExport",
    "formatVersion",
    "extractedFrom",
    "survey",
    "vessel",
    "sounder",
    "chiefScientist",
    "baseForm",
    "dimensions",
    "signalFilters",
    "signals"
})
@XmlRootElement(name = "Echoes")
public class Echoes {

    @XmlElement(name = "Software", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String software;
    @XmlElement(name = "DataTag", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String dataTag;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(required = true)
    protected String nomDirExport;
    @XmlElement(name = "FormatVersion")
    protected int formatVersion;
    @XmlElement(name = "ExtractedFrom", required = true)
    protected String extractedFrom;
    @XmlElement(name = "Survey", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String survey;
    @XmlElement(name = "Vessel", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String vessel;
    @XmlElement(name = "Sounder", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String sounder;
    @XmlElement(name = "ChiefScientist", required = true)
    protected String chiefScientist;
    @XmlElement(name = "BaseForm")
    protected String baseForm;
    @XmlElement(name = "Dimensions", required = true)
    protected Dimensions dimensions;
    @XmlElement(name = "SignalFilters")
    protected SignalFilters signalFilters;
    @XmlElement(name = "Signals", required = true)
    protected Signals signals;

    /**
     * Obtient la valeur de la propriété software.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftware() {
        return software;
    }

    /**
     * Définit la valeur de la propriété software.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftware(String value) {
        this.software = value;
    }

    /**
     * Obtient la valeur de la propriété dataTag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTag() {
        return dataTag;
    }

    /**
     * Définit la valeur de la propriété dataTag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTag(String value) {
        this.dataTag = value;
    }

    /**
     * Obtient la valeur de la propriété name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Définit la valeur de la propriété name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtient la valeur de la propriété nomDirExport.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomDirExport() {
        return nomDirExport;
    }

    /**
     * Définit la valeur de la propriété nomDirExport.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomDirExport(String value) {
        this.nomDirExport = value;
    }

    /**
     * Obtient la valeur de la propriété formatVersion.
     * 
     */
    public int getFormatVersion() {
        return formatVersion;
    }

    /**
     * Définit la valeur de la propriété formatVersion.
     * 
     */
    public void setFormatVersion(int value) {
        this.formatVersion = value;
    }

    /**
     * Obtient la valeur de la propriété extractedFrom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtractedFrom() {
        return extractedFrom;
    }

    /**
     * Définit la valeur de la propriété extractedFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtractedFrom(String value) {
        this.extractedFrom = value;
    }

    /**
     * Obtient la valeur de la propriété survey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurvey() {
        return survey;
    }

    /**
     * Définit la valeur de la propriété survey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurvey(String value) {
        this.survey = value;
    }

    /**
     * Obtient la valeur de la propriété vessel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVessel() {
        return vessel;
    }

    /**
     * Définit la valeur de la propriété vessel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVessel(String value) {
        this.vessel = value;
    }

    /**
     * Obtient la valeur de la propriété sounder.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSounder() {
        return sounder;
    }

    /**
     * Définit la valeur de la propriété sounder.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSounder(String value) {
        this.sounder = value;
    }

    /**
     * Obtient la valeur de la propriété chiefScientist.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiefScientist() {
        return chiefScientist;
    }

    /**
     * Définit la valeur de la propriété chiefScientist.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiefScientist(String value) {
        this.chiefScientist = value;
    }

    /**
     * Obtient la valeur de la propriété baseForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseForm() {
        return baseForm;
    }

    /**
     * Définit la valeur de la propriété baseForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseForm(String value) {
        this.baseForm = value;
    }

    /**
     * Obtient la valeur de la propriété dimensions.
     * 
     * @return
     *     possible object is
     *     {@link Dimensions }
     *     
     */
    public Dimensions getDimensions() {
        return dimensions;
    }

    /**
     * Définit la valeur de la propriété dimensions.
     * 
     * @param value
     *     allowed object is
     *     {@link Dimensions }
     *     
     */
    public void setDimensions(Dimensions value) {
        this.dimensions = value;
    }

    /**
     * Obtient la valeur de la propriété signalFilters.
     * 
     * @return
     *     possible object is
     *     {@link SignalFilters }
     *     
     */
    public SignalFilters getSignalFilters() {
        return signalFilters;
    }

    /**
     * Définit la valeur de la propriété signalFilters.
     * 
     * @param value
     *     allowed object is
     *     {@link SignalFilters }
     *     
     */
    public void setSignalFilters(SignalFilters value) {
        this.signalFilters = value;
    }

    /**
     * Obtient la valeur de la propriété signals.
     * 
     * @return
     *     possible object is
     *     {@link Signals }
     *     
     */
    public Signals getSignals() {
        return signals;
    }

    /**
     * Définit la valeur de la propriété signals.
     * 
     * @param value
     *     allowed object is
     *     {@link Signals }
     *     
     */
    public void setSignals(Signals value) {
        this.signals = value;
    }

}
