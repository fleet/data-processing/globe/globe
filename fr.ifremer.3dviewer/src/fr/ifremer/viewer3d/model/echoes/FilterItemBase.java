/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.model.echoes;

import java.beans.PropertyChangeSupport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * REQ-IHM-004.
 */
@XmlAccessorType(XmlAccessType.NONE)
public class FilterItemBase {

	private PropertyChangeSupport propertyChangeSupport;
	private float minValue = Float.NaN;
	private float maxValue = Float.NaN;
	private int precision;

	/**
	 * Accept a new value and compute the min and max values.
	 */
	public void setMinMaxValue(float value) {
		minValue = NumberUtils.Min(minValue, value);
		maxValue = NumberUtils.Max(maxValue, value);
	}

	/**
	 * @return the {@link #propertyChangeSupport}
	 */
	public PropertyChangeSupport getPropertyChangeSupport() {
		return propertyChangeSupport;
	}

	/**
	 * @param _pcs
	 *            the {@link #propertyChangeSupport} to set
	 */
	public void setPropertyChangeSupport(PropertyChangeSupport _pcs) {
		this.propertyChangeSupport = _pcs;
	}

	/**
	 * @return the {@link #minValue}
	 */
	public float getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue
	 *            the {@link #minValue} to set
	 */
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the {@link #maxValue}
	 */
	public float getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue
	 *            the {@link #maxValue} to set
	 */
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @return the {@link #precision}
	 */
	public int getPrecision() {
		return precision;
	}

	/**
	 * @param precision
	 *            the {@link #precision} to set
	 */
	public void setPrecision(int precision) {
		this.precision = precision;
	}

}
