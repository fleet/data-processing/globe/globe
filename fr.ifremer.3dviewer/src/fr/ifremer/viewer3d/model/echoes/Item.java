//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.01.05 à 10:40:05 AM CET 
//


package fr.ifremer.viewer3d.model.echoes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Name"/>
 *         &lt;element ref="{}Dimensions"/>
 *         &lt;element ref="{}Storage"/>
 *         &lt;element ref="{}Endianness" minOccurs="0"/>
 *         &lt;element ref="{}Unit"/>
 *         &lt;element ref="{}FileName"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "name",
    "dimensions",
    "storage",
    "endianness",
    "unit",
    "fileName"
})
public class Item {

    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "Dimensions", required = true)
    protected Dimensions dimensions;
    @XmlElement(name = "Storage", required = true)
    @XmlSchemaType(name = "string")
    protected StorageType storage;
    @XmlElement(name = "Endianness")
    @XmlSchemaType(name = "string")
    protected EndianType endianness;
    @XmlElement(name = "Unit", required = true, nillable = true)
    protected String unit;
    @XmlElement(name = "FileName", required = true)
    protected String fileName;

    /**
     * Obtient la valeur de la propriété name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Définit la valeur de la propriété name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtient la valeur de la propriété dimensions.
     * 
     * @return
     *     possible object is
     *     {@link Dimensions }
     *     
     */
    public Dimensions getDimensions() {
        return dimensions;
    }

    /**
     * Définit la valeur de la propriété dimensions.
     * 
     * @param value
     *     allowed object is
     *     {@link Dimensions }
     *     
     */
    public void setDimensions(Dimensions value) {
        this.dimensions = value;
    }

    /**
     * Obtient la valeur de la propriété storage.
     * 
     * @return
     *     possible object is
     *     {@link StorageType }
     *     
     */
    public StorageType getStorage() {
        return storage;
    }

    /**
     * Définit la valeur de la propriété storage.
     * 
     * @param value
     *     allowed object is
     *     {@link StorageType }
     *     
     */
    public void setStorage(StorageType value) {
        this.storage = value;
    }

    /**
     * Obtient la valeur de la propriété endianness.
     * 
     * @return
     *     possible object is
     *     {@link EndianType }
     *     
     */
    public EndianType getEndianness() {
        return endianness;
    }

    /**
     * Définit la valeur de la propriété endianness.
     * 
     * @param value
     *     allowed object is
     *     {@link EndianType }
     *     
     */
    public void setEndianness(EndianType value) {
        this.endianness = value;
    }

    /**
     * Obtient la valeur de la propriété unit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Définit la valeur de la propriété unit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Obtient la valeur de la propriété fileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Définit la valeur de la propriété fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

}
