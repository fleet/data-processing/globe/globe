//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.01.05 à 10:40:05 AM CET 
//


package fr.ifremer.viewer3d.model.echoes;

import java.awt.Color;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1
    extends XmlAdapter<String, Color>
{


    public Color unmarshal(String value) {
        return (fr.ifremer.viewer3d.util.colors.ColorConverter.parseStringToColor(value));
    }

    public String marshal(Color value) {
        return (fr.ifremer.viewer3d.util.colors.ColorConverter.printColorToString(value));
    }

}
