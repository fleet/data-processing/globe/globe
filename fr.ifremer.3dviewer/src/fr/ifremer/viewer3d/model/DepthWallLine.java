package fr.ifremer.viewer3d.model;

import cern.colt.list.LongArrayList;
import gov.nasa.worldwind.geom.Position;

/**
 * Create a {@see PathWallLines} with a navigation line and depth values.
 * 
 */
public class DepthWallLine extends PathWallLines {

	private LongArrayList times=new LongArrayList();

	public DepthWallLine() {
		super();
	}


	public void addPoint(Position top, double bottomDepth, long datetime) {
		addPoint(top,new Position(top.latitude,top.longitude,bottomDepth));
		times.add(datetime);
	}

}
