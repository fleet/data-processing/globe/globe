package fr.ifremer.viewer3d.model;

import gov.nasa.worldwind.geom.Position;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * this interface allow a Layer to be played, implementing method.
 * 
 * @author mantoine
 * 
 */
public interface IPlayable {

	public static final String ISPLAYABLEWITHTIME = "isPlaybleWithTime";
	public static final String ISPLAYABLEWITHPING = "isPlayableWithPing";
	public static final String ISPLAYABLEWITHANGLE = "isPlaybleWithAngle";

	/**
	 * return the name that will be show in the playList panel (most of the time
	 * the layer name).
	 * 
	 * @return a name
	 */
	public String getName();

	/**
	 * return the first time or the start time registered in the layer file
	 * 
	 * @return a time
	 */
	public Long getStartTime();

	/**
	 * return the last time or the end time registered in the layer file
	 * 
	 * @return a time
	 */
	public Long getEndTime();

	public String getPlayableType();

	public void propertyChange(PropertyChangeEvent evt);

	public void addPropertyChangeListener(PropertyChangeListener listener);

	public void removePropertyChangeListener(PropertyChangeListener listener);

	public void setPosition(Position startPos);

	public Position getPosition();

	public Position getPosition(int index);

	void refresh(int newIndex);
}
