/*
 * @License@
 */
package fr.ifremer.viewer3d.model;

// Start of user code for imports

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

// End of user code
import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.layers.plume.PlumeLayerParameters;
import fr.ifremer.viewer3d.layers.plume.filter.GenericFilter;
import fr.ifremer.viewer3d.layers.plume.filter.SignalFiltersList;
import fr.ifremer.viewer3d.layers.plume.filter.GenericFilter.filteringStatus;
import fr.ifremer.viewer3d.model.echoes.FilterItem;
import fr.ifremer.viewer3d.model.echoes.SignalFilters;

/**
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 * @generated
 */
public class PlumeBean {

	/**
	 * Property name for energyFilter change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_ENERGYFILTER = "energyFilter";

	/**
	 * Property name for minTotalEnergyValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MINTOTALENERGYVALUE = "minTotalEnergyValue";

	/**
	 * Property name for minDepthValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MINDEPTHVALUE = "minDepthValue";

	/**
	 * Property name for file change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_FILE = "file";

	/**
	 * Property name for totalEnergyFilter change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_TOTALENERGYFILTER = "totalEnergyFilter";

	/**
	 * Property name for minEnergyValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MINENERGYVALUE = "minEnergyValue";

	/**
	 * Property name for echoes change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_ECHOES = "echoes";

	/**
	 * Property name for maxTotalEnergyValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MAXTOTALENERGYVALUE = "maxTotalEnergyValue";

	/**
	 * Property name for minPingValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MINPINGVALUE = "minPingValue";

	/**
	 * Property name for maxDepthValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MAXDEPTHVALUE = "maxDepthValue";

	/**
	 * Property name for maxEnergyValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MAXENERGYVALUE = "maxEnergyValue";

	/**
	 * Property name for bicolorEnd change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_BICOLOREND = "bicolorEnd";

	/**
	 * Property name for maxPingValue change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_MAXPINGVALUE = "maxPingValue";

	/**
	 * Property name for depthFilter change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_DEPTHFILTER = "depthFilter";

	/**
	 * Property name for hueScale change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_HUESCALE = "hueScale";

	/**
	 * Property name for pingsFilter change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_PINGSFILTER = "pingsFilter";

	/**
	 * Property name for binaryFiles change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_BINARYFILES = "binaryFiles";

	/**
	 * Property name for grayscale change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_GRAYSCALE = "grayscale";

	/**
	 * Property name for bicolorScale change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_BICOLORSCALE = "bicolorScale";

	/**
	 * Property name for bicolorStart change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_BICOLORSTART = "bicolorStart";

	private PlumeLayerParameters parameters;

	/**
	 * Property name for echo size change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_ECHOSIZE = "echoSize";

	/**
	 * Utility instance for property change support.
	 * 
	 * @generated
	 */

	// REQ-IHM-004 - Markers Filtering
	public static final String PROPERTY_SIGNALFILTER = "signalFilter";

	public static final String PROPERTY_ALLPARAMETER = "AllParametersChanged";

	protected SignalFilters signalFilters; // Signal filters description read
	// from XML file
	protected List<Echo> filteredEchoes;
	protected List<Echo> lowerFilteredEchoes;
	protected List<Echo> upperFilteredEchoes;

	protected transient PropertyChangeSupport _pcs = new PropertyChangeSupport(this);

	/**
	 * Adds a change listener to this object.
	 * 
	 * @param listener
	 *            the listener to add
	 * @generated
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this._pcs.addPropertyChangeListener(listener);

		// REQ-IHM-004 - Markers Filtering - set property change for all filters
		if (signalFilters != null) {
			List<FilterItem> filters = signalFilters.getItem();

			for (FilterItem filter : filters) {
				filter.setPropertyChangeSupport(_pcs);
			}
		}
	}

	/**
	 * Removes a change listener from this object.
	 * 
	 * @param listener
	 *            the listener to remove
	 * @generated
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this._pcs.removePropertyChangeListener(listener);

		// REQ-IHM-004 - Markers Filtering - Remove property change for all
		// filters
		if (signalFilters != null) {
			List<FilterItem> filters = signalFilters.getItem();

			for (FilterItem filter : filters) {
				filter.setPropertyChangeSupport(null);
			}
		}
	}

	/**
	 * Default constructor.
	 * 
	 * @generated
	 */
	public PlumeBean() {
		super();
		this._echoes = new ArrayList<Echo>();
		this._binaryFiles = new ArrayList<File>();
		// Start of user code PlumeBean default ctor

		// REQ-IHM-004 - Markers Filtering
		filteredEchoes = new ArrayList<Echo>();
		lowerFilteredEchoes = new ArrayList<Echo>();
		upperFilteredEchoes = new ArrayList<Echo>();

		parameters = new PlumeLayerParameters();
		// End of user code
	}

	/**
	 * For Serializable and Externalizable classes, the readResolve method
	 * allows a class to replace/resolve the object read from the stream before
	 * it is returned to the caller. By implementing the readResolve method, a
	 * class can directly control the types and instances of its own instances
	 * being deserialized.
	 * 
	 * @generated
	 */
	private Object readResolve() {
		this._pcs = new PropertyChangeSupport(this);
		// Start of user code PlumeBean readResolve

		// End of user code
		return this;
	}

	/**
	 * The file attribute.
	 * 
	 * @generated
	 */
	private File _file = null;

	/**
	 * The echoes attribute.
	 * 
	 * @generated
	 */
	private transient List<Echo> _echoes;

	/**
	 * The binaryFiles attribute.
	 * 
	 * @generated
	 */
	private List<File> _binaryFiles;

	/**
	 * The file attribute.
	 * 
	 * @generated
	 */
	public File getFile() {
		return this._file;
	}

	/**
	 * * The file attribute.
	 * 
	 * @generated
	 */
	public void setFile(File file) {
		if (this._file != file) {
			this._pcs.firePropertyChange(PROPERTY_FILE, this._file, this._file = file);
		}
	}

	/**
	 * The echoes attribute.
	 * 
	 * @generated
	 */
	public List<Echo> getEchoes() {
		return this._echoes;
	}

	/**
	 * * The echoes attribute.
	 * 
	 * @see #addEchoes
	 * @see #removeEchoes
	 * @generated
	 */
	protected void setEchoes(List<Echo> echoes) {
		if (this._echoes != echoes) {
			// Start of user code custom setter for echoes

			// End of user code
			this._pcs.firePropertyChange(PROPERTY_ECHOES, this._echoes, this._echoes = echoes);
		}
	}

	/**
	 * Add a echoes to the echoes collection.
	 * 
	 * @param echoesElt
	 *            Element to add
	 * @generated
	 */
	public void addEchoe(Echo echoesElt) {
		this._echoes.add(echoesElt);
		// Start of user code custom add for echoes
		if (echoesElt != null) {
			if (signalFilters != null) {
				// REQ-IHM-004 - Markers Filtering: initialise min/max values
				List<FilterItem> filters = signalFilters.getItem();
				for (FilterItem filter : filters) {
					filter.setMinMaxValue(echoesElt.getParameter(filter.getSignal()));
					filter.setPrecision(echoesElt.getPrecision(filter.getSignal()));
				}
			}
		}
		// End of user code
		this._pcs.firePropertyChange(PROPERTY_ECHOES, null, echoesElt);
	}

	/**
	 * Remove a echoes from the echoes collection.
	 * 
	 * @param echoesElt
	 *            Element to remove
	 * @generated
	 */
	public void removeEchoes(Echo echoesElt) {
		this._echoes.remove(echoesElt);
		// Start of user code custom remove for echoes

		// End of user code
		this._pcs.firePropertyChange(PROPERTY_ECHOES, echoesElt, null);
	}

	/**
	 * End color for bicolor scale (should only be used if bicolorScale is
	 * true).
	 * 
	 * @generated
	 */
	public Color getBicolorEnd() {
		return this.parameters.getBicolorEnd();
	}

	/**
	 * End color for bicolor scale (should only be used if bicolorScale is
	 * true).
	 * 
	 * @generated
	 */
	public void setBicolorEnd(Color bicolorEnd) {
		if (this.parameters.getBicolorEnd() != bicolorEnd) {
			Color old = this.parameters.getBicolorEnd();
			parameters.setBicolorEnd(bicolorEnd);
			this._pcs.firePropertyChange(PROPERTY_BICOLOREND, old, bicolorEnd);
		}
	}

	/**
	 * Value for hue scale color rendering.
	 * 
	 * @generated
	 */
	public boolean getHueScale() {
		return this.parameters.isHueScale();
	}

	/**
	 * Value for hue scale color rendering.
	 * 
	 * @generated
	 */
	public void setHueScale(boolean hueScale) {
		if (this.parameters.isHueScale() != hueScale) {
			parameters.setHueScale(hueScale);
			this._pcs.firePropertyChange(PROPERTY_HUESCALE, !hueScale, hueScale);
		}
	}

	/**
	 * The binaryFiles attribute.
	 * 
	 * @generated
	 */
	public List<File> getBinaryFiles() {
		return this._binaryFiles;
	}

	/**
	 * * The binaryFiles attribute.
	 * 
	 * @see #addBinaryFiles
	 * @see #removeBinaryFiles
	 * @generated
	 */
	protected void setBinaryFiles(List<File> binaryFiles) {
		if (this._binaryFiles != binaryFiles) {
			this._pcs.firePropertyChange(PROPERTY_BINARYFILES, this._binaryFiles, this._binaryFiles = binaryFiles);
		}
	}

	/**
	 * Add a binaryFiles to the binaryFiles collection.
	 * 
	 * @param binaryFilesElt
	 *            Element to add
	 * @generated
	 */
	public void addBinaryFile(File binaryFilesElt) {
		this._binaryFiles.add(binaryFilesElt);
		// _pcs.firePropertyChange(PROPERTY_BINARYFILES, null, binaryFilesElt);
	}

	/**
	 * Remove a binaryFiles from the binaryFiles collection.
	 * 
	 * @param binaryFilesElt
	 *            Element to remove
	 * @generated
	 */
	public void removeBinaryFiles(File binaryFilesElt) {
		this._binaryFiles.remove(binaryFilesElt);
		this._pcs.firePropertyChange(PROPERTY_BINARYFILES, binaryFilesElt, null);
	}

	/**
	 * Value for gray scale color rendering.
	 * 
	 * @generated
	 */
	public boolean getGrayscale() {
		return this.parameters.isGrayscale();
	}

	/**
	 * Value for gray scale color rendering.
	 * 
	 * @generated
	 */
	public void setGrayscale(boolean grayscale) {
		if (this.getGrayscale() != grayscale) {
			parameters.setGrayscale(grayscale);
			this._pcs.firePropertyChange(PROPERTY_GRAYSCALE, !grayscale, grayscale);
		}
	}

	/**
	 * Value for bicolor scale rendering.
	 * 
	 * @generated
	 */
	public boolean getBicolorScale() {
		return this.parameters.isBicolorScale();
	}

	/**
	 * Value for bicolor scale rendering.
	 * 
	 * @generated
	 */
	public void setBicolorScale(boolean bicolorScale) {
		if (this.parameters.isBicolorScale() != bicolorScale) {
			parameters.setBicolorScale(bicolorScale);
			this._pcs.firePropertyChange(PROPERTY_BICOLORSCALE, !bicolorScale, bicolorScale);
		}
	}

	/**
	 * Start color for bicolor scale (should only be used if bicolorScale is
	 * true).
	 * 
	 * @generated
	 */
	public Color getBicolorStart() {
		return this.parameters.getBicolorStart();
	}

	/**
	 * Start color for bicolor scale (should only be used if bicolorScale is
	 * true).
	 * 
	 * @generated
	 */
	public void setBicolorStart(Color bicolorStart) {
		if (this.parameters.getBicolorStart() != bicolorStart) {
			Color old = bicolorStart;
			parameters.setBicolorStart(bicolorStart);
			this._pcs.firePropertyChange(PROPERTY_BICOLORSTART, old, bicolorStart);
		}
	}

	// /**
	// *
	// * @return
	// * @generated
	// */
	// public List<Echo> getFilteredEchoes() {
	// // Start of user code for operation getFilteredEchoes
	//
	// List<Echo> result = new ArrayList<Echo>(this._echoes.size());
	//
	// for (Echo echo : this._echoes) {
	//
	// // REQ-IHM-004 - Markers Filtering: echo tested by all filters
	// if (!signalFilterObjects.valueValidity(echo)) {
	// echo = null;
	// }
	//
	// if (echo != null) {
	// result.add(echo);
	// }
	// }
	//
	// return result;
	//
	// // End of user code
	// }

	// REQ-IHM-004 - Markers Filtering: echo tested by all filters
	public List<Echo> getFilteredEchoes2() {
		filteredEchoes.clear();
		upperFilteredEchoes.clear();
		lowerFilteredEchoes.clear();

		((ArrayList<Echo>) filteredEchoes).ensureCapacity(this._echoes.size());
		((ArrayList<Echo>) upperFilteredEchoes).ensureCapacity(this._echoes.size());
		((ArrayList<Echo>) lowerFilteredEchoes).ensureCapacity(this._echoes.size());

		SignalFiltersList signalFilterObjects = parameters.getSignalFilterObjects();

		for (Echo echo : this._echoes) {
			signalFilterObjects.valueValidity(echo);

			if (signalFilterObjects.getFilteringSatus() == filteringStatus.NO_FILTERED) {
				filteredEchoes.add(echo);
			} else if (signalFilterObjects.getFilteringSatus() == filteringStatus.LOWER_MIN) {
				lowerFilteredEchoes.add(echo);
			} else if (signalFilterObjects.getFilteringSatus() == filteringStatus.UPPER_MAX) {
				upperFilteredEchoes.add(echo);
			}
		}

		return filteredEchoes;
	}

	// REQ-IHM-004 - Markers Filtering
	public List<Echo> getLowerFilteredEchoes() {
		return lowerFilteredEchoes;
	}

	// REQ-IHM-004 - Markers Filtering
	public List<Echo> getUpperFilteredEchoes() {
		return upperFilteredEchoes;
	}

	// REQ-IHM-004 - Markers Filtering

	// REQ-IHM-004 - Markers Filtering
	public void setSignalfilters(SignalFilters filters) {
		this.signalFilters = filters;
		this.parameters.fillFilterList(filters);
	}

	// // REQ-IHM-004 - Markers Filtering
	// public SignalFilters getSignalFilters() {
	// return signalFilters;
	// }

	// // REQ-IHM-004 - Markers Filtering
	// public SignalFiltersList getSignalFilterObjects() {
	// return signalFilterObjects;
	// }

	// REQ-IHM-004 - Markers Filtering
	public boolean isFilterActive(String signal) {

		GenericFilter filter = parameters.getFilter(signal);
		if (filter != null) {
			return true;
		}
		return false;
	}

	// REQ-IHM-004 - Markers Filtering
	public GenericFilter getFilter(String signal) {
		return parameters.getFilter(signal);

	}

	public double getEchoSize() {
		return parameters.getEchoSize();
	}

	/***
	 * displaying size of the echo
	 * 
	 * @param echoSize
	 */
	public void setEchoSize(double echoSize) {
		if (this.parameters.getEchoSize() != echoSize) {
			double old = this.parameters.getEchoSize();
			parameters.setEchoSize(echoSize);
			this._pcs.firePropertyChange(PROPERTY_ECHOSIZE, old, echoSize);
		}
	}

	public PlumeLayerParameters getLayerParameters() {
		return parameters;
	}

	public void setLayerParameters(LayerParameters arg) {
		if (arg instanceof PlumeLayerParameters) {
			if(signalFilters != null){
				this.parameters = (PlumeLayerParameters) arg;
				parameters.updateSignalFilterParameters(getSignalFilters());
				this._pcs.firePropertyChange(PlumeBean.PROPERTY_ALLPARAMETER, null, parameters);
			}

		}
	}

	/**
	 * @return the signalFilters
	 */
	public SignalFilters getSignalFilters() {
		return signalFilters;
	}

}
