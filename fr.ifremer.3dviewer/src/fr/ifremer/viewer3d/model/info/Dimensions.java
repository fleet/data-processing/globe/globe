package fr.ifremer.viewer3d.model.info;

public class Dimensions {

	private int nbPings;
	private int nbSlices;
	private int nbRows;

	private int nx;
	private int ny;
	private int nz;

	public int getNbPings() {
		return nbPings;
	}

	public int getNbSlices() {
		return nbSlices;
	}

	public int getNbRows() {
		return nbRows;
	}

	public int getNx() {
		return nx;
	}

	public int getNy() {
		return ny;
	}

	public int getNz() {
		return nz;
	}
}
