/**
 * 
 */
package fr.ifremer.viewer3d.model.info;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

/**
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 * 
 */
public class Signature2Converter extends AbstractSingleValueConverter {

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class clazz) {
		return Signature2.class.equals(clazz);
	}

	@Override
	public Object fromString(String value) {
		for (Signature2 s : Signature2.values()) {
			if (value.equals(s.toString())) {
				return s;
			}
		}
		return null;
	}
}
