package fr.ifremer.viewer3d.model.info;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Converter utilisé pour les enums ayant un label associé.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class LabelEnumConverter implements Converter {

	private Logger logger = LoggerFactory.getLogger(LabelEnumConverter.class);

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return type.isEnum() && LabelEnum.class.isAssignableFrom(type);
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		writer.setValue(((LabelEnum) source).label());
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Class<?> type = context.getRequiredType();
		try {
			Method m = type.getMethod("fromLabel", String.class);
			return m.invoke(null, reader.getValue());
		} catch (SecurityException e) {
			logger.warn("Error loading enum from XML input", e);
		} catch (NoSuchMethodException e) {
			logger.warn("Error loading enum from XML input", e);
		} catch (IllegalArgumentException e) {
			logger.warn("Error loading enum from XML input", e);
		} catch (IllegalAccessException e) {
			logger.warn("Error loading enum from XML input", e);
		} catch (InvocationTargetException e) {
			logger.warn("Error loading enum from XML input", e);
		}
		return null;
	}

}
