package fr.ifremer.viewer3d.model.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Format de données V2.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
@XStreamAlias("Info")
public class Info {
	@XStreamAlias("Signature1")
	private String signature1;

	@XStreamAlias("Signature2")
	private Signature2 signature2;

	@XStreamAlias("ImageColor")
	private String imageColor;

	@XStreamAlias("FormatVersion")
	private String formatVersion;

	@XStreamAlias("Name")
	private String name;

	@XStreamAlias("ExtractedFrom")
	private String extractedFrom;

	@XStreamAlias("Survey")
	private String survey;

	@XStreamAlias("Vessel")
	private String vessel;

	@XStreamAlias("Sounder")
	private String sounder;

	@XStreamAlias("ChiefScientist")
	private String chiefScientist;

	@XStreamAlias("ProcessedBy")
	private String processedBy;

	@XStreamAlias("Comments")
	private String comments;

	@XStreamAlias("DepthTop")
	private Double depthTop;

	@XStreamAlias("DepthBottom")
	private Double depthBottom;

	@XStreamAlias("MinValue")
	private Double minValue;

	@XStreamAlias("MaxValue")
	private Double maxValue;

	@XStreamAlias("MiddleSlice")
	private int middleSlice;

	@XStreamAlias("Dimensions")
	private Dimensions dimensions;

	@XStreamAlias("Signals")
	private Signals signals;

	@XStreamAlias("Images")
	private Images images;

	@XStreamAlias("Strings")
	private Strings strings;

	public String getSignature1() {
		return signature1;
	}

	public Signature2 getSignature2() {
		return signature2;
	}

	public String getFormatVersion() {
		return formatVersion;
	}

	public String getName() {
		return name;
	}

	public String getExtractedFrom() {
		return extractedFrom;
	}

	public String getSurvey() {
		return survey;
	}

	public String getVessel() {
		return vessel;
	}

	public String getSounder() {
		return sounder;
	}

	public String getChiefScientist() {
		return chiefScientist;
	}

	public Double getDepthTop() {
		return depthTop;
	}

	public Double getDepthBottom() {
		return depthBottom;
	}

	public Double getMinValue() {
		if(minValue==null) return 0.;
		return minValue;
	}

	public Double getMaxValue() {
		if(maxValue==null) return 255.;
		return maxValue;
	}

	public int getMiddleSlice() {
		return middleSlice;
	}

	public Dimensions getDimensions() {
		return dimensions;
	}

	public Signals getSignals() {
		return signals;
	}

	public Images getImages() {
		return images;
	}

	public Strings getStrings() {
		return strings;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public String getComments() {
		return comments;
	}

	/**
	 * @return the imageColor
	 */
	public String getImageColor() {
		return imageColor;
	}
}
