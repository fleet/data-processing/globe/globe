package fr.ifremer.viewer3d.model.info;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Signals")
public class Signals {

	@XStreamAlias("item")
	@XStreamImplicit
	private List<Item> items;

	@XStreamAlias("Name")
	private String name;

	@XStreamAlias("Dimensions")
	private String dimensions;

	@XStreamAlias("Storage")
	private StorageType storage;

	@XStreamAlias("Unit")
	private String unit;

	@XStreamAlias("FileName")
	private String fileName;

	@XStreamAlias("Tag")
	private String tag;

	public String getName() {
		return name;
	}

	public String getDimensions() {
		return dimensions;
	}

	public StorageType getStorage() {
		return storage;
	}

	public String getUnit() {
		return unit;
	}

	public String getFileName() {
		return fileName;
	}

	public String getTag() {
		return tag;
	}

	public List<Item> getItems() {
		if (items == null) {
			Item item = new Item();
			item.setName(this.getName());
			item.setDimensions(this.getDimensions());
			item.setStorage(this.getStorage());
			item.setUnit(this.getUnit());
			item.setFileName(this.getFileName());
			items = new ArrayList<Item>();
			items.add(item);
		}
		return items;
	}

}
