package fr.ifremer.viewer3d.model.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class Item {
	@XStreamAlias("Name")
	private String name;

	@XStreamAlias("Dimensions")
	private String dimensions;

	@XStreamAlias("Storage")
	private StorageType storage;

	@XStreamAlias("Unit")
	private String unit;

	@XStreamAlias("FileName")
	private String fileName;

	@XStreamAlias("Tag")
	private String tag;

	public String getName() {
		return name;
	}

	public String getDimensions() {
		return dimensions;
	}

	public StorageType getStorage() {
		return storage;
	}

	public String getUnit() {
		return unit;
	}

	public String getFileName() {
		return fileName;
	}

	public String getTag() {
		return tag;
	}

	// setters pour creation si List<Item> vide

	public void setName(String name) {
		this.name = name;
	}

	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	public void setStorage(StorageType storage) {
		this.storage = storage;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
