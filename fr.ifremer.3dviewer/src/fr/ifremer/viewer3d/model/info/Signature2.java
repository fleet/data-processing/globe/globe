/**
 * 
 */
package fr.ifremer.viewer3d.model.info;

/**
 * Enumération des différents types de données pour formats V2.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public enum Signature2 {
	Seismic3D, SismicAlongNav, SeismicAlongNav, ImagesAlongNavigation, WCAlongNavigation
}
