package fr.ifremer.viewer3d.model.info;

/**
 * Enumération des différents types de données binaire pour format V2.
 * 
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public enum StorageType implements LabelEnum {
	CHAR("char"), UINT_8("uint8"), UCHAR("uchar"), INT_16("int16"), UINT_16("uint16"), INT_32("int32"), UINT_32("uint32"), INT_64("int64"), UINT_64("uint64"), SINGLE("single"), DOUBLE("double");

	private final String label;

	StorageType(String v) {
		label = v;
	}

	@Override
	public String label() {
		return label;
	}

	public static StorageType fromLabel(String v) {
		for (StorageType c : StorageType.values()) {
			if (c.label.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
