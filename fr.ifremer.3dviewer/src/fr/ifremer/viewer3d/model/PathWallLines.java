package fr.ifremer.viewer3d.model;

import java.util.ArrayList;
import java.util.List;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;

/**
 * Add list of two lines, one matching the bottom line, the other the top line, used to create vertical slices
 *
 */
public class PathWallLines {
	/**
	 * The name attribute.
	 */
	private String name = "";

	/**
	 * bottom points list
	 */
	private List<Position> bottomPoints;

	/**
	 * top points list
	 */
	protected List<Position> topPoints;

	/**
	 * Constructor
	 */
	public PathWallLines() {
		topPoints = new ArrayList<>();
		bottomPoints = new ArrayList<>();
	}

	/**
	 * Constructor
	 */
	public PathWallLines(List<Position> topPoints, List<Position> bottomPoints) {
		this.topPoints = topPoints;
		this.bottomPoints = bottomPoints;
	}

	public int size() {
		return topPoints.size();
	}

	public Position getTop(int index) {
		return topPoints.get(index);
	}

	public Position getBottom(int index) {
		return bottomPoints.get(index);
	}

	public void addPoint(Position top, Position bottom) {
		topPoints.add(top);
		bottomPoints.add(bottom);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Sector computeBoundingSector() {
		return Sector.boundingSector(topPoints);
	}

}