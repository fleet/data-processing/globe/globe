package fr.ifremer.viewer3d.model;


public interface IPlayerSynchronizable {
	PlayerBean getPlayerBean();
	PlayerBean getMasterPlayerBean();
	boolean isPlayerSynchronized();
	String getWindowID();
	void setWindowID(String windowID);
}
