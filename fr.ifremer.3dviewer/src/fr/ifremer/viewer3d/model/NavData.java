/*
 * @License@
 */
package fr.ifremer.viewer3d.model;

// Start of user code for imports

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import fr.ifremer.globe.ui.ANavLine;
import gov.nasa.worldwind.geom.Position;

// End of user code

/**
 * Navigation data.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 * @generated
 */

public class NavData {

	/**
	 * Property name for file change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_FILE = "file";

	/**
	 * Property name for lines change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_LINES = "lines";

	/**
	 * Property name for binaryFiles change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_BINARYFILES = "binaryFiles";

	/**
	 * Property name for name change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_NAME = "name";

	/**
	 * Utility instance for property change support.
	 * 
	 * @generated
	 */
	protected transient PropertyChangeSupport _pcs = new PropertyChangeSupport(this);

	/**
	 * Adds a change listener to this object.
	 * 
	 * @param listener
	 *            the listener to add
	 * @generated
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		_pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Removes a change listener from this object.
	 * 
	 * @param listener
	 *            the listener to remove
	 * @generated
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		_pcs.removePropertyChangeListener(listener);
	}

	/**
	 * Default constructor.
	 * 
	 * @generated
	 */
	public NavData() {
		super();
		this._lines = new ArrayList<ANavLine<? extends Position>>();
		this._binaryFiles = new ArrayList<File>();
		// Start of user code NavData default ctor

		// End of user code
	}

	/**
	 * For Serializable and Externalizable classes, the readResolve method
	 * allows a class to replace/resolve the object read from the stream before
	 * it is returned to the caller. By implementing the readResolve method, a
	 * class can directly control the types and instances of its own instances
	 * being deserialized.
	 * 
	 * @generated
	 */
	private Object readResolve() {
		_pcs = new PropertyChangeSupport(this);
		// Start of user code NavData readResolve

		// End of user code
		return this;
	}

	/**
	 * The file attribute.
	 * 
	 * @generated
	 */
	private File _file = null;

	/**
	 * The lines attribute.
	 * 
	 * @generated
	 */
	private List<ANavLine<? extends Position>> _lines;

	/**
	 * The binaryFiles attribute.
	 * 
	 * @generated
	 */
	private List<File> _binaryFiles;

	/**
	 * The name attribute.
	 * 
	 * @generated
	 */
	private String _name = null;

	/**
	 * The software attribute.
	 * 
	 * @generated
	 */
	private String _software = null;

	/**
	 * The collada path.
	 * 
	 * @generated
	 */
	private String _collada = null;

	/**
	 * The file attribute.
	 * 
	 * @generated
	 */
	public File getFile() {
		return this._file;
	}

	/**
	 * * The file attribute.
	 * 
	 * @generated
	 */
	public void setFile(File file) {
		if (this._file != file) {
			_pcs.firePropertyChange(PROPERTY_FILE, this._file, this._file = file);
		}
	}

	/**
	 * The lines attribute.
	 * 
	 * @generated
	 */
	public List<ANavLine<? extends Position>> getLines() {
		return this._lines;
	}

	/**
	 * * The lines attribute.
	 * 
	 * @see #addLines
	 * @see #removeLines
	 * @generated
	 */
	protected void setLines(List<ANavLine<? extends Position>> lines) {
		if (this._lines != lines) {
			_pcs.firePropertyChange(PROPERTY_LINES, this._lines, this._lines = lines);
		}
	}

	/**
	 * Add a lines to the lines collection.
	 * 
	 * @param linesElt
	 *            Element to add
	 * @generated
	 */
	public void addLine(ANavLine<? extends Position> linesElt) {
		this._lines.add(linesElt);
		_pcs.firePropertyChange(PROPERTY_LINES, null, linesElt);
	}

	/**
	 * Remove a lines from the lines collection.
	 * 
	 * @param linesElt
	 *            Element to remove
	 * @generated
	 */
	public void removeLines(ANavLine<? extends Position> linesElt) {
		this._lines.remove(linesElt);
		_pcs.firePropertyChange(PROPERTY_LINES, linesElt, null);
	}

	/**
	 * The binaryFiles attribute.
	 * 
	 * @generated
	 */
	public List<File> getBinaryFiles() {
		return this._binaryFiles;
	}

	/**
	 * * The binaryFiles attribute.
	 * 
	 * @see #addBinaryFiles
	 * @see #removeBinaryFiles
	 * @generated
	 */
	protected void setBinaryFiles(List<File> binaryFiles) {
		if (this._binaryFiles != binaryFiles) {
			_pcs.firePropertyChange(PROPERTY_BINARYFILES, this._binaryFiles, this._binaryFiles = binaryFiles);
		}
	}

	/**
	 * Add a binaryFiles to the binaryFiles collection.
	 * 
	 * @param binaryFilesElt
	 *            Element to add
	 * @generated
	 */
	public void addBinaryFile(File binaryFilesElt) {
		this._binaryFiles.add(binaryFilesElt);
		_pcs.firePropertyChange(PROPERTY_BINARYFILES, null, binaryFilesElt);
	}

	/**
	 * Remove a binaryFiles from the binaryFiles collection.
	 * 
	 * @param binaryFilesElt
	 *            Element to remove
	 * @generated
	 */
	public void removeBinaryFiles(File binaryFilesElt) {
		this._binaryFiles.remove(binaryFilesElt);
		_pcs.firePropertyChange(PROPERTY_BINARYFILES, binaryFilesElt, null);
	}

	/**
	 * The name attribute.
	 * 
	 * @generated
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * * The name attribute.
	 * 
	 * @generated
	 */
	public void setName(String name) {
		if (this._name != name) {
			_pcs.firePropertyChange(PROPERTY_NAME, this._name, this._name = name);
		}
	}

	/**
	 * The software attribute.
	 */
	public String getSoftware() {
		return this._software;
	}

	/**
	 * * The software attribute.
	 */
	public void setSoftware(String software) {
		this._software = software;
	}

	/**
	 * * The collada path.
	 */
	public String getCollada() {
		return this._collada;

	}

	/**
	 * * The collada path.
	 */
	public void setCollada(String collada) {
		this._collada = collada;

	}

	public void sort() {
		this._lines.sort(new Comparator<ANavLine<? extends Position>>() {

			@Override
			public int compare(ANavLine<? extends Position> o1, ANavLine<? extends Position> o2) {

				return o1.getName().compareTo(o2.getName());
			}
		});

	}
}
