package fr.ifremer.viewer3d.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

/**
 * Watercolumn data for vertical images.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class Seismic3DData implements IInfos {

	/**
	 * Watercolumn data input file (xml).
	 */
	private final File inputFile;

	private List<Double> tList;
	private List<Double> zList;

	public int getNx() {
		return nx;
	}

	public void setNx(int nx) {
		this.nx = nx;
	}

	public int getNy() {
		return ny;
	}

	public void setNy(int ny) {
		this.ny = ny;
	}

	public int getNz() {
		return nz;
	}

	public void setNz(int nz) {
		this.nz = nz;
	}

	private int nx;
	private int ny;
	private int nz;

	/**
	 * lat/lon.
	 */
	private LatLon[][] latlon;

	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	private String imgDir;

	/**
	 * The infos displayed by the popupmenu action Infos
	 */
	private AbstractInfos infos;

	private List<Position> positions = new ArrayList<Position>();

	public List<Position> getPositions() {
		return this.positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	/**
	 * Ctor.
	 * 
	 * @param file
	 *            Watercolumn data input file (xml).
	 */
	public Seismic3DData(File file) {
		this.inputFile = file;
		this.zList = new ArrayList<Double>();
		this.tList = new ArrayList<Double>();
		this.latlon = null;
		this.imgDir = null;
		this.infos = new AbstractInfos();
	}

	/**
	 * Watercolumn data input file (xml).
	 */
	public File getFile() {
		return this.inputFile;
	}

	public List<Double> getZList() {
		return this.zList;
	}

	public Double getZ(int index) {
		return this.zList.get(index);
	}

	public void addZ(Double bottomElevation) {
		this.zList.add(bottomElevation);
	}

	public List<Double> getTList() {
		return this.tList;
	}

	public Double getT(int index) {
		return this.tList.get(index);
	}

	public void addT(Double t) {
		this.tList.add(t);
	}

	public LatLon[][] getLatlon() {
		return this.latlon;
	}

	public LatLon getLatlon(int x, int y) {
		return this.latlon[x][y];
	}

	public void setLatlon(LatLon[][] latlon) {
		this.latlon = latlon;
	}

	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	public String getImgDir() {
		return this.imgDir;
	}

	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	public void setImgDir(String directory) {
		this.imgDir = directory;
	}



	@Override
	public List<Property<?>> getProperties() {
		return this.infos.getProperties();
	}

}
