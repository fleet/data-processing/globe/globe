package fr.ifremer.viewer3d.model.watercolumn;

public class BathyRawData {
	double[] bathyDepointage ;
	double[] accross ;
	float[] along ;
	boolean[] valid;

	double[] bathyDepth ;
	public double[] twoWayTravelTime;
	int beamCount;
	//	double range[];
	double soundSpeed;
	double shipLatitude;
	double shipLongitude;
	double headingRad;
	double roll;
	// mean along distance for valid beams
	double meanValidAlongDistance;
	double depthAntenna;

	public void allocate(int beamCount)
	{
		this.beamCount=beamCount;
		bathyDepointage = new double [beamCount];
		accross = new double [beamCount];
		along = new float [beamCount];
		bathyDepth = new double [beamCount];
		twoWayTravelTime = new double [beamCount];
		valid=new boolean[beamCount];
	}
}
