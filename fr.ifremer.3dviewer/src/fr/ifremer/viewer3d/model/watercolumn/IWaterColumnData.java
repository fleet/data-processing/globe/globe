package fr.ifremer.viewer3d.model.watercolumn;

import java.io.File;
import java.util.List;

import fr.ifremer.globe.core.model.file.IInfos;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

public interface IWaterColumnData extends IInfos {

	List<Long> getTimeList();

	int getDataCount();

	int getRxBeamCount();

	int getTxBeamCount();

	int getPingCount();

	int getPingAtIndex(int dataIndex);

	int getIndexForPing(int ping);

	Long getTime(int dataIndex);

	LatLon getLatlon1(int dataIndex);

	LatLon getLatlon2(int dataIndex);

	double getBottomElevation(int dataIndex);

	double getTopElevation(int dataIndex);

	LatLon getLatLonCenter(int dataIndex);

	List<Position> getPositions();

	Position getPosition(int dataIndex);

	File getFile();

}
