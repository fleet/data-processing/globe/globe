package fr.ifremer.viewer3d.model.watercolumn;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.viewer3d.deprecated.JulianDay;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

/**
 * Watercolumn data for vertical images.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class WaterColumnData implements IWaterColumnData {

	/**
	 * Watercolumn data input file (xml).
	 */
	private final File inputFile;

	private List<Double> topElevationList;
	private List<Double> bottomElevationList;

	/**
	 * Port lat/lon.
	 */
	private List<LatLon> latlonList1;

	/**
	 * Starboard lat/lon.
	 */
	private List<LatLon> latlonList2;
	private List<Integer> indexList;
	private Map<Integer, Integer> indexMap;

	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	private String imgDir;

	/**
	 * List of all the images
	 */
	private List<String> imgList = new ArrayList<String>();

	/**
	 * The infos displayed by the popupmenu action Infos
	 */
	private AbstractInfos infos;

	/*
	 * Add M.antoine 12/04/2010
	 */

	public AbstractInfos getInfos() {
		return infos;
	}

	/**
	 * Center lat/lon.
	 */
	private List<LatLon> latlonCenterList;

	private List<Long> timeList;

	private List<Position> positions = new ArrayList<Position>();

	@Override
	public List<Position> getPositions() {
		return this.positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	/*
	 * end add M.antoine 12/04/2010
	 */

	/**
	 * Ctor.
	 * 
	 * @param file
	 *            Watercolumn data input file (xml).
	 */
	public WaterColumnData(File file) {
		this.inputFile = file;
		this.bottomElevationList = new ArrayList<Double>();
		this.topElevationList = new ArrayList<Double>();
		this.latlonList1 = new ArrayList<LatLon>();
		this.latlonList2 = new ArrayList<LatLon>();
		this.latlonCenterList = new ArrayList<LatLon>();
		this.indexList = new ArrayList<Integer>();
		this.indexMap = new HashMap<Integer, Integer>();
		this.imgDir = null;
		this.infos = new AbstractInfos();
		this.timeList = new ArrayList<Long>();
	}

	/**
	 * Watercolumn data input file (xml).
	 */
	@Override
	public File getFile() {
		return this.inputFile;
	}

	public List<Double> getBottomElevationList() {
		return this.bottomElevationList;
	}

	@Override
	public double getBottomElevation(int index) {
		return this.bottomElevationList.get(index);
	}

	public void addBottomElevation(Double bottomElevation) {
		this.bottomElevationList.add(bottomElevation);
	}

	/*
	 * Add M.antoine 12/04/2010
	 */
	@Override
	public List<Long> getTimeList() {
		return this.timeList;
	}

	@Override
	public Long getTime(int index) {
		return this.timeList.get(index);
	}

	public void addTime(long time) {
		this.timeList.add(time);
	}

	public void addTime(Double date, Double hour) {
		long day = JulianDay.fromJulian(date, hour);
		this.timeList.add(day);
	}

	/*
	 * end add M.antoine 12/04/2010
	 */

	@Override
	public double getTopElevation(int idx) {
		// default elevation is 0
		if (this.topElevationList.size() == 0) {
			return 0;
		}
		return this.topElevationList.get(idx);
	}

	public void addTopElevation(Double topElevation) {
		this.topElevationList.add(topElevation);
	}

	public List<LatLon> getLatlonList1() {
		return this.latlonList1;
	}

	@Override
	public LatLon getLatlon1(int index) {
		return this.latlonList1.get(index);
	}

	public void addLatlon1(LatLon latlon1) {
		this.latlonList1.add(latlon1);
	}

	public List<LatLon> getLatlonList2() {
		return this.latlonList2;
	}

	@Override
	public LatLon getLatlon2(int index) {
		return this.latlonList2.get(index);
	}

	public void addLatlon2(LatLon latlon2) {
		this.latlonList2.add(latlon2);
	}

	public List<Integer> getIndexList() {
		return this.indexList;
	}

	@Override
	public int getPingAtIndex(int index) {
		return this.indexList.get(index);
	}

	public void addIndex(Integer index) {
		this.indexList.add(index);
	}

	public Map<Integer, Integer> getIndexMap() {
		return this.indexMap;
	}

	/**
	 * 
	 * @param ping
	 * @return index corresponding to the ping
	 */
	@Override
	public int getIndexForPing(int ping) {
		if(indexMap.get(ping)!=null)
			return this.indexMap.get(ping);
		return 0;
	}

	public void addIndexMap(int ping, int index) {
		this.indexMap.put(ping, index);
	}

	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	public String getImgDir() {
		return this.imgDir;
	}

	public List<String> getImgList() {
		return imgList;
	}

	public void setImgList(List<String> imgList) {
		this.imgList = imgList;
	}
	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	public void setImgDir(String directory) {
		this.imgDir = directory;
	}

	@Override
	public int getDataCount() {
		return this.indexList.size();
	}



	@Override
	public List<Property<?>> getProperties() {
		return this.infos.getProperties();
	}

	public void addLatLonCenter(LatLon latlonCenter) {
		this.latlonCenterList.add(latlonCenter);
	}

	@Override
	public LatLon getLatLonCenter(int index) {
		return this.latlonCenterList.get(index);
	}

	@Override
	public int getRxBeamCount() {
		// NA
		return 0;
	}

	@Override
	public int getPingCount() {
		return this.imgList.size();
	}

	@Override
	public Position getPosition(int dataIndex) {
		return this.positions.get(dataIndex);
	}

	@Override
	public int getTxBeamCount() {
		// NA
		return 0;
	}
}
