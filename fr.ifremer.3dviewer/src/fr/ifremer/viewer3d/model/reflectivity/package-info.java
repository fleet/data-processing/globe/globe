/**
 * This package contains classes for vertical water column data.
 * <hr/>
 * Sur la version V1.0, les échogrammes sont visibles transversalement à la
 * navigation. IFREMER souhaite disposer d’une représentation le long de la
 * navigation.
 * <p>
 * Les données d’entrée de cette représentation sont les suivantes :
 * <ul>
 * <li>Fichier de temps ou de Profondeur,
 * <li>Fichier de latitude en fonction de la valeur Across Distance (X),
 * <li>Fichier de longitude en fonction de la valeur Across Distance (X),
 * <li>Fichier de valeur selon les axes Ping/Profondeur (Z)/Across Distance (X),
 * <li>Fichier de navigation latitude, longitude, time.
 * </ul>
 * </p>
 * <p>
 * On travaille avec une image dont le contenu d’origine est non plus une image
 * « PNG » comme sur les échogrammes mais des matrices de données auxquelles on
 * accède interactivement. L’opérateur choisit une valeur X sur un curseur
 * allant de Xmin à Xmax selon le layer chargé. On récupère les latitudes et les
 * longitudes pour la valeur de X sélectionnée par l’opérateur. Cette valeur
 * sert d’index dans la lecture du fichier. La visualisation est, compte tenu de
 * l’indexation choisie pour le contenu de fichier, parallèle à la navigation.
 * </p>
 * On introduit dans cette nouvelle vision la mise à disposition d’un variable
 * Zmax. En effet, actuellement, les échogrammes sont compris entre la surface
 * et le fond. Pour les images longitudinales et les sondeurs de sédiments,
 * cette valeur Zmax peut être différente de 0.
 * <img src="doc-files/Bathyscope_REQ-NR-003.png" alt="REQ-NR-003" />
 * <h2>Extract from Altran_OUEST_ASD_RCU_PTF_100493_0_d</h2>
 * <p style="background: lavender" >
 * <b>Exigence REQ-NR-003 : Water column le long de la navigation </b><br/>
 * Représentation sur un axe parallèle à une navigation d’une Water column le
 * long de la navigation des valeurs d’échogrammes.<br/>
 * L’utilisateur peut accéder via un curseur aux données selon l’axe X, Across
 * Distance.<br/>
 * Introduction d’une variable Zmax.
 * </p>
 */
package fr.ifremer.viewer3d.model.reflectivity;

