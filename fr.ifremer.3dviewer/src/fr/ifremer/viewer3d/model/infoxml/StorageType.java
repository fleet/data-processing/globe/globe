//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2016.11.22 � 01:40:12 PM CET 
//


package fr.ifremer.viewer3d.model.infoxml;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour StorageType.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="StorageType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="char"/>
 *     &lt;enumeration value="uint8"/>
 *     &lt;enumeration value="uchar"/>
 *     &lt;enumeration value="int16"/>
 *     &lt;enumeration value="uint16"/>
 *     &lt;enumeration value="int32"/>
 *     &lt;enumeration value="uint32"/>
 *     &lt;enumeration value="int64"/>
 *     &lt;enumeration value="uint64"/>
 *     &lt;enumeration value="single"/>
 *     &lt;enumeration value="double"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StorageType")
@XmlEnum
public enum StorageType {

	@XmlEnumValue("char")
	CHAR("char"),
	@XmlEnumValue("uint8")
	UINT_8("uint8"),
	@XmlEnumValue("uchar")
	UCHAR("uchar"),
	@XmlEnumValue("int16")
	INT_16("int16"),
	@XmlEnumValue("uint16")
	UINT_16("uint16"),
	@XmlEnumValue("int32")
	INT_32("int32"),
	@XmlEnumValue("uint32")
	UINT_32("uint32"),
	@XmlEnumValue("int64")
	INT_64("int64"),
	@XmlEnumValue("uint64")
	UINT_64("uint64"),
	@XmlEnumValue("single")
	SINGLE("single"),
	@XmlEnumValue("double")
	DOUBLE("double");
	private final String value;

	StorageType(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static StorageType fromValue(String v) {
		for (StorageType c: StorageType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
