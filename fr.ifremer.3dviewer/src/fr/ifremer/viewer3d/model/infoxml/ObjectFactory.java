//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2016.11.22 � 01:40:12 PM CET 
//


package fr.ifremer.viewer3d.model.infoxml;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _ChiefScientist_QNAME = new QName("", "ChiefScientist");
	private final static QName _DepthBottom_QNAME = new QName("", "DepthBottom");
	private final static QName _EndTime_QNAME = new QName("", "EndTime");
	private final static QName _DepthTop_QNAME = new QName("", "DepthTop");
	private final static QName _FileName_QNAME = new QName("", "FileName");
	private final static QName _StartTime_QNAME = new QName("", "StartTime");
	private final static QName _Unit_QNAME = new QName("", "Unit");
	private final static QName _Endianness_QNAME = new QName("", "Endianness");
	private final static QName _NbPings_QNAME = new QName("", "nbPings");
	private final static QName _NbRows_QNAME = new QName("", "nbRows");
	private final static QName _Name_QNAME = new QName("", "Name");
	private final static QName _Sounder_QNAME = new QName("", "Sounder");
	private final static QName _Vessel_QNAME = new QName("", "Vessel");
	private final static QName _Survey_QNAME = new QName("", "Survey");
	private final static QName _Storage_QNAME = new QName("", "Storage");
	private final static QName _ExtractedFrom_QNAME = new QName("", "ExtractedFrom");
	private final static QName _FormatVersion_QNAME = new QName("", "FormatVersion");
	private final static QName _NbSlices_QNAME = new QName("", "nbSlices");
	private final static QName _Signature2_QNAME = new QName("", "Signature2");
	private final static QName _Signature1_QNAME = new QName("", "Signature1");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link Signals }
	 * 
	 */
	public Signals createSignals() {
		return new Signals();
	}

	/**
	 * Create an instance of {@link Item }
	 * 
	 */
	public Item createItem() {
		return new Item();
	}

	/**
	 * Create an instance of {@link Dimensions }
	 * 
	 */
	public Dimensions createDimensions() {
		return new Dimensions();
	}

	/**
	 * Create an instance of {@link Images }
	 * 
	 */
	public Images createImages() {
		return new Images();
	}

	/**
	 * Create an instance of {@link Info }
	 * 
	 */
	public Info createInfo() {
		return new Info();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "ChiefScientist")
	public JAXBElement<String> createChiefScientist(String value) {
		return new JAXBElement<String>(_ChiefScientist_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "DepthBottom")
	public JAXBElement<Float> createDepthBottom(Float value) {
		return new JAXBElement<Float>(_DepthBottom_QNAME, Float.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "EndTime")
	public JAXBElement<XMLGregorianCalendar> createEndTime(XMLGregorianCalendar value) {
		return new JAXBElement<XMLGregorianCalendar>(_EndTime_QNAME, XMLGregorianCalendar.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "DepthTop")
	public JAXBElement<Float> createDepthTop(Float value) {
		return new JAXBElement<Float>(_DepthTop_QNAME, Float.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "FileName")
	public JAXBElement<String> createFileName(String value) {
		return new JAXBElement<String>(_FileName_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "StartTime")
	public JAXBElement<XMLGregorianCalendar> createStartTime(XMLGregorianCalendar value) {
		return new JAXBElement<XMLGregorianCalendar>(_StartTime_QNAME, XMLGregorianCalendar.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Unit")
	public JAXBElement<String> createUnit(String value) {
		return new JAXBElement<String>(_Unit_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link EndianType }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Endianness")
	public JAXBElement<EndianType> createEndianness(EndianType value) {
		return new JAXBElement<EndianType>(_Endianness_QNAME, EndianType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "nbPings")
	public JAXBElement<Integer> createNbPings(Integer value) {
		return new JAXBElement<Integer>(_NbPings_QNAME, Integer.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "nbRows")
	public JAXBElement<Integer> createNbRows(Integer value) {
		return new JAXBElement<Integer>(_NbRows_QNAME, Integer.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Name")
	public JAXBElement<String> createName(String value) {
		return new JAXBElement<String>(_Name_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Sounder")
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	public JAXBElement<String> createSounder(String value) {
		return new JAXBElement<String>(_Sounder_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Vessel")
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	public JAXBElement<String> createVessel(String value) {
		return new JAXBElement<String>(_Vessel_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Survey")
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	public JAXBElement<String> createSurvey(String value) {
		return new JAXBElement<String>(_Survey_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link StorageType }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Storage")
	public JAXBElement<StorageType> createStorage(StorageType value) {
		return new JAXBElement<StorageType>(_Storage_QNAME, StorageType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "ExtractedFrom")
	public JAXBElement<String> createExtractedFrom(String value) {
		return new JAXBElement<String>(_ExtractedFrom_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "FormatVersion")
	public JAXBElement<Integer> createFormatVersion(Integer value) {
		return new JAXBElement<Integer>(_FormatVersion_QNAME, Integer.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "nbSlices")
	public JAXBElement<Integer> createNbSlices(Integer value) {
		return new JAXBElement<Integer>(_NbSlices_QNAME, Integer.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Signature2")
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	public JAXBElement<String> createSignature2(String value) {
		return new JAXBElement<String>(_Signature2_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "Signature1")
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	public JAXBElement<String> createSignature1(String value) {
		return new JAXBElement<String>(_Signature1_QNAME, String.class, null, value);
	}

}
