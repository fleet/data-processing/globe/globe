//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2016.11.22 � 01:40:12 PM CET 
//


package fr.ifremer.viewer3d.model.infoxml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Signature1"/>
 *         &lt;element ref="{}Signature2"/>
 *         &lt;element ref="{}ImageColor"/>
 *         &lt;element ref="{}FormatVersion"/>
 *         &lt;element ref="{}Name" minOccurs="0"/>
 *         &lt;element ref="{}ExtractedFrom"/>
 *         &lt;element ref="{}Survey"/>
 *         &lt;element ref="{}Vessel"/>
 *         &lt;element ref="{}Sounder"/>
 *         &lt;element ref="{}ChiefScientist"/>
 *         &lt;element ref="{}DepthTop" minOccurs="0"/>
 *         &lt;element ref="{}DepthBottom" minOccurs="0"/>
 *         &lt;element ref="{}MinValue" minOccurs="0"/>
 *         &lt;element ref="{}MaxValue" minOccurs="0"/>
 *         &lt;element ref="{}Dimensions"/>
 *         &lt;element ref="{}Signals"/>
 *         &lt;element ref="{}Images"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"signature1",
		"signature2",
		"imageColor",
		"formatVersion",
		"name",
		"extractedFrom",
		"survey",
		"vessel",
		"sounder",
		"chiefScientist",
		"depthTop",
		"depthBottom",
		"minValue",
		"maxValue",
		"dimensions",
		"signals",
		"images"
})
@XmlRootElement(name = "Info")
public class Info {

	@XmlElement(name = "Signature1", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	protected String signature1;
	@XmlElement(name = "Signature2", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	protected String signature2;
	@XmlElement(name = "ImageColor", required = false)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	private String imageColor;
	@XmlElement(name = "FormatVersion")
	protected int formatVersion;
	@XmlElement(name = "Name")
	protected String name;
	@XmlElement(name = "ExtractedFrom", required = true)
	protected String extractedFrom;
	@XmlElement(name = "Survey", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	protected String survey;
	@XmlElement(name = "Vessel", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	protected String vessel;
	@XmlElement(name = "Sounder", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	protected String sounder;
	@XmlElement(name = "ChiefScientist", required = true)
	protected String chiefScientist;
	@XmlElement(name = "DepthTop")
	protected Float depthTop;
	@XmlElement(name = "DepthBottom")
	protected Float depthBottom;
	@XmlElement(name = "MinValue")
	protected Float minValue;
	@XmlElement(name = "MaxValue")
	protected Float maxValue;
	@XmlElement(name = "Dimensions", required = true)
	protected Dimensions dimensions;
	@XmlElement(name = "Signals", required = true)
	protected Signals signals;
	@XmlElement(name = "Images", required = true)
	protected Images images;

	/**
	 * Obtient la valeur de la propri�t� signature1.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSignature1() {
		return signature1;
	}

	/**
	 * D�finit la valeur de la propri�t� signature1.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSignature1(String value) {
		this.signature1 = value;
	}

	/**
	 * Obtient la valeur de la propri�t� signature2.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSignature2() {
		return signature2;
	}

	/**
	 * D�finit la valeur de la propri�t� signature2.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSignature2(String value) {
		this.signature2 = value;
	}

	/**
	 * Obtient la valeur de la propri�t� formatVersion.
	 * 
	 */
	public int getFormatVersion() {
		return formatVersion;
	}

	/**
	 * D�finit la valeur de la propri�t� formatVersion.
	 * 
	 */
	public void setFormatVersion(int value) {
		this.formatVersion = value;
	}

	/**
	 * Obtient la valeur de la propri�t� name.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getName() {
		return name;
	}

	/**
	 * D�finit la valeur de la propri�t� name.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Obtient la valeur de la propri�t� extractedFrom.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getExtractedFrom() {
		return extractedFrom;
	}

	/**
	 * D�finit la valeur de la propri�t� extractedFrom.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setExtractedFrom(String value) {
		this.extractedFrom = value;
	}

	/**
	 * Obtient la valeur de la propri�t� survey.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSurvey() {
		return survey;
	}

	/**
	 * D�finit la valeur de la propri�t� survey.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSurvey(String value) {
		this.survey = value;
	}

	/**
	 * Obtient la valeur de la propri�t� vessel.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getVessel() {
		return vessel;
	}

	/**
	 * D�finit la valeur de la propri�t� vessel.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setVessel(String value) {
		this.vessel = value;
	}

	/**
	 * Obtient la valeur de la propri�t� sounder.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSounder() {
		return sounder;
	}

	/**
	 * D�finit la valeur de la propri�t� sounder.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSounder(String value) {
		this.sounder = value;
	}

	/**
	 * Obtient la valeur de la propri�t� chiefScientist.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getChiefScientist() {
		return chiefScientist;
	}

	/**
	 * D�finit la valeur de la propri�t� chiefScientist.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setChiefScientist(String value) {
		this.chiefScientist = value;
	}

	/**
	 * Obtient la valeur de la propri�t� depthTop.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Float }
	 *     
	 */
	public Float getDepthTop() {
		return depthTop;
	}

	/**
	 * D�finit la valeur de la propri�t� depthTop.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Float }
	 *     
	 */
	public void setDepthTop(Float value) {
		this.depthTop = value;
	}

	/**
	 * Obtient la valeur de la propri�t� depthBottom.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Float }
	 *     
	 */
	public Float getDepthBottom() {
		return depthBottom;
	}

	/**
	 * D�finit la valeur de la propri�t� depthBottom.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Float }
	 *     
	 */
	public void setDepthBottom(Float value) {
		this.depthBottom = value;
	}



	/**
	 * Obtient la valeur de la propri�t� minvalue.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Float }
	 *     
	 */
	public Float getMinValue() {
		return minValue;
	}

	/**
	 * D�finit la valeur de la propri�t� minvalue.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Float }
	 *     
	 */
	public void setMinValue(Float value) {
		this.minValue = value;
	}




	/**
	 * Obtient la valeur de la propri�t� maxvalue.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Float }
	 *     
	 */
	public Float getMaxValue() {
		return maxValue;
	}

	/**
	 * D�finit la valeur de la propri�t� maxvalue.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Float }
	 *     
	 */
	public void setMaxValue(Float value) {
		this.maxValue = value;
	}



	/**
	 * Obtient la valeur de la propri�t� dimensions.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Dimensions }
	 *     
	 */
	public Dimensions getDimensions() {
		return dimensions;
	}

	/**
	 * D�finit la valeur de la propri�t� dimensions.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Dimensions }
	 *     
	 */
	public void setDimensions(Dimensions value) {
		this.dimensions = value;
	}

	/**
	 * Obtient la valeur de la propri�t� signals.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Signals }
	 *     
	 */
	public Signals getSignals() {
		return signals;
	}

	/**
	 * D�finit la valeur de la propri�t� signals.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Signals }
	 *     
	 */
	public void setSignals(Signals value) {
		this.signals = value;
	}

	/**
	 * Obtient la valeur de la propri�t� images.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Images }
	 *     
	 */
	public Images getImages() {
		return images;
	}

	/**
	 * D�finit la valeur de la propri�t� images.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Images }
	 *     
	 */
	public void setImages(Images value) {
		this.images = value;
	}

	public void setImageColor(String value) {
		this.imageColor = value;

	}
	public String getImageColor()
	{
		return imageColor;
	}

}
