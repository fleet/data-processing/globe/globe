//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2016.11.22 � 01:40:12 PM CET 
//


package fr.ifremer.viewer3d.model.infoxml;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour EndianType.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EndianType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="little-endian"/>
 *     &lt;enumeration value="big-endian"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EndianType")
@XmlEnum
public enum EndianType {

	@XmlEnumValue("little-endian")
	LITTLE_ENDIAN("little-endian"),
	@XmlEnumValue("big-endian")
	BIG_ENDIAN("big-endian");
	private final String value;

	EndianType(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static EndianType fromValue(String v) {
		for (EndianType c: EndianType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
