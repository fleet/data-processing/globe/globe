//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2016.11.22 � 01:40:12 PM CET 
//


package fr.ifremer.viewer3d.model.infoxml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Name"/>
 *         &lt;element ref="{}Dimensions"/>
 *         &lt;element ref="{}Storage"/>
 *         &lt;element ref="{}Endianness" minOccurs="0"/>
 *         &lt;element ref="{}Unit"/>
 *         &lt;element ref="{}FileName"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"name",
		"dimensions",
		"storage",
		"endianness",
		"unit",
		"fileName"
})
@XmlRootElement(name = "item")
public class Item {

	@XmlElement(name = "Name", required = true)
	protected String name;
	@XmlElement(name = "Dimensions", required = true)
	protected Dimensions dimensions;
	@XmlElement(name = "Storage", required = true)
	@XmlSchemaType(name = "string")
	protected StorageType storage;
	@XmlElement(name = "Endianness")
	@XmlSchemaType(name = "string")
	protected EndianType endianness;
	@XmlElement(name = "Unit", required = true, nillable = true)
	protected String unit;
	@XmlElement(name = "FileName", required = true)
	protected String fileName;

	/**
	 * Obtient la valeur de la propri�t� name.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getName() {
		return name;
	}

	/**
	 * D�finit la valeur de la propri�t� name.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Obtient la valeur de la propri�t� dimensions.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Dimensions }
	 *     
	 */
	public Dimensions getDimensions() {
		return dimensions;
	}

	/**
	 * D�finit la valeur de la propri�t� dimensions.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Dimensions }
	 *     
	 */
	public void setDimensions(Dimensions value) {
		this.dimensions = value;
	}

	/**
	 * Obtient la valeur de la propri�t� storage.
	 * 
	 * @return
	 *     possible object is
	 *     {@link StorageType }
	 *     
	 */
	public StorageType getStorage() {
		return storage;
	}

	/**
	 * D�finit la valeur de la propri�t� storage.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link StorageType }
	 *     
	 */
	public void setStorage(StorageType value) {
		this.storage = value;
	}

	/**
	 * Obtient la valeur de la propri�t� endianness.
	 * 
	 * @return
	 *     possible object is
	 *     {@link EndianType }
	 *     
	 */
	public EndianType getEndianness() {
		return endianness;
	}

	/**
	 * D�finit la valeur de la propri�t� endianness.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link EndianType }
	 *     
	 */
	public void setEndianness(EndianType value) {
		this.endianness = value;
	}

	/**
	 * Obtient la valeur de la propri�t� unit.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * D�finit la valeur de la propri�t� unit.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setUnit(String value) {
		this.unit = value;
	}

	/**
	 * Obtient la valeur de la propri�t� fileName.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * D�finit la valeur de la propri�t� fileName.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setFileName(String value) {
		this.fileName = value;
	}

}
