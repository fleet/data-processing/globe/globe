/*
 * @License@
 */
package fr.ifremer.viewer3d.model;

// Start of user code for imports

import fr.ifremer.viewer3d.loaders.PlumeEchoesLoader;
import gov.nasa.worldwind.geom.Position;

// End of user code

/**
 * This class defines an independent echo, rendered as a translucent sphere.
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 * @generated
 */

public class Echo {

	/**
	 * Default constructor.
	 * 
	 * @generated
	 */
	public Echo() {
		super();
		// Start of user code Echo default ctor

		// End of user code
	}

	/**
	 * The totalEnergy attribute.
	 * 
	 * @generated
	 */
	private float _totalEnergy;

	/**
	 * The ping attribute.
	 * 
	 * @generated
	 */
	private int _ping;

	/**
	 * The energy for this echo (dB).
	 * 
	 * @generated
	 */
	private float _energy;

	/**
	 * The totalEnergy attribute.
	 * 
	 * @generated
	 */
	public float getTotalEnergy() {
		return this._totalEnergy;
	}

	/**
	 * * The totalEnergy attribute.
	 * 
	 * @generated
	 */
	public void setTotalEnergy(float totalEnergy) {
		this._totalEnergy = totalEnergy;
	}

	/**
	 * The ping attribute.
	 * 
	 * @generated
	 */
	public int getPing() {
		return this._ping;
	}

	/**
	 * * The ping attribute.
	 * 
	 * @generated
	 */
	public void setPing(int ping) {
		this._ping = ping;
	}

	/**
	 * Position for this echo.
	 * <p>
	 * Latitude (degrees), longitude (degrees) and elevation (meters).
	 * </p>
	 * 
	 * @generated
	 */
	private Position _position;

	// FT5440
	private int date;
	private long hour;
	private float xCoor;
	private float yCoor;
	private float angle;
	private float rangeInsample;
	private float acrossDistance;
	private float celerity;

	/**
	 * The energy for this echo (dB).
	 * 
	 * @generated
	 */
	public float getEnergy() {
		return this._energy;
	}

	/**
	 * The energy for this echo (dB).
	 * 
	 * @generated
	 */
	public void setEnergy(float energy) {
		this._energy = energy;
	}

	/**
	 * Position for this echo.
	 * <p>
	 * Latitude (degrees), longitude (degrees) and elevation (meters).
	 * </p>
	 * 
	 * @generated
	 */
	public Position getPosition() {
		return this._position;
	}

	/**
	 * Position for this echo.
	 * <p>
	 * Latitude (degrees), longitude (degrees) and elevation (meters).
	 * </p>
	 * 
	 * @generated
	 */
	public void setPosition(Position position) {
		this._position = position;
	}

	// REQ-IHM-004 - Markers Filtering - Return parameter value from its name
	public float getParameter(String parameterName) {
		float value = 0f;

		if (parameterName.equals(PlumeEchoesLoader.ALT_NAME)) {
			value = (float) this.getPosition().getAltitude();
		} else if (parameterName.equals(PlumeEchoesLoader.ENERGY_NAME)) {
			value = this._energy;
		} else if (parameterName.equals(PlumeEchoesLoader.LATITUDE_NAME)) {
			value = (float) this.getPosition().getLatitude().getDegrees();
		} else if (parameterName.equals(PlumeEchoesLoader.LONGITUDE_NAME)) {
			value = (float) this.getPosition().getLongitude().getDegrees();
		} else if (parameterName.equals(PlumeEchoesLoader.PING_NAME)) {
			value = this._ping;
		} else if (parameterName.equals(PlumeEchoesLoader.TOTAL_ENERGY_NAME)) {
			value = this._totalEnergy;
		} else if (parameterName.equals(PlumeEchoesLoader.DATE_NAME)) {
			value = this.date;
		} else if (parameterName.equals(PlumeEchoesLoader.HOUR_NAME)) {
			value = this.hour;
		} else if (parameterName.equals(PlumeEchoesLoader.XCOOR_NAME)) {
			value = this.xCoor;
		} else if (parameterName.equals(PlumeEchoesLoader.YCOOR_NAME)) {
			value = this.yCoor;
		} else if (parameterName.equals(PlumeEchoesLoader.ANGLE_NAME)) {
			value = this.angle;
		} else if (parameterName.equals(PlumeEchoesLoader.RANGE_IN_SAMPLE_NAME)) {
			value = this.rangeInsample;
		} else if (parameterName.equals(PlumeEchoesLoader.ACROSS_DISTANCE_NAME)) {
			value = this.acrossDistance;
		} else if (parameterName.equals(PlumeEchoesLoader.CELERITY_NAME)) {
			value = this.celerity;
		} else {
			value = 0f;
		}

		return value;
	}

	// FT5440
	public void setDate(int date) {
		this.date = date;
	}

	public void setHour(long hour) {
		this.hour = hour;
	}

	public void setXCoordinate(float xCoor) {
		this.xCoor = xCoor;
	}

	public void setYCoordinate(float yCoor) {
		this.yCoor = yCoor;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

	public void setRangeInSample(float range) {
		this.rangeInsample = range;

	}

	public void setAcrossDistance(float distance) {
		this.acrossDistance = distance;

	}

	public void setCelerity(float celerity) {
		this.celerity = celerity;
	}

	public int getDate() {
		return date;
	}

	public long getHour() {
		return hour;
	}

	public float getXCoor() {
		return xCoor;
	}

	public float getYCoor() {
		return yCoor;
	}

	public float getAngle() {
		return angle;
	}

	public float getRangeInsample() {
		return rangeInsample;
	}

	public float getAcrossDistance() {
		return acrossDistance;
	}

	public float getCelerity() {
		return celerity;
	}

	/**
	 * Return the precision of value
	 * 
	 * @param parameterName
	 * @return
	 */
	public int getPrecision(String parameterName) {
		int value = 0;

		if (parameterName.equals(PlumeEchoesLoader.ALT_NAME)) {
			value = 2;
		} else if (parameterName.equals(PlumeEchoesLoader.ENERGY_NAME)) {
			value = 2;
		} else if (parameterName.equals(PlumeEchoesLoader.LATITUDE_NAME)) {
			value = 6;
		} else if (parameterName.equals(PlumeEchoesLoader.LONGITUDE_NAME)) {
			value = 6;
		} else if (parameterName.equals(PlumeEchoesLoader.PING_NAME)) {
			value = 0;
		} else if (parameterName.equals(PlumeEchoesLoader.TOTAL_ENERGY_NAME)) {
			value = 2;
		} else if (parameterName.equals(PlumeEchoesLoader.DATE_NAME)) {
			value = 0;
		} else if (parameterName.equals(PlumeEchoesLoader.HOUR_NAME)) {
			value = 0;
		} else if (parameterName.equals(PlumeEchoesLoader.XCOOR_NAME)) {
			value = 2;
		} else if (parameterName.equals(PlumeEchoesLoader.YCOOR_NAME)) {
			value = 2;
		} else if (parameterName.equals(PlumeEchoesLoader.ANGLE_NAME)) {
			value = 2;
		} else if (parameterName.equals(PlumeEchoesLoader.RANGE_IN_SAMPLE_NAME)) {
			value = 3;
		} else if (parameterName.equals(PlumeEchoesLoader.ACROSS_DISTANCE_NAME)) {
			value = 2;
		} else if (parameterName.equals(PlumeEchoesLoader.CELERITY_NAME)) {
			value = 2;
		} else {
			value = 0;
		}

		return value;
	}
}
