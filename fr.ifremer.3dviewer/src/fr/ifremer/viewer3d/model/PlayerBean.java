package fr.ifremer.viewer3d.model;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

import javax.swing.Timer;

/**
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 * 
 */
public class PlayerBean implements ActionListener {

	/** Property name for reversePlaying change support. */
	public static final String PROPERTY_REVERSEPLAYING = "reversePlaying";

	/** Property name for playing change support. */
	public static final String PROPERTY_PLAYING = "playing";

	/** Property name for loop change support. */
	public static final String PROPERTY_LOOP = "loop";

	/** Property name for playIndex change support. */
	public static final String PROPERTY_PLAY_INDEX = "playIndex";
	public static final String PROPERTY_PLAY_NUMBER = "playNumber";

	/** Property name for minPlayIndex change support. */
	public static final String PROPERTY_MIN_PLAY_INDEX = "minPlayIndex";
	public static final String PROPERTY_MIN_PLAY_NUMBER = "minPlayNumber";

	/** Property name for maxPlayIndex change support. */
	public static final String PROPERTY_MAX_PLAY_INDEX = "maxPlayIndex";
	public static final String PROPERTY_MAX_PLAY_NUMBER = "maxPlayNumber";

	/** Utility instance for property change support. */
	protected transient PropertyChangeSupport _pcs = new PropertyChangeSupport(this);

	/** ID used to distinct multiple playerBeans */
	private String name;

	/** Current max play index. */
	private int _maxPlayIndex = 0;

	/** Default max play number. */
	private int defaultMaxPlayIndex = 0;

	/** The playingSpeed attribute. */
	private float _playingSpeed = 1.0f;

	/** The reversePlaying attribute. */
	private boolean _reversePlaying = false;

	/** True if this player is currently playing. */
	private boolean _playing = false;

	/**
	 * True if this player should replay at first index after last index has
	 * been played (loop).
	 */
	private boolean _loop = false;

	/** Timer used for autoplay mode */
	private Timer timer;

	/** Default timer delay */
	private int timerDefaultDelay = 400;

	/** The current play index. */
	private int _playIndex = -1;

	/** Current min playindex. */
	private int _minPlayIndex = Integer.MAX_VALUE;

	/** Default min play number. */
	private int defaultMinPlayIndex = 0;

	private int _maxPlayNumber = 0;

	private int _minPlayNumber = 0;

	private List<Integer> _indexList;

	private Integer _playNumber = 1;

	public PlayerBean() {
		this("");
	}

	public PlayerBean(String name) {
		this.name = name;
		timer = new Timer(timerDefaultDelay, this);
	}

	public PlayerBean(String name, PlayerBean initWith) {
		this.name = name;
		timer = new Timer(timerDefaultDelay, this);

		_maxPlayIndex = initWith._maxPlayIndex;
		defaultMaxPlayIndex = initWith.defaultMaxPlayIndex;	
		_playingSpeed = initWith._playingSpeed;
		_reversePlaying = initWith._reversePlaying;
		_playing = initWith._playing;
		_loop = initWith._loop;
		_playIndex = initWith._playIndex;
		_minPlayIndex = initWith._minPlayIndex;
		defaultMinPlayIndex = initWith.defaultMinPlayIndex;
		_maxPlayNumber = initWith._maxPlayNumber;
		_minPlayNumber = initWith._minPlayNumber;
		_indexList =  initWith._indexList;
		_playNumber = initWith._playNumber;
	}


	/**
	 * Adds a change listener to this object.
	 * 
	 * @param listener
	 *            the listener to add
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		_pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Removes a change listener from this object.
	 * 
	 * @param listener
	 *            the listener to remove
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		_pcs.removePropertyChangeListener(listener);
	}

	/**
	 * For Serializable and Externalizable classes, the readResolve method
	 * allows a class to replace/resolve the object read from the stream before
	 * it is returned to the caller. By implementing the readResolve method, a
	 * class can directly control the types and instances of its own instances
	 * being deserialized.
	 * 
	 * 
	 */
	private Object readResolve() {
		_pcs = new PropertyChangeSupport(this);
		// Start of user code PlayerBean readResolve

		// End of user code
		return this;
	}

	public float getPlayingSpeed() {
		return this._playingSpeed;
	}

	public void setPlayingSpeed(float playingSpeed) {
		if (this._playingSpeed != playingSpeed) {
			if (playingSpeed > 0) {
				float delay = timerDefaultDelay / playingSpeed;
				this.timer.setDelay((int) delay);
			}
			this._playingSpeed = playingSpeed;
		}
	}

	/**
	 * The reversePlaying attribute.
	 */
	public boolean getReversePlaying() {
		return this._reversePlaying;
	}

	/**
	 * True if this player is currently playing.
	 */
	public boolean getPlaying() {
		return this._playing;
	}

	/**
	 * True if this player is currently playing.
	 */
	public void setPlaying(boolean playing) {
		if (this._playing != playing) {
			if (playing) {
				setReversePlaying(false);
				timer.start();
			}
			else {
				timer.stop();
			}
			_pcs.firePropertyChange(PROPERTY_PLAYING, this._playing, this._playing = playing);
		}
	}

	/**
	 * * The reversePlaying attribute.
	 */
	public void setReversePlaying(boolean reversePlaying) {
		if (this._reversePlaying != reversePlaying) {
			if (reversePlaying) {
				setPlaying(false);
				timer.start();
			}
			else {
				timer.stop();
			}
			_pcs.firePropertyChange(PROPERTY_REVERSEPLAYING, this._reversePlaying, this._reversePlaying = reversePlaying);
		}
	}

	/**
	 * True if this player should replay at first index after last index has
	 * been played (loop).
	 * 
	 * 
	 */
	public boolean getLoop() {
		return this._loop;
	}

	/**
	 * True if this player should replay at first index after last index has
	 * been played (loop).
	 * 
	 * 
	 */
	public void setLoop(boolean loop) {
		if (this._loop != loop) {
			_pcs.firePropertyChange(PROPERTY_LOOP, this._loop, this._loop = loop);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private void jumpToNextPlayIndex() {
		int delta = Math.max(1, (int) _playingSpeed);
		int newIdx = _playIndex;
		if (_reversePlaying) {
			newIdx -= delta;
			if (newIdx < _minPlayIndex) {
				newIdx = _maxPlayIndex;
				if (!_loop) {
					setReversePlaying(false);
				}
			}
		} else {
			newIdx += delta;
			if (newIdx > _maxPlayIndex) {
				newIdx = _minPlayIndex;
				if (!_loop) {
					setPlaying(false);
				}
			}
		}
		setPlayIndex(newIdx);
	}

	/**
	 * The current play index.
	 */
	public int getPlayIndex() {
		return this._playIndex;
	}

	/**
	 * Set the current play index.
	 */
	public void setPlayIndex(int playIndex) {
		if (this._playIndex != playIndex) {
			_pcs.firePropertyChange(PROPERTY_PLAY_INDEX, this._playIndex, this._playIndex = playIndex);
			// BTC j'ai ajouté pour éviter l'apparition d'exception mais je ne
			// sais si c'est pertinent
			// if (_indexList != null && playIndex > 0) {
			if (_indexList != null && playIndex >= 0 && playIndex < _indexList.size()) {
				_pcs.firePropertyChange(PROPERTY_PLAY_NUMBER, this._playNumber, this._playNumber = _indexList.get(playIndex));
			}
		}
	}

	/**
	 * The start play index.
	 */
	public int getMinPlayIndex() {
		return this._minPlayIndex;
	}

	/**
	 * The start play index.
	 */
	public void setMinPlayIndex(int minPlayIndex) {
		if (this._minPlayIndex != minPlayIndex) {
			if (defaultMinPlayIndex == 0) {
				defaultMinPlayIndex = minPlayIndex;
			}
			_pcs.firePropertyChange(PROPERTY_MIN_PLAY_INDEX, this._minPlayIndex, this._minPlayIndex = minPlayIndex);

			if (_indexList != null) {
				_pcs.firePropertyChange(PROPERTY_MIN_PLAY_NUMBER, this._minPlayNumber, this._minPlayNumber = _indexList.get(minPlayIndex));
			}
		}
	}

	/**
	 * The last play index.
	 */
	public int getMaxPlayIndex() {
		return this._maxPlayIndex;
	}

	/**
	 * The last play index.
	 */
	public void setMaxPlayIndex(int maxPlayIndex) {
		if (this._maxPlayIndex != maxPlayIndex) {
			if (defaultMaxPlayIndex == 0) {
				defaultMaxPlayIndex = maxPlayIndex;
			}
			_pcs.firePropertyChange(PROPERTY_MAX_PLAY_INDEX, this._maxPlayIndex, this._maxPlayIndex = maxPlayIndex);

			if (_indexList != null) {
				_pcs.firePropertyChange(PROPERTY_MAX_PLAY_NUMBER, this._maxPlayNumber, this._maxPlayNumber = _indexList.get(maxPlayIndex));
			}
		}
	}

	public void setMinPlayNumber(int minPlayNumber) {
		if (this._minPlayNumber != minPlayNumber) {
			_pcs.firePropertyChange(PROPERTY_MIN_PLAY_NUMBER, this._minPlayNumber, this._minPlayNumber = minPlayNumber);

			if (_indexList != null) {
				int index = _indexList.indexOf(minPlayNumber);
				if (index != -1 && this._minPlayIndex != index) {
					_pcs.firePropertyChange(PROPERTY_MIN_PLAY_INDEX, this._minPlayIndex, this._minPlayIndex = index);
				}
			}
		}
	}

	public void setMaxPlayNumber(int maxPlayNumber) {
		if (this._maxPlayNumber != maxPlayNumber) {
			_pcs.firePropertyChange(PROPERTY_MAX_PLAY_NUMBER, this._maxPlayNumber, this._maxPlayNumber = maxPlayNumber);

			if (_indexList != null) {
				int index = _indexList.indexOf(maxPlayNumber);
				if (index != -1 && this._maxPlayIndex != index) {
					_pcs.firePropertyChange(PROPERTY_MAX_PLAY_INDEX, this._maxPlayIndex, this._maxPlayIndex = index);
				}
			}
		}
	}

	public void setPlayNumber(int playNumber) {
		if (this._playNumber != playNumber) {
			_pcs.firePropertyChange(PROPERTY_PLAY_NUMBER, this._playNumber, this._playNumber = playNumber);

			if (_indexList != null) {
				int index = _indexList.indexOf(playNumber);
				if (index != -1 && this._playIndex != index) {
					_pcs.firePropertyChange(PROPERTY_PLAY_INDEX, this._playIndex, this._playIndex = index);
				}
			}
		}
	}


	public int getMinPlayNumber() {
		return this._minPlayNumber;
	}

	public int getMaxPlayNumber() {
		return this._maxPlayNumber;
	}

	public int getPlayNumber() {
		return this._playNumber;
	}

	public List<Integer> getIndexList() {
		return this._indexList;
	}

	public void setIndexList(List<Integer> indexList) {
		this._indexList = indexList;
	}

	/***
	 * set min/max play index and number according to indexes without
	 * triggering events
	 * 
	 * @param minPlayIndex
	 * @param maxPlayIndex
	 */
	public void initializeLimitValuesWithoutTiggeringEvents(int minPlayIndex, int maxPlayIndex) {
		if (_indexList != null) {
			_minPlayNumber = _indexList.get(minPlayIndex);
			_maxPlayNumber = _indexList.get(maxPlayIndex);
		}

		_minPlayIndex = minPlayIndex;
		_maxPlayIndex = maxPlayIndex;
	}

	public int getDefaultMinPlayIndex() {
		return defaultMinPlayIndex;
	}

	public int getDefaultMaxPlayIndex() {
		return defaultMaxPlayIndex;
	}

	public void stepForward() {
		setPlaying(false);
		setReversePlaying(false);

		int imgIndex = Math.min(getMaxPlayIndex(), getPlayIndex() + 1);
		setPlayIndex(imgIndex);
	}

	public void stepBackward() {
		setPlaying(false);
		setReversePlaying(false);

		int imgIndex = Math.max(getMinPlayIndex(), getPlayIndex() - 1);
		setPlayIndex(imgIndex);
	}

	public void playPause() {
		if (getPlaying()) {
			setPlaying(false);
			setReversePlaying(false);
		} else {
			setPlaying(true);
			setReversePlaying(false);
		}
	}

	public void revertPlayPause() {
		if (getReversePlaying()) {
			setPlaying(false);
			setReversePlaying(false);
		} else {
			setPlaying(false);
			setReversePlaying(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof Timer) {
			this.jumpToNextPlayIndex();
		}
	}

	public int getDelay() {
		return timer.getDelay();
	}

	public void setDelay(int delay) {
		timer.setDelay(delay);
	}
}
