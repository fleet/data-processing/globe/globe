/*
 * @License@
 */
package fr.ifremer.viewer3d.model;

// Start of user code for imports

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;

// End of user code

/**
 * Navigation data.
 * 
 * @author Sophie &lt;sophie.bregent@altran.com&gt;
 * @generated
 */

public class ReflectivityNavData implements IInfos {

	private List<Integer> indexList;
	private Map<Integer, Integer> indexMap;

	private List<String> sliceNameList = null;

	/**
	 * Property name for file change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_FILE = "file";

	/**
	 * Property name for lines change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_LINES = "lines";

	/**
	 * Property name for binaryFiles change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_BINARYFILES = "binaryFiles";

	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	private String imgDir;

	/**
	 * Property name for name change support.
	 * 
	 * @generated
	 */
	public static final String PROPERTY_NAME = "name";

	/**
	 * Utility instance for property change support.
	 * 
	 * @generated
	 */
	protected transient PropertyChangeSupport _pcs = new PropertyChangeSupport(this);

	/**
	 * Adds a change listener to this object.
	 * 
	 * @param listener
	 *            the listener to add
	 * @generated
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		_pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Removes a change listener from this object.
	 * 
	 * @param listener
	 *            the listener to remove
	 * @generated
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		_pcs.removePropertyChangeListener(listener);
	}

	/**
	 * The infos displayed by the popupmenu action Infos
	 */
	private AbstractInfos infos;

	public AbstractInfos getInfos() {
		return infos;
	}

	/**
	 * Default constructor.
	 * 
	 * @generated
	 */
	public ReflectivityNavData(File file) {
		this._file = file;
		_lines = new ArrayList<>();
		_binaryFiles = new ArrayList<File>();
		indexList = new ArrayList<Integer>();
		this.indexMap = new HashMap<Integer, Integer>();
		this.imgDir = SSVConfiguration.configurationDirectory();
		this.infos = new AbstractInfos();
	}

	/**
	 * For Serializable and Externalizable classes, the readResolve method
	 * allows a class to replace/resolve the object read from the stream before
	 * it is returned to the caller. By implementing the readResolve method, a
	 * class can directly control the types and instances of its own instances
	 * being deserialized.
	 * 
	 * @generated
	 */
	private Object readResolve() {
		_pcs = new PropertyChangeSupport(this);
		// Start of user code NavData readResolve

		// End of user code
		return this;
	}

	/**
	 * The file attribute.
	 * 
	 * @generated
	 */
	private File _file = null;

	/**
	 * The lines attribute.
	 * 
	 * @generated
	 */
	private List<PathWallLines> _lines;

	/**
	 * A list of bidimensionnal arrays of float (a slice) mapping the index of a
	 * navline (= index of an array in the list) to the corresponding slice
	 * 
	 */
	private List<float[][]> _mapLineReflectiviltyValues = new ArrayList<float[][]>();

	/**
	 * The binaryFiles attribute.
	 * 
	 * @generated
	 */
	private List<File> _binaryFiles;

	/**
	 * The name attribute.
	 * 
	 * @generated
	 */
	private String _name = null;

	/**
	 * The software attribute.
	 * 
	 * @generated
	 */
	private String _software = null;

	/**
	 * The file attribute.
	 * 
	 * @generated
	 */
	public File getFile() {
		return this._file;
	}

	/**
	 * * The file attribute.
	 * 
	 * @generated
	 */
	/*
	 * public void setFile(File file) { if (this._file != file) {
	 * _pcs.firePropertyChange(PROPERTY_FILE, this._file, this._file = file); }
	 * }
	 */

	/**
	 * The lines attribute.
	 * 
	 * @generated
	 */
	public List<PathWallLines> getLines() {
		return this._lines;
	}





	/**
	 * Add a lines to the lines collection at the index given by sliceNumber.
	 * 
	 * @param sliceNumber
	 *            the slice index
	 * @param linesElt
	 *            Element to add
	 * @generated
	 */
	public void addLine(int sliceNumber, PathWallLines linesElt) {
		this._lines.add(sliceNumber, linesElt);
		_pcs.firePropertyChange(PROPERTY_LINES, null, linesElt);
	}



	/**
	 * The binaryFiles attribute.
	 * 
	 * @generated
	 */
	public List<File> getBinaryFiles() {
		return this._binaryFiles;
	}

	/**
	 * * The binaryFiles attribute.
	 * 
	 * @see #addBinaryFiles
	 * @see #removeBinaryFiles
	 * @generated
	 */
	protected void setBinaryFiles(List<File> binaryFiles) {
		if (this._binaryFiles != binaryFiles) {
			_pcs.firePropertyChange(PROPERTY_BINARYFILES, this._binaryFiles, this._binaryFiles = binaryFiles);
		}
	}

	/**
	 * Add a binaryFiles to the binaryFiles collection.
	 * 
	 * @param binaryFilesElt
	 *            Element to add
	 * @generated
	 */
	public void addBinaryFile(File binaryFilesElt) {
		this._binaryFiles.add(binaryFilesElt);
		_pcs.firePropertyChange(PROPERTY_BINARYFILES, null, binaryFilesElt);
	}

	/**
	 * Remove a binaryFiles from the binaryFiles collection.
	 * 
	 * @param binaryFilesElt
	 *            Element to remove
	 * @generated
	 */
	public void removeBinaryFiles(File binaryFilesElt) {
		this._binaryFiles.remove(binaryFilesElt);
		_pcs.firePropertyChange(PROPERTY_BINARYFILES, binaryFilesElt, null);
	}

	/**
	 * The name attribute.
	 * 
	 * @generated
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @return the value _mapLineReflectiviltyValues
	 * 
	 */
	public List<float[][]> getMapLineReflectiviltyValues() {
		return _mapLineReflectiviltyValues;
	}

	/**
	 * @return the value _mapLineReflectiviltyValues
	 * 
	 */
	public void setMapLineReflectiviltyValues(List<float[][]> list) {
		_mapLineReflectiviltyValues = list;
	}

	/**
	 * * The name attribute.
	 * 
	 * @generated
	 */
	public void setName(String name) {
		if (this._name != name) {
			_pcs.firePropertyChange(PROPERTY_NAME, this._name, this._name = name);
		}
	}

	/**
	 * The software attribute.
	 */
	public String getSoftware() {
		return this._software;
	}

	/**
	 * * The software attribute.
	 */
	public void setSoftware(String software) {
		this._software = software;
	}


	public void addInfo(String key, Object value) {
		if (value != null) {
			infos.addInfo(key, value.toString());
		}
	}

	@Override
	public List<Property<?>> getProperties() {
		return infos.getProperties();
	}



	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	public String getImgDir() {
		return imgDir;
	}

	/**
	 * Name of directory containing image (png) files. Relative path, eg.
	 * "PolarEchograms".
	 */
	public void setImgDir(String directory) {
		this.imgDir = directory;
	}

	public List<Integer> getIndexList() {
		return indexList;
	}

	public int getIndex(int index) {
		return indexList.get(index);
	}

	public void addIndex(Integer index) {
		indexList.add(index);
	}

	public Map<Integer, Integer> getIndexMap() {
		return this.indexMap;
	}

	public int getIndexMap(int slice) {
		return this.indexMap.get(slice);
	}

	public void addIndexMap(int slice, int index) {
		this.indexMap.put(slice, index);
	}

	public List<String> getSliceNameList() {
		return sliceNameList;
	}

	public void setSliceNameList(List<String> sliceNameList) {
		this.sliceNameList = sliceNameList;
	}

	public String getSliceName(int sliceNumber) {
		return sliceNameList.get(sliceNumber);
	}

	public void addSliceName(String sliceName) {
		sliceNameList.add(sliceName);
	}

}