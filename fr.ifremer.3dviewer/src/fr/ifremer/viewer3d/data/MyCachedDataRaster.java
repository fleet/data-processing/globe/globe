package fr.ifremer.viewer3d.data;

import java.io.IOException;
import java.util.logging.Level;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.cache.MemoryCache;
import gov.nasa.worldwind.data.CachedDataRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.DataRasterReader;
import gov.nasa.worldwind.exception.WWRuntimeException;
import gov.nasa.worldwind.util.Logging;

public class MyCachedDataRaster extends CachedDataRaster implements IBoundedMinMax {
	private double valMin = 0.0;
	private double valMax = 0.0;

	public MyCachedDataRaster(Object source, AVList params, DataRasterReader reader, MemoryCache cache) throws IOException, IllegalArgumentException {
		super(source, params, reader, cache);
	}

	@Override
	public DataRaster[] getDataRasters() throws IOException, WWRuntimeException {
		synchronized (this.rasterRetrievalLock)
		{
			DataRaster[] rasters = (this.rasterCache != null)
					? (DataRaster[]) this.rasterCache.getObject(this.dataSource) : null;

					if (null != rasters)
						return rasters;

					// prevent an attempt to re-read rasters which failed to load
					if (this.rasterCache == null || !this.rasterCache.contains(this.dataSource))
					{
						long memoryDelta = 0L;

						try
						{
							AVList rasterParams = this.copy();

							try
							{
								long before = getTotalUsedMemory();
								rasters = this.dataReader.read(this.getDataSource(), rasterParams);
								// PATCH GLOBE - BEGIN - Added compared to super method
								if (this.dataReader instanceof IMinMax) {
									valMin = ((IMinMax) (this.dataReader)).getValMin();
									valMax = ((IMinMax) (this.dataReader)).getValMax();
								} else {
									Double elevMin = (Double) dataReader.getValue(AVKey.ELEVATION_MIN);
									Double elevMax = (Double) dataReader.getValue(AVKey.ELEVATION_MAX);
									if (elevMin != null) {
										valMin = elevMin.doubleValue();
									}
									if (elevMax != null) {
										valMax = elevMax.doubleValue();
									}
								}
								// PATCH GLOBE - END
								memoryDelta = getTotalUsedMemory() - before;
							}
							catch (OutOfMemoryError e)
							{
								Logging.logger().finest(this.composeExceptionReason(e));
								this.releaseMemory();
								// let's retry after the finalization and GC

								long before = getTotalUsedMemory();
								rasters = this.dataReader.read(this.getDataSource(), rasterParams);
								memoryDelta = getTotalUsedMemory() - before;
							}
						}
						catch (Throwable t)
						{
							disposeRasters(rasters); // cleanup in case of exception
							rasters = null;
							String message = Logging.getMessage("DataRaster.CannotRead", this.composeExceptionReason(t));
							Logging.logger().severe(message);
							throw new WWRuntimeException(message);
						}
						finally
						{
							// Add rasters to the cache, even if "rasters" is null to prevent multiple failed reads.
							if (this.rasterCache != null)
							{
								long totalBytes = getSizeInBytes(rasters);
								totalBytes = (memoryDelta > totalBytes) ? memoryDelta : totalBytes;
								if (totalBytes > 0L)
									this.rasterCache.add(this.dataSource, rasters, totalBytes);
							}
						}
					}
					// PATCH GLOBE - BEGIN - Commented compared to super method
					// if (null == rasters || rasters.length == 0)
					// {
					// String message = Logging.getMessage("generic.CannotCreateRaster", this.getDataSource());
					// Logging.logger().severe(message);
					// throw new WWRuntimeException(message);
					// }
					// PATCH GLOBE - END

					return rasters;
		}
	}

	@Override
	public void drawOnTo(DataRaster canvas) {
		// PATCH GLOBE - BEGIN - getDataRasters() can return null now
		synchronized (this.rasterUsageLock) {
			try {
				DataRaster[] rasters = this.getDataRasters();
				if (rasters != null) {
					super.drawOnTo(canvas);
				}
			} catch (IOException | WWRuntimeException t) {
				String reason = this.composeExceptionReason(t);
				Logging.logger().log(Level.SEVERE, reason, t);
			}
		}
		// PATCH GLOBE - END
	}

	@Override
	public double getValMin() {
		return valMin;
	}

	@Override
	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	@Override
	public double getValMax() {
		return valMax;
	}

	@Override
	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	@Override
	public double getValGlobaleMax() {
		if (this.dataReader instanceof IBoundedMinMax) {
			return ((IBoundedMinMax) (this.dataReader)).getValGlobaleMax();
		}
		return valMax;
	}

	@Override
	public double getValGlobaleMin() {
		if (this.dataReader instanceof IBoundedMinMax) {
			return ((IBoundedMinMax) (this.dataReader)).getValGlobaleMin();
		}
		return valMin;
	}

	@Override
	public void setValGlobaleMin(double valGlobaleMin) {
	}

	@Override
	public void setValGlobaleMax(double valGlobaleMax) {
	}
}
