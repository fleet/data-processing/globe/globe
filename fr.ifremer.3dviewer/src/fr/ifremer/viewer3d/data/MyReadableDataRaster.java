package fr.ifremer.viewer3d.data;

import java.util.logging.Level;

import com.jogamp.opengl.util.texture.TextureData;

import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.cache.MemoryCache;
import gov.nasa.worldwind.data.ByteBufferRaster;
import gov.nasa.worldwind.data.CachedDataRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.DataRasterReader;
import gov.nasa.worldwind.util.Logging;

public class MyReadableDataRaster extends CachedDataRaster implements DataRaster {
	private TextureData textureData;

	public MyReadableDataRaster(Object source, AVList params, DataRasterReader reader, MemoryCache cache) throws java.io.IOException {
		super(source, params, reader, cache);
	}

	@Override
	public void drawOnTo(DataRaster canvas) {
		synchronized (this.rasterUsageLock) {
			try {
				DataRaster[] rasters;
				try {
					rasters = this.getDataRasters();
					for (DataRaster raster : rasters) {
						raster.drawOnTo(canvas);
						// PATCH GLOBE - BEGIN - Added compared to super method
						if (raster instanceof ByteBufferRaster) {
							if (this.getDataRasterReader() instanceof MyGeotiffElevationRasterReader) {
								textureData = ((MyGeotiffElevationRasterReader) this.getDataRasterReader()).getTextureData();
							} //else if (this.getDataRasterReader() instanceof NetcdfElevationRasterReader) {
								//textureData = ((NetcdfElevationRasterReader) this.getDataRasterReader()).getTextureData();
							//}
						}
						// PATCH GLOBE - END
					}
				} catch (OutOfMemoryError e) {
					Logging.logger().finest(this.composeExceptionReason(e));
					this.releaseMemory();

					rasters = this.getDataRasters();
					for (DataRaster raster : rasters) {
						raster.drawOnTo(canvas);
					}
				}
			} catch (Throwable t) {
				String reason = this.composeExceptionReason(t);
				Logging.logger().log(Level.SEVERE, reason, t);
			}
		}
	}

	public TextureData getTextureData() {
		return textureData;
	}

}
