package fr.ifremer.viewer3d.data.reader;

import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.viewer3d.data.reader.raster.DataRasterGenerator;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.data.DataRaster;

public class NetcdfImageReader extends NetcdfReader {

	public NetcdfImageReader(String sourceFilename, String dataField)  {
		super(sourceFilename, dataField);
		if (metadata != null) {
			metadata.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.viewer3d.data.AbstractNetcdfReader#doRead()
	 */
	@Override
	public DataRaster doRead() throws FileFormatException{
		readFile();

		DataRasterGenerator dataRasterGenerator = new DataRasterGenerator(rasterBuffer);
		return dataRasterGenerator.generate(metadata);
	}

	public double getValMin() {
		return rasterAttributes.getValidMin();
	}

	public double getValMax() {
		return rasterAttributes.getValidMax();
	}

}
