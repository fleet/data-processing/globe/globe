package fr.ifremer.viewer3d.data.reader.raster;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.util.ImageUtil;

/**
 * Generate a {@link #DataRaster} from a {@link #TerrainBuffer}
 */
public class DataRasterGenerator  {

	/** Terrain data */
	protected final Raster rasterBuffer;

	/**
	 * Constructor.
	 */
	public DataRasterGenerator(Raster rasterBuffer) {
		this.rasterBuffer = rasterBuffer;
	}

	/**
	 * Generate a {@link #DataRaster} from a {@link #TerrainBuffer}
	 */
	public DataRaster generate(AVList values) {
		RasterAttributes rasterAttributes = rasterBuffer.getTerrainAttributes();
		BufferedImage rgbImage = null;

		rgbImage = new BufferedImage(rasterAttributes.getColumns(), rasterAttributes.getLines(), BufferedImage.TYPE_INT_ARGB);
		WritableRaster wrRaster = rgbImage.getRaster();

		//we need to work as float, if not a small difference can make the exact min and max values appears with a wrong color
		float elevMin = (float) rasterAttributes.getValidMin();
		float elevMax = (float) rasterAttributes.getValidMax();

		for (int y = 0; y <  rasterAttributes.getLines(); y++) {
			for (int x = 0; x < rasterAttributes.getColumns(); x++) {
				float fvalue = rasterBuffer.get(x,  y);
				if (fvalue == Raster.NO_VALUE) {
					wrRaster.setSample(x, y, 0, (0));
					wrRaster.setSample(x, y, 1, (0));
					wrRaster.setSample(x, y, 2, (0));
					wrRaster.setSample(x, y, 3, (0));
				} else {
					//threshold on min/max values
					if(fvalue < elevMin) fvalue=elevMin; 
					if(fvalue > elevMax) fvalue=elevMax;
					int value = (int) ((fvalue - elevMin) / (elevMax - elevMin) * 0xFFFFFF);// elevation
					char red = (char) ((value & 0xFF0000) >> 16); // first 8 bits
					char green = (char) ((value & 0x00FF00) >> 8); // next
					char blue = (char) ((value & 0x0000FF));
					wrRaster.setSample(x, y, 0, (red));
					wrRaster.setSample(x, y, 1, (green));
					wrRaster.setSample(x, y, 2, (blue));
					wrRaster.setSample(x, y, 3, (255));
				}
			}
		}

		values.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
		rgbImage = ImageUtil.toCompatibleImage(rgbImage);
		return BufferedImageRaster.wrap(rgbImage, values);

	}
}
