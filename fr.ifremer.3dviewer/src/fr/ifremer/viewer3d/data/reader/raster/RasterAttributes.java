package fr.ifremer.viewer3d.data.reader.raster;

import org.gdal.gdal.Dataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;


/**
 * All attributes describing a raster.
 * 
 */
public class RasterAttributes {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(RasterAttributes.class);

	/** Name of the variable containing the number of line */
	protected String linesAttributeName = "LINES";
	/** Name of the variable containing the number of column */
	protected String columnAttributeName = "COLUMNS";
	/** Name of the variable designating the layer to load by default */
	protected String layerName = "DEPTH";
	/** Name of the variable designating the projection (ESRI WKT by default) */
	protected String projectionAttributeName = "esri_pe_string";

	/** File to read. */
	protected final String filename;

	/** Line count. */
	protected int lines = 0;
	/** Column count. */
	protected int columns = 0;
	/** Size of a line. */
	protected double lineSpacing = Double.NaN;
	/** Size of a column. */
	protected double columnSpacing = Double.NaN;
	/** True to read the lines from last to first. */
	protected boolean lineReversed = false;

	/** Whenever last read was successfull. */
	protected boolean readSuccessfully = false;

	protected double missingValue = 32767;
	protected double depthOffset = 0.0;
	protected double depthScale = 1.0;
	protected GeoBox geoBox = new GeoBox();

	protected double validMin = Double.NEGATIVE_INFINITY;
	protected double validMax = Double.POSITIVE_INFINITY;

	/** Dataset extract by gdal when possible. */
	protected Dataset dataset;

	/**
	 * Constructor.
	 */
	public RasterAttributes(String filename) {
		this.filename = filename;
	}

	/**
	 * @return the {@link #lines}
	 */
	public int getLines() {
		return lines;
	}

	/**
	 * @param lines
	 *            the {@link #lines} to set
	 */
	public void setLines(int lines) {
		this.lines = lines;
	}

	/**
	 * @return the {@link #columns}
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * @param columns
	 *            the {@link #columns} to set
	 */
	public void setColumns(int columns) {
		this.columns = columns;
	}

	/**
	 * @return the {@link #readSuccessfully}
	 */
	public boolean isReadSuccessfully() {
		return readSuccessfully;
	}

	/**
	 * @param readSuccessfully
	 *            the {@link #readSuccessfully} to set
	 */
	public void setReadSuccessfully(boolean readSuccessfully) {
		this.readSuccessfully = readSuccessfully;
	}

	/**
	 * @return the {@link #geoBox}
	 */
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/**
	 * @param geoBox
	 *            the {@link #geoBox} to set
	 */
	public void setGeoBox(GeoBox geoBox) {
		this.geoBox = geoBox;
	}

	/**
	 * @return the {@link #linesAttributeName}
	 */
	public String getLinesAttributeName() {
		return linesAttributeName;
	}

	/**
	 * @param linesAttributeName
	 *            the {@link #linesAttributeName} to set
	 */
	public void setLinesAttributeName(String linesAttributeName) {
		this.linesAttributeName = linesAttributeName;
	}

	/**
	 * @return the {@link #columnAttributeName}
	 */
	public String getColumnAttributeName() {
		return columnAttributeName;
	}

	/**
	 * @param columnAttributeName
	 *            the {@link #columnAttributeName} to set
	 */
	public void setColumnAttributeName(String columnAttributeName) {
		this.columnAttributeName = columnAttributeName;
	}

	/**
	 * @return the {@link #layerName}
	 */
	public String getLayerName() {
		return layerName;
	}

	/**
	 * @param layerName
	 *            the {@link #layerName} to set
	 */
	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}

	/**
	 * @return the {@link #filename}
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @return the {@link #missingValue}
	 */
	public double getMissingValue() {
		return missingValue;
	}

	/**
	 * @param missingValue
	 *            the {@link #missingValue} to set
	 */
	public void setMissingValue(double missingValue) {
		this.missingValue = missingValue;
	}

	/**
	 * @return the {@link #depthOffset}
	 */
	public double getDepthOffset() {
		return depthOffset;
	}

	/**
	 * @param depthOffset
	 *            the {@link #depthOffset} to set
	 */
	public void setDepthOffset(double depthOffset) {
		this.depthOffset = depthOffset;
	}

	/**
	 * @return the {@link #depthScale}
	 */
	public double getDepthScale() {
		return depthScale;
	}

	/**
	 * @param depthScale
	 *            the {@link #depthScale} to set
	 */
	public void setDepthScale(double depthScale) {
		this.depthScale = depthScale;
	}

	/**
	 * @return the {@link #validMin}
	 */
	public double getValidMin() {
		return validMin;
	}

	/**
	 * @param validMin
	 *            the {@link #validMin} to set
	 */
	public void setValidMin(double validMin) {
		this.validMin = validMin;
	}

	/**
	 * @return the {@link #validMax}
	 */
	public double getValidMax() {
		return validMax;
	}

	/**
	 * @param validMax
	 *            the {@link #validMax} to set
	 */
	public void setValidMax(double validMax) {
		this.validMax = validMax;
	}

	/**
	 * @return the {@link #projectionAttributeName}
	 */
	public String getProjectionAttributeName() {
		return projectionAttributeName;
	}

	/**
	 * @param projectionAttributeName
	 *            the {@link #projectionAttributeName} to set
	 */
	public void setProjectionAttributeName(String projectionAttributeName) {
		this.projectionAttributeName = projectionAttributeName;
	}

	/**
	 * @return the {@link #lineSpacing}
	 */
	public double getLineSpacing() {
		return lineSpacing;
	}

	/**
	 * @param lineSpacing
	 *            the {@link #lineSpacing} to set
	 */
	public void setLineSpacing(double lineSpacing) {
		this.lineSpacing = lineSpacing;
	}

	/**
	 * @return the {@link #columnSpacing}
	 */
	public double getColumnSpacing() {
		return columnSpacing;
	}

	/**
	 * @param columnSpacing
	 *            the {@link #columnSpacing} to set
	 */
	public void setColumnSpacing(double columnSpacing) {
		this.columnSpacing = columnSpacing;
	}

	/**
	 * @return the {@link #lineReversed}
	 */
	public boolean isLineReversed() {
		return lineReversed;
	}

	/**
	 * @param lineReversed the {@link #lineReversed} to set
	 */
	public void setLineReversed(boolean lineReversed) {
		this.lineReversed = lineReversed;
	}

	/**
	 * @return the {@link #dataset}
	 */
	public Dataset getDataset() {
		return dataset;
	}

	/**
	 * @param dataset the {@link #dataset} to set
	 */
	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	/**
	 * Release all ressources.
	 */
	public void dispose() {
		if( dataset != null) {
			dataset.delete();
			dataset = null;
		}
	}
}
