package fr.ifremer.viewer3d.data.reader.raster;

import java.io.File;

import org.gdal.gdal.gdal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.exception.FileFormatException;

/**
 * Some raster utilities.
 * 
 */
public class Utils {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(Utils.class);

	/**
	 * Manage the error.
	 */
	public static  void throwFileFormatException(RasterAttributes rasterAttributes, Exception e) throws FileFormatException {

		File file = new File(rasterAttributes.getFilename());
		logger.warn("Error while parsing the file : " + file.getName(), e);
		throwFileFormatException(rasterAttributes, e.getMessage());
	}

	/**
	 * Manage the error.
	 */
	public static void throwFileFormatException(RasterAttributes rasterAttributes) throws FileFormatException {
		throwFileFormatException(rasterAttributes, gdal.GetLastErrorMsg());
	}

	/**
	 * Manage the error.
	 */
	public static void throwFileFormatException(RasterAttributes rasterAttributes,String errorMsg) throws FileFormatException {
		if (errorMsg != null && !errorMsg.isEmpty()) {
			throw new FileFormatException(errorMsg);
		}
		File file = new File(rasterAttributes.getFilename());
		throw new FileFormatException("Unable to parse the file " + file.getName());

	}

}
