package fr.ifremer.viewer3d.data.reader.raster;

import java.io.IOException;

import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.viewer3d.Activator;

/**
 * this array contains the depth read from the current DTM file Its dimensions
 * are input file data matrix width and height
 */
public class Raster {
	public static final float NO_VALUE = -32768.0f;

	/** All Attributes read in a file. */
	protected final RasterAttributes rasterAttributes;

	protected final IFloatArray dtm;

	protected float minValue = Float.POSITIVE_INFINITY;
	protected float maxValue = Float.NEGATIVE_INFINITY;

	/**
	 * Constructor.
	 */
	public Raster(RasterAttributes rasterAttributes) throws IOException {
		this.rasterAttributes = rasterAttributes;
		dtm = Activator.getDefault().getArrayFactory().makeFloatArray(rasterAttributes.getLines(), rasterAttributes.getColumns());
	}

	/**
	 * @return the value at the the specified cell or MISSING_VALUE if none.
	 */
	public float get(int column, int line) {
		float result = NO_VALUE;
		if (contains(column, line)) {
			line = rasterAttributes.isLineReversed() ? rasterAttributes.getLines() - line - 1 : line;
			result = dtm.getFloat(line, column);
		}
		return result;
	}

	/**
	 * @return true when this buffer contains a cell with the specified coords.
	 */
	public boolean contains(int column, int line) {
		return !(line < 0 || line >= rasterAttributes.getLines() || column < 0 || column >= rasterAttributes.getColumns());
	}

	public void fill(float fillValue) {
		dtm.fill(fillValue);
	}

	public void set(int column, int line, float value) {
		if (line < 0 || line >= rasterAttributes.getLines() || column < 0 || column >= rasterAttributes.getColumns()) {
			return;
		}
		if (NO_VALUE != value) {
			if (value < minValue) {
				minValue = value;
			}
			if (value > maxValue) {
				maxValue = value;
			}
		}

		dtm.putFloat(line, column, value);
	}

	public int getLength() {
		return (int) dtm.getElementCount();
	}

	/**
	 * @return the {@link #minValue}
	 */
	public float getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue
	 *            the {@link #minValue} to set
	 */
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the {@link #maxValue}
	 */
	public float getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue
	 *            the {@link #maxValue} to set
	 */
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @return the {@link #rasterAttributes}
	 */
	public RasterAttributes getTerrainAttributes() {
		return rasterAttributes;
	}

}