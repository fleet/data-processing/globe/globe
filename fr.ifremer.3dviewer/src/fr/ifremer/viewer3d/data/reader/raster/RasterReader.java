package fr.ifremer.viewer3d.data.reader.raster;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.utils.exception.FileFormatException;

/**
 * Base class for Netcdf TM
 * 
 */
public class RasterReader {

	/** Logger. */
	private static final Logger logger = LoggerFactory.getLogger(RasterReader.class);

	/** All Attributes read in a file. */
	protected final RasterAttributes rasterAttributes;

	/**
	 * Constructor.
	 */
	public RasterReader(RasterAttributes rasterAttributes) {
		this.rasterAttributes = rasterAttributes;
	}

	/**
	 * Parse the file and fill a TerrainBuffer
	 */
	public Raster readFile() throws FileFormatException {
		rasterAttributes.setReadSuccessfully(false);
		Raster result = null;
		try {
			result = new Raster(rasterAttributes);
			if (rasterAttributes.getDataset() != null) {
				readByGdal(result);
			}
		} catch (Exception e) {
			Utils.throwFileFormatException(rasterAttributes, e);
		} finally {
			rasterAttributes.dispose();
		}
		return result;
	}

	/**
	 * Parse a Netcdf file.
	 */
	protected void readByGdal(Raster raster) throws Exception {
		Dataset dataset = rasterAttributes.getDataset();
		if (dataset != null && dataset.GetRasterCount() > 0) {
			Band band = dataset.GetRasterBand(1);
			GdalUtils.readRaster(band, (line, column, value) -> {
				if (Double.isNaN(value) || value == rasterAttributes.getMissingValue()) {
					raster.set(column, line, Raster.NO_VALUE);
				} else {
					raster.set(column, line, (float) value);
				}
			});
		}
	}

	/**
	 * @return the {@link #rasterAttributes}
	 */
	public RasterAttributes getRasterAttributes() {
		return rasterAttributes;
	}
}
