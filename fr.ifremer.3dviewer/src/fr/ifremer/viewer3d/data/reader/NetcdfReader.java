package fr.ifremer.viewer3d.data.reader;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.viewer3d.data.AbstractNetcdfReader;

/**
 * Reader class for CARAIBES DTM
 */
@SuppressWarnings("deprecation")
public abstract class NetcdfReader extends AbstractNetcdfReader {

	public static final String DEFAULT_FIELD = "DEPTH";

	public NetcdfReader(String path, String dataField) {
		super(path);
		rasterAttributes.setLayerName(dataField != null ? dataField : DEFAULT_FIELD);
		List<String> variablesList = getAvailableData(path);

		// find if datafield match
		if (!variablesList.contains(dataField)) {
			// we does not find default value, first check if a value exists
			// with the same name but a different case. This is typicall of old
			// mnt files
			for (String variable : variablesList) {
				if (variable.compareToIgnoreCase(dataField) == 0) {
					// typicall DEPTH vs Depth
					rasterAttributes.setLayerName(variable);
					break;
				}
			}
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.viewer3d.data.AbstractNetcdfReader#loadRasterAttributes()
	 */
	@Override
	protected void loadRasterAttributes() throws FileFormatException {
		// MNTRasterAttributesFiller terrainAttributesFiller = new MNTRasterAttributesFiller(rasterAttributes);
		// terrainAttributesFiller.fill();
	}

	private static final String DIM_LINES = "LINES";
	private static final String DIM_COLUMNS = "COLUMNS";

	/**
	 * return the dimension name of lines
	 */
	static String getDimLineName() {
		return DIM_LINES;
	}

	/**
	 * return the dimension name of lines
	 */
	static String getDimColumnName() {
		return DIM_COLUMNS;
	};

	/**
	 * Return the available data of this netcdf file. An available data has as dimension a line and a column.
	 * 
	 * @param filename The file name of the netcdf file
	 * @return The available data
	 */
	public static List<String> getAvailableData(String filename) {

		List<String> availableData = new ArrayList<String>();
		// try (var ncfile = NetcdfFile.open(filename, Mode.readonly)) {
		// for (var v : ncfile.getVariables()) {
		// for (var d : v.geta) {
		// if (d.getName().equals(getDimLineName())) {
		// for (Dimension dd : v.getDimensions()) {
		// if (dd.getName().equals(getDimColumnName())) {
		// availableData.add(v.getFullName());
		//
		// }
		// }
		// }
		// }
		// }
		// }
		return availableData;
	}

	/**
	 * Return true if the file is a netcdf, return false else.
	 * 
	 * @param filename
	 * @return true if the file is a netcdf, return false else
	 */
	public static boolean isNetcdf(String filename) {
		try (var ncfile = NetcdfFile.open(filename, Mode.readonly)) {
			var ld = ncfile.getDimension(DIM_LINES);
			var cd = ncfile.getDimension(DIM_COLUMNS);
			if (ld == null || cd == null)
				return false;
		} catch (NCException e) {
			return false;
		}
		return true;
	}

}
