package fr.ifremer.viewer3d.data.reader;

import gov.nasa.worldwind.avlist.AVKey;

public class NetcdfElevationReader extends NetcdfReader {

	public NetcdfElevationReader(String sourceFilename)  {
		super(sourceFilename,DEFAULT_FIELD);
		if (metadata != null) {
			metadata.setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
		}
	}
}
