package fr.ifremer.viewer3d.data;

/**
 * Denotes a class having values ranging from $min to $max values. The min/max values are bounded by global min/max values.
 *
 * @author apside
 */
public interface IBoundedMinMax extends IMinMax {

	/**
	 * The lower bound of all values. Global lower bound is lesser than or equal to {@link #getValMin()} and {@link #getValMax()}.
	 * 
	 * @return The min bound of all values.
	 */
	double getValGlobaleMin();

	/**
	 * Sets the global lower bound of all values. Global lower bound is lesser than or equal to {@link #getValMin()} and {@link #getValMax()}.
	 * 
	 * @param valGlobaleMin
	 *            The min bound of all values.
	 */
	void setValGlobaleMin(double valGlobaleMin);

	/**
	 * The upper bound of all values. Global upper bound is greater than or equal to {@link #getValMin()} and {@link #getValMax()}.
	 * 
	 * @return The max bound of all values.
	 */
	double getValGlobaleMax();

	/**
	 * Sets the global upper bound of all values. Global upper bound is greater than or equal to {@link #getValMin()} and {@link #getValMax()}.
	 * 
	 * @param valGlobaleMax
	 *            The max bound of all values.
	 */
	void setValGlobaleMax(double valGlobaleMax);

}
