/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.data;

/**
 * Type of data in layer and file
 */
public enum DataType {
	elevation, texture, uncertainty
}
