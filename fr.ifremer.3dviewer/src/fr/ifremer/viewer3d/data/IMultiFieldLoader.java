package fr.ifremer.viewer3d.data;
/**
 * class having a multi layer selection capability
 * The displayed layer is one among a set of layer
 * */
public interface IMultiFieldLoader {

	/**
	 * set the name of the displayed layer to be loaded
	 * */
	void setDataField(String dataField);

}
