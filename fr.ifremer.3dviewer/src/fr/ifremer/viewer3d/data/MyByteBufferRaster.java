package fr.ifremer.viewer3d.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.Calendar;

import fr.ifremer.globe.utils.cache.TemporaryCache;
import gov.nasa.worldwind.Version;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.BufferWrapperRaster;
import gov.nasa.worldwind.data.ByteBufferRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.formats.tiff.GeoTiff;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.BufferWrapper;
import gov.nasa.worldwind.util.Logging;

public class MyByteBufferRaster extends BufferWrapperRaster {

	// if data is not needed we save it in cache
	// retrieve it on getBuffer()
	private File cacheFile = null;

	/***
	 * previous image
	 */
	private static BufferWrapper img;
	/***
	 * previous image path
	 */
	private static File previousFile = null;
	/***
	 * second previous image path
	 */
	private static BufferWrapper img2;
	/***
	 * second previous image path
	 */
	private static File previousFile2 = null;

	AVList list = null;

	public MyByteBufferRaster(int width, int height, Sector sector, AVList params) {
		super(width, height, sector, BufferWrapper.wrap(createCompatibleBuffer(width, height, params), params), params);
		list = params;
		this.validateParameters(list);
	}

	private void validateParameters(AVList list) throws IllegalArgumentException {
		this.doValidateParameters(list);
	}

	protected void doValidateParameters(AVList list) throws IllegalArgumentException {
	}

	public static DataRaster createGeoreferencedRaster(AVList params) {
		if (null == params) {
			String msg = Logging.getMessage("nullValue.AVListIsNull");
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		if (!params.hasKey(AVKey.WIDTH)) {
			String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.WIDTH);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		int width = (Integer) params.getValue(AVKey.WIDTH);

		if (!(width > 0)) {
			String msg = Logging.getMessage("generic.InvalidWidth", width);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		if (!params.hasKey(AVKey.HEIGHT)) {
			String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.HEIGHT);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		int height = (Integer) params.getValue(AVKey.HEIGHT);

		if (!(height > 0)) {
			String msg = Logging.getMessage("generic.InvalidWidth", height);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		if (!params.hasKey(AVKey.SECTOR)) {
			String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.SECTOR);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		if (null == sector) {
			String msg = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (!params.hasKey(AVKey.COORDINATE_SYSTEM)) {
			// assume Geodetic Coordinate System
			params.setValue(AVKey.COORDINATE_SYSTEM, AVKey.COORDINATE_SYSTEM_GEOGRAPHIC);
		}

		String cs = params.getStringValue(AVKey.COORDINATE_SYSTEM);
		if (!params.hasKey(AVKey.PROJECTION_EPSG_CODE)) {
			if (AVKey.COORDINATE_SYSTEM_GEOGRAPHIC.equals(cs)) {
				// assume WGS84
				params.setValue(AVKey.PROJECTION_EPSG_CODE, GeoTiff.GCS.WGS_84);
			} else {
				String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.PROJECTION_EPSG_CODE);
				Logging.logger().finest(msg);
				throw new IllegalArgumentException(msg);
			}
		}

		// if PIXEL_WIDTH is specified, we are not overriding it because UTM
		// images
		// will have different pixel size
		if (!params.hasKey(AVKey.PIXEL_WIDTH)) {
			if (AVKey.COORDINATE_SYSTEM_GEOGRAPHIC.equals(cs)) {
				double pixelWidth = sector.getDeltaLonDegrees() / width;
				params.setValue(AVKey.PIXEL_WIDTH, pixelWidth);
			} else {
				String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.PIXEL_WIDTH);
				Logging.logger().finest(msg);
				throw new IllegalArgumentException(msg);
			}
		}

		// if PIXEL_HEIGHT is specified, we are not overriding it
		// because UTM images will have different pixel size
		if (!params.hasKey(AVKey.PIXEL_HEIGHT)) {
			if (AVKey.COORDINATE_SYSTEM_GEOGRAPHIC.equals(cs)) {
				double pixelHeight = sector.getDeltaLatDegrees() / height;
				params.setValue(AVKey.PIXEL_HEIGHT, pixelHeight);
			} else {
				String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.PIXEL_HEIGHT);
				Logging.logger().finest(msg);
				throw new IllegalArgumentException(msg);
			}
		}

		if (!params.hasKey(AVKey.PIXEL_FORMAT)) {
			String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.PIXEL_FORMAT);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		} else {
			String pixelFormat = params.getStringValue(AVKey.PIXEL_FORMAT);
			if (!AVKey.ELEVATION.equals(pixelFormat) && !AVKey.IMAGE.equals(pixelFormat)) {
				String msg = Logging.getMessage("generic.UnknownValueForKey", pixelFormat, AVKey.PIXEL_FORMAT);
				Logging.logger().severe(msg);
				throw new IllegalArgumentException(msg);
			}
		}

		if (!params.hasKey(AVKey.DATA_TYPE)) {
			String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.DATA_TYPE);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		// validate elevation parameters
		if (AVKey.ELEVATION.equals(params.getValue(AVKey.PIXEL_FORMAT))) {
			String type = params.getStringValue(AVKey.DATA_TYPE);
			if (!AVKey.FLOAT32.equals(type) && !AVKey.INT16.equals(type)) {
				String msg = Logging.getMessage("generic.UnknownValueForKey", type, AVKey.DATA_TYPE);
				Logging.logger().severe(msg);
				throw new IllegalArgumentException(msg);
			}
		}

		if (!params.hasKey(AVKey.ORIGIN) && AVKey.COORDINATE_SYSTEM_GEOGRAPHIC.equals(cs)) {
			// set UpperLeft corner as the origin, if not specified
			LatLon origin = new LatLon(sector.getMaxLatitude(), sector.getMinLongitude());
			params.setValue(AVKey.ORIGIN, origin);
		}

		if (!params.hasKey(AVKey.BYTE_ORDER)) {
			String msg = Logging.getMessage("generic.MissingRequiredParameter", AVKey.BYTE_ORDER);
			Logging.logger().finest(msg);
			throw new IllegalArgumentException(msg);
		}

		if (!params.hasKey(AVKey.DATE_TIME)) {
			// add NUL (\0) termination as required by TIFF v6 spec (20 bytes
			// length)
			String timestamp = String.format("%1$tY:%1$tm:%1$td %tT\0", Calendar.getInstance());
			params.setValue(AVKey.DATE_TIME, timestamp);
		}

		if (!params.hasKey(AVKey.VERSION)) {
			params.setValue(AVKey.VERSION, Version.getVersion());
		}

		return new ByteBufferRaster(width, height, sector, params);
	}

	@Override
	public long getSizeInBytes() {
		// PATCH GLOBE - estimate with the size of the cache file
		if (cacheFile != null) { 
			return cacheFile.length();
		} else {
			return super.getSizeInBytes();
		}
	}

	@Override
	public void dispose() {
		buffer = null;
		if (cacheFile != null) {
			cacheFile.delete();
		}
	}

	/***
	 * write buffer as cache file
	 * 
	 * @param data
	 * @param width
	 * @param height
	 */
	public void write(float[][] data) {
		// no data?
		if (data == null || data.length == 0) {
			return;
		}

		dispose();

		try {
			cacheFile = TemporaryCache.createTemporaryFile("elevationFile", ".txt");
			cacheFile.deleteOnExit();
			FileOutputStream raf = new FileOutputStream(cacheFile.getAbsolutePath());
			ObjectOutputStream stream = new ObjectOutputStream(raf);

			stream.writeObject(data);
			raf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ByteBuffer getByteBuffer() {
		BufferWrapper w = getBuffer();
		if (w != null) {
			ByteBuffer b = (ByteBuffer) w.getBackingBuffer();
			return b;
		}
		return null;
	}

	@Override
	public BufferWrapper getBuffer() {
		if (buffer != null) {
			return this.buffer;
		} else if (cacheFile != null) {
			// if file already loaded
			if (img != null && cacheFile.equals(previousFile)) {
				return img;
			}

			if (img2 != null && cacheFile.equals(previousFile2)) {
				return img2;
			}

			// else
			// load file
			if (previousFile != null && img != null) {
				previousFile2 = previousFile;
				img2 = img;
			}
			previousFile = cacheFile;
			img = load(cacheFile);
			return img;// ImageIO.read(cacheFile);
		}
		return null;
	}

	private BufferWrapper load(File cache) {
		if (cache == null) {
			return null;
		}

		try {

			FileInputStream raf = new FileInputStream(cacheFile.getAbsolutePath());
			ObjectInputStream stream = new ObjectInputStream(raf);
			float[][] data = (float[][]) stream.readObject();
			ByteBuffer buff = createCompatibleBuffer(width, height, list);
			BufferWrapper b = BufferWrapper.wrap(buff, list);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					try {
						b.putDouble(indexFor(x, y), data[0][y * width + x]);
					} catch (Exception e) {
						System.out.print("Could  not cache 1 for Elevation");
					}
				}
			}

			raf.close();
			return b;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	public static java.nio.ByteBuffer createCompatibleBuffer(int width, int height, AVList params) {
		if (width < 1) {
			String message = Logging.getMessage("generic.ArgumentOutOfRange", "width < 1");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}
		if (height < 1) {
			String message = Logging.getMessage("generic.ArgumentOutOfRange", "height < 1");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}
		if (params == null) {
			String message = Logging.getMessage("nullValue.ParamsIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		//Object dataType = params.getValue(AVKey.DATA_TYPE);

		int sizeOfDataType = (Float.SIZE / 8);

		int sizeInBytes = sizeOfDataType * width * height;
		return java.nio.ByteBuffer.allocate(sizeInBytes);
	}

	@Override
	protected void doDrawOnTo(BufferWrapperRaster canvas) {
		try {
			super.doDrawOnTo(canvas);
		} catch (Exception e) {
			System.out.print("problme generation bil");
		} catch (Throwable t) {
			System.out.print("problme generation bil");
		}
	}

}