package fr.ifremer.viewer3d.data;

import java.awt.Point;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BandedSampleModel;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.nio.FloatBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLProfile;

import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.viewer3d.formats.tiff.BaselineTiff;
import fr.ifremer.viewer3d.formats.tiff.GeotiffReader;
import fr.ifremer.viewer3d.formats.tiff.Tiff;
import fr.ifremer.viewer3d.formats.tiff.TiffIFDEntry;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.data.ByteBufferRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ElevationsUtil;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;

public class MyGeotiffElevationReader extends GeotiffReader {

	protected TextureData textureData;

	public MyGeotiffElevationReader(String sourceFilename) throws IOException {
		super(sourceFilename);
	}

	@Override
	public DataRaster doRead(int imageIndex) throws IOException {
		checkImageIndex(imageIndex);
		AVList values = this.metadata.get(imageIndex);

		// Extract the various IFD tags we need to read this image. We want to
		// loop over the tag set once, instead
		// multiple times if we simply used our general getByTag() method.

		long[] stripOffsets = null;
		byte[][] cmap = null;
		long[] stripCounts = null;

		boolean tiffDifferencing = false;

		TiffIFDEntry[] ifd = this.tiffIFDs.get(imageIndex);

		BaselineTiff tiff = BaselineTiff.extract(ifd, this.tiffReader);

		if (null == tiff) {
			String message = Logging.getMessage("GeotiffReader.BadGeotiff");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		if (tiff.width <= 0) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.width, "width", Tiff.Tag.IMAGE_WIDTH);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.height <= 0) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.height, "height", Tiff.Tag.IMAGE_LENGTH);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.samplesPerPixel <= Tiff.Undefined) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.samplesPerPixel, "samplesPerPixel", Tiff.Tag.SAMPLES_PER_PIXEL);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.photometric <= Tiff.Photometric.Undefined || tiff.photometric > Tiff.Photometric.YCbCr) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.photometric, "PhotoInterpretation", Tiff.Tag.PHOTO_INTERPRETATION);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.rowsPerStrip <= Tiff.Undefined) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.rowsPerStrip, "RowsPerStrip", Tiff.Tag.ROWS_PER_STRIP);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		if (tiff.planarConfig != Tiff.PlanarConfiguration.PLANAR && tiff.planarConfig != Tiff.PlanarConfiguration.CHUNKY) {
			String msg = Logging.getMessage("GeotiffReader.InvalidIFDEntryValue", tiff.planarConfig, "PhotoInterpretation", Tiff.Tag.PHOTO_INTERPRETATION);
			Logging.logger().severe(msg);
			throw new IOException(msg);
		}

		for (TiffIFDEntry entry : ifd) {
			try {
				switch (entry.tag) {
				case Tiff.Tag.STRIP_OFFSETS:
					stripOffsets = entry.getAsLongs();
					break;

				case Tiff.Tag.STRIP_BYTE_COUNTS:
					stripCounts = entry.getAsLongs();
					break;

				case Tiff.Tag.COLORMAP:
					cmap = this.tiffReader.readColorMap(entry);
					break;
				}
			} catch (IOException e) {
				Logging.logger().finest(e.toString());
			}
		}

		if (null == stripOffsets || 0 == stripOffsets.length) {
			String message = Logging.getMessage("GeotiffReader.MissingRequiredTag", "StripOffsets");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		if (null == stripCounts || 0 == stripCounts.length) {
			String message = Logging.getMessage("GeotiffReader.MissingRequiredTag", "StripCounts");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		TiffIFDEntry notToday = getByTag(ifd, Tiff.Tag.COMPRESSION);
		boolean lzwCompressed = false;
		if (notToday != null && notToday.asLong() == Tiff.Compression.LZW) {
			lzwCompressed = true;
			TiffIFDEntry predictorEntry = getByTag(ifd, Tiff.Tag.TIFF_PREDICTOR);
			if ((predictorEntry != null) && (predictorEntry.asLong() != 0)) {
				tiffDifferencing = true;
			}
		} else if (notToday != null && notToday.asLong() != Tiff.Compression.NONE) {
			String message = Logging.getMessage("GeotiffReader.CompressionFormatNotSupported");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		notToday = getByTag(ifd, Tiff.Tag.TILE_WIDTH);
		if (notToday != null) {
			String message = Logging.getMessage("GeotiffReader.NoTiled");
			Logging.logger().severe(message);
			throw new IOException(message);
		}

		long offset = stripOffsets[0];
		// int sampleFormat = (null != tiff.sampleFormat) ? tiff.sampleFormat[0]
		// : Tiff.Undefined;
		// int bitsPerSample = (null != tiff.bitsPerSample) ?
		// tiff.bitsPerSample[0] : Tiff.Undefined;

		if (values.getValue(AVKey.PIXEL_FORMAT) == AVKey.ELEVATION) {
			ByteBufferRaster raster = new ByteBufferRaster(tiff.width, tiff.height, (Sector) values.getValue(AVKey.SECTOR), values);

			double minSampleValue = Double.MIN_VALUE;
			double maxSampleValue = Double.MAX_VALUE;
			double nodataSignal = Short.MIN_VALUE;

			if (raster.hasKey(AVKey.ELEVATION_MIN) && raster.hasKey(AVKey.ELEVATION_MAX)) {
				minSampleValue = (Double) raster.getValue(AVKey.ELEVATION_MIN);
				maxSampleValue = (Double) raster.getValue(AVKey.ELEVATION_MAX);

				if (raster.hasKey(AVKey.MISSING_DATA_SIGNAL)) {
					nodataSignal = (Double) raster.getValue(AVKey.MISSING_DATA_SIGNAL);
				} else {
					raster.setValue(AVKey.MISSING_DATA_SIGNAL, nodataSignal);
				}
			}

			if (raster.getValue(AVKey.DATA_TYPE) == AVKey.INT8) {
				byte[][] data = this.tiffReader.readPlanar8(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

				int next = 0;
				for (int y = 0; y < tiff.height; y++) {
					for (int x = 0; x < tiff.width; x++) {
						raster.setDoubleAtPosition(y, x, data[0][next++]);
					}
				}
			} else if (raster.getValue(AVKey.DATA_TYPE) == AVKey.INT16) {
				short[][] data = this.tiffReader.readPlanar16(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

				int next = 0;
				for (int y = 0; y < tiff.height; y++) {
					for (int x = 0; x < tiff.width; x++) {
						double value = data[0][next++];
						value = (value > maxSampleValue || value < minSampleValue) ? nodataSignal : value;
						raster.setDoubleAtPosition(y, x, value);
					}
				}

				short[] image = this.tiffReader.read16bitPixelInterleavedImage(imageIndex, tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

				BufferedImage grayImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_USHORT_GRAY);
				WritableRaster wrRaster = grayImage.getRaster();

				next = 0;
				for (int y = 0; y < tiff.height; y++) {
					for (int x = 0; x < tiff.width; x++) {
						wrRaster.setSample(x, y, 0, 0xFFFF & (image[next++]));
					}
				}

				GLProfile glprofile = GLProfile.getDefault();
				textureData = AWTTextureIO.newTextureData(glprofile, grayImage, true);
			} else if (raster.getValue(AVKey.DATA_TYPE) == AVKey.FLOAT32) {
				float[][] data = this.tiffReader.readPlanarFloat32(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

				int next = 0;
				FloatBuffer buff = FloatBuffer.allocate(tiff.width * tiff.height);
				for (int y = 0; y < tiff.height; y++) {
					for (int x = 0; x < tiff.width; x++) {
						double value = data[0][next++];
						// value = (value > maxSampleValue || value <
						// minSampleValue) ? nodataSignal : value;
						// value = 0.0;
						raster.setDoubleAtPosition(y, x, value);
						buff.put((float) value);
					}
				}

				GLProfile glprofile = GLProfile.getDefault();
				textureData = new TextureData(glprofile, GL2.GL_LUMINANCE32F, tiff.width, tiff.height, 0, GL.GL_LUMINANCE, GL.GL_FLOAT, false, false, false, buff.rewind(), null);
			} else {
				String message = Logging.getMessage("Geotiff.UnsupportedDataTypeRaster", tiff.toString());
				Logging.logger().severe(message);
				throw new IOException(message);
			}

			ElevationsUtil.rectify(raster);

			return raster;
		} else if (values.getValue(AVKey.PIXEL_FORMAT) == AVKey.IMAGE && values.getValue(AVKey.IMAGE_COLOR_FORMAT) == AVKey.GRAYSCALE) {
			BufferedImage grayImage = null;

			if (values.getValue(AVKey.DATA_TYPE) == AVKey.INT8) {
				byte[][] image = this.tiffReader.readPlanar8(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

				grayImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_BYTE_GRAY);
				WritableRaster wrRaster = grayImage.getRaster();

				int next = 0;
				for (int y = 0; y < tiff.height; y++) {
					for (int x = 0; x < tiff.width; x++) {
						wrRaster.setSample(x, y, 0, 0xFF & (image[0][next++]));
					}
				}
			} else if (values.getValue(AVKey.DATA_TYPE) == AVKey.INT16 && tiff.samplesPerPixel == 1) {
				short[] image = this.tiffReader.read16bitPixelInterleavedImage(imageIndex, tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

				BufferedImage buffImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_USHORT_GRAY);
				grayImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_USHORT_GRAY);
				ColorSpace Gray = ColorSpace.getInstance(ColorSpace.CS_GRAY);
				WritableRaster wrRaster = buffImage.getRaster();

				int next = 0;
				for (int y = 0; y < tiff.height; y++) {
					for (int x = 0; x < tiff.width; x++) {
						wrRaster.setSample(x, y, 0, 0xFFFF & 2 * image[next++]);
					}
				}
				ColorConvertOp theOp = new ColorConvertOp(Gray, null);
				theOp.filter(buffImage, grayImage);
			} else if (values.getValue(AVKey.DATA_TYPE) == AVKey.INT16 && tiff.samplesPerPixel > 1) {
				short[] image = this.tiffReader.read16bitPixelInterleavedImage(imageIndex, tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);

				grayImage = new BufferedImage(tiff.width, tiff.height, BufferedImage.TYPE_USHORT_GRAY);
				WritableRaster wrRaster = grayImage.getRaster();

				int next = 0;
				for (int y = 0; y < tiff.height; y++) {
					for (int x = 0; x < tiff.width; x++) {
						wrRaster.setSample(x, y, 0, image[next++]);
					}
				}
			}

			if (null == grayImage) {
				String message = Logging.getMessage("Geotiff.UnsupportedDataTypeRaster", tiff.toString());
				Logging.logger().severe(message);
				throw new IOException(message);
			}

			grayImage = ImageUtil.toCompatibleImage(grayImage);
			return BufferedImageRaster.wrap(grayImage, values);
		} else if (values.getValue(AVKey.PIXEL_FORMAT) == AVKey.IMAGE && values.getValue(AVKey.IMAGE_COLOR_FORMAT) == AVKey.COLOR) {

			ColorModel colorModel = null;
			WritableRaster raster;
			BufferedImage colorImage;

			// make sure a DataBufferByte is going to do the trick
			for (int bits : tiff.bitsPerSample) {
				if (bits != 8) {
					String message = Logging.getMessage("GeotiffReader.Not8bit", bits);
					Logging.logger().warning(message);
					throw new IOException(message);
				}
			}

			if (tiff.photometric == Tiff.Photometric.Color_RGB) {
				int transparency = Transparency.OPAQUE;
				boolean hasAlpha = false;

				if (tiff.samplesPerPixel == Tiff.SamplesPerPixel.RGB) {
					transparency = Transparency.OPAQUE;
					hasAlpha = false;
				} else if (tiff.samplesPerPixel == Tiff.SamplesPerPixel.RGBA) {
					transparency = Transparency.TRANSLUCENT;
					hasAlpha = true;
				}
				colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), tiff.bitsPerSample, hasAlpha, false, transparency, DataBuffer.TYPE_BYTE);
			} else if (tiff.photometric == Tiff.Photometric.Color_Palette) {
				colorModel = new IndexColorModel(tiff.bitsPerSample[0], cmap[0].length, cmap[0], cmap[1], cmap[2]);
			} else if (tiff.photometric == Tiff.Photometric.CMYK) {
				// colorModel = new
				// ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_),
				// tiff.bitsPerSample,
				// false, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
			}

			int[] bankOffsets = new int[tiff.samplesPerPixel];
			for (int i = 0; i < tiff.samplesPerPixel; i++) {
				bankOffsets[i] = i;
			}
			int[] offsets = new int[(tiff.planarConfig == Tiff.PlanarConfiguration.CHUNKY) ? 1 : tiff.samplesPerPixel];
			for (int i = 0; i < offsets.length; i++) {
				offsets[i] = 0;
			}

			// construct the right SampleModel...
			ComponentSampleModel sampleModel;

			if (tiff.samplesPerPixel == Tiff.SamplesPerPixel.MONOCHROME) {
				sampleModel = new ComponentSampleModel(DataBuffer.TYPE_BYTE, tiff.width, tiff.height, 1, tiff.width, bankOffsets);
			} else {
				sampleModel = (tiff.planarConfig == Tiff.PlanarConfiguration.CHUNKY) ? new PixelInterleavedSampleModel(DataBuffer.TYPE_BYTE, tiff.width, tiff.height, tiff.samplesPerPixel, tiff.width
						* tiff.samplesPerPixel, bankOffsets) : new BandedSampleModel(DataBuffer.TYPE_BYTE, tiff.width, tiff.height, tiff.width, bankOffsets, offsets);
			}

			// Get the image data and make our Raster...
			byte[][] imageData;
			if (tiff.planarConfig == Tiff.PlanarConfiguration.CHUNKY) {
				if (lzwCompressed && (tiff.samplesPerPixel > 2)) {
					imageData = new byte[1][tiff.width * tiff.height * tiff.samplesPerPixel];

					imageData[0] = this.tiffReader.readLZWCompressed(tiff.width, tiff.height, offset, tiff.samplesPerPixel, tiffDifferencing, stripOffsets, stripCounts);
				} else {
					imageData = this.tiffReader.readPixelInterleaved8(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts);
				}
			} else {
				imageData = this.tiffReader.readPlanar8(tiff.width, tiff.height, tiff.samplesPerPixel, stripOffsets, stripCounts, tiff.rowsPerStrip);
			}

			DataBufferByte dataBuff = new DataBufferByte(imageData, tiff.width * tiff.height, offsets);
			raster = Raster.createWritableRaster(sampleModel, dataBuff, new Point(0, 0));

			colorImage = new BufferedImage(colorModel, raster, false, null);

			colorImage = ImageUtil.toCompatibleImage(colorImage);
			return BufferedImageRaster.wrap(colorImage, values);
		}

		String message = Logging.getMessage("Geotiff.UnsupportedDataTypeRaster", tiff.toString());
		Logging.logger().severe(message);
		throw new IOException(message);
	}

	public void setTextureData(TextureData textureData) {
		this.textureData = textureData;
	}

	public TextureData getTextureData() {
		return textureData;
	}
}
