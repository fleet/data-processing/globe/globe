/**
 * 
 */
package fr.ifremer.viewer3d.data;

/**
 * @author vlegendr All class which implement this interface can be read as a
 *         discreet field of value.
 */
public interface AdaptDMatrix {

	public float get(int p_i, int p_j);

	public float getMissingValue();

	public float getMinValue();

	public float getMaxValue();

	public int getColumns();

	public int getLines();

}
