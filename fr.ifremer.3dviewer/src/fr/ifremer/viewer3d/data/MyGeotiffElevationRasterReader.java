package fr.ifremer.viewer3d.data;

import com.jogamp.opengl.util.texture.TextureData;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.GeotiffRasterReader;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;

public class MyGeotiffElevationRasterReader extends GeotiffRasterReader {

	private TextureData textureData;

	public MyGeotiffElevationRasterReader() {
		super();
	}

	@Override
	protected void doReadMetadata(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("nullValue.PathIsNull", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		// PATCH GLOBE - Reader specific : MyGeotiffElevationReader instead of GeotiffReader
		// GeotiffReader reader = null;
		MyGeotiffElevationReader reader = null;

		try {
			reader = new MyGeotiffElevationReader(path);
			reader.copyMetadataTo(params);

			boolean isGeoTiff = reader.isGeotiff(0);
			if (!isGeoTiff && params.hasKey(AVKey.WIDTH) && params.hasKey(AVKey.HEIGHT)) {
				int[] size = new int[2];

				size[0] = (Integer) params.getValue(AVKey.WIDTH);
				size[1] = (Integer) params.getValue(AVKey.HEIGHT);

				params.setValue(WorldFile.WORLD_FILE_IMAGE_SIZE, size);

				WorldFile.readWorldFiles(source, params);

				Object o = params.getValue(AVKey.SECTOR);
				if (o == null || !(o instanceof Sector)) {
					ImageUtil.calcBoundingBoxForUTM(params);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("DataRaster.CannotRead", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		AVList metadata = new AVListImpl();
		if (null != params) {
			metadata.setValues(params);
		}

		// PATCH GLOBE - Reader specific : MyGeotiffElevationReader instead of GeotiffReader
		// GeotiffReader reader = null;
		MyGeotiffElevationReader reader = null;

		DataRaster[] rasters = null;
		try {
			this.readMetadata(source, metadata);

			reader = new MyGeotiffElevationReader(path);
			reader.copyMetadataTo(metadata);

			rasters = reader.readDataRaster();
			textureData = reader.getTextureData();

			if (null != rasters) {
				String[] keysToCopy = new String[] { AVKey.SECTOR };
				for (DataRaster raster : rasters) {
					WWUtil.copyValues(metadata, raster, keysToCopy, false);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return rasters;
	}

	public void setTextureData(TextureData textureData) {
		this.textureData = textureData;
	}

	public TextureData getTextureData() {
		return textureData;
	}

	// /**
	// * Convert the buffered image to a texture
	// *
	// * @param bufferedImage
	// * The image to convert to a texture
	// * @param texture
	// * The texture to store the data into
	// * @return A buffer containing the data
	// */
	// private ByteBuffer convertImageData(BufferedImage bufferedImage) {
	// ByteBuffer imageBuffer = null;
	//
	// if (bufferedImage.getData() != null) {
	// Raster raster = bufferedImage.getData();
	// if (raster.getDataBuffer() != null) {
	// DataBuffer buffer = raster.getDataBuffer();
	// int[] data = ((DataBufferInt) buffer).getData();
	// imageBuffer = ByteBuffer.allocateDirect(4 * data.length);
	// imageBuffer.order(ByteOrder.nativeOrder());
	// for (int i = 0; i < data.length; i++) {
	// imageBuffer.putInt(data[i]);
	// }
	// imageBuffer.flip();
	// }
	// }
	//
	// return imageBuffer;
	// }

}
