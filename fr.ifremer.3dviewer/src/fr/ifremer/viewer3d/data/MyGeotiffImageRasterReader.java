package fr.ifremer.viewer3d.data;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.GeotiffRasterReader;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;

public class MyGeotiffImageRasterReader extends GeotiffRasterReader implements IBoundedMinMax {

	private double valGlobaleMin = -20000;
	private double valGlobaleMax = 20000;
	private double valMin = 0;
	private double valMax = 0;

	public MyGeotiffImageRasterReader() {
		super();
	}

	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("DataRaster.CannotRead", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		AVList metadata = new AVListImpl();
		if (null != params)
			metadata.setValues(params);

		// PATCH GLOBE - Reader specific : MyGeotiffImageReader instead of GeotiffReader
		// GeotiffReader reader = null;
		MyGeotiffImageReader reader = null;

		DataRaster[] rasters = null;
		try {
			this.readMetadata(source, metadata);

			reader = new MyGeotiffImageReader(path, valGlobaleMin, valGlobaleMax);
			reader.copyMetadataTo(metadata);

			rasters = reader.readDataRaster();
			valMin = reader.getValMin();
			valMax = reader.getValMax();

			if (null != rasters) {
				String[] keysToCopy = new String[] { AVKey.SECTOR };
				for (DataRaster raster : rasters) {
					WWUtil.copyValues(metadata, raster, keysToCopy, false);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return rasters;
	}

	@Override
	protected void doReadMetadata(Object source, AVList params) throws java.io.IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("nullValue.PathIsNull", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		// PATCH GLOBE - Reader specific : MyGeotiffElevationReader instead of GeotiffReader
		// GeotiffReader reader = null;
		MyGeotiffImageReader reader = null;
		try {
			reader = new MyGeotiffImageReader(path, valGlobaleMin, valGlobaleMax);
			reader.copyMetadataTo(params);

			boolean isGeoTiff = reader.isGeotiff(0);
			if (!isGeoTiff && params.hasKey(AVKey.WIDTH) && params.hasKey(AVKey.HEIGHT)) {
				int[] size = new int[2];

				size[0] = (Integer) params.getValue(AVKey.WIDTH);
				size[1] = (Integer) params.getValue(AVKey.HEIGHT);

				params.setValue(WorldFile.WORLD_FILE_IMAGE_SIZE, size);

				WorldFile.readWorldFiles(source, params);

				Object o = params.getValue(AVKey.SECTOR);
				if (o == null || !(o instanceof Sector)) {
					ImageUtil.calcBoundingBoxForUTM(params);
				}
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	@Override
	public double getValGlobaleMin() {
		return valGlobaleMin;
	}

	@Override
	public void setValGlobaleMin(double valGlobaleMin) {
		this.valGlobaleMin = valGlobaleMin;
	}

	@Override
	public double getValGlobaleMax() {
		return valGlobaleMax;
	}

	@Override
	public void setValGlobaleMax(double valGlobaleMax) {
		this.valGlobaleMax = valGlobaleMax;
	}

	@Override
	public double getValMin() {
		return valMin;
	}

	@Override
	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	@Override
	public double getValMax() {
		return valMax;
	}

	@Override
	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

}
