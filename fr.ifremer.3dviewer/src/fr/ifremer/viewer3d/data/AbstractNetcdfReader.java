package fr.ifremer.viewer3d.data;

import java.io.IOException;
import java.nio.FloatBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.data.reader.raster.Raster;
import fr.ifremer.viewer3d.data.reader.raster.RasterAttributes;
import fr.ifremer.viewer3d.data.reader.raster.RasterReader;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.ByteBufferRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

/**
 * Base class for Netcdf TM
 * 
 */
public abstract class AbstractNetcdfReader {

	private static final Logger logger = LoggerFactory.getLogger(AbstractNetcdfReader.class);

	BooleanPreferenceAttribute useCacheForProjection;

	protected Raster rasterBuffer;

	/** All attributes describing a raster. */
	protected final RasterAttributes rasterAttributes;

	private boolean isNetcdf = false;
	protected AVList metadata = null;

	private TextureData textureData = null;

	/**
	 * Constructor.
	 */
	public AbstractNetcdfReader(String filename) {
		this.rasterAttributes = new RasterAttributes(filename);
		useCacheForProjection = Activator.getPluginParameters().getUseCacheForProjection();
		metadata = new AVListImpl();
	}

	/**
	 * Initialize the RasterAttributes.
	 */
	protected abstract void loadRasterAttributes() throws FileFormatException;

	public DataRaster[] readDataRaster() throws FileFormatException {
		DataRaster[] rasters = new DataRaster[1];
		rasters[0] = doRead();
		return rasters;
	}

	public int getWidth(int imageIndex) throws IOException {
		AVList values = this.metadata;
		return (values.hasKey(AVKey.WIDTH)) ? (Integer) values.getValue(AVKey.WIDTH) : 0;
	}

	public int getHeight(int imageIndex) throws IOException {
		AVList values = this.metadata;
		return (values.hasKey(AVKey.HEIGHT)) ? (Integer) values.getValue(AVKey.HEIGHT) : 0;
	}

	protected AVList getMetadata() throws IOException {
		AVList values = this.metadata;
		return (null != values) ? values.copy() : null;
	}

	public AVList copyMetadataTo(int imageIndex, AVList values) throws IOException {
		AVList list = this.getMetadata();
		if (null != values) {
			values.setValues(list);
		} else {
			values = list;
		}
		return values;
	}

	public AVList copyMetadataTo(AVList list) throws IOException {
		return this.copyMetadataTo(0, list);
	}

	/**
	 * read dtm.
	 * <p>
	 * return real depth.
	 * </p>
	 */
	public float getRealDepth(int p_x, int p_y) {
		return rasterBuffer != null ? rasterBuffer.get(p_x, p_y) : Float.NaN;
	}

	public void readMetaData() throws FileFormatException {

		AVList values = this.metadata;

		loadRasterAttributes();

		values.setValue(AVKey.FILE_NAME, rasterAttributes.getFilename());
		// after we read all data, we have everything as BIG_ENDIAN
		values.setValue(AVKey.BYTE_ORDER, AVKey.BIG_ENDIAN);
		values.setValue(AVKey.WIDTH, getColumns());
		values.setValue(AVKey.HEIGHT, getLines());
		values.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);

		// Sector
		values.setValue(AVKey.ORIGIN,
				LatLon.fromDegrees(rasterAttributes.getGeoBox().getTop(), rasterAttributes.getGeoBox().getLeft()));
		values.setValue(AVKey.SECTOR,
				new Sector(Angle.fromDegrees(rasterAttributes.getGeoBox().getBottom()),
						Angle.fromDegrees(rasterAttributes.getGeoBox().getTop()),
						Angle.fromDegrees(rasterAttributes.getGeoBox().getLeft()),
						Angle.fromDegrees(rasterAttributes.getGeoBox().getRight())));

		isNetcdf = rasterAttributes.isReadSuccessfully();
	}

	public void readFile() throws FileFormatException {
		if (rasterBuffer == null) {
			RasterReader rasterReader = new RasterReader(rasterAttributes);
			rasterBuffer = rasterReader.readFile();
			isNetcdf = rasterAttributes.isReadSuccessfully();
		}
	}

	protected DataRaster doRead() throws FileFormatException {
		readFile();

		Sector sector = (Sector) metadata.getValue(AVKey.SECTOR);
		ByteBufferRaster raster = new ByteBufferRaster(getColumns(), getLines(), sector, metadata);
		if (logger.isDebugEnabled()) {
			logger.debug("Read new raster " + getColumns() + " x " + getLines() + ", for sector " + sector);
		}

		FloatBuffer buff = FloatBuffer.allocate(getColumns() * getLines());
		for (int line = 0; line < getLines(); line++) {
			for (int column = 0; column < getColumns(); column++) {
				try {
					float value = rasterBuffer.get(column, line);
					raster.setDoubleAtPosition(line, column, value);
					buff.put(value);
				} catch (Throwable t) {
					logger.error("Error reading DataRaster", t);
				}
			}
		}

		raster.setValue(AVKey.MISSING_DATA_SIGNAL, Raster.NO_VALUE);

		GLProfile glprofile = GLProfile.getDefault();
		textureData = new TextureData(glprofile, GL2.GL_LUMINANCE32F, getColumns(), getLines(), 0, GL.GL_LUMINANCE,
				GL.GL_FLOAT, false, false, false, buff.rewind(), null);

		return raster;

	}

	public boolean isNetcdf() {
		return isNetcdf;
	}

	public float getMissingValue() {
		return Raster.NO_VALUE;
	}

	public float getMinValue() {
		return rasterBuffer.getMinValue();
	}

	public float getMaxValue() {
		return rasterBuffer.getMaxValue();
	}

	public int getColumns() {
		return rasterAttributes.getColumns();
	}

	public int getLines() {
		return rasterAttributes.getLines();
	}

	public GeoBox getGeoBox() {
		return rasterAttributes.getGeoBox();
	}

	public void setTextureData(TextureData textureData) {
		this.textureData = textureData;
	}

	public TextureData getTextureData() {
		return textureData;
	}

	/**
	 * @return the {@link #rasterAttributes}
	 */
	public RasterAttributes getRasterAttributes() {
		return rasterAttributes;
	}

}
