package fr.ifremer.viewer3d.data;

import fr.ifremer.globe.utils.exception.FileFormatException;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.AbstractDataRasterReader;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;

import java.io.IOException;

import com.jogamp.opengl.util.texture.TextureData;

public abstract class AbstractNetcdfElevationRasterReader extends AbstractDataRasterReader {

	private TextureData textureData;
	AbstractNetcdfReader reader = null;
	private String currentPath = null;

	abstract AbstractNetcdfReader createReader(String path);

	protected AbstractNetcdfElevationRasterReader(String[] netcdfMimeTypes, String[] netcdfSuffixes) {
		super(netcdfMimeTypes, netcdfSuffixes);
	}

	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws IOException {
		String path = WWIO.getSourcePath(source);
		if (path != currentPath) {
			currentPath = path;
			reader = null;
		}
		if (path == null) {
			String message = Logging.getMessage("DataRaster.CannotRead", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		AVList metadata = new AVListImpl();
		if (null != params) {
			metadata.setValues(params);
		}

		DataRaster[] rasters = null;
		this.readMetadata(source, metadata);

		if (reader == null) {
			reader = createReader(path);
		}
		reader.copyMetadataTo(metadata);

		try {
			rasters = reader.readDataRaster();
		} catch (FileFormatException e) {
			throw new IOException(e.getMessage());
		}

		textureData = reader.getTextureData();

		if (null != rasters) {
			String[] keysToCopy = new String[] { AVKey.SECTOR };
			for (DataRaster raster : rasters) {
				WWUtil.copyValues(metadata, raster, keysToCopy, false);
			}
		}
		return rasters;
	}

	public void setTextureData(TextureData textureData) {
		this.textureData = textureData;
	}

	public TextureData getTextureData() {
		return textureData;
	}

	@Override
	protected void doReadMetadata(Object source, AVList params) throws IOException {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			String message = Logging.getMessage("nullValue.PathIsNull", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		try {
			if (path != currentPath) {
				currentPath = path;
				reader = null;
			}
			if (reader == null) {
				reader = createReader(path);
				reader.readMetaData();
			}
			reader.copyMetadataTo(params);

			boolean isNetcdf = reader.isNetcdf();
			if (!isNetcdf && params.hasKey(AVKey.WIDTH) && params.hasKey(AVKey.HEIGHT)) {
				int[] size = new int[2];

				size[0] = (Integer) params.getValue(AVKey.WIDTH);
				size[1] = (Integer) params.getValue(AVKey.HEIGHT);

				params.setValue(WorldFile.WORLD_FILE_IMAGE_SIZE, size);

				WorldFile.readWorldFiles(source, params);

				Object o = params.getValue(AVKey.SECTOR);
				if (o == null || !(o instanceof Sector)) {
					ImageUtil.calcBoundingBoxForUTM(params);
				}
			}
		} catch (FileFormatException e) {
			throw new IOException(e.getMessage());
		}

	}
}
