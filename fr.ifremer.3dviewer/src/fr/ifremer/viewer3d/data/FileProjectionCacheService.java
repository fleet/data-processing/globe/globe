package fr.ifremer.viewer3d.data;

import java.nio.file.Path;
import java.util.HashMap;

public class FileProjectionCacheService {
	static FileProjectionCacheService instance=null;
	HashMap<Path, FileProjectionCache> map=new HashMap<>();
	protected FileProjectionCacheService()
	{

	}
	public static FileProjectionCacheService getInstance()
	{
		if(instance==null)
		{
			instance=new FileProjectionCacheService();

		}
		return instance;
	}
	/**
	 * return a {@link FileProjectionCache} for the given file, if file does not exists return null
	 * */
	public  FileProjectionCache get(Path file)
	{
		return map.get(file);
	}
	/**
	 * add a {@link FileProjectionCache} for the given file
	 * */
	public  FileProjectionCache put(Path key,FileProjectionCache cache)
	{
		return map.put(key,cache);
	}


	/**
	 * remove a {@link FileProjectionCache} for the given file
	 * */
	public  void remove(Path file)
	{
		FileProjectionCache del=map.remove(file);
		if(del!=null)
			del.dispose();
	}

}
