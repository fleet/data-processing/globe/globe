package fr.ifremer.viewer3d.data;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.cache.BasicMemoryCache;
import gov.nasa.worldwind.cache.MemoryCache;
import gov.nasa.worldwind.data.BILRasterReader;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.DataRasterReader;
import gov.nasa.worldwind.data.DataRasterWriter;
import gov.nasa.worldwind.data.GeotiffRasterWriter;
import gov.nasa.worldwind.data.TiledElevationProducer;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Tile;

/**
 * Allows to create quad-tree tiled elevations files for WW cache mechanism.
 * <p>
 * See <a href= "https://worldwind.arc.nasa.gov/java/latest/javadoc/index.html?gov/nasa/worldwind/data"
 * >gov.nasa.worldwind.data's javadoc</a>
 * </p>
 *
 */
public class MyTiledElevationProducer extends TiledElevationProducer {

	public static long CACHE_CAPACITY = 900_000_000L;

	protected static final Logger logger = LoggerFactory.getLogger(MyTiledElevationProducer.class);
	protected static final int DEFAULT_TILE_WIDTH_AND_HEIGHT = 512;
	protected static final int DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT = 512;

	protected LevelSet levels;

	/**
	 * By default, force the tiles production even if the cache already has them. <br>
	 * See {@link #doStartProduction} This is used in Placa's morphing, to avoid unassary tiles production by setting
	 * the value to false.
	 */
	protected boolean recomputeCache = true;

	/** Readers used to for unknown data sources */
	protected ArrayList<DataRasterReader> readers = new ArrayList<>();

	/** Constructor */
	public MyTiledElevationProducer(MemoryCache cache, int writeThreadPoolSize, boolean recomputeCache) {
		super(cache, writeThreadPoolSize);
		this.recomputeCache = recomputeCache;
		initReadersList();
	}

	public MyTiledElevationProducer(boolean recomputeCache) {
		// FIXME : Kludge to fix input files greater than default cache size
		// (which leads to out of memory exceptions)
		// 2 : DEFAULT_WRITE_THREAD_POOL_SIZE
		this(new BasicMemoryCache((long) (0.6 * CACHE_CAPACITY), CACHE_CAPACITY), 2, recomputeCache);
	}

	/** {@inheritDoc} */
	@Override
	protected void initLevelSetParameters(AVList params) {
		// PATCH GLOBE - BEGIN - Use dataRasters instead of this.dataRasterList or getDataRasters() to avoid
		// ConcurrentModificationException
		List<DataRaster> dataRasters = new ArrayList<>();
		getDataRasters().forEach(dataRasters::add);
		// PATCH GLOBE - END

		int largeThreshold = Configuration.getIntegerValue(AVKey.TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD,
				MyTiledImageProducer.DEFAULT_TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD);
		boolean isDataSetLarge = this.isDataSetLarge(dataRasters, largeThreshold);

		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		if (sector == null) {
			// Compute a sector that bounds the data rasters. Make sure the sector does not exceed the limits of
			// latitude and longitude.
			sector = this.computeBoundingSector(dataRasters);
			if (sector != null) {
				sector = sector.intersection(Sector.FULL_SPHERE);
			}
			params.setValue(AVKey.SECTOR, sector);
		}

		Integer tileWidth = (Integer) params.getValue(AVKey.TILE_WIDTH);
		if (tileWidth == null) {
			tileWidth = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_WIDTH, tileWidth);
		}

		Integer tileHeight = (Integer) params.getValue(AVKey.TILE_HEIGHT);
		if (tileHeight == null) {
			tileHeight = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_HEIGHT, tileHeight);
		}

		LatLon rasterTileDelta = this.computeRasterTileDelta(tileWidth, tileHeight, dataRasters);
		LatLon desiredLevelZeroDelta = this.computeDesiredTileDelta(sector);

		Integer numLevels = (Integer) params.getValue(AVKey.NUM_LEVELS);
		if (numLevels == null) {
			// If the data set is large, then use compute a number of levels for the full pyramid. Otherwise use a
			// single level.
			// PATCH GLOBE - BEGIN - Always compute
			// @TODO : have to explain this modifiction
			// numLevels = isDataSetLarge ? this.computeNumLevels(desiredLevelZeroDelta, rasterTileDelta) : 1;
			numLevels = this.computeNumLevels(desiredLevelZeroDelta, rasterTileDelta);
			// PATCH GLOBE - END
			params.setValue(AVKey.NUM_LEVELS, numLevels);
		}

		Integer numEmptyLevels = (Integer) params.getValue(AVKey.NUM_EMPTY_LEVELS);
		if (numEmptyLevels == null) {
			numEmptyLevels = 0;
			params.setValue(AVKey.NUM_EMPTY_LEVELS, numEmptyLevels);
		}

		LatLon levelZeroTileDelta = (LatLon) params.getValue(AVKey.LEVEL_ZERO_TILE_DELTA);
		if (levelZeroTileDelta == null) {
			double scale = Math.pow(2d, numLevels - 1d);
			levelZeroTileDelta = LatLon.fromDegrees(scale * rasterTileDelta.getLatitude().degrees,
					scale * rasterTileDelta.getLongitude().degrees);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);
		}

		LatLon tileOrigin = (LatLon) params.getValue(AVKey.TILE_ORIGIN);
		if (tileOrigin == null) {
			tileOrigin = new LatLon(sector.getMinLatitude(), sector.getMinLongitude());
			params.setValue(AVKey.TILE_ORIGIN, tileOrigin);
		}

		// If the default or caller-specified values define a level set that does not fit in the limits of latitude
		// and longitude, then we re-define the level set parameters using values known to fit in those limits.
		if (!this.isWithinLatLonLimits(sector, levelZeroTileDelta, tileOrigin)) {
			levelZeroTileDelta = this.computeIntegralLevelZeroTileDelta(levelZeroTileDelta);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);

			tileOrigin = new LatLon(Angle.NEG90, Angle.NEG180);
			params.setValue(AVKey.TILE_ORIGIN, tileOrigin);

			numLevels = this.computeNumLevels(levelZeroTileDelta, rasterTileDelta);
			params.setValue(AVKey.NUM_LEVELS, numLevels);
		}
	}

	@Override
	protected void doStartProduction(AVList parameters) throws Exception {

		// PATCH GLOBE - BEGIN - from TiledElevationProducer...
		extremes = null;

		// PATCH GLOBE - from TiledRasterProducer...
		// Copy production parameters to prevent changes to caller's reference.
		productionParams = parameters.copy();
		productionParams.setValue(AVKey.IMAGE_FORMAT, "image/tiff");
		this.initProductionParameters(productionParams);

		// Assemble the source data rasters.
		// PATCH GLOBE - BEGIN - Line added compared to the original code
		((List<?>) getDataRasters()).clear();
		// PATCH GLOBE - END
		this.assembleDataRasters();

		// Initialize the level set parameters, and create the level set.
		this.initLevelSetParameters(productionParams);
		LevelSet levelSet = new LevelSet(productionParams);
		// Install the each tiles of the LevelSet.
		// PATCH GLOBE - Condition added
		if (recomputeCache) {
			this.installLevelSet(levelSet, productionParams);
		}

		// PATCH GLOBE - BEGIN - code added
		levelSet.setValues(productionParams);
		setLevels(levelSet);
		// PATCH GLOBE - END

		// Wait for concurrent tasks to complete.
		this.waitForInstallTileTasks();

		// Clear the raster cache.
		// PATCH GLOBE - Condition added
		if (recomputeCache) {
			this.getCache().clear();
		}

		// Install the data descriptor for this tiled raster set.
		this.installConfigFile(productionParams);

		if (AVKey.SERVICE_NAME_LOCAL_RASTER_SERVER.equals(productionParams.getValue(AVKey.SERVICE_NAME))) {
			this.installRasterServerConfigFile(productionParams);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected DataRasterReader[] getDataRasterReaders() {
		return readers.toArray(new DataRasterReader[readers.size()]);
	}

	/**
	 * Returns an ElevationModel configuration document which describes the tiled elevation data produced by this
	 * TiledElevationProducer. The document's contents are based on the configuration document for a basic
	 * ElevationModel, except this document describes an offline dataset. This returns null if the parameter list is
	 * null, or if the configuration document cannot be created for any reason.
	 *
	 * @param params the parameters which describe an ElevationModel configuration document's contents.
	 *
	 * @return the configuration document, or null if the parameter list is null or does not contain the required
	 *         parameters.
	 */
	@Override
	protected Document createConfigDoc(AVList params) {
		AVList configParams = params.copy();

		// Determine a default display name if none exists.
		if (configParams.getValue(AVKey.DISPLAY_NAME) == null) {
			configParams.setValue(AVKey.DISPLAY_NAME, params.getValue(AVKey.DATASET_NAME));
		}

		// Set the SERVICE_NAME and NETWORK_RETRIEVAL_ENABLED parameters to indicate this dataset is offline.
		if (configParams.getValue(AVKey.SERVICE_NAME) == null) {
			configParams.setValue(AVKey.SERVICE_NAME, AVKey.SERVICE_NAME_OFFLINE);
		}

		configParams.setValue(AVKey.NETWORK_RETRIEVAL_ENABLED, Boolean.FALSE);

		// TiledElevationProducer and the DataRaster classes use MISSING_DATA_REPLACEMENT to denote the missing data
		// value, whereas all other WWWJ code uses MISSING_DATA_SIGNAL. Replace the MISSING_DATA_REPLACEMENT with the
		// MISSING_DATA_SIGNAL here to ensure this discrapancy is limited to data installation code.
		configParams.removeKey(AVKey.MISSING_DATA_REPLACEMENT);
		configParams.setValue(AVKey.MISSING_DATA_SIGNAL, params.getValue(AVKey.MISSING_DATA_REPLACEMENT));

		// If we able to successfully compute extreme values for this elevation data, then set the extreme elevation
		// values ELEVATION_MIN and ELEVATION_MAX. If the extremes array is null or has length less than 2, the imported
		// elevations are either empty or contain only missing data values. In either case we cannot determine the
		// extreme values.
		if (extremes != null && extremes.length >= 2) {
			configParams.setValue(AVKey.ELEVATION_MIN, extremes[0]);
			configParams.setValue(AVKey.ELEVATION_MAX, extremes[1]);
		}

		// PATCH GLOBE - BEGIN - code added
		// PLACA WORKAROUND seems that WWXML.makeXPath() return a NullPointerException on mac platform
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(new URLClassLoader(new URL[0], cl));
		// PATCH GLOBE - END

		// Return a configuration file for a BasicElevationModel. BasicElevationModel is the standard WWJ component
		// which consumes tiled elevation data.

		return BasicElevationModel.createBasicElevationModelConfigDocument(configParams);

	}

	// correction non generation du cache
	@Override
	protected void installTileRasterLater(final LevelSet levelSet, final Tile tile, final DataRaster tileRaster,
			final AVList params) {
		// PATCH GLOBE - From TiledElevationProducer
		this.updateExtremeElevations(tileRaster);

		// PATCH GLOBE - From TiledRasterProducer
		// Redefine TiledRasterProducer.installTileRasterLater to call installTileRaster synchronously
		try {
			installTileRaster(tile, tileRaster, params);
			tileRaster.dispose();
		} catch (IOException e) {
			logger.error("Error during tile installation", e);
		}

	}

	protected void initReadersList() {
		//readers.add(new GrdElevationRasterReader());

		readers.add(new BILRasterReader());
		readers.add(new MyGeotiffElevationRasterReader());
		//readers.add(new NetcdfElevationRasterReader());
	}

	public void addReader(DataRasterReader reader) {
		readers.add(reader);
	}

	public void removeReader(DataRasterReader reader) {
		readers.remove(reader);
	}

	/**
	 * @return the writer to use for saving a tile in the WW cache. Here, we decide to save elevations tiles as Tiff
	 *         instead of Bil.
	 */
	@Override
	protected DataRasterWriter[] getDataRasterWriters() {
		return new DataRasterWriter[] { new GeotiffRasterWriter() };
	}

	/**
	 * Instanciates a DataRaster to representes the tile.<br>
	 * Each DataRaster contained in the list dataRasterList dump their data into the resulting DataRaster.
	 */
	@Override
	protected DataRaster createTileRaster(LevelSet levelSet, Tile tile, AVList params) throws java.io.IOException {
		// ByteBufferRaster containing elevation data for the specified tile
		DataRaster tileRaster = super.createTileRaster(levelSet, tile, params);
		// GeotiffRasterWriter expects pixel format as elevation.
		tileRaster.setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
		return tileRaster;
	}

	/**
	 * Create a elevation raster to hold the tile's data. <br>
	 * Called by {@link #createTileRaster}
	 */
	@Override
	protected DataRaster createDataRaster(int width, int height, Sector sector, AVList params) {
		// Expected format of tile's data in a Geotiff : float
		params.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
		params.setValue(AVKey.BYTE_ORDER, AVKey.BIG_ENDIAN);

		return super.createDataRaster(width, height, sector, params);
	}

	/** @return the configured cache size */
	public static long getCacheSize() {
		return SSVConfiguration.getConfiguration().getCacheSize();
	}

	/** Getter of {@link #levels} */
	public LevelSet getLevels() {
		return levels;
	}

	/** Setter of {@link #levels} */
	public void setLevels(LevelSet levels) {
		this.levels = levels;
	}

	/** Getter of {@link #recomputeCache} */
	public boolean isRecomputeCache() {
		return recomputeCache;
	}

	/** Setter of {@link #recomputeCache} */
	public void setRecomputeCache(boolean recomputeCache) {
		this.recomputeCache = recomputeCache;
	}
}
