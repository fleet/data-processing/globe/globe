package fr.ifremer.viewer3d.data;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTiledImageLayer;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.BasicMemoryCache;
import gov.nasa.worldwind.data.BufferWrapperRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.DataRasterReader;
import gov.nasa.worldwind.data.DataRasterWriter;
import gov.nasa.worldwind.data.ImageIORasterReader;
import gov.nasa.worldwind.data.RPFRasterReader;
import gov.nasa.worldwind.data.TiledImageProducer;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.Tile;

/**
 * Tiled image producer for ?
 */
public class MyTiledImageProducer extends TiledImageProducer implements IBoundedMinMax {

	public static long CACHE_CAPACITY = 900000000;

	private Logger logger = LoggerFactory.getLogger(MyTiledImageProducer.class);

	private LevelSet levels;
	protected static final int DEFAULT_TILE_WIDTH_AND_HEIGHT = 512;
	protected static final int DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT = 512;
	/** 3000 pixels. */
	public static final int DEFAULT_TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD = 3000;

	private double valGlobaleMin = -20000.0;
	private double valGlobaleMax = 20000.0;
	private double valMin = 0.0;
	private double valMax = 0.0;
	private String unit = "m";
	private boolean recomputeCache = true;

	private ArrayList<DataRasterReader> readers = new ArrayList<>();

	@Override
	protected DataRasterWriter[] getDataRasterWriters() {
		return new DataRasterWriter[] {
				// Configure the ImageIO writer to disable writing of georeference
				// files. Georeferencing files are
				// redundant for tiled images. The image format is defined in the data
				// configuration file, and each
				// tile's georeferencing information is implicit in the tile structure.
				new MyImageIORasterWriter(false) };
	}

	public MyTiledImageProducer(String dataField, String unit, double min, double max, double valMin, double valMax) {

		super(new BasicMemoryCache((long) (0.6 * CACHE_CAPACITY), CACHE_CAPACITY), 2);
		this.unit = unit;
		valGlobaleMin = min;
		valGlobaleMax = max;
		this.valMin = valMin;
		this.valMax = valMax;

		initReadersList();

		for (DataRasterReader reader : readers) {
			if (reader instanceof IBoundedMinMax) {
				IBoundedMinMax readerWithMinMax = (IBoundedMinMax) reader;
				readerWithMinMax.setValGlobaleMin(valGlobaleMin);
				readerWithMinMax.setValGlobaleMax(valGlobaleMax);
				readerWithMinMax.setValMin(valMin);
				readerWithMinMax.setValMax(valMax);
			}
			if (reader instanceof IMultiFieldLoader) {
				((IMultiFieldLoader) reader).setDataField(dataField);
			}
		}
	}

	public boolean isRecomputeCache() {
		return recomputeCache;
	}

	public void setRecomputeCache(boolean recomputeCache) {
		this.recomputeCache = recomputeCache;
	}

	@Override
	protected void assembleDataSource(Object source, AVList params) throws Exception {
		List<DataRaster> rasters;
		Iterable<DataRaster> dr = this.getDataRasters();
		if (dr instanceof List) {
			rasters = (List<DataRaster>) dr;
		} else {
			throw new InvalidParameterException();
		}

		if (source instanceof DataRaster) {
			rasters.add((DataRaster) source);

		} else {
			DataRasterReader reader = this.getReaderFactory().findReaderFor(source, params,
					this.getDataRasterReaders());
			rasters.add(new MyCachedDataRaster(source, params, reader, this.getCache()));
		}
	}

	@Override
	protected DataRasterReader[] getDataRasterReaders() {
		return readers.toArray(new DataRasterReader[readers.size()]);
	}

	@Override
	protected DataRaster drawDataSources(LevelSet levelSet, Tile tile, Iterable<DataRaster> dataRasters, AVList params)
			throws java.io.IOException {
		DataRaster tileRaster = null;

		// Find the data sources that intersect this tile and intersect the LevelSet sector.
		java.util.ArrayList<DataRaster> intersectingRasters = new java.util.ArrayList<>();
		for (DataRaster raster : dataRasters) {
			if (raster.getSector().intersects(tile.getSector())
					&& raster.getSector().intersects(levelSet.getSector())) {
				intersectingRasters.add(raster);
			}
		}

		// If any data sources intersect this tile, and the tile's level is not empty, then we attempt to read those
		// sources and render them into this tile.
		if (!intersectingRasters.isEmpty() && !tile.getLevel().isEmpty()) {
			// Create the tile raster to render into.
			tileRaster = this.createDataRaster(tile.getLevel().getTileWidth(), tile.getLevel().getTileHeight(),
					tile.getSector(), params);
			// Render each data source raster into the tile raster.
			for (DataRaster raster : intersectingRasters) {
				raster.drawOnTo(tileRaster);

				// PATCH GLOBE - BEGIN - code added
				if (raster instanceof IBoundedMinMax) {
					IBoundedMinMax rasterWithMinMax = (IBoundedMinMax) raster;
					if (valMin == 0.0 && valMax == 0.0) {
						valMin = rasterWithMinMax.getValMin();
						valMax = rasterWithMinMax.getValMax();
						valGlobaleMax = rasterWithMinMax.getValGlobaleMax();
						valGlobaleMin = rasterWithMinMax.getValGlobaleMin();
					}
				}
				// PATCH GLOBE - END
			}
		}

		// Make the data rasters available for garbage collection.
		intersectingRasters.clear();
		// noinspection UnusedAssignment
		intersectingRasters = null;

		return tileRaster;
	}

	/**
	 * Handles production.
	 *
	 * @param parameters Production parameters.
	 */
	@Override
	protected void doStartProduction(AVList parameters) throws Exception {
		// Copy production parameters to prevent changes to caller's reference.
		productionParams = parameters.copy();
		this.initProductionParameters(productionParams);

		// Assemble the source data rasters.
		// PATCH GLOBE - BEGIN - Line added compared to the original code
		((List<?>) getDataRasters()).clear();
		// PATCH GLOBE - END
		this.assembleDataRasters();

		// Initialize the level set parameters, and create the level set.
		this.initLevelSetParameters(productionParams);
		LevelSet levelSet = new LevelSet(productionParams);
		// Install the each tiles of the LevelSet.
		// PATCH GLOBE - Condition added
		if (recomputeCache) {
			this.installLevelSet(levelSet, productionParams);
		}

		// PATCH GLOBE - BEGIN - code added
		levelSet.setValues(productionParams);
		setLevels(levelSet);
		// PATCH GLOBE - END

		// Wait for concurrent tasks to complete.
		this.waitForInstallTileTasks();

		// Clear the raster cache.
		// PATCH GLOBE - Condition added
		if (recomputeCache) {
			this.getCache().clear();
		}

		// Install the data descriptor for this tiled raster set.
		this.installConfigFile(productionParams);

		if (AVKey.SERVICE_NAME_LOCAL_RASTER_SERVER.equals(productionParams.getValue(AVKey.SERVICE_NAME))) {
			this.installRasterServerConfigFile(productionParams);
		}
	}

	@Override
	protected void initLevelSetParameters(AVList params) {
		// PATCH GLOBE - BEGIN - Use dataRasters instead of this.dataRasterList or getDataRasters() to avoid
		// ConcurrentModificationException
		List<DataRaster> dataRasters = new ArrayList<>();
		getDataRasters().forEach(dataRasters::add);
		// PATCH GLOBE - END

		int largeThreshold = Configuration.getIntegerValue(AVKey.TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD,
				MyTiledImageProducer.DEFAULT_TILED_RASTER_PRODUCER_LARGE_DATASET_THRESHOLD);
		boolean isDataSetLarge = this.isDataSetLarge(dataRasters, largeThreshold);

		Sector sector = (Sector) params.getValue(AVKey.SECTOR);
		if (sector == null) {
			// Compute a sector that bounds the data rasters. Make sure the sector does not exceed the limits of
			// latitude and longitude.
			sector = this.computeBoundingSector(dataRasters);
			if (sector != null) {
				sector = sector.intersection(Sector.FULL_SPHERE);
			}
			params.setValue(AVKey.SECTOR, sector);
		}

		Integer tileWidth = (Integer) params.getValue(AVKey.TILE_WIDTH);
		if (tileWidth == null) {
			tileWidth = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_WIDTH, tileWidth);
		}

		Integer tileHeight = (Integer) params.getValue(AVKey.TILE_HEIGHT);
		if (tileHeight == null) {
			tileHeight = isDataSetLarge ? DEFAULT_TILE_WIDTH_AND_HEIGHT : DEFAULT_SINGLE_LEVEL_TILE_WIDTH_AND_HEIGHT;
			params.setValue(AVKey.TILE_HEIGHT, tileHeight);
		}

		LatLon rasterTileDelta = this.computeRasterTileDelta(tileWidth, tileHeight, dataRasters);
		LatLon desiredLevelZeroDelta = this.computeDesiredTileDelta(sector);

		Integer numLevels = (Integer) params.getValue(AVKey.NUM_LEVELS);
		if (numLevels == null) {
			// If the data set is large, then use compute a number of levels for the full pyramid. Otherwise use a
			// single level.
			// PATCH GLOBE - BEGIN - Always compute
			// numLevels = isDataSetLarge ? this.computeNumLevels(desiredLevelZeroDelta, rasterTileDelta) : 1;
			numLevels = this.computeNumLevels(desiredLevelZeroDelta, rasterTileDelta);
			// PATCH GLOBE - END
			params.setValue(AVKey.NUM_LEVELS, numLevels);
		}

		Integer numEmptyLevels = (Integer) params.getValue(AVKey.NUM_EMPTY_LEVELS);
		if (numEmptyLevels == null) {
			numEmptyLevels = 0;
			params.setValue(AVKey.NUM_EMPTY_LEVELS, numEmptyLevels);
		}

		LatLon levelZeroTileDelta = (LatLon) params.getValue(AVKey.LEVEL_ZERO_TILE_DELTA);
		if (levelZeroTileDelta == null) {
			double scale = Math.pow(2d, numLevels - 1);
			levelZeroTileDelta = LatLon.fromDegrees(scale * rasterTileDelta.getLatitude().degrees,
					scale * rasterTileDelta.getLongitude().degrees);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);
		}

		LatLon tileOrigin = (LatLon) params.getValue(AVKey.TILE_ORIGIN);
		if (tileOrigin == null) {
			tileOrigin = new LatLon(sector.getMinLatitude(), sector.getMinLongitude());
			params.setValue(AVKey.TILE_ORIGIN, tileOrigin);
		}

		// If the default or caller-specified values define a level set that does not fit in the limits of latitude
		// and longitude, then we re-define the level set parameters using values known to fit in those limits.
		if (!this.isWithinLatLonLimits(sector, levelZeroTileDelta, tileOrigin)) {
			levelZeroTileDelta = this.computeIntegralLevelZeroTileDelta(levelZeroTileDelta);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, levelZeroTileDelta);

			tileOrigin = new LatLon(Angle.NEG90, Angle.NEG180);
			params.setValue(AVKey.TILE_ORIGIN, tileOrigin);

			numLevels = this.computeNumLevels(levelZeroTileDelta, rasterTileDelta);
			params.setValue(AVKey.NUM_LEVELS, numLevels);
		}
	}

	/**
	 * Returns a Layer configuration document which describes the tiled imagery produced by this TiledImageProducer. The
	 * document's contents are based on the configuration document for a TiledImageLayer, except this document describes
	 * an offline dataset. This returns null if the parameter list is null, or if the configuration document cannot be
	 * created for any reason.
	 *
	 * @param params the parameters which describe a Layer configuration document's contents.
	 *
	 * @return the configuration document, or null if the parameter list is null or does not contain the required
	 *         parameters.
	 */
	@Override
	protected Document createConfigDoc(AVList params) {
		AVList configParams = params.copy();

		// Determine a default display name if none exists.
		if (configParams.getValue(AVKey.DISPLAY_NAME) == null) {
			configParams.setValue(AVKey.DISPLAY_NAME, params.getValue(AVKey.DATASET_NAME));
		}

		// Set the SERVICE_NAME and NETWORK_RETRIEVAL_ENABLED parameters to indicate this dataset is offline.
		// PATCH GLOBE - BEGIN - Always offline
		// if (configParams.getValue(AVKey.SERVICE_NAME) == null)
		// configParams.setValue(AVKey.SERVICE_NAME, AVKey.SERVICE_NAME_OFFLINE);
		configParams.setValue(AVKey.SERVICE_NAME, "Offline");
		// PATCH GLOBE - END

		configParams.setValue(AVKey.NETWORK_RETRIEVAL_ENABLED, Boolean.FALSE);

		// Set the texture format to DDS. If the texture data is already in DDS format, this parameter is benign.
		configParams.setValue(AVKey.TEXTURE_FORMAT, DEFAULT_TEXTURE_FORMAT);

		// Set the USE_MIP_MAPS, and USE_TRANSPARENT_TEXTURES parameters to true. The imagery produced by
		// TiledImageProducer is best viewed with mipMapping enabled, and texture transparency enabled. These parameters
		// tell the consumer of this imagery to enable these features during rendering.
		configParams.setValue(AVKey.USE_MIP_MAPS, Boolean.TRUE);
		configParams.setValue(AVKey.USE_TRANSPARENT_TEXTURES, Boolean.TRUE);

		// PATCH GLOBE - BEGIN - More params
		// Bathyscope contrast properties
		configParams.setValue(MyTiledImageLayer.XML_KEY_UNIT, unit);
		configParams.setValue(MyTiledImageLayer.XML_KEY_GlobalMinValue, Double.toString(valGlobaleMin));
		configParams.setValue(MyTiledImageLayer.XML_KEY_GlobalMaxValue, Double.toString(valGlobaleMax));
		configParams.setValue(MyTiledImageLayer.XML_KEY_MinValue, Double.toString(valMin));
		configParams.setValue(MyTiledImageLayer.XML_KEY_MaxValue, Double.toString(valMax));
		// PATCH GLOBE - END

		// Return a configuration file for a TiledImageLayer. TiledImageLayer is the standard WWJ component which
		// consumes and renders tiled imagery.

		// PATCH GLOBE - BEGIN - specific writer
		// return BasicTiledImageLayer.createTiledImageLayerConfigDocument(configParams);
		return MyTiledImageLayer.createTiledImageLayerConfigDocument(configParams);
		// PATCH GLOBE - END
	}

	@Override
	protected String validateDataSource(Object source, AVList params) {
		// TiledElevationProducer does not accept null data sources.
		if (source == null) {
			return Logging.getMessage("nullValue.SourceIsNull");
		}

		// TiledElevationProducer accepts BufferWrapperRaster as a data source.
		// If the data source is a DataRaster, then
		// check that it's a BufferWrapperRaster.
		if (source instanceof DataRaster) {
			DataRaster raster = (DataRaster) source;

			if (!(raster instanceof BufferWrapperRaster)) {
				return Logging.getMessage("TiledRasterProducer.UnrecognizedDataSource", raster);
			}

			String s = this.validateDataSourceParams(raster, String.valueOf(raster));
			if (s != null) {
				return s;
			}
		}
		// For any other data source, attempt to find a reader for the data
		// source. If the reader know's the data
		// source's raster type, then check that it's elevation data.
		else {
			DataRasterReader reader = this.getReaderFactory().findReaderFor(source, params,
					this.getDataRasterReaders());
			if (reader == null) {
				return Logging.getMessage("TiledRasterProducer.UnrecognizedDataSource", source);
			}

			// Copy the parameter list to insulate changes from the caller.
			params = params != null ? params.copy() : new AVListImpl();

			try {
				reader.readMetadata(source, params);
			} catch (IOException e) {
				return Logging.getMessage("TiledRasterProducer.ExceptionWhileReading", source, e.getMessage());
			}

			String s = this.validateDataSourceParams(params, String.valueOf(source));
			if (s != null) {
				return s;
			}
		}

		return null;
	}

	public void setLevels(LevelSet levels) {
		this.levels = levels;
	}

	public LevelSet getLevels() {
		return levels;
	}

	@Override
	public double getValGlobaleMin() {
		return valGlobaleMin;
	}

	@Override
	public void setValGlobaleMin(double valGlobaleMin) {
		this.valGlobaleMin = valGlobaleMin;
	}

	@Override
	public double getValGlobaleMax() {
		return valGlobaleMax;
	}

	@Override
	public void setValGlobaleMax(double valGlobaleMax) {
		this.valGlobaleMax = valGlobaleMax;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public double getValMin() {
		return valMin;
	}

	@Override
	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	@Override
	public double getValMax() {
		return valMax;
	}

	@Override
	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	// correction non generation du cache
	@Override
	protected void installTileRasterLater(final LevelSet levelSet, final Tile tile, final DataRaster tileRaster,
			final AVList params) {
		try {
			installTileRaster(tile, tileRaster, params);
			// Dispose the data raster.
			if (tileRaster instanceof Disposable) {
				((Disposable) tileRaster).dispose();
			}
		} catch (IOException e) {
			logger.error("Error during tile installation", e);
		}

	}

	public static long getCACHE_CAPACITY() {
		return CACHE_CAPACITY;
	}

	public static void setCACHE_CAPACITY(long capacity) {
		CACHE_CAPACITY = capacity;
	}

	protected void initReadersList() {
		if (readers.size() > 0) {
			readers.clear();
		}
		readers.add(new RPFRasterReader());
		//readers.add(new GrdImageRasterReader());

		// readers.add(new GDALDataRasterReader());
		readers.add(new ImageIORasterReader());
		readers.add(new MyGeotiffImageRasterReader());
		//readers.add(new NetcdfImageRasterReader());
	}

	public void addReader(DataRasterReader a_reader) {
		readers.add(a_reader);
	}

	public void removeReader(DataRasterReader a_reader) {
		readers.remove(a_reader);
	}
}
