package fr.ifremer.viewer3d.data;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.viewer3d.loaders.ElevationLoader;

/**
 * Class handling a cache to prevent from recomputing file points deprojection to lat long<br>
 * This prevents the deprojection to be computed twice, once for elevation data and once for texture data.<br>
 * Due to possible large amount of data (up to several Gb), it is store to a temporary file<br>
 * It is up to the {@link ElevationLoader} to create and free the cache<br>
 * Data maintained in cache are a bidimensionnal array of double<br>
 * <br><br>
 * */
public class FileProjectionCache {
	private static final Logger logger = LoggerFactory.getLogger(FileProjectionCache.class);
	private Path sourceFile;
	int dimx;
	int dimy;
	/**the file channel*/
	private FileChannel fc;
	private ByteBuffer buffer;

	int sizeofDouble=8;
	private File tempFile;
	/**
	 * Create a new FileCache for the given file
	 * @throws IOException 
	 * */
	public FileProjectionCache(Path file,int dimX,int dimY) throws IOException
	{
		this.sourceFile=file;
		createFile(dimX, dimY);
		FileProjectionCacheService.getInstance().put(sourceFile, this);
	}

	/**
	 * load a line and return the dataset
	 * @throws IOException 
	 * */
	public DoubleBuffer getLine(int line) throws IOException 
	{
		fc.position(((long)line)*dimx*sizeofDouble);
		buffer.rewind();
		fc.read(buffer);
		buffer.rewind();

		return buffer.asDoubleBuffer();

	}
	/**
	 * Get the double buffer, for writing purpose
	 * */
	public DoubleBuffer getBuffer()
	{
		buffer.rewind();
		return buffer.asDoubleBuffer();
	}
	/**
	 * write the current buffer to the disk
	 * */
	public void write(int line) throws Exception
	{

		if(line>=dimy)
		{
			throw new UnsupportedOperationException("Bad dimension, line number is >= "+dimy); 
		}
		fc.position(((long)line)*dimx*sizeofDouble);
		fc.write(buffer);
	}
	private void createFile(int dimX,int dimY) throws IOException {
		tempFile= TemporaryCache.createTemporaryFile(sourceFile.getFileName().toString(), null);
		fc = (FileChannel.open(tempFile.toPath(),StandardOpenOption.READ, StandardOpenOption.WRITE));
		//allocate space storage for one line
		dimx=dimX;
		dimy=dimY;
		buffer=ByteBuffer.allocate(dimX*sizeofDouble);

	}

	public void dispose() {
		try {
			fc.close();
			TemporaryCache.delete(tempFile);
		} catch (Exception e) {
			logger.error(String.format("cannot close file : %s", fc), e);
		}
	}
}
