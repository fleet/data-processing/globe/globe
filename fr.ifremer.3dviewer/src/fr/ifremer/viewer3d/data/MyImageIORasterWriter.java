package fr.ifremer.viewer3d.data;

import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.ImageIORasterWriter;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWUtil;

public class MyImageIORasterWriter extends ImageIORasterWriter {

	public MyImageIORasterWriter(boolean b) {
		super(b);
	}

	@Override
	public void write(DataRaster raster, String formatSuffix, java.io.File file) throws java.io.IOException {
		if (raster == null)
		{
			String message = Logging.getMessage("nullValue.RasterIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}
		if (formatSuffix == null)
		{
			String message = Logging.getMessage("nullValue.FormatSuffixIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		formatSuffix = WWUtil.stripLeadingPeriod(formatSuffix);
		// PATCH GLOBE - BEGIN - Commented
		// unneccessary, has been checked before
		//        if (!this.canWrite(raster, formatSuffix, file))
		//        {
		//        	String message = Logging.getMessage("DataRaster.CannotWrite", raster, formatSuffix, file);
		//        	Logging.logger().severe(message);
		//        	throw new IllegalArgumentException(message);
		//        }
		// PATCH GLOBE - END 

		this.doWrite(raster, formatSuffix, file);
	}

}
