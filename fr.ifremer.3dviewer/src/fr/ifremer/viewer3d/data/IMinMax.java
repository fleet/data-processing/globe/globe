package fr.ifremer.viewer3d.data;

/**
 * Denotes a class having values ranging from $min to $max values.
 *
 * @author apside
 */
public interface IMinMax {
	/**
	 * @return The minimal value found in the object.
	 */
	double getValMin();

	/**
	 * Sets the min value.
	 * 
	 * @param valMin
	 *            the min value.
	 */
	void setValMin(double valMin);

	/**
	 * @return The maximal value found in the object.
	 */
	double getValMax();

	/**
	 * Sets the max value.
	 * 
	 * @param valMax
	 *            the max value.
	 */
	void setValMax(double valMax);
}
