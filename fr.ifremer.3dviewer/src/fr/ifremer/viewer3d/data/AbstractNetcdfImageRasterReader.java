package fr.ifremer.viewer3d.data;

import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.AbstractDataRasterReader;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;

import java.io.IOException;

import com.jogamp.opengl.util.texture.TextureData;

public abstract class AbstractNetcdfImageRasterReader extends AbstractDataRasterReader implements IBoundedMinMax, IMultiFieldLoader {

	private TextureData textureData;
	private double valGlobaleMin = -20000;
	private double valGlobaleMax = 20000;
	private double valMin = 0;
	private double valMax = 0;
	protected AbstractNetcdfReader reader = null;
	private String currentPath = null;
	private String dataField = "DEPTH";

	protected AbstractNetcdfImageRasterReader(String []netcdfMimeTypes, String []netcdfSuffixes) {
		super(netcdfMimeTypes, netcdfSuffixes);
	}

	abstract AbstractNetcdfReader createReader(String path,String dataField) ;

	@Override
	protected
	abstract boolean doCanRead(Object source, AVList params);


	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws IOException {
		String path = WWIO.getSourcePath(source);
		if (path != currentPath) {
			currentPath = path;
			reader = null;
		}
		if (path == null) {
			String message = Logging.getMessage("DataRaster.CannotRead", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		AVList metadata = new AVListImpl();
		if (null != params) {
			metadata.setValues(params);
		}

		DataRaster[] rasters = null;
		this.readMetadata(source, metadata);

		if (reader == null) {
			reader = createReader(path, dataField);
		}

		reader.copyMetadataTo(metadata);
		try {
			rasters = reader.readDataRaster();
		} catch (FileFormatException e) {
			throw new IOException(e.getMessage());
		}
		textureData = reader.getTextureData();
		valMin = reader.getMinValue();
		valMax = reader.getMaxValue();
		valGlobaleMin = reader.getMinValue();
		valGlobaleMax = reader.getMaxValue();
		if (null != rasters) {
			String[] keysToCopy = new String[] { AVKey.SECTOR };
			for (DataRaster raster : rasters) {
				WWUtil.copyValues(metadata, raster, keysToCopy, false);
			}
		}

		return rasters;
	}

	public void setTextureData(TextureData textureData) {
		this.textureData = textureData;
	}

	public TextureData getTextureData() {
		return textureData;
	}

	@Override
	protected void doReadMetadata(Object source, AVList params) throws IOException {
		String path = WWIO.getSourcePath(source);
		if (path != currentPath) {
			currentPath = path;
			reader = null;
		}
		if (path == null) {
			String message = Logging.getMessage("nullValue.PathIsNull", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		try {
			if (reader == null) {
				try {
					reader = createReader(path, dataField);
					reader.readMetaData();
				} catch (GIOException e) {
					throw new IOException(e);
				}
			}
			reader.copyMetadataTo(params);

			boolean isNetcdf = reader.isNetcdf();
			if (!isNetcdf && params.hasKey(AVKey.WIDTH) && params.hasKey(AVKey.HEIGHT)) {
				int[] size = new int[2];

				size[0] = (Integer) params.getValue(AVKey.WIDTH);
				size[1] = (Integer) params.getValue(AVKey.HEIGHT);

				params.setValue(WorldFile.WORLD_FILE_IMAGE_SIZE, size);

				WorldFile.readWorldFiles(source, params);

				Object o = params.getValue(AVKey.SECTOR);
				if (o == null || !(o instanceof Sector)) {
					ImageUtil.calcBoundingBoxForUTM(params);
				}
			}
		} finally {
			if (reader != null) {
				// reader close in read
			}
		}

	}

	@Override
	public double getValGlobaleMin() {
		return valGlobaleMin;
	}

	@Override
	public void setValGlobaleMin(double valGlobaleMin) {
		this.valGlobaleMin = valGlobaleMin;
	}

	@Override
	public double getValGlobaleMax() {
		return valGlobaleMax;
	}

	@Override
	public void setValGlobaleMax(double valGlobaleMax) {
		this.valGlobaleMax = valGlobaleMax;
	}

	@Override
	public double getValMin() {
		return valMin;
	}

	@Override
	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	@Override
	public double getValMax() {
		return valMax;
	}

	@Override
	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	@Override
	public void setDataField(String dataField) {
		this.dataField = dataField;

	}

}
