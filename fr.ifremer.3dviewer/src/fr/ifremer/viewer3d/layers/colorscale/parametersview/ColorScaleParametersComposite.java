package fr.ifremer.viewer3d.layers.colorscale.parametersview;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import fr.ifremer.viewer3d.layers.colorscale.ColorScaleLayer;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Vec4;
import jakarta.inject.Inject;

/**
 * Parameter composite for {@link ColorScaleLayer}.
 */
public class ColorScaleParametersComposite extends Composite {

	/**
	 * Parameters for the position of the {@link ColorScaleLayer}
	 */
	private enum PositionParameters {
		SOUTHEAST(AVKey.SOUTHEAST, "South east"), //
		SOUTHWEST(AVKey.SOUTHWEST, "South west"), //
		NORTHEAST(AVKey.NORTHEAST, "North east"), //
		NORTHWEST(AVKey.NORTHWEST, "North west");

		protected final String key;
		protected final String label;

		private PositionParameters(String key, String label) {
			this.key = key;
			this.label = label;
		}

		static PositionParameters fromKey(String key) {
			for (PositionParameters param : values())
				if (param.key.equals(key))
					return param;
			return null;
		}
	}

	/** Corresponding {@link ColorScaleLayer} **/
	private IWWColorScaleLayer layer;

	/** Global {@link WWFileLayerStoreModel} **/
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	@Inject
	private IGeographicViewService geographicViewService;

	/** Inner components **/
	private Spinner spinnerX;
	private Spinner spinnerY;
	private ComboViewer viewer;
	private Button btnDisplayLabel;
	private Spinner labelPositionSpinner;
	private Button btnHideOtherScales;

	/**
	 * Constructor
	 */
	public ColorScaleParametersComposite(Composite parent, IWWColorScaleLayer colorScaleLayer) {
		super(parent, SWT.NONE);

		// injection to retrieve the fileLayerStoreModel
		ContextInjectionFactory.inject(this, EclipseContextFactory
				.getServiceContext(OsgiUtils.getBundleContext(ColorScaleParametersComposite.class)));

		initializeComposite();
		setLayer(colorScaleLayer);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	public void initializeComposite() {
		setLayout(new GridLayout(1, false));

		// position
		Group grpLayer = new Group(this, SWT.NONE);
		grpLayer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpLayer.setText("Position");
		grpLayer.setLayout(new GridLayout(1, false));

		viewer = new ComboViewer(grpLayer, SWT.READ_ONLY);
		Combo combo = viewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((PositionParameters) element).label;
			}
		});
		viewer.setInput(PositionParameters.values());
		viewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			layer.setPosition(((PositionParameters) selection.getFirstElement()).key);
			geographicViewService.refresh();
		});

		// Margin
		Group grpMarging = new Group(this, SWT.NONE);
		grpMarging.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpMarging.setText("Position offset");
		grpMarging.setLayout(new GridLayout(4, false));

		Label lblTop = new Label(grpMarging, SWT.NONE);
		lblTop.setText("X");
		spinnerX = new Spinner(grpMarging, SWT.BORDER);
		spinnerX.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		spinnerX.setMaximum(1500);
		spinnerX.setMinimum(-1500);

		Label lblBottom = new Label(grpMarging, SWT.NONE);
		lblBottom.setText("Y");
		spinnerY = new Spinner(grpMarging, SWT.BORDER);
		spinnerY.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		spinnerY.setMaximum(1500);
		spinnerY.setMinimum(-1500);

		Listener onMarginSpinnerUpdate = event -> {
			layer.setLocationOffset(new Vec4(spinnerX.getSelection(), spinnerY.getSelection(), 0, 0));
			geographicViewService.refresh();
		};
		spinnerX.addListener(SWT.Selection, onMarginSpinnerUpdate);
		spinnerY.addListener(SWT.Selection, onMarginSpinnerUpdate);

		// label
		Group grpLabel = new Group(this, SWT.NONE);
		grpLabel.setLayout(new GridLayout(3, false));
		grpLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpLabel.setText("Label");

		btnDisplayLabel = new Button(grpLabel, SWT.CHECK);
		btnDisplayLabel.setText("Display label");
		btnDisplayLabel.addListener(SWT.Selection, e -> {
			layer.setLabelEnabled(btnDisplayLabel.getSelection());
			geographicViewService.refresh();
		});

		Label lblPositionOffset = new Label(grpLabel, SWT.NONE);
		lblPositionOffset.setAlignment(SWT.RIGHT);
		GridData gd_lblPositionOffset = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_lblPositionOffset.widthHint = 120;
		lblPositionOffset.setLayoutData(gd_lblPositionOffset);
		lblPositionOffset.setText("Position (Y offset)");

		labelPositionSpinner = new Spinner(grpLabel, SWT.BORDER);
		labelPositionSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		labelPositionSpinner.addListener(SWT.Selection, e -> {
			layer.setLabelPositionOffset(labelPositionSpinner.getSelection());
			geographicViewService.refresh();
		});
		labelPositionSpinner.setMaximum(1000);

		// disable other scales
		btnHideOtherScales = new Button(this, SWT.NONE);
		btnHideOtherScales.setText("Disable other scales");
		btnHideOtherScales.addListener(SWT.Selection, event -> {
			// disable other ColorScaleLayer
			fileLayerStoreModel.getLayers(IWWColorScaleLayer.class)//
					.filter(scaleLayer -> scaleLayer != layer)//
					.forEach(scaleLayer -> scaleLayer.setEnabled(false));
		});
	}

	/**
	 * Sets the {@link ColorScaleLayer} related to parameters.
	 */
	public void setLayer(IWWColorScaleLayer colorScaleLayer) {
		this.layer = colorScaleLayer;
		viewer.setSelection(new StructuredSelection(PositionParameters.fromKey(layer.getPosition())));
		spinnerX.setSelection(layer.getLocationOffset() != null ? (int) layer.getLocationOffset().x : 0);
		spinnerY.setSelection(layer.getLocationOffset() != null ? (int) layer.getLocationOffset().y : 0);
		setEnabled(colorScaleLayer.isEnabled());
		btnDisplayLabel.setSelection(layer.isLabelEnabled());
		labelPositionSpinner.setSelection(layer.getLabelPositionOffset());
	}

	/**
	 * @return the {@link ColorScaleLayer} related to parameters.
	 */
	public IWWColorScaleLayer getLayer() {
		return this.layer;
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		spinnerX.setEnabled(enabled);
		spinnerY.setEnabled(enabled);
		viewer.getCombo().setEnabled(enabled);
		btnDisplayLabel.setEnabled(enabled);
		labelPositionSpinner.setEnabled(enabled);
		btnHideOtherScales.setEnabled(enabled);
	}
}
