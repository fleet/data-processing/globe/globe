/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.colorscale.parametersview;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.layers.colorscale.ColorScaleLayer;

/**
 * Make a composite to edit the {@link ColorScaleLayer} parameters.
 */
@Component(name = "globe_viewer3d_colorscale_layer_param_composite_factory", service = IParametersViewCompositeFactory.class)
public class ColorScaleLayerParamCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof ColorScaleLayer
				? Optional.of(new ColorScaleParametersComposite(parent, (ColorScaleLayer) selection))
				: Optional.empty();
	}
}
