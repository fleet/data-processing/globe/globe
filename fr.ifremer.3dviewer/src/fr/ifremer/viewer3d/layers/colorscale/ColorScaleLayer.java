package fr.ifremer.viewer3d.layers.colorscale;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.function.Supplier;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.utils.map.TypedMap;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.render.TextRenderer;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.OGLStackHandler;
import gov.nasa.worldwind.util.OGLTextRenderer;

public class ColorScaleLayer extends AbstractLayer implements IWWColorScaleLayer {
	// Units constants
	public static final String UNIT_METRIC = "gov.nasa.worldwind.ScalebarLayer.Metric";
	public static final String UNIT_IMPERIAL = "gov.nasa.worldwind.ScalebarLayer.Imperial";
	public static final String UNIT_NAUTICAL = "gov.nasa.worldwind.ScalebarLayer.Nautical";

	// Display parameters
	private Dimension size = new Dimension(300, 20);
	private Color color = Color.white;
	private int borderWidth = 20;
	private String position = AVKey.SOUTHEAST;
	private String resizeBehavior = AVKey.RESIZE_SHRINK_ONLY;
	private String unit = UNIT_METRIC;
	private Font defaultFont = Font.decode("Arial-PLAIN-12");
	private double toViewportScale = 0.2;

	private Vec4 locationCenter = null;
	private Vec4 locationOffset = new Vec4(-170, 0, 0, 0);
	private double pixelSize;
	private String name;
	private boolean isLabelEnabled = true;
	private int labelPositionOffset = 90;

	// Draw it as ordered with an eye distance of 0 so that it shows up in front of most other things.
	// TODO: Add general support for this common pattern.
	private OrderedIcon orderedImage = new OrderedIcon();
	private Supplier<ColorContrastModel> source;

	private String scaleLabel = "";

	private class OrderedIcon implements OrderedRenderable {
		@Override
		public double getDistanceFromEye() {
			return 0;
		}

		@Override
		public void pick(DrawContext dc, Point pickPoint) {
			draw(dc);
		}

		@Override
		public void render(DrawContext dc) {
			draw(dc);
		}
	}

	/** Constructor **/
	public ColorScaleLayer(String name, Supplier<ColorContrastModel> source, String label) {
		this.name = name;
		this.source = source;
		this.scaleLabel = label;
		setPickEnabled(false);
		setEnabled(false);
	}

	@Override
	public void dispose() {
		super.dispose();
		orderedImage = null;
		// PropertyChangeSupport has a reference of this instance
		clearList();
	}

	// Public properties

	/**
	 * Get the apparent pixel size in meter at the reference position.
	 *
	 * @return the apparent pixel size in meter at the reference position.
	 */
	public double getPixelSize() {
		return pixelSize;
	}

	/**
	 * Get the scalebar graphic Dimension (in pixels)
	 *
	 * @return the scalebar graphic Dimension
	 */
	public Dimension getSize() {
		return size;
	}

	/**
	 * Set the scalebar graphic Dimenion (in pixels)
	 *
	 * @param size the scalebar graphic Dimension
	 */
	public void setSize(Dimension size) {
		if (size == null) {
			String message = Logging.getMessage("nullValue.DimensionIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}
		this.size = size;
	}

	/**
	 * Get the scalebar color
	 *
	 * @return the scalebar Color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Set the scalbar Color
	 *
	 * @param color the scalebar Color
	 */
	public void setColor(Color color) {
		if (color == null) {
			String msg = Logging.getMessage("nullValue.ColorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}
		this.color = color;
	}

	/**
	 * Returns the scalebar-to-viewport scale factor.
	 *
	 * @return the scalebar-to-viewport scale factor
	 */
	public double getToViewportScale() {
		return toViewportScale;
	}

	/**
	 * Sets the scale factor applied to the viewport size to determine the displayed size of the scalebar. This scale
	 * factor is used only when the layer's resize behavior is AVKey.RESIZE_STRETCH or AVKey.RESIZE_SHRINK_ONLY. The
	 * scalebar's width is adjusted to occupy the proportion of the viewport's width indicated by this factor. The
	 * scalebar's height is adjusted to maintain the scalebar's Dimension aspect ratio.
	 *
	 * @param toViewportScale the scalebar to viewport scale factor
	 */
	public void setToViewportScale(double toViewportScale) {
		this.toViewportScale = toViewportScale;
	}

	@Override
	public String getPosition() {
		return position;
	}

	@Override
	public void setPosition(String position) {
		if (position == null) {
			String msg = Logging.getMessage("nullValue.PositionIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}
		this.position = position;
	}

	/**
	 * Returns the current scalebar center location.
	 *
	 * @return the current location center. May be null.
	 */
	public Vec4 getLocationCenter() {
		return locationCenter;
	}

	/**
	 * Specifies the screen location of the scalebar center. May be null. If this value is non-null, it overrides the
	 * position specified by #setPosition. The location is specified in pixels. The origin is the window's lower left
	 * corner. Positive X values are to the right of the origin, positive Y values are upwards from the origin. The
	 * final scalebar location will be affected by the currently specified location offset if a non-null location offset
	 * has been specified (see #setLocationOffset).
	 *
	 * @param locationCenter the scalebar center. May be null.
	 *
	 * @see #setPosition
	 * @see #setLocationOffset
	 */
	public void setLocationCenter(Vec4 locationCenter) {
		this.locationCenter = locationCenter;
	}

	@Override
	public Vec4 getLocationOffset() {
		return locationOffset;
	}

	@Override
	public void setLocationOffset(Vec4 locationOffset) {
		this.locationOffset = locationOffset;
	}

	/**
	 * Returns the layer's resize behavior.
	 *
	 * @return the layer's resize behavior
	 */
	public String getResizeBehavior() {
		return resizeBehavior;
	}

	/**
	 * Sets the behavior the layer uses to size the scalebar when the viewport size changes, typically when the World
	 * Wind window is resized. If the value is AVKey.RESIZE_KEEP_FIXED_SIZE, the scalebar size is kept to the size
	 * specified in its Dimension scaled by the layer's current icon scale. If the value is AVKey.RESIZE_STRETCH, the
	 * scalebar is resized to have a constant size relative to the current viewport size. If the viewport shrinks the
	 * scalebar size decreases; if it expands then the scalebar enlarges. If the value is AVKey.RESIZE_SHRINK_ONLY (the
	 * default), scalebar sizing behaves as for AVKey.RESIZE_STRETCH but it will not grow larger than the size specified
	 * in its Dimension.
	 *
	 * @param resizeBehavior the desired resize behavior
	 */
	public void setResizeBehavior(String resizeBehavior) {
		this.resizeBehavior = resizeBehavior;
	}

	public int getBorderWidth() {
		return borderWidth;
	}

	/**
	 * Sets the scalebar offset from the viewport border.
	 *
	 * @param borderWidth the number of pixels to offset the scalebar from the borders indicated by
	 *            {@link #setPosition(String)}.
	 */
	public void setBorderWidth(int borderWidth) {
		this.borderWidth = borderWidth;
	}

	public String getUnit() {
		return unit;
	}

	/**
	 * Sets the unit the scalebar uses to display distances. Can be one of {@link #UNIT_METRIC} (the default), or
	 * {@link #UNIT_IMPERIAL}.
	 *
	 * @param unit the desired unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Get the scalebar legend Fon
	 *
	 * @return the scalebar legend Font
	 */
	public Font getFont() {
		return defaultFont;
	}

	/**
	 * Set the scalebar legend Fon
	 *
	 * @param font the scalebar legend Font
	 */
	public void setFont(Font font) {
		if (font == null) {
			String msg = Logging.getMessage("nullValue.FontIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}
		defaultFont = font;
	}

	// Rendering
	@Override
	public void doRender(DrawContext dc) {
		dc.addOrderedRenderable(orderedImage);
	}

	@Override
	public void doPick(DrawContext dc, Point pickPoint) {
		// Delegate drawing to the ordered renderable list
		dc.addOrderedRenderable(orderedImage);
	}

	// Rendering
	public void draw(DrawContext dc) {
		var colorContrastModel = source.get();
		GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.

		OGLStackHandler ogsh = new OGLStackHandler();

		try {
			ogsh.pushAttrib(gl, GL2.GL_TRANSFORM_BIT);

			gl.glDisable(GL.GL_DEPTH_TEST);

			double width = size.width;
			double height = size.height;

			// Load a parallel projection with xy dimensions (viewportWidth, viewportHeight)
			// into the GL projection matrix.
			java.awt.Rectangle viewport = dc.getView().getViewport();
			ogsh.pushProjectionIdentity(gl);
			double maxwh = width > height ? width : height;
			gl.glOrtho(0d, viewport.width, 0d, viewport.height, -0.6 * maxwh, 0.6 * maxwh);

			ogsh.pushModelviewIdentity(gl);

			// Scale to a width x height space
			// located at the proper position on screen
			double scale = computeScale(viewport);
			Vec4 locationSW = computeLocation(viewport, scale);
			gl.glTranslated(locationSW.x(), locationSW.y(), locationSW.z());
			gl.glScaled(scale, scale, 1);
			if (!dc.isPickingMode()) {
				gl.glEnable(GL.GL_BLEND);
				gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
				// Set color using current layer opacity
				Color backColor = getBackgroundColor(color);

				// ColorMap
				// gl.glLineWidth(0.0f);
				// gl.glLineWidth(1f);

				gl.glBegin(GL2.GL_QUADS);
				// gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
				// gl.glBegin(GL2.GL_LINES);
				float i_f = 0;
				float[][] colors = ColorMap.getColormap(colorContrastModel.colorMapIndex());
				int maxQuad = 255;
				for (int i = 0; i < maxQuad; i++) {
					i_f = i / (float) maxQuad;
					int index = i;
					double quadLength = width / maxQuad;
					if (colorContrastModel.invertColor()) {
						index = 255 - i;
					}
					gl.glColor4d(colors[ColorMap.red][index], colors[ColorMap.green][index],
							colors[ColorMap.blue][index], getOpacity());
					gl.glVertex3d(i_f * width, 0, getOpacity());
					gl.glVertex3d(i_f * width, height, getOpacity());
					gl.glVertex3d(i_f * width + quadLength, height, getOpacity());
					gl.glVertex3d(i_f * width + quadLength, 0, getOpacity());
				}
				gl.glEnd();

				float[] colorRGB = backColor.getRGBColorComponents(null);
				gl.glColor4d(colorRGB[0], colorRGB[1], colorRGB[2], getOpacity());

				// Border
				gl.glLineWidth(1.5f);
				gl.glBegin(GL.GL_LINE_STRIP);
				gl.glColor4d(1.0f, 1.0f, 1.0f, getOpacity());
				gl.glVertex3d(0, 0, 0);
				gl.glVertex3d(0, height, 0);
				gl.glVertex3d(width, height, 0);
				gl.glVertex3d(width, 0, 0);
				gl.glVertex3d(0, 0, 0);
				gl.glEnd();

				// Draw label
				gl.glLoadIdentity();
				gl.glDisable(GL.GL_CULL_FACE);

				// max label
				String label = String.format("%.2f ", colorContrastModel.contrastMax());
				drawLabel(dc, label, locationSW.add3(new Vec4(width * scale, -0.75 * height, 0)));

				// min label
				label = String.format("%.2f ", colorContrastModel.contrastMin());
				drawLabel(dc, label, locationSW.add3(new Vec4(0, -0.75 * height, 0)));

				// middle label
				label = String.format("%.2f ",
						(colorContrastModel.contrastMax() + colorContrastModel.contrastMin()) / 2.);
				drawLabel(dc, label, locationSW.add3(new Vec4(width * scale / 2., -0.75 * height, 0)));

				// title
				if (isLabelEnabled)
					drawLabel(dc, scaleLabel,
							locationSW.add3(new Vec4(width * scale / 2, labelPositionOffset / 100f * height, 0)));
			} else {
				// no picking

			}
		} finally {
			gl.glColor4d(1d, 1d, 1d, 1d); // restore the default OpenGL color
			gl.glEnable(GL.GL_DEPTH_TEST);

			if (!dc.isPickingMode()) {
				gl.glBlendFunc(GL.GL_ONE, GL.GL_ZERO); // restore to default blend function
				gl.glDisable(GL.GL_BLEND); // restore to default blend state
			}

			ogsh.pop(gl);
		}
	}

	// Draw the scale label
	@SuppressWarnings("unused")
	private void drawLabel(DrawContext dc, String text, Vec4 screenPoint) {
		TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(dc.getTextRendererCache(), defaultFont);

		Rectangle2D nameBound = textRenderer.getBounds(text);
		int x = (int) (screenPoint.x() - nameBound.getWidth() / 2d);
		int y = (int) screenPoint.y();

		textRenderer.begin3DRendering();
		Color backColor = getBackgroundColor(color);
		Color color = new Color(backColor.getRed(), backColor.getGreen(), backColor.getBlue(),
				(int) (255 * getOpacity()));
		textRenderer.setColor(backColor);
		textRenderer.draw(text, x + 1, y - 1);
		textRenderer.setColor(this.color);
		textRenderer.draw(text, x, y);

		textRenderer.end3DRendering();
	}

	private final float[] compArray = new float[4];

	// Compute background color for best contrast
	private Color getBackgroundColor(Color color) {
		Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), compArray);
		if (compArray[2] > 0.5)
			return new Color(0, 0, 0, 0.7f);
		else
			return new Color(1, 1, 1, 0.7f);
	}

	private double computeScale(java.awt.Rectangle viewport) {
		if (resizeBehavior.equals(AVKey.RESIZE_SHRINK_ONLY)) {
			return Math.min(1d, toViewportScale * viewport.width / size.width);
		} else if (resizeBehavior.equals(AVKey.RESIZE_STRETCH)) {
			return toViewportScale * viewport.width / size.width;
		} else if (resizeBehavior.equals(AVKey.RESIZE_KEEP_FIXED_SIZE)) {
			return 1d;
		} else {
			return 1d;
		}
	}

	private Vec4 computeLocation(java.awt.Rectangle viewport, double scale) {
		double scaledWidth = scale * size.width;
		double scaledHeight = scale * size.height;

		double x;
		double y;

		if (locationCenter != null) {
			x = locationCenter.x - scaledWidth / 2;
			y = locationCenter.y - scaledHeight / 2;
		} else if (position.equals(AVKey.NORTHEAST)) {
			x = viewport.getWidth() - scaledWidth - borderWidth;
			y = viewport.getHeight() - scaledHeight - borderWidth;
		} else if (position.equals(AVKey.SOUTHEAST)) {
			x = viewport.getWidth() - scaledWidth - borderWidth;
			y = 0d + borderWidth;
		} else if (position.equals(AVKey.NORTHWEST)) {
			x = 0d + borderWidth;
			y = viewport.getHeight() - scaledHeight - borderWidth;
		} else if (position.equals(AVKey.SOUTHWEST)) {
			x = 0d + borderWidth;
			y = 0d + borderWidth;
		} else // use North East
		{
			x = viewport.getWidth() - scaledWidth / 2 - borderWidth;
			y = viewport.getHeight() - scaledHeight / 2 - borderWidth;
		}

		if (locationOffset != null) {
			x += locationOffset.x;
			y += locationOffset.y;
		}

		return new Vec4(x, y, 0);
	}

	@Override
	public String toString() {
		return getName();
	}

	/**
	 * @return the {@link #name}
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name the {@link #name} to set
	 */
	@Override
	public void setName(String name) {
		super.setName(name);
		this.name = name;
	}

	public Supplier<ColorContrastModel> getSource() {
		return source;
	}

	@Override
	public boolean isLabelEnabled() {
		return isLabelEnabled;
	}

	@Override
	public void setLabelEnabled(boolean isLabelEnabled) {
		this.isLabelEnabled = isLabelEnabled;
	}

	@Override
	public int getLabelPositionOffset() {
		return labelPositionOffset;
	}

	@Override
	public void setLabelPositionOffset(int labelPositionOffset) {
		this.labelPositionOffset = labelPositionOffset;
	}

	/**
	 * @return the {@link #scaleLabel}
	 */
	@Override
	public String getScaleLabel() {
		return scaleLabel;
	}

	/**
	 * @param scaleLabel the {@link #scaleLabel} to set
	 */
	@Override
	public void setScaleLabel(String scaleLabel) {
		this.scaleLabel = scaleLabel;
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(ColorScaleParameters.SESSION_KEY.bindValue(new ColorScaleParameters(isEnabled(),
				getOpacity(), isLabelEnabled, position, locationOffset, labelPositionOffset, scaleLabel)));
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(ColorScaleParameters.SESSION_KEY).ifPresent(sessionParameters -> {
			isLabelEnabled = sessionParameters.labelEnabled;
			position = sessionParameters.position;
			locationOffset = sessionParameters.locationOffset;
			labelPositionOffset = sessionParameters.labelPositionOffset;
			scaleLabel = sessionParameters.scaleLabel;
			setOpacity(sessionParameters.opacity);
			setEnabled(sessionParameters.enabled);
		});
	}

}