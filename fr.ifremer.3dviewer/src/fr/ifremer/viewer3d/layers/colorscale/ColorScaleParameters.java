/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.colorscale;

import fr.ifremer.globe.utils.map.TypedKey;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Parameter of ColorScaleLayer
 */
public class ColorScaleParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<ColorScaleParameters> SESSION_KEY = new TypedKey<>("Color_scale_parameters");

	/** Layer enabled flag. */
	public final boolean enabled;

	/** Opacity. */
	public final double opacity;

	/** Label is displayed. */
	public final boolean labelEnabled;

	/** The relative viewport location to display the scalebar. */
	public final String position;

	/** The current location offset. */
	public final Vec4 locationOffset;

	/** The offset of label position. */
	public final int labelPositionOffset;

	/** The label */
	public final String scaleLabel;

	/**
	 * Constructor
	 */
	public ColorScaleParameters(boolean enabled, double opacity, boolean labelEnabled, String position,
			Vec4 locationOffset, int labelPositionOffset, String scaleLabel) {
		this.enabled = enabled;
		this.opacity = opacity;
		this.labelEnabled = labelEnabled;
		this.position = position;
		this.locationOffset = locationOffset;
		this.labelPositionOffset = labelPositionOffset;
		this.scaleLabel = scaleLabel;
	}

}
