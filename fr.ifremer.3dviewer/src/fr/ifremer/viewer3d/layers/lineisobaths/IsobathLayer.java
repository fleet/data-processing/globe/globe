/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lineisobaths;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;

import jakarta.inject.Inject;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.swt.widgets.Display;
import org.gdal.ogr.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWIsobathLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.DoubleIndexedSupplier;
import fr.ifremer.globe.utils.function.IntIndexedSupplier;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathGenerator;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathLayerFiller;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathLayerFiller.LinePropertiesOfLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathTopics;
import fr.ifremer.viewer3d.layers.lines.ILineProperties;
import fr.ifremer.viewer3d.layers.lines.LinesLayer;
import fr.ifremer.viewer3d.layers.lines.impl.LineProperties;
import fr.ifremer.viewer3d.util.VBO;

/**
 * Layer to render isobaths.
 */
public class IsobathLayer extends LinesLayer implements IWWIsobathLayer, IProcessFunction {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(IsobathLayer.class);

	/** Parameters */
	private final IsobathLayerFiller isobathLayerFiller;
	protected IsobathParameters isobathParameters;
	protected final IChangeListener parametersListener = this::onParametersChanged;

	private File demFile;
	private File shpContourFile;
	private Supplier<File> demFileSupplier;

	private LinePropertiesOfLayer linePropertiesOfLayer;

	/** All managed main lines. */
	protected List<LineProperties> mainLines = new ArrayList<>();

	/** All managed secondary lines. */
	protected List<LineProperties> otherLines = new ArrayList<>();

	/** Event broker service */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected IEventBroker eventBroker;

	/**
	 * Constructor.
	 */
	public IsobathLayer(String name, GeoBox geoBox, Supplier<File> demFileSupplier,
			IsobathLayerFiller isobathLayerFiller) {
		super(name, geoBox);
		this.demFileSupplier = demFileSupplier;
		this.isobathLayerFiller = isobathLayerFiller;
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
	}

	/**
	 * Constructor.
	 */
	public IsobathLayer(String name, GeoBox geoBox, File demFile, IsobathLayerFiller isobathLayerFiller) {
		super(name, geoBox);
		this.demFile = demFile;
		this.isobathLayerFiller = isobathLayerFiller;
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.of(newName -> {
			var result = new IsobathLayer(newName, GeoBoxUtils.convert(sector), demFile, isobathLayerFiller);
			result.demFileSupplier = demFileSupplier;
			var param = new IsobathParameters();
			param.load(isobathParameters);
			result.setParameters(param);
			return result;
		});
	}

	/** Sets {@link IsobathParameters} */
	public void setParameters(IsobathParameters isobathParameters) {
		if (this.isobathParameters != null) {
			dispose();
		}

		this.isobathParameters = isobathParameters;
		Display.getDefault().asyncExec(() -> {
			isobathParameters.getLineWidth().addChangeListener(parametersListener);
			isobathParameters.getMainLineColor().addChangeListener(parametersListener);
			isobathParameters.getMainLineVisible().addChangeListener(parametersListener);
			isobathParameters.getOtherLineUnicolor().addChangeListener(parametersListener);
			isobathParameters.getOtherLineColors().addChangeListener(parametersListener);
			isobathParameters.getOtherLineVisible().addChangeListener(parametersListener);
		});
	}

	public void compute() {
		IProcessService.grab().runInForeground("Isobath computing with parameters : " + isobathParameters, this);
	}

	/**
	 * Computes isobath lines from the data elevation model (DEM file).
	 *
	 * @throws GIOException
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Compute isobath...", 100);
		removeAllLines(); // first: remove previous lines
		org.gdal.ogr.Layer gdalLayer = null;
		DataSource isobathsDatasource = null;
		try {
			// Get DEM file (data elevation model = .tiff)
			if (demFile == null) {
				subMonitor.setTaskName("Generating data elevation model...");
				demFile = demFileSupplier.get();
			}
			if (demFile == null) {
				throw new GIOException(
						"Error with " + getName() + " isobaths generation : DEM file not found (.tiff).");
			}

			// Generate isobath contour file (.shp)
			subMonitor.setTaskName("Generating contour shape file for " + this.getName() + "...");
			File isobathsFile = new File(TemporaryCache.getRootPath(), getClass().getSimpleName());

			String layerName = String.valueOf(System.currentTimeMillis() + Math.random() * 100);
			isobathsDatasource = new IsobathGenerator().generate(demFile, null, 0,
					isobathParameters.getStep().floatValue(), isobathsFile, layerName, subMonitor.split(50));
			gdalLayer = isobathsDatasource.GetLayer(layerName);
			shpContourFile = new File(isobathsFile, layerName + ".shp");

			// Creates lines and get linePropertiesOfLayer
			subMonitor.setTaskName("Creating isobath lines for " + this.getName() + "...");
			linePropertiesOfLayer = isobathLayerFiller
					.createLines(Arrays.asList(gdalLayer), this, 0, subMonitor.split(50)).get(gdalLayer);

			IGeographicViewService.grab().moveLayerToFront(this);
			ISessionService.grab().saveSession();
			if (eventBroker != null) {
				eventBroker.post(IsobathTopics.TOPIC_ISOBATH_UPDATED, isobathParameters);
			}
			return Status.OK_STATUS;

		} catch (IOException e) {
			throw new GIOException("Error with " + getName() + " isobaths generation : " + e.getMessage(), e);
		} finally {
			if (gdalLayer != null) {
				gdalLayer.delete();
			}
			if (isobathsDatasource != null) {
				isobathsDatasource.delete();
			}
		}
	}

	/**
	 * Add main lines
	 *
	 * @return the pojo representing the lines or null if the lines was rejected
	 */
	public ILineProperties acceptMainLines(int pointCount, DoubleIndexedSupplier xSupplier,
			DoubleIndexedSupplier ySupplier, DoubleIndexedSupplier zSupplier, int lineCount,
			IntIndexedSupplier firstPointIndex) {
		LineProperties result = (LineProperties) super.acceptLine(pointCount, xSupplier, ySupplier, zSupplier,
				getMainLineColor(), getLineWidth());
		if (result != null) {
			split(result, lineCount, firstPointIndex, i -> getMainLineColor(), getLineWidth());
			mainLines.add(result);
			result.setVisible(isobathParameters.getMainLineVisible().get());
		}
		return result;
	}

	/**
	 * Add secondary lines
	 *
	 * @return the pojo representing the lines or null if the lines was rejected
	 */
	public ILineProperties acceptSecondaryLines(int pointCount, DoubleIndexedSupplier xSupplier,
			DoubleIndexedSupplier ySupplier, DoubleIndexedSupplier zSupplier, int lineCount,
			IntIndexedSupplier linePointCountSupplier, DoubleIndexedSupplier elevationSupplier) {

		LineProperties result = (LineProperties) super.acceptLine(pointCount, xSupplier, ySupplier, zSupplier,
				new GColor(0, 0, 0), getLineWidth());

		if (result != null) {
			split(result, lineCount, linePointCountSupplier, i -> getOtherLineColor(elevationSupplier.getAsDouble(i)),
					getLineWidth());
			otherLines.add(result);
			result.setVisible(isobathParameters.getOtherLineVisible().get());
		}

		return result;
	}

	/**
	 * Split lines
	 */
	protected void split(ILineProperties result, int lineCount, IntIndexedSupplier linePointCountSupplier,
			IntFunction<GColor> colorSupplier, float width) {
		int firstIndex = 0;
		for (int line = 0; line < lineCount; line++) {
			int linePointCount = linePointCountSupplier.getAsInteger(line);
			int lastIndex = firstIndex + linePointCount - 1; // last index included
			result.split(firstIndex, lastIndex, colorSupplier.apply(line), width);
			firstIndex = firstIndex + linePointCount;
		}
	}

	/** One parameter has changed */
	protected void onParametersChanged(ChangeEvent e) {
		mainLines.forEach(mainLine -> {
			mainLine.setColor(isobathParameters.getMainLineColor().get());
			mainLine.setWidth(isobathParameters.getLineWidth().floatValue());
			mainLine.setVisible(isobathParameters.getMainLineVisible().isTrue());
		});
		otherLines.forEach(otherLine -> {
			otherLine.setWidth(isobathParameters.getLineWidth().floatValue());
			otherLine.setVisible(isobathParameters.getOtherLineVisible().isTrue());
			if (isobathParameters.getIsOtherLineMultiColor().isFalse()) {
				otherLine.setColor(isobathParameters.getOtherLineUnicolor().get());
			}
		});
		IGeographicViewService.grab().refresh();
		ISessionService.grab().saveSession();
		if (eventBroker != null) {
			eventBroker.post(IsobathTopics.TOPIC_ISOBATH_UPDATED, isobathParameters);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected LineProperties newLineProperties(VBO vbo, int firstIndex, int lastIndex, GColor color, float width) {
		return new LineProperties(vbo, firstIndex, lastIndex, color, width);
	}

	protected GColor getMainLineColor() {
		return isobathParameters.getMainLineColor().get();
	}

	protected GColor getOtherLineColor(double elevation) {
		boolean isMulticolor = isobathParameters.getIsOtherLineMultiColor().get();

		if (isMulticolor) {
			int index = (int) Math.round(-elevation / isobathParameters.getStep().doubleValue()
					% isobathParameters.getMainLine().intValue());
			index = (index - 1) % 10;
			return isobathParameters.getOtherLineColors().asList().get(index);
		}

		return isobathParameters.getOtherLineUnicolor().get();
	}

	protected float getLineWidth() {
		return isobathParameters.getLineWidth().floatValue();
	}

	/** Getter of {@link #isobathParameters} */
	public IsobathParameters getIsobathParameters() {
		return isobathParameters;
	}

	/** Getter of {@link #linePropertiesOfLayer} */
	public LinePropertiesOfLayer getLinePropertiesOfLayer() {
		return linePropertiesOfLayer;
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		super.dispose();
		if (isobathParameters != null) {
			isobathParameters.getLineWidth().removeChangeListener(parametersListener);
			isobathParameters.getMainLineColor().removeChangeListener(parametersListener);
			isobathParameters.getMainLineVisible().removeChangeListener(parametersListener);
			isobathParameters.getOtherLineUnicolor().removeChangeListener(parametersListener);
			isobathParameters.getOtherLineColors().removeChangeListener(parametersListener);
			isobathParameters.getOtherLineVisible().removeChangeListener(parametersListener);
			isobathParameters.dispose();
		}
		mainLines = new ArrayList<>();
		otherLines = new ArrayList<>();
	}

	/**
	 * @return the {@link #demFile}
	 */
	public File getDemFile() {
		return demFile;
	}

	/** @return the temporary shape file computed by GDAL contour. **/
	public File getShpContourFile() {
		return shpContourFile;
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of((isobathParameters != null ? isobathParameters : new IsobathParameters()).toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(IsobathParameters.SESSION_KEY)
				.ifPresent(sessionParameters -> setParameters(new IsobathParameters(sessionParameters)));
	}

}
