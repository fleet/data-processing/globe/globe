/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lineisobaths.tools;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.ogr.DataSource;
import org.gdal.osr.SpatialReference;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathConstants;

/**
 * Generator of isobaths using gdal contour tools.
 */
public class IsobathGenerator {

	/**
	 * Constructor.
	 */
	public IsobathGenerator() {
		super();
	}

	/**
	 * Generates a vector contour file (isobathFolder) from the input raster elevation model (demFile).
	 *
	 * @param demFile input raster
	 * @param spatialReference the coordinate system to use for the layer. Assumed SRS_WKT_WGS84 if null
	 * @param contourBase One main contour elevation
	 * @param step elevation interval between contours
	 * @param isobathFolder resulting folder
	 * @param layerName resulting file name
	 * @return gdal datasource containing all contour geometries
	 */
	public DataSource generate(File demFile, SpatialReference spatialReference, float contourBase, double step,
			File isobathFolder, String layerName, IProgressMonitor monitor) throws IOException {
		DataSource dataSource = null;
		if (demFile.exists()) {
			SubMonitor subMonitor = SubMonitor.convert(monitor, 1000);
			Dataset inputDataset = gdal.Open(demFile.getAbsolutePath());
			if (inputDataset != null) {
				Band band = inputDataset.GetRasterBand(1);
				try {
					dataSource = GdalUtils.generateContour(spatialReference, band, step, contourBase, layerName,
							IsobathConstants.ATTRIBUTE_NAME, isobathFolder.getAbsolutePath(),
							GdalUtils.newProgressCallback(subMonitor));
				} finally {
					band.delete();
					inputDataset.delete();
				}
			}
			subMonitor.done();
		}
		return dataSource;
	}

}
