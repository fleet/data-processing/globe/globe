/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lineisobaths.tools;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * All topics managed in the isobath layer through the IEventBroker
 */
public class IsobathTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/viewer3d/layers/isobaths";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to inform that isobath have been updated.<br>
	 */
	public static final String TOPIC_ISOBATH_UPDATED = TOPIC_BASE + UIEvents.TOPIC_SEP + "ISOBATH_UPDATED";

	/** Constructor */
	private IsobathTopics() {
		// private constructor to hide the implicit public one.
	}

}
