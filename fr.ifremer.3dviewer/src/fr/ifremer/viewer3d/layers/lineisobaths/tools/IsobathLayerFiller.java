/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lineisobaths.tools;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.IntStream;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.ogr.Feature;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;

import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.function.DoubleIndexedConsumer;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathConstants;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;
import fr.ifremer.viewer3d.layers.lines.ILineProperties;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;

/**
 * Fill isobaths to IsobathLayer with lines contained in a DataSource.
 */
public class IsobathLayerFiller {

	/** Factory of {@link IArray}. */
	protected IArrayFactory arrayFactory;

	private final boolean fillValuesFor3DViewer;

	/**
	 * Constructor.
	 */
	public IsobathLayerFiller(IArrayFactory arrayFactory, boolean fillValuesFor3DViewer) {
		this.arrayFactory = arrayFactory;
		this.fillValuesFor3DViewer = fillValuesFor3DViewer;
	}

	/**
	 * Prepare the lines production by computing the necessary properties
	 * 
	 * @param layers containing all contour geometries
	 * @param contourBase One main contour elevation
	 * @param step elevation interval in meters between contours
	 * @param mainLineFrequency main line frequency
	 * @param sampling percent of point to keep
	 */
	private LinesProperties initializeLinesProperties(Collection<Layer> layers, float contourBase, double step,
			int mainLineFrequency, float sampling) {
		LinesProperties linesProperties = new LinesProperties(layers, contourBase, step, mainLineFrequency);
		for (Entry<Layer, LinePropertiesOfLayer> entry : linesProperties.getLinePropertiesOfLayers().entrySet()) {
			Layer layer = entry.getKey();
			LinePropertiesOfLayer linePropertiesOfLayer = entry.getValue();

			layer.ResetReading();
			Feature feature = null;
			while ((feature = layer.GetNextFeature()) != null) {
				Geometry line = feature.GetGeometryRef();
				int pointCount = line.GetPointCount();
				// Line too small are ignored
				if (pointCount >= IsobathConstants.MIN_POINTS_PER_LINE) {
					if (isMainLine(feature.GetFieldAsDouble(IsobathConstants.ATTRIBUTE_NAME), linesProperties)) {
						linePropertiesOfLayer.addOneMainLine(pointCount);
					} else {
						double elevation = feature.GetFieldAsDouble(IsobathConstants.ATTRIBUTE_NAME);
						linePropertiesOfLayer.addOneOtherLine(pointCount, elevation);
					}
				}
			}
		}

		linesProperties.applySampling(sampling);
		return linesProperties;
	}

	/**
	 * Update the specified layer according the lines properties
	 */
	public Map<Layer, LinePropertiesOfLayer> createLines(Collection<Layer> layers, IsobathLayer isobathLayer,
			int contourBase, IProgressMonitor monitor) throws IOException {

		IsobathParameters params = isobathLayer.getIsobathParameters();
		LinesProperties linesProperties = initializeLinesProperties(layers, contourBase, params.getStep().doubleValue(),
				params.getMainLine().intValue(), params.getSampling().floatValue() / 100f);

		SubMonitor subMonitor = SubMonitor.convert(monitor, 3);

		IDoubleArray mainLineXYZ = arrayFactory.makeDoubleArray(linesProperties.getTotalMainLinePointCount() * 3l);
		IDoubleArray otherLineXYZ = arrayFactory.makeDoubleArray(linesProperties.getTotalOtherLinePointCount() * 3l);

		try {

			extractPoints(linesProperties, mainLineXYZ, otherLineXYZ, subMonitor.split(1));

			for (Entry<Layer, LinePropertiesOfLayer> entry : linesProperties.getLinePropertiesOfLayers().entrySet()) {
				LinePropertiesOfLayer linePropertiesOfLayer = entry.getValue();

				int otherLineIndex = linePropertiesOfLayer.getOtherLineIndexInBuffer();
				ILineProperties otherLineProperties = isobathLayer.acceptSecondaryLines(
						linePropertiesOfLayer.getTotalOtherLinePointCount(), //
						index -> otherLineXYZ.getDouble(otherLineIndex + (index * 3)), //
						index -> otherLineXYZ.getDouble(otherLineIndex + (index * 3 + 1)), //
						index -> otherLineXYZ.getDouble(otherLineIndex + (index * 3 + 2)), //
						linePropertiesOfLayer.getOtherLineCount(), //
						linePropertiesOfLayer::getOtherLinePointCount, //
						linePropertiesOfLayer::getOtherLineElevation);
				linePropertiesOfLayer.setOtherLineProperties(otherLineProperties);

				subMonitor.worked(1);

				int mainLineIndex = linePropertiesOfLayer.getMainLineIndexInBuffer();
				ILineProperties mainLineProperties = isobathLayer.acceptMainLines(
						linePropertiesOfLayer.getTotalMainLinePointCount(), //
						index -> mainLineXYZ.getDouble(mainLineIndex + (index * 3)), //
						index -> mainLineXYZ.getDouble(mainLineIndex + (index * 3 + 1)), //
						index -> mainLineXYZ.getDouble(mainLineIndex + (index * 3 + 2)), //
						linePropertiesOfLayer.getMainLineCount(), //
						linePropertiesOfLayer::getMainLinePointCount);
				linePropertiesOfLayer.setMainLineProperties(mainLineProperties);
			}
			subMonitor.done();
		} finally {
			IOUtils.closeQuietly(mainLineXYZ, otherLineXYZ);
			monitor.done();
		}
		return Collections.unmodifiableMap(linesProperties.linePropertiesOfLayers);
	}

	/** Extracts all point line from the layer and fill their coordinates to IDoubleArray */
	protected void extractPoints(LinesProperties linesProperties, IDoubleArray mainLineXYZ, IDoubleArray otherLineXYZ,
			IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, linesProperties.getLinePropertiesOfLayers().size());

		for (Entry<Layer, LinePropertiesOfLayer> entry : linesProperties.linePropertiesOfLayers.entrySet()) {
			LinePropertiesOfLayer linePropertiesOfLayer = entry.getValue();
			int mainLineIndex = linePropertiesOfLayer.getMainLineIndexInBuffer();
			int otherLineIndex = linePropertiesOfLayer.getOtherLineIndexInBuffer();

			extractPoints(linesProperties, //
					entry.getKey(), //
					linePropertiesOfLayer, //
					(index, value) -> mainLineXYZ.putDouble(mainLineIndex + index, value), //
					(index, value) -> otherLineXYZ.putDouble(otherLineIndex + index, value)//
					);

			subMonitor.worked(1);
		}
	}

	/** Extracts all line's points from the layer and fill their coordinates to consumers */
	protected void extractPoints(LinesProperties linesProperties, Layer layer,
			LinePropertiesOfLayer linePropertiesOfLayer, DoubleIndexedConsumer mainLineConsumer,
			DoubleIndexedConsumer otherLineConsumer) {
		int mainLineIndex = 0;
		int otherLineIndex = 0;
		int mainLineXyzIndex = 0;
		int otherLineXyzIndex = 0;

		double[] point = new double[2];
		layer.ResetReading();
		Feature feature = null;

		Optional<Globe> globe = fillValuesFor3DViewer ? Optional.of(Viewer3D.getWwd().getModel().getGlobe())
				: Optional.empty();
		double vertExa = fillValuesFor3DViewer ? Viewer3D.getWwd().getSceneController().getVerticalExaggeration() : 1;

		while ((feature = layer.GetNextFeature()) != null) {
			// Lines too small are ignored
			Geometry line = feature.GetGeometryRef();
			if (line.GetPointCount() >= IsobathConstants.MIN_POINTS_PER_LINE) {

				double elevation = feature.GetFieldAsDouble(IsobathConstants.ATTRIBUTE_NAME);
				boolean isMainLine = isMainLine(elevation, linesProperties);
				int expectedLinePointCount = 0;
				if (isMainLine) {
					expectedLinePointCount = linePropertiesOfLayer.getMainLinePointCount(mainLineIndex);
					mainLineIndex++;
				} else {
					expectedLinePointCount = linePropertiesOfLayer.getOtherLinePointCount(otherLineIndex);
					otherLineIndex++;
				}

				for (int i = 0; i < expectedLinePointCount; i++) {
					int pointIndex = Math.round((1f / linesProperties.getSampling()) * i);
					line.GetPoint_2D(pointIndex, point);

					// Projection to 3DViewer model if necessary
					Vec4 p = globe.isPresent()
							? globe.get().computePointFromPosition(Position.fromDegrees(point[1], point[0], vertExa*elevation))
									: new Vec4(point[0], point[1], elevation);
							//
							if (isMainLine) {
								mainLineConsumer.accept(mainLineXyzIndex++, p.x);
								mainLineConsumer.accept(mainLineXyzIndex++, p.y);
								mainLineConsumer.accept(mainLineXyzIndex++, p.z);
							} else {
								otherLineConsumer.accept(otherLineXyzIndex++, p.x);
								otherLineConsumer.accept(otherLineXyzIndex++, p.y);
								otherLineConsumer.accept(otherLineXyzIndex++, p.z);
							}
				}
			}
		}
	}
	
	double previousElevation = -1;

	/** @return true when elevation is good for a main line */
	protected boolean isMainLine(double elevation, LinesProperties linesProperties) {
		return Math.round((elevation - linesProperties.getContourBase()) / linesProperties.getStep())
				% linesProperties.getMainLineFrequency() == 0;
	}

	public static class LinesProperties {
		/** LinePropertiesOfLayer attach to a Layer */
		protected Map<Layer, LinePropertiesOfLayer> linePropertiesOfLayers = new LinkedHashMap<>();

		/** One main contour elevation */
		protected float contourBase;
		/** elevation interval between contours */
		protected double step;
		/** main line frequency */
		protected int mainLineFrequency;
		/** % of kept point from original isobaths computation */
		protected float sampling;

		/** Constructor */
		LinesProperties(Collection<Layer> layers, float contourBase, double step, int mainLineFrequency) {
			this.contourBase = contourBase;
			this.step = step;
			this.mainLineFrequency = mainLineFrequency;
			layers.forEach(layer -> linePropertiesOfLayers.put(layer, new LinePropertiesOfLayer()));
		}

		/**
		 * Sampling.
		 * 
		 * @param sampling % of kept point from original isobaths computation
		 */
		void applySampling(float sampling) {
			this.sampling = sampling;
			linePropertiesOfLayers.values()
			.forEach(linePropertiesOfLayer -> linePropertiesOfLayer.applySampling(this.sampling));
			// total point count can not exceed MAX_POINT_COUNT
			if (getTotalPointCount() > IsobathConstants.MAX_POINT_COUNT) {
				this.sampling = IsobathConstants.MAX_POINT_COUNT / (float) getTotalPointCount();
				linePropertiesOfLayers.values()
				.forEach(linePropertiesOfLayer -> linePropertiesOfLayer.applySampling(this.sampling));
			}

			int mainLineIndex = 0;
			int otherLineIndex = 0;
			for (Entry<Layer, LinePropertiesOfLayer> entry : linePropertiesOfLayers.entrySet()) {
				LinePropertiesOfLayer linePropertiesOfLayer = entry.getValue();
				linePropertiesOfLayer.setMainLineIndexInBuffer(mainLineIndex);
				mainLineIndex += linePropertiesOfLayer.getTotalMainLinePointCount() * 3;
				linePropertiesOfLayer.setOtherLineIndexInBuffer(otherLineIndex);
				otherLineIndex += linePropertiesOfLayer.getTotalOtherLinePointCount() * 3;
			}

		}

		/** Getter of {@link #totalMainLinePointCount} */
		int getTotalMainLinePointCount() {
			return linePropertiesOfLayers.values().stream().mapToInt(LinePropertiesOfLayer::getTotalMainLinePointCount)
					.sum();
		}

		/** Getter of {@link #totalOtherLinePointCount} */
		int getTotalOtherLinePointCount() {
			return linePropertiesOfLayers.values().stream().mapToInt(LinePropertiesOfLayer::getTotalOtherLinePointCount)
					.sum();
		}

		public int getTotalPointCount() {
			return getTotalOtherLinePointCount() + getTotalMainLinePointCount();
		}

		/** Getter of {@link #contourBase} */
		public float getContourBase() {
			return contourBase;
		}

		/** Getter of {@link #mainLineFrequency} */
		public int getMainLineFrequency() {
			return mainLineFrequency;
		}

		/** Getter of {@link #step} */
		public double getStep() {
			return step;
		}

		/** Getter of {@link #sampling} */
		float getSampling() {
			return sampling;
		}

		/** Getter of {@link #linePropertiesOfLayers} */
		Map<Layer, LinePropertiesOfLayer> getLinePropertiesOfLayers() {
			return linePropertiesOfLayers;
		}
	}

	/** ILineProperties produced for a layer */
	public static class LinePropertiesOfLayer {
		/** Index of first point of each main lines */
		protected IntArrayList mainLinePointCounts = new IntArrayList();
		/** Index of first point of each secondary lines */
		protected IntArrayList otherLinePointCounts = new IntArrayList();
		/** Index of first point of each secondary lines */
		protected DoubleArrayList otherLineElevations = new DoubleArrayList();

		/** Main lines of the profile */
		protected ILineProperties mainLineProperties;
		/** Secondary lines of the profile */
		protected ILineProperties otherLineProperties;
		/** Main lines of the profile */
		protected int mainLineIndexInBuffer;
		/** Secondary lines of the profile */
		protected int otherLineIndexInBuffer;

		/** Constructor */
		LinePropertiesOfLayer() {
		}

		/** Sampling of the points statistics */
		public void applySampling(float sampling) {
			for (int i = 0; i < mainLinePointCounts.size(); i++) {
				int pointCount = (int) Math.floor(mainLinePointCounts.get(i) * sampling);
				mainLinePointCounts.set(i, pointCount);
			}
			for (int i = 0; i < otherLinePointCounts.size(); i++) {
				int pointCount = (int) Math.floor(otherLinePointCounts.get(i) * sampling);
				otherLinePointCounts.set(i, pointCount);
			}
		}

		/** Getter of {@link #mainLinePointCounts} */
		public int getMainLinePointCount(int index) {
			return mainLinePointCounts.getQuick(index);
		}

		/** Get the number of main lines */
		public int getMainLineCount() {
			return mainLinePointCounts.size();
		}

		/** Add property of an other main line */
		public void addOneMainLine(int pointCount) {
			mainLinePointCounts.add(pointCount);
		}

		/** Number of points in all main lines */
		public int getTotalMainLinePointCount() {
			return IntStream.of(mainLinePointCounts.elements()).sum();
		}

		/** Getter of {@link #mainLinePointCounts} */
		public IntArrayList getMainLinePointCounts() {
			return mainLinePointCounts;
		}

		/** Getter of {@link #otherLinePointCounts} */
		public int getOtherLinePointCount(int index) {
			return otherLinePointCounts.getQuick(index);
		}

		/** Getter of {@link #otherLinePointCounts} */
		public double getOtherLineElevation(int index) {
			return otherLineElevations.getQuick(index);
		}

		/** Get the number of secondary lines */
		public int getOtherLineCount() {
			return otherLinePointCounts.size();
		}

		/** Add property of an other secondary line */
		public void addOneOtherLine(int pointCount, double elevation) {
			otherLinePointCounts.add(pointCount);
			otherLineElevations.add(elevation);
		}

		/** Number of points in all secondary lines */
		public int getTotalOtherLinePointCount() {
			return IntStream.of(otherLinePointCounts.elements()).sum();
		}

		/** Getter of {@link #otherLinePointCounts} */
		public IntArrayList getOtherLinePointCounts() {
			return otherLinePointCounts;
		}

		/** Getter of {@link #mainLineProperties} */
		public ILineProperties getMainLineProperties() {
			return mainLineProperties;
		}

		/** Setter of {@link #mainLineProperties} */
		void setMainLineProperties(ILineProperties mainLineProperties) {
			this.mainLineProperties = mainLineProperties;
		}

		/** Getter of {@link #otherLineProperties} */
		public ILineProperties getOtherLineProperties() {
			return otherLineProperties;
		}

		/** Setter of {@link #otherLineProperties} */
		void setOtherLineProperties(ILineProperties otherLineProperties) {
			this.otherLineProperties = otherLineProperties;
		}

		/** Getter of {@link #otherLineIndexInBuffer} */
		int getOtherLineIndexInBuffer() {
			return otherLineIndexInBuffer;
		}

		/** Setter of {@link #otherLineIndexInBuffer} */
		void setOtherLineIndexInBuffer(int otherLineIndexInBuffer) {
			this.otherLineIndexInBuffer = otherLineIndexInBuffer;
		}

		/** Getter of {@link #mainLineIndexInBuffer} */
		int getMainLineIndexInBuffer() {
			return mainLineIndexInBuffer;
		}

		/** Setter of {@link #mainLineIndexInBuffer} */
		void setMainLineIndexInBuffer(int mainLineIndexInBuffer) {
			this.mainLineIndexInBuffer = mainLineIndexInBuffer;
		}

	}

}
