/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lineisobaths;

/**
 * Some usefull constants.
 */
public class IsobathConstants {

	/** Name of the layer in the generated DataSource */
	public static final String LAYER_NAME = "Isobaths";
	/** Name of the elevation attribute in the generated DataSource */
	public static final String ATTRIBUTE_NAME = "elevation";

	/** Max number of point accepted by the isobath layer */
	public static final int MAX_POINT_COUNT = 100_000_000;

	/** Min number of point per line.  */
	public static final int MIN_POINTS_PER_LINE = 2;

	/**
	 * Constructor
	 */
	private IsobathConstants() {
		// not meant to be instantiated
	}

}
