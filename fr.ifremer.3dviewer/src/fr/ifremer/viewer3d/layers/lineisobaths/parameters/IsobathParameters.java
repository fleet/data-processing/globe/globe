/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lineisobaths.parameters;

import java.util.List;

import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.NumberPreferenceAttribute;
import fr.ifremer.globe.ui.databinding.model.PreferenceBasedModel;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableNumber;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.preference.IsobathsPreference;

/** Parameters of isobath generation. */
public class IsobathParameters extends PreferenceBasedModel {

	/** Key used to save this bean in session */
	public static final TypedKey<SessionIsobathParameters> SESSION_KEY = new TypedKey<>("Isobath_parameters");

	// Generation parameters

	/** Main line (every x lines). */
	protected WritableNumber mainLine = new WritableNumber(5);
	/** Distance between two secondary lines. */
	protected WritableNumber step = new WritableNumber(10);
	/** % of points extected per lines. */
	protected WritableNumber sampling = new WritableNumber(50);

	// Presentation parameters
	private final IsobathsPreference prefs = Activator.getIsobathsPreference();

	/** Color of main lines */
	protected WritableObject<GColor> mainLineColor = new WritableObject<>(prefs.getIsobathMasterLineColor().getValue());
	/** True to display main lines */
	protected WritableBoolean mainLineVisible = new WritableBoolean(true);

	/** True when other line has different colors */
	protected WritableBoolean isOtherLineMultiColor = new WritableBoolean(false);

	/** Colors of other lines **/
	protected WritableObject<GColor> otherLineUnicolor = new WritableObject<>(prefs.getIsobathLineColor().getValue());
	protected WritableObjectList<GColor> otherLineColors = new WritableObjectList<>(new ColorProvider().getColors());

	/** True to display Secondary lines */
	protected WritableBoolean otherLineVisible = new WritableBoolean(true);
	/** Width of the isobaths lines */
	protected WritableNumber lineWidth = new WritableNumber(prefs.getIsoBathlineSize().getValue().intValue());

	/**
	 * Constructor
	 */
	public IsobathParameters() {
		// default constructor
	}

	/** Restore settings from the session bean */
	public IsobathParameters(SessionIsobathParameters sessionParameters) {
		mainLine.set(sessionParameters.mainLine);
		step.set(sessionParameters.step);
		sampling.set(sessionParameters.sampling);
		mainLineColor.set(sessionParameters.mainLineColor);
		mainLineVisible.set(sessionParameters.mainLineVisible);
		isOtherLineMultiColor.set(sessionParameters.isOtherLineMultiColor);
		otherLineUnicolor.set(sessionParameters.otherLineUnicolor);
		otherLineColors.getRealm().exec(() -> otherLineColors.addAll(sessionParameters.otherLineColors));
		otherLineVisible.set(sessionParameters.otherLineVisible);
		lineWidth.set(sessionParameters.lineWidth);
	}

	/** Loads parameters values from another {@link IsobathParameters} **/
	public void load(IsobathParameters otherParameters) {
		step.set(otherParameters.getStep().get());
		sampling.set(otherParameters.getSampling().get());
		lineWidth.set(otherParameters.getLineWidth().get());

		mainLine.set(otherParameters.getMainLine().get());
		mainLineColor.set(otherParameters.getMainLineColor().get());
		mainLineVisible.set(otherParameters.getMainLineVisible().get());

		otherLineVisible.set(otherParameters.getOtherLineVisible().get());
		isOtherLineMultiColor.set(otherParameters.getIsOtherLineMultiColor().get());
		otherLineUnicolor.set(otherParameters.getOtherLineUnicolor().get());
		otherLineColors.clear();
		otherLineColors.addAll(otherParameters.getOtherLineColors().asList());
	}

	/** Getter of {@link #mainLine} */
	public WritableNumber getMainLine() {
		return mainLine;
	}

	/** Sync attribute on preference */
	public void syncMainLine(NumberPreferenceAttribute... preferences) {
		sync(List.of(preferences), mainLine);
	}

	/** Getter of {@link #step} */
	public WritableNumber getStep() {
		return step;
	}

	/** Sync attribute on preference */
	public void syncStep(NumberPreferenceAttribute... preferences) {
		sync(List.of(preferences), step);
	}

	/** Getter of {@link #sampling} */
	public WritableNumber getSampling() {
		return sampling;
	}

	/** Sync attribute on preference */
	public void syncSampling(NumberPreferenceAttribute... preferences) {
		sync(List.of(preferences), sampling);
	}

	/** Getter of {@link #mainLineColor} */
	public WritableObject<GColor> getMainLineColor() {
		return mainLineColor;
	}

	/** Sync attribute on preference */
	public void syncMainLineColor(ColorPreferenceAttribute preference) {
		sync(preference, mainLineColor);
	}

	/** Getter of {@link #mainLineVisible} */
	public WritableBoolean getMainLineVisible() {
		return mainLineVisible;
	}

	/** Sync attribute on preference */
	public void syncMainLineVisible(BooleanPreferenceAttribute preference) {
		sync(preference, mainLineVisible);
	}

	/** Getter of {@link #isOtherLineMultiColor} */
	public WritableBoolean getIsOtherLineMultiColor() {
		return isOtherLineMultiColor;
	}

	/** Getter of {@link #otherLineColor} */
	public WritableObjectList<GColor> getOtherLineColors() {
		return otherLineColors;
	}

	/** Sync attribute on preference */
	public void syncOtherLineColor(ColorPreferenceAttribute preference) {
		sync(preference, otherLineUnicolor);
	}

	/** Getter of {@link #otherLineUnicolor} **/
	public WritableObject<GColor> getOtherLineUnicolor() {
		return otherLineUnicolor;
	}

	/** Getter of {@link #otherLineVisible} */
	public WritableBoolean getOtherLineVisible() {
		return otherLineVisible;
	}

	/** Sync attribute on preference */
	public void syncOtherLineVisible(BooleanPreferenceAttribute preference) {
		sync(preference, otherLineVisible);
	}

	/** Getter of {@link #lineWidth} */
	public WritableNumber getLineWidth() {
		return lineWidth;
	}

	/** Sync attribute on preference */
	public void syncLineWidth(NumberPreferenceAttribute preference) {
		sync(preference, lineWidth);
	}

	/** Export this IsobathParameters to a session bean */
	public TypedEntry<SessionIsobathParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(//
				new SessionIsobathParameters(//
						mainLine.intValue(), //
						step.doubleValue(), //
						sampling.intValue(), //
						mainLineColor.get(), //
						mainLineVisible.isTrue(), //
						isOtherLineMultiColor.isTrue(), //
						otherLineUnicolor.get(), //
						otherLineColors.asList(), //
						otherLineVisible.isTrue(), //
						lineWidth.intValue()));
	}

	/** Record to save the isobath's parameters in session */
	public static class SessionIsobathParameters {

		/** Main line (every x lines). */
		public final int mainLine;
		/** Distance between two secondary lines. */
		public final double step;
		/** % of points extected per lines. */
		public final int sampling;

		/** Color of main lines */
		public final GColor mainLineColor;
		/** True to display main lines */
		public final boolean mainLineVisible;

		/** True when other line has different colors */
		public final boolean isOtherLineMultiColor;

		/** Colors of other lines **/
		public final GColor otherLineUnicolor;
		public final List<GColor> otherLineColors;

		/** True to display Secondary lines */
		public final boolean otherLineVisible;
		/** Width of the isobaths lines */
		public final int lineWidth;

		/**
		 * Constructor
		 */
		public SessionIsobathParameters(int mainLine, double step, int sampling, GColor mainLineColor,
				boolean mainLineVisible, boolean isOtherLineMultiColor, GColor otherLineUnicolor,
				List<GColor> otherLineColors, boolean otherLineVisible, int lineWidth) {
			this.mainLine = mainLine;
			this.step = step;
			this.sampling = sampling;
			this.mainLineColor = mainLineColor;
			this.mainLineVisible = mainLineVisible;
			this.isOtherLineMultiColor = isOtherLineMultiColor;
			this.otherLineUnicolor = otherLineUnicolor;
			this.otherLineColors = otherLineColors;
			this.otherLineVisible = otherLineVisible;
			this.lineWidth = lineWidth;
		}

	}

	@Override
	public String toString() {
		return "IsobathParameters [step=" + step.getValue() + ", sampling=" + sampling.getValue() + "]";
	}

}