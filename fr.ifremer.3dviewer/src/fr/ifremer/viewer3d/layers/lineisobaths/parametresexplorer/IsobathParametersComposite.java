package fr.ifremer.viewer3d.layers.lineisobaths.parametresexplorer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;

/**
 * Composite that changes contrast and shading of ShaderElevationLayer.
 */
public class IsobathParametersComposite extends Composite {

	/** Binding context */
	protected DataBindingContext dataBindingContext;

	/** Model used in this Composite */
	protected IsobathParameters isobathLayerParameters;
	private final Runnable onComputeCallback;

	/** Widgets */

	/** Generation widgets **/
	protected Spinner stepSpinner;
	protected Spinner mainSpinner;
	protected Spinner samplingSpinner;
	protected Button computeButton;

	/** Presentation widgets **/
	protected Composite mainColorLabel;
	protected Button mainLinesVisible;
	protected Button otherLinesVisible;
	protected Combo multiColorCombo;
	protected Spinner lineWidthSpinner;

	private DataBindingContext bindingContext = new DataBindingContext();

	private List<Composite> colorPickerList = new ArrayList<>();

	/**
	 * Constructor.
	 *
	 * @wbp.parser.constructor
	 */
	public IsobathParametersComposite(Composite parent, IsobathLayer isobathLayer) {
		super(parent, SWT.NONE);

		if (isobathLayer.getIsobathParameters() == null) {
			IsobathParameters isobathParameters = new IsobathParameters();
			isobathParameters.getStep().set(100);
			isobathLayer.setParameters(isobathParameters);
		}
		onComputeCallback = isobathLayer::compute;

		isobathLayerParameters = isobathLayer.getIsobathParameters();
		initializeComposite();
		postInitialization();
	}

	/**
	 * Constructor.
	 */
	public IsobathParametersComposite(Composite parent, IsobathParameters isobathParams, Runnable onCompute) {
		super(parent, SWT.NONE);
		isobathLayerParameters = isobathParams;
		onComputeCallback = onCompute;
		initializeComposite();
		postInitialization();

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	public void initializeComposite() {
		setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite mainComposite = new Composite(this, SWT.NONE);
		GridLayout glComposite = new GridLayout(1, false);
		glComposite.verticalSpacing = 0;
		glComposite.marginWidth = 0;
		glComposite.marginHeight = 0;
		glComposite.horizontalSpacing = 0;
		mainComposite.setLayout(glComposite);

		Composite subComposite = new Composite(mainComposite, SWT.NONE);
		subComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		subComposite.setLayout(new GridLayout(1, false));

		initializeGenerationWidgets(subComposite);
		initializePresentationWidgets(subComposite);
	}

	public void initializeGenerationWidgets(Composite parent) {
		Group grpGeneration = new Group(parent, SWT.NONE);
		grpGeneration.setText("Generation");
		grpGeneration.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpGeneration.setLayout(new GridLayout(3, false));

		Label lblStep = new Label(grpGeneration, SWT.NONE);
		lblStep.setText(DicoBundle.getString("WORD_STEP"));

		stepSpinner = new Spinner(grpGeneration, SWT.BORDER);
		stepSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		stepSpinner.setMaximum(100000);
		stepSpinner.setMinimum(1);
		stepSpinner.setDigits(1);
		stepSpinner.setIncrement((int) Math.pow(10, stepSpinner.getDigits()));

		Label lblM = new Label(grpGeneration, SWT.NONE);
		lblM.setText("m");

		Label lblMasterLines = new Label(grpGeneration, SWT.NONE);
		lblMasterLines.setText(DicoBundle.getString("ISOBATHS_MASTER_LINES"));

		mainSpinner = new Spinner(grpGeneration, SWT.BORDER);
		mainSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		mainSpinner.setMaximum(20);
		mainSpinner.setMinimum(2);

		Label lbleveryXLines = new Label(grpGeneration, SWT.NONE);
		lbleveryXLines.setText(DicoBundle.getString("ISOBATHS_EVERY_X_LINES"));

		Label lblPrecision = new Label(grpGeneration, SWT.NONE);
		lblPrecision.setText(DicoBundle.getString("WORD_SAMPLING"));

		samplingSpinner = new Spinner(grpGeneration, SWT.BORDER);
		samplingSpinner.setPageIncrement(1);
		samplingSpinner.setMinimum(1);
		samplingSpinner.setSelection(10);

		Label lblPercent = new Label(grpGeneration, SWT.NONE);
		lblPercent.setText("%");

		computeButton = new Button(grpGeneration, SWT.NONE);
		computeButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 3, 1));
		computeButton.addListener(SWT.Selection, e -> onComputeCallback.run());
		computeButton.setText(DicoBundle.getString("WORD_COMPUTE"));
	}

	public void initializePresentationWidgets(Composite parent) {
		boolean isMultiColor = isobathLayerParameters.getIsOtherLineMultiColor().get();

		Group grpPresentation = new Group(parent, SWT.NONE);
		grpPresentation.setText("Presentation");
		GridData gdGrpPresentation = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gdGrpPresentation.verticalIndent = 10;
		grpPresentation.setLayoutData(gdGrpPresentation);
		grpPresentation.setLayout(new GridLayout(3, false));

		Label lblLineWidth = new Label(grpPresentation, SWT.NONE);
		lblLineWidth.setText(DicoBundle.getString("ISOBATHS_LINES_WIDTH"));

		lineWidthSpinner = new Spinner(grpPresentation, SWT.BORDER);
		lineWidthSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lineWidthSpinner.setMaximum(10);
		lineWidthSpinner.setMinimum(1);

		Label lblPixels = new Label(grpPresentation, SWT.NONE);
		lblPixels.setText(DicoBundle.getString("ISOBATHS_PIXEL_UNIT"));

		mainLinesVisible = new Button(grpPresentation, SWT.CHECK);
		mainLinesVisible.setText(DicoBundle.getString("ISOBATHS_MASTER_LINES"));

		mainColorLabel = new Composite(grpPresentation, SWT.BORDER);
		GridData mainColorLabelLayoutData = new GridData(SWT.LEFT, SWT.FILL, false, false, 2, 1);
		mainColorLabel.setLayoutData(mainColorLabelLayoutData);
		mainColorLabelLayoutData.heightHint = 25;

		otherLinesVisible = new Button(grpPresentation, SWT.CHECK);
		otherLinesVisible.setText(DicoBundle.getString("ISOBATHS_SECONDARY_LINES"));

		multiColorCombo = new Combo(grpPresentation, SWT.READ_ONLY);
		multiColorCombo.add("Unicolor");
		multiColorCombo.add("Multicolor");
		multiColorCombo.select(isMultiColor ? 1 : 0);

		multiColorCombo.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 2, 1));

		GColor unicolor = isobathLayerParameters.getOtherLineUnicolor().get();
		WritableList<GColor> multicolors = isobathLayerParameters.getOtherLineColors();

		Label uniColorLabel = new Label(grpPresentation, SWT.NONE);
		uniColorLabel.setText("Unicolor");
		uniColorLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		ColorPicker uniColorPicker = new ColorPicker(grpPresentation, SWT.BORDER, unicolor, c -> onColorChanged(0, c));
		GridData uniColorGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		uniColorGridData.heightHint = 20;
		uniColorPicker.setLayoutData(uniColorGridData);

		Label multiColorLabel = new Label(grpPresentation, SWT.NONE);
		multiColorLabel.setText("Multicolor");
		multiColorLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Composite multicolorPickers = new Composite(grpPresentation, SWT.BORDER);
		GridLayout multicolorLayout = new GridLayout(multicolors.size(), false);
		multicolorLayout.marginWidth = 0;
		multicolorLayout.marginHeight = 0;
		multicolorLayout.horizontalSpacing = 0;
		multicolorPickers.setLayout(multicolorLayout);
		multicolorPickers.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true, 2, 1));

		int i = 0;
		for (GColor color : multicolors) {
			final int j = i;
			ColorPicker picker = new ColorPicker(multicolorPickers, SWT.NONE, color, c -> onColorChanged(j, c));
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
			gridData.widthHint = 25;
			gridData.heightHint = 25;

			picker.setLayoutData(gridData);
			picker.setColor(color);
			colorPickerList.add(picker);
			i++;
		}
	}

	private void onColorChanged(int index, GColor c) {
		if (isobathLayerParameters.getIsOtherLineMultiColor().get().booleanValue()) {
			isobathLayerParameters.getOtherLineColors().set(index, c);
			onComputeCallback.run(); // TODO avoid recompute
		} else {
			isobathLayerParameters.getOtherLineUnicolor().set(c);
		}
	}

	/**
	 * Hook all listeners before displaying this composite.
	 */
	public void postInitialization() {
		dataBindingContext = initDataBindings();
		dataBindingContext.updateTargets();

		mainColorLabel.addListener(SWT.MouseDown, event -> changeMainLineColor());
		mainColorLabel.setCursor(mainColorLabel.getDisplay().getSystemCursor(SWT.CURSOR_HAND));

		multiColorCombo.addListener(SWT.Selection, e -> onComputeCallback.run());
	}

	/**
	 * Change main lines color
	 */
	protected void changeMainLineColor() {
		ColorDialog colorDialog = new ColorDialog(mainColorLabel.getShell());
		GColor oldColor = getIsobathLayerParameters().getMainLineColor().get();
		if (oldColor != null) {
			colorDialog.setRGB(ColorUtils.convertGColorToSWT(oldColor).getRGB());
		}
		RGB rgb = colorDialog.open();
		if (rgb != null) {
			Color newColor = new Color(mainColorLabel.getDisplay(), rgb);
			getIsobathLayerParameters().getMainLineColor().set(ColorUtils.convertSWTToGColor(newColor));
		}
	}

	protected void changeMainLineColor(GColor color) {
		mainColorLabel.getDisplay().asyncExec(() -> mainColorLabel.setBackground(ColorUtils.convertGColorToSWT(color)));
	}

	/** Getter of {@link #isobathLayerParameters} */
	public IsobathParameters getIsobathLayerParameters() {
		return isobathLayerParameters;
	}

	/** Setter of {@link #isobathLayerParameters} */
	public void setIsobathLayerParameters(IsobathParameters isobathLayerParameters) {
		this.isobathLayerParameters = isobathLayerParameters;
		initDataBindings();
		dataBindingContext.updateTargets();
	}

	protected DataBindingContext initDataBindings() {

		bindingContext.dispose();

		//
		var stepSpinnerObserver = org.eclipse.jface.databinding.swt.typed.WidgetProperties.spinnerSelection()
				.observe(stepSpinner);
		UpdateValueStrategy<Integer, Number> targetToModel = UpdateValueStrategy.create(
				IConverter.create(spinnerValue -> ((long) spinnerValue / Math.pow(10, stepSpinner.getDigits()))));
		UpdateValueStrategy<Number, Integer> modelToTarget = UpdateValueStrategy.create(IConverter
				.create(value -> (int) Math.round(value.doubleValue() * Math.pow(10, stepSpinner.getDigits()))));
		bindingContext.bindValue(stepSpinnerObserver, isobathLayerParameters.getStep(), targetToModel, modelToTarget);

		//
		var observeSelectionMainSpinnerObserveWidget = WidgetProperties.spinnerSelection().observe(mainSpinner);
		bindingContext.bindValue(observeSelectionMainSpinnerObserveWidget, isobathLayerParameters.getMainLine(), null,
				null);
		//
		var observeSelectionPrecisionSpinnerObserveWidget = WidgetProperties.spinnerSelection()
				.observe(samplingSpinner);
		bindingContext.bindValue(observeSelectionPrecisionSpinnerObserveWidget, isobathLayerParameters.getSampling(),
				null, null);
		//
		var observeSelectionMainLinesVisibleObserveWidget = WidgetProperties.buttonSelection()
				.observe(mainLinesVisible);
		bindingContext.bindValue(observeSelectionMainLinesVisibleObserveWidget,
				isobathLayerParameters.getMainLineVisible(), null, null);
		//
		var observeSelectionOtherLinesVisibleObserveWidget = WidgetProperties.buttonSelection()
				.observe(otherLinesVisible);
		bindingContext.bindValue(observeSelectionOtherLinesVisibleObserveWidget,
				isobathLayerParameters.getOtherLineVisible(), null, null);
		//
		var observeSelectionLineWidthSpinnerObserveWidget = WidgetProperties.spinnerSelection()
				.observe(lineWidthSpinner);
		bindingContext.bindValue(observeSelectionLineWidthSpinnerObserveWidget, isobathLayerParameters.getLineWidth(),
				null, null);
		//
		var observeBackgroundMainColorLabelObserveWidget = WidgetProperties.background().observe(mainColorLabel);
		UpdateValueStrategy mainToModelStrategy = new UpdateValueStrategy();
		mainToModelStrategy.setConverter(IConverter.create(Color.class, GColor.class,
				swtColor -> ColorUtils.convertSWTToGColor((Color) swtColor)));
		UpdateValueStrategy mainToViewStrategy = new UpdateValueStrategy();
		mainToViewStrategy.setConverter(
				IConverter.create(GColor.class, Color.class, gColor -> ColorUtils.convertGColorToSWT((GColor) gColor)));
		bindingContext.bindValue(observeBackgroundMainColorLabelObserveWidget,
				isobathLayerParameters.getMainLineColor(), mainToModelStrategy, mainToViewStrategy);

		var observeCombo = WidgetProperties.singleSelectionIndex().observe(multiColorCombo);
		UpdateValueStrategy multiColorComboStrategy = new UpdateValueStrategy();
		multiColorComboStrategy.setConverter(IConverter.create(int.class, boolean.class, index -> (int) index == 1));
		UpdateValueStrategy isMulticolorStrategy = new UpdateValueStrategy();
		isMulticolorStrategy
				.setConverter(IConverter.create(boolean.class, int.class, isMulti -> (boolean) isMulti ? 1 : 0));
		bindingContext.bindValue(observeCombo, isobathLayerParameters.getIsOtherLineMultiColor(),
				multiColorComboStrategy, isMulticolorStrategy);

		return bindingContext;
	}
}
