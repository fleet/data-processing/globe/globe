/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lineisobaths.parametresexplorer;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;

/**
 * Make a Composite to edit the IsobathLayer parameters
 */
@Component(name = "globe_viewer3d_lineisobaths_composite_factory", service = IParametersViewCompositeFactory.class)
public class IsobathCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof IsobathLayer
				? Optional.of(new IsobathParametersComposite(parent, (IsobathLayer) selection))
				: Optional.empty();
	}
}
