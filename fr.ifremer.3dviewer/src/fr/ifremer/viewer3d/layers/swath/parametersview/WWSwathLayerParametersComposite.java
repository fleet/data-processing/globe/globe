package fr.ifremer.viewer3d.layers.swath.parametersview;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;

/**
 * Parameter composite for {@link IWWNavigationLayer}.
 */
public class WWSwathLayerParametersComposite extends Composite {

	/** Edited layer */
	private final IWWSwathSelectionLayer layer;

	/**
	 * Constructor
	 */
	public WWSwathLayerParametersComposite(Composite parent, IWWSwathSelectionLayer layer) {
		super(parent, SWT.NONE);
		this.layer = layer;
		initializeComposite();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	public void initializeComposite() {
		setLayout(new GridLayout(1, false));

		Group grpSwath = new Group(this, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(grpSwath);
		grpSwath.setText("Swaths configuration");
		grpSwath.setLayout(new GridLayout(2, false));

		// Highlight color
		Label uniColorLabel = new Label(grpSwath, SWT.NONE);
		uniColorLabel.setText("Highlight");
		GridDataFactory.fillDefaults().applyTo(uniColorLabel);

		ColorPicker uniColorPicker = new ColorPicker(grpSwath, SWT.BORDER, layer.getParameters().highlightColor(),
				c -> {
					var param = layer.getParameters();
					param = param.withColor(c, param.selectionColor());
					layer.setParameters(param);
				});
		GridDataFactory.fillDefaults().grab(true, false).hint(0, 20).applyTo(uniColorPicker);

		// Selection color
		Label selectionColorLabel = new Label(grpSwath, SWT.NONE);
		selectionColorLabel.setText("Selection");
		GridDataFactory.fillDefaults().applyTo(selectionColorLabel);

		ColorPicker selectionColorPicker = new ColorPicker(grpSwath, SWT.BORDER,
				layer.getParameters().selectionColor(), c -> {
					var param = layer.getParameters();
					param = param.withColor(param.highlightColor(), c);
					layer.setParameters(param);
				});
		GridDataFactory.fillDefaults().grab(true, false).hint(0, 20).applyTo(selectionColorPicker);

		// Opacity
		Group grpOpacity = new Group(this, SWT.NONE);
		grpOpacity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpOpacity.setText("Opacity");
		grpOpacity.setLayout(new GridLayout(1, false));
		var opacitySliderComposite = new SliderComposite(grpOpacity, SWT.NONE, false,
				value -> layer.setParameters(layer.getParameters().withOpacity(value)));
		opacitySliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		opacitySliderComposite.setValue((float) layer.getParameters().opacity());
	}

}
