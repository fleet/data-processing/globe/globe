/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.swath;

import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.apache.commons.lang.math.IntRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.WWSwathLayerParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.WWSwathSelection;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.lines.LinesLayer;
import fr.ifremer.viewer3d.layers.lines.impl.LineProperties;
import fr.ifremer.viewer3d.layers.lines.impl.SubLineProperties;
import fr.ifremer.viewer3d.render.ToolTipAnnotation;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.render.DrawContext;

/**
 * This class will display the swath layer
 */
public class SwathSelectionLayer extends LinesLayer implements IWWSwathSelectionLayer {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(SwathSelectionLayer.class);

	/** Transparent color for unselected swath */
	private GColor defaultColor = new GColor(0, 0, 0, 0);

	private final Comparator<ISounderNcInfo> sounderNcInfoSorter = (info1, info2) -> info1.getStartEndDate().get()
			.getFirst().compareTo(info2.getStartEndDate().get().getFirst());
	/** All managed lines, sorted by start date */
	private final TreeMap<ISounderNcInfo, LineProperties> swathLines = new TreeMap<>(sounderNcInfoSorter);
	private final TreeMap<ISounderNcInfo, LongLoadableLayer1D> pingTimes = new TreeMap<>(sounderNcInfoSorter);

	/** Layer parameters */
	private WWSwathLayerParameters parameters;
	/** Projection of selection */
	private final Projection projection;

	/** Listeners for the selection */
	private MouseListener mouseListener;

	/** Listeners for the selection */
	private FocusListener focusListener;

	/** Highlighted swaths of one ISounderNcInfo */
	private ISounderNcInfo highlightedSounderNcInfo = null;
	private int highlightedSwath = -1;

	/** Current selection */
	private ISounderNcInfo selectedSounderNcInfo = null;
	private int selectedSwath = -1;

	/** Selection listener */
	private Optional<Consumer<WWSwathSelection>> listener = Optional.empty();
	/** Tooltip */
	private ToolTipAnnotation tooltip;

	@Inject
	private ISessionService sessionService;
	@Inject
	private IGeographicViewService geographicViewService;

	/** Globe */
	private Globe globe;

	@Inject
	private IGlobalCursorService globalCursorService;
	/** Listener of GlobalCursor hooked to IGlobalCursorService */
	private final Consumer<GlobalCursor> globalCursorListener = this::onGlobalCursor;

	/**
	 * Constructor.
	 */
	public SwathSelectionLayer(String name, ProjectionSettings projectionSettings, int swathCountInSelection,
			GeoBox geoBox) {
		super(name, geoBox);
		parameters = new WWSwathLayerParameters().withSwathsCountInSelection(swathCountInSelection)
				.withProjection(projectionSettings);
		projection = new Projection(projectionSettings);
		setIcon(ImageResources.getImage("/icons/16/swath.png", getClass()));
		ContextInitializer.inject(this);
		setEnabled(false); // Enable must be requested explicitly
	}

	@PostConstruct
	private void postConstruct() {
		globe = geographicViewService.getWwd().getModel().getGlobe();

		// Initialize the tooltip
		tooltip = new ToolTipAnnotation("Swath");
		tooltip.setTooltipOffset(new Point(20, 0));
		tooltip.getAttributes().setVisible(false);
	}

	/** {@inheritDoc} */
	@Override
	public void addSwathsFrom(List<SounderDataContainer> dataContainers) {

		for (SounderDataContainer container : dataContainers) {
			try {
				var allPoints = generatesAllSwaths(container);
				logger.debug("Number of swath to draw : {}", allPoints.size());
				LineProperties result = (LineProperties) super.acceptLine(allPoints.size(), i -> allPoints.get(i).x,
						i -> allPoints.get(i).y, i -> allPoints.get(i).z, defaultColor, 2f);

				for (int swath = 0; swath < allPoints.size(); swath += 2) {
					result.split(swath, swath + 1, defaultColor, 2f);
				}
				swathLines.put(container.getInfo(), result);

				LongLoadableLayer1D timeLayer = container.getLayer(BeamGroup1Layers.PING_TIME);
				pingTimes.put(container.getInfo(), timeLayer);
			} catch (GIOException e) {
				logger.error("Error with file loading : " + e.getMessage(), e);
			}
		}
	}

	@Override
	public void shiftSelection(int increment) {
		if (selectedSounderNcInfo == null)
			return; // Nothing to shift

		int newSwathIdx = selectedSwath + increment * parameters.swathsCountInSelection();
		ISounderNcInfo newSounderInfo = null;
		// compute next file and swath index to select
		if (newSwathIdx >= 0 && newSwathIdx < selectedSounderNcInfo.getCycleCount())
			// next swath into the same file
			newSounderInfo = selectedSounderNcInfo;
		else if (newSwathIdx < 0) {
			// back to previous file if exist
			newSounderInfo = swathLines.lowerKey(selectedSounderNcInfo);
			if (newSounderInfo != null)
				newSwathIdx = newSounderInfo.getCycleCount() - 1;
		} else if (newSwathIdx >= selectedSounderNcInfo.getCycleCount()) {
			// switch to next file if exist
			newSounderInfo = swathLines.higherKey(selectedSounderNcInfo);
			newSwathIdx = 0;
		}

		// update selection
		if (newSounderInfo != null) {
			manageSwathSelection(newSounderInfo, newSwathIdx);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void setListener(Consumer<WWSwathSelection> listener) {
		this.listener = Optional.of(listener);
	}

	/**
	 * Return a list of pair of points (2 consecutive points are a pair). <br>
	 * Each pair is the starting and ending point of a swath
	 */
	private List<Vec4> generatesAllSwaths(SounderDataContainer container) throws GIOException {
		var result = new ArrayList<Vec4>(container.getInfo().getCycleCount() * 2);
		DoubleLoadableLayer2D lats = container.getLayer(BathymetryLayers.DETECTION_LATITUDE);
		DoubleLoadableLayer2D lons = container.getLayer(BathymetryLayers.DETECTION_LONGITUDE);
		DoubleLayer2D elevs = container.getCsLayers(CoordinateSystem.FCS).getProjectedZ();
		for (int swathIdx = 0; swathIdx < container.getInfo().getCycleCount(); swathIdx++) {
			generatesOneSwath(container, lats, lons, elevs, swathIdx, result);
		}
		return result;
	}

	/**
	 * Add to the list a pair of points (starting and ending point of a swath)
	 */
	private void generatesOneSwath(SounderDataContainer container, DoubleLoadableLayer2D lats,
			DoubleLoadableLayer2D lons, DoubleLayer2D elevs, int swathIdx, List<Vec4> detections) throws GIOException {
		double vertExa = geographicViewService.getWwd().getSceneController().getVerticalExaggeration();

		// Search first valid detection
		Vec4 firstPoint = null;
		for (int firstIdx = 0; firstPoint == null && firstIdx < container.getInfo().getTotalRxBeamCount(); firstIdx++) {
			double lon = lons.get(swathIdx, firstIdx);
			double lat = lats.get(swathIdx, firstIdx);
			double elev = -elevs.get(swathIdx, firstIdx);
			if (Double.isFinite(lon) && Double.isFinite(lat) && Double.isFinite(elev)) {
				firstPoint = globe.computePointFromPosition(Position.fromDegrees(lat, lon, vertExa * elev));
			}
		}
		// Search last valid detection
		Vec4 lastPoint = null;
		for (int lastIdx = container.getInfo().getTotalRxBeamCount() - 1; lastPoint == null
				&& lastIdx >= 0; lastIdx--) {
			double lon = lons.get(swathIdx, lastIdx);
			double lat = lats.get(swathIdx, lastIdx);
			double elev = -elevs.get(swathIdx, lastIdx);
			if (Double.isFinite(lon) && Double.isFinite(lat) && Double.isFinite(elev)) {
				lastPoint = globe.computePointFromPosition(Position.fromDegrees(lat, lon, vertExa * elev));
			}
		}

		if (firstPoint != null && lastPoint != null) {
			detections.add(firstPoint);
			detections.add(lastPoint);
		} else {
			// No valid coordinates found for the Swath. Add a fake vec4 to list of detections
			detections.add(Vec4.ZERO);
			detections.add(Vec4.ZERO);
		}
	}

	/** Search the swath under the mouse and highlight it */
	private void highlightSwathAt(int mouseX, int mouseY) {
		var view = geographicViewService.getWwd().getView();
		mouseY = view.getViewport().height - mouseY;
		Vec4 mouse = new Vec4(mouseX, mouseY);

		double minDistance = 10.; // pixels max around the line
		int nearestSwathIdx = -1;
		ISounderNcInfo nearestFile = null;
		for (var swathEntry : swathLines.entrySet()) {
			LineProperties lineProps = swathEntry.getValue();
			for (var oneSubLine : lineProps.getSubLineProperties()) {
				// Working in in screen coordinates : project points
				Vec4 from = view.project(lineProps.getPoint(oneSubLine.getFirstIndex()));
				Vec4 to = view.project(lineProps.getPoint(oneSubLine.getLastIndex()));
				var distance = Line.distanceToSegment(from, to, mouse);
				if (distance < minDistance) {
					minDistance = distance;
					nearestSwathIdx = oneSubLine.getFirstIndex() / 2;
					nearestFile = swathEntry.getKey();
				}
			}
		}

		// update highlight
		unhighlight();
		if (nearestSwathIdx >= 0)
			highlight(nearestFile, nearestSwathIdx);
	}

	/** Select the specified swaths */
	private void manageSwathSelection(ISounderNcInfo sounderNcInfo, int swath) {
		if (sounderNcInfo != null) {
			LineProperties line = swathLines.get(sounderNcInfo);
			List<SubLineProperties> subLines = line.getSubLineProperties();

			clearSelection();
			selectedSounderNcInfo = sounderNcInfo;
			selectedSwath = swath;
			var swathRange = computeSwathRange(selectedSounderNcInfo, selectedSwath);
			GeoBox geobox = null;
			for (int swathIdx = swathRange.getMinimumInteger(); swathIdx < swathRange.getMaximumInteger(); swathIdx++) {
				try {
					SubLineProperties subLine = subLines.get(swathIdx);
					subLine.setColor(parameters.selectionColor());
					GeoBox swathGeobox = inferGeoBox(line, subLine);
					if (geobox == null)
						geobox = swathGeobox;
					else
						geobox.extend(swathGeobox);
				} catch (ProjectionException e) {
					logger.warn("Projection error, selection rejected : {}", e.getMessage());
				}
			}

			if (geobox != null && listener.isPresent()) {
				var selection = new WWSwathSelection(sounderNcInfo, swathRange.getMinimumInteger(),
						swathRange.getMaximumInteger() - 1, geobox);
				listener.stream().forEach(l -> l.accept(selection));
				geographicViewService.refresh();
			}

			Instant selectedInstant = gainInstant(pingTimes.get(sounderNcInfo), swath);
			if (selectedInstant != null)
				globalCursorService.submit(new GlobalCursor().withSource(this).withTime(selectedInstant));
		}
	}

	/** Clear all selected swath */
	private void clearSelection() {
		if (selectedSounderNcInfo != null) {
			changeSelectionColor(defaultColor);
			selectedSounderNcInfo = null;
			selectedSwath = -1;
		}
	}

	/** Unhighlight the current line */
	private void unhighlight() {
		if (highlightedSounderNcInfo != null) {
			LineProperties line = swathLines.get(highlightedSounderNcInfo);
			var swathRange = computeSwathRange(highlightedSounderNcInfo, highlightedSwath);
			for (int swathIdx = swathRange.getMinimumInteger(); swathIdx < swathRange.getMaximumInteger(); swathIdx++) {
				SubLineProperties subLine = line.getSubLineProperties().get(swathIdx);
				if (subLine.getColor() != parameters.selectionColor())
					subLine.setColor(defaultColor);
			}
		}
		highlightedSounderNcInfo = null;
		highlightedSwath = -1;
		tooltip.getAttributes().setVisible(false);
	}

	/** Highlight the current line */
	private void highlight(ISounderNcInfo info, int mainSwathIdx) {
		LineProperties line = swathLines.get(info);
		highlightedSounderNcInfo = info;
		highlightedSwath = mainSwathIdx;
		var swathRange = computeSwathRange(info, mainSwathIdx);
		for (int swathIdx = swathRange.getMinimumInteger(); swathIdx < swathRange.getMaximumInteger(); swathIdx++) {
			SubLineProperties subLine = line.getSubLineProperties().get(swathIdx);
			if (subLine.getColor() != parameters.selectionColor())
				subLine.setColor(parameters.highlightColor());
		}

		tooltip.setText(info.getFilename() + "\nSwath " + mainSwathIdx);
		tooltip.getAttributes().setVisible(true);
	}

	/**
	 * @return the {@link #parameters}
	 */
	@Override
	public WWSwathLayerParameters getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the {@link #parameters} to set
	 */
	@Override
	public void setParameters(WWSwathLayerParameters parameters) {
		boolean swathsCountInSelectionChanged = parameters.swathsCountInSelection() != this.parameters
				.swathsCountInSelection();
		fitParameters(parameters);
		if (swathsCountInSelectionChanged)
			manageSwathSelection(selectedSounderNcInfo, selectedSwath);
		geographicViewService.refresh();
		sessionService.saveSession();
	}

	/** Set parameters quietly */
	private void fitParameters(WWSwathLayerParameters parameters) {
		changeSelectionColor(defaultColor); // Reset selection before applying a new one
		this.parameters = parameters;
		changeSelectionColor(parameters.selectionColor());
		setOpacity(parameters.opacity());
	}

	/** Change the color of the selected swath */
	private void changeSelectionColor(GColor color) {
		if (selectedSounderNcInfo != null) {
			LineProperties line = swathLines.get(selectedSounderNcInfo);
			var swathRange = computeSwathRange(selectedSounderNcInfo, selectedSwath);
			for (int swathIdx = swathRange.getMinimumInteger(); swathIdx < swathRange.getMaximumInteger(); swathIdx++) {
				SubLineProperties subLine = line.getSubLineProperties().get(swathIdx);
				subLine.setColor(color);
			}
		}
	}

	/** Compute the range of swath index */
	private IntRange computeSwathRange(ISounderNcInfo sounderNcInfo, int swathIdx) {
		int firstSwath = Math.max(swathIdx - (parameters.swathsCountInSelection() / 2), 0);
		int lastSwath = Math.min(firstSwath + parameters.swathsCountInSelection(), sounderNcInfo.getCycleCount());
		return new IntRange(firstSwath, lastSwath);
	}

	/** Compute the geobox of the line */
	private GeoBox inferGeoBox(LineProperties line, SubLineProperties subLine) throws ProjectionException {
		// Cartesian point in the geographic view projection
		Vec4 wwFrom = line.getPoint(subLine.getFirstIndex());
		Vec4 wwTo = line.getPoint(subLine.getLastIndex());

		// Fake point : swath have no valid point
		if (Objects.equals(wwFrom, Vec4.ZERO) || Objects.equals(wwTo, Vec4.ZERO)) {
			return null;
		}
		// Projects cartesian points to LonLat
		Position from = globe.computePositionFromPoint(wwFrom);
		Position to = globe.computePositionFromPoint(wwTo);

		// Projects LonLat to expected projection
		double[] coods1 = projection.project(from.longitude.degrees, from.latitude.degrees);
		double[] coods2 = projection.project(to.longitude.degrees, to.latitude.degrees);
		GeoBoxBuilder builder = new GeoBoxBuilder(projection);
		builder.addPoint(coods1[0], coods1[1]);
		builder.addPoint(coods2[0], coods2[1]);
		return builder.build();
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(parameters.toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(WWSwathLayerParameters.SESSION_KEY).ifPresent(this::fitParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected void doRender(DrawContext dc) {
		super.doRender(dc);
		tooltip.render(dc);
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		super.dispose();
		swathLines.clear();
		pingTimes.clear();
		unhookListeners();
	}

	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (enabled) {
			hookListeners();
		} else {
			unhookListeners();
		}
	}

	/**
	 * Add the mouse listeners
	 */
	private void hookListeners() {
		if (mouseListener == null) {
			mouseListener = new MouseListener();
			geographicViewService.getWwd().getInputHandler().addMouseListener(mouseListener);
			geographicViewService.getWwd().getInputHandler().addMouseMotionListener(mouseListener);
			focusListener = new FocusListenerImpl();
			geographicViewService.getWwd().addFocusListener(focusListener);
			globalCursorService.addListener(globalCursorListener);
		}
	}

	/**
	 * Remove the mouse listeners
	 */
	private void unhookListeners() {
		if (mouseListener != null) {
			geographicViewService.getWwd().getInputHandler().removeMouseListener(mouseListener);
			geographicViewService.getWwd().getInputHandler().removeMouseMotionListener(mouseListener);
			mouseListener = null;
			geographicViewService.getWwd().removeFocusListener(focusListener);
			focusListener = null;
			globalCursorService.removeListener(globalCursorListener);
		}
		unhighlight();
	}

	/**
	 * Mouse listener for the click and drag during the selection
	 */
	private class MouseListener extends MouseAdapter {

		/**
		 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseClicked(MouseEvent e) {
			manageSwathSelection(highlightedSounderNcInfo, highlightedSwath);
			highlightedSounderNcInfo = null;
			highlightedSwath = -1;
			tooltip.getAttributes().setVisible(false);

		}

		/**
		 * @see java.awt.event.MouseAdapter#mouseMoved(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseMoved(MouseEvent e) {
			highlightSwathAt(e.getX(), e.getY());
		}
	}

	/**
	 * Focus listener
	 */
	private class FocusListenerImpl implements FocusListener {

		@Override
		public void focusGained(FocusEvent e) {
			//
		}

		@Override
		public void focusLost(FocusEvent e) {
			unhighlight();
		}
	}

	/**
	 * Reacts to the reception of a GlobalCursor.<br>
	 * If a time is present, search the closest swath to this time and select it
	 */
	private void onGlobalCursor(GlobalCursor globalcursor) {
		if (Optional.of(this).equals(globalcursor.source())) // Cursor emitted by myself
			return;
		globalcursor.time().ifPresent(expectedTime -> // Search sounder file for the instant
		pingTimes.keySet().stream()//
				.filter(sounderNcInfo -> {
					var startEndDate = sounderNcInfo.getStartEndDate().orElse(null);
					return startEndDate != null
							&& DateUtils.between(expectedTime, startEndDate.getFirst(), startEndDate.getSecond());
				})//
				.findFirst()//
				.ifPresent(sounderInfoAtInstant -> {
					// Search nearest swath of the instant
					LongLoadableLayer1D pingTimeLayer = pingTimes.get(sounderInfoAtInstant);
					int swathIdxAtInstant = DateUtils.binarySearch(expectedTime, sounderInfoAtInstant.getCycleCount(),
							swathIdx -> gainInstant(pingTimeLayer, swathIdx));
					manageSwathSelection(sounderInfoAtInstant, swathIdxAtInstant);
				}));
	}

	/** @return the ping time or null when error occures */
	private Instant gainInstant(LongLoadableLayer1D pingTimeLayer, int swathIdx) {
		return DateUtils.epochNanoToInstant(pingTimeLayer.get(swathIdx));
	}
}
