#version 150 compatibility

in float value;
out float outValue;

in float index;
out float outIndex;

void main()
{
	outValue = value;
	outIndex = index;
	vec4 point = gl_Vertex;
    gl_Position = gl_ModelViewProjectionMatrix * point;
} 