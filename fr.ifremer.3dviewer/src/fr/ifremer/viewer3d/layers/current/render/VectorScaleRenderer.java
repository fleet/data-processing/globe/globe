package fr.ifremer.viewer3d.layers.current.render;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

import org.eclipse.e4.core.di.annotations.Creatable;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import fr.ifremer.globe.core.utils.color.GColor;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.TextRenderer;
import gov.nasa.worldwind.util.OGLStackHandler;
import gov.nasa.worldwind.util.OGLTextRenderer;

/**
 * Vector scale Renderable.
 */
@Creatable
public class VectorScaleRenderer implements Renderable {
	/** Scale applyed to these vectors */
	private final VectorRenderer vectorRenderer;

	/** Size of the scale */
	private Dimension size = new Dimension(150, 10);
	/** Font for the label */
	private Font defaultFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

	/** When scale was drawn for the last time */
	private long lastDrawnTimeStamp;

	/**
	 * Constructor
	 */
	public VectorScaleRenderer(VectorRenderer vectorRenderer) {
		this.vectorRenderer = vectorRenderer;
	}

	/** {@inheritDoc} */
	@Override
	public void render(DrawContext dc) {
		if (dc.isContinuous2DGlobe() && lastDrawnTimeStamp == dc.getFrameTimeStamp())
			return;

		// Vector visible ?
		if (!vectorRenderer.isVisible() || !vectorRenderer.getSector().intersects(dc.getVisibleSector()))
			return;
		Position centerPosition = dc.getViewportCenterPosition();
		if (centerPosition != null) {
			Vec4 groundTarget = dc.getGlobe().computePointFromPosition(centerPosition);
			double eyeDistance = dc.getView().getEyePoint().distanceTo3(groundTarget);
			double pixelCountForOneMeter = 1d / dc.getView().computePixelSizeAtDistance(eyeDistance);

			double renderingScaleFactor = vectorRenderer.getRenderingScaleFactor();
			if (pixelCountForOneMeter > 0d)
				dc.addOrderedRenderable(new OrderedScale(pixelCountForOneMeter, renderingScaleFactor));

			lastDrawnTimeStamp = dc.getFrameTimeStamp();
		}
	}

	/**
	 * OrderedRenderable to request the scale rendering
	 */
	private class OrderedScale implements OrderedRenderable {
		private double pixelCountForOneMeter;
		private double renderingScaleFactor;

		public OrderedScale(double pixelForOneMeter, double renderingScaleFactor) {
			this.pixelCountForOneMeter = pixelForOneMeter;
			this.renderingScaleFactor = renderingScaleFactor;
		}

		@Override
		public double getDistanceFromEye() {
			// On the top
			return 0;
		}

		@Override
		public void pick(DrawContext dc, Point pickPoint) {
			// No picking
		}

		@Override
		public void render(DrawContext dc) {
			GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
			OGLStackHandler ogsh = new OGLStackHandler();
			try {
				ogsh.pushAttrib(gl, GL2.GL_TRANSFORM_BIT);

				gl.glDisable(GL.GL_DEPTH_TEST);

				double width = size.width;
				double height = size.height;

				// Load a parallel projection with xy dimensions (viewportWidth, viewportHeight)
				// into the GL projection matrix.
				java.awt.Rectangle viewport = dc.getView().getViewport();
				ogsh.pushProjectionIdentity(gl);
				double maxwh = width > height ? width : height;
				gl.glOrtho(0d, viewport.width, 0d, viewport.height, -0.6 * maxwh, 0.6 * maxwh);

				ogsh.pushModelviewIdentity(gl);

				Vec4 locationSW = computeLocation(viewport);
				gl.glTranslated(locationSW.x(), locationSW.y(), locationSW.z());

				// Compute scale size in real world
				Double scaleSizeInMeter = width / pixelCountForOneMeter / renderingScaleFactor;

				// Draw scale
				gl.glEnable(GL.GL_BLEND);
				gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

				GColor color = vectorRenderer.getParameters().defaultColor();
				gl.glColor4d(color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(),
						vectorRenderer.getParameters().opacity());
				drawArrow(dc, width, height);

				// Draw label
				String label = String.format("%.2f m/s", scaleSizeInMeter);
				gl.glLoadIdentity();
				gl.glDisable(GL.GL_CULL_FACE);
				drawLabel(dc, label, locationSW.add3(new Vec4(width / 2, height, 0)));
			} finally {
				gl.glColor4d(1d, 1d, 1d, 1d); // restore the default OpenGL color
				gl.glEnable(GL.GL_DEPTH_TEST);

				if (!dc.isPickingMode()) {
					gl.glBlendFunc(GL.GL_ONE, GL.GL_ZERO); // restore to default blend function
					gl.glDisable(GL.GL_BLEND); // restore to default blend state
				}

				ogsh.pop(gl);
			}
		}

		private Vec4 computeLocation(java.awt.Rectangle viewport) {
			double scaledWidth = size.width;
			double x = viewport.getWidth() - scaledWidth - 180;
			double y = 0d + 20;
			return new Vec4(x, y, 0);
		}

		// Draw scale graphic
		private void drawArrow(DrawContext dc, double width, double height) {
			GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
			gl.glBegin(GL.GL_LINE_STRIP);
			gl.glVertex3d(0, height / 2, 0);
			gl.glVertex3d(width, height / 2, 0);
			gl.glVertex3d(width - 10, height / 2 - 6, 0);
			gl.glVertex3d(width, height / 2, 0);
			gl.glVertex3d(width - 10, height / 2 + 6, 0);
			gl.glEnd();
		}

		// Draw the scale label
		private void drawLabel(DrawContext dc, String text, Vec4 screenPoint) {
			TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(dc.getTextRendererCache(), defaultFont);

			Rectangle2D nameBound = textRenderer.getBounds(text);
			int x = (int) (screenPoint.x() - nameBound.getWidth() / 2d);
			int y = (int) screenPoint.y();

			textRenderer.begin3DRendering();
			GColor color = vectorRenderer.getParameters().defaultColor();
			textRenderer.setColor(color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(),
					vectorRenderer.getParameters().opacity());
			textRenderer.draw(text, x, y);

			textRenderer.end3DRendering();
		}
	}
}
