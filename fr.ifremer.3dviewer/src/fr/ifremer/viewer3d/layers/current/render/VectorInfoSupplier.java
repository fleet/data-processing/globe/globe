package fr.ifremer.viewer3d.layers.current.render;

import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.geom.Position;

/**
 * Supply characteristics of vectors to render
 */
public interface VectorInfoSupplier {
	/** @return the Nb of vector to render */
	int getVectorCount();

	/** @return the origin of the vector at the specified index */
	Position getOrigin(int vectorIndex) throws GIOException;

	/** @return the value of the vector at the specified index */
	double getValue(int vectorIndex) throws GIOException;

	/** @return the heading of the vector at the specified index */
	double getHeading(int vectorIndex) throws GIOException;

}
