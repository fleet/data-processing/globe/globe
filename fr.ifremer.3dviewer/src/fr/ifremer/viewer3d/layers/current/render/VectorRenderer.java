package fr.ifremer.viewer3d.layers.current.render;

import java.awt.Color;
import java.awt.Point;
import java.nio.IntBuffer;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.IntFunction;
import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;

import jakarta.inject.Inject;
import javax.swing.SwingUtilities;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;
import com.jogamp.opengl.util.texture.Texture;

import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ogl.util.PaletteTexture;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.SceneController;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;

/**
 * Renderer of vectors (arrow).
 */
@Creatable
public class VectorRenderer implements OrderedRenderable, Disposable {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(VectorRenderer.class);

	/** How many vertices are needed to draw a vector */
	private static final int VERTICES_PER_VECTOR = 4;
	/** How many lines are needed to draw a vector */
	private static final int LINES_PER_VECTOR = 3;

	/** Shader programs source code */
	private static final String SHADER_VERT = VectorRenderer.class.getResource("shader/vector.vert").getFile();
	private static final String SHADER_FRAG = VectorRenderer.class.getResource("shader/vector.frag").getFile();

	/** Attribute names in shaders */
	private static final String INDEX_ATTRIB_NAME = "index";
	private static final String VALUE_ATTRIB_NAME = "value";

	@Inject
	private IGeographicViewService geographicViewService;

	/** Picking management */
	private PickSupport pickSupport = new PickSupport();
	/** Supplier of cursor. Invoked with the index of the selected vector. */
	private IntFunction<GlobalCursor> globalCursorSupplier;
	/** Current layer during picking process */
	private Layer pickLayer;

	/** Global sector containing the vectors */
	private Sector sector = Sector.EMPTY_SECTOR;

	/** True to enable the render */
	private boolean visible;
	/** True when vectors are selected */
	private boolean selected;

	/** Rendering parameters */
	private VectorRendererParameters parameters = VectorRendererParameters.getDefault();

	/** Shader programs used by this renderer */
	private int shaderProgram = 0;
	/** Used to save/restore OpenGL ES state */
	private float[] vectorSizeToRestore = new float[1];

	/** Index buffer used by glDrawElements */
	protected IntBuffer drawIndexes;
	/** VBO of positions */
	private final VBO vertexVbo = new VBO(3);
	/** VBO of values */
	private final VBO valueVbo = new VBO(1);
	/** VBO of index of vectors */
	private final VBO indexVbo = new VBO(1);
	/** True when model of vector has changed */
	private AtomicBoolean modelHasChanged = new AtomicBoolean();

	/** Palette of color */
	private final PaletteTexture palette = new PaletteTexture();

	/** Cartesian point from a geographic center position. */
	private Vec4 referenceCenter;
	/** Exaggeration apply to the vector rendering. */
	private double renderingScaleFactor = 1d;

	/**
	 * Accept a set of vectors
	 * 
	 * @param positionSupplier Supplien of positions (lon/lat/elevation)
	 * @param valueSupplier Value of each vector. Floats used to compute the color if the contrastModel.
	 */
	public void acceptVectors(VectorInfoSupplier infoSupplier, boolean clampToGround, double verticalExaggeration,
			int scaleFactor) {
		this.renderingScaleFactor = 100.0d * scaleFactor;
		int vectorCount = infoSupplier.getVectorCount();
		if (vectorCount <= 0)
			return;
		logger.debug("Accepting a new set of {} vectors", vectorCount);
		try {
			computeSectorAndReferenceCenter(infoSupplier, clampToGround, verticalExaggeration);

			int verticesCount = vectorCount * VERTICES_PER_VECTOR;
			drawIndexes = Buffers.newDirectIntBuffer(vectorCount * LINES_PER_VECTOR * 2);

			// Allocate buffers
			if (vertexVbo.getElementCount() != verticesCount) {
				vertexVbo.allocateBuffer(verticesCount);
				valueVbo.allocateBuffer(verticesCount);
				indexVbo.allocateBuffer(verticesCount);
			}
			for (int vectorIndex = 0; vectorIndex < vectorCount; vectorIndex++) {
				// Add 4 vertices to the buffer
				fillVertexBuffer(infoSupplier.getOrigin(vectorIndex), infoSupplier.getValue(vectorIndex),
						infoSupplier.getHeading(vectorIndex), clampToGround, verticalExaggeration);
				fillIndexBuffer(vectorIndex);
			}

			drawIndexes.rewind();
			vertexVbo.rewind();
			indexVbo.rewind();

			modelHasChanged.set(true);
		} catch (GIOException e) {
			dispose();
			logger.warn("IO error while reading vectors", e);
		}
	}

	/**
	 * Compute 4 vertices to draw an arrow and fill VBO vertexBuffer with them
	 * 
	 * <pre>
	 *  Vertex identification :
	 *  
	 *                          V3
	 *                           \
	 *                            \
	 *      V1 --------------------> V2
	 *                            /
	 *                           / 
	 *                          V4
	 * </pre>
	 * 
	 */
	private void fillVertexBuffer(Position vectorOrigin, double vectorValue, double vectorHeading,
			boolean clampToGround, double verticalExaggeration) {
		Globe globe = geographicViewService.getWwd().getModel().getGlobe();

		vectorOrigin = computeOriginOfVector(vectorOrigin, clampToGround, verticalExaggeration);

		// Vertex of tail point (origin of vector) : V1
		Vec4 v1 = globe.computePointFromPosition(vectorOrigin);
		fillVertexBuffer(v1);

		Position v2Position = computeVerticePosition(vectorOrigin, vectorValue, vectorHeading, globe);
		Vec4 v2 = globe.computePointFromPosition(v2Position);
		fillVertexBuffer(v2);

		float headSize = (float) (0.02d + (renderingScaleFactor / 1000000d));
		Position v3Position = computeVerticePosition(v2Position, headSize, vectorHeading + 160f, globe);
		Vec4 v3 = globe.computePointFromPosition(v3Position);
		fillVertexBuffer(v3);

		Position v4Position = computeVerticePosition(v2Position, headSize, vectorHeading - 160f, globe);
		Vec4 v4 = globe.computePointFromPosition(v4Position);
		fillVertexBuffer(v4);
	}

	/** Computes the location on a great circle arc with the given starting position, heading, and distance */
	private Position computeVerticePosition(Position fromPosition, double distance, double heading, Globe globe) {
		Angle arcDistance = Angle.fromRadians(distance * renderingScaleFactor / globe.getRadius());
		LatLon latLon = LatLon.greatCircleEndPosition(fromPosition, Angle.fromDegrees(heading), arcDistance);
		return new Position(latLon, fromPosition.elevation);
	}

	/**
	 * For the following arrow, fill the drawIndexes to render the lines [V1, V2], [V2, V3] and [V2, V4]
	 * 
	 * <pre>
	 *  Vertex identification :
	 *  
	 *                          V3
	 *                           \
	 *                            \
	 *      V1 --------------------> V2
	 *                            /
	 *                           / 
	 *                          V4
	 * </pre>
	 * 
	 */
	private void fillIndexBuffer(int vectorIndex) {
		// Connect the vertices by 2 to form the lines constituting the arrow
		int v1Index = vectorIndex * VERTICES_PER_VECTOR;
		int v2Index = v1Index + 1;
		int v3Index = v2Index + 1;
		int v4Index = v3Index + 1;
		drawIndexes.put(v1Index).put(v2Index);
		drawIndexes.put(v2Index).put(v3Index);
		drawIndexes.put(v2Index).put(v4Index);
		IntStream.range(0, VERTICES_PER_VECTOR).forEach(i -> indexVbo.put(vectorIndex));
	}

	/** Fill the value VBO */
	private void fillValueBuffer(IntToDoubleFunction colorValueSupplier) {
		if (valueVbo.getBuffer() != null) {
			valueVbo.rewind();
			int vectorCount = valueVbo.getElementCount() / VERTICES_PER_VECTOR;
			for (int vectorIndex = 0; vectorIndex < vectorCount; vectorIndex++) {
				float value = (float) colorValueSupplier.applyAsDouble(vectorIndex);
				for (int i = 0; i < VERTICES_PER_VECTOR; i++)
					valueVbo.put(value);
			}
			valueVbo.rewind();
			modelHasChanged.set(true);
		}
	}

	/**
	 * Add coordinates of a vertex in the VBO vertexBuffer
	 */
	private void fillVertexBuffer(Vec4 vertex) {
		vertex = vertex.subtract3(referenceCenter);
		vertexVbo.put(vertex.x);
		vertexVbo.put(vertex.y);
		vertexVbo.put(vertex.z);
	}

	/** Compute the position of the origin of the vector */
	private Position computeOriginOfVector(Position vectorOrigin, boolean clampToGround, double verticalExaggeration) {
		if (clampToGround) {
			double elevation = geographicViewService.getElevationModel().getElevation(vectorOrigin.latitude,
					vectorOrigin.longitude);
			vectorOrigin = new Position(vectorOrigin.latitude, vectorOrigin.longitude,
					elevation * verticalExaggeration);
		} else if (verticalExaggeration != 1d || parameters.verticalOffset() != 0f) {
			vectorOrigin = new Position(vectorOrigin.latitude, vectorOrigin.longitude,
					(vectorOrigin.elevation + parameters.verticalOffset()) * verticalExaggeration);
		}
		return vectorOrigin;
	}

	/** {@inheritDoc} */
	@Override
	public void render(DrawContext dc) {
		// Are the conditions met to draw
		if (!isVisible() || vertexVbo.getElementCount() == 0 || !sector.intersects(dc.getVisibleSector())) {
			// Free memory
			GL2 gl = dc.getGL().getGL2();
			disposeVBO(gl);
			return;
		}

		// Does SceneController is currently rendering OrderedRenderable ?
		if (!dc.isOrderedRenderingMode()) {
			pickLayer = dc.getCurrentLayer();
			dc.addOrderedSurfaceRenderable(this);
		} else {
			draw(dc, 0);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void pick(DrawContext dc, Point pickVector) {
		try {
			this.pickSupport.beginPicking(dc);
			Color firstPickedColor = dc.getUniquePickColorRange(vertexVbo.getElementCount() / VERTICES_PER_VECTOR);
			if (firstPickedColor != null) {
				int firstPickedRgb = firstPickedColor.getRGB();
				this.pickSupport.addPickableObjectRange(firstPickedRgb, vertexVbo.getElementCount(), color -> {
					PickedVectorIndex pickedVectorIndex = new PickedVectorIndex(this, color - firstPickedRgb);
					var result = new PickedObject(color, pickedVectorIndex);
					result.setParentLayer(this.pickLayer);
					if (globalCursorSupplier != null)
						pickedVectorIndex.setValue(GlobalCursor.AV_KEY,
								globalCursorSupplier.apply(pickedVectorIndex.getVectorIndex()));
					return result;
				});
				draw(dc, firstPickedRgb);
			} else {
				logger.error("Not enough colors to satisfy the requested picking range.");
			}
		} finally {
			this.pickSupport.endPicking(dc);
			this.pickSupport.resolvePick(dc, pickVector, this.pickLayer);
		}
	}

	/**
	 * Draw the vectors
	 * 
	 * @param firstPickedRgb : when positive, indicates the first color to apply to the first vector (instead of
	 *            colorize with the palette).
	 */
	private void draw(DrawContext dc, int firstPickedRgb) {
		GL2 gl = dc.getGL().getGL2();
		try {
			dc.getView().pushReferenceCenter(dc, referenceCenter);
			beginDrawing(gl);
			doDraw(gl, firstPickedRgb);
		} catch (GIOException | GLException e) {
			logger.error("Unable to draw lines layer {}", e.getMessage());
		} finally {
			endDrawing(gl);
			dc.getView().popReferenceCenter(dc);
		}
	}

	/**
	 * Before drawing
	 */
	private void beginDrawing(GL2 gl) {
		gl.glPushAttrib(GL.GL_DEPTH_BUFFER_BIT | GL.GL_BLEND);
		gl.glGetFloatv(GL.GL_POINT_SIZE, vectorSizeToRestore, 0);

		// For opacity
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
	}

	/**
	 * Restore drawing state changed in beginDrawing to the default.
	 */
	private void endDrawing(GL2 gl) {
		gl.glPopAttrib();
		gl.glPointSize(vectorSizeToRestore[0]);
	}

	/**
	 * Render the vectors
	 */
	private void doDraw(GL2 gl, int firstPickedRgb) throws GIOException {
		// Get the shader program ID for the future drawing
		int shaderProgramID = getShaderProgramID(gl);
		// Initialize the uniforms of the shader program
		gl.glUseProgram(shaderProgramID);

		// Bind VBOs
		if (modelHasChanged.compareAndSet(true, false)) {
			disposeVBO(gl);
		}
		vertexVbo.bindAndLoad(gl);
		valueVbo.bindAndLoad(gl);
		indexVbo.bindAndLoad(gl);

		// Draw the OpenGL shape from the initialized buffers
		configureShaderAndDraw(gl, shaderProgramID, 1, firstPickedRgb);

		// Returns to the main shader program
		if (shaderProgramID != 0) {
			gl.glUseProgram(0);
		}
	}

	/**
	 * Configure all the OpenGL drawing variables.
	 */
	private void configureShaderAndDraw(GL2 gl, int shaderProgram, float verticalExaggeration, int firstPickedRgb) {
		// Initialize the vectorer to buffers
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glEnableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, VALUE_ATTRIB_NAME));
		gl.glEnableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, INDEX_ATTRIB_NAME));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "verticalExaggeration"), verticalExaggeration);

		// Default color
		GColor defaultColor = selected ? GColor.WHITE : parameters.defaultColor();
		gl.glUniform4f(gl.glGetUniformLocation(shaderProgram, "defaultColor"), defaultColor.getFloatRed(),
				defaultColor.getFloatGreen(), defaultColor.getFloatBlue(), defaultColor.getAlpha());

		// First color code in case of picking
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "firstPickedColor"), firstPickedRgb);

		// Opacity
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), parameters.opacity());

		// Contrast model. When absent, set contrast range to NaN (forcing shader to use default color).
		ColorContrastModel colorContrastModel = parameters.colorContrastModel()
				.orElse(new ColorContrastModelBuilder().setContrastMin(Float.NaN).build());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minContrast"), colorContrastModel.contrastMin());
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxContrast"), colorContrastModel.contrastMax());

		// sending color map texture to the GPU
		Texture paletteTexture = palette.getColorPalette(gl, colorContrastModel.colorMapIndex(),
				colorContrastModel.invertColor());

		gl.glActiveTexture(GL.GL_TEXTURE0);
		paletteTexture.bind(gl);

		// Increase the size of vector in case of picking to make it easier to select
		float vectorSize = firstPickedRgb != 0 ? 4f : parameters.width();
		vectorSize = selected ? 1.5f * vectorSize : vectorSize;
		drawVectors(gl, vectorSize);

		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glDisableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, VALUE_ATTRIB_NAME));
		gl.glDisableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, INDEX_ATTRIB_NAME));
	}

	/** Draw all lines and vectors */
	private void drawVectors(GL2 gl, float vectorSize) {
		// Vertices
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vertexVbo.getBufferName()[0]);
		gl.glVertexPointer(vertexVbo.getElementSize(), GL.GL_FLOAT, 0, 0);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

		// Values
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, valueVbo.getBufferName()[0]);
		gl.glVertexAttribPointer(gl.glGetAttribLocation(shaderProgram, VALUE_ATTRIB_NAME), valueVbo.getElementSize(),
				GL.GL_FLOAT, false, 0, 0);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

		// Index
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, indexVbo.getBufferName()[0]);
		gl.glVertexAttribPointer(gl.glGetAttribLocation(shaderProgram, INDEX_ATTRIB_NAME), indexVbo.getElementSize(),
				GL.GL_FLOAT, false, 0, 0);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

		gl.glLineWidth(vectorSize);

		gl.glDrawElements(GL.GL_LINES, drawIndexes.capacity(), GL.GL_UNSIGNED_INT, drawIndexes);
	}

	/**
	 * Compute the sector and reference center for the set of vectors
	 */
	private void computeSectorAndReferenceCenter(VectorInfoSupplier infoSupplier, boolean clampToGround,
			double verticalExaggeration) throws GIOException {
		GeoBoxBuilder geoBoxBuilder = new GeoBoxBuilder();
		double minElevation = Double.POSITIVE_INFINITY;
		double maxElevation = Double.NEGATIVE_INFINITY;
		int vectorCount = infoSupplier.getVectorCount();
		for (int index = 0; index < vectorCount; index++) {
			Position origin = infoSupplier.getOrigin(index);
			double elevation = origin.elevation;
			if (clampToGround) {
				elevation = geographicViewService.getElevationModel().getElevation(origin.latitude, origin.longitude);
			}
			minElevation = Math.min(minElevation, elevation * verticalExaggeration);
			maxElevation = Math.max(maxElevation, elevation * verticalExaggeration);
			geoBoxBuilder.addPoint(origin.longitude.degrees, origin.latitude.degrees);
		}
		sector = GeoBoxUtils.convert(geoBoxBuilder.build());

		referenceCenter = geographicViewService
				.computePointFromPosition(new Position(sector.getCentroid(), (minElevation + maxElevation) / 2d));
	}

	/**
	 * Get the ID of the shader program used by this renderer.
	 */
	private int getShaderProgramID(GL2 gl) throws GIOException {
		if (shaderProgram == 0) {
			// Compile and generate a shader program ID and add it to the shader prodram renderer list
			shaderProgram = ShaderUtil.createShader(this, gl, SHADER_VERT, SHADER_FRAG);
		}

		return shaderProgram;
	}

	/**
	 * @see gov.nasa.worldwind.layers.AbstractLayer#dispose()
	 */
	@Override
	public void dispose() {
		SwingUtilities.invokeLater(() -> {
			GLContext glContext = getGLContext();
			if (glContext != null) {

				if (shaderProgram != 0) {
					glContext.getGL().getGL2().glDeleteShader(shaderProgram);
					shaderProgram = 0;
				}
				disposeVBO(glContext.getGL());
				glContext.release();
			}
		});
	}

	private void disposeVBO(GL gl) {
		vertexVbo.delete(gl);
		valueVbo.delete(gl);
		indexVbo.delete(gl);
	}

	/**
	 * @return the <code>DrawContext</code>s </code>com.jogamp.opengl.GLContext</code>.
	 */
	private GLContext getGLContext() {
		GLContext result = null;
		SceneController sceneController = Viewer3D.isUp() ? Viewer3D.getWwd().getSceneController() : null;
		if (sceneController != null) {
			DrawContext drawContext = sceneController.getDrawContext();
			if (drawContext != null) {
				GLContext glContext = drawContext.getGLContext();
				if (glContext != null && glContext.makeCurrent() != GLContext.CONTEXT_NOT_CURRENT) {
					result = glContext;
				}
			}
		}
		return result;

	}

	/** {@inheritDoc} */
	@Override
	public double getDistanceFromEye() {
		return 0; // Value never used by WW
	}

	/**
	 * @return the {@link #visible}
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the {@link #visible} to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the {@link #parameters}
	 */
	public VectorRendererParameters getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the {@link #parameters} to set
	 */
	public void setParameters(VectorRendererParameters parameters) {
		if (!parameters.colorContrastModel().equals(this.parameters.colorContrastModel())) {
			fillValueBuffer(parameters.colorValueSupplier());
		}
		this.parameters = parameters;
	}

	/**
	 * @param globalCursorSupplier the {@link #globalCursorSupplier} to set
	 */
	public void setGlobalCursorSupplier(IntFunction<GlobalCursor> globalCursorSupplier) {
		this.globalCursorSupplier = globalCursorSupplier;
	}

	/**
	 * @return the {@link #selected}
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the {@link #selected} to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
		geographicViewService.refresh();
	}

	/**
	 * @return the {@link #pickLayer}
	 */
	public Layer getPickLayer() {
		return pickLayer;
	}

	/**
	 * @return the {@link #referenceCenter}
	 */
	public Vec4 getReferenceCenter() {
		return referenceCenter;
	}

	/**
	 * @return the {@link #renderingScaleFactor}
	 */
	public double getRenderingScaleFactor() {
		return renderingScaleFactor;
	}

	/**
	 * @return the {@link #sector}
	 */
	public Sector getSector() {
		return sector;
	}

}
