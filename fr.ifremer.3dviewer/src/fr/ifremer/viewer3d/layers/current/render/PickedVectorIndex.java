package fr.ifremer.viewer3d.layers.current.render;

import gov.nasa.worldwind.avlist.AVListImpl;

/**
 * Index of the vector picked in a VectorRenderer
 */
public class PickedVectorIndex extends AVListImpl {
	private final VectorRenderer vectorRenderer;
	private final int vectorIndex;

	/**
	 * Constructor
	 */
	public PickedVectorIndex(VectorRenderer vectorRenderer, int vectorIndex) {
		this.vectorRenderer = vectorRenderer;
		this.vectorIndex = vectorIndex;
	}

	/**
	 * @return the {@link #vectorRenderer}
	 */
	public VectorRenderer getVectorRenderer() {
		return vectorRenderer;
	}

	/**
	 * @return the {@link #vectorIndex}
	 */
	public int getVectorIndex() {
		return vectorIndex;
	}

}
