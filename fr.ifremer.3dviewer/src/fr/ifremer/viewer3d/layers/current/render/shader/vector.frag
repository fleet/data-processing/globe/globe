#version 150 compatibility

uniform vec4 defaultColor;
uniform float firstPickedColor;
uniform float opacity;
uniform sampler2D palette;
uniform float minContrast;
uniform float maxContrast;

in float outValue;
in float outIndex;

void main()
{
	if(firstPickedColor > 0) {
		// Compute the picking color 
		int colorMultiplexed = int(firstPickedColor + outIndex);
		gl_FragColor.b = float(colorMultiplexed & 255) / 255.0;
		gl_FragColor.g = float((colorMultiplexed>> 8) & 255) / 255.0;
		gl_FragColor.r = float((colorMultiplexed>>16) & 255) / 255.0;
		gl_FragColor.a = 1.0;
	} else if( isnan(minContrast) ) { // No contrast model present, use default color
		gl_FragColor = defaultColor;
		gl_FragColor.a = opacity;
	} else if(isnan(outValue)) {
		// Invalid value
		gl_FragColor = vec4(224.0/ 255.0, 224.0/ 255.0, 224.0/ 255.0, opacity / 2.0);
	} else {
		float normalizedValue = (outValue - minContrast) / (maxContrast - minContrast);
		gl_FragColor = vec4(texture2D(palette, vec2(normalizedValue, 0)).rgb, opacity);
	}
	
}
