package fr.ifremer.viewer3d.layers.current.render;

import java.util.Optional;
import java.util.function.IntToDoubleFunction;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;

/**
 * Rendering parameters of the VectorRenderer
 */
public record VectorRendererParameters(
		/** Vector width. */
		float width,
		/** Opacity. */
		float opacity,
		/** Color by default */
		GColor defaultColor,
		/** Color model */
		Optional<ColorContrastModel> colorContrastModel, //
		/** Value of vector used to define the color */
		IntToDoubleFunction colorValueSupplier, //
		float verticalOffset) {

	/**
	 * @return a default instance of parameters
	 */
	public static VectorRendererParameters getDefault() {
		return new VectorRendererParameters(2f, 1f, GColor.ORANGE, Optional.empty(), i -> 0d, 0f);
	}
}
