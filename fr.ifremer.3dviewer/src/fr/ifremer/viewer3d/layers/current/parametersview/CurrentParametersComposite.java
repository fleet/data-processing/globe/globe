package fr.ifremer.viewer3d.layers.current.parametersview;

import java.util.Comparator;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.stream.Stream;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.apache.commons.lang.math.FloatRange;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.widgets.LabelFactory;
import org.eclipse.jface.widgets.SpinnerFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.io.adcp.AdcpInfo;
import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.current.WWCurrentLayerParameters;
import fr.ifremer.globe.ui.widget.AbstractParametersCompositeWithTabs;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdWidget;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.colorscale.parametersview.ColorScaleParametersComposite;
import fr.ifremer.viewer3d.layers.sync.current.WWCurrentLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Composite that changes parameters of IWWCurrentLayer.
 */
public class CurrentParametersComposite extends AbstractParametersCompositeWithTabs {

	/** The layer parameters synchronizer get from Context **/
	@Inject
	private WWCurrentLayerSynchronizer layersSynchronizer;

	/** Model used in this Composite */
	private final IWWCurrentLayer currentLayer;

	/** Current vertical offset model */
	private EnabledNumberModel verticalOffsetModel = new EnabledNumberModel(false, 0d);

	private Spinner widthSpinner;
	private Spinner scaleSpinner;
	private ColorPicker absoluteColorPicker;
	private ComboViewer variableColorComboViewer;
	private ColorContrastWidget colorContrastWidget;
	private SliderComposite opacitySliderComposite;
	private Button variableColorRadioButton;
	private Spinner vectorCountSpinner;

	private Composite colorScaleTab;

	private Button btnEnableColorScale;

	private ColorScaleParametersComposite colorScaleComposite;

	private FloatThresholdModel rangeModel;

	/**
	 * Constructor.
	 */
	public CurrentParametersComposite(Composite parent, IWWCurrentLayer currentLayer) {
		super(parent);
		this.currentLayer = currentLayer;
		ContextInitializer.inject(this);
	}

	@PostConstruct
	private void postConstruct() {
		initializeComposite();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void initializeComposite() {
		Label layerNameLabel = new Label(this, SWT.NONE);
		layerNameLabel.setText(currentLayer.getName());

		createSynchronizationGroup();
		createDisplayTab();
		createColorTab();
		createColorScaleTab();
		createPositionTab();
		if (currentLayer.getCurrentData().getFileInfo() instanceof AdcpInfo adcpinfo)
			createFilterTab(adcpinfo);
	}

	/** Synchronizer composite */
	private void createSynchronizationGroup() {
		var grpSynchronization = new LayerParametersSynchronizerComposite<>(this, layersSynchronizer, currentLayer);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(grpSynchronization);
		grpSynchronization.setText("Synchronization with other layers");
		grpSynchronization.adaptToLayer(currentLayer);
	}

	private void createDisplayTab() {
		Composite displayTab = createTab("Display");

		Group grpNavigationPoints = new Group(displayTab, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(grpNavigationPoints);
		grpNavigationPoints.setText("Vectors");
		grpNavigationPoints.setLayout(new GridLayout(2, false));

		// Width
		LabelFactory.newLabel(SWT.NONE)//
				.text("Width : ")//
				.layoutData(GridDataFactory.swtDefaults().create())//
				.create(grpNavigationPoints);

		widthSpinner = SpinnerFactory.newSpinner(SWT.BORDER) //
				.bounds(1, 10)//
				.onModify(e -> updateLayer())//
				.layoutData(GridDataFactory.fillDefaults().create())//
				.create(grpNavigationPoints);
		widthSpinner.setSelection(getParameters().width());

		// Scale factor
		LabelFactory.newLabel(SWT.NONE)//
				.text("Length : ")//
				.layoutData(GridDataFactory.swtDefaults().create())//
				.create(grpNavigationPoints);

		scaleSpinner = SpinnerFactory.newSpinner(SWT.BORDER) //
				.bounds(1, 1000)//
				.onModify(e -> updateLayer())//
				.layoutData(GridDataFactory.fillDefaults().create())//
				.create(grpNavigationPoints);
		scaleSpinner.setSelection(getParameters().scaleFactor());
	}

	private void createColorTab() {
		var colorTab = createTab("Color");
		colorTab.setLayout(new GridLayout(4, false));

		// color
		Button constantColorRadioButton = new Button(colorTab, SWT.RADIO);
		constantColorRadioButton.setText("Constant color :");

		// navigation line color
		absoluteColorPicker = new ColorPicker(colorTab, SWT.BORDER, getParameters().absoluteColor(),
				color -> updateLayer());
		GridData gdConstantColorPicker = new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1);
		gdConstantColorPicker.heightHint = 18;
		gdConstantColorPicker.widthHint = 50;
		absoluteColorPicker.setLayoutData(gdConstantColorPicker);

		// variable color
		variableColorRadioButton = new Button(colorTab, SWT.RADIO);
		variableColorRadioButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		variableColorRadioButton.setText("Variable color :");
		variableColorRadioButton.addListener(SWT.Selection, e -> updateLayer());

		// combobox to select the variable to use
		variableColorComboViewer = new ComboViewer(colorTab, SWT.READ_ONLY);
		Combo combo = variableColorComboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		variableColorComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		var input = new TreeSet<NavigationMetadataType<?>>(Comparator.comparing(mt -> mt.name.toLowerCase()));
		input.addAll(currentLayer.getColorContrastModelByMetadataType().keySet());
		variableColorComboViewer.setInput(input);
		variableColorComboViewer
				.setSelection(new StructuredSelection(getParameters().metadataUsedToGetColor().orElse(input.first())));
		variableColorComboViewer.setLabelProvider(
				LabelProvider.createTextProvider(obj -> ((NavigationMetadataType<?>) obj).getNameWithUnit()));
		variableColorComboViewer.addSelectionChangedListener(e -> {
			var selectedMetadataType = (NavigationMetadataType<?>) e.getStructuredSelection().getFirstElement();
			var colorContrastModel = currentLayer.getColorContrastModel(selectedMetadataType);
			colorContrastWidget.setModel(colorContrastModel);
			updateLayer();
			colorContrastWidget.setMinMaxSyncValues(layersSynchronizer.getMinMaxSyncValues(currentLayer));
		});

		Composite variableColorComposite = new Composite(colorTab, SWT.NONE);
		variableColorComposite.setLayout(new GridLayout(2, false));
		variableColorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));

		// color & contrast widget used if "variable" color is selected
		colorContrastWidget = new ColorContrastWidget(variableColorComposite, getParameters().colorContrastModel(), false);
		colorContrastWidget.setMinMaxSyncValues(layersSynchronizer.getMinMaxSyncValues(currentLayer));
		colorContrastWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		colorContrastWidget.subscribe(m -> updateLayer());

		Group grpOpacity = new Group(colorTab, SWT.NONE);
		grpOpacity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		grpOpacity.setText("Opacity");
		grpOpacity.setLayout(new GridLayout(1, false));
		opacitySliderComposite = new SliderComposite(grpOpacity, SWT.NONE, false, value -> updateLayer());
		opacitySliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		opacitySliderComposite.setValue(getParameters().opacity());

		Consumer<Boolean> variableColorSetEnabled = enabled -> {
			variableColorComposite.setEnabled(enabled);
			colorContrastWidget.setEnabled(enabled);
			variableColorComboViewer.getCombo().setEnabled(enabled);
			Stream.of(variableColorComposite.getChildren()).forEach(control -> control.setEnabled(enabled));
		};

		constantColorRadioButton.addListener(SWT.Selection, e -> {
			boolean isConstantColor = constantColorRadioButton.getSelection();
			absoluteColorPicker.setEnabled(isConstantColor);
			variableColorSetEnabled.accept(!isConstantColor);
			updateLayer();
		});

		// initialize enable state
		constantColorRadioButton.setSelection(!getParameters().isVariableColor());
		variableColorRadioButton.setSelection(getParameters().isVariableColor());
		absoluteColorPicker.setEnabled(!getParameters().isVariableColor());
		variableColorSetEnabled.accept(getParameters().isVariableColor());

	}

	protected void createColorScaleTab() {
		colorScaleTab = createTab("Color scale");
		colorScaleTab.setLayout(new GridLayout(1, false));

		btnEnableColorScale = new Button(colorScaleTab, SWT.CHECK);
		btnEnableColorScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnEnableColorScale.setText("Display color scale (available if 'Variable color' is selected)");
		btnEnableColorScale.addListener(SWT.Selection, e -> {
			currentLayer.getColorScaleLayer().ifPresent(layer -> layer.setEnabled(btnEnableColorScale.getSelection()));
			if (colorScaleComposite != null && !colorScaleComposite.isDisposed())
				colorScaleComposite.setEnabled(btnEnableColorScale.getSelection());
		});

		updateColorScaleTab();
	}

	private void updateColorScaleTab() {
		var optColorScaleLayer = currentLayer.getColorScaleLayer();
		if (optColorScaleLayer.isPresent() && (colorScaleComposite == null || colorScaleComposite.isDisposed())) {
			var colorScaleLayer = optColorScaleLayer.get();
			btnEnableColorScale.setEnabled(true);
			btnEnableColorScale.setSelection(colorScaleLayer.isEnabled());
			colorScaleComposite = new ColorScaleParametersComposite(colorScaleTab, colorScaleLayer);
			GridLayout gridLayout = new GridLayout(1, false);
			gridLayout.marginHeight = 0;
			gridLayout.marginWidth = 0;
			colorScaleComposite.setLayout(gridLayout);
			colorScaleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			colorScaleTab.layout();
		} else if (optColorScaleLayer.isEmpty() && colorScaleComposite != null && !colorScaleComposite.isDisposed()) {
			btnEnableColorScale.setEnabled(false);
			colorScaleComposite.dispose();
			colorScaleTab.layout();
		}
	}

	private void updateLayer() {
		if (opacitySliderComposite != null) {
			var displayParameters = new WWCurrentLayerParameters(widthSpinner.getSelection(), //
					opacitySliderComposite.getValue(), //
					absoluteColorPicker.getColor(), //
					variableColorRadioButton.getSelection(), //
					Optional.ofNullable((NavigationMetadataType<?>) variableColorComboViewer.getStructuredSelection()
							.getFirstElement()),
					colorContrastWidget.getModel(), //
					scaleSpinner.getSelection(), //
					verticalOffsetModel.enable, verticalOffsetModel.value.floatValue());

			currentLayer.setParameters(displayParameters);
			layersSynchronizer.synchonizeWith(currentLayer);
			updateColorScaleTab();
		}
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 */
	private void createPositionTab() {
		WWCurrentLayerParameters param = currentLayer.getParameters();
		verticalOffsetModel = new EnabledNumberModel(param.verticalOffsetEnabled(), param.verticalOffset());

		super.createPositionTab(verticalOffsetModel, newVerticalOffsetModel -> {
			verticalOffsetModel = newVerticalOffsetModel;
			updateLayer();
		});
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 *
	 * @return the built {@link FloatThresholdWidget}.
	 */
	private void createFilterTab(AdcpInfo adcpinfo) {
		Composite container = createTab("Filter");
		container.setLayout(new GridLayout(2, false));

		Button filterButton = new Button(container, SWT.PUSH);
		filterButton.setText("Apply filtering");
		filterButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false, 2, 1));
		filterButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			FloatRange rangeThreshold = null;
			if (rangeModel.enable)
				rangeThreshold = new FloatRange(rangeModel.minValue, rangeModel.maxValue);
			currentLayer.filter(
					new CurrentFiltering(vectorCountSpinner.getSelection(), Optional.ofNullable(rangeThreshold)));
		}));

		Label vectorCountLabel = new Label(container, SWT.NONE);
		vectorCountLabel.setText("Sampling :");

		int maxVectorCount = adcpinfo.getVectorCount();
		int vectorCount = getCurrentFiltering().sampling();
		vectorCountSpinner = new Spinner(container, SWT.BORDER);
		vectorCountSpinner.setValues(vectorCount, Math.min(10, vectorCount), maxVectorCount, 0, 1, 1000);
		vectorCountSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		vectorCountSpinner.setToolTipText("Maximum number of vector to display");

		// create threshold value widget
		FloatRange rangeLimits = adcpinfo.getRangeLimits();
		Optional<FloatRange> currentRange = getCurrentFiltering().range();

		rangeModel = new FloatThresholdModel(//
				currentRange.isPresent() ? currentRange.get().getMinimumFloat() : rangeLimits.getMinimumFloat(), //
				currentRange.isPresent() ? currentRange.get().getMaximumFloat() : rangeLimits.getMaximumFloat(), //
				currentRange.isPresent(), //
				rangeLimits.getMinimumFloat(), rangeLimits.getMaximumFloat());
		FloatThresholdWidget valueThreshold = new FloatThresholdWidget("Range threshold", container, rangeModel);
		valueThreshold.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		// response to event
		valueThreshold.getPublisher().subscribe(i -> this.rangeModel = i);

	}

	private WWCurrentLayerParameters getParameters() {
		return currentLayer.getParameters();
	}


	private CurrentFiltering getCurrentFiltering() {
		return currentLayer.getCurrentFiltering();
	}
}
