package fr.ifremer.viewer3d.layers.current;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.IntToDoubleFunction;

import javax.swing.SwingUtilities;

import org.apache.commons.lang.math.FloatRange;
import org.eclipse.swt.graphics.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.adcp.AdcpInfo;
import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.current.WWCurrentLayerParameters;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.current.render.VectorInfoSupplier;
import fr.ifremer.viewer3d.layers.current.render.VectorRenderer;
import fr.ifremer.viewer3d.layers.current.render.VectorRendererParameters;
import fr.ifremer.viewer3d.layers.current.render.VectorScaleRenderer;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

/**
 * Layer for current data.
 */
public class WWCurrentLayer extends RenderableLayer implements IWWCurrentLayer {

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(WWCurrentLayer.class);

	/** Session service */
	@Inject
	private ISessionService sessionService;
	/** 3D view service */
	@Inject
	private IGeographicViewService geographicViewService;
	@Inject
	private IWWLayerFactory wwLayerFactory;
	@Inject
	private WWFileLayerStoreModel layerStoreModel;
	@Inject
	private IProcessService processService;

	/** Current data received in constructor */
	private ICurrentData currentData;

	/** True when currentVectorModel have been modified and renderers must be updated */
	private boolean currentVectorModelUpdated = false;
	/** Last applied vertical exaggeration */
	private double verticalExaggeration = 1d;

	/** Display parameters **/
	private WWCurrentLayerParameters currentLayerParameters = new WWCurrentLayerParameters();
	/** Filtering parameters **/
	private CurrentFiltering currentFiltering;
	/** Scale factor currently used */
	private int currentScaleFactor = currentLayerParameters.scaleFactor();
	/** Verticale offset currently used */
	private float currentVerticalOffset = currentLayerParameters.verticalOffset();

	/** Renderable of vectors **/
	@Inject
	private VectorRenderer vectorRenderer;

	private VectorInfoSupplier currentVectorModel;

	private Map<NavigationMetadataType<?>, ColorContrastModel> colorContrastModelByMetadata = Map.of();
	/** Related color scale layer **/
	private Optional<IWWColorScaleLayer> optColorScaleLayer = Optional.empty();

	/**
	 * Constructor
	 */
	public WWCurrentLayer(ICurrentData currentData) {
		this.currentData = currentData;
		currentFiltering = currentData.getFiltering();
		ContextInitializer.inject(this);
		setName("Current of " + currentData.getFileInfo().getBaseName());
		setValue(AVKey.SECTOR, GeoBoxUtils.convert(currentData.getFileInfo().getGeoBox()));
	}

	/**
	 * Initialization after construction
	 */
	@PostConstruct
	public void postConstruct() {
		addRenderable(vectorRenderer);
		addRenderable(new VectorScaleRenderer(vectorRenderer));
		vectorRenderer.setVisible(true);
		vectorRenderer.setGlobalCursorSupplier(this::buildGlobalCursorForVector);
		loadCurrentModel();
		computeMinMaxMetadata();
		configureVectorRenderer();

		if (!colorContrastModelByMetadata.isEmpty()) {
			var colorContrastEntry = colorContrastModelByMetadata.entrySet().iterator().next();
			currentLayerParameters = currentLayerParameters
					.withMetadataUsedToGetColor(Optional.of(colorContrastEntry.getKey()))
					.withColorContrastModel(colorContrastEntry.getValue());
		}
	}

	/**
	 * Computes {@link #currentVectorModel}
	 */
	private void loadCurrentModel() {
		LOGGER.info("Nb of vector to render : {}", currentData.getFiltering().sampling());
		currentVectorModel = new VectorInfoSupplier() {

			@Override
			public int getVectorCount() {
				return currentData.getVectorCount();
			}

			@Override
			public Position getOrigin(int vectorIndex) throws GIOException {
				return Position.fromDegrees(currentData.getVectorLatitude(vectorIndex),
						currentData.getVectorLongitude(vectorIndex), currentData.getVectorElevation(vectorIndex));
			}

			@Override
			public double getValue(int vectorIndex) throws GIOException {
				return currentData.getVectorValue(vectorIndex);
			}

			@Override
			public double getHeading(int vectorIndex) throws GIOException {
				return currentData.getVectorHeading(vectorIndex);
			}
		};
		currentVectorModelUpdated = true;
	}

	/**
	 * Computes min/max for each metadata.
	 */
	private void computeMinMaxMetadata() {
		try {
			var minMaxMap = new HashMap<NavigationMetadata<?>, FloatRange>();
			for (int i = 0; i < currentData.getVectorCount(); i++) {
				// compute min/max for each metadata
				for (var metadataValue : currentData.getAllVectorMetadataValues(i)) {
					var minMaxRange = minMaxMap.get(metadataValue.metadata());
					float floatValue = metadataValue.getValueAsNumber().floatValue();
					if (Float.isNaN(floatValue))
						continue;
					var minValue = minMaxRange != null ? Math.min(minMaxRange.getMinimumFloat(), floatValue)
							: floatValue;
					var maxValue = minMaxRange != null ? Math.max(minMaxRange.getMaximumFloat(), floatValue)
							: floatValue;
					minMaxMap.put(metadataValue.metadata(), new FloatRange(minValue, maxValue));
				}
			}

			// compute color contrast model
			colorContrastModelByMetadata = new HashMap<>();
			var metadataList = currentData.getVectorMetadata();
			for (var metadata : metadataList) {
				FloatRange minMaxRange = minMaxMap.get(metadata);
				if (minMaxRange != null) {
					float minValue = metadata.getOptMinValue().orElse(minMaxRange.getMinimumNumber()).floatValue();
					float maxValue = metadata.getOptMaxValue().orElse(minMaxRange.getMaximumFloat()).floatValue();
					colorContrastModelByMetadata.put(metadata.getType(), new ColorContrastModelBuilder()//
							.setContrastMin(minValue).setLimitMin(minValue - 0.25f * (maxValue - minValue))//
							.setContrastMax(maxValue).setLimitMax(maxValue + 0.25f * (maxValue - minValue))//
							.build());
					// initialize first min/max values
					var optMetadataUsedToGetColor = currentLayerParameters.metadataUsedToGetColor();
					if (optMetadataUsedToGetColor.isPresent() && optMetadataUsedToGetColor.get() == metadata.getType())
						currentLayerParameters = currentLayerParameters.withColorContrastModel(
								currentLayerParameters.colorContrastModel().withContrastMinMax(minValue, maxValue));
				}
			}
		} catch (GIOException e) {
			LOGGER.warn("Error when computing min/max for each metadata", e);
		}
	}

	/**
	 * Prepare rendering
	 */
	@Override
	protected void doPreRender(DrawContext dc) {
		if (currentVectorModelUpdated //
				|| verticalExaggeration != dc.getVerticalExaggeration()
				|| currentScaleFactor != currentLayerParameters.scaleFactor()//
				|| currentVerticalOffset != currentLayerParameters.verticalOffset()) {
			verticalExaggeration = dc.getVerticalExaggeration();
			currentScaleFactor = currentLayerParameters.scaleFactor();
			currentVerticalOffset = currentLayerParameters.verticalOffset();
			vectorRenderer.acceptVectors(//
					currentVectorModel, false, verticalExaggeration, currentScaleFactor);
			currentVectorModelUpdated = false;
		}
	}

	/** Configure Point rendering */
	private void configureVectorRenderer() {
		Optional<ColorContrastModel> colorContrastModel = Optional.empty();
		IntToDoubleFunction colorValueSupplier = vectorRenderer.getParameters().colorValueSupplier();
		if (currentLayerParameters.isVariableColor() && currentLayerParameters.metadataUsedToGetColor().isPresent()) {
			colorContrastModel = Optional.of(currentLayerParameters.colorContrastModel());
			NavigationMetadataType<?> metadataType = currentLayerParameters.metadataUsedToGetColor().get();
			NavigationMetadata<Double> metadata = currentData.getVectorMetadata().stream()
					.filter(m -> m.getType().equals(metadataType)).findFirst().orElse(null);
			if (metadata != null) {
				colorValueSupplier = index -> {
					try {
						return metadata.getValue(index).doubleValue();
					} catch (Exception e) {
						return Double.NaN;
					}
				};
			}
		}

		vectorRenderer.setParameters(new VectorRendererParameters(//
				currentLayerParameters.width(), //
				currentLayerParameters.opacity(), //
				currentLayerParameters.absoluteColor(), //
				colorContrastModel, colorValueSupplier, //
				currentLayerParameters.verticalOffset()));
	}

	/**
	 * @return a GlobalCursor for the specified vector index. Invoked when a picking is processed in the VectorRenderer
	 */
	private GlobalCursor buildGlobalCursorForVector(int vectorIndex) {
		var result = new GlobalCursor().withSource(vectorRenderer).withVisible(false);
		try {
			if (vectorIndex < currentData.getVectorCount()) {
				result = result.withPosition(Position.fromDegrees(currentData.getVectorLatitude(vectorIndex),
						currentData.getVectorLongitude(vectorIndex), currentData.getVectorElevation(vectorIndex)))//
						.withTime(currentData.getVectorInstant(vectorIndex))//
						.withDescription(getName() + currentData.getVectorDescription(vectorIndex));
			}
		} catch (GIOException e) {
			LOGGER.warn("Error when describing a vector {}", e.getMessage());
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public WWCurrentLayerParameters getParameters() {
		return currentLayerParameters;
	}

	/** {@inheritDoc} */
	@Override
	public void setParameters(WWCurrentLayerParameters parameters) {
		fitToParameters(parameters);
		sessionService.saveSession();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TypedMap describeParametersForSession() {
		return currentLayerParameters.toTypedMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(WWCurrentLayerParameters.SESSION_KEY).ifPresent(restoredDisplayParameters -> {
			currentVectorModelUpdated = true;
			// Restore the instance of NavigationMetadataType.
			parameters.get(WWCurrentLayerParameters.SESSION_KEY_METADATA).ifPresentOrElse(restoredMetadataName -> {
				// Use the name stored in the session to find the instance of NavigationMetadataType among the
				// configured map colorContrastModelByMetadata
				Optional<NavigationMetadataType<?>> restoredMetadataUsedToGetColor = getNavigationMetadataType(
						restoredMetadataName);
				fitToParameters(restoredDisplayParameters.withMetadataUsedToGetColor(restoredMetadataUsedToGetColor));
			}, () -> fitToParameters(restoredDisplayParameters));
		});
	}

	/** Change parameters */
	private void fitToParameters(WWCurrentLayerParameters parameters) {
		currentLayerParameters = parameters;

		// Current data updated in AWT thread to avoid VBO access conflicts with openGL (JVM crash)
		SwingUtilities.invokeLater(() -> {
			configureVectorRenderer();
			layerStoreModel.get(this).ifPresent(layerStore -> {
				// create color scale layer for variable color
				if (currentLayerParameters.isVariableColor() && optColorScaleLayer.isEmpty()) {
					optColorScaleLayer = Optional
							.of(wwLayerFactory.createColorScaleLayer(this, currentData.getFileInfo().getBaseName()));
					layerStoreModel.addLayersToStore(layerStore, optColorScaleLayer.get());
				}
				// remove color scale layer if it is a constant color
				else if (!currentLayerParameters.isVariableColor() && optColorScaleLayer.isPresent()) {
					layerStoreModel.removeLayersToStore(layerStore, optColorScaleLayer.get());
					optColorScaleLayer = Optional.empty();
				}
			});
			geographicViewService.refresh();
		});
	}

	/** {@inheritDoc} */
	@Override
	public void filter(CurrentFiltering filtering) {
		if (currentData.getFileInfo() instanceof AdcpInfo adcpinfo) {
			Optional<ICurrentData> newCurrentData = adcpinfo.getCurrentData();
			if (newCurrentData.isPresent()) {
				processService.runInForeground("Current layer reloading with sampling : " + filtering.sampling(),
						() -> {
							if (adcpinfo.filterCurrentData(filtering)) {
								// Store filtering values in session
								FileInfoSettings settings = AdcpInfo.computeFileInfoSettings(filtering);
								sessionService.getPropertiesContainer().add(adcpinfo.getPath(),
										FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS, settings);
								sessionService.saveSession();
								currentData = newCurrentData.get();
								currentFiltering = filtering;
								loadCurrentModel();
								computeMinMaxMetadata();
								configureVectorRenderer();
								geographicViewService.refresh();
							}
						});
			}
		}
	}

	/**
	 * @return the icon to represent the layer
	 */
	@Override
	public Optional<Image> getIcon() {
		return Optional.ofNullable(ImageResources.getImage("icons/16/current.png", getClass()));
	}

	/**
	 * Search a NavigationMetadataType with the specified name
	 */
	protected Optional<NavigationMetadataType<?>> getNavigationMetadataType(String name) {
		return colorContrastModelByMetadata.keySet().stream().filter(type -> type.name.equalsIgnoreCase(name))
				.findFirst();
	}

	@Override
	public ColorContrastModel getColorContrastModel(NavigationMetadataType<?> metadataType) {
		return getNavigationMetadataType(metadataType.name).map(colorContrastModelByMetadata::get)
				.orElse(getParameters().colorContrastModel());
	}

	@Override
	public Map<NavigationMetadataType<?>, ColorContrastModel> getColorContrastModelByMetadataType() {
		return Collections.unmodifiableMap(colorContrastModelByMetadata);
	}

	@Override
	public Optional<IWWColorScaleLayer> getColorScaleLayer() {
		return optColorScaleLayer;
	}

	/**
	 * @return the {@link #currentData}
	 */
	@Override
	public ICurrentData getCurrentData() {
		return currentData;
	}

	@Override
	public CurrentFiltering getCurrentFiltering() {
		return currentFiltering;
	}

}
