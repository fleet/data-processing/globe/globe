package fr.ifremer.viewer3d.layers;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTiledImageLayer;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.event.PositionEvent;
import gov.nasa.worldwind.event.PositionListener;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;

/**
 * Extends abstract layer with SSV functionalities. Used for Shader functionalities
 *
 * @author MORVAN
 */
public abstract class MyAbstractLayer extends AbstractLayer
		implements PositionListener, SelectListener, IMovableLayer, ILayerParameters {

	private Logger logger = LoggerFactory.getLogger(MyAbstractLayer.class);

	public static final String PROPERTY_CHANGE_SUPPORT = "avlist.PropertyChangeSupport";

	public static final String PROPERTY_OFFSET = "offset";
	public static final String PROTERTY_MOVEMENT = "movement";

	/** Value used as opacity value when opacity doesn't make sense for a layer. */
	public static final double OPACITY_NOT_AVAILABLE = -1.0;

	static protected boolean highQualityDisplayOfTexture = false;

	public static String displayHightResolution = "Display terrains at hight resolution";

	MyAbstractLayerParameter parameters;

	@Override
	public LayerParameters getLayerParameters() {
		return parameters;
	}

	@Override
	public void setLayerParameters(LayerParameters arg) {
		if (arg instanceof MyAbstractLayerParameter) {
			parameters.load((MyAbstractLayerParameter) arg);
		}
	}

	protected boolean visible = true;

	protected boolean _isRGB = false;

	protected boolean underMouse;

	protected List<String> _availableColorMaps;

	protected boolean selected = false;

	protected double dateLineLongitudeOffset = 0.0;

	private static Position currentPosition;
	private static List<Layer> visibleTiledLayers = new ArrayList<>();
	private static List<Layer> visibleWaterColumnLayers = new ArrayList<>();

	protected ISectorMovementProcessing movement = null;

	public MyAbstractLayer() {
		if (Viewer3D.getWwd() != null) {
			Viewer3D.getWwd().addSelectListener(this);
			Viewer3D.getWwd().addPositionListener(this);
		}
		parameters = new MyAbstractLayerParameter();
	}

	public MyAbstractLayer(MyAbstractLayer parent) {
		parameters = new MyAbstractLayerParameter(parent.parameters);

		// colormap

		_isRGB = parent._isRGB;

		underMouse = parent.underMouse;
		_availableColorMaps = parent._availableColorMaps;

		selected = parent.selected;

		visible = parent.visible;

		movement = parent.movement;
	}

	public void listerColorMap(boolean useFullList) {

		_availableColorMaps = new ArrayList<>();

		/**
		 *
		 * */
		if (!useFullList) {
			_availableColorMaps.add("Jet");
			_availableColorMaps.add("Catherine");
			_availableColorMaps.add("Grey");
			setColorMap(0);
			setMinTextureValue(0);
			setMaxTextureValue(255);
			setMinContrast(0);
			setMaxContrast(255);
			setMinTransparency(0);
			setMaxTransparency(255);

		} else {
			_availableColorMaps.addAll(ColorMap.getColormapsNames());
			setColorMap(0);
			setMinTextureValue(0);
			setMaxTextureValue(255);
			setMinContrast(0);
			setMaxContrast(255);
			setMinTransparency(0);
			setMaxTransparency(255);
		}

	}

	public List<String> getAvailableColorMaps() {
		return _availableColorMaps;
	}

	// highlight

	public boolean isHighlighted() {
		return parameters.isHighlighted();
	}

	public void setHighlighted(boolean highlight) {
		parameters.setHighlighted(highlight);
	}

	// contrast

	public void setUseContrast(boolean useContrast) {
		parameters.setUseContrast(useContrast);
	}

	public boolean isUseContrast() {
		return parameters.isUseContrast();
	}

	public void setMinContrast(double minContrast) {
		parameters.setMinTextureContrast(minContrast);
	}

	public double getMinContrast() {
		return parameters.getMinTextureContrast();
	}

	public void setMaxContrast(double maxContrast) {
		parameters.setMaxTextureContrast(maxContrast);
	}

	public double getMaxContrast() {
		return parameters.getMaxTextureContrast();
	}

	// colormap

	public void setUseColorMap(boolean useColorMap) {
		parameters.setUseColorMap(useColorMap);
	}

	public boolean isUseColorMap() {
		return parameters.isUseColorMap();
	}

	public void setColorMap(float colorMap) {
		int colorMapIndex = (int) colorMap;
		if (colorMapIndex >= _availableColorMaps.size()) {
			colorMapIndex = 0;
			logger.error("ColorMap not found, using default ColorMap for index" + colorMap);
		}
		parameters.setColormap(_availableColorMaps.get(colorMapIndex));
	}

	public float getColorMap() {

		float index = _availableColorMaps != null ? _availableColorMaps.indexOf(parameters.getColormap()) : 0;
		if (index < 0) {
			index = 0;
		}

		return index;
	}

	public void setColorMapOri(float colorMap) {
		int colorMapIndex = (int) colorMap;
		if (colorMapIndex >= _availableColorMaps.size()) {
			colorMapIndex = 0;
			logger.error("ColorMap not found, using default ColorMap for index" + colorMap);
			logger.error("ColorMap not found, using default ColorMap for index" + colorMap);
		}
		parameters.setColormapori(_availableColorMaps.get(colorMapIndex));
	}

	public float getColorMapOri() {
		float index = _availableColorMaps.indexOf(parameters.getColormapori());
		if (index == -1) {
			index = 0;
		}
		return index;
	}

	// ombrage

	public void setUseOmbrage(boolean useOmbrage) {
		parameters.setUseOmbrage(useOmbrage);
	}

	public boolean isUseOmbrage() {
		return parameters.isUseOmbrage();
	}

	public void setAzimuth(double azimuth) {
		parameters.setAzimuth(azimuth);
	}

	public double getAzimuth() {
		return parameters.getAzimuth();
	}

	public void setZenith(double zenith) {
		parameters.setZenith(zenith);
	}

	public double getZenith() {
		return parameters.getZenith();
	}

	public void setOmbrageExaggeration(double exaggeration) {
		parameters.setOmbrageExaggeration(exaggeration);
	}

	public double getOmbrageExaggeration() {
		return parameters.getOmbrageExaggeration();
	}

	public void setMinTextureValue(double minTextureValue) {
		parameters.setMinTextureValue(minTextureValue);
	}

	public double getMinTextureValue() {
		return parameters.getMinTextureValue();
	}

	public void setMaxTextureValue(double maxTextureValue) {
		parameters.setMaxTextureValue(maxTextureValue);
	}

	public double getMaxTextureValue() {
		return parameters.getMaxTextureValue();
	}

	public void setIsRGB(boolean _isRGB) {
		this._isRGB = _isRGB;
	}

	public boolean isRGB() {
		return _isRGB;
	}

	public void setInverseColorMap(boolean _inverseColorMap) {
		parameters.setInverseColorMap(_inverseColorMap);
	}

	public boolean isInverseColorMap() {
		return parameters.isInverseColorMap();
	}

	public void setMinRecommandeValue(double _minRecommandeValue) {
		parameters.setMinRecommandeValue(_minRecommandeValue);
	}

	public double getMinRecommandeValue() {
		return parameters.getMinRecommandeValue();
	}

	public void setMaxRecommandeValue(double _maxRecomandeValue) {
		parameters.setMaxRecommandeValue(_maxRecomandeValue);
	}

	public double getMaxRecommandeValue() {
		return parameters.getMaxRecommandeValue();
	}

	public void setUseGradient(boolean useGradient) {
		parameters.setUseGradient(useGradient);

	}

	public boolean isUseGradient() {
		return parameters.isUseGradient();

	}

	public void setUseOmbrageLogarithmique(boolean useOmbrageLogarithmique) {
		parameters.setUseOmbrageLogarithmique(useOmbrageLogarithmique);
	}

	public boolean isUseOmbrageLogarithmique() {
		return parameters.isUseOmbrageLogarithmique();
	}

	/**
	 * @param dc the current draw context
	 *
	 * @throws IllegalArgumentException if <code>dc</code> is null, or <code>dc</code>'s <code>Globe</code> or
	 *             <code>View</code> is null
	 */
	@Override
	public void render(DrawContext dc) {
		if (!this.isEnabled()) {
			return; // Don't check for arg errors if we're disabled
		}

		if (null == dc) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (null == dc.getGlobe()) {
			String message = Logging.getMessage("layers.AbstractLayer.NoGlobeSpecifiedInDrawingContext");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (null == dc.getView()) {
			String message = Logging.getMessage("layers.AbstractLayer.NoViewSpecifiedInDrawingContext");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (!this.isLayerActive(dc)) {
			return;
		}

		// PATCH GLOBE - BEGIN - Code replaced
		// if (!this.isLayerInView(dc))
		// return;
		if (!this.isLayerInView(dc)) {
			setVisible(false);
			underMouse = false;
			// setValue(SSVAVKey.CURRENTLY_VISIBLE, false);
			return;
		} else {
			setVisible(true);
		}
		// PATCH GLOBE - END

		this.doRender(dc);
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public void setUnderMouse(boolean underMouse) {
		this.underMouse = underMouse;
	}

	public boolean isUnderMouse() {
		return underMouse;
	}

	@Override
	public void selected(SelectEvent event) {
		try {
			MyTiledImageLayer currentTiledLayer = null;
			WaterColumnLayer currentEchosLayer = null;
			LayerList list = Viewer3D.getModel().getLayers();
			DrawContext dc = Viewer3D.getWwd().getSceneController().getDrawContext();

			ListIterator<Layer> iterator = list.listIterator();
			while (iterator.hasNext()) {
				Object o = iterator.next();
				if (o instanceof MyTiledImageLayer) {
					MyTiledImageLayer layer = (MyTiledImageLayer) o;
					layer.setUnderMouse(false);
					if (layer.isEnabled() && layer.isLayerActive(dc) && layer.isLayerInView(dc)) {
						visibleTiledLayers.add(0, layer);
						currentTiledLayer = layer;
					}
				} else if (o instanceof WaterColumnLayer) {
					WaterColumnLayer layer = (WaterColumnLayer) o;
					layer.setUnderMouse(false);
					if (layer.isSelected()) {
						visibleWaterColumnLayers.add(0, layer);
						currentEchosLayer = layer;
					} else {
						visibleWaterColumnLayers.add(layer);
					}
				}
			}
			if (currentTiledLayer != null || currentEchosLayer != null) {
				if (event.getEventAction().equals(SelectEvent.ROLLOVER)) {
					if (currentTiledLayer != null) {
						currentTiledLayer.setUnderMouse(true);
					}
					if (currentEchosLayer != null) {
						currentEchosLayer.setUnderMouse(true);
					}
					Viewer3D.getWwd().redraw();
				}
			}
		} catch (Exception e) {
			logger.warn("Exception", e);
		}
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	/** Modify current position. */
	@Override
	public void moved(PositionEvent event) {
		if (event != null) {
			currentPosition = event.getPosition();
		}
	}

	public static void setCurrentPosition(Position position) {
		MyAbstractLayer.currentPosition = position;
	}

	public static Position getCurrentPosition() {
		return currentPosition;
	}

	public static void setVisibleTiledLayers(List<Layer> visibleTiledLayers) {
		MyAbstractLayer.visibleTiledLayers = visibleTiledLayers;
	}

	public static List<Layer> getVisibleTiledLayers() {
		return visibleTiledLayers;
	}

	public static void setVisibleWaterColumnLayers(List<Layer> visibleWaterColumnLayers) {
		MyAbstractLayer.visibleWaterColumnLayers = visibleWaterColumnLayers;
	}

	public static List<Layer> getVisibleWaterColumnLayers() {
		return visibleWaterColumnLayers;
	}

	@Override
	synchronized public Object getValue(String key) {
		return super.getValue(key);
	}

	@Override
	public synchronized Object setValue(String key, Object value) {
		// if (key == SSVAVKey.CURRENTLY_VISIBLE) {
		// if (value != getValue(SSVAVKey.CURRENTLY_VISIBLE) && value instanceof Boolean) {
		// new LayerVisibilityEvent(this, (Boolean) value);
		// }
		// }
		Object out = super.setValue(key, value);

		/*
		 * if (event != null) { EventBus.publish(event); }
		 */

		return out;
	}

	@Override
	public Sector move(Sector st) {
		// trick for date line
		// if(isOnDateLine()){
		// st = new MovableSector(st.getMinLatitude(), st.getMaxLatitude(),
		// st.getMaxLongitude().addDegrees(-180),
		// st.getMinLongitude().addDegrees(180));
		// }

		if (movement != null) {
			st = movement.move(st);
		}

		return st;
	}

	@Override
	public LatLon move(LatLon latLon) {
		if (movement != null) {
			return movement.move(latLon);
		} else {
			return latLon;
		}
	}

	@Override
	public Sector moveInv(Sector st) {

		if (movement != null) {
			return movement.moveInv(st);
		} else {
			return st;
		}
	}

	@Override
	public LatLon moveInv(LatLon latLon) {
		if (movement != null) {
			return movement.moveInv(latLon);
		} else {
			return latLon;
		}
	}

	@Override
	public ISectorMovementProcessing getMovement() {
		return movement;
	}

	@Override
	public void setMovement(ISectorMovementProcessing movement) {
		this.movement = movement;
	}

	public static boolean isHighQualityDisplayOfTexture() {
		return highQualityDisplayOfTexture;
	}

	public static void setHighQualityDisplayOfTexture(boolean highQualityDisplayOfTexture) {
		MyAbstractLayer.highQualityDisplayOfTexture = highQualityDisplayOfTexture;
	}

	public static void setHighQualityDisplayOfTexture() {
		highQualityDisplayOfTexture = !highQualityDisplayOfTexture;
	}

	public double getOffset() {
		return parameters.getOffset();
	}

	public void setOffset(double offset) {
		double oldOffset = parameters.getOffset();
		parameters.setOffset(offset);
		firePropertyChange(PROPERTY_OFFSET, oldOffset, offset);
	}

	/** Indicates that offset is computed dynamicaly or not */
	public void setOffsetAuto(boolean offsetAuto) {
		parameters.setOffsetAuto(offsetAuto);
	}

	/**
	 * Getter of offsetAuto
	 */
	public boolean isOffsetAuto() {
		return parameters.isOffsetAuto();
	}

	public double getScaleFactor() {
		return parameters.getScaleFactor();
	}

	public void setScaleFactor(double scaleFactor) {
		parameters.setScaleFactor(scaleFactor);
	}

	/***
	 * value min before transparency (kind of transparent contrast)
	 *
	 * @param minTransparency
	 */
	public void setMinTransparency(double minTransparency) {
		parameters.minTransparency = minTransparency;

	}

	/***
	 * value min before transparency (kind of transparent contrast)
	 *
	 * @return minTransparency
	 */
	public double getMinTransparency() {
		return parameters.minTransparency;
	}

	/***
	 * value max before transparency (kind of transparent contrast)
	 *
	 * @param maxTransparency
	 */
	public void setMaxTransparency(double maxTransparency) {
		parameters.maxTransparency = maxTransparency;

	}

	/***
	 * value max before transparency (kind of transparent contrast)
	 *
	 * @return maxTransparency
	 */
	public double getMaxTransparency() {
		return parameters.maxTransparency;
	}

	/***
	 * Getter/Setter
	 */
	public Integer getContrastLevel() {
		return parameters.getContrastLevel();
	}

	/***
	 * Getter/Setter
	 */
	public void setContrastLevel(int _contrastLevel) {
		parameters.setContrastLevel(_contrastLevel);
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		super.dispose();
		if (Viewer3D.getWwd() != null) {
			Viewer3D.getWwd().removeSelectListener(this);
			Viewer3D.getWwd().removePositionListener(this);
		}

		// Memory leak : PropertyChangeSupport has a reference of this instance
		removeKey(PROPERTY_CHANGE_SUPPORT);

		disposeListeners.forEach(Runnable::run);
	}

	/** All recorded listener */
	protected List<Runnable> disposeListeners = new ArrayList<>();

	/** {@inheritDoc} */
	public void addDisposeListener(Runnable listener) {
		disposeListeners.add(listener);
	}

	/** {@inheritDoc} */
	public void removeDisposeListener(Runnable listener) {
		disposeListeners.remove(listener);
	}

}
