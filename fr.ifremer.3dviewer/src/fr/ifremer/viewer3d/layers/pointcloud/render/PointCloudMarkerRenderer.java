/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.pointcloud.render;

import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.markers.Marker;
import gov.nasa.worldwind.render.markers.MarkerRenderer;

/**
 * Renderable of Marker to draw points cloud
 */
public class PointCloudMarkerRenderer extends MarkerRenderer implements Renderable {

	private boolean visible;
	private Iterable<Marker> markers;

	/**
	 * Constructor
	 */
	public PointCloudMarkerRenderer() {
		setKeepSeparated(false);
	}

	@Override
	public void render(DrawContext dc) {
		if (markers != null) {
			render(dc, markers);
		}
	}

	/**
	 * @return the {@link #markers}
	 */
	public Iterable<Marker> getMarkers() {
		return markers;
	}

	/**
	 * @param markers the {@link #markers} to set
	 */
	public void setMarkers(Iterable<Marker> markers) {
		this.markers = markers;
	}

	/**
	 * @return the {@link #visible}
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the {@link #visible} to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}
