package fr.ifremer.viewer3d.layers.pointcloud.parametersview;

import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.DisplayMode;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.PointCloudParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.PointSizeMode;
import fr.ifremer.globe.ui.widget.AbstractParametersCompositeWithTabs;
import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget;
import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget.Model;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.colorscale.parametersview.ColorScaleParametersComposite;
import fr.ifremer.viewer3d.layers.pointcloud.PointCloudLayer;
import fr.ifremer.viewer3d.layers.sync.pointcloud.WWPointCloudLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;
import jakarta.inject.Inject;

/**
 * Parameter composite for {@link PointCloudLayer}.
 */
public class PointCloudParametersComposite extends AbstractParametersCompositeWithTabs {

	/** The layer getParameters() synchronizer get from Context **/
	@Inject
	private WWPointCloudLayerSynchronizer layersSynchronizer;
	/** Service of Geographic View */
	@Inject
	private IGeographicViewService geographicViewService;
	/** Service of Geographic View */
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Edited {@link PointCloudLayer} */
	private final IWWPointCloudLayer pointCloudLayer;

	private LayerParametersSynchronizerComposite<IWWPointCloudLayer> grpSynchronization;

	private ColorContrastWidget colorComposite;

	private Spinner pointSizeSpinner;
	private Button markerRadioButton;
	private SliderAndSpinnersWidget variableSizeSlider;
	private Button resetMinMaxButton;
	private Button pointRadioButton;
	private Button variableSizeButton;
	private Composite sizeModeCmp;
	private Button pointSizeButton;

	/**
	 * Constructor
	 */
	public PointCloudParametersComposite(Composite parent, IWWPointCloudLayer layer) {
		super(parent);
		pointCloudLayer = layer;

		ContextInitializer.inject(this);

		createSynchronizationGroup();
		createDisplayTab();
		createColorTab();

		Optional<IWWColorScaleLayer> colorScaleLayer = searchColorScaleLayer();
		if (colorScaleLayer.isPresent())
			createColorScaleTab(colorScaleLayer.get());

		Optional<IWWLabelLayer> labelLayer = searchLabelLayer();
		if (labelLayer.isPresent())
			createLabelTab(labelLayer.get());

		initializeListeners();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void createSynchronizationGroup() {

		// Synchronizer composite
		grpSynchronization = new LayerParametersSynchronizerComposite<>(this, layersSynchronizer, pointCloudLayer);
		grpSynchronization.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		updateSynchronizedGroup();
		grpSynchronization.setSynchronizationChangedListener(this::onSynchronizationChanged);
	}

	/** Creates a tab to manage shape options of the points */
	private void createDisplayTab() {
		Composite displayTab = createTab("Display");
		displayTab.setLayout(new GridLayout(3, false));
		Label displayModeLabel = new Label(displayTab, SWT.NONE);
		displayModeLabel.setText("Mode : ");

		markerRadioButton = new Button(displayTab, SWT.RADIO);
		markerRadioButton.setText("Marker");
		markerRadioButton.setSelection(getParameters().displayMode() != DisplayMode.POINT);

		pointRadioButton = new Button(displayTab, SWT.RADIO);
		pointRadioButton.setText("Point");
		pointRadioButton.setSelection(getParameters().displayMode() == DisplayMode.POINT);

		Composite sizeCmp = new Composite(displayTab, SWT.NONE);
		sizeCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		GridLayout glSizeCmp = new GridLayout(1, false);
		glSizeCmp.marginWidth = 0;
		glSizeCmp.marginHeight = 0;
		sizeCmp.setLayout(glSizeCmp);

		sizeModeCmp = new Composite(sizeCmp, SWT.NONE);
		GridLayout glSizeModeCmp = new GridLayout(3, false);
		glSizeModeCmp.marginWidth = 0;
		glSizeModeCmp.horizontalSpacing = 0;
		sizeModeCmp.setLayout(glSizeModeCmp);

		pointSizeButton = new Button(sizeModeCmp, SWT.RADIO);
		GridData gdPointSizeButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		pointSizeButton.setLayoutData(gdPointSizeButton);
		pointSizeButton.setText("Constant size :");
		pointSizeButton.setSelection(getParameters().pointSizeMode() == PointSizeMode.CONSTANT);

		Label pointSizeLabel = new Label(sizeModeCmp, SWT.NONE);
		GridData gdPointSizeLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		pointSizeLabel.setLayoutData(gdPointSizeLabel);
		pointSizeLabel.setText("Point size : ");

		pointSizeSpinner = new Spinner(sizeModeCmp, SWT.BORDER);
		pointSizeSpinner.setMinimum(1);
		pointSizeSpinner.setMaximum(Integer.MAX_VALUE);
		pointSizeSpinner.setSelection(getParameters().constantSize());
		pointSizeSpinner.setEnabled(getParameters().pointSizeMode() == PointSizeMode.CONSTANT);

		variableSizeButton = new Button(sizeModeCmp, SWT.RADIO);
		GridData gdVariableSizeButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1);
		variableSizeButton.setLayoutData(gdVariableSizeButton);
		variableSizeButton.setText("Variable size :");
		variableSizeButton.setSelection(getParameters().pointSizeMode() == PointSizeMode.VARIABLE);

		Group variableGroup = new Group(sizeCmp, SWT.NONE);
		variableGroup.setLayout(new GridLayout(1, false));
		GridData gdVariableGroup = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdVariableGroup.horizontalIndent = 5;
		variableGroup.setLayoutData(gdVariableGroup);
		variableGroup.setText("Value range");

		variableSizeSlider = new SliderAndSpinnersWidget(variableGroup);
		GridLayout gridLayout = (GridLayout) variableSizeSlider.getLayout();
		gridLayout.marginHeight = 0;
		variableSizeSlider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		variableSizeSlider.setEnabled(getParameters().pointSizeMode() == PointSizeMode.VARIABLE);
		variableSizeSlider.update(getParameters().variablePointSizeModel().min(),
				getParameters().variablePointSizeModel().max(), getParameters().variablePointSizeModel().minLimit(),
				getParameters().variablePointSizeModel().maxLimit());

		resetMinMaxButton = new Button(variableGroup, SWT.NONE);
		GridData gdResetMinMaxButton = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
		gdResetMinMaxButton.horizontalIndent = 5;
		resetMinMaxButton.setLayoutData(gdResetMinMaxButton);
		resetMinMaxButton
				.setText(String.format("Reset to %.1f/%.1f", getParameters().variablePointSizeModel().minLimit(),
						getParameters().variablePointSizeModel().maxLimit()));
		resetMinMaxButton.setEnabled(getParameters().pointSizeMode() == PointSizeMode.VARIABLE);

		// Variable size possible ?
		if (getParameters().minContrast() < getParameters().maxContrast())
			gdPointSizeLabel.exclude = true;
		else {
			gdPointSizeButton.exclude = true;
			gdVariableSizeButton.exclude = true;
			gdVariableGroup.exclude = true;
		}
	}

	/** Creates a tab to manage color options of points or markers */
	private void createColorTab() {
		var colorTab = createTab("Color");
		colorTab.setLayout(new GridLayout(1, false));

		PointCloudFieldInfo fieldInfo = pointCloudLayer.getFieldInfo();

		Range minMaxSyncValues = null;
		try {
			minMaxSyncValues = layersSynchronizer.getMinMaxSyncValues(pointCloudLayer)
					.orElse(new FloatRange(getParameters().minContrast(), getParameters().maxContrast()));
		} catch (IllegalArgumentException e) {
			// May happened when min and may are NaN
			minMaxSyncValues = new FloatRange(0f, 0f);
		}

		ColorContrastModel colorContrastModel = new ColorContrastModelBuilder() //
				.setColorMapIndex(getParameters().colorMapIndex()) //
				.setInvertColor(getParameters().colorMapInverted()) //
				.setContrastMin(minMaxSyncValues.getMinimumFloat()) //
				.setContrastMax(minMaxSyncValues.getMaximumFloat())//
				.setOpacity(getParameters().opacity())//
				.setLimitMin((float) fieldInfo.minValue()) //
				.setLimitMax((float) fieldInfo.maxValue())//
				.setResetMin((float) fieldInfo.minValue()) //
				.setResetMax((float) fieldInfo.maxValue())//
				.build();

		colorComposite = new ColorContrastWidget(colorTab, colorContrastModel, Optional.empty());
		colorComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		colorComposite.subscribe(model -> {
			var newParameters = getParameters().withColor(model.invertColor(), model.colorMapIndex(),
					model.contrastMin(), model.contrastMax(), model.opacity());
			setParameters(newParameters);
		});

	}

	/** Creates a tab to manage display options of the ColorScale of the current layer */
	private void createColorScaleTab(IWWColorScaleLayer colorScaleLayer) {
		Composite colorScaleTab = createTab("Color scale");
		colorScaleTab.setLayout(new GridLayout(1, false));

		Button btnEnableColorScale = new Button(colorScaleTab, SWT.CHECK);
		btnEnableColorScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnEnableColorScale.setText("Display color scale");
		btnEnableColorScale.setSelection(colorScaleLayer.isEnabled());

		var colorScaleComposite = new ColorScaleParametersComposite(colorScaleTab, colorScaleLayer);
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		colorScaleComposite.setLayout(gridLayout);
		colorScaleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		btnEnableColorScale.addListener(SWT.Selection, e -> {
			colorScaleLayer.setEnabled(btnEnableColorScale.getSelection());
			colorScaleComposite.setEnabled(btnEnableColorScale.getSelection());
		});
	}

	/** Creates a tab to manage display options of the labels */
	private void createLabelTab(IWWLabelLayer labelLayer) {
		Composite labelTab = createTab("Label");
		labelTab.setLayout(new GridLayout(1, false));

		Button btnEnableLabel = new Button(labelTab, SWT.CHECK);
		btnEnableLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnEnableLabel.setText("Display labels");
		btnEnableLabel.setSelection(labelLayer.isEnabled());

		btnEnableLabel.addListener(SWT.Selection, e -> {
			labelLayer.setEnabled(btnEnableLabel.getSelection());
		});
	}

	/**
	 * Hooks listeners
	 */
	private void initializeListeners() {
		markerRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				swithDisplayMode();
			}
		});

		pointRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				swithDisplayMode();
			}
		});

		variableSizeSlider.getPublisher().subscribe(this::onWeightedPointSizeModelChanged);

		pointSizeSpinner.addModifyListener(e -> {
			var newParameters = getParameters().withConstantSize(pointSizeSpinner.getSelection());
			setParameters(newParameters);
		});

		pointSizeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				var newParameters = getParameters().withPointSizeMode(
						pointSizeButton.getSelection() ? PointSizeMode.CONSTANT : PointSizeMode.VARIABLE);
				setParameters(newParameters);
				pointSizeSpinner.setEnabled(pointSizeButton.getSelection());
			}
		});

		variableSizeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				var newParameters = getParameters().withPointSizeMode(
						variableSizeButton.getSelection() ? PointSizeMode.VARIABLE : PointSizeMode.CONSTANT);
				setParameters(newParameters);
				variableSizeSlider.setEnabled(variableSizeButton.getSelection());
				resetMinMaxButton.setEnabled(variableSizeButton.getSelection());
			}
		});

		resetMinMaxButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Model weightedModel = getParameters().variablePointSizeModel();
				variableSizeSlider.update(weightedModel.minLimit(), weightedModel.maxLimit(), weightedModel.minLimit(),
						weightedModel.maxLimit());
				var newParameters = getParameters().withVariablePointSizeModel(variableSizeSlider.getModel());
				setParameters(newParameters);
			}
		});
	}

	/** Browse the WWFileLayerStore and search the color IWWColorScaleLayer of the current layer */
	private Optional<IWWColorScaleLayer> searchColorScaleLayer() {
		Optional<WWFileLayerStore> layerStoreOpt = fileLayerStoreModel.get(pointCloudLayer);
		if (layerStoreOpt.isPresent()) {
			WWFileLayerStore layerStore = layerStoreOpt.get();
			return layerStore.getLayers(IWWColorScaleLayer.class)
					.filter(colorScaleLayer -> colorScaleLayer.getSource() == pointCloudLayer).findFirst();
		}
		return Optional.empty();
	}

	/** Browse the WWFileLayerStore and search the color IWWLabelLayer */
	private Optional<IWWLabelLayer> searchLabelLayer() {
		Optional<WWFileLayerStore> layerStoreOpt = fileLayerStoreModel.get(pointCloudLayer);
		if (layerStoreOpt.isPresent()) {
			WWFileLayerStore layerStore = layerStoreOpt.get();
			return layerStore.getLayers(IWWLabelLayer.class).findFirst();
		}
		return Optional.empty();
	}

	private void updateSynchronizedGroup() {
		grpSynchronization.setText(
				String.format("Synchronization with other '%s' layers", pointCloudLayer.getFieldInfo().name()));
		grpSynchronization.adaptToLayer(pointCloudLayer);
	}

	/** The synchronization option has changed. Update the minMaxSyncValues in the ColorComposite */
	private void onSynchronizationChanged(IWWPointCloudLayer layer) {
		if (colorComposite != null) {
			Optional<Range> minMaxSyncValues = layersSynchronizer.getMinMaxSyncValues(layer);
			colorComposite.setMinMaxSyncValues(minMaxSyncValues);
		}
	}

	/** Switch mode Marker <-> Point */
	private void swithDisplayMode() {
		var newParameters = getParameters()
				.withDisplayMode(markerRadioButton.getSelection() ? DisplayMode.MARKER : DisplayMode.POINT);
		setParameters(newParameters);
	}

	private void onWeightedPointSizeModelChanged(Model weightedPointSizeModel) {
		var newParameters = getParameters().withVariablePointSizeModel(weightedPointSizeModel);
		setParameters(newParameters);
	}

	private PointCloudParameters getParameters() {
		return pointCloudLayer.getParameters();
	}

	/** Switch mode Marker <-> Point */
	private void setParameters(PointCloudParameters newParameters) {
		pointCloudLayer.setParameters(newParameters);
		geographicViewService.refresh();
		layersSynchronizer.synchonizeWith(pointCloudLayer);
	}

}
