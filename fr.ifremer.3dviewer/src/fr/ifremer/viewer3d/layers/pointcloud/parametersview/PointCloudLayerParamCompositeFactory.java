/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.pointcloud.parametersview;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode.CheckState;
import fr.ifremer.viewer3d.layers.pointcloud.PointCloudLayer;

/**
 * Make a Composite to edit the {@link PointCloudLayer} parameters.
 */
@Component(name = "globe_viewer3d_layers_pointcloud_param_composite_factory", service = IParametersViewCompositeFactory.class)
public class PointCloudLayerParamCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		if (selection instanceof FileInfoNode pointCloudInfoNode
				&& pointCloudInfoNode.getFileInfo().getContentType() == ContentType.POINT_CLOUD_CSV) {
			for (TreeNode subNode : pointCloudInfoNode.getChildren()) {
				if (subNode instanceof LayerNode layerNode && layerNode.getState() == CheckState.TRUE)
					return Optional.of(new PointCloudInfoNodeComposite(parent, pointCloudInfoNode,
							layerNode.getLayer().map(IWWPointCloudLayer.class::cast)));
			}
			// No layer currently selected
			return Optional.of(new PointCloudInfoNodeComposite(parent, pointCloudInfoNode, Optional.empty()));
		}

		// Not concerned
		return Optional.empty();
	}
}
