package fr.ifremer.viewer3d.layers.pointcloud.parametersview;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.IPointCloudFieldInfoService;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics.PointCloudRequestInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.pointcloud.PointCloudLayer;
import jakarta.inject.Inject;

/**
 * Parameter composite for FileInfoNode of PointCloud.
 */
public class PointCloudInfoNodeComposite extends Composite {

	/** Service of events */
	@Inject
	private IEventBroker eventBroker;
	/** Service of PointCloudFieldInfo */
	@Inject
	private IPointCloudFieldInfoService pointCloudFieldInfoService;
	/** Global {@link WWFileLayerStoreModel} **/
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Selected node in project explorer */
	private final FileInfoNode pointCloudInfoNode;

	/** Inner parameter composite to display parameter of the selected layer **/
	private PointCloudParametersComposite pointCloudParametersComposite;
	private ComboViewer viewer;

	/**
	 * Constructor
	 */
	public PointCloudInfoNodeComposite(Composite parent, FileInfoNode pointCloudInfoNode,
			Optional<IWWPointCloudLayer> layer) {
		super(parent, SWT.NONE);
		this.pointCloudInfoNode = pointCloudInfoNode;
		ContextInitializer.inject(this);

		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);
		initializeCombo(pointCloudInfoNode.getFileInfo());
		layer.ifPresent(pointCloudLayer -> {
			viewer.setSelection(new StructuredSelection(pointCloudLayer.getFieldInfo()));
		});

		Consumer<WWFileLayerStoreEvent> listener = evt -> {
			if (!isDisposed() && evt.getState() == FileLayerStoreState.UPDATED)
				getDisplay().asyncExec(() -> onLayerUpdated(evt.getFileLayerStore(), evt.getUpdatedLayers()));
		};
		fileLayerStoreModel.addListener(listener);
		addDisposeListener(evt -> fileLayerStoreModel.removeListener(listener));
	}

	/**
	 * Constructor
	 */
	private void initializeCombo(IFileInfo fileInfo) {

		// Layer
		Group grpLayer = new Group(this, SWT.NONE);
		grpLayer.setText(String.format("Field of '%s'", fileInfo.getFilename()));
		GridDataFactory.fillDefaults().grab(true, false).applyTo(grpLayer);
		grpLayer.setLayout(new GridLayout(1, false));

		// combo to select layer
		viewer = new ComboViewer(grpLayer, SWT.READ_ONLY);
		viewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		viewer.setContentProvider(ArrayContentProvider.getInstance());

		// fill combo with DTM layers
		List<PointCloudFieldInfo> pointCloudFieldInfos = pointCloudFieldInfoService
				.supplyPointCloudFieldInfos(fileInfo);

		viewer.setLabelProvider(LabelProvider.createTextProvider(o -> ((PointCloudFieldInfo) o).name()));
		viewer.setInput(pointCloudFieldInfos);
		// handle selection of layer
		viewer.addSelectionChangedListener(this::onFieldInfoSelected);
	}

	/**
	 * Called when a {@link RasterInfo} (= layer) is selected.
	 */
	private void onFieldInfoSelected(SelectionChangedEvent event) {
		PointCloudFieldInfo selectedFieldInfo = (PointCloudFieldInfo) event.getStructuredSelection().getFirstElement();

		IWWPointCloudLayer selectedPointCloudLayer = null;
		for (TreeNode treeNode : pointCloudInfoNode.getChildren())
			if (treeNode instanceof LayerNode layerNode) {
				var optLayer = layerNode.getLayer().filter(IWWPointCloudLayer.class::isInstance)
						.map(IWWPointCloudLayer.class::cast);
				if (optLayer.isPresent()) {
					var layer = optLayer.get();
					if (layer.getFieldInfo().equals(selectedFieldInfo)) {
						// Layer found
						selectedPointCloudLayer = layer;
						layerNode.setChecked(true);
					} else
						// Not the selected layer : desactivates it
						layerNode.setChecked(false);
				}
			}

		if (selectedPointCloudLayer != null)
			// Update composite with the found layer
			updateSubComposites(selectedPointCloudLayer);
		else
			// Layer does not exist. Requests it
			eventBroker.post(UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED,
					new PointCloudRequestInfo(pointCloudInfoNode.getFileInfo(), selectedFieldInfo));
	}

	private void updateSubComposites(IWWPointCloudLayer layer) {
		// Dispose the sub-composite if any
		if (pointCloudParametersComposite != null)
			pointCloudParametersComposite.dispose();

		// Create a new sub-composite
		pointCloudParametersComposite = new PointCloudParametersComposite(this, layer);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(pointCloudParametersComposite);
		getParent().setSize(getParent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
		pointCloudParametersComposite.requestLayout();
		getParent().layout(true);
	}

	/** Reacts to a layer creation. Refresh the view if current fieldInfo is found among the created layers */
	private void onLayerUpdated(WWFileLayerStore fileLayerStore, List<IWWLayer> updatedLayers) {
		getSelectedFieldInfo().ifPresent(currentFielInfo -> {
			if (Objects.equals(fileLayerStore.getFileInfo(), pointCloudInfoNode.getFileInfo()))
				for (var layer : updatedLayers)
					if (layer instanceof PointCloudLayer pointCloudLayer)
						if (currentFielInfo.name().equals(pointCloudLayer.getFieldInfo().name()))
							updateSubComposites(pointCloudLayer);
		});
	}

	/** Current selected field in the combo */
	private Optional<PointCloudFieldInfo> getSelectedFieldInfo() {
		return Optional.ofNullable((PointCloudFieldInfo) viewer.getStructuredSelection().getFirstElement());
	}

}
