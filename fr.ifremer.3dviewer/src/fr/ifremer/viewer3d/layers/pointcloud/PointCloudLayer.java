package fr.ifremer.viewer3d.layers.pointcloud;

import java.util.List;
import java.util.Optional;
import java.util.function.DoubleUnaryOperator;

import fr.ifremer.globe.core.model.file.pointcloud.IPointCloudFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.DisplayMode;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.PointCloudParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.PointSizeMode;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;
import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget.Model;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.markers.render.WWMarker;
import fr.ifremer.viewer3d.layers.point.render.PointRenderer;
import fr.ifremer.viewer3d.layers.point.render.PointRendererParameters;
import fr.ifremer.viewer3d.layers.pointcloud.render.PointCloudMarkerRenderer;
import gov.nasa.worldwind.layers.MarkerLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.markers.Marker;
import jakarta.inject.Inject;

/**
 * Extends the default Worldwind {@link MarkerLayer} to display a point cloud (with markers).
 */
public class PointCloudLayer extends RenderableLayer implements IWWPointCloudLayer {

	/** Session services */
	@Inject
	private ISessionService sessionService;

	/** Renderer of points using WW markers */
	private final PointCloudMarkerRenderer markerRenderer = new PointCloudMarkerRenderer();
	/** Renderable of points using OpenGL **/
	@Inject
	private PointRenderer pointRenderer;

	/** Model of points to render */
	private List<PointMarkerModel> pointModel;
	/** True when pointModel have been modified and renderers must be updated */
	private boolean pointModelUpdated = false;
	/** Last applied vertical exaggeration */
	private double verticalExaggeration = 1d;

	/** {@link IPointCloudFileInfo} **/
	private final IPointCloudFileInfo pointCloudFileInfo;

	/** Current represented field **/
	private final PointCloudFieldInfo fieldInfo;

	/** The parameters used to render the layer */
	private PointCloudParameters parameters;

	/**
	 * Constructor
	 */
	public PointCloudLayer(IPointCloudFileInfo pointCloudFileInfo, PointCloudFieldInfo fieldInfo) {
		ContextInitializer.inject(this);
		this.pointCloudFileInfo = pointCloudFileInfo;
		this.fieldInfo = fieldInfo;
		parameters = new PointCloudParameters(DisplayMode.MARKER, false, ColorMaps.GMT_JET.getIndex(),
				(float) fieldInfo.minValue(), (float) fieldInfo.maxValue(), //
				PointSizeMode.CONSTANT, 10, //
				new Model(1f, 100f, 1f, 100f), // Variable point size model
				1f);

		addRenderable(markerRenderer);
		addRenderable(pointRenderer);
		pointRenderer.setGlobalCursorSupplier(this::buildGlobalCursorForPoint);

		refresh();
	}

	/**
	 * Displays points as Marker.
	 */
	public void refresh() {
		int columnIndex = fieldInfo.columnIndex();
		DoubleUnaryOperator pointSizeSupplier = gainPointSizeSupplier(); // How to compute point size
		this.pointModel = pointCloudFileInfo.getPoints().stream()//
				.map(point -> new PointMarkerModel(point, columnIndex,
						(float) pointSizeSupplier.applyAsDouble(point.getValue(columnIndex)), getOpacity(),
						this::computeColor))//
				.toList();

		configureRendrable();
		pointModelUpdated = true;
		Viewer3D.refreshRequired();
	}

	/** Returns the algorithm to compute a point size */
	private DoubleUnaryOperator gainPointSizeSupplier() {
		// Simple case, all points have the same size
		if (parameters.pointSizeMode() == PointSizeMode.CONSTANT)
			return v -> Math.max(parameters.constantSize(), 1);

		// Variable point size
		Model weightedModel = parameters.variablePointSizeModel();
		float minSize = weightedModel.min();
		float maxSize = weightedModel.max();

		// Specific case, min == max : same than constant size
		if (maxSize <= minSize)
			return v -> Math.max(minSize, 1d);

		// pointSize is defined in function of its value, between minSize and maxSize.
		return value -> {
			double sizeFactor = (value - fieldInfo.minValue()) / (fieldInfo.maxValue() - fieldInfo.minValue());
			sizeFactor = Math.max(0d, Math.min(1d, sizeFactor));
			return minSize + sizeFactor * (maxSize - minSize);
		};
	}

	/** Configure Marker or Point rendering, depending on DisplayMode */
	private void configureRendrable() {
		if (parameters.displayMode() == DisplayMode.POINT) {

			Model weightedPointSizeModel = parameters.pointSizeMode() == PointSizeMode.VARIABLE
					? parameters.variablePointSizeModel()
					: null;

			pointRenderer.setParameters(new PointRendererParameters(//
					parameters.constantSize(), //
					parameters.opacity(), //
					GColor.TRANSPARENT, //
					Optional.of(new ColorContrastModel(parameters.colorMapIndex(), parameters.colorMapInverted(),
							parameters.minContrast(), parameters.maxContrast(), parameters.minContrast(),
							parameters.maxContrast(), parameters.minContrast(),
							parameters.maxContrast(), false, parameters.opacity())), //
					Optional.ofNullable(weightedPointSizeModel)));
		}
	}

	/** {@inheritDoc} */
	@Override
	protected void doPreRender(DrawContext dc) {
		if (parameters.displayMode() != DisplayMode.POINT) {
			markerRenderer.setVisible(true);
			if (pointModelUpdated) {
				markerRenderer.setMarkers(pointModel.stream().map(WWMarker::new).map(Marker.class::cast).toList());
				pointModelUpdated = false;
			}
			pointRenderer.setVisible(false);
			pointRenderer.clear();
		} else {
			markerRenderer.setVisible(false);
			markerRenderer.setMarkers(List.of());
			pointRenderer.setVisible(true);
			if (verticalExaggeration != dc.getVerticalExaggeration() || pointModelUpdated) {
				verticalExaggeration = dc.getVerticalExaggeration();
				pointRenderer.acceptPoints(//
						pointModel.size(), //
						i -> pointModel.get(i).getPosition(), //
						i -> (float) pointModel.get(i).getValue(), //
						false, //
						verticalExaggeration);
				pointModelUpdated = false;
			}
		}
	}

	/**
	 * Computes point color with contrast parameters.
	 */
	private GColor computeColor(double value) {
		return ColorMap.getColor(parameters.colorMapIndex(), parameters.colorMapInverted(), parameters.minContrast(),
				parameters.maxContrast(), value);
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(getParameters().toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap sessionParameters) {
		sessionParameters.get(PointCloudParameters.SESSION_KEY).ifPresent(this::fitParameters);
	}

	// GETTERS & SETTERS

	public IPointCloudFileInfo getPointCloudFileInfo() {
		return pointCloudFileInfo;
	}

	/**
	 * @return the {@link #fieldInfo}
	 */
	@Override
	public PointCloudFieldInfo getFieldInfo() {
		return fieldInfo;
	}

	@Override
	public PointCloudParameters getParameters() {
		return parameters;
	}

	@Override
	public void setParameters(PointCloudParameters parameters) {
		fitParameters(parameters);
		sessionService.saveSession();
	}

	/** Accepts new parameters */
	private void fitParameters(PointCloudParameters parameters) {
		this.parameters = parameters;

		if (parameters.pointSizeMode() == null)
			this.parameters = parameters.withPointSizeMode(PointSizeMode.CONSTANT);
		if (parameters.variablePointSizeModel() == null)
			this.parameters = parameters.withVariablePointSizeModel(new Model((float) fieldInfo.minValue(),
					(float) fieldInfo.maxValue(), (float) fieldInfo.minValue(), (float) fieldInfo.maxValue()));

		setOpacity(this.parameters.opacity());

		refresh();
	}

	/**
	 * @return a GlobalCursor for the specified point index. Invoked when a picking is processed in the PointRenderer
	 */
	private GlobalCursor buildGlobalCursorForPoint(int pointIndex) {
		var result = new GlobalCursor().withSource(pointRenderer).withVisible(false);
		if (pointIndex < pointModel.size()) {
			PointMarkerModel pointMarker = pointModel.get(pointIndex);
			result = result.withPosition(pointMarker.getPosition());
			String tooltip = pointMarker.getTooltip().orElse(null);
			if (tooltip != null)
				result = result.withDescription(tooltip);
		}
		return result;
	}

	@Override
	public ColorContrastModel get() {
		return new ColorContrastModel(parameters.colorMapIndex(), parameters.colorMapInverted(),
				parameters.minContrast(), parameters.maxContrast(), parameters.minContrast(), parameters.maxContrast(),
				parameters.minContrast(), parameters.maxContrast(),false, parameters.opacity());
	}
}
