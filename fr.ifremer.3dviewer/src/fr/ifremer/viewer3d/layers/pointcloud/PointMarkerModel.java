package fr.ifremer.viewer3d.layers.pointcloud;

import java.util.Optional;
import java.util.function.DoubleFunction;

import fr.ifremer.globe.core.model.file.pointcloud.IPoint;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.viewer3d.layers.markers.render.IWWMarkerModel;
import gov.nasa.worldwind.geom.Position;

/**
 * Implementation of {@link IWWMarkerModel} for {@link IPoint}.
 */
public class PointMarkerModel implements IWWMarkerModel<IPoint> {

	private final IPoint model;
	private final int valueIndex;
	private final DoubleFunction<GColor> colorProvider;
	private final float size;
	private final double opacity;

	/**
	 * Constructor
	 */
	public PointMarkerModel(IPoint point, int valueIndex, float size, double opacity,
			DoubleFunction<GColor> colorProvider) {
		this.model = point;
		this.valueIndex = valueIndex;
		this.colorProvider = colorProvider;
		this.size = size;
		this.opacity = opacity;
	}

	@Override
	public IPoint getSource() {
		return model;
	}

	@Override
	public Position getPosition() {
		return Position.fromDegrees(model.getLatitude(), model.getLongitude(), model.getElevation());
	}

	@Override
	public float getSize() {
		return size;
	}

	@Override
	public GColor getColor() {
		double value = valueIndex >= 0 ? model.getValue(valueIndex) : 0d;
		return colorProvider.apply(value);
	}

	@Override
	public Optional<String> getTooltip() {
		var tooltip = new StringBuilder();
		if (valueIndex >= 0)
			tooltip.append("Value : ").append(model.getValue(valueIndex));
		model.getLabel().ifPresent(label -> {
			if (!tooltip.isEmpty())
				tooltip.append("\n");
			tooltip.append("Label : ").append(label);
		});
		model.getTime().ifPresent(time -> {
			if (!tooltip.isEmpty())
				tooltip.append("\n");
			tooltip.append("Time : ").append(time.toString());
		});
		return !tooltip.isEmpty() ? Optional.of(tooltip.toString()) : Optional.empty();
	}

	@Override
	public double getOpacity() {
		return opacity;
	}

	/**
	 * @return the value in model
	 */
	public double getValue() {
		return model.getValue(valueIndex);
	}

	@Override
	public boolean isResizeWithZoomEnabled() {
		return true;
	}

}
