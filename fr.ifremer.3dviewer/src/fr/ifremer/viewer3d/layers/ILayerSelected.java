package fr.ifremer.viewer3d.layers;

import java.awt.Color;

/**
 * Interface used by "onSelection" in Viewer3D.
 * <p>
 * Classes implementing this interface provides a capacity to change color when
 * selected.
 * </p>
 * 
 */
public interface ILayerSelected {

	public static Color selectedColor = Color.GREEN;

	/**
	 * select the associated layer, its color will be updated
	 * */
	public void setSelectedLayer(boolean selected);
}
