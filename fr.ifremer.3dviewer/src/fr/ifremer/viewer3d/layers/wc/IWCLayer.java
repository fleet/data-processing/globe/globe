package fr.ifremer.viewer3d.layers.wc;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;

/**
 * Base interface implemented by WC provider It allows for picking to retrieve metadata associated with the current
 * state of the layer/player
 */
public interface IWCLayer extends IWWLayer {

	/** @return the vertical offset applied to this layer */
	float getVerticalOffset();

}
