package fr.ifremer.viewer3d.layers.wc;

/**
 * This class will display WC volumic layer
 * */

import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.wc.DataProducer;
import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.LatLonSample;
import fr.ifremer.globe.core.model.wc.PingLoader;
import fr.ifremer.globe.core.model.wc.SlidingBuffer;
import fr.ifremer.globe.core.model.wc.SlidingBufferSetting;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.exception.LayerNotFoundException;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWaterColumnVolumicLayer;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerData;
import fr.ifremer.globe.utils.Result;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.wc.render.AbstractSinglePingSamples3D;
import fr.ifremer.viewer3d.layers.wc.render.SinglePingSamples;
import fr.ifremer.viewer3d.layers.wc.render.SinglePingSamples3D;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRenderer;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;

public class WCVolumicLayer extends AbstractLayer implements ITarget, IWCLayer, IWWWaterColumnVolumicLayer {

	static final Logger logger = LoggerFactory.getLogger(WCVolumicLayer.class);

	/** Session properties */
	public static final String IN_USE_IN_VIEWER2D = "inUseInViewer2d";
	public static final String ACTIVATED = "Activated";
	public static final String BEAM_GROUP_INDEX = "Beam_Group_Index";
	public static final String SKIP_MISSING_SWATH = "Skip_Missing_Swath";

	public static final TypedKey<Boolean> ACTIVATED_SESSION_KEY = new TypedKey<>(ACTIVATED);
	public static final TypedKey<Integer> BEAM_GROUP_INDEX_SESSION_KEY = new TypedKey<>(BEAM_GROUP_INDEX);
	public static final TypedKey<Boolean> SKIP_MISSING_SWATH_SESSION_KEY = new TypedKey<>(SKIP_MISSING_SWATH);

	@Inject
	protected IDataContainerFactory sounderDataFactory;
	@Inject
	protected ISessionService sessionService;
	@Inject
	protected IGeographicViewService geographicViewService;
	/** Listener of GlobalCursor hooked to IGlobalCursorService */
	private final Consumer<GlobalCursor> globalCursorListener = this::onGlobalCursor;

	/** {@link IDataContainerOwner} to book the {@link DataContainer} **/
	private final IDataContainerOwner dataContainerOwner = IDataContainerOwner.generate("Water column volumic layer",
			true);
	private ISounderDataContainerToken sounderDataContainerToken;

	protected String name;
	protected final ISounderNcInfo info;

	protected IndexedPlayer player;
	private PingLoader loader;
	private SlidingBuffer<AbstractSinglePingSamples3D> buffer;

	/** True when Viewer 2D benefits from the activation of this layer */
	private boolean inUseInViewer2d;
	private boolean active;
	private SounderDataContainer container;
	private VolumicRenderer renderer;

	protected VolumicRendererDisplayParameters displayParameters;
	private double verticalExaggeration = 1d;
	protected FilterParameters filterParameters;
	private int currentBeamGroupId = 1;
	private boolean skipMissingSwath = true;
	private Optional<Sector> sector;

	/**
	 * Constructor
	 */
	public WCVolumicLayer(ISounderNcInfo info) {
		this.info = info;
		name = guessName();

		ContextInitializer.inject(this);

		// create with default values
		displayParameters = VolumicRendererDisplayParameters.getDefault();
		filterParameters = FilterParameters.getDefault(info);

		// create a player for wc data
		player = new IndexedPlayer(info.getFilename(), 0, info.getCycleCount() - 1);
		player.subscribe(index -> loadIndex(false));
		sector = Optional.ofNullable(GeoBoxUtils.convert(this.info.getGeoBox()));
	}

	@PostConstruct
	private void postConstruct(IGlobalCursorService globalCursorService) {
		globalCursorService.addListener(globalCursorListener);
	}

	@PreDestroy
	private void preDestroy(IGlobalCursorService globalCursorService) {
		globalCursorService.removeListener(globalCursorListener);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.of(newName -> {
			var result = new WCVolumicLayer(info);
			result.name = newName;
			result.setName(newName);
			result.displayParameters = displayParameters;
			result.filterParameters = filterParameters;
			result.player.setCurrent(player.getCurrent());
			return result;
		});
	}

	/***
	 * Activate this layer, book the files and so on
	 */
	public Result activate(boolean activate) {
		active = activate;

		// Save in session
		sessionService.saveSession();

		if (active) {
			return createsRenderer();
		} else {
			loader = null;
			if (renderer != null) {
				renderer.dispose();
				renderer = null;
			}
			if (buffer != null) {
				buffer.dispose();
				buffer = null;
			}
			container = null;
			sounderDataContainerToken.close();
			dispose(); // useful to dispose SonarNativeProxy
			firePropertyChange(ACTIVATED, null, this);
			return Result.success();
		}
	}

	/**
	 * Because of the activation, creates the VolumicRenderer and initialize the player
	 */
	protected Result createsRenderer() {
		try {
			sounderDataContainerToken = sounderDataFactory.book(info, dataContainerOwner);
			container = sounderDataContainerToken.getDataContainer();
			SonarBeamGroup sonarBeamGroup = container.getBeamGroup(currentBeamGroupId);
			// convert data to db in case of Reson
			boolean convert = info.getConstructor().toLowerCase().contains("Reson".toLowerCase());
			loader = new PingLoader(container, sonarBeamGroup, filterParameters, convert);

			// we do not use lambda here because of a compilation issue du to the templating
			DataProducer<AbstractSinglePingSamples3D> producer = this::produceSinglePingSamples;

			buffer = SlidingBuffer.buildSingle(producer, info.getCycleCount() - 1);
			buffer.clear();

			renderer = new VolumicRenderer(this);
			loadIndex(false);
			firePropertyChange(ACTIVATED, null, this);
			return Result.success();
		} catch (LayerNotFoundException layerExcept) {
			return Result.failed("Water column of " + getInfo().getFilename() + " cannot be loaded, missing data");
		} catch (GIOException ioExcept) {
			logger.error("Error with file loading : " + ioExcept.getMessage(), ioExcept);
			return Result
					.failed("File " + getInfo().getFilename() + " cannot be loaded, it is locked by another editor");
		}
	}

	protected AbstractSinglePingSamples3D produceSinglePingSamples(int index) throws GIOException {
		return new SinglePingSamples3D(loader, index, this::getVerticalOffset, this::getVerticalExaggeration);
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		if (active && container != null) {
			active = false;
			container = null;
			sounderDataContainerToken.close();
		}
		if (buffer != null) {
			buffer.dispose();
			buffer = null;
		}
		if (renderer != null) {
			renderer.dispose();
			renderer = null;
		}

		super.dispose();
	}

	public void load(IndexedPlayerData playerData, VolumicRendererDisplayParameters display, FilterParameters filters,
			SlidingBufferSetting bufferSettings) {
		player.setPlaying(false);

		loader.setFilterParameters(filters);
		buffer.setBufferSettings(bufferSettings);
		player.setMaxPlayIndex(playerData.maxPlayIndex());
		player.setMinPlayIndex(playerData.minPlayIndex());
		displayParameters = display;
		if (player.getCurrent() != playerData.currentValue()) {
			player.setCurrent(playerData.currentValue());
		} else {
			// force reload of buffer if settings changed but not ping number
			buffer.clear();
			buffer.setCurrent(player.getCurrent());
		}
	}

	float minAmplitude = Float.POSITIVE_INFINITY;
	float maxAmplitude = Float.NEGATIVE_INFINITY;

	/**
	 * load data for the given index
	 */
	private void loadIndex(boolean force) {
		if (active && buffer != null) {
			if(force || !player.hasCurrent())
				buffer.clear();
			
			if (player.hasCurrent())
				buffer.setCurrent(player.getCurrent());
			
			if (skipMissingSwath && player.getLastActualIncrement() != 0
					&& buffer.values().stream().allMatch(AbstractSinglePingSamples3D::isEmpty)) {
				player.skipIndex();
				return;
			}
			geographicViewService.refresh();
		}

	}

	public IndexedPlayer getPlayer() {
		return player;
	}

	@Override
	protected void doRender(DrawContext dc) {
		if (active) {
			// check visibility
			if (sector.isPresent() && !sector.get().intersects(dc.getVisibleSector())) {
				return;
			}

			// retrieve vertical exaggeration
			if (verticalExaggeration != dc.getVerticalExaggeration()) {
				verticalExaggeration = dc.getVerticalExaggeration();
				if (renderer != null && player.hasCurrent()) {
					// Reload the ISinglePingSamples
					loadIndex(true);
					return;
				}
			}
			// check to prevent a lot of logging error
			if (renderer == null) {
				return;
			}
			renderer.render(dc);
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see gov.nasa.worldwind.layers.AbstractLayer#doPick(gov.nasa.worldwind.render.DrawContext, java.awt.Point)
	 */
	@Override
	protected void doPick(DrawContext dc, Point point) {
		if (active) {
			if (dc != null) {
				// check visibility
				if (sector.isPresent() && !sector.get().intersects(dc.getVisibleSector())) {
					return;
				}
				dc.addOrderedRenderable(renderer);
			}
		}
	}

	@Override
	public boolean isPickEnabled() {
		return super.isPickEnabled() && geographicViewService.isDetailPickEnabled();
	}

	@Override
	public String getName() {
		return name;
	}

	public void updateRendererParameters(VolumicRendererDisplayParameters parameters) {
		if (displayParameters != null && displayParameters.reloadNeeded(parameters)) {
			// remove old data
			buffer.clear();
		}
		displayParameters = parameters;
		sessionService.saveSession();

		setOpacity(parameters.getOpacity());
		firePropertyChange(AVKey.LAYER, null, this);

		player.notifyIndex(); // update 2D view
		geographicViewService.refresh();
	}

	@Override
	public FilterParameters getFilterParameters() {
		return loader != null ? loader.getFilterParameters() : filterParameters;
	}

	public void setFilterParameters(FilterParameters parameters) {
		if (parameters.equals(getFilterParameters())) {
			return;
		}

		// Save in session
		filterParameters = parameters;
		sessionService.saveSession();

		if (loader != null)
			loader.setFilterParameters(parameters);
		if (buffer != null) {
			// remove old data
			buffer.clear();
			// reload buffer
			player.notifyIndex();
		}
	}

	public int getBufferForwardLength() {
		if (buffer != null) {
			return buffer.getForwardLenght();
		}
		return 0;
	}

	public void setBufferForwardLength(int value) {
		if (buffer != null) {
			buffer.stopProducing();
			buffer.setForwardLenght(value);
			player.setCurrent(player.getCurrent());
			player.notifyIndex();
		}
	}

	public int getBufferBackwardLength() {
		if (buffer != null) {
			return buffer.getBackLength();
		}
		return 0;
	}

	public void setBufferBackwardLength(int value) {
		if (buffer != null) {
			buffer.stopProducing();
			buffer.setBackLength(value);
			player.setCurrent(player.getCurrent());
			player.notifyIndex();
		}
	}

	public boolean getSkipMissingSwath() {
		return skipMissingSwath;
	}

	public void setSkipMissingSwath(boolean skip) {
		skipMissingSwath = skip;
	}

	public ISounderNcInfo getInfo() {
		return info;
	}

	/**
	 * Export water column as Lat long depth value csv
	 *
	 * @throws GIOException
	 */
	public void exportAsCSV(String fileName) throws GIOException {
		player.setPlaying(false);
		// Add the ";" separator to the CSVWriter and delete the quotes
		try (ICSVWriter writer = new CSVWriterBuilder(new FileWriter(fileName)) //
				.withSeparator(';').withQuoteChar(ICSVWriter.NO_QUOTE_CHARACTER).build()) {

			String[] v = { "Longitude", "Latitude", "Elevation", "Value" };
			writer.writeNext(v);

			for (AbstractSinglePingSamples3D ping : buffer.values()) {
				for (var iterator = ping.getSampleIterator(); iterator.hasNext();) {
					LatLonSample sample = iterator.next();
					double lat = sample.latitude;
					double lon = sample.longitude;
					double value = sample.echo.value;
					double elevation = sample.elevation;
					String[] v2 = { Double.toString(lon), Double.toString(lat), Double.toString(elevation),
							Double.toString(value) };
					writer.writeNext(v2);
				}
			}
		} catch (IOException e) {
			throw new GIOException("Error while writing file" + fileName, e);
		}
	}

	/** Redefine to avoid a Stack overflow on ACTIVATED_PROPERTY_NAME notifications */
	@Override
	public void propertyChange(java.beans.PropertyChangeEvent propertyChangeEvent) {
		if (!ACTIVATED.equals(propertyChangeEvent.getPropertyName())) {
			super.propertyChange(propertyChangeEvent);
		}
	}

	public PingLoader getPingLoader() {
		return loader;
	}

	public void computeMinMaxAmplitude() {
		minAmplitude = Float.POSITIVE_INFINITY;
		maxAmplitude = Float.NEGATIVE_INFINITY;

		for (SinglePingSamples ping : buffer.values()) {
			minAmplitude = Math.min(minAmplitude, ping.getMinAmplitude());
			maxAmplitude = Math.max(maxAmplitude, ping.getMaxAmplitude());
		}
	}

	public float getMinAmplitude() {
		return minAmplitude;
	}

	public float getMaxAmplitude() {
		return maxAmplitude;
	}

	public float estimateNormalizationOffset() throws GIOException {
		return 0.f;
	}
	
	/**
	 * @return the {@link #displayParameters}
	 */
	public VolumicRendererDisplayParameters getDisplayParameters() {
		return displayParameters;
	}

	/**
	 * @return the {@link #buffer}
	 */
	public SlidingBuffer<AbstractSinglePingSamples3D> getBuffer() {
		return buffer;
	}

	public boolean isLayerActive() {
		return active;
	}

	/**
	 * Number of Netcdf group /Sonar/Beam_group.
	 */
	public int getBeamGroupCount() {
		return info.getBeamGroupCount();
	}

	/**
	 * Switching to an other Beam group
	 */
	public void switchToBeamGroupId(int beamGroupId) {
		if (container != null) {
			try {
				SonarBeamGroup sonarBeamGroup = container.getBeamGroup(beamGroupId);
				// Need an other Pinloader configured on layers of the desired beam group
				loader = new PingLoader(container, sonarBeamGroup, filterParameters, loader.isConvertToDb());
				// remove old data
				buffer.clear();
				// reload buffer
				currentBeamGroupId = beamGroupId;
				player.notifyIndex();
				// Save in session
				sessionService.saveSession();
			} catch (GIOException e) {
				Messages.openErrorMessage("Error", "Unable to swith to beam group " + beamGroupId, e);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(//
				ACTIVATED_SESSION_KEY.bindValue(active), //
				BEAM_GROUP_INDEX_SESSION_KEY.bindValue(currentBeamGroupId), //
				SKIP_MISSING_SWATH_SESSION_KEY.bindValue(skipMissingSwath), //
				player.getData().toTypedEntry(), //
				displayParameters.toTypedEntry(), //
				filterParameters.toTypedEntry()//
		);
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		active = parameters.get(ACTIVATED_SESSION_KEY).orElse(Boolean.FALSE);
		currentBeamGroupId = parameters.get(BEAM_GROUP_INDEX_SESSION_KEY).orElse(currentBeamGroupId);
		skipMissingSwath = parameters.get(SKIP_MISSING_SWATH_SESSION_KEY).orElse(skipMissingSwath);
		parameters.get(IndexedPlayerData.SESSION_KEY).ifPresent(player::setData);
		displayParameters = parameters.get(VolumicRendererDisplayParameters.SESSION_KEY).orElse(displayParameters);
		filterParameters = parameters.get(FilterParameters.SESSION_KEY).orElse(filterParameters);

		// No activation in session. Must activate by preference ?
		active = active || Activator.getPluginParameters().getWcActivated().getValue();

		if (active) {
			createsRenderer();
		}
	}

	/**
	 * Reacts to the reception of a GlobalCursor.<br>
	 * If a time is present, search the closest navigation point and set the index to the player
	 */
	private void onGlobalCursor(GlobalCursor globalcursor) {
		if (container == null)
			return; // No WC

		Pair<Instant, Instant> startEndDate = info.getStartEndDate().orElse(null);
		if (startEndDate != null && globalcursor.syncWcPayers()) {
			globalcursor.time().ifPresent(time -> {
				// modelStartingDate <= time <= modelEndDate ?
				if (DateUtils.between(time, startEndDate.getFirst(), startEndDate.getSecond())) {
					try {
						SonarBeamGroup sonarBeamGroup = container.getBeamGroup(currentBeamGroupId);
						LongLoadableLayer1D timeLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PING_TIME);
						int pingCount = (int) timeLayer.getDimensions()[0];
						int swathIndex = DateUtils.binarySearch(time, pingCount, index -> {
							return index >= 0 && index < pingCount ? DateUtils.epochNanoToInstant(timeLayer.get(index))
									: null;
						});
						if (swathIndex >= 0 && swathIndex <= player.getData().maxLimit())
							player.setCurrent(swathIndex);
						else
							logger.warn("GlobalCursor ignored, index {} is out of range", swathIndex);

					} catch (GIOException e) {
						logger.warn("GlobalCursor ignored", e);
					}
				} else {
					player.noCurrent();
				}
			});
		}
	}

	/** {@inheritDoc} */
	@Override
	public float getVerticalOffset() {
		return displayParameters.isVerticalOffsetEnabled() ? displayParameters.getVerticalOffset() : 0f;
	}

	protected double getVerticalExaggeration() {
		return verticalExaggeration;
	}

	protected String guessName() {
		return "legacy_wc_" + new File(info.getPath()).getName();
	}

	/**
	 * @return the {@link #currentBeamGroupId}
	 */
	public int getCurrentBeamGroupId() {
		return currentBeamGroupId;
	}

	/**
	 * @param displayParameters the {@link #displayParameters} to set
	 */
	protected void setDisplayParameters(VolumicRendererDisplayParameters displayParameters) {
		this.displayParameters = displayParameters;
	}

	@Override
	public Sector getSector() {
		return sector.orElse(null);
	}

	/**
	 * @return the {@link #container}
	 */
	public SounderDataContainer getContainer() {
		return container;
	}

	/**
	 * @return the {@link #inUseInViewer2d}
	 */
	public boolean isInUseInViewer2d() {
		return inUseInViewer2d;
	}

	/**
	 * @param inUseInViewer2d the {@link #inUseInViewer2d} to set
	 */
	public void setInUseInViewer2d(boolean inUseInViewer2d) {
		if (this.inUseInViewer2d != inUseInViewer2d) {
			this.inUseInViewer2d = inUseInViewer2d;
			firePropertyChange(IN_USE_IN_VIEWER2D, null, this);
		}
	}
}
