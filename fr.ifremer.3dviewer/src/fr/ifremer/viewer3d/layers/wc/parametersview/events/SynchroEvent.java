package fr.ifremer.viewer3d.layers.wc.parametersview.events;

import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.viewer3d.layers.wc.parametersview.deprecated.PlayerComposite;



public class SynchroEvent implements BaseEvent {
	private boolean synchro;
	private PlayerComposite sender;
	private String name;
	private String windowID;
	//private boolean checked;

	public SynchroEvent(PlayerComposite c, boolean s, String w, String n) { //, boolean ch
		sender = c;
		synchro = s;
		windowID = w;
		name = n;
		//checked = ch;
	}

	public boolean getSynchro() {
		return synchro;
	}

	public PlayerComposite getSender() {
		return sender;
	}

	public String getName() {
		return name;
	}

	public String getWindowID() {
		return windowID;
	}

	/*public boolean getChecked() {
		// TODO Auto-generated method stub
		return checked;
	}*/

}
