package fr.ifremer.viewer3d.layers.wc.parametersview.deprecated;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.AbstractPlayableLayer;
import fr.ifremer.viewer3d.layers.wc.parametersview.events.CloseView2DEvent;
import fr.ifremer.viewer3d.layers.wc.parametersview.events.NewView2DEvent;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;

/**
 * Composite for layers with a PlayerBean
 * 
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 * 
 * @deprecated use {@link IndexedPlayer} instead
 */
@Deprecated
public class PlayerComposite extends Composite implements PropertyChangeListener {

	private final String WINDOW_3D_NAME = "3D";
	protected PlayerBean playerBean;

	protected final Group playerGroup;

	// SWT widget
	protected Combo selectedWindowCombo;
	protected Button synchroButton;
	private Spinner minIndexSpinner;
	private Spinner currentIndexSpinner;
	private Spinner maxIndexSpinner;
	private Scale indexSlider;
	private Button stepBackwardButton;
	private Button reverseButton;
	private Button playButton;
	private Button stepForwardButton;
	private Button repeatButton;
	private Label speedLabel;
	private Combo speedCombo;
	private Spinner offsetSpinner;
	private Spinner lengthBeforeSpinner;
	private Spinner lengthAfterSpinner;
	private Label beforeLabel;
	private Label afterLabel;
	private Button displayGroundButton;

	protected String currentWindowID = WINDOW_3D_NAME;
	protected Map<String, String> registeredWindows;
	protected List<String> sortedRegisteredWindows;

	private AbstractPlayableLayer layer;
	final private String speedValues[] = { "0.1", "0.2", "0.5", "1.0", "2.0", "4.0", "8.0" };

	public PlayerComposite(Composite parent, int style, AbstractPlayableLayer layer, PlayerBean bean) {
		super(parent, style);
		this.layer = layer;
		this.playerBean = bean;
		this.playerBean.addPropertyChangeListener(this);
		this.playerGroup = new Group(this, SWT.NONE);
		this.registeredWindows = new HashMap<>();
		this.sortedRegisteredWindows = new ArrayList<>();

		inititialize();
		// ModelRoot.getInstance().getEventBus().subscribe(this);

		addDisposeListener(e -> {
			playerBean.removePropertyChangeListener(this);
			// ModelRoot.getInstance().getEventBus().unsubscribe(this);
		});
	}

	@Override
	public void dispose() {
		playerBean.removePropertyChangeListener(this);
		super.dispose();
	}

	//@Handler
	private void onNew2DView(NewView2DEvent evt) {
		String name = getMasterPlayer().getName();
		if (evt.getPlayerBean().getName().equals(name)) {
			String wID = evt.getWindowID();
			currentWindowID = wID;
			playerGroup.setText(computeViewName(wID, name));
			registerWindow(name, wID);
		}
	}

	//@Handler
	private void on2DViewClosed(CloseView2DEvent evt) {
		if (evt.getName().equals(playerBean.getName())) {
			UnRegisterWindow(computeViewName(evt.getWindowID(), evt.getName()));
			if (currentWindowID == evt.getWindowID()) {
				select3DView();
			}
		}
	}

	public void select3DView() {
		updatePlayer(getMasterPlayer(), true, WINDOW_3D_NAME);
	}

	public PlayerBean getMasterPlayer() {
		return layer.getPlayerBean();
	}

	public void updatePlayer(PlayerBean bean, boolean synchro, String windowID) {
		if (playerBean != bean) {
			playerBean.removePropertyChangeListener(this);
			bean.addPropertyChangeListener(this);
			playerBean = bean;
			refreshComposite();
		}
		synchroButton.setSelection(synchro);
		currentWindowID = windowID;

		String name = getSelectedViewName();
		playerGroup.setText(name);
		selectedWindowCombo.select(sortedRegisteredWindows.indexOf(name));
		synchroButton.setEnabled(isSynchroButtonEnabled());
	}

	private String computeViewName(String windowID, String name) {
		return String.format("%s - %s", windowID, name);
	}

	protected String getSelectedViewName() {
		return computeViewName(currentWindowID, playerBean.getName());
	}

	protected void refreshComposite() {
		playerBean.removePropertyChangeListener(this);
		playerBean.addPropertyChangeListener(this);

		minIndexSpinner.setMinimum(playerBean.getMinPlayNumber());
		minIndexSpinner.setMaximum(playerBean.getMaxPlayNumber());
		minIndexSpinner.setSelection(playerBean.getMinPlayNumber());

		currentIndexSpinner.setMinimum(playerBean.getMinPlayNumber());
		currentIndexSpinner.setMaximum(playerBean.getMaxPlayNumber());
		currentIndexSpinner.setSelection(playerBean.getPlayNumber());

		maxIndexSpinner.setMinimum(playerBean.getMinPlayNumber());
		maxIndexSpinner.setMaximum(playerBean.getMaxPlayNumber());
		maxIndexSpinner.setSelection(playerBean.getMaxPlayNumber());

		indexSlider.setMinimum(playerBean.getMinPlayIndex());
		indexSlider.setMaximum(playerBean.getMaxPlayIndex());
		indexSlider.setSelection(playerBean.getPlayIndex());
		int inc = (playerBean.getMaxPlayIndex() - playerBean.getMinPlayIndex());
		if (inc == 0) {
			inc = 1;
		}
		indexSlider.setPageIncrement(inc);
		repeatButton.setSelection(playerBean.getLoop());

		speedCombo.select(Arrays.asList(speedValues).indexOf(Float.toString(playerBean.getPlayingSpeed())));

		refreshPlayingButtons();
	}

	private void refreshPlayingButtons() {
		boolean playing = playerBean.getPlaying();
		boolean reversePlaying = playerBean.getReversePlaying();
		playButton.setSelection(playing);
		reverseButton.setSelection(reversePlaying);

		if (playing) {
			playButton.setImage(ImageResources.getImage("/icons/32/media-playback-pause.png", getClass()));
			reverseButton.setImage(ImageResources.getImage("/icons/32/media-reverse-start.png", getClass()));
		} else if (reversePlaying) {
			reverseButton.setImage(ImageResources.getImage("/icons/32/media-playback-pause.png", getClass()));
			playButton.setImage(ImageResources.getImage("/icons/32/media-playback-start.png", getClass()));
		} else {
			playButton.setImage(ImageResources.getImage("/icons/32/media-playback-start.png", getClass()));
			reverseButton.setImage(ImageResources.getImage("/icons/32/media-reverse-start.png", getClass()));
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void inititialize() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);

		createPlayerGroup();
		createOffsetGroup();
		createLengthGroup();
		createPointSizeGroup();
		createDisplayGroundGroup();
		// ModelRoot.getInstance().getEventBus().publish(new NewPlayerCompositeEvent(this));
	}

	public void registerWindow(String name, String windowID) {
		registeredWindows.put(computeViewName(windowID, name), windowID);
		updateSortedRegisteredWindows();
	}

	private void UnRegisterWindow(String name) {
		registeredWindows.remove(name);
		updateSortedRegisteredWindows();
	}

	private void updateSortedRegisteredWindows() {
		String name = computeViewName(WINDOW_3D_NAME, playerBean.getName());

		// Sort registered windows and keep 3D first
		sortedRegisteredWindows = new ArrayList<>(registeredWindows.keySet());
		sortedRegisteredWindows.remove(name);
		java.util.Collections.sort(sortedRegisteredWindows);
		sortedRegisteredWindows.add(0, name);

		// Refresh the list of windows
		selectedWindowCombo.setItems(getRegisteredWindowsArray());
		selectedWindowCombo.select(sortedRegisteredWindows.indexOf(getSelectedViewName()));
	}

	private String[] getRegisteredWindowsArray() {
		return sortedRegisteredWindows.toArray(new String[0]);
	}

	private Group createPlayerGroup() {
		// player group
		playerGroup.setText(getSelectedViewName());
		playerGroup.setLayout(new GridLayout(4, true));
		playerGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		selectedWindowCombo = new Combo(playerGroup, SWT.READ_ONLY);
		synchroButton = new Button(playerGroup, SWT.CHECK);
		minIndexSpinner = new Spinner(playerGroup, SWT.NONE);
		currentIndexSpinner = new Spinner(playerGroup, SWT.NONE);
		maxIndexSpinner = new Spinner(playerGroup, SWT.NONE);
		indexSlider = new Scale(playerGroup, SWT.NONE);
		stepBackwardButton = new Button(playerGroup, SWT.NONE);
		reverseButton = new Button(playerGroup, SWT.TOGGLE);
		playButton = new Button(playerGroup, SWT.TOGGLE);
		stepForwardButton = new Button(playerGroup, SWT.NONE);
		repeatButton = new Button(playerGroup, SWT.CHECK);
		speedLabel = new Label(playerGroup, SWT.NONE);
		speedCombo = new Combo(playerGroup, SWT.READ_ONLY);

		// Menu reset for minSpinner
		final Menu resetMinMenu = new Menu(Display.getDefault().getActiveShell(), SWT.POP_UP);
		final MenuItem resetMinMenuItem = new MenuItem(resetMinMenu, SWT.PUSH);
		resetMinMenuItem.setText("Reset");
		resetMinMenuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				minIndexSpinner.setSelection(playerBean.getDefaultMinPlayIndex());
			}
		});

		selectedWindowCombo.setToolTipText("Selected view");
		registerWindow(playerBean.getName(), currentWindowID);
		selectedWindowCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				currentViewChanged();
			}
		});
		selectedWindowCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		synchroButton.setText("Synchronize");
		synchroButton.setToolTipText("Synchronize player with the 3D view");
		synchroButton.setSelection(true);
		synchroButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				changeSynchro(synchroButton.getSelection());
			}
		});
		synchroButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		synchroButton.setEnabled(isSynchroButtonEnabled());

		minIndexSpinner.setToolTipText("First image index");
		minIndexSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.setPlaying(false);
				playerBean.setReversePlaying(false);
				playerBean.setMinPlayNumber(minIndexSpinner.getSelection());

				if (minIndexSpinner.getSelection() > indexSlider.getSelection()) {
					playerBean.setPlayNumber(minIndexSpinner.getSelection());
				}

				currentIndexSpinner.setMinimum(minIndexSpinner.getSelection());
				indexSlider.setMinimum(minIndexSpinner.getSelection() - 1);
			}
		});
		// mouse listener for resetMenu
		minIndexSpinner.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.button == MouseEvent.BUTTON3) {
					Rectangle bounds = minIndexSpinner.getBounds();
					Point point = playerGroup.toDisplay(bounds.x, bounds.y + bounds.height);
					resetMinMenu.setLocation(point);
					resetMinMenu.setVisible(true);
				}
			}
		});
		minIndexSpinner.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

		// Menu reset for currentSpinner
		final Menu resetCurrentMenu = new Menu(Display.getDefault().getActiveShell(), SWT.POP_UP);
		final MenuItem resetCurrentMenuItem = new MenuItem(resetCurrentMenu, SWT.PUSH);
		resetCurrentMenuItem.setText("Reset");
		resetCurrentMenuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				currentIndexSpinner.setSelection(playerBean.getMinPlayIndex());
			}
		});

		currentIndexSpinner.setToolTipText("Current image index");
		currentIndexSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.setPlaying(false);
				playerBean.setReversePlaying(false);
				playerBean.setPlayNumber(currentIndexSpinner.getSelection());
			}
		});
		// mouse listener for resetMenu
		currentIndexSpinner.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.button == MouseEvent.BUTTON3) {
					Rectangle bounds = currentIndexSpinner.getBounds();
					Point point = playerGroup.toDisplay(bounds.x, bounds.y + bounds.height);
					resetCurrentMenu.setLocation(point);
					resetCurrentMenu.setVisible(true);
				}
			}
		});

		currentIndexSpinner.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));

		// Menu reset for maxSpinner
		final Menu resetMaxMenu = new Menu(Display.getDefault().getActiveShell(), SWT.POP_UP);
		final MenuItem resetMaxMenuItem = new MenuItem(resetMaxMenu, SWT.PUSH);
		resetMaxMenuItem.setText("Reset");
		resetMaxMenuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				maxIndexSpinner.setSelection(playerBean.getDefaultMaxPlayIndex());
			}
		});

		maxIndexSpinner.setToolTipText("Last image index");
		maxIndexSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.setPlaying(false);
				playerBean.setReversePlaying(false);
				playerBean.setMaxPlayNumber(maxIndexSpinner.getSelection());

				if (maxIndexSpinner.getSelection() < indexSlider.getSelection()) {
					playerBean.setPlayNumber(maxIndexSpinner.getSelection());
				}
				currentIndexSpinner.setMaximum(maxIndexSpinner.getSelection());
				indexSlider.setMaximum(maxIndexSpinner.getSelection());
			}
		});
		// mouse listener for resetMaxMenu
		maxIndexSpinner.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.button == MouseEvent.BUTTON3) {
					Rectangle bounds = maxIndexSpinner.getBounds();
					Point point = playerGroup.toDisplay(bounds.x, bounds.y + bounds.height);
					resetMaxMenu.setLocation(point);
					resetMaxMenu.setVisible(true);
				}
			}
		});
		maxIndexSpinner.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

		indexSlider.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.setPlaying(false);
				playerBean.setReversePlaying(false);
				// GLOBE576
				playerBean.setPlayIndex(indexSlider.getSelection());
			}
		});
		indexSlider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		stepBackwardButton.setToolTipText("Previous image");
		stepBackwardButton.setImage(ImageResources.getImage("/icons/32/media-step-backward.png", getClass()));
		stepBackwardButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.stepBackward();
			}
		});
		stepBackwardButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		reverseButton.setToolTipText("Play reverse");
		reverseButton.setImage(ImageResources.getImage("/icons/32/media-reverse-start.png", getClass()));
		reverseButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.revertPlayPause();
			}
		});
		reverseButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		playButton.setToolTipText("Play/Pause");
		playButton.setImage(ImageResources.getImage("/icons/32/media-playback-start.png", getClass()));
		playButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.playPause();
			}
		});
		playButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		stepForwardButton.setToolTipText("Next image");
		stepForwardButton.setImage(ImageResources.getImage("/icons/32/media-step-forward.png", getClass()));
		stepForwardButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.stepForward();
			}
		});
		stepForwardButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		repeatButton.setText("Repeat");
		repeatButton.setToolTipText("Repeat");
		repeatButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.setLoop(repeatButton.getSelection());
			}
		});
		repeatButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		speedLabel.setText("X");
		speedLabel.setLayoutData(new GridData(SWT.END, SWT.CENTER, true, false));

		speedCombo.setToolTipText("speed");
		speedCombo.setItems(speedValues);
		speedCombo.select(3);
		speedCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				playerBean.setPlayingSpeed(Float.parseFloat(speedValues[speedCombo.getSelectionIndex()]));
			}
		});
		speedCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		addListener(SWT.Hide, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (Viewer3D.getModel() != null) {
					//Viewer3D.getModel().getPlayer().addPlayerBean(playerBean);
				}
			}
		});

		addListener(SWT.Show, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (Viewer3D.getModel() != null) {
					//Viewer3D.getModel().getPlayer().removePlayerBean(playerBean);
				}
			}
		});

		refreshComposite();

		return playerGroup;
	}

	private void createOffsetGroup() {

		final Group offsetGroup = new Group(this, SWT.NONE);
		offsetGroup.setText("Offset");
		offsetGroup.setLayout(new GridLayout(2, true));
		offsetGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));

		offsetSpinner = new Spinner(offsetGroup, SWT.NONE);
		offsetSpinner.setToolTipText("Offset");
		offsetSpinner.setDigits(2);
		offsetSpinner.setMinimum(-1000000);
		offsetSpinner.setMaximum(1000000);
		offsetSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				layer.setOffset(offsetSpinner.getSelection() / 100.0f);
				RefreshOpenGLView.fullRefresh();
			}
		});
		new Label(offsetGroup, SWT.NONE);
	}

	private void createLengthGroup() {

		final Group lengthGroup = new Group(this, SWT.NONE);
		lengthGroup.setText("Length of volumetric water column");
		lengthGroup.setLayout(new GridLayout(4, false));
		lengthGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));

		beforeLabel = new Label(lengthGroup, SWT.NONE);
		beforeLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		beforeLabel.setText("Before :");

		lengthBeforeSpinner = new Spinner(lengthGroup, SWT.NONE);
		lengthBeforeSpinner.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		lengthBeforeSpinner.setToolTipText("Number of pings before current index.");
		lengthBeforeSpinner.setMinimum(playerBean.getMinPlayIndex());
		lengthBeforeSpinner.setMaximum(playerBean.getMaxPlayIndex());

		afterLabel = new Label(lengthGroup, SWT.NONE);
		afterLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		afterLabel.setText("After :");

		lengthAfterSpinner = new Spinner(lengthGroup, SWT.NONE);
		lengthAfterSpinner.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		lengthAfterSpinner.setToolTipText("Number of pings after current index.");
		lengthAfterSpinner.setMinimum(playerBean.getMinPlayIndex());
		lengthAfterSpinner.setMaximum(playerBean.getMaxPlayIndex());
	}

	private void createPointSizeGroup() {

		final Group pointSizeGroup = new Group(this, SWT.NONE);
		pointSizeGroup.setText("Size of points drawn in the water column.");
		pointSizeGroup.setLayout(new GridLayout(1, true));
		pointSizeGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));

	}

	private void createDisplayGroundGroup() {

		final Group displayGround = new Group(this, SWT.NONE);
		displayGround.setText("Display samples under detected ground");
		displayGround.setLayout(new GridLayout(1, true));
		displayGround.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));

		displayGroundButton = new Button(displayGround, SWT.CHECK);
		displayGroundButton.setText("Display below detected ground");
	}

	protected boolean isSynchroButtonEnabled() {
		return !WINDOW_3D_NAME.equals(currentWindowID);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String name = event.getPropertyName();
		if (PlayerBean.PROPERTY_PLAY_NUMBER.equals(name)) {
			final int imgIndex = (Integer) event.getNewValue();
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					// fix focus problem
					if (currentIndexSpinner.getSelection() != imgIndex) {
						currentIndexSpinner.setSelection(imgIndex);
					}
					int index = playerBean.getIndexList().indexOf((imgIndex));
					if (index != -1) {
						indexSlider.setSelection(index);
					}
				}
			});
		} else if (PlayerBean.PROPERTY_PLAYING.equals(name) || PlayerBean.PROPERTY_REVERSEPLAYING.equals(name)) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					refreshPlayingButtons();
				}
			});
		}
	}

	public void refresh() {
		this.refreshComposite();
	}

	protected void changeSynchro(boolean isSynchronized) {
		// ModelRoot.getInstance().getEventBus().publish(new SynchroPlayerEvent(PlayerComposite.this, isSynchronized,
		// currentWindowID, playerBean.getName()));
	}

	protected void currentViewChanged() {
		if (selectedWindowCombo.getText().equals(computeViewName(WINDOW_3D_NAME, getMasterPlayer().getName()))) {
			select3DView();
		} else {
			String wID = registeredWindows.get(selectedWindowCombo.getText());
			// ModelRoot.getInstance().getEventBus().publish(new ChangeViewEvent(wID, PlayerComposite.this));
		}
	}

}
