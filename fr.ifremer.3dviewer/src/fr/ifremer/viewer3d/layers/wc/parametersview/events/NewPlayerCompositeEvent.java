package fr.ifremer.viewer3d.layers.wc.parametersview.events;

import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.viewer3d.layers.wc.parametersview.deprecated.PlayerComposite;



public class NewPlayerCompositeEvent implements BaseEvent {
	private PlayerComposite sender;

	public NewPlayerCompositeEvent(PlayerComposite s) {
		sender = s;
	}

	public PlayerComposite getSender() {
		return sender;
	}

}
