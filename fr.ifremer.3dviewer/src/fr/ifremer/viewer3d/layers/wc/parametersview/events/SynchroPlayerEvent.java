package fr.ifremer.viewer3d.layers.wc.parametersview.events;

import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.viewer3d.layers.wc.parametersview.deprecated.PlayerComposite;



public class SynchroPlayerEvent implements BaseEvent {
	private boolean synchro;
	private PlayerComposite sender;
	private String name;
	private String windowID;

	public SynchroPlayerEvent(PlayerComposite c, boolean s, String w, String n) { //, boolean ch
		sender = c;
		synchro = s;
		windowID = w;
		name = n;
	}

	public boolean getSynchro() {
		return synchro;
	}

	public PlayerComposite getSender() {
		return sender;
	}

	public String getName() {
		return name;
	}

	public String getWindowID() {
		return windowID;
	}
}
