package fr.ifremer.viewer3d.layers.wc.parametersview.events;

import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.viewer3d.layers.wc.parametersview.deprecated.PlayerComposite;



public class ChangeViewEvent implements BaseEvent {

	private String windowID;

	private PlayerComposite playerComposite;
	
	public ChangeViewEvent(String w, PlayerComposite c) {
		windowID = w;
		playerComposite = c;
	}

	/**
	 * @return the {@link #playerComposite}
	 */
	public PlayerComposite getPlayerComposite() {
		return playerComposite;
	}

	/**
	 * @return the {@link #windowID}
	 */
	public String getWindowID() {
		return windowID;
	}
}
