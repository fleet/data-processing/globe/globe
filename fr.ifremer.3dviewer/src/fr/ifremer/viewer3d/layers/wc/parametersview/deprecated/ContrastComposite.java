package fr.ifremer.viewer3d.layers.wc.parametersview.deprecated;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.widget.color.ColorComposite;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.plume.parametersview.ContrastBaseComposite;
import fr.ifremer.viewer3d.layers.plume.parametersview.OffsetComposite;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;
import gov.nasa.worldwind.avlist.AVKey;

/**
 * Composite that changes contrast and color of MyAbstractLayer
 *
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 */
@Deprecated
public class ContrastComposite extends Composite implements PropertyChangeListener {

	protected MyAbstractLayer layer;

	// SWT Widget
	protected Spinner minSpinner;
	protected Spinner maxSpinner;
	protected Spinner minTransparencySpinner;
	protected Spinner maxTransparencySpinner;
	protected Button minMaxButton;
	protected Button customButton;
	protected Combo colormapCombo;
	protected Combo colormapOriCombo;
	protected Button invertButton;
	protected Spinner offsetSpinner;

	/** Scalar of values. */
	protected int scalar = 100;

	private ColorComposite colorComposite;

	public ContrastComposite(Composite parent, int style, MyAbstractLayer layer) {
		super(parent, style);
		this.layer = layer;
		init();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		if (layer.getMaxTextureValue() * 100d > Integer.MAX_VALUE
				|| layer.getMinTextureValue() * 100d > Integer.MIN_VALUE) {
			scalar = 1;
		}
		createContrastGroup();
	}

	private void createContrastGroup() {
		// Contrast Group
		Group transparencyGroup = new Group(this, SWT.NONE);
		transparencyGroup.setText("Filtering Threshold");
		transparencyGroup.setLayout(new GridLayout(1, false));
		transparencyGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));

		minTransparencySpinner = new Spinner(transparencyGroup, SWT.NONE);
		maxTransparencySpinner = new Spinner(transparencyGroup, SWT.NONE);

		minTransparencySpinner.setEnabled(true);
		minTransparencySpinner.setMaximum((int) layer.getMaxTextureValue() * scalar);
		minTransparencySpinner.setMinimum((int) layer.getMinTextureValue() * scalar);
		minTransparencySpinner.setDigits(scalar > 1 ? 2 : 0); // allow 2 decimal places
		minTransparencySpinner.setSelection((int) layer.getMinTransparency() * scalar);
		minTransparencySpinner.addListener(SWT.Selection, event -> {
			layer.setMinTransparency(minTransparencySpinner.getSelection() / (double) scalar);
			updateCustomStep(minTransparencySpinner, maxTransparencySpinner);
			RefreshOpenGLView.fullRefresh();
			layer.firePropertyChange(AVKey.LAYER, null, layer);

		});
		minTransparencySpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		maxTransparencySpinner.setEnabled(true);
		maxTransparencySpinner.setMaximum((int) layer.getMaxTextureValue() * scalar);
		maxTransparencySpinner.setMinimum((int) layer.getMinTextureValue() * scalar);
		maxTransparencySpinner.setDigits(scalar > 1 ? 2 : 0); // allow 2 decimal places
		maxTransparencySpinner.setSelection((int) layer.getMaxTransparency() * scalar);
		maxTransparencySpinner.addListener(SWT.Selection, event -> {
			layer.setMaxTransparency(maxTransparencySpinner.getSelection() / (double) scalar);
			updateCustomStep(minTransparencySpinner, maxTransparencySpinner);
			RefreshOpenGLView.fullRefresh();
			layer.firePropertyChange(AVKey.LAYER, null, layer);
		});

		maxTransparencySpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		// Contrast Group
		ContrastBaseComposite contrastComposite = new ContrastBaseComposite(this, SWT.FILL, layer);
		contrastComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// Original Color Group
		createOriginalColorGroup(this);

		// Color Group
		colorComposite = new ColorComposite(this, SWT.NONE, true, layer.getAvailableColorMaps(),
				(int) layer.getColorMap(), layer.isInverseColorMap());
		colorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		colorComposite.addPropertyChangeListener(this);

		// offset group
		OffsetComposite offsetComposite = new OffsetComposite(this, SWT.FILL, layer);
		offsetComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	}

	protected void createOriginalColorGroup(Composite contrastTabGroup) {
		Group colorOriginalGroup = new Group(contrastTabGroup, SWT.NONE);
		colorOriginalGroup.setText("Color Original");
		colorOriginalGroup.setLayout(new GridLayout(2, false));
		colorOriginalGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		final Button disableOriginalButton = new Button(colorOriginalGroup, SWT.CHECK);
		colormapOriCombo = new Combo(colorOriginalGroup, SWT.READ_ONLY);
		disableOriginalButton.setSelection(false);
		disableOriginalButton.addListener(SWT.Selection, event -> {
			colormapOriCombo.setEnabled(disableOriginalButton.getSelection());
			Viewer3D.getWwd().redraw();
			layer.firePropertyChange(AVKey.LAYER, null, layer);
		});
		disableOriginalButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		layer.setUseColorMap(true);

		colormapOriCombo.setEnabled(disableOriginalButton.getSelection());
		colormapOriCombo.add("Jet", 0);
		colormapOriCombo.add("Catherine", 1);
		colormapOriCombo.add("Grey", 2);
		colormapOriCombo.select((int) layer.getColorMapOri());
		colormapOriCombo.addListener(SWT.Selection, event -> {
			layer.setColorMapOri(colormapOriCombo.getSelectionIndex());
			Viewer3D.getWwd().redraw();
			layer.firePropertyChange(AVKey.LAYER, null, layer);
		});
		colormapOriCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {

		String eventName = event.getPropertyName();
		if (eventName.equals(ColorComposite.PROPERTY_COLORMAP_UPDATE)) {
			layer.setColorMap((int) event.getNewValue());
		} else if (eventName.equals(ColorComposite.PROPERTY_INVERT_UPDATE)) {
			layer.setInverseColorMap((boolean) event.getNewValue());
		}
		Viewer3D.getWwd().redraw();
		layer.firePropertyChange(AVKey.LAYER, null, layer);
	}

	/**
	 * Update the custom step for a couple of min and max spinners.
	 *
	 * @param min The min spinner
	 * @param max The max spinner
	 */
	private void updateCustomStep(Spinner min, Spinner max) {
		double customStep = Math.abs(max.getSelection() - min.getSelection()) / 100d;
		min.setIncrement((int) customStep);
		max.setIncrement((int) customStep);
	}
}
