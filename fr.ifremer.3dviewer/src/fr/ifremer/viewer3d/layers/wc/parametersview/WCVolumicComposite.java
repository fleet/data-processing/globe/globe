package fr.ifremer.viewer3d.layers.wc.parametersview;

import java.beans.PropertyChangeListener;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import jakarta.inject.Inject;

import org.apache.commons.lang.math.FloatRange;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.model.wc.SpatializationMethodParameter;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.widget.AbstractParametersCompositeWithTabs;
import fr.ifremer.globe.ui.widget.EnumSelector;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget.SpinnerWidgetLayout;
import fr.ifremer.globe.ui.widget.wc.WCFilterParametersComposite;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.Result;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.gui.wcmarker.PoiCreateWidget;
import fr.ifremer.viewer3d.gui.wcmarker.PoiData;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;
import fr.ifremer.viewer3d.layers.sync.wc.WWWaterColumnVolumicSynchronizer;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters.Rendering;
import io.reactivex.BackpressureStrategy;

/**
 * Composite which contains all tools relative to water column features.
 */
public class WCVolumicComposite extends AbstractParametersCompositeWithTabs {

	/** {@link WCVolumicLayer} handled by this composite **/
	private final WCVolumicLayer layer;
	private final PropertyChangeListener wcVolumicLayerListener = e -> onWCLayerPropertyChanged();

	/** The layer parameters synchronizer get from Context **/
	@Inject
	private WWWaterColumnVolumicSynchronizer layersSynchronizer;
	@Inject
	private IParametersViewService parametersViewService;

	private LayerParametersSynchronizerComposite<WCVolumicLayer> grpSynchronization;

	/** Save the selected tab index **/
	private static int selectedTab;

	/** Widgets */
	private final Button btnEnable;

	/**
	 * Contructor of {@link WaterColumnNcwComposite}.
	 *
	 * @param parent parent of this composite
	 * @param layer layer concerned by the composite tools
	 */
	public WCVolumicComposite(Composite parent, WCVolumicLayer layer) {
		super(parent);
		this.layer = layer;
		ContextInitializer.inject(this);

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean activate = btnEnable.getSelection();
				Result res = layer.activate(activate);
				if (res.isSuccess()) {
					if (tabFolder == null) {
						initialize();
					}
					grpSynchronization.setVisible(activate);
					tabFolder.setVisible(activate);
					IGeographicViewService.grab().refresh();
				} else {
					btnEnable.setEnabled(false);
					Messages.openErrorMessage("WC loading error", res.getErrorMessage());
				}
			}
		});
		btnEnable.setText("Display water column");
		btnEnable.setSelection(layer.isLayerActive());

		if (layer.isInUseInViewer2d()) {
			btnEnable.setEnabled(false);
			UIUtils.asyncExecSafety(btnEnable, () -> Messages.openToolTip(btnEnable,
					"Unauthorised deactivation\nThe layer is currently being used by the 2D viewer"));
		}
		layer.addPropertyChangeListener(wcVolumicLayerListener);
		addDisposeListener(e -> layer.removePropertyChangeListener(wcVolumicLayerListener));

		if (layer.isLayerActive()) {
			initialize();
		}
	}

	/**
	 * Initializes the composite : builds inner composites & tabs.
	 */
	private void initialize() {
		createSynchGroup();
		createPlayerTab();
		createDisplayTab();
		createFilterTab();
		createPositionTab();
		tabFolder.setSelection(selectedTab);
		IParametersViewService.grab().resize();
	}

	@Override
	protected void onSelectedTab(int selectedTab) {
		WCVolumicComposite.selectedTab = selectedTab;
	}

	/**
	 * Creates group with synchronization buttons.
	 */
	private void createSynchGroup() {
		grpSynchronization = new LayerParametersSynchronizerComposite<>(WCVolumicComposite.this, layersSynchronizer,
				layer);
		grpSynchronization.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		grpSynchronization.adaptToLayer(layer);
	}

	/**
	 * Creates a tab with a player.
	 */
	private void createPlayerTab() {
		Composite container = super.createPlayerTab(layer.getPlayer());

		Group grpSwathDisplayedParams = new Group(container, SWT.NONE);
		grpSwathDisplayedParams.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSwathDisplayedParams.setText("swath displayed parameters");
		grpSwathDisplayedParams.setLayout(new GridLayout(5, false));

		// skip
		Button skipButton = new Button(grpSwathDisplayedParams, SWT.CHECK);
		skipButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 5, 1));
		skipButton.setText("skip missing swath");
		skipButton.setToolTipText("Skip missing swath");
		skipButton.setSelection(layer.getSkipMissingSwath());
		skipButton.addListener(SWT.Selection, event -> layer.setSkipMissingSwath(skipButton.getSelection()));
		// count
		Label lblPingCountValue = new Label(grpSwathDisplayedParams, SWT.NONE);
		lblPingCountValue.setText("swath count");
		lblPingCountValue.setToolTipText("Number of swaths displayed before current swath (included)");

		Spinner pingCountSpinner = new Spinner(grpSwathDisplayedParams, SWT.BORDER);
		pingCountSpinner.setDigits(0);
		pingCountSpinner.setIncrement(1);
		pingCountSpinner.setMinimum(1);
		pingCountSpinner.setMaximum(Integer.MAX_VALUE);
		pingCountSpinner.setSelection(layer.getBufferBackwardLength() + layer.getBufferForwardLength() + 1);

		pingCountSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		pingCountSpinner.addListener(SWT.Selection,
				event -> layer.setBufferBackwardLength(pingCountSpinner.getSelection() - 1));

		Text lblPingRange = new Text(grpSwathDisplayedParams, SWT.READ_ONLY);
		lblPingRange.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
		layer.getPlayer().subscribe(data -> UIUtils.asyncExecSafety(lblPingRange, () -> {
			if (pingCountSpinner.getSelection() > 1) {
				lblPingRange
						.setText("swath range : " + layer.getBuffer().getMinMaxForCurrentIndex(data.currentValue()));
			} else {
				lblPingRange.setText("");
			}
			lblPingRange.setRedraw(true);
		}));

		if (layer.getBeamGroupCount() > 1) {
			Group grpBeamGroup = new Group(container, SWT.NONE);
			grpBeamGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			grpBeamGroup.setText("Beam group");
			grpBeamGroup.setLayout(new GridLayout(5, false));

			Label lblBeamGroupId = new Label(grpBeamGroup, SWT.NONE);
			lblBeamGroupId.setText("Index of beam group");

			Spinner spnBeamGroupId = new Spinner(grpBeamGroup, SWT.BORDER);
			spnBeamGroupId.setDigits(0);
			spnBeamGroupId.setIncrement(1);
			spnBeamGroupId.setMinimum(1);
			spnBeamGroupId.setMaximum(layer.getBeamGroupCount());
			spnBeamGroupId.setSelection(layer.getCurrentBeamGroupId());
			spnBeamGroupId.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			spnBeamGroupId.addListener(SWT.Selection,
					event -> layer.switchToBeamGroupId(spnBeamGroupId.getSelection()));
		}

		Button btmPOI = new Button(container, SWT.NONE);
		btmPOI.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PoiData s = new PoiData(layer.getInfo().getFilename(), "", "", "...", layer.getDisplayParameters(),
						layer.getPlayer().getData(), layer.getFilterParameters(),
						layer.getBuffer().getBufferSettings());
				PoiCreateWidget v = new PoiCreateWidget(WCVolumicComposite.this.getShell(), s);
				v.open();
			}
		});
		btmPOI.setText("add Point of Interest");

		Button btnCsvExport = new Button(container, SWT.NONE);
		btnCsvExport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// first query a file name
				// Only one file to export: Dialog box to select file name
				FileDialog dialog = new FileDialog(WCVolumicComposite.this.getShell(), SWT.SAVE);
				String[] FORMAT_EXTENSIONS = new String[] { "*.csv", "*.txt" };
				dialog.setFilterExtensions(FORMAT_EXTENSIONS);
				dialog.setFileName(FileUtils.changeExtension(layer.getInfo().getFilename(), ".csv"));
				final String fileName = dialog.open();
				if (fileName != null) {
					try {
						layer.exportAsCSV(fileName);
					} catch (GIOException e1) {
						Messages.openErrorMessage("Error during export",
								"Unexpected error while exporting Water column data, see console log for detailed message "
										+ System.lineSeparator() + e1.getMessage());
					}
				}

			}
		});
		btnCsvExport.setText("Export to CSV");
	}

	/**
	 * Creates a tab which contains display options.
	 */
	private void createDisplayTab() {
		Composite container = createTab("Display");

		// Display params
		VolumicRendererDisplayParameters displayParams = layer.getDisplayParameters();

		////////////////////////
		// Rendering mode
		EnumSelector<Rendering> renderingWidget = new EnumSelector<>(container, SWT.READ_ONLY,
				layer.getDisplayParameters().getRendering(), "Wc rendering mode");
		renderingWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		renderingWidget.getPublisher().debounce(300, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					VolumicRendererDisplayParameters param = layer.getDisplayParameters();
					layer.updateRendererParameters(param.withRendering(model.selection));
					layersSynchronizer.synchonizeWith(layer);
				});

		/////////////////
		// Normalization group
		Group normalizationGroup = new Group(container, SWT.NONE);
		normalizationGroup.setText("Normalization by range (compensation)");
		normalizationGroup.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));
		normalizationGroup.setLayout(new GridLayout(3, false));
		
		// create offset widget
		var normalizationOffsetModel = new EnabledNumberModel(displayParams.isNormalizationEnabled(),
				displayParams.getNormalizationOffset());
		
		var normalizationOffsetWidget = new SpinnerWidget("Enable with ref level : ", normalizationGroup, normalizationOffsetModel, 1, SpinnerWidgetLayout.HORIZONTAL);
		normalizationOffsetWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		// response to event
		normalizationOffsetWidget.subscribe(model -> {
			layer.updateRendererParameters(
					layer.getDisplayParameters().withNormalization(model.enable, model.value.floatValue()));
			layersSynchronizer.synchonizeWith(layer);
		});

		
		Button btAutoNormalizationOffset = new Button(normalizationGroup, SWT.NONE);
		btAutoNormalizationOffset.setText("Auto");
		btAutoNormalizationOffset.setToolTipText("Estimate from current ping");
		btAutoNormalizationOffset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					float offset = layer.estimateNormalizationOffset();
					var model = new EnabledNumberModel(layer.getDisplayParameters().isNormalizationEnabled(),
							offset);
					normalizationOffsetWidget.setModel(model);
				} catch (GIOException e1) {
					// IGNORE
				}
			}
		});
		/////////////////
		// Color Contrast
		ColorContrastModelBuilder modelBuilder = new ColorContrastModelBuilder()
				.setColorMapIndex(displayParams.getColorMapIndex()).setInvertColor(displayParams.isInvertColor())
				.setContrastMax(displayParams.getContrastMax()).setContrastMin(displayParams.getContrastMin())
				.setLimitMin(-127).setLimitMax(127).setOpacity((float) layer.getOpacity());
		ColorContrastModel initModel = modelBuilder.build();

		// Color constrast widget
		ColorContrastWidget colorComposite = new ColorContrastWidget(container, initModel, Optional.empty());
		colorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		colorComposite.subscribe(model -> {
			VolumicRendererDisplayParameters param = layer.getDisplayParameters();
			layer.updateRendererParameters(param.withColorModel(model.colorMapIndex(), model.invertColor(),
					model.contrastMin(), model.contrastMax(), model.opacity()));
			layersSynchronizer.synchonizeWith(layer);
		});

		Runnable auto = () -> {
			layer.computeMinMaxAmplitude();
			ColorContrastModel model = colorComposite.getModel().withContrastMinMax(layer.getMinAmplitude(),
					layer.getMaxAmplitude());
			colorComposite.setModel(model);
		};
		colorComposite.setAutoContrast(Optional.of(auto));

	}

	/**
	 * Creates a tab containing filter composites.
	 */
	private void createFilterTab() {
		Composite tabContainer = createTab("Filter");

		WCFilterParametersComposite wcFilterComposite = new WCFilterParametersComposite(tabContainer, SWT.NONE,
				layer.getFilterParameters());
		wcFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// response to event
		wcFilterComposite.getPublisher().subscribe(model -> {
			layer.setFilterParameters(model.from(layer.getFilterParameters().getSpatializationMethod()));
			layersSynchronizer.synchonizeWith(layer);
		});
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 */
	private void createPositionTab() {
		VolumicRendererDisplayParameters displayParams = layer.getDisplayParameters();
		var verticalOffsetModel = new EnabledNumberModel(displayParams.isVerticalOffsetEnabled(),
				displayParams.getVerticalOffset());
		Composite positionComposite = super.createPositionTab(verticalOffsetModel, model -> {
			layer.updateRendererParameters(
					layer.getDisplayParameters().withVerticalOffset(model.enable, model.value.floatValue()));
			layersSynchronizer.synchonizeWith(layer);
		});

		// Spatialization method
		SpatializationMethodParameter spatializationMethodParameter = layer.getFilterParameters().spatializationMethod;
		EnumSelector<SpatializationMethodParameter> spatializationWidget = new EnumSelector<>(positionComposite,
				SWT.READ_ONLY, spatializationMethodParameter, "Spatialization method");
		spatializationWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// response to event
		spatializationWidget.getPublisher().debounce(300, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					layer.setFilterParameters(layer.getFilterParameters().from(model.selection));
					layersSynchronizer.synchonizeWith(layer);
				});
	}

	/** A property changed of layer */
	private void onWCLayerPropertyChanged() {
		UIUtils.asyncExecSafety(btnEnable, () -> {
			if (btnEnable.getSelection() != layer.isLayerActive()
					|| btnEnable.getEnabled() == layer.isInUseInViewer2d())
				parametersViewService.refresh();
		});
	}

}
