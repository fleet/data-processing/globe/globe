package fr.ifremer.viewer3d.layers.wc.parametersview.events;

import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.viewer3d.model.PlayerBean;



public class NewView2DEvent implements BaseEvent {
	private String windowID;
	private PlayerBean bean;

	public NewView2DEvent(String w, PlayerBean b) {
		windowID = w;
		bean = b;
	}

	public String getWindowID() {
		return windowID;
	}

	public PlayerBean getPlayerBean() {
		return bean;
	}

}
