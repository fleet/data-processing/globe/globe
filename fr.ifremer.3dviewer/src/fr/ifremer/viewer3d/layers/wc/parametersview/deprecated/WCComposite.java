package fr.ifremer.viewer3d.layers.wc.parametersview.deprecated;

import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.UIEvents.EventTags;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.event.Event;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import fr.ifremer.viewer3d.model.IPlayerSynchronizable;

/**
 * Composite for WaterColumnLayer (from .xml)
 */
public class WCComposite extends MyAbstractWallLayerComposite {

	public WCComposite(Composite parent, int style, WaterColumnLayer layer) {
		super(parent, style, layer);
		ContextInitializer.inject(this);
	}

	/**
	 * Synchronizes player with the selected part.
	 */
	@Inject
	@Optional
	public void subscribeTopicPartActivation(@UIEventTopic(UIEvents.UILifeCycle.ACTIVATE) Event event) {
		Object element = event.getProperty(EventTags.ELEMENT);
		if (!isDisposed() && element instanceof MPart) {
			Object selection = ((MPart) element).getObject();
			if (selection instanceof IPlayerSynchronizable) {
				IPlayerSynchronizable view = (IPlayerSynchronizable) selection;
				if (playerComposite.getMasterPlayer() == view.getMasterPlayerBean()) {
					if (view.isPlayerSynchronized()) {
						// use master player bean
						playerComposite.updatePlayer(playerComposite.getMasterPlayer(), true, view.getWindowID());
					} else {
						// use view player bean
						playerComposite.updatePlayer(view.getPlayerBean(), false, view.getWindowID());
					}
				}

			}
			if (selection instanceof Viewer3D) {
				playerComposite.select3DView();
			}
		}

	}
}
