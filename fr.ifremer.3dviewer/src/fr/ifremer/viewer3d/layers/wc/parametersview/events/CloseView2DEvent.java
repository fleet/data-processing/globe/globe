package fr.ifremer.viewer3d.layers.wc.parametersview.events;

import fr.ifremer.globe.ui.events.BaseEvent;



public class CloseView2DEvent implements BaseEvent {
	private String windowID;
	private String name;

	public CloseView2DEvent(String w, String n) {
		windowID = w;
		name = n;
	}

	public String getWindowID() {
		return windowID;
	}

	public String getName() {
		return name;
	}

}
