package fr.ifremer.viewer3d.layers.wc.parametersview.deprecated;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.viewer3d.layers.MyAbstractWallLayer;

/**
 * Composite for WaterColumnLayer
 * 
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 */
public class MyAbstractWallLayerComposite extends Composite {

	private MyAbstractWallLayer layer;
	protected PlayerComposite playerComposite;


	public PlayerComposite getPlayerComposite() {
		return playerComposite;
	}

	public MyAbstractWallLayerComposite(Composite parent, int style, MyAbstractWallLayer layer) {
		super(parent, style);
		this.layer = layer;
		init();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);

		// tabfolder
		CTabFolder tabfolder = new CTabFolder(this, SWT.TOP);
		tabfolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		// if only one image, no player
		if (layer.getPlayerBean()!=null && (layer.getPlayerBean().getMinPlayNumber() != layer.getPlayerBean().getMaxPlayNumber())) {
			CTabItem playerTab = new CTabItem(tabfolder, SWT.NONE);
			tabfolder.setSelection(playerTab);
			playerTab.setText("Echogram Player");

			createPlayerGroup(tabfolder, playerTab);
		}
		CTabItem contrastTab = new CTabItem(tabfolder, SWT.NONE);
		contrastTab.setText("Contrast Panel");
		if (layer.getPlayerBean()==null || (layer.getPlayerBean().getMinPlayNumber() == layer.getPlayerBean().getMaxPlayNumber())) {
			tabfolder.setSelection(contrastTab);
		}

		// Contrast group
		createContrastGroup(tabfolder, contrastTab);
	}

	private Group createPlayerGroup(CTabFolder tabfolder, CTabItem playerTab) {
		// player group
		Group playerGroup = new Group(tabfolder, SWT.NONE);
		playerGroup.setText("Echogram Player");
		playerComposite = new PlayerComposite(tabfolder, SWT.NONE, layer, layer.getPlayerBean());
		playerTab.setControl(playerComposite);

		return playerGroup;
	}

	private Group createContrastGroup(CTabFolder tabfolder, CTabItem contrastTab) {
		// Contrast group
		Group contrastTabGroup = new Group(tabfolder, SWT.NONE);
		contrastTabGroup.setText("Contrast Panel");
		contrastTab.setControl(new ContrastComposite(tabfolder, SWT.NONE, layer));

		return contrastTabGroup;
	}

	public boolean isSynchronized() {
		return true;
	}

}
