package fr.ifremer.viewer3d.layers.wc;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.wc.Spatializer;
import fr.ifremer.globe.core.model.wc.SwathSpatialData;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.wc.render.AbstractSinglePingSamples3D;
import fr.ifremer.viewer3d.layers.wc.render.NativeSinglePingSamples3D;

public class NativeWCVolumicLayer extends WCVolumicLayer {

	private final SonarNativeProxy sonarNativeProxy;
	private SwathSpatialData swathSpatialData;

	/**
	 * Constructor
	 *
	 * @throws GIOException when the ISounderNcInfo is not suitable for Sonar Native
	 */
	public NativeWCVolumicLayer(ISounderNcInfo info) {
		super(info);
		sonarNativeProxy = new SonarNativeProxy(info);
		sonarNativeProxy.switchToBeamGroupNumber(getCurrentBeamGroupId());
	}

	@Override
	public void switchToBeamGroupId(int beamGroupId) {
		sonarNativeProxy.switchToBeamGroupNumber(beamGroupId);
		super.switchToBeamGroupId(beamGroupId);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.ofNullable(newName -> {
			NativeWCVolumicLayer result = null;
			result = new NativeWCVolumicLayer(info);
			result.name = newName;
			result.setName(newName);
			result.displayParameters = displayParameters;
			result.filterParameters = filterParameters;
			result.player.setCurrent(player.getCurrent());
			return result;
		});
	}

	@Override
	protected AbstractSinglePingSamples3D produceSinglePingSamples(int index) throws GIOException {
		Spatializer spatializer = getPingLoader().getSpatializer();
		spatializer.configure(index, getFilterParameters().sampling.getSampling());
		swathSpatialData = spatializer.getPing();

		var result = new AtomicReference<AbstractSinglePingSamples3D>();
		sonarNativeProxy.loadPing(index, getFilterParameters(), getDisplayParameters(),
				nativeBeamSample -> result.set(new NativeSinglePingSamples3D(nativeBeamSample, spatializer,
						this::getVerticalOffset, this::getVerticalExaggeration)));
		geographicViewService.refresh();
		return result.get();
	}

	@Override
	public float estimateNormalizationOffset() throws GIOException {
		return sonarNativeProxy.estimateNormalizationRefLevel(player.getCurrent());
	}
	
	
	@Override
	protected String guessName() {
		return "wc_" + new File(getInfo().getPath()).getName();
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		sonarNativeProxy.dispose();
		super.dispose();
	}

	/**
	 * @return the {@link #swathSpatialData}
	 */
	public SwathSpatialData getSwathSpatialData() {
		return swathSpatialData;
	}

	/**
	 * @return the {@link #sonarNativeProxy}
	 */
	public SonarNativeProxy getSonarNativeProxy() {
		return sonarNativeProxy;
	}

}
