/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.wc;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.Objects;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.io.xsf.info.XsfInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.XsfWCPreferences;
import fr.ifremer.globe.core.model.wc.filters.BottomFilterParameter.ToleranceType;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.sonarnative.SonarNative;
import fr.ifremer.sonarnative.SonarNativeEchoBuffers;
import fr.ifremer.sonarnative.SonarNativeException;
import fr.ifremer.sonarnative.jni.AcrossDistanceParameter;
import fr.ifremer.sonarnative.jni.BeamIndexParameter;
import fr.ifremer.sonarnative.jni.BottomFilterParameter;
import fr.ifremer.sonarnative.jni.DepthParameter;
import fr.ifremer.sonarnative.jni.MultipingParameter;
import fr.ifremer.sonarnative.jni.SampleIndexParameter;
import fr.ifremer.sonarnative.jni.RangeNormalizationParameter;
import fr.ifremer.sonarnative.jni.ResonOffsetParameter;
import fr.ifremer.sonarnative.jni.SamplingParameter;
import fr.ifremer.sonarnative.jni.SideLobeParameter;
import fr.ifremer.sonarnative.jni.SpatializationMethod;
import fr.ifremer.sonarnative.jni.SpatializerHolder;
import fr.ifremer.sonarnative.jni.SpecularFilterParameter;
import fr.ifremer.sonarnative.jni.ThresholdParameter;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters;

/**
 * Proxy of fr.ifremer.sonarnative library for spatializing samples of WC. <br>
 * Produce a NativeBeamSample of each ping on demand. <br>
 * This class is theads safe.
 */
public class SonarNativeProxy {

	private static final Logger logger = LoggerFactory.getLogger(SonarNativeProxy.class);

	private final ISounderNcInfo ncInfo;
	private SpatializerHolder spatializerHolder;
	private SonarNativeEchoBuffers echoBuffers;
	private NativeBeamSample lastNativeBeamSample;
	private FilterParameters lastFilterParameters;
	private VolumicRendererDisplayParameters lastDisplayParameters;

	/** Number of the Sonar/Beam_group to require */
	private int beamGroupNumber = 1;

	XsfWCPreferences displayPreferences = Activator.getXsfWCPreferences();

	/**
	 * Constructor
	 */
	public SonarNativeProxy(ISounderNcInfo ncInfo) {
		this.ncInfo = ncInfo;
	}

	/**
	 * Destructor
	 */
	public void dispose() {
		if (spatializerHolder != null) {
			SonarNative.closeSpatializer(spatializerHolder);
			spatializerHolder = null;
		}
	}

	/**
	 * Requires the loading of a ping. As soon as data are available, the consumer is invoked
	 */
	public synchronized void loadPing(int pingIndex, FilterParameters filterParameters,
			VolumicRendererDisplayParameters displayParameters, Consumer<NativeBeamSample> sampleConsumer)
			throws GIOException {
		try {
			if (spatializerHolder == null) {
				int ncid = -1;
				if (ncInfo instanceof XsfInfo xsfInfo) {
					ncid = xsfInfo.getFile().getGroupId();
				} else if (ncInfo instanceof SonarNcInfo info) {
					ncid = info.getFile().getGroupId();
				}
				spatializerHolder = SonarNative.openSpatializer(ncInfo.getAbsolutePath(), ncid);
				if (spatializerHolder == null || !spatializerHolder.getLastErrorMsg().isEmpty()) {
					throw new GIOException(ncInfo.getFilename() + " not suitable for Sonar Native library");
				}
			}

			// Check if the required ping is not the same than the previous one
			if (loadNeeded(pingIndex, filterParameters, displayParameters)) {
				// Filtering changes ?
				if (!Objects.equals(lastFilterParameters, filterParameters))
					applyFilterParameters(filterParameters);

				// Max number of echos in this ping
				SonarNative.changeBeamGroupNumber(spatializerHolder, beamGroupNumber);
				if (spatializerHolder == null || !spatializerHolder.getLastErrorMsg().isEmpty()) {
					throw new GIOException(ncInfo.getFilename() + " not suitable for Sonar Native library");
				}
				int echoCount = SonarNative.estimateBeamEchoCount(spatializerHolder, pingIndex, 1);

				// Need to create a new buffer ?
				if (echoCount > 0) {
					if (echoBuffers == null || echoBuffers.latitudes.capacity() < echoCount) {
						echoBuffers = new SonarNativeEchoBuffers(echoCount);
					}
					if (filterParameters.spatializationMethod == null) {
						SonarNative.setSpatializationMethod(spatializerHolder,
								SpatializationMethod.DetectionInterpolation);
					} else {
						switch (filterParameters.spatializationMethod) {
						case ConstantCelerity:
							SonarNative.setSpatializationMethod(spatializerHolder,
									SpatializationMethod.ConstantCelerity);
							break;
						case EquivalentCelerity:
							SonarNative.setSpatializationMethod(spatializerHolder,
									SpatializationMethod.EquivalentCelerity);
							break;
						case DetectionInterpolation:
						default:
							SonarNative.setSpatializationMethod(spatializerHolder,
									SpatializationMethod.DetectionInterpolation);
							break;
						}
					}
					SonarNative.applySignalProcessing(spatializerHolder,
							new ResonOffsetParameter((float) displayPreferences.getResonGlobalOffset()));
					SonarNative.applySignalProcessing(spatializerHolder,
							new RangeNormalizationParameter(displayParameters.isNormalizationEnabled(), displayParameters.getNormalizationOffset()));
					lastDisplayParameters = displayParameters;

					SonarNative.spatialize(spatializerHolder, pingIndex, 1, echoBuffers);
					logger.debug("Ping {} loaded. Containing {} echoes", pingIndex, echoBuffers.getEchoCount());

					lastNativeBeamSample = new NativeBeamSample(pingIndex, echoBuffers.getEchoCount(),
							echoBuffers.longitudes, echoBuffers.latitudes, echoBuffers.acrosses,
							echoBuffers.beamOpeningAcrosses, echoBuffers.beamOpeningAlongs, echoBuffers.elevations,
							echoBuffers.heights, echoBuffers.echoes, echoBuffers.nonOverlappingBeamOpeningAngles);
				} else {
					lastNativeBeamSample = new NativeBeamSample(pingIndex, 0, null, null, null, null, null, null, null,
							null, null);
				}
			}
		} catch (SonarNativeException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}

		sampleConsumer.accept(lastNativeBeamSample);
	}

	private boolean loadNeeded(int pingIndex, FilterParameters filterParameters,
			VolumicRendererDisplayParameters displayParameters) {
		return (lastNativeBeamSample == null || //
				lastNativeBeamSample.pingIndex != pingIndex || //
				!Objects.equals(lastFilterParameters, filterParameters) || //
				lastDisplayParameters.reloadNeeded(displayParameters));
	}

	/** Applying the filtering */
	private void applyFilterParameters(FilterParameters filterParameters) throws SonarNativeException {
		boolean enabled = filterParameters != null && filterParameters.enabled ;
		// Sampling
		var samplingParameter = enabled && filterParameters.sampling.getSampling() > 1 //
				? new SamplingParameter(filterParameters.sampling.getSampling())//
				: new SamplingParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, samplingParameter);

		// Multiping sequence
		var multipingParameter = enabled && filterParameters.multiping.isEnable() //
				? new MultipingParameter(true, filterParameters.multiping.getMultipingIndex())//
				: new MultipingParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, multipingParameter);

		// Threshold
		var thresholdParameter = enabled && filterParameters.threshold.isEnable() //
				? new ThresholdParameter(true, filterParameters.threshold.getMinValue(),
						filterParameters.threshold.getMaxValue())//
				: new ThresholdParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, thresholdParameter);

		// Specular
		var specularParameter = enabled && filterParameters.specular.isEnable() //
				? new SpecularFilterParameter(true, filterParameters.specular.getFilterBelow(),
						filterParameters.specular.getTolerance())//
				: new SpecularFilterParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, specularParameter);

		// Beam
		var beamIndexParameter = enabled && filterParameters.beam.isEnable() //
				? new BeamIndexParameter(true, filterParameters.beam.getMinValue(), filterParameters.beam.getMaxValue())//
				: new BeamIndexParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, beamIndexParameter);

		// Range
		var sampleIndexParameter = enabled && filterParameters.sample.isEnable() //
				? new SampleIndexParameter(true, filterParameters.sample.getMinValue(), filterParameters.sample.getMaxValue())//
				: new SampleIndexParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, sampleIndexParameter);

		// Depth
		var depthParameter = enabled && filterParameters.depth.isEnable() //
				? new DepthParameter(true, filterParameters.depth.getMinValue(), filterParameters.depth.getMaxValue())//
				: new DepthParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, depthParameter);

		// Across
		var acrossDistanceParameter = enabled && filterParameters.acrossDistance.isEnable() //
				? new AcrossDistanceParameter(true, filterParameters.acrossDistance.getMinValue(),
						filterParameters.acrossDistance.getMaxValue())//
				: new AcrossDistanceParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, acrossDistanceParameter);

		// Side lobe
		var sideLobeParameter = enabled && filterParameters.sidelobe.isEnable() //
				? new SideLobeParameter(true, filterParameters.sidelobe.getThreshold())//
				: new SideLobeParameter(); // Cancel filtering
		SonarNative.applyFilter(spatializerHolder, sideLobeParameter);

		// Bottom filter
		var bottomParameter = new BottomFilterParameter();
		if (enabled && filterParameters.bottom.isEnable()) {
			bottomParameter = filterParameters.bottom.getType() == ToleranceType.SAMPLE//
					? BottomFilterParameter.new_sample(filterParameters.bottom.getAngleCoefficient(),
							filterParameters.bottom.getToleranceAbsolute()) //
					: BottomFilterParameter.new_range_percent(filterParameters.bottom.getAngleCoefficient(),
							filterParameters.bottom.getTolerancePercent());
		}
		SonarNative.applyFilter(spatializerHolder, bottomParameter);

		lastFilterParameters = filterParameters;
	}

	/** Change the Sonar_Beamgroup */
	public void switchToBeamGroupNumber(int beamGroupNumber) {
		if (this.beamGroupNumber != beamGroupNumber) {
			this.beamGroupNumber = beamGroupNumber;
			// Force next generation
			lastNativeBeamSample = null;
		}
	}
	
	/** Estimate normalization ref level 
	 * @throws GIOException */
	public float estimateNormalizationRefLevel(int pingIndex) throws GIOException {
		try {
			return SonarNative.estimateNormalizationRefLevel(spatializerHolder, pingIndex, 1);
		} catch (SonarNativeException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}
	}

	

	/** Data loaded for a ping by SonarNative */
	public static class NativeBeamSample {
		public final int pingIndex;
		public final int sampleCount;

		public final DoubleBuffer longitudes;
		public final DoubleBuffer latitudes;
		public final FloatBuffer acrosses;
		public final FloatBuffer beamOpeningAcrosses;
		public final FloatBuffer beamOpeningAlongs;
		public final FloatBuffer elevations;
		public final FloatBuffer heights;
		public final FloatBuffer echoes;
		public final FloatBuffer nonOverlappingBeamOpeningAngles;

		/**
		 * Constructor
		 */
		public NativeBeamSample(int pingIndex, int sampleCount, DoubleBuffer longitudes, DoubleBuffer latitudes,
				FloatBuffer acrosses, FloatBuffer beamOpeningAcrosses, FloatBuffer beamOpeningAlongs,
				FloatBuffer elevations, FloatBuffer heights, FloatBuffer echoes,
				FloatBuffer nonOverlappingBeamOpeningAngles) {
			this.pingIndex = pingIndex;
			this.sampleCount = sampleCount;
			this.longitudes = longitudes;
			this.latitudes = latitudes;
			this.acrosses = acrosses;
			this.beamOpeningAcrosses = beamOpeningAcrosses;
			this.beamOpeningAlongs = beamOpeningAlongs;
			this.elevations = elevations;
			this.heights = heights;
			this.echoes = echoes;
			this.nonOverlappingBeamOpeningAngles = nonOverlappingBeamOpeningAngles;
		}

	}

}
