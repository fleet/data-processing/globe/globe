package fr.ifremer.viewer3d.layers.wc.render;

import javax.swing.SwingUtilities;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;

import fr.ifremer.globe.core.model.wc.Manageable;
import fr.ifremer.globe.ogl.util.GLUtils;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.geom.Vec4;

/**
 * a class containing WC samples for a single Ping
 */
public abstract class SinglePingSamples implements Manageable {

	protected int pingIndex;
	protected int sampleCount = 0;

	/** Beams data buffer */
	protected VBO vertexVBO;
	protected VBO amplitudeVBO;
	protected VBO beamWidthMajorVBO;
	protected VBO echoHeight;
	protected VBO beamWidthMinorVBO;

	protected Vec4 alongVector;
	protected Vec4 acrossVector;
	protected Vec4 topPoint;

	private GLContext currentGL;

	/**
	 * Constructor
	 */
	public SinglePingSamples(int pingIndex) {
		this.pingIndex = pingIndex;
	}

	@Override
	public void close() {
		SwingUtilities.invokeLater(() -> {
			// System.out.println("Dispose " + sampleCount + " elements for ping " + pingIndex);
			// release buffers
			if (currentGL == null || currentGL.makeCurrent() == GLContext.CONTEXT_NOT_CURRENT)
				return;
			if (amplitudeVBO != null) {
				amplitudeVBO.delete(currentGL.getGL());
				amplitudeVBO = null;
			}
			if (beamWidthMajorVBO != null) {
				beamWidthMajorVBO.delete(currentGL.getGL());
				beamWidthMajorVBO = null;
			}
			if (beamWidthMinorVBO != null) {
				beamWidthMinorVBO.delete(currentGL.getGL());
				beamWidthMinorVBO = null;
			}
			if (echoHeight != null) {
				echoHeight.delete(currentGL.getGL());
				echoHeight = null;
			}
			if (vertexVBO != null) {
				vertexVBO.delete(currentGL.getGL());
				vertexVBO = null;
			}
			currentGL = null;
		});
	}

	protected int getPingIndex() {
		return pingIndex;
	}
	
	public float getMinAmplitude() {
		float min = Float.POSITIVE_INFINITY;
		VBO buffer = getAmplitudeBuffer();
		buffer.rewind();
		for(int i = 0; i < buffer.getElementCount(); i++) {
			min = Math.min(min, buffer.get());
		}
		return min;
	}
	
	public float getMaxAmplitude() {
		float max = Float.NEGATIVE_INFINITY;
		VBO buffer = getAmplitudeBuffer();
		buffer.rewind();
		for(int i = 0; i < buffer.getElementCount(); i++) {
			max = Math.max(max, buffer.get());
		}
		return max;
	}
	/**
	 * Draws the samples
	 */
	void draw(GL2 gl, VolumicRendererDisplayParameters parameters, VolumicAttribLocation attributes,
			VolumicUniformLocations uniforms) {

		if (sampleCount == 0) {
			return;
		}
		GLUtils.checkGLError(gl);
		// Setting buffer
		gl.glUniform4f(uniforms.alongVector, (float) alongVector.getX(), (float) alongVector.getY(),
				(float) alongVector.getZ(), 1);

		gl.glUniform4f(uniforms.acrossVector, (float) acrossVector.getX(), (float) acrossVector.getY(),
				(float) acrossVector.getZ(), 1);

		gl.glUniform4f(uniforms.topPoint, (float) topPoint.getX(), (float) topPoint.getY(), (float) topPoint.getZ(), 1);

		// Setting amp buffer
		// bind amplitude buffer
		if (attributes.getAmplitudeLocationEnabled()) {
			if (getAmplitudeBuffer().getBufferName() == null)
				getAmplitudeBuffer().bindAndLoad(gl);
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, getAmplitudeBuffer().getBufferName()[0]);
			gl.glVertexAttribPointer(attributes.amplitudeLocation, getAmplitudeBuffer().getElementSize(), GL.GL_FLOAT,
					false, 0, 0);
		}
		// bind buffer
		if (attributes.getBeamWidthAcrossEnabled()) {
			if (getBeamWidthAcrossBuffer().getBufferName() == null)
				getBeamWidthAcrossBuffer().bindAndLoad(gl);
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, getBeamWidthAcrossBuffer().getBufferName()[0]);
			gl.glVertexAttribPointer(attributes.beamWidthAcross, getBeamWidthAcrossBuffer().getElementSize(),
					GL.GL_FLOAT, false, 0, 0);
		}
		if (attributes.getBeamWidthAlongEnabled()) {
			// bind buffer
			if (getBeamWidthAlongBuffer().getBufferName() == null)
				getBeamWidthAlongBuffer().bindAndLoad(gl);
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, getBeamWidthAlongBuffer().getBufferName()[0]);
			gl.glVertexAttribPointer(attributes.beamWidthAlong, getBeamWidthAlongBuffer().getElementSize(), GL.GL_FLOAT,
					false, 0, 0);
		}

		if (attributes.getHeightEnabled()) {
			// bind buffer
			if (getEchoHeightBuffer().getBufferName() == null)
				getEchoHeightBuffer().bindAndLoad(gl);
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, getEchoHeightBuffer().getBufferName()[0]);
			gl.glVertexAttribPointer(attributes.height, getEchoHeightBuffer().getElementSize(), GL.GL_FLOAT, false, 0,
					0);
		}
		// Setting vertex buffer
		// Set the vertex pointer to the vertex buffer, we do not use
		if (getVertexBuffer().getBufferName() == null) {
			getVertexBuffer().bindAndLoad(gl);
			this.currentGL = gl.getContext();

		}

		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, getVertexBuffer().getBufferName()[0]);
		gl.glVertexPointer(getVertexBuffer().getElementSize(), GL.GL_FLOAT, 0, 0);

		// Rendering
		gl.glDrawArrays(GL.GL_POINTS, 0, getVertexBuffer().getElementCount());
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

		GLUtils.checkGLError(gl);
	}

	void render_pick(GL2 gl, VolumicRendererDisplayParameters parameters, VolumicAttribLocation attributes,
			VolumicUniformLocations uniforms) {
		draw(gl, parameters, attributes, uniforms);
	}

	/**
	 * get or allocate the vertex buffer associated with this set of samples
	 */
	protected VBO getVertexBuffer() {
		if (vertexVBO == null) {
			vertexVBO = new VBO(3);
			vertexVBO.allocateBuffer(sampleCount);
		}
		return vertexVBO;
	}

	/**
	 * get or allocate the vertex buffer associated with this set of samples
	 */
	protected VBO getAmplitudeBuffer() {
		if (amplitudeVBO == null) {
			amplitudeVBO = new VBO(1);
			amplitudeVBO.allocateBuffer(sampleCount);
		}
		return amplitudeVBO;
	}

	/**
	 * get or allocate the vertex buffer associated with this set of samples
	 */
	protected VBO getBeamWidthAcrossBuffer() {
		if (beamWidthMajorVBO == null) {
			beamWidthMajorVBO = new VBO(1);
			beamWidthMajorVBO.allocateBuffer(sampleCount);
		}
		return beamWidthMajorVBO;
	}

	/**
	 * get or allocate the vertex buffer associated with this set of samples
	 */
	protected VBO getBeamWidthAlongBuffer() {
		if (beamWidthMinorVBO == null) {
			beamWidthMinorVBO = new VBO(1);
			beamWidthMinorVBO.allocateBuffer(sampleCount);
		}
		return beamWidthMinorVBO;
	}

	/**
	 * get or allocate the vertex buffer associated with this set of samples
	 */
	protected VBO getEchoHeightBuffer() {
		if (echoHeight == null) {
			echoHeight = new VBO(1);
			echoHeight.allocateBuffer(sampleCount);
		}
		return echoHeight;
	}

	public boolean isEmpty() {
		return sampleCount == 0;
	}
}
