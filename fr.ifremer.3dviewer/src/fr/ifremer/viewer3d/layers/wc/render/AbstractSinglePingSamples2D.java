package fr.ifremer.viewer3d.layers.wc.render;

import fr.ifremer.globe.core.model.wc.SwathSpatialData;

/**
 * Abstract container of WC samples for a single Ping, for the 2d viewer
 */
public abstract class AbstractSinglePingSamples2D extends SinglePingSamples {

	// stats
	protected double topElevation;
	protected double bottomElevation;
	protected double maxAccrossDistance;
	protected double minAccrossDistance;
	protected double[] leftLatLon;
	protected double[] rightLatLon;

	protected SwathSpatialData swathSpatialData;

	/**
	 * Constructor
	 */
	protected AbstractSinglePingSamples2D(int pingIndex) {
		super(pingIndex);
	}

	public SwathSpatialData getSwathSpatialData() {
		return swathSpatialData;
	}

	//// STATS GETTERS

	public double getTopElevation() {
		return topElevation;
	}

	public double getBottomElevation() {
		return bottomElevation;
	}

	public double getMaxAccrossDistance() {
		return maxAccrossDistance;
	}

	public double getMinAccrossDistance() {
		return minAccrossDistance;
	}

	public double[] getLeftLatLon() {
		return leftLatLon;
	}

	public double[] getRightLatLon() {
		return rightLatLon;
	}

}
