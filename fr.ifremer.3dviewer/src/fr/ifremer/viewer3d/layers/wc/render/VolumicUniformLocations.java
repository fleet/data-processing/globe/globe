package fr.ifremer.viewer3d.layers.wc.render;

import com.jogamp.opengl.GL2;

public class VolumicUniformLocations {

	public final int minConstrast;
	public final int maxContrast;
	public final int opacity;
	public final int acrossVector;
	public final int alongVector;
	public final int palette;
	public final int topPoint;
	public final int resolution;
	public final int minValue;
	
	public VolumicUniformLocations(GL2 gl, int shaderProgram) {
		opacity = gl.glGetUniformLocation(shaderProgram, "opacity");
		minConstrast = gl.glGetUniformLocation(shaderProgram, "minContrast");
		maxContrast = gl.glGetUniformLocation(shaderProgram, "maxContrast");
		alongVector = gl.glGetUniformLocation(shaderProgram, "alongVector");
		acrossVector = gl.glGetUniformLocation(shaderProgram, "acrossVector");
		palette = gl.glGetUniformLocation(shaderProgram, "palette");
		topPoint = gl.glGetUniformLocation(shaderProgram, "topPoint");
		resolution = gl.glGetUniformLocation(shaderProgram, "resolution");
		minValue = gl.glGetUniformLocation(shaderProgram, "minValue");
	}
}
