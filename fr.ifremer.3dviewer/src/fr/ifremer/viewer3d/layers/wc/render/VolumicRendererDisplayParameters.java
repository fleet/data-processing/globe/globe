package fr.ifremer.viewer3d.layers.wc.render;

import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.viewer3d.Activator;

/**
 * Display parameters associated with volumic rendering
 */
public class VolumicRendererDisplayParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<VolumicRendererDisplayParameters> SESSION_KEY = new TypedKey<>(
			"Volumic_Renderer_Display_Parameters");

	public enum Rendering {
		Point,
		Square
	}

	/**
	 * Rendering mode
	 */
	private final Rendering rendering;

	/**
	 * current colorMapIndex
	 */
	private final int colorMapIndex;

	/**
	 * invert ColorMap
	 */
	private final boolean invertColor;

	/**
	 * min constrast value
	 */
	private final float contrastMin;
	/**
	 * max contrast value
	 */
	private final float contrastMax;
	/**
	 * Opacity
	 */
	private final double opacity;

	/** Vertical offset */
	private final boolean verticalOffsetEnabled;
	private final float verticalOffset;

	/**
	 * Use compensated display Apply normalization by range
	 */
	private final boolean normalizationEnabled;
	private final float normalizationOffset;

	/**
	 * Constructor
	 */
	public VolumicRendererDisplayParameters(int colorMapIndex, boolean invertColor, float contrastMin,
			float contrastMax, Rendering rendering, double opacity, boolean verticalOffsetEnabled, float verticalOffset,
			boolean normalizationEnabled, float normalizationOffset) {
		this.colorMapIndex = colorMapIndex;
		this.invertColor = invertColor;
		this.contrastMin = contrastMin;
		this.contrastMax = contrastMax;
		this.rendering = rendering;
		this.opacity = opacity;
		this.verticalOffsetEnabled = verticalOffsetEnabled;
		this.verticalOffset = verticalOffset;
		this.normalizationEnabled = normalizationEnabled;
		this.normalizationOffset = normalizationOffset;
	}

	/**
	 * build a default value
	 */
	public static VolumicRendererDisplayParameters getDefault() {
		String paletteName = Activator.getPalettesPreference().getWcPalette().getValue();
		return new VolumicRendererDisplayParameters(ColorMap.getColorMapIntIndex(paletteName), false, -35, 50,
				Rendering.Square, 1f, false, 0f, false, 0f);
	}

	/**
	 * build an other instance with the specified rendering
	 */
	public VolumicRendererDisplayParameters withRendering(Rendering rendering) {
		return new VolumicRendererDisplayParameters(colorMapIndex, invertColor, contrastMin, contrastMax, rendering,
				opacity, verticalOffsetEnabled, verticalOffset, normalizationEnabled, normalizationOffset);
	}

	/**
	 * build an other instance with the specified color model
	 */
	public VolumicRendererDisplayParameters withColorModel(int colorMapIndex, boolean invertColor, float contrastMin,
			float contrastMax, double opacity) {
		return new VolumicRendererDisplayParameters(colorMapIndex, invertColor, contrastMin, contrastMax, rendering,
				opacity, verticalOffsetEnabled, verticalOffset, normalizationEnabled, normalizationOffset);
	}

	/**
	 * build an other instance with the specified vertical offset
	 */
	public VolumicRendererDisplayParameters withVerticalOffset(boolean verticalOffsetEnabled, float verticalOffset) {
		return new VolumicRendererDisplayParameters(colorMapIndex, invertColor, contrastMin, contrastMax, rendering,
				opacity, verticalOffsetEnabled, verticalOffset, normalizationEnabled, normalizationOffset);
	}

	/**
	 * build an other instance with the specified compensated parameter
	 */
	public VolumicRendererDisplayParameters withNormalization(boolean normalizationEnabled, float normalizationOffset) {
		return new VolumicRendererDisplayParameters(colorMapIndex, invertColor, contrastMin, contrastMax, rendering,
				opacity, verticalOffsetEnabled, verticalOffset, normalizationEnabled, normalizationOffset);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + colorMapIndex;
		result = prime * result + Float.floatToIntBits(contrastMax);
		result = prime * result + Float.floatToIntBits(contrastMin);
		result = prime * result + Boolean.hashCode(invertColor);
		result = prime * result + (rendering == null ? 0 : rendering.hashCode());
		result = prime * result + (verticalOffsetEnabled ? Float.floatToIntBits(verticalOffset) : 0);
		result = prime * result + Boolean.hashCode(normalizationEnabled);
		result = prime * result + (normalizationEnabled ? Float.floatToIntBits(normalizationOffset) : 0);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VolumicRendererDisplayParameters other = (VolumicRendererDisplayParameters) obj;
		if (colorMapIndex != other.colorMapIndex) {
			return false;
		}
		if (Float.floatToIntBits(contrastMax) != Float.floatToIntBits(other.contrastMax)) {
			return false;
		}
		if (Float.floatToIntBits(contrastMin) != Float.floatToIntBits(other.contrastMin)) {
			return false;
		}
		if (invertColor != other.invertColor) {
			return false;
		}
		if (rendering != other.rendering) {
			return false;
		}
		if (verticalOffsetEnabled != other.verticalOffsetEnabled) {
			return false;
		}
		if (verticalOffset != other.verticalOffset) {
			return false;
		}
		if (normalizationEnabled != other.normalizationEnabled) {
			return false;
		}
		if (normalizationOffset != other.normalizationOffset) {
			return false;
		}		
		return true;
	}

	@Override
	public String toString() {
		return "VolumicRendererDisplayParameters [colorMapIndex=" + colorMapIndex + ", invertColor=" + invertColor
				+ ", contrastMin=" + contrastMin + ", contrastMax=" + contrastMax + ", rendering=" + rendering
				+ ", normalized=" + normalizationEnabled + "]";
	}

	/*
	 * @return true if updating parameters with other needs to reload VolumicLayer
	 */
	public boolean reloadNeeded(VolumicRendererDisplayParameters other) {
		return (isNormalizationEnabled() != other.isNormalizationEnabled() || //
				getNormalizationOffset() != other.getNormalizationOffset() || //
				isVerticalOffsetEnabled() != other.isVerticalOffsetEnabled() || //
				getVerticalOffset() != other.getVerticalOffset());
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<VolumicRendererDisplayParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

	/**
	 * @return the {@link #rendering}
	 */
	public Rendering getRendering() {
		return rendering;
	}

	/**
	 * @return the {@link #colorMapIndex}
	 */
	public int getColorMapIndex() {
		return colorMapIndex;
	}

	/**
	 * @return the {@link #invertColor}
	 */
	public boolean isInvertColor() {
		return invertColor;
	}

	/**
	 * @return the {@link #contrastMin}
	 */
	public float getContrastMin() {
		return contrastMin;
	}

	/**
	 * @return the {@link #contrastMax}
	 */
	public float getContrastMax() {
		return contrastMax;
	}

	/**
	 * @return the {@link #opacity}
	 */
	public double getOpacity() {
		return opacity;
	}

	/**
	 * @return the {@link #verticalOffsetEnabled}
	 */
	public boolean isVerticalOffsetEnabled() {
		return verticalOffsetEnabled;
	}

	/**
	 * @return the {@link #verticalOffset}
	 */
	public float getVerticalOffset() {
		return verticalOffset;
	}

	/**
	 * @return the {@link #normalizationEnabled}
	 */
	public boolean isNormalizationEnabled() {
		return normalizationEnabled;
	}
	
	/**
	 * @return the {@link #normalizationOffset}
	 */
	public float getNormalizationOffset() {
		return normalizationOffset;
	}
}
