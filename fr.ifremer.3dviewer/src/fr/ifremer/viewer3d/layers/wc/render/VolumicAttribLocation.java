package fr.ifremer.viewer3d.layers.wc.render;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;

import fr.ifremer.globe.ogl.util.GLUtils;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters.Rendering;

public class VolumicAttribLocation {

	public final int amplitudeLocation;
	public final int beamWidthAcross;
	public final int beamWidthAlong;
	public final int height;

	public VolumicAttribLocation(GL2 gl, int shaderProgram, Rendering rendering) {
		amplitudeLocation = gl.glGetAttribLocation(shaderProgram, "amplitude");
		switch (rendering) {
		case Square:
			beamWidthAcross = gl.glGetAttribLocation(shaderProgram, "beamWidthAcross");
			beamWidthAlong = gl.glGetAttribLocation(shaderProgram, "beamWidthAlong");
			height = gl.glGetAttribLocation(shaderProgram, "height");
			break;
		case Point:
		default:
			beamWidthAcross = -1;
			beamWidthAlong = -1;
			height = -1;
			break;
		}

	}

	public void enable(GL2 gl) {
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		if (amplitudeLocation != -1) {
			gl.glEnableVertexAttribArray(amplitudeLocation);
		}
		if (beamWidthAcross != -1) {
			gl.glEnableVertexAttribArray(beamWidthAcross);
		}
		if (beamWidthAlong != -1) {
			gl.glEnableVertexAttribArray(beamWidthAlong);
		}
		if (height != -1) {
			gl.glEnableVertexAttribArray(height);
		}
		GLUtils.checkGLError(gl);

	}

	public void disable(GL2 gl) {

		if (amplitudeLocation != -1) {
			gl.glDisableVertexAttribArray(amplitudeLocation);
		}

		if (beamWidthAcross != -1) {
			gl.glDisableVertexAttribArray(beamWidthAcross);
		}
		if (beamWidthAlong != -1) {
			gl.glDisableVertexAttribArray(beamWidthAlong);
		}
		if (height != -1) {
			gl.glDisableVertexAttribArray(height);
		}
		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);

		GLUtils.checkGLError(gl);
	}

	public boolean getAmplitudeLocationEnabled() {
		return amplitudeLocation != -1;
	}

	public boolean getBeamWidthAcrossEnabled() {
		return beamWidthAcross != -1;
	}

	public boolean getBeamWidthAlongEnabled() {
		return beamWidthAlong != -1;
	}

	public boolean getHeightEnabled() {
		return height != -1;
	}
}
