package fr.ifremer.viewer3d.layers.wc.render;

import java.time.Instant;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.viewer3d.layers.wc.IWCLayer;
import gov.nasa.worldwind.geom.Position;

/**
 * Class containing a picked object result on watercolumn.
 */
public interface IWCPickedObject {

	/**
	 * @return the WC layer associated with the picked object.
	 */
	IWCLayer getLayer();

	/***
	 * @return the ping identifier for the picked object.
	 */
	public int getId();

	/**
	 * @return the picked object's position.
	 */
	public Position getPosition();

	/**
	 * @return the picked object's value.
	 */
	public float getValue();

	/**
	 * @return date of the picked object (optional).
	 */
	public Optional<Instant> getDate();

	/**
	 * @return picked object's metadata.
	 */
	public default Map<String, String> getMetadata() {
		return Collections.emptyMap();
	}

	/**
	 * @return the tooltip associated with the picked object.
	 */
	public default Optional<String> getTooltip() {
		// ignore NaN values
		if (Float.isNaN(getValue()))
			return Optional.empty();

		StringBuilder sb = new StringBuilder(getLayer().getName() + System.lineSeparator());

		// id
		String id = getMetadata().containsKey(IWWTextureData.NAME_METADATA_KEY)
				? getMetadata().get(IWWTextureData.NAME_METADATA_KEY)
				: "Id : " + getId();
		sb.append(id + System.lineSeparator());

		// value
		String units = getMetadata().containsKey(IWWTextureData.UNITS_METADATA_KEY)
				? getMetadata().get(IWWTextureData.UNITS_METADATA_KEY)
				: "";
		sb.append(String.format(Locale.US, "Value : %.2f %s %n", getValue(), units));

		// position - elevation
		Optional.ofNullable(getPosition()).ifPresent(position -> {
			String lat = FormatLatitudeLongitude.latitudeToString(position.latitude.getDegrees());
			String lon = FormatLatitudeLongitude.longitudeToString(position.longitude.getDegrees());
			sb.append("Position : " + lat + " : " + lon + "\n");
			sb.append(String.format(Locale.US, "%s : %.2f m%n", IMarker.ELEVATION_LONG_NAME,
					getPosition().getElevation()));
		});

		// date
		getDate().ifPresent(date -> sb.append("Date : " + date.toString() + System.lineSeparator()));

		return Optional.of(sb.toString());
	}
}
