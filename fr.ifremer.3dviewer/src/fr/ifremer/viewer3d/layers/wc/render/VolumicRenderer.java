package fr.ifremer.viewer3d.layers.wc.render;

import java.awt.Color;
import java.awt.Point;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.time.Instant;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.texture.Texture;

import fr.ifremer.globe.core.model.wc.LatLonSample;
import fr.ifremer.globe.ogl.util.GLUtils;
import fr.ifremer.globe.ogl.util.PaletteTexture;
import fr.ifremer.globe.ogl.util.ShaderStore;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.wc.IWCLayer;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import swingintegration.example.Platform;

public class VolumicRenderer implements OrderedRenderable {

	private static final Logger LOGGER = LoggerFactory.getLogger(VolumicRenderer.class);

	private WCVolumicLayer parent;
	// Shader programs arguments
	/**
	 * Map which link all the shader programs with their ID. One shader program instance needed for each OGL context.
	 */
	private ShaderStore shaderProgramsVolumicRendering;
	private ShaderStore shaderProgramsPointRendering;
	private ShaderStore shaderProgramsVolumicRenderingPicking;
	private ShaderStore shaderProgramsPointRenderingPicking;

	protected static final String VERTEX_SHADER = "/shader/volumicWC.vert";
	protected static final String VERTEX_SHADER_120 = "/shader/volumicWC_120.vert";
	protected static final String VERTEX_SHADER_PICK = "/shader/volumicWCPicking.vert";
	protected static final String VERTEX_SHADER_PICK_120 = "/shader/volumicWCPicking_120.vert";
	protected static final String FRAGMENT_SHADER = "/shader/volumicWC.frag";
	protected static final String FRAGMENT_SHADER_120 = "/shader/volumicWC_120.frag";
	protected static final String FRAGMENT_SHADER_PICKING = "/shader/volumicWC_picking.frag";
	protected static final String FRAGMENT_SHADER_PICKING_120 = "/shader/volumicWC_picking_120.frag";
	protected static final String GEOMETRY_SHADER_VOLUMIC = "/shader/volumicWC.geom";
	protected static final String GEOMETRY_SHADER_POINT = "/shader/volumicWCPoints.geom";

	PaletteTexture palette = new PaletteTexture();

	// Argument about renderer selection
	/** {@link WaterColumnRenderer} selection support. */
	private PickSupport pickSupport;

	VolumicRendererDisplayParameters displayParameters;
	private boolean wireframe;

	public VolumicRenderer(WCVolumicLayer parent) {
		this.parent = parent;
		displayParameters = parent.getDisplayParameters();
		if (!Platform.isMac())

		{
			shaderProgramsVolumicRendering = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, VERTEX_SHADER, GEOMETRY_SHADER_VOLUMIC, FRAGMENT_SHADER));
			shaderProgramsPointRendering = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, VERTEX_SHADER, GEOMETRY_SHADER_POINT, FRAGMENT_SHADER));
			shaderProgramsVolumicRenderingPicking = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, VERTEX_SHADER, GEOMETRY_SHADER_VOLUMIC, FRAGMENT_SHADER_PICKING));
			shaderProgramsPointRenderingPicking = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, VERTEX_SHADER_PICK, GEOMETRY_SHADER_POINT, FRAGMENT_SHADER));
		} else {
			shaderProgramsPointRendering = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, VERTEX_SHADER_120, FRAGMENT_SHADER_120));
			shaderProgramsVolumicRendering = shaderProgramsPointRendering;

			shaderProgramsVolumicRenderingPicking = shaderProgramsPointRendering;
			shaderProgramsPointRenderingPicking = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, VERTEX_SHADER_PICK_120, FRAGMENT_SHADER_PICKING_120));
		}

		pickSupport = new PickSupport();

	}

	public void dispose() {
		shaderProgramsVolumicRendering.dispose();
		shaderProgramsPointRendering.dispose();
		shaderProgramsVolumicRenderingPicking.dispose();
		shaderProgramsPointRenderingPicking.dispose();
		palette.dispose();
	}

	@Override
	public void render(DrawContext dc) {
		try {
			// retrieve last parameters
			displayParameters = parent.getDisplayParameters();
			// Get the OpenGL environment context
			GL2 gl = dc.getGL().getGL2();
			preRender(dc);
			draw(gl, dc);
			GLUtils.checkGLError(gl).ifPresent(
					error -> LOGGER.debug("Error with {} render method: {} ", getClass().getSimpleName(), error));
			// Push the drawing matrix, draw the rendering and pop the drawing matrix
			// dc.getView().pushReferenceCenter(dc, mReference);
			// draw(gl, shaderProgram);
			// dc.getView().popReferenceCenter(dc);
			// Set OpenGL depth settings

			postRender(dc);
		} catch (Exception e) {
			// we will trace only the first time an exception is thrown
			e.printStackTrace();
		}
	}

	class VolumicRendererPickedObject implements IWCPickedObject {
		private IWCLayer layer;
		Instant date;
		int index = 0;
		float value = Float.NaN;
		Position position;

		VolumicRendererPickedObject(IWCLayer layer) {
			this.layer = layer;
		}

		@Override
		public IWCLayer getLayer() {
			return layer;
		}

		@Override
		public int getId() {
			return index;
		}

		@Override
		public Position getPosition() {
			return position;
		}

		@Override
		public float getValue() {
			return value;
		}

		@Override
		public Optional<Instant> getDate() {
			return Optional.ofNullable(date);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getEnclosingInstance().hashCode();
			result = prime * result + (date == null ? 0 : date.hashCode());
			result = prime * result + index;
			result = prime * result + (layer == null ? 0 : layer.hashCode());
			result = prime * result + (position == null ? 0 : position.hashCode());
			result = prime * result + Float.floatToIntBits(value);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VolumicRendererPickedObject other = (VolumicRendererPickedObject) obj;
			if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
				return false;
			if (date == null) {
				if (other.date != null)
					return false;
			} else if (!date.equals(other.date))
				return false;
			if (index != other.index)
				return false;
			if (layer == null) {
				if (other.layer != null)
					return false;
			} else if (!layer.equals(other.layer))
				return false;
			if (position == null) {
				if (other.position != null)
					return false;
			} else if (!position.equals(other.position))
				return false;
			return Float.floatToIntBits(value) == Float.floatToIntBits(other.value);
		}

		private VolumicRenderer getEnclosingInstance() {
			return VolumicRenderer.this;
		}
	}

	private void draw(GL2 gl, DrawContext dc) throws GIOException {
		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // Alpha test function and reference value
				| GL2.GL_ENABLE_BIT // Enable bits for the user-definable clipping planes
				| GL2.GL_CURRENT_BIT // Current RGBA color
				| GL.GL_DEPTH_BUFFER_BIT // Depth buffer test function
				| GL2.GL_TEXTURE_BIT // Enable bits for the four texture coordinates
				| GL2.GL_TRANSFORM_BIT // Coefficients of the six clipping planes
				| GL2.GL_POLYGON_BIT);

		GLUtils.checkGLError(gl).ifPresent(error -> LOGGER.debug("Error with {} draw method (init attributes1) : {} ",
				getClass().getSimpleName(), error));
		gl.glEnable(GL.GL_BLEND);
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		GLUtils.checkGLError(gl).ifPresent(error -> LOGGER.debug("Error with {} draw method (init attributes) : {} ",
				getClass().getSimpleName(), error));

		if (wireframe)
			gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_LINE);

		// Initialize the rendering if needed and get the ID of the shader program generated
		int shaderProgram = ShaderStore.DEFAULT_SHADER_PROGRAM;
		switch (displayParameters.getRendering()) {
		case Point:
			shaderProgram = shaderProgramsPointRendering.getShaderProgram(gl);
			break;
		case Square:
		default:
			shaderProgram = shaderProgramsVolumicRendering.getShaderProgram(gl);
			break;
		}
		gl.glUseProgram(shaderProgram);
		GLUtils.checkGLError(gl).ifPresent(error -> LOGGER
				.debug("Error with {} draw method (setup shader program) : {} ", getClass().getSimpleName(), error));

		VolumicUniformLocations uniforms = new VolumicUniformLocations(gl, shaderProgram);
		// get the position of the uniforms in the fragment program
		gl.glUniform1i(uniforms.palette, 0);
		GLUtils.checkGLError(gl).ifPresent(error -> LOGGER.debug("Error with {} draw method (glGenTexture) : {} ",
				getClass().getSimpleName(), error));

		// sending color map texture to the GPU
		Texture paletteTexture = palette.getColorPalette(gl, displayParameters.getColorMapIndex(),
				displayParameters.isInvertColor());
		gl.glActiveTexture(GL.GL_TEXTURE0);
		paletteTexture.bind(gl);
		// update uniform
		GLUtils.checkGLError(gl).ifPresent(error -> LOGGER
				.debug("Error with {} draw method (bind texture palette): {} ", getClass().getSimpleName(), error));

		// parameters opacity, contrast
		gl.glUniform1f(uniforms.opacity, (float) parent.getOpacity());
		if (uniforms.minConstrast != -1)
			gl.glUniform1f(uniforms.minConstrast, displayParameters.getContrastMin());
		if (uniforms.minConstrast != -1)
			gl.glUniform1f(uniforms.maxContrast, displayParameters.getContrastMax());

		VolumicAttribLocation attributes = new VolumicAttribLocation(gl, shaderProgram,
				displayParameters.getRendering());
		attributes.enable(gl);
		// retrieve previous point size
		FloatBuffer pre = ByteBuffer.allocateDirect(Float.BYTES).asFloatBuffer();
		gl.glGetFloatv(GL.GL_POINT_SIZE, pre);
		pre.rewind();
		float latPointSize = pre.get();
		// set point size for us
		gl.glPointSize(4);
		GLUtils.checkGLError(gl)
				.ifPresent(error -> LOGGER.debug("Error with {} draw method (opacity, contrast, point size...): {} ",
						getClass().getSimpleName(), error));

		// really draw
		for (AbstractSinglePingSamples3D ping : parent.getBuffer().values()) {
			ping.draw(displayParameters, dc, attributes, uniforms);
		}
		GLUtils.checkGLError(gl).ifPresent(
				error -> LOGGER.debug("Error with {} draw method (ping.draw): {} ", getClass().getSimpleName(), error));

		attributes.disable(gl); // disable vertex arrays
		// set previous point size
		if (latPointSize > 0 && latPointSize < 256) // ensure that value is more or less valid
			gl.glPointSize(latPointSize);

		gl.glActiveTexture(GL.GL_TEXTURE0);
		gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		gl.glDisable(GL.GL_BLEND);
		gl.glDisable(GL.GL_TEXTURE_2D);
		gl.glPopAttrib();
		GLUtils.checkGLError(gl).ifPresent(
				error -> LOGGER.debug("Error with {} draw method (end) : {} ", getClass().getSimpleName(), error));
	}

	/**
	 * Render the water column for picking
	 */
	private void renderPick(GL2 gl, DrawContext dc) throws GIOException {
		if (!dc.isPickingMode())
			throw new GIOException("Error picking rendering not in picking mode");

		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // Alpha test function and reference value
				| GL2.GL_ENABLE_BIT // Enable bits for the user-definable clipping planes
				| GL2.GL_CURRENT_BIT // Current RGBA color
				| GL.GL_DEPTH_BUFFER_BIT // Depth buffer test function
				| GL2.GL_TEXTURE_BIT // Enable bits for the four texture coordinates
				| GL2.GL_TRANSFORM_BIT // Coefficients of the six clipping planes
				| GL2.GL_POLYGON_BIT);
		//
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		// Initialize the rendering if needed and get the ID of the shader program generated
		int shaderProgram = ShaderStore.DEFAULT_SHADER_PROGRAM;
		switch (displayParameters.getRendering()) {
		case Point:
			shaderProgram = shaderProgramsPointRenderingPicking.getShaderProgram(gl);
			break;
		case Square:
		default:
			shaderProgram = shaderProgramsVolumicRenderingPicking.getShaderProgram(gl);
			break;
		}
		gl.glUseProgram(shaderProgram);

		VolumicUniformLocations uniforms = new VolumicUniformLocations(gl, shaderProgram);

		int pickColor = gl.glGetUniformLocation(shaderProgram, "picking_color");
		Color color = dc.getUniquePickColor();
		int colorCode = color.getRGB();

		pickSupport.addPickableObject(colorCode, new VolumicRendererPickedObject(parent));

		gl.glUniform4f(pickColor, color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, 1);
		// update uniform
		VolumicAttribLocation attributes = new VolumicAttribLocation(gl, shaderProgram,
				displayParameters.getRendering());
		attributes.enable(gl);
		// retrieve previous point size
		FloatBuffer pre = ByteBuffer.allocateDirect(Float.BYTES).asFloatBuffer();
		gl.glGetFloatv(GL.GL_POINT_SIZE, pre);
		pre.rewind();
		float latPointSize = pre.get();
		// set point size for us
		gl.glPointSize(4);
		// really draw
		for (AbstractSinglePingSamples3D ping : parent.getBuffer().values()) {
			ping.render_pick(displayParameters, dc, attributes, uniforms);
		}
		// Disable vertex arrays
		attributes.disable(gl);
		// set previous point size
		if (latPointSize > 0 && latPointSize < 256) // ensure that value is more or less valid
			gl.glPointSize(latPointSize);

		GLUtils.checkGLError(gl).ifPresent(
				error -> LOGGER.debug("Error with {} renderPick method : {} ", getClass().getSimpleName(), error));

		gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		gl.glDisable(GL.GL_BLEND);
		gl.glDisable(GL.GL_TEXTURE_2D);

		gl.glPopAttrib();
		gl.glUseProgram(0);
	}

	private void preRender(DrawContext dc) {
		// update display parameters if necessary
		// Get the Globe 3D wireframe mode
		wireframe = dc.getModel().isShowWireframeExterior() || dc.getModel().isShowWireframeInterior();
	}

	private void postRender(DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		// Go back to the main shader program
		gl.glUseProgram(ShaderStore.DEFAULT_SHADER_PROGRAM);
	}

	@Override
	public double getDistanceFromEye() {
		return 0;
	}

	@Override
	public void pick(DrawContext dc, Point pickPoint) {
		pickSupport.clearPickList();
		pickSupport.beginPicking(dc);
		try {
			// compute line intersection
			renderPick(dc.getGL().getGL2(), dc);
		} catch (GIOException | GLException e) {
			// do not trace exception while rendering
			LOGGER.debug("Error while rendering WC picking");

		} finally {
			pickSupport.endPicking(dc);

			PickedObject picked = pickSupport.resolvePick(dc, pickPoint, parent);
			if (picked != null) {
				// now we will parse all data to find the picking point
				Line ray = dc.getView().computeRayFromScreenPoint(pickPoint.getX(), pickPoint.getY());
				double bestDistance = Double.MAX_VALUE;
				Optional<LatLonSample> bestPoint = Optional.empty();
				for (AbstractSinglePingSamples3D ping : parent.getBuffer().values()) {
					PickedPointResult value = ping.find_nearest(ray, bestDistance);
					if (value.sample.isPresent()) {
						// we got a result
						bestDistance = value.bestDistanceRetained;
						bestPoint = value.sample;
					}
				}
				if (bestPoint.isPresent()) {
					LatLonSample s = bestPoint.get();
					picked.setPosition(new Position(LatLon.fromDegrees(s.latitude, s.longitude), s.elevation));
					VolumicRendererPickedObject obj = (VolumicRendererPickedObject) picked.getObject();
					obj.index = s.ping.pingIndex;
					obj.date = Instant.ofEpochMilli(DateUtils.nanoSecondToMilli(s.ping.pingTimeNs));
					obj.value = s.echo.value;
					obj.position = picked.getPosition();
				}
			}
		}
		GLUtils.checkGLError(dc.getGL().getGL2()).ifPresent(error -> LOGGER
				.debug("Error with {} pick method (init attributes2) : {} ", getClass().getSimpleName(), error));

	}

}
