package fr.ifremer.viewer3d.layers.wc.render.algo;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WaterColumnPointsComputer {

	private static Logger logger = LoggerFactory.getLogger(WaterColumnPointsComputer.class);

	private float[] a;
	private float[] b;
	private float[] c;
	private int slicesNumber;
	private int numberOfPoints;

	public WaterColumnPointsComputer(float[] a, float[] b, float[] c, int slicesNumber) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.slicesNumber = slicesNumber;
		this.numberOfPoints = slicesNumber + 3;	// If I slice once, I got one point per piece (2). I count also extremum points (2). So, I have to add 3 to the number of slices.
	}

	/**
	 * Computes coordinates of all points drawn above.
	 * @param a please refer to the triangle drawn above ([] = {x, y, z})
	 * @param b please refer to the triangle drawn above ([] = {x, y, z})
	 * @param c please refer to the triangle drawn above ([] = {x, y, z})
	 * @param slicesNumber number of horizontal slices inside the triangle
	 * 
	 *  <br/>
	 *  <pre>
	 *  C
	 *  |\
	 *  |.\
	 *  |--\
	 *  | . \
	 *  |----\
	 *  |  .  \
	 *  |------\
	 *  |   .   \
	 *  |________\
	 *  A        B
	 *  </pre>
	 *  
	 * @return all coordinates of calculated points
	 */
	public float[][] computePoints() {

		if(this.a.length != 3) {
			logger.debug("Wrong dimension for point A. End of function.");
			return null;
		}

		if(this.b.length != 3) {
			logger.debug("Wrong dimension for point B. End of function.");
			return null;
		}

		if(this.c.length != 3) {
			logger.debug("Wrong dimension for point C. End of function.");
			return null;
		}

		if(this.slicesNumber < 0) {
			logger.debug("slicesNumber have to be greater or equal to 0. End of function.");
			return null;
		}

		List<float[]> intersectionPointListAC = computeACIntersectionPoints();
		List<float[]> intersectionPointListBC = computeBCIntersectionPoints();
		float[][] usefulPoints = computeUsefullPoints(intersectionPointListAC, intersectionPointListBC);

		return usefulPoints;
	}

	private List<float[]> computeACIntersectionPoints() {	
		// Compute all intersection points between AC and slices lines
		List<float[]> intersectionPointListAC = new ArrayList<float[]>();	
		float intervallX;
		float intervallY;
		float intervallZ;

		if((this.c[0]-this.a[0])<=0) {
			intervallX = Math.abs((this.a[0]-this.c[0])/(this.slicesNumber + 1));
		} else {
			intervallX = Math.abs((this.c[0]-this.a[0])/(this.slicesNumber + 1));
		}
		if((this.c[1]-this.a[1])<=0) {
			intervallY = Math.abs((this.a[1]-this.c[1])/(this.slicesNumber + 1));			
		} else {
			intervallY = Math.abs((this.c[1]-this.a[1])/(this.slicesNumber + 1));			
		}
		if ((this.c[2]-this.a[2])<=0) {
			intervallZ = Math.abs((this.a[2]-this.c[2])/(this.slicesNumber + 1)); 
		} else {
			intervallZ = Math.abs((this.c[2]-this.a[2])/(this.slicesNumber + 1));			
		}

		intersectionPointListAC.add(this.a);

		for(int i=1; i<=this.slicesNumber; i++) {
			float[] intersectionPoint = new float[3];
			if(this.a[0]<=this.c[0]) {
				intersectionPoint[0] = this.a[0]+i*intervallX;
			} else {
				intersectionPoint[0] = this.a[0]-i*intervallX;
			}
			if(this.a[1]<=this.c[1]) {
				intersectionPoint[1] = this.a[1]+i*intervallY;
			} else {
				intersectionPoint[1] = this.a[1]-i*intervallY;
			}
			if(this.a[2]<=this.c[2]) {
				intersectionPoint[2] = this.a[2]+i*intervallZ;
			} else {
				intersectionPoint[2] = this.a[2]-i*intervallZ;
			}
			intersectionPointListAC.add(intersectionPoint);
		}

		intersectionPointListAC.add(this.c);

		/*logger.debug("Coordinates found between A(" + this.a[0] + ", " + this.a[1] + ", " + this.a[2] + ") and C(" + this.c[0] + ", " + this.c[1] + ", " + this.c[2] + ") :");
		for(float[] coordinates : intersectionPointListAC) {
			logger.debug("(" + coordinates[0] + ", " + coordinates[1] + ", " + coordinates[2] + ")");
		}*/

		return intersectionPointListAC;
	}

	private List<float[]> computeBCIntersectionPoints() {
		// Compute all intersection points between BC and slices lines
		List<float[]> intersectionPointListBC = new ArrayList<float[]>();
		float intervallX;
		float intervallY;
		float intervallZ;

		if((this.c[0]-this.b[0])<=0) {
			intervallX = Math.abs((this.b[0]-this.c[0])/(this.slicesNumber + 1));
		} else {
			intervallX = Math.abs((this.c[0]-this.b[0])/(this.slicesNumber + 1));
		}
		if((this.c[1]-this.b[1])<=0) {
			intervallY = Math.abs((this.b[1]-this.c[1])/(this.slicesNumber + 1));			
		} else {
			intervallY = Math.abs((this.c[1]-this.b[1])/(this.slicesNumber + 1));			
		}
		if ((this.c[2]-this.b[2])<=0) {
			intervallZ = Math.abs((this.c[2]-this.b[2])/(this.slicesNumber + 1));
		} else {
			intervallZ = Math.abs((this.b[2]-this.c[2])/(this.slicesNumber + 1));			
		}

		intersectionPointListBC.add(this.b);

		for(int i=1; i<=this.slicesNumber; i++) {			
			float[] intersectionPoint = new float[3];
			if(this.b[0]<=this.c[0]) {
				intersectionPoint[0] = this.b[0]+i*intervallX;
			} else {
				intersectionPoint[0] = this.b[0]-i*intervallX;
			}
			if(this.b[1]<=this.c[1]) {
				intersectionPoint[1] = this.b[1]+i*intervallY;
			} else {
				intersectionPoint[1] = this.b[1]-i*intervallY;
			}
			if(this.b[2]<=this.c[2]) {
				intersectionPoint[2] = this.b[2]+i*intervallZ;
			} else {
				intersectionPoint[2] = this.b[2]-i*intervallZ;
			}
			intersectionPointListBC.add(intersectionPoint);
		}

		intersectionPointListBC.add(this.c);

		/*logger.debug("Coordinates found between B(" + this.b[0] + ", " + this.b[1] + ", " + this.b[2] + ") and C(" + this.c[0] + ", " + this.c[1] + ", " + this.c[2] + ") :");
		for(float[] coordinates : intersectionPointListBC) {
			logger.debug("(" + coordinates[0] + ", " + coordinates[1] + ", " + coordinates[2] + ")");
		}*/

		return intersectionPointListBC;
	}

	private float[][] computeUsefullPoints(List<float[]> intersectionPointListAC, List<float[]> intersectionPointListBC) {
		// Compute each useful point from the previous intersection points 
		// D   E
		// |----\
		// |  .  \
		// |------\
		// F       G
		float[][] usefulPointTab = new float[numberOfPoints][];
		float[] ABMiddle = {(intersectionPointListBC.get(0)[0]+intersectionPointListAC.get(0)[0])/2, (intersectionPointListBC.get(0)[1]+intersectionPointListAC.get(0)[1])/2, this.a[2]};
		usefulPointTab[0] = ABMiddle;

		for(int i=0; i<intersectionPointListAC.size()-1; i++) {
			float[] FGMiddle = {middle(intersectionPointListBC.get(i)[0], intersectionPointListAC.get(i)[0]), middle(intersectionPointListBC.get(i)[1], intersectionPointListAC.get(i)[1]), middle(intersectionPointListBC.get(i)[2], intersectionPointListAC.get(i)[2])};
			float[] DEMiddle = {middle(intersectionPointListBC.get(i+1)[0], intersectionPointListAC.get(i+1)[0]), middle(intersectionPointListBC.get(i+1)[1], intersectionPointListAC.get(i+1)[1]), middle(intersectionPointListBC.get(i+1)[2], intersectionPointListAC.get(i+1)[2])};
			float[] FGDEMiddle = {middle(FGMiddle[0], DEMiddle[0]), middle(FGMiddle[1], DEMiddle[1]), middle(FGMiddle[2], DEMiddle[2])};
			float[] usefulPoint = {FGDEMiddle[0], FGDEMiddle[1], FGDEMiddle[2]};

			usefulPointTab[i+1] = usefulPoint;	// i+1 because we have already load a data into the 0 index of usefulPointTab[]
		}

		usefulPointTab[numberOfPoints-1] = this.c;

		/*logger.debug("Useful points :");
				for(float[] coordinates : usefulPointTab) {
					logger.debug("(" + coordinates[0] + ", " + coordinates[1] + ", " + coordinates[2] + ")");
				}*/

		return usefulPointTab;
	}

	/**
	 * Returns the middle between two relatives coordinates.
	 * @param x first relative coordinate
	 * @param y second relative coordinate
	 * @return the middle between relatives coordinates
	 */
	private float middle(float x, float y) {
		if(x > 0 && y > 0 || x < 0 && y < 0) {
			return (x + y)/2;
		} else {
			if(x < y) {
				return x + (Math.abs(x)+Math.abs(y))/2;
			} else {
				return y + (Math.abs(x)+Math.abs(y))/2;
			}
		}
	}
}
