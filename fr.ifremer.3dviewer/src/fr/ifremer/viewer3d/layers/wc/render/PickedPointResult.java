package fr.ifremer.viewer3d.layers.wc.render;

import java.util.Optional;

import fr.ifremer.globe.core.model.wc.LatLonSample;

/**
 * Utility class for picking result
 */
public class PickedPointResult {
	Optional<LatLonSample> sample;
	double bestDistanceRetained;

	PickedPointResult(double bestDistance) {
		sample = Optional.empty();
		bestDistanceRetained = bestDistance;
	}

	public void update(double distance, LatLonSample sample) {
		bestDistanceRetained = distance;
		this.sample = Optional.of(sample);
	}
}
