package fr.ifremer.viewer3d.layers.wc.render;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;

import fr.ifremer.globe.core.model.wc.SlidingBuffer;
import fr.ifremer.globe.ogl.util.GLUtils;
import fr.ifremer.globe.ogl.util.PaletteTexture;
import fr.ifremer.globe.ogl.util.ShaderStore;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.utils.exception.GIOException;

public class VolumicRenderer2D  {

	/** Map which link all the shader programs with their ID. One shader program instance needed for each OGL context. */
	private ShaderStore shaderProgramsVolumicRendering;
	private ShaderStore shaderProgramsPointRendering;
	private ShaderStore shaderProgramsVolumicRenderingRedirect;
	private ShaderStore shaderProgramsPointRenderingRedirect;

	protected static final String VERTEX_SHADER = "/shader/volumicWC.vert";
	protected static final String FRAGMENT_SHADER = "/shader/volumicWC.frag";
	protected static final String GEOMETRY_SHADER_VOLUMIC= "/shader/volumicWC.geom";
	protected static final String GEOMETRY_SHADER_POINT= "/shader/volumicWCPoints.geom";
	protected static final String VERTEX_SHADER_REDIRECT = "/shader/volumicWCRedirect.vert";
	
	/** Generated ID for the textures associated to the shader program. */
	// private int[] texturesId;

	PaletteTexture palette=new PaletteTexture();

	private boolean wireframe;

	private SlidingBuffer<AbstractSinglePingSamples2D> buffer;

	public VolumicRenderer2D(SlidingBuffer<AbstractSinglePingSamples2D> buffer){
		shaderProgramsVolumicRendering=new ShaderStore(gl-> ShaderUtil.createShader(this, gl, VERTEX_SHADER, GEOMETRY_SHADER_VOLUMIC, FRAGMENT_SHADER));
		shaderProgramsPointRendering=new ShaderStore(gl-> ShaderUtil.createShader(this, gl, VERTEX_SHADER, GEOMETRY_SHADER_POINT, FRAGMENT_SHADER));
		shaderProgramsVolumicRenderingRedirect=new ShaderStore(gl-> ShaderUtil.createShader(this, gl, VERTEX_SHADER_REDIRECT, GEOMETRY_SHADER_VOLUMIC, FRAGMENT_SHADER));
		shaderProgramsPointRenderingRedirect=new ShaderStore(gl-> ShaderUtil.createShader(this, gl, VERTEX_SHADER_REDIRECT, GEOMETRY_SHADER_POINT, FRAGMENT_SHADER));
		this.buffer = buffer;
	}

	public void dispose() {
		shaderProgramsVolumicRendering.dispose();
		shaderProgramsPointRendering.dispose();
		shaderProgramsVolumicRenderingRedirect.dispose();
		shaderProgramsPointRenderingRedirect.dispose();
		palette.dispose();
	}

	public void draw(GL2 gl, VolumicRendererDisplayParameters displayParameters) throws GIOException
	{
		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT 	// Alpha test function and reference value
				| GL2.GL_ENABLE_BIT				// Enable bits for the user-definable clipping planes
				| GL2.GL_CURRENT_BIT			// Current RGBA color
				| GL.GL_DEPTH_BUFFER_BIT 		// Depth buffer test function
				| GL2.GL_TEXTURE_BIT 			// Enable bits for the four texture coordinates
				| GL2.GL_TRANSFORM_BIT			// 	Coefficients of the six clipping planes
				| GL2.GL_POLYGON_BIT);
		//
		gl.glEnable(GL.GL_BLEND);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		if(wireframe) {
			gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);
		}
		// Initialize the rendering if needed and get the ID of the shader program generated
		int shaderProgram = ShaderStore.DEFAULT_SHADER_PROGRAM;
		switch(displayParameters.getRendering())
		{
		case Point:
			shaderProgram = shaderProgramsPointRendering.getShaderProgram(gl);
			break;

		case Square:
		default:
			shaderProgram=shaderProgramsVolumicRendering.getShaderProgram(gl);
			break;
		}
		gl.glUseProgram(shaderProgram);
		GLUtils.checkGLError(gl);
		VolumicUniformLocations uniforms=new VolumicUniformLocations(gl, shaderProgram);
		// Get the position of the uniforms in the fragment program
		gl.glUniform1i(uniforms.palette,0);
		GLUtils.checkGLError(gl);

		Texture paletteTexture=palette.getColorPalette(gl, displayParameters.getColorMapIndex(), displayParameters.isInvertColor());
		GLUtils.checkGLError(gl);

		// Sending color map texture to the GPU
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		paletteTexture.bind(gl);
		//update uniform
		GLUtils.checkGLError(gl);

		gl.glUniform1f(uniforms.opacity, 1); //TODO
		GLUtils.checkGLError(gl);
		gl.glUniform1f(uniforms.minConstrast,  displayParameters.getContrastMin());
		GLUtils.checkGLError(gl);
		gl.glUniform1f(uniforms.maxContrast, displayParameters.getContrastMax());
		GLUtils.checkGLError(gl);

		GLUtils.checkGLError(gl);
		VolumicAttribLocation attributes=new VolumicAttribLocation(gl, shaderProgram,displayParameters.getRendering());
		attributes.enable(gl);
		// retrieve previous point size
		GLUtils.checkGLError(gl);
		FloatBuffer pre=ByteBuffer.allocateDirect(Float.BYTES).asFloatBuffer();
		gl.glGetFloatv(GL2.GL_POINT_SIZE,pre);
		//set point size for us
		gl.glPointSize(4);
		//really draw
		GLUtils.checkGLError(gl);
		for(AbstractSinglePingSamples2D ping: buffer.values())
		{
			ping.draw(gl, displayParameters,attributes,uniforms);
		}
		// Disable vertex arrays
		attributes.disable(gl);
		//set previous point size
		pre.rewind();
		gl.glPointSize(pre.get());
		//
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);
		gl.glDisable(GL.GL_BLEND);
		gl.glDisable(GL.GL_TEXTURE_2D);

		gl.glPopAttrib();

		GLUtils.checkGLError(gl);

		gl.glUseProgram(ShaderStore.DEFAULT_SHADER_PROGRAM);
		GLUtils.checkGLError(gl);
	} 

	public void redirect(GL2 gl,  VolumicRendererDisplayParameters displayParameters, float resolution, float minValue) throws GIOException
	{
		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT 	// Alpha test function and reference value
				| GL2.GL_ENABLE_BIT				// Enable bits for the user-definable clipping planes
				| GL2.GL_CURRENT_BIT			// Current RGBA color
				| GL.GL_DEPTH_BUFFER_BIT 		// Depth buffer test function
				| GL2.GL_TEXTURE_BIT 			// Enable bits for the four texture coordinates
				| GL2.GL_TRANSFORM_BIT			// 	Coefficients of the six clipping planes
				| GL2.GL_POLYGON_BIT);
		//
		gl.glDisable(GL.GL_BLEND);
		gl.glDisable(GL2.GL_TEXTURE_2D);
		// Initialize the rendering if needed and get the ID of the shader program generated
		int shaderProgram = ShaderStore.DEFAULT_SHADER_PROGRAM;
		switch(displayParameters.getRendering())
		{
		case Point:
			shaderProgram = shaderProgramsPointRenderingRedirect.getShaderProgram(gl);
			break;

		case Square:
		default:
			shaderProgram=shaderProgramsVolumicRenderingRedirect.getShaderProgram(gl);
			break;
		}
		gl.glUseProgram(shaderProgram);
		GLUtils.checkGLError(gl);
		VolumicUniformLocations uniforms=new VolumicUniformLocations(gl, shaderProgram);
		// Get the position of the uniforms in the fragment program
		gl.glUniform1f(uniforms.resolution, resolution);
		gl.glUniform1f(uniforms.minValue, minValue);
		GLUtils.checkGLError(gl);
		
		VolumicAttribLocation attributes=new VolumicAttribLocation(gl, shaderProgram,displayParameters.getRendering());
		attributes.enable(gl);
		// retrieve previous point size
		GLUtils.checkGLError(gl);
		FloatBuffer pre=ByteBuffer.allocateDirect(Float.BYTES).asFloatBuffer();
		gl.glGetFloatv(GL2.GL_POINT_SIZE,pre);
		//set point size for us
		gl.glPointSize(4);
		//really draw
		GLUtils.checkGLError(gl);
		for(AbstractSinglePingSamples2D ping: buffer.values())
		{
			ping.draw(gl, displayParameters,attributes,uniforms);
		}
		// Disable vertex arrays
		attributes.disable(gl);
		//set previous point size
		pre.rewind();
		gl.glPointSize(pre.get());
		//
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);
		gl.glDisable(GL.GL_BLEND);
		gl.glDisable(GL.GL_TEXTURE_2D);

		gl.glPopAttrib();

		GLUtils.checkGLError(gl);

		gl.glUseProgram(ShaderStore.DEFAULT_SHADER_PROGRAM);
		GLUtils.checkGLError(gl);
	} 

}
