package fr.ifremer.viewer3d.layers.wc.render;

import java.util.Iterator;
import java.util.function.DoubleSupplier;

import com.jogamp.opengl.GL2;

import fr.ifremer.globe.core.model.wc.LatLonSample;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Abstract container of WC samples for a single Ping, for the 3d viewer
 */
public abstract class AbstractSinglePingSamples3D extends SinglePingSamples {

	protected Vec4 referencePoint;

	protected final DoubleSupplier verticalOffsetSupplier;
	protected final DoubleSupplier verticalExaggerationSupplier;

	/**
	 * Constructor
	 */
	protected AbstractSinglePingSamples3D(int pingIndex, DoubleSupplier verticalOffsetSupplier,
			DoubleSupplier verticalExaggerationSupplier) {
		super(pingIndex);
		this.verticalOffsetSupplier = verticalOffsetSupplier;
		this.verticalExaggerationSupplier = verticalExaggerationSupplier;
	}

	public double getVerticalOffset() {
		return verticalOffsetSupplier.getAsDouble();
	}

	public double getVerticalExaggeration() {
		return verticalExaggerationSupplier.getAsDouble();
	}

	public void draw(VolumicRendererDisplayParameters parameters, DrawContext dc, VolumicAttribLocation attributes,
			VolumicUniformLocations uniforms) {
		if (isEmpty()) {
			return;
		}

		dc.getView().pushReferenceCenter(dc, referencePoint);
		GL2 gl = dc.getGL().getGL2();
		super.draw(gl, parameters, attributes, uniforms);
		dc.getView().popReferenceCenter(dc);
	}

	public void render_pick(VolumicRendererDisplayParameters parameters, DrawContext dc,
			VolumicAttribLocation attributes, VolumicUniformLocations uniforms) {
		draw(parameters, dc, attributes, uniforms);
	}

	/**
	 * Find the nearests point to the ray, compute distance to the ray and return the lowest one.
	 *
	 * @param ray the line to intersect
	 * @param previousBestDistance the last lowest known distance to the ray
	 */
	public abstract PickedPointResult find_nearest(Line ray, double previousBestDistance);

	public abstract Iterator<LatLonSample> getSampleIterator();

}
