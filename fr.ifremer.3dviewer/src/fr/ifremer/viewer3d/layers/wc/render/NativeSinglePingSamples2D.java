package fr.ifremer.viewer3d.layers.wc.render;

import java.nio.FloatBuffer;

import fr.ifremer.globe.core.model.ModelPreferences;
import fr.ifremer.globe.core.model.wc.SwathSpatialData;
import fr.ifremer.viewer3d.layers.wc.SonarNativeProxy.NativeBeamSample;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.geom.Vec4;

/**
 * a class containing WC samples for a single Ping
 */
public class NativeSinglePingSamples2D extends AbstractSinglePingSamples2D {

	private final NativeBeamSample beamSamples;

	/**
	 * Constructor
	 */
	public NativeSinglePingSamples2D(NativeBeamSample beamSamples, SwathSpatialData swathSpatialData) {
		super(beamSamples.pingIndex);
		this.swathSpatialData = swathSpatialData;
		this.beamSamples = beamSamples;
		sampleCount = beamSamples.sampleCount;
		fillBuffers();
	}

	/**
	 * Fill vertex buffer with sample values projected in Nwd coordinates. This means that the content of this buffer is
	 * not only dependent of the data itself but also of offset applyied to data and vertical exaggeration
	 */
	protected void fillBuffers() {

		acrossVector = new Vec4(1, 0, 0);
		topPoint = new Vec4(0, -swathSpatialData.topPointDepth, 0);
		alongVector = new Vec4(0, 0, 1);

		VBO vertex = getVertexBuffer();
		VBO amplitude = getAmplitudeBuffer();
		VBO beamWidthAcross = getBeamWidthAcrossBuffer();
		VBO beamWidthAlong = getBeamWidthAlongBuffer();
		VBO height = getEchoHeightBuffer();
		vertex.rewind();
		amplitude.rewind();
		beamWidthAcross.rewind();
		beamWidthAlong.rewind();
		height.rewind();

		minAccrossDistance = Double.POSITIVE_INFINITY;
		maxAccrossDistance = Double.NEGATIVE_INFINITY;
		topElevation = Double.NEGATIVE_INFINITY;
		bottomElevation = Double.POSITIVE_INFINITY;

		FloatBuffer beamOpeningAngles = beamSamples.beamOpeningAcrosses;
		float scaleOpeningAngle = 1.f;
		if (ModelPreferences.getInstance().getRecompute_beam_widths().getValue()) {
			beamOpeningAngles = beamSamples.nonOverlappingBeamOpeningAngles;
			scaleOpeningAngle = 1.1f;
		}

		for (int index = 0; index < sampleCount; index++) {
			float acrossDistance = beamSamples.acrosses.get(index);
			float elevation = beamSamples.elevations.get(index);
			vertex.put(acrossDistance); // x
			vertex.put(elevation); // y
			vertex.put(0); // z = 0 for 2D display
			amplitude.put(beamSamples.echoes.get(index));

			// apply scale to avoid display artefacts
			beamWidthAcross.put(Math.toRadians(scaleOpeningAngle * beamOpeningAngles.get(index)));
			beamWidthAlong.put(Math.toRadians(beamSamples.beamOpeningAlongs.get(index)));
			height.put(beamSamples.heights.get(index));

			// compute stats
			bottomElevation = Math.min(bottomElevation, elevation);
			topElevation = Math.max(topElevation, elevation);

			if (acrossDistance < minAccrossDistance) {
				minAccrossDistance = acrossDistance;
				leftLatLon = new double[] { beamSamples.latitudes.get(index), beamSamples.longitudes.get(index) };
			}
			if (acrossDistance > maxAccrossDistance) {
				maxAccrossDistance = acrossDistance;
				rightLatLon = new double[] { beamSamples.latitudes.get(index), beamSamples.longitudes.get(index) };
			}
		}
	}
}
