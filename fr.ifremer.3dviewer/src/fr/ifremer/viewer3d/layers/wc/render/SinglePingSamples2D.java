package fr.ifremer.viewer3d.layers.wc.render;

import java.util.LinkedList;

import fr.ifremer.globe.core.model.wc.LatLonSample;
import fr.ifremer.globe.core.model.wc.PingLoader;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.geom.Vec4;

/**
 * a class containing WC samples for a single Ping
 */
public class SinglePingSamples2D extends AbstractSinglePingSamples2D {

	protected LinkedList<LatLonSample> samples;

	/**
	 * Constructor
	 */
	public SinglePingSamples2D(PingLoader loader, int pingIndex) throws GIOException {
		super(pingIndex);
		if (loader != null) { // May be null when data loading failed
			load(loader);
		}
	}

	/**
	 * Loads data in buffers
	 */
	protected void load(PingLoader loader) throws GIOException {
		samples = new LinkedList<>();
		if (loader != null) { // May be null when data loading failed
			samples = loader.load(pingIndex);
			// data are loaded, filtered and spatialized
			if (!samples.isEmpty()) {
				sampleCount = samples.size();
				LatLonSample firstSample = samples.getFirst();
				swathSpatialData = firstSample.ping;

				// compute along ship direction vector
				acrossVector = new Vec4(1, 0, 0);
				topPoint = new Vec4(0, -swathSpatialData.topPointDepth, 0);
				alongVector = new Vec4(0, 0, 1);

				fillBuffers();
			}
		}
	}

	/**
	 * Fill vertex buffer with sample values projected in Nwd coordinates. This means that the content of this buffer is
	 * not only dependent of the data itself but also of offset applyied to data and vertical exaggeration
	 */
	private void fillBuffers() {
		VBO vertex = getVertexBuffer();
		VBO amplitude = getAmplitudeBuffer();
		VBO beamWidthAcross = getBeamWidthAcrossBuffer();
		VBO beamWidthAlong = getBeamWidthAlongBuffer();
		VBO height = getEchoHeightBuffer();
		vertex.rewind();
		amplitude.rewind();
		beamWidthAcross.rewind();
		beamWidthAlong.rewind();
		height.rewind();

		minAccrossDistance = Double.POSITIVE_INFINITY;
		maxAccrossDistance = Double.NEGATIVE_INFINITY;
		topElevation = Double.NEGATIVE_INFINITY;
		bottomElevation = Double.POSITIVE_INFINITY;

		for (LatLonSample sample : samples) {
			vertex.put(sample.acrossDistance); // x
			vertex.put(sample.elevation); // y
			vertex.put(0); // z = 0 for 2D display
			amplitude.put(sample.echo.value);

			// useless for 2D ?
			beamWidthAcross.put(Math.toRadians(sample.echo.data.beamwidth_across[sample.echo.beam]));
			beamWidthAlong.put(Math.toRadians(sample.echo.data.beamwidth_along[sample.echo.beam]));
			height.put(sample.height);

			// compute stats
			bottomElevation = Math.min(bottomElevation, sample.elevation);
			topElevation = Math.max(topElevation, sample.elevation);

			if (sample.acrossDistance < minAccrossDistance) {
				minAccrossDistance = sample.acrossDistance;
				leftLatLon = new double[] { sample.latitude, sample.longitude };
			}
			if (sample.acrossDistance > maxAccrossDistance) {
				maxAccrossDistance = sample.acrossDistance;
				rightLatLon = new double[] { sample.latitude, sample.longitude };
			}
		}
	}

}
