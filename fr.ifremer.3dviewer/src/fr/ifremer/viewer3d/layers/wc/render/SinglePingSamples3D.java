package fr.ifremer.viewer3d.layers.wc.render;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.DoubleSupplier;

import fr.ifremer.globe.core.model.wc.LatLonSample;
import fr.ifremer.globe.core.model.wc.PingLoader;
import fr.ifremer.globe.core.model.wc.SwathSpatialData;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.MbesUtils;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

/**
 * a class containing WC samples for a single Ping
 */
public class SinglePingSamples3D extends AbstractSinglePingSamples3D {

	protected LinkedList<LatLonSample> samples;

	private IGeographicViewService geographicViewService = IGeographicViewService.grab();

	/**
	 * Constructor
	 */
	public SinglePingSamples3D(PingLoader loader, int pingIndex, DoubleSupplier verticalOffsetSupplier,
			DoubleSupplier verticalExaggerationSupplier) throws GIOException {
		super(pingIndex, verticalOffsetSupplier, verticalExaggerationSupplier);
		load(loader);
	}

	/**
	 * Loads data in buffers
	 */
	protected void load(PingLoader loader) throws GIOException {
		samples = new LinkedList<>();
		samples = loader.load(pingIndex);
		sampleCount = samples.size();
		// data are loaded, filtered and spatialized
		if (!samples.isEmpty()) {
			LatLonSample firstSample = samples.getFirst();
			double offset = getVerticalOffset();
			double verticalExaggeration = getVerticalExaggeration();
			SwathSpatialData ping = firstSample.ping;
			referencePoint = geographicViewService.computePointFromPosition(Position.fromDegrees(ping.shipLatitudeDeg,
					ping.shipLongitudeDeg, (-ping.shipDepth + offset) * verticalExaggeration));
			// reference point in the summit, it match the antenna position

			// compute along ship direction vector

			double fakeDistance = 100;
			double[] latlon = MbesUtils.acrossAlongToWGS84LatLong(ping.shipLatitudeDeg, ping.shipLongitudeDeg,
					ping.shipHeadingDeg, fakeDistance, 0);

			acrossVector = geographicViewService.computePointFromPosition(
					Position.fromDegrees(latlon[0], latlon[1], (-ping.shipDepth + offset) * verticalExaggeration));
			acrossVector = acrossVector.subtract3(referencePoint);

			latlon = MbesUtils.acrossAlongToWGS84LatLong(ping.shipLatitudeDeg, ping.shipLongitudeDeg,
					ping.shipHeadingDeg, 0, fakeDistance);

			alongVector = geographicViewService.computePointFromPosition(
					Position.fromDegrees(latlon[0], latlon[1], (-ping.shipDepth + offset) * verticalExaggeration));
			alongVector = alongVector.subtract3(referencePoint);

			topPoint = geographicViewService.computePointFromPosition(
					Position.fromDegrees(ping.topPointLatitude, ping.topPointLongitude),
					(-ping.topPointDepth + offset) * verticalExaggeration);
			topPoint = topPoint.subtract3(referencePoint);

			fillBuffers();

		}

	}

	/**
	 * Fill vertex buffer with sample values projected in Nwd coordinates. This means that the content of this buffer is
	 * not only dependent of the data itself but also of offset applyied to data and vertical exaggeration
	 */
	private void fillBuffers() {
		VBO vertex = getVertexBuffer();
		VBO amplitude = getAmplitudeBuffer();
		VBO beamWidthAcross = getBeamWidthAcrossBuffer();
		VBO beamWidthAlong = getBeamWidthAlongBuffer();
		VBO height = getEchoHeightBuffer();
		vertex.rewind();
		amplitude.rewind();
		beamWidthAcross.rewind();
		beamWidthAlong.rewind();
		height.rewind();

		double offset = getVerticalOffset();
		double verticalExaggeration = getVerticalExaggeration();

		for (LatLonSample sample : samples) {
			Vec4 center = geographicViewService.computePointFromPosition(Position.fromDegrees(sample.latitude,
					sample.longitude, (sample.elevation + offset) * verticalExaggeration));
			center = center.subtract3(referencePoint);
			vertex.put(center.x);
			vertex.put(center.y);
			vertex.put(center.z);
			amplitude.put(sample.echo.value);
			beamWidthAcross.put(Math.toRadians(sample.echo.data.beamwidth_across[sample.echo.beam]));
			beamWidthAlong.put(Math.toRadians(sample.echo.data.beamwidth_along[sample.echo.beam]));
			height.put(verticalExaggeration * sample.height);
		}
	}

	/**
	 * Find the nearests point to the ray, compute distance to the ray and return the lowest one.
	 *
	 * @param ray the line to intersect
	 * @param previousBestDistance the last lowest known distance to the ray
	 */
	@Override
	public PickedPointResult find_nearest(Line ray, double previousBestDistance) {
		double offset = getVerticalOffset();
		double verticalExaggeration = getVerticalExaggeration();
		PickedPointResult ret = new PickedPointResult(previousBestDistance);
		for (var iterator = getSampleIterator(); iterator.hasNext();) {
			LatLonSample sample = iterator.next();
			Vec4 center = geographicViewService.computePointFromPosition(Position.fromDegrees(sample.latitude,
					sample.longitude, (sample.elevation + offset) * verticalExaggeration));
			double distance = ray.distanceTo(center);
			if (distance < ret.bestDistanceRetained) {
				ret.update(distance, sample);
			}
		}
		return ret;
	}

	/** {@inheritDoc} */
	@Override
	public Iterator<LatLonSample> getSampleIterator() {
		return samples.iterator();
	}
}
