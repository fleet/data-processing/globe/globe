package fr.ifremer.viewer3d.layers.wc.render;

import java.nio.FloatBuffer;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.DoubleSupplier;

import fr.ifremer.globe.core.model.ModelPreferences;
import fr.ifremer.globe.core.model.wc.LatLonSample;
import fr.ifremer.globe.core.model.wc.Spatializer;
import fr.ifremer.globe.core.model.wc.SwathBeamSample;
import fr.ifremer.globe.core.model.wc.SwathSpatialData;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.utils.mbes.MbesUtils;
import fr.ifremer.viewer3d.layers.wc.SonarNativeProxy.NativeBeamSample;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

/**
 * a class containing WC samples for a single Ping
 */
public class NativeSinglePingSamples3D extends AbstractSinglePingSamples3D {

	private final NativeBeamSample beamSamples;
	private final SwathSpatialData swathSpatialData;

	private IGeographicViewService geographicViewService = IGeographicViewService.grab();

	/**
	 * Constructor
	 */
	public NativeSinglePingSamples3D(NativeBeamSample beamSamples, Spatializer spatializer,
			DoubleSupplier verticalOffsetSupplier, DoubleSupplier verticalExaggerationSupplier) {
		super(beamSamples.pingIndex, verticalOffsetSupplier, verticalExaggerationSupplier);
		sampleCount = beamSamples.sampleCount;
		this.beamSamples = beamSamples;
		swathSpatialData = spatializer.getPing();

		configure();
		fillBuffers();
	}

	private void configure() {
		var shipLatitudeDeg = swathSpatialData.shipLatitudeDeg;
		var shipLongitudeDeg = swathSpatialData.shipLongitudeDeg;
		var shipHeadingDeg = swathSpatialData.shipHeadingDeg;
		double refVerticalOffset = getVerticalOffset() - swathSpatialData.shipDepth;
		double verticalExaggeration = getVerticalExaggeration();

		double txVerticalOffset = getVerticalOffset() - swathSpatialData.topPointDepth;

		// apply exaggeration
		refVerticalOffset *= verticalExaggeration;
		txVerticalOffset *= verticalExaggeration;

		// wc echoes are referenced from the surface
		referencePoint = geographicViewService
				.computePointFromPosition(Position.fromDegrees(shipLatitudeDeg, shipLongitudeDeg, refVerticalOffset));

		double fakeDistance = 100;
		double[] latlon = MbesUtils.acrossAlongToWGS84LatLong(shipLatitudeDeg, shipLongitudeDeg, shipHeadingDeg,
				fakeDistance, 0);

		acrossVector = geographicViewService
				.computePointFromPosition(Position.fromDegrees(latlon[0], latlon[1], refVerticalOffset));
		acrossVector = acrossVector.subtract3(referencePoint);

		latlon = MbesUtils.acrossAlongToWGS84LatLong(shipLatitudeDeg, shipLongitudeDeg, shipHeadingDeg, 0,
				fakeDistance);

		alongVector = geographicViewService
				.computePointFromPosition(Position.fromDegrees(latlon[0], latlon[1], refVerticalOffset));
		alongVector = alongVector.subtract3(referencePoint);

		topPoint = geographicViewService.computePointFromPosition(
				Position.fromDegrees(swathSpatialData.topPointLatitude, swathSpatialData.topPointLongitude),
				txVerticalOffset);
		topPoint = topPoint.subtract3(referencePoint);
	}

	protected void fillBuffers() {

		VBO vertex = getVertexBuffer();
		VBO amplitude = getAmplitudeBuffer();
		VBO beamWidthAcross = getBeamWidthAcrossBuffer();
		VBO beamWidthAlong = getBeamWidthAlongBuffer();
		VBO height = getEchoHeightBuffer();

		vertex.rewind();
		amplitude.rewind();
		beamWidthAcross.rewind();
		beamWidthAlong.rewind();
		height.rewind();

		double offset = getVerticalOffset() - swathSpatialData.shipDepth;
		double verticalExaggeration = getVerticalExaggeration();

		FloatBuffer beamOpeningAngles = beamSamples.beamOpeningAcrosses;
		float scaleOpeningAngle = 1.f;
		if (ModelPreferences.getInstance().getRecompute_beam_widths().getValue()) {
			beamOpeningAngles = beamSamples.nonOverlappingBeamOpeningAngles;
			scaleOpeningAngle = 1.1f;
		}

		for (int i = 0; i < sampleCount; i++) {
			Vec4 center = geographicViewService.computePointFromPosition(
					Position.fromDegrees(beamSamples.latitudes.get(i), beamSamples.longitudes.get(i),
							(beamSamples.elevations.get(i) + offset) * verticalExaggeration));
			center = center.subtract3(referencePoint);
			vertex.put(center.x);
			vertex.put(center.y);
			vertex.put(center.z);
			amplitude.put(beamSamples.echoes.get(i));
			// apply scale to avoid display artefacts
			beamWidthAcross.put(Math.toRadians(scaleOpeningAngle * beamOpeningAngles.get(i) / verticalExaggeration));
			beamWidthAlong.put(Math.toRadians(beamSamples.beamOpeningAlongs.get(i) / verticalExaggeration));
			height.put(verticalExaggeration * beamSamples.heights.get(i));
		}
	}

	/**
	 * Find the nearests point to the ray, compute distance to the ray and return the lowest one.
	 *
	 * @param ray the line to intersect
	 * @param previousBestDistance the last lowest known distance to the ray
	 */
	@Override
	public PickedPointResult find_nearest(Line ray, double previousBestDistance) {
		double offset = getVerticalOffset();
		double verticalExaggeration = getVerticalExaggeration();
		PickedPointResult ret = new PickedPointResult(previousBestDistance);
		for (int index = 0; index < sampleCount; index++) {
			Vec4 center = geographicViewService.computePointFromPosition(
					Position.fromDegrees(beamSamples.latitudes.get(index), beamSamples.longitudes.get(index),
							(beamSamples.elevations.get(index) + offset) * verticalExaggeration));
			double distance = ray.distanceTo(center);
			if (distance < ret.bestDistanceRetained) {
				ret.update(distance, makeLatLonSample(index));
			}
		}
		return ret;
	}

	@Override
	public Iterator<LatLonSample> getSampleIterator() {
		return new Iterator<LatLonSample>() {
			int index = 0;

			@Override
			public boolean hasNext() {
				return sampleCount > index;
			}

			@Override
			public LatLonSample next() {
				if (!hasNext())
					throw new NoSuchElementException();
				var result = makeLatLonSample(index);
				index++;
				return result;
			}

		};
	}

	/**
	 * @return
	 */
	protected LatLonSample makeLatLonSample(int index) {
		return new LatLonSample(beamSamples.latitudes.get(index), beamSamples.longitudes.get(index),
				beamSamples.beamOpeningAcrosses.get(index), beamSamples.beamOpeningAlongs.get(index),
				beamSamples.elevations.get(index), beamSamples.heights.get(index),
				new SwathBeamSample(0, 0, beamSamples.echoes.get(index), null), swathSpatialData);
	}

}
