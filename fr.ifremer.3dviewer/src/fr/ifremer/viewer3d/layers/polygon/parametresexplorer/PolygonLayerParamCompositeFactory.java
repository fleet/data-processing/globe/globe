/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.polygon.parametresexplorer;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.polygon.PolygonLayer;
import fr.ifremer.viewer3d.util.BasicParametersViewCompositeFactory;

/**
 * Make a Composite to edit the {@link PolygonLayer} parameters.
 */
@Component(name = "globe_viewer3d_layers_polygon_param_composite_factory", service = IParametersViewCompositeFactory.class)
public class PolygonLayerParamCompositeFactory extends BasicParametersViewCompositeFactory<IWWPolygonLayer> {

	/**
	 * Constructor
	 */
	public PolygonLayerParamCompositeFactory() {
		super(ContentType.POLYGON_GDAL, IWWPolygonLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	protected Composite makeComposite(Composite parent, IWWPolygonLayer layer) {
		var result = new PolygonParametersComposite(parent, layer);
		ContextInitializer.inject(result);
		return result;
	}
}
