package fr.ifremer.viewer3d.layers.polygon.parametresexplorer;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.widgets.LabelFactory;
import org.eclipse.jface.widgets.SpinnerFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.PolygonParameters;
import fr.ifremer.globe.ui.widget.CheckBoxGroup;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import fr.ifremer.viewer3d.layers.sync.polygon.WWPolygonLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Composite that changes parameters of IWWPolygonLayer.
 */
public class PolygonParametersComposite extends Composite {

	/** The layer parameters synchronizer get from Context **/
	@Inject
	private WWPolygonLayerSynchronizer layersSynchronizer;

	/** Model used in this Composite */
	private final IWWPolygonLayer polygonLayer;

	/**
	 * Constructor.
	 */
	public PolygonParametersComposite(Composite parent, IWWPolygonLayer polygonLayer) {
		super(parent, SWT.NONE);
		this.polygonLayer = polygonLayer;
	}

	@PostConstruct
	private void postConstruct() {
		initializeComposite();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void initializeComposite() {
		GridLayoutFactory.fillDefaults().margins(0, 0).numColumns(1).applyTo(this);

		// Synchronizer composite
		var grpSynchronization = new LayerParametersSynchronizerComposite<>(this, layersSynchronizer, polygonLayer);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(grpSynchronization);
		grpSynchronization.setText("Synchronization with other layers");
		grpSynchronization.adaptToLayer(polygonLayer);

		// Outline
		CheckBoxGroup grpOutline = new CheckBoxGroup(this, SWT.NONE);
		grpOutline.setText("Border");
		grpOutline.addSelectionListener(SelectionListener
				.widgetSelectedAdapter(e -> fireParametersChanged(getParameters().withOutLine(grpOutline.getSelection(),
						getParameters().lineColor, getParameters().lineWidth))));
		GridDataFactory.fillDefaults().grab(true, false).applyTo(grpOutline);
		GridLayoutFactory.fillDefaults().margins(5, 5).numColumns(2).applyTo(grpOutline);

		LabelFactory.newLabel(SWT.NONE)//
				.text("Color : ")//
				.layoutData(GridDataFactory.swtDefaults().create())//
				.create(grpOutline.getContent());

		new ColorPicker(grpOutline.getContent(), SWT.BORDER, getParameters().lineColor, //
				color -> fireParametersChanged(
						getParameters().withOutLine(getParameters().drawOutline, color, getParameters().lineWidth))//
		).setLayoutData(GridDataFactory.fillDefaults().hint(SWT.DEFAULT, 20)//
				.create());

		LabelFactory.newLabel(SWT.NONE)//
				.text("Width : ")//
				.layoutData(GridDataFactory.swtDefaults().create())//
				.create(grpOutline.getContent());

		Spinner lineWidthSpinner = SpinnerFactory.newSpinner(SWT.BORDER) //
				.bounds(1, 10)//
				.onModify(e -> fireParametersChanged(getParameters().withOutLine(getParameters().drawOutline,
						getParameters().lineColor, ((Spinner) e.widget).getSelection())))//
				.layoutData(GridDataFactory.fillDefaults().create())//
				.create(grpOutline.getContent());
		lineWidthSpinner.setSelection(getParameters().lineWidth);
		grpOutline.setSelection(getParameters().drawOutline);

		// Interior
		CheckBoxGroup grpInterior = new CheckBoxGroup(this, SWT.NONE);
		grpInterior.setText("Interior");
		grpInterior.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> fireParametersChanged(
				getParameters().withFill(grpInterior.getSelection(), getParameters().fillColor))));
		GridDataFactory.fillDefaults().grab(true, false).applyTo(grpInterior);
		GridLayoutFactory.fillDefaults().margins(5, 5).numColumns(2).applyTo(grpInterior);

		LabelFactory.newLabel(SWT.NONE)//
				.text("Color : ")//
				.layoutData(GridDataFactory.swtDefaults().create())//
				.create(grpInterior.getContent());
		new ColorPicker(grpInterior.getContent(), SWT.BORDER, getParameters().fillColor, //
				color -> fireParametersChanged(getParameters().withFill(getParameters().fill, color))//
		).setLayoutData(GridDataFactory.fillDefaults().hint(SWT.DEFAULT, 20).create());
		grpInterior.setSelection(getParameters().fill);

		// Opacity
		Group grpOpacity = new Group(this, SWT.NONE);
		grpOpacity.setText("Opacity");
		grpOpacity.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
		GridLayoutFactory.fillDefaults().margins(5, 5).applyTo(grpOpacity);

		SliderComposite sliderOpacity = new SliderComposite(grpOpacity, SWT.NONE, 0f, 1f, 0.1f, false, //
				opacity -> fireParametersChanged(getParameters().withOpacity(opacity)));
		sliderOpacity.setValue(getParameters().opacity);
		sliderOpacity.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
	}

	/**
	 * Changes and propagates parameters
	 */
	private void fireParametersChanged(PolygonParameters newParameters) {
		polygonLayer.setParameters(newParameters);
		layersSynchronizer.synchonizeWith(polygonLayer);
	}

	/**
	 * @return the {@link #polygonParameters}
	 */
	private PolygonParameters getParameters() {
		return polygonLayer.getParameters();
	}
}
