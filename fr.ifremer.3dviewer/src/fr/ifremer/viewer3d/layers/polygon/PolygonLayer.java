/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.polygon;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import javax.swing.SwingUtilities;

import org.eclipse.e4.ui.di.UIEventTopic;
import org.gdal.ogr.Geometry;

import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.PolygonParameters;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonController;
import fr.ifremer.viewer3d.gui.polygoneditor.PolygonTopics;
import fr.ifremer.viewer3d.layers.markers.render.WWMarker;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuController;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuEvent;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuInfo;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuItemAction;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuItemInfo;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Intersection;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.pick.PickedObjectList;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.SurfacePolygon;
import gov.nasa.worldwind.render.airspaces.BasicAirspaceAttributes;
import gov.nasa.worldwind.render.airspaces.editor.AirspaceEditorUtil;
import gov.nasa.worldwind.render.markers.BasicMarker;
import gov.nasa.worldwind.render.markers.BasicMarkerAttributes;
import gov.nasa.worldwind.render.markers.Marker;
import gov.nasa.worldwind.render.markers.MarkerAttributes;
import gov.nasa.worldwind.render.markers.MarkerRenderer;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Layer used to display and modify a polygon on 3D view
 */
public class PolygonLayer extends RenderableLayer implements IWWPolygonLayer {

	/** 3D view service */
	@Inject
	private IGeographicViewService geographicViewService;

	/** Session service */
	@Inject
	private ISessionService sessionService;

	/** Controller of polygons */
	@Inject
	private PolygonController controller;

	/** Use react to handle back pressure on position list changes */
	private final PublishSubject<Geometry> positionsPublisher = PublishSubject.create();
	private Disposable computeSubscriber;

	/** Current edited positions */
	private List<Position> positions = new ArrayList<>();

	/** PolygonFileInfo edited in this layer */
	private final PolygonFileInfo polygonFileInfo;

	/** Current edited polygon in its WW format */
	private Optional<WWSurfacePolygon> wwPolygon = Optional.empty();

	/** List of the control points, ie markers used to edit points of polygon */
	private final List<Marker> controlPoints = new ArrayList<>();
	/** Flag is true when controlPoints list must be refreshed */
	private final AtomicBoolean refreshControlPoints = new AtomicBoolean(true);

	/** Default style of a control point */
	private BasicMarkerAttributes vertexControlAttributes;
	/** Style of a highlighted control point */
	private BasicMarkerAttributes highlightVertexControlAttributes;
	/** Renderer of control points */
	private final MarkerRenderer markerRenderer;

	/** ActiveControlPoint when no point is active */
	private final ActiveControlPoint noActiveControlPoint = new ActiveControlPoint(-1);
	/** Current edited point */
	private ActiveControlPoint activeControlPoint = noActiveControlPoint;

	/** Mode of the layer */
	private LayerMode layerMode = LayerMode.VISUALIZATION;

	/** Mouse listener */
	private final MouseManager mouseManager = new MouseManager();

	/** Last mouse position */
	private Point mousePoint;

	/** Parameters used to represent the layer */
	private PolygonParameters polygonParameters = new PolygonParameters();

	/**
	 * Kind of action on polygons
	 */
	private enum MouseAction {
		NONE, MOVE_VERTEX
	}

	/**
	 * Kind of operation on current polygon
	 */
	enum LayerMode {
		VISUALIZATION, CREATION, UPDATE
	}

	/**
	 * Current action on polygon
	 */
	private MouseAction activeAction;

	/**
	 * Constructor
	 */
	public PolygonLayer(PolygonFileInfo polygonFileInfo) {
		this.polygonFileInfo = polygonFileInfo;
		setName("Polygons of " + polygonFileInfo.getPath());

		markerRenderer = new MarkerRenderer();
		markerRenderer.setKeepSeparated(false);
		markerRenderer.setOverrideMarkerElevation(false);
		markerRenderer.setEnablePickSizeReturn(true);
	}

	/**
	 * Post injection initialization
	 */
	@PostConstruct
	public void postConstruct() {
		onPolygonFileChanged(controller.getCurrentPolygonFile());
		computeSubscriber = positionsPublisher//
				.toFlowable(BackpressureStrategy.LATEST)//
				.observeOn(Schedulers.computation(), false, 3)//
				.subscribe(this::notifyController);

		WorldWindowGLCanvas wwd = geographicViewService.getWwd();
		wwd.getInputHandler().addMouseListener(mouseManager);
		wwd.getInputHandler().addMouseMotionListener(mouseManager);
	}

	/** Notify the controller. The layer updates the view when onPolygonChanged is invoked */
	private void notifyController(Geometry geometry) {
		controller.updateEditedPolygon(geometry);
	}

	/** Recreate all renderables */
	private void createRenderables() {
		Optional<IPolygon> currentPolygon = controller.getCurrentPolygonFile() == polygonFileInfo
				? controller.getEditedPolygon()
				: Optional.empty();

		SwingUtilities.invokeLater(() -> {
			clearRenderables();
			wwPolygon = Optional.empty();

			for (IPolygon polygon : polygonFileInfo.getPolygons()) {
				// Creates one WWSurfacePolygon per IPolygon
				WWSurfacePolygon oneWwPolygon = new WWSurfacePolygon(polygonFileInfo, polygon);
				if (currentPolygon.isPresent() && Objects.equals(polygon, currentPolygon.get())) {
					// This is the current edited polygon
					wwPolygon = Optional.of(oneWwPolygon);
				}
				addRenderable(oneWwPolygon);
			}
			assembleAttributes();
			geographicViewService.getWwd().redraw();
		});
	}

	/**
	 * Creates a list of Position from the polygon geometry
	 */
	public List<Position> convertGeometryToPositions(IPolygon polygon) {
		List<Position> result = new ArrayList<>();
		if (polygon != null) {
			double[][] lonlats = polygon.getGeometry().GetPoints();
			if (lonlats != null) {
				Arrays.stream(lonlats).map(lonlat -> Position.fromDegrees(lonlat[1], lonlat[0])).forEach(result::add);
			}
		}
		return result;
	}

	/**
	 * Creates a geometry from the list of Position
	 */
	private Geometry convertPositionsToGeometry(List<Position> positions) {
		Geometry geometry = IPolygon.makeEmptyGeometry();
		positions.forEach(p -> geometry.AddPoint_2D(p.longitude.degrees, p.latitude.degrees));
		return geometry;
	}

	/** Change the operation mode */
	private void switchTo(LayerMode mode) {
		layerMode = mode;

		// Refresh positions
		positions = convertGeometryToPositions(controller.getEditedPolygon().orElse(null));
		if (mode == LayerMode.CREATION && !positions.isEmpty()) {
			// If not empty, need at least 2 points
			if (positions.size() == 1) {
				positions.add(positions.get(0));
			}
			activeControlPoint = new ActiveControlPoint(positions.size() - 2);
			duplicateActiveControlPoint();
		} else {
			setNoActiveControlPoint();
		}

		createRenderables();
	}

	/** {@inheritDoc} */
	@Override
	protected void doRender(DrawContext dc) {
		if (refreshControlPoints.compareAndSet(true, false)) {
			assembleControlPoints(dc);
		}

		if (isEditing()) {
			markerRenderer.render(dc, controlPoints);
		}

		super.doRender(dc);
	}

	/** {@inheritDoc} */
	@Override
	protected void doPick(DrawContext dc, Point point) {
		doRender(dc); // Same logic for picking and rendering
	}

	private void updateControlPointIndex(Object topObject) {
		int i = 0;
		for (Marker marker : controlPoints) {
			ControlPointMarker controlPoint = (ControlPointMarker) marker;
			if (controlPoint.equals(topObject)) {
				activeControlPoint = new ActiveControlPoint(controlPoint, i);
				refreshControlPoints.set(true);
				geographicViewService.refresh();
				break;
			}
			i++;
		}
	}

	/** Recompute control points are re-computed each frame */
	private void assembleControlPoints(DrawContext dc) {
		controlPoints.clear();

		wwPolygon.ifPresent(p -> {
			if (p.getReferencePosition() != null) {

				Globe globe = geographicViewService.getWwd().getModel().getGlobe();
				int i = 0;
				for (var position : positions) {
					Vec4 vec = dc.computeTerrainPoint(position.getLatitude(), position.getLongitude(), 0d);
					Position vertexPosition = globe.computePositionFromPoint(vec);
					MarkerAttributes attributes = i == activeControlPoint.index ? highlightVertexControlAttributes
							: vertexControlAttributes;
					ControlPointMarker controlPointMarker = new ControlPointMarker(vertexPosition, vec, attributes, i);
					controlPoints.add(controlPointMarker);
					if (layerMode == LayerMode.CREATION && i == activeControlPoint.index) {
						activeControlPoint = new ActiveControlPoint(controlPointMarker, i);
					}
					i++;
				}
			}
		});

	}

	/**
	 * Moving the highlighted control point to the current mousePoint
	 */
	private void moveControlPoint(ControlPointMarker controlPoint) {
		getTerrainPosition().ifPresent(//
				pos -> {
					// First and last point must stay the same
					if (controlPoint.index == 0 || controlPoint.index == positions.size() - 1) {
						positions.set(0, pos);
						positions.set(positions.size() - 1, pos);
					} else {
						positions.set(controlPoint.index, pos);
					}
					// Notify controller
					firePositionsHaveChanged(positions);
				});
	}

	/**
	 * Remove a vertex from the polygon.
	 */
	private void removeControlPoint(ControlPointMarker controlPoint) {
		if (positions.size() <= 4) {
			return;
		}

		setNoActiveControlPoint();
		if (controlPoint.index == 0 || controlPoint.index == positions.size() - 1) {
			// Remove first and last
			positions.remove(0);
			positions.remove(positions.size() - 1);
			positions.add(positions.get(0));
		} else {
			positions.remove(controlPoint.index);
		}
		firePositionsHaveChanged(positions);
	}

	/**
	 * Add a vertex to the polygon's outer boundary.
	 */
	private void addVertex() {
		// Try to find the edge that is closest to a ray passing through the screen point. We're trying to determine
		// the user's intent as to which edge a new two control points should be added to.

		Line ray = geographicViewService.getWwd().getView().computeRayFromScreenPoint(mousePoint.getX(),
				mousePoint.getY());
		getTerrainPoint(ray).ifPresent(pickPoint -> {
			double nearestDistance = Double.MAX_VALUE;
			int newVertexIndex = 0;

			// Loop through the control points and determine which edge is closest to the pick point
			for (int i = 0; i < controlPoints.size(); i++) {
				ControlPointMarker thisMarker = (ControlPointMarker) controlPoints.get(i);
				ControlPointMarker nextMarker = (ControlPointMarker) controlPoints.get((i + 1) % controlPoints.size());

				Vec4 pointOnEdge = AirspaceEditorUtil.nearestPointOnSegment(thisMarker.point, nextMarker.point,
						pickPoint);
				if (!AirspaceEditorUtil.isPointBehindLineOrigin(ray, pointOnEdge)) {
					double d = pointOnEdge.distanceTo3(pickPoint);
					if (d < nearestDistance) {
						newVertexIndex = i + 1;
						nearestDistance = d;
					}
				}
			}

			// Add the new vertex
			Position newPosition = geographicViewService.getWwd().getModel().getGlobe()
					.computePositionFromPoint(pickPoint);
			positions.add(newVertexIndex, newPosition);
			firePositionsHaveChanged(positions);
		});

	}

	/** Handles the TOPIC_POLYGON_FILE_CHANGED event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPolygonFileChanged(
			@UIEventTopic(PolygonTopics.TOPIC_POLYGON_FILE_CHANGED) PolygonFileInfo editedPolygonFile) {
		if (Objects.equals(polygonFileInfo, editedPolygonFile)) {
			refreshControlPoints.set(true);
			switchTo(controller.isCreationInProgress() ? LayerMode.CREATION : LayerMode.UPDATE);
		} else {
			switchTo(LayerMode.VISUALIZATION);
		}
	}

	/** Handles the TOPIC_POLYGON_CHANGED event */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPolygonChanged(@UIEventTopic(PolygonTopics.TOPIC_POLYGON_CHANGED) IPolygon polygon) {
		// Is this polygon managed by this layer ?
		for (Renderable renderable : getRenderables()) {
			if (renderable instanceof WWSurfacePolygon wwSurfacePolygon
					&& Objects.equals(wwSurfacePolygon.model, polygon)) {
				// Process in EDT to avoid conflict with WW repaiting
				SwingUtilities.invokeLater(() -> {
					positions = convertGeometryToPositions(polygon);
					wwSurfacePolygon.configure();
					refreshControlPoints.set(true);

					if (layerMode == LayerMode.CREATION && !controller.isCreationInProgress()) {
						switchTo(LayerMode.UPDATE);
					} else {
						createRenderables();
					}
				});
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public PolygonParameters getParameters() {
		return polygonParameters;
	}

	/** {@inheritDoc} */
	@Override
	public void setParameters(PolygonParameters parameters) {
		fitToParameters(parameters);
		sessionService.saveSession();
	}

	/** Change parameters */
	private void fitToParameters(PolygonParameters parameters) {
		polygonParameters = parameters;
		assembleAttributes();
		geographicViewService.refresh();
	}

	/**
	 * @return the position of the terrain under the mouse cursor
	 */
	private Optional<Position> getTerrainPosition() {
		return mousePoint != null ? getTerrainPosition(mousePoint.getX(), mousePoint.getY()) : Optional.empty();
	}

	/**
	 * Alert controller that positions have changed
	 */
	private void firePositionsHaveChanged(List<Position> newPositions) {
		positionsPublisher.onNext(convertPositionsToGeometry(newPositions));
	}

	/**
	 * @return the point of the terrain at the specified coordinates
	 * @param x the horizontal coordinate originating from the left side of <code>View's</code> projection plane.
	 * @param y the vertical coordinate originating from the top of <code>View's</code> projection plane.
	 */
	private Optional<Vec4> getTerrainPoint(Line ray) {
		Intersection[] intersection = geographicViewService.getWwd().getSceneController().getTerrain().intersect(ray);
		return Optional.ofNullable(
				intersection != null && intersection.length > 0 ? intersection[0].getIntersectionPoint() : null);
	}

	/**
	 * @return the position of the terrain at the specified coordinates
	 */
	private Optional<Position> getTerrainPosition(double x, double y) {
		Line ray = geographicViewService.getWwd().getView().computeRayFromScreenPoint(x, y);
		return getTerrainPoint(ray)
				.map(point -> geographicViewService.getWwd().getModel().getGlobe().computePositionFromPoint(point));
	}

	/**
	 * Called by the session to retrieve all the savable parameters of this layer<br>
	 *
	 * A layer implementing this method must also implement setSessionParameter to be able to retrieve the values of the
	 * parameters saved in session.
	 */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(polygonParameters.toTypedEntry());
	}

	/**
	 * Called by the session when the parameters have been restored.
	 */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(PolygonParameters.SESSION_KEY).ifPresent(this::fitToParameters);
	}

	/**
	 * @return the {@link #polygonFileInfo}
	 */
	@Override
	public PolygonFileInfo getPolygonFileInfo() {
		return polygonFileInfo;
	}

	/** Indicates if edition is active */
	public boolean isEditing() {
		return layerMode != LayerMode.VISUALIZATION;
	}

	/**
	 * Consider no ControlPointMarker is active
	 */
	private void setNoActiveControlPoint() {
		activeControlPoint = noActiveControlPoint;
		activeAction = MouseAction.NONE;
	}

	/**
	 * Consider no ControlPointMarker is active
	 */
	private void duplicateActiveControlPoint() {
		var newPositions = new ArrayList<>(positions);
		newPositions.add(activeControlPoint.index, newPositions.get(activeControlPoint.index));
		firePositionsHaveChanged(newPositions);
		activeControlPoint = new ActiveControlPoint(activeControlPoint.index + 1);
		activeAction = MouseAction.MOVE_VERTEX;
	}

	/**
	 * Prepare attributes to render vertex
	 */
	private void assembleAttributes() {
		vertexControlAttributes = new BasicMarkerAttributes();
		vertexControlAttributes.setMaterial(new Material(ColorUtils.convertGColorToAWT(polygonParameters.lineColor)));
		vertexControlAttributes.setOpacity(polygonParameters.opacity);
		highlightVertexControlAttributes = new BasicMarkerAttributes();
		highlightVertexControlAttributes.setMaterial(new Material(Color.ORANGE));
		highlightVertexControlAttributes.setOpacity(polygonParameters.opacity);

		var polygonAttributes = new BasicAirspaceAttributes();
		polygonAttributes.setOutlineMaterial(new Material(ColorUtils.convertGColorToAWT(polygonParameters.lineColor)));
		polygonAttributes.setDrawOutline(polygonParameters.drawOutline);
		polygonAttributes.setOutlineWidth(polygonParameters.lineWidth);
		polygonAttributes.setOutlineOpacity(polygonParameters.opacity);
		polygonAttributes.setDrawInterior(polygonParameters.fill);
		polygonAttributes.setInteriorOpacity(polygonParameters.opacity / 2f);
		polygonAttributes.setInteriorMaterial(new Material(ColorUtils.convertGColorToAWT(polygonParameters.fillColor)));

		renderables.stream()//
				.filter(WWSurfacePolygon.class::isInstance) //
				.map(WWSurfacePolygon.class::cast) //
				.forEach(p -> {
					p.setAttributes(polygonAttributes);
					p.setHighlightAttributes(polygonAttributes);
				});
	}

	/**
	 * Event handler for CONTEXT_MENU_EVENT_TOPIC (a click on context menu items)
	 */
	@org.eclipse.e4.core.di.annotations.Optional
	@Inject
	protected void onContextMenuEvt(
			@UIEventTopic(ContextMenuController.CONTEXT_MENU_EVENT_TOPIC) ContextMenuEvent evt) {
		Object source = evt.getSource();
		if (source instanceof WWSurfacePolygon wwSurfacePolygon
				&& wwSurfacePolygon.polygonFileInfo == polygonFileInfo) {
			if (WWSurfacePolygon.DELETE_ACTION_NAME.equals(evt.getName())) {
				controller.requestPolygonDeletion(wwSurfacePolygon.polygonFileInfo, wwSurfacePolygon.model);
			} else if (WWMarker.EDIT_ACTION_NAME.equals(evt.getName())) {
				controller.requestPolygonEdition(wwSurfacePolygon.polygonFileInfo, wwSurfacePolygon.model);
			}
		}
	}

	/** Manager of mouse actions */
	class MouseManager extends MouseAdapter {
		/** {@inheritDoc} */
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON3) {
				if (layerMode == LayerMode.CREATION) {
					finalizeTheCeation();
					e.consume();
				}
			} else if (e.getButton() == MouseEvent.BUTTON1) {
				if (layerMode == LayerMode.CREATION) {
					if (e.getClickCount() == 2) {
						finalizeTheCeation();
						e.consume();
					} else if (positions.isEmpty()) {
						// First point
						getTerrainPosition().ifPresent(position -> {
							firePositionsHaveChanged(new ArrayList<>(List.of(position, position, position)));
							activeControlPoint = new ActiveControlPoint(1);
							activeAction = MouseAction.MOVE_VERTEX;
						});
						e.consume();
					} else if (e.getClickCount() == 1) {
						// Position has been updated in mouveMoved
						// Just prepare the next point from the same position
						duplicateActiveControlPoint();
						e.consume();
					}
				} else if (e.getClickCount() == 2) {
					mousePoint = e.getPoint();
					Object topObject = getTopObject();
					if (topObject instanceof ControlPointMarker marker) {
						removeControlPoint(marker);
						e.consume();
					} else if (Optional.ofNullable(topObject).equals(wwPolygon)) {
						addVertex();
						e.consume();
					}
				}
			}

			// Avoid the goto to the clicked point
			if (layerMode == LayerMode.CREATION) {
				e.consume();
			}
		}

		/** {@inheritDoc} */
		@Override
		public void mouseDragged(MouseEvent e) {
			if (wwPolygon.isEmpty() || layerMode != LayerMode.UPDATE) {
				return;
			}
			mousePoint = e.getPoint();
			if (MouseAction.MOVE_VERTEX == activeAction) {
				activeControlPoint.controlPoint.ifPresent(cp -> {
					moveControlPoint(cp);
					e.consume();
				});
			}
		}

		/** {@inheritDoc} */
		@Override
		public void mousePressed(MouseEvent e) {
			if (e.getButton() != MouseEvent.BUTTON1) {
				return;
			}

			mousePoint = e.getPoint();

			if (layerMode == LayerMode.UPDATE) {
				Object topObject = null;
				PickedObjectList pickedObjects = geographicViewService.getWwd().getObjectsAtCurrentPosition();
				if (pickedObjects != null) {
					topObject = pickedObjects.getTopObject();
				}

				if (topObject instanceof ControlPointMarker marker) {
					activeControlPoint = activeControlPoint.withControlPoint(marker);
					activeAction = MouseAction.MOVE_VERTEX;
					updateControlPointIndex(topObject);
					e.consume();
				} else if (topObject == wwPolygon) {
					activeControlPoint = noActiveControlPoint;
					activeAction = MouseAction.NONE;
				}
			}
		}

		/** {@inheritDoc} */
		@Override
		public void mouseMoved(MouseEvent e) {
			mousePoint = e.getPoint();
			if (layerMode == LayerMode.CREATION) {
				mousePoint = e.getPoint();
				if (activeControlPoint != null && MouseAction.MOVE_VERTEX == activeAction) {
					activeControlPoint.controlPoint.ifPresent(cp -> {
						moveControlPoint(cp);
						e.consume();
					});
				}
			} else {
				Object topObject = getTopObject();
				if (topObject instanceof ControlPointMarker) {
					updateControlPointIndex(topObject);
				} else {
					activeControlPoint = noActiveControlPoint;
				}
				e.consume();
			}
		}

		private Object getTopObject() {
			PickedObjectList pickedObjects = geographicViewService.getWwd().getObjectsAtCurrentPosition();
			if (pickedObjects != null) {
				return pickedObjects.getTopObject();
			}
			return null;
		}

		/**
		 * Finalize the creation of the polygon if possible
		 */
		private void finalizeTheCeation() {
			Geometry geometry = IPolygon.makeEmptyGeometry();
			positions.forEach(p -> geometry.AddPoint_2D(p.longitude.degrees, p.latitude.degrees));
			controller.finalizeTheCreation(geometry);
		}
	}

	/** Current informations of the active control point */
	private static class ActiveControlPoint {
		public final Optional<ControlPointMarker> controlPoint;
		public final int index;

		/**
		 * Constructor
		 */
		public ActiveControlPoint(ControlPointMarker controlPoint, int index) {
			this.controlPoint = Optional.ofNullable(controlPoint);
			this.index = index;
		}

		/**
		 * Constructor
		 */
		public ActiveControlPoint(int index) {
			controlPoint = Optional.empty();
			this.index = index;
		}

		/**
		 * Make a new ActiveControlPoint with the specified ControlPointMarker for the same index
		 */
		public ActiveControlPoint withControlPoint(ControlPointMarker controlPoint) {
			return new ActiveControlPoint(controlPoint, index);
		}
	}

	/** Marker in WW window to represent one point of the edited polygon */
	private static class ControlPointMarker extends BasicMarker {
		/** Index of the represented point in the polygon */
		public final int index;
		/** Computed coordinates of the point of terrain */
		public final Vec4 point;

		public ControlPointMarker(Position position, Vec4 point, MarkerAttributes attrs, int index) {
			super(position, attrs);
			this.point = point;
			this.index = index;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		computeSubscriber.dispose();

		// WW listener management
		WorldWindowGLCanvas wwd = geographicViewService.getWwd();
		wwd.getInputHandler().removeMouseListener(mouseManager);
		wwd.getInputHandler().removeMouseMotionListener(mouseManager);

		super.dispose();
	}

	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {

		WorldWindowGLCanvas wwd = geographicViewService.getWwd();
		wwd.getInputHandler().removeMouseListener(mouseManager);
		wwd.getInputHandler().removeMouseMotionListener(mouseManager);
		if (enabled) {
			wwd.getInputHandler().addMouseListener(mouseManager);
			wwd.getInputHandler().addMouseMotionListener(mouseManager);
		}
		super.setEnabled(enabled);
	}

	/** Decorator of IPolygon */
	private class WWSurfacePolygon extends SurfacePolygon {

		/** Delete point identifier. */
		public static final String DELETE_ACTION_NAME = "DeleteAction";
		/** Move point identifier. */
		public static final String EDIT_ACTION_NAME = "EditAction";

		/** PolygonFileInfo edited in this layer */
		private final PolygonFileInfo polygonFileInfo;

		/** Decorated polygon */
		public final IPolygon model;

		/**
		 * Constructor
		 */
		public WWSurfacePolygon(PolygonFileInfo polygonFileInfo, IPolygon model) {
			this.polygonFileInfo = polygonFileInfo;
			this.model = model;
			configure();
		}

		/**
		 * Configure the SurfacePolygon according to the model
		 */
		public void configure() {
			setDragEnabled(false);
			setOuterBoundary(convertGeometryToPositions(model));
			setValue(AVKey.DISPLAY_NAME, model.getName());
			setValue(AVKey.ROLLOVER_TEXT,
					String.format("File : %s%nName :  %s", polygonFileInfo.getFilename(), model.getName()));
			setValue(ContextMenuController.CONTEXT_MENU_INFO_KEY, makeMenu());
		}

		public ContextMenuInfo makeMenu() {
			// context menu
			ContextMenuInfo contextMenu = new ContextMenuInfo(model.getName());
			ContextMenuItemInfo item = new ContextMenuItemInfo("Edit");
			item.setAction(new ContextMenuItemAction(EDIT_ACTION_NAME));
			contextMenu.addItem(item);
			item = new ContextMenuItemInfo("Delete");
			item.setAction(new ContextMenuItemAction(DELETE_ACTION_NAME));
			contextMenu.addItem(item);
			return contextMenu;
		}
	}
}
