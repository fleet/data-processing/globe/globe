/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.textures.parametersview;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Provide the parameter composite for DTM.
 */
@Component(name = "globe_viewer3d_parameters_texture_file_composite_factory", service = IParametersViewCompositeFactory.class)
public class TextureFileParamCompositeFactory implements IParametersViewCompositeFactory {

	/** Constructor */
	public TextureFileParamCompositeFactory() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		if (!(selection instanceof FileInfoNode)) {
			return Optional.empty();
		}
		IFileInfo fileInfo = ((FileInfoNode) selection).getFileInfo();

		if (fileInfo.getContentType() == ContentType.G3D_NETCDF_4) {
			return Optional.of(new TextureFileParamComposite(parent, (FileInfoNode) selection));
		}
		if (fileInfo.getContentType() == ContentType.SONAR_NETCDF_4) {
			@SuppressWarnings("resource")
			ISounderNcInfo ncInfo = (ISounderNcInfo) fileInfo;
			if (!ncInfo.getGridGroupNames().isEmpty() // Case of one texture by Grid_group
					|| ncInfo.getWcRxBeamCount() == 1// Case of one texture by Beam_group
			)
				return Optional.of(new TextureFileParamComposite(parent, (FileInfoNode) selection));
		}

		return Optional.empty();
	}

}
