package fr.ifremer.viewer3d.layers.textures.parametersview;

import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.application.event.WWLayerDescription;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.selection.WWTextureLayerSelectionSynchronizer;
import gov.nasa.worldwind.layers.Layer;

/**
 * Parameter view for DTM files.
 */
public class TextureFileParamComposite extends Composite {

	/** Global {@link WWFileLayerStoreModel} **/
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	@Inject
	private WWTextureLayerSelectionSynchronizer selectionSynchronizer;

	/** Inner parameter composite to display parameter of the selected layer **/
	private TextureParametersComposite<? extends IWWTextureLayer> textureParametersComposite;

	/** The file */
	private final IFileInfo fileInfo;
	/** {@link IWWTextureLayer} of the current {@link FileInfoNode} **/
	private List<IWWTextureLayer> layers;
	private ComboViewer comboViewer;
	private ISelectionChangedListener layerChangeListener = this::onLayerSelected;

	/** Button to enable selection synchronization */
	private Button groupSyncSelectionBtn;

	/**
	 * Constructor for {@link IWWTextureLayer}
	 */
	public TextureFileParamComposite(Composite parent, FileInfoNode fileInfoNode) {
		super(parent, SWT.NONE);
		fileInfo = fileInfoNode.getFileInfo();

		// injection to retrieve the fileLayerStoreModel
		ContextInitializer.inject(this);

		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		Group grpLayer = new Group(this, SWT.NONE);
		grpLayer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpLayer.setText(String.format("Layer of '%s'", fileInfoNode.getName()));
		grpLayer.setLayout(new GridLayout(1, false));

		// combo to select layer
		comboViewer = new ComboViewer(grpLayer, SWT.READ_ONLY);
		Combo combo = comboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return element instanceof IWWTextureLayer textureLayer ? textureLayer.getName()
						: ((WWLayerDescription) element).name();
			}
		});

		// Selection of some textures can be synchronized (EK80, synchronized on frequency),
		addTextureSelectionSynchronization(grpLayer);

		WWFileLayerStore layerStore = fileLayerStoreModel.get(fileInfo).orElseThrow();
		fitToLayerStore(layerStore);

		// init with first enabled layer
		layers.stream().filter(Layer::isEnabled).findFirst().ifPresent(enabledLayer -> {
			comboViewer.setSelection(new StructuredSelection(enabledLayer));
			updateComposite(enabledLayer);
		});

		// handle new selection of layer
		comboViewer.addSelectionChangedListener(layerChangeListener);

		fileLayerStoreModel.addListener(this::onLayerStoreModelChanged);
		addDisposeListener(i -> fileLayerStoreModel.removeListener(this::onLayerStoreModelChanged));
	}

	/** Refresh the view model */
	private void fitToLayerStore(WWFileLayerStore layerStore) {
		Object selection = comboViewer.getStructuredSelection().getFirstElement();
		String selectedlayerName = selection instanceof WWLayerDescription layerDesc ? layerDesc.name() : null;

		// fill combo with layers
		layers = layerStore.getLayers().stream().filter(IWWTextureLayer.class::isInstance)
				.map(IWWTextureLayer.class::cast).toList();

		// Model contains layer instance and names of optional layers
		var comboModel = new ArrayList<Object>(layers);
		comboModel.addAll(layerStore.getOptionalLayers());
		comboViewer.setInput(comboModel);

		if (selectedlayerName != null) {
			// Restoring the selection
			comboViewer.removeSelectionChangedListener(layerChangeListener);
			layers.stream().filter(layer -> layer.getName().equals(selectedlayerName)).findFirst().ifPresent(layer -> {
				comboViewer.setSelection(new StructuredSelection(layer));
				updateComposite(layer);
			});
			// handle new selection of layer
			comboViewer.addSelectionChangedListener(layerChangeListener);

		}

	}

	/** React to the change of the layer store */
	private void onLayerStoreModelChanged(WWFileLayerStoreEvent storeEvent) {
		if (storeEvent.getFileLayerStore().getFileInfo().equals(fileInfo)) {
			UIUtils.asyncExecSafety(comboViewer, () -> fitToLayerStore(storeEvent.getFileLayerStore()));
		}
	}

	/**
	 * Called when a new layer is selected in the combo.<br>
	 * First, collects optilnal layers and request their load<br>
	 * Then, as soon as the FileLayer model is completed with the new layers, synchronize the selection of all this
	 * layers of the same sync key
	 */
	private void onLayerSelected(SelectionChangedEvent event) {
		// selection is a Layer or the name of an optional layer
		Object selection = event.getStructuredSelection().getFirstElement();
		if (selection instanceof IWWTextureLayer textureLayer) {
			selectionSynchronizer.manageCheckStatus(textureLayer);
			updateComposite(textureLayer);
		} else
			selectionSynchronizer.manageCheckStatus(fileInfo, (WWLayerDescription) selection);
	}

	/**
	 * Updates inner parameter composite.
	 */
	private void updateComposite(IWWTextureLayer layer) {
		// dispose old composite
		if (textureParametersComposite != null)
			textureParametersComposite.dispose();

		// create new composite
		if (layer instanceof IWWPlayableMultiTextureLayer playableMultiTextureLayer)
			textureParametersComposite = new TextureParametersComposite<>(TextureFileParamComposite.this,
					playableMultiTextureLayer);
		else if (layer instanceof IWWPlayableTextureLayer playableTextureLayer)
			textureParametersComposite = new TextureParametersComposite<>(TextureFileParamComposite.this,
					playableTextureLayer);
		else if (layer instanceof IWWMultiTextureLayer multiTextureLayer)
			textureParametersComposite = new TextureParametersComposite<>(TextureFileParamComposite.this,
					multiTextureLayer);
		else {
			textureParametersComposite = new TextureParametersComposite<>(TextureFileParamComposite.this, layer);
			if (layer.hasTextureData()) {
				String syncLabel = layer.getTextureData().getSynchronizationLabel();
				if (!syncLabel.isEmpty()) {
					groupSyncSelectionBtn.setText("Also select layers of the same " + syncLabel);
					UIUtils.setVisible(groupSyncSelectionBtn, true);
				} else {
					UIUtils.setVisible(groupSyncSelectionBtn, false);
				}
			} else {
				UIUtils.setVisible(groupSyncSelectionBtn, false);
			}
		}

		textureParametersComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		// resize to display correctly the new composite
		setSize(getParent().getSize());
		getParent().pack();
	}

	/** Add a button to the composite to manage the synchronisation of texture selection */
	private void addTextureSelectionSynchronization(Group grpLayer) {
		groupSyncSelectionBtn = new Button(grpLayer, SWT.CHECK);
		groupSyncSelectionBtn.setText("Synchronize selection on...");
		BooleanPreferenceAttribute syncTextureLayerSelectionPref = Activator.getPluginParameters()
				.getSyncTextureLayerSelection();
		groupSyncSelectionBtn.setSelection(syncTextureLayerSelectionPref.getValue());
		groupSyncSelectionBtn.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			syncTextureLayerSelectionPref.setValue(groupSyncSelectionBtn.getSelection(), false);
			Activator.getPluginParameters().save();
		}));
		UIUtils.setVisible(groupSyncSelectionBtn, false);
	}
}
