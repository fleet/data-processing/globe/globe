package fr.ifremer.viewer3d.layers.textures.parametersview;

import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer.ComputableTextureParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureDataCache;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.AbstractParametersCompositeWithTabs;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerComposite;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdWidget;
import fr.ifremer.globe.ui.widget.threshold.IntThresholdModel;
import fr.ifremer.globe.ui.widget.threshold.IntThresholdWidget;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.sync.player.IndexedPlayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.AbstractTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWComputableTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWMultiTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWPlayableMultiTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWPlayableTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.texture.WWTextureSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Composite which contains all tools relative to water column features.
 */
public class TextureParametersComposite<T extends IWWTextureLayer> extends AbstractParametersCompositeWithTabs {

	/** Save the selected tab index **/
	private static int selectedTab;

	/** The layer parameters synchronizer get from Context **/
	private final AbstractTextureSynchronizer<T> layersSynchronizer;
	/** The player synchronizer get from Context **/
	private final Optional<IndexedPlayerSynchronizer> playerSynchronizer;

	/** Inner composites **/
	private ColorContrastWidget colorComposite;
	private FloatThresholdWidget valueThreshold;

	/** Pref of sync */
	private BooleanPreferenceAttribute syncTextureLayerWithCriteria = Activator.getPluginParameters()
			.getSyncTextureLayerWithCriteria();

	/**
	 * Constructor for {@link IWWTextureLayer}
	 */
	@SuppressWarnings("unchecked")
	public TextureParametersComposite(Composite parent, IWWTextureLayer layer) {
		super(parent);
		layersSynchronizer = (AbstractTextureSynchronizer<T>) ContextInitializer
				.getInContext(WWTextureSynchronizer.class);
		playerSynchronizer = Optional.empty();
		createSynchronizerGroup((T) layer);
		createOrUpdateContrastTab((T) layer);
		createFilterTab((T) layer);
		createPositionTab((T) layer);
		tabFolder.setSelection(selectedTab);
	}

	/**
	 * Constructor for {@link IWWComputableTextureLayer}
	 */
	@SuppressWarnings("unchecked")
	public TextureParametersComposite(Composite parent, IWWComputableTextureLayer layer) {
		super(parent);
		layersSynchronizer = (AbstractTextureSynchronizer<T>) ContextInitializer
				.getInContext(WWComputableTextureSynchronizer.class);
		playerSynchronizer = Optional.empty();
		createSynchronizerGroup((T) layer);
		createComputeTab(layer);
		createOrUpdateContrastTab((T) layer);
		createFilterTab((T) layer);
		tabFolder.setSelection(selectedTab);
	}

	/**
	 * Constructor for {@link IWWMultiTextureLayer}
	 */
	@SuppressWarnings("unchecked")
	public TextureParametersComposite(Composite parent, IWWMultiTextureLayer layer) {
		super(parent);
		layersSynchronizer = (AbstractTextureSynchronizer<T>) ContextInitializer
				.getInContext(WWMultiTextureSynchronizer.class);
		playerSynchronizer = Optional.empty();
		createSynchronizerGroup((T) layer);
		createLayerSelectionCombo(layer);
		createOrUpdateContrastTab((T) layer);
		createFilterTab((T) layer);
		tabFolder.setSelection(selectedTab);
	}

	/**
	 * Constructor for {@link IWWPlayableTextureLayer}
	 */
	@SuppressWarnings("unchecked")
	public TextureParametersComposite(Composite parent, IWWPlayableTextureLayer layer) {
		super(parent);
		layersSynchronizer = (AbstractTextureSynchronizer<T>) ContextInitializer
				.getInContext(WWPlayableTextureSynchronizer.class);

		playerSynchronizer = layer.getPlayer().getSyncKey()
				.map(k -> ContextInitializer.getInContext(IndexedPlayerSynchronizer.class));

		createSynchronizerGroup((T) layer);
		createPlayerTabWithSync(layer.getPlayer(), layer);
		createOrUpdateContrastTab((T) layer);
		createFilterTab((T) layer);
		createPositionTab((T) layer);
		tabFolder.setSelection(selectedTab);
	}

	/**
	 * Constructor for {@link IWWPlayableMultiTextureLayer}
	 *
	 * @wbp.parser.constructor
	 */
	@SuppressWarnings("unchecked")
	public TextureParametersComposite(Composite parent, IWWPlayableMultiTextureLayer layer) {
		super(parent);
		layersSynchronizer = (AbstractTextureSynchronizer<T>) ContextInitializer
				.getInContext(WWPlayableMultiTextureSynchronizer.class);
		playerSynchronizer = Optional.empty();
		createSynchronizerGroup((T) layer);
		createLayerSelectionCombo(layer);
		createPlayerTab(layer.getPlayer());
		createOrUpdateContrastTab((T) layer);
		createFilterTab((T) layer);
		tabFolder.setSelection(selectedTab);
	}

	/**
	 * Creates a combo box to select the texture data layer to display.
	 */
	@SuppressWarnings("unchecked")
	private void createLayerSelectionCombo(IWWMultiTextureLayer layer) {
		final ComboViewer viewer = new ComboViewer(this, SWT.READ_ONLY);
		viewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setInput(layer.getAvailableDataLayers());
		viewer.setSelection(new StructuredSelection(layer.getSelectedDataLayer()));
		viewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			layer.setSelectedDataLayer((String) selection.getFirstElement());
			// update color/contrast model with current layer parameters
			createOrUpdateContrastTab((T) layer);
			// update filter model with current layer parameters
			valueThreshold.update(getThesholdModel(layer));
		});
	}

	@Override
	protected void onSelectedTab(int selectedTab) {
		TextureParametersComposite.selectedTab = selectedTab;
	}

	/**
	 * Creates a group which contains the synchronizer.
	 */
	private void createSynchronizerGroup(T layer) {
		var grpSynchronization = new LayerParametersSynchronizerComposite<>(this, layersSynchronizer, layer);
		grpSynchronization.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		grpSynchronization.adaptToLayer(layer);
		grpSynchronization.setSynchronizationChangedListener(this::onSynchronizationChanged);

		// Allow the synchronization of texture layers of the same file (ignore SyncKey in that case)
		if (layer.hasTextureData() && !layer.getTextureData().getSynchronizationLabel().isEmpty()) {
			var syncBtn = new Button(grpSynchronization, SWT.CHECK);
			syncBtn.setText("Apply only to layers of the same " + layer.getTextureData().getSynchronizationLabel());
			syncBtn.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 4, 1));
			syncBtn.setSelection(syncTextureLayerWithCriteria.getValue());
			syncBtn.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
				syncTextureLayerWithCriteria.setValue(syncBtn.getSelection(), false);
				Activator.getPluginParameters().save();
			}));
		}

	}

	/**
	 * Creates a tab which contains all tools relatives to color and contrast features.
	 */
	private void createOrUpdateContrastTab(T layer) {
		ColorContrastModel colorContrastModel = new ColorContrastModelBuilder(layer.getDisplayParameters()).build();
		Optional<Range> minMaxSyncValues = layersSynchronizer.getMinMaxSyncValues(layer);

		if (colorComposite == null) {
			colorComposite = super.createContrastTab(colorContrastModel, minMaxSyncValues, //
					model -> {
						// update from composite
						WWTextureDisplayParameters param = layer.getDisplayParameters().withContrastAndColor(
								model.contrastMin(), model.contrastMax(), model.colorMapIndex(),
								model.invertColor(), model.interpolated(), model.opacity());
						setParametersToLayer(layer, param);
					});
		} else {
			colorComposite.setModel(colorContrastModel);
			colorComposite.setMinMaxSyncValues(minMaxSyncValues);
		}
	}

	/**
	 * @return the value {@link FloatThresholdModel} for the specified {@link IWWTextureLayer}.
	 */
	private FloatThresholdModel getThesholdModel(IWWTextureLayer layer) {
		WWTextureDisplayParameters param = layer.getDisplayParameters();
		return new FloatThresholdModel(param.getMinThreshold(), param.getMaxThreshold(), param.isThesholdEnabled(),
				param.getInitialMinContrast(), param.getInitialMaxContrast());
	}

	/**
	 * @return the value {@link FloatThresholdModel} for the specified {@link IWWTextureLayer}.
	 */
	private EnabledNumberModel getPositionModel(IWWTextureLayer layer) {
		WWTextureDisplayParameters param = layer.getDisplayParameters();
		return new EnabledNumberModel(param.verticalOffsetEnabled(), param.verticalOffset());
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 */
	private void createFilterTab(T layer) {
		valueThreshold = super.createFilterTab(getThesholdModel(layer), model -> {
			WWTextureDisplayParameters param = layer.getDisplayParameters().withThreshold(model.minValue,
					model.maxValue, model.enable);
			setParametersToLayer(layer, param);
		});
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 */
	private void createPositionTab(T layer) {
		super.createPositionTab(getPositionModel(layer), model -> {
			WWTextureDisplayParameters param = layer.getDisplayParameters().withVerticalOffset(model.enable,
					model.value.floatValue());
			setParametersToLayer(layer, param);
		});
	}

	/**
	 * Creates a tab containing all tools relatives to "compute" feature.
	 */
	private void createComputeTab(IWWComputableTextureLayer layer) {
		Composite container = createTab("Compute");
		Button btnNewButton = new Button(container, SWT.NONE);
		btnNewButton.setText("Compute");

		// create threshold value widget
		ComputableTextureParameters parameters = layer.getComputeParameters();
		IntThresholdModel initialParameters = new IntThresholdModel(parameters.min(), parameters.max(),
				parameters.isEnabled(), 0, 800);
		IntThresholdWidget thresholdWidget = new IntThresholdWidget("Beam index threshold", container,
				initialParameters);
		thresholdWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// response to event
		btnNewButton.addListener(SWT.Selection, e -> {
			try {
				IntThresholdModel model = thresholdWidget.buildModelFromWidget();
				layer.setComputeParameters(
						new ComputableTextureParameters(model.enable, model.minValue, model.maxValue));

				// Clean the texture cache to force the creation
				var textureDataCache = ContextInitializer.getInContext(IWWTextureDataCache.class);
				textureDataCache.delete(layer);

				layer.buildTextureRenderable();
			} catch (Exception e1) {
				Messages.openErrorMessage("Compute error", "Error while computing texture : " + e1.getMessage());
			}
		});
	}

	/**
	 * Creates a tab with a player.
	 */
	protected Composite createPlayerTabWithSync(IndexedPlayer player, IWWPlayableTextureLayer layer) {
		Composite container = createTab("Player");
		playerSynchronizer.ifPresent(syncer -> {
			var playerSyncComposite = new LayerParametersSynchronizerComposite<IWWPlayableTextureLayer>(container,
					syncer, layer);
			playerSyncComposite.setText("Synchronization with other players");
			playerSyncComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			playerSyncComposite.adaptToLayer(layer);
		});
		IndexedPlayerComposite playerComposite = new IndexedPlayerComposite(container, SWT.NONE, player);
		playerComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		return container;
	}

	/** The synchronization option has changed. Update the minMaxSyncValues in the ColorComposite */
	private void onSynchronizationChanged(T layer) {
		if (colorComposite != null) {
			Optional<Range> minMaxSyncValues = layersSynchronizer.getMinMaxSyncValues(layer);
			colorComposite.setMinMaxSyncValues(minMaxSyncValues);
		}
	}

	/** Set the DisplayParameters to the layer and perform specific synchronization */
	private void setParametersToLayer(T layer, WWTextureDisplayParameters param) {
		layer.setDisplayParameters(param);

		// By default, synchronize all layers
		Predicate<T> criteria = l -> true;
		if (syncTextureLayerWithCriteria.getValue().booleanValue()) {
			// Synchronize layers with the same sync key
			String syncKey = getSyncKey(layer);
			criteria = t -> getSyncKey(t).equals(syncKey);
		}
		layersSynchronizer.synchonizeWith(layer, criteria);
	}

	/** @return the synchronized key of the layer */
	private String getSyncKey(T layer) {
		return layer.hasTextureData() ? layer.getTextureData().getSynchronizationKey() : "";
	}

}
