package fr.ifremer.viewer3d.layers.textures;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;

/**
 * Texture with several layers.
 */
public class MultiTextureLayer extends TextureLayer implements IWWMultiTextureLayer {

	/** Available layers **/
	private final Set<String> availableDataLayers;

	/** {@link Function} to get {@link IWWTextureData} **/
	private Function<String, IWWTextureData> textureDataByLayer;

	/** Selected layer **/
	protected String selectedDataLayer;

	/** last selected layer (used for cache) **/
	protected String lastSelectedDataLayer;

	/** Display parameter by layer **/
	private final Map<String, WWTextureDisplayParameters> displayParametersByLayer;

	/**
	 * Constructor
	 */
	public MultiTextureLayer(String name, Map<String, WWTextureDisplayParameters> textureDisplayParametersByLayer) {
		super(name, textureDisplayParametersByLayer.values().iterator().next());
		displayParametersByLayer = textureDisplayParametersByLayer;

		// get available data layers
		availableDataLayers = textureDisplayParametersByLayer.keySet();
		selectedDataLayer = availableDataLayers.iterator().next();
	}

	/**
	 * Constructor
	 */
	public MultiTextureLayer(String name, Function<String, IWWTextureData> textureDataByLayer,
			Map<String, WWTextureDisplayParameters> textureDisplayParametersByLayer) {
		this(name, textureDisplayParametersByLayer);
		this.textureDataByLayer = textureDataByLayer;

		buildTextureRenderable();
	}

	/**
	 * @return the {@link IWWTextureData} for the specified data layer.
	 */
	private synchronized IWWTextureData getTextureData(String dataLayer) {
		if (!lastSelectedDataLayer.equals(dataLayer)) {
			lastSelectedDataLayer = selectedDataLayer;
			textureData = textureDataByLayer.apply(selectedDataLayer);
		}
		return textureData;
	}

	/*************** GETTERS & SETTERS ******************/

	@Override
	public IWWTextureData getTextureData() {
		return getTextureData(selectedDataLayer);
	}

	@Override
	public Set<String> getAvailableDataLayers() {
		return availableDataLayers;
	}

	@Override
	public String getSelectedDataLayer() {
		return selectedDataLayer;
	}

	@Override
	public void setSelectedDataLayer(String selectedDataLayer) {
		this.selectedDataLayer = selectedDataLayer;
		setDisplayParameters(displayParametersByLayer.get(selectedDataLayer));
		buildTextureRenderable();
	}

}
