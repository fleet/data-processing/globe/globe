package fr.ifremer.viewer3d.layers.textures.render;

import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.viewer3d.layers.textures.TextureLayer;
import fr.ifremer.viewer3d.layers.wc.IWCLayer;
import fr.ifremer.viewer3d.layers.wc.render.IWCPickedObject;
import gov.nasa.worldwind.geom.Position;

public class TexturePickedObject implements IWCPickedObject {

	/** Properties **/

	private final TextureLayer layer;

	private Position position;

	private float value = Float.NaN;

	private Instant date;

	private Map<String, String> metadata = Collections.emptyMap();

	/**
	 * Constructor
	 */
	public TexturePickedObject(TextureLayer layer) {
		super();
		this.layer = layer;
	}

	@Override
	public IWCLayer getLayer() {
		return layer;
	}

	@Override
	public int getId() {
		// try to find ID (= swath index) from metadata
		if (layer.getTextureData() != null) {
			Optional<Integer> optSwathIndex = WWTextureDataUtils.getSwathIndex(layer.getTextureData());
			if (optSwathIndex.isPresent())
				return optSwathIndex.get();
		}
		return layer instanceof IWWPlayableTextureLayer ? //
				((IWWPlayableTextureLayer) layer).getPlayer().getCurrent() : 0;
	}

	@Override
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@Override
	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	@Override
	public Optional<Instant> getDate() {
		return Optional.ofNullable(date);
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	@Override
	public Map<String, String> getMetadata() {
		return this.metadata;
	}

	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((layer == null) ? 0 : layer.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + Float.floatToIntBits(value);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TexturePickedObject other = (TexturePickedObject) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (layer == null) {
			if (other.layer != null)
				return false;
		} else if (!layer.equals(other.layer))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (Float.floatToIntBits(value) != Float.floatToIntBits(other.value))
			return false;
		return true;
	}
}