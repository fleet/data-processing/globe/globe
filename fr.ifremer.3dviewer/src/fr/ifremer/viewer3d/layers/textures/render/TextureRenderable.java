package fr.ifremer.viewer3d.layers.textures.render;

import java.awt.Color;
import java.awt.Point;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

import fr.ifremer.globe.ogl.renderer.jogl.buffers.FloatVertexBuffer;
import fr.ifremer.globe.ogl.util.GLUtils;
import fr.ifremer.globe.ogl.util.PaletteTexture;
import fr.ifremer.globe.ogl.util.ShaderStore;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.texture.TextureRenderer;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.textures.TextureLayer;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Triangle;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.util.WWMath;
import swingintegration.example.Platform;

/**
 * This class handles the rendering of a texture.
 */
public class TextureRenderable implements OrderedRenderable {
	private static final Logger LOGGER = LoggerFactory.getLogger(TextureRenderable.class);

	/**
	 * Render shader program. One shader program instance needed for each OGL context.
	 */
	private static final String TEXTURE_FRAG_PICKING_SHADER = "/shader/texturePicking.frag";
	private static final String TEXTURE_VERT_SHADER_120 = "/shader/texture_120.vert";
	private static final String TEXTURE_FRAG_SHADER_120 = "/shader/texture_120.frag";
	private static final String TEXTURE_FRAG_PICKING_SHADER_120 = "/shader/texturePicking_120.frag";
	private final ShaderStore shaderStore;
	private final ShaderStore pickingShaderStore;

	/** OpenGL texture */
	private Texture texture;

	/** Eye distance (used by {@link OrderedRenderable} **/
	private double eyeDistance;

	/** Color palette **/
	private PaletteTexture palette = new PaletteTexture();

	/** Reference center */
	protected Vec4 referenceCenter;

	/** Points buffer (x,y,z) */
	protected FloatVertexBuffer viewCoordinatesBuffer = new FloatVertexBuffer(3);

	/** Texture vectors buffer (s,t) */
	protected FloatVertexBuffer textureCoordinatesBuffer = new FloatVertexBuffer(2);

	/** Index buffer */
	protected IntBuffer indexBuffer;

	/** Parent {@link IWWTextureLayer} **/
	private TextureLayer layer;

	/** Model */
	private IWWTextureData data;

	/** Picking support */
	private final PickSupport pickSupport = new PickSupport();

	/** Current vertical offset used to display the loaded texture */
	private double verticalOffset;
	private double verticalExaggeration;

	/**
	 * Current GLContext
	 */
	private GL currentGL;

	/**
	 * Constructor
	 */
	public TextureRenderable(TextureLayer parentLayer, IWWTextureData data) {
		if (Platform.isMac()) {
			shaderStore = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, TEXTURE_VERT_SHADER_120, TEXTURE_FRAG_SHADER_120));
			pickingShaderStore = new ShaderStore(
					gl -> ShaderUtil.createShader(this, gl, TEXTURE_VERT_SHADER_120, TEXTURE_FRAG_PICKING_SHADER_120));
		} else {
			shaderStore = new ShaderStore(gl -> ShaderUtil.createShader(this, gl, TextureRenderer.TEXTURE_VERT_SHADER,
					TextureRenderer.TEXTURE_FRAG_SHADER));
			pickingShaderStore = new ShaderStore(gl -> ShaderUtil.createShader(this, gl,
					TextureRenderer.TEXTURE_VERT_SHADER, TEXTURE_FRAG_PICKING_SHADER));
		}
		this.data = data;
		layer = parentLayer;
	}

	/**
	 * Dispose
	 */
	public void dispose() {
		shaderStore.dispose();
		pickingShaderStore.dispose();
		palette.dispose();
		SwingUtilities.invokeLater(() -> {
			if (currentGL != null && currentGL.getContext().makeCurrent() != GLContext.CONTEXT_NOT_CURRENT) {
				if (texture != null) {
					texture.destroy(currentGL);
				}
				viewCoordinatesBuffer.delete(currentGL);
				textureCoordinatesBuffer.delete(currentGL);
			}
		});
	}

	/**
	 * Computes OpenGL texture and coordinate buffers.
	 */
	protected void load(DrawContext dc) {
		// get vertical exaggeration
		verticalExaggeration = Viewer3D.getWwd().getSceneController().getVerticalExaggeration();
		verticalOffset = getVerticalOffsetFromParameters();

		if (texture == null) { // we do not reallocate texture if not necessary
			// refresh texture
			int width = data.getWidth();
			int height = data.getHeight();

			// check texture size
			int maxTextureSize = dc.getGLRuntimeCapabilities().getMaxTextureSize();
			if (width > maxTextureSize)
				LOGGER.error("Texture width (= {}) over OpenGL maximum texture size (= {})", width, maxTextureSize);
			if (height > maxTextureSize)
				LOGGER.error("Texture height (= {}) over OpenGL maximum texture size (= {})", height, maxTextureSize);

			TextureData textureData = new TextureData(dc.getGL().getGLProfile(), GL.GL_ALPHA32F, width, height, 0,
					GL.GL_ALPHA, GL.GL_FLOAT, false, false, false, data.getBuffer(), null);

			texture = TextureIO.newTexture(textureData);
			GLUtils.checkGLError((GL2) dc.getGL()).ifPresent(
					error -> LOGGER.error("Error with {} load method: {} ", getClass().getSimpleName(), error));
		}

		// clear coordinate buffers
		viewCoordinatesBuffer.clear();
		textureCoordinatesBuffer.clear();
		referenceCenter = null;

		// associate 3D position with a position in texture
		List<IWWTextureCoordinate> coordinates = data.getCoordinates();
		viewCoordinatesBuffer.allocate(coordinates.size());
		textureCoordinatesBuffer.allocate(coordinates.size());
		for (IWWTextureCoordinate coord : coordinates) {
			Vec4 mpoint = dc.getGlobe()
					.computePointFromPosition(coord.getPosition(verticalOffset, verticalExaggeration));
			if (referenceCenter == null) {
				referenceCenter = mpoint;
			}
			mpoint = mpoint.subtract3(referenceCenter);
			// 3D view position
			viewCoordinatesBuffer.put(new float[] { (float) mpoint.x, (float) mpoint.y, (float) mpoint.z });
			// position in texture
			textureCoordinatesBuffer.put(new float[] { coord.getS(), coord.getT() });
		}
		indexBuffer = WWMath.computeIndicesForGridInterior(data.getCoordGridHeight(), data.getCoordGridWidth());
		currentGL = dc.getGL().getGL2();
		viewCoordinatesBuffer.sendToGpu(currentGL);
		textureCoordinatesBuffer.sendToGpu(currentGL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void render(DrawContext dc) {
		double newVerticalExaggeration = Viewer3D.getWwd().getSceneController().getVerticalExaggeration();

		if (texture == null || newVerticalExaggeration != verticalExaggeration
				|| verticalOffset != getVerticalOffsetFromParameters()) {
			load(dc);
		}

		try {
			GL2 gl = dc.getGL().getGL2();
			// Compute eye distance with first point, TODO : get the closest point ?
			Vec4 mpoint = dc.getGlobe().computePointFromPosition(
					data.getCoordinates().get(0).getPosition(verticalOffset, verticalExaggeration));
			eyeDistance = mpoint.distanceTo3(dc.getView().getEyePoint());

			// TODO to check
			gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
					| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for depth func
					| GL2.GL_TEXTURE_BIT // for texture env
					| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

			// Enable shader
			int shaderProgram = shaderStore.getShaderProgram(gl);
			gl.glUseProgram(shaderProgram);

			// Enable blending (transparency)
			gl.glEnable(GL.GL_BLEND);
			gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

			// Set shader parameters (opacity, contrast, threshold...)
			var textureDisplayParameters = layer.getDisplayParameters();
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"),
					(float) textureDisplayParameters.getOpacity());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minContrast"),
					textureDisplayParameters.getMinContrast());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxContrast"),
					textureDisplayParameters.getMaxContrast());
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "isThresholdEnabled"),
					textureDisplayParameters.isThesholdEnabled() ? 1 : 0);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minThreshold"),
					textureDisplayParameters.getMinThreshold());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxThreshold"),
					textureDisplayParameters.getMaxThreshold());

			// Palette texture is created here to avoid a bug at the first rendering: to
			// explain...
			Texture paletteTexture = palette.getColorPalette(gl.getGL2(), textureDisplayParameters.getColorMapIndex(),
					textureDisplayParameters.isColorMapReversed());

			// Bind texture values
			gl.glEnable(GL.GL_TEXTURE_2D);

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "texture"), 0);
			texture.bind(gl);

			// Texture filtering parameters (define interpolation)
			int filtering = textureDisplayParameters.isInterpolationEnabled() ? GL.GL_LINEAR : GL.GL_NEAREST;
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, filtering);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, filtering);

			// Texture wrap parameters : do not display texture outside bounds
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL2GL3.GL_CLAMP_TO_BORDER);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL2GL3.GL_CLAMP_TO_BORDER);

			// Bind color palette texture
			gl.glActiveTexture(GL.GL_TEXTURE1);
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "palette"), 1);
			paletteTexture.bind(gl);

			// Palette texture warp parameters (use limit color outside contrast limits)
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

			GLUtils.checkGLError(gl).ifPresent(
					error -> LOGGER.debug("Error with {} render method: {} ", getClass().getSimpleName(), error));

			draw(dc);

			// Disable shaders
			if (shaderProgram != 0)
				gl.glUseProgram(0);

			gl.glPopAttrib();
		} catch (GIOException e) {
			LOGGER.debug("Error while rendering texture : " + e.getMessage(), e);
		}
	}

	/**
	 * Pick method
	 */
	@Override
	public void pick(DrawContext dc, Point pickPoint) {
		if (texture == null || layer.getOpacity() == 0)
			return;

		pickSupport.clearPickList();
		pickSupport.beginPicking(dc);

		Color pickingColor = dc.getUniquePickColor();

		renderForPicking(dc, pickingColor);

		pickSupport.addPickableObject(pickingColor.getRGB(), new TexturePickedObject(layer));

		pickSupport.endPicking(dc);
		PickedObject picked = pickSupport.resolvePick(dc, pickPoint, layer);
		if (picked != null) {
			computePickedPositionAndValue(dc, pickPoint, picked);
		}
	}

	/**
	 * Renders the texture for picking (draws the texture and fills it with current picking color)
	 */
	private void renderForPicking(DrawContext dc, Color pickingColor) {
		try {
			GL2 gl = dc.getGL().getGL2();

			// Enable picking shaders
			int shaderProgram = pickingShaderStore.getShaderProgram(gl);
			gl.glUseProgram(shaderProgram);

			// Set shader parameters (opacity, contrast, threshold...)
			var textureDisplayParameters = layer.getDisplayParameters();
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "isThresholdEnabled"),
					textureDisplayParameters.isThesholdEnabled() ? 1 : 0);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minThreshold"),
					textureDisplayParameters.getMinThreshold());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxThreshold"),
					textureDisplayParameters.getMaxThreshold());
			gl.glUniform4f(gl.glGetUniformLocation(shaderProgram, "pickingColor"), //
					pickingColor.getRed() / 255f, pickingColor.getGreen() / 255f, pickingColor.getBlue() / 255f, 1);

			// Bind texture values
			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "texture"), 0);
			texture.bind(gl);

			GLUtils.checkGLError(gl).ifPresent(
					error -> LOGGER.debug("Error with {} render method: {} ", getClass().getSimpleName(), error));

			draw(dc);

			// Disable shaders
			if (shaderProgram != 0)
				gl.glUseProgram(0);

		} catch (GIOException e) {
			LOGGER.debug("Error while rendering texture : " + e.getMessage(), e);
		}
	}

	/**
	 * Draws the texture (triangulation with texture coordinates).
	 */
	private void draw(DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();

		// Set reference center
		dc.getView().pushReferenceCenter(dc, referenceCenter);

		// Enable vertex arrays
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

		// Set the vertex pointer to the vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, viewCoordinatesBuffer.getBufferName());
		gl.glVertexPointer(viewCoordinatesBuffer.getVerticeSize(), GL.GL_FLOAT, 0, 0);

		// texture
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, textureCoordinatesBuffer.getBufferName());
		gl.glTexCoordPointer(textureCoordinatesBuffer.getVerticeSize(), GL.GL_FLOAT, 0, 0);

		// Rendering
		gl.glDrawElements(GL.GL_TRIANGLE_STRIP, indexBuffer.capacity(), GL.GL_UNSIGNED_INT, indexBuffer);
		// Clean context
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);
		dc.getView().popReferenceCenter(dc);

		GLUtils.checkGLError(gl)
				.ifPresent(error -> LOGGER.debug("Error with {} draw method: {} ", getClass().getSimpleName(), error));
	}

	@Override
	public double getDistanceFromEye() {
		return eyeDistance; // avoid hiding others layers (dtm...) when the texture is transparent
	}

	/**
	 * Computes position and value of the current picking.
	 */
	private void computePickedPositionAndValue(DrawContext dc, Point pickPoint, PickedObject picked) {
		Line ray = dc.getView().computeRayFromScreenPoint(pickPoint.getX(), pickPoint.getY());

		IWWTextureCoordinate textureCoord2 = data.getCoordinates().get(indexBuffer.get(0));
		Vec4 p2 = dc.getGlobe()
				.computePointFromPosition(textureCoord2.getPosition(verticalOffset, verticalExaggeration));
		IWWTextureCoordinate textureCoord3 = data.getCoordinates().get(indexBuffer.get(1));
		Vec4 p3 = dc.getGlobe()
				.computePointFromPosition(textureCoord3.getPosition(verticalOffset, verticalExaggeration));

		int indexSize = indexBuffer.capacity();
		for (int i = 2; i < indexSize; i++) {
			// update triangle points
			IWWTextureCoordinate textureCoord1 = textureCoord2;
			textureCoord2 = textureCoord3;
			textureCoord3 = data.getCoordinates().get(indexBuffer.get(i));
			Vec4 p1 = p2;
			p2 = p3;
			p3 = dc.getGlobe()
					.computePointFromPosition(textureCoord3.getPosition(verticalOffset, verticalExaggeration));

			// step 1 : find intersection point
			Vec4 intersectionPoint = new Triangle(p1, p2, p3).intersect(ray);
			if (intersectionPoint == null)
				continue; // current triangle not intersected: move on next

			// step 2: if intersection point found, compute value
			Vec4 t0;
			Vec4 t1;
			float t0Value;
			float t1Value;
			if (textureCoord1.getT() != textureCoord2.getT() && textureCoord1.getS() == textureCoord2.getS()) {
				t0 = p1;
				t1 = p2;
				t0Value = textureCoord1.getT();
				t1Value = textureCoord2.getT();
			} else if (textureCoord1.getT() != textureCoord3.getT() && textureCoord1.getS() == textureCoord3.getS()) {
				t0 = p1;
				t1 = p3;
				t0Value = textureCoord1.getT();
				t1Value = textureCoord3.getT();
			} else {
				t0 = p2;
				t1 = p3;
				t0Value = textureCoord2.getT();
				t1Value = textureCoord3.getT();
			}
			Vec4 vecT = t1.subtract3(t0);
			// Vec4 vecproj = intersectionPoint.subtract3(t0).projectOnto3(vecT);
			// double t = t0Value + (t1Value - t0Value) * (vecproj.getLength3() / vecT.getLength3());
			// Could be done wit dot operations
			// double t = vecT.dot3(intersectionPoint.subtract3(t0))/vecT.dotSelf3();
			Vec4 vecprojOnT = intersectionPoint.subtract3(t0);
			double t = t0Value + (vecT.dot3(vecprojOnT) / vecT.dotSelf3()) * (t1Value - t0Value);

			// Compute S
			Vec4 s0;
			Vec4 s1;
			float s0Value;
			float s1Value;
			if (textureCoord1.getS() != textureCoord2.getS() && textureCoord1.getT() == textureCoord2.getT()) {
				s0 = p1;
				s1 = p2;
				s0Value = textureCoord1.getS();
				s1Value = textureCoord2.getS();
			} else if (textureCoord1.getS() != textureCoord3.getS() && textureCoord1.getT() == textureCoord3.getT()) {
				s0 = p1;
				s1 = p3;
				s0Value = textureCoord1.getS();
				s1Value = textureCoord3.getS();
			} else {
				s0 = p2;
				s1 = p3;
				s0Value = textureCoord2.getS();
				s1Value = textureCoord3.getS();
			}

			Vec4 vecS = s1.subtract3(s0);
			// Vec4 vecprojOnS = intersectionPoint.subtract3(s0).projectOnto3(vecS);
			// double s = s0Value + (vecprojOnS.getLength3() / vecS.getLength3()) * (s1Value - s0Value);
			// Could be done wit dot operations
			Vec4 vecprojOnS = intersectionPoint.subtract3(s0);
			double s = s0Value + (vecS.dot3(vecprojOnS) / vecS.dotSelf3()) * (s1Value - s0Value);


			// ... set value
			int row = (int) (t * data.getHeight());
			int col = (int) (s * data.getWidth());
			if (row >= 0 && row < data.getHeight() && col >= 0 && col < data.getWidth()) {
				float value = data.getValue(row, col);

				// check pixel visibility. If not displayed, move next
				if (Float.isNaN(value))
					continue;
				// step 3 : update picked object
				TexturePickedObject pickedObject = (TexturePickedObject) picked.getObject();
				pickedObject.setValue(value);

				// ... set position
				var pickedPosition = dc.getGlobe().computePositionFromPoint(intersectionPoint);

				pickedObject.setPosition(
						Position.fromDegrees(pickedPosition.latitude.degrees, pickedPosition.longitude.degrees,
								pickedPosition.elevation / verticalExaggeration));

				// ... set date
				data.getDate(row, col).ifPresent(pickedObject::setDate);

				// ... set metadata
				Map<String, String> metadata = new HashMap<>();
				metadata.putAll(data.getMetadata());
				metadata.putAll(data.getCellMetadata(row, col));

				pickedObject.setMetadata(metadata);
				return;
			}
		}
	}

	/**
	 * @return the vertical offset or 0f if not enabled
	 */
	protected double getVerticalOffsetFromParameters() {
		double vOffset = layer.getDisplayParameters().verticalOffsetEnabled()
				? layer.getDisplayParameters().verticalOffset()
				: 0.0;

		vOffset += data.getVerticalOffset();
		return vOffset;
	}

}