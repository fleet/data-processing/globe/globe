package fr.ifremer.viewer3d.layers.textures;

import java.util.Optional;
import java.util.function.Function;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;

/**
 * Computable texture layer.
 */
public class ComputableTextureLayer extends TextureLayer implements IWWComputableTextureLayer {

	/** {@link Function} to compute {@link IWWTextureData} **/
	private final Function<ComputableTextureParameters, IWWTextureData> dataComputer;

	/** Compute parameters **/
	protected ComputableTextureParameters computeParameters;

	/** Compute parameters **/
	protected ComputableTextureParameters lastComputeParameters;

	/**
	 * Constructor
	 */
	public ComputableTextureLayer(String name, Function<ComputableTextureParameters, IWWTextureData> dataComputer,
			WWTextureDisplayParameters textureDisplayParameters, ComputableTextureParameters defaultComputeParameters) {
		super(name, textureDisplayParameters);
		this.dataComputer = dataComputer;
		computeParameters = defaultComputeParameters;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.of(newName -> //
		new ComputableTextureLayer(newName, dataComputer, getDisplayParameters(), computeParameters));
	}

	/*************** GETTERS & SETTERS ******************/

	@Override
	public IWWTextureData getTextureData() {
		if (textureData == null || computeParameters != lastComputeParameters) {
			textureData = dataComputer.apply(computeParameters);
			lastComputeParameters = computeParameters;
		}
		return textureData;
	}

	@Override
	public ComputableTextureParameters getComputeParameters() {
		return computeParameters;
	}

	@Override
	public void setComputeParameters(ComputableTextureParameters computeParameters) {
		this.computeParameters = computeParameters;
	}

}
