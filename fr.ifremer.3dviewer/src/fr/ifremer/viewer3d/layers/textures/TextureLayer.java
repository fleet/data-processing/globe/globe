package fr.ifremer.viewer3d.layers.textures;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import jakarta.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.CapacityInspector;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.sync.marker.WWMarkerLayerSynchronizer;
import fr.ifremer.viewer3d.layers.textures.render.TextureRenderable;
import fr.ifremer.viewer3d.layers.wc.IWCLayer;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Texture layer : display a texture with a {@link TextureRenderable}.
 */
public class TextureLayer extends AbstractLayer implements IWWTextureLayer, IWCLayer {

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(TextureLayer.class);

	/** OpenGL max texture size **/
	private static final int GL_MAX_TEXTURE_SIZE = CapacityInspector.getMaxTextureSize();

	@Inject
	private ISessionService sessionService;
	@Inject
	private WWMarkerLayerSynchronizer markerLayerSyncrhonizer;

	/** Layer name **/
	private final String name;

	/** Display parameters **/
	private WWTextureDisplayParameters textureDisplayParameters;

	/** Render **/
	protected final List<TextureRenderable> textureRenderables = Collections.synchronizedList(new ArrayList<>());

	/** Cached {@link IWWTextureData} **/
	protected IWWTextureData textureData;

	/**
	 * Constructor used by child classes.
	 */
	protected TextureLayer(String name, WWTextureDisplayParameters displayParams) {
		this.name = name;
		textureDisplayParameters = displayParams;
		ContextInitializer.inject(this);
	}

	/**
	 * Constructor with a {@link TextureData}.
	 */
	public TextureLayer(String name, IWWTextureData textureData, WWTextureDisplayParameters displayParams) {
		this(name, displayParams);
		this.textureData = textureData;
		buildTextureRenderable();
	}

	@Override
	public void dispose() {
		clearTextureRenderables();
		super.dispose();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.of(newName -> new TextureLayer(newName, textureData, textureDisplayParameters));
	}

	/**
	 * Builds the {@link TextureRenderable}.
	 */
	@Override
	public void buildTextureRenderable() {
		clearTextureRenderables();
		synchronized (textureRenderables) {
			try {
				// getTextureData is called to load current data
				IWWTextureData currentTextureData = getTextureData();
				if (currentTextureData != null) {
					if (textureData.getWidth() > GL_MAX_TEXTURE_SIZE || textureData.getHeight() > GL_MAX_TEXTURE_SIZE) {
						// if texture size is over OpenGL limits : the texture data is split
						var textureDataMatrix = WWTextureDataUtils.split(currentTextureData, GL_MAX_TEXTURE_SIZE,
								GL_MAX_TEXTURE_SIZE);
						for (IWWTextureData[] element : textureDataMatrix)
							for (IWWTextureData element2 : element)
								textureRenderables.add(new TextureRenderable(this, element2));
					} else
						textureRenderables.add(new TextureRenderable(this, currentTextureData));

					firePropertyChange(DISPLAY_PARAMETERS_PROPERTY, null, textureDisplayParameters);
				}
			} catch (Exception ex) {
				// catch & log exceptions to avoid io.reactivex to crash (playable layers)
				LOGGER.error("Error while building texture renderable : " + ex.getMessage(), ex);
			}
		}
	}

	/**
	 * 
	 */
	private void clearTextureRenderables() {
		synchronized (textureRenderables) {
			textureRenderables.forEach(TextureRenderable::dispose);
			textureRenderables.clear();
		}
	}

	/**
	 * Renders texture.
	 */
	@Override
	protected void doRender(DrawContext dc) {
		synchronized (textureRenderables) {
			textureRenderables.forEach(textureRenderable -> textureRenderable.render(dc));
		}
	}

	@Override
	protected void doPick(DrawContext dc, Point point) {
		if (dc != null)
			synchronized (textureRenderables) {
				textureRenderables.forEach(dc::addOrderedRenderable);
			}
	}

	/*************** GETTERS & SETTERS ******************/

	@Override
	public String getName() {
		return name;
	}

	@Override
	public IWWTextureData getTextureData() {
		return textureData;
	}

	@Override
	public WWTextureDisplayParameters getDisplayParameters() {
		return textureDisplayParameters;
	}

	/**
	 * @param textureDisplayParameters the {@link #textureDisplayParameters} to set
	 */
	@Override
	public void setDisplayParameters(WWTextureDisplayParameters textureDisplayParameters) {
		boolean verticalOffsetChanged = this.textureDisplayParameters
				.verticalOffsetEnabled() != textureDisplayParameters.verticalOffsetEnabled()
				|| this.textureDisplayParameters.verticalOffset() != textureDisplayParameters.verticalOffset();

		// Retaining initial min and max values
		this.textureDisplayParameters = textureDisplayParameters.withInitialMinContrast(
				this.textureDisplayParameters.getInitialMinContrast(),
				this.textureDisplayParameters.getInitialMaxContrast());
		firePropertyChange(DISPLAY_PARAMETERS_PROPERTY, null, this.textureDisplayParameters);
		sessionService.saveSession();

		if (verticalOffsetChanged) {
			markerLayerSyncrhonizer.synchronizeWith(this, getVerticalOffset());
		}
	}

	/** @return the value of the vertical offset if enabled */
	@Override
	public float getVerticalOffset() {
		return textureDisplayParameters.verticalOffsetEnabled() ? textureDisplayParameters.verticalOffset() : 0f;
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(textureDisplayParameters.toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(WWTextureDisplayParameters.SESSION_KEY).ifPresent(param -> {
			textureDisplayParameters = param;

			// Synchronize the vertical offset of all markers
			markerLayerSyncrhonizer.synchronizeWith(this, getVerticalOffset());
		});
	}

	@Override
	public boolean hasTextureData() {
		return textureData != null;
	}

}
