package fr.ifremer.viewer3d.layers.textures.playable;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;

import jakarta.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.WWPlayableTextureInitParameters;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerData;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.layers.textures.TextureLayer;
import io.reactivex.disposables.Disposable;

/**
 * Texture with several slice: a player is used to select the one to display.
 */
public class PlayableTextureLayer extends TextureLayer implements IWWPlayableTextureLayer {

	static final Logger logger = LoggerFactory.getLogger(PlayableTextureLayer.class);

	@Inject
	private ISessionService sessionService;
	@Inject
	private IGlobalCursorService globalCursorService;
	/** Listener of GlobalCursor hooked to IGlobalCursorService */
	private final Consumer<GlobalCursor> globalCursorListener = this::onGlobalCursor;

	/** index **/
	private int currentIndex = -1;

	/** Player **/
	private final IndexedPlayer player;
	private final Disposable playerListener;
	private final List<Instant> instantOfIndex;

	/** {@link IntFunction} to get {@link IWWTextureData} **/
	private IntFunction<IWWTextureData> textureDataByIndex;

	/**
	 * Constructor
	 */
	public PlayableTextureLayer(WWPlayableTextureInitParameters playableTextureInitParameters,
			IntFunction<IWWTextureData> textureDataByIndex, WWTextureDisplayParameters textureDisplayParameters) {
		super(playableTextureInitParameters.name(), textureDisplayParameters);
		this.textureDataByIndex = textureDataByIndex;
		player = initPlayer(playableTextureInitParameters.sliceCount(), this::getTextureDataLabel);
		player.setSyncKey(playableTextureInitParameters.syncKey());
		instantOfIndex = playableTextureInitParameters.sliceTime();
		// Require to save the session when player index changed

		playerListener = player.subscribe(data -> sessionService.saveSession());
		buildTextureRenderable();

		globalCursorService.addListener(globalCursorListener);

	}

	/** {@inheritDoc} */
	@Override
	public Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.of(newName -> //
		new PlayableTextureLayer(//
				new WWPlayableTextureInitParameters(newName, //
						player.getData().maxLimit() + 1, // sliceCount
						instantOfIndex, //
						player.getSyncKey()), //
				textureDataByIndex, //
				getDisplayParameters()));
	}

	@Override
	public void dispose() {
		super.dispose();
		playerListener.dispose();
		player.dispose();
	}

	/**
	 * @return the {@link IWWTextureData} for the specified index.
	 */
	private synchronized IWWTextureData getTextureData(int index) {
		if (currentIndex != index) {
			currentIndex = index;
			textureData = textureDataByIndex.apply(currentIndex);
		}
		return textureData;
	}

	private String getTextureDataLabel(int index) {
		getTextureData(index);
		return textureData != null && textureData.getMetadata().containsKey(IWWTextureData.NAME_METADATA_KEY)
				? textureData.getMetadata().get(IWWTextureData.NAME_METADATA_KEY)
				: "";
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		var result = super.describeParametersForSession();
		result.put(player.getData().toTypedEntry());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		super.setSessionParameter(parameters);
		parameters.get(IndexedPlayerData.SESSION_KEY).ifPresent(player::setData);
	}

	/**
	 * Reacts to the reception of a GlobalCursor.<br>
	 * If a time is present, search the closest navigation point and set the index to the player
	 */
	private void onGlobalCursor(GlobalCursor globalcursor) {

		if (instantOfIndex.isEmpty())
			return; // No date time available

		if (globalcursor.syncTexturePlayers()) {
			globalcursor.time().ifPresent(time -> {
				Instant startDate = instantOfIndex.get(0);
				Instant endDate = instantOfIndex.get(instantOfIndex.size() - 1);
				if (DateUtils.between(time, startDate, endDate)) {
					int closestIndex = DateUtils.binarySearch(time, instantOfIndex.size(), instantOfIndex::get);
					if (closestIndex >= 0 && closestIndex <= player.getData().maxLimit())
						player.setCurrent(closestIndex);
					else
						logger.warn("GlobalCursor ignored, index {} is out of range", closestIndex);
				}
			});
		}

	}

	/*************** GETTERS & SETTERS ******************/

	@Override
	public IWWTextureData getTextureData() {
		return getTextureData(player.getCurrent());
	}

	@Override
	public IndexedPlayer getPlayer() {
		return player;
	}
}
