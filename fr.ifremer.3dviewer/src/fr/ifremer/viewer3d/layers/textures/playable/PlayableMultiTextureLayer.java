package fr.ifremer.viewer3d.layers.textures.playable;

import java.util.Map;
import java.util.function.BiFunction;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.viewer3d.layers.textures.MultiTextureLayer;

/**
 * Texture with several layers and slices.
 */
public class PlayableMultiTextureLayer extends MultiTextureLayer implements IWWPlayableMultiTextureLayer {

	/** index **/
	private int currentIndex = -1;

	/** selected layer **/
	private String currentSelectedDataLayer = "";

	/** Player **/
	private final IndexedPlayer player;

	private final BiFunction<String, Integer, IWWTextureData> textureDataByLayerAndIndex;

	/**
	 * Constructor
	 */
	public PlayableMultiTextureLayer(String name, int sliceCount,
			BiFunction<String, Integer, IWWTextureData> textureDataByLayerAndIndex,
			Map<String, WWTextureDisplayParameters> textureDisplayParametersByLayer) {
		super(name, textureDisplayParametersByLayer);
		this.textureDataByLayerAndIndex = textureDataByLayerAndIndex;
		this.player = initPlayer(sliceCount, this::getTextureDataLabel);
		buildTextureRenderable();
	}

	@Override
	public void dispose() {
		super.dispose();
		player.dispose();
	}

	/**
	 * @return the {@link IWWTextureData} for the specified index and texture layer.
	 */
	private synchronized IWWTextureData getTextureData(int index, String dataLayer) {
		if (currentIndex != index || !currentSelectedDataLayer.equals(dataLayer)) {
			currentIndex = index;
			currentSelectedDataLayer = selectedDataLayer;
			textureData = textureDataByLayerAndIndex.apply(currentSelectedDataLayer, currentIndex);
		}
		return textureData;
	}

	private String getTextureDataLabel(int index) {
		getTextureData(index, selectedDataLayer);
		return textureData != null && textureData.getMetadata().containsKey(IWWTextureData.NAME_METADATA_KEY)
				? textureData.getMetadata().get(IWWTextureData.NAME_METADATA_KEY)
				: "";
	}

	/*************** GETTERS & SETTERS ******************/

	@Override
	public IWWTextureData getTextureData() {
		return getTextureData(player.getCurrent(), selectedDataLayer);
	}

	@Override
	public IndexedPlayer getPlayer() {
		return player;
	}
}
