package fr.ifremer.viewer3d.layers;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerModel;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWGeoBoxLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWVerticalImageLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWallLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWaterColumnVolumicLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWwLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;
import fr.ifremer.globe.ui.widget.usbl.UsblLayerParameterComposite;
import fr.ifremer.viewer3d.layers.current.parametersview.CurrentParametersComposite;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.layers.geobox.parametersview.GeoBoxComposite;
import fr.ifremer.viewer3d.layers.navigation.parametersview.WWNavigationLayerParametersComposite;
import fr.ifremer.viewer3d.layers.pointcloud.parametersview.PointCloudParametersComposite;
import fr.ifremer.viewer3d.layers.swath.parametersview.WWSwathLayerParametersComposite;
import fr.ifremer.viewer3d.layers.terrain.parametersview.TerrainLayerParametersComposite;
import fr.ifremer.viewer3d.layers.terrain.parametersview.TerrainWithPaletteLayerParametersComposite;
import fr.ifremer.viewer3d.layers.textures.parametersview.TextureParametersComposite;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import fr.ifremer.viewer3d.layers.wc.parametersview.WCVolumicComposite;
import fr.ifremer.viewer3d.layers.wc.parametersview.deprecated.ContrastComposite;
import fr.ifremer.viewer3d.layers.wc.parametersview.deprecated.MyAbstractWallLayerComposite;
import fr.ifremer.viewer3d.layers.wc.parametersview.deprecated.WCComposite;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import gov.nasa.worldwind.layers.Layer;

@Component(name = "globe_viewer3d_worldwind_layer_parameters_composite_factory", service = IParametersViewCompositeFactory.class)
public class WWLayerParametersCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		if (selection instanceof ShaderElevationLayer)
			ShaderElevationLayer.setCurrent((ShaderElevationLayer) selection);

		if (selection instanceof Layer layer) {
			// composite for opacity, not needed for MultiMarkerLayer
			if (layer.getOpacity() != MyAbstractLayer.OPACITY_NOT_AVAILABLE //
					&& !(layer instanceof IWWTerrainLayer)//
					&& !(layer instanceof IWWTerrainWithPaletteLayer)//
					&& !(layer instanceof IWWTextureLayer)//
					&& !(layer instanceof IWWWaterColumnVolumicLayer) //
					&& !(layer instanceof IWWSwathSelectionLayer) //
					&& !(layer instanceof IWWNavigationLayer)//
					&& !(layer instanceof IWWPointCloudLayer)//
					&& !(layer instanceof IWWPolygonLayer)//
					&& !(layer instanceof IWWMarkerLayer)//
					&& !(layer instanceof IWWCurrentLayer) //
					&& !(layer instanceof IWWAnnotationLayer)//
					&& !(layer instanceof IWWUsblLayer)) {
				Composite opacityComposite = new OpacityComposite(parent, SWT.NONE, layer);
				opacityComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			}

		}

		if (selection instanceof IWWLayer wwLayer)
			return wwLayer.perform(new CompositeFactory(parent)).getCreatedComposite();

		return Optional.empty();
	}

	/** Visitor of Layer to instanciate the suitable parameters composite */
	public static class CompositeFactory extends BasicWwLayerOperation {
		private Composite parent;
		private Optional<Composite> createdComposite;

		/**
		 * Constructor
		 */
		public CompositeFactory(Composite parent) {
			this.parent = parent;
		}

		@Override
		public CompositeFactory accept(IWWTerrainLayer layer) {
			createdComposite = Optional.of(new TerrainLayerParametersComposite(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWTerrainWithPaletteLayer layer) {
			createdComposite = Optional.of(new TerrainWithPaletteLayerParametersComposite(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWTextureLayer layer) {
			createdComposite = Optional.of(new TextureParametersComposite<>(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWComputableTextureLayer layer) {
			createdComposite = Optional.of(new TextureParametersComposite<>(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWMultiTextureLayer layer) {
			createdComposite = Optional.of(new TextureParametersComposite<>(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWPlayableTextureLayer layer) {
			createdComposite = Optional.of(new TextureParametersComposite<>(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWPlayableMultiTextureLayer layer) {
			createdComposite = Optional.of(new TextureParametersComposite<>(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWWaterColumnVolumicLayer layer) {
			createdComposite = Optional.of(new WCVolumicComposite(parent, (WCVolumicLayer) layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWNavigationLayer layer) {
			createdComposite = Optional.of(new WWNavigationLayerParametersComposite(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWSwathSelectionLayer layer) {
			createdComposite = Optional.of(new WWSwathLayerParametersComposite(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWPointCloudLayer layer) {
			createdComposite = Optional.of(new PointCloudParametersComposite(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWUsblLayer layer) {
			createdComposite = Optional.of(new UsblLayerParameterComposite(parent, layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWCurrentLayer layer) {
			createdComposite = layer.getCurrentData().getVectorCount() > 0
					? Optional.of(new CurrentParametersComposite(parent, layer))
					: Optional.empty();
			return this;
		}

		/**
		 * Old XML water column layers (EP & EL).
		 */
		@Override
		public CompositeFactory accept(IWWWallLayer layer) {
			if (layer instanceof WaterColumnLayer) {
				createdComposite = Optional.of(new WCComposite(parent, SWT.NONE, (WaterColumnLayer) layer));
			} else if (layer instanceof MyAbstractWallLayer) {
				createdComposite = Optional
						.of(new MyAbstractWallLayerComposite(parent, SWT.NONE, (MyAbstractWallLayer) layer));
			}
			return this;
		}

		@Override
		public CompositeFactory accept(IWWVerticalImageLayer layer) {
			createdComposite = Optional.of(new ContrastComposite(parent, SWT.NONE, (MyAbstractLayer) layer));
			return this;
		}

		@Override
		public CompositeFactory accept(IWWGeoBoxLayer layer) {
			IGeoBoxLayerModel model = (IGeoBoxLayerModel) layer.getValue(IGeoBoxLayerModel.class.getName());
			if (model != null) {
				createdComposite = Optional.of(new GeoBoxComposite(parent, model));
			}
			return this;
		}

		/** By deault, no Composite is created */
		@Override
		public void performDefaultOperation(IWWLayer layer) {
			createdComposite = Optional.empty();
		}

		/**
		 * @return the {@link #createdComposite}
		 */
		public Optional<Composite> getCreatedComposite() {
			return createdComposite;
		}

	}

}
