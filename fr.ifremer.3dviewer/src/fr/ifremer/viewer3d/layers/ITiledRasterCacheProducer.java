package fr.ifremer.viewer3d.layers;

import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.data.DataStoreProducer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.LevelSet;

import org.w3c.dom.Document;

/**
 * This class aims to provide a way to a ShaderElevationLayer to regenerate elevation or textured tiles in the World Wind cache file format in the WW file store.
 * 
 * @author apside
 */
public interface ITiledRasterCacheProducer extends Disposable {

	/**
	 * This method is called by the layer in order to launch the production of cached tiles.
	 * 
	 * @param dc
	 *            The drawing context.
	 * @return The XML document containing cache metadata.
	 */
	Document produceCachedTiles(DrawContext dc);

	/**
	 * This method helps to keep track of levels read fro the data source during production of tiles.
	 * 
	 * @return All levels read from the source file.
	 */
	LevelSet getLevels();

	/**
	 * Get the underlying producer or <code>null</code> if the production has not been done yet.
	 * 
	 * @return The underlying producer responsible for tiles generation.
	 */
	DataStoreProducer getProducer();
}
