package fr.ifremer.viewer3d.layers.xml;

import fr.ifremer.viewer3d.loaders.SubLayer;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.cache.BasicMemoryCache;
import gov.nasa.worldwind.cache.GpuResourceCache;
import gov.nasa.worldwind.cache.MemoryCache;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Extent;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.SurfaceTile;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.Tile;
import gov.nasa.worldwind.util.TileKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

/***
 * Tile containing references to multiple textures </br> Selection according to
 * the {@link SubLayer} and the date
 * 
 * @author MORVAN
 * 
 */
public class MultiTextureTile extends Tile implements SurfaceTile {
	/***
	 * List of textures selected according to {@link SubLayer}
	 */
	private HashMap<Integer, HashMap<SubLayer, TextureData>> textureData = new HashMap<Integer, HashMap<SubLayer, TextureData>>(); // if
	// non-null,
	// then
	// must
	// be

	/***
	 * holds texture to use if own texture not available
	 */
	private MultiTextureTile fallbackTile = null;
	private Vec4 centroid; // Cartesian coordinate of lat/lon center
	private Extent extent = null; // bounding volume
	private Object globeStateKey;
	private boolean hasMipmapData = false;
	private long updateTime = 0;

	/***
	 * {@link TileKey} indexed by {@link SubLayer} and by date
	 * 
	 */
	private HashMap<SubLayer, HashMap<Integer, TileKey>> tileKeys = new HashMap<SubLayer, HashMap<Integer, TileKey>>();

	/***
	 * layer containing the tile
	 */
	private MultiTextureLayer layer;

	/***
	 * current date of the tile Used to compare with {@link MultiTextureLayer}
	 * date for reloading purpose
	 * 
	 * @see reloadAccordingToTheDate
	 */
	private int date;

	public MultiTextureTile(Sector sector, MultiTextureLayer layer) {
		super(sector);
		this.layer = layer;
	}

	public MultiTextureTile(Sector sector, Level level, int row, int col, MultiTextureLayer layer) {
		super(sector, level, row, col);
		this.layer = layer;
	}

	/**
	 * Returns the memory cache used to cache tiles for this class and its
	 * subclasses, initializing the cache if it doesn't yet exist.
	 * 
	 * @return the memory cache associated with the tile.
	 */
	public static synchronized MemoryCache getMemoryCache() {
		if (!WorldWind.getMemoryCacheSet().containsCache(MultiTextureTile.class.getName())) {
			long size = Configuration.getLongValue(AVKey.TEXTURE_IMAGE_CACHE_SIZE, 30000000L);
			MemoryCache cache = new BasicMemoryCache((long) (0.85 * size), size);
			cache.setName(MultiTextureTile.class.getName());
			WorldWind.getMemoryCacheSet().addCache(MultiTextureTile.class.getName(), cache);
		}

		return WorldWind.getMemoryCacheSet().getCache(MultiTextureTile.class.getName());
	}

	@Override
	public final long getSizeInBytes() {
		long size = super.getSizeInBytes();

		/*
		 * if (this.textureData != null) for(TextureData data :
		 * this.textureData.get(dateIndex).values()) size +=
		 * data.getEstimatedMemorySize();
		 */

		return size;
	}

	@Override
	public List<? extends LatLon> getCorners() {
		ArrayList<LatLon> list = new ArrayList<LatLon>(4);
		for (LatLon ll : this.getSector()) {
			list.add(ll);
		}

		return list;
	}

	public MultiTextureTile getFallbackTile() {
		if (fallbackTile == null) {
			return this;
		}
		return this.fallbackTile;
	}

	public void setFallbackTile(MultiTextureTile fallbackTile) {
		this.fallbackTile = fallbackTile;
	}

	/**
	 * Returns the texture data most recently specified for the tile. New
	 * texture data is typically specified when a new image is read, either
	 * initially or in response to image expiration.
	 * <p/>
	 * If texture data is non-null, a new texture is created from the texture
	 * data when the tile is next bound or otherwise initialized. The texture
	 * data field is then set to null. Subsequently setting texture data to be
	 * non-null causes a new texture to be created when the tile is next bound
	 * or initialized.
	 * 
	 * @return the texture data, which may be null.
	 */
	public HashMap<SubLayer, TextureData> getTextureData(int dateIndex) {
		return this.textureData.get(dateIndex);
	}

	/**
	 * Returns the texture data most recently specified for the tile and the
	 * subLayer. New texture data is typically specified when a new image is
	 * read, either initially or in response to image expiration.
	 * <p/>
	 * If texture data is non-null, a new texture is created from the texture
	 * data when the tile is next bound or otherwise initialized. The texture
	 * data field is then set to null. Subsequently setting texture data to be
	 * non-null causes a new texture to be created when the tile is next bound
	 * or initialized.
	 * 
	 * @return the texture data, which may be null.
	 */
	public TextureData getTextureData(SubLayer subLayer, int indexDate) {
		// first retrieve textureData
		// if date changed
		reloadAccordingToTheDate(indexDate);
		if (this.textureData != null && textureData.containsKey(indexDate)) {
			return textureData.get(indexDate).get(subLayer);
		}
		return null;
	}

	/***
	 * reload textureData if layer date has changed
	 */
	private void reloadAccordingToTheDate(int indexDate) {
		if (date == indexDate) {
			return;
		}

		// reload textures
		date = indexDate;
	}

	/**
	 * Specifies new texture data for the tile. New texture data is typically
	 * specified when a new image is read, either initially or in response to
	 * image expiration.
	 * <p/>
	 * If texture data is non-null, a new texture is created from the texture
	 * data when the tile is next bound or otherwise initialized. The texture
	 * data field is then set to null. Subsequently setting texture data to be
	 * non-null causes a new texture to be created when the tile is next bound
	 * or initialized.
	 * <p/>
	 * When a texture is created from the texture data, the texture data field
	 * is set to null to indicate that the data has been converted to a texture
	 * and its resources may be released.
	 * 
	 * @param textureData
	 *            the texture data, which may be null.
	 */
	public void setTextureData(HashMap<SubLayer, TextureData> textureData, int dateIndex) {
		this.textureData.put(dateIndex, textureData);
		boolean hasMipmap = true;
		if (textureData != null) {
			for (TextureData data : textureData.values()) {
				if (data.getMipmapData() == null) {
					// if one texture has no mipmap
					// don't use it
					hasMipmap = false;
				}
			}
		}
		this.hasMipmapData = hasMipmap;
	}

	public Texture getTexture(GpuResourceCache tc, SubLayer subLayer, int indexDate) {
		if (tc == null) {
			String message = Logging.getMessage("nullValue.TextureCacheIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		// return (Texture) tc.get(this.getTileKey(subLayer));
		return (Texture) tc.get(this.getTileKey(subLayer, indexDate));
	}

	public boolean isTextureInMemory(GpuResourceCache tc, SubLayer subLayer, int indexDate) {
		if (tc == null) {
			String message = Logging.getMessage("nullValue.TextureCacheIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		return this.getTexture(tc, subLayer, indexDate) != null || this.getTextureData(indexDate) != null;
	}

	public boolean isTextureInMemory(GpuResourceCache textureCache, List<SubLayer> subLayers, int indexDate) {

		for (SubLayer subLayer : subLayers) {
			if (this.getTexture(textureCache, subLayer, indexDate) == null && this.getTextureData(indexDate) == null) {
				return false;
			}
		}

		return true;
	}

	public boolean isTextureExpired() {
		return this.isTextureExpired(this.getLevel().getExpiryTime());
	}

	public boolean isTextureExpired(long expiryTime) {
		return this.updateTime > 0 && this.updateTime < expiryTime;
	}

	public void setTexture(GpuResourceCache tc, Texture texture, SubLayer subLayer, int indexDate) {
		if (tc == null) {
			String message = Logging.getMessage("nullValue.TextureCacheIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		tc.put(this.getTileKey(subLayer, indexDate), texture);
		this.updateTime = System.currentTimeMillis();

		// No more need for texture data; allow garbage collector and memory
		// cache to reclaim it.
		// This also signals that new texture data has been converted.
		this.textureData.get(indexDate).remove(subLayer);
		if (textureData.get(indexDate).size() == 0) {
			textureData.remove(indexDate);
		}
		this.updateMemoryCache(subLayer, indexDate);
	}

	public Vec4 getCentroidPoint(Globe globe) {
		if (globe == null) {
			String msg = Logging.getMessage("nullValue.GlobeIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (this.centroid == null) {
			LatLon c = this.getSector().getCentroid();
			this.centroid = globe.computePointFromPosition(c.getLatitude(), c.getLongitude(), 0);
		}

		return this.centroid;
	}

	@Override
	public Extent getExtent(DrawContext dc) {
		if (dc == null) {
			String msg = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (this.extent == null || !this.isExtentValid(dc)) {
			this.extent = Sector.computeBoundingCylinder(dc.getGlobe(), dc.getVerticalExaggeration(), this.getSector());
			this.globeStateKey = dc.getGlobe().getStateKey(dc);
			this.centroid = null;
		}

		return this.extent;
	}

	private boolean isExtentValid(DrawContext dc) {
		return !(dc.getGlobe() == null || this.globeStateKey == null) && this.globeStateKey.equals(dc.getGlobe().getStateKey(dc));
	}

	public MultiTextureTile[] createSubTiles(Level nextLevel) {
		if (nextLevel == null) {
			String msg = Logging.getMessage("nullValue.LevelIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}
		Angle p0 = this.getSector().getMinLatitude();
		Angle p2 = this.getSector().getMaxLatitude();
		Angle p1 = Angle.midAngle(p0, p2);

		Angle t0 = this.getSector().getMinLongitude();
		Angle t2 = this.getSector().getMaxLongitude();
		Angle t1 = Angle.midAngle(t0, t2);

		String nextLevelCacheName = nextLevel.getCacheName();
		int nextLevelNum = nextLevel.getLevelNumber();
		int row = this.getRow();
		int col = this.getColumn();

		MultiTextureTile[] subTiles = new MultiTextureTile[4];

		TileKey key = new TileKey(nextLevelNum, 2 * row, 2 * col, nextLevelCacheName);
		MultiTextureTile subTile = this.getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[0] = subTile;
		} else {
			subTiles[0] = new MultiTextureTile(new Sector(p0, p1, t0, t1), nextLevel, 2 * row, 2 * col, this.layer);
		}

		key = new TileKey(nextLevelNum, 2 * row, 2 * col + 1, nextLevelCacheName);
		subTile = this.getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[1] = subTile;
		} else {
			subTiles[1] = new MultiTextureTile(new Sector(p0, p1, t1, t2), nextLevel, 2 * row, 2 * col + 1, this.layer);
		}

		key = new TileKey(nextLevelNum, 2 * row + 1, 2 * col, nextLevelCacheName);
		subTile = this.getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[2] = subTile;
		} else {
			subTiles[2] = new MultiTextureTile(new Sector(p1, p2, t0, t1), nextLevel, 2 * row + 1, 2 * col, this.layer);
		}

		key = new TileKey(nextLevelNum, 2 * row + 1, 2 * col + 1, nextLevelCacheName);
		subTile = this.getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[3] = subTile;
		} else {
			subTiles[3] = new MultiTextureTile(new Sector(p1, p2, t1, t2), nextLevel, 2 * row + 1, 2 * col + 1, this.layer);
		}

		return subTiles;
	}

	private MultiTextureTile getTileFromMemoryCache(TileKey tileKey) {
		return (MultiTextureTile) getMemoryCache().getObject(tileKey);
	}

	private void updateMemoryCache(SubLayer subLayer, int indexDate) {
		if (this.getTileFromMemoryCache(this.getTileKey(subLayer, indexDate)) != null) {
			getMemoryCache().add(this.getTileKey(subLayer, indexDate), this);
		}
	}

	protected Texture initializeTexture(DrawContext dc, SubLayer subLayer, int indexDate) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		Texture t = this.getTexture(dc.getTextureCache(), subLayer, indexDate);
		// Return texture if found and there is no new texture data
		if (t != null && this.getTextureData(subLayer, indexDate) == null) {
			return t;
		}

		if (this.getTextureData(subLayer, indexDate) == null) // texture not in
		// cache yet
		// texture
		// data is null, can't initialize
		{
			// create a transparent texture to match missing texture
			return TextureIO.newTexture(MultiTextureLayer.getMissingTexture());
		}

		try {
			t = TextureIO.newTexture(this.getTextureData(indexDate).get(subLayer));
		} catch (Exception e) {
			// create a transparent texture to match missing texture
			return TextureIO.newTexture(MultiTextureLayer.getMissingTexture());
		}

		this.setTexture(dc.getTextureCache(), t, subLayer, indexDate);
		t.bind(dc.getGL());

		this.setTextureParameters(dc, t, subLayer);

		return t;
	}

	protected void setTextureParameters(DrawContext dc, Texture t, SubLayer subLayer) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		GL2 gl = dc.getGL().getGL2();

		// Use a mipmap minification filter when either of the following is
		// true:
		// a. The texture has mipmap data. This is typically true for formats
		// with embedded mipmaps, such as DDS.
		// b. The texture is setup to have GL automatically generate mipmaps.
		// This is typically true when a texture is
		// loaded from a standard image type, such as PNG or JPEG, and the
		// caller instructed JOGL to generate
		// mipmaps.
		// Additionally, the texture must be in the latitude range (-80, 80). We
		// do this to prevent seams that appear
		// between textures near the poles.
		//
		// TODO: remove the latitude range restriction if a better tessellator
		// fixes the problem.

		boolean useMipmapFilter = (this.hasMipmapData || t.isUsingAutoMipmapGeneration()) && this.getSector().getMaxLatitude().degrees < 80d && this.getSector().getMinLatitude().degrees > -80;

		// Set the texture minification filter. If the texture qualifies for
		// mipmaps, apply a minification filter that
		// will access the mipmap data using the highest quality algorithm. If
		// the anisotropic texture filter is
		// available, we will enable it. This will sharpen the appearance of the
		// mipmap filter when the textured
		// surface is at a high slope to the eye.
		if (useMipmapFilter) {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR_MIPMAP_LINEAR);

			// If the maximum degree of anisotropy is 2.0 or greater, then we
			// know this graphics context supports
			// the anisotropic texture filter.
			double maxAnisotropy = dc.getGLRuntimeCapabilities().getMaxTextureAnisotropy();
			if (dc.getGLRuntimeCapabilities().isUseAnisotropicTextureFilter() && maxAnisotropy >= 2.0) {
				gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT, (float) maxAnisotropy);
			}
		}
		// If the texture does not qualify for mipmaps, then apply a linear
		// minification filter.
		else {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
		}

		// Set the texture magnification filter to a linear filter. This will
		// blur the texture as the eye gets very
		// near, but this is still a better choice than nearest neighbor
		// filtering.
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);

		// Set the S and T wrapping modes to clamp to the texture edge. This way
		// no border pixels will be sampled by
		// either the minification or magnification filters.
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
	}

	/***
	 * Bind texture to current texture unit according to {@link SubLayer} and
	 * {@link MultiTextureLayer}'s DateIndex
	 * 
	 * @param dc
	 * @param subLayer
	 * @param indexDate
	 *            : insure that all tile will be loaded at accurate date
	 * @return true if texture binded
	 */
	public boolean bind(DrawContext dc, SubLayer subLayer, int indexDate) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		// Reinitialize texture if new texture data
		if (this.getTextureData(indexDate) != null) {
			Texture t = this.initializeTexture(dc, subLayer, indexDate);
			if (t != null) {
				return true; // texture was bound during initialization.
			}
		}

		Texture t = this.getTexture(dc.getTextureCache(), subLayer, indexDate);
		if (t == null) {
			layer.createRequestTask(this, indexDate);

			// TODO bad implement
			// low performances
			t = this.initializeTexture(dc, subLayer, indexDate);
			if (t != null) {
				return true;
			}
		}
		t = this.getTexture(dc.getTextureCache(), subLayer, indexDate);

		if (t == null && this.getFallbackTile() != null) {
			MultiTextureTile resourceTile = this.getFallbackTile();
			t = resourceTile.getTexture(dc.getTextureCache(), subLayer, indexDate);
			if (t == null) {
				t = resourceTile.initializeTexture(dc, subLayer, indexDate);
				if (t != null) {
					return true; // texture was bound during initialization.
				}
			}
		}

		if (t != null) {
			t.enable(dc.getGL());
			t.bind(dc.getGL());
		}

		return t != null;
	}

	private void applyResourceTextureTransform(DrawContext dc) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (this.getLevel() == null) {
			return;
		}

		int levelDelta = this.getLevelNumber() - this.getFallbackTile().getLevelNumber();
		if (levelDelta <= 0) {
			return;
		}

		double twoToTheN = Math.pow(2, levelDelta);
		double oneOverTwoToTheN = 1 / twoToTheN;

		double sShift = oneOverTwoToTheN * (this.getColumn() % twoToTheN);
		double tShift = oneOverTwoToTheN * (this.getRow() % twoToTheN);

		dc.getGL().getGL2().glTranslated(sShift, tShift, 0);
		dc.getGL().getGL2().glScaled(oneOverTwoToTheN, oneOverTwoToTheN, 1);
	}

	@Override
	@Deprecated
	/***
	 * Deprecated
	 * @see applyInternalTransform(DrawContext dc, SubLayer subLayer,
	 *		boolean textureIdentityActive)
	 */
	public void applyInternalTransform(DrawContext dc, boolean textureIdentityActive) {
		// use method below
	}

	/***
	 * Apply texture transform according to {@link SubLayer}
	 * 
	 * @param dc
	 * @param subLayer
	 * @param textureIdentityActive
	 */
	public void applyInternalTransform(DrawContext dc, SubLayer subLayer, boolean textureIdentityActive, int indexDate) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		Texture t = null;
		for (SubLayer s : layer.getNeededSubLayers()) {
			Texture tmp;
			if (this.getTextureData(indexDate) != null) {
				tmp = this.initializeTexture(dc, s, indexDate);
			} else {
				tmp = this.getTexture(dc.getTextureCache(), s, indexDate); // Use
				// the
				// tile's
				// texture
				// if
				// available
			}

			if (s.equals(subLayer)) {
				t = tmp;
			}
		}

		if (t != null) {
			if (t.getMustFlipVertically()) {
				GL2 gl = GLContext.getCurrent().getGL().getGL2();
				if (!textureIdentityActive) {
					gl.glMatrixMode(GL.GL_TEXTURE);
					gl.glLoadIdentity();
				}
				gl.glScaled(1, -1, 1);
				gl.glTranslated(0, -1, 0);
			}
			return;
		}

		// Use the tile's fallback texture if its primary texture is not
		// available.
		MultiTextureTile resourceTile = this.getFallbackTile();
		if (resourceTile == null) {
			return;
		}

		t = resourceTile.getTexture(dc.getTextureCache(), subLayer, indexDate);
		if (t == null && resourceTile.getTextureData(indexDate) != null) {
			t = resourceTile.initializeTexture(dc, subLayer, indexDate);
		}

		if (t == null) {
			return;
		}

		// Apply necessary transforms to the fallback texture.
		GL2 gl = GLContext.getCurrent().getGL().getGL2();
		if (!textureIdentityActive) {
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glLoadIdentity();
		}

		if (t.getMustFlipVertically()) {
			gl.glScaled(1, -1, 1);
			gl.glTranslated(0, -1, 0);
		}

		this.applyResourceTextureTransform(dc);
	}

	/***
	 * {@link MultiTextureLayer} containing the tile
	 * 
	 * @param layer
	 */
	public void setLayer(MultiTextureLayer layer) {
		this.layer = layer;
	}

	/***
	 * 
	 * @return {@link MultiTextureLayer} containing the tile
	 */
	public MultiTextureLayer getLayer() {
		return layer;
	}

	@Override
	@Deprecated
	/***
	 * multiTexture to bind
	 * @see bind(DrawContext dc, SubLayer subLayer)
	 */
	public boolean bind(DrawContext dc) {
		return false;
	}

	/***
	 * 
	 * @param subLayer
	 * @return the {@link TileKey} according to the {@link MultiTextureLayer}'s
	 *         date and the {@link SubLayer}
	 */
	public TileKey getTileKey(SubLayer subLayer, int indexDate) {
		HashMap<Integer, TileKey> keys = tileKeys.get(subLayer);
		if (keys == null) {
			// create keys for this SubLayer
			keys = new HashMap<Integer, TileKey>();
			tileKeys.put(subLayer, keys);
		}

		TileKey key;
		if (subLayer.isTimeIndexed()) {
			key = keys.get(indexDate);
		} else {
			key = keys.get(0);
		}

		if (key == null) {
			// change cache path to select the SubLayer and date
			// accurate pyramid
			PrivateAccessor.setField(this, "cacheName", Tile.class, subLayer.getTexture(indexDate));
			key = new TileKey(this);

			if (subLayer.isTimeIndexed()) {
				keys.put(indexDate, key);
			} else {
				keys.put(0, key);
			}
		}
		return key;
	}
}
