package fr.ifremer.viewer3d.layers.xml;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;

import javax.imageio.ImageIO;
import javax.xml.xpath.XPath;

import org.eclipse.core.runtime.IProgressMonitor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.geom.MovableSector;
import fr.ifremer.viewer3d.loaders.SubLayer;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.FileStore;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.ogc.wms.WMSCapabilities;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.retrieve.AbstractRetrievalPostProcessor;
import gov.nasa.worldwind.retrieve.HTTPRetriever;
import gov.nasa.worldwind.retrieve.Retriever;
import gov.nasa.worldwind.retrieve.RetrieverFactory;
import gov.nasa.worldwind.retrieve.URLRetriever;
import gov.nasa.worldwind.util.AbsentResourceList;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.SessionCacheUtils;
import gov.nasa.worldwind.util.Tile;
import gov.nasa.worldwind.util.TileKey;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/***
 * Abstract Layer used to manage and sort {@link MultiTextureTile}
 *
 * @author mmorvan
 * @see MultiTextureLayer
 *
 */
public abstract class AbstractMultiTextureTiledImageLayer extends AbstractMultiDataLayer {

	public AbstractMultiTextureTiledImageLayer(String name, int dateMin, int dateMax, int dateStep) {
		super(name, dateMin, dateMax, dateStep);
	}

	// ============== Tile Assembly ======================= //
	// ============== Tile Assembly ======================= //
	// ============== Tile Assembly ======================= //

	// Infrastructure
	// Infrastructure
	protected static final LevelComparer levelComparer = new LevelComparer();
	protected LevelSet levels = null;

	public MultiTextureTile[][] getTilesInSector(Sector sector, int levelNumber) {
		if (sector == null) {
			String msg = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		Level targetLevel = levels.getLastLevel();
		if (levelNumber >= 0) {
			for (int i = levelNumber; i < this.getLevels().getLastLevel().getLevelNumber(); i++) {
				if (levels.isLevelEmpty(i)) {
					continue;
				}

				targetLevel = levels.getLevel(i);
				break;
			}
		}

		// Collect all the tiles intersecting the input sector.
		LatLon delta = targetLevel.getTileDelta();
		LatLon origin = levels.getTileOrigin();
		final int nwRow = Tile.computeRow(delta.getLatitude(), sector.getMaxLatitude(), origin.getLatitude());
		final int nwCol = Tile.computeColumn(delta.getLongitude(), sector.getMinLongitude(), origin.getLongitude());
		final int seRow = Tile.computeRow(delta.getLatitude(), sector.getMinLatitude(), origin.getLatitude());
		final int seCol = Tile.computeColumn(delta.getLongitude(), sector.getMaxLongitude(), origin.getLongitude());

		int numRows = nwRow - seRow + 1;
		int numCols = seCol - nwCol + 1;
		MultiTextureTile[][] sectorTiles = new MultiTextureTile[numRows][numCols];

		for (int row = nwRow; row >= seRow; row--) {
			for (int col = nwCol; col <= seCol; col++) {
				TileKey key = new TileKey(targetLevel.getLevelNumber(), row, col, targetLevel.getCacheName());
				Sector tileSector = levels.computeSectorForKey(key);
				if (this instanceof MultiTextureLayer) {
					sectorTiles[nwRow - row][col - nwCol] = new MultiTextureTile(tileSector, targetLevel, row, col,
							(MultiTextureLayer) this);
				}
			}
		}

		return sectorTiles;
	}

	public void setLevelSet(LevelSet levelSet) {
		if (levelSet != null) {
			levels = levelSet;
		}
	}

	protected ArrayList<MultiTextureTile> topLevels;
	protected boolean forceLevelZeroLoads = false;
	protected boolean levelZeroLoaded = false;
	protected boolean retainLevelZeroTiles = false;
	protected String tileCountName;
	protected double detailHintOrigin = 2.8;
	protected double detailHint = 0;
	protected boolean useMipMaps = true;
	protected boolean useTransparentTextures = false;
	protected ArrayList<String> supportedImageFormats = new ArrayList<>();
	protected String textureFormat;

	public static String XML_KEY_UNIT = "Unit";
	// Global min/max ne sont plus necessaires
	// on sait calculer en unite de la donnee
	public static String XML_KEY_GlobalMinValue = "GlobalMin";
	public static String XML_KEY_GlobalMaxValue = "GlobalMax";
	public static String XML_KEY_MinValue = "ValMin";
	public static String XML_KEY_MaxValue = "ValMax";

	private static boolean highQualityDisplayOfTexture = false;

	protected final Object fileLock = new Object();

	// Layer resource properties.
	protected ScheduledExecutorService resourceRetrievalService;
	protected AbsentResourceList absentResources;
	protected static final int RESOURCE_ID_OGC_CAPABILITIES = 1;
	protected static final int DEFAULT_MAX_RESOURCE_ATTEMPTS = 3;
	protected static final int DEFAULT_MIN_RESOURCE_CHECK_INTERVAL = (int) 6e5; // 10
	// minutes
	protected static final int SERVICE_CAPABILITIES_RESOURCE_ID = 1;
	protected boolean serviceInitialized = false;

	/**
	 * The infos displayed by the popupmenu action Infos.
	 */
	protected AbstractInfos infos;

	/**
	 * Directory for binary data files.
	 */
	protected File binFolder;

	// Stuff computed each frame
	protected boolean atMaxResolution = false;
	protected PriorityBlockingQueue<Runnable> requestQ = new PriorityBlockingQueue<>(200);
	private double detailFactorForMovable = 1.0;

	@Override
	public Object setValue(String key, Object value) {
		Object out = super.setValue(key, value);

		// Offer it to the level set
		if (this.getLevels() != null) {
			this.getLevels().setValue(key, value);
		}

		return out;
	}

	@Override
	public Object getValue(String key) {
		Object value = super.getValue(key);

		return value != null ? value : this.getLevels().getValue(key); // see if
		// the
		// level
		// set
		// has
		// it
	}

	@Override
	public void setName(String name) {
		super.setName(name);
		tileCountName = this.getName() + " Tiles";
	}

	public boolean isForceLevelZeroLoads() {
		return forceLevelZeroLoads;
	}

	public void setForceLevelZeroLoads(boolean forceLevelZeroLoads) {
		this.forceLevelZeroLoads = forceLevelZeroLoads;
	}

	public boolean isRetainLevelZeroTiles() {
		return retainLevelZeroTiles;
	}

	public void setRetainLevelZeroTiles(boolean retainLevelZeroTiles) {
		this.retainLevelZeroTiles = retainLevelZeroTiles;
	}

	/**
	 * Indicates the layer's detail hint, which is described in {@link #setDetailHint(double)}.
	 *
	 * @return the detail hint
	 *
	 * @see #setDetailHint(double)
	 */
	public double getDetailHint() {
		return detailHint;
	}

	/**
	 * Modifies the default relationship of image resolution to screen resolution as the viewing altitude changes.
	 * Values greater than 0 cause imagery to appear at higher resolution at greater altitudes than normal, but at an
	 * increased performance cost. Values less than 0 decrease the default resolution at any given altitude. The default
	 * value is 0. Values typically range between -0.5 and 0.5.
	 * <p/>
	 * Note: The resolution-to-height relationship is defined by a scale factor that specifies the approximate size of
	 * discernable lengths in the image relative to eye distance. The scale is specified as a power of 10. A value of 3,
	 * for example, specifies that 1 meter on the surface should be distinguishable from an altitude of 10^3 meters
	 * (1000 meters). The default scale is 1/10^2.8, (1 over 10 raised to the power 2.8). The detail hint specifies
	 * deviations from that default. A detail hint of 0.2 specifies a scale of 1/1000, i.e., 1/10^(2.8 + .2) = 1/10^3.
	 * Scales much larger than 3 typically cause the applied resolution to be higher than discernable for the altitude.
	 * Such scales significantly decrease performance.
	 *
	 * @param detailHint the degree to modify the default relationship of image resolution to screen resolution with
	 *            changing view altitudes. Values greater than 1 increase the resolution. Values less than zero decrease
	 *            the resolution. The default value is 0.
	 */
	public void setDetailHint(double detailHint) {
		this.detailHint = detailHint;
	}

	public LevelSet getLevels() {
		return levels;
	}

	protected PriorityBlockingQueue<Runnable> getRequestQ() {
		return requestQ;
	}

	@Override
	public boolean isMultiResolution() {
		return this.getLevels() != null && this.getLevels().getNumLevels() > 1;
	}

	@Override
	public boolean isAtMaxResolution() {
		return atMaxResolution;
	}

	/**
	 * Returns the format used to store images in texture memory, or null if images are stored in their native format.
	 *
	 * @return the texture image format; null if images are stored in their native format.
	 *
	 * @see #setTextureFormat(String)
	 */
	public String getTextureFormat() {
		return textureFormat;
	}

	/**
	 * Specifies the format used to store images in texture memory, or null to store images in their native format.
	 * Suppported texture formats are as follows:
	 * <ul>
	 * <li><code>image/dds</code> - Stores images in the compressed DDS format. If the image is already in DDS format
	 * it's stored as-is.</li>
	 * </ul>
	 *
	 * @param textureFormat the texture image format; null to store images in their native format.
	 */
	public void setTextureFormat(String textureFormat) {
		this.textureFormat = textureFormat;
	}

	public boolean isUseMipMaps() {
		return useMipMaps;
	}

	public void setUseMipMaps(boolean useMipMaps) {
		this.useMipMaps = useMipMaps;
	}

	public boolean isUseTransparentTextures() {
		return useTransparentTextures;
	}

	public void setUseTransparentTextures(boolean useTransparentTextures) {
		this.useTransparentTextures = useTransparentTextures;
	}

	/**
	 * Specifies the time of the layer's most recent dataset update, beyond which cached data is invalid. If greater
	 * than zero, the layer ignores and eliminates any in-memory or on-disk cached data older than the time specified,
	 * and requests new information from the data source. If zero, the default, the layer applies any expiry times
	 * associated with its individual levels, but only for on-disk cached data. In-memory cached data is expired only
	 * when the expiry time is specified with this method and is greater than zero. This method also overwrites the
	 * expiry times of the layer's individual levels if the value specified to the method is greater than zero.
	 *
	 * @param expiryTime the expiry time of any cached data, expressed as a number of milliseconds beyond the epoch. The
	 *            default expiry time is zero.
	 *
	 * @see System#currentTimeMillis() for a description of milliseconds beyond the epoch.
	 */
	@Override
	public void setExpiryTime(long expiryTime) // Override this method to use
	// intrinsic level-specific
	// expiry times
	{
		super.setExpiryTime(expiryTime);

		if (expiryTime > 0) {
			levels.setExpiryTime(expiryTime); // remove this in sub-class
			// to use level-specific
			// expiry times
		}
	}

	public static boolean isHighQualityDisplayOfTexture() {
		return highQualityDisplayOfTexture;
	}

	public static void setHighQualityDisplayOfTexture(boolean highRes) {
		highQualityDisplayOfTexture = highRes;
	}

	protected boolean isTileVisible(DrawContext dc, MultiTextureTile tile) {
		// considers whether the tile should be rendered considering tectonic
		// movement
		Sector st = move(tile.getSector());
		// if(st instanceof MovableSector)
		// {
		// st = ((MovableSector) st).getEnglobingSector();
		// }
		if (isHighQualityDisplayOfTexture()) {
			// jma asks for better resolution
			// detail must be upgraded for better accuracy
			detailFactorForMovable = 1.8;
		} else {
			detailFactorForMovable = 1.0;
		}

		if (st instanceof MovableSector) {
			// st = ((MovableSector) st).getEnglobingSector();
			for (Sector englobingSt : ((MovableSector) st).getEnglobingSector()) {
				if (dc.getVisibleSector() == null || dc.getVisibleSector().intersects(englobingSt)) {
					return true;
				}
			}
		} else {
			return dc.getVisibleSector() == null || dc.getVisibleSector().intersects(st);
		}
		return false;
	}

	protected boolean meetsRenderCriteria(DrawContext dc, MultiTextureTile tile) {
		// considers whether the tile should be rendered considering tectonic
		// movement
		return levels.isFinalLevel(tile.getLevelNumber()) || !needToSplit(dc, move(tile.getSector()), tile.getLevel());
	}

	protected double getDetailFactor() {
		return (detailHintOrigin + this.getDetailHint()) * detailFactorForMovable;
	}

	protected boolean needToSplit(DrawContext dc, Sector sector, Level level) {
		Vec4[] corners = sector.computeCornerPoints(dc.getGlobe(), dc.getVerticalExaggeration());
		Vec4 centerPoint = sector.computeCenterPoint(dc.getGlobe(), dc.getVerticalExaggeration());

		// Get the eye distance for each of the sector's corners and its center.
		View view = dc.getView();
		double d1 = view.getEyePoint().distanceTo3(corners[0]);
		double d2 = view.getEyePoint().distanceTo3(corners[1]);
		double d3 = view.getEyePoint().distanceTo3(corners[2]);
		double d4 = view.getEyePoint().distanceTo3(corners[3]);
		double d5 = view.getEyePoint().distanceTo3(centerPoint);

		// Find the minimum eye distance. Compute cell height at the
		// corresponding point.
		double minDistance = d1;
		double cellHeight = corners[0].getLength3() * level.getTexelSize(); // globe
		// radius
		// x
		// radian
		// texel
		// size
		double texelSize = level.getTexelSize();
		if (d2 < minDistance) {
			minDistance = d2;
			cellHeight = corners[1].getLength3() * texelSize;
		}
		if (d3 < minDistance) {
			minDistance = d3;
			cellHeight = corners[2].getLength3() * texelSize;
		}
		if (d4 < minDistance) {
			minDistance = d4;
			cellHeight = corners[3].getLength3() * texelSize;
		}
		if (d5 < minDistance) {
			minDistance = d5;
			cellHeight = centerPoint.getLength3() * texelSize;
		}

		// Split when the cell height (length of a texel) becomes greater than
		// the specified fraction of the eye
		// distance. The fraction is specified as a power of 10. For example, a
		// detail factor of 3 means split when the
		// cell height becomes more than one thousandth of the eye distance.
		// Another way to say it is, use the current
		// tile if its cell height is less than the specified fraction of the
		// eye distance.
		//
		// NOTE: It's tempting to instead compare a screen pixel size to the
		// texel size, but that calculation is
		// window-size dependent and results in selecting an excessive number of
		// tiles when the window is large.

		return cellHeight > minDistance * Math.pow(10, -this.getDetailFactor());
	}

	@Override
	public Double getMinEffectiveAltitude(Double radius) {
		if (radius == null) {
			radius = Earth.WGS84_EQUATORIAL_RADIUS;
		}

		// Get the cell size for the highest-resolution level.
		double texelSize = this.getLevels().getLastLevel().getTexelSize();
		double cellHeight = radius * texelSize;

		// Compute altitude associated with the cell height at which it would
		// switch if it had higher-res levels.
		return cellHeight * Math.pow(10, this.getDetailFactor());
	}

	@Override
	public Double getMaxEffectiveAltitude(Double radius) {
		if (radius == null) {
			radius = Earth.WGS84_EQUATORIAL_RADIUS;
		}

		// Find first non-empty level. Compute altitude at which it comes into
		// effect.
		for (int i = 0; i < this.getLevels().getLastLevel().getLevelNumber(); i++) {
			if (levels.isLevelEmpty(i)) {
				continue;
			}

			// Compute altitude associated with the cell height at which it
			// would switch if it had a lower-res level.
			// That cell height is twice that of the current lowest-res level.
			double texelSize = levels.getLevel(i).getTexelSize();
			double cellHeight = 2 * radius * texelSize;

			return cellHeight * Math.pow(10, this.getDetailFactor());
		}

		return null;
	}

	protected boolean atMaxLevel(DrawContext dc) {
		Position vpc = dc.getViewportCenterPosition();
		if (dc.getView() == null || this.getLevels() == null || vpc == null) {
			return false;
		}

		if (!this.getLevels().getSector().contains(vpc.getLatitude(), vpc.getLongitude())) {
			return true;
		}

		Level nextToLast = this.getLevels().getNextToLastLevel();
		if (nextToLast == null) {
			return true;
		}

		Sector centerSector = nextToLast.computeSectorForPosition(vpc.getLatitude(), vpc.getLongitude(),
				levels.getTileOrigin());

		return this.needToSplit(dc, centerSector, nextToLast);
	}

	// ============== Rendering ======================= //
	// ============== Rendering ======================= //
	// ============== Rendering ======================= //

	@Override
	public void render(DrawContext dc) {
		atMaxResolution = this.atMaxLevel(dc);
		super.render(dc);
	}

	protected void checkTextureExpiration(DrawContext dc, ArrayList<MultiTextureTile> currentTiles, int indexDate) {
		for (MultiTextureTile tile : currentTiles) {
			if (tile.isTextureExpired()) {
				this.requestTexture(dc, tile, indexDate);
			}
		}
	}

	protected void setBlendingFunction(DrawContext dc) {
		// Set up a premultiplied-alpha blending function. Any texture read by
		// JOGL will have alpha-premultiplied color
		// components, as will any DDS file created by World Wind or the World
		// Wind WMS. We'll also set up the base
		// color as a premultiplied color, so that any incoming premultiplied
		// color will be properly combined with the
		// base color.

		GL2 gl = dc.getGL().getGL2();

		double alpha = this.getOpacity();
		gl.glColor4d(alpha, alpha, alpha, alpha);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
	}

	protected void sendRequests() {
		Runnable task = requestQ.poll();
		while (task != null) {
			if (!WorldWind.getTaskService().isFull()) {
				WorldWind.getTaskService().addTask(task);
			}
			task = requestQ.poll();
		}
	}

	@Override
	public boolean isLayerInView(DrawContext dc) {
		boolean result = super.isLayerInView(dc);
		if (result) {
			Sector visibleSector = dc.getVisibleSector();
			Sector levelSector = levels.getSector();
			Sector rotated = move(levelSector);
			if (rotated instanceof MovableSector) {
				for (Sector englobingSt : ((MovableSector) rotated).getEnglobingSector()) {
					if (((visibleSector == null) || englobingSt.intersects(visibleSector))) {
						result = true;
						break;
					}
				}

			} else {
				// consider whether the sector is visible considering tectonic rotation
				result = ((visibleSector == null) || rotated.intersects(visibleSector));
			}
		}
		return result;
	}

	protected Vec4 computeReferencePoint(DrawContext dc) {
		if (dc.getViewportCenterPosition() != null) {
			return dc.getGlobe().computePointFromPosition(dc.getViewportCenterPosition());
		}

		java.awt.geom.Rectangle2D viewport = dc.getView().getViewport();
		int x = (int) viewport.getWidth() / 2;
		for (int y = (int) (0.5 * viewport.getHeight()); y >= 0; y--) {
			Position pos = dc.getView().computePositionFromScreenPoint(x, y);
			if (pos == null) {
				continue;
			}

			return dc.getGlobe().computePointFromPosition(pos.getLatitude(), pos.getLongitude(), 0d);
		}

		return null;
	}

	protected Vec4 getReferencePoint(DrawContext dc) {
		return this.computeReferencePoint(dc);
	}

	protected static class LevelComparer implements Comparator<MultiTextureTile> {
		@Override
		public int compare(MultiTextureTile ta, MultiTextureTile tb) {
			int la = ta.getFallbackTile() == null ? ta.getLevelNumber() : ta.getFallbackTile().getLevelNumber();
			int lb = tb.getFallbackTile() == null ? tb.getLevelNumber() : tb.getFallbackTile().getLevelNumber();

			return la < lb ? -1 : la == lb ? 0 : 1;
		}
	}

	// **************************************************************//
	// ******************** Configuration *************************//
	// **************************************************************//

	/**
	 * Creates a configuration document for a TiledImageLayer described by the specified params. The returned document
	 * may be used as a construction parameter to {@link gov.nasa.worldwind.layers.BasicTiledImageLayer}.
	 *
	 * @param params parameters describing the TiledImageLayer.
	 *
	 * @return a configuration document for the TiledImageLayer.
	 */
	public static Document createTiledImageLayerConfigDocument(AVList params) {
		Document doc = WWXML.createDocumentBuilder(true).newDocument();

		Element root = WWXML.setDocumentElement(doc, "Layer");
		WWXML.setIntegerAttribute(root, "version", 1);
		WWXML.setTextAttribute(root, "layerType", "TiledImageLayer");

		createTiledImageLayerConfigElements(params, root);

		return doc;
	}

	/**
	 * Appends TiledImageLayer configuration parameters as elements to the specified context. This appends elements for
	 * the following parameters:
	 * <table>
	 * <tr>
	 * <th>Parameter</th>
	 * <th>Element Path</th>
	 * <th>Type</th>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#SERVICE_NAME}</td>
	 * <td>Service/@serviceName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#IMAGE_FORMAT}</td>
	 * <td>ImageFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#AVAILABLE_IMAGE_FORMATS}</td>
	 * <td>AvailableImageFormats/ImageFormat</td>
	 * <td>String array</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#FORCE_LEVEL_ZERO_LOADS}</td>
	 * <td>ForceLevelZeroLoads</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETAIN_LEVEL_ZERO_TILES}</td>
	 * <td>RetainLevelZeroTiles</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#TEXTURE_FORMAT}</td>
	 * <td>TextureFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_MIP_MAPS}</td>
	 * <td>UseMipMaps</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_TRANSPARENT_TEXTURES}</td>
	 * <td>UseTransparentTextures</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_CONNECT_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ConnectTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_READ_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ReadTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT}</td>
	 * <td>RetrievalTimeouts/StaleRequestLimit/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * </table>
	 * This also writes common layer and LevelSet configuration parameters by invoking
	 * {@link gov.nasa.worldwind.layers.AbstractLayer#createLayerConfigElements(gov.nasa.worldwind.avlist.AVList, org.w3c.dom.Element)}
	 * and
	 * {@link DataConfigurationUtils#createLevelSetConfigElements(gov.nasa.worldwind.avlist.AVList, org.w3c.dom.Element)}
	 * .
	 *
	 * @param params the key-value pairs which define the TiledImageLayer configuration parameters.
	 * @param context the XML document root on which to append TiledImageLayer configuration elements.
	 *
	 * @return a reference to context.
	 *
	 * @throws IllegalArgumentException if either the parameters or the context are null.
	 */
	public static Element createTiledImageLayerConfigElements(AVList params, Element context) {
		if (params == null) {
			String message = Logging.getMessage("nullValue.ParametersIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (context == null) {
			String message = Logging.getMessage("nullValue.ContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		XPath xpath = WWXML.makeXPath();

		// Common layer properties.
		AbstractLayer.createLayerConfigElements(params, context);

		// LevelSet properties.
		DataConfigurationUtils.createLevelSetConfigElements(params, context);

		// Service properties.
		// Try to get the SERVICE_NAME property, but default to "WWTileService".
		String s = AVListImpl.getStringValue(params, AVKey.SERVICE_NAME, "WWTileService");
		if (s != null && s.length() > 0) {
			// The service element may already exist, in which case we want to
			// append to it.
			Element el = WWXML.getElement(context, "Service", xpath);
			if (el == null) {
				el = WWXML.appendElementPath(context, "Service");
			}
			WWXML.setTextAttribute(el, "serviceName", s);
		}

		WWXML.checkAndAppendBooleanElement(params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE, context,
				"RetrievePropertiesFromService");

		// Image format properties.
		WWXML.checkAndAppendTextElement(params, AVKey.IMAGE_FORMAT, context, "ImageFormat");
		WWXML.checkAndAppendTextElement(params, AVKey.TEXTURE_FORMAT, context, "TextureFormat");

		Object o = params.getValue(AVKey.AVAILABLE_IMAGE_FORMATS);
		if (o != null && o instanceof String[]) {
			String[] strings = (String[]) o;
			if (strings.length > 0) {
				// The available image formats element may already exists, in
				// which case we want to append to it, rather
				// than create entirely separate paths.
				Element el = WWXML.getElement(context, "AvailableImageFormats", xpath);
				if (el == null) {
					el = WWXML.appendElementPath(context, "AvailableImageFormats");
				}
				WWXML.appendTextArray(el, "ImageFormat", strings);
			}
		}

		// Optional behavior properties.
		WWXML.checkAndAppendBooleanElement(params, AVKey.FORCE_LEVEL_ZERO_LOADS, context, "ForceLevelZeroLoads");
		WWXML.checkAndAppendBooleanElement(params, AVKey.RETAIN_LEVEL_ZERO_TILES, context, "RetainLevelZeroTiles");
		WWXML.checkAndAppendBooleanElement(params, AVKey.USE_MIP_MAPS, context, "UseMipMaps");
		WWXML.checkAndAppendBooleanElement(params, AVKey.USE_TRANSPARENT_TEXTURES, context, "UseTransparentTextures");
		WWXML.checkAndAppendDoubleElement(params, AVKey.DETAIL_HINT, context, "DetailHint");

		// Retrieval properties.
		if (params.getValue(AVKey.URL_CONNECT_TIMEOUT) != null || params.getValue(AVKey.URL_READ_TIMEOUT) != null
				|| params.getValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT) != null) {
			Element el = WWXML.getElement(context, "RetrievalTimeouts", xpath);
			if (el == null) {
				el = WWXML.appendElementPath(context, "RetrievalTimeouts");
			}

			WWXML.checkAndAppendTimeElement(params, AVKey.URL_CONNECT_TIMEOUT, el, "ConnectTimeout/Time");
			WWXML.checkAndAppendTimeElement(params, AVKey.URL_READ_TIMEOUT, el, "ReadTimeout/Time");
			WWXML.checkAndAppendTimeElement(params, AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, el,
					"StaleRequestLimit/Time");
		}

		// Bathyscope contrast properties
		Element el = WWXML.appendElementPath(context, "BathyscopeContrast");
		WWXML.checkAndAppendTextElement(params, AVKey.ELEVATION_EXTREMES_FILE, el, "FileName");
		WWXML.checkAndAppendTextElement(params, XML_KEY_UNIT, el, XML_KEY_UNIT);
		WWXML.checkAndAppendTextElement(params, XML_KEY_GlobalMinValue, el, XML_KEY_GlobalMinValue);
		WWXML.checkAndAppendTextElement(params, XML_KEY_GlobalMaxValue, el, XML_KEY_GlobalMaxValue);
		WWXML.checkAndAppendTextElement(params, XML_KEY_MinValue, el, XML_KEY_MinValue);
		WWXML.checkAndAppendTextElement(params, XML_KEY_MaxValue, el, XML_KEY_MaxValue);

		return context;
	}

	/**
	 * Parses TiledImageLayer configuration parameters from the specified DOM document. This writes output as key-value
	 * pairs to params. If a parameter from the XML document already exists in params, that parameter is ignored.
	 * Supported key and parameter names are:
	 * <table>
	 * <tr>
	 * <th>Parameter</th>
	 * <th>Element Path</th>
	 * <th>Type</th>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#SERVICE_NAME}</td>
	 * <td>Service/@serviceName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#IMAGE_FORMAT}</td>
	 * <td>ImageFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#AVAILABLE_IMAGE_FORMATS}</td>
	 * <td>AvailableImageFormats/ImageFormat</td>
	 * <td>String array</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#FORCE_LEVEL_ZERO_LOADS}</td>
	 * <td>ForceLevelZeroLoads</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETAIN_LEVEL_ZERO_TILES}</td>
	 * <td>RetainLevelZeroTiles</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#TEXTURE_FORMAT}</td>
	 * <td>TextureFormat</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_MIP_MAPS}</td>
	 * <td>UseMipMaps</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_TRANSPARENT_TEXTURES}</td>
	 * <td>UseTransparentTextures</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_CONNECT_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ConnectTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_READ_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ReadTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT}</td>
	 * <td>RetrievalTimeouts/StaleRequestLimit/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * </table>
	 * This also parses common layer and LevelSet configuration parameters by invoking
	 * {@link gov.nasa.worldwind.layers.AbstractLayer#getLayerConfigParams(org.w3c.dom.Element, gov.nasa.worldwind.avlist.AVList)}
	 * and
	 * {@link gov.nasa.worldwind.util.DataConfigurationUtils#getLevelSetConfigParams(org.w3c.dom.Element, gov.nasa.worldwind.avlist.AVList)}
	 * .
	 *
	 * @param domElement the XML document root to parse for TiledImageLayer configuration parameters.
	 * @param params the output key-value pairs which recieve the TiledImageLayer configuration parameters. A null
	 *            reference is permitted.
	 *
	 * @return a reference to params, or a new AVList if params is null.
	 *
	 * @throws IllegalArgumentException if the document is null.
	 */
	public static AVList getTiledImageLayerConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		XPath xpath = WWXML.makeXPath();

		// Common layer properties.
		AbstractLayer.getLayerConfigParams(domElement, params);

		// LevelSet properties.
		DataConfigurationUtils.getLevelSetConfigParams(domElement, params);

		// Service properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.SERVICE_NAME, "Service/@serviceName", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE,
				"RetrievePropertiesFromService", xpath);

		// Image format properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.IMAGE_FORMAT, "ImageFormat", xpath);
		WWXML.checkAndSetStringParam(domElement, params, AVKey.TEXTURE_FORMAT, "TextureFormat", xpath);
		WWXML.checkAndSetUniqueStringsParam(domElement, params, AVKey.AVAILABLE_IMAGE_FORMATS,
				"AvailableImageFormats/ImageFormat", xpath);

		// Optional behavior properties.
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.FORCE_LEVEL_ZERO_LOADS, "ForceLevelZeroLoads", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETAIN_LEVEL_ZERO_TILES, "RetainLevelZeroTiles", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_MIP_MAPS, "UseMipMaps", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_TRANSPARENT_TEXTURES, "UseTransparentTextures",
				xpath);
		WWXML.checkAndSetDoubleParam(domElement, params, AVKey.DETAIL_HINT, "DetailHint", xpath);
		WWXML.checkAndSetColorArrayParam(domElement, params, AVKey.TRANSPARENCY_COLORS, "TransparencyColors/Color",
				xpath);

		// Retrieval properties. Convert the Long time values to Integers,
		// because BasicTiledImageLayer is expecting
		// Integer values.
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_CONNECT_TIMEOUT,
				"RetrievalTimeouts/ConnectTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_READ_TIMEOUT,
				"RetrievalTimeouts/ReadTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT,
				"RetrievalTimeouts/StaleRequestLimit/Time", xpath);

		// Parse the legacy configuration parameters. This enables
		// TiledImageLayer to recognize elements from previous
		// versions of configuration documents.
		getLegacyTiledImageLayerConfigParams(domElement, params);

		return params;
	}

	/**
	 * Parses TiledImageLayer configuration parameters from previous versions of configuration documents. This writes
	 * output as key-value pairs to params. If a parameter from the XML document already exists in params, that
	 * parameter is ignored. Supported key and parameter names are:
	 * <table>
	 * <tr>
	 * <th>Parameter</th>
	 * <th>Element Path</th>
	 * <th>Type</th>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#TEXTURE_FORMAT}</td>
	 * <td>CompressTextures</td>
	 * <td>"image/dds" if CompressTextures is "true"; null otherwise</td>
	 * </tr>
	 * </table>
	 *
	 * @param domElement the XML document root to parse for legacy TiledImageLayer configuration parameters.
	 * @param params the output key-value pairs which recieve the TiledImageLayer configuration parameters. A null
	 *            reference is permitted.
	 *
	 * @return a reference to params, or a new AVList if params is null.
	 *
	 * @throws IllegalArgumentException if the document is null.
	 */
	protected static AVList getLegacyTiledImageLayerConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		XPath xpath = WWXML.makeXPath();

		Object o = params.getValue(AVKey.TEXTURE_FORMAT);
		if (o == null) {
			Boolean b = WWXML.getBoolean(domElement, "CompressTextures", xpath);
			if (b != null && b) {
				params.setValue(AVKey.TEXTURE_FORMAT, "image/dds");
			}
		}

		return params;
	}

	// ============== Image Composition ======================= //
	// ============== Image Composition ======================= //
	// ============== Image Composition ======================= //

	public List<String> getAvailableImageFormats() {
		return new ArrayList<>(supportedImageFormats);
	}

	public boolean isImageFormatAvailable(String imageFormat) {
		return imageFormat != null && supportedImageFormats.contains(imageFormat);
	}

	public String getDefaultImageFormat() {
		return supportedImageFormats.size() > 0 ? supportedImageFormats.get(0) : null;
	}

	protected void setAvailableImageFormats(String[] formats) {
		supportedImageFormats.clear();

		if (formats != null) {
			supportedImageFormats.addAll(Arrays.asList(formats));
		}
	}

	protected BufferedImage requestImage(MultiTextureTile tile, String mimeType)
			throws URISyntaxException, InterruptedIOException, MalformedURLException {
		String pathBase = tile.getPathBase();
		String suffix = WWIO.makeSuffixForMimeType(mimeType);
		String path = pathBase + suffix;
		File f = new File(path);
		URL url;
		if (f.isAbsolute() && f.exists()) {
			url = f.toURI().toURL();
		} else {
			url = this.getDataFileStore().findFile(path, false);
		}

		if (url == null) {
			return null;
		}

		if (WWIO.isFileOutOfDate(url, tile.getLevel().getExpiryTime())) {
			// The file has expired. Delete it.
			this.getDataFileStore().removeFile(url);
			String message = Logging.getMessage("generic.DataFileExpired", url);
			Logging.logger().fine(message);
		} else {
			try {
				File imageFile = new File(url.toURI());
				BufferedImage image = ImageIO.read(imageFile);
				if (image == null) {
					String message = Logging.getMessage("generic.ImageReadFailed", imageFile);
					throw new RuntimeException(message);
				}

				levels.unmarkResourceAbsent(tile);
				return image;
			} catch (InterruptedIOException e) {
				throw e;
			} catch (IOException e) {
				// Assume that something's wrong with the file and delete it.
				this.getDataFileStore().removeFile(url);
				levels.markResourceAbsent(tile);
				String message = Logging.getMessage("generic.DeletedCorruptDataFile", url);
				Logging.logger().info(message);
			}
		}

		return null;
	}

	protected void downloadImage(final MultiTextureTile tile, String mimeType, int timeout) throws Exception {
		if (this.getValue(AVKey.RETRIEVER_FACTORY_LOCAL) != null) {
			this.retrieveLocalImage(tile, mimeType, timeout);
		} else {
			// Assume it's remote.
			this.retrieveRemoteImage(tile, mimeType, timeout);
		}
	}

	protected void retrieveRemoteImage(final MultiTextureTile tile, String mimeType, int timeout) throws Exception {
		// TODO: apply retriever-factory pattern for remote retrieval case.
		final URL resourceURL = tile.getResourceURL(mimeType);
		if (resourceURL == null) {
			return;
		}

		Retriever retriever;

		String protocol = resourceURL.getProtocol();

		if ("http".equalsIgnoreCase(protocol) || "https".equalsIgnoreCase(protocol)) {
			retriever = new HTTPRetriever(resourceURL, new CompositionRetrievalPostProcessor(tile));
			retriever.setValue(URLRetriever.EXTRACT_ZIP_ENTRY, "true"); // supports
			// legacy
			// layers
		} else {
			String message = Logging.getMessage("layers.TextureLayer.UnknownRetrievalProtocol", resourceURL);
			throw new RuntimeException(message);
		}

		Logging.logger().log(java.util.logging.Level.FINE, "Retrieving " + resourceURL.toString());
		retriever.setConnectTimeout(10000);
		retriever.setReadTimeout(timeout);
		retriever.call();
	}

	protected void retrieveLocalImage(MultiTextureTile tile, String mimeType, int timeout) throws Exception {
		if (!WorldWind.getLocalRetrievalService().isAvailable()) {
			return;
		}

		RetrieverFactory retrieverFactory = (RetrieverFactory) this.getValue(AVKey.RETRIEVER_FACTORY_LOCAL);
		if (retrieverFactory == null) {
			return;
		}

		AVListImpl avList = new AVListImpl();
		avList.setValue(AVKey.SECTOR, tile.getSector());
		avList.setValue(AVKey.WIDTH, tile.getWidth());
		avList.setValue(AVKey.HEIGHT, tile.getHeight());
		avList.setValue(AVKey.FILE_NAME, tile.getPath());
		avList.setValue(AVKey.IMAGE_FORMAT, mimeType);

		Retriever retriever = retrieverFactory.createRetriever(avList, new CompositionRetrievalPostProcessor(tile));

		Logging.logger().log(java.util.logging.Level.FINE, "Locally retrieving " + tile.getPath());
		retriever.setReadTimeout(timeout);
		retriever.call();
	}

	public int computeLevelForResolution(Sector sector, double resolution) {
		if (sector == null) {
			String message = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		// Find the first level exceeding the desired resolution
		double texelSize;
		Level targetLevel = levels.getLastLevel();
		for (int i = 0; i < this.getLevels().getLastLevel().getLevelNumber(); i++) {
			if (levels.isLevelEmpty(i)) {
				continue;
			}

			texelSize = levels.getLevel(i).getTexelSize();
			if (texelSize > resolution) {
				continue;
			}

			targetLevel = levels.getLevel(i);
			break;
		}

		// Choose the level closest to the resolution desired
		if (targetLevel.getLevelNumber() != 0 && !levels.isLevelEmpty(targetLevel.getLevelNumber() - 1)) {
			Level nextLowerLevel = levels.getLevel(targetLevel.getLevelNumber() - 1);
			double dless = Math.abs(nextLowerLevel.getTexelSize() - resolution);
			double dmore = Math.abs(targetLevel.getTexelSize() - resolution);
			if (dless < dmore) {
				targetLevel = nextLowerLevel;
			}
		}

		Logging.logger().fine(Logging.getMessage("layers.TiledImageLayer.LevelSelection", targetLevel.getLevelNumber(),
				Double.toString(targetLevel.getTexelSize())));
		return targetLevel.getLevelNumber();
	}

	/**
	 * Create an image for the portion of this layer lying within a specified sector. The image is created at a
	 * specified aspect ratio within a canvas of a specified size. This returns the specified image if this layer has no
	 * content in the specified sector.
	 *
	 * @param sector the sector of interest.
	 * @param canvasWidth the width of the canvas.
	 * @param canvasHeight the height of the canvas.
	 * @param aspectRatio the aspect ratio, width/height, of the window. If the aspect ratio is greater or equal to one,
	 *            the full width of the canvas is used for the image; the height used is proportional to the inverse of
	 *            the aspect ratio. If the aspect ratio is less than one, the full height of the canvas is used, and the
	 *            width used is proportional to the aspect ratio.
	 * @param levelNumber the target level of the tiled image layer.
	 * @param mimeType the type of image to create, e.g., "png" and "jpg".
	 * @param abortOnError indicates whether to stop assembling the image if an error occurs. If false, processing
	 *            continues until all portions of the layer that intersect the specified sector have been added to the
	 *            image. Portions for which an error occurs will be blank.
	 * @param image if non-null, a {@link BufferedImage} in which to place the image. If null, a new buffered image is
	 *            created. The image must be the width and height specified in the <code>canvasWidth</code> and
	 *            <code>canvasHeight</code> arguments.
	 * @param timeout The amount of time to allow for reading the image from the server.
	 *
	 * @return image the assembled image, of size indicated by the <code>canvasWidth</code> and
	 *         <code>canvasHeight</code>. If the specified aspect ratio is one, all pixels contain values. If the aspect
	 *         ratio is greater than one, a full-width segment along the top of the canvas is blank. If the aspect ratio
	 *         is less than one, a full-height segment along the right side of the canvase is blank. If the
	 *         <code>image</code> argument was non-null, that buffered image is returned.
	 *
	 * @throws IllegalArgumentException if <code>sector</code> is null.
	 * @see ImageUtil#mergeImage(gov.nasa.worldwind.geom.Sector, gov.nasa.worldwind.geom.Sector, double,
	 *      java.awt.image.BufferedImage, java.awt.image.BufferedImage) ;
	 */
	public BufferedImage composeImageForSector(Sector sector, int canvasWidth, int canvasHeight, double aspectRatio,
			int levelNumber, String mimeType, boolean abortOnError, BufferedImage image, int timeout) throws Exception {
		if (sector == null) {
			String message = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (!levels.getSector().intersects(sector)) {
			Logging.logger().severe(
					Logging.getMessage("generic.SectorRequestedOutsideCoverageArea", sector, levels.getSector()));
			return image;
		}

		Sector intersection = levels.getSector().intersection(sector);

		if (levelNumber < 0) {
			levelNumber = levels.getLastLevel().getLevelNumber();
		} else if (levelNumber > levels.getLastLevel().getLevelNumber()) {
			Logging.logger().warning(Logging.getMessage("generic.LevelRequestedGreaterThanMaxLevel", levelNumber,
					levels.getLastLevel().getLevelNumber()));
			levelNumber = levels.getLastLevel().getLevelNumber();
		}

		int numTiles = 0;
		MultiTextureTile[][] tiles = this.getTilesInSector(intersection, levelNumber);
		for (MultiTextureTile[] row : tiles) {
			numTiles += row.length;
		}

		if (tiles.length == 0 || tiles[0].length == 0) {
			Logging.logger().severe(Logging.getMessage("layers.TiledImageLayer.NoImagesAvailable"));
			return image;
		}

		if (image == null) {
			image = new BufferedImage(canvasWidth, canvasHeight, BufferedImage.TYPE_INT_RGB);
		}

		double tileCount = 0;
		for (MultiTextureTile[] row : tiles) {
			for (MultiTextureTile tile : row) {
				if (tile == null) {
					continue;
				}

				BufferedImage tileImage;
				try {
					tileImage = this.getImage(tile, mimeType, timeout);
					Thread.sleep(1); // generates InterruptedException if thread
					// has been interupted

					if (tileImage != null) {
						ImageUtil.mergeImage(sector, tile.getSector(), aspectRatio, tileImage, image);
					}

					this.firePropertyChange(AVKey.PROGRESS, tileCount / numTiles, ++tileCount / numTiles);
				} catch (InterruptedException e) {
					throw e;
				} catch (InterruptedIOException e) {
					throw e;
				} catch (Exception e) {
					if (abortOnError) {
						throw e;
					}

					String message = Logging.getMessage("generic.ExceptionWhileRequestingImage", tile.getPath());
					Logging.logger().log(java.util.logging.Level.WARNING, message, e);
				}
			}
		}

		return image;
	}

	public long countImagesInSector(Sector sector) {
		long count = 0;
		for (int i = 0; i <= this.getLevels().getLastLevel().getLevelNumber(); i++) {
			if (!levels.isLevelEmpty(i)) {
				count += countImagesInSector(sector, i);
			}
		}
		return count;
	}

	public long countImagesInSector(Sector sector, int levelNumber) {
		if (sector == null) {
			String msg = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		Level targetLevel = levels.getLastLevel();
		if (levelNumber >= 0) {
			for (int i = levelNumber; i < this.getLevels().getLastLevel().getLevelNumber(); i++) {
				if (levels.isLevelEmpty(i)) {
					continue;
				}

				targetLevel = levels.getLevel(i);
				break;
			}
		}

		// Collect all the tiles intersecting the input sector.
		LatLon delta = targetLevel.getTileDelta();
		LatLon origin = levels.getTileOrigin();
		final int nwRow = Tile.computeRow(delta.getLatitude(), sector.getMaxLatitude(), origin.getLatitude());
		final int nwCol = Tile.computeColumn(delta.getLongitude(), sector.getMinLongitude(), origin.getLongitude());
		final int seRow = Tile.computeRow(delta.getLatitude(), sector.getMinLatitude(), origin.getLatitude());
		final int seCol = Tile.computeColumn(delta.getLongitude(), sector.getMaxLongitude(), origin.getLongitude());

		long numRows = nwRow - seRow + 1;
		long numCols = seCol - nwCol + 1;

		return numRows * numCols;
	}

	protected BufferedImage getImage(MultiTextureTile tile, String mimeType, int timeout) throws Exception {
		// Read the image from disk.
		BufferedImage image = this.requestImage(tile, mimeType);
		Thread.sleep(1); // generates InterruptedException if thread has been
		// interrupted
		if (image != null) {
			return image;
		}

		// Retrieve it from the net since it's not on disk.
		this.downloadImage(tile, mimeType, timeout);

		// Try to read from disk again after retrieving it from the net.
		image = this.requestImage(tile, mimeType);
		Thread.sleep(1); // generates InterruptedException if thread has been
		// interupted
		if (image == null) {
			String message = Logging.getMessage("layers.TiledImageLayer.ImageUnavailable", tile.getPath());
			throw new RuntimeException(message);
		}

		return image;
	}

	protected class CompositionRetrievalPostProcessor extends AbstractRetrievalPostProcessor {
		protected MultiTextureTile tile;

		public CompositionRetrievalPostProcessor(MultiTextureTile tile) {
			this.tile = tile;
		}

		@Override
		protected File doGetOutputFile() {
			String suffix = WWIO.makeSuffixForMimeType(this.getRetriever().getContentType());
			if (suffix == null) {
				Logging.logger()
						.severe(Logging.getMessage("generic.UnknownContentType", this.getRetriever().getContentType()));
				return null;
			}

			String path = tile.getPathBase();
			path += suffix;

			File f = new File(path);
			final File outFile = f.isAbsolute() ? f : getDataFileStore().newFile(path);
			if (outFile == null) {
				return null;
			}

			return outFile;
		}

		@Override
		protected boolean isDeleteOnExit(File outFile) {
			return outFile.getPath().contains(WWIO.DELETE_ON_EXIT_PREFIX);
		}

		@Override
		protected boolean overwriteExistingFile() {
			return true;
		}

		@Override
		protected void markResourceAbsent() {
			levels.markResourceAbsent(tile);
		}

		@Override
		protected void handleUnsuccessfulRetrieval() {
			// Don't mark the tile as absent because the caller may want to try
			// again.
		}
	}

	public void addInfo(String key, Object value) {
		infos.addInfo(key, value);
	}

	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	/**
	 * Sets folder for binary data for this layer.
	 */
	public void setBinFolder(File file) {
		binFolder = file;

	}

	protected static AVList getParamsFromDocument(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		getTiledImageLayerConfigParams(domElement, params);
		setFallbacks(params);

		return params;
	}

	protected static void setFallbacks(AVList params) {
		if (params.getValue(AVKey.LEVEL_ZERO_TILE_DELTA) == null) {
			Angle delta = Angle.fromDegrees(36);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, new LatLon(delta, delta));
		}

		if (params.getValue(AVKey.TILE_WIDTH) == null) {
			params.setValue(AVKey.TILE_WIDTH, 512);
		}

		if (params.getValue(AVKey.TILE_HEIGHT) == null) {
			params.setValue(AVKey.TILE_HEIGHT, 512);
		}

		if (params.getValue(AVKey.FORMAT_SUFFIX) == null) {
			params.setValue(AVKey.FORMAT_SUFFIX, ".dds");
		}

		if (params.getValue(AVKey.NUM_LEVELS) == null) {
			params.setValue(AVKey.NUM_LEVELS, 19); // approximately 0.1 meters
			// per pixel
		}

		if (params.getValue(AVKey.NUM_EMPTY_LEVELS) == null) {
			params.setValue(AVKey.NUM_EMPTY_LEVELS, 0);
		}
	}

	protected void forceTextureLoad(MultiTextureTile tile, int indexDate) {
		final URL textureURL = this.getDataFileStore().findFile(tile.getPath(), false);

		if (textureURL != null && !this.isTextureFileExpired(tile, textureURL, this.getDataFileStore())) {
			this.loadTexture(tile, textureURL, indexDate);
		}
	}

	protected void requestTexture(DrawContext dc, MultiTextureTile tile, int indexDate) {
		Vec4 centroid = tile.getCentroidPoint(dc.getGlobe());
		Vec4 referencePoint = this.getReferencePoint(dc);
		if (referencePoint != null) {
			tile.setPriority(centroid.distanceTo3(referencePoint));
		}

		RequestTask task = this.createRequestTask(tile, indexDate);
		this.getRequestQ().add(task);
	}

	protected RequestTask createRequestTask(MultiTextureTile tile, int indexDate) {
		if (this instanceof MultiTextureLayer) {
			return new RequestTask(tile, (MultiTextureLayer) this, indexDate);
		} else {
			return null;
		}
	}

	protected static class RequestTask implements Runnable, Comparable<RequestTask> {
		protected final MultiTextureLayer layer;
		protected final MultiTextureTile tile;
		protected final int indexDate;

		protected RequestTask(MultiTextureTile tile, MultiTextureLayer multiTextureLayer, int indexDate) {
			layer = multiTextureLayer;
			this.tile = tile;
			this.indexDate = indexDate;
		}

		@Override
		public void run() {
			// TODO: check to ensure load is still needed
			// AL -----------------------------
			// AL final java.net.URL textureURL =
			// WorldWind.getDataFileStore().findFile(tile.getPath(), false);
			java.net.URL textureURL = null;
			if (layer.getLevels().getFirstLevel().getFormatSuffix().compareTo(".bil") != 0) {
				textureURL = layer.getDataFileStore().findFile(tile.getPath(), false);

				if (textureURL == null) {
					File fileStoreLocation = WorldWindUtils.getInstallLocation();
					String path = fileStoreLocation.getAbsolutePath() + "/" + tile.getPath();
					try {
						textureURL = new URL("file:" + path);
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			// <BRL: Hack to use 4 digits elevation data. (where does they
			// come from ??!!)
			try {
				if (textureURL == null && tile.getResourceURL() == null
						&& layer.getLevels().getFirstLevel().getFormatSuffix().compareTo(".bil") != 0) {
					// FIXME avoiding tile.path with PrivateAccessor...
					Level lvl = tile.getLevel();
					PrivateAccessor.setField(tile, "path", Tile.class,
							lvl.getPath() + "/" + String.format("%04d", tile.getRow()) + "/"
									+ String.format("%04d", tile.getRow()) + "_"
									+ String.format("%04d", tile.getColumn()));
					if (!lvl.isEmpty()) {
						PrivateAccessor.setField(tile, "path", Tile.class, tile.getPath() + lvl.getFormatSuffix());
					}
					textureURL = layer.getDataFileStore().findFile(tile.getPath(), false);
				}
			} catch (MalformedURLException e) {
				Logging.logger().fine("Can't retrieve texture data.");
			}
			// BRL>

			if (textureURL != null && !layer.isTextureFileExpired(tile, textureURL, layer.getDataFileStore())) {
				// AL -----------------------------
				if (layer.loadTexture(tile, textureURL, indexDate)) {
					layer.getLevels().unmarkResourceAbsent(tile);
					// Evite un deadlock très problématique!!!
					Model model = Viewer3D.getModel();
					synchronized (model) {
						layer.firePropertyChange(AVKey.LAYER, null, this);
						return;
					}
				} else {
					// Assume that something's wrong with the file and delete
					// it.
					// AL -----------------------------
					layer.getDataFileStore().removeFile(textureURL);
					// AL -----------------------------
					layer.getLevels().markResourceAbsent(tile);
					String message = Logging.getMessage("generic.DeletedCorruptDataFile", textureURL);
					Logging.logger().info(message);
				}
			}
		}

		/**
		 * @param that the task to compare
		 *
		 * @return -1 if <code>this</code> less than <code>that</code>, 1 if greater than, 0 if equal
		 *
		 * @throws IllegalArgumentException if <code>that</code> is null
		 */
		@Override
		public int compareTo(RequestTask that) {
			if (that == null) {
				String msg = Logging.getMessage("nullValue.RequestTaskIsNull");
				Logging.logger().severe(msg);
				throw new IllegalArgumentException(msg);
			}
			return tile.getPriority() == that.tile.getPriority() ? 0
					: tile.getPriority() < that.tile.getPriority() ? -1 : 1;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}

			final RequestTask that = (RequestTask) o;

			// Don't include layer in comparison so that requests are shared
			// among layers
			return !(tile != null ? !tile.equals(that.tile) : that.tile != null);
		}

		@Override
		public int hashCode() {
			return tile != null ? tile.hashCode() : 0;
		}

		@Override
		public String toString() {
			return tile.toString();
		}
	}

	protected boolean isTextureFileExpired(MultiTextureTile tile, java.net.URL textureURL, FileStore fileStore) {
		if (!WWIO.isFileOutOfDate(textureURL, tile.getLevel().getExpiryTime())) {
			return false;
		}

		// The file has expired. Delete it.
		fileStore.removeFile(textureURL);
		String message = Logging.getMessage("generic.DataFileExpired", textureURL);
		Logging.logger().fine(message);
		return true;
	}

	protected boolean loadTexture(MultiTextureTile tile, java.net.URL textureURL, int indexDate) {
		HashMap<SubLayer, TextureData> textureData;

		synchronized (fileLock) {
			textureData = readTexture(textureURL, this.getTextureFormat(), this.isUseMipMaps(), indexDate);
		}

		if (textureData == null) {
			return false;
		}

		tile.setTextureData(textureData, indexDate);
		if (tile.getLevelNumber() != 0 || !this.isRetainLevelZeroTiles()) {
			this.addTileToCache(tile, indexDate);
		}

		return true;
	}

	protected abstract void addTileToCache(MultiTextureTile tile, int indexDate);

	/**
	 * Reads and returns the texture data at the specified URL, optionally converting it to the specified format and
	 * generating mip-maps. If <code>textureFormat</code> is a recognized mime type, this returns the texture data in
	 * the specified format. Otherwise, this returns the texture data in its native format. If <code>useMipMaps</code>
	 * is true, this generates mip maps for any non-DDS texture data, and uses any mip-maps contained in DDS texture
	 * data.
	 * <p/>
	 * Supported texture formats are as follows:
	 * <ul>
	 * <li><code>image/dds</code> - Returns DDS texture data, converting the data to DDS if necessary. If the data is
	 * already in DDS format it's returned as-is.</li>
	 * </ul>
	 *
	 * @param url the URL referencing the texture data to read.
	 * @param textureFormat the texture data format to return.
	 * @param useMipMaps true to generate mip-maps for the texture data or use mip maps already in the texture data, and
	 *            false to read the texture data without generating or using mip-maps.
	 * @param indexDate
	 *
	 * @return TextureData the texture data from the specified URL, in the specified format and with mip-maps.
	 */
	protected abstract HashMap<SubLayer, TextureData> readTexture(java.net.URL url, String textureFormat,
			boolean useMipMaps, int indexDate);

	protected static String getCache(String urlToMatch, SubLayer subLayer, int indexDate) {
		String accurate = urlToMatch.substring(0, urlToMatch.indexOf("/" + NasaCache.getCacheSubDirectory() + "/") + 1);
		urlToMatch = urlToMatch.substring(urlToMatch.indexOf("/" + NasaCache.getCacheSubDirectory() + "/")
				+ NasaCache.getCacheSubDirectory().length() + 2);
		// remove name of the layer
		urlToMatch = urlToMatch.substring(urlToMatch.indexOf("/") + 1);
		// name of the SubLayer
		accurate += subLayer.getTexture(indexDate);
		urlToMatch = urlToMatch.substring(urlToMatch.indexOf("/") + 1);
		urlToMatch = urlToMatch.substring(urlToMatch.indexOf("/"));
		// index of the tile
		accurate += urlToMatch;
		return accurate;
	}

	// **************************************************************//
	// ******************** Resources *****************************//
	// **************************************************************//

	protected void checkResources() {
		AVList params = (AVList) this.getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (params == null) {
			return;
		}

		this.initializeResources(params);
	}

	protected void initializeResources(AVList params) {
		Boolean b = (Boolean) params.getValue(AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE);
		if (b != null && b) {
			this.initPropertiesFromService(params);
		}
	}

	protected void initPropertiesFromService(AVList params) {
		if (serviceInitialized) {
			return;
		}

		URL url = DataConfigurationUtils.getOGCGetCapabilitiesURL(params);
		if (url == null) {
			return;
		}

		if (absentResources == null || absentResources.isResourceAbsent(SERVICE_CAPABILITIES_RESOURCE_ID)) {
			return;
		}

		WMSCapabilities caps = SessionCacheUtils.getOrRetrieveSessionCapabilities(url, WorldWind.getSessionCache(), url,
				absentResources, SERVICE_CAPABILITIES_RESOURCE_ID, this, AVKey.LAYER);

		if (caps == null) {
			return;
		}

		this.initPropertiesFromCapabilities(caps, params);
		serviceInitialized = true;
	}

	protected void initPropertiesFromCapabilities(WMSCapabilities caps, AVList params) {
		String[] names = DataConfigurationUtils.getOGCLayerNames(params);
		if (names == null || names.length == 0) {
			return;
		}

		Long expiryTime = caps.getLayerLatestLastUpdateTime(names);
		if (expiryTime != null) {
			this.setExpiryTime(expiryTime);
		}
	}
}
