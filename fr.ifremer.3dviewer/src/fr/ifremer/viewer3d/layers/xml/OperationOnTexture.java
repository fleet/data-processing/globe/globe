package fr.ifremer.viewer3d.layers.xml;

import fr.ifremer.viewer3d.loaders.SubLayer;

/***
 * Defines an {@link Operation} occurring between {@link SubLayer}
 * 
 * @author MORVAN
 * 
 */
public class OperationOnTexture extends Operation {

	/***
	 * texture on which the Operation should be perform
	 */
	private String type;

	/***
	 * unit of texture on which the Operation should be perform
	 */
	private String unit;

	private float min;
	private float max;

	private float textureMin;
	private float textureMax;

	/***
	 * 
	 * @param typeOfOperation
	 * @param type
	 * @param min
	 * @param max
	 */
	public OperationOnTexture(String typeOfOperation, String type, String unit, double min, double max) {
		super(type + " " + typeOfOperation, typeOfOperation);
		this.type = type;
		this.unit = unit;
		this.setMin((float) min);
		this.setMax((float) max);

		textureMin = (float) min;
		textureMax = (float) max;
	}

	/***
	 * texture on which the Operation should be perform
	 */
	public String getType() {
		return type;
	}

	/***
	 * 
	 * @return unit of type
	 */
	public String getUnit() {
		return unit;
	}

	/***
	 * 
	 * @return min value before filtering
	 */
	public float getMin() {
		return min;
	}

	/***
	 * min value before filtering
	 * 
	 * @param min
	 */
	public void setMin(float min) {
		this.min = min;
	}

	/***
	 * 
	 * @return max value before filtering
	 */
	public float getMax() {
		return max;
	}

	/***
	 * max value before filtering
	 * 
	 * @param max
	 */
	public void setMax(float max) {
		this.max = max;
	}

	/***
	 * 
	 * @return texture min to compare with min/max values in shader
	 */
	public float getValMin() {
		return textureMin;
	}

	/***
	 * 
	 * @return texture max to compare with min/max values in shader
	 */
	public float getValMax() {
		return textureMax;
	}

}
