package fr.ifremer.viewer3d.layers.xml;

import java.util.HashMap;

import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.layers.LayerParameters.AbstractLayerParameters;
import fr.ifremer.viewer3d.loaders.SubLayer;
import fr.ifremer.viewer3d.multidata.SubLayerParameters;

public class MultiTextureLayerParameters extends AbstractLayerParameters {

	private int currentSubLayerIndex;

	/***
	 * Display all {@link SubLayer} with the type of current {@link SubLayer}
	 */
	private boolean displaySameType = false;
	/***
	 * Use an {@link OperationOnTexture} for filtering
	 */
	private boolean useTextureFilter = false;
	/***
	 * Use an {@link OperationOnTexture} for filtering
	 */
	private boolean useSecondTextureFilter = false;

	/***
	 * {@link SubLayer} parameters common to a same data type
	 */
	private HashMap<String, SubLayerParameters> subLayersParameters = new HashMap<>();

	public void load(MultiTextureLayerParameters other) {
		visibility = other.visibility;
		currentSubLayerIndex = other.currentSubLayerIndex;
		displaySameType = other.displaySameType;
		useTextureFilter = other.useTextureFilter;
		useSecondTextureFilter = other.useSecondTextureFilter;
		subLayersParameters.clear();
		subLayersParameters.putAll(other.subLayersParameters);
	}

	/**
	 * @return the currentSubLayerIndex
	 */
	public int getCurrentSubLayerIndex() {
		return currentSubLayerIndex;
	}

	/**
	 * @param currentSubLayerIndex the currentSubLayerIndex to set
	 */
	public void setCurrentSubLayerIndex(int currentSubLayerIndex) {
		pcs.firePropertyChange("currentSubLayerIndex", this.currentSubLayerIndex,
				this.currentSubLayerIndex = currentSubLayerIndex);
	}

	/***
	 *
	 * @return true if must display all {@link SubLayer} with same type of current one
	 */
	public boolean isDisplaySameType() {
		return displaySameType;
	}

	/***
	 * Ask to display all {@SubLayer] with the same type of current one
	 *
	 * @param displaySameType
	 */
	public void setDisplaySameType(boolean displaySameType) {
		pcs.firePropertyChange("displaySameType", this.displaySameType, this.displaySameType = displaySameType);
	}

	/***
	 * Use an {@link OperationOnTexture} for filtering?
	 *
	 * @return parameters.isUseTextureFilter()
	 */
	public boolean isUseTextureFilter() {
		return useTextureFilter;
	}

	/**
	 * @param useTextureFilter the useTextureFilter to set
	 */
	public void setUseTextureFilter(boolean useTextureFilter) {
		pcs.firePropertyChange("useTextureFilter", this.useTextureFilter, this.useTextureFilter = useTextureFilter);
	}

	/**
	 * @return the useSecondTextureFilter
	 */
	public boolean isUseSecondTextureFilter() {
		return useSecondTextureFilter;
	}

	/**
	 * @param useSecondTextureFilter the useSecondTextureFilter to set
	 */
	public void setUseSecondTextureFilter(boolean useSecondTextureFilter) {
		pcs.firePropertyChange("useSecondTextureFilter", this.useSecondTextureFilter,
				this.useSecondTextureFilter = useSecondTextureFilter);
	}

	/***
	 * {@link OperationOnTexture} currently performed </br>
	 */
	private OperationOnTexture operationOnTexture = null;

	/***
	 * Second {@link OperationOnTexture} currently performed </br>
	 */
	private OperationOnTexture secondOperationOnTexture = null;

	/**
	 * @return the operationOnTexture
	 */
	public OperationOnTexture getOperationOnTexture() {
		return operationOnTexture;
	}

	/**
	 * @param operationOnTexture the operationOnTexture to set
	 */
	public void setOperationOnTexture(OperationOnTexture operationOnTexture) {
		this.operationOnTexture = operationOnTexture;
	}

	/**
	 * @return the secondOperationOnTexture
	 */
	public OperationOnTexture getSecondOperationOnTexture() {
		return secondOperationOnTexture;
	}

	/**
	 * @param secondOperationOnTexture the secondOperationOnTexture to set
	 */
	public void setSecondOperationOnTexture(OperationOnTexture secondOperationOnTexture) {
		this.secondOperationOnTexture = secondOperationOnTexture;
	}

	/***
	 *
	 * @param type
	 * @return {@link SubLayer} parameters common to a same data type, cannot be null
	 */
	public SubLayerParameters getSubLayerParameters(String type) {
		SubLayerParameters parameters = findSubLayerParameters(type);
		if (parameters == null) {
			parameters = new SubLayerParameters(type);
			setSubLayerParameters(type, parameters);
		}
		return parameters;
	}

	/***
	 *
	 * @param type
	 * @return {@link SubLayer} parameters common to a same data type, might be null
	 */
	public SubLayerParameters findSubLayerParameters(String type) {
		return subLayersParameters.get(type);
	}

	/***
	 *
	 * @param type
	 * @return {@link SubLayer} parameters common to a same data type
	 */
	public void setSubLayerParameters(String type, SubLayerParameters params) {
		subLayersParameters.put(type, params);
	}

}
