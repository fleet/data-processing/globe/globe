package fr.ifremer.viewer3d.layers.xml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;

/**
 * Ordered implementation of IInfos.
 * 
 * @author apside
 */
public class AbstractInfos implements IInfos, Iterable<Property<?>> {

	private LinkedHashMap<String,Property<?>> properties=new LinkedHashMap<>();

	public AbstractInfos() {
	}

	@Override
	public Iterator<Property<?>> iterator() {
		return properties.values().iterator();
	}

	@Override
	public List<Property<?>> getProperties() {
		ArrayList<Property<?>> ret=new ArrayList<>();
		ret.addAll(properties.values());
		return ret;
	}

	public void addInfo(Property<?> p) {
		if(p==null || p.getKey()==null) return;
		properties.put(p.getKey(),p);
	}

	public void addInfo(String key, Object value) {
		if (key != null) {
			properties.put(key,Property.build(key, value != null ? String.valueOf(value) : ""));
		}
	}

	public Property<?> getInfo(String key) {
		return properties.get(key);
	}

	public Property<?> removeInfo(String key) {
		Property<?> result = null;
		for (Iterator<Property<?>> it = iterator(); it.hasNext();) {
			Property<?> current = it.next();
			if (current.getKey().equals(key)) {
				result = current;
				it.remove();
			}
		}
		return result;
	}

}
