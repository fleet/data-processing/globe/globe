package fr.ifremer.viewer3d.layers.xml.parametersview;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public abstract class ApplyAllComposite extends Composite {
	protected Button allLayersButton;

	public ApplyAllComposite(Composite parent, int style) {
		super(parent, style);
	}

	public boolean isAllLayerActive() {
		return allLayersButton.getSelection();

	}

	/**
	 * apply settings for all layers with same type
	 */
	public void applyAll() {

	}
}
