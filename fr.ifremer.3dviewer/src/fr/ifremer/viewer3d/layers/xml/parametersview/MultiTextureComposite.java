package fr.ifremer.viewer3d.layers.xml.parametersview;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.widget.color.ColormapComposite;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerComposite;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import fr.ifremer.viewer3d.loaders.SubLayer;
import fr.ifremer.viewer3d.multidata.SubLayerParameters;
import fr.ifremer.viewer3d.terrain.MultiElevationModel;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Composite used to change rendering parameters of {@link MultiTextureLayer} according to current {@link SubLayer}
 *
 * @author MMORVAN;
 */
public class MultiTextureComposite extends ApplyAllComposite implements PropertyChangeListener {

	private MultiTextureLayer layer;

	// SWT Widget
	private Spinner minSpinner;
	private Spinner maxSpinner;
	private Spinner minTransparencySpinner;
	private Spinner maxTransparencySpinner;
	private Button minMaxButton;
	private Button rehauss5Button;
	private Button rehauss1Button;
	private Button customButton;
	private Combo colormapCombo;
	private Button invertButton;
	private Button shadeButton;
	private AzimuthShadeComposite azimuthShadeComposite;
	private ZenithShadeComposite zenithShadeComposite;
	private Button shadeLogButton;
	private Button gradientButton;
	private Scale shadeScale;
	private Combo filterCombo;
	private Scale gaussianScale;
	private Scale filterScale;
	private HistogramComposite histogramComposite;
	private Button histogramButon;
	private Composite shadeComposite;
	// private Spinner offsetSpinner;
	private Combo dataTypeCombo;
	private Combo subLayerCombo;
	private List<SubLayer> sublayers = new ArrayList<>();
	private Label typeLabel;
	private FilterTextureComposite filterTextureComposite1 = null;
	private FilterTextureComposite filterTextureComposite2 = null;

	private ColormapComposite colormapComposite;

	private PropertyChangeListener shadingListener = e -> Display.getDefault().asyncExec(this::applyAll);

	public MultiTextureComposite(Composite parent, int style, MultiTextureLayer layer) {
		super(parent, style);
		this.layer = layer;
		init();
		layer.addPropertyChangeListener(this);

		addDisposeListener(evt -> {
			azimuthShadeComposite.removePropertyChangeListener(shadingListener);
			zenithShadeComposite.removePropertyChangeListener(shadingListener);
			layer.removePropertyChangeListener(this);
		});
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);

		createContrastShadeGroup();

	}

	private Group createContrastShadeGroup() {
		final Group contrastShadeGroup = new Group(this, SWT.NONE);
		contrastShadeGroup.setText("Parameters");
		contrastShadeGroup.setLayout(new GridLayout(1, false));
		contrastShadeGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// SubLayer Group
		// no need to select data if no choice
		if (layer.getSubLayers().size() > 1) {
			Group subLayerGroup = new Group(contrastShadeGroup, SWT.NONE);

			subLayerGroup.setText("Data Choice");
			subLayerGroup.setLayout(new GridLayout(2, false));
			subLayerGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));

			String dataType = layer.getCurrentSubLayer().getType();
			// no need for a combo box if no choice
			if (layer.getDataTypes().size() > 1) {
				dataTypeCombo = new Combo(subLayerGroup, SWT.READ_ONLY);
				int i = 0;
				for (String type : layer.getDataTypes()) {
					dataTypeCombo.add(type, i++);
				}

				dataTypeCombo.select(layer.getDataTypes().indexOf(layer.getCurrentSubLayer().getType()));
				dataTypeCombo.setEnabled(true);
				dataTypeCombo.addListener(SWT.Selection, event -> {
					String dataType1 = layer.getDataTypes().get(dataTypeCombo.getSelectionIndex());
					refreshSubLayerCombo(dataType1);
					layer.selectTypeSubLayer(dataType1);
					subLayerCombo.select(layer.getSubLayers().indexOf(layer.getCurrentSubLayer()));

					RefreshOpenGLView.fullRefresh();
					// redraw panel to change data type
					refresh();
				});
				dataTypeCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));
			}

			subLayerCombo = new Combo(subLayerGroup, SWT.READ_ONLY);
			refreshSubLayerCombo(dataType);

			subLayerCombo.select(layer.getSubLayers().indexOf(layer.getCurrentSubLayer()));
			subLayerCombo.setEnabled(true);
			subLayerCombo.addListener(SWT.Selection, event -> {

				List<WWFileLayerStore> layerStores = IWWLayerService.grab().getFileLayerStoreModel().getAll().stream()//
						.collect(Collectors.toList());

				// ///FIXME et l'elevation???????
				// TODO passer l'elevationmodel en parametre!!!
				for (WWFileLayerStore ls : layerStores) {
					ElevationModel em = ls.getElevationModel();
					if (em instanceof MultiElevationModel) {
						MultiElevationModel mem = (MultiElevationModel) em;
						mem.setCurrentSubElevation(mem.getSubElevationList().get(subLayerCombo.getSelectionIndex()));
						break;
					}
				}

				layer.setCurrentSubLayer(sublayers.get(subLayerCombo.getSelectionIndex()));

				RefreshOpenGLView.fullRefresh();
				// redraw panel to change data type
				refresh();
			});
			subLayerCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));
		}

		// Date Group
		// composite visible only if many date
		if (layer.getDateMin() != layer.getDateMax()) {
			Group dateGroup = new Group(contrastShadeGroup, SWT.NONE);
			dateGroup.setText("Date");
			dateGroup.setLayout(new GridLayout(1, false));
			dateGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

			IndexedPlayer player = new IndexedPlayer("Date", layer.getDateMin(), layer.getDateMax());
			IndexedPlayerComposite indexedPlayerComposite = new IndexedPlayerComposite(dateGroup, SWT.NONE, player);
			indexedPlayerComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
			player.setCurrent(layer.getDateIndex());
			player.subscribe(playerData -> Display.getDefault().asyncExec(() -> {
				layer.setDateIndex(playerData.currentValue());
				IGeographicViewService.grab().refresh();
			}));
		}

		// Operations on SubLayers
		// no Operation if only one SubLayer
		if (layer.getSubLayers().size() > 1) {
			Group operationsGroup = new Group(contrastShadeGroup, SWT.NONE);
			operationsGroup.setText("Operations");
			operationsGroup.setLayout(new GridLayout(2, false));
			operationsGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));
			final Button displaySameType = new Button(operationsGroup, SWT.CHECK);
			displaySameType.setText("Mosaicing");
			displaySameType.setSelection(layer.getLayerParameters().isDisplaySameType());
			displaySameType.addListener(SWT.Selection, event -> {

				boolean mosaicing = displaySameType.getSelection();

				if (filterTextureComposite1 != null) {
					filterTextureComposite1.setEnabled(!mosaicing);
				}
				if (filterTextureComposite2 != null) {
					filterTextureComposite2.setEnabled(!mosaicing);
				}

				layer.getLayerParameters().setDisplaySameType(mosaicing);
				layer.refreshRenderingList();
				RefreshOpenGLView.fullRefresh();
			});
			displaySameType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

			// Filtering operation
			// many type are needed for Operation purpose
			if (layer.getDataTypes().size() > 1) {
				// first texture filter
				filterTextureComposite1 = new FilterTextureComposite(this, layer, operationsGroup, 1);
			}
			if (layer.getDataTypes().size() > 2) {
				// second texture filter
				filterTextureComposite2 = new FilterTextureComposite(this, layer, operationsGroup, 2);
			}
		}

		// parameters of currently visible SubLayers
		SubLayerParameters parameters = layer.getLayerParameters()
				.getSubLayerParameters(layer.getCurrentSubLayer().getType());

		// Contrast Group
		Group transparencyGroup = new Group(contrastShadeGroup, SWT.NONE);
		transparencyGroup.setText("Filtering Threshold");
		transparencyGroup.setLayout(new GridLayout(2, false));
		transparencyGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));
		minTransparencySpinner = new Spinner(transparencyGroup, SWT.NONE);
		maxTransparencySpinner = new Spinner(transparencyGroup, SWT.NONE);

		minTransparencySpinner.setDigits(2); // allow 2 decimal places
		minTransparencySpinner.setEnabled(true);
		minTransparencySpinner.setMinimum(Integer.MIN_VALUE);
		minTransparencySpinner.setMaximum(Integer.MAX_VALUE);
		minTransparencySpinner.setSelection((int) (parameters.getMinTransparency() - parameters.getOffset()) * 100);
		minTransparencySpinner.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setMinTransparency(minTransparencySpinner.getSelection() / 100.0 + parameters1.getOffset());

			// update step for transparency spinners
			updateCustomStep(minTransparencySpinner, maxTransparencySpinner);

			RefreshOpenGLView.fullRefresh();
		});
		minTransparencySpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		maxTransparencySpinner.setDigits(2); // allow 2 decimal places
		maxTransparencySpinner.setEnabled(true);
		maxTransparencySpinner.setMinimum(Integer.MIN_VALUE);
		maxTransparencySpinner.setMaximum(Integer.MAX_VALUE);
		maxTransparencySpinner.setSelection((int) (parameters.getMaxTransparency() - parameters.getOffset()) * 100);
		maxTransparencySpinner.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setMaxTransparency(maxTransparencySpinner.getSelection() / 100.0f + parameters1.getOffset());

			// update step for transparency spinners
			updateCustomStep(minTransparencySpinner, maxTransparencySpinner);

			RefreshOpenGLView.fullRefresh();
		});
		maxTransparencySpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		// Contrast Group
		Group contrastGroup = new Group(contrastShadeGroup, SWT.NONE);
		contrastGroup.setText("Contrast");
		contrastGroup.setLayout(new GridLayout(4, false)); // 4 colonnes au lieu
		// de 2
		contrastGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));

		typeLabel = new Label(contrastGroup, SWT.NONE);
		minSpinner = new Spinner(contrastGroup, SWT.NONE);
		maxSpinner = new Spinner(contrastGroup, SWT.NONE);

		final Composite nullComposite = new Composite(contrastGroup, SWT.NONE);

		minMaxButton = new Button(contrastGroup, SWT.RADIO);
		rehauss5Button = new Button(contrastGroup, SWT.RADIO);
		rehauss1Button = new Button(contrastGroup, SWT.RADIO);

		customButton = new Button(contrastGroup, SWT.RADIO);

		typeLabel.setText(layer.getCurrentSubLayer().getType() + " (" + layer.getCurrentSubLayer().getUnit() + ")");
		typeLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		minSpinner.setDigits(2); // allow 2 decimal places
		minSpinner.setEnabled(false);
		minSpinner.setMinimum(Integer.MIN_VALUE);
		minSpinner.setMaximum(Integer.MAX_VALUE);
		minSpinner.setSelection((int) (parameters.getMinContrast() - parameters.getOffset()) * 100);
		minSpinner.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setMinContrast(minSpinner.getSelection() / 100.0 + parameters1.getOffset());

			// update step for contrast spinners
			updateCustomStep(minSpinner, maxSpinner);

			if (allLayersButton.getSelection()) {
				applyAll();
			}

			RefreshOpenGLView.fullRefresh();
		});
		minSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		maxSpinner.setDigits(2); // allow 2 decimal places
		maxSpinner.setEnabled(false);
		maxSpinner.setMinimum(Integer.MIN_VALUE);
		maxSpinner.setMaximum(Integer.MAX_VALUE);
		maxSpinner.setSelection((int) (parameters.getMaxContrast() - parameters.getOffset()) * 100);
		maxSpinner.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setMaxContrast(maxSpinner.getSelection() / 100.0f + parameters1.getOffset());

			// update step for contrast spinners
			updateCustomStep(minSpinner, maxSpinner);

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		maxSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		nullComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		minMaxButton.setText("Min / Max");
		minMaxButton.setSelection(true);
		minMaxButton.setEnabled(true);
		minMaxButton.setSelection(parameters.getContrastLevel() == 0);
		minMaxButton.addListener(SWT.Selection, event -> {
			if (minMaxButton.getSelection()) {
				layer.contrastMinMax();
				refreshCompositeAfterContrast();
			}

			if (allLayersButton.getSelection()) {
				applyAll();
			}

		});

		rehauss5Button.setText("0.5%");
		rehauss5Button.setEnabled(true);
		rehauss5Button.setSelection(parameters.getContrastLevel() == 1);
		rehauss5Button.addListener(SWT.Selection, event -> {
			if (rehauss5Button.getSelection()) {
				layer.contrast05PerCent();
				refreshCompositeAfterContrast();
			}
		});
		rehauss5Button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		rehauss1Button.setText("1%");
		rehauss1Button.setEnabled(true);
		rehauss1Button.setSelection(parameters.getContrastLevel() == 2);
		rehauss1Button.addListener(SWT.Selection, event -> {
			if (rehauss1Button.getSelection()) {
				layer.contrast1PerCent();
				refreshCompositeAfterContrast();
			}
		});
		rehauss1Button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		customButton.setText("Custom");
		customButton.setEnabled(true);
		customButton.setSelection(parameters.getContrastLevel() == 3);
		customButton.addListener(SWT.Selection, event -> {
			if (customButton.getSelection()) {
				// contrast custom
				SubLayerParameters parameters1 = layer.getLayerParameters()
						.getSubLayerParameters(layer.getCurrentSubLayer().getType());
				parameters1.setContrastLevel(3);
				minSpinner.setEnabled(true);
				maxSpinner.setEnabled(true);

				// update step for contrast spinners
				updateCustomStep(minSpinner, maxSpinner);
			}

			if (allLayersButton.getSelection()) {
				applyAll();
			}
		});
		customButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// Color Group
		Group colorGroup = new Group(contrastShadeGroup, SWT.NONE);
		colorGroup.setText("Color");
		colorGroup.setLayout(new GridLayout(2, false));
		colorGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		colormapCombo = new Combo(colorGroup, SWT.READ_ONLY);
		invertButton = new Button(colorGroup, SWT.CHECK);
		colormapComposite = new ColormapComposite(colorGroup, SWT.BORDER | SWT.EMBEDDED,
				ColorMap.getColorMapIntIndex(parameters.getColorMap()));

		int ii = 0;
		// list of cpt files available in colormap directory
		for (String colormap : ColorMap.getColormapsNames()) {
			colormapCombo.add(colormap, ii);
			ii++;
		}

		colormapCombo.select(ColorMap.getColorMapIntIndex(parameters.getColorMap()));
		colormapCombo.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setColorMap(ColorMap.getColormapsNames().get(colormapCombo.getSelectionIndex()));
			colormapComposite.setIndexColormap(colormapCombo.getSelectionIndex());

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		colormapCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		invertButton.setText("Invert");
		invertButton.setSelection(parameters.isInverseColorMap());
		invertButton.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setInverseColorMap(invertButton.getSelection());
			colormapComposite.setInvertColormap(invertButton.getSelection());

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		invertButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, true);
		gridData.heightHint = 15;
		colormapComposite.setLayoutData(gridData);

		// Shade Group
		Group shadeGroup = new Group(contrastShadeGroup, SWT.NONE);
		shadeGroup.setText("Shading");
		shadeGroup.setLayout(new GridLayout(2, false));
		shadeGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		shadeButton = new Button(shadeGroup, SWT.CHECK);
		shadeComposite = new Composite(shadeGroup, SWT.BORDER | SWT.NONE);
		shadeComposite.setLayout(new GridLayout(2, true));
		shadeComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 4));

		Label azimuthLabel = new Label(shadeComposite, SWT.NONE);
		azimuthLabel.setText("Azimuth");
		Label zenithLabel = new Label(shadeComposite, SWT.NONE);
		zenithLabel.setText("Zenith");

		azimuthShadeComposite = new AzimuthShadeComposite(shadeComposite, SWT.BORDER | SWT.EMBEDDED, () -> layer);
		azimuthShadeComposite.setAzimuth(Math.toRadians(parameters.getAzimuth()), true);
		azimuthShadeComposite.setEnabled(parameters.isUseOmbrage());
		azimuthShadeComposite.addPropertyChangeListener(shadingListener);

		zenithShadeComposite = new ZenithShadeComposite(shadeComposite, SWT.BORDER | SWT.EMBEDDED, () -> layer);
		zenithShadeComposite.setZenith(Math.toRadians(parameters.getZenith()), true);
		zenithShadeComposite.setEnabled(parameters.isUseOmbrage());
		zenithShadeComposite.addPropertyChangeListener(shadingListener);

		shadeLogButton = new Button(shadeGroup, SWT.CHECK);
		gradientButton = new Button(shadeGroup, SWT.CHECK);

		Label exaggerationLabel = new Label(shadeComposite, SWT.NONE);
		exaggerationLabel.setText("Exaggeration (log10)");
		exaggerationLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));

		// Labels for ticks for exaggeration scale
		Composite tickComposite = new Composite(shadeComposite, SWT.NONE);

		tickComposite.setLayout(new GridLayout(9, true)); // scale -3...5

		tickComposite.setLayoutData(new GridData(GridData.FILL, GridData.END, true, false, 2, 1));

		int shadeScaleBegin = -3;
		int shadeScaleEnd = 5;

		final Label valueShadeBeginLabel = new Label(tickComposite, SWT.NONE);
		valueShadeBeginLabel.setText("" + shadeScaleBegin);
		valueShadeBeginLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.END, true, false, 1, 1));

		for (int shadeScaleLabel = shadeScaleBegin + 1; shadeScaleLabel < shadeScaleEnd; shadeScaleLabel++) {
			final Label valueShadeLabel = new Label(tickComposite, SWT.NONE);
			valueShadeLabel.setText("" + shadeScaleLabel);
			valueShadeLabel.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		}

		final Label valueShadeEndLabel = new Label(tickComposite, SWT.NONE);
		valueShadeEndLabel.setText("" + shadeScaleEnd);
		valueShadeEndLabel.setLayoutData(new GridData(GridData.END, GridData.END, true, false, 1, 1));

		shadeScale = new Scale(shadeComposite, SWT.NONE);

		shadeButton.setText("Shading");
		shadeButton.setSelection(parameters.isUseOmbrage());
		shadeButton.addListener(SWT.Selection, event -> {
			boolean useShade = shadeButton.getSelection();
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setUseOmbrage(useShade);
			azimuthShadeComposite.setEnabled(useShade);
			zenithShadeComposite.setEnabled(useShade);
			shadeScale.setEnabled(useShade);
			shadeLogButton.setEnabled(useShade);
			gradientButton.setEnabled(useShade);
			if (!useShade) {
				gradientButton.setSelection(false);
				parameters1.setUseGradient(false);
			}

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		shadeButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		shadeLogButton.setText("Log Shade");
		shadeLogButton.setEnabled(parameters.isUseOmbrage());
		shadeLogButton.setSelection(parameters.getUseOmbrageLogarithmique());
		shadeLogButton.addListener(SWT.Selection, new Listener() {
			private int previousShadeForLogVsShade;

			@Override
			public void handleEvent(Event event) {
				SubLayerParameters parameters = layer.getLayerParameters()
						.getSubLayerParameters(layer.getCurrentSubLayer().getType());
				parameters.setUseOmbrageLogarithmique(shadeLogButton.getSelection());
				// save log or shade cursor, and reset it at next switch
				int prev = previousShadeForLogVsShade;
				previousShadeForLogVsShade = shadeScale.getSelection();
				shadeScale.setSelection(prev);
				parameters.setOmbrageExaggeration(Math.pow(10, (double) shadeScale.getSelection() / 10 - 2));

				if (allLayersButton.getSelection()) {
					applyAll();
				}
				RefreshOpenGLView.fullRefresh();
			}
		});
		shadeLogButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		gradientButton.setText("Gradient");
		gradientButton.setSelection(parameters.getUseGradient());
		gradientButton.setEnabled(parameters.isUseOmbrage());
		gradientButton.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setUseGradient(gradientButton.getSelection());

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		gradientButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		shadeScale.setMinimum(0); // 10^-3
		shadeScale.setMaximum(80); // 10^5
		shadeScale.setSelection((int) Math.log10(parameters.getOmbrageExaggeration()) * 10 + 30);
		shadeScale.setEnabled(shadeButton.getSelection());
		shadeScale.setPageIncrement(10);
		shadeScale.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setOmbrageExaggeration(Math.pow(10, (double) shadeScale.getSelection() / 10 - 3));

			if (allLayersButton.getSelection()) {
				applyAll();
			}

			RefreshOpenGLView.fullRefresh();
		});
		shadeScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));

		// offset group
		/*
		 * final Group offsetGroup = new Group(contrastShadeGroup, SWT.NONE); offsetGroup
		 * .setText("Offset ("+layer.getCurrentSubLayer().getUnit()+")"); offsetGroup.setLayout(new GridLayout(2,true));
		 * offsetGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));
		 *
		 * offsetSpinner = new Spinner(offsetGroup, SWT.NONE); offsetSpinner.setToolTipText("Offset");
		 * offsetSpinner.setDigits(2); // allow 2 decimal places offsetSpinner.setMinimum(-1000000);
		 * offsetSpinner.setMaximum(1000000); offsetSpinner.setSelection((int) parameters.getOffset()*100);
		 * offsetSpinner.addListener(SWT.Selection, new Listener() {
		 *
		 * @Override public void handleEvent(Event event) { SubLayerParameters parameters =
		 * layer.getLayerParameters().getSubLayerParameters(layer.getCurrentSubLayer ().getType());
		 * parameters.setOffset(offsetSpinner.getSelection()/100.0f); refreshCompositeAfterScaleOffset(); } });
		 */

		// Filter Group
		final Group filterGroup = new Group(contrastShadeGroup, SWT.NONE);
		filterGroup.setText("Filter");
		filterGroup.setLayout(new GridLayout(2, true));
		filterGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		filterCombo = new Combo(filterGroup, SWT.READ_ONLY);

		// Composite for stack filters
		final Composite filterStackComposite = new Composite(filterGroup, SWT.NONE);
		final StackLayout stackLayout = new StackLayout();
		filterStackComposite.setLayout(stackLayout);
		filterStackComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// Composite for gauss filters
		final Composite gaussComposite = new Composite(filterStackComposite, SWT.NONE);
		gaussComposite.setLayout(new GridLayout(1, false));
		gaussComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		stackLayout.topControl = gaussComposite;

		// Composite for ticks gaussfilter
		Composite gaussTickComposite = new Composite(gaussComposite, SWT.NONE);
		gaussianScale = new Scale(gaussComposite, SWT.NONE);

		// Composite for other ticks
		final Composite filterComposite = new Composite(filterStackComposite, SWT.NONE);
		filterComposite.setLayout(new GridLayout(1, false));
		filterComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// Composite for ticks other ticks
		Composite filterTickComposite = new Composite(filterComposite, SWT.NONE);
		filterScale = new Scale(filterComposite, SWT.NONE);

		filterCombo.add("Gauss", 0);
		filterCombo.add("AlphaLineaire", 1);
		filterCombo.add("Lee", 2);
		filterCombo.add("Wiener", 3);
		filterCombo.select(parameters.getFilterName());
		filterCombo.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setFilterName(filterCombo.getSelectionIndex());

			if (filterCombo.getSelectionIndex() == 0) {// gaussian
				stackLayout.topControl = gaussComposite;
			} else {
				stackLayout.topControl = filterComposite;
			}

			filterStackComposite.layout();

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		filterCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		gaussTickComposite.setLayout(new GridLayout(4, true));
		gaussTickComposite.setLayoutData(new GridData(GridData.FILL, GridData.END, true, false, 1, 1));
		final Label gauss0Label = new Label(gaussTickComposite, SWT.NONE);
		gauss0Label.setText("0");
		gauss0Label.setLayoutData(new GridData(GridData.BEGINNING, GridData.END, true, false, 1, 1));
		final Label gauss1Label = new Label(gaussTickComposite, SWT.NONE);
		gauss1Label.setText("1");
		gauss1Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label gauss2Label = new Label(gaussTickComposite, SWT.NONE);
		gauss2Label.setText("2");
		gauss2Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label gauss3Label = new Label(gaussTickComposite, SWT.NONE);
		gauss3Label.setText("3");
		gauss3Label.setLayoutData(new GridData(GridData.END, GridData.END, true, false, 1, 1));

		gaussianScale.setMinimum(0); // 0
		gaussianScale.setMaximum(30); // 3
		gaussianScale.setSelection((int) (parameters.getSigmaGauss() * 10));
		gaussianScale.setPageIncrement(10);
		gaussianScale.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setSigmaGauss((double) gaussianScale.getSelection() / 10);

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		gaussianScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		filterTickComposite.setLayout(new GridLayout(8, true));
		filterTickComposite.setLayoutData(new GridData(GridData.FILL, GridData.END, true, false, 1, 1));
		final Label filter0Label = new Label(filterTickComposite, SWT.NONE);
		filter0Label.setText("-2");
		filter0Label.setLayoutData(new GridData(GridData.BEGINNING, GridData.END, true, false, 1, 1));
		final Label filter1Label = new Label(filterTickComposite, SWT.NONE);
		filter1Label.setText("1");
		filter1Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label filter2Label = new Label(filterTickComposite, SWT.NONE);
		filter2Label.setText("2");
		filter2Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label filter3Label = new Label(filterTickComposite, SWT.NONE);
		filter3Label.setText("3");
		filter3Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label filter4Label = new Label(filterTickComposite, SWT.NONE);
		filter4Label.setText("4");
		filter4Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label filter5Label = new Label(filterTickComposite, SWT.NONE);
		filter5Label.setText("5");
		filter5Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label filter6Label = new Label(filterTickComposite, SWT.NONE);
		filter6Label.setText("6");
		filter6Label.setLayoutData(new GridData(GridData.CENTER, GridData.END, true, false, 1, 1));
		final Label filter7Label = new Label(filterTickComposite, SWT.NONE);
		filter7Label.setText("7");
		filter7Label.setLayoutData(new GridData(GridData.END, GridData.END, true, false, 1, 1));

		filterScale.setMinimum(0); // 0
		filterScale.setMaximum(70); // 7
		filterScale.setSelection((int) Math.log10(parameters.getFilterHeight()) * 10);
		filterScale.setPageIncrement(10);
		filterScale.addListener(SWT.Selection, event -> {
			SubLayerParameters parameters1 = layer.getLayerParameters()
					.getSubLayerParameters(layer.getCurrentSubLayer().getType());
			parameters1.setFilterHeight((double) filterScale.getSelection() / 10);
			parameters1.setFilterWidth((double) filterScale.getSelection() / 10);

			if (allLayersButton.getSelection()) {
				applyAll();
			}
			RefreshOpenGLView.fullRefresh();
		});
		filterScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// All layers Group
		Group allLayersGroup = new Group(contrastShadeGroup, SWT.NONE);
		allLayersGroup.setText("Apply configuration for all layers");
		allLayersGroup.setLayout(new GridLayout(1, false));
		allLayersGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// apply for all layer with same type
		allLayersButton = new Button(allLayersGroup, SWT.CHECK);
		allLayersButton.setText("Apply for all " + layer.getCurrentSubLayer().getType() + " Layers");
		allLayersButton.addListener(SWT.Selection, event -> {
			if (allLayersButton.getSelection()) {
				applyAll();
			}
		});

		// Histogram Group
		Group histogramGroup = new Group(contrastShadeGroup, SWT.NONE);
		histogramGroup.setText("Histogram");
		histogramGroup.setLayout(new GridLayout(1, false));
		histogramGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		histogramComposite = new HistogramComposite(histogramGroup, SWT.EMBEDDED);
		histogramButon = new Button(histogramGroup, SWT.NONE);

		GridLayout layout = new GridLayout(1, false);
		layout.marginWidth = 95;
		layout.marginHeight = 60;
		histogramComposite.setLayout(layout);
		histogramComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false, 1, 1));

		// compute histogram
		histogramButon.setText("Compute Histogram");
		histogramButon.addListener(SWT.Selection, event -> {
			rehauss5Button.setSelection(false);
			rehauss1Button.setSelection(false);
			MultiTextureLayer.facteurRehaussement = -1; // pas de
			// rehaussement
			MultiTextureLayer.typeHisto = layer.getCurrentSubLayer().getType();

			if (!MultiTextureLayer.isCreateHisto()) {
				MultiTextureLayer.preComputeHisto();
			}

			// call redraw to add tile in tileList to compute histogramm
			Viewer3D.getWwd().redrawNow();

			if (MultiTextureLayer.isCreateHisto()) {
				MultiTextureLayer.computeHisto();
			}

			histogramComposite.repaint();
		});

		return contrastShadeGroup;
	}

	/***
	 * Update panel when current {@link SubLayer} is changed
	 */
	protected void refresh() {
		SubLayerParameters parameters = layer.getLayerParameters()
				.getSubLayerParameters(layer.getCurrentSubLayer().getType());
		// type label
		typeLabel.setText(layer.getCurrentSubLayer().getType() + " (" + layer.getCurrentSubLayer().getUnit() + ")");

		// transparency
		minTransparencySpinner.setSelection((int) (parameters.getMinTransparency() - parameters.getOffset()) * 100);
		maxTransparencySpinner.setSelection((int) (parameters.getMaxTransparency() - parameters.getOffset()) * 100);

		// contrast
		minSpinner.setSelection((int) (parameters.getMinContrast() - parameters.getOffset()) * 100);
		maxSpinner.setSelection((int) (parameters.getMaxContrast() - parameters.getOffset()) * 100);

		minMaxButton.setSelection(parameters.getContrastLevel() == 0);
		rehauss5Button.setSelection(parameters.getContrastLevel() == 1);
		rehauss1Button.setSelection(parameters.getContrastLevel() == 2);
		customButton.setSelection(parameters.getContrastLevel() == 3);

		// colormap
		colormapCombo.select(ColorMap.getColorMapIntIndex(parameters.getColorMap()));
		colormapComposite.setIndexColormap(ColorMap.getColorMapIntIndex(parameters.getColorMap()));
		invertButton.setSelection(parameters.isInverseColorMap());
		colormapComposite.setInvertColormap(parameters.isInverseColorMap());

		// shading
		shadeButton.setSelection(parameters.isUseOmbrage());

		azimuthShadeComposite.setAzimuth(Math.toRadians(parameters.getAzimuth()), true);
		azimuthShadeComposite.setEnabled(parameters.isUseOmbrage());

		zenithShadeComposite.setZenith(Math.toRadians(parameters.getZenith()), true);
		zenithShadeComposite.setEnabled(parameters.isUseOmbrage());

		shadeLogButton.setEnabled(parameters.isUseOmbrage());
		shadeLogButton.setSelection(parameters.getUseOmbrageLogarithmique());

		gradientButton.setSelection(parameters.getUseGradient());
		gradientButton.setEnabled(parameters.isUseOmbrage());

		shadeScale.setSelection((int) Math.log10(parameters.getOmbrageExaggeration()) * 10 + 30);

		// offset
		// offsetSpinner.setSelection((int) parameters.getOffset()*100);

		// filtering
		filterCombo.select(parameters.getFilterName());

		gaussianScale.setSelection((int) (parameters.getSigmaGauss() * 10));
		filterScale.setSelection((int) Math.log10(parameters.getFilterHeight()) * 10);

		// refresh
		getParent().layout();
		invertButton.setSelection(parameters.isInverseColorMap());
	}

	/**
	 * apply settings for all layers with same type
	 */
	@Override
	public void applyAll() {
		SubLayerParameters parameters = layer.getLayerParameters()
				.getSubLayerParameters(layer.getCurrentSubLayer().getType());
		for (Layer l : Viewer3D.getWwd().getModel().getLayers()) {
			if (l instanceof MultiTextureLayer && !l.equals(layer) && ((MultiTextureLayer) l).getCurrentSubLayer()
					.getType().equalsIgnoreCase(layer.getCurrentSubLayer().getType())) {

				// Layer
				SubLayer subLayer = ((MultiTextureLayer) l).getCurrentSubLayer();
				SubLayerParameters p = ((MultiTextureLayer) l).getLayerParameters()
						.getSubLayerParameters(subLayer.getType());
				// contrast
				p.setMinContrast(parameters.getMinContrast());
				p.setMaxContrast(parameters.getMaxContrast());

				// colormap
				p.setColorMap(parameters.getColorMap());
				p.setInverseColorMap(parameters.isInverseColorMap());

				// shade
				p.setUseOmbrage(parameters.isUseOmbrage());
				p.setAzimuth(parameters.getAzimuth());
				p.setZenith(parameters.getZenith());
				p.setOmbrageExaggeration(parameters.getOmbrageExaggeration());
				p.setUseGradient(parameters.getUseGradient());
				p.setUseOmbrageLogarithmique(parameters.getUseOmbrageLogarithmique());

				// filter
				p.setFilterName(parameters.getFilterName());
				p.setSigmaGauss(parameters.getSigmaGauss());
				p.setFilterHeight(parameters.getFilterHeight());
				p.setFilterWidth(parameters.getFilterHeight());

				// Layer Composite
				if (((MultiTextureLayer) l).getComposite() != null) {
					MultiTextureComposite composite = (MultiTextureComposite) ((MultiTextureLayer) l).getComposite();
					// contrast
					composite.minSpinner.setSelection(minSpinner.getSelection());
					composite.maxSpinner.setSelection(maxSpinner.getSelection());
					if (minMaxButton.getSelection()) {
						composite.minMaxButton.setSelection(false);
						composite.rehauss5Button.setSelection(false);
						composite.rehauss1Button.setSelection(false);
						composite.customButton.setSelection(true);
						composite.minSpinner.setEnabled(true);
						composite.maxSpinner.setEnabled(true);
						minSpinner.setEnabled(true);
						maxSpinner.setEnabled(true);
					} else {
						composite.minMaxButton.setSelection(false);
						composite.rehauss5Button.setSelection(rehauss5Button.getSelection());
						composite.rehauss1Button.setSelection(rehauss1Button.getSelection());
						composite.customButton.setSelection(customButton.getSelection());
					}
					composite.minSpinner.setEnabled(minSpinner.isEnabled());
					composite.maxSpinner.setEnabled(maxSpinner.isEnabled());
					composite.minMaxButton.setEnabled(minMaxButton.isEnabled());
					composite.rehauss5Button.setEnabled(rehauss5Button.isEnabled());
					composite.rehauss1Button.setEnabled(rehauss1Button.isEnabled());
					composite.customButton.setEnabled(customButton.isEnabled());

					// colormap
					// composite.colormapButton.setSelection(colormapButton.getSelection());
					composite.colormapCombo.select(colormapCombo.getSelectionIndex());
					composite.invertButton.setEnabled(invertButton.isEnabled());
					composite.invertButton.setSelection(invertButton.getSelection());

					// shade
					composite.shadeButton.setSelection(shadeButton.getSelection());
					composite.azimuthShadeComposite.setEnabled(azimuthShadeComposite.isEnabled());
					composite.azimuthShadeComposite.setAzimuth(azimuthShadeComposite.getAzimuth(), true);
					composite.zenithShadeComposite.setEnabled(zenithShadeComposite.isEnabled());
					composite.zenithShadeComposite.setZenith(zenithShadeComposite.getZenith(), true);
					composite.shadeScale.setEnabled(shadeScale.isEnabled());
					composite.shadeScale.setSelection(shadeScale.getSelection());
					composite.gradientButton.setSelection(gradientButton.getSelection());
					composite.shadeLogButton.setSelection(shadeLogButton.getSelection());

					// filter
					composite.filterCombo.select(filterCombo.getSelectionIndex());
					composite.filterScale.setSelection(filterScale.getSelection());
					composite.gaussianScale.setSelection(gaussianScale.getSelection());

					// Apply ALL
					composite.allLayersButton.setSelection(allLayersButton.getSelection());
				}

			}
		}
		RefreshOpenGLView.fullRefresh();
	}

	/**
	 * Update the custom step for a couple of min and max spinners.
	 *
	 * @param min The min spinner
	 * @param max The max spinner
	 */
	private void updateCustomStep(Spinner min, Spinner max) {
		double customStep = Math.abs(max.getSelection() - min.getSelection()) / 100.0f;
		min.setIncrement((int) customStep);
		max.setIncrement((int) customStep);
	}

	/***
	 * updates IHM after contrast
	 */
	private void refreshCompositeAfterContrast() {
		SubLayerParameters parameters = layer.getLayerParameters()
				.getSubLayerParameters(layer.getCurrentSubLayer().getType());

		double min = MultiTextureLayer.staticRehaussementMin;
		double max = MultiTextureLayer.staticRehaussementMax;

		histogramComposite.repaint();

		minSpinner.setEnabled(false);

		maxSpinner.setEnabled(false);

		minSpinner.setSelection((int) (min - parameters.getOffset()) * 100);
		maxSpinner.setSelection((int) (max - parameters.getOffset()) * 100);

		if (allLayersButton.getSelection()) {
			applyAll();
		}
	}

	// /***
	// * updates IHM after scale/offset
	// */
	// private void refreshCompositeAfterScaleOffset() {
	// SubLayerParameters parameters =
	// layer.getLayerParameters().getSubLayerParameters(layer.getCurrentSubLayer().getType());
	//
	// minSpinner.setSelection((int) (layer.getCurrentSubLayer().getValMin() -
	// parameters.getOffset()) * 100);
	// maxSpinner.setSelection((int) (layer.getCurrentSubLayer().getValMax() -
	// parameters.getOffset()) * 100);
	// }

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getSource() instanceof MultiTextureLayer && layer.equals(event.getSource())) {
			final String name = event.getPropertyName();
			final Object newValue = event.getNewValue();
			Display.getDefault().asyncExec(() -> {
				if (MultiTextureLayer.view_Heading.equals(name)) {
					azimuthShadeComposite.setViewHeading((Double) newValue);
				} else {
					if (MultiTextureLayer.Contrast_05.equals(name)) {
						rehauss5Button.setSelection(true);
						rehauss1Button.setSelection(false);
						customButton.setSelection(false);
						minMaxButton.setSelection(false);
						refreshCompositeAfterContrast();
					} else if (MultiTextureLayer.Contrast_1.equals(name)) {
						rehauss1Button.setSelection(true);
						rehauss5Button.setSelection(false);
						customButton.setSelection(false);
						minMaxButton.setSelection(false);
						refreshCompositeAfterContrast();
					} else if (MultiTextureLayer.Contrast_MinMax.equals(name)) {
						rehauss1Button.setSelection(false);
						rehauss5Button.setSelection(false);
						customButton.setSelection(false);
						minMaxButton.setSelection(true);
						refreshCompositeAfterContrast();
					}
				}
			});

		}
	}

	/***
	 * fill SubLayerCombo with the subLayers of dataType
	 *
	 * @param dataType
	 */
	private void refreshSubLayerCombo(String dataType) {
		if (subLayerCombo == null) {
			return;
		}

		sublayers.clear();
		subLayerCombo.removeAll();
		for (SubLayer sublayer : layer.getSubLayers()) {
			if (sublayer.getType().equals(dataType)) {
				sublayers.add(sublayer);
				subLayerCombo.add(sublayer.getName());
			}
		}
	}
}
