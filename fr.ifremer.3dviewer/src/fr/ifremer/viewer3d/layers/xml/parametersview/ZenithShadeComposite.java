package fr.ifremer.viewer3d.layers.xml.parametersview;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.function.Supplier;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import fr.ifremer.viewer3d.multidata.SubLayerParameters;
import gov.nasa.worldwind.layers.Layer;

/**
 * Composite that changes zenith of shading of ShaderElevationLayer
 * 
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 */
public class ZenithShadeComposite extends Composite implements MouseListener, MouseMotionListener {

	/** For property change support. */
	public static final String PROPERTY_ZENITH = "zenith";

	private Supplier<Layer> provider;
	protected Canvas canvas;

	protected static final int radius = 55;
	protected static final int spotRadius = 4;
	protected static final int cornerx = -51;
	protected static final int cornery = 4;

	protected double theta;
	protected Color circleColor;
	protected Color circleColor2;
	protected Color sunColor;
	protected Color spotColor;

	protected boolean pressedOnSpot;
	protected boolean enabled;

	protected PropertyChangeSupport pcs;

	public ZenithShadeComposite(Composite parent, int style, Supplier<Layer> provider) {
		super(parent, style);
		this.provider = provider;
		this.pcs = new PropertyChangeSupport(this);
		init(style);
	}

	/**
	 * Adds a PropertyChangeListener.
	 * 
	 * @param listener The listener to add.
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Removes a PropertyChangeListener.
	 * 
	 * @param listener The listener to remove.
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init(int style) {
		theta = Math.PI / 4;
		pressedOnSpot = false;
		circleColor = Color.lightGray;
		circleColor2 = Color.gray;
		sunColor = Color.gray;
		spotColor = Color.lightGray;

		Frame frame = SWT_AWT.new_Frame(this);
		canvas = new Canvas() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			/**
			 * Paint the JKnob on the graphics context given. The knob is a filled circle with a small filled circle
			 * offset within it to show the current angular position of the knob.
			 * 
			 * @param g The graphics context on which to paint the knob.
			 */
			@Override
			public void paint(Graphics g) {

				g.clearRect(0, 0, 2 * radius, 2 * radius);

				// Cercle interieur principal
				g.setColor(circleColor);
				g.fillArc(cornerx, cornery, 2 * radius, 2 * radius, 0, 90);

				// //Cercle contour principal
				g.setColor(circleColor2);
				g.drawArc(cornerx, cornery, 2 * radius, 2 * radius, 0, 90);
				g.drawLine(cornerx + radius, cornery + radius, cornerx + radius, cornery);
				g.drawLine(cornerx + radius, cornery + radius, cornerx + 2 * radius, cornery + radius);

				// Find the center of the spot.
				Point pt = getSpotCenter();
				int xc = (int) pt.getX();
				int yc = (int) pt.getY();

				// Fleche
				g.setColor(Color.gray);
				g.drawLine(cornerx + radius, cornery + radius, xc, yc);

				// Rayons de soleil
				drawSunRays(g, xc, yc);

				// Cercle centre
				g.setColor(circleColor2);
				g.fillOval(cornerx + radius - spotRadius, cornery + radius - spotRadius, 2 * spotRadius,
						2 * spotRadius);

				// Draw the spot.
				g.setColor(spotColor);
				g.fillOval(xc - spotRadius, yc - spotRadius, 2 * spotRadius, 2 * spotRadius);

				g.setColor(sunColor);
				g.drawOval(xc - spotRadius, yc - spotRadius, 2 * spotRadius, 2 * spotRadius);
			}
		};
		frame.add(canvas);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
	}

	public void drawSunRays(Graphics g, int xc, int yc) {
		g.setColor(sunColor);
		g.drawLine(xc, yc, xc - 2 * spotRadius, yc);
		g.drawLine(xc, yc, xc + 2 * spotRadius, yc);
		g.drawLine(xc, yc, xc, yc - 2 * spotRadius);
		g.drawLine(xc, yc, xc, yc + 2 * spotRadius);

		g.drawLine(xc, yc, (int) (xc - 1.5 * spotRadius), (int) (yc - 1.5 * spotRadius));
		g.drawLine(xc, yc, (int) (xc - 1.5 * spotRadius), (int) (yc + 1.5 * spotRadius));
		g.drawLine(xc, yc, (int) (xc + 1.5 * spotRadius), (int) (yc - 1.5 * spotRadius));
		g.drawLine(xc, yc, (int) (xc + 1.5 * spotRadius), (int) (yc + 1.5 * spotRadius));

	}

	/**
	 * Calculate the x, y coordinates of the center of the spot.
	 * 
	 * @return a Point containing the x,y position of the center of the spot.
	 */
	protected Point getSpotCenter() {

		// Calculate the center point of the spot RELATIVE to the
		// center of the of the circle.

		// int r = radius - spotRadius; // spot à l'interieur
		int r = radius - 0; // spot sur le cercle

		int xcp = (int) (r * Math.sin(theta));
		int ycp = (int) (r * Math.cos(theta));

		// Adjust the center point of the spot so that it is offset
		// from the center of the circle. This is necessary becasue
		// 0,0 is not actually the center of the circle, it is the
		// upper left corner of the component!
		int xc = radius + xcp + cornerx;
		int yc = radius - ycp + cornery;

		// Create a new Point to return since we can't
		// return 2 values!
		return new Point(xc, yc);
	}

	/**
	 * Determine if the mouse click was on the spot or not. If it was return true, otherwise return false.
	 * 
	 * @return true if x,y is on the spot and false if not.
	 */
	protected boolean isOnSpot(Point pt) {
		if (enabled) {
			if (pt.distance(getSpotCenter()) <= spotRadius + 2
					|| (pt.distance(new Point(cornerx + radius, cornery + radius)) <= radius
							&& pt.x > (cornerx + radius) && pt.y < (cornery + radius))) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @param radians Zenith value, in radians.
	 */
	public void setZenith(final double radians, boolean notify) {

		if (this.theta != radians) {
			double oldValue = this.theta;

			this.theta = radians;
			double degrees = Math.toDegrees(radians);
			Layer layer = provider.get();
			if (layer instanceof ShaderElevationLayer) {
				((ShaderElevationLayer) layer).setZenith(degrees);
			} else if (layer instanceof MultiTextureLayer) {
				SubLayerParameters parameters = ((MultiTextureLayer) layer).getLayerParameters()
						.getSubLayerParameters(((MultiTextureLayer) layer).getCurrentSubLayer().getType());
				parameters.setZenith(degrees);
			}

			if (notify) {
				pcs.firePropertyChange(PROPERTY_ZENITH, oldValue, this.theta);
			} else {
				canvas.repaint();
			}
			Viewer3D.getWwd().redraw();
		}
	}

	public double getZenith() {
		return theta;
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.enabled = enabled;
		if (!enabled) {
			circleColor = Color.lightGray;
			circleColor2 = Color.gray;
			sunColor = Color.gray;
			spotColor = Color.lightGray;
		} else {
			circleColor = Color.white;
			circleColor2 = Color.orange;
			sunColor = new Color(255, 127, 0);
			spotColor = Color.yellow;
		}
		setZenith(theta, true);
		canvas.repaint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (pressedOnSpot) {

			int mx = e.getX();
			int my = e.getY();

			// Compute the x, y position of the mouse RELATIVE
			// to the center of the knob.
			int mxp = 0;
			int myp = 0;
			if (mx >= (cornerx + radius) && my <= (cornery + radius)) {
				mxp = mx - (radius + cornerx);
				myp = (radius + cornery) - my;
			} else if (my > (radius + cornery)) {
				mxp = radius;
				myp = 0;
			} else if (mx < (cornerx + radius)) {
				mxp = 0;
				myp = radius;
			}
			setZenith(Math.atan2(mxp, myp), true);

			canvas.repaint();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Point mouseLoc = e.getPoint();
		pressedOnSpot = isOnSpot(mouseLoc);
		if (pressedOnSpot) {

			int mx = e.getX();
			int my = e.getY();

			// Compute the x, y position of the mouse RELATIVE
			// to the center of the knob.
			int mxp = mx - (radius + cornerx);
			int myp = (radius + cornery) - my;

			// Compute the new angle of the knob from the
			// new x and y position of the mouse.
			// Math.atan2(...) computes the angle at which
			// x,y lies from the positive y axis with cw rotations
			// being positive and ccw being negative.
			theta = Math.atan2(mxp, myp);

			setZenith(theta, true);

			canvas.repaint();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {

		Point mouseLoc = e.getPoint();
		pressedOnSpot = isOnSpot(mouseLoc);
		canvas.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		pressedOnSpot = false;
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

}
