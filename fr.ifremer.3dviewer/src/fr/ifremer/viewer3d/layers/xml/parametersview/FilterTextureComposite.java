package fr.ifremer.viewer3d.layers.xml.parametersview;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import fr.ifremer.viewer3d.layers.xml.Operation;
import fr.ifremer.viewer3d.layers.xml.OperationOnTexture;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;

public class FilterTextureComposite extends Composite {
	private MultiTextureLayer layer = null;
	private Group group = null;

	/***
	 * first or second texture filter
	 */
	private int filterNumber = 1;

	private Button useTextureFilter;
	private int previousFiltering = -1;
	private OperationOnTexture currentOperation = null;

	private Combo filterTypeCombo;
	private Text typeLabel;
	private Spinner minTextureFilterSpinner;
	private Spinner maxTextureFilterSpinner;

	public FilterTextureComposite(MultiTextureComposite parent, MultiTextureLayer layer, Group group, int filterNumber) {
		super(parent, parent.getStyle());
		this.layer = layer;
		this.group = group;
		this.filterNumber = filterNumber;

		createComposite();
	}

	private void createComposite() {

		Group filteringGroup = new Group(group, SWT.NONE);
		filteringGroup.setText("Filtering");
		filteringGroup.setLayout(new GridLayout(3, false));
		filteringGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));

		useTextureFilter = new Button(filteringGroup, SWT.CHECK);
		useTextureFilter.setText("Use filter?");
		useTextureFilter.setSelection(layer.isUseOperationTextureFilter());
		useTextureFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		useTextureFilter.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				layer.setUseOperationTextureFilter(useTextureFilter.getSelection(), filterNumber);
				filterTypeCombo.setEnabled(useTextureFilter.getSelection());
				minTextureFilterSpinner.setEnabled(useTextureFilter.getSelection());
				maxTextureFilterSpinner.setEnabled(useTextureFilter.getSelection());

				// redefine the operation if changed
				if (previousFiltering != filterTypeCombo.getSelectionIndex()) {
					currentOperation = layer.setOperation(Operation.Filtering, layer.getDataTypes().get(filterTypeCombo.getSelectionIndex()), filterNumber);

					minTextureFilterSpinner.setSelection((int) (currentOperation.getMin() * 100));
					maxTextureFilterSpinner.setSelection((int) (currentOperation.getMax() * 100));
				}

				RefreshOpenGLView.fullRefresh();
			}
		});

		filterTypeCombo = new Combo(filteringGroup, SWT.READ_ONLY);
		for (String dataType : layer.getDataTypes()) {
			filterTypeCombo.add(dataType);
		}

		// load session case
		if (previousFiltering == -1 && layer.getLayerParameters().getOperationOnTexture() != null) {
			currentOperation = layer.getLayerParameters().getOperationOnTexture();
			previousFiltering = layer.getDataTypes().indexOf(currentOperation.getType());
		} else {
			previousFiltering = 0;

			currentOperation = layer.setOperation(Operation.Filtering, layer.getDataTypes().get(previousFiltering), filterNumber);
		}

		filterTypeCombo.select(previousFiltering);

		filterTypeCombo.setEnabled(useTextureFilter.getSelection());
		filterTypeCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				previousFiltering = filterTypeCombo.getSelectionIndex();
				currentOperation = layer.setOperation(Operation.Filtering, layer.getDataTypes().get(previousFiltering), filterNumber);

				minTextureFilterSpinner.setSelection((int) (currentOperation.getMin() * 100));
				maxTextureFilterSpinner.setSelection((int) (currentOperation.getMax() * 100));

				// change unit
				typeLabel.setText("(" + currentOperation.getUnit() + ") : ");

				RefreshOpenGLView.fullRefresh();
				// redraw panel to change data type
				layout();
			}
		});
		filterTypeCombo.setLayoutData(new GridData(GridData.BEGINNING, GridData.END, true, false, 2, 1));

		typeLabel = new Text(filteringGroup, SWT.NONE);
		typeLabel.setText("(" + currentOperation.getUnit() + ") : ");
		typeLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.END, true, false, 1, 1));

		minTextureFilterSpinner = new Spinner(filteringGroup, SWT.NONE);
		maxTextureFilterSpinner = new Spinner(filteringGroup, SWT.NONE);

		minTextureFilterSpinner.setDigits(2); // allow 2 decimal places
		minTextureFilterSpinner.setEnabled(useTextureFilter.getSelection());
		minTextureFilterSpinner.setMinimum(Integer.MIN_VALUE);
		minTextureFilterSpinner.setMaximum(Integer.MAX_VALUE);
		minTextureFilterSpinner.setSelection((int) (currentOperation.getMin() * 100));
		minTextureFilterSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				currentOperation.setMin((float) (minTextureFilterSpinner.getSelection() / 100.0));

				// update step for filter spinners
				updateCustomStep(minTextureFilterSpinner, maxTextureFilterSpinner);

				RefreshOpenGLView.fullRefresh();
			}
		});
		minTextureFilterSpinner.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, true, false, 1, 1));

		maxTextureFilterSpinner.setDigits(2); // allow 2 decimal places
		maxTextureFilterSpinner.setEnabled(useTextureFilter.getSelection());
		maxTextureFilterSpinner.setMinimum(Integer.MIN_VALUE);
		maxTextureFilterSpinner.setMaximum(Integer.MAX_VALUE);
		maxTextureFilterSpinner.setSelection((int) (currentOperation.getMax() * 100));
		maxTextureFilterSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				currentOperation.setMax((float) (maxTextureFilterSpinner.getSelection() / 100.0));

				// update step for filter spinners
				updateCustomStep(minTextureFilterSpinner, maxTextureFilterSpinner);

				RefreshOpenGLView.fullRefresh();
			}
		});
		maxTextureFilterSpinner.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false, 1, 1));
	}

	/**
	 * Update the custom step for a couple of min and max spinners.
	 * 
	 * @param min
	 *            The min spinner
	 * @param max
	 *            The max spinner
	 */
	private void updateCustomStep(Spinner min, Spinner max) {
		double customStep = Math.abs(max.getSelection() - min.getSelection()) / 100.0f;
		min.setIncrement((int) customStep);
		max.setIncrement((int) customStep);
	}

	/**
	 * Set enabled ou disabled the filter composite
	 * 
	 * @param enable
	 */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);

		if (!enabled) {
			useTextureFilter.setSelection(enabled);
		} else {
			if (filterNumber == 1) {
				useTextureFilter.setSelection(layer.isUseOperationTextureFilter());
			} else if (filterNumber == 2) {
				useTextureFilter.setSelection(layer.isUseSecondOperationTextureFilter());
			}

		}
		useTextureFilter.setEnabled(enabled);

		filterTypeCombo.setEnabled(enabled);
		minTextureFilterSpinner.setEnabled(enabled);
		maxTextureFilterSpinner.setEnabled(enabled);
	}
}
