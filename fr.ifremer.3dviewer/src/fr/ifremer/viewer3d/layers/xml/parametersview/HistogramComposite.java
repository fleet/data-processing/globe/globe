package fr.ifremer.viewer3d.layers.xml.parametersview;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.viewer3d.layers.ContrastShadeHistogram;

/**
 * Composite that draw the histogram of ShaderElevationLayer.
 * 
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 */
public class HistogramComposite extends Composite {

	/** Canvas of AWT frame */
	protected JComponent canvas;

	protected int width;
	protected int height;
	protected int histogramSize = 255;
	protected BufferedImage img;

	public HistogramComposite(Composite parent, int style) {
		super(parent, style);
		init(this, style);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init(Composite parent, int style) {
		this.width = 180;
		this.height = 100;

		// Use an image to avoid the histogram recomputation during painting
		this.img = new BufferedImage(width+80, height+20, BufferedImage.TYPE_INT_ARGB);
		computeImage();

		Frame frame = SWT_AWT.new_Frame(this);
		canvas = new JComponent() {
			private static final long serialVersionUID = 1L;
			@Override
			public void paint(Graphics g) {
				g.drawImage(img, 0, 0, null);
			}
		};

		frame.add(canvas);
	}

	protected void computeImage()
	{
		Graphics g = img.getGraphics();

		g.setColor(Color.white);
		g.fillRect(0, 0, width+80, height+20);

		int[] histogram = ContrastShadeHistogram.getHistogram();
		int borneMin = (int) ContrastShadeHistogram.getStaticMin();
		int borneMax = (int) ContrastShadeHistogram.getStaticMax();

		// border of histogram
		g.setColor(Color.black);
		g.drawLine(0, height, 255 * width / histogramSize, height);

		g.drawLine(0, height, 0, 0);
		g.drawLine(255 * width / histogramSize, height, 255 * width / histogramSize, 0);

		g.setColor(Color.gray);
		g.drawLine(51 * width / histogramSize, height, 51 * width / histogramSize, 0);
		g.drawLine(102 * width / histogramSize, height, 102 * width / histogramSize, 0);
		g.drawLine(153 * width / histogramSize, height, 153 * width / histogramSize, 0);
		g.drawLine(204 * width / histogramSize, height, 204 * width / histogramSize, 0);

		if (histogram != null) {
			// compute max of histogram
			int max = 0;
			for (int i = 0; i < 256; i++) {
				if (histogram[i] > max) {
					max = histogram[i];
				}
			}

			if (max != 0) {
				// draw histogram
				g.setColor(Color.darkGray);
				for (int i = 0; i < 256; i++) {
					g.drawLine(i * width / histogramSize, height - histogram[i] * height / max, (i + 1) * width / histogramSize, height - histogram[i] * height / max);
					if (i != 255) {
						g.drawLine((i + 1) * width / histogramSize, height - histogram[i] * height / max, (i + 1) * width / histogramSize, height - histogram[i + 1] * height / max);
					}
				}
			}
			g.setFont(new Font("Serif", Font.PLAIN, 10));
			g.drawString(String.valueOf(borneMin), 0, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 1 + borneMin), 51 * width / histogramSize - 5, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 2 + borneMin), 102 * width / histogramSize - 10, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 3 + borneMin), 153 * width / histogramSize - 10, height + 10);
			g.drawString(String.valueOf((borneMax - borneMin) / 5 * 4 + borneMin), 204 * width / histogramSize - 10, height + 10);
			g.drawString(String.valueOf(borneMax), 255 * width / histogramSize - 10, height + 10);
		}
	}

	/** This method calls the repaint of canvas */
	public void repaint() {
		computeImage();
		canvas.repaint();
	}

	@Override
	public Point getSize() {
		return new Point(500, 500);

	}

}
