package fr.ifremer.viewer3d.layers.xml.parametersview;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;

@Component(name = "globe_viewer3d_parameters_multitexture_composite_factory", service = IParametersViewCompositeFactory.class)
public class MultiTextureCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		Composite result = null;
		if (selection instanceof MultiTextureLayer) {
			result = new MultiTextureComposite(parent, SWT.NONE, (MultiTextureLayer) selection);
			((MultiTextureLayer) selection).setComposite(result);
			MultiTextureLayer.setCurrent((MultiTextureLayer) selection);
		}
		return Optional.ofNullable(result);
	}
}
