package fr.ifremer.viewer3d.layers.xml;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.MyAbstractWallLayer;
import fr.ifremer.viewer3d.layers.wc.IWCLayer;
import fr.ifremer.viewer3d.loaders.EchogramLoader;
import fr.ifremer.viewer3d.model.IPlayable;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.model.watercolumn.WaterColumnData;
import fr.ifremer.viewer3d.render.VolatilTexturedWall;
import fr.ifremer.viewer3d.util.IFiles;
import fr.ifremer.viewer3d.util.ILazyLoading;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Extent;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.view.BasicView;

/**
 * This layer shows echograms images.
 * 
 * @author C.Laurent
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class WaterColumnLayer extends MyAbstractWallLayer implements PropertyChangeListener, ITarget, IInfos, ILazyLoading, IFiles, IDeletable, IPlayable, IWCLayer {

	/**
	 * Textured wall used to render current image's texture.
	 */
	protected VolatilTexturedWall texturedWall = null;

	/**
	 * The water column data displayed in this layer.
	 */
	protected WaterColumnData waterColumn = null;

	/**
	 * Index of last displayed frame.
	 */
	protected int lastIndex = 0;

	/**
	 * Sector for this layer location.
	 */
	protected Sector sector;

	protected transient PropertyChangeSupport pcs;

	protected Position startPos = null;

	@Override
	public Position getPosition() {
		return this.startPos;
	}

	@Override
	public void setPosition(Position startPos) {
		this.startPos = startPos;
	}

	@Override
	public Position getPosition(int index) {
		return this.waterColumn.getPositions().get(index);
	}

	public int getCurrentPingIndex() {
		return this.lastIndex;
	}

	/**
	 * Ctor.
	 * 
	 * @param playerdata
	 *            bean instance containing current playing state
	 * @param waterColumnData
	 *            water column data (echograms) to render
	 */
	public WaterColumnLayer(PlayerBean playerdata, WaterColumnData waterColumnData) {

		if (Viewer3D.getWwd() != null) {
			((BasicView) Viewer3D.getWwd().getView()).setDetectCollisions(false);
		}

		if (playerdata == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_WATERCOLUMNLAYER_ERROR_MESSAGE")); //$NON-NLS-1$
		}

		setName(playerdata.getName());

		// applicationFrame = appFrame;
		this.waterColumn = waterColumnData;
		this.playerbean = playerdata;
		this.playerbean.addPropertyChangeListener(this);
		this.pcs = new PropertyChangeSupport(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		super.dispose();

		// BRL : JVM doesn't provide any unmap method see bug 4715154
		// we just remove references call garbage collector ... and pray

		if (this.imagesList != null) {
			this.imagesList.clear();
		}
		System.gc();
		texturedWall.dispose();
	}

	/**
	 * Initialize this layer : creates memory maps for input images an loads
	 * first image.
	 */
	@Override
	public void initialize() {
		if (this.waterColumn != null) {
			EchogramLoader loader = new EchogramLoader();

			int size = this.waterColumn.getDataCount();

			this.startPos = new Position(this.waterColumn.getLatLonCenter(0), 0);
			// not yet initialized
			if (size == 0) {
				File file = this.waterColumn.getFile();
				try {
					loader.fullLoad(file, this);
				} catch (Exception e) {
					Logging.logger().severe("Error reading echograms input. " + e);
					// avoid another loading with same error data
					this.waterColumn = null;
					return;
				}
				size = this.waterColumn.getDataCount();
			}

			this.imagesList =  this.waterColumn.getImgList();

			// playable
			this.timeList = new ArrayList<Long>(size);

			File wcFile = this.waterColumn.getFile();
			if (wcFile != null && wcFile.exists()) {

				// initialize with first available image
				if (size > 0) {
					this.texturedWall = new VolatilTexturedWall(this, this.waterColumn.getLatlon1(0), this.waterColumn.getLatlon2(0), this.waterColumn.getBottomElevation(0),
							this.waterColumn.getTopElevation(0),()->new WCTexturedPickedObject(this));
				} 

				// BRL 11/04/2011 pick enable should be true in order to use
				// picking for remarkable points definition.
				setPickEnabled(true);
				if (this.texturedWall == null) {
					Logging.logger().severe("No image found");
					// avoid another loading with same error data
					this.waterColumn = null;
				} else {

					// Replaced by ordered renderables
					// addRenderable(texturedWall);

					// init sector
					this.sector = Sector.boundingSector(this.waterColumn.getLatlonList1());
				}
			}
		}
		if (this.waterColumn != null) {
			this.timeList.addAll(this.waterColumn.getTimeList());

			// set position pour l'object 3D
			this.setPosition(new Position(this.waterColumn.getLatLonCenter(0), 0));
			refresh(this.waterColumn.getPingAtIndex(0));
		}

		this._availableColorMaps = new ArrayList<String>();

		// colormap
		listerColorMap(false);// la donnée est sur un char, elle n'est pas en
		// int24
		setColorMapOri(0);
	}

	/**
	 * Updates rendering.
	 * 
	 * @param pingIndex
	 *            ping of the image to display
	 * 
	 */
	@Override
	public void refresh(int pingIndex) {
		if (this.texturedWall != null) {
			int newIndex = this.waterColumn.getIndexForPing(pingIndex);
			int size = this.imagesList.size();
			updateTooltip(newIndex);
			if (newIndex >= 0 && newIndex < size /* && this.lastIndex != newIndex */) {
				LatLon p1 = this.waterColumn.getLatlon1(newIndex);
				LatLon p2 = this.waterColumn.getLatlon2(newIndex);
				Double bottom = this.waterColumn.getBottomElevation(newIndex);
				Double top = this.waterColumn.getTopElevation(newIndex);
				this.texturedWall.setLocation1(p1);
				this.texturedWall.setLocation2(p2);
				this.texturedWall.setBottomElevation(bottom);
				this.texturedWall.setTopElevation(top);

				// update texture's image file
				String imageFile = this.imagesList.get(newIndex);
				this.texturedWall.updateImageFile(imageFile);

				this.lastIndex = newIndex;

				firePropertyChange(AVKey.LAYER, null, this);
				WorldWindowGLCanvas wwd = Viewer3D.getWwd();
				if (wwd != null) {
					wwd.redraw();
				}
			}
		}
	}

	private void updateTooltip(int newIndex) {
		LatLon p1 = this.waterColumn.getLatlon1(newIndex);
		LatLon p2 = this.waterColumn.getLatlon2(newIndex);
		Double bottom = this.waterColumn.getBottomElevation(newIndex);
		Double top = this.waterColumn.getTopElevation(newIndex);
		StringBuilder sb = new StringBuilder(this.getStringValue(AVKey.DISPLAY_NAME));
		sb.append("\nPing ");
		sb.append(this.waterColumn.getPingAtIndex(newIndex));
		Long date = this.waterColumn.getTime(newIndex);
		if (date != null) {
			sb.append("\n");
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			sb.append(sdf.format(new Date(date)));
		}
		int width = texturedWall.getWidth();
		if (width > 0) {
			double eqR = 6378137.0; // equatorial radius WGS84
			double poR = 6356752.3142; // polar radius
			double dist = LatLon.ellipsoidalDistance(p1, p2, eqR, poR);
			sb.append("\nRes X : ");
			sb.append(String.format("%.2f", dist / width));
			sb.append(" px/m");
		}
		int height = texturedWall.getWidth();
		if (height > 0) {
			double h = top - bottom;
			sb.append("\nRes Y : ");
			sb.append(String.format("%.2f", h / height));
			sb.append(" px/m");
		}

		setValue(AVKey.ROLLOVER_TEXT, sb.toString());
	}

	/**
	 * Updates rendering.
	 * 
	 * @param currentTime
	 *            the current Time to render
	 * @param newTime
	 *            the new Time to render
	 */
	public void refresh(Long currentTime, Long newTime) {
		if (this.timeList.contains(newTime)) {
			int newIndex;
			newIndex = this.timeList.indexOf(newTime);
			refresh(newIndex);
		}
	}

	/**
	 * Time interval between two frames (ms).
	 * 
	 * @return time interval between two frames (ms).
	 */
	public int getDelay() {
		return this.playerbean.getDelay();
	}

	/**
	 * Time interval between two frames (ms).
	 * 
	 * @param delay
	 *            time interval between two frames (ms).
	 */
	public void setDelay(int delay) {
		this.playerbean.setDelay(delay);
		Logging.logger().info(DicoBundle.getString("KEY_WATERCOLUMNLAYER_DELAY_MESSAGE") + " = " + delay); //$NON-NLS-1$
	}

	/**
	 * Returns player bean used by this layer.
	 * 
	 * @return player bean used by this layer
	 */
	@Override
	public PlayerBean getPlayerBean() {
		return this.playerbean;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String name = event.getPropertyName();
		if (PlayerBean.PROPERTY_PLAY_NUMBER.equals(name)) {
			// int oldIdx = (Integer) event.getOldValue();
			int newIdx = (Integer) event.getNewValue();
			refresh(newIdx);
		}

		super.propertyChange(event);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sector getSector() {
		return this.sector;
	}

	@Override
	protected void doRender(DrawContext dc) {
		if (this.texturedWall == null) {
			initialize();
		}

		if (dc != null && this.texturedWall != null) {
			Sector current = Sector.boundingSector(texturedWall.getLocation1(), texturedWall.getLocation2());
			Extent extent = Sector.computeBoundingCylinder(dc.getGlobe(), dc.getVerticalExaggeration(), current);
			if (dc.getView().getFrustumInModelCoordinates().intersects(extent)) {
				//setValue(SSVAVKey.CURRENTLY_VISIBLE, true);
				this.texturedWall.updateEyeDistance(dc);
				dc.addOrderedRenderable(this.texturedWall);
			} else {
				//setValue(SSVAVKey.CURRENTLY_VISIBLE, false);
			}
		}
	}

	@Override
	protected void doPick(DrawContext dc, Point point) {
		if (dc != null && this.texturedWall != null) {
			this.texturedWall.updateEyeDistance(dc);
			dc.addOrderedRenderable(this.texturedWall);
		}
	}

	/**
	 * Returns input data.
	 */
	public WaterColumnData getDatas() {
		return this.waterColumn;
	}

	@Override
	public File getInputFile() {
		if (this.waterColumn != null) {
			return this.waterColumn.getFile();
		}
		return null;
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public List<Property<?>> getProperties() {
		return this.waterColumn.getProperties();
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOpacity(double opacity) {
		super.setOpacity(opacity);
		if (this.texturedWall != null) {
			this.texturedWall.setOpacity(opacity);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<File, Boolean> getFiles() {
		if (this.waterColumn != null) {
			File f = this.waterColumn.getFile();
			if (f != null) {
				Map<File, Boolean> files = new HashMap<File, Boolean>(2);
				files.put(f, true);

				// images directory
				String imgDir = this.waterColumn.getImgDir();
				if (imgDir != null && !imgDir.isEmpty()) {
					String dir = f.getParent() + "/" + imgDir;
					if (dir != null) {
						files.put(new File(dir), true);
					}
				}
				return files;
			}
		}
		return null;
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	@Override
	public Long getEndTime() {
		return this.timeList.get(this.timeList.size() - 1);
	}

	@Override
	public Long getStartTime() {
		return this.timeList.get(0);
	}

	public Long getTime(int index) {
		return this.timeList.get(index);
	}

	@Override
	public String getPlayableType() {
		return this.playableType;
	}

	@Override
	public void firePropertyChange(String propertyName, Object oldValue,
			Object newValue) {
		this.pcs.firePropertyChange(propertyName, oldValue, newValue);
		super.firePropertyChange(propertyName, oldValue, newValue);
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}
	/**
	 * Return the current ping identifier (might be different from the ping index)
	 * */
	public int getPingIdentifier()
	{
		return getPlayerBean().getPlayNumber();
	}

	/***
	 * return the current ping index
	 */

	public int getPingIndex()
	{
		return getDatas().getIndexForPing(getPlayerBean().getPlayNumber());
	}
	/**
	 * Return the current ping datetime
	 * */
	public Date getPingDate()
	{
		return new Date(getTime(getPingIndex()));
	}


	/** {@inheritDoc} */
	@Override
	public float getVerticalOffset() {
		// Vertical offset not managed by this layer
		return 0f;
	}
}
