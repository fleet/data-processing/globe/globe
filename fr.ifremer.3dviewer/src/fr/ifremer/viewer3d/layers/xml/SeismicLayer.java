package fr.ifremer.viewer3d.layers.xml;

import java.io.File;
import java.util.ArrayList;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.loaders.EchogramLoader;
import fr.ifremer.viewer3d.model.IPlayable;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.model.watercolumn.WaterColumnData;
import fr.ifremer.viewer3d.render.VolatilHideableTexturedWall;
import fr.ifremer.viewer3d.render.VolatilTexturedWall;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;

/**
 * ???
 * 
 * <b>Exigence : REQ-NR-004 :</b> Affichage de données sismiques ou de sondeurs
 * de sédiments<br/>
 * L’objectif de cette phase est de proposer et de mettre en oeuvre une vue
 * synthétique permettant de superposer le DTM de la zone concernée, les
 * échogrammes et les images sismiques ou de sondeurs de sédiments. Ces deux
 * derniers groupes d’images sont manipulables par l’opérateur de façon
 * interactive en accès rejeu (avance, avance rapide, image par image, …).
 */
public class SeismicLayer extends WaterColumnLayer {

	protected VolatilTexturedWall texturedWall = null;

	private String playableType = IPlayable.ISPLAYABLEWITHPING;

	public SeismicLayer(PlayerBean playerBean, WaterColumnData wcd) {
		super(playerBean, wcd);

		// original color map should be jet
		setColorMapOri(0);
	}

	@Override
	public void initialize() {
		if (this.waterColumn != null) {
			EchogramLoader loader = new EchogramLoader();

			int size = this.waterColumn.getDataCount();

			this.startPos = new Position(this.waterColumn.getLatLonCenter(0), 0);

			// not yet initialized
			if (size == 0) {
				File file = this.waterColumn.getFile();
				try {
					loader.fullLoad(file, this);
				} catch (Exception e) {
					Logging.logger().severe("Error reading echograms input. " + e);
					// avoid another loading with same error data
					this.waterColumn = null;
					return;
				}
				size = this.waterColumn.getDataCount();
			}

			this.imagesList = this.waterColumn.getImgList();

			// playable
			this.timeList = new ArrayList<Long>(size);

			File wcFile = this.waterColumn.getFile();
			if (wcFile != null && wcFile.exists()) {
				// initialize with first available image
				if (size > 0) {
					this.texturedWall = new VolatilHideableTexturedWall(this, this.waterColumn.getLatlon1(0), this.waterColumn.getLatlon2(0), this.waterColumn.getBottomElevation(0),
							this.waterColumn.getTopElevation(0),()->new WCTexturedPickedObject(this));

				}

				setPickEnabled(false);
				if (this.texturedWall == null) {
					Logging.logger().severe("No image found");
					// avoid another loading with same error data
					this.waterColumn = null;
				} else {

					// Replaced by ordered renderables
					// addRenderable(texturedWall);

					// init sector
					this.sector = Sector.boundingSector(this.waterColumn.getLatlonList1());
				}
			}
		}
		if (this.waterColumn != null) {
			this.timeList.addAll(this.waterColumn.getTimeList());

			// set position pour l'object 3D
			this.setPosition(new Position(this.waterColumn.getLatLonCenter(0), 0));
			refresh(this.waterColumn.getPingAtIndex(0));
		}
	}

	/**
	 * Updates rendering.
	 * 
	 * @param ping
	 *            the ping number
	 */
	@Override
	public void refresh(int ping) {
		if (this.texturedWall != null) {
			// donne le numero d'index en fonction du numero de ping
			int newIndex = this.waterColumn.getIndexForPing(ping); 
			int size = this.imagesList.size();
			if (newIndex >= 0 && newIndex < size /* && this.lastIndex != newIndex */) {
				this.texturedWall.setLocation1(this.waterColumn.getLatlon1(newIndex));
				this.texturedWall.setLocation2(this.waterColumn.getLatlon2(newIndex));
				this.texturedWall.setBottomElevation(this.waterColumn.getBottomElevation(newIndex));
				this.texturedWall.setTopElevation(this.waterColumn.getTopElevation(newIndex));

				String imageFile = this.imagesList.get(newIndex);
				this.texturedWall.updateImageFile(imageFile);

				this.pcs.firePropertyChange("position", new Position(this.waterColumn.getLatLonCenter(this.lastIndex), 0f), new Position(this.waterColumn.getLatLonCenter(newIndex), 0f));

				this.lastIndex = newIndex;
				firePropertyChange(AVKey.LAYER, null, this);
				WorldWindowGLCanvas wwd = Viewer3D.getWwd();
				if (wwd != null) {
					wwd.redraw();
				}
			}
		}
	}

	@Override
	protected void doRender(DrawContext dc) {
		if (this.texturedWall == null) {
			initialize();
		}

		if (dc != null && this.texturedWall != null) {
			this.texturedWall.updateEyeDistance(dc);
			dc.addOrderedRenderable(this.texturedWall);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOpacity(double opacity) {
		if (this.texturedWall != null) {
			this.texturedWall.setOpacity(opacity);
		}
	}

	public boolean isHide() {
		return ((VolatilHideableTexturedWall) this.texturedWall).isHide();
	}

	public void setHide(boolean hide) {
		((VolatilHideableTexturedWall) this.texturedWall).setHide(hide);
		Viewer3D.getWwd().display();
	}

	@Override
	public String getPlayableType() {
		return this.playableType;
	}

}
