package fr.ifremer.viewer3d.layers.xml;

import fr.ifremer.viewer3d.loaders.SubLayer;

/***
 * Defines an operation between textures contained in a {@link MultiTextureTile}
 * examples : subtraction, filtering
 * 
 * @author MORVAN
 * 
 */
public abstract class Operation {
	public static String Filtering = "Filtering";

	/***
	 * Displaying name of the operation
	 */
	private String name;

	/***
	 * 
	 * Name of the operation to perform Used to select operation in shader
	 */
	private String typeOfOperation;

	/***
	 * true if operation must be used
	 */
	private boolean used = false;

	/***
	 * define the operation name and the type of operation to perform
	 * 
	 * @param name
	 * @param typeOfOperation
	 */
	public Operation(String name, String typeOfOperation) {
		this.name = name;
		this.typeOfOperation = typeOfOperation;
	}

	/***
	 * 
	 * @return the displaying name of the operation
	 */
	public String getName() {
		return name;
	}

	/***
	 * 
	 * @return the name of the operation to perform Used to select operation in
	 *         shader
	 */
	public String getTypeOfOperation() {
		return typeOfOperation;
	}

	/***
	 * 
	 * @return true if operation must be used
	 */
	public boolean isUsed() {
		return used;
	}

	/***
	 * {@link SubLayer} rendering must use the operation?
	 * 
	 * @param used
	 */
	public void setUsed(boolean used) {
		this.used = used;
	}

}
