package fr.ifremer.viewer3d.layers.xml;

import fr.ifremer.viewer3d.geom.MovableSector;
import fr.ifremer.viewer3d.layers.IMovableLayer;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.render.DrawContext;

/***
 * Abstract layer class defining the date and SubLayer/SubElevation selected
 * 
 * @author MORVAN
 * 
 */
public abstract class AbstractMultiDataLayer extends AbstractLayer implements Layer, IMovableLayer {

	/***
	 * min date for which a texture is defined for previous date, use the
	 * associated texture
	 */
	private int dateMin;
	/***
	 * max date for which a texture is defined for later date, use the
	 * associated texture
	 */
	private int dateMax;

	/***
	 * Step between to date
	 */
	private int dateStep;

	/***
	 * date index
	 */
	protected int dateIndex = 0;

	/***
	 * Offset used to render data on date line
	 */
	protected double dateLineLongitudeOffset = 0.0;

	/***
	 * {@link ISectorMovementProcessing} used for simulation for exemple
	 * Tectonic movement
	 * 
	 * @see TectonicMovement
	 */
	protected ISectorMovementProcessing movement = null;

	/***
	 * 
	 * @param dateMin
	 * @param dateMax
	 * @param dateStep
	 */
	public AbstractMultiDataLayer(String name, int dateMin, int dateMax, int dateStep) {
		setName(name);
		this.setDateMin(dateMin);
		this.setDateMax(dateMax);
		this.setDateStep(dateStep);
		this.setDateIndex(dateMin);
	}

	@Override
	protected abstract void doRender(DrawContext dc);

	/***
	 * min date for which a texture is defined for previous date, use the
	 * associated texture
	 */
	public int getDateMin() {
		return dateMin;
	}

	/***
	 * min date for which a texture is defined for previous date, use the
	 * associated texture
	 */
	public void setDateMin(int dateMin) {
		this.dateMin = dateMin;
	}

	/***
	 * max date for which a texture is defined for later date, use the
	 * associated texture
	 */
	public int getDateMax() {
		return dateMax;
	}

	/***
	 * max date for which a texture is defined for later date, use the
	 * associated texture
	 */
	public void setDateMax(int dateMax) {
		this.dateMax = dateMax;
	}

	/***
	 * list of the date for which at least one texture is defined
	 */
	public int getDateIndex() {
		return dateIndex;
	}

	/***
	 * list of the date for which at least one texture is defined
	 */
	public void setDateIndex(int dateIndex) {
		this.dateIndex = dateIndex;
	}

	public int getDateStep() {
		return dateStep;
	}

	public void setDateStep(int dateStep) {
		this.dateStep = dateStep;
	}

	@Override
	public Sector move(Sector st) {
		// trick for date line
		if (dateLineLongitudeOffset != 0) {
			if (st.getMaxLongitude().addDegrees(dateLineLongitudeOffset).getDegrees() < -178) {
				System.out.print("special sector");
			}
			// return rotate(s, false);
			if (st.getMinLongitude().addDegrees(dateLineLongitudeOffset).getDegrees() > 180) {
				// System.out.print("dateLine");
				st = new Sector(st.getMinLatitude(), st.getMaxLatitude(), st.getMinLongitude().addDegrees(dateLineLongitudeOffset - 360), st.getMaxLongitude()
						.addDegrees(dateLineLongitudeOffset - 360));
			} else if (st.getMaxLongitude().addDegrees(dateLineLongitudeOffset).getDegrees() > 180) {
				MovableSector sector = new MovableSector(st.getMinLatitude(), st.getMaxLatitude(), st.getMinLongitude().addDegrees(dateLineLongitudeOffset), st.getMaxLongitude().addDegrees(
						dateLineLongitudeOffset - 360));
				sector.setWidth(st.getDeltaLonDegrees());
				st = sector;
			} else {
				st = new Sector(st.getMinLatitude(), st.getMaxLatitude(), st.getMinLongitude().addDegrees(dateLineLongitudeOffset), st.getMaxLongitude().addDegrees(dateLineLongitudeOffset));
			}
		}

		if (movement != null) {
			st = movement.move(st);
		}

		return st;
	}

	@Override
	public LatLon move(LatLon latLon) {
		if (movement != null) {
			return movement.move(latLon);
		} else {
			return latLon;
		}
	}

	@Override
	public Sector moveInv(Sector st) {
		// trick for date line
		if (dateLineLongitudeOffset != 0) {
			// return rotate(s, false);
			if (st.getMinLongitude().addDegrees(-dateLineLongitudeOffset).getDegrees() > 180)

			{
				// System.out.print("dateLine");
				st = new Sector(st.getMinLatitude(), st.getMaxLatitude(), st.getMinLongitude().addDegrees(-dateLineLongitudeOffset - 360), st.getMaxLongitude().addDegrees(
						-dateLineLongitudeOffset - 360));
			} else {
				st = new Sector(st.getMinLatitude(), st.getMaxLatitude(), st.getMinLongitude().addDegrees(-dateLineLongitudeOffset), st.getMaxLongitude().addDegrees(-dateLineLongitudeOffset));
			}
		}

		if (movement != null) {
			return movement.moveInv(st);
		} else {
			return st;
		}
	}

	@Override
	public LatLon moveInv(LatLon latLon) {
		if (movement != null) {
			return movement.moveInv(latLon);
		} else {
			return latLon;
		}
	}

	@Override
	public ISectorMovementProcessing getMovement() {
		return movement;
	}

	@Override
	public void setMovement(ISectorMovementProcessing movement) {
		this.movement = movement;
	}

	/***
	 * Offset used to render data on date line
	 */
	public double getDateLineLongitudeOffset() {
		return dateLineLongitudeOffset;
	}

	/***
	 * Offset used to render data on date line
	 */
	public void setDateLineLongitudeOffset(double dateLineLongitudeOffset) {
		this.dateLineLongitudeOffset = dateLineLongitudeOffset;
	}

}
