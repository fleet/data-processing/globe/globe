package fr.ifremer.viewer3d.layers.xml;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWOldMultiTextureLayer;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.geom.MovableSector;
import fr.ifremer.viewer3d.layers.ILayerParameters;
import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.loaders.SubLayer;
import fr.ifremer.viewer3d.multidata.SubLayerParameters;
import fr.ifremer.viewer3d.multidata.TiledMultiTextureProducer;
import fr.ifremer.viewer3d.render.MultiTextureRenderer;
import fr.ifremer.viewer3d.terrain.MultiElevationModel;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.PerformanceStatistic;
import gov.nasa.worldwind.util.Tile;

/***
 * Layer containing multitextures {@link SubLayer} for which operations {@link Operation} could be performed.<br/>
 * {@link SubLayer} parameters are saved as a list of {@link SubLayerParameters}<br/>
 * Those textures are indexed by date in SubLayer to perform evolution. <br/>
 * A mask is available to select the area of interest.<br/>
 * Rendering is done through {@link MultiTextureRenderer}<br/>
 * <br/>
 * Warning : {@link MultiTextureLayer}'s {@link SubLayer} must be synchronize</br>
 * ie : DTM gridding algorithms must be the same for all SubLayers
 * 
 * @author MORVAN
 * 
 */
public class MultiTextureLayer extends AbstractMultiTextureTiledImageLayer
		implements ITarget, IDeletable, ILayerParameters, IWWOldMultiTextureLayer {
	protected Logger logger = LoggerFactory.getLogger(MultiTextureLayer.class);

	/***
	 * Shader program used to perform multitexturing and {@link Operation}
	 */
	protected String vertex_shader = "/shader/multiTextureShader.vert";
	protected String fragment_shader = "/shader/multiTextureShader.frag";

	/***
	 * used to update heading in IHM
	 */
	public static String view_Heading = "View Heading for multi textures shading";

	private static Map<GL, Integer> shaderprograms = new HashMap<GL, Integer>(4);

	private Composite composite;

	/***
	 * View heading used to update orientation in azimuth composite according to view hheading
	 */
	protected double viewHeading = 0;

	/***
	 * List of group of {@link SubLayer}. {@link Operation} is perform between {@link SubLayer} of a group.
	 */
	private List<String> productIDs = new ArrayList<String>();

	/***
	 * {@link SubLayer} with such an groupID is member of all groupIDs
	 */
	public static String allGroupId = "all";

	/***
	 * List of {@link SubLayer} data types used to determine the availability of an {@link Operation} Some
	 * {@link Operation} needs 2 data types to be available
	 */
	private List<String> dataTypes = new ArrayList<String>();

	/***
	 * list of {@link SubLayer} available for this layer
	 */
	private List<SubLayer> subLayers = new ArrayList<SubLayer>();

	/***
	 * list of {@link SubLayer} available for this layer classified by GroupID
	 */
	private HashMap<String, List<SubLayer>> subLayersByGroupID = new HashMap<String, List<SubLayer>>();

	/***
	 * list of {@link SubLayer} which are members of all groupIDs
	 */
	private HashMap<String, SubLayer> allGroupIDs = new HashMap<String, SubLayer>();

	/***
	 * list of {@link SubLayer} available for this layer classified by Type
	 */
	private HashMap<String, List<SubLayer>> subLayersByType = new HashMap<String, List<SubLayer>>();

	/***
	 * {@link SubLayer} selected in UI
	 */
	private SubLayer currentSubLayer = null;

	/***
	 * List of tiles to be rendered
	 */
	protected ArrayList<MultiTextureTile> currentTiles = new ArrayList<MultiTextureTile>();

	/***
	 * Current tile to be loaded
	 */
	protected MultiTextureTile currentResourceTile;

	/***
	 * Resolution in degree used to sort layers and computes shading according to the resolution
	 */
	private double resolution;

	private boolean init = false;

	/***
	 * Tiles on which histogram will be computed
	 */
	static private List<MultiTextureTile> listTile = new ArrayList<MultiTextureTile>();
	/***
	 * current visible sector needed to compute histogram
	 */
	private static Sector visibleSector = null;

	private String layerName;

	MultiTextureLayerParameters parameters;

	/***
	 * List of displayed {@link SubLayer}
	 */
	private List<SubLayer> subLayersToBeRendered = null;
	/***
	 * List of {@link SubLayer} needed for filtering
	 */
	private List<SubLayer> subLayersForFilter = null;
	/***
	 * List of {@link SubLayer} needed for second filtering
	 */
	private List<SubLayer> subLayersForSecondFilter = null;

	/***
	 * {@link MultiTextureRenderer} which render the multi-textures, and which could perform the {@link Operation}
	 */
	private MultiTextureRenderer render = new MultiTextureRenderer(this);

	/***
	 * Elevation model associated with layer
	 */
	private MultiElevationModel multiElevationModel;

	/***
	 * Missing textureData replacement by transparent textureData
	 */
	private static TextureData missingTextureData = null;

	private static int NBTEXTUREUNITUSED = 5;

	// ////////////////////////////////////////////////////////////////////
	// ///////////////////////Constructors/////////////////////////////////
	// ////////////////////////////////////////////////////////////////////
	public MultiTextureLayer(String name, int dateMin, int dateMax, int dateStep) {
		super(name, dateMin, dateMax, dateStep);
		layerName = name;
		this.infos = new AbstractInfos();
		this.setPickEnabled(false); // textures are assumed to be terrain unless
		// specifically indicated otherwise.
		this.tileCountName = this.getName() + " Tiles";

		parameters = new MultiTextureLayerParameters();
		if (currentLayer == null) {
			currentLayer = this;
		}

	}

	// ////////////////////////////////////////////////////////////////////
	// /////////////////////Standard Layer methods/////////////////////////
	// ////////////////////////////////////////////////////////////////////

	public static TextureData getMissingTexture() {
		if (missingTextureData == null) {
			BufferedImage img = new BufferedImage(TiledMultiTextureProducer.DEFAULT_TILE_WIDTH_AND_HEIGHT,
					TiledMultiTextureProducer.DEFAULT_TILE_WIDTH_AND_HEIGHT, BufferedImage.TYPE_INT_ARGB);
			GLProfile glprofile = GLProfile.getDefault();
			missingTextureData = AWTTextureIO.newTextureData(glprofile, img, false);
		}

		return missingTextureData;
	}

	@Override
	public String getName() {
		return layerName;
	}

	@Override
	public Sector getSector() {
		if (levels == null) {
			return null;
		}
		return levels.getSector();
	}

	/***
	 * resolution of the data, used to sort data by resolution and compute the shading in unit (same shading for
	 * different resolutions)
	 * 
	 * @param resolution
	 */
	public void setResolution(double resolution) {
		this.resolution = resolution;
	}

	/***
	 * the resolution in degree Used to compute an accurate shading and to sort data by resolution
	 * 
	 * @return resolution
	 */
	public double getResolution() {
		return resolution;
	}

	// //////////////////////////////////////////////////////////////////////////
	// ///////////////////////SubLayers management//////////////////////////////
	// //////////////////////////////////////////////////////////////////////////

	/***
	 * Defines the {@link OperationOnTexture} to perform
	 * 
	 * @param dataType
	 * @param operationNumber : first or second filter
	 */
	public OperationOnTexture setOperation(String operationType, String dataType, int operationNumber) {
		double min = 0.0;
		double max = 0.0;
		String unit = "";
		for (SubLayer s : subLayers) {
			if (s.getType().equals(dataType)) {
				min = s.getValMin();
				max = s.getValMax();
				unit = s.getUnit();
			}
		}

		refreshRenderingList();

		if (operationNumber != 2) {
			parameters.setOperationOnTexture(new OperationOnTexture(operationType, dataType, unit, min, max));
			return parameters.getOperationOnTexture();
		}

		parameters.setSecondOperationOnTexture(new OperationOnTexture(operationType, dataType, unit, min, max));
		return parameters.getSecondOperationOnTexture();
	}

	/***
	 * add a {@link SubLayer} to this layer {@link SubLayer} will be added to the list of subLayers and indexed by
	 * groupID
	 * 
	 * @param subLayer
	 */
	public void addSubLayer(SubLayer subLayer) {
		if (subLayer == null) {
			return;
		}

		subLayers.add(subLayer);

		// first subLayer added is current by default
		if (currentSubLayer == null) {
			currentSubLayer = subLayer;
		}

		// we need to now the list of productID
		if (!productIDs.contains(subLayer.getGroupID())) {
			productIDs.add(subLayer.getGroupID());
		}

		// we need to know the list of data type
		if (!dataTypes.contains(subLayer.getType())) {
			dataTypes.add(subLayer.getType());
		}

		List<SubLayer> l;
		// classify subLayers by groupID
		if (subLayer.getGroupID().equals(allGroupId)) {
			allGroupIDs.put(subLayer.getType(), subLayer);
		} else {
			// does one SubLayer of same groupID already exist?
			l = subLayersByGroupID.get(subLayer.getGroupID());
			if (l == null) {
				l = new ArrayList<SubLayer>();
				subLayersByGroupID.put(subLayer.getGroupID(), l);
			}
			// add subLayer to the list of groupID
			l.add(subLayer);
		}

		// classify subLayers by type
		// does one SubLayer of same type already exist?
		l = subLayersByType.get(subLayer.getType());
		if (l == null) {
			l = new ArrayList<SubLayer>();
			subLayersByType.put(subLayer.getType(), l);
		}
		// add subLayer to the list of groupID
		l.add(subLayer);

		// does SubLayer parameters are available for this data type?
		SubLayerParameters p = parameters.getSubLayerParameters(subLayer.getType());
		if (p == null) {
			parameters.setSubLayerParameters(subLayer.getType(), subLayer.getParameters());
		}
	}

	/***
	 * 
	 * @return the list of {@link SubLayer}
	 */
	public List<SubLayer> getSubLayers() {
		return subLayers;
	}

	/***
	 * 
	 * @return the list of {@link SubLayer} indexed by GroupID Useful to send textures groupID by groupID to the graphic
	 *         card
	 * @see getSubLayersToBeRendered
	 */
	public HashMap<String, List<SubLayer>> getSubLayersByGroupID() {
		return subLayersByGroupID;
	}

	/***
	 * 
	 * @return the list of {@link SubLayer} indexed by type
	 * @see getSubLayersToBeRendered
	 */
	public HashMap<String, List<SubLayer>> getSubLayersByType() {
		return subLayersByType;
	}

	/***
	 * refresh lists of rendered {@link SubLayer} </br>
	 */
	public void refreshRenderingList() {
		subLayersToBeRendered = null;
		subLayersForFilter = null;
		subLayersForSecondFilter = null;
	}

	/***
	 * 
	 * @return list of {@link SubLayer} needed for rendering </br>
	 *         ie : sum of getSubLayersToBeRendered, getSubLayersForFiltering and getSubLayersForSecondFiltering
	 */
	public List<SubLayer> getNeededSubLayers() {
		List<SubLayer> sub = new ArrayList<SubLayer>();
		sub.addAll(getSubLayersToBeRendered());
		sub.addAll(getSubLayersForFiltering().getSecond());
		sub.addAll(getSubLayersForSecondFiltering().getSecond());
		return sub;
	}

	/***
	 * 
	 * @return the ordered list of {@link SubLayer} to be rendered for the current {@link Operation}
	 */
	public List<SubLayer> getSubLayersToBeRendered() {

		List<SubLayer> sub = new ArrayList<SubLayer>();
		// not mosaicing
		if (!parameters.isDisplaySameType()) {
			sub.add(currentSubLayer);
		} else { // mosaicing
			List<SubLayer> subLayerOfSameType = subLayersByType.get(currentSubLayer.getType());
			int currentIndex = subLayerOfSameType.indexOf(currentSubLayer);
			int sizeTypeList = subLayerOfSameType.size();

			// Case of subLayerOfSameType.size is < of the NBTEXTUREUNITUSED
			if (subLayerOfSameType.size() <= NBTEXTUREUNITUSED) {
				for (SubLayer s : subLayerOfSameType) {
					if (s != currentSubLayer) {
						sub.add(s);
					}
				}
			}
			// General case
			else if (currentIndex >= NBTEXTUREUNITUSED / 2 && currentIndex < (sizeTypeList - NBTEXTUREUNITUSED / 2)) {
				for (int i = currentIndex - NBTEXTUREUNITUSED / 2; i <= currentIndex + NBTEXTUREUNITUSED / 2; i++) {
					if (subLayerOfSameType.get(i) != currentSubLayer) {
						sub.add(subLayerOfSameType.get(i));
					}
				}

			} // Case of currentSubLayer on top of subLayerOfSameType
			else if (currentIndex < NBTEXTUREUNITUSED / 2) {
				for (int i = 0; i < NBTEXTUREUNITUSED; i++) {
					if (subLayerOfSameType.get(i) != currentSubLayer) {
						sub.add(subLayerOfSameType.get(i));
					}
				}

			} // Case of currentSubLayer on bottom of subLayerOfSameType
			else if (currentIndex >= (sizeTypeList - NBTEXTUREUNITUSED / 2)) {
				for (int i = (sizeTypeList - NBTEXTUREUNITUSED); i < sizeTypeList; i++) {
					if (subLayerOfSameType.get(i) != currentSubLayer) {
						sub.add(subLayerOfSameType.get(i));
					}
				}
			}

			sub.add(currentSubLayer); // current subLayer on botom of the list
			// to display it in last position

			if (sizeTypeList > NBTEXTUREUNITUSED && sub.size() != NBTEXTUREUNITUSED) {
				logger.debug("The size of list of sublayer is different of the expected size");
			}
		}

		subLayersToBeRendered = sub;
		return subLayersToBeRendered;
	}

	/***
	 * 
	 * @return the ordered list of {@link SubLayer} used as filter for the current {@link Operation}
	 */
	public Pair<Operation, List<SubLayer>> getSubLayersForFiltering() {

		// if(subLayersForFilter == null)
		{
			List<SubLayer> sub = new ArrayList<SubLayer>();
			if (parameters.isUseTextureFilter() && parameters.getOperationOnTexture() != null) {
				// case Operation texture type => allGroupID texture
				SubLayer allGroupIDSubLayer = allGroupIDs.get(parameters.getOperationOnTexture().getType());
				if (allGroupIDSubLayer != null) {
					sub.add(allGroupIDSubLayer);
					// case with same groupID
				} else {
					for (SubLayer s : subLayersByGroupID.get(currentSubLayer.getGroupID())) {
						if (s.getType().equals(parameters.getOperationOnTexture().getType())) {
							// add the texture filter of current sublayer first
							sub.add(s);
							break;
						}
					}

					// add filter for others if needed
					if (parameters.isDisplaySameType()) {
						for (SubLayer s : subLayersByType.get(parameters.getOperationOnTexture().getType())) {
							if (!(s.getGroupID().equals(currentSubLayer.getGroupID()))
									&& s.getType().equals(parameters.getOperationOnTexture().getType())) {
								// add other texture filter
								sub.add(s);
							}
						}
					}
				}
			}
			subLayersForFilter = sub;
		}
		return new Pair<Operation, List<SubLayer>>(parameters.getOperationOnTexture(), subLayersForFilter);
	}

	/***
	 * 
	 * @return the ordered list of {@link SubLayer} used as second filter for the second {@link Operation}
	 */
	public Pair<Operation, List<SubLayer>> getSubLayersForSecondFiltering() {

		// if(subLayersForSecondFilter == null)
		{
			List<SubLayer> sub = new ArrayList<SubLayer>();
			if (parameters.isUseSecondTextureFilter() && parameters.getSecondOperationOnTexture() != null) {
				// case Operation texture type => allGroupID texture
				SubLayer allGroupIDSubLayer = allGroupIDs.get(parameters.getSecondOperationOnTexture().getType());
				if (allGroupIDSubLayer != null) {
					sub.add(allGroupIDSubLayer);
					// case with same groupID
				} else {
					for (SubLayer s : subLayersByGroupID.get(currentSubLayer.getGroupID())) {
						if (s.getType().equals(parameters.getSecondOperationOnTexture().getType())) {
							// add the texture filter of current sublayer first
							sub.add(s);
							break;
						}
					}

					// add filter for others if needed
					if (parameters.isDisplaySameType()) {
						for (SubLayer s : subLayersByType.get(parameters.getSecondOperationOnTexture().getType())) {
							if (!(s.getGroupID().equals(currentSubLayer.getGroupID()))
									&& s.getType().equals(parameters.getSecondOperationOnTexture().getType())) {
								// add other texture filter
								sub.add(s);
							}
						}
					}
				}
			}
			subLayersForSecondFilter = sub;
		}
		return new Pair<Operation, List<SubLayer>>(parameters.getSecondOperationOnTexture(), subLayersForSecondFilter);
	}

	/***
	 * 
	 * @return the texture to be rendered
	 */
	public SubLayer getCurrentSubLayer() {
		return currentSubLayer;
	}

	/***
	 * set the texture to be rendered
	 * 
	 * @param currentSubLayer
	 */
	public void setCurrentSubLayer(SubLayer currentSubLayer) {
		if (currentSubLayer != null) {
			this.parameters.setCurrentSubLayerIndex(subLayers.indexOf(currentSubLayer));
			this.currentSubLayer = currentSubLayer;

			refreshRenderingList();
		}
	}

	/***
	 * set the texture to be rendered
	 * 
	 * @param index
	 */
	public void setCurrentSubLayer(int index) {
		if (index >= subLayers.size()) {
			index = 0;
		}
		this.currentSubLayer = subLayers.get(index);
		this.parameters.setCurrentSubLayerIndex(index);

		refreshRenderingList();

	}

	public static MultiTextureLayer getCurrent() {
		return currentLayer;
	}

	/***
	 * Defines current {@link MultiTextureLayer} corresponding to selected Composite
	 * 
	 * @param layer
	 */
	public static void setCurrent(MultiTextureLayer layer) {
		currentLayer = layer;
	}

	/***
	 * List of {@link SubLayer} data types used to determine the availability of an {@link Operation} Some
	 * {@link Operation} needs 2 data types to be available
	 */
	public List<String> getDataTypes() {
		return dataTypes;
	}

	/***
	 * Select the {@link SubLayer} of dataType with same groupID than current {@link SubLayer} if none choose first
	 * {@link SubLayer} of dataType if none current SubLayer remains selected
	 * 
	 * @param dataType
	 */
	public void selectTypeSubLayer(String dataType) {
		for (SubLayer subLayer : subLayers) {
			if (subLayer.getType().equals(dataType) && subLayer.getGroupID().equals(currentSubLayer.getGroupID())) {
				currentSubLayer = subLayer;
				return;
			}
		}

		for (SubLayer subLayer : subLayers) {
			if (subLayer.getType().equals(dataType)) {
				currentSubLayer = subLayer;
				return;
			}
		}

		refreshRenderingList();
	}

	/***
	 * Use an {@link OperationOnTexture} for filtering?
	 * 
	 * @param parameters .isUseTextureFilter()
	 * @param operationNumber : first or second filter
	 */
	public void setUseOperationTextureFilter(boolean useTextureFilter, int operationNumber) {
		if (operationNumber != 2) {
			this.parameters.setUseTextureFilter(useTextureFilter);
		} else {
			this.parameters.setUseSecondTextureFilter(useTextureFilter);
		}

		refreshRenderingList();
	}

	/***
	 * Use an {@link OperationOnTexture} for filtering?
	 * 
	 * @return parameters.isUseTextureFilter()
	 */
	public boolean isUseOperationTextureFilter() {
		return this.parameters.isUseTextureFilter();
	}

	/***
	 * Use second {@link OperationOnTexture} for filtering?
	 * 
	 * @return parameters.isUseTextureFilter()
	 */
	public boolean isUseSecondOperationTextureFilter() {
		return this.parameters.isUseSecondTextureFilter();
	}

	// //////////////////////////////////////////////////////////////////////////
	// ///////////////////////rendering//////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////
	@Override
	protected void doRender(DrawContext dc) {
		int indexDate = getDateIndex();
		if (!this.isInitialized()) {
			this.initialize(dc);

			// Check number of texture units available
			IntBuffer value = IntBuffer.allocate(1);
			dc.getGL().getGL2().glGetIntegerv(GL2ES2.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, value);
			int nbTextureUnitAvailables = value.get(0);
			logger.info("nbTextureUnitAvailables = " + nbTextureUnitAvailables);
		}

		GL2 gl = dc.getGL().getGL2();
		if (!shaderprograms.containsKey(gl)) {
			initialize(dc);
		}

		// update IHM Azimuth linked to view heading
		firePropertyChange(view_Heading, this.viewHeading,
				viewHeading = Viewer3D.getWwd().getView().getHeading().getDegrees());

		if (this.forceLevelZeroLoads && !this.levelZeroLoaded) {
			this.loadAllTopLevelTextures(dc, indexDate);
		}
		if (dc.getSurfaceGeometry() == null || dc.getSurfaceGeometry().size() < 1) {
			return;
		}

		draw(dc, indexDate);
	}

	protected void draw(DrawContext dc, int indexDate) {
		this.checkResources(); // Check whether resources are present, and
		// perform any necessary initialization.

		this.assembleTiles(dc, indexDate); // Determine the tiles to draw.

		if (this.currentTiles.size() >= 1) {
			if (this.getScreenCredit() != null) {
				dc.addScreenCredit(this.getScreenCredit());
			}

			MultiTextureTile[] sortedTiles = new MultiTextureTile[this.currentTiles.size()];
			sortedTiles = this.currentTiles.toArray(sortedTiles);
			Arrays.sort(sortedTiles, levelComparer);

			GL2 gl = dc.getGL().getGL2();

			if (this.isUseTransparentTextures() || this.getOpacity() < 1) {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT | GL2.GL_CURRENT_BIT);
				this.setBlendingFunction(dc);
			} else {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT);
			}

			gl.glPolygonMode(GL.GL_FRONT, GL2GL3.GL_FILL);
			gl.glEnable(GL.GL_CULL_FACE);
			gl.glCullFace(GL.GL_BACK);

			dc.setPerFrameStatistic(PerformanceStatistic.IMAGE_TILE_COUNT, this.tileCountName,
					this.currentTiles.size());

			render.renderTiles(dc, this.currentTiles, indexDate);

			gl.glPopAttrib();

			// Check texture expiration. Memory-cached textures are checked for
			// expiration only when an explicit,
			// non-zero expiry time has been set for the layer. If none has been
			// set, the expiry times of the layer's
			// individual levels are used, but only for images in the local file
			// cache, not textures in memory. This is
			// to avoid incurring the overhead of checking expiration of
			// in-memory textures, a very rarely used feature.
			if (this.getExpiryTime() > 0 && this.getExpiryTime() < System.currentTimeMillis()) {
				this.checkTextureExpiration(dc, this.currentTiles, indexDate);
			}

			this.currentTiles.clear();
		}

		this.sendRequests();
		this.requestQ.clear();
	}

	/***
	 * 
	 * @param gl
	 * @return shader program for current gl
	 */
	public Integer getShaderprograms(GL gl) {
		if (shaderprograms != null && shaderprograms.get(gl) != null) {
			return shaderprograms.get(gl);
		} else {
			return 0;
		}
	}

	/***
	 * Initialize shader program
	 * 
	 * @param dc
	 */
	public void initialize(DrawContext dc) {
		// Création Shader
		try {
			GL2 gl = dc.getGL().getGL2();
			if (!shaderprograms.containsKey(gl)) {
				shaderprograms.put(gl, ShaderUtil.createShader(this, gl, vertex_shader, fragment_shader));
			}
		} catch (GIOException e) {
			logger.error("Error reading fragment shader : ", e);
		}

		this.init = true;
	}

	/***
	 * 
	 * @return true if shader program is initialized
	 */
	public boolean isInitialized() {
		if (this.init == true) {
			return true;
		} else {
			return false;
		}
	}

	// //////////////////////////////////////////////////////////////////////////
	// ///////////////////////Histogram /
	// Contrast///////////////////////////////
	// //////////////////////////////////////////////////////////////////////////

	static public double facteurRehaussement = -1; // -1==> pas de rehaussement
	public static double staticRehaussementMin;
	public static double staticRehaussementMax;
	public static double staticMin;
	public static double staticMax;

	static public int[] histogram;
	static private final int HISTOGRAM_SIZE = 256;

	static private boolean createHisto;
	static public String typeHisto = new String();

	/***
	 * used to update contrast 0.5% in IHM
	 */
	public static String Contrast_05 = "Contrast 0.5%";
	/***
	 * used to update contrast 1% in IHM
	 */
	public static String Contrast_1 = "Contrast 1%";
	/***
	 * used to update contrast minmax in IHM
	 */
	public static String Contrast_MinMax = "Contrast min max";

	/***
	 * Layer on which shortcuts are applied
	 */
	private static MultiTextureLayer currentLayer = null;

	/***
	 * contrast 1%
	 */
	public void contrast1PerCent() {
		MultiTextureLayer.facteurRehaussement = 0.01; // rehaussement a 1%
		parameters.getSubLayerParameters(currentSubLayer.getType()).setContrastLevel(2);
		contrastComputation();

		firePropertyChange(Contrast_1, MultiTextureLayer.facteurRehaussement,
				MultiTextureLayer.facteurRehaussement = 0.0);
	}

	/**
	 * contrast 0.5%
	 */
	public void contrast05PerCent() {
		MultiTextureLayer.facteurRehaussement = 0.005; // rehaussement a 0.5%
		parameters.getSubLayerParameters(currentSubLayer.getType()).setContrastLevel(1);
		contrastComputation();

		firePropertyChange(Contrast_05, MultiTextureLayer.facteurRehaussement,
				MultiTextureLayer.facteurRehaussement = 0.0);
	}

	/***
	 * contrast min max
	 */
	public void contrastMinMax() {
		facteurRehaussement = -1;
		parameters.getSubLayerParameters(currentSubLayer.getType()).setContrastLevel(0);
		contrastComputation();

		firePropertyChange(Contrast_MinMax, MultiTextureLayer.facteurRehaussement,
				MultiTextureLayer.facteurRehaussement = 0.0);

	}

	/***
	 * Computes contrast according to facteurRehaussement
	 */
	private void contrastComputation() {
		if (getCurrentSubLayer() == null) {
			return;
		}
		MultiTextureLayer.typeHisto = getCurrentSubLayer().getType();

		staticRehaussementMin = getCurrentSubLayer().getValMin();
		staticRehaussementMax = getCurrentSubLayer().getValMax();

		if (facteurRehaussement != -1) {

			if (!MultiTextureLayer.isCreateHisto()) {
				MultiTextureLayer.preComputeHisto();
			}

			// call redraw to add tile in tileList to compute histogramm
			Viewer3D.getWwd().redrawNow();

			if (MultiTextureLayer.isCreateHisto()) {
				MultiTextureLayer.computeHisto();
			}
		}

		parameters.getSubLayerParameters(currentSubLayer.getType()).setMinContrast(staticRehaussementMin);
		parameters.getSubLayerParameters(currentSubLayer.getType()).setMaxContrast(staticRehaussementMax);
		Viewer3D.getWwd().redraw();
	}

	/***
	 * Clean list of tiles on which histogram must be computed
	 */
	public static void preComputeHisto() {
		listTile.clear();
		histogram = null;
		setCreateHisto(true);
	}

	/***
	 * Computes histogram according to displayed {@link SubLayer} and "transparency contrast"
	 */
	public static void computeHisto() {
		setCreateHisto(false);

		double min = Integer.MAX_VALUE;
		double max = -Integer.MAX_VALUE;
		double taille_totale = 0;

		// Cache
		File fileStoreLocation = WorldWindUtils.getCacheLocation();

		int r, g, b, a;
		double val, vald;
		BufferedImage img = null;
		File f;
		Sector tileSector, intersect;
		int size;
		int beginWidth, endWidth, beginHeight, endHeight;

		// MIN / MAX / TOTAL
		for (MultiTextureTile tile : listTile) {
			if (tile.getPath() != null && tile.getPath().endsWith(".png")) {
				MultiTextureLayer layerTile = tile.getLayer();
				SubLayer currentSubLayer = layerTile.getCurrentSubLayer();
				SubLayerParameters parameters = layerTile.parameters.getSubLayerParameters(currentSubLayer.getType());

				List<SubLayer> subLayersForHisto = new ArrayList<SubLayer>();
				if (layerTile.getLayerParameters().isDisplaySameType()) {
					subLayersForHisto = layerTile.subLayersByType.get(currentSubLayer.getType());
				} else {
					subLayersForHisto.add(currentSubLayer);
				}

				for (SubLayer s : subLayersForHisto) {
					f = new File(getCache(fileStoreLocation.getPath() + "/" + tile.getPath(), s, layerTile.dateIndex));
					if (f.exists()) {
						Image src = new ImageIcon(f.getAbsolutePath()).getImage();
						img = new BufferedImage(src.getWidth(null), src.getHeight(null), BufferedImage.TYPE_INT_ARGB);
						img.getGraphics().drawImage(src, 0, 0, null);

						// find intersection between visiblesector and tile
						tileSector = tile.getSector();
						intersect = tile.getSector().intersection(visibleSector);

						size = src.getWidth(null);

						// position of intersection as pixel position in canvas
						beginWidth = (int) ((intersect.getMinLongitude().degrees - tileSector.getMinLongitude().degrees)
								/ tileSector.getDeltaLonDegrees() * size);
						endWidth = (int) ((intersect.getMaxLongitude().degrees - tileSector.getMinLongitude().degrees)
								/ tileSector.getDeltaLonDegrees() * size);

						beginHeight = -(int) ((intersect.getMaxLatitude().degrees - tileSector.getMaxLatitude().degrees)
								/ tileSector.getDeltaLatDegrees() * size);
						endHeight = -(int) ((intersect.getMinLatitude().degrees - tileSector.getMaxLatitude().degrees)
								/ tileSector.getDeltaLatDegrees() * size);

						for (int width = beginWidth; width < endWidth; width++) {
							for (int height = beginHeight; height < endHeight; height++) {
								a = img.getRaster().getSample(width, height, 3); // transparence

								if (a == 255) {
									r = img.getRaster().getSample(width, height, 0);
									g = img.getRaster().getSample(width, height, 1);
									b = img.getRaster().getSample(width, height, 2);
									val = r * 256 * 256 + g * 256 + b;

									vald = (val) / (256.0 * 256.0 * 256.0)
											* (currentSubLayer.getValMax() - currentSubLayer.getValMin())
											+ currentSubLayer.getValMin();

									// only visible data are considered
									// "Transparency contrast" taken into
									// account
									if (vald < min && vald >= parameters.getMinTransparency()
											&& vald <= parameters.getMaxTransparency()) {
										min = vald;
									}
									if (vald > max && vald >= parameters.getMinTransparency()
											&& vald <= parameters.getMaxTransparency()) {
										max = vald;
									}
									taille_totale++;
								}
							}
						}
					}
				}
			}
		}

		staticMin = min;
		staticMax = max;

		// HISTOGRAM
		histogram = new int[HISTOGRAM_SIZE];
		for (MultiTextureTile tile : listTile) {
			if (tile.getPath() != null && tile.getPath().endsWith(".png")) {
				MultiTextureLayer layerTile = tile.getLayer();
				SubLayer currentSubLayer = layerTile.getCurrentSubLayer();
				SubLayerParameters parameters = layerTile.parameters.getSubLayerParameters(currentSubLayer.getType());

				List<SubLayer> subLayersForHisto = new ArrayList<SubLayer>();
				if (layerTile.getLayerParameters().isDisplaySameType()) {
					subLayersForHisto = layerTile.subLayersByType.get(currentSubLayer.getType());
				} else {
					subLayersForHisto.add(currentSubLayer);
				}

				for (SubLayer s : subLayersForHisto) {
					f = new File(getCache(fileStoreLocation.getPath() + "/" + tile.getPath(), s, layerTile.dateIndex));

					if (f.exists()) {
						Image src = new ImageIcon(f.getAbsolutePath()).getImage();
						img = new BufferedImage(src.getWidth(null), src.getHeight(null), BufferedImage.TYPE_INT_ARGB);
						img.getGraphics().drawImage(src, 0, 0, null);

						// find intersection between visiblesector and tile
						tileSector = tile.getSector();
						intersect = tile.getSector().intersection(visibleSector);

						size = src.getWidth(null);

						// position of intersection as pixel position in canvas
						beginWidth = (int) ((intersect.getMinLongitude().degrees - tileSector.getMinLongitude().degrees)
								/ tileSector.getDeltaLonDegrees() * size);
						endWidth = (int) ((intersect.getMaxLongitude().degrees - tileSector.getMinLongitude().degrees)
								/ tileSector.getDeltaLonDegrees() * size);

						beginHeight = -(int) ((intersect.getMaxLatitude().degrees - tileSector.getMaxLatitude().degrees)
								/ tileSector.getDeltaLatDegrees() * size);
						endHeight = -(int) ((intersect.getMinLatitude().degrees - tileSector.getMaxLatitude().degrees)
								/ tileSector.getDeltaLatDegrees() * size);

						for (int width = beginWidth; width < endWidth; width++) {
							for (int height = beginHeight; height < endHeight; height++) {
								a = img.getRaster().getSample(width, height, 3);
								if (a == 255) {
									r = img.getRaster().getSample(width, height, 0);
									g = img.getRaster().getSample(width, height, 1);
									b = img.getRaster().getSample(width, height, 2);
									val = r * 256 * 256 + g * 256 + b;

									vald = (val) / (256.0 * 256.0 * 256.0)
											* (currentSubLayer.getValMax() - currentSubLayer.getValMin())
											+ currentSubLayer.getValMin();
									int result = (int) (((vald) - (min)) / ((max - min)) * 255.0);

									// only visible data are considered
									// "Transparency contrast" taken into
									// account
									if (vald >= parameters.getMinTransparency()
											&& vald <= parameters.getMaxTransparency()) {
										if (result >= 0 && result < HISTOGRAM_SIZE) {
											histogram[result]++;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		// REHAUSSEMENT
		if (facteurRehaussement != -1) {
			// calcul histogramme cumulé
			int[] histogram_cumule = new int[HISTOGRAM_SIZE];
			for (int i = 0; i < HISTOGRAM_SIZE; i++) {
				for (int j = 0; j < i + 1; j++) {
					histogram_cumule[i] += histogram[j];
				}
			}

			double borne_min = taille_totale * facteurRehaussement / 2;
			double borne_max = taille_totale - taille_totale * facteurRehaussement / 2;

			for (int i = 0; i < HISTOGRAM_SIZE; i++) {
				if (borne_min < histogram_cumule[i]) {
					borne_min = (i) / (255.0) * (max - min) + (min);
					break;
				}
			}
			for (int i = 0; i < HISTOGRAM_SIZE; i++) {
				if (borne_max < histogram_cumule[i]) {
					borne_max = (i) / (255.0) * (max - min) + (min);
					break;
				}
			}

			if (borne_min < min) {
				borne_min = min;
			}
			if (borne_max > max) {
				borne_max = max;
			}

			min = borne_min;
			max = borne_max;
			staticRehaussementMin = borne_min;
			staticRehaussementMax = borne_max;
		}

		listTile.clear();
	}

	public static void setCreateHisto(boolean createHistogramme) {
		MultiTextureLayer.createHisto = createHistogramme;
	}

	public static boolean isCreateHisto() {
		return createHisto;
	}

	public Composite getComposite() {
		return composite;
	}

	public void setComposite(Composite composite) {
		this.composite = composite;
	}

	// //////////////////////////////////////////////////////////////////////////
	// ///////////////////////Tile
	// Management////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////
	/***
	 * Add tile if visible and tile's level is sufficient </br>
	 * Otherwise, if tile visible, splits Tile until sufficient level is achieved
	 * 
	 * @param dc
	 * @param tile
	 */
	protected void addTileOrDescendants(DrawContext dc, MultiTextureTile tile, int indexDate) {
		if (this.meetsRenderCriteria(dc, tile)) {
			this.addTile(dc, tile, indexDate);
			return;
		}

		// The incoming tile does not meet the rendering criteria, so it must be
		// subdivided and those
		// subdivisions tested against the criteria.

		// All tiles that meet the selection criteria are drawn, but some of
		// those tiles will not have
		// textures associated with them either because their texture isn't
		// loaded yet or because they
		// are finer grain than the layer has textures for. In these cases the
		// tiles use the texture of
		// the closest ancestor that has a texture loaded. This ancestor is
		// called the currentResourceTile.
		// A texture transform is applied during rendering to align the sector's
		// texture coordinates with the
		// appropriate region of the ancestor's texture.

		MultiTextureTile ancestorResource = null;

		try {
			if (tile.isTextureInMemory(dc.getTextureCache(), subLayers, indexDate) || tile.getLevelNumber() == 0) {
				ancestorResource = this.currentResourceTile;
				this.currentResourceTile = tile;
			} else if (!tile.getLevel().isEmpty()) {
				// this.addTile(dc, tile);
				// return;

				// Issue a request for the parent before descending to the
				// children.
				// if (tile.getLevelNumber() < this.levels.getNumLevels())
				// {
				// // Request only tiles with data associated at this level
				// if (!this.levels.isResourceAbsent(tile))
				// this.requestTexture(dc, tile);
				// }
			}

			MultiTextureTile[] subTiles = tile.createSubTiles(this.levels.getLevel(tile.getLevelNumber() + 1));
			for (MultiTextureTile child : subTiles) {
				// consider the englobing sector for movable sector
				// becaus it could be rotated
				Sector st = child.getSector();
				if (st instanceof MovableSector) {
					// st = ((MovableSector) st).getEnglobingSector();
					for (Sector englobingSt : ((MovableSector) st).getEnglobingSector()) {
						if (this.getLevels().getSector().intersects(englobingSt) && this.isTileVisible(dc, child)) {
							this.addTileOrDescendants(dc, child, indexDate);
						}
					}
				} else {
					if (this.getLevels().getSector().intersects(st) && this.isTileVisible(dc, child)) {
						this.addTileOrDescendants(dc, child, indexDate);
					}
				}
			}
		} finally {
			if (ancestorResource != null) {
				this.currentResourceTile = ancestorResource;
			}
		}
	}

	/***
	 * Add tiles to render
	 * 
	 * @param dc
	 * @param tile
	 */
	protected void addTile(DrawContext dc, MultiTextureTile tile, int indexDate) {
		tile.setFallbackTile(null);

		if (createHisto && getCurrentSubLayer().getType() == MultiTextureLayer.typeHisto) {
			tile.setLayer(this);
			listTile.add(tile);
			// current visible sector
			visibleSector = dc.getVisibleSector();
		}

		List<SubLayer> needed = getNeededSubLayers();

		if (tile.isTextureInMemory(dc.getTextureCache(), needed, indexDate)) {
			this.addTileToCurrent(tile);
			return;
		}

		// Level 0 loads may be forced
		if (tile.getLevelNumber() == 0 && this.forceLevelZeroLoads
				&& !tile.isTextureInMemory(dc.getTextureCache(), needed, indexDate)) {
			this.forceTextureLoad(tile, indexDate);
			if (tile.isTextureInMemory(dc.getTextureCache(), needed, indexDate)) {
				this.addTileToCurrent(tile);
				return;
			}
		}

		// Tile's texture isn't available, so request it
		if (tile.getLevelNumber() < this.levels.getNumLevels()) {
			// Request only tiles with data associated at this level
			if (!this.levels.isResourceAbsent(tile)) {
				this.requestTexture(dc, tile, indexDate);
			}
		}

		// Set up to use the currentResource tile's texture
		if (this.currentResourceTile != null) {
			for (SubLayer subLayer : needed) {
				if (this.currentResourceTile.getLevelNumber() == 0 && this.forceLevelZeroLoads
						&& !this.currentResourceTile.isTextureInMemory(dc.getTextureCache(), subLayer, indexDate)
						&& !this.currentResourceTile.isTextureInMemory(dc.getTextureCache(), subLayer, indexDate)) {
					this.forceTextureLoad(this.currentResourceTile, indexDate);
				}

				if (this.currentResourceTile.isTextureInMemory(dc.getTextureCache(), subLayer, indexDate)) {
					tile.setFallbackTile(currentResourceTile);
					this.addTileToCurrent(tile);
				}
			}
		}

		// add tile we will load the texture later
		// this.addTileToCurrent(tile);
	}

	protected void addTileToCurrent(MultiTextureTile tile) {
		this.currentTiles.add(tile);
	}

	protected void createTopLevelTiles() {
		Sector sector = this.levels.getSector();

		Level level = levels.getFirstLevel();
		Angle dLat = level.getTileDelta().getLatitude();
		Angle dLon = level.getTileDelta().getLongitude();
		Angle latOrigin = this.levels.getTileOrigin().getLatitude();
		Angle lonOrigin = this.levels.getTileOrigin().getLongitude();

		// Determine the row and column offset from the common World Wind global
		// tiling origin.
		int firstRow = Tile.computeRow(dLat, sector.getMinLatitude(), latOrigin);
		int firstCol = Tile.computeColumn(dLon, sector.getMinLongitude(), lonOrigin);
		int lastRow = Tile.computeRow(dLat, sector.getMaxLatitude(), latOrigin);
		int lastCol = Tile.computeColumn(dLon, sector.getMaxLongitude(), lonOrigin);

		int nLatTiles = lastRow - firstRow + 1;
		int nLonTiles = lastCol - firstCol + 1;

		this.topLevels = new ArrayList<MultiTextureTile>(nLatTiles * nLonTiles);

		Angle p1 = Tile.computeRowLatitude(firstRow, dLat, latOrigin);
		for (int row = firstRow; row <= lastRow; row++) {
			Angle p2;
			p2 = p1.add(dLat);

			Angle t1 = Tile.computeColumnLongitude(firstCol, dLon, lonOrigin);
			for (int col = firstCol; col <= lastCol; col++) {
				Angle t2;
				t2 = t1.add(dLon);

				this.topLevels.add(new MultiTextureTile(new Sector(p1, p2, t1, t2), level, row, col, this));
				t1 = t2;
			}
			p1 = p2;
		}
	}

	/***
	 * 
	 * @return top level tiles (low resolution)
	 */
	public List<MultiTextureTile> getTopLevels() {
		if (this.topLevels == null) {
			this.createTopLevelTiles();
		}

		return topLevels;
	}

	protected void loadAllTopLevelTextures(DrawContext dc, int indexDate) {
		for (MultiTextureTile tile : this.getTopLevels()) {
			if (!tile.isTextureInMemory(dc.getTextureCache(), subLayers, indexDate)) {
				this.forceTextureLoad(tile, indexDate);
			}
		}

		this.levelZeroLoaded = true;
	}

	// ============== Tile Assembly ======================= //
	// ============== Tile Assembly ======================= //
	// ============== Tile Assembly ======================= //

	/***
	 * Load tiles to be rendered
	 * 
	 * @param dc
	 * @see addTileOrDescendants
	 */
	protected void assembleTiles(DrawContext dc, int indexDate) {
		this.currentTiles.clear();

		for (MultiTextureTile tile : this.getTopLevels()) {
			if (this.isTileVisible(dc, tile)) {
				this.currentResourceTile = null;
				this.addTileOrDescendants(dc, tile, indexDate);
			}
		}
	}

	@Override
	protected void addTileToCache(MultiTextureTile tile, int indexDate) {
		for (SubLayer subLayer : getNeededSubLayers()) {
			MultiTextureTile.getMemoryCache().add(tile.getTileKey(subLayer, indexDate), tile);
		}
	}

	/**
	 * Reads and returns the texture data at the specified URL, optionally converting it to the specified format and
	 * generating mip-maps. If <code>textureFormat</code> is a recognized mime type, this returns the texture data in
	 * the specified format. Otherwise, this returns the texture data in its native format. If <code>useMipMaps</code>
	 * is true, this generates mip maps for any non-DDS texture data, and uses any mip-maps contained in DDS texture
	 * data.
	 * <p/>
	 * Supported texture formats are as follows:
	 * <ul>
	 * <li><code>image/dds</code> - Returns DDS texture data, converting the data to DDS if necessary. If the data is
	 * already in DDS format it's returned as-is.</li>
	 * </ul>
	 * 
	 * @param url the URL referencing the texture data to read.
	 * @param textureFormat the texture data format to return.
	 * @param useMipMaps true to generate mip-maps for the texture data or use mip maps already in the texture data, and
	 *            false to read the texture data without generating or using mip-maps.
	 * 
	 * @return TextureData the texture data from the specified URL, in the specified format and with mip-maps.
	 */
	@Override
	protected HashMap<SubLayer, TextureData> readTexture(java.net.URL url, String textureFormat, boolean useMipMaps,
			int indexDate) {
		HashMap<SubLayer, TextureData> textureData = new HashMap<SubLayer, TextureData>();
		try {
			for (SubLayer subLayer : getNeededSubLayers()) {
				// URL must be changed to match SubLayer and date
				String urlToMatch = url.toString();
				String accurate = getCache(urlToMatch, subLayer, indexDate);
				try {
					GLProfile glprofile = GLProfile.getDefault();
					TextureData data = TextureIO.newTextureData(glprofile, new URL(accurate), useMipMaps, null);
					textureData.put(subLayer, data);
				} catch (Exception e) {
					// texture could not exist because all transparent tiles are
					// not written
					// create a transparent texture to match missing texture

					textureData.put(subLayer, getMissingTexture());
				}
			}
		} catch (Exception e) {
			String msg = Logging.getMessage("layers.TextureLayer.ExceptionAttemptingToReadTextureFile", url);
			Logging.logger().log(java.util.logging.Level.SEVERE, msg, e);
			return null;
		}
		return textureData;
	}

	public boolean hasElevationModel() {
		return (multiElevationModel != null);
	}

	/***
	 * 
	 * @return the {@link MultiElevationModel} associated with this layer
	 */
	public MultiElevationModel getElevation() {
		return multiElevationModel;
	}

	/***
	 * set the {@link MultiElevationModel} associated with this layer
	 * 
	 * @param elevation
	 */
	public void setElevation(MultiElevationModel elevation) {
		multiElevationModel = elevation;
	}

	@Override
	public MultiTextureLayerParameters getLayerParameters() {
		return parameters;
	}

	@Override
	public void setLayerParameters(LayerParameters arg) {
		if (arg instanceof MultiTextureLayerParameters) {
			this.parameters.load((MultiTextureLayerParameters) arg);
			// now check sublayer to set the new parameters
			for (SubLayer sublayer : this.getSubLayers()) {
				String type = sublayer.getType();
				SubLayerParameters subparam = parameters.findSubLayerParameters(type);
				if (subparam == null) {
					parameters.setSubLayerParameters(sublayer.getType(), sublayer.getParameters());
				} else {
					sublayer.setParameters(subparam);
				}
			}
			setCurrentSubLayer(parameters.getCurrentSubLayerIndex());
		}
	}
}
