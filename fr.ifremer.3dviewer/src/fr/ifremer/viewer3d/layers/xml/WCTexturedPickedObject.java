package fr.ifremer.viewer3d.layers.xml;

import java.time.Instant;
import java.util.Optional;

import fr.ifremer.viewer3d.layers.wc.render.IWCPickedObject;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.geom.Position;

/**
 * Texture picked object for XML layers.
 */
@Deprecated
public class WCTexturedPickedObject implements IWCPickedObject {

	private WaterColumnLayer layer;

	public WCTexturedPickedObject(WaterColumnLayer layer) {
		this.layer = layer;
	}

	@Override
	public WaterColumnLayer getLayer() {
		return layer;
	}

	@Override
	public int getId() {
		return getLayer().getPingIdentifier();
	}

	@Override
	public Position getPosition() {
		return null;
	}

	@Override
	public float getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Optional<Instant> getDate() {
		return Optional.of(getLayer().getPingDate().toInstant());
	}

	/**
	 * get the tooltip associated with the picked object
	 */
	@Override
	public Optional<String> getTooltip() {
		WaterColumnLayer layer = getLayer();
		if (layer instanceof AVList) {
			return Optional.of(((AVList) layer).getStringValue(AVKey.ROLLOVER_TEXT));

		}
		return Optional.empty();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((layer == null) ? 0 : layer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WCTexturedPickedObject other = (WCTexturedPickedObject) obj;
		if (layer == null) {
			if (other.layer != null)
				return false;
		} else if (!layer.equals(other.layer))
			return false;
		return true;
	}

}
