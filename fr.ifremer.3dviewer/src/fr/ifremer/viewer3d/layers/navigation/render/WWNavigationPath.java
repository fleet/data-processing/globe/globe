package fr.ifremer.viewer3d.layers.navigation.render;

import java.awt.Point;
import java.nio.FloatBuffer;
import java.time.Instant;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.viewer3d.layers.navigation.WWNavigationLayer;
import fr.ifremer.viewer3d.layers.navigation.WWNavigationPosition;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Path;

public class WWNavigationPath extends Path {

	private final WWNavigationLayer layer;
	private boolean isSelected;

	/**
	 * This class overrides resolvePick to propagates a Global cursor while picking.
	 */
	class WWNavigationPathPickSupport extends PathPickSupport {

		/** Services **/
		private IGeographicViewService geographicViewService = IGeographicViewService.grab();

		@Override
		public PickedObject resolvePick(DrawContext dc, Point pickPoint, Layer layer) {
			var po = super.resolvePick(dc, pickPoint, layer);

			if (geographicViewService.isDetailPickEnabled() && po != null
					&& po.getObject() instanceof WWNavigationPath) {
				var userObject = (WWNavigationPath) po.getObject();

				// get position
				double yInGLCoords = dc.getView().getViewport().height - pickPoint.getY() - 1;
				FloatBuffer depthBuffer = Buffers.newDirectFloatBuffer(1);
				dc.getGL().glReadPixels((int) pickPoint.getX(), (int) yInGLCoords, 1, 1, GL2.GL_DEPTH_COMPONENT,
						GL.GL_FLOAT, depthBuffer);
				Vec4 screenCoords = new Vec4(pickPoint.getX(), yInGLCoords, depthBuffer.get(0));
				Vec4 intersection = dc.getView().unProject(screenCoords);
				var geographicPosition = dc.getGlobe().computePositionFromPoint(intersection);
				userObject.setValue(AVKey.POSITION, geographicPosition);

				// get cursor from closest position
				var closestPostion = getClosest(geographicPosition);
				var cursor = new GlobalCursor().withSource(WWNavigationPath.this).withPosition(closestPostion)
						.withClampToGround(getLayer().getDisplayParameters().displayGroundPath())
						.withDescription(WWNavigationPath.this.layer.getName() + closestPostion.getDescription());
				var optTime = closestPostion.getMetadata().stream()
						.filter(metadataValue -> metadataValue.getType().equals(NavigationMetadataType.TIME)).findAny()
						.map(value -> Instant.ofEpochMilli(value.getValueAsNumber().longValue()));
				if (optTime.isPresent())
					cursor = cursor.withTime(optTime.get());
				userObject.setValue(GlobalCursor.AV_KEY, cursor);
			}

			return po;
		}
	}

	/**
	 * Constructor
	 *
	 * @param wwNavigationLayer : parent layer
	 */
	public WWNavigationPath(WWNavigationLayer wwNavigationLayer) {
		layer = wwNavigationLayer;
		pickSupport = new WWNavigationPathPickSupport();
		setValue(AVKey.LAYER, this);
	}

	/**
	 * @return true if selected.
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * Set selected (= enables highlight).
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
		super.setHighlighted(isSelected);
	}

	/**
	 * Highlight enabled only if not selected.
	 */
	@Override
	public void setHighlighted(boolean highlighted) {
		if (!isSelected)
			super.setHighlighted(highlighted);
	}

	public WWNavigationPosition getClosest(Position p2) {
		WWNavigationPosition closestPosition = null;
		var minDist = Double.MAX_VALUE;
		for (var p : getPositions()) {
			var currDist = LatLon.linearDistance(p, p2).degrees;
			if (currDist < minDist) {
				closestPosition = (WWNavigationPosition) p;
				minDist = currDist;
			}
		}
		return closestPosition;
	}

	/**
	 * @return the {@link #layer}
	 */
	public WWNavigationLayer getLayer() {
		return layer;
	}

}
