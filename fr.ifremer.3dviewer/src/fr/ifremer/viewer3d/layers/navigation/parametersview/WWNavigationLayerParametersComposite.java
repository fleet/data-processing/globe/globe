package fr.ifremer.viewer3d.layers.navigation.parametersview;

import java.beans.PropertyChangeListener;
import java.util.Comparator;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.lang.math.Range;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer.DisplayMode;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.ui.widget.AbstractParametersCompositeWithTabs;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.colorscale.parametersview.ColorScaleParametersComposite;
import fr.ifremer.viewer3d.layers.sync.navigation.WWNavigationLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Parameter composite for {@link IWWNavigationLayer}.
 */
public class WWNavigationLayerParametersComposite extends AbstractParametersCompositeWithTabs {

	/** Save the selected tab index **/
	private static int selectedTab;

	/** {@link IWWNavigationLayer} & associated {@link WWNavigationLayerParameters} **/
	private final IWWNavigationLayer layer;
	private final PropertyChangeListener navigationLayerListener = e -> onNavigationLayerPropertyChanged();

	/** Layer synchronizer **/
	private WWNavigationLayerSynchronizer layersSynchronizer;

	/** Inner widgets **/

	private Spinner lineWidthSpinner;
	private Button btnDisplayNavigationPoints;
	private ColorPicker navLineColorPicker;
	private SliderComposite opacitySliderComposite;
	private Button constantColorRadioButton;
	private ComboViewer variableColorComboViewer;
	private ColorContrastWidget colorContrastWidget;

	private Button btnEnableColorScale;
	private ColorScaleParametersComposite colorScaleComposite;

	private Composite colorScaleTab;

	/** Current vertical offset model */
	private EnabledNumberModel verticalOffsetModel = new EnabledNumberModel(false, 0d);

	private StackLayout displayModeLayout;

	private Button lineBtnProjectionOn;
	private Button lineRadioButton;
	private Composite lineDisplayCompo;
	private Button lineEnableDecimationBtn;
	private Spinner lineDecimationSpinner;

	private Button pointBtnProjectionOn;
	private Spinner pointSizeSpinner;
	private Composite pointDisplayCompo;
	private Button pointEnableDecimationBtn;
	private Spinner pointDecimationSpinner;

	private Composite displayModeCompo;

	/**
	 * Constructor
	 */
	public WWNavigationLayerParametersComposite(Composite parent, IWWNavigationLayer layer) {
		super(parent);

		// injection to retrieve the fileLayerStoreModel
		ContextInitializer.inject(this);

		this.layer = layer;

		Label layerNameLabel = new Label(this, SWT.NONE);
		layerNameLabel.setText(layer.getName());

		createSynchronizationGroup();
		createDisplayTab();
		createColorTab();
		createColorScaleTab();
		createPositionTab();

		tabFolder.setSelection(selectedTab);
		
		// initialize variable color selection
		 if (variableColorComboViewer != null)
			 variableColorComboViewer
			 .setSelection(variableColorComboViewer.getStructuredSelection());
		 
		layer.addPropertyChangeListener(navigationLayerListener);
		addDisposeListener(e -> layer.removePropertyChangeListener(navigationLayerListener));

	}

	/**
	 * Saves tab selection.
	 */
	@Override
	protected void onSelectedTab(int selectedTab) {
		WWNavigationLayerParametersComposite.selectedTab = selectedTab;
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" }) // no generic specified to avoid error with WindowBuilder
	public void createSynchronizationGroup() {
		layersSynchronizer = ContextInitializer.getInContext(WWNavigationLayerSynchronizer.class);
		var grpSynchronization = new LayerParametersSynchronizerComposite(this, layersSynchronizer, layer);
		grpSynchronization.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSynchronization.adaptToLayer(layer);
		grpSynchronization.setSynchronizationChangedListener(l -> onSynchronizationChanged());
	}

	private void createDisplayTab() {
		Composite displayTab = createTab("Display");
		displayTab.setLayout(new GridLayout(3, false));
		Label displayModeLabel = new Label(displayTab, SWT.NONE);
		displayModeLabel.setText("Mode : ");

		lineRadioButton = new Button(displayTab, SWT.RADIO);
		lineRadioButton.setText("Line");
		lineRadioButton.setSelection(getModel().displayMode() == DisplayMode.LINE);
		lineRadioButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> swithDisplayMode()));

		Button pointRadioButton = new Button(displayTab, SWT.RADIO);
		pointRadioButton.setText("Point");
		pointRadioButton.setSelection(getModel().displayMode() == DisplayMode.POINT);
		pointRadioButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> swithDisplayMode()));

		displayModeCompo = new Composite(displayTab, SWT.NONE);
		displayModeLayout = new StackLayout();
		displayModeCompo.setLayout(displayModeLayout);

		displayModeCompo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		createModeLineComposite(displayModeCompo);
		createModePointComposite(displayModeCompo);
		displayModeLayout.topControl = getModel().displayMode() == DisplayMode.LINE ? lineDisplayCompo
				: pointDisplayCompo;
	}

	/** Switch mode Line <-> Point */
	private void swithDisplayMode() {
		if (lineRadioButton.getSelection()) {
			displayModeLayout.topControl = lineDisplayCompo;
			lineWidthSpinner.setSelection(getModel().lineWidth());
			lineBtnProjectionOn.setSelection(getModel().displayGroundPath());
			lineEnableDecimationBtn.setSelection(getModel().decimationEnabled());
			lineDecimationSpinner.setSelection(getModel().decimationPercentage());

		} else {
			displayModeLayout.topControl = pointDisplayCompo;
			pointSizeSpinner.setSelection(getModel().lineWidth());
			pointBtnProjectionOn.setSelection(getModel().displayGroundPath());
			pointEnableDecimationBtn.setSelection(getModel().decimationEnabled());
			pointDecimationSpinner.setSelection(getModel().decimationPercentage());
		}

		displayModeCompo.requestLayout();
		updateLayer();
	}

	/** Make the Composite for parameters of Point mode */
	private void createModePointComposite(Composite parent) {
		pointDisplayCompo = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(pointDisplayCompo);

		Group grpNavigationPoints = new Group(pointDisplayCompo, SWT.NONE);
		grpNavigationPoints.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpNavigationPoints.setText("Navigation points");
		grpNavigationPoints.setLayout(new GridLayout(2, false));

		// line width
		Label pointSizeLabel = new Label(grpNavigationPoints, SWT.NONE);
		pointSizeLabel.setText("Point size : ");
		pointSizeSpinner = new Spinner(grpNavigationPoints, SWT.BORDER);
		pointSizeSpinner.setMaximum(10);
		pointSizeSpinner.setMinimum(1);
		pointSizeSpinner.setSelection(getModel().lineWidth());
		pointSizeSpinner.addListener(SWT.Selection, e -> updateLayer());

		// projection on ground
		pointBtnProjectionOn = new Button(grpNavigationPoints, SWT.CHECK);
		pointBtnProjectionOn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		pointBtnProjectionOn.setText("Project on the ground");
		pointBtnProjectionOn.setSelection(getModel().displayGroundPath());
		pointBtnProjectionOn.addListener(SWT.Selection, e -> updateLayer());

		Group grpDecimation = new Group(pointDisplayCompo, SWT.NONE);
		grpDecimation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpDecimation.setText("Decimation");
		grpDecimation.setLayout(new GridLayout(2, false));

		pointEnableDecimationBtn = new Button(grpDecimation, SWT.CHECK);
		pointEnableDecimationBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		pointEnableDecimationBtn.setText("Apply decimation");
		pointEnableDecimationBtn.setSelection(getModel().decimationEnabled());
		pointEnableDecimationBtn.addListener(SWT.Selection, e -> updateLayer());

		Label lblNewLabel = new Label(grpDecimation, SWT.NONE);
		lblNewLabel.setText("Decimation percentage : ");

		pointDecimationSpinner = new Spinner(grpDecimation, SWT.BORDER);
		pointDecimationSpinner.setMaximum(100);
		pointDecimationSpinner.setMinimum(1);
		pointDecimationSpinner.setSelection(getModel().decimationPercentage());
		pointDecimationSpinner.addListener(SWT.Selection, e -> updateLayer());
	}

	/** Make the Composite for parameters of Line mode */
	private void createModeLineComposite(Composite parent) {
		lineDisplayCompo = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().margins(0, 0).applyTo(lineDisplayCompo);

		Group grpNavLine = new Group(lineDisplayCompo, SWT.NONE);
		grpNavLine.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpNavLine.setText("Navigation line");
		grpNavLine.setLayout(new GridLayout(2, false));

		// line width
		Label lineWidthLabel = new Label(grpNavLine, SWT.NONE);
		lineWidthLabel.setText("Line width : ");
		lineWidthSpinner = new Spinner(grpNavLine, SWT.BORDER);
		lineWidthSpinner.setMaximum(10);
		lineWidthSpinner.setMinimum(1);
		lineWidthSpinner.setSelection(getModel().lineWidth());
		lineWidthSpinner.addListener(SWT.Selection, e -> updateLayer());

		// projection on ground
		lineBtnProjectionOn = new Button(grpNavLine, SWT.CHECK);
		lineBtnProjectionOn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		lineBtnProjectionOn.setText("Project on the ground");
		lineBtnProjectionOn.setSelection(getModel().displayGroundPath());
		lineBtnProjectionOn.addListener(SWT.Selection, e -> updateLayer());

		Group grpNavigationPoints = new Group(lineDisplayCompo, SWT.NONE);
		grpNavigationPoints.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpNavigationPoints.setText("Navigation points");
		grpNavigationPoints.setLayout(new GridLayout(2, false));

		lineEnableDecimationBtn = new Button(grpNavigationPoints, SWT.CHECK);
		lineEnableDecimationBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		lineEnableDecimationBtn.setText("Apply decimation");
		lineEnableDecimationBtn.setSelection(getModel().decimationEnabled());
		lineEnableDecimationBtn.addListener(SWT.Selection, e -> updateLayer());

		Label lblNewLabel = new Label(grpNavigationPoints, SWT.NONE);
		lblNewLabel.setText("Decimation percentage : ");

		lineDecimationSpinner = new Spinner(grpNavigationPoints, SWT.BORDER);
		lineDecimationSpinner.setMaximum(100);
		lineDecimationSpinner.setMinimum(1);
		lineDecimationSpinner.setSelection(getModel().decimationPercentage());
		lineDecimationSpinner.addListener(SWT.Selection, e -> updateLayer());

		// display points
		btnDisplayNavigationPoints = new Button(grpNavigationPoints, SWT.CHECK);
		btnDisplayNavigationPoints.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		btnDisplayNavigationPoints.setText("Display navigation points");
		btnDisplayNavigationPoints.setSelection(getModel().displayPoints());
		btnDisplayNavigationPoints.addListener(SWT.Selection, e -> updateLayer());
	}

	private void createColorTab() {
		var colorTab = createTab("Color");
		colorTab.setLayout(new GridLayout(4, false));

		// color
		constantColorRadioButton = new Button(colorTab, SWT.RADIO);
		constantColorRadioButton.setText("Constant color :");

		// navigation line color
		navLineColorPicker = new ColorPicker(colorTab, SWT.BORDER, getModel().absolutePathColor(),
				color -> updateLayer());
		GridData gd_navLineColorPicker = new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1);
		gd_navLineColorPicker.heightHint = 18;
		gd_navLineColorPicker.widthHint = 50;
		navLineColorPicker.setLayoutData(gd_navLineColorPicker);

		// variable color
		var metadataTypes = layer.getMetadataTypes();

		if (!metadataTypes.isEmpty()) {

			Button variableColorRadioButton = new Button(colorTab, SWT.RADIO);
			variableColorRadioButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			variableColorRadioButton.setText("Variable color :");
			variableColorRadioButton.addListener(SWT.Selection, e -> updateLayer());

			// combobox to select the variable to use
			variableColorComboViewer = new ComboViewer(colorTab, SWT.READ_ONLY);
			Combo combo = variableColorComboViewer.getCombo();
			combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
			variableColorComboViewer.setContentProvider(ArrayContentProvider.getInstance());
			var input = new TreeSet<NavigationMetadataType<?>>(Comparator.comparing(mt -> mt.name.toLowerCase()));
			input.addAll(metadataTypes);
			variableColorComboViewer.setInput(input);

			Consumer<NavigationMetadataType<?>> variableColorSelectionChanged = selectedMetadataType -> {
				var layerColorContrastModel = layer.getColorContrastModel(selectedMetadataType);
				colorContrastWidget.setModel(layerColorContrastModel);
				colorContrastWidget.setResetModel(layerColorContrastModel);
				updateLayer();
				colorContrastWidget.setMinMaxSyncValues(layersSynchronizer.getMinMaxSyncValues(layer));
				
			};
			variableColorComboViewer
					.setSelection(new StructuredSelection(getModel().metadataUsedToGetColor().orElse(input.first())));
			variableColorComboViewer.addSelectionChangedListener(e -> {
				var selectedMetadataType = (NavigationMetadataType<?>) e.getStructuredSelection().getFirstElement();
				variableColorSelectionChanged.accept(selectedMetadataType);
			});
			variableColorComboViewer.setLabelProvider(
					LabelProvider.createTextProvider(obj -> ((NavigationMetadataType<?>) obj).getNameWithUnit()));

			Composite variableColorComposite = new Composite(colorTab, SWT.NONE);
			variableColorComposite.setLayout(new GridLayout(2, false));
			variableColorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));

			// color & contrast widget used if "variable" color is selected
			colorContrastWidget = new ColorContrastWidget(variableColorComposite, getModel().colorContrastModel(), false);
			colorContrastWidget.setMinMaxSyncValues(layersSynchronizer.getMinMaxSyncValues(layer));
			colorContrastWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			colorContrastWidget.subscribe(m -> updateLayer());

			Consumer<Boolean> variableColorSetEnabled = enabled -> {
				variableColorComposite.setEnabled(enabled);
				colorContrastWidget.setEnabled(enabled);
				variableColorComboViewer.getCombo().setEnabled(enabled);
				Stream.of(variableColorComposite.getChildren()).forEach(control -> control.setEnabled(enabled));
			};

			constantColorRadioButton.addListener(SWT.Selection, e -> {
				boolean isConstantColor = constantColorRadioButton.getSelection();
				navLineColorPicker.setEnabled(isConstantColor);
				variableColorSetEnabled.accept(!isConstantColor);
				updateLayer();
			});
			variableColorRadioButton.setSelection(getModel().isVariableColor());
			variableColorSetEnabled.accept(getModel().isVariableColor());
		}

		Group grpOpacity = new Group(colorTab, SWT.NONE);
		grpOpacity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		grpOpacity.setText("Opacity");
		grpOpacity.setLayout(new GridLayout(1, false));
		opacitySliderComposite = new SliderComposite(grpOpacity, SWT.NONE, false, value -> updateLayer());
		opacitySliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		opacitySliderComposite.setValue(getModel().opacity());

		// initialize enable state
		constantColorRadioButton.setSelection(!getModel().isVariableColor());
		navLineColorPicker.setEnabled(!getModel().isVariableColor());

	}

	protected void createColorScaleTab() {
		colorScaleTab = createTab("Color scale");
		colorScaleTab.setLayout(new GridLayout(1, false));

		btnEnableColorScale = new Button(colorScaleTab, SWT.CHECK);
		btnEnableColorScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnEnableColorScale.setText("Display color scale (available if 'Variable color' is selected)");
		btnEnableColorScale.addListener(SWT.Selection, e -> {
			layer.getColorScaleLayer().ifPresent(layer -> layer.setEnabled(btnEnableColorScale.getSelection()));
			if (colorScaleComposite != null && !colorScaleComposite.isDisposed())
				colorScaleComposite.setEnabled(btnEnableColorScale.getSelection());
		});

		updateColorScaleTab();
	}

	private void updateColorScaleTab() {
		var optColorScaleLayer = layer.getColorScaleLayer();
		if (optColorScaleLayer.isPresent() && (colorScaleComposite == null || colorScaleComposite.isDisposed())) {
			var colorScaleLayer = optColorScaleLayer.get();
			btnEnableColorScale.setEnabled(true);
			btnEnableColorScale.setSelection(colorScaleLayer.isEnabled());
			colorScaleComposite = new ColorScaleParametersComposite(colorScaleTab, colorScaleLayer);
			GridLayout gridLayout = new GridLayout(1, false);
			gridLayout.marginHeight = 0;
			gridLayout.marginWidth = 0;
			colorScaleComposite.setLayout(gridLayout);
			colorScaleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			colorScaleTab.layout();
		} else if (optColorScaleLayer.isEmpty() && colorScaleComposite != null && !colorScaleComposite.isDisposed()) {
			btnEnableColorScale.setEnabled(false);
			colorScaleComposite.dispose();
			colorScaleTab.layout();
		}
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 */
	private void createPositionTab() {
		WWNavigationLayerParameters param = layer.getDisplayParameters();
		verticalOffsetModel = new EnabledNumberModel(param.verticalOffsetEnabled(), param.verticalOffset());

		super.createPositionTab(verticalOffsetModel, newVerticalOffsetModel -> {
			verticalOffsetModel = newVerticalOffsetModel;
			updateLayer();
		});
	}

	/**
	 * Builds new {@link WWNavigationLayerParameters} from user selection and updates layer(s).
	 */
	private void updateLayer() {
		var isLine = lineRadioButton.getSelection();

		// build new WWNavigationLayerParameters
		var displayParameters = new WWNavigationLayerParameters(isLine ? DisplayMode.LINE : DisplayMode.POINT,
				isLine ? lineBtnProjectionOn.getSelection() : pointBtnProjectionOn.getSelection(),
				isLine ? lineWidthSpinner.getSelection() : pointSizeSpinner.getSelection(),
				btnDisplayNavigationPoints.getSelection(), opacitySliderComposite.getValue(),
				// color
				!constantColorRadioButton.getSelection(), navLineColorPicker.getColor(),
				Optional.ofNullable(variableColorComboViewer)
						.map(comboViewer -> ((NavigationMetadataType<?>) comboViewer.getStructuredSelection()
								.getFirstElement())),
				colorContrastWidget != null ? colorContrastWidget.getModel() : new ColorContrastModelBuilder().build(),
				// decimation
				isLine ? lineEnableDecimationBtn.getSelection() : pointEnableDecimationBtn.getSelection(),
				isLine ? lineDecimationSpinner.getSelection() : pointDecimationSpinner.getSelection(),
				// vertical offset
				verticalOffsetModel.enable, verticalOffsetModel.value.floatValue());

		layer.setDisplayParameters(displayParameters);
		layersSynchronizer.synchonizeWith(layer);

		updateColorScaleTab();
	}

	/** The synchronization option has changed. Update the minMaxSyncValues in the ColorComposite */
	private void onSynchronizationChanged() {
		if (colorContrastWidget != null) {
			Optional<Range> minMaxSyncValues = layersSynchronizer.getMinMaxSyncValues(layer);
			colorContrastWidget.setMinMaxSyncValues(minMaxSyncValues);
		}
	}

	private WWNavigationLayerParameters getModel() {
		return layer.getDisplayParameters();
	}
	

	/** A property changed of layer */
	private void onNavigationLayerPropertyChanged() {
		UIUtils.asyncExecSafety(variableColorComboViewer, () -> {
			 variableColorComboViewer
				 .setSelection(variableColorComboViewer.getStructuredSelection());
		});
	}

}
