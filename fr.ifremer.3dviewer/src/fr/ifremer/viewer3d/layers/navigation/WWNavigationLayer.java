package fr.ifremer.viewer3d.layers.navigation;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.StreamSupport;

import org.apache.commons.lang.math.FloatRange;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.ModelPreferences;
import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata.NavigationMetadataValue;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.utils.NavigationDecimator;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.FloatIndexedSupplier;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.ILayerSelected;
import fr.ifremer.viewer3d.layers.navigation.render.WWNavigationPath;
import fr.ifremer.viewer3d.layers.point.render.PointRenderer;
import fr.ifremer.viewer3d.layers.point.render.PointRendererParameters;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Path;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.ShapeAttributes;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import jakarta.inject.Inject;

/**
 * Layer for navigation data.
 */
public class WWNavigationLayer extends RenderableLayer
		implements IWWNavigationLayer, ILayerSelected, Path.PositionColors {

	private static final int MIN_COUNT_AFTER_DECIMATION = 200;

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(WWNavigationLayer.class);

	/** Color used for NaN values **/
	private static final Color NAN_COLOR = new Color(224, 224, 224, 128);

	@Inject
	private ISessionService sessionService;
	@Inject
	private IProcessService processService;
	@Inject
	private IGeographicViewService geographicViewService;
	@Inject
	private IWWLayerFactory wwLayerFactory;
	@Inject
	private WWFileLayerStoreModel layerStoreModel;
	@Inject
	private IGlobalCursorService globalCursorService;

	/** Supplier of {@link INavigationData} **/
	private final INavigationDataSupplier navigationDataSupplier;

	/** Navigation model extracted from INavigationData */
	private List<WWNavigationPosition> navigationModel = List.of();
	/** Navigation metdata types extracted from INavigationData */
	private List<NavigationMetadataType<?>> navigationMetadataTypes = List.of();
	/** First Instant of the loaded model */
	private Instant modelStartingDate = Instant.MAX;
	/** First Instant of the loaded model */
	private Instant modelEndDate = Instant.MIN;

	/** True when navigationModel have been modified and renderers must be updated */
	private boolean navigationModelUpdated = false;
	/** True when displayParameters have been modified and renderables must be updated */
	private boolean displayParametersUpdated = false;
	/** Last applied vertical exaggeration */
	private double verticalExaggeration = 1d;

	/** Renderable {@link Path} **/
	private final WWNavigationPath renderablePath = new WWNavigationPath(this);
	/** Renderable of points **/
	@Inject
	private PointRenderer renderablePoint;
	/** Renderer of temporal point */
	private PointPlacemark temporalMarker;
	/** Listener of GlobalCursor hooked to IGlobalCursorService */
	private final Consumer<GlobalCursor> globalCursorListener = this::onGlobalCursor;

	/** Display parameters **/
	private WWNavigationLayerParameters displayParameters;

	private Map<NavigationMetadataType<?>, ColorContrastModel> colorContrastModelByMetadata = Map.of();

	/** Preference parameters **/
	private final BooleanPreferenceAttribute displayProjectionPrefAttribute = Activator.getPluginParameters()
			.getDisplayNavigationLineProjection();
	private IntegerPreferenceAttribute decimationPercentPrefAttribute = ModelPreferences.getInstance()
			.getNavigationDecimation();

	/** Related color scale layer **/
	private Optional<IWWColorScaleLayer> optColorScaleLayer = Optional.empty();

	private String filename;

	/** Use of reactive to apply reload with back pressure strategy **/
	private final PublishSubject<Object> updatePublisher = PublishSubject.create();
	private final Disposable updateDisposable = updatePublisher.sample(1, TimeUnit.SECONDS)
			.toFlowable(BackpressureStrategy.LATEST).subscribe(obj -> scheduleUpdate());

	private Queue<NavigationMetadataType<?>> metadataUpdateQueue = new ConcurrentLinkedQueue<>();
	boolean reloadNeeded = false;
	// set to false after initialization
	boolean isUpdating = true;

	/**
	 * Constructor
	 */
	public WWNavigationLayer(INavigationDataSupplier navigationDataSupplier, WWNavigationLayerParameters parameters) {
		ContextInitializer.inject(this);
		this.navigationDataSupplier = navigationDataSupplier;
		this.displayParameters = parameters.withDisplayGroundPath(displayProjectionPrefAttribute.getValue());

		// set name
		try (var navigationData = navigationDataSupplier.getNavigationData(true)) {
			filename = new File(navigationData.getFileName()).getName();
			setName("Navigation of " + filename);

			// initialize decimation
			if (navigationData.size() > 0) {
				var decimationPercent = Math.max(decimationPercentPrefAttribute.getValue(),
						(MIN_COUNT_AFTER_DECIMATION * 100) / navigationData.size());
				displayParameters = displayParameters.withDecimation(displayParameters.decimationEnabled(),
						decimationPercent);
			}
			loadNavigationModel(navigationData);
		} catch (GIOException | IOException e) {
			LOGGER.error("Exception while loading navigation layer {} : {}", getName(), e.getMessage(), e);
		}

		setPickEnabled(true);
		renderablePath.setValue(AVKey.ROLLOVER_TEXT, getName());
		addRenderable(renderablePath);
		addRenderable(renderablePoint);

		globalCursorService.addListener(globalCursorListener);

		renderablePoint.setGlobalCursorSupplier(this::buildGlobalCursorForPoint);
		displayParametersUpdated = true;
		isUpdating = false;
	}

	@Override
	public void dispose() {
		globalCursorService.removeListener(globalCursorListener);
		super.dispose();
		updateDisposable.dispose();
	}

	@Override
	public void reload() {
		reloadNeeded = true;
		updatePublisher.onNext(this);
	}

	public void update(NavigationMetadataType<?> metadatatype) {
		metadataUpdateQueue.add(metadatatype);
		updatePublisher.onNext(this);
	}

	public void scheduleUpdate() {
		if (isUpdating)
			return;
		isUpdating = true;
		processService.runInBackground("Navigation layer update " + (displayParameters.decimationEnabled()
				? (" with decimation : " + displayParameters.decimationPercentage() + "%")
				: ""), (monitor, logger) -> {
					while (reloadNeeded || !metadataUpdateQueue.isEmpty()) {
						try (var navigationData = navigationDataSupplier.getNavigationData(true)) {
							if (reloadNeeded) {
								reloadNeeded = false;
								loadNavigationModel(navigationData);
							}
							while (!metadataUpdateQueue.isEmpty()) {
								var metadatatype = metadataUpdateQueue.poll();
								if (!colorContrastModelByMetadata.containsKey(metadatatype)) {
									colorContrastModelByMetadata.put(metadatatype,
											new ColorContrastModelBuilder().build());
									updateNavigationModel(navigationData, metadatatype);
								}
							}
						} catch (GIOException | IOException e) {
							LOGGER.error("Exception while loading navigation layer {} : {}", getName(), e.getMessage(),
									e);
							isUpdating = false;
							return Status.CANCEL_STATUS;
						}
					}
					setDisplayParameters(displayParameters); // enable to keep variable color
					geographicViewService.refresh();
					firePropertyChange(AVKey.LAYER, null, this);
					isUpdating = false;
					return Status.OK_STATUS;
				});
	}

	/**
	 * Computes {@link Position} from {@link INavigationDataSupplier} and set the result to navigationModel attribute.
	 */
	private void loadNavigationModel(INavigationData navigationData) throws GIOException {
		navigationModel = new ArrayList<>();
		navigationMetadataTypes = navigationData.getMetadataTypes();

		var geoboxBuilder = new GeoBoxBuilder();
		double maxElevation = Double.NEGATIVE_INFINITY;

		for (int i : displayParameters.decimationEnabled() ? NavigationDecimator.decimate(
				(int) Math.round(navigationData.size() * (displayParameters.decimationPercentage() / 100d)),
				navigationData) : navigationData) {
			if (!navigationData.isValid(i))
				continue; // keep only valid points
			double lat = navigationData.getLatitude(i);
			double lon = navigationData.getLongitude(i);
			if (!Double.isNaN(lat) && !Double.isNaN(lon)) {
				double elev = gainElevationWithOffset(navigationData, i);
				var metadataValues = new ArrayList<NavigationMetadataValue<?>>();
				maxElevation = Math.max(maxElevation, elev);
				navigationModel.add(new WWNavigationPosition(lat, lon, elev, i, metadataValues));
				geoboxBuilder.addPoint(lon, lat);
			}
		}
		// set sector for this layer (allows "Go to" function)
		setValue(AVKey.SECTOR, GeoBoxUtils.convert(geoboxBuilder.build()));
		if (Double.isFinite(maxElevation))
			setValue(IWWLayer.MAX_ELEVATION_KEY, maxElevation);

		// compute color contrast model
		colorContrastModelByMetadata = new HashMap<>();

		// force load of elevation, time
		if(!navigationMetadataTypes.isEmpty()) {
			List<NavigationMetadataType<?>> preloadedTypes = List.of(navigationMetadataTypes.get(0), NavigationMetadataType.ELEVATION,
					NavigationMetadataType.TIME);
	
			for (var metadata : preloadedTypes) {
				if (navigationMetadataTypes.contains(metadata))
					updateNavigationModel(navigationData, metadata);
			}
		}

		setDisplayParameters(displayParameters); // enable to keep variable color
		geographicViewService.refresh();
	}

	private void updateNavigationModel(INavigationData navigationData, NavigationMetadataType<?> metadatatype)
			throws GIOException {
		var metadata = navigationData.getNavigationMetadata(metadatatype).orElse(null);
		if (metadata == null)
			return;
		FloatRange minMaxRange = null;
		long startingDate = Long.MAX_VALUE;
		long endDate = Long.MIN_VALUE;
		for (var pos : navigationModel) {
			int i = pos.getIndex();
			var metadataValue = metadata.getMetadataValue(i);
			pos.getMetadata().add(metadataValue);

			var floatValue = metadataValue.getValueAsNumber().floatValue();
			if (Float.isNaN(floatValue))
				continue;
			var minValue = minMaxRange != null ? Math.min(minMaxRange.getMinimumFloat(), floatValue) : floatValue;
			var maxValue = minMaxRange != null ? Math.max(minMaxRange.getMaximumFloat(), floatValue) : floatValue;
			minMaxRange = new FloatRange(minValue, maxValue);
			if (metadatatype.equals(NavigationMetadataType.TIME)) {
				long positionDate = metadataValue.getValueAsNumber().longValue();
				startingDate = Math.min(startingDate, positionDate);
				endDate = Math.max(endDate, positionDate);
			}
		}

		// Memories starting and end dates
		if (metadatatype.equals(NavigationMetadataType.TIME)) {
			if (startingDate != Long.MAX_VALUE) {
				modelStartingDate = Instant.ofEpochMilli(startingDate);
				modelEndDate = Instant.ofEpochMilli(endDate);
			} else {
				modelStartingDate = Instant.MAX;
				modelEndDate = Instant.MIN;
			}
		}

		ColorContrastModel colorContrastModel;
		// update color contrast model
		if (minMaxRange != null) {
			float minValue = (float) metadata.getOptMinValue().orElse(minMaxRange.getMinimumFloat());
			float maxValue = (float) metadata.getOptMaxValue().orElse(minMaxRange.getMaximumFloat());
			colorContrastModel = new ColorContrastModelBuilder()//
					.setContrastMin(minValue).setLimitMin(minValue - 0.25f * (maxValue - minValue))//
					.setContrastMax(maxValue).setLimitMax(maxValue + 0.25f * (maxValue - minValue))//
					.build();
		} else {
			colorContrastModel = new ColorContrastModelBuilder().build();
		}
		colorContrastModelByMetadata.put(metadata.getType(), colorContrastModel);
		// initialize first min/max values
		var optMetadataUsedToGetColor = displayParameters.metadataUsedToGetColor();
		if (optMetadataUsedToGetColor.isPresent() && optMetadataUsedToGetColor.get().equals(metadata.getType())) {
			displayParameters = displayParameters.withColorContrastModel(colorContrastModel);
		}
		navigationModelUpdated = true;
	}

	/**
	 * @return the elevation of the navigation at the specified point
	 */
	protected double gainElevation(INavigationData navigationData, int index) throws GIOException {
		float elev = navigationData.getMetadataValue(NavigationMetadataType.ELEVATION, index)
				.filter(value -> !Float.isNaN(value)).orElse(0f);
		// Apply tide
		elev += navigationData.getMetadataValue(NavigationMetadataType.WATERLINE_TO_CHART_DATUM, index)
				.filter(value -> !Float.isNaN(value)).orElse(0f);
		return elev;
	}

	/**
	 * @return the elevation of the navigation at the specified point and add the offset
	 */
	protected double gainElevationWithOffset(INavigationData navigationData, int index) throws GIOException {
		double elev = gainElevation(navigationData, index);
		if (displayParameters.verticalOffsetEnabled()) {
			elev += displayParameters.verticalOffset();
		}
		return elev;
	}

	/** Configure Path or Point rendering, depending on DisplayMode */
	private void configureRenderable() {
		if (displayParameters.displayMode() != DisplayMode.POINT) {
			configureRenderablePath();
		} else {
			configureRenderablePoint();
		}
	}

	/** Configure Path rendering */
	private void configureRenderablePath() {
		ShapeAttributes attrs = new BasicShapeAttributes();
		if (!displayParameters.isVariableColor())
			attrs.setOutlineMaterial(
					new Material(ColorUtils.convertGColorToAWT(displayParameters.absolutePathColor())));
		attrs.setEnableAntialiasing(true);
		attrs.setOutlineOpacity(displayParameters.opacity());
		attrs.setOutlineWidth(displayParameters.lineWidth());
		renderablePath.setAttributes(attrs);

		// renderableAbsolutePath.setVisible(!displayParameters.displayGroundPath);
		renderablePath.setAltitudeMode(
				displayParameters.displayGroundPath() ? WorldWind.CLAMP_TO_GROUND : WorldWind.ABSOLUTE);
		renderablePath.setPathType(AVKey.RHUMB_LINE);
		renderablePath.setShowPositions(displayParameters.displayPoints());
		renderablePath.setShowPositionsThreshold(1e6);
		renderablePath.setDragEnabled(false);
		renderablePath.setNumSubsegments(0);

		BasicShapeAttributes highlightAttrs = new BasicShapeAttributes(attrs);
		highlightAttrs.setOutlineMaterial(Material.WHITE);
		highlightAttrs.setOutlineWidth(displayParameters.lineWidth() * 1.5);
		renderablePath.setHighlightAttributes(highlightAttrs);

		renderablePath.setPositionColors(displayParameters.isVariableColor() ? this : null);
	}

	/** Configure Point rendering */
	private void configureRenderablePoint() {
		renderablePoint.setParameters(new PointRendererParameters(//
				displayParameters.lineWidth(), //
				displayParameters.opacity(), //
				displayParameters.absolutePathColor(), //
				Optional.ofNullable(displayParameters.isVariableColor() ? //
						adaptLimitsToFloat(displayParameters.colorContrastModel())//
						: null), //
				Optional.empty() // Constant point size
		));
	}

	/**
	 * Highlights on selection.
	 */
	@Override
	public void setSelectedLayer(boolean selected) {
		renderablePath.setSelected(selected);
		renderablePoint.setSelected(selected);
	}

	/**
	 * @return the color to apply for a {@link Position}.
	 */
	@Override
	public Color getColor(Position position, int ordinal) {
		if (displayParameters.isVariableColor() && displayParameters.metadataUsedToGetColor().isPresent()) {
			// get metadata value as Number
			var value = ((WWNavigationPosition) position).getMetadata().stream()//
					.filter(metadataValue -> metadataValue.getType()
							.equals(displayParameters.metadataUsedToGetColor().get()))//
					.map(NavigationMetadataValue::getValueAsNumber)//
					.findFirst();

			if (value.isPresent()) {
				double doubleValue = value.get().doubleValue();
				if (Double.isFinite(doubleValue)) {
					var colorContrastModel = displayParameters.colorContrastModel();
					GColor color = ColorMap.getColor(colorContrastModel.colorMapIndex(),
							colorContrastModel.invertColor(), colorContrastModel.contrastMin(),
							colorContrastModel.contrastMax(), value.get().doubleValue());
					return ColorUtils.convertGColorToAWT(color);
				} else
					return NAN_COLOR;
			}
		}
		return ColorUtils.convertGColorToAWT(displayParameters.absolutePathColor());
	}

	// GETTERS & SETTERS

	@Override
	public WWNavigationLayerParameters getDisplayParameters() {
		return displayParameters;
	}

	@Override
	public void setDisplayParameters(WWNavigationLayerParameters displayParameters) {
		// Must apply the vertical offset ?
		double previousVerticalOffset = gainVerticalOffset(this.displayParameters);
		double newVerticalOffset = gainVerticalOffset(displayParameters);
		if (previousVerticalOffset != newVerticalOffset) {
			applyVerticalOffset(newVerticalOffset - previousVerticalOffset);
		}

		// Force sending positions (with metadata) to renderers
		navigationModelUpdated = navigationModelUpdated
				|| this.displayParameters.displayMode() != displayParameters.displayMode()
				|| this.displayParameters.displayGroundPath() != displayParameters.displayGroundPath()
				|| this.displayParameters.isVariableColor() != displayParameters.isVariableColor()
				|| !Objects.equals(this.displayParameters.colorContrastModel(), displayParameters.colorContrastModel());

		// Ensure that incoming NavigationMetadataType is known
		if (displayParameters.isVariableColor() && displayParameters.metadataUsedToGetColor().isPresent()) {
			var optionalNavigationMetadataType = getNavigationMetadataType(
					displayParameters.metadataUsedToGetColor().get().name);
			if (optionalNavigationMetadataType.isPresent()) {
				// Current ColorContrastModel for the specified NavigationMetadataType
				NavigationMetadataType<?> navigationMetadataType = optionalNavigationMetadataType.get();
				ColorContrastModel colorContrastModel = getColorContrastModel(navigationMetadataType);

				// So, in the new displayParameters, retain my well known NavigationMetadataType
				displayParameters = displayParameters.withVariableColor(true, optionalNavigationMetadataType);
				// And retain the min/max limits of the ColorContrastModel
				displayParameters = displayParameters.withColorContrastModel(displayParameters.colorContrastModel()
						.withLimitMinMax(colorContrastModel.limitMin(), colorContrastModel.limitMax())
						.withResetMinMax(colorContrastModel.resetMin(), colorContrastModel.resetMax()));
				
				if (!colorContrastModelByMetadata.containsKey(navigationMetadataType)) {
					update(navigationMetadataType);
				} else if (displayParameters.colorContrastModel().isValid()) {
					// Update the map with the new ColorContrastModel
					colorContrastModelByMetadata.put(navigationMetadataType, displayParameters.colorContrastModel());
				}
			} else {
				// Unknown incoming NavigationMetadataType : ignored
				displayParameters = displayParameters.withVariableColor(false, Optional.empty());
			}
		}

		// unselect the layer : the 'highlight' due to the selection can hide the new parameters
		setSelectedLayer(false);

		fitDisplayParameters(displayParameters);
		sessionService.saveSession();

		// save "display ground path" option in preferences
		Activator.getPluginParameters().save();
	}

	/**
	 * Apply a vertical offset to the elevation of each position
	 */
	public void applyVerticalOffset(double offset) {
		navigationModel = StreamSupport.stream(navigationModel.spliterator(), false)//
				.filter(WWNavigationPosition.class::isInstance)//
				.map(WWNavigationPosition.class::cast)//
				.map(pos -> pos.applyOffset(offset))//
				.toList();
		navigationModelUpdated = true;
	}

	protected void fitDisplayParameters(WWNavigationLayerParameters displayParameters) {
		// If decimation parameters have changed : reload
		boolean reload = false;
		if (this.displayParameters.decimationEnabled() != displayParameters.decimationEnabled()
				|| this.displayParameters.decimationPercentage() != displayParameters.decimationPercentage())
			reload = true;

		this.displayParameters = displayParameters;

		if (reload) {
			// deffered reload
			reload();
		}

		// ask update display (in AWT thread)
		displayParametersUpdated = true;

		// save "display ground path" option in preferences
		displayProjectionPrefAttribute.setValue(displayParameters.displayGroundPath(), false);

		layerStoreModel.get(this).ifPresent(layerStore -> {
			// create color scale layer for variable color
			if (displayParameters.isVariableColor() && optColorScaleLayer.isEmpty()) {
				optColorScaleLayer = Optional.of(wwLayerFactory.createColorScaleLayer(this, filename));
				layerStoreModel.addLayersToStore(layerStore, optColorScaleLayer.get());
			}
			// remove color scale layer if it is a constant color
			else if (!displayParameters.isVariableColor() && optColorScaleLayer.isPresent()) {
				layerStoreModel.removeLayersToStore(layerStore, optColorScaleLayer.get());
				optColorScaleLayer = Optional.empty();
			}
		});
	}

	/**
	 * @return the vertical offset configured in the parameters
	 */
	protected double gainVerticalOffset(WWNavigationLayerParameters parameters) {
		return parameters.verticalOffsetEnabled() ? parameters.verticalOffset() : 0d;
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return displayParameters.toTypedMap();
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(WWNavigationLayerParameters.SESSION_KEY).ifPresent(restoredDisplayParameters -> {
			// Restore the instance of NavigationMetadataType.
			parameters.get(WWNavigationLayerParameters.SESSION_KEY_METADATA).ifPresentOrElse(restoredMetadataName -> {
				// Use the name stored in the session to find the instance of NavigationMetadataType among the
				// configured navigationMetadataTypes
				Optional<NavigationMetadataType<?>> restoredMetadataUsedToGetColor = getNavigationMetadataType(
						restoredMetadataName);
				setDisplayParameters(restoredDisplayParameters.withVariableColor(
						restoredDisplayParameters.isVariableColor(), restoredMetadataUsedToGetColor));
			}, () -> setDisplayParameters(restoredDisplayParameters));
			navigationModelUpdated = true;
		});
	}

	@Override
	public ColorContrastModel getColorContrastModel(NavigationMetadataType<?> metadataType) {
		return getNavigationMetadataType(metadataType.name).map(colorContrastModelByMetadata::get)
				.orElse(new ColorContrastModelBuilder().build());
	}

	@Override
	public Map<NavigationMetadataType<?>, ColorContrastModel> getColorContrastModelByMetadataType() {
		return Collections.unmodifiableMap(colorContrastModelByMetadata);
	}

	@Override
	public List<NavigationMetadataType<?>> getMetadataTypes() {
		return navigationMetadataTypes;
	}

	@Override
	public Optional<IWWColorScaleLayer> getColorScaleLayer() {
		return optColorScaleLayer;
	}

	/**
	 * Search a NavigationMetadataType with the specified name
	 */
	protected Optional<NavigationMetadataType<?>> getNavigationMetadataType(String name) {
		return navigationMetadataTypes.stream().filter(type -> type.name.equalsIgnoreCase(name)).findFirst();
	}

	/** {@inheritDoc} */
	@Override
	protected void doPreRender(DrawContext dc) {
		boolean updateDisplay = displayParametersUpdated || navigationModelUpdated;
		boolean updateModel = navigationModelUpdated;
		navigationModelUpdated = false;
		displayParametersUpdated = false;

		if (displayParameters.displayMode() != DisplayMode.POINT) {
			renderablePath.setVisible(true);
			if (updateModel) {
				renderablePath.setPositions(navigationModel);
			}
			renderablePoint.setVisible(false);
			renderablePoint.clear();
		} else {
			renderablePath.setVisible(false);
			renderablePath.setPositions(List.of());
			renderablePoint.setVisible(true);
			if (verticalExaggeration != dc.getVerticalExaggeration() || updateDisplay) {
				FloatIndexedSupplier valueSupplier = i -> Float.NaN;
				if (displayParameters.isVariableColor() && displayParameters.metadataUsedToGetColor().isPresent()) {
					valueSupplier = this::getMetadaValueAsFloat;
				}
				verticalExaggeration = dc.getVerticalExaggeration();
				renderablePoint.acceptPoints(navigationModel.size(), navigationModel::get, valueSupplier,
						displayParameters.displayGroundPath(), verticalExaggeration);
			}
		}
		if (updateDisplay) {
			// update display
			configureRenderable();
		}
	}

	/**
	 * @return the float value for a NavigationMetadaValue specified by the index
	 */
	private float getMetadaValueAsFloat(int positionIndex) {
		NavigationMetadataValue<?> metadaValue = navigationModel.get(positionIndex).getMetadata().stream()//
				.filter(metadataValue -> metadataValue.getType()
						.equals(displayParameters.metadataUsedToGetColor().get()))//
				.findFirst()//
				.orElse(null);
		if (metadaValue != null) {
			// Specific case for Long (As Time) : return index as value (cannot make a float with a long)
			if (metadaValue.getType().equals(NavigationMetadataType.TIME))
				return positionIndex;
			return metadaValue.getValueAsNumber().floatValue();
		}
		return Float.NaN;
	}

	/**
	 * For Time metadata, index of position is used as value. So set the limits to the size of position list
	 */
	private ColorContrastModel adaptLimitsToFloat(ColorContrastModel colorContrastModel) {
		if (displayParameters.metadataUsedToGetColor().filter(NavigationMetadataType.TIME::equals).isPresent()) {
			return new ColorContrastModel(colorContrastModel.colorMapIndex(), colorContrastModel.invertColor(), 0,
					navigationModel.size(), 0, navigationModel.size(), 0, navigationModel.size(),
					colorContrastModel.interpolated(), colorContrastModel.opacity());
		}
		return colorContrastModel;
	}

	/**
	 * @return a GlobalCursor for the specified point index. Invoked when a picking is processed in the PointRenderer
	 */
	private GlobalCursor buildGlobalCursorForPoint(int pointIndex) {
		var result = new GlobalCursor().withSource(renderablePath);
		if (pointIndex < navigationModel.size()) {
			WWNavigationPosition navPoint = navigationModel.get(pointIndex);
			result = result.withPosition(navPoint).withDescription(getName() + navPoint.getDescription());
			var optTime = gainTimeInMetadata(navPoint);
			if (optTime != null)
				result = result.withTime(optTime);
		}
		return result;
	}

	/**
	 * Reacts to the reception of a GlobalCursor.<br>
	 * If a time is present, search the closest point to this time and highlight it with a marker
	 */
	private void onGlobalCursor(GlobalCursor globalcursor) {
		if (globalcursor.source().filter(renderablePath::equals).isEmpty()// Cursor does not come from this layer ?
				&& !navigationModel.isEmpty()) {
			// modelStartingDate <= time <= modelEndDate ?
			Instant time = globalcursor.time().orElse(null);
			if (time != null && DateUtils.between(time, modelStartingDate, modelEndDate)) {
				WWNavigationPosition navigationPosition = binarySearch(time);
				if (navigationPosition != null) {
					addTemporalMarker(navigationPosition);
					return;
				}
			}
		}

		// Clean previous marker
		if (temporalMarker != null) {
			removeRenderable(temporalMarker);
			temporalMarker = null;
		}
	}

	/** A a marker to the specified position */
	private void addTemporalMarker(WWNavigationPosition navigationPosition) {
		if (temporalMarker == null) {
			temporalMarker = new PointPlacemark(navigationPosition);
			var attrs = new PointPlacemarkAttributes();
			attrs.setLineColor(new GColor("#F6A564A5").toAABBGGRR());
			attrs.setUsePointAsDefaultImage(true);
			attrs.setScale(15d);
			temporalMarker.setAttributes(attrs);
			addRenderable(temporalMarker);
		} else
			temporalMarker.setPosition(navigationPosition);

		temporalMarker.setAltitudeMode(
				displayParameters.displayGroundPath() ? WorldWind.CLAMP_TO_GROUND : WorldWind.ABSOLUTE);
		geographicViewService.refresh();
	}

	/**
	 * Searches the navigation model for the specified time using the binary search algorithm (dicotomic search). <br>
	 * This algorithm is inspired by Collections.binarySearch
	 */
	private WWNavigationPosition binarySearch(Instant expectedTime) {
		int positionIndex = DateUtils.binarySearch(expectedTime, navigationModel.size(),
				i -> gainTimeInMetadata(navigationModel.get(i)));
		return positionIndex >= 0 && positionIndex < navigationModel.size() ? navigationModel.get(positionIndex) : null;
	}

	/** Return the Instant contained in the metatadata of the specified point if any */
	private Instant gainTimeInMetadata(WWNavigationPosition navPoint) {
		return navPoint.getMetadata().stream()
				.filter(metadataValue -> metadataValue.getType().equals(NavigationMetadataType.TIME))//
				.findAny()//
				.map(value -> Instant.ofEpochMilli(value.getValueAsNumber().longValue()))//
				.orElse(null);
	}

}
