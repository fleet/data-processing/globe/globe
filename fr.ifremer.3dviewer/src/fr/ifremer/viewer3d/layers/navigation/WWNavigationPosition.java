package fr.ifremer.viewer3d.layers.navigation;

import java.util.List;

import fr.ifremer.globe.core.model.navigation.NavigationMetadata.NavigationMetadataValue;
import fr.ifremer.globe.ui.utils.PositionUtils;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;

/**
 * This class extends the {@link Position} class to add metadata.
 */
public class WWNavigationPosition extends Position {

	private final int index;
	private final List<NavigationMetadataValue<?>> metadataValues;

	/** Constructor **/
	public WWNavigationPosition(double latitude, double longitude, double elevation, int index,
			List<NavigationMetadataValue<?>> metadataValues) {
		super(Angle.fromDegrees(latitude), Angle.fromDegrees(longitude), elevation);
		this.index = index;
		this.metadataValues = metadataValues;
	}

	/**
	 * Constructor
	 */
	public WWNavigationPosition(Angle latitude, Angle longitude, double elevation, int index,
			List<NavigationMetadataValue<?>> metadataValues) {
		super(latitude, longitude, elevation);
		this.index = index;
		this.metadataValues = metadataValues;
	}

	/** Build the same instance with an offsed applied to the elevation */
	public WWNavigationPosition applyOffset(double offset) {
		return new WWNavigationPosition(latitude, longitude, elevation + offset, index, metadataValues);
	}

	public int getIndex() {
		return index;
	}

	public List<NavigationMetadataValue<?>> getMetadata() {
		return metadataValues;
	}

	public String getDescription() {
		var desc = new StringBuilder("[" + index + "] " + "\n");
		desc.append("Position = " + PositionUtils.positionToString(this) + "\n");
		metadataValues.forEach(value -> desc.append(value.getType().name + " = " + value.getValueAsString() + "\n"));
		return desc.toString();
	}

}
