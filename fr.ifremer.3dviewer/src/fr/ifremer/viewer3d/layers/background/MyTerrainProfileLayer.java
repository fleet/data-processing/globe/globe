package fr.ifremer.viewer3d.layers.background;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.TerrainProfileLayer;
import gov.nasa.worldwind.render.DrawContext;

public class MyTerrainProfileLayer extends TerrainProfileLayer {

	private boolean active = true;

	public MyTerrainProfileLayer() {
		// AnnotationProcessor.process(this);
		setZeroBased(false);
		setName("Terrain Profile");
		setPosition(AVKey.SOUTHEAST);
		setStartLatLon(LatLon.fromDegrees(0, -10));
		setEndLatLon(LatLon.fromDegrees(0, 65));
	}

	/**
	 * Overridden to fire {@link PropertyChangeEvent}
	 */
	@Override
	public void setEnabled(boolean enabled) {
		Boolean oldEnabled = isEnabled();
		super.setEnabled(enabled);
		super.firePropertyChange(new PropertyChangeEvent(this, "Enabled", oldEnabled, enabled));
	}

	public Position[] getPositions() {
		return this.positions;
	}

	public void setPositions(Position[] pos) {
		this.positions = pos;
	}

	@Override
	public void doRender(DrawContext dc) {
		if (this.active) {
			super.doRender(dc);
		}
	}

	@Override
	public void drawProfile(DrawContext dc) {
		this.computeProfile(dc);

		if ((this.positions == null || (this.minElevation == 0 && this.maxElevation == 0)) && !this.initialized) {
			this.initialize(dc);
		}

		if (this.positions == null || (this.minElevation == 0 && this.maxElevation == 0)) {
			return;
		}

		GL2 gl = dc.getGL().getGL2();

		boolean attribsPushed = false;
		boolean modelviewPushed = false;
		boolean projectionPushed = false;

		try {
			gl.glPushAttrib(GL.GL_DEPTH_BUFFER_BIT | GL.GL_COLOR_BUFFER_BIT | GL2.GL_ENABLE_BIT | GL2.GL_TEXTURE_BIT
					| GL2.GL_TRANSFORM_BIT | GL2.GL_VIEWPORT_BIT | GL2.GL_CURRENT_BIT);
			attribsPushed = true;

			gl.glDisable(GL.GL_TEXTURE_2D); // no textures

			gl.glEnable(GL.GL_BLEND);
			gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
			gl.glDisable(GL.GL_DEPTH_TEST);

			Rectangle viewport = dc.getView().getViewport();
			Dimension drawSize = this.isMinimized ? new Dimension(MINIMIZED_SIZE, MINIMIZED_SIZE)
					: this.isMaximized
							? new Dimension(viewport.width - this.borderWidth * 2,
									viewport.height * 2 / 3 - this.borderWidth * 2)
							: this.size;
			double width = drawSize.width;
			double height = drawSize.height;

			// Load a parallel projection with xy dimensions (viewportWidth,
			// viewportHeight)
			// into the GL projection matrix.
			gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
			gl.glPushMatrix();
			projectionPushed = true;
			gl.glLoadIdentity();
			double maxwh = width > height ? width : height;
			gl.glOrtho(0d, viewport.width, 0d, viewport.height, -0.6 * maxwh, 0.6 * maxwh);

			gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
			gl.glPushMatrix();
			modelviewPushed = true;
			gl.glLoadIdentity();

			// Scale to a width x height space
			// located at the proper position on screen
			double scale = this.computeScale(viewport);
			Vec4 locationSW = this.computeLocation(viewport, scale);
			gl.glTranslated(locationSW.x(), locationSW.y(), locationSW.z());
			gl.glScaled(scale, scale, 1d);

			if (!dc.isPickingMode()) {
				// Draw grid - Set color using current layer opacity
				this.drawGrid(dc, drawSize);

				// Draw profile graph
				this.drawGraph(dc, drawSize);

				if (!this.isMinimized) {
					// Draw GUI buttons
					drawGUI(dc, drawSize);

					// Draw labels
					String label = String.format("min %.0fm   max %.0fm", this.minElevation, this.maxElevation);
					if (this.unit.equals(UNIT_IMPERIAL)) {
						label = String.format("min %.0fft   max %.0fft", this.minElevation * METER_TO_FEET,
								this.maxElevation * METER_TO_FEET);
					}
					gl.glLoadIdentity();
					gl.glDisable(GL.GL_CULL_FACE);
					drawLabel(dc, label, locationSW.add3(new Vec4(0, -12, 0)), -1); // left
					// aligned
					if (this.pickedSample != -1) {
						double pickedElevation = this.positions[this.pickedSample].getElevation();
						label = String.format("%.0fm", pickedElevation);
						if (this.unit.equals(UNIT_IMPERIAL)) {
							label = String.format("%.0fft", pickedElevation * METER_TO_FEET);
						}
						drawLabel(dc, label, locationSW.add3(new Vec4(width, -12, 0)), 1); // right
						// aligned
					}
				}
			} else {
				// Picking
				this.pickSupport.clearPickList();
				this.pickSupport.beginPicking(dc);
				// Draw unique color across the rectangle
				Color color = dc.getUniquePickColor();
				int colorCode = color.getRGB();
				if (!this.isMinimized) {
					// Update graph pick point
					computePickPosition(dc, locationSW, new Dimension((int) (width * scale), (int) (height * scale)));
					// Draw GUI buttons
					drawGUI(dc, drawSize);
				} else {
					// Add graph to the pickable list for 'un-minimize' click
					this.pickSupport.addPickableObject(colorCode, this);
					gl.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());
					gl.glBegin(GL2.GL_POLYGON);
					gl.glVertex3d(0, 0, 0);
					gl.glVertex3d(width, 0, 0);
					gl.glVertex3d(width, height, 0);
					gl.glVertex3d(0, height, 0);
					gl.glVertex3d(0, 0, 0);
					gl.glEnd();
				}
				// Done picking
				this.pickSupport.endPicking(dc);
				this.pickSupport.resolvePick(dc, dc.getPickPoint(), this);
			}
		} catch (Exception e) {
			// e.printStackTrace();
		} finally {
			if (projectionPushed) {
				gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
				gl.glPopMatrix();
			}
			if (modelviewPushed) {
				gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
				gl.glPopMatrix();
			}
			if (attribsPushed) {
				gl.glPopAttrib();
			}
		}
	}

	@Override
	protected Vec4 computeLocation(java.awt.Rectangle viewport, double scale) {
		double scaledWidth = scale * (this.isMinimized ? MINIMIZED_SIZE
				: this.isMaximized ? viewport.width - this.borderWidth * 2 : this.size.width);
		double scaledHeight = scale * (this.isMinimized ? MINIMIZED_SIZE
				: this.isMaximized ? viewport.height * 2 / 3 - this.borderWidth * 2 : this.size.height);

		double x;
		double y;

		if (this.locationCenter != null) {
			x = this.locationCenter.x - scaledWidth / 2;
			y = this.locationCenter.y - scaledHeight / 2;
		} else if (this.position.equals(AVKey.NORTHEAST)) {
			x = viewport.getWidth() - scaledWidth - this.borderWidth;
			y = viewport.getHeight() - scaledHeight - this.borderWidth;
		} else if (this.position.equals(AVKey.SOUTHEAST)) {
			x = viewport.getWidth() - scaledWidth - this.borderWidth;
			y = 0d + this.borderWidth + 40;
		} else if (this.position.equals(AVKey.NORTHWEST)) {
			x = 0d + this.borderWidth;
			y = viewport.getHeight() - scaledHeight - this.borderWidth;
		} else if (this.position.equals(AVKey.SOUTHWEST)) {
			x = 0d + this.borderWidth;
			y = 0d + this.borderWidth;
		} else // use North East
		{
			x = viewport.getWidth() - scaledWidth / 2 - this.borderWidth;
			y = viewport.getHeight() - scaledHeight / 2 - this.borderWidth;
		}

		if (this.locationOffset != null) {
			x += this.locationOffset.x;
			y += this.locationOffset.y;
		}

		return new Vec4(x, y, 0);
	}

	/*
	 * @EventSubscriber(eventClass = MeasureProfileEvent.class) public void onMeasureProfile(MeasureProfileEvent evt) {
	 * if (this != evt.getSource()) { this.active = !evt.isActive(); } }
	 */
}
