package fr.ifremer.viewer3d.layers.background;

import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;

/**
 * Provide the parameter Composite of a ShpLayer
 */
@Component(name = "globe_parameters_view_bakground_node_composite_factory", service = IParametersViewCompositeFactory.class)
public class BackgroundNodeCompositeFactory implements IParametersViewCompositeFactory {

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	/** Constructor */
	public BackgroundNodeCompositeFactory() {
		ContextInitializer.inject(this);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection == projectExplorerModel.getBackgroundNode()
				? Optional.of(new BackgroundComposite(parent, SWT.NONE))
				: Optional.empty();
	}

}
