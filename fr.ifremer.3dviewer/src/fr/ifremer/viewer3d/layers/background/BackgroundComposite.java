package fr.ifremer.viewer3d.layers.background;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Timer;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.utils.preference.PreferenceEditorDialog;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.Viewer3DSceneController;
import fr.ifremer.viewer3d.gui.actions.CameraTranslationAction;
import fr.ifremer.viewer3d.preference.Viewer3DPreferences;
import fr.ifremer.viewer3d.util.MyMeasureTool;
import fr.ifremer.viewer3d.util.MyMeasureToolController;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;
import gov.nasa.worldwind.SceneController;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.util.measure.MeasureTool;

/**
 * Composite with general options for the globe
 * 
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 */
public class BackgroundComposite extends Composite implements PropertyChangeListener {
	private DataBindingContext bindingContext;

	/**
	 * Measure tool utilities.
	 */
	private static MyMeasureTool measureTool;
	private PropertyChangeListener measureToolPropertyChangeListener;

	// SWT Widgets
	private Button measureButton;
	private Button exportButton;
	private Button importButton;
	private Label lengthLabel;
	private Label lengthValueLabel;
	private Label directLenghtLabel;
	private Label directLenghtValueLabel;
	private Label helpLabel;
	private Scale elevationScale;
	private Timer camElevationTimer;
	private Button hightQualityOftexture;
	private Button interpolate;
	private Button modifyMaxExaggeration;

	private Button hideAllLayersButton;
	private Button btnClear;

	private WritableBoolean displayNavigationLineProjection = new WritableBoolean();

	public BackgroundComposite(Composite parent, int style) {
		super(parent, style);
		init();
		initMeasurTool();

		// add the pcs to listen to the event shortcut
		Viewer3D.addListener(this);

		// remove listeners on dispose
		addDisposeListener(e -> {
			Viewer3D.removeListener(this);
			if (measureToolPropertyChangeListener != null)
				measureTool.removePropertyChangeListener(measureToolPropertyChangeListener);
		});

	}

	/**
	 * Initialize measure tool.
	 */
	private void initMeasurTool() {
		if (measureTool == null) {
			measureTool = new MyMeasureTool(Viewer3D.getWwd());
			// Handle measure tool events
			measureTool.setController(new MyMeasureToolController());
			measureTool.setMeasureShapeType(MeasureTool.SHAPE_LINE);
			measureTool.setFollowTerrain(true);
			measureTool.setShowAnnotation(true);
			// if loxodromic (default is orthodromic ie. GREAT_CIRCLE) see #58
			// _measureTool.setPathType(AVKey.LINEAR);

		}
		measureToolPropertyChangeListener = event -> {
			// Metric changed - sent after each render frame
			if (event.getPropertyName().equals(MeasureTool.EVENT_METRIC_CHANGED)) {
				Display.getDefault().asyncExec(() -> {
					if (!measureButton.isDisposed() && measureButton.getSelection()) {
						updateMetric();
					}
				});
			}
		};
		measureTool.addPropertyChangeListener(measureToolPropertyChangeListener);
		updateMetric();
		measureButton.setSelection(measureTool.isSelected());
		measureButton.notifyListeners(SWT.Selection, new Event());
	}

	/**
	 * Update value labels.
	 */
	private void updateMetric() {
		if (measureTool != null && lengthValueLabel != null && directLenghtValueLabel != null) {
			// Update length label
			double value = measureTool.getLength();
			exportButton.setEnabled(value > 0);
			String s;
			if (value <= 0) {
				s = "na";
			} else if (value < 1000) {
				s = String.format("%7.1f m", value);
			} else {
				s = String.format("%7.4f km", value / 1000);
			}
			lengthValueLabel.setText(s);

			// A  vol d'oiseau
			value = measureTool.getDirectLength();
			if (value <= 0) {
				s = "na";
			} else if (value < 1000) {
				s = String.format("%7.1f m", value);
			} else {
				s = String.format("%7.4f km", value / 1000);
			}
			directLenghtValueLabel.setText(s);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);

		createToolsGroup();

	}

	private Group createToolsGroup() {
		Group toolsGroup = new Group(this, SWT.NONE);
		toolsGroup.setText("Tools Panel");
		toolsGroup.setLayout(new GridLayout(1, false));
		toolsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// Vertical Exaggeration Group
		Group exaggerationGroup = new Group(toolsGroup, SWT.NONE);
		exaggerationGroup.setText("Vertical Exaggeration");
		exaggerationGroup.setLayout(new GridLayout(3, false));
		exaggerationGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		IGeographicViewService geographicView = IGeographicViewService.grab();

		int maxExaggeration = 20;
		Viewer3DPreferences prefs = null;
		PreferenceComposite preferencesRoot = PreferenceRegistry.getInstance().getViewsSettingsNode();
		List<PreferenceComposite> childList = preferencesRoot.getChildList();
		for (PreferenceComposite preferenceComposite : childList) {
			if (preferenceComposite instanceof Viewer3DPreferences) {
				prefs = (Viewer3DPreferences) preferenceComposite;
				maxExaggeration = prefs.getMaxVerticalExaggeration().getValue().intValue();
				break;
			}
		}

		int pageIncrement = maxExaggeration / 5;
		int increment = 1;

		Composite tickComposite = new Composite(exaggerationGroup, SWT.NONE);
		tickComposite.setLayout(new GridLayout(2, true));
		tickComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));

		final Label valueMinLabel = new Label(tickComposite, SWT.NONE);
		valueMinLabel.setText("1x");
		valueMinLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.END, true, false, 1, 1));

		final Label valueMaxLabel = new Label(tickComposite, SWT.NONE);
		valueMaxLabel.setText(maxExaggeration + "x");
		valueMaxLabel.setLayoutData(new GridData(GridData.END, GridData.END, true, false, 1, 1));
		new Label(exaggerationGroup, SWT.NONE);

		final Scale exaggerationScale = new Scale(exaggerationGroup, SWT.HORIZONTAL);
		exaggerationScale.setMinimum(1);
		exaggerationScale.setMaximum(maxExaggeration);
		exaggerationScale.setIncrement(increment);
		exaggerationScale.setPageIncrement(pageIncrement);
		exaggerationScale.setSelection((int) Viewer3D.getWwd().getSceneController().getVerticalExaggeration());
		exaggerationScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));

		modifyMaxExaggeration = new Button(exaggerationGroup, SWT.NONE);
		modifyMaxExaggeration.setImage(ImageResources.getImage("/icons/16/preferences-system.png", getClass()));

		var fPrefs = prefs;
		modifyMaxExaggeration.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				PreferenceEditorDialog dialog = new PreferenceEditorDialog(getShell());
				dialog.open(fPrefs, fPrefs.getMaxVerticalExaggeration());
			}
		});
		modifyMaxExaggeration.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 1, 1));

		final Label exaggerationLabel = new Label(exaggerationGroup, SWT.NONE);
		exaggerationLabel.setText("Vertical Exaggeration : ");
		exaggerationLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 1, 1));

		final Label valueLabel = new Label(exaggerationGroup, SWT.NONE);
		valueLabel.setAlignment(SWT.LEFT);
		valueLabel.setText("1x");
		valueLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));
		exaggerationScale.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				int value = exaggerationScale.getSelection();
				valueLabel.setText(value + "x");
				exaggerationGroup.redraw();
				geographicView.getWwd().getSceneController().setVerticalExaggeration(value);
			}
		});

		// observe preference attribute : max vertical exaggeration
		Observer preferenceObserver = (Observable o, Object arg) -> {
			Integer newMaxExaggeration = ((IntegerPreferenceAttribute) o).getValue();
			double currentExaggeration = geographicView.getWwd().getSceneController().getVerticalExaggeration();

			valueMaxLabel.setText(newMaxExaggeration.intValue() + "x");
			exaggerationScale.setMaximum(newMaxExaggeration.intValue());
			exaggerationScale.setPageIncrement((int) Math.max(1.0, newMaxExaggeration.doubleValue() / 5));

			if (currentExaggeration > newMaxExaggeration.doubleValue()) {
				valueLabel.setText(newMaxExaggeration + "x");
				exaggerationGroup.redraw();
				geographicView.getWwd().getSceneController().setVerticalExaggeration(newMaxExaggeration.doubleValue());
			}
		};
		var maxVerticalExaggerationPreferenceAttribute = prefs.getMaxVerticalExaggeration();
		maxVerticalExaggerationPreferenceAttribute.addObserver(preferenceObserver);
		addDisposeListener(e -> maxVerticalExaggerationPreferenceAttribute.deleteObserver(preferenceObserver));

		// Observe vertical exageration viewController
		PropertyChangeListener verticalExagerationListener = e -> Display.getDefault().asyncExec(() -> {
			if (!exaggerationScale.isDisposed() && exaggerationScale.getSelection() != (Double) e.getNewValue()) {
				exaggerationScale.setSelection((int) ((Double) e.getNewValue()).doubleValue());
			}
		});

		geographicView.getWwd().getSceneController().addPropertyChangeListener(AVKey.VERTICAL_EXAGGERATION,
				verticalExagerationListener);
		addDisposeListener(e -> geographicView.getWwd().getSceneController()
				.removePropertyChangeListener(verticalExagerationListener));

		// Measure Group
		Group measureGroup = new Group(toolsGroup, SWT.NONE);
		measureGroup.setText("Measure");
		measureGroup.setLayout(new GridLayout(2, false));
		measureGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		measureButton = new Button(measureGroup, SWT.CHECK);
		measureButton.setToolTipText("Removes current measure");
		exportButton = new Button(measureGroup, SWT.NONE);

		btnClear = new Button(measureGroup, SWT.NONE);
		btnClear.setToolTipText("Removes current mesasure");
		btnClear.setText("Clear");
		btnClear.addListener(SWT.Selection, e -> measureTool.clear());

		importButton = new Button(measureGroup, SWT.NONE);
		lengthLabel = new Label(measureGroup, SWT.NONE);
		lengthValueLabel = new Label(measureGroup, SWT.NONE);
		directLenghtLabel = new Label(measureGroup, SWT.NONE);
		directLenghtValueLabel = new Label(measureGroup, SWT.NONE);
		helpLabel = new Label(measureGroup, SWT.NONE);

		measureButton.setText("Measure");
		measureButton.addListener(SWT.Selection, event -> {
			boolean selected = measureButton.getSelection();
			if (!selected && measureTool.isSelected()) {
				measureTool.exit();
				selected = measureTool.isSelected();
				measureButton.setSelection(selected);
				measureTool.clear();
			}
			importButton.setEnabled(selected);
			measureTool.setSelected(selected);
			helpLabel.setVisible(selected);
			updateMetric();
		});
		measureButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		exportButton.setText("Export Profile");
		exportButton.setEnabled(false);
		exportButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				measureTool.export();
			}
		});
		exportButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		importButton.setText("Import Profile..");
		importButton.setEnabled(false);
		importButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				measureTool.importProfile();
			}
		});
		importButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		lengthLabel.setText("Length : ");
		lengthLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		lengthValueLabel.setText("na");
		lengthValueLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		directLenghtLabel.setText("Direct Length : ");
		directLenghtLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		directLenghtValueLabel.setText("na");
		directLenghtValueLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		helpLabel.setText("Use \"P\" key + left click to add new measure points.\nUse \"P\" key + right click to remove points.");
		helpLabel.setVisible(measureButton.getSelection());
		helpLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));

		// Vertical Elevation
		Group vElevationGroup = new Group(toolsGroup, SWT.NONE);
		vElevationGroup.setText("Vertical Elevation");
		vElevationGroup.setLayout(new GridLayout(1, true));
		vElevationGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		elevationScale = new Scale(vElevationGroup, SWT.HORIZONTAL);
		elevationScale.setMinimum(0);
		elevationScale.setMaximum(20);
		elevationScale.setSelection(10);
		elevationScale.setPageIncrement(1);

		camElevationTimer = new Timer(80, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CameraTranslationAction
								.changeElevation2((int) (-1 * Math.signum(elevationScale.getSelection() - 10)
										* Math.pow(elevationScale.getSelection() - 10, 2)));
						// elevationLabel.setText(CameraTranslationAction.getCameraElevation());
					}

				});
			}
		});

		elevationScale.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				camElevationTimer.stop();
				elevationScale.setSelection(10);
			}

			@Override
			public void mouseDown(MouseEvent e) {
				camElevationTimer.start();
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		elevationScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// Skirts
		final Button skirtButton = new Button(toolsGroup, SWT.CHECK);
		skirtButton.setText("Activate All Vertical Tiles (Skirts)");
		skirtButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		skirtButton.setSelection(geographicView.getWwd().getModel().getGlobe().getTessellator().isMakeTileSkirts());
		skirtButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				geographicView.getWwd().getModel().getGlobe().getTessellator()
						.setMakeTileSkirts(skirtButton.getSelection());
				geographicView.refresh();
			}
		});

		hideAllLayersButton = new Button(toolsGroup, SWT.PUSH);
		hideAllLayersButton.setText("Display/Hide all Terrain Layers");
		hideAllLayersButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				SceneController s = geographicView.getWwd().getSceneController();
				if (s instanceof Viewer3DSceneController) {
					// true
					((Viewer3DSceneController) (s)).setDisplayorHideTerrain();
				}
				RefreshOpenGLView.fullRefresh();
			}
		});

		// Quality of textures
		hightQualityOftexture = new Button(toolsGroup, SWT.CHECK);
		hightQualityOftexture.setText("Display textures at a higher resolution");
		hightQualityOftexture.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));
		hightQualityOftexture.setSelection(geographicView.isHighResolution());
		hightQualityOftexture.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				geographicView.setHighResolution();
			}
		});

		// interpolate
		interpolate = new Button(toolsGroup, SWT.CHECK);
		interpolate.setText("Linear Interpolation");
		interpolate.setSelection(geographicView.isInterpolate());
		interpolate.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				geographicView.interpolateTerrain();
			}
		});

		return toolsGroup;
	}

	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		final String name = event.getPropertyName();

		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				if (Viewer3D.RESOLUTION_EVENT.equals(name)) {
					hightQualityOftexture.setSelection((Boolean) event.getNewValue());
				} else if (Viewer3D.INTERPOLATION_EVENT.equals(name)) {
					interpolate.setSelection((Boolean) event.getNewValue());
				}
			}
		});

	}

}
