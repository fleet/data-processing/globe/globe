package fr.ifremer.viewer3d.layers.background;

import java.util.Observable;

import jakarta.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.core.model.ModelPreferences;
import fr.ifremer.globe.core.utils.latlon.CoordinateDisplayType;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.layers.LatLonGraticuleLayer;

/**
 * {@link LatLonGraticuleLayer} bound on user preferences.
 */
@Creatable
@Singleton
public class WWLatLonGraticuleLayer extends LatLonGraticuleLayer {

	/**
	 * Constructor
	 */
	public WWLatLonGraticuleLayer() {
		super();
		ModelPreferences.getInstance().getGridCoordinate()
				.addObserver((Observable o, Object arg) -> updateAngleFormatFromPreference());
		updateAngleFormatFromPreference();
	}

	private void updateAngleFormatFromPreference() {
		setAngleFormat((CoordinateDisplayType) ModelPreferences.getInstance().getGridCoordinate().getValue());
	}

	/**
	 * Calls setAngleFormat() with {@link CoordinateDisplayType} parameter.
	 */
	public void setAngleFormat(CoordinateDisplayType displayFormat) {
		switch (displayFormat) {
		case DECIMAL -> setAngleFormat(Angle.ANGLE_FORMAT_DD);
		case DEG_MIN_DEC -> setAngleFormat(Angle.ANGLE_FORMAT_DM);
		case DEG_MIN_SEC -> setAngleFormat(Angle.ANGLE_FORMAT_DMS);
		}
	}

}
