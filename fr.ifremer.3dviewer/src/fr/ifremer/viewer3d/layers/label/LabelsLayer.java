/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.label;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.ILabelProperties;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.viewer3d.layers.label.impl.LabelProperties;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.GeographicText;
import gov.nasa.worldwind.render.GeographicTextRenderer;
import gov.nasa.worldwind.render.SurfaceText;
import gov.nasa.worldwind.render.UserFacingText;

/**
 * Layer to render labels.
 */
public class LabelsLayer extends RenderableLayer implements IWWLabelLayer {

	/** Font used to render labels */
	private Font defaultFont = Font.decode("Arial-PLAIN-20");
	/** Default color to render labels */
	private GColor defaultColor = GColor.DARK_RED;

	/** List of label to display */
	private List<LabelProperties> labelProperties = new ArrayList<>();

	/** Renderer of GeographicText */
	private GeographicTextRenderer textRenderer;

	/** Labels will be visible under this eye distance */
	private double visibilityDistance = 4000d;

	/** Constructor */
	public LabelsLayer(String name) {
		setName(name);
		setPickEnabled(false);

		textRenderer = new GeographicTextRenderer() {

			/** {@inheritDoc} */
			@Override
			protected Color applyOpacity(Color color, double opacity) {
				return super.applyOpacity(color, LabelsLayer.this.getOpacity());
			}
		};
		textRenderer.setCullTextEnabled(true);
		textRenderer.setCullTextMargin(2);
		textRenderer.setEffect(AVKey.TEXT_EFFECT_OUTLINE);
	}

	/** Add one label to render */
	@Override
	public ILabelProperties acceptLabel(String label, double longitude, double latitude, double elevation) {
		Position position = Position.fromDegrees(latitude, longitude, elevation);
		LabelProperties result = new LabelProperties(label, position, defaultColor, defaultFont, true);
		labelProperties.add(result);
		return result;
	}

	@Override
	public ILabelProperties acceptLabel(String label, double longitude, double latitude, GColor color,
			boolean userFacing, String fontName, int fontSize) {
		Position position = Position.fromDegrees(latitude, longitude);
		LabelProperties result = new LabelProperties(label, position, color, decodeFont(fontName, fontSize),
				userFacing);
		labelProperties.add(result);
		return result;
	}

	/** Clear all */
	@Override
	public void removeAllLabels() {
		labelProperties = new ArrayList<>();
	}

	/** Remove one label */
	@Override
	public void removelLabel(ILabelProperties oneLabelProperties) {
		List<LabelProperties> newlabelProperties = new ArrayList<>(labelProperties);
		newlabelProperties.remove(oneLabelProperties);
		labelProperties = newlabelProperties;
	}

	/** {@inheritDoc} */
	@Override
	public void doRender(DrawContext dc) {
		super.doRender(dc);
		if (!labelProperties.isEmpty() && dc.getGlobe() != null) {
			ElevationModel elevationModel = dc.getGlobe().getElevationModel();
			List<GeographicText> geographicTexts = new ArrayList<>();
			for (LabelProperties oneLabelProperties : labelProperties) {
				GeographicText geographicText = computeGeographicTextRenderer(elevationModel, oneLabelProperties);
				geographicTexts.add(geographicText);
			}
			manageVisibility(dc, geographicTexts);
			this.textRenderer.render(dc, geographicTexts);
		}
	}

	private GeographicText computeGeographicTextRenderer(ElevationModel elevationModel,
			LabelProperties oneLabelProperties) {
		GeographicText result = oneLabelProperties.getGeographicText();
		if (result == null) {
			if (oneLabelProperties.isUserFacing()) {
				// WW expects the distance above the terrain, not the absolute elevation
				Position absolutePosition = oneLabelProperties.getPosition();
				double terrainElevation = elevationModel.getElevation(absolutePosition.latitude,
						absolutePosition.longitude);
				double distanceAboveTerrain = absolutePosition.elevation - terrainElevation;
				var positionAboveTerrain = new Position(absolutePosition, distanceAboveTerrain);
				result = new UserFacingText(oneLabelProperties.getLabel(), positionAboveTerrain);
			} else
				result = new SurfaceText(oneLabelProperties.getLabel(), oneLabelProperties.getPosition());
			result.setColor(ColorUtils.convertGColorToAWT(oneLabelProperties.getColor()));
			result.setFont(oneLabelProperties.getFont());
			oneLabelProperties.setGeographicText(result);
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		super.dispose();
		labelProperties.clear();
		textRenderer = null;
	}

	/** Getter of {@link #defaultFont} */
	public Font getDefaultFont() {
		return defaultFont;
	}

	/** Manage the visibility of all labels */
	private void manageVisibility(DrawContext dc, List<GeographicText> geographicTexts) {
		Vec4 eyeVec = dc.getView().getEyePoint();
		for (GeographicText geographicText : geographicTexts) {
			Vec4 namePoint = dc.getGlobe().computePointFromPosition(geographicText.getPosition());
			double dist = eyeVec.distanceTo3(namePoint);
			geographicText.setVisible(dist < getVisibilityDistance());
		}
	}

	private Font decodeFont(String fontName, int fontSize) {
		if (fontName != null && !fontName.isEmpty() && fontSize > 0) {
			Font result = Font.decode(fontName);
			return result.deriveFont((float) fontSize);
		}
		return defaultFont;

	}

	/** Getter of {@link #visibilityDistance} */
	@Override
	public double getVisibilityDistance() {
		return visibilityDistance;
	}

	/** Setter of {@link #visibilityDistance} */
	@Override
	public void setVisibilityDistance(double visibilityDistance) {
		this.visibilityDistance = visibilityDistance;
	}
}
