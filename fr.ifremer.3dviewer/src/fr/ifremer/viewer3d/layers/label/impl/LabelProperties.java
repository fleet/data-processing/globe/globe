/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.label.impl;

import java.awt.Font;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.ILabelProperties;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.GeographicText;

/**
 * Pojo defining a label
 */
public class LabelProperties implements ILabelProperties {

	/** What to display Label */
	protected String label;
	/** Where to display Label */
	protected Position position;
	/** Color of the label */
	protected GColor color;
	/** How to display Label */
	protected boolean userFacing;
	/** Font used to render labels */
	protected Font font =null;

	/** Renderer of text */
	protected GeographicText geographicText;

	/** Constructor */
	public LabelProperties(String label, Position position, GColor color, Font font, boolean userFacing) {
		this.label = label;
		this.position = position;
		this.color = color;
		this.font = font;
		this.userFacing = userFacing;
	}

	/** Getter of {@link #label} */
	@Override
	public String getLabel() {
		return label;
	}

	/** {@inheritDoc} */
	@Override
	public void setLabel(String label) {
		this.label = label;
		geographicText = null;
	}

	/** Getter of {@link #position} */
	@Override
	public Position getPosition() {
		return position;
	}

	/** {@inheritDoc} */
	@Override
	public void setPosition(Position position) {
		this.position = position;
		geographicText = null;
	}

	/** Getter of {@link #color} */
	@Override
	public GColor getColor() {
		return color;
	}

	/** {@inheritDoc} */
	@Override
	public void setColor(GColor color) {
		this.color = color;
		geographicText = null;
	}

	/** Getter of {@link #userFacing} */
	@Override
	public boolean isUserFacing() {
		return userFacing;
	}

	/** {@inheritDoc} */
	@Override
	public void setUserFacing(boolean userFacing) {
		this.userFacing = userFacing;
		geographicText = null;
	}

	/** Getter of {@link #font} */
	public Font getFont() {
		return font;
	}

	/** Setter of {@link #font} */
	public void setFont(Font font) {
		this.font = font;
		geographicText = null;
	}

	/** Getter of {@link #geographicText} */
	public GeographicText getGeographicText() {
		return geographicText;
	}

	/** Setter of {@link #geographicText} */
	public void setGeographicText(GeographicText geographicText) {
		this.geographicText = geographicText;
	}

}
