package fr.ifremer.viewer3d.layers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.render.SurfaceTile;
import gov.nasa.worldwind.terrain.CompoundElevationModel;

/**
 * A WMS layer definition loaded from a WMS server. NB : it could contain more than one NWW layer.
 */
public class WMSLayerComponent implements ITarget, IDeletable {

	private List<Layer> _layers;
	private List<ElevationModel> _elevations;
	private WorldWindow _wwd;
	private String _name;

	public WMSLayerComponent(WorldWindow wwd) {
		_layers = new ArrayList<Layer>();
		_elevations = new ArrayList<ElevationModel>();
		_wwd = wwd;
	}

	public void add(Object layerOrElevationComponent) {
		if (layerOrElevationComponent instanceof Layer) {
			Layer layer = (Layer) layerOrElevationComponent;

			if (_name == null) {
				_name = layer.getName();
			}

			_layers.add(layer);

		} else if (layerOrElevationComponent instanceof ElevationModel) {
			ElevationModel elevation = (ElevationModel) layerOrElevationComponent;

			if (_name == null) {
				_name = elevation.getName();
			}

			_elevations.add(elevation);
		}
	}

	public void setVisible(boolean visible) {
		for (Layer layer : _layers) {
			if (!_wwd.getModel().getLayers().contains(layer)) {
				Viewer3D.insertBeforePlacenames(layer);
			}

			layer.setEnabled(visible);
		}
		for (ElevationModel elevation : _elevations) {
			CompoundElevationModel compoundModel = (CompoundElevationModel) _wwd.getModel().getGlobe()
					.getElevationModel();
			if (visible) {
				if (!compoundModel.getElevationModels().contains(elevation)) {
					compoundModel.addElevationModel(elevation);
				}
			} else {
				if (compoundModel.getElevationModels().contains(elevation)) {
					compoundModel.removeElevationModel(elevation);
				}
			}
		}
	}

	@Override
	public Sector getSector() {
		Sector sector = null;
		for (Layer layer : _layers) {
			if (layer instanceof SurfaceTile) {
				SurfaceTile tiledLayer = (SurfaceTile) layer;
				if (sector == null) {
					sector = tiledLayer.getSector();
				} else {
					sector.union(tiledLayer.getSector());
				}
			}
		}
		for (@SuppressWarnings("unused")
		ElevationModel elevation : _elevations) {
		}
		return sector;
	}

	public List<Layer> getLayers() {
		return _layers;
	}

	public List<ElevationModel> getElevations() {
		return _elevations;
	}

	public String getName() {
		return _name;
	}

	public void setOpacity(double d) {
		for (Layer layer : _layers) {
			layer.setOpacity(d);
		}
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		for (Layer l : _layers)
			IGeographicViewService.grab().removeLayer(l);
		return true;
	}
}
