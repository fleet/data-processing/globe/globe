package fr.ifremer.viewer3d.layers;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Scale;

import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.Layer;

/**
 * Composite that changes opacity scale for all layers
 * 
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 */
public class OpacityComposite extends Composite {

	// SWT Widget
	private Scale opacityScale;
	private Layer layer;

	public OpacityComposite(Composite parent, int style, Layer layer) {
		super(parent, style);
		this.layer = layer;
		init();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		this.setLayout(layout);
		createOpacityGroup();
	}

	private Group createOpacityGroup() {
		Group opacityGroup = new Group(this, SWT.NONE);
		opacityGroup.setText("Opacity Panel");
		opacityGroup.setLayout(new GridLayout(1, false));
		opacityGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		opacityScale = new Scale(opacityGroup, SWT.NONE);
		opacityScale.setMinimum(0);
		opacityScale.setMaximum(100);
		opacityScale.setSelection((int) (layer.getOpacity() * 100));
		opacityScale.setPageIncrement(25);
		opacityScale.addListener(SWT.Selection, event -> {
			layer.setOpacity(((double) opacityScale.getSelection()) / 100);
			Viewer3D.getWwd().redraw();
			layer.firePropertyChange(AVKey.LAYER, null, layer);
		});
		opacityScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		return opacityGroup;
	}

}
