/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.usbl;

import java.awt.Color;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.locationtech.jts.algorithm.ConvexHull;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;

import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.io.usbl.UsblSettings;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.layers.navigation.WWNavigationLayer;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Path;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.SurfaceCircle;
import gov.nasa.worldwind.render.SurfacePolygon;
import gov.nasa.worldwind.util.WWUtil;
import jakarta.inject.Inject;

/**
 * Layer used to display specific shape about an {@link UsblFileInfo}.
 */
public class UsblLayer extends RenderableLayer implements IWWUsblLayer {

	/** Constants **/
	private static final Color TARGET_COLOR = ColorUtils.convertGColorToAWT(GColor.GREEN);
	private static final Color USBL_COLOR = ColorUtils.convertGColorToAWT(GColor.ORANGE);
	private static final Color DELTA_COLOR = ColorUtils.convertGColorToAWT(GColor.RED);
	private static final double POINT_SIZE = 10d;

	/** Session service */
	@Inject
	private ISessionService sessionService;

	/** Layer's {@link UsblFileInfo}. */
	private final UsblFileInfo fileInfo;

	/**
	 * Constructor
	 */
	public UsblLayer(UsblFileInfo usblFileInfo) {
		this.fileInfo = usblFileInfo;
		setPickEnabled(false);
		setName("usbl_layer_" + FilenameUtils.getBaseName(usblFileInfo.getPath()));
		fitParameters(fileInfo.getSettings());
	}

	@Override
	public UsblFileInfo getFieldInfo() {
		return fileInfo;
	}

	@Override
	public UsblSettings getParameters() {
		return fileInfo.getSettings();
	}

	@Override
	public void setParameters(UsblSettings parameters) {
		fitParameters(parameters);
		sessionService.saveSession();
	}

	/** Accepts new parameters */
	private void fitParameters(UsblSettings parameters) {
		this.fileInfo.setSettings(parameters);
		setOpacity(getParameters().opacity());
		refresh();

		// Try to reload navigation layer with new user settings (to update color which depends of settings).
		Optional.ofNullable(IWWLayerService.grab()).map(IWWLayerService::getFileLayerStoreModel)
				.flatMap(wWFileLayerStoreModel -> wWFileLayerStoreModel.get(fileInfo))
				.map(layerStore -> layerStore.getLayers(WWNavigationLayer.class)).flatMap(Stream::findFirst)
				.ifPresent(WWNavigationLayer::reload);
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(getParameters().toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap sessionParameters) {
		sessionParameters.get(UsblSettings.SESSION_KEY).ifPresent(this::fitParameters);
	}

	@Override
	public void refresh() {
		clearRenderables();
		drawTarget();
		drawUsbl();
		drawDeltaLine();
		setOpacity(getParameters().opacity());
	}

	private Optional<LatLon> getTargetLatLon() {
		return Double.isFinite(getParameters().targetLatitude()) && Double.isFinite(getParameters().targetLongitude())
				? Optional.of(LatLon.fromDegrees(getParameters().targetLatitude(), getParameters().targetLongitude()))
				: Optional.empty();
	}

	private LatLon getPositionBeforeLanding() {
		return LatLon.fromDegrees(fileInfo.getAverageLatitudeBeforeLanding(),
				fileInfo.getAverageLongitudeBeforeLanding());
	}

	/**
	 * Draws circle from target position.
	 */
	private void drawTarget() {
		var parameters = getParameters();
		var optLatLonTarget = getTargetLatLon();
		if (parameters.displayTarget() && optLatLonTarget.isPresent()) {
			var latLonTarget = optLatLonTarget.get();
			// Circle
			var circle = new SurfaceCircle(latLonTarget, parameters.targetRadius());
			var attrs = new BasicShapeAttributes();
			attrs.setInteriorMaterial(new Material(TARGET_COLOR));
			attrs.setOutlineMaterial(new Material(WWUtil.makeColorBrighter(TARGET_COLOR)));
			attrs.setOutlineWidth(1);
			attrs.setOutlineOpacity(parameters.opacity());
			attrs.setInteriorOpacity(parameters.opacity());
			circle.setAttributes(attrs);
			addRenderable(circle);

			// Center point
			displayPoint(latLonTarget, TARGET_COLOR, "Target");
		}
	}

	/**
	 * Draws USBL average location before landing.
	 */
	private void drawUsbl() {
		var parameters = getParameters();
		if (parameters.displayLanding()) {
			var latLon = getPositionBeforeLanding();

			// Center point
			displayPoint(latLon, USBL_COLOR, String.format("Landing", parameters.durationBeforeLandingInSeconds()));

			// Points used to compute average.
			if (parameters.displayAllPointsBeforeLanding()) {
				var landingData = fileInfo.getDataBeforeLanding();
				// Calculate Convex Hull from landing data
				var coordinates = landingData.stream()
						.map(data -> new Coordinate(data.getLongitude(), data.getLatitude()))
						.toArray(Coordinate[]::new);
				ConvexHull convexHull2 = new ConvexHull(coordinates, new GeometryFactory());
				var hullPositions = Stream.of(convexHull2.getConvexHull().getCoordinates())
						.map(coordinate -> LatLon.fromDegrees(coordinate.y, coordinate.x)).toList();

				// Draw polygon from convex hull
				var pgon = new SurfacePolygon(hullPositions);
				pgon.setValue(AVKey.DISPLAY_NAME, "Surrounds the north pole");
				var pgonAttributes = new BasicShapeAttributes();
				pgonAttributes.setDrawInterior(true);
				pgonAttributes.setOutlineOpacity(parameters.opacity());
				pgonAttributes.setInteriorOpacity(parameters.opacity());
				pgonAttributes.setInteriorMaterial(new Material(USBL_COLOR));
				pgonAttributes.setOutlineMaterial(new Material(WWUtil.makeColorBrighter(USBL_COLOR)));
				pgon.setAttributes(pgonAttributes);
				addRenderable(pgon);

				/// Display landing positions
				landingData.forEach(
						data -> displayPoint(LatLon.fromDegrees(data.getLatitude(), data.getLongitude()), USBL_COLOR));
			}
		}
	}

	/**
	 * Draws line between target and USBL positions, with label.
	 */
	private void drawDeltaLine() {
		var optTargetLatLon = getTargetLatLon();
		if (optTargetLatLon.isPresent()) {
			Position targetPosition = new Position(optTargetLatLon.get(), 0);
			Position usblPosition = new Position(getPositionBeforeLanding(), 0);

			displayPoint(getPositionBeforeLanding(), DELTA_COLOR);

			// Dot line between target and USBL
			Path dottedLine = new Path(targetPosition, usblPosition);
			ShapeAttributes lineAttributes = new BasicShapeAttributes();
			lineAttributes.setOutlineMaterial(new Material(DELTA_COLOR));
			lineAttributes.setOutlineWidth(4);
			lineAttributes.setOutlineStippleFactor(2); // dot width
			dottedLine.setAttributes(lineAttributes);
			dottedLine.setAltitudeMode(WorldWind.CLAMP_TO_GROUND);
			dottedLine.setPathType(AVKey.GREAT_CIRCLE);
			addRenderable(dottedLine);

			// Label
			double deltaDistance = LatLon.ellipsoidalDistance(targetPosition, usblPosition,
					Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS);
			Angle deltaAzimuth = LatLon.ellipsoidalForwardAzimuth(targetPosition, usblPosition,
					Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS);
			var label = String.format("%.2fm %.1f°", deltaDistance, deltaAzimuth.degrees);
			displayPoint(Position.interpolate(0.5, targetPosition, usblPosition), DELTA_COLOR, Optional.of(label),
					Optional.of(0d));
		}
	}

	private void displayPoint(LatLon latlon, Color color) {
		displayPoint(latlon, color, Optional.empty(), Optional.empty());
	}

	private void displayPoint(LatLon latlon, Color color, String label) {
		displayPoint(latlon, color, Optional.of(label), Optional.empty());
	}

	private void displayPoint(LatLon latlon, Color color, Optional<String> optLabel, Optional<Double> optScale) {
		var pp = new PointPlacemark(new Position(latlon, 0));
		pp.setAltitudeMode(WorldWind.CLAMP_TO_GROUND);
		var ppAttrs = new PointPlacemarkAttributes();
		ppAttrs.setLineColor(ColorUtils.convertAWTtoGColor(color).toAABBGGRR());
		ppAttrs.setUsePointAsDefaultImage(true);
		ppAttrs.setScale(optScale.orElse(POINT_SIZE));

		// Label
		optLabel.ifPresent((text) -> {
			pp.setLabelText(text);
			ppAttrs.setLabelColor("ffffffff");
		});

		pp.setAttributes(ppAttrs);
		addRenderable(pp);
	}

}
