/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;

import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.event.Message;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.render.DrawContext;

/** Composite layer */
public class CompoundLayer implements Layer {

	protected Layer[] compoundLayers;

	/** Constructor */
	public CompoundLayer(Layer... layers) {
		this.compoundLayers = layers;
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		Arrays.stream(compoundLayers).forEach(Layer::dispose);
	}
	/** {@inheritDoc} */
	@Override
	public void onMessage(Message msg) {
		Arrays.stream(compoundLayers).forEach(layer->layer.onMessage(msg));
	}
	/** {@inheritDoc} */
	@Override
	public Object setValue(String key, Object value) {
		return compoundLayers[0].setValue(key, value);
	}
	/** {@inheritDoc} */
	@Override
	public boolean isEnabled() {
		return compoundLayers[0].isEnabled();
	}
	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		Arrays.stream(compoundLayers).forEach(layer->layer.propertyChange(evt));
	}
	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setEnabled(enabled));
	}
	/** {@inheritDoc} */
	@Override
	public String getName() {
		return compoundLayers[0].getName();
	}
	/** {@inheritDoc} */
	@Override
	public void setName(String name) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setName(name));
	}
	/** {@inheritDoc} */
	@Override
	public AVList setValues(AVList avList) {
		return compoundLayers[0].setValues(avList);
	}
	/** {@inheritDoc} */
	@Override
	public String getRestorableState() {
		return compoundLayers[0].getRestorableState();
	}
	/** {@inheritDoc} */
	@Override
	public double getOpacity() {
		return compoundLayers[0].getOpacity();
	}
	/** {@inheritDoc} */
	@Override
	public void restoreState(String stateInXml) {
		Arrays.stream(compoundLayers).forEach(layer->layer.restoreState(stateInXml));
	}
	/** {@inheritDoc} */
	@Override
	public Object getValue(String key) {
		return compoundLayers[0].getValue(key);
	}
	/** {@inheritDoc} */
	@Override
	public void setOpacity(double opacity) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setOpacity(opacity));
	}
	/** {@inheritDoc} */
	@Override
	public Collection<Object> getValues() {
		return compoundLayers[0].getValues();
	}
	/** {@inheritDoc} */
	@Override
	public String getStringValue(String key) {
		return compoundLayers[0].getStringValue(key);
	}
	/** {@inheritDoc} */
	@Override
	public boolean isPickEnabled() {
		return compoundLayers[0].isPickEnabled();
	}
	/** {@inheritDoc} */
	@Override
	public Set<Entry<String, Object>> getEntries() {
		return compoundLayers[0].getEntries();
	}
	/** {@inheritDoc} */
	@Override
	public boolean hasKey(String key) {
		return compoundLayers[0].hasKey(key);
	}
	/** {@inheritDoc} */
	@Override
	public Object removeKey(String key) {
		return compoundLayers[0].removeKey(key);
	}
	/** {@inheritDoc} */
	@Override
	public void setPickEnabled(boolean isPickable) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setPickEnabled(isPickable));
	}
	/** {@inheritDoc} */
	@Override
	public void preRender(DrawContext dc) {
		Arrays.stream(compoundLayers).forEach(layer->layer.preRender(dc));
	}
	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		Arrays.stream(compoundLayers).forEach(layer->layer.addPropertyChangeListener(propertyName,  listener));
	}
	/** {@inheritDoc} */
	@Override
	public void render(DrawContext dc) {
		Arrays.stream(compoundLayers).forEach(layer->layer.render(dc));
	}
	/** {@inheritDoc} */
	@Override
	public void pick(DrawContext dc, Point pickPoint) {
		Arrays.stream(compoundLayers).forEach(layer->layer.pick(dc,  pickPoint));
	}
	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		Arrays.stream(compoundLayers).forEach(layer->layer.removePropertyChangeListener(propertyName,  listener));
	}
	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		Arrays.stream(compoundLayers).forEach(layer->layer.addPropertyChangeListener(listener));
	}
	/** {@inheritDoc} */
	@Override
	public boolean isAtMaxResolution() {
		return compoundLayers[0].isAtMaxResolution();
	}
	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		Arrays.stream(compoundLayers).forEach(layer->layer.removePropertyChangeListener(listener));
	}
	/** {@inheritDoc} */
	@Override
	public boolean isMultiResolution() {
		return compoundLayers[0].isMultiResolution();
	}
	/** {@inheritDoc} */
	@Override
	public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		Arrays.stream(compoundLayers).forEach(layer->layer.firePropertyChange(propertyName,  oldValue,  newValue));
	}
	/** {@inheritDoc} */
	@Override
	public double getScale() {
		return compoundLayers[0].getScale();
	}
	/** {@inheritDoc} */
	@Override
	public boolean isNetworkRetrievalEnabled() {
		return compoundLayers[0].isNetworkRetrievalEnabled();
	}
	/** {@inheritDoc} */
	@Override
	public void setNetworkRetrievalEnabled(boolean networkRetrievalEnabled) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setNetworkRetrievalEnabled(networkRetrievalEnabled));
	}
	/** {@inheritDoc} */
	@Override
	public void firePropertyChange(PropertyChangeEvent propertyChangeEvent) {
		Arrays.stream(compoundLayers).forEach(layer->layer.firePropertyChange(propertyChangeEvent));
	}
	/** {@inheritDoc} */
	@Override
	public AVList copy() {
		return compoundLayers[0].copy();
	}
	/** {@inheritDoc} */
	@Override
	public void setExpiryTime(long expiryTime) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setExpiryTime(expiryTime));
	}
	/** {@inheritDoc} */
	@Override
	public AVList clearList() {
		return compoundLayers[0].clearList();
	}
	/** {@inheritDoc} */
	@Override
	public long getExpiryTime() {
		return compoundLayers[0].getExpiryTime();
	}
	/** {@inheritDoc} */
	@Override
	public double getMinActiveAltitude() {
		return compoundLayers[0].getMinActiveAltitude();
	}
	/** {@inheritDoc} */
	@Override
	public void setMinActiveAltitude(double minActiveAltitude) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setMinActiveAltitude(minActiveAltitude));
	}
	/** {@inheritDoc} */
	@Override
	public double getMaxActiveAltitude() {
		return compoundLayers[0].getMaxActiveAltitude();
	}
	/** {@inheritDoc} */
	@Override
	public void setMaxActiveAltitude(double maxActiveAltitude) {
		Arrays.stream(compoundLayers).forEach(layer->layer.setMaxActiveAltitude(maxActiveAltitude));
	}
	/** {@inheritDoc} */
	@Override
	public boolean isLayerInView(DrawContext dc) {
		return compoundLayers[0].isLayerInView(dc);
	}
	/** {@inheritDoc} */
	@Override
	public boolean isLayerActive(DrawContext dc) {
		return compoundLayers[0].isLayerActive(dc);
	}
	/** {@inheritDoc} */
	@Override
	public Double getMaxEffectiveAltitude(Double radius) {
		return compoundLayers[0].getMaxEffectiveAltitude(radius);
	}
	/** {@inheritDoc} */
	@Override
	public Double getMinEffectiveAltitude(Double radius) {
		return compoundLayers[0].getMinEffectiveAltitude(radius);
	}
}
