package fr.ifremer.viewer3d.layers;

import java.util.List;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWWallLayer;

/** ??? */
public abstract class MyAbstractWallLayer extends AbstractPlayableLayer implements IWWWallLayer {

	/**
	 * Init and wrong distance between mouse and layer.
	 */
	public static final double HIGH_DISTANCE = 10000;

	/**
	 * List of input images.
	 */
	protected List<String> imagesList;
}
