/*
 * @License@ 
 */
package fr.ifremer.viewer3d.layers;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidParameterException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.model.IPlayable;
import fr.ifremer.viewer3d.model.PathWallLines;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.model.ReflectivityNavData;
import fr.ifremer.viewer3d.model.info.Info;
import fr.ifremer.viewer3d.model.info.Signature2;
import fr.ifremer.viewer3d.render.SimplePathWallFragment;
import fr.ifremer.viewer3d.util.IFiles;
import fr.ifremer.viewer3d.util.ILazyLoading;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.view.BasicView;

/**
 * Layer for reflectivity data.
 * <p>
 * <b>Exigence : REQ-NR-003 :</b> Water column le long de la navigation<br/>
 * Représentation sur un axe parallèle à une navigation d’une Water column le long de la navigation des valeurs
 * d’échogrammes.<br/>
 * L’utilisateur peut accéder via un curseur aux données selon l’axe X, Across Distance.<br/>
 * Introduction d’une variable Zmax.
 * </p>
 * 
 * @author Sophie &lt;sophie.bregentl@altran.com&gt;
 */
@SuppressWarnings("unused") 
public class ReflectivityLayer extends MyAbstractWallLayer implements PropertyChangeListener, ITarget, IInfos, ILazyLoading, IFiles, IDeletable, IPlayable {

	protected Logger logger = LoggerFactory.getLogger(ReflectivityLayer.class);

	/** Un des format possible pour le nom des fichiers images. */
	private static final String IMG_FORMAT_A = "Reflectivity_%d.png";
	/** Un des format possible pour le nom des fichiers images. */
	private static final String IMG_FORMAT_B = "%05d.png";

	private SimplePathWallFragment texturedWall;

	/** Directory where input images are stored. */
	private String imagesPath;
	/** Input images filename format. */
	private String imgFormat = IMG_FORMAT_A;

	private int imgIndexBegin = 0;

	/**
	 * Default time interval between two frames in "auto-play" mode.
	 */
	private static final int TIMER_DEFAULT_DELAY = 100;

	/**
	 * Init and wrong distance between mouse and layer.
	 */
	public static final double HIGH_DISTANCE = 10000;

	/**
	 * The current player data.
	 */
	private PlayerBean playerbean;

	/**
	 * List of input images as a byte buffers list (one byte buffer for each image).
	 */
	protected List<ByteBuffer> byteBuffersList;

	/** Width in meter used to compute step between slices. */
	protected double width;

	/*
	 * add MAntoine playable
	 */
	protected List<Long> timeList;
	private String playableType = IPlayable.ISPLAYABLEWITHTIME;
	/*
	 * end
	 */
	private Signature2 type;

	/**
	 * The reflectivity data displayed in this layer.
	 */
	protected ReflectivityNavData reflectivityData = null;

	/**
	 * Index of last displayed frame.
	 */
	protected int lastIndex = 0;

	/**
	 * Sector for this layer location.
	 */
	protected Sector sector;

	protected Position startPos = null;

	/***
	 * water column along nav
	 */
	private String wcType = "WCAlongNavigation";

	/***
	 * water column along nav
	 */
	private String sismicType = "SismicAlongNav";

	protected boolean useMipMaps;

	@Override
	public Position getPosition() {
		return startPos;
	}


	/**
	 * Constructor. Creates a layer that does not use mipmaps.
	 * 
	 * @param playerdata
	 * @param reflectivityData navigation data displayed by this layer.
	 * @param name
	 * @param signature
	 */
	public ReflectivityLayer(PlayerBean playerdata, ReflectivityNavData reflectivityData, String name, Signature2 signature,Info infoXML) {
		this(playerdata, reflectivityData, name, signature, false,infoXML);
	}

	/**
	 * Ctor.
	 * 
	 * @param reflectivityData
	 *            navigation data displayed by this layer.
	 * @throws InvalidParameterException
	 *             if navData is null
	 */
	public ReflectivityLayer(PlayerBean playerdata, ReflectivityNavData reflectivityData, String name, Signature2 signature, boolean useMipMaps, Info infoXML) {
		if (playerdata == null) {
			throw new InvalidParameterException("Player data cannot be null");
		}

		if (Viewer3D.getWwd() != null) {
			((BasicView) Viewer3D.getWwd().getView()).setDetectCollisions(false);
		}
		setName(name);
		this.type = signature;

		// applicationFrame = appFrame;
		this.reflectivityData = reflectivityData;

		this.useMipMaps = useMipMaps;

		// Initialize the color palette feature
		listerColorMap(true);
		setUseColorMap(true);
		setColorMapOri(0);
		setColorMap(0);

		// original color map should be jet for WC, gray for sismic, otherwise
		// we don't know
		int indexOri=0;
		if(infoXML!=null)
		{
			String colorOri=infoXML.getImageColor();
			if(colorOri!=null)
			{
				indexOri = this._availableColorMaps.indexOf(colorOri);
				if(indexOri ==-1) indexOri=0;
			}
		}
		switch (type) {
		case ImagesAlongNavigation:
		case WCAlongNavigation:
			indexOri=0;
			break;
		case SismicAlongNav:
		case SeismicAlongNav:
			indexOri=2;
			break;
		default:
			indexOri=0;
			break;
		}
		setColorMapOri(indexOri);
		setColorMap(findMatchingColorMap(indexOri));

		playerbean = playerdata;
		if (playerbean != null) {
			playerbean.addPropertyChangeListener(this);
			playerbean.setDelay(TIMER_DEFAULT_DELAY);
		}

		//Update texture, contrast and transparency with XML min/max values
		this.setMaxTextureValue(infoXML.getMaxValue());
		this.setMinTextureValue(infoXML.getMinValue());
		this.setMaxContrast(infoXML.getMaxValue());
		this.setMinContrast(infoXML.getMinValue());
		this.setMaxTransparency(infoXML.getMaxValue());
		this.setMinTransparency(infoXML.getMinValue());

		String imgDir = checkImagesDir(reflectivityData.getFile());
		if (imgDir != null) {
			reflectivityData.setImgDir(imgDir);
			imagesPath = imgDir;
		} else {
			imgDir = reflectivityData.getImgDir();
			// File f = new File(imgDir);
			createImages(Integer.valueOf(reflectivityData.getInfos().getInfo("Pings").getValueAsString()), Integer.valueOf(reflectivityData.getInfos().getInfo("Rows").getValueAsString()), reflectivityData.getMapLineReflectiviltyValues());
		}
		// MAJ des indexs des images
		if (playerbean != null) {

			if (playerbean != null) {
				playerbean.setIndexList(reflectivityData.getIndexList());
				playerbean.initializeLimitValuesWithoutTiggeringEvents(0, reflectivityData.getIndexList().size() - 1);
				if (reflectivityData.getInfos().getInfo("MiddleSlice") != null && Integer.valueOf(reflectivityData.getInfos().getInfo("MiddleSlice").getValueAsString()) > 0) {
					playerbean.setPlayIndex(Integer.valueOf(reflectivityData.getInfos().getInfo("MiddleSlice").getValueAsString()));
				} else {
					playerbean.setPlayIndex(reflectivityData.getLines().size() / 2);
				}

			}
		}
	}
	/**
	 * return the color map index given the origin index (Jet, Catherine, Grey)
	 * */
	private int findMatchingColorMap(int originIndex)
	{
		String pattern="GRAY";
		switch (originIndex) {
		case 1:
			pattern= "ather";
			break;
		case 0: 
			pattern ="jet";
			break;
		default:
			pattern="GRAY";
			break;
		}
		for(int i=0;i<_availableColorMaps.size();i++)
		{
			if(_availableColorMaps.get(i).contains(pattern))
			{
				return i;
			}
		}
		return 0;
	}
	/*
	@Override
	public void setMaxContrast(double maxContrast)
	{
		double value= (255*(maxContrast- this.getMinTextureValue()))/(this.getMaxTextureValue() - this.getMinTextureValue()); 
		super.setMaxContrast(value);

	}

	@Override
	public void setMinContrast(double minContrast)
	{
		double value=(255*(minContrast- this.getMinTextureValue()))/(this.getMaxTextureValue() - this.getMinTextureValue()); 
		super.setMinContrast(value);

	}


	@Override
	public void setMaxTransparency(double maxTransparency)
	{
		double value=(255*(maxTransparency- this.getMinTextureValue()))/(this.getMaxTextureValue() - this.getMinTextureValue()); 
		super.setMaxTransparency(value);

	}


	@Override
	public void setMinTransparency(double minTransparency)
	{
		double value=(255*(minTransparency- this.getMinTextureValue()))/(this.getMaxTextureValue() - this.getMinTextureValue()); 
		super.setMinTransparency(value);

	}*/




	/**
	 * Checks if images already exists, otherwise try to build them from 3D raw data.
	 * 
	 * @param file
	 *            the input XML file
	 * @return the images directory absolute path
	 */
	private String checkImagesDir(File file) {
		// sometimes it begins with 00000, but sometimes 00001 ...
		String[] defaultFilenames = new String[] { "/00000.png", "/00001.png" };

		// filename without extension
		String name = file.getName();
		name = name.substring(0, name.lastIndexOf("."));

		// checks if images are directly in data directory
		String dirPath = file.getParent() + "/" + name;
		for (String dfn : defaultFilenames) {
			File imgFile = new File(dirPath + dfn);
			if (imgFile.exists()) {
				imgIndexBegin = Integer.parseInt(dfn.substring(5, 6)); // begins
				// at 0
				// or 1
				imgFormat = IMG_FORMAT_B;
				return dirPath;
			}
		}

		// checks if images are in numbered images directories
		dirPath = file.getParent() + "/" + name + "/000/";
		for (String dfn : defaultFilenames) {
			File imgFile = new File(dirPath + dfn);
			if (imgFile.exists()) {
				imgIndexBegin = Integer.parseInt(dfn.substring(5, 6)); // begins
				// at 0
				// or 1
				imgFormat = IMG_FORMAT_B;
				return dirPath;
			}
		}

		return null;
	}

	private void createImages(int nbPings, int nbRows, List<float[][]> reflectiviltyValues) {
		int i = 0;
		// creation du dossier parent dans le cache
		File layerDirectory = new File(reflectivityData.getImgDir(), this.getName());
		if (!layerDirectory.exists()) {
			layerDirectory.mkdir();
		}
		imagesPath = layerDirectory.getPath();
		// fin creation
		for (float[][] floatArray : reflectiviltyValues) {
			// building an image for one slice.
			calcImage(nbPings, nbRows, floatArray, i);
			i++;
		}
		reflectiviltyValues.clear();
	}

	private void calcImage(int nbPings, int nbRows, float[][] reflectiviltyValues, int counter) {
		BufferedImage img = new BufferedImage(nbPings, nbRows, BufferedImage.TYPE_INT_ARGB);
		int[] pixelTransparent = { 0, 0, 0, 0 };
		int[] pixelRouge = { 255, 0, 0, 255 };

		for (int rowNb = 0; rowNb < nbRows; rowNb++) {
			for (int pingNb = 0; pingNb < nbPings; pingNb++) {
				float reflect = reflectiviltyValues[pingNb][rowNb];
				if (reflect != 0.0) {
					img.getRaster().setPixel(pingNb, nbRows - 1 - rowNb, pixelRouge);
				} else {
					img.getRaster().setPixel(pingNb, nbRows - 1 - rowNb, pixelTransparent);
				}
			}
		}
		try {
			// creation des images
			// test si image existe
			// si oui ne pas créé
			String fileName = String.format(IMG_FORMAT_A, counter);
			File outputfile = new File(imagesPath, fileName);
			if (!outputfile.exists()) {
				ImageIO.write(img, "png", outputfile);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updates rendering.
	 * 
	 * @param currentIndex
	 *            the current image index (to replace)
	 * @param newIndex
	 *            the new image index
	 * 
	 *            TODO deal with currentIndex
	 */
	@Override
	public void refresh(int newIndex) {
		newIndex = this.reflectivityData.getIndexMap(newIndex); // donne le
		// numero
		// d'index en
		// fonction du
		// numero de
		// ping
		// if (imgIndexBegin==0) {
		// newIndex = newIndex-1;
		// }
		if (texturedWall != null) {
			// File inputfile = new File(imagesPath, fileName);
			String prefix = reflectivityData.getFile().getName();
			prefix = prefix.substring(0, prefix.lastIndexOf("."));
			File dir = reflectivityData.getFile().getParentFile();
			File inputfile = new File(getImageFileName(prefix, newIndex + 1, dir));
			if (inputfile.exists()) {
				try {
					int ping = newIndex;
					// if (imgIndexBegin==1) {
					// ping = newIndex-1;
					// }
					BufferedImage imageSource = ImageIO.read(inputfile);
					List<PathWallLines> lines = reflectivityData.getLines();
					texturedWall = new SimplePathWallFragment(imageSource, lines.get(ping), this, useMipMaps);
					lastIndex = newIndex;
					firePropertyChange(AVKey.LAYER, null, this);
					Viewer3D.getWwd().redraw();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			initialize();
		}
		updateTooltip(newIndex);
	}

	private void updateTooltip(int newIndex) {
		StringBuilder sb = new StringBuilder(this.getStringValue(AVKey.DISPLAY_NAME));
		sb.append("\nSlice ");
		// sb.append(newIndex);
		sb.append(this.reflectivityData.getIndex(newIndex));
		if (reflectivityData.getSliceNameList() != null && reflectivityData.getSliceName(newIndex) != null) {
			sb.append("\n" + reflectivityData.getSliceName(newIndex));
		} else {
			int size = reflectivityData.getLines().size();
			if (size > 0) {
				sb.append(String.format("\nStep %.2fm", width / size));
			}
		}
		setValue(AVKey.ROLLOVER_TEXT, sb.toString());
	}

	/**
	 * Updates rendering.
	 * 
	 * @param currentTime
	 *            the current Time to render
	 * @param newTime
	 *            the new Time to render
	 */
	public void refresh(Long currentTime, Long newTime) {
		if (timeList.contains(newTime)) {
			int newIndex;
			newIndex = timeList.indexOf(newTime);
			refresh(newIndex);
		}
	}

	/**
	 * Time interval between two frames (ms).
	 * 
	 * @return time interval between two frames (ms).
	 */
	public int getDelay() {
		return playerbean.getDelay();
	}

	/**
	 * Time interval between two frames (ms).
	 * 
	 * @param delay
	 *            time interval between two frames (ms).
	 */
	public void setDelay(int delay) {
		playerbean.setDelay(delay);
		Logging.logger().info(DicoBundle.getString("KEY_WATERCOLUMNLAYER_DELAY_MESSAGE") + " = " + delay); //$NON-NLS-1$
	}

	/**
	 * Returns player bean used by this layer.
	 * 
	 * @return player bean used by this layer
	 */
	@Override
	public PlayerBean getPlayerBean() {
		return playerbean;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String name = event.getPropertyName();
		if (PlayerBean.PROPERTY_PLAY_INDEX.equals(name)) {
			int newIdx = (Integer) event.getNewValue();
			refresh(newIdx);
		}

		super.propertyChange(event);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sector getSector() {
		return this.sector;
	}

	@Override
	protected void doPick(DrawContext dc, Point point) {
		if (texturedWall == null) {
			initialize();
		}
		if (dc != null && texturedWall != null) {
			if (sector.intersects(dc.getVisibleSector())) {
				texturedWall.updateEyeDistance(dc);
				dc.addOrderedRenderable(texturedWall);
			}
		}
	}

	@Override
	protected void doRender(DrawContext dc) {
		if (texturedWall == null) {
			initialize();
		}
		if (dc != null && texturedWall != null) {
			if (sector.intersects(dc.getVisibleSector())) {
				texturedWall.updateEyeDistance(dc);
				dc.addOrderedRenderable(texturedWall);
			} else {
				//setValue(SSVAVKey.CURRENTLY_VISIBLE, false);
			}
		}
	}

	/**
	 * Returns input data.
	 */
	public ReflectivityNavData getData() {
		return reflectivityData;
	}

	@Override
	public File getInputFile() {
		if (reflectivityData != null) {
			return reflectivityData.getFile();
		}
		return null;
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public List<Property<?>> getProperties() {
		return reflectivityData.getProperties();
	}

	public void addInfo(String key, Object value) {
		if (value != null) {
			reflectivityData.addInfo(key, value);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<File, Boolean> getFiles() {
		Map<File, Boolean> files = new HashMap<File, Boolean>(2);
		if (reflectivityData != null) {
			files.put(reflectivityData.getFile(), true);
			for (File f : reflectivityData.getBinaryFiles()) {
				files.put(f, true);
			}
			files.put(new File(imagesPath), true);
		}
		return files;

	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	@Override
	public Long getEndTime() {
		return timeList.get(timeList.size() - 1);
	}

	@Override
	public Long getStartTime() {
		return timeList.get(0);
	}

	public Long getTime(int index) {
		return timeList.get(index);
	}

	@Override
	public String getPlayableType() {
		return playableType;
	}

	@Override
	public void setMovement(ISectorMovementProcessing movement) {
		Object oldValue = this.movement;
		super.setMovement(movement);
		firePropertyChange(new PropertyChangeEvent(this, "movement", oldValue, movement));
	}


	/** ???!!! */
	public double distance(double X, double Y) {
		double distance = HIGH_DISTANCE;
		return distance;
	}

	@Override
	public void setPosition(Position startPos) {
		// no location for now
	}

	@Override
	public Position getPosition(int index) {
		// no location for now
		return null;
	}

	@Override
	public void initialize() {
		int size = reflectivityData.getLines().size();
		int indexMiddle;
		if (reflectivityData.getInfos().getInfo("MiddleSlice").getValue() != null && Integer.valueOf(reflectivityData.getInfos().getInfo("MiddleSlice").getValueAsString()) > 0) {
			indexMiddle = Integer.valueOf(reflectivityData.getInfos().getInfo("MiddleSlice").getValueAsString()) + 1;
		} else {
			indexMiddle = size / 2;
		}
		if(indexMiddle>=reflectivityData.getLines().size())
		{
			indexMiddle = 0;

		}
		if (indexMiddle >= 0) {
			PathWallLines nlmin = reflectivityData.getLines().get(0);
			PathWallLines nlmax = reflectivityData.getLines().get(size - 1);
			Position p1 = nlmin.getTop(0);
			Position p2 = nlmax.getTop(0);
			double eqR = 6378137.0; // equatorial radius WGS84
			double poR = 6356752.3142; // polar radius
			width = LatLon.ellipsoidalDistance(p1, p2, eqR, poR);
			if (indexMiddle == 0) {
				width = 0;
			}
		}
		this.sector = reflectivityData.getLines().get(indexMiddle).computeBoundingSector();

		String prefix = reflectivityData.getFile().getName();
		prefix = prefix.substring(0, prefix.lastIndexOf("."));
		File dir = reflectivityData.getFile().getParentFile();
		File inputfile = new File(getImageFileName(prefix, indexMiddle + 1, dir));

		BufferedImage imageSource;
		try {
			imageSource = ImageIO.read(inputfile);
			texturedWall = new SimplePathWallFragment(imageSource, reflectivityData.getLines().get(indexMiddle), this, useMipMaps);
		} catch (IOException e) {
			logger.error("Error loading image file " + inputfile.getAbsolutePath(), e);
		}
	}

	/**
	 * Returns image file name.
	 * 
	 * @param imgDir
	 *            image directory name
	 * @param imageNumber
	 *            the image index
	 * @param dir
	 *            the image base directory
	 * @return the image file name or null if not found
	 */
	protected String getImageFileName(String imgDir, int imageNumber, File dir) {
		Formatter formatter = new Formatter();
		String fileName = null;

		// format without extension
		String imgFormat_png = imgFormat.substring(0, imgFormat.lastIndexOf("."));
		if (dir != null) {
			if (!imagesPath.endsWith("000/")) {
				fileName = formatter.format("%s/%s/" + imgFormat_png, dir //$NON-NLS-1$
						.getAbsolutePath(), imgDir, imageNumber) + DicoBundle.getString("KEY_WATERCOLUMNLAYER_FILE_EXTENSION"); //$NON-NLS-1$
			} else {
				int dirNumber = imageNumber / 100;
				fileName = formatter.format("%s/%s/%03d/" + imgFormat_png, dir //$NON-NLS-1$
						.getAbsolutePath(), imgDir, dirNumber, imageNumber) + DicoBundle.getString("KEY_WATERCOLUMNLAYER_FILE_EXTENSION"); //$NON-NLS-1$
			}
		}
		formatter.close();

		return fileName;
	}

}
