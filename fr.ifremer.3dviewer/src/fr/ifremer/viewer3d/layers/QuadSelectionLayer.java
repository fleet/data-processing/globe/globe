/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import jakarta.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWQuadSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.WWRenderableLayer;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.view.orbit.OrbitView3DInputHandler;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.ViewInputHandler;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Path;

/**
 * Layer to manage quad selection.
 */
public class QuadSelectionLayer extends WWRenderableLayer implements IWWQuadSelectionLayer {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(QuadSelectionLayer.class);

	@Inject
	private IGeographicViewService geographicViewService;

	/** Color of selection polylines */
	protected Color selectionColor = Color.red;
	/** Width of the selection lines */
	protected int selectionLineWidth = 2;

	/** Selection quad */
	protected Path polyline;

	/** Listeners for the quad selection */
	protected MouseListener mouseListener;
	protected EscapeListener escapeListener;
	protected Optional<Consumer<List<GeoPoint>>> selectionListener;

	/** True when selection is started */
	protected boolean selectionArmed = false;

	/** Sounder's projection */
	protected Projection projection;

	/**
	 * Constructor.
	 */
	public QuadSelectionLayer(Projection projection) {
		this.projection = projection;
		setPickEnabled(false);
		setName("Detection selection");
		setIcon(ImageResources.getImage("/icons/16/selection.png", getClass()));
		ContextInitializer.inject(this);
		setEnabled(false); // Enable must be requested explicitly
	}

	/**
	 * Initialize this layer to accept some other lines.
	 */
	public void clear() {
		if (polyline != null) {
			removeRenderable(polyline);
			polyline = null;
		}

	}

	/**
	 * Add listeners
	 */
	protected void hookListeners() {
		if (mouseListener == null) {
			mouseListener = new MouseListener();
			geographicViewService.getWwd().getInputHandler().addMouseListener(mouseListener);
			geographicViewService.getWwd().getInputHandler().addMouseMotionListener(mouseListener);
		}

		if (escapeListener == null) {
			escapeListener = new EscapeListener();
			geographicViewService.getWwd().getInputHandler().addKeyListener(escapeListener);
		}
	}

	/**
	 * Add listeners
	 */
	protected void unhookListeners() {
		if (mouseListener != null) {
			geographicViewService.getWwd().getInputHandler().removeMouseListener(mouseListener);
			geographicViewService.getWwd().getInputHandler().removeMouseMotionListener(mouseListener);
			mouseListener = null;
		}

		if (escapeListener != null) {
			geographicViewService.getWwd().getInputHandler().removeKeyListener(escapeListener);
			escapeListener = null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (enabled) {
			hookListeners();
		} else {
			unhookListeners();
		}
	}

	/**
	 * Disposes the mouse and escape listeners
	 */
	@Override
	public void dispose() {
		super.dispose();
		unhookListeners();
		polyline = null;
	}

	/**
	 * Add a position to the polyline after a mouse event
	 */
	protected void addPosition(Position position) {

		List<Position> positions = new ArrayList<>();
		Iterable<? extends Position> iterablePosition = polyline.getPositions();
		Iterator<? extends Position> positionIterator = iterablePosition.iterator();
		Position firstPosition = positionIterator.next();
		positions.add(firstPosition);
		positions.add(position);
		positions.add(position);
		positions.add(firstPosition);
		positions.add(firstPosition);
		polyline.setPositions(positions);
	}

	/**
	 * Recompute of the polyline according to the mouse position
	 */
	protected void changeLastPosition(Position position) throws ProjectionException {

		Iterator<? extends Position> positionIterator = polyline.getPositions().iterator();

		Position p1 = positionIterator.next();
		Position p2 = positionIterator.next();
		GeoPoint g1 = project(p1);
		GeoPoint g2 = project(p2);
		GeoPoint g3Temp = project(position);

		if (positionIterator.hasNext()) {
			double x3tmp = g3Temp.getLong();
			double y3tmp = g3Temp.getLat();

			double x1 = g1.getLong();
			double y1 = g1.getLat();
			double x2 = g2.getLong();
			double y2 = g2.getLat();
			double x3;
			double y3;
			double x4;
			double y4;

			if (y2 == y1) {
				x3 = x2;
				y3 = y3tmp;
				x4 = x1;
				y4 = y3;
			} else if (x2 == x1) {
				x3 = x3tmp;
				y3 = y2;
				y4 = y1;
				x4 = x3;
			} else {
				double a = (y2 - y1) / (x2 - x1);
				double ap = 1 / a;

				x3 = (y3tmp - a * x3tmp - (y2 + x2 * ap)) / (-ap - a);
				y3 = a * x3 + (y3tmp - a * x3tmp);

				x4 = x3 + (x1 - x2);
				y4 = y3 + (y1 - y2);
			}

			List<Position> positions = new ArrayList<>();
			positions.add(p1);
			positions.add(p2);
			positions.add(unproject(new GeoPoint(x3, y3)));
			positions.add(unproject(new GeoPoint(x4, y4)));
			positions.add(p1);

			polyline.setPositions(positions);
		} else {
			List<Position> positions = new ArrayList<>();
			positions.add(p1);
			positions.add(position);

			polyline.setPositions(positions);
		}
	}

	/**
	 * Creates a polyline
	 */
	protected void createPolyline() {
		polyline = new Path();
		polyline.setFollowTerrain(true);
		polyline.setPathType(AVKey.LINEAR);
		polyline.setAltitudeMode(WorldWind.CLAMP_TO_GROUND);
		BasicShapeAttributes attr = new BasicShapeAttributes();
		attr.setDrawInterior(false);
		attr.setOutlineMaterial(new Material(selectionColor));
		attr.setOutlineOpacity(1d);
		attr.setOutlineWidth(selectionLineWidth);
		polyline.setAttributes(attr);
		addRenderable(polyline);
	}

	/**
	 * Validates the selection
	 */
	protected void validateSelection() throws ProjectionException {
		GeoPoint[] geoPoints = new GeoPoint[4];
		Iterable<? extends Position> iterablePosition = polyline.getPositions();
		Iterator<? extends Position> positionIterator = iterablePosition.iterator();

		for (int i = 0; i < geoPoints.length && positionIterator.hasNext(); i++) {
			geoPoints[i] = project(positionIterator.next());
		}

		if (geoPoints[geoPoints.length - 1] != null) {
			notifySelectionDone(geoPoints);
		}

		selectionArmed = false;
	}

	/** Reinitialize the polyline to mouse position */
	protected void prepareNewSelection(Position position) {
		allowGlobeRotation(false);
		List<Position> positions = new ArrayList<>();
		positions.add(position);
		positions.add(position);
		polyline.setPositions(positions);

		selectionArmed = true;
	}

	/**
	 * Allows or not the globe rotation
	 */
	protected void allowGlobeRotation(boolean enabled) {
		WorldWindowGLCanvas wwd = geographicViewService.getWwd();
		if (wwd != null) {
			View view = wwd.getView();
			if (view != null) {
				ViewInputHandler viewInputHandler = view.getViewInputHandler();
				if (viewInputHandler instanceof OrbitView3DInputHandler) {
					OrbitView3DInputHandler orbitView3DInputHandler = (OrbitView3DInputHandler) viewInputHandler;
					orbitView3DInputHandler.setEnabledMouseDragged(enabled);
				}
			}
		}
	}

	/** Notifies that the selection has been made or has changed */
	protected void notifySelectionDone(GeoPoint... geoPoints) {
		selectionListener.ifPresent(l -> l.accept(List.of(geoPoints[0], geoPoints[1], geoPoints[2], geoPoints[3])));
		allowGlobeRotation(true);
	}

	/** {@inheritDoc} */
	@Override
	public void setListener(Consumer<List<GeoPoint>> listener) {
		selectionListener = Optional.of(listener);
	}

	/** Set the quad selection */
	@Override
	public void setSelection(GeoPoint geoPoint1, GeoPoint geoPoint2, GeoPoint geoPoint3, GeoPoint geoPoint4) {
		try {
			Position p1 = unproject(geoPoint1);
			Position p2 = unproject(geoPoint2);
			Position p3 = unproject(geoPoint3);
			Position p4 = unproject(geoPoint4);

			if (polyline == null) {
				createPolyline();
			}
			List<Position> positions = new ArrayList<>();
			positions.add(p1);
			positions.add(p2);
			positions.add(p3);
			positions.add(p4);
			positions.add(p1);
			polyline.setPositions(positions);

			selectionArmed = false;

			notifySelectionDone(geoPoint1, geoPoint2, geoPoint3, geoPoint4);
		} catch (ProjectionException e) {
			logger.debug("Error while shifting the selection {}", e.getMessage());
		}
	}

	/** Convert a GeoPoint (latitude/longitude) to a Position (x/y in meters) */
	protected Position unproject(GeoPoint geoPoint) throws ProjectionException {
		double[] coords = getProjection().unproject(geoPoint.getLong(), geoPoint.getLat());
		return Position.fromDegrees(coords[1], coords[0]);
	}

	/** Convert a Position (x/y in meters) to a GeoPoint (latitude/longitude) */
	protected GeoPoint project(Position position) throws ProjectionException {
		double[] lonLat = getProjection().project(position.getLongitude().getDegrees(),
				position.getLatitude().getDegrees());
		return new GeoPoint(lonLat[0], lonLat[1]);
	}

	/**
	 * @return the {@link #selectionColor}
	 */
	public Color getSelectionColor() {
		return selectionColor;
	}

	/**
	 * @param selectionColor the {@link #selectionColor} to set
	 */
	public void setSelectionColor(Color selectionColor) {
		this.selectionColor = selectionColor;
	}

	/**
	 * @return the {@link #selectionLineWidth}
	 */
	public int getSelectionLineWidth() {
		return selectionLineWidth;
	}

	/**
	 * @param selectionLineWidth the {@link #selectionLineWidth} to set
	 */
	public void setSelectionLineWidth(int selectionLineWidth) {
		this.selectionLineWidth = selectionLineWidth;
	}

	/**
	 * Mouse listener for the click and drag during the selection
	 */
	protected class MouseListener extends MouseAdapter {

		/**
		 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseClicked(MouseEvent e) {
			try {
				Position position = geographicViewService.getWwd().getCurrentPosition();
				if (position != null) {
					position = new Position(position, 0d);
					if (polyline != null) {
						if (selectionArmed) {
							Iterable<? extends Position> iterablePosition = polyline.getPositions();
							AtomicInteger positionCount = new AtomicInteger();
							iterablePosition.forEach(p -> positionCount.incrementAndGet());

							if (positionCount.get() <= 2) {
								addPosition(position);
							} else {
								validateSelection();
							}
						} else {
							prepareNewSelection(position);
						}
					} else {
						createPolyline();
						prepareNewSelection(position);
					}
				}
			} catch (ProjectionException e1) {
				// latitude or longitude exceeded limits : ignore this point
			}
		}

		/**
		 * @see java.awt.event.MouseAdapter#mouseMoved(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseMoved(MouseEvent e) {
			try {
				if (polyline != null && selectionArmed) {
					Position position = geographicViewService.getWwd().getCurrentPosition();
					if (position != null) {
						position = new Position(position, 0d);
						changeLastPosition(position);
					}
				}
			} catch (ProjectionException e1) {
				// latitude or longitude exceeded limits : ignore this point
			}
		}

	}

	/**
	 * Cancels the selection by pressing the escape key
	 */
	public class EscapeListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
				clear();
				allowGlobeRotation(true);
				geographicViewService.refresh();
			}
		}
	}

	/**
	 * @return the {@link #selectionArmed}
	 */
	@Override
	public boolean isSelectionArmed() {
		return selectionArmed;
	}

	/**
	 * Getter of projection
	 */
	public Projection getProjection() {
		return projection;
	}

	/**
	 * Setter of projection
	 */
	public void setProjection(Projection projection) {
		this.projection = projection;
	}

}
