package fr.ifremer.viewer3d.layers.deprecated.dtm;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import javax.imageio.ImageIO;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.math.Range;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import fr.ifremer.globe.ui.utils.CacheDirectory;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.data.IBoundedMinMax;
import fr.ifremer.viewer3d.layers.ContrastShadeControler;
import fr.ifremer.viewer3d.layers.ContrastShadeHistogram;
import fr.ifremer.viewer3d.layers.MyAbstractLayerParameter;
import fr.ifremer.viewer3d.render.ShaderSurfaceTileRenderer;
import fr.ifremer.viewer3d.util.ILazyLoading;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.ScreenCredit;
import gov.nasa.worldwind.retrieve.BulkRetrievable;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWXML;

/**
 * Terrain tiled layer contrasted and shaded texture through shader
 *
 * @author MORVAN
 *
 */
@Deprecated(forRemoval = true)
public class ShaderElevationLayer extends MyBasicTiledImageLayer
		implements IWWTerrainLayer, BulkRetrievable, ILazyLoading, IBoundedMinMax {

	private ISessionService sessionService = ISessionService.grab();

	/** Current selected ShaderElevationLayer in the LayerView. */
	private static ShaderElevationLayer current;

	/** used to update heading in IHM */
	public static String view_Heading = "View Heading for shading";

	/** This flag can swith off all ShaderElevationLayers. */
	protected static boolean isDisplayAllowed = true;

	private Logger logger = LoggerFactory.getLogger(ShaderElevationLayer.class);

	private Composite composite;

	protected Sector sector;

	private String type = "";
	private String unit;
	private double globalMin;
	private double globalMax;
	private double valMin;
	private double valMax;
	private double resolution;
	private boolean is_Int24;
	protected boolean isInitialized;
	private boolean transparencyEnabled;

	protected String vertex_shader = "/shader/elevationShader16.vert";
	protected String fragment_shader = "/shader/elevationShader16.frag";

	protected Map<GL, Integer> shaderprograms = new HashMap<>(4);

	protected ShaderSurfaceTileRenderer renderer;

	protected double viewHeading;

	protected ContrastShadeControler contrastControler;

	/**
	 * Constructor.
	 *
	 * @param inputFile Source file.
	 * @param levelset Set of levels to use to render the file.
	 */
	public ShaderElevationLayer(File inputFile, LevelSet levelset) {
		super(inputFile, levelset);

		contrastControler = new ContrastShadeControler(this);

		renderer = createSurfaceTileRenderer();
		String s = (String) levelset.getValue(AVKey.DISPLAY_NAME);
		if (s == null) {
			s = levelset.getFirstLevel().getCacheName();
			if (s == null) {
				s = (String) levelset.getValue(AVKey.NAME);
			}
		}

		if (s != null) {
			s = s.replace("tmp/", "");
			s = s.replace(CacheDirectory.getCacheSubDirectory() + "/", "");
			s = s.replace(".mnt", "");
			setName(s);
		}

		// colormap
		_availableColorMaps = new ArrayList<>();
		setIsInt24(false);
		listerColorMap(true);

		// Save the session when one of the parameters changed
		getLayerParameters().gainPropertyChangeSupport().addPropertyChangeListener(e -> sessionService.saveSession());

	}

	/**
	 * Constructor.
	 *
	 * @param sourceFile Source file.
	 * @param domElement XML document describing the layer.
	 * @param params list of layer's parameters.
	 * @param type Layer's type ("DEPTH", "REFLECTIVITY" ...etc).
	 * @param unit Layer's values unit (like "m" for meters...).
	 * @param globalMin layer's global min bound.
	 * @param globalMax layer's global max bound.
	 * @param valMin layer's min value.
	 * @param valMax layer's max value.
	 */
	public ShaderElevationLayer(File sourceFile, Element domElement, AVList params, String type, String unit,
			double globalMin, double globalMax, double valMin, double valMax) {
		this(getParamsFromDocument(domElement, params), type);

		inputFile = sourceFile;
		renderer = createSurfaceTileRenderer();
		contrastControler = new ContrastShadeControler(this);
		reinit(type, unit, globalMin, globalMax, valMin, valMax);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.of(newName -> {
			var result = new ShaderElevationLayer(inputFile, levels);
			result.setName(newName);
			result.reinit(type, unit, globalMin, globalMax, valMin, valMax);
			result.setLayerParameters(getLayerParameters());
			return result;
		});
	}

	/**
	 */
	public void reinit(String type, String unit, double globalMin, double globalMax, double valMin, double valMax) {
		setType(type);
		setUnit(unit);
		this.globalMin = globalMin;
		this.globalMax = globalMax;
		this.valMin = valMin;
		this.valMax = valMax;

		// colormap
		_availableColorMaps = new ArrayList<>();
		setIsInt24(true);
		listerColorMap(true);
		setUseColorMap(true);
	}

	/**
	 * Constructor.
	 *
	 * @param parent Parent layer.
	 */
	public ShaderElevationLayer(ShaderElevationLayer parent) {
		super(parent);
		renderer = createSurfaceTileRenderer();
		contrastControler = new ContrastShadeControler(this);
	}

	/**
	 * Constructor.
	 *
	 * @param params Layer's parameters.
	 */
	public ShaderElevationLayer(AVList params, String type) {

		super(new LevelSet(params));

		String s = params.getStringValue(AVKey.DISPLAY_NAME);
		if (s != null) {
			s = s.replace("tmp/", "");
			s = s.replace(CacheDirectory.getCacheSubDirectory() + "/", "");
			s = s.replace(".mnt", "");
			setName(type + "_" + s);
		}

		String[] strings = (String[]) params.getValue(AVKey.AVAILABLE_IMAGE_FORMATS);
		if (strings != null && strings.length > 0) {
			setAvailableImageFormats(strings);
		}

		s = params.getStringValue(AVKey.TEXTURE_FORMAT);
		if (s != null) {
			setTextureFormat(s);
		}

		Double d = (Double) params.getValue(AVKey.OPACITY);
		if (d != null) {
			setOpacity(d);
		}

		d = (Double) params.getValue(AVKey.MAX_ACTIVE_ALTITUDE);
		if (d != null) {
			setMaxActiveAltitude(d);
		}

		d = (Double) params.getValue(AVKey.MIN_ACTIVE_ALTITUDE);
		if (d != null) {
			setMinActiveAltitude(d);
		}

		d = (Double) params.getValue(AVKey.MAP_SCALE);
		if (d != null) {
			setValue(AVKey.MAP_SCALE, d);
		}

		d = (Double) params.getValue(AVKey.DETAIL_HINT);
		if (d != null) {
			setDetailHint(d);
		}

		Boolean b = (Boolean) params.getValue(AVKey.FORCE_LEVEL_ZERO_LOADS);
		if (b != null) {
			setForceLevelZeroLoads(b);
		}

		b = (Boolean) params.getValue(AVKey.RETAIN_LEVEL_ZERO_TILES);
		if (b != null) {
			setRetainLevelZeroTiles(b);
		}

		b = (Boolean) params.getValue(AVKey.NETWORK_RETRIEVAL_ENABLED);
		if (b != null) {
			setNetworkRetrievalEnabled(b);
		}

		b = (Boolean) params.getValue(AVKey.USE_MIP_MAPS);
		if (b != null) {
			setUseMipMaps(b);
		}

		b = (Boolean) params.getValue(AVKey.USE_TRANSPARENT_TEXTURES);
		if (b != null) {
			setUseTransparentTextures(b);
		}

		Object o = params.getValue(AVKey.URL_CONNECT_TIMEOUT);
		if (o != null) {
			setValue(AVKey.URL_CONNECT_TIMEOUT, o);
		}

		o = params.getValue(AVKey.URL_READ_TIMEOUT);
		if (o != null) {
			setValue(AVKey.URL_READ_TIMEOUT, o);
		}

		o = params.getValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT);
		if (o != null) {
			setValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, o);
		}

		ScreenCredit sc = (ScreenCredit) params.getValue(AVKey.SCREEN_CREDIT);
		if (sc != null) {
			setScreenCredit(sc);
		}

		if (params.getValue(AVKey.TRANSPARENCY_COLORS) != null) {
			setValue(AVKey.TRANSPARENCY_COLORS, params.getValue(AVKey.TRANSPARENCY_COLORS));
		}

		setValue(AVKey.CONSTRUCTION_PARAMETERS, params.copy());

		// If any resources should be retrieved for this Layer, start a task to
		// retrieve those resources, and initialize
		// this Layer once those resources are retrieved.
		/*
		 * if (this.isRetrieveResources()) { this.startResourceRetrieval(); }
		 */
		setIsInt24(true);

		// Save the session when one of the parameters changed
		getLayerParameters().gainPropertyChangeSupport().addPropertyChangeListener(e -> sessionService.saveSession());

	}

	/**
	 * Constructor.
	 */
	public ShaderElevationLayer(File sourceFile, Document document, AVList parameters, String type) {
		this(sourceFile, document.getDocumentElement(), parameters, type, parseUnit(document),
				parseDouble(document, MyTiledImageLayer.XML_KEY_GlobalMinValue, -20000d),
				parseDouble(document, MyTiledImageLayer.XML_KEY_GlobalMaxValue, 20000d),
				parseDouble(document, MyTiledImageLayer.XML_KEY_MinValue, -20000d),
				parseDouble(document, MyTiledImageLayer.XML_KEY_MaxValue, 20000d));
	}

	protected static String parseUnit(Document document) {
		String result = IConstants.MeterUnit;
		NodeList parse_unit = document.getElementsByTagName("Unit"); //$NON-NLS-1$
		try {
			result = ((Element) parse_unit.item(0)).getChildNodes().item(0).getNodeValue();
		} catch (Exception e) {
			// Let the default value
		}
		return result;
	}

	protected static double parseDouble(Document document, String attributeName, double defaultValue) {
		double result = defaultValue;
		NodeList nodeList = document.getElementsByTagName(attributeName);
		try {
			if (nodeList.getLength() > 0) {
				result = Double.valueOf(((Element) nodeList.item(0)).getChildNodes().item(0).getNodeValue());
			}
		} catch (Exception e) {
			// Let the default value
		}
		return result;
	}

	protected ShaderSurfaceTileRenderer createSurfaceTileRenderer() {
		return new ShaderSurfaceTileRenderer(this);
	}

	@Override
	protected void clearTextureCache(DrawContext dc) {
		super.clearTextureCache(dc);
		renderer.clearCache();
	}

	/***
	 *
	 * @param gl
	 * @return shader program for current gl
	 */
	public Integer getShaderprograms(GL gl) {
		if (shaderprograms != null && shaderprograms.get(gl) != null) {
			return shaderprograms.get(gl);
		} else {
			return 0;
		}
	}

	@Override
	public boolean isInitialized() {
		return isInitialized;
	}

	public void initialize(DrawContext dc) {
		// Création Shader
		try {
			GL2 gl = dc.getGL().getGL2();
			if (!shaderprograms.containsKey(gl)) {
				shaderprograms.put(gl,
						ShaderUtil.createShader(Activator.getDefault(), gl, vertex_shader, getFragmentShaderName()));
			}
		} catch (GIOException e) {
			logger.error("Error reading fragment shader : ", e);
		}

		// init sector
		sector = getSector();

		isInitialized = true;
	}

	/** fragment_shader accessor */
	protected String getFragmentShaderName() {
		return fragment_shader;
	}

	@Override
	public void render(DrawContext dc) {
		// forecce hide of all terrain layers
		if (!isDisplayAllowed) {
			return;
		}

		// if (sector != null && sector.intersects(dc.getVisibleSector())) {
		// setValue(SSVAVKey.CURRENTLY_VISIBLE, true);
		// } else {
		// setValue(SSVAVKey.CURRENTLY_VISIBLE, false);
		// }

		GL2 gl = dc.getGL().getGL2();
		if (!isInitialized() || !shaderprograms.containsKey(gl)) {
			initialize(dc);
		}

		// update IHM Azimuth linked to view heading
		if (viewHeading != Viewer3D.getWwd().getView().getHeading().getDegrees()) {
			firePropertyChange(view_Heading, viewHeading,
					viewHeading = Viewer3D.getWwd().getView().getHeading().getDegrees());
		}

		super.render(dc);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void drawTiles(DrawContext dc) {

		// Sort tiles according to thier level number (ASC)
		MyTextureTile[] sortedTiles = new MyTextureTile[currentTiles.size()];
		sortedTiles = currentTiles.toArray(sortedTiles);
		Arrays.sort(sortedTiles, levelComparer);
		renderer.setLevel(sortedTiles[0].getLevel().getLevelNumber());

		// Delegate to the renderer
		renderer.setShowImageTileOutlines(isDrawTileBoundaries());
		renderer.renderTiles(dc, currentTiles);
	}

	@Override
	protected boolean loadTexture(MyTextureTile tile, java.net.URL textureURL) {
		TextureData textureData = null;

		synchronized (fileLock) {
			if (!getLevels().getFirstLevel().getFormatSuffix().equals(".tif")) {
				textureData = readTexture(textureURL, isUseMipMaps());
			}
		}

		if (textureData == null) {
			return false;
		}

		tile.setTextureData(textureData);
		renderer.setWidth(textureData.getWidth());
		renderer.setHeight(textureData.getHeight());

		if (tile.getLevelNumber() != 0 || !isRetainLevelZeroTiles()) {
			addTileToCache(tile);
		}

		return true;
	}

	public ShaderSurfaceTileRenderer getRender() {
		return renderer;
	}

	public static Document createTiledImageLayerConfigDocument(AVList params) {
		Document doc = WWXML.createDocumentBuilder(true).newDocument();

		Element root = WWXML.setDocumentElement(doc, "Layer");
		WWXML.setIntegerAttribute(root, "version", 1);
		WWXML.setTextAttribute(root, "layerType", "ShaderElevationLayer");

		createTiledImageLayerConfigElements(params, root);

		return doc;
	}

	protected static AVList getParamsFromDocument(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		getTiledImageLayerConfigParams(domElement, params);
		setFallbacks(params);

		return params;
	}

	public static AVList getTiledImageLayerConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		XPath xpath = WWXML.makeXPath();

		// Common layer properties.
		AbstractLayer.getLayerConfigParams(domElement, params);

		// LevelSet properties.
		DataConfigurationUtils.getLevelSetConfigParams(domElement, params);

		// Service properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.SERVICE_NAME, "Service/@serviceName", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE,
				"RetrievePropertiesFromService", xpath);

		// Image format properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.IMAGE_FORMAT, "ImageFormat", xpath);
		WWXML.checkAndSetStringParam(domElement, params, AVKey.TEXTURE_FORMAT, "TextureFormat", xpath);
		WWXML.checkAndSetUniqueStringsParam(domElement, params, AVKey.AVAILABLE_IMAGE_FORMATS,
				"AvailableImageFormats/ImageFormat", xpath);

		// Optional behavior properties.
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.FORCE_LEVEL_ZERO_LOADS, "ForceLevelZeroLoads", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETAIN_LEVEL_ZERO_TILES, "RetainLevelZeroTiles", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_MIP_MAPS, "UseMipMaps", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_TRANSPARENT_TEXTURES, "UseTransparentTextures",
				xpath);
		WWXML.checkAndSetDoubleParam(domElement, params, AVKey.DETAIL_HINT, "DetailHint", xpath);
		WWXML.checkAndSetColorArrayParam(domElement, params, AVKey.TRANSPARENCY_COLORS, "TransparencyColors/Color",
				xpath);

		// Retrieval properties. Convert the Long time values to Integers,
		// because BasicTiledImageLayer is expecting
		// Integer values.
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_CONNECT_TIMEOUT,
				"RetrievalTimeouts/ConnectTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_READ_TIMEOUT,
				"RetrievalTimeouts/ReadTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT,
				"RetrievalTimeouts/StaleRequestLimit/Time", xpath);

		// Parse the legacy configuration parameters. This enables
		// TiledImageLayer to recognize elements from previous
		// versions of configuration documents.
		getLegacyTiledImageLayerConfigParams(domElement, params);

		return params;
	}

	protected static AVList getLegacyTiledImageLayerConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		XPath xpath = XPathFactory.newInstance().newXPath();

		Object o = params.getValue(AVKey.TEXTURE_FORMAT);
		if (o == null) {
			Boolean b = WWXML.getBoolean(domElement, "CompressTextures", xpath);
			if (b != null && b) {
				params.setValue(AVKey.TEXTURE_FORMAT, "image/dds");
			}
		}

		return params;
	}

	public void setIsInt24(boolean is_Int24) {
		this.is_Int24 = is_Int24;
	}

	public boolean isInt24() {
		return is_Int24;
	}

	protected TextureData readTexture(java.net.URL url, boolean useMipMaps) {

		try {
			GLProfile glprofile = GLProfile.getDefault();
			// return TextureIO.newTextureData(glprofile, url, useMipMaps,
			// null);
			BufferedImage bi = ImageIO.read(url);
			return AWTTextureIO.newTextureData(glprofile, bi, this.useMipMaps);

		} catch (Exception e) {
			logger.error("layers.TextureLayer.ExceptionAttemptingToReadTextureFile", e);
			return null;
		}
	}

	public void setResolution(double resolution) {
		this.resolution = resolution;
	}

	public double getResolution() {
		return resolution;
	}

	@Override
	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	@Override
	public double getValMin() {
		return valMin;
	}

	@Override
	public void setValMax(double valMax) {
		this.valMax = valMax;
	}

	@Override
	public double getValMax() {
		return valMax;
	}

	@Override
	protected void addTile(DrawContext dc, MyTextureTile tile) {
		tile.setFallbackTile(null);

		if (ContrastShadeHistogram.isCreateHisto() && type == ContrastShadeHistogram.getTypeHistogram()) {
			tile.setLayer(this);
			ContrastShadeHistogram.addTile(tile);
		}

		if (tile.isTextureInMemory(dc.getTextureCache())) {
			addTileToCurrent(tile);
			return;
		}

		// Level 0 loads may be forced
		if (tile.getLevelNumber() == 0 && forceLevelZeroLoads && !tile.isTextureInMemory(dc.getTextureCache())) {
			forceTextureLoad(tile);
			if (tile.isTextureInMemory(dc.getTextureCache())) {
				addTileToCurrent(tile);
				return;
			}
		}

		// Tile's texture isn't available, so request it
		if (tile.getLevelNumber() < levels.getNumLevels()) {
			// Request only tiles with data associated at this level
			if (!levels.isResourceAbsent(tile)) {
				requestTexture(dc, tile);
			}
		}
	}

	@Override
	public void listerColorMap(boolean useFullList) {
		super.listerColorMap(useFullList);
		if (type.equals("Reflectivity")) {
			setColorMap(ColorMap.getColorMapIndex("GRAY"));
		} else {
			setColorMap(ColorMap.getColorMapIndex("GMT_jet"));
		}

		setMinTextureValue(getValMin() + getOffset());
		setMaxTextureValue(getValMax() + getOffset());
		setMinContrast(getValMin() + getOffset());
		setMaxContrast(getValMax() + getOffset());
		setMinTransparency(getValMin() + getOffset());
		setMaxTransparency(getValMax() + getOffset());
	}

	public void setComposite(Composite composite) {
		this.composite = composite;
	}

	public Composite getComposite() {
		return composite;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getType() {
		return type;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit() {
		return unit;
	}

	@Override
	public void setValGlobaleMin(double min) {
		globalMin = min;
	}

	@Override
	public double getValGlobaleMin() {
		return globalMin;
	}

	@Override
	public void setValGlobaleMax(double max) {
		globalMax = max;
	}

	@Override
	public double getValGlobaleMax() {
		return globalMax;
	}

	/***
	 * set current layer
	 *
	 * @param layer
	 */
	public static void setCurrent(ShaderElevationLayer layer) {
		current = layer;
	}

	public static ShaderElevationLayer getCurrent() {
		return current;
	}

	public ContrastShadeControler getContrastControler() {
		return contrastControler;
	}

	/***
	 * display or hide all terrain layers
	 */
	public static void displayHide() {
		isDisplayAllowed = !isDisplayAllowed;
	}

	@Override
	public void dispose() {
		super.dispose();
		contrastControler.dispose();
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(getLayerParameters().toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(MyAbstractLayerParameter.SESSION_KEY).ifPresent(this::setLayerParameters);
	}

	/** {@inheritDoc} */
	@Override
	public MyAbstractLayerParameter getLayerParameters() {
		return (MyAbstractLayerParameter) super.getLayerParameters();
	}

	@Override
	public TerrainParameters getTextureParameters() {
		return new TerrainParameters(
				// Color contrast
				getOpacity(), getMinContrast(), getMaxContrast(), (int) getColorMap(), isInverseColorMap(),
				// Shading
				isUseOmbrage(), isUseGradient(), isUseOmbrageLogarithmique(), getOmbrageExaggeration(), getAzimuth(),
				getZenith(),
				// Filter
				transparencyEnabled, getMinTransparency(), getMaxTransparency(), getMinTextureValue(),
				getMaxTextureValue());
	}

	@Override
	public void setTextureParameters(TerrainParameters parameters) {
		// apply color contrast
		setMinContrast(parameters.minContrast);
		setMaxContrast(parameters.maxContrast);
		setInverseColorMap(parameters.reverseColorMap);
		setOpacity(parameters.opacity);
		setColorMap(parameters.colorMap);

		// Apply shading
		setUseOmbrage(parameters.useShading);
		setUseGradient(parameters.useGradient);
		setUseOmbrageLogarithmique(parameters.useLogarithmicShading);
		setOmbrageExaggeration(parameters.shadingExaggeration);
		setAzimuth(parameters.azimuth);
		setZenith(parameters.zenith);

		// Apply Filter
		transparencyEnabled = parameters.transparencyEnabled;
		setMinTransparency(parameters.minTransparency);
		setMaxTransparency(parameters.maxTransparency);
	}

	@Override
	public Optional<Range> getVisibleValueRange() {
		return Optional.empty();
	}

	@Override
	public boolean isVisibleInView() {
		return true;
	}

}
