/*
Copyright (C) 2001, 2006 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
 */
package fr.ifremer.viewer3d.layers.deprecated.dtm;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.util.ILazyLoading;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.FileStore;
import gov.nasa.worldwind.event.BulkRetrievalListener;
import gov.nasa.worldwind.exception.WWRuntimeException;
import gov.nasa.worldwind.formats.dds.DDSCompressor;
import gov.nasa.worldwind.formats.dds.DXTCompressionAttributes;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.BasicTiledImageLayer;
import gov.nasa.worldwind.layers.BasicTiledImageLayerBulkDownloader;
import gov.nasa.worldwind.ogc.wms.WMSCapabilities;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.ScreenCredit;
import gov.nasa.worldwind.retrieve.AbstractRetrievalPostProcessor;
import gov.nasa.worldwind.retrieve.BulkRetrievable;
import gov.nasa.worldwind.retrieve.BulkRetrievalThread;
import gov.nasa.worldwind.retrieve.Retriever;
import gov.nasa.worldwind.retrieve.RetrieverFactory;
import gov.nasa.worldwind.retrieve.URLRetriever;
import gov.nasa.worldwind.util.AbsentResourceList;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.RestorableSupport;
import gov.nasa.worldwind.util.SessionCacheUtils;
import gov.nasa.worldwind.util.Tile;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * @author dom
 * @version $Id: MyBasicTiledImageLayer.java,v 1.3 2012/11/12 15:09:18 bvalliere Exp $
 */
@Deprecated(forRemoval = true)
public class MyBasicTiledImageLayer extends MyTiledImageLayer implements BulkRetrievable, ILazyLoading {
	protected final Object fileLock = new Object();

	// Layer resource properties.
	protected ScheduledExecutorService resourceRetrievalService;
	protected AbsentResourceList absentResources;
	protected static final int RESOURCE_ID_OGC_CAPABILITIES = 1;
	protected static final int DEFAULT_MAX_RESOURCE_ATTEMPTS = 3;
	protected static final int DEFAULT_MIN_RESOURCE_CHECK_INTERVAL = (int) 6e5; // 10
	// minutes
	protected static final int SERVICE_CAPABILITIES_RESOURCE_ID = 1;
	protected boolean serviceInitialized = false;

	/**
	 * Input file for this layer.
	 */
	protected File inputFile;

	public MyBasicTiledImageLayer(File inputFile, LevelSet levelSet) {
		super(levelSet);

		String s = (String) levelSet.getValue(AVKey.DISPLAY_NAME);
		if (s != null) {
			setName(s);
		} else {
			s = levelSet.getFirstLevel().getCacheName();
			if (s != null) {
				setName(s);
			} else {
				s = (String) levelSet.getValue(AVKey.NAME);
				if (s != null) {
					setName(s);
				}
			}
		}

		this.inputFile = inputFile;
	}

	public MyBasicTiledImageLayer(LevelSet levelSet) {
		super(levelSet);

		String s = (String) levelSet.getValue(AVKey.DISPLAY_NAME);
		if (s != null) {
			setName(s);
		} else {
			s = levelSet.getFirstLevel().getCacheName();
			if (s != null) {
				setName(s);
			} else {
				s = (String) levelSet.getValue(AVKey.NAME);
				if (s != null) {
					setName(s);
				}
			}
		}

		inputFile = null;
	}

	public MyBasicTiledImageLayer(MyBasicTiledImageLayer parent) {
		super(parent);
		inputFile = null;
	}

	public MyBasicTiledImageLayer(AVList params) {
		this(new LevelSet(params));

		String s = params.getStringValue(AVKey.DISPLAY_NAME);
		if (s != null) {
			setName(s);
		}

		String[] strings = (String[]) params.getValue(AVKey.AVAILABLE_IMAGE_FORMATS);
		if (strings != null && strings.length > 0) {
			setAvailableImageFormats(strings);
		}

		s = params.getStringValue(AVKey.TEXTURE_FORMAT);
		if (s != null) {
			setTextureFormat(s);
		}

		Double d = (Double) params.getValue(AVKey.OPACITY);
		if (d != null) {
			setOpacity(d);
		}

		d = (Double) params.getValue(AVKey.MAX_ACTIVE_ALTITUDE);
		if (d != null) {
			setMaxActiveAltitude(d);
		}

		d = (Double) params.getValue(AVKey.MIN_ACTIVE_ALTITUDE);
		if (d != null) {
			setMinActiveAltitude(d);
		}

		d = (Double) params.getValue(AVKey.MAP_SCALE);
		if (d != null) {
			setValue(AVKey.MAP_SCALE, d);
		}

		d = (Double) params.getValue(AVKey.DETAIL_HINT);
		if (d != null) {
			setDetailHint(d);
		}

		Boolean b = (Boolean) params.getValue(AVKey.FORCE_LEVEL_ZERO_LOADS);
		if (b != null) {
			setForceLevelZeroLoads(b);
		}

		b = (Boolean) params.getValue(AVKey.RETAIN_LEVEL_ZERO_TILES);
		if (b != null) {
			setRetainLevelZeroTiles(b);
		}

		b = (Boolean) params.getValue(AVKey.NETWORK_RETRIEVAL_ENABLED);
		if (b != null) {
			setNetworkRetrievalEnabled(b);
		}

		b = (Boolean) params.getValue(AVKey.USE_MIP_MAPS);
		if (b != null) {
			setUseMipMaps(b);
		}

		b = (Boolean) params.getValue(AVKey.USE_TRANSPARENT_TEXTURES);
		if (b != null) {
			setUseTransparentTextures(b);
		}

		Object o = params.getValue(AVKey.URL_CONNECT_TIMEOUT);
		if (o != null) {
			setValue(AVKey.URL_CONNECT_TIMEOUT, o);
		}

		o = params.getValue(AVKey.URL_READ_TIMEOUT);
		if (o != null) {
			setValue(AVKey.URL_READ_TIMEOUT, o);
		}

		o = params.getValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT);
		if (o != null) {
			setValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, o);
		}

		ScreenCredit sc = (ScreenCredit) params.getValue(AVKey.SCREEN_CREDIT);
		if (sc != null) {
			setScreenCredit(sc);
		}

		if (params.getValue(AVKey.TRANSPARENCY_COLORS) != null) {
			setValue(AVKey.TRANSPARENCY_COLORS, params.getValue(AVKey.TRANSPARENCY_COLORS));
		}

		setValue(AVKey.CONSTRUCTION_PARAMETERS, params.copy());

		// If any resources should be retrieved for this Layer, start a task to
		// retrieve those resources, and initialize
		// this Layer once those resources are retrieved.
		if (isRetrieveResources()) {
			startResourceRetrieval();
		}
	}

	public MyBasicTiledImageLayer(Document dom, AVList params) {
		this(dom.getDocumentElement(), params);
	}

	public MyBasicTiledImageLayer(Element domElement, AVList params) {
		this(getParamsFromDocument(domElement, params));
	}

	public MyBasicTiledImageLayer(String restorableStateInXml) {
		this(restorableStateToParams(restorableStateInXml));

		RestorableSupport rs;
		try {
			rs = RestorableSupport.parse(restorableStateInXml);
		} catch (Exception e) {
			// Parsing the document specified by stateInXml failed.
			String message = Logging.getMessage("generic.ExceptionAttemptingToParseStateXml", restorableStateInXml);
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message, e);
		}

		doRestoreState(rs, null);
	}

	/**
	 * Overridden to cancel periodic non-tile resource retrieval tasks scheduled by this Layer.
	 */
	@Override
	public void dispose() {
		super.dispose();

		// Stop any scheduled non-tile resource retrieval tasks. Resource
		// retrievals are performed in a separate thread,
		// and are unnecessary once the Layer is disposed.
		stopResourceRetrieval();
	}

	protected static AVList getParamsFromDocument(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		getTiledImageLayerConfigParams(domElement, params);
		setFallbacks(params);

		return params;
	}

	protected static void setFallbacks(AVList params) {
		if (params.getValue(AVKey.LEVEL_ZERO_TILE_DELTA) == null) {
			Angle delta = Angle.fromDegrees(36);
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, new LatLon(delta, delta));
		}

		if (params.getValue(AVKey.TILE_WIDTH) == null) {
			params.setValue(AVKey.TILE_WIDTH, 512);
		}

		if (params.getValue(AVKey.TILE_HEIGHT) == null) {
			params.setValue(AVKey.TILE_HEIGHT, 512);
		}

		if (params.getValue(AVKey.FORMAT_SUFFIX) == null) {
			params.setValue(AVKey.FORMAT_SUFFIX, ".dds");
		}

		if (params.getValue(AVKey.NUM_LEVELS) == null) {
			params.setValue(AVKey.NUM_LEVELS, 19); // approximately 0.1 meters
			// per pixel
		}

		if (params.getValue(AVKey.NUM_EMPTY_LEVELS) == null) {
			params.setValue(AVKey.NUM_EMPTY_LEVELS, 0);
		}
	}

	@Override
	protected void forceTextureLoad(MyTextureTile tile) {
		final URL textureURL = getDataFileStore().findFile(tile.getPath(), true);

		if (textureURL != null && !isTextureFileExpired(tile, textureURL, getDataFileStore())) {
			if (loadTexture(tile, textureURL)) {
				getLevels().unmarkResourceAbsent(tile);
			}
		}
	}

	@Override
	protected void requestTexture(DrawContext dc, MyTextureTile tile) {
		Vec4 centroid = tile.getCentroidPoint(dc.getGlobe());
		Vec4 referencePoint = getReferencePoint(dc);
		if (referencePoint != null) {
			tile.setPriority(centroid.distanceTo3(referencePoint));
		}

		RequestTask task = createRequestTask(tile);
		getRequestQ().add(task);
	}

	protected RequestTask createRequestTask(MyTextureTile tile) {
		return new RequestTask(tile, this);
	}

	protected static class RequestTask implements Runnable, Comparable<RequestTask> {
		protected final MyBasicTiledImageLayer layer;
		protected final MyTextureTile tile;

		protected RequestTask(MyTextureTile tile, MyBasicTiledImageLayer layer) {
			this.layer = layer;
			this.tile = tile;
		}

		@Override
		public void run() {
			// TODO: check to ensure load is still needed
			// AL -----------------------------
			// AL final java.net.URL textureURL =
			// WorldWind.getDataFileStore().findFile(tile.getPath(), false);

			boolean isBilFileFormat = layer.getLevels().getFirstLevel().getFormatSuffix().equals(".bil");

			java.net.URL textureURL = null;
			if (!isBilFileFormat) {
				textureURL = layer.getDataFileStore().findFile(tile.getPath(), false);
			}

			// <BRL: Hack to use 4 digits elevation data. (where does they
			// come from ??!!)
			try {
				if (textureURL == null && tile.getResourceURL() == null && !isBilFileFormat) {
					// FIXME avoiding tile.path with PrivateAccessor...
					Level lvl = tile.getLevel();
					String sRow = String.format("%04d", tile.getRow());
					String sCol = String.format("%04d", tile.getColumn());
					String path = lvl.getPath() + "/" + sRow + "/" + sRow + "_" + sCol;

					// try to find texture
					textureURL = layer.getDataFileStore().findFile(path, false);

					// if texture found, update tile path
					if (textureURL != null) {
						PrivateAccessor.setField(tile, "path", Tile.class, path);
						if (!lvl.isEmpty()) {
							PrivateAccessor.setField(tile, "path", Tile.class, tile.getPath() + lvl.getFormatSuffix());
						}
					}
				}
			} catch (MalformedURLException e) {
				Logging.logger().fine("Can't retrieve texture data : " + textureURL);
			}
			// BRL>

			if (textureURL != null && !layer.isTextureFileExpired(tile, textureURL, layer.getDataFileStore())) {
				// AL -----------------------------
				if (layer.loadTexture(tile, textureURL)) {
					layer.getLevels().unmarkResourceAbsent(tile);
					// Evite un deadlock très problématique!!!
					Model model = Viewer3D.getModel();
					synchronized (model) {
						layer.firePropertyChange(AVKey.LAYER, null, this);
						return;
					}
				} else {
					// Assume that something's wrong with the file and delete
					// it.
					// AL -----------------------------
					layer.getDataFileStore().removeFile(textureURL);
					// AL -----------------------------
					layer.getLevels().markResourceAbsent(tile);
					String message = Logging.getMessage("generic.DeletedCorruptDataFile", textureURL);
					Logging.logger().info(message);
				}
			}
			// AL -----------------------------
			if (!isBilFileFormat) {
				layer.retrieveTexture(tile, layer.createDownloadPostProcessor(tile));
				// AL -----------------------------
			}
		}

		/**
		 * @param that the task to compare
		 *
		 * @return -1 if <code>this</code> less than <code>that</code>, 1 if greater than, 0 if equal
		 *
		 * @throws IllegalArgumentException if <code>that</code> is null
		 */
		@Override
		public int compareTo(RequestTask that) {
			if (that == null) {
				String msg = Logging.getMessage("nullValue.RequestTaskIsNull");
				Logging.logger().severe(msg);
				throw new IllegalArgumentException(msg);
			}
			return tile.getPriority() == that.tile.getPriority() ? 0
					: tile.getPriority() < that.tile.getPriority() ? -1 : 1;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}

			final RequestTask that = (RequestTask) o;

			// Don't include layer in comparison so that requests are shared
			// among layers
			return !(tile != null ? !tile.equals(that.tile) : that.tile != null);
		}

		@Override
		public int hashCode() {
			return tile != null ? tile.hashCode() : 0;
		}

		@Override
		public String toString() {
			return tile.toString();
		}
	}

	protected boolean isTextureFileExpired(MyTextureTile tile, java.net.URL textureURL, FileStore fileStore) {
		if (!WWIO.isFileOutOfDate(textureURL, tile.getLevel().getExpiryTime())) {
			return false;
		}

		// The file has expired. Delete it.
		fileStore.removeFile(textureURL);
		String message = Logging.getMessage("generic.DataFileExpired", textureURL);
		Logging.logger().fine(message);
		return true;
	}

	protected boolean loadTexture(MyTextureTile tile, java.net.URL textureURL) {
		TextureData textureData;

		synchronized (fileLock) {
			textureData = readTexture(textureURL, getTextureFormat(), isUseMipMaps());
		}

		if (textureData == null) {
			return false;
		}

		tile.setTextureData(textureData);
		if (tile.getLevelNumber() != 0 || !isRetainLevelZeroTiles()) {
			addTileToCache(tile);
		}

		return true;
	}

	/**
	 * Reads and returns the texture data at the specified URL, optionally converting it to the specified format and
	 * generating mip-maps. If <code>textureFormat</code> is a recognized mime type, this returns the texture data in
	 * the specified format. Otherwise, this returns the texture data in its native format. If <code>useMipMaps</code>
	 * is true, this generates mip maps for any non-DDS texture data, and uses any mip-maps contained in DDS texture
	 * data.
	 * <p/>
	 * Supported texture formats are as follows:
	 * <ul>
	 * <li><code>image/dds</code> - Returns DDS texture data, converting the data to DDS if necessary. If the data is
	 * already in DDS format it's returned as-is.</li>
	 * </ul>
	 *
	 * @param url the URL referencing the texture data to read.
	 * @param textureFormat the texture data format to return.
	 * @param useMipMaps true to generate mip-maps for the texture data or use mip maps already in the texture data, and
	 *            false to read the texture data without generating or using mip-maps.
	 *
	 * @return TextureData the texture data from the specified URL, in the specified format and with mip-maps.
	 */
	protected TextureData readTexture(java.net.URL url, String textureFormat, boolean useMipMaps) {
		try {
			// If the caller has enabled texture compression, and the texture
			// data is not a DDS file, then use read the
			// texture data and convert it to DDS.
			if ("image/dds".equalsIgnoreCase(textureFormat) && !url.toString().toLowerCase().endsWith("dds")) {
				// Configure a DDS compressor to generate mipmaps based
				// according to the 'useMipMaps' parameter, and
				// convert the image URL to a compressed DDS format.
				DXTCompressionAttributes attributes = DDSCompressor.getDefaultCompressionAttributes();
				attributes.setBuildMipmaps(useMipMaps);
				ByteBuffer buffer = DDSCompressor.compressImageURL(url, attributes);

				GLProfile glprofile = GLProfile.getDefault();
				return TextureIO.newTextureData(glprofile, WWIO.getInputStreamFromByteBuffer(buffer), useMipMaps, null);
			}
			// If the caller has disabled texture compression, or if the texture
			// data is already a DDS file, then read
			// the texture data without converting it.
			else {
				GLProfile glprofile = GLProfile.getDefault();
				return TextureIO.newTextureData(glprofile, url, useMipMaps, null);
			}
		} catch (Exception e) {
			String msg = Logging.getMessage("layers.TextureLayer.ExceptionAttemptingToReadTextureFile", url);
			Logging.logger().log(java.util.logging.Level.SEVERE, msg, e);
			return null;
		}
	}

	protected void addTileToCache(MyTextureTile tile) {
		MyTextureTile.getMemoryCache().add(tile.getTileKey(), tile);
	}

	// *** Bulk download ***
	// *** Bulk download ***
	// *** Bulk download ***

	/**
	 * Start a new {@link BulkRetrievalThread} that downloads all imagery for a given sector and resolution to the
	 * current World Wind file cache, without downloading imagery that is already in the cache.
	 * <p/>
	 * This method creates and starts a thread to perform the download. A reference to the thread is returned. To create
	 * a downloader that has not been started, construct a {@link BasicTiledImageLayerBulkDownloader}.
	 * <p/>
	 * Note that the target resolution must be provided in radians of latitude per texel, which is the resolution in
	 * meters divided by the globe radius.
	 *
	 * @param sector the sector to download imagery for.
	 * @param resolution the target resolution, provided in radians of latitude per texel.
	 * @param listener an optional retrieval listener. May be null.
	 *
	 * @return the {@link BulkRetrievalThread} executing the retrieval or <code>null</code> if the specified sector does
	 *         not intersect the layer bounding sector.
	 *
	 * @throws IllegalArgumentException if the sector is null or the resolution is less than zero.
	 * @see BasicTiledImageLayerBulkDownloader
	 */
	@Override
	public BulkRetrievalThread makeLocal(Sector sector, double resolution, BulkRetrievalListener listener) {
		return makeLocal(sector, resolution, null, listener);
	}

	/**
	 * Start a new {@link BulkRetrievalThread} that downloads all imagery for a given sector and resolution to a
	 * specified {@link FileStore}, without downloading imagery tht is already in the file store.
	 * <p/>
	 * This method creates and starts a thread to perform the download. A reference to the thread is returned. To create
	 * a downloader that has not been started, construct a {@link BasicTiledImageLayerBulkDownloader}.
	 * <p/>
	 * Note that the target resolution must be provided in radians of latitude per texel, which is the resolution in
	 * meters divided by the globe radius.
	 *
	 * @param sector the sector to download data for.
	 * @param resolution the target resolution, provided in radians of latitude per texel.
	 * @param fileStore the file store in which to place the downloaded imagery. If null the current World Wind file
	 *            cache is used.
	 * @param listener an optional retrieval listener. May be null.
	 *
	 * @return the {@link BulkRetrievalThread} executing the retrieval or <code>null</code> if the specified sector does
	 *         not intersect the layer bounding sector.
	 *
	 * @throws IllegalArgumentException if the sector is null or the resolution is less than zero.
	 * @see BasicTiledImageLayerBulkDownloader
	 */
	@Override
	public BulkRetrievalThread makeLocal(Sector sector, double resolution, FileStore fileStore,
			BulkRetrievalListener listener) {
		Sector targetSector = sector != null ? getLevels().getSector().intersection(sector) : null;
		if (targetSector == null) {
			return null;
		}

		MyBasicTiledImageLayerBulkDownloader thread = new MyBasicTiledImageLayerBulkDownloader(this, targetSector,
				resolution, fileStore != null ? fileStore : getDataFileStore(), listener);
		thread.setDaemon(true);
		thread.start();
		return thread;
	}

	/**
	 * Get the estimated size in bytes of the imagery not in the World Wind file cache for the given sector and
	 * resolution.
	 * <p/>
	 * Note that the target resolution must be provided in radians of latitude per texel, which is the resolution in
	 * meters divided by the globe radius.
	 *
	 * @param sector the sector to estimate.
	 * @param resolution the target resolution, provided in radians of latitude per texel.
	 *
	 * @return the estimated size in bytes of the missing imagery.
	 *
	 * @throws IllegalArgumentException if the sector is null or the resolution is less than zero.
	 */
	@Override
	public long getEstimatedMissingDataSize(Sector sector, double resolution) {
		return this.getEstimatedMissingDataSize(sector, resolution, null);
	}

	/**
	 * Get the estimated size in bytes of the imagery not in a specified file store for a specified sector and
	 * resolution.
	 * <p/>
	 * Note that the target resolution must be provided in radians of latitude per texel, which is the resolution in
	 * meters divided by the globe radius.
	 *
	 * @param sector the sector to estimate.
	 * @param resolution the target resolution, provided in radians of latitude per texel.
	 * @param fileStore the file store to examine. If null the current World Wind file cache is used.
	 *
	 * @return the estimated size in byte of the missing imagery.
	 *
	 * @throws IllegalArgumentException if the sector is null or the resolution is less than zero.
	 */
	@Override
	public long getEstimatedMissingDataSize(Sector sector, double resolution, FileStore fileStore) {
		Sector targetSector = sector != null ? getLevels().getSector().intersection(sector) : null;
		if (targetSector == null) {
			return 0;
		}

		MyBasicTiledImageLayerBulkDownloader downloader = new MyBasicTiledImageLayerBulkDownloader(this, sector,
				resolution, fileStore != null ? fileStore : getDataFileStore(), null);

		return downloader.getEstimatedMissingDataSize();
	}

	// *** Tile download ***
	// *** Tile download ***
	// *** Tile download ***

	protected void retrieveTexture(MyTextureTile tile, DownloadPostProcessor postProcessor) {
		if (getValue(AVKey.RETRIEVER_FACTORY_LOCAL) != null) {
			retrieveLocalTexture(tile, postProcessor);
		} else {
			// Assume it's remote, which handles the legacy cases.
			retrieveRemoteTexture(tile, postProcessor);
		}
	}

	protected void retrieveLocalTexture(MyTextureTile tile, DownloadPostProcessor postProcessor) {
		if (!WorldWind.getLocalRetrievalService().isAvailable()) {
			return;
		}

		RetrieverFactory retrieverFactory = (RetrieverFactory) getValue(AVKey.RETRIEVER_FACTORY_LOCAL);
		if (retrieverFactory == null) {
			return;
		}

		AVListImpl avList = new AVListImpl();
		avList.setValue(AVKey.SECTOR, tile.getSector());
		avList.setValue(AVKey.WIDTH, tile.getWidth());
		avList.setValue(AVKey.HEIGHT, tile.getHeight());
		avList.setValue(AVKey.FILE_NAME, tile.getPath());

		Retriever retriever = retrieverFactory.createRetriever(avList, postProcessor);

		WorldWind.getLocalRetrievalService().runRetriever(retriever, tile.getPriority());
	}

	protected void retrieveRemoteTexture(MyTextureTile tile, DownloadPostProcessor downloadPostProcessor) {
		if (!isNetworkRetrievalEnabled()) {
			getLevels().markResourceAbsent(tile);
			return;
		}

		if (!WorldWind.getRetrievalService().isAvailable()) {
			return;
		}

		java.net.URL url;
		try {
			url = tile.getResourceURL();
			if (url == null) {
				return;
			}

			if (WorldWind.getNetworkStatus().isHostUnavailable(url)) {
				getLevels().markResourceAbsent(tile);
				return;
			}
		} catch (java.net.MalformedURLException e) {
			Logging.logger().log(java.util.logging.Level.SEVERE,
					Logging.getMessage("layers.TextureLayer.ExceptionCreatingTextureUrl", tile), e);
			return;
		}

		Retriever retriever;

		if (downloadPostProcessor == null) {
			downloadPostProcessor = createDownloadPostProcessor(tile);
		}
		retriever = URLRetriever.createRetriever(url, downloadPostProcessor);
		if (retriever == null) {
			Logging.logger().severe(Logging.getMessage("layers.TextureLayer.UnknownRetrievalProtocol", url.toString()));
			return;
		}
		retriever.setValue(URLRetriever.EXTRACT_ZIP_ENTRY, "true"); // supports
		// legacy
		// layers

		// Apply any overridden timeouts.
		Integer cto = AVListImpl.getIntegerValue(this, AVKey.URL_CONNECT_TIMEOUT);
		if (cto != null && cto > 0) {
			retriever.setConnectTimeout(cto);
		}
		Integer cro = AVListImpl.getIntegerValue(this, AVKey.URL_READ_TIMEOUT);
		if (cro != null && cro > 0) {
			retriever.setReadTimeout(cro);
		}
		Integer srl = AVListImpl.getIntegerValue(this, AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT);
		if (srl != null && srl > 0) {
			retriever.setStaleRequestLimit(srl);
		}

		WorldWind.getRetrievalService().runRetriever(retriever, tile.getPriority());
	}

	protected DownloadPostProcessor createDownloadPostProcessor(MyTextureTile tile) {
		return new DownloadPostProcessor(tile, this);
	}

	protected static class DownloadPostProcessor extends AbstractRetrievalPostProcessor {
		protected final MyTextureTile tile;
		protected final MyBasicTiledImageLayer layer;
		protected final FileStore fileStore;

		public DownloadPostProcessor(MyTextureTile tile, MyBasicTiledImageLayer layer) {
			this(tile, layer, null);
		}

		public DownloadPostProcessor(MyTextureTile tile, MyBasicTiledImageLayer layer, FileStore fileStore) {
			// noinspection RedundantCast
			super(layer);

			this.tile = tile;
			this.layer = layer;
			this.fileStore = fileStore;
		}

		protected FileStore getFileStore() {
			return fileStore != null ? fileStore : layer.getDataFileStore();
		}

		@Override
		protected void markResourceAbsent() {
			layer.getLevels().markResourceAbsent(tile);
		}

		@Override
		protected Object getFileLock() {
			return layer.fileLock;
		}

		@Override
		protected File doGetOutputFile() {
			return getFileStore().newFile(tile.getPath());
		}

		@Override
		protected ByteBuffer handleSuccessfulRetrieval() {
			ByteBuffer buffer = super.handleSuccessfulRetrieval();

			if (buffer != null) {
				// We've successfully cached data. Check if there's a
				// configuration file for this layer, create one
				// if there's not.
				layer.writeConfigurationFile(getFileStore());

				// Fire a property change to denote that the layer's backing
				// data has changed.
				layer.firePropertyChange(AVKey.LAYER, null, this);
			}

			return buffer;
		}

		@Override
		protected ByteBuffer handleTextContent() throws IOException {
			markResourceAbsent();

			return super.handleTextContent();
		}
	}

	// **************************************************************//
	// ******************** Non-Tile Resource Retrieval ***********//
	// **************************************************************//

	/**
	 * Retrieves any non-tile resources associated with this Layer, either online or in the local filesystem, and
	 * initializes properties of this Layer using those resources. This returns a key indicating the retrieval state:
	 * {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_SUCCESSFUL} indicates the retrieval succeeded,
	 * {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_ERROR} indicates the retrieval failed with errors, and
	 * <code>null</code> indicates the retrieval state is unknown. This method may invoke blocking I/O operations, and
	 * therefore should not be executed from the rendering thread.
	 *
	 * @return {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_SUCCESSFUL} if the retrieval succeeded,
	 *         {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_ERROR} if the retrieval failed with errors, and
	 *         <code>null</code> if the retrieval state is unknown.
	 */
	protected String retrieveResources() {
		// This Layer has no construction parameters, so there is no description
		// of what to retrieve. Return a key
		// indicating the resources have been successfully retrieved, though
		// there is nothing to retrieve.
		AVList params = (AVList) getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (params == null) {
			String message = Logging.getMessage("nullValue.ConstructionParametersIsNull");
			Logging.logger().warning(message);
			return AVKey.RETRIEVAL_STATE_SUCCESSFUL;
		}

		// This Layer has no OGC Capabilities URL in its construction
		// parameters. Return a key indicating the resources
		// have been successfully retrieved, though there is nothing to
		// retrieve.
		URL url = DataConfigurationUtils.getOGCGetCapabilitiesURL(params);
		if (url == null) {
			String message = Logging.getMessage("nullValue.CapabilitiesURLIsNull");
			Logging.logger().warning(message);
			return AVKey.RETRIEVAL_STATE_SUCCESSFUL;
		}

		// The OGC Capabilities resource is marked as absent. Return null
		// indicating that the retrieval was not
		// successful, and we should try again later.
		if (absentResources.isResourceAbsent(RESOURCE_ID_OGC_CAPABILITIES)) {
			return null;
		}

		// Get the service's OGC Capabilities resource from the session cache,
		// or initiate a retrieval to fetch it in
		// a separate thread.
		// SessionCacheUtils.getOrRetrieveSessionCapabilities() returns null if
		// it initiated a
		// retrieval, or if the OGC Capabilities URL is unavailable.
		//
		// Note that we use the URL's String representation as the cache key. We
		// cannot use the URL itself, because
		// the cache invokes the methods Object.hashCode() and Object.equals()
		// on the cache key. URL's implementations
		// of hashCode() and equals() perform blocking IO calls. World Wind does
		// not perform blocking calls during
		// rendering, and this method is likely to be called from the rendering
		// thread.
		WMSCapabilities caps;
		if (isNetworkRetrievalEnabled()) {
			caps = SessionCacheUtils.getOrRetrieveSessionCapabilities(url, WorldWind.getSessionCache(), url.toString(),
					absentResources, RESOURCE_ID_OGC_CAPABILITIES, null, null);
		} else {
			caps = SessionCacheUtils.getSessionCapabilities(WorldWind.getSessionCache(), url.toString(),
					url.toString());
		}

		// The OGC Capabilities resource retrieval is either currently running
		// in another thread, or has failed. In
		// either case, return null indicating that that the retrieval was not
		// successful, and we should try again
		// later.
		if (caps == null) {
			return null;
		}

		// We have sucessfully retrieved this Layer's OGC Capabilities resource.
		// Intialize this Layer using the
		// Capabilities document, and return a key indicating the retrieval has
		// succeeded.
		initFromOGCCapabilitiesResource(caps, params);

		return AVKey.RETRIEVAL_STATE_SUCCESSFUL;
	}

	/**
	 * Initializes this Layer's expiry time property from the specified WMS Capabilities document and parameter list
	 * describing the WMS layer names associated with this Layer. This method is thread safe; it synchronizes changes to
	 * this Layer by wrapping the appropriate method calls in {@link SwingUtilities#invokeLater(Runnable)}.
	 *
	 * @param caps the WMS Capabilities document retrieved from this Layer's WMS server.
	 * @param params the parameter list describing the WMS layer names associated with this Layer.
	 *
	 * @throws IllegalArgumentException if either the Capabilities or the parameter list is null.
	 */
	protected void initFromOGCCapabilitiesResource(WMSCapabilities caps, AVList params) {
		if (caps == null) {
			String message = Logging.getMessage("nullValue.CapabilitiesIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			String message = Logging.getMessage("nullValue.ParametersIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		String[] names = DataConfigurationUtils.getOGCLayerNames(params);
		if (names == null || names.length == 0) {
			return;
		}

		final Long expiryTime = caps.getLayerLatestLastUpdateTime(names);
		if (expiryTime == null) {
			return;
		}

		// Synchronize changes to this Layer with the Event Dispatch Thread.
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MyBasicTiledImageLayer.this.setExpiryTime(expiryTime);
				MyBasicTiledImageLayer.this.firePropertyChange(AVKey.LAYER, null, MyBasicTiledImageLayer.this);
			}
		});
	}

	/**
	 * Returns a boolean value indicating if this Layer should retrieve any non-tile resources, either online or in the
	 * local filesystem, and initialize itself using those resources.
	 *
	 * @return <code>true</code> if this Layer should retrieve any non-tile resources, and <code>false</code> otherwise.
	 */
	protected boolean isRetrieveResources() {
		AVList params = (AVList) getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (params == null) {
			return false;
		}

		Boolean b = (Boolean) params.getValue(AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE);
		return b != null && b;
	}

	/**
	 * Starts retrieving non-tile resources associated with this Layer in a non-rendering thread. By default, this
	 * schedules a task immediately to retrieve those resources, and then every 10 seconds thereafter until the
	 * retrieval succeeds.
	 * <p/>
	 * If this method is invoked while any non-tile resource tasks are running or pending, this cancels any pending
	 * tasks (but allows any running tasks to finish).
	 */
	protected void startResourceRetrieval() {
		// Configure an AbsentResourceList with the specified number of max
		// retrieval attempts, and the smallest
		// possible min attempt interval. We specify a small attempt interval
		// because the resource retrieval service
		// itself schedules the attempts at our specified interval. We therefore
		// want to bypass AbsentResourceLists's
		// internal timing scheme.
		absentResources = new AbsentResourceList(DEFAULT_MAX_RESOURCE_ATTEMPTS, 1);

		// Stop any pending resource retrieval tasks.
		if (resourceRetrievalService != null) {
			resourceRetrievalService.shutdown();
		}

		// Schedule a task to retrieve non-tile resources immediately, then at
		// intervals thereafter.
		Runnable task = createResourceRetrievalTask();
		String taskName = Logging.getMessage("layers.TiledImageLayer.ResourceRetrieverThreadName", getName());
		resourceRetrievalService = DataConfigurationUtils.createResourceRetrievalService(taskName);
		resourceRetrievalService.scheduleAtFixedRate(task, 0, DEFAULT_MIN_RESOURCE_CHECK_INTERVAL,
				TimeUnit.MILLISECONDS);
	}

	/**
	 * Cancels any pending non-tile resource retrieval tasks, and allows any running tasks to finish.
	 */
	protected void stopResourceRetrieval() {
		if (resourceRetrievalService != null) {
			resourceRetrievalService.shutdownNow();
			resourceRetrievalService = null;
		}
	}

	/**
	 * Returns a Runnable task which retrieves any non-tile resources associated with a specified Layer in it's run
	 * method. This task is used by the Layer to schedule periodic resource checks. If the task's run method throws an
	 * Exception, it will no longer be scheduled for execution. By default, this returns a reference to a new
	 * {@link ResourceRetrievalTask}.
	 *
	 * @return Runnable who's run method retrieves non-tile resources.
	 */
	protected Runnable createResourceRetrievalTask() {
		return new ResourceRetrievalTask(this);
	}

	/**
	 * ResourceRetrievalTask retrieves any non-tile resources associated with this Layer in it's run method.
	 */
	protected static class ResourceRetrievalTask implements Runnable {
		protected MyBasicTiledImageLayer layer;

		/**
		 * Constructs a new ResourceRetrievalTask, but otherwise does nothing.
		 *
		 * @param layer the BasicTiledImageLayer who's non-tile resources should be retrieved in the run method.
		 *
		 * @throws IllegalArgumentException if the layer is null.
		 */
		public ResourceRetrievalTask(MyBasicTiledImageLayer layer) {
			if (layer == null) {
				String message = Logging.getMessage("nullValue.LayerIsNull");
				Logging.logger().severe(message);
				throw new IllegalArgumentException(message);
			}

			this.layer = layer;
		}

		/**
		 * Returns the layer who's non-tile resources are retrieved by this ResourceRetrievalTask
		 *
		 * @return the layer who's non-tile resources are retireved.
		 */
		public MyBasicTiledImageLayer getLayer() {
			return layer;
		}

		/**
		 * Retrieves any non-tile resources associated with the specified Layer, and cancels any pending retrieval tasks
		 * if the retrieval succeeds, or if an exception is thrown during retrieval.
		 */
		@Override
		public void run() {
			try {
				if (layer.isEnabled()) {
					retrieveResources();
				}
			} catch (Throwable t) {
				handleUncaughtException(t);
			}
		}

		/**
		 * Invokes {@link BasicTiledImageLayer#retrieveResources()}, and cancels any pending retrieval tasks if the call
		 * returns {@link gov.nasa.worldwind.avlist.AVKey#RETRIEVAL_STATE_SUCCESSFUL}.
		 */
		protected void retrieveResources() {
			String state = layer.retrieveResources();

			if (state != null && state.equals(AVKey.RETRIEVAL_STATE_SUCCESSFUL)) {
				layer.stopResourceRetrieval();
			}
		}

		/**
		 * Logs a message describing the uncaught exception thrown during a call to run, and cancels any pending
		 * retrieval tasks.
		 *
		 * @param t the uncaught exception.
		 */
		protected void handleUncaughtException(Throwable t) {
			String message = Logging.getMessage("layers.TiledImageLayer.ExceptionRetrievingResources", layer.getName());
			Logging.logger().log(java.util.logging.Level.FINE, message, t);

			layer.stopResourceRetrieval();
		}
	}

	// **************************************************************//
	// ******************** Configuration *************************//
	// **************************************************************//

	protected void writeConfigurationFile(FileStore fileStore) {
		// TODO: configurable max attempts for creating a configuration file.

		try {
			AVList configParams = getConfigurationParams(null);
			writeConfigurationParams(fileStore, configParams);
		} catch (Exception e) {
			String message = Logging.getMessage("generic.ExceptionAttemptingToWriteConfigurationFile");
			Logging.logger().log(java.util.logging.Level.SEVERE, message, e);
		}
	}

	protected void writeConfigurationParams(FileStore fileStore, AVList params) {
		// Determine what the configuration file name should be based on the
		// configuration parameters. Assume an XML
		// configuration document type, and append the XML file suffix.
		String fileName = DataConfigurationUtils.getDataConfigFilename(params, ".xml");
		if (fileName == null) {
			String message = Logging.getMessage("nullValue.FilePathIsNull");
			Logging.logger().severe(message);
			throw new WWRuntimeException(message);
		}

		// Check if this component needs to write a configuration file. This
		// happens outside of the synchronized block
		// to improve multithreaded performance for the common case: the
		// configuration file already exists, this just
		// need to check that it's there and return. If the file exists but is
		// expired, do not remove it - this
		// removes the file inside the synchronized block below.
		if (!needsConfigurationFile(fileStore, fileName, params, false)) {
			return;
		}

		synchronized (fileLock) {
			// Check again if the component needs to write a configuration file,
			// potentially removing any existing file
			// which has expired. This additional check is necessary because the
			// file could have been created by
			// another thread while we were waiting for the lock.
			if (!needsConfigurationFile(fileStore, fileName, params, true)) {
				return;
			}

			doWriteConfigurationParams(fileStore, fileName, params);
		}
	}

	protected void doWriteConfigurationParams(FileStore fileStore, String fileName, AVList params) {
		java.io.File file = fileStore.newFile(fileName);
		if (file == null) {
			String message = Logging.getMessage("generic.CannotCreateFile", fileName);
			Logging.logger().severe(message);
			throw new WWRuntimeException(message);
		}

		Document doc = createConfigurationDocument(params);
		WWXML.saveDocumentToFile(doc, file.getPath());

		String message = Logging.getMessage("generic.ConfigurationFileCreated", fileName);
		Logging.logger().fine(message);
	}

	protected boolean needsConfigurationFile(FileStore fileStore, String fileName, AVList params,
			boolean removeIfExpired) {
		long expiryTime = getExpiryTime();
		if (expiryTime <= 0) {
			expiryTime = AVListImpl.getLongValue(params, AVKey.EXPIRY_TIME, 0L);
		}

		return !DataConfigurationUtils.hasDataConfigFile(fileStore, fileName, removeIfExpired, expiryTime);
	}

	protected AVList getConfigurationParams(AVList params) {
		if (params == null) {
			params = new AVListImpl();
		}

		// Gather all the construction parameters if they are available.
		AVList constructionParams = (AVList) getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (constructionParams != null) {
			params.setValues(constructionParams);
		}

		// Gather any missing LevelSet parameters from the LevelSet itself.
		DataConfigurationUtils.getLevelSetConfigParams(getLevels(), params);

		return params;
	}

	protected Document createConfigurationDocument(AVList params) {
		return createTiledImageLayerConfigDocument(params);
	}

	// **************************************************************//
	// ******************** Restorable Support ********************//
	// **************************************************************//

	@Override
	public String getRestorableState() {
		// We only create a restorable state XML if this elevation model was
		// constructed with an AVList.
		AVList constructionParams = (AVList) getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (constructionParams == null) {
			return null;
		}

		RestorableSupport rs = RestorableSupport.newRestorableSupport();
		// Creating a new RestorableSupport failed. RestorableSupport logged the
		// problem, so just return null.
		if (rs == null) {
			return null;
		}

		doGetRestorableState(rs, null);
		return rs.getStateAsXml();
	}

	@Override
	public void restoreState(String stateInXml) {
		String message = Logging.getMessage("RestorableSupport.RestoreRequiresConstructor");
		Logging.logger().severe(message);
		throw new UnsupportedOperationException(message);
	}

	protected void doGetRestorableState(RestorableSupport rs, RestorableSupport.StateObject context) {
		AVList constructionParams = (AVList) getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (constructionParams != null) {
			for (Map.Entry<String, Object> avp : constructionParams.getEntries()) {
				getRestorableStateForAVPair(avp.getKey(), avp.getValue(), rs, context);
			}
		}

		rs.addStateValueAsBoolean(context, "Layer.Enabled", isEnabled());
		rs.addStateValueAsDouble(context, "Layer.Opacity", getOpacity());
		rs.addStateValueAsDouble(context, "Layer.MinActiveAltitude", getMinActiveAltitude());
		rs.addStateValueAsDouble(context, "Layer.MaxActiveAltitude", getMaxActiveAltitude());
		rs.addStateValueAsBoolean(context, "Layer.NetworkRetrievalEnabled", isNetworkRetrievalEnabled());
		rs.addStateValueAsString(context, "Layer.Name", getName());
		rs.addStateValueAsBoolean(context, "TiledImageLayer.UseMipMaps", isUseMipMaps());
		rs.addStateValueAsBoolean(context, "TiledImageLayer.UseTransparentTextures", isUseTransparentTextures());

		RestorableSupport.StateObject so = rs.addStateObject(context, "avlist");
		for (Map.Entry<String, Object> avp : getEntries()) {
			getRestorableStateForAVPair(avp.getKey(), avp.getValue(), rs, so);
		}
	}

	@Override
	public void getRestorableStateForAVPair(String key, Object value, RestorableSupport rs,
			RestorableSupport.StateObject context) {
		if (value == null) {
			return;
		}

		if (key.equals(AVKey.CONSTRUCTION_PARAMETERS)) {
			return;
		}

		if (value instanceof LatLon) {
			rs.addStateValueAsLatLon(context, key, (LatLon) value);
		} else if (value instanceof Sector) {
			rs.addStateValueAsSector(context, key, (Sector) value);
		} else if (value instanceof Color) {
			rs.addStateValueAsColor(context, key, (Color) value);
		} else {
			super.getRestorableStateForAVPair(key, value, rs, context);
		}
	}

	protected void doRestoreState(RestorableSupport rs, RestorableSupport.StateObject context) {
		Boolean b = rs.getStateValueAsBoolean(context, "Layer.Enabled");
		if (b != null) {
			setEnabled(b);
		}

		Double d = rs.getStateValueAsDouble(context, "Layer.Opacity");
		if (d != null) {
			setOpacity(d);
		}

		d = rs.getStateValueAsDouble(context, "Layer.MinActiveAltitude");
		if (d != null) {
			setMinActiveAltitude(d);
		}

		d = rs.getStateValueAsDouble(context, "Layer.MaxActiveAltitude");
		if (d != null) {
			setMaxActiveAltitude(d);
		}

		b = rs.getStateValueAsBoolean(context, "Layer.NetworkRetrievalEnabled");
		if (b != null) {
			setNetworkRetrievalEnabled(b);
		}

		String s = rs.getStateValueAsString(context, "Layer.Name");
		if (s != null) {
			setName(s);
		}

		b = rs.getStateValueAsBoolean(context, "TiledImageLayer.UseMipMaps");
		if (b != null) {
			setUseMipMaps(b);
		}

		b = rs.getStateValueAsBoolean(context, "TiledImageLayer.UseTransparentTextures");
		if (b != null) {
			setUseTransparentTextures(b);
		}

		RestorableSupport.StateObject so = rs.getStateObject(context, "avlist");
		if (so != null) {
			RestorableSupport.StateObject[] avpairs = rs.getAllStateObjects(so, "");
			if (avpairs != null) {
				for (RestorableSupport.StateObject avp : avpairs) {
					if (avp != null) {
						doRestoreStateForObject(rs, avp);
					}
				}
			}
		}
	}

	protected void doRestoreStateForObject(RestorableSupport rs, RestorableSupport.StateObject so) {
		if (so == null) {
			return;
		}

		setValue(so.getName(), so.getValue());
	}

	protected static AVList restorableStateToParams(String stateInXml) {
		if (stateInXml == null) {
			String message = Logging.getMessage("nullValue.StringIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		RestorableSupport rs;
		try {
			rs = RestorableSupport.parse(stateInXml);
		} catch (Exception e) {
			// Parsing the document specified by stateInXml failed.
			String message = Logging.getMessage("generic.ExceptionAttemptingToParseStateXml", stateInXml);
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message, e);
		}

		AVList params = new AVListImpl();
		restoreStateForParams(rs, null, params);
		return params;
	}

	protected static void restoreStateForParams(RestorableSupport rs, RestorableSupport.StateObject context,
			AVList params) {
		String s = rs.getStateValueAsString(context, AVKey.DATA_CACHE_NAME);
		if (s != null) {
			params.setValue(AVKey.DATA_CACHE_NAME, s);
		}

		s = rs.getStateValueAsString(context, AVKey.SERVICE);
		if (s != null) {
			params.setValue(AVKey.SERVICE, s);
		}

		s = rs.getStateValueAsString(context, AVKey.DATASET_NAME);
		if (s != null) {
			params.setValue(AVKey.DATASET_NAME, s);
		}

		s = rs.getStateValueAsString(context, AVKey.FORMAT_SUFFIX);
		if (s != null) {
			params.setValue(AVKey.FORMAT_SUFFIX, s);
		}

		Integer i = rs.getStateValueAsInteger(context, AVKey.NUM_EMPTY_LEVELS);
		if (i != null) {
			params.setValue(AVKey.NUM_EMPTY_LEVELS, i);
		}

		i = rs.getStateValueAsInteger(context, AVKey.NUM_LEVELS);
		if (i != null) {
			params.setValue(AVKey.NUM_LEVELS, i);
		}

		i = rs.getStateValueAsInteger(context, AVKey.TILE_WIDTH);
		if (i != null) {
			params.setValue(AVKey.TILE_WIDTH, i);
		}

		i = rs.getStateValueAsInteger(context, AVKey.TILE_HEIGHT);
		if (i != null) {
			params.setValue(AVKey.TILE_HEIGHT, i);
		}

		Long lo = rs.getStateValueAsLong(context, AVKey.EXPIRY_TIME);
		if (lo != null) {
			params.setValue(AVKey.EXPIRY_TIME, lo);
		}

		LatLon ll = rs.getStateValueAsLatLon(context, AVKey.LEVEL_ZERO_TILE_DELTA);
		if (ll != null) {
			params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, ll);
		}

		ll = rs.getStateValueAsLatLon(context, AVKey.TILE_ORIGIN);
		if (ll != null) {
			params.setValue(AVKey.TILE_ORIGIN, ll);
		}

		Sector sector = rs.getStateValueAsSector(context, AVKey.SECTOR);
		if (sector != null) {
			params.setValue(AVKey.SECTOR, sector);
		}
	}

	@Override
	public Sector getSector() {
		LevelSet ls = getLevels();
		if (ls != null) {
			return ls.getSector();
		}
		return null;
	}

	@Override
	public Map<File, Boolean> getFiles() {
		Map<File, Boolean> files = new HashMap<File, Boolean>(2);
		files.put(inputFile, true);
		if (binFolder != null) {
			files.put(binFolder, true);
		}
		return files;
	}

	@Override
	public File getInputFile() {
		return inputFile;
	}

	@Override
	public boolean isInitialized() {

		return true;
	}

	@Override
	public void initialize() {

	}

	// **************************************************************//
	// ******************** Resources *****************************//
	// **************************************************************//

	@Override
	protected void checkResources() {
		AVList params = (AVList) getValue(AVKey.CONSTRUCTION_PARAMETERS);
		if (params == null) {
			return;
		}

		initializeResources(params);
	}

	protected void initializeResources(AVList params) {
		Boolean b = (Boolean) params.getValue(AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE);
		if (b != null && b) {
			initPropertiesFromService(params);
		}
	}

	protected void initPropertiesFromService(AVList params) {
		if (serviceInitialized) {
			return;
		}

		URL url = DataConfigurationUtils.getOGCGetCapabilitiesURL(params);
		if (url == null) {
			return;
		}

		if (absentResources.isResourceAbsent(SERVICE_CAPABILITIES_RESOURCE_ID)) {
			return;
		}

		WMSCapabilities caps = SessionCacheUtils.getOrRetrieveSessionCapabilities(url, WorldWind.getSessionCache(), url,
				absentResources, SERVICE_CAPABILITIES_RESOURCE_ID, this, AVKey.LAYER);

		if (caps == null) {
			return;
		}

		initPropertiesFromCapabilities(caps, params);
		serviceInitialized = true;
	}

	protected void initPropertiesFromCapabilities(WMSCapabilities caps, AVList params) {
		String[] names = DataConfigurationUtils.getOGCLayerNames(params);
		if (names == null || names.length == 0) {
			return;
		}

		Long expiryTime = caps.getLayerLatestLastUpdateTime(names);
		if (expiryTime != null) {
			setExpiryTime(expiryTime);
		}
	}
}
