/*
Copyright (C) 2001, 2006 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
 */
package fr.ifremer.viewer3d.layers.deprecated.dtm;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Observer;
import java.util.concurrent.PriorityBlockingQueue;

import javax.imageio.ImageIO;
import javax.xml.xpath.XPath;

import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GL2GL3;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.utils.perfcounter.PerfoCounter;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.ITiledRasterCacheProducer;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import fr.ifremer.viewer3d.util.IFiles;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.GpuResourceCache;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Box;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.TextRenderer;
import gov.nasa.worldwind.retrieve.AbstractRetrievalPostProcessor;
import gov.nasa.worldwind.retrieve.HTTPRetriever;
import gov.nasa.worldwind.retrieve.Retriever;
import gov.nasa.worldwind.retrieve.RetrieverFactory;
import gov.nasa.worldwind.retrieve.URLRetriever;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.OGLTextRenderer;
import gov.nasa.worldwind.util.PerformanceStatistic;
import gov.nasa.worldwind.util.Tile;
import gov.nasa.worldwind.util.TileKey;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * @author dom
 */
@Deprecated(forRemoval = true)
public abstract class MyTiledImageLayer extends MyAbstractLayer implements ITarget, IFiles, IInfos, IDeletable {

	protected final static Logger logger = LoggerFactory.getLogger(MyTiledImageLayer.class);

	// Infrastructure
	// Infrastructure
	protected static final LevelComparer levelComparer = new LevelComparer();
	protected LevelSet levels;
	protected ArrayList<MyTextureTile> topLevels;
	protected boolean forceLevelZeroLoads = false;
	protected boolean levelZeroLoaded = false;
	protected boolean retainLevelZeroTiles = false;
	protected String tileCountName;
	protected double detailHintOrigin = 2.8;
	protected double detailHint = 0;
	protected boolean useMipMaps = true;
	protected boolean useTransparentTextures = false;
	protected ArrayList<String> supportedImageFormats = new ArrayList<String>();
	protected String textureFormat;
	protected boolean isTextureCacheObsolete;
	protected ITiledRasterCacheProducer cachedTilesProducer;

	public static String XML_KEY_UNIT = "Unit";
	public static String XML_KEY_GlobalMinValue = "GlobalMin";
	public static String XML_KEY_GlobalMaxValue = "GlobalMax";
	public static String XML_KEY_MinValue = "ValMin";
	public static String XML_KEY_MaxValue = "ValMax";

	/**
	 * The infos displayed by the popupmenu action Infos.
	 */
	protected AbstractInfos infos;

	/**
	 * Directory for binary data files.
	 */
	protected File binFolder;

	// Diagnostic flags
	protected boolean drawTileBoundaries = false;
	protected boolean drawTileIDs = false;
	protected boolean drawBoundingVolumes = false;

	// Stuff computed each frame
	protected ArrayList<MyTextureTile> currentTiles = new ArrayList<MyTextureTile>();
	protected MyTextureTile currentResourceTile;
	protected boolean atMaxResolution = false;
	protected PriorityBlockingQueue<Runnable> requestQ = new PriorityBlockingQueue<Runnable>(200);
	private double detailFactorForMovable = 1.0;

	// Listeners
	protected Observer preferencesObserver;

	abstract protected void requestTexture(DrawContext dc, MyTextureTile tile);

	abstract protected void forceTextureLoad(MyTextureTile tile);

	abstract protected void checkResources();

	public MyTiledImageLayer(MyTiledImageLayer parent) {
		super(parent);
		levels = parent.levels;
		topLevels = parent.topLevels;
		forceLevelZeroLoads = parent.forceLevelZeroLoads;
		levelZeroLoaded = parent.levelZeroLoaded;
		retainLevelZeroTiles = parent.retainLevelZeroTiles;
		tileCountName = parent.tileCountName;
		detailHintOrigin = parent.detailHintOrigin;
		detailHint = parent.detailHint;
		useMipMaps = parent.useMipMaps;
		useTransparentTextures = parent.useTransparentTextures;
		supportedImageFormats = parent.supportedImageFormats;
		textureFormat = parent.textureFormat;
		isClearTextureCache = true;
		registerListeners();
	}

	public MyTiledImageLayer(LevelSet levelSet) {
		super();
		if (levelSet == null) {
			String message = Logging.getMessage("nullValue.LevelSetIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		levels = new LevelSet(levelSet); // the caller's levelSet may
		// change internally, so we copy
		// it.
		infos = new AbstractInfos();
		setPickEnabled(false); // textures are assumed to be terrain unless
		// specifically indicated otherwise.
		tileCountName = getName() + " Tiles";
		isClearTextureCache = true;

		registerListeners();
	}

	/**
	 * Register all listeners.
	 */
	private void registerListeners() {

		// Listener on Viewer3D preferences
		preferencesObserver = (observable, value) -> {
			isSynchronousRedrawOnCacheRefresh = ((BooleanPreferenceAttribute) observable).getValue();
		};

		// Listen to "SynchronousRedrawAfterCacheRefresh" pref
		BooleanPreferenceAttribute isSynchronousRedraw = Activator.getPluginParameters()
				.getSynchronousRedrawAfterCacheRefresh();
		isSynchronousRedraw.addObserver(preferencesObserver);

		// Initialize values that depends on the listener
		isSynchronousRedrawOnCacheRefresh = isSynchronousRedraw.getValue();
	}

	/**
	 * Unregister all listeners.
	 */
	private void unregisterListeners() {
		BooleanPreferenceAttribute result = Activator.getPluginParameters().getSynchronousRedrawAfterCacheRefresh();
		result.deleteObserver(preferencesObserver);
	}

	@Override
	public Object setValue(String key, Object value) {
		Object out = super.setValue(key, value);

		// Offer it to the level set
		if (getLevels() != null && !PROPERTY_CHANGE_SUPPORT.equals(key)) {
			getLevels().setValue(key, value);
		}

		return out;
	}

	@Override
	public Object getValue(String key) {
		Object value = super.getValue(key);

		// see if the level set has it
		return value != null || PROPERTY_CHANGE_SUPPORT.equals(key) ? value : getLevels().getValue(key);
	}

	@Override
	public void setName(String name) {
		super.setName(name);
		tileCountName = getName() + " Tiles";
	}

	public boolean isForceLevelZeroLoads() {
		return forceLevelZeroLoads;
	}

	public void setForceLevelZeroLoads(boolean forceLevelZeroLoads) {
		this.forceLevelZeroLoads = forceLevelZeroLoads;
	}

	public boolean isRetainLevelZeroTiles() {
		return retainLevelZeroTiles;
	}

	public void setRetainLevelZeroTiles(boolean retainLevelZeroTiles) {
		this.retainLevelZeroTiles = retainLevelZeroTiles;
	}

	public boolean isDrawTileIDs() {
		return drawTileIDs;
	}

	public void setDrawTileIDs(boolean drawTileIDs) {
		this.drawTileIDs = drawTileIDs;
	}

	public boolean isDrawTileBoundaries() {
		return drawTileBoundaries;
	}

	public void setDrawTileBoundaries(boolean drawTileBoundaries) {
		this.drawTileBoundaries = drawTileBoundaries;
	}

	public boolean isDrawBoundingVolumes() {
		return drawBoundingVolumes;
	}

	public void setDrawBoundingVolumes(boolean drawBoundingVolumes) {
		this.drawBoundingVolumes = drawBoundingVolumes;
	}

	/**
	 * Indicates the layer's detail hint, which is described in {@link #setDetailHint(double)}.
	 *
	 * @return the detail hint
	 *
	 * @see #setDetailHint(double)
	 */
	public double getDetailHint() {
		return detailHint;
	}

	/**
	 * Modifies the default relationship of image resolution to screen resolution as the viewing altitude changes.
	 * Values greater than 0 cause imagery to appear at higher resolution at greater altitudes than normal, but at an
	 * increased performance cost. Values less than 0 decrease the default resolution at any given altitude. The default
	 * value is 0. Values typically range between -0.5 and 0.5.
	 * <p/>
	 * Note: The resolution-to-height relationship is defined by a scale factor that specifies the approximate size of
	 * discernable lengths in the image relative to eye distance. The scale is specified as a power of 10. A value of 3,
	 * for example, specifies that 1 meter on the surface should be distinguishable from an altitude of 10^3 meters
	 * (1000 meters). The default scale is 1/10^2.8, (1 over 10 raised to the power 2.8). The detail hint specifies
	 * deviations from that default. A detail hint of 0.2 specifies a scale of 1/1000, i.e., 1/10^(2.8 + .2) = 1/10^3.
	 * Scales much larger than 3 typically cause the applied resolution to be higher than discernable for the altitude.
	 * Such scales significantly decrease performance.
	 *
	 * @param detailHint the degree to modify the default relationship of image resolution to screen resolution with
	 *            changing view altitudes. Values greater than 1 increase the resolution. Values less than zero decrease
	 *            the resolution. The default value is 0.
	 */
	public void setDetailHint(double detailHint) {
		this.detailHint = detailHint;
	}

	public LevelSet getLevels() {
		return levels;
	}

	protected PriorityBlockingQueue<Runnable> getRequestQ() {
		return requestQ;
	}

	@Override
	public boolean isMultiResolution() {
		return getLevels() != null && getLevels().getNumLevels() > 1;
	}

	@Override
	public boolean isAtMaxResolution() {
		return atMaxResolution;
	}

	/**
	 * Returns the format used to store images in texture memory, or null if images are stored in their native format.
	 *
	 * @return the texture image format; null if images are stored in their native format.
	 *
	 * @see #setTextureFormat(String)
	 */
	public String getTextureFormat() {
		return textureFormat;
	}

	/**
	 * Specifies the format used to store images in texture memory, or null to store images in their native format.
	 * Suppported texture formats are as follows:
	 * <ul>
	 * <li><code>image/dds</code> - Stores images in the compressed DDS format. If the image is already in DDS format
	 * it's stored as-is.</li>
	 * </ul>
	 *
	 * @param textureFormat the texture image format; null to store images in their native format.
	 */
	public void setTextureFormat(String textureFormat) {
		this.textureFormat = textureFormat;
	}

	public boolean isUseMipMaps() {
		return useMipMaps;
	}

	public void setUseMipMaps(boolean useMipMaps) {
		this.useMipMaps = useMipMaps;
	}

	public boolean isUseTransparentTextures() {
		return useTransparentTextures;
	}

	public void setUseTransparentTextures(boolean useTransparentTextures) {
		this.useTransparentTextures = useTransparentTextures;
	}

	/**
	 * Specifies the time of the layer's most recent dataset update, beyond which cached data is invalid. If greater
	 * than zero, the layer ignores and eliminates any in-memory or on-disk cached data older than the time specified,
	 * and requests new information from the data source. If zero, the default, the layer applies any expiry times
	 * associated with its individual levels, but only for on-disk cached data. In-memory cached data is expired only
	 * when the expiry time is specified with this method and is greater than zero. This method also overwrites the
	 * expiry times of the layer's individual levels if the value specified to the method is greater than zero.
	 *
	 * @param expiryTime the expiry time of any cached data, expressed as a number of milliseconds beyond the epoch. The
	 *            default expiry time is zero.
	 *
	 * @see System#currentTimeMillis() for a description of milliseconds beyond the epoch.
	 */
	@Override
	public void setExpiryTime(long expiryTime) // Override this method to use
	// intrinsic level-specific expiry times
	{
		super.setExpiryTime(expiryTime);

		if (expiryTime > 0) {
			levels.setExpiryTime(expiryTime); // remove this in sub-class
			// to use level-specific expiry times
		}
	}

	public List<MyTextureTile> getTopLevels() {
		if (topLevels == null) {
			createTopLevelTiles();
		}

		return topLevels;
	}

	protected void createTopLevelTiles() {
		Sector sector = levels.getSector();

		Level level = levels.getFirstLevel();
		Angle dLat = level.getTileDelta().getLatitude();
		Angle dLon = level.getTileDelta().getLongitude();
		Angle latOrigin = levels.getTileOrigin().getLatitude();
		Angle lonOrigin = levels.getTileOrigin().getLongitude();

		// Determine the row and column offset from the common World Wind global
		// tiling origin.
		int firstRow = Tile.computeRow(dLat, sector.getMinLatitude(), latOrigin);
		int firstCol = Tile.computeColumn(dLon, sector.getMinLongitude(), lonOrigin);
		int lastRow = Tile.computeRow(dLat, sector.getMaxLatitude(), latOrigin);
		int lastCol = Tile.computeColumn(dLon, sector.getMaxLongitude(), lonOrigin);

		int nLatTiles = lastRow - firstRow + 1;
		int nLonTiles = lastCol - firstCol + 1;

		topLevels = new ArrayList<MyTextureTile>(nLatTiles * nLonTiles);

		Angle p1 = Tile.computeRowLatitude(firstRow, dLat, latOrigin);
		for (int row = firstRow; row <= lastRow; row++) {
			Angle p2;
			p2 = p1.add(dLat);

			Angle t1 = Tile.computeColumnLongitude(firstCol, dLon, lonOrigin);
			for (int col = firstCol; col <= lastCol; col++) {
				Angle t2;
				t2 = t1.add(dLon);

				topLevels.add(new MyTextureTile(GeoBoxUtils.normalizedDegrees(new Sector(p1, p2, t1, t2)), level, row,
						col, this));
				t1 = t2;
			}
			p1 = p2;
		}
	}

	protected void loadAllTopLevelTextures(DrawContext dc) {
		for (MyTextureTile tile : getTopLevels()) {
			if (!tile.isTextureInMemory(dc.getTextureCache())) {
				forceTextureLoad(tile);
			}
		}

		levelZeroLoaded = true;
	}

	// ============== Tile Assembly ======================= //
	// ============== Tile Assembly ======================= //
	// ============== Tile Assembly ======================= //

	protected void assembleTiles(DrawContext dc) {
		currentTiles.clear();

		for (MyTextureTile tile : getTopLevels()) {

			if (isTileVisible(dc, tile)) {
				currentResourceTile = null;
				addTileOrDescendants(dc, tile);
			}
		}
	}

	protected void addTileOrDescendants(DrawContext dc, MyTextureTile tile) {
		if (meetsRenderCriteria(dc, tile)) {
			addTile(dc, tile);
			return;
		}

		// The incoming tile does not meet the rendering criteria, so it must be
		// subdivided and those
		// subdivisions tested against the criteria.

		// All tiles that meet the selection criteria are drawn, but some of
		// those tiles will not have
		// textures associated with them either because their texture isn't
		// loaded yet or because they
		// are finer grain than the layer has textures for. In these cases the
		// tiles use the texture of
		// the closest ancestor that has a texture loaded. This ancestor is
		// called the currentResourceTile.
		// A texture transform is applied during rendering to align the sector's
		// texture coordinates with the
		// appropriate region of the ancestor's texture.

		MyTextureTile ancestorResource = null;

		try {
			// TODO: Revise this to reflect that the parent layer is only
			// requested while the algorithm continues
			// to search for the layer matching the criteria.
			// At this point the tile does not meet the render criteria but it
			// may have its texture in memory.
			// If so, register this tile as the resource tile. If not, then this
			// tile will be the next level
			// below a tile with texture in memory. So to provide progressive
			// resolution increase, add this tile
			// to the draw list. That will cause the tile to be drawn using its
			// parent tile's texture, and it will
			// cause it's texture to be requested. At some future call to this
			// method the tile's texture will be in
			// memory, it will not meet the render criteria, but will serve as
			// the parent to a tile that goes
			// through this same process as this method recurses. The result of
			// all this is that a tile isn't rendered
			// with its own texture unless all its parents have their textures
			// loaded. In addition to causing
			// progressive resolution increase, this ensures that the parents
			// are available as the user zooms out, and
			// therefore the layer remains visible until the user is zoomed out
			// to the point the layer is no longer
			// active.
			if (tile.isTextureInMemory(dc.getTextureCache()) || tile.getLevelNumber() == 0) {
				ancestorResource = currentResourceTile;
				currentResourceTile = tile;
			} else if (!tile.getLevel().isEmpty()) {
				// this.addTile(dc, tile);
				// return;

				// Issue a request for the parent before descending to the
				// children.
				// if (tile.getLevelNumber() < this.levels.getNumLevels())
				// {
				// // Request only tiles with data associated at this level
				// if (!this.levels.isResourceAbsent(tile))
				// this.requestTexture(dc, tile);
				// }
			}

			MyTextureTile[] subTiles = tile.createSubTiles(levels.getLevel(tile.getLevelNumber() + 1));
			var levelSector = getLevels().getSector();
			for (MyTextureTile child : subTiles) {
				boolean intersect = GeoBoxUtils.intersects(levelSector, child.getSector());
				boolean isVisible = isTileVisible(dc, child);
				if (intersect && isVisible) {
					addTileOrDescendants(dc, child);
				}
			}
		} finally {
			if (ancestorResource != null) {
				// currentResource ancestor
				currentResourceTile = ancestorResource;
			}
		}
	}

	protected void addTile(DrawContext dc, MyTextureTile tile) {
		tile.setFallbackTile(null);

		if (tile.isTextureInMemory(dc.getTextureCache())) {
			addTileToCurrent(tile);
			return;
		}

		// Level 0 loads may be forced
		if (tile.getLevelNumber() == 0 && forceLevelZeroLoads && !tile.isTextureInMemory(dc.getTextureCache())) {
			forceTextureLoad(tile);
			if (tile.isTextureInMemory(dc.getTextureCache())) {
				addTileToCurrent(tile);
				return;
			}
		}

		// Tile's texture isn't available, so request it
		if (tile.getLevelNumber() < levels.getNumLevels()) {
			// Request only tiles with data associated at this level
			if (!levels.isResourceAbsent(tile)) {
				requestTexture(dc, tile);
			}
		}

		// Set up to use the currentResource tile's texture
		if (currentResourceTile != null) {
			if (currentResourceTile.getLevelNumber() == 0 && forceLevelZeroLoads
					&& !currentResourceTile.isTextureInMemory(dc.getTextureCache())) {
				forceTextureLoad(currentResourceTile);
			}

			if (currentResourceTile.isTextureInMemory(dc.getTextureCache())) {
				tile.setFallbackTile(currentResourceTile);
				addTileToCurrent(tile);
			}
		}
	}

	protected void addTileToCurrent(MyTextureTile tile) {
		currentTiles.add(tile);
	}

	protected boolean isTileVisible(DrawContext dc, MyTextureTile tile) {
		Sector sector = null;

		if (isHighQualityDisplayOfTexture()) {
			// jma asks for better resolution
			// detail must be upgraded for better accuracy
			detailFactorForMovable = 1.8;
		} else {
			detailFactorForMovable = 1.0;
		}

		if (getMovement() != null) {
			// compute the moved sector of the tile to check if it intersect
			// the visible sector
			sector = this.move(tile.getSector());

			return dc.getVisibleSector() == null || dc.getVisibleSector().intersects(sector);
		} else {
			sector = tile.getSector();
			return tile.getExtent(dc).intersects(dc.getView().getFrustumInModelCoordinates())
					&& (dc.getVisibleSector() == null || dc.getVisibleSector().intersects(sector));
		}
		// return tile.getExtent(dc).intersects(
		// dc.getView().getFrustumInModelCoordinates())
		// && (dc.getVisibleSector() == null || dc.getVisibleSector()
		// .intersects(sector));
	}

	protected boolean meetsRenderCriteria(DrawContext dc, MyTextureTile tile) {
		return levels.isFinalLevel(tile.getLevelNumber()) || !needToSplit(dc, tile.getSector(), tile.getLevel());
	}

	protected double getDetailFactor() {
		return (detailHintOrigin + getDetailHint()) * detailFactorForMovable;
	}

	protected boolean needToSplit(DrawContext dc, Sector sector, Level level) {
		Vec4[] corners = sector.computeCornerPoints(dc.getGlobe(), dc.getVerticalExaggeration());
		Vec4 centerPoint = sector.computeCenterPoint(dc.getGlobe(), dc.getVerticalExaggeration());

		// Get the eye distance for each of the sector's corners and its center.
		View view = dc.getView();
		double d1 = view.getEyePoint().distanceTo3(corners[0]);
		double d2 = view.getEyePoint().distanceTo3(corners[1]);
		double d3 = view.getEyePoint().distanceTo3(corners[2]);
		double d4 = view.getEyePoint().distanceTo3(corners[3]);
		double d5 = view.getEyePoint().distanceTo3(centerPoint);

		// Find the minimum eye distance. Compute cell height at the
		// corresponding point.
		double minDistance = d1;
		double cellHeight = corners[0].getLength3() * level.getTexelSize(); // globe
		// radius
		// x
		// radian
		// texel
		// size
		double texelSize = level.getTexelSize();
		if (d2 < minDistance) {
			minDistance = d2;
			cellHeight = corners[1].getLength3() * texelSize;
		}
		if (d3 < minDistance) {
			minDistance = d3;
			cellHeight = corners[2].getLength3() * texelSize;
		}
		if (d4 < minDistance) {
			minDistance = d4;
			cellHeight = corners[3].getLength3() * texelSize;
		}
		if (d5 < minDistance) {
			minDistance = d5;
			cellHeight = centerPoint.getLength3() * texelSize;
		}

		// Split when the cell height (length of a texel) becomes greater than
		// the specified fraction of the eye
		// distance. The fraction is specified as a power of 10. For example, a
		// detail factor of 3 means split when the
		// cell height becomes more than one thousandth of the eye distance.
		// Another way to say it is, use the current
		// tile if its cell height is less than the specified fraction of the
		// eye distance.
		//
		// NOTE: It's tempting to instead compare a screen pixel size to the
		// texel size, but that calculation is
		// window-size dependent and results in selecting an excessive number of
		// tiles when the window is large.

		return cellHeight > minDistance * Math.pow(10, -getDetailFactor());
	}

	@Override
	public Double getMinEffectiveAltitude(Double radius) {
		if (radius == null) {
			radius = Earth.WGS84_EQUATORIAL_RADIUS;
		}

		// Get the cell size for the highest-resolution level.
		double texelSize = getLevels().getLastLevel().getTexelSize();
		double cellHeight = radius * texelSize;

		// Compute altitude associated with the cell height at which it would
		// switch if it had higher-res levels.
		return cellHeight * Math.pow(10, getDetailFactor());
	}

	@Override
	public Double getMaxEffectiveAltitude(Double radius) {
		if (radius == null) {
			radius = Earth.WGS84_EQUATORIAL_RADIUS;
		}

		// Find first non-empty level. Compute altitude at which it comes into
		// effect.
		for (int i = 0; i < getLevels().getLastLevel().getLevelNumber(); i++) {
			if (levels.isLevelEmpty(i)) {
				continue;
			}

			// Compute altitude associated with the cell height at which it
			// would switch if it had a lower-res level.
			// That cell height is twice that of the current lowest-res level.
			double texelSize = levels.getLevel(i).getTexelSize();
			double cellHeight = 2 * radius * texelSize;

			return cellHeight * Math.pow(10, getDetailFactor());
		}

		return null;
	}

	protected boolean atMaxLevel(DrawContext dc) {
		Position vpc = dc.getViewportCenterPosition();
		if (dc.getView() == null || getLevels() == null || vpc == null) {
			return false;
		}

		if (!getLevels().getSector().contains(vpc.getLatitude(), vpc.getLongitude())) {
			return true;
		}

		Level nextToLast = getLevels().getNextToLastLevel();
		if (nextToLast == null) {
			return true;
		}

		Sector centerSector = nextToLast.computeSectorForPosition(vpc.getLatitude(), vpc.getLongitude(),
				levels.getTileOrigin());

		return needToSplit(dc, centerSector, nextToLast);
	}

	// ============== Rendering ======================= //
	// ============== Rendering ======================= //
	// ============== Rendering ======================= //

	@Override
	public void render(DrawContext dc) {
		atMaxResolution = atMaxLevel(dc);
		super.render(dc);
	}

	@Override
	protected final void doRender(DrawContext dc) {
		if (forceLevelZeroLoads && !levelZeroLoaded) {
			loadAllTopLevelTextures(dc);
		}
		if (dc.getSurfaceGeometry() == null || dc.getSurfaceGeometry().size() < 1) {
			return;
		}

		dc.getGeographicSurfaceTileRenderer().setShowImageTileOutlines(isDrawTileBoundaries());

		draw(dc);
	}

	protected boolean isSynchronousRedrawOnCacheRefresh;
	private boolean isClearTextureCache = false;

	/**
	 * Hint allowing to start a redraw operation immediately after tiles has been generated in the cache system.
	 *
	 * @return <code>true</code> to activate the synchronous redraw functionality after cache generation.
	 */
	protected boolean isSynchronousRedrawOnCacheRefresh() {
		return isSynchronousRedrawOnCacheRefresh;
	}

	protected void draw(DrawContext dc) {
		// Check whether resources are present, and perform any necessary initialization.
		checkResources();

		// Clear the texture cache if it needs to be regenerated.
		if (isTextureCacheObsolete()) {
			// First generate new PNG files AND THEN clear texture cache (IN THIS STRICT ORDER)
			regenerateCachedTiles(dc);
			isTextureCacheObsolete = false;
			isClearTextureCache = true;
		}
		if (isClearTextureCache) {
			clearTextureCache(dc);
			if (isSynchronousRedrawOnCacheRefresh())
				Viewer3D.getWwd().redrawNow();
		}

		assembleTiles(dc); // Determine the tiles to draw.

		if (currentTiles.size() >= 1) {

			if (getScreenCredit() != null) {
				dc.addScreenCredit(getScreenCredit());
			}

			GL2 gl = dc.getGL().getGL2();

			if (isUseTransparentTextures() || getOpacity() < 1) {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT | GL2.GL_CURRENT_BIT);
				setBlendingFunction(dc);
			} else {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT);
			}

			gl.glPolygonMode(GL.GL_FRONT, GL2GL3.GL_FILL);
			gl.glEnable(GL.GL_CULL_FACE);
			gl.glCullFace(GL.GL_BACK);

			dc.setPerFrameStatistic(PerformanceStatistic.IMAGE_TILE_COUNT, tileCountName, currentTiles.size());

			drawTiles(dc);

			gl.glPopAttrib();

			if (drawTileIDs) {
				drawTileIDs(dc, currentTiles);
			}

			if (drawBoundingVolumes) {
				drawBoundingVolumes(dc, currentTiles);
			}

			// Check texture expiration. Memory-cached textures are checked for
			// expiration only when an explicit,
			// non-zero expiry time has been set for the layer. If none has been
			// set, the expiry times of the layer's
			// individual levels are used, but only for images in the local file
			// cache, not textures in memory. This is
			// to avoid incurring the overhead of checking expiration of
			// in-memory textures, a very rarely used feature.
			if (getExpiryTime() > 0 && getExpiryTime() < System.currentTimeMillis()) {
				checkTextureExpiration(dc, currentTiles);
			}

			currentTiles.clear();
		}

		sendRequests();
	}

	/**
	 * Render the current tiles.
	 *
	 * @param dc Drawing context.
	 */
	protected void drawTiles(DrawContext dc) {
		dc.getGeographicSurfaceTileRenderer().renderTiles(dc, currentTiles);
	}

	protected void checkTextureExpiration(DrawContext dc, List<MyTextureTile> tiles) {
		for (MyTextureTile tile : tiles) {
			if (tile.isTextureExpired()) {
				requestTexture(dc, tile);
			}
		}
	}

	protected void setBlendingFunction(DrawContext dc) {
		// Set up a premultiplied-alpha blending function. Any texture read by
		// JOGL will have alpha-premultiplied color
		// components, as will any DDS file created by World Wind or the World
		// Wind WMS. We'll also set up the base
		// color as a premultiplied color, so that any incoming premultiplied
		// color will be properly combined with the
		// base color.

		GL2 gl = dc.getGL().getGL2();

		double alpha = getOpacity();
		gl.glColor4d(alpha, alpha, alpha, alpha);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
	}

	protected void sendRequests() {
		Runnable task = requestQ.poll();
		while (task != null) {
			if (!WorldWind.getTaskService().isFull()) {
				WorldWind.getTaskService().addTask(task);
			}
			task = requestQ.poll();
		}
	}

	@Override
	public boolean isLayerInView(DrawContext dc) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (dc.getView() == null) {
			String message = Logging.getMessage("layers.AbstractLayer.NoViewSpecifiedInDrawingContext");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		Sector sector = null;
		if (getMovement() != null) {
			// compute the moved sector of the layer to check if it intersect
			// the visible sector
			sector = move(levels.getSector());
		} else {
			sector = levels.getSector();
		}

		return !(dc.getVisibleSector() != null && !GeoBoxUtils.intersects(sector, dc.getVisibleSector()));
	}

	protected Vec4 computeReferencePoint(DrawContext dc) {
		if (dc.getViewportCenterPosition() != null) {
			return dc.getGlobe().computePointFromPosition(dc.getViewportCenterPosition());
		}

		java.awt.geom.Rectangle2D viewport = dc.getView().getViewport();
		int x = (int) viewport.getWidth() / 2;
		for (int y = (int) (0.5 * viewport.getHeight()); y >= 0; y--) {
			Position pos = dc.getView().computePositionFromScreenPoint(x, y);
			if (pos == null) {
				continue;
			}

			return dc.getGlobe().computePointFromPosition(pos.getLatitude(), pos.getLongitude(), 0d);
		}

		return null;
	}

	protected Vec4 getReferencePoint(DrawContext dc) {
		return computeReferencePoint(dc);
	}

	protected static class LevelComparer implements Comparator<MyTextureTile> {
		@Override
		public int compare(MyTextureTile ta, MyTextureTile tb) {
			int la = ta.getFallbackTile() == null ? ta.getLevelNumber() : ta.getFallbackTile().getLevelNumber();
			int lb = tb.getFallbackTile() == null ? tb.getLevelNumber() : tb.getFallbackTile().getLevelNumber();

			return la - lb;
		}
	}

	protected void drawTileIDs(DrawContext dc, ArrayList<MyTextureTile> tiles) {
		java.awt.Rectangle viewport = dc.getView().getViewport();
		TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(dc.getTextRendererCache(),
				java.awt.Font.decode("Arial-Plain-13"));

		dc.getGL().glDisable(GL.GL_DEPTH_TEST);
		dc.getGL().glDisable(GL.GL_BLEND);
		dc.getGL().glDisable(GL.GL_TEXTURE_2D);

		textRenderer.beginRendering(viewport.width, viewport.height);
		textRenderer.setColor(java.awt.Color.YELLOW);
		for (MyTextureTile tile : tiles) {
			String tileLabel = tile.getLabel();

			if (tile.getFallbackTile() != null) {
				tileLabel += "/" + tile.getFallbackTile().getLabel();
			}

			LatLon ll = tile.getSector().getCentroid();
			Vec4 pt = dc.getGlobe().computePointFromPosition(ll.getLatitude(), ll.getLongitude(),
					dc.getGlobe().getElevation(ll.getLatitude(), ll.getLongitude()));
			pt = dc.getView().project(pt);
			textRenderer.draw(tileLabel, (int) pt.x, (int) pt.y);
		}
		textRenderer.setColor(java.awt.Color.WHITE);
		textRenderer.endRendering();
	}

	protected void drawBoundingVolumes(DrawContext dc, ArrayList<MyTextureTile> tiles) {
		float[] previousColor = new float[4];
		dc.getGL().getGL2().glGetFloatv(GL2ES1.GL_CURRENT_COLOR, previousColor, 0);
		dc.getGL().getGL2().glColor3d(0, 1, 0);

		for (MyTextureTile tile : tiles) {
			if (tile.getExtent(dc) instanceof Renderable) {
				((Renderable) tile.getExtent(dc)).render(dc);
			}
		}

		Box c = Sector.computeBoundingBox(dc.getGlobe(), dc.getVerticalExaggeration(), levels.getSector());
		dc.getGL().getGL2().glColor3d(1, 1, 0);
		c.render(dc);

		dc.getGL().getGL2().glColor4fv(previousColor, 0);
	}

	// **************************************************************//
	// ******************** Configuration *************************//
	// **************************************************************//

	/**
	 * Creates a configuration document for a TiledImageLayer described by the specified params. The returned document
	 * may be used as a construction parameter to {@link gov.nasa.worldwind.layers.BasicTiledImageLayer}.
	 *
	 * @param params parameters describing the TiledImageLayer.
	 *
	 * @return a configuration document for the TiledImageLayer.
	 */
	public static Document createTiledImageLayerConfigDocument(AVList params) {
		Document doc = WWXML.createDocumentBuilder(true).newDocument();

		Element root = WWXML.setDocumentElement(doc, "Layer");
		WWXML.setIntegerAttribute(root, "version", 1);
		WWXML.setTextAttribute(root, "layerType", "TiledImageLayer");

		createTiledImageLayerConfigElements(params, root);

		return doc;
	}

	/**
	 * Appends TiledImageLayer configuration parameters as elements to the specified context. This appends elements for
	 * the following parameters:
	 * <table>
	 * <tr>
	 * <th>Parameter</th>
	 * <th>Element Path</th>
	 * <th>Type</th>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#SERVICE_NAME}</td>
	 * <td>Service/@serviceName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#IMAGE_FORMAT}</td>
	 * <td>ImageFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#AVAILABLE_IMAGE_FORMATS}</td>
	 * <td>AvailableImageFormats/ImageFormat</td>
	 * <td>String array</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#FORCE_LEVEL_ZERO_LOADS}</td>
	 * <td>ForceLevelZeroLoads</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETAIN_LEVEL_ZERO_TILES}</td>
	 * <td>RetainLevelZeroTiles</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#TEXTURE_FORMAT}</td>
	 * <td>TextureFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_MIP_MAPS}</td>
	 * <td>UseMipMaps</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_TRANSPARENT_TEXTURES}</td>
	 * <td>UseTransparentTextures</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_CONNECT_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ConnectTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_READ_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ReadTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT}</td>
	 * <td>RetrievalTimeouts/StaleRequestLimit/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * </table>
	 * This also writes common layer and LevelSet configuration parameters by invoking
	 * {@link gov.nasa.worldwind.layers.AbstractLayer#createLayerConfigElements(gov.nasa.worldwind.avlist.AVList, org.w3c.dom.Element)}
	 * and
	 * {@link DataConfigurationUtils#createLevelSetConfigElements(gov.nasa.worldwind.avlist.AVList, org.w3c.dom.Element)}
	 * .
	 *
	 * @param params the key-value pairs which define the TiledImageLayer configuration parameters.
	 * @param context the XML document root on which to append TiledImageLayer configuration elements.
	 *
	 * @return a reference to context.
	 *
	 * @throws IllegalArgumentException if either the parameters or the context are null.
	 */
	public static Element createTiledImageLayerConfigElements(AVList params, Element context) {
		if (params == null) {
			String message = Logging.getMessage("nullValue.ParametersIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (context == null) {
			String message = Logging.getMessage("nullValue.ContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		// PLACA WORKAROUND seems that WWXML.makeXPath() return a NullPointerException on mac
		// platform
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(new URLClassLoader(new URL[0], cl));
		// END WORKAROUND

		XPath xpath = WWXML.makeXPath();

		// Common layer properties.
		AbstractLayer.createLayerConfigElements(params, context);

		// LevelSet properties.
		DataConfigurationUtils.createLevelSetConfigElements(params, context);

		// Service properties.
		// Try to get the SERVICE_NAME property, but default to "WWTileService".
		String s = AVListImpl.getStringValue(params, AVKey.SERVICE_NAME, "WWTileService");
		if (s != null && s.length() > 0) {
			// The service element may already exist, in which case we want to
			// append to it.
			Element el = WWXML.getElement(context, "Service", xpath);
			if (el == null) {
				el = WWXML.appendElementPath(context, "Service");
			}
			WWXML.setTextAttribute(el, "serviceName", s);
		}

		WWXML.checkAndAppendBooleanElement(params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE, context,
				"RetrievePropertiesFromService");

		// Image format properties.
		WWXML.checkAndAppendTextElement(params, AVKey.IMAGE_FORMAT, context, "ImageFormat");
		WWXML.checkAndAppendTextElement(params, AVKey.TEXTURE_FORMAT, context, "TextureFormat");

		Object o = params.getValue(AVKey.AVAILABLE_IMAGE_FORMATS);
		if (o != null && o instanceof String[]) {
			String[] strings = (String[]) o;
			if (strings.length > 0) {
				// The available image formats element may already exists, in
				// which case we want to append to it, rather
				// than create entirely separate paths.
				Element el = WWXML.getElement(context, "AvailableImageFormats", xpath);
				if (el == null) {
					el = WWXML.appendElementPath(context, "AvailableImageFormats");
				}
				WWXML.appendTextArray(el, "ImageFormat", strings);
			}
		}

		// Optional behavior properties.
		WWXML.checkAndAppendBooleanElement(params, AVKey.FORCE_LEVEL_ZERO_LOADS, context, "ForceLevelZeroLoads");
		WWXML.checkAndAppendBooleanElement(params, AVKey.RETAIN_LEVEL_ZERO_TILES, context, "RetainLevelZeroTiles");
		WWXML.checkAndAppendBooleanElement(params, AVKey.USE_MIP_MAPS, context, "UseMipMaps");
		WWXML.checkAndAppendBooleanElement(params, AVKey.USE_TRANSPARENT_TEXTURES, context, "UseTransparentTextures");
		WWXML.checkAndAppendDoubleElement(params, AVKey.DETAIL_HINT, context, "DetailHint");

		// Retrieval properties.
		if (params.getValue(AVKey.URL_CONNECT_TIMEOUT) != null || params.getValue(AVKey.URL_READ_TIMEOUT) != null
				|| params.getValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT) != null) {
			Element el = WWXML.getElement(context, "RetrievalTimeouts", xpath);
			if (el == null) {
				el = WWXML.appendElementPath(context, "RetrievalTimeouts");
			}

			WWXML.checkAndAppendTimeElement(params, AVKey.URL_CONNECT_TIMEOUT, el, "ConnectTimeout/Time");
			WWXML.checkAndAppendTimeElement(params, AVKey.URL_READ_TIMEOUT, el, "ReadTimeout/Time");
			WWXML.checkAndAppendTimeElement(params, AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, el,
					"StaleRequestLimit/Time");
		}

		// Bathyscope contrast properties
		Element el = WWXML.appendElementPath(context, "BathyscopeContrast");
		WWXML.checkAndAppendTextElement(params, AVKey.ELEVATION_EXTREMES_FILE, el, "FileName");
		WWXML.checkAndAppendTextElement(params, XML_KEY_UNIT, el, XML_KEY_UNIT);
		WWXML.checkAndAppendTextElement(params, XML_KEY_GlobalMinValue, el, XML_KEY_GlobalMinValue);
		WWXML.checkAndAppendTextElement(params, XML_KEY_GlobalMaxValue, el, XML_KEY_GlobalMaxValue);
		WWXML.checkAndAppendTextElement(params, XML_KEY_MinValue, el, XML_KEY_MinValue);
		WWXML.checkAndAppendTextElement(params, XML_KEY_MaxValue, el, XML_KEY_MaxValue);

		return context;
	}

	/**
	 * Parses TiledImageLayer configuration parameters from the specified DOM document. This writes output as key-value
	 * pairs to params. If a parameter from the XML document already exists in params, that parameter is ignored.
	 * Supported key and parameter names are:
	 * <table>
	 * <tr>
	 * <th>Parameter</th>
	 * <th>Element Path</th>
	 * <th>Type</th>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#SERVICE_NAME}</td>
	 * <td>Service/@serviceName</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#IMAGE_FORMAT}</td>
	 * <td>ImageFormat</td>
	 * <td>String</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#AVAILABLE_IMAGE_FORMATS}</td>
	 * <td>AvailableImageFormats/ImageFormat</td>
	 * <td>String array</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#FORCE_LEVEL_ZERO_LOADS}</td>
	 * <td>ForceLevelZeroLoads</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETAIN_LEVEL_ZERO_TILES}</td>
	 * <td>RetainLevelZeroTiles</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#TEXTURE_FORMAT}</td>
	 * <td>TextureFormat</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_MIP_MAPS}</td>
	 * <td>UseMipMaps</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#USE_TRANSPARENT_TEXTURES}</td>
	 * <td>UseTransparentTextures</td>
	 * <td>Boolean</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_CONNECT_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ConnectTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#URL_READ_TIMEOUT}</td>
	 * <td>RetrievalTimeouts/ReadTimeout/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT}</td>
	 * <td>RetrievalTimeouts/StaleRequestLimit/Time</td>
	 * <td>Integer milliseconds</td>
	 * </tr>
	 * </table>
	 * This also parses common layer and LevelSet configuration parameters by invoking
	 * {@link gov.nasa.worldwind.layers.AbstractLayer#getLayerConfigParams(org.w3c.dom.Element, gov.nasa.worldwind.avlist.AVList)}
	 * and
	 * {@link gov.nasa.worldwind.util.DataConfigurationUtils#getLevelSetConfigParams(org.w3c.dom.Element, gov.nasa.worldwind.avlist.AVList)}
	 * .
	 *
	 * @param domElement the XML document root to parse for TiledImageLayer configuration parameters.
	 * @param params the output key-value pairs which recieve the TiledImageLayer configuration parameters. A null
	 *            reference is permitted.
	 *
	 * @return a reference to params, or a new AVList if params is null.
	 *
	 * @throws IllegalArgumentException if the document is null.
	 */
	public static AVList getTiledImageLayerConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		// PLACA WORKAROUND seems that WWXML.makeXPath() return a NullPointerException on mac
		// platform
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(new URLClassLoader(new URL[0], cl));
		// END WORKAROUND
		XPath xpath = WWXML.makeXPath();

		// Common layer properties.
		AbstractLayer.getLayerConfigParams(domElement, params);

		// LevelSet properties.
		DataConfigurationUtils.getLevelSetConfigParams(domElement, params);

		// Service properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.SERVICE_NAME, "Service/@serviceName", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE,
				"RetrievePropertiesFromService", xpath);

		// Image format properties.
		WWXML.checkAndSetStringParam(domElement, params, AVKey.IMAGE_FORMAT, "ImageFormat", xpath);
		WWXML.checkAndSetStringParam(domElement, params, AVKey.TEXTURE_FORMAT, "TextureFormat", xpath);
		WWXML.checkAndSetUniqueStringsParam(domElement, params, AVKey.AVAILABLE_IMAGE_FORMATS,
				"AvailableImageFormats/ImageFormat", xpath);

		// Optional behavior properties.
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.FORCE_LEVEL_ZERO_LOADS, "ForceLevelZeroLoads", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.RETAIN_LEVEL_ZERO_TILES, "RetainLevelZeroTiles", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_MIP_MAPS, "UseMipMaps", xpath);
		WWXML.checkAndSetBooleanParam(domElement, params, AVKey.USE_TRANSPARENT_TEXTURES, "UseTransparentTextures",
				xpath);
		WWXML.checkAndSetDoubleParam(domElement, params, AVKey.DETAIL_HINT, "DetailHint", xpath);
		WWXML.checkAndSetColorArrayParam(domElement, params, AVKey.TRANSPARENCY_COLORS, "TransparencyColors/Color",
				xpath);

		// Retrieval properties. Convert the Long time values to Integers,
		// because BasicTiledImageLayer is expecting
		// Integer values.
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_CONNECT_TIMEOUT,
				"RetrievalTimeouts/ConnectTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.URL_READ_TIMEOUT,
				"RetrievalTimeouts/ReadTimeout/Time", xpath);
		WWXML.checkAndSetTimeParamAsInteger(domElement, params, AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT,
				"RetrievalTimeouts/StaleRequestLimit/Time", xpath);

		// Parse the legacy configuration parameters. This enables
		// TiledImageLayer to recognize elements from previous
		// versions of configuration documents.
		getLegacyTiledImageLayerConfigParams(domElement, params);

		return params;
	}

	/**
	 * Parses TiledImageLayer configuration parameters from previous versions of configuration documents. This writes
	 * output as key-value pairs to params. If a parameter from the XML document already exists in params, that
	 * parameter is ignored. Supported key and parameter names are:
	 * <table>
	 * <tr>
	 * <th>Parameter</th>
	 * <th>Element Path</th>
	 * <th>Type</th>
	 * </tr>
	 * <tr>
	 * <td>{@link AVKey#TEXTURE_FORMAT}</td>
	 * <td>CompressTextures</td>
	 * <td>"image/dds" if CompressTextures is "true"; null otherwise</td>
	 * </tr>
	 * </table>
	 *
	 * @param domElement the XML document root to parse for legacy TiledImageLayer configuration parameters.
	 * @param params the output key-value pairs which recieve the TiledImageLayer configuration parameters. A null
	 *            reference is permitted.
	 *
	 * @return a reference to params, or a new AVList if params is null.
	 *
	 * @throws IllegalArgumentException if the document is null.
	 */
	protected static AVList getLegacyTiledImageLayerConfigParams(Element domElement, AVList params) {
		if (domElement == null) {
			String message = Logging.getMessage("nullValue.DocumentIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (params == null) {
			params = new AVListImpl();
		}

		// PLACA WORKAROUND seems that WWXML.makeXPath() return a NullPointerException on mac
		// platform
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(new URLClassLoader(new URL[0], cl));
		// END WORKAROUND
		XPath xpath = WWXML.makeXPath();

		Object o = params.getValue(AVKey.TEXTURE_FORMAT);
		if (o == null) {
			Boolean b = WWXML.getBoolean(domElement, "CompressTextures", xpath);
			if (b != null && b) {
				params.setValue(AVKey.TEXTURE_FORMAT, "image/dds");
			}
		}

		return params;
	}

	// ============== Image Composition ======================= //
	// ============== Image Composition ======================= //
	// ============== Image Composition ======================= //

	public List<String> getAvailableImageFormats() {
		return new ArrayList<String>(supportedImageFormats);
	}

	public boolean isImageFormatAvailable(String imageFormat) {
		return imageFormat != null && supportedImageFormats.contains(imageFormat);
	}

	public String getDefaultImageFormat() {
		return supportedImageFormats.size() > 0 ? supportedImageFormats.get(0) : null;
	}

	protected void setAvailableImageFormats(String[] formats) {
		supportedImageFormats.clear();

		if (formats != null) {
			supportedImageFormats.addAll(Arrays.asList(formats));
		}
	}

	protected BufferedImage requestImage(MyTextureTile tile, String mimeType)
			throws URISyntaxException, InterruptedIOException, MalformedURLException {
		String pathBase = tile.getPathBase();
		String suffix = WWIO.makeSuffixForMimeType(mimeType);
		String path = pathBase + suffix;
		File f = new File(path);
		URL url;
		if (f.isAbsolute() && f.exists()) {
			url = f.toURI().toURL();
		} else {
			url = getDataFileStore().findFile(path, false);
		}

		if (url == null) {
			return null;
		}

		if (WWIO.isFileOutOfDate(url, tile.getLevel().getExpiryTime())) {
			// The file has expired. Delete it.
			getDataFileStore().removeFile(url);
			String message = Logging.getMessage("generic.DataFileExpired", url);
			Logging.logger().fine(message);
		} else {
			try {
				File imageFile = new File(url.toURI());
				BufferedImage image = ImageIO.read(imageFile);
				if (image == null) {
					String message = Logging.getMessage("generic.ImageReadFailed", imageFile);
					throw new RuntimeException(message);
				}

				levels.unmarkResourceAbsent(tile);
				return image;
			} catch (InterruptedIOException e) {
				throw e;
			} catch (IOException e) {
				// Assume that something's wrong with the file and delete it.
				getDataFileStore().removeFile(url);
				levels.markResourceAbsent(tile);
				String message = Logging.getMessage("generic.DeletedCorruptDataFile", url);
				Logging.logger().info(message);
			}
		}

		return null;
	}

	protected void downloadImage(final MyTextureTile tile, String mimeType, int timeout) throws Exception {
		if (getValue(AVKey.RETRIEVER_FACTORY_LOCAL) != null) {
			retrieveLocalImage(tile, mimeType, timeout);
		} else {
			// Assume it's remote.
			retrieveRemoteImage(tile, mimeType, timeout);
		}
	}

	protected void retrieveRemoteImage(final MyTextureTile tile, String mimeType, int timeout) throws Exception {
		// TODO: apply retriever-factory pattern for remote retrieval case.
		final URL resourceURL = tile.getResourceURL(mimeType);
		if (resourceURL == null) {
			return;
		}

		Retriever retriever;

		String protocol = resourceURL.getProtocol();

		if ("http".equalsIgnoreCase(protocol) || "https".equalsIgnoreCase(protocol)) {
			retriever = new HTTPRetriever(resourceURL, new CompositionRetrievalPostProcessor(tile));
			retriever.setValue(URLRetriever.EXTRACT_ZIP_ENTRY, "true"); // supports
			// legacy
			// layers
		} else {
			String message = Logging.getMessage("layers.TextureLayer.UnknownRetrievalProtocol", resourceURL);
			throw new RuntimeException(message);
		}

		Logging.logger().log(java.util.logging.Level.FINE, "Retrieving " + resourceURL.toString());
		retriever.setConnectTimeout(10000);
		retriever.setReadTimeout(timeout);
		retriever.call();
	}

	protected void retrieveLocalImage(MyTextureTile tile, String mimeType, int timeout) throws Exception {
		if (!WorldWind.getLocalRetrievalService().isAvailable()) {
			return;
		}

		RetrieverFactory retrieverFactory = (RetrieverFactory) getValue(AVKey.RETRIEVER_FACTORY_LOCAL);
		if (retrieverFactory == null) {
			return;
		}

		AVListImpl avList = new AVListImpl();
		avList.setValue(AVKey.SECTOR, tile.getSector());
		avList.setValue(AVKey.WIDTH, tile.getWidth());
		avList.setValue(AVKey.HEIGHT, tile.getHeight());
		avList.setValue(AVKey.FILE_NAME, tile.getPath());
		avList.setValue(AVKey.IMAGE_FORMAT, mimeType);

		Retriever retriever = retrieverFactory.createRetriever(avList, new CompositionRetrievalPostProcessor(tile));

		Logging.logger().log(java.util.logging.Level.FINE, "Locally retrieving " + tile.getPath());
		retriever.setReadTimeout(timeout);
		retriever.call();
	}

	public int computeLevelForResolution(Sector sector, double resolution) {
		if (sector == null) {
			String message = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		// Find the first level exceeding the desired resolution
		double texelSize;
		Level targetLevel = levels.getLastLevel();
		for (int i = 0; i < getLevels().getLastLevel().getLevelNumber(); i++) {
			if (levels.isLevelEmpty(i)) {
				continue;
			}

			texelSize = levels.getLevel(i).getTexelSize();
			if (texelSize > resolution) {
				continue;
			}

			targetLevel = levels.getLevel(i);
			break;
		}

		// Choose the level closest to the resolution desired
		if (targetLevel.getLevelNumber() != 0 && !levels.isLevelEmpty(targetLevel.getLevelNumber() - 1)) {
			Level nextLowerLevel = levels.getLevel(targetLevel.getLevelNumber() - 1);
			double dless = Math.abs(nextLowerLevel.getTexelSize() - resolution);
			double dmore = Math.abs(targetLevel.getTexelSize() - resolution);
			if (dless < dmore) {
				targetLevel = nextLowerLevel;
			}
		}

		Logging.logger().fine(Logging.getMessage("layers.TiledImageLayer.LevelSelection", targetLevel.getLevelNumber(),
				Double.toString(targetLevel.getTexelSize())));
		return targetLevel.getLevelNumber();
	}

	public long countImagesInSector(Sector sector, int levelNumber) {
		if (sector == null) {
			String msg = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		Level targetLevel = levels.getLastLevel();
		if (levelNumber >= 0) {
			for (int i = levelNumber; i < getLevels().getLastLevel().getLevelNumber(); i++) {
				if (levels.isLevelEmpty(i)) {
					continue;
				}

				targetLevel = levels.getLevel(i);
				break;
			}
		}

		// Collect all the tiles intersecting the input sector.
		LatLon delta = targetLevel.getTileDelta();
		LatLon origin = levels.getTileOrigin();
		final int nwRow = Tile.computeRow(delta.getLatitude(), sector.getMaxLatitude(), origin.getLatitude());
		final int nwCol = Tile.computeColumn(delta.getLongitude(), sector.getMinLongitude(), origin.getLongitude());
		final int seRow = Tile.computeRow(delta.getLatitude(), sector.getMinLatitude(), origin.getLatitude());
		final int seCol = Tile.computeColumn(delta.getLongitude(), sector.getMaxLongitude(), origin.getLongitude());

		long numRows = nwRow - seRow + 1;
		long numCols = seCol - nwCol + 1;

		return numRows * numCols;
	}

	public MyTextureTile[][] getTilesInSector(Sector sector, int levelNumber) {
		if (sector == null) {
			String msg = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		Level targetLevel = levels.getLastLevel();
		if (levelNumber >= 0) {
			for (int i = levelNumber; i < getLevels().getLastLevel().getLevelNumber(); i++) {
				if (levels.isLevelEmpty(i)) {
					continue;
				}

				targetLevel = levels.getLevel(i);
				break;
			}
		}

		// Collect all the tiles intersecting the input sector.
		LatLon delta = targetLevel.getTileDelta();
		LatLon origin = levels.getTileOrigin();
		final int nwRow = Tile.computeRow(delta.getLatitude(), sector.getMaxLatitude(), origin.getLatitude());
		final int nwCol = Tile.computeColumn(delta.getLongitude(), sector.getMinLongitude(), origin.getLongitude());
		final int seRow = Tile.computeRow(delta.getLatitude(), sector.getMinLatitude(), origin.getLatitude());
		final int seCol = Tile.computeColumn(delta.getLongitude(), sector.getMaxLongitude(), origin.getLongitude());

		int numRows = nwRow - seRow + 1;
		int numCols = seCol - nwCol + 1;
		MyTextureTile[][] sectorTiles = new MyTextureTile[numRows][numCols];

		for (int row = nwRow; row >= seRow; row--) {
			for (int col = nwCol; col <= seCol; col++) {
				TileKey key = new TileKey(targetLevel.getLevelNumber(), row, col, targetLevel.getCacheName());
				Sector tileSector = levels.computeSectorForKey(key);
				sectorTiles[nwRow - row][col - nwCol] = new MyTextureTile(tileSector, targetLevel, row, col, this);
			}
		}

		return sectorTiles;
	}

	protected BufferedImage getImage(MyTextureTile tile, String mimeType, int timeout) throws Exception {
		// Read the image from disk.
		BufferedImage image = requestImage(tile, mimeType);
		Thread.sleep(1); // generates InterruptedException if thread has been
		// interrupted
		if (image != null) {
			return image;
		}

		// Retrieve it from the net since it's not on disk.
		downloadImage(tile, mimeType, timeout);

		// Try to read from disk again after retrieving it from the net.
		image = requestImage(tile, mimeType);
		Thread.sleep(1); // generates InterruptedException if thread has been
		// interupted
		if (image == null) {
			String message = Logging.getMessage("layers.TiledImageLayer.ImageUnavailable", tile.getPath());
			throw new RuntimeException(message);
		}

		return image;
	}

	protected class CompositionRetrievalPostProcessor extends AbstractRetrievalPostProcessor {
		protected MyTextureTile tile;

		public CompositionRetrievalPostProcessor(MyTextureTile tile) {
			this.tile = tile;
		}

		@Override
		protected File doGetOutputFile() {
			String suffix = WWIO.makeSuffixForMimeType(getRetriever().getContentType());
			if (suffix == null) {
				Logging.logger()
						.severe(Logging.getMessage("generic.UnknownContentType", getRetriever().getContentType()));
				return null;
			}

			String path = tile.getPathBase();
			path += suffix;

			File f = new File(path);
			final File outFile = f.isAbsolute() ? f : getDataFileStore().newFile(path);
			if (outFile == null) {
				return null;
			}

			return outFile;
		}

		@Override
		protected boolean isDeleteOnExit(File outFile) {
			return outFile.getPath().contains(WWIO.DELETE_ON_EXIT_PREFIX);
		}

		@Override
		protected boolean overwriteExistingFile() {
			return true;
		}

		@Override
		protected void markResourceAbsent() {
			levels.markResourceAbsent(tile);
		}

		@Override
		protected void handleUnsuccessfulRetrieval() {
			// Don't mark the tile as absent because the caller may want to try
			// again.
		}
	}

	@Override
	public List<Property<?>> getProperties() {
		return infos.getProperties();
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	/**
	 * Sets folder for binary data for this layer.
	 */
	public void setBinFolder(File file) {
		binFolder = file;
	}

	public void setCachedTilesProducer(ITiledRasterCacheProducer cachedTilesProducer) {
		this.cachedTilesProducer = cachedTilesProducer;
	}

	/**
	 * Import current tiles or top level tiles into the NWW file store and add them in the texture cache.
	 *
	 * @param dc The drawing context.
	 */
	protected void regenerateCachedTiles(DrawContext dc) {
		PerfoCounter counter = new PerfoCounter("cachedTiles.regeneration");
		counter.start();

		if (cachedTilesProducer != null) {

			Document doc = cachedTilesProducer.produceCachedTiles(dc);
			if (doc != null) {
				// Propagate new generated parameters into existing LevelSet
				// Note : NASA WW uses LevelSet parameters for Tiles identification.
				LevelSet newLevels = cachedTilesProducer.getLevels();

				levels.setValues(newLevels);

				for (int iLevel = 0; iLevel < levels.getNumLevels(); iLevel++) {
					Level newLevel = newLevels.getLevel(iLevel);
					Level oldLevel = levels.getLevel(iLevel);

					AVList newParams = newLevel.getParams();
					oldLevel.setValues(newLevel);
					oldLevel.getParams().setValues(newParams);

					String cacheName = newParams.getStringValue(AVKey.DATA_CACHE_NAME);
					PrivateAccessor.setField(oldLevel, "cacheName", cacheName);
					PrivateAccessor.setField(oldLevel, "path", cacheName + "/" + newLevel.getLevelName());
				}

				ShaderElevationLayer.getTiledImageLayerConfigParams(doc.getDocumentElement(), this);

				synchronized (Viewer3D.getModel()) {
					firePropertyChange(AVKey.LAYER, null, this);
				}
			}
		}

		// Print some statistics about cache regeneration (async)
		double timeElapsed = counter.stop().getTimeElapsed();
		if (logger.isDebugEnabled()) {
			logger.debug("Cached tiles for " + getName() + " regenerated in " + timeElapsed * 1000 + "ms");
		}
	}

	/**
	 * Create absolutely all tiles of all levels.
	 *
	 * @param dc Drawing context.
	 * @param allTiles List to be filled by the method.
	 * @param tile The top level tile to start from.
	 */
	public void assembleAllTiles(DrawContext dc, List<MyTextureTile> allTiles, MyTextureTile tile) {
		allTiles.add(tile);

		Level nextLevel = levels.getLevel(tile.getLevelNumber() + 1);
		if (nextLevel != null) {
			MyTextureTile[] subTiles = tile.createSubTiles(nextLevel);
			for (MyTextureTile child : subTiles) {
				assembleAllTiles(dc, allTiles, child);
			}
		}
	}

	/**
	 * Clear all textures in the drawing context's cache.
	 *
	 * @param dc Drawing context.
	 */
	protected void clearTextureCache(DrawContext dc) {
		//
		// Note : topLevel tiles must have been recomputed according to the current level.
		// This means that currentLevel's path must be up-to-date because topLevel tiles
		// contains tiles having a reference on the level's path !!!

		List<MyTextureTile> allTiles = new ArrayList<>();
		for (MyTextureTile tile : getTopLevels()) {
			assembleAllTiles(dc, allTiles, tile);
		}

		GpuResourceCache gpuCache = dc.getTextureCache();
		for (MyTextureTile tile : allTiles) {
			gpuCache.remove(tile.getTileKey());
		}

		isClearTextureCache = false;
	}

	/**
	 * Marks the texture cache as cobsolete. Next time the {@link #render(DrawContext)} method will be called, all tiles
	 * textures will be cleared from the cache and then reloaded from their respective data sources.
	 */
	public void setClearTextureCache(boolean isClearTextureCache) {
		this.isClearTextureCache = isClearTextureCache;
	}

	public boolean isClearTextureCache() {
		return isClearTextureCache;
	}

	/**
	 * Marks the texture cache as cobsolete. Next time the {@link #render(DrawContext)} method will be called, all tiles
	 * textures will be cleared from the cache and then reloaded from their respective data sources.
	 */
	public void setTextureCacheObsolete() {
		isTextureCacheObsolete = true;
	}

	/**
	 * @return <code>true</code> only if the texture cache must be recomputed.
	 */
	public boolean isTextureCacheObsolete() {
		return isTextureCacheObsolete;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		unregisterListeners();
		if (cachedTilesProducer != null) {
			cachedTilesProducer.dispose();
		}
		super.dispose();
	}

	/**
	 * @return the cachedTilesProducer
	 */
	public ITiledRasterCacheProducer getCachedTilesProducer() {
		return cachedTilesProducer;
	}
}