/*
Copyright (C) 2001, 2006 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
 */
package fr.ifremer.viewer3d.layers.deprecated.dtm;

import java.util.ArrayList;
import java.util.List;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.cache.BasicMemoryCache;
import gov.nasa.worldwind.cache.GpuResourceCache;
import gov.nasa.worldwind.cache.MemoryCache;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Extent;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.SurfaceTile;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.Tile;
import gov.nasa.worldwind.util.TileKey;

/**
 * This class manages the conversion and timing of image data to a JOGL Texture, and provides an interface for binding
 * the texture and applying any texture transforms to align the texture and texture coordinates.
 * <p/>
 *
 * @author tag
 * @version $Id: MyTextureTile.java,v 1.3 2012/11/12 15:09:18 bvalliere Exp $
 */
@Deprecated(forRemoval = true)
public class MyTextureTile extends Tile implements SurfaceTile {
	private volatile TextureData textureData; // if non-null, then must be
	// converted to a Texture
	private MyTextureTile fallbackTile = null; // holds texture to use if own
	// texture not available
	private Vec4 centroid; // Cartesian coordinate of lat/lon center
	private Extent extent = null; // bounding volume
	private Object globeStateKey;
	private boolean hasMipmapData = false;
	private long updateTime = 0;
	private MyTiledImageLayer layer = null;

	/**
	 * Returns the memory cache used to cache tiles for this class and its subclasses, initializing the cache if it
	 * doesn't yet exist.
	 *
	 * @return the memory cache associated with the tile.
	 */
	public static synchronized MemoryCache getMemoryCache() {
		if (!WorldWind.getMemoryCacheSet().containsCache(MyTextureTile.class.getName())) {
			long size = Configuration.getLongValue(AVKey.TEXTURE_IMAGE_CACHE_SIZE, 30000000L);
			MemoryCache cache = new BasicMemoryCache((long) (0.85 * size), size);
			cache.setName("Texture Tiles");
			WorldWind.getMemoryCacheSet().addCache(MyTextureTile.class.getName(), cache);
		}

		return WorldWind.getMemoryCacheSet().getCache(MyTextureTile.class.getName());
	}

	public MyTextureTile(Sector sector, MyTiledImageLayer layer) {
		super(sector);
		this.layer = layer;
	}

	public MyTextureTile(Sector sector, Level level, int row, int col, MyTiledImageLayer layer) {
		super(sector, level, row, col);
		this.layer = layer;
	}

	@Override
	public final long getSizeInBytes() {
		long size = super.getSizeInBytes();

		if (textureData != null) {
			size += textureData.getEstimatedMemorySize();
		}

		return size;
	}

	@Override
	public List<? extends LatLon> getCorners() {
		ArrayList<LatLon> list = new ArrayList<LatLon>(4);
		for (LatLon ll : getSector()) {
			list.add(ll);
		}

		return list;
	}

	public MyTextureTile getFallbackTile() {
		return fallbackTile;
	}

	public void setFallbackTile(MyTextureTile fallbackTile) {
		this.fallbackTile = fallbackTile;
	}

	/**
	 * Returns the texture data most recently specified for the tile. New texture data is typically specified when a new
	 * image is read, either initially or in response to image expiration.
	 * <p/>
	 * If texture data is non-null, a new texture is created from the texture data when the tile is next bound or
	 * otherwise initialized. The texture data field is then set to null. Subsequently setting texture data to be
	 * non-null causes a new texture to be created when the tile is next bound or initialized.
	 *
	 * @return the texture data, which may be null.
	 */
	public TextureData getTextureData() {
		return textureData;
	}

	/**
	 * Specifies new texture data for the tile. New texture data is typically specified when a new image is read, either
	 * initially or in response to image expiration.
	 * <p/>
	 * If texture data is non-null, a new texture is created from the texture data when the tile is next bound or
	 * otherwise initialized. The texture data field is then set to null. Subsequently setting texture data to be
	 * non-null causes a new texture to be created when the tile is next bound or initialized.
	 * <p/>
	 * When a texture is created from the texture data, the texture data field is set to null to indicate that the data
	 * has been converted to a texture and its resources may be released.
	 *
	 * @param textureData the texture data, which may be null.
	 */
	public void setTextureData(TextureData textureData) {
		this.textureData = textureData;
		if (textureData.getMipmapData() != null) {
			hasMipmapData = true;
		}
	}

	public Texture getTexture(GpuResourceCache tc) {
		if (tc == null) {
			String message = Logging.getMessage("nullValue.TextureCacheIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		return (Texture) tc.get(getTileKey());
	}

	public boolean isTextureInMemory(GpuResourceCache tc) {
		if (tc == null) {
			String message = Logging.getMessage("nullValue.TextureCacheIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		return getTexture(tc) != null || getTextureData() != null;
	}

	public boolean isTextureExpired() {
		return this.isTextureExpired(getLevel().getExpiryTime());
	}

	public boolean isTextureExpired(long expiryTime) {
		return updateTime > 0 && updateTime < expiryTime;
	}

	public void setTexture(GpuResourceCache tc, Texture texture) {
		if (tc == null) {
			String message = Logging.getMessage("nullValue.TextureCacheIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		tc.put(getTileKey(), texture);
		updateTime = System.currentTimeMillis();

		// No more need for texture data; allow garbage collector and memory
		// cache to reclaim it.
		// This also signals that new texture data has been converted.
		textureData = null;
		updateMemoryCache();
	}

	public Vec4 getCentroidPoint(Globe globe) {
		if (globe == null) {
			String msg = Logging.getMessage("nullValue.GlobeIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		LatLon c = getSector().getCentroid();
		centroid = globe.computePointFromPosition(c.getLatitude(), c.getLongitude(), 0);

		return centroid;
	}

	@Override
	public Extent getExtent(DrawContext dc) {
		if (dc == null) {
			String msg = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}

		if (extent == null || !isExtentValid(dc)) {
			extent = Sector.computeBoundingCylinder(dc.getGlobe(), dc.getVerticalExaggeration(),
					GeoBoxUtils.normalizedDegrees(getSector()));
			globeStateKey = dc.getGlobe().getStateKey(dc);
			centroid = null;
		}

		return extent;
	}

	private boolean isExtentValid(DrawContext dc) {
		return !(dc.getGlobe() == null || globeStateKey == null) && globeStateKey.equals(dc.getGlobe().getStateKey(dc));
	}

	public MyTextureTile[] createSubTiles(Level nextLevel) {
		if (nextLevel == null) {
			String msg = Logging.getMessage("nullValue.LevelIsNull");
			Logging.logger().severe(msg);
			throw new IllegalArgumentException(msg);
		}
		Angle p0 = getSector().getMinLatitude();
		Angle p2 = getSector().getMaxLatitude();
		Angle p1 = Angle.midAngle(p0, p2);

		Angle t0 = getSector().getMinLongitude();
		Angle t2 = getSector().getMaxLongitude();
		Angle t1 = Angle.midAngle(t0, t2);

		String nextLevelCacheName = nextLevel.getCacheName();
		int nextLevelNum = nextLevel.getLevelNumber();
		int row = getRow();
		int col = getColumn();

		MyTextureTile[] subTiles = new MyTextureTile[4];

		TileKey key = new TileKey(nextLevelNum, 2 * row, 2 * col, nextLevelCacheName);
		MyTextureTile subTile = getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[0] = subTile;
		} else {
			subTiles[0] = new MyTextureTile(new Sector(p0, p1, t0, t1), nextLevel, 2 * row, 2 * col, layer);
		}

		key = new TileKey(nextLevelNum, 2 * row, 2 * col + 1, nextLevelCacheName);
		subTile = getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[1] = subTile;
		} else {
			subTiles[1] = new MyTextureTile(new Sector(p0, p1, t1, t2), nextLevel, 2 * row, 2 * col + 1, layer);
		}

		key = new TileKey(nextLevelNum, 2 * row + 1, 2 * col, nextLevelCacheName);
		subTile = getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[2] = subTile;
		} else {
			subTiles[2] = new MyTextureTile(new Sector(p1, p2, t0, t1), nextLevel, 2 * row + 1, 2 * col, layer);
		}

		key = new TileKey(nextLevelNum, 2 * row + 1, 2 * col + 1, nextLevelCacheName);
		subTile = getTileFromMemoryCache(key);
		if (subTile != null) {
			subTiles[3] = subTile;
		} else {
			subTiles[3] = new MyTextureTile(new Sector(p1, p2, t1, t2), nextLevel, 2 * row + 1, 2 * col + 1, layer);
		}

		return subTiles;
	}

	private MyTextureTile getTileFromMemoryCache(TileKey tileKey) {
		return (MyTextureTile) getMemoryCache().getObject(tileKey);
	}

	private void updateMemoryCache() {
		if (getTileFromMemoryCache(getTileKey()) != null) {
			getMemoryCache().add(getTileKey(), this);
		}
	}

	protected Texture initializeTexture(DrawContext dc) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		Texture t = getTexture(dc.getTextureCache());
		// Return texture if found and there is no new texture data
		if (t != null && getTextureData() == null) {
			return t;
		}

		if (getTextureData() == null) // texture not in cache yet texture
		// data is null, can't initialize
		{
			String msg = Logging.getMessage("nullValue.TextureDataIsNull");
			Logging.logger().severe(msg);
			throw new IllegalStateException(msg);
		}

		try {
			t = TextureIO.newTexture(getTextureData());
		} catch (Exception e) {
			return null;
		}

		setTexture(dc.getTextureCache(), t);
		t.bind(dc.getGL());

		setTextureParameters(dc, t);

		return t;
	}

	protected void setTextureParameters(DrawContext dc, Texture t) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		GL2 gl = dc.getGL().getGL2();

		// Use a mipmap minification filter when either of the following is
		// true:
		// a. The texture has mipmap data. This is typically true for formats
		// with embedded mipmaps, such as DDS.
		// b. The texture is setup to have GL automatically generate mipmaps.
		// This is typically true when a texture is
		// loaded from a standard image type, such as PNG or JPEG, and the
		// caller instructed JOGL to generate
		// mipmaps.
		// Additionally, the texture must be in the latitude range (-80, 80). We
		// do this to prevent seams that appear
		// between textures near the poles.
		//
		// TODO: remove the latitude range restriction if a better tessellator
		// fixes the problem.

		boolean useMipmapFilter = (hasMipmapData || t.isUsingAutoMipmapGeneration())
				&& getSector().getMaxLatitude().degrees < 80d && getSector().getMinLatitude().degrees > -80;

		// Set the texture minification filter. If the texture qualifies for
		// mipmaps, apply a minification filter that
		// will access the mipmap data using the highest quality algorithm. If
		// the anisotropic texture filter is
		// available, we will enable it. This will sharpen the appearance of the
		// mipmap filter when the textured
		// surface is at a high slope to the eye.
		if (useMipmapFilter) {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR_MIPMAP_LINEAR);

			// If the maximum degree of anisotropy is 2.0 or greater, then we
			// know this graphics context supports
			// the anisotropic texture filter.
			double maxAnisotropy = dc.getGLRuntimeCapabilities().getMaxTextureAnisotropy();
			if (dc.getGLRuntimeCapabilities().isUseAnisotropicTextureFilter() && maxAnisotropy >= 2.0) {
				gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT, (float) maxAnisotropy);
			}
		}
		// If the texture does not qualify for mipmaps, then apply a linear
		// minification filter.
		else {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
		}

		// Set the texture magnification filter to a linear filter. This will
		// blur the texture as the eye gets very
		// near, but this is still a better choice than nearest neighbor
		// filtering.
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);

		// Set the S and T wrapping modes to clamp to the texture edge. This way
		// no border pixels will be sampled by
		// either the minification or magnification filters.
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
	}

	@Override
	public boolean bind(DrawContext dc) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		// Reinitialize texture if new texture data
		if (getTextureData() != null) {
			Texture t = initializeTexture(dc);
			if (t != null) {
				return true; // texture was bound during initialization.
			}
		}

		Texture t = getTexture(dc.getTextureCache());

		if (t == null && getFallbackTile() != null) {
			MyTextureTile resourceTile = getFallbackTile();
			t = resourceTile.getTexture(dc.getTextureCache());
			if (t == null) {
				t = resourceTile.initializeTexture(dc);
				if (t != null) {
					return true; // texture was bound during initialization.
				}
			}
		}

		if (t != null) {
			t.bind(dc.getGL());
		}

		return t != null;
	}

	private void applyResourceTextureTransform(DrawContext dc) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (getLevel() == null) {
			return;
		}

		int levelDelta = getLevelNumber() - getFallbackTile().getLevelNumber();
		if (levelDelta <= 0) {
			return;
		}

		double twoToTheN = Math.pow(2, levelDelta);
		double oneOverTwoToTheN = 1 / twoToTheN;

		double sShift = oneOverTwoToTheN * (getColumn() % twoToTheN);
		double tShift = oneOverTwoToTheN * (getRow() % twoToTheN);

		dc.getGL().getGL2().glTranslated(sShift, tShift, 0);
		dc.getGL().getGL2().glScaled(oneOverTwoToTheN, oneOverTwoToTheN, 1);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final MyTextureTile tile = (MyTextureTile) o;

		return !(getTileKey() != null ? !getTileKey().equals(tile.getTileKey()) : tile.getTileKey() != null);
	}

	@Override
	public int hashCode() {
		return getTileKey() != null ? getTileKey().hashCode() : 0;
	}

	@Override
	public String toString() {
		return getSector().toString();
	}

	@Override
	public void applyInternalTransform(DrawContext dc, boolean textureIdentityActive) {
		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		Texture t;
		if (getTextureData() != null) {
			t = initializeTexture(dc);
		} else {
			t = getTexture(dc.getTextureCache()); // Use the tile's texture
			// if available
		}

		if (t != null) {
			if (t.getMustFlipVertically()) {
				GL2 gl = GLContext.getCurrent().getGL().getGL2();
				if (!textureIdentityActive) {
					gl.glMatrixMode(GL.GL_TEXTURE);
					gl.glLoadIdentity();
				}
				gl.glScaled(1, -1, 1);
				gl.glTranslated(0, -1, 0);
			}
			return;
		}

		// Use the tile's fallback texture if its primary texture is not
		// available.
		MyTextureTile resourceTile = getFallbackTile();
		if (resourceTile == null) {
			return;
		}

		t = resourceTile.getTexture(dc.getTextureCache());
		if (t == null && resourceTile.getTextureData() != null) {
			t = resourceTile.initializeTexture(dc);
		}

		if (t == null) {
			return;
		}

		// Apply necessary transforms to the fallback texture.
		GL2 gl = GLContext.getCurrent().getGL().getGL2();
		if (!textureIdentityActive) {
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glLoadIdentity();
		}

		if (t.getMustFlipVertically()) {
			gl.glScaled(1, -1, 1);
			gl.glTranslated(0, -1, 0);
		}

		applyResourceTextureTransform(dc);
	}

	public ArrayList<LatLon> computeLocations(MyTextureTile tile) {
		// TODO calcul tile density
		int density = 512;// value for tile of 512*512
		int numVertices = (density + 3) * (density + 3);

		Angle latMax = tile.getSector().getMaxLatitude();
		Angle dLat = tile.getSector().getDeltaLat().divide(density);
		Angle lat = tile.getSector().getMinLatitude();

		Angle lonMin = tile.getSector().getMinLongitude();
		Angle lonMax = tile.getSector().getMaxLongitude();
		Angle dLon = tile.getSector().getDeltaLon().divide(density);

		ArrayList<LatLon> latlons = new ArrayList<LatLon>(numVertices);
		for (int j = 0; j <= density + 2; j++) {
			Angle lon = lonMin;
			for (int i = 0; i <= density + 2; i++) {
				latlons.add(new LatLon(lat, lon));

				if (i > density) {
					lon = lonMax;
				} else if (i != 0) {
					lon = lon.add(dLon);
				}

				if (lon.degrees < -180) {
					lon = Angle.NEG180;
				} else if (lon.degrees > 180) {
					lon = Angle.POS180;
				}
			}

			if (j > density) {
				lat = latMax;
			} else if (j != 0) {
				lat = lat.add(dLat);
			}
		}

		return latlons;
	}

	public void setLayer(MyTiledImageLayer layer) {
		this.layer = layer;
	}

	public MyTiledImageLayer getLayer() {
		return layer;
	}
}
