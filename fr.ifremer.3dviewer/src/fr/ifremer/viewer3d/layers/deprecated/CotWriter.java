package fr.ifremer.viewer3d.layers.deprecated;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.nasa.worldwind.geom.Position;

public class CotWriter {

	protected Logger logger = LoggerFactory.getLogger(CotWriter.class);

	/**
	 * Extension for tectonic files.
	 */

	public static final String EXTENSION_COT = "cot"; //$NON-NLS-1$

	private static final String GREATER_CHARACTER = ">";

	public boolean export(File file, List<Iterable<Position>> exportableCotPositions) {

		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			for (Iterable<Position> it : exportableCotPositions) {
				bw.write(GREATER_CHARACTER + "\n");
				for (Position p : it) {
					bw.write(p.getLongitude().getDegrees() + " " + p.getLatitude().getDegrees() + "\n");
				}
			}
		} catch (Exception e) {
			logger.error("Export failed", e.getMessage());
		} finally {
			try {
				if (bw != null) {
					bw.close();
				}
			} catch (IOException e) {
				logger.error("Export failed", e.getMessage());
			}
		}

		return true;

	}

}
