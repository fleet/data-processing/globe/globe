package fr.ifremer.viewer3d.layers.markers;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.model.marker.impl.MarkerBuilder;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.typology.model.Typology;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.viewer3d.handlers.ShowMarkerEditor;
import fr.ifremer.viewer3d.layers.markers.render.WWMarkerShape;
import fr.ifremer.viewer3d.layers.wc.render.IWCPickedObject;
import fr.ifremer.viewer3d.util.tooltip.ToolTipPickedObject;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.pick.PickedObjectList;
import gov.nasa.worldwind.terrain.CompoundElevationModel;

/**
 * This class handles the add of markers from a selection in the geographic view.
 */
@Creatable
@Singleton
public class MarkerController implements MouseListener {

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(MarkerController.class);

	@Inject
	private IGeographicViewService geographicViewService;

	/** Active {@link WWMarkerLayer} **/
	private WWMarkerLayer<? extends IMarker> activeMarkerLayer;

	private GColor selectionColor = new GColor(255, 0, 0);
	private WWMarkerShape selectionShape = WWMarkerShape.SPHERE;
	private float selectionSize = 50;
	private String selectionComment = "";
	private boolean isUseTypologyColor = true;

	/**
	 * @return the active {@link WWMarkerLayer}, used to add/remove new marker.
	 */
	public WWMarkerLayer<? extends IMarker> getActiveMarkerLayer() {
		return activeMarkerLayer;
	}

	/**
	 * Sets the active {@link WWMarkerLayer}, used to add/remove new marker.
	 */
	public void setActiveMarkerLayer(WWMarkerLayer<? extends IMarker> markerLayer) {
		activeMarkerLayer = markerLayer;
	}

	/**
	 * Method called after a click on geographic view.
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
		PickedObjectList pickedObjects = geographicViewService.getWwd().getObjectsAtCurrentPosition();

		// keep only left click
		if (event.getButton() != MouseEvent.BUTTON1 || pickedObjects == null
				|| pickedObjects.getTopPickedObject() == null || event.isConsumed())
			return;

		PickedObject obj = pickedObjects.getTopPickedObject();

		// picking on terrain
		if (obj.isTerrain() || obj.getObject() instanceof ToolTipPickedObject) {
			addNewMarker(geographicViewService.getWwd().getCurrentPosition());
		}
		// picking on water column
		else if (obj.getObject() instanceof IWCPickedObject) {
			IWCPickedObject picked = (IWCPickedObject) obj.getObject();
			Position pos = picked.getPosition();
			if (pos != null) {
				// Fix the position by subtracting the vertical offset applyied to the layers (WC and marker)
				float verticalOffset = picked.getLayer().getVerticalOffset();
				if (activeMarkerLayer != null && activeMarkerLayer.getParameters().applyVerticalOffset) {
					verticalOffset += activeMarkerLayer.getParameters().verticalOffset;
				}
				if (verticalOffset != 0f) {
					pos = new Position(pos.latitude, pos.longitude, pos.elevation - verticalOffset);
				}
			} else {
				// IWCPickedObject from XML picking doesn't provide position : get it from raw PickObject
				pos = obj.getPosition();
			}
			addNewWCMarker(pos, picked.getLayer().getName(), picked.getId(), picked.getDate());
		}
	}

	/**
	 * Adds new terrain marker into the active {@link WWMarkerLayer}.
	 */
	public void addNewMarker(Position pos) {
		// check if marker file is correct
		if (activeMarkerLayer == null) {
			openMarkerEditor();
			return;
		}
		if (activeMarkerLayer instanceof WWWaterColumnMarkerLayer) {
			openMarkerEditor();
			Messages.openWarningMessage("Marker file not compatible",
					"Please select a marker file of type : 'terrain'.");
			return;
		}

		// add the marker to the active layer
		MarkerBuilder builder = getMarkerBuilder(pos);
		((WWMarkerLayer<IMarker>) activeMarkerLayer).addMarker(builder.build());
	}

	/**
	 * Adds new marker into the active {@link WWMarkerLayer}.
	 */
	public void addNewWCMarker(Position pos, String layer, int ping, Optional<Instant> dateTime) {
		// check if marker file is correct
		if (activeMarkerLayer == null) {
			openMarkerEditor();
			return;
		}
		if (!(activeMarkerLayer instanceof WWWaterColumnMarkerLayer)) {
			openMarkerEditor();
			Messages.openWarningMessage("Marker file not compatible",
					"Please select a marker file of type : 'water column'.");
			return;
		}

		// add the marker to the active layer
		MarkerBuilder builder = getMarkerBuilder(pos).setWaterColumnLayerPing(layer, ping);
		dateTime.ifPresent(builder::setDateTime);
		((WWWaterColumnMarkerLayer) activeMarkerLayer).addMarker((IWCMarker) builder.build());
	}

	private MarkerBuilder getMarkerBuilder(Position pos) {
		// compute ID
		List<? extends IMarker> markers = activeMarkerLayer.getIMarkers().asList();
		AtomicInteger id = new AtomicInteger(1);
		Pair<String, Double> bathymetryElevataion = getElevationModelAndValue(pos.latitude, pos.longitude);
		markers.forEach(m -> id.set(Math.max(id.get(), Integer.parseInt(m.getID()) + 1)));

		// compute color
		Optional<Typology> selectedTypology = Optional.ofNullable(Typologies.getInstance().getSelectedTypology());
		GColor selectedColor = isUseTypologyColor && selectedTypology.isPresent()
				? GColor.fromTypologyColor(selectedTypology.get().getColor())
				: selectionColor;

		return new MarkerBuilder(String.format("%03d", id.get()), pos.latitude.degrees, pos.longitude.degrees,
				pos.elevation)
						// bathymetry layer
						.setLayer(bathymetryElevataion.getFirst()).setDepth(bathymetryElevataion.getSecond())
						// typology & comment
						.setTypology(selectedTypology.orElse(null)).setComment(selectionComment)
						// display properties
						.setColor(selectedColor).setShape(selectionShape.getName()).setSize(selectionSize);
	}

	/**
	 * Gets elevation value and model for a specific position.<br>
	 * This method is a redefinition of {@link CompoundElevationModel#getUnmappedElevation} to grab the elevation model
	 * responsible for the elevation value
	 */
	private Pair<String, Double> getElevationModelAndValue(Angle latitude, Angle longitude) {
		List<ElevationModel> elevationModels = geographicViewService.getElevationModel().getElevationModels();
		ElevationModel em = null;
		// Find the best elevation available at the specified (latitude, longitude) coordinates.
		Double value = -Double.MAX_VALUE;
		for (int i = elevationModels.size() - 1; i >= 0; i--) // iterate from highest resolution to lowest
		{
			em = elevationModels.get(i);

			if (!em.isEnabled() || !em.contains(latitude, longitude))
				continue;

			double emValue = em.getUnmappedElevation(latitude, longitude);

			// Since we're working from highest resolution to lowest, we return the first value that's not a missing
			// data flag. Check this against the current ElevationModel's missing data flag, which might be different
			// from our own.
			if (emValue != -Double.MAX_VALUE && emValue != em.getMissingDataSignal()) {
				value = emValue;
				break;
			}
		}
		if (em instanceof CompoundElevationModel)
			return new Pair<>("Globe default layer", value);
		return em != null ? new Pair<>(em.getName(), value) : new Pair<>("undefined", 0d);
	}

	/**
	 * Opens the marker editor and check if a file is selected.
	 */
	public void openMarkerEditor() {
		try {
			CommandUtils.executeCommand(ContextInitializer.getEclipseContext(), ShowMarkerEditor.COMMAND_ID);

			// check if a marker file has been selected
			if (activeMarkerLayer == null)
				Messages.openInfoMessage("Marker file requiered", "Please select a marker file in the Marker Editor");

		} catch (GException e) {
			LOGGER.error("Error while opening Marker Editor : {}", e.getMessage(), e);
		}
	}

	public GColor getSelectionColor() {
		return selectionColor;
	}

	public void setSelectionColor(GColor color) {
		selectionColor = color;
	}

	public WWMarkerShape getSelectionShape() {
		return selectionShape;
	}

	public void setSelectionShape(WWMarkerShape selectionShape) {
		this.selectionShape = selectionShape;
	}

	public float getSelectionSize() {
		return selectionSize;
	}

	public void setSelectionSize(float selectionSize) {
		this.selectionSize = selectionSize;
	}

	public String getSelectionComment() {
		return selectionComment;
	}

	public void setSelectionComment(String selectionComment) {
		this.selectionComment = selectionComment;
	}

	public boolean isUseTypologyColor() {
		return isUseTypologyColor;
	}

	public void setUseTypologyColor(boolean isUseTypologyColor) {
		this.isUseTypologyColor = isUseTypologyColor;
	}

	//// NOT USED EVENTS

	@Override
	public void mousePressed(MouseEvent e) {// NOT USED
	}

	@Override
	public void mouseReleased(MouseEvent e) {// NOT USED
	}

	@Override
	public void mouseEntered(MouseEvent e) {// NOT USED
	}

	@Override
	public void mouseExited(MouseEvent e) {// NOT USED
	}

}
