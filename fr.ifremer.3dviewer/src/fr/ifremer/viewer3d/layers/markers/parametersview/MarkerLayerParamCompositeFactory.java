/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.markers.parametersview;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;

/**
 * Make a Composite to edit the {@link WWMarkerLayer} parameters
 */
@Component(name = "globe_viewer3d_markerlayer_param_composite_factory", service = IParametersViewCompositeFactory.class)
public class MarkerLayerParamCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof WWMarkerLayer
				? Optional.of(new MarkerLayerParametersComposite(parent, (WWMarkerLayer) selection))
				: Optional.empty();
	}
}
