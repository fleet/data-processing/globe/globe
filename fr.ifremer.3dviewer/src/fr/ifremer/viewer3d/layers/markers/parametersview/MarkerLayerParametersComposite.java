package fr.ifremer.viewer3d.layers.markers.parametersview;

import javax.swing.Timer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Scale;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.WWMarkerLayerParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.AbstractParametersCompositeWithTabs;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget.SpinnerWidgetLayout;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;
import fr.ifremer.viewer3d.layers.sync.marker.WWMarkerLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Parameter composite for {@link WWMarkerLayer}.
 */
public class MarkerLayerParametersComposite extends AbstractParametersCompositeWithTabs {

	/** Corresponding {@link WWMarkerLayer} **/
	private final WWMarkerLayer<? extends IMarker> layer;

	private WWMarkerLayerSynchronizer layersSynchronizer;

	/** Inner widgets **/
	private Button btnResizingWithZoom;
	private Button btnProjection;
	private EnabledNumberModel verticalOffset;
	private ColorPicker colorPicker;
	private Button isOverrideColorEnabled;
	private SliderComposite opacitySliderComposite;
	private Button saveColorButton;
	private Button saveSizeButton;
	private SpinnerWidget sizeWidget;

	/** Save the selected tab index **/
	private static int selectedTab;

	/**
	 * Constructor
	 */
	public MarkerLayerParametersComposite(Composite parent, WWMarkerLayer<? extends IMarker> layer) {
		super(parent);
		this.layer = layer;
		createSynchronizationGroup();
		createDisplayTab();
		createPositionTab();
		tabFolder.setSelection(selectedTab);
	}

	@Override
	protected void onSelectedTab(int selectedTab) {
		MarkerLayerParametersComposite.selectedTab = selectedTab;
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" }) // no generic specified to avoid error with WindowBuilder
	public void createSynchronizationGroup() {
		layersSynchronizer = ContextInitializer.getInContext(WWMarkerLayerSynchronizer.class);
		var grpSynchronization = new LayerParametersSynchronizerComposite(this, layersSynchronizer, layer);
		grpSynchronization.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSynchronization.adaptToLayer(layer);
	}

	private void createDisplayTab() {
		var displayTab = createTab("Display");

		// size
		Group grpMarkerSize = new Group(displayTab, SWT.NONE);
		grpMarkerSize.setLayout(new GridLayout(2, false));
		grpMarkerSize.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpMarkerSize.setText("Size");

		btnResizingWithZoom = new Button(grpMarkerSize, SWT.CHECK);
		btnResizingWithZoom.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		btnResizingWithZoom.setText("Resizing with zoom");
		btnResizingWithZoom.setSelection(layer.getParameters().resizingWithZoom);
		btnResizingWithZoom.addListener(SWT.Selection, e -> updateLayer());

		var sizeModel = new EnabledNumberModel(layer.getParameters().applySize, layer.getParameters().size);
		sizeWidget = new SpinnerWidget("Override size : ", grpMarkerSize, sizeModel, 2, SpinnerWidgetLayout.HORIZONTAL);
		sizeWidget.setMinMaxValues(0.01, 1000);
		sizeWidget.subscribe(m -> {
			updateLayer();
			saveSizeButton.setEnabled(layer.getParameters().applySize && layer.isSaveable());
		});
		sizeWidget.setToolTipText("Override each marker size");
		sizeWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		Scale sizeScale = new Scale(grpMarkerSize, SWT.HORIZONTAL);
		sizeScale.setMinimum(0);
		sizeScale.setMaximum(20);
		sizeScale.setSelection(10);
		sizeScale.setPageIncrement(1);

		Timer sizeScaleTimer = new Timer(80, e -> Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				double variation = (float) (Math.pow(sizeScale.getSelection() - 10.0, 3) * 0.01);
				double newValue = sizeWidget.getModel().value.floatValue() + variation;
				sizeWidget.setModel(new EnabledNumberModel(sizeWidget.getModel().enable, (float) newValue));
			}
		}));

		sizeScale.addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(MouseEvent e) {
				sizeScaleTimer.stop();
				sizeScale.setSelection(10);
			}

			@Override
			public void mouseDown(MouseEvent e) {
				sizeScaleTimer.start();
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		sizeScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		saveSizeButton = new Button(grpMarkerSize, SWT.PUSH);
		saveSizeButton.setText("Save in file");
		saveSizeButton.addListener(SWT.Selection, e -> saveSize());
		saveSizeButton.setEnabled(layer.getParameters().applySize && layer.isSaveable());

		// color
		Group grpColor = new Group(displayTab, SWT.NONE);
		grpColor.setLayout(new GridLayout(3, false));
		grpColor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpColor.setText("Color");

		isOverrideColorEnabled = new Button(grpColor, SWT.CHECK);
		isOverrideColorEnabled.setText("Override color : ");
		isOverrideColorEnabled.setSelection(layer.getParameters().applyColor);
		isOverrideColorEnabled.addListener(SWT.Selection, e -> {
			colorPicker.setEnabled(isOverrideColorEnabled.getSelection());
			saveColorButton.setEnabled(isOverrideColorEnabled.getSelection() && layer.isSaveable());
			updateLayer();
		});

		colorPicker = new ColorPicker(grpColor, SWT.BORDER, layer.getParameters().color, color -> updateLayer());
		GridData gd_navLineColorPicker = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_navLineColorPicker.heightHint = 18;
		gd_navLineColorPicker.widthHint = 50;
		colorPicker.setLayoutData(gd_navLineColorPicker);
		colorPicker.setEnabled(isOverrideColorEnabled.getSelection());

		saveColorButton = new Button(grpColor, SWT.PUSH);
		saveColorButton.setText("Save in file");
		saveColorButton.addListener(SWT.Selection, e -> saveColor());
		saveColorButton.setEnabled(isOverrideColorEnabled.getSelection() && layer.isSaveable());

		// opacity
		Group opacityGroup = new Group(displayTab, SWT.NONE);
		opacityGroup.setText("Opacity");
		opacityGroup.setLayout(new GridLayout(1, false));
		opacityGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		opacitySliderComposite = new SliderComposite(opacityGroup, SWT.NONE, false, value -> updateLayer());
		opacitySliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		opacitySliderComposite.setValue(layer.getParameters().opacity);
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 */
	private void createPositionTab() {
		var param = layer.getParameters();
		verticalOffset = new EnabledNumberModel(param.applyVerticalOffset, param.verticalOffset);
		var tab = super.createPositionTab(verticalOffset, newVerticalOffsetModel -> {
			verticalOffset = newVerticalOffsetModel;
			updateLayer();
		});

		// projection
		btnProjection = new Button(tab, SWT.CHECK);
		btnProjection.setText("Project on terrain");

		btnProjection.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnProjection.setSelection(layer.getParameters().applyProjectionOnTerrain);
		btnProjection.addListener(SWT.Selection, e -> updateLayer());
	}

	/**
	 * Builds new {@link WWNavigationLayerParameters} from user selection and updates layer(s).
	 */
	private void updateLayer() {
		// build new parameters
		var parameters = new WWMarkerLayerParameters(//
				opacitySliderComposite.getValue(), // opacity
				btnProjection.getSelection(), // projection
				btnResizingWithZoom.getSelection(), // resizing with zoom
				verticalOffset.enable, verticalOffset.value.floatValue(), // vertical offset
				sizeWidget.getModel().enable, sizeWidget.getModel().value.floatValue(), // size
				isOverrideColorEnabled.getSelection(), colorPicker.getColor()); // color
		// apply parameters
		layer.setParameters(parameters);
		layersSynchronizer.synchonizeWith(layer);
	}

	private void saveColor() {
		if (Messages.openSyncQuestionMessage("Save marker color",
				"The color of each marker will be overridden, continue ?", "Yes", "Cancel") == 0) {
			layer.getIMarkers().asList().forEach(m -> m.setColor(colorPicker.getColor()));
			layer.save();
		}
	}

	private void saveSize() {
		if (Messages.openSyncQuestionMessage("Save marker size",
				"The size of each marker will be overridden, continue ?", "Yes", "Cancel") == 0) {
			var size = sizeWidget.getModel().value.floatValue();
			layer.getIMarkers().asList().forEach(m -> m.setSize(size));
			layer.save();
		}
	}

}
