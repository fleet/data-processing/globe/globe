package fr.ifremer.viewer3d.layers.markers;

import java.util.Optional;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.viewer3d.layers.markers.render.IWWMarkerModel;
import fr.ifremer.viewer3d.layers.markers.render.WWMarkerShape;
import gov.nasa.worldwind.geom.Position;

/**
 * Implementation of {@link IWWMarkerModel} for {@link IMarker}.
 */
public class WWMarkerModel implements IWWMarkerModel<IMarker> {

	private final IMarker model;
	private final IWWMarkerLayer parentLayer;

	/**
	 * Constructor
	 */
	public WWMarkerModel(IMarker model, IWWMarkerLayer parentLayer) {
		super();
		this.model = model;
		this.parentLayer = parentLayer;
	}

	@Override
	public IMarker getSource() {
		return model;
	}

	@Override
	public Position getPosition() {
		return Position.fromDegrees(model.getLat(), model.getLon(), model.getElevation() + model.getVerticalOffset());
	}

	@Override
	public float getSize() {
		return parentLayer.getParameters().applySize ? parentLayer.getParameters().size : model.getSize();
	}

	@Override
	public boolean isResizeWithZoomEnabled() {
		return parentLayer.getParameters().resizingWithZoom;
	}

	@Override
	public GColor getColor() {
		return parentLayer.getParameters().applyColor ? parentLayer.getParameters().color : model.getColor();
	}

	@Override
	public WWMarkerShape getShape() {
		return WWMarkerShape.fromName(model.getShape()).orElse(IWWMarkerModel.super.getShape());
	}

	@Override
	public boolean isSelected() {
		return model.isSelected();
	}

	@Override
	public Optional<String> getTooltip() {
		StringBuilder tooltipBuilder = new StringBuilder();
		tooltipBuilder.append("Marker " + model.getID() + "\n\n");

		if (model instanceof IWCMarker) {
			tooltipBuilder.append("Layer : " + ((IWCMarker) model).getWaterColumnLayer() + "\n");
			tooltipBuilder.append("Swath : " + ((IWCMarker) model).getPing() + "\n");
		}

		tooltipBuilder.append("Position : " + LatLongFormater.latitudeToString(model.getLat()) + " : "
				+ LatLongFormater.longitudeToString(model.getLon()) + "\n");
		tooltipBuilder.append(
				IMarker.ELEVATION_LONG_NAME + " : " + NumberUtils.getStringDouble(model.getElevation()) + " m\n");

		if (model instanceof IWCMarker) {
			IWCMarker wcModel = (IWCMarker) model;
			wcModel.getDateTime().ifPresent(date -> tooltipBuilder.append("Date : " + date.toString() + "\n"));

			wcModel.getAtltitude().ifPresent(altitude -> tooltipBuilder
					.append(IMarker.ALTITUDE_LONG_NAME + " : " + NumberUtils.getStringDouble(altitude) + " m\n"));

			wcModel.getSeaFloorElevation().ifPresent(elevation -> {
				tooltipBuilder.append(IMarker.SEA_FLOOR_ELEVATION_LONG_NAME + " : "
						+ NumberUtils.getStringDouble(elevation) + " m\n");
				tooltipBuilder.append(IMarker.SEA_FLOOT_LAYER_LONG_NAME + " : " + wcModel.getBathyLayer() + "\n");
			});

		}

		model.getTypology().ifPresent(typology -> tooltipBuilder
				.append("Typology : " + typology.getGroup() + " " + typology.getClazz() + "\n"));

		tooltipBuilder.append("Comment : " + model.getComment() + "\n");

		return Optional.of(tooltipBuilder.toString());
	}

	/** {@inheritDoc} */
	@Override
	public double getOpacity() {
		// Opacity of a marker is the same than the layer's one
		return parentLayer.getOpacity();
	}

}
