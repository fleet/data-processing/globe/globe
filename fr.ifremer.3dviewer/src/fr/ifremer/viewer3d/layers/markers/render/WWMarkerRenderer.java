/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.markers.render;

import fr.ifremer.globe.ui.service.worldwind.layer.marker.WWMarkerLayerParameters;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.markers.Marker;
import gov.nasa.worldwind.render.markers.MarkerRenderer;

/**
 * This class aim at drawing WWMarkers
 */
public class WWMarkerRenderer extends MarkerRenderer {

	/** Parameters used for rendering */
	private WWMarkerLayerParameters parameters;

	/**
	 * Constructor
	 */
	public WWMarkerRenderer(WWMarkerLayerParameters parameters) {
		this.parameters = parameters;
		setKeepSeparated(false);
	}

	/** computeSurfacePoint is redefined to apply the vertical offset if enabled */
	@Override
	protected Vec4 computeSurfacePoint(DrawContext dc, Position pos) {
		if (parameters.applyVerticalOffset && parameters.verticalOffset != 0f)
			pos = new Position(pos.latitude, pos.longitude, pos.elevation + parameters.verticalOffset);
		return super.computeSurfacePoint(dc, pos);
	}

	/**
	 * @param parameters the {@link #parameters} to set
	 */
	public void fitParameters(WWMarkerLayerParameters parameters) {
		this.parameters = parameters;
		// projection on terrain
		setOverrideMarkerElevation(parameters.applyProjectionOnTerrain);
		setElevation(0);
	}

	@Override
	protected void drawAll(DrawContext dc, Iterable<Marker> markers) {
		try {
			super.drawAll(dc, markers);
		} catch (IndexOutOfBoundsException ex) {
			// avoid unexpected exception during draw 
		}
	}

}
