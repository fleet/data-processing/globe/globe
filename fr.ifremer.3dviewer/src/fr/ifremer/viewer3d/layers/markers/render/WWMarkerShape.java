package fr.ifremer.viewer3d.layers.markers.render;

import java.util.Optional;

import gov.nasa.worldwind.render.markers.BasicMarkerShape;

public enum WWMarkerShape {

	SPHERE("Sphere", BasicMarkerShape.SPHERE), //
	CONE("Cone", BasicMarkerShape.CONE), //
	CUBE("Cube", BasicMarkerShape.CUBE), //
	CYLINDER("Cylinder", BasicMarkerShape.CYLINDER); //
	//HEADING_ARROW("Arrow", BasicMarkerShape.HEADING_ARROW), //
	//HEADING_LINE("Line", BasicMarkerShape.HEADING_LINE);//

	/** Properties **/
	private final String name;
	private final String nasaKey;

	/** Constructor **/
	WWMarkerShape(String name, String nasaKey) {
		this.name = name;
		this.nasaKey = nasaKey;
	}

	public String getName() {
		return name;
	}

	public String getNasaKey() {
		return nasaKey;
	}

	public static Optional<WWMarkerShape> fromName(String name) {
		for (WWMarkerShape shape : values()) {
			if (shape.name.equals(name))
				return Optional.of(shape);
		}
		return Optional.empty();
	}
}
