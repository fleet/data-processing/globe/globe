package fr.ifremer.viewer3d.layers.markers.render;

import java.util.Optional;

import fr.ifremer.globe.core.utils.color.GColor;
import gov.nasa.worldwind.geom.Position;

/**
 * Model interface for {@link WWMarker}.
 *
 * @param <T> data source.
 */
public interface IWWMarkerModel<T> {

	T getSource();

	Position getPosition();

	float getSize();

	GColor getColor();

	double getOpacity();

	default WWMarkerShape getShape() {
		return WWMarkerShape.SPHERE;
	}

	Optional<String> getTooltip();

	default boolean isSelected() {
		return false;
	}

	boolean isResizeWithZoomEnabled();

}
