package fr.ifremer.viewer3d.layers.markers.render;

import java.util.Optional;
import java.util.function.Supplier;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuInfo;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuItemAction;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuItemInfo;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.markers.BasicMarker;
import gov.nasa.worldwind.render.markers.BasicMarkerAttributes;
import gov.nasa.worldwind.render.markers.MarkerAttributes;

/**
 * Worldwind marker build from a {@link IMarker}. Provides a contextual menu.
 */
public class WWMarker<T> extends BasicMarker implements Supplier<ContextMenuInfo> {

	/** Delete point identifier. */
	public static final String DELETE_ACTION_NAME = "DeleteAction";
	/** Move point identifier. */
	public static final String EDIT_ACTION_NAME = "EditAction";

	/** Constants **/
	// define the minimum marker size (when zoom-in the marker is not resized when too close)
	private static final double MIN_MARKER_SIZE = 0.005d;
	// define the maximum marker size (when zoom-out the marker stopped to be resized, to avoid to large markers)
	private static final double MAX_MARKER_SIZE_FACTOR = 100d; // will be multiplied with the marker pixel size
	// factor to apply when a marker is selected
	private static final float HIGHLIGTH_SIZE_FACTOR = 1.5f;

	/** Marker (model) **/
	private final IWWMarkerModel<T> model;

	/**
	 * Private constructor : user build static method
	 */
	public WWMarker(IWWMarkerModel<T> model) {
		super(model.getPosition(), new BasicMarkerAttributes());
		this.model = model;
	}

	/**
	 * Computes attributes with model properties.
	 */
	@Override
	public MarkerAttributes getAttributes() {
		// position
		setPosition(model.getPosition());

		// color
		Material material = model.isSelected() ? Material.WHITE
				: new Material(ColorUtils.convertGColorToAWT(model.getColor()));

		// size
		double markerPixels = model.getSize();
		double minMarkerSize = model.getSize();
		double maxMarkerSize = model.getSize();
		if (model.isResizeWithZoomEnabled()) {
			minMarkerSize *= MIN_MARKER_SIZE;
			maxMarkerSize *= MAX_MARKER_SIZE_FACTOR;
		}

		if (model.isSelected()) {
			markerPixels *= HIGHLIGTH_SIZE_FACTOR;
			minMarkerSize *= HIGHLIGTH_SIZE_FACTOR;
			maxMarkerSize *= HIGHLIGTH_SIZE_FACTOR;
		}

		attributes = new BasicMarkerAttributes(material, model.getShape().getNasaKey(), model.getOpacity(),
				markerPixels, minMarkerSize, maxMarkerSize);

		return attributes;
	}

	/**
	 * @return the data source of this marker.
	 */
	public T getModel() {
		return model.getSource();
	}

	/**
	 * Builds a tooltip string.
	 */
	public Optional<String> getTooltip() {
		return model.getTooltip();
	}

	/**
	 * Builds a contextual menu.
	 */
	@Override
	public ContextMenuInfo get() {
		// context menu
		ContextMenuInfo contextMenu = new ContextMenuInfo("Maker ");
		ContextMenuItemInfo item = new ContextMenuItemInfo("Edit");
		item.setAction(new ContextMenuItemAction(EDIT_ACTION_NAME));
		contextMenu.addItem(item);
		item = new ContextMenuItemInfo("Delete");
		item.setAction(new ContextMenuItemAction(DELETE_ACTION_NAME));
		contextMenu.addItem(item);
		return contextMenu;
	}

}
