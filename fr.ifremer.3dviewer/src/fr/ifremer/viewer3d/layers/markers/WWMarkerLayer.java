package fr.ifremer.viewer3d.layers.markers;

import java.beans.PropertyChangeEvent;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.marker.info.MarkerFileInfo;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerFileInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.WWMarkerLayerParameters;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.gui.markereditor.MarkerEditDialog;
import fr.ifremer.viewer3d.layers.markers.render.WWMarker;
import fr.ifremer.viewer3d.layers.markers.render.WWMarkerRenderer;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import fr.ifremer.viewer3d.util.conf.UISettings;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuController;
import fr.ifremer.viewer3d.util.contextmenu.ContextMenuEvent;
import gov.nasa.worldwind.layers.MarkerLayer;
import gov.nasa.worldwind.render.markers.Marker;

/**
 * Extends the default {@link MarkerLayer} to handle {@link IMarker}.
 */
public class WWMarkerLayer<T extends IMarker> extends MarkerLayer implements IWWMarkerLayer {

	private static final Logger LOGGER = LoggerFactory.getLogger(WWMarkerLayer.class);

	/** Service of session file */
	@Inject
	private ISessionService sessionService;

	/** Service of 3D view */
	@Inject
	private IGeographicViewService geographicViewService;

	/** Label of {@link PropertyChangeEvent} when marker are selected **/
	public static final String SELECTED_MARKER_PROPERTY = "marker_selected";

	/** Label of {@link PropertyChangeEvent} when marker are enabled/disabled **/
	public static final String ENABLED_MARKER_PROPERTY = "marker_selected";

	/** Input file **/
	protected final IMarkerFileInfo<T> markerFileInfo;

	/** Observable list of markers (do not confuse with {@link MarkerLayer}'s markers) **/
	protected final WritableObjectList<T> markers;

	/** Boolean 'isDirty' : true if the local markers have not been saved in file **/
	private final WritableBoolean isDirty = new WritableBoolean();
	/** Boolean 'isSaveable' : true if the save action is supported **/
	private boolean isSaveable = true;

	private WWMarkerLayerParameters parameters = new WWMarkerLayerParameters();

	/**
	 * Constructor
	 */
	public WWMarkerLayer(IMarkerFileInfo<T> markerFileInfo) {
		ContextInitializer.inject(this); // injection (useful for event)
		setMarkerRenderer(new WWMarkerRenderer(parameters));

		// load markers from file
		this.markerFileInfo = markerFileInfo;
		markers = new WritableObjectList<>(markerFileInfo.getMarkers());
		markers.addChangeListener(e -> {
			isDirty.set(true);
			refresh();
		});
		refresh();
	}

	/**
	 * Adds a new marker & refreshes the layer.
	 */
	public void addMarker(T marker) {
		markers.getRealm().exec(() -> markers.add(marker));
	}

	/**
	 * Removes a maker and refreshes the layer.
	 */
	public void removeMarker(IMarker marker) {
		markers.getRealm().exec(() -> markers.remove(marker));
	}

	/**
	 * Edit a maker and refreshes the layer.
	 */
	public void editMarker(IMarker marker) {
		markers.getRealm().exec(() -> {
			if (markers.contains(marker)) {
				new MarkerEditDialog(marker).open();
				markers.forceFireChange();
			}
		});
	}

	/**
	 * Refreshes the rendered {@link Marker} (display).
	 */
	public void refresh() {
		setMarkers(markers.asList().stream().filter(IMarker::isEnabled)
				.map(m -> new WWMarker<>(new WWMarkerModel(m, this))).collect(Collectors.toList()));
		geographicViewService.refresh();
	}

	/**
	 * Event handler for CONTEXT_MENU_EVENT_TOPIC (a click on context menu items)
	 */
	@org.eclipse.e4.core.di.annotations.Optional
	@Inject
	protected void onContextMenuEvt(
			@UIEventTopic(ContextMenuController.CONTEXT_MENU_EVENT_TOPIC) ContextMenuEvent evt) {
		Object source = evt.getSource();
		if (source instanceof WWMarker && markers.contains(((WWMarker<?>) source).getModel())) {
			if (WWMarker.DELETE_ACTION_NAME.equals(evt.getName())) {
				removeMarker(((WWMarker<IMarker>) source).getModel());
			} else if (WWMarker.EDIT_ACTION_NAME.equals(evt.getName())) {
				editMarker(((WWMarker<IMarker>) source).getModel());
			}
		}
	}

	public IMarkerFileInfo<T> getMarkerFileInfo() {
		return markerFileInfo;
	}

	public WritableObjectList<T> getIMarkers() {
		return markers;
	}

	/** {@inheritDoc} */
	@Override
	public <M extends IMarker> List<M> getMarkers(Class<M> typeOfMarkers) {
		return markers.asList().stream()//
				.filter(typeOfMarkers::isInstance)//
				.map(typeOfMarkers::cast)//
				.collect(Collectors.toList());
	}

	/**
	 * Selects markers (and fires {@link PropertyChangeEvent}).
	 */
	public void selectMarkers(List<IMarker> selectedMarkers) {
		var oldSelectedMarkers = markers.asList().stream().filter(IMarker::isSelected).collect(Collectors.toList());
		markers.asList().forEach(m -> m.setSelected(selectedMarkers.contains(m)));
		firePropertyChange(SELECTED_MARKER_PROPERTY, oldSelectedMarkers, selectedMarkers);
	}

	/**
	 * Enables/disables markers (and fires {@link PropertyChangeEvent}).
	 */
	public void setMarkersEnabled(List<IMarker> marker, boolean enable) {
		markers.asList().stream().filter(marker::contains).forEach(m -> m.setEnabled(enable));
		refresh();
		firePropertyChange(ENABLED_MARKER_PROPERTY, null, marker);
	}

	/** {@inheritDoc} */
	@Override
	protected WWMarkerRenderer getMarkerRenderer() {
		return (WWMarkerRenderer) super.getMarkerRenderer();
	}

	@Override
	public WWMarkerLayerParameters getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the {@link #parameters} to set
	 */
	@Override
	public void setParameters(WWMarkerLayerParameters parameters) {
		fitParameters(parameters);
		sessionService.saveSession();
	}

	/** Accepts new parameters */
	private void fitParameters(WWMarkerLayerParameters parameters) {
		this.parameters = parameters;
		setOpacity(parameters.opacity);
		getMarkerRenderer().fitParameters(parameters);
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(getParameters().toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap sessionParameters) {
		sessionParameters.get(WWMarkerLayerParameters.SESSION_KEY).ifPresent(this::fitParameters);
	}

	public WritableBoolean isDirty() {
		return isDirty;
	}

	/*
	 * SAVE METHODS
	 */

	public boolean isSaveable() {
		return isSaveable;
	}

	public void setSaveable(boolean isSaveable) {
		this.isSaveable = isSaveable;
	}

	/**
	 * Save layer's markers into the associated {@link IMarkerFileInfo}.
	 */
	public boolean save() {
		try {
			markerFileInfo.setMarkers(markers.asList());
			markerFileInfo.save();
			isDirty.set(false);
			Messages.openInfoMessage("Sucessful save", "Makers saved in : " + markerFileInfo.getFilename());
			return true;
		} catch (GIOException ex) {
			Messages.openErrorMessage("Error : " + ex.getMessage(), ex);
			LOGGER.error("Error : {} ", ex.getMessage(), ex);
			return false;
		}
	}

	/**
	 * Save layer's markers into the associated {@link IMarkerFileInfo}.
	 */
	public boolean saveAs() {
		try {
			List<T> markersToSave = getMarkerToSaveAs();
			if (markersToSave.isEmpty())
				return false;

			String filepath = openSaveAsFileSelection();
			if (filepath == null)
				return false;

			IMarkerFileInfo<T> newMarkerFileInfo = new MarkerFileInfo<>(filepath, markersToSave);
			newMarkerFileInfo.save();
			Messages.openInfoMessage("Sucessful save", "Makers saved in : " + newMarkerFileInfo.getFilename());
			return true;
		} catch (GIOException ex) {
			Messages.openErrorMessage("Error : " + ex.getMessage(), ex);
			LOGGER.error("Error : {} ", ex.getMessage(), ex);
			return false;
		}
	}

	protected List<T> getMarkerToSaveAs() {
		List<T> markersToSave = markers.asList();
		// ask if save only enabled markers
		if (!markers.asList().stream().allMatch(IMarker::isEnabled)) {
			switch (Messages.openSyncQuestionMessage("Marker Editor : Save as...", "Save only enabled markers ?",
					"Only enabled markers", "All markers")) {
			case 0: // save only enabled markers
				markersToSave = markers.asList().stream().filter(IMarker::isEnabled).collect(Collectors.toList());
				break;
			case 1: // all markers
				return markersToSave;
			default:
				return Collections.emptyList();
			}
		}
		return markersToSave;
	}

	protected String openSaveAsFileSelection() {
		FileDialog dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.SAVE);
		dialog.setFilterExtensions(new String[] { "*.csv" });
		dialog.setFileName(getName() + ".csv");

		// loads default path from configuration
		SSVConfiguration conf = SSVConfiguration.getConfiguration();
		if (conf != null && conf.getUISettings() != null) {
			String path = conf.getUISettings().getDefault(UISettings.EXPORT_PROFILE_PATH);
			if (path != null) {
				File f = new File(path);
				dialog.setFilterPath(f.getParent());
			}
		}

		String filepath = dialog.open();

		// save current directory as default directory
		if (filepath != null && conf != null && conf.getUISettings() != null) {
			conf.getUISettings().setDefault(UISettings.EXPORT_PROFILE_PATH, filepath);
		}

		return filepath;
	}

}
