package fr.ifremer.viewer3d.layers.markers;

import java.util.List;

import jakarta.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.marker.info.WCMarkerFileInfo;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerFileInfo;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.sync.marker.WWMarkerLayerSynchronizer;
import gov.nasa.worldwind.layers.MarkerLayer;

/**
 * Extends the default {@link MarkerLayer} to handle {@link IMarker}.
 */
public class WWWaterColumnMarkerLayer extends WWMarkerLayer<IWCMarker> {

	private static final Logger LOGGER = LoggerFactory.getLogger(WWWaterColumnMarkerLayer.class);

	@Inject
	private WWMarkerLayerSynchronizer markerLayerSynchronizer;

	/**
	 * Constructor
	 */
	public WWWaterColumnMarkerLayer(IMarkerFileInfo<IWCMarker> markerFileInfo) {
		super(markerFileInfo);
	}

	/**
	 * Adds a new marker & refreshes the layer.
	 */
	@Override
	public void addMarker(IWCMarker marker) {
		super.addMarker(marker);
		markerLayerSynchronizer.markerAdded(marker);
	}

	/**
	 * Removes a maker and refreshes the layer.
	 */
	public void removeMarker(IWCMarker marker) {
		super.removeMarker(marker);
		markerLayerSynchronizer.markerRemoved(marker);
	}

	/**
	 * Save layer's markers into the associated {@link IMarkerFileInfo}.
	 */
	@Override
	public boolean saveAs() {
		try {
			List<IWCMarker> markersToSave = getMarkerToSaveAs();
			if (markersToSave.isEmpty())
				return false;

			String filepath = openSaveAsFileSelection();
			if (filepath == null)
				return false;

			var newMarkerFileInfo = new WCMarkerFileInfo(filepath, markersToSave);
			newMarkerFileInfo.save();
			Messages.openInfoMessage("Sucessful save", "Makers saved in : " + newMarkerFileInfo.getFilename());
			return true;
		} catch (GIOException ex) {
			Messages.openErrorMessage("Error : " + ex.getMessage(), ex);
			LOGGER.error("Error : {} ", ex.getMessage(), ex);
			return false;
		}
	}

}
