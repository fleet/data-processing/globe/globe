/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.geobox;

import java.util.List;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWGeoBoxLayer;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.gui.actions.GoToActionUtil;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.SurfacePolygon;

/**
 * Simple layer to draw a GeoBox
 */
public class GeoBoxLayer extends RenderableLayer implements IWWGeoBoxLayer {

	/** Key of the IGeoBoxLayerModel among the layer values */
	public static final String MODEL_KEY = IGeoBoxLayerModel.class.getName();

	/** Displayed Nasa object */
	protected SurfacePolygon path;
	/** How the path is drawn */
	protected BasicShapeAttributes pathShapeAttributes;

	/**
	 * Constructor
	 */
	public GeoBoxLayer(GeoBoxLayerModel model) {
		setValue(MODEL_KEY, model);
		setPickEnabled(false);
		setName(GeoBoxLayer.class.getSimpleName());

		updatePath();
		model.addChangeListener(event -> updatePath());
	}

	/** @return the model */
	public GeoBoxLayerModel getModel() {
		return (GeoBoxLayerModel) getValue(MODEL_KEY);
	}

	/**
	 * Disposes the mouse and escape listeners
	 */
	@Override
	public void dispose() {
		getModel().dispose();
		removeKey(MODEL_KEY);
		super.dispose();
	}

	/** Creates or update the GeoBox path */
	protected void updatePath() {
		GeoBoxLayerModel model = getModel();
		if (model.getGeoBox().isNotNull() && model.getGeoBox().get().isValid()) {
			try {
				var geobox = model.getGeoBox().get().asLonLat();
				if (Double.isFinite(geobox.getHeight()) && Double.isFinite(geobox.getWidth())
						&& geobox.getProjection().isLongLatProjection()) {
					Sector sector = GeoBoxUtils.convert(geobox);
					if (path == null) {
						pathShapeAttributes = new BasicShapeAttributes();
						path = new SurfacePolygon(pathShapeAttributes);
						addRenderable(path);
						GoToActionUtil.zoomToSector(sector);
					}

					List<Position> positions = sector.asList().stream().map(latLon -> new Position(latLon, 0d))
							.collect(Collectors.toList());
					path.setLocations(positions);

					pathShapeAttributes.setDrawInterior(model.getFilled().isTrue());
					pathShapeAttributes.setInteriorMaterial(new Material(model.getInteriorColor().get()));
					pathShapeAttributes.setInteriorOpacity(model.getInteriorOpacity().get());
					pathShapeAttributes.setOutlineMaterial(new Material(model.getOutlineColor().get()));
					pathShapeAttributes.setOutlineOpacity(model.getOutlineOpacity().get());
					pathShapeAttributes.setOutlineWidth(model.getOutlineWidth().get());
				}
			} catch (ProjectionException e) {
				// Unable to transform the GeoBox to a GeoBox with a LonLat projection
			}
		} else {
			clearRenderables();
			path = null;
			pathShapeAttributes = null;
		}
		Viewer3D.refreshRequired();
	}
}
