/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.geobox;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.databinding.observable.IChangeListener;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableGeoBox;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerModel;
import fr.ifremer.viewer3d.Activator;

/**
 * Model of GeoBoxLayer
 */
public class GeoBoxLayerModel implements IGeoBoxLayerModel, Observer {

	/** GeoBox to display */
	protected WritableGeoBox geoBox;
	/** Interior filled */
	protected WritableBoolean filled = new WritableBoolean();
	/** Interior color */
	protected WritableObject<Color> interiorColor = new WritableObject<>(Color.RED);
	/** Outline opacity */
	protected WritableDouble interiorOpacity = new WritableDouble(0.15d);
	/** Outline color */
	protected WritableObject<Color> outlineColor = new WritableObject<>(Color.RED);
	/** Outline opacity */
	protected WritableDouble outlineOpacity = new WritableDouble(1d);
	/** Outline width */
	protected WritableDouble outlineWidth = new WritableDouble(2d);

	/**
	 * Constructor
	 */
	public GeoBoxLayerModel() {
		this(null);
	}

	/**
	 * Constructor
	 */
	public GeoBoxLayerModel(GeoBox geoBox) {
		this.geoBox = new WritableGeoBox(geoBox);
		Activator.getPluginParameters().getIsFillPolygonAttribute().addObserver(this);
		filled.set(Activator.getPluginParameters().getIsFillPolygonAttribute().getValue());
	}

	/** Free memory */
	public void dispose() {
		Activator.getPluginParameters().getIsFillPolygonAttribute().deleteObserver(this);
		geoBox.dispose();
		filled.dispose();
		interiorColor.dispose();
		interiorOpacity.dispose();
		outlineColor.dispose();
		outlineOpacity.dispose();
		outlineWidth.dispose();
	}

	/** Add the listener of all fields */
	public void addChangeListener(IChangeListener listener) {
		geoBox.addChangeListener(listener);
		filled.addChangeListener(listener);
		interiorColor.addChangeListener(listener);
		interiorOpacity.addChangeListener(listener);
		outlineColor.addChangeListener(listener);
		outlineOpacity.addChangeListener(listener);
		outlineWidth.addChangeListener(listener);
	}

	/** {@inheritDoc} */
	@Override
	public WritableGeoBox getGeoBox() {
		return geoBox;
	}

	/**
	 * @return the {@link #filled}
	 */
	@Override
	public WritableBoolean getFilled() {
		return filled;
	}

	/** {@inheritDoc} */
	@Override
	public WritableObject<Color> getInteriorColor() {
		return interiorColor;
	}

	/** {@inheritDoc} */
	@Override
	public WritableDouble getInteriorOpacity() {
		return interiorOpacity;
	}

	/** {@inheritDoc} */
	@Override
	public WritableObject<Color> getOutlineColor() {
		return outlineColor;
	}

	/** {@inheritDoc} */
	@Override
	public WritableDouble getOutlineOpacity() {
		return outlineOpacity;
	}

	/** {@inheritDoc} */
	@Override
	public WritableDouble getOutlineWidth() {
		return outlineWidth;
	}

	/** FillPolygon changed. */
	@Override
	public void update(Observable o, Object arg) {
		filled.set(Activator.getPluginParameters().getIsFillPolygonAttribute().getValue());
	}

}
