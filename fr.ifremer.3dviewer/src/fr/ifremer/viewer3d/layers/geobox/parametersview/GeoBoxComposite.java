/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.geobox.parametersview;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.preference.ColorSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.forms.widgets.FormToolkit;

import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerModel;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

/**
 * Composite for GeoBoxLayer layers
 */
public class GeoBoxComposite extends Composite {

	/** Binding */
	protected DataBindingContext bindingContext = new DataBindingContext();

	/**
	 * Constructor
	 */
	public GeoBoxComposite(Composite parent, IGeoBoxLayerModel model) {
		super(parent, SWT.NONE);
		setLayout(new FillLayout());
		GridDataFactory.fillDefaults().grab(true, false).applyTo(this);

		Group group = new Group(this, SWT.NONE);
		GridLayoutFactory.swtDefaults().numColumns(2).applyTo(group);
		group.setText("GeoBox");

		FormToolkit formToolkit = new FormToolkit(parent.getDisplay());

		formToolkit.createLabel(group, "Outline");
		ColorSelector outlineColorSelector = new ColorSelector(group);
		outlineColorSelector.setColorValue(ColorUtils.convertAWTToRGB(model.getOutlineColor().get()));

		formToolkit.createLabel(group, "Interior");
		ColorSelector interiorColorSelector = new ColorSelector(group);
		interiorColorSelector.setColorValue(ColorUtils.convertAWTToRGB(model.getInteriorColor().get()));

		Button fillButton = formToolkit.createButton(group, "Interior filled", SWT.CHECK);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(fillButton);

		outlineColorSelector.addListener(
				event -> model.getOutlineColor().set(ColorUtils.convertRGBToAWT(outlineColorSelector.getColorValue())));
		interiorColorSelector.addListener(event -> model.getInteriorColor()
				.set(ColorUtils.convertRGBToAWT(interiorColorSelector.getColorValue())));

		bindingContext.bindValue(WidgetProperties.enabled().observe(interiorColorSelector.getButton()),
				model.getFilled());
		bindingContext.bindValue(WidgetProperties.buttonSelection().observe(fillButton), model.getFilled());
		bindingContext.updateTargets();
	}
}
