/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lines;

import org.apache.commons.lang.math.IntRange;

import fr.ifremer.globe.core.utils.color.GColor;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Pojo defining all properties of a line
 */
public interface ILineProperties {

	/**
	 * Add a new color interpretation for some vertice of this line
	 */
	void split(int firstIndex, int lastIndex, GColor color, float width);

	/**
	 * Cancel all previous invocations of {@link #split(int, int, Color, float)}
	 */
	void canelSplittings();

	/**
	 * Highlight a single point of this line
	 */
	void highlightPoint(int index, GColor color, float pointSize);

	/**
	 * Unhighlight the single point
	 */
	void unHighlightPoint();

	/**
	 * Highlight a range of points of this line
	 */
	void highlightPoints(IntRange indexes, GColor highlightColor, float pointsize);

	/**
	 * Unhighlight the range of points
	 */
	void unHighlightPoints();

	/**
	 * Change coods XY of one of the points in this line
	 */
	void updatePoint(int index, Vec4 point);

	/**
	 * @return the number of points in this line
	 */
	int getPointCount();

	/**
	 * Getter of color
	 */
	GColor getColor();

	/**
	 * Setter of color
	 */
	void setColor(GColor color);

	/**
	 * Getter of width
	 */
	float getWidth();

	/**
	 * Setter of width
	 */
	void setWidth(float width);

	/**
	 * Getter of point's color
	 */
	GColor getPointColor();

	/**
	 * Setter of pointColor
	 */
	void setPointColor(GColor pointColor);

	/**
	 * Getter of highlightPointIndex
	 */
	int getHighlightPointIndex();

	/**
	 * Getter of highlightPointColor
	 */
	GColor getHighlightPointColor();

	/**
	 * Getter of highlightPointSize
	 */
	float getHighlightPointSize();

	/** Getter of {@link #visible} */
	boolean isVisible();

	/** Setter of {@link #visible} */
	void setVisible(boolean visible);

	/** @return the offset applied on x axis for each vertex */
	float getxOffset();

	/** Set the offset to apply on x axis for each vertex */
	void setxOffset(float xOffset);

	/** @return the offset applied on y axis for each vertex */
	float getyOffset();

	/** Set the offset to apply on y axis for each vertex */
	void setyOffset(float yOffset);

	/** @return the offset applied on z axis for each vertex */
	float getzOffset();

	/** Set the offset to apply on z axis for each vertex */
	void setzOffset(float zOffset);

	/** Set x, y and z offsets to 0 */
	void resetOffsets();

	/** Getter of {@link #pointWidth} */
	float getPointWidth();

	/** Setter of {@link #pointWidth} */
	void setPointWidth(float pointWidth);

	/** Update one point coords */
	void updatePoint(int pointIndex, double x, double y, double z);

}