/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lines.impl;

import java.util.ArrayList;
import java.util.List;

import com.jogamp.opengl.GL;

import org.apache.commons.lang.math.IntRange;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.viewer3d.layers.lines.ILineProperties;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Pojo defining all properties of a line
 */
public class LineProperties implements ILineProperties {

	/** Line positions */
	protected VBO vbo;
	/** True when VBO has to be bind */
	protected boolean bindVbo;
	/** Index in VBO of First/last vertice of the line */

	/** Offset to apply on x axis for each vertex */
	protected float xOffset = 0f;
	/** Offset to apply on y axis for each vertex */
	protected float yOffset = 0f;
	/** Offset to apply on z axis for each vertex */
	protected float zOffset = 0f;

	protected IntRange indexRange;
	/** Color of the line */
	protected GColor color;
	/** Width of the line */
	protected float width;
	/** Color of the points */
	protected GColor pointColor;
	/** Width of the points */
	protected float pointWidth;

	/** True when line can be drawn */
	protected boolean visible = true;

	/** Index of the highlighted point */
	protected int highlightPointIndex = -1;
	/** Color of the highlighted point */
	protected GColor highlightPointColor;
	/** Size of the highlighted point */
	protected float highlightPointSize;

	/** Index of the range of highlighted point */
	protected IntRange highlightPointsIndex;
	/** Color of the range of highlighted point */
	protected GColor highlightPointsColor;
	/** Size of the range of highlighted point */
	protected float highlightPointsSize;

	/** Declination of this LineProperties in an other presentation format */
	protected List<SubLineProperties> subLineProperties = new ArrayList<>();

	/**
	 * Constructor
	 */
	public LineProperties(VBO vbo, int firstIndex, int lastIndex, GColor color, float width) {
		this.vbo = vbo;
		bindVbo = true;
		indexRange = new IntRange(firstIndex, lastIndex);
		this.color = color;
		this.width = width;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#highlightPoint(int, fr.ifremer.globe.core.utils.color.GColor,
	 *      float)
	 */
	@Override
	public void highlightPoint(int index, GColor color, float pointSize) {
		if (indexRange.containsInteger(index)) {
			highlightPointIndex = index;
			highlightPointColor = color;
			highlightPointSize = pointSize;
		}
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#highlightPoints(org.apache.commons.lang.math.IntRange,
	 *      fr.ifremer.globe.core.utils.color.GColor, float)
	 */
	@Override
	public void highlightPoints(IntRange pointsIndexes, GColor color, float pointSize) {
		if (indexRange.containsRange(pointsIndexes)) {
			highlightPointsIndex = pointsIndexes;
			highlightPointsColor = color;
			highlightPointsSize = pointSize;
		}
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#unHighlightPoint()
	 */
	@Override
	public void unHighlightPoint() {
		highlightPointIndex = -1;
		highlightPointColor = null;
		highlightPointSize = 0f;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#unHighlightPoints()
	 */
	@Override
	public void unHighlightPoints() {
		highlightPointsIndex = null;
		highlightPointsColor = null;
		highlightPointsSize = 0f;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#split(int, int, fr.ifremer.globe.model.utils.color.Color,
	 *      float)
	 */
	/** {@inheritDoc} */
	@Override
	public void split(int firstIndex, int lastIndex, GColor otherColor, float otherWidth) {
		if (firstIndex <= lastIndex && firstIndex >= 0 && lastIndex <= getLastIndex()) {
			List<SubLineProperties> oldSubLineProperties = new ArrayList<>(subLineProperties);
			if (oldSubLineProperties.isEmpty()) {
				oldSubLineProperties
						.add(new SubLineProperties(getFirstIndex(), getLastIndex(), getColor(), getWidth()));
			}

			List<SubLineProperties> newSubLineProperties = new ArrayList<>();
			IntRange newIndexRange = new IntRange(firstIndex, lastIndex);

			for (SubLineProperties subLineProperties : oldSubLineProperties) {
				if (subLineProperties.getIndexRange().containsRange(newIndexRange)) {
					if (firstIndex > subLineProperties.getFirstIndex()) {
						newSubLineProperties.add(new SubLineProperties(subLineProperties.getFirstIndex(), firstIndex,
								subLineProperties.getColor(), subLineProperties.getWidth()));
					}
					newSubLineProperties.add(new SubLineProperties(firstIndex, lastIndex, otherColor, otherWidth));
					if (lastIndex < getLastIndex()) {
						newSubLineProperties.add(new SubLineProperties(lastIndex + 1, subLineProperties.getLastIndex(),
								subLineProperties.getColor(), subLineProperties.getWidth()));
					}
				} else {
					newSubLineProperties.add(subLineProperties);
				}
			}

			subLineProperties = newSubLineProperties;
		}
	}

	/**
	 * Change coods of one of the points in this line
	 */
	@Override
	public void updatePoint(int index, Vec4 point) {
		vbo.position(index * 3);
		vbo.put(point.x);
		vbo.put(point.y);
		vbo.put(point.z);
		vbo.rewind();
		bindVbo = true;
	}

	/** @return true when this line can be drawn */
	public boolean canBeDrawn(DrawContext dc) {
		return true;
	}

	/**
	 * @return the number of points in this line
	 */
	@Override
	public int getPointCount() {
		return indexRange.getMaximumInteger() - indexRange.getMinimumInteger();
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#canelSplittings()
	 */
	@Override
	public void canelSplittings() {
		subLineProperties.clear();
	}

	/**
	 * Free ressources
	 */
	public void unbind(GL gl) {
		if (vbo != null) {
			vbo.delete(gl);
			bindVbo = true;
		}
	}

	/** Bind the VBO if nedeed */
	public void bind(GL gl) {
		if (bindVbo) {
			vbo.bindAndLoad(gl);
			bindVbo = false;
		}
	}

	/**
	 * Getter of vbo
	 */
	public VBO getVbo() {
		return vbo;
	}

	/**
	 * Returns the coods of one of the points in this line
	 */
	public Vec4 getPoint(int index) {
		vbo.position(index * 3);
		Vec4 result = new Vec4(vbo.get(), vbo.get(), vbo.get());
		vbo.rewind();
		return result;
	}

	/**
	 * Setter of vbo
	 */
	public void setVbo(VBO vbo) {
		this.vbo = vbo;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#getColor()
	 */
	@Override
	public GColor getColor() {
		return color;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#setColor(java.awt.Color)
	 */
	@Override
	public void setColor(GColor color) {
		for (SubLineProperties oneSubLineProperties : subLineProperties) {
			oneSubLineProperties.setColor(color);
		}
		this.color = color;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#getWidth()
	 */
	@Override
	public float getWidth() {
		return width;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#setWidth(float)
	 */
	@Override
	public void setWidth(float width) {
		for (SubLineProperties oneSubLineProperties : subLineProperties) {
			if (oneSubLineProperties.getWidth() == this.width) {
				oneSubLineProperties.setWidth(width);
			}
		}
		this.width = width;
	}

	/**
	 * Getter of firstIndex
	 */
	public int getFirstIndex() {
		return indexRange.getMinimumInteger();
	}

	/**
	 * Getter of lastIndex
	 */
	public int getLastIndex() {
		return indexRange.getMaximumInteger();
	}

	/**
	 * Getter of subLineProperties
	 */
	public List<SubLineProperties> getSubLineProperties() {
		return subLineProperties;
	}

	/**
	 * Getter of pointColor
	 */
	@Override
	public GColor getPointColor() {
		return pointColor;
	}

	/**
	 * Setter of pointColor
	 */
	@Override
	public void setPointColor(GColor pointColor) {
		this.pointColor = pointColor;
	}

	/**
	 * Getter of highlightPointIndex
	 */
	@Override
	public int getHighlightPointIndex() {
		return highlightPointIndex;
	}

	/**
	 * Getter of highlightPointColor
	 */
	@Override
	public GColor getHighlightPointColor() {
		return highlightPointColor;
	}

	/**
	 * Getter of highlightPointSize
	 */
	@Override
	public float getHighlightPointSize() {
		return highlightPointSize;
	}

	/**
	 * Getter of highlightPointsIndex
	 */
	public IntRange getHighlightPointsIndex() {
		return highlightPointsIndex;
	}

	/**
	 * Getter of highlightPointsColor
	 */
	public GColor getHighlightPointsColor() {
		return highlightPointsColor;
	}

	/**
	 * Getter of highlightPointsSize
	 */
	public float getHighlightPointsSize() {
		return highlightPointsSize;
	}

	/** Getter of {@link #visible} */
	@Override
	public boolean isVisible() {
		return visible;
	}

	/** Setter of {@link #visible} */
	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/** Getter of {@link #xOffset} */
	@Override
	public float getxOffset() {
		return xOffset;
	}

	/** Setter of {@link #xOffset} */
	@Override
	public void setxOffset(float xOffset) {
		this.xOffset = xOffset;
	}

	/** Getter of {@link #yOffset} */
	@Override
	public float getyOffset() {
		return yOffset;
	}

	/** Setter of {@link #yOffset} */
	@Override
	public void setyOffset(float yOffset) {
		this.yOffset = yOffset;
	}

	/** Getter of {@link #zOffset} */
	@Override
	public float getzOffset() {
		return zOffset;
	}

	/** Setter of {@link #zOffset} */
	@Override
	public void setzOffset(float zOffset) {
		this.zOffset = zOffset;
	}

	/** {@inheritDoc} */
	@Override
	public void resetOffsets() {
		xOffset = 0f;
		yOffset = 0f;
		zOffset = 0f;
	}

	/** Getter of {@link #pointWidth} */
	@Override
	public float getPointWidth() {
		return pointWidth;
	}

	/** Setter of {@link #pointWidth} */
	@Override
	public void setPointWidth(float pointWidth) {
		this.pointWidth = pointWidth;
	}

	/** {@inheritDoc} */
	@Override
	public void updatePoint(int pointIndex, double x, double y, double z) {
		getVbo().position(pointIndex * 3);
		getVbo().put(x);
		getVbo().put(y);
		getVbo().put(z);
		getVbo().rewind();
	}

}
