/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lines.impl;

import org.apache.commons.lang.math.IntRange;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Pojo defining a subline
 */
public class SubLineProperties {

	protected IntRange indexRange;
	/** Color of the line */
	protected GColor color;
	/** Width of the line */
	protected float width;

	/**
	 * Constructor
	 */
	protected SubLineProperties(int firstIndex, int lastIndex, GColor color, float width) {
		this.indexRange = new IntRange(firstIndex, lastIndex);
		this.color = color;
		this.width = width;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#getColor()
	 */
	public GColor getColor() {
		return color;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#setColor(java.awt.Color)
	 */
	public void setColor(GColor color) {
		this.color = color;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#getWidth()
	 */
	public float getWidth() {
		return width;
	}

	/**
	 * @see fr.ifremer.viewer3d.layers.lines.ILineProperties#setWidth(float)
	 */
	public void setWidth(float width) {
		this.width = width;
	}

	/**
	 * Getter of firstIndex
	 */
	public int getFirstIndex() {
		return indexRange.getMinimumInteger();
	}

	/**
	 * Getter of lastIndex
	 */
	public int getLastIndex() {
		return indexRange.getMaximumInteger();
	}

	/**
	 * Getter of indexRange
	 */
	public IntRange getIndexRange() {
		return indexRange;
	}

}
