/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.lines;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;

import javax.swing.SwingUtilities;

import org.apache.commons.lang.math.IntRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.DoubleIndexedSupplier;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.lines.impl.LineProperties;
import fr.ifremer.viewer3d.layers.lines.impl.SubLineProperties;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.SceneController;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Layer to render simple lines.
 */
public class LinesLayer extends AbstractLayer implements ITarget, GLComponent {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(LinesLayer.class);

	/** Shader programs source code */
	protected static final String SHADER_VERT = "/shader/linesLayer.vert";
	protected static final String SHADER_FRAG = "/shader/constantColor.frag";

	/** Sector of rendering. */
	protected Sector sector;
	/** Supplier return true to fit all points on surface. */
	protected BooleanSupplier followSurface = () -> false;

	/** All managed lines. */
	protected List<LineProperties> lines = new ArrayList<>();
	/** Shader programs used by this renderer */
	protected int shaderProgram = 0;
	/** Used to save/restore OpenGL ES state */
	protected float[] lineSizeToRestore = new float[1];
	/** Used to save/restore OpenGL ES state */
	protected float[] pointSizeToRestore = new float[1];

	/**
	 * Constructor
	 */
	public LinesLayer(String name, Sector sector) {
		setName(name);
		setPickEnabled(false);
		this.sector = sector;
	}

	/**
	 * Constructor
	 */
	public LinesLayer(String name, GeoBox geoBox) {
		setName(name);
		setPickEnabled(false);
		sector = Sector.fromDegrees(geoBox.getBottom(), geoBox.getTop(), geoBox.getLeft(), geoBox.getRight());
	}

	/**
	 * Add one line
	 *
	 * @return the pojo representing the line or null if the line was rejected
	 */
	public ILineProperties acceptLine(int pointCount, DoubleIndexedSupplier xSupplier, DoubleIndexedSupplier ySupplier,
			DoubleIndexedSupplier zSupplier, GColor color, float width) {
		LineProperties result = null;
		if (pointCount > 0) {
			VBO vbo = new VBO(3);
			vbo.allocateBuffer(pointCount);
			for (int index = 0; index < pointCount; index++) {
				vbo.put(xSupplier.getAsDouble(index));
				vbo.put(ySupplier.getAsDouble(index));
				vbo.put(zSupplier.getAsDouble(index));
			}
			vbo.rewind();

			result = newLineProperties(vbo, 0, pointCount - 1, color, width);
			lines.add(result);
		}
		return result;
	}

	/**
	 * Remove one line
	 */
	public void removeLine(ILineProperties iLineProperties) {
		List<LineProperties> newLines = new ArrayList<>(lines);
		int index = newLines.indexOf(iLineProperties);
		if (index >= 0) {
			dispose(newLines.remove(index));
			lines = newLines;
		}
	}

	/**
	 * Remove all lines
	 */
	public void removeAllLines() {
		List<LineProperties> oldLines = lines;
		oldLines.forEach(this::dispose);
		lines = new ArrayList<>();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.ui.service.worldwind.ITarget#getSector()
	 */
	@Override
	public Sector getSector() {
		return sector;
	}

	/**********************************************************************************************************
	 * DRAW METHODS
	 **********************************************************************************************************/

	/**
	 * Draws in 3D Viewer
	 *
	 * @see gov.nasa.worldwind.layers.AbstractLayer#doRender(gov.nasa.worldwind.render.DrawContext)
	 */
	@Override
	protected void doRender(DrawContext dc) {
		if (!dc.isPickingMode())
			doRender(dc.getGL().getGL2(), 1);// DOES NOT WORK : see draw method (float) dc.getVerticalExaggeration());
	}

	/**
	 * Dispose OpenGL resources
	 */
	@Override
	public void dispose(GLAutoDrawable glad) {
		/* not used */
	}

	/**
	 * Draws in 2D openGL scene
	 */
	@Override
	public void draw(GLAutoDrawable glad) {
		if (isVisible())
			doRender(glad.getGL().getGL2(), 0f);
	}

	private void doRender(GL2 gl, float verticalExaggeration) {
		try {
			beginDrawing(gl);
			drawLines(gl, verticalExaggeration);
		} catch (GIOException | GLException e) {
			logger.error("Unable to draw lines layer {}", e.getMessage());
		} finally {
			endDrawing(gl);
		}
	}

	/**
	 * Before drawing
	 */
	protected void beginDrawing(GL2 gl) {
		gl.glPushAttrib(GL.GL_DEPTH_BUFFER_BIT | GL.GL_BLEND);
		gl.glGetFloatv(GL.GL_LINE_WIDTH, lineSizeToRestore, 0);
		gl.glGetFloatv(GL.GL_POINT_SIZE, pointSizeToRestore, 0);

		// For opacity
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

	/**
	 * Restore drawing state changed in beginDrawing to the default.
	 */
	protected void endDrawing(GL2 gl) {
		gl.glPopAttrib();
		gl.glLineWidth(lineSizeToRestore[0]);
		gl.glPointSize(pointSizeToRestore[0]);
	}

	protected void drawLines(GL2 gl, float verticalExaggeration) throws GIOException {
		// Get the shader program ID for the future drawing
		int shaderProgramID = getShaderProgramID(gl);
		// Initialize the uniforms of the shader program
		gl.glUseProgram(shaderProgramID);
		// Bind VBOs
		lines.forEach(line -> line.bind(gl));
		// Draw the OpenGL shape from the initialized buffers
		draw(gl, shaderProgramID, verticalExaggeration);
		// Free memory
		lines.forEach(line -> line.unbind(gl));

		// Returns to the main shader program
		if (shaderProgramID != 0) {
			gl.glUseProgram(0);
		}
	}

	/**
	 * Configure all the OpenGL drawing variables.
	 *
	 * @param gl OpenGL environment context
	 * @param shaderProgram id of the shader program to draw
	 */
	protected void draw(GL2 gl, int shaderProgram, float verticalExaggerationDc) {
		// Disable GL_DETPH_TEST to avoid line being hidden
		gl.glDisable(GL.GL_DEPTH_TEST);
		// Initialize the pointer to the vertices buffer
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);

		// 0 to fit points on surface when elevations are disabled
		// TODO DOES NOT WORK ANYMORE : this code was implemented before, when a translation to a local plan repair was
		// used. Now, to fix bug and get a better precision, the current coordinate system is the global of 3DViewer.
		// So, a simple multiplication to apply the vertical exaggeration does not work!
		// float verticalExaggeration = followSurface.getAsBoolean() ? 0f : verticalExaggerationDc;
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "verticalExaggeration"), verticalExaggerationDc);// verticalExaggeration);

		// Opacity
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), (float) getOpacity());
		draw(gl);

		gl.glEnable(GL.GL_DEPTH_TEST);
		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
	}

	/** Draw all lines and points */
	protected void draw(GL2 gl) {
		for (LineProperties lineProperties : lines) {
			if (lineProperties.isVisible()) {
				// Offsets
				gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "xOffset"), lineProperties.getxOffset());
				gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "yOffset"), lineProperties.getyOffset());
				gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "zOffset"), lineProperties.getzOffset());

				drawLine(gl, lineProperties);
				drawPoints(gl, lineProperties);
			}
		}
	}

	/** Draw one line */
	protected void drawLine(GL2 gl, LineProperties lineProperties) {
		VBO vbo = lineProperties.getVbo();
		List<SubLineProperties> subLineProperties = lineProperties.getSubLineProperties();
		if (subLineProperties.isEmpty()) {
			drawLine(gl, vbo, lineProperties.getColor(), lineProperties.getWidth(), lineProperties.getFirstIndex(),
					lineProperties.getLastIndex());
		} else {
			subLineProperties.forEach(subLine -> drawLine(gl, vbo, subLine.getColor(), subLine.getWidth(),
					subLine.getFirstIndex(), subLine.getLastIndex()));
		}
	}

	/** Draw one line */
	protected void drawLine(GL2 gl, VBO vbo, GColor color, float size, int firstIndex, int lastIndex) {
		if (vbo.getBufferName() != null && lastIndex > firstIndex && color.getAlpha() > 0f) {
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo.getBufferName()[0]);
			gl.glVertexPointer(vbo.getElementSize(), GL.GL_FLOAT, 0, 0);

			// draw the line
			gl.glUniform4f(gl.glGetUniformLocation(shaderProgram, "color"), color.getFloatRed(), color.getFloatGreen(),
					color.getFloatBlue(), color.getAlpha());
			gl.glLineWidth(computeWidth(size));
			gl.glDrawArrays(GL.GL_LINE_STRIP, firstIndex, lastIndex - firstIndex + 1); // last index included

			// Initialize buffer
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		}
	}

	/** Compute point and line size according the eye altitude */
	protected float computeWidth(float size) {
		/*
		 * Compute size in function of altitude : disabled (not suitable for 2D rendering)
		 *
		 * double altitude = Viewer3D.getWwd().getView().getEyePosition().getAltitude(); if (altitude < 200e3) size =
		 * Math.max(0.8f * size, 1f); else if (altitude < 400e3) size = Math.max(0.6f * size, 1f); else if (altitude <
		 * 700e3) size = Math.max(0.4f * size, 1f); else if (altitude < 1e6) size = Math.max(0.2f * size, 1f); else size
		 * = Math.max(0.1f * size, 1f);
		 */
		return size;
	}

	/** Draw points on one line */
	protected void drawPoints(GL2 gl, LineProperties lineProperties) {
		VBO vbo = lineProperties.getVbo();
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo.getBufferName()[0]);
		gl.glVertexPointer(vbo.getElementSize(), GL.GL_FLOAT, 0, 0);

		// draw the points
		GColor pointColor = lineProperties.getPointColor();
		if (pointColor != null) {
			gl.glUniform4f(gl.glGetUniformLocation(shaderProgram, "color"), pointColor.getFloatRed(),
					pointColor.getFloatGreen(), pointColor.getFloatBlue(), pointColor.getAlpha());
			gl.glPointSize(computeWidth(lineProperties.getPointWidth()));
			gl.glDrawArrays(GL.GL_POINTS, lineProperties.getFirstIndex(),
					lineProperties.getLastIndex() - lineProperties.getFirstIndex());
		}

		// draw the highlighted single point
		IntRange highlightPointsIndex = lineProperties.getHighlightPointsIndex();
		if (highlightPointsIndex != null) {
			GColor highlightPointColor = lineProperties.getHighlightPointsColor();
			gl.glUniform4f(gl.glGetUniformLocation(shaderProgram, "color"), highlightPointColor.getFloatRed(),
					highlightPointColor.getFloatGreen(), highlightPointColor.getFloatBlue(),
					highlightPointColor.getAlpha());
			gl.glPointSize(computeWidth(lineProperties.getHighlightPointsSize()));
			gl.glDrawArrays(GL.GL_POINTS, highlightPointsIndex.getMinimumInteger(),
					highlightPointsIndex.getMaximumInteger() - highlightPointsIndex.getMinimumInteger() + 1);
		}

		// draw the highlighted single point
		int highlightPointIndex = lineProperties.getHighlightPointIndex();
		if (highlightPointIndex >= 0) {
			GColor highlightPointColor = lineProperties.getHighlightPointColor();
			gl.glUniform4f(gl.glGetUniformLocation(shaderProgram, "color"), highlightPointColor.getFloatRed(),
					highlightPointColor.getFloatGreen(), highlightPointColor.getFloatBlue(),
					highlightPointColor.getAlpha());
			gl.glPointSize(computeWidth(lineProperties.getHighlightPointSize()));
			gl.glDrawArrays(GL.GL_POINTS, highlightPointIndex, 1);
		}

		// Initialize buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
	}

	/**
	 * Get the ID of the shader program used by this renderer.
	 */
	protected int getShaderProgramID(GL2 gl) throws GIOException {
		if (shaderProgram == 0) {
			// Compile and generate a shader program ID and add it to the shader prodram renderer list
			shaderProgram = ShaderUtil.createShader(this, gl, SHADER_VERT, SHADER_FRAG);
		}

		return shaderProgram;
	}

	/**
	 * @see gov.nasa.worldwind.layers.AbstractLayer#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();

		SwingUtilities.invokeLater(() -> {
			GLContext glContext = getGLContext();
			if (glContext != null) {
				if (shaderProgram != 0) {
					glContext.getGL().getGL2().glDeleteShader(shaderProgram);
					shaderProgram = 0;
				}
				glContext.release();
			}
			lines = new ArrayList<>();
		});
	}

	/**
	 * Free OpenGl ressources
	 */
	protected void dispose(LineProperties lineProperties) {
		if (lineProperties != null && lineProperties.getVbo() != null
				&& lineProperties.getVbo().getBufferName() != null) {
			SwingUtilities.invokeLater(() -> {
				GLContext glContext = getGLContext();
				if (glContext != null) {
					lineProperties.getVbo().delete(glContext.getGL());
				}
				glContext.release();
			});
		}
	}

	/**
	 * @return the <code>DrawContext</code>s </code>com.jogamp.opengl.GLContext</code>.
	 */
	protected GLContext getGLContext() {
		GLContext result = null;
		SceneController sceneController = Viewer3D.isUp() ? Viewer3D.getWwd().getSceneController() : null;
		if (sceneController != null) {
			DrawContext drawContext = sceneController.getDrawContext();
			if (drawContext != null) {
				GLContext glContext = drawContext.getGLContext();
				if (glContext != null && glContext.makeCurrent() != GLContext.CONTEXT_NOT_CURRENT) {
					result = glContext;
				}
			}
		}
		return result;

	}

	/** Subclass may redefine this method to bring their own definition of LineProperties */
	protected LineProperties newLineProperties(VBO vbo, int firstIndex, int lastIndex, GColor color, float width) {
		return new LineProperties(vbo, firstIndex, lastIndex, color, width);
	}

	/** Getter of {@link #followSurface} */
	public BooleanSupplier getFollowSurface() {
		return followSurface;
	}

	/** Setter of {@link #followSurface} */
	public void setFollowSurface(BooleanSupplier followSurface) {
		this.followSurface = followSurface;
	}

	@Override
	public boolean isVisible() {
		return isEnabled();
	}

}
