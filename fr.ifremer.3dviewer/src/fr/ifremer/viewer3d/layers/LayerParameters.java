package fr.ifremer.viewer3d.layers;

import java.beans.PropertyChangeSupport;

/**
 * Interface used by layer declaring parameters. Parameters are saved in session.xml and re-injected when layer are
 * recreated
 */
public interface LayerParameters {

	/**
	 * check if the layer should be displayed (use to set initial display state)
	 */
	boolean getVisibility();

	/**
	 * set the initial visibility state
	 */
	void setVisibility(boolean visibility);

	/**
	 * Method is named gain... (not get...) to avoid its storage in the session (not a JavaBean property).
	 *
	 * @return the {@link PropertyChangeSupport}
	 */
	PropertyChangeSupport gainPropertyChangeSupport();

	/**
	 * Minimal implementation of the <code>LayerParameters</code> interface.
	 */
	public abstract class AbstractLayerParameters implements LayerParameters {

		/** Mark transient to avoid GSon serialization */
		protected transient PropertyChangeSupport pcs = new PropertyChangeSupport(this);

		/** Visibility property */
		protected boolean visibility = true;

		/** {@inheritDoc} */
		@Override
		public boolean getVisibility() {
			return visibility;
		}

		/** {@inheritDoc} */
		@Override
		public void setVisibility(boolean visibility) {
			pcs.firePropertyChange("visibility", this.visibility, this.visibility = visibility);
		}

		/** {@inheritDoc} */
		@Override
		public PropertyChangeSupport gainPropertyChangeSupport() {
			return pcs;
		}
	}
}
