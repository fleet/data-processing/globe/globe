/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.current;

import java.util.Optional;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;

import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.current.WWCurrentLayerParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.sync.AbstractSynchronizedValueComputer;

/**
 * Synchronize the parameters of IWWCurrentLayer layers.
 */
public class WWCurrentLayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWCurrentLayer, WWCurrentLayerParameters> {

	/**
	 * Constructor
	 */
	public WWCurrentLayerSynchronizer() {
		super(IWWCurrentLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWCurrentLayer layer, WWCurrentLayerParameters currentLayerParameters) {
		layer.setParameters(currentLayerParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<WWCurrentLayerParameters> makeLayerParameters(IWWCurrentLayer layer) {
		return Optional.of(layer.getParameters());
	}

	/**
	 * No distinction among polygons, they are all synced together
	 */
	@Override
	protected String inferType(IWWCurrentLayer layer) {
		return IWWCurrentLayer.class.getSimpleName();
	}

	/**
	 * Compute the min and max texture values over the layers synchronized with the specified layer
	 */
	public Optional<Range> getMinMaxSyncValues(IWWCurrentLayer layer) {
		return new MinMaxSyncValuesComputer().compute(layer);
	}

	/**
	 * An AbstractSynchronizedValueComputer to compute the min and max texture values over the layers synchronized with
	 * one specified layer
	 */
	class MinMaxSyncValuesComputer
			extends AbstractSynchronizedValueComputer<IWWCurrentLayer, WWCurrentLayerParameters, Range> {
		/**
		 * Constructor
		 */
		protected MinMaxSyncValuesComputer() {
			super(WWCurrentLayerSynchronizer.this);
		}

		/** {@inheritDoc} */
		@Override
		protected Range extractResultFromParameter(WWCurrentLayerParameters p) {
			// Compute initial contrast minValue and maxValue
			// See WWNavigationLayer.load :
			// LimitMin = minValue - 0.25f * (maxValue - minValue)
			// LimitMax = maxValue + 0.25f * (maxValue - minValue)
			var result = new DoubleRange(p.colorContrastModel().contrastMin(), p.colorContrastModel().contrastMax());
			if (p.metadataUsedToGetColor().isPresent()) {
				double limitMin = p.colorContrastModel().limitMin();
				double limitMax = p.colorContrastModel().limitMax();
				double minValue = (20d * limitMin + 4d * limitMax) / 24d;
				double maxValue = 5d * minValue - 4d * limitMin;
				result = new DoubleRange(minValue, maxValue);
			}

			return result;
		}

		/** {@inheritDoc} */
		@Override
		protected Range cumulateResultOfParameters(Range p1, Range p2) {
			if(!Float.isFinite(p1.getMinimumFloat()) || !Float.isFinite(p1.getMaximumFloat()))
				return p2;
			else if (!Float.isFinite(p2.getMinimumFloat()) || !Float.isFinite(p2.getMaximumFloat()))
				return p1;
			
			return new FloatRange(Math.min(p1.getMinimumFloat(), p2.getMinimumFloat()),
					Math.max(p1.getMaximumFloat(), p2.getMaximumFloat()));
		}
	}

}
