/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.dtm;

import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainWithPaletteParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;

/**
 * Synchronize the display parameters of IWWTerrainWithPaletteLayer layers.
 */
public class WWTerrainLayerWithPaletteSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWTerrainWithPaletteLayer, TerrainWithPaletteParameters> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(WWTerrainLayerWithPaletteSynchronizer.class);

	/**
	 * Constructor
	 */
	public WWTerrainLayerWithPaletteSynchronizer() {
		super(IWWTerrainWithPaletteLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWTerrainWithPaletteLayer layer,
			TerrainWithPaletteParameters textureParameters) {
		layer.setTextureParameters(textureParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<TerrainWithPaletteParameters> makeLayerParameters(IWWTerrainWithPaletteLayer layer) {
		return Optional.of(layer.getTextureParameters());
	}

	/**
	 * Infer the type expected by the synchronizer when handling a kind of raster
	 *
	 * @param kindOfRaster is one the RasterInfo.RASTER_* constant
	 */
	@Override
	protected String inferType(IWWTerrainWithPaletteLayer layer) {
		AColorPalette palette = layer.getTextureParameters().palette();
		return IWWTerrainWithPaletteLayer.class.getSimpleName() + "_" + palette.getType();
	}

	/**
	 * Compute the min and max texture values over the layers synchronized with the specified layer
	 */
	public Optional<Range> getMinMaxSyncValues(IWWTerrainWithPaletteLayer layer) {
		var p = layer.getTextureParameters();
		return Optional.of(new FloatRange(p.palette().getMinValue(), p.palette().getMaxValue()));
	}
}
