/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.dtm;

import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.sync.AbstractSynchronizedValueComputer;

/**
 * Synchronize the display parameters of IWWDtmLayer layers.
 */
public class WWTerrainLayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWTerrainLayer, TerrainParameters> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(WWTerrainLayerSynchronizer.class);

	/**
	 * Constructor
	 */
	public WWTerrainLayerSynchronizer() {
		super(IWWTerrainLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWTerrainLayer layer, TerrainParameters textureParameters) {
		layer.setTextureParameters(textureParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<TerrainParameters> makeLayerParameters(IWWTerrainLayer layer) {
		return Optional.of(layer.getTextureParameters());
	}

	/**
	 * Infer the type expected by the synchronizer when handling a kind of raster
	 *
	 * @param kindOfRaster is one the RasterInfo.RASTER_* constant
	 */
	@Override
	protected String inferType(IWWTerrainLayer layer) {
		String result = IWWTerrainLayer.class.getSimpleName();
		String kindOfRaster = layer.getType();
		if (kindOfRaster.equals("Bathymetry"))
			return result + "_" + RasterInfo.RASTER_ELEVATION;
		else
			return result + "_" + kindOfRaster;
	}

	/**
	 * Compute the min and max texture values over the layers synchronized with the specified layer
	 */
	public Optional<Range> getMinMaxSyncValues(IWWTerrainLayer layer) {
		return new MinMaxSyncValuesComputer().compute(layer);
	}

	/**
	 * An AbstractSynchronizedValueComputer to compute the min and max texture values over the layers synchronized with
	 * one specified layer
	 */
	class MinMaxSyncValuesComputer
			extends AbstractSynchronizedValueComputer<IWWTerrainLayer, TerrainParameters, Range> {
		/**
		 * Constructor
		 */
		protected MinMaxSyncValuesComputer() {
			super(WWTerrainLayerSynchronizer.this);
		}

		/** {@inheritDoc} */
		@Override
		protected Range extractResultFromParameter(TerrainParameters p) {
			return new FloatRange(p.minTextureValue, p.maxTextureValue);
		}

		/** {@inheritDoc} */
		@Override
		protected Range cumulateResultOfParameters(Range p1, Range p2) {
			if(!Float.isFinite(p1.getMinimumFloat()) || !Float.isFinite(p1.getMaximumFloat()))
				return p2;
			else if (!Float.isFinite(p2.getMinimumFloat()) || !Float.isFinite(p2.getMaximumFloat()))
				return p1;
			
			return new FloatRange(Math.min(p1.getMinimumFloat(), p2.getMinimumFloat()),
					Math.max(p1.getMaximumFloat(), p2.getMaximumFloat()));
		}
	}

}
