/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync;

import java.util.Optional;
import java.util.stream.Stream;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer.SynchronizationOption;

/**
 * Abstract class used to factorize a way to calculate a result over a set of synchronized layers. <br>
 * <ul>
 * <li>First of all, search all layers T synchronized with the one specified in compute().</li>
 * <li>then retrieve the parameters P of each layer.</li>
 * <li>compute a result R on each parameter P.</li>
 * <li>cumulate all the results R in a single one. This is the result R of the algo.</li>
 * </ul>
 *
 * @param <T> Kind of layer managed by the synchronizer
 * @param <P> Kind of parameter synchronized over layers
 * @param <R> Type of the result of the computation
 */
public abstract class AbstractSynchronizedValueComputer<T extends IWWLayer, P, R> {

	protected final AbstractLayerParametersSynchronizer<T, P> synchronizer;

	/**
	 * Constructor
	 */
	protected AbstractSynchronizedValueComputer(AbstractLayerParametersSynchronizer<T, P> synchronizer) {
		this.synchronizer = synchronizer;
	}

	/**
	 * Main method
	 */
	public Optional<R> compute(T layer) {
		Optional<R> result = Optional.empty();
		var syncOption = synchronizer.getSynchronizationOption(layer);
		if (syncOption == SynchronizationOption.APPLY_TO_ALL) {
			String layerType = synchronizer.inferType(layer);
			result = computeForLayers(synchronizer.getAllSynchronizedLayers(layerType));
		} else if (syncOption == SynchronizationOption.APPLY_TO_GROUP) {
			GroupNode groupNode = synchronizer.inferGroup(layer);
			if (groupNode != null) {
				String layerType = synchronizer.inferType(layer);
				result = computeForLayers(synchronizer.getGroupedSynchronizedLayers(groupNode, layerType));
			}
		}
		return result;
	}

	/** Delegates the actual calculation on the set of identified layers */
	protected Optional<R> computeForLayers(Stream<T> allSynchronizedLayers) {
		return allSynchronizedLayers.map(synchronizer::makeLayerParameters)//
				.filter(Optional::isPresent).map(Optional::get) //
				.map(this::extractResultFromParameter) //
				.reduce(this::cumulateResultOfParameters);
	}

	/** the subclass is responsible for extracting the return value for the given parameter */
	protected abstract R extractResultFromParameter(P p);

	/** the subclass is responsible for cumulating the return values of all parameters */
	protected abstract R cumulateResultOfParameters(R p1, R p2);

}
