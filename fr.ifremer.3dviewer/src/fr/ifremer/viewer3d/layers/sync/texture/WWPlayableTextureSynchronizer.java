/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.texture;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;

/**
 * Synchronize the display parameters of IWWPlayableTextureLayer layers.
 */
public class WWPlayableTextureSynchronizer extends AbstractTextureSynchronizer<IWWPlayableTextureLayer> {
	/**
	 * Constructor
	 */
	public WWPlayableTextureSynchronizer() {
		super(IWWPlayableTextureLayer.class);
	}

	/**
	 * @return the synchronization type. All layers of class IWWPlayableTextureLayer are synchronized
	 */
	@Override
	protected String inferType(IWWPlayableTextureLayer layer) {
		String result = IWWPlayableTextureLayer.class.getSimpleName();
		IWWTextureData textureData = layer.getTextureData();
		if (textureData != null && textureData.getMetadata().containsKey(IWWTextureData.DATA_TYPE_METADATA_KEY))
			result += "/" + layer.getTextureData().getMetadata().get(IWWTextureData.DATA_TYPE_METADATA_KEY);
		return result;
	}

}