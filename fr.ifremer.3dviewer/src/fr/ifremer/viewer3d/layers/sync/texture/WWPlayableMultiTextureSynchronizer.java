/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.texture;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;

/**
 * Synchronize the display parameters of IWWPlayableMultiTextureLayer layers.
 */
public class WWPlayableMultiTextureSynchronizer extends AbstractTextureSynchronizer<IWWPlayableMultiTextureLayer> {
	/**
	 * Constructor
	 */
	public WWPlayableMultiTextureSynchronizer() {
		super(IWWPlayableMultiTextureLayer.class);
	}

	/**
	 * @return the synchronization type. Only layers of class IWWPlayableMultiTextureLayer displaying the same data type
	 *         will be synchronized
	 */
	@Override
	protected String inferType(IWWPlayableMultiTextureLayer layer) {
		return IWWPlayableMultiTextureLayer.class.getSimpleName() + "/" + layer.getSelectedDataLayer();
	}

}