/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.texture;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;

/**
 * Synchronize the display parameters of IWWComputableTextureLayer layers.
 */
public class WWComputableTextureSynchronizer extends AbstractTextureSynchronizer<IWWComputableTextureLayer> {
	/**
	 * Constructor
	 */
	public WWComputableTextureSynchronizer() {
		super(IWWComputableTextureLayer.class);
	}

	/**
	 * @return the synchronization type. All layers of class IWWComputableTextureLayer are synchronized
	 */
	@Override
	protected String inferType(IWWComputableTextureLayer layer) {
		return IWWComputableTextureLayer.class.getSimpleName();
	}

}