/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.texture;

import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.sync.AbstractSynchronizedValueComputer;

/**
 * Synchronize the display parameters of IWWTextureLayer layers.
 */
public abstract class AbstractTextureSynchronizer<T extends IWWTextureLayer>
		extends AbstractLayerParametersSynchronizer<T, WWTextureDisplayParameters> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(AbstractTextureSynchronizer.class);

	/**
	 * Constructor
	 */
	AbstractTextureSynchronizer(Class<T> textureLayerClass) {
		super(textureLayerClass);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWTextureLayer layer, WWTextureDisplayParameters layerParameters) {
		layer.setDisplayParameters(layerParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<WWTextureDisplayParameters> makeLayerParameters(IWWTextureLayer layer) {
		WWTextureDisplayParameters source = layer.getDisplayParameters();
		return Optional.of(source.withOpacity((float) layer.getOpacity()));
	}

	/**
	 * Compute the min and max texture values over the layers synchronized with the specified layer
	 */
	public Optional<Range> getMinMaxSyncValues(T layer) {
		return new MinMaxSyncValuesComputer().compute(layer);
	}

	/**
	 * An AbstractSynchronizedValueComputer to compute the min and max texture values over the layers synchronized with
	 * one specified layer
	 */
	class MinMaxSyncValuesComputer extends AbstractSynchronizedValueComputer<T, WWTextureDisplayParameters, Range> {
		/**
		 * Constructor
		 */
		protected MinMaxSyncValuesComputer() {
			super(AbstractTextureSynchronizer.this);
		}

		/** {@inheritDoc} */
		@Override
		protected Range extractResultFromParameter(WWTextureDisplayParameters p) {
			return new FloatRange(p.getInitialMinContrast(), p.getInitialMaxContrast());
		}

		/** {@inheritDoc} */
		@Override
		protected Range cumulateResultOfParameters(Range p1, Range p2) {
			return new FloatRange(Math.min(p1.getMinimumFloat(), p2.getMinimumFloat()),
					Math.max(p1.getMaximumFloat(), p2.getMaximumFloat()));
		}
	}
}
