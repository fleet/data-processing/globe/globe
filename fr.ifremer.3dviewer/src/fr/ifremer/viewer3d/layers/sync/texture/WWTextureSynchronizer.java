/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.texture;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;

/**
 * Synchronize the display parameters of IWWTextureLayer layers.
 */
public class WWTextureSynchronizer extends AbstractTextureSynchronizer<IWWTextureLayer> {
	/**
	 * Constructor
	 */
	public WWTextureSynchronizer() {
		super(IWWTextureLayer.class);
	}

	/**
	 * @return the synchronization type. All layers of class IWWTextureLayer are synchronized
	 */
	@Override
	protected String inferType(IWWTextureLayer layer) {
		return IWWComputableTextureLayer.class.getSimpleName();
	}

}