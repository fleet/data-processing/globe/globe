/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.texture;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;

/**
 * Synchronize the display parameters of IWWMultiTextureLayer layers.
 */
public class WWMultiTextureSynchronizer extends AbstractTextureSynchronizer<IWWMultiTextureLayer> {
	/**
	 * Constructor
	 */
	public WWMultiTextureSynchronizer() {
		super(IWWMultiTextureLayer.class);
	}

	/**
	 * @return the synchronization type. Only layers of class IWWMultiTextureLayer displaying the same data type will be
	 *         synchronized
	 */
	@Override
	protected String inferType(IWWMultiTextureLayer layer) {
		return IWWPlayableMultiTextureLayer.class.getSimpleName() + "/" + layer.getSelectedDataLayer();
	}

}