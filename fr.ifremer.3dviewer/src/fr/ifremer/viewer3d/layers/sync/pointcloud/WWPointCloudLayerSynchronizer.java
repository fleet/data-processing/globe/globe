/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.pointcloud;

import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.PointCloudParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.sync.AbstractSynchronizedValueComputer;

/**
 * Synchronize the display parameters of IWWPointCloudLayer layers.
 */
public class WWPointCloudLayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWPointCloudLayer, PointCloudParameters> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(WWPointCloudLayerSynchronizer.class);

	/**
	 * Constructor
	 */
	public WWPointCloudLayerSynchronizer() {
		super(IWWPointCloudLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWPointCloudLayer layer, PointCloudParameters pointCloudParameters) {
		layer.setParameters(pointCloudParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<PointCloudParameters> makeLayerParameters(IWWPointCloudLayer layer) {
		return Optional.of(layer.getParameters());
	}

	/**
	 * Infer the type expected by the synchronizer when handling a kind of point cloud's field
	 */
	@Override
	protected String inferType(IWWPointCloudLayer layer) {
		String result = IWWPointCloudLayer.class.getSimpleName();
		PointCloudFieldInfo fieldInfo = layer.getFieldInfo();
		return result + (fieldInfo != null ? "_" + fieldInfo.name() : "");
	}

	/**
	 * Compute the min and max contrast values over the layers synchronized with the specified layer
	 */
	public Optional<Range> getMinMaxSyncValues(IWWPointCloudLayer layer) {
		return new MinMaxSyncValuesComputer().compute(layer);
	}

	/**
	 * An AbstractSynchronizedValueComputer to compute the min and max contrast values over the layers synchronized with
	 * one specified layer
	 */
	class MinMaxSyncValuesComputer
			extends AbstractSynchronizedValueComputer<IWWPointCloudLayer, PointCloudParameters, Range> {
		/**
		 * Constructor
		 */
		protected MinMaxSyncValuesComputer() {
			super(WWPointCloudLayerSynchronizer.this);
		}

		/** {@inheritDoc} */
		@Override
		protected Range extractResultFromParameter(PointCloudParameters p) {
			return new FloatRange(p.minContrast(), p.maxContrast());
		}

		/** {@inheritDoc} */
		@Override
		protected Range cumulateResultOfParameters(Range p1, Range p2) {
			if(!Float.isFinite(p1.getMinimumFloat()) || !Float.isFinite(p1.getMaximumFloat()))
				return p2;
			else if (!Float.isFinite(p2.getMinimumFloat()) || !Float.isFinite(p2.getMaximumFloat()))
				return p1;
			
			return new FloatRange(Math.min(p1.getMinimumFloat(), p2.getMinimumFloat()),
					Math.max(p1.getMaximumFloat(), p2.getMaximumFloat()));
		}
	}

}
