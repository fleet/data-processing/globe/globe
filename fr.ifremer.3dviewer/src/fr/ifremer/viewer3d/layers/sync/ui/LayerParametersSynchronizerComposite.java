/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.ui;

import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer.SynchronizationOption;

/**
 * Group containing the buttons to swith the synchronization mode for a layer.
 */
public class LayerParametersSynchronizerComposite<T extends IWWLayer> extends Group {

	/** The layer parameters synchronizer **/
	protected AbstractLayerParametersSynchronizer<T, ?> layersSynchronizer;

	/** Considered layer */
	protected T layer;

	protected Button btnApplyToAll;
	protected Button btnApplyToGroup;
	protected Button btnApplyToFile;

	/** Notify this instance when synchronization option changed */
	protected Optional<Consumer<T>> synchronizationChangedListener = Optional.empty();

	/**
	 * Constructor
	 *
	 * @wbp.parser.constructor
	 */
	protected LayerParametersSynchronizerComposite(Composite parent) {
		super(parent, SWT.NONE);

		setLayout(new GridLayout(4, false));

		Label labelApply = new Label(this, SWT.NONE);
		labelApply.setText("Apply to : ");

		btnApplyToAll = new Button(this, SWT.RADIO);
		btnApplyToAll.setText("All");
		btnApplyToAll.addListener(SWT.Selection, e -> {
			if (btnApplyToAll.getSelection() && layersSynchronizer != null) {
				layersSynchronizer.activateSynchronizationForAll(layer);
				synchronizationChangedListener.ifPresent(l -> l.accept(layer));
			}
		});

		btnApplyToGroup = new Button(this, SWT.RADIO);
		btnApplyToGroup.setText("Group");
		btnApplyToGroup.addListener(SWT.Selection, e -> {
			if (btnApplyToGroup.getSelection() && layersSynchronizer != null) {
				layersSynchronizer.activateSynchronizationForGroup(layer);
				synchronizationChangedListener.ifPresent(l -> l.accept(layer));
			}
		});

		btnApplyToFile = new Button(this, SWT.RADIO);
		btnApplyToFile.setText("None");
		btnApplyToFile.addListener(SWT.Selection, e -> {
			if (btnApplyToFile.getSelection() && layersSynchronizer != null) {
				layersSynchronizer.desactivateSynchronization(layer);
				synchronizationChangedListener.ifPresent(l -> l.accept(layer));
			}
		});
		setText("Synchronization with other layers");
	}

	/**
	 * Constructor
	 */
	public LayerParametersSynchronizerComposite(Composite parent,
			AbstractLayerParametersSynchronizer<T, ?> layersSynchronizer, T layer) {
		this(parent);

		this.layersSynchronizer = layersSynchronizer;
		this.layer = layer;
	}

	/**
	 * @return the {@link #synchronizationOption}
	 */
	public SynchronizationOption getSynchronizatonOption() {
		return layersSynchronizer.getSynchronizationOption(layer);
	}

	/** Layer has changed... */
	public void adaptToLayer(T layer) {
		this.layer = layer;
		SynchronizationOption synchronizationOption = getSynchronizatonOption();
		if (synchronizationOption == SynchronizationOption.NOT_SET) {
			synchronizationOption = SynchronizationOption.NONE;
		}
		btnApplyToAll.setSelection(synchronizationOption == SynchronizationOption.APPLY_TO_ALL);
		btnApplyToGroup.setSelection(synchronizationOption == SynchronizationOption.APPLY_TO_GROUP);
		btnApplyToFile.setSelection(synchronizationOption == SynchronizationOption.NONE);

	}

	/** {@inheritDoc} */
	@Override
	protected void checkSubclass() {
		// I want to subclass !
	}

	public void setSynchronizationChangedListener(Consumer<T> synchronizationChangedListener) {
		this.synchronizationChangedListener = Optional.ofNullable(synchronizationChangedListener);
	}

}
