/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.navigation;

import java.util.Optional;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;

import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.sync.AbstractSynchronizedValueComputer;

/**
 * Synchronize the display parameters of {@link IWWNavigationLayer}.
 */
public class WWNavigationLayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWNavigationLayer, WWNavigationLayerParameters> {

	/**
	 * Constructor
	 */
	public WWNavigationLayerSynchronizer() {
		super(IWWNavigationLayer.class);
	}

	@Override
	public void propagateParametersTo(IWWNavigationLayer layer, WWNavigationLayerParameters layerParameters) {
		layer.setDisplayParameters(layerParameters);
	}

	@Override
	protected Optional<WWNavigationLayerParameters> makeLayerParameters(IWWNavigationLayer layer) {
		return Optional.of(layer.getDisplayParameters());
	}

	@Override
	protected String inferType(IWWNavigationLayer layer) {
		return IWWNavigationLayer.class.getSimpleName();
	}

	/**
	 * Compute the min and max texture values over the layers synchronized with the specified layer
	 */
	public Optional<Range> getMinMaxSyncValues(IWWNavigationLayer layer) {
		return new MinMaxSyncValuesComputer().compute(layer);
	}

	/**
	 * An AbstractSynchronizedValueComputer to compute the min and max texture values over the layers synchronized with
	 * one specified layer
	 */
	class MinMaxSyncValuesComputer
			extends AbstractSynchronizedValueComputer<IWWNavigationLayer, WWNavigationLayerParameters, Range> {
		/**
		 * Constructor
		 */
		protected MinMaxSyncValuesComputer() {
			super(WWNavigationLayerSynchronizer.this);
		}

		/** {@inheritDoc} */
		@Override
		protected Range extractResultFromParameter(WWNavigationLayerParameters p) {
			// Compute initial contrast minValue and maxValue
			return new DoubleRange(p.colorContrastModel().resetMin(), p.colorContrastModel().resetMax());
		}

		/** {@inheritDoc} */
		@Override
		protected Range cumulateResultOfParameters(Range p1, Range p2) {
			if(!Float.isFinite(p1.getMinimumFloat()) || !Float.isFinite(p1.getMaximumFloat()))
				return p2;
			else if (!Float.isFinite(p2.getMinimumFloat()) || !Float.isFinite(p2.getMaximumFloat()))
				return p1;
			
			return new FloatRange(Math.min(p1.getMinimumFloat(), p2.getMinimumFloat()),
					Math.max(p1.getMaximumFloat(), p2.getMaximumFloat()));
		}
	}

}
