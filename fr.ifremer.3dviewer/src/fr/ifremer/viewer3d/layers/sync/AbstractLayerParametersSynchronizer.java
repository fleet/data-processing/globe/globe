/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

/**
 * Template method to to synchronize display parameters of a kind of layer
 *
 * @param <T> Kind of layer managed by the synchronizer
 * @param <P> Kind of parameter synchronized over layers
 */
public abstract class AbstractLayerParametersSynchronizer<T extends IWWLayer, P> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(AbstractLayerParametersSynchronizer.class);

	@Inject
	protected WWFileLayerStoreModel fileLayerStoreModel;
	@Inject
	private ProjectExplorerModel projectExplorerModel;
	@Inject
	protected IGeographicViewService geographicViewService;

	/** Synchronization options **/
	public enum SynchronizationOption {
		APPLY_TO_ALL,
		APPLY_TO_GROUP,
		NONE,
		NOT_SET
	}

	/** Class of the managed layers */
	protected final Class<T> layerClass;

	/**
	 * Synchronization mode. <br>
	 * Key is the layer type : {@link #inferType(IWWLayer)}
	 */
	protected Map<String, SynchronizationOption> synchronizationOption = new ConcurrentHashMap<>();

	/**
	 * Parameters to set when Apply to all is required. <br>
	 * Key is the layer type : {@link #inferType(IWWLayer)}
	 */
	protected Map<String, P> layerParametersForAll = new ConcurrentHashMap<>();

	/**
	 * Parameters to set when Apply to group is required. <br>
	 * Key is the layer type : {@link #inferType(IWWLayer)}. <br>
	 * Key of the inner map is the group path : {@link TreeNode#getPath())}.
	 */
	protected Map<String, Map<String, P>> layerParametersForGroup = new ConcurrentHashMap<>();

	/** Set of listeners, invoked when parameters of layer are changed */
	private final Set<Consumer<T>> layerParameterConsumers = new HashSet<>();

	/**
	 * Constructor
	 */
	protected AbstractLayerParametersSynchronizer(Class<T> layerClass) {
		this.layerClass = layerClass;
	}

	/**
	 * Constructor
	 */
	@PostConstruct
	protected void postConstruct() {
		fileLayerStoreModel.addListener(//
				layerClass, //
				this::onLayerCreatedOrUpdated, //
				this::onLayerCreatedOrUpdated, //
				this::onLayerDeleted);
	}

	/** Instanciates a Pojo to hold all parameters of the sychronized layers */
	protected abstract Optional<P> makeLayerParameters(T layer);

	/**
	 * Infer the type of the synchronized layers. <br>
	 * Used to differentiate layers of the same Class (Elevation raster, STDEV...). <br>
	 * Only layers with same Class and this type are synchronized
	 *
	 * @return the type of specified layer or null when this layer is not suitable
	 */
	protected abstract String inferType(T layer);

	/** Delegates the propagation of parameters to subclasses */
	protected abstract void propagateParametersTo(T layer, P layerParameters);

	/*
	 * Activates the synchronization for all layers
	 */
	public void activateSynchronizationForAll(T masterLayer) {
		String layerType = inferType(masterLayer);
		if (layerType != null && synchronizationOption.get(layerType) != SynchronizationOption.APPLY_TO_ALL) {
			logger.debug("Activates the parameters synchronization of all layers of type {}", layerType);
			synchronizationOption.put(layerType, SynchronizationOption.APPLY_TO_ALL);
			// Store the layer parameters
			makeLayerParameters(masterLayer).ifPresent(p -> {
				layerParametersForAll.put(layerType, p);
				layerParametersForGroup.remove(layerType);
				// Propagates the parameters to all other layers
				getAllSynchronizedLayers(layerType).forEach(layer -> propagateParametersTo(layer, p));
				// Require a refresh
				geographicViewService.refresh();
			});
		}
	}

	/*
	 * Activates the synchronization for grouped layers
	 */
	public void activateSynchronizationForGroup(T masterLayer) {
		String layerType = inferType(masterLayer);
		if (layerType != null && synchronizationOption.get(layerType) != SynchronizationOption.APPLY_TO_GROUP) {
			logger.debug("Activates the parameters synchronization of grouped layers of type {}", layerType);
			synchronizationOption.put(layerType, SynchronizationOption.APPLY_TO_GROUP);
			layerParametersForAll.remove(layerType);

			// Synchronize all the layers of the group to which the master layer belongs
			synchronizeLayersOfSameGroup(masterLayer, layerType, l -> true);
			var layerParametersForLayerType = this.layerParametersForGroup.computeIfAbsent(layerType,
					k -> new HashMap<>());

			// Now, synchronize all other grouped layers
			getAllSynchronizedLayers(layerType).forEach(otherLayer -> {
				var otherGroupNode = inferGroup(otherLayer);
				// layerParametersForLayerType contains the group means the otherLayer has already be processed
				if (otherGroupNode != null && !layerParametersForLayerType.containsKey(otherGroupNode.getPath())) {
					synchronizeLayersOfSameGroup(otherLayer, layerType, l -> true);
				}
			});
		}

		// Require a refresh
		geographicViewService.refresh();
	}

	/*
	 * Deactivates the synchronization of WC masterLayer
	 */
	public void desactivateSynchronization(T layer) {
		String layerType = inferType(layer);
		if (layer != null) {
			logger.debug("Desactivates the synchronization of layers of type {}", layerType);
			layerParametersForAll.remove(layerType);
			synchronizationOption.put(layerType, SynchronizationOption.NONE);

			// Require a refresh
			geographicViewService.refresh();
		}
	}

	/**
	 * Notify the synchronizer that a layer has changed and its parameters must be propagated to other layers
	 */
	public void synchonizeWith(T masterLayer) {
		synchonizeWith(masterLayer, l -> true);
	}

	/**
	 * Notify the synchronizer that a layer has changed and its parameters must be propagated to other layers
	 */
	public void synchonizeWith(T masterLayer, Predicate<T> criteria) {
		String layerType = inferType(masterLayer);
		if (layerType != null) {
			makeLayerParameters(masterLayer).ifPresent(p -> {
				if (synchronizationOption.get(layerType) == SynchronizationOption.APPLY_TO_ALL) {
					// Store the layer parameters
					layerParametersForAll.put(layerType, p);
					// Propagates the parameters to all other layers
					getAllSynchronizedLayers(layerType).filter(criteria).forEach(layer -> {
						propagateParametersTo(layer, p);
						notifyLayerParameterConsumers(layer);
					});
				} else if (synchronizationOption.get(layerType) == SynchronizationOption.APPLY_TO_GROUP) {
					synchronizeLayersOfSameGroup(masterLayer, layerType, criteria);
				}
				notifyLayerParameterConsumers(masterLayer);
			});
		}

		// Require a refresh because at least the specified master layer has changed
		geographicViewService.refresh();
	}

	/**
	 * Extracts the parameters of the specified layer and propagates them to all the layers of the group
	 */
	protected void synchronizeLayersOfSameGroup(T masterLayer, String layerType, Predicate<T> criteria) {
		GroupNode groupNode = inferGroup(masterLayer);
		if (groupNode != null) {
			var masterGroupPath = groupNode.getPath();
			// Store the layer parameters
			makeLayerParameters(masterLayer).ifPresent(p -> {
				this.layerParametersForGroup.computeIfAbsent(layerType, k -> new HashMap<>()).put(masterGroupPath, p);
				// Propagates the parameters to the group
				getGroupedSynchronizedLayers(groupNode, layerType).filter(criteria)
						.forEach(layer -> propagateParametersTo(layer, p));
			});
		}
	}

	/**
	 * <<<<<<< HEAD Propagates the specified parameters to all the layer belonging to the group node
	 */
	protected void synchonizeGroupParameters(GroupNode groupNode, String layerType, P layerParameters) {
		// Propagates the parameters to the group
		getGroupedSynchronizedLayers(groupNode, layerType).forEach(layer -> {
			propagateParametersTo(layer, layerParameters);
			notifyLayerParameterConsumers(layer);
		});
	}

	/**
	 * ======= >>>>>>> c85e70de2 (EK80 - synchronizes parameters between layers of the same frequency) React on the
	 * creation or update of the layer.<br>
	 * Apply the parameters to this layer if any
	 */
	protected void onLayerCreatedOrUpdated(T layer) {
		String layerType = inferType(layer);
		if (layerType != null) {
			doOnLayerCreatedOrUpdated(layerType, layer);
			// Require a refresh because at least the specified master layer has changed
			geographicViewService.refresh();
		}
	}

	/**
	 * A layer has been created or uptaded.
	 */
	protected void doOnLayerCreatedOrUpdated(String layerType, T layer) {
		if (synchronizationOption.get(layerType) == SynchronizationOption.APPLY_TO_ALL) {
			// All synchronization : apply the same parameters for all this layer type
			P layerParameters = layerParametersForAll.get(layerType);
			if (layerParameters != null) {
				propagateParametersTo(layer, layerParameters);
				notifyLayerParameterConsumers(layer);
			}
		} else if (synchronizationOption.get(layerType) == SynchronizationOption.APPLY_TO_GROUP) {
			// Group synchronization : apply the same parameters for the layer of the same group
			var groupNode = inferGroup(layer);
			if (groupNode != null) {
				var layerParametersForLayerType = this.layerParametersForGroup.computeIfAbsent(layerType,
						k -> new HashMap<>());
				P layerParameters = layerParametersForLayerType.get(groupNode.getPath());
				if (layerParameters != null) {
					// Apply existing parameters
					propagateParametersTo(layer, layerParameters);
					notifyLayerParameterConsumers(layer);
				} else {
					// Store
					makeLayerParameters(layer).ifPresent(p -> layerParametersForLayerType.put(groupNode.getPath(), p));
				}
			}
			// FIXME bug : infinite loop if layer is not under "Data" group (for Swath Editor for example)
			// else {
			// TODO, find a better way
			// Posponed, not yet hooked to a node
			// Display.getDefault().asyncExec(() -> onLayerCreatedOrUpdated(layer));
			// }
		}
	}

	/**
	 * React on the deletion of a layer
	 */
	protected void onLayerDeleted(T layer) {
		// Nothing to do
	}

	/**
	 * @return the {@link #synchronizationOption}
	 */
	public SynchronizationOption getSynchronizationOption(T layer) {
		String layerType = inferType(layer);
		if (layerType != null) {
			return synchronizationOption.getOrDefault(layerType, SynchronizationOption.NOT_SET);
		}
		return SynchronizationOption.NOT_SET;
	}

	/**
	 * @return all layers of the same class and type than the specified one
	 */
	protected Stream<T> getAllSynchronizedLayers(String masterLayerType) {
		return fileLayerStoreModel.getLayers(layerClass).filter(layer -> masterLayerType.equals(inferType(layer)));
	}

	/**
	 * @return layers belonging to the same GroupNode and having the same type
	 */
	protected Stream<T> getGroupedSynchronizedLayers(GroupNode groupNode, String masterLayerType) {
		return groupNode.getChildren().stream()//
				.filter(FileInfoNode.class::isInstance).map(FileInfoNode.class::cast)//
				.flatMap(fileNode -> fileNode.getChildren().stream())//
				.filter(LayerNode.class::isInstance).map(LayerNode.class::cast)//
				.map(LayerNode::getLayer).filter(Optional::isPresent).map(Optional::get)//
				.filter(layerClass::isInstance).map(layerClass::cast) //
				.filter(layer -> masterLayerType.equals(inferType(layer)));
	}

	protected GroupNode inferGroup(T masterLayer) {
		// Ugly code... TODO: wait for model re-factoring to update... the model should provide group/parent ect...
		// get file info node for the current masterLayer ()
		Optional<WWFileLayerStore> optWWFileLayerStore = fileLayerStoreModel.getAll().stream()
				.filter(wWFileLayerStore -> wWFileLayerStore.getLayers().contains(masterLayer)).findFirst();
		if (optWWFileLayerStore.isEmpty()) {
			return null;
		}
		IFileInfo fileInfo = optWWFileLayerStore.get().getFileInfo();
		return projectExplorerModel.getFileInfoNode(fileInfo).flatMap(FileInfoNode::getParent).orElse(null);
	}

	/**
	 * @return the {@link #synchronizationOption}
	 */
	public Map<String, SynchronizationOption> getSynchronizationOption() {
		return synchronizationOption;
	}

	/** Accept an other listener to inform when parameters changed on a layer */
	public void addParameterChangedListener(Consumer<T> layerParameterConsumer) {
		layerParameterConsumers.add(layerParameterConsumer);
	}

	/** Removes a listener */
	public void removeParameterChangedListener(Consumer<T> layerParameterConsumer) {
		layerParameterConsumers.remove(layerParameterConsumer);
	}

	/** Parameters have changed for the layer. Notify all listeners */
	private void notifyLayerParameterConsumers(T layer) {
		layerParameterConsumers.stream().forEach(c -> c.accept(layer));
	}
}
