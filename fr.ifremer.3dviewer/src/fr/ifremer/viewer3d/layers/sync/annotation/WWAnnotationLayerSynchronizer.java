package fr.ifremer.viewer3d.layers.sync.annotation;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.worldwind.layer.annotation.AnnotationParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;

/**
 * Synchronize the display parameters of IWWShapeAnnotationLayer layers.
 */
public class WWAnnotationLayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWAnnotationLayer, AnnotationParameters> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(WWAnnotationLayerSynchronizer.class);

	/**
	 * Constructor
	 */
	public WWAnnotationLayerSynchronizer() {
		super(IWWAnnotationLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWAnnotationLayer layer, AnnotationParameters polygonParameters) {
		layer.setParameters(polygonParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<AnnotationParameters> makeLayerParameters(IWWAnnotationLayer layer) {
		return Optional.of(layer.getParameters());
	}

	/**
	 * No distinction among polygons, they are all synced together
	 */
	@Override
	protected String inferType(IWWAnnotationLayer layer) {
		return IWWAnnotationLayer.class.getSimpleName();
	}

}
