/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.wc;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.viewer3d.layers.wc.render.VolumicRendererDisplayParameters;

/** Simple Record to hold all parameters */
public /* Record */class WWWaterColumnVolumicParameters {
	// color contrast parameters
	public VolumicRendererDisplayParameters displayParameters;

	// Filter
	public FilterParameters filterParameters;
}
