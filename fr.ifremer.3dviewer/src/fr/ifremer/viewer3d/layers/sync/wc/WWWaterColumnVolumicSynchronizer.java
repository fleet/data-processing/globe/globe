/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.wc;

import java.beans.PropertyChangeListener;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;

/**
 * Synchronize the display parameters of WCVolumicLayer layers.
 */
public class WWWaterColumnVolumicSynchronizer
		extends AbstractLayerParametersSynchronizer<WCVolumicLayer, WWWaterColumnVolumicParameters> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(WWWaterColumnVolumicSynchronizer.class);

	/** Listener on property WCVolumicLayer.ACTIVATED_PROPERTY_NAME */
	protected PropertyChangeListener activationListener = e -> onLayerActivationChanged(
			(WCVolumicLayer) e.getNewValue());

	/**
	 * Constructor
	 */
	public WWWaterColumnVolumicSynchronizer() {
		super(WCVolumicLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(WCVolumicLayer layer, WWWaterColumnVolumicParameters layerParameters) {
		layer.setOpacity(layerParameters.displayParameters.getOpacity());

		if (layerParameters.filterParameters != null) {
			layer.setFilterParameters(layerParameters.filterParameters);
		}
		if (layerParameters.displayParameters != null) {
			layer.updateRendererParameters(layerParameters.displayParameters);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<WWWaterColumnVolumicParameters> makeLayerParameters(WCVolumicLayer layer) {
		WWWaterColumnVolumicParameters result = null;
		if (layer.isLayerActive()) {
			// WC activated when FilterParameters are present
			result = new WWWaterColumnVolumicParameters();
			result.displayParameters = layer.getDisplayParameters();
			result.filterParameters = layer.getFilterParameters();
		}
		return Optional.ofNullable(result);
	}

	/**
	 * Two types of WC are created to synchronize an active layer with other active ones.
	 */
	@Override
	protected String inferType(WCVolumicLayer layer) {
		return layer.isLayerActive() ? WCVolumicLayer.class.getSimpleName() : null;
	}

	/** {@inheritDoc} */
	@Override
	protected void onLayerCreatedOrUpdated(WCVolumicLayer layer) {
		layer.removePropertyChangeListener(WCVolumicLayer.ACTIVATED, activationListener);
		layer.addPropertyChangeListener(WCVolumicLayer.ACTIVATED, activationListener);
		super.onLayerCreatedOrUpdated(layer);
	}

	/** {@inheritDoc} */
	@Override
	protected void onLayerDeleted(WCVolumicLayer layer) {
		layer.removePropertyChangeListener(WCVolumicLayer.ACTIVATED, activationListener);
		super.onLayerDeleted(layer);
	}

	/** Invoke when activate property changed on a layer */
	protected void onLayerActivationChanged(WCVolumicLayer layer) {
		if (layer.isLayerActive()) {
			// Layer has been activated. Can now be managed as if it was created
			super.onLayerCreatedOrUpdated(layer);
		} else {
			super.onLayerDeleted(layer);
		}

	}

}
