/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.marker;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.WWMarkerLayerParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.wc.IWCLayer;

/**
 * Synchronize the display parameters of {@link IWWMarkerLayer}.
 */
public class WWMarkerLayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWMarkerLayer, WWMarkerLayerParameters> {

	@Inject
	protected WWFileLayerStoreModel fileLayerStoreModel;

	@Inject
	protected IGeographicViewService geographicViewService;

	/**
	 * Registry of all IWCLayer contained in the model. <br>
	 * Link between WC layer and its markers.
	 */
	protected Map<IWCLayer, Set<IWCMarker>> wcLayerRegistry = new ConcurrentHashMap<>();

	/**
	 * Constructor
	 */
	public WWMarkerLayerSynchronizer() {
		super(IWWMarkerLayer.class);
	}

	/**
	 * Post construction
	 */
	@PostConstruct
	protected void postConstruct() {
		fileLayerStoreModel.addListener(//
				IWCLayer.class, //
				this::onLayerCreated, //
				this::onLayerModified, //
				this::onLayerDeleted);
		fileLayerStoreModel.addListener(//
				IWWMarkerLayer.class, //
				this::onLayerCreated, //
				this::onLayerModified, //
				this::onLayerDeleted);
	}

	@Override
	public void propagateParametersTo(IWWMarkerLayer layer, WWMarkerLayerParameters layerParameters) {
		layer.setParameters(layerParameters);
	}

	@Override
	protected Optional<WWMarkerLayerParameters> makeLayerParameters(IWWMarkerLayer layer) {
		return Optional.of(layer.getParameters());
	}

	@Override
	protected String inferType(IWWMarkerLayer layer) {
		return IWWMarkerLayer.class.getSimpleName();
	}

	/**
	 * Reacts to the creation of a IWWMarkerLayer
	 */
	protected void onLayerCreated(IWWMarkerLayer markerLayer) {
		markerLayer.getMarkers(IWCMarker.class).forEach(this::markerAdded);
	}

	/**
	 * Reacts to the deletion of a IWWMarkerLayer
	 */
	protected void onLayerDeleted(IWWMarkerLayer markerLayer) {
		markerLayer.getMarkers(IWCMarker.class).forEach(this::markerRemoved);
	}

	/**
	 * Reacts to the modification of a IWWLayer
	 */
	protected void onLayerModified(IWWLayer wcLayer) {
		// Nothing to do
	}

	/**
	 * Reacts to the creation of a IWCLayer
	 */
	protected void onLayerCreated(IWCLayer wcLayer) {
		// Prepare the association wc -> markers
		var allMarkersOfWc = wcLayerRegistry.computeIfAbsent(wcLayer, layer -> new HashSet<>());

		// Link the new WC layer to all known markers in the model
		String wcLayerName = wcLayer.getName();
		fileLayerStoreModel.getLayers(IWWMarkerLayer.class)//
				.flatMap(markerLayer -> markerLayer.getMarkers(IWCMarker.class).stream()) //
				.filter(marker -> wcLayerName.equals(marker.getWaterColumnLayer()))//
				.forEach(marker -> {
					allMarkersOfWc.add(marker);
					marker.setVerticalOffset(wcLayer.getVerticalOffset());
				});

	}

	/**
	 * Reacts to the deletion of a IWCLayer
	 */
	protected void onLayerDeleted(IWCLayer wcLayer) {
		// Clean association wc -> markers
		var allMarkersOfWc = wcLayerRegistry.remove(wcLayer);

		// Clean association markers -> wc
		if (allMarkersOfWc != null) {
			allMarkersOfWc.forEach(marker -> marker.setVerticalOffset(0f));
		}

	}

	/**
	 * Reacts to the change of the vertical offset in the specified WC layer
	 */
	public void synchronizeWith(IWCLayer wcLayer, float verticalOffset) {
		var allMarkersOfWc = wcLayerRegistry.get(wcLayer);
		if (allMarkersOfWc != null) {
			allMarkersOfWc.forEach(marker -> marker.setVerticalOffset(verticalOffset));
		}
	}

	/**
	 * This method has to be called when a new WC marker has been created.
	 */
	public void markerAdded(IWCMarker marker) {
		// Searchs if the WC layer targeted by the marker is registered
		String wcLayerName = marker.getWaterColumnLayer();
		if (wcLayerName != null) {
			for (var entry : wcLayerRegistry.entrySet()) {
				if (entry.getKey().getName().equals(wcLayerName)) {
					// Yes, the WC layer targeted by the marker has been found
					// Links the marker layer to the WC layer
					entry.getValue().add(marker);
					// Synchronize the vertical offset
					marker.setVerticalOffset(entry.getKey().getVerticalOffset());
				}
			}
		}
	}

	/**
	 * This method has to be called when a new WC marker has been removed.
	 */
	public void markerRemoved(IWCMarker marker) {
		// Searchs if the WC layer targeted by the marker is registered
		String wcLayerName = marker.getWaterColumnLayer();
		if (wcLayerName != null) {
			for (var entry : wcLayerRegistry.entrySet()) {
				if (entry.getKey().getName().equals(wcLayerName)) {
					// Yes, the WC layer targeted by the marker has been found
					// Links the marker layer to the WC layer
					entry.getValue().remove(marker);
				}
			}
		}
	}
}
