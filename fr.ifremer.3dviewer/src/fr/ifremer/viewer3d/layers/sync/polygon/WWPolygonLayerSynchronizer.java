/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.polygon;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.PolygonParameters;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;

/**
 * Synchronize the display parameters of IWWPolygonLayer layers.
 */
public class WWPolygonLayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWPolygonLayer, PolygonParameters> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(WWPolygonLayerSynchronizer.class);

	/**
	 * Constructor
	 */
	public WWPolygonLayerSynchronizer() {
		super(IWWPolygonLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWPolygonLayer layer, PolygonParameters polygonParameters) {
		layer.setParameters(polygonParameters);
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<PolygonParameters> makeLayerParameters(IWWPolygonLayer layer) {
		return Optional.of(layer.getParameters());
	}

	/**
	 * No distinction among polygons, they are all synced together
	 */
	@Override
	protected String inferType(IWWPolygonLayer layer) {
		return IWWPolygonLayer.class.getSimpleName();
	}

}
