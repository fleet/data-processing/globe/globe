/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.sync.player;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerData;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerStatus;
import fr.ifremer.viewer3d.layers.sync.AbstractLayerParametersSynchronizer;
import fr.ifremer.viewer3d.layers.sync.player.IndexedPlayerSynchronizer.PlayerData;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;

/**
 * Synchronize the index of Players of IWWPlayableTextureLayer layers.
 */
public class IndexedPlayerSynchronizer
		extends AbstractLayerParametersSynchronizer<IWWPlayableTextureLayer, PlayerData> {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(IndexedPlayerSynchronizer.class);

	/** Subscription on Player */
	private final Map<IWWPlayableTextureLayer, Disposable> diposables = new ConcurrentHashMap<>();
	/** Subscription on Player */
	private final Map<IWWPlayableTextureLayer, Flowable<IndexedPlayer>> playerFlowable = new ConcurrentHashMap<>();

	/**
	 * Constructor
	 */
	public IndexedPlayerSynchronizer() {
		super(IWWPlayableTextureLayer.class);
	}

	/** {@inheritDoc} */
	@Override
	public void propagateParametersTo(IWWPlayableTextureLayer layer, PlayerData playerData) {
		var player = layer.getPlayer();
		// Must synchronize before modifying the map (because of multithreading of Rx)
		synchronized (diposables) {
			unsubscribe(layer);
			try {
				player.syncWith(playerData.status, playerData.data);
			} finally {
				subscribe(layer);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<PlayerData> makeLayerParameters(IWWPlayableTextureLayer layer) {
		return Optional.of(new PlayerData(layer.getPlayer().getStatus(), layer.getPlayer().getData()));
	}

	/** {@inheritDoc} */
	@Override
	protected String inferType(IWWPlayableTextureLayer layer) {
		return layer.getPlayer().getSyncKey().orElse(null);
	}

	/** {@inheritDoc} */
	@Override
	protected void onLayerCreatedOrUpdated(IWWPlayableTextureLayer layer) {
		super.onLayerCreatedOrUpdated(layer);
		String layerType = inferType(layer);
		if (layerType != null) {
			// A layer with a player has been created. Subscribe to receive the index
			subscribe(layer);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected void onLayerDeleted(IWWPlayableTextureLayer layer) {
		super.onLayerDeleted(layer);
		unsubscribe(layer);
		playerFlowable.remove(layer);
	}

	/** Subscibes to the player of the specified layer. */
	protected void subscribe(IWWPlayableTextureLayer layer) {
		IndexedPlayer player = layer.getPlayer();
		// Creates a flow to invoke synchonizeWith when the data OR the status changed on the player.
		var flow = playerFlowable.computeIfAbsent(layer, l -> player.getFlowable().map(d -> player)
				.mergeWith(layer.getPlayer().getStatusPublisher().toFlowable(BackpressureStrategy.LATEST)));
		// Subscription to this flow
		diposables.computeIfAbsent(layer, l -> flow.subscribe(data -> synchonizeWith(l)));
	}

	/** Unsubscibes to the player of the specified layer. */
	protected void unsubscribe(IWWPlayableTextureLayer layer) {
		var disposable = diposables.remove(layer);
		if (disposable != null) {
			disposable.dispose();
		}
	}

	/**
	 * Data used to sync players
	 */
	static class PlayerData {

		public final IndexedPlayerStatus status;
		public final IndexedPlayerData data;

		/**
		 * Constructor
		 */
		public PlayerData(IndexedPlayerStatus status, IndexedPlayerData data) {
			this.status = status;
			this.data = data;
		}
	}
}
