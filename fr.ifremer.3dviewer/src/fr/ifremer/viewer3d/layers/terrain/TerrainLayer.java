package fr.ifremer.viewer3d.layers.terrain;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang.math.DoubleRange;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.Activator;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import jakarta.inject.Inject;

/**
 * Tiled WW layer to display terrain data
 */
public class TerrainLayer extends AbstractTerrainTextureLayer implements IWWTerrainLayer, IInfos {

	@Inject
	private ISessionService sessionService;
	@Inject
	private IRasterService rasterService;

	/** Kind of data like Elevation, STDEV... */
	public final String type;
	/** All rendering parameters */
	protected TerrainParameters textureParameters;

	/**
	 * Constructor.
	 */
	public TerrainLayer(Document document, String type, DoubleRange minMaxValues) {
		super(document);
		this.type = type;

		String paletteName = Activator.getPalettesPreference().getGriddedDataPalette().getValue();
		String smallType = type.toLowerCase();
		if (smallType.contains("reflectivity") || smallType.contains("backscatter")) {
			paletteName = Activator.getPalettesPreference().getBackscatterPalette().getValue();
		} else if (smallType.contains("depth") || smallType.contains("elevation")) {
			paletteName = Activator.getPalettesPreference().getBathymetryPalette().getValue();
		}
		textureParameters = TerrainParameters.defaultInstance(minMaxValues);
		textureParameters = textureParameters.withColor(ColorMap.getColorMapIntIndex(paletteName));

		adaptUniformModelToParameters();
	}

	/**
	 * Create and set a new instance of UniformModel for SuperClass
	 */
	private void adaptUniformModelToParameters() {
		// colormap in one array for efficiency
		// and because shader can't handle to many parameters
		float[][] colormapBuffer = ColorMap.getColormap(textureParameters.colorMap);
		IntList colormap = new ArrayIntList(256);
		for (int index = 0; index < 256; index++) {
			colormap.add(255 << 24 // alpha
					| (int) (colormapBuffer[ColorMap.red][index] * 255) << 16 //
					| (int) (colormapBuffer[ColorMap.green][index] * 255) << 8//
					| (int) (colormapBuffer[ColorMap.blue][index] * 255));
		}

		UniformModel newModel = new UniformModel(
				// Contrast
				textureParameters.opacity, textureParameters.minContrast, textureParameters.maxContrast,
				// Color map
				colormap, textureParameters.reverseColorMap,
				// Shading
				textureParameters.useShading, textureParameters.useGradient, textureParameters.useLogarithmicShading,
				textureParameters.shadingExaggeration, textureParameters.azimuth, textureParameters.zenith,
				// Filtering
				textureParameters.transparencyEnabled, textureParameters.minTransparency,
				textureParameters.maxTransparency, textureParameters.minTextureValue, textureParameters.maxTextureValue,
				// Allow to set the shader program according to the interpolation configuration
				true);
		setUniformModel(newModel);
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(getTextureParameters().toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(TerrainParameters.SESSION_KEY).ifPresent(p -> {
			textureParameters = p;
			adaptUniformModelToParameters();
		});
	}

	/**
	 * @return the {@link #textureParameters}
	 */
	@Override
	public TerrainParameters getTextureParameters() {
		return textureParameters;
	}

	/**
	 * @param textureParameters the {@link #textureParameters} to set
	 */
	@Override
	public void setTextureParameters(TerrainParameters newParams) {
		textureParameters = new TerrainParameters(newParams.opacity, newParams.minContrast, newParams.maxContrast,
				newParams.colorMap, newParams.reverseColorMap, newParams.useShading, newParams.useGradient,
				newParams.useLogarithmicShading, newParams.shadingExaggeration, newParams.azimuth, newParams.zenith,
				newParams.transparencyEnabled, newParams.minTransparency, newParams.maxTransparency,
				// Must retaining the min and max texture values
				textureParameters.minTextureValue, textureParameters.maxTextureValue);
		adaptUniformModelToParameters();
		sessionService.saveSession();
	}

	/**
	 * @return the {@link #type}
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public Optional<String> getDataCacheName() {
		if (getValue(AVKey.CONSTRUCTION_PARAMETERS) instanceof AVList constructionParameters
				&& constructionParameters.getValue(AVKey.DATA_CACHE_NAME) instanceof String dataCacheName) {
			return Optional.of(dataCacheName);
		}
		return Optional.empty();
	}

	/**
	 * Invoke by PropertiesView to list the properties of this layer. Search the rasterinfo that created the layer to
	 * add its properties
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new LinkedList<>();
		if (type != null) {
			result.add(Property.build("Type", type));
			getFileLayerStoreModel().getFileInfo(this).ifPresent(//
					fileInfo -> rasterService.getRasterInfos(fileInfo)//
							.stream()//
							.filter(r -> type.equalsIgnoreCase(r.getDataType()))//
							.findFirst()//
							.ifPresent(rasterInfo -> result.addAll(rasterInfo.getProperties())));
		}
		return result;
	}
}
