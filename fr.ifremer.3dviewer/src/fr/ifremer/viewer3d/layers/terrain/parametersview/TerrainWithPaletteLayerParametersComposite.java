package fr.ifremer.viewer3d.layers.terrain.parametersview;

import jakarta.inject.Inject;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.core.utils.color.BitColorPalette;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainWithPaletteParameters;
import fr.ifremer.globe.ui.widget.color.ColorPaletteParameterComposite;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.sync.dtm.WWTerrainLayerWithPaletteSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Composite which contains all tools relative a {@link IWWTerrainWithPaletteLayer}.
 */
public class TerrainWithPaletteLayerParametersComposite extends Composite {

	/** The layer parameters synchronizer get from Context **/
	@Inject
	private WWTerrainLayerWithPaletteSynchronizer layersSynchronizer;
	@Inject
	private IGeographicViewService geographicViewService;

	/** Layer **/
	private final IWWTerrainWithPaletteLayer layer;

	/**
	 * Constructor with a predefined {@link IWWTerrainWithPaletteLayer}
	 */
	public TerrainWithPaletteLayerParametersComposite(Composite parent, IWWTerrainWithPaletteLayer layer) {
		super(parent, SWT.NONE);
		this.layer = layer;
		// injection to retrieve the fileLayerStoreModel
		ContextInitializer.inject(this);

		// update composites
		setLayout(GridLayoutFactory.fillDefaults().create());

		Group grpLayer = new Group(this, SWT.NONE);
		grpLayer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpLayer.setText(String.format("Layer of '%s'", layer.getName()));
		grpLayer.setLayout(new GridLayout(1, false));

		makeSynchronizedGroup(grpLayer);
		makePaletteComposite(grpLayer);
		makeOpacityComposite(grpLayer);
	}

	private void makeSynchronizedGroup(Composite parent) {
		// first set: create the composite
		var grpSynchronization = new LayerParametersSynchronizerComposite<>(parent, layersSynchronizer, layer);
		grpSynchronization.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSynchronization.setText("Synchronization with other layers");
		grpSynchronization.adaptToLayer(layer);
	}

	/**
	 * Creates a tab which contains all tools relatives to color and contrast features.
	 */
	private void makePaletteComposite(Composite parent) {
		TerrainWithPaletteParameters textureParameters = layer.getTextureParameters();
		var colorPaletteComposite = new ColorPaletteParameterComposite(parent, SWT.NONE, textureParameters.palette(),
				this::updateColorPalette);
		colorPaletteComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	}

	private void makeOpacityComposite(Composite parent) {
		Group grpOpacity = new Group(parent, SWT.NONE);
		grpOpacity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpOpacity.setText("Opacity");
		grpOpacity.setLayout(GridLayoutFactory.fillDefaults().create());
		var opacitySliderComposite = new SliderComposite(grpOpacity, SWT.NONE, false, this::updateOpacity);
		opacitySliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		opacitySliderComposite.setValue(layer.getTextureParameters().opacity());
	}

	private void updateColorPalette(AColorPalette palette) {
		if (palette instanceof BitColorPalette bitColorPalette) {
			TerrainWithPaletteParameters textureParameters = layer.getTextureParameters();
			layer.setTextureParameters(textureParameters.withPalette(bitColorPalette));
			layersSynchronizer.synchonizeWith(layer);
			geographicViewService.refresh();
		}
	}

	private void updateOpacity(float opacity) {
		TerrainWithPaletteParameters textureParameters = layer.getTextureParameters();
		layer.setTextureParameters(textureParameters.withOpacity(opacity));
		geographicViewService.refresh();
	}

}
