package fr.ifremer.viewer3d.layers.terrain.parametersview;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import jakarta.inject.Inject;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.io.dtm.common.info.DtmInfo;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.widget.AbstractParametersCompositeWithTabs;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;
import fr.ifremer.globe.ui.widget.shading.ShadingWidget;
import fr.ifremer.globe.ui.widget.shading.ShadingWidgetModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdWidget;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.colorscale.ColorScaleLayer;
import fr.ifremer.viewer3d.layers.colorscale.parametersview.ColorScaleParametersComposite;
import fr.ifremer.viewer3d.layers.sync.dtm.WWTerrainLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Composite which contains all tools relative a {@link IWWTerrainLayer}.
 */
public class TerrainLayerParametersComposite extends AbstractParametersCompositeWithTabs
		implements PropertyChangeListener {

	/** Global {@link WWFileLayerStoreModel} **/
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;
	/** The layer parameters synchronizer get from Context **/
	@Inject
	private WWTerrainLayerSynchronizer layersSynchronizer;

	/** Layer **/
	private IWWTerrainLayer layer;

	/** Save the selected tab index **/
	private static int selectedTab;

	/** Inner composites **/
	private LayerParametersSynchronizerComposite<IWWTerrainLayer> grpSynchronization;
	private ColorContrastWidget colorComposite;
	private FloatThresholdWidget valueThreshold;
	private ShadingWidget shadingComposite;
	private ColorScaleParametersComposite colorScaleComposite;
	private Button btnEnableColorScale;
	private Composite colorScaleTab;

	/**
	 * Constructor
	 *
	 * @wbp.parser.constructor
	 */
	public TerrainLayerParametersComposite(Composite parent) {
		super(parent);
		// injection to retrieve the fileLayerStoreModel
		ContextInitializer.inject(this);

		final Consumer<IWWTerrainLayer> layerParameterConsumer = this::parameterChangedForLayer;
		layersSynchronizer.addParameterChangedListener(layerParameterConsumer);

		// add the pcs to listen to the event shortcut
		Viewer3D.addListener(this);
		addDisposeListener(e -> {
			layersSynchronizer.removeParameterChangedListener(layerParameterConsumer);
			Viewer3D.removeListener(this);
		});
	}

	/**
	 * Constructor with a predefined {@link IWWTerrainLayer}
	 */
	public TerrainLayerParametersComposite(Composite parent, IWWTerrainLayer layer) {
		this(parent);
		setModel(layer);
	}

	@Override
	protected void onSelectedTab(int selectedTab) {
		TerrainLayerParametersComposite.selectedTab = selectedTab;
	}

	public void setModel(IWWTerrainLayer layer) {
		if (isDisposed()) {
			return;
		}
		this.layer = layer;

		// update composites
		updateSynchronizedGroup();
		updateContrastTab();
		updateShadingTab();
		updateFilterTab();
		updateColorScaleTab();
		tabFolder.setSelection(selectedTab);

		// refresh layout
		ScrolledComposite cm = (ScrolledComposite) getParent().getParent();
		cm.setContent(getParent());
		Composite composite = getParent();
		composite.setSize(
				composite.computeSize(cm.getParent().getSize().x - cm.getVerticalBar().getSize().x, SWT.DEFAULT));
		cm.layout(true);
	}

	private void updateSynchronizedGroup() {
		if (grpSynchronization == null) {
			// first set: create the composite
			grpSynchronization = new LayerParametersSynchronizerComposite<>(this, layersSynchronizer, layer);
			grpSynchronization.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			grpSynchronization.setSynchronizationChangedListener(this::onSynchronizationChanged);
		}

		grpSynchronization.setText(String.format("Synchronization with other '%s' layers", layer.getType()));
		grpSynchronization.adaptToLayer(layer);
	}

	/**
	 * Creates a tab which contains all tools relatives to color and contrast features.
	 */
	private void updateContrastTab() {
		// compute parameters
		ColorContrastModel params = new ColorContrastModelBuilder(layer, Viewer3D.isInterpolate(),
				gainHistogramFromRasterInfo()).build();

		// parameters to apply after a "reset"
		Optional<Range> globalResetRange = layersSynchronizer.getMinMaxSyncValues(layer);

		if (colorComposite == null) {
			// first set: create the composite
			colorComposite = super.createContrastTab(params, globalResetRange,
					// called when parameters updated
					model -> {
						applyColorContrast(layer, model);
						if (layersSynchronizer != null) {
							layersSynchronizer.synchonizeWith(layer);
						}
					});
		} else {
			colorComposite.setModel(params, globalResetRange);
		}
	}

	private void applyColorContrast(IWWTerrainLayer layer, ColorContrastModel model) {
		layer.setTextureParameters(//
				layer.getTextureParameters().withColorContrast(//
						model.opacity(), model.contrastMin(), model.contrastMax(), model.colorMapIndex(),
						model.invertColor()));

		if (Viewer3D.isInterpolate() != model.interpolated()) {
			Viewer3D.interpolateTerrain();
		}
	}

	/***
	 * Creates a tab which contains all tools relatives to shading features.
	 */
	private void updateShadingTab() {
		// compute parameters
		ShadingWidgetModel params = new ShadingWidgetModel(layer.getTextureParameters());

		if (shadingComposite == null) {
			// first set: create the composite
			Composite container = createTab("Shading");
			shadingComposite = new ShadingWidget(container, params);
			shadingComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			shadingComposite.getPublisher().subscribe(
					// called when parameters updated
					shadingWidgetModel -> {
						applyShading(layer, shadingWidgetModel);
						if (layersSynchronizer != null) {
							layersSynchronizer.synchonizeWith(layer);
						}
					});
		} else {
			shadingComposite.setModel(params);
		}
	}

	private void applyShading(IWWTerrainLayer layer, ShadingWidgetModel model) {
		layer.setTextureParameters(//
				layer.getTextureParameters().withShading(//
						model.isEnabled(), model.isGradientEnabled(), model.isLogaritmicEnabled(),
						model.getShadingExageration(), model.getAzimuth(), model.getZenith()));
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 */
	private void updateFilterTab() {
		FloatThresholdModel params = getFilterParams(layer.getTextureParameters());
		if (valueThreshold == null) {
			// first set: create the composite
			valueThreshold = super.createFilterTab(params,
					// called when parameters updated
					model -> {
						applyFilter(layer, model);
						if (layersSynchronizer != null) {
							layersSynchronizer.synchonizeWith(layer);
						}
					});
		} else {
			valueThreshold.update(params);
		}
	}

	private FloatThresholdModel getFilterParams(TerrainParameters parameters) {
		return new FloatThresholdModel((float) parameters.minTransparency, (float) parameters.maxTransparency,
				parameters.transparencyEnabled, (float) parameters.minTextureValue, (float) parameters.maxTextureValue);
	}

	private void applyFilter(IWWTerrainLayer layer, FloatThresholdModel model) {
		layer.setTextureParameters(//
				layer.getTextureParameters().withFiltering(//
						model.enable, model.minValue, model.maxValue, model.minLimit, model.maxLimit));
	}

	/***
	 * Creates a tab which contains all tools relatives to the color scale.
	 */
	private void updateColorScaleTab() {
		// get related ColorScaleLayer
		Optional<ColorScaleLayer> optColorScaleLayer = fileLayerStoreModel.getLayers(ColorScaleLayer.class)//
				.filter(colorScaleLayer -> colorScaleLayer.getSource() == layer)//
				.findFirst();

		if (optColorScaleLayer.isPresent()) {
			ColorScaleLayer colorScaleLayer = optColorScaleLayer.get();
			if (colorScaleComposite == null) {
				// first set: create the composite
				colorScaleTab = createTab("Scale");
				colorScaleTab.setLayout(new GridLayout(1, false));
				btnEnableColorScale = new Button(colorScaleTab, SWT.CHECK);
				btnEnableColorScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
				btnEnableColorScale.setText("Display color scale");
				btnEnableColorScale.setSelection(colorScaleLayer.isEnabled());
				btnEnableColorScale.addListener(SWT.Selection, e -> {
					colorScaleComposite.getLayer().setEnabled(btnEnableColorScale.getSelection());
					colorScaleComposite.setEnabled(btnEnableColorScale.getSelection());
				});
				colorScaleComposite = new ColorScaleParametersComposite(colorScaleTab, colorScaleLayer);
				GridLayout gridLayout = new GridLayout(1, false);
				gridLayout.marginHeight = 0;
				gridLayout.marginWidth = 0;
				colorScaleComposite.setLayout(gridLayout);
				colorScaleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			} else {
				colorScaleComposite.setLayer(colorScaleLayer);
				btnEnableColorScale.setSelection(colorScaleLayer.isEnabled());
			}
		} else {
			if (btnEnableColorScale != null) {
				btnEnableColorScale.dispose();
			}
			if (colorScaleComposite != null) {
				colorScaleComposite.dispose();
			}
			if (colorScaleTab != null) {
				colorScaleTab.dispose();
			}
		}
	}

	/** Listens for {@link PropertyChangeEvent} **/
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		final String name = event.getPropertyName();
		Display.getDefault().asyncExec(() -> {
			if (Viewer3D.INTERPOLATION_EVENT.equals(name) && layer != null) {
				updateContrastTab();
			}
		});
	}

	/** The synchronization option has changed. Update the minMaxSyncValues in the ColorComposite */
	private void onSynchronizationChanged(IWWTerrainLayer layer) {
		if (colorComposite != null) {
			Optional<Range> minMaxSyncValues = layersSynchronizer.getMinMaxSyncValues(layer);
			colorComposite.setMinMaxSyncValues(minMaxSyncValues);
		}
	}

	private void parameterChangedForLayer(IWWTerrainLayer terrainlayer) {
		if (Objects.equals(terrainlayer, layer)) {
			UIUtils.asyncExecSafety(colorComposite, () -> {
				ColorContrastModel params = new ColorContrastModelBuilder(layer, Viewer3D.isInterpolate(),
						gainHistogramFromRasterInfo()).build();
				colorComposite.setModel(params);
			});
		}
	}

	/** Search the RasterInfo to complete the params with the histogram */
	private IntList gainHistogramFromRasterInfo() {
		String layerType = layer.getType();
		if (layerType != null && !layerType.isEmpty()) {
			WWFileLayerStore store = fileLayerStoreModel.get(layer).orElse(null);
			if (store != null && store.getFileInfo() instanceof DtmInfo dtmInfo) {
				RasterInfo rasterInfo = dtmInfo.getRasterInfo(layerType).orElse(null);
				if (rasterInfo != null) {
					return rasterInfo.getHistogram();
				}
			}
		}
		return new ArrayIntList(0);
	}
}
