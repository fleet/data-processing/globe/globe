/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain.parametersview;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Provide the parameter composite for DTM.
 */
@Component(name = "globe_viewer3d_parameters_dtm_file_composite_factory", service = IParametersViewCompositeFactory.class)
public class DtmInfoNodeCompositeFactory implements IParametersViewCompositeFactory {

	/** Constructor */
	public DtmInfoNodeCompositeFactory() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof FileInfoNode fileInfoNode && fileInfoNode.getFileInfo().getContentType().isDtm()
				? Optional.of(new DtmInfoParametersComposite(parent, fileInfoNode.getFileInfo()))
				: Optional.empty();
	}

}
