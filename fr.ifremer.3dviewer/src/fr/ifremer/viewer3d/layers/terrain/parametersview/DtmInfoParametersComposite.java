package fr.ifremer.viewer3d.layers.terrain.parametersview;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.ContrastShadeModel;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.loaders.ElevationLoader;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Parameter view for DTM files.
 */
public class DtmInfoParametersComposite extends Composite {

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(DtmInfoParametersComposite.class);

	/** Global {@link WWFileLayerStoreModel} **/
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Global {@link IGeographicViewService} **/
	@Inject
	private IGeographicViewService geographicViewService;

	/** Service of events **/
	@Inject
	private IEventBroker eventBroker;

	/** Global {@link IRasterService} **/
	@Inject
	private IRasterService rasterService;

	/** DTM file **/
	private final IFileInfo fileInfo;

	/** List of {@link RasterInfo} related to the {@link IFileInfo} **/
	private List<RasterInfo> rasterInfos;

	/** Selected {@link RasterInfo} (== layer) **/
	private RasterInfo selectedRasterInfo;

	/** Combo viewer to select raster **/
	private final ComboViewer rasterInfoViewer;

	/** Current selected layer **/
	private IWWTerrainLayer currentSelectedLayer;

	/** Inner parameter composite to display parameter of the selected layer **/
	private final TerrainLayerParametersComposite terrainParametersComposite;

	/**
	 * Constructor for {@link IWWTextureLayer}
	 */
	public DtmInfoParametersComposite(Composite parent, IFileInfo fileInfo) {
		super(parent, SWT.NONE);
		this.fileInfo = fileInfo;

		// injection to retrieve the fileLayerStoreModel
		ContextInitializer.inject(this);

		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		Group grpLayer = new Group(this, SWT.NONE);
		grpLayer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpLayer.setText(String.format("Layer of '%s'", fileInfo.getBaseName()));
		grpLayer.setLayout(new GridLayout(1, false));

		// combo to select layer
		rasterInfoViewer = new ComboViewer(grpLayer, SWT.READ_ONLY);
		Combo combo = rasterInfoViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		rasterInfoViewer.setContentProvider(ArrayContentProvider.getInstance());

		// layer parameters
		terrainParametersComposite = new TerrainLayerParametersComposite(getParent());
		terrainParametersComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// fill combo with DTM layers
		rasterInfos = rasterService.getRasterInfos(fileInfo);

		if (!rasterInfos.isEmpty()) {
			rasterInfoViewer.setLabelProvider(new LabelProvider() {
				@Override
				public String getText(Object element) {
					return ((RasterInfo) element).getDataType();
				}
			});
			rasterInfoViewer.setInput(rasterInfos);
			selectLayerOnTop();

			// handle selection of layer
			rasterInfoViewer.addSelectionChangedListener(this::onRasterSelected);
		} else {
			// NO RASTER INFO FOUND : DEPRECATED CODE TO HANDLE OLD DTM TODO : to remove after update of old DTM reading
			fileLayerStoreModel.get(fileInfo).ifPresent(wWFileLayerStore -> {
				List<ShaderElevationLayer> shaderElevationLayers = wWFileLayerStore.getLayers().stream()
						.filter(wWLayer -> wWLayer instanceof ShaderElevationLayer)
						.map(wWLayer -> (ShaderElevationLayer) wWLayer).collect(Collectors.toList());

				if (shaderElevationLayers.isEmpty()) // should not happen
					return;

				ContrastShadeModel contrastModel = shaderElevationLayers.get(0).getContrastControler().getModel();
				rasterInfoViewer.setInput(contrastModel.getData());
				rasterInfoViewer.addSelectionChangedListener(this::onDataTypeSelected);

				// select first enabled layer
				shaderElevationLayers.stream().filter(ShaderElevationLayer::isEnabled).findFirst()
						.ifPresent(layer -> rasterInfoViewer.setSelection(new StructuredSelection(layer.getType())));
			});
		}

		Consumer<WWFileLayerStoreEvent> listener = this::onSelectedLayerCreated;
		fileLayerStoreModel.addListener(listener);
		addDisposeListener(evt -> fileLayerStoreModel.removeListener(listener));
	}

	/**
	 * Selects currently displayed layer (enabled and on top)
	 */
	private void selectLayerOnTop() {
		var layersInGeographicView = geographicViewService.getLayers();
		var topIndex = getTerrainLayers().stream().filter(Layer::isEnabled).mapToInt(layersInGeographicView::indexOf)
				.max().orElse(-1);
		if (topIndex != -1) {
			var selectedLayer = layersInGeographicView.get(topIndex);
			// update combobox
			rasterInfos.stream()
					.filter(rasterInfo -> rasterInfo.getDataType().equals(((IWWTerrainLayer) selectedLayer).getType()))
					.findFirst() //
					.ifPresent(rasterInfo -> rasterInfoViewer.setSelection(new StructuredSelection(rasterInfo)));
			updateSubComposites((IWWTerrainLayer) selectedLayer);
		}
	}

	private List<IWWTerrainLayer> getTerrainLayers() {
		return fileLayerStoreModel
				.get(fileInfo).map(layerStore -> layerStore.getLayers().stream()
						.filter(IWWTerrainLayer.class::isInstance).map(IWWTerrainLayer.class::cast).toList())
				.orElse(Collections.emptyList());
	}

	private Optional<IWWTerrainLayer> getLayerFromDataType(String datatype) {
		return getTerrainLayers().stream().filter(l -> datatype.equals(l.getType())).findFirst();
	}

	private Optional<IWWTerrainLayer> getLayerFromRasterInto(RasterInfo rasterInfo) {
		return getLayerFromDataType(rasterInfo.getDataType());
	}

	/**
	 * Called when a {@link RasterInfo} (= layer) is selected.
	 */
	private void onRasterSelected(SelectionChangedEvent event) {
		IStructuredSelection selection = (IStructuredSelection) event.getSelection();
		selectedRasterInfo = (RasterInfo) selection.getFirstElement();

		// enable selected layer, disable others
		getTerrainLayers().forEach(l -> l.setEnabled(selectedRasterInfo.getDataType().equals(l.getType())));

		// update inner composites if layer already exists, else : launch event to generate a new layer
		getLayerFromRasterInto(selectedRasterInfo).ifPresentOrElse(this::updateSubComposites,
				() -> eventBroker.post(UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED, selectedRasterInfo));
	}

	/**
	 * When a new {@link RasterInfo} is selected, a layer is created. This method is called after the layer creation.
	 * 
	 * @param event
	 */
	private void onSelectedLayerCreated(WWFileLayerStoreEvent event) {
		if (selectedRasterInfo != null) {
			getLayerFromRasterInto(selectedRasterInfo).ifPresent(this::updateSubComposites);
			selectedRasterInfo = null;
		} else if (currentSelectedLayer != null && fileLayerStoreModel.get(currentSelectedLayer).isEmpty()) {
			// if current selected layer is no longer in model : refresh with top layer
			UIUtils.asyncExecSafety(this, this::selectLayerOnTop);
		}
	}

	/**
	 * Updates the layer parameter composites.
	 */
	private void updateSubComposites(IWWTerrainLayer selectedLayer) {
		currentSelectedLayer = selectedLayer;
		UIUtils.asyncExecSafety(terrainParametersComposite, () -> terrainParametersComposite.setModel(selectedLayer));
	}

	/**
	 * Method to switch between layers of old DTM.
	 *
	 * TODO: will be removed when the old DTM driver will be updated to provide RasterInfo (like DTM NetCDF 4)
	 */
	@Deprecated
	public void onDataTypeSelected(SelectionChangedEvent event) {
		IStructuredSelection selection = (IStructuredSelection) event.getSelection();
		String selectedDataType = (String) selection.getFirstElement();

		// disable other layers
		getTerrainLayers().forEach(layer -> layer.setEnabled(false));

		// get layer node to select
		getLayerFromDataType(selectedDataType).ifPresentOrElse(//
				// if layer exists: select its node & update inner composites
				layer -> {
					layer.setEnabled(true);
					updateSubComposites(layer);
				},
				// or launch event to generate a new layer
				() -> {
					File file = new File(fileInfo.getPath());
					ElevationLoader loader = new ElevationLoader();
					ElevationLoader.Context context = loader.new Context();
					context.setDataField(selectedDataType);
					BusyIndicator.showWhile(Display.getCurrent(), () -> {
						try {
							// Loading raster
							List<Pair<Layer, ElevationModel>> pairList = loader.load(file, context,
									new NullProgressMonitor());
							if (!pairList.isEmpty()) {
								var newLayers = new ArrayList<IWWLayer>();
								for (var pair : pairList) {
									var layer = pair.getFirst();
									if (layer instanceof ShaderElevationLayer) {
										var elevLayer = (ShaderElevationLayer) layer;
										elevLayer.setType(selectedDataType);
										newLayers.add(elevLayer);

										// add color scale layer
										var colorScaleLayer = IWWLayerFactory.grab().createColorScaleLayer(elevLayer,
												elevLayer.getName());
										newLayers.add(colorScaleLayer);

										updateSubComposites(elevLayer);
									}
								}
								var rasterInfo = new RasterInfo(null, file.getPath());
								fileLayerStoreModel.update(new WWRasterLayerStore(rasterInfo, newLayers, null));
							}
						} catch (Exception e) {
							LOGGER.debug("Error when swapping to {}", selectedDataType, e);
						}
					});
				});
	}

}
