/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain;

import java.awt.Point;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;

import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Set of IWWTerrainLayer sharing the same TerrainParameters
 */
public class CompoundTerrainLayer extends AbstractLayer implements IWWTerrainLayer {

	private final List<IWWTerrainLayer> components;

	/**
	 * Constructor
	 */
	public CompoundTerrainLayer(List<IWWTerrainLayer> components, String name, DoubleRange minMaxValues) {
		this.components = components;
		setName(name);
		setTextureParameters(TerrainParameters.defaultInstance(minMaxValues));
	}

	/** {@inheritDoc} */
	@Override
	protected void doRender(DrawContext dc) {
		components.forEach(layer -> layer.render(dc));
	}

	/** {@inheritDoc} */
	@Override
	public void setClearTextureCache(boolean clearFlag) {
		components.forEach(layer -> layer.setClearTextureCache(clearFlag));
	}

	@Override
	public String getType() {
		return components.get(0).getType();
	}

	/**
	 * @return the {@link #textureParameters}
	 */
	@Override
	public TerrainParameters getTextureParameters() {
		return components.get(0).getTextureParameters();
	}

	/**
	 * @param textureParameters the {@link #textureParameters} to set
	 */
	@Override
	public void setTextureParameters(TerrainParameters textureParameters) {
		components.forEach(layer -> layer.setTextureParameters(textureParameters));
	}

	/** {@inheritDoc} */
	@Override
	public boolean isEnabled() {
		return components.get(0).isEnabled();
	}

	/** {@inheritDoc} */
	@Override
	public boolean isPickEnabled() {
		return components.get(0).isPickEnabled();
	}

	/** {@inheritDoc} */
	@Override
	public void setPickEnabled(boolean pickable) {
		components.forEach(layer -> layer.setPickEnabled(pickable));
		super.setPickEnabled(pickable);
	}

	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {
		components.forEach(layer -> layer.setEnabled(enabled));
		super.setEnabled(enabled);
	}

	/** {@inheritDoc} */
	@Override
	public double getOpacity() {
		return components.get(0).getOpacity();
	}

	/** {@inheritDoc} */
	@Override
	public void preRender(DrawContext dc) {
		components.forEach(layer -> layer.preRender(dc));
		super.preRender(dc);
	}

	/** {@inheritDoc} */
	@Override
	public void pick(DrawContext dc, Point point) {
		components.forEach(layer -> layer.pick(dc, point));
		super.pick(dc, point);
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		components.forEach(Disposable::dispose);
		super.dispose();
	}

	/** {@inheritDoc} */
	@Override
	public void setExpiryTime(long expiryTime) {
		components.forEach(layer -> layer.setExpiryTime(expiryTime));
		super.setExpiryTime(expiryTime);
	}

	@Override
	public Optional<Range> getVisibleValueRange() {
		Range result = null;
		for (IWWTerrainLayer terrainLayer : components) {
			Range visibleValueRange = terrainLayer.getVisibleValueRange().orElse(null);
			if (visibleValueRange != null) {
				result = result != null
						? new DoubleRange(Math.min(result.getMinimumDouble(), visibleValueRange.getMinimumDouble()),
								Math.max(result.getMaximumDouble(), visibleValueRange.getMaximumDouble()))
						: visibleValueRange;
			}
		}
		return Optional.ofNullable(result);
	}

	@Override
	public boolean isVisibleInView() {
		return components.stream().anyMatch(IWWTerrainLayer::isVisibleInView);
	}

}
