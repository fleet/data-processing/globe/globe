/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain;

import java.awt.Color;
import java.awt.Point;
import java.net.URL;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import javax.swing.SwingUtilities;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.tile.GdalTiledTerrainProducer;
import fr.ifremer.viewer3d.util.tooltip.ToolTipPickedObject;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.cache.GpuResourceCache;
import gov.nasa.worldwind.layers.BasicTiledImageLayer;
import gov.nasa.worldwind.layers.TextureTile;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.render.DrawContext;
import jakarta.inject.Inject;

/**
 * Template method to render tiled WW layer for terrain data
 */
abstract class AbstractTerrainTextureLayer extends BasicTiledImageLayer implements IWWLayer {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(AbstractTerrainTextureLayer.class);

	/** No data value used in tiles */
	public static final double MISSING_VALUE = GdalTiledTerrainProducer.NO_DATA_VALUE.doubleValue();

	@Inject
	private TerrainShaderStore shaderStore;
	@Inject
	private IGeographicViewService geographicViewService;
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Values of uniforms in draw mode */
	private UniformModel drawUniformModel;

	private Map<String, Range> tilesStatistics = new HashMap<>();

	/** Min and max currently visible in the Geographic view */
	private Optional<Range> visibleValueRange = Optional.empty();

	/** True when the layer is visible in the Geographic view */
	private boolean visibleInView = false;

	/**
	 * Constructor.
	 */
	protected AbstractTerrainTextureLayer(Document document) {
		super(document, new AVListImpl());

		setUseMipMaps(true);
		setUseTransparentTextures(false);

		// For debugging purpose, set any property to true.
		setDrawTileIDs(false);
		setDrawTileBoundaries(false);
		setDrawBoundingVolumes(false);
		setPickEnabled(true);
	}

	/**
	 * Overrides to render tiles with the renderTiles method instead of delegating to SurfaceTileRenderer.<br>
	 * This is exactly the super implementation. Only the lines tagged GLOBE PATCH bring a change
	 */
	@Override
	protected void draw(DrawContext dc) {
		doDraw(dc, drawUniformModel);
	}

	/**
	 * Overrides to render tiles with the renderTiles method instead of delegating to SurfaceTileRenderer.<br>
	 * This is exactly the super implementation. Only the lines tagged GLOBE PATCH bring a change
	 */
	private void doDraw(DrawContext dc, UniformModel uniformModel) {
		if (uniformModel == null)
			return;

		GL2 gl = dc.getGL().getGL2();
		try {
			int shaderProgram = shaderStore.getShaderProgram(gl);
			gl.glUseProgram(shaderProgram);

			// bind the uniform samplers to texture units
			setUniform1i(gl, shaderProgram, "elevations", 0);
			setUniform1i(gl, shaderProgram, "slopes", 1 + TerrainTextureTile.AdditionalTextureUnitOffset);
			setUniform1i(gl, shaderProgram, "cosAspects", 2 + TerrainTextureTile.AdditionalTextureUnitOffset);
			setUniform1i(gl, shaderProgram, "sinAspects", 3 + TerrainTextureTile.AdditionalTextureUnitOffset);

			setUniform1f(gl, shaderProgram, "noDataValue", GdalTiledTerrainProducer.NO_DATA_VALUE.floatValue());

			setUniform1f(gl, shaderProgram, "opacity", (float) uniformModel.opacity);
			setUniform1f(gl, shaderProgram, "reverseColorMap", uniformModel.reverseColorMap);

			// transparency

			setUniform1f(gl, shaderProgram, "minTransparency",
					(float) (uniformModel.transparencyEnabled ? uniformModel.minTransparency
							: uniformModel.minTextureValue));
			setUniform1f(gl, shaderProgram, "maxTransparency",
					(float) (uniformModel.transparencyEnabled ? uniformModel.maxTransparency
							: uniformModel.maxTextureValue));

			// Min max values
			setUniform1f(gl, shaderProgram, "minContrast", (float) uniformModel.minContrast);
			setUniform1f(gl, shaderProgram, "maxContrast", (float) uniformModel.maxContrast);

			// Color map
			int[] colormap = uniformModel.colormap.toArray();
			setUniform1i(gl, shaderProgram, "colorMapLength", colormap.length);
			gl.glUniform1iv(gl.glGetUniformLocation(shaderProgram, "colormap"), colormap.length, colormap, 0);

			setUniform1f(gl, shaderProgram, "useShading", uniformModel.useShading);
			setUniform1f(gl, shaderProgram, "azimuth", (float) uniformModel.azimuth);
			setUniform1f(gl, shaderProgram, "zenith", (float) uniformModel.zenith);

			// TODO : can be simplified if textureParameters.shadingExaggeration is stored in log2 scale
			if (uniformModel.useLogarithmicShading) {
				setUniform1f(gl, shaderProgram, "shadingExaggeration", (float) uniformModel.shadingExaggeration);
			} else {
				setUniform1f(gl, shaderProgram, "shadingExaggeration",
						(float) Math.pow(4, Math.log10(uniformModel.shadingExaggeration)));
			}
			setUniform1f(gl, shaderProgram, "useLogarithmicShading", uniformModel.useLogarithmicShading);
			setUniform1f(gl, shaderProgram, "useGradient", uniformModel.useGradient);

			super.draw(dc);
			gl.glUseProgram(0);
		} catch (GIOException e) {
			logger.warn("Unable to load the shader program");
		}
	}

	/**
	 * Overrides to order tiles from lower level to higher. Remove fallback tiles to avoid tiles overlaying of poor
	 * quality.
	 */
	@Override
	protected void assembleTiles(DrawContext dc) {
		super.assembleTiles(dc);
		this.currentTiles.sort(levelComparer);
		this.currentTiles.stream().forEach(tile -> {
			if (this.levels.isResourceAbsent(tile)) {
				tile.setFallbackTile(null);
			}
		});

		// Reset the value Range and wait of texture binding to compute it
		visibleValueRange = Optional.empty();
	}

	/**
	 * Reacts to the fact that a texture has been bind
	 * 
	 * @param TerrainTextureTile bound texture. Only values are expected (Empty for slopes, cosAspects or sinAspects).
	 */
	protected void onBindTexture(GL2 gl, Optional<TextureTile> tileOfValues) {
		// Texture filtering parameters (define interpolation)
		int filtering = GL.GL_NEAREST;
		if (drawUniformModel != null && drawUniformModel.interpolationEnabled && Viewer3D.isInterpolate()) {
			filtering = GL.GL_LINEAR;
		}
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, filtering);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, filtering);

		// Update min and max of visible values
		tileOfValues.ifPresent(textureTile -> {
			Range tileStatistics = tilesStatistics.get(textureTile.getPath());
			if (tileStatistics != null) {
				Range globalTilesStatistics = visibleValueRange.orElse(null);
				if (globalTilesStatistics == null)
					visibleValueRange = Optional.of(tileStatistics);
				else
					visibleValueRange = Optional.of(new DoubleRange(
							Math.min(globalTilesStatistics.getMinimumDouble(), tileStatistics.getMinimumDouble()),
							Math.max(globalTilesStatistics.getMaximumDouble(), tileStatistics.getMaximumDouble())));
			}
		});
	}

	/** Top levels TextureTile have been created. Replace them by TerrainTextureTile */
	@Override
	protected void createTopLevelTiles() {
		super.createTopLevelTiles();

		// Change the Tile's implementation
		var wrappedTopLevels = new ArrayList<TextureTile>(topLevels.size());
		for (TextureTile textureTile : topLevels) {
			wrappedTopLevels.add(new TerrainTextureTile(textureTile, this));
		}
		topLevels = wrappedTopLevels;
	}

	/**
	 * Create a TextureData for the tile with the raw data contained in the Tiff file.
	 */
	@Override
	protected TextureData readTexture(URL tiffTile, String textureFormat, boolean useMipMaps) {
		TextureData result = null;
		String tilePath = tiffTile.toString();
		try {
			tilePath = Paths.get(tiffTile.toURI()).toFile().getPath();
			logger.debug("Loading tile {} ", tilePath);

			// Read float from tiff
			try (var gdalDataset = GdalDataset.open(tilePath)) {
				Optional<ByteBuffer> tileBuffer = gdalDataset.readAll(Float.class);
				if (tileBuffer.isPresent()) {
					result = new TextureData(Configuration.getMaxCompatibleGLProfile(), // GL profile
							GL.GL_ALPHA32F, // internal format
							gdalDataset.getWidth(), gdalDataset.getBandCount() * gdalDataset.getHeight(), // dimension
							0, // border
							GL.GL_ALPHA, // pixel format
							GL.GL_FLOAT, // pixel type
							useMipMaps, // mipmap
							false, // dataIsCompressed
							true, // mustFlipVertically : Tiff origin is Bottom/Left and texture is top/left
							tileBuffer.get(), null); // buffer, flusher
				}
			}
		} catch (Exception e) {
			logger.warn("Unable to load to tile {} : {}", tilePath, e.getMessage());
		}
		return result;
	}

	/**
	 * Specify the value of a float uniform variable
	 */
	protected void setUniform1f(GL2 gl, int shaderProgram, String uniforName, float value) {
		int location = gl.glGetUniformLocation(shaderProgram, uniforName);
		if (location < 0) {
			logger.error("Uniform {} not defined in the shader program", uniforName);
		} else {
			gl.glUniform1f(location, value);
		}
	}

	/**
	 * Specify the value of a int uniform variable
	 */
	protected void setUniform1i(GL2 gl, int shaderProgram, String uniforName, int value) {
		int location = gl.glGetUniformLocation(shaderProgram, uniforName);
		if (location < 0) {
			logger.error("Uniform {} not defined in the shader program", uniforName);
		} else {
			gl.glUniform1i(location, value);
		}
	}

	/**
	 * Specify the boolean value of a float uniform variable
	 */
	protected void setUniform1f(GL2 gl, int shaderProgram, String uniforName, boolean value) {
		int location = gl.glGetUniformLocation(shaderProgram, uniforName);
		if (location < 0) {
			logger.error("Uniform {} not defined in the shader program", uniforName);
		} else {
			gl.glUniform1f(location, value ? 1f : 0f);
		}
	}

	/** {@inheritDoc} */
	public void setClearTextureCache(boolean clearFlag) {
		if (clearFlag) {
			SwingUtilities.invokeLater(this::clearTextureCache);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		SwingUtilities.invokeLater(this::clearTextureCache);
	}

	/** Clean the WW cache */
	private void clearTextureCache() {
		var gpuResourceCache = geographicViewService.getWwd().getGpuResourceCache();
		if (gpuResourceCache != null) {
			GLContext context = geographicViewService.getWwd().getContext();
			if (context != null && !context.isCurrent()) {
				context.makeCurrent();
			}
			getTopLevels().forEach(tile -> clearTextureCache(gpuResourceCache, tile));
		}
	}

	/** Remove the tile from cache and try also to remove subtiles */
	private void clearTextureCache(GpuResourceCache gpuResourceCache, TextureTile tile) {
		((TerrainTextureTile) tile).removeFromCache(gpuResourceCache);
		if (tile.getLevelNumber() < levels.getNumLevels() - 1) {
			TextureTile[] subTiles = tile.createSubTiles(levels.getLevel(tile.getLevelNumber() + 1));
			for (TextureTile subTile : subTiles) {
				clearTextureCache(gpuResourceCache, subTile);
			}
		}
	}

	/** Simple Record to hold all terrain parameters */
	static record UniformModel(
			// Contrast
			double opacity, double minContrast, double maxContrast,
			// Color map
			IntList colormap, boolean reverseColorMap,
			// Shading
			boolean useShading, boolean useGradient, boolean useLogarithmicShading, double shadingExaggeration,
			double azimuth, double zenith,
			// Filtering
			boolean transparencyEnabled, double minTransparency, double maxTransparency, double minTextureValue,
			double maxTextureValue,
			// True to configure the texture minifying function of the shader program according to the interpolation
			boolean interpolationEnabled) {
	}

	/**
	 * @param model the {@link #drawUniformModel} to set
	 */
	protected void setUniformModel(UniformModel model) {
		this.drawUniformModel = model;
	}

	/**
	 * @return the {@link #visibleValueRange}
	 */
	public Optional<Range> getVisibleValueRange() {
		return isEnabled() && isVisibleInView() ? visibleValueRange : Optional.empty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.nasa.worldwind.layers.BasicTiledImageLayer#loadTexture(gov.nasa.worldwind.layers.TextureTile,
	 * java.net.URL)
	 */
	@Override
	protected boolean loadTexture(TextureTile tile, URL textureURL) {
		boolean result = super.loadTexture(tile, textureURL);
		if (result) {
			// Compute Min/Max value of the tile
			TextureData valuesData = tile.getTextureData();
			if (valuesData != null) {
				Buffer buffer = valuesData.getBuffer();
				if (buffer instanceof ByteBuffer byteBuffer) {
					FloatBuffer elevations = byteBuffer.asFloatBuffer();
					DoubleSummaryStatistics valuesStatistics = IntStream.range(0, elevations.capacity())
							.mapToDouble(elevations::get).filter(e -> Double.isFinite(e) && e != MISSING_VALUE)
							.summaryStatistics();
					if (valuesStatistics.getCount() > 0)
						this.tilesStatistics.put(tile.getPath(),
								new DoubleRange(valuesStatistics.getMin(), valuesStatistics.getMax()));
				}
			}
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (!enabled)
			this.visibleValueRange = Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public boolean isLayerInView(DrawContext dc) {
		visibleInView = super.isLayerInView(dc);
		return visibleInView;
	}

	/**
	 * @return the {@link #visibleInView}
	 */
	public boolean isVisibleInView() {
		return visibleInView;
	}

	@Override
	public boolean isPickEnabled() {
		return super.isPickEnabled() && geographicViewService.isDetailPickEnabled();
	}

	/** {@link WaterColumnRenderer} selection support. */
	private PickSupport pickSupport = new PickSupport();

	/**
	 * Draw in a single color for picking
	 */
	@Override
	public void doPick(DrawContext dc, Point pickPoint) {
		fileLayerStoreModel.getFileInfo(this).ifPresent(fileInfo -> {
			pickSupport.clearPickList();
			pickSupport.beginPicking(dc);
			try {
				// Generates a color map with only one color
				Color color = dc.getUniquePickColor();
				IntList colormap = new ArrayIntList(2);
				colormap.add(255 << 24 | color.getRGB());

				UniformModel uniformModel = new UniformModel(
						// Contrast
						1d, drawUniformModel.minTextureValue, drawUniformModel.maxTextureValue,
						// Color map
						colormap, false /* reverseColorMap */,
						// Shading
						false /* useShading */, false /* useGradient */, false /* useLogarithmicShading */,
						0d /* shadingExaggeration */, 0d /* azimuth */, 0d /* zenith */,
						// Filtering
						false /* transparencyEnabled */, 0d /* minTransparency */, 0d /* maxTransparency */,
						drawUniformModel.minTextureValue, drawUniformModel.maxTextureValue, //
						false // Ignore interpolation configuration
				);

				// Render the texture with only one color
				doDraw(dc, uniformModel);

				var pickObject = new ToolTipPickedObject(this, fileInfo);
				pickSupport.addPickableObject(color.getRGB(), pickObject);
			} finally {
				this.pickSupport.endPicking(dc);
				this.pickSupport.resolvePick(dc, pickPoint, this);
			}
		});
	}

	/**
	 * @return the {@link #fileLayerStoreModel}
	 */
	protected WWFileLayerStoreModel getFileLayerStoreModel() {
		return fileLayerStoreModel;
	}
}
