/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

import fr.ifremer.globe.ui.utils.Messages;
import gov.nasa.worldwind.cache.GpuResourceCache;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.TextureTile;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.TileKey;

/**
 * Redefinition of TextureTile.<br>
 * This class aim at replacing TextureTile produced by TiledImageLayer (ww). The goal is to inform the
 * AbstractTerrainTextureLayer when the tile is about to be bind.
 */
public class TerrainTextureTile extends TextureTile {

	/** Offset to avoid conflict with default alpha end outline texture and units in SurfaceTileRenderer */
	public static final int AdditionalTextureUnitOffset = 2;

	/** TerrainLayer to inform */
	protected final AbstractTerrainTextureLayer terrainLayer;

	/** Properties used to check cache memory **/
	private static final AtomicBoolean cacheWarningHasBeenDisplay = new AtomicBoolean();
	private static long cacheUsedCapacity;

	/**
	 * Constructor
	 */
	public TerrainTextureTile(TextureTile wrappedTextureTile, AbstractTerrainTextureLayer terrainLayer) {
		super(wrappedTextureTile.getSector(), wrappedTextureTile.getLevel(), wrappedTextureTile.getRow(),
				wrappedTextureTile.getColumn());
		this.terrainLayer = terrainLayer;
	}

	/**
	 * Constructor
	 */
	public TerrainTextureTile(Sector sector, Level level, int row, int col, AbstractTerrainTextureLayer terrainLayer) {
		super(sector, level, row, col);
		this.terrainLayer = terrainLayer;
	}

	/** {@inheritDoc} */
	@Override
	protected TerrainTextureTile createSubTile(Sector sector, Level level, int row, int col) {
		return new TerrainTextureTile(sector, level, row, col, terrainLayer);
	}

	/**
	 * Called by WW when this tile must be rendered
	 */
	@Override
	public boolean bind(DrawContext dc) {
		checkTextureMemoryCache(dc);

		GL2 gl = dc.getGL().getGL2();
		// Bind elevations
		gl.glActiveTexture(GL.GL_TEXTURE0);
		boolean result = super.bind(dc);
		terrainLayer.onBindTexture(gl, Optional.of(this));

		if (result) {
			GpuResourceCache tc = dc.getTextureCache();
			// 1=slopes, 2=cosAspects or 3=sinAspects
			for (int layer = 1; layer <= 3; layer++) {
				bindTexture(gl, tc, layer);
			}
		}
		// Restore default
		gl.glActiveTexture(GL.GL_TEXTURE0);
		return result;
	}

	/**
	 * Bind a texture found in cache.
	 * 
	 * @param layer is 1=slopes, 2=cosAspects or 3=sinAspects
	 */
	private void bindTexture(GL2 gl, GpuResourceCache tc, int layer) {
		Texture texture = tc.getTexture(getTileKey(layer));
		if (texture != null) {
			gl.glActiveTexture(GL.GL_TEXTURE0 + layer + AdditionalTextureUnitOffset);
			texture.bind(gl);
			terrainLayer.onBindTexture(gl, Optional.empty());
		}
	}

	/**
	 * Called by WW when this tile is not in cache<br>
	 * Returning a non-null instance means for WW that a new Texture must be created
	 */
	@Override
	public TextureData getTextureData() {
		TextureData result = super.getTextureData();
		if (result != null) {
			// Return the TextureData of elevations
			result = sliceTextureData(0, result);
		}
		return result;
	}

	/**
	 * Called by WW when the Texture (of elevations) have just been created and must be stored in the cache
	 */
	@Override
	public void setTexture(GpuResourceCache tc, Texture elevations) {
		// Calling super.setTexture will set the attribute textureData to null.
		// So before, prepare and store in cache the 3 other textures
		TextureData originalTextureData = super.getTextureData();

		// 1=slopes, 2=cosAspects or 3=sinAspects
		for (int layer = 1; layer <= 3; layer++) {
			Texture textures = TextureIO.newTexture(sliceTextureData(layer, originalTextureData));
			tc.put(getTileKey(layer), textures);
		}

		super.setTexture(tc, elevations);
	}

	/**
	 * Remove a bound textures from cache.
	 * 
	 * @param layer is 1=slopes, 2=cosAspects or 3=sinAspects
	 */
	public void removeFromCache(GpuResourceCache tc) {
		tc.remove(getTileKey());
		// 1=slopes, 2=cosAspects or 3=sinAspects
		for (int layer = 1; layer <= 3; layer++) {
			tc.remove(getTileKey(layer));
		}
	}

	/**
	 * @return the TileKey for the layer (1=slopes, 2=cosAspects, 3=sinAspects)<br>
	 *         TileKey of elevations can be obtained with getTileKey()
	 */
	private TileKey getTileKey(int layer) {
		TileKey tileKey = getTileKey();
		return new TileKey(tileKey.getLevelNumber(), tileKey.getRow(), tileKey.getColumn(),
				tileKey.getCacheName() + layer + "_" + layer);
	}

	/**
	 * Check if the texture cache memory is not full, else if : a warning dialog is opened.
	 * 
	 * The warning dialog is opened only one time.
	 */
	private void checkTextureMemoryCache(DrawContext dc) {
		if (cacheWarningHasBeenDisplay.compareAndSet(false, true)) {
			var cache = dc.getTextureCache();
			if (cacheUsedCapacity != cache.getUsedCapacity()) {
				cacheUsedCapacity = cache.getUsedCapacity();
				if (((float) cacheUsedCapacity / cache.getCapacity()) > 0.8f) {
					Messages.openWarningMessage("Geographic View cache memory full",
							"Warning : the memory cache used by the Geographic View is almost full."
									+ " Please, increase the texture cache capacity from 'Window' > 'Preferences Nasa'.");
					return;
				}
			}
			cacheWarningHasBeenDisplay.set(false);
		}
	}

	private TextureData sliceTextureData(int layer, TextureData textureData) {
		int textureLength = textureData.getBuffer().capacity() / 4;
		var buffer = (ByteBuffer) textureData.getBuffer().slice(textureLength * layer, textureLength);
		buffer.order(ByteOrder.nativeOrder());
		return new TextureData(textureData.getGLProfile(), // GL profile
				textureData.getInternalFormat(), // internal format
				textureData.getWidth(), textureData.getHeight() / 4, // dimension
				textureData.getBorder(), // border
				textureData.getPixelFormat(), // pixel format
				textureData.getPixelType(), // pixel type
				textureData.getMipmap(), // mipmap
				textureData.isDataCompressed(), // dataIsCompressed
				textureData.getMustFlipVertically(), //
				buffer, null); // buffer, flusher
	}
}
