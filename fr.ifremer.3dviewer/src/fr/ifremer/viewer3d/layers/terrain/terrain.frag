#version 130
uniform sampler2D elevations;
uniform sampler2D slopes;
uniform sampler2D cosAspects;
uniform sampler2D sinAspects;

uniform float noDataValue;
uniform float minTextureValue;
uniform float maxTextureValue;
uniform float minTransparency;
uniform float maxTransparency;

uniform float minContrast;
uniform float maxContrast;
uniform float reverseColorMap;
uniform int colorMapLength;
uniform int colormap[256];
 
uniform float useShading;
// Azimuth of the sun in degrees to the east of north (-180 180)
uniform float azimuth;
// Altitude of the sun in degrees above the horizon (0 90)
uniform float zenith;
// shading exaggeration in linear scale (0 100000) 
uniform float shadingExaggeration;
uniform float useLogarithmicShading;
uniform float useGradient;
uniform float cellSize;

uniform float width;
uniform float height;

uniform float opacity;
uniform float ambient = 0.2;

//compute the index of colormap according to contrast and colorMapLength
vec4 computeColorIndex(float contrast) {
	vec4 demultiplexed = vec4(0.0, 0.0, 0.0, 1.0);
	int index = int((colorMapLength - 1.0) * contrast);
	if (index < 0)
		index = 0;
	if (index > colorMapLength - 1)
		index = colorMapLength - 1; 
	int colorMultiplexed = colormap[index];

	demultiplexed.b = float(colorMultiplexed & 255) / 255.0 * opacity;
	demultiplexed.g = float((colorMultiplexed>> 8) & 255) / 255.0 * opacity;
	demultiplexed.r = float((colorMultiplexed>>16) & 255) / 255.0 * opacity;
	demultiplexed.a = float((colorMultiplexed>>24) & 255) / 255.0 * opacity;

	return demultiplexed;
}

void main() {
	vec2 st = gl_TexCoord[0].st;

	//suppression de l'interpolation en alpha sur les bords des tuiles
	if (st.s < 0.0 || st.s > 1.0 || st.t < 0.0 || st.t > 1.0)
		discard;

	// Get value from the texture
	float value = texture2D(elevations, st).a;
	
	// discard no data values (== -Float.MAX)
	if(isnan(value) || value == noDataValue)
		discard;
		
	// if value is very low, it is due to an interpolation between real value and "no data" value
	// compute S & T to get the real value of this fragment without interpolation (= get texture cell center color)
	if(value < -10000){
		vec2 size = textureSize(elevations,0);
		float cellWidth = 1 / size.x;
		float cellHeight = 1 / size.y;
		float cellCenterS = st.s - mod(st.s, cellWidth) + cellWidth/2;
		float cellCenterT = st.t - mod(st.t, cellHeight) + cellHeight/2;
		st = vec2(cellCenterS,cellCenterT);
		value = texture2D(elevations, st).a;
	}
	
	// Filter values
	if (value < minTransparency || value > maxTransparency)
		discard;

	// Apply color contrast
	float normalizedValue = (value - minContrast) / (maxContrast - minContrast);
	normalizedValue = max(min(normalizedValue, 1.0), 0.0);
	if (reverseColorMap == 1.0) 
		normalizedValue = 1.0 - normalizedValue;
	
	// Shading
	float shadingGradient = 1.0;
	if( useShading == 1.0) {
		float sunAltitude = radians(90.0 - zenith);
		float sunAzimuth = radians(90.0 - azimuth);
	
		// get slope from green value
		float slope = texture2D(slopes, st).a;
		
		// apply exaggeration
		float exaggeration = shadingExaggeration;
		if(useLogarithmicShading == 1){
 			exaggeration = log(1.0 + exaggeration)/log(10.0);
		}
		slope = radians(90.0) - atan(exaggeration*tan(radians(slope)));
		
		// get cos/sin of aspect from blue and alpha values (use of cos/sin to avoid issues with angle interpolation)
		// 0° means East, 90° North, 180° West, 270° South.
		float cosAspect = texture2D(cosAspects, st).a;
		float sinAspect = texture2D(sinAspects, st).a;
		
		shadingGradient = sin(sunAltitude) * sin(slope) + cos(sunAltitude) * cos(slope) * (cos(sunAzimuth) * cosAspect + sin(sunAzimuth) * sinAspect);
		shadingGradient = max(0.0, shadingGradient);
	} 

	if (useGradient == 1.0) {
		gl_FragColor.rgba = vec4(shadingGradient, shadingGradient, shadingGradient, opacity);
	} else {
		shadingGradient = ambient + (1.0 - ambient)*shadingGradient;
		// Apply color to the fragment
		vec4 color = computeColorIndex(normalizedValue);
		gl_FragColor.rgba = vec4(color.r * shadingGradient, color.g *shadingGradient, color.b *shadingGradient, color.a);
	}
	
}
