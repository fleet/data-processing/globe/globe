/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain.contrast;

import java.util.HashSet;
import java.util.Set;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.sync.dtm.WWTerrainLayerSynchronizer;

/**
 * Compute of contrast on visible part of Terrain Layer.
 *
 */
@Singleton
@Creatable
public class TerrainContrastController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TerrainContrastController.class);

	/** Set of IWWTerrainLayer available in FileLayerStoreModel */
	private final Set<IWWTerrainLayer> terrainLayers = new HashSet<>();

	@PostConstruct
	private void postConstruct(WWFileLayerStoreModel fileLayerStoreModel) {
		fileLayerStoreModel.addListener(this::onFileLayerStoreEvent);
	}

	/**
	 * Set contrast to an estimation of min and max elevation of the visible terrain layers.
	 */
	public void computeContrast() {
		if (terrainLayers.isEmpty()) {
			LOGGER.debug("No terrain layer loaded. Contrast computation aborted");
			return;
		}

		Range globalValueRange = null;
		for (IWWTerrainLayer terrainLayer : terrainLayers) {
			if (terrainLayer.isVisibleInView() && terrainLayer.isEnabled()) {
				Range visibleElevationRange = terrainLayer.getVisibleValueRange().orElse(null);
				if (visibleElevationRange != null) {
					globalValueRange = globalValueRange != null ? new DoubleRange(
							Math.min(globalValueRange.getMinimumDouble(), visibleElevationRange.getMinimumDouble()),
							Math.max(globalValueRange.getMaximumDouble(), visibleElevationRange.getMaximumDouble()))
							: visibleElevationRange;
				}
			}
		}

		if (globalValueRange != null) {
			LOGGER.info("Contrast set to [{}, {}] for all visible layers", globalValueRange.getMinimumDouble(),
					globalValueRange.getMaximumDouble());
			var terrainLayerSynchronizer = ContextInitializer.getInContext(WWTerrainLayerSynchronizer.class);
			for (IWWTerrainLayer terrainLayer : terrainLayers) {
				terrainLayer.setTextureParameters(terrainLayer.getTextureParameters()
						.withContrast(globalValueRange.getMinimumDouble(), globalValueRange.getMaximumDouble()));
				terrainLayerSynchronizer.synchonizeWith(terrainLayer);
			}
		}
	}

	/**
	 * Reset contrast to min and max elevation of the visible terrain layers.
	 */
	public void resetContrast() {
		var terrainLayerSynchronizer = ContextInitializer.getInContext(WWTerrainLayerSynchronizer.class);
		for (IWWTerrainLayer terrainLayer : terrainLayers) {
			TerrainParameters terrainParameters = terrainLayer.getTextureParameters();
			terrainLayer.setTextureParameters(terrainLayer.getTextureParameters()
					.withContrast(terrainParameters.minTextureValue, terrainParameters.maxTextureValue));
			terrainLayerSynchronizer.synchonizeWith(terrainLayer);
		}
	}

	/** React on a layer model changes */
	private void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		synchronized (terrainLayers) {
			switch (event.getState()) {
			case ADDED -> event.getFileLayerStore().getLayers(IWWTerrainLayer.class)//
					.filter(l -> RasterInfo.RASTER_ELEVATION.equals(l.getType()))//
					.forEach(terrainLayers::add);
			case UPDATED -> event.getUpdatedLayers().stream()//
					.filter(IWWTerrainLayer.class::isInstance)//
					.map(IWWTerrainLayer.class::cast)//
					.filter(l -> RasterInfo.RASTER_ELEVATION.equals(l.getType()))//
					.forEach(terrainLayers::add);
			case REMOVED -> event.getFileLayerStore().getLayers(IWWTerrainLayer.class).forEach(terrainLayers::remove);
			}

		}
	}

}
