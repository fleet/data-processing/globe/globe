/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain;

import java.awt.Point;
import java.util.List;

import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainWithPaletteParameters;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Set of IWWTerrainWithPaletteLayer sharing the same TerrainWithPaletteParameters
 */
public class CompoundTerrainWithPaletteLayer extends AbstractLayer implements IWWTerrainWithPaletteLayer {

	private final List<IWWTerrainWithPaletteLayer> components;

	/**
	 * Constructor
	 */
	public CompoundTerrainWithPaletteLayer(List<IWWTerrainWithPaletteLayer> components, String name) {
		this.components = components;
		setName(name);
	}

	/** {@inheritDoc} */
	@Override
	protected void doRender(DrawContext dc) {
		components.forEach(layer -> layer.render(dc));
	}

	/** {@inheritDoc} */
	@Override
	public void setClearTextureCache(boolean clearFlag) {
		components.forEach(layer -> layer.setClearTextureCache(clearFlag));
	}

	/**
	 * @return the {@link #textureParameters}
	 */
	@Override
	public TerrainWithPaletteParameters getTextureParameters() {
		return components.get(0).getTextureParameters();
	}

	/**
	 * @param textureParameters the {@link #textureParameters} to set
	 */
	@Override
	public void setTextureParameters(TerrainWithPaletteParameters textureParameters) {
		components.forEach(layer -> layer.setTextureParameters(textureParameters));
	}

	/** {@inheritDoc} */
	@Override
	public boolean isEnabled() {
		return components.get(0).isEnabled();
	}

	/** {@inheritDoc} */
	@Override
	public boolean isPickEnabled() {
		return components.get(0).isPickEnabled();
	}

	/** {@inheritDoc} */
	@Override
	public void setPickEnabled(boolean pickable) {
		components.forEach(layer -> layer.setPickEnabled(pickable));
		super.setPickEnabled(pickable);
	}

	/** {@inheritDoc} */
	@Override
	public void setEnabled(boolean enabled) {
		components.forEach(layer -> layer.setEnabled(enabled));
		super.setEnabled(enabled);
	}

	/** {@inheritDoc} */
	@Override
	public double getOpacity() {
		return components.get(0).getOpacity();
	}

	/** {@inheritDoc} */
	@Override
	public void preRender(DrawContext dc) {
		components.forEach(layer -> layer.preRender(dc));
		super.preRender(dc);
	}

	/** {@inheritDoc} */
	@Override
	public void pick(DrawContext dc, Point point) {
		components.forEach(layer -> layer.pick(dc, point));
		super.pick(dc, point);
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		components.forEach(Disposable::dispose);
		super.dispose();
	}

	/** {@inheritDoc} */
	@Override
	public void setExpiryTime(long expiryTime) {
		components.forEach(layer -> layer.setExpiryTime(expiryTime));
		super.setExpiryTime(expiryTime);
	}

}
