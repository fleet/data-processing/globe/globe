/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain;

import jakarta.inject.Inject;

import org.w3c.dom.Document;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainWithPaletteParameters;
import fr.ifremer.globe.utils.map.TypedMap;

/**
 * Tiled WW layer to display terrain data
 */
public class TerrainWithPatletteLayer extends AbstractTerrainTextureLayer implements IWWTerrainWithPaletteLayer {

	@Inject
	private ISessionService sessionService;

	/** All rendering parameters */
	private TerrainWithPaletteParameters textureParameters;

	/**
	 * Constructor.
	 */
	public TerrainWithPatletteLayer(Document document, AColorPalette palette) {
		super(document);

		textureParameters = new TerrainWithPaletteParameters(1f, palette);
		adaptUniformModelToParameters();
	}

	/**
	 * Create and set a new instance of UniformModel for SuperClass
	 */
	private void adaptUniformModelToParameters() {
		double minValue = textureParameters.palette().getMinValue();
		double maxValue = textureParameters.palette().getMaxValue();
		UniformModel newModel = new UniformModel(
				// Contrast
				textureParameters.opacity(), minValue, maxValue,
				// Color map
				textureParameters.palette().toColorMap(), false /* reverseColorMap */,
				// Shading
				false /* useShading */, false /* useGradient */, false /* useLogarithmicShading */,
				0d /* shadingExaggeration */, 0d /* azimuth */, 0d /* zenith */,
				// Filtering
				false /* transparencyEnabled */, 0d /* minTransparency */, 0d /* maxTransparency */,
				minValue /* minTextureValue */, maxValue /* maxTextureValue */, //
				false // Ignore interpolation configuration
		);
		setUniformModel(newModel);
	}

	/** {@inheritDoc} */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(getTextureParameters().toTypedEntry());
	}

	/** {@inheritDoc} */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(TerrainWithPaletteParameters.SESSION_KEY).ifPresent(p -> {
			textureParameters = p;
			adaptUniformModelToParameters();
		});
	}

	/**
	 * @return the {@link #textureParameters}
	 */
	@Override
	public TerrainWithPaletteParameters getTextureParameters() {
		return textureParameters;
	}

	/**
	 * @param textureParameters the {@link #textureParameters} to set
	 */
	@Override
	public void setTextureParameters(TerrainWithPaletteParameters newParams) {
		textureParameters = newParams;
		adaptUniformModelToParameters();
		sessionService.saveSession();
	}

}
