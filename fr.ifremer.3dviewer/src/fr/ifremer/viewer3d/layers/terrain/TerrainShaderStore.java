/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.terrain;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Singleton;
import com.jogamp.opengl.GL2;

import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.ogl.util.ShaderStore;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Manage shaders for rendering TerrainLayers
 */
@Singleton
@Creatable
public class TerrainShaderStore {

	/** Render shader program. */
	private static final String TEXTURE_VERT_SHADER = TerrainShaderStore.class.getResource("terrain.vert").getFile();
	private static final String TEXTURE_FRAG_SHADER = TerrainShaderStore.class.getResource("terrain.frag").getFile();

	/** Real store */
	private final ShaderStore shaderStore;

	/**
	 * Constructor
	 */
	public TerrainShaderStore() {
		shaderStore = new ShaderStore(gl -> ShaderUtil.createShader(this, gl, TEXTURE_VERT_SHADER, TEXTURE_FRAG_SHADER));
	}

	/**
	 * Constructor
	 */
	@PostConstruct
	public void postConstruct(WWFileLayerStoreModel fileLayerStoreModel) {
		fileLayerStoreModel.addListener(this::onFileLayerStoreEvent);
	}

	/**
	 * retrieve the shader program associated with the current GL context
	 *
	 * @param gl OpenGL environment context
	 * @return the ID of the shader program used by this renderer
	 * @throws GIOException if an error occured during compilation *
	 */
	public int getShaderProgram(GL2 gl) throws GIOException {
		return shaderStore.getShaderProgram(gl);
	}

	/**
	 * Cleaning
	 */
	@PreDestroy
	void dispose() {
		// shaderStore.dispose();
	}

	/** React on a layer model changes */
	protected void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		// delete the shader program
		if (event.getState() == FileLayerStoreState.REMOVED) {
			shaderStore.dispose();
		}
	}

}
