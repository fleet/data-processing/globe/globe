/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.point.render;

import gov.nasa.worldwind.avlist.AVListImpl;

/**
 * Index of the point picked in a PointRendere
 */
public class PickedPointIndex extends AVListImpl {
	private final PointRenderer pointRenderer;
	private final int pointIndex;

	/**
	 * Constructor
	 */
	public PickedPointIndex(PointRenderer pointRenderer, int pointIndex) {
		this.pointRenderer = pointRenderer;
		this.pointIndex = pointIndex;
	}

	/**
	 * @return the {@link #pointRenderer}
	 */
	public PointRenderer getPointRenderer() {
		return pointRenderer;
	}

	/**
	 * @return the {@link #pointIndex}
	 */
	public int getPointIndex() {
		return pointIndex;
	}

}
