/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.point.render;

import java.util.Optional;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget.Model;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;

/**
 * Rendering parameters of the PointRenderer layer
 */
public record PointRendererParameters(
		/** Point width. Same for all points */
		float width,
		/** Opacity. Same for all points */
		float opacity,
		/** Color by default. Same for all points */
		GColor defaultColor,
		/** Color model to apply to each point */
		Optional<ColorContrastModel> colorContrastModel, //
		/** Model to compute point size */
		Optional<Model> variablePointSizeModel) {

	/**
	 * @return a default instance of parameters
	 */
	public static PointRendererParameters getDefault() {
		return new PointRendererParameters(2f, 1f, GColor.RED, Optional.empty(), Optional.empty());

	}
}
