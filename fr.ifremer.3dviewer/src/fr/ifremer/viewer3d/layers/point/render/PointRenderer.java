/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.point.render;

import java.awt.Color;
import java.awt.Point;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.IntFunction;

import javax.swing.SwingUtilities;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;
import com.jogamp.opengl.util.texture.Texture;

import fr.ifremer.globe.core.model.geo.GeoBoxBuilder;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ogl.util.PaletteTexture;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget.Model;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.FloatIndexedSupplier;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.selection.WWLayerSelectionController;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.SceneController;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import jakarta.inject.Inject;

/**
 * Layer to render plain points.
 */
@Creatable
public class PointRenderer implements OrderedRenderable, Disposable {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(PointRenderer.class);

	/** Shader programs source code */
	private static final String SHADER_VERT = PointRenderer.class.getResource("shader/point.vert").getFile();
	private static final String SHADER_FRAG = PointRenderer.class.getResource("shader/point.frag").getFile();

	/** Attribute names in shaders */
	private static final String INDEX_ATTRIB_NAME = "index";
	private static final String VALUE_ATTRIB_NAME = "value";

	@Inject
	private IGeographicViewService geographicViewService;
	@Inject
	private WWLayerSelectionController layerSelectionController;

	/** Picking management */
	private PickSupport pickSupport = new PickSupport();
	/** Supplier of cursor. Invoked with the index of the selected point. */
	private IntFunction<GlobalCursor> globalCursorSupplier;
	/** Current layer during picking process */
	private Layer pickLayer;

	/** Global sector containing the points */
	private Sector sector = Sector.EMPTY_SECTOR;

	/** True to enable the render */
	private boolean visible;
	/** True when points are selected */
	private boolean selected;

	/** Rendering parameters */
	private PointRendererParameters parameters = PointRendererParameters.getDefault();

	/** Shader programs used by this renderer */
	private int shaderProgram = 0;
	/** Used to save/restore OpenGL ES state */
	private float[] pointSizeToRestore = new float[1];

	/** VBO of positions */
	private final VBO vertexBuffer = new VBO(3);
	/** VBO of values */
	private final VBO valueBuffer = new VBO(1);
	/** VBO of index of points */
	private final VBO indexBuffer = new VBO(1);
	/** True when model of point has changed */
	private AtomicBoolean modelHasChanged = new AtomicBoolean();

	/** Palette of color */
	private final PaletteTexture palette = new PaletteTexture();

	private Vec4 referencePoint;

	/**
	 * Accept a set of points
	 * 
	 * @param positionSupplier Supplier of positions (lon/lat/elevation)
	 * @param valueSupplier Value of each point. Floats used to compute the color if the contrastModel.
	 */
	public <T extends Position> void acceptPoints(int pointCount, IntFunction<T> positionSupplier,
			FloatIndexedSupplier valueSupplier, boolean clampToGround, double verticalExaggeration) {
		logger.debug("Accepting a new set of {} points", pointCount);
		computeSectorAndReferencePoint(pointCount, positionSupplier, clampToGround);

		vertexBuffer.allocateBuffer(pointCount);
		valueBuffer.allocateBuffer(pointCount);
		indexBuffer.allocateBuffer(pointCount);
		for (int index = 0; index < pointCount; index++) {
			Position position = positionSupplier.apply(index);
			double elevation = position.elevation;
			if (clampToGround) {
				elevation = geographicViewService.getElevationModel().getElevation(position.latitude,
						position.longitude);
			}
			Vec4 coords = geographicViewService.computePointFromPosition(position, elevation * verticalExaggeration);
			coords = coords.subtract3(referencePoint);
			vertexBuffer.put(coords.x);
			vertexBuffer.put(coords.y);
			vertexBuffer.put(coords.z);
			valueBuffer.put(valueSupplier.getAsFloat(index));
			indexBuffer.put(index);
		}
		vertexBuffer.rewind();
		valueBuffer.rewind();
		indexBuffer.rewind();

		modelHasChanged.set(true);
	}

	/**
	 * Remove all points
	 */
	public void clear() {
		vertexBuffer.clear();
		valueBuffer.clear();
		indexBuffer.clear();
	}

	/** {@inheritDoc} */
	@Override
	public void render(DrawContext dc) {
		// Are the conditions met to draw
		if (!isVisible() || vertexBuffer.getElementCount() == 0 || !sector.intersects(dc.getVisibleSector())) {
			// Free memory
			GL2 gl = dc.getGL().getGL2();
			disposeVBO(gl);
			return;
		}

		// Does SceneController is currently rendering OrderedRenderable ?
		if (!dc.isOrderedRenderingMode()) {
			pickLayer = dc.getCurrentLayer();
			dc.addOrderedSurfaceRenderable(this);
		} else {
			draw(dc, 0, false);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void pick(DrawContext dc, Point pickPoint) {
		try {
			this.pickSupport.beginPicking(dc);
			// On rectangular selection, same color for all points
			if (layerSelectionController.isRectangularSelection()) {
				Color pickedColor = dc.getUniquePickColor();
				if (pickedColor != null) {
					int pickedRgb = pickedColor.getRGB();
					pickSupport.addPickableObject(pickedRgb, new PickedPointIndex(this, pickedRgb));
					draw(dc, pickedRgb, false);
				} else {
					logger.error("Not enough colors to satisfy the requested picking range.");
				}
			} else {
				Color firstPickedColor = dc.getUniquePickColorRange(vertexBuffer.getElementCount());
				if (firstPickedColor != null) {
					int firstPickedRgb = firstPickedColor.getRGB();
					this.pickSupport.addPickableObjectRange(firstPickedRgb, vertexBuffer.getElementCount(), color -> {
						PickedPointIndex pickedPointIndex = new PickedPointIndex(this, color - firstPickedRgb);
						var result = new PickedObject(color, pickedPointIndex);
						result.setParentLayer(this.pickLayer);
						if (globalCursorSupplier != null)
							pickedPointIndex.setValue(GlobalCursor.AV_KEY,
									globalCursorSupplier.apply(pickedPointIndex.getPointIndex()));
						return result;
					});
					draw(dc, firstPickedRgb, true);
				} else {
					logger.error("Not enough colors to satisfy the requested picking range.");
				}
			}
		} finally {
			this.pickSupport.endPicking(dc);
			this.pickSupport.resolvePick(dc, pickPoint, this.pickLayer);
		}
	}

	/**
	 * Draw the points
	 * 
	 * @param firstPickedRgb : when positive, indicates the first color to apply to the first point (instead of colorize
	 *            with the palette).
	 * @param multiPickedColor false to use firtsPickedRgb for all points
	 */
	private void draw(DrawContext dc, int firstPickedRgb, boolean multiPickedColor) {
		GL2 gl = dc.getGL().getGL2();
		try {
			dc.getView().pushReferenceCenter(dc, referencePoint);
			beginDrawing(gl);
			doDraw(gl, firstPickedRgb, multiPickedColor);
		} catch (GIOException | GLException e) {
			logger.error("Unable to draw lines layer {}", e.getMessage());
		} finally {
			endDrawing(gl);
			dc.getView().popReferenceCenter(dc);
		}
	}

	/**
	 * Before drawing
	 */
	private void beginDrawing(GL2 gl) {
		gl.glPushAttrib(GL.GL_DEPTH_BUFFER_BIT | GL.GL_BLEND | GL3.GL_PROGRAM_POINT_SIZE);
		gl.glGetFloatv(GL.GL_POINT_SIZE, pointSizeToRestore, 0);

		// For opacity
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);

		// Enable gl_PointSize in shader
		if (parameters.variablePointSizeModel().isPresent())
			gl.glEnable(GL3.GL_PROGRAM_POINT_SIZE);
	}

	/**
	 * Restore drawing state changed in beginDrawing to the default.
	 */
	private void endDrawing(GL2 gl) {
		gl.glPopAttrib();
		gl.glPointSize(pointSizeToRestore[0]);
	}

	/**
	 * Render the points
	 */
	private void doDraw(GL2 gl, int firstPickedRgb, boolean multiPickedColor) throws GIOException {
		// Get the shader program ID for the future drawing
		int shaderProgramID = getShaderProgramID(gl);
		// Initialize the uniforms of the shader program
		gl.glUseProgram(shaderProgramID);

		// Bind VBOs
		if (modelHasChanged.compareAndSet(true, false)) {
			disposeVBO(gl);
		}
		vertexBuffer.bindAndLoad(gl);
		valueBuffer.bindAndLoad(gl);
		indexBuffer.bindAndLoad(gl);

		// Draw the OpenGL shape from the initialized buffers
		configureShaderAndDraw(gl, shaderProgramID, 1, firstPickedRgb, multiPickedColor);

		// Returns to the main shader program
		if (shaderProgramID != 0) {
			gl.glUseProgram(0);
		}
	}

	/**
	 * Configure all the OpenGL drawing variables.
	 */
	private void configureShaderAndDraw(GL2 gl, int shaderProgram, float verticalExaggeration, int firstPickedRgb,
			boolean multiPickedColor) {
		// Initialize the pointer to buffers
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glEnableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, VALUE_ATTRIB_NAME));
		gl.glEnableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, INDEX_ATTRIB_NAME));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "verticalExaggeration"), verticalExaggeration);

		// Default color
		GColor defaultColor = selected ? GColor.WHITE : parameters.defaultColor();
		gl.glUniform4f(gl.glGetUniformLocation(shaderProgram, "defaultColor"), defaultColor.getFloatRed(),
				defaultColor.getFloatGreen(), defaultColor.getFloatBlue(), defaultColor.getAlpha());

		// First color code in case of picking
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "firstPickedColor"), firstPickedRgb);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "multiPickedColor"), multiPickedColor ? 1f : 0f);

		// Opacity
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), parameters.opacity());

		// Contrast model. When absent, set contrast range to NaN (forcing shader to use default color).
		ColorContrastModel colorContrastModel = parameters.colorContrastModel()
				.orElse(new ColorContrastModelBuilder().setContrastMin(Float.NaN).build());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minContrast"), colorContrastModel.contrastMin());
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxContrast"), colorContrastModel.contrastMax());

		// sending color map texture to the GPU
		Texture paletteTexture = palette.getColorPalette(gl, colorContrastModel.colorMapIndex(),
				colorContrastModel.invertColor());

		gl.glActiveTexture(GL.GL_TEXTURE0);
		paletteTexture.bind(gl);

		// Increase the size of point in case of picking to make it easier to select
		float pointSize = firstPickedRgb != 0 ? parameters.width() + 4f : parameters.width();
		pointSize = selected ? 1.5f * pointSize : pointSize;

		// variable point size
		if (parameters.variablePointSizeModel().isPresent()) {
			Model valueModel = parameters.variablePointSizeModel().get();
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minValue"), colorContrastModel.limitMin());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxValue"), colorContrastModel.limitMax());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minSize"), valueModel.min());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxSize"), valueModel.max());
		}

		drawPoints(gl, pointSize);

		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glDisableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, VALUE_ATTRIB_NAME));
		gl.glDisableVertexAttribArray(gl.glGetAttribLocation(shaderProgram, INDEX_ATTRIB_NAME));
	}

	/** Draw all lines and points */
	private void drawPoints(GL2 gl, float pointSize) {
		// Vertices
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vertexBuffer.getBufferName()[0]);
		gl.glVertexPointer(vertexBuffer.getElementSize(), GL.GL_FLOAT, 0, 0);

		// Values
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, valueBuffer.getBufferName()[0]);
		gl.glVertexAttribPointer(gl.glGetAttribLocation(shaderProgram, VALUE_ATTRIB_NAME), valueBuffer.getElementSize(),
				GL.GL_FLOAT, false, 0, 0);

		// Index
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, indexBuffer.getBufferName()[0]);
		gl.glVertexAttribPointer(gl.glGetAttribLocation(shaderProgram, INDEX_ATTRIB_NAME), indexBuffer.getElementSize(),
				GL.GL_FLOAT, false, 0, 0);

		gl.glPointSize(pointSize);
		gl.glDrawArrays(GL.GL_POINTS, 0, vertexBuffer.getElementCount());

		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
	}

	/**
	 * Compute the sector and reference point for the set of points
	 */
	private <T extends Position> void computeSectorAndReferencePoint(int pointCount, IntFunction<T> positionSupplier,
			boolean clampToGround) {
		GeoBoxBuilder geoBoxBuilder = new GeoBoxBuilder();
		double minElevation = Double.POSITIVE_INFINITY;
		double maxElevation = Double.NEGATIVE_INFINITY;
		for (int index = 0; index < pointCount; index++) {
			Position position = positionSupplier.apply(index);
			double elevation = position.elevation;
			if (clampToGround) {
				elevation = geographicViewService.getElevationModel().getElevation(position.latitude,
						position.longitude);
			}
			minElevation = Math.min(minElevation, elevation);
			maxElevation = Math.max(maxElevation, elevation);
			geoBoxBuilder.addPoint(position.longitude.degrees, position.latitude.degrees);
		}
		sector = GeoBoxUtils.convert(geoBoxBuilder.build());

		referencePoint = geographicViewService
				.computePointFromPosition(new Position(sector.getCentroid(), (minElevation + maxElevation) / 2d));

	}

	/**
	 * Get the ID of the shader program used by this renderer.
	 */
	private int getShaderProgramID(GL2 gl) throws GIOException {
		if (shaderProgram == 0) {
			// Compile and generate a shader program ID and add it to the shader prodram renderer list
			shaderProgram = ShaderUtil.createShader(this, gl, SHADER_VERT, SHADER_FRAG);
		}

		return shaderProgram;
	}

	/**
	 * @see gov.nasa.worldwind.layers.AbstractLayer#dispose()
	 */
	@Override
	public void dispose() {
		SwingUtilities.invokeLater(() -> {
			GLContext glContext = getGLContext();
			if (glContext != null) {

				if (shaderProgram != 0) {
					glContext.getGL().getGL2().glDeleteShader(shaderProgram);
					shaderProgram = 0;
				}
				disposeVBO(glContext.getGL());
				glContext.release();
			}
		});
	}

	private void disposeVBO(GL gl) {
		vertexBuffer.delete(gl);
		valueBuffer.delete(gl);
		indexBuffer.delete(gl);
	}

	/**
	 * @return the <code>DrawContext</code>s </code>com.jogamp.opengl.GLContext</code>.
	 */
	private GLContext getGLContext() {
		GLContext result = null;
		SceneController sceneController = Viewer3D.isUp() ? Viewer3D.getWwd().getSceneController() : null;
		if (sceneController != null) {
			DrawContext drawContext = sceneController.getDrawContext();
			if (drawContext != null) {
				GLContext glContext = drawContext.getGLContext();
				if (glContext != null && glContext.makeCurrent() != GLContext.CONTEXT_NOT_CURRENT) {
					result = glContext;
				}
			}
		}
		return result;

	}

	/** {@inheritDoc} */
	@Override
	public double getDistanceFromEye() {
		return 0; // Value never used by WW
	}

	/**
	 * @return the {@link #visible}
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the {@link #visible} to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the {@link #parameters}
	 */
	public PointRendererParameters getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the {@link #parameters} to set
	 */
	public void setParameters(PointRendererParameters parameters) {
		parameters.colorContrastModel().ifPresentOrElse(//
				m -> logger.info("Changing parameters with contrast = [{}, {}]", m.contrastMin(), m.contrastMax()),
				() -> logger.info("Changing parameters with color {}]", parameters.defaultColor()));
		this.parameters = parameters;
	}

	/**
	 * @param globalCursorSupplier the {@link #globalCursorSupplier} to set
	 */
	public void setGlobalCursorSupplier(IntFunction<GlobalCursor> globalCursorSupplier) {
		this.globalCursorSupplier = globalCursorSupplier;
	}

	/**
	 * @return the {@link #selected}
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the {@link #selected} to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
		geographicViewService.refresh();
	}

	/**
	 * @return the {@link #pickLayer}
	 */
	public Layer getPickLayer() {
		return pickLayer;
	}
}