#version 150 compatibility

in float value;
out float outValue;

in float index;
out float outIndex;

// Variable point size range
uniform float minSize;
uniform float maxSize;
// Range of values of the points
uniform float minValue;
uniform float maxValue;

void main()
{
	outValue = value;
	outIndex = index;
	vec4 point = gl_Vertex;
    gl_Position = gl_ModelViewProjectionMatrix * point;
    
    // Compute a weighted point size. Effective when GL_PROGRAM_POINT_SIZE is enabled
	gl_PointSize = minSize;
    if( maxSize > minSize) {
		float sizeFactor = (value - minValue) / (maxValue - minValue);
		sizeFactor = max(0.0, min(1.0, sizeFactor));
		gl_PointSize = minSize + sizeFactor * (maxSize - minSize);
   	} 
   	
} 