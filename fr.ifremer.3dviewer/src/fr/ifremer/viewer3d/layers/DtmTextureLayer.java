package fr.ifremer.viewer3d.layers;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.imageio.ImageIO;

import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.viewer3d.render.DtmTextureRender;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.SurfaceImageLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.SurfaceImage;
import gov.nasa.worldwind.util.ImageTiler;
import gov.nasa.worldwind.util.Logging;

/**
 * Layer to handle {@link IFileInfo} layers visualization (default visualization is bounding box).
 * <p>
 * If the provided IInfoStore has a not null GeoBox, shows an outline and also try load a texture image from its
 * associated file (for now 2012-04-25, the only implementation is for MNT files).
 * </p>
 * 
 * @see MNTTextureRender
 */
public class DtmTextureLayer extends SurfaceImageLayer
		implements ITarget, IDeletable, ILayerSelected, ILayerParameters {

	protected Logger logger = LoggerFactory.getLogger(DtmTextureLayer.class);

	VisibleLayerParameter layerparameters = new VisibleLayerParameter();

	/** The parent data store for this layer. */
	private IFileInfo store;

	protected ConcurrentHashMap<String, ArrayList<DtmTextureRender>> imageTable = new ConcurrentHashMap<String, ArrayList<DtmTextureRender>>();
	private boolean init = false;

	List<Polyline> lineList = new ArrayList<Polyline>();

	public DtmTextureLayer(IFileInfo store) {
		if (store == null) {
			throw new InvalidParameterException("IInfoStore cannot be null");
		}

		this.store = store;
		setName(store.getPath());
		this.setPickEnabled(false);

		// outline
		GeoBox box = store.getGeoBox();
		if (box != null) {
			initOutline(box);
		}

		this.setEnabled(false);
	}

	@Override
	public String getName() {
		if (store != null) {
			String filename = store.getPath();
			if (filename != null) {
				File f = new File(filename);
				return f.getName();
			}
		}
		return super.getName();
	}

	/** Creates outline from the 4 corners of provided geobox. */
	private void initOutline(GeoBox box) {
		List<Position> list = new LinkedList<Position>();
		list.add(Position.fromDegrees(box.getTop(), box.getLeft(), 0.0));// first
		// geo
		// point
		list.add(Position.fromDegrees(box.getBottom(), box.getLeft(), 0.0));// second
		// geo
		// point
		list.add(Position.fromDegrees(box.getBottom(), box.getRight(), 0.0));// third
		// geo
		// point
		list.add(Position.fromDegrees(box.getTop(), box.getRight(), 0.0));// fourth
		// geo
		// point
		list.add(Position.fromDegrees(box.getTop(), box.getLeft(), 0.0));// again
		// first
		// point

		Polyline polyline = new Polyline(list);
		polyline.setFollowTerrain(true);
		polyline.setColor(Color.WHITE);
		polyline.setLineWidth(3.0);
		polyline.setPathType(Polyline.RHUMB_LINE);
		addRenderable(polyline);
		lineList.add(polyline);
	}

	/**
	 * Remove an image to the collection, reprojecting it to geographic (latitude & longitude) coordinates if necessary.
	 * The image's location is determined from metadata files co-located with the image file.
	 * 
	 * @param metadata provides the path to the image file.
	 */
	@Override
	public void removeImage(String path) {
		try {
			ArrayList<DtmTextureRender> images = this.imageTable.get(path);
			if (images == null) {
				path = path.replace("\\", "/");
				images = this.imageTable.get(path);
				if (images == null) {
					return;
				}
			}

			this.imageTable.remove(path);

			for (SurfaceImage si : images) {
				if (si != null) {
					this.removeRenderable(si);
				}
			}
		} catch (Exception e) {
			Logging.logger().severe("Texture not removed");
		}
	}

	/**
	 * Overrode in order to implement an imageTiler using Sectors despite Lat/Lon corners.
	 */
	@Override
	public void addImage(String name, BufferedImage image, Sector sector) {
		if (name == null) {
			String message = Logging.getMessage("nullValue.NameIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (image == null) {
			String message = Logging.getMessage("nullValue.ImageSource");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (sector == null) {
			String message = Logging.getMessage("nullValue.SectorIsNull");
			Logging.logger().severe(message);
			throw new IllegalArgumentException(message);
		}

		if (this.imageTable.contains(name)) {
			this.removeImage(name);
		}

		final ArrayList<DtmTextureRender> surfaceImages = new ArrayList<DtmTextureRender>();
		this.imageTable.put(name, surfaceImages);
		this.imageTiler.tileImage(image, sector, new ImageTiler.ImageTilerListener() {
			@Override
			public void newTile(BufferedImage tileImage, Sector tileSector) {
				try {
					File tempFile = TemporaryCache.createTemporaryFile("wwj-", ".png");
					tempFile.deleteOnExit();
					ImageIO.write(tileImage, "png", tempFile);
					DtmTextureRender si = new DtmTextureRender(tempFile.getPath(), tileSector);
					surfaceImages.add(si);
					si.setOpacity(DtmTextureLayer.this.getOpacity());
					DtmTextureLayer.this.addRenderable(si);
				} catch (IOException e) {
					String message = Logging.getMessage("generic.ImageReadFailed");
					Logging.logger().severe(message);
				}
			}

			@Override
			public void newTile(BufferedImage tileImage, List<? extends LatLon> corners) {
			}
		});
	}

	@Override
	protected void doRender(DrawContext dc, Iterable<? extends Renderable> renderables) {
		if (!this.isInitialized()) {
			// this.initialize(dc.getGL());
		}
		for (Renderable renderable : renderables) {
			try {
				// If the caller has specified their own Iterable,
				// then we cannot make any guarantees about its contents.
				if (renderable != null) {
					renderable.render(dc);
				}
			} catch (Exception e) {
				String msg = Logging.getMessage("generic.ExceptionWhileRenderingRenderable");
				logger.error(msg, e);
				// continue to next renderable
			}
		}
	}

	public boolean isInitialized() {
		return this.init;
	}

	public IFileInfo getMetadata() {
		return store;
	}

	@Override
	public Sector getSector() {
		GeoBox gb = store.getGeoBox();
		if (gb != null) {
			Sector s = Sector.fromDegrees(gb.getBottom(), gb.getTop(), gb.getRight(), gb.getLeft());
			return s;
		}
		return null;
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	@Override
	public LayerParameters getLayerParameters() {
		return layerparameters;
	}

	@Override
	public void setLayerParameters(LayerParameters arg) {
		if (arg instanceof VisibleLayerParameter) {
			this.layerparameters.load((VisibleLayerParameter) arg);
		}
	}

	@Override
	public void setSelectedLayer(boolean selected) {
		Color color = selected ? ILayerSelected.selectedColor : Color.WHITE;

		for (Polyline l : lineList) {
			l.setColor(color);
		}
	}

}
