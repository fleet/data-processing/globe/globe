package fr.ifremer.viewer3d.layers;

import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

public interface IMovableLayer {

	/***
	 * 
	 * @return movement
	 */
	public ISectorMovementProcessing getMovement();

	/***
	 * set the movement defining a new position when calling move methods
	 * 
	 * @param movement
	 */
	public void setMovement(ISectorMovementProcessing movement);

	/***
	 * moves tile by the movement defined by ISectorMovementProcessing Useful
	 * for tectonic movement
	 * 
	 * @param Sector
	 * @return Sector
	 */
	public Sector move(Sector st);

	/***
	 * moves tile by the movement defined by ISectorMovementProcessing Useful
	 * for tectonic movement
	 */
	public LatLon move(LatLon latLon);

	/***
	 * moves tile by the inverse movement defined by ISectorMovementProcessing
	 * Useful for tectonic movement
	 * 
	 * @param Sector
	 * @return Sector
	 */
	public Sector moveInv(Sector st);

	/***
	 * moves tile by the inverse movement defined by ISectorMovementProcessing
	 * Useful for tectonic movement
	 */
	public LatLon moveInv(LatLon latLon);

	public String getName();

}
