package fr.ifremer.viewer3d.layers;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections4.ListUtils;

import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.viewer3d.data.DataType;
import fr.ifremer.viewer3d.data.reader.NetcdfReader;
import fr.ifremer.viewer3d.layers.MyAbstractLayerParameter.ContrastType;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Model for {@link ContrastShadeComposite}.
 *
 * @author apside
 */
public class ContrastShadeModel implements Comparable<ContrastShadeModel> {

	private PropertyChangeSupport pcs;

	private ShaderElevationLayer layer;

	private ContrastShadeModelMemento contrastShadeModelState;

	/**
	 * association between layers and their datatype, used to prevent to loose setting when changing datatype in
	 * ContrastShadeComposite
	 */
	private HashMap<String, List<Pair<Layer, ElevationModel>>> layers = new HashMap<>();

	private PropertyChangeListener layerListener;

	/** True to consider offset in AbsoluteValue2LayerValue computation */
	protected boolean layerValueWithOffset = true;

	protected IRasterService rasterService = IRasterService.grab();

	public ContrastShadeModel(ShaderElevationLayer layer) {
		pcs = new PropertyChangeSupport(this);
		setCurrentLayer(layer);
		layerListener = evt -> pcs.firePropertyChange(new PropertyChangeEvent(ContrastShadeModel.this,
				evt.getPropertyName(), evt.getOldValue(), evt.getNewValue()));
		this.layer.addPropertyChangeListener(layerListener);
	}

	public void dispose() {
		layer.removePropertyChangeListener(layerListener);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public ShaderElevationLayer getCurrentLayer() {
		return layer;
	}

	public List<Pair<Layer, ElevationModel>> getLayer(String dataType) {
		return layers.get(dataType);
	}

	public void setCurrentLayer(ShaderElevationLayer layer) {
		this.layer = layer;
	}

	public void retainLayers(List<Pair<Layer, ElevationModel>> layer, String type) {
		layers.put(type, layer);
	}

	public boolean isUseContrast() {
		return layer.isUseContrast();
	}

	public void setUseContrast(boolean useContrast) {
		layer.setUseContrast(useContrast);
	}

	public double getMinTransparency() {
		return layer.getMinTransparency();
	}

	public void setMinTransparency(double minTransparency) {
		layer.setMinTransparency(minTransparency);
	}

	public int[] getTransparencyRange() {
		int min = (int) layerValue2AbsoluteValue(layer.getValGlobaleMin());
		int max = (int) layerValue2AbsoluteValue(layer.getValGlobaleMax());
		return new int[] { min, max };
	}

	public double getMaxTransparency() {
		return layer.getMaxTransparency();
	}

	public void setMaxTransparency(double maxTransparency) {
		layer.setMaxTransparency(maxTransparency);
	}

	public ContrastType getContrastType() {
		MyAbstractLayerParameter parameters = (MyAbstractLayerParameter) layer.getLayerParameters();
		return parameters.getContrastType();
	}

	public void setContrastType(ContrastType type) {
		MyAbstractLayerParameter parameters = (MyAbstractLayerParameter) layer.getLayerParameters();
		parameters.setContrastType(type);
		layer.setContrastLevel(type.getLevel());
	}

	public int getContrastLevel() {
		return layer.getContrastLevel();
	}

	public boolean isUseColorMap() {
		return layer.isUseColorMap();
	}

	public void setUseColorMap(boolean useColorMap) {
		layer.setUseColorMap(useColorMap);
	}

	public int getColormap() {
		return (int) layer.getColorMap();
	}

	public void setColormap(int colormap) {
		layer.setColorMap(colormap);
	}

	public boolean isInverseColorMap() {
		return layer.isInverseColorMap();
	}

	public void setInverseColorMap(boolean inverseColorMap) {
		layer.setInverseColorMap(inverseColorMap);
	}

	public boolean isUseOmbrage() {
		return layer.isUseOmbrage();
	}

	public void setUseOmbrage(boolean useOmbrage) {
		layer.setUseOmbrage(useOmbrage);
	}

	public boolean isUseGradient() {
		return layer.isUseGradient();
	}

	public void setUseGradient(boolean useGradient) {
		layer.setUseGradient(useGradient);
	}

	public double getAzimuth() {
		return layer.getAzimuth();
	}

	public void setAzimuth(double azimuth) {
		layer.setAzimuth(azimuth);
	}

	public double getZenith() {
		return layer.getZenith();
	}

	public void setZenith(double zenith) {
		layer.setZenith(zenith);
	}

	public double getOmbrageExaggeration() {
		return layer.getOmbrageExaggeration();
	}

	public void setOmbrageExaggeration(double ombrageExaggeration) {
		layer.setOmbrageExaggeration(ombrageExaggeration);
	}

	public double getMinTextureValue() {
		return layer.getMinTextureValue();
	}

	public void setMinTextureValue(double minTextureValue) {
		layer.setMinTextureValue(minTextureValue);
	}

	public double getMaxTextureValue() {
		return layer.getMaxTextureValue();
	}

	public void setMaxTextureValue(double maxTextureValue) {
		layer.setMaxTextureValue(maxTextureValue);
	}

	public int[] getTextureValueRange() {
		int min = (int) layerValue2AbsoluteValue(layer.getValGlobaleMin());
		int max = (int) layerValue2AbsoluteValue(layer.getValGlobaleMax());
		return new int[] { min, max };
	}

	public double getMinContrast() {
		return layer.getMinContrast();
	}

	public void setMinContrast(double minContrast) {
		layer.setMinContrast(minContrast);
	}

	public double getMaxContrast() {
		return layer.getMaxContrast();
	}

	public void setMaxContrast(double maxContrast) {
		layer.setMaxContrast(maxContrast);
	}

	public boolean isUseOmbrageLogarithmique() {
		return layer.isUseOmbrageLogarithmique();
	}

	public void setUseOmbrageLogarithmique(boolean useOmbrageLogarithmique) {
		layer.setUseOmbrageLogarithmique(useOmbrageLogarithmique);
	}

	public double getScaleFactor() {
		return layer.getScaleFactor();
	}

	public void setScaleFactor(double scaleFactor) {
		layer.setScaleFactor(scaleFactor);
	}

	public void setAllContrastsSynchronized(boolean isAllLayersSynchronized) {
		if (isAllLayersSynchronized && contrastShadeModelState == null) {
			contrastShadeModelState = new ContrastShadeModelMemento(this);
		} else if (!isAllLayersSynchronized && contrastShadeModelState != null) {
			contrastShadeModelState.applyStateToModel(this, false);
			contrastShadeModelState = null;
		}
		MyAbstractLayerParameter parameters = (MyAbstractLayerParameter) layer.getLayerParameters();
		parameters.setAllContrastSynchronized(isAllLayersSynchronized);
	}

	public boolean isAllContrastsSynchronized() {
		MyAbstractLayerParameter parameters = (MyAbstractLayerParameter) layer.getLayerParameters();
		return parameters.isAllContrastSynchronized();
	}

	public void setAllThresholdSynchronized(boolean isAllThresholdSynchronized) {
		if (isAllThresholdSynchronized && contrastShadeModelState == null) {
			contrastShadeModelState = new ContrastShadeModelMemento(this);
		} else if (!isAllThresholdSynchronized && contrastShadeModelState != null) {
			contrastShadeModelState.applyStateToModel(this, true);
			contrastShadeModelState = null;
		}
		MyAbstractLayerParameter parameters = (MyAbstractLayerParameter) layer.getLayerParameters();
		parameters.setAllThresholdSynchronized(isAllThresholdSynchronized);
	}

	public boolean isAllThresholdSynchronized() {
		MyAbstractLayerParameter parameters = (MyAbstractLayerParameter) layer.getLayerParameters();
		return parameters.isAllThreasholdSynchronized();
	}

	/**
	 * Convert a layout value to its absolute value, taking into account offset and scale factor.
	 **/
	public double layerValue2AbsoluteValue(double value) {
		return value * layer.getScaleFactor() + (isLayerValueWithOffset() ? layer.getOffset() : 0);
	}

	/**
	 * Convert an absolute value to a layer value, taking into account offset and scale factor.
	 **/
	public double absoluteValue2LayerValue(double value) {
		return (value - (isLayerValueWithOffset() ? layer.getOffset() : 0)) / layer.getScaleFactor();
	}

	@Override
	public int hashCode() {
		return getCurrentLayer().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ContrastShadeModel) {
			return getCurrentLayer().equals(((ContrastShadeModel) obj).getCurrentLayer());
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getClass().getSimpleName());
		sb.append("[layer=").append(getCurrentLayer().getName()).append("]");
		return sb.toString();
	}

	@Deprecated // Better to use getRasterInfos
	public List<String> getData() {

		List<String> data = new ArrayList<>();
		String fileName = getCurrentLayer().getInputFile().getAbsolutePath();

		// if the data file is a netcdf or a BAG File
		List<String> list = null;
		if (NetcdfReader.isNetcdf(fileName)) {
			list = NetcdfReader.getAvailableData(fileName);
		}

		if (list != null) {
			data.addAll(list);
		} else {
			if ("DEPTH".equalsIgnoreCase(getCurrentLayer().getType())) {
				data.add(DataType.elevation.name());
			}
		}
		return data;
	}

	/**
	 * @return all RasterInfo deducted from the current file
	 */
	public List<RasterInfo> getRasterInfos() {
		String fileName = getCurrentLayer().getInputFile().getAbsolutePath();
		return rasterService.getRasterInfos(fileName);
	}

	public void setType(String type) {
		getCurrentLayer().setType(type);
	}

	public void setUnit(String unit) {
		getCurrentLayer().setUnit(unit);
	}

	public void setContrastLevel(int contrastLevel) {
		getCurrentLayer().setContrastLevel(contrastLevel);
	}

	public void setTextureValueRange(int[] textureValueRange) {
		getCurrentLayer().setValGlobaleMin(textureValueRange[0]);
		getCurrentLayer().setValGlobaleMax(textureValueRange[1]);
	}

	public void setTransparencyRange(int[] transparencyRange) {
		getCurrentLayer().setValGlobaleMin(transparencyRange[0]);
		getCurrentLayer().setValGlobaleMax(transparencyRange[1]);
	}

	@Override
	public int compareTo(ContrastShadeModel contrastShadeModel) {

		if (!ListUtils.isEqualList(contrastShadeModel.getData(), this.getData())) {
			return -1;
		} else if (contrastShadeModel.getCurrentLayer().getType().compareTo(this.getCurrentLayer().getType()) != 0) {
			return -1;
		} else if (contrastShadeModel.getCurrentLayer().getUnit().compareTo(this.getCurrentLayer().getUnit()) != 0) {
			return -1;
		} else if (contrastShadeModel.getAzimuth() != this.getAzimuth()) {
			return -1;
		} else if (contrastShadeModel.getColormap() != this.getColormap()) {
			return -1;
		} else if (contrastShadeModel.getContrastLevel() != this.getContrastLevel()) {
			return -1;
		} else if (contrastShadeModel.getContrastType() != this.getContrastType()) {
			return -1;
		} else if (contrastShadeModel.getMaxContrast() != this.getMaxContrast()) {
			return -1;
		} else if (contrastShadeModel.getMaxTextureValue() != this.getMaxTextureValue()) {
			return -1;
		} else if (contrastShadeModel.getMinContrast() != this.getMinContrast()) {
			return -1;
		} else if (contrastShadeModel.getMinTextureValue() != this.getMinTextureValue()) {
			return -1;
		} else if (contrastShadeModel.getMinTransparency() != this.getMinTransparency()) {
			return -1;
		} else if (contrastShadeModel.getMaxTransparency() != this.getMaxTransparency()) {
			return -1;
		} else if (contrastShadeModel.getOmbrageExaggeration() != this.getOmbrageExaggeration()) {
			return -1;
		} else if (contrastShadeModel.getScaleFactor() != this.getScaleFactor()) {
			return -1;
		} else if (!Arrays.equals(contrastShadeModel.getTextureValueRange(), this.getTextureValueRange())) {
			return -1;
		} else if (!Arrays.equals(contrastShadeModel.getTransparencyRange(), this.getTransparencyRange())) {
			return -1;
		} else if (contrastShadeModel.getZenith() != this.getZenith()) {
			return -1;
		} else if (contrastShadeModel.isInverseColorMap() != contrastShadeModel.isInverseColorMap()) {
			return -1;
		} else if (contrastShadeModel.isUseColorMap() != contrastShadeModel.isUseColorMap()) {
			return -1;
		} else if (this.isUseContrast() != contrastShadeModel.isUseContrast()) {
			return -1;
		} else if (this.isUseGradient() != contrastShadeModel.isUseGradient()) {
			return -1;
		} else if (this.isUseOmbrage() != contrastShadeModel.isUseOmbrage()) {
			return -1;
		} else if (this.isUseOmbrageLogarithmique() != contrastShadeModel.isUseOmbrageLogarithmique()) {
			return -1;
		}
		return 0;
	}

	/**
	 * @return the contrastShadeModelState
	 */
	public ContrastShadeModelMemento getContrastShadeModelState() {
		return contrastShadeModelState;
	}

	/**
	 * @param contrastShadeModelState the contrastShadeModelState to set
	 */
	public void setContrastShadeModelState(ContrastShadeModelMemento contrastShadeModelState) {
		this.contrastShadeModelState = contrastShadeModelState;
	}

	/**
	 * Getter of layerValueWithOffset
	 */
	public boolean isLayerValueWithOffset() {
		return layerValueWithOffset;
	}

	/**
	 * Setter of layerValueWithOffset
	 */
	public void setLayerValueWithOffset(boolean layerValueWithOffset) {
		this.layerValueWithOffset = layerValueWithOffset;
	}
}
