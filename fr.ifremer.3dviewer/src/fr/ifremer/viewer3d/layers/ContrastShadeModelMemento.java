package fr.ifremer.viewer3d.layers;
import fr.ifremer.viewer3d.layers.MyAbstractLayerParameter.ContrastType;

public class ContrastShadeModelMemento {

	private String type;
	private String unit;
	private double azimuth;
	private int colormap;
	private int contrastLevel;
	private ContrastType contrastType;
	private double maxContrast;
	private double maxTextureValue;
	private double minContrast;
	private double minTextureValue;
	private double minTransparency;
	private double maxTransparency;
	private double ombrageExaggeration;
	private double scaleFactor;
	private int[] textureValueRange;
	private int[] transparencyRange;
	private double zenith;
	private boolean inverseColorMap;
	private boolean useColorMap;
	private boolean useContrast;
	private boolean useGradient;
	private boolean useOmbrage;
	private boolean useOmbrageLogarithmique;

	public ContrastShadeModelMemento(ContrastShadeModel contrastShadeModel) {

		this.type = contrastShadeModel.getCurrentLayer().getType();
		this.unit = contrastShadeModel.getCurrentLayer().getUnit();
		this.azimuth = contrastShadeModel.getAzimuth();
		this.colormap = contrastShadeModel.getColormap();
		this.contrastLevel = contrastShadeModel.getContrastLevel();
		this.contrastType = contrastShadeModel.getContrastType();
		this.maxContrast = contrastShadeModel.getMaxContrast();
		this.maxTextureValue = contrastShadeModel.getMaxTextureValue();
		this.minContrast = contrastShadeModel.getMinContrast();
		this.minTextureValue = contrastShadeModel.getMinTextureValue();
		this.minTransparency = contrastShadeModel.getMinTransparency();
		this.maxTransparency = contrastShadeModel.getMaxTransparency();
		this.ombrageExaggeration = contrastShadeModel.getOmbrageExaggeration();
		this.scaleFactor = contrastShadeModel.getScaleFactor();
		this.textureValueRange = contrastShadeModel.getTextureValueRange();
		this.transparencyRange = contrastShadeModel.getTransparencyRange();
		this.zenith = contrastShadeModel.getZenith();
		this.inverseColorMap = contrastShadeModel.isInverseColorMap();
		this.useColorMap = contrastShadeModel.isUseColorMap();
		this.useContrast = contrastShadeModel.isUseContrast();
		this.useGradient = contrastShadeModel.isUseGradient();
		this.useOmbrage = contrastShadeModel.isUseOmbrage();
		this.useOmbrageLogarithmique = contrastShadeModel.isUseOmbrageLogarithmique();
	}

	/**
	 * Apply this saved state to a {@link ContrastShadeModel}.
	 * @param contrastShadeModel contrast model to restore at a saved state
	 * @param contrastOnly true if only the contrast need to be restore
	 */
	public void applyStateToModel(ContrastShadeModel contrastShadeModel, boolean contrastOnly) {

		contrastShadeModel.setMinTransparency(this.minTransparency);
		contrastShadeModel.setMaxTransparency(this.maxTransparency);

		if(!contrastOnly) {
			contrastShadeModel.setMinContrast(this.minTextureValue);
			contrastShadeModel.setMaxContrast(this.maxTextureValue);
			contrastShadeModel.getCurrentLayer().setType(this.type);
			contrastShadeModel.getCurrentLayer().setUnit(this.unit);
			contrastShadeModel.setAzimuth(this.azimuth);
			contrastShadeModel.setColormap(this.colormap);
			contrastShadeModel.setContrastLevel(this.contrastLevel);
			contrastShadeModel.setContrastType(this.contrastType);
			contrastShadeModel.setMinTextureValue(this.minContrast);
			contrastShadeModel.setMaxTextureValue(this.maxContrast);
			if(!contrastShadeModel.isLayerValueWithOffset()) {
				contrastShadeModel.setMinContrast(this.minContrast);
				contrastShadeModel.setMaxContrast(this.maxContrast);
			}
			contrastShadeModel.setOmbrageExaggeration(this.ombrageExaggeration);
			contrastShadeModel.setScaleFactor(this.scaleFactor);
			contrastShadeModel.setZenith(this.zenith);
			contrastShadeModel.setInverseColorMap(this.inverseColorMap);
			contrastShadeModel.setUseColorMap(this.useColorMap);
			contrastShadeModel.setUseGradient(this.useGradient);
			contrastShadeModel.setUseOmbrage(this.useOmbrage);
			contrastShadeModel.setUseOmbrageLogarithmique(this.useOmbrageLogarithmique);		
		}
	}

	public String getType() {
		return type;
	}

	public String getUnit() {
		return unit;
	}

	public double getAzimuth() {
		return azimuth;
	}

	public int getColormap() {
		return colormap;
	}

	public int getContrastLevel() {
		return contrastLevel;
	}

	public ContrastType getContrastType() {
		return contrastType;
	}



	public double getMaxContrast() {
		return maxContrast;
	}

	public double getMaxTextureValue() {
		return maxTextureValue;
	}

	public double getMinContrast() {
		return minContrast;
	}

	public double getMinTextureValue() {
		return minTextureValue;
	}

	public double getMinTransparency() {
		return minTransparency;
	}

	public double getMaxTransparency() {
		return maxTransparency;
	}

	public double getOmbrageExaggeration() {
		return ombrageExaggeration;
	}

	public double getScaleFactor() {
		return scaleFactor;
	}



	public int[] getTextureValueRange() {
		return textureValueRange;
	}

	public int[] getTransparencyRange() {
		return transparencyRange;
	}

	public double getZenith() {
		return zenith;
	}


	public boolean isInverseColorMap() {
		return inverseColorMap;
	}

	public boolean isUseColorMap() {
		return useColorMap;
	}

	public boolean isUseContrast() {
		return useContrast;
	}

	public boolean isUseGradient() {
		return useGradient;
	}

	public boolean isUseOmbrage() {
		return useOmbrage;
	}

	public boolean isUseOmbrageLogarithmique() {
		return useOmbrageLogarithmique;
	}
}