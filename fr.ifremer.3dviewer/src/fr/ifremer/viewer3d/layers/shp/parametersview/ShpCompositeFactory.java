/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.shp.parametersview;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.layers.shp.ShpLayer;

/**
 * Provide the parameter Composite of a ShpLayer
 */
@Component(name = "globe_viewer3d_parameters_shp_composite_factory", service = IParametersViewCompositeFactory.class)
public class ShpCompositeFactory implements IParametersViewCompositeFactory {

	/** Constructor */
	public ShpCompositeFactory() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		ShpParameterComposite result = null;
		if (selection instanceof ShpLayer) {
			result = new ShpParameterComposite((ShpLayer) selection, parent, SWT.NONE);
			result.bindColorMap();
		}
		return Optional.ofNullable(result);
	}

}
