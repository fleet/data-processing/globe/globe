/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.shp.parametersview;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColorCellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.ui.widget.color.ColorComposite;
import fr.ifremer.viewer3d.layers.shp.ShpLayer;
import fr.ifremer.viewer3d.layers.shp.ShpLayerParameters;
import fr.ifremer.viewer3d.layers.shp.ShpLayerParameters.ShpColoredValue;

public class ShpParameterComposite extends Composite {

	/** Layer parameters */
	protected ShpLayerParameters shpLayerParameters;

	/** Widgets */
	protected ColorComposite colorComposite;
	protected ComboViewer allAttributesNamesComboViewer;

	/** Bindings between model and widgets */
	protected DataBindingContext dataBindingContext;
	protected Combo combo;
	protected Table colorTable;
	protected TableViewer colorTableViewer;

	/**
	 * Create the composite.
	 */
	public ShpParameterComposite(ShpLayer shpLayer, Composite parent, int style) {
		super(parent, style);
		shpLayerParameters = shpLayer.getDisplayParameters();
		initializeComposite();

		// Bindings
		bindAllAttributeNamesCombo();
		bindColorMap();
		dataBindingContext.updateTargets();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	protected void initializeComposite() {

		setLayout(new GridLayout(2, false));

		Label lblAttribute = new Label(this, SWT.NONE);
		lblAttribute.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAttribute.setText("Attribute");

		allAttributesNamesComboViewer = new ComboViewer(this, SWT.READ_ONLY);
		combo = allAttributesNamesComboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		colorComposite = new ColorComposite(this, SWT.NONE, true, shpLayerParameters.getColorMapIndex().get(), false);
		colorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		colorComposite.setColormapSelection(shpLayerParameters.getColorMapIndex().get());
		colorComposite.setInvertSelection(shpLayerParameters.getColorMapInvert().isTrue());

		Composite tableComposite = new Composite(this, SWT.BORDER);
		tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		colorTableViewer = new TableViewer(tableComposite, SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
		colorTable = colorTableViewer.getTable();
		colorTable.setHeaderVisible(true);
		colorTable.setLinesVisible(true);
		// Disable selection !
		colorTable.addListener(SWT.EraseItem, event -> {
			// Highlight the row with the same color than the current ShpColoredValue
			ShpColoredValue coloredValue = (ShpColoredValue) event.item.getData();
			event.gc.setBackground(ColorUtils.convertAWTToSWT(coloredValue.getColor()));
			event.gc.fillRectangle(colorTable.getColumn(0).getWidth(), event.y, colorTable.getColumn(1).getWidth() - 2,
					event.height);
			event.detail &= ~SWT.HOT;
			event.detail &= ~SWT.SELECTED;
		});

		// Define columns
		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		tableComposite.setLayout(tableColumnLayout);

		defineColorTableViewerColumns(tableColumnLayout);

		dataBindingContext = initDataBindings();
		allAttributesNamesComboViewer.addSelectionChangedListener(evt -> IParametersViewService.grab().resize());
	}

	/** Defines the columns of the TableViewer */
	protected void defineColorTableViewerColumns(TableColumnLayout tableColumnLayout) {
		defineValueColumn(tableColumnLayout);
		defineColorColumn(tableColumnLayout);
	}

	/** Defines the Value column of the TableViewer */
	protected void defineValueColumn(TableColumnLayout tableColumnLayout) {
		TableViewerColumn valueTableViewerColumn = new TableViewerColumn(colorTableViewer, SWT.NONE);
		valueTableViewerColumn.getColumn().setText("Value");
		tableColumnLayout.setColumnData(valueTableViewerColumn.getColumn(), new ColumnWeightData(70, 50, true));
		valueTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				String result = ((ShpColoredValue) element).getValue();
				return result != null ? result : "<Empty>";
			}
		});
	}

	/** Defines the Color column of the TableViewer */
	protected void defineColorColumn(TableColumnLayout tableColumnLayout) {
		TableViewerColumn colorTableViewerColumn = new TableViewerColumn(colorTableViewer, SWT.NONE);
		colorTableViewerColumn.getColumn().setText("Color");
		tableColumnLayout.setColumnData(colorTableViewerColumn.getColumn(), new ColumnWeightData(30, 20, true));
		colorTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return "";
			}

			@Override
			public Color getBackground(Object element) {
				return ColorUtils.convertAWTToSWT(((ShpColoredValue) element).getColor());
			}

			/** {@inheritDoc} */
			@Override
			public Color getForeground(Object element) {
				return getBackground(element);
			}
		});

		colorTableViewerColumn.setEditingSupport(new EditingSupport(colorTableViewer) {
			@Override
			protected void setValue(Object element, Object value) {
				shpLayerParameters.changeColor((ShpColoredValue) element, ColorUtils.convertRGBToAWT((RGB) value));
			}

			@Override
			protected Object getValue(Object element) {
				return ColorUtils.convertAWTToRGB(((ShpColoredValue) element).getColor());
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return new ColorCellEditor(colorTable);
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});
	}

	/** Initialize a specific binding on ColorComposite (not managed by Windows Builder) */
	public void bindColorMap() {
		colorComposite.addPropertyChangeListener(evt -> {
			if (evt.getNewValue() instanceof Integer) {
				shpLayerParameters.getColorMapIndex().set((Integer) evt.getNewValue());
			} else if (evt.getNewValue() instanceof Boolean) {
				shpLayerParameters.getColorMapInvert().set((Boolean) evt.getNewValue());
			}
		});

		IChangeListener changeListener = evt -> colorComposite
				.setColormapSelection(shpLayerParameters.getColorMapIndex().get());
		shpLayerParameters.getColorMapIndex().addChangeListener(changeListener);
		colorComposite
				.addDisposeListener(e -> shpLayerParameters.getColorMapIndex().removeChangeListener(changeListener));
	}

	/** Initialize a specific binding on ComboViewer (not managed by Windows Builder) */
	protected void bindAllAttributeNamesCombo() {
		allAttributesNamesComboViewer.setLabelProvider(new LabelProvider());
		allAttributesNamesComboViewer.setContentProvider(new ObservableListContentProvider());
		allAttributesNamesComboViewer.setInput(shpLayerParameters.getAllAttributeNames());

		colorTableViewer.setContentProvider(new ObservableListContentProvider());
		colorTableViewer.setInput(shpLayerParameters.getPossibleValues());
	}

	/** {@inheritDoc} */
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue<?> observeSingleSelectionAllAttributesNamesComboViewer = ViewerProperties.singleSelection()
				.observe(allAttributesNamesComboViewer);
		bindingContext.bindValue(observeSingleSelectionAllAttributesNamesComboViewer,
				shpLayerParameters.getAttributeName(), null, null);
		//
		return bindingContext;
	}
}
