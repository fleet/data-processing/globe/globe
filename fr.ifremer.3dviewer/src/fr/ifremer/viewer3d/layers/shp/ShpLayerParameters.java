/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.shp;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.layers.VisibleLayerParameter;

/**
 * Model of Shp presentation
 */
public class ShpLayerParameters extends VisibleLayerParameter {

	private final static String ATTRIBUT_COLOR = "color";

	/** {@link ShpLayer} **/
	private final ShpLayer layer;

	/** Fields contained in SHP file */
	protected Map<String, Set<String>> fields;

	/** Index of the ColorMap */
	protected WritableInt colorMapIndex;

	/** Flag true when ColorMap is reversed */
	protected WritableBoolean colorMapInvert;

	/** Current selected attribute name used to color shapes */
	protected WritableString attributeName;

	/** All attribute name */
	protected WritableObjectList<String> allAttributeNames;

	/** All possible values for the current attribute name */
	protected WritableObjectList<ShpColoredValue> possibleValues;

	/** fill polygon option **/
	private WritableBoolean fillPolygonFlag = new WritableBoolean(
			Activator.getPluginParameters().getIsFillPolygonAttribute().getValue());

	/**
	 * Constructor
	 */
	public ShpLayerParameters(ShpLayer layer, Map<String, Set<String>> fields) {
		this.layer = layer;
		this.fields = fields;
		Display.getDefault().syncExec(() -> {
			colorMapIndex = new WritableInt(ColorMaps.GMT_JET.getIndex());
			colorMapInvert = new WritableBoolean(false);
			allAttributeNames = new WritableObjectList<>();
			possibleValues = new WritableObjectList<>();

			allAttributeNames.addAll(fields.keySet());
			attributeName = new WritableString(allAttributeNames.isEmpty() ? ""
					: allAttributeNames.contains(ATTRIBUT_COLOR) ? ATTRIBUT_COLOR : allAttributeNames.get(0));

			colorMapIndex.addChangeListener(evt -> computeColors());
			colorMapInvert.addChangeListener(evt -> computeColors());
			attributeName.addChangeListener(evt -> computeColors());
			computeColors();
		});

	}

	/** Free memory and listeners */
	public void dispose() {
		fields.clear();
		colorMapIndex.dispose();
		colorMapInvert.dispose();
		attributeName.dispose();
		allAttributeNames.dispose();
		possibleValues.dispose();
	}

	/**
	 * When AttributeName changed, the list of possible values are updated
	 */
	protected void computeColors() {
		possibleValues.clear();
		Set<String> values = fields.get(attributeName.get());
		List<Color> shapeColors = ColorMap.getColormapAwtValues(colorMapIndex.get());
		List<ShpColoredValue> newShpColoredValues = new ArrayList<>();

		// First color for null
		newShpColoredValues
				.add(new ShpColoredValue(null, shapeColors.get(colorMapInvert.isFalse() ? 0 : shapeColors.size() - 1)));

		if (values != null) {
			// if attribute is color : get directly values to define colors
			if (attributeName.get().equals(ATTRIBUT_COLOR)) {
				for (String value : values)
					newShpColoredValues
							.add(new ShpColoredValue(value, ColorUtils.convertGColorToAWT(new GColor(value))));
			} else {
				double delta = ((double) shapeColors.size()) / values.size();
				double valueIndex = 0;
				for (String value : values) {
					int intValueIndex = (int) (colorMapInvert.isFalse() ? valueIndex + delta
							: shapeColors.size() - valueIndex - delta);
					intValueIndex = Math.max(intValueIndex, 0);
					intValueIndex = Math.min(intValueIndex, shapeColors.size() - 1);
					newShpColoredValues.add(new ShpColoredValue(value, shapeColors.get(intValueIndex)));
					valueIndex += delta;
				}
			}
		}
		possibleValues.addAll(newShpColoredValues);
	}

	/** Update one color and notify everybody */
	public void changeColor(ShpColoredValue coloredValue, Color color) {
		coloredValue.setColor(color);
		possibleValues.set(possibleValues.indexOf(coloredValue), coloredValue);
	}

	/** Getter of {@link #colorMapIndex} */
	public WritableInt getColorMapIndex() {
		return colorMapIndex;
	}

	/** Getter of {@link #attributeName} */
	public WritableString getAttributeName() {
		return attributeName;
	}

	/** Getter of {@link #allAttributeNames} */
	public WritableObjectList<String> getAllAttributeNames() {
		return allAttributeNames;
	}

	/** Getter of {@link #possibleValues} */
	public WritableObjectList<ShpColoredValue> getPossibleValues() {
		return possibleValues;
	}

	/** Getter of {@link #colorMapInvert} */
	public WritableBoolean getColorMapInvert() {
		return colorMapInvert;
	}

	public double getOpacity() {
		return layer.getOpacity();
	}

	public WritableBoolean getFillPolygonFlag() {
		return fillPolygonFlag;
	}

	public boolean isFillEnabled() {
		return fillPolygonFlag.doGetValue();
	}

	/** Association between a value and a color */
	public static class ShpColoredValue {

		/** Constructor */
		private ShpColoredValue(String value, Color color) {
			this.value = value;
			this.color = color;
		}

		/** One possible value of the selected attribute */
		protected String value;
		/** The color */
		protected Color color;

		/** Getter of {@link #value} */
		public String getValue() {
			return value;
		}

		/** Getter of {@link #color} */
		public Color getColor() {
			return color;
		}

		/** Setter of {@link #color} */
		void setColor(Color color) {
			this.color = color;
		}
	}

}
