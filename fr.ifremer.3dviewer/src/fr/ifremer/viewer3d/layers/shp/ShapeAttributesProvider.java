/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.shp;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.viewer3d.layers.shp.ShpLayerFactory.ShpPointPlacemark;
import fr.ifremer.viewer3d.layers.shp.ShpLayerParameters.ShpColoredValue;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.formats.shapefile.ShapefileRecord;
import gov.nasa.worldwind.formats.shapefile.ShapefileRecordPoint;
import gov.nasa.worldwind.formats.shapefile.ShapefileRenderable;
import gov.nasa.worldwind.formats.shapefile.ShapefileRenderable.Record;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.util.WWUtil;

/**
 * Provider of ShapeAttributes
 */
public class ShapeAttributesProvider implements ShapefileRenderable.AttributeDelegate, IShpLayerRenderableVisitor {

	private static final double DEFAULT_SCALE = 15d;

	/** Parameters of ShpLayer */
	private final ShpLayerParameters shpDisplayParameters;

	/** Mapping between an file record values and a ShapeAttributes */
	private final Map<String, ShapeAttributes> shapeAttrs = new HashMap<>();

	/**
	 * Constructor
	 */
	public ShapeAttributesProvider(ShpLayerParameters shpModel) {
		this.shpDisplayParameters = shpModel;
	}

	/** {@inheritDoc} */
	@Override
	public void assignAttributes(ShapefileRecord shapefileRecord, Record renderableRecord) {
		// Save all attributes from Shapefile to the Record.
		Display.getDefault().syncExec(() -> {
			for (String attributeName : shpDisplayParameters.getAllAttributeNames())
				renderableRecord.setValue(attributeName, shapefileRecord.getAttributes().getStringValue(attributeName));
		});
		assignAttributes(renderableRecord);
	}

	@Override
	public void visit(ShapefileRenderable shapefileRenderable) {
		for (int i = 0; i < shapefileRenderable.getRecordCount(); i++)
			assignAttributes(shapefileRenderable.getRecord(i));
	}

	/** Change the ShapeAttributes of the Record */
	public void assignAttributes(Record renderableRecord) {
		if (shapeAttrs.isEmpty()) {
			Display.getDefault().syncExec(() -> {
				for (ShpColoredValue coloredValue : shpDisplayParameters.getPossibleValues()) {
					ShapeAttributes attrs = new BasicShapeAttributes();
					attrs.setInteriorMaterial(new Material(coloredValue.getColor()));
					attrs.setDrawInterior(shpDisplayParameters.isFillEnabled());
					if (shpDisplayParameters.isFillEnabled())
						attrs.setOutlineMaterial(new Material(WWUtil.makeColorBrighter(coloredValue.getColor())));
					else
						attrs.setOutlineMaterial(new Material(coloredValue.getColor()));
					attrs.setInteriorOpacity(shpDisplayParameters.getOpacity());
					attrs.setOutlineOpacity(shpDisplayParameters.getOpacity());
					attrs.setOutlineWidth(2);

					shapeAttrs.put(coloredValue.getValue(), attrs);
				}
			});
		}

		if (!shapeAttrs.isEmpty()) {
			// Set the ShapeAttributes according the value of the selected attribute
			var attributeValue = renderableRecord.getStringValue(shpDisplayParameters.getAttributeName().get());
			var shapeAttributes = shapeAttrs.getOrDefault(attributeValue, shapeAttrs.values().iterator().next());
			renderableRecord.setAttributes(shapeAttributes);
			renderableRecord.setHighlightAttributes(shapeAttributes);
		}
	}

	@Override
	public void visit(ShpPointPlacemark pointPlacemark) {
		Display.getDefault().syncExec(() -> {
			ShapefileRecordPoint recordPoints = (ShapefileRecordPoint) pointPlacemark
					.getValue(ShpLayer.RECORD_POINT_KEY);
			var pointPlacemarkAttributes = new PointPlacemarkAttributes();
			pointPlacemarkAttributes.setUsePointAsDefaultImage(true);

			// set size & tooltip
			var tooltip = new StringBuilder();
			pointPlacemarkAttributes.setScale(DEFAULT_SCALE);
			recordPoints.getAttributes().getEntries().forEach(entry -> {
				switch (entry.getKey()) {
				case "size":
					pointPlacemarkAttributes.setScale(((double) entry.getValue()) / 3d);
					break;
				default:
					tooltip.append(entry.getKey() + " : " + entry.getValue() + "\n");
				}
			});
			pointPlacemark.setValue(AVKey.ROLLOVER_TEXT, tooltip.toString());

			// set color & opacity
			var attriName = shpDisplayParameters.getAttributeName().get();
			var pointValue = Optional.ofNullable(recordPoints.getAttributes().getStringValue(attriName)).orElse("");
			for (ShpColoredValue coloredValue : shpDisplayParameters.getPossibleValues()) {
				if (coloredValue.getValue() != null && compareValues(coloredValue.getValue(), pointValue)) {
					var color = ColorUtils.convertAWTtoGColor(coloredValue.getColor());
					var opacity = (int) (shpDisplayParameters.getOpacity() * 250);
					color = new GColor(color.getRed(), color.getGreen(), color.getBlue(), opacity);
					pointPlacemarkAttributes.setLineColor(color.toAABBGGRR());
				}
			}

			pointPlacemark.setAttributes(pointPlacemarkAttributes);
		});

	}

	private boolean compareValues(String recordValue, String colorValue) {
		return colorValue.equals(recordValue)
				|| (NumberUtils.isParsable(recordValue) && NumberUtils.isParsable(colorValue)
						&& (Math.abs(NumberUtils.toDouble(recordValue) - NumberUtils.toDouble(colorValue)) < 0.01));
	}

}
