package fr.ifremer.viewer3d.layers.shp;

import fr.ifremer.viewer3d.layers.shp.ShpLayerFactory.ShpExtrudedPolygons;
import fr.ifremer.viewer3d.layers.shp.ShpLayerFactory.ShpPointPlacemark;
import fr.ifremer.viewer3d.layers.shp.ShpLayerFactory.ShpPolygons;
import gov.nasa.worldwind.formats.shapefile.ShapefileRenderable;

public interface IShpLayerRenderableVisitor {

	void visit(ShapefileRenderable renderable);

	default void visit(ShpPolygons polygon) {
		visit((ShapefileRenderable) polygon);
	}

	default void visit(ShpExtrudedPolygons extrudedPolygons) {
		visit((ShapefileRenderable) extrudedPolygons);
	}

	void visit(ShpPointPlacemark pointPlacemark);

}
