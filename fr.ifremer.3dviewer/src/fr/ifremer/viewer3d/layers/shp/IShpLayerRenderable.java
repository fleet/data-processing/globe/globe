package fr.ifremer.viewer3d.layers.shp;

public interface IShpLayerRenderable {
	
	void accept(IShpLayerRenderableVisitor visitor);

}
