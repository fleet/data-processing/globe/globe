/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.shp;

import java.util.List;

import fr.ifremer.viewer3d.layers.VisibleLayerParameter;
import fr.ifremer.viewer3d.layers.shp.ShpLayerParameters.ShpColoredValue;

/**
 * Pojo to save model as LayerParameters in session.
 */
public class ShpSessionParameters extends VisibleLayerParameter {

	/** Index of the ColorMap */
	protected int colorMapIndex;

	/** Flag true when ColorMap is reversed */
	protected boolean colorMapInvert;

	/** Current selected attribute name used to color shapes */
	protected String attributeName;

	/** All possible values for the current attribute name */
	protected List<ShpColoredValue> possibleValues;

	/** Parameters of ShpLayer */
	protected ShpLayerParameters shpModel;

	/** Constructor */
	public ShpSessionParameters() {
		super();
	}

	/** Link this ShpLayerParameters and the ShpModel */
	public void hook(ShpLayerParameters shpModel) {
		shpModel.getAttributeName().addChangeListener(evt -> attributeName = shpModel.getAttributeName().get());
		shpModel.getColorMapIndex().addChangeListener(evt -> colorMapIndex = shpModel.getColorMapIndex().get());
		shpModel.getColorMapInvert().addChangeListener(evt -> colorMapInvert = shpModel.getColorMapInvert().get());
		shpModel.getPossibleValues().addChangeListener(evt -> possibleValues = shpModel.getPossibleValues().asList());
	}

	/** Getter of {@link #colorMapIndex} */
	public int getColorMapIndex() {
		return colorMapIndex;
	}

	/** Setter of {@link #colorMapIndex} */
	public void setColorMapIndex(int colorMapIndex) {
		pcs.firePropertyChange("colorMapIndex", this.colorMapIndex, this.colorMapIndex = colorMapIndex);
	}

	/** Getter of {@link #colorMapInvert} */
	public boolean isColorMapInvert() {
		return colorMapInvert;
	}

	/** Setter of {@link #colorMapInvert} */
	public void setColorMapInvert(boolean colorMapInvert) {
		pcs.firePropertyChange("colorMapInvert", this.colorMapInvert, this.colorMapInvert = colorMapInvert);
	}

	/** Getter of {@link #attributeName} */
	public String getAttributeName() {
		return attributeName;
	}

	/** Setter of {@link #attributeName} */
	public void setAttributeName(String attributeName) {
		pcs.firePropertyChange("attributeName", this.attributeName, this.attributeName = attributeName);
	}

	/** Getter of {@link #possibleValues} */
	public List<ShpColoredValue> getPossibleValues() {
		return possibleValues;
	}

	/** Setter of {@link #possibleValues} */
	public void setPossibleValues(List<ShpColoredValue> possibleValues) {
		this.possibleValues = possibleValues;
	}

}
