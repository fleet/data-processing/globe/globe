/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.shp;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWShapefileLayer;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.formats.shapefile.Shapefile;
import gov.nasa.worldwind.formats.shapefile.ShapefileExtrudedPolygons;
import gov.nasa.worldwind.formats.shapefile.ShapefileLayerFactory;
import gov.nasa.worldwind.formats.shapefile.ShapefilePolygons;
import gov.nasa.worldwind.formats.shapefile.ShapefileRecordPoint;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.ShapeAttributes;

/**
 * A factory that creates {@link gov.nasa.worldwind.layers.Layer} instances from ShpInfo.
 */
public class ShpLayerFactory extends ShapefileLayerFactory {

	private IWWShapefileLayer.Data shapefileData;

	/**
	 * Create a Layer to represent the Shapefile in the Viewer3D
	 *
	 * @param shapefile file to parse
	 * @return the resulting layer. May be null
	 */
	public IWWShapefileLayer createFromShapefile(IWWShapefileLayer.Data shapefileData) {
		ShpLayer result = null;
		this.shapefileData = shapefileData;

		try (Shapefile shp = new Shapefile(shapefileData.getShapeFile())) {
			AtomicBoolean done = new AtomicBoolean(false);
			CompletionCallback callback = new CompletionCallback() {
				@Override
				public void completion(Object layerAsObject) {
					done.set(true);
				}

				@Override
				public void exception(Exception e) {
					done.set(true);
				}
			};

			result = (ShpLayer) createFromShapefileSource(shp, callback);

			// Waiting for the result
			while (!done.get()) {
				try {
					Thread.sleep(100l);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}

		return result;
	}

	/** Overloaded to instanciade a ShpLayer instead of RenderableLayer */
	@Override
	protected ShpLayer doCreateFromShapefile(Object shapefileSource, CompletionCallback callback) throws Exception {
		var layer = new ShpLayer(shapefileData);

		// mandatory to save all attributes from Shapefile to the Record
		// see ShapeAttributesProvider.assignAttributes(...)
		setAttributeDelegate(new ShapeAttributesProvider(layer.getDisplayParameters()));

		createShapefileLayer(shapefileSource, layer, callback);
		return layer;
	}

	@Override
	protected void addRenderablesForPoints(Shapefile shp, RenderableLayer layer) {
		while (shp.hasNext()) {
			var record = shp.nextRecord();
			if (Shapefile.isPointType(record.getShapeType())
					&& record instanceof ShapefileRecordPoint shapefileRecordPoint)
				layer.addRenderable(new ShpPointPlacemark(shapefileRecordPoint));
		}
	}

	/** {@inheritDoc} */
	@Override
	protected void addRenderablesForSurfacePolygons(Shapefile shp, RenderableLayer layer) {
		layer.addRenderable(new ShpPolygons(shp, this.normalShapeAttributes, this.highlightShapeAttributes, this));
	}

	/** {@inheritDoc} */
	@Override
	protected void addRenderablesForExtrudedPolygons(Shapefile shp, RenderableLayer layer) {
		layer.addRenderable(
				new ShpExtrudedPolygons(shp, this.normalShapeAttributes, this.highlightShapeAttributes, this));
	}

	/**
	 * {@link Renderable} for {@link ShapefileRecordPoint} with elevation.
	 */
	static class ShpPointPlacemark extends PointPlacemark implements Disposable, IShpLayerRenderable {

		/**
		 * Constructor
		 */
		public ShpPointPlacemark(ShapefileRecordPoint shapefileRecordPoint) {
			super(Position.fromDegrees(shapefileRecordPoint.getPoint()[1], shapefileRecordPoint.getPoint()[0],
					Optional.ofNullable(shapefileRecordPoint.getZ()).orElse(0d)));
			setAltitudeMode(shapefileRecordPoint.getZ() != null ? WorldWind.ABSOLUTE : WorldWind.CLAMP_TO_GROUND);
			setValue(ShpLayer.RECORD_POINT_KEY, shapefileRecordPoint);
		}

		/** {@inheritDoc} */
		@Override
		public void dispose() {
			clearList();
		}

		@Override
		public void accept(IShpLayerRenderableVisitor visitor) {
			visitor.visit(this);
		}
	}

	/** Redefine ShapefilePolygons to free memory */
	static class ShpPolygons extends ShapefilePolygons implements Disposable, IShpLayerRenderable {

		/**
		 * Constructor
		 */
		public ShpPolygons(Shapefile shapefile, ShapeAttributes normalAttrs, ShapeAttributes highlightAttrs,
				AttributeDelegate attributeDelegate) {
			super(shapefile, normalAttrs, highlightAttrs, attributeDelegate);
		}

		/** {@inheritDoc} */
		@Override
		public void dispose() {
			layer = null;
			recordTree = null;
			initAttributeDelegate = null;
			cache.clear();
			clearList();
		}

		@Override
		public void accept(IShpLayerRenderableVisitor visitor) {
			visitor.visit(this);
		}
	}

	/** Redefine ShapefileExtrudedPolygons to free memory */
	static class ShpExtrudedPolygons extends ShapefileExtrudedPolygons implements Disposable, IShpLayerRenderable {

		/**
		 * Constructor
		 */
		public ShpExtrudedPolygons(Shapefile shapefile, ShapeAttributes normalAttrs, ShapeAttributes highlightAttrs,
				AttributeDelegate attributeDelegate) {
			super(shapefile, normalAttrs, highlightAttrs, attributeDelegate);
		}

		/** {@inheritDoc} */
		@Override
		public void dispose() {
			pickLayer = null;
			initAttributeDelegate = null;
			records.clear();
			clearList();
		}

		@Override
		public void accept(IShpLayerRenderableVisitor visitor) {
			visitor.visit(this);
		}
	}

}
