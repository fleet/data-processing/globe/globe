/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers.shp;

import java.util.Observer;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWShapefileLayer;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.layers.ILayerParameters;
import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.layers.shp.ShpLayerFactory.ShpExtrudedPolygons;
import fr.ifremer.viewer3d.layers.shp.ShpLayerFactory.ShpPolygons;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Renderable;

/**
 * WW Layer for rendering SHP file
 */
public class ShpLayer extends RenderableLayer implements ILayerParameters, IWWShapefileLayer {

	public static final String RECORD_POINT_KEY = "fr.ifremer.shapefile.recordpoint";

	/** Parameters of ShpLayer */
	private final ShpLayerParameters displayParameters;

	/** LayerParameters */
	protected ShpSessionParameters sessionParameters;

	private BooleanPreferenceAttribute fillPolygonPref;
	private Observer fillPolygonPrefObserver;
	private IChangeListener fillFlagChangeListener;

	/**
	 * Constructor
	 */
	public ShpLayer(IWWShapefileLayer.Data data) {
		displayParameters = new ShpLayerParameters(this, data.getFields());
		displayParameters.getPossibleValues().addChangeListener(evt -> updateShpRenderableAttributes());
	}

	@Override
	public void dispose() {
		super.dispose();
		if (fillPolygonPref != null && fillPolygonPrefObserver != null)
			fillPolygonPref.deleteObserver(fillPolygonPrefObserver);
		if (fillFlagChangeListener != null)
			displayParameters.getFillPolygonFlag().removeChangeListener(fillFlagChangeListener);
		displayParameters.dispose();
	}

	@Override
	public void addRenderable(Renderable renderable) {
		super.addRenderable(renderable);

		if (fillPolygonPrefObserver == null
				&& (renderable instanceof ShpPolygons || renderable instanceof ShpExtrudedPolygons)) {
			// Polygons have to be filled ?
			fillPolygonPref = Activator.getPluginParameters().getIsFillPolygonAttribute();

			// react when preference updated
			fillPolygonPrefObserver = (observable, arg) -> displayParameters.getFillPolygonFlag()
					.set(fillPolygonPref.getValue());
			fillPolygonPref.addObserver(fillPolygonPrefObserver);

			// react when flag updated
			setValue("WritableBoolean.fillFlag", displayParameters.getFillPolygonFlag());
			fillFlagChangeListener = e -> updateShpRenderableAttributes();
			displayParameters.getFillPolygonFlag().addChangeListener(fillFlagChangeListener);
		}

		if (renderable instanceof IShpLayerRenderable shpLayerRenderable)
			shpLayerRenderable.accept(new ShapeAttributesProvider(displayParameters));
	}

	/** Some colors have changed... */
	private void updateShpRenderableAttributes() {
		ShapeAttributesProvider shapeAttributesProvider = new ShapeAttributesProvider(displayParameters);
		StreamSupport.stream(getRenderables().spliterator(), false)//
				.filter(IShpLayerRenderable.class::isInstance)//
				.map(IShpLayerRenderable.class::cast)//
				.forEach(renderable -> renderable.accept(shapeAttributesProvider));
		IGeographicViewService.grab().refresh();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage("icons/16/shapes.png", getClass()));
	}

	/** Getter of {@link #displayParameters} */
	public ShpLayerParameters getDisplayParameters() {
		return displayParameters;
	}

	/** {@inheritDoc} */
	@Override
	public void setOpacity(double opacity) {
		super.setOpacity(opacity);
		updateShpRenderableAttributes();
	}

	/** Getter of {@link #sessionParameters} */
	@Override
	public ShpSessionParameters getLayerParameters() {
		if (sessionParameters == null) {
			sessionParameters = new ShpSessionParameters();
			sessionParameters.setAttributeName(displayParameters.getAttributeName().get());
			sessionParameters.setColorMapIndex(displayParameters.getColorMapIndex().get());
			sessionParameters.setColorMapInvert(displayParameters.getColorMapInvert().get());
			sessionParameters.setPossibleValues(displayParameters.getPossibleValues().asList());
			sessionParameters.setVisibility(isEnabled());
			sessionParameters.hook(displayParameters);
		}
		return sessionParameters;
	}

	/** Setter of {@link #sessionParameters} */
	@Override
	public void setLayerParameters(LayerParameters layerParameters) {
		if (layerParameters instanceof ShpSessionParameters) {
			sessionParameters = (ShpSessionParameters) layerParameters;
			Display.getDefault().syncExec(() -> {
				displayParameters.getAttributeName().set(sessionParameters.getAttributeName());
				displayParameters.getColorMapIndex().set(sessionParameters.getColorMapIndex());
				displayParameters.getColorMapInvert().set(sessionParameters.isColorMapInvert());
				displayParameters.getPossibleValues().clear();
				displayParameters.getPossibleValues().addAll(sessionParameters.getPossibleValues());
				setEnabled(sessionParameters.getVisibility());
				sessionParameters.hook(displayParameters);
			});
		}
	}

}
