package fr.ifremer.viewer3d.layers.globalcursor;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.viewer3d.util.tooltip.ToolTipController;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import jakarta.inject.Inject;

/**
 * {@link RenderableLayer} to display a cursor on geographic view.
 */
public class GlobalCursorLayer extends RenderableLayer implements Consumer<GlobalCursor> {

	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	/** Displayed Nasa object */
	private PointPlacemark pointPlacemark;
	private Instant lastTimeCursor = Instant.now();

	private final IGlobalCursorService globalCursorService;
	private final ToolTipController toolTipController;
	private final IGeographicViewService geographicViewService;

	@Inject
	public GlobalCursorLayer(IGlobalCursorService globalCursorService, ToolTipController toolTipController,
			IGeographicViewService geographicViewService) {
		setName("Global Cursor");
		this.globalCursorService = globalCursorService;
		this.toolTipController = toolTipController;
		this.geographicViewService = geographicViewService;
		geographicViewService.addLayerBeforeCompass(this);
		globalCursorService.addListener(this);
		scheduler.scheduleAtFixedRate(this::checkExpirationDate, 1, 1, TimeUnit.SECONDS);
		// PointPlacemark in this layer is not pickable.
		setPickEnabled(false);
	}

	@Override
	public void dispose() {
		scheduler.shutdown();
		globalCursorService.removeListener(this);
	}

	/** Displays cursor on geographic view */
	@Override
	public void accept(GlobalCursor cursor) {
		var position = cursor.position();
		if (position.isPresent()) {
			if (cursor.visible()) {
				if (pointPlacemark == null) {
					pointPlacemark = new PointPlacemark(position.get());
					addRenderable(pointPlacemark);
				} else
					pointPlacemark.setPosition(position.get());

				pointPlacemark.setAltitudeMode(cursor.clampToGround() ? WorldWind.CLAMP_TO_GROUND : WorldWind.ABSOLUTE);
				var attrs = new PointPlacemarkAttributes();
				attrs.setLineColor(new GColor("#64B5F6A5").toAABBGGRR());
				attrs.setUsePointAsDefaultImage(true);
				attrs.setScale(15d);
				pointPlacemark.setAttributes(attrs);
				lastTimeCursor = Instant.now();
				geographicViewService.refresh();
			}
			cursor.description().ifPresent(desc -> toolTipController.showToolTip(desc, cursor.position().get()));
		} else {
			clear();
		}
	}

	private void clear() {
		if (pointPlacemark != null) {
			pointPlacemark = null;
			clearRenderables();
			toolTipController.hideToolTip();
			geographicViewService.refresh();
		}
	}

	private void checkExpirationDate() {
		if (pointPlacemark != null && Duration.between(lastTimeCursor, Instant.now()).getSeconds() > 20)
			clear();
	}

}
