package fr.ifremer.viewer3d.layers;

import java.util.List;

import fr.ifremer.viewer3d.model.IPlayable;
import fr.ifremer.viewer3d.model.PlayerBean;

/** ??? */
public abstract class AbstractPlayableLayer extends MyAbstractLayer  {

	// playable
	protected List<Long> timeList;

	protected String playableType = IPlayable.ISPLAYABLEWITHTIME;

	/**
	 * The current player data.
	 */
	protected PlayerBean playerbean;

	/**
	 * Returns player bean used by this layer.
	 * 
	 * @return player bean used by this layer
	 */
	public PlayerBean getPlayerBean() {
		return playerbean;
	}
}
