//REQ-IHM-004 - Markers Filtering
/**
 * Min/Max filter object
 */
package fr.ifremer.viewer3d.layers.plume.filter;

import fr.ifremer.viewer3d.model.Echo;

public class MinMaxFilter extends GenericFilter {

	protected boolean minSaturation;
	protected boolean maxSaturation;

	public MinMaxFilter(String signal) {
		super(signal);
		minSaturation = false;
		maxSaturation = false;
	}

	@Override
	public boolean valueValidity(Echo echo) {

		boolean result = true;

		status = filteringStatus.NO_FILTERED;

		if (activity && echo != null) {
			float value = echo.getParameter(signal);

			if (value < minValue) {
				result = false;

				if (minSaturation) {
					status = filteringStatus.LOWER_MIN;
				} else {
					status = filteringStatus.FILTERED;
				}
			} else if (value > maxValue) {
				result = false;

				if (maxSaturation) {
					status = filteringStatus.UPPER_MAX;
				} else {
					status = filteringStatus.FILTERED;
				}
			}
		}

		return result;
	}

	public void setMinSaturation(boolean minSaturation) {
		this.minSaturation = minSaturation;
	}

	public boolean isMinSaturation() {
		return minSaturation;
	}

	public boolean isMaxSaturation() {
		return maxSaturation;
	}

	public void setMaxSaturation(boolean maxSaturation) {
		this.maxSaturation = maxSaturation;
	}
}
