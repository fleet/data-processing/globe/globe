//REQ-IHM-004 - Markers Filtering
/**
 * This class maintain an filter objects list
 */
package fr.ifremer.viewer3d.layers.plume.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import fr.ifremer.viewer3d.layers.plume.filter.GenericFilter.filteringStatus;
import fr.ifremer.viewer3d.model.Echo;

public class SignalFiltersList {

	protected List<GenericFilter> filters;
	protected filteringStatus status;

	public SignalFiltersList() {
		filters = new ArrayList<GenericFilter>();
		status = filteringStatus.NO_FILTERED;
	}

	public void addFilter(GenericFilter filter) {
		filters.add(filter);
	}

	public boolean valueValidity(Echo echo) {
		boolean result = true;

		// for(GenericFilter filter: filters)
		// {
		// result = result && filter.valueValidity(echo);
		// }
		ListIterator<GenericFilter> it = filters.listIterator();

		while (result == true && it.hasNext()) {
			GenericFilter filter = it.next();
			result = result && filter.valueValidity(echo);
			status = filter.getFilteringStatus();
		}

		return result;
	}

	public List<GenericFilter> getFilterObjects() {
		return filters;
	}

	public filteringStatus getFilteringSatus() {
		return status;
	}
}
