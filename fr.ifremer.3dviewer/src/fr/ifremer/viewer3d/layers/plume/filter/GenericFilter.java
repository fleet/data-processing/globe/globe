//REQ-IHM-004 - Markers Filtering
/**
 * Base class for all filter objects
 */
package fr.ifremer.viewer3d.layers.plume.filter;

import fr.ifremer.viewer3d.model.Echo;

public abstract class GenericFilter {

	protected boolean activity;
	protected String signal;
	protected float minValue;
	protected float maxValue;
	protected filteringStatus status;

	public enum filteringStatus {
		LOWER_MIN, NO_FILTERED, FILTERED, UPPER_MAX
	}

	public GenericFilter(String signal) {
		activity = false;
		this.signal = signal;
		minValue = Float.MAX_VALUE;
		maxValue = 0f;
		status = filteringStatus.NO_FILTERED;
	}

	public abstract boolean valueValidity(Echo echo);

	public void setActivity(boolean value) {
		activity = value;
	}

	public String getSignal() {
		return signal;
	}

	public void setMinValue(float value) {
		minValue = value;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMaxValue(float value) {
		maxValue = value;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public filteringStatus getFilteringStatus() {
		return status;
	}
}
