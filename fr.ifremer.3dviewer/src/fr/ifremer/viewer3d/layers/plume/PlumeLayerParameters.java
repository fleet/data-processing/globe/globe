package fr.ifremer.viewer3d.layers.plume;

import java.awt.Color;
import java.util.List;

import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.layers.LayerParameters.AbstractLayerParameters;
import fr.ifremer.viewer3d.layers.plume.filter.GenericFilter;
import fr.ifremer.viewer3d.layers.plume.filter.MinMaxFilter;
import fr.ifremer.viewer3d.layers.plume.filter.SignalFiltersList;
import fr.ifremer.viewer3d.model.echoes.FilterItem;
import fr.ifremer.viewer3d.model.echoes.SignalFilters;

public class PlumeLayerParameters extends AbstractLayerParameters {

	public static final String FILTER_MINMAX = "MinMax";

	public PlumeLayerParameters() {
		signalFilterObjects = new SignalFiltersList();
	}

	protected SignalFiltersList signalFilterObjects; // filters objects

	/**
	 * Value for hue scale color rendering.
	 *
	 * @generated
	 */
	private boolean hueScale = true;
	/**
	 * Value for gray scale color rendering.
	 *
	 * @generated
	 */
	private boolean grayscale = false;
	/**
	 * Value for bicolor scale rendering.
	 *
	 * @generated
	 */
	private boolean bicolorScale = false;
	/***
	 * echo's displaying size
	 */
	private double echoSize = 10.0;

	/**
	 * End color for bicolor scale (should only be used if bicolorScale is true).
	 *
	 * @generated
	 */
	private Color bicolorEnd = new Color(255, 0, 0);

	/**
	 * Start color for bicolor scale (should only be used if bicolorScale is true).
	 *
	 * @generated
	 */
	private Color bicolorStart = new Color(255, 255, 255);

	/**
	 * @return the hueScale
	 */
	public boolean isHueScale() {
		return hueScale;
	}

	/**
	 * @param hueScale the hueScale to set
	 */
	public void setHueScale(boolean hueScale) {
		pcs.firePropertyChange("hueScale", this.hueScale, this.hueScale = hueScale);
	}

	/**
	 * @return the grayscale
	 */
	public boolean isGrayscale() {
		return grayscale;
	}

	/**
	 * @param grayscale the grayscale to set
	 */
	public void setGrayscale(boolean grayscale) {
		pcs.firePropertyChange("grayscale", this.grayscale, this.grayscale = grayscale);
	}

	/**
	 * @return the bicolorScale
	 */
	public boolean isBicolorScale() {
		return bicolorScale;
	}

	/**
	 * @param bicolorScale the bicolorScale to set
	 */
	public void setBicolorScale(boolean bicolorScale) {
		pcs.firePropertyChange("bicolorScale", this.bicolorScale, this.bicolorScale = bicolorScale);
	}

	/**
	 * @return the echoSize
	 */
	public double getEchoSize() {
		return echoSize;
	}

	/**
	 * @param echoSize the echoSize to set
	 */
	public void setEchoSize(double echoSize) {
		pcs.firePropertyChange("echoSize", this.echoSize, this.echoSize = echoSize);
	}

	/**
	 * @return the bicolorEnd
	 */
	public Color getBicolorEnd() {
		return bicolorEnd;
	}

	/**
	 * @param bicolorEnd the bicolorEnd to set
	 */
	public void setBicolorEnd(Color bicolorEnd) {
		pcs.firePropertyChange("bicolorEnd", this.bicolorEnd, this.bicolorEnd = bicolorEnd);
	}

	/**
	 * @return the bicolorStart
	 */
	public Color getBicolorStart() {
		return bicolorStart;
	}

	/**
	 * @param bicolorStart the bicolorStart to set
	 */
	public void setBicolorStart(Color bicolorStart) {
		pcs.firePropertyChange("bicolorStart", this.bicolorStart, this.bicolorStart = bicolorStart);
	}

	/**
	 * @return the signalFilterObjects
	 */
	public SignalFiltersList getSignalFilterObjects() {
		return signalFilterObjects;
	}

	/**
	 * find a signal filter with the same signal
	 */
	public GenericFilter getFilter(String signal) {

		GenericFilter signalFilter = null;

		List<GenericFilter> filters = getSignalFilterObjects().getFilterObjects();

		for (GenericFilter filter : filters) {
			if (filter.getSignal().equals(signal)) {
				signalFilter = filter;
				break;
			}
		}

		return signalFilter;

	}

	// create and fill the list of filter parameters
	public void fillFilterList(SignalFilters filters) {
		signalFilterObjects.getFilterObjects().clear();
		if (filters != null) {
			for (FilterItem filter : filters.getItem()) {
				signalFilterObjects.addFilter(new MinMaxFilter(filter.getSignal()));
			}
		}
	}

	/**
	 * When setting a parameters to the layer (typically after a session load), update all {@link SignalFilters} with
	 * their filter values
	 */
	public void updateSignalFilterParameters(SignalFilters filters) {
		for (FilterItem filter : filters.getItem()) {
			GenericFilter param = getFilter(filter.getSignal());
			if (param == null) {
				if (filter.getType() == FILTER_MINMAX) {
					signalFilterObjects.addFilter(new MinMaxFilter(filter.getSignal()));
				}
			}
		}
	}

}
