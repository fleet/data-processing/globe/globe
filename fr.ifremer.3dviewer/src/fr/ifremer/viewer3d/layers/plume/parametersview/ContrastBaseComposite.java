package fr.ifremer.viewer3d.layers.plume.parametersview;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import gov.nasa.worldwind.avlist.AVKey;

/**
 * Composite that changes contrast 
 */
public class ContrastBaseComposite extends Composite {

	private MyAbstractLayer layer;

	// SWT Widget
	private Spinner minSpinner;
	private Spinner maxSpinner;
	private Button minMaxButton;
	private Button customButton;

	/** Scalar of values. */
	protected int scalar = 100;

	public ContrastBaseComposite(Composite parent, int style, MyAbstractLayer layer) {
		super(parent, style);
		this.layer = layer;
		init();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);
		
		if( layer.getMaxTextureValue() * 100d > Integer.MAX_VALUE  || layer.getMinTextureValue() * 100d < Integer.MIN_VALUE) 
			scalar = 1;
		
		createContrastGroup();
	}

	/**
	 * Builds the composite
	 */
	private void createContrastGroup() {

		// Contrast Group
		Group contrastGroup = new Group(this, SWT.NONE);
		contrastGroup.setText("Contrast");
		contrastGroup.setLayout(new GridLayout(2, false));
		contrastGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		minSpinner = new Spinner(contrastGroup, SWT.NONE);
		maxSpinner = new Spinner(contrastGroup, SWT.NONE);
		minMaxButton = new Button(contrastGroup, SWT.RADIO);
		customButton = new Button(contrastGroup, SWT.RADIO);

		minSpinner.setEnabled(false);
		minSpinner.setMaximum(Integer.MAX_VALUE);
		minSpinner.setMinimum(Integer.MIN_VALUE);
		minSpinner.setDigits(scalar > 1 ? 2 : 0); // allow 2 decimal places
		minSpinner.setSelection((int) (layer.getMinContrast() * scalar));
		minSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				layer.setMinContrast(minSpinner.getSelection()/ (double) scalar);
				updateCustomStep(minSpinner, maxSpinner);
				Viewer3D.getWwd().redraw();
				layer.firePropertyChange(AVKey.LAYER, null, layer);
			}

		});
		minSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		maxSpinner.setEnabled(false);
		maxSpinner.setMaximum(Integer.MAX_VALUE);
		maxSpinner.setMinimum(0);
		maxSpinner.setDigits(scalar > 1 ? 2 : 0); // allow 2 decimal places
		maxSpinner.setSelection((int) (layer.getMaxContrast() * scalar));
		maxSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				layer.setMaxContrast(maxSpinner.getSelection()/ (double) scalar);
				updateCustomStep(minSpinner, maxSpinner);
				Viewer3D.getWwd().redraw();
				layer.firePropertyChange(AVKey.LAYER, null, layer);

			}
		});
		maxSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		minMaxButton.setText("Min / Max");
		minMaxButton.setSelection(true);
		minMaxButton.setEnabled(true);
		minMaxButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (minMaxButton.getSelection()) {

					double min = layer.getMinTextureValue();
					double max = layer.getMaxTextureValue();

					minSpinner.setEnabled(false);
					minSpinner.setSelection((int) (min * scalar));

					maxSpinner.setEnabled(false);
					maxSpinner.setSelection((int) (max * scalar));

					layer.setMinContrast(min);
					layer.setMaxContrast(max);
					Viewer3D.getWwd().redraw();
					layer.firePropertyChange(AVKey.LAYER, null, layer);
				}
			}
		});
		minMaxButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		customButton.setText("Custom");
		customButton.setEnabled(true);
		customButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (customButton.getSelection()) {

					double min = layer.getMinTextureValue();
					double max = layer.getMaxTextureValue();

					minSpinner.setSelection((int) (min * scalar));
					maxSpinner.setSelection((int) (max * scalar));

					minSpinner.setEnabled(true);
					maxSpinner.setEnabled(true);

					updateCustomStep(minSpinner, maxSpinner);

					layer.setMaxContrast(maxSpinner.getSelection()/ (double) scalar);
					layer.setMinContrast(minSpinner.getSelection()/ (double) scalar);

					Viewer3D.getWwd().redraw();
					layer.firePropertyChange(AVKey.LAYER, null, layer);

				}
			}
		});
		customButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

	}
	/**
	 * Update the custom step for a couple of min and max spinners.
	 * 
	 * @param min
	 *            The min spinner
	 * @param max
	 *            The max spinner
	 */
	private void updateCustomStep(Spinner min, Spinner max) {
		double customStep = Math.abs(max.getSelection() - min.getSelection()) / 100d;
		min.setIncrement((int) customStep);
		max.setIncrement((int) customStep);
	}
}
