package fr.ifremer.viewer3d.layers.plume.parametersview;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Timer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Scale;

import fr.ifremer.globe.ui.widget.color.ColorComposite;
import fr.ifremer.viewer3d.gui.FilterUIFactory;
import fr.ifremer.viewer3d.gui.GenericFilterUI;
import fr.ifremer.viewer3d.gui.MinMaxFilterUI;
import fr.ifremer.viewer3d.layers.plume.PlumeLayer;
import fr.ifremer.viewer3d.layers.plume.filter.GenericFilter;
import fr.ifremer.viewer3d.model.PlumeBean;
import fr.ifremer.viewer3d.model.echoes.FilterItem;

/**
 * Composite for PlumeLayer
 * 
 * @author Pierre &lt;pierre.mahoudo@altran.com&gt;
 */
public class PlumeComposite extends Composite {

	private PlumeBean plumeBean;
	private Timer echoSizeTimer;;
	private Scale echoSizeScale;
	private PlumeLayer parentLayer;

	public PlumeComposite(Composite parent, int style, PlumeLayer layer) {
		super(parent, style);

		this.parentLayer = layer;
		this.plumeBean = layer.getPlume();

		createPlumeGroup();
	}

	private Group createPlumeGroup() {

		setLayout(new FillLayout(SWT.HORIZONTAL));

		Group plumeGroup = new Group(this, SWT.NONE);
		plumeGroup.setText("Plume Panel");
		plumeGroup.setLayout(new GridLayout(1, false));

		// Filters Group
		Group filterGroup = new Group(plumeGroup, SWT.NONE);
		filterGroup.setText("Filters");
		filterGroup.setLayout(new GridLayout(5, false));
		filterGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		if (plumeBean.getLayerParameters().getSignalFilterObjects().getFilterObjects().size() > 0 && plumeBean.getSignalFilters() != null) {
			// REQ-IHM-004 - Markers Filtering: Construct UI filter and filter
			// object from filter list (read from XML file)
			List<FilterItem> filters = plumeBean.getSignalFilters().getItem();
			for (FilterItem filter : filters) {
				// find filterparameters
				GenericFilter genericfilter = plumeBean.getFilter(filter.getSignal());
				if (genericfilter != null) {
					GenericFilterUI filterUI = FilterUIFactory.createFilterUI(filter.getType(), filterGroup, filter.getLabel(), filter.getSignal(), filter.getPropertyChangeSupport(),
							filter.getPrecision(), genericfilter);

					// Set filter parameters
					if (filterUI instanceof MinMaxFilterUI) {
						((MinMaxFilterUI) filterUI).setMinMaxInterval(filter.getMinValue(), filter.getMaxValue());
					}
				}

			}
		}

		// Contrast group
		ContrastBaseComposite contrastComposite = new ContrastBaseComposite(plumeGroup, SWT.FILL, this.parentLayer);
		contrastComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// Color selection
		ColorComposite colorComposite = new ColorComposite(plumeGroup, SWT.NONE, true, (int) this.parentLayer.getColorMap(), this.parentLayer.isInverseColorMap());
		colorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		colorComposite.addPropertyChangeListener(this.parentLayer);

		// offset group
		OffsetComposite offsetComposite = new OffsetComposite(plumeGroup, SWT.FILL, this.parentLayer);
		offsetComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// echo size group
		Group echoSizeGroup = new Group(plumeGroup, SWT.NONE);
		echoSizeGroup.setText("Echo's displaying size");
		echoSizeGroup.setLayout(new GridLayout(1, false));
		echoSizeGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		echoSizeScale = new Scale(echoSizeGroup, SWT.HORIZONTAL);
		echoSizeScale.setMinimum(0);
		echoSizeScale.setMaximum(20);
		echoSizeScale.setSelection(10);
		echoSizeScale.setPageIncrement(1);

		echoSizeTimer = new Timer(80, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {

						double value = echoSizeScale.getSelection() - 10;

						if ((plumeBean.getEchoSize() + value) >= 0 && (plumeBean.getEchoSize() + value) <= 70) {
							plumeBean.setEchoSize(plumeBean.getEchoSize() + value);
						}
					}

				});
			}
		});

		echoSizeScale.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				echoSizeTimer.stop();
				echoSizeScale.setSelection(10);
			}

			@Override
			public void mouseDown(MouseEvent e) {
				echoSizeTimer.start();
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		echoSizeScale.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		return plumeGroup;
	}
}
