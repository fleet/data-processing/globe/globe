package fr.ifremer.viewer3d.layers.plume.parametersview;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.layers.plume.PlumeLayer;

public class PlumeCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof PlumeLayer
				? Optional.of(new PlumeComposite(parent, SWT.NONE, (PlumeLayer) selection))
				: Optional.empty();
	}

}
