package fr.ifremer.viewer3d.layers.plume.parametersview;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;

/**
 * Composite which contains a spinner to change the vertical offset
 */
public class OffsetComposite extends Composite {


	/**
	 * Constructor : builds the composite
	 */
	public OffsetComposite(Composite parent, int style, MyAbstractLayer layer) {
		super(parent, style);

		// Layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);

		// Group 
		final Group offsetGroup = new Group(this, SWT.NONE);
		offsetGroup.setText("Vertical offset");
		offsetGroup.setLayout(new GridLayout(2, true));
		offsetGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 2));

		// Spinner
		Spinner offsetSpinner = new Spinner(offsetGroup, SWT.NONE);
		offsetSpinner.setToolTipText("Offset");
		offsetSpinner.setDigits(2); // allow 2 decimal places
		offsetSpinner.setMinimum(-1000000);
		offsetSpinner.setMaximum(1000000);
		offsetSpinner.setSelection((int) layer.getOffset() * 100);
		new Label(offsetGroup, SWT.NONE);
		offsetSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				layer.setOffset(offsetSpinner.getSelection() / 100.0f);
				RefreshOpenGLView.fullRefresh();
			}
		});
	}

}
