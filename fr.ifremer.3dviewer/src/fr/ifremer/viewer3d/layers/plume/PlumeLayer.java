/*
 * @License@ 
 */
package fr.ifremer.viewer3d.layers.plume;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.widget.color.ColorComposite;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.ILayerParameters;
import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.pointcloud.PointCloudLayer;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import fr.ifremer.viewer3d.loaders.PlumeEchoesLoader;
import fr.ifremer.viewer3d.model.Echo;
import fr.ifremer.viewer3d.model.PlumeBean;
import fr.ifremer.viewer3d.util.IFiles;
import fr.ifremer.viewer3d.util.ILazyLoading;
import fr.ifremer.viewer3d.util.VBO;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;

/**
 * Layer which displays a points cloud which symbolizes echos detected below a navigation.
 * 
 * @deprecated use {@link PointCloudLayer} instead
 * 
 */
@Deprecated
@XStreamAlias("PlumeLayer")
public class PlumeLayer extends MyAbstractLayer implements Serializable, PropertyChangeListener, ITarget, ILazyLoading,
		IInfos, IFiles, IDeletable, ILayerParameters {

	private static final long serialVersionUID = 1L;
	public static final float DEFAULT_POINT_SIZE = 1.0f;

	// Shader programs source code
	protected static final String SHADER_VERT = "/shader/verticalIntegrationShader.vert";
	protected static final String SHADER_FRAG = "/shader/verticalIntegrationShader.frag";

	/** Plume data for this layer */
	private PlumeBean plume;

	private String baseForm;

	/** If already initialized */
	private boolean initialized = false;

	/** The infos displayed by the popupmenu action Infos */
	private AbstractInfos infos;

	public AbstractInfos getInfos() {
		return infos;
	}

	private Sector sector;

	// Arguments about OpenGL buffers
	/** OpenGL buffer which contains echo data. */
	private VBO echoVBO;
	/** OpenGL buffer which contains 3D position of echos to draw. */
	private VBO verticesVBO;
	// Argument about color palette
	private Texture paletteTexture;
	private BufferedImage paletteImage;
	private int currentPalette;
	private boolean currentPaletteInverted;
	private int[] texturesId;
	// List of the shader programs used by this renderer
	private Map<GL, Integer> shaderPrograms = new HashMap<GL, Integer>(4);
	/**
	 * If the content to render as change this flag should be put to true in order to rebuild the underlying display
	 * list during next redraw.
	 */
	private boolean needRebuild;
	// Variables about echoes data
	private float minEnergy;
	private float maxEnergy;

	/**
	 * Constructor of {@link PlumeLayer}.
	 * 
	 * @param plumeBean {@link PlumeBean}
	 */
	public PlumeLayer(PlumeBean plumeBean) {

		this.plume = plumeBean;
		this.infos = new AbstractInfos();
		// Set the layer name
		setName((this.plume.getFile() == null) ? DicoBundle.getString("KEY_PLUMEECHOESLOADER_LAYER_DEFAULT_NAME")
				: this.plume.getFile().getName());
		// Add a property listener to the bean which is useful for filters creation and their events management
		this.plume.addPropertyChangeListener(this);
		// Initialize the variables about echoes data
		this.minEnergy = Float.MAX_VALUE;
		this.maxEnergy = Float.NEGATIVE_INFINITY;
		// Initialize the drawing configuration
		this.needRebuild = true;
		// Initialize the echo OpenGL buffer
		this.echoVBO = new VBO(1);
		// Initialize the vertices OpenGL buffer
		this.verticesVBO = new VBO(3);
		// Initialize the color palette feature
		listerColorMap(true);
		setUseColorMap(true);
		setColorMap(0);
		setColorMapOri(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.nasa.worldwind.layers.AbstractLayer#dispose()
	 */
	@Override
	public void dispose() {
		this.plume.removePropertyChangeListener(this);
		this.removePropertyChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ifremer.viewer3d.util.ILazyLoading#getInputFile()
	 */
	@Override
	public File getInputFile() {
		return this.plume.getFile();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doRender(DrawContext dc) {
		// Get the OpenGL context from the input argument
		GL2 gl = dc.getGL().getGL2();
		// Get the shader program ID for the future drawing
		int shaderProgramID = getShaderProgramID(gl);
		// Recompute the color palette
		initializeColorPalette(gl);
		// Initialize the uniforms of the shader program
		initializeShaderProgramUniforms(gl, shaderProgramID);
		// Recompute the whole model if needed
		if (needRebuild) {
			computeEchosExtremumValues(dc);
		}
		// Draw the OpenGL shape from the initialized buffers
		draw(gl, shaderProgramID);
		// Configuration of vertices depth drawing
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glDepthMask(true);
		// Returns to the main shader program
		if (shaderProgramID != 0) {
			gl.glUseProgram(0);
		}
	}

	/**
	 * Initialize the constant uniforms of the fragment program used by this renderer.
	 * 
	 * @param gl OpenGL environment context
	 * @param shaderProgramID id of the shader program used by this renderer
	 */
	private void initializeShaderProgramUniforms(GL2 gl, int shaderProgramID) {
		if (shaderProgramID != 0) {
			// Initialize a uniform which will manage the renderer opacity
			gl.glUseProgram(shaderProgramID);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgramID, "opacity"), (float) getOpacity());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgramID, "minContrastValue"), (float) this.getMinContrast());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgramID, "maxContrastValue"), (float) this.getMaxContrast());
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgramID, "inverseColormap"),
					this.currentPaletteInverted ? 1.0f : 0.0f);

		}
	}

	/**
	 * Get the ID of the shader program used by this renderer.
	 * 
	 * @param gl OpenGL environment context
	 * @return the ID of the shader program used by this renderer
	 */
	protected int getShaderProgramID(GL2 gl) {

		int shaderProgramID = 0;

		try {
			if (this.shaderPrograms.containsKey(gl)) {
				// Get the desired shader program if it exists
				shaderProgramID = this.shaderPrograms.get(gl);
			} else {
				// Compile and generate a shader program ID and add it to the shader prodram renderer list
				shaderProgramID = ShaderUtil.createShader(this, gl, SHADER_VERT, SHADER_FRAG);
				if (shaderProgramID != 0) {
					this.shaderPrograms.put(gl, shaderProgramID);
				}
			}
		} catch (GIOException e) {
			e.printStackTrace();
		}

		return shaderProgramID;
	}

	/**
	 * Compute the extremum echo values from the input {@link PlumeLayer#echoes} list.
	 * 
	 * @param dc OpenGL environment context
	 */
	private void computeEchosExtremumValues(DrawContext dc) {

		// Allocate the echo OpenGL buffer (its size is equal to the number of echos contained in the input file)
		this.echoVBO.allocateBuffer(this.plume.getFilteredEchoes2().size());
		// Allocate the vertices OpenGL buffer (number of echos contained in the input file per three dimension
		// coordinates)
		this.verticesVBO.allocateBuffer(this.plume.getFilteredEchoes2().size() * 3);

		// Initialize the variables about echoes data
		List<Echo> echoes = this.plume.getFilteredEchoes2();

		boolean initMinMaxValues = this.minEnergy == Float.MAX_VALUE && this.maxEnergy == Float.NEGATIVE_INFINITY;

		for (Echo echo : echoes) {
			float energy = echo.getEnergy();
			minEnergy = Math.min(minEnergy, energy);
			maxEnergy = Math.max(maxEnergy, energy);
		}

		if (initMinMaxValues) {
			setMinTextureValue(minEnergy);
			setMaxTextureValue(maxEnergy);
			setMinContrast(minEnergy);
			setMaxContrast(maxEnergy);
		}

		fillOpenGLBuffers(dc, echoes);
	}

	/**
	 * Fill the vertices and the echos buffers and send them to the GPU.
	 * 
	 * @param dc OpenGL environment context
	 * @param echoes input data to put in the two buffers
	 */
	private void fillOpenGLBuffers(DrawContext dc, List<Echo> echoes) {

		for (Echo echo : echoes) {
			// Get the position of the echo in the 3D globe
			Position position = echo.getPosition();
			Vec4 point = dc.getGlobe().computePointFromPosition(position.latitude, position.longitude,
					position.elevation * dc.getVerticalExaggeration() + getOffset());
			// Fill the vertices buffer with the computed position
			this.verticesVBO.put(point.x);
			this.verticesVBO.put(point.y);
			this.verticesVBO.put(point.z);
			// Fill the echo buffer with the energy value
			this.echoVBO.put(echo.getEnergy());
		}
		// Rewind and send the filled buffers to the GPU
		this.verticesVBO.rewind();
		this.verticesVBO.bindAndLoad(dc.getGL().getGL2());
		this.echoVBO.rewind();
		this.echoVBO.bindAndLoad(dc.getGL().getGL2());
	}

	/**
	 * Initialize the color palette which will be applied to this renderer.
	 * 
	 * @param gl OpenGL environment context
	 */
	protected void initializeColorPalette(GL2 gl) {

		// Generate an ID to the buffer texture
		this.texturesId = new int[5];
		gl.glGenTextures(5, this.texturesId, 0);

		if (this.paletteImage == null || this.currentPalette != (int) getColorMap()
				|| this.currentPaletteInverted != isInverseColorMap()) {
			// Reset the variables which defines the condition just before
			this.currentPalette = (int) getColorMap();
			this.currentPaletteInverted = isInverseColorMap();
			// Build the buffered image which will represent the choosen palette
			float[][] colors = ColorMap.getColormap(this.currentPalette);
			int width = 100;
			int height = 5;
			this.paletteImage = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
					.getDefaultConfiguration().createCompatibleImage(width, height);
			// Define a shader program for color palette generation
			Graphics2D g2 = (Graphics2D) paletteImage.getGraphics();
			// Fill the buffered image with the color defined by the choosen palette
			for (int i = 0; i < width; i++) {
				int index = Math.round(i / (float) width * 256);
				int r = (int) (colors[ColorMap.red][index] * 255);
				int g = (int) (colors[ColorMap.green][index] * 255);
				int b = (int) (colors[ColorMap.blue][index] * 255);
				// Use OpenGL instruction to draw a vertival line with the desired color
				g2.setColor(new Color(r, g, b));
				g2.drawLine(i, 0, i, height);
			}
			// Dispose the shader program used for color palette generation
			g2.dispose();
			// Generation of a texture from the buffered image.
			this.paletteTexture = AWTTextureIO.newTexture(gl.getGLProfile(), this.paletteImage, false);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOpacity(double opacity) {
		super.setOpacity(opacity);
		this.needRebuild = true;
	}

	/**
	 * Configure all the OpenGL drawing variables.
	 * 
	 * @param gl OpenGL environment context
	 * @param shaderProgram id of the shader program to draw
	 */
	protected void draw(GL2 gl, int shaderProgram) {

		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT
				| GL2.GL_TEXTURE_BIT | GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

		gl.glEnable(GL.GL_BLEND);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		// Initialize a uniform which will manage the color palette
		gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "palette"), 0);

		// Initialize the pointer to the vertices buffer
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.verticesVBO.getBufferName()[0]);
		gl.glVertexPointer(this.verticesVBO.getElementSize(), GL.GL_FLOAT, 0, 0);

		// Initialize the amplification attribute
		int attributeName = gl.glGetAttribLocation(shaderProgram, "amplificationBuffer");
		gl.glEnableVertexAttribArray(attributeName);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.echoVBO.getBufferName()[0]);
		gl.glVertexAttribPointer(attributeName, this.echoVBO.getElementSize(), GL.GL_FLOAT, false, 0, 0);

		// Initialize the pointer to the palette buffer
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, texturesId[0]);
		this.paletteTexture.bind(gl);

		// Define type of shapes used for rendering
		gl.glPointSize((float) this.plume.getEchoSize());
		gl.glDrawArrays(GL2.GL_POINTS, 0, this.plume.getFilteredEchoes2().size());
		gl.glPointSize(DEFAULT_POINT_SIZE);

		// Initialize buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

		// Unbind color texture
		if (paletteTexture != null) {
			paletteTexture.disable(gl);
		}

		// Send texture to the fragment program
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);

		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

		// Dispose the OpenGL environment context
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glPopAttrib();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {

		String name = event.getPropertyName();

		if (PlumeBean.PROPERTY_ECHOES.equals(name)) {

			this.needRebuild = true;

		} else if (PlumeBean.PROPERTY_PINGSFILTER.equals(name) || PlumeBean.PROPERTY_ENERGYFILTER.equals(name)
				|| PlumeBean.PROPERTY_TOTALENERGYFILTER.equals(name) || PlumeBean.PROPERTY_DEPTHFILTER.equals(name)
				|| PlumeBean.PROPERTY_MINPINGVALUE.equals(name) || PlumeBean.PROPERTY_MAXPINGVALUE.equals(name)
				|| PlumeBean.PROPERTY_MINENERGYVALUE.equals(name) || PlumeBean.PROPERTY_MAXENERGYVALUE.equals(name)
				|| PlumeBean.PROPERTY_MINTOTALENERGYVALUE.equals(name)
				|| PlumeBean.PROPERTY_MAXTOTALENERGYVALUE.equals(name) || PlumeBean.PROPERTY_MINDEPTHVALUE.equals(name)
				|| PlumeBean.PROPERTY_MAXDEPTHVALUE.equals(name) || PlumeBean.PROPERTY_SIGNALFILTER.equals(name)
				|| PlumeBean.PROPERTY_ALLPARAMETER.equals(name)) {

			this.needRebuild = true;
			firePropertyChange(AVKey.LAYER, null, this);

		} else if (ColorComposite.PROPERTY_COLORMAP_UPDATE.equals(name)) {

			this.setColorMap((int) event.getNewValue());
			firePropertyChange(AVKey.LAYER, null, this);

		} else if (ColorComposite.PROPERTY_INVERT_UPDATE.equals(name)) {

			this.setInverseColorMap((boolean) event.getNewValue());
			firePropertyChange(AVKey.LAYER, null, this);

		} else {
			super.propertyChange(event);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sector getSector() {

		if (this.plume == null) {
			return Sector.EMPTY_SECTOR;
		}

		if (this.sector == null) {
			List<Echo> echoes = new ArrayList<Echo>(this.plume.getEchoes());
			List<Position> positions = new ArrayList<Position>(echoes.size());
			for (Echo echo : echoes) {
				positions.add(echo.getPosition());
			}
			this.sector = Sector.boundingSector(positions);
		}
		return this.sector;
	}

	@Override
	public void initialize() {

		if (!this.initialized) {
			PlumeEchoesLoader loader = new PlumeEchoesLoader();
			try {
				loader.fullLoad(this.plume.getFile(), this);
				this.initialized = true;
			} catch (Exception e) {
				Logging.logger().severe("Plume loading fails :\n" + e);
				return;
			}
		}
	}

	@Override
	public boolean isInitialized() {
		return this.initialized;
	}

	@Override
	public List<Property<?>> getProperties() {
		return this.infos.getProperties();
	}

	@Override
	public Map<File, Boolean> getFiles() {

		Map<File, Boolean> files = new HashMap<File, Boolean>(2);
		files.put(this.plume.getFile(), true);
		List<File> binaries = this.plume.getBinaryFiles();
		if (binaries != null && !binaries.isEmpty()) {
			File first = binaries.get(0);
			File dir = first.getParentFile();
			if (dir.isDirectory()) {
				files.put(dir, false);
			}

			for (File f : binaries) {
				files.put(f, true);
			}
		}
		return files;
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	@Override
	public LayerParameters getLayerParameters() {
		return this.plume.getLayerParameters();
	}

	@Override
	public void setLayerParameters(LayerParameters arg) {
		this.plume.setLayerParameters(arg);
	}

	/**
	 * @return the {@link #plume}
	 */
	public PlumeBean getPlume() {
		return plume;
	}

	/**
	 * @return the {@link #baseForm}
	 */
	public String getBaseForm() {
		return baseForm;
	}

	/**
	 * @param baseForm the {@link #baseForm} to set
	 */
	public void setBaseForm(String baseForm) {
		this.baseForm = baseForm;
	}
}
