/**
 * 
 */
package fr.ifremer.viewer3d.layers.annotation.http;

import java.io.File;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.annotation.AnnotationFileInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.annotation.parametresexplorer.AnnotationLayerParamCompositeFactory;

/**
 * Service for loading annotations data from sextant server
 */
@Creatable
@Singleton
public class HttpAnnotationController {

	/** Logger */
	private static final Logger log = LoggerFactory.getLogger(HttpAnnotationController.class);

	@Inject
	private IFileService fileService;
	@Inject
	private IWWLayerService layerService;
	@Inject
	private ITreeNodeFactory treeNodeFactory;
	@Inject
	private IProcessService processService;
	@Inject
	private ProjectExplorerModel projectExplorerModel;
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;
	@Inject
	private AnnotationLayerParamCompositeFactory annotationLayerParamCompositeFactory;

	/** Show annotation layer. */
	public void show() {
		GroupNode groupNode = annotationLayerParamCompositeFactory.getAnnotationLayerNode().orElse(null);
		if (groupNode == null)
			processService.runInForeground("Loading annotations", this::loadInProcess);
		else
			groupNode.setChecked(true);
	}

	public void hide() {
		annotationLayerParamCompositeFactory.getAnnotationLayerNode().ifPresent(n -> n.setChecked(false));
	}

	public boolean isAnnotationsVisible() {
		return annotationLayerParamCompositeFactory.getAnnotationLayerNode().isPresent()
				&& annotationLayerParamCompositeFactory.getAnnotationLayerNode().get().isChecked();
	}

	/** Download annotations data, parse the result and add WW layer to model */
	private IStatus loadInProcess(IProgressMonitor monitor, Logger logger) {
		// List of known annotation files
		List<String> urls = List.of(
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_BlackSea_01_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_EastMed_02_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_CentralMed_03_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_WestMed_04_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_Biscay_05_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_Azores_06_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_MadCan_07_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_BalticSea_08_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_Iberian_NEAtl_09_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_CelticSea_10_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_NorthSea_11_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_Norwegian_IcelandicSea_12_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_Barents_13_2022",
				"https://sextant.ifremer.fr/geonetwork/srv/api/annotations/emodnet_bathy_Artic_14_2022");

		GroupNode groupNode = new GroupNode("Annotations", Icons.ANNOTATION.toImage());
		SubMonitor subMonitor = SubMonitor.convert(monitor, urls.size());
		for (String url : urls) {
			subMonitor.setTaskName("Downloading " + url);
			if (monitor.isCanceled())
				break;
			downloadAnnotations(url, logger).ifPresent(annotationFileInfo -> {
				try {
					layerService.getLayers(annotationFileInfo, true, new NullProgressMonitor()).ifPresent(store -> {

						FolderNode folderNode = treeNodeFactory.createFolderNode(groupNode,
								annotationFileInfo.getBaseName(), "Downloaded from " + url);
						fileLayerStoreModel.add(store);
						treeNodeFactory.createOrUpdateLayerNodes(folderNode, store);
					});
				} catch (GIOException e) {
					logger.warn("Unable to create annotation layers", e);
				}
			});
			subMonitor.worked(1);
		}

		if (!monitor.isCanceled()) {
			if (groupNode.getChildren().isEmpty()) {
				Messages.openInfoMessage("Loading annotations", "No annotation available");
			} else {
				projectExplorerModel.addBackgroundNode(groupNode);
				annotationLayerParamCompositeFactory.setAnnotationLayerNode(Optional.of(groupNode));
			}
		}

		return Status.OK_STATUS;
	}

	/** Request the annotations from sextant and return the corresponding AnnotationFileInfo */
	private Optional<AnnotationFileInfo> downloadAnnotations(String url, Logger logger) {
		try {

			HttpRequest request = HttpRequest.newBuilder()//
					.uri(new URI(url))//
					.header("Accept", "application/json")//
					.GET() //
					.build();

			String filename = url.substring(url.lastIndexOf("/") + 1);
			File tempFile = new File(TemporaryCache.getRootPath(), filename + ".json");
			HttpResponse<Path> response = HttpClient.newBuilder()//
					.build()//
					.send(request, BodyHandlers.ofFile(tempFile.toPath()));

			if (response.statusCode() == 200) {
				return fileService.getFileInfo(tempFile.getPath())//
						.stream()//
						.filter(AnnotationFileInfo.class::isInstance)//
						.map(AnnotationFileInfo.class::cast)//
						.findFirst();
			} else {
				logger.warn("Http response {}", response.statusCode());
			}
		} catch (InterruptedException e) {
			// Restore interrupted state...
			Thread.currentThread().interrupt();
		} catch (Exception e) {
			log.info("Unable to get annotation from {} : {}", url, e.getMessage());
		}

		return Optional.empty();
	}

}
