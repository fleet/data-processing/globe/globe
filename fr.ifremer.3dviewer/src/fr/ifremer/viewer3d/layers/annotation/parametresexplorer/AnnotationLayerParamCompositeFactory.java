package fr.ifremer.viewer3d.layers.annotation.parametresexplorer;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.viewer3d.application.context.ContextInitializer;

/**
 * Make a Composite to edit the {@link IWWAnnotationLayer} parameters.
 */
@Component(name = "globe_viewer3d_layers_annotation_param_composite_factory", service = {
		IParametersViewCompositeFactory.class, AnnotationLayerParamCompositeFactory.class })
public class AnnotationLayerParamCompositeFactory implements IParametersViewCompositeFactory {

	/** Layer node of annotation in project explorer */
	private Optional<GroupNode> annotationLayerNode = Optional.empty();

	/** {@inheritDoc} */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		if (selection instanceof IWWAnnotationLayer annotationLayer)
			return Optional.of(makeComposite(parent, annotationLayer));

		if(annotationLayerNode.isPresent()) {
			if (annotationLayerNode.equals(Optional.of(selection)))
				return Optional.of(makeComposite(parent, null));
	
			if (selection instanceof FolderNode folderNode && folderNode.getParent().equals(annotationLayerNode))
				return Optional.of(IParametersViewService.grab().buildComposite(folderNode.getChildren().get(0), parent));
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	protected Composite makeComposite(Composite parent, IWWAnnotationLayer layer) {
		boolean forAllAnnotations = layer == null;
		if (layer == null && annotationLayerNode.isPresent()) {
			GroupNode rootNode = annotationLayerNode.get();
			if (rootNode.hasChildren()) {
				FolderNode folderNode = rootNode.getChildNode(FolderNode.class, f -> true).orElse(null);
				if (folderNode != null) {
					LayerNode layerNode = folderNode.getChildNode(LayerNode.class, f -> true).orElse(null);
					if (layerNode != null && layerNode.getLayer().isPresent()
							&& layerNode.getLayer().get() instanceof IWWAnnotationLayer annotationLayer)
						layer = annotationLayer;
				}
			}
		}

		var result = new AnnotationParametersComposite(parent, layer, forAllAnnotations);
		ContextInitializer.inject(result);
		return result;
	}

	public Optional<GroupNode> getAnnotationLayerNode() {
		return annotationLayerNode;
	}

	public void setAnnotationLayerNode(Optional<GroupNode> annotationLayerNode) {
		this.annotationLayerNode = annotationLayerNode;
	}
}
