package fr.ifremer.viewer3d.layers.annotation.parametresexplorer;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.ui.service.worldwind.layer.annotation.AnnotationParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import fr.ifremer.viewer3d.layers.sync.annotation.WWAnnotationLayerSynchronizer;
import fr.ifremer.viewer3d.layers.sync.ui.LayerParametersSynchronizerComposite;

/**
 * Composite that changes parameters of IWWPolygonLayer.
 */
public class AnnotationParametersComposite extends Composite {

	/** The layer parameters synchronizer get from Context **/
	@Inject
	private WWAnnotationLayerSynchronizer layersSynchronizer;

	/** Model used in this Composite */
	private final IWWAnnotationLayer annotationLayer;
	/** When true, synchronize all annotations layers */
	private final boolean forAllAnnotations;

	/**
	 * Constructor.
	 */
	public AnnotationParametersComposite(Composite parent, IWWAnnotationLayer annotationLayer,
			boolean forAllAnnotations) {
		super(parent, SWT.NONE);
		this.annotationLayer = annotationLayer;
		this.forAllAnnotations = forAllAnnotations;
	}

	@PostConstruct
	private void postConstruct() {
		initializeComposite();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void initializeComposite() {
		GridLayoutFactory.fillDefaults().margins(0, 0).numColumns(1).applyTo(this);

		// Synchronizer composite
		if (!forAllAnnotations) {
			var grpSynchronization = new LayerParametersSynchronizerComposite<>(this, layersSynchronizer,
					annotationLayer);
			GridDataFactory.fillDefaults().grab(true, true).applyTo(grpSynchronization);
			grpSynchronization.setText("Synchronization with annotation layers");
			grpSynchronization.adaptToLayer(annotationLayer);
		}

		// Visibility
		Group grpVisibility = new Group(this, SWT.NONE);
		grpVisibility.setText("Visibility");
		grpVisibility.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
		GridLayoutFactory.fillDefaults().margins(5, 5).applyTo(grpVisibility);

		Button borderBtn = new Button(grpVisibility, SWT.CHECK);
		borderBtn.setText("Border");
		borderBtn.setSelection(getParameters().drawOutline());
		borderBtn.addSelectionListener(SelectionListener.widgetSelectedAdapter(
				e -> fireParametersChanged(getParameters().withDrawOutline(borderBtn.getSelection()))));

		Button interiorBtn = new Button(grpVisibility, SWT.CHECK);
		interiorBtn.setText("Interior");
		interiorBtn.setSelection(getParameters().fill());
		interiorBtn.addSelectionListener(SelectionListener.widgetSelectedAdapter(
				e -> fireParametersChanged(getParameters().withFill(interiorBtn.getSelection()))));

		Button labelBtn = new Button(grpVisibility, SWT.CHECK);
		labelBtn.setText("Label");
		labelBtn.setSelection(getParameters().label());
		labelBtn.addSelectionListener(SelectionListener
				.widgetSelectedAdapter(e -> fireParametersChanged(getParameters().withLabel(labelBtn.getSelection()))));

		Button markerBtn = new Button(grpVisibility, SWT.CHECK);
		markerBtn.setText("Marker");
		markerBtn.setSelection(getParameters().label());
		markerBtn.addSelectionListener(SelectionListener.widgetSelectedAdapter(
				e -> fireParametersChanged(getParameters().withMarker(markerBtn.getSelection()))));

		// Opacity
		Group grpOpacity = new Group(this, SWT.NONE);
		grpOpacity.setText("Opacity");
		grpOpacity.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
		GridLayoutFactory.fillDefaults().margins(5, 5).applyTo(grpOpacity);

		SliderComposite sliderOpacity = new SliderComposite(grpOpacity, SWT.NONE, 0f, 1f, 0.1f, false, //
				opacity -> fireParametersChanged(getParameters().withOpacity(opacity)));
		sliderOpacity.setValue(getParameters().opacity());
		sliderOpacity.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
	}

	/**
	 * Changes and propagates parameters
	 */
	private void fireParametersChanged(AnnotationParameters newParameters) {
		annotationLayer.setParameters(newParameters);

		if (forAllAnnotations)
			layersSynchronizer.activateSynchronizationForAll(annotationLayer);

		layersSynchronizer.synchonizeWith(annotationLayer);
	}

	/**
	 * @return the {@link #AnnotationParameters}
	 */
	private AnnotationParameters getParameters() {
		return annotationLayer.getParameters();
	}
}
