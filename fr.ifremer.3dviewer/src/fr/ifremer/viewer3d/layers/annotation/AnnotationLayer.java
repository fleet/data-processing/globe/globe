package fr.ifremer.viewer3d.layers.annotation;

import java.awt.Color;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.gdal.ogr.Geometry;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.annotation.Annotation;
import fr.ifremer.globe.core.model.file.pointcloud.IPoint;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerFileInfo;
import fr.ifremer.globe.core.model.marker.impl.MarkerBuilder;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.AnnotationParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.WWRenderableLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.map.TypedMap;
import fr.ifremer.viewer3d.layers.label.LabelsLayer;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.SurfacePolygon;
import gov.nasa.worldwind.render.airspaces.BasicAirspaceAttributes;
import jakarta.inject.Inject;

/**
 * Layer used to display annotation (polygon and point) on 3D view
 */
public class AnnotationLayer extends WWRenderableLayer implements IWWAnnotationLayer {

	/** 3D view service */
	@Inject
	private IGeographicViewService geographicViewService;
	/** Session service */
	@Inject
	private ISessionService sessionService;

	/** Original file info */
	private final IFileInfo fileInfo;

	/** Rendering parameters */
	private AnnotationParameters parameters = new AnnotationParameters(true, true, true, true, 1f);

	/** Layer to render labels */
	private final List<IMarker> markers = new LinkedList<>();
	/** Layer to render labels */
	private final IWWLabelLayer labelLayer;

	/** Layer to render markers */
	private final WWMarkerLayer<IMarker> markerLayer;

	/**
	 * Constructor
	 */
	public AnnotationLayer(IFileInfo fileInfo) {
		this.fileInfo = fileInfo;
		setName("Annotations of " + fileInfo.getPath());
		setPickEnabled(true);

		labelLayer = new LabelsLayer("Labels of " + fileInfo.getPath());
		labelLayer.setVisibilityDistance(1e6);

		markerLayer = new WWMarkerLayer<>(new MarkerProvider());
		markerLayer.setSaveable(false);
		markerLayer.setName("Markers of " + fileInfo.getPath());
		markerLayer
				.setParameters(markerLayer.getParameters().withProjectMarkerOnTerrain(true).withResizingWithZoom(true));
	}

	/** Add one annotation */
	@Override
	public void acceptAnnotation(Annotation annotation) {
		List<Position> positions = convertGeometryToPositions(annotation.geometry());
		if (positions.isEmpty())
			return;
		if (positions.size() > 1)
			addSurfaceAnnotation(annotation, positions);
		else if (positions.size() == 1)
			addMarkerAnnotation(annotation, positions.get(0));

		if (!annotation.label().isEmpty()) {
			Geometry geometry = annotation.geometry();
			Geometry center = geometry.Centroid();
			labelLayer.acceptLabel(annotation.label(), center.GetX(), center.GetY(), annotation.labelColor(), false,
					annotation.fontName(), annotation.fontSize());
		}

	}

	/** Add one annotation */
	@Override
	public void acceptLabelAnnotation(IPoint point) {
		Optional<String> label = point.getLabel();
		if (label.isPresent()) {
			labelLayer.acceptLabel(label.get(), point.getLongitude(), point.getLatitude(), GColor.FUCHSIA, false,
					"Arial-PLAIN-20", 20);
		}
	}

	/** Creates a SurfacePolygon to render a multi-points geometry */
	private void addSurfaceAnnotation(Annotation annotation, List<Position> positions) {
		SurfacePolygon result = new SurfacePolygon();
		result.setDragEnabled(false);
		result.setOuterBoundary(positions);

		var attributes = new BasicAirspaceAttributes();
		Color outlineColor = ColorUtils.convertGColorToAWT(annotation.outline());
		attributes.setDrawOutline(true);
		attributes.setOutlineMaterial(new Material(outlineColor));
		attributes.setOutlineWidth(annotation.outlineWidth());
		attributes.setOutlineOpacity(parameters.opacity() * (outlineColor.getAlpha() / 255d));

		Color interiorColor = ColorUtils.convertGColorToAWT(annotation.interior());
		attributes.setDrawInterior(true);
		attributes.setInteriorMaterial(new Material(interiorColor));
		attributes.setInteriorOpacity(parameters.opacity() * (interiorColor.getAlpha() / 255d));
		result.setValue("original_opacity", interiorColor.getAlpha() / 255d);

		result.setAttributes(attributes);
		result.setHighlightAttributes(attributes);

		if (!annotation.label().isEmpty())
			result.setValue(AVKey.ROLLOVER_TEXT, annotation.label());
		else
			result.setValue(AVKey.ROLLOVER_TEXT, getName());

		addRenderable(result);
	}

	/** Creates a Marker to render a single point geometry */
	private void addMarkerAnnotation(Annotation annotation, Position position) {
		String markerId = fileInfo.getPath() + "_" + markers.size();
		MarkerBuilder builder = new MarkerBuilder(markerId, position.latitude.degrees, position.longitude.degrees, 0);
		builder.setColor(annotation.interior());
		builder.setComment(annotation.label());
		builder.setShape("Sphere");
		builder.setSize(annotation.outlineWidth());

		IMarker marker = builder.build();
		markers.add(marker);
		markerLayer.addMarker(marker);
	}

	/**
	 * Creates a list of Position from the polygon geometry
	 */
	public List<Position> convertGeometryToPositions(Geometry geometry) {
		if (geometry != null) {
			if (geometry.GetGeometryCount() > 0)
				geometry = geometry.GetGeometryRef(0);
			double[][] lonlats = geometry.GetPoints();
			if (lonlats != null)
				return Arrays.stream(lonlats).map(lonlat -> Position.fromDegrees(lonlat[1], lonlat[0])).toList();
		}
		return List.of();
	}

	/** {@inheritDoc} */
	@Override
	public AnnotationParameters getParameters() {
		return parameters;
	}

	/** {@inheritDoc} */
	@Override
	public void setParameters(AnnotationParameters parameters) {
		fitToParameters(parameters);
		sessionService.saveSession();
	}

	/** Change parameters */
	private void fitToParameters(AnnotationParameters parameters) {
		this.parameters = parameters;
		assembleAttributes();

		labelLayer.setOpacity(parameters.opacity());
		labelLayer.setEnabled(parameters.label());

		markerLayer.setOpacity(parameters.opacity());
		markerLayer.setEnabled(parameters.marker());

		geographicViewService.refresh();
	}

	/**
	 * Prepare attributes to render shapes
	 */
	private void assembleAttributes() {
		for (Renderable renderable : renderables) {
			if (renderable instanceof SurfacePolygon surfacePolygon) {
				surfacePolygon.getAttributes().setDrawInterior(parameters.fill());
				surfacePolygon.getAttributes().setDrawOutline(parameters.drawOutline());

				double originalOpacity = (double) surfacePolygon.getValue("original_opacity");
				surfacePolygon.getAttributes().setInteriorOpacity(parameters.opacity() * originalOpacity);
				surfacePolygon.getAttributes().setOutlineOpacity(parameters.opacity() * originalOpacity);

			}
		}

	}

	/**
	 * Called by the session to retrieve all the parameters of this layer
	 */
	@Override
	public TypedMap describeParametersForSession() {
		return TypedMap.of(parameters.toTypedEntry());
	}

	/**
	 * Called by the session when the parameters have been restored.
	 */
	@Override
	public void setSessionParameter(TypedMap parameters) {
		parameters.get(AnnotationParameters.SESSION_KEY).ifPresent(this::fitToParameters);
	}

	@Override
	public IWWLabelLayer getLabelLayer() {
		return labelLayer;
	}

	@Override
	public Optional<IWWMarkerLayer> getMarkerLayer() {
		if (markers.isEmpty())
			return Optional.empty();
		return Optional.of(markerLayer);
	}

	private class MarkerProvider implements IMarkerFileInfo<IMarker> {

		@Override
		public String getPath() {
			return fileInfo.getPath();
		}

		@Override
		public List<Property<?>> getProperties() {
			return fileInfo.getProperties();
		}

		@Override
		public List<IMarker> getMarkers() {
			return markers;
		}

		@Override
		public void setMarkers(List<IMarker> markers) {
			// Markers are never updated
		}

		@Override
		public void save() throws GIOException {
			// Markers are never saved
		}
	}

}
