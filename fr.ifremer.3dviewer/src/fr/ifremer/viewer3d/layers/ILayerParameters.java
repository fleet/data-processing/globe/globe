package fr.ifremer.viewer3d.layers;

/**
 * @Deprecated used with old xml session file
 */
@Deprecated
public interface ILayerParameters {
	/**
	 * Provides the persistent parameters stored in session
	 */
	public LayerParameters getLayerParameters();

	/**
	 * Set the persistent parameters from in session to the layer
	 */
	public void setLayerParameters(LayerParameters arg);

}
