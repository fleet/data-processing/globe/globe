package fr.ifremer.viewer3d.layers;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.model.IPlayable;
import fr.ifremer.viewer3d.model.PlayerBean;
import fr.ifremer.viewer3d.model.Seismic3DData;
import fr.ifremer.viewer3d.render.VolatilTexturedWall;
import fr.ifremer.viewer3d.util.IFiles;
import fr.ifremer.viewer3d.util.ILazyLoading;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.view.BasicView;

/**
 * This layer shows echograms images.
 * 
 * @author C.Laurent
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public class Seismic3DLayer extends MyAbstractWallLayer implements PropertyChangeListener, ITarget, IInfos, ILazyLoading, IFiles, IDeletable, IPlayable {

	/**
	 * Textured wall used to render current image's texture.
	 */
	protected VolatilTexturedWall texturedWall = null;

	/**
	 * The water column data displayed in this layer.
	 */
	protected Seismic3DData seismicData = null;

	/**
	 * Index of last displayed frame.
	 */
	protected int lastIndex = 0;

	/**
	 * Sector for this layer location.
	 */
	protected Sector sector;

	protected transient PropertyChangeSupport pcs;

	protected Position startPos = null;

	@Override
	public Position getPosition() {
		return this.startPos;
	}

	@Override
	public void setPosition(Position startPos) {
		this.startPos = startPos;
	}

	@Override
	public Position getPosition(int index) {
		return this.seismicData.getPositions().get(index);
	}

	public int getCurrentPingIndex() {
		return this.lastIndex;
	}

	/**
	 * Default ctor.
	 */
	public Seismic3DLayer() {
		super();
	}

	/**
	 * Ctor.
	 * 
	 * @param playerdata
	 *            bean instance containing current playing state
	 * @param waterColumnData
	 *            water column data (echograms) to render
	 */
	public Seismic3DLayer(PlayerBean playerdata, Seismic3DData waterColumnData) {

		this();

		if (Viewer3D.getWwd() != null) {
			((BasicView) Viewer3D.getWwd().getView()).setDetectCollisions(false);
		}

		if (playerdata == null) {
			throw new InvalidParameterException(DicoBundle.getString("KEY_WATERCOLUMNLAYER_ERROR_MESSAGE")); //$NON-NLS-1$
		}

		setName("Unknown");

		// applicationFrame = appFrame;
		this.seismicData = waterColumnData;
		this.playerbean = playerdata;
		this.playerbean.addPropertyChangeListener(this);
		this.pcs = new PropertyChangeSupport(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		super.dispose();

		// BRL : JVM doesn't provide any unmap method see bug 4715154
		// we just remove references call garbage collector ... and pray

		if (this.imagesList != null) {
			this.imagesList.clear();
		}
		System.gc();
	}

	/**
	 * Initialize this layer : creates memory maps for input images an loads
	 * first image.
	 */
	@Override
	public void initialize() {
		if (this.seismicData != null) {
			LatLon[][] ll = seismicData.getLatlon();
			int size = ll.length;
			int width = ll[0].length;

			this.startPos = new Position(ll[0][0], 0);

			this.imagesList = new ArrayList<String>(size);

			// playable
			this.timeList = new ArrayList<Long>(size);

			File wcFile = this.seismicData.getFile();
			if (wcFile != null && wcFile.exists()) {
				File dir = wcFile.getParentFile();

				for (int i = 1; i < size - 1; i++) {
					String prefix = this.seismicData.getImgDir();
					String image = getImageFileName(prefix, i, dir);

					if (image != null) {
						File fileSource = new File(image);
						if (fileSource.exists()) {
							this.imagesList.add(image);
						}
					}

					// initialize with first available image
					if (this.texturedWall == null) {
						this.texturedWall = new VolatilTexturedWall(this, this.seismicData.getLatlon(i, 0), this.seismicData.getLatlon(i, width - 1), this.seismicData.getZ(i), 0,()-> this);
					}
				}

				// BRL 11/04/2011 pick enable should be true in order to use
				// picking for remarkable points definition.
				setPickEnabled(true);
				if (this.texturedWall == null) {
					Logging.logger().severe("No image found");
					// avoid another loading with same error data
					this.seismicData = null;
				} else {

					// Replaced by ordered renderables
					// addRenderable(texturedWall);

					// init sector
					this.sector = Sector.boundingSector(ll[0][0], ll[0][1]);
				}
			}
		}
		// if (this.seismicData != null) {
		// this.timeList.addAll(this.seismicData.getTimeList());
		//
		// // set position pour l'object 3D
		// this.setPosition(new Position(this.seismicData.getLatLonCenter(0),
		// 0));
		// refresh(1);
		// }

		this._availableColorMaps = new ArrayList<String>();

		// colormap
		listerColorMap(false);// la donnée est sur un char, elle n'est pas en
		// int16
		setColorMapOri(0);
	}

	/**
	 * Returns image file name.
	 * 
	 * @param imgDir
	 *            image directory name
	 * @param imageNumber
	 *            the image index
	 * @param dir
	 *            the image base directory
	 * @return the image file name or null if not found
	 */
	protected String getImageFileName(String imgDir, int imageNumber, File dir) {
		Formatter formatter = new Formatter();
		String fileName = null;
		if (dir != null) {
			int dirNumber = imageNumber / 100;
			fileName = formatter.format("%s/%s/dim3/%03d/%05d", dir //$NON-NLS-1$
					.getAbsolutePath(), imgDir, dirNumber, imageNumber) + DicoBundle.getString("KEY_WATERCOLUMNLAYER_FILE_EXTENSION"); //$NON-NLS-1$
		}
		formatter.close();

		return fileName;
	}

	/**
	 * Updates rendering.
	 * 
	 * @param currentIndex
	 *            the current image index (to replace)
	 * @param newIndex
	 *            the new image index
	 */
	@Override
	public void refresh(int newIndex) {
		if (this.texturedWall != null) {
			int size = this.imagesList.size();
			// ugly hack : buffer list are 0-based despite ping index are
			// 1-based
			newIndex--;
			if (newIndex >= 0 && newIndex < size && this.lastIndex != newIndex) {
				// seismicData.getLatlon(index)
				// this.texturedWall.setLocation1(this.seismicData
				// .getLatlon1(newIndex));
				// this.texturedWall.setLocation2(this.seismicData
				// .getLatlon2(newIndex));
				// this.texturedWall.setBottomElevation(this.seismicData
				// .getBottomElevation(newIndex));
				// this.texturedWall.setTopElevation(-this.seismicData
				// .getTopElevation(newIndex));

				String imageFile = this.imagesList.get(newIndex);
				this.texturedWall.updateImageFile(imageFile);

				// this.pcs.firePropertyChange("position", new Position(
				// this.seismicData.getLatLonCenter(this.lastIndex), 0f),
				// new Position(
				// this.seismicData.getLatLonCenter(newIndex), 0f));

				this.lastIndex = newIndex;
				firePropertyChange(AVKey.LAYER, null, this);
				Viewer3D.getWwd().redraw();
			}
		}
	}

	/**
	 * Updates rendering.
	 * 
	 * @param currentTime
	 *            the current Time to render
	 * @param newTime
	 *            the new Time to render
	 */
	public void refresh(Long currentTime, Long newTime) {
		if (this.timeList.contains(newTime)) {
			int newIndex;
			newIndex = this.timeList.indexOf(newTime);
			refresh(newIndex);
		}
	}

	/**
	 * Time interval between two frames (ms).
	 * 
	 * @return time interval between two frames (ms).
	 */
	public int getDelay() {
		return this.playerbean.getDelay();
	}

	/**
	 * Time interval between two frames (ms).
	 * 
	 * @param delay
	 *            time interval between two frames (ms).
	 */
	public void setDelay(int delay) {
		this.playerbean.setDelay(delay);
		Logging.logger().info(DicoBundle.getString("KEY_WATERCOLUMNLAYER_DELAY_MESSAGE") + " = " + delay); //$NON-NLS-1$
	}

	/**
	 * Returns player bean used by this layer.
	 * 
	 * @return player bean used by this layer
	 */
	@Override
	public PlayerBean getPlayerBean() {
		return this.playerbean;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String name = event.getPropertyName();
		if (PlayerBean.PROPERTY_PLAY_NUMBER.equals(name)) {
			// int oldIdx = (Integer) event.getOldValue();
			int newIdx = (Integer) event.getNewValue();
			refresh(newIdx);
		}

		super.propertyChange(event);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sector getSector() {
		return this.sector;
	}

	@Override
	protected void doRender(DrawContext dc) {
		if (this.texturedWall == null) {
			initialize();
		}

		// refresh(1);

		if (dc != null && this.texturedWall != null) {
			// Sector current =
			// Sector.boundingSector(texturedWall.getLocation1(),
			// texturedWall.getLocation2());
			// Extent extent = Sector.computeBoundingCylinder(dc.getGlobe(),
			// dc.getVerticalExaggeration(), current);
			// if
			// (dc.getView().getFrustumInModelCoordinates().intersects(extent))
			// {
			//setValue(SSVAVKey.CURRENTLY_VISIBLE, true);
			this.texturedWall.updateEyeDistance(dc);
			dc.addOrderedRenderable(this.texturedWall);
			// } else {
			// setValue(SSVAVKey.CURRENTLY_VISIBLE, false);
			// }
		}
	}

	@Override
	protected void doPick(DrawContext dc, Point point) {
		if (dc != null && this.texturedWall != null) {
			this.texturedWall.updateEyeDistance(dc);
			dc.addOrderedRenderable(this.texturedWall);
		}
	}

	/**
	 * Returns input data.
	 */
	public Seismic3DData getData() {
		return this.seismicData;
	}

	@Override
	public File getInputFile() {
		if (this.seismicData != null) {
			return this.seismicData.getFile();
		}
		return null;
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public List<Property<?>> getProperties() {
		return this.seismicData.getProperties();
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOpacity(double opacity) {
		if (this.texturedWall != null) {
			this.texturedWall.setOpacity(opacity);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<File, Boolean> getFiles() {
		if (this.seismicData != null) {
			File f = this.seismicData.getFile();
			if (f != null) {
				Map<File, Boolean> files = new HashMap<File, Boolean>(2);
				files.put(f, true);

				// images directory
				String imgDir = this.seismicData.getImgDir();
				if (imgDir != null && !imgDir.isEmpty()) {
					String dir = f.getParent() + "/" + imgDir;
					if (dir != null) {
						files.put(new File(dir), true);
					}
				}
				return files;
			}
		}
		return null;
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	@Override
	public Long getEndTime() {
		return this.timeList.get(this.timeList.size() - 1);
	}

	@Override
	public Long getStartTime() {
		return this.timeList.get(0);
	}

	public Long getTime(int index) {
		return this.timeList.get(index);
	}

	@Override
	public String getPlayableType() {
		return this.playableType;
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public double distance(double X, double Y) {
		double distance = HIGH_DISTANCE;
		return distance;
	}

}
