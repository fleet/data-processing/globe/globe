package fr.ifremer.viewer3d.layers;

import fr.ifremer.viewer3d.layers.LayerParameters.AbstractLayerParameters;

public class VisibleLayerParameter extends AbstractLayerParameters {

	public void load(VisibleLayerParameter other) {
		visibility = other.visibility;
	}
}
