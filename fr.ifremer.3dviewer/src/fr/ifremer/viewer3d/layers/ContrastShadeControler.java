package fr.ifremer.viewer3d.layers;

import java.beans.PropertyChangeListener;
import java.util.EventObject;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.event.EventListenerSupport;

import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.globe.ui.service.session.impl.SessionController;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.MyAbstractLayerParameter.ContrastType;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import gov.nasa.worldwind.Disposable;

/**
 * Controler for {@link ContrastShadeComposite}.
 *
 * @author apside
 */
public class ContrastShadeControler implements Disposable {
	/***
	 * used to update contrast 0.5% in IHM
	 */
	public static String Contrast_05 = "Contrast 0.5%";
	/***
	 * used to update contrast 1% in IHM
	 */
	public static String Contrast_1 = "Contrast 1%";
	/***
	 * used to update contrast minmax in IHM
	 */
	public static String Contrast_MinMax = "Contrast min max";

	/**
	 * Event ttriggered when contrast parameters of all layers must re-synchronize on the current master layer.
	 */
	public static class SyncAllContrastsEvent extends EventObject implements BaseEvent {

		private static final long serialVersionUID = -6646434077499042900L;

		public SyncAllContrastsEvent(ContrastShadeControler source) {
			super(source);
		}
	}

	/**
	 * Event ttriggered when transparency thresholds of all layers must re-synchronize on the current master layer.
	 */
	public static class SyncAllThresholdsEvent extends EventObject implements BaseEvent {

		private static final long serialVersionUID = 4213795725740688439L;

		public SyncAllThresholdsEvent(ContrastShadeControler source) {
			super(source);
		}
	}

	/**
	 * A listener with only one {@link IContrastShadeUpdateListener#update() update} callback.
	 */
	public interface IContrastShadeUpdateListener {
		/**
		 * Callback notified when the underlying {@link ContrastShadeModel} changes.
		 */
		void update();
	}

	/** Retain a reference on all controlers in order to synchronize them all together. */
	private List<ContrastShadeControler> allControlers = new CopyOnWriteArrayList<>();

	private static boolean isAllContrastSynchronized;

	private static boolean isAllThresholdSynchronized;

	/**
	 * Adds the controler into the global list of controlers and then updates its model with global parameters. Global
	 * parameters are : $isAllContrastSynchronized and $isAllThresholdSynchronized. These variables are static but each
	 * ShaderElevationLayer has a copy of this global variable so that we can benefit from the data session saving
	 * mechanism (see {@link SessionController}). So we must also reinitialize the global variables' values with the
	 * very value of each layer, by consolidating them at creation time. This is what this method is intended to.
	 *
	 * @param controler The controler to register for global variables consolidation.
	 */
	protected final void registerControler(ContrastShadeControler controler) {

		synchronized (allControlers) {
			if (!allControlers.contains(controler)) {

				allControlers.add(controler);

				isAllContrastSynchronized = isAllContrastSynchronized
						|| controler.getModel().isAllContrastsSynchronized();
				isAllThresholdSynchronized = isAllThresholdSynchronized
						|| controler.getModel().isAllThresholdSynchronized();

				for (ContrastShadeControler c : allControlers) {
					c.getModel().setAllContrastsSynchronized(isAllContrastSynchronized);
					c.getModel().setAllThresholdSynchronized(isAllThresholdSynchronized);
				}
			}
		}
	}

	// -----------------------------------------------------------------------------------------

	/** Model this controler is synchronized on. */
	private ContrastShadeModel model;
	private boolean fireModelChanged = true;

	/** Listeners to alert for model changes. */
	private EventListenerSupport<IContrastShadeUpdateListener> updateListeners;

	/** Listener listening to the underlying layer changes. */
	private PropertyChangeListener layerListener;

	/**
	 * Constructor.
	 *
	 * @param model The model to control.
	 */
	public ContrastShadeControler(ShaderElevationLayer layer) {
		model = new ContrastShadeModel(layer);
		initialize(layer);
	}

	private void initialize(ShaderElevationLayer layer) {

		updateListeners = new EventListenerSupport<>(IContrastShadeUpdateListener.class, getClass().getClassLoader());

		layerListener = evt -> {
			if (evt.getPropertyName().equalsIgnoreCase(MyAbstractLayer.PROPERTY_OFFSET)) {
				refreshMinMaxContrast();
			}
		};

		registerControler(this);

		layer.addPropertyChangeListener(layerListener);
	}

	@Override
	public void dispose() {
		model.getCurrentLayer().removePropertyChangeListener(layerListener);
		allControlers.remove(this);
	}

	public void firePropertyChange(String propertyName, double oldValue, double newValue) {
		if (fireModelChanged) {
			model.getCurrentLayer().firePropertyChange(propertyName, oldValue, newValue);
		}
	}

	public void addUpdateListener(IContrastShadeUpdateListener listener) {
		updateListeners.addListener(listener);
	}

	public void removeUpdateListener(IContrastShadeUpdateListener listener) {
		updateListeners.removeListener(listener);
	}

	public ContrastShadeModel getModel() {
		return model;
	}

	/**
	 * Update the min/max contrast of the underlying layer when its offset changes.
	 */
	public void refreshMinMaxContrast() {
		if (model.isLayerValueWithOffset()) {
			// Change min/max contrast
			boolean isCustom = model.getContrastType() == ContrastType.CONTRAST_CUSTOM;
			boolean isMinMax = model.getContrastType() == ContrastType.CONTRAST_MIN_MAX;
			if (isCustom || isMinMax) {
				double minContrast = model.absoluteValue2LayerValue(model.getMinTextureValue());
				double maxContrast = model.absoluteValue2LayerValue(model.getMaxTextureValue());
				model.setMinContrast(minContrast);
				model.setMaxContrast(maxContrast);
			}
		}
	}

	//@Handler
	private void onSynchronizeAllContrastsEvent(SyncAllContrastsEvent evt) {
		ContrastShadeControler sourceOfEvent = (ContrastShadeControler) evt.getSource();
		ContrastShadeModel masterModel = sourceOfEvent.getModel();

		isAllContrastSynchronized = masterModel.isAllContrastsSynchronized();
		model.setAllContrastsSynchronized(isAllContrastSynchronized);

		if (evt.getSource() != this && isAllContrastSynchronized) {

			if (model.getCurrentLayer().getType().equalsIgnoreCase(masterModel.getCurrentLayer().getType())) {

				// In slave mode, if the master is in "min/max" mode, we switch to "custom" mode
				if (masterModel.getContrastType() == ContrastType.CONTRAST_MIN_MAX) {
					model.setContrastType(ContrastType.CONTRAST_CUSTOM);
				}

				// contrast
				model.setUseContrast(masterModel.isUseContrast());
				model.setMinTextureValue(masterModel.getMinTextureValue());
				model.setMaxTextureValue(masterModel.getMaxTextureValue());
				if (!model.isLayerValueWithOffset()) {
					model.setMinContrast(masterModel.getMinContrast());
					model.setMaxContrast(masterModel.getMaxContrast());
				}

				// colormap
				model.setUseColorMap(masterModel.isUseColorMap());
				model.setColormap(masterModel.getColormap());
				model.setInverseColorMap(masterModel.isInverseColorMap());

				// shade
				model.setUseOmbrage(masterModel.isUseOmbrage());
				model.setAzimuth(masterModel.getAzimuth());
				model.setZenith(masterModel.getZenith());
				model.setOmbrageExaggeration(masterModel.getOmbrageExaggeration());
				model.setUseGradient(masterModel.isUseGradient());
				model.setUseOmbrageLogarithmique(masterModel.isUseOmbrageLogarithmique());
			}
		}

		refreshMinMaxContrast();

		// Notify observers
		updateListeners.fire().update();
	}

	//@Handler
	private void onSynchronizeAllThresholdsEvent(SyncAllThresholdsEvent evt) {

		ContrastShadeControler sourceOfEvent = (ContrastShadeControler) evt.getSource();
		ContrastShadeModel masterModel = sourceOfEvent.getModel();

		isAllThresholdSynchronized = masterModel.isAllThresholdSynchronized();
		model.setAllThresholdSynchronized(isAllThresholdSynchronized);

		if (evt.getSource() != this && isAllThresholdSynchronized) {
			if (model.getCurrentLayer().getType().equalsIgnoreCase(masterModel.getCurrentLayer().getType())) {

				// update min max transparency
				model.setMinTransparency(masterModel.getMinTransparency());
				model.setMaxTransparency(masterModel.getMaxTransparency());

			}
		}

		// Notify observers
		updateListeners.fire().update();
	}

	/**
	 * Compute histogram but doesn't update min/max contrast values.
	 */
	public void computeHistogram() {
		ContrastShadeHistogram.autoComputeHistogram(model.getCurrentLayer());
	}

	/**
	 * Compute histogram and then update the min/max contrast values.
	 */
	public void computeContrast() {
		ContrastShadeHistogram.setTypeHistogram(model.getCurrentLayer().getType());

		if (!ContrastShadeHistogram.isCreateHisto()) {
			ContrastShadeHistogram.preComputeHisto();
		}

		// call redraw to add tile in tileList to compute histogramm
		Viewer3D.getWwd().redrawNow();

		if (ContrastShadeHistogram.isCreateHisto()) {
			ContrastShadeHistogram.computeHisto();
		}

		if (model.isLayerValueWithOffset()) {
			model.getCurrentLayer().setMinTextureValue(ContrastShadeHistogram.getStaticRehaussementMin());
			model.getCurrentLayer().setMaxTextureValue(ContrastShadeHistogram.getStaticRehaussementMax());
		}
		model.getCurrentLayer().setMinContrast(ContrastShadeHistogram.getStaticRehaussementMin());
		model.getCurrentLayer().setMaxContrast(ContrastShadeHistogram.getStaticRehaussementMax());

		Viewer3D.getWwd().redraw();
	}

	/***
	 * contrast 1%
	 */
	public void contrast1PerCent() {
		ContrastShadeHistogram.setfacteurRehaussement(0.01); // rehaussement a 1%
		model.setContrastType(ContrastType.CONTRAST_1_PERCENT);
		computeContrast();

		double oldValue = ContrastShadeHistogram.getfacteurRehaussement();
		ContrastShadeHistogram.setfacteurRehaussement(0.0);
		firePropertyChange(Contrast_1, oldValue, 0.0);
	}

	/**
	 * contrast 0.5%
	 */
	public void contrast05PerCent() {
		ContrastShadeHistogram.setfacteurRehaussement(0.005); // rehaussement a 0.5%
		model.setContrastType(ContrastType.CONTRAST_05_PERCENT);
		computeContrast();

		double oldValue = ContrastShadeHistogram.getfacteurRehaussement();
		ContrastShadeHistogram.setfacteurRehaussement(0.0);
		firePropertyChange(Contrast_05, oldValue, 0.0);
	}

	/***
	 * contrast min max
	 */
	public void contrastMinMax() {
		ContrastShadeHistogram.setfacteurRehaussement(-1); // pas de rehaussement
		model.setContrastType(ContrastType.CONTRAST_MIN_MAX);
		computeContrast();

		double oldValue = ContrastShadeHistogram.getfacteurRehaussement();
		ContrastShadeHistogram.setfacteurRehaussement(0.0);
		firePropertyChange(Contrast_MinMax, oldValue, 0.0);
	}

	@Override
	public int hashCode() {
		return model.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ContrastShadeControler) {
			return model.equals(((ContrastShadeControler) obj).getModel());
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getClass().getSimpleName()).append("[layer=")
				.append(model.getCurrentLayer().getName()).append("]");
		return sb.toString();
	}

	/**
	 * Getter of fireModerChanged
	 */
	public boolean isFireModelChanged() {
		return fireModelChanged;
	}

	/**
	 * Setter of fireModerChanged
	 */
	public void setFireModelChanged(boolean fireModerChanged) {
		fireModelChanged = fireModerChanged;
	}

}
