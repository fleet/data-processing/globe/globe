package fr.ifremer.viewer3d.layers;

import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.layers.LayerParameters.AbstractLayerParameters;
import fr.ifremer.viewer3d.preference.DtmContrast;

public class MyAbstractLayerParameter extends AbstractLayerParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<MyAbstractLayerParameter> SESSION_KEY = new TypedKey<>("My_abstract_layer_parameter");

	public enum ContrastType {

		UNDEFINED(-1), CONTRAST_MIN_MAX(0), CONTRAST_05_PERCENT(1), CONTRAST_1_PERCENT(2), CONTRAST_CUSTOM(3);

		private int level;

		private ContrastType(int level) {
			this.level = level;
		}

		public int getLevel() {
			return level;
		}
	}

	/** Interpolation type. */
	public static final String INTERPOLATION = "interpolation";
	/** Linear interpolation. */
	public static final String INTERPOLATION_LINEAR = "linear";
	/** Nearest neighbour interpolation. */
	public static final String INTERPOLATION_NEAREST = "nearest";

	// contrast
	protected boolean useContrast;
	protected double minTextureContrast;
	protected double maxTextureContrast;
	protected double minTransparency;
	protected double maxTransparency;
	/***
	 * level of contrast to retrieve after load session 0 : min max 1 : 0.5% 2 : 1% 3 : custom
	 */
	protected int contrastLevel = ContrastType.UNDEFINED.getLevel();
	protected ContrastType contrastType;

	// colormap
	protected boolean useColorMap;
	protected String colormap;
	protected String colormapori;
	protected boolean inverseColorMap;

	// highlight
	protected boolean isHighlighted;

	// ombrage
	protected boolean useOmbrage;
	protected boolean useGradient;
	protected double azimuth;
	protected double zenith;
	protected double ombrageExaggeration = 1.0; // (0 in log10)

	// texture
	protected double minTextureValue;
	protected double maxTextureValue;
	protected double minRecommandeValue;
	protected double maxRecommandeValue;

	protected boolean useOmbrageLogarithmique = false;

	/** Offset used to modify data displaying for comparison between layers */
	protected double offset = 0.0;
	/** Indicates that offset is computed dynamicaly or not */
	protected boolean offsetAuto = false;

	/***
	 * scale used to modify data displaying for comparison between layers
	 */
	private double scaleFactor = 1.0;

	/** Flag indicating if all layers' contrast properties are synchronized together. */
	private boolean isAllContrastsSynchronized;

	/** Flag indicating if all layers' transparency thresholds are synchronized together. */
	private boolean isAllThresholdSynchronized;

	/**
	 * Default constructor.
	 */
	public MyAbstractLayerParameter() {
		// select defaut contrast
		DtmContrast contrast = Activator.getPluginParameters().getDtmDefaultContrast();
		switch (contrast) {
		case MINMAX:
			contrastType = ContrastType.CONTRAST_MIN_MAX;
			break;
		case REHAUSS05:
			contrastType = ContrastType.CONTRAST_05_PERCENT;
			break;
		case REHAUSS1:
			contrastType = ContrastType.CONTRAST_1_PERCENT;
			break;
		default:
			contrastType = ContrastType.UNDEFINED;
			break;
		}
		contrastLevel = contrastType.getLevel();
	}

	public MyAbstractLayerParameter(MyAbstractLayerParameter parent) {
		this();
		load(parent);
	}

	/**
	 * Copy the parent's parameters into this object.
	 *
	 * @param parent The parent's parameters to copy.
	 */
	public void load(MyAbstractLayerParameter parent) {

		visibility = parent.visibility;

		// contrast
		useContrast = parent.useContrast;
		minTextureContrast = parent.minTextureContrast;
		maxTextureContrast = parent.maxTextureContrast;
		minTransparency = parent.minTransparency;
		maxTransparency = parent.maxTransparency;
		contrastLevel = parent.contrastLevel;
		contrastType = parent.contrastType;
		isAllContrastsSynchronized = parent.isAllContrastsSynchronized;
		isAllThresholdSynchronized = parent.isAllThresholdSynchronized;

		// colormap
		useColorMap = parent.useColorMap;
		inverseColorMap = parent.inverseColorMap;
		colormapori = parent.colormapori;
		colormap = parent.colormap;

		// ombrage
		useOmbrage = parent.useOmbrage;
		useGradient = parent.useGradient;
		azimuth = parent.azimuth;
		zenith = parent.zenith;
		ombrageExaggeration = parent.ombrageExaggeration;

		// texture value
		minTextureValue = parent.minTextureValue;
		maxTextureValue = parent.maxTextureValue;
		minRecommandeValue = parent.minRecommandeValue;
		maxRecommandeValue = parent.maxRecommandeValue;
		scaleFactor = parent.scaleFactor;

		// vertical offset
		offset = parent.offset;
	}

	/**
	 * @return the useContrast
	 */
	public boolean isUseContrast() {
		return useContrast;
	}

	/**
	 * @param useContrast the useContrast to set
	 */
	public void setUseContrast(boolean useContrast) {
		pcs.firePropertyChange("useContrast", this.useContrast, this.useContrast = useContrast);
	}

	/**
	 * @return the minTextureContrast
	 */
	public double getMinTextureContrast() {
		return minTextureContrast;
	}

	/**
	 * @param minTextureContrast the minTextureContrast to set
	 */
	public void setMinTextureContrast(double minTextureContrast) {
		pcs.firePropertyChange("minTextureContrast", this.minTextureContrast,
				this.minTextureContrast = minTextureContrast);
	}

	/**
	 * @return the maxTextureContrast
	 */
	public double getMaxTextureContrast() {
		return maxTextureContrast;
	}

	/**
	 * @param maxTextureContrast the maxTextureContrast to set
	 */
	public void setMaxTextureContrast(double maxTextureContrast) {
		pcs.firePropertyChange("maxTextureContrast", this.maxTextureContrast,
				this.maxTextureContrast = maxTextureContrast);
	}

	/**
	 * @return the minTransparency
	 */
	public double getMinTransparency() {
		return minTransparency;
	}

	/**
	 * @param minTransparency the minTransparency to set
	 */
	public void setMinTransparency(double minTransparency) {
		pcs.firePropertyChange("minTransparency", this.minTransparency, this.minTransparency = minTransparency);
	}

	/**
	 * @return the maxTransparency
	 */
	public double getMaxTransparency() {
		return maxTransparency;
	}

	/**
	 * @param maxTransparency the maxTransparency to set
	 */
	public void setMaxTransparency(double maxTransparency) {
		pcs.firePropertyChange("maxTransparency", this.maxTransparency, this.maxTransparency = maxTransparency);
	}

	/**
	 * @return the contrastLevel
	 */
	public int getContrastLevel() {
		return contrastLevel;
	}

	/**
	 * @param contrastLevel the contrastLevel to set
	 */
	public void setContrastLevel(int contrastLevel) {
		pcs.firePropertyChange("contrastLevel", this.contrastLevel, this.contrastLevel = contrastLevel);
	}

	/**
	 * @return the useColorMap
	 */
	public boolean isUseColorMap() {
		return useColorMap;
	}

	/**
	 * @param useColorMap the useColorMap to set
	 */
	public void setUseColorMap(boolean useColorMap) {
		pcs.firePropertyChange("useColorMap", this.useColorMap, this.useColorMap = useColorMap);
	}

	/**
	 * @return the colormap
	 */
	public String getColormap() {
		return colormap;
	}

	/**
	 * @param colormap the colormap to set
	 */
	public void setColormap(String colormap) {
		pcs.firePropertyChange("colormap", this.colormap, this.colormap = colormap);
	}

	/**
	 * @return the inverseColorMap
	 */
	public boolean isInverseColorMap() {
		return inverseColorMap;
	}

	/**
	 * @param inverseColorMap the inverseColorMap to set
	 */
	public void setInverseColorMap(boolean inverseColorMap) {
		pcs.firePropertyChange("inverseColorMap", this.inverseColorMap, this.inverseColorMap = inverseColorMap);
	}

	/**
	 * @return the useOmbrage
	 */
	public boolean isUseOmbrage() {
		return useOmbrage;
	}

	/**
	 * @param useOmbrage the useOmbrage to set
	 */
	public void setUseOmbrage(boolean useOmbrage) {
		pcs.firePropertyChange("useOmbrage", this.useOmbrage, this.useOmbrage = useOmbrage);
	}

	/**
	 * @return the useGradient
	 */
	public boolean isUseGradient() {
		return useGradient;
	}

	/**
	 * @param useGradient the useGradient to set
	 */
	public void setUseGradient(boolean useGradient) {
		pcs.firePropertyChange("useGradient", this.useGradient, this.useGradient = useGradient);
	}

	/**
	 * @return the azimuth
	 */
	public double getAzimuth() {
		return azimuth;
	}

	/**
	 * @param azimuth the azimuth to set
	 */
	public void setAzimuth(double azimuth) {
		pcs.firePropertyChange("Azimuth", this.azimuth, this.azimuth = azimuth);
	}

	/**
	 * @return the zenith
	 */
	public double getZenith() {
		return zenith;
	}

	/**
	 * @param zenith the zenith to set
	 */
	public void setZenith(double zenith) {
		this.zenith = zenith;
	}

	/**
	 * @return the ombrageExaggeration
	 */
	public double getOmbrageExaggeration() {
		return ombrageExaggeration;
	}

	/**
	 * @param ombrageExaggeration the ombrageExaggeration to set
	 */
	public void setOmbrageExaggeration(double ombrageExaggeration) {
		pcs.firePropertyChange("ombrageExaggeration", this.ombrageExaggeration,
				this.ombrageExaggeration = ombrageExaggeration);
	}

	/**
	 * @return the minTextureValue
	 */
	public double getMinTextureValue() {
		return minTextureValue;
	}

	/**
	 * @param minTextureValue the minTextureValue to set
	 */
	public void setMinTextureValue(double minTextureValue) {
		pcs.firePropertyChange("minTextureValue", this.minTextureValue, this.minTextureValue = minTextureValue);
	}

	/**
	 * @return the maxTextureValue
	 */
	public double getMaxTextureValue() {
		return maxTextureValue;
	}

	/**
	 * @param maxTextureValue the maxTextureValue to set
	 */
	public void setMaxTextureValue(double maxTextureValue) {
		pcs.firePropertyChange("maxTextureValue", this.maxTextureValue, this.maxTextureValue = maxTextureValue);
	}

	/**
	 * @return the minRecommandeValue
	 */
	public double getMinRecommandeValue() {
		return minRecommandeValue;
	}

	/**
	 * @param minRecommandeValue the minRecommandeValue to set
	 */
	public void setMinRecommandeValue(double minRecommandeValue) {
		pcs.firePropertyChange("minRecommandeValue", this.minRecommandeValue,
				this.minRecommandeValue = minRecommandeValue);
	}

	/**
	 * @return the maxRecommandeValue
	 */
	public double getMaxRecommandeValue() {
		return maxRecommandeValue;
	}

	/**
	 * @param maxRecommandeValue the maxRecommandeValue to set
	 */
	public void setMaxRecommandeValue(double maxRecommandeValue) {
		pcs.firePropertyChange("maxRecommandeValue", this.maxRecommandeValue,
				this.maxRecommandeValue = maxRecommandeValue);
	}

	/**
	 * @return the useOmbrageLogarithmique
	 */
	public boolean isUseOmbrageLogarithmique() {
		return useOmbrageLogarithmique;
	}

	/**
	 * @param useOmbrageLogarithmique the useOmbrageLogarithmique to set
	 */
	public void setUseOmbrageLogarithmique(boolean useOmbrageLogarithmique) {
		pcs.firePropertyChange("useOmbrageLogarithmique", this.useOmbrageLogarithmique,
				this.useOmbrageLogarithmique = useOmbrageLogarithmique);
	}

	/**
	 * @return the offset
	 */
	public double getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(double offset) {
		pcs.firePropertyChange("offset", this.offset, this.offset = offset);
	}

	/**
	 * @return the scaleFactor
	 */
	public double getScaleFactor() {
		return scaleFactor;
	}

	/**
	 * @param scaleFactor the scaleFactor to set
	 */
	public void setScaleFactor(double scaleFactor) {
		pcs.firePropertyChange("scaleFactor", this.scaleFactor, this.scaleFactor = scaleFactor);
	}

	/**
	 * @return the colormapori
	 */
	public String getColormapori() {
		return colormapori;
	}

	/**
	 * @param colormapori the colormapori to set
	 */
	public void setColormapori(String colormapori) {
		pcs.firePropertyChange("colormapori", this.colormapori, this.colormapori = colormapori);
	}

	public void setAllContrastSynchronized(boolean isAllContrastsSynchronized) {
		pcs.firePropertyChange("allContrastSynchronized", this.isAllContrastsSynchronized,
				this.isAllContrastsSynchronized = isAllContrastsSynchronized);
	}

	public boolean isAllContrastSynchronized() {
		return isAllContrastsSynchronized;
	}

	public boolean isAllThreasholdSynchronized() {
		return isAllThresholdSynchronized;
	}

	public void setAllThresholdSynchronized(boolean isAllThresholdSynchronized) {
		pcs.firePropertyChange("allThresholdSynchronized", this.isAllThresholdSynchronized,
				this.isAllThresholdSynchronized = isAllThresholdSynchronized);
	}

	public ContrastType getContrastType() {
		return contrastType;
	}

	public void setContrastType(ContrastType contrastType) {
		pcs.firePropertyChange("contrastType", this.contrastType, this.contrastType = contrastType);
	}

	public boolean isHighlighted() {
		return isHighlighted;
	}

	public void setHighlighted(boolean highlight) {
		isHighlighted = highlight;
	}

	/**
	 * Getter of offsetAuto
	 */
	public boolean isOffsetAuto() {
		return offsetAuto;
	}

	/**
	 * Setter of offsetAuto
	 */
	public void setOffsetAuto(boolean offsetAuto) {
		pcs.firePropertyChange("offsetAuto", this.offsetAuto, this.offsetAuto = offsetAuto);
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<MyAbstractLayerParameter> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}
}
