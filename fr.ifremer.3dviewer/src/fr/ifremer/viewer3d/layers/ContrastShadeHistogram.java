package fr.ifremer.viewer3d.layers;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTextureTile;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.cache.FileStore;

/**
 * Class responsible for inter-layers histograms management. 
 * 
 * @author Apside
 */
public class ContrastShadeHistogram {

	private static final int HISTOGRAM_SIZE = 256;
	private static ContrastShadeHistogram instance = new ContrastShadeHistogram();

	private List<MyTextureTile> listTile = new ArrayList<MyTextureTile>();
	private int[] histogram;
	private boolean isCreateHisto;
	private String typeHisto = new String();
	private double facteurRehaussement = -1; // -1==> pas de rehaussement
	private double staticRehaussementMin;
	private double staticRehaussementMax;
	private double staticMin;
	private double staticMax;

	private ContrastShadeHistogram(){
	}

	public static void setCreateHisto(boolean isCreateHisto) {
		instance.isCreateHisto = isCreateHisto;
	}

	public static boolean isCreateHisto() {
		return instance.isCreateHisto;
	}

	public static void preComputeHisto() {
		instance.listTile.clear();
		instance.histogram = null;
		instance.isCreateHisto = true;
	}

	public static void autoComputeHistogram(ShaderElevationLayer layer) {

		// pas de rehaussement
		instance.facteurRehaussement = -1;
		instance.typeHisto = layer.getType();

		if (!isCreateHisto()) {
			preComputeHisto();
		}

		// call redraw to add tile in tileList to compute histogramm
		Viewer3D.getWwd().redrawNow();

		if (isCreateHisto()) {
			computeHisto();
		}
	}

	public static void computeHisto() {
		setCreateHisto(false);

		double min = Integer.MAX_VALUE;
		double max = -Integer.MAX_VALUE;
		double taille_totale = 0;
		// Dossier temporaire
		boolean checkClassPath = false;
		FileStore fileStore = WorldWind.getDataFileStore();

		// MIN / MAX / TOTAL
		for (MyTextureTile tile : instance.listTile) {
			if (tile.getPath() != null && tile.getPath().endsWith(".png")) {

				BufferedImage img = null;

				URL url = fileStore.findFile(tile.getPath(), checkClassPath);
				if (url != null) {
					Image src = new ImageIcon(url).getImage();
					img = new BufferedImage(src.getWidth(null), src.getHeight(null), BufferedImage.TYPE_INT_ARGB);
					img.getGraphics().drawImage(src, 0, 0, null);

					ShaderElevationLayer layerTile = ((ShaderElevationLayer) tile.getLayer());

					for (int width = 0; width < src.getWidth(null); width++) {
						for (int height = 0; height < src.getWidth(null); height++) {
							int a = img.getRaster().getSample(width, height, 3); // transparence

							if (a == 255) {
								int r = img.getRaster().getSample(width, height, 0);
								int g = img.getRaster().getSample(width, height, 1);
								int b = img.getRaster().getSample(width, height, 2);
								double val = r * 256 * 256 + g * 256 + b;

								double vald = (val) / (256.0 * 256.0 * 256.0) * (layerTile.getValGlobaleMax() - layerTile.getValGlobaleMin()) + layerTile.getValGlobaleMin();
								if (vald < min) {
									min = vald;
								}
								if (vald > max) {
									max = vald;
								}
								taille_totale++;
							}
						}
					}
				}
			}
		}

		instance.staticMin = min;
		instance.staticMax = max;

		// HISTOGRAM
		instance.histogram = new int[HISTOGRAM_SIZE];
		for (MyTextureTile tile : instance.listTile) {
			if (tile.getPath() != null && tile.getPath().endsWith(".png")) {

				BufferedImage img = null;

				URL url = fileStore.findFile(tile.getPath(), checkClassPath);
				if (url != null) {
					Image src = new ImageIcon(url).getImage();
					img = new BufferedImage(src.getWidth(null), src.getHeight(null), BufferedImage.TYPE_INT_ARGB);
					img.getGraphics().drawImage(src, 0, 0, null);

					ShaderElevationLayer layerTile = ((ShaderElevationLayer) tile.getLayer());

					for (int width = 0; width < src.getWidth(null); width++) {
						for (int height = 0; height < src.getWidth(null); height++) {
							int a = img.getRaster().getSample(width, height, 3);
							if (a == 255) {
								int r = img.getRaster().getSample(width, height, 0);
								int g = img.getRaster().getSample(width, height, 1);
								int b = img.getRaster().getSample(width, height, 2);
								double val = r * 256 * 256 + g * 256 + b;

								double vald = (val) / (256.0 * 256.0 * 256.0) * (layerTile.getValGlobaleMax() - layerTile.getValGlobaleMin()) + layerTile.getValGlobaleMin();
								int result = (int) (((vald) - (min)) / ((max - min)) * 255.0);
								if (result >= 0 && result < HISTOGRAM_SIZE) {
									instance.histogram[result]++;
								}
							}
						}
					}
				}
			}
		}

		// REHAUSSEMENT
		if (instance.facteurRehaussement != -1) {
			// calcul histogramme cumulé
			int[] histogram_cumule = new int[HISTOGRAM_SIZE];
			for (int i = 0; i < HISTOGRAM_SIZE; i++) {
				for (int j = 0; j < i + 1; j++) {
					histogram_cumule[i] += instance.histogram[j];
				}
			}

			double borne_min = taille_totale * instance.facteurRehaussement / 2;
			double borne_max = taille_totale - taille_totale * instance.facteurRehaussement / 2;

			for (int i = 0; i < HISTOGRAM_SIZE; i++) {
				if (borne_min < histogram_cumule[i]) {
					borne_min = (i) / (255.0) * (max - min) + (min);
					break;
				}
			}
			for (int i = 0; i < HISTOGRAM_SIZE; i++) {
				if (borne_max < histogram_cumule[i]) {
					borne_max = (i) / (255.0) * (max - min) + (min);
					break;
				}
			}

			if (borne_min < min) {
				borne_min = min;
			}
			if (borne_max > max) {
				borne_max = max;
			}

			min = borne_min;
			max = borne_max;
			instance.staticRehaussementMin = borne_min;
			instance.staticRehaussementMax = borne_max;
		} else {
			instance.staticRehaussementMin = ShaderElevationLayer.getCurrent().getValMin();
			instance.staticRehaussementMax = ShaderElevationLayer.getCurrent().getValMax();
		}
		instance.listTile.clear();
	}

	public static void addTile(MyTextureTile tile) {
		instance.listTile.add(tile);
	}

	public static void setTypeHistogram(String type) {
		instance.typeHisto = type;
	}

	public static String getTypeHistogram() {
		return instance.typeHisto;
	}

	public static double getStaticRehaussementMin() {
		return instance.staticRehaussementMin;
	}

	public static void setStaticRehaussementMin(double staticRehaussementMin) {
		instance.staticRehaussementMin = staticRehaussementMin;
	}

	public static double getStaticRehaussementMax() {
		return instance.staticRehaussementMax;
	}

	public static void setStaticRehaussementMax(double staticRehaussementMax) {
		instance.staticRehaussementMax = staticRehaussementMax;
	}

	public static double getStaticMin() {
		return instance.staticMin;
	}

	public static void setStaticMin(double staticMin) {
		instance.staticMin = staticMin;
	}

	public static double getStaticMax() {
		return instance.staticMax;
	}

	public static void setStaticMax(double staticMax) {
		instance.staticMax = staticMax;
	}

	public static int[] getHistogram() {
		return instance.histogram;
	}

	public static void setfacteurRehaussement(double d) {
		instance.facteurRehaussement = d;
	}

	public static double getfacteurRehaussement() {
		return instance.facteurRehaussement;
	}
}
