/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.layers;

import java.util.Optional;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import gov.nasa.worldwind.layers.Layer;

/**
 * Factory of parameter composite for layers with a file "WritableBoolean.fillFlag" property
 */
@Component(name = "globe_viewer3d_parameters_composite_factory_fill_polygon", service = IParametersViewCompositeFactory.class)
public class FillPolygonCompositeFactory implements IParametersViewCompositeFactory {

	/**
	 * Create a parameter composite only for layers with a file "WritableBoolean.fillFlag" property
	 */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		WritableBoolean fillFlag = selection instanceof Layer
				? (WritableBoolean) ((Layer) selection).getValue("WritableBoolean.fillFlag")
				: null;
		return fillFlag != null ? Optional.of(new FillPolygonComposite(parent, fillFlag)) : Optional.empty();
	}

	/**
	 * Composite to edit fillFlag parameter
	 */
	static class FillPolygonComposite extends Composite {

		/** Binding */
		protected DataBindingContext bindingContext = new DataBindingContext();

		/**
		 * Constructor
		 */
		public FillPolygonComposite(Composite parent, WritableBoolean fillFlag) {
			super(parent, SWT.NONE);
			setLayout(new FillLayout());
			GridDataFactory.fillDefaults().grab(true, false).applyTo(this);

			Group group = new Group(this, SWT.NONE);
			GridLayoutFactory.swtDefaults().applyTo(group);
			group.setText("Polygons");

			FormToolkit formToolkit = new FormToolkit(parent.getDisplay());

			Button fillButton = formToolkit.createButton(group, "Display polygons as filled", SWT.CHECK);
			GridDataFactory.fillDefaults().grab(true, false).applyTo(fillButton);

			bindingContext.bindValue(WidgetProperties.buttonSelection().observe(fillButton), fillFlag);
			bindingContext.updateTargets();
		}
	}
}
