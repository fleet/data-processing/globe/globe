package fr.ifremer.viewer3d;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalUtils;

public class CapacityInspector {

	private static Logger logger = LoggerFactory.getLogger(CapacityInspector.class);

	private static class Attr {
		private Attr(String name) {
			this.name = name;
		}

		private String name;
		public int value = -1;

		@Override
		public String toString() {
			return name + " : " + value;
		}
	}

	private final static Map<Integer, Attr> OPEN_GL_ATTRIBUTES = Map.of(//
			GL.GL_STENCIL_BITS, new Attr("stencil bits"), //
			GL.GL_DEPTH_BITS, new Attr("depth bits"), //
			GL2.GL_MAX_TEXTURE_UNITS, new Attr("max texture units"), //
			GL2.GL_MAX_TEXTURE_COORDS_ARB, new Attr("max texture coords"), //
			GL.GL_MAX_TEXTURE_SIZE, new Attr("max texture size"), //
			GL2.GL_MAX_ELEMENTS_INDICES, new Attr("max elements indices"), //
			GL2.GL_MAX_ELEMENTS_VERTICES, new Attr("max elements vertices"), //
			GL2.GL_MAX_LIGHTS, new Attr("max lights"));


	public void dump() {

		logger.debug(gov.nasa.worldwind.Version.getVersion() + "\n");
		GdalUtils.logGdalInfo();
		logger.debug("\nSystem Properties\n");
		logger.debug("Processors: " + Runtime.getRuntime().availableProcessors() + "\n");
		logger.debug("Free memory: " + Runtime.getRuntime().freeMemory() + " bytes\n");
		logger.debug("Max memory: " + Runtime.getRuntime().maxMemory() + " bytes\n");
		logger.debug("Total memory: " + Runtime.getRuntime().totalMemory() + " bytes\n");

		// clone needed to avoid java.util.ConcurrentModificationException
		Properties props = (Properties) System.getProperties().clone();

		for (Map.Entry<Object, Object> prop : props.entrySet()) {
			logger.debug(prop.getKey() + " = " + prop.getValue() + "\n");
		}

		if (GLContext.getCurrent() != null) {
			com.jogamp.opengl.GL gl = GLContext.getCurrent().getGL();
			logger.debug("\nOpenGL Values\n");
			String oglVersion = gl.glGetString(GL.GL_VERSION);
			logger.debug("OpenGL version: " + oglVersion + "\n");

			int[] intVals = new int[1];
			for (Entry<Integer, Attr> entry : OPEN_GL_ATTRIBUTES.entrySet()) {
				gl.glGetIntegerv((Integer) entry.getKey(), intVals, 0);
				entry.getValue().value = intVals[0];
				logger.debug(entry.getValue().toString() + "\n");
			}

			String extensionString = gl.glGetString(GL.GL_EXTENSIONS);
			String[] extensions = extensionString.split(" ");
			logger.debug("Extensions\n");
			for (String ext : extensions) {
				logger.debug("    " + ext + "\n");
			}
		}

		logger.debug("\nJOGL Values\n");
		String pkgName = "javax.media.opengl";
		try {
			Class<?> c = getClass().getClassLoader().loadClass(pkgName + ".GL2");

			Package p = c.getPackage();

			if (p == null) {
				logger.debug("WARNING: Package.getPackage(" + pkgName + ") is null\n");
			} else {
				logger.debug("Specification Title = " + p.getSpecificationTitle() + "\n");
				logger.debug("Specification Version = " + p.getSpecificationVersion() + "\n");
				logger.debug("Implementation Version = " + p.getImplementationVersion() + "\n");
			}
		} catch (ClassNotFoundException e) {
			logger.debug("Unable to load " + pkgName + "\n");
		}

	}

	public static int getMaxTextureSize() {
		return OPEN_GL_ATTRIBUTES.get(GL.GL_MAX_TEXTURE_SIZE).value;
	}
}
