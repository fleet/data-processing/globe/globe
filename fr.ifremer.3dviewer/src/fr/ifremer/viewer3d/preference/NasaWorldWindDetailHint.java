/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.preference;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.SwingUtilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.viewer3d.layers.terrain.TerrainLayer;
import fr.ifremer.viewer3d.util.conf.ProxyConf;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.TiledImageLayer;
import gov.nasa.worldwind.terrain.BasicElevationModel;
import gov.nasa.worldwind.terrain.CompoundElevationModel;

/**
 * Manager of NasaWorldWind Detail Hints
 */
public class NasaWorldWindDetailHint {

	protected List<WeakReference<DetailHintComposite>> detailHintComposites = new ArrayList<>();

	/**
	 * Constructor
	 */
	public NasaWorldWindDetailHint() {
	}

	/**
	 * Create a view
	 */
	public DetailHintComposite initializeView(Composite parent, int style) {
		DetailHintComposite view = new DetailHintComposite(parent, style);
		ProxyConf proxy = SSVConfiguration.getConfiguration().getProxy();
		view.setTerrainDetailHint(proxy.getLOD());
		view.setImageDetailHint(proxy.getImageLevelOfDetail());

		view.getTerrainScale().addListener(SWT.Selection, (e) -> {
			double elevationDetailHint = view.getTerrainDetailHint();
			if (elevationDetailHint != proxy.getLOD()) {
				proxy.setLOD(elevationDetailHint);
				applyTerrainDetailHint(view.getTerrainDetailHint());
				// Update all other views
				for (Iterator<WeakReference<DetailHintComposite>> iterator = detailHintComposites.iterator(); iterator
						.hasNext();) {
					WeakReference<DetailHintComposite> weakReference = iterator.next();
					DetailHintComposite detailHintComposite = weakReference.get();
					if (detailHintComposite != view) {
						if (detailHintComposite != null && !detailHintComposite.isDisposed()
								&& !weakReference.isEnqueued()) {
							detailHintComposite.setTerrainDetailHint(elevationDetailHint);
						} else {
							iterator.remove();
						}
					}
				}
			}
		});

		view.getImageScale().addListener(SWT.Selection, (e) -> {
			double imageDetailHint = view.getImageDetailHint();
			if (imageDetailHint != proxy.getImageLevelOfDetail()) {
				proxy.setImageLevelOfDetail(imageDetailHint);
				applyImageDetailHint(imageDetailHint);
				// Update all other views
				for (Iterator<WeakReference<DetailHintComposite>> iterator = detailHintComposites.iterator(); iterator
						.hasNext();) {
					WeakReference<DetailHintComposite> weakReference = iterator.next();
					DetailHintComposite detailHintComposite = weakReference.get();
					if (detailHintComposite != view) {
						if (detailHintComposite != null && !detailHintComposite.isDisposed()
								&& !weakReference.isEnqueued()) {
							detailHintComposite.setImageDetailHint(imageDetailHint);
						} else {
							iterator.remove();
						}
					}
				}
			}
		});

		detailHintComposites.add(new WeakReference<>(view));

		return view;
	}

	/**
	 * Apply the detail hint to all elevation models
	 *
	 * @param elevationDetailHint hint from -1 to 1
	 */
	public void applyTerrainDetailHint(double elevationDetailHint) {
		if (!Double.isNaN(elevationDetailHint)) {
			SwingUtilities.invokeLater(() -> {
				var viewer3d = IGeographicViewService.grab();
				setElevationModelDetailHint(viewer3d.getElevationModel(),
						viewer3d.isHighResolution() ? 1.0 : elevationDetailHint);
				viewer3d.getWwd().redraw();
			});
		}
	}

	/**
	 * Apply the detail hint to all Image models
	 *
	 * @param elevationDetailHint hint from -1 to 1
	 */
	public void applyImageDetailHint(double imageDetailHint) {
		if (!Double.isNaN(imageDetailHint)) {
			SwingUtilities.invokeLater(() -> {
				var viewer3d = IGeographicViewService.grab();
				LayerList layerList = viewer3d.getLayers();
				for (Layer layer : layerList) {
					if (layer instanceof TerrainLayer terrainLayer) {
						terrainLayer.setDetailHint(viewer3d.isHighResolution() ? 1.0 : imageDetailHint);
					} else if (layer instanceof TiledImageLayer tiledLayer) {
						tiledLayer.setDetailHint(imageDetailHint);
					}
				}
				viewer3d.getWwd().redraw();
			});
		}
	}

	protected void setElevationModelDetailHint(ElevationModel elevationModel, double detailHint) {
		if (elevationModel instanceof BasicElevationModel basicModel) {
			basicModel.setDetailHint(detailHint);
		} else if (elevationModel instanceof CompoundElevationModel compoundModel) {
			for (ElevationModel m : compoundModel.getElevationModels()) {
				setElevationModelDetailHint(m, detailHint);
			}
		}
	}

	/**
	 * View
	 */
	public static class DetailHintComposite extends Composite {
		protected Scale terrainScale;
		protected Scale imageScale;

		/**
		 * Constructor
		 */
		public DetailHintComposite(Composite parent, int style) {
			super(parent, style);
			setLayout(new GridLayout(4, false));

			Label label = new Label(this, SWT.NONE);
			label.setText("Terrain :");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			label = new Label(this, SWT.NONE);
			label.setText("min");
			GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false);
			gridData.horizontalIndent = 15;
			label.setLayoutData(gridData);
			terrainScale = new Scale(this, SWT.NONE);
			terrainScale.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			terrainScale.setMinimum(0);
			terrainScale.setMaximum(20);
			label = new Label(this, SWT.NONE);
			label.setText("max");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

			label = new Label(this, SWT.NONE);
			label.setText("Image :");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			label = new Label(this, SWT.NONE);
			label.setText("min");
			label.setLayoutData(gridData);
			imageScale = new Scale(this, SWT.NONE);
			imageScale.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			imageScale.setMinimum(0);
			imageScale.setMaximum(20);
			label = new Label(this, SWT.NONE);
			label.setText("max");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		}

		public double getTerrainDetailHint() {
			return (getTerrainScale().getSelection() - 10d) / 10d;
		}

		public void setTerrainDetailHint(double value) {
			getTerrainScale().getDisplay().asyncExec(() -> {
				if (!getTerrainScale().isDisposed()) {
					getTerrainScale().setSelection((int) (value * 10d + 10d));
				}
			});
		}

		public double getImageDetailHint() {
			return (getImageScale().getSelection() - 10d) / 10d;
		}

		public void setImageDetailHint(double value) {
			getImageScale().getDisplay().asyncExec(() -> {
				if (!getImageScale().isDisposed()) {
					getImageScale().setSelection((int) (value * 10d + 10d));
				}
			});
		}

		/**
		 * Getter of terrainScale
		 */
		public Scale getTerrainScale() {
			return terrainScale;
		}

		/**
		 * Getter of imageScale
		 */
		public Scale getImageScale() {
			return imageScale;
		}
	}

}
