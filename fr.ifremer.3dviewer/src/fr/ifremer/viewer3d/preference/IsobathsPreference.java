package fr.ifremer.viewer3d.preference;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DoublePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;

/**
 * Preferences of the isobaths
 */
public class IsobathsPreference extends PreferenceComposite {

	protected DoublePreferenceAttribute isoBathlineSize;
	protected IntegerPreferenceAttribute isoBathOffSampling;

	protected ColorPreferenceAttribute isobathMasterLineColor;
	protected ColorPreferenceAttribute isobathLineColor;

	/**
	 * Constructor
	 */
	public IsobathsPreference(PreferenceComposite superNode) {
		super(superNode,  "Isobaths");

		isoBathlineSize = new DoublePreferenceAttribute("10_0_isolineSize","isobath line size", 2d);
		isoBathOffSampling = new IntegerPreferenceAttribute("10_0_isoBathOffSampling","isobath sampling", 70);
		isobathMasterLineColor = new ColorPreferenceAttribute("10_1_isobathMasterColor","Isobath master line color", new GColor(255,0,0, 255));
		isobathLineColor =new ColorPreferenceAttribute("10_1_isobathColor","Isobath line color", new GColor(255,255,255, 255));

		declareAttribute(isoBathlineSize);
		declareAttribute(isoBathOffSampling);
		declareAttribute(isobathMasterLineColor);
		declareAttribute(isobathLineColor);

		load();
	}

	/**
	 * @return the {@link #isoBathlineSize}
	 */
	public DoublePreferenceAttribute getIsoBathlineSize() {
		return isoBathlineSize;
	}

	/**
	 * @param isoBathlineSize the {@link #isoBathlineSize} to set
	 */
	public void setIsoBathlineSize(DoublePreferenceAttribute isoBathlineSize) {
		this.isoBathlineSize = isoBathlineSize;
	}

	/**
	 * @return the {@link #isoBathOffSampling}
	 */
	public IntegerPreferenceAttribute getIsoBathOffSampling() {
		return isoBathOffSampling;
	}

	/**
	 * @param isoBathOffSampling the {@link #isoBathOffSampling} to set
	 */
	public void setIsoBathOffSampling(IntegerPreferenceAttribute isoBathOffSampling) {
		this.isoBathOffSampling = isoBathOffSampling;
	}

	/**
	 * @return the {@link #isobathMasterLineColor}
	 */
	public ColorPreferenceAttribute getIsobathMasterLineColor() {
		return isobathMasterLineColor;
	}

	/**
	 * @param isobathMasterLineColor the {@link #isobathMasterLineColor} to set
	 */
	public void setIsobathMasterLineColor(ColorPreferenceAttribute isobathMasterLineColor) {
		this.isobathMasterLineColor = isobathMasterLineColor;
	}

	/**
	 * @return the {@link #isobathLineColor}
	 */
	public ColorPreferenceAttribute getIsobathLineColor() {
		return isobathLineColor;
	}

	/**
	 * @param isobathLineColor the {@link #isobathLineColor} to set
	 */
	public void setIsobathLineColor(ColorPreferenceAttribute isobathLineColor) {
		this.isobathLineColor = isobathLineColor;
	}

}
