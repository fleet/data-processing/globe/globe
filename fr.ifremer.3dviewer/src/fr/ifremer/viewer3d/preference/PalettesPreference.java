package fr.ifremer.viewer3d.preference;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.PalettePreferenceAttribute;
import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;

/**
 * Preferences of palettes
 */
public class PalettesPreference extends PreferenceComposite {

	private final PalettePreferenceAttribute bathymetryPalette;
	private final PalettePreferenceAttribute backscatterPalette;
	private final PalettePreferenceAttribute wcPalette;
	private final PalettePreferenceAttribute griddedDataPalette;

	/**
	 * Constructor
	 */
	public PalettesPreference(PreferenceComposite superNode) {
		super(superNode, "Color palettes");

		bathymetryPalette = new PalettePreferenceAttribute("palette_bathymetry", "Bathymetry", ColorMaps.GMT_JET.label);
		declareAttribute(bathymetryPalette);

		backscatterPalette = new PalettePreferenceAttribute("palette_backscatter", "Backscatter", ColorMaps.GRAY.label);
		declareAttribute(backscatterPalette);

		wcPalette = new PalettePreferenceAttribute("palette_wc", "Water column", ColorMaps.U_CMOCEAN_THERMAL.label);
		declareAttribute(wcPalette);

		griddedDataPalette = new PalettePreferenceAttribute("palette_gridded_data", "Gridded data",
				ColorMaps.U_CMOCEAN_THERMAL.label);
		declareAttribute(griddedDataPalette);

		load();
	}

	/**
	 * @return the {@link #bathymetryPalette}
	 */
	public PalettePreferenceAttribute getBathymetryPalette() {
		return bathymetryPalette;
	}

	/**
	 * @return the {@link #backscatterPalette}
	 */
	public PalettePreferenceAttribute getBackscatterPalette() {
		return backscatterPalette;
	}

	/**
	 * @return the {@link #wcPalette}
	 */
	public PalettePreferenceAttribute getWcPalette() {
		return wcPalette;
	}

	/**
	 * @return the {@link #griddedDataPalette}
	 */
	public PalettePreferenceAttribute getGriddedDataPalette() {
		return griddedDataPalette;
	}

}
