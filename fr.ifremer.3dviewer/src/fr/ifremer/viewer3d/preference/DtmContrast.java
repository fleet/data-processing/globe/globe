package fr.ifremer.viewer3d.preference;

/**
 * enumeration of available contrast for default dtm contrast
 */
public enum DtmContrast {
	MINMAX,REHAUSS05,REHAUSS1
}
