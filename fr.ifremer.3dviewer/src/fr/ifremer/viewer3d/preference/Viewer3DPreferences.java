package fr.ifremer.viewer3d.preference;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.OptionalBooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.OptionalBooleanPreferenceAttribute.OptionalBoolean;

public class Viewer3DPreferences extends PreferenceComposite {

	protected BooleanPreferenceAttribute useCacheForProjection;
	protected StringPreferenceAttribute columnsOrder;
	protected StringPreferenceAttribute hiddenColumns;
	protected BooleanPreferenceAttribute singlePointingMode;
	protected ColorPreferenceAttribute PointSelectionColor;
	private BooleanPreferenceAttribute displayNavigationLineProjection;
	private BooleanPreferenceAttribute syncTextureLayerSelection;
	private BooleanPreferenceAttribute syncTextureLayerWithCriteria;
	protected EnumPreferenceAttribute dtmDefaultContrast;
	protected BooleanPreferenceAttribute enableGamePadDetection;
	protected IntegerPreferenceAttribute maxDTMExaggerationValue;
	protected IntegerPreferenceAttribute maxValuesFilterLevel;
	protected BooleanPreferenceAttribute wcActivated;
	protected BooleanPreferenceAttribute isSynchronousRedrawAfterCacheRefresh;
	protected BooleanPreferenceAttribute isFillPolygonShape;
	private BooleanPreferenceAttribute interpolateElevationModel;

	public Viewer3DPreferences(PreferenceComposite superNode, String nodeName) {
		super(superNode, nodeName);
		// root
		dtmDefaultContrast = new EnumPreferenceAttribute("dtmDefaultContrast", "Default DTM contrast",
				DtmContrast.MINMAX);
		declareAttribute(dtmDefaultContrast);

		isFillPolygonShape = new BooleanPreferenceAttribute("fillPolygonShape", "Display polygon as filled", false);
		declareAttribute(isFillPolygonShape);

		singlePointingMode = new BooleanPreferenceAttribute("singlePointingMode3D",
				"Toggle single pointing mode (for 3D view)", false);
		declareAttribute(singlePointingMode);
		PointSelectionColor = new ColorPreferenceAttribute("PointSelectionColor", "Selection border color",
				new GColor(255, 255, 255));
		declareAttribute(PointSelectionColor);

		displayNavigationLineProjection = new BooleanPreferenceAttribute("displayNavigationLineProjection",
				"Display projections of navigation lines on the terrain", false);
		declareAttribute(displayNavigationLineProjection);

		syncTextureLayerSelection = new BooleanPreferenceAttribute("syncTextureLayerSelection",
				"Synchronize texture layer selection", false);
		declareAttribute(syncTextureLayerSelection);
		syncTextureLayerWithCriteria = new BooleanPreferenceAttribute("syncTextureLayerWithCriteria",
				"Synchronize texture layer using criteria (frequence)", false);
		declareAttribute(syncTextureLayerWithCriteria);

		// Elevation model
		PreferenceComposite elevationModel = new PreferenceComposite(this, "Elevation model");
		interpolateElevationModel = new BooleanPreferenceAttribute("interpolateElevationModel",
				"Missing data interpolation", true);
		elevationModel.declareAttribute(interpolateElevationModel);

		// netCDF sub node
		PreferenceComposite netCDF = new PreferenceComposite(this, "NetCDF");
		useCacheForProjection = new BooleanPreferenceAttribute("useCacheForProjection", "use cache for projection",
				true);
		netCDF.declareAttribute(useCacheForProjection);

		// markerEditor sub node
		PreferenceComposite markerEditor = new PreferenceComposite(this, "markerEditor");
		columnsOrder = new StringPreferenceAttribute("columnsOrder", "columns order(comma separated)", "");
		hiddenColumns = new StringPreferenceAttribute("hiddenColumns", "hidden columns(comma separated)", "");
		markerEditor.declareAttribute(columnsOrder);
		markerEditor.declareAttribute(hiddenColumns);

		enableGamePadDetection = new BooleanPreferenceAttribute("gamePadDetection", "Enable GamePad auto detect",
				false);
		declareAttribute(enableGamePadDetection);

		maxDTMExaggerationValue = new IntegerPreferenceAttribute("maxDTMExaggerationValue",
				"Max DTM vertical exaggeration", 20);
		declareAttribute(maxDTMExaggerationValue);

		isSynchronousRedrawAfterCacheRefresh = new BooleanPreferenceAttribute("isSynchronousRedrawAfterCacheRefresh",
				"Synchronize redraw with cache refreshing", Boolean.TRUE);
		declareAttribute(isSynchronousRedrawAfterCacheRefresh);

		PreferenceComposite waterColumn = new PreferenceComposite(this, "WaterColumn");
		maxValuesFilterLevel = new IntegerPreferenceAttribute("maxValuesFilterLevel", "Level of max values filter", 20);
		waterColumn.declareAttribute(maxValuesFilterLevel);
		// Activates WC when loading. Null means not already selected by user
		wcActivated = new BooleanPreferenceAttribute("isWcActivated", "Activates when loading", false);
		waterColumn.declareAttribute(wcActivated);

		load();
	}

	public DtmContrast getDtmDefaultContrast() {
		return (DtmContrast) dtmDefaultContrast.getValue();
	}

	public BooleanPreferenceAttribute getUseCacheForProjection() {
		return useCacheForProjection;
	}

	public StringPreferenceAttribute getColumnsOrder() {
		return columnsOrder;
	}

	public StringPreferenceAttribute getHiddenColumns() {
		return hiddenColumns;
	}

	public boolean isSinglePointingModeEnabled() {
		return singlePointingMode.getValue();
	}

	public BooleanPreferenceAttribute getEnableGamePadDetection() {
		return enableGamePadDetection;
	}

	public IntegerPreferenceAttribute getMaxVerticalExaggeration() {
		return maxDTMExaggerationValue;
	}

	public BooleanPreferenceAttribute getInterpolateElevationModel() {
		return interpolateElevationModel;
	}

	public GColor getPointSelectionColor() {
		// TODO verifier que la couleur n'est pas presente dans la typologie, sinon on remet blanc (que l'on interdi
		// dans la typo
		// si une correction est faite, on envoi un message
		return PointSelectionColor.getValue();
	}

	public BooleanPreferenceAttribute getDisplayNavigationLineProjection() {
		return displayNavigationLineProjection;
	}

	public BooleanPreferenceAttribute getSyncTextureLayerSelection() {
		return syncTextureLayerSelection;
	}

	public BooleanPreferenceAttribute getSyncTextureLayerWithCriteria() {
		return syncTextureLayerWithCriteria;
	}

	public int getMaxValuesLevelFilter() {
		return maxValuesFilterLevel.getValue();
	}

	public IntegerPreferenceAttribute getMaxValuesLevelFilterPref() {
		return maxValuesFilterLevel;
	}

	public void setMaxValuesFilterLevel(int maxValuesFilterLevel) {
		this.maxValuesFilterLevel.setValue(maxValuesFilterLevel, true);
		save();
	}

	public BooleanPreferenceAttribute getSynchronousRedrawAfterCacheRefresh() {
		return isSynchronousRedrawAfterCacheRefresh;
	}

	public boolean getIsFillPolygon() {
		return isFillPolygonShape.getValue();
	}

	public BooleanPreferenceAttribute getIsFillPolygonAttribute() {
		return isFillPolygonShape;
	}

	/**
	 * @return the {@link #wcActivated}
	 */
	public BooleanPreferenceAttribute getWcActivated() {
		return wcActivated;
	}

}
