package fr.ifremer.viewer3d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;

public class SynchronizeHandler {

	@Execute
	public void execute(IGeographicViewService geographicViewService) {
		geographicViewService.setSyncViewSelection(!geographicViewService.isSyncViewSelection());
	}
}
