package fr.ifremer.viewer3d.handlers;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.ProxyController;
import fr.ifremer.viewer3d.dialogs.PreferencesDialog;

public class PreferencesHandler {

	private Logger logger = LoggerFactory.getLogger(PreferencesHandler.class);

	@Inject
	@Named(IServiceConstants.ACTIVE_SHELL)
	protected Shell shell;

	@Execute
	public void execute(ProxyController proxyController) {
		logger.debug("Preferences Handler");
		PreferencesDialog dialog = new PreferencesDialog(shell, proxyController);
		dialog.open();
	}

}
