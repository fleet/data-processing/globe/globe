package fr.ifremer.viewer3d.handlers;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;

import fr.ifremer.viewer3d.util.CDITool;

public class CDIHandler {

	@Inject
	private CDITool cdiTool;

	@Execute
	public void execute(@Optional @Active MHandledToolItem item) {
		if (item.isSelected()) {
			cdiTool.hookListeners();
		} else {
			cdiTool.exit();
		}
	}

}
