package fr.ifremer.viewer3d.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.dialogs.ShortcutsDialog;

public class ControlsHandler {

	private Logger logger = LoggerFactory.getLogger(ControlsHandler.class);

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		logger.info("Controls handler");

		ShortcutsDialog dialog = new ShortcutsDialog(shell);
		dialog.open();
	}

}
