package fr.ifremer.viewer3d.handlers;

import java.util.concurrent.atomic.AtomicBoolean;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectToolItem;

import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.viewer3d.layers.background.WWLatLonGraticuleLayer;

/** Handler to show / hide the graticule */
public class ShowGraticuleLayerHandler {

	private final AtomicBoolean isInitilized = new AtomicBoolean();

	@Inject
	private WWLatLonGraticuleLayer latLonGraticuleLayer;
	@Inject
	private ProjectExplorerModel projectExplorerModel;

	@CanExecute
	public boolean canExecute(@Active MDirectToolItem item, MPart part) {
		if (isInitilized.compareAndSet(false, true)) {
			item.setSelected(latLonGraticuleLayer.isEnabled());
			latLonGraticuleLayer.addPropertyChangeListener("Enabled",
					evt -> item.setSelected(latLonGraticuleLayer.isEnabled()));
		}
		return true;
	}

	/** Show / hide the graticule */
	@Execute
	public void execute(@Optional @Active MDirectToolItem item) {
		// Search the LayerNode of the graticule to change its checked state (thus change the layer enabled flag */
		java.util.Optional<LayerNode> layerNode = projectExplorerModel.getBackgroundNode().getChildNode(LayerNode.class,
				n -> n.getLayer().filter(latLonGraticuleLayer::equals).isPresent());
		layerNode.ifPresent(node -> node.setChecked(!latLonGraticuleLayer.isEnabled()));
	}
}
