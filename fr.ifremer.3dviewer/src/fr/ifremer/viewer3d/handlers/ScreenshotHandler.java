package fr.ifremer.viewer3d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.screenshot.OGLScreenshot;
import fr.ifremer.viewer3d.Viewer3D;

public class ScreenshotHandler {

	private Logger logger = LoggerFactory.getLogger(ScreenshotHandler.class);

	@Execute
	public void execute(Shell shell) {
		logger.debug("Screenshot Handler");

		FileDialog dialog = new FileDialog(shell, SWT.SAVE);

		dialog.setFilterNames(OGLScreenshot.FORMAT_NAMES);
		dialog.setFilterExtensions(OGLScreenshot.FORMAT_EXTENSIONS);
		// if ((defaultDirectory != null) && (!defaultDirectory.isEmpty())) {
		// dialog.setFilterPath(defaultDirectory);
		// }
		final String filename = dialog.open();
		// defaultDirectory = dialog.getFilterPath();
		if (filename != null) {
			new OGLScreenshot(Viewer3D.getWwd(), filename);
		} else {
			logger.info("Null file name : no screen shot");
		}
	}

}
