package fr.ifremer.viewer3d.handlers;

import java.io.File;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.session.ISessionService;

public class ChangeSessionHandler {

	private Logger logger = LoggerFactory.getLogger(ChangeSessionHandler.class);

	@Execute
	public void execute(Shell shell, ISessionService sessionService) {
		logger.debug("Change Session Handler");

		FileDialog dialog = new FileDialog(shell, SWT.OPEN);

		dialog.setFilterNames(new String[] { "Session file" });
		dialog.setFilterExtensions(new String[] { "*.json;*.xml" });

		final String path = dialog.open();

		// Create survey from xml session file
		if (path != null) {
			File sessionFile = new File(path);
			try {
				logger.debug("Loading session file " + path);
				sessionService.loadSession(sessionFile);
			} catch (Exception e) {
				logger.error("Error loading file " + path + " : " + e.getMessage(), e);
				MessageDialog.openError(shell, "Error loading file",
						"Error loading file : (error message:" + e.getMessage() + ")");
			}
		}
	}
}