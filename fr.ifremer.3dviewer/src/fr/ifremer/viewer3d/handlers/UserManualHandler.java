package fr.ifremer.viewer3d.handlers;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import jakarta.inject.Named;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserManualHandler {

	private Logger logger = LoggerFactory.getLogger(UserManualHandler.class);

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		logger.info("User Manual handler");

		String pdfName = Platform.getInstallLocation().getURL().getPath() + "/3DViewer_User_Manual.pdf";

		// reading and writing pdf file...
		File pdfFile = new File(pdfName);
		if (!pdfFile.exists()) {
			try {
				URL fileUrl = Platform.getBundle("fr.ifremer.3dviewer.help").getEntry("/");
				String filename = FileLocator.resolve(fileUrl).getFile() + "/help/3DViewer_User_Manual.pdf";
				InputStream inputStream = new FileInputStream(filename);
				OutputStream out = new FileOutputStream(pdfFile);
				byte buf[] = new byte[1024];
				int len;
				while ((len = inputStream.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.close();
				inputStream.close();
			} catch (IOException e) {
				logger.error("Can't read or write user manual");
			}
		}

		// opening pdf file...
		if (pdfName != null) {
			try {
				if (pdfFile.exists()) {
					if (Desktop.isDesktopSupported()) {
						Desktop.getDesktop().open(pdfFile);
					} else {
						logger.debug("Awt Desktop is not supported!");
					}
				} else {
					logger.debug("PDF File is not exists!");
				}
			} catch (Exception ex) {
				try {
					if (Desktop.isDesktopSupported()) {
						Desktop.getDesktop().open(new File(Platform.getInstallLocation().getURL().getPath()));
					} else {
						logger.debug("Awt Desktop is not supported!");
					}
				} catch (Exception ex2) {
					logger.error("Can't open user manual " + ex2);
				}
			}
		}
	}
}
