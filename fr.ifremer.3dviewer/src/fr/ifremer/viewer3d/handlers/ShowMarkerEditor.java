/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.viewer3d.gui.markereditor.MarkerEditor;

/**
 * Open the marker editor
 */
public class ShowMarkerEditor {

	public final static String COMMAND_ID = "fr.ifremer.3dviewer.command.showmarkereditor";

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application,
			EModelService modelService) {
		MPart openedMarkerEditor = PartUtil.findPart(application, partService, MarkerEditor.PART_ID);
		// create part if necessary
		if (openedMarkerEditor == null) {
			openedMarkerEditor = partService.createPart(MarkerEditor.PART_ID);
			PartManager.addPartToStack(openedMarkerEditor, application, modelService,
					PartManager.RIGHT_BOTTOM_STACK_ID);
		}
		PartUtil.showPartSashContainer(openedMarkerEditor);
		PartManager.showPart(partService, openedMarkerEditor);
	}
}
