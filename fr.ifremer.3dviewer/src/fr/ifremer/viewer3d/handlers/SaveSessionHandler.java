package fr.ifremer.viewer3d.handlers;

import java.io.File;
import java.io.IOException;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;

public class SaveSessionHandler {

	private Logger logger = LoggerFactory.getLogger(SaveSessionHandler.class);

	/**
	 * Execute this handler.
	 */
	@Execute
	public void execute(Shell shell, ISessionService sessionService) {
		logger.debug("Save Session Handler");
		MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO | SWT.CANCEL);
		messageBox.setText(DicoBundle.getString("SAVE_SESSION_ACTION"));
		messageBox.setMessage(DicoBundle.getString("SAVE_SESSION_RELATIVE_PATH"));
		int answer = messageBox.open();
		if (answer == SWT.CANCEL) {
			return;
		}

		// Save survey in xml session file
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);

		dialog.setFilterNames(new String[] { "Session file (*.json)" });
		dialog.setFilterExtensions(new String[] { "*.json" });

		final String path = dialog.open();

		if (path != null) {
			try {
				sessionService.saveSession(answer == SWT.YES, new File(path));
			} catch (IOException e) {
				logger.error("Error saving file " + path + " : " + e.getMessage(), e);
				MessageDialog.openError(shell, "Error loading file",
						"Error saving file : (error message:" + e.getMessage() + ")");
			}

		} else {
			logger.info("Null configuration file name");
		}
	}

}
