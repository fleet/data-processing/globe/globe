/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.viewer3d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.Viewer3D;

public class QuitHandler {

	private Logger logger = LoggerFactory.getLogger(QuitHandler.class);

	@Execute
	public void execute(IWorkbench workbench) {
		logger.info("quit handler");
		if (MessageDialog.openConfirm(Display.getDefault().getActiveShell(), "Confirmation", "Do you want to exit?")) {
			Viewer3D.getCurrentViewer3D().saveConfiguration();
			workbench.close();
		}

	}
}
