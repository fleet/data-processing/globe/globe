package fr.ifremer.viewer3d.handlers;

import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.view.BasicViewPropertyLimits;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

import org.eclipse.e4.core.di.annotations.Execute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResetViewHandler {

	private Logger logger = LoggerFactory.getLogger(ResetViewHandler.class);

	@Execute
	public void execute() {
		logger.debug("Reset View Handler");

		View view = Viewer3D.getWwd().getView();
		if (view == null) {
			return;
		}

		if (view instanceof BasicOrbitView) {
			Angle newHeading = BasicViewPropertyLimits.limitHeading(Angle.ZERO, ((BasicOrbitView) view).getOrbitViewLimits());
			Angle newPitch = BasicViewPropertyLimits.limitPitch(Angle.ZERO, ((BasicOrbitView) view).getOrbitViewLimits());
			// view.applyStateIterator(ScheduledOrbitViewStateIterator.createHeadingPitchIterator(
			// ((OrbitView) view).getHeading(), newHeading, ((OrbitView)
			// view).getPitch(), newPitch));
			((BasicOrbitView) view).addHeadingPitchAnimator(((BasicOrbitView) view).getHeading(), newHeading, ((BasicOrbitView) view).getPitch(), newPitch);
		}
	}

}
