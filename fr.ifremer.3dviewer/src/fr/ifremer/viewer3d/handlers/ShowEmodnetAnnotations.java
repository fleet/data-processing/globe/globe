
package fr.ifremer.viewer3d.handlers;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

import fr.ifremer.viewer3d.layers.annotation.http.HttpAnnotationController;

/** Handler to show or hide annotations layer */
public class ShowEmodnetAnnotations {

	@CanExecute
	private boolean canExecute(@Active MToolItem item, HttpAnnotationController controller) {
		if (controller.isAnnotationsVisible())
			item.setLabel("Hide EMODnet CVE annotations");
		else
			item.setLabel("Show EMODnet CVE annotations");
		return true;
	}

	@Execute
	private void execute(@Active MToolItem item, HttpAnnotationController controller) {
		if (item.isSelected()) {
			controller.show();
		} else {
			controller.hide();
		}
	}

}