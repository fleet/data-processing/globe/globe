package fr.ifremer.viewer3d.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.dialogs.GoToDialog;

public class GoToHandler {

	private Logger logger = LoggerFactory.getLogger(GoToHandler.class);

	@Execute
	public void execute() {
		logger.debug("GoTo Handler");

		Dialog dialog = new GoToDialog(Display.getCurrent().getActiveShell());
		dialog.open();

	}

}
