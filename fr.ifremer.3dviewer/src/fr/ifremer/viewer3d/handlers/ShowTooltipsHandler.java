package fr.ifremer.viewer3d.handlers;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectToolItem;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.viewer3d.util.tooltip.ToolTipController;

public class ShowTooltipsHandler {

	@Inject
	ToolTipController tooltipController;

	@Inject
	IGlobalCursorService globalCursorService;

	@Inject
	IGeographicViewService geographicViewService;

	@Execute
	public void execute(@Optional @Active MDirectToolItem item) {
		if (item.isSelected()) {
			geographicViewService.setTooltipEnabled(true);
			geographicViewService.getWwd().addSelectListener(tooltipController);
			if (!geographicViewService.getLayers().contains(tooltipController.getLayer())) {
				geographicViewService.addLayerBeforeCompass(tooltipController.getLayer());
			}
		} else {
			geographicViewService.setTooltipEnabled(false);
			geographicViewService.getWwd().removeSelectListener(tooltipController);
			geographicViewService.removeLayer(tooltipController.getLayer());
			globalCursorService.submitEmpty();
		}

	}
}
