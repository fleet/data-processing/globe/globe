package fr.ifremer.viewer3d.handlers;

import java.awt.Cursor;

import jakarta.inject.Inject;

import org.eclipse.core.commands.CommandManager;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.viewer3d.gui.polygoneditor.handler.CreatePolygonHandler;
import fr.ifremer.viewer3d.layers.markers.MarkerController;
import fr.ifremer.viewer3d.util.SSVCursor;

public class EnableMarkersHandler {

	/** Command id defined in E4 model */
	public static final String COMMAND_ID = "fr.ifremer.3dviewer.command.addmarkers";
	/** ToolItem id defined in E4 model */
	public static final String TOOL_ITEM_ID = "fr.ifremer.3dviewer.handledtoolitem.addmarkers";

	/** Controller of marker */
	@Inject
	private MarkerController markerController;

	@Inject
	IGeographicViewService geographicViewService;

	@Execute
	public void execute(@Active MToolItem toolItem, CommandManager commandManager, EModelService modelService) {
		// Execution from Marker toolItem ?
		if (TOOL_ITEM_ID.equals(toolItem.getElementId())) {
			if (toolItem.isSelected()) {
				// Disarming the marker tool
				disarmPolygons(toolItem.getParent(), commandManager, modelService);

				geographicViewService.setMarkersEnabled(true);
				geographicViewService.getWwd().addMouseListener(markerController);

				// open marker editor
				markerController.openMarkerEditor();
			} else {
				geographicViewService.setMarkersEnabled(false);
				geographicViewService.getWwd().removeMouseListener(markerController);
			}

			// update cursor
			geographicViewService.getWwd()
					.setCursor(toolItem.isSelected() ? SSVCursor.getPredefinedCursor(SSVCursor.CURSOR_POINT)
							: Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		} else {
			// Direct execution of the command to force marker disarming
			geographicViewService.setMarkersEnabled(false);
			geographicViewService.getWwd().removeMouseListener(markerController);
		}
	}

	/**
	 * Disarming the polygon tool
	 */
	private void disarmPolygons(MElementContainer<MUIElement> toolbar, CommandManager commandManager,
			EModelService modelService) {
		MUIElement markerToolItem = modelService.find(CreatePolygonHandler.TOOL_ITEM_ID, toolbar);
		if (markerToolItem instanceof MItem) {
			MItem mItem = (MItem) markerToolItem;
			if (mItem.isSelected()) {
				mItem.setSelected(false);
				CommandUtils.executeCommand(commandManager, CreatePolygonHandler.COMMAND_ID);
			}
		}
	}

}
