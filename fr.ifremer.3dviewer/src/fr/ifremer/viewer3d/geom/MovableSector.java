package fr.ifremer.viewer3d.geom;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

import java.util.ArrayList;
import java.util.List;

/***
 * When sector are moved, for exemple by tectonic movement (see
 * ISectorMovement), the size expressed as a difference of latitude and
 * longitude could changed if mean latitude changed
 * 
 * @author MORVAN
 * 
 */
public class MovableSector extends Sector {

	private double width = -1;
	private double height = -1;
	private double heading = 0;
	private boolean isPole = false;
	@SuppressWarnings("unused")
	private boolean wasPole = false;

	private LatLon latlonSW;
	private LatLon latlonSE;
	private LatLon latlonNW;
	private LatLon latlonNE;

	public MovableSector(Angle minLatitude, Angle maxLatitude, Angle minLongitude, Angle maxLongitude) {
		super(minLatitude, maxLatitude, minLongitude, maxLongitude);
	}

	public MovableSector(Sector sector) {
		super(sector);
	}

	public MovableSector(LatLon latlonSW, LatLon latlonSE, LatLon latlonNW, LatLon latlonNE) {
		super(Angle.min(Angle.min(latlonSW.getLatitude(), latlonSE.getLatitude()), Angle.min(latlonNW.getLatitude(), latlonNE.getLatitude())), Angle.max(
				Angle.max(latlonSW.getLatitude(), latlonSE.getLatitude()), Angle.max(latlonNW.getLatitude(), latlonNE.getLatitude())), Angle.min(
						Angle.min(latlonSW.getLongitude(), latlonSE.getLongitude()), Angle.min(latlonNW.getLongitude(), latlonNE.getLongitude())), Angle.max(
								Angle.max(latlonSW.getLongitude(), latlonSE.getLongitude()), Angle.max(latlonNW.getLongitude(), latlonNE.getLongitude())));
		this.setLatlonSW(latlonSW);
		this.setLatlonSE(latlonSE);
		this.setLatlonNW(latlonNW);
		this.setLatlonNE(latlonNE);
	}

	/***
	 * width in degrees delta between max and min longitude if width not set
	 * 
	 * @return width
	 */
	public double getWidth() {
		if (width != -1) {
			return width;
		} else {
			return super.getDeltaLonDegrees();
		}
	}

	/***
	 * set longitude size in degrees
	 * 
	 * @param width
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/***
	 * height in degrees delta between max and min latitude if height not set
	 * 
	 * @return height
	 */
	public double getHeight() {
		if (height != -1) {
			return height;
		} else {
			return super.getDeltaLonDegrees();
		}
	}

	/***
	 * set latitude size in degrees
	 * 
	 * @param height
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/***
	 * ISectorMovement could induce rotation
	 * 
	 * @return heading in degrees
	 */
	public double getHeading() {
		return heading;
	}

	/***
	 * rotation centered on Latitude min and longitude min in degrees
	 * 
	 * @param heading
	 */
	public void setHeading(double heading) {
		this.heading = heading;
	}

	/***
	 * 
	 * @return the inner square of a circle centered at minLatLon with a radius
	 *         of sqrt(width*width + height*height)
	 */
	public List<Sector> getEnglobingSector() {
		List<Sector> sectorList = new ArrayList<Sector>();

		double distance = Math.sqrt(getWidth() * getWidth() + getHeight() * getHeight());
		/*
		 * if(distance > 90) distance = 90;
		 */

		// min lat
		//Angle a = getMinLatitude().subtractDegrees(distance);
		//		// min long
		//		Angle b = getMinLongitude().subtractDegrees(distance);
		//		// max lat
		//		Angle c = getMaxLatitude().addDegrees(distance);
		//		// max long
		//		Angle d = getMaxLongitude().addDegrees(distance);

		List<LatLon> l1 = new ArrayList<LatLon>();
		Angle a1 = getMinLatitude().subtractDegrees(distance);

		Angle b1 = getMinLongitude().subtractDegrees(distance);

		l1.add(new LatLon(a1, b1));

		Angle c1 = getMaxLatitude().addDegrees(distance);

		Angle d1 = getMaxLongitude().addDegrees(distance);

		l1.add(new LatLon(c1, d1));

		sectorList.add(boundingSector(l1));

		return sectorList;
	}

	public boolean isPole(LatLon NW, LatLon NE, LatLon SW, LatLon SE) {
		double diffNLat = NW.getLongitude().degrees - NE.getLongitude().degrees;
		double diffSLat = SW.getLongitude().degrees - SE.getLongitude().degrees;

		if (Math.signum(diffNLat) == Math.signum(diffSLat)) {
			return false;
		}

		return true;

	}

	public boolean isPole() {
		return isPole;
	}

	public void setPole(boolean isPole) {
		this.isPole = isPole;
	}

	// /***
	// * antarctic must be completed for design purpose
	// * waiting for correct computation of pole movement
	// * @param b
	// */
	// public void setWasPole(boolean b) {
	// this.wasPole = b;
	//
	// }
	//
	// public boolean wasPole() {
	// return wasPole ;
	// }

	public LatLon getLatlonSW() {
		return latlonSW;
	}

	public void setLatlonSW(LatLon latlonSW) {
		this.latlonSW = latlonSW;
	}

	public LatLon getLatlonSE() {
		return latlonSE;
	}

	public void setLatlonSE(LatLon latlonSE) {
		this.latlonSE = latlonSE;
	}

	public LatLon getLatlonNW() {
		return latlonNW;
	}

	public void setLatlonNW(LatLon latlonNW) {
		this.latlonNW = latlonNW;
	}

	public LatLon getLatlonNE() {
		return latlonNE;
	}

	public void setLatlonNE(LatLon latlonNE) {
		this.latlonNE = latlonNE;
	}

}
