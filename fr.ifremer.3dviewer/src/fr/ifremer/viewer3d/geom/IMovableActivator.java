package fr.ifremer.viewer3d.geom;

import gov.nasa.worldwind.Movable;
import gov.nasa.worldwind.render.Polyline;

/**
 * Extends the capacity of {@link Movable} layer to have their movement disabled
 * this allow us to reuse nasa base class (like {@link Path } or {@link Polyline}
 * and disable their drag and drop capacities
 * */
public interface IMovableActivator {
	/**
	 * tell if the current layer is currently movable
	 */
	public boolean isMovable();

	/**
	 * activate or not movement
	 * */
	public void setMovable(boolean value);

}
