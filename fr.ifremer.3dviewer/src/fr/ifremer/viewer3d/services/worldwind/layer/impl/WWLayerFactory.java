/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.services.worldwind.layer.impl;

import java.awt.Color;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;

import javax.xml.xpath.XPath;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Dataset;
import org.gdal.osr.SpatialReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.IPointCloudFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerFileInfo;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.ui.databinding.session.ModelSessionBeanWrapper;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWGeoBoxLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWIsobathLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWQuadSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWRenderableLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWShapefileLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWTiledImageLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWVerticalImageLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWVerticalImageLayer.Data;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWaterColumnVolumicLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.WWRenderableLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer.ComputableTextureParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.WWPlayableTextureInitParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiIntToDoubleFunction;
import fr.ifremer.globe.utils.function.BiIntToFloatFunction;
import fr.ifremer.sonarnative.SonarNativeLibLoader;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.layers.QuadSelectionLayer;
import fr.ifremer.viewer3d.layers.annotation.AnnotationLayer;
import fr.ifremer.viewer3d.layers.colorscale.ColorScaleLayer;
import fr.ifremer.viewer3d.layers.current.WWCurrentLayer;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.layers.geobox.GeoBoxLayer;
import fr.ifremer.viewer3d.layers.geobox.GeoBoxLayerModel;
import fr.ifremer.viewer3d.layers.label.LabelsLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathLayerFiller;
import fr.ifremer.viewer3d.layers.markers.WWMarkerLayer;
import fr.ifremer.viewer3d.layers.markers.WWWaterColumnMarkerLayer;
import fr.ifremer.viewer3d.layers.navigation.WWNavigationLayer;
import fr.ifremer.viewer3d.layers.pointcloud.PointCloudLayer;
import fr.ifremer.viewer3d.layers.polygon.PolygonLayer;
import fr.ifremer.viewer3d.layers.shp.ShpLayer;
import fr.ifremer.viewer3d.layers.shp.ShpLayerFactory;
import fr.ifremer.viewer3d.layers.swath.SwathSelectionLayer;
import fr.ifremer.viewer3d.layers.terrain.CompoundTerrainLayer;
import fr.ifremer.viewer3d.layers.terrain.CompoundTerrainWithPaletteLayer;
import fr.ifremer.viewer3d.layers.terrain.TerrainLayer;
import fr.ifremer.viewer3d.layers.terrain.TerrainWithPatletteLayer;
import fr.ifremer.viewer3d.layers.textures.ComputableTextureLayer;
import fr.ifremer.viewer3d.layers.textures.MultiTextureLayer;
import fr.ifremer.viewer3d.layers.textures.TextureLayer;
import fr.ifremer.viewer3d.layers.textures.playable.PlayableMultiTextureLayer;
import fr.ifremer.viewer3d.layers.textures.playable.PlayableTextureLayer;
import fr.ifremer.viewer3d.layers.usbl.UsblLayer;
import fr.ifremer.viewer3d.layers.wc.NativeWCVolumicLayer;
import fr.ifremer.viewer3d.layers.wc.WCVolumicLayer;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.WWXML;

/**
 * Implementation of IWWLayerFactory
 */
@Component(name = "globe_viewer3d_layer_factory", service = IWWLayerFactory.class)
public class WWLayerFactory implements IWWLayerFactory {

	@Reference
	protected IArrayFactory arrayFactory;

	@Override
	public IWWTerrainLayer createElevationLayer(File sourceFile, Document document, String kindOfData) {
		AVList layerParams = DataConfigurationUtils.getLevelSetConfigParams(document.getDocumentElement(), null);
		ShaderElevationLayer result = new ShaderElevationLayer(sourceFile, document, new LevelSet(layerParams),
				kindOfData);
		result.setUnit(IConstants.MeterUnit);
		return result;
	}

	@Override
	public IWWColorScaleLayer createColorScaleLayer(Supplier<ColorContrastModel> source, String label) {
		return new ColorScaleLayer("color_scale_" + label, source, label);
	}

	@Override
	public IWWColorScaleLayer createColorScaleLayer(IWWTerrainLayer terrainLayer) {
		return new ColorScaleLayer("color_scale_" + terrainLayer.getName(), terrainLayer, terrainLayer.getShortName());
	}

	/* @formatter:off
		_ _  _ ____ ____ ____    _    ____ _   _ ____ ____ ____
		| |\/| |__| | __ |___    |    |__|  \_/  |___ |__/ [__
		| |  | |  | |__] |___    |___ |  |   |   |___ |  \ ___]

     @formatter:on */

	@Override
	public IWWTerrainLayer createDtmLayer(File sourceFile, Document document, String kindOfData) {
		return new ShaderElevationLayer(sourceFile, document, null, kindOfData);
	}

	@Override
	public IWWTerrainLayer createTerrainLayer(List<Document> documents, String kindOfData, String name) {
		XPath xpath = WWXML.makeXPath();

		List<IWWTerrainLayer> result = new LinkedList<>();
		DoubleRange globalMinMaxValues = null;
		for (Document document : documents) {
			var domElement = document.getDocumentElement();
			Double minValue = WWXML.getDouble(domElement, "ExtremeElevations/@min", xpath);
			Double maxValue = WWXML.getDouble(domElement, "ExtremeElevations/@max", xpath);
			DoubleRange minMaxValues = new DoubleRange(-20000d, 20000d);
			if (minValue != null && maxValue != null) {
				minMaxValues = new DoubleRange(minValue, maxValue);
			}
			var rasterLayer = new TerrainLayer(document, kindOfData, minMaxValues);
			rasterLayer.setDetailHint(SSVConfiguration.getConfiguration().getProxy().getImageLevelOfDetail());

			if (globalMinMaxValues == null) {
				globalMinMaxValues = minMaxValues;
			} else {
				globalMinMaxValues = new DoubleRange(Math.min(minValue, globalMinMaxValues.getMinimumDouble()),
						Math.min(maxValue, globalMinMaxValues.getMaximumDouble()));
			}

			ContextInitializer.inject(rasterLayer);
			result.add(rasterLayer);
		}

		return result.size() == 1 ? result.get(0) : new CompoundTerrainLayer(result, name, globalMinMaxValues);
	}

	/** {@inheritDoc} */
	@Override
	public IWWTerrainWithPaletteLayer createTerrainWithPaletteLayer(List<Document> documents, String name,
			AColorPalette palette) {
		List<IWWTerrainWithPaletteLayer> result = new LinkedList<>();
		for (Document document : documents) {
			var rasterLayer = new TerrainWithPatletteLayer(document, palette);
			rasterLayer.setDetailHint(SSVConfiguration.getConfiguration().getProxy().getImageLevelOfDetail());
			ContextInitializer.inject(rasterLayer);
			result.add(rasterLayer);
		}
		return result.size() == 1 ? result.get(0) : new CompoundTerrainWithPaletteLayer(result, name);
	}

	@Override
	public IWWTiledImageLayer createTiledImageLayer(Document document) {
		return new WWTiledImageLayer(document);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<WWRasterLayerStore> createRasterLayers(RasterFeatures rasterFeatures,
			BiIntToDoubleFunction rasterValueSupplier, IProgressMonitor monitor) throws GIOException {
		SubMonitor submonitor = SubMonitor.convert(monitor, 100);

		// Translate expected projection
		SpatialReference srs = new SpatialReference();
		srs.ImportFromProj4(rasterFeatures.geoBox.getProjection().proj4String());
		// Adapt the value supplier. Gdal origin is SW (rasters like DTM managed in Globe have NW origin)
		BiIntToFloatFunction adaptValueSupplier = (row,
				column) -> (float) rasterValueSupplier.apply(rasterFeatures.rowCount - row - 1, column);
		// Generates the tiff
		Dataset rasterDataset = GdalUtils.generateTiffFile(//
				rasterFeatures.rowCount, //
				rasterFeatures.columnCount, //
				(float) rasterFeatures.noDataValue, //
				srs.ExportToWkt(), //
				rasterFeatures.geoBox.getTop(), //
				rasterFeatures.geoBox.getBottom(), //
				rasterFeatures.geoBox.getLeft(), //
				rasterFeatures.geoBox.getRight(), //
				adaptValueSupplier, //
				rasterFeatures.filePath.getPath(), //
				GdalUtils.newProgressCallback(submonitor.split(50))//
		);
		rasterDataset.delete();

		// Generates layers (using GdalRasterLayerProvider)
		var rasterInfo = IRasterService.grab().getRasterInfos(rasterFeatures.filePath.getPath()).get(0);
		rasterInfo.setDataType(rasterFeatures.rasterDataType);
		return IWWLayerService.grab().getRasterLayers(rasterInfo, submonitor.split(50));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWWWaterColumnVolumicLayer createWCLayer(ISounderNcInfo info) {
		return new WCVolumicLayer(info);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<IWWWaterColumnVolumicLayer> createNativeWCLayer(ISounderNcInfo info) {
		if (SonarNativeLibLoader.isAvailable()) {
			return Optional.of(new NativeWCVolumicLayer(info));
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public IWWVerticalImageLayer createVerticalImageLayer(Data data) {
		return new WWVerticalImageLayer(data);
	}

	/* @formatter:off
		___ ____ _  _ ___ _  _ ____ ____    _    ____ _   _ ____ ____ ____
		 |  |___  \/   |  |  | |__/ |___    |    |__|  \_/  |___ |__/ [__
		 |  |___ _/\_  |  |__| |  \ |___    |___ |  |   |   |___ |  \ ___]

 	@formatter:on */

	@Override
	public IWWTextureLayer createWWTextureLayer(String name, IWWTextureData textureData,
			WWTextureDisplayParameters displayParameters) {
		return new TextureLayer(name, textureData, displayParameters);
	}

	@Override
	public IWWComputableTextureLayer createWWComputableTextureLayer(String name,
			Function<ComputableTextureParameters, IWWTextureData> textureData,
			WWTextureDisplayParameters displayParameters, ComputableTextureParameters defaultComputeParameters) {
		return new ComputableTextureLayer(name, textureData, displayParameters, defaultComputeParameters);
	}

	@Override
	public IWWMultiTextureLayer createWWMultiTextureLayer(String name,
			Function<String, IWWTextureData> textureDataByLayer,
			Map<String, WWTextureDisplayParameters> textureDisplayParametersByLayer) {
		return new MultiTextureLayer(name, textureDataByLayer, textureDisplayParametersByLayer);
	}

	@Override
	public IWWPlayableTextureLayer createWWPlayableTextureLayer(
			WWPlayableTextureInitParameters playableTextureInitParameters,
			IntFunction<IWWTextureData> textureDataByIndex, WWTextureDisplayParameters displayParameters) {
		return new PlayableTextureLayer(playableTextureInitParameters, textureDataByIndex, displayParameters);
	}

	@Override
	public IWWPlayableMultiTextureLayer createWWPlayableMultiTextureLayer(String name, int sliceCount,
			BiFunction<String, Integer, IWWTextureData> textureDataByLayerAndIndex,
			Map<String, WWTextureDisplayParameters> textureDisplayParametersByLayer) {
		return new PlayableMultiTextureLayer(name, sliceCount, textureDataByLayerAndIndex,
				textureDisplayParametersByLayer);
	}

	/* @formatter:off
		_  _ ____ ____ ___ ____ ____ _ ____ _       _    ____ _   _ ____ ____ ____
		|  | |___ |     |  |  | |__/ | |__| |       |    |__|  \_/  |___ |__/ [__
		 \/  |___ |___  |  |__| |  \ | |  | |___    |___ |  |   |   |___ |  \ ___]

	@formatter:on */

	@Override
	public IWWNavigationLayer createNavigationLayer(INavigationDataSupplier navigationDataSupplier) {
		return createNavigationLayer(navigationDataSupplier, new WWNavigationLayerParameters());
	}

	@Override
	public IWWNavigationLayer createNavigationLayer(INavigationDataSupplier navigationDataSupplier,
			WWNavigationLayerParameters parameters) {
		return new WWNavigationLayer(navigationDataSupplier, parameters);
	}

	@Override
	public IWWIsobathLayer createIsobathLayer(String name, GeoBox geoBox, Supplier<File> demFileSupplier) {
		return new IsobathLayer("Isobaths" + (name != null ? "_" + name : ""), geoBox, demFileSupplier,
				new IsobathLayerFiller(arrayFactory, true));
	}

	/** {@inheritDoc} */
	@Override
	public IWWSwathSelectionLayer createSwathLayer(String name, ProjectionSettings projection,
			int swathCountInSelection, GeoBox geoBox) {
		return new SwathSelectionLayer(name, projection, swathCountInSelection, geoBox);
	}

	@Override
	public IWWShapefileLayer createShapefileLayer(IWWShapefileLayer.Data data) {
		ShpLayerFactory factory = new ShpLayerFactory();
		IWWShapefileLayer layer = factory.createFromShapefile(data);
		layer.setName(data.getName());

		ModelSessionBeanWrapper sessionBean = new ModelSessionBeanWrapper(((ShpLayer) layer).getDisplayParameters());
		ISessionService.grab().getPropertiesContainer().monitor(data.getShapeFile().getPath(), "Shp_Model", sessionBean,
				sessionBean.getPropertyChangeSupport());

		return layer;
	}

	@Override
	public IWWGeoBoxLayer createGeoBoxLayer(GeoBox geoBox) {
		return new GeoBoxLayer(new GeoBoxLayerModel(geoBox));
	}

	@Override
	public <T extends IMarker> IWWMarkerLayer createMarkerLayer(IMarkerFileInfo<T> markerFileInfo) {
		return new WWMarkerLayer<T>(markerFileInfo);
	}

	@Override
	public IWWMarkerLayer createWaterColumnMarkerLayer(IMarkerFileInfo<IWCMarker> markerFileInfo) {
		return new WWWaterColumnMarkerLayer(markerFileInfo);
	}

	@Override
	public IWWPointCloudLayer createPointCloudLayer(IPointCloudFileInfo pointCloudFileInfo,
			PointCloudFieldInfo fieldInfo) {
		return new PointCloudLayer(pointCloudFileInfo, fieldInfo);
	}

	@Override
	public IWWPolygonLayer createPolygonLayer(PolygonFileInfo polygon) {
		PolygonLayer result = new PolygonLayer(polygon);
		ContextInitializer.inject(result);
		return result;
	}

	@Override
	public IWWCurrentLayer createCurrentLayer(ICurrentData currentData) {
		WWCurrentLayer result = new WWCurrentLayer(currentData);
		ContextInitializer.inject(result);
		return result;
	}

	@Override
	public IWWAnnotationLayer createAnnotationLayer(IFileInfo fileInfo) {
		AnnotationLayer result = new AnnotationLayer(fileInfo);
		ContextInitializer.inject(result);
		return result;
	}

	@Override
	public IWWLabelLayer createLabelLayer(IFileInfo fileInfo) {
		LabelsLayer result = new LabelsLayer("Labels of " + fileInfo.getPath());
		ContextInitializer.inject(result);
		return result;
	}

	@Override
	public IWWUsblLayer createUsblTargetLayer(UsblFileInfo usblFileInfo) {
		var result = new UsblLayer(usblFileInfo);
		ContextInitializer.inject(result);
		return result;
	}

	/* @formatter:off
	____ ____ _  _ ____ ____ _ ____    _    ____ _   _ ____ ____
	| __ |___ |\ | |___ |__/ | |       |    |__|  \_/  |___ |__/
	|__] |___ | \| |___ |  \ | |___    |___ |  |   |   |___ |  \

	@formatter:on */

	/** {@inheritDoc} */
	@Override
	public IWWRenderableLayer createRenderableLayer() {
		return new WWRenderableLayer();
	}

	/** {@inheritDoc} */
	@Override
	public IWWQuadSelectionLayer createQuadSelectionLayer(Projection projection, Color selectionColor,
			int selectionLineWidth) {
		var result = new QuadSelectionLayer(projection);
		result.setSelectionColor(selectionColor);
		result.setSelectionLineWidth(selectionLineWidth);
		return result;
	}

}
