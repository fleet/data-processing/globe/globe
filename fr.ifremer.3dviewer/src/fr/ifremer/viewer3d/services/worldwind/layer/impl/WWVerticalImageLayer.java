package fr.ifremer.viewer3d.services.worldwind.layer.impl;

import java.beans.PropertyChangeEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWVerticalImageLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.model.PathWallLines;
import fr.ifremer.viewer3d.render.SimplePathWall;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Layer to render a vertical image.
 * 
 * @deprecated use {@link IWWTextureLayer} instead.
 */
@Deprecated
public class WWVerticalImageLayer extends MyAbstractLayer implements ITarget, IWWVerticalImageLayer {

	/** Logger */
	protected Logger logger = LoggerFactory.getLogger(WWVerticalImageLayer.class);

	/** Vertical texture. */
	protected final SimplePathWall texture;

	/** Sector for this layer location. */
	protected Sector sector = null;

	/**
	 * Constructor.
	 */
	public WWVerticalImageLayer(IWWVerticalImageLayer.Data data) {
		setName(data.getName());

		sector = Sector.boundingSector(data.getTopPoints());

		// colormap
		listerColorMap(false);
		setUseColorMap(true);
		setColorMapOri(2);
		setColorMap(2);

		setMinTextureValue(data.getMinMaxValues().getMinimumDouble());
		setMaxTextureValue(data.getMinMaxValues().getMaximumDouble());

		texture = new SimplePathWall(data.getImage(), new PathWallLines(data.getTopPoints(), data.getBottomPoints()),
				this, true);
	}

	/** {@inheritDoc} */
	@Override
	public void setMovement(ISectorMovementProcessing movement) {
		Object oldValue = this.movement;
		super.setMovement(movement);
		firePropertyChange(new PropertyChangeEvent(this, "movement", oldValue, movement));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.viewer3d.layers.MyAbstractLayer#setMaxContrast(double)
	 */
	@Override
	public void setMaxContrast(double maxContrast) {
		double value = 255 * (maxContrast - this.getMinTextureValue())
				/ (this.getMaxTextureValue() - this.getMinTextureValue());
		super.setMaxContrast(value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.viewer3d.layers.MyAbstractLayer#setMinContrast(double)
	 */
	@Override
	public void setMinContrast(double minContrast) {
		double value = 255 * (minContrast - this.getMinTextureValue())
				/ (this.getMaxTextureValue() - this.getMinTextureValue());
		super.setMinContrast(value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.viewer3d.layers.MyAbstractLayer#setMaxTransparency(double)
	 */
	@Override
	public void setMaxTransparency(double maxTransparency) {
		double value = 255 * (maxTransparency - this.getMinTextureValue())
				/ (this.getMaxTextureValue() - this.getMinTextureValue());
		super.setMaxTransparency(value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.viewer3d.layers.MyAbstractLayer#setMinTransparency(double)
	 */
	@Override
	public void setMinTransparency(double minTransparency) {
		double value = 255 * (minTransparency - this.getMinTextureValue())
				/ (this.getMaxTextureValue() - this.getMinTextureValue());
		super.setMinTransparency(value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.ui.service.worldwind.ITarget#getSector()
	 */
	@Override
	public Sector getSector() {
		return sector;
	}

	/**
	 * Follow the link.
	 *
	 * @see gov.nasa.worldwind.layers.AbstractLayer#doRender(gov.nasa.worldwind.render.DrawContext)
	 */
	@Override
	protected void doRender(DrawContext dc) {
		if (dc != null && texture != null) {
			if (sector.intersects(dc.getVisibleSector())) {
				//setValue(SSVAVKey.CURRENTLY_VISIBLE, true);
				texture.updateEyeDistance(dc);
				dc.addOrderedRenderable(texture);
			} else {
				//setValue(SSVAVKey.CURRENTLY_VISIBLE, false);
			}
		}
	}
}
