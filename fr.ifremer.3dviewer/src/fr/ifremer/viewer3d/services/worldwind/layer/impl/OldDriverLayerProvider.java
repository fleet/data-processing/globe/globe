/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.services.worldwind.layer.impl;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWwLayerOperation;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.deprecated.BridgeFileInfo;
import fr.ifremer.viewer3d.deprecated.DataDriverRegistry;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.event.Message;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.render.DrawContext;

/**
 * Bridge to consider old IDataDriver as IWWLayerProvider
 */
@Component(name = "globe_model_service_driver_layer_provider", service = IWWLayerProvider.class)
public class OldDriverLayerProvider implements IWWLayerProvider {

	@Reference
	protected DataDriverRegistry dataDriverRegistry;
	
	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** Delegated method */
	@Override
	public Set<ContentType> getContentType() {
		return dataDriverRegistry.getContentTypes();
	}

	/** Delegated method */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof BridgeFileInfo) {
			BridgeFileInfo bridgeFileInfo = (BridgeFileInfo) fileInfo;
			List<Pair<IWWLayer, ElevationModel>> layerPairs = createLayers(bridgeFileInfo, monitor);
			List<IWWLayer> layers = layerPairs.stream().map(Pair::getFirst).collect(Collectors.toList());
			ElevationModel elevationModel = layerPairs.stream().map(Pair::getSecond).filter(Objects::nonNull)
					.findFirst().orElse(null);

			result = new WWFileLayerStore(bridgeFileInfo);
			result.addLayers(layers);
			result.setElevationModel(elevationModel);
		}
		return Optional.ofNullable(result);
	}

	private List<Pair<IWWLayer, ElevationModel>> createLayers(BridgeFileInfo bridgeFileInfo, IProgressMonitor monitor) {
		List<Pair<IWWLayer, ElevationModel>> result = new ArrayList<>();

		List<Pair<Layer, ElevationModel>> layers = null;
		if (bridgeFileInfo.getDriver() != null) {
			try {
				Method loadMethod = bridgeFileInfo.getDriver().getClass().getDeclaredMethod("load", File.class,
						IProgressMonitor.class);
				layers = (List<Pair<Layer, ElevationModel>>) loadMethod.invoke(bridgeFileInfo.getDriver(),
						bridgeFileInfo.toFile(), monitor);
			} catch (Exception e) {
				e.printStackTrace();
				// Unable to access to load method
			}

			if (layers != null) {
				for (Pair<Layer, ElevationModel> pair : layers) {
					if (pair.getFirst() instanceof IWWLayer) {
						result.add(new Pair<>((IWWLayer) pair.getFirst(), pair.getSecond()));
					} else {
						result.add(new Pair<>(new OldLayerAdapter(pair.getFirst()) {
						}, pair.getSecond()));
					}
				}
			}
		}

		return result;
	}

	public static class OldLayerAdapter implements IWWLayer, ITarget {
		protected Layer wrappedLayer;

		/**
		 * Constructor
		 */
		public OldLayerAdapter(Layer wrappedLayer) {
			this.wrappedLayer = wrappedLayer;
		}

		/** Delegated method */
		@Override
		public void dispose() {
			wrappedLayer.dispose();
		}

		/** Delegated method */
		@Override
		public void onMessage(Message msg) {
			wrappedLayer.onMessage(msg);
		}

		/** Delegated method */
		@Override
		public Object setValue(String key, Object value) {
			return wrappedLayer.setValue(key, value);
		}

		/** Delegated method */
		@Override
		public boolean isEnabled() {
			return wrappedLayer.isEnabled();
		}

		/** Delegated method */
		@Override
		public void setEnabled(boolean enabled) {
			wrappedLayer.setEnabled(enabled);
		}

		/** Delegated method */
		@Override
		public String getName() {
			return wrappedLayer.getName();
		}

		/** Delegated method */
		@Override
		public void setName(String name) {
			wrappedLayer.setName(name);
		}

		/** Delegated method */
		@Override
		public AVList setValues(AVList avList) {
			return wrappedLayer.setValues(avList);
		}

		/** Delegated method */
		@Override
		public String getRestorableState() {
			return wrappedLayer.getRestorableState();
		}

		/** Delegated method */
		@Override
		public double getOpacity() {
			return wrappedLayer.getOpacity();
		}

		/** Delegated method */
		@Override
		public void restoreState(String stateInXml) {
			wrappedLayer.restoreState(stateInXml);
		}

		/** Delegated method */
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			wrappedLayer.propertyChange(evt);
		}

		/** Delegated method */
		@Override
		public Object getValue(String key) {
			return wrappedLayer.getValue(key);
		}

		/** Delegated method */
		@Override
		public void setOpacity(double opacity) {
			wrappedLayer.setOpacity(opacity);
		}

		/** Delegated method */
		@Override
		public Collection<Object> getValues() {
			return wrappedLayer.getValues();
		}

		/** Delegated method */
		@Override
		public String getStringValue(String key) {
			return wrappedLayer.getStringValue(key);
		}

		/** Delegated method */
		@Override
		public boolean isPickEnabled() {
			return wrappedLayer.isPickEnabled();
		}

		/** Delegated method */
		@Override
		public Set<Entry<String, Object>> getEntries() {
			return wrappedLayer.getEntries();
		}

		/** Delegated method */
		@Override
		public boolean hasKey(String key) {
			return wrappedLayer.hasKey(key);
		}

		/** Delegated method */
		@Override
		public Object removeKey(String key) {
			return wrappedLayer.removeKey(key);
		}

		/** Delegated method */
		@Override
		public void setPickEnabled(boolean isPickable) {
			wrappedLayer.setPickEnabled(isPickable);
		}

		/** Delegated method */
		@Override
		public void preRender(DrawContext dc) {
			wrappedLayer.preRender(dc);
		}

		/** Delegated method */
		@Override
		public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
			wrappedLayer.addPropertyChangeListener(propertyName, listener);
		}

		/** Delegated method */
		@Override
		public void render(DrawContext dc) {
			wrappedLayer.render(dc);
		}

		/** Delegated method */
		@Override
		public void pick(DrawContext dc, Point pickPoint) {
			wrappedLayer.pick(dc, pickPoint);
		}

		/** Delegated method */
		@Override
		public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
			wrappedLayer.removePropertyChangeListener(propertyName, listener);
		}

		/** Delegated method */
		@Override
		public void addPropertyChangeListener(PropertyChangeListener listener) {
			wrappedLayer.addPropertyChangeListener(listener);
		}

		/** Delegated method */
		@Override
		public boolean isAtMaxResolution() {
			return wrappedLayer.isAtMaxResolution();
		}

		/** Delegated method */
		@Override
		public void removePropertyChangeListener(PropertyChangeListener listener) {
			wrappedLayer.removePropertyChangeListener(listener);
		}

		/** Delegated method */
		@Override
		public boolean isMultiResolution() {
			return wrappedLayer.isMultiResolution();
		}

		/** Delegated method */
		@Override
		public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
			wrappedLayer.firePropertyChange(propertyName, oldValue, newValue);
		}

		/** Delegated method */
		@Override
		public double getScale() {
			return wrappedLayer.getScale();
		}

		/** Delegated method */
		@Override
		public boolean isNetworkRetrievalEnabled() {
			return wrappedLayer.isNetworkRetrievalEnabled();
		}

		/** Delegated method */
		@Override
		public void setNetworkRetrievalEnabled(boolean networkRetrievalEnabled) {
			wrappedLayer.setNetworkRetrievalEnabled(networkRetrievalEnabled);
		}

		/** Delegated method */
		@Override
		public void firePropertyChange(PropertyChangeEvent propertyChangeEvent) {
			wrappedLayer.firePropertyChange(propertyChangeEvent);
		}

		/** Delegated method */
		@Override
		public AVList copy() {
			return wrappedLayer.copy();
		}

		/** Delegated method */
		@Override
		public void setExpiryTime(long expiryTime) {
			wrappedLayer.setExpiryTime(expiryTime);
		}

		/** Delegated method */
		@Override
		public AVList clearList() {
			return wrappedLayer.clearList();
		}

		/** Delegated method */
		@Override
		public long getExpiryTime() {
			return wrappedLayer.getExpiryTime();
		}

		/** Delegated method */
		@Override
		public double getMinActiveAltitude() {
			return wrappedLayer.getMinActiveAltitude();
		}

		/** Delegated method */
		@Override
		public void setMinActiveAltitude(double minActiveAltitude) {
			wrappedLayer.setMinActiveAltitude(minActiveAltitude);
		}

		/** Delegated method */
		@Override
		public double getMaxActiveAltitude() {
			return wrappedLayer.getMaxActiveAltitude();
		}

		/** Delegated method */
		@Override
		public void setMaxActiveAltitude(double maxActiveAltitude) {
			wrappedLayer.setMaxActiveAltitude(maxActiveAltitude);
		}

		/** Delegated method */
		@Override
		public boolean isLayerInView(DrawContext dc) {
			return wrappedLayer.isLayerInView(dc);
		}

		/** Delegated method */
		@Override
		public boolean isLayerActive(DrawContext dc) {
			return wrappedLayer.isLayerActive(dc);
		}

		/** Delegated method */
		@Override
		public Double getMaxEffectiveAltitude(Double radius) {
			return wrappedLayer.getMaxEffectiveAltitude(radius);
		}

		/** Delegated method */
		@Override
		public Double getMinEffectiveAltitude(Double radius) {
			return wrappedLayer.getMinEffectiveAltitude(radius);
		}

		/** {@inheritDoc} */
		@Override
		public <T extends IWWLayerOperation> T perform(T operation) {
			if (operation instanceof BasicWwLayerOperation) {
				((BasicWwLayerOperation) operation).performDefaultOperation(this);
			}
			return operation;
		}

		/** {@inheritDoc} */
		@Override
		public Sector getSector() {
			if (wrappedLayer instanceof ITarget) {
				return ((ITarget) wrappedLayer).getSector();
			}
			return null;
		}
	}
}
