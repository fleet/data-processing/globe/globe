/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.services.worldwind.layer.impl;

import org.w3c.dom.Document;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWTiledImageLayer;
import fr.ifremer.viewer3d.layers.ILayerParameters;
import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.layers.VisibleLayerParameter;
import gov.nasa.worldwind.layers.BasicTiledImageLayer;

/**
 * Decorator of BasicTiledImageLayer
 */
class WWTiledImageLayer extends BasicTiledImageLayer implements IWWTiledImageLayer, ILayerParameters {

	/** Layer visible or not */
	protected VisibleLayerParameter layerparameters = new VisibleLayerParameter();

	/**
	 * Constructor
	 */
	public WWTiledImageLayer(Document document) {
		super(document, null);
	}

	/** {@inheritDoc} */
	@Override
	public LayerParameters getLayerParameters() {
		return layerparameters;
	}

	/** {@inheritDoc} */
	@Override
	public void setLayerParameters(LayerParameters arg) {
		if (arg instanceof VisibleLayerParameter) {
			layerparameters.load((VisibleLayerParameter) arg);
		}
	}

}
