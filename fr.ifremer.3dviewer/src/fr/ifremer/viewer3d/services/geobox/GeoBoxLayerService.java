/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.services.geobox;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerModel;
import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerService;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.gui.actions.GoToActionUtil;
import fr.ifremer.viewer3d.layers.geobox.GeoBoxLayer;
import fr.ifremer.viewer3d.layers.geobox.GeoBoxLayerModel;

/**
 * Implementation of IGeoBoxLayerService
 */
@Component(name = "globe_viewer3d_geobox_layer_service", service = IGeoBoxLayerService.class)
public class GeoBoxLayerService implements IGeoBoxLayerService {

	Map<IGeoBoxLayerModel, GeoBoxLayer> cache = new HashMap<>();

	/** {@inheritDoc} */
	@Override
	public IGeoBoxLayerModel createLayer() {
		GeoBoxLayerModel model = new GeoBoxLayerModel();
		GeoBoxLayer layer = new GeoBoxLayer(model);
		Viewer3D.getModel().getLayers().add(layer);
		cache.put(model, layer);
		return model;
	}

	/** {@inheritDoc} */
	@Override
	public void removeLayer(IGeoBoxLayerModel model) {
		GeoBoxLayer layer = cache.remove(model);
		if (layer != null) {
			Viewer3D.getModel().getLayers().remove(layer);
			layer.dispose();
		}
	}

	/** {@inheritDoc} */
	@Override
	public void revealLayer(IGeoBoxLayerModel model) {
		if (model != null && model.getGeoBox().isNotNull()) {
			GoToActionUtil.zoomToSector(GeoBoxUtils.convert(model.getGeoBox().get()));
		}
	}
}
