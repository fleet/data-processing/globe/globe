/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.services.geograhicview;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.ExtensionParameters;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.application.context.ContextInitializer;
import fr.ifremer.viewer3d.elevation.RootElevationModel;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import jakarta.inject.Inject;

/**
 * Implementation of IGeographicViewService
 */
@Component(name = "globe_viewer3d_geographic_view_service", service = IGeographicViewService.class)
public class GeographicViewService implements IGeographicViewService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GeographicViewService.class);

	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Invoke a refresh when the property changed */
	protected PropertyChangeListener refreshListener = evt -> refresh();

	/**
	 * Ordered list of layer to load later.... Key=Layer name, value = Layer enabled
	 */
	protected List<Pair<String, Boolean>> predictedLayers = Collections.emptyList();

	/** Placemmarks layer **/
	private RenderableLayer placemarkLayer;

	/** marker flag **/
	private boolean markersEnabled = false;

	/** tooltip flag **/
	private boolean tooltipEnabled = false;

	/** Synchronization with selection **/
	private boolean isSynchWithSelection = false;

	private WorldWindowGLCanvas worldWindowGLCanvas;

	/**
	 * Constructor
	 */
	public GeographicViewService() {
		ContextInitializer.inject(this);

		// observe fileLayerStoreModel
		fileLayerStoreModel.addListener(this::onFileLayerStoreEvent);
	}

	/** React on a layer model changes */
	private void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		switch (event.getState()) {
		case ADDED -> onLayersImported(event.getFileLayerStore().getLayers(),
				event.getFileLayerStore().getElevationModel());
		case UPDATED -> Optional.ofNullable(event.getUpdatedLayers())
				.ifPresent(updatedLayers -> onLayersImported(updatedLayers, event.getElevationModel()));
		case REMOVED -> onFileLayerStoreRemoved(event.getFileLayerStore());
		}
	}

	/** WW layers have been created. Add them to the model */
	private void onLayersImported(List<IWWLayer> layers, ElevationModel elevationModel) {
		Optional<WWFileLayerStore> storeWithBetterResolution = Optional.empty();

		if (elevationModel != null) {
			var rootElevationModel = getElevationModel();

			// first, check if a WWFileLayerStore with better resolution exists
			double newModelResolution = elevationModel.getBestResolution(null);
			double closestModelResolution = Double.NaN;
			for (var fileLayerStore : fileLayerStoreModel.getAll()) {
				if (fileLayerStore.getElevationModel() != null) {
					var resolution = fileLayerStore.getElevationModel().getBestResolution(null);
					// search for the lower resolution better than the new model resolution
					if (resolution < newModelResolution
							&& (Double.isNaN(closestModelResolution) || resolution > closestModelResolution)) {
						closestModelResolution = resolution;
						storeWithBetterResolution = Optional.of(fileLayerStore);
					}
				}
			}

			// add elevation model
			if (storeWithBetterResolution.isPresent()) {
				// if there is a store with better resolution, the new elevation model is added just behind
				var elevationModels = rootElevationModel.getElevationModels();
				var index = elevationModels.indexOf(storeWithBetterResolution.get().getElevationModel());
				if (index != -1) {
					LOGGER.debug("Add elevation model at index : " + index + "/" + elevationModels.size());
					rootElevationModel.addElevationModel(index, elevationModel);
				}
			} else {
				rootElevationModel.addElevationModel(elevationModel);
			}
		}

		// add layers
		for (var layer : layers) {
			if (storeWithBetterResolution.isPresent()) {
				// if there is a store with better resolution, the new layer is added just behind
				var index = storeWithBetterResolution.get().getLayers().stream().mapToInt(getLayers()::indexOf).min()
						.orElse(-1);
				replaceLayer(layer, index);
			} else {
				replaceLayer(layer);
			}
		}

		refresh();
	}

	/** Some layers have been removed */
	private void onFileLayerStoreRemoved(WWFileLayerStore fileLayerStore) {
		fileLayerStore.getLayers().forEach(this::removeLayer);
		if (fileLayerStore.getElevationModel() != null)
			removeElevationModel(fileLayerStore.getElevationModel());
	}

	/** {@inheritDoc} */
	@Override
	public WorldWindowGLCanvas getWwd() {
		if (worldWindowGLCanvas == null)
			worldWindowGLCanvas = Viewer3D.getWwd();
		return worldWindowGLCanvas;
	}

	@Override
	public void refresh() {
		Viewer3D.refreshRequired();
	}

	/** {@inheritDoc} */
	@Override
	public LayerList getLayers() {
		// Viewer3D not available in unit tests : return empty LayerList
		return Viewer3D.getCurrentViewer3D() != null ? Viewer3D.getModel().getLayers() : new LayerList();
	}

	/** {@inheritDoc} */
	@Override
	public void setPredictedLayers(List<Pair<String, Boolean>> predictedLayers) {
		this.predictedLayers = new ArrayList<>(predictedLayers);

		for (Pair<String, Boolean> predictedLayer : predictedLayers) {
			LayerList layers = getLayers();
			Layer existingLayer = layers.getLayerByName(predictedLayer.getFirst());
			if (existingLayer != null) {
				// Layer already exists : manage the enable flag
				existingLayer.setEnabled(predictedLayer.getSecond());
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void unPredictedLayer(String layerName) {
		int predicatedLayerIndex = indexOfPredictedLayers(layerName);
		if (predicatedLayerIndex >= 0) {
			predictedLayers.remove(predicatedLayerIndex);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void addLayer(Layer layer) {
		if (layer != null && getWwd().getModel().getLayers().addIfAbsent(layer)) {
			hookLayerListener(layer);
		}
	}

	@Override
	public void addLayerBeforeCompass(Layer layer) {
		Viewer3D.getCurrentViewer3D().insertBeforeCompass(layer);
	}

	/** {@inheritDoc} */
	@Override
	public boolean hasLayer(String layerName) {
		LayerList displayedLayers = getWwd().getModel().getLayers();
		return displayedLayers.getLayerByName(layerName) != null;
	}

	@Override
	public void replaceLayer(Layer layer) {
		replaceLayer(layer, -1);
	}

	/** {@inheritDoc} */
	public void replaceLayer(Layer layer, int inputIndex) {
		if (layer != null) {
			hookLayerListener(layer);

			// Same layer exists ? Replace it if true
			LayerList displayedLayers = getWwd().getModel().getLayers();
			Layer existingLayer = displayedLayers.getLayerByName(layer.getName());
			if (existingLayer != null) {
				int layerIndex = displayedLayers.indexOf(existingLayer);
				if (layerIndex >= 0) {
					displayedLayers.set(layerIndex, layer);
					removeLayer(existingLayer);
					return;
				}
			}

			// Is it a predicted layer ?
			int predicatedLayerIndex = indexOfPredictedLayers(layer.getName());
			// Restore the enable flag
			if (predicatedLayerIndex >= 0)
				layer.setEnabled(predictedLayers.get(predicatedLayerIndex).getSecond());

			// search one of the previous predicated layers among the current displayed layers
			while (predicatedLayerIndex > 0) {
				predicatedLayerIndex--;
				Layer previousPredicatedLayer = displayedLayers
						.getLayerByName(predictedLayers.get(predicatedLayerIndex).getFirst());
				if (previousPredicatedLayer != null) {
					// The previous predicated layer is actually displayed
					// Insert the new layer at the next position
					int insertIndex = displayedLayers.indexOf(previousPredicatedLayer) + 1;
					if (insertIndex < displayedLayers.size()) {
						displayedLayers.add(displayedLayers.indexOf(previousPredicatedLayer) + 1, layer);
					} else {
						displayedLayers.add(layer);
					}
					return; // Job done
				}
			}

			// use input index, or by default insert on top
			int insertIndex = inputIndex != -1 ? inputIndex : displayedLayers.size();

			// IWWSwathLayer always of top
			while (displayedLayers.get(insertIndex - 1) instanceof IWWSwathSelectionLayer)
				insertIndex--;

			LOGGER.debug("Add layer " + layer.getName() + " at index : " + insertIndex + "/" + displayedLayers.size());
			displayedLayers.add(insertIndex, layer);
		}
	}

	protected int indexOfPredictedLayers(String layerName) {
		int result = 0;
		for (Pair<String, Boolean> pair : predictedLayers) {
			if (pair.getFirst().equals(layerName)) {
				return result;
			}
			result++;
		}
		return -1;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isPredictedLayer(String layerName) {
		return indexOfPredictedLayers(layerName) >= 0;
	}

	/** {@inheritDoc} */
	@Override
	public void removeLayer(Layer layer) {
		if (layer != null) {
			unhookLayerListener(layer);
			unPredictedLayer(layer.getName());
			if (getWwd() != null && getWwd().getModel() != null)
				getWwd().getModel().getLayers().remove(layer);
			layer.dispose();
		}
	}

	@Override
	public RootElevationModel getElevationModel() {
		return (RootElevationModel) getWwd() //
				.getModel() // fr.ifremer.viewer3d.BathyscopeModel
				.getGlobe() // fr.ifremer.viewer3d.Earth
				.getElevationModel(); // fr.ifremer.viewer3d.elevation.RootElevationModel
	}

	/** {@inheritDoc} */
	@Override
	public void addElevationModel(ElevationModel elevationModel) {
		if (elevationModel != null)
			getElevationModel().addElevationModel(elevationModel);
	}

	/** {@inheritDoc} */
	@Override
	public void removeElevationModel(ElevationModel elevationModel) {
		if (elevationModel != null) {
			getElevationModel().removeElevationModel(elevationModel);
			elevationModel.dispose();
		}
	}

	protected void hookLayerListener(Layer layer) {
		layer.addPropertyChangeListener("Enabled", refreshListener);
	}

	protected void unhookLayerListener(Layer layer) {
		layer.removePropertyChangeListener("Enabled", refreshListener);
	}

	@Override
	public void waitForClickByUser(Consumer<Position> resultConsumer) {
		worldWindowGLCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		worldWindowGLCanvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				worldWindowGLCanvas.removeMouseListener(this);
				worldWindowGLCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				resultConsumer.accept(worldWindowGLCanvas.getCurrentPosition());
			}
		});
	}

	@Override
	public PointPlacemark addPlacemark(Position position, String label) {
		if (placemarkLayer == null) {
			placemarkLayer = new RenderableLayer();
			addLayer(placemarkLayer);
		}
		var pointPlacemark = new PointPlacemark(position);
		pointPlacemark.setLabelText(label);
		placemarkLayer.addRenderable(pointPlacemark);
		return pointPlacemark;
	}

	@Override
	public void removePlacemark(PointPlacemark placemark) {
		placemarkLayer.removeRenderable(placemark);
	}

	@Override
	public void zoomTo(LatLon latlon) {
		zoomToSector(Sector.boundingSector(List.of(latlon)), 0f);
	}

	@Override
	public void zoomToGeobox(GeoBox geoBox) {
		zoomToSector(GeoBoxUtils.convert(geoBox), 0f);
	}

	@Override
	public void zoomToGeobox(List<GeoBox> geoBoxes) {
		zoomToSector(geoBoxes.stream().map(GeoBoxUtils::convert).toList(), 0f);
	}

	@Override
	public void zoomToSector(List<Sector> sectors, float elevation) {
		if (sectors.size() == 1) {
			zoomToSector(sectors.get(0), elevation);

		} else if (!sectors.isEmpty()) {
			zoomToSector(GeoBoxUtils.union(sectors), elevation);
		}
	}

	@Override
	public void zoomToSector(Sector sector, float elevation) {
		if (getWwd().getView() instanceof BasicOrbitView view) {
			view.stopAnimations();
			LatLon[] corners = sector.getCorners();

			// BasicView.fieldOfView is set to 45°.
			// So you need to go up twice the width of the sector to be visualized.
			double dist = 2 * LatLon.ellipsoidalDistance(corners[0], corners[2], Earth.WGS84_EQUATORIAL_RADIUS,
					Earth.WGS84_POLAR_RADIUS);
			double zoomElevation = (!Double.isNaN(dist) ? dist : 1000);

			var position = new Position(GeoBoxUtils.getCentroid(sector), elevation);
			view.addPanToAnimator(position, //
					elevation == 0f ? view.getHeading() : Angle.ZERO, //
					elevation == 0f ? view.getPitch() : Angle.ZERO, //
					zoomElevation);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void setExtraStatusBarInfo(String info) {
		Viewer3D.getCurrentViewer3D().setExtraStatusBarLabel(info);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ExtensionParameters> getExtensionParameters(ElevationModel elevationModel) {
		return getElevationModel().getElevationParameters(elevationModel);
	}

	/** {@inheritDoc} */
	@Override
	public void applyExtensionParameters(ElevationModel elevationModel, ExtensionParameters flatteningParameters) {
		getElevationModel().applyElevationParameters(elevationModel, flatteningParameters);
	}

	/** {@inheritDoc} */
	@Override
	public void setMarkersEnabled(boolean enable) {
		markersEnabled = enable;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isMarkersEnabled() {
		return markersEnabled;
	}

	/** {@inheritDoc} */
	@Override
	public void setTooltipEnabled(boolean enable) {
		tooltipEnabled = enable;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isTooltipEnabled() {
		return tooltipEnabled;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isDetailPickEnabled() {
		return markersEnabled || tooltipEnabled;
	}

	/** {@inheritDoc} */
	@Override
	public void requestFocus() {
		Runnable runnable = () -> {
			var partService = ContextInitializer.getInContext(EPartService.class);
			partService.activate(partService.findPart(Viewer3D.PART_ID), true);
		};
		if (Display.getCurrent() != null) {
			runnable.run();
		} else {
			Display.getDefault().asyncExec(runnable);
		}
	}

	@Override
	public void moveLayerToBackground(IWWLayer layer) {
		var layerList = getLayers();
		if (!layerList.contains(layer))
			return;
		// find first IWWLayer index, and move layer at this index
		layerList.stream().filter(IWWLayer.class::isInstance).findFirst().map(layerList::indexOf)
				.ifPresent(destinationIndex -> {
					layerList.remove(layer);
					layerList.add(destinationIndex, layer);
				});
	}

	@Override
	public void moveLayerToFront(IWWLayer layer) {
		var layerList = getLayers();
		if (!layerList.contains(layer))
			return;
		layerList.remove(layer);
		layerList.add(layer);
	}

	@Override
	public void moveElevationToBack(ElevationModel layer) {
		var elevationModel = getElevationModel();
		elevationModel.removeElevationModel(layer);
		elevationModel.addElevationModel(0, layer);
	}

	@Override
	public void moveElevationToFront(ElevationModel layer) {
		var elevationModel = getElevationModel();
		elevationModel.removeElevationModel(layer);
		elevationModel.addElevationModel(layer);
	}

	@Override
	public void activatePerformance() {
		Viewer3D.getCurrentViewer3D().startPerformanceRecord();
	}

	@Override
	public boolean isInterpolate() {
		return Viewer3D.isInterpolate();
	}

	@Override
	public void interpolateTerrain() {
		Viewer3D.interpolateTerrain();
	}

	@Override
	public boolean isHighResolution() {
		return Viewer3D.getCurrentViewer3D().isHighResolution();
	}

	@Override
	public void setHighResolution() {
		Viewer3D.getCurrentViewer3D().setHighResolution();
	}

	@Override
	public boolean isSyncViewSelection() {
		return isSynchWithSelection;
	}

	@Override
	public void setSyncViewSelection(boolean b) {
		this.isSynchWithSelection = b;
	}

}
