/**
 * GLOBE - Ifremer
 */
package fr.ifremer.viewer3d.services;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModel;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.viewer3d.terrain.CompoundInterpolateElevationModel;
import fr.ifremer.viewer3d.terrain.InterpolateElevationModel;
import fr.ifremer.viewer3d.terrain.MovableInterpolateElevationModel;
import fr.ifremer.viewer3d.util.conf.SSVConfiguration;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.terrain.BasicElevationModel;

/**
 * Implementation of IWWLayerFactory
 */
@Component(name = "globe_viewer3d_elevation_model_factory", service = IWWElevationModelFactory.class)
public class WWElevationModelFactory implements IWWElevationModelFactory {

	/** {@inheritDoc} */
	@Override
	public IWWElevationModel createInterpolateElevationModel(Element domElement) {
		return createInterpolateElevationModel(domElement, null);
	}

	@Override
	public IWWElevationModel createInterpolateElevationModel(Element domElement, AVList params) {
		InterpolateElevationModel result = new InterpolateElevationModel(domElement, params);
		result.setDetailHint(SSVConfiguration.getConfiguration().getProxy().getLOD());
		return result;
	}

	@Override
	public BasicElevationModel createMovableInterpolateElevationModel(Element domElement) {
		return createMovableInterpolateElevationModel(domElement, null);
	}

	@Override
	public BasicElevationModel createMovableInterpolateElevationModel(Element domElement, AVList params) {
		MovableInterpolateElevationModel result = new MovableInterpolateElevationModel(domElement, params);
		result.setDetailHint(SSVConfiguration.getConfiguration().getProxy().getLOD());
		return result;
	}

	@Override
	public IWWElevationModel createInterpolateElevationModel(String name, GeoBox geoBox, List<Document> tilesPyramids) {
		IWWElevationModel result = null;
		// Single pyramid ?
		if (tilesPyramids.size() == 1) {
			result = createInterpolateElevationModel(tilesPyramids.get(0).getDocumentElement());
		} else {
			var compoundElevationModel = new CompoundInterpolateElevationModel();
			for (Document tilesPyramid : tilesPyramids) {
				compoundElevationModel
						.addElevationModel(createInterpolateElevationModel(tilesPyramid.getDocumentElement()));
				compoundElevationModel.setName(name);
			}
			result = compoundElevationModel;
		}

		result.setValue(AVKey.SECTOR, GeoBoxUtils.convert(geoBox));
		return result;
	}
}
