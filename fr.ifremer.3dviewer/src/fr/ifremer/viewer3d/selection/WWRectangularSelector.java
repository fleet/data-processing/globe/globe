
package fr.ifremer.viewer3d.selection;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.viewer3d.render.ScreenRectangle;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.RenderableLayer;

/**
 * Provides interactive screen rectangle selection with visual feedback, and tracks the list of objects intersecting the
 * screen rectangle. <br>
 * The screen rectangle is displayed on a layer created by WWObjectSelector, and is used as the WorldWindow's pick
 * rectangle to perform object selection. <br>
 * Objects intersecting the screen rectangle are filtered by {@link #objectPredicate} and transmited to
 * {@link #objectConsumer} when seletion is done.
 */
public class WWRectangularSelector implements MouseListener, MouseMotionListener, SelectListener {
	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(WWRectangularSelector.class);

	/** Predicate to filter expected object returned to the objectConsumer */
	private final Consumer<List<Object>> objectConsumer;
	/** Consumer waiting for the selected objects */
	private final Predicate<Object> objectPredicate;

	/** WW window initialize at the first enable */
	private WorldWindow wwd;

	/** Layer holding the rectangle */
	private final Layer layer;
	/** Renderable to draw the rectangle */
	private final ScreenRectangle screenRectangle;

	/** List of selected objects built when selection is in progress */
	private final List<Object> selectedObjects = new ArrayList<>();

	/** True when selection is in progress */
	private boolean armed;

	/**
	 * Constructor
	 * 
	 * @param objectPredicate Predicate to filter expected object returned to the objectConsumer
	 * @param objectConsumer Consumer waiting for the selected objects
	 */
	public WWRectangularSelector(Predicate<Object> objectPredicate, Consumer<List<Object>> objectConsumer) {
		this.objectPredicate = objectPredicate;
		this.objectConsumer = objectConsumer;
		layer = new RenderableLayer();
		layer.setPickEnabled(false); // The screen selector is not pickable.
		screenRectangle = new ScreenRectangle();
		((RenderableLayer) layer).addRenderable(screenRectangle);
	}

	/** Arm this selector. Start listening the mouse events and drawing the pick rectangle */
	public void enable(WorldWindow wwd) {
		this.wwd = wwd;

		// Clear any existing selection and clear set the SceneController's pick rectangle. This ensures that this
		// ScreenSelector starts with the correct state when enabled.
		screenRectangle.clearSelection();
		wwd.getSceneController().setPickRectangle(null);

		// Add and enable the layer that displays this ScreenSelector's selection rectangle.
		LayerList layers = wwd.getModel().getLayers();

		if (!layers.contains(getLayer()))
			layers.add(getLayer());

		if (!getLayer().isEnabled())
			getLayer().setEnabled(true);

		// Listen for mouse input on the WorldWindow.
		wwd.getInputHandler().addMouseListener(this);
		wwd.getInputHandler().addMouseMotionListener(this);
	}

	/** Unarm this selector. Stop listening the mouse events and drawing the pick rectangle */
	public void disable() {
		armed = false;

		// Clear the selection, clear the SceneController's pick rectangle, and stop listening for changes in the pick
		// rectangle selection. These steps should have been done when the selection ends, but do them here in case the
		// caller disables this ScreenSelector before the selection ends.
		screenRectangle.clearSelection();
		wwd.getSceneController().setPickRectangle(null);
		wwd.removeSelectListener(this);

		// Remove the layer that displays this ScreenSelector's selection rectangle.
		wwd.getModel().getLayers().remove(getLayer());

		// Stop listening for mouse input on the WorldWindow.
		wwd.getInputHandler().removeMouseListener(this);
		wwd.getInputHandler().removeMouseMotionListener(this);
	}

	/** {@inheritDoc} */
	@Override
	public void mouseClicked(MouseEvent mouseEvent) {
		// Nothing to do.
	}

	/** {@inheritDoc} */
	@Override
	public void mousePressed(MouseEvent mouseEvent) {
		if (!armed && checkInputEvent(mouseEvent)) {
			armed = true;
			selectionStarted(mouseEvent);
			mouseEvent.consume();
		}
	}

	/** {@inheritDoc} */
	@Override
	public void mouseReleased(MouseEvent mouseEvent) {
		if (armed) {
			selectionEnded();
			mouseEvent.consume();
		}
	}

	/** {@inheritDoc} */
	@Override
	public void mouseEntered(MouseEvent mouseEvent) {
		// Nothing to do.
	}

	/** {@inheritDoc} */
	@Override
	public void mouseExited(MouseEvent mouseEvent) {
		// Nothing to do.
	}

	/** {@inheritDoc} */
	@Override
	public void mouseDragged(MouseEvent mouseEvent) {
		if (armed) {
			selectionChanged(mouseEvent);
			mouseEvent.consume();
		}
	}

	@Override
	public void mouseMoved(MouseEvent mouseEvent) {
		// Nothing to do.
	}

	/** Start to manage a selection */
	private void selectionStarted(MouseEvent mouseEvent) {
		screenRectangle.startSelection(mouseEvent.getPoint());
		wwd.getSceneController().setPickRectangle(null);
		wwd.addSelectListener(this); // Listen for changes in the pick rectangle selection.
		wwd.redraw();

		// Clear the list of selected objects and send a message indicating that the user has started a selection.
		selectedObjects.clear();
	}

	/** Stop managing a selection. Send the result to the consumer */
	private void selectionEnded() {
		disable();
		screenRectangle.clearSelection();
		wwd.getSceneController().setPickRectangle(null);
		wwd.removeSelectListener(this); // Stop listening for changes the pick rectangle selection.
		wwd.redraw();
		notifySelection();
	}

	/** Stop managing a selection. Send the result to the consumer */
	private void notifySelection() {
		List<Object> filteredObjects = selectedObjects.stream().filter(objectPredicate).toList();
		objectConsumer.accept(filteredObjects);
	}

	/** Mouse moved, modifing the pick rectangle */
	protected void selectionChanged(MouseEvent mouseEvent) {
		// Limit the end point to the WorldWindow's viewport rectangle. This ensures that a user drag event to define
		// the selection does not exceed the viewport and the viewing frustum. This is only necessary during mouse drag
		// events because those events are reported when the cursor is outside the WorldWindow's viewport.
		Point p = limitPointToWorldWindow(mouseEvent.getPoint());

		// Specify the selection's end point and set the scene controller's pick rectangle to the selected rectangle.
		// We create a copy of the selected rectangle to insulate the scene controller from changes to rectangle
		// returned by ScreenRectangle.getSelection.
		screenRectangle.endSelection(p);
		wwd.getSceneController().setPickRectangle(
				screenRectangle.hasSelection() ? new Rectangle(screenRectangle.getSelection()) : null);
		wwd.redraw();

		notifySelection();
	}

	/**
	 * Limits the specified point's x and y coordinates to the WorldWindow's viewport, and returns a new point with the
	 * limited coordinates. For example, if the WorldWindow's viewport rectangle is x=0, y=0, width=100, height=100 and
	 * the point's coordinates are x=50, y=200 this returns a new point with coordinates x=50, y=100. If the specified
	 * point is already inside the WorldWindow's viewport, this returns a new point with the same x and y coordinates as
	 * the specified point.
	 *
	 * @param point the point to limit.
	 *
	 * @return a new Point representing the specified point limited to the WorldWindow's viewport rectangle.
	 */
	protected Point limitPointToWorldWindow(Point point) {
		Rectangle viewport = wwd.getView().getViewport();

		int x = point.x;
		if (x < viewport.x)
			x = viewport.x;
		if (x > viewport.x + viewport.width)
			x = viewport.x + viewport.width;

		int y = point.y;
		if (y < viewport.y)
			y = viewport.y;
		if (y > viewport.y + viewport.height)
			y = viewport.y + viewport.height;

		return new Point(x, y);
	}

	@Override
	public void selected(SelectEvent event) {
		try {
			// Respond to box rollover select events when armed.
			if (event.getEventAction().equals(SelectEvent.BOX_ROLLOVER) && armed)
				selectObjects(event.getAllTopObjects());
		} catch (Exception e) {
			// Wrap the handler in a try/catch to keep exceptions from bubbling up
			logger.debug(e.getMessage() != null ? e.getMessage() : e.toString());
		}
	}

	protected void selectObjects(List<?> list) {
		if (selectedObjects.equals(list))
			return; // Same thing selected.

		selectedObjects.clear();

		// If the selection is empty, then we've cleared the list of selected objects and there's nothing left to do.
		// Otherwise, we add the selected objects to our list.
		if (list != null)
			selectedObjects.addAll(list);
	}

	public void setInteriorColor(Color color) {
		screenRectangle.setInteriorColor(color);
	}

	public void setBorderColor(Color color) {
		screenRectangle.setBorderColor(color);
	}

	/**
	 * @return the {@link #layer}
	 */
	public Layer getLayer() {
		return layer;
	}

	/**
	 * Check that CTRL and BUTTON1 are down, but SHIFT and ALT are up
	 * 
	 * @return true when MouseEvent is suitable for the ScreenSelector<br>
	 * 
	 */
	public static boolean checkInputEvent(InputEvent mouseEvent) {
		if (mouseEvent == null) // Ignore null events.
			return false;

		int onmask = InputEvent.CTRL_DOWN_MASK | InputEvent.BUTTON1_DOWN_MASK;
		int offmask = InputEvent.SHIFT_DOWN_MASK | InputEvent.ALT_DOWN_MASK;
		return (mouseEvent.getModifiersEx() & (onmask | offmask)) == onmask;
	}

	/**
	 * @return the {@link #armed}
	 */
	public boolean isArmed() {
		return armed;
	}

}
