package fr.ifremer.viewer3d.selection;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionService;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.event.WWLayerEvent;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.viewer3d.layers.ILayerSelected;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.layers.navigation.render.WWNavigationPath;
import fr.ifremer.viewer3d.layers.point.render.PickedPointIndex;
import fr.ifremer.viewer3d.layers.wc.render.IWCPickedObject;
import fr.ifremer.viewer3d.layers.xml.MultiTextureLayer;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.pick.PickedObjectList;

/**
 * This class manages the selection of layers.
 */
@Creatable
@Singleton
public class WWLayerSelectionController extends MouseAdapter {

	@Inject
	private IEventBroker eventBroker;

	@Inject
	private IGeographicViewService geographicViewService;

	/** List of selected layers. */
	private ConcurrentLinkedQueue<IWWLayer> selectedLayers = new ConcurrentLinkedQueue<>();

	/** Rectangular selector in WW window. */
	private final WWRectangularSelector rectangularSelector = new WWRectangularSelector(
			this::filterRectangularSelection, this::onRectangularSelection);

	/**
	 * Last selection from the RectangularSelector. Useful for ensuring that a new selection is different from the
	 * previous one.
	 */
	private IWWLayer[] lastSelectedLayers = null;

	/**
	 * Handles {@link WorldWind} selection event (from Geographic view).
	 */
	@Override
	public void mouseClicked(MouseEvent event) {

		// keep only left click
		if (event.getButton() != MouseEvent.BUTTON1 || event.isConsumed())
			return;

		PickedObjectList pickedObjects = geographicViewService.getWwd().getObjectsAtCurrentPosition();
		if (pickedObjects != null && pickedObjects.getTopPickedObject() != null) {
			var layer = pickedObjects.getTopPickedObject().getParentLayer();
			if (layer instanceof IWWLayer wwLayer) {
				// if CTRL key down : multiple selection, keep previous selection
				List<IWWLayer> layers = event.isControlDown() ? new ArrayList<>(selectedLayers) : new ArrayList<>();
				if (!layers.contains(layer))
					layers.add(wwLayer);
				else
					layers.remove(layer);
				eventBroker.post(WWLayerEvent.TOPIC_LAYER_SELECTED, layers.toArray(IWWLayer[]::new));
				return;
			}
		}

		// remove selection if click outside of any layer
		clearSelection();
	}

	/**
	 * Handles mouse pressed event from Geographic view.
	 */
	@Override
	public void mousePressed(MouseEvent event) {
		// Is it an event for WWObjectSelector ?
		if (WWRectangularSelector.checkInputEvent(event)) {
			rectangularSelector.enable(geographicViewService.getWwd()); // Now, WWObjectSelector will catch all the
																		// mouse events
			rectangularSelector.mousePressed(event);
		}
	}

	/**
	 * Handles Eclipse selection event (see {@link ISelectionService}).
	 */
	@Inject
	public void onSelection(
			@org.eclipse.e4.core.di.annotations.Optional @Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection) {
		if (structuredSelection != null) {
			List<IWWLayer> layers = new ArrayList<>();
			// compute new selection
			for (var selection : structuredSelection) {
				if (selection instanceof FileInfoNode fileInfoNode) {
					fileInfoNode.getChildren().stream() //
							.filter(LayerNode.class::isInstance).map(LayerNode.class::cast)//
							.map(LayerNode::getLayer).filter(Optional::isPresent).map(Optional::get) //
							.filter(IWWLayer.class::isInstance).map(IWWLayer.class::cast)//
							.forEach(layers::add);
				}
				if (selection instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
						&& layerNode.getLayer().get() instanceof IWWLayer wwLayer) {
					layers.add(wwLayer);
				}
			}
			onLayerSelected(layers.toArray(IWWLayer[]::new));
		}
	}

	/**
	 * Handles Eclipse events (see {@link IEventBroker}) of topic {@link WWLayerEvent}.
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onLayerSelected(@UIEventTopic(WWLayerEvent.TOPIC_LAYER_SELECTED) IWWLayer... layers) {
		setSelectedLayers(false);
		selectedLayers.clear();
		selectedLayers.addAll(Arrays.asList(layers));
		setSelectedLayers(true);
		updateSelectedLayer();
		IGeographicViewService.grab().refresh();
	}

	/**
	 * Clears current selection.
	 */
	private void clearSelection() {
		setSelectedLayers(false);
		selectedLayers.clear();
		IGeographicViewService.grab().refresh();
	}

	/**
	 * Updates {@link ILayerSelected}.
	 */
	private void setSelectedLayers(boolean selected) {
		selectedLayers.stream().filter(ILayerSelected.class::isInstance).map(ILayerSelected.class::cast)
				.forEach(layer -> layer.setSelectedLayer(selected));
	}

	/**
	 * Updates in 3DV the selected terrain CompositeShade.
	 */
	private void updateSelectedLayer() {
		for (Layer layer : selectedLayers) {
			if (layer instanceof ShaderElevationLayer shaderElevationLayer)
				ShaderElevationLayer.setCurrent(shaderElevationLayer);
			if (layer instanceof MultiTextureLayer multiTextureLayer)
				MultiTextureLayer.setCurrent(multiTextureLayer);
		}
	}

	/**
	 * Filters selection of {@link WWRectangularSelector}.
	 */
	private boolean filterRectangularSelection(Object selection) {
		return selection instanceof WWNavigationPath || selection instanceof PickedPointIndex
				|| selection instanceof IWCPickedObject;
	}

	/**
	 * Reacts to a selection by the {@link WWRectangularSelector}.
	 */
	private void onRectangularSelection(List<Object> objects) {
		var layers = objects.stream()//
				.map(obj -> {
					if (obj instanceof WWNavigationPath navigationPath)
						return navigationPath.getLayer();
					if (obj instanceof PickedPointIndex pickedPointIndex)
						return pickedPointIndex.getPointRenderer().getPickLayer();
					if (obj instanceof IWCPickedObject wCPickedObject)
						return wCPickedObject.getLayer();
					return null;
				})//
				.filter(IWWLayer.class::isInstance)//
				.distinct()//
				.toArray(IWWLayer[]::new);

		if (!Arrays.equals(layers, lastSelectedLayers)) {
			lastSelectedLayers = layers;
			eventBroker.post(WWLayerEvent.TOPIC_LAYER_SELECTED, layers);
		}
	}

	/** Return true when a rectangular selection is running */
	public boolean isRectangularSelection() {
		return rectangularSelector.isArmed();
	}
}
