package fr.ifremer.viewer3d.selection;

import java.util.LinkedList;
import java.util.List;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.services.events.IEventBroker;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics.LayerRequestInfo;
import fr.ifremer.globe.ui.application.event.WWLayerDescription;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.viewer3d.Activator;

/**
 * This class aim at managing the Check status (selection in project explorer) of texture layer with synchronization
 * key.
 */
@Creatable
public class WWTextureLayerSelectionSynchronizer {

	/** Service of events */
	@Inject
	private IEventBroker eventBroker;
	/** Global {@link WWFileLayerStoreModel} **/
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;
	@Inject
	private ProjectExplorerModel projectExplorerModel;
	@Inject
	private IProcessService processService;

	/**
	 * Manage the check status of the corresponding TreeNode for the specified layer.<br>
	 * If synchronization of the check status is required, search other layers with the same sync key and perform the
	 * same processing on them <br>
	 * The loading of the layer with same sync key is requested if necessary
	 */
	public void manageCheckStatus(IWWTextureLayer textureLayer) {
		boolean synchronizeCheckStatus = Activator.getPluginParameters().getSyncTextureLayerSelection().getValue();
		if (synchronizeCheckStatus) {
			String syncKey = getSynchronizationKey(textureLayer);
			if (syncKey.isEmpty())
				return;
			List<Pair<IFileInfo, WWLayerDescription>> layerToLoad = retrieveLayerToLoad(syncKey);
			if (!layerToLoad.isEmpty())
				loadLayersAndManageCheckStatus(layerToLoad, syncKey);
			else
				synchronizeCheckStatus(syncKey);
		} else {
			checkOneTreeNode(textureLayer.getName());
		}
	}

	/**
	 * Manage the check status of the corresponding TreeNode for the specified layer description.<br>
	 * If synchronization of the check status is required, search other layers with the same sync key and perform the
	 * same processing on them <br>
	 * The loading of the layer with same sync key is requested if necessary
	 */
	public void manageCheckStatus(IFileInfo fileInfo, WWLayerDescription textureDescription) {
		boolean synchronizeCheckStatus = Activator.getPluginParameters().getSyncTextureLayerSelection().getValue();
		if (synchronizeCheckStatus) {
			String syncKey = textureDescription.syncKey();
			if (syncKey.isEmpty())
				return;
			List<Pair<IFileInfo, WWLayerDescription>> layerToLoad = retrieveLayerToLoad(syncKey);
			if (!layerToLoad.isEmpty())
				loadLayersAndManageCheckStatus(layerToLoad, syncKey);
			else
				synchronizeCheckStatus(syncKey);
		} else {
			loadLayersAndManageCheckStatus(List.of(Pair.of(fileInfo, textureDescription)), "");
			checkOneTreeNode(textureDescription.name());
		}
	}

	/**
	 * Retrieves all WWLayerDescription with the same sync key
	 */
	private List<Pair<IFileInfo, WWLayerDescription>> retrieveLayerToLoad(String syncKey) {
		var result = new LinkedList<Pair<IFileInfo, WWLayerDescription>>();
		for (WWFileLayerStore layerStore : fileLayerStoreModel.getAll())
			for (WWLayerDescription optionalLayer : layerStore.getOptionalLayers())
				if (syncKey.equals(optionalLayer.syncKey()))
					result.add(Pair.of(layerStore.getFileInfo(), optionalLayer));
		return result;
	}

	/** Requests the loading of some layers. When done, manage the check status of the corresonding TreeNode */
	private void loadLayersAndManageCheckStatus(List<Pair<IFileInfo, WWLayerDescription>> layerToLoad, String syncKey) {
		processService.runInForeground("Producing texture layer", (monitor, logger) -> {
			SubMonitor progress = SubMonitor.convert(monitor, layerToLoad.size());
			for (Pair<IFileInfo, WWLayerDescription> pair : layerToLoad) {
				if (progress.isCanceled())
					return Status.CANCEL_STATUS;
				eventBroker.send(UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED,
						new LayerRequestInfo(pair.getFirst(), pair.getSecond(), progress.split(1), logger));
			}

			if (!progress.isCanceled() && !syncKey.isEmpty())
				synchronizeCheckStatus(syncKey);

			return Status.OK_STATUS;
		});

	}

	/**
	 * Browses all TreeNode with a synchronizable texture <br>
	 * Manages their Check status
	 */
	private void synchronizeCheckStatus(String syncKey) {
		if (syncKey.isEmpty())
			return;
		fileLayerStoreModel.getAll().stream().map(ls -> ls.getLayers(IWWTextureLayer.class).toList())
				// keep only file with at least one layer with the key
				.filter(layers -> layers.stream().map(this::getSynchronizationKey).anyMatch(syncKey::equals))
				// enables layer with same key, disable others
				.flatMap(List::stream)
				.forEach(textureLayer -> textureLayer.setEnabled(syncKey.equals(getSynchronizationKey(textureLayer))));
	}

	/**
	 * Search and check the treeNode of the texture
	 */
	private void checkOneTreeNode(String layerName) {
		projectExplorerModel.getDataNode().traverseDown(node -> {
			if (node instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
					&& layerNode.getLayer().get() instanceof IWWTextureLayer textureLayer) {
				layerNode.setChecked(layerName.equals(textureLayer.getName()));
			}
		});
	}

	/** Return the SynchronizationKey of the IWWTextureLayer or a WWLayerDescription */
	private String getSynchronizationKey(IWWTextureLayer textureLayer) {
		if (textureLayer.hasTextureData()) {
			String result = textureLayer.getTextureData().getSynchronizationKey();
			return result != null && !result.isEmpty() ? result : "";
		}
		return "";
	}

}
