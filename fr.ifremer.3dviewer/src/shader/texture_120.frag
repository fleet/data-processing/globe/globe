#version 120

uniform sampler2D texture;
uniform sampler2D palette;
uniform float minContrast;
uniform float maxContrast;
uniform int isThresholdEnabled;
uniform float minThreshold;
uniform float maxThreshold;
uniform float opacity;

varying vec4 vTexCoord;


bool isnan( float val )
{
  return ( val < 0.0 || 0.0 < val || val == 0.0 ) ? false : true;
  // important: some nVidias failed to cope with version below.
  // Probably wrong optimization.
  /*return ( val <= 0.0 || 0.0 <= val ) ? false : true;*/
}

void main()
{
	// Get value from the texture
	float value = texture2D(texture, vTexCoord.st).a;
	float normalizedValue= (value-minContrast)/(maxContrast-minContrast);

	// Set opactiy (0 if value == NaN)

	float lopacity = isnan(value) ? 0 : opacity;
	
	// Apply threshold filter
	if(isThresholdEnabled == 1)
		lopacity = value < minThreshold || value > maxThreshold ? 0 : opacity;
	
	// Discard the fragment if opactity == 0 (avoid issue with transparency & NaN values...Quick fix, not the best solution)
	if (lopacity == 0)
		discard; 
	
	// Get the color of amplification from the palette choosen by the user
	vec3 paletteColor = texture2D(palette, vec2(normalizedValue, 0)).rgb;

	// Apply this color to the fragment
	gl_FragColor = vec4(paletteColor.r, paletteColor.g, paletteColor.b, lopacity);
}
