#version 120

uniform sampler2D texture;
uniform int isThresholdEnabled;
uniform float minThreshold;
uniform float maxThreshold;
uniform vec4 pickingColor;

varying vec4 vTexCoord;

bool isnan( float val )
{
  return ( val < 0.0 || 0.0 < val || val == 0.0 ) ? false : true;
  // important: some nVidias failed to cope with version below.
  // Probably wrong optimization.
  /*return ( val <= 0.0 || 0.0 <= val ) ? false : true;*/
}

void main()
{
	// Get value from the texture
	float value = texture2D(texture, vTexCoord.st).a;

	// disable picking for NAN & filtered values
	if(isnan(value) || ((isThresholdEnabled == 1) && (value < minThreshold || value > maxThreshold )))
		discard;
	
	// Apply this color to the fragment
	gl_FragColor = pickingColor;
}
