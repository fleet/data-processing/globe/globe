#version 120

uniform vec3 topPoint;			// Coordinates of the water column top point
uniform vec2 topTexture;		// Coordinates in the texture of the water column top point
uniform float subset;
uniform vec4 alongVector;
uniform vec4 accrossVector;

attribute vec2 textureBuffer;	// Coordinates in the texture of the vertice  to draw
attribute vec4 beamBuffer;		// Data relatives to the beam (x : current index, y : maximum index in the beam, z : angle between current beam and water column height, w : opening angle inside the beam)

varying vec2 vTextureCoordinates;

varying float beamOpeningAngle; //beam opening angle
varying vec4 position;		// Data relatives to the beam (x : current index, y : maximum sample index in the beam, z : not used here)
varying vec4 verticalShift; //vertical translation of the point, aligne with a point to bottom vector

void main()
{

	// Compute coefficients of vector topPoint -> gl_Vertex[0] in texture reference
	float b = topTexture.y - textureBuffer.y;

	// Compute position of the point to display
	float u = textureBuffer.x;
	float v = topTexture.y - (beamBuffer.x * b) / beamBuffer.y;

	// Apply found values to openGL variable
	vTextureCoordinates = vec2(u,v);
	// Compute front color
	gl_FrontColor = gl_Color;
	vec3 top2Bottom=topPoint-gl_Vertex.xyz; //vector from top to bottomPoint detection point of the beam
	vec3 displayedPoint=topPoint-(beamBuffer.x/beamBuffer.y)*top2Bottom;

	// Apply found values to openGL variable
	gl_Position =  vec4(displayedPoint.xyz, 1.0);

	float rangeTranslation=length(top2Bottom);
	rangeTranslation=(rangeTranslation/beamBuffer.y)/2.0;
	rangeTranslation = rangeTranslation*subset;
	// rangeTranslation is the vertical displacement length along the top to center of the cell

	verticalShift= vec4(rangeTranslation* (normalize(topPoint-displayedPoint)),0); //normalizeVerticalVector

	beamOpeningAngle = beamBuffer.w;
	position=vec4(displayedPoint.xyz, 1.0);

}
