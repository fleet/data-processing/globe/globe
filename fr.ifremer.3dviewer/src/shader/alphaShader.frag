uniform sampler2D tex;

void main()
{
	vec4 color = texture2D(tex,gl_TexCoord[0].st);
	if(color.a > 0.2)
		gl_FragColor = gl_Color;
	else
		gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0); 
}