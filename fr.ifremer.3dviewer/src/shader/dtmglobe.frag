uniform sampler2D tex;
uniform float min;
uniform float max;
uniform float globalMin;
uniform float globalMax;
uniform float colormap;

uniform float azimuth;
uniform float zenith;
uniform float ombrageExaggeration;
uniform float cellSize;

uniform float ombrage;
uniform float width;
uniform float height;
uniform float inverseColormap;
uniform float gradient;
uniform float use;

uniform float opacity;

void main()
{
	vec4 colorOri = texture2D(tex, gl_TexCoord[0].st);
	float intensity = 0.0;
	float contrast = 0.0;
	float sobel[9];
	float pi = 3.14;
	float ombre = 0.0;
	
	float dx = 0.0;
	float dy = 0.0;
	
	float r;
	float g;
	float b;

	
	sobel[0] = -1.0;
	sobel[1] = -2.0;
	sobel[2] = -1.0;
	sobel[3] = 0.0;
	sobel[4] = 0.0;
	sobel[5] = 0.0;
	sobel[6] = 1.0;
	sobel[7] = 2.0;
	sobel[8] = 1.0;
	
	
	if(colorOri.r == 0.0 && colorOri.g == 0.0 && colorOri.b == 0.0)
	{
		gl_FragColor = colorOri;
		gl_FragColor.a = 0.0;
	}
	else if(gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s > 1.0 || gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t > 1.0)
	{
		gl_FragColor = colorOri;
		gl_FragColor.a = 0.0;
	}
	else if(use == 0.0)
	{
		gl_FragColor = colorOri;
	}
	else
	{		
		float val = (colorOri.r*256.0*256.0 + colorOri.g*256.0 + colorOri.b)/(256.0*256.0);
		if(ombrage == 1.0)
		{	
			dx = 0.0;
			dy = 0.0;
			for(int i=0; i<3; i++)
			{
				for(int j=0; j<3; j++)
				{
					vec2 offset = vec2(float(i-1)/width, float(j-1)/height);

					vec4 tmp = texture2D(tex, gl_TexCoord[0].st + offset);
					
					//normalisation de l'elevation 
					dx += ((tmp.r*256.0*256.0 + tmp.g*256.0 + tmp.b)/(256.0 * 256.0)) * sobel[i+3*j];
					dy += ((tmp.r*256.0*256.0 + tmp.g*256.0 + tmp.b)/(256.0 * 256.0)) * sobel[3*i+j];
					

				}
			}
							
								
				////////////////////////////////////////////
				float  rayon_zenithal = radians(zenith);
				
				float azimuth_mathematique = azimuth+180.0;
				if (azimuth_mathematique >=360.0) {
				azimuth_mathematique = azimuth_mathematique - 360.0;
				}
				float rayon_azimutal = radians(azimuth_mathematique);
				
				float dzdx = dx/(8.0 * cellSize);
				float dzdy = dy/(8.0 * cellSize);
			
				float rayon_pente = atan(ombrageExaggeration * sqrt(dzdx * dzdx + dzdy *dzdy));
				
				float rayon_exposition = 0.0;
				
				if (dzdx!=0.0) {
					rayon_exposition = atan(dzdy, -dzdx);
					if (rayon_exposition < 0.0) {
						rayon_exposition = 2.0 * pi + rayon_exposition;
					}
				} else if(dzdx== 0.0) {
					if (dzdy > 0.0) {
						rayon_exposition = pi / 2.0;
					} else if (dzdy < 0.0) {
						rayon_exposition = 2.0 * pi - pi / 2.0;
					} else {
						rayon_exposition = rayon_exposition;
					}			
				}
				
				
				ombre = ((cos(rayon_zenithal) * cos(rayon_pente)) + 
           		  (sin(rayon_zenithal) * sin(rayon_pente) * cos(rayon_azimutal - rayon_exposition)));
				if (ombre<0.0) {
				ombre = 0.0;
				}
			
		}
		
		if(gradient == 1.0)
		{
			//float val = (ombre-min)/(max-min);
			r = ombre;
			g = ombre;
			b = ombre;
		}
		else
		{
			intensity = val;
				
			contrast = intensity;//(intensity - min)/(max-min);
			
			if(inverseColormap == 1.0)
			{
				contrast = 1.0 - contrast;
			}
			
			if(colormap < 0.1) //jet colormap
			{
				if(contrast <= 31.0/255.0) //index entre 0 et 31
				{
					r = 0.0;
					g = 0.0;
					b = 0.531250 + contrast*(1-0.531250)/31.0*255.0;
				}
				else if(contrast <= 95.0/255.0) //index entre 32 et 95
				{
					r = 0.0;
					g = (contrast-32.0/255.0)*255.0/(95.0-32.0);
					b = 1.0;
				}
				else if(contrast <= 159.0/255.0) //index entre 96 et 159
				{
					r = (contrast-96.0/255.0)*255.0/(159.0-96.0);
					g = 1.0;
					b = 1.0 - (contrast-96.0/255.0)*255.0/(159.0-96.0);
				}
				else if(contrast <= 223.0/255.0) //index entre 160 et 223
				{
					r = 1.0;
					g = 1.0 - (contrast-160.0/255.0)*255.0/(223.0-160.0);
					b = 0.0;
				}
				else //index entre 224 et 255
				{
					r = 1.0 - (contrast-224.0/255.0)*0.5*255.0/(255.0-224.0);
					g = 0.0;
					b = 0.0;
				}
			}
			else if(colormap < 1.1) //catherine
			{
				if(contrast <= 35.0/255.0)//R
				{
					r = 0.0391 + contrast*255.0/35.0*(0.0976-0.0391);
				}
				else if(contrast <= 63.0/255.0)//R
				{
					r = 0.1035 + (contrast*255.0-36.0)/(63.0-36.0)*(0.2634-0.1035);
				}
				else if(contrast <= 116.0/255.0)//R
				{
					r = 0.267 + (contrast*255.0-64.0)/(116.0-64.0)*(0.451-0.267);
				}
				else if(contrast <= 160.0/255.0)//R
				{
					r = 0.4634 + (contrast*255.0-117.0)/(160.0-117.0)*(0.9961-0.4634);
				}
				else //R
				{
					r = 0.9944 + (contrast*255.0-161.0)/(255.0-161.0)*(0.8392-0.9944);
				}
				
				if(contrast <= 63.0/255.0)//G
				{
					g = contrast*255.0/63.0*0.7876;
				}
				else if(contrast <= 160.0/255.0)//G
				{
					g = 0.7901 + (contrast*255.0-64.0)/(160.0-64.0)*(0.9882-0.7901);
				}
				else if(contrast <= 206.0/255.0)//G
				{
					g = 0.9842 + (contrast*255.0-161.0)/(206.0-161.0)*(0.8052-0.9842);
				}
				else//G
				{
					g = 0.7933 + (contrast*255.0-206.0)/(255.0-206.0)*(0.2-0.8052);
				}
				
				if(contrast <= 35.0/255.0)//B
				{
					b = 0.4727 + contrast*255.0/35.0*(0.94-0.4727);
				}
				else if(contrast <= 63.0/255.0)//B
				{
					b = 0.942 + (contrast*255.0-36.0)/(63.0-36.0)*(0.9961-0.942);
				}
				else if(contrast <= 116.0/255.0)//B
				{
					b = 0.9891 + (contrast*255.0-64.0)/(116.0-64.0)*(0.6275-0.9891);
				}
				else if(contrast <= 160.0/255.0)//B
				{
					b = 0.6241 + (contrast*255.0-117.0)/(160.0-117.0)*(0.4784-0.6241);
				}
				else if(contrast <= 206.0/255.0)//B
				{
					b = 0.4766 + (contrast*255.0-161.0)/(206.0-161.0)*(0.3902-0.4766);
				}
				else//B
				{
					b = 0.3824 + (contrast*255.0-206.0)/(255.0-206.0)*(0.0-0.3824);
				}
			}
			else if(colormap < 2.1) //grayscale
				{
					r = contrast;
					g = contrast;
					b = contrast;
				}
				else //caraibes
				{
					if(contrast <= 4.0/100.0)//R
					{
						r = 4.0*contrast*100.0/255.0;
					}
					else if(contrast <= 10.0/100.0)//R
					{
						r = (4.0*contrast*100.0 - 1.0)/255.0;
					}
					else if(contrast <= 17.0/100.0)//R
					{
						r = (4.0*contrast*100.0 - 2.0)/255.0;
					}
					else if(contrast <= 23.0/100.0)//R
					{
						r = (4.0*contrast*100.0 - 3.0)/255.0;
					}
					else if(contrast <= 26.0/100.0)//R
					{
						r = (4.0*contrast*100.0 - 4.0)/255.0;
					}
					else if(contrast <= 50.0/100.0)//R
					{
						r = (-4.0*contrast*100.0 + 200.0)/255.0;
					}
					else if(contrast <= 73.0/100.0)//R
					{
						r = (11.0*contrast*100.0 - 550.0)/255.0;
					}
					else
					{
						r = 255.0/255.0;
					}
					
					if(contrast <= 26.0/100.0)//G
					{
						g = 6.0*contrast*100.0/255.0;
					}
					else if(contrast <= 50.0/100.0)//G
					{
						g = (4.0*contrast*100.0 + 45.0)/255.0;
					}
					else if(contrast <= 73.0/100.0)//G
					{
						g = 255.0/255.0;
					}
					else
					{
						g = (-10.0*contrast*100.0 + 990.0)/255.0;
					}
					
					if(contrast <= 26.0/100.0)//B
					{
						b = (contrast*100.0 + 200.0)/255.0;
					}
					else if(contrast <= 50.0/100.0)//B
					{
						b = (-10.0*contrast*100.0 + 500.0)/255.0;
					}
					else 
					{
						b = 0.0/255.0;
					}
				}
		}
		
		if(ombrage == 1.0 && gradient != 1.0)
		{	
			gl_FragColor.r = r * ombre;
			gl_FragColor.g = g * ombre;
			gl_FragColor.b = b * ombre;						
		}
		else
		{
			gl_FragColor.r = r;
			gl_FragColor.g = g;
			gl_FragColor.b = b;
		}
					
		gl_FragColor.a = colorOri.a * opacity;	
	 }
	 
}