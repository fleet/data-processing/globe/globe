#version 150 compatibility

layout(points) in;
layout(triangle_strip, max_vertices = 14) out;

in vec2 vTextureCoordinates[];
uniform vec4 alongVector;
uniform vec4 acrossVector;

in float beamOpeningAngle[];
in vec4 position[];
in vec4 verticalShift[];

uniform vec3 topPoint;			// Coordinates of the water column top point, matching the transducer position

mat4 rotationMatrix(vec4 axisin, float angle)
{
	vec3 axis = normalize(vec3(axisin.xyz));
	float s = sin(angle);
	float c = cos(angle);
	float oc = 1.0 - c;

	return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
			oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
			oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
			0.0,                                0.0,                                0.0,                                1.0);
}

void main()
{
	gl_TexCoord[0].st = vTextureCoordinates[0];



	// rotation matrix along the along vector which means rotation in across distance
	mat4 mAlong = rotationMatrix(alongVector,beamOpeningAngle[0]);
	mat4 mAlongNeg = rotationMatrix(alongVector,-beamOpeningAngle[0]);
	mat4 mAcross = rotationMatrix(acrossVector,beamOpeningAngle[0]);
	mat4 mAcrossNeg = rotationMatrix(acrossVector,-beamOpeningAngle[0]);

	vec4 top4=vec4(topPoint,0);
	vec4 topTranslated=position[0]+verticalShift[0]-top4;
	vec4 bottomTranslated=position[0]-verticalShift[0]-top4;

	gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(topTranslated)+top4); //Front top left
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcrossNeg*(topTranslated)+top4); //Front top right
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(bottomTranslated)+top4); //Front bottom left
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcrossNeg*(bottomTranslated)+top4);  // Front-bottom-right
	EmitVertex();

	gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(bottomTranslated)+top4);   // Back-bottom-right
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcrossNeg*(topTranslated)+top4); //Front top right
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(topTranslated)+top4);  // Back-top-right
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(topTranslated)+top4); //Front top left
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcross*(topTranslated)+top4); // Back-top-left
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(bottomTranslated)+top4);// Front-bottom-left
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcross*(bottomTranslated)+top4); // Back-bottom-left
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(bottomTranslated)+top4);   // Back-bottom-right
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcross*(topTranslated)+top4);   // Back-top-left
	EmitVertex();
	gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(topTranslated)+top4);   // Back-top-right
	EmitVertex();

	EndPrimitive();



}
