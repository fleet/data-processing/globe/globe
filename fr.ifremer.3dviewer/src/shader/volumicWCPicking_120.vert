#version 120

uniform float opacity;
varying vec4 vcolor;
uniform vec4 picking_color;

attribute float beamWidthAcross;
attribute float beamWidthAlong;
attribute float height;

varying float gAlongAngle;
varying float gAcrossAngle;
varying float gHeight;

void main()
{
	vcolor=vec4(picking_color.r, picking_color.g, picking_color.b, opacity);
	gAcrossAngle	=beamWidthAcross;
	gAlongAngle=beamWidthAlong;
	gHeight=height;
	gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
}
