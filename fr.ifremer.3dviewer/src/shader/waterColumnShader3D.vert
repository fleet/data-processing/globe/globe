#version 120

uniform vec3 topPoint;			// Coordinates of the water column top point
uniform vec2 topTexture;		// Coordinates in the texture of the water column top point

attribute vec2 textureBuffer;	// Coordinates in the texture of the vertice  to draw
attribute vec3 beamBuffer;		// Data relatives to the beam (x : current index, y : maximum sample index in the beam, z : not used here)

void main()
{
	// Compute coefficients of vector topPoint -> gl_Vertex[0] in texture reference
	float b = topTexture.y - textureBuffer.y;

	// Compute position of the point to display
	float u = textureBuffer.x;
	float v = topTexture.y - (beamBuffer.x * b) / beamBuffer.y;

	// Apply found values to openGL variable
	gl_TexCoord[0].st= vec2(u, v);

	// Compute front color
	gl_FrontColor = gl_Color;

	// Compute coefficients of vector topPoint -> gl_Vertex[0] in coordinates reference
	float d = topPoint.x - gl_Vertex.x;
	float e = topPoint.y - gl_Vertex.y;
	float f = topPoint.z - gl_Vertex.z;

	// Compute position of the point to display
	float xv = topPoint.x - (beamBuffer.x * d) / beamBuffer.y;
	float yv = topPoint.y - (beamBuffer.x * e) / beamBuffer.y;
	float zv = topPoint.z - (beamBuffer.x * f) / beamBuffer.y;

	// Apply found values to openGL variable
	gl_Position = gl_ModelViewProjectionMatrix * vec4(xv, yv, zv, 1.0);
}
