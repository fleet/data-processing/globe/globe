#version 150 compatibility
precision highp float;

layout(points) in;
layout(triangle_strip, max_vertices = 14) out;

in vec4 vcolor[];
out vec4 color;

uniform vec4 alongVector;
uniform vec4 acrossVector;
in float gAlongAngle[];
in float gAcrossAngle[];
uniform vec4 topPoint;			// Coordinates of the water column top point
in float gHeight[]; // displayed height of an echo


mat4 rotationMatrix(vec4 axisin, float angle)
{
	vec3 axis = normalize(vec3(axisin.xyz));
	float s = sin(angle);
	float c = cos(angle);
	float oc = 1.0 - c;

	return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
			oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
			oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
			0.0,                                0.0,                                0.0,                                1.0);
}

void main()
{

	int i;
	// fake loop, since input is point, length=1
	for ( i=0; i < gl_in.length(); i++) {

		// rotation matrix along the along vector which means rotation in across distance
		mat4 mAlong = rotationMatrix(alongVector,gAcrossAngle[i]/2);
		mat4 mAlongNeg = rotationMatrix(alongVector,-gAcrossAngle[i]/2);
		mat4 mAcross = rotationMatrix(acrossVector,gAlongAngle[i]/2);
		mat4 mAcrossNeg = rotationMatrix(acrossVector,-gAlongAngle[i]/2);

		vec4 pos=gl_in[i].gl_Position;

		vec4 verticalShift=normalize(topPoint-pos)*gHeight[i]/2; //translation vector from center of the echo to the upper surface

		vec4 topTranslated=pos-topPoint-verticalShift;
		vec4 bottomTranslated=pos-topPoint+verticalShift;

		color=vcolor[i];

		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(topTranslated)+topPoint); //Front top left
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcrossNeg*(topTranslated)+topPoint); //Front top right
		EmitVertex();

		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(bottomTranslated)+topPoint); //Front bottom left
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcrossNeg*(bottomTranslated)+topPoint);  // Front-bottom-right
		EmitVertex();

		gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(bottomTranslated)+topPoint);   // Back-bottom-right
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcrossNeg*(topTranslated)+topPoint); //Front top right
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(topTranslated)+topPoint);  // Back-top-right
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(topTranslated)+topPoint); //Front top left
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcross*(topTranslated)+topPoint); // Back-top-left
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(bottomTranslated)+topPoint);// Front-bottom-left
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcross*(bottomTranslated)+topPoint); // Back-bottom-left
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(bottomTranslated)+topPoint);   // Back-bottom-right
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcross*(topTranslated)+topPoint);   // Back-top-left
		EmitVertex();
		gl_Position =gl_ModelViewProjectionMatrix*(mAlong*mAcross*(topTranslated)+topPoint);   // Back-top-right
		EmitVertex();


//		color=vcolor[i];
//		//		gl_Position =gl_ModelViewProjectionMatrix*(mAlongNeg*mAcrossNeg*(pos)); //Front top left
//		//		EmitVertex();
//		gl_Position =  gl_ModelViewProjectionMatrix*pos; //center
//		EmitVertex();

	}


	EndPrimitive();
}
