#version 150

uniform sampler2D texture;
uniform int isThresholdEnabled;
uniform float minThreshold;
uniform float maxThreshold;
uniform vec4 pickingColor;

in vec4 vTexCoord;

out vec4 out_Color;

void main()
{
	// Get value from the texture
	float value = texture2D(texture, vTexCoord.st).a;
	
	// disable picking for NAN & filtered values
	if(isnan(value) || ((isThresholdEnabled == 1) && (value < minThreshold || value > maxThreshold )))
		discard;

	out_Color = pickingColor;
}
