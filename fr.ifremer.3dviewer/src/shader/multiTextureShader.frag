#version 120
uniform sampler2D Texture0;
uniform sampler2D Texture1;
uniform sampler2D Texture2;
uniform sampler2D Texture3;
uniform sampler2D Texture4;
/*
uniform sampler2D Texture5;
uniform sampler2D Texture6;
uniform sampler2D Texture7;
uniform sampler2D Texture8;
uniform sampler2D Texture9;
uniform sampler2D Texture10;
uniform sampler2D Texture11;
uniform sampler2D Texture12;
uniform sampler2D Texture13;
uniform sampler2D Texture14;
uniform sampler2D Texture15;
*/

uniform sampler2D TextureFilter0;
uniform sampler2D TextureFilter1;
uniform sampler2D TextureFilter2;
uniform sampler2D TextureFilter3;
uniform sampler2D TextureFilter4;
/*
uniform sampler2D TextureFilter5;
uniform sampler2D TextureFilter6;
uniform sampler2D TextureFilter7;
uniform sampler2D TextureFilter8;
uniform sampler2D TextureFilter9;
*/

uniform sampler2D TextureSecondFilter0;
uniform sampler2D TextureSecondFilter1;
uniform sampler2D TextureSecondFilter2;
uniform sampler2D TextureSecondFilter3;
uniform sampler2D TextureSecondFilter4;
/*
uniform sampler2D TextureSecondFilter5;
uniform sampler2D TextureSecondFilter6;
uniform sampler2D TextureSecondFilter7;
uniform sampler2D TextureSecondFilter8;
uniform sampler2D TextureSecondFilter9;
*/

uniform float nbTexture;
uniform float useTextureFilter;
uniform float useTextureSecondFilter;

uniform float filterMinValue;
uniform float filterMaxValue;

uniform float secondFilterMinValue;
uniform float secondFilterMaxValue;

uniform float min;
uniform float max;
uniform float minTransparency;
uniform float maxTransparency;
uniform float globalMin;
uniform float globalMax;

uniform int colorMapLength;
uniform int colormap[256];
uniform float logColormap;

uniform float azimuth;
uniform float zenith;
uniform float ombrageExaggeration;
uniform float cellSize;

uniform float ombrage;
uniform float width;
uniform float height;
uniform float inverseColormap;
uniform float gradient;
uniform float ombrageLogarithmique;

uniform float filterName;
uniform float sigmaGauss;
uniform float filterWidth;
uniform float filterHeight;
uniform float alphaFilter;
uniform float Lookup;

uniform float offset;

uniform float resolution;

uniform float opacity;

//////////   Utils  ///////////
//compute the index of colormap according to 
//contrast and colorMapLength
vec4 computeColorIndex(float contrast)
{
	vec4 demultiplexed = vec4(0.0,0.0,0.0,1.0);
	int index = int((colorMapLength-1.0)*contrast);
	if(index<0)
		index = 0;
	if(index>colorMapLength-2.0)
		index = colorMapLength-2;
	int colorMultiplexed = colormap[index];
	
	//red == MSB
	float r = floor(colorMultiplexed/256.0/256.0);
	demultiplexed.r = r*opacity/255.0;
	
	//green
	float g = floor((colorMultiplexed - r*256.0*256.0)/256.0); 
	demultiplexed.g = g*opacity/255.0;
	
	//blue
	float b = floor(colorMultiplexed - r*256.0*256.0 - g*256.0); 
	demultiplexed.b = b*opacity/255.0;
	
	return demultiplexed;
}

//extract the 24 bits value coded as RGB in the texture 
float textureToValue(vec4 textureRGBA)
{
	float value = (textureRGBA.r*256.0*256.0 + textureRGBA.g*256.0 + textureRGBA.b)*255.0/(256.0 * 256.0 * 256.0);
	//adapt data to other layers
	//TODO diminution de la precision due a reference
	//return (value-reference)*scale+reference+offset;
	return value+offset;
}

//computes the real value from the 24 bits value
float BitsValueToGlobalValue(float val)
{
	return val*(globalMax - globalMin) + globalMin;
}

//transform the real value in 24 bits value
float globalValueToBitsValue(float val)
{
	return (val-globalMin)/(globalMax - globalMin);
}


//////////   End Utils  ///////////

//filtre alpha_lieaire
//source "imagerie a vision laterale" 1988
float alphaLinear(sampler2D tex)
{
	float val = 0.0;
	float moyAlphaUn = 0.0;
	float moyAlpha = 0.0;
	float moyDAlpha = 0.0;
	float moyD = 0.0;
	float moy = 0.0;
	float nb = 0.0;
	vec4 tmp;
	float a = 0.0;
	
	for(float i = 0.0; i<filterWidth; i++)
	{
		for(float j = 0.0; j<filterHeight; j++)
		{
			float x = (i-(filterWidth/2.0));
			float y = (j-(filterHeight/2.0));
			
			vec2 off = vec2(x/width, y/height);

			tmp = texture2D(tex, gl_TexCoord[0].st + off);
			
			//suppression de l'interpolation en alpha sur les bords des tuiles
			if(tmp.a == 1.0)
			{
				val = textureToValue(tmp);
				val = BitsValueToGlobalValue(val);
				
				moyAlphaUn += pow(val, alphaFilter + 1.0);
				moyAlpha += pow(val, alphaFilter);
				moyDAlpha += pow(val, 2.0*alphaFilter);
				moyD += pow(val, 2.0);
				moy += val;
				nb++;
			}
		}
	}
	
	tmp = texture2D(tex, gl_TexCoord[0].st);
	val = textureToValue(tmp);
	val = BitsValueToGlobalValue(val);
			
	a = (moyAlphaUn/nb*Lookup/(Lookup+alphaFilter)-moyAlpha/nb*moy/nb)/(moyD/nb-moy/nb*moy/nb);
	if(a>0.0)
		val=a*pow(val, alphaFilter) + moy/nb - a*moyAlpha/nb;
	else
		val=moy/nb;
	
	return globalValueToBitsValue(val);
}

//filtre de lee
float leeFilter(sampler2D tex)
{
	float val = 0.0;
	float moyD = 0.0;
	float moy = 0.0;
	float nb = 0.0;
	float gamma = 0.0;
	float k;
	vec4 tmp;
	
	for(float i = 0.0; i<filterWidth; i++)
	{
		for(float j = 0.0; j<filterHeight; j++)
		{
			float x= (i - (filterWidth/2.0));
			float y = (j - (filterHeight/2.0));
			
			vec2 off = vec2(x/width, y/height);

			tmp = texture2D(tex, gl_TexCoord[0].st + off);
			//suppression de l'interpolation en alpha sur les bords des tuiles
			if(tmp.a == 1.0)
			{
				val = textureToValue(tmp);
				val = BitsValueToGlobalValue(val);
				
				moyD += pow(val, 2.0);
				moy += val;
				nb++;
			}
		}
	}
	
	tmp = texture2D(tex, gl_TexCoord[0].st);
	val = textureToValue(tmp);
	val = BitsValueToGlobalValue(val);
			
	gamma = (moyD/nb-moy/nb*moy/nb)/(moy/nb*moy/nb);
	if(gamma == 0.0)
		gamma = 1.0/Lookup;
		
	k = 1.0 - 1.0/(Lookup*gamma);
	
	if(k > 0.0 && k <= 1.0)
		val = moy/nb + k*(val-moy/nb);
	else
		val = moy/nb;
	
	return globalValueToBitsValue(val);
}

//filtre gaussien
//la variable globale sigmaGauss definie la taille du filtre
//le parametre offset permet d'utiliser le filtre au sein d'une convolution 
//(notamment l'ombrage)
float gaussianFilter(sampler2D tex, vec2 offset)
{
	float pi = 3.14;
	int filterGaussSize = int(sigmaGauss*4.0 + 1.0);
	int offsetGauss = int(2.0*sigmaGauss);
	if(filterGaussSize<3)
	{
		filterGaussSize = 3;
		offsetGauss = 1;
	}
	
	float gaussian;
	float si2 = sigmaGauss*sigmaGauss;
	
	float val = 0.0;
	float gaussianSum = 0.0;
	for(int i = 0; i<filterGaussSize; i++)
	{
		for(int j = 0; j<filterGaussSize; j++)
		{
			float x = float(i - offsetGauss);
			float y = float(j - offsetGauss);
			
			vec2 off = vec2(x/width, y/height);

			vec4 tmp = texture2D(tex, gl_TexCoord[0].st + off + offset);
			//suppression de l'interpolation en alpha sur les bords des tuiles
			if(tmp.a == 1.0)
			{
				//normalisation de l'elevation 
				gaussian = exp(-(x*x+y*y)/(2.0*si2)) / (2.0*pi*si2);
				val += BitsValueToGlobalValue(textureToValue(tmp)) * gaussian;
				gaussianSum += gaussian;
			}
		}
	}
	val /= gaussianSum;
	return globalValueToBitsValue(val);
}

//Wiener filter
float wienerFilter(sampler2D tex)
{
	float val = 0.0;
	float moyD = 0.0;
	float moy = 0.0;
	float nb = 0.0;
	float gamma = 0.0;
	vec4 tmp = texture2D(tex, gl_TexCoord[0].st);
	float noise = 0.0001;
	float f = 0.0;
	float g = textureToValue(tmp);
	float variance = 0.0;
	
	//calcul moyenne et variance
	for(float i = 0.0; i<filterWidth; i++)
	{
		for(float j = 0.0; j<filterHeight; j++)
		{
			float x= (i - (filterWidth/2.0));
			float y = (j - (filterHeight/2.0));
			
			vec2 off = vec2(x/width, y/height);

			tmp = texture2D(tex, gl_TexCoord[0].st + off);
			//suppression de l'interpolation en alpha sur les bords des tuiles
			if(tmp.a == 1.0)
			{
				val = textureToValue(tmp);
				//val = BitsValueToGlobalValue(val);
				
				moyD += pow(val, 2.0);
				moy += val;
				nb++;
			}
		}
	}
	
	moy = moy/nb;
	variance = moyD/nb -moy*moy; 
	//calcul bruit
	//TODO on CPU : calcul de la moyenne de la variance
	
	//wiener
	f = g-moy;
	g=variance-noise;
	
	if(g<0)
	{
		g=0;
	}
	
	if(variance<noise)
	{
		variance=noise;
	}
	
	f=f/variance;
	f=f*g;
	f=f+moy;
	
	return f;
}

//fonction realisant l'ombrage par mesure de la pente 
//grace a un filtre de sobel
float ombrageGradient(sampler2D tex)
{
	float sobel[9];
	float pi = 3.14;
	float ombre = 0.0;
	
	float v;
	
	float dx = 0.0;
	float dy = 0.0;
	
	sobel[0] = -1.0;
	sobel[1] = -2.0;
	sobel[2] = -1.0;
	sobel[3] = 0.0;
	sobel[4] = 0.0;
	sobel[5] = 0.0;
	sobel[6] = 1.0;
	sobel[7] = 2.0;
	sobel[8] = 1.0;
	
	dx = 0.0;
	dy = 0.0;
	
	//Astuce pour les bordures, on fait le calcul sur un pixel qui ne pr�sente pas de problem
	//d ou offset de +/-2
	float trickX = 0.0;
	float trickY = 0.0;
	//border left
	if(gl_TexCoord[0].s <= 1.0/width)
	{
		trickX = 1.0/width;
	}
	
	//border right
	if(gl_TexCoord[0].s >= 1.0 - 1.0/width)
	{
		trickX = -1.0/width;
	}

	//border top
	
	if(gl_TexCoord[0].t <= 1.0/height)
	{
		trickY = 1.0/height;
	}

	//border bottom
	if(gl_TexCoord[0].t >= 1.0 - 1.0/height)
	{
		trickY = -1.0/height;
	}
	
	//offset trick border
	vec2 trickOffset = vec2(trickX,trickY);
	
	//determination de la pente par gradient de sobel
	float is_xBorder = 0.0;
	float is_yBorder = 0.0;
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			vec2 offset = vec2(float(i-1)/width, float(j-1)/height);

			vec4 tmp = texture2D(tex, gl_TexCoord[0].st + offset + trickOffset);
			
			float border = 0.0;
			if(gl_TexCoord[0].s + offset.s + trickOffset.s < 0.0 || gl_TexCoord[0].s + offset.s + trickOffset.s >= 1.0)
			{
				is_xBorder = 1.0;
				border = 1.0;
			}
			if(gl_TexCoord[0].t + offset.t + trickOffset.t < 0.0 || gl_TexCoord[0].t + offset.t + trickOffset.t >= 1.0)
			{
				is_yBorder = 1.0;
				border = 1.0;
			}
			
			if(border == 0.0)
			{
				if(sigmaGauss != 0.0)
				{
					//filtrage gaussien pour lisser le bruit
					//passage de l'offset de la convolution de sobel
					v = gaussianFilter(tex, offset);
				}
				else
					v = textureToValue(tmp);
				
				//normalisation de l'elevation 
				if(tmp.a == 1.0)
				{
					v = BitsValueToGlobalValue(v);
					
					//ombrage logarithmique
					if(ombrageLogarithmique == 1.0)
					{
						//gestion log(0)
						if(abs(v)<0.001)
							v = sign(v)*0.001;
							
						v = sign(v)*log(abs(v))/log(10.0);
					}
					
					//pentes x et y
					dx += v * sobel[i+3*j];
					dy += v * sobel[3*i+j];
				}
				else
					//transparent neighborhood must be discared
					return 10000.0;
			}
		}
	}
	
	if(is_xBorder == 1.0)
	{
		dx= dx*2.0*resolution;
	}
	if(is_yBorder == 1.0)
	{
		dy= dy*2.0*resolution;
	}
								
	////////////////////////////////////////////
	float  rayon_zenithal = radians(zenith);
	
	float azimuth_mathematique = azimuth+180.0;
	if (azimuth_mathematique >=360.0) {
	azimuth_mathematique = azimuth_mathematique - 360.0;
	}
	float rayon_azimutal = radians(azimuth_mathematique);
	
	float dzdx = dx/(8.0 * cellSize);
	float dzdy = dy/(8.0 * cellSize);

	float rayon_pente = atan(ombrageExaggeration * sqrt(dzdx * dzdx + dzdy *dzdy));
	
	float rayon_exposition = 0.0;
	
	if (dzdx!=0.0) {
		rayon_exposition = atan(dzdy, -dzdx);
		if (rayon_exposition < 0.0) {
			rayon_exposition = 2.0 * pi + rayon_exposition;
		}
	} else if(dzdx == 0.0) {
		if (dzdy > 0.0) {
			rayon_exposition = pi / 2.0;
		} else if (dzdy < 0.0) {
			rayon_exposition = 2.0 * pi - pi / 2.0;
		} else {
			rayon_exposition = rayon_exposition;
		}			
	}
	
	//calcul de l'ombre en fonction de la pente
	ombre = ((cos(rayon_zenithal) * cos(rayon_pente)) + 
	  (sin(rayon_zenithal) * sin(rayon_pente) * cos(rayon_azimutal - rayon_exposition)));
	  
	if (ombre<0.0) {
		ombre = 0.0;
	}
	
	return ombre;
}

vec4 computeFragColor(vec4 result, sampler2D tex, sampler2D filtering, sampler2D secondFiltering, int index)
{
	vec4 colorValue = texture2D(tex, gl_TexCoord[0].st);
	float val = textureToValue(colorValue);
	
	float contrast = 0.0;
	float ombreGradient = 0.0;
	
	//gestion du multitexturing
	if(gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s >= 1.0 || gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t >= 1.0)
	{
		return result;
	}
	
	if(colorValue.a < 1.0)
		return result;
		
	if(useTextureFilter == 1.0)
	{
		vec4 colorFiltrage = texture2D(filtering, gl_TexCoord[0].st);
		float filterValue = textureToValue(colorFiltrage);
		if(filterValue < filterMinValue || filterValue > filterMaxValue)
			return result;
	}
	
	if(useTextureSecondFilter == 1.0)
	{
		vec4 colorFiltrage = texture2D(secondFiltering, gl_TexCoord[0].st);
		float filterValue = textureToValue(colorFiltrage);
		if(filterValue < secondFilterMinValue || filterValue > secondFilterMaxValue)
			return result;
	}
	
	if(filterName == 0.0 && sigmaGauss != 0.0)
	{
		//filtrage gaussien pour filtrer le bruit
		//offset a 0 car l'offset sert au sein d'une convolution
		vec2 offset = vec2(0.0, 0.0);
		val = gaussianFilter(tex, offset);
	}
	else if(filterName == 1.0 && filterWidth*filterHeight>=3.0)
	{
		val = alphaLinear(tex);
	}
	else if(filterName == 2.0 && filterWidth*filterHeight>=3.0)
	{
		val = leeFilter(tex);
	}
	else if(filterName == 3.0 && filterWidth*filterHeight>=3.0)
	{
		val = wienerFilter(tex);
	}
		
	if(ombrage == 1.0)
	{	
		//calcul de l'ombre
		ombreGradient = ombrageGradient(tex);
		
		//transparent neighborhood must be discared
		if(10000.0 != ombreGradient)
		{
		}
		else
			return result;
	}
	
	if(gradient == 1.0)
	{
		//float val = (ombre-min)/(max-min);
		result.r = ombreGradient*opacity;
		result.g = ombreGradient*opacity;
		result.b = ombreGradient*opacity;
		
		//gestion de l'opacite
		result.a = opacity;
	}
	else
	{
		float min_cmp = min+offset;
		float max_cmp = max+offset;
		if(logColormap == 1.0)
		{
			contrast = (val-min_cmp)*255.0;
			if(contrast<0.1)
				contrast = 0.1;
			contrast = (log(contrast)-log(0.1))/(log((max_cmp-min_cmp)*255.0)-log(0.1));
		}
		else
			contrast = (val - min_cmp)/(max_cmp-min_cmp);
		
		//inversion de l'intensite?
		if(inverseColormap == 1.0)
		{
			contrast = 1.0 - contrast;
		}
		
		if(contrast < 0.0)
		{
			contrast = 0.0;
		}
		if(contrast > 1.0)
		{
			contrast = 1.0;
		}
		
		//colormap
		result = computeColorIndex(contrast);
		
		//application de l'ombrage
		if(ombrage == 1.0)
		{	
			result *= ombreGradient;						
		}
		
		if(minTransparency>val || val>maxTransparency)
		{
			result.a = 0.0;
		}
		else
		{
			//gestion de l'opacite
			result.a = opacity;
		}
	}
	return result;
}


/* Compute the frag color for a mosaicking texture ( dont need the filter texture) */
vec4 computeMosaicFragColor(vec4 result, sampler2D tex, int index)
{
	vec4 colorValue = texture2D(tex, gl_TexCoord[0].st);
	float val = textureToValue(colorValue);
	
	float contrast = 0.0;
	float ombreGradient = 0.0;
	
	//gestion du multitexturing
	if(gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s >= 1.0 || gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t >= 1.0)
	{
		return result;
	}
	
	if(colorValue.a < 1.0)
		return result;
		
	
	if(filterName == 0.0 && sigmaGauss != 0.0)
	{
		//filtrage gaussien pour filtrer le bruit
		//offset a 0 car l'offset sert au sein d'une convolution
		vec2 offset = vec2(0.0, 0.0);
		val = gaussianFilter(tex, offset);
	}
	else if(filterName == 1.0 && filterWidth*filterHeight>=3.0)
	{
		val = alphaLinear(tex);
	}
	else if(filterName == 2.0 && filterWidth*filterHeight>=3.0)
	{
		val = leeFilter(tex);
	}
	else if(filterName == 3.0 && filterWidth*filterHeight>=3.0)
	{
		val = wienerFilter(tex);
	}
		
	if(ombrage == 1.0)
	{	
		//calcul de l'ombre
		ombreGradient = ombrageGradient(tex);
		
		//transparent neighborhood must be discared
		if(10000.0 != ombreGradient)
		{
		}
		else
			return result;
	}
	
	if(gradient == 1.0)
	{
		//float val = (ombre-min)/(max-min);
		result.r = ombreGradient*opacity;
		result.g = ombreGradient*opacity;
		result.b = ombreGradient*opacity;
		
		//gestion de l'opacite
		result.a = opacity;
	}
	else
	{
		float min_cmp = min+offset;
		float max_cmp = max+offset;
		if(logColormap == 1.0)
		{
			contrast = (val-min_cmp)*255.0;
			if(contrast<0.1)
				contrast = 0.1;
			contrast = (log(contrast)-log(0.1))/(log((max_cmp-min_cmp)*255.0)-log(0.1));
		}
		else
			contrast = (val - min_cmp)/(max_cmp-min_cmp);
		
		//inversion de l'intensite?
		if(inverseColormap == 1.0)
		{
			contrast = 1.0 - contrast;
		}
		
		if(contrast < 0.0)
		{
			contrast = 0.0;
		}
		if(contrast > 1.0)
		{
			contrast = 1.0;
		}
		
		//colormap
		result = computeColorIndex(contrast);
		
		//application de l'ombrage
		if(ombrage == 1.0)
		{	
			result *= ombreGradient;						
		}
		
		if(minTransparency>val || val>maxTransparency)
		{
			result.a = 0.0;
		}
		else
		{
			//gestion de l'opacite
			result.a = opacity;
		}
	}
	return result;
}
void main()
{
	vec4 result = vec4(0.0,0.0,0.0,0.0);
	
	if (nbTexture == 1) {
		result = computeFragColor(result, Texture0, TextureFilter0, TextureSecondFilter0, 0);
	} else {
		/* //TODO ameliorer la gestion du nombre de texture en fonction du mosacking / usefilter....
		if (useTextureFilter == 1 || useTextureFilter == 1) {
			if(nbTexture>1) {
				result = computeFragColor(result, Texture0, TextureFilter0, TextureSecondFilter0, 0);
				result = computeFragColor(result, Texture1, TextureFilter1, TextureSecondFilter1, 1);
			}	
			if(nbTexture>2) {
				result = computeFragColor(result, Texture2, TextureFilter2, TextureSecondFilter2, 2);
			}
			if(nbTexture>3) {
				result = computeFragColor(result, Texture3, TextureFilter3, TextureSecondFilter3, 3);
			}
			if(nbTexture>4) {
				result = computeFragColor(result, Texture4, TextureFilter4, TextureSecondFilter4, 4);
			}
		} else {
		*/
			if(nbTexture>1)
			{
				result = computeMosaicFragColor(result, Texture1, 1);
			}	
			if(nbTexture>2) {
				result = computeMosaicFragColor(result, Texture2, 2);
			}
			if(nbTexture>3) {
				result = computeMosaicFragColor(result, Texture3, 3);
			}
			if(nbTexture>4) {
				result = computeMosaicFragColor(result, Texture4, 4);
			}
			/*
			if(nbTexture>5) {
				result = computeMosaicFragColor(result, Texture5, 5);
			}
			if(nbTexture>6) {
				result = computeMosaicFragColor(result, Texture6, 6);
			}
			if(nbTexture>7) {
				result = computeMosaicFragColor(result, Texture7, 7);
			}
			if(nbTexture>8) {
				result = computeMosaicFragColor(result, Texture8, 8);
			}
			if(nbTexture>9) {
				result = computeMosaicFragColor(result, Texture9, 9);
			}
			if(nbTexture>10) {
				result = computeMosaicFragColor(result, Texture10, 10);
			}
			if(nbTexture>11) {
				result = computeMosaicFragColor(result, Texture11, 11);
			}
			if(nbTexture>12) {
				result = computeMosaicFragColor(result, Texture12, 12);
			}
			if(nbTexture>13) {
				result = computeMosaicFragColor(result, Texture13, 13);
			}
			if(nbTexture>14) {
				result = computeMosaicFragColor(result, Texture14, 14);
			}
			if(nbTexture>15) {
				result = computeMosaicFragColor(result, Texture15, 15);
			}
			*/
		//}
	} 






			
	gl_FragColor.r = result.r;
	gl_FragColor.g = result.g;
	gl_FragColor.b = result.b;
	
	//gestion de l'opacite
	gl_FragColor.a = result.a;
	 
}