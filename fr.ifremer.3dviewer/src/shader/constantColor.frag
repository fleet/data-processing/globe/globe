#version 120

uniform vec4 color;
uniform float opacity;

void main()
{
	gl_FragColor = color;
	gl_FragColor.a = opacity;
}
