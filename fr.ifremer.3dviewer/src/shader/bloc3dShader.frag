uniform sampler3D tex;
uniform float min;
uniform float max;
uniform float colormap;
uniform float colormapOri;


void main()
{
	vec4 colorOri = texture3D(tex, gl_TexCoord[0].xyz);
	float intensity = 0.0;
	float contrast = 0.0;

	if(colorOri.a != 0.0)
	{
		if(colormapOri == 0.0) //jet colormap
		{		
			intensity = (colorOri.r+colorOri.g+colorOri.b)/3.0;	
		}		
		contrast = (intensity - min)/(max-min);
		
		if(colormap == 0.0) //jet colormap
		{
			if(contrast <= 31.0/255.0) //index entre 0 et 31
			{
					gl_FragColor.r = 0.0;
					gl_FragColor.g = 0.0;
					gl_FragColor.b = 0.531250 + contrast*(1-0.531250)/31.0*255.0;
			}
			else if(contrast <= 95.0/255.0) //index entre 32 et 95
			{
					gl_FragColor.r = 0.0;
					gl_FragColor.g = (contrast-32.0/255.0)*255.0/(95.0-32.0);
					gl_FragColor.b = 1.0;		
			}
			else if(contrast <= 159.0/255.0) //index entre 96 et 159
			{
					gl_FragColor.r = (contrast-96.0/255.0)*255.0/(159.0-96.0);
					gl_FragColor.g = 1.0;
					gl_FragColor.b = 1.0 - (contrast-96.0/255.0)*255.0/(159.0-96.0);	
			}
			else if(contrast <= 223.0/255.0) //index entre 160 et 223
			{
					gl_FragColor.r = 1.0;
					gl_FragColor.g = 1.0 - (contrast-160.0/255.0)*255.0/(223.0-160.0);
					gl_FragColor.b = 0.0;		
			}
			else //index entre 224 et 255
			{
					gl_FragColor.r = 1.0 - (contrast-224.0/255.0)*0.5*255.0/(255.0-224.0);
					gl_FragColor.g = 0.0;
					gl_FragColor.b = 0.0;
			}
		}
		if(intensity<0.01){gl_FragColor.a = 0.0;}
		else{gl_FragColor.a = 1.0;	;	}
	}
	else
	 {
	   gl_FragColor = colorOri;
	 }
}