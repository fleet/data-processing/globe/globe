// factor of z exaggeration. 0 to fit the surface
uniform float verticalExaggeration;

// 	Offset to apply on x axis for each vertex
uniform float xOffset;
// 	Offset to apply on y axis for each vertex
uniform float yOffset;
// 	Offset to apply on z axis for each vertex
uniform float zOffset;

void main()
{
	vec4 point = gl_Vertex;
	point.x += xOffset;
	point.y += yOffset;
	point.z += zOffset;
	point.z *= verticalExaggeration;
    gl_Position = gl_ModelViewProjectionMatrix * point;
} 