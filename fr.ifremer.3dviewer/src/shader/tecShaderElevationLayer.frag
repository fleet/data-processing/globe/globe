#version 120
uniform sampler2D tex;
uniform float min;
uniform float max;
uniform float minTransparency;
uniform float maxTransparency;
uniform float globalMin; //minEncoding value
uniform float globalMax; //maxEncoding value

uniform float isRGB;
uniform float displaySubsidence;

uniform sampler2D ageGrid;
uniform int hasAgeGrid;
uniform float age;

uniform int colorMapLength;
uniform int colormap[256];
uniform float logColormap;

uniform float azimuth;
uniform float zenith;
uniform float ombrageExaggeration;
uniform float cellSize;

uniform float ombrage;
uniform float width;
uniform float height;
uniform float inverseColormap;
uniform float gradient;
uniform float ombrageLogarithmique;

uniform float Lookup;

uniform float scale;
uniform float offsetAuto;
uniform float constantOffset;

uniform float resolution;

uniform float highlight;

//for data other than reflectivity, toEnergy should be 0
uniform float toEnergy;

uniform float opacity;

//compute deplacement if needed
uniform float computeMovement;

//deplacement Matrix
uniform float m11;
uniform float m12;
uniform float m13;
uniform float m14;
uniform float m21;
uniform float m22;
uniform float m23;
uniform float m24;
uniform float m31;
uniform float m32;
uniform float m33;
uniform float m34;
uniform float m41;
uniform float m42;
uniform float m43;
uniform float m44;

//texture bounding box
uniform float latitudeNorth;
uniform float latitudeSouth;
uniform float longitudeWest;
uniform float longitudeEast;

//original position
uniform float originalLatitudeNorth;
uniform float originalLatitudeSouth;
uniform float originalLongitudeWest;
uniform float originalLongitudeEast;

//////////   Utils  ///////////
// Extract the age of the current cell
float getCellAge(vec2 st) {
	float cellAge = -1.0;
	if (hasAgeGrid > 0.0) {
		// Unpack age (see TecShaderSurfaceTileRenderer.copyAgeToRaster)
		vec4 maskColor = texture2D(ageGrid, st);
		maskColor = maskColor * 256.0;
		cellAge = maskColor.r * 256.0 + maskColor.g;
	}
	return cellAge;
}

//compute the index of colormap according to
//contrast and colorMapLength
vec4 computeColor(float contrast) {
	vec4 demultiplexed = vec4(0.0, 0.0, 0.0, 1.0);
	int index = int((colorMapLength - 1.0) * contrast);
	if (index < 0)
		index = 0;
	if (index > colorMapLength - 2)
		index = colorMapLength - 2; //FIXME why 2 and not 1?
	int colorMultiplexed = colormap[index];

	//red == MSB
	float r = floor(colorMultiplexed / 256.0 / 256.0);
	demultiplexed.r = r * opacity / 255.0;

	//green
	float g = floor((colorMultiplexed - r * 256.0 * 256.0) / 256.0);
	demultiplexed.g = g * opacity / 255.0;

	//blue
	float b = floor(colorMultiplexed - r * 256.0 * 256.0 - g * 256.0);
	demultiplexed.b = b * opacity / 255.0;

	return demultiplexed;
}

//extract the 24 bits value coded as RGB in the texture
float getElevation(vec4 textureRGBA, float cellAge) {
	// See TecImageRasterReader.doRead
	float mixedElevation = (textureRGBA.r * 256.0 * 256.0
			+ textureRGBA.g * 256.0 + textureRGBA.b) * 255.0
			/ (256.0 * 256.0 * 256.0);
	// Compute real elevation
	float elevation = globalMin + (mixedElevation * (globalMax - globalMin));
	// Compute elevation offset
	float offset = constantOffset;
	if (cellAge >= age && offsetAuto > 0.0) {
		// Parson & Sclater 1977 law 
		offset = (350 * (sqrt(cellAge) - sqrt(cellAge - age)));
	}

	if (displaySubsidence == 1.0) {
		elevation = cellAge;
	} else if (displaySubsidence == 2.0) {
		elevation = offset;
	} else {
		elevation = elevation + offset;
	}

	return elevation;
}

//////////   End Utils  ///////////

//fonction realisant l'ombrage par mesure de la pente
//grace a un filtre de sobel
float ombrageGradient(vec2 st) {
	float sobel[9];
	float pi = 3.14;
	float ombre = 0.0;

	float neighborElevation;

	float dx = 0.0;
	float dy = 0.0;

	sobel[0] = -1.0;
	sobel[1] = -2.0;
	sobel[2] = -1.0;
	sobel[3] = 0.0;
	sobel[4] = 0.0;
	sobel[5] = 0.0;
	sobel[6] = 1.0;
	sobel[7] = 2.0;
	sobel[8] = 1.0;

	dx = 0.0;
	dy = 0.0;

	//Astuce pour les bordures, on fait le calcul sur un pixel qui ne presente pas de probleme
	//d ou offset de +/-2
	float trickX = 0.0;
	float trickY = 0.0;
	//border left
	if (st.s <= 1.0 / width) {
		trickX = 1.0 / width;
	}

	//border right
	if (st.s >= 1.0 - 1.0 / width) {
		trickX = -1.0 / width;
	}

	//border top
	if (st.t <= 1.0 / height) {
		trickY = 1.0 / height;
	}

	//border bottom
	if (st.t >= 1.0 - 1.0 / height) {
		trickY = -1.0 / height;
	}

	//offset trick border
	vec2 trickOffset = vec2(trickX, trickY);

	//determination de la pente par gradient de sobel
	float is_xBorder = 0.0;
	float is_yBorder = 0.0;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			vec2 neighborOffset = vec2(float(i - 1) / width,
					float(j - 1) / height);
			vec2 neighborCoords = st.st + neighborOffset + trickOffset;
			vec4 neighborColor = texture2D(tex, neighborCoords);

			float border = 0.0;
			if (st.s + neighborOffset.s + trickOffset.s < 0.0
					|| st.s + neighborOffset.s + trickOffset.s >= 1.0) {
				is_xBorder = 1.0;
				border = 1.0;
			}
			if (st.t + neighborOffset.t + trickOffset.t < 0.0
					|| st.t + neighborOffset.t + trickOffset.t >= 1.0) {
				is_yBorder = 1.0;
				border = 1.0;
			}

			if (border == 0.0) {
				float neighborAge = getCellAge(neighborCoords);
				neighborElevation = getElevation(neighborColor, neighborAge);
				// little hack to retrieve shading like it was before refactoring of this shader file
				neighborElevation = (neighborElevation - globalMin)
						/ (globalMax - globalMin);
				//normalisation de l'elevation
				if (neighborColor.a == 1.0) {
					//ombrage logarithmique
					if (ombrageLogarithmique == 1.0) {
						//gestion log(0)
						if (abs(neighborElevation) < 0.001)
							neighborElevation = sign(neighborElevation) * 0.001;

						neighborElevation = sign(neighborElevation)
								* log(abs(neighborElevation)) / log(10.0);
					}

					//pentes x et y
					dx += neighborElevation * sobel[i + 3 * j];
					dy += neighborElevation * sobel[3 * i + j];
				}
			}
		}
	}

	if (is_xBorder == 1.0) {
		dx = dx * 2.0 * resolution;
	}
	if (is_yBorder == 1.0) {
		dy = dy * 2.0 * resolution;
	}

	////////////////////////////////////////////
	float rayon_zenithal = radians(zenith);

	float azimuth_mathematique = azimuth + 180.0;
	if (azimuth_mathematique >= 360.0) {
		azimuth_mathematique = azimuth_mathematique - 360.0;
	}
	float rayon_azimutal = radians(azimuth_mathematique);

	float dzdx = dx / (8.0 * cellSize);
	float dzdy = dy / (8.0 * cellSize);

	float rayon_pente = atan(
			ombrageExaggeration * sqrt(dzdx * dzdx + dzdy * dzdy));

	float rayon_exposition = 0.0;

	if (dzdx != 0.0) {
		rayon_exposition = atan(dzdy, -dzdx);
		if (rayon_exposition < 0.0) {
			rayon_exposition = 2.0 * pi + rayon_exposition;
		}
	} else if (dzdx == 0.0) {
		if (dzdy > 0.0) {
			rayon_exposition = pi / 2.0;
		} else if (dzdy < 0.0) {
			rayon_exposition = 2.0 * pi - pi / 2.0;
		} else {
			rayon_exposition = rayon_exposition;
		}
	}

	//calcul de l'ombre en fonction de la pente
	ombre = ((cos(rayon_zenithal) * cos(rayon_pente))
			+ (sin(rayon_zenithal) * sin(rayon_pente)
					* cos(rayon_azimutal - rayon_exposition)));

	if (ombre < 0.0) {
		ombre = 0.0;
	}

	return ombre;
}

//////////   Movement /////////

//computes the real texCoord from gl_TexCoord[0].st
//return the coords as vec2
vec2 move() {
	if (computeMovement == 0.0)
		return gl_TexCoord[0].st;

	float latitude = (1.0 - gl_TexCoord[0].t) * (latitudeNorth - latitudeSouth)
			+ latitudeSouth;
	float longitude = gl_TexCoord[0].s * (longitudeEast - longitudeWest)
			+ longitudeWest;

	//PtGeoGtoGeoC
	float x = 0.0, y = 0.0, z = 0.0;
	if (abs(latitude) + abs(longitude) == 0.0) {
		x = 0.0;
		y = 0.0;
		z = 0.0;
	} else if (abs(latitude) == 90.0) {
		x = 0.0;
		y = 0.0;
		z = -1.0;
		if (latitude > 0.0)
			z = 1.0;
	} else {
		latitude = radians(latitude);
		longitude = radians(longitude);
		float elat = atan(0.9933 * tan(latitude));
		x = cos(elat) * cos(longitude);
		y = cos(elat) * sin(longitude);
		z = sin(elat);
	}

	//performesRotation
	float xm = m11 * x + m12 * y + m13 * z;
	float ym = m21 * x + m22 * y + m23 * z;
	float zm = m31 * x + m32 * y + m33 * z;

	//PtGeoCtoGeoG
	//latitude = 0.0;
	//longitude = 0.0;
	if (abs(xm) + abs(ym) + abs(zm) == 0.0) {
		latitude = 0.0;
		longitude = 0.0;
	} else if (abs(zm) == 1.0) {
		latitude = -90.0;
		if (zm > 0.0)
			latitude = 90.0;
	} else {
		latitude = atan(zm / sqrt(1.0 - zm * zm) / 0.9933);
		latitude = degrees(latitude);
		if (xm == 0.0) {
			longitude = -90.0;
			if (ym > 0.0)
				longitude = -90.0;
		} else {
			longitude = degrees(atan(ym, xm));
		}
	}

	float realS = (longitude - originalLongitudeWest)
			/ (originalLongitudeEast - originalLongitudeWest);
	float realT = 1.0
			- (latitude - originalLatitudeSouth)
					/ (originalLatitudeNorth - originalLatitudeSouth);
	return vec2(realS, realT);
}
//////////  End Movement /////////

void main() {
	vec2 st = move();

	// Applying mask grid
	float cellAge = getCellAge(st);
	if (cellAge >= 0.0 && cellAge < age) {
		discard;
	}

	vec4 colorOri = texture2D(tex, st);
	if (highlight == 1.0) {
		gl_FragColor.r = 1.0;
		gl_FragColor.g = 1.0;
		gl_FragColor.b = 1.0;
		return;
	}

	float colorMapCoordinate = 0.0;
	float ombreGradient = 0.0;

	//gestion du multitexturing
	//suppression de l'interpolation en alpha sur les bords des tuiles
	if (gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s >= 1.0
			|| gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t >= 1.0 || st.s < 0.0
			|| st.s >= 1.0 || st.t < 0.0 || st.t >= 1.0 || colorOri.a == 0.0) {
		gl_FragColor = colorOri;
		gl_FragColor.a = 0.0;
	} else if (isRGB == 1.0) {
		gl_FragColor = colorOri;
		gl_FragColor.a = opacity;
	} else {
		float elevation = getElevation(colorOri, cellAge);

		if (ombrage == 1.0) {
			//calcul de l'ombre
			ombreGradient = ombrageGradient(st);
		}

		if (gradient == 1.0) {
			gl_FragColor.r = ombreGradient * opacity;
			gl_FragColor.g = ombreGradient * opacity;
			gl_FragColor.b = ombreGradient * opacity;

			//gestion de l'opacite
			gl_FragColor.a = opacity;
		} else {
			if (logColormap == 1.0) {
				colorMapCoordinate = (elevation - min) * 255.0;
				if (colorMapCoordinate < 0.1)
					colorMapCoordinate = 0.1;
				colorMapCoordinate = (log(colorMapCoordinate) - log(0.1))
						/ (log((max - min) * 255.0) - log(0.1));
			} else
				colorMapCoordinate = (elevation - min) / (max - min);

			//inversion de l'intensite?
			if (inverseColormap == 1.0) {
				colorMapCoordinate = 1.0 - colorMapCoordinate;
			}

			if (colorMapCoordinate < 0.0) {
				colorMapCoordinate = 0.0;
			}
			if (colorMapCoordinate > 1.0) {
				colorMapCoordinate = 1.0;
			}

			//changement de colormap
			gl_FragColor = computeColor(colorMapCoordinate);

			//application de l'ombrage
			if (ombrage == 1.0) {
				gl_FragColor *= ombreGradient;
			}

			/* FT5412 Ajout d'un delta pour parer aux impr�cisions de calculs */
			/**this old precision of 1e-3 was not compatible with globe dtm, since min/max  global values are higher*/
			float delta = 1e-7;

			if (elevation < minTransparency - delta
					|| elevation > maxTransparency + delta) {
				gl_FragColor.a = 0.0;
			} else {
				//gestion de l'opacite
				gl_FragColor.a = opacity;
			}
		}
	}
}
