#version 120

varying vec4 vTexCoord;

void main()
{
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	vTexCoord = gl_MultiTexCoord0;
}
