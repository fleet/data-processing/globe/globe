#version 150 compatibility
precision highp float;

layout(points) in;
layout(points, max_vertices = 1) out;

in vec4 vcolor[];
out vec4 color;

uniform vec4 alongVector;
uniform vec4 acrossVector;
in float gAlongAngle[];
in float gAcrossAngle[];
uniform vec4 topPoint;			// Coordinates of the water column top point
in float gHeight[]; // displayed height of an echo



void main()
{

	int i;
	// fake loop, since input is point, length=1
	for ( i=0; i < gl_in.length(); i++) {

		vec4 pos=gl_in[i].gl_Position;

		color=vcolor[i];
		gl_Position =  gl_ModelViewProjectionMatrix*pos; //center
		EmitVertex();

	}


	EndPrimitive();
}
