#version 120

//La texture contenant les valeurs colonne d'eau
uniform sampler2D texture;
//La palette de couleur
uniform sampler2D palette;
//La texture contenant les valeurs médiannes
uniform sampler2D medianValues;
//La texture contenant les valeurs max
uniform sampler2D maxValues;

uniform float opacity;
uniform float minVisibleValue;
uniform float maxVisibleValue;
uniform float minPaletteValue;
uniform float maxPaletteValue;
uniform int medianFilterEnabled;
uniform int maxValueFilterEnabled;
uniform float maxValueFilterLevel;

void main()
{
	vec4 textureColor = texture2D(texture, gl_TexCoord[0].xy).rgba;

	// La valeur de la donnée à représenter (réduite entre 0 et 1)  est stockée dans la composante rouge de la texture
	float dataValue = textureColor.r ;

	float alpha = opacity;

	if(medianFilterEnabled == 1)
	{
		float medianValue = texture2D(medianValues, gl_TexCoord[0].xy).r;
		dataValue = textureColor.r - medianValue;

		// On applique le filtrage éventuel
		float modifiedMinVisibleValue = minVisibleValue - medianValue;
		float modifiedMaxVisibleValue = maxVisibleValue - medianValue;
		if (dataValue < modifiedMinVisibleValue)
			discard;
		else if (dataValue > modifiedMaxVisibleValue)
			discard;

		// On recale la palette en fonction des paramètres
		float modifiedMinPaletteValue = minPaletteValue - medianValue;
		float modifiedMaxPaletteValue = maxPaletteValue - medianValue;
		if(modifiedMinPaletteValue <0)
		{
		modifiedMinPaletteValue = 0;
		}
		dataValue = (dataValue - modifiedMinPaletteValue )/(modifiedMaxPaletteValue-modifiedMinPaletteValue);
	}
	else
	{
		if(maxValueFilterEnabled == 1)
		{
			float maxValue = texture2D(maxValues, gl_TexCoord[0].xy).r;
			dataValue = textureColor.r;
			if(dataValue < maxValue - maxValueFilterLevel )
			{
				dataValue = 0.0f;
				discard;
			}
		}
		if (dataValue < minVisibleValue)
			discard;
		else if (dataValue > maxVisibleValue)
			discard;

		// On recale la palette en fonction des paramètres
		dataValue = (dataValue - minPaletteValue)/(maxPaletteValue-minPaletteValue);
	}

	if(dataValue < 0.0)
	{
		dataValue = 0.0;
	}
	if(dataValue > 1.0)
	{
		dataValue = 1.0;
	}

	// On map cette valeur sur la palette pour récupérer la couleur correspondante
	vec3 paletteColor = texture2D(palette, vec2(dataValue, 0)).rgb;

	gl_FragColor = vec4(paletteColor.r, paletteColor.g, paletteColor.b, alpha);
}

