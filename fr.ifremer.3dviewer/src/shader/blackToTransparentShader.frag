uniform sampler2D tex;

void main()
{
	vec4 colorOri = texture2D(tex, gl_TexCoord[0].st);
	
	if(gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s >= 1.0 || gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t >= 1.0)
	{
	   gl_FragColor.r = 0.0;
	   gl_FragColor.g = 0.0;
	   gl_FragColor.b = 0.0;
	   gl_FragColor.a = 0.0;
	}
	else if(colorOri.a == 0.0 || colorOri.r != 0.0 || colorOri.b != 0.0 || colorOri.g != 0.0)
	{
		gl_FragColor = colorOri;
	}
	else
	{
	   gl_FragColor.r = 0.0;
	   gl_FragColor.g = 0.0;
	   gl_FragColor.b = 0.0;
	   gl_FragColor.a = 0.0;
	}
}