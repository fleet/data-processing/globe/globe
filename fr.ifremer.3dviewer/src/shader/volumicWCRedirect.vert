#version 400 compatibility

uniform float opacity;

out vec4 vcolor;
attribute float amplitude;
attribute float beamWidthAcross;
attribute float beamWidthAlong;
attribute float height;

varying float gAlongAngle;
varying float gAcrossAngle;
varying float gHeight;

uniform float minValue;
uniform float resolution;

// float to uint
uint encode(float value)
{
	return uint((amplitude - minValue) / resolution);
}

// rgba8Unorm to vec4
vec4 unpack(uint value)
{
	return unpackUnorm4x8(value);
}

void main()
{
	vcolor= unpack(encode(amplitude));
	gAcrossAngle = beamWidthAcross;
	gAlongAngle = beamWidthAlong;
	gHeight = height;
	gl_Position = gl_Vertex;
}
