#version 120

uniform sampler2D palette;

uniform float opacity;
uniform float minContrastValue;
uniform float maxContrastValue;
uniform float inverseColormap;

varying float colorData;

void main()
{
	// Apply the contrast settings	
	float contrastedColorData = (colorData - minContrastValue)/(maxContrastValue - minContrastValue);
	
	// Apply the inverseColormap settings
	if(inverseColormap == 1.0)
	{
		contrastedColorData = 1.0 - contrastedColorData;
	}
	
	// Get the color of amplification from the palette choosen by the user
	vec3 paletteColor = texture2D(palette, vec2(contrastedColorData, 0)).rgb;
	
	// Apply this color to the fragment which will be managed by OpenGL
	gl_FragColor = vec4(paletteColor.r, paletteColor.g, paletteColor.b, opacity);
}