uniform sampler2D tex;
uniform float min;
uniform float max;
uniform float colormap;
uniform float cosIntensiteOmbrage;
uniform float sinIntensiteOmbrage;
uniform float cosOmbrage;
uniform float sinOmbrage;
uniform float ombrage;
uniform float width;
uniform float height;
uniform float inverseColormap;
uniform float gradient;

void main()
{
	vec4 colorOri = texture2D(tex, gl_TexCoord[0].st);
	float intensity = 0;
	float contrast = 0.0;
	vec4 filterX[9];
	vec4 filterY[9];
	float sobel[9];
	float pi = 3.14;
	float elevation = 30.0;
	float ombre = 0.0;
	float scalaire = 0.0;
	

	vec4 dx;
	vec4 dy;

	float r;
	float g;
	float b;
	
	sobel[0] = -1.0/10.0;
	sobel[1] = -2.0/10.0;
	sobel[2] = -1.0/10.0;
	sobel[3] = 0.0/10.0;
	sobel[4] = 0.0/10.0;
	sobel[5] = 0.0/10.0;
	sobel[6] = 1.0/10.0;
	sobel[7] = 2.0/10.0;
	sobel[8] = 1.0/10.0;
	
	if(gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s > 1.0 || gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t > 1.0)
	{
		gl_FragColor = colorOri;
		gl_FragColor.a = 0.0;
	}
	else
	{		
		if(colorOri.r == colorOri.g && colorOri.r == colorOri.b)
		{
			if(ombrage == 1.0)
			{
				filterY[0].rgba = sobel[0]; filterX[0].rgba =  sobel[0];
				filterY[1].rgba = sobel[3]; filterX[1].rgba =  sobel[1];
				filterY[2].rgba = sobel[6]; filterX[2].rgba =  sobel[2];
				filterY[3].rgba = sobel[1]; filterX[3].rgba =  sobel[3];
				filterY[4].rgba = sobel[4]; filterX[4].rgba =  sobel[4];
				filterY[5].rgba = sobel[7]; filterX[5].rgba =  sobel[5];
				filterY[6].rgba = sobel[2]; filterX[6].rgba =  sobel[6];
				filterY[7].rgba = sobel[5]; filterX[7].rgba =  sobel[7];
				filterY[8].rgba = sobel[8]; filterX[8].rgba =  sobel[8];
			
				dx = vec4(0.0);
				dy = vec4(0.0);
				for(int i=0; i<3; i++)
				{
					for(int j=0; j<3; j++)
					{
						vec2 offset = vec2((i-1)/width, (j-1)/height);

						vec4 tmp = texture2D(tex, gl_TexCoord[0].st + offset);
						
						dx += tmp * filterX[i+3*j];
						dy += tmp * filterY[i+3*j];
						
					}
				}
				dx.r = dx.r * 50.0;
				dy.r = dy.r * 50.0;
				scalaire = sqrt(dx.r*sinOmbrage*sinIntensiteOmbrage + dy.r*cosOmbrage*sinIntensiteOmbrage + cosIntensiteOmbrage)/sqrt(dx.r*dx.r+dy.r*dy.r+1.0);
				ombre = pi / 2.0 - scalaire - scalaire * scalaire * scalaire / 6.0;//acos(scalaire);
				ombre = ombre*ombre;//teta_carre
				ombre = ombre*ombre;// optimisation pour mac laurin
				ombre = 1.0 -  ombre/2.0 + ombre*ombre/24.0;//cos(ombre*ombre);
				ombre = 0.5 + ombre * 0.5; 
			}
			
			if(gradient == 1.0)
			{
			    r = ombre;
				g = ombre;
				b = ombre;
			}
			else
			{
				intensity = colorOri.r;
					
				contrast = (intensity - min)/(max-min);
				
				if(inverseColormap == 1.0)
				{
					contrast = 1.0 - contrast;
				}
				
				if(colormap < 0.1) //jet colormap
				{
					if(contrast <= 31.0/255.0) //index entre 0 et 31
					{
						r = 0;
						g = 0;
						b = 0.531250 + contrast*(1-0.531250)/31.0*255.0;
					}
					else if(contrast <= 95.0/255.0) //index entre 32 et 95
					{
						r = 0;
						g = (contrast-32.0/255.0)*255.0/(95.0-32.0);
						b = 1;
					}
					else if(contrast <= 159.0/255.0) //index entre 96 et 159
					{
						r = (contrast-96.0/255.0)*255.0/(159.0-96.0);
						g = 1.0;
						b = 1.0 - (contrast-96.0/255.0)*255.0/(159.0-96.0);
					}
					else if(contrast <= 223.0/255.0) //index entre 160 et 223
					{
						r = 1.0;
						g = 1.0 - (contrast-160.0/255.0)*255.0/(223.0-160.0);
						b = 0;
					}
					else //index entre 224 et 255
					{
						r = 1.0 - (contrast-224.0/255.0)*0.5*255.0/(255.0-224.0);
						g = 0;
						b = 0;
					}
				}
				else if(colormap < 1.1) //catherine
				{
					if(contrast <= 35.0/255.0)//R
					{
						r = 0.0391 + contrast*255.0/35.0*(0.0976-0.0391);
					}
					else if(contrast <= 63.0/255.0)//R
					{
						r = 0.1035 + (contrast*255.0-36.0)/(63.0-36.0)*(0.2634-0.1035);
					}
					else if(contrast <= 116.0/255.0)//R
					{
						r = 0.267 + (contrast*255.0-64.0)/(116.0-64.0)*(0.451-0.267);
					}
					else if(contrast <= 160.0/255.0)//R
					{
						r = 0.4634 + (contrast*255.0-117.0)/(160.0-117.0)*(0.9961-0.4634);
					}
					else //R
					{
						r = 0.9944 + (contrast*255.0-161.0)/(255.0-161.0)*(0.8392-0.9944);
					}
					
					if(contrast <= 63.0/255.0)//G
					{
						g = contrast*255.0/63.0*0.7876;
					}
					else if(contrast <= 160.0/255.0)//G
					{
						g = 0.7901 + (contrast*255.0-64.0)/(160.0-64.0)*(0.9882-0.7901);
					}
					else if(contrast <= 206.0/255.0)//G
					{
						g = 0.9842 + (contrast*255.0-161.0)/(206.0-161.0)*(0.8052-0.9842);
					}
					else//G
					{
						g = 0.7933 + (contrast*255.0-206.0)/(255.0-206.0)*(0.2-0.8052);
					}
					
					if(contrast <= 35.0/255.0)//B
					{
						b = 0.4727 + contrast*255.0/35.0*(0.94-0.4727);
					}
					else if(contrast <= 63.0/255.0)//B
					{
						b = 0.942 + (contrast*255.0-36.0)/(63.0-36.0)*(0.9961-0.942);
					}
					else if(contrast <= 116.0/255.0)//B
					{
						b = 0.9891 + (contrast*255.0-64.0)/(116.0-64.0)*(0.6275-0.9891);
					}
					else if(contrast <= 160.0/255.0)//B
					{
						b = 0.6241 + (contrast*255.0-117.0)/(160.0-117.0)*(0.4784-0.6241);
					}
					else if(contrast <= 206.0/255.0)//B
					{
						b = 0.4766 + (contrast*255.0-161.0)/(206.0-161.0)*(0.3902-0.4766);
					}
					else//B
					{
						b = 0.3824 + (contrast*255.0-206.0)/(255.0-206.0)*(0.0-0.3824);
					}
				}
				else //grayscale
				{
					r = contrast;
					g = contrast;
					b = contrast;
				}
			}
			
			if(ombrage == 1.0 && gradient != 1.0)
			{	
				gl_FragColor.r = r * ombre;
				gl_FragColor.g = g * ombre;
				gl_FragColor.b = b * ombre;									
			}
			else
			{
				gl_FragColor.r = r;
				gl_FragColor.g = g;
				gl_FragColor.b = b;
			}
						
			gl_FragColor.a = colorOri.a;	
		}
		else
		 {
		   gl_FragColor = colorOri;
		 }
	 }
	 
	 gl_FragColor = gl_FragColor * gl_FrontColor.a;
}