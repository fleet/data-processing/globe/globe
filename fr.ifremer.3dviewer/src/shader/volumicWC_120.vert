#version 120

uniform sampler2D palette;
uniform float opacity;
uniform float minContrast;
uniform float maxContrast;

varying vec4 vcolor;
attribute float amplitude;
attribute float beamWidthAcross;
attribute float beamWidthAlong;
attribute float height;

varying float gAlongAngle;
varying float gAcrossAngle;
varying float gHeight;

void main()
{
	float normalizedValue=(amplitude-minContrast)/(maxContrast-minContrast);
	// On map cette valeur sur la palette pour récupérer la couleur correspondante
	vec3 paletteColor = texture2D(palette, vec2(normalizedValue, 0)).rgb;
	vcolor=vec4(paletteColor.r, paletteColor.g, paletteColor.b, opacity);
	gAcrossAngle	=beamWidthAcross;
	gAlongAngle=beamWidthAlong;
	gHeight=height;
	gl_Position =gl_ModelViewProjectionMatrix* gl_Vertex;
}
