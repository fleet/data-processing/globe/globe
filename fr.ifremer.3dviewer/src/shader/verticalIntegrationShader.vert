attribute float amplificationBuffer;

varying float colorData;

void main()
{
    colorData = amplificationBuffer;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
} 