uniform sampler2D texture;
uniform sampler2D palette;
uniform float minTransparency;
uniform float maxTransparency;
uniform float minContrastValue;
uniform float maxContrastValue;
uniform float inverseColormap;
uniform float opacity;
uniform float colormapOri;

/**
 * decode intensity from texture values
 * */
float decodeIntensity(vec4 textureColor )
{
	float intensity = 0.0;
	if(colormapOri == 0.0) //jet colormap
	{
		if(textureColor.r <= 0.1 && textureColor.g <= 0.1) //index entre 0 et 31
		{
			intensity = (float(textureColor.b) - 0.515625000000000 )/(1.0-0.515625000000000)*31.0/255.0;
		}
		else if(textureColor.r <= 0.1 && textureColor.b >= 0.9) //index entre 32 et 95
		{
			intensity = 32.0/255.0 + textureColor.g*(95.0-32.0)/255.0;
			
		}
		else if(textureColor.g >= 0.9) //index entre 96 et 159
		{
			intensity = 96.0/255.0 + textureColor.r*(159.0-96.0)/255.0;
		}
		else if(textureColor.r >= 0.9 && textureColor.b <= 0.1) //index entre 160 et 223
		{
			intensity = 160.0/255.0 + (1.0 - textureColor.g)*(223.0-160.0)/255.0;
		}
		else if(textureColor.g <= 0.1 && textureColor.b <= 0.1) //index entre 224 et 255
		{
			float min=224.0/255.0;
			float max=1.0;
			intensity=min+ 2*(1.0-textureColor.r)*(max-min);
			intensity=textureColor.r;
		} else
		{ 
			//we should not be here interpolation of color is probably done through shader
			intensity=1;
		}
		
	}
	else if(colormapOri == 1.0) //catherine
	{
		intensity = textureColor.r;
	}
	else //grayscale
	{
		intensity = textureColor.r;
	}
	
	if(intensity <0)
	{
		intensity=0;
	} 
	if (intensity>1)
	{
		intensity=1;
	}
	return intensity;
}
void main()
{
	float alpha = opacity;
	vec4 colorFromTexture = texture2D(texture, gl_TexCoord[0].st);
	float intensity=decodeIntensity(colorFromTexture);
	alpha*=colorFromTexture.a;
	// Filter data
	if(intensity > maxTransparency || intensity < minTransparency)
	{
		alpha = 0.0;
	}

	// Apply the contrast settings
	float contrastedColorData = (intensity - minContrastValue)/(maxContrastValue - minContrastValue);

	// Apply the inverseColormap settings
	if(inverseColormap == 1.0)
	{
		contrastedColorData = 1.0 - contrastedColorData;
	}

	// Get the color of amplification from the palette choosen by the user
	vec3 paletteColor = texture2D(palette, vec2(contrastedColorData, 0)).rgb;

	// Apply this color to the fragment which will be managed by OpenGL
	gl_FragColor = vec4(paletteColor.r, paletteColor.g, paletteColor.b, alpha);
}
