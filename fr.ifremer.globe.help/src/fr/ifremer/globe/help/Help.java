package fr.ifremer.globe.help;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

public class Help {

	public static void showHelp(String bundleName, Path path, String filename, String label)
			throws IOException, URISyntaxException {
		Bundle bundle = Platform.getBundle(bundleName);
		URL locationUrl = FileLocator.find(bundle, path, null);
		URL fileUrl = null;
		fileUrl = FileLocator.toFileURL(locationUrl);
		String location = new Path(fileUrl.getPath()).toString();

		String prefix = "reference:file:";
		if (location.startsWith(prefix)) {
			location = location.substring(prefix.length());
		}
		String fileName = new File(location, filename).getAbsolutePath();
		URI uri = new File(fileName).toURI();
		if (label != null && !label.isEmpty()) {
			uri = new URI(String.format("%s#%s", uri.toString(), label));
		}

		showURI(uri);
	}

	public static void showGenericHelpFor(String label) throws URISyntaxException, IOException {
		showHelp("fr.ifremer.globe.help", new Path("documentation"), "index.html", label);
	}

	/**
	 * Open an HTML page in the system navigator.
	 *
	 * @param page path of the page based in documentation folder
	 */
	public static void showGenericHelpPage(String page) throws URISyntaxException, IOException {
		showHelp("fr.ifremer.globe.help", new Path("documentation"), page, null);
	}

	public static void showURI(URI uri) throws IOException {
		Desktop.getDesktop().browse(uri);
	}

}
