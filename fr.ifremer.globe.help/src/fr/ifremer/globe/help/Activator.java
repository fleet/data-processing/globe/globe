package fr.ifremer.globe.help;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;

public class Activator implements BundleActivator {

	private static HelpPreference param;
	public static HelpPreference getPluginParameters() {return param;}

	@Override
	public void start(BundleContext context) throws Exception {
		PreferenceComposite root = PreferenceRegistry.getInstance().getGlobalSettingsNode();
		param = new HelpPreference(root, "Help");
	}

	@Override
	public void stop(BundleContext context) throws Exception {
	}
}
