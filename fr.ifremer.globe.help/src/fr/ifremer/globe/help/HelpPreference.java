package fr.ifremer.globe.help;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

public class HelpPreference  extends PreferenceComposite {
	private StringPreferenceAttribute forumGlobe;

	public HelpPreference(PreferenceComposite father, String name) {
		super(father, name);
		forumGlobe = new StringPreferenceAttribute("forumGlobe", "Globe forum Address", "https://forge.ifremer.fr/forum/forum.php?forum_id=568");
		this.declareAttribute(forumGlobe);
		load();
	}

	public StringPreferenceAttribute getForumGlobe() {
		return forumGlobe;
	}

}
