package fr.ifremer.globe.help.handler;

import java.io.IOException;
import java.net.URISyntaxException;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Path;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.MContribution;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.help.Help;

public class CaraibesHelpHandler {

	private Logger logger = LoggerFactory.getLogger(CaraibesHelpHandler.class);

	@CanExecute
	public boolean canExecute(@Named(IServiceConstants.ACTIVE_PART) final MContribution activePart, MItem item) {
		return true;
	}

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application, EModelService modelService,	@Named(IServiceConstants.ACTIVE_PART) final MContribution activePart) {
			//compute caraibes URL
			try {
				Help.showHelp("fr.ifremer.globe.help", new Path("documentation/caraibesHelp/CIB_Help.UK/Html"), "index.html", null);
			} catch (IOException | URISyntaxException  e) {
				logger.error("Error while trying to open caraibes Help: ("+e.getMessage()+")");

			}
		}
}
