/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.help.handler;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.help.Help;

/**
 * Handler to display on a specific page contained in the Documentation folder
 */
public class ShowSpecificHelpPageHandler {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(ShowSpecificHelpPageHandler.class);

	private static final String HELP_PAGE_PATH_PARAM = "fr.ifremer.globe.help.commandparameter.pagepath";

	@Execute
	public void execute(@Named(HELP_PAGE_PATH_PARAM) String page) {
		try {
			Help.showGenericHelpPage(page);
		} catch (Exception e) {
			logger.warn("Can't open help page " + page, e);
		}
	}
}
