// define images directory relative to this path
ifndef::imagesdir[:imagesdir: ../images]

=== How to use node filtering to set visible a large bunch of files 

This chapter will show how to use the filter capacities to hide or show a set of layers at once in the geographic view


==== Filtering

To select a special layer in data of your treeViewer, you can enter your research in the search bar and then click on the button "Search". 

In this example, in the treeViewer, there are mnt file which contains a scale. "scale" will be our research. 

image::04_how_tos/node_filter/27.png[]

There are two ways to do a research. Either the searched element is masked in the treeViewer, or it's the only one to appear in the treeViewer.

Here, on the right, there is the signe "different". Element which contain not the research appears in the treeviewer. 

image::04_how_tos/node_filter/32.png[]

You can reverse your research by clicking on the signe. The sign "equal" appears.

image::04_how_tos/node_filter/30.png[]

Element which contain the research appear in the treeviewer, others are masked. 

image::04_how_tos/node_filter/20.png[]

To stop the selection by the research click on "Reset Search".

image::04_how_tos/node_filter/33.png[]

==== Display

After filtering, you can decide if the researched element displays on Geographic view of GLOBE or not. 

To manage diplaying in the two way of research, click or not in the Data's checkbox.

For an non inversed research, if you uncheked "Data" only the layer which contains your research is visible in tree viewer and invisible in 3D viewer GLOBE.

image::04_how_tos/node_filter/21.png[]

For an non inversed research, when you check "Data" only the layer which contains your research is visible and selected in our tree viewer and visible in 3D viewer GLOBE.     

image::04_how_tos/node_filter/25.png[]

For an inversed research, when you check "Data" only the layer which contains your research isn't visible in our tree viewer and invisible too in 3D viewer GLOBE. 

image::04_how_tos/node_filter/22.png[]

For an inversed research, when you uncheck "Data" only the layer which contains your research isn't visible in our tree viewer but visible in 3D viewer GLOBE. 

image::04_how_tos/node_filter/24.png[]