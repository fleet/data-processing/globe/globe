
// define images directory relative to this path
ifndef::imagesdir[:imagesdir: ../images]


[BACKSCATTER]
=== How to generate a DTM with normalized backscatter layer

This chapter will present a step by step workflow from a raw file to a dtm file.

==== Convert raw files to XSF

Backscatter treatments are only available for XSF files. 
The first step is then to convert your raw files to XSF with  *Convert -> Raw files (.all, .kmall, .s7k) to Sounder files (.mbg, .xsf.nc, .nvi)...*

[WARNING]
The minimal supported XSF version for backscatter processes is 0.5. Please use *Convert -> XSF upgrade* to generate up to date files

==== Process bathymetry and export a reference DTM

Xsf files with tide corrected and clean bathymetry are required to obtain a good correction.
Refer to dedicated *How Tos* to obtain desired outputs.

xref:04_how_tos/SimpleProcessingFlow.adoc#SIMPLE_PROCESSING_FLOW[How to simply process a .all file]

xref:03_functionalities/033_processes/TideCorrection.adoc#TIDE[Tide Correction]

[NOTE]
When exporting the reference DTM, you can select the backscatter layer to have the uncorrected layer as point of comparison.

image::04_how_tos/backscatter/ReferenceDtm.png[]

==== Apply backscatter corrections

Two main methods are available to treat seafloor backscatter in Globe.

* A static method which consists in computing statistics on a selected part of input files eventually limited by polygons. Then applying computed statistics to the whole files.

* A dynamic method (recommended) which consists in the same statistics of the static method but applied on a rolling window across all files. The main advantage of this method is its capacity to adapt to seafloor type variations. 


===== Dynamic correction (recommended)

First check the sonar model used to generate your data

image::04_how_tos/backscatter/CheckSonarModel.png[]

Then launch *PyToolbox -> Backscatter rolling angular renormalization*. 
Select your XSF files as input and fill parameters as described in dedicated help.

image::04_how_tos/backscatter/RollingParameters.png[]

Select the previously generated reference DTM

image::04_how_tos/backscatter/RollingReferenceDtmParameter.png[]

Click on *Finish* to generate new XSF files with corrected backscatter values.

From new XSF files, generate a new DTM with a corrected backscatter layer and display it on top of previous DTM.

image::04_how_tos/backscatter/RollingDtm.png[]


===== Static correction (advanced)

Static correction consists first to generate statistics on a part of the data. Generally we search to select a set of areas matching the same kind of surface through the different modes used by the sounder.
These statistics are stored in a netcdf file with .bsar.nc extension and represents the angular response by mode by sector by antenna, by seafloor incidence angle and by transmission angle.
   
====== Generate .bsar.nc file (backscatter angular response)

Create a kml file with the Polygon editor to select the reference areas (optional).

image::04_how_tos/backscatter/Polygons.png[]

Check the sonar model used to generate your data (as for dynamic correction).

Then launch *Pytoolbox -> Backscatter statistical model response*. 
Select your XSF files as input and fill parameters as described in dedicated help.

image::04_how_tos/backscatter/BsarParameters.png[]

The reference DTM and the polygon kml files can also be given optionally to improve quality.

Click on *Finish* to generate the resulting bsar file with statistics

Right click on bsar file *Open with > Chart Editor* to check stats.
Chart editor allows to view angular response curves and eventually remove outliers from it.

image::04_how_tos/backscatter/BsarEditor.png[]

====== Apply renormalization 

The second step consists in using the bsar statistics file to normalize the seafloor backscatter variable of the input XSF files.

Launch *PyToolbox -> Backscatter angular renormalization*. 
Select your XSF files as input, the previously generated bsar file and fill parameters as described in dedicated help.

image::04_how_tos/backscatter/RenormBsarSelection.png[]

image::04_how_tos/backscatter/RenormParameters.png[]

Select as well the reference DTM to improve quality.

Other parameters are deduced from the bsar file to ensure consistency in the two processes.

Click on *Finish* to generate new XSF files with corrected backscatter values.

From new XSF files, generate a new DTM with a corrected backscatter layer and display it on top of previous DTM.

image::04_how_tos/backscatter/RenormDtm.png[]
