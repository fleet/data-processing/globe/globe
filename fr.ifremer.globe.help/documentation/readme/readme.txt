To generate the documentation, install asciidoctor and run the command: 
asciidoctor index.adoc

See "WriteDocumentation.adoc" for more details.