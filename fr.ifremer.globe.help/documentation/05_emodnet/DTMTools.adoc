// define images directory relative to this path
ifndef::imagesdir[:imagesdir: ../images]

==== DTM Post processing
===== Interpolation Tools
Globe integrates the following interpolation tools: 

* Gap filling
+
====
Method extracted from the Ifremer's Caraibes software : gaps are filled by bilinear interpolation.
====
* Gap fill nodata 
+
====
Apply a gdal fill_nodata algorithm to elevation layer. Fill selected raster regions by interpolation from the edges.
====
* Heighmap interpolation process
+
====
Interpolation functions for heightmaps developed within the EMODnet Bathymetry (High Resolution Seabed Mapping) project by Coronis. See  https://github.com/coronis-computing/heightmap_interpolation

====

===== Reset cell
The reset cell algorithm allows to erase (reset) part of the input file depending on a set of criteria
User can define a set of criteria, all of them are cumulative and cells matching all of the criteria will be resetted.

Criteria can be : 

* based on the input CDI
* based on numerical values in an input layer
* base on geographical region defined in the mask 
criteria can be applyied on each of input layer
+
image::05_emodnet/emodnet_regional_2020/ResetCell.png[]

===== Additional tools on DTMs
Specific tools are provided in the framework of the EMODnet Bathymetry project.

- [ ] Split by CDI : This application takes DTM file(s) in new format as input and will return as much cdis as there are in the input file(s). The program creates one file for each cdi. 
- [ ] Modify CDI : rename an existing CDI entry
- [ ] Set CDI : set a given CDI to all valid cell in a grid. Previous CDI are erased
- [ ] EMODnet create default layers for each missing layer. If they are missing, min and max depth layers are setted to mean depth, valid soundings layer to one and stdev is setted to 0.
