// define images directory relative to this path
ifndef::imagesdir[:imagesdir: ../images]

:toc: left
:toclevels: 3

=== EMODnet (2023)

_This chapter presents the tooling set used specifically for EMODnet in Globe._

==== EMODnet workflow

GLOBE is used to generate DTM (Digital Terrain Model) files and to process them. There is 2 flows, illustrated by the following figure :
 
.EMODnet Workflow
image::05_emodnet/emodnet_2021/EMODnetWorkflow.png[,700, align=center]

For more details about the DTM file format (.dtm.nc), see chapter : <<Digital Terrain Model (DTM) NetCDF4 (.dtm.nc)>> 

==== Tools and processes 

Processes, for the generation of DTM or post processing, are implemented in Python scripts that can be launch through 3 ways :

[options="",cols="1,3"]
|===
|From the *PyToolbox* | Open the PyToolbox image:02_generalities/026_Python/python.png[] view, and select the script to run.
|From the menu *Tools* |  Click on the `Tools` menu, and select `Convert...` or `Process..`.
|From the *Project explorer* | Right click on a file loaded in the `Project Explorer` : select `Export to...` or `Execute with...`.
|===

Please, see chapter <<Process call>> for details.

Once the process to run selected, a window will appear to allow the selection of each parameters. 
This view also provides an `Help` button.

[TIP]
====
The *PyToolbox* image:02_generalities/026_python/python.png[] view is usefull to get an overview and display help of each process.
====
[TIP]
====
Python scripts are developed in PyAT project (open source) : https://gitlab.ifremer.fr/fleet/pyat
====

==== How to generate DTM ?

GLOBE provides converters to generate DTM NetCDF files (.dtm.nc) from sounding detection, gridded data or other rater files :

.PyAT Tools to generate DTMs 
[cols="1,4,4,3", width="100%",options="header"]
|====================
|FLOW|FLOW 1 : from sounding data files|
FLOW 2 (a) : from gridded data| 
FLOW 2 (b) : from raster file

|**Inputs**| 
Text file (Ascii) with point cloud of *sounding detection* (one detection per row).

Accept files : .csv; .txt; .xyz; .asc;

Expected columns : longitude/X, latitude/Y and depth. 
|
Text file (Ascii) with *Gridded data* (one cell per row). 

Accept files : .csv; .txt; .xyz; .asc; *.emo*

Mandatory columns : longitude/X, latitude/Y, depth. 

Optional columns (used to fill corresponding layers) : CDI, Min elevation, Max elevation, Elevation smoothed, Backscatter, Interpolation flag and Std dev.


|
Any raster file readable by *GDAL* (Geospatial Data Abstraction Library).

Accept files : .tiff...

See : https://gdal.org/drivers/raster/index.html

|**Tool**|

`Export CSV to DTM wizard`

image:05_emodnet/emodnet_2023/pytoolbox_export_csv.png[]|
`Export gridded CSV to DTM`

image:05_emodnet/emodnet_2023/pytoolbox_export_gridded.png[]|
`Export raster file (TIFF...) to DTM`

image:05_emodnet/emodnet_2023/pytoolbox_export_raster.png[]|
|====================

[TIP]
====
*Old ".dtm" file ?* Use the process `Upgrade DTM format` to upgrade them to the new NetCDF format (.dtm.nc).
====

EMODnet parameters to select: 

.EMODnet paramters to use in 'Export... to DTM' tools
[cols=",", grid=rows,frame, options="header"]
|===
|Page| Parameters

| image:05_emodnet/emodnet_2023/export_csv_p1.png[,450] | 
*Input CSV file list*

Select input file(s) to convert.

| image:05_emodnet/emodnet_2023/export_csv_p2.png[,450] | 
*Output file list (.dtm.nc)*

Define output file(s). 

Select "Load after..." option to load the generated files.

| image:05_emodnet/emodnet_2023/export_csv_p3.png[,450] | 
*CSV parameters*

Describe the properties of input text file.

| image:05_emodnet/emodnet_2023/export_csv_p4.png[,450] | 
*Spatial reference*

Set the spatial reference of input file.

__Note : DTM will be generated with the same spatial reference. If necessary, the reprojection can be done later with the tool : `Reproject and Update bounding box` __

| image:05_emodnet/emodnet_2023/export_csv_pgeobox.png[,450] | 
*Geographic bounds and spatial resolution*

-  Geographic bounds : computed from input file by default, 

=> click on "**Expand to an integer number of angle minute**" to adjust values.

- Spatial resolution : 1/16, 1/32… arcminute according to the data resolution (a reduction can be applied later for integration in regional DTMs). 

| image:05_emodnet/emodnet_2023/export_csv_p6.png[,450] | 
*Geobox override parameters*

Option to *recompute Geobox* is usefull only in "batch" mode, *if several files with different geographic bounds* are generated : a specific geobox will be computed for each one.

For *gridded data*, an option allows to specifiy the position of values into the cells.

| image:05_emodnet/emodnet_2023/export_csv_p7.png[,450] | 
*Layers*

Select all optional layers.

__Note : the elevation layer is exported by default.__

| image:05_emodnet/emodnet_2023/export_csv_pfillgap.png[,450] | 
*Fill Gap*

Optional : can be used to fill small holes.

__Note : it can be better to apply interpolation in a second step, to clearly observe the result.__

| image:05_emodnet/emodnet_2023/export_csv_cdi.png[,450] | 
*CDI (only for sounding detection)*

Indicate the file identifiant.  

It is possible to *drop a text file* with list of File/CDI.

__Note : CDI identifiant shall have the syntax : `SDN:CDI:LOCAL:Edmo code_Filename` (metadata in Seadatanet)__

__From gridded data, the identifiant (CPRD) is set in a second step.__

| image:05_emodnet/emodnet_2023/export_csv_metadata.png[,450] | 
*Metadata*

Optional: can help to organize files.

__Note : metadata are saved as NetCDF global attributes and can be retrieved in NetCDF Viewer.__

| image:05_emodnet/emodnet_2023/export_csv_summary.png[,450] | 
*Summary*

This page summarizes all selected parameters.

|===

==== How to check DTM contents ?

===== ... in Geographic View

DTM files (.dtm.nc) can be loaded in `Project Explorer`, and the `Parameters View` enables to switch layers and set display parameters. 

.DTM displayed in Geographic View
image::05_emodnet/emodnet_2023/geographic_view.png[,450]

===== ... with NetCDF Viewer
The NetCDF Viewer allows to open and navigate through any NetCDF file. It can be used on DTM files to check the raw data.

Right click on file in `Project Explorer` > `Open With...` > `NetCDF Viewer` 

.DTM displayed in NetCDF Viewer
[cols=",", frame=none, grid=none]
|===
| image:05_emodnet/emodnet_2023/netcdf_viewer_1.png[,450]
| image:05_emodnet/emodnet_2023/netcdf_viewer_2.png[,450]
|===

[TIP]
====
This viewer provides *filters* and different *display mode* (graphics, or table). Parameters of 2D display can be set from the **__Parameter View__** (color, contrast...). 
====

===== ... with statistics and analysis tools

The tools `Compute statistics` and `Compute quality indicators` can be used to check DTMs with different indicators : mean, median, standard deviation, data distribution, anormal standard deviation, lake of detection...

.Tools to compute statistic and quality indicators
[cols=",", frame=none, grid=none]
|===
| image:05_emodnet/emodnet_2023/pytoolbox_compute_stats.png[,450]
| image:05_emodnet/emodnet_2023/pytoolbox_compute_qi.png[,450]
|===

[WARNING]
====
Some indicators need a minimum of layer to be computed. For example, `Compute of quality indicators` is not suitable for DTM generated from gridded CSV.
====

===== ... outside Globe
The DTM NetCDF file format is built on NetCDF and Geographic standards : it can therefore be opened in various software : QGIS, Panoply...

==== How to modify projection and/or bounding box ?
If necessary use the tool `Reproject and Update bounding box` to get data in the suitable *projection*.

.Reproject and update bounding box wizard
image::05_emodnet/emodnet_2023/pytoolbox_reproject.png[,450]

==== How to handle CDI (Common Data Identifier) or CPRD ?

===== ... how to set CDI ? 

Use the tool `Set CDI` to add the Id layer of the CPRD.

[NOTE]
====
*CPRD* Id shall have the syntax : SDN:CPRD:LOCAL:Edmo code_Filename (metadata in Sextant).
====

.Set CDI
image::05_emodnet/emodnet_2023/pytoolbox_set_cdi.png[,450]

===== ... how to check CDI ? 

The `Properties` view displays CDI values of a DTM.

It is also possible to get CDI information in a tooltip in Geographic view, 
select the button image:05_emodnet/emodnet_2021/CDIIcon.png[] and left click on the DTM layer.

Click on the "SeaDataNet" link to open the web page associated with the CDI. 

.CDI validation
image::05_emodnet/emodnet_2021/CheckCDI.png[,450]

For DTM files with CDI, the `Parameters view` allows the selection of the layer : `CDI Index`.

.CDI layer
image::05_emodnet/emodnet_2021/CDILayer.png[,450]


==== Other tools

List of Pytoolbox processes useful for EMODnet bathymetry :

[options="header",cols="1,4"]
|===
|Title   |Description   
//-------------
|*Laloxy* | Convert lat/lon coordinates into XY projected coordinates, and reverse.  
|*Upgrade DTM format* | Convert DTM from the latter Globe format to the current NC format.
|*Set CDI and modify existing CDI* | Set up the correct CDI for the Globe grid.    
|*Reduction* | Reduce the grid spatial resolution.   
|*Fill Gap* | Interpolation for small gaps.   
|*Reset cells* | Tool to invalidate cells.   
|*Heigmap interpolation*| Recommended for wide area to be interpolated.   
|*Kernell smoothing* | Smoothing function for the Elevation layer (averaging with neighboring cells).   
|*Linear transform* | Allow to reverse, if needed, the sign of the Elevation layer.  
|*Merge simple and merge fill* | Two options for merging DTMs.
|*Shrink Bounding Box* __(new)__ | Shrink the geographical box. The resulting geographic area can be scaled to a full minute of angle orfull spatial resolution
|*...*| __Open the PyToolbox to get an overview of all available processes...__
|===

==== Coronis interpolation tools

GLOBE integrates Coronis interpolation tools, which can be launched like any other tools.

.Coronis tools in PyToolbox
image::05_emodnet/emodnet_2023/pytoolbox-coronis.png[,450]


'''
