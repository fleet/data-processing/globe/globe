
  EXEMPLE FICHIER DE COMMANDE DE FUSION
  =====================================

  Ceci est un exemple de fichier de commande de realisation de mosaique de mosaiques.

  Le premier champ de fichier est le nom de l'image a inserer
  Le deuxieme champ indique quel type de recouvrement est a faire entre OVERLAY UNDERLAY MIXED
  Ces definitions peuvent etre en minuscule ou majuscules

    OVERLAY  : On projette l'image par dessus toutes les autres.
    UNDERLAY : On projette l'image dessous toutes les autres.
    MIXED    : On opere une moyenne sur les pixels qui se recouvrent.
               C'est une sorte de transparence.

  Les troisieme et quatrieme champs sont les bornes de rehaussement de contraste a appliquer
  a l'image a inserer: en dB si l'image est en dB (sortie Ereamo, Mosaic)
  Si les champs sont vides (blanc)  il n'y a pas de rehaussement de contraste

  Remarque : 1) Seules les lignes commancant par ">" seront decodees
  
  Nom fichier mosaique                  Type recouvrement
  -------------------------------------------------------
> /home6/latina/rejeu_em1000/transrho/mosa74.imo OVERLAY    0 255
> /home6/latina/rejeu_em1000/transrho/mosa77.imo UNDERLAY   0 255
> /home6/latina/rejeu_em1000/transrho/mosa79.imo UNDERLAY   0 255
> /home6/latina/rejeu_em1000/transrho/mosa90.imo UNDERLAY   0 255
> /home6/latina/rejeu_em1000/transrho/mosa17.imo UNDERLAY   0 255
