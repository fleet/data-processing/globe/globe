#Using PreferenceComposites in Globe plugin
##Presentation: 
 The composite preference system is made to organize preferences. The system offer the possibility to declare nodes that may contain attributes. Each node is attached to a father, except the root node. The system provided a GUI to visualize the nodes attached to the hierarchy, and edit their attributes ensuring saving changes. The preferences of each plugin must be declared at start of the plugin to ensure the automatic loading of attributes at Globe startup.

![editeur](./displayParam.png)

##Declaration:
To declare preferences we create a class that inherits from PreferenceComposite:

    public class PluginParameters extends PreferenceComposite {
    	private StringPreferenceAttribute parameter1;
    	
    	StringPreferenceAttribute getParameter1()
    	{
    		return parameter1;
    	}
    	
    	//1
    	public PluginParameters(PreferenceComposite superNode, String nodeName) {
    		super(superNode, nodeName);
    		// 2
    		parameter1 = new StringPreferenceAttribute("ParameterKey","Parameter name", "value");
    		// 3
    		this.declareAttribute(parameter1);
    	}
    }
The constructor (1) is used to declare a node with his embedded attributes. Each attribute has to be created (2) and linked to this node (3). To access attributes value, simply call the node method: `getAttribute("key").GetValue()`.

##Instantiation: 
The instantiation of the previous class is made in the "start" method of the plugin activator:

    public class PluginActivator implements BundleActivator {
    	//3
    	private static PluginParameters param;
    	public static PluginParameters getPluginParameters() {return param;}
    	@Override
    	public void start(BundleContext bundleContext) throws Exception {
    		
    		//1
    		PreferenceComposite root = PreferenceRegistry.getInstance().getRootNode();
    		//2
    		param = new PluginParameters(root, "Plugin node name");
    	}
    }
The root node is obtain through PreferenceRegistry (1) and used (2) to create the plugin specific node. An accessor method may be written (3) to facilitate the subsequent use of the node.


##Use :
Now you can call your plugin from your code : 

	Activator.getPluginParameters().getParameter1().getValue();
