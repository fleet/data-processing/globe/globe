#Bias Correction
##Presentation: 
The plugin `fr.ifremer.globe.biascorrection` allows to correct the heading, roll, pitch, heave, immersion and time of a sounder file.

This correction can be directly on sounder files in Layer Explorer, or on beams in the Swath Editor.
The process and the algorithm are the same for both operations but data models are different.


##Proxy Data:
To avoid code duplication and to simplify the reading, the model of the bias correction uses an interface `IDataProxy` that contains the methods to get data needed by the process.

- In the case of a process launched from Layer Explorer, the class `MbgProxy` that implements the interface `IDataProxy` is instantiated and found data in the `cylcleContainer` model linked with the sounder file.
- In the case of a process launched from Swath Editor, the class `SoundingsProxy` that implements the interface `IDataProxy` is instantiated and found data in the sounding model linked with the Swath Editor.

##Applier:
The application of correction is made by the interface `IDataApplier` that contains methods to modify the needed data.

- In the case of a process launched from Layer Explorer, the class `MbgProxyApplier` that implements the interface `IDataApplier` is instantiated and modify data in the `cylcleContainer` model linked with the sounder file.
- In the case of a process launched from Swath Editor, the class `SoundingsProxyApplier` that implements the interface `IDataApplier` is instantiated and modify data in the sounding model linked with the Swath Editor.



