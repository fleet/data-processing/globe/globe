#Using exception in Globe

Globe exception are all supposed to be inherited from `GException` and `GRuntimeException` in package
`globe.ifremer.util.exception`

The purpose of these base class are to add a log file whenever a new Exception is fired