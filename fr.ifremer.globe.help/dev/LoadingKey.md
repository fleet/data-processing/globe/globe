#Loading Key
##Presentation: 
Le chargement des CycleContainer dispose maintenant d’un mécanisme de gestion par clés.
Les clés contiennent la liste des attributs optionnels à charger (à noter que les attributs obligatoires sont toujours chargés et leur liste n’est pas modifiable).

Une Factory `fr.ifremer.globe.model.datastore.loading.LoadingKeyFactory` permet la création de clés génériques.
Un Builder `fr.ifremer.globe.model.datastore.loading.LoadingKeyBuilder` permet la création de clés personnalisées. Lors de la création de la clé, une vérification est effectuée afin de contrôler que les attributs présents dans la clé sont bien disponibles dans le driver concerné.

Au niveau du CycleContainer, une clé de synthèse est présente afin de permettre le chargement avec plusieurs clés. Ainsi, l’intégralité des attributs présents dans les différentes clés sera chargée.

La présence de ces clés de chargement permet d’allouer uniquement les attributs demandés par les clés. Lors du déchargement d’une clé au niveau du CycleContainer, les espaces alloués aux attributs n’étant plus utilisés sont déchargés afin de libérer l’espace occupé.

Les clés possèdent également un (ou plusieurs) type(s) de chargement (`fr.ifremer.globe.model.datastore.loading.ELoadingType`). En effet, à de nombreux endroits du code, des tests sont effectués sur le type de la clé afin de savoir pour quel type de traitement celle-ci est utilisée (Bathy, Imagery, Navigation, etc…). Ce fonctionnement a été gardé afin de permettre un aiguillage des traitements plus efficace.

Les déclarations des attributs au niveau du cycle container ne sont plus à effectuer manuellement. Il est de la responsabilité de l’InfoStore de surcharger les méthodes permettant de récupérer les attributs optionnels (ex : `fr.ifremer.globe.model.ISounderDriverInfo.getRxAntennaOptionalAttributes()`)