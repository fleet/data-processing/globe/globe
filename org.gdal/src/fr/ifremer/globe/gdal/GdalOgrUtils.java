/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.gdal;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import java.util.function.Consumer;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.gdal;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.Driver;
import org.gdal.ogr.Feature;
import org.gdal.ogr.FeatureDefn;
import org.gdal.ogr.FieldDefn;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.apps.ogr2ogr;

/**
 * OGR utilities (GDAL vector files)
 */
public class GdalOgrUtils {
	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(GdalOgrUtils.class);

	/** Reused empty vector */
	protected static final Vector<?> EMPTY_VECTOR = new Vector<>();

	/** Constructor */
	private GdalOgrUtils() {
	}

	/**
	 * Read metadata (SpatialReference, extent, geometry) from the vector file
	 *
	 * @param inFile vector file
	 * @param srsFitter callback for the SpatialReference. May receive null.
	 * @param extentFitter callback for the Extent. May receive null.
	 * @param geometryFitter callback for the geometry type.
	 * @param fieldFitter callback for field definition.
	 * @throws IOException Read failed
	 */
	public static void readMetadata(String inFile, Consumer<SpatialReference> srsFitter,
			Consumer<double[]> extentFitter, Consumer<String> geometryFitter,
			Consumer<Map<String, Set<String>>> fieldFitter) throws IOException {
		// Open with Gdal.
		DataSource datasource = null;
		try {
			datasource = ogr.Open(inFile, false);
			if (datasource == null) {
				throw new IOException("Unsupported file " + FilenameUtils.getName(inFile));
			}

			readMetadata(datasource, srsFitter, extentFitter, geometryFitter, fieldFitter);
		} finally {
			if (datasource != null) {
				datasource.delete();
			}
		}
	}

	/**
	 * Read metadata (SpatialReference, extent, geometry) from the vector file
	 *
	 * @param inFile vector file
	 * @param srsFitter callback for the SpatialReference. May receive null.
	 * @param extentFitter callback for the Extent. May receive null.
	 * @param geometryFitter callback for the geometry type.
	 * @param fieldFitter callback for field definition.
	 * @throws IOException Read failed
	 */
	public static void readMetadata(DataSource datasource, Consumer<SpatialReference> srsFitter,
			Consumer<double[]> extentFitter, Consumer<String> geometryFitter,
			Consumer<Map<String, Set<String>>> fieldFitter) throws IOException {
		// Open with Gdal.
		Layer layer = null;
		try {
			layer = datasource.GetLayer(0); // Only one layer in a SHP
			if (layer == null) {
				throw new IOException("Unsupported file. No layer");
			}

			srsFitter.accept(layer.GetSpatialRef());
			extentFitter.accept(layer.GetExtent());
			geometryFitter.accept(ogr.GeometryTypeToName(layer.GetGeomType()));

			FeatureDefn featureDefn = layer.GetLayerDefn();
			Map<String, Set<String>> fields = new HashMap<>();
			if (featureDefn != null) {
				// Collect all fields
				int fieldCount = featureDefn.GetFieldCount();
				for (int i = 0; i < fieldCount; i++) {
					FieldDefn field = featureDefn.GetFieldDefn(i);
					fields.put(field.GetName(), new HashSet<>());
					field.delete();
				}

				// Collect all values of all fields
				Feature feature = layer.GetNextFeature();
				while (feature != null) {
					for (Entry<String, Set<String>> entry : fields.entrySet()) {
						String value = feature.GetFieldAsString(entry.getKey());
						if (value != null && !value.isEmpty()) {
							entry.getValue().add(value);
						}
					}
					feature.delete();
					feature = layer.GetNextFeature();
				}
				fieldFitter.accept(fields);
			}
		} finally {
			if (layer != null) {
				layer.delete();
			}
		}
	}

	/**
	 * Create a new vector file
	 *
	 * @param outDriver expected vector format of the outputFile
	 * @param outputFile resulting vector file
	 * @return the resulting DataSource
	 * @throws IOException Transformation failed
	 */
	public static DataSource createDataSource(String outDriver, String outputFile) throws IOException {
		Driver poDriver = ogr.GetDriverByName(outDriver);
		if (poDriver == null || !poDriver.TestCapability(ogr.ODrCCreateDataSource)) {
			throw new IOException("Unable to create a file in the format " + outDriver);
		}
		return poDriver.CreateDataSource(outputFile, EMPTY_VECTOR);

	}

	/**
	 * Transform the vector file to the specfied SRS
	 *
	 * @param inputFile Vector file to reproject
	 * @param outSrs SpatialReference to transform to
	 * @param outDriver expected vector format of the outputFile
	 * @param outputFile resulting vector file
	 * @param monitor progression monitoring
	 * @throws IOException Transformation failed
	 */
	public static void transformToSrs(String inputFile, SpatialReference outSrs, String outDriver, String outputFile,
			IProgressMonitor monitor) throws IOException {
		DataSource srcDataSource = ogr.Open(inputFile, false);
		if (srcDataSource == null) {
			throw new IOException(gdal.GetLastErrorMsg());
		}

		try {
			DataSource outDataSource = createDataSource(outDriver, outputFile);
			int layerCount = srcDataSource.GetLayerCount();
			SubMonitor subMonitor = SubMonitor.convert(monitor, layerCount);
			for (int iLayer = 0; iLayer < srcDataSource.GetLayerCount(); iLayer++) {
				Layer srcLayer = srcDataSource.GetLayer(iLayer);
				if (srcLayer != null) {
					transformToSrs(srcDataSource, srcLayer, outDataSource, outSrs, subMonitor.split(1));
					srcLayer.delete();
				}
			}
			outDataSource.delete();
		} finally {
			srcDataSource.delete();
		}
	}

	/**
	 * Transform the layer to the specfied SRS
	 *
	 * @param srcDataSource DataSource to reproject
	 * @param srcLayer Layer of the DataSource to reproject
	 * @param output resulting DataSource
	 * @param outSRS new SpatialReference
	 * @param monitor progression monitoring
	 * @throws IOException Reproction failed
	 */
	public static void transformToSrs(DataSource srcDataSource, Layer srcLayer, DataSource output,
			SpatialReference outSRS, IProgressMonitor monitor) throws IOException {
		logger.info("Start reprojecting");
		long startTime = System.currentTimeMillis();

		boolean result = ogr2ogr.TranslateLayer( //
				srcDataSource, // poSrcDS
				srcLayer, // poSrcLayer
				output, // poDstDS
				EMPTY_VECTOR, // papszLCO
				null, // pszNewLayerName
				true, // bTransform
				outSRS, // poOutputSRS
				null, // poSourceSRS
				EMPTY_VECTOR, // papszSelFields
				false, // bAppend
				-2, // eGType. -2 == not specified
				true, // bOverwrite
				0d, // dfMaxSegmentLength
				EMPTY_VECTOR, // papszFieldTypesToString
				0l, // nCountLayerFeatures
				GdalUtils.newProgressCallback(monitor));

		if (result) {
			logger.info("End reprojecting in {} ms", System.currentTimeMillis() - startTime);
		} else {
			throw new IOException(gdal.GetLastErrorMsg());
		}
	}

}
