/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.gdal;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.ProgressCallback;
import org.gdal.gdal.RasterizeOptions;
import org.gdal.gdal.TranslateOptions;
import org.gdal.gdal.WarpOptions;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.Driver;
import org.gdal.ogr.Feature;
import org.gdal.ogr.FeatureDefn;
import org.gdal.ogr.FieldDefn;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.gdal.ogr.ogrConstants;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.function.BiIntToFloatFunction;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Some common utilities.
 */
public class GdalUtils {
	protected static final Logger logger = LoggerFactory.getLogger(GdalUtils.class);

	/** Returned value on success. */
	public static final int OK = gdalconstConstants.CE_None;

	/** true expected by gdal. */
	public static final int TRUE = 1;
	/** false expected by gdal. */
	public static final int FALSE = 0;

	/** Shape file driver name. */
	public static final String SHP_DRIVER_NAME = "ESRI Shapefile";

	/** KML file driver name. */
	public static final String KML_DRIVER_NAME = "KML";

	/** GeoJSON file driver name. */
	public static final String GEOJSON_DRIVER_NAME = "GeoJSON";
	/** NetCDF file driver name. */
	public static final String NETCDF_DRIVER_NAME = "NetCDF";
	/** HDF5 file driver name. */
	public static final String HDF5_DRIVER_NAME = "HDF5";
	/** Geotiff file driver name. */
	public static final String TIF_DRIVER_NAME = "GTiff";
	/** Geotiff file driver name. */
	public static final String XYZ_DRIVER_NAME = "XYZ";
	/** Memory driver name. */
	public static final String MEMORY_DRIVER_NAME = "MEM";

	/** Alpha value for opaque pixel. */
	public static final short OPAQUE = 255;
	/** Alpha value for transparent pixel. */
	public static final short TRANSPARENT = 0;

	/**
	 * Constructor.
	 */
	private GdalUtils() {
	}

	public static void logGdalInfo() {
		if (logger.isInfoEnabled()) {
			logger.info("GDAL version : {}", gdal.VersionInfo());
			logger.info("Nb of GDAL drivers : {} ", gdal.GetDriverCount());
			logger.info("Drivers : {}", StringUtils.join(IntStream.range(0, gdal.GetDriverCount())
					.mapToObj(gdal::GetDriver).map(org.gdal.gdal.Driver::getShortName).toList(), ", "));
			logger.info("Nb of OGR drivers : {}", ogr.GetDriverCount());
			logger.info("Drivers : {}", StringUtils.join(IntStream.range(0, ogr.GetDriverCount())
					.mapToObj(ogr::GetDriver).map(org.gdal.ogr.Driver::getName).toList(), ", "));
		}
	}

	/**
	 * @return the boundingbox of the raster as [max latitude (North) , min latitude (South), max longitude (East), min
	 *         longitude (West)].
	 */
	public static double[] getBoundingBox(Geometry geometry) {
		double[] result = new double[4];
		geometry.GetEnvelope(result);
		return new double[] { Math.max(result[2], result[3]), Math.min(result[2], result[3]),
				Math.max(result[0], result[1]), Math.min(result[0], result[1]) };
	}

	/**
	 * @return the boundingbox of the raster as North/up/ymax, South/down/ymin, East/right/xmax, West/left/xmin
	 */
	public static double[] getBoundingBox(Dataset dataset) {
		var extent = getExtent(dataset);
		var result = new double[] { //
				extent[3], // North/up/ymax
				extent[1], // South/down/ymin
				extent[2], // East/right = xmax
				extent[0] // West/left = xmin
		};
		SpatialReference srs = getSpatialReference(dataset);
		if (srs.IsGeographic() == TRUE) {
			result[0] = NumberUtils.normalizedDegreesLatitude(result[0]);
			result[1] = NumberUtils.normalizedDegreesLatitude(result[1]);
			result[2] = NumberUtils.normalizedDegreesLongitude(result[2]);
			result[3] = NumberUtils.normalizedDegreesLongitude(result[3]);
		}

		return result;
	}

	/**
	 * @return the extent of the raster (xmin ymin xmax ymax => left down right up )
	 */
	public static double[] getExtent(Dataset dataset) {
		int lastX = dataset.getRasterXSize();
		int lastY = dataset.getRasterYSize();
		double[] geoTransform = dataset.GetGeoTransform();
		double[] leftUp = new double[] { 0d, 0d };
		transform(leftUp, geoTransform);
		double[] rightDown = new double[] { lastX, lastY };
		transform(rightDown, geoTransform);

		return new double[] { leftUp[0], rightDown[1], rightDown[0], leftUp[1] };
	}

	/**
	 * Shift the raster by changing the origin (xmin ymin of the extents)
	 */
	public static void shift(Dataset dataset, double xmin, double ymin) {
		double[] geoTransform = dataset.GetGeoTransform();
		geoTransform[0] = xmin;
		geoTransform[3] = ymin;
		dataset.SetGeoTransform(geoTransform);
	}

	/** Return the {@link SpatialReference} of the specified {@link Dataset} */
	public static SpatialReference getSpatialReference(Dataset dataset) {
		// Projection ?
		String projWkt = dataset.GetProjectionRef();
		if (projWkt == null || projWkt.isEmpty()) {
			projWkt = dataset.GetProjection();
		}
		SpatialReference srs = GdalOsrUtils.SRS_WGS84;
		if (projWkt != null && !projWkt.isEmpty()) {
			srs = new SpatialReference(projWkt);
		}
		return srs;
	}

	/**
	 * Create a Shape file containing the specified geometry
	 *
	 * @return true if success.
	 */
	public static boolean writeShapeFile(Geometry polygon, File shapeFile, String proj4SpatialReference) {
		boolean result = false;

		// Create a Shape Dataset
		Driver driver = ogr.GetDriverByName(SHP_DRIVER_NAME);
		DataSource dataSource = driver.CreateDataSource(shapeFile.getAbsolutePath());
		try {
			SpatialReference spatialReference = new SpatialReference();
			if (proj4SpatialReference != null) {
				spatialReference.ImportFromProj4(proj4SpatialReference);
			}
			Layer outputLayer = dataSource.CreateLayer("layer", spatialReference, ogrConstants.wkbPolygon);
			// Get the output Layer's Feature Definition
			FeatureDefn featureDefn = outputLayer.GetLayerDefn();
			// create a new feature
			Feature feature = new Feature(featureDefn);

			// Set new geometry
			if (feature.SetGeometry(polygon) == OK) {
				// Set the bounding box
				polygon.GetEnvelope(outputLayer.GetExtent());
				// Set the feature on layer
				result = outputLayer.CreateFeature(feature) == OK;
			}
		} finally {
			dataSource.delete();
		}
		return result;
	}

	/**
	 * Delete a Shape file and all dependent files
	 *
	 * @return true if success.
	 */
	public static boolean deleteShapeFile(File shapeFile) {
		if (shapeFile.exists()) {
			Driver driver = ogr.GetDriverByName(SHP_DRIVER_NAME);
			return driver.DeleteDataSource(shapeFile.getAbsolutePath()) == OK;
		}
		return true;
	}

	/**
	 * @return the NoData value of a Band
	 */
	public static double getNoDataValue(Band band) {
		Double[] result = new Double[1];
		band.GetNoDataValue(result);
		return result[0] != null ? result[0].doubleValue() : Double.NaN;
	}

	/**
	 * @return the Scale value of a Band
	 */
	public static double getScale(Band band) {
		Double[] result = new Double[1];
		band.GetScale(result);
		return result[0] != null ? result[0].doubleValue() : 1d;
	}

	/**
	 * @return the Offset value of a Band
	 */
	public static double getOffset(Band band) {
		Double[] result = new Double[1];
		band.GetOffset(result);
		return result[0] != null ? result[0].doubleValue() : 0d;
	}

	/**
	 * Utility for clipping raster with vector layer using GDAL into a GTiff file
	 *
	 * @param nodata Set nodata values for output bands. Ignore if NaN
	 *
	 * @return The resulting raster dataset. null if error occurs
	 */
	public static Dataset clipRaster(Dataset raster, File shapeFile, File outFile, String proj4SpatialReference,
			double nodata, boolean crop) {
		WarpOptions warpOptions = buildClipOptions(raster, shapeFile, proj4SpatialReference, nodata, crop);
		Dataset result = gdal.Warp(outFile != null ? outFile.getAbsolutePath() : "", new Dataset[] { raster },
				warpOptions);
		warpOptions.delete();
		return result;
	}

	/**
	 * Utility for reproject raster
	 *
	 * @return The resulting raster dataset. null if error occurs
	 */
	public static Dataset reprojectRaster(Dataset raster, double xmin, double ymin, double xmax, double ymax) {

		Vector<String> options = new Vector<>();
		options.add("-overwrite");
		options.add("-q");
		// This is a number of extra pixels added around the source to take care
		// of rounding error
		options.add("-wo");
		options.add("SOURCE_EXTRA=1000");
		options.add("-te");
		options.add(String.valueOf(xmin));
		options.add(String.valueOf(ymin));
		options.add(String.valueOf(xmax));
		options.add(String.valueOf(ymax));
		options.add("-ts");
		options.add(String.valueOf(raster.getRasterXSize()));
		options.add(String.valueOf(raster.getRasterYSize()));
		WarpOptions warpOptions = new WarpOptions(options);

		Dataset result = gdal.Warp(generateInMemoryFilePath(), new Dataset[] { raster }, warpOptions);
		warpOptions.delete();

		return result;
	}

	/**
	 * Utility for reproject raster
	 *
	 * @paran xmin, ymin, xmax, ymax set georeferenced extents of output file to be created
	 * @paran width, ymin, height set output file size in pixels and lines. If width or height is set to 0, the other
	 *        dimension will be guessed from the computed resolution
	 *
	 * @return The resulting raster dataset. null if error occurs
	 */
	public static Dataset reprojectRaster(Dataset raster, int width, int height, double nodata, double xmin,
			double ymin, double xmax, double ymax) {

		Vector<String> options = new Vector<>();
		options.add("-q");
		options.add("-wo");
		options.add("SOURCE_EXTRA=1000");
		options.add("-te");
		options.add(String.valueOf(xmin));
		options.add(String.valueOf(ymin));
		options.add(String.valueOf(xmax));
		options.add(String.valueOf(ymax));
		options.add("-ts");
		options.add(String.valueOf(width));
		options.add(String.valueOf(height));
		options.add("-srcnodata");
		options.add(String.valueOf(nodata));
		WarpOptions warpOptions = new WarpOptions(options);

		Dataset result = gdal.Warp(generateInMemoryFilePath(), new Dataset[] { raster }, warpOptions);
		warpOptions.delete();

		return result;
	}

	public static void rasterizeShapeFile(File input, File output, int rasterXSize, int rasterYSize) {
		Vector<String> options = new Vector<>();
		options.add("-ts");
		options.add(Integer.toString(rasterXSize));
		options.add(Integer.toString(rasterYSize));
		options.add("-burn");
		options.add("255");
		RasterizeOptions rasterOptions = new RasterizeOptions(options);
		Dataset dset = gdal.OpenEx(input.getAbsolutePath());
		Dataset out = gdal.Rasterize(output.getAbsolutePath(), dset, rasterOptions);
		dset.delete();
		out.delete();
	}

	/**
	 * Create a Dataset in memory
	 */
	public static Dataset createDataset(int xSize, int ySize, int dataType, int... colorInterpretation) {
		return createDataset(MEMORY_DRIVER_NAME, "Memory " + System.currentTimeMillis(), xSize, ySize, dataType,
				colorInterpretation);
	}

	/**
	 * Create a Dataset for a specifiv driver
	 */
	public static Dataset createDataset(String driverName, String destination, int xSize, int ySize, int dataType,
			int... colorInterpretation) {
		Dataset result = null;

		// Create a Dataset in memory
		org.gdal.gdal.Driver driver = gdal.GetDriverByName(driverName);
		String createCapability = driver.GetMetadataItem(gdalconstConstants.GDAL_DCAP_CREATE);
		if ("YES".equals(createCapability)) {
			result = driver.Create(destination, xSize, ySize, colorInterpretation.length, dataType);
			if (result != null) {
				for (int i = 0; i < colorInterpretation.length; i++) {
					result.GetRasterBand(i + 1).SetColorInterpretation(colorInterpretation[i]);
				}
			}
		} else {
			Dataset memoryDataset = createDataset(xSize, ySize, dataType, colorInterpretation);
			result = driver.CreateCopy(destination, memoryDataset);
			asyncDeleteDataset(memoryDataset);
		}
		return result;
	}

	/**
	 * Create a memory Dataset copy of the specified one.
	 */
	public static Dataset copyDataset(Dataset dataset) {
		// Create a Dataset in memory
		org.gdal.gdal.Driver memoryDriver = gdal.GetDriverByName(MEMORY_DRIVER_NAME);
		return memoryDriver.CreateCopy("Memory " + System.currentTimeMillis(), dataset);
	}

	/**
	 * Creates a copy of the specified {@link Dataset}.
	 *
	 * @param dataset : input {@link Dataset}
	 * @param driverName : driver used to defined output type
	 * @param outputPath : output path
	 * @return result {@link Dataset}
	 */
	public static Dataset createCopy(Dataset dataset, String driverName, String outputPath) {
		org.gdal.gdal.Driver driver = gdal.GetDriverByName(driverName);
		return driver.CreateCopy(outputPath, dataset);
	}

	/**
	 * @return ColorInterpretation of all bands od the dataset
	 */
	public static int[] getColorInterpretation(Dataset dataset) {
		int[] result = new int[dataset.getRasterCount()];
		for (int i = 1; i <= result.length; i++) {
			Band band = dataset.GetRasterBand(i);
			result[i - 1] = band.GetColorInterpretation();
			band.delete();
		}
		return result;
	}

	/**
	 * Use to prepare all options for calling gdal.Warp
	 *
	 * @param nodata Set nodata values for output bands. Ignore if NaN
	 */
	protected static WarpOptions buildClipOptions(Dataset raster, File shapeFile, String proj4SpatialReference,
			double nodata, boolean crop) {
		Vector<String> options = new Vector<>();
		options.add("-overwrite");
		options.add("-q");
		if (proj4SpatialReference != null) {
			// source spatial reference set
			options.add("-s_srs");
			options.add(proj4SpatialReference);
			// target spatial reference set
			options.add("-t_srs");
			options.add(proj4SpatialReference);
		} else {
			// transformer option : no transformation
			options.add("-to");
			options.add("SRC_METHOD=NO_GEOTRANSFORM");
			options.add("-to");
			options.add("DST_METHOD=NO_GEOTRANSFORM");
		}
		options.add("-cutline");
		options.add(shapeFile.getAbsolutePath());
		options.add("-cl");
		options.add(FilenameUtils.getBaseName(shapeFile.getName()));

		// // Crop the extent of the target dataset to the extent of the
		// cutline.
		if (crop) {
			options.add("-crop_to_cutline");
		}

		if (!Double.isNaN(nodata)) {
			options.add("-dstnodata");
			options.add(String.valueOf(nodata));
		}

		// Create an output alpha band to identify nodata
		if (raster.GetRasterCount() > 1) {
			options.add("-srcalpha");
			options.add("-dstalpha");
		}

		return new WarpOptions(options);
	}

	/**
	 * Find the band for a specific color interpretation
	 *
	 * @param colorInterpretation can be gdalconst.GCI_AlphaBand, GCI_BlueBand, GCI_GreenBand, GCI_RedBand...
	 * @return null if band not found
	 */
	public static Band getBand(Dataset raster, int... colorInterpretation) {
		Band result = null;
		for (int bandIndex = 1; bandIndex <= raster.getRasterCount(); bandIndex++) {
			result = raster.GetRasterBand(bandIndex);
			if (result != null) {
				final int bandColorInterpretation = result.GetColorInterpretation();
				if (Arrays.stream(colorInterpretation)
						.anyMatch(interpretation -> interpretation == bandColorInterpretation)) {
					break;
				}
			}
			result = null;
		}
		return result;
	}

	/**
	 * Create a Vector Dataset in memory
	 */
	public static DataSource createDataSource() {
		return createDataSource("Memory", "DataSource");
	}

	/**
	 * Create a Vector Dataset for a specific driver
	 */
	public static DataSource createDataSource(String driverName, String outputFilePath) {
		// Create a Shape Dataset in memory
		Driver driver = ogr.GetDriverByName(driverName);
		return driver.CreateDataSource(outputFilePath);
	}

	/**
	 * Utility for grabbing the content of a raster
	 */
	public static void readRaster(Band band, RasterValueCatcher rasterValueCatcher) {
		readRaster(band, 0, 0, band.GetXSize(), band.GetYSize(), rasterValueCatcher);
	}

	/**
	 * Utility for grabbing the content of a raster
	 */
	public static void readRaster(Band band, int columnOffset, int lineOffset, int columnCount, int lineCount,
			RasterValueCatcher rasterValueCatcher) {
		boolean loggedOnce = false;
		double[] result = new double[columnCount];
		for (int line = 0; line < lineCount; line++) {
			int read = band.ReadRaster(columnOffset, line + lineOffset, columnCount, 1, result);
			if (read == OK) {
				for (int column = 0; column < columnCount; column++) {
					rasterValueCatcher.processValue(line + lineOffset, column + columnOffset, result[column]);
				}
			} else {
				if (!loggedOnce) {
					logger.error("Error while reading raster band with gdal {}", gdal.GetLastErrorMsg());
					loggedOnce = true;
				}
			}
		}
	}

	/**
	 * Operation to a double
	 */
	@FunctionalInterface
	public interface RasterValueCatcher {
		void processValue(int line, int column, double value);
	}

	/**
	 * Utility for filling the content of a raster
	 *
	 * @throws Exception
	 */
	public static int writeRaster(Band band, BiIntToFloatFunction rasterValueSupplier,
			ProgressCallback progressCallback) {
		int result = OK;
		int lineCount = band.GetYSize();
		int columnCount = band.GetXSize();
		float[] values = new float[columnCount];
		for (int line = 0; line < lineCount; line++) {
			for (int column = 0; column < columnCount; column++) {
				values[column] = rasterValueSupplier.apply(line, column);
			}
			result = band.WriteRaster(0, line, columnCount, 1, values);
			if (progressCallback != null) {
				progressCallback.run((double) line / lineCount, "");
			}
			if (result != OK) {
				break;
			}
		}
		return result;
	}

	/** Apply a coordinate transformation to the geometry. */
	public static void transform(Geometry geometry, double[] geoTransform, boolean round) {
		for (int i = 0; i < geometry.GetGeometryCount(); i++) {
			transform(geometry.GetGeometryRef(i), geoTransform, round);
		}
		double[] point = new double[3];
		for (int i = 0; i < geometry.GetPointCount(); i++) {
			geometry.GetPoint(i, point);
			transform(point, geoTransform);
			if (round) {
				geometry.SetPoint(i, Math.round(point[0]), Math.round(point[1]));
			} else {
				geometry.SetPoint(i, point[0], point[1]);
			}
		}
	}

	/**
	 * Compute a coordinates transform as :<br>
	 *
	 * <pre>
	 *
	 * Xgeo = geoTransform(0) + coords[0]*geoTransform(1) + coords[1]*geoTransform(2)
	 * Ygeo = geoTransform(3) + coords[0]*geoTransform(4) + coords[1]*geoTransform(5)
	 *
	 * </pre>
	 *
	 * @param coords x,y coordinates (may be longitude/latitude)
	 * @param geoTransform The affine transform consists of six coefficients which map pixel/line coordinates into
	 *            georeferenced space
	 */
	public static void transform(double[] coords, double[] geoTransform) {
		double[] x = { 0d };
		double[] y = { 0d };
		gdal.ApplyGeoTransform(geoTransform, coords[0], coords[1], x, y);
		coords[0] = x[0];
		coords[1] = y[0];
	}

	/**
	 * @return the spatial resolution (x, y) of the dataset
	 */
	public static double[] computeSpatialResolution(Dataset dataset) {
		double[] geoTransform = dataset.GetGeoTransform();
		return new double[] { Math.abs(geoTransform[1]), Math.abs(geoTransform[5]) };
	}

	/**
	 * Compute a coordinates transform as :<br>
	 */
	public static void transform(double[] coords, Dataset dataset) {
		transform(coords, dataset.GetGeoTransform());
	}

	/**
	 * Modify the geometry such it has at least the specified number of segment.
	 */
	public static void segmentize(Geometry geometry, int segmentCount) {
		double length = geometry.Length();
		if (length > 0 && segmentCount > 1 && length / (segmentCount - 1) > 0) {
			geometry.Segmentize(length / (segmentCount - 1));
		}
	}

	/**
	 * @return the index of the nearest point of the specified coords in the geometry.
	 */
	public static int getNearestPointIndex(Geometry geometry, double x, double y) {
		int result = -1;
		if (geometry != null) {
			Geometry line = new Geometry(ogrConstants.wkbLineString);
			line.AddPoint_2D(x, y);
			line.AddPoint_2D(0d, 0d);

			double length = Double.POSITIVE_INFINITY;
			double[] point = new double[2];
			for (int i = 0; i < geometry.GetPointCount(); i++) {
				geometry.GetPoint_2D(i, point);
				line.SetPoint_2D(1, point[0], point[1]);
				if (line.Length() < length) {
					length = line.Length();
					result = i;
				}
			}
			line.delete();
		}
		return result;
	}

	/**
	 * @return a segment as Geometry to link the two points.
	 */
	public static Geometry makeSegment(double x, double y, double x2, double y2) {
		Geometry result = new Geometry(ogrConstants.wkbLineString);
		result.AddPoint_2D(x, y);
		result.AddPoint_2D(x2, y2);
		return result;
	}

	/**
	 * @return a square as Geometry to link the two points.
	 */
	public static Geometry makeSquare(int x, int y, int width, int height) {
		Geometry result = new Geometry(ogrConstants.wkbLinearRing);
		result.AddPoint_2D(x, y);
		result.AddPoint_2D((double) x + width, y);
		result.AddPoint_2D((double) x + width, (double) y + height);
		result.AddPoint_2D(x, (double) y + height);
		result.AddPoint_2D(x, y);
		return result;
	}

	/**
	 * @return a polygon containing a Geometry.
	 */
	public static Geometry makePolygon(Geometry geometry) {
		Geometry result = new Geometry(ogrConstants.wkbPolygon);
		result.AddGeometry(geometry);
		return result;
	}

	/**
	 * @return a Point as Geometry.
	 */
	public static Geometry makePoint(double x, double y) {
		Geometry result = new Geometry(ogrConstants.wkbPoint);
		result.AddPoint_2D(x, y);
		return result;
	}

	/**
	 * @return a segment as Geometry to link the two points.
	 */
	public static Geometry makeSegments(double[] point1, double[] point2) {
		return makeSegment(point1[0], point1[1], point2[0], point2[1]);
	}

	/**
	 * Release the dataset asynchronously.
	 */
	public static void asyncDeleteDataset(Dataset dataset) {
		if (dataset != null) {
			CompletableFuture.runAsync(dataset::delete);
		}
	}

	/**
	 * Generates a raster file with specified informations
	 */
	public static Dataset generateDataset(double[][] data, double missingValue, double[] geoTransform) {
		// Raster size
		int ySize = data.length;
		int xSize = data[0].length;

		// Create the raster
		Dataset result = createDataset(xSize, ySize, gdalconstConstants.GDT_Float32, gdalconstConstants.GCI_GrayIndex);
		if (result != null) {
			// Write raster
			Band band = result.GetRasterBand(1);
			band.SetNoDataValue(missingValue);
			for (int y = 0; y < ySize; y++) {
				band.WriteRaster(0, y, xSize, 1, data[y]);
			}

			// Position
			result.SetGeoTransform(geoTransform);
			result.SetSpatialRef(GdalOsrUtils.SRS_WGS84);

			// Upate stats
			updateStatistics(band);

			result.FlushCache();

			// Free
			band.delete();
		}
		return result;
	}

	/**
	 * Generates a raster file with specified informations
	 */
	public static void generateFile(double[][] data, double missingValue, double[] geoTransform, String outFormat,
			File outfile) {

		// Create the raster
		Dataset memDataset = generateDataset(data, missingValue, geoTransform);
		if (memDataset != null) {
			// Generate resulting file
			org.gdal.gdal.Driver outDriver = gdal.GetDriverByName(outFormat);
			Dataset gmtDataset = outDriver.CreateCopy(outfile.getAbsolutePath(), memDataset);
			// Free
			memDataset.delete();
			if (gmtDataset != null) {
				gmtDataset.FlushCache();
				gmtDataset.delete();
			}
		}
	}

	/**
	 * Create a Geotiff file.
	 *
	 * @throws Exception
	 */
	public static Dataset generateTiffFile(int lineCount, int columnCount, float missingValue, double latNorth,
			double latSouth, double lonWest, double lonEast, BiIntToFloatFunction rasterValueSupplier, String outFile,
			ProgressCallback progressCallback) {

		// Generate the raster
		Dataset result = createDataset(TIF_DRIVER_NAME, outFile, columnCount, lineCount, gdalconstConstants.GDT_Float32,
				gdalconstConstants.GCI_GrayIndex);
		if (result != null) {
			Band band = result.GetRasterBand(1);
			band.SetNoDataValue(missingValue);
			writeRaster(band, rasterValueSupplier, progressCallback);

			// Configure the file (stats, geoloc, projection)
			updateStatistics(band);
			setBoundingBox(result, latNorth, latSouth, lonWest, lonEast);
			result.SetSpatialRef(GdalOsrUtils.SRS_WGS84);

			// Clean
			result.FlushCache();
			band.delete();
		}
		return result;
	}

	/**
	 * Create a Geotiff file.
	 *
	 * @param projection the projection reference string (OGC WKT or PROJ.4 format)
	 * @return null if error occured
	 */
	public static Dataset generateTiffFile(int lineCount, int columnCount, float missingValue, String projection,
			double latNorth, double latSouth, double lonWest, double lonEast, BiIntToFloatFunction rasterValueSupplier,
			String outFile, ProgressCallback progressCallback) {

		// Generate the raster
		Dataset result = createDataset(TIF_DRIVER_NAME, outFile, columnCount, lineCount, gdalconstConstants.GDT_Float32,
				gdalconstConstants.GCI_GrayIndex);
		if (result != null) {
			Band band = result.GetRasterBand(1);
			int ok = band.SetNoDataValue(missingValue);
			if (ok == OK) {
				ok = writeRaster(band, rasterValueSupplier, progressCallback);
			}
			if (ok == OK) {
				ok = updateStatistics(band);
			}
			if (ok == OK) {
				ok = setBoundingBox(result, latNorth, latSouth, lonWest, lonEast);
			}
			if (ok == OK) {
				if (projection != null) {
					ok = result.SetProjection(projection);
				} else {
					ok = result.SetSpatialRef(GdalOsrUtils.SRS_WGS84);
				}
			}

			// Clean
			band.delete();
			if (ok == OK) {
				result.FlushCache();
			} else {
				result.delete();
				result = null;
			}
		}
		return result;
	}

	/**
	 * Update statistics of the band
	 */
	public static int updateStatistics(Band band) {
		double[] min = { 0d };
		double[] max = { 0d };
		double[] mean = { 0d };
		double[] stddev = { 0d };
		int result = band.ComputeStatistics(false, min, max, mean, stddev);
		if (result == OK) {
			result = band.SetStatistics(min[0], max[0], mean[0], stddev[0]);
		}
		return result;
	}

	/**
	 * Translate a raster to PNG.
	 */
	public static Dataset translateToPNG(File tempFile, Dataset dataset) {
		Vector<String> options = new Vector<>();
		options.add("-ot");
		options.add("Byte");
		options.add("-of");
		options.add("PNG");
		options.add("-scale");
		options.add("-a_nodata");
		options.add("255");

		TranslateOptions translateOptions = new TranslateOptions(options);
		Dataset result = gdal.Translate(tempFile.getAbsolutePath(), dataset, translateOptions);
		translateOptions.delete();
		return result;
	}

	/**
	 * Translate the specified sataset to an oter format
	 *
	 * @param dataset dataset to translate
	 * @param outFormat expected format (TIF_DRIVER_NAME, XYZ_DRIVER_NAME...)
	 * @param outFile destination file
	 *
	 * @return resulting dataset
	 */
	public static void translate(Dataset dataset, String outFormat, String outFile) {
		Band band = dataset.GetRasterBand(1);
		Vector<String> options = new Vector<>();
		options.add("-ot");
		options.add(gdal.GetDataTypeName(band.getDataType()));
		options.add("-of");
		options.add(outFormat);
		options.add("-a_nodata");
		options.add(String.valueOf(GdalUtils.getNoDataValue(band)));
		options.add("-stats");

		TranslateOptions translateOptions = new TranslateOptions(options);
		Dataset result = gdal.Translate(outFile, dataset, translateOptions);
		if (result != null) {
			asyncDeleteDataset(result);
		}

		translateOptions.delete();
		band.delete();
	}

	/**
	 * Affect the bounding box to the Dataset
	 *
	 * @return the transformation matrix
	 */
	public static int setBoundingBox(Dataset dataset, double latNorth, double latSouth, double lonWest,
			double lonEast) {
		return dataset.SetGeoTransform(computeGeoTransform(dataset.getRasterXSize(), dataset.getRasterYSize(), latNorth,
				latSouth, lonWest, lonEast));
	}

	/**
	 * compute transformation matrix from the bounding box
	 *
	 * @return the transformation matrix
	 */
	public static double[] computeGeoTransform(int xSize, int ySize, double latNorth, double latSouth, double lonWest,
			double lonEast) {
		return new double[] { lonWest, (lonEast - lonWest) / xSize, 0d, latNorth, 0d, (latSouth - latNorth) / ySize };
	}

	/**
	 * compute transformation matrix from the bounding box
	 *
	 * @return the transformation matrix
	 */
	public static double[] computeGeoTransform(int xSize, int ySize, double[] boundingBox) {
		return computeGeoTransform(xSize, ySize, boundingBox[0], boundingBox[1], boundingBox[2], boundingBox[3]);
	}

	/**
	 * Builds vector contour lines from a raster elevation model (band).
	 *
	 * @param spatialReference the coordinate system to use for the layer. Assumed SRS_WKT_WGS84 if null
	 *
	 * @return the created Datasource. May be null if error occurs. Must be deleted
	 */
	public static DataSource generateContour(SpatialReference spatialReference, Band band, double contourInterval,
			double contourBase, String layerName, String elevationAttributeName, String outFile,
			ProgressCallback progressCallback) throws IOException {
		// Create Shape file
		Driver driver = ogr.GetDriverByName(SHP_DRIVER_NAME);
		DataSource result = driver.CreateDataSource(outFile);
		if (result != null) {
			generateContour(spatialReference, band, contourInterval, contourBase, layerName, elevationAttributeName,
					result, progressCallback);
		}
		return result;
	}

	/**
	 * Builds vector contour lines from a raster elevation model (band).
	 *
	 * @param spatialReference the coordinate system to use for the layer. Assumed SRS_WKT_WGS84 if null
	 */
	public static void generateContour(SpatialReference spatialReference, Band band, double contourInterval,
			double contourBase, String layerName, String elevationAttributeName, DataSource shapfile,
			ProgressCallback progressCallback) throws IOException {
		Layer layer = shapfile.CreateLayer(layerName,
				spatialReference != null ? spatialReference : GdalOsrUtils.SRS_WGS84, ogrConstants.wkbLineString);
		if (layer == null) {
			throw new IOException(gdal.GetLastErrorMsg());
		}
		FieldDefn idFieldDefn = new FieldDefn("id", ogrConstants.OFTInteger);
		layer.CreateField(idFieldDefn);
		FieldDefn elevationFieldDefn = new FieldDefn(elevationAttributeName, ogrConstants.OFTReal);
		layer.CreateField(elevationFieldDefn);

		// Generate contour
		boolean success = OK == gdal.ContourGenerate(band, contourInterval, contourBase, null, TRUE,
				getNoDataValue(band), layer, layer.FindFieldIndex("id", TRUE),
				layer.FindFieldIndex(elevationAttributeName, TRUE), progressCallback);

		// Clean
		elevationFieldDefn.delete();
		idFieldDefn.delete();
		layer.delete();

		if (success) {
			shapfile.FlushCache();
		} else {
			shapfile.delete();
		}
	}

	/**
	 * @return the min and max values of the band
	 */
	public static DoubleRange getMinMaxValues(Band band) {
		DoubleRange result = null;
		Double[] storeMin = new Double[1];
		band.GetMinimum(storeMin);
		Double[] storeMax = new Double[1];
		band.GetMaximum(storeMax);

		if (storeMin[0] == null || storeMax[0] == null) {
			double[] min = new double[1];
			double[] max = new double[1];
			double[] mean = new double[1];
			double[] stddev = new double[1];
			if (band.GetStatistics(false, true, min, max, mean, stddev) == gdalconstConstants.CE_None) {
				result = new DoubleRange(min[0], max[0]);
			}
		} else {
			result = new DoubleRange(storeMin[0], storeMax[0]);
		}
		logger.debug("getMinMaxValues returns {}", result);
		return result;
	}

	/**
	 * @return the min and max values of the band
	 * @recompute true to force the computation of min max values or false to use values in metadata
	 */
	public static DoubleRange getMinMaxValues(Band band, boolean recompute) {
		// Grab stats
		double[] min = new double[1];
		double[] max = new double[1];
		double[] mean = new double[1];
		double[] stddev = new double[1];
		band.GetStatistics(false, recompute, min, max, mean, stddev);
		return new DoubleRange(min[0], max[0]);
	}

	/** Generate a file name to represent a file in memory. */
	public static String generateInMemoryFilePath() {
		return "/vsimem/Memory-" + System.currentTimeMillis();
	}

	/** @return true when the file is in memory. */
	public static boolean isInMemoryFilePath(String filePath) {
		return filePath.startsWith("/vsimem");
	}

	/**
	 * Deletes a file object from the file system. This method goes through the VSIFileHandler virtualization and may
	 * work on unusual filesystems such as in memory.
	 */
	public static boolean deleteFile(String filepath) {
		return gdal.Unlink(filepath) == OK;
	}

	/**
	 * @return a ProgressCallback to follow long processes
	 */
	public static ProgressCallback newProgressCallback(IProgressMonitor monitor, int nbTicks) {
		AtomicInteger totalWorked = new AtomicInteger(0);
		return new ProgressCallback() {
			/**
			 * Follow the link.
			 *
			 * @see org.gdal.gdal.ProgressCallback#run(double, java.lang.String)
			 */
			@Override
			public int run(double complete, String message) {
				if (!monitor.isCanceled()) {
					int worked = (int) (complete * nbTicks);
					int progress = worked - totalWorked.get();
					totalWorked.set(worked);
					if (progress > 0) {
						monitor.worked(progress);
					}
					return 1;
				}
				return 0;
			}
		};
	}

	/**
	 * @return a ProgressCallback to follow long processes
	 */
	public static ProgressCallback newProgressCallback(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 1000);
		return newProgressCallback(subMonitor, 1000);
	}

	/** Return the number of byte to store the data type of the band */
	public static int getDataTypeSize(Band band) {
		int type = band.getDataType();
		if (type == gdalconstConstants.GDT_CInt16 || type == gdalconstConstants.GDT_Int16
				|| type == gdalconstConstants.GDT_UInt16)
			return Short.BYTES;
		if (type == gdalconstConstants.GDT_CInt32 || type == gdalconstConstants.GDT_Int32
				|| type == gdalconstConstants.GDT_UInt32)
			return Integer.BYTES;
		if (type == gdalconstConstants.GDT_CFloat32 || type == gdalconstConstants.GDT_Float32)
			return Float.BYTES;
		if (type == gdalconstConstants.GDT_CFloat64 || type == gdalconstConstants.GDT_Float64)
			return Double.BYTES;
		return Byte.BYTES;
	}
}
