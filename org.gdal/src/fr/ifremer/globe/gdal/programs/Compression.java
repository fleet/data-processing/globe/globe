/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.gdal.programs;

/**
 * The compression to use for tiff files
 * https://gdal.org/drivers/raster/gtiff.html#raster-gtiff
 */
public enum Compression {
	JPEG, LZW, PACKBITS, DEFLATE, CCITTRLE, CCITTFAX3, CCITTFAX4, LZMA, ZSTD, LERC, LERC_DEFLATE, LERC_ZSTD, WEBP, JXL,
	NONE;
}
