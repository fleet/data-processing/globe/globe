package fr.ifremer.globe.gdal.programs;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.TranslateOptions;
import org.gdal.gdal.gdal;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Provides methods to configure and launch a gdal_translate command (Converts raster data between different formats).
 *
 */
public class GdalTranslate {

	/** Logger */
	protected final Logger logger = LoggerFactory.getLogger(GdalTranslate.class);

	/** List of options */
	protected List<String> options = new LinkedList<>();

	/** Select the output format */
	private String format = GdalUtils.TIF_DRIVER_NAME;

	// input Dataset
	private final Dataset inputDataset;

	// output file
	private Optional<File> outputFile = Optional.empty();
	/**
	 * Assign a specified nodata value to output bands. It can be set to none to avoid setting a nodata value to the
	 * output file if one exists for the source file
	 */
	private String noDataValue = "none";

	/**
	 * Constructor of {@link GdalTranslate}.
	 */
	public GdalTranslate(Dataset inputDataset) {
		this.inputDataset = inputDataset;
	}

	/**
	 * Launches the program.
	 *
	 * @return the generated {@link Dataset}.
	 */
	public GdalDataset run(IProgressMonitor monitor) {
		GdalDataset result = GdalDataset.empty();
		try {
			if (outputFile.isEmpty()) {
				outputFile = Optional.of(TemporaryCache.createTemporaryFile("Translate_", ".tif"));
			}

			logger.debug("Start GdalTranslate process for {}", outputFile.get().getPath());
			TranslateOptions translateOptions = buildOptions();
			gdal.ErrorReset();
			Dataset resultingDataset = gdal.Translate(outputFile.get().getAbsolutePath(), inputDataset,
					translateOptions, GdalUtils.newProgressCallback(monitor));
			if (resultingDataset != null) {
				result = new GdalDataset(resultingDataset);
			} else {
				String error = gdal.GetLastErrorMsg();
				logger.warn("GdalTranslate failed : {}", error);
			}
			logger.debug("End of GdalTranslate process for {}", outputFile.get().getAbsolutePath());
		} catch (IOException e) {
			logger.warn("GdalTranslate failed : {}", e.getMessage());
		}

		return result;
	}

	/**
	 * Launches the program and close the resulting file.
	 *
	 * @param src input {@link Dataset}
	 * @param outputFile output {@link File}
	 */
	public void runAndClose(IProgressMonitor monitor) {
		var result = run(monitor);
		result.close();
	}

	/**
	 * Builds command options.
	 */
	private TranslateOptions buildOptions() {
		var processOptions = new Vector<>(options);
		options.add("-a_nodata");
		options.add(noDataValue);
		if (GdalUtils.TIF_DRIVER_NAME.equals(format)) {
			// Add common option for a Tiff
			processOptions.add("-co");
			processOptions.add("COMPRESS=LZW");
			processOptions.add("-co");
			processOptions.add("PREDICTOR=2");
			processOptions.add("-co");
			processOptions.add("BIGTIFF=IF_SAFER");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Translate options : {}",
					processOptions.stream().map(op -> "'" + op + "'").collect(Collectors.joining(" ")));
		}

		return new TranslateOptions(processOptions);
	}

	/**
	 * Set the size of the output file. Outsize is in pixels and lines unless ‘%’ is attached in which case it is as a
	 * fraction of the input image size. If one of the 2 values is set to 0, its value will be determined from the other
	 * one, while maintaining the aspect ratio of the source dataset.
	 */
	public void addTargetSize(Number width, Number height) {
		options.add("-outsize");
		options.add(String.valueOf(width));
		options.add(String.valueOf(height));
	}

	/**
	 * Assign a specified nodata value to output bands.
	 */
	public void addNoData(Number noDataValue) {
		this.noDataValue = noDataValue.toString();
	}

	/**
	 * @param format the {@link #format} to set
	 */
	public void setFormat(String format) {
		this.format = format;
		options.add("-of");
		options.add(format);
	}

	/**
	 * Apply the scale/offset metadata for the bands to convert scaled values to unscaled values
	 */
	public void setUnscale() {
		options.add("-unscale");
	}

	/**
	 * @param outputFile the {@link #outputFile} to set
	 */
	public void setOutputFile(File outputFile) {
		this.outputFile = Optional.of(outputFile);
	}

	/**
	 * Force the output image bands to have a specific data type supported by the driver
	 */
	public <T extends Number> void setOutputType(Class<T> kindOfData) {
		options.add("-ot");
		if (Short.class == kindOfData)
			options.add("Int16");
		else if (Integer.class == kindOfData)
			options.add("Int32");
		else if (Long.class == kindOfData)
			options.add("Int32");
		else if (Float.class == kindOfData)
			options.add("Float32");
		else if (Double.class == kindOfData)
			options.add("Float64");
		else
			options.add("Byte");
	}

	/** Override the projection for the output file. */
	public void setProjection(SpatialReference projection) {
		options.add("-a_srs");
		options.add(projection.ExportToProj4());
	}

}
