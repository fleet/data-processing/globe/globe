package fr.ifremer.globe.gdal.programs;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.WarpOptions;
import org.gdal.gdal.gdal;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;

/**
 * Provides methods to configure and launch a gdalwarp command (image reprojection and warping utility : see
 * https://gdal.org/programs/gdalwarp.html).
 *
 */
public class GdalWarp {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalWarp.class);

	/**
	 * Program parameters
	 */

	// input Dataset
	private final Dataset inputDataset;

	// output file
	private final String outputFile;

	// overwrite
	private static final String OPT_OVERWRITE = "-overwrite";
	private boolean overwrite;

	// source spatial reference
	private static final String OPT_SOURCE_SRS = "-s_srs";
	private Optional<SpatialReference> sourceProjection = Optional.empty();

	// target spatial reference
	private static final String OPT_TARGET_SRS = "-t_srs";
	private Optional<SpatialReference> targetProjection = Optional.empty();
	// target resolution
	private static final String OPT_TARGET_RESOLUTION = "-tr";
	private Optional<double[]> targetResolution = Optional.empty();
	// target size
	private static final String OPT_TARGET_SIZE = "-ts";
	private Optional<int[]> targetSize = Optional.empty();
	// target aligned pixels
	private static final String OPT_TARGET_ALIGNED_PIXELS = "-tap";
	private boolean targetAlignedPixels = false;

	// Set georeferenced extents of output file to be created (in target SRS)
	private static final String OPT_GEO_BOX = "-te";
	private Optional<double[]> geobox = Optional.empty();

	// Resampling method to use
	private static final String OPT_RESAMPLING = "-r";
	private Optional<ResamplingMethod> resampling = Optional.empty();

	// Do not copy metadata
	private static final String OPT_NO_METADATA = "-nomd";
	private boolean metadata = true;

	// Set the color interpretation
	private static final String OPT_SET_COLOR_INTERPRETATION = "-setci";
	private boolean colorInterpretation = false;

	// Force the last band of a source image to be considered as a source alpha band
	private static final String OPT_SRC_ALPHA = "-srcalpha";
	private boolean srcalpha = false;

	// Set nodata values for output bands
	private static final String OPT_DST_NO_DATA = "-dstnodata";
	private Optional<Number> dstnodata = Optional.empty();
	// 
	private static final String OPT_TARGET_TYPE = "-ot";
	private Optional<String> targetType = Optional.empty();
	
	/** Progression monitor */
	private IProgressMonitor monitor = new NullProgressMonitor();

	private static final String OPT_CREATION_OPTION = "-co";
	// Resampling method to use
	private Optional<Photometric> photometric = Optional.empty();
	// Activate compression 
	private Optional<Compression> compression = Optional.empty();

	/**
	 * Available resampling methods (see gdalwarp doc to add others...)
	 */
	public enum ResamplingMethod {
		NEAR("near"), // nearest neighbour resampling (default, fastest algorithm, worst interpolation quality)
		BILINEAR("bilinear"), // bilinear resampling
		AVERAGE("average"); // average resampling, computes the average of all non-NODATA contributing pixels

		private final String value;

		ResamplingMethod(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	/**
	 * Constructor of {@link GdalWarp} : image reprojection and warping.
	 */
	public GdalWarp(Dataset inputDataset, String outputFile) {
		this.inputDataset = inputDataset;
		this.outputFile = outputFile;
	}

	/**
	 * Launches the program.
	 *
	 * @param src input {@link Dataset}
	 * @param outputFile output {@link File}
	 *
	 * @return the generated {@link Dataset}.
	 */
	public Dataset run() {
		logger.debug("Start Gdal warp process for {}", outputFile);
		WarpOptions options = buildOptions();
		gdal.ErrorReset();
		Dataset resultDataset = gdal.Warp(outputFile, new Dataset[] { inputDataset }, options,
				GdalUtils.newProgressCallback(monitor));
		if (resultDataset != null) {
			resultDataset.FlushCache();
		}
		options.delete();
		logger.debug("End of Gdal warp process for {}", outputFile);
		return resultDataset;
	}

	/**
	 * Launches the program and close the resulting file.
	 *
	 * @param src input {@link Dataset}
	 * @param outputFile output {@link File}
	 */
	public void runAndClose() {
		var result = run();
		if (result != null)
			result.delete();
	}

	/**
	 * Builds command options.
	 */
	private WarpOptions buildOptions() {
		var options = new Vector<>();

		// This is a number of extra pixels added around the source to take care
		// of rounding error
		// options.add("-wo");
		// options.add("SOURCE_EXTRA=1000");

		if (overwrite) {
			options.add(OPT_OVERWRITE);
		}

		if (sourceProjection.isPresent()) {
			options.add(OPT_SOURCE_SRS);
			options.add(sourceProjection.get().ExportToProj4());
		}

		if (targetProjection.isPresent()) {
			options.add(OPT_TARGET_SRS);
			options.add(targetProjection.get().ExportToProj4());
		}

		if (targetResolution.isPresent()) {
			options.add(OPT_TARGET_RESOLUTION);
			options.add(String.valueOf(targetResolution.get()[0]));
			options.add(String.valueOf(targetResolution.get()[1]));
		}

		if (targetSize.isPresent()) {
			options.add(OPT_TARGET_SIZE);
			options.add(String.valueOf(targetSize.get()[0]));
			options.add(String.valueOf(targetSize.get()[1]));
		}

		if (targetAlignedPixels) {
			options.add(OPT_TARGET_ALIGNED_PIXELS);
		}

		if (geobox.isPresent()) {
			options.add(OPT_GEO_BOX);
			options.add(String.valueOf(geobox.get()[0]));
			options.add(String.valueOf(geobox.get()[1]));
			options.add(String.valueOf(geobox.get()[2]));
			options.add(String.valueOf(geobox.get()[3]));
		}

		if (resampling.isPresent()) {
			options.add(OPT_RESAMPLING);
			options.add(resampling.get().toString());
		}

		if (!metadata) {
			options.add(OPT_NO_METADATA);
		}

		dstnodata.ifPresent(opt -> {
			options.add(OPT_DST_NO_DATA);
			options.add(opt.toString());
		});

		targetType.ifPresent(opt -> {
			options.add(OPT_TARGET_TYPE);
			options.add(opt);
		});

		if (colorInterpretation) {
			options.add(OPT_SET_COLOR_INTERPRETATION);
		}

		if (srcalpha) {
			options.add(OPT_SRC_ALPHA);
		}

		photometric.ifPresent(opt -> {
			options.add(OPT_CREATION_OPTION);
			options.add("PHOTOMETRIC=" + opt.name());
		});

		compression.ifPresent(opt -> {
			options.add(OPT_CREATION_OPTION);
			options.add("COMPRESS=" + opt.name());
		});

		if (logger.isDebugEnabled()) {
			logger.debug("Warp options : {}",
					options.stream().map(op -> "'" + op.toString() + "'").collect(Collectors.joining(" ")));
		}

		return new WarpOptions(options);
	}

	/**
	 * Methods to set optional parameters
	 */

	public GdalWarp addOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
		return this;
	}

	public GdalWarp addMetadata(boolean metadata) {
		this.metadata = metadata;
		return this;
	}

	public GdalWarp addTargetNoData(Number value) {
		dstnodata = Optional.of(value);
		return this;
	}

	public GdalWarp addSourceProjection(SpatialReference sourceProjection) {
		this.sourceProjection = Optional.of(sourceProjection);
		return this;
	}

	public GdalWarp addTargetProjection(SpatialReference targetProjection) {
		this.targetProjection = Optional.of(targetProjection);
		return this;
	}

	public GdalWarp addGeobox(double xmin, double ymin, double xmax, double ymax) {
		geobox = Optional.of(new double[] { xmin, ymin, xmax, ymax });
		return this;
	}

	public GdalWarp addGeobox(double[] extent) {
		geobox = Optional.of(extent);
		return this;
	}

	/**
	 * Set georeferenced extents (xmin ymin xmax ymax)
	 */
	public GdalWarp addTargetExtent(double[] extent) {
		geobox = Optional.ofNullable(extent);
		return this;
	}

	public GdalWarp addTargetResolution(double xres, double yres) {
		targetResolution = Optional.of(new double[] { xres, yres });
		return this;
	}

	public GdalWarp addTargetSize(int width, int height) {
		targetSize = Optional.of(new int[] { width, height });
		return this;
	}
	
	public GdalWarp addTargetAlignedPixels(boolean targetAlignedPixels) {
		this.targetAlignedPixels = targetAlignedPixels;
		return this;
	}
	
	public GdalWarp addTargetType(String type) {
		targetType = Optional.of(type);
		return this;
	}

	public GdalWarp addResamplingMethod(ResamplingMethod resamplingMethod) {
		resampling = Optional.of(resamplingMethod);
		return this;
	}

	public GdalWarp addPhotometric(Photometric photometric) {
		this.photometric = Optional.of(photometric);
		return this;
	}

	public GdalWarp addCompression(Compression compression) {
		this.compression = Optional.of(compression);
		return this;
	}

	public GdalWarp addProgressMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
		return this;
	}

	/**
	 * @param colorInterpretation the {@link #colorInterpretation} to set
	 */
	public GdalWarp addColorInterpretation(boolean colorInterpretation) {
		this.colorInterpretation = colorInterpretation;
		return this;
	}

	/**
	 * @param srcalpha the {@link #srcalpha} to set
	 */
	public GdalWarp addSrcAlpha(boolean srcalpha) {
		this.srcalpha = srcalpha;
		return this;
	}

	/**
	 * Creates a Warp process to the wrap dataset with Wgs84 as target spatial reference.
	 */
	public static GdalWarp warpToSrsWgs84(Dataset dataset) throws IOException {
		GdalWarp gdalWarp = new GdalWarp(dataset, GdalUtils.generateInMemoryFilePath());
		gdalWarp.addMetadata(false);
		gdalWarp.addTargetProjection(GdalOsrUtils.SRS_WGS84);
		return gdalWarp;
	}

	/**
	 * Creates a Warp process to the wrap dataset with UTM as target spatial reference.
	 */
	public static GdalWarp warpToUtm(Dataset dataset) throws IOException {
		// Computes the SRS centered of dataset
		SpatialReference mercatorSrs = new SpatialReference();
		mercatorSrs.SetProjCS("Utm");
		mercatorSrs.SetWellKnownGeogCS("WGS84");

		// Utm parameter
		double[] extent = GdalUtils.getExtent(dataset);
		double lat = (extent[3] + extent[1]) / 2d;
		double lon = (extent[2] + extent[0]) / 2d;
		int zone = (int) ((lon + 180) / 6) + 1;
		int north = -80 < lat && lat < 0 ? 0 : 1;
		mercatorSrs.SetUTM(zone, north);

		GdalWarp gdalWarp = new GdalWarp(dataset, GdalUtils.generateInMemoryFilePath());
		gdalWarp.addMetadata(false);
		gdalWarp.addTargetProjection(mercatorSrs);
		return gdalWarp;
	}

	/**
	 * @return the {@link #outputFile}
	 */
	public String getOutputFile() {
		return outputFile;
	}
}
