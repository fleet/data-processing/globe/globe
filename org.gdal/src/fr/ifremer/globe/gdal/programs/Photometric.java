/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.gdal.programs;

/**
 * Photometric interpretation tag for tiff files
 */
public enum Photometric {
	MINISBLACK, MINISWHITE, RGB, CMYK, YCBCR, CIELAB, ICCLAB, ITULAB;
}
