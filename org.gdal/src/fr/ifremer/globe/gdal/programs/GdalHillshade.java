package fr.ifremer.globe.gdal.programs;

import java.io.File;

import org.gdal.gdal.Dataset;

/**
 * Generate a shaded relief map from any GDAL-supported elevation raster
 *
 * <pre>
 * gdaldem hillshade input_dem output_hillshade
            [-z ZFactor (default=1)] [-s scale* (default=1)]
            [-az Azimuth (default=315)] [-alt Altitude (default=45)]
            [-alg Horn|ZevenbergenThorne] [-combined | -multidirectional | -igor]
            [-compute_edges] [-b Band (default=1)] [-of format] [-co "NAME=VALUE"]* [-q]
 * </pre>
 */
public class GdalHillshade extends AbstractGdalDem {

	/**
	 * Constructor of {@link GdalHillshade}.
	 */
	public GdalHillshade(Dataset inputDem, File outputFile) {
		super("hillshade", inputDem, outputFile);
	}

	/**
	 * Set Vertical exaggeration used to pre-multiply the elevations
	 */
	public void setZfactor(double value) {
		options.add("-f");
		options.add(String.valueOf(value));
	}

	/**
	 * Set Ratio of vertical units to horizontal.
	 */
	public void setScale(double value) {
		options.add("-s");
		options.add(String.valueOf(value));
	}

	/**
	 * Set Azimuth of the light, in degrees
	 */
	public void setAzimuth(double value) {
		options.add("-az");
		options.add(String.valueOf(value));
	}

	/**
	 * Altitude of the light, in degrees.
	 */
	public void setAltitude(double value) {
		options.add("-alt");
		options.add(String.valueOf(value));
	}

	/**
	 * Set algo as ZevenbergenThorne
	 */
	public void setZevenbergenThorne() {
		options.add("-alg");
		options.add("ZevenbergenThorne");
	}

	/**
	 * combined shading, a combination of slope and oblique shading.
	 */
	public void setCombined() {
		options.add("-combined");
	}

	/**
	 * multidirectional shading, a combination of hillshading illuminated from 225 deg, 270 deg, 315 deg, and 360 deg
	 * azimuth.
	 */
	public void setMultidirectional() {
		options.add("-multidirectional");
	}

	/**
	 * shading which tries to minimize effects on other map features beneath.
	 */
	public void setIgor() {
		options.add("-igor");
	}

	/**
	 * Do the computation at raster edges and near nodata values
	 */
	public void setComputeEdges() {
		options.add("-compute_edges");
	}

}
