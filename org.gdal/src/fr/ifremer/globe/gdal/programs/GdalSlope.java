package fr.ifremer.globe.gdal.programs;

import java.io.File;

import org.gdal.gdal.Dataset;

/**
 * Generate a slope map from any GDAL-supported elevation raster
 *
 * <pre>
 * gdaldem slope input_dem output_slope_map
                 [-p use percent slope (default=degrees)] [-s scale* (default=1)]
                 [-alg Horn|ZevenbergenThorne]
                 [-compute_edges] [-b Band (default=1)] [-of format] [-co "NAME=VALUE"]* [-q]
 * </pre>
 */
public class GdalSlope extends AbstractGdalDem {

	/**
	 * Constructor of {@link GdalSlope}.
	 */
	public GdalSlope(Dataset inputDem, File outputFile) {
		super("slope", inputDem, outputFile);
	}

	/**
	 * Slope will be expressed as percent slope. Otherwise, it is expressed as degrees
	 */
	public void setPercent() {
		options.add("-p");
	}

	/**
	 * Set Ratio of vertical units to horizontal.
	 */
	public void setScale(double value) {
		options.add("-s");
		options.add(String.valueOf(value));
	}

	/**
	 * Set algo as ZevenbergenThorne
	 */
	public void setZevenbergenThorne() {
		options.add("-alg");
		options.add("ZevenbergenThorne");
	}

	/**
	 * Set algo as Horn
	 */
	public void setHorn() {
		options.add("-alg");
		options.add("Horn");
	}

	/**
	 * Do the computation at raster edges and near nodata values
	 */
	public void setComputeEdges() {
		options.add("-compute_edges");
	}

}
