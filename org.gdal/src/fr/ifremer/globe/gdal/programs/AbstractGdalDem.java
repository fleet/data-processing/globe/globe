package fr.ifremer.globe.gdal.programs;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.gdal.gdal.DEMProcessingOptions;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;

/**
 * Provides methods to configure and launch a gdaldem command (Tools to analyze and visualize DEMs).
 *
 */
abstract class AbstractGdalDem {

	/** Logger */
	protected final Logger logger = LoggerFactory.getLogger(AbstractGdalDem.class);

	/** List of options */
	protected List<String> options = new LinkedList<>();

	/** Process mode */
	private final String mode;

	/** Select the output format */
	private String format = GdalUtils.TIF_DRIVER_NAME;

	// input Dataset
	private final Dataset inputDem;

	/** color text file containing lines of the format "elevation_value red green blue" */
	protected Optional<File> colorReliefMap = Optional.empty();

	// output file
	private final File outputFile;

	/**
	 * Constructor of {@link AbstractGdalDem}.
	 */
	protected AbstractGdalDem(String mode, Dataset inputDem, File outputFile) {
		this.mode = mode;
		this.inputDem = inputDem;
		this.outputFile = outputFile;
	}

	/**
	 * Launches the program.
	 *
	 * @return the generated {@link Dataset}.
	 */
	public GdalDataset run(IProgressMonitor monitor) {
		logger.debug("Start Gdaldem {} process for {}", mode, outputFile.getAbsolutePath());
		DEMProcessingOptions processingOptions = buildOptions();
		Dataset resultDataset = gdal.DEMProcessing(outputFile.getAbsolutePath(), inputDem, mode,
				colorReliefMap.map(File::getAbsolutePath).orElse(null), processingOptions,
				GdalUtils.newProgressCallback(monitor));
		if (resultDataset != null)
			resultDataset.FlushCache();
		processingOptions.delete();
		logger.debug("End of Gdaldem process for {}", outputFile.getAbsolutePath());
		return new GdalDataset(resultDataset);
	}

	/**
	 * Launches the program and close the resulting file.
	 *
	 * @param src input {@link Dataset}
	 * @param outputFile output {@link File}
	 */
	public void runAndClose(IProgressMonitor monitor) {
		var result = run(monitor);
		result.close();
	}

	/**
	 * Builds command options.
	 */
	private DEMProcessingOptions buildOptions() {
		var processOptions = new Vector<>(options);
		if (GdalUtils.TIF_DRIVER_NAME.equals(format)) {
			processOptions.add("-co");
			processOptions.add("BIGTIFF=IF_SAFER");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Dem options : {}",
					processOptions.stream().map(op -> "'" + op.toString() + "'").collect(Collectors.joining(" ")));
		}

		return new DEMProcessingOptions(processOptions);
	}

	/**
	 * Algorithm ZevenbergenThorne|Wilson|Riley
	 */
	protected void setAlgorithm(String algorithm) {
		options.add("-alg");
		options.add(algorithm);
	}

	/**
	 * Select an input band to be processed
	 */
	public void setBand(int band) {
		options.add("-b");
		options.add(String.valueOf(band));
	}

	/**
	 * @param format the {@link #format} to set
	 */
	public void setFormat(String format) {
		this.format = format;
		options.add("-of");
		options.add(format);
	}
}
