package fr.ifremer.globe.gdal.programs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

import org.apache.commons.lang.math.DoubleRange;
import org.gdal.gdal.Dataset;

import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Generate a color relief map from any GDAL-supported elevation raster
 *
 * <pre>
 * gdaldem color-relief input_dem color_text_file output_color_relief_map
                 [-alpha] [-exact_color_entry | -nearest_color_entry]
                 [-b Band (default=1)] [-of format] [-co "NAME=VALUE"]* [-q]
     where color_text_file contains lines of the format "elevation_value red green blue"
 * </pre>
 */
public class GdalColorRelief extends AbstractGdalDem {

	/**
	 * Constructor of {@link GdalColorRelief}.
	 */
	public GdalColorRelief(Dataset inputDem, File outputFile) {
		super("color-relief", inputDem, outputFile);
	}

	/**
	 * Generates the content of the color file
	 */
	public void fitColorsFor(DoubleRange minMax) throws IOException {
		File colorsFile = TemporaryCache.createTemporaryFile("ColorMap", ".txt");
		colorsFile.deleteOnExit();
		colorReliefMap = Optional.of(colorsFile);

		try (FileWriter colorsWriter = new FileWriter(colorsFile)) {
			int paletteSize = 0x1FFFF + 1;
			double stepElevation = (minMax.getMaximumDouble() - minMax.getMinimumDouble()) / paletteSize;
			int stepColor = 0x80;
			// Transparency under min value NaN
			colorsWriter.append(String.valueOf(minMax.getMinimumFloat() - 1f)).append(" 0 0 0 0\n");

			// Compute a palette of color between min and max values
			int color = 0;
			double depth = minMax.getMinimumDouble();
			for (int i = 0; i < paletteSize; i++) {
				int red = color >> 16 & 0xFF;
				int green = color >> 8 & 0xFF;
				int blue = color & 0xFF;
				colorsWriter.append(String.valueOf((float) depth))//
						.append(' ').append(String.valueOf(red))//
						.append(' ').append(String.valueOf(green))//
						.append(' ').append(String.valueOf(blue))//
						.append(" 255\n");
				depth += stepElevation;
				color += stepColor;
			}
			// Transparency above max value NaN
			colorsWriter.append(String.valueOf(minMax.getMaximumFloat())).append(" 255 255 255 255\n");
			// Transparency above max value NaN
			colorsWriter.append(String.valueOf(minMax.getMaximumFloat() + 1f)).append(" 0 0 0 0\n");

			// Transparency for NaN
			colorsWriter.append("nv 0 0 0 0").append('\n');
		}
	}

	/**
	 * Add an alpha channel to the output raster
	 */
	public void setAlpha() {
		options.add("-alpha");
	}

	/**
	 * Use strict matching when searching in the color configuration file
	 */
	public void setExactColorEntry() {
		options.add("-exact_color_entry");
	}

	/**
	 * Use the RGBA quadruplet corresponding to the closest entry in the color configuration file
	 */
	public void setNearestColorEntry() {
		options.add("-nearest_color_entry");
	}

}
