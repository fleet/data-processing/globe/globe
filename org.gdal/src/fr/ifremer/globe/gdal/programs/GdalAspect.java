package fr.ifremer.globe.gdal.programs;

import java.io.File;

import org.gdal.gdal.Dataset;

/**
 * Generate an aspect map from any GDAL-supported elevation raster, outputs a 32-bit float raster with pixel values from
 * 0-360 indicating azimuth
 *
 * <pre>
 * gdaldem aspect input_dem output_aspect_map
            [-trigonometric] [-zero_for_flat]
            [-alg Horn|ZevenbergenThorne]
            [-compute_edges] [-b Band (default=1)] [-of format] [-co "NAME=VALUE"]* [-q]
 * </pre>
 */
public class GdalAspect extends AbstractGdalDem {

	/**
	 * Constructor of {@link GdalAspect}.
	 */
	public GdalAspect(Dataset inputDem, File outputFile) {
		super("aspect", inputDem, outputFile);
	}

	/**
	 * Return trigonometric angle instead of azimuth. Thus 0° means East, 90° North, 180° West, 270° South
	 */
	public void setTrigonometric() {
		options.add("-trigonometric");
	}

	/**
	 * Return 0 for flat areas with slope=0, instead of -9999.
	 */
	public void setZeroForFlat() {
		options.add("-zero_for_flat");
	}

	/**
	 * Set algo as ZevenbergenThorne
	 */
	public void setZevenbergenThorne() {
		options.add("-alg");
		options.add("ZevenbergenThorne");
	}

	/**
	 * Set algo as Horn
	 */
	public void setHorn() {
		options.add("-alg");
		options.add("Horn");
	}

	/**
	 * Do the computation at raster edges and near nodata values
	 */
	public void setComputeEdges() {
		options.add("-compute_edges");
	}

}
