/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.gdal.dataset;

import java.io.Closeable;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang.math.DoubleRange;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.Driver;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;

/**
 * Holder of Dataset used in try catch ressource.
 */
public class GdalDataset implements Closeable {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(GdalDataset.class);

	/** Wrapped Dataset */
	private Optional<Dataset> dataset;

	/** Delete the file when closing */
	private boolean deleteOnClose = false;

	/**
	 * Constructor
	 */
	public GdalDataset(Dataset dataset) {
		this.dataset = Optional.ofNullable(dataset);
	}

	/** @return the file */
	public Optional<File> getFile() {
		if (dataset.isPresent()) {
			List<?> files = get().GetFileList();
			if (!files.isEmpty()) {
				return Optional.of(new File(files.get(0).toString()));
			}
		}
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public void close() {
		dataset.ifPresent(d -> {
			synchronized (gdal.class) {
				List<?> files = d.GetFileList();
				d.delete();
				if (deleteOnClose) {
					files.forEach(f -> gdal.Unlink(f.toString()));
				}
			}
		});
		dataset = Optional.empty();
	}

	/** Fetch raster width in pixels */
	public int getWidth() {
		return dataset.map(Dataset::getRasterXSize).orElse(0);
	}

	/** Fetch raster height in pixels */
	public int getHeight() {
		return dataset.map(Dataset::getRasterYSize).orElse(0);
	}

	/** Fetch raster height in pixels */
	public int getBandCount() {
		return dataset.map(Dataset::GetRasterCount).orElse(0);
	}

	/** @return true when the specified band is empty */
	public boolean isBandEmpty(int band) {
		boolean result = false;
		if (dataset.isPresent() && band >= 1 && getBandCount() >= band) {
			Band gdalBand = get().GetRasterBand(band);
			if (gdalBand != null) {
				double[] min = { 0d };
				double[] max = { 0d };
				double[] mean = { 0d };
				double[] stddev = { 0d };
				if (gdalBand.ComputeStatistics(false, min, max, mean, stddev) != gdalconstConstants.CE_None) {
					gdal.ErrorReset();
					result = true;
				}
				gdalBand.delete();
			}
		}
		return result;
	}

	/** Read the data of one band */
	public <T extends Number> Optional<ByteBuffer> read(int band, Class<T> kindOfData) {
		Optional<ByteBuffer> result = Optional.empty();

		if (dataset.isPresent() && band >= 1 && getBandCount() >= band) {
			Band gdalBand = get().GetRasterBand(band);
			if (gdalBand != null) {
				int width = getWidth();
				int height = getHeight();
				var buffer = ByteBuffer.allocateDirect(width * height * getGdalDataSize(kindOfData));
				buffer.order(ByteOrder.nativeOrder());
				gdal.ErrorReset();
				if (gdalBand.ReadRaster_Direct(0, 0, width, height, width, height, getGdalDataType(kindOfData),
						buffer) == gdalconstConstants.CE_None) {
					buffer.rewind();
					result = Optional.of(buffer);
				} else {
					String error = gdal.GetLastErrorMsg();
					logger.warn("Unreable band {} : {}", band, error);
				}
				gdalBand.delete();
			}
		}

		return result;
	}

	/**
	 * Read the data of all bands sequentially<br>
	 */
	public <T extends Number> Optional<ByteBuffer> readAll(Class<T> kindOfData) {
		Optional<ByteBuffer> result = Optional.empty();
		if (dataset.isPresent()) {
			int bandCount = getBandCount();
			int width = getWidth();
			int height = getHeight();
			var buffer = ByteBuffer.allocateDirect(width * height * getGdalDataSize(kindOfData) * bandCount);
			buffer.order(ByteOrder.nativeOrder());
			gdal.ErrorReset();
			if (get().ReadRaster_Direct(0, 0, width, height, width, height, getGdalDataType(kindOfData), buffer, //
					null /* null means all bands */) == gdalconstConstants.CE_None) {
				buffer.rewind();
				result = Optional.of(buffer);
			} else {
				String error = gdal.GetLastErrorMsg();
				logger.warn("Unable to read all bands : {}", error);
			}
		}
		return result;
	}

	/**
	 * Read the data of all bands<br>
	 * All values of all bands are mixed in the resulting buffer
	 */
	public <T extends Number> Optional<ByteBuffer> readAllMixed(Class<T> kindOfData) {
		Optional<ByteBuffer> result = Optional.empty();
		if (dataset.isPresent()) {
			int bandCount = getBandCount();
			int dataSize = getGdalDataSize(kindOfData);
			int width = getWidth();
			int height = getHeight();
			var buffer = ByteBuffer.allocateDirect(width * height * getGdalDataSize(kindOfData) * bandCount);
			buffer.order(ByteOrder.nativeOrder());
			gdal.ErrorReset();
			if (get().ReadRaster_Direct(0, 0, width, height, width, height, getGdalDataType(kindOfData), buffer, //
					null, // null means all bands
					bandCount * dataSize, // Pixel has bandCount values. Each value takes dataSize bytes
					0, //
					dataSize //
			) == gdalconstConstants.CE_None) {
				buffer.rewind();
				result = Optional.of(buffer);
			} else {
				String error = gdal.GetLastErrorMsg();
				logger.warn("Unable to read all bands : {}", error);
			}
		}

		return result;
	}

	/**
	 * @param deleteOnClose the {@link #deleteOnClose} to set
	 */
	public void deleteOnClose(boolean delete) {
		deleteOnClose = delete;
	}

	public static GdalDataset empty() {
		return new GdalDataset(null);
	}

	/**
	 * @return the extent of the raster (xmin ymin xmax ymax)
	 */
	public double[] getExtent() {
		return map(GdalUtils::getExtent).orElse(new double[4]);
	}

	/** Return the {@link SpatialReference} of the {@link Dataset} */
	public SpatialReference getSpatialReference() {
		return map(GdalUtils::getSpatialReference).orElse(GdalOsrUtils.SRS_WGS84);
	}

	/** Open a file */
	public static GdalDataset open(String filepath) {
		synchronized (gdal.class) {
			return new GdalDataset(gdal.Open(filepath, gdalconstConstants.GA_ReadOnly));
		}
	}

	/** Open a file with access flags */
	public static GdalDataset open(String filepath, int eAccess) {
		synchronized (gdal.class) {
			return new GdalDataset(gdal.Open(filepath, eAccess));
		}
	}

	/** Delegated method to dataset */
	public boolean isPresent() {
		return dataset.isPresent();
	}

	/** Delegated method to dataset */
	public boolean isEmpty() {
		return dataset.isEmpty();
	}

	/** Delegated method to dataset */
	public Dataset get() {
		return dataset.get();
	}

	/** Delegated method to dataset */
	public void ifPresent(Consumer<? super Dataset> action) {
		dataset.ifPresent(action);
	}

	/** Delegated method to dataset */
	public void ifPresentOrElse(Consumer<? super Dataset> action, Runnable emptyAction) {
		dataset.ifPresentOrElse(action, emptyAction);
	}

	/** Delegated method to dataset */
	public Optional<Dataset> filter(Predicate<? super Dataset> predicate) {
		return dataset.filter(predicate);
	}

	/** Delegated method to dataset */
	public <U> Optional<U> map(Function<? super Dataset, ? extends U> mapper) {
		return dataset.map(mapper);
	}

	/** Delegated method to dataset */
	public <U> Optional<U> flatMap(Function<? super Dataset, ? extends Optional<? extends U>> mapper) {
		return dataset.flatMap(mapper);
	}

	/** Delegated method to dataset */
	public Optional<Dataset> or(Supplier<? extends Optional<? extends Dataset>> supplier) {
		return dataset.or(supplier);
	}

	/** Delegated method to dataset */
	public Dataset orElse(Dataset other) {
		return dataset.orElse(other);
	}

	/** Delegated method to dataset */
	public Dataset orElseGet(Supplier<? extends Dataset> supplier) {
		return dataset.orElseGet(supplier);
	}

	/** Delegated method to dataset */
	public Dataset orElseThrow() {
		return dataset.orElseThrow();
	}

	/** Delegated method to dataset */
	public <X extends Throwable> Dataset orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
		return dataset.orElseThrow(exceptionSupplier);
	}

	/** @return the suitable Number class for the gdal constant gdalconstConstants.GDT_ */
	public static Class<? extends Number> getDataType(int gdalDataType) {
		if (gdalconstConstants.GDT_Byte == gdalDataType)
			return Byte.class;
		if (gdalconstConstants.GDT_Int16 == gdalDataType)
			return Short.class;
		if (gdalconstConstants.GDT_Int32 == gdalDataType)
			return Integer.class;
		if (gdalconstConstants.GDT_Int64 == gdalDataType)
			return Long.class;
		if (gdalconstConstants.GDT_Float32 == gdalDataType)
			return Float.class;
		if (gdalconstConstants.GDT_Float64 == gdalDataType)
			return Double.class;
		return Byte.class; // default ?
	}

	/** @return the gdal constant gdalconstConstants.GDT_ suitable for the Number class */
	public <T extends Number> int getGdalDataType(Class<T> kindOfData) {
		if (Short.class == kindOfData)
			return gdalconstConstants.GDT_Int16;
		if (Integer.class == kindOfData)
			return gdalconstConstants.GDT_Int32;
		if (Long.class == kindOfData)
			return gdalconstConstants.GDT_Int32;
		if (Float.class == kindOfData)
			return gdalconstConstants.GDT_Float32;
		if (Double.class == kindOfData)
			return gdalconstConstants.GDT_Float64;
		return gdalconstConstants.GDT_Byte;
	}

	/** @return the size of a primitive */
	public <T extends Number> int getGdalDataSize(Class<T> kindOfData) {
		if (Short.class == kindOfData)
			return Short.BYTES;
		if (Integer.class == kindOfData)
			return Integer.BYTES;
		if (Long.class == kindOfData)
			return Integer.BYTES;
		if (Float.class == kindOfData)
			return Float.BYTES;
		if (Double.class == kindOfData)
			return Double.BYTES;
		return Byte.BYTES;
	}

	public static GdalDataset of(Dataset dataset) {
		return new GdalDataset(dataset);
	}

	/** Computes the checksum of the first band */
	public int checksum() {
		int result = 0;
		if (dataset.isPresent()) {
			Band band = dataset.get().GetRasterBand(1);
			if (band != null) {
				result = band.Checksum();
				band.delete();
			}
		}
		return result;
	}

	/**
	 * Fetch the no data value for the first band
	 */
	public double getNoDataValue() {
		return getBandFeatures().noDataValue();
	}

	/**
	 * Fetch the feature of the band
	 */
	public BandFeatures getBandFeatures(int bandIndex) {
		var noDataValue = new Double[1];
		var offset = new Double[1];
		var scale = new Double[1];
		var dataType = -1;
		if (dataset.isPresent()) {
			Band band = dataset.get().GetRasterBand(bandIndex);
			if (band != null) {
				band.GetNoDataValue(noDataValue);
				band.GetOffset(offset);
				band.GetScale(scale);
				dataType = band.GetRasterDataType();
				band.delete();
			}
		}
		return new BandFeatures(noDataValue[0] != null ? noDataValue[0] : Double.NaN,
				offset[0] != null ? offset[0] : 0d, scale[0] != null ? scale[0] : 1d, dataType);
	}

	/**
	 * Fetch the features of the band
	 */
	public BandFeatures getBandFeatures() {
		return getBandFeatures(1);
	}

	/**
	 * @return the spatial resolution
	 */
	public Resolution getResolution() {
		if (dataset.isPresent()) {
			double[] geoTransform = dataset.get().GetGeoTransform();
			if (geoTransform != null) {
				return new Resolution(Math.abs(geoTransform[1]), Math.abs(geoTransform[5]));
			}
		}
		return new Resolution(0d, 0d);
	}

	/**
	 * Set the feature of the band
	 */
	public void setBandFeatures(int bandIndex, BandFeatures features) {
		if (dataset.isPresent()) {
			Band band = dataset.get().GetRasterBand(bandIndex);
			if (band != null) {
				if (band.SetNoDataValue(features.noDataValue()) != GdalUtils.OK) {
					logger.warn("Failed to set NoDataValue on gdal raster {}", gdal.GetLastErrorMsg());
				}
				if (band.SetOffset(features.offset()) != GdalUtils.OK) {
					logger.warn("Failed to set the offset on gdal raster {}", gdal.GetLastErrorMsg());
				}
				if (band.SetScale(features.scale()) != GdalUtils.OK) {
					logger.warn("Failed to set the scale on gdal raster {}", gdal.GetLastErrorMsg());
				}
				band.delete();
			}
		}
	}

	/**
	 * Set the features of the first band
	 */
	public void setBandFeatures(BandFeatures features) {
		setBandFeatures(1, features);
	}

	/**
	 * Fetch or compute the min/max values for the first band
	 */
	public Optional<DoubleRange> computeMinMax() {
		DoubleRange result = null;
		if (dataset.isPresent()) {
			Band band = dataset.get().GetRasterBand(1);
			if (band != null) {
				Double[] storedmin = new Double[1];
				band.GetMinimum(storedmin);
				Double[] storedMax = new Double[1];
				band.GetMaximum(storedMax);

				if (storedmin[0] == null || storedMax[0] == null) {
					double[] min = { 0d };
					double[] max = { 0d };
					double[] mean = { 0d };
					double[] stddev = { 0d };
					if (band.ComputeStatistics(false, min, max, mean, stddev) == gdalconstConstants.CE_None) {
						gdal.ErrorReset();
						result = new DoubleRange(min[0], max[0]);
					}
				} else {
					result = new DoubleRange(storedmin[0], storedMax[0]);
				}
				band.delete();
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * Plain record to hold all statistics of a RasterBand
	 */
	public static record Statistics(double min, double max, double mean, double stddev, IntList histogram) {
	}

	/**
	 * Compute the statistics values for the first band
	 */
	public Optional<Statistics> computeStatistics(int histogramSize) {
		Statistics result = null;
		if (dataset.isPresent()) {
			Band band = dataset.get().GetRasterBand(1);
			if (band != null) {
				double[] min = { 0d };
				double[] max = { 0d };
				double[] mean = { 0d };
				double[] stddev = { 0d };
				IntList histogram = new ArrayIntList();
				if (band.ComputeStatistics(false, min, max, mean, stddev) == gdalconstConstants.CE_None) {
					if (histogramSize > 0) {
						int[] buckets = new int[histogramSize];
						if (band.GetHistogram(min[0], max[0], buckets) == gdalconstConstants.CE_None) {
							Arrays.stream(buckets).forEach(histogram::add);
						}
					}
					result = new Statistics(min[0], max[0], mean[0], stddev[0], histogram);
				}
				band.delete();
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * Apply the operation on each double value of a band. <br>
	 * The original values are overwritten with the calculated values
	 */
	public void resample(int bandIndex, BandResampler resampler) {
		if (dataset.isPresent()) {
			Band band = dataset.get().GetRasterBand(bandIndex);
			if (band != null) {
				int blockXSize = band.GetBlockXSize();
				int blockYSize = band.GetBlockYSize();
				int xBlockCount = (int) Math.ceil((double) band.getXSize() / blockXSize);
				int yBlockCount = (int) Math.ceil((double) band.getYSize() / blockYSize);
				double noDataValue = GdalUtils.getNoDataValue(band);
				var buffer = ByteBuffer
						.allocateDirect(GdalUtils.getDataTypeSize(band) * band.GetBlockXSize() * band.GetBlockYSize());
				buffer.order(ByteOrder.nativeOrder());
				int isSuccess = gdalconstConstants.CE_None;
				gdal.ErrorReset();
				for (int xOff = 0; isSuccess == gdalconstConstants.CE_None && xOff < xBlockCount; xOff++) {
					for (int yOff = 0; isSuccess == gdalconstConstants.CE_None && yOff < yBlockCount; yOff++) {
						buffer.rewind();
						isSuccess = band.ReadBlock_Direct(xOff, yOff, buffer);
						if (isSuccess == gdalconstConstants.CE_None) {
							resampler.accept(buffer, noDataValue);
							buffer.rewind();
							isSuccess = band.WriteBlock_Direct(xOff, yOff, buffer);
							if (isSuccess != gdalconstConstants.CE_None) {
								logger.warn("Block write failed : {}", gdal.GetLastErrorMsg());
							}
						} else {
							logger.warn("Block read failed : {}", gdal.GetLastErrorMsg());
						}
					}
				}
				band.FlushCache();
				band.delete();
			}
		}
	}

	/**
	 * Interface to implement for calling resample
	 */
	@FunctionalInterface
	public static interface BandResampler {
		void accept(ByteBuffer buffer, double noDataValue);
	}

	/**
	 * Create a new tiff file with the wrapped dataset.
	 */
	public GdalDataset copy(String outputFile) {
		if (isPresent()) {
			Driver driver = gdal.GetDriverByName(GdalUtils.TIF_DRIVER_NAME);
			Dataset copyDataset = driver.CreateCopy(outputFile, get());
			if (copyDataset != null) {
				copyDataset.FlushCache();
				return GdalDataset.of(copyDataset);
			}
		}
		return empty();
	}

	/**
	 * All features of a Gdal raster band
	 */
	public static record BandFeatures(double noDataValue, double offset, double scale, int gdalDatatype) {

		public Class<? extends Number> getDataType() {
			return GdalDataset.getDataType(gdalDatatype);
		}

	}

	/**
	 * Spatial resolution of the Dataset
	 */
	public static record Resolution(double xRes, double yRes) {
	}

}
