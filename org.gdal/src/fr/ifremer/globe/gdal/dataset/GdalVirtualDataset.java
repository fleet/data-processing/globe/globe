package fr.ifremer.globe.gdal.dataset;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

import org.gdal.gdal.BuildVRTOptions;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Builds a VRT from a list of datasets.
 *
 * <pre>
 *  gdalbuildvrt [-tileindex field_name]
            [-resolution {highest|lowest|average|user}]
            [-te xmin ymin xmax ymax] [-tr xres yres] [-tap]
            [-separate] [-b band]* [-sd subdataset]
            [-allow_projection_difference] [-q]
            [-addalpha] [-hidenodata]
            [-srcnodata "value [value...]"] [-vrtnodata "value [value...]"]
            [-ignore_srcmaskband]
            [-a_srs srs_def]
            [-r {nearest,bilinear,cubic,cubicspline,lanczos,average,mode}]
            [-oo NAME=VALUE]*
            [-input_file_list my_list.txt] [-overwrite]
            [-strict | -non_strict]
            output.vrt [gdalfile]*
 * </pre>
 */
public class GdalVirtualDataset {

	/** Logger */
	protected final Logger logger = LoggerFactory.getLogger(GdalVirtualDataset.class);

	/** List of options */
	private List<String> options = new LinkedList<>();

	/** Input datasets */
	private final List<Dataset> inputDatasets = new LinkedList<>();

	/** Output vrt file */
	private Optional<File> outputFile = Optional.empty();

	/**
	 * Build the VRT file
	 */
	public GdalDataset run() throws IOException {
		logger.debug("Start Gdalvrt");
		BuildVRTOptions vrtOptions = buildOptions();

		boolean createTmpVrt = outputFile.isEmpty();
		if (createTmpVrt) {
			outputFile = Optional.of(TemporaryCache.createTemporaryFile("Vrt", ".vrt"));
		}

		var result = new GdalDataset(gdal.BuildVRT(outputFile.get().getAbsolutePath(),
				inputDatasets.toArray(new Dataset[inputDatasets.size()]), vrtOptions));
		vrtOptions.delete();
		if (createTmpVrt) {
			result.deleteOnClose(true);
		}
		logger.debug("End of Gdalvrt");
		return result;
	}

	/**
	 * Builds command options.
	 */
	private BuildVRTOptions buildOptions() {
		var processOptions = new Vector<>(options);
		if (logger.isDebugEnabled()) {
			logger.debug("Vrt options : {}",
					processOptions.stream().map(op -> "'" + op.toString() + "'").collect(Collectors.joining(" ")));
		}
		return new BuildVRTOptions(processOptions);
	}

	/**
	 * Place each input file into a separate band
	 */
	public void setSeparate() {
		options.add("-separate");
	}

	/**
	 * Even if any band contains nodata value, giving this option makes the VRT band not report the NoData
	 */
	public void setHideNoData() {
		options.add("-hidenodata");
	}

	/**
	 * Select an input band to be processed
	 */
	public void setSrcNoData(Number... value) {
		options.add("-srcnodata");
		options.add(
				value != null ? Arrays.stream(value).map(String::valueOf).collect(Collectors.joining(" ")) : "None");
	}

	/**
	 * Select an input band to be processed
	 */
	public void setVrtNoData(Number... value) {
		options.add("-vrtnodata");
		options.add(
				value != null ? Arrays.stream(value).map(String::valueOf).collect(Collectors.joining(" ")) : "None");
	}

	/**
	 * @param outputFile the {@link #outputFile} to set
	 */
	public void setOutputFile(File outputFile) {
		this.outputFile = Optional.of(outputFile);
	}

	/**
	 * @param outputFile the {@link #outputFile} to set
	 */
	public void addDataset(Dataset dataset) {
		inputDatasets.add(dataset);
	}

}
