package fr.ifremer.globe.gdal;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.gdal.gdal.Driver;
import org.gdal.gdal.gdal;
import org.gdal.ogr.ogr;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Apside
 *
 */
public class Activator implements BundleActivator {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(Activator.class);

	/**
	 *
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		ServiceReference<?> envInfoSR = context.getServiceReference(EnvironmentInfo.class.getName());
		if (envInfoSR == null) {
			throw new IOException("Platform architecture not supported by GDAL");
		}

		// try-catch gdal library loading to avoid exceptions while running SoundingFileConverter on Linux
		try {
			//
			Bundle bundle = context.getBundle();
			String bundleFolder = FilenameUtils.normalize(FileLocator.getBundleFile(bundle).getAbsolutePath());
			copyProjLib(new File(bundleFolder));

			GdalLibLoader.loadNativeLibrary();

			gdal.SetConfigOption("CPL_LOG_ERRORS", "OFF");
			gdal.DontUseExceptions();

			gdal.SetConfigOption("GDAL_DATA", new File(bundleFolder, "data").getAbsolutePath());

			gdal.AllRegister();

			// Remove XYZ driver
			Driver xyzDriver = gdal.GetDriverByName("XYZ");
			if (xyzDriver != null) {
				xyzDriver.Deregister();
				xyzDriver.delete();
			}

			ogr.DontUseExceptions();
			ogr.RegisterAll();

		} catch (UnsatisfiedLinkError e) {
			logger.error("Error in Gdal activator : " + e.getMessage(), e);
		}
	}

	/** Ensure Proj4 shared files are avalaible */
	protected void copyProjLib(File pluginFolder) {

		// PROJ_LIB environment variable may point to the proj4 shared file
		String projLibEnvVar = System.getenv("PROJ_LIB");
		if (projLibEnvVar != null) {
			File sharedProjDbFile = new File(projLibEnvVar, "proj.db");
			if (sharedProjDbFile.exists()) {
				logger.debug("Proj4 will use share file {}", sharedProjDbFile.getPath());
				return;
			}
		}

		// Proj4 proj.db already present in the local share folder ?
		File projShareFolder = new File(pluginFolder, "../share/proj");
		File sharedProjDbFile = new File(projShareFolder, "proj.db");
		if (sharedProjDbFile.exists()) {
			logger.debug("Proj4 will use share file {}", sharedProjDbFile.getPath());
			return;
		}

		// Copy proj4 files to the local share folder
		File pluginShareFolder = new File(pluginFolder, "proj");
		if (pluginShareFolder.exists()) {
			projShareFolder.mkdirs();
			try {
				FileUtils.copyDirectory(pluginShareFolder, projShareFolder);
				logger.debug("Proj4 will use share file {}", sharedProjDbFile.getPath());
			} catch (IOException e) {
				logger.error("Unable to copy proj directory", e);
			}
		} else {
			logger.warn("No share files found in the plugin ({}). Configuration of proj4 is aborted",
					pluginShareFolder.getPath());
		}
	}

	/**
	 *
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		// Nothing to do
	}

}