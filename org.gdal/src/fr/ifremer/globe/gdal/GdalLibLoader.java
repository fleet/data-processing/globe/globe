package fr.ifremer.globe.gdal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.gdal.gdal.gdal;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GdalLibLoader {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(Activator.class);

	protected static final AtomicBoolean isLoaded = new AtomicBoolean();

	/**
	 * Constructor
	 */
	private GdalLibLoader() {
	}

	/**
	 * Loads native libraries (gdal, hdf5, netcdf...)
	 */
	public static void loadNativeLibrary() throws IOException {

		if (!isLoaded.compareAndSet(false, true))
			return;

		String pluginId = "org.gdal." + Platform.getOS() + "." + Platform.getOSArch();
		Bundle bundle = Platform.getBundle(pluginId);
		if (bundle == null)
			throw new NullPointerException(
					"Error during gdal native lib loading: bundle not retrieved (pluginId: " + pluginId + ")");

		logger.debug("Loading GDAL library from {}", pluginId);

		// get library to load
		List<String> librariesToLoad = readTextFile(bundle, "libraryToLoad.txt");
		File bundleFolder = FileLocator.getBundleFile(bundle);

		for (String libname : librariesToLoad) {
			logger.debug("Loading lib : {}", libname);
			try {
				System.load(new File(bundleFolder, libname).getAbsolutePath());
			} catch (UnsatisfiedLinkError e) {
				logger.error("Error while loading lib '{}' : {}", libname, e.getMessage(), e);
			}
		}

		String gdalPath = FilenameUtils
				.normalize(FileLocator.getBundleFile(Platform.getBundle(pluginId)).getAbsolutePath());
		gdal.SetConfigOption("GDAL_DRIVER_PATH", new File(gdalPath, "plugins").getAbsolutePath());
		// avoid creation of .xml.aux files to store stats
		gdal.SetConfigOption("GDAL_PAM_ENABLED" , "FALSE");
	}

	/**
	 * @return the content of a file in a bundle.
	 */
	private static List<String> readTextFile(Bundle bundle, String filename) throws IOException {
		URL urlToFile = bundle.getEntry(filename);

		try (InputStream stream = urlToFile.openStream()) {
			return IOUtils.readLines(stream, StandardCharsets.UTF_8);
		}
	}

}
