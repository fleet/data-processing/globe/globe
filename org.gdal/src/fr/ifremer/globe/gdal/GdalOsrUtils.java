/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.gdal;

import org.gdal.ogr.Geometry;
import org.gdal.ogr.ogrConstants;
import org.gdal.osr.SpatialReference;
import org.gdal.osr.osrConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * OSR utilities
 */
public class GdalOsrUtils {
	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(GdalOsrUtils.class);

	/** WGS84 SpatialReference */
	public static final SpatialReference SRS_WGS84 = new SpatialReference() {

		/** {@inheritDoc} */
		@Override
		public synchronized void delete() {
			// Refuse to delete this shared instance
		}
	};

	static {
		SRS_WGS84.ImportFromProj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
	}

	/** Constructor */
	private GdalOsrUtils() {
	}

	/**
	 * Tranform a bounding box to SRS_WKT_WGS84
	 *
	 * @param inSrs original SpatialReference
	 * @param boundingBox North/up/ymax, South/down/ymin, East/right/xmax, West/left/xmin
	 * @return boundingBox (North/up/ymax, South/down/ymin, East/right/xmax, West/left/xmin)
	 */
	public static void tranformBoundingBoxToWGS84(SpatialReference inSrs, double[] boundingBox) {
		Geometry line = new Geometry(ogrConstants.wkbLinearRing);
		line.AddPoint(boundingBox[3], boundingBox[0]); // upperLeft
		line.AddPoint(boundingBox[3], boundingBox[1]); // lowerLeft
		line.AddPoint(boundingBox[2], boundingBox[1]); // lowerRight
		line.AddPoint(boundingBox[2], boundingBox[0]); // upperRight
		line.CloseRings();

		line.AssignSpatialReference(inSrs);
		line.TransformTo(SRS_WGS84);

		double[] envelope = new double[4];
		line.GetEnvelope(envelope);
		line.delete();

		if (!isTransformationSwapCoords(inSrs, SRS_WGS84)) {
			boundingBox[0] = envelope[3]; // North
			boundingBox[1] = envelope[2]; // South
			boundingBox[2] = envelope[1]; // East
			boundingBox[3] = envelope[0]; // West
		} else {
			boundingBox[0] = envelope[1]; // North
			boundingBox[1] = envelope[0]; // South
			boundingBox[2] = envelope[3]; // East
			boundingBox[3] = envelope[2]; // West
		}
	}

	/**
	 * Return True when calling TransformTo with (x,y) or (longitude,latitude) in parameter may return a point with
	 * [0=latitude, 1=longitude] rather than [0=longitude, 1=latitude]
	 */
	public static boolean isTransformationSwapCoords(SpatialReference fromSrs, SpatialReference toSrs) {
		boolean fromIsTraditionalGisOrder = isTraditionalGisOrder(fromSrs);
		boolean toIsTraditionalGisOrder = isTraditionalGisOrder(toSrs);
		return (fromSrs.IsProjected() == 1 && toSrs.IsGeographic() == 1 && toIsTraditionalGisOrder)
				|| (fromIsTraditionalGisOrder != toIsTraditionalGisOrder);
	}

	/**
	 * @return true when for geographic CRS with lat/long order, the data will still be long/lat ordered
	 */
	public static boolean isTraditionalGisOrder(SpatialReference spatialReference) {
		if (spatialReference.IsProjected() == 1) {
			return false;
		}
		int axisMappingStrategy = spatialReference.GetAxisMappingStrategy();
		if (axisMappingStrategy == osrConstants.OAMS_TRADITIONAL_GIS_ORDER) {
			return true;
		}
		if (axisMappingStrategy == osrConstants.OAMS_CUSTOM) {
			return false;
		}

		// Case of OAMS_AUTHORITY_COMPLIANT. True if first axis is Latitude
		int firstAxisOrientation = spatialReference.GetAxisOrientation(null /* search for an authority node */, 0);
		return firstAxisOrientation == osrConstants.OAO_North || firstAxisOrientation == osrConstants.OAO_South;
	}

}
