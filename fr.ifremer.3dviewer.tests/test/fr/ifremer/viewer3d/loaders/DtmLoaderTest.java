package fr.ifremer.viewer3d.loaders;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class DtmLoaderTest {

	private static final String TEST_FILE_DTM_1 = GlobeTestUtil.getTestDataPath() + "/atalanteEM710/accGlobeAT10M47N4W.mnt";

	@Test
	public void testAccept() {

		ElevationLoader driver = new ElevationLoader();

		Assert.assertFalse(driver.accept((String) null));

		Assert.assertFalse(driver.accept("fichierInexistant.mnt"));

		Assert.assertTrue(driver.accept(TEST_FILE_DTM_1));
	}

	@Test
	public void testInitData() {

		// ElevationLoader driver = new ElevationLoader();
		//
		// long beginTime = System.currentTimeMillis();
		// IInfoStore store = driver.initData(TEST_FILE_DTM_1);
		// long endTime = System.currentTimeMillis();
		//
		// Assert.assertTrue(store instanceof DtmStore);
		// Assert.assertTrue(((DtmStore) store).getNbLines() > 0);
		//
		// System.out.println("Dtm read metadata: " + (endTime - beginTime));
	}
}
