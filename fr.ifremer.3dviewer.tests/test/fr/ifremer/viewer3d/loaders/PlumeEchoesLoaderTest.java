package fr.ifremer.viewer3d.loaders;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;
import fr.ifremer.viewer3d.util.XSDValidator;

/**
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class PlumeEchoesLoaderTest {

	/** {@value} */
	private static final String ECHOES_V1_TESTFILE01 = GlobeTestUtil.getTestDataPath() + "/WC/Marmesonet/Echoes/0306_Echoes.xml";
	/** {@value} */
	private static final String ECHOES_V1_TESTFILE02 = GlobeTestUtil.getTestDataPath() + "/WC/Marmesonet/Echoes/0307_Echoes.xml";

	@Test
	public void testFormatV1_01() throws FileNotFoundException {
		String filePath = ECHOES_V1_TESTFILE01;
		String schemaPath = "../fr.ifremer.3dviewer/etc/" + PlumeEchoesLoader.SCHEMA_FILENAME;
		boolean valid = XSDValidator.valid(new FileInputStream(filePath), new FileInputStream(schemaPath));

		Assert.assertTrue(valid);
	}

	@Test
	public void testFormatV1_02() throws FileNotFoundException {
		String filePath = ECHOES_V1_TESTFILE02;
		String schemaPath = "../fr.ifremer.3dviewer/etc/" + PlumeEchoesLoader.SCHEMA_FILENAME;
		boolean valid = XSDValidator.valid(new FileInputStream(filePath), new FileInputStream(schemaPath));

		Assert.assertTrue(valid);
	}
}
