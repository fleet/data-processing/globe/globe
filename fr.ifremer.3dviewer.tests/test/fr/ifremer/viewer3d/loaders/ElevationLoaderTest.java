package fr.ifremer.viewer3d.loaders;

import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public class ElevationLoaderTest {

	/** {@value} */
	private static final String TESTFILE01 = "/Terrain/Marmara/V2.2/marmara_50-N40-v2-bornes_newFmt.mnt";
	/** {@value} */
	private static final String TESTFILE02 = "/Terrain/Marmara/V2.2/marmara_50-N40-v2-bornes_newFmt.dtm";

	/** Fichier {@link #TESTFILE01} */
	@Test
	public void test01Dtm() throws FileNotFoundException {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + TESTFILE01;

		ElevationLoader el = new ElevationLoader();

		Assert.assertTrue(el.accept(filePath));

//		LayerStore ls = null;
//		try {
//			ls = el.initData(filePath, new NullProgressMonitor());
//		} catch (GIOException e) {
//			e.printStackTrace();
//		}

//		Assert.assertNotNull(ls);
	}

	/** Fichier {@link #TESTFILE02} */
	@Test
	public void test02DTM() throws FileNotFoundException {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + TESTFILE02;

		ElevationLoader el = new ElevationLoader();

		Assert.assertTrue(el.accept(filePath));
//
//		LayerStore ls = null;
//		try {
//			ls = el.initData(filePath, new NullProgressMonitor());
//		} catch (GIOException e) {
//			e.printStackTrace();
//		}
//
//		Assert.assertNotNull(ls);
	}
}
