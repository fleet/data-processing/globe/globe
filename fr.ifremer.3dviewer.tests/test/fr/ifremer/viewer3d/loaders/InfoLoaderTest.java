//package fr.ifremer.viewer3d.loaders;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Assert;
//import org.junit.Test;
//
//import com.thoughtworks.xstream.XStream;
//import com.thoughtworks.xstream.mapper.CannotResolveClassException;
//
//import fr.ifremer.globe.model.ANavLine;
//import fr.ifremer.globe.model.utils.Pair;
//import fr.ifremer.globe.utils.test.GlobeTestUtil;
//import fr.ifremer.viewer3d.layers.ReflectivityLayer;
//import fr.ifremer.viewer3d.model.DepthNavLine;
//import fr.ifremer.viewer3d.model.info.Dimensions;
//import fr.ifremer.viewer3d.model.info.Info;
//import fr.ifremer.viewer3d.model.info.Item;
//import fr.ifremer.viewer3d.model.info.StorageType;
//import fr.ifremer.viewer3d.util.PrivateAccessor;
//import gov.nasa.worldwind.globes.ElevationModel;
//import gov.nasa.worldwind.layers.Layer;
//
///**
// * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
// * 
// */
//public class InfoLoaderTest {
//
//	/** Sismique le long de la navigation. */
//	private static final String SUBBOTTOM_TESTFILE = GlobeTestUtil.getTestDataPath() + "/Subbottom/AUV0001_D20091123_T050935_process.xml";
//	private static final String WC_ALONG_NAV_TESTFILE = GlobeTestUtil.getTestDataPath() + "/WC/Marmesonet/Echogrammes Longitudinaux/0306_20091113_160950_Lesuroit_WCSliceComp.xml";
//	private static final String WC_ALONG_NAV_IMGPATH = GlobeTestUtil.getTestDataPath() + "/WC/Marmesonet/Echogrammes Longitudinaux/0306_20091113_160950_Lesuroit_WCSliceComp/";
//	private static final String WC_V1_TESTFILE = "/WC/Marmesonet/Echoes/0306_Echoes.xml";
//
//	@Test
//	public void testAcceptSubbottom() {
//		String filePath = SUBBOTTOM_TESTFILE;
//
//		InfoLoader il = new InfoLoader();
//		boolean value = il.accept(filePath);
//
//		Assert.assertTrue(value);
//
//		filePath = WC_V1_TESTFILE;
//		value = il.accept(filePath);
//
//		Assert.assertFalse(value);
//
//	}
//
//	@Test
//	public void testXMLSubbottom() throws CannotResolveClassException, IOException {
//		String filePath = SUBBOTTOM_TESTFILE;
//		File file = new File(filePath);
//
//		InfoLoader il = new InfoLoader();
//		Object value = il.readXmlFile(file);
//
//		Assert.assertTrue(value instanceof Info);
//		Info info = (Info) value;
//		Assert.assertEquals("SonarScope", info.getSignature1());
//		Assert.assertNotNull(info.getDimensions());
//		Assert.assertEquals(1763, info.getDimensions().getNbPings());
//		Assert.assertEquals(9, info.getSignals().getItems().size());
//		Assert.assertEquals(StorageType.SINGLE, info.getSignals().getItems().get(0).getStorage());
//		Assert.assertEquals(StorageType.DOUBLE, info.getSignals().getItems().get(3).getStorage());
//		Assert.assertEquals(2, info.getImages().getItems().size());
//	}
//
//	@Test
//	public void testSubbottom() throws Exception {
//		String filePath = SUBBOTTOM_TESTFILE;
//		File file = new File(filePath);
//
//		InfoLoader il = new InfoLoader();
//		List<Pair<Layer, ElevationModel>> value = il.load(file, null);
//
//		Assert.assertNotNull(value);
//		Assert.assertEquals(1, value.size());
//		Assert.assertNull(value.get(0).getSecond());
//		Assert.assertTrue(value.get(0).getFirst() instanceof ReflectivityLayer);
//		ReflectivityLayer layer = (ReflectivityLayer) value.get(0).getFirst();
//		Assert.assertNotNull(PrivateAccessor.getField(layer, "texturedWall"));
//		Assert.assertEquals(1, layer.getData().getLines().size());
//		ANavLine line = layer.getData().getLines().get(0);
//		Assert.assertEquals(1763, line.size());
//		Assert.assertTrue(line instanceof DepthNavLine);
//		Assert.assertEquals(1763, ((DepthNavLine) line).getDepths().size());
//	}
//
//	@Test
//	public void testXMLWCAlongNav() throws CannotResolveClassException, IOException {
//		String filePath = WC_ALONG_NAV_TESTFILE;
//		File file = new File(filePath);
//
//		InfoLoader il = new InfoLoader();
//		Object value = il.readXmlFile(file);
//
//		Assert.assertTrue(value instanceof Info);
//		Info info = (Info) value;
//		Assert.assertEquals("SonarScope", info.getSignature1());
//		Assert.assertNotNull(info.getDimensions());
//		Assert.assertEquals(1394, info.getDimensions().getNbPings());
//		Assert.assertEquals(9, info.getSignals().getItems().size());
//		Assert.assertEquals(StorageType.SINGLE, info.getSignals().getItems().get(0).getStorage());
//		Assert.assertEquals(StorageType.DOUBLE, info.getSignals().getItems().get(3).getStorage());
//		Assert.assertEquals(2, info.getImages().getItems().size());
//	}
//
//	@Test
//	public void testWCAlongNav() throws Exception {
//		String filePath = WC_ALONG_NAV_TESTFILE;
//		String imgPath = WC_ALONG_NAV_IMGPATH;
//		File file = new File(filePath);
//
//		InfoLoader il = new InfoLoader();
//		List<Pair<Layer, ElevationModel>> value = il.load(file, null);
//
//		Assert.assertNotNull(value);
//		Assert.assertEquals(1, value.size());
//		Assert.assertNull(value.get(0).getSecond());
//		Assert.assertTrue(value.get(0).getFirst() instanceof ReflectivityLayer);
//		ReflectivityLayer layer = (ReflectivityLayer) value.get(0).getFirst();
//		Assert.assertEquals(new File(imgPath), new File((String) PrivateAccessor.getField(layer, "imagesPath")));
//		Assert.assertNotNull(PrivateAccessor.getField(layer, "texturedWall"));
//	}
//
//	/**
//	 * Test de debug pour afficher le XML généré.
//	 */
//	@Test
//	public void testSaveXML() {
//		XStream xstream = new XStream();
//		xstream.autodetectAnnotations(true);
//
//		Info info = new Info();
//		PrivateAccessor.setField(info, "signature1", "SonarScope");
//		Dimensions dim = new Dimensions();
//		PrivateAccessor.setField(dim, "nbPings", 123);
//		PrivateAccessor.setField(info, "dimensions", dim);
//		List<Item> list = new ArrayList<Item>();
//		Item s = new Item();
//		PrivateAccessor.setField(s, "name", "toto");
//		list.add(s);
//		s = new Item();
//		PrivateAccessor.setField(s, "name", "autre");
//		list.add(s);
//		// PrivateAccessor.setField(info, "signals", list);
//
//		String xml = xstream.toXML(info);
//		System.out.println(xml);
//	}
//
//}
