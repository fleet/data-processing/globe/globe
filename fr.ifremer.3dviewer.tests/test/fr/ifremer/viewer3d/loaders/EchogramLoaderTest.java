package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.test.GlobeTestUtil;
import fr.ifremer.viewer3d.layers.xml.WaterColumnLayer;
import fr.ifremer.viewer3d.util.XSDValidator;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 * 
 */
public class EchogramLoaderTest {

	/** {@value} */
	private static final String WC_V1_TESTFILE01 = "/WC/Marmesonet/Echogrammes/0306_PolarEchograms.xml";
	/** {@value} */
	private static final String WC_V1_TESTFILE02 = "/WC/Marmesonet/Echogrammes/0307_PolarEchograms.xml";

	private Logger logger = LoggerFactory.getLogger(EchogramLoaderTest.class);

	/*
	 * NB : les données de colonne d'eau le long de la navigation sont chargées avec InfoLoader.
	 */

	/**
	 * Fichier {@link #WC_V1_TESTFILE01}
	 * 
	 * @throws IOException
	 */
	@Test
	public void testFormatV1XML01() throws IOException {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + WC_V1_TESTFILE01;
		String schemaPath = "../fr.ifremer.3dviewer/etc/" + EchogramLoader.SCHEMA_FILENAME;
		logger.info(filePath + " => " + schemaPath);
		boolean valid = XSDValidator.valid(new FileInputStream(filePath), new FileInputStream(schemaPath));

		Assert.assertTrue(valid);
	}

	/** Fichier {@link #WC_V1_TESTFILE01} */
	@Test
	public void testFormatV1Bin01() throws Exception {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + WC_V1_TESTFILE01;
		EchogramLoader el = new EchogramLoader();
		List<Pair<Layer, ElevationModel>> result = el.load(new File(filePath), new NullProgressMonitor());

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertNull(result.get(0).getSecond());
		Assert.assertTrue(result.get(0).getFirst() instanceof WaterColumnLayer);
		WaterColumnLayer layer = (WaterColumnLayer) result.get(0).getFirst();
		Assert.assertEquals(1394, layer.getDatas().getDataCount());
	}

	/** Fichier {@link #WC_V1_TESTFILE02} */
	@Test
	public void testFormatV1XML02() throws FileNotFoundException {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + WC_V1_TESTFILE02;
		String schemaPath = "../fr.ifremer.3dviewer/etc/" + EchogramLoader.SCHEMA_FILENAME;
		boolean valid = XSDValidator.valid(new FileInputStream(filePath), new FileInputStream(schemaPath));

		Assert.assertTrue(valid);
	}

	/** Fichier {@link #WC_V1_TESTFILE02} */
	@Test
	public void testFormatV1Bin02() throws Exception {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + WC_V1_TESTFILE02;
		EchogramLoader el = new EchogramLoader();
		List<Pair<Layer, ElevationModel>> result = el.load(new File(filePath), new NullProgressMonitor());

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertNull(result.get(0).getSecond());
		Assert.assertTrue(result.get(0).getFirst() instanceof WaterColumnLayer);
		WaterColumnLayer layer = (WaterColumnLayer) result.get(0).getFirst();
		Assert.assertEquals(1454, layer.getDatas().getDataCount());
	}

	// @Test
	// public void testWCImmersion() throws Exception {
	// String dataPath = GlobeTestUtil.getTestDataPath();
	// String filePath = dataPath + WC_V2_TESTFILE;
	// String schemaPath = "etc/" + EchogramLoader.SCHEMA_FILENAME;
	// boolean valid = XSDValidator.valid(new FileInputStream(filePath),
	// new FileInputStream(schemaPath));
	//
	// Assert.assertTrue(valid);
	// }
}
