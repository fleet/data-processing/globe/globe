package fr.ifremer.viewer3d.loaders;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.utils.test.GlobeTestUtil;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTiledImageLayer;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * @author G.Bourel &lt;guillaume.bourel@altran.com&gt;
 * 
 */
public class XMLLoaderTest {
	/** {@value} */
	private static final String TERRAIN_TESTFILE01 = "/Terrain/Marmara/V1.1/Dive6789_elevation.xml";
	/** {@value} */
	private static final String TERRAIN_TESTFILE02 = "/Terrain/Marmara/V1.1/Dive6789_SunShading_2.xml";
	/** {@value} */
	private static final String TERRAIN_TESTFILE03 = "/Terrain/Marmara/V1.1/Marmara_complete_elevation_2.xml";
	/** {@value} */
	private static final String TERRAIN_TESTFILE04 = "/Terrain/Marmara/V1.1/Marmara_sun_shading_2.xml";

	/** Fichier {@link #WC_V1_TESTFILE01} */
	@Test
	public void testTerrain01() throws Exception {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + TERRAIN_TESTFILE01;
		XMLLoader loader = new XMLLoader();
		List<Pair<Layer, ElevationModel>> result = loader.load(new File(filePath), new NullProgressMonitor());

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertNotNull(result.get(0).getSecond());
		Assert.assertTrue(result.get(0).getFirst() instanceof MyTiledImageLayer);
		MyTiledImageLayer layer = (MyTiledImageLayer) (result.get(0).getFirst());
		Assert.assertEquals("Dive6789_elevation", layer.getLevels().getLevel(0).getCacheName());
		Assert.assertTrue(result.get(0).getSecond() instanceof ElevationModel);
	}

	/** Fichier {@link #WC_V1_TESTFILE02} */
	@Test
	public void testTerrain02() throws Exception {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + TERRAIN_TESTFILE02;
		XMLLoader loader = new XMLLoader();
		List<Pair<Layer, ElevationModel>> result = loader.load(new File(filePath), new NullProgressMonitor());

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertNull(result.get(0).getSecond());
		Assert.assertTrue(result.get(0).getFirst() instanceof MyTiledImageLayer);
	}

	/** Fichier {@link #WC_V1_TESTFILE03} */
	@Test
	public void testTerrain03() throws Exception {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + TERRAIN_TESTFILE03;
		XMLLoader loader = new XMLLoader();
		List<Pair<Layer, ElevationModel>> result = loader.load(new File(filePath), new NullProgressMonitor());

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertNotNull(result.get(0).getSecond());
		Assert.assertTrue(result.get(0).getFirst() instanceof MyTiledImageLayer);
		Assert.assertTrue(result.get(0).getSecond() instanceof ElevationModel);
	}

	/** Fichier {@link #WC_V1_TESTFILE04} */
	@Test
	public void testTerrain04() throws Exception {
		String dataPath = GlobeTestUtil.getTestDataPath();
		String filePath = dataPath + TERRAIN_TESTFILE04;
		XMLLoader loader = new XMLLoader();
		List<Pair<Layer, ElevationModel>> result = loader.load(new File(filePath), new NullProgressMonitor());

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertNull(result.get(0).getSecond());
		Assert.assertTrue(result.get(0).getFirst() instanceof MyTiledImageLayer);
	}
}
