package fr.ifremer.viewer3d.model;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.utils.perfcounter.PerfoCounter;

public class IndexedPlayerTest {

	@Test
	public void test() throws InterruptedException {

		int start = 1;
		int end = 10;
		IndexedPlayer p = new IndexedPlayer("test", start, end);
		ArrayList<Integer> indexes = new ArrayList<>();
		p.getIndexPublisher().subscribe(index -> {
			indexes.add(index.currentValue());
		});
		p.setCurrent(p.getCurrent());
		PerfoCounter counter = new PerfoCounter("Double speed");
		counter.start();

		p.playPause();
		waitEndOfReading(p);
		System.err.println(counter.stop());
		Assert.assertEquals(end - start, indexes.size()); // we expect to find 9 values, since the first one is the
															// default one
		for (int i = 0; i < indexes.size(); i++) {
			int value = indexes.get(i);
			Assert.assertEquals(i + start + 1, value); // we are not notified for the first value
		}
		indexes.clear();
		p.setCurrent(5);
		Assert.assertEquals(5, (int) indexes.get(0));
		// test backward
		indexes.clear();
		p.stepBackward();
		Assert.assertEquals(4, (int) indexes.get(0));
		// test forward
		indexes.clear();
		p.stepForward();
		Assert.assertEquals(5, (int) indexes.get(0));

		// test backward reading
		p.setCurrent(p.getMaxPlayIndex());
		indexes.clear();
		p.revertPlayPause();
		waitEndOfReading(p);
		Assert.assertEquals(end - start, indexes.size()); // we expect to find 9 values, since the first one is the
															// default one
		for (int i = 0; i < indexes.size(); i++) {
			int value = indexes.get(i);
			Assert.assertEquals(p.getMaxPlayIndex() - 1 - i, value); // we are not notified for the first value
		}

		// check with double speed if we do not miss anything
		counter = new PerfoCounter("speed x4");
		counter.start();
		p.setCurrent(p.getMinPlayIndex());
		p.setPlayingSpeed(4);
		indexes.clear();
		p.playPause();
		waitEndOfReading(p);
		System.err.println(counter.stop());
		Assert.assertEquals(end - start, indexes.size()); // we expect to find 9 values, since the first one is the
															// default one
		for (int i = 0; i < indexes.size(); i++) {
			int value = indexes.get(i);
			Assert.assertEquals(i + start + 1, value); // we are not notified for the first value
		}

		p.setMinPlayIndex(5);
		p.setCurrent(5);
		p.setMaxPlayIndex(7);
		indexes.clear();
		p.playPause();
		waitEndOfReading(p);
		// check start and stop
		Assert.assertEquals(2, indexes.size());
		Assert.assertEquals(6, (int) indexes.get(0));
		Assert.assertEquals(7, (int) indexes.get(1));

		// stop after reading 2 times
		p.setLoop(true);
		p.setMaxPlayIndex(p.getData().maxLimit());
		p.setMinPlayIndex(p.getData().minLimit());
	}

	private void waitEndOfReading(IndexedPlayer p) throws InterruptedException {

		while (p.getStatus().isPlaying()) {
			Thread.sleep(100);
		}
	}
}
