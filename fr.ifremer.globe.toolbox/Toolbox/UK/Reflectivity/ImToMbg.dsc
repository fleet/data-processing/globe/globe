# ***************************************************************************
# 
#  TRT : InToMbg
#  DOM : Imagerie 
#  ROL : Importation des échantillons de réflectivité dans le Mbg
#
#  CRE : 07/06/2019
#  AUT : MPC
#  VER : @(#)1.0
#        @(#)07/06/2019
# ***************************************************************************
#  
#  HST : 07/06/2019 MPC  Creation
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_MulImageComp.png}
		HELP		{Import reflectivity data (.IM,.im) in MBG bathymetry files}
		IN		{BeamFile	FILEGEN	mbg}
		IN		{ImageName	FILEGEN	IM}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{ImToMbgPanel 000 000 800 180 "Parameters for import reflectivity data (.IM,.im) in MBG bathymetry file"
	 
	PANEL	{InterfacesPanel	000 000 800 610 "Parameters for import reflectivity data (.IM,.im) in MBG bathymetry file"

		LABEL	 {Label		000 020 230 026 "" "Image file names (.IM)"}
		FILEGEN	 {ImageName	240 020 340 026 "Image file names (.IM)" IM} 
		
		LABEL	 {Label        	000 060 230 026 "" "Index file directory (.im)"}
	        FILEGEN  {IdxDirName	240 060 340 026 "index files directory (.im) if they aren't in .IM directory (mandatory for Windows version)" im Lock}
	        LABEL	 {Label        	590 060 200 026 "" "(Mandatory for Windows version)"}
	        
		LABEL	 {Label		000 090 230 026 "" "Bathymetry file names (.mbg)"}
		FILEGEN	 {BeamFile	240 090 340 026 "Bathymetry file names (.mbg)" mbg}

     	}
    }

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of ImToMbg' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "ImToMbg" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of ImToMbg' >> ${PROGRESS_FILE}
	}
