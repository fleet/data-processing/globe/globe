# ***************************************************************************
# 
#  TRT : Coratt 
#  DOM : Bathymetrie 
#  ROL : Correction de biais sur les sondes d'un fichier multifaisceaux
# 
#  CRE : 31/03/99
#  AUT : CEDY
#  VER : @(#)1.16
#        @(#)09/05/27
# ***************************************************************************
#  
#  HST : 31/03/99 CEDY MOD-072: Creation
#        01/12/99 CEDY ANO-412: Modifications de labels
#        01/02/01 JMS  MOD-481: Traitement des biais en fonction du temps
#        06/04/01 CEDY ANO-618: Type de biais
#        02/07/03 CE   MOD-632: Ajout de modification d'immersion
#        29/10/03 CE   MOD-635: Format de fichier de biais
#        03/03/04 CE   MOD-641: Biais de temps
#        29/09/04 CE   ANO-859: Fichiers avec nom generique en sortie
#        19/08/05 CE   Immersion = immersion absolue
#	 	 01/10/05 CE   MOD-698: Selection de faisceaux
#        29/11/05 JMS  MOD-706: Evolution des formats des fichiers de biais
#						utilisation de la classe CIB_DOU_ChronoTableFile
#        29/12/05 CE   MOD-696: Choix correction par biais ou valeur absolue
#        11/03/09 CE   MOD-804: Selection de faisceaux par Phase/Amplitude
#        17/06/10 CE   MOD-877: Correction artefact roulis ME70
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_BathyComp.png}
		HELP		{Correction of bathymetry with attitude bias}
		IN		{BeamFileIn	FILEGEN		mul}
		OUT		{BeamFileOut	FILEMANY	mul}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{CorattPanel	000 000 600 520 "Parameters for correction of bathymetry with attitude bias"
	
	LABEL	{Label		000 000 235 026 "" "Caraibes Bathymetry file to correct"}
	FILEGEN	{BeamFileIn	240 000 340 026 "Multibeam file name in CARAIBES format at input" mul}

	LABEL		{Label		000 040 230 026 "" "Caraibes Bathymetry file result"}
	FILEMANY	{BeamFileOut	240 040 340 026 "Multibeam file name in CARAIBES format at output" LockGen}

	LABEL	{Label		000 080 230 026 "" "Data from ping header to correct"}
	OPTION	{BiasType	240 080 180 026 "Data from ping header to correct"
			HEADING		{"Heading"		PanelBias{} PanelType{} PanelDegrees{}}
			ROLL		{"Roll"			PanelBias{} PanelType{} PanelDegrees{}}
			PITCH		{"Pitch"		PanelBias{} PanelType{} PanelDegrees{}}
			HEAVE		{"Heave"		PanelBias{} PanelType{} PanelMeters{}}
		 	IMMERSION	{"Immersion"	PanelBias{} PanelType{} PanelMeters{}}
		 	TIME		{"Time"			PanelBiasOnly{} PanelType{} PanelSeconds{}}
		 	ROLL_ME70	{"ME70 roll"    PanelMisAlignment{}}
		 	PITCH_FI	{"AUV bias inter-profiles" PanelLevelArm{}}
			VEL			{"Velocity" PanelBiasOnly{} PanelType{} PanelMetresSecond{}}}

	PANEL 	{PanelBias	000 120 420 030 ""

		LABEL	{Label		000 000 230 026 "" "Data correction by"}
		OPTION	{CorrectionType	240 000 180 026 "Correction by bias or absolute value"
		 		Bias		{"Bias"}
		 		AbsoluteValue	{"Absolute value"}}
		}

	PANEL 	{PanelBiasOnly	000 120 420 030 ""

		LABEL	{Label	000 000 230 026 "" "Correction input by"}
		OPTION	{Option	240 000 180 026 "Only constant bias"
		 		Bias		{"Bias"}}
		}

	PANEL 	{PanelType	000 160 580 070 ""

		LABEL	{Label		000 000 230 026 "" "Correction input by"}
		OPTION	{BiasTypeOption	240 000 180 026 "Constant bias or f(t)"
				Value	{"Constant value"		ValuePanel{}}
				File	{"Value from file = f(t)"	FilePanel{}}
			}

		PANEL 	{ValuePanel     040 040 400 030 ""

			LABEL	{Label		000 000 190 026 "" "Constant value for correction"}
			NUMBER	{ConstantValue	200 000 060 026 "Constant value for correction"}

			PANEL 	{PanelDegrees	270 000 080 030 ""
				LABEL	{Label	000 000 080 026 "" "degrees"}
				}

			PANEL 	{PanelMeters	270 000 080 030 ""
				LABEL	{Label	000 000 080 026 "" "meters"}
				}

			PANEL 	{PanelSeconds	270 000 080 030 ""
				LABEL	{Label	000 000 080 026 "" "seconds"}
				}
			
			PANEL 	{PanelMetresSecond	270 000 080 030 ""
				LABEL	{Label	000 000 080 026 "" "m/s"}
				}
			}

		PANEL 	{FilePanel      040 040 560 030 ""

			LABEL	{Label		000 000 190 026 "" "Corrections file f(t)"}
			FILE	{CorrectionFile	200 000 340 026 "Corrections file f(t)"}
			}
		}

	PANEL 	{PanelMisAlignment	000 120 420 030 ""

		LABEL	{Label		000 000 230 026 "" "Alignment correction of the VRU"}
		NUMBER	{MisAlignment	240 000 060 026 "Alignment correction of the VRU"}
		LABEL	{Label		310 000 060 026 "" "Degrees"}
		}

	PANEL 	{PanelLevelArm	000 120 420 030 ""

		LABEL	{Label		000 000 230 026 "" "Level arm"}
		NUMBER	{LevelArm	240 000 060 026 "Level arm"}
		LABEL	{Label		310 000 060 026 "" "m"}
		}

	PERIOD_SELECTION	{PeriodSelection 000 240 600 140 "" "Choice of time selection"}

	LABEL	{Label		000 390 230 026 "" "Beams selection"}
	OPTION	{BeamSelection	240 390 200 026 "Beams selection or not"
		 	No		{"No"}
		 	Beams		{"By numbers" Panel_Beams{}}
		 	Phase		{"Phase detection"}
		 	Amplitude	{"Amplitude detection"}}

	PANEL	{Panel_Beams	040 420 520 090 ""

		LABEL	{Label		000 000 270 026 "" "(1st port beam = number 1)"}
		LABEL	{Label		280 000 110 026 "" "1 - From beams"}
		NUMBER	{BeginBeam1	400 000 040 026 "Begin number of beam"}
		LABEL	{Label		450 000 020 026 "" " to"}
		NUMBER	{EndBeam1	480 000 040 026 "End number of beam"}

		LABEL	{Label		000 030 110 026 "" "2 - From beams"}
		NUMBER	{BeginBeam2	120 030 040 026 "Begin number of beam"}
		LABEL	{Label		170 030 020 026 "" " to"}
		NUMBER	{EndBeam2	200 030 040 026 "End number of beam"}

		LABEL	{Label		280 030 110 026 "" "3 - From beams"}
		NUMBER	{BeginBeam3	400 030 040 026 "Begin number of beam"}
		LABEL	{Label		450 030 020 026 "" " to"}
		NUMBER	{EndBeam3	480 030 040 026 "End number of beam"}

		LABEL	{Label		000 060 110 026 "" "4 - From beams"}
		NUMBER	{BeginBeam4	120 060 040 026 "Begin number of beam"}
		LABEL	{Label		170 060 020 026 "" " to"}
		NUMBER	{EndBeam4	200 060 040 026 "End number of beam"}

		LABEL	{Label		280 060 110 026 "" "5 - From beams"}
		NUMBER	{BeginBeam5	400 060 040 026 "Begin number of beam"}
		LABEL	{Label		450 060 020 026 "" " to"}
		NUMBER	{EndBeam5	480 060 040 026 "End number of beam"}
		}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Coratt' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Coratt" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Coratt' >> ${PROGRESS_FILE}
	}
