# ***************************************************************************
# 
#  TRT : ImpTide 
#  DOM : Importation
#  ROL : Traitement d'importation de la maree 
# 
#  CRE : 07/06/2007
#  AUT : CR
#  VER : @(#)1.7
#        @(#)10/03/09
# ***************************************************************************
#  
#  HST : 07/06/2007 CR  FDI-2007-003 : Creation
#        18/06/2007 CR  FDI-2007-003 : Ajout option duree de la moyenne glissante
#                                      et option Donnees maregraphe
#        22/06/2007 CR  FDI-2007-003 : Correction suite test version anglaise
#        03/04/2009 PLR Remplacement fichier RAF98 par une seule valeur en parametre
#        12/05/2009 PLR Suppression DeltaGeoid
#        02/09/2009 MPC ANO-1071 : Distinction des z�ro hydro dans le cas du RTK et du mar�graphe
#        04/03/2010 CE  MOD-863 : Modele RAF98
#        17/02/2011 CE  ANO-298 : Zero hydro du maregraphe (idem RTK)
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_Importation.png}
		HELP		{Tide importation}
		OUT			{TideFileName      FILE	*}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{ImpTidePanel 000 000 580 300 "Tide importation parameters"
	 
	LABEL	{Label        	   000 010 200 026 "" "Output tide file"}
	FILE   	{TideFileName      210 010 340 026 "Caraibes tide file name" *}

	LABEL	{Label        	   000 050 200 026 "" "Tide data origin"}
	OPTION	{TideOrigin   	   210 050 200 026 "Origin of the tide data to import"
			GPS_RTK		{"GPS RTK Data" GpsRtkPanel{} HydroZeroPanel{} RAF98Panel{}}
			TIDE_GAUGE	{"Tide gauge" TideGaugePanel{} HydroZeroPanel{}}
			SHOM	  	{"SHOM Data" ShomPanel{}}}

	PANEL	{GpsRtkPanel	000 090 570 090 "GPS RTK data importation parameters"

		LABEL	{Label        		000 000 200 026 "" "Caraibes navigation files"}
		FILEGEN	{InputNavFileNames	210 000 340 026 "Caraibes input navigation file names" nvi}

		LABEL	{Label	000 030 320 026 "" "Gap between navigation reference and waterline"}
		NUMBER	{Height	330 030 080 026 "Height of navigation reference above waterline in meters"}
		LABEL	{Label	410 030 100 026 "" "meters"}

		LABEL	{Label		000 060 320 026 "" "Sliding mean duration"}
		NUMBER	{MeanDuration	330 060 080 026 "Navigation points sliding mean duration in seconds"}
		LABEL	{Label		410 060 100 026 "" "seconds"}
		}
		
	PANEL	{HydroZeroPanel 000 180 570 120 "Hydrographic zero parameters"

		LABEL	{Label		000 000 320 026 "" "Height of hydrographic zero above the WGS84: ZH"}
#		NUMBER	{HydroZeroIGN69	330 000 080 026 "Height of hydrographic zero above WGS84 elipsoid in meters"}
		LABEL	{Label		410 000 050 026 "" "meters"}

		LABEL	{Label		000 030 320 026 "" "Geodesic shift IGN69/WGS84"}
		OPTION	{GeodesicShift	330 030 150 026 "Correction of geodesic shift"
				No		{"No"}}
#				Constant	{"Constant" ConstantPanel{}}
#				RAF98		{"RAF98 model" PositionPanel{TideOrigin TIDE_GAUGE}}}

		PANEL	{ConstantPanel	000 060 550 030 ""

		 	LABEL	{Label		000 000 320 026 "" "Gap between IGN69 and WGS84"}
		 	NUMBER	{ConstantValue	330 000 080 026 "Correction value in meters"}
		 	LABEL	{Label		420 000 100 026 "" "meters"}
			}

		PANEL	{PositionPanel	000 060 550 060 ""

			LABEL		{Label		000 000 240 026 "" "Geographic position on the model -"}
			LABEL		{Label		250 000 070 026 "" "Latitude"}
			LATITUDE	{Latitude	330 000 220 026 "Latitude"}
			LABEL		{Label		250 030 070 026 "" "Longitude"}
			LONGITUDE	{Longitude	330 030 220 026 "Longitude"}
			}
		}

	PANEL	{TideGaugePanel	000 090 550 030 ""

		LABEL	{Label			000 000 200 026 "" "Tide gauge file"}
		FILE	{TideGaugeFileName	210 000 340 026 "Name of the file provided by the tide gauge" *}
		}

	PANEL	{ShomPanel	000 090 550 030 ""

		LABEL	{Label		000 000 200 026 "" "SHOM tide file"}
		FILE	{SHOMFileName	210 000 340 026 "SHOM tide file name" *}
		}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Execution of ImpTide' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "ImpTide" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of ImpTide' >> ${PROGRESS_FILE}
	}


