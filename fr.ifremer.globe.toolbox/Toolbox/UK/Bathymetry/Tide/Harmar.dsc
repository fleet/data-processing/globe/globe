# ***************************************************************************
# 
#  TRT : Harmar
#  DOM : Bathymetrie 
#  ROL : Correction de maree constantes harmoniques sur les sondes d'un fichier 
#        multifaisceaux
# 
#  CRE : 20/09/96
#  AUT : TBIG
#  VER : @(#)1.10
#        @(#)03/10/01
# ***************************************************************************
#  
#  HST : 20/09/96 TBIG Creation
#        19/02/97 CEDY Validation
#	 04/01/99 LLG  EVOL-IFR 227: introduction de la genericite
#        01/04/99 CEDY FICANO-246: extension fichiers entree/sortie
#        24/10/00 CEDY ANO-571: Correction mineure de presentation
#        21/02/11 PLR  Ajout du choix de l'algorithme
# 
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_TideComp.png}
		HELP		{Tide correction using harmonic method prediction}
		IN		{BeamFileIn	FILEGEN		mbg}
		IN		{HarmonicsFile	FILE		har}
		OUT             {BeamFileOut    FILEMANY	mul LockGen}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{
	 HarmarPanel 000 000 620 300 "Tide correction using  harmonic method prediction parameters"
	
	PANEL	{
		 InterfacesPanel 000 000 620 300 " Harmonic constant tide correction parameters"
	 
		 LABEL          {Label         000 000 270 026 "" "Caraibes bathymetry file(s) to correct"}
		 FILEGEN        {BeamFileIn    280 000 340 026 "Input multibeam file name(s) in CARAIBES format"}
		 
		 LABEL		{Label		000 040 270 026 "" "Harmonic constant File type"}
		 OPTION		{HarmonicsType	280 040 150 026 "Harmonic constant File type"
				 	23	{"23 constants"}
				 	143	{"143 constants"}}

		 LABEL          {Label         000 080 270 026 "" "Harmonic constant File"}
		 FILE		{HarmonicsFile 280 080 340 026 "Harmonic constant file name"}

		 LABEL          {Label         000 120 270 026 "" "Caraibes bathymetry result file(s)"}
		 FILEMANY	{BeamFileOut   280 120 340 026 "Output multibeam file name(s) in CARAIBES format" LockGen}

		 PERIOD_SELECTION          {Period 000 160 590 150 "Time selection to apply to input data" "Period to process"}
		}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Harmar' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Harmar" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Harmar' >> ${PROGRESS_FILE}
	}


