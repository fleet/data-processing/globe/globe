# ***************************************************************************
# 
#  TRT : Marnav
#  DOM : Exportation 
#  ROL : Prediction de maree a partir d'un fichier de points de calcul et d'un fichier d'harmoniques
# 
#  CRE : 13/08/97
#  AUT : CR
#  VER :  @(#)1.5
#         @(#)02/01/15
# ***************************************************************************
#  
#  HST : 13/08/96 CR Creation
#        21/02/11 PLR   Ajout du choix de l'algorithme
# 
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_TideComp.png}
		HELP		{Tide prediction with harmonic method}
		IN		{AsciiFileIn	FILE       *}
		IN		{HarmonicsFile	FILE     har}
		OUT		{AsciiFileOut	FILE       *}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{
	 MarnavPanel 000 000 590 200 "Parameters for tide prediction with harmonic method"
	
	PANEL	{
		 InterfacesPanel 000 000 590 200 "Parameters for tide prediction with harmonic method"
	 
		 LABEL          {Label         000 030 240 026 "" "Calculation points file"}
		 FILE		{AsciiFileIn       250 030 340 026 "Calculation points file name"}
		 
		 LABEL		{Label		000 070 240 026 "" "Type of algorithm "}
		 OPTION		{HarmonicsType	250 070 150 026 "Type of algorithm"
				 	23	{"23 constants"}
				 	143	{"143 constants"}}

		 LABEL          {Label         000 110 240 026 "" "Harmonic constants file"}
		 FILE		{HarmonicsFile 250 110 340 026 "Harmonic constants file name"}

		 LABEL          {Label         000 150 240 026 "" "Output calculation points file"}
		 FILE		{AsciiFileOut  250 150 340 026 "Output calculation points file name" *}

	 	}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Marnav' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Marnav" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Marnav' >> ${PROGRESS_FILE}
	}


