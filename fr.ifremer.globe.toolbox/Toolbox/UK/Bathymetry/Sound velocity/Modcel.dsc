# ***************************************************************************
# 
#  TRT : Modcel 
#  DOM : Celerite
#  ROL : Modification de profil de celerite pour un fichier XY
# 
#  CRE : 19/11/96
#  AUT : TBIG
#  VER : %Z%%I%
#        %Z%%E%
# ***************************************************************************
#  
#  HST : 19/11/96 TBIG Creation
#        19/02/97 CEDY Validation
#        09/03/98 CEDY FICMOD-31: Celerites origine et caracteristiques transducteur
#        16/12/98 PLR  Adaptation au format SMF
#        04/06/99 CEDY FICANO-278: Nom de variables V1.x
#        26/08/99 CEDY FICANO-332: Decoupe horaire
#        30/11/99 CEDY ANO-434: Anomalie de label
#        14/12/99 CEDY ANO-438: Fichier generique en sortie
#        13/12/01 CEDY MOD-540: Celerite origine
#        20/05/03 OA   Fichiers MBG en entree
#        22/05/03 OA   Suppression fichiers NAV pour l'identification profil celerite par distance
#        21/01/05 CE   Validation v3.1 (compensation layer)
#        18/01/10 CE   MOD-855: Position des profils de celerite
#        10/12/10 CE   ANO-1151: Suppression du compensation layer
#        18/04/14 PLR  FDI013-16 : Utilisation optionnelle des temps de parcours lus
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_VelocityComp.png}
		HELP		{Modify sound velocity profile for MBG file}
		IN		{MBGProfilesFile   FILE        vel}
		IN		{MBGFileIn         FILEGEN     mbg}
		OUT		{MBGFileOut        FILEMANY    mbg}	
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{ModcelPanel	000 000 600 520 "Sound velocity profile for MBG file modification parameters"
	
	LABEL	{Label		000 000 230 026 "" "Input Caraibes mbg bathy. files"}
	FILEGEN	{MBGFileIn	240 000 340 026 "MBG file names in CARAIBES format at input" mbg}

	LABEL	{Label		000 030 230 026 "" "Celerity of input bathymetry"}
	OPTION	{SourceVelocity	240 030 180 026 "Celerity source of input bathymetry"
			CONSTANT_VELOCITY	{"Constant celerity" ConstanteVelocityPanel{}}
			VELOCITY_PROFILES	{"Celerity profiles" VelocityProfilesPanel{}}}

	PANEL	{ConstanteVelocityPanel	030 060 500 030 ""
	
		LABEL	{Label			000 000 200 026 "" "Sound velocity value"}
		NUMBER	{BathyInVelocity	210 000 100 026 "Sound velocity value"}
		LABEL	{Label			320 000 150 026 "" "meters/second"}
		}

	PANEL	{VelocityProfilesPanel	030 060 550 030 ""

		LABEL	{Label			000 000 200 026 "" "Caraibes Sound Velocity File"}
		FILE	{MBGProfilesFile	210 000 340 026 "Sound velocity file name for input MBG file" vel}
		}

	LABEL		{Label		000 100 230 026 "" "Output Caraibes mbg bathy. files"}
	FILEMANY	{MBGFileOut	240 100 340 026 "MBG result names in CARAIBES format" LockGen}

	PERIOD_SELECTION	{PeriodSelection 000 140 600 140 "" "Choice for time division"}

	LABEL	{Label		000 280 230 026 "" "Beam travel time origin"}
	OPTION	{RangeType	240 280 200 026 "Beam travel time origin : read or computed"
			COMPUTED 	{"Calculation"}
			FILE   		{"File"}}

	LABEL	{Label		000 320 230 026 "" "Choose profile for recomputation"}
	OPTION	{ComputeType	240 320 200 026 "Type of computation to perform"
			CONSTANT_VELOCITY 	{"Constant sound velocity" VelocityPanel{}}
			CONSTANT_PROFILE   	{"Constant profile" ProfilePanel{} ConstantProfilePanel{}}
			TIME_NEAREST		{"Closer time" ProfilePanel{}}
			TIME_INTERPOLATION	{"Interpolation vs Time" ProfilePanel{}}
			DISTANCE_NEAREST   	{"Closer distance" ProfilePanel{} ProfilePositionPanel{}}
			DISTANCE_INTERPOLATION  {"Interpolation vs Distance" ProfilePanel{} ProfilePositionPanel{}}}

	PANEL	{VelocityPanel	020 350 550 030 ""
	
		LABEL	{Label			000 000 210 026 "" "Reference sound velocity"}
		NUMBER	{ReferenceVelocity	220 000 100 026 "Reference sound velocity"}
		LABEL	{Label			320 000 150 026 "" "meters/second"}
		}

	PANEL	{ProfilePanel	020 350 590 030 ""

		LABEL	{Label		000 000 210 026 "" "Sound Velocity for recomputing"}
		FILE	{ProfilesFile	220 000 340 026 "Sound velocity file name in CARAIBES format for recomputing" vel}
		}
	
	PANEL	{ProfilePositionPanel	020 380 590 030 ""

		LABEL	{Label			000 000 210 026 "" "Geographic position of the profiles"}
		OPTION	{ProfilePosition	220 000 150 026 "Geographic position of the profiles"
			MBG	{"Mbg navigation"}
			PROFILE	{"Profile position"}}
		}
	
	PANEL	{ConstantProfilePanel	020 380 550 030 ""
	
		LABEL	{Label		000 000 210 026 "" "Profile date"}
		DATE	{ProfileTime	220 000 200 026 "Profile date"}
		}

	LABEL	{Label		000 420 230 026 "" "Gap transducer/VRU"}
	OPTION	{Transducer	240 420 150 026 "Input or not of exact transducer specifications"
			False	{"Negligible"}
			True	{"To define" TransducerPanel{}}}

	PANEL	{TransducerPanel	020 450 590 060 ""
	
		LABEL	{Label		000 000 210 026 "" "Transducer depth"}
		NUMBER	{TransducerImm	220 000 050 026 "Depth of transducer below the ship's hull"}
		LABEL	{Label		270 000 060 026 "" "meters"}

		LABEL	{Label		000 030 210 026 "" "Gap transducer-VRU ->     Along:"}
		NUMBER	{TransVruLong	220 030 050 026 "Along track gap: + if transducer in front of VRU"}

		LABEL	{Label		270 030 060 026 "" ", Transv:"}
		NUMBER	{TransVruLong	330 030 050 026 "Across track gap: + if transducer on starboard of VRU"}

		LABEL	{Label		390 030 050 026 "" ", Vert:"}
		NUMBER	{TransVruLong	440 030 050 026 "Vertical gap: + si transducer below the VRU"}
		LABEL	{Label		500 030 050 026 "" "meters"}
		}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Modcel' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Modcel" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Modcel' >> ${PROGRESS_FILE}
	}


