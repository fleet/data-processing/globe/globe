# ***************************************************************************
# 
#  TRT : MbgUpd
#  DOM : Mise a jour des modes de fonctionnement d'un fichier de bathymetrie
#  ROL : Pour chaque cycle, on recupere le mode de fonctionnement dans un fichier de parametres runtime
# 
#  CRE : 18/09/2007
#  AUT : YC
#  VER : @(#)1.2
#        @(#)07/11/05
# ***************************************************************************
#  
#  HST : 18/09/2007 YC    Creation
#        31/10/2007 YC    Extension a l'ensemble des SMF Simrad et aux fichiers .all
#                      EM12d,EM12s, EM950/1000, EM120, EM1002, EM300, EM3000, EM3002
#                      EM710 et ME70.
#        20/06/2013 Mantis 1723 MPC : ajout de la mise � jour des angles transverses
#        04/03/2015 PLR Mode de calcul 'Format' pour simple mise a jour du format
#
# ***************************************************************************

DESCRIPTION	{
		BITMAP 		{CIB_PIT_BathyComp.png}
		HELP		{Update bathymetry files}
		IN		{InputFileNames		FILEGEN		mbg}
		OUT		{OutputFileNames 	FILEMANY	mbg}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------

PANEL	{MbgUpdPanel 000 000 600 350 "Parameters for updating bathymetry files"

	LABEL	{Label			000 030 240 026 "" "Name of the bathymetry input files :"}
	FILEGEN	{InputFileNames		250 030 340 026 "Filenames" mbg}


	LABEL	{Label		        000 070 230 026 "" "Update type : "}
	OPTION	{UpdateType	        250 070 160 026 ""
					Format	        {"Format" FormatPanel {}}
					Runtime	        {"Runtime Parameters" RuntimeParametersPanel {} OptionOutFilePanel {}}
					AcrossAngles	{"Accross Angles" AngleParametersPanel {} OptionOutFilePanel {} }}
					
	PANEL	{FormatPanel	000 110 600 40 ""								
			LABEL		{Label			000 000 240 026 "" "Name of the bathymetry output files : :"}
			FILEMANY	{OutputFileNames	250 000 340 026 "Filenames" LockGen}			
        }				
	PANEL	{AngleParametersPanel	000 110 600 065 ""					
					
             LABEL   {Label			000 40 240 026 "" "Name of .all SIS :"}
             FILEGEN {InputAllFileNames	250 40 340 026 "Filesnames" *}
        }
        					
	PANEL	{RuntimeParametersPanel	000 110 600 065 ""
        						
                LABEL   {Label          000 000 240 026 "" "Data type :"}
                OPTION  {SourceOption   250 000 240 026 ""
                                                 EM12ARCHIV		{"EM 12 - Caraibes/Archiv"}						 
                                                 EM12SIMRAD   {"EM 12 - Simrad"}
                                                 EM1000ARCHIV {"EM 950/1000 - Caraibes/Archiv"}
						 EM1000SIMRAD {"EM 950/1000 - Simrad"}
						 EM120MERLIN  {"EM 120 - Merlin/Solaris"}
						 EM120SIS     {"EM 120 - SIS/PC"}
						 EM1002MERLIN {"EM 1002 - Merlin/Solaris"}
						 EM1002SIS    {"EM 1002 - SIS/PC"}
						 EM2000MERLIN {"EM 2000 - Merlin/Solaris"}
						 EM2000SIS    {"EM 2000 - SIS/PC"}
						 EM300ARCHIV  {"EM 300 - Caraibes/Archiv"}
						 EM300MERLIN  {"EM 300 - Merlin/Solaris"}
						 EM300SIS     {"EM 300 - SIS/PC"}
						 EM3000MERLIN {"EM 3000 - Merlin/Solaris"}
						 EM3000SIS    {"EM 3000 - SIS/PC"}
						 EM3002SIS    {"EM 3002 - SIS/PC"}
						 ME70SIS      {"ME 70 - SIS/PC"}
						 EM710SIS     {"EM 710 - SIS/PC"} }						 


             LABEL   {Label			000 040 240 026 "" "Name of the runtime parameters files :"}
             FILEGEN {InputParamFileNames	250 040 340 026 "Filenames" *}
	}
	
	 PANEL {OptionOutFilePanel 000 180 600 080 ""
    
	    	 LABEL	{Label		000 000 230 026 "" "Update inputfiles : "}
	     
	    	 OPTION	{OverwriteOption	250 000 120 026 ""
					 		 	Yes	{"Oui"}
					  			No	{"Non" OutputFileNamesPanel {}}}				
			PANEL	{OutputFileNamesPanel	000 040 600 040 ""						
					LABEL		{Label			000 000 240 026 "" "Name of the bathymetry output files :"}
					FILEMANY	{OutputFileNames	250 000 340 026 "Filenames" LockGen}		
			        }		
		}


}
# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of MbgUpd' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "MbgUpd" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of MbgUpd' >> ${PROGRESS_FILE}
	}
