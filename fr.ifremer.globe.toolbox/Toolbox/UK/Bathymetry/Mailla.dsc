# ***************************************************************************
# 
#  TRT : Mailla 
#  DOM : Traitement des donnees brutes de bathymetrie 
#  ROL : Creation d'un fichier MNT avec ponderation et rayon d'influence
# 
#     Ce module realise le maillage des donnees multifaisceaux a partir
#     de donnees au format (x,y) : on obtient un Modele Numerique
#     de Terrain
#
#  CRE : 04/11/96
#  AUT : JMS
#  VER : @(#)1.22
#        @(#)06/12/28
# ***************************************************************************
#  
#  HST : 04/11/96 JMS  Creation
#  	 27/01/97 JMS  Modification suite a recette interne
#        19/02/97 CEDY Validation
#	 05/05/98 LLG  ANO-IFR 57: Correction traduction et toggle interpolation
#        16/10/98 PLR  Adaptation SMF
#        29/12/98 JFP  FDI 38 : Gestion des flags de validite
#        13/01/99 CEDY FICMOD-239: Decoupe horaire
#        04/06/99 CEDY FICANO-278: Nom de variables V1.x
#        16/12/99 CEDY ANO-412: Modifications de labels
#	 23/05/00 PLB  Choix MNT/MNQ
#	 11/09/00 CR   MOD-161: Definition du cadre geo droit par l'utilisateur
#        12/07/01 CE   ANO-635: Panneau trop grand
#        19/04/02 CE   MOD-568: Suppression resolution
#        14/05/03 OA   Ajout projection cartographique et fichiers MBG en entree
#        13/06/05 PLR  FDI 2005-016 : Ajout option fichier de sondes isolees
#        25/07/06 CE   MOD-718: Angle d'emission
#        20/10/06 CE   MOD-730: Maiullage de la reflectivite
#        26/03/10 PLR  Evols 2010: Ajout de l'option CDI pour les MNQ
#        26/05/10 PLR  Correction conditions d'affichage du panneau d'interpolation pour MNQ
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_BathyComp.png}
		HELP		{Simple average of close values for grid mapping}
		IN		{MBGFile	FILEGEN   mbg}
		OUT		{MNTFile	FILE     mnt}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{MaillaPanel 000 000 640 600 "Grid mapping parameters using simple average of close values"
	
	PANEL	{InterfacesPanel 000 000 640 565 "Grid mapping parameters using simple average of close values"
	 
		LABEL	{Label        000 000 230 026 "" "Caraibes MBG Bathymetry files read"}
		FILEGEN	{MBGFile      240 000 360 026 "MBG file names to be grid mapped in CARAIBES format at input" mbg}

		LABEL	{Label        000 035 230 026 "" "DTM Caraibes result"}
		FILE	{MNTFile      240 035 360 026 "DTM result file name in CARAIBES format at output" mnt}

		LABEL	{Label        000 070 230 026 "" "Result file type"}
		OPTION	{FileType     240 070 360 026 "Result file type" 
						DTM  	{"DTM - Digital Terrain Model - Bathymetry" Panel_Interpolation{}}
						DRM  	{"DTM - Digital Terrain Model - Reflectivity" Panel_Interpolation{}}
						DQM	{"DQM - Digital Quality Model - Bathymetry" Panel_Interpolation{CdiOption No} Panel_CDI{}}}

		LABEL	{Label        000 105 230 026 "" "Size of elementary DTM cell"}
		NUMBER	{StepGrid     240 105 060 026 "Grid step in meters"}
		LABEL	{Label        310 105 280 026 "" "meters (spacing between lines and columns)"}

		LABEL	{Label	000 140 230 026 "" "DTM cells assigned by a sounding"}
		OPTION	{Links	240 140 250 026 "Number of assigned adjacent nodes "
				 1    	{"1 (the closest)"}
				 2  	{"2 (the 2 closest)"}
				 4   	{"4 (the 4 around each sounding)"}}

		LABEL	{Label		000 175 230 026 "" "Filtering on emission angle"}
		OPTION	{EmissionAngle	240 175 080 026 "Taking in account or not of emission angle" 
				Yes  	{"Yes" Panel_FilterAngle{}}
				No	{"No"}}

		PANEL	{Panel_FilterAngle 340 175 300 030 ""

			LABEL	{Label	000 000 300 026 "" "Note : a profile splitting is necessary"}
			}

		LABEL	{Label	       000 210 230 026 "" "XY soundings mini-maxi check"}
		OPTION	{DepthLimits   240 210 080 026 "XY soundings with values out of interval can be deleted"
				 False	  {"No"}
				 True	  {"Yes" Panel_DepthLimits{}}}

		PANEL	{Panel_DepthLimits 330 210 310 060 ""

			LABEL	{Label          000 000 120 026 "" "Minimum (high)"}
			NUMBER	{TopLimits   	130 000 060 026 "Minimum depth (in meters) above which all grid soundings will be deleted"}
			LABEL	{Label          200 000 110 026 "" "meters (>0)"}
			LABEL	{Label          000 030 100 026 "" "Maximum (low)"}
			NUMBER	{BottomLimits   130 030 060 026 "Maximum depth (in meters) beyond which all grid soundings will be deleted"}
			LABEL	{Label          200 030 110 026 "" "meters (>0)"}
			}

		LABEL	{Label	       000 275 230 026 "" "Delete isolated values"}
		OPTION	{AloneValues   240 275 080 026 "Delete values assigned in isolation on a  DTM point"
				 False	  {"No"}
				 True	  {"Yes" Panel_AloneValues{}}}

		PANEL	{Panel_AloneValues 330 275 310 030 ""

			LABEL	{Label          000 000 120 026 "" "With at least"}
			NUMBER	{SuppNbr   	130 000 040 026 "Minimum  number of soundings from MBG file already assigned to grid point"}
			LABEL	{Label          180 000 130 026 "" "soundings / DTM cell"}
			}

		PANEL   {Panel_Interpolation 000 310 640 090 "" 
			LABEL	{Label	       000 000 230 026 "" "Interpolation after computation"}
			OPTION	{Interpolation 240 000 080 026 "Bi-linear Interpolation"
				 False	  {"No"}
				 True	  {"Yes" Step_Interpolation{} Contour_Interpolation{}}}

			PANEL	{Step_Interpolation 330 000 310 030 ""

				LABEL	{Label               000 000 120 026 "" "With a step of"}
				NUMBER	{InterpolationStep   130 000 040 026 "Step"}
				LABEL	{Label               180 000 130 026 "" "lines / columns"}
				}

			PANEL	{Contour_Interpolation 050 030 570 060 ""

				LABEL	{Label               000 000 180 026 "" "Limitation to a contour"}
				OPTION	{InterpolationGrid   190 000 080 026 "Interpolation over entire grid"
				 	True	  {"Yes" Panel_InterpolationGrid{}}
				 	False	  {"No"}}

				PANEL	{Panel_InterpolationGrid 000 030 550 030 ""

					LABEL          {Label   	000 000 180 026 "" "Contours File"}
					FILE	       {PerimetreFile   190 000 360 026 "Perimeter file name"}
					}
				}
			}

		LABEL	{Label		000 405 230 026 "" "Soundings to process"}
		OPTION	{SoundsOption	240 405 200 026 "Process invalid soundings"
				 No	{"Valid only"}
				 Yes	{"Valid and invalid"}}

		LABEL	{Label	             000 440 230 026 "" "Soundings files integration "}
		OPTION	{SoundingFilesOption 240 440 080 026 "Option to add soundings from soundings files"
				 No	  {"No"}
				 Yes	  {"Yes" Panel_SoundingSign{} Panel_SoundingFiles{}}}

		PANEL	{Panel_SoundingSign 330 440 310 030 ""

			LABEL	{Label		    000 000 190 026 "" "Swap soundings sign"}
			OPTION	{ChangeSoundingSign 190 000 080 026 "Option to change the sign of soundings"
				 	No	  {"No"}
				 	Yes	  {"Yes"}}
			}

		PANEL	{Panel_SoundingFiles 000 470 640 030 ""

			LABEL	{Label              000 000 230 026 "" "Soundings files names"}
			FILEGEN	{SoundingFilesNames 240 000 360 026 "Names of the soundings files in ASCII format" *}
			}

		PANEL	{Panel_CDI 000 505 640 060 ""
			LABEL	{Label	   000 000 230 026 "" "CDI Layer option"}
			OPTION	{CdiOption 240 000 080 026 "Option to add a layer of CDI (Common data Index Seadatanet)"
				 No	  {"No"}
				 Yes	  {"Yes" Panel_CdiFiles{}}}

			PANEL	{Panel_CdiFiles 000 035 640 030 ""

				LABEL	{Label              000 000 230 026 "" "CDI lists files"}
				FILEGEN	{CdiFilesNames 240 000 360 026 "Name of files containing CDI lists, ASCII format" *}
				}
			}
		}

	PANEL	{Cartography 000 020 620 520 "Cartographic parameters for DTM file creation"

		LABEL	{Label		000 000 230 026 "" "Geometry of result DTM"}
		OPTION	{FrameType	240 000 200 026 "Straight grid:  follows borders of geo frame of MBG file or entered by user. Oblique grid: define a three point base, to characterize gridded zone which does not follow borders of geo frame."
				 Straight    	{"MBG files Straight frame"}
				 StraightUser	{"Straight user frame" Panel_StraightFrame{}}
				 Oblic  	{"Oblique frame" Panel_OblicFrame{}}}

		PANEL	{Panel_OblicFrame 000 040 610 200 "Grid is defined by 3 points A,B and C. (AB,AC) must form a direct trihedron. More simply : AB <=> x, AC <=> y"
			LABEL		{Label          200 000 100 026 "" "Latitude"}
			LABEL		{Label          440 000 100 026 "" "Longitude"}
			LABEL		{Label          020 030 060 026 "" "Base"}
			LABEL		{Label          040 060 060 026 "" "Point A"}
			LATITUDE	{LatitudeA   	110 060 220 026 "point A : Origin of DTM base"}
			LONGITUDE	{LongitudeA   	350 060 220 026 "point A : Origin of DTM base"}
			LABEL		{Label          040 090 060 026 "" "Point B"}
			LATITUDE	{LatitudeB   	110 090 220 026 "point B : Defines x axes of oblique marker. (constrains number of columns in the grid, as well as rotation between the geographic frame and the DTM)."}
			LONGITUDE	{LongitudeB   	350 090 220 026 "point B : Defines x axes of oblique marker. (constrains number of columns in the grid, as well as rotation between the geographic frame and the DTM)."}
			LABEL		{Label          020 130 060 026 "" "Vertex"}
			LABEL		{Label          040 160 060 026 "" "Point C"}
			LATITUDE	{LatitudeC   	110 160 220 026 "point C : Defines y axes (length AC is what determines the line number of the grid)"}
			LONGITUDE	{LongitudeC   	350 160 220 026 "point C : Defines y axes (length AC is what determines the line number of the grid)"}
			}

		 PANEL   {Panel_StraightFrame 000 040 610 200 "Geographic frame defined by user"

                        GEOFRAME        {GeographicFrame  000 000 450 200 "Straight user frame" "V"}
                        }

		LABEL	     {Label      000 250 080 026 "" "XY projection"}  
		PROJECTION   {Projection    100 250 500 350 "" "Type of XY projection"} 

		}

	PANEL	{BeamSelection 000 000 620 510 "Beams to process in grid selection parameters"

		LABEL	{Label		000 020 200 026 "" "Beams to process per ping"}
		OPTION	{BeamSelection	210 020 220 026 "Beam selection option"
				 AllBeams	{"All beams"}
				 VertBeams	{"Vertical beams" Panel_VertBeams {}}
				 ListBeams	{"List of beams"  Panel_ListBeams {}}}

		PANEL  	{Panel_VertBeams 100 070 450 026 ""
		
			LABEL	{Label			000 000 150 026 "" "Number of beams"}
			NUMBER	{VertBeamsNumber	160 000 040 026 "Number of beams"}
			LABEL	{Label			210 000 200 026 "" " (on either side of vertical)"}
			}
				 
		PANEL  	{Panel_ListBeams 000 050 640 300  ""

			LABEL	{Label		000 000 080 026 "" "Interval 1"}
			LABEL	{Label		090 000 075 026 "" "From beam"}
			NUMBER	{BeginBeam1	170 000 040 026 "Interval start beam number"}
			LABEL	{Label		215 000 025 026 "" " , to "}
			NUMBER	{EndBeam1	250 000 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 030 080 026 "" " Interval  2"}
			LABEL	{Label		090 030 075 026 "" "From beam"}
			NUMBER	{BeginBeam2	170 030 040 026 "Interval start beam number"}
			LABEL	{Label		215 030 025 026 "" " , to "}
			NUMBER	{EndBeam2	250 030 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 060 080 026 "" " Interval  3"}
			LABEL	{Label		090 060 075 026 "" " From beam"}
			NUMBER	{BeginBeam3	170 060 040 026 "Interval start beam number"}
			LABEL	{Label		215 060 025 026 "" " , to "}
			NUMBER	{EndBeam3	250 060 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 090 080 026 "" " Interval  4"}
			LABEL	{Label		090 090 075 026 "" " From beam"}
			NUMBER	{BeginBeam4	170 090 040 026 "Interval start beam number"}
			LABEL	{Label		215 090 025 026 "" " , to "}
			NUMBER	{EndBeam4	250 090 040 026 "Interval end beam number"}

			LABEL	{Label		000 120 080 026 "" " Interval  5"}
			LABEL	{Label		090 120 075 026 "" " From beam"}
			NUMBER	{BeginBeam5	170 120 040 026 "Interval start beam number"}
			LABEL	{Label		215 120 025 026 "" " , to "}
			NUMBER	{EndBeam5	250 120 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 150 080 026 "" " Interval  6"}
			LABEL	{Label		090 150 075 026 "" " From beam"}
			NUMBER	{BeginBeam6	170 150 040 026 "Interval start beam number"}
			LABEL	{Label		215 150 025 026 "" " , to "}
			NUMBER	{EndBeam6	250 150 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 180 080 026 "" " Interval  7"}
			LABEL	{Label		090 180 075 026 "" " From beam"}
			NUMBER	{BeginBeam7	170 180 040 026 "Interval start beam number"}
			LABEL	{Label		215 180 025 026 "" " , to "}
			NUMBER	{EndBeam7	250 180 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 210 080 026 "" " Interval  8"}
			LABEL	{Label		090 210 075 026 "" " From beam"}
			NUMBER	{BeginBeam8	170 210 040 026 "Interval start beam number"}
			LABEL	{Label		215 210 025 026 "" " , to "}
			NUMBER	{EndBeam8	250 210 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 240 080 026 "" " Interval  9"}
			LABEL	{Label		090 240 075 026 "" " From beam"}
			NUMBER	{BeginBeam9	170 240 040 026 "Interval start beam number"}
			LABEL	{Label		215 240 025 026 "" " , to "}
			NUMBER	{EndBeam9	250 240 040 026 "Interval end beam number"}
		 
			LABEL	{Label		000 270 080 026 "" " Interval 10"}
			LABEL	{Label		090 270 075 026 "" " From beam"}
			NUMBER	{BeginBeam10	170 270 040 026 "Interval start beam number"}
			LABEL	{Label		215 270 025 026 "" " , to "}
			NUMBER	{EndBeam10	250 270 040 026 "Interval  end  beam number"}
		 
			LABEL	{Label		320 000 080 026 "" " Interval 11"}
			LABEL	{Label		410 000 075 026 "" " From beam"}
			NUMBER	{BeginBeam11	490 000 040 026 "Interval start beam number"}
			LABEL	{Label		535 000 025 026 "" " , to "}
			NUMBER	{EndBeam11	570 000 040 026 "Interval end beam number"}
		 
			LABEL	{Label		320 030 080 026 "" " Interval 12"}
			LABEL	{Label		410 030 075 026 "" " From beam"}
			NUMBER	{BeginBeam12	490 030 040 026 "Interval start beam number"}
			LABEL	{Label		535 030 025 026 "" " , to "}
			NUMBER	{EndBeam12	570 030 040 026 "Interval end beam number"}
		 
			LABEL	{Label		320 060 080 026 "" " Interval  13"}
			LABEL	{Label		410 060 075 026 "" " From beam"}
			NUMBER	{BeginBeam13	490 060 040 026 "Interval start beam number"}
			LABEL	{Label		535 060 025 026 "" " , to "}
			NUMBER	{EndBeam13	570 060 040 026 "Interval start beam number"}
		 
			LABEL	{Label		320 090 080 026 "" " Interval 14"}
			LABEL	{Label		410 090 075 026 "" " From beam"}
			NUMBER	{BeginBeam14	490 090 040 026 "Interval start beam number"}
			LABEL	{Label		535 090 025 026 "" " , to "}
			NUMBER	{EndBeam14	570 090 040 026 "Interval end beam number"}

			LABEL   {Label		320 120 080 026 "" " Interval 15"}
			LABEL	{Label		410 120 075 026 "" " From beam"}
			NUMBER	{BeginBeam15	490 120 040 026 "Interval start beam number"}
			LABEL	{Label		535 120 025 026 "" " , to "}
		 	NUMBER	{EndBeam15	570 120 040 026 "Interval end beam number"}
		
			LABEL	{Label		320 150 080 026 "" " Interval 16"}
			LABEL	{Label		410 150 075 026 "" " From beam"}
			NUMBER	{BeginBeam16	490 150 040 026 "Interval start beam number"}
			LABEL	{Label		535 150 025 026 "" " , to "}
			NUMBER	{EndBeam16	570 150 040 026 "Interval end beam number"}
		
			LABEL	{Label		320 180 080 026 "" " Interval 17"}
			LABEL	{Label		410 180 075 026 "" " From beam"}
			NUMBER	{BeginBeam17	490 180 040 026 "Interval start beam number"}
			LABEL	{Label		535 180 025 026 "" " , to "}
			NUMBER	{EndBeam17	570 180 040 026 "Interval end beam number"}
		
			LABEL	{Label		320 210 080 026 "" " Interval 18"}
			LABEL	{Label		410 210 075 026 "" " From beam"}
			NUMBER	{BeginBeam18	490 210 040 026 "Interval start beam number"}
			LABEL	{Label		535 210 025 026 "" " , to "}
			NUMBER	{EndBeam18	570 210 040 026 "Interval end beam number"}
		 
			LABEL	{Label		320 240 080 026 "" " Interval 19"}
			LABEL	{Label		410 240 075 026 "" " From beam"}
			NUMBER	{BeginBeam19	490 240 040 026 "Interval start beam number"}
			LABEL	{Label		535 240 025 026 "" " , to "}
			NUMBER	{EndBeam19	570 240 040 026 "Interval end beam number"}
		 
			LABEL	{Label		320 270 080 026 "" " Interval 20"}
			LABEL	{Label		410 270 075 026 "" " From beam"}
			NUMBER	{BeginBeam20	490 270 040 026 "Interval start beam number"}
			LABEL	{Label		535 270 025 026 "" " , to "}
			NUMBER	{EndBeam20	570 270 040 026 "Interval end beam number"}
			}

		PERIOD_SELECTION	{PeriodSelection	000 370 600 140 "" "Time division"}
		}

	BUTTON 	{Button1 100 575 100 026 "Access interface panel"
		 Activate {"Interfaces" InterfacesPanel{}}}

	BUTTON 	{Button2 220 575 100 026 "Access cartographic characteristics panel"
		 Activate {"Cartography" Cartography{}}}

	BUTTON 	{Button3 340 575 150 026 "Access beam/time division selection panel"
		 Activate {"Beams/Division" BeamSelection{}}}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Mailla' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Mailla" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Mailla' >> ${PROGRESS_FILE}
	}
 
