# ***************************************************************************
# 
#  TRT : FlagMbb
#  DOM : Invalidation de cycles et/ou de faisceaux
#  ROL : 
# 
#     Ce module permet la validation/invalidation de sondes, de faisceaux ou
#     de cycles en fonction de critères divers
#
#  CRE : 16/06/2003
#  AUT : JL
#  VER : @(#)1.16
#        @(#)09/03/20
# ***************************************************************************
#  
#  HST : 16/06/03 JL   Creation
#        14/06/04 CE   ANO-840: Erreur nom de variable
#        02/07/04 JL   Ajout d'une option d'invalidation : par fichier ASCII
#        06/12/04 CE   Validation V3.1
#        26/01/05 PLR  Remplacement libelle angle d'emission par angle d'incidence
#        29/03/05 OA   FDI-2005-003 : Ajout d'une option de mode de repérage pour
#                      l'invalidation/revalidation par fichier ASCII
#        24/03/06 CE   Invalidation par codes qualité des sondeurs Reson
#	 22/06/07 VF   FDI-2007-07 : Option de filtrage sur profondeur Min/Max
#        11/12/07 MPC  MOD-  : Ajout de NADIR FILTER
#        08/07/07 JMS  MOD-782  : Ajout de quality factor
#        19/03/09 CE   MOD-804 : Selection des faisceaux par mode dee detection
#        14/11/12 PLR  MOD-1422 : L'option Facteur Qualite passe au niveau general ParamCriteria
#        17/04/13 PLR  FDI-2013-005 : Ajout du parametre mode de detection pour l'option Facteur Qualite
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_BathyComp.png}
		HELP		{Validation/Invalidation of pings and beams}
		IN		{MBGFile	FILEGEN   mbg}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{FlagMbbPanel 000 000 640 330 "Parameters of validation/invalidation of pings and beams"

	LABEL	  {Label	000 000 230 026 "" "Bathymetry files (.mbg)"}
	FILEGEN	  {MBGFile      240 000 360 026 "Name of the entry files" mbg}

	LABEL     {Label        000 050 230 026 "" "Type of process"}
	OPTION	  {ProcessType  240 050 150 026 ""
		  Unvalidate    {"Invalidation"}
		  Validate	{"Validation"}
		  }

	LABEL     {Label         000 100 230 026 "" "Selection criteria"}
	OPTION	  {ParamCriteria 240 100 200 026 ""
		  BeamNum       {"Numbers of beams"		PanelBeamNum{} }
		  SoundAng      {"Incidence angles"		PanelSoundAng {} }
		  SoundDist     {"Lateral distances"		PanelSoundDist{} }
		  PingNum       {"Numbers of pings"		PanelPingNum{} }
		  PingDate      {"Period selection"		PanelPingDate{} }
		  AsciiFile     {"Ascii file"			PanelAsciiFile{} }
		  Threshold     {"Threshold of soundings"	PanelThreshold1{} }
		  ResonS7k      {"71xx Reson MBES"		PanelResonS7k{} }
		  Phase		{"Phase detection" }
		  Amplitude	{"Amplitude detection" }
		  Quality	{"Quality factor"		PanelQualityFactor{}}
		  }

	PANEL     {PanelBeamNum 000 150 640 180 ""
		LABEL     {Label        000 000 230 026 "" "Beams numbers interval(s)"}
		TEXTFIELD {BeamNumbers  240 000 360 026 "Example : 7/20;30/40"}
		  }

	PANEL     {PanelSoundAng 000 150 640 180 ""
		LABEL     {Label        000 000 230 026 "" "Incidence angles interval(s)"}
		TEXTFIELD {SoundAngles  240 000 360 026 "Example : -5.4/6.5;10.5/35.8"}
		  }

	PANEL     {PanelSoundDist 000 150 640 180 ""
		LABEL     {Label        000 000 230 026 "" "Laterals distances interval(s)"}
		TEXTFIELD {SoundDistLat 240 000 360 026 "Example : -3.2/3.2"}
		  }

	PANEL     {PanelPingNum 000 150 640 180 ""
		LABEL     {Label        000 000 230 026 "" "Pings numbers interval(s)"}
		TEXTFIELD {PingNumbers  240 000 360 026 "Exemple : 500/780;1040/1630"}
		  }

	PANEL     {PanelPingDate 000 150 640 180 ""
		PERIOD_SELECTION	{PeriodSelection	000 040 600 140 "" "Period selection"}
		  }

	PANEL	  {PanelAsciiFile 000 150 640 180 ""
		LABEL     {Label	000 000 230 026 "" "Name of the Ascii file"}
		FILE      {AscFileName  240 000 360 026 "Name of the file listing all soundings to modify" txt}

		LABEL     {Label         000 050 230 026 "" "Location mode"}
		OPTION	  {AsciiLocationMode 240 050 250 026 ""
		  PingNumberMode    {"Ping / beam numbers"}
		  JulianTimeMode    {"Julian time / hour / beam"}
		  GregorianTimeMode {"Gregorian date / hour /beam"}}
		  } 

	 PANEL	{PanelThreshold1 000 150 650 026 "Threshold Min/Max depth value"
		 LABEL          {Label	           000 000 100 026 "" "Threshold "}
		 LABEL          {Label	           240 000 026 026 "" "Min"}
		 NUMBER		{Min               270 000 075 026 "Threshold minimal value"}
		 LABEL          {Label	           350 000 020 026 "" "m"}

		 LABEL          {Label	           380 000 026 026 "" "Max"}
		 NUMBER		{Max               410 000 075 026 "Threshold maximal value"}
		 LABEL          {Label	           490 000 020 026 "" "m"}
		}

	PANEL	  {PanelResonS7k 000 150 640 180 ""
		LABEL     {Label 		000 000 230 026 "" "Type of test"}
		OPTION	  {ResonS7kTest		240 000 200 026 ""
		  BrightnessTest	{"Brightness"}
		  ColinearityTest 	{"Colinearity"}
		  BothTest		{"Brightness and colinearity"}
		  NadirTest             {"Nadir"}
                  FrequencyPlane        {"Frequency plane"	PanelFrequencyPlane{}}}


        PANEL   {PanelFrequencyPlane 240 040 640 026 "Frequency planes selection"

                        LABEL   {Label          000 000 055 026 "" "Freq.   1"}
                        TOGGLE  {FreqNb1        057 000 020 026 "Selection of the frequency 1 or not"
                                False   {}
                                True    {}}

                        LABEL   {Label          090 000 006 026 "" "2"}
                        TOGGLE  {FreqNb2        102 000 020 026 "Selection of the frequency 2 or not"
                                False   {}
                                True    {}}

                        LABEL   {Label          135 000 006 026 "" "3"}
                        TOGGLE  {FreqNb3        148 000 020 026 "Selection of the frequency 3 or not"
                                False   {}
                                True    {}}

                        LABEL   {Label          180 000 006 026 "" "4"}
                        TOGGLE  {FreqNb4        192 000 020 026 "Selection of the frequency 4 or not"
                                False   {}
                                True    {}}

                } 
	  }

	PANEL	{PanelQualityFactor 000 150 640 180 "Filtering threshold"
		 	LABEL	{Label         000 000 230 026 "" "Detection mode selection"}
		 	OPTION	{DetectionMode 240 000 200 026 ""
		 			Phase		{"Phase detection"}
		 			Amplitude	{"Amplitude detection"}
		 			All		{"No selection"}}

		 	LABEL          {Label	           000 050 150 026 "" "Threshold value"}
		 	NUMBER	       {QualityFactor      240 050 075 026 "Threshold value 2#1% 3#0.1%"}
		 	LABEL          {Label	           350 050 150 026 "" "log (H/delta(H))"}
			}
	   		
	}
	
# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of FlagMbb' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "FlagMbb" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of FlagMbb' >> ${PROGRESS_FILE}
	}
 
