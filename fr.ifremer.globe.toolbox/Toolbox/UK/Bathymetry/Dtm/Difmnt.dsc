# ***************************************************************************
# 
#  TRT : Difmnt 
#  DOM : Bathymetrie
#  ROL : Operations arithmetiques sur 2 grilles (MNT, MNQ, MOS)
# 
#  CRE : 15/01/97
#  AUT : TBIG
#  VER : @(#)1.14
#        @(#)05/11/23
# ***************************************************************************
#  
#  HST : 15/01/97 TBIG Creation
#        19/02/97 CEDY Validation
#        11/06/99 CEDY FICANO-294: petites anomalies de presentation
#        28/03/00 CEDY ANO-482: Labels
#        13/09/00 PLB  Adaptation pour traitement de fichiers multi-canaux
#        12/12/03 CE   Recette CIRCE-SHOM
#	 31/03/10 PLR  Evols 2010 : Filtrage des layers en entree
#	 14/04/10 PLR  Evols 2010 : Adaptations Emodnet
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_Dtmcomp.png}
		HELP		{Operations on two grids}
		IN		{DtmFile2In	FILE     mnt}
		IN		{DtmFile1In	FILE     mnt}
		OUT		{DtmFileOut	FILE     mnt}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{
	 DifmntPanel 000 000 580 480 "Operation parameters on two grids"

	 LABEL           {Label              000 000 220 026 "" " Caraibes Grid - 1st operand"}
	 FILECHANNELLIST {DtmFile1In         230 000 340 060 "Grid 1 file name in CARAIBES format at input" "" ALL}

	 LABEL          {Label              000 070 220 026 "" " Caraibes Grid - 2nd operand"}
	 FILE	        {DtmFile2In         230 070 340 026 "Grid 2 file name in CARAIBES format at input" mnt}

	 LABEL          {Label              000 100 220 026 "" " Caraibes Grid result"}
	 FILE		{DtmFileOut         230 100 340 026 " Grid file name in CARAIBES format at output" mnt}

	LABEL		{Label              000 140 220 026 "" "Output channels"}
	OPTION		{OutChannels        230 140 250 026 "Output DTM composition"
				 All		{"All input channels"}
				 Selected	{"Only processed channels"}}

	 LABEL          {Label              000 180 220 026 "" "Type of operation"}
	 OPTION		{OperationType      230 180 200 026 "Type of operation to perform on two grids at input"
			 DIFFERENCE 		{"Subtraction"}
			 ABSDIFFERENCE		{"| Subtraction |"}
			 LINEARCOMBINATION      {"Linear combination" CombinationLabel {} FactorsPanel {}}
			 AVERAGE                {"Weighted average"     AverageLabel {}     FactorsPanel {}}
                         COMPLETION             {"Complement" CdiPanel{}}
                         SLOPE                  {"Weighting / slopes" SlopePanel {}}}

	 LABEL          {CombinationLabel   030 210 200 026 "" "A*Grid1 + B*Grid2"}
	 LABEL          {AverageLabel       030 210 200 026 "" "(A*Grid1 + B*Grid2) / (A+B)"}

	 PANEL		{
			 FactorsPanel       230 210 550 030 ""

			 LABEL          {Label	        000 000 030 026 "" "A ="}
			 NUMBER		{A              030 000 080 026 "Coefficient A"}
			 LABEL          {Label	        120 000 040 026 "" ", B ="}
			 NUMBER		{B              160 000 080 026 "Coefficient B"}
			}

	 PANEL		{
			 SlopePanel   000 210 570 100 "Weighting / slopes parameters"

			 LABEL          {Label	        030 000 170 026 "" "Minimum slope threshold"}
			 NUMBER		{MinSlope       230 000 080 026 "Minimum slope"}
			 LABEL          {Label	        320 000 100 026 "" "%       Maximum"}
			 NUMBER		{MaxSlope       430 000 080 026 "Maximum slope"}
			 LABEL          {Label	        520 000 020 026 "" "%"}

			 LABEL          {Label	        030 030 190 026 "" "Slope computation with"}
			 OPTION		{UseSlopeFile   230 030 120 026 "Slope computation grid"
					 False    	{"Grid 1"}
					 True  	        {"Other grid" SlopeFilePanel {}}}

			 PANEL	{
				 SlopeFilePanel  000 070 570 030 ""

				 LABEL          {Label         030 000 190 026 "" "Grid for slope computation"}
				 FILE		{SlopeFile     230 000 340 026 "Grid 3 file name in CARAIBES format for slope computation" mnt}
				}
			}

	 PANEL		{
			 CdiPanel   000 220 550 060 "Cdi option for 2nd MNT"

			 LABEL          {Label	        000 000 220 026 "" "Enter CDI for 2nd MNT"}
			 OPTION		{CdiOption      230 000 080 026 "Option to enter a CDI for the 2nd MNT"
					 Yes    	{"Yes" CdiValuePanel {}}
					 No  	        {"No"}}
			 PANEL	{
				 	CdiValuePanel  000 030 550 030 ""

					LABEL		{Label   030 000 190 026 "" "CDI (Common Data Index)"}
				 	TEXTFIELD	{Cdi     230 000 310 026 "CDI (Common Data Index)"}
				}
			}

 	 LABEL          {Label              000 340 200 026 "" "Check results"}
	 TOGGLE		{CheckGrid          230 340 030 026 "Check results with minimum-maximum filter"
			 False	{}
			 True	{CheckGridPanel {}}}
 	 LABEL          {Label              280 340 100 026 "" "(min-max)"}

	 PANEL		{
			 CheckGridPanel     000 370 550 030 ""

			 LABEL          {Label	        030 000 190 026 "" "Minimum boundary"}
			 NUMBER		{Minimum        230 000 080 026 "Minimum boundary"}
			 LABEL          {Label	        320 000 100 026 "" "  ,       maximum"}
			 NUMBER		{Maximum        430 000 080 026 "Maximum boundary"}
			}

	 LABEL          {Label              000 410 200 026 "" "Operation limited to a contour"}
	 TOGGLE		{Contouring         230 410 030 026 "Select part of grid by a contour"
			 False	{}
			 True	{ ContouringFilePanel {}}}

	 PANEL		{	
			 ContouringFilePanel  000 440 570 030 ""

			 LABEL          {Label              030 000 170 026 "" "Contours file"}
			 FILE		{ContouringFile     230 000 340 026 "Contour file name" *}
			}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Difmnt' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Difmnt" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Difmnt' >> ${PROGRESS_FILE}
	}
