# ***************************************************************************
# 
#  TRT : Invmnt 
#  DOM : Bathymetrie 
#  ROL : Operations arithmetiques sur une grille (MNT/MNQ/MOS)
# 
#  CRE : 17/12/96
#  AUT : TBIG
#  VER : @(#)1.19
#        @(#)09/07/23
# ***************************************************************************
#  
#  HST : 17/12/96 TBIG Creation
#        20/01/97 TBIG Correction des resolutions de sonde : 0,1 -> 0.1
#        19/02/97 CEDY Validation
#        31/12/97 CEDY FICMOD-22:  Nouvelle operation "Valeurs significatives"
#        20/04/98 LLG  ANO-IFR 45: correction anomalie de traduction
#	 05/05/98 LLG  ANO-IFR 57: correction anomalie de traduction
#        16/06/98 CEDY FICANO-89:  Probleme avec option raz complement
#        26/03/99 CEDY FICMOD-53:  Translation geometrique
#        20/05/99 CEDY FICANO-287: Options invisibles
#        07/09/00 PLB  Adaptation pour traitement de fichiers multi-canaux
#        23/04/02 CE   MOD-568: Suppression changement de resolution
#        26/04/02 CE   MOD-433: Changement de latitude conservee
#        29/04/02 CE   MOD-439: Changement de systeme geodesique
#        15/05/02 CE   MOD-567: Changement de meridien central
#        22/07/09 CE   MOD-738: Fenetre de lissage par filtre numerique
#	 31/03/10 PLR  Evols 2010 : Filtrage des layers en entree
#	 06/04/10 PLR  Evols 2010 : Ajout de l'option Reduction-Emodnet
#	 07/07/10 PLR  Evols 2010 : Ajout de l'option d'export de prefix CDI
#	 17/11/11 PLR  FDI 2011-051 : Suppression du filtre numerique le FILECHANNELLIST 
#	 10/05/12 PLR  FDI 2012-006 : Ajout de l'option de remplacement de CDI 
#	 29/06/12 PLR  FDI 2012-008 : Remplacement de CDI par fichier
#        11/10/13 PLR  FDI 2013-013 : Saisie d'un pas de reduction Emodnet
#        28/11/13 PLR  FDI 2013-013 : Option de reduction du cadre geo pour reduction Emodnet
#        08/01/14 PLR  FDI 2013-014 : Suppression de l'option de reduction du cadre geo Emodnet
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_Dtmcomp.png}
		HELP		{Operations on a Grid}
		IN		{DtmFileIn	FILE     mnt}
		OUT		{DtmFileOut	FILE     mnt}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{InvmntPanel	000 000 600 460 "Operations on a Grid parameters"

	LABEL           {Label		000 000 200 026 "" " Caraibes Grid to process"}
	FILECHANNELLIST	{DtmFileIn	210 000 340 060 "File name in CARAIBES format at input" ""}

	LABEL	{Label	000 070 200 026 "" " Caraibes Grid result"}
	FILE	{DtmFileOut	210 070 340 026 "File name in CARAIBES format at output" mnt}

	PANEL	{OutputLayersPanel	000 120 590 026 "Output file Options"

		LABEL	{Label		000 000 200 026 "" "Output"}
		OPTION	{OutputOption	210 000 260 026 "" "Output file Options"

			SELECTEDLAYERS      { "Only selected layers"}
			ALLLAYERS           { "All input file layers"}}
		}
	
	PANEL	{AllLayersPanel	000 120 610 026 "Forced option : All input file layers"

		LABEL	{Label	000 000 200 026 "" "Output"}
		LABEL	{label	210 000 400 026 "" "ALL THE LAYERS OF THE INPUT FILE WILL BE COMPUTED"}
		}
	
	PANEL	{DefinedLayersPanel	000 120 610 026 "Les canaux en sortie sont predefinis"

		LABEL          {Label	000 000 200 026 "" "Output"}
		LABEL          {label	210 000 400 026 "" "THE LIST OF LAYERS IN OUTPUT FILE IS PREDEFINED"}
		}

	LABEL	{Label		000 160 200 026 "" "Operation type"}
	OPTION	{OperationType	210 160 250 026 "Type of operation to perform on an input grid"
			REDUCTION		{"Reduction scale"		ReductionModePanel {}
										ReductionStepPanel {}
										AllLayersPanel {}}
			
			EMODNET 		{"Reduction - Emodnet specifications"	DefinedLayersPanel {}
											ReductionStepPanel {}}

			EXTRACTCDI 		{"Extraction of Emodnet CDI prefixes"	AllLayersPanel {}}

			REPLACECDI 		{"Modification of Emodnet CDI"	AllLayersPanel {}
										ReplaceCDIPanel{}}

			INTERPOLATION		{"Interpolation"		InterpolationPanel {} 
										ContouringPanelAll {}
										OutputLayersPanel {}}

			RESET			{"Reset"			ContouringFilePanelOnly {}
										OutputLayersPanel {}}

			LINEARFUNCTION		{"Linear function"		LinearFunctionPanel {} 
										ContouringPanelAll {}
										OutputLayersPanel {}}

			OPPOSITESIGN		{"Opposite sign"		ContouringPanelAll {}
										OutputLayersPanel {}}

			ABSOLUTEVALUE		{"Absolute value"		ContouringPanelAll {}
										OutputLayersPanel {}}

			EXTRACTION		{"Extraction"			ContouringFilePanelOnly {}
										OutputLayersPanel {}}

			NUMERICFILTER		{"Digital filter smoothing"	NumericFilterPanel {} 
										ContouringPanelAll {}
										OutputLayersPanel {}}

			CONSTANTVALUE		{"Constant value"		ConstantValuePanel {} 
										ContouringPanelAll {}
										OutputLayersPanel {}}

			MINMAXFILTER		{"Min-Max filter"		MinMaxFilterPanel {}
										ContouringPanelAll {}
										OutputLayersPanel {}}

			SIGNIFVALUE		{"Significant value"		SignifValuePanel {}
										OutputLayersPanel {}}

			GEOMETRICTRANSLATION	{"Geometric translation"	TranslationPanel {}
										ContouringPanelAll {}
										OutputLayersPanel {}}

			TRUESCALE		{"True scale latitude"		TrueScalePanel {}
										AllLayersPanel {}}

			ORIGINMERIDIAN		{"Origin meridian"		OriginMeridianPanel {}
										AllLayersPanel {}}

			GEODESICSYSTEM		{"Geodesic system"		GeodesicSystemPanel {}
										AllLayersPanel {}}
			}

	PANEL	{ReductionModePanel	000 200 550 030 "Grid reduction method parameter"

		LABEL	{Label		030 000 170 026 "" "Reduction method"}
		OPTION	{ReductionMode	210 000 150 026 "Reduction method"
				SAMPLE	{"Sampling"}
				AVERAGE	{"Average"}
				MIN	{"Minimum"}
				MAX	{"Maximum"}}
		}

	PANEL	{ReductionStepPanel	000 230 550 030 "Grid reduction step parameter"

		LABEL	{Label		030 000 170 026 "" "Reduction step"}
		NUMBER	{ReductionStep	210 000 050 026 "Grid reduction step"}
		LABEL	{Label		270 000 150 026 "" " (lines and columns)"}
		}
	
	PANEL	{InterpolationPanel	000 200 550 030 "Grid interpolation parameters"

		LABEL	{Label			030 000 170 026 "" "Interpolation step"}
		NUMBER	{InterpolationStep	210 000 050 026 "Grid interpolation step"}
		LABEL	{Label			270 000 150 026 "" " (lines and columns)"}
		}

	PANEL	{LinearFunctionPanel	000 200 550 030 "Linear fucntion parameters"

		LABEL	{Label	030 000 170 026 "" "A * Grid + B"}
		LABEL	{Label	210 000 030 026 "" "A ="}
		NUMBER	{A	240 000 080 026 "A coefficient"}
		LABEL	{Label	340 000 040 026 "" ", B ="}
		NUMBER	{B	380 000 080 026 "B coefficient"}
		}

	PANEL	{NumericFilterPanel	000 200 600 150 "Numerical filter parameter"

		LABEL	{Label		030 000 170 026 "" "Smoothing with weighting"}
		OPTION	{Weighting	210 000 220 026 "Smoothing with weighting"
				False	{"No" WindowSizePanel{}}
				Mini	{"Yes / Matrix 3x3" K012_Panel{}}
				True	{"Yes / lines-columns" WindowSizePanel{} WeightingCoefPanel{}}}

		PANEL	{WindowSizePanel	030 030 550 030 "Smoothing window size"

			LABEL	{Label		000 000 175 026 "" "Smoothing window size"}
			OPTION	{WindowSize	180 000 060 026 "Smoothing window size"
					1	{"1" K1_Panel{}}
					2	{"2" K1_Panel{} K2_Panel{}}
					3	{"3" K1_Panel{} K2_Panel{} K3_Panel{}}
					4	{"4" K1_Panel{} K2_Panel{} K3_Panel{} K4_Panel{}}
					5	{"5" K1_Panel{} K2_Panel{} K3_Panel{} K4_Panel{}  K5_Panel{}}
					6	{"6" K1_Panel{} K2_Panel{} K3_Panel{} K4_Panel{}  K5_Panel{} K6_Panel{}}}
			LABEL	{Label		250 000 300 026 "" "DTM cells around the central point"}
			}

		PANEL	{K012_Panel	030 060 400 090 "K0, K1 and K2 coefficients "

			LABEL	{Label	000 000 080 026 "" "Coefficients"}
			LABEL	{Label	140 000 030 026 "" "K0 ="}
			NUMBER	{K0	180 000 050 026 "K0 coefficient"}
			LABEL	{label	240 000 150 026 "" "(point central)"}

			LABEL	{label	140 030 030 026 "" "K1 ="}
			NUMBER	{K1	180 030 050 026 "K1 coefficient"}

			LABEL	{label	260 030 030 026 "" "K2 ="}
			NUMBER	{K2	300 030 050 026 "K2 coefficient"}
			}

		PANEL	{WeightingCoefPanel	030 060 550 090 "Weighting coefficients"

			LABEL	{Label	000 000 080 026 "" "Coefficients"}
			LABEL	{Label	140 000 030 026 "" "K0 ="}
			NUMBER	{K0	180 000 050 026 "K0 coefficient"}
			LABEL	{label	240 000 150 026 "" "(central point)"}

			PANEL	{K1_Panel	140 030 110 030 "K1 coefficient"
				LABEL	{label	000 000 030 026 "" "K1 ="}
				NUMBER	{K1	040 000 050 026 "K1 coefficient"}
				}

			PANEL	{K2_Panel	260 030 110 030 "K2 coefficient"
				LABEL	{label	000 000 030 026 "" "K2 ="}
				NUMBER	{K2	040 000 050 026 "K2 coefficient"}
				}

			PANEL	{K3_Panel	370 030 110 030 "K3 coefficient"
				LABEL	{label	000 000 030 026 "" "K3 ="}
				NUMBER	{K3	040 000 050 026 "K3 coefficient"}
				}

			PANEL	{K4_Panel	140 060 110 030 "K4 coefficient"
				LABEL	{label	000 000 030 026 "" "K4 ="}
				NUMBER	{K4	040 000 050 026 "K4 coefficient"}
				}

			PANEL	{K5_Panel	260 060 110 030 "K5 coefficient"
				LABEL	{label	000 000 030 026 "" "K5 ="}
				NUMBER	{K5	040 000 050 026 "K5 coefficient"}
				}

			PANEL	{K6_Panel	370 060 110 030 "K6 coefficient"
				LABEL	{label	000 000 030 026 "" "K6 ="}
				NUMBER	{K6	040 000 050 026 "K6 coefficient"}
				}
			}
		}

	PANEL	{ConstantValuePanel	000 200 550 060 "Constant value parameter"

		LABEL	{Label		030 000 170 026 "" "Replaced values"}
		OPTION	{ReplacedValues	210 000 260 026 "Data to be replaced by the constant value"
				SIGVALUES    	{"Only significant values"}
				NOSIGVALUES  	{"Only non-significant values"}
				ALLVALUES      {"All data"}}

		LABEL	{Label		030 030 170 026 "" "Constant value"}
		NUMBER	{Constant	210 030 100 026 "Constant value to apply"}
		LABEL	{Label		320 030 170 026 "" "Grid units"}
		}

	PANEL	{MinMaxFilterPanel	000 200 550 060 "Min/Max filter parameters"

		LABEL	{Label		030 000 170 026 "" "Minimum value"}
		NUMBER	{MinValue	210 000 100 026 "Minimum filtered value"}
		LABEL	{Label		320 000 170 026 "" "Grid units"}
		LABEL	{Label		030 030 170 026 "" "Maximum value"}
		NUMBER	{MaxValue	210 030 100 026 "Maximum filtered value"}
		LABEL	{Label		320 030 170 026 "" "Grid units"}
		}

	PANEL	{SignifValuePanel	000 200 550 060 "Significant value replacement parameters"

		LABEL	{Label		030 000 240 026 "" "Significant values replaced by"}
		NUMBER	{SignifValue	280 000 100 026 "Replacement value for significant values"}
		LABEL	{Label		390 000 170 026 "" "Grid units"}
		LABEL	{Label		030 030 240 026 "" "Non-significant values replaced by"}
		NUMBER	{NoSignifValue	280 030 100 026 "Replacement value
 for non-significant values"}
		LABEL	{Label		390 030 170 026 "" "Grid units"}
		}

	PANEL	{TranslationPanel	000 200 550 060 "Geometric translation parameters"

		LABEL	{Label		030 000 240 026 "" "X translation (columns direction)"}
		NUMBER	{TranslationX	280 000 100 026 "Shift of grid data in direction of columns"}
		LABEL	{Label		390 000 170 026 "" "meters"}
		LABEL	{Label		030 030 240 026 "" "Y translation (lines direction)"}
		NUMBER	{TranslationY	280 030 100 026 "Shift of grid data in direction of lines"}
		LABEL	{Label		390 030 170 026 "" "meters"}
		}

	PANEL	{TrueScalePanel	000 200 550 060 "True scale latitude parameters"

		LABEL		{Label		030 000 240 026 "" "New true scale latitude"}
		LABEL		{Label		030 030 500 026 "" "(MERCATOR or EQUDISTANT CYLINDRICAL projection only)"}
		LATITUDE	{NewLatitude	280 000 250 026 "New true scale latitude"}
		}

	PANEL	{OriginMeridianPanel	000 200 550 060 "Origin meridian parameters"

		LABEL		{Label		030 000 240 026 "" "New origin meridian"}
		LABEL		{Label		030 030 500 026 "" "(projection MERCATOR or CYLINDIQUE EQUIDISTANTE only)"}
		LONGITUDE	{NewMeridian	280 000 250 026 "New origin meridian"}
		}

	PANEL	{GeodesicSystemPanel	000 200 580 120 "geodesic system parameters"

		LABEL		{Label		030 000 170 026 "" "New geodesy"}
		ELLIPSOID	{NewEllipsoid	210 000 370 120 "New geodesic system" Conversion}
		}

	PANEL	{ContouringFilePanelOnly	030 200 550 030 ""

		LABEL	{Label			000 000 140 026 "" "Contour file"}
		FILE	{ContouringFileOnly	180 000 340 026 "Contour filename" *}
		}

	PANEL	{ContouringPanelAll	000 360 550 090 ""

		LABEL	{Label		000 000 200 026 "" "Operation limited to a contour"}
		TOGGLE	{Contouring	210 000 030 026 "Part of the grid selection using a contour"
				False	{}
				True	{ContouringFilePanel{} ResetOutOfFramePanel{}}}

		PANEL	{ContouringFilePanel	000 030 550 030 ""

		 	LABEL	{Label		030 000 170 026 "" "Contour file"}
		 	FILE	{ContouringFile	210 000 340 026 "Contour filename" *}
			}

		PANEL	{ResetOutOfFramePanel	000 060 550 030 ""

	 	 	LABEL	{Label			000 000 200 026 "" "Reset of the remaining data"}
		 	TOGGLE	{ResetOutOfFrame	210 000 030 026 "Reset of the grid outside the given contour"
					False	{}
					True	{}}
	 	 	LABEL	{Label	250 000 200 026 "" "(that is no value)"}
			}
		}
		
	PANEL {ReplaceCDIPanel 000 200 550 110 "CDI change parameters"
	
		LABEL	{Label	            030 000 170 026 "" "Modification mode"}
		OPTION	{ReplaceCDIOption   210 000 150 026 "Modification mode"
				SINGLE	{"Single CDI" SingleCdiPanel{}}
				FILE	{"CDI list by file" FileCdiPanel{}}}

		PANEL {SingleCdiPanel 000 040 550 070 "Single CDI change"
			LABEL		{Label   030 000 170 026 "" "CDI to be replaced"}
			TEXTFIELD	{OldCdi  210 000 310 026 "CDI value to be replaced"}
		
			LABEL		{Label   030 040 170 026 "" "New CDI to write"}
			TEXTFIELD	{NewCdi  210 040 310 026 "New CDI value to write"}
		}	

		PANEL {FileCdiPanel 000 040 550 070 "CDI list change"
			LABEL	{Label   	030 000 170 026 "" "CDI modification file"}
			FILE	{CdiChangesFile	210 000 340 026 "File name of CDI changes list" *}
		}	
	}
	
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Invmnt' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Invmnt" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Invmnt' >> ${PROGRESS_FILE}
	}


