# ***************************************************************************
# 
#  TRT : DtmUpd 
#  DOM : Bathymetrie 
#  ROL : Mise a jour du format des fichiers DtmMos
# 
#  CRE : 08/12/2009
#  AUT : PLR
#  VER : @(#)1.1
#        @(#)09/12/09
#
# ***************************************************************************
#  
#  HST : 08/12/2009 PLR  Creation
#        10/03/2010 PLR  Ajout fichiers en sortie
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_Dtmcomp.png}
		HELP		{MNT and Mosaic files format upgrade}
		IN		{DtmMosFiles	FILEGEN		mnt}
		OUT		{OutputFiles 	FILEMANY	mnt}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{DtmUpdPanel	000 000 560 120 "Parameters for MNT and Mosaic files format upgrade"

	LABEL		{Label		000 020 220 026 "" "MNT or Mosaic files to process"}
	FILEGEN		{DtmMosFiles	230 020 320 026 "MNT or Mosaic files to process" *}
	
	LABEL		{Label		000 060 220 026 "" "Output file names"}
	FILEMANY	{OutputFiles	230 060 320 026 "Name of new files to be created" LockGen}

	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Runing DtmUpd' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "DtmUpd" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of DtmUpd' >> ${PROGRESS_FILE}
	}


