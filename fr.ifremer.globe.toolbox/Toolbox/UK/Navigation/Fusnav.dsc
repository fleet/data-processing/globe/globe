# ***************************************************************************
# 
#  TRT : Fusnav 
#  DOM : Navigation 
#  ROL : Fusion de navigations
# 
#  CRE : 16/07/96
#  AUT : TBIG
#  VER : @(#)1.12
#        @(#)04/09/29
# ***************************************************************************
#  
#  HST : 16/07/96 TBIG Creation
#        19/02/97 CEDY Validation
#        27/12/98 JMS  Introduction de la genericite
#        06/01/99 CEDY FICMOD-225: Attributs globaux
#        08/01/99 JFP  FDI-44 : ajout du PIT PERIOD_SELECTION
#        01/04/99 CEDY FICANO-246: extension fichiers entree/sortie
#        01/12/99 CEDY ANO-412: Modifications de labels
#        19/04/04 JL   Modification des extensions nav en nvi
# 
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_NavigationComp.png}
		HELP		{Navigation Merging/Extraction}
		IN		{NavFilesIn	FILEGEN  nvi}
		OUT		{NavFileOut	FILE     nvi}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{FusnavPanel 000 000 600 410 "Navigation merging/extraction parameters"
	
	LABEL	{Label		000 000 230 026 "" " Caraibes navigation files read"}
	FILEGEN	{NavFilesIn	240 000 350 026 "Navigation file names in CARAIBES format at input" nvi}

	LABEL	{Label		000 040 230 026 "" "Caraibes Navigation result file"}
	FILE	{NavFileOut	240 040 350 026 "Navigation file names in CARAIBES format at output" nvi}

	LABEL	{Label		000 080 230 026 "" "Input point sampling"}
	OPTION	{Sampling	240 080 180 026 "No: all points / One point out of n / One point every n seconds"
			NONE		{"No"}
			BYNUMBER	{"1 point out of n" NumberSamplingPanel {}}
			BYTIME		{"1 point / n seconds" TimeSamplingPanel {}}}

	PANEL	{NumberSamplingPanel	050 110 550 30 ""

		LABEL	{Label		000 000 180 026 "" "One point every"}
		NUMBER	{NumberStep	190 000 060 026 "One point out of n input files will be put in output file"}
		LABEL	{Label		260 000 100 026 "" "Points"}
		}

	PANEL	{TimeSamplingPanel	050 110 550 030 ""

		LABEL	{Label		000 000 180 026 "" "One point every"}
		NUMBER	{TimeStep	190 000 060 026 "One point every n seconds of input files will be put in output file"}
		LABEL	{Label		260 000 100 026 "" "Seconds"}
		}

	PERIOD_SELECTION {InterOption	000 150 600 150 "Selection to process"}

	LABEL		{Label		000 310 230 026 "" "Ship name"}
	TEXTFIELD       {ShipName	240 310 350 026 "Full name of the ship which made the survey"}

	LABEL		{Label		000 340 230 026 "" "Cruise name"}
	TEXTFIELD	{SurveyName	240 340 350 026 "Full name of the cruise" }

	LABEL		{Label		000 370 230 026 "" "Reference point description"} 
	TEXTFIELD       {Reference	240 370 350 026 "Reference point description of the ship position"}

	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Fusnav' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Fusnav" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Fusnav' >> ${PROGRESS_FILE}
	}


