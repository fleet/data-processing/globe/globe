# ***************************************************************************
# 
#  TRT : Tnmg77 
#  DOM : Importation 
#  ROL : Traitement d'importation de navigation sous forme ascii
# 
#  CRE : 26/06/96
#  AUT : TBIG
#  VER : @(#)1.2
#        @(#)97/05/26
# ***************************************************************************
#  
#  HST : 26/06/96 TBIG Creation
#        03/03/97 CEDY Validation
#        29/12/98 CEDY FICMOD-218: Formats Shom-Ifremer et genericite
#        03/02/99 CEDY FICMOD-247: Format Simrad/GGA
#        04/06/99 CEDY FICANO-278: Nom de variables V1.x
#        03/11/99 CEDY ANO-412: Modification de labels divers
#        22/12/00 CEDY MOD-470: Importation CINNA/NACOU
#        25/10/01 CEDY MOD-534: Ellipsoide par defaut
#        26/01/04 CE   MOD-639: Format CARIS
#        01/06/04 CE   MOD-659: Format generique
#        18/02/05 CE   Validation v3.1
#        09/04/04 JL   Format TECHSAS / Modification nav en nvi
#        13/05/05 CE   MOD-676: Navigation NAVSM au format NMEA
#        02/11/05 CE   MOD-699: Format NAVSM/TECHSAS et .mbg
#        17/12/06 PM   Ajout option pour import sondeur GeoSwath
#        26/03/07 CR : FDI-2006-053 : Ajout parametres calibration sondeur
#        27/05/09 CE : MOD-820: Format de tirs sismiques
#        14/12/09 CE : MOD-850: Format GAPS
#        12/09/11 PLR  FDI 2011-34 : Ajout de l'import CINNA-NASYn
#        18/06/12 MPC  Mantis 1130 : Ajout de l'import CINNA-NASY3
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_Importation.png}
		HELP		{Import Ascii navigation}
		OUT		{NavFile	FILEMANY	nvi}
		IN		{AsciiFiles	FILEGEN 	*}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{Tnmg77Panel 000 000 600 470 "Ascii navigation importation parameters "

	PANEL   {InterfacesPanel 000 000 600 470 "Ascii navigation importation parameters "	 

		LABEL	{Label        000 000 230 026 "" "Navigation input file(s)"}
		FILEGEN	{AsciiFiles   240 000 340 026 "List of navigation files at input"}

		LABEL	{Label        000 030 230 026 "" "Input files format"}
		OPTION	{FileFormat   240 030 220 026 "Input navigation files format at input"
			 	GENERIC	{"Generic"}
			 	MGD77		{"MGD77"}
			 	MAGMA	  	{"MAGMA"}
			 	TRISMUS	{"TRISMUS" TrismusPanel{}}
				TRINAV		{"TRINAV"}
				TRINAUT	{"TRINAUT"}
				CINNA_NACOU	{"NACOU / NMEA-CINNA"}
			 	CINNA_NASY1	{"NASY1 / NMEA-CINNA"}
			 	CINNA_NASY2	{"NASY2 / NMEA-CINNA"}
			 	CINNA_NASY3	{"NASY3 / NMEA-CINNA"}
                CINNA_NASY4	{"NASY4 / NMEA-CINNA"}
			 	CINNA_NASYX	{"NASYX / NMEA-CINNA"}
			 	CINNA_NAEN1 {"NAEN1 / NMEA-CINNA"}
			 	CINNA_NAEN2 {"NAEN2 / NMEA-CINNA"}		
				TECHSAS_NACOU	{"NACOU / NMEA-TECHSAS"}
				GGA		{"Simrad / GGA"}
				CARIS		{"CARIS / Ascii"}
				NAVSM		{"NAVSM / NMEA" NavsmPanel{}}
				TECHSAS        {"TECHSAS / NetCDF"}
				MBG        	{"MBG" SelectionPanel{}}
                        	RDF            {"GeoSwath-RDF" GeoSwathPanel{}}
                        	SHOTS          {"Seismic shots"}
                        	GAPS		{"GAPS" GAPSPanel{}}}

		PANEL	{TrismusPanel 000 060 550 060 "Ascii navigation imporation parameters"

			LABEL	{Label        040 000 190 026 "" "Date format"}
			OPTION	{DateFormat   240 000 200 026 "Gregorian: day, month, year / Julian: number of day"
					GREGORIEN	{"Gregorian date"}
					JULIEN	  	{"Julian calendar day"}}

			LABEL	{Label        040 030 190 026 "" "Time format"}
			OPTION	{TimeFormat   240 030 200 026 "Hours, minutes, seconds in day / Seconds in day"
					HHMMSS		{"Hours, minutes, seconds"}
					SSSSS		{"Seconds in day"}}
			}

		PANEL	{NavsmPanel 000 060 550 030 "Parameters for NAVSM navigation importation"

			LABEL	{Label		040 000 190 026 "" "Navigation to import"}
			OPTION	{NavsmType	240 000 180 026 "Navigation to import: BUC or dead reckoning"
					RVBC	{"BUC (RVBC)"}
					RVES	{"Dead reckoning (RVES)"}}
			}

		PANEL	{SelectionPanel 040 060 560 030 "Parameters for navigation points selection"

			LABEL	{Label		000 000 190 026 "" "Points sampling"}
			OPTION	{NavSelection	200 000 200 026 "Selection of navigation points read"
			 		No		{"No"}
			 		Sampling	{"1 point / n" Panel_sample{}}
			 		Timing		{"1 point / n seconds" Panel_time{}}}

			PANEL	{Panel_sample	410 000 150 026 ""

	 		 	LABEL	{Label   000 000 020 026 "" "n = "}
	 		 	NUMBER	{Sample  030 000 050 026 "Sampling step"}
				}

	 		PANEL	{Panel_time	410 000 150 026 ""

	 			LABEL	{Label   000 000 020 026 "" "n = "}
	 			NUMBER	{Time    030 000 050 026 "Sampling frequency"}
	 			LABEL	{Label   090 000 060 026 "" "seconds"}
				}
			}

		PANEL	{GAPSPanel 000 060 560 060 "Parameters for GAPS navigation importation"

			LABEL	{Label		040 000 190 026 "" "Navigation to import"}
			OPTION	{GAPSType	240 000 100 026 "Navigation to import: ship or vehicle"
					 Ship		{"Ship"}
					 Vehicle	{"Vehicle" NumberPanel{}}}
		
			PANEL	{NumberPanel	360 000 200 030 "Number of the vehicle "

				LABEL	{Label		000 000 050 026 "" "Number of the vehicle"}
				NUMBER	{VehicleNumber	060 000 050 026 "Vehicle beacon number"}
				LABEL	{Label		120 000 080 026 "" "(1 to 15)"}
				}
			}

		LABEL		{Label		000 140 230 026 "" "Caraibes Navig. output file(s)"}
		FILEMANY	{NavFile	240 140 340 026 "Navigation File(s) in Caraibes format at output" nvi}

		LABEL	{Label        000 170 230 026 "" "Creation or extension of output file"}
		OPTION	{ImportType   240 170 120 026 "Create or stretch  navigation result files (single file only)"
				LENGTHENING	{"Extension" WarningPanel{}}
				CREATION	{"Creation" PanelCreation{}}}

		PANEL	{WarningPanel 370 170 200 030 "Warning if stretching"

			LABEL	{Label        000 000 210 026 "" "(if single file output)"}
			}

		PANEL	{PanelCreation 	000 220 580 220 ""
			LABEL		{Label		000 000 160 026 "" "Default ellipsoid"}
			LABEL		{Label		000 030 160 026 "" "(if missing in input file)"}
			ELLIPSOID	{Ellipsoid	170 000 400 120 "Default ellipsoid"}

			LABEL		{Label		000 130 230 026 "" "Ship name"}
	 		TEXTFIELD	{ShipName	240 130 340 026 "Full name of the ship which made the survey"}

	 		LABEL		{Label		000 160 230 026 "" "Cruise name"}
	 		TEXTFIELD	{SurveyName	240 160 340 026 "Full name of the cruise" }

	 		LABEL		{Label		000 190 230 026 "" "Reference point description"} 
	 		TEXTFIELD	{Reference	240 190 340 026 "Description of navigation reference point" }
			}
                
        	PANEL	{GeoSwathPanel 	000 440 580 160 ""

                	BUTTON  {Button1	080 000 100 030 "Acces navigation importation parameters panel" 
                	Activate {"Interfaces" InterfacesPanel{}}}
    
                	BUTTON  {Button2	400 000 100 030 "Acces projection parameters panel"
                	Activate {"Projection" ProjectionPanel{}}}
			}
		}

	PANEL   {ProjectionPanel 000 030 580 450 "Reference projection"

                	PROJECTION	{Projection	130 000 450 280 "Projection parameters"}
                	BUTTON		{Button1	080 410 100 030 "Acces navigation importation parameters panel" 
                	Activate	{"Interfaces" InterfacesPanel{}}}
    
               		BUTTON		{Button2	400 410 100 030 "Acces projection parameters panel"
               		Activate	{"Projection" ProjectionPanel{}}}
		}

	NUMBER  {HeadingPortCalibrationCorrection	000 000 000 000 "Correction de cap voie babord en degres"}
	NUMBER  {HeadingStarboardCalibrationCorrection	000 000 000 000 "Correction de cap voie tribord en degres"}
	NUMBER  {GPSTimeShift				000 000 000 000 "Ecart de temps entre le GPS et l'acquisition en millisecondes"}
	NUMBER  {AttitudeStationTimeShift		000 000 000 000 "Ecart de temps entre la centrale d'attitude et l'acquisition en millisecondes"}
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of ImpNav' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "ImpNav" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of ImpNav' >> ${PROGRESS_FILE}
	}


