# ***************************************************************************
# 
#  TRT : TGeoS
#  DOM : Importation 
#  ROL : Traitement d'importation des donnees sondeur GeoSwath
# 
#  REM : 
#
#  CRE : 10/11/2006
#  AUT : PM
#  VER : @(#)1.8
#    @(#)09/11/03
# ***************************************************************************
#  
#  HST : 10/11/2006 PM : Creation
#	 25/01/2007 CR : Reprise suite modification specifications
#        21/03/2007 CR : FDI-2006-053 : Modification libelles decalages
#        26/03/2007 CR : FDI-2006-053 : Ajout parametres calibration sondeur
#        30/03/2007 CR : Petibato : modification libelles
#        04/04/2007 CR : Petibato : modification libelles
#        16/10/2008 JMS: Petibato : offrir une option avec une largeur de 
#                        fauch�e �gale � 5 fois la profondeur
#        20/04/2009 PLR: Suppression option d echantillonnage des faisceaux
#                        Ajout de l'option d'import de l'imagerie
#        14/09/2009 PLR: FDI 2009-012 : Import bathy optionnel
#        21/01/2010 PLR: FDI 2009-023 : Correction de gain d'antenne
#        02/11/2010 JMS: Largeur de fauch�e � 7
#        26/01/2011 MPC: ANO-155: mise � jour d'un mum�ro d'antenne
#        26/04/2011 PLR: FDI 2011-23 : Options de filtrage des echantillons
#	 03/12/2012 PLR: FDI 2012-20 : Filtrage sur angle d'incidence + complement flags qualite
#	 06/12/2012 PLR: FDI 2012-20 : Ajout borne min pour le filtre sur angle d'incidence
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION {
        BITMAP      {CIB_PIT_Importation.png}
        HELP        {Importation sondeur GeoSwath}
        IN          {SounderFiles   FILEGEN     *}
	OUT         {ImageFile		FILEMANY	sni}
	OUT         {VelocityFile	FILE		vel}
	OUT         {NavFile		FILEMANY	nvi}
        OUT         {BeamFile   	FILEMANY	mbg}
        	}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL   {TGeoSPanel 000 000 660 600 "GeoSwath sounder importation parameters"
    
    NUMBER  {PortAntennaAngle   000 000 000 000 "Blank parameter to define the sounder port antenna angle"}
    NUMBER  {StarboardAntennaAngle   000 000 000 000 "Blank parameter to define the sounder starboard antenna angle"}
    
    PANEL   {InterfacesPanel 000 010 660 560 "Import parameter of sounder GeoSwath"

        LABEL   {Label          000 000 220 026 "" "Origin of file(s)"}
        OPTION  {FormatType     230 000 200 026 "Format type of files at input"
                GeoSwath    {"GeoSwath - RDF"}}

        LABEL   {Label      	000 040 220 026 "" "RDF input files"}
        FILEGEN {SounderFiles   230 040 340 026 "GeoSwath-RDF sounder data file names at input"}

        LABEL       {Label      000 080 220 026 "" "Ship name"}
        TEXTFIELD   {ShipName   230 080 350 026 "Full name of ship which made the survey"}

        LABEL       {Label      000 110 220 026 "" "cruise name"}
        TEXTFIELD   {SurveyName 230 110 350 026 "Full cruise name" }

        LABEL       {Label      000 140 220 026 "" "Reference point description"} 
        TEXTFIELD   {Reference  230 140 350 026 "Description of navigation reference point" }

	PANEL   {PanelComputing 020 200 630 340 "Computing parameters"
	
            LABEL   {Label          000 000 500 026 "" "Shift between motion reference unit and port transducer (meters)"}
            LABEL   {Label          020 030 060 026 "" "Along"}
            NUMBER  {XPortAntenna   090 030 050 026 "Along shift in meters"}
            LABEL   {Label          140 030 080 026 "" "(+ ahead)"}

            LABEL   {Label          230 030 050 026 "" "Across"}
            NUMBER  {YPortAntenna   290 030 050 026 "Across shift in meters"}
            LABEL   {Label          340 030 080 026 "" "(+ starboard)"}

            LABEL   {Label          440 030 060 026 "" "Vertical"}
            NUMBER  {ZPortAntenna   500 030 050 026 "Vertical shift in meters"}
            LABEL   {Label          550 030 080 026 "" "(+ below)"}
        
	    LABEL   {Label          000 070 500 026 "" "Shift between motion reference unit and starboard transducer (meters)"}
            LABEL   {Label          020 100 060 026 "" "Along"}
            NUMBER  {XStbAntenna    090 100 050 026 "Along shift in meters"}
            LABEL   {Label          140 100 080 026 "" "(+ ahead)"}

            LABEL   {Label          230 100 060 026 "" "Across"}
            NUMBER  {YStbAntenna    290 100 050 026 "Across shift in meters"}
            LABEL   {Label          340 100 080 026 "" "(+ starboard)"}

            LABEL   {Label          440 100 060 026 "" "Vertical"}
            NUMBER  {ZStbAntenna    500 100 050 026 "Vertical shift in meters"}
            LABEL   {Label          550 100 080 026 "" "(+ below)"}
        
            LABEL   {Label          000 140 300 026 "" "Motion reference unit height % waterline (meters)"}
            NUMBER  {Immersion      340 140 050 026 "Z shift between navigation-attitude station and waterline"}
            LABEL   {Label          390 140 080 026 "" "(+ below)"}

            LABEL   {Label          000 180 200 026 "" "Heading correction (degrees)"}
            LABEL   {Label          230 180 060 026 "" "Port"}
	    NUMBER  {HeadingPortCalibrationCorrection   290 180 050 026 "Heading port side correction in degrees"}
            LABEL   {Label          340 180 080 026 "" "(+ starboard)"}

	    LABEL   {Label          440 180 060 026 "" "Starboard"}
	    NUMBER  {HeadingStarboardCalibrationCorrection   500 180 050 026 "Heading starboard side correction in degrees"}
            LABEL   {Label          550 180 080 026 "" "(+ starboard)"}

            LABEL   {Label          000 210 200 026 "" "Roll correction (degrees)"}
            LABEL   {Label          230 210 060 026 "" "Port"}
	    NUMBER  {RollPortCalibrationCorrection   290 210 050 026 "Roll port side correction in degrees"}
            LABEL   {Label          340 210 080 026 "" "(+ starboard)"}

	    LABEL   {Label          440 210 060 026 "" "Starboard"}
	    NUMBER  {RollStarboardCalibrationCorrection   500 210 050 026 "Roll starboard side correction in degrees"}
            LABEL   {Label          550 210 080 026 "" "(+ starboard)"}

            LABEL   {Label          000 240 200 026 "" "Pitch correction (degrees)"}
            LABEL   {Label          230 240 060 026 "" "Port"}
	    NUMBER  {PitchPortCalibrationCorrection   290 240 050 026 "Pitch portside correction in degrees"}
            LABEL   {Label          340 240 100 026 "" "(+ below)"}

	    LABEL   {Label          440 240 060 026 "" "Starboard"}
	    NUMBER  {PitchStarboardCalibrationCorrection   500 240 050 026 "Pitch starboard side correction in degrees"}
            LABEL   {Label          550 240 100 026 "" "(+ below)"}

            LABEL   {Label          000 280 300 026 "" "Delay between GPS and acquisition"}
	    NUMBER  {GPSTimeShift   300 280 050 026 "" "Delay between acquisition in milliseconds"}
            LABEL   {Label          360 280 130 026 "" "milliseconds"}

	    LABEL   {Label          000 310 300 026 "" "Delay between attitude station and acquisition"}
	    NUMBER  {AttitudeStationTimeShift   300 310 050 026 "Delay between attitude station and acquisition in milliseconds"}
            LABEL   {Label          360 310 130 026 "" "milliseconds"}
	}
 
            FRAME   {Frame          000 180 660 370 "" "Computing parameters"}
        }   

    PANEL   {BathyImageryPanel 000 010 660 560 "Bathymetry/Imagery import parameters"

	LABEL	{Label               000 000 280 026 "" "Bathymetry importation into mbg format"}
	OPTION	{BathymetryImport    280 000 080 026 "Bathymetry importation option"
			Yes	{"Yes"  Panel_BatFiles{}}
			No	{"No"}}

	PANEL	{Panel_BatFiles	040 030 620 030 ""
		LABEL       {Label      000 000 225 026 "" "Caraibes bathymetry output files"}
		FILEMANY    {BeamFile  	240 000 340 026 "Multibeam file name(s) in Caraibes format at output" mbg} 
		}
		
	PANEL	{Panel_BatImaOptions	040 100 620 260 ""
        
        	PANEL   {PanelSelBeam   000 000 680 026 ""
            
        		LABEL           {Label         000 000 225 026 "" "Beam selection"}
        		OPTION     	{FilterType    240 000 190 026 "Beam selection"
				No      {"No"}
				Yes     {"Yes" Panel_sample{}}}

                	PANEL   {Panel_sample   460 000 280 026 ""

                    		LABEL           {Label       000 000 020 026 "" "n = "}
                    		NUMBER      	{BeamNumber  030 000 050 026 "beam number"}
                    		LABEL           {Label       090 000 160 026 "" "beams"}
                    		}
            		}
        
        	PANEL   {Panel_Beam 000 040 680 026 ""
            
			LABEL       {Label         000 000 230 026 "" "Ping sampling"}
			OPTION      {BeamSelection 240 000 190 026 "Ping selection"
				No      	{"No"}
				Sampling    {"1 point / n" Panel_sample{}}
				Timing      {"1 point / n seconds" Panel_time{}}}

			PANEL   {Panel_sample   460 000 100 026 ""

				LABEL           {Label       000 000 020 026 "" "n = "}
				NUMBER      	{BeamSample  030 000 050 026 "Sampling step"}
				}

			PANEL   {Panel_time 460 000 250 026 ""

				LABEL           {Label   	 000 000 020 026 "" "n = "}
				NUMBER      	{BeamTime    030 000 050 026 "Sampling frequency"}
				LABEL           {Label   	 090 000 160 026 "" "seconds"}
				} 
			}
        
			PANEL   {Panel_Beam 000 080 580 070 ""
            
				LABEL       {Label          000 000 120 026 "" "Depth (min)"}
				NUMBER      {DepthMin       120 000 050 026 "Minimum depth in meters"}
				LABEL       {Label          180 000 050 026 "" "meters"}

				LABEL       {Label          240 000 120 026 "" "Depth (max)"}
				NUMBER      {DepthMax       380 000 050 026 "Maximum depth in meters"}
				LABEL       {Label          440 000 080 026 "" "meters"}
        
				LABEL       {Label             000 040 120 026 "" "Amplitude filter"}
				NUMBER      {AmplitudeFilter   120 040 050 026 "Remove the n% weakest sample"}
				LABEL       {Label             180 040 050 026 "" "%"}
                
				LABEL       {Label      240 040 120 026 "" "Beam width"}
				OPTION      {BeamFilterSize 380 040 050 026 ""
					2   {"2"}
					4   {"4"}
					5   {"5"}
					6   {"6"}
					7   {"7"}
					8   {"8"}
					10  {"10"}
					12  {"12"}}
					LABEL       {Label          440 040 120 026 "" "X Depth"}
			}
				
			LABEL       {Label             	000 160 120 026 "" "Incidence min"}
			NUMBER      {IncidenceMin   	120 160 050 026 "Lower bound for incidence angle in degrees"}
			LABEL       {Label          	180 160 050 026 "" "degrees"}
			
			LABEL       {Label             	240 160 120 026 "" "Incidence max"}
			NUMBER      {IncidenceMax   	380 160 050 026 "Higher bound for incidence angle in degrees"}
			LABEL       {Label          	440 160 050 026 "" "degrees"}
				
			LABEL       {Label             	000 200 180 026 "Filtering on GS+ quality filter value" "Quality factor filter"}
	
			LABEL	    {Label		240 200 080 026 "" "Water column"}
	                TOGGLE	    {WaterColumn	330 200 020 026 "Water column filter"
	 	 			False	{}
	 	 			True	{}}
	
			LABEL	    {Label		380 200 040 026 "" "Limits"}
	                TOGGLE	    {Limits		420 200 020 026 "Limits"
	 	 			False	{}
	 	 			True	{}}
	
			LABEL	    {Label		460 200 120 026 "" "Group/bottom track"}
	                TOGGLE	    {GroupBottom        580 200 020 026 "Group/bottom track"
	 	 			False	{}
	 	 			True	{}}
	
			LABEL	    {Label		240 230 080 026 "" "Amplitude"}
	                TOGGLE	    {Amplitude	        330 230 020 026 "Amplitude"
	 	 			False	{}
	 	 			True	{}}
	
			LABEL	    {Label		380 230 040 026 "" "Across"}
	                TOGGLE	    {Across		420 230 020 026 "Across"
	 	 			False	{}
	 	 			True	{}}
	
			LABEL	    {Label		460 230 120 026 "" "Along"}
	                TOGGLE	    {Along		580 230 020 026 "Along"
	 	 			False	{}
	 	 			True	{}}
		}
  
		        LABEL   {Label          000 370 280 026 "" "Imagery importation into sni format"}
		        OPTION  {ImageryImport  280 370 080 026 "Imagery importation"
		                Yes {"Yes" Panel_ImaFiles{} Panel_GainCorrection{}}
		                No  {"No"}}
		
		        PANEL   {Panel_ImaFiles 040 410 650 030 ""
		            	LABEL       {Label      000 000 180 026 "" "Imagery output files"}
		            	FILEMANY    {ImageFile  240 000 340 026 "Imagery file names at output" sni}
		            	}
        
 			PANEL   {Panel_GainCorrection 040 450 680 100 ""
				LABEL	{Label		000 000 225 026 "" "Antenna gain correction"}
				OPTION	{GainCorOption	240 000 080 026 "Antenna gain correction option"
					No      {"No"}
					Yes     {"Yes" Panel_Antenna{}}}

				PANEL   {Panel_Antenna 040 030 500 060 ""
					LABEL		{Label	000 000 180 026 "" "Port antenna Nb "}
					OPTION	{PrtAntNb	200 000 080 026 "Port antenna number"
						17823	{"17823"}
						17849	{"17849"}
						19401	{"19401"}}
					LABEL		{Label	000 030 180 026 "" "Starboard antenna Nb "}
					OPTION	{StbAntNb	200 030 080 026 "Starboard antenna number"
						17823	{"17823"}
						17849	{"17849"}
						19401	{"19401"}}
					} 
			}
        }   
    

    PANEL   {NavigationPanel 000 010 660 490 "Navigation importation parameters"

        LABEL   {Label      000 000 180 026 "" "Navigation importation"}
        OPTION  {NavImport  190 000 080 026 "Navigation transfer"
                Yes {"Yes" Navigation{}}
                No  {"No"}}
	LABEL   {Label      275 000 400 026 "" "(necessary to save the positions into mbg files)"}

        PANEL   {Navigation 000 040 650 450 "Navigation importation parameters"

            LABEL       {Label      000 000 180 026 "" "Caraibes navigation output files"}
            FILEMANY    {NavFile    190 000 340 026 "Navigation file names at output" nvi}
            
            LABEL   {Label      	000 040 180 026 "" "Points sampling"}
            OPTION  {NavSelection   190 040 190 026 "Selection of navigation points read"
                    No      	{"No"}
                    Sampling    {"1 point / n" Panel_sample{}}
                    Timing      {"1 point / n seconds" Panel_time{}}}

            PANEL   {Panel_sample   400 040 100 026 ""

                LABEL   {Label      000 000 020 026 "" "n = "}
                NUMBER  {NavSample  030 000 050 026 "Sampling step"}
                }

            PANEL   {Panel_time 400 040 200 026 ""

                LABEL   {Label      000 000 020 026 "" "n = "}
                NUMBER  {NavTime    030 000 050 026 "Sampling frequency"}
                LABEL   {Label      090 000 110 026 "" "secondes"}
                }

            LABEL       {Label      000 080 180 026 "" "Ellipsoid for output file"}
            ELLIPSOID   {Ellipsoid  190 080 390 120 "Ellipsoid for output file"}

            LABEL       {Label      000 200 180 026 "" "Reference projection"}
            PROJECTION  {Projection 190 200 455 280 "Projection parameters"}
            }
        }
        
    PANEL   {VelocityPanel 000 010 650 400 "Velocity importation parameters"

        LABEL   {Label          	000 010 180 026 "" "Velocity importation"}
        OPTION  {VelocityExtraction 190 010 080 026 "Velocity transfer"
                Yes {"Yes" Velocity{}}
                No  {"No"}}
        PANEL   {Velocity 000 050 650 450 "Velocity importation parameters"

            LABEL       {Label          000 050 270 026 "" "Velocity GS+ data file names at input "}
            FILEGEN     {GSplusFile     280 050 350 026 "Velocity file name(s) at input"}
     
            LABEL       {Label          000 100 270 026 "" "Velocity profile file at output"}
            FILE        {VelocityFile   280 100 350 026 "Velocity profile filename"}
            }
        }

    BUTTON  {Button1 052 570 100 030 "Access interfaces parameters panel" 
        Activate {"Interfaces" InterfacesPanel{}}}
     
    BUTTON  {Button2  204 570 100 030 "Access bathymetryImagery importation parameters panel" 
        Activate {"Bathy/Imagery" BathyImageryPanel{}}}

    BUTTON  {Button3 356 570 100 030 "Acces navigation importation parameters panel"
        Activate {"Navigation" NavigationPanel{}}}
        
    BUTTON  {Button4 508 570 100 030 "Acces celerity importation parameters panel"
        Activate {"Velocity" VelocityPanel{}}}
    }

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of TGeoS' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "TGeoS" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of TGeoS' >> ${PROGRESS_FILE}
	}
