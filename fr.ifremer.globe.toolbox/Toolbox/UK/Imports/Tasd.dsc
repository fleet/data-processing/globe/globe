# ***************************************************************************
# 
#  TRT : Tasd
#  DOM : Importation 
#  ROL : Traitement d'importation des fichiers ASD
# 
#  REM : 
#
#  CRE : 10/06/2009
#  AUT : PLR
#  VER : %Z%%I%
#        %Z%%E%
# ***************************************************************************
#  
#  HST : 10/06/2009 PLR : Creation
#        07/07/2009 PLR : Ajout import fichier SBD
#        28/07/2009 PLR : Ajout option profil de celerite
#        28/01/2010 PLR : FDI-2010-001 ajout importation des SNI
#        09/02/2010 PLR : Ajout type de sondeur si fichier de celerite
#        11/02/2010 PLR : FDI 2010-003 : Prise en compte des parametres d'installation
#        06/09/2010 CE :  ANO-1136 : Type de sondeur Hydrosweep MD/DS
#        13/04/2012 PLR : Ajout d'un caractere espace manquant
#        17/01/2013 PLR : FDI 2013-001 : Ajout du sondeur Fansweep
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION {
        BITMAP      {CIB_PIT_Importation.png}
        HELP        {ASD and SBD files import}
        IN          {SounderFiles   FILEGEN     *}
	OUT         {ImageFile		FILEMANY	sni}
        OUT         {BeamFile   	FILEMANY	mbg}
        	}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL   {TasdPanel 000 000 650 580 "ASD ans SBD bathymetry files importation parameters"
    
    PANEL   {InterfacesPanel 000 010 650 520 "Import parameter of sounder Hydrosweep"
            
        LABEL       {Label         000 010 220 026 "" "Input files format"}
        OPTION      {ImportType    230 010 180 026 "Input files format ASD or SBD"
                    ASD      	{"ASD / Atlas" Panel_Imagery{}}
                    SBD         {"SBD / Eiva" Panel_Proj{}} }

        LABEL   {Label      	000 050 220 026 "" "ASD/SBD input files"}
        FILEGEN {SounderFiles   230 050 350 026 "ASD/SBD data file names at input"}

        LABEL       {Label             000 090 220 026 "" "Sounder type"}
        OPTION      {SounderType       230 090 190 026 "Sounder type, Hydrosweep deep/swallow water or Fansweep"
                    MD       {"Hydrosweep MD / 5m-2500m"}
                    DS       {"Hydrosweep DS / 10m-11000m"}
                    FS       {"Fansweep" OffsetAntenna2{}}}

		LABEL       {Label      000 130 225 026 "" "Caraibes bathymetry output files"}
        FILEMANY    {BeamFile   230 130 350 026 "Multibeam file name(s) in Caraibes format at output" mbg}   
   
	PANEL   {Panel_Imagery   000 170 650 070 "Imagery import parameters"
		LABEL   {Label          000 000 220 026 "" "Imagery importation into sni format"}
		OPTION  {ImageryImport  230 000 080 026 "Imagery importation"
			Yes {"Yes" Panel_ImaFiles{}}
			No  {"No"}}

		PANEL   {Panel_ImaFiles 000 040 650 030 ""
			LABEL       {Label      050 000 170 026 "" "Imagery output files"}
			FILEMANY    {ImageFile  230 000 350 026 "Imagery file names at output" sni}
			}
		}
       
        PANEL   {Panel_Beam 000 260 680 026 ""
            
        LABEL       {Label         000 000 230 026 "" "Ping sampling"}
        OPTION      {BeamSelection 230 000 180 026 "Ping selection"
                    No      	{"No"}
                    Sampling    {"1 point / n" Panel_sample{}}
                    Timing      {"1 point / n seconds" Panel_time{}}}

        PANEL   {Panel_sample   450 000 100 026 ""

                LABEL           {Label       000 000 020 026 "" "n = "}
                NUMBER      	{BeamSample  030 000 050 026 "Sampling step"}
            }

        PANEL   {Panel_time 450 000 250 026 ""

                LABEL           {Label       000 000 020 026 "" "n = "}
                NUMBER      	{BeamTime    030 000 050 026 "Sampling frequency"}
                LABEL           {Label       090 000 160 026 "" "seconds"}
            } 
        }
            
        LABEL       {Label          000 300 220 026 "" "Use an input velocity file"}
        OPTION      {VelocityOption 230 300 080 026 "Use an input velocity file"
                    No      	{"No"}
                    Yes         {"Yes" Panel_velocity{}}}

        PANEL   {Panel_velocity   000 340 580 060 ""
                 LABEL       {Label             050 000 170 026 "" "Input velocity file name"}
                 FILE        {VelocityFileName  230 000 350 026 "Input velocity file name"}
            }
        
    PANEL   {Panel_Shift    000 400 580 100 ""

            LABEL       {Label      000 000 220 026 "" "Ship name"}
            TEXTFIELD   {ShipName   230 000 350 026 "Full ship name used for the survey"}

			LABEL       {Label      000 030 220 026 "" "Cruise name"}
			TEXTFIELD   {SurveyName 230 030 350 026 "Full cruise name" }

			LABEL       {Label      000 060 220 026 "" "Reference point description"} 
			TEXTFIELD   {Reference  230 060 350 026 "Description of navigation reference point" }
			}
		}
      
    PANEL	{InstallationPanel 000 000 650 530 "Installation parameters"
	
		LABEL   {Label	000 000 520 026 "" "Navigation reference point coordinates % ship reference point (*)"}
		LABEL   {Label	040 026 060 026 "" "Nx : "}
		NUMBER  {XNav	110 026 050 026 "Across shift in meters"}
		LABEL   {Label	170 026 130 026 "" "meters (+ port)"}

		LABEL   {Label	040 052 060 026 "" "Ny : "}
		NUMBER  {YNav	110 052 050 026 "Along shift in meters"}
		LABEL   {Label	170 052 130 026 "" "meters (+ ahead)"}

		LABEL   {Label	040 078 060 026 "" "Nz : "}
		NUMBER  {ZNav	110 078 050 026 "Vertical shift in meters"}
		LABEL   {Label	170 078 130 026 "" "meters (+ below)"}

		LABEL   {Label  000 120 520 026 "" "Attitude station reference point coordinates % ship reference point (*)"}

		PANEL	{AttitudeOffsetPosition	040 146 260 090 ""
		
			LABEL   {Label	000 000 060 026 "" "Cx : "}
			NUMBER  {XAtt	070 000 050 026 "Across shift in meters"}
			LABEL   {Label	130 000 130 026 "" "meters (+ port)"}

			LABEL   {Label	000 026 060 026 "" "Cy : "}
			NUMBER  {YAtt	070 026 050 026 "Along shift in meters"}
			LABEL   {Label	130 026 130 026 "" "meters (+ ahead)"}

			LABEL   {Label	000 052 060 026 "" "Cz : "}
			NUMBER  {ZAtt	070 052 050 026 "Vertical shift in meters"}
			LABEL   {Label	130 052 130 026 "" "meters (+ below)"}
			}

		PANEL	{AttitudeOffsetOrientation	310 146 650 090 ""
		
			LABEL   {Label		000 000 050 026 "" "Offsets"}
			LABEL   {Label		060 000 070 026 "" "Heading :"}
			NUMBER  {HeadingAtt	140 000 050 026 "Heading offset in degrees"}
			LABEL   {Label		200 000 140 026 "" "deg. (+ clockwise)"}

			LABEL   {Label		060 026 070 026 "" "Roll :"}
			NUMBER  {RollAtt	140 026 050 026 "Roll offset in degrees"}
			LABEL   {Label		200 026 140 026 "" "deg. (+ port up)"}

			LABEL   {Label		060 052 070 026 "" "Pitch :"}
			NUMBER  {PitchAtt	140 052 050 026 "Pitch offset in degrees"}
			LABEL   {Label		200 052 140 026 "" "deg. (+ bow up)"}
			}

		LABEL   {Label	000 240 450 026 "" "Sounder antenna 1 coordinates % ship reference point (*)"}

		PANEL	{PortOffsetPosition	040 266 260 090 ""
		
			LABEL   {Label          000 000 060 026 "" "Sx : "}
			NUMBER  {XAntenna	070 000 050 026 "Across shift in meters"}
			LABEL   {Label          130 000 130 026 "" "meters (+ port)"}

			LABEL   {Label          000 026 060 026 "" "Sy : "}
			NUMBER  {YAntenna	070 026 050 026 "Along shift in meters"}
			LABEL   {Label          130 026 130 026 "" "meters (+ ahead)"}

			LABEL   {Label          000 052 060 026 "" "Sz : "}
			NUMBER  {ZAntenna	070 052 050 026 "Vertical shift in meters"}
			LABEL   {Label          130 052 130 026 "" "meters (+ below)"}
			}
      
		PANEL	{AntOffsetOrientation	310 266 650 090 ""
		
			LABEL   {Label		000 000 050 026 "" "Offsets"}
			LABEL   {Label		060 000 070 026 "" "Heading :"}
			NUMBER  {HeadingAntenna	140 000 050 026 "Heading offset din degrees"}
			LABEL   {Label		200 000 140 026 "" "deg. (+ clockwise)"}

			LABEL   {Label		060 026 070 026 "" "Roll :"}
			NUMBER  {RollAntenna	140 026 050 026 "Roll offset in degrees"}
			LABEL   {Label		200 026 140 026 "" "deg. (+ port up)"}

			LABEL   {Label		060 052 070 026 "" "Pitch :"}
			NUMBER  {PitchAntenna	140 052 050 026 "Pitch offset in degrees"}
			LABEL   {Label		200 052 140 026 "" "deg. (+ bow up)"}
			}

		PANEL	{OffsetAntenna2	000 360 650 110 ""
	
			LABEL   {Label	000 000 450 026 "" "Sounder antenna 2 coordinates % ship reference point (*)"}
	
			PANEL	{PortOffsetPosition	040 026 260 090 ""
			
				LABEL   {Label          000 000 060 026 "" "Sx : "}
				NUMBER  {XAntenna2	070 000 050 026 "Across shift in meters"}
				LABEL   {Label          130 000 130 026 "" "meters (+ port)"}
	
				LABEL   {Label          000 026 060 026 "" "Sy : "}
				NUMBER  {YAntenna2	070 026 050 026 "Along shift in meters"}
				LABEL   {Label          130 026 130 026 "" "meters (+ ahead)"}
	
				LABEL   {Label          000 052 060 026 "" "Sz : "}
				NUMBER  {ZAntenna2	070 052 050 026 "Vertical shift in meters"}
				LABEL   {Label          130 052 130 026 "" "meters (+ below)"}
				}
	      
			PANEL	{AntOffsetOrientation	310 026 650 090 ""
			
				LABEL   {Label			000 000 050 026 "" "Offsets"}
				LABEL   {Label			060 000 070 026 "" "Heading :"}
				NUMBER  {HeadingAntenna2	140 000 050 026 "Heading offset din degrees"}
				LABEL   {Label			200 000 140 026 "" "deg. (+ clockwise)"}
	
				LABEL   {Label			060 026 070 026 "" "Roll :"}
				NUMBER  {RollAntenna2		140 026 050 026 "Roll offset in degrees"}
				LABEL   {Label			200 026 140 026 "" "deg. (+ port up)"}
	
				LABEL   {Label			060 052 070 026 "" "Pitch :"}
				NUMBER  {PitchAntenna2		140 052 050 026 "Pitch offset in degrees"}
				LABEL   {Label			200 052 140 026 "" "deg. (+ bow up)"}
				}
			}

		LABEL   {Label          000 470 400 026 "" "(*) The ship reference point is on the waterline"}	
                    
	        LABEL   {Label		000 500 250 026 "" "Time delay between navigation and sounder"}
		NUMBER  {NavTimeShift   250 500 050 026 "Delay between navigation and sounder acquisition in milliseconds"}
	        LABEL   {Label          310 500 130 026 "" "milliseconds"}
		}

    PANEL   {NavigationPanel 000 010 650 520 "Navigation importation parameters"

        LABEL   {Label      000 000 180 026 "" "Navigation importation"}
        OPTION  {NavImport  190 000 080 026 "Navigation transfer"
                Yes {"Yes" Navigation{}}
                No  {"No"}}

        PANEL   {Navigation 000 050 650 490 "Navigation importation parameters"

            LABEL       {Label      000 000 180 026 "" "Caraibes navigation output files"}
            FILEMANY    {NavFile    190 000 340 026 "Navigation file names at output" nvi}
            
            LABEL   {Label          000 050 180 026 "" "Points sampling"}
            OPTION  {NavSelection   190 050 190 026 "Selection of navigation points read"
                    No      	{"No"}
                    Sampling    {"1 point / n" Panel_sample{}}
                    Timing      {"1 point / n seconds" Panel_time{}}}

            PANEL   {Panel_sample   400 050 100 026 ""

                LABEL   {Label      000 000 020 026 "" "n = "}
                NUMBER  {NavSample  030 000 050 026 "Sampling step"}
                }

            PANEL   {Panel_time 400 050 200 026 ""

                LABEL   {Label      000 000 020 026 "" "n = "}
                NUMBER  {NavTime    030 000 050 026 "Sampling frequency"}
                LABEL   {Label      090 000 110 026 "" "secondes"}
                }

            LABEL       {Label      000 100 180 026 "" "Ellipsoid for output file"}
            ELLIPSOID   {Ellipsoid  190 100 390 120 "Ellipsoid for output file"}

            PANEL   {Panel_Proj   000 220 650 280 ""
				LABEL       {Label      000 000 180 026 "" "Reference projection"}
				PROJECTION  {Projection 190 000 455 280 "Projection parameters used if the reference navigation is NaviPac"}
				LABEL       {Label      000 040 189 026 "" "Used if navigation = NaviPac"}
				}
            }
        }

    BUTTON  {Button1 050 550 150 030 "Access interfaces parameters panel" 
        Activate {"Interfaces" InterfacesPanel{}}}

    BUTTON  {Button2 250 550 150 030 "Acces installation parameters panel"
	    Activate {"Ship parameters" InstallationPanel{}}}

    BUTTON  {Button3 450 550 150 030 "Acces navigation importation parameters panel"
        Activate {"Navigation" NavigationPanel{}}}
}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Tasd' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Tasd" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Tasd' >> ${PROGRESS_FILE}
	}
