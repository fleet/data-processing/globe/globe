# ***************************************************************************
# 
#  TRT : Ts2100
#  DOM : Importation 
#  ROL : Traitement d'importation des donnees sondeur Seabeam 2100
# 
#  REM : Le format du sondeur EM1002 pris en compte dans CARAIBES :
#
#    - format MB
#
#  CRE : 14/06/99
#  AUT : CEDY
#  VER : @(#)1.1
#        @(#)05/10/17
# ***************************************************************************
#  
#  HST : 14/06/99 CEDY FICMOD-231: Creation
#        02/10/00 JMS  MOD-442: Importation SEABEAM 2100 format MB-43.
#        03/11/99 CEDY ANO-412: Modification de labels divers
#        06/11/01 CEDY MOD-529: Ellipsoide
#        30/01/02 CEDY ANO-664: Validation v2.4
#        19/04/04 JL   Modification des extensions nav par nvi
#
# ***************************************************************************

# ---------------------------------------------------------------------------
# DESCRIPTION
# ---------------------------------------------------------------------------
DESCRIPTION	{
		BITMAP 		{CIB_PIT_Importation.png}
		HELP		{Import 2100 / Seabeam sounder}
		IN		{SounderFiles	FILEGEN		*}
		OUT		{BeamFile	FILEMANY	mul}
		}

# ---------------------------------------------------------------------------
# PANNEAU DE SAISIE DES PARAMETRES
# ---------------------------------------------------------------------------
PANEL	{Tm2100Panel	000 000 620 500 "Seabeam-2100 sounder imporation parameters"
	 
	PANEL	{BathymetryPanel	000 010 620 400 "Bathymetry imporation parameters"

		LABEL	{Label        	000 000 220 026 "" "Origin of file(s)"}
		OPTION	{FormatType   	230 000 140 026 "Format type of files at input"
			 	MB_system_41 	{"MB system 41"}
			 	MB_system_43 	{"MB system 43"}}

		LABEL	{Label        	000 040 220 026 "" "Seabeam-2100 input files"}
		FILEGEN	{SounderFiles 	230 040 350 026 "Seabeam-2100 sounder data file names at input"}

		LABEL		{Label        	000 080 220 026 "" "Caraibes Bathymetry output files"}
		FILEMANY	{BeamFile     	230 080 350 026 "Multibeam file name in Caraibes format at output" mul}

		LABEL	{Label        	000 120 220 026 "" "Creation or extension of output file"}
		OPTION	{ImportType   	230 120 120 026 "Create or stretch bathymetry and navigation result files (single file only)"
				 Creation	{"Creation" Panel_Shift{} Panel_Ellipsoid{}}
			 	Lengthening 	{"Extension"}}

		LABEL	{Label        000 160 220 026 "" "Navigation importation"}
		OPTION	{NavType      230 160 080 026 "Navigation transfer"
				 Yes	{"Yes" Button2{}}
				 No	{"No"}}

		PANEL	{Panel_Shift 	000 200 580 190 ""

			LABEL		{Label        	000 000 400 026 "" "Shift between sounder antenna and navigation reference"}
			LABEL		{Label        	040 030 070 026 "" "Along"}
			NUMBER		{LongOffset   	120 030 050 026 "Along shift in meters"}
			LABEL		{Label        	180 030 130 026 "" "meters (+ ahead)"}

			LABEL		{Label        	320 030 070 026 "" "Across"}
			NUMBER		{TransOffset  	400 030 050 026 "Across shift in meters"}
			LABEL		{Label        	460 030 130 026 "" "meters (+ starboard)"}

			LABEL		{Label        	040 060 070 026 "" "Vertical"}
			NUMBER		{VertOffset   	120 060 050 026 "Vertical shift in meters"}
			LABEL		{Label        	180 060 130 026 "" "meters (+ below)"}

			LABEL		{Label		000 100 230 026 "" "Ship name"}
	 		TEXTFIELD	{ShipName	240 100 340 026 "Full name of ship which made the survey"}

	 		LABEL		{Label		000 130 230 026 "" "Cruise name"}
	 		TEXTFIELD	{SurveyName	240 130 340 026 "Full cruise name" }

	 		LABEL		{Label		000 160 230 026 "" "Reference point description"} 
	 		TEXTFIELD	{Reference	240 160 340 026 "Description of navigation reference point" }
			}
		}

	PANEL	{NavigationPanel	000 010 620 400 "Navigation/Celerite importation parameters"

		LABEL           {Label        000 000 220 026 "" "Caraibes Navig. output files"}
		FILEMANY	{NavFile      230 000 340 026 "Navigation file name at output" nvi}

		LABEL	{Label        000 040 220 026 "" "Points sampling"}
		OPTION	{NavSelection 230 040 190 026 "Selection of navigation points read"
				No		{"No"}
				Sampling	{"1 point / n" Panel_sample{}}
				Timing		{"1 point / n seconds" Panel_time{}}}

		PANEL	{Panel_sample	430 040 100 026 ""
			LABEL           {Label   000 000 020 026 "" "n = "}
			NUMBER		{Sample  030 000 050 026 "Sampling step"}
			}

		PANEL	{Panel_time	430 040 170 026 ""
			LABEL           {Label   000 000 020 026 "" "n = "}
			NUMBER		{Time    030 000 050 026 "Sampling frequency"}
			LABEL           {Label   090 000 080 026 "" "seconds"}
			}

		PANEL	{Panel_Ellipsoid	000 080 580 130 ""
			LABEL		{Label		000 000 170 026 "" "Ellipsoid for output files"}
			ELLIPSOID	{Ellipsoid	180 000 400 120 "Ellipsoid for output files"}
			}

		LABEL	{Label          000 220 220 026 "" "Sound Velocity profiles importation"}
		OPTION	{Velocity       230 220 080 026 "Sound velocity profile extraction from Seabeam file"
				False   {"No"}
				True    {"Yes" VelocityPanel{}}}

		PANEL	{VelocityPanel 000 250 580  030 ""
			LABEL		{Label		040 000 180 026 "" "SV profiles output file"}	
			FILE            {VelocityFile	230 000 350 026 "Sound velocity profile extraction from Seabeam file"}
			}
		}

	BUTTON 	{Button1 050 460 100 030 "Access to bathymetry panel" 
		Activate {"Bathymetry" BathymetryPanel{}}}
	
	BUTTON 	{Button2 200 460 150 030 "Access to navigation/celerity panel"
		Activate {"Navigation/Celerity" NavigationPanel{}}}
	
	}

# ---------------------------------------------------------------------------
# REGLES D'ACTIVATION
# ---------------------------------------------------------------------------
RULES	{
	@echo 'Running of Ts2100' >> ${PROGRESS_FILE}
	@${PROCESS_RUNNER} "Ts2100" ${PARAMETER_FILE} ${PROGRESS_FILE}
	@echo 'End of Ts2100' >> ${PROGRESS_FILE}
	}
