@echo off
set CIB_OS=_WIN
set CIB_SITE=Q:/workspace/fr.ifremer.globe.toolbox/Toolbox
set CIB_ERROR=%CIB_SITE%/SYS/DAT/CIB_Error
set CIB_CONF=%CIB_SITE%/SYS/DAT/CIB_Toolbox_WIN.cfg
set CIB_LANG=FR
set PATH=.;%CIB_SITE%/outils/bin;%PATH%
set CYGWIN=nodosfilewarning
