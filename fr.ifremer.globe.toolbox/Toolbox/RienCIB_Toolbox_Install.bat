@echo off

REM ***************************************************************************
REM 
REM  PRO : CARAIBES 
REM  MOD : CIB_Toolbox_Install 
REM  ROL : Script de configuration de la boite a outils CARAIBES pour GLOBE
REM       
REM
REM 
REM  CRE : 12/04/2012
REM  AUT : PLR
REM ***************************************************************************
REM  
REM  HST : 12/04/2012 PLR Creation
REM ***************************************************************************
REM 
REM  Copyright, IFREMER, 2012
REM 
REM ***************************************************************************

REM ****************************************************************************/
REM	Plate-forme
REM ****************************************************************************/
set CIB_OS=_WIN

REM ***************************************************************************
REM * Mise a jour de la configuration
REM ***************************************************************************

echo .
echo Starting configuration of CARAIBES Toolbox
echo .
echo           Copyright, IFREMER, 2012            
echo .

REM ***************************************************************************
REM * Recuperation du site d'installation du logiciel
REM ***************************************************************************
set TOOLBOX_INSTALL_SITE=%~dp0
set TOOLBOX_INSTALL_SITE=%TOOLBOX_INSTALL_SITE:~0,-1%
set TOOLBOX_INSTALL_DIR=%TOOLBOX_INSTALL_SITE:\=/%
echo .
echo The Toolbox is installed in the directory : %TOOLBOX_INSTALL_SITE%
echo .

REM ***************************************************************************
REM * Mise a jour du site d'installation dans les fichiers de configuration
REM ***************************************************************************
echo .
echo Updating configuration files
echo .

set OLD_DIR=%CD%
chdir /d %TOOLBOX_INSTALL_SITE%\SYS\DAT

echo CIB_Toolbox%CIB_OS%.cfg
copy CIB_Toolbox.Model%CIB_OS%.cfg CIB_Toolbox%CIB_OS%.cfg
call :replace CIB_Toolbox%CIB_OS%.cfg TOOLBOX_DIR %TOOLBOX_INSTALL_DIR%

echo CIB_Process%CIB_OS%.cfg
copy CIB_Process.Model%CIB_OS%.cfg CIB_Process%CIB_OS%.cfg
call :replace CIB_Process%CIB_OS%.cfg TOOLBOX_DIR %TOOLBOX_INSTALL_DIR%

echo Toolbox_env.bat
copy Toolbox_env.Model.bat Toolbox_env.bat
call :replace Toolbox_env.bat TOOLBOX_DIR %TOOLBOX_INSTALL_DIR%
copy Toolbox_env.bat %TOOLBOX_INSTALL_SITE%\Toolbox_env.bat

chdir /d %OLD_DIR%

REM ***************************************************************************
REM * Message de fin d'installation
REM ***************************************************************************
echo .
echo CARAIBES Toolbox is now installed
goto :eof

REM ***************************************************************************
REM * Fonction de remplacement d'une chaine par une autre dans un fichier
REM * %1 : Nom du fichier
REM * %2 : Chaine a rechercher
REM * %3 : Chaine de subsitution
REM ***************************************************************************
:replace
for /f "tokens=* delims=&&" %%a in (%1) do ( 
  call :switch "%%a" %2 %3  tmp.txt
)
del %1
rename tmp.txt %1
goto :eof

:switch
SetLocal EnableDelayedExpansion
REM Parameters:
REM %1  = Line to search for replacement
REM %2  = Key to replace
REM %3  = Value to replace key with
REM %4  = File in which to write the replacement
REM

REM Read in line
REM
set line=%1

REM Write line to specified file, replacing key (%2) with value (%3)
REM
set new_line=!line:%2=%3!
for /f "useback tokens=*" %%a in ('%new_line%') do @echo %%~a>>%4

REM Restore delayed expansion
REM
EndLocal
goto :eof

:eof
