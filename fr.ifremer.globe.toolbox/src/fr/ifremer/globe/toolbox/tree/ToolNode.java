package fr.ifremer.globe.toolbox.tree;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.toolbox.tool.Tool;
import fr.ifremer.globe.ui.tree.ITreeNode;
import fr.ifremer.globe.ui.tree.TreeNode;
import fr.ifremer.globe.ui.utils.image.ImageResources;

/**
 * 
 */
public class ToolNode extends TreeNode<Tool> {

	public ToolNode(ITreeNode parent, Tool model) {
		super(parent, model);
	}

	@Override
	public String getName() {
		return model.label();
	}

	@Override
	protected void createChildren() {
	}

	@Override
	public boolean hasChildren() {
		return false;
	}

	@Override
	public Image getImage() {
		Image img = ImageResources.getImage("/icons/16/tool.png", getClass());
		return img;
	}
}
