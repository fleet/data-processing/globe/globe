package fr.ifremer.globe.toolbox.tree;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.toolbox.tool.Tool;
import fr.ifremer.globe.toolbox.toolboxExplorer.ToolboxExplorer;
import fr.ifremer.globe.ui.tree.ITreeNode;
import fr.ifremer.globe.ui.tree.TreeNode;
import fr.ifremer.globe.ui.utils.image.ImageResources;

/**
 * 
 */
public class ToolsetNode extends TreeNode<File> {

	protected static ToolboxExplorer _toolboxExplorer;

	public ToolsetNode(TreeViewer viewer, File model, ToolboxExplorer toolboxExplorer) {
		super(viewer, model);
		_toolboxExplorer = toolboxExplorer;
	}

	public ToolsetNode(ToolsetNode parent, File model) {
		super(parent, model);
	}

	@Override
	public String getName() {
		return model.getName();
	}

	@Override
	protected void createChildren() {
		if (children == null) {
			children = new ArrayList<ITreeNode>();
		}
		File[] files = model.listFiles();
		for (int l_loop = 0; l_loop < files.length; l_loop++) {
			if (!files[l_loop].isDirectory()) {
				if (Tool.isModule(files[l_loop].getAbsolutePath())) {
					children.add(new ToolNode(this, new Tool(files[l_loop].getAbsolutePath(), _toolboxExplorer)));
				}
			}
		}
		for (int l_loop = 0; l_loop < files.length; l_loop++) {
			if (files[l_loop].isDirectory() && !files[l_loop].getName().startsWith(".")) {
				children.add(new ToolsetNode(this, files[l_loop]));
			}
		}
	}

	@Override
	public boolean hasChildren() {
		return model.isDirectory();
	}

	@Override
	public Image getImage() {
		Image img = ImageResources.getImage("/icons/16/toolbox.png", getClass());
		return img;
	}

}
