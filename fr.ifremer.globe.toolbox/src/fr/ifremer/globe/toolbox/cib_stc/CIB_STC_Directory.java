package fr.ifremer.globe.toolbox.cib_stc;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CIB_STC_Directory {

	protected static final String CIB_STC_k_DEFAULTFILTER = "./*"; //$NON-NLS-1$
	protected static final char CIB_STC_k_SEPARATOR = '/';

	public static void scan(String p_filter, // I : Filtre (chemin+filtre)
			List<String> p_fileList, // O : Liste des fichiers
			boolean p_ignoreDir // I : Ignore les repertoire pour le filtrage
	) {

		String l_fullFilter = p_filter;
		String l_filterPath = ""; //$NON-NLS-1$
		String l_filterName = ""; //$NON-NLS-1$
		List<String> l_dirList = new ArrayList<String>();
		List<String> l_fileList = new ArrayList<String>();

		int l_index = l_fullFilter.lastIndexOf(CIB_STC_k_SEPARATOR);
		if (l_index > 0) {
			l_filterPath = l_fullFilter.substring(0, l_index + 1);
		}

		for (int l_i = l_index + 1; l_i < l_fullFilter.length(); l_i++) {
			switch (l_fullFilter.charAt(l_i)) {

			case '?':
				l_filterName += "[^\\/]"; //$NON-NLS-1$
				break;

			case '*':
				l_filterName += "[^\\/]*"; //$NON-NLS-1$
				break;

			case '^':
			case '.':
			case '[':
			case ']':
			case '$':
			case '(':
			case ')':
			case '+':
			case '{':
			case '}':
				l_filterName += "\\" + l_fullFilter.charAt(l_i); //$NON-NLS-1$
				break;

			default:
				l_filterName += l_fullFilter.charAt(l_i);
				break;
			}
		}

		File directory = new File(l_filterPath);
		if (directory.exists() && directory.isDirectory()) {
			File[] files = directory.listFiles();

			for (int l_loop = 0; l_loop < files.length; l_loop++) {

				String l_fileName = files[l_loop].getName();

				if (files[l_loop].isDirectory()) {
					if (p_ignoreDir || (!p_ignoreDir && l_fileName.matches(l_filterName))) {
						l_dirList.add(l_fileName + CIB_STC_k_SEPARATOR);
					}
				} else {
					if (l_fileName.matches(l_filterName)) {
						l_fileList.add(l_fileName);
					}
				}
			}

			Collections.sort(l_dirList);
			p_fileList.addAll(l_dirList);

			Collections.sort(l_fileList);
			p_fileList.addAll(l_fileList);

		}
	}

}
