package fr.ifremer.globe.toolbox.cib_stc;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CIB_STC_AccessFilePara {

	static char CIB_STC_k_OpenBloc = '{';
	static char CIB_STC_k_CloseBloc = '}';
	static char CIB_STC_k_EndLine = '\n';
	static char CIB_STC_k_SartComment = '#';
	static char CIB_STC_k_TextField = '"';
	static char CIB_STC_k_Space = ' ';
	static String CIB_STC_k_StrEndLine = "\n"; //$NON-NLS-1$
	static String CIB_STC_k_StopAt = "[ ]*[\t]*\\{"; //$NON-NLS-1$

	protected String _fileName;
	protected StringBuffer _content;
	protected int _index;
	protected int _startBloc;
	protected int _startIdent;
	protected int _endBloc;
	protected int _lenBloc;

	public CIB_STC_AccessFilePara() {
		_fileName = ""; //$NON-NLS-1$
		_content = new StringBuffer();
		_index = 0;
		_startBloc = 0;
		_startIdent = 0;
		_endBloc = 0;
		_lenBloc = 0;
	}

	public void setByString(String p_string) {
		_fileName = ""; //$NON-NLS-1$
		_content = new StringBuffer(p_string);
		_index = 0;
		_startBloc = 0;
		_endBloc = 0;
		_startIdent = 0;
		_lenBloc = 0;
	}

	public void setByFile(String p_file) {
		_fileName = p_file;
		_index = 0;
		_startBloc = 0;
		_endBloc = 0;
		_startIdent = 0;
		_lenBloc = 0;
		try {
			_content = readFileAsString(p_file);
		} catch (Exception ex) {
			CIB_STC_Error.logError(Messages.getString("CIB_STC_AccessFilePara.4"), ex); //$NON-NLS-1$
		}

	}

	public String read(String p_name) {

		String l_value = ""; //$NON-NLS-1$
		String[] l_param = new String[2];

		l_param[0] = p_name;
		if (findBloc(l_param)) {
			l_value = l_param[1];
		}

		return l_value;
	}

	public boolean findBloc(String[] p_param) {

		boolean l_find = false;
		String[] l_param = new String[2];

		// ********************************************************************
		// recherche sur le premier identificateur, ici il est possible de
		// sortir en erreur si le fichier ne contient aucun bloc
		// ********************************************************************
		first(l_param);

		if (l_param[0].equals(p_param[0])) {
			l_find = true;
		}

		// ********************************************************************
		// on poursuit la recherche sur les identificateurs suivants
		// ********************************************************************

		while (!l_find && next(l_param)) {
			if (l_param[0].equals(p_param[0])) {
				l_find = true;
			}
		}

		p_param[1] = l_param[1];

		return l_find;
	}

	public boolean first(String[] p_param) {

		boolean l_status = true;

		getIdent(0, p_param);
		if (p_param[0].isEmpty()) {
			l_status = false;
			p_param[1] = ""; //$NON-NLS-1$
		}

		return l_status;
	}

	public boolean next(String[] p_param) {

		boolean l_status = true;

		getIdent(_index, p_param);
		if (p_param[0].isEmpty()) {
			l_status = false;
			p_param[1] = ""; //$NON-NLS-1$
		}

		return (l_status);
	}

	public void write(String p_name, String p_parametre) {

		String[] l_param = new String[2];

		l_param[0] = p_name;
		l_param[1] = p_parametre;

		writeBloc(l_param);

	}

	public void writeBloc(String[] p_param) {

		int l_index = 0;
		String l_space = " "; //$NON-NLS-1$
		String[] l_param = new String[2];

		l_param[0] = p_param[0];
		if (!findBloc(l_param)) {
			_content.append(l_space);
			_content.append(p_param[0]);
			_content.append(CIB_STC_k_Space);
			_content.append(CIB_STC_k_OpenBloc);
			_content.append(p_param[1]);
			_content.append(CIB_STC_k_CloseBloc);
			_content.append(CIB_STC_k_EndLine);
		} else {
			deleteBloc(p_param[0]);

			l_index = _startIdent;

			_content.insert(l_index, p_param[0]);

			l_index += p_param[0].length();
			_content.insert(l_index, CIB_STC_k_Space);
			l_index++;

			_content.insert(l_index, CIB_STC_k_OpenBloc);
			l_index++;

			_content.insert(l_index, p_param[1]);
			l_index += p_param[1].length();

			_content.insert(l_index, CIB_STC_k_CloseBloc);
			l_index++;

			_content.insert(l_index, CIB_STC_k_EndLine);
		}
	}

	public void writeBlocNoErase(String[] p_param) {

		String l_space = " "; //$NON-NLS-1$

		_content.append(l_space);
		_content.append(p_param[0]);
		_content.append(CIB_STC_k_Space);
		_content.append(CIB_STC_k_OpenBloc);
		_content.append(p_param[1]);
		_content.append(CIB_STC_k_CloseBloc);
		_content.append(CIB_STC_k_EndLine);

	}

	public void writeCurrentBloc(String[] p_param) {

		String l_space = " "; //$NON-NLS-1$

		deleteCurrentBloc();

		_index = _startIdent;
		_content.insert(_index, l_space);
		_index++;

		_content.insert(_index, p_param[0]);
		_index += p_param[0].length();

		_content.insert(_index, CIB_STC_k_Space);
		_index++;

		_content.insert(_index, CIB_STC_k_OpenBloc);
		_index++;

		_content.insert(_index, p_param[1]);
		_startBloc = _index;
		_index += p_param[1].length();
		_endBloc = _index - 1;

		_content.insert(_index, CIB_STC_k_CloseBloc);
		_index++;

	}

	public void deleteBloc(String p_ident) {

		String[] l_param = new String[2];

		l_param[0] = p_ident;
		if (findBloc(l_param)) {
			deleteCurrentBloc();
		}

	}

	public void deleteCurrentBloc() {

		int l_lenBloc = 0;

		l_lenBloc = _endBloc - _startIdent + 1;
		_content.delete(_startIdent, _endBloc + 2);
		_index = _endBloc - l_lenBloc;
		if (_index < 0) {
			_index = 0;
		}
		_startBloc = _index + 1;
		_endBloc = _index;

	}

	public String readAll() {

		return new String(_content);

	}

	public void flush(String p_fileName) {

		try {
			if (!p_fileName.isEmpty()) {
				BufferedWriter out = new BufferedWriter(new FileWriter(p_fileName));
				out.write(_content.toString());
				out.close();
			}
		} catch (Exception e) {
			CIB_STC_Error.logError(Messages.getString("CIB_STC_AccessFilePara.10") + p_fileName, e); //$NON-NLS-1$
		}

		try {
			if (!_fileName.isEmpty()) {
				BufferedWriter out = new BufferedWriter(new FileWriter(_fileName));
				out.write(_content.toString());
				out.close();
			}
		} catch (Exception e) {
			CIB_STC_Error.logError(Messages.getString("CIB_STC_AccessFilePara.10") + _fileName, e); //$NON-NLS-1$
		}
	}

	private void getIdent(int p_pos, String[] p_param) {

		boolean l_find = false;
		int l_index = p_pos;
		int l_lenStart = 0;
		int l_svgdIndex = 0;
		String l_ident = ""; //$NON-NLS-1$
		Pattern l_reg = Pattern.compile(CIB_STC_k_StopAt);
		Matcher l_matcher;

		p_param[0] = ""; //$NON-NLS-1$
		p_param[1] = ""; //$NON-NLS-1$

		while (!l_find && (l_index < _content.length()) && (l_index != -1)) {

			l_index = passComment(l_index);

			if ((l_index < _content.length()) && (l_index != -1)) {

				l_svgdIndex = l_index;
				l_matcher = l_reg.matcher(_content);
				l_matcher.region(l_index, _content.length() - 1);
				if (l_matcher.find()) {

					l_index = l_matcher.start();
					l_lenStart = l_matcher.end() - l_matcher.start();

					l_ident = _content.substring(l_svgdIndex, l_matcher.start());

					StringTokenizer l_token = new StringTokenizer(l_ident);
					while (l_token.hasMoreTokens()) {
						p_param[0] = l_token.nextToken();
					}

					_startIdent = l_index - p_param[0].length();

					l_find = true;
				} else {
					l_index = -1;
				}
			}
		}

		if (l_find) {
			_startBloc = l_index + l_lenStart;
			findEndBloc(_startBloc);

			_index = _endBloc + 1;

			if (_endBloc == -1) {
				CIB_STC_Error.logError(Messages.getString("CIB_STC_AccessFilePara.15"), null); //$NON-NLS-1$
			}
		}

		_lenBloc = _endBloc - _startBloc;
		if (_lenBloc > 0) {
			p_param[1] = _content.substring(_startBloc, _endBloc);
		}

	}

	private int passComment(int p_index) {

		while ((p_index < _content.length()) && (p_index != -1) && (_content.charAt(p_index) == CIB_STC_k_EndLine)) {
			p_index++;
		}

		while ((p_index < _content.length()) && (p_index != -1) && _content.charAt(p_index) == CIB_STC_k_SartComment) {

			p_index = _content.indexOf(CIB_STC_k_StrEndLine, p_index) + 1;

			while ((p_index < _content.length()) && (p_index != -1) && _content.charAt(p_index) == CIB_STC_k_EndLine) {
				p_index++;
			}
		}

		return p_index;
	}

	private void findEndBloc(int p_index) {

		int l_flag = -1;
		int l_index = 0;
		String l_strTextField = ""; //$NON-NLS-1$

		l_strTextField += CIB_STC_k_TextField;
		l_index = p_index;

		while (l_flag != 0 && _endBloc != -1) {

			if (l_index >= _content.length()) {
				_endBloc = -1;
			} else {

				if (_content.charAt(l_index) == CIB_STC_k_TextField) {

					l_index = _content.indexOf(l_strTextField, l_index + 1);

					if (l_index == -1) {
						CIB_STC_Error.logError(Messages.getString("CIB_STC_AccessFilePara.15"), null); //$NON-NLS-1$
					}
				}

				if (_content.charAt(l_index) == CIB_STC_k_OpenBloc) {
					l_flag -= 1;
				}

				if (_content.charAt(l_index) == CIB_STC_k_CloseBloc) {
					l_flag += 1;
				}

				_endBloc = l_index;
				l_index++;
			}
		}

	}

	private static StringBuffer readFileAsString(String p_file) throws java.io.IOException {
		byte[] buffer = new byte[(int) new File(p_file).length()];
		BufferedInputStream f = new BufferedInputStream(new FileInputStream(p_file));
		f.read(buffer);
		f.close();
		return new StringBuffer(new String(buffer));
	}
}
