package fr.ifremer.globe.toolbox.cib_stc;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

public class CIB_STC_Properties {

	private static final String Properties_k_SUFFIX = ".properties"; //$NON-NLS-1$

	private String _fileName;
	private Properties _properties = new Properties();

	public CIB_STC_Properties(String p_fileName) {

		_fileName = p_fileName;

		if (_fileName == null) {
			throw new IllegalArgumentException(Messages.getString("CIB_STC_Properties.1")); //$NON-NLS-1$
		}

		if (_fileName.endsWith(Properties_k_SUFFIX)) {
			_fileName = _fileName.substring(0, _fileName.length() - Properties_k_SUFFIX.length());
		}

		try {
			final ResourceBundle rb = ResourceBundle.getBundle(_fileName);
			String key;

			for (Enumeration<String> keys = rb.getKeys(); keys.hasMoreElements();) {
				key = keys.nextElement();
				_properties.put(key, rb.getString(key));
			}
		} catch (Throwable ex) {
			CIB_STC_Error.logError(Messages.getString("CIB_STC_Properties.2") + _fileName, ex); //$NON-NLS-1$
		}
	}

	public String getProperty(String key) {
		return _properties.getProperty(key);
	}

	public void setProperty(String key, String value) {
		_properties.setProperty(key, value);
	}

	public Properties getProperties() {
		return _properties;
	}

	public void write() {
		String l_fileName = _fileName;
		try {
			FileOutputStream fos = null;

			l_fileName = l_fileName.replace('.', '/');
			if (!l_fileName.endsWith(Properties_k_SUFFIX)) {
				l_fileName = l_fileName.concat(Properties_k_SUFFIX);
			}

			URL l_url = ClassLoader.getSystemResource(l_fileName);
			l_fileName = l_url.toURI().getPath();
			_properties.store((fos = new FileOutputStream(l_fileName)), null);
			fos.close();
		} catch (Throwable ex) {
			CIB_STC_Error.logError(Messages.getString("CIB_STC_Properties.3") + l_fileName, ex); //$NON-NLS-1$

		}
	}

	public void write(String p_fileName) {
		try {
			FileOutputStream fos = null;

			_properties.store((fos = new FileOutputStream(p_fileName)), null);
			fos.close();
		} catch (IOException ex) {
			CIB_STC_Error.logError(Messages.getString("CIB_STC_Properties.3") + p_fileName, ex); //$NON-NLS-1$

		}

	}

}
