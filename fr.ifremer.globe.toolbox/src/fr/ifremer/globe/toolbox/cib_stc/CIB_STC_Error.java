package fr.ifremer.globe.toolbox.cib_stc;

import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import fr.ifremer.globe.ui.dialogs.PickWorkspaceDialog;

public class CIB_STC_Error {

	static Logger _logger = null;

	private static Logger getLogger() {
		try {
			if (_logger == null) {
				Handler fh = new FileHandler(Paths.get(PickWorkspaceDialog.getLastSetWorkspaceDirectory()) + "/Toolbox/myLog.log", false); //$NON-NLS-1$
				_logger = Logger.getLogger("cib_legacy.stc.Logger"); //$NON-NLS-1$
				_logger.addHandler(fh);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(Messages.getString("CIB_STC_Error.2") + ex); //$NON-NLS-1$
		}

		return _logger;
	}

	/**
	 * Log the specified information.
	 * 
	 * @param message
	 *            , a human-readable message, localized to the current locale.
	 */
	public static void logInfo(String message) {
		log(Level.INFO, message, null);
	}

	/**
	 * Log the specified error.
	 * 
	 * @param exception
	 *            , a low-level exception.
	 */
	public static void logError(Throwable exception) {
		logError(Messages.getString("CIB_STC_Error.3"), exception); //$NON-NLS-1$
	}

	/**
	 * Log the specified error.
	 * 
	 * @param message
	 *            , a human-readable message, localized to the current locale.
	 * @param exception
	 *            , a low-level exception, or <code>null</code> if not
	 *            applicable.
	 */
	public static void logError(String message, Throwable exception) {
		log(Level.SEVERE, message, exception);
	}

	/**
	 * Log the specified information.
	 */
	public static void log(Level severity, String message, Throwable exception) {
		getLogger().log(severity, message, exception);
	}
}
