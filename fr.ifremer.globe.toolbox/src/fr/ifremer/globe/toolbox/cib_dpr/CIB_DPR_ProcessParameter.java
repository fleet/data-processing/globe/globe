package fr.ifremer.globe.toolbox.cib_dpr;

import java.util.List;
import java.util.StringTokenizer;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_DPR_ProcessParameter {

	public static final String CIB_DPR_k_LATITUDE_NORTH = "N"; //$NON-NLS-1$
	public static final String CIB_DPR_k_LATITUDE_SOUTH = "S"; //$NON-NLS-1$
	public static final String CIB_DPR_k_LONGITUDE_EAST = "E"; //$NON-NLS-1$
	public static final String CIB_DPR_k_LONGITUDE_WEST = "W"; //$NON-NLS-1$
	public static final double CIB_DPR_k_DEGREE_MINUTE = 60.0;

	public static String read(CIB_STC_AccessFilePara p_bloc, String p_name) {
		return p_bloc.read(p_name);
	}

	public static void write(CIB_STC_AccessFilePara p_bloc, String p_name, String p_parametre) {
		p_bloc.write(p_name, p_parametre);
	}

	public static void read(CIB_STC_AccessFilePara p_bloc, String p_name, List<String> p_list) {

		String l_value = read(p_bloc, p_name);
		StringTokenizer l_token = new StringTokenizer(l_value);
		while (l_token.hasMoreTokens()) {
			p_list.add(l_token.nextToken());
		}
	}

	public static void write(CIB_STC_AccessFilePara p_bloc, String p_name, List<String> p_list) {

		String l_value = ""; //$NON-NLS-1$
		for (int l_loop = 0; l_loop < p_list.size(); l_loop++) {
			l_value += p_list.get(l_loop);
			l_value += " "; //$NON-NLS-1$
		}
		write(p_bloc, p_name, l_value);
	}

	public static int readInt(CIB_STC_AccessFilePara p_bloc, String p_name) {

		int l_intVal = 0;
		String l_value = read(p_bloc, p_name);
		try {
			l_intVal = Integer.parseInt(l_value);
		} catch (Exception e) {
			l_intVal = 0;
		}

		return l_intVal;
	}

	public static void writeInt(CIB_STC_AccessFilePara p_bloc, String p_name, int p_value) {
		write(p_bloc, p_name, Integer.toString(p_value));
	}

}
