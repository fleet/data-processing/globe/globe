package fr.ifremer.globe.toolbox.cib_dpr;

public class CIB_DPR_FileMany {

	public static final String CIB_DPR_k_FILEMANY_OUTPUT_TYPE = "OUTPUT"; // type de sortie (fichier simple, genericite) //$NON-NLS-1$
	public static final String CIB_DPR_k_FILEMANY_EXTENSION = "EXTENSION"; // extension de fichier //$NON-NLS-1$
	public static final String CIB_DPR_k_FILEMANY_FILE = "FILE"; // fichier simple //$NON-NLS-1$
	public static final String CIB_DPR_k_FILEMANY_ROOT_NAME = "ROOT_NAME"; // racine du nom des fichiers //$NON-NLS-1$

	public static final int CIB_DPR_k_SIMPLE_GENERIC = 0; // Type de nom :
															// generique simple
	public static final int CIB_DPR_k_DOUBLE_GENERIC = -1; // Type de nom :
															// generique double
	public static final int CIB_DPR_k_TRIPLE_GENERIC = -2; // Type de nom :
															// generique triple
	public static final int CIB_DPR_k_FILTER_GENERIC = -3; // Type de nom :
															// generique filtre
	public static final int CIB_DPR_k_SIMPLE_LIST = -4; // Type de noms : liste
														// de fichiers

	public static final int CIB_DPR_k_MANY_SIMPLE = 0; // Type de filemany :
														// simple
	public static final int CIB_DPR_k_MANY_GENERIC = 1; // Type de filemany :
														// generique

}
