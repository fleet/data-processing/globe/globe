package fr.ifremer.globe.toolbox.cib_dpr;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_DPR_FileGen {

	public static String CIB_DPR_k_FILEGEN_FILELIST = "FILELIST"; // liste des fichiers //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_INPUT_TYPE = "INPUT"; // type d entree (liste de fichiers, genericite) //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_EXTENSION = "EXTENSION"; // extension de fichier //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_ROOT_NAME = "ROOT_NAME"; // racine du nom des fichiers //$NON-NLS-1$
	public static String CIB_DPR_k_UNKNOWN = "UNKNOWN"; // Type de valeur inconnu //$NON-NLS-1$
	public static String CIB_DPR_k_FILELIST = "FILELIST"; // Type de valeur liste etendue //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN = "FILEGEN"; // Type de valeur liste generique en entree //$NON-NLS-1$
	public static String CIB_DPR_k_FILEMANY = "FILEMANY"; // Type de valeur liste generique en sortie //$NON-NLS-1$

	public static String CIB_DPR_k_FILEGEN_START1 = "START1"; // profil de debut 1 //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_END1 = "END1"; // profil de fin 1 //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_LENGTH1 = "LENGTH1"; // longueur du champ profil 1 //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_START2 = "START2"; // profil de debut 2 //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_END2 = "END2"; // profil de fin 2 //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_LENGTH2 = "LENGTH2"; // longueur du champ profil 2 //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_START3 = "START3"; // profil de debut 3 //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_END3 = "END3"; // profil de fin 3  //$NON-NLS-1$
	public static String CIB_DPR_k_FILEGEN_LENGTH3 = "LENGTH3"; // longueur du champ profil 3 //$NON-NLS-1$

	public static String read(CIB_STC_AccessFilePara p_bloc, String p_name) {

		String l_value = ""; //$NON-NLS-1$
		String[] l_param = new String[2];

		l_param[0] = p_name;
		if (p_bloc.findBloc(l_param)) {
			l_value = l_param[1];
		}

		return l_value;
	}

	public static void write(CIB_STC_AccessFilePara p_bloc, String p_name, String p_parametre) { // I
																									// :
																									// Valeur

		String[] l_param = new String[2];

		l_param[0] = p_name;
		l_param[1] = p_parametre;

		p_bloc.writeBloc(l_param);

	}

}
