package fr.ifremer.globe.toolbox.toolboxExplorer;

import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MBasicFactory;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.toolbox.tool.Tool;
import fr.ifremer.globe.toolbox.tool.ToolConfig;
import fr.ifremer.globe.toolbox.tree.ToolNode;
import fr.ifremer.globe.toolbox.tree.ToolsetNode;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.providers.TreeContentProvider;
import fr.ifremer.globe.ui.providers.TreeLabelProvider;
import fr.ifremer.globe.ui.utils.Messages;


/**
 * Data explorer view part.
 */

public class ToolboxExplorer {

	protected Logger logger = LoggerFactory.getLogger(ToolboxExplorer.class);

	private final Composite parent;
	private TreeViewer viewer;

	@Inject
	ESelectionService selectionService;

	@Inject
	private EMenuService menuService;

	@Inject
	private EModelService modelService;

	@Inject
	private MApplication application;

	@Inject
	private EPartService partService;

	@Inject
	public ToolboxExplorer(Composite parent, IWorkspace workspace) {
		this.parent = parent;
	}

	@Focus
	public void onFocus() {
		this.logger.debug("Toolbox view has focus");
	}

	@PostConstruct
	public void createUI() {

		final GridLayout layout = new GridLayout(2, false);
		this.parent.setLayout(layout);

		// Search.createSearchTool(parent, this);

		this.viewer = new TreeViewer(this.parent, SWT.SINGLE);

		this.viewer.setLabelProvider(new TreeLabelProvider());
		this.viewer.setContentProvider(new TreeContentProvider());

		this.viewer.setAutoExpandLevel(1);
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		this.viewer.getControl().setLayoutData(gridData);

		this.viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) ToolboxExplorer.this.viewer.getSelection();
				ToolboxExplorer.this.selectionService.setSelection(selection);
			}
		});

		this.viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				String osName = System.getProperty("os.name");
				if (osName.contains("Mac")) {
					Messages.openInfoMessage("Toolbox Caraibes", "Impossible to execute on MAC; only Windows or linux ");
				} else {
					Tool l_tool = null;
					Object elem = ((IStructuredSelection) ToolboxExplorer.this.viewer.getSelection()).getFirstElement();
					if (elem != null && elem instanceof ToolNode) {
						l_tool = ((ToolNode) elem).getModel();
					}
					if (l_tool != null) {
						l_tool.open();
					}
				}
			}
		});

		setupContextMenu(this.viewer, this.parent.getShell());
		this.menuService.registerContextMenu(this.viewer.getControl(), "fr.ifremer.globe.ui.projectexplorer.popupmenu");

		refreshInit();
	}

	private void refreshInit() {
		ToolConfig toolConfig = ToolConfig.getInstance() ;
		if (toolConfig != null)
			this.viewer.setInput(new ToolsetNode(this.viewer, toolConfig.getToolboxRoot(), this));
		else
			viewer = null;
	}

	private void setupContextMenu(final TreeViewer viewer, final Shell shell) {
		MenuManager mgr = new MenuManager();
		Menu menu = mgr.createContextMenu(viewer.getControl());

		viewer.getControl().setMenu(menu);

		mgr.setRemoveAllWhenShown(true);
		mgr.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager manager) {
			}
		});
	}

	public void run(Tool p_tool) {

		MPart toolboxConsolePart = PartManager.forceCreatePart(this.partService, ToolboxConsole.TOOLBOX_CONSOLE_ID);
		toolboxConsolePart.setLabel(p_tool.name());

		List<MPartStack> soundingsStacks = this.modelService.findElements(this.application, PartManager.RIGHT_STACK_ID, MPartStack.class, null);
		assert soundingsStacks != null : "Stack " + PartManager.RIGHT_STACK_ID + " must be defined in Application.e4xmi";
		if (soundingsStacks.isEmpty()) {
			soundingsStacks.add(MBasicFactory.INSTANCE.createPartStack());
		}
		soundingsStacks.get(0).getChildren().add(toolboxConsolePart);

		this.modelService.bringToTop(toolboxConsolePart);

		Object l_obj = toolboxConsolePart.getObject();
		final ToolboxConsole l_console = (ToolboxConsole) l_obj;

		p_tool.run(l_console);
	}

	public TreeViewer getViewer() {
		return this.viewer;
	}

}