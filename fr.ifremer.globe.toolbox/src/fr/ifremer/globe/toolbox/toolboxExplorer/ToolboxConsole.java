package fr.ifremer.globe.toolbox.toolboxExplorer;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ToolboxConsole {

	public static final String TOOLBOX_CONSOLE_ID = "fr.ifremer.globe.toolboxConsole";

	protected Logger logger = LoggerFactory.getLogger(ToolboxConsole.class);

	private final Composite parent;
	private Text _consoleText;

	@Inject
	public ToolboxConsole(Composite parent, IWorkspace workspace) {
		this.parent = parent;
		this._consoleText = new Text(parent, SWT.READ_ONLY | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
	}

	@Focus
	public void onFocus() {
		this.logger.debug("ToolboxConsole has focus");
	}

	@PostConstruct
	public void createUI() {
	}

	public void append(StringBuffer p_buffer) {
		if (!_consoleText.isDisposed()) {
			this._consoleText.append(p_buffer.toString());
			FontRegistry fontRegistry = new FontRegistry(this.parent.getDisplay());
			fontRegistry.put("console", new FontData[] { new FontData("Courier New", 9, SWT.NORMAL) });
			this._consoleText.setFont(fontRegistry.get("console"));
			this._consoleText.setBackground(this.parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		}		
	}
	
	public boolean isDisposed() {
		return _consoleText.isDisposed();
	}

}
