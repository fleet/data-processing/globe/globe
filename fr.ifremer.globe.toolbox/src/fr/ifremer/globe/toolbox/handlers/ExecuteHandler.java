package fr.ifremer.globe.toolbox.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.toolbox.tool.Tool;
import fr.ifremer.globe.toolbox.tree.ToolNode;

public class ExecuteHandler {
	private Logger logger = LoggerFactory.getLogger(ExecuteHandler.class);

	@CanExecute
	public boolean canExecute(ESelectionService selectionService) {
		Object obj = selectionService.getSelection();
		if (obj instanceof StructuredSelection) {
			Object elem = ((StructuredSelection) obj).getFirstElement();
			if (elem != null && elem instanceof ToolNode) {
				return true;
			}
		}
		return false;
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService) {
		logger.info("RunTool handler");

		Object obj = selectionService.getSelection();
		if (obj instanceof StructuredSelection) {
			Object elem = ((StructuredSelection) obj).getFirstElement();
			if (elem != null && elem instanceof ToolNode) {
				final Tool l_tool = ((ToolNode) elem).getModel();
				l_tool.run();
			}
		}
	}

}
