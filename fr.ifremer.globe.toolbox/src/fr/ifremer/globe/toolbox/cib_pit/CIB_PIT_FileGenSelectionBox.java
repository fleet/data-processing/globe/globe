package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_FileGen;
import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_FileMany;
import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_FileGenSelectionBox extends CIB_PIT_SelectionBox {

	protected static int CIB_PIT_k_FILEGEN_SIMPLE_LIST = 0; // choix Liste
															// simple
	protected static int CIB_PIT_k_FILEGEN_GENERAL_LIST = 4; // choix Liste
																// generalisee

	protected int _selectionType; // Type de selection
	protected Combo _inputType; // Combo type de selection
	protected Composite _listContainer; // Conteneur liste
	CIB_PIT_GenSelectionPanel _genPanel; // Panneau liste generique
	CIB_PIT_FileListSelectionPanel _fileListPanel; // Panneau liste simple

	public CIB_PIT_FileGenSelectionBox(Display p_display, String p_name, boolean p_visible) {
		super(p_display, p_name, Messages.getString("CIB_PIT_FileGenSelectionBox.0"), p_visible); //$NON-NLS-1$
		useRun(false);
		initialize();
	}

	private void initialize() {
		load();
	}

	public void load() {

		GridData l_gridData;

		_mainContainer.setVisible(false);

		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		_mainContainer.setLayout(layout);

		// label
		Label l_label = new Label(_mainContainer, SWT.BOLD);
		l_label.setText(Messages.getString("CIB_PIT_FileGenSelectionBox.1")); //$NON-NLS-1$

		// Combo type de selection
		_inputType = new Combo(_mainContainer, SWT.READ_ONLY);
		_inputType.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_inputType.add(Messages.getString("CIB_PIT_FileGenSelectionBox.2")); //$NON-NLS-1$
		_inputType.add(Messages.getString("CIB_PIT_FileGenSelectionBox.3")); //$NON-NLS-1$
		_inputType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				changeInputType(_inputType.getSelectionIndex() == 0 ? CIB_PIT_k_FILEGEN_SIMPLE_LIST : CIB_PIT_k_FILEGEN_GENERAL_LIST);
			}
		});

		Label separator = new Label(_mainContainer, SWT.HORIZONTAL | SWT.SEPARATOR | SWT.BORDER);
		l_gridData = new GridData(GridData.FILL_HORIZONTAL);
		l_gridData.horizontalSpan = 2;
		separator.setLayoutData(l_gridData);

		// Panneau liste
		_listContainer = new Composite(_mainContainer, SWT.NONE);
		l_gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		l_gridData.horizontalSpan = 2;
		_listContainer.setLayoutData(l_gridData);

		_genPanel = new CIB_PIT_GenSelectionPanel(_listContainer, _helpContainer);
		_genPanel.setVisible(false);

		_fileListPanel = new CIB_PIT_FileListSelectionPanel(_listContainer, _helpContainer);
		_genPanel.setVisible(false);

		_shell.pack();

		changeInputType(CIB_PIT_k_FILEGEN_SIMPLE_LIST);
		_mainContainer.setVisible(true);

	}

	int getSelectionType() {
		return _selectionType;
	}

	void changeInputType(int p_selection) {
		_selectionType = p_selection;
		_inputType.select(_selectionType == CIB_PIT_k_FILEGEN_SIMPLE_LIST ? 0 : 1);

		if (_selectionType == CIB_PIT_k_FILEGEN_SIMPLE_LIST) {
			_fileListPanel.setVisible(true);
			_genPanel.setVisible(false);
		} else {
			_fileListPanel.setVisible(false);
			_genPanel.setVisible(true);
		}
	}

	public void preLoad() {
		_genPanel.select();
		_fileListPanel.select();
	}

	public void downLoad() {
		if (_selectionType == CIB_PIT_k_FILEGEN_SIMPLE_LIST) {
			_fileListPanel.apply();
		} else {
			_genPanel.apply();
		}
	}

	public String getMainValue() {
		String l_fileName;
		if (_selectionType == CIB_PIT_k_FILEGEN_SIMPLE_LIST) {
			l_fileName = _fileListPanel.getMainValue();
		} else {
			l_fileName = _genPanel.getMainValue();
		}

		return l_fileName;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _paramName);
		l_filePara.setByString(l_value);

		int l_inputType = CIB_DPR_ProcessParameter.readInt(l_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_INPUT_TYPE);

		if (l_inputType == CIB_DPR_FileMany.CIB_DPR_k_SIMPLE_LIST) {
			l_inputType = CIB_PIT_k_FILEGEN_SIMPLE_LIST;
		} else if (l_inputType == CIB_DPR_FileMany.CIB_DPR_k_FILTER_GENERIC) {
			l_inputType = CIB_PIT_k_FILEGEN_GENERAL_LIST;
		}

		_fileListPanel.update(l_filePara);
		_genPanel.update(l_filePara);

		changeInputType(l_inputType);

	}

	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_result = true;

		if (_selectionType == CIB_PIT_k_FILEGEN_SIMPLE_LIST) {
			l_result = _fileListPanel.validate(p_warning, p_context);
		} else {
			l_result = _genPanel.validate(p_warning, p_context);
		}

		setAutoHide(l_result);

		return l_result;
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		int l_selection = 0;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		if (_selectionType == CIB_PIT_k_FILEGEN_SIMPLE_LIST) {
			l_selection = CIB_DPR_FileMany.CIB_DPR_k_SIMPLE_LIST;
		} else if (_selectionType == CIB_PIT_k_FILEGEN_GENERAL_LIST) {
			l_selection = CIB_DPR_FileMany.CIB_DPR_k_FILTER_GENERIC;
		}

		CIB_DPR_ProcessParameter.writeInt(l_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_INPUT_TYPE, l_selection);

		_fileListPanel.dump(l_filePara);
		_genPanel.dump(l_filePara);

		CIB_DPR_ProcessParameter.write(p_filePara, _paramName, l_filePara.readAll());

	}

}
