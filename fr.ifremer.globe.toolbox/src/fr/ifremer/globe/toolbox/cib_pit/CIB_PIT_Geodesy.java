package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Geodesy extends CIB_PIT_ProcessParameter {

	protected static final String CIB_PIT_k_GEOFRAME = "GEOFRAME"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_PROJECTION = "PROJECTION"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_ELLIPSOID = "ELLIPSOID"; //$NON-NLS-1$

	CIB_PIT_GeographicalFrame _geoFrame;
	CIB_PIT_Projection _projection;
	CIB_PIT_Ellipsoid _ellipsoid;

	public CIB_PIT_Geodesy(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		_geoFrame = null;
		_projection = null;
		_ellipsoid = null;

		initialize(p_attributes);
	}

	public void initialize(StringBuffer p_attributes) {

		load();

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;

		if (_geoFrame != null) {
			l_status = _geoFrame.validate(p_warning, p_context);
		}
		if (l_status && _projection != null) {
			l_status = _projection.validate(p_warning, p_context);
		}
		if (l_status && _ellipsoid != null) {
			l_status = _ellipsoid.validate(p_warning, p_context);
		}

		return l_status;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		_geoFrame.update(l_filePara);
		_projection.update(l_filePara);
		_ellipsoid.update(l_filePara);
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();
		CIB_DPR_ProcessParameter.write(l_filePara, "NAME", "NoName"); //$NON-NLS-1$ //$NON-NLS-2$
		_geoFrame.dump(l_filePara);
		_projection.dump(l_filePara);
		_ellipsoid.dump(l_filePara);

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());
	}

	public void load() {

		RowLayout layout = new RowLayout();
		layout.marginHeight = 30;
		layout.type = SWT.VERTICAL;
		layout.justify = true;
		setLayout(layout);

		// ************************************************************
		// Creation parametre cadre geo
		// ************************************************************
		_geoFrame = new CIB_PIT_GeographicalFrame(this, CIB_PIT_k_GEOFRAME, new CIB_PIT_Rect(50, 80, 200, 200), "", _helpContainer, ""); //$NON-NLS-1$ //$NON-NLS-2$
		_projection = new CIB_PIT_Projection(this, CIB_PIT_k_PROJECTION, new CIB_PIT_Rect(50, 80, 200, 200), "", _helpContainer); //$NON-NLS-1$
		_ellipsoid = new CIB_PIT_Ellipsoid(this, CIB_PIT_k_ELLIPSOID, new CIB_PIT_Rect(50, 80, 200, 200), "", _helpContainer); //$NON-NLS-1$

		layout();

	}

}
