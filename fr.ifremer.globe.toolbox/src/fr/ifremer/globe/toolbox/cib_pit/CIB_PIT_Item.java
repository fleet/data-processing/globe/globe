package fr.ifremer.globe.toolbox.cib_pit;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Item {

	protected String _name;
	protected String _label;
	protected List<CIB_PIT_ItemPanel> _allPanels = new ArrayList<CIB_PIT_ItemPanel>();

	public CIB_PIT_Item(String p_itemName, String p_panels) {
		_name = p_itemName;

		int l_start;
		int l_end;
		String l_panels = p_panels;
		String[] l_param = new String[2];
		CIB_PIT_ItemPanel l_panel;
		CIB_STC_AccessFilePara l_paramFile = new CIB_STC_AccessFilePara();

		l_paramFile.setByString(l_panels);
		if (l_paramFile.first(l_param)) {
			do {
				l_panel = new CIB_PIT_ItemPanel(l_param[0], l_param[1]);
				_allPanels.add(l_panel);
			} while (l_paramFile.next(l_param));
		}

		l_start = l_panels.indexOf("\""); //$NON-NLS-1$
		if (l_start != -1) {
			l_end = l_panels.indexOf("\"", l_start + 1); //$NON-NLS-1$
			if (l_end > 0) {
				_label = l_panels.substring(l_start + 1, l_end);
			}
			l_panels = l_panels.substring(0, l_end + 1);
		}
	}

	public String name() {
		return _name;
	}

	public String label() {
		return _label;
	}

	List<CIB_PIT_ItemPanel> panels() {
		return _allPanels;
	}

	boolean equals(CIB_PIT_Item p_other) {
		return (_name.equals(p_other._name));
	}

}
