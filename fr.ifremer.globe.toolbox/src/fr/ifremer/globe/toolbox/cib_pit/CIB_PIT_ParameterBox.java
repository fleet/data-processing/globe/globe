package fr.ifremer.globe.toolbox.cib_pit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Error;

public class CIB_PIT_ParameterBox extends CIB_PIT_SelectionBox {

	private final String CIB_PIT_k_MAINPANEL = "PANEL"; //$NON-NLS-1$

	CIB_PIT_ParameterPanel _mainParameterPanel;

	public CIB_PIT_ParameterBox(Display p_display, String p_name, String p_title, boolean p_visible) {
		super(p_display, p_name, p_title, p_visible);
		_mainParameterPanel = null;

	}

	public void readFile(String p_filename) {

		CIB_STC_AccessFilePara l_paramFile = new CIB_STC_AccessFilePara();
		String[] l_param = new String[2];
		StringBuffer l_attributes;

		_mainContainer.setVisible(false);
		_mainParameterPanel = null;

		l_paramFile.setByFile(p_filename);
		l_param[0] = CIB_PIT_k_MAINPANEL;
		if (!l_paramFile.findBloc(l_param)) {
			CIB_STC_Error.logError(Messages.getString("CIB_PIT_ParameterBox.1"), null); //$NON-NLS-1$
		} else {
			l_attributes = new StringBuffer(l_param[1]);
			_mainParameterPanel = new CIB_PIT_PanelFolder(_mainContainer, l_attributes, _helpContainer);

			_shell.pack();
		}
		_mainParameterPanel.refresh();
		_mainContainer.setVisible(true);
	}

	public void update(String p_filename) {

		CIB_STC_AccessFilePara l_param = new CIB_STC_AccessFilePara();

		l_param.setByFile(p_filename);
		update(l_param);

	}

	public void dump(String p_filename) {

		CIB_STC_AccessFilePara l_param = new CIB_STC_AccessFilePara();
		File f = new File(p_filename);

		if (f.exists()) {
			l_param.setByFile(p_filename);
			dump(l_param);
			l_param.flush(p_filename);
		}
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_param) {

		_mainParameterPanel.update(p_param);
		_mainParameterPanel.refresh();

	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_param) {

		_mainParameterPanel.dump(p_param);

	}

	@Override
	public boolean validateSelection() {
		return validate();
	}

	public boolean validate() {

		List<String> l_warnings = new ArrayList<String>();
		String l_context = ""; //$NON-NLS-1$
		boolean l_result = true;

		l_result = _mainParameterPanel.validate(l_warnings, l_context);

		if (l_warnings.size() > 0) {
			CIB_STC_Error.logInfo(l_warnings.get(0));
		}

		if (!l_result) {
			_shell.setVisible(true);
		}

		return l_result;
	}
}
