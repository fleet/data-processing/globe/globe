package fr.ifremer.globe.toolbox.cib_pit;

public class CIB_PIT_ItemPanel {

	protected String _name;
	protected String _conditions;

	public CIB_PIT_ItemPanel(String p_panelName, String p_conditions) {
		_name = p_panelName;
		_conditions = p_conditions;
	}

	public String name() {
		return _name;
	}

	public String conditions() {
		return _conditions;
	}

	public boolean equals(CIB_PIT_ItemPanel p_other) {
		return ((_name.equals(p_other._name) && (_conditions.equals(p_other._conditions))));
	}

}
