package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Projection extends CIB_PIT_ProcessParameter {

	public static final int CIB_PIT_k_PROJECTION_MERCATOR = 0;
	public static final int CIB_PIT_k_PROJECTION_LAMBERT = 1;
	public static final int CIB_PIT_k_PROJECTION_UTM = 2;
	public static final int CIB_PIT_k_PROJECTION_STEREOGRAPHIQUE_POLAIRE = 3;
	public static final int CIB_PIT_k_PROJECTION_CYLINDRIQUE_EQUIDISTANTE = 4;

	public static final String CIB_PIT_k_TYPE_PROJECTION = "TYPE"; // ident. type projection	 //$NON-NLS-1$
	public static final String CIB_PIT_k_MERCATOR = "MERCATOR"; // ident. projection Mercator //$NON-NLS-1$
	public static final String CIB_PIT_k_LAMBERT = "LAMBERT"; // ident. projection lambert //$NON-NLS-1$
	public static final String CIB_PIT_k_UTM = "UTM"; // ident. projection utm //$NON-NLS-1$
	public static final String CIB_PIT_k_POLARSTEREOGRAPHIC = "POLAR_STEREOGRAPHIC"; // ident. projection //$NON-NLS-1$
	public static final String CIB_PIT_k_CYLINDRICED = "CYLINDRIC_EQUID"; // ident. projection cyl. equidistante //$NON-NLS-1$

	protected Combo _projectionMenu; // Combo type de projection
	protected CIB_PIT_Component _contArray[]; // Tableau des conteneurs
	protected CIB_PIT_Component _mercatorCont; // Conteneur panneau mercator
	protected CIB_PIT_Component _lambertCont; // Conteneur panneau lambert
	protected CIB_PIT_Component _utmCont; // Conteneur panneau utm
	protected CIB_PIT_Component _stereoCont; // Conteneur panneau
												// stereographique polaire
	protected CIB_PIT_Component _cylindCont; // Conteneur panneau cylindrique
												// equidistante

	CIB_PIT_Mercator _mercator; // projection Mercator
	CIB_PIT_Lambert _lambert; // projection Lambert
	CIB_PIT_UTM _utm; // projection UTM
	CIB_PIT_PolarStereographic _polarStereographic; // projection
													// Stereographique polaire
	CIB_PIT_CylindricEd _cylindricEd; // projection Cylindrique equidistante

	public CIB_PIT_Projection(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_Projection(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	private void initialize() {
		load();
	}

	public void load() {

		Group l_group = new Group(this, SWT.BOLD);
		l_group.setBounds(new Rectangle(0, 0, 450, 250));
		l_group.setText(Messages.getString("CIB_PIT_Projection.6")); //$NON-NLS-1$

		// label
		Label l_label = new Label(l_group, SWT.BOLD);
		l_label.setBounds(new Rectangle(15, 20, 180, 22));
		l_label.setText(Messages.getString("CIB_PIT_Projection.7")); //$NON-NLS-1$

		// Combo type de selection
		_projectionMenu = new Combo(l_group, SWT.READ_ONLY);
		_projectionMenu.setBounds(new CIB_PIT_Rect(210, 20, 200, 22).getRect());
		_projectionMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_projectionMenu.add(Messages.getString("CIB_PIT_Projection.8")); //$NON-NLS-1$
		_projectionMenu.add(Messages.getString("CIB_PIT_Projection.9")); //$NON-NLS-1$
		_projectionMenu.add(Messages.getString("CIB_PIT_Projection.10")); //$NON-NLS-1$
		_projectionMenu.add(Messages.getString("CIB_PIT_Projection.11")); //$NON-NLS-1$
		_projectionMenu.add(Messages.getString("CIB_PIT_Projection.12")); //$NON-NLS-1$
		_projectionMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				projectionOption();
			}
		});

		Composite l_panel = new Composite(l_group, SWT.NONE);
		l_panel.setBounds(new CIB_PIT_Rect(10, 50, 435, 190).getRect());

		// Panneau Mercator
		_mercatorCont = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.14"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		_mercator = new CIB_PIT_Mercator(_mercatorCont, CIB_PIT_k_MERCATOR, new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.15"), _helpContainer); //$NON-NLS-1$

		// Panneau Lambert
		_lambertCont = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.17"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		_lambert = new CIB_PIT_Lambert(_lambertCont, CIB_PIT_k_LAMBERT, new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.18"), _helpContainer); //$NON-NLS-1$

		// Panneau Utm
		_utmCont = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.20"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		_utm = new CIB_PIT_UTM(_utmCont, CIB_PIT_k_UTM, new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.21"), _helpContainer); //$NON-NLS-1$

		// Panneau Stereographique Polaire
		_stereoCont = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.23"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		_polarStereographic = new CIB_PIT_PolarStereographic(_stereoCont, CIB_PIT_k_POLARSTEREOGRAPHIC, new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.24"), _helpContainer); //$NON-NLS-1$

		// Panneau Cylindrique Equidistante
		_cylindCont = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.26"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		_cylindricEd = new CIB_PIT_CylindricEd(_cylindCont, CIB_PIT_k_CYLINDRICED, new CIB_PIT_Rect(0, 0, 435, 190), Messages.getString("CIB_PIT_Projection.27"), _helpContainer); //$NON-NLS-1$

		_contArray = new CIB_PIT_Component[] { _mercatorCont, _lambertCont, _utmCont, _stereoCont, _cylindCont };

		_projectionMenu.select(CIB_PIT_k_PROJECTION_MERCATOR);
		projectionOption();

	}

	protected void projectionOption() {
		for (int l_idx = 0; l_idx < _contArray.length; l_idx++) {
			_contArray[l_idx].setVisible(l_idx == _projectionMenu.getSelectionIndex());
		}
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		int l_projection = 0;
		String l_projectionType;
		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		l_projectionType = CIB_DPR_ProcessParameter.read(l_filePara, CIB_PIT_k_TYPE_PROJECTION);

		if (l_projectionType.equals(CIB_PIT_k_MERCATOR)) {
			l_projection = CIB_PIT_k_PROJECTION_MERCATOR;
			_mercator.update(l_filePara);
		} else if (l_projectionType.equals(CIB_PIT_k_LAMBERT)) {
			l_projection = CIB_PIT_k_PROJECTION_LAMBERT;
			_lambert.update(l_filePara);
		} else if (l_projectionType.equals(CIB_PIT_k_UTM)) {
			l_projection = CIB_PIT_k_PROJECTION_UTM;
			_utm.update(l_filePara);
		} else if (l_projectionType.equals(CIB_PIT_k_POLARSTEREOGRAPHIC)) {
			l_projection = CIB_PIT_k_PROJECTION_STEREOGRAPHIQUE_POLAIRE;
			_polarStereographic.update(l_filePara);
		} else if (l_projectionType.equals(CIB_PIT_k_CYLINDRICED)) {
			l_projection = CIB_PIT_k_PROJECTION_CYLINDRIQUE_EQUIDISTANTE;
			_cylindricEd.update(l_filePara);
		}

		_projectionMenu.select(l_projection);
		projectionOption();
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		int l_projection;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_projection = _projectionMenu.getSelectionIndex();

		switch (l_projection) {
		case CIB_PIT_k_PROJECTION_MERCATOR:
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_TYPE_PROJECTION, CIB_PIT_k_MERCATOR);
			_mercator.dump(l_filePara);
			break;
		case CIB_PIT_k_PROJECTION_LAMBERT:
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_TYPE_PROJECTION, CIB_PIT_k_LAMBERT);
			_lambert.dump(l_filePara);
			break;
		case CIB_PIT_k_PROJECTION_UTM:
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_TYPE_PROJECTION, CIB_PIT_k_UTM);
			_utm.dump(l_filePara);
			break;
		case CIB_PIT_k_PROJECTION_STEREOGRAPHIQUE_POLAIRE:
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_TYPE_PROJECTION, CIB_PIT_k_POLARSTEREOGRAPHIC);
			_polarStereographic.dump(l_filePara);
			break;
		case CIB_PIT_k_PROJECTION_CYLINDRIQUE_EQUIDISTANTE:
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_TYPE_PROJECTION, CIB_PIT_k_CYLINDRICED);
			_cylindricEd.dump(l_filePara);
			break;
		}

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {
		boolean l_result = true;
		int l_projection;

		l_projection = _projectionMenu.getSelectionIndex();

		switch (l_projection) {
		case CIB_PIT_k_PROJECTION_MERCATOR:
			l_result = _mercator.validate(p_warning, p_context);
			break;
		case CIB_PIT_k_PROJECTION_LAMBERT:
			l_result = _lambert.validate(p_warning, p_context);
			break;
		case CIB_PIT_k_PROJECTION_UTM:
			l_result = _utm.validate(p_warning, p_context);
			break;
		case CIB_PIT_k_PROJECTION_STEREOGRAPHIQUE_POLAIRE:
			l_result = _polarStereographic.validate(p_warning, p_context);
			break;
		case CIB_PIT_k_PROJECTION_CYLINDRIQUE_EQUIDISTANTE:
			l_result = _cylindricEd.validate(p_warning, p_context);
			break;
		}

		return l_result;
	}

}
