package fr.ifremer.globe.toolbox.cib_pit;

import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Error;

public class CIB_PIT_OptionMenu extends CIB_PIT_PanelChooser {

	protected Combo _optionMenu;

	public CIB_PIT_OptionMenu(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		_optionMenu = null;

		initialize(p_attributes);
	}

	private void initialize(StringBuffer p_attributes) {

		load();
		_optionMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		_optionMenu.setSize(getSize());
		_optionMenu.addMouseTrackListener(_helpTracker);

		if (_allItems.size() > 0) {
			for (Iterator<CIB_PIT_Item> itr = _allItems.iterator(); itr.hasNext();) {
				_optionMenu.add(itr.next().label());
			}
		} else {
			CIB_STC_Error.logError(Messages.getString("CIB_PIT_OptionMenu.0") + _name, null); //$NON-NLS-1$
		}

		_optionMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				changeValue();
			}
		});

		_optionMenu.select(0);

	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		boolean l_itemFound = false;
		int l_idx;
		String l_value;

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);

		for (l_idx = 0; l_idx < _allItems.size() && !l_itemFound; l_idx++) {
			if (l_value.equals(_allItems.get(l_idx).name())) {
				l_itemFound = true;
			}
		}

		if (l_itemFound) {
			_optionMenu.select(l_idx - 1);
		} else {
			CIB_STC_Error.logError(Messages.getString("CIB_PIT_OptionMenu.1") + _name, null); //$NON-NLS-1$
		}
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		String l_value;

		l_value = getValue();

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);

	}

	@Override
	public void refresh() {

		String l_value;

		clear();
		l_value = getValue();

		newItem(l_value);

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {
		return true;
	}

	protected void changeValue() {

		refreshAll();

	}

	public void load() {

		_optionMenu = new Combo(this, SWT.READ_ONLY);

	}

	@Override
	public String getValue() {
		int l_selected;
		String l_value;

		l_selected = _optionMenu.getSelectionIndex();
		if (l_selected == -1) {
			l_selected = 0;
		}
		l_value = (_allItems.get(l_selected)).name();

		return l_value;
	}

}
