package fr.ifremer.globe.toolbox.cib_pit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Directory;

public class CIB_PIT_FileSelector extends CIB_PIT_Component {

	public static final String CIP_PIT_k_GOUPFOLDER = "../"; //$NON-NLS-1$
	public static final String CIB_PIT_k_ALLFILES = "/*"; //$NON-NLS-1$

	protected CIB_PIT_TextField _filterField;
	protected CIB_PIT_List _fileList;
	protected CIB_PIT_TextField _selectionField;

	public CIB_PIT_FileSelector(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);
		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		CIB_PIT_Label l_label;

		// Filtre
		l_label = new CIB_PIT_Label(this, "", new CIB_PIT_Rect(2, 0, 150, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_FileSelector.4")); //$NON-NLS-1$
		_filterField = new CIB_PIT_TextField(this, "", new CIB_PIT_Rect(2, 25, 295, 22), Messages.getString("CIB_PIT_FileSelector.6"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		_filterField.getTextField().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					filter();
				}
			}
		});

		// Boutons
		CIB_PIT_Component l_btn = new CIB_PIT_Component(this, "", new CIB_PIT_Rect(240, 60, 60, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		GridLayout layout = new GridLayout(2, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		l_btn.setLayout(layout);

		Button l_button;
		l_button = new Button(l_btn, SWT.PUSH);
		l_button.setText("*"); //$NON-NLS-1$
		l_button.setLayoutData(new GridData(SWT.BEGINNING, SWT.FILL, true, true));
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_fileList.getList().selectAll();
				_fileList.getList().deselect(0);
			}
		});

		l_button = new Button(l_btn, SWT.PUSH);
		l_button.setText("x"); //$NON-NLS-1$
		l_button.setLayoutData(new GridData(SWT.END, SWT.FILL, true, true));
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_fileList.getList().deselectAll();
			}
		});

		l_btn.layout();

		// Liste
		l_label = new CIB_PIT_Label(this, "", new CIB_PIT_Rect(2, 90, 150, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_FileSelector.13")); //$NON-NLS-1$
		_fileList = new CIB_PIT_List(this, "", new CIB_PIT_Rect(2, 115, 295, 245), Messages.getString("CIB_PIT_FileSelector.15"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		_fileList.getList().addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectFile();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				selectFile();
			}
		});

		_fileList.getList().addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				listFile();
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}
		});

		// Selection
		l_label = new CIB_PIT_Label(this, "", new CIB_PIT_Rect(2, 380, 150, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_FileSelector.18")); //$NON-NLS-1$
		_selectionField = new CIB_PIT_TextField(this, "", new CIB_PIT_Rect(2, 405, 295, 22), Messages.getString("CIB_PIT_FileSelector.20"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public void setPath(String p_path) {
		p_path += CIB_PIT_k_ALLFILES;
		_filterField.setText(p_path);
		filter();
	}

	public void setSelectedFile(String p_fileName) {

		_selectionField.setText(p_fileName);

		String l_path = ""; //$NON-NLS-1$
		int l_idx = p_fileName.lastIndexOf('/');
		if (l_idx > -1) {
			l_path = p_fileName.substring(0, l_idx);
		}
		setPath(l_path);

		filter();
	}

	public void getSelectedFiles(List<String> p_list) {
		p_list.clear();

		String l_path = _filterField.getText();
		int l_idx = l_path.lastIndexOf('/');
		if (l_idx > -1) {
			l_path = l_path.substring(0, l_idx + 1);
		}

		if (_fileList.getList().getSelectionCount() > 0) {
			String[] l_selected = _fileList.getList().getSelection();
			for (int l_loop = 0; l_loop < l_selected.length; l_loop++) {
				p_list.add(l_path.concat(l_selected[l_loop]));
			}
		} else {
			String l_field = _selectionField.getText();
			l_field = l_field.replace('\\', '/');
			p_list.add(l_field);
		}
	}

	public void filter() {

		int l_idx = 0;
		String l_fileName;
		String l_field;
		List<String> l_fileList = new ArrayList<String>();

		l_field = _filterField.getText();
		l_field = l_field.replace('\\', '/');
		_filterField.setText(l_field);
		CIB_STC_Directory.scan(l_field, l_fileList, true);

		_fileList.getList().removeAll();
		_fileList.getList().add(CIP_PIT_k_GOUPFOLDER);

		for (Iterator<String> itr = l_fileList.iterator(); itr.hasNext();) {
			l_fileName = itr.next();

			l_idx = l_fileName.lastIndexOf('/');
			if (l_idx == (l_fileName.length() - 1)) {
				l_idx = l_fileName.lastIndexOf('/', l_idx - 1);
			}

			_fileList.getList().add(l_fileName.substring(l_idx + 1));
		}
	}

	protected void selectFile() {

		String l_fileName = _filterField.getText();
		int l_idx = l_fileName.lastIndexOf('/');
		if (l_idx > -1) {
			l_fileName = l_fileName.substring(0, l_idx + 1);
		}

		if (_fileList.getList().isSelected(0)) {
			_fileList.getList().deselect(0);
		}

		if (_fileList.getList().getSelectionCount() > 0) {
			String l_selectFile = _fileList.getList().getSelection()[0];
			l_fileName += l_selectFile;
		}

		l_idx = l_fileName.lastIndexOf('/');
		if (l_idx != -1 && l_idx == (l_fileName.length() - 1)) {
			l_fileName.substring(0, l_idx);
		}

		_selectionField.setText(l_fileName);

	}

	protected void listFile() {

		String l_scanDir;

		if (_fileList.getList().getSelectionCount() == 0) {
			l_scanDir = CIP_PIT_k_GOUPFOLDER;
		} else {
			l_scanDir = _fileList.getList().getSelection()[0];
		}

		if (l_scanDir.charAt(l_scanDir.length() - 1) == '/' || l_scanDir.equals(CIP_PIT_k_GOUPFOLDER)) {

			// Recuperer le filtre courant
			String l_filter = _filterField.getText();
			int l_idx = l_filter.lastIndexOf('/');
			if (l_idx != -1) {
				if (l_scanDir.equals(CIP_PIT_k_GOUPFOLDER)) {
					// Remonter d'un repertoire
					l_scanDir = l_filter.substring(0, l_idx + 1);
					l_filter = l_filter.substring(l_idx + 1);
					l_idx = l_scanDir.lastIndexOf('/', l_idx - 1);
					if (l_idx != -1) {
						l_scanDir = l_scanDir.substring(0, l_idx + 1);
					}
					l_filter = l_scanDir + l_filter;
				} else {
					// Descendre d'un repertoire
					l_filter = l_filter.substring(0, l_idx + 1) + l_scanDir + l_filter.substring(l_idx + 1);
				}
			}

			// Listing du nouveau repertoire avec le filtre
			_filterField.setText(l_filter);
			filter();
			selectFile();

		}
	}

}
