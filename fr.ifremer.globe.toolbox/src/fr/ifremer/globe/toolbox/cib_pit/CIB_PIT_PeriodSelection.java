package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_PeriodSelection extends CIB_PIT_ProcessParameter {

	public static final int CIB_PIT_k_CRITERIA_SELECTION_NONE = 0;
	public static final int CIB_PIT_k_CRITERIA_SELECTION_PERIOD = 1;
	public static final int CIB_PIT_k_CRITERIA_SELECTION_TIME_DIVISION_FILE = 2;

	protected static final String CIB_PIT_k_PERIOD = "PERIOD"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_PERIOD_SELECTION = "PERIOD_SELECTION"; // ident. selection sur periode //$NON-NLS-1$
	protected static final String CIB_PIT_k_PERIOD_OPTION = "PERIOD_OPTION"; // ident. type de selection //$NON-NLS-1$
	protected static final String CIB_PIT_k_DIVISION_TIME_FILE = "DIVISION_TIME_FILE"; // ident. fichier de decoupe //$NON-NLS-1$

	protected int _selectionType; // type de selection
	protected Combo _option; // Combo type de selection
	protected CIB_PIT_Component _periodCont; // Conteneur periode
	protected CIB_PIT_Component _timeDivisionCont; // Conteneur time divisin
													// file
	protected CIB_PIT_Period _period; // periode
	protected CIB_PIT_FileSelection _timeDivision; // fichier de decoupe

	public CIB_PIT_PeriodSelection(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	private void initialize() {
		load();
	}

	public void load() {

		CIB_PIT_Label l_lab;

		// label
		Label l_label = new Label(this, SWT.BOLD);
		l_label.setBounds(new CIB_PIT_Rect(0, 2, 220, 22).getRect());
		l_label.setText(Messages.getString("CIB_PIT_PeriodSelection.4")); //$NON-NLS-1$

		// Combo type de selection
		_option = new Combo(this, SWT.READ_ONLY);
		_option.setBounds(new CIB_PIT_Rect(240, 2, 207, 22).getRect());
		_option.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_option.add(Messages.getString("CIB_PIT_PeriodSelection.5")); //$NON-NLS-1$
		_option.add(Messages.getString("CIB_PIT_PeriodSelection.6")); //$NON-NLS-1$
		_option.add(Messages.getString("CIB_PIT_PeriodSelection.7")); //$NON-NLS-1$
		_option.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				periodOption();
			}
		});

		Composite l_panel = new Composite(this, SWT.NONE);
		l_panel.setBounds(new CIB_PIT_Rect(0, 37, 600, 98).getRect());

		// Panneau periode
		_periodCont = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 600, 98), Messages.getString("CIB_PIT_PeriodSelection.9"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		_period = new CIB_PIT_Period(_periodCont, CIB_PIT_k_PERIOD, new CIB_PIT_Rect(90, 0, 600, 98), Messages.getString("CIB_PIT_PeriodSelection.10"), _helpContainer, CIB_PIT_Period.CIB_PIT_k_V); //$NON-NLS-1$

		// Panneau fichier de d�coupe
		_timeDivisionCont = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 10, 600, 98), Messages.getString("CIB_PIT_PeriodSelection.12"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		l_lab = new CIB_PIT_Label(_timeDivisionCont, "", new CIB_PIT_Rect(0, 0, 220, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_PeriodSelection.15")); //$NON-NLS-1$

		_timeDivision = new CIB_PIT_FileSelection(_timeDivisionCont, CIB_PIT_k_DIVISION_TIME_FILE, new CIB_PIT_Rect(240, 0, 332, 22), Messages.getString("CIB_PIT_PeriodSelection.16"), _helpContainer); //$NON-NLS-1$

		_option.select(CIB_PIT_k_CRITERIA_SELECTION_NONE);
		periodOption();

	}

	protected void periodOption() {

		switch (_option.getSelectionIndex()) {
		case CIB_PIT_k_CRITERIA_SELECTION_NONE:
			_periodCont.setVisible(false);
			_timeDivisionCont.setVisible(false);
			break;
		case CIB_PIT_k_CRITERIA_SELECTION_PERIOD:
			_periodCont.setVisible(true);
			_timeDivisionCont.setVisible(false);
			break;
		case CIB_PIT_k_CRITERIA_SELECTION_TIME_DIVISION_FILE:
			_periodCont.setVisible(false);
			_timeDivisionCont.setVisible(true);
			break;

		}

	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		int l_option;
		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		l_option = CIB_DPR_ProcessParameter.readInt(l_filePara, CIB_PIT_k_PERIOD_OPTION);

		_period.update(l_filePara);

		_timeDivision.update(l_filePara);

		_option.select(l_option);
		periodOption();

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_result = true;
		String l_context;

		l_context = p_context;
		l_context = l_context.concat("->"); //$NON-NLS-1$
		l_context = l_context.concat(_name);

		if ((l_result = _period.validate(p_warning, l_context))) {
			l_result = _timeDivision.validate(p_warning, l_context);
		}

		return l_result;
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		CIB_DPR_ProcessParameter.writeInt(l_filePara, CIB_PIT_k_PERIOD_OPTION, _option.getSelectionIndex());
		_period.dump(l_filePara);
		_timeDivision.dump(l_filePara);

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());

	}

}
