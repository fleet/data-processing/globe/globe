package fr.ifremer.globe.toolbox.cib_pit;

import java.io.File;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_FileMany;
import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_FileSelection extends CIB_PIT_ProcessParameter {

	Text _text;
	Button _button;

	public CIB_PIT_FileSelection(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_FileSelection(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
		_text.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		if (_helpTracker != null) {
			_text.addMouseTrackListener(_helpTracker);
			_button.addMouseTrackListener(_helpTracker);
		}

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;
		String l_value;
		String l_context;
		String l_warning;

		l_value = _text.getText();

		l_context = p_context;
		l_context = l_context.concat("->"); //$NON-NLS-1$
		l_context = l_context.concat(_name);

		if (l_value.isEmpty()) {
			l_warning = l_context;
			l_warning = l_warning.concat("->"); //$NON-NLS-1$

			l_warning = l_warning.concat(Messages.getString("CIB_PIT_FileSelection.2")); //$NON-NLS-1$
			p_warning.add(l_warning);
		} else {
			File f = new File(l_value);
			if (!f.exists()) {
				l_warning = l_context;
				l_warning = l_warning.concat("->"); //$NON-NLS-1$

				l_warning = l_warning.concat(Messages.getString("CIB_PIT_FileSelection.4") + l_value + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				p_warning.add(l_warning);
			}
		}

		return l_status;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_value = ""; //$NON-NLS-1$
		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		_text.setText(l_value);
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		String l_value;

		l_value = _text.getText();
		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);

	}

	public void load() {

		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		_text = new Text(this, SWT.BOLD | SWT.SINGLE | SWT.BORDER);
		_text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		_button = new Button(this, SWT.PUSH);
		_button.setText(Messages.getString("CIB_PIT_FileSelection.7")); //$NON-NLS-1$
		_button.setLayoutData(new GridData(SWT.END, SWT.FILL, false, true));
		_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectFile();
			}
		});

		layout();

	}

	protected void selectFile() {
		String l_value = _text.getText();
		if (l_value.isEmpty()) {
			l_value = CIB_PIT_Component._defaultPath + "/"; //$NON-NLS-1$
		}
		if(_name.equals(CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_ROOT_NAME)) {
			DirectoryDialog directoryDialog = new DirectoryDialog(getShell());
			l_value = l_value.replace('\\', '/');
			int l_index = l_value.lastIndexOf('/');
			if (l_index > 0) {
				String l_path = l_value.substring(0, l_index + 1);
				String l_name = l_value.substring(l_index + 1, l_value.length());
				directoryDialog.setText(l_path);
				directoryDialog.setFilterPath(l_name);

			}
			String path = directoryDialog.open();
			if (path != null) {
				path = path.replace('\\', '/');
				_text.setText(path + '/');
			}
		}else {
			FileDialog dialog = new FileDialog(getShell(), SWT.NULL);
			l_value = l_value.replace('\\', '/');
			int l_index = l_value.lastIndexOf('/');
			if (l_index > 0) {
				String l_path = l_value.substring(0, l_index + 1);
				String l_name = "";
				if(_name.equals(CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_FILE)) {
					l_name = l_value.substring(l_index + 1, l_value.length());
				} 		
				dialog.setFilterPath(l_path);
				dialog.setFileName(l_name);
			}

			String path = dialog.open();
			if (path != null) {
				path = path.replace('\\', '/');
				_text.setText(path);
			}
		}
	}

	public void set(String p_fileName) {
		_text.setText(p_fileName);
	}

	public String get() {
		return _text.getText();
	}

}
