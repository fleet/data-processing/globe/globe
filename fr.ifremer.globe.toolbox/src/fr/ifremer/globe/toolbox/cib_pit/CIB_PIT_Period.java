package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Period extends CIB_PIT_ProcessParameter {

	public static final String CIB_PIT_k_H = "H"; // disposition horizontale //$NON-NLS-1$
	public static final String CIB_PIT_k_V = "V"; // disposition verticale	 //$NON-NLS-1$
	protected static final String CIB_DPR_k_DATESTART = "DATE_START"; // date de debut de periode //$NON-NLS-1$
	protected static final String CIB_DPR_k_DATEEND = "DATE_END"; // date de fin de periode //$NON-NLS-1$

	protected CIB_PIT_Date _date1; // date de debut de periode
	protected CIB_PIT_Date _date2; // date de fin de periode

	public CIB_PIT_Period(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		String l_option = extractString(p_attributes);
		if (l_option.length() == 0) {
			l_option = CIB_PIT_k_V;
		}

		initialize(l_option);
	}

	public CIB_PIT_Period(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer, String p_option) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize(p_option);
	}

	public void initialize(String p_option) {

		load(p_option);
	}

	public void load(String p_option) {

		CIB_PIT_Label l_lab;

		if (p_option.equals(CIB_PIT_k_V)) {
			Group l_grp = new Group(this, SWT.SHADOW_ETCHED_IN);
			l_grp.setText(Messages.getString("CIB_PIT_Period.4")); //$NON-NLS-1$
			l_grp.setSize(490, 90);

			l_lab = new CIB_PIT_Label(l_grp, "Label", new CIB_PIT_Rect(15, 20, 170, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
			l_lab.setText(Messages.getString("CIB_PIT_Period.7")); //$NON-NLS-1$

			_date1 = new CIB_PIT_Date(l_grp, CIB_DPR_k_DATESTART, new CIB_PIT_Rect(200, 20, 196, 22), Messages.getString("CIB_PIT_Period.8"), _helpContainer); //$NON-NLS-1$

			l_lab = new CIB_PIT_Label(l_grp, "Label", new CIB_PIT_Rect(15, 54, 170, 22), Messages.getString("CIB_PIT_Period.10"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
			l_lab.setText(Messages.getString("CIB_PIT_Period.11")); //$NON-NLS-1$

			_date2 = new CIB_PIT_Date(l_grp, CIB_DPR_k_DATEEND, new CIB_PIT_Rect(200, 54, 196, 22), Messages.getString("CIB_PIT_Period.12"), _helpContainer); //$NON-NLS-1$

		} else {

			l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(2, 0, 30, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
			l_lab.setText(Messages.getString("CIB_PIT_Period.15")); //$NON-NLS-1$

			_date1 = new CIB_PIT_Date(this, CIB_DPR_k_DATESTART, new CIB_PIT_Rect(36, 0, 196, 22), Messages.getString("CIB_PIT_Period.16"), _helpContainer); //$NON-NLS-1$

			l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(250, 0, 20, 22), Messages.getString("CIB_PIT_Period.18"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
			l_lab.setText(Messages.getString("CIB_PIT_Period.19")); //$NON-NLS-1$

			_date2 = new CIB_PIT_Date(this, CIB_DPR_k_DATEEND, new CIB_PIT_Rect(275, 0, 196, 22), Messages.getString("CIB_PIT_Period.20"), _helpContainer); //$NON-NLS-1$

		}
	}

	public void get(String p_date) {
		_date1.getDate();
		_date2.getDate();
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		_date1.update(l_filePara);
		_date2.update(l_filePara);
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		_date1.dump(l_filePara);
		_date2.dump(l_filePara);

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;

		if (!(_date1.getIntDate() <= _date2.getIntDate())) {
			l_status = false;
			MessageBox mb = new MessageBox(getShell(), SWT.ICON_ERROR | SWT.OK);
			mb.setText(Messages.getString("CIB_PIT_Period.21")); //$NON-NLS-1$
			mb.setMessage(Messages.getString("CIB_PIT_Period.22")); //$NON-NLS-1$
			mb.open();

		}

		return l_status;
	}

}
