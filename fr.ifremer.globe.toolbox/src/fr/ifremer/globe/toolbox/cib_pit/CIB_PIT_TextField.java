package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_TextField extends CIB_PIT_ProcessParameter {

	protected Text _text;

	public CIB_PIT_TextField(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_TextField(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
		_text.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		_text.setSize(getSize());
		if (_helpTracker != null) {
			_text.addMouseTrackListener(_helpTracker);
		}
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;
		String l_value;
		String l_context;
		String l_warning;

		l_value = _text.getText();

		if (l_value.isEmpty()) {
			l_context = p_context;
			l_context = l_context.concat("->"); //$NON-NLS-1$
			l_context = l_context.concat(_name);

			l_warning = l_context;
			l_warning = l_warning.concat("->"); //$NON-NLS-1$

			l_warning = l_warning.concat(Messages.getString("CIB_PIT_TextField.2")); //$NON-NLS-1$
			p_warning.add(l_warning);
		}

		return l_status;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_value = ""; //$NON-NLS-1$
		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		_text.setText(l_value);
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		String l_value;

		l_value = _text.getText();
		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);

	}

	public void load() {

		_text = new Text(this, SWT.BOLD | SWT.SINGLE | SWT.BORDER);

	}

	public void setText(String p_text) {
		_text.setText(p_text);
	}

	public String getText() {
		return _text.getText();
	}

	public Text getTextField() {
		return _text;
	}

}
