package fr.ifremer.globe.toolbox.cib_pit;

import java.util.StringTokenizer;

import org.eclipse.swt.graphics.Rectangle;

public class CIB_PIT_Rect {

	protected Rectangle _rect;

	public CIB_PIT_Rect() {
		_rect = new Rectangle(0, 0, 100, 100);
	}

	public CIB_PIT_Rect(int p_x, int p_y, int p_w, int p_h) {
		_rect = new Rectangle(p_x, p_y, p_w, p_h);
	}

	public CIB_PIT_Rect(String p_rectangle) {
		_rect = new Rectangle(0, 0, 100, 100);
		setValues(p_rectangle);
	}

	public Rectangle getRect() {
		return _rect;
	}

	public void setValues(String p_rectangle) {

		StringTokenizer l_token = new StringTokenizer(p_rectangle);
		String l_tmp;

		l_tmp = l_token.nextToken();
		_rect.x = Integer.parseInt(l_tmp);

		l_tmp = l_token.nextToken();
		_rect.y = Integer.parseInt(l_tmp);

		l_tmp = l_token.nextToken();
		_rect.width = Integer.parseInt(l_tmp);

		l_tmp = l_token.nextToken();
		_rect.height = Integer.parseInt(l_tmp);
	}

}
