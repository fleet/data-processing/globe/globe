package fr.ifremer.globe.toolbox.cib_pit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_pit.CIB_PIT_SelectionBox.ISelectionBox;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_FileChannelListSelection extends CIB_PIT_FileSelection implements ISelectionBox {

	public static final String CIB_PIT_k_FILECHANNEL_CHANNEL = "Channel"; // suffixe parametre liste //$NON-NLS-1$
																			// canaux
	public static final String CIB_PIT_k_CHANNELS_SEPARATOR = ";"; // separateur //$NON-NLS-1$

	protected Button _channelButton;
	protected Text _channelField;
	protected CIB_PIT_ChannelListSelectionBox _channelSelectionBox;

	public CIB_PIT_FileChannelListSelection(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes,
			Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
	}

	@Override
	public void initialize() {
		// super.initialize();
		load();
		_text.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		if (_helpTracker != null) {
			_text.addMouseTrackListener(_helpTracker);
			_button.addMouseTrackListener(_helpTracker);
			_channelButton.addMouseTrackListener(_helpTracker);
			_channelField.addMouseTrackListener(_helpTracker);
		}
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;

		if (_channelSelectionBox != null) {
			l_status = _channelSelectionBox.validate(p_warning, p_context);
		}

		return l_status;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_value = ""; //$NON-NLS-1$
		String l_channels = ""; //$NON-NLS-1$
		List<String> l_channelList = new ArrayList<>();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		_text.setText(l_value);

		CIB_DPR_ProcessParameter.read(p_paramFile, _name + CIB_PIT_k_FILECHANNEL_CHANNEL, l_channelList);
		for (String element : l_channelList) {
			l_channels += element + CIB_PIT_k_CHANNELS_SEPARATOR;
		}
		if (!l_channels.isEmpty()) {
			l_channels = l_channels.substring(0, l_channels.length() - 1);
		}
		_channelField.setText(l_channels);

		if (_channelSelectionBox != null) {
			_channelSelectionBox.setFile(l_value);
			_channelSelectionBox.setSelection(l_channelList);
		}
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		String l_value;
		List<String> l_list = new ArrayList<>();

		l_value = _text.getText();
		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);

		_channelSelectionBox.getSelection(l_list);
		CIB_DPR_ProcessParameter.write(p_filePara, _name + CIB_PIT_k_FILECHANNEL_CHANNEL, l_list);

	}

	@Override
	public void load() {

		CIB_PIT_Label l_lab;

		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 2;
		layout.marginWidth = 0;
		setLayout(layout);

		_text = new Text(this, SWT.BOLD | SWT.SINGLE | SWT.BORDER);
		_text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		_button = new Button(this, SWT.PUSH);
		_button.setText(Messages.getString("CIB_PIT_FileChannelListSelection.0")); //$NON-NLS-1$
		_button.setLayoutData(new GridData(SWT.END, SWT.FILL, false, true));
		_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectFile();
			}
		});

		// Selection de canaux
		Composite l_panel = new Composite(this, SWT.NONE);
		l_panel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		l_panel.setLayout(layout);

		// Panneau nom generique
		l_lab = new CIB_PIT_Label(l_panel, "Label", new CIB_PIT_Rect(0, 50, 80, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_FileChannelListSelection.1")); //$NON-NLS-1$

		_channelField = new Text(l_panel, SWT.BOLD | SWT.SINGLE | SWT.BORDER);
		_channelField.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		_channelField.setEnabled(false);

		_channelButton = new Button(this, SWT.PUSH);
		_channelButton.setText(Messages.getString("CIB_PIT_FileChannelListSelection.0")); //$NON-NLS-1$
		_channelButton.setLayoutData(new GridData(SWT.END, SWT.FILL, false, true));
		_channelButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectChannel();
			}
		});

		// ************************************************************
		// Creation de la ChannelistSelectionBox
		// ************************************************************
		_channelSelectionBox = new CIB_PIT_ChannelListSelectionBox(getDisplay(), _name, false);
		_channelSelectionBox.setClientData(this);

		layout();
	}

	public void selectChannel() {
		if (_channelSelectionBox != null) {

			String l_dtmName = _text.getText();

			File f = new File(l_dtmName);
			if (!f.exists()) {
				MessageDialog.openError(getShell(), Messages.getString("CIB_PIT_ChannelListSelectionBox.0"), //$NON-NLS-1$
						Messages.getString("CIB_PIT_FileChannelListSelection.2")); //$NON-NLS-1$
			} else {
				_channelSelectionBox.setFile(l_dtmName);
				_channelSelectionBox.setList(IFileService.grab().getLayers(l_dtmName));
				_channelSelectionBox.show();
			}
		}
	}

	@Override
	public void apply(CIB_PIT_SelectionBox selectionBox) {
		String l_channels = ""; //$NON-NLS-1$
		List<String> l_channelList = new ArrayList<>();
		_channelSelectionBox.getSelection(l_channelList);
		for (String element : l_channelList) {
			l_channels += element + CIB_PIT_k_CHANNELS_SEPARATOR;
		}
		if (!l_channels.isEmpty()) {
			l_channels = l_channels.substring(0, l_channels.length() - 1);
		}
		_channelField.setText(l_channels);

	}

	@Override
	public void run(CIB_PIT_SelectionBox selectionBox) {
		// TODO Auto-generated method stub

	}

	@Override
	public void cancel(CIB_PIT_SelectionBox selectionBox) {
		// TODO Auto-generated method stub

	}

}
