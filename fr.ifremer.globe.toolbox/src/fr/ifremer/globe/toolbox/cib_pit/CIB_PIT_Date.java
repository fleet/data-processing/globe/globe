package fr.ifremer.globe.toolbox.cib_pit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Date extends CIB_PIT_ProcessParameter {

	Composite _dateTime;
	DateTime _date;
	DateTime _time;

	public CIB_PIT_Date(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_Date(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
		if (_helpTracker != null) {
			_dateTime.addMouseTrackListener(_helpTracker);
			_date.addMouseTrackListener(_helpTracker);
			_time.addMouseTrackListener(_helpTracker);
		}
	}

	public void load() {

		_dateTime = new Composite(this, SWT.BORDER | SWT.CAP_ROUND);
		_dateTime.setSize(getSize());
		RowLayout l_datelayout = new RowLayout(SWT.HORIZONTAL);
		l_datelayout.marginTop = -1;
		l_datelayout.spacing = 20;
		l_datelayout.justify = true;
		_dateTime.setLayout(l_datelayout);

		_date = new DateTime(_dateTime, SWT.DATE | SWT.MEDIUM);
		_time = new DateTime(_dateTime, SWT.TIME | SWT.MEDIUM);

		_dateTime.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		_dateTime.layout();
	}

	public void setDate(String p_date) {

		int l_day = 0;
		int l_month = 0;
		int l_year = 0;
		int l_hours = 0;
		int l_minutes = 0;
		int l_seconds = 0;

		if (p_date.length() >= 17) {
			l_day = Integer.parseInt(p_date.substring(0, 2));
			l_month = Integer.parseInt(p_date.substring(3, 5));
			l_year = Integer.parseInt(p_date.substring(6, 8));
			l_hours = Integer.parseInt(p_date.substring(9, 11));
			l_minutes = Integer.parseInt(p_date.substring(12, 14));
			l_seconds = Integer.parseInt(p_date.substring(15, 17));
		}

		if (l_year < 38) {
			l_year += 2000;
		} else {
			l_year += 1900;
		}

		_date.setDate(l_year, l_month - 1, l_day);
		_time.setTime(l_hours, l_minutes, l_seconds);
	}

	public String getDate() {
		String l_date = ""; //$NON-NLS-1$

		l_date += String.format("%02d", _date.getDay()); //$NON-NLS-1$
		l_date += "/"; //$NON-NLS-1$
		l_date += String.format("%02d", _date.getMonth() + 1); //$NON-NLS-1$
		l_date += "/"; //$NON-NLS-1$
		l_date += String.format("%02d", _date.getYear() % 100); //$NON-NLS-1$
		l_date += " "; //$NON-NLS-1$
		l_date += String.format("%02d", _time.getHours()); //$NON-NLS-1$
		l_date += ":"; //$NON-NLS-1$
		l_date += String.format("%02d", _time.getMinutes()); //$NON-NLS-1$
		l_date += ":"; //$NON-NLS-1$
		l_date += String.format("%02d", _time.getSeconds()); //$NON-NLS-1$

		return l_date;

	}

	public int getIntDate() {
		return _date.getYear() * 1000000000 + _date.getMonth() * 10000000 + _date.getDay() * 100000 + _time.getHours() * 1000 + _time.getMinutes() * 10 + _time.getSeconds();
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_value = ""; //$NON-NLS-1$
		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		setDate(l_value);
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		String l_value;

		l_value = getDate();
		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);

	}

}
