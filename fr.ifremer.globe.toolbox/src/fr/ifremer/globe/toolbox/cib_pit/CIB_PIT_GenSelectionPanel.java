package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_FileGen;
import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_GenSelectionPanel extends CIB_PIT_ProcessParameter {

	protected CIB_PIT_FileSelection _rootNameSelection;
	protected CIB_PIT_TextField _extensionField;
	protected String _rootFileName = ""; //$NON-NLS-1$
	protected String _extension = ""; //$NON-NLS-1$

	public CIB_PIT_GenSelectionPanel(Composite p_parent, Label p_helpContainer) {
		super(p_parent, "", new CIB_PIT_Rect(0, 0, 650, 430), Messages.getString("CIB_PIT_GenSelectionPanel.3"), p_helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		CIB_PIT_Label l_label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(0, 10, 200, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_GenSelectionPanel.6")); //$NON-NLS-1$

		_rootNameSelection = new CIB_PIT_FileSelection(this, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_ROOT_NAME, new CIB_PIT_Rect(210, 10, 350, 26),
				Messages.getString("CIB_PIT_GenSelectionPanel.7"), _helpContainer); //$NON-NLS-1$

		l_label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(0, 50, 200, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_GenSelectionPanel.10")); //$NON-NLS-1$

		_extensionField = new CIB_PIT_TextField(this, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_EXTENSION, new CIB_PIT_Rect(210, 50, 80, 26),
				Messages.getString("CIB_PIT_GenSelectionPanel.11"), _helpContainer); //$NON-NLS-1$

	}

	public void select() {
		_rootNameSelection.set(_rootFileName);
		_extensionField.setText(_extension);
	}

	public String getMainValue() {
		return _rootFileName;
	}

	public void apply() {
		_rootFileName = _rootNameSelection.get();
		_extension = _extensionField.getText();
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		_rootFileName = CIB_DPR_ProcessParameter.read(p_paramFile, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_ROOT_NAME);
		_rootFileName = _rootFileName.trim();

		_extension = CIB_DPR_ProcessParameter.read(p_paramFile, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_EXTENSION);

		select();

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;
		String l_value;
		String l_context;
		String l_warning;

		l_value = _rootNameSelection.get();

		if (l_value.isEmpty()) {
			l_context = p_context;
			l_context = l_context.concat("->"); //$NON-NLS-1$
			l_context = l_context.concat(_name);

			l_warning = l_context;
			l_warning = l_warning.concat("->"); //$NON-NLS-1$

			l_warning = l_warning.concat(Messages.getString("CIB_PIT_GenSelectionPanel.14")); //$NON-NLS-1$
			p_warning.add(l_warning);
		}

		return l_status;
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		CIB_DPR_ProcessParameter.write(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_ROOT_NAME, _rootFileName);
		CIB_DPR_ProcessParameter.write(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_EXTENSION, _extension);

		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_START1, 0);
		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_END1, 999);
		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_LENGTH1, 3);

		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_START2, 0);
		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_END2, 999);
		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_LENGTH2, 3);

		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_START3, 0);
		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_END3, 999);
		CIB_DPR_ProcessParameter.writeInt(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_LENGTH3, 3);

	}

}
