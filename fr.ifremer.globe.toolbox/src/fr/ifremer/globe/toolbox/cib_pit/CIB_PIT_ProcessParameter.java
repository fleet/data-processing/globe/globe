package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Error;

public class CIB_PIT_ProcessParameter extends CIB_PIT_Component {

	public class ReadParameter {
		public String _name;
		public String _label;
		public int _lineNumber;
		public int _height;
		public String _paramBlock;
	}

	protected CIB_PIT_ProcessParameter _parent;
	protected boolean _isVisiblePanel;

	public CIB_PIT_ProcessParameter(Shell shell, StringBuffer p_attributes, Label p_helpContainer) {
		super(shell, p_helpContainer);

		_isVisiblePanel = true;
		_parent = null;
		initialize(p_attributes);
	}

	public CIB_PIT_ProcessParameter(Composite parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(parent, p_helpContainer);

		_isVisiblePanel = true;
		_parent = null;
		initialize(p_attributes);
	}

	public CIB_PIT_ProcessParameter(CIB_PIT_ProcessParameter parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(parent, p_helpContainer);

		_isVisiblePanel = true;
		_parent = parent;
		initialize(p_attributes);
	}

	public CIB_PIT_ProcessParameter(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		_isVisiblePanel = true;
		_parent = null;
	}

	private void initialize(StringBuffer p_attributes) {

		CIB_PIT_Rect l_bbox = new CIB_PIT_Rect();
		String l_helpMessage = null;
		StringTokenizer l_token;
		int l_pos;

		l_token = new StringTokenizer(p_attributes.toString());
		if (!l_token.hasMoreTokens()) {
			CIB_STC_Error.logError(Messages.getString("CIB_PIT_ProcessParameter.0"), null); //$NON-NLS-1$
		} else {
			_name = l_token.nextToken();
			l_pos = p_attributes.indexOf(_name);
			p_attributes.delete(l_pos, l_pos + _name.length());

			l_bbox.setValues(p_attributes.toString());
			this.setBounds(l_bbox.getRect());

			l_helpMessage = extractString(p_attributes);
		}

		if (l_helpMessage != null) {
			setHelpMessage(l_helpMessage);
		}

	}

	public boolean validate(List<String> p_warning, String p_context) {
		return true;
	}

	public void update(CIB_STC_AccessFilePara p_filePara) {

	}

	public void dump(CIB_STC_AccessFilePara p_filePara) {

	}

	public String getValue() {
		return null;
	}

	public CIB_PIT_ProcessParameter find(CIB_PIT_ProcessParameter p_client, String p_name) {

		CIB_PIT_ProcessParameter l_parameter = null;

		if (_name.equals(p_name)) {
			l_parameter = this;
		}

		return l_parameter;

	}

	public void refresh() {

	}

	public void refreshAll() {

	}

	public void show() {

		_isVisiblePanel = true;
		setVisible(true);

	}

	public boolean showChild(CIB_PIT_ProcessParameter p_client, String p_name, String p_conditions) {

		_isVisiblePanel = true;
		return true;
	}

	public void hide() {

		_isVisiblePanel = false;
		setVisible(false);
	}

	public boolean hideChild(CIB_PIT_ProcessParameter p_client, String p_name) {

		_isVisiblePanel = false;
		return true;
	}

	public String extractString(StringBuffer p_attributes) {

		int l_start;
		int l_end;
		String l_string = ""; //$NON-NLS-1$

		l_start = p_attributes.indexOf("\""); //$NON-NLS-1$
		if (l_start != -1) {
			l_end = p_attributes.indexOf("\"", l_start + 1); //$NON-NLS-1$
			if (l_end > 0) {
				l_string = p_attributes.substring(l_start + 1, l_end);
			}
			p_attributes.delete(0, l_end + 1);
		}

		return l_string;
	}

	public CIB_PIT_ProcessParameter getParentParameter() {
		return _parent;
	}

	public boolean equals(CIB_PIT_ProcessParameter p_other) {

		boolean l_retValue = false;

		if (p_other != null) {
			if (_parent == null) {
				l_retValue = ((p_other._parent == null) && (_name.equals(p_other._name)));
			} else {
				l_retValue = ((_parent.equals(p_other._parent)) && (_name.equals(p_other._name)));
			}
		}

		return l_retValue;
	}

	@Override
	public boolean isVisible() {

		if (_isVisiblePanel && _parent != null) {
			return (_parent.isVisible());
		} else {
			return (_isVisiblePanel);
		}
	}

	public void setParameterParent(CIB_PIT_ProcessParameter p_parent) {
		_parent = p_parent;
	}

}
