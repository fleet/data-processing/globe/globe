package fr.ifremer.globe.toolbox.cib_pit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Error;

public class CIB_PIT_PanelChooser extends CIB_PIT_ProcessParameter {

	protected List<CIB_PIT_Item> _allItems = new ArrayList<CIB_PIT_Item>();

	public CIB_PIT_PanelChooser(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize(p_attributes);
	}

	private void initialize(StringBuffer p_attributes) {

		String[] l_param = new String[2];
		CIB_STC_AccessFilePara l_paramFile = new CIB_STC_AccessFilePara();

		l_paramFile.setByString(p_attributes.toString());
		l_paramFile.first(l_param);

		if (l_paramFile.first(l_param)) {
			do {
				_allItems.add(new CIB_PIT_Item(l_param[0], l_param[1]));
			} while (l_paramFile.next(l_param));
		}

	}

	protected void clear() {

		for (Iterator<CIB_PIT_Item> itr = _allItems.iterator(); itr.hasNext();) {
			hidePan((itr.next()));
		}

	}

	@Override
	public void refreshAll() {

		CIB_PIT_ProcessParameter l_parent = this;

		while (l_parent.getParentParameter() != null) {
			l_parent = l_parent.getParentParameter();
		}

		l_parent.refresh();

	}

	public CIB_PIT_Item getItem(int p_itemIdx) {
		CIB_PIT_Item l_item = null;
		if (p_itemIdx < _allItems.size()) {
			l_item = _allItems.get(p_itemIdx);
		}
		return l_item;
	}

	protected void newItem(String p_itemName) {

		CIB_PIT_Item l_tstItem = null;
		CIB_PIT_Item l_item = null;
		CIB_PIT_ItemPanel l_itemPanel = null;
		List<CIB_PIT_ItemPanel> l_panels;

		for (Iterator<CIB_PIT_Item> itr = _allItems.iterator(); itr.hasNext() && l_item == null;) {
			l_tstItem = itr.next();
			if (l_tstItem.name().equals(p_itemName)) {
				l_item = l_tstItem;
			}
		}

		if (l_item != null) {

			l_panels = l_item.panels();
			for (Iterator<CIB_PIT_ItemPanel> itr2 = l_panels.iterator(); itr2.hasNext();) {
				l_itemPanel = itr2.next();
				if (!_parent.showChild(this, l_itemPanel.name(), l_itemPanel.conditions())) {
					CIB_STC_Error.logError(Messages.getString("CIB_PIT_PanelChooser.0") + l_itemPanel.name(), null); //$NON-NLS-1$
				}
			}
		}

	}

	protected void hidePan(CIB_PIT_Item p_item) {

		List<CIB_PIT_ItemPanel> l_panels = p_item.panels();

		for (Iterator<CIB_PIT_ItemPanel> itr = l_panels.iterator(); itr.hasNext();) {
			hidePan((itr.next()));
		}

	}

	protected void hidePan(CIB_PIT_ItemPanel p_itemPanel) {

		if (_parent != null) {
			if (!_parent.hideChild(this, p_itemPanel.name())) {
				CIB_STC_Error.logError(Messages.getString("CIB_PIT_PanelChooser.1") + p_itemPanel.name(), null); //$NON-NLS-1$
			}
		}

	}

}
