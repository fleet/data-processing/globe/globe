package fr.ifremer.globe.toolbox.cib_pit;

import java.util.ArrayList;

import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Error;
import fr.ifremer.globe.toolbox.tool.Tool;

public class CIB_PIT_ParameterPanel extends CIB_PIT_ProcessParameter {
	protected Logger logger = LoggerFactory.getLogger(Tool.class);

	

	final String CIB_PIT_k_BUTTON = "BUTTON"; //$NON-NLS-1$
	final String CIB_PIT_k_OPTION = "OPTION"; //$NON-NLS-1$
	final String CIB_PIT_k_FILE = "FILE"; //$NON-NLS-1$
	final String CIB_PIT_k_FILECHANNELLIST = "FILECHANNELLIST"; //$NON-NLS-1$
	final String CIB_PIT_k_FILECHANNEL = "FILECHANNEL"; //$NON-NLS-1$
	final String CIB_PIT_k_FILELIST = "FILELIST"; //$NON-NLS-1$
	final String CIB_PIT_k_FILEMEASURELIST = "FILEMEASURELIST"; //$NON-NLS-1$
	final String CIB_PIT_k_FRAME = "FRAME"; //$NON-NLS-1$
	final String CIB_PIT_k_LABEL = "LABEL"; //$NON-NLS-1$
	final String CIB_PIT_k_NUMBER = "NUMBER"; //$NON-NLS-1$
	final String CIB_PIT_k_LATITUDE = "LATITUDE"; //$NON-NLS-1$
	final String CIB_PIT_k_LONGITUDE = "LONGITUDE"; //$NON-NLS-1$
	final String CIB_PIT_k_ELLIPSOID = "ELLIPSOID"; //$NON-NLS-1$
	final String CIB_PIT_k_PROJECTION = "PROJECTION"; //$NON-NLS-1$
	final String CIB_PIT_k_PANEL = "PANEL"; //$NON-NLS-1$
	final String CIB_PIT_k_SEPARATOR = "SEPARATOR"; //$NON-NLS-1$
	final String CIB_PIT_k_TOGGLE = "TOGGLE"; //$NON-NLS-1$
	final String CIB_PIT_k_TEXT = "TEXT"; //$NON-NLS-1$
	final String CIB_PIT_k_TEXTFIELD = "TEXTFIELD"; //$NON-NLS-1$
	final String CIB_PIT_k_LABELM = "LABELM"; //$NON-NLS-1$
	final String CIB_PIT_k_DATE = "DATE"; //$NON-NLS-1$
	final String CIB_PIT_k_DAY = "DAY"; //$NON-NLS-1$
	final String CIB_PIT_k_PERIOD = "PERIOD"; //$NON-NLS-1$
	final String CIB_PIT_k_GEOFRAME = "GEOFRAME"; //$NON-NLS-1$
	final String CIB_PIT_k_MERCATOR = "MERCATOR"; //$NON-NLS-1$
	final String CIB_PIT_k_STEREOPOLAIRE = "STEREOPOLAIRE"; //$NON-NLS-1$
	final String CIB_PIT_k_LAMBERT = "LAMBERT"; //$NON-NLS-1$
	final String CIB_PIT_k_UTM = "UTM"; //$NON-NLS-1$
	final String CIB_PIT_k_GEODESY = "GEODESY"; //$NON-NLS-1$
	final String CIB_PIT_k_GEOCOORD = "GEOCOORD"; //$NON-NLS-1$
	final String CIB_PIT_k_ORIENTFRAME = "ORIENTFRAME"; //$NON-NLS-1$
	final String CIB_PIT_k_MULTIFILE = "MULTIFILE"; //$NON-NLS-1$
	final String CIB_PIT_k_CHANNEL = "CHANNEL"; //$NON-NLS-1$
	final String CIB_PIT_k_FILEGEN = "FILEGEN"; //$NON-NLS-1$
	final String CIB_PIT_k_FILEMANY = "FILEMANY"; //$NON-NLS-1$
	final String CIB_PIT_k_PERIOD_SELECTION = "PERIOD_SELECTION"; //$NON-NLS-1$
	final String CIB_PIT_k_FILEGENCHANNEL = "FILEGENCHANNEL"; //$NON-NLS-1$
	final String CIB_PIT_k_DESCRIPTION = "DESCRIPTION"; //$NON-NLS-1$

	protected List<CIB_PIT_ProcessParameter> _allParameters = new ArrayList<CIB_PIT_ProcessParameter>();

	public CIB_PIT_ParameterPanel(Composite p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		make(p_attributes);
	}

	public CIB_PIT_ParameterPanel(CIB_PIT_ParameterPanel p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		make(p_attributes);
	}

	protected void make(StringBuffer p_attributes) {

		CIB_STC_AccessFilePara l_paramFile = new CIB_STC_AccessFilePara();
		CIB_PIT_ProcessParameter l_parameter = null;
		String[] l_param = new String[2];
		String l_type;
		StringBuffer l_attributes;

		l_paramFile.setByString(p_attributes.toString());
		l_paramFile.first(l_param);

		do {
			l_type = l_param[0];
			l_attributes = new StringBuffer(l_param[1]);

			if (l_type.equals(CIB_PIT_k_PANEL)) {
				l_parameter = new CIB_PIT_ParameterPanel(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_FILE)) {
				l_parameter = new CIB_PIT_FileSelection(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_FILECHANNELLIST)) {
				l_parameter = new CIB_PIT_FileChannelListSelection(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_FILELIST)
				// l_parameter = new CIB_PIT_FileListSelection (this,
				// l_attributes);
				//
				// else if (l_type == CIB_PIT_k_FILEMEASURELIST)
				// l_parameter = new CIB_PIT_FileMeasureListSelection (this,
				// l_attributes);
			} else if (l_type.equals(CIB_PIT_k_FRAME)) {
				l_parameter = new CIB_PIT_Frame(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_LABEL)) {
				l_parameter = new CIB_PIT_Label(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_NUMBER)) {
				l_parameter = new CIB_PIT_Number(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_LATITUDE)) {
				l_parameter = new CIB_PIT_Latitude(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_LONGITUDE)) {
				l_parameter = new CIB_PIT_Longitude(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_ELLIPSOID)) {
				l_parameter = new CIB_PIT_Ellipsoid(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_PROJECTION)) {
				l_parameter = new CIB_PIT_Projection(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_MERCATOR)
				// l_parameter = new CIB_PIT_Mercator (this, l_attributes);
				//
			} else if (l_type.equals(CIB_PIT_k_GEODESY)) {
				l_parameter = new CIB_PIT_Geodesy(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_OPTION)) {
				l_parameter = new CIB_PIT_OptionMenu(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_SEPARATOR)
				// l_parameter = new CIB_PIT_Separator (this, l_attributes);
			} else if (l_type.equals(CIB_PIT_k_TOGGLE)) {
				l_parameter = new CIB_PIT_Toggle(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_TEXT)
				// l_parameter = new CIB_PIT_Text (this, l_attributes);
			} else if (l_type.equals(CIB_PIT_k_TEXTFIELD)) {
				l_parameter = new CIB_PIT_TextField(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_LABELM)
				// l_parameter = new CIB_PIT_LabelMultiLines (this,
				// l_attributes);
			} else if (l_type.equals(CIB_PIT_k_DATE)) {
				l_parameter = new CIB_PIT_Date(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_DAY)
				// l_parameter = new CIB_PIT_Day (this, l_attributes);
			} else if (l_type.equals(CIB_PIT_k_PERIOD)) {
				l_parameter = new CIB_PIT_Period(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_GEOFRAME)) {
				l_parameter = new CIB_PIT_GeographicalFrame(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_GEOCOORD)
				// l_parameter = new CIB_PIT_GeographicalCoordinate (this,
				// l_attributes);
				//
				// else if (l_type == CIB_PIT_k_ORIENTFRAME)
				// l_parameter = new CIB_PIT_ObliqueFrame (this, l_attributes);
				//
				// else if (l_type == CIB_PIT_k_MULTIFILE)
				// l_parameter = new CIB_PIT_MultiFileSelection (this,
				// l_attributes);
				//
				// else if (l_type == CIB_PIT_k_CHANNEL)
				// l_parameter = new CIB_PIT_Channel (this, l_attributes);
			} else if (l_type.equals(CIB_PIT_k_FILEGEN)) {
				l_parameter = new CIB_PIT_FileGenSelection(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_FILEMANY)) {
				l_parameter = new CIB_PIT_FileManySelection(this, l_attributes, _helpContainer);
			} else if (l_type.equals(CIB_PIT_k_PERIOD_SELECTION)) {
				l_parameter = new CIB_PIT_PeriodSelection(this, l_attributes, _helpContainer);
				//
				// else if (l_type == CIB_PIT_k_FILEGENCHANNEL)
				// l_parameter = new CIB_PIT_FileGenChannelSelection (this,
				// l_attributes);
			} else {
				CIB_STC_Error.logError(Messages.getString("CIB_PIT_ParameterPanel.38") + l_type, null); //$NON-NLS-1$
			}

			if (l_parameter != null) {
				_allParameters.add(l_parameter);
				l_parameter = null;
			}
		} while (l_paramFile.next(l_param));

		initializeObjectsVisibility();
	}

	public void initializeObjectsVisibility() {

		int l_entries;
		int l_loop;
		int l_loop1;
		Rectangle l_refBbox;
		Rectangle l_bbox;

		l_entries = _allParameters.size();

		for (l_loop = 0; l_loop < l_entries - 1; l_loop++) {
			l_refBbox = _allParameters.get(l_loop).getBounds();

			for (l_loop1 = l_loop + 1; l_loop1 < l_entries; l_loop1++) {
				l_bbox = _allParameters.get(l_loop1).getBounds();
				if (l_refBbox.intersects(l_bbox)) {
					_allParameters.get(l_loop1).setVisible(false);
				}
			}
		}

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_result = true;
		String l_context;
		int l_loop = 0;

		l_context = p_context = "->" + _name; //$NON-NLS-1$

		while (l_loop < _allParameters.size() && l_result) {
			l_result = _allParameters.get(l_loop++).validate(p_warning, l_context);
			if ( ! l_result) 
				logger.info("Error on parameter PIT Parameter Panel " + l_loop);
		}

		return l_result;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_filePara) {

		for (int l_loop = 0; l_loop < _allParameters.size(); l_loop++) {
			_allParameters.get(l_loop).update(p_filePara);
		}

	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		for (int l_loop = 0; l_loop < _allParameters.size(); l_loop++) {
			_allParameters.get(l_loop).dump(p_filePara);
		}

	}

	@Override
	public void refresh() {

		for (int l_loop = 0; l_loop < _allParameters.size(); l_loop++) {
			_allParameters.get(l_loop).refresh();
		}

	}

	@Override
	public boolean showChild(CIB_PIT_ProcessParameter p_client, String p_name, String p_conditions) {

		CIB_PIT_ProcessParameter l_parameter = null;
		Rectangle l_bbox;
		boolean l_returnValue = false;

		for (int l_loop = 0; l_loop < _allParameters.size() && l_parameter == null; l_loop++) {

			if (_allParameters.get(l_loop).name().equals(p_name)) {
				l_parameter = _allParameters.get(l_loop);
			}
		}

		if (l_parameter == null) {
			l_parameter = find(this, p_name);

			if (l_parameter != null) {
				if (l_parameter.getParentParameter() != null) {
					l_parameter.getParentParameter().showChild(this, p_name, p_conditions);
				}
			}
		} else {
			if (evaluate(p_conditions)) {
				l_bbox = l_parameter.getBounds();
				for (int l_loop1 = 0; l_loop1 < _allParameters.size(); l_loop1++) {
					hideUnder(_allParameters.get(l_loop1), l_bbox);
				}
				l_parameter.show();
			}
		}

		l_returnValue = true;

		return l_returnValue;
	}

	@Override
	public boolean hideChild(CIB_PIT_ProcessParameter p_client, String p_name) {

		CIB_PIT_ProcessParameter l_parameter = null;
		boolean l_returnValue = false;

		for (int l_loop = 0; l_loop < _allParameters.size() && l_parameter == null; l_loop++) {

			if (_allParameters.get(l_loop).name().equals(p_name)) {
				l_parameter = _allParameters.get(l_loop);
			}
		}

		if (l_parameter == null) {
			l_parameter = find(this, p_name);

			if (l_parameter != null) {
				if (l_parameter.getParentParameter() != null) {
					l_parameter.getParentParameter().hideChild(this, p_name);
				}
			}
		} else {
			l_parameter.hide();
		}

		l_returnValue = true;

		return l_returnValue;
	}

	private void hideUnder(CIB_PIT_ProcessParameter p_parameter, Rectangle p_bbox) {

		Rectangle l_paramBbox;

		if (p_parameter.isVisible()) {
			l_paramBbox = p_parameter.getBounds();
			if (p_bbox.intersects(l_paramBbox)) {
				p_parameter.hide();
			}
		}

	}

	@Override
	public CIB_PIT_ProcessParameter find(CIB_PIT_ProcessParameter p_client, String p_name) {

		int l_loop = 0;
		CIB_PIT_ProcessParameter l_parameter = super.find(p_client, p_name);

		while (l_parameter == null && l_loop < _allParameters.size()) {
			if (!_allParameters.get(l_loop).equals(p_client)) {
				l_parameter = (_allParameters.get(l_loop)).find(this, p_name);
			}
			l_loop++;
		}

		if (l_parameter == null && _parent != null && !_parent.equals(p_client)) {
			l_parameter = _parent.find(this, p_name);
		}

		return l_parameter;

	}

	private boolean evaluate(String p_condition) {

		String l_condition = p_condition;
		String l_parameterName;
		String l_value;
		int l_pos;
		boolean l_result = true;
		CIB_PIT_ProcessParameter l_parameter;

		if (!p_condition.isEmpty()) {

			StringTokenizer l_token = new StringTokenizer(l_condition);
			l_parameterName = l_token.nextToken();
			l_value = l_token.nextToken();

			l_parameter = find(this, l_parameterName);

			if (l_parameter == null) {
				CIB_STC_Error.logError(Messages.getString("CIB_PIT_ParameterPanel.40") + l_parameterName, null); //$NON-NLS-1$
				l_result = false;
			} else {
				if (l_parameter.getValue().equals(l_value)) {

					if (l_token.hasMoreTokens()) {

						l_parameterName = l_token.nextToken();
						l_pos = l_condition.indexOf(l_value);
						l_condition = l_condition.substring(l_pos + l_value.length());
						l_result = evaluate(l_condition);
					}
				}
			}
		}

		return l_result;
	}

}
