package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_PanelFolder extends CIB_PIT_ParameterPanel {

	protected CTabFolder _tabFolder;
	protected boolean _needRebuild;

	public CIB_PIT_PanelFolder(Composite p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
	}

	public CIB_PIT_PanelFolder(CIB_PIT_ParameterPanel p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
	}

	@Override
	protected void make(StringBuffer p_attributes) {

		CIB_STC_AccessFilePara l_paramFile = new CIB_STC_AccessFilePara();
		CIB_STC_AccessFilePara l_paramTab = new CIB_STC_AccessFilePara();
		CIB_PIT_PanelActivator l_panelActivator = null;
		CIB_PIT_ParameterPanel l_tabPanel = null;
		CIB_PIT_Item l_item = null;
		CIB_PIT_ItemPanel l_itemPanel = null;
		CTabItem l_tab = null;
		String[] l_param = new String[2];
		String l_name;
		int l_idx = 0;

		l_paramFile.setByString(p_attributes.toString());

		l_param[0] = CIB_PIT_k_BUTTON;
		if (l_paramFile.findBloc(l_param)) {
			load();
			do {
				l_panelActivator = new CIB_PIT_PanelActivator(this, new StringBuffer(l_param[1]), _helpContainer);

				l_item = l_panelActivator.getItem(0);
				if (l_item != null) {
					List<CIB_PIT_ItemPanel> l_panels = l_item.panels();
					if (l_panels.size() > 0) {
						l_itemPanel = l_panels.get(0);
						l_tab = new CTabItem(_tabFolder, SWT.FLAT);
						l_tab.setText(l_item.label());

						l_paramTab.setByString(p_attributes.toString());
						l_param[0] = CIB_PIT_k_PANEL;
						l_tabPanel = null;
						while (l_paramTab.findBloc(l_param) && l_tabPanel == null) {
							l_name = l_param[1];
							l_idx = l_name.indexOf(' ');
							if (l_idx != -1) {
								l_name = l_name.substring(0, l_idx);
							}
							l_idx = l_name.indexOf('\t');
							if (l_idx != -1) {
								l_name = l_name.substring(0, l_idx);
							}
							l_name = l_name.trim();
							if (l_name.equals(l_itemPanel.name())) {
								l_tabPanel = new CIB_PIT_ParameterPanel(_tabFolder, new StringBuffer(l_param[1]), _helpContainer);
								l_tabPanel.setParameterParent(this);
								l_tab.setControl(l_tabPanel);
								l_panelActivator.setPanel(l_tabPanel);
								_allParameters.add(l_panelActivator);
								_allParameters.add(l_tabPanel);
							}
							l_paramTab.deleteCurrentBloc();
						}
					}
				}
				l_paramFile.deleteCurrentBloc();
				l_param[0] = CIB_PIT_k_BUTTON;
			} while (l_paramFile.findBloc(l_param));

			_tabFolder.setBounds(getBounds());

			refresh();

		} else {
			super.make(p_attributes);
		}

	}

	public void load() {

		_tabFolder = new CTabFolder(this, SWT.FLAT);
		_tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT));
		_tabFolder.marginHeight = 10;
		_tabFolder.marginWidth = 10;

	}

	@Override
	public void refresh() {

		_needRebuild = false;
		super.refresh();
		if (_needRebuild) {
			rebuild();
		}

	}

	public void needRebuild() {
		_needRebuild = true;
	}

	protected void rebuild() {

		int l_loop;
		CTabItem l_tab = null;
		CTabItem[] tabs = _tabFolder.getItems();
		CIB_PIT_PanelActivator l_panelActivator;
		Control l_selectedControl = null;

		if (_tabFolder.getSelection() != null) {
			l_selectedControl = _tabFolder.getSelection().getControl();
		}

		for (l_loop = 0; l_loop < tabs.length; l_loop++) {
			tabs[l_loop].dispose();
		}

		for (l_loop = 0; l_loop < _allParameters.size(); l_loop++) {
			if (_allParameters.get(l_loop).getClass() == CIB_PIT_PanelActivator.class) {
				l_panelActivator = (CIB_PIT_PanelActivator) _allParameters.get(l_loop);
				if (l_panelActivator.isVisible()) {
					l_tab = new CTabItem(_tabFolder, SWT.FLAT);
					l_tab.setText(l_panelActivator.getItem(0).label());
					l_tab.setControl(l_panelActivator.getPanel());
					if (l_tab.getControl() == l_selectedControl) {
						_tabFolder.setSelection(l_tab);
					}
				}
			}
		}

	}
}
