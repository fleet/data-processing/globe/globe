package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_FileMany;
import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_FileManySelectionBox extends CIB_PIT_SelectionBox {

	public static final String CIB_PIT_k_FILEMANY_NOLOCK = "NoLock"; // Option de verouillage dans le .dsc : pas de mode force //$NON-NLS-1$
	public static final String CIB_PIT_k_FILEMANY_LOCKGEN = "LockGen"; // Option de verouillage dans le .dsc : mode generique force //$NON-NLS-1$

	protected static int CIB_PIT_k_FILEMANY_SIMPLE_FILE = 0; // item selection d
																// un nom simple
	protected static int CIB_PIT_k_FILEMANY_GENERIC_TYPE = 1; // item selection
																// d un type nom
																// generique

	protected int _selectionType; // type de selection
	protected int _selectionTypeInit; // type de selection
	protected String _lockOption; // option de verrouillage
	protected String _file; // nom de fichier
	protected String _rootFileName; // racine du nom des fichiers
	protected String _extension; // extension du nom des fichiers

	protected Combo _outputType; // Combo type de selection
	protected CIB_PIT_Component _simpleContainer; // Conteneur nom simple
	protected CIB_PIT_Component _geneContainer; // Conteneur nom simple
	protected CIB_PIT_FileSelection _rootNameSelection; // Selecteur de fichier
														// pour racine des noms
														// de fichiers
	protected CIB_PIT_TextField _extensionField; // Champ de saisie extension
													// des nom de fichiers
	protected CIB_PIT_FileSelection _fileSelection; // Selecteur de fichier pour
													// racine des noms simple
	private String _rootFileNameInit;
	private String _fileInit;
	private String _extensionInit;

	public CIB_PIT_FileManySelectionBox(Display p_display, String p_name, boolean p_visible, String p_lockOption) {
		super(p_display, p_name, Messages.getString("CIB_PIT_FileManySelectionBox.2"), p_visible); //$NON-NLS-1$
		_lockOption = p_lockOption;
		_file = ""; //$NON-NLS-1$
		_rootFileName = ""; //$NON-NLS-1$
		_extension = ""; //$NON-NLS-1$
		useRun(false);
		initialize();
	}

	private void initialize() {
		load();
	}

	public void load() {

		GridData l_gridData;
		CIB_PIT_Label l_lab;

		_mainContainer.setVisible(false);

		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		_mainContainer.setLayout(layout);

		// label
		Label l_label = new Label(_mainContainer, SWT.BOLD);
		l_label.setText(Messages.getString("CIB_PIT_FileManySelectionBox.6")); //$NON-NLS-1$

		// Combo type de selection
		_outputType = new Combo(_mainContainer, SWT.READ_ONLY);
		_outputType.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_outputType.add(Messages.getString("CIB_PIT_FileManySelectionBox.7")); //$NON-NLS-1$
		_outputType.add(Messages.getString("CIB_PIT_FileManySelectionBox.8")); //$NON-NLS-1$
		_outputType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				changeOutputType(_outputType.getSelectionIndex());
			}
		});

		Label separator = new Label(_mainContainer, SWT.HORIZONTAL | SWT.SEPARATOR | SWT.BORDER);
		l_gridData = new GridData(GridData.FILL_HORIZONTAL);
		l_gridData.horizontalSpan = 2;
		separator.setLayoutData(l_gridData);

		Composite l_panel = new Composite(_mainContainer, SWT.NONE);
		l_gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		l_gridData.horizontalSpan = 2;
		l_panel.setLayoutData(l_gridData);

		// Panneau nom generique
		_geneContainer = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 650, 300), Messages.getString("CIB_PIT_FileManySelectionBox.10"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		l_lab = new CIB_PIT_Label(_geneContainer, "Label", new CIB_PIT_Rect(0, 10, 200, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_FileManySelectionBox.13")); //$NON-NLS-1$

		_rootNameSelection = new CIB_PIT_FileSelection(_geneContainer, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_ROOT_NAME, new CIB_PIT_Rect(210, 10, 350, 26),
				Messages.getString("CIB_PIT_FileManySelectionBox.14"), _helpContainer); //$NON-NLS-1$

		l_lab = new CIB_PIT_Label(_geneContainer, "Label", new CIB_PIT_Rect(0, 50, 200, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_FileManySelectionBox.17")); //$NON-NLS-1$

		_extensionField = new CIB_PIT_TextField(_geneContainer, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_EXTENSION, new CIB_PIT_Rect(210, 50, 80, 26),
				Messages.getString("CIB_PIT_FileManySelectionBox.18"), _helpContainer); //$NON-NLS-1$

		// Panneau nom simple
		_simpleContainer = new CIB_PIT_Component(l_panel, "", new CIB_PIT_Rect(0, 0, 650, 300), Messages.getString("CIB_PIT_FileManySelectionBox.20"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		l_gridData.horizontalSpan = 2;
		_simpleContainer.setLayoutData(l_gridData);

		l_lab = new CIB_PIT_Label(_simpleContainer, "Label", new CIB_PIT_Rect(0, 10, 200, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_FileManySelectionBox.23")); //$NON-NLS-1$

		_fileSelection = new CIB_PIT_FileSelection(_simpleContainer, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_FILE, new CIB_PIT_Rect(210, 10, 350, 26),
				Messages.getString("CIB_PIT_FileManySelectionBox.24"), _helpContainer); //$NON-NLS-1$

		_shell.pack();

		// ************************************************************
		// Verouillage en mode Generique
		// ************************************************************
		if (_lockOption.equals(CIB_PIT_k_FILEMANY_LOCKGEN)) {
			_selectionType = CIB_PIT_k_FILEMANY_GENERIC_TYPE;
			_outputType.setEnabled(false);
		}
		changeOutputType(_selectionType);
		_mainContainer.setVisible(true);

	}

	void changeOutputType(int p_selection) {

		_selectionType = p_selection;
		if (_lockOption.equals(CIB_PIT_k_FILEMANY_LOCKGEN)) {
			_selectionType = CIB_PIT_k_FILEMANY_GENERIC_TYPE;
		}

		_outputType.select(_selectionType);

		if (_selectionType == CIB_PIT_k_FILEMANY_SIMPLE_FILE) {
			_simpleContainer.setVisible(true);
			_geneContainer.setVisible(false);
		} else {
			_simpleContainer.setVisible(false);
			_geneContainer.setVisible(true);
		}
	}

	public void preLoad() {
		_rootNameSelection.set(_rootFileName);
		_extensionField.setText(_extension);
		_fileSelection.set(_file);
		changeOutputType(_selectionType);
	}

	public void downLoad() {
		_file = _fileSelection.get();
		_rootFileName = _rootNameSelection.get();
		_extension = _extensionField.getText();
	}

	public String getDirectory() {
		String l_directory;
		if (_selectionType == CIB_PIT_k_FILEMANY_SIMPLE_FILE) {
			l_directory = _file;
		} else {
			l_directory = _rootFileName;
		}
		return l_directory;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _paramName);
		l_filePara.setByString(l_value);

		_selectionType = CIB_DPR_ProcessParameter.readInt(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_OUTPUT_TYPE);
		_selectionTypeInit = _selectionType;

		_file = CIB_DPR_ProcessParameter.read(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_FILE);
		_file = _file.trim();
		_fileInit = new String(_file);

		_rootFileName = CIB_DPR_ProcessParameter.read(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_ROOT_NAME);
		_rootFileName = _rootFileName.trim();
		_rootFileNameInit = new String(_rootFileName);
		_extension = CIB_DPR_ProcessParameter.read(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_EXTENSION);
		_extensionInit = new String(_extension);

		if (_lockOption == CIB_PIT_k_FILEMANY_LOCKGEN) {
			_selectionType = CIB_PIT_k_FILEMANY_GENERIC_TYPE;
		}

		preLoad();

	}

	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_result = true;

		setAutoHide(l_result);

		return l_result;
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		
		if ((!_rootFileName.equals(_rootFileNameInit)) || !(_file.equals(_fileInit)) || !(_extension.equals(_extensionInit)) || (_selectionType != _selectionTypeInit)) {

			CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

			CIB_DPR_ProcessParameter.writeInt(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_OUTPUT_TYPE, _selectionType);
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_FILE, _file);
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_ROOT_NAME, _rootFileName);
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_DPR_FileMany.CIB_DPR_k_FILEMANY_EXTENSION, _extension);

			CIB_DPR_ProcessParameter.write(p_filePara, _paramName, l_filePara.readAll());
		}

	}

}
