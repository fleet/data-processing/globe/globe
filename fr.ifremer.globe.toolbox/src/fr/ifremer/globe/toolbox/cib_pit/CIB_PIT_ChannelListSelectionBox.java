package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;

public class CIB_PIT_ChannelListSelectionBox extends CIB_PIT_SelectionBox {

	protected CIB_PIT_TextField _filterField;
	protected CIB_PIT_List _list;
	protected CIB_PIT_List _selectedList;

	public CIB_PIT_ChannelListSelectionBox(Display p_display, String p_name, boolean p_visible) {
		super(p_display, p_name, Messages.getString("CIB_PIT_ChannelListSelectionBox.0"), p_visible); //$NON-NLS-1$
		useRun(false);
		initialize();
	}

	public void initialize() {
		load();
	}

	public void load() {

		CIB_PIT_Label l_label;

		_mainContainer.setVisible(false);

		GridLayout layout = new GridLayout(3, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		_mainContainer.setLayout(layout);

		// Field selector
		CIB_PIT_Component l_stringSelector = new CIB_PIT_Component(_mainContainer, "", new CIB_PIT_Rect(0, 0, 300, 450), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		// Filtre
		l_label = new CIB_PIT_Label(l_stringSelector, "", new CIB_PIT_Rect(2, 0, 150, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_ChannelListSelectionBox.1")); //$NON-NLS-1$
		_filterField = new CIB_PIT_TextField(l_stringSelector, "", new CIB_PIT_Rect(2, 25, 295, 22), Messages.getString("CIB_PIT_FileSelector.6"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		// Boutons
		CIB_PIT_Component l_btn = new CIB_PIT_Component(l_stringSelector, "", new CIB_PIT_Rect(240, 60, 60, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		GridLayout l_btnLyout = new GridLayout(2, true);
		l_btnLyout.marginHeight = 0;
		l_btnLyout.marginWidth = 0;
		l_btn.setLayout(l_btnLyout);

		Button l_button;
		l_button = new Button(l_btn, SWT.PUSH);
		l_button.setText("*"); //$NON-NLS-1$
		l_button.setLayoutData(new GridData(SWT.BEGINNING, SWT.FILL, true, true));
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_list.getList().selectAll();
			}
		});

		l_button = new Button(l_btn, SWT.PUSH);
		l_button.setText("x"); //$NON-NLS-1$
		l_button.setLayoutData(new GridData(SWT.END, SWT.FILL, true, true));
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_list.getList().deselectAll();
			}
		});

		l_btn.layout();

		// Liste
		l_label = new CIB_PIT_Label(l_stringSelector, "", new CIB_PIT_Rect(2, 90, 160, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_ChannelListSelectionBox.2")); //$NON-NLS-1$
		_list = new CIB_PIT_List(l_stringSelector, "", new CIB_PIT_Rect(2, 115, 295, 310), Messages.getString("CIB_PIT_ChannelListSelectionBox.3"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		// list selector
		CIB_PIT_Component l_listSelector = new CIB_PIT_Component(_mainContainer, "", new CIB_PIT_Rect(0, 0, 50, 450), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		RowLayout rowLayout = new RowLayout(SWT.VERTICAL);
		rowLayout.center = true;
		rowLayout.marginHeight = 0;
		rowLayout.marginWidth = 0;
		l_listSelector.setLayout(rowLayout);

		l_button = new Button(l_listSelector, SWT.PUSH);
		l_button.setText(">"); //$NON-NLS-1$
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (_list.getList().getSelectionCount() > 0) {
					String[] l_selected = _list.getList().getSelection();
					for (int l_loop = 0; l_loop < l_selected.length; l_loop++) {
						_selectedList.getList().add(l_selected[l_loop]);
					}
				}
			}
		});

		l_button = new Button(l_listSelector, SWT.PUSH);
		l_button.setText("<"); //$NON-NLS-1$
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_selectedList.getList().remove(_selectedList.getList().getSelectionIndices());
			}
		});

		l_button = new Button(l_listSelector, SWT.PUSH);
		l_button.setText("<<"); //$NON-NLS-1$
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_selectedList.getList().removeAll();
			}
		});

		l_listSelector.layout();

		// selected list
		CIB_PIT_Component l_selectedList = new CIB_PIT_Component(_mainContainer, "", new CIB_PIT_Rect(0, 0, 300, 450), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		l_label = new CIB_PIT_Label(l_selectedList, "", new CIB_PIT_Rect(2, 0, 150, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_ChannelListSelectionBox.4")); //$NON-NLS-1$
		_selectedList = new CIB_PIT_List(l_selectedList, "", new CIB_PIT_Rect(2, 25, 300, 400), Messages.getString("CIB_PIT_FileListSelectionPanel.15"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		_shell.pack();

		_mainContainer.setVisible(true);
	}

	public void setFile(String p_fileName) {
		_filterField.setText(p_fileName);
	}

	public void setList(List<String> p_liste) {

		_list.getList().removeAll();

		for (int l_loop = 0; l_loop < p_liste.size(); l_loop++) {
			_list.getList().add(p_liste.get(l_loop));
		}
	}

	public void setSelection(List<String> p_list) {

		_selectedList.getList().removeAll();

		for (int l_loop = 0; l_loop < p_list.size(); l_loop++) {
			_selectedList.getList().add(p_list.get(l_loop));
		}
	}

	public void getSelection(List<String> p_list) {
		p_list.clear();
		int l_nbFiles = _selectedList.getList().getItemCount();
		for (int l_loop = 0; l_loop < l_nbFiles; l_loop++) {
			p_list.add(_selectedList.getList().getItem(l_loop));
		}
	}

	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;

		String l_context;
		String l_warning;

		if (_selectedList.getList().getItemCount() == 0) {
			l_context = p_context;
			l_context = l_context.concat("->"); //$NON-NLS-1$
			l_context = l_context.concat(_paramName);
			l_warning = l_context;
			l_warning = l_warning.concat("->"); //$NON-NLS-1$
			l_warning = l_warning.concat(Messages.getString("CIB_PIT_ChannelListSelectionBox.5") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			p_warning.add(l_warning);
		}

		return l_status;
	}

}
