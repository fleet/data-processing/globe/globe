package fr.ifremer.globe.toolbox.cib_pit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_FileGen;
import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_FileListSelectionPanel extends CIB_PIT_ProcessParameter {

	protected CIB_PIT_FileSelector _fileSelector;
	protected CIB_PIT_List _selectedFilesList;
	protected List<String> _filesList = new ArrayList<String>();

	public CIB_PIT_FileListSelectionPanel(Composite p_parent, Label p_helpContainer) {
		super(p_parent, "", new CIB_PIT_Rect(0, 0, 650, 430), Messages.getString("CIB_PIT_FileListSelectionPanel.1"), p_helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		GridLayout layout = new GridLayout(3, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		// file selector
		_fileSelector = new CIB_PIT_FileSelector(this, "FileSelector", new CIB_PIT_Rect(0, 0, 300, 450), Messages.getString("CIB_PIT_FileListSelectionPanel.3"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		// list selector
		CIB_PIT_Component l_listSelector = new CIB_PIT_Component(this, "", new CIB_PIT_Rect(0, 0, 50, 450), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		RowLayout rowLayout = new RowLayout(SWT.VERTICAL);
		rowLayout.center = true;
		rowLayout.marginHeight = 0;
		rowLayout.marginWidth = 0;
		l_listSelector.setLayout(rowLayout);

		Button l_button;
		l_button = new Button(l_listSelector, SWT.PUSH);
		l_button.setText(">"); //$NON-NLS-1$
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				List<String> l_fileList = new ArrayList<String>();
				_fileSelector.getSelectedFiles(l_fileList);
				for (int l_loop = 0; l_loop < l_fileList.size(); l_loop++) {
					_selectedFilesList.getList().add(l_fileList.get(l_loop));
				}
			}
		});

		l_button = new Button(l_listSelector, SWT.PUSH);
		l_button.setText("<"); //$NON-NLS-1$
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_selectedFilesList.getList().remove(_selectedFilesList.getList().getSelectionIndices());
			}
		});

		l_button = new Button(l_listSelector, SWT.PUSH);
		l_button.setText("<<"); //$NON-NLS-1$
		l_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				_selectedFilesList.getList().removeAll();
			}
		});

		l_listSelector.layout();

		// selected list
		CIB_PIT_Component l_selectedList = new CIB_PIT_Component(this, "", new CIB_PIT_Rect(0, 0, 300, 450), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		CIB_PIT_Label l_label = new CIB_PIT_Label(l_selectedList, "", new CIB_PIT_Rect(2, 0, 150, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_label.setText(Messages.getString("CIB_PIT_FileListSelectionPanel.13")); //$NON-NLS-1$
		_selectedFilesList = new CIB_PIT_List(l_selectedList, "", new CIB_PIT_Rect(2, 25, 300, 400), Messages.getString("CIB_PIT_FileListSelectionPanel.15"), _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$

		layout();
	}

	public void select() {
		setList(_filesList);
	}

	public void setList(List<String> p_liste) {

		_selectedFilesList.getList().removeAll();

		if (p_liste.size() > 0) {
			_fileSelector.setSelectedFile(p_liste.get(0));
		} else {
			_fileSelector.setSelectedFile(CIB_PIT_Component._defaultPath + "/"); //$NON-NLS-1$
		}

		for (int l_loop = 0; l_loop < p_liste.size(); l_loop++) {
			_selectedFilesList.getList().add(p_liste.get(l_loop));
		}
	}

	public String getMainValue() {
		String l_fileName = ""; //$NON-NLS-1$
		if (_filesList.size() > 0) {
			l_fileName = _filesList.get(0);
		}
		return l_fileName;
	}

	public void apply() {

		_filesList.clear();

		int l_nbFiles = _selectedFilesList.getList().getItemCount();
		for (int l_loop = 0; l_loop < l_nbFiles; l_loop++) {
			_filesList.add(_selectedFilesList.getList().getItem(l_loop));
		}
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		_filesList.clear();
		CIB_DPR_ProcessParameter.read(p_paramFile, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_FILELIST, _filesList);
		select();
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;
		String l_fileName;
		String l_context;
		String l_warning;

		l_context = p_context;
		l_context = l_context.concat("->"); //$NON-NLS-1$
		l_context = l_context.concat(_name);

		for (int l_loop = 0; l_loop < _filesList.size(); l_loop++) {
			l_fileName = _filesList.get(l_loop);
			File f = new File(l_fileName);
			if (!f.exists()) {
				l_warning = l_context;
				l_warning = l_warning.concat("->"); //$NON-NLS-1$

				l_warning = l_warning.concat(Messages.getString("CIB_PIT_FileListSelectionPanel.19") + l_fileName + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				p_warning.add(l_warning);
			}
		}

		return l_status;
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		CIB_DPR_ProcessParameter.write(p_filePara, CIB_DPR_FileGen.CIB_DPR_k_FILEGEN_FILELIST, _filesList);
	}

}
