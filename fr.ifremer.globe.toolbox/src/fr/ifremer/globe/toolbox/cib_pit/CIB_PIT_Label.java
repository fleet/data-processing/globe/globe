package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Label extends CIB_PIT_ProcessParameter {

	private boolean _updatable;
	protected Label _label;

	public CIB_PIT_Label(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		_updatable = false;
		_label = null;

		initialize(p_attributes);

	}

	public CIB_PIT_Label(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
		_label.setSize(getSize());
		if (_helpTracker != null) {
			_label.addMouseTrackListener(_helpTracker);
		}
	}

	public void initialize(StringBuffer p_attributes) {

		String l_tmp;

		initialize();

		l_tmp = extractString(p_attributes);
		_label.setText(l_tmp);

		if (p_attributes.length() > 3) {
			l_tmp = extractString(p_attributes);
			if (l_tmp.equals("1")) { //$NON-NLS-1$
				_updatable = true;
			}
		}
	}

	public void setText(String p_text) {
		_label.setText(p_text);
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		if (_updatable) {
			_label.setText(CIB_DPR_ProcessParameter.read(p_paramFile, _name));
		}
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {
		return true;
	}

	public void load() {
		_label = new Label(this, SWT.BOLD);
		Point l_point = _label.getLocation();
		l_point.y += 2;
		_label.setLocation(l_point);
	}

}
