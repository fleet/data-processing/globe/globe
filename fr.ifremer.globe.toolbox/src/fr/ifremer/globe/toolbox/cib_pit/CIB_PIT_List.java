package fr.ifremer.globe.toolbox.cib_pit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

public class CIB_PIT_List extends CIB_PIT_Component {

	protected List _list;

	public CIB_PIT_List(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
		_list.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		_list.setSize(getSize());
		if (_helpTracker != null) {
			_list.addMouseTrackListener(_helpTracker);
		}
	}

	public void load() {

		_list = new List(this, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.BOLD | SWT.BORDER);

	}

	public List getList() {
		return _list;
	}

}
