package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_GeographicalFrame extends CIB_PIT_ProcessParameter {

	protected static final String CIB_PIT_k_GEOFRAME = "GEOFRAME"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_GEOFRAME_LATITUDE_SOUTH = "LATITUDE_SOUTH"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_GEOFRAME_LONGITUDE_WEST = "LONGITUDE_WEST"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_GEOFRAME_LATITUDE_NORTH = "LATITUDE_NORTH"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_GEOFRAME_LONGITUDE_EST = "LONGITUDE_EST"; //$NON-NLS-1$

	protected CIB_PIT_Latitude _latitudeN; // latitude
	protected CIB_PIT_Longitude _longitudeE; // longitude
	protected CIB_PIT_Latitude _latitudeS; // latitude
	protected CIB_PIT_Longitude _longitudeW; // longitude

	public CIB_PIT_GeographicalFrame(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_GeographicalFrame(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer, String p_option) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		CIB_PIT_Label l_lab;
		Group l_group;

		l_group = new Group(this, SWT.NONE);
		l_group.setBounds(new Rectangle(0, 0, 450, 155));
		l_group.setText(Messages.getString("CIB_PIT_GeographicalFrame.5")); //$NON-NLS-1$

		l_lab = new CIB_PIT_Label(l_group, "Label", new CIB_PIT_Rect(15, 20, 170, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_GeographicalFrame.8")); //$NON-NLS-1$
		_latitudeN = new CIB_PIT_Latitude(l_group, CIB_PIT_k_GEOFRAME_LATITUDE_NORTH, new CIB_PIT_Rect(230, 20, 218, 22), _helpMessage, _helpContainer);

		l_lab = new CIB_PIT_Label(l_group, "Label", new CIB_PIT_Rect(15, 50, 170, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_GeographicalFrame.11")); //$NON-NLS-1$
		_longitudeW = new CIB_PIT_Longitude(l_group, CIB_PIT_k_GEOFRAME_LONGITUDE_WEST, new CIB_PIT_Rect(230, 50, 218, 22), _helpMessage, _helpContainer);

		Label l_ligne = new Label(l_group, SWT.HORIZONTAL | SWT.SEPARATOR | SWT.BORDER);
		l_ligne.setBounds(11, 83, 430, 3);

		l_lab = new CIB_PIT_Label(l_group, "Label", new CIB_PIT_Rect(15, 90, 170, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_GeographicalFrame.14")); //$NON-NLS-1$
		_latitudeS = new CIB_PIT_Latitude(l_group, CIB_PIT_k_GEOFRAME_LATITUDE_SOUTH, new CIB_PIT_Rect(230, 90, 218, 26), _helpMessage, _helpContainer);

		l_lab = new CIB_PIT_Label(l_group, "Label", new CIB_PIT_Rect(15, 120, 170, 22), "", _helpContainer); //$NON-NLS-1$ //$NON-NLS-2$
		l_lab.setText(Messages.getString("CIB_PIT_GeographicalFrame.17")); //$NON-NLS-1$
		_longitudeE = new CIB_PIT_Longitude(l_group, CIB_PIT_k_GEOFRAME_LONGITUDE_EST, new CIB_PIT_Rect(230, 120, 218, 22), _helpMessage, _helpContainer);
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		_longitudeE.update(l_filePara);
		_longitudeW.update(l_filePara);
		_latitudeN.update(l_filePara);
		_latitudeS.update(l_filePara);
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		_longitudeE.dump(l_filePara);
		_longitudeW.dump(l_filePara);
		_latitudeN.dump(l_filePara);
		_latitudeS.dump(l_filePara);

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;
		String l_context = p_context;
		double l_latS = 0.0;
		double l_latN = 0.0;
		double l_longW = 0.0;
		double l_longE = 0.0;

		l_context += "->"; //$NON-NLS-1$
		if ((l_status = _latitudeN.validate(p_warning, l_context))) {
			if ((l_status = _longitudeE.validate(p_warning, l_context))) {
				if ((l_status = _latitudeS.validate(p_warning, l_context))) {
					if ((l_status = _longitudeW.validate(p_warning, l_context))) {
						l_latN = _latitudeN.getVal();
						l_latS = _latitudeS.getVal();
						l_longE = _longitudeE.getVal();
						l_longW = _longitudeW.getVal();
						if (l_longW >= l_longE || l_latS > l_latN) {
							l_status = false;
						}
					}
				}
			}
		}

		if (!l_status) {
			MessageDialog.openError(getShell(), Messages.getString("CIB_PIT_GeographicalFrame.19"), Messages.getString("CIB_PIT_GeographicalFrame.20")); //$NON-NLS-1$ //$NON-NLS-2$
		}

		return l_status;
	}

}
