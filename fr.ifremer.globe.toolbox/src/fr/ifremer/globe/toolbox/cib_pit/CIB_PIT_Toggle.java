package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Error;

public class CIB_PIT_Toggle extends CIB_PIT_PanelChooser {

	protected static final String CIB_PIT_k_TRUE = "True"; // Item associe a l'etat True du bouton //$NON-NLS-1$
	protected static final String CIB_PIT_k_FALSE = "False"; // Item associe a l'etat False du bouton //$NON-NLS-1$

	protected Button _toggle;

	public CIB_PIT_Toggle(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		_toggle = null;

		initialize(p_attributes);
	}

	private void initialize(StringBuffer p_attributes) {

		load();
		Point l_size = getSize();
		int l_dim = 0;
		if (l_size.x > l_size.y) {
			l_dim = l_size.y;
		} else {
			l_dim = l_size.x;
		}
		_toggle.setSize(new Point(l_dim, l_dim));
		_toggle.addMouseTrackListener(_helpTracker);

		if (_allItems.size() > 0) {
			if (_allItems.get(0).name().equals(CIB_PIT_k_TRUE)) {
				_toggle.setSelection(true);
			} else if (_allItems.get(0).name().equals(CIB_PIT_k_FALSE)) {
				_toggle.setSelection(false);
			} else {
				CIB_STC_Error.logError(Messages.getString("CIB_PIT_Toggle.2") + _name, null); //$NON-NLS-1$
			}
		} else {
			CIB_STC_Error.logError(Messages.getString("CIB_PIT_Toggle.3") + _name, null); //$NON-NLS-1$
		}

		_toggle.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				changeValue();
			}
		});

		changeValue();
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {

		String l_state;

		l_state = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		if (l_state.equals(CIB_PIT_k_TRUE)) {
			_toggle.setSelection(true);
		} else if (l_state.equals(CIB_PIT_k_FALSE)) {
			_toggle.setSelection(false);
		} else {
			CIB_STC_Error.logError(Messages.getString("CIB_PIT_Toggle.4") + _name, null); //$NON-NLS-1$
		}

		changeValue();
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		String l_value;
		boolean l_state = _toggle.getSelection();

		if (l_state == true) {
			l_value = CIB_PIT_k_TRUE;
		} else {
			l_value = CIB_PIT_k_FALSE;
		}

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);
	}

	@Override
	public void refresh() {
		String l_value;
		boolean l_state = _toggle.getSelection();

		if (l_state == true) {
			l_value = CIB_PIT_k_TRUE;
		} else {
			l_value = CIB_PIT_k_FALSE;
		}

		clear();
		newItem(l_value);
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {
		return true;
	}

	protected void changeValue() {

		refreshAll();

	}

	public void load() {

		_toggle = new Button(this, SWT.CHECK);

	}

}
