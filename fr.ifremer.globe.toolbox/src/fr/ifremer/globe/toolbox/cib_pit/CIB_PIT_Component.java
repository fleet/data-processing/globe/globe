package fr.ifremer.globe.toolbox.cib_pit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class CIB_PIT_Component extends Composite {

	static final int CIB_PIT_k_INPUT_COLOR = SWT.COLOR_WHITE;
	static final int CIB_PIT_k_BACKGROUND_COLOR = SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT;

	protected String _name;
	protected String _helpMessage;
	protected Label _helpContainer;
	protected MouseTrackAdapter _helpTracker;
	protected static String _defaultPath;

	public CIB_PIT_Component(Shell shell, Label p_helpContainer) {
		super(shell, SWT.NONE);

		_helpContainer = p_helpContainer;
		_helpMessage = null;
		_helpTracker = null;
		_name = ""; //$NON-NLS-1$
		_defaultPath = ""; //$NON-NLS-1$
	}

	public CIB_PIT_Component(Composite parent, Label p_helpContainer) {
		super(parent, SWT.NONE);

		_helpContainer = p_helpContainer;
		_helpMessage = null;
		_helpTracker = null;
		_name = ""; //$NON-NLS-1$
		_defaultPath = ""; //$NON-NLS-1$
	}

	public CIB_PIT_Component(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, SWT.NONE);

		_helpContainer = p_helpContainer;
		_helpMessage = null;
		_helpTracker = null;
		_name = p_name;
		_defaultPath = ""; //$NON-NLS-1$
		this.setBounds(p_size.getRect());
		if (p_helpMessage != null && p_helpMessage.length() > 0) {
			setHelpMessage(p_helpMessage);
		}
	}

	public void setHelpMessage(String p_helpMessage) {

		_helpMessage = p_helpMessage;

		_helpTracker = new MouseTrackAdapter() {
			@Override
			public void mouseExit(MouseEvent e) {
				_helpContainer.setText(""); //$NON-NLS-1$
			}

			@Override
			public void mouseHover(MouseEvent event) {
				_helpContainer.setText(_helpMessage);
			}
		};

		addMouseTrackListener(_helpTracker);
	}

	public String name() {
		return _name;
	}

	public void name(String p_name) {
		_name = p_name;
	}

	public static void setDefaultPath(String p_defaultPath) {
		_defaultPath = p_defaultPath.replace('\\', '/');
	}

}
