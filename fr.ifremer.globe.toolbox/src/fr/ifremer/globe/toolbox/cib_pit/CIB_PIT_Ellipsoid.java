package fr.ifremer.globe.toolbox.cib_pit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Ellipsoid extends CIB_PIT_ProcessParameter {

	public static class CIB_Ellipsoid {
		double _halfA;
		double _halfB;
		String _name;

		public CIB_Ellipsoid(double p_halfA, double p_halfB, String p_name) {
			_halfA = p_halfA;
			_halfB = p_halfB;
			_name = p_name;
		}
	};

	protected static final CIB_Ellipsoid[] _ellipsArray = { new CIB_Ellipsoid(6378249.2000, 6356514.999988159, "Clarke-1880(IGN)"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378388.0000, 6356911.946127946, "Europe-50"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378135.0000, 6356750.520016094, "WGS-72"), //$NON-NLS-1$
			new CIB_Ellipsoid(6377397.1550, 6356078.962818189, "Bessel-1841"), //$NON-NLS-1$
			new CIB_Ellipsoid(6377276.3450, 6356075.413140240, "Everest-1830"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378206.4000, 6356583.800000000, "Clarke-1866"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378166.0000, 6356784.283607107, "Mercury-59"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378150.0000, 6356768.337244385, "Mercury-68"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378145.0000, 6356759.769488684, "WGS-66(NWL-9D)"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378144.0000, 6356757.340000000, "NWL-69"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378155.0000, 6356770.300000000, "SAO-69"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378160.0000, 6356774.516089331, "AIG-67(SAD-69)"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378140.0000, 6356755.288156210, "AIG-1975"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378137.0000, 6356752.314245179, "WGS-84"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378249.1453, 6356514.869848753, "Clarke-1880(RGS)"), //$NON-NLS-1$
			new CIB_Ellipsoid(6378137.0000, 6356752.314140356, "GRS80") //$NON-NLS-1$
	};

	protected static final int CIB_PIT_k_USER_DEFINED = 16;
	protected static final String CIB_PIT_k_ELLIPSOID_USER_DEFFINED = "UserDefined"; //$NON-NLS-1$

	protected Combo _ellipsoidMenu;
	protected CIB_PIT_Label _halfALabel;
	protected CIB_PIT_Number _halfA;
	protected CIB_PIT_Label _halfAUnitLabel;
	protected CIB_PIT_Label _halfBLabel;
	protected CIB_PIT_Number _halfB;

	public CIB_PIT_Ellipsoid(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_Ellipsoid(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	private void initialize() {
		load();
	}

	public void load() {

		Group l_group = new Group(this, SWT.BOLD);
		l_group.setBounds(new Rectangle(0, 0, 450, 105));
		l_group.setText(Messages.getString("CIB_PIT_Ellipsoid.17")); //$NON-NLS-1$

		// label
		Label l_label = new Label(l_group, SWT.BOLD);
		l_label.setBounds(new Rectangle(15, 20, 170, 22));
		l_label.setText(Messages.getString("CIB_PIT_Ellipsoid.18")); //$NON-NLS-1$

		// Combo type de selection
		_ellipsoidMenu = new Combo(l_group, SWT.READ_ONLY);
		_ellipsoidMenu.setBounds(new CIB_PIT_Rect(210, 20, 150, 22).getRect());
		_ellipsoidMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		for (int l_idx = 0; l_idx < CIB_PIT_k_USER_DEFINED; l_idx++) {
			_ellipsoidMenu.add(_ellipsArray[l_idx]._name);
		}
		_ellipsoidMenu.add(Messages.getString("CIB_PIT_Ellipsoid.19")); //$NON-NLS-1$
		_ellipsoidMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ellipsoidOption();
			}
		});

		_halfALabel = new CIB_PIT_Label(l_group, "Label", new CIB_PIT_Rect(20, 50, 170, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_halfALabel.setText(Messages.getString("CIB_PIT_Ellipsoid.21")); //$NON-NLS-1$
		_halfA = new CIB_PIT_Number(l_group, "halfA", new CIB_PIT_Rect(230, 50, 100, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_halfAUnitLabel = new CIB_PIT_Label(l_group, "Label", new CIB_PIT_Rect(335, 50, 80, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_halfAUnitLabel.setText(Messages.getString("CIB_PIT_Ellipsoid.24")); //$NON-NLS-1$

		_halfBLabel = new CIB_PIT_Label(l_group, "Label", new CIB_PIT_Rect(20, 80, 170, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_halfBLabel.setText(Messages.getString("CIB_PIT_Ellipsoid.26")); //$NON-NLS-1$
		_halfB = new CIB_PIT_Number(l_group, "halfB", new CIB_PIT_Rect(230, 80, 100, 22), _helpMessage, _helpContainer); //$NON-NLS-1$

		_ellipsoidMenu.select(0);
		ellipsoidOption();

	}

	protected void ellipsoidOption() {
		switch (_ellipsoidMenu.getSelectionIndex()) {
		case CIB_PIT_k_USER_DEFINED:
			_halfALabel.setVisible(true);
			_halfA.setVisible(true);
			_halfAUnitLabel.setVisible(true);
			_halfBLabel.setVisible(true);
			_halfB.setVisible(true);
			break;
		default:
			_halfALabel.setVisible(false);
			_halfA.setVisible(false);
			_halfAUnitLabel.setVisible(false);
			_halfBLabel.setVisible(false);
			_halfB.setVisible(false);
			break;
		}
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		int l_ellipsoid = -1;
		String l_ellipsoidType;
		String l_halfA;
		String l_halfB;
		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		StringTokenizer l_param = new StringTokenizer(l_value);
		l_ellipsoidType = l_param.nextToken();
		l_halfA = l_param.nextToken();
		l_halfB = l_param.nextToken();

		for (int l_idx = 0; l_idx < CIB_PIT_k_USER_DEFINED && l_ellipsoid == -1; l_idx++) {
			if (l_ellipsoidType.equals(_ellipsArray[l_idx]._name)) {
				l_ellipsoid = l_idx;
			}
		}

		if (l_ellipsoid == -1) {
			l_ellipsoid = CIB_PIT_k_USER_DEFINED;
			_halfA.setText(l_halfA);
			_halfB.setText(l_halfB);
		}

		_ellipsoidMenu.select(l_ellipsoid);
		ellipsoidOption();
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		int l_ellipsoid;
		String l_name;
		double l_halfA;
		double l_halfB;
		String l_value;

		l_ellipsoid = _ellipsoidMenu.getSelectionIndex();
		if (l_ellipsoid == CIB_PIT_k_USER_DEFINED) {
			l_name = CIB_PIT_k_ELLIPSOID_USER_DEFFINED;
			l_halfA = Double.valueOf(_halfA.getText());
			l_halfB = Double.valueOf(_halfB.getText());
		} else {
			l_name = _ellipsArray[l_ellipsoid]._name;
			l_halfA = _ellipsArray[l_ellipsoid]._halfA;
			l_halfB = _ellipsArray[l_ellipsoid]._halfB;
		}

		NumberFormat nf = NumberFormat.getNumberInstance(Locale.UK);
		DecimalFormat df = (DecimalFormat) nf;
		df.applyPattern("#.######E0"); //$NON-NLS-1$
		df.setMinimumFractionDigits(6);

		// l_value = l_name + " " + Double.toString(l_halfA) + " " +
		// Double.toString(l_halfB);
		l_value = l_name + " " + df.format(l_halfA) + " " + df.format(l_halfB); //$NON-NLS-1$ //$NON-NLS-2$

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {
		boolean l_result = true;
		String l_context;

		l_context = p_context;
		l_context = l_context.concat("->"); //$NON-NLS-1$
		l_context = l_context.concat(_name);

		if (_ellipsoidMenu.getSelectionIndex() == CIB_PIT_k_USER_DEFINED) {
			l_result = _halfA.validate(p_warning, l_context);
			if (l_result) {
				_halfB.validate(p_warning, l_context);
			}
		}

		return l_result;
	}

}
