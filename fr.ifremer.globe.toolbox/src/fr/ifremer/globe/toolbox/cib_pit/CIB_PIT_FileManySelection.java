package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.toolbox.cib_pit.CIB_PIT_SelectionBox.ISelectionBox;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_Error;

public class CIB_PIT_FileManySelection extends CIB_PIT_ProcessParameter implements ISelectionBox {

	String _lockOption;
	Text _text;
	Button _button;
	CIB_PIT_FileManySelectionBox _fileManySelectionBox;

	public CIB_PIT_FileManySelection(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		_text = null;
		_button = null;
		_fileManySelectionBox = null;

		initialize(p_attributes);
	}

	public void initialize(StringBuffer p_attributes) {

		StringTokenizer l_token = new StringTokenizer(p_attributes.toString());
		int l_pos;

		if (!l_token.hasMoreTokens()) {
			_lockOption = CIB_PIT_FileManySelectionBox.CIB_PIT_k_FILEMANY_NOLOCK;

			//CIB_STC_Error.logError(Messages.getString("CIB_PIT_FileManySelection.0"), null); //$NON-NLS-1$
		} else {
			_lockOption = l_token.nextToken();
			_lockOption = _lockOption.trim();

			if (_lockOption.equals(CIB_PIT_FileManySelectionBox.CIB_PIT_k_FILEMANY_NOLOCK) || _lockOption.equals(CIB_PIT_FileManySelectionBox.CIB_PIT_k_FILEMANY_LOCKGEN)) {
				l_pos = p_attributes.indexOf(_lockOption);
				p_attributes.delete(l_pos, l_pos + _lockOption.length());
			} else {
				_lockOption = CIB_PIT_FileManySelectionBox.CIB_PIT_k_FILEMANY_NOLOCK;
			}
		}

		load();
		_text.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		_text.addMouseTrackListener(_helpTracker);
		_button.addMouseTrackListener(_helpTracker);

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;

		if (_fileManySelectionBox != null) {
			l_status = _fileManySelectionBox.validate(p_warning, p_context);
		}

		return l_status;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		if (_fileManySelectionBox != null) {
			_fileManySelectionBox.update(p_paramFile);
			apply(null);
		}
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		if (_fileManySelectionBox != null) {
			_fileManySelectionBox.dump(p_filePara);
		}
	}

	public void load() {

		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		_text = new Text(this, SWT.BOLD | SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
		_text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		_button = new Button(this, SWT.PUSH);
		_button.setText(Messages.getString("CIB_PIT_FileManySelection.1")); //$NON-NLS-1$
		_button.setLayoutData(new GridData(SWT.END, SWT.FILL, false, true));
		_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				select();
			}
		});

		// ************************************************************
		// Creation de la FileListSelectionBox
		// ************************************************************
		_fileManySelectionBox = new CIB_PIT_FileManySelectionBox(getDisplay(), _name, false, _lockOption);
		_fileManySelectionBox.setClientData(this);

		layout();

	}

	public void select() {
		if (_fileManySelectionBox != null) {
			_fileManySelectionBox.preLoad();
			_fileManySelectionBox.show();
		}
	}

	@Override
	public void apply(CIB_PIT_SelectionBox selectionBox) {
		_fileManySelectionBox.downLoad();
		_text.setText(_fileManySelectionBox.getDirectory());
	}

	@Override
	public void run(CIB_PIT_SelectionBox selectionBox) {
	}

	@Override
	public void cancel(CIB_PIT_SelectionBox selectionBox) {
	}

}
