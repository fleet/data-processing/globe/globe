package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.toolbox.cib_pit.CIB_PIT_SelectionBox.ISelectionBox;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_FileGenSelection extends CIB_PIT_ProcessParameter implements ISelectionBox {

	int _selectionType;
	Text _text;
	Button _button;
	CIB_PIT_FileGenSelectionBox _fileGenSelectionBox;

	public CIB_PIT_FileGenSelection(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		_selectionType = 0;
		_text = null;
		_button = null;
		_fileGenSelectionBox = null;

		initialize(p_attributes);
	}

	public void initialize(StringBuffer p_attributes) {

		load();
		_text.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_k_INPUT_COLOR));
		_text.addMouseTrackListener(_helpTracker);
		_button.addMouseTrackListener(_helpTracker);

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;

		if (_fileGenSelectionBox != null) {
			l_status = _fileGenSelectionBox.validate(p_warning, p_context);
		}

		return l_status;
	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		if (_fileGenSelectionBox != null) {
			_fileGenSelectionBox.update(p_paramFile);
			apply(null);
		}
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		if (_fileGenSelectionBox != null) {
			_fileGenSelectionBox.dump(p_filePara);
		}
	}

	public void load() {

		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		_text = new Text(this, SWT.BOLD | SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
		_text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		_button = new Button(this, SWT.PUSH);
		_button.setText(Messages.getString("CIB_PIT_FileGenSelection.0")); //$NON-NLS-1$
		_button.setLayoutData(new GridData(SWT.END, SWT.FILL, false, true));
		_button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				select();
			}
		});

		// ************************************************************
		// Creation de la FileListSelectionBox
		// ************************************************************
		_fileGenSelectionBox = new CIB_PIT_FileGenSelectionBox(getDisplay(), _name, false);
		_fileGenSelectionBox.setClientData(this);
		_selectionType = _fileGenSelectionBox.getSelectionType();

		layout();

	}

	public void select() {
		if (_fileGenSelectionBox != null) {
			_fileGenSelectionBox.changeInputType(_selectionType);
			_fileGenSelectionBox.preLoad();
			_fileGenSelectionBox.show();
		}
	}

	@Override
	public void apply(CIB_PIT_SelectionBox selectionBox) {
		_selectionType = _fileGenSelectionBox.getSelectionType();
		_fileGenSelectionBox.downLoad();
		_text.setText(_fileGenSelectionBox.getMainValue());
	}

	@Override
	public void run(CIB_PIT_SelectionBox selectionBox) {
	}

	@Override
	public void cancel(CIB_PIT_SelectionBox selectionBox) {
	}

}
