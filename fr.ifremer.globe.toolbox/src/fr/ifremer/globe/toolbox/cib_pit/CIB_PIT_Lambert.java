package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Lambert extends CIB_PIT_ProcessParameter {

	protected static final int CIB_PIT_Lambert_France_93 = 0;
	protected static final int CIB_PIT_Lambert_France_1 = 1;
	protected static final int CIB_PIT_Lambert_France_2 = 2;
	protected static final int CIB_PIT_Lambert_France_3 = 3;
	protected static final int CIB_PIT_Lambert_France_4 = 4;
	protected static final int CIB_PIT_Lambert_France_1_Etendu = 5;
	protected static final int CIB_PIT_Lambert_France_2_Etendu = 6;
	protected static final int CIB_PIT_Lambert_France_3_Etendu = 7;
	protected static final int CIB_PIT_Lambert_France_4_Etendu = 8;
	protected static final int CIB_PIT_EuropeLambert = 9;
	protected static final int CIB_PIT_MondeLambert = 10;
	protected static final int CIB_PIT_AutreLambertSecant = 11;
	protected static final int CIB_PIT_AutreLambertTangent = 12;

	protected static final String CIB_PIT_k_EUROPE = "Europe"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_WORLD = "World"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_SECANT = "Other - Secant"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_TANGENT = "Other - Tangent"; //$NON-NLS-1$

	protected static final String CIB_PIT_k_NAME_LAMBERT = "NOM"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LAMBERT_LATITUDE_FIRST = "LATITUDE_FIRST"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LAMBERT_LATITUDE_SECOND = "LATITUDE_SECOND"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LAMBERT_LONGITUDE_WEST = "LONGITUDE_WEST"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LAMBERT_LONGITUDE_EAST = "LONGITUDE_EST"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LAMBERT_CENTRAL_MERIDIAN = "MERIDIEN"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LAMBERT_LONGITUDES_W_E = "LONGITUDES_W_E"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LAMBERT_INTERNALTYPE = "INTERNAL_TYPE"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_ORIGINE_X = "ORIGINE_X"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_ORIGINE_Y = "ORIGINE_Y"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_SCALE_FACTOR = "ECHELLE"; //$NON-NLS-1$

	protected Combo _typeMenu; // combo type de projection Lambert
	protected CIB_PIT_Latitude _lat1; // latitude
	protected CIB_PIT_Latitude _lat2; // latitude
	protected CIB_PIT_Longitude _longW; // longitude est
	protected CIB_PIT_Longitude _longE; // longitude ouest
	protected CIB_PIT_Longitude _longC; // meridien central
	protected Combo _meridianMenu; // combo type de meridien central
	protected CIB_PIT_Number _x0; // X origine
	protected CIB_PIT_Number _y0; // Y origine
	protected CIB_PIT_Number _scaleFactor; // facteur d'echelle

	protected CIB_PIT_Label _latSouthLabel; // label latitude sud
	protected CIB_PIT_Label _meridianLabel; // label meridien central
	protected CIB_PIT_Label _longWLabel; // label longitude ouest
	protected CIB_PIT_Label _longELabel; // label longitude est
	protected CIB_PIT_Label _longCLabel; // label longitude meridien central
	protected CIB_PIT_Label _latIsoLabel; // label latitude isometrique
	protected CIB_PIT_Label _latLabel; // label latitude conservee
	protected CIB_PIT_Label _lat1Label; // label latitude conservee 1
	protected CIB_PIT_Label _lat2Label; // label latitude conservee 2
	protected CIB_PIT_Label _origineLabel; // label origine
	protected CIB_PIT_Label _x0Label; // label origine x
	protected CIB_PIT_Label _x0UnitLabel; // label unite x
	protected CIB_PIT_Label _y0Label; // label origine y
	protected CIB_PIT_Label _y0UnitLabel; // label unite y
	protected CIB_PIT_Label _scalefactorLabel; // label facteur d'echelle

	public CIB_PIT_Lambert(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_Lambert(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		CIB_PIT_Label l_lab;

		// Type de Lambert
		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 2, 180, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setText(Messages.getString("CIB_PIT_Lambert.16")); //$NON-NLS-1$
		_typeMenu = new Combo(this, SWT.READ_ONLY);
		_typeMenu.setBounds(new CIB_PIT_Rect(200, 2, 200, 22).getRect());
		_typeMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.17")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.18")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.19")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.20")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.21")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.22")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.23")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.24")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.25")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.26")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.27")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.28")); //$NON-NLS-1$
		_typeMenu.add(Messages.getString("CIB_PIT_Lambert.29")); //$NON-NLS-1$
		_typeMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				typeOption();
			}
		});

		// Champs de saisie complementaires
		_lat1 = new CIB_PIT_Latitude(this, CIB_PIT_k_LAMBERT_LATITUDE_FIRST, new CIB_PIT_Rect(220, 35, 220, 22), _helpMessage, _helpContainer);
		_lat2 = new CIB_PIT_Latitude(this, CIB_PIT_k_LAMBERT_LATITUDE_SECOND, new CIB_PIT_Rect(220, 65, 220, 22), _helpMessage, _helpContainer);
		_longW = new CIB_PIT_Longitude(this, CIB_PIT_k_LAMBERT_LONGITUDE_WEST, new CIB_PIT_Rect(220, 100, 220, 22), _helpMessage, _helpContainer);
		_longE = new CIB_PIT_Longitude(this, CIB_PIT_k_LAMBERT_LONGITUDE_EAST, new CIB_PIT_Rect(220, 128, 220, 22), _helpMessage, _helpContainer);
		_longC = new CIB_PIT_Longitude(this, CIB_PIT_k_LAMBERT_CENTRAL_MERIDIAN, new CIB_PIT_Rect(220, 100, 220, 22), _helpMessage, _helpContainer);

		_x0 = new CIB_PIT_Number(this, CIB_PIT_k_ORIGINE_X, new CIB_PIT_Rect(157, 132, 80, 22), _helpMessage, _helpContainer);
		_y0 = new CIB_PIT_Number(this, CIB_PIT_k_ORIGINE_Y, new CIB_PIT_Rect(333, 132, 80, 22), _helpMessage, _helpContainer);
		_scaleFactor = new CIB_PIT_Number(this, CIB_PIT_k_SCALE_FACTOR, new CIB_PIT_Rect(220, 162, 100, 22), _helpMessage, _helpContainer);

		// Type de definition du meridien central
		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 2, 180, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setText(Messages.getString("CIB_PIT_Lambert.31")); //$NON-NLS-1$
		_meridianMenu = new Combo(this, SWT.READ_ONLY);
		_meridianMenu.setBounds(new CIB_PIT_Rect(220, 70, 160, 22).getRect());
		_meridianMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_meridianMenu.add(Messages.getString("CIB_PIT_Lambert.32")); //$NON-NLS-1$
		_meridianMenu.add(Messages.getString("CIB_PIT_Lambert.33")); //$NON-NLS-1$
		_meridianMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				meridianOption();
			}
		});

		// libelles compl�mentaires
		_latSouthLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 37, 170, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_latSouthLabel.setText(Messages.getString("CIB_PIT_Lambert.35")); //$NON-NLS-1$
		_meridianLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 70, 170, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_meridianLabel.setText(Messages.getString("CIB_PIT_Lambert.37")); //$NON-NLS-1$
		_longWLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 102, 170, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_longWLabel.setText(Messages.getString("CIB_PIT_Lambert.39")); //$NON-NLS-1$
		_longELabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 133, 170, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_longELabel.setText(Messages.getString("CIB_PIT_Lambert.41")); //$NON-NLS-1$
		_longCLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 102, 170, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_longCLabel.setText(Messages.getString("CIB_PIT_Lambert.43")); //$NON-NLS-1$

		_latIsoLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 37, 200, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_latIsoLabel.setText(Messages.getString("CIB_PIT_Lambert.45")); //$NON-NLS-1$
		_latLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 37, 150, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_latLabel.setText(Messages.getString("CIB_PIT_Lambert.47")); //$NON-NLS-1$
		_lat1Label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(175, 37, 50, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_lat1Label.setText(Messages.getString("CIB_PIT_Lambert.49")); //$NON-NLS-1$
		_lat2Label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(175, 67, 50, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_lat2Label.setText(Messages.getString("CIB_PIT_Lambert.51")); //$NON-NLS-1$
		_origineLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 132, 80, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_origineLabel.setText(Messages.getString("CIB_PIT_Lambert.53")); //$NON-NLS-1$
		_x0Label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(124, 132, 30, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_x0Label.setText("X0"); //$NON-NLS-1$
		_x0UnitLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(242, 132, 60, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_x0UnitLabel.setText(Messages.getString("CIB_PIT_Lambert.57")); //$NON-NLS-1$
		_y0Label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(308, 132, 30, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_y0Label.setText("Y0"); //$NON-NLS-1$
		_y0UnitLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(419, 132, 60, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_y0UnitLabel.setText(Messages.getString("CIB_PIT_Lambert.61")); //$NON-NLS-1$
		_scalefactorLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 162, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_scalefactorLabel.setText(Messages.getString("CIB_PIT_Lambert.63")); //$NON-NLS-1$

		_meridianMenu.select(0);
		meridianOption();

		_typeMenu.select(CIB_PIT_Lambert_France_93);
		typeOption();
	}

	protected void meridianOption() {

		switch (_meridianMenu.getSelectionIndex()) {
		case 0: // longitude
			_longWLabel.setVisible(false);
			_longW.setVisible(false);
			_longELabel.setVisible(false);
			_longE.setVisible(false);
			_longCLabel.setVisible(true);
			_longC.setVisible(true);
			break;
		case 1: // milieu ouest-est
			_longWLabel.setVisible(true);
			_longW.setVisible(true);
			_longELabel.setVisible(true);
			_longE.setVisible(true);
			_longCLabel.setVisible(false);
			_longC.setVisible(false);
			break;
		}

	}

	protected void typeOption() {

		_latIsoLabel.setVisible(false);
		_latLabel.setVisible(false);
		_latSouthLabel.setVisible(false);
		_lat1Label.setVisible(false);
		_lat1.setVisible(false);
		_lat2Label.setVisible(false);
		_lat2.setVisible(false);
		_meridianLabel.setVisible(false);
		_meridianMenu.setVisible(false);
		_longWLabel.setVisible(false);
		_longW.setVisible(false);
		_longELabel.setVisible(false);
		_longE.setVisible(false);
		_longCLabel.setVisible(false);
		_longC.setVisible(false);
		_origineLabel.setVisible(false);
		_x0Label.setVisible(false);
		_x0.setVisible(false);
		_x0UnitLabel.setVisible(false);
		_y0Label.setVisible(false);
		_y0.setVisible(false);
		_y0UnitLabel.setVisible(false);
		_scalefactorLabel.setVisible(false);
		_scaleFactor.setVisible(false);

		switch (_typeMenu.getSelectionIndex()) {
		case CIB_PIT_EuropeLambert:
			_meridianLabel.setVisible(true);
			_meridianMenu.setVisible(true);
			meridianOption();
			break;
		case CIB_PIT_MondeLambert:
			_latSouthLabel.setVisible(true);
			_lat1.setVisible(true);
			_meridianLabel.setVisible(true);
			_meridianMenu.setVisible(true);
			meridianOption();
			break;
		case CIB_PIT_AutreLambertSecant:
			_latLabel.setVisible(true);
			_lat1Label.setVisible(true);
			_lat1.setVisible(true);
			_lat2Label.setVisible(true);
			_lat2.setVisible(true);
			_longCLabel.setVisible(true);
			_longC.setVisible(true);
			_origineLabel.setVisible(true);
			_x0Label.setVisible(true);
			_x0.setVisible(true);
			_x0UnitLabel.setVisible(true);
			_y0Label.setVisible(true);
			_y0.setVisible(true);
			_y0UnitLabel.setVisible(true);
			_scalefactorLabel.setVisible(true);
			_scaleFactor.setVisible(true);
			break;
		case CIB_PIT_AutreLambertTangent:
			_latIsoLabel.setVisible(true);
			_lat1.setVisible(true);
			_longCLabel.setVisible(true);
			_longC.setVisible(true);
			_origineLabel.setVisible(true);
			_x0Label.setVisible(true);
			_x0.setVisible(true);
			_x0UnitLabel.setVisible(true);
			_y0Label.setVisible(true);
			_y0.setVisible(true);
			_y0UnitLabel.setVisible(true);
			_scalefactorLabel.setVisible(true);
			_scaleFactor.setVisible(true);
			break;
		}

	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		int l_projType = 0;
		int l_option = 0;
		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		l_value = CIB_DPR_ProcessParameter.read(l_filePara, CIB_PIT_k_NAME_LAMBERT);

		if (l_value.equals(CIB_PIT_k_EUROPE)) {
			l_projType = CIB_PIT_EuropeLambert;
		} else if (l_value.equals(CIB_PIT_k_WORLD)) {
			l_projType = CIB_PIT_MondeLambert;
		} else if (l_value.equals(CIB_PIT_k_SECANT)) {
			l_projType = CIB_PIT_AutreLambertSecant;
		} else if (l_value.equals(CIB_PIT_k_TANGENT)) {
			l_projType = CIB_PIT_AutreLambertTangent;
		} else {
			for (int l_idx = 0; l_idx < _typeMenu.getItemCount(); l_idx++) {
				if (l_value.equals(_typeMenu.getItems()[l_idx])) {
					l_projType = l_idx;
				}
			}
		}
		_typeMenu.select(l_projType);
		typeOption();

		switch (l_projType) {
		case CIB_PIT_EuropeLambert:
		case CIB_PIT_MondeLambert:
			_lat1.update(l_filePara);
			l_value = CIB_DPR_ProcessParameter.read(l_filePara, CIB_PIT_k_LAMBERT_INTERNALTYPE);
			if (l_value.equals(CIB_PIT_k_LAMBERT_CENTRAL_MERIDIAN)) {
				l_option = 0;
				_longC.update(l_filePara);
			} else if (l_value.equals(CIB_PIT_k_LAMBERT_LONGITUDES_W_E)) {
				l_option = 1;
				_longW.update(l_filePara);
				_longE.update(l_filePara);
			}
			_meridianMenu.select(l_option);
			meridianOption();
			break;
		case CIB_PIT_AutreLambertSecant:
		case CIB_PIT_AutreLambertTangent:
			_lat1.update(l_filePara);
			_lat2.update(l_filePara);
			_longC.update(l_filePara);
			_x0.update(l_filePara);
			_y0.update(l_filePara);
			_scaleFactor.update(l_filePara);
			break;
		}

	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		switch (_typeMenu.getSelectionIndex()) {
		case CIB_PIT_Lambert_France_93:
		case CIB_PIT_Lambert_France_1:
		case CIB_PIT_Lambert_France_2:
		case CIB_PIT_Lambert_France_3:
		case CIB_PIT_Lambert_France_4:
		case CIB_PIT_Lambert_France_1_Etendu:
		case CIB_PIT_Lambert_France_2_Etendu:
		case CIB_PIT_Lambert_France_3_Etendu:
		case CIB_PIT_Lambert_France_4_Etendu:
			l_value = _typeMenu.getItems()[_typeMenu.getSelectionIndex()];
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_NAME_LAMBERT, l_value);
			break;
		case CIB_PIT_EuropeLambert:
		case CIB_PIT_MondeLambert:
			if (_typeMenu.getSelectionIndex() == CIB_PIT_EuropeLambert) {
				l_value = CIB_PIT_k_EUROPE;
			} else {
				l_value = CIB_PIT_k_WORLD;
			}
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_NAME_LAMBERT, l_value);
			_lat1.dump(l_filePara);
			switch (_meridianMenu.getSelectionIndex()) {
			case 0: // longitude
				l_value = CIB_PIT_k_LAMBERT_CENTRAL_MERIDIAN;
				_longC.dump(l_filePara);
				break;
			case 1: // milieu ouest-est
				l_value = CIB_PIT_k_LAMBERT_LONGITUDES_W_E;
				_longW.dump(l_filePara);
				_longE.dump(l_filePara);
				break;
			}
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_LAMBERT_INTERNALTYPE, l_value);
			break;
		case CIB_PIT_AutreLambertSecant:
		case CIB_PIT_AutreLambertTangent:
			if (_typeMenu.getSelectionIndex() == CIB_PIT_AutreLambertSecant) {
				l_value = CIB_PIT_k_SECANT;
			} else {
				l_value = CIB_PIT_k_TANGENT;
			}
			CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_NAME_LAMBERT, l_value);
			_lat1.dump(l_filePara);
			_lat2.dump(l_filePara);
			_longC.dump(l_filePara);
			_x0.dump(l_filePara);
			_y0.dump(l_filePara);
			_scaleFactor.dump(l_filePara);
			break;
		}

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_result = true;
		String l_context;

		l_context = p_context;
		l_context = l_context.concat("->"); //$NON-NLS-1$
		l_context = l_context.concat(_name);

		switch (_typeMenu.getSelectionIndex()) {
		case CIB_PIT_EuropeLambert:
		case CIB_PIT_MondeLambert:
			if (_typeMenu.getSelectionIndex() == CIB_PIT_MondeLambert) {
				l_result = _lat1.validate(p_warning, l_context);
			}

			if (l_result) {
				switch (_meridianMenu.getSelectionIndex()) {
				case 0: // longitude
					l_result = _longC.validate(p_warning, l_context);
					break;
				case 1: // milieu ouest-est
					l_result = _longW.validate(p_warning, l_context);
					if (l_result) {
						l_result = _longE.validate(p_warning, l_context);
					}
					break;
				}
			}
			break;
		case CIB_PIT_AutreLambertSecant:
		case CIB_PIT_AutreLambertTangent:
			l_result = _lat1.validate(p_warning, l_context);
			if (l_result) {
				if (_typeMenu.getSelectionIndex() == CIB_PIT_AutreLambertSecant) {
					l_result = _lat2.validate(p_warning, l_context);
				}
			}
			if (l_result) {
				l_result = _longC.validate(p_warning, l_context);
			}
			if (l_result) {
				l_result = _x0.validate(p_warning, l_context);
			}
			if (l_result) {
				l_result = _y0.validate(p_warning, l_context);
			}
			if (l_result) {
				l_result = _scaleFactor.validate(p_warning, l_context);
			}
			break;
		}

		return l_result;
	}

}
