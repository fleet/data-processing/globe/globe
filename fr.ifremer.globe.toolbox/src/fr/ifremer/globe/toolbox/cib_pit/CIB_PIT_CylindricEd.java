package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_CylindricEd extends CIB_PIT_ProcessParameter {

	protected static final String CIB_PIT_k_LATITUDE = "LATITUDE"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_LONGITUDE = "LONGITUDE"; //$NON-NLS-1$

	CIB_PIT_Latitude _latitude; // latitude
	CIB_PIT_Longitude _longitude; // longitude

	public CIB_PIT_CylindricEd(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_CylindricEd(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		CIB_PIT_Label l_lab;

		// Latitude echelle conserv�e
		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 37, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setText(Messages.getString("CIB_PIT_CylindricEd.3")); //$NON-NLS-1$
		_latitude = new CIB_PIT_Latitude(this, CIB_PIT_k_LATITUDE, new CIB_PIT_Rect(220, 35, 220, 22), _helpMessage, _helpContainer);

		// Meridien origine
		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 72, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setText(Messages.getString("CIB_PIT_CylindricEd.5")); //$NON-NLS-1$
		_longitude = new CIB_PIT_Longitude(this, CIB_PIT_k_LONGITUDE, new CIB_PIT_Rect(220, 70, 220, 22), _helpMessage, _helpContainer);

	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		String l_value;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		_latitude.update(l_filePara);
		_longitude.update(l_filePara);

	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		_latitude.dump(l_filePara);
		_longitude.dump(l_filePara);

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_result = true;
		String l_context;

		l_context = p_context;
		l_context = l_context.concat("->"); //$NON-NLS-1$
		l_context = l_context.concat(_name);

		if ((l_result = _latitude.validate(p_warning, l_context))) {
			l_result = _longitude.validate(p_warning, l_context);
		}

		return l_result;
	}

}
