package fr.ifremer.globe.toolbox.cib_pit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_SelectionBox {

	public interface ISelectionBox {
		abstract void apply(CIB_PIT_SelectionBox selectionBox);

		abstract void run(CIB_PIT_SelectionBox selectionBox);

		abstract void cancel(CIB_PIT_SelectionBox selectionBox);
	}

	protected String _paramName;
	protected boolean _autoHide;
	protected boolean _isApplied;
	protected ISelectionBox _clientData;
	protected Shell _shell;
	protected Composite _mainContainer;
	protected Label _helpContainer;

	protected Button _runButton;

	public CIB_PIT_SelectionBox(Display p_display, String p_name, String p_title, boolean p_visible) {
		_shell = new Shell(p_display, SWT.APPLICATION_MODAL | SWT.RESIZE | SWT.CLOSE);

		_paramName = p_name;
		_autoHide = true;
		_isApplied = false;

		_shell.setText(p_title);
		_shell.setVisible(p_visible);

		initialize();
	}

	private void initialize() {

		GridLayout gridLayout = new GridLayout(1, false);
		_shell.setLayout(gridLayout);

		_mainContainer = new Composite(_shell, SWT.NONE);
		_mainContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Composite actions = new Composite(_shell, SWT.NONE);
		actions.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		RowLayout l_actionslayout = new RowLayout(SWT.HORIZONTAL);
		l_actionslayout.center = true;
		l_actionslayout.justify = true;
		actions.setLayout(l_actionslayout);

		// Bouton Valider
		Button okButton = new Button(actions, SWT.NONE);
		okButton.setText(Messages.getString("CIB_PIT_SelectionBox.0")); //$NON-NLS-1$
		okButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				super.widgetSelected(e);
				apply();
			};
		});

		// Bouton Executer
		_runButton = new Button(actions, SWT.NONE);
		_runButton.setText(Messages.getString("CIB_PIT_SelectionBox.1")); //$NON-NLS-1$
		_runButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				run();
			}
		});

		// Bouton Annuler
		Button cancelButton = new Button(actions, SWT.NONE);
		cancelButton.setText(Messages.getString("CIB_PIT_SelectionBox.2")); //$NON-NLS-1$
		cancelButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				cancel();
			}
		});

		actions.layout();

		// Footer
		_helpContainer = new Label(_shell, SWT.LEFT | SWT.BOLD | SWT.BORDER);
		_helpContainer.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT));
		_helpContainer.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

		_shell.pack();

	}

	private void apply() {

		if (validateSelection()) {
			_isApplied = true;
			if (_autoHide) {
				_shell.setVisible(false);
			}
			if (_clientData != null) {
				_clientData.apply(this);
			}
		}
	}

	private void run() {
		_isApplied = true;
		_shell.setVisible(false);
		if (_clientData != null) {
			_clientData.run(this);
		}
	}

	private void cancel() {
		_isApplied = false;
		_shell.setVisible(false);
		if (_clientData != null) {
			_clientData.cancel(this);
		}
	}

	public void setClientData(ISelectionBox p_clientData) {
		_clientData = p_clientData;
	}

	public void setAutoHide(boolean p_autoHide) {
		_autoHide = p_autoHide;
	}

	public void useRun(boolean p_useRun) {
		_runButton.setVisible(p_useRun);
	}

	public boolean validateSelection() {
		return true;
	}

	public boolean isApplied() {
		return _isApplied;
	}

	public void setName(String p_paramName) {
		_paramName = p_paramName;
	}

	public void update(CIB_STC_AccessFilePara p_context) {
	}

	public void dump(CIB_STC_AccessFilePara p_context) {
	}

	public void show() {
		_shell.open();
		var display = Display.getDefault();
		while (!_shell.isDisposed() && !display.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public void dispose() {
		_shell.dispose();
	}

}
