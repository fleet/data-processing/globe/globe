package fr.ifremer.globe.toolbox.cib_pit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

public class CIB_PIT_Frame extends CIB_PIT_ProcessParameter {

	protected Group _group;

	public CIB_PIT_Frame(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize(p_attributes);
	}

	public void initialize() {

		load();
		Rectangle l_bounds = getBounds();
		_group.setBounds(l_bounds.x + 2, l_bounds.y, l_bounds.width - 10, l_bounds.height);
		_group.moveBelow(null);
		if (_helpTracker != null) {
			_group.addMouseTrackListener(_helpTracker);
		}
	}

	public void initialize(StringBuffer p_attributes) {

		String l_tmp;

		initialize();

		l_tmp = extractString(p_attributes);
		_group.setText(l_tmp);
	}

	public void load() {

		_group = new Group(_parent, SWT.NONE);

	}

	@Override
	public void hide() {
	}

}
