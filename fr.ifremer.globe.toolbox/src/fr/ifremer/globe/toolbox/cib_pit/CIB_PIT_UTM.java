package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_UTM extends CIB_PIT_ProcessParameter {

	protected static final String CIB_PIT_k_LONGITUDE = "LONGITUDE"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_ZONE = "FUSEAU"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_HEMISPHERE = "HEMISPHERE"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_FLAG = "INTERNAL_TYPE"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_UTM_MERIDIEN = "MERIDIEN"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_UTM_NORMEE = "NORMEE"; //$NON-NLS-1$
	protected static final String CIB_PIT_k_UTM_NON_NORMEE = "NON_NORMEE"; //$NON-NLS-1$

	protected Combo _hemisphereMenu;
	protected Combo _meridianMenu;
	protected Combo _normalizedMenu;
	protected CIB_PIT_Label _longCLabel;
	protected CIB_PIT_Longitude _longC;
	protected CIB_PIT_Label _zoneLabel;
	protected CIB_PIT_Number _zone;
	protected CIB_PIT_Label _x0Label;
	protected CIB_PIT_Number _x0;
	protected CIB_PIT_Label _x0UnitLabel;
	protected CIB_PIT_Label _y0Label;
	protected CIB_PIT_Number _y0;
	protected CIB_PIT_Label _y0UnitLabel;

	public CIB_PIT_UTM(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_UTM(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		CIB_PIT_Label l_lab;

		// Hemisphere
		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 2, 200, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setText(Messages.getString("CIB_PIT_UTM.8")); //$NON-NLS-1$
		_hemisphereMenu = new Combo(this, SWT.READ_ONLY);
		_hemisphereMenu.setBounds(new CIB_PIT_Rect(220, 2, 70, 22).getRect());
		_hemisphereMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_hemisphereMenu.add(Messages.getString("CIB_PIT_UTM.9")); //$NON-NLS-1$
		_hemisphereMenu.add(Messages.getString("CIB_PIT_UTM.10")); //$NON-NLS-1$
		_hemisphereMenu.select(0);

		// Type de definition du meridien central
		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 37, 200, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setText(Messages.getString("CIB_PIT_UTM.12")); //$NON-NLS-1$
		_meridianMenu = new Combo(this, SWT.READ_ONLY);
		_meridianMenu.setBounds(new CIB_PIT_Rect(220, 37, 154, 22).getRect());
		_meridianMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_meridianMenu.add(Messages.getString("CIB_PIT_UTM.13")); //$NON-NLS-1$
		_meridianMenu.add(Messages.getString("CIB_PIT_UTM.14")); //$NON-NLS-1$
		_meridianMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				meridianOption();
			}
		});

		_longCLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 70, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_longCLabel.setText(Messages.getString("CIB_PIT_UTM.16")); //$NON-NLS-1$
		_longC = new CIB_PIT_Longitude(this, CIB_PIT_k_LONGITUDE, new CIB_PIT_Rect(220, 70, 220, 22), _helpMessage, _helpContainer);

		_zoneLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 70, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_zoneLabel.setText(Messages.getString("CIB_PIT_UTM.18")); //$NON-NLS-1$
		_zone = new CIB_PIT_Number(this, CIB_PIT_k_ZONE, new CIB_PIT_Rect(220, 72, 24, 22), _helpMessage, _helpContainer);

		// UTM normalisee O/N
		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 107, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setText(Messages.getString("CIB_PIT_UTM.20")); //$NON-NLS-1$
		_normalizedMenu = new Combo(this, SWT.READ_ONLY);
		_normalizedMenu.setBounds(new CIB_PIT_Rect(220, 107, 65, 22).getRect());
		_normalizedMenu.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_normalizedMenu.add(Messages.getString("CIB_PIT_UTM.21")); //$NON-NLS-1$
		_normalizedMenu.add(Messages.getString("CIB_PIT_UTM.22")); //$NON-NLS-1$
		_normalizedMenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				normalizedOption();
			}
		});

		_x0Label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 139, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_x0Label.setText(Messages.getString("CIB_PIT_UTM.24")); //$NON-NLS-1$
		_x0 = new CIB_PIT_Number(this, "X0", new CIB_PIT_Rect(220, 139, 100, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_x0UnitLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(325, 139, 50, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_x0UnitLabel.setText(Messages.getString("CIB_PIT_UTM.27")); //$NON-NLS-1$

		_y0Label = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(10, 167, 210, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_y0Label.setText(Messages.getString("CIB_PIT_UTM.29")); //$NON-NLS-1$
		_y0 = new CIB_PIT_Number(this, "Y0", new CIB_PIT_Rect(220, 167, 100, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_y0UnitLabel = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(325, 167, 50, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		_y0UnitLabel.setText(Messages.getString("CIB_PIT_UTM.32")); //$NON-NLS-1$

		_meridianMenu.select(0);
		meridianOption();

		_normalizedMenu.select(0);
		normalizedOption();

	}

	protected void meridianOption() {

		switch (_meridianMenu.getSelectionIndex()) {
		case 0: // longitude
			_longCLabel.setVisible(true);
			_longC.setVisible(true);
			_zoneLabel.setVisible(false);
			_zone.setVisible(false);
			break;
		case 1: // Fuseau
			_longCLabel.setVisible(false);
			_longC.setVisible(false);
			_zoneLabel.setVisible(true);
			_zone.setVisible(true);
			break;
		}

	}

	protected void normalizedOption() {

		switch (_normalizedMenu.getSelectionIndex()) {
		case 0: // Oui
			_x0Label.setVisible(false);
			_x0.setVisible(false);
			_x0UnitLabel.setVisible(false);
			_y0Label.setVisible(false);
			_y0.setVisible(false);
			_y0UnitLabel.setVisible(false);
			break;
		case 1: // Non
			_x0Label.setVisible(true);
			_x0.setVisible(true);
			_x0UnitLabel.setVisible(true);
			_y0Label.setVisible(true);
			_y0.setVisible(true);
			_y0UnitLabel.setVisible(true);
			break;
		}

	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		String l_value;
		String l_hemisphere;
		String l_string;
		String l_flagString;
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);
		l_filePara.setByString(l_value);

		l_hemisphere = CIB_DPR_ProcessParameter.read(l_filePara, CIB_PIT_k_HEMISPHERE);
		if (l_hemisphere.equals(CIB_DPR_ProcessParameter.CIB_DPR_k_LATITUDE_NORTH)) {
			_hemisphereMenu.select(0);
		} else {
			_hemisphereMenu.select(1);
		}

		l_flagString = CIB_DPR_ProcessParameter.read(l_filePara, CIB_PIT_k_FLAG);
		StringTokenizer l_token1 = new StringTokenizer(l_flagString);

		l_string = l_filePara.readAll();
		StringTokenizer l_token2 = new StringTokenizer(l_string);

		l_value = l_token1.nextToken();
		if (l_value.equals(CIB_PIT_k_ZONE)) {
			_meridianMenu.select(1);
			_zone.setText(l_token2.nextToken());
		} else if (l_value.equals(CIB_PIT_k_UTM_MERIDIEN)) {
			_meridianMenu.select(0);
			_longC.update(l_filePara);
		}
		meridianOption();

		l_value = l_token1.nextToken();
		if (l_value.equals(CIB_PIT_k_UTM_NORMEE)) {
			_normalizedMenu.select(0);
		} else if (l_value.equals(CIB_PIT_k_UTM_NON_NORMEE)) {
			_normalizedMenu.select(1);
			_x0.setText(l_token2.nextToken());
			_y0.setText(l_token2.nextToken());
		}
		normalizedOption();
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {

		String l_hemisphere;
		String l_string = ""; //$NON-NLS-1$
		String l_flagString = ""; //$NON-NLS-1$
		String l_longitude = ""; //$NON-NLS-1$
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();

		switch (_meridianMenu.getSelectionIndex()) {
		case 0: // longitude
			l_flagString = CIB_PIT_k_UTM_MERIDIEN;
			l_flagString += " "; //$NON-NLS-1$
			_longC.dump(l_filePara);
			l_longitude = l_filePara.readAll();
			break;
		case 1: // Fuseau
			l_flagString = CIB_PIT_k_ZONE;
			l_flagString += " "; //$NON-NLS-1$
			l_string = _zone.getText();
			l_string += " "; //$NON-NLS-1$
			break;
		}

		switch (_normalizedMenu.getSelectionIndex()) {
		case 0: // type UTM normee
			l_flagString += CIB_PIT_k_UTM_NORMEE;
			l_flagString += " "; //$NON-NLS-1$
			break;
		case 1: // type UTM non normee
			l_flagString += CIB_PIT_k_UTM_NON_NORMEE;
			l_flagString += " "; //$NON-NLS-1$
			l_string += _x0.getText();
			l_string += " "; //$NON-NLS-1$
			l_string += _y0.getText();
			l_string += " "; //$NON-NLS-1$
			break;
		}

		l_string += l_longitude;

		l_filePara.setByString(l_string);

		CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_FLAG, l_flagString);

		if (_hemisphereMenu.getSelectionIndex() == 0) {
			l_hemisphere = CIB_DPR_ProcessParameter.CIB_DPR_k_LATITUDE_NORTH;
		} else {
			l_hemisphere = CIB_DPR_ProcessParameter.CIB_DPR_k_LATITUDE_SOUTH;
		}
		CIB_DPR_ProcessParameter.write(l_filePara, CIB_PIT_k_HEMISPHERE, l_hemisphere);

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_filePara.readAll());
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_result = true;
		String l_context;

		l_context = p_context;
		l_context = l_context.concat("->"); //$NON-NLS-1$
		l_context = l_context.concat(_name);

		switch (_meridianMenu.getSelectionIndex()) {
		case 0: // longitude
			l_result = _longC.validate(p_warning, l_context);
			break;
		case 1: // Fuseau
			l_result = _zone.validate(p_warning, l_context);
			break;
		}

		if (l_result) {
			switch (_normalizedMenu.getSelectionIndex()) {
			case 0: // type UTM normee
				break;
			case 1: // type UTM non normee
				l_result = _x0.validate(p_warning, l_context);
				if (l_result) {
					_y0.validate(p_warning, l_context);
				}
				break;
			}
		}

		return l_result;
	}

}
