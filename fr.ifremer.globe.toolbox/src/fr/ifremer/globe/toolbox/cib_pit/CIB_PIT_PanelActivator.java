package fr.ifremer.globe.toolbox.cib_pit;

import org.eclipse.swt.widgets.Label;

public class CIB_PIT_PanelActivator extends CIB_PIT_PanelChooser {

	protected CIB_PIT_ParameterPanel _panel;

	public CIB_PIT_PanelActivator(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
		_panel = null;
	}

	public void setPanel(CIB_PIT_ParameterPanel p_panel) {
		_panel = p_panel;
	}

	public CIB_PIT_ParameterPanel getPanel() {
		return _panel;
	}

	@Override
	public void show() {

		super.show();
		((CIB_PIT_PanelFolder) getParentParameter()).needRebuild();
	}

	@Override
	public void hide() {

		super.hide();
		((CIB_PIT_PanelFolder) getParentParameter()).needRebuild();
	}

}
