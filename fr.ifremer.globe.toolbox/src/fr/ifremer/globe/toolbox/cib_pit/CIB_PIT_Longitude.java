package fr.ifremer.globe.toolbox.cib_pit;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.toolbox.cib_dpr.CIB_DPR_ProcessParameter;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;

public class CIB_PIT_Longitude extends CIB_PIT_ProcessParameter {

	protected static int CIB_PIT_k_LONGITUDE_E = 0; // choix combo
	protected static int CIB_PIT_k_LONGITUDE_W = 1; // choix combo

	protected Combo _longCombo;
	protected CIB_PIT_Number _deg;
	protected CIB_PIT_Number _min;

	public CIB_PIT_Longitude(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);

		initialize();
	}

	public CIB_PIT_Longitude(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);

		initialize();
	}

	public void initialize() {

		load();
	}

	public void load() {

		CIB_PIT_Label l_lab;

		GridLayout layout = new GridLayout(5, false);
		GridData gridData = new GridData();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		// Combo N/S
		_longCombo = new Combo(this, SWT.READ_ONLY);
		_longCombo.setBackground(Display.getCurrent().getSystemColor(CIB_PIT_Component.CIB_PIT_k_INPUT_COLOR));
		_longCombo.add(CIB_DPR_ProcessParameter.CIB_DPR_k_LONGITUDE_EAST);
		_longCombo.add(CIB_DPR_ProcessParameter.CIB_DPR_k_LONGITUDE_WEST);
		gridData.widthHint = 15;
		_longCombo.setLayoutData(gridData);

		_deg = new CIB_PIT_Number(this, _name, new CIB_PIT_Rect(0, 0, 30, 22), _helpMessage, _helpContainer);
		_deg.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, true));

		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(0, 0, 30, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true));
		l_lab.setText(Messages.getString("CIB_PIT_Longitude.1")); //$NON-NLS-1$

		_min = new CIB_PIT_Number(this, _name, new CIB_PIT_Rect(0, 0, 60, 22), _helpMessage, _helpContainer);
		_min.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, true));

		l_lab = new CIB_PIT_Label(this, "Label", new CIB_PIT_Rect(0, 0, 30, 22), _helpMessage, _helpContainer); //$NON-NLS-1$
		l_lab.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true));
		l_lab.setText(Messages.getString("CIB_PIT_Longitude.4")); //$NON-NLS-1$

		layout();

	}

	@Override
	public void update(CIB_STC_AccessFilePara p_paramFile) {
		String l_value = ""; //$NON-NLS-1$
		String l_str = ""; //$NON-NLS-1$

		l_value = CIB_DPR_ProcessParameter.read(p_paramFile, _name);

		StringTokenizer l_token = new StringTokenizer(l_value);
		l_str = l_token.nextToken();
		if (l_str.equals(CIB_DPR_ProcessParameter.CIB_DPR_k_LONGITUDE_WEST)) {
			_longCombo.select(CIB_PIT_k_LONGITUDE_W);
		} else {
			_longCombo.select(CIB_PIT_k_LONGITUDE_E);
		}

		l_str = l_token.nextToken();
		_deg.setText(l_str);

		l_str = l_token.nextToken();
		_min.setText(l_str);
	}

	@Override
	public void dump(CIB_STC_AccessFilePara p_filePara) {
		String l_value = ""; //$NON-NLS-1$
		int l_deg;
		double l_min;

		if (_longCombo.getSelectionIndex() == CIB_PIT_k_LONGITUDE_W) {
			l_value += CIB_DPR_ProcessParameter.CIB_DPR_k_LONGITUDE_WEST;
		} else {
			l_value += CIB_DPR_ProcessParameter.CIB_DPR_k_LONGITUDE_EAST;
		}
		l_value += " ";

		try {
			l_deg = Math.abs(new Double(_deg.getText()).intValue());
		} catch (Exception e) {
			l_deg = 0;
		}
		l_value += l_deg;
		l_value += " ";

		NumberFormat l_formatter = NumberFormat.getInstance(Locale.UK);
		l_formatter.setMaximumFractionDigits(5);
		l_formatter.setMinimumFractionDigits(1);
		try {
			l_min = Math.abs(new Double(_min.getText()).doubleValue());
		} catch (Exception e) {
			l_min = 0.0;
		}
		l_value += l_formatter.format(l_min);

		CIB_DPR_ProcessParameter.write(p_filePara, _name, l_value);
	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;
		Double l_longitude = 0.0;

		l_longitude = getVal();
		if (l_longitude > 180.0 || l_longitude < -180.0) {
			l_status = false;
		}

		if (!l_status) {
			MessageDialog.openError(getShell(), Messages.getString("CIB_PIT_Longitude.5"), Messages.getString("CIB_PIT_Longitude.6")); //$NON-NLS-1$ //$NON-NLS-2$
		}

		return l_status;
	}

	public double getVal() {

		double l_longitude = 0.0;
		int l_deg;
		double l_min;

		try {
			l_deg = Math.abs(new Double(_deg.getText()).intValue());
		} catch (Exception e) {
			l_deg = 0;
		}

		try {
			l_min = Math.abs(new Double(_min.getText()).doubleValue());
		} catch (Exception e) {
			l_min = 0.0;
		}
		l_longitude = l_deg + l_min / CIB_DPR_ProcessParameter.CIB_DPR_k_DEGREE_MINUTE;
		if (_longCombo.getSelectionIndex() == CIB_PIT_k_LONGITUDE_W) {
			l_longitude *= -1.0;
		}

		return l_longitude;
	}

}
