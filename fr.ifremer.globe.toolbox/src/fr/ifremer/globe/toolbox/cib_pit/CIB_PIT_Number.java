package fr.ifremer.globe.toolbox.cib_pit;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.toolbox.tool.Tool;

public class CIB_PIT_Number extends CIB_PIT_TextField {
	protected Logger logger = LoggerFactory.getLogger(Tool.class);


	public CIB_PIT_Number(CIB_PIT_ProcessParameter p_parent, StringBuffer p_attributes, Label p_helpContainer) {
		super(p_parent, p_attributes, p_helpContainer);
	}

	public CIB_PIT_Number(Composite parent, String p_name, CIB_PIT_Rect p_size, String p_helpMessage, Label p_helpContainer) {
		super(parent, p_name, p_size, p_helpMessage, p_helpContainer);
	}

	@Override
	public void initialize() {

		super.initialize();

		_text.addListener(SWT.Verify, new Listener() {
			@Override
			public void handleEvent(Event event) {
				String value = event.text;
				for (int i = 0; i < value.length(); i++) {
					char c = value.charAt(i);
					if (!('0' <= c && c <= '9') && !(c == '+' || c == '-' || c == '.' || c == ',' || c == 'E')) {
						event.doit = false;
						getDisplay().beep();
					}
				}
			}
		});

	}

	@Override
	public boolean validate(List<String> p_warning, String p_context) {

		boolean l_status = true;
		String l_value;
		String l_context;
		String l_warning;

		l_value = _text.getText();

		if (l_value.isEmpty()) {
			l_context = p_context;
			l_context = l_context.concat("->"); //$NON-NLS-1$
			l_context = l_context.concat(_name);

			l_warning = l_context;
			l_warning = l_warning.concat("->"); //$NON-NLS-1$

			l_warning = l_warning.concat(Messages.getString("CIB_PIT_Number.2")); //$NON-NLS-1$
			p_warning.add(l_warning);
		} else {
			try {
				new Double(l_value);
			} catch (Exception e) {
				l_context = p_context;
				l_context = l_context.concat("->"); //$NON-NLS-1$
				l_context = l_context.concat(_name);

				l_warning = l_context;
				l_warning = l_warning.concat("->"); //$NON-NLS-1$

				l_warning = l_warning.concat(Messages.getString("CIB_PIT_Number.5")); //$NON-NLS-1$
				p_warning.add(l_warning);
				logger.info(" erreur parameter PIT NUMBER " + l_value);

				l_status = false;
			}
		}

		return l_status;
	}

	@Override
	public void load() {

		_text = new Text(this, SWT.RIGHT | SWT.BOLD | SWT.SINGLE | SWT.BORDER);

	}

}
