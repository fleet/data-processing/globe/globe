/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.toolbox;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.jobs.Job;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.toolbox.tool.ToolJob;
import fr.ifremer.globe.ui.dialogs.PickWorkspaceDialog;
import fr.ifremer.globe.ui.utils.Messages;

public class Activator implements BundleActivator {

	private static final Logger LOGGER = LoggerFactory.getLogger(Activator.class);

	protected final static String TOOLBOX_K_TMP_PATH = "Toolbox/CIB_Temp";

	@Override
	public void start(BundleContext context) throws Exception {
		// NOTHING TO DO
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		disposeResources();
	}

	private void disposeResources() {
		LOGGER.info("Dispose Caraibes resources...");

		// delete the contents of the toolbox temporary directory (Caraibes modules)
		Path tmpPath = Paths.get(PickWorkspaceDialog.getLastSetWorkspaceDirectory() + "/" + TOOLBOX_K_TMP_PATH);
		if (tmpPath.toFile().exists()) {
			try {
				FileUtils.cleanDirectory(tmpPath.toFile());
			} catch (IOException e) {
				Messages.openErrorMessage("Toolbox temporary diectory",
						"Impossible to delete files in : " + TOOLBOX_K_TMP_PATH + "\n" + e.toString());
			}
		}

		// Closing properly CARAIBES "jobs"
		Arrays.stream(Job.getJobManager().find(null))
				.filter(job -> job.getName().contains("Toolbox") && job.belongsTo(ToolJob.CARAIBES_TOOLBOX))
				.forEach(Job::cancel);
	}

}