package fr.ifremer.globe.toolbox.tool;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.ui.dialogs.PickWorkspaceDialog;
import fr.ifremer.globe.ui.utils.Messages;

public class ToolConfig {

	protected final static String TOOLBOX_K_ROOT = "Toolbox";
	protected final static String TOOLBOX_K_CONFIG = "SYS/DAT/CIB_Toolbox_WIN.cfg";
	protected final static String TOOLBOX_K_ERROR = "SYS/DAT/CIB_Error";
	protected final static String TOOLBOX_K_BIN_DIR = "outils/bin";
	protected final static String TOOLBOX_K_INIT_VAL_DIR = "VAL";
	protected final static String TOOLBOX_K_VAL_DIR = "Toolbox/VAL";
	protected final static String TOOLBOX_K_VAL_EXT = ".val";
	protected final static String TOOLBOX_K_PROCESS_FILE = "PROCESS_FILE";
	protected final static String TOOLBOX_K_TMP_PATH = "Toolbox/CIB_Temp";
	protected final static String TOOLBOX_K_LANG_UK = "UK";

	private static ToolConfig instance = null;

	private String _path;
	private String _country;
	private File _toolboxRoot;

	private Hashtable<String, String> executableNames;

	/** return the singleton instance **/
	public static ToolConfig getInstance() {
		if (instance == null) {
			try {
				instance = new ToolConfig();
			} catch (IOException e) {
				Messages.openErrorMessage("Toolbox Temporary directory (.val)", "Impossible to create in workspace: " + TOOLBOX_K_TMP_PATH + "\n" + e.toString());
			}
		}
		return instance;
	}

	/** @see #getInstance() */
	private ToolConfig()  throws IOException{

		// initialisation du chemin
		_path = getPath();

		// initialisation de la langue
		_country = Locale.getDefault().getCountry();

		// always display english labels
		_country = TOOLBOX_K_LANG_UK;

		// initialisation de la racine des descriptions de modules
		String l_toolboxRoot = _path + "/" + _country;
		_toolboxRoot = new File(l_toolboxRoot);
		
		// création du répertoire dans le workspace s'il n'existe pas
		java.nio.file.Path tmpPath = Paths.get(PickWorkspaceDialog.getLastSetWorkspaceDirectory() + "/" + TOOLBOX_K_TMP_PATH);	

		if (!tmpPath.toFile().exists())	
			Files.createDirectories(tmpPath);
	
		// init hashtable

		executableNames = new Hashtable<String, String>();
		
		executableNames.put(new String("Coratt"), new String("CIB_MPC_Coratt.exe"));
		executableNames.put(new String("FlagMbb"), new String("CIB_MPC_FlagMbb.exe"));
		executableNames.put(new String("Mailla"), new String("CIB_MPC_Mailla.exe"));
		executableNames.put(new String("MbgUpd"), new String("CIB_MPC_MbgUpd.exe"));
		
		executableNames.put(new String("Difmnt"), new String("CIB_MPC_Difmnt.exe"));
		executableNames.put(new String("DtmUpd"), new String("CIB_MPC_DtmUpd.exe"));
		executableNames.put(new String("Invmnt"), new String("CIB_MPC_Invmnt.exe"));
		
		executableNames.put(new String("Modcel"), new String("CIB_MPC_Modcel.exe"));
		
		executableNames.put(new String("ImpTide"), new String("CIB_MPC_ImpTide.exe"));

		executableNames.put(new String("Tasd"), new String("CIB_MPC_Tasd.exe"));
		executableNames.put(new String("Ts2100"), new String("CIB_MPC_Ts2100.exe"));
		executableNames.put(new String("TGeoS"), new String("CIB_MPC_TGeoS.exe"));
		
		executableNames.put(new String("Fusnav"), new String("CIB_MPC_Fusnav.exe"));
		executableNames.put(new String("ImpNav"), new String("CIB_MPC_ImpNav.exe"));

		executableNames.put(new String("ImToMbg"), new String("CIB_MIM_ImToMbg.exe"));
		}

	
	// ** returns the path to current plugin */
	public String getPath() {
		String l_path = "";
		try {
			Bundle bundle = Platform.getBundle("fr.ifremer.globe.toolbox");
			URL locationUrl = FileLocator.find(bundle, new Path(TOOLBOX_K_ROOT), null);
			URL fileUrl = FileLocator.toFileURL(locationUrl);
			l_path = new Path(fileUrl.getPath()).toString();
		} catch (IOException e) {
			System.err.println("Error while trying to get the path to Toolbox directory.");
		}
		return l_path;
	}

	// ** returns the current country */
	public String getLang() {
		return _country;
	}

	// ** Returns the toolbox root for current language *//
	public File getToolboxRoot() {
		return _toolboxRoot;
	}

	// ** Returns the name of toolbox configuration file *//
	public String getConfigName() {
		return _path + "/" + TOOLBOX_K_CONFIG;
	}

	// ** Returns the name of process configuration file *//
	public String getProcessName() {
		CIB_STC_AccessFilePara l_config = new CIB_STC_AccessFilePara();
		l_config.setByFile(getConfigName());
		return l_config.read(TOOLBOX_K_PROCESS_FILE);
	}

	// ** Returns the name of VAL file for a module *//
	public String getValName(String p_toolName) {
		// Get the last used workspace location
		String lastUsedWs = PickWorkspaceDialog.getLastSetWorkspaceDirectory();
		// Get the directory of user files .val
		java.nio.file.Path valUserPath = Paths.get(lastUsedWs + "/" + TOOLBOX_K_VAL_DIR);
		// Get temporary directory
		java.nio.file.Path tmpPath = Paths.get(lastUsedWs + "/" + TOOLBOX_K_TMP_PATH);		
		if (!valUserPath.toFile().exists())
			try {
				Files.createDirectories(valUserPath);
			} catch (IOException e) {
				Messages.openErrorMessage("Directory of parameters files (.val)", "Impossible to create in workspace: " + TOOLBOX_K_VAL_DIR + "\n" + e.toString());
				return null;
			}
		if (!tmpPath.toFile().exists())
			try {
				Files.createDirectories(tmpPath);
			} catch (IOException e) {
				Messages.openErrorMessage("Toolbox Temporary directory (.val)", "Impossible to create in workspace: " + TOOLBOX_K_TMP_PATH + "\n" + e.toString());
				return null;
			}
		java.nio.file.Path valFileUserPath = valUserPath.resolve(p_toolName + TOOLBOX_K_VAL_EXT);
		// Check if user file .val exists.
		if (!valFileUserPath.toFile().exists()) {
			// Copy .val by default 
			java.nio.file.Path valFileInitPath = Paths.get(_path + "/" + TOOLBOX_K_INIT_VAL_DIR + "/" + p_toolName + TOOLBOX_K_VAL_EXT);
			if (valFileInitPath.toFile().exists()) {
				try {
					Files.copy(valFileInitPath, valFileUserPath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
		return valFileUserPath.toString();


	}

	// ** Returns the name of EXE file for a module *//
	public String getExeName(String p_toolName) {

		String exeName = this.getPath();

		exeName += "/MPC/";

		String osName = System.getProperty("os.name");
		if (osName != null) {
			if (osName.contains("Windows")) {
				exeName += "/EXE_WIN/";
			} else if (osName.contains("Linux")) {
				exeName += "/EXE_LINUX/";
			} else if (osName.contains("Mac")) {
				exeName += "/EXE_MAC/";
			} else {
				return null;
			}
		}
		exeName += executableNames.get(new String(p_toolName));

		return exeName;

		/*
		 * CIB_STC_AccessFilePara l_config = new CIB_STC_AccessFilePara();
		 * l_config.setByFile(getProcessName()); return
		 * l_config.read(p_toolName);
		 */
	}

	// ** Returns array of module environment variables *//
	public String[] getToolEnv() {
		List<String> l_envArray = new ArrayList<String>();

		String osName = System.getProperty("os.name");
		if (osName != null) {
			if (osName.contains("Windows")) {
				l_envArray.add("CYGWIN=nodosfilewarning");
				l_envArray.add("CIB_OS=_WIN");
				l_envArray.add("CIB_SITE=" + _path);
				l_envArray.add("CIB_ERROR=" + _path + "/" + TOOLBOX_K_ERROR);
				l_envArray.add("CIB_CONF=" + _path + "/" + TOOLBOX_K_CONFIG);
				l_envArray.add("CIB_LANG=" + getLang());
				l_envArray.add("PATH=.;" + _path + "/" + TOOLBOX_K_BIN_DIR);
				writeCIB_Toolbox_WIN();
			} else if (osName.contains("Linux")) {
				l_envArray.add("LD_LIBRARY_PATH=" + this.getPath() + "/MPC/EXE_LINUX/");
				l_envArray.add("CIB_OS=_L64");
				l_envArray.add("CIB_SITE=" + _path);
				l_envArray.add("CIB_ERROR=" + _path + "/" + TOOLBOX_K_ERROR);
				l_envArray.add("CIB_CONF=" + _path + "/" + TOOLBOX_K_CONFIG);
				l_envArray.add("CIB_LANG=" + getLang());
				l_envArray.add("PATH=.;" + _path + "/" + TOOLBOX_K_BIN_DIR);
				writeCIB_Toolbox_WIN();
			} else if (osName.contains("Mac")) {
				l_envArray.add("CIB_OS=_MAC");
				l_envArray.add("CIB_SITE=" + _path);
				l_envArray.add("CIB_ERROR=" + _path + "/" + TOOLBOX_K_ERROR);
				l_envArray.add("CIB_CONF=" + _path + "/" + TOOLBOX_K_CONFIG);
				l_envArray.add("CIB_LANG=" + getLang());
				l_envArray.add("PATH=.:/bin:/usr/bin:" + _path + "/" + TOOLBOX_K_BIN_DIR);
				writeCIB_Toolbox_WIN();
			} else {
				return null;
			}
		}

		return l_envArray.toArray(new String[l_envArray.size()]);
	}

	/*
	 * rewrite CIB_Toolbox_WIN.cfg to be independent from platform
	 */

	public void writeCIB_Toolbox_WIN() {
		String fileName = _path + "/" + TOOLBOX_K_CONFIG;
		FileWriter outFile = null;
		try {
			outFile = new FileWriter(fileName);
		} catch (IOException e) {

			e.printStackTrace();
		}

		PrintWriter out = new PrintWriter(outFile);
		out.println("SITE_NAME               {GLOBE Toolbox}");
		out.println("FLOWLOG_PATH 		{" + _path + "/SYS/DAT/CIB_ProcessingFlowLog}");
		out.println("TMP_PATH		{" + PickWorkspaceDialog.getLastSetWorkspaceDirectory().replace('\\', '/') + "/" + TOOLBOX_K_TMP_PATH + "} ");
		out.println("PALETTE_PATH		{" + _path + "}");
		out.println("CONF_PATH		{" + _path + "/SYS/DAT/CIB_Configuration}");
		out.println("PROCESS_FILE		{" + _path + "/SYS/DAT/CIB_Process_WIN.cfg}");
		out.println("DEFAULT_PATH		{" + _path + "}");
		out.println("DEFAULT_PATH		{" + _path + "}");
		out.println("SMF_VERSION_NUMBER {200}");
		out.println("END_CFG {}");
		out.close();
	}
}