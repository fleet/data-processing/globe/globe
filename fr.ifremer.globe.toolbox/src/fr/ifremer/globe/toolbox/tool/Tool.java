package fr.ifremer.globe.toolbox.tool;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.toolbox.cib_pit.CIB_PIT_Component;
import fr.ifremer.globe.toolbox.cib_pit.CIB_PIT_ParameterBox;
import fr.ifremer.globe.toolbox.cib_pit.CIB_PIT_SelectionBox;
import fr.ifremer.globe.toolbox.cib_pit.CIB_PIT_SelectionBox.ISelectionBox;
import fr.ifremer.globe.toolbox.cib_stc.CIB_STC_AccessFilePara;
import fr.ifremer.globe.toolbox.toolboxExplorer.ToolboxConsole;
import fr.ifremer.globe.toolbox.toolboxExplorer.ToolboxExplorer;
import fr.ifremer.globe.ui.workspace.WorkspaceDirectory;

public class Tool extends File implements ISelectionBox {

	protected Logger logger = LoggerFactory.getLogger(Tool.class);

	private static final long serialVersionUID = 1L;

	protected final static String TOOL_K_DESCRIPTION = "DESCRIPTION";
	protected final static String TOOL_K_HELP = "HELP";
	protected final static String TOOL_K_RULES = "RULES";
	protected final static String TOOL_K_PROCESS_RUNNER = "PROCESS_RUNNER";
	public static final String TOOL_K_DSC = "dsc";

	protected String _name;
	protected String _fileName;
	protected String _description;
	protected String _exec;
	protected ToolboxExplorer _toolboxExplorer;

	/** Constructor based on a .dsc file name **/
	public Tool(String p_fileName, ToolboxExplorer toolboxExplorer) {
		super(p_fileName);
		_toolboxExplorer = toolboxExplorer;
		_fileName = p_fileName;
		_name = getName().substring(0, getName().lastIndexOf('.'));

		ReadDescFile();
	}

	/** Extract description and executable name from .dsc file **/
	protected void ReadDescFile() {
		CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();
		l_filePara.setByFile(_fileName);
		_exec = null;
		String l_rules = l_filePara.read(TOOL_K_RULES);
		if (l_rules.length() > 0) {
			String delimiter = "\n";
			String[] tmpStr = l_rules.split(delimiter);
			for (int i = 0; i < tmpStr.length && _exec == null; i++) {
				if (tmpStr[i].contains(TOOL_K_PROCESS_RUNNER)) {
					String[] tmpStr2 = tmpStr[i].split("\"");
					if (tmpStr2.length > 1) {
						_exec = tmpStr2[1];
					}
				}
			}
		}

		String l_desc = l_filePara.read(TOOL_K_DESCRIPTION);
		if (l_desc.length() > 0) {
			l_filePara.setByString(l_desc);
			_description = l_filePara.read(TOOL_K_HELP);
		}
	}

	/** Check if a file name is a .dsc file name **/
	static public boolean isModule(String p_fileName) {
		boolean l_status = false;
		String l_name = p_fileName;
		String l_extension = l_name.substring(l_name.lastIndexOf('.') + 1);
		if (l_extension.equals(Tool.TOOL_K_DSC)) {
			CIB_STC_AccessFilePara l_filePara = new CIB_STC_AccessFilePara();
			l_filePara.setByFile(p_fileName);

			String[] l_param = new String[2];
			l_param[0] = TOOL_K_DESCRIPTION;
			if (l_filePara.findBloc(l_param)) {
				l_status = true;
			}
		}
		return l_status;
	}

	/** return the module name */
	public String name() {
		return _name;
	}

	/** return the description string */
	public String description() {
		return _description;
	}

	/** return the long descritpion string */
	public String label() {
		return String.format("%s - %s", _name, _description); //$NON-NLS-1$
	}

	/** Initializes and opens the parameter window */
	public void open() {
		// Create window if .val (parameters file) exists or can be create
		String l_valFileName = ToolConfig.getInstance().getValName(_name);
		if (l_valFileName != null) {
			CIB_PIT_ParameterBox l_paramBox = new CIB_PIT_ParameterBox(Display.getDefault(), _name, label(), true);
		
			l_paramBox.setClientData(this);
			l_paramBox.readFile(_fileName);
			CIB_PIT_Component.setDefaultPath(WorkspaceDirectory.getInstance().getPath());

			File l_valFile = new File(l_valFileName);
			if (l_valFile.exists() && !l_valFile.isDirectory()) {
				l_paramBox.update(l_valFileName);
			}
			// show window
			l_paramBox.show();
		}

	}

	/** runs the module */
	public void run(ToolboxConsole p_console) {
		ToolConfig l_config = ToolConfig.getInstance();
		String l_valFileName = l_config.getValName(_name);
		String l_exeFileName = l_config.getExeName(_name);

		if (l_valFileName == null) {
			logger.error("Name of parameter file not found");
		} else {
			File l_valFile = new File(l_valFileName);
			if (!l_valFile.exists()) {
				logger.error("The parameter file does not exist : " + l_valFileName);
			} else {
				if (l_exeFileName == null) {
					logger.error("Name of executable file not found");
				} else {
					File l_exeFile = new File(l_exeFileName);
					if (!l_exeFile.exists()) {
						logger.error("The executable file does not exist : ");
					} else {
						final String command = '"' + l_exeFileName + '"' + " 1 " + l_valFileName;
						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						Date date = new Date();
						final String processName = "Toolbox_exec at " + dateFormat.format(date) + " : " + _name ;

						ToolJob job = new ToolJob(l_exeFile.getName(),  processName, command, l_config.getToolEnv(), p_console);
						//job.setUser(true);
						job.schedule();
						logger.info("Running : " + command);
					}
				}
			}
		}
	}

	@Override
	public void apply(CIB_PIT_SelectionBox selectionBox) {
		if (((CIB_PIT_ParameterBox) selectionBox).validate()) {
			String l_valFileName = ToolConfig.getInstance().getValName(_name);
			if (l_valFileName != null) {
				try {
					File l_valFile = new File(l_valFileName);
					if (!l_valFile.exists()) {
						l_valFile.createNewFile();
					}
				} catch (Throwable ex) {
					logger.error("Creation failed for parameter file : " + _fileName, ex);

				}
				((CIB_PIT_ParameterBox) selectionBox).dump(l_valFileName);
			}
			selectionBox.dispose();
		} else {
			logger.info("Error on parameter input.");
		}

	}

	@Override
	public void run(CIB_PIT_SelectionBox selectionBox) {
		apply(selectionBox);
		run();
	}

	public void run() {
		_toolboxExplorer.run(this);
	}

	@Override
	public void cancel(CIB_PIT_SelectionBox selectionBox) {
		selectionBox.dispose();
	}

}
