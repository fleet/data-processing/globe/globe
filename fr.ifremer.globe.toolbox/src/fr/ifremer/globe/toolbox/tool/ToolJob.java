package fr.ifremer.globe.toolbox.tool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;
import fr.ifremer.globe.toolbox.toolboxExplorer.ToolboxConsole;
import fr.ifremer.globe.ui.utils.Messages;

public class ToolJob extends Job {

	public static final Object CARAIBES_TOOLBOX = "CARAIBES_TOOLBOX";
	protected String _processName;
	protected String _exeName;
	protected String _command;
	protected String[] _env;
	protected ToolboxConsole _console;
	private int pid = -1;

	public ToolJob(String p_exeName, String p_processName, String p_command, String[] p_env, ToolboxConsole p_console) {
		super(p_processName);
		_processName = p_processName;
		_exeName = p_exeName;
		_command = p_command;
		_env = p_env;
		_console = p_console;
	}

	@Override
	public boolean belongsTo(Object family) {
	   return family == CARAIBES_TOOLBOX;
	}
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			String osName = System.getProperty("os.name");
			String[] cmd = null;

			if (osName != null) {
				// first case
				// if os is linux
				// first , set the correct rights on the exe file

				if (osName.contains("Linux") || osName.contains("Mac")) {
					cmd = new String[3];
					// first , set the correct rights on the exe file
					cmd[0] = "chmod";
					cmd[1] = "777";
					cmd[2] = _command.substring(0, _command.indexOf(".exe ") + 4);

					try {
						Runtime.getRuntime().exec(cmd);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if (osName.contains("Windows")) {
					cmd = new String[3];
					cmd[0] = new String("cmd");
					cmd[1] = new String("/c");
					cmd[2] = _command;
				} else if (osName.contains("Linux") || osName.contains("Mac")) {
					// command is : "<executable.exe> 1 <parameter_file>"
					// split _command in three Strings
					cmd = new String[3];
					cmd[0] = _command.substring(0, _command.indexOf(".exe ") + 4);
					cmd[1] = "1";
					cmd[2] = _command.substring(_command.indexOf(".exe ") + 8, _command.length());

				} else {
					// ????
				}
			}
			//Launch process
			final Process l_process = Runtime.getRuntime().exec(cmd, _env);

			// Get pid command
			String command = "";
			
            // Search PID of executable 
			if (osName.contains("Windows")) {
				command = "taskList /fi \"imagename eq " + _exeName + "\"" + " /v /fo csv";
				
				System.out.println(command);
			} else if (osName.contains("Linux") || osName.contains("Mac")) {
				command = "echo $$";
			}
			final Process  l_pidProcess = Runtime.getRuntime().exec(command);
			if (l_pidProcess != null) {
				// Consommation de la sortie d'erreur de l'application externe
				// dans un Thread separe
				Thread pidThread = new Thread() {
					@Override
					public void run() {
						try {
							BufferedReader reader =
									new BufferedReader(new InputStreamReader(l_pidProcess.getInputStream()));
							String cvsSplitBy = ",";
							String line = "";
							long minProcTime = 999999;
							try {
								if (osName.contains("Windows")) {
									while ((line = reader.readLine()) != null && !monitor.isCanceled()) {
										String[] column = line.split(cvsSplitBy);
										if (column[0].contains(_exeName)) {
											try {
												DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss z");
												//process time
												Long procTime = dateFormat.parse(column[7].replace("\"", "") + " GMT").getTime();
												if(procTime < minProcTime) {
													minProcTime = procTime;
													pid = Integer.parseInt(column[1].replaceAll("\"", ""));
													System.out.println("Pid retenu :" + pid);
												}
											}
											catch (NumberFormatException | ParseException ioe) {
												ioe.printStackTrace();
											}
										}
									}
									if (osName.contains("Linux") || osName.contains("Mac")) {
										if((line = reader.readLine()) != null)
											pid  = Integer.parseInt(line);
									}
								}					
							} finally {
								reader.close();
								if (monitor.isCanceled()) {
									l_pidProcess.destroyForcibly();
								}
							}
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}		
					}
				};
				pidThread.start();	
			}
			l_pidProcess.waitFor();
	
			if (l_process != null) {
				// Consommation de la sortie d'erreur de l'application externe
				// dans un Thread separe
				Thread thread = new Thread() {
					@Override
					public void run() {
						try {
							BufferedReader reader = new BufferedReader(new InputStreamReader(l_process.getErrorStream(), "iso-8859-1"));
							String line = "";
							final StringBuffer l_buffer = new StringBuffer();
							try {
								while ((line = reader.readLine()) != null && !monitor.isCanceled()) {
									// Traitement du flux d'erreur de
									// l'application si besoin est
									if (_console != null) 
										if (_console.isDisposed() ) {
											monitor.setCanceled(true);	
											Messages.openWarningMessage( "Stop process ", _processName + " is stopped");
										}
										else { 
											l_buffer.setLength(0);
											l_buffer.append(line);
											l_buffer.append('\n');
											Display.getDefault().syncExec(new Runnable() {
												@Override
												public void run() {
													if(!_console.isDisposed())
														_console.append(l_buffer);
												}
											});
										}
								}								
							} finally {
								reader.close();
								if (monitor.isCanceled()) {
						            // killing the task.
									if (pid != -1) {
										System.out.println(" Pid stop= " + pid); //<-- Parse data here.
										if (osName.contains("Windows")) 
											Runtime.getRuntime().exec("taskkill /F /PID " + pid);
										else if (osName.contains("Linux") || osName.contains("Mac")) {	
											Runtime.getRuntime().exec("kill -9 " + pid);
										}
									}
									l_process.destroyForcibly();
								}
							}
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
					}
				};
				thread.start();
			}

			l_process.waitFor();
			refreshProjectSpace();


		} catch (Exception e) {
			e.printStackTrace();
		}
		return Status.OK_STATUS;
	}
	

	private void refreshProjectSpace() {

	}
}
