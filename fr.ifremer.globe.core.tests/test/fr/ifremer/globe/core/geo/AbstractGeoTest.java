package fr.ifremer.globe.core.geo;

import org.gdal.osr.CoordinateTransformation;
import org.gdal.osr.SpatialReference;
import org.junit.Before;

import fr.ifremer.globe.core.model.projection.StandardProjection;

public abstract class AbstractGeoTest {

	protected static final int CYCLE_COUNT = 50;
	protected static final int BEAM_COUNT = 800;

	protected static double[] coords1D = new double[CYCLE_COUNT * BEAM_COUNT * 2];
	protected static double[][] coords2D = new double[CYCLE_COUNT * BEAM_COUNT][2];

	@Before
	public void initialize() {
		for (int i = 0; i < coords1D.length; i += 2) {
			coords1D[i] = -4 + 10.0 / coords1D.length * i;
			coords1D[i + 1] = 40 + 5.5 / coords1D.length * i;
		}
		for (int i = 0; i < coords2D.length; i++) {
			coords2D[i][0] = -4 + 10.0 / coords2D.length * i;
			coords2D[i][1] = 40 + 5.5 / coords2D.length * i;
		}
	}

	public CoordinateTransformation getCoordinateTransformation() {
		SpatialReference sourceSR = new SpatialReference();
		sourceSR.ImportFromProj4("+proj=latlong +datum=WGS84 +no_defs");

		SpatialReference destSR = new SpatialReference();
		destSR.ImportFromProj4(StandardProjection.MERCATOR.proj4String());

		return new CoordinateTransformation(sourceSR, destSR);
	}
}
