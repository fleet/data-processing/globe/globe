package fr.ifremer.globe.core.geo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.gdal.osr.CoordinateTransformation;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.geo.GeoSegment;
import fr.ifremer.globe.core.model.geo.GeoUtils;
import fr.ifremer.globe.core.model.geo.GeoZone;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.utils.perfcounter.PerfoCounter;

public class GeoTest extends AbstractGeoTest {

	private static final int numberOfGeneration = 1000;

	/**
	 * Test of method GeoBox.intersects
	 */
	@Test
	public void testIntersecGeobox() {
		GeoBox reference = new GeoBox(43.6621801, 43.6587610, -1.5669411, -1.4998103, StandardProjection.LONGLAT);

		Assert.assertTrue(reference.intersects(reference));
		Assert.assertFalse(reference.intersects(GeoBox.EMPTY));
		Assert.assertFalse(GeoBox.EMPTY.intersects(reference));

		GeoBox aBitUpper = new GeoBox(reference.getTop() + 1e-7, reference.getBottom(), reference.getLeft(),
				reference.getRight(), StandardProjection.LONGLAT);
		Assert.assertTrue(reference.intersects(aBitUpper));
		Assert.assertTrue(aBitUpper.intersects(reference));

		GeoBox aBitlower = new GeoBox(reference.getTop(), reference.getBottom() - 1e-7, reference.getLeft(),
				reference.getRight(), StandardProjection.LONGLAT);
		Assert.assertTrue(reference.intersects(aBitlower));
		Assert.assertTrue(aBitlower.intersects(reference));

		GeoBox aBitFurtherWest = new GeoBox(reference.getTop(), reference.getBottom(), reference.getLeft() - 1e-7,
				reference.getRight(), StandardProjection.LONGLAT);
		Assert.assertTrue(reference.intersects(aBitFurtherWest));
		Assert.assertTrue(aBitFurtherWest.intersects(reference));

		GeoBox aBitFurtherEast = new GeoBox(reference.getTop(), reference.getBottom(), reference.getLeft(),
				reference.getRight() + 1e-7, StandardProjection.LONGLAT);
		Assert.assertTrue(reference.intersects(aBitFurtherEast));
		Assert.assertTrue(aBitFurtherEast.intersects(reference));

		GeoBox aBitSmaller = new GeoBox(reference.getTop() - 1e-7, reference.getBottom() + 1e-7,
				reference.getLeft() + 1e-7, reference.getRight() - 1e-7, StandardProjection.LONGLAT);
		Assert.assertTrue(reference.intersects(aBitSmaller));
		Assert.assertTrue(aBitSmaller.intersects(reference));

		GeoBox aBitBigger = new GeoBox(reference.getTop() + 1e-7, reference.getBottom() - 1e-7,
				reference.getLeft() - 1e-7, reference.getRight() + 1e-7, StandardProjection.LONGLAT);
		Assert.assertTrue(reference.intersects(aBitBigger));
		Assert.assertTrue(aBitBigger.intersects(reference));

		GeoBox furtherNorth = new GeoBox(reference.getTop() + 1, reference.getBottom() + 1, reference.getLeft(),
				reference.getRight(), StandardProjection.LONGLAT);
		Assert.assertFalse(reference.intersects(furtherNorth));
		Assert.assertFalse(furtherNorth.intersects(reference));

		GeoBox furtherSouth = new GeoBox(reference.getTop() - 1, reference.getBottom() - 1, reference.getLeft(),
				reference.getRight(), StandardProjection.LONGLAT);
		Assert.assertFalse(reference.intersects(furtherSouth));
		Assert.assertFalse(furtherSouth.intersects(reference));

		GeoBox furtherWest = new GeoBox(reference.getTop(), reference.getBottom(), reference.getLeft() - 1,
				reference.getRight() - 1, StandardProjection.LONGLAT);
		Assert.assertFalse(reference.intersects(furtherWest));
		Assert.assertFalse(furtherWest.intersects(reference));

		GeoBox furtherEast = new GeoBox(reference.getTop(), reference.getBottom(), reference.getLeft() + 1,
				reference.getRight() + 1, StandardProjection.LONGLAT);
		Assert.assertFalse(reference.intersects(furtherEast));
		Assert.assertFalse(furtherEast.intersects(reference));

	}

	@Test
	public void testGeoZone() {
		GeoZone zone = new GeoZone();
		zone.addPoint(new GeoPoint(-4.004, 48.008));
		zone.addPoint(new GeoPoint(-4.003, 48.006));
		zone.addPoint(new GeoPoint(-4.002, 48.006));
		zone.addPoint(new GeoPoint(-4.002, 48.008));

		GeoBox box = new GeoBox(48.009, 48.007, -4.003, -4.005);

		Assert.assertTrue(GeoUtils.intersects(zone, box));
	}

	@Test
	public void testGeoZone1() {

		GeoZone zone1 = new GeoZone();
		zone1.addPoint(new GeoPoint(4.001, -48.001));
		zone1.addPoint(new GeoPoint(4.001, -48.003));
		zone1.addPoint(new GeoPoint(4.004, -48.003));
		zone1.addPoint(new GeoPoint(4.004, -48.001));

		GeoZone zone2 = new GeoZone();
		zone2.addPoint(new GeoPoint(4.003, -48.002));
		zone2.addPoint(new GeoPoint(4.002, -48.005));
		zone2.addPoint(new GeoPoint(4.005, -48.005));
		zone2.addPoint(new GeoPoint(4.006, -48.004));

		Assert.assertTrue(GeoUtils.overlaps(zone1, zone2));
	}

	@Test
	public void testGeoZoneOverlaps() {

		GeoZone zone1 = new GeoZone();
		zone1.addPoint(new GeoPoint(4, 0));
		zone1.addPoint(new GeoPoint(8, 4));
		zone1.addPoint(new GeoPoint(4, 8));
		zone1.addPoint(new GeoPoint(0, 3));

		GeoZone zone2 = new GeoZone();
		zone2.addPoint(new GeoPoint(3, 3));
		zone2.addPoint(new GeoPoint(5, 3));
		zone2.addPoint(new GeoPoint(5, 5));
		zone2.addPoint(new GeoPoint(3, 5));

		Assert.assertTrue(GeoUtils.overlaps(zone1, zone2));
		Assert.assertTrue(GeoUtils.overlaps(zone2, zone1));
	}

	@Test
	public void testGeoZone2() {

		GeoZone zone1 = new GeoZone();
		zone1.addPoint(new GeoPoint(-4.244256944456413, 47.39519383338921));
		zone1.addPoint(new GeoPoint(-4.2452628553753025, 47.394251214561514));
		zone1.addPoint(new GeoPoint(-4.243791720209158, 47.393446448437));
		zone1.addPoint(new GeoPoint(-4.242785809290269, 47.394389081749054));

		GeoZone zone2 = new GeoZone();
		zone2.addPoint(new GeoPoint(-4.246123154786827, 47.39668571025917));
		zone2.addPoint(new GeoPoint(-4.246038312806632, 47.394726960017245));
		zone2.addPoint(new GeoPoint(-4.24146124757876, 47.39482858410756));
		zone2.addPoint(new GeoPoint(-4.241546089558955, 47.39678733054861));

		Assert.assertTrue(GeoUtils.overlaps(zone1, zone2));
	}

	@Test
	public void testGeoSegment() {
		GeoZone zone = new GeoZone(1, -1, 1, -1);

		GeoSegment s1 = new GeoSegment(new GeoPoint(-2, 0), new GeoPoint(0, 2));
		Assert.assertTrue(GeoUtils.intersects(zone, s1));

		GeoSegment s2 = new GeoSegment(new GeoPoint(0, 0), new GeoPoint(0, 2));
		Assert.assertTrue(GeoUtils.intersects(zone, s2));

		GeoSegment s3 = new GeoSegment(new GeoPoint(-2, -2), new GeoPoint(-3, -3));
		Assert.assertFalse(GeoUtils.intersects(zone, s3));

		GeoSegment s4 = new GeoSegment(new GeoPoint(-0.5, -0.5), new GeoPoint(-0.5, -0.5));
		Assert.assertFalse(GeoUtils.intersects(zone, s4));
	}

	private class GDALCallable implements Callable<Double> {

		private CoordinateTransformation ct;
		private double[][] coords;

		public GDALCallable(CoordinateTransformation ct, double[][] coords) {
			this.ct = ct;
			this.coords = coords;
		}

		@Override
		public Double call() throws Exception {
			ct.TransformPoints(coords);
			return 0.0;
		}
	}

	// @Test
	public double[][] testGDALIndividualConversion() {

		CoordinateTransformation ct = getCoordinateTransformation();

		double[][] projectedCoords = new double[coords2D.length][2];

		PerfoCounter sequentialPerf = new PerfoCounter(
				String.format("Sequential for %d cycles, %d beams", CYCLE_COUNT, BEAM_COUNT));
		sequentialPerf.start();
		for (int i = 0; i < coords2D.length; i++) {
			double[] xy = ct.TransformPoint(coords2D[i][0], coords2D[i][1]);
			projectedCoords[i][0] = xy[0];
			projectedCoords[i][1] = xy[1];
		}
		sequentialPerf.stop();

		return projectedCoords;
	}

	// @Test
	public void compareIndivudualBatchResults() {

		double[][] projectedCoords = testGDALIndividualConversion();

		testGDALBatchConversion();

		// comparison
		for (int i = 0; i < coords2D.length; i++) {
			Assert.assertTrue(Double.compare(coords2D[i][0], projectedCoords[i][0]) == 0);
			Assert.assertTrue(Double.compare(coords2D[i][1], projectedCoords[i][1]) == 0);
		}
	}

	@Test
	public void testGDALMultithreading() {

		PerfoCounter multithreadPerf = new PerfoCounter(
				String.format("Multithread for %d cycles, %d beams", CYCLE_COUNT, BEAM_COUNT));
		multithreadPerf.start();

		for (int n = 0; n < numberOfGeneration; n++) {
			CoordinateTransformation ct = getCoordinateTransformation();

			// initialize();

			int threadCount = 4;
			ExecutorService executor = Executors.newFixedThreadPool(threadCount);
			List<FutureTask<Double>> tasks = new ArrayList<FutureTask<Double>>();
			List<double[][]> coordsList = new ArrayList<double[][]>();

			// create tasks
			for (int i = 0; i < threadCount; i++) {
				int start = i * CYCLE_COUNT * BEAM_COUNT / threadCount;
				int end = (i + 1) * CYCLE_COUNT * BEAM_COUNT / threadCount;

				double[][] subCoords = Arrays.copyOfRange(coords2D, start, end);
				coordsList.add(subCoords);

				GDALCallable callable = new GDALCallable(ct, subCoords);
				tasks.add(new FutureTask<Double>(callable));
			}

			// launch tasks
			for (int i = 0; i < threadCount; i++) {
				executor.execute(tasks.get(i));
			}
			try {
				// wait for tasks to finish
				for (int i = 0; i < threadCount; i++) {
					tasks.get(i).get();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

			executor.shutdown();
		}
		multithreadPerf.stop();
	}

	@Test
	public void testGDALBatchConversion() {
		CoordinateTransformation ct = getCoordinateTransformation();

		PerfoCounter batchPerf = new PerfoCounter(
				String.format("Batch for %d cycles, %d beams", CYCLE_COUNT, BEAM_COUNT));
		batchPerf.start();
		for (int n = 0; n < numberOfGeneration; n++) {
			// initialize();

			ct.TransformPoints(coords2D);
		}
		batchPerf.stop();
	}

	@Test
	public void testGDALNoBatchConversion() {
		CoordinateTransformation ct = getCoordinateTransformation();

		PerfoCounter batchPerf = new PerfoCounter(
				String.format("Batch for %d cycles, %d beams", CYCLE_COUNT, BEAM_COUNT));
		batchPerf.start();
		for (int n = 0; n < numberOfGeneration; n++) {
			// initialize();
			for (double[] element : coords2D) {
				ct.TransformPoint(element[0], element[1]);
			}
		}
		batchPerf.stop();
	}
}
