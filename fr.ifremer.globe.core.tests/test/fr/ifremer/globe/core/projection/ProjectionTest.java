package fr.ifremer.globe.core.projection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.gdal.osr.CoordinateTransformation;
import org.gdal.osr.SpatialReference;
import org.gdal.osr.osrConstants;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.gdal.GdalOsrUtils;

public class ProjectionTest {

	@Test
	public void testLambertProjection() throws ProjectionException {

		Projection projection = new Projection(StandardProjection.LAMBERT_EURO);

		double[] dstPoint = projection.project(-4, 47);
		Assert.assertEquals(2975727.86, dstPoint[0], 1e-2);
		Assert.assertEquals(2360031.53, dstPoint[1], 1e-2);

		double[] newPoint = projection.unproject(dstPoint[0], dstPoint[1]);
		Assert.assertEquals(-4, newPoint[0], 1e-13);
		Assert.assertEquals(47, newPoint[1], 1e-13);
	}

	@Test
	public void testMercatorProjection() throws ProjectionException {

		Projection projection = new Projection(StandardProjection.MERCATOR);

		// Expected values, same than :
		// "echo -4 47| proj +proj=merc"
		double[] dstPoint = projection.project(-4, 47);
		Assert.assertEquals(-445277.96, dstPoint[0], 1e-2);
		Assert.assertEquals(5910809.62, dstPoint[1], 1e-2);

		double[] newPoint = projection.unproject(dstPoint[0], dstPoint[1]);
		Assert.assertEquals(-4, newPoint[0], 1e-13);
		Assert.assertEquals(47, newPoint[1], 1e-13);
	}

	/**
	 * test "latitude of true scale" parameter (lat_ts) for mercator projection with
	 * fr.ifremer.globe.core.projection.Projection class
	 * 
	 * @throws ProjectionException
	 */
	@Test
	public void testNorthMercatorProjection() throws ProjectionException {

		Projection projection = Projection.createFromProj4String(
				"+ellps=WGS84 +k=1 +lat_ts=65 +lon_0=22 +no_defs +proj=merc +units=m +x_0=0 +y_0=0");

		// Expecting same result than
		// "echo 22 65.9019357979 | proj +lat_ts=65 +lon_0=22 +proj=merc"
		double[] dstPoint1 = projection.project(22, 65.9019357979);
		Assert.assertEquals(0.0, dstPoint1[0], 1e-2);
		Assert.assertEquals(4157758.51, dstPoint1[1], 1e-2);
		double[] dstPoint2 = projection.project(22, 65.7672067798);
		Assert.assertEquals(0.0, dstPoint2[0], 1e-2);
		Assert.assertEquals(4142250.00, dstPoint2[1], 1e-2);

	}

	/**
	 * test "latitude of true scale" parameter (lat_ts) for mercator projection with org.gdal.osr.SpatialReference and
	 * org.gdal.osr.CoordinateTransformation
	 */
	@Test
	public void testLongLatToMercator() {

		SpatialReference srs_proj4_longlat = new SpatialReference();
		srs_proj4_longlat.ImportFromProj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

		SpatialReference srs_mercator = new SpatialReference();
		srs_mercator
				.ImportFromProj4("+proj=merc +ellps=WGS84 +k=1 +lat_ts=65 +lon_0=22 +no_defs +units=m +x_0=0 +y_0=0");
		CoordinateTransformation ct_longlat_to_mercator = new CoordinateTransformation(srs_proj4_longlat, srs_mercator);

		// Expecting same result than
		// "echo 22 65.9019357979 | proj +lat_ts=65 +lon_0=22 +proj=merc"
		double[] northPoint = ct_longlat_to_mercator.TransformPoint(22, 65.9019357979);
		Assert.assertEquals(0.0, northPoint[0], 1e-2);
		Assert.assertEquals(4157758.51, northPoint[1], 1e-2);

		double[] southPoint = ct_longlat_to_mercator.TransformPoint(22, 65.7672067798);
		Assert.assertEquals(0.0, southPoint[0], 1e-2);
		Assert.assertEquals(4142250.00, southPoint[1], 1e-2);
	}

	@Test
	public void testMercatorToGeo() {
		SpatialReference srs_mercator = new SpatialReference();
		srs_mercator
				.ImportFromProj4("+proj=merc +ellps=WGS84 +k=1 +lat_ts=65 +lon_0=22 +no_defs +units=m +x_0=0 +y_0=0");

		SpatialReference srs_proj4_longlat = new SpatialReference();
		srs_proj4_longlat.ImportFromProj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
		assertFalse(GdalOsrUtils.isTraditionalGisOrder(srs_proj4_longlat));
		CoordinateTransformation ct_mercator_to_longlat = new CoordinateTransformation(srs_mercator, srs_proj4_longlat);
		double[] point = ct_mercator_to_longlat.TransformPoint(0d, 4142250d);
		assertFalse(GdalOsrUtils.isTransformationSwapCoords(srs_mercator, srs_proj4_longlat));
		Assert.assertEquals(22d, point[0], 1e-10);
		Assert.assertEquals(65.7672067684, point[1], 1e-10);

		SpatialReference srs_wkt_wgs84 = new SpatialReference(osrConstants.SRS_WKT_WGS84_LAT_LONG);
		assertTrue(GdalOsrUtils.isTraditionalGisOrder(srs_wkt_wgs84)); // Expecting latitude before longitude
		CoordinateTransformation ct_mercator_to_latlong = new CoordinateTransformation(srs_mercator, srs_wkt_wgs84);
		point = ct_mercator_to_latlong.TransformPoint(0d, 4142250d);
		assertTrue(GdalOsrUtils.isTransformationSwapCoords(srs_mercator, srs_wkt_wgs84));
		Assert.assertEquals(65.7672067684, point[0], 1e-10);
		Assert.assertEquals(22d, point[1], 1e-10);

		// ==> TransformPoint Swath coords when input SRS is projected and ouput SRS is in TraditionalGisOrder

	}

	@Test
	public void testLongLatProjection() throws ProjectionException {

		double latitude = 43.65296600714793;
		double longitude = -1.5735266032183568;

		// 4 differents Geographic SRS : +proj=longlat, +proj=latlong, WKT and EPSG 4326
		SpatialReference srs_proj4_longlat = new SpatialReference();
		srs_proj4_longlat.ImportFromProj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
		assertFalse(GdalOsrUtils.isTraditionalGisOrder(srs_proj4_longlat)); // Expecting longitude before latitude

		SpatialReference srs_proj4_latlong = new SpatialReference();
		srs_proj4_latlong.ImportFromProj4("+proj=latlong +datum=WGS84 +no_defs");
		// !! +proj=longlat and +proj=latlong are the same projection
		assertEquals(srs_proj4_longlat.ExportToWkt(), srs_proj4_latlong.ExportToWkt());
		// !! not considered as a traditional gis order projection
		assertFalse(GdalOsrUtils.isTraditionalGisOrder(srs_proj4_latlong));

		SpatialReference srs_wkt_wgs84 = new SpatialReference(osrConstants.SRS_WKT_WGS84_LAT_LONG);
		assertTrue(GdalOsrUtils.isTraditionalGisOrder(srs_wkt_wgs84)); // Expecting latitude before longitude

		SpatialReference srs_wkt_lonlat = new SpatialReference("""
						GEOGCS["unknown",
						    DATUM["WGS_1984",
						        SPHEROID["WGS 84",6378137,298.257223563,
						            AUTHORITY["EPSG","7030"]],
						        AUTHORITY["EPSG","6326"]],
						    PRIMEM["Greenwich",0,
						        AUTHORITY["EPSG","8901"]],
						    UNIT["degree",0.0174532925199433,
						        AUTHORITY["EPSG","9122"]],
						    AXIS["Longitude",EAST],
						    AXIS["Latitude",NORTH]]
				""");
		assertFalse(GdalOsrUtils.isTraditionalGisOrder(srs_wkt_lonlat)); // longitude before latitude

		SpatialReference srs_wkt_geogcrs = new SpatialReference("""
				GEOGCRS["WGS 84",
				    DATUM["World Geodetic System 1984",
				        ELLIPSOID["WGS 84",6378137,298.257223563,
				            LENGTHUNIT["metre",1]]],
				    PRIMEM["Greenwich",0,
				        ANGLEUNIT["degree",0.0174532925199433]],
				    CS[ellipsoidal,2],
				        AXIS["geodetic latitude (Lat)",north,
				            ORDER[1],
				            ANGLEUNIT["degree",0.0174532925199433]],
				        AXIS["geodetic longitude (Lon)",east,
				            ORDER[2],
				            ANGLEUNIT["degree",0.0174532925199433]],
				    ID["EPSG",4326]]				""");
		assertTrue(GdalOsrUtils.isTraditionalGisOrder(srs_wkt_geogcrs)); // longitude before latitude

		SpatialReference srs_4326 = new SpatialReference();
		srs_4326.ImportFromEPSG(4326);
		assertTrue(GdalOsrUtils.isTraditionalGisOrder(srs_4326)); // Expecting latitude before longitude

		var all_srs = List.of(srs_proj4_longlat, srs_proj4_latlong, srs_wkt_wgs84, srs_4326, srs_wkt_lonlat,
				srs_wkt_geogcrs);
		for (SpatialReference from_srs : all_srs) {
			for (SpatialReference to_srs : all_srs) {
				CoordinateTransformation ct = new CoordinateTransformation(from_srs, to_srs);
				double[] coords = ct.TransformPoint(longitude, latitude); // TransformPoint ask x and y in parameters...
				if (!GdalOsrUtils.isTransformationSwapCoords(from_srs,to_srs)) {
					Assert.assertEquals(longitude, coords[0], 1e-15);
					Assert.assertEquals(latitude, coords[1], 1e-15);
				} else {
					Assert.assertEquals(latitude, coords[0], 1e-15);
					Assert.assertEquals(longitude, coords[1], 1e-15);
				}
			}
		}

		// ==> TransformPoint Swath coords when in TraditionalGisOrder of the 2 srs are different
	}

	@Test
	public void testTranformBoundingBoxToWGS84() throws ProjectionException {

		double north = 43.65296600714793;
		double south = 43.55685424556554;
		double west = -1.5735266032183568;
		double east = -1.5546784416873744;

		// Try tranformBoundingBoxToWGS84 with a NON TraditionalGisOrder SRS
		SpatialReference srs_proj4_longlat = new SpatialReference();
		srs_proj4_longlat.ImportFromProj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
		assertFalse(GdalOsrUtils.isTraditionalGisOrder(srs_proj4_longlat)); // Expecting longitude before latitude
		double boundingBox[] = { north, south, east, west };
		GdalOsrUtils.tranformBoundingBoxToWGS84(srs_proj4_longlat, boundingBox);
		Assert.assertEquals(north, boundingBox[0], 1e-15);
		Assert.assertEquals(south, boundingBox[1], 1e-15);
		Assert.assertEquals(east, boundingBox[2], 1e-15);
		Assert.assertEquals(west, boundingBox[3], 1e-15);

		// Try tranformBoundingBoxToWGS84 with a TraditionalGisOrder SRS
		SpatialReference srs_4326 = new SpatialReference();
		srs_4326.ImportFromEPSG(4326);
		assertTrue(GdalOsrUtils.isTraditionalGisOrder(srs_4326));
		boundingBox = new double[] { north, south, east, west };
		GdalOsrUtils.tranformBoundingBoxToWGS84(srs_proj4_longlat, boundingBox);
		Assert.assertEquals(north, boundingBox[0], 1e-15);
		Assert.assertEquals(south, boundingBox[1], 1e-15);
		Assert.assertEquals(east, boundingBox[2], 1e-15);
		Assert.assertEquals(west, boundingBox[3], 1e-15);
	}
}
