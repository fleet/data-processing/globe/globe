package fr.ifremer.globe.core.navigation;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.nvi.info.HistoryConstants;
import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.navigation.utils.NavigationApplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

@SuppressWarnings("restriction")
public class ApplyNavigationTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplyNavigationTests.class);

	// input paths
	public static final String INPUT_FILE_PATH = GlobeTestUtil.getTestDataPath() + "/file/applynavigation";

	public static final String NVI_FILE_PATH = INPUT_FILE_PATH + "/0001_20150424_042219_Europe.nvi";
	public static final String NVI_WITH_EDITED_IMMERSION_FILE_PATH = INPUT_FILE_PATH
			+ "/0001_20150424_042219_Europe_with_modified_immersion.nvi";

	public static final String MBG_FILE_PATH = INPUT_FILE_PATH + "/0001_20150424_042219_Europe.mbg";
	public static final String MBG_FILE_PATH_REF = INPUT_FILE_PATH + "/0001_20150424_042219_Europe_Reference.mbg";

	public static final String XSF_FILE_PATH = INPUT_FILE_PATH + "/0001_20150424_042219_Europe.xsf.nc";
	public static final String XSF_FILE_PATH_REF = INPUT_FILE_PATH + "/0001_20150424_042219_Europe_ref.xsf.nc";

	public static final String XSF_FILE_PATH_REF_IMMERSION = INPUT_FILE_PATH
			+ "/0001_20150424_042219_Europe_ref_immersion.xsf.nc";

	// output paths
	public static final String OUTPUT_FILE_PATH = INPUT_FILE_PATH + "/generated";
	public static final String MBG_FILE_PATH_TEST = OUTPUT_FILE_PATH + "/mbgTest.mbg";
	public static final String XSF_FILE_PATH_TEST = OUTPUT_FILE_PATH + "/xsfTest.xsf.nc";
	public static final String XSF_FILE_PATH_TEST_IMMERSION = OUTPUT_FILE_PATH + "/xsfTestImmersion.xsf.nc";

	/** Clean output directory before tests */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		File outputDir = new File(OUTPUT_FILE_PATH);
		if (!outputDir.exists())
			outputDir.mkdirs();
		FileUtils.cleanDirectory(outputDir);
	}

	/**
	 * Tests {@link NavigationApplier} on MBG.
	 */
	@Test
	public void applyNavigationTestForMbg() throws GIOException, NCException, IOException {
		applyNavigationTest(MBG_FILE_PATH, MBG_FILE_PATH_TEST, MBG_FILE_PATH_REF, NVI_FILE_PATH,
				Optional.of(buildHookForMbg()), false);
	}

	/**
	 * Tests {@link NavigationApplier} on XSF.
	 */
	@Test
	public void applyNavigationTestForXsf() throws GIOException, NCException, IOException {
		applyNavigationTest(XSF_FILE_PATH, XSF_FILE_PATH_TEST, XSF_FILE_PATH_REF, NVI_FILE_PATH,
				Optional.of(buildHookForXsf()), false);
	}

	/**
	 * Tests {@link NavigationApplier} with a new immersion on XSF.
	 */
	@Test
	public void applyNavigationTestForXsfWithImmersion() throws GIOException, NCException, IOException {
		applyNavigationTest(XSF_FILE_PATH, XSF_FILE_PATH_TEST_IMMERSION, XSF_FILE_PATH_REF_IMMERSION,
				NVI_WITH_EDITED_IMMERSION_FILE_PATH, Optional.of(buildHookForXsf()), true);
	}

	/**
	 * Applies navigation on outFilePath and compares with refFilePath.
	 */
	private void applyNavigationTest(String inputFilePath, String testFilePath, String refFilePath,
			String navigationFilePath, Optional<NetcdfComparatorHook> hook, boolean applyImmersion)
			throws IOException, GIOException, NCException {
		// copy input to test file
		Files.copy(Paths.get(inputFilePath), Paths.get(testFilePath), StandardCopyOption.REPLACE_EXISTING);

		ISounderNcInfo testFile = (ISounderNcInfo) IFileService.grab().getFileInfo(testFilePath)
				.orElseThrow(() -> new GIOException("File not available " + testFilePath));
		INavigationDataSupplier navigation = INavigationService.grab().getNavigationDataProvider(navigationFilePath)
				.orElseThrow(() -> new GIOException("File not available " + navigationFilePath));

		// apply navigation
		new NavigationApplier().apply(testFile, navigation, applyImmersion);

		StringBuilder resume = new StringBuilder("Result : \n");
		boolean testFailed = false;

		Optional<String> errors = NetcdfComparator.compareFiles(refFilePath, testFilePath, hook);
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(refFilePath + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(testFilePath + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(resume.toString(), testFailed);
	}

	private NetcdfComparatorHook buildHookForMbg() {
		NetcdfComparatorHook result = new NetcdfComparatorHook();
		result.ignoreGlobalAttribute(NviConstants.Name);
		result.ignoreGlobalAttribute(NviConstants.NbrHistoryRec);
		result.ignoreGlobalAttribute(NviConstants.NbrHistoryRec);
		result.ignoreGlobalAttribute(NviConstants.NorthLatitude);
		result.ignoreGlobalAttribute(NviConstants.SouthLatitude);
		result.ignoreGlobalAttribute(NviConstants.EastLongitude);
		result.ignoreGlobalAttribute(NviConstants.WestLongitude);
		result.ignoreGlobalAttribute(MbgConstants.MinDepth);
		result.ignoreGlobalAttribute(MbgConstants.MaxDepth);
		result.setToleranceForAttribute("mbEllipsoidE2", 1.12E-16);
		result.setToleranceForAttribute("mbEllipsoidInvF", 1.12E-16);
		result.ignoreVariable(HistoryConstants.DATE_VALUE);
		result.ignoreVariable(HistoryConstants.TIME_VALUE);
		result.ignoreVariable(HistoryConstants.AUTOR_VALUE);
		result.ignoreVariable(HistoryConstants.COMMENT_VALUE);
		result.ignoreVariable(HistoryConstants.AUTOR_VALUE);
		result.ignoreVariable(HistoryConstants.MODULE_VALUE);
		result.ignoreVariable(MbgConstants.HistCode);
		// Later introduction (MBG for Globe version > 2.3.4)
		result.ignoreGlobalAttribute("mbBiasCorrectionRef");
		return result;
	}

	private NetcdfComparatorHook buildHookForXsf() {
		NetcdfComparatorHook result = new NetcdfComparatorHook();
		// variables not readable by comparator
		result.ignoreVariable("seabed_image_samples_r");
		result.ignoreVariable("water_level");
		result.ignoreVariable("sample_count");

		// TODO update ref files with processing_status attribute
		result.ignoreGlobalAttribute("date_created");
		result.ignoreAttribute("/Provenance", "history");
		return result;
	}
}
