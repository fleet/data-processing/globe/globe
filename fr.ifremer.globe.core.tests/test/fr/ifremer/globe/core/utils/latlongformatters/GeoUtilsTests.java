/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.utils.latlongformatters;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoUtils;
import fr.ifremer.globe.core.utils.latlon.DegMinSecFormatter;

/**
 * Test class for GeoUtils
 *
 */
public class GeoUtilsTests {

	@Test
	public void test() {
		DegMinSecFormatter fmt = DegMinSecFormatter.getInstance();
		GeoBox geobox = new GeoBox(//
				fmt.parseLatitude("N 46°57'51''"), //
				fmt.parseLatitude("N 46°15'29''"), //
				fmt.parseLongitude("W 004°22'44''"), //
				fmt.parseLongitude("W 005°34'14''") //
		);
		geobox = GeoUtils.realign(geobox, 1.0 / 60.0);
		assertEquals("N 46°58'00''", fmt.formatLatitude(geobox.getTop()));
		assertEquals("N 46°15'00''", fmt.formatLatitude(geobox.getBottom()));
		assertEquals("W 004°22'00''", fmt.formatLongitude(geobox.getRight()));
		assertEquals("W 005°35'00''", fmt.formatLongitude(geobox.getLeft()));

		// Already rounded. Geobox must be the same
		geobox = GeoUtils.realign(geobox, 1.0 / 60.0);
		assertEquals("N 46°58'00''", fmt.formatLatitude(geobox.getTop()));
		assertEquals("N 46°15'00''", fmt.formatLatitude(geobox.getBottom()));
		assertEquals("W 004°22'00''", fmt.formatLongitude(geobox.getRight()));
		assertEquals("W 005°35'00''", fmt.formatLongitude(geobox.getLeft()));

		geobox = new GeoBox(//
				fmt.parseLatitude("S 46°15'29''"), //
				fmt.parseLatitude("S 46°57'51''"), //
				fmt.parseLongitude("E 005°34'14''"), //
				fmt.parseLongitude("E 004°22'44''") //
		);
		geobox = GeoUtils.realign(geobox, 1.0 / 60.0);
		assertEquals("S 46°15'00''", fmt.formatLatitude(geobox.getTop()));
		assertEquals("S 46°58'00''", fmt.formatLatitude(geobox.getBottom()));
		assertEquals("E 005°35'00''", fmt.formatLongitude(geobox.getRight()));
		assertEquals("E 004°22'00''", fmt.formatLongitude(geobox.getLeft()));

		geobox = new GeoBox(//
				43.75000013994674, // N 43°45'00''
				41.06666666666667, // N 41°04'00''
				8.23333355151117, // E 008°14'00''
				4.05 // E 004°03'00''
		);
		geobox = GeoUtils.realign(geobox, 1.0 / 60.0);
		assertEquals("N 43°45'00''", fmt.formatLatitude(geobox.getTop()));
		assertEquals("N 41°04'00''", fmt.formatLatitude(geobox.getBottom()));
		assertEquals("E 008°14'00''", fmt.formatLongitude(geobox.getRight()));
		assertEquals("E 004°03'00''", fmt.formatLongitude(geobox.getLeft()));
	}

}
