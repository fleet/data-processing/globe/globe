/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.utils.latlongformatters;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ifremer.globe.core.utils.latlon.DegMinSecFormatter;

/**
 * Test class for GeoUtils
 *
 */
public class DegMinSecFormatterTests {

	@Test
	public void testLongitude() {
		DegMinSecFormatter fmt = DegMinSecFormatter.getInstance();
		{
			double longitude = 8.23611111111111;
			String formattedValue = fmt.formatLongitude(longitude);
			assertEquals("E 008°14'10''", formattedValue);

			double parsedLongitude = fmt.parseLongitude(formattedValue);
			formattedValue = fmt.formatLongitude(parsedLongitude);
			assertEquals("E 008°14'10''", formattedValue);
		}
	}

	@Test
	public void testLatitude() {
		DegMinSecFormatter fmt = DegMinSecFormatter.getInstance();
		{
			double latitude = 43.75000013994674;
			String formattedValue = fmt.formatLatitude(latitude);
			assertEquals("N 43°45'00''", formattedValue);

			double parsedLatitude = fmt.parseLatitude(formattedValue);
			formattedValue = fmt.formatLatitude(parsedLatitude);
			assertEquals("N 43°45'00''", formattedValue);
		}
	}
}
