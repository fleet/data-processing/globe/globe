package fr.ifremer.globe.core.utils;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.utils.latlon.CoordinateDisplayType;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;

/**
 * Class test for {@link LatLongFormater}
 */
public class TestFormatLatitudeLongitude {

	@Test
	public void testLatLongFormatter() {
		double value;
		String str;

		// latitudes
		value = -4.586464;
		str = LatLongFormater.latitudeToString(value, CoordinateDisplayType.DECIMAL);
		Assert.assertEquals("-4.5864640°", str);
		str = LatLongFormater.latitudeToString(value, CoordinateDisplayType.DEG_MIN_DEC);
		Assert.assertEquals("S 04 35.188", str);
		str = LatLongFormater.latitudeToString(value, CoordinateDisplayType.DEG_MIN_SEC);
		Assert.assertEquals("S 04°35'11''", str);

		value = 50.61379;
		str = LatLongFormater.latitudeToString(value, CoordinateDisplayType.DECIMAL);
		Assert.assertEquals("50.6137900°", str);
		str = LatLongFormater.latitudeToString(value, CoordinateDisplayType.DEG_MIN_DEC);
		Assert.assertEquals("N 50 36.827", str);
		str = LatLongFormater.latitudeToString(value, CoordinateDisplayType.DEG_MIN_SEC);
		Assert.assertEquals("N 50°36'50''", str);

		// longitudes
		value = -50.654;
		str = LatLongFormater.longitudeToString(value, CoordinateDisplayType.DECIMAL);
		Assert.assertEquals("-50.6540000°", str);
		str = LatLongFormater.longitudeToString(value, CoordinateDisplayType.DEG_MIN_DEC);
		Assert.assertEquals("W 050 39.240", str);
		str = LatLongFormater.longitudeToString(value, CoordinateDisplayType.DEG_MIN_SEC);
		Assert.assertEquals("W 050°39'14''", str);

		value = 2.20356;
		str = LatLongFormater.longitudeToString(value, CoordinateDisplayType.DECIMAL);
		Assert.assertEquals("2.2035600°", str);
		str = LatLongFormater.longitudeToString(value, CoordinateDisplayType.DEG_MIN_DEC);
		Assert.assertEquals("E 002 12.214", str);
		str = LatLongFormater.longitudeToString(value, CoordinateDisplayType.DEG_MIN_SEC);
		Assert.assertEquals("E 002°12'13''", str);

	}

}
