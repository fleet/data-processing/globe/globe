/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.gson;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.Gson;

/**
 * Tests serialization of a Record
 */
public class TestRecord {

	/** Record to serialize */
	static public record GsonRecord(int anInt, String aString) {
	}

	@Test
	public void testGsonRecord() {
		var record1 = new GsonRecord(12, "StringValue");
		String serialization = new Gson().toJson(record1);
		Assert.assertTrue(!serialization.isEmpty());

		var record2 = new Gson().fromJson(serialization, GsonRecord.class);
		Assert.assertTrue(record2 instanceof GsonRecord);
		Assert.assertEquals(12, record2.anInt());
		Assert.assertEquals("StringValue", record2.aString());
	}

}
