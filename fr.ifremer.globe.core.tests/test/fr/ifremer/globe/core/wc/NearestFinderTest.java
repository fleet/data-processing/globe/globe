package fr.ifremer.globe.core.wc;


import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.wc.NearestFinder;
import fr.ifremer.globe.utils.exception.GIOException;

public class NearestFinderTest {

	@Test
	public void testNearestPointSearch() throws GIOException
	{
		class MyValue{
			double v;
			public double get() { return v;};
			public MyValue(double value) {
				this.v=value;
			}
		}
		NearestFinder<MyValue> finder=new NearestFinder<>(MyValue::get);
		//treat case with one value
		finder.add(new MyValue(1));
		MyValue found=finder.findNearest(new MyValue(1.5));
		Assert.assertNotNull(found);
		Assert.assertEquals(1,found.get(),0);
		found=finder.findNearest(new MyValue(0.5));
		Assert.assertNotNull(found);
		Assert.assertEquals(1,found.get(),0);
		//check with two values
		finder.add(new MyValue(300));
		finder.add(new MyValue(600));
		finder.add(new MyValue(900));
		found=finder.findNearest(new MyValue(0.5));
		Assert.assertEquals(1,found.get(),0);
		found=finder.findNearest(new MyValue(1000));
		Assert.assertEquals(900,found.get(),0);
		found=finder.findNearest(new MyValue(700));
		Assert.assertEquals(600,found.get(),0);
		found=finder.findNearest(new MyValue(300));
		Assert.assertEquals(300,found.get(),0);
		found=finder.findNearest(new MyValue(10));
		Assert.assertEquals(1,found.get(),0);
		
	}
}
