package fr.ifremer.globe.core.wc;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.wc.Manageable;
import fr.ifremer.globe.core.model.wc.SlidingBuffer;

public class SlidingBufferTest {

	class Value implements Manageable
	{
		private int value;
		Value(int v)
		{
			this.value=v;
		}
		@Override
		public void close() {
		}
		
	}
	public void testSingle()
	{
		SlidingBuffer<Value> myBuffer=SlidingBuffer.buildSingle(i->new Value(i),100);
		myBuffer.setCurrent(5);
		myBuffer.setCurrent(6);
		myBuffer.setCurrent(7);
		//check that we have only one value, and this value is 7
		Assert.assertEquals(1,myBuffer.values().size());
		myBuffer.values().forEach(v->Assert.assertEquals(v.value, 7));
		myBuffer.setCurrent(50);
		Assert.assertEquals(1,myBuffer.values().size());
		myBuffer.values().forEach(v->Assert.assertEquals(v.value, 50));

	}
	@Test
	public void testMultiple()
	{
		SlidingBuffer<Value> myBuffer=SlidingBuffer.build(i->new Value(i), 2, 3, 10);
		myBuffer.setCurrent(0);

		//check that we have 0 + 5 values after
		Assert.assertEquals(6,myBuffer.values().size());
		Value [] values=myBuffer.values().toArray(new Value[myBuffer.values().size()]);
		Assert.assertEquals(0,values[0].value);
		Assert.assertEquals(1,values[1].value);
		Assert.assertEquals(2,values[2].value);
		Assert.assertEquals(3,values[3].value);
		Assert.assertEquals(4,values[4].value);
		Assert.assertEquals(5,values[5].value);
		//check that we have 4 values before, one after around 9
		myBuffer.setCurrent(9);
		Assert.assertEquals(6,myBuffer.values().size());
		values=myBuffer.values().toArray(new Value[myBuffer.values().size()]);
		Assert.assertEquals(5,values[0].value);
		Assert.assertEquals(6,values[1].value);
		Assert.assertEquals(7,values[2].value);
		Assert.assertEquals(8,values[3].value);
		Assert.assertEquals(9,values[4].value);
		Assert.assertEquals(10,values[5].value);
		
		myBuffer.setCurrent(5);
		Assert.assertEquals(6,myBuffer.values().size());
	}
	class ValueDisposeable implements Manageable
	{
		boolean dispose=false;
		
		@Override
		public void close() {
			
			dispose=true;
		}
	}
	@Test
	public void testDisposeCall()
	{
		SlidingBuffer<ValueDisposeable> myBuffer=SlidingBuffer.buildSingle(i->new ValueDisposeable(),100);
		myBuffer.setCurrent(5);
		//retrieve first value
		Optional<ValueDisposeable> value=myBuffer.values().stream().findFirst();
		Assert.assertTrue(value.isPresent());
		//change index
		myBuffer.setCurrent(0);
		//check that first value was disposed
		Assert.assertTrue(value.get().dispose);

	}

}
