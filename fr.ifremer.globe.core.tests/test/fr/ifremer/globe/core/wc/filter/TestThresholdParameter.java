package fr.ifremer.globe.core.wc.filter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.thoughtworks.xstream.XStream;

import fr.ifremer.globe.core.model.wc.filters.ThresholdParameter;


public class TestThresholdParameter {
	@Rule
	public TemporaryFolder folder= new TemporaryFolder();

	/**
	 * @throws IOException 
	 * @throws ClassNotFoundException */
	@Test
	public void testSerialization() throws IOException, ClassNotFoundException
	{
		File output= folder.newFile("myfile.xml");
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.ignoreUnknownElements();
		ObjectOutputStream out = xstream.createObjectOutputStream(new FileWriter(output), "PARAM");
		ThresholdParameter param=new ThresholdParameter(10, 20,false);
		out.writeObject(param);
		out.close();
		//read the file
		xstream.setClassLoader(param.getClass().getClassLoader());
		ObjectInputStream in = xstream.createObjectInputStream(new FileReader(output));
		ThresholdParameter readParameters=(ThresholdParameter)in.readObject();
		Assert.assertTrue(param.getMinValue()==readParameters.getMinValue());
		Assert.assertTrue(param.getMaxValue()==readParameters.getMaxValue());

	}
}
