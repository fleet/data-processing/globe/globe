package fr.ifremer.globe.core.io.vel.export;

import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityService;
import fr.ifremer.globe.core.model.velocity.IVelWriter;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class VelRegressionTests extends VelExportTests {

	private static final String OUTPUT_FILE_PATH = GlobeTestUtil.getTestDataPath() + "/file/vel/regression/generated";

	private static final String S7K_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/vel/regression/20071213_170635_PP-7150.s7k";
	private static final String ALL_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/vel/regression/0210_20201010_065008_EM122_Marion_Dufresne.all";
	private static final String KMALL_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/vel/regression/0013_20180908_154822.kmall";

	private static final String S7K_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/vel/regression/20071213_170635_PP-7150_fromS7K.vel";
	private static final String ALL_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/vel/regression/0210_20201010_065008_EM122_Marion_Dufresne_fromALL.vel";
	private static final String KMALL_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/vel/regression/0013_20180908_154822_fromKMALL.vel";

	private static final String VEL_FILE_PATH_S7K = OUTPUT_FILE_PATH + "/generated_fromS7K.vel";
	private static final String VEL_FILE_PATH_ALL = OUTPUT_FILE_PATH + "/generated_fromALL.vel";
	private static final String VEL_FILE_PATH_KMALL = OUTPUT_FILE_PATH + "/generated_fromKMALL.vel";

	/** Clean output directory before tests */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		clearDirectory(OUTPUT_FILE_PATH);
	}

	@Test
	public void lookRegressionsForS7K() throws IOException, GException {
		Optional<ISoundVelocityData> src = ISoundVelocityService.grab().getSoundVelocityData(S7K_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		ISoundVelocityData velData = src.get();
		IVelWriter.grab().write(velData, VEL_FILE_PATH_S7K);
		compareFiles(S7K_FILE_PATH_REFERENCE, VEL_FILE_PATH_S7K);
	}

	@Test
	public void lookRegressionsForALL() throws IOException, GException {
		Optional<ISoundVelocityData> src = ISoundVelocityService.grab().getSoundVelocityData(ALL_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		ISoundVelocityData velData = src.get();
		IVelWriter.grab().write(velData, VEL_FILE_PATH_ALL);
		compareFiles(ALL_FILE_PATH_REFERENCE, VEL_FILE_PATH_ALL);
	}

	@Test
	public void lookRegressionsForKMALL() throws IOException, GException {
		Optional<ISoundVelocityData> src = ISoundVelocityService.grab().getSoundVelocityData(KMALL_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		ISoundVelocityData velData = src.get();
		IVelWriter.grab().write(velData, VEL_FILE_PATH_KMALL);
		compareFiles(KMALL_FILE_PATH_REFERENCE, VEL_FILE_PATH_KMALL);
	}

}
