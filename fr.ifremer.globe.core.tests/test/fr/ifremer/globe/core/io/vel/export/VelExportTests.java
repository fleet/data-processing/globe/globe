package fr.ifremer.globe.core.io.vel.export;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.vel.info.HistoryConstants;
import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.exception.GException;

public abstract class VelExportTests {
	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(VelExportTests.class);

	/** Clean output directory before tests */
	protected static void clearDirectory(String path) throws IOException {
		File outputDir = new File(path);
		if (!outputDir.exists())
			outputDir.mkdirs();
		FileUtils.cleanDirectory(outputDir);
	}

	/**
	 * Compare two Netcdf files
	 *
	 * @param refFilePath reference file path
	 * @param testFilePath test result file path
	 * @throws IOException 
	 * @throws GException 
	 */
	protected void compareFiles(String refFilePath, String testFilePath) throws GException, IOException {
		
		StringBuilder resume = new StringBuilder("VelExport result : \n");
		boolean testFailed = false;
		
		// Check if  file exists
		
		Assert.assertTrue(new File (refFilePath).exists());
		
		Assert.assertTrue(new File(testFilePath).exists());		

		NetcdfComparatorHook hook = buildHook();
		Optional<String> errors = NetcdfComparator.compareFiles(refFilePath, testFilePath, Optional.of(hook));
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(testFilePath + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(testFilePath + " : OK \n");
		}
		
		LOGGER.debug(resume.toString());
		Assert.assertFalse(resume.toString(), testFailed);
	}

	/**
	 * Defines attributes and variables to ignore
	 */
	protected NetcdfComparatorHook buildHook() {
		NetcdfComparatorHook result = new NetcdfComparatorHook();

		// Name and history field are obviously different
		result.ignoreGlobalAttribute(VelocityConstants.Name);

		result.ignoreVariable(HistoryConstants.DATE_VALUE);
		result.ignoreVariable(HistoryConstants.TIME_VALUE);
		result.ignoreVariable(HistoryConstants.AUTOR_VALUE);
		result.ignoreVariable(HistoryConstants.COMMENT_VALUE);
		result.ignoreVariable(HistoryConstants.MODULE_VALUE);
		result.ignoreVariable(HistoryConstants.CODE_VALUE);

		return result;
	}

}
