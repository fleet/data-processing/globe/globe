package fr.ifremer.globe.core.io.nvi.export;

import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;


public class S7kToNviExportTests extends NviExportTests {

	private static final String OUTPUT_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/generated/fromS7k";

	private static final String S7K_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/20150902_041108_PP_7150_24kHz.s7k";

	private static final String S7K_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/Tous_From_S7k_20150902_041108_PP_7150_24kHz.nvi";
	private static final String S7K_FILE_PATH_SOUNDINGS_SAMPLING_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/1sur15_From_S7k_20150902_041108_PP_7150_24kHz.nvi";
	private static final String S7K_FILE_PATH_TIME_SAMPLING_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/1sur8sec_From_S7k_20150902_041108_PP_7150_24kHz.nvi";

	private static final String NVI_FILE_PATH_S7K = OUTPUT_FILE_PATH + "/generated_fromS7K.nvi";
	private static final String NVI_FILE_PATH_S7K_SOUNDINGS_SAMPLING = OUTPUT_FILE_PATH
			+ "/generated_fromS7K_soundingsSampling.nvi";
	private static final String NVI_FILE_PATH_S7K_TIME_SAMPLING = OUTPUT_FILE_PATH
			+ "/generated_fromS7K_timeSampling.nvi";

	/** Clean output directory before tests */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		clearDirectory(OUTPUT_FILE_PATH);
	}

	@Test
	public void lookRegressionForS7K() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(S7K_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_FILE_PATH_S7K, SamplingMode.NONE, 0);
		compareFiles(S7K_FILE_PATH_REFERENCE, NVI_FILE_PATH_S7K);
	}

	@Test
	public void lookRegressionForS7KWithSoundingsSampling() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(S7K_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_FILE_PATH_S7K_SOUNDINGS_SAMPLING, SamplingMode.SOUNDINGS, 15);
		compareFiles(S7K_FILE_PATH_SOUNDINGS_SAMPLING_REFERENCE, NVI_FILE_PATH_S7K_SOUNDINGS_SAMPLING);
	}

	@Test
	public void lookRegressionForS7KWithTimeSampling() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(S7K_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_FILE_PATH_S7K_TIME_SAMPLING, SamplingMode.TIME, 8);
		compareFiles(S7K_FILE_PATH_TIME_SAMPLING_REFERENCE, NVI_FILE_PATH_S7K_TIME_SAMPLING);
	}
}