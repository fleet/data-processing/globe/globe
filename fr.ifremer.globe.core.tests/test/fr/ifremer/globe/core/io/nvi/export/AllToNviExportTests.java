package fr.ifremer.globe.core.io.nvi.export;

import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class AllToNviExportTests extends NviExportTests {

	private static final String OUTPUT_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/generated/fromAll";

	private static final String ALL_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/0001_20150424_042219_Europe.all";

	private static final String ALL_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/Tous_From_All_0001_20150424_042219_Europe.nvi";
	private static final String ALL_FILE_PATH_SOUNDINGS_SAMPLING_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/1sur5_From_All_0001_20150424_042219_Europe.nvi";
	private static final String ALL_FILE_PATH_TIME_SAMPLING_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/export/1sur10sec_From_All__0001_20150424_042219_Europe.nvi";

	private static final String NVI_FILE_PATH_ALL = OUTPUT_FILE_PATH + "/generated_fromALL.nvi";
	private static final String NVI_FILE_PATH_ALL_SOUNDINGS_SAMPLING = OUTPUT_FILE_PATH
			+ "/generated_fromALL_soundingsSampling.nvi";
	private static final String NVI_FILE_PATH_ALL_TIME_SAMPLING = OUTPUT_FILE_PATH
			+ "/generated_fromALL_timeSampling.nvi";

	/** Clean output directory before tests */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		clearDirectory(OUTPUT_FILE_PATH);
	}

	@Test
	public void lookRegressionForALL() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(ALL_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_FILE_PATH_ALL, SamplingMode.NONE, 0);
		compareFiles(ALL_FILE_PATH_REFERENCE, NVI_FILE_PATH_ALL);

	}

	@Test
	public void lookRegressionForALLWithSoundingsSampling() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(ALL_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_FILE_PATH_ALL_SOUNDINGS_SAMPLING, SamplingMode.SOUNDINGS, 5);
		compareFiles(ALL_FILE_PATH_SOUNDINGS_SAMPLING_REFERENCE, NVI_FILE_PATH_ALL_SOUNDINGS_SAMPLING);
	}

	@Test
	public void lookRegressionForALLWithTimeSampling() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(ALL_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_FILE_PATH_ALL_TIME_SAMPLING, SamplingMode.TIME, 10);
		compareFiles(ALL_FILE_PATH_TIME_SAMPLING_REFERENCE, NVI_FILE_PATH_ALL_TIME_SAMPLING);
	}
}