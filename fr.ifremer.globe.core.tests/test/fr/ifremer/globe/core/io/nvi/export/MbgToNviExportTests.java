package fr.ifremer.globe.core.io.nvi.export;

import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class MbgToNviExportTests extends NviExportTests {

	private static final String BASE_FILE_PATH = GlobeTestUtil.getTestDataPath() + "/file/nvi/export/mbg";
	private static final String OUTPUT_FILE_PATH = BASE_FILE_PATH + "/generated";

	// File 1
	private static final String MBG_FILE_PATH = BASE_FILE_PATH + "/0001_20150424_042219_Europe.mbg";

	private static final String NVI_REFERENCE = BASE_FILE_PATH + "/tous_0001_20150424_042219_Europe.nvi";
	private static final String NVI_SOUNDINGS_SAMPLING_REFERENCE = BASE_FILE_PATH
			+ "/1sur5pts_0001_20150424_042219_Europe.nvi";
	private static final String NVI_TIME_SAMPLING_REFERENCE = BASE_FILE_PATH
			+ "/1sur10sec_0001_20150424_042219_Europe.nvi";

	private static final String NVI_TEST = OUTPUT_FILE_PATH + "/tous_0001_20150424_042219_Europe.nvi";
	private static final String NVI_SOUNDINGS_SAMPLING_TEST = OUTPUT_FILE_PATH
			+ "/1sur5pts_0001_20150424_042219_Europe.nvi";
	private static final String NVI_TIME_SAMPLING_TEST = OUTPUT_FILE_PATH
			+ "/1sur10sec_0001_20150424_042219_Europe.nvi";

	// File 2
	private static final String MBG_FILE_PATH_2 = BASE_FILE_PATH + "/0202_20090629_170203.mbg";

	private static final String NVI_REFERENCE_2 = BASE_FILE_PATH + "/0202_20090629_170203_Tous_From_MBG.nvi";
	private static final String NVI_SOUNDINGS_SAMPLING_REFERENCE_2 = BASE_FILE_PATH
			+ "/0202_20090629_170203_1sur5_From_MBG.nvi";
	private static final String NVI_TIME_SAMPLING_REFERENCE_2 = BASE_FILE_PATH
			+ "/0202_20090629_170203_1sur10sec_From_MBG.nvi";

	private static final String NVI_TEST_2 = OUTPUT_FILE_PATH + "/0202_20090629_170203_Tous_From_MBG.nvi";
	private static final String NVI_SOUNDINGS_SAMPLING_TEST_2 = OUTPUT_FILE_PATH
			+ "/0202_20090629_170203_1sur5_From_MBG.nvi";
	private static final String NVI_TIME_SAMPLING_TEST_2 = OUTPUT_FILE_PATH
			+ "/0202_20090629_170203_1sur10sec_From_MBG.nvi";

	/** Clean output directory before tests */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		clearDirectory(OUTPUT_FILE_PATH);
	}

	//// FILE 1

	@Test
	public void lookRegressionForMBG() throws IOException, GException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(MBG_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		try (INavigationData navData = src.get().getNavigationData(true)) {
			INviWriter.grab().write(navData, NVI_TEST);
		}
		compareFiles(NVI_REFERENCE, NVI_TEST);
	}

	@Test
	public void lookRegressionForMBGWithSoundingsSampling() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(MBG_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_SOUNDINGS_SAMPLING_TEST, SamplingMode.SOUNDINGS, 5);
		compareFiles(NVI_SOUNDINGS_SAMPLING_REFERENCE, NVI_SOUNDINGS_SAMPLING_TEST);
	}

	@Test
	public void lookRegressionForMBGWithTimeSampling() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(MBG_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_TIME_SAMPLING_TEST, SamplingMode.TIME, 10);
		compareFiles(NVI_TIME_SAMPLING_REFERENCE, NVI_TIME_SAMPLING_TEST);
	}

	//// FILE 2

	@Test
	public void lookRegressionForMBG2() throws IOException, GException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(MBG_FILE_PATH_2);
		Assert.assertTrue(src.isPresent());
		try (INavigationData navData = src.get().getNavigationData(true)) {
			INviWriter.grab().write(navData, NVI_TEST_2);
		}
		compareFiles(NVI_REFERENCE_2, NVI_TEST_2);
	}

	@Test
	public void lookRegressionForMBGWithSoundingsSampling2() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(MBG_FILE_PATH_2);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_SOUNDINGS_SAMPLING_TEST_2, SamplingMode.SOUNDINGS, 5);
		compareFiles(NVI_SOUNDINGS_SAMPLING_REFERENCE_2, NVI_SOUNDINGS_SAMPLING_TEST_2);
	}

	@Test
	public void lookRegressionForMBGWithTimeSampling2() throws GException, IOException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(MBG_FILE_PATH_2);
		Assert.assertTrue(src.isPresent());
		INviWriter.grab().write(src.get(), NVI_TIME_SAMPLING_TEST_2, SamplingMode.TIME, 10);
		compareFiles(NVI_TIME_SAMPLING_REFERENCE_2, NVI_TIME_SAMPLING_TEST_2);
	}

}