package fr.ifremer.globe.core.io.nvi.export;

import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class NviRegressionTests extends NviExportTests {

	private static final String OUTPUT_FILE_PATH = GlobeTestUtil.getTestDataPath() + "/file/nvi/regression/generated";

	private static final String S7K_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/regression/20150902_041108_PP_7150_24kHz.s7k";
	private static final String ALL_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/regression/0001_20150424_042219_Europe.all";
	private static final String MBG_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/regression/0001_20150424_042219_Europe.mbg";

	private static final String S7K_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/regression/20150902_041108_PP_7150_24kHz_fromS7K.nvi";
	private static final String ALL_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/regression/0001_20150424_042219_Europe_fromALL.nvi";
	private static final String MBG_FILE_PATH_REFERENCE = GlobeTestUtil.getTestDataPath()
			+ "/file/nvi/regression/0001_20150424_042219_Europe_fromMBG.nvi";

	private static final String NVI_FILE_PATH_S7K = OUTPUT_FILE_PATH + "/generated_fromS7K.nvi";
	private static final String NVI_FILE_PATH_ALL = OUTPUT_FILE_PATH + "/generated_fromALL.nvi";
	private static final String NVI_FILE_PATH_MBG = OUTPUT_FILE_PATH + "/generated_fromMBG.nvi";

	/** Clean output directory before tests */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		clearDirectory(OUTPUT_FILE_PATH);
	}

	@Test
	public void lookRegressionsForS7K() throws IOException, GException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(S7K_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		try (INavigationData navData = src.get().getNavigationData(true)) {
			INviWriter.grab().write(navData, NVI_FILE_PATH_S7K);
		}
		compareFiles(S7K_FILE_PATH_REFERENCE, NVI_FILE_PATH_S7K);
	}

	@Test
	public void lookRegressionsForALL() throws IOException, GException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(ALL_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		try (INavigationData navData = src.get().getNavigationData(true)) {
			INviWriter.grab().write(navData, NVI_FILE_PATH_ALL);
		}
		compareFiles(ALL_FILE_PATH_REFERENCE, NVI_FILE_PATH_ALL);
	}

	@Test
	public void lookRegressionsForMBG() throws IOException, GException {
		Optional<INavigationDataSupplier> src = INavigationService.grab().getNavigationDataProvider(MBG_FILE_PATH);
		Assert.assertTrue(src.isPresent());
		try (INavigationData navData = src.get().getNavigationData(true)) {
			INviWriter.grab().write(navData, NVI_FILE_PATH_MBG);
		}
		compareFiles(MBG_FILE_PATH_REFERENCE, NVI_FILE_PATH_MBG);
	}
}
