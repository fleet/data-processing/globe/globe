/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.filtri;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.filtri.FilTriProcess;
import fr.ifremer.globe.core.processes.filtri.FilTriProcess.FilTriProcessResult;
import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * FilTri tests.
 */
@FixMethodOrder(MethodSorters.JVM)
public class MbgFiltTriTest {
	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(MbgFiltTriTest.class);

	/** Répertoire contenant les fichiers de référence. */
	public static final String REFERENCE_FOLDER = GlobeTestUtil.getTestDataPath() + "/file/FiltTri/REFGlobe1.6.2";

	/** Fichier à tester. */
	public static final String MBG_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/CARAIBES/20130322_204233_PP-7150-24kHz-CIB.mbg";
	/** Fichier à tester. */
	public static final String MBG_FILE2 = GlobeTestUtil.getTestDataPath()
			+ "/file/Kongsberg/CARAIBES/0184_20150530_120935_thalia-CIB.mbg";
	/** Temporary folder. */
	protected String tempDir;

	/**
	 * Test de FiltTri, OPTION_NORMAL.
	 */
	@Test
	public void testFiltTriNormal1() throws Exception {
		FiltTriParameters params = getNormalFiltTriParameters();
		testConversion(MBG_FILE1, params, 70, 937);
	}

	/**
	 * Test de FiltTri, OPTION_HEIGHT.
	 */
	@Test
	public void testFiltTriHeight1() throws Exception {
		FiltTriParameters params = getHeightFiltTriParameters();
		testConversion(MBG_FILE1, params, 35, 937);
	}

	/**
	 * Test de FiltTri, NEIGHBOUR.
	 */
	@Test
	public void testFiltTriNeighbour1() throws Exception {
		FiltTriParameters params = getNeighbourFiltTriParameters();
		testConversion(MBG_FILE1, params, 34, 937);
	}

	/**
	 * Test de FiltTri, OPTION_NORMAL.
	 */
	@Test
	public void testFiltTriNormal2() throws Exception {
		FiltTriParameters params = getNormalFiltTriParameters();
		testConversion(MBG_FILE2, params, 2987, 79196);
	}

	/**
	 * Test de FiltTri, OPTION_HEIGHT.
	 */
	@Test
	public void testFiltTriHeight2() throws Exception {
		FiltTriParameters params = getHeightFiltTriParameters();
		testConversion(MBG_FILE2, params, 2935, 79196);
	}

	/**
	 * Test de FiltTri, NEIGHBOUR.
	 */
	@Test
	public void testFiltTriNeighbour2() throws Exception {
		FiltTriParameters params = getNeighbourFiltTriParameters();
		testConversion(MBG_FILE2, params, 3222, 79196);
	}

	/**
	 * Test de conversion du fichier donné.
	 */
	public void testConversion(String inputMbgFileName, FiltTriParameters params, int filteredNbSounds,
			int totalNbSounds) throws GException, IOException {

		StringBuilder resume = new StringBuilder("FilTri result : \n");
		boolean testFailed = false;

		String prefix = params.getProperty(FiltTriParameters.PROPERTY_NEW_FILES_PREFIX);
		File inputMbgFile = new File(inputMbgFileName);
		String refFileName = MbgFiltTriTest.REFERENCE_FOLDER + "/" + prefix + inputMbgFile.getName();
		File resultMbgFile = new File(this.tempDir, prefix + inputMbgFile.getName());
		String resultMbgFileName = resultMbgFile.getAbsolutePath();

		// File configuration
		params.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_DIRECTORY, this.tempDir);
		params.setBoolProperty(FiltTriParameters.PROPERTY_NEW_FILE_OPTION, true);
		params.setIntProperty(FiltTriParameters.PROPERTY_SLICE_SIZE, 250);

		// Projection

		ISounderNcInfo info = IDataContainerInfoService.grab().getSounderNcInfo(inputMbgFileName).orElse(null);
		double lat = Math.round(info.getGeoBox().getCenterY() * 10.) / 10.;
		String projection = "+proj=merc ";
		projection += "+lat_ts=" + lat;
		projection += " +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs ";
		params.setProjection(new ProjectionSettings(projection));

		// Launch process
		FilTriProcess filtri = new FilTriProcess();

		// Launch process: % CARAIBES coordinate system

		/*
		 * FilTriProcess filtri = new FilTriProcess() {
		 * 
		 * @Override public SoundingsDataContainerProxy buildSoundings(ISounderNcInfo
		 * info, Projection proj) throws GIOException { return new
		 * SoundingsDataContainerProxy(info, proj) {
		 * 
		 * @Override protected void initLayers() throws GIOException {
		 * super.initLayers(); this.depthLayer =
		 * dataContainer.getLayer(MbgPredefinedLayers.DEPTH); } }; } };
		 */

		FilTriProcessResult result = filtri.process(new File(inputMbgFileName), resultMbgFile, params, Optional.empty(),
				new NullProgressMonitor());

		Assert.assertEquals("Wrong number of total beam", totalNbSounds, result.getTotalNbSounds());
		Assert.assertEquals("Wrong number of filtered beam", filteredNbSounds, result.getFilteredNbSounds());

		// Check if file exists

		Assert.assertTrue(new File(refFileName).exists());

		Assert.assertTrue(resultMbgFile.exists());

		NetcdfComparatorHook hook = buildHook();
		Optional<String> errors = NetcdfComparator.compareFiles(refFileName, resultMbgFileName, Optional.of(hook));
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(resultMbgFileName + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(resultMbgFileName + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(resume.toString(), testFailed);
	}

	@Before
	public void init() throws IOException {
		this.tempDir = Files.createTempDirectory("FilTri_", new FileAttribute[0]).toAbsolutePath().toString();
	}

	/**
	 * Nettoyage après chaque test.
	 */
	@After
	public void afterConversion() throws IOException {
		if (this.tempDir != null)
			FileUtils.forceDelete(new File(this.tempDir));
	}

	/**
	 * @return un NetCdfHook permettant de comparer deux fichiers MBG.
	 */
	protected NetcdfComparatorHook buildHook() {
		NetcdfComparatorHook hook = new NetcdfComparatorHook();
		hook.ignoreVariable("mbHistCode");
		hook.ignoreVariable("mbHistDate");
		hook.ignoreVariable("mbHistTime");
		hook.ignoreVariable("mbHistAutor");
		hook.ignoreVariable("mbHistModule");
		hook.ignoreVariable("mbHistComment");

		hook.ignoreGlobalAttribute("mbNbrHistoryRec");
		hook.ignoreGlobalAttribute("mbEllipsoidName");
		hook.setToleranceForAttribute("mbEllipsoidE2", 1.12E-16);
		hook.setToleranceForAttribute("mbEllipsoidInvF", 1.12E-16);
		hook.setToleranceForAttribute("mbEllipsoidInvF", 1.12E-16);

		// due to an old bug (fixed now), the "ManualCleaning" flag is ignored: TODO
		// update test files
		hook.ignoreGlobalAttribute(MbgConstants.ManualCleaning);
		
		// Later introduction (MBG for Globe version > 2.3.4)
		hook.ignoreGlobalAttribute("mbBiasCorrectionRef");

		return hook;
	}

	/**
	 * Configure FiltTri for a Height method.
	 */
	protected FiltTriParameters getHeightFiltTriParameters() {
		FiltTriParameters parameters = new FiltTriParameters();
		parameters.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_PREFIX, "MethodHeight_");

		parameters.setIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION,
				FiltTriParameters.FILTERING_OPTION_HEIGHT);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_HEIGHT_INVALID_LIMIT, 4d);
		parameters.setIntProperty(FiltTriParameters.PROPERTY_HEIGHT_ITERATION_NUMBER, 2);
		return parameters;
	}

	/**
	 * Configure FiltTri for a Neighbour method.
	 */
	protected FiltTriParameters getNeighbourFiltTriParameters() {
		FiltTriParameters parameters = new FiltTriParameters();
		parameters.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_PREFIX, "MethodNeighbour_");

		parameters.setIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION,
				FiltTriParameters.FILTERING_OPTION_NEIGHBOUR);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_HEIGHT_INVALID_LIMIT, 5d);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_SELECT_LIMIT, 4d);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_DISTANCE_INVALID_LIMIT, 70d);
		return parameters;
	}

	/**
	 * Configure FiltTri for a Normal method.
	 */
	protected FiltTriParameters getNormalFiltTriParameters() {
		FiltTriParameters parameters = new FiltTriParameters();
		parameters.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_PREFIX, "MethodNormal_");

		parameters.setIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION,
				FiltTriParameters.FILTERING_OPTION_NORMAL);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_HEIGHT_INVALID_LIMIT, 5d);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_SELECT_LIMIT, 4d);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_ANGLE_INVALID_LIMIT, 60d);

		return parameters;
	}

}
