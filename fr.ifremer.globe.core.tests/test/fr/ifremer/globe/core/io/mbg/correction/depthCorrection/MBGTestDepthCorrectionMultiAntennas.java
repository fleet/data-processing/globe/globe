package fr.ifremer.globe.core.io.mbg.correction.depthCorrection;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class MBGTestDepthCorrectionMultiAntennas extends MBGDepthCorrection {

	// Input reference file
	public static final String DEPTH_CORRECTION_REF_INPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/refMBG/0018_20120222_131427_ShipName.mbg";

	/***Tide Correction***/
	// Output reference file
	public static final String TIDE_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/Tide_0018_20120222_131427_ShipName.mbg";
	// File to be processed
	public static final String TIDE_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/TideCorrection_A.mbg";
	// File to be processed
	public static final String TIDE_CORRECTION_TST_FILE_2 = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/TideCorrection_A_2.mbg";
	// Tide reference file
	public static final String TIDE_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/TideCorrection-A.ttb";


	/***Tide Correction with Cut file***/
	// Output reference file
	public static final String TIDE_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/Tide_cut_0018_20120222_131427_ShipName.mbg";
	// File to be processed
	public static final String TIDE_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/TideCorrectionCut_A.mbg";
	// Tide reference file
	public static final String TIDE_CUT_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/TideCorrection-A.ttb";
	// Cut file
	public static final String TIDE_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/multiAntennas/decoupe_tide2.cut";


	/***DynamicDraught Correction***/
	// Output reference file
	public static final String DRAUGHT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/multiAntennas/draught_0018_20120222_131427_ShipName.mbg";
	// File to be processed
	public static final String DRAUGHT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/multiAntennas/DraughtCorrection_A.mbg";
	// Tide reference file
	public static final String DRAUGHT_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/multiAntennas/DraughtCorrection-A.ttb";


	/***DynamicDraught Correction with Cut file***/
	// Output reference file
	public static final String DRAUGHT_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/multiAntennas/draught_cut0018_20120222_131427_ShipName.mbg";
	// File to be processed
	public static final String DRAUGHT_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/multiAntennas/DraughtCorrectionCut_A.mbg";
	// Tide reference file
	public static final String DRAUGHT_CUT_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/multiAntennas/DraughtCorrection-A.ttb";
	// Cut file
	public static final String DRAUGHT_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/multiAntennas/decoupe_draught2.cut";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * Processes a test on tide correction: first tide correction then second correction with the same file correction. Goal : no difference with
	 * the same MBG reference file
	 */
	@Test
	public void testTideCorrection() throws Exception {
		tideCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, TIDE_CORRECTION_REF_OUTPUT_FILE, TIDE_CORRECTION_TST_FILE, TIDE_CORRECTION_TIDE_FILE, null);

		// Second correction
		tideCorrection(TIDE_CORRECTION_TST_FILE, TIDE_CORRECTION_REF_OUTPUT_FILE, TIDE_CORRECTION_TST_FILE_2, TIDE_CORRECTION_TIDE_FILE, null);

	}

	/**
	 * Processes a test on tide correction with a correction file.
	 */
	@Test
	public void testTideCutCorrection() throws Exception {
		tideCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, TIDE_CUT_CORRECTION_REF_OUTPUT_FILE, TIDE_CUT_CORRECTION_TST_FILE, TIDE_CUT_CORRECTION_TIDE_FILE, TIDE_CUT_CORRECTION_CUT_FILE);
	}

	/**
	 * Processes a test on draught correction.
	 */
	@Test
	public void testDraughtCorrection() throws Exception {
		draughtCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, DRAUGHT_CORRECTION_REF_OUTPUT_FILE, DRAUGHT_CORRECTION_TST_FILE, DRAUGHT_CORRECTION_TIDE_FILE, null);
	}

	/**
	 * Processes a test on draught correction with a correction file.
	 */
	@Test
	public void testDraughtCutCorrection() throws Exception {
		draughtCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, DRAUGHT_CUT_CORRECTION_REF_OUTPUT_FILE, DRAUGHT_CUT_CORRECTION_TST_FILE, DRAUGHT_CUT_CORRECTION_TIDE_FILE, DRAUGHT_CUT_CORRECTION_CUT_FILE);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		FileUtils.deleteQuietly(new File(TIDE_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(TIDE_CORRECTION_TST_FILE_2));
		FileUtils.deleteQuietly(new File(TIDE_CUT_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(DRAUGHT_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(DRAUGHT_CUT_CORRECTION_TST_FILE));
	}

}
