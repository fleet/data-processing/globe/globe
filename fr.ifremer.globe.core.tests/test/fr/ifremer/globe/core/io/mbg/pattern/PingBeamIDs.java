package fr.ifremer.globe.core.io.mbg.pattern;

public class PingBeamIDs {
	public PingBeamIDs(int pingId, int beamId) {
		this.pingId = pingId;
		this.beamId = beamId;
	}

	int pingId;
	int beamId;

	@Override
	public String toString() {
		return pingId + ":" + beamId;
	}
}