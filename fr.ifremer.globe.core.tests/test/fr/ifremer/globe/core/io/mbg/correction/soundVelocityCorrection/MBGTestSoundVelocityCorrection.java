package fr.ifremer.globe.core.io.mbg.correction.soundVelocityCorrection;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.correction.MBGCorrection;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.model.SoundVelocityCorrectionParameters;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.process.SoundVelocityCorrection;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Class test for sound velocity correction Caraibes algorithm is known to be
 * bugged and
 * 
 */
public class MBGTestSoundVelocityCorrection extends MBGCorrection implements IDataContainerOwner {
	@Rule
	public TemporaryFolder tmpFolder = new TemporaryFolder();

	// Input reference file
	public static final String SOUNDVELOCITY_INPUT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/soundVelocityCorrection/reference/0372_20140827_195125_AT_EM122.mbg";

	// Corrected reference file this file was manually inspected
	public static final String REF_CORRECTED_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/soundVelocityCorrection/reference/0372_20140827_195125_AT_EM122_cts_profile_manually_validated.mbg";

	// velocity profile
	public static final String VEL_PROFILE_1300 = GlobeTestUtil.getTestDataPath()
			+ "/file/soundVelocityCorrection/reference/constant_vel_1300_19_51_19.vel";
	// velocity profile
	public static final String VEL_PROFILE_1500 = GlobeTestUtil.getTestDataPath()
			+ "/file/soundVelocityCorrection/reference/constant_vel_1500_19_59_48.vel";
	// Cut reference file
	public static final String TIMEINTERPOLATION_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/soundVelocityCorrection/reference/decoupe_0372_20140827_195125_AT_EM122.cut";

	private SoundVelocityCorrection create(File output, TimeIntervalParameters timeIntervalParameters)
			throws Exception {
		// Copy input file before processing to be sure we do not modify source dataset
		File inputFile = new File(SOUNDVELOCITY_INPUT_FILE);
		FileUtils.copyFile(inputFile, output);

		MbgInfo mbgInfo = (MbgInfo) IFileService.grab().getFileInfo(output.getAbsolutePath()).orElse(null);
		Assert.assertNotNull(mbgInfo);
		//ModelRoot.getInstance().addInfoStore(mbgInfo);

		// Setup process
		// Parameters
		SoundVelocityCorrectionParameters parameters = new SoundVelocityCorrectionParameters();
		parameters.setArrayProperty(SoundVelocityCorrectionParameters.PROPERTY_VEL_FILE_LIST,
				new String[] { VEL_PROFILE_1300, VEL_PROFILE_1500 });
		parameters.setBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_TIME_INTERPOLATION_OPTION, Boolean.TRUE);
		parameters.setBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_NEAREST_NEIGHTBOR_OPTION, Boolean.FALSE);
		List<String> fileNames = new ArrayList<String>();
		fileNames.add(output.getAbsolutePath());

		SoundVelocityCorrection soundVelocityCorrection = new SoundVelocityCorrection(parameters,
				timeIntervalParameters);
		soundVelocityCorrection.setFileNameList(fileNames);

		return soundVelocityCorrection;
	}

	/**
	 * Non regression test that create a velocity correction and compare results
	 * with a reference file which had been validated manually and visually
	 * inspected This use two constant velocity profile located at the start and
	 * stop of file
	 */
	@Test
	public void testSoundVelocityCorrection() throws Exception {

		File outputFolder = tmpFolder.getRoot();
		File output = File.createTempFile("out_", ".mbg", outputFolder);

		// TimeInterval Parameters
		TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
				"fr.ifremer.globe.process.soundvelocity");
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION, Boolean.FALSE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION, Boolean.FALSE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_NOINTERVAL_OPTION, Boolean.TRUE);

		SoundVelocityCorrection proc = create(output, timeIntervalParameters);

		// Activate process
		IStatus result = proc.apply(new NullProgressMonitor(), logger);
		Assert.assertTrue("Sound velocity correction result checking", result.isOK());

		// now compare results with the processed reference file
		hook = buildHook();
		hook.setToleranceForAttribute(MbgConstants.NorthLatitude, 1E-6);
		hook.setToleranceForAttribute(MbgConstants.SouthLatitude, 1E-6);
		hook.setToleranceForAttribute(MbgConstants.WestLongitude, 1E-6);
		hook.setToleranceForAttribute(MbgConstants.EastLongitude, 1E-6);
		hook.setToleranceForAttribute(MbgConstants.MinDepth, 1E-6);
		hook.setToleranceForAttribute(MbgConstants.MaxDepth, 1E-5);
		hook.setToleranceForVariable(MbgConstants.Depth, 1e-2);
		hook.setToleranceForVariable(MbgConstants.AcrossDistance, 1e-2);

		compareFiles(REF_CORRECTED_FILE, output.getAbsolutePath(), Optional.of(hook));

	}

	@Test
	public void testSoundVelocityCorrectionWithCut() throws Exception {

		File outputFolder = tmpFolder.getRoot();
		File output = File.createTempFile("out_", ".mbg", outputFolder);

		// TimeInterval Parameters
		TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
				"fr.ifremer.globe.process.soundvelocity");
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION, Boolean.TRUE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION, Boolean.FALSE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_NOINTERVAL_OPTION, Boolean.FALSE);
		timeIntervalParameters.setProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME,
				TIMEINTERPOLATION_CUT_CORRECTION_CUT_FILE);

		SoundVelocityCorrection proc = create(output, timeIntervalParameters);
		// Activate process
		IStatus result = proc.apply(new NullProgressMonitor(), logger);
		Assert.assertTrue("Sound velocity correction result checking", result.isOK());

		// NOW DO SOME TESTS

		// Injection required to retrieve the container factory
		MbgInfo mbgOutpuInfo = (MbgInfo) IFileService.grab().getFileInfo(output.getAbsolutePath()).orElse(null);
		MbgInfo mbgRefInputInfo = (MbgInfo) IFileService.grab().getFileInfo(SOUNDVELOCITY_INPUT_FILE).orElse(null);
		MbgInfo mbgRefComputedInfo = (MbgInfo) IFileService.grab().getFileInfo(REF_CORRECTED_FILE).orElse(null);

		var dataContainerFactory = IDataContainerFactory.grab();
		try (ISounderDataContainerToken outToken = dataContainerFactory.book(mbgOutpuInfo, this);
				ISounderDataContainerToken refToken = dataContainerFactory.book(mbgRefInputInfo, this);
				ISounderDataContainerToken processedToken = dataContainerFactory.book(mbgRefComputedInfo, this)) {

			int cycleCount = mbgOutpuInfo.getCycleCount();
			int beamCount = mbgOutpuInfo.getTotalRxBeamCount();

			FloatLoadableLayer2D depth_out = outToken.getDataContainer().getLayer(BathymetryLayers.DETECTION_Z);
			FloatLoadableLayer2D depth_ref = refToken.getDataContainer().getLayer(BathymetryLayers.DETECTION_Z);
			FloatLoadableLayer2D depth_ref_processed = processedToken.getDataContainer()
					.getLayer(BathymetryLayers.DETECTION_Z);

			// check that values are equals to depth_ref up to ping number x
			// then check that values are equals to depth_ref_processed after ping number x
			// (matching the data in cut file
			for (int cycle = 0; cycle < 92; cycle++) {
				for (int beam = 0; beam < beamCount; beam++) {
					Assert.assertEquals("cycle:" + cycle, depth_out.get(cycle, beam),
							depth_ref_processed.get(cycle, beam), 1e-2);
				}
			}
			for (int cycle = 92; cycle < cycleCount; cycle++) {
				for (int beam = 0; beam < beamCount; beam++) {
					Assert.assertEquals("cycle:" + cycle, depth_out.get(cycle, beam), depth_ref.get(cycle, beam), 1e-2);
				}
			}
		}
	}

}
