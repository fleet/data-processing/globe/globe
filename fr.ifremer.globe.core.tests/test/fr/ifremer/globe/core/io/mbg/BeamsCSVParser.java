package fr.ifremer.globe.core.io.mbg;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import org.junit.Assert;

/**
 * Class used to parse csv file extracted from caraibes (informations about beams)
 * */
public class BeamsCSVParser {

	public class Record {
		public int filePingNumber;
		public int pingId;
		public int antennaId;
		public int CFlag;
		public double heading;
		public double roll;
		public double pitch;
		public double heave;
		public double immersion;
		public double tide;
		public double vertDepth;
		public double dyDraught;
		public int beamIndex;
		public double latitude;
		public double longitude;
		public double depth;
		public float alongDist;
		public double acrossDist;
		public float azimutAngle;
		public float acrossAngle;
		public double range;
		public int sFlag;
		public int ampPhase;
		public float qualityFactor;
		public double reflectivity;
		public String Date;
		public String Hour;
		
		
		
		@Override
		public String toString() {
			return pingId + ";" + beamIndex + ";" + latitude + ";" + longitude ;
		}
	}

	private ArrayList<Record> listOfRecord = new ArrayList<Record>();

	public void parseFile(String fileName) {
		Scanner scan = null;

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line = null;
			reader.readLine();
			while ((line = reader.readLine()) != null) {
                line = line.replace(" ","");
				scan = new Scanner(line.trim());
				scan.useDelimiter(";|\n");
				scan.useLocale(Locale.US); // Pour les floats
				Record rec = new Record();
				rec.filePingNumber = scan.nextInt();
				rec.pingId = scan.nextInt();
				rec.antennaId = scan.nextInt();
				rec.CFlag = scan.nextInt();
				rec.heading = scan.nextDouble();
				rec.roll = scan.nextDouble();
				rec.pitch = scan.nextDouble();
				rec.heave = scan.nextDouble();
				rec.immersion = scan.nextDouble();
				rec.tide = scan.nextDouble();
				rec.vertDepth = scan.nextDouble();
				rec.dyDraught = scan.nextDouble();
				rec.beamIndex = scan.nextInt();
				rec.longitude = scan.nextDouble();
				rec.latitude = scan.nextDouble();
				rec.depth = scan.nextDouble();
				rec.alongDist = scan.nextFloat();
				rec.acrossDist = scan.nextDouble();
				rec.azimutAngle = scan.nextFloat();
				rec.acrossAngle = scan.nextFloat();
				rec.range = scan.nextDouble();
				rec.sFlag = scan.nextInt();
				rec.ampPhase = scan.nextInt();
				rec.qualityFactor = scan.nextFloat();
				rec.reflectivity = scan.nextDouble();
				rec.Date = scan.next();
				rec.Hour = scan.next();

				listOfRecord.add(rec);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Assert.assertTrue("File" + fileName + " does not exists", false);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.assertTrue("File" + fileName + " does not exists", false);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}

	}

	public ArrayList<Record> getListOfRecord() {
		return listOfRecord;
	}

}
