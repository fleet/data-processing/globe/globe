/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.core.io.mbg.export;

import java.io.File;

import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionStatusApplier;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionStatusExporter;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalker;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Tests d'export DTM vers ValidityFlag.
 */
public class MBGToValidityFlagTest implements IDataContainerOwner {

	protected static final Logger LOGGER = LoggerFactory.getLogger(MBGToValidityFlagTest.class);

	/** Fichier à tester. */
	public static final String MBG_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/CARAIBES/20130322_204233_PP-7150-24kHz-CIB.mbg";

	@Inject
	LayersWalker layersWalker;

	@Before
	public void setUp() {
		ContextInjectionFactory.inject(this, EclipseContextFactory.getServiceContext(Activator.getContext()));
	}

	/**
	 * export les validités de mbg.<br>
	 * mise à zéro des validités de mbg.<br>
	 * import les validités.<br>
	 * vérifie que les validités sont bien revenues à leur valeur initiale
	 */
	@Test
	public void testConversion() throws Exception {
		// Copy input file before processing
		File orinalMbgFile = new File(MBG_FILE1);
		File testMbgFile = TemporaryCache.createTemporaryFile(MBGToValidityFlagTest.class.getSimpleName(), ".mbg");
		FileUtils.copyFile(orinalMbgFile, testMbgFile);

		// export des validités d'origine
		File originalValidityFile = TemporaryCache.createTemporaryFile(MBGToValidityFlagTest.class.getSimpleName(), ".csv");
		MbgInfo mbgInfo = (MbgInfo) IFileService.grab().getFileInfo(testMbgFile.getAbsolutePath()).orElse(null);
		Assert.assertNotNull(mbgInfo);

		byte[][] expectedStatus = new byte[mbgInfo.getCycleCount()][mbgInfo.getTotalRxBeamCount()];
		byte[][] expectedStatusDetails = new byte[mbgInfo.getCycleCount()][mbgInfo.getTotalRxBeamCount()];
		loadValidities(mbgInfo, expectedStatus, expectedStatusDetails);

		// export
		try (var detectionStatusExporter = new DetectionStatusExporter(layersWalker, mbgInfo, originalValidityFile,
				false, ';')) {
			detectionStatusExporter.apply(new NullProgressMonitor(), LOGGER);
		}

		// modification des validités avant réimport
		resetValidities(mbgInfo);

		// import
		new DetectionStatusApplier(mbgInfo, originalValidityFile).apply(new NullProgressMonitor(), LOGGER);

		checkValidities(mbgInfo, expectedStatus, expectedStatusDetails);

		// clean
		testMbgFile.delete();
		originalValidityFile.delete();
	}

	/** mise à zéro des validités de mbg. */
	protected void resetValidities(MbgInfo info) throws Exception {
		try (ISounderDataContainerToken sounderDataContainerToken = IDataContainerFactory.grab().book(info, this)) {
			SounderDataContainer dataContainer = sounderDataContainerToken.getDataContainer();
			int cycleCount = dataContainer.getInfo().getCycleCount();
			int beamCount = dataContainer.getInfo().getTotalRxBeamCount();

			ByteLoadableLayer2D statusLayer = dataContainer.getLayer(BathymetryLayers.STATUS);
			ByteLoadableLayer2D statusLayerDetails = dataContainer.getLayer(BathymetryLayers.STATUS_DETAIL);

			for (int cycle = 0; cycle < cycleCount; cycle++) {
				for (int beam = 0; beam < beamCount; beam++) {
					statusLayer.set(cycle, beam, (byte) 0);
					statusLayerDetails.set(cycle, beam, (byte) 0);
				}
			}
			dataContainer.flush();
		}
	}

	/** charge les validités dans des tableaux pour une future comparaison */

	protected void loadValidities(MbgInfo info, byte[][] expectedStatus, byte[][] expectedStatusDetails)
			throws Exception {
		try (ISounderDataContainerToken sounderDataContainerToken = IDataContainerFactory.grab().book(info, this)) {
			SounderDataContainer dataContainer = sounderDataContainerToken.getDataContainer();
			int cycleCount = dataContainer.getInfo().getCycleCount();
			int beamCount = dataContainer.getInfo().getTotalRxBeamCount();

			ByteLoadableLayer2D statusLayer = dataContainer.getLayer(BathymetryLayers.STATUS);
			ByteLoadableLayer2D statusLayerDetails = dataContainer.getLayer(BathymetryLayers.STATUS_DETAIL);

			// check
			for (int cycle = 0; cycle < cycleCount; cycle++) {
				for (int beam = 0; beam < beamCount; beam++) {
					expectedStatus[cycle][beam] = statusLayer.get(cycle, beam);
					expectedStatusDetails[cycle][beam] = statusLayerDetails.get(cycle, beam);
				}
			}
		}
	}

	/** vérifie que les validités sont bien revenues à leur valeur initiale */
	protected void checkValidities(MbgInfo info, byte[][] expectedStatus, byte[][] expectedStatusDetails)
			throws Exception {
		try (ISounderDataContainerToken sounderDataContainerToken = IDataContainerFactory.grab().book(info, this)) {
			SounderDataContainer dataContainer = sounderDataContainerToken.getDataContainer();
			int cycleCount = dataContainer.getInfo().getCycleCount();
			int beamCount = dataContainer.getInfo().getTotalRxBeamCount();

			ByteLoadableLayer2D statusLayer = dataContainer.getLayer(BathymetryLayers.STATUS);
			ByteLoadableLayer2D statusLayerDetails = dataContainer.getLayer(BathymetryLayers.STATUS_DETAIL);

			// check
			for (int cycle = 1; cycle < cycleCount; cycle++) {
				for (int beam = 1; beam < beamCount; beam++) {
					statusLayer.set(cycle, beam, expectedStatus[cycle][beam]);
					statusLayerDetails.set(cycle, beam, expectedStatusDetails[cycle][beam]);
				}
			}
		}
	}
}
