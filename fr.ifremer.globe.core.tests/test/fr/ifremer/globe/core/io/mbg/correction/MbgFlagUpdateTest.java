package fr.ifremer.globe.core.io.mbg.correction;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.io.mbg.writer.MbgInfoWriter;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * This class test the flag edition of MBG, with files from different version of Globe.
 */
public class MbgFlagUpdateTest {

	protected static Logger logger = LoggerFactory.getLogger(MbgFlagUpdateTest.class);

	public static final String TEST_DIR = GlobeTestUtil.getTestDataPath() + "/file/mbg/flagtest";

	@Test
	public void testMbgFlag1() throws GIOException, NCException, IOException {
		testMbgFlag("0075_20120525_140214_Thalia_1-16-12.mbg");
	}

	@Test
	public void testMbgFlag2() throws GIOException, NCException, IOException {
		testMbgFlag("0076_20190720_223624_EM122_Marion_Dufresne.mbg");
	}

	@Test
	public void testMbgFlag3() throws GIOException, NCException, IOException {
		testMbgFlag("0075_20120525_140214_Thalia_1-10-9.mbg");
	}

	@Test
	public void testMbgFlag4() throws GIOException, NCException, IOException {
		testMbgFlag("0075_20120525_140214_Thalia.mbg");
	}

	public void testMbgFlag(String refFileName) throws IOException, GIOException, NCException {
		// create test file (copy reference file)
		String refFilePath = TEST_DIR + File.separator + refFileName;
		String testFilePath = refFilePath.replace(".mbg", "_test.mbg");
		Files.copy(new File(refFilePath).toPath(), new File(testFilePath).toPath(),
				StandardCopyOption.REPLACE_EXISTING);

		// set flag & write
		MbgInfo mbgTest = IFileService.grab().getFileInfo(testFilePath).map(MbgInfo.class::cast).orElseThrow();
		mbgTest.setManualCleaning((byte) 1);
		mbgTest.setAutomaticCleaning((byte) 2);
		mbgTest.setVelocityCorrection((byte) 3);
		mbgTest.setBiasCorrection((byte) 4);
		mbgTest.setTideCorrection((byte) 5);
		mbgTest.setDraughtCorrection((byte) 6);
		mbgTest.setPositionCorrection((byte) 7);
		new MbgInfoWriter().writeProperties(mbgTest);
		mbgTest.close();

		// reopen file & check values
		try(NetcdfFile file = NetcdfFile.open(testFilePath, Mode.readwrite)) {
			checkCharAttribute(file, MbgConstants.ManualCleaning, (char) 1);
			checkCharAttribute(file, MbgConstants.AutomaticCleaning, (char) 2);
			checkCharAttribute(file, MbgConstants.VelocityCorrection, (char) 3);
			checkCharAttribute(file, MbgConstants.BiasCorrection, (char) 4);
			checkCharAttribute(file, MbgConstants.TideCorrection, (char) 5);
			checkCharAttribute(file, MbgConstants.DraughtCorrection, (char) 6);
			checkCharAttribute(file, MbgConstants.PositionCorrection, (char) 7);
		}
	}

	private void checkCharAttribute(NetcdfFile file, String name, char expectedValue) throws NCException {
		assertEquals("Check " + name, (char) expectedValue, file.getAttributeChar(name));
	}
}
