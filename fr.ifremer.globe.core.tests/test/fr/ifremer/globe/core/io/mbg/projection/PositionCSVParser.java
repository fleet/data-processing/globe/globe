package fr.ifremer.globe.core.io.mbg.projection;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import org.junit.Assert;

/**
 * Class used to parse csv file extracted from caraibes
 * */
public class PositionCSVParser {

	public class Record {
		public int pingId;
		public int beamIndex;
		public double latitude;
		public double longitude;
		public double X;
		public double Y;

		@Override
		public String toString() {
			return pingId + ";" + beamIndex + ";" + latitude + ";" + longitude + ";" + X + ";" + Y;
		}
	}

	private ArrayList<Record> listOfRecord = new ArrayList<Record>();

	public void parseFile(String fileName) {
		Scanner scan = null;

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line = null;
			reader.readLine();
			while ((line = reader.readLine()) != null) {

				scan = new Scanner(line.trim());
				scan.useDelimiter(";|\n");
				scan.useLocale(Locale.US); // Pour les floatss
				Record rec = new Record();
				rec.pingId = scan.nextInt();
				rec.beamIndex = scan.nextInt();
				rec.latitude = scan.nextDouble();
				rec.longitude = scan.nextDouble();
				rec.X = scan.nextDouble();
				rec.Y = scan.nextDouble();
				getListOfRecord().add(rec);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Assert.assertTrue("File" + fileName + " does not exists", false);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.assertTrue("File" + fileName + " does not exists", false);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}

	}

	public ArrayList<Record> getListOfRecord() {
		return listOfRecord;
	}

}
