/**
 *
 */
package fr.ifremer.globe.core.io.mbg.convert;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Test class for MbgDriver. Ici l'objet est de comparer 2 fichiers de type mbg et de vérifier les écarts attendus entre
 * chacun des fichiers
 */
public  class KongsbergMBGTestConverter {
	
	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(KongsbergMBGTestConverter.class);
	
	public static final String TEST_ALL_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/Kongsberg/0370_20140827_181502_AT_EM122.all";
	public static final String TEST_GLOBE_ALL_MBG_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/0370_20140827_181502_AT_EM122-GLOBE.mbg";
	public static final String TEST_CARAIBES_ALL_MBG_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/Kongsberg/CARAIBES/0370_20140827_181502_AT_EM122-CIB.mbg";




	/**
	 * Main class for the test, will read the file, load it into model and check every variables in memory to ensure
	 * that they are equals
	 * @throws IOException 
	 * @throws NCException 
	 */
	@Test
	public void testInitData() throws GIOException, IOException, NCException {
		MbgConverterParameters parameters = new MbgConverterParameters(TEST_ALL_FILE, TEST_GLOBE_ALL_MBG_FILE);
		parameters.setShipName("PP");
		parameters.setSurveyName("DD");
		parameters.setReference("GG");
		parameters.setCdi("kkkkk");

		try {
			// parse the file into a mbg file in the output dir
			new SounderFileConverter().convertToMbg(parameters);
		} catch (Exception e) {
			File outputFile = new File(TEST_GLOBE_ALL_MBG_FILE);
			if (outputFile != null) {
				outputFile.delete();
			}
			e.printStackTrace();
		}

		// Check if  files exists

		Assert.assertTrue(new File (TEST_GLOBE_ALL_MBG_FILE).exists());

		Assert.assertTrue(new File(TEST_CARAIBES_ALL_MBG_FILE).exists());

		StringBuilder resume = new StringBuilder("MbgConverter result : \n");
		boolean testFailed = false;

		NetcdfComparatorHook hook = buildHook();
		Optional<String> errors = NetcdfComparator.compareFiles(TEST_CARAIBES_ALL_MBG_FILE, TEST_GLOBE_ALL_MBG_FILE, Optional.of(hook));
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(TEST_GLOBE_ALL_MBG_FILE + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(TEST_GLOBE_ALL_MBG_FILE + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(testFailed);
	}

	/*
	 * public KongsbergMBGTestConverter() { hook = new NetcdfComparatorHook();
	 * buildHook(); }
	 */
	private NetcdfComparatorHook buildHook() {
		NetcdfComparatorHook hook = new NetcdfComparatorHook();

		hook.ignoreGlobalAttribute(MbgConstants.Name);  
		hook.ignoreGlobalAttribute(MbgConstants.mbTxAntennaLeverArm); 
		// No comparison because offsets with CARAIBES > à 10e-15
		// better in GLOBE normally because extreme distances no coded before calculation of the cartographic limit
		// positions
		
		hook.ignoreAttributeForAllVariables("_FillValue"); 
 
		hook.setToleranceForAttribute(MbgConstants.NorthLatitude, 0.000005);
		hook.setToleranceForAttribute(MbgConstants.SouthLatitude, 0.000005);
		hook.setToleranceForAttribute(MbgConstants.WestLongitude, 0.000005);
		hook.setToleranceForAttribute(MbgConstants.EastLongitude, 0.000005);
		hook.setToleranceForAttribute(MbgConstants.EllipsoidE2, 1.12E-16);
		hook.setToleranceForAttribute(MbgConstants.EllipsoidInvF, 1.12E-16);
		hook.setToleranceForAttribute(MbgConstants.MinDepth, 4E-4);
		hook.setToleranceForAttribute(MbgConstants.MaxDepth, 2E-3);
		
		hook.setToleranceForVariable(MbgConstants.Range, 1.1E-6);
		
		hook.ignoreVariable("mbHistCode");
		hook.ignoreVariable("mbHistDate");
		hook.ignoreVariable("mbHistAutor");
		hook.ignoreVariable("mbHistTime");
		hook.ignoreVariable("mbHistModule");
		hook.ignoreVariable("mbHistComment");
		hook.ignoreVariable("mbSounderMode"); // runtime datagram better assigned in GLOBE
		hook.ignoreVariable("mbRoll");// because depth datagram before first attitude datagram: case not treat in
										// CARAIBES (dychotomy)
		hook.ignoreVariable("mbPitch");// because depth datagram before first attitude datagram: case not treat in
										// CARAIBES
		hook.ignoreVariable("mbTransmissionHeave"); // because depth datagram before first attitude datagram: case not
													// treat in CARAIBES
		hook.ignoreVariable("mbDistanceScale"); // Calculated on valid sounds in CARAIBES
		hook.ignoreVariable("mbVerticalDepth"); // calculated on rounded depth in CARAIBES
		hook.ignoreVariable("mbCQuality"); // Number of valid beams in CARAIBES (often the number of beams exceeds the
											// maximum of a char); 127 in GLOBE
		hook.ignoreVariable("mbTransmitBeamwidth"); // runtime datagram better assigned in GLOBE
		hook.ignoreVariable("mbReceiveBeamwidth"); // runtime datagram better assigned in GLOBE
		hook.ignoreVariable("mbTransmitPulseLength"); // ""
		hook.ignoreVariable("mbOperatorStationStaus");// ""
		hook.ignoreVariable("mbProcessingUnitStatus");// ""
		hook.ignoreVariable("mbBSPStatus"); // ""
		hook.ignoreVariable("mbSonarStatus"); // ""
		hook.ignoreVariable("mbFilterIdentifier"); // ""
		hook.ignoreVariable("mbParamMinimumDepth"); // ""
		hook.ignoreVariable("mbParamMaximumDepth"); // ""
		hook.ignoreVariable("mbAbsorptionCoefficient"); // ""
		hook.ignoreVariable("mbTransmitPowerReMax"); // ""
		hook.ignoreVariable("mbReceiveBandwidth"); // ""
		hook.ignoreVariable("mbReceiverFixedGain"); // ""
		hook.ignoreVariable("mbTVGLawCrossoverAngle"); // ""
		hook.ignoreVariable("mbTransVelocitySource"); // ""
		hook.ignoreVariable("mbMaxPortWidth"); // ""
		hook.ignoreVariable("mbBeamSpacing"); // ""
		hook.ignoreVariable("mbMaxPortCoverage"); // ""
		hook.ignoreVariable("mbYawPitchStabMode"); // ""
		hook.ignoreVariable("mbMaxStarboardCoverage"); // ""
		hook.ignoreVariable("mbMaxStarboardWidth"); // ""
		hook.ignoreVariable("mbDurotongSpeed"); // ""
		hook.ignoreVariable("mbHiloAbsorptionration");// ""
		hook.ignoreVariable("mbAlongDistance"); // different distance scale between GLOBE and CARAIBES + rounded
												// different
		hook.ignoreVariable("mbAcrossDistance"); // different distance scale between GLOBE and CARAIBES + rounded
													// different
		hook.ignoreVariable("mbAcrossBeamAngle"); // For the first pings in CARAIBES attitude datagrams are null (the
													// first attitude datagram is after the first depth datagram)
		// extrapolation in GLOBE
		hook.ignoreVariable("mbQuality"); // sometimes rounded different
		hook.ignoreVariable("mbVelProfilRef"); // not used in CARAIBES
		hook.ignoreVariable("mbVelProfilIdx"); // "
		hook.ignoreVariable("mbVelProfilDate"); // "
		hook.ignoreVariable("mbVelProfilTime"); // "
		
		//FIXME 24/07/20 : this parameters are not managed by the new converter : TODO!
		hook.ignoreAttribute("mbShip", null);
		hook.ignoreAttribute("mbSurvey", null);
		hook.ignoreAttribute("mbReference", null);
		hook.ignoreAttribute("mbCDI", null);
		return hook;
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		FileUtils.deleteQuietly(new File(TEST_GLOBE_ALL_MBG_FILE));
	}
}
