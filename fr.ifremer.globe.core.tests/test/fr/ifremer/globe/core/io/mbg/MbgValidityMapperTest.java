package fr.ifremer.globe.core.io.mbg;

import java.util.EnumSet;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.io.mbg.datacontainer.MbgValidityConstants;
import fr.ifremer.globe.core.io.mbg.datacontainer.MbgValidityConstants.SoundingValidity;
import fr.ifremer.globe.core.io.mbg.datacontainer.ValidityMapper;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatusDetail;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.StatusPair;

public class MbgValidityMapperTest {

	/**
	 * Defines a combination of [ping/beam/antenna/sound mbg flag] & [model status] to test.
	 */
	class MappingTest {

		/** Properties **/

		/* MBG: valid by default */
		private int pingFlag = MbgValidityConstants.PNG_VALID;
		private int beamFlag = MbgValidityConstants.BEA_VALID;
		private int antennaFlag = MbgValidityConstants.ANT_VALID;
		private SoundingValidity soundingValidity = SoundingValidity.SND_VALID;

		/* Model */
		private EnumSet<SonarDetectionStatus> status = EnumSet.noneOf(SonarDetectionStatus.class);
		private SonarDetectionStatusDetail details = SonarDetectionStatusDetail.UNKNOWN;

		/** Builder methods **/

		protected MappingTest withPingFlag(int f) {
			pingFlag = f;
			return this;
		}

		protected MappingTest withBeamFlag(int f) {
			beamFlag = f;
			return this;
		}

		protected MappingTest withAntennaFlag(int f) {
			antennaFlag = f;
			return this;
		}

		protected MappingTest withSoundingValidity(MbgValidityConstants.SoundingValidity sv) {
			soundingValidity = sv;
			return this;
		}

		protected MappingTest withStatus(SonarDetectionStatus s) {
			status.add(s);
			return this;
		}

		protected MappingTest withStatus(EnumSet<SonarDetectionStatus> s) {
			status = s;
			return this;
		}

		protected MappingTest withDetails(SonarDetectionStatusDetail d) {
			details = d;
			return this;
		}

		/**
		 * test the mapping from MBG flags to model status
		 */
		protected void testMbgToModel() {
			byte statusByte = SonarDetectionStatus.getValue(status);
			byte detailsByte = details.getValue();
			StatusPair expectedResult = new StatusPair(statusByte, detailsByte);
			StatusPair testResult = ValidityMapper.getStatus(pingFlag, beamFlag, antennaFlag, soundingValidity);
			Assert.assertEquals("MbgToModel:", expectedResult, testResult);
		}

		/**
		 * test the mapping from model status to MBG flags
		 */
		protected void testModelToMbg() {
			byte statusByte = SonarDetectionStatus.getValue(status);
			byte detailsByte = details.getValue();

			// Test beam flag
			Assert.assertEquals("ModelToMbg: beam flag", beamFlag, ValidityMapper.getDetectionBeamValidity(statusByte));

			// Test cycle flag
			Assert.assertEquals("ModelToMbg: cycle flag", pingFlag, ValidityMapper.getPingValidity(statusByte));

			// Test sounding validity
			Assert.assertEquals("ModelToMbg: sounding flag", soundingValidity,
					ValidityMapper.getSoundingValidity(statusByte, detailsByte));
		}
	}

	//// TEST VALID FLAGS & STATUS

	@Test
	public void testValid1() {
		MappingTest test = new MappingTest().withSoundingValidity(SoundingValidity.SND_VALID);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testValid2() {
		MappingTest test = new MappingTest().withSoundingValidity(SoundingValidity.SND_VALID)
				.withStatus(SonarDetectionStatus.VALID);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	//// TEST PING FLAG

	@Test
	public void testPing1() {
		MappingTest test = new MappingTest().withPingFlag(MbgValidityConstants.PNG_UNVALID_ACQUIS)
				.withStatus(SonarDetectionStatus.INVALID_SWATH);
		test.testMbgToModel();
		test.withPingFlag(MbgValidityConstants.PNG_UNVALID); // details about the flag invalidity are lost in the model
		test.testModelToMbg();
	}

	@Test
	public void testPing2() {
		MappingTest test = new MappingTest().withPingFlag(MbgValidityConstants.PNG_UNVALID_AUTO)
				.withStatus(SonarDetectionStatus.INVALID_SWATH);
		test.testMbgToModel();
		test.withPingFlag(MbgValidityConstants.PNG_UNVALID); // details about the flag invalidity are lost in the model
		test.testModelToMbg();
	}

	@Test
	public void testPing3() {
		MappingTest test = new MappingTest().withPingFlag(MbgValidityConstants.PNG_UNVALID)
				.withStatus(SonarDetectionStatus.INVALID_SWATH);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testPing4() {
		MappingTest test = new MappingTest().withPingFlag(MbgValidityConstants.PNG_MISSING)
				.withStatus(SonarDetectionStatus.INVALID_SWATH);
		test.testMbgToModel();
		test.withPingFlag(MbgValidityConstants.PNG_UNVALID); // details about the flag invalidity are lost in the model
		test.testModelToMbg();
	}

	@Test
	public void testPing5() {
		MappingTest test = new MappingTest().withPingFlag(MbgValidityConstants.PNG_UNVALID_VALIDATED)
				.withStatus(SonarDetectionStatus.VALID);
		test.testMbgToModel();
		test.withPingFlag(MbgValidityConstants.PNG_VALID); // details about the flag are lost in the model
		test.testModelToMbg();
	}

	@Test
	public void testPing6() {
		MappingTest test = new MappingTest().withPingFlag(MbgValidityConstants.PNG_MOVED).withStatus(SonarDetectionStatus.VALID);
		test.testMbgToModel();
		test.withPingFlag(MbgValidityConstants.PNG_VALID); // details about the flag are lost in the model
		test.testModelToMbg();
	}

	//// TEST BEAM FLAG

	@Test
	public void testBeamFlag1() {
		MappingTest test = new MappingTest().withBeamFlag(MbgValidityConstants.BEA_UNVALID)
				.withStatus(SonarDetectionStatus.INVALID_SOUNDING_ROW);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testBeamFlag2() {
		MappingTest test = new MappingTest().withBeamFlag(MbgValidityConstants.BEA_MISSING)
				.withStatus(SonarDetectionStatus.INVALID_SOUNDING_ROW);
		test.testMbgToModel();
		test.withBeamFlag(MbgValidityConstants.BEA_UNVALID); // details about the flag are lost in the model
		test.testModelToMbg();
	}

	//// TEST ANTENNA FLAG

	@Test
	public void testAntennaFlag1() {
		MappingTest test = new MappingTest().withAntennaFlag(MbgValidityConstants.ANT_UNVALID)
				.withStatus(SonarDetectionStatus.REJECTED).withDetails(SonarDetectionStatusDetail.UNKNOWN);
		test.testMbgToModel();
		// antenna flag not managed by model: sounding from invalid antenna are invalidated
		test.withSoundingValidity(SoundingValidity.SND_UNVALID);
		test.testModelToMbg();
	}

	@Test
	public void testAntennaFlag2() {
		MappingTest test = new MappingTest().withAntennaFlag(MbgValidityConstants.ANT_MISSING)
				.withStatus(SonarDetectionStatus.REJECTED).withDetails(SonarDetectionStatusDetail.UNKNOWN);
		test.testMbgToModel();
		// antenna flag not managed by model: sounding from invalid antenna are invalidated
		test.withSoundingValidity(SoundingValidity.SND_UNVALID);
		test.testModelToMbg();
	}

	//// TEST SOUNDING VALIDITY

	@Test
	public void testSoundingValidity1() {
		MappingTest test = new MappingTest().withSoundingValidity(SoundingValidity.SND_UNVALID_ACQUIS)
				.withStatus(SonarDetectionStatus.INVALID_ACQUISITION);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testSoundingValidity2() {
		MappingTest test = new MappingTest().withSoundingValidity(SoundingValidity.SND_UNVALID_AUTO)
				.withStatus(SonarDetectionStatus.REJECTED).withDetails(SonarDetectionStatusDetail.AUTO);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testSoundingValidity3() {
		MappingTest test = new MappingTest().withSoundingValidity(SoundingValidity.SND_UNVALID)
				.withStatus(SonarDetectionStatus.REJECTED).withDetails(SonarDetectionStatusDetail.MANUAL);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testSoundingValidity4() {
		MappingTest test = new MappingTest().withSoundingValidity(SoundingValidity.SND_MISSING)
				.withStatus(SonarDetectionStatus.INVALID_CONVERSION);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testSoundingValidity5() {
		MappingTest test = new MappingTest().withSoundingValidity(SoundingValidity.SND_DOUBTFUL)
				.withStatus(SonarDetectionStatus.REJECTED).withDetails(SonarDetectionStatusDetail.DOUBTFULL);
		test.testMbgToModel();
		test.testModelToMbg();
	}

	//// MIXED TEST

	@Test
	public void testBeamAndPingInvalid() {
		MappingTest test = new MappingTest();

		test.withBeamFlag(MbgValidityConstants.BEA_UNVALID);
		test.withPingFlag(MbgValidityConstants.PNG_UNVALID);
		test.withStatus(SonarDetectionStatus.INVALID_SWATH).withStatus(SonarDetectionStatus.INVALID_SOUNDING_ROW);

		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testBeamAndAntennaInvalid() {
		MappingTest test = new MappingTest();

		test.withBeamFlag(MbgValidityConstants.BEA_UNVALID);
		test.withAntennaFlag(MbgValidityConstants.ANT_UNVALID);
		test.withStatus(SonarDetectionStatus.INVALID_SOUNDING_ROW).withStatus(SonarDetectionStatus.REJECTED);
		test.withDetails(SonarDetectionStatusDetail.UNKNOWN);

		test.testMbgToModel();
		// antenna flag not managed by model: sounding from invalid antenna are invalidated
		test.withSoundingValidity(SoundingValidity.SND_UNVALID);
		test.testModelToMbg();
	}

	@Test
	public void testSwathAndAntennaInvalid() {
		MappingTest test = new MappingTest();

		test.withSoundingValidity(MbgValidityConstants.SoundingValidity.SND_UNVALID_ACQUIS);
		test.withAntennaFlag(MbgValidityConstants.ANT_UNVALID);
		test.withStatus(SonarDetectionStatus.INVALID_ACQUISITION).withStatus(SonarDetectionStatus.REJECTED);
		test.withDetails(SonarDetectionStatusDetail.UNKNOWN);

		test.testMbgToModel();
		test.testModelToMbg();
	}

	@Test
	public void testMultipleInvalid() {
		MappingTest test = new MappingTest();

		test.withSoundingValidity(SoundingValidity.SND_DOUBTFUL);
		test.withAntennaFlag(MbgValidityConstants.ANT_UNVALID);
		test.withPingFlag(MbgValidityConstants.PNG_UNVALID);
		test.withBeamFlag(MbgValidityConstants.BEA_UNVALID);

		test.withStatus(EnumSet.of(SonarDetectionStatus.REJECTED, SonarDetectionStatus.INVALID_SOUNDING_ROW,
				SonarDetectionStatus.INVALID_SWATH));
		test.withDetails(SonarDetectionStatusDetail.DOUBTFULL);

		test.testMbgToModel();
		test.testModelToMbg();
	}

}
