package fr.ifremer.globe.core.io.mbg.pattern;

import fr.ifremer.globe.utils.mbes.Ellipsoid;
import fr.ifremer.globe.utils.mbes.MbesUtils;

public class ProjectionMBES {
	public static double[] project(double lat, double across) {
		return project(lat, 0, across, 0, 0);
	}

	public static double[] project(double latNav, double longitudeNav, double across, double along, double headingNav) {

		double[] normRayon = MbesUtils.calcNormRayon(latNav, Ellipsoid.WGS84);
		double norm = normRayon[0];
		double rayon = normRayon[1];
		double sinHeading = Math.sin(Math.toRadians(headingNav));
		double cosHeading = Math.cos(Math.toRadians(headingNav));

		double latlongNE[] = MbesUtils.acrossAlongToLatitudeLongitude(longitudeNav, latNav, along, across, rayon, sinHeading, cosHeading, norm);
		return latlongNE;
	}
}
