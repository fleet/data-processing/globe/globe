/**
 *
 */
package fr.ifremer.globe.core.io.mbg.convert;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Test class for MbgDriver. Ici l'objet est de comparer 2 fichiers de type mbg et de vérifier les écarts attendus entre
 * chacun des fichiers
 */
public class S7K_7111_MBGTestConverter {
	private static final Logger LOGGER = LoggerFactory.getLogger(S7K_7111_MBGTestConverter.class);

	public static final String TEST_S7K_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/20110929_104041_PP-7111-100kHz-.s7k";
	public static final String TEST_GLOBE_S7K_MBG_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/20110929_104041_PP-7111-100kHz-GLOBE.mbg";
	public static final String TEST_CARAIBES_S7K_MBG_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/CARAIBES/20110929_104041_PP-7111-100kHz-CIB.mbg";

	/**
	 * Main class for the test, will read the file, load it into model and check every variables in memory to ensure
	 * that they are equals
	 * @throws IOException 
	 * @throws NCException 
	 */
	@Test
	public void testInitData() throws GIOException, IOException, NCException {
		MbgConverterParameters parameters = new MbgConverterParameters(TEST_S7K_FILE, TEST_GLOBE_S7K_MBG_FILE);
		parameters.setShipName("PP");
		parameters.setSurveyName("DD");
		parameters.setReference("GG");
		parameters.setCdi("kkkkk");

		try {
			// parse the file into a mbg file in the output dir
			new SounderFileConverter().convertToMbg(parameters);
		} catch (Exception e) {
			File outputFile = new File(TEST_GLOBE_S7K_MBG_FILE);
			if (outputFile != null) {
				outputFile.delete();
			}
			e.printStackTrace();
		}

		// Check if  files exists

		Assert.assertTrue(new File (TEST_GLOBE_S7K_MBG_FILE).exists());

		Assert.assertTrue(new File(TEST_CARAIBES_S7K_MBG_FILE).exists());

		StringBuilder resume = new StringBuilder("MbgConverter result : \n");
		boolean testFailed = false;

		NetcdfComparatorHook hook = buildHook();
		Optional<String> errors = NetcdfComparator.compareFiles(TEST_CARAIBES_S7K_MBG_FILE, TEST_GLOBE_S7K_MBG_FILE, Optional.of(hook));
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(TEST_GLOBE_S7K_MBG_FILE + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(TEST_GLOBE_S7K_MBG_FILE + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(testFailed);

	}
	
	private NetcdfComparatorHook buildHook() {
		
		NetcdfComparatorHook hook = new NetcdfComparatorHook();
		
		hook.ignoreAttributeForAllVariables("_FillValue"); 

		hook.ignoreGlobalAttribute(MbgConstants.Name);
		hook.ignoreGlobalAttribute(MbgConstants.InstallParameters); // "Variable not filled with CARAIBES"
		hook.ignoreGlobalAttribute(MbgConstants.mbTxAntennaLeverArm); 


		// No comparison because offsets with CARAIBES > à 10e-15
		// better in GLOBE normally because extreme distances no coded before calculation of the cartographic limit
		// positions
		hook.setToleranceForAttribute(MbgConstants.NorthLatitude, 0.000005);
		hook.setToleranceForAttribute(MbgConstants.SouthLatitude, 0.000037);
		hook.setToleranceForAttribute(MbgConstants.WestLongitude, 0.000005);
		hook.setToleranceForAttribute(MbgConstants.EastLongitude, 0.000052);
		hook.setToleranceForAttribute(MbgConstants.EllipsoidE2, 1.12E-16);
		hook.setToleranceForAttribute(MbgConstants.EllipsoidInvF, 1.12E-16);		
		hook.setToleranceForAttribute(MbgConstants.MinDepth, 0.0035);
		hook.setToleranceForAttribute(MbgConstants.MaxDepth, 0.0051);
		
		hook.setToleranceForVariable(MbgConstants.AlongDistance, 1.0); // Apply MbDistanceScale to get distance in meters
		hook.setToleranceForVariable(MbgConstants.AcrossDistance, 1.0); // Apply MbDistanceScale to get distance in meters
		hook.setToleranceForVariable(MbgConstants.Range, 1.1E-6);

		hook.ignoreVariable("mbHistCode");
		hook.ignoreVariable("mbHistDate");
		hook.ignoreVariable("mbHistAutor");
		hook.ignoreVariable("mbHistTime");
		hook.ignoreVariable("mbHistModule");
		hook.ignoreVariable("mbHistComment");
		hook.ignoreVariable("mbVelProfilRef"); // not used in CARAIBES
		hook.ignoreVariable("mbVelProfilIdx"); // "
		hook.ignoreVariable("mbVelProfilDate"); // "
		hook.ignoreVariable("mbVelProfilTime"); // "

		// FIXME 24/07/20 : this parameters are not managed by the new converter : TODO!
		hook.ignoreAttribute("mbShip", null);
		hook.ignoreAttribute("mbSurvey", null);
		hook.ignoreAttribute("mbReference", null);
		hook.ignoreAttribute("mbCDI", null);
		
		// bug fixed in installation parameter reading ('Z' was not read with the correct sign) 
		hook.ignoreVariable(MbgConstants.Immersion);
		
		return hook;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		FileUtils.deleteQuietly(new File(TEST_GLOBE_S7K_MBG_FILE));
	}
}
