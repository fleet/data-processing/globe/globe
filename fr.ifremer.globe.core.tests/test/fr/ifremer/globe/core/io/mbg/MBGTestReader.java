/**
 *
 */
package fr.ifremer.globe.core.io.mbg;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.common.mbg.MbgConstants;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Test class for MBGDriver. The aim of this class is to validate the reading
 * and filling of model via an MBG Driver To do this, a MBG File is read and
 * every field is checked with regards to the expected values (which are
 * obtained either from caraibe or from a netcfd reader)
 *
 */
@SuppressWarnings("unused")
public class MBGTestReader {
	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(MBGTestReader.class);

	public static final String TEST_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/Kongsberg/CARAIBES/0184_20150530_120935_thalia-CIB.mbg";
	public static final String BEAMS_CARAIBES_MBG = GlobeTestUtil.getTestDataPath()
			+ "/file/kongsberg/CARAIBES/0184_20150530_120935_thalia-CIB.csv";

	private static final double DELTA1 = 1e-7;
	private static final double DELTA2 = 0.0001;

	static NetcdfFile netcdfFile;

	static MbgInfo store;
	StringBuilder diffStringBuilder = new StringBuilder();

	String stringFormat = "Attribute '%s' has different value: %s vs %s\n";

	boolean testFailed;

	/**
	 * Main class for the test, will read the file, load it into model and check
	 * every variables in memory to ensure that they are equals
	 *
	 * @throws GIOException
	 * @throws NCException
	 */
	@Before
	public void setUp() throws GIOException, NCException {
		File f = new File(TEST_FILE);

		// Check if file exists
		Assert.assertTrue(f.exists());

		netcdfFile = NetcdfFile.open(TEST_FILE, Mode.readonly);

		// create the store, only global variables are in memory at that time
		store = (MbgInfo) IFileService.grab().getFileInfo(f.getAbsolutePath()).orElse(null);
		Assert.assertNotNull(store);

		// check if we can read the file
		Assert.assertTrue(store instanceof MbgInfo);

	}

	@Test
	public void testDimension() throws NCException {
		// check dimensions
		runDimensionTest();
		boolean testFailed = false;
		StringBuilder resume = new StringBuilder();
		if (diffStringBuilder.length() > 0) {
			testFailed = true;
			resume.append("Test Mbg Reader (dimensions)" + " : FAILED \n");
			resume.append(diffStringBuilder + "\n");
		} else {
			resume.append("Test Mbg Reader (dimensions)" + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(testFailed);
	}

	/**
	 * Réalise les tests sur les dimensions
	 * 
	 * @throws NCException
	 */
	private void runDimensionTest() throws NCException {
		// check Dimension
		NCDimension dim = netcdfFile.getDimension(MbgConstants.HISTORY_REC_NBR);
		CheckValue(MbgConstants.HISTORY_REC_NBR, Long.toString(dim.getLength()),
				Integer.toString(MbgConstants.HistoryRecNbrValue));

		// Don't check NameLength : could change with version changes
		// dim = netcdfFile.getDimension(MbgConstants.NameLength);
		// CheckValue(MbgConstants.NameLength, Long.toString(dim.getLength()),
		// Integer.toString(MbgConstants.NameLengthValue)); // name length has been
		// increased

		dim = netcdfFile.getDimension(MbgConstants.CommentLength);
		CheckValue(MbgConstants.CommentLength, Long.toString(dim.getLength()),
				Integer.toString(MbgConstants.CommentLengthValue));

		dim = netcdfFile.getDimension(MbgConstants.CYCLE_NUMBER);
		CheckValue(MbgConstants.CYCLE_NUMBER, Long.toString(dim.getLength()), Integer.toString(store.getCycleCount()));

		dim = netcdfFile.getDimension(MbgConstants.BEAM_NUMBER);
		CheckValue(MbgConstants.BEAM_NUMBER, Long.toString(dim.getLength()),
				Integer.toString(store.getTotalRxBeamCount()));

		dim = netcdfFile.getDimension(MbgConstants.ANTENNA_NUMBER);
		CheckValue(MbgConstants.ANTENNA_NUMBER, Long.toString(dim.getLength()),
				Integer.toString(store.getRxParameters().size()));

		dim = netcdfFile.getDimension(MbgConstants.VELOCITY_PROFILE_NUMBER);
		CheckValue(MbgConstants.VELOCITY_PROFILE_NUMBER, Long.toString(dim.getLength()),
				Integer.toString(store.getVelocityProfileCount()));

	}

	@Test
	public void testGlobalAttributes() throws NCException {
		// check global variable
		runGlobalTest();
		boolean testFailed = false;
		StringBuilder resume = new StringBuilder();
		if (diffStringBuilder.length() > 0) {
			testFailed = true;
			resume.append("Test Mbg Reader (global variables)" + " : FAILED \n");
			resume.append(diffStringBuilder + "\n");
		} else {
			resume.append("Test Mbg Reader (global variables)" + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(testFailed);
	}

	/**
	 * Réalise les tests sur les variables globales
	 * 
	 * @throws NCException
	 */
	private void runGlobalTest() throws NCException {
		// check

		short shortValue = netcdfFile.getAttributeShort(MbgConstants.Version);
		CheckValue(MbgConstants.Version, Short.toString(shortValue), Integer.toString(store.getVersion()));

		String stringValue = netcdfFile.getAttributeText(MbgConstants.Name);
		CheckValue(MbgConstants.Name, stringValue, store.getMbgName());

		stringValue = netcdfFile.getAttributeText(MbgConstants.Classe);
		CheckValue(MbgConstants.Classe, stringValue, store.getMbgClass());

		shortValue = netcdfFile.getAttributeShort(MbgConstants.Level);
		CheckValue(MbgConstants.Level, Short.toString(shortValue), Short.toString(store.getLevel()));

		shortValue = netcdfFile.getAttributeShort(MbgConstants.NbrHistoryRec);
		CheckValue(MbgConstants.NbrHistoryRec, Short.toString(shortValue), Integer.toString(store.getNbHistoryRec()));

		stringValue = netcdfFile.getAttributeText(MbgConstants.TimeReference);
		CheckValue(MbgConstants.TimeReference, stringValue, store.getTimeReference());

		int intValue = netcdfFile.getAttributeInt(MbgConstants.StartDate);
		int intValue2 = netcdfFile.getAttributeInt(MbgConstants.StartTime);
		Date date = DateUtils.getDateFromJulian(intValue, intValue2);
		DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss.SSS");
		String strDate = dateFormat.format(date);
		String strDateStore = dateFormat.format(store.getFirstPingDate());
		Assert.assertEquals(date, store.getFirstPingDate());
		CheckValue(MbgConstants.StartDate + "/" + MbgConstants.StartTime, strDate, strDateStore);

		intValue = netcdfFile.getAttributeInt(MbgConstants.EndDate);
		intValue2 = netcdfFile.getAttributeInt(MbgConstants.EndTime);
		date = DateUtils.getDateFromJulian(intValue, intValue2);
		strDate = dateFormat.format(date);
		strDateStore = dateFormat.format(store.getLastPingDate());
		Assert.assertEquals(date, store.getLastPingDate());
		CheckValue(MbgConstants.EndDate + "/" + MbgConstants.EndTime, strDate, strDateStore);

		double doubleValue = netcdfFile.getAttributeDouble(MbgConstants.NorthLatitude);
		CheckDoubleValue(MbgConstants.NorthLatitude, doubleValue, store.getGeoBox().getTop(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.SouthLatitude);
		CheckDoubleValue(MbgConstants.SouthLatitude, doubleValue, store.getGeoBox().getBottom(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.EastLongitude);
		CheckDoubleValue(MbgConstants.EastLongitude, doubleValue, store.getGeoBox().getRight(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.WestLongitude);
		CheckDoubleValue(MbgConstants.WestLongitude, doubleValue, store.getGeoBox().getLeft(), DELTA1);

		stringValue = netcdfFile.getAttributeText(MbgConstants.Meridian180);
		CheckValue(MbgConstants.Meridian180, stringValue, store.getMeridien180());

		stringValue = netcdfFile.getAttributeText(MbgConstants.GeodesicSystem);
		CheckValue(MbgConstants.GeodesicSystem, stringValue, store.getGeodesicSystem());

		stringValue = netcdfFile.getAttributeText(MbgConstants.EllipsoidName).trim();
		;
		CheckValue(MbgConstants.EllipsoidName, stringValue, store.getEllipsoidName());

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.EllipsoidA);
		CheckDoubleValue(MbgConstants.EllipsoidA, doubleValue, store.getEllipsoidA(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.EllipsoidInvF);
		CheckDoubleValue(MbgConstants.EllipsoidInvF, doubleValue, store.getEllipsoidFlatness(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.EllipsoidE2);
		Assert.assertEquals(doubleValue, store.getEllipsoidE2(), DELTA1);
		CheckDoubleValue(MbgConstants.EllipsoidE2, doubleValue, store.getEllipsoidE2(), DELTA1);

		shortValue = netcdfFile.getAttributeShort(MbgConstants.ProjType);
		CheckValue(MbgConstants.ProjType, Short.toString(shortValue), Short.toString(store.getProjType()));

		double[] arrayDouble = netcdfFile.getAttributeDoubleArray(MbgConstants.ProjParameterValue);
		for (int i = 0; i < arrayDouble.length; i++) {
			CheckDoubleValue(MbgConstants.ProjParameterValue + "[" + Integer.toString(i) + "]", arrayDouble[i],
					store.getProjParameterValue()[i], DELTA1);
		}

		stringValue = netcdfFile.getAttributeText(MbgConstants.ProjParameterCode);
		CheckValue(MbgConstants.ProjParameterCode, stringValue, store.getProjParameterCode());

		shortValue = netcdfFile.getAttributeShort(MbgConstants.Sounder);
		CheckValue(MbgConstants.Sounder, Short.toString(shortValue), Short.toString(store.getEchoSounderIndex()));

		shortValue = netcdfFile.getAttributeShort(MbgConstants.SerialNumber);
		CheckValue(MbgConstants.SerialNumber, Short.toString(shortValue), store.getTxParameters().antennaID);

		stringValue = netcdfFile.getAttributeText(MbgConstants.Ship);
		CheckValue(MbgConstants.Ship, stringValue, store.getShipName());

		stringValue = netcdfFile.getAttributeText(MbgConstants.Survey);
		CheckValue(MbgConstants.Survey, stringValue, store.getSurveyname());

		stringValue = netcdfFile.getAttributeText(MbgConstants.Reference);
		CheckValue(MbgConstants.Reference, stringValue, store.getReferencePoint());

		stringValue = netcdfFile.getAttributeText(MbgConstants.CDI);
		CheckValue(MbgConstants.CDI, stringValue, store.getCdi());

		arrayDouble = netcdfFile.getAttributeDoubleArray(MbgConstants.AntennaOffset);
		CheckDoubleValue(MbgConstants.AntennaOffset + "[0]", arrayDouble[0], store.getShiftAlongNavigation(), DELTA1);
		CheckDoubleValue(MbgConstants.AntennaOffset + "[1]", arrayDouble[0], store.getShiftAcrossNavigation(), DELTA1);
		CheckDoubleValue(MbgConstants.AntennaOffset + "[2]", arrayDouble[0], store.getShiftVerticalNavigation(),
				DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.AntennaDelay);
		CheckDoubleValue(MbgConstants.AntennaDelay, doubleValue, store.getAntennaDelay(), DELTA1);

		arrayDouble = netcdfFile.getAttributeDoubleArray(MbgConstants.SounderOffset);
		for (int i = 0; i < arrayDouble.length; i++) {
			CheckDoubleValue(MbgConstants.SounderOffset + "[" + Integer.toString(i) + "]", arrayDouble[i],
					store.getSounderOffset()[i], DELTA1);
		}

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.SounderDelay);
		CheckDoubleValue(MbgConstants.SounderDelay, doubleValue, store.getAntennaDelay(), DELTA1);

		arrayDouble = netcdfFile.getAttributeDoubleArray(MbgConstants.VRUOffset);
		for (int i = 0; i < arrayDouble.length; i++) {
			CheckDoubleValue(MbgConstants.VRUOffset + "[" + Integer.toString(i) + "]", arrayDouble[i],
					store.getVRUOffset()[i], DELTA1);
		}

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.VRUDelay);
		CheckDoubleValue(MbgConstants.VRUDelay, doubleValue, store.getAntennaDelay(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.HeadingBias);
		CheckDoubleValue(MbgConstants.HeadingBias, doubleValue, store.getHeadingBias(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.RollBias);
		CheckDoubleValue(MbgConstants.RollBias, doubleValue, store.getRollBias(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.PitchBias);
		CheckDoubleValue(MbgConstants.PitchBias, doubleValue, store.getPitchBias(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.HeaveBias);
		CheckDoubleValue(MbgConstants.HeaveBias, doubleValue, store.getHeaveBias(), DELTA1);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.Draught);
		CheckDoubleValue(MbgConstants.Draught, doubleValue, store.getDraught(), DELTA1);

		shortValue = netcdfFile.getAttributeShort(MbgConstants.NavType);
		CheckValue(MbgConstants.NavType, Short.toString(shortValue), Short.toString(store.getNavType()));

		stringValue = netcdfFile.getAttributeText(MbgConstants.NavRef);
		CheckValue(MbgConstants.NavRef, stringValue, store.getNavRef());

		shortValue = netcdfFile.getAttributeShort(MbgConstants.TideType);
		CheckValue(MbgConstants.TideType, Short.toString(shortValue), Short.toString(store.getTideType()));

		stringValue = netcdfFile.getAttributeText(MbgConstants.TideRef);
		CheckValue(MbgConstants.TideRef, stringValue, store.getTideRef());

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.MinDepth);
		CheckDoubleValue(MbgConstants.MinDepth, doubleValue, -store.getMinDepth(), DELTA2);

		doubleValue = netcdfFile.getAttributeDouble(MbgConstants.MaxDepth);
		CheckDoubleValue(MbgConstants.MaxDepth, doubleValue, -store.getMaxDepth(), DELTA2);

		intValue = netcdfFile.getAttributeInt(MbgConstants.CycleCounter);
		CheckValue(MbgConstants.CycleCounter, Integer.toString(intValue), Integer.toString(store.getCycleCount()));

		stringValue = netcdfFile.getAttributeText(MbgConstants.InstallParameters).trim();
		CheckValue(MbgConstants.InstallParameters, stringValue, store.getRawInstallParameters());

	}

	/*
	 * @Test public void testVariables() throws GIOException {
	 * Assert.assertTrue(store.getTotalRxBeamCount() > 0);
	 * 
	 * // Build the loading key ILoadingKey loadingKey =
	 * LoadingKeyFactory.generateFullAttributesLoadingKey(store,
	 * ELoadingType.BATHY_AND_IMAGING);
	 * 
	 * // load the data
	 * Activator.getCycleContainerRegistry().bookCycleContainer(store, this);
	 * CycleContainer cycles = (CycleContainer)
	 * store.getDriver().loadDataStore(loadingKey, store, this);
	 * 
	 * // check cycles and beams runCycleVariablesTest(cycles);
	 * System.out.println("Reading CARAIBES file: end of the test on variables");
	 * 
	 * cycles.removeLoadingKey(loadingKey);
	 * Activator.getCycleContainerRegistry().dispose(store, this); }
	 */

	/**
	 * Réalise les tests sur le contenu des variables
	 *
	 * @throws GIOException
	 */
	// private void runCycleVariablesTest(CycleContainer cycles) throws GIOException
	// {
	//
	// class AntennaStructure {
	// AntennaStructure(int antennaIdx) {
	// RxAntenna antenna = cycles.getRxAntennas().getAntenna(antennaIdx);
	//
	// filePingNumber =
	// antenna.getStorage(RxAntennaMandatoryAttributes.filePingNumber);
	// date = antenna.getStorage(RxAntennaMandatoryAttributes.date);
	// CFlag = antenna.getStorage(RxAntennaMandatoryAttributes.cycleValidity);
	//
	// heading = antenna.getStorage(RxAntennaMandatoryAttributes.heading); //
	// Heading array
	// roll = antenna.getStorage(RxAntennaOptionalAttributes.roll); // Roll array
	// pitch = antenna.getStorage(RxAntennaOptionalAttributes.pitch); // Pitch array
	// heave = antenna.getStorage(RxAntennaOptionalAttributes.heave); // Heave array
	// immersion = antenna.getStorage(RxAntennaOptionalAttributes.referenceDepth);
	// // Immersion array
	// tide = antenna.getStorage(RxAntennaOptionalAttributes.tide); // Tide array
	// vertDepth = antenna.getStorage(RxAntennaOptionalAttributes.verticalDepth); //
	// VerticalDepth array
	// dyDraught = antenna.getStorage(RxAntennaOptionalAttributes.dynamicDraught);
	// // DyDraught array
	// shipLongitude =
	// antenna.getStorage(RxAntennaMandatoryAttributes.shipLongitude); // ping
	// longitude array
	// shipLatitude = antenna.getStorage(RxAntennaMandatoryAttributes.shipLatitude);
	// // ping latitude array
	// }
	//
	// protected LongDeclaredAttributeArray filePingNumber; // Date des pings
	// protected LongDeclaredAttributeArray date; // Date des pings
	// protected ByteDeclaredAttributeArray CFlag; // Flags des pings
	// protected DoubleDeclaredAttributeArray heading; // Cap des pings
	// protected DoubleDeclaredAttributeArray roll; // Roulis des pings
	// protected DoubleDeclaredAttributeArray pitch; // Tangage des pings
	// protected DoubleDeclaredAttributeArray heave; // Pilonnement des pings
	// protected DoubleDeclaredAttributeArray immersion; // Immersion des pings
	// protected DoubleDeclaredAttributeArray tide; // Tide des pings
	// protected DoubleDeclaredAttributeArray vertDepth; // Vertical depth des pings
	// protected DoubleDeclaredAttributeArray dyDraught; // Dynamic Draught des
	// pings
	// protected DoubleDeclaredAttributeArray shipLongitude; // Longitude des pings
	// protected DoubleDeclaredAttributeArray shipLatitude; // Latitude des pings
	// }
	// AntennaStructure[] antennas;
	// // read csv file
	//
	// BeamsCSVParser beamsCSV = new BeamsCSVParser();
	// beamsCSV.parseFile(BEAMS_CARAIBES_MBG);
	// long filePingNumber;
	// long date;
	// byte CFlag = 0;
	// double heading;
	// double roll;
	// double pitch;
	// double heave;
	// double immersion;
	// double tide;
	// double vertDepth;
	// double dyDraught;
	// double longitude;
	// double latitude;
	// double delta;
	// String string1;
	// String string2;
	// String string3;
	// double depth;
	// float alongDist;
	// double acrossDist;
	// float azimutAngle;
	// float acrossAngle;
	// double range;
	// float qualityFactor;
	// double reflectivity;
	// byte sFlag;
	// boolean ampPhase;
	// int previousPingId = -1;
	// int previousAntennaId = -1;
	// final SimpleDateFormat formatter = new
	// SimpleDateFormat("dd/MM/yyHH:mm:ss.SSS");
	// formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
	// long dateCIB;
	// int intAmpPhase;
	//
	// antennas = new AntennaStructure[cycles.getRxAntennas().getAntennaCount()];
	// for (int i = 0; i < cycles.getRxAntennas().getAntennaCount(); i++) {
	// antennas[i] = new AntennaStructure(i);
	// }
	// ObjectArrayList rangeArrays = new ObjectArrayList();
	// DoubleDeclaredAttributeArray rangeArray;
	//
	// for (int i = 0; i < cycles.getRxAntennas().getBeamCount(); i++) {
	// rangeArray =
	// cycles.getRxAntennas().getBeam(i).getStorage(RxBeamOptionalAttributes.twoWayTravelTime);
	// rangeArrays.add(rangeArray);
	// }
	// for (Record csvRecord : beamsCSV.getListOfRecord()) {
	//
	// string1 = "differs (tolerance=" + DELTA1 + "): cycle " + csvRecord.pingId + "
	// beam " + csvRecord.beamIndex;
	// string2 = "differs (tolerance=" + DELTA2 + "): cycle " + csvRecord.pingId + "
	// beam " + csvRecord.beamIndex;
	// string3 = "differs (tolerance=" + DELTA2 + "): cycle " + csvRecord.pingId + "
	// antenna "
	// + csvRecord.antennaId;
	//
	// // check the variables headers of pings.
	// if (previousPingId != csvRecord.pingId || previousAntennaId !=
	// csvRecord.antennaId) {
	// filePingNumber =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].filePingNumber);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + "
	// filePingNumber "
	// + (int) filePingNumber + " filePingNumber CIB " + csvRecord.filePingNumber);
	// Assert.assertEquals(
	// "File ping number differs: cycle " + csvRecord.pingId + " antenna " +
	// csvRecord.antennaId,
	// csvRecord.filePingNumber, filePingNumber);
	//
	// date =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].date);
	// try {
	// dateCIB = formatter.parse(csvRecord.Date + csvRecord.Hour).getTime();
	// } catch (ParseException e) {
	// throw new GIOException("Error parse date", e);
	// }
	// Assert.assertEquals("date differs: cycle " + csvRecord.pingId + " antenna " +
	// csvRecord.antennaId,
	// dateCIB, date);
	//
	// CFlag =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].CFlag);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + " CFlag "
	// + (int) CFlag
	// + " CFlag CIB " + csvRecord.CFlag);
	// Assert.assertEquals("cycle flag differs: cycle " + csvRecord.pingId + "
	// antenna " + csvRecord.antennaId,
	// csvRecord.CFlag, CFlag);
	//
	// heading =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].heading);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + " heading
	// " + heading
	// + " heading CIB " + csvRecord.heading);
	// Assert.assertEquals("cap " + string3, csvRecord.heading, heading, DELTA2);
	//
	// roll =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].roll);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + " roll " +
	// roll + " roll CIB "
	// + csvRecord.roll);
	// Assert.assertEquals("roll " + string3, csvRecord.roll, roll, DELTA2);
	//
	// pitch =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].pitch);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + " pitch "
	// + pitch + " pitch CIB "
	// + csvRecord.pitch);
	// Assert.assertEquals("pitch " + string3, csvRecord.pitch, pitch, DELTA2);
	//
	// heave =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].heave);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + " heave "
	// + heave + " heave CIB "
	// + csvRecord.heave);
	// Assert.assertEquals("heave " + string3, csvRecord.heave, heave, DELTA2);
	//
	// immersion =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].immersion);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + "
	// immersion " + immersion
	// + " immersion CIB " + csvRecord.immersion);
	// Assert.assertEquals("immersion " + string3, csvRecord.immersion, immersion,
	// DELTA2);
	//
	// tide =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].tide);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + " tide " +
	// tide + " tide CIB "
	// + csvRecord.tide);
	// Assert.assertEquals("tide " + string3, csvRecord.tide, tide, DELTA2);
	//
	// vertDepth =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].vertDepth);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + "
	// vertDepth " + vertDepth
	// + " vertDepth CIB " + csvRecord.vertDepth);
	// Assert.assertEquals("vert depth " + string3, csvRecord.vertDepth, vertDepth,
	// DELTA2);
	//
	// dyDraught =
	// cycles.getRxAntennas().getAntenna(csvRecord.antennaId).getValue(csvRecord.pingId,
	// antennas[csvRecord.antennaId].dyDraught);
	// System.out.println(csvRecord.pingId + "/ " + csvRecord.antennaId + "
	// diDraught " + dyDraught
	// + " dyDraught CIB " + csvRecord.dyDraught);
	// Assert.assertEquals("dynamic draught " + string3, csvRecord.dyDraught,
	// dyDraught, DELTA2);
	//
	// previousPingId = csvRecord.pingId;
	// previousAntennaId = csvRecord.antennaId;
	//
	// }
	// longitude =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getLongitude(csvRecord.pingId);
	// Assert.assertEquals("longitude " + string1, csvRecord.longitude, longitude,
	// DELTA1);
	//
	// latitude =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getLatitude(csvRecord.pingId);
	// Assert.assertEquals("latitude " + string1, csvRecord.latitude, latitude,
	// DELTA1);
	//
	// depth =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getDepth(csvRecord.pingId);
	// Assert.assertEquals("depth " + string2, csvRecord.depth, depth, DELTA2);
	//
	// alongDist =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getAlongDistance(csvRecord.pingId);
	// Assert.assertEquals("along Distance " + string2, csvRecord.alongDist,
	// alongDist, DELTA2);
	//
	// acrossDist =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getAcrossDistance(csvRecord.pingId);
	// Assert.assertEquals("across Distance " + string2, csvRecord.acrossDist,
	// acrossDist, DELTA2);
	//
	// azimutAngle =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getAzimutAngle(csvRecord.pingId);
	// Assert.assertEquals("azimut angle " + string2, csvRecord.azimutAngle,
	// azimutAngle, DELTA2);
	//
	// acrossAngle =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getAcrossAngle(csvRecord.pingId);
	// Assert.assertEquals("across angle " + string2, csvRecord.acrossAngle,
	// acrossAngle, DELTA2);
	//
	// // DoubleDeclaredAttributeArray rangeArray =
	// //
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getStorage(RxBeamOptionalAttributes.twoWayTravelTime);
	// range =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getValue(csvRecord.pingId,
	// (DoubleDeclaredAttributeArray) rangeArrays.getQuick(csvRecord.beamIndex));
	// Assert.assertEquals("range " + string2, csvRecord.range, range, DELTA1);
	//
	// sFlag =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getValidBathymetry(csvRecord.pingId).getValue();
	// Assert.assertEquals("Beam flag differs: cycle " + csvRecord.pingId + " beam "
	// + csvRecord.beamIndex,
	// csvRecord.sFlag, sFlag);
	//
	// ampPhase =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getAmplitudePhaseDetection(csvRecord.pingId);
	// intAmpPhase = (ampPhase ? 1 : 0); // true:amplitude; false:phase
	// Assert.assertEquals("Beam flag differs: cycle " + csvRecord.pingId + " beam "
	// + csvRecord.beamIndex,
	// csvRecord.ampPhase, intAmpPhase);
	//
	// qualityFactor = cycles.getRxAntennas().getBeam(csvRecord.beamIndex)
	// .getScalarQualityFactor(csvRecord.pingId);
	// Assert.assertEquals("quality Factor " + string2, csvRecord.qualityFactor,
	// qualityFactor, DELTA2);
	//
	// reflectivity =
	// cycles.getRxAntennas().getBeam(csvRecord.beamIndex).getReflectivity(csvRecord.pingId);
	// Assert.assertEquals("reflectivite " + string2, csvRecord.reflectivity,
	// reflectivity, DELTA2);
	//
	// }
	// }
	public void CheckValue(String label, String stringVal1, String stringVal2) {
		if (!stringVal1.contentEquals(stringVal2))
			diffStringBuilder.append(String.format(stringFormat, label, stringVal1, stringVal2));
	}

	public void CheckDoubleValue(String label, Double val1, double val2, double offset) {
		if (Math.abs(val1 - val2) > offset)
			diffStringBuilder.append(String.format(stringFormat, label, Double.toString(val1), Double.toString(val2)));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		netcdfFile.close();
		// store.getOldFile().close();
	}
}
