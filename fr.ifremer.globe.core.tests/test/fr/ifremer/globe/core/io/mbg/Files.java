package fr.ifremer.globe.core.io.mbg;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class Files {
	public static String MBG_GLOBE = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/CARAIBES/20130322_204233_PP-7150-24kHz-CIB.mbg";
	public static String POS_CARAIBES_MAILLA = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/CARAIBES/PositionCaraibes 20130322_204233_PP-7150-24kHz-CIB.mbg.csv";
	public static String POS_CARAIBES_IMPORT = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/CARAIBES/PositionCaraibes 20130322_204233_PP-7150-24kHz-CIB.import.csv";
	public static String MBG_PATTERN = GlobeTestUtil.getTestDataPath() + "/file/generated/fullPattern.mbg";
}
