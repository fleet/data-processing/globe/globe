package fr.ifremer.globe.core.io.mbg.projection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.globe.core.io.mbg.Files;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.io.mbg.projection.PositionCSVParser.Record;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
//import fr.ifremer.globe.test.Activator;
import fr.ifremer.globe.utils.exception.GIOException;

public class TestProjection implements IDataContainerOwner {

	static double DELTA = 1e-7;
	PositionCSVParser positionsMailla = null;
	PositionCSVParser positionsImport = null;

	@Before
	public void setUp() throws Exception {

		// read csv file
		positionsMailla = new PositionCSVParser();
		positionsMailla.parseFile(Files.POS_CARAIBES_MAILLA);

		// read csv file
		positionsImport = new PositionCSVParser();
		positionsImport.parseFile(Files.POS_CARAIBES_IMPORT);

		// for POS_CARAIBES_IMPORT beam a identified from 1 to nbBeam, adapt
		// them to the format expected (beam index and ping number (index+1))
		for (Record rec : positionsImport.getListOfRecord()) {
			rec.beamIndex -= 1;
		}
		System.out.println(" (tolerance=" + DELTA + "/~" + DELTA * 60 * 1852.0 + "m)");
	}

	/**
	 * Test differences between caraibes projection one extracted from mailla, the
	 * other extracted from caraibes imports
	 *
	 * @throws GIOException
	 */
	/*
	 * @Test public void testCaraibesProjection() { //compute difference between
	 * CARAIBES modules
	 *
	 * //now we got data from caraibes, check if projection in globe match
	 *
	 * DeltaCumulator latCumul=new DeltaCumulator(); DeltaCumulator longCumul=new
	 * DeltaCumulator();
	 *
	 * for(Record csvRecordImport : positionsImport.getListOfRecord()) { //find
	 * matching record in positionsMailla for(Record csvRecordMailla :
	 * positionsMailla.getListOfRecord()) {
	 * if(csvRecordImport.pingId==csvRecordMailla.pingId &&
	 * csvRecordImport.beamIndex==csvRecordMailla.beamIndex) { //found matching
	 * record double delta=Math.abs( csvRecordMailla.longitude-
	 * csvRecordImport.longitude); longCumul.addElement(delta);
	 * delta=Math.abs(csvRecordMailla.latitude- csvRecordImport.latitude);
	 * latCumul.addElement(delta); } } } System.err.println("longitude "+longCumul
	 * .toString()+" latitude "+latCumul.toString());
	 * Assert.assertTrue("longitude differs " +longCumul.toString()+" (tolerance="
	 * +DELTA+"/~"+DELTA*60*1852.0+"m)",longCumul.maxDelta<DELTA); Assert.assertTrue
	 * ("latitude differs "+latCumul.toString()+" (tolerance="+DELTA
	 * +"/~"+DELTA*60*1852.0+"m)",latCumul.maxDelta<DELTA);
	 *
	 *
	 * }
	 */

	@Test
	public void testProjectionMBG() throws GIOException {

		// now we got data from caraibes, check if projection in globe match
		// create the store, only globale variables are in memory at that time
		// load the mbg file in globe

		// create the store, only globale variables are in memory at that time
		MbgInfo store = (MbgInfo) IFileService.grab().getFileInfo(Files.MBG_GLOBE).orElse(null);
		Assert.assertNotNull(store);
		// check if we can read the file
		Assert.assertTrue("Cannot read file" + Files.MBG_GLOBE, store != null);
		Assert.assertTrue(store instanceof MbgInfo);

		try (ISounderDataContainerToken token = IDataContainerFactory.grab().book(store, this)) {
			testProjection(token.getDataContainer(), positionsImport);
		}
	}

	// NOT USED RIGHT NOW
	/*
	 * @Test public void testProjections7k() {
	 *
	 * //now we got data from caraibes, check if projection in globe match
	 *
	 *
	 * S7KDriver driver7=new S7KDriver();
	 *
	 * // create the store, only globale variables are in memory at that time
	 * IInfoStore store = driver7.initData(Files.S7K_GLOBE);
	 * Assert.assertNotNull(store); // check if we can read the file
	 * Assert.assertTrue("Cannot read file" + Files.S7K_GLOBE, store != null); //
	 * load the data CycleContainer cycles7= (CycleContainer)
	 * store.getDriver().loadDataStore(SounderNamedDataStore.Bathy, store);
	 * Assert.assertNotNull(cycles7); testProjection(cycles7,positionsImport); }
	 */

	class DeltaCumulator {
		// / private int elementCount;
		DeltaCumulator() {
			// this.elementCount=0;
			maxDelta = 0;
			minDelta = Double.MAX_VALUE;
			counter = 0;
		}

		double maxDelta = 0;
		double minDelta = Double.MAX_VALUE;
		int counter = 0;

		public void addElement(double value) {
			maxDelta = Math.max(maxDelta, value);
			minDelta = Math.min(minDelta, value);
			counter++;
		}

		@Override
		public String toString() {
			return "Max(" + maxDelta + ") Min(" + minDelta + ")";
		}
	}

	private void testProjection(SounderDataContainer container, PositionCSVParser ref) throws GIOException {

		// now we got data from caraibes, check if projection in globe match

		DeltaCumulator latCumul = new DeltaCumulator();
		DeltaCumulator longCumul = new DeltaCumulator();

		DoubleLoadableLayer2D longitudeLayer = container.getLayer(BathymetryLayers.DETECTION_LONGITUDE);
		DoubleLoadableLayer2D latitdueLayer = container.getLayer(BathymetryLayers.DETECTION_LATITUDE);

		for (Record csvRecord : ref.getListOfRecord()) {

			double longitude = longitudeLayer.get(csvRecord.pingId - 1, csvRecord.beamIndex);
			double latitude = latitdueLayer.get(csvRecord.pingId - 1, csvRecord.beamIndex);

			double delta = Math.abs(longitude - csvRecord.longitude);
			longCumul.addElement(delta);
			delta = Math.abs(latitude - csvRecord.latitude);
			latCumul.addElement(delta);
			// Assert.assertEquals("latitude differs (tolerance="+DELTA+")",latitude,
			// csvRecord.latitude, DELTA);

		}
		System.err.println("longitude " + longCumul.toString() + " latitude " + latCumul.toString());
		Assert.assertTrue("longitude differs " + longCumul.toString() + " (tolerance=" + DELTA + "/~"
				+ DELTA * 60 * 1852.0 + "m)", longCumul.maxDelta < DELTA);
		Assert.assertTrue(
				"latitude differs " + latCumul.toString() + " (tolerance=" + DELTA + "/~" + DELTA * 60 * 1852.0 + "m)",
				latCumul.maxDelta < DELTA);

	}
}
