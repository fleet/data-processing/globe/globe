package fr.ifremer.globe.core.io.mbg.correction.biasCorrection;

import java.io.File;
import java.util.OptionalInt;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class MBGTestBiasCorrectionOneAntenna extends MBGTestBiasCorrection {

	// Input reference file
	public static final String BIAS_CORRECTION_REF_INPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/refMBG/0372_20140827_195125_AT_EM122.mbg";

	/***Heading Absolute Correction***/
	// Output reference file
	public static final String HEADING_ABSOLUTE_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heading/oneAntenna/heading_absolute_A_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String HEADING_ABSOLUTE_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heading/oneAntenna/HeadingAbsoluteCorrection_A.mbg";
	// Correction reference file
	public static final String HEADING_ABSOLUTE_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heading/oneAntenna/HeadingCorrection-absolute.cor";

	/***Heading Bias Correction***/
	// Output reference file
	public static final String HEADING_BIAS_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heading/oneAntenna/heading_bias_A_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String HEADING_BIAS_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heading/oneAntenna/HeadingBiasCorrection_A.mbg";
	// Correction reference file
	public static final String HEADING_BIAS_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heading/oneAntenna/HeadingCorrection-bias.cor";

	/***Roll Correction***/
	/***Roll Absolute Correction***/
	// Output reference file
	public static final String ROLL_ABSOLUTE_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/roll_absolute_A_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String ROLL_ABSOLUTE_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/RollAbsoluteCorrection_A.mbg";
	// Correction reference file
	public static final String ROLL_ABSOLUTE_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/RollCorrection-absolute.cor";

	/***Roll Bias Correction***/
	// Output reference file
	public static final String ROLL_BIAS_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/roll_bias_A_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String ROLL_BIAS_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/RollBiasCorrection_A.mbg";
	// Correction reference file
	public static final String ROLL_BIAS_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/RollCorrection-bias.cor";

	/***Roll Bias Correction with cut***/
	// Output reference file
	public static final String ROLL_BIAS_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/roll_bias_A_cut2_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String ROLL_BIAS_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/RollBiasCutCorrection_A.mbg";
	// Correction reference file
	public static final String ROLL_BIAS_CUT_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/roll/oneAntenna/RollCorrection-bias.cor";
	// Cut reference file
	public static final String ROLL_BIAS_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/decoupe_2.cut";

	/***Pitch Correction***/
	/***Pitch Absolute Correction***/
	// Output reference file
	public static final String PITCH_ABSOLUTE_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/pitch_absolute_A_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String PITCH_ABSOLUTE_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/PitchAbsoluteCorrection_A.mbg";
	// Correction reference file
	public static final String PITCH_ABSOLUTE_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/PitchCorrection-absolute.cor";

	/***Pitch Bias Correction***/
	// Output reference file
	public static final String PITCH_BIAS_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/pitch_bias_A_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String PITCH_BIAS_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/PitchBiasCorrection_A.mbg";
	// Correction reference file
	public static final String PITCH_BIAS_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/PitchCorrection-bias.cor";

	/***Pitch Bias Correction with cut***/
	// Output reference file
	public static final String PITCH_BIAS_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/pitch_bias_A_cut2_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String PITCH_BIAS_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/PitchBiasCutCorrection_A.mbg";
	// Correction reference file
	public static final String PITCH_BIAS_CUT_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/PitchCorrection-bias.cor";
	// Cut reference file
	public static final String PITCH_BIAS_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/pitch/oneAntenna/decoupe_2.cut";

	/***Heave Correction***/
	/***Heave Bias Correction with cut***/
	// Output reference file
	public static final String HEAVE_BIAS_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heave/oneAntenna/heave_bias_A_cut1_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String HEAVE_BIAS_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heave/oneAntenna/HeaveBiasCutCorrection_A.mbg";
	// Correction reference file
	public static final String HEAVE_BIAS_CUT_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heave/oneAntenna/HeaveCorrection-bias.cor";
	// Cut reference file
	public static final String HEAVE_BIAS_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/heave/oneAntenna/decoupe_1.cut";

	/***Velocity Correction***/
	// Output reference file
	public static final String VELOCITY_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/velocity/oneAntenna/BiasVelocity_0372_20140827_195125_AT_EM122.mbg";
	// File to be processed
	public static final String VELOCITY_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/velocity/oneAntenna/VelocityCorrection_A.mbg";
	// Correction reference file
	public static final String VELOCITY_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/velocity/oneAntenna/VelocityCorrection-bias.cor";



	/***All Bias Correction***/

	//	/***Time Correction***/
	//	/***Time Bias Correction***/
	//	// Output reference file
	//	public static final String TIME_BIAS_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/time/oneAntenna/time_bias_A_0372_20140827_195125_AT_EM122.mbg";
	//	// File to be processed
	//	public static final String TIME_BIAS_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/time/oneAntenna/TimeBiasCorrection_A.mbg";
	//	// Correction reference file
	//	public static final String TIME_BIAS_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/time/oneAntenna/TimeCorrection-A.ttb";
	//
	//	/***Time Bias Correction with cut***/
	//	// Output reference file
	//	public static final String TIME_BIAS_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/time/oneAntenna/time_bias_A_cut1_0372_20140827_195125_AT_EM122.mbg";
	//	// File to be processed
	//	public static final String TIME_BIAS_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/time/oneAntenna/TimeBiasCutCorrection_A.mbg";
	//	// Correction reference file
	//	public static final String TIME_BIAS_CUT_CORRECTION_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/time/oneAntenna/TimeCorrection-A.ttb";
	//	// Cut reference file
	//	public static final String TIME_BIAS_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/biasCorrection/time/oneAntenna/decoupe_1.cut";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * Processes a test on heading correction in absolute mode.
	 */
	@Test
	public void testHeadingAbsoluteCorrection() throws Exception {
		testHeadingCorrection(BIAS_CORRECTION_REF_INPUT_FILE, HEADING_ABSOLUTE_CORRECTION_REF_OUTPUT_FILE, HEADING_ABSOLUTE_CORRECTION_TST_FILE, HEADING_ABSOLUTE_CORRECTION_FILE, java.util.OptionalInt.empty());
	}

	/**
	 * Processes a test on heading correction in bias mode.
	 */
	@Test
	public void testHeadingBiasCorrection() throws Exception {
		testHeadingCorrection(BIAS_CORRECTION_REF_INPUT_FILE, HEADING_BIAS_CORRECTION_REF_OUTPUT_FILE, HEADING_BIAS_CORRECTION_TST_FILE, HEADING_BIAS_CORRECTION_FILE, java.util.OptionalInt.empty());
	}
	

	/**
	 * Processes a test on roll correction in absolute mode.
	 */
	@Test
	public void testRollAbsoluteCorrection() throws Exception {
		testRollCorrection(BIAS_CORRECTION_REF_INPUT_FILE, ROLL_ABSOLUTE_CORRECTION_REF_OUTPUT_FILE, ROLL_ABSOLUTE_CORRECTION_TST_FILE, ROLL_ABSOLUTE_CORRECTION_FILE, null, OptionalInt.empty());
	}

	/**
	 * Processes a test on roll correction in bias mode.
	 */
	@Test
	public void testRollBiasCorrection() throws Exception {
		testRollCorrection(BIAS_CORRECTION_REF_INPUT_FILE, ROLL_BIAS_CORRECTION_REF_OUTPUT_FILE, ROLL_BIAS_CORRECTION_TST_FILE, ROLL_BIAS_CORRECTION_FILE, null, OptionalInt.empty());
	}

	/**
	 * Processes a test on roll correction in bias mode with a .cut file.
	 */
	@Test
	public void testRollBiasCutCorrection() throws Exception {
		testRollCorrection(BIAS_CORRECTION_REF_INPUT_FILE, ROLL_BIAS_CUT_CORRECTION_REF_OUTPUT_FILE, ROLL_BIAS_CORRECTION_TST_FILE, ROLL_BIAS_CORRECTION_FILE, ROLL_BIAS_CUT_CORRECTION_CUT_FILE, OptionalInt.empty());
	}

	/**
	 * Processes a test on pitch correction in absolute mode.
	 */
	@Test
	public void testPitchAbsoluteCorrection() throws Exception {
		testPitchCorrection(BIAS_CORRECTION_REF_INPUT_FILE, PITCH_ABSOLUTE_CORRECTION_REF_OUTPUT_FILE, PITCH_ABSOLUTE_CORRECTION_TST_FILE, PITCH_ABSOLUTE_CORRECTION_FILE, null, OptionalInt.empty());
	}

	/**
	 * Processes a test on pitch correction in bias mode.
	 */
	@Test
	public void testPitchBiasCorrection() throws Exception {
		testPitchCorrection(BIAS_CORRECTION_REF_INPUT_FILE, PITCH_BIAS_CORRECTION_REF_OUTPUT_FILE, PITCH_BIAS_CORRECTION_TST_FILE, PITCH_BIAS_CORRECTION_FILE, null, OptionalInt.empty());
	}

	/**
	 * Processes a test on pitch correction in bias mode with a .cut file.
	 */
	@Test
	public void testPitchBiasCutCorrection() throws Exception {
		testPitchCorrection(BIAS_CORRECTION_REF_INPUT_FILE, PITCH_BIAS_CUT_CORRECTION_REF_OUTPUT_FILE, PITCH_BIAS_CORRECTION_TST_FILE, PITCH_BIAS_CORRECTION_FILE, PITCH_BIAS_CUT_CORRECTION_CUT_FILE, OptionalInt.empty());
	}

	/**
	 * Processes a test on heave correction in bias mode with a .cut file.
	 */
	@Test
	public void testHeaveBiasCutCorrection() throws Exception {
		testHeaveCorrection(BIAS_CORRECTION_REF_INPUT_FILE, HEAVE_BIAS_CUT_CORRECTION_REF_OUTPUT_FILE, HEAVE_BIAS_CUT_CORRECTION_TST_FILE, HEAVE_BIAS_CUT_CORRECTION_FILE, HEAVE_BIAS_CUT_CORRECTION_CUT_FILE, OptionalInt.empty());
	}

	
	/**
	 * Processes a test on velocity correction.
	 */
	@Test
	public void testVelocityCorrection() throws Exception {
		testVelocityCorrection(BIAS_CORRECTION_REF_INPUT_FILE, VELOCITY_CORRECTION_REF_OUTPUT_FILE, VELOCITY_CORRECTION_TST_FILE, VELOCITY_CORRECTION_FILE, OptionalInt.empty());
	}
	

	//	/**
	//	 * Processes a test on time correction in bias mode.
	//	 */
	//	@Test
	//	public void testTimeBiasCorrection() throws Exception {
	//		testTimeBiasCorrection(BIAS_CORRECTION_REF_INPUT_FILE, TIME_BIAS_CORRECTION_REF_OUTPUT_FILE, TIME_BIAS_CORRECTION_TST_FILE, TIME_BIAS_CORRECTION_FILE, null);
	//	}

	//	/**
	//	 * Processes a test on time correction in bias mode with a .cut file.
	//	 */
	//	@Test
	//	public void testTimeBiasCutCorrection() throws Exception {
	//		testTimeBiasCorrection(BIAS_CORRECTION_REF_INPUT_FILE, TIME_BIAS_CORRECTION_REF_OUTPUT_FILE, TIME_BIAS_CORRECTION_TST_FILE, TIME_BIAS_CORRECTION_FILE, TIME_BIAS_CUT_CORRECTION_CUT_FILE);
	//	}


	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		FileUtils.deleteQuietly(new File(HEADING_ABSOLUTE_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(HEADING_BIAS_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(ROLL_ABSOLUTE_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(ROLL_BIAS_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(ROLL_BIAS_CUT_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(PITCH_ABSOLUTE_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(PITCH_BIAS_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(PITCH_BIAS_CUT_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(HEAVE_BIAS_CUT_CORRECTION_TST_FILE));
		//		FileUtils.deleteQuietly(new File(TIME_BIAS_CORRECTION_TST_FILE));
		//		FileUtils.deleteQuietly(new File(TIME_BIAS_CUT_CORRECTION_TST_FILE));

	}
}
