package fr.ifremer.globe.core.io.mbg.correction.biasCorrection;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.OptionalInt;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

/** Unitary tests of bias correction with an antenna filtering */
public class MBGTestBiasCorrectionAntennaFilter extends MBGTestBiasCorrection {

	// Input reference file
	private static final String BIAS_CORRECTION_REF_INPUT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/refMBG/0018_20120222_131427_ShipName.mbg";

	/*** Heading Bias Correction ***/
	// Output reference file
	private static final String HEADING_BIAS_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/heading/multiAntennas/heading_bias_A_0018_20120222_131427_ShipName.mbg";
	// File to be processed
	private static final String HEADING_BIAS_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/heading/multiAntennas/HeadingBiasCorrection_A.mbg";
	// Correction reference file
	private static final String HEADING_BIAS_CORRECTION_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/heading/multiAntennas/HeadingCorrection-bias.cor";


	/*** Roll Bias Correction ***/
	// Output reference file
	private static final String ROLL_BIAS_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/roll/multiAntennas/roll_bias_A_0018_20120222_131427_ShipName.mbg";
	// File to be processed
	private static final String ROLL_BIAS_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/roll/multiAntennas/RollBiasCorrection_A.mbg";
	// Correction reference file
	private static final String ROLL_BIAS_CORRECTION_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/roll/multiAntennas/RollCorrection-bias.cor";


	/*** Pitch Bias Correction ***/
	// Output reference file
	private static final String PITCH_BIAS_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/pitch/multiAntennas/pitch_bias_A_0018_20120222_131427_ShipName.mbg";
	// File to be processed
	private static final String PITCH_BIAS_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/pitch/multiAntennas/PitchBiasCorrection_A.mbg";
	// Correction reference file
	private static final String PITCH_BIAS_CORRECTION_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/pitch/multiAntennas/PitchCorrection-bias.cor";

	/*** Velocity Correction ***/
	// Output reference file
	private static final String VELOCITY_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/velocity/multiAntennas/BiasVelocity_0018_20120222_131427_ShipName.mbg";
	// File to be processed
	private static final String VELOCITY_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/velocity/multiAntennas/VelocityCorrection_A.mbg";
	// Correction reference file
	private static final String VELOCITY_CORRECTION_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/biasCorrection/velocity/multiAntennas/VelocityCorrection-bias.cor";

	/**
	 * Processes a test on heading correction in bias mode.
	 */
	@Test
	public void testHeadingBiasCorrection() throws Exception {
		// Heading does not depend on antenna. So expected result is the reference file
		testHeadingCorrection(BIAS_CORRECTION_REF_INPUT_FILE, HEADING_BIAS_CORRECTION_REF_OUTPUT_FILE,
				HEADING_BIAS_CORRECTION_TST_FILE, HEADING_BIAS_CORRECTION_FILE, java.util.OptionalInt.of(0));
	}

	/**
	 * Processes a test on roll correction in bias mode.
	 */

	@Test
	public void testRollBiasCorrection() throws Exception {
		// Roll correction changes variables Depth, AcrossDistance, AcrossAngle
		// Expecting error on them

		// Filtering on antenna 0
		// Differences must start at index 400 (ie, first index of antenna 1)
		try {
			testRollCorrection(BIAS_CORRECTION_REF_INPUT_FILE, ROLL_BIAS_CORRECTION_REF_OUTPUT_FILE,
					ROLL_BIAS_CORRECTION_TST_FILE, ROLL_BIAS_CORRECTION_FILE, null, OptionalInt.of(0));
		} catch (AssertionError e) {
			assertTrue(e.getMessage().contains("[/mbDepth] Data not equals for index [0:400]"));
			assertTrue(e.getMessage().contains("[/mbAcrossDistance] Data not equals for index [0:400]"));
			assertTrue(e.getMessage().contains("[/mbAcrossBeamAngle] Data not equals for index [0:400]"));
		}

		// Filtering on antenna 1
		// Differences must start at index 0 (ie, first index of antenna 0)
		try {
			testRollCorrection(BIAS_CORRECTION_REF_INPUT_FILE, ROLL_BIAS_CORRECTION_REF_OUTPUT_FILE,
					ROLL_BIAS_CORRECTION_TST_FILE, ROLL_BIAS_CORRECTION_FILE, null, OptionalInt.of(1));
		} catch (AssertionError e) {
			assertTrue(e.getMessage().contains("[/mbDepth] Data not equals for index [0:0]"));
			assertTrue(e.getMessage().contains("[/mbAcrossDistance] Data not equals for index [0:0]"));
			assertTrue(e.getMessage().contains("[/mbAcrossBeamAngle] Data not equals for index [0:0]"));
		}
	}

	/**
	 * Processes a test on pitch correction in bias mode.
	 */
	@Test
	public void testPitchBiasCorrection() throws Exception {
		// Pitch correction changes variables Depth, AlongDistance, AzimutAngle
		// Expecting error on them

		// Filtering on antenna 0
		// Differences must start at index 400 (ie, first index of antenna 1)
		try {
			testPitchCorrection(BIAS_CORRECTION_REF_INPUT_FILE, PITCH_BIAS_CORRECTION_REF_OUTPUT_FILE,
					PITCH_BIAS_CORRECTION_TST_FILE, PITCH_BIAS_CORRECTION_FILE, null, OptionalInt.of(0));
		} catch (AssertionError e) {
			assertTrue(e.getMessage().contains("[/mbDepth] Data not equals for index [0:400]"));
			assertTrue(e.getMessage().contains("[/mbAlongDistance] Data not equals for index [0:400]"));
			assertTrue(e.getMessage().contains("[/mbAzimutBeamAngle] Data not equals for index [0:400]"));
		}

		// Filtering on antenna 1
		// Differences must start at index 0 (ie, first index of antenna 0)
		try {
			testPitchCorrection(BIAS_CORRECTION_REF_INPUT_FILE, PITCH_BIAS_CORRECTION_REF_OUTPUT_FILE,
					PITCH_BIAS_CORRECTION_TST_FILE, PITCH_BIAS_CORRECTION_FILE, null, OptionalInt.of(1));
		} catch (AssertionError e) {
			assertTrue(e.getMessage().contains("[/mbDepth] Data not equals for index [0:0]"));
			assertTrue(e.getMessage().contains("[/mbAlongDistance] Data not equals for index [0:0]"));
			assertTrue(e.getMessage().contains("[/mbAzimutBeamAngle] Data not equals for index [0:0]"));
		}

	}

	/**
	 * Processes a test on velocity correction.
	 */
	@Test
	public void testVelocityCorrection() throws Exception {
		// SoundVelocityCorrector changes only variables Depth
		// Expecting error on it

		// Filtering on antenna 0
		// Differences must start at index 400 (ie, first index of antenna 1)
		try {
			testVelocityCorrection(BIAS_CORRECTION_REF_INPUT_FILE, VELOCITY_CORRECTION_REF_OUTPUT_FILE,
					VELOCITY_CORRECTION_TST_FILE, VELOCITY_CORRECTION_FILE, OptionalInt.of(0));
		} catch (AssertionError e) {
			// Differences start to beam 569 because of tolerence
			assertTrue(e.getMessage().contains("[/mbDepth] Data not equals for index [0:569]"));
		}

		// Filtering on antenna 1
		// Differences must start at index 0 (ie, first index of antenna 0)
		try {
			testVelocityCorrection(BIAS_CORRECTION_REF_INPUT_FILE, VELOCITY_CORRECTION_REF_OUTPUT_FILE,
					VELOCITY_CORRECTION_TST_FILE, VELOCITY_CORRECTION_FILE, OptionalInt.of(1));
		} catch (AssertionError e) {
			assertTrue(e.getMessage().contains("[/mbDepth] Data not equals for index [0:0]"));
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		FileUtils.deleteQuietly(new File(HEADING_BIAS_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(ROLL_BIAS_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(PITCH_BIAS_CORRECTION_TST_FILE));
	}
}
