package fr.ifremer.globe.core.io.mbg.correction;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.info.HistoryConstants;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.exception.GException;



public class MBGCorrection {

	protected static Logger logger = LoggerFactory.getLogger(MBGCorrection.class);
	public NetcdfComparatorHook hook;

	/**
	 * Compare two Netcdf files
	 *
	 * @param refFilePath reference file path
	 * @param testFilePath test result file path
	 * @throws IOException 
	 * @throws GException 
	 */
	public static void compareFiles(String refFilePath, String testFilePath, Optional<NetcdfComparatorHook> hook) throws GException, IOException {
		
		StringBuilder resume = new StringBuilder("MbgResult result : \n");
		boolean testFailed = false;
		
		// Check if  file exists
		
		Assert.assertTrue(new File (refFilePath).exists());
		
		Assert.assertTrue(new File(testFilePath).exists());		
		
		Optional<String> errors = NetcdfComparator.compareFiles(refFilePath, testFilePath, hook);
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(testFilePath + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(testFilePath + " : OK \n");
		}
		
		logger.debug(resume.toString());
		Assert.assertFalse(resume.toString(), testFailed);
	}

	
	/**
	 * Defines attributes and variables to ignore
	 */
	public static NetcdfComparatorHook buildHook() {
		NetcdfComparatorHook result = new NetcdfComparatorHook();
		
		// Global _NCProperties different due to the NetCDF library update...
		result.ignoreGlobalAttribute("_NCProperties");
		result.ignoreGlobalAttribute("mbGeoRepresentation");
		result.ignoreGlobalAttribute("mbGeoDictionnary");
		result.ignoreVariable(HistoryConstants.DATE_VALUE);
		result.ignoreVariable(HistoryConstants.TIME_VALUE);
		result.ignoreVariable(HistoryConstants.AUTOR_VALUE);
		result.ignoreVariable(HistoryConstants.COMMENT_VALUE);
		result.ignoreVariable(HistoryConstants.MODULE_VALUE);
		result.ignoreVariable(HistoryConstants.CODE_VALUE);
		result.ignoreGlobalAttribute("mbNbrHistoryRec");
		result.ignoreGlobalAttribute("mbTxAntennaLeverArm");

		result.setToleranceForAttribute("mbEllipsoidE2", 1.12E-16);
		result.setToleranceForAttribute("mbEllipsoidInvF", 1.12E-16);
		result.ignoreGlobalAttribute("mbTxAntennaLeverArm");
		// Later introduction (MBG for Globe version > 2.3.4)
		result.ignoreGlobalAttribute("mbBiasCorrectionRef");
		 
		return result;
	}
}
