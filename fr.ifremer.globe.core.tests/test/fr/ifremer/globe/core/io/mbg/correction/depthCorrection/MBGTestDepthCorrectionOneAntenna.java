package fr.ifremer.globe.core.io.mbg.correction.depthCorrection;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class MBGTestDepthCorrectionOneAntenna extends MBGDepthCorrection {

	// Input reference file
	public static final String DEPTH_CORRECTION_REF_INPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/refMBG/0371_20140827_191459_AT_EM122.mbg";

	/***Tide Correction***/
	// Output reference file
	public static final String TIDE_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/tide_A_0371_20140827_191459_AT_EM122.mbg";
	// File to be processed
	public static final String TIDE_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/TideCorrection_A.mbg";
	// File to be processed
	public static final String TIDE_CORRECTION_TST_FILE_2 = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/TideCorrection_A_2.mbg";
	// Tide reference file
	public static final String TIDE_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/TideCorrection-A.ttb";


	/***Tide Correction with Cut file***/
	// Output reference file
	public static final String TIDE_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/tide_B_cut_2_0371_20140827_191459_AT_EM122.mbg";
	// File to be processed
	public static final String TIDE_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/TideCorrection_B_cut.mbg";
	// Tide reference file
	public static final String TIDE_CUT_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/TideCorrection-B.ttb";
	// Cut file
	public static final String TIDE_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/tide/oneAntenna/decoupe_maree_2.cut";


	/***DynamicDraught Correction***/
	// Output reference file
	public static final String DRAUGHT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/oneAntenna/tirant_B_0371_20140827_191459_AT_EM122.mbg";
	// File to be processed
	public static final String DRAUGHT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/oneAntenna/DraughtCorrection_B.mbg";
	// Tide reference file
	public static final String DRAUGHT_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/oneAntenna/TirantEauCorrection-B.ttb";


	/***DynamicDraught Correction with Cut file***/
	// Output reference file
	public static final String DRAUGHT_CUT_CORRECTION_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/oneAntenna/tirant_A_cut_2_0371_20140827_191459_AT_EM122.mbg";
	// File to be processed
	public static final String DRAUGHT_CUT_CORRECTION_TST_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draughtone/Antenna/DraughtCutCorrection_A_cut.mbg";
	// Tide reference file
	public static final String DRAUGHT_CUT_CORRECTION_TIDE_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/oneAntenna/TirantEauCorrection-A.ttb";
	// Cut file
	public static final String DRAUGHT_CUT_CORRECTION_CUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/depthCorrection/draught/oneAntenna/decoupe_TirantEau_2.cut";


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * Processes a test on tide correction.
	 */
	@Test
	public void testTideCorrection() throws Exception {
		tideCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, TIDE_CORRECTION_REF_OUTPUT_FILE, TIDE_CORRECTION_TST_FILE, TIDE_CORRECTION_TIDE_FILE, null);

		// Second correction
		tideCorrection(TIDE_CORRECTION_TST_FILE, TIDE_CORRECTION_REF_OUTPUT_FILE, TIDE_CORRECTION_TST_FILE_2, TIDE_CORRECTION_TIDE_FILE, null);
	}

	/**
	 * Processes a test on tide correction with a correction file.
	 */
	@Test
	public void testTideCutCorrection() throws Exception {
		tideCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, TIDE_CUT_CORRECTION_REF_OUTPUT_FILE, TIDE_CUT_CORRECTION_TST_FILE, TIDE_CUT_CORRECTION_TIDE_FILE, TIDE_CUT_CORRECTION_CUT_FILE);
	}

	/**
	 * Processes a test on draught correction.
	 */
	@Test
	public void testDraughtCorrection() throws Exception {
		draughtCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, DRAUGHT_CORRECTION_REF_OUTPUT_FILE, DRAUGHT_CORRECTION_TST_FILE, DRAUGHT_CORRECTION_TIDE_FILE, null);
	}

	/**
	 * Processes a test on draught correction with a correction file.
	 */
	@Test
	public void testDraughtCutCorrection() throws Exception {
		draughtCorrection(DEPTH_CORRECTION_REF_INPUT_FILE, DRAUGHT_CUT_CORRECTION_REF_OUTPUT_FILE, DRAUGHT_CUT_CORRECTION_TST_FILE, DRAUGHT_CUT_CORRECTION_TIDE_FILE, DRAUGHT_CUT_CORRECTION_CUT_FILE);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		FileUtils.deleteQuietly(new File(TIDE_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(TIDE_CORRECTION_TST_FILE_2));
		FileUtils.deleteQuietly(new File(TIDE_CUT_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(DRAUGHT_CORRECTION_TST_FILE));
		FileUtils.deleteQuietly(new File(DRAUGHT_CUT_CORRECTION_TST_FILE));
	}

}
