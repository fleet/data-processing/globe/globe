package fr.ifremer.globe.core.io.segy;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.io.segy.info.SegyFileInfo;
import fr.ifremer.globe.core.io.segy.info.SegyInfoSupplier;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Class test for SegyInfoSupplier.
 */
public class SegyTest {

	/** tested file */
	public static final String SEGY_FILE = GlobeTestUtil.getTestDataPath() + "/file/Segy/Salsa11.sgy";

	/**
	 * test method initData.
	 */
	@Test
	public void testInitData() {

		Assert.assertTrue(new java.io.File(SEGY_FILE).exists());
		SegyInfoSupplier supplier = new SegyInfoSupplier();
		SegyFileInfo segyInfo = supplier.getFileInfo(SEGY_FILE).orElse(null);
		Assert.assertNotNull(segyInfo);

		List<Property<?>> properties = segyInfo.getProperties();
		Assert.assertEquals("97 MB", findProperty(properties, "File size").getValue());
		Assert.assertEquals("S 14 50.768", findProperty(properties, "North Latitude").getValue());
		Assert.assertEquals("W 038 44.462", findProperty(properties, "West Longitude").getValue());
		Assert.assertEquals("S 15 02.488", findProperty(properties, "South Latitude").getValue());
		Assert.assertEquals("W 037 12.141", findProperty(properties, "East Longitude").getValue());
	}

	private Property<?> findProperty(List<Property<?>> properties, String key) {
		Property<?> property = properties.stream().filter(p -> key.equals(p.getKey())).findFirst().orElse(null);
		Assert.assertNotNull(property);
		return property;
	}
}
