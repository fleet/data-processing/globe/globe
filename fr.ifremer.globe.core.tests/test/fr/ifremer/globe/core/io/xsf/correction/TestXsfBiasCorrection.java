// TODO : move this package to a new bundle globe.process.tests (when all globe.process.* will be merged in a single bundle)

package fr.ifremer.globe.core.io.xsf.correction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.biascorrection.BiasCorrection;
import fr.ifremer.globe.core.processes.biascorrection.filereaders.CorrectionFileUtils;
import fr.ifremer.globe.core.processes.biascorrection.model.BiasCorrectionParameters;
import fr.ifremer.globe.core.processes.biascorrection.model.CommonCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList.CorrectionType;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class TestXsfBiasCorrection implements IDataContainerOwner {

	// Input reference file
	public static final String BIAS_CORRECTION_REF_ALL_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/xsf/xsfUnitTests/all/0095_20141031_133613_Thalia_small.all";
	public static final String BIAS_CORRECTION_REF_XSF_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/0095_20141031_133613_Thalia_small.xsf.nc";

	protected static final Logger LOGGER = LoggerFactory.getLogger(TestXsfBiasCorrection.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		File allFile = new File(BIAS_CORRECTION_REF_ALL_FILE);
		File xsfFile = new File(BIAS_CORRECTION_REF_XSF_FILE);

		if (xsfFile.exists())
			FileUtils.deleteQuietly(xsfFile);

		XsfConverterParameters convertParameters = new XsfConverterParameters(allFile.getCanonicalPath(),
				xsfFile.getCanonicalPath());
		convertParameters.setIgnoreWaterColumn(true);
		(new SounderFileConverter()).convertToXsf(convertParameters);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		FileUtils.deleteQuietly(new File(BIAS_CORRECTION_REF_XSF_FILE));
		TemporaryCache.cleanAllFiles();
	}

	/**
	 * Test a simple correction, constant, without time interval, with one correction point modifying heading, pitch,
	 * roll, velocity, vertical_offset
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void testSimpleCorrection() throws NCException, GIOException, BadParameterException, IOException {
		File outputFile = generateOutputFile();
		File inputFile = getInputFile();
		assertNotNull(inputFile);

		// generated temp correction files
		File biasCorrectionFile = TemporaryCache.createTemporaryFile("bias-", ".cor");

		ISounderNcInfo xsfInputInfo = IDataContainerInfoService.grab().getSounderNcInfo(inputFile.getAbsolutePath())
				.orElse(null);
		ISounderNcInfo xsfOutputInfo = IDataContainerInfoService.grab().getSounderNcInfo(outputFile.getAbsolutePath())
				.orElse(null);

		// generate constant correction file
		double headingBiasValue = 1.0;
		double pitchBiasValue = 2.0;
		double rollBiasValue = 3.0;
		double verticalOffsetBiasValue = 4.0;
		double velocityBiasValue = 5.0;

		CorrectionList corrections = new CorrectionList(CorrectionType.BIAS);
		CorrectionPoint cp = new CommonCorrectionPoint();
		cp.setHeading(headingBiasValue);
		cp.setPitch(pitchBiasValue);
		cp.setRoll(rollBiasValue);
		cp.setPlatformVerticalOffset(verticalOffsetBiasValue);
		cp.setVelocity(velocityBiasValue);
		corrections.addCorrectionPoint(cp);
		CorrectionFileUtils.write(biasCorrectionFile.getAbsolutePath(), corrections);

		// Setup process
		// Parameters
		BiasCorrectionParameters parameters = new BiasCorrectionParameters();
		parameters.setProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME,
				biasCorrectionFile.getAbsolutePath());
		parameters.setAntennaIndex(-1);

		BiasCorrection biasCorrection = new BiasCorrection(parameters, getDefaultTimeIntervalParameters());
		biasCorrection.setFileNameList(Arrays.asList(outputFile.getAbsolutePath()));

		// Apply process
		try {
			biasCorrection.applyForUnitaryTest(new NullProgressMonitor(), LOGGER);
		} catch (GException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// check results
		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();
		try ( //
				ISounderDataContainerToken outputToken = dataContainerFactory.book(xsfOutputInfo, this); //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			SounderDataContainer outputContainer = outputToken.getDataContainer();
			FloatLoadableLayer1D inputHeading = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
			FloatLoadableLayer1D outputHeading = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
			FloatLoadableLayer1D inputPitch = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);
			FloatLoadableLayer1D outputPitch = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);
			FloatLoadableLayer1D inputRoll = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
			FloatLoadableLayer1D outputRoll = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
			FloatLoadableLayer1D inputVelocity = inputContainer.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);
			FloatLoadableLayer1D outputVelocity = outputContainer.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);

			FloatLoadableLayer1D inputTxDepth = inputContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
			FloatLoadableLayer1D outputTxDepth = outputContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
			FloatLoadableLayer1D inputPlatformVerticalOffset = inputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
			FloatLoadableLayer1D outputPlatformVerticalOffset = outputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

			long size = inputHeading.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = 0; i < size; i++) {
				assertEquals(inputHeading.get(i) + headingBiasValue, outputHeading.get(i), 10e-6);
				assertEquals(inputPitch.get(i) + pitchBiasValue, outputPitch.get(i), 10e-6);
				assertEquals(inputRoll.get(i) + rollBiasValue, outputRoll.get(i), 10e-6);
				assertEquals(inputPlatformVerticalOffset.get(i) + verticalOffsetBiasValue,
						outputPlatformVerticalOffset.get(i), 10e-6);
				assertEquals(inputTxDepth.get(i) - verticalOffsetBiasValue, outputTxDepth.get(i), 10e-6);

				// Surface Sound velocity is not modified
				assertEquals(inputVelocity.get(i), outputVelocity.get(i), 0.0);
			}
		}
	}

	/**
	 * Test a simple MruHeading correction, constant, without time interval
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void testMruHeadingCorrection() throws NCException, GIOException, BadParameterException, IOException {
		File outputFile = generateOutputFile();
		File inputFile = getInputFile();
		assertNotNull(inputFile);

		// generated temp correction files
		File biasCorrectionFile = TemporaryCache.createTemporaryFile("bias-", ".cor");

		ISounderNcInfo xsfInputInfo = IDataContainerInfoService.grab().getSounderNcInfo(inputFile.getAbsolutePath())
				.orElse(null);
		ISounderNcInfo xsfOutputInfo = IDataContainerInfoService.grab().getSounderNcInfo(outputFile.getAbsolutePath())
				.orElse(null);

		// generate constant correction file
		// Apply 90 degree offset should revert pitch and roll
		double mruHeadingBiasValue = 90.0;

		CorrectionList corrections = new CorrectionList(CorrectionType.BIAS);
		CorrectionPoint cp = new CommonCorrectionPoint();
		cp.setMruHeading(mruHeadingBiasValue);
		corrections.addCorrectionPoint(cp);
		CorrectionFileUtils.write(biasCorrectionFile.getAbsolutePath(), corrections);

		// Setup process
		// Parameters
		BiasCorrectionParameters parameters = new BiasCorrectionParameters();
		parameters.setProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME,
				biasCorrectionFile.getAbsolutePath());
		parameters.setAntennaIndex(-1);

		BiasCorrection biasCorrection = new BiasCorrection(parameters, getDefaultTimeIntervalParameters());
		biasCorrection.setFileNameList(Arrays.asList(outputFile.getAbsolutePath()));

		// Apply process
		try {
			biasCorrection.applyForUnitaryTest(new NullProgressMonitor(), LOGGER);
		} catch (GException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// check results
		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();
		try ( //
				ISounderDataContainerToken outputToken = dataContainerFactory.book(xsfOutputInfo, this); //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			SounderDataContainer outputContainer = outputToken.getDataContainer();
			FloatLoadableLayer1D inputPitch = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);
			FloatLoadableLayer1D outputPitch = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);
			FloatLoadableLayer1D inputRoll = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
			FloatLoadableLayer1D outputRoll = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);

			long size = inputRoll.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = 0; i < size; i++) {
				assertEquals(inputPitch.get(i), -outputRoll.get(i), 10e-6);
				// pitch not modified for now
				assertEquals(outputPitch.get(i), outputPitch.get(i), 10e-6);
			}
		}
	}

	/**
	 * Test a time delay correction on roll/pitch/heading
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void testAttitudeTimeCorrection() throws NCException, GIOException, BadParameterException, IOException {
		File outputFile = generateOutputFile();
		File inputFile = getInputFile();
		assertNotNull(inputFile);

		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();

		// generated temp correction files
		File biasCorrectionFile = TemporaryCache.createTemporaryFile("bias-", ".cor");

		ISounderNcInfo xsfInputInfo = IDataContainerInfoService.grab().getSounderNcInfo(inputFile.getAbsolutePath())
				.orElse(null);
		ISounderNcInfo xsfOutputInfo = IDataContainerInfoService.grab().getSounderNcInfo(outputFile.getAbsolutePath())
				.orElse(null);

		CorrectionList corrections = new CorrectionList(CorrectionType.BIAS);
		int pingShift = 4;
		// generate corrections with 10 ping delays
		try ( //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			LongLoadableLayer1D inputTime = inputContainer.getLayer(BeamGroup1Layers.PING_TIME);
			long size = inputTime.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = pingShift; i < size; i++) {
				CorrectionPoint cp = new CommonCorrectionPoint();
				cp.setDate(DateUtils.epochNanoToInstant(inputTime.get(i)));
				// set negative value to apply an attitude in the past
				cp.setAttitudeTimeMs(DateUtils.nanoSecondToMilli(inputTime.get(i - pingShift))
						- DateUtils.nanoSecondToMilli(inputTime.get(i)));
				corrections.addCorrectionPoint(cp);
			}
		}
		CorrectionFileUtils.write(biasCorrectionFile.getAbsolutePath(), corrections);

		// Setup process
		// Parameters
		BiasCorrectionParameters parameters = new BiasCorrectionParameters();
		parameters.setProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME,
				biasCorrectionFile.getAbsolutePath());
		parameters.setAntennaIndex(-1);

		BiasCorrection biasCorrection = new BiasCorrection(parameters, getDefaultTimeIntervalParameters());
		biasCorrection.setFileNameList(Arrays.asList(outputFile.getAbsolutePath()));

		// Apply process
		try {
			biasCorrection.applyForUnitaryTest(new NullProgressMonitor(), LOGGER);
		} catch (GException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// check results
		try ( //
				ISounderDataContainerToken outputToken = dataContainerFactory.book(xsfOutputInfo, this); //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			SounderDataContainer outputContainer = outputToken.getDataContainer();
			FloatLoadableLayer1D inputHeading = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
			FloatLoadableLayer1D outputHeading = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
			FloatLoadableLayer1D inputPitch = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);
			FloatLoadableLayer1D outputPitch = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_PITCH);
			FloatLoadableLayer1D inputRoll = inputContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
			FloatLoadableLayer1D outputRoll = outputContainer.getLayer(BeamGroup1Layers.PLATFORM_ROLL);
			long size = inputHeading.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = pingShift + 1; i < size; i++) { // first ping begins before attitude datagram
				assertEquals(inputHeading.get(i - pingShift), outputHeading.get(i), 0.01001f);
				assertEquals(inputPitch.get(i - pingShift), outputPitch.get(i), 0.001f);
				assertEquals(inputRoll.get(i - pingShift), outputRoll.get(i), 0.001f);
			}
		}
	}

	/**
	 * Test a time delay correction on vertical_offset
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void testVerticalOffsetTimeCorrection() throws NCException, GIOException, BadParameterException, IOException {
		File outputFile = generateOutputFile();
		File inputFile = getInputFile();
		assertNotNull(inputFile);

		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();

		// generated temp correction files
		File biasCorrectionFile = TemporaryCache.createTemporaryFile("bias-", ".cor");

		ISounderNcInfo xsfInputInfo = IDataContainerInfoService.grab().getSounderNcInfo(inputFile.getAbsolutePath())
				.orElse(null);
		ISounderNcInfo xsfOutputInfo = IDataContainerInfoService.grab().getSounderNcInfo(outputFile.getAbsolutePath())
				.orElse(null);

		CorrectionList corrections = new CorrectionList(CorrectionType.BIAS);
		int pingShift = 4;
		// generate corrections with 10 ping delays
		try ( //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			LongLoadableLayer1D inputTime = inputContainer.getLayer(BeamGroup1Layers.PING_TIME);
			long size = inputTime.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = pingShift; i < size; i++) {
				CorrectionPoint cp = new CommonCorrectionPoint();
				cp.setDate(DateUtils.epochNanoToInstant(inputTime.get(i)));
				// set negative value to apply an attitude in the past
				cp.setVerticalOffsetTimeMs(DateUtils.nanoSecondToMilli(inputTime.get(i - pingShift))
						- DateUtils.nanoSecondToMilli(inputTime.get(i)));
				corrections.addCorrectionPoint(cp);
			}
		}
		CorrectionFileUtils.write(biasCorrectionFile.getAbsolutePath(), corrections);

		// Setup process
		// Parameters
		BiasCorrectionParameters parameters = new BiasCorrectionParameters();
		parameters.setProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME,
				biasCorrectionFile.getAbsolutePath());
		parameters.setAntennaIndex(-1);

		BiasCorrection biasCorrection = new BiasCorrection(parameters, getDefaultTimeIntervalParameters());
		biasCorrection.setFileNameList(Arrays.asList(outputFile.getAbsolutePath()));

		// Apply process
		try {
			biasCorrection.applyForUnitaryTest(new NullProgressMonitor(), LOGGER);
		} catch (GException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// check results
		try ( //
				ISounderDataContainerToken outputToken = dataContainerFactory.book(xsfOutputInfo, this); //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			SounderDataContainer outputContainer = outputToken.getDataContainer();
			FloatLoadableLayer1D inputPlatformVerticalOffset = inputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
			FloatLoadableLayer1D outputPlatformVerticalOffset = outputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

			long size = inputPlatformVerticalOffset.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = pingShift + 1; i < size; i++) { // first ping begins before attitude datagram
				assertEquals(inputPlatformVerticalOffset.get(i - pingShift), //
						outputPlatformVerticalOffset.get(i), 0.01f);
			}
		}
	}

	/**
	 * Test writing and reading back a correction file.
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void writeReadCorrectionFileTest() throws NCException, GIOException, BadParameterException, IOException {
		// generated temp correction files
		File biasCorrectionFile = TemporaryCache.createTemporaryFile("bias-", ".cor");

		// generate constant correction file
		double headingBiasValue = 1.0;
		double pitchBiasValue = 2.0;
		double rollBiasValue = 3.0;
		double verticalOffsetBiasValue = 4.0;
		double velocityBiasValue = 5.0;
		double mruHeadingBiasValue = 6.0;
		double attitudeTimeBiasValue = 7.0;
		double verticalOffsetTimeBiasValue = 8.0;

		// INPUT
		CorrectionList inputCorrections = new CorrectionList(CorrectionType.BIAS);
		CorrectionPoint cp1 = new CommonCorrectionPoint();
		cp1.setDate(Instant.now());
		cp1.setHeading(headingBiasValue);
		cp1.setPitch(pitchBiasValue);
		cp1.setRoll(rollBiasValue);
		inputCorrections.addCorrectionPoint(cp1);
		CorrectionPoint cp2 = new CommonCorrectionPoint();
		cp2.setDate(Instant.now().plusSeconds(1));
		cp2.setPlatformVerticalOffset(verticalOffsetBiasValue);
		cp2.setVelocity(velocityBiasValue);
		cp2.setMruHeading(mruHeadingBiasValue);
		cp2.setAttitudeTimeMs(attitudeTimeBiasValue);
		cp2.setVerticalOffsetTimeMs(verticalOffsetTimeBiasValue);
		inputCorrections.addCorrectionPoint(cp2);

		// WRITE
		CorrectionFileUtils.write(biasCorrectionFile.getAbsolutePath(), inputCorrections);

		// READ AND COMPARE
		CorrectionList outputCorrections = CorrectionFileUtils.read(biasCorrectionFile.getAbsolutePath());
		for (int i = 0; i < inputCorrections.getSize(); i++) {
			CorrectionPoint inputCorrection = inputCorrections.getCorrectionPointList().get(i);
			CorrectionPoint outputCorrection = outputCorrections.getCorrectionPointList().get(i);
			assertEquals(inputCorrection.getDate().toEpochMilli(), outputCorrection.getDate().toEpochMilli(), 1);
			assertEquals(inputCorrection.getHeading(), outputCorrection.getHeading(), 0.0);
			assertEquals(inputCorrection.getPitch(), outputCorrection.getPitch(), 0.0);
			assertEquals(inputCorrection.getRoll(), outputCorrection.getRoll(), 0.0);
			assertEquals(inputCorrection.getPlatformVerticalOffset(), outputCorrection.getPlatformVerticalOffset(),
					0.0);
			assertEquals(inputCorrection.getVelocity(), outputCorrection.getVelocity(), 0.0);
			assertEquals(inputCorrection.getMruHeading(), outputCorrection.getMruHeading(), 0.0);
			assertEquals(inputCorrection.getAttitudeTimeMs(), outputCorrection.getAttitudeTimeMs(), 0.0);
			assertEquals(inputCorrection.getVerticalOffsetTimeMs(), outputCorrection.getVerticalOffsetTimeMs(), 0.0);
		}
	}

	public File getInputFile() {
		return new File(BIAS_CORRECTION_REF_XSF_FILE);
	}

	public File generateOutputFile() throws IOException {
		// Copy input file before processing
		File inputFile = new File(BIAS_CORRECTION_REF_XSF_FILE);
		assertNotNull(inputFile);
		File outputFile = TemporaryCache.createTemporaryFile("bias-", ".xsf.nc");
		FileUtils.copyFile(inputFile, outputFile);
		return outputFile;
	}

	private TimeIntervalParameters getDefaultTimeIntervalParameters() {
		TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
				"fr.ifremer.globe.process.biascorrection");
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION, Boolean.FALSE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION, Boolean.FALSE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_NOINTERVAL_OPTION, Boolean.TRUE);
		return timeIntervalParameters;
	}

}
