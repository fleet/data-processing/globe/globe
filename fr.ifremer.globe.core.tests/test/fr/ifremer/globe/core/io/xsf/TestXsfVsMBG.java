package fr.ifremer.globe.core.io.xsf;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.io.xsf.info.XsfInfo;
import fr.ifremer.globe.core.model.dtm.SounderDataDtmDepthRangeComputer;
import fr.ifremer.globe.core.model.dtm.SounderDataDtmGeoboxComputer;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Test XSF vs MBG
 */
public class TestXsfVsMBG implements IDataContainerOwner {
	protected Logger logger = LoggerFactory.getLogger(TestXsfVsMBG.class);

	/** Factory of the sounder data container */
	private final IDataContainerFactory containerFactory = IDataContainerFactory.grab();

	public final static String sourceDirectory = GlobeTestUtil.getTestDataPath();
	public final static File inReson = new File(sourceDirectory + File.separator + "file" + File.separator + "xsf"
			+ File.separator + "XSFValidation" + File.separator + "20150902_041108_PP_7150_24kHz.s7k");
	public final static File xsfReson = new File(sourceDirectory + File.separator + "file" + File.separator + "xsf"
			+ File.separator + "XSFValidation" + File.separator + "outmbgCompare.s7k" + XsfBase.EXTENSION_XSF);

	public final static File mbgRefReson = new File(sourceDirectory + File.separator + "file" + File.separator + "xsf"
			+ File.separator + "XSFValidation" + File.separator + "20150902_041108_PP_7150_24kHz.mbg");

	@Test
	public void compare() throws IOException, GException {
		boolean regenerate = true;
		if (regenerate) {
			if (xsfReson.exists())
				Files.delete(xsfReson.toPath());
			new SounderFileConverter()
					.convertToXsf(new XsfConverterParameters(inReson.getCanonicalPath(), xsfReson.getCanonicalPath()));
		}
		IFileService fileService = IFileService.grab();
		MbgInfo mbg = (MbgInfo) fileService.getFileInfo(mbgRefReson.getAbsolutePath()).orElse(null);
		Assert.assertNotNull(mbg);
		XsfInfo xsf = (XsfInfo) fileService.getFileInfo(xsfReson.getAbsolutePath()).orElse(null);
		Assert.assertNotNull(xsf);

		// MBG getGeobox() is read from file attributes
		// XSF getGeobox() is computed on navigationData only
		// the precisions are chosen to take in account that difference
		double geoboxLowPrecision = 10e-2;
		double geoboxHighPrecision = 10e-6;
		SounderDataDtmGeoboxComputer geoboxComputer = new SounderDataDtmGeoboxComputer(List.of(xsf));
		GeoBox xsfGeoBox = geoboxComputer.computeValidGeobox();
		Assert.assertEquals(xsf.getGeoBox().getTop(), xsfGeoBox.getTop(), geoboxLowPrecision);
		Assert.assertEquals(xsf.getGeoBox().getBottom(), xsfGeoBox.getBottom(), geoboxLowPrecision);
		Assert.assertEquals(xsf.getGeoBox().getRight(), xsfGeoBox.getRight(), geoboxLowPrecision);
		Assert.assertEquals(xsf.getGeoBox().getLeft(), xsfGeoBox.getLeft(), geoboxLowPrecision);

		Assert.assertEquals(mbg.getGeoBox().getTop(), xsfGeoBox.getTop(), geoboxHighPrecision);
		Assert.assertEquals(mbg.getGeoBox().getBottom(), xsfGeoBox.getBottom(), geoboxHighPrecision);
		Assert.assertEquals(mbg.getGeoBox().getRight(), xsfGeoBox.getRight(), geoboxHighPrecision);
		Assert.assertEquals(mbg.getGeoBox().getLeft(), xsfGeoBox.getLeft(), geoboxHighPrecision);

		SounderDataDtmDepthRangeComputer depthRangeComputer = new SounderDataDtmDepthRangeComputer(List.of(xsf));
		DoubleRange xsfDepthRange = depthRangeComputer.computeValidDepthRange();

		Assert.assertEquals(-1 * mbg.getMaxDepth(), xsfDepthRange.getMaximumDouble(), 20); // we add some tolerance
																							// since mbg is relative to
																							// water line and xsf to
																							// reference
		Assert.assertEquals(-1 * mbg.getMinDepth(), xsfDepthRange.getMinimumDouble(), 20); // we add some tolerance
																							// since mbg is relative to
																							// water line and xsf to
																							// reference

		// Min and max depth are not comparable immediately : they are computed asynchronously in XSF
		// Assert.assertTrue(xsfDepthRange.getMinimumDouble() <= xsf.getMinDepth()
		// && xsfDepthRange.getMaximumDouble() >= xsf.getMaxDepth());

		// check for differences in positions
		try (ISounderDataContainerToken mbgToken = containerFactory.book(mbg, this);
				ISounderDataContainerToken xsfToken = containerFactory.book(xsf, this)) {
			TestLayers mbgL = new TestLayers(mbgToken.getDataContainer());
			TestLayers xsfL = new TestLayers(xsfToken.getDataContainer());

			double diffDepth = Double.NaN;
			double diffLat = Double.NaN;
			double diffLon = Double.NaN;
			DescriptiveStatistics vertStat = new DescriptiveStatistics();

			for (int swath = 0; swath < mbg.getCycleCount(); swath++) {
				vertStat.addValue(xsfL.vertical_offset.get(swath));
				for (int detection = 0; detection < mbg.getTotalRxBeamCount(); detection++) {
					Assert.assertEquals(String.format("Status differs at [%d,%d]", swath, detection),
							mbgL.status.isValid(swath, detection), xsfL.status.isValid(swath, detection));
					if (xsfL.status.isValid(swath, detection)) {
						double depthVariation = Math
								.abs(mbgL.depth.get(swath, detection) - xsfL.depth.get(swath, detection));
						diffDepth = NumberUtils.Max(depthVariation, diffDepth);
						diffLat = NumberUtils.Max(
								Math.abs(mbgL.lat.get(swath, detection) - xsfL.lat.get(swath, detection)), diffLat);
						diffLon = NumberUtils.Max(
								Math.abs(mbgL.lon.get(swath, detection) - xsfL.lon.get(swath, detection)), diffLon);
					}
				}
			}

			// we know there is a mismatch between mbg and XSF since mbg sometimes made a confusion between vehicule
			// depth
			// and heave
			// thus we allow differences at maximum equals to vehicule depth variation
			Assert.assertTrue(diffDepth < 1.1 * vertStat.getMax());
			Assert.assertTrue(diffLon < 10e-6);
			Assert.assertTrue(diffLat < 10e-6);
		}
	}

	@After
	public void after() {
		try {
			Files.delete(xsfReson.toPath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class TestLayers {
		public TestLayers(SounderDataContainer container) throws GIOException {
			CompositeCSLayers csLayer = container.getCsLayers(CoordinateSystem.FCS);
			lat = csLayer.getProjectedX();
			lon = csLayer.getProjectedY();
			depth = csLayer.getProjectedZ();
			status = container.getStatusLayer();
			vertical_offset = container.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
		}

		DoubleLayer2D lat;
		DoubleLayer2D lon;
		DoubleLayer2D depth;
		DetectionStatusLayer status;
		FloatLoadableLayer1D vertical_offset;
	}
}
