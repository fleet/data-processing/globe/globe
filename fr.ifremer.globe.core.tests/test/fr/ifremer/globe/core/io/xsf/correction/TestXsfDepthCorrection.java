// TODO : move this package to a new bundle globe.process.tests (when all globe.process.* will be merged in a single bundle)

package fr.ifremer.globe.core.io.xsf.correction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.core.io.tide.ttb.TtbFileWriter;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrection;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionParameters;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionSource;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.TideLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.DynamicDraughtLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class TestXsfDepthCorrection implements IDataContainerOwner {

	// Input reference file
	public static final String DEPTH_CORRECTION_REF_ALL_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/xsf/ALLFiles/0003_20130201_165823_Thalia.all";
	public static final String DEPTH_CORRECTION_REF_XSF_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/0003_20130201_165823_Thalia.xsf.nc";

	protected static final Logger LOGGER = LoggerFactory.getLogger(TestXsfDepthCorrection.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		File allFile = new File(DEPTH_CORRECTION_REF_ALL_FILE);
		File xsfFile = new File(DEPTH_CORRECTION_REF_XSF_FILE);

		if (xsfFile.exists())
			FileUtils.deleteQuietly(xsfFile);

		XsfConverterParameters convertParameters = new XsfConverterParameters(allFile.getCanonicalPath(),
				xsfFile.getCanonicalPath());
		convertParameters.setIgnoreWaterColumn(true);
		(new SounderFileConverter()).convertToXsf(convertParameters);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		FileUtils.deleteQuietly(new File(DEPTH_CORRECTION_REF_XSF_FILE));
		TemporaryCache.cleanAllFiles();
	}

	/**
	 * Test a simple tide correction, constant, without time interval.
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void simpleTideCorrectionTest() throws NCException, GIOException, BadParameterException, IOException {
		boolean applyTide = true;
		boolean applyDraught = false;
		boolean applyPlatform = false;
		File outputFile = generateOutputFile();
		correctionTest(outputFile, applyTide, applyDraught, applyPlatform);
		resetTest(outputFile);
	}

	/**
	 * Test a simple draught correction, constant, without time interval.
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void simpleDraughtCorrectionTest() throws NCException, GIOException, BadParameterException, IOException {
		boolean applyTide = false;
		boolean applyDraught = true;
		boolean applyPlatform = false;
		File outputFile = generateOutputFile();
		correctionTest(outputFile, applyTide, applyDraught, applyPlatform);
		resetTest(outputFile);
	}

	/**
	 * Test a simple platform correction, constant, without time interval.
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void simplePlatformCorrectionTest() throws NCException, GIOException, BadParameterException, IOException {
		boolean applyTide = false;
		boolean applyDraught = false;
		boolean applyPlatform = true;
		File outputFile = generateOutputFile();
		correctionTest(outputFile, applyTide, applyDraught, applyPlatform);
		resetTest(outputFile);
	}

	/**
	 * Test a combined platform correction with draught, constant, without time interval.
	 * 
	 * @throws IOException
	 * @throws BadParameterException
	 * @throws GIOException
	 * @throws NCException
	 */
	@Test
	public void simplePlatformWithDraughtCorrectionTest()
			throws NCException, GIOException, BadParameterException, IOException {
		boolean applyTide = false;
		boolean applyDraught = true;
		boolean applyPlatform = true;
		File outputFile = generateOutputFile();
		correctionTest(outputFile, applyTide, applyDraught, applyPlatform);
		resetTest(outputFile);
	}

	public File getInputFile() {
		return new File(DEPTH_CORRECTION_REF_XSF_FILE);
	}

	public File generateOutputFile() throws IOException {
		// Copy input file before processing
		File inputFile = new File(DEPTH_CORRECTION_REF_XSF_FILE);
		assertNotNull(inputFile);
		File outputFile = TemporaryCache.createTemporaryFile("tide-", ".xsf.nc");
		FileUtils.copyFile(inputFile, outputFile);
		return outputFile;
	}

	private void correctionTest(File outputFile, boolean applyTide, boolean applyDraught, boolean applyPlatform)
			throws IOException, NCException, GIOException, BadParameterException {
		File inputFile = getInputFile();
		assertNotNull(inputFile);

		// generated temp correction files
		File tideCorrectionFile = TemporaryCache.createTemporaryFile("tide-", ".ttb");
		File draughtCorrectionFile = TemporaryCache.createTemporaryFile("draught-", ".ttb");
		File platformCorrectionFile = TemporaryCache.createTemporaryFile("platform-", ".ttb");

		ISounderNcInfo xsfInputInfo = IDataContainerInfoService.grab().getSounderNcInfo(inputFile.getAbsolutePath())
				.orElse(null);
		ISounderNcInfo xsfOutputInfo = IDataContainerInfoService.grab().getSounderNcInfo(outputFile.getAbsolutePath())
				.orElse(null);

		// generate constant correction file
		double tideValue = 10.0;
		double platformValue = 15.0;
		double platformVOffset = -1.0;
		double draughtValue = -0.15;
		// tide
		List<ITideData> tideData = Arrays.asList(
				ITideData.build(xsfInputInfo.getFirstPingDate().toInstant(), tideValue),
				ITideData.build(xsfInputInfo.getLastPingDate().toInstant(), tideValue));
		TtbFileWriter.write(tideCorrectionFile.getAbsolutePath(), tideData);
		// draught
		List<ITideData> draughtData = Arrays.asList(
				ITideData.build(xsfInputInfo.getFirstPingDate().toInstant(), draughtValue),
				ITideData.build(xsfInputInfo.getLastPingDate().toInstant(), draughtValue));
		TtbFileWriter.write(draughtCorrectionFile.getAbsolutePath(), draughtData);
		// platform
		List<ITideData> platformData = Arrays.asList(
				ITideData.build(xsfInputInfo.getFirstPingDate().toInstant(), platformValue),
				ITideData.build(xsfInputInfo.getLastPingDate().toInstant(), platformValue));
		TtbFileWriter.write(platformCorrectionFile.getAbsolutePath(), platformData);

		// Setup process
		// Parameters
		DepthCorrectionParameters parameters = new DepthCorrectionParameters();
		parameters.setProperty(DepthCorrectionParameters.PROPERTY_TIDE_FILENAME, tideCorrectionFile.getAbsolutePath());
		parameters.setProperty(DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_FILENAME,
				platformCorrectionFile.getAbsolutePath());
		parameters.setProperty(DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_VOFFSET,
				Double.toString(platformVOffset));
		parameters.setProperty(DepthCorrectionParameters.PROPERTY_DYNAMIC_DRAUGHT_FILENAME,
				draughtCorrectionFile.getAbsolutePath());

		double computedDraughtValue = 0.0;
		double computedTideValue = 0.0;
		if (applyDraught) {
			computedDraughtValue = draughtValue;
			parameters.setProperty(DepthCorrectionParameters.PROPERTY_DRAUGHT_CORRECTION_SOURCE,
					DepthCorrectionSource.CORRECTION_FILE.name());
		} else {
			parameters.setProperty(DepthCorrectionParameters.PROPERTY_DRAUGHT_CORRECTION_SOURCE,
					DepthCorrectionSource.NONE.name());
		}

		if (applyTide) {
			computedTideValue = tideValue;
			parameters.setProperty(DepthCorrectionParameters.PROPERTY_TIDE_CORRECTION_SOURCE,
					DepthCorrectionSource.CORRECTION_FILE.name());
		} else if (applyPlatform) {
			computedTideValue = platformValue - platformVOffset - computedDraughtValue;
			parameters.setProperty(DepthCorrectionParameters.PROPERTY_TIDE_CORRECTION_SOURCE,
					DepthCorrectionSource.PLATFORM_ELEVATION_FILE.name());
		} else {
			parameters.setProperty(DepthCorrectionParameters.PROPERTY_TIDE_CORRECTION_SOURCE,
					DepthCorrectionSource.NONE.name());
		}

		DepthCorrection depthCorrection = new DepthCorrection(parameters, getDefaultTimeIntervalParameters(), LOGGER);
		depthCorrection.setFileNameList(Arrays.asList(outputFile.getAbsolutePath()));

		// Apply process
		depthCorrection.apply(new NullProgressMonitor(), LOGGER);

		// check results
		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();
		try ( //
				ISounderDataContainerToken outputToken = dataContainerFactory.book(xsfOutputInfo, this); //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			SounderDataContainer outputContainer = outputToken.getDataContainer();
			FloatLoadableLayer1D tide = outputContainer.getLayer(TideLayers.TIDE_INDICATIVE);
			FloatLoadableLayer1D waterline = outputContainer.getLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);
			FloatLoadableLayer1D draught = outputContainer.getLayer(DynamicDraughtLayers.DELTA_DRAUGHT);
			FloatLoadableLayer1D inputTxDepth = inputContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
			FloatLoadableLayer1D outputTxDepth = outputContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
			FloatLoadableLayer1D inputPlatformVerticalOffset = inputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
			FloatLoadableLayer1D outputPlatformVerticalOffset = outputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

			long size = tide.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = 0; i < size; i++) {
				assertEquals(computedTideValue, tide.get(i), 10e-6);
				assertEquals(computedTideValue, waterline.get(i), 10e-6);
				assertEquals(computedDraughtValue, draught.get(i), 10e-6);
				assertEquals(inputPlatformVerticalOffset.get(i) + computedDraughtValue,
						outputPlatformVerticalOffset.get(i), 10e-6);
				assertEquals(inputTxDepth.get(i) - computedDraughtValue, outputTxDepth.get(i), 10e-6);
			}
		}
	}

	private void resetTest(File outputFile) throws GIOException {
		File inputFile = getInputFile();
		// generated temp correction files
		ISounderNcInfo xsfInputInfo = IDataContainerInfoService.grab().getSounderNcInfo(inputFile.getAbsolutePath())
				.orElse(null);
		ISounderNcInfo xsfOutputInfo = IDataContainerInfoService.grab().getSounderNcInfo(outputFile.getAbsolutePath())
				.orElse(null);

		// Setup reset process
		// Parameters
		DepthCorrectionParameters parameters = new DepthCorrectionParameters();

		parameters.setProperty(DepthCorrectionParameters.PROPERTY_DRAUGHT_CORRECTION_SOURCE,
				DepthCorrectionSource.RESET.name());

		parameters.setProperty(DepthCorrectionParameters.PROPERTY_TIDE_CORRECTION_SOURCE,
				DepthCorrectionSource.RESET.name());

		DepthCorrection depthCorrection = new DepthCorrection(parameters, getDefaultTimeIntervalParameters(), LOGGER);
		depthCorrection.setFileNameList(Arrays.asList(outputFile.getAbsolutePath()));

		// Apply process
		depthCorrection.apply(new NullProgressMonitor(), LOGGER);

		// check results
		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();
		try ( //
				ISounderDataContainerToken outputToken = dataContainerFactory.book(xsfOutputInfo, this); //
				ISounderDataContainerToken inputToken = dataContainerFactory.book(xsfInputInfo, this) //
		) {
			SounderDataContainer inputContainer = inputToken.getDataContainer();
			SounderDataContainer outputContainer = outputToken.getDataContainer();
			FloatLoadableLayer1D tide = outputContainer.getLayer(TideLayers.TIDE_INDICATIVE);
			FloatLoadableLayer1D waterline = outputContainer.getLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);
			FloatLoadableLayer1D draught = outputContainer.getLayer(DynamicDraughtLayers.DELTA_DRAUGHT);
			FloatLoadableLayer1D inputTxDepth = inputContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
			FloatLoadableLayer1D outputTxDepth = outputContainer.getLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);
			FloatLoadableLayer1D inputPlatformVerticalOffset = inputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);
			FloatLoadableLayer1D outputPlatformVerticalOffset = outputContainer
					.getLayer(BeamGroup1Layers.PLATFORM_VERTICAL_OFFSET);

			long size = tide.getDimensions()[0];
			assertTrue(size > 0);
			for (int i = 0; i < size; i++) {
				assertEquals(0.0, tide.get(i), 10e-6);
				assertEquals(0.0, waterline.get(i), 10e-6);
				assertEquals(0.0, draught.get(i), 10e-6);
				assertEquals(inputPlatformVerticalOffset.get(i), outputPlatformVerticalOffset.get(i), 10e-6);
				assertEquals(inputTxDepth.get(i), outputTxDepth.get(i), 10e-6);
			}
		}
	}

	private TimeIntervalParameters getDefaultTimeIntervalParameters() {
		TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
				"fr.ifremer.globe.process.depthcorrection");
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION, Boolean.FALSE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION, Boolean.FALSE);
		timeIntervalParameters.setBoolProperty(TimeIntervalParameters.PROPERTY_NOINTERVAL_OPTION, Boolean.TRUE);
		return timeIntervalParameters;
	}

}
