package fr.ifremer.globe.core;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.core.dtm.DtmResolutionComputerTest;
import fr.ifremer.globe.core.geo.GeoTest;
import fr.ifremer.globe.core.gson.TestRecord;
import fr.ifremer.globe.core.io.mbg.MBGTestReader;
import fr.ifremer.globe.core.io.mbg.MbgValidityMapperTest;
import fr.ifremer.globe.core.io.mbg.convert.KongsbergMBGTestConverter;
import fr.ifremer.globe.core.io.mbg.convert.S7K_7111_MBGTestConverter;
import fr.ifremer.globe.core.io.mbg.convert.S7K_7125_MBGTestConverter;
import fr.ifremer.globe.core.io.mbg.convert.S7K_7150_Before_19_3_2011_MBGTestConverter;
import fr.ifremer.globe.core.io.mbg.correction.MbgFlagUpdateTest;
import fr.ifremer.globe.core.io.mbg.correction.biasCorrection.MBGTestBiasCorrectionAntennaFilter;
import fr.ifremer.globe.core.io.mbg.correction.biasCorrection.MBGTestBiasCorrectionMultiAntennas;
import fr.ifremer.globe.core.io.mbg.correction.biasCorrection.MBGTestBiasCorrectionOneAntenna;
import fr.ifremer.globe.core.io.mbg.correction.depthCorrection.MBGTestDepthCorrectionMultiAntennas;
import fr.ifremer.globe.core.io.mbg.correction.depthCorrection.MBGTestDepthCorrectionOneAntenna;
import fr.ifremer.globe.core.io.mbg.correction.soundVelocityCorrection.MBGTestSoundVelocityCorrection;
import fr.ifremer.globe.core.io.mbg.export.MBGToValidityFlagTest;
import fr.ifremer.globe.core.io.mbg.filtri.MbgFiltTriTest;
import fr.ifremer.globe.core.io.mbg.projection.TestProjection;
import fr.ifremer.globe.core.io.nvi.export.AllToNviExportTests;
import fr.ifremer.globe.core.io.nvi.export.MbgToNviExportTests;
import fr.ifremer.globe.core.io.nvi.export.NviRegressionTests;
import fr.ifremer.globe.core.io.nvi.export.S7kToNviExportTests;
import fr.ifremer.globe.core.io.segy.SegyTest;
import fr.ifremer.globe.core.io.vel.export.VelRegressionTests;
import fr.ifremer.globe.core.io.xsf.TestXsfVsMBG;
import fr.ifremer.globe.core.io.xsf.correction.TestXsfDepthCorrection;
import fr.ifremer.globe.core.merge.mbg.MbgMergeSimpleTests;
import fr.ifremer.globe.core.merge.mbg.MbgMergeWithTimeIntervalTests;
import fr.ifremer.globe.core.merge.nvi.NviMergeSimpleTests;
import fr.ifremer.globe.core.navigation.ApplyNavigationTests;
import fr.ifremer.globe.core.projection.ProjectionTest;
import fr.ifremer.globe.core.utils.TestFormatLatitudeLongitude;
import fr.ifremer.globe.core.utils.latlongformatters.DegMinSecFormatterTests;
import fr.ifremer.globe.core.utils.latlongformatters.GeoUtilsTests;
import fr.ifremer.globe.core.wc.NearestFinderTest;
import fr.ifremer.globe.core.wc.SlidingBufferTest;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Test suite for maven , declared test will be launched while compiling with command mvn integration-test
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ //
		ApplyNavigationTests.class, //
		SlidingBufferTest.class, //
		NearestFinderTest.class, //
		GeoUtilsTests.class, //
		DegMinSecFormatterTests.class, //
		TestFormatLatitudeLongitude.class, //
		ProjectionTest.class, //
		GeoTest.class, //
		DtmResolutionComputerTest.class, //

		// XSF
		TestXsfVsMBG.class, //
		TestXsfDepthCorrection.class, //
		// MBG
		MBGTestReader.class, //
		MbgValidityMapperTest.class, //
		TestProjection.class, //
		MbgFlagUpdateTest.class, //
		// NVI
		NviRegressionTests.class, //
		// VEL
		VelRegressionTests.class, //
		// SEGY
		SegyTest.class, //
		// Process Correction, Filtri
		MBGTestBiasCorrectionOneAntenna.class, //
		MBGTestBiasCorrectionMultiAntennas.class, //
		MBGTestDepthCorrectionOneAntenna.class, //
		MBGTestDepthCorrectionMultiAntennas.class, //
		MBGTestBiasCorrectionAntennaFilter.class, //
		MBGTestSoundVelocityCorrection.class, //
		MbgFiltTriTest.class, //

		// Conversion .mbg to .csv
		MBGToValidityFlagTest.class, //

		// Conversion .all to .mbg/.nvi
		// @Test FIXME 24/07/20: find why the new converter doesn't handle this old
		// file.
		// KongsbergMBGTestConverterOldFormatAll.class, //
		KongsbergMBGTestConverter.class, //
		AllToNviExportTests.class, //

		// Conversion .s7f to .mbg/.nvi
		S7K_7111_MBGTestConverter.class, //
		S7K_7125_MBGTestConverter.class, //
		S7K_7150_Before_19_3_2011_MBGTestConverter.class, //
		S7kToNviExportTests.class, //

		// Conversion .mbg to .nvi
		MbgToNviExportTests.class, //

		// merge
		MbgMergeSimpleTests.class, MbgMergeWithTimeIntervalTests.class, NviMergeSimpleTests.class, //

		// Gson
		TestRecord.class })
public class MvnTestSuite {	
	@AfterClass
	public static void cleanUp() {
		TemporaryCache.cleanAllFiles();
	}
}
