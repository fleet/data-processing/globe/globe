package fr.ifremer.globe.core.merge.nvi;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.LongRange;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.info.HistoryConstants;
import fr.ifremer.globe.core.io.nvi.info.NviConstants;
import fr.ifremer.globe.core.io.nvi.info.AbstractNviInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.merge.NviMergeProcess;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Class to test NVI merge
 */
public class NviMergeSimpleTests {
	private static final Logger LOGGER = LoggerFactory.getLogger(NviMergeSimpleTests.class);

	public static final String TEST_DIR = GlobeTestUtil.getTestDataPath() + "/file/FusNVI/";
	public static final String TEST_DIR_INPUT = TEST_DIR + "In/";
	public static final String TEST_DIR_OUTPUT = TEST_DIR + "Out/";

	/** Input files */
	public static final String NVI_FILE1 = TEST_DIR_INPUT + "file1.nvi";
	public static final String NVI_FILE2 = TEST_DIR_INPUT + "file2.nvi";
	public static final String NVI_FILE3 = TEST_DIR_INPUT + "file3.nvi";

	/** Reference files */
	public static final String NVI_OUT_FULL_NO_CUT = TEST_DIR_OUTPUT + "Fus-Full-No-Cut.nvi";
	public static final String NVI_OUT_WITH_CUT = TEST_DIR_OUTPUT + "Fus-With-Cut.nvi";

	private static String outputNviGlobe;

	/**
	 * Test a simple merge of 1 file, without time interval. (== like a copy)
	 */
	@Test
	public void simpleMerge1FileTest() throws IOException, NCException, GIOException, BadParameterException {
		List<IDataContainerInfo> inputFiles = Arrays.asList((AbstractNviInfo) IFileService.grab().getFileInfo(NVI_FILE1).get());
		String outputNviGlobe = TemporaryCache.createTemporaryFile(getClass().getName(), ".nvi").getAbsolutePath();
		try {
			NviMergeProcess nviMergeTool = new NviMergeProcess(inputFiles);
			nviMergeTool.setOutputFilePath(outputNviGlobe);
			nviMergeTool.setTimeInterval(Optional.empty());

			nviMergeTool.run(new NullProgressMonitor());

			compareFiles(NVI_FILE1, outputNviGlobe);
		} finally {
			FileUtils.deleteQuietly(new File(outputNviGlobe));
		}
	}

	/**
	 * Test a simple merge of 3 files, without time interval.
	 */
	@Test
	public void simpleMerge3FilesTest() throws IOException, NCException, GIOException, BadParameterException {
		List<IDataContainerInfo> inputFiles = Arrays.asList(//
				IDataContainerInfoService.grab().getDataContainerInfo(NVI_FILE2).get(), //
				IDataContainerInfoService.grab().getDataContainerInfo(NVI_FILE3).get(), //
				IDataContainerInfoService.grab().getDataContainerInfo(NVI_FILE1).get());
		String outputNviGlobe = TemporaryCache.createTemporaryFile(getClass().getName(), ".nvi").getAbsolutePath();

		try {
			NviMergeProcess nviMergeTool = new NviMergeProcess(inputFiles);
			nviMergeTool.setOutputFilePath(outputNviGlobe);
			nviMergeTool.setTimeInterval(Optional.empty());

			nviMergeTool.run(new NullProgressMonitor());

			compareFiles(NVI_OUT_FULL_NO_CUT, outputNviGlobe);
		} finally {
			FileUtils.deleteQuietly(new File(outputNviGlobe));
		}

	}

	/**
	 * Test a simple merge of 3 files, without time interval.
	 */
	@Test
	public void simpleMergeFileWithIntervalTest() throws IOException, NCException, GIOException, BadParameterException {
		List<IDataContainerInfo> inputFiles = Arrays.asList(//
				IDataContainerInfoService.grab().getDataContainerInfo(NVI_FILE2).get(), //
				IDataContainerInfoService.grab().getDataContainerInfo(NVI_FILE3).get(), //
				IDataContainerInfoService.grab().getDataContainerInfo(NVI_FILE1).get());

		outputNviGlobe = TemporaryCache.createTemporaryFile(getClass().getName(), ".nvi").getAbsolutePath();
		try {
			LongRange timeInverval = new LongRange(Instant.parse("2012-06-07T07:10:00.000Z").toEpochMilli(),
					Instant.parse("2012-06-07T07:40:10.000Z").toEpochMilli());

			NviMergeProcess nviMergeTool = new NviMergeProcess(inputFiles);
			nviMergeTool.setOutputFilePath(outputNviGlobe);
			nviMergeTool.setTimeInterval(Optional.of(timeInverval));
			nviMergeTool.run(new NullProgressMonitor());

			compareFiles(NVI_OUT_WITH_CUT, outputNviGlobe);
		} finally {
			FileUtils.deleteQuietly(new File(outputNviGlobe));
		}

	}

	/**
	 * Defines attributes and variables to ignore
	 */
	protected NetcdfComparatorHook buildHook() {
		NetcdfComparatorHook result = new NetcdfComparatorHook();

		// Global _NCProperties different due to the NetCDF library update...
		result.ignoreGlobalAttribute("_NCProperties");
		result.ignoreGlobalAttribute("mbGeoRepresentation");
		result.ignoreGlobalAttribute("mbGeoDictionnary");

		// Name and history field are obviously different
		result.ignoreGlobalAttribute(NviConstants.Name);
		result.ignoreGlobalAttribute(NviConstants.ProjParameterValue);

		result.ignoreVariable(HistoryConstants.DATE_VALUE);
		result.ignoreVariable(HistoryConstants.TIME_VALUE);
		result.ignoreVariable(HistoryConstants.AUTOR_VALUE);
		result.ignoreVariable(HistoryConstants.COMMENT_VALUE);
		result.ignoreVariable(HistoryConstants.MODULE_VALUE);
		result.ignoreVariable(HistoryConstants.CODE_VALUE);

		// long name of "mbDate" is now "Date" instead of "Date of cycle" (same for
		// time)
		result.ignoreAttribute("/" + NviConstants.NAME_DATE, NviConstants.ATTRIBUTE_LONG_NAME);
		result.ignoreAttribute("/" + NviConstants.NAME_TIME, NviConstants.ATTRIBUTE_LONG_NAME);

		// long name of "mbImmersion" is now "Immersion" instead of "Altitude"
		result.ignoreAttribute("/" + NviConstants.NAME_IMMERSION, NviConstants.ATTRIBUTE_LONG_NAME);

		// Flag information and ProjParameterValue are now contained in String
		result.ignoreAttribute("/" + NviConstants.NAME_PFLAG, NviConstants.ATTRIBUTE_FLAG_VALUES);
		result.ignoreGlobalAttribute("/" + NviConstants.ProjParameterValue);

		return result;
	}

	protected void compareFiles(String refFile, String testFile) throws NCException, IOException {
		// Check if files exists

		Assert.assertTrue(new File(refFile).exists());

		Assert.assertTrue(new File(testFile).exists());

		StringBuilder resume = new StringBuilder("NviMerge result : \n");
		boolean testFailed = false;

		NetcdfComparatorHook hook = buildHook();
		Optional<String> errors = NetcdfComparator.compareFiles(refFile, testFile, Optional.of(hook));
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(testFile + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(testFile + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(resume.toString(), testFailed);

	}

}
