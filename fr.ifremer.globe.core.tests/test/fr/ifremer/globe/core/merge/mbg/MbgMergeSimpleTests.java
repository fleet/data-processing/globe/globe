package fr.ifremer.globe.core.merge.mbg;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.merge.MbgMergeProcess;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class MbgMergeSimpleTests {

	public static final String TEST_DIR = GlobeTestUtil.getTestDataPath() + "/file/FusMBG/";
	public static final String TEST_DIR_INPUT = TEST_DIR + "In/";
	public static final String TEST_DIR_OUTPUT = TEST_DIR + "Out/";

	/** Input files */
	public static final String MBG_FILE1 = TEST_DIR_INPUT + "FiltTri_cel_tide_0033_20160325_172722_Thalia.mbg";
	public static final String MBG_FILE2 = TEST_DIR_INPUT + "FiltTri_cel_tide_0034_20160325_173402_Thalia.mbg";
	public static final String MBG_FILE3 = TEST_DIR_INPUT + "FiltTri_cel_tide_0035_20160325_174855_Thalia.mbg";

	/** Reference files */
	public static final String MBG_OUT_FULL_NO_CUT = TEST_DIR_OUTPUT + "Fus-Full-No-Cut.mbg";
	public static final String MBG_OUT_THREE_LINES = TEST_DIR_OUTPUT + "Fus-Three-lines-Using-Line_001-to-Line_003.mbg";
	public static final String MBG_OUT_LINE_001 = TEST_DIR_OUTPUT + "Fus-Single-line-Using-Line_001.mbg";
	public static final String MBG_OUT_LINE_002 = TEST_DIR_OUTPUT + " Fus-Single-line-Using-Line_002.mbg";
	public static final String MBG_OUT_LINE_003 = TEST_DIR_OUTPUT + "Fus-Single-line-Using-Line_003.mbg";
	private static String outputMbgGlobe;

	/**
	 * Test a simple merge of 1 file, without time interval. (== like a copy)
	 */
	@Test
	public void simpleMerge1FileTest() throws IOException, NCException, GIOException, BadParameterException {
		ISounderNcInfo mbgInfo = IDataContainerInfoService.grab().getSounderNcInfo(MBG_FILE1).orElse(null);
		Assert.assertNotNull(mbgInfo);
		List<ISounderNcInfo> inputFiles = Collections.singletonList(mbgInfo);
		outputMbgGlobe = TemporaryCache.createTemporaryFile(getClass().getName(), "Merge1.mbg").getAbsolutePath();
		try {
			MbgMergeProcess mbgMergeTool = new MbgMergeProcess(inputFiles);
			mbgMergeTool.setOutputFilePath(outputMbgGlobe);
			mbgMergeTool.setTimeInterval(Optional.empty());

			mbgMergeTool.run(new NullProgressMonitor());

			// open in netCDF
			//NetcdfComparator.compareFiles(new File(MBG_FILE1), outputFile, MbgMergeTestUtils.buildHook());
			MbgMergeTestUtils.compareFiles(MBG_FILE1, outputMbgGlobe);
		} finally {
			FileUtils.deleteQuietly(new File(outputMbgGlobe));	
		}
	}

	/**
	 * Test a simple merge of 3 files, without time interval.
	 */
	@Test
	public void simpleMerge3FilesTest() throws IOException, NCException, GIOException, BadParameterException {
		ISounderNcInfo mbgInfo1 = IDataContainerInfoService.grab().getSounderNcInfo(MBG_FILE1).orElse(null);
		Assert.assertNotNull(mbgInfo1);
		ISounderNcInfo mbgInfo2 = IDataContainerInfoService.grab().getSounderNcInfo(MBG_FILE2).orElse(null);
		Assert.assertNotNull(mbgInfo2);
		ISounderNcInfo mbgInfo3 = IDataContainerInfoService.grab().getSounderNcInfo(MBG_FILE3).orElse(null);
		Assert.assertNotNull(mbgInfo3);
		List<ISounderNcInfo> inputFiles = Arrays.asList(mbgInfo2, mbgInfo1, mbgInfo3);
		outputMbgGlobe = TemporaryCache.createTemporaryFile(getClass().getName(), "Simple.mbg").getAbsolutePath();
		try {
			MbgMergeProcess mbgMergeTool = new MbgMergeProcess(inputFiles);
			mbgMergeTool.setOutputFilePath(outputMbgGlobe);
			mbgMergeTool.setTimeInterval(Optional.empty());

			mbgMergeTool.run(new NullProgressMonitor());

			// open in netCDF
			//NetcdfComparator.compareFiles(new File(MBG_OUT_FULL_NO_CUT), outputFile, MbgMergeTestUtils.buildHook());
			MbgMergeTestUtils.compareFiles(MBG_OUT_FULL_NO_CUT, outputMbgGlobe);
		} finally {
			FileUtils.deleteQuietly(new File(outputMbgGlobe));
		}

	}

}
