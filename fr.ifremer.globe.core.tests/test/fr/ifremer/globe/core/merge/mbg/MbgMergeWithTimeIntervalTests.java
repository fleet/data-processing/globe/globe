package fr.ifremer.globe.core.merge.mbg;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.LongRange;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Test;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.merge.MbgMergeProcess;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class MbgMergeWithTimeIntervalTests {

	private static final String TEST_DIR = GlobeTestUtil.getTestDataPath() + "/file/FusMBG/";
	private static final String TEST_DIR_INPUT = TEST_DIR + "In/";
	private static final String TEST_DIR_OUTPUT = TEST_DIR + "Out/";

	/** Input files */
	private static final String MBG_FILE1 = TEST_DIR_INPUT + "FiltTri_cel_tide_0033_20160325_172722_Thalia.mbg";
	private static final String MBG_FILE2 = TEST_DIR_INPUT + "FiltTri_cel_tide_0034_20160325_173402_Thalia.mbg";
	private static final String MBG_FILE3 = TEST_DIR_INPUT + "FiltTri_cel_tide_0035_20160325_174855_Thalia.mbg";
	private static final List<String> INPUT_FILE_PATHS = Arrays.asList(MBG_FILE1, MBG_FILE2, MBG_FILE3);

	/** Reference files */
	private static final String MBG_REF_001 = TEST_DIR_OUTPUT + "Fus-Single-line-Using-Line_001.mbg";
	private static final String MBG_REF_002 = TEST_DIR_OUTPUT + "Fus-Single-line-Using-Line_002.mbg";
	private static final String MBG_REF_003 = TEST_DIR_OUTPUT + "Fus-Single-line-Using-Line_003.mbg";
	// private static final String MBG_REF_ALL = TEST_DIR_OUTPUT + "Fus-Three-lines-Using-Line_001-to-Line_003.mbg";

	/** Time intervals **/
	private static final LongRange TIME_INTERVAL_1 = new LongRange(
			Instant.parse("2016-03-25T17:32:29.511Z").toEpochMilli(),
			Instant.parse("2016-03-25T17:34:53.129Z").toEpochMilli());

	private static final LongRange TIME_INTERVAL_2 = new LongRange(
			Instant.parse("2016-03-25T17:40:31.511Z").toEpochMilli(),
			Instant.parse("2016-03-25T17:42:13.286Z").toEpochMilli());

	private static final LongRange TIME_INTERVAL_3 = new LongRange(
			Instant.parse("2016-03-25T17:48:36.196Z").toEpochMilli(),
			Instant.parse("2016-03-25T17:50:18.998Z").toEpochMilli());

	@Test
	public void mergeWithTimeInterval1Test() throws IOException, NCException, GIOException, BadParameterException {
		mergeWithTimeInterval(INPUT_FILE_PATHS, MBG_REF_001, TIME_INTERVAL_1);
	}

	@Test
	public void mergeWithTimeInterval2Test() throws IOException, NCException, GIOException, BadParameterException {
		mergeWithTimeInterval(INPUT_FILE_PATHS, MBG_REF_002, TIME_INTERVAL_2);
	}

	@Test
	public void mergeWithTimeInterval3Test() throws IOException, NCException, GIOException, BadParameterException {
		mergeWithTimeInterval(INPUT_FILE_PATHS, MBG_REF_003, TIME_INTERVAL_3);
	}

	/**
	 * Test merge with time interval.
	 */
	private void mergeWithTimeInterval(List<String> inputFiles, String referenceFile, LongRange timeInterval)
			throws IOException, NCException, GIOException, BadParameterException {
		// input files
		IDataContainerInfoService fileService = IDataContainerInfoService.grab();
		List<ISounderNcInfo> mbgInputFiles = inputFiles.stream()//
				.map(fileService::getSounderNcInfo)//
				.filter(Optional::isPresent) //
				.map(Optional::get)//
				.collect(Collectors.toList());

		// output file
		String outputMbgGlobe = TemporaryCache.createTemporaryFile(getClass().getName(), ".mbg").getAbsolutePath();

		try {
			// run the merge process
			MbgMergeProcess mbgMergeTool = new MbgMergeProcess(mbgInputFiles);
			mbgMergeTool.setOutputFilePath(outputMbgGlobe);
			mbgMergeTool.setTimeInterval(Optional.of(timeInterval));
			mbgMergeTool.run(new NullProgressMonitor());

			// check output file
			MbgMergeTestUtils.compareFiles(referenceFile, outputMbgGlobe);

		} finally {
			FileUtils.deleteQuietly(new File(outputMbgGlobe));	
		}
	}

}
