package fr.ifremer.globe.core.merge.mbg;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.merge.nvi.NviMergeSimpleTests;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;

public class MbgMergeTestUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(NviMergeSimpleTests.class);

	/**
	 * Constructor
	 */
	private MbgMergeTestUtils() {
		// private constructor : utility class
	}
	
	/**
	 * @return hook form
	 */
	protected static NetcdfComparatorHook buildHook() {
		NetcdfComparatorHook result = new NetcdfComparatorHook();
		
		// Global _NCProperties different due to the NetCDF library update...
		result.ignoreGlobalAttribute("_NCProperties");
		result.ignoreGlobalAttribute("mbGeoRepresentation");
		result.ignoreGlobalAttribute("mbGeoDictionnary");
		
		// Attributes ignored because re-computed by mergetool
		result.ignoreGlobalAttribute("mbName");
		result.ignoreGlobalAttribute("mbNbrHistoryRec");
		result.ignoreGlobalAttribute("mbVersion");
		
		// Not managed
		result.ignoreGlobalAttribute("mbShip");
		result.ignoreGlobalAttribute("mbSurvey");
		result.ignoreGlobalAttribute("mbReference");

		// geobox computed from sounding position (and not from swath position)
		result.ignoreGlobalAttribute("mbNorthLatitude");
		result.ignoreGlobalAttribute("mbSouthLatitude");
		result.ignoreGlobalAttribute("mbEastLongitude");
		result.ignoreGlobalAttribute("mbWestLongitude");
		
		// Attributes ignored because of type double precision
		result.ignoreGlobalAttribute("mbMinDepth");
		result.ignoreGlobalAttribute("mbMaxDepth");

		// Attributes ignored because initialized by merge tool
		result.ignoreGlobalAttribute("mbAutomaticCleaning");
		result.ignoreGlobalAttribute("mbManualCleaning");
		result.ignoreGlobalAttribute("mbPositionCorrection");
		result.ignoreGlobalAttribute("mbVelocityCorrection");
		result.ignoreGlobalAttribute("mbBiasCorrection");
		result.ignoreGlobalAttribute("mbTideCorrection");
		result.ignoreGlobalAttribute("mbSoundingCorrection");
		result.ignoreGlobalAttribute("mbTxAntennaLeverArm");// Created in Caraibes but values to 0 (not exist in input files); not created by Globe
        // warning: mbAcrossAngleCorrect: yes if angles % vertical: some old mbg files (EM300, EM710, EM122) are bad across angles M
		// MbgUpd corrects this problem
		result.ignoreGlobalAttribute("mbInstallParameters");
		result.ignoreGlobalAttribute("mbTideType");
		result.ignoreGlobalAttribute("mbTideRef");
		result.ignoreGlobalAttribute("mbBiasCorrectionRef");
		
		result.ignoreAttribute("/mbQuality", "long_name");
		result.ignoreAttribute("/mbSQuality", "long_name");
		
		result.setToleranceForAttribute("mbEllipsoidE2", 1.12E-16);
		result.setToleranceForAttribute("mbEllipsoidInvF", 1.12E-16);

		// Variables not considered by merge tool
		result.ignoreVariable("mbHistDate");
		result.ignoreVariable("mbHistTime");
		result.ignoreVariable("mbHistAutor");
		result.ignoreVariable("mbHistCode");
		result.ignoreVariable("mbHistModule");
		result.ignoreVariable("mbHistComment");
		result.ignoreVariable("mbVelProfilRef");
		result.ignoreVariable("mbVelProfilIdx");
		result.ignoreVariable("mbVelProfilDate");
		result.ignoreVariable("mbVelProfilTime");
		
		return result;
	}
	public static void compareFiles(String refFile, String testFile) throws NCException, IOException {
		// Check if  files exists

		Assert.assertTrue(new File (refFile).exists());

		Assert.assertTrue(new File (testFile).exists());

		StringBuilder resume = new StringBuilder("MbgMerge result : \n");
		boolean testFailed = false;

		NetcdfComparatorHook hook = buildHook();
		Optional<String> errors = NetcdfComparator.compareFiles(refFile, testFile, Optional.of(hook));
		if (errors.isPresent()) {
			testFailed = true;
			resume.append(testFile + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(testFile + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(testFailed);
		
	}
}
