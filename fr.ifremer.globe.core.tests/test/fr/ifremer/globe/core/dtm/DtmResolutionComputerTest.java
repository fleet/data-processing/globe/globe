package fr.ifremer.globe.core.dtm;

import java.io.File;
import java.util.Collections;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.api.xsf.converter.MbgConverterParameters;
import fr.ifremer.globe.api.xsf.converter.SounderFileConverter;
import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.core.model.dtm.DtmDefaultResolutionComputer;
import fr.ifremer.globe.core.model.dtm.SounderDataDtmResolutionComputer;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class DtmResolutionComputerTest {
	// Logger
	private static final Logger LOGGER = LoggerFactory.getLogger(DtmResolutionComputerTest.class);

	// Input reference file
	public static final String TEST_ALL_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/xsf/xsfUnitTests/all/0095_20141031_133613_Thalia_small.all";
	public static final String TEST_GLOBE_ALL_MBG_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/0095_20141031_133613_Thalia_small-GLOBE.mbg";
	public static final String TEST_GLOBE_ALL_XSF_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/0095_20141031_133613_Thalia_small-GLOBE.xsf.nc";

	public static final String TEST_S7K_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/Reson/20130322_204233_PP-7150-24kHz-.s7k";
	public static final String TEST_GLOBE_S7K_XSF_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/20130322_204233_PP-7150-24kHz-GLOBE.xsf.nc";

	public static final String TEST_KMALL_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/xsf/xsfUnitTests/kmall/0018_20170306_141335_Pingeline_small.kmall";
	public static final String TEST_GLOBE_KMALL_XSF_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/generated/0018_20170306_141335_Pingeline_small.xsf.nc";

	@Test
	public void testDtmDefaultResolutionComputer() throws GException {

		// Test LongLat projection
		Projection longLatProjection = new Projection(
				new ProjectionSettings(StandardProjection.LONGLAT.proj4String()));

		DtmDefaultResolutionComputer computer = new DtmDefaultResolutionComputer(longLatProjection);
		double longLatDefaultResolution = computer.computeGridDefaultResolution();

		// Default resolution is 60/16. = 3.75
		Assert.assertEquals(3.75, longLatDefaultResolution, 1e-13);

		// Test Mercator projection. Should fail, only LongLat projection returns a value
		Projection mercatorProjection = new Projection(
				new ProjectionSettings(StandardProjection.MERCATOR.proj4String()));

		computer.setProjection(mercatorProjection);

		double mercatorDefaultResolution = computer.computeGridDefaultResolution();
		Assert.assertEquals(Double.NaN, mercatorDefaultResolution, 0);
	}

	@Test
	public void testSounderDataDtmResolutionComputerFromAllToMbg() throws GException {

		StringBuilder resume = new StringBuilder("testSounderDataDtmResolutionComputerFromAllToMbg result : \n");

		// Setup process
		boolean overwrite = true;
		MbgConverterParameters parameters = new MbgConverterParameters(TEST_ALL_FILE, TEST_GLOBE_ALL_MBG_FILE,
				overwrite);
		try {
			// parse the file into a MBG file in the output dir
			new SounderFileConverter().convertToMbg(parameters);
		} catch (Exception e) {
			File outputFile = new File(TEST_GLOBE_ALL_MBG_FILE);
			if (outputFile != null) {
				outputFile.delete();
			}
			e.printStackTrace();
		}

		double mercatorResolution = checkDtmResolutionComputer(TEST_GLOBE_ALL_MBG_FILE);
		Assert.assertTrue(mercatorResolution > 1. && mercatorResolution < 100.);

		resume.append("computed DTM resolution : " + mercatorResolution + "\n");
		LOGGER.debug(resume.toString());
	}

	@Test
	public void testSounderDataDtmResolutionComputerFromAllToXsf() throws GException {
		checkSounderDataDtmResolutionComputerXsf(TEST_ALL_FILE, TEST_GLOBE_ALL_XSF_FILE);
	}

	@Test
	public void testSounderDataDtmResolutionComputerFromS7kToXsf() throws GException {
		checkSounderDataDtmResolutionComputerXsf(TEST_S7K_FILE, TEST_GLOBE_S7K_XSF_FILE);
	}

	@Test
	public void testSounderDataDtmResolutionComputerFromKmallToXsf() throws GException {
		checkSounderDataDtmResolutionComputerXsf(TEST_KMALL_FILE, TEST_GLOBE_KMALL_XSF_FILE);
	}

	private void checkSounderDataDtmResolutionComputerXsf(String inputFilename, String outputFilename)
			throws GException {

		StringBuilder resume = new StringBuilder("testSounderDataDtmResolutionComputerXsf result : \n");
		resume.append("Source file : " + inputFilename + "\n");
		resume.append("Dest file : " + outputFilename + "\n");
		// Setup process
		boolean overwrite = true;
		boolean ignoreWC = true;
		XsfConverterParameters parameters = new XsfConverterParameters(inputFilename, outputFilename, overwrite,
				ignoreWC);
		try {
			// parse the file into a XSF file in the output dir
			new SounderFileConverter().convertToXsf(parameters);
		} catch (Exception e) {
			File outputFile = new File(outputFilename);
			if (outputFile != null) {
				outputFile.delete();
			}
			e.printStackTrace();
		}

		double mercatorResolution = checkDtmResolutionComputer(outputFilename);
		Assert.assertTrue(mercatorResolution > 1. && mercatorResolution < 100.);

		resume.append("computed DTM resolution : " + mercatorResolution + "\n");
		LOGGER.debug(resume.toString());
	}

	private double checkDtmResolutionComputer(String filepath) throws GException {

		// Get sounder info
		Optional<IFileInfo> fileInfo = IFileService.grab().getFileInfo(filepath);
		Assert.assertTrue(fileInfo.isPresent());
		ISounderNcInfo sounderNcInfo = (ISounderNcInfo) fileInfo.get();

		// Get projection
		GeoPoint center = sounderNcInfo.getGeoBox().getCenter();
		Projection mercatorProjection = new Projection(
				StandardProjection.getTransverseMercatorProjectionSettings(center.getLong(), center.getLat()));

		// Compute resolution
		SounderDataDtmResolutionComputer computer = new SounderDataDtmResolutionComputer(
				Collections.singletonList(sounderNcInfo), mercatorProjection);

		return computer.computeGridDefaultResolution();
	}

}
