;
; Script d'installation et désinstallation Structure d'accueil Globe
;


!define PRODUCT_NAME "Globe"
!define PRODUCT_VERSION "2.5.15"
!define PRODUCT_PUBLISHER "IFREMER"
!define PRODUCT_WEB_SITE "http://www.ifremer.fr"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\Globe.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_STARTMENU_REGVAL "NSIS:StartMenuDir"
!define PRODUCT_LAUNCHER "$INSTDIR\Globe.exe"
!define PRODUCT_UNINST "$INSTDIR\uninst.exe"
!define UNINST_LOG "uninstall.log"
!define PRODUCT_LOCATION "product"
var UninstLog


SetCompressor lzma

;==================================================================================================
; INCLUDES
;==================================================================================================

; MUI 1.67 compatible ------
  !include "MUI.nsh"
  !include "ATL_locateJVM.nsh"
  !include "ATL_StrTok.nsh"
  !include "ATL_envvar.nsh"
  !include "ATL_GetBetween.nsh"
  !include "StrRep.nsh"
  !include "ReplaceInFile.nsh"
  !include "BackupFile.nsh"
  !include "LogicLib.nsh"
  !include "WriteEnvStr.nsh"

;==================================================================================================
; MUI
;==================================================================================================
!define MUI_ABORTWARNING
!define MUI_ICON "icons/ifremer.ico"
!define MUI_UNICON "icons/uninst.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
; !insertmacro MUI_PAGE_LICENSE "LICENSE"
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; page pour definition de la base
; Page custom CustomLoadParametersBase CustomGetParametersBase

;==================================================================================================
; VARIABLES
;==================================================================================================
;  Var _hostname
;  Var _port
;  Var _login
;  Var _password
;  Var _database
  
; Start menu page
var ICONS_GROUP
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${PRODUCT_NAME}"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${PRODUCT_STARTMENU_REGVAL}"
!insertmacro MUI_PAGE_STARTMENU Application $ICONS_GROUP
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!define MUI_FINISHPAGE_RUN "${PRODUCT_LAUNCHER}"
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

;==================================================================================================
; LANGUAGE
;==================================================================================================
!insertmacro MUI_LANGUAGE "English"

; Reserve files
!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "GlobeSetup-${PRODUCT_VERSION}.exe"
InstallDir "C:\${PRODUCT_NAME}"
;InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails hide

Section "MainSection" SEC01
  ;Rmdir /r "$INSTDIR"
  SetOutPath "$INSTDIR"
  SetOverwrite try

  File /r /x .svn  "${PRODUCT_LOCATION}\*"
    
; Shortcuts
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  SetShellVarContext all
  CreateDirectory "$SMPROGRAMS\$ICONS_GROUP"
  SetOutPath "$INSTDIR\"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\${PRODUCT_NAME}.lnk" "${PRODUCT_LAUNCHER}"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Uninstall ${PRODUCT_NAME}.lnk" "${PRODUCT_UNINST}"
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "${PRODUCT_LAUNCHER}"
  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd


Section -Post
  WriteUninstaller "${PRODUCT_UNINST}"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "${PRODUCT_LAUNCHER}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "${PRODUCT_UNINST}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "${PRODUCT_LAUNCHER}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd


Function .onInit
 
  
 
FunctionEnd


Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name): Uninstall was completed successfully."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Do you want to uninstall $(^Name) ?" IDYES +2

  Abort
FunctionEnd

Section Uninstall
  !insertmacro MUI_STARTMENU_GETFOLDER "Application" $ICONS_GROUP

  Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\${PRODUCT_NAME}.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\Uninstall ${PRODUCT_NAME}.lnk"

  Delete "$INSTDIR\$UNINST_LOG"
  RMDir /r "$INSTDIR"
   
  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd
