!define LocateMYSQL "!insertmacro LocateMYSQL"

!macro LocateMYSQL MYSQLVersion
  Push "${MYSQLVersion}"
  Call LocateMYSQL
!macroend

Var MYSQL_VER
Var MYSQL_INSTALLATION_MSG

Function LocateMYSQL
    ;Get input from user
    Exch $R0
    
    ;Check for Java version and location
    Push $0
    Push $1

    ReadRegStr $MYSQL_VER HKLM "SOFTWARE\MySQL AB\MySQL Server $R0" Version
    StrCmp "" "$MYSQL_VER" MYSQLNotPresent done

    MYSQLNotPresent:
        StrCpy $MYSQL_INSTALLATION_MSG "MySQL version $R0 is not installed on your computer."
        Goto Done

    Done:
        Pop $1
        Pop $0
        Push $R0

FunctionEnd
