!macro GetBetween This AndThis In Return
Push "${This}"
Push "${AndThis}"
Push "${In}"
 Call GetBetween
Pop "${Return}"
!macroend
!define GetBetween "!insertmacro GetBetween"

!macro un.GetBetween This AndThis In Return
Push "${This}"
Push "${AndThis}"
Push "${In}"
 Call un.GetBetween
Pop "${Return}"
!macroend
!define un.GetBetween "!insertmacro GetBetween"

Function GetBetween
 Exch $R0 ; file
 Exch
 Exch $R1 ; before this (marker 2)
 Exch 2
 Exch $R2 ; after this  (marker 1)
 Exch 2
 Push $R3 ; marker 1 len
 Push $R4 ; marker 2 len
 Push $R5 ; marker pos
 Push $R6 ; file handle
 Push $R7 ; current line string
 Push $R8 ; current chop

 FileOpen $R6 $R0 r

 StrLen $R4 $R2
 StrLen $R3 $R1

 StrCpy $R0 ""

 Read1:
  ClearErrors
  FileRead $R6 $R7
  IfErrors Done
  StrCpy $R5 0

 FindMarker1:
  IntOp $R5 $R5 - 1
  StrCpy $R8 $R7 $R4 $R5
  StrCmp $R8 "" Read1
  StrCmp $R8 $R2 0 FindMarker1
   IntOp $R5 $R5 + $R4
   StrCpy $R7 $R7 "" $R5

  StrCpy $R5 -1
  Goto FindMarker2

 Read2:
  ClearErrors
  FileRead $R6 $R7
  IfErrors Done
  StrCpy $R5 -1

 FindMarker2:
  IntOp $R5 $R5 + 1
  StrCpy $R8 $R7 $R3 $R5
  StrCmp $R8 "" 0 +3
   StrCpy $R0 $R0$R7
  Goto Read2
  StrCmp $R8 $R1 0 FindMarker2
   StrCpy $R7 $R7 $R5
   StrCpy $R0 $R0$R7

 Done:
  FileClose $R6

 Pop $R8
 Pop $R7
 Pop $R6
 Pop $R5
 Pop $R4
 Pop $R3
 Pop $R2
 Pop $R1
 Exch $R0
FunctionEnd

Function un.GetBetween
 Exch $R0 ; file
 Exch
 Exch $R1 ; before this (marker 2)
 Exch 2
 Exch $R2 ; after this  (marker 1)
 Exch 2
 Push $R3 ; marker 1 len
 Push $R4 ; marker 2 len
 Push $R5 ; marker pos
 Push $R6 ; file handle
 Push $R7 ; current line string
 Push $R8 ; current chop

 FileOpen $R6 $R0 r

 StrLen $R4 $R2
 StrLen $R3 $R1

 StrCpy $R0 ""

 Read1:
  ClearErrors
  FileRead $R6 $R7
  IfErrors Done
  StrCpy $R5 0

 FindMarker1:
  IntOp $R5 $R5 - 1
  StrCpy $R8 $R7 $R4 $R5
  StrCmp $R8 "" Read1
  StrCmp $R8 $R2 0 FindMarker1
   IntOp $R5 $R5 + $R4
   StrCpy $R7 $R7 "" $R5

  StrCpy $R5 -1
  Goto FindMarker2

 Read2:
  ClearErrors
  FileRead $R6 $R7
  IfErrors Done
  StrCpy $R5 -1

 FindMarker2:
  IntOp $R5 $R5 + 1
  StrCpy $R8 $R7 $R3 $R5
  StrCmp $R8 "" 0 +3
   StrCpy $R0 $R0$R7
  Goto Read2
  StrCmp $R8 $R1 0 FindMarker2
   StrCpy $R7 $R7 $R5
   StrCpy $R0 $R0$R7

 Done:
  FileClose $R6

 Pop $R8
 Pop $R7
 Pop $R6
 Pop $R5
 Pop $R4
 Pop $R3
 Pop $R2
 Pop $R1
 Exch $R0
FunctionEnd