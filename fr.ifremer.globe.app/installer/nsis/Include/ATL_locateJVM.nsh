!define LocateJVM "!insertmacro LocateJVM"

!macro LocateJVM JVVMVersion
  Push "${JVVMVersion}"
  Call LocateJVM
!macroend

Var JAVA_HOME
Var JAVA_VER
Var JAVA_INSTALLATION_MSG

Function LocateJVM
    ;Get input from user
    Exch $R0

    ;Check for Java version and location
    Push $0
    Push $1

    ReadRegStr $JAVA_VER HKLM "SOFTWARE\JavaSoft\Java Runtime Environment" CurrentVersion
    StrCmp "" "$JAVA_VER" JavaNotPresent CheckJavaVer

    JavaNotPresent:
        StrCpy $JAVA_INSTALLATION_MSG "Java Runtime Environment is not installed on your computer. You need version $JAVA_VER or newer to run this program."
        Goto Done

    CheckJavaVer:
        ReadRegStr $0 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment\$JAVA_VER" JavaHome
        GetFullPathName /SHORT $JAVA_HOME "$0"
        StrCpy $0 $JAVA_VER 1 0
        StrCpy $1 $JAVA_VER 1 2
        StrCpy $JAVA_VER "$0$1"
        IntCmp $R0 $JAVA_VER FoundCorrectJavaVer FoundCorrectJavaVer JavaVerNotCorrect

    FoundCorrectJavaVer:
        IfFileExists "$JAVA_HOME\bin\javaw.exe" 0 JavaNotPresent
        ;MessageBox MB_OK "Found Java: $JAVA_VER at $JAVA_HOME"
        Goto Done

    JavaVerNotCorrect:
        StrCpy $JAVA_INSTALLATION_MSG "The version of Java Runtime Environment installed on your computer is $JAVA_VER. Version $JAVA_VER or newer is required to run this program."

    Done:
        Pop $1
        Pop $0
        Push $R0

FunctionEnd
