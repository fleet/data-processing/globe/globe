Function GetDrives
	!define GetDrives `!insertmacro GetDrivesCall`

	!macro GetDrivesCall _DRV _FUNC
		Push $0
		Push `${_DRV}`
		GetFunctionAddress $0 `${_FUNC}`
		Push `$0`
		Call GetDrives
		Pop $0
	!macroend

	Exch $1
	Exch
	Exch $0
	Exch
	Push $2
	Push $3
	Push $4
	Push $5
	Push $6
	Push $8
	Push $9

	System::Alloc /NOUNLOAD 1024
	Pop $2
	System::Call /NOUNLOAD 'kernel32::GetLogicalDriveStringsA(i,i) i(1024, r2)'

	StrCmp $0 ALL drivestring
	StrCmp $0 '' 0 typeset
	StrCpy $0 ALL
	goto drivestring

	typeset:
	StrCpy $6 -1
	IntOp $6 $6 + 1
	StrCpy $8 $0 1 $6
	StrCmp $8$0 '' enumex
	StrCmp $8 '' +2
	StrCmp $8 '+' 0 -4
	StrCpy $8 $0 $6
	IntOp $6 $6 + 1
	StrCpy $0 $0 '' $6

	StrCmp $8 'FDD' 0 +3
	StrCpy $6 2
	goto drivestring
	StrCmp $8 'HDD' 0 +3
	StrCpy $6 3
	goto drivestring
	StrCmp $8 'NET' 0 +3
	StrCpy $6 4
	goto drivestring
	StrCmp $8 'CDROM' 0 +3
	StrCpy $6 5
	goto drivestring
	StrCmp $8 'RAM' 0 typeset
	StrCpy $6 6

	drivestring:
	StrCpy $3 $2

	enumok:
	System::Call /NOUNLOAD 'kernel32::lstrlenA(t) i(i r3) .r4'
	StrCmp $4$0 '0ALL' enumex
	StrCmp $4 0 typeset
	System::Call /NOUNLOAD 'kernel32::GetDriveTypeA(t) i(i r3) .r5'

	StrCmp $0 ALL +2
	StrCmp $5 $6 letter enumnext
	StrCmp $5 2 0 +3
	StrCpy $8 FDD
	goto letter
	StrCmp $5 3 0 +3
	StrCpy $8 HDD
	goto letter
	StrCmp $5 4 0 +3
	StrCpy $8 NET
	goto letter
	StrCmp $5 5 0 +3
	StrCpy $8 CDROM
	goto letter
	StrCmp $5 6 0 enumex
	StrCpy $8 RAM

	letter:
	System::Call /NOUNLOAD '*$3(&t1024 .r9)'

	Push $0
	Push $1
	Push $2
	Push $3
	Push $4
	Push $5
	Push $6
	Push $8
	Call $1
	Pop $9
	Pop $8
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Pop $0
	StrCmp $9 'StopGetDrives' enumex

	enumnext:
	IntOp $3 $3 + $4
	IntOp $3 $3 + 1
	goto enumok

	enumex:
	System::Free $2

	Pop $9
	Pop $8
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Pop $0
FunctionEnd