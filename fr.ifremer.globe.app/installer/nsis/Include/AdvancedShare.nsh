!macro CreateNewShare USERNAME SHARENAME SHARE_TYPE SHARE_COMMENT SHARE_PERMISSIONS ACL_ACCESS MAX_USERS CURRENT_USES SHARE_PATH SHARE_PASS
# Get the user's SID from the username
  System::Call /NOUNLOAD '*(&w${NSIS_MAX_STRLEN})i.R9'
  System::Call /NOUNLOAD 'advapi32::LookupAccountNameA(,t "${USERNAME}",i R9,*i ${NSIS_MAX_STRLEN},w .R8,*i ${NSIS_MAX_STRLEN},*i .r4)i.r5'
  System::Call /NOUNLOAD 'advapi32::ConvertSidToStringSid(i R9,*t .R8)i.r5'
${If} $R8 == ''
  MessageBox MB_OK|MB_ICONSTOP|MB_TOPMOST 'Utilisateur "${USERNAME}" inexistant!$\n'
  System::Free $R9
  Quit
${EndIf}
# Create an EXPLICIT_ACCESS structure and place it on $R6
  System::Call /NOUNLOAD '*${strEXPLICIT_ACCESS}(${ACL_ACCESS},${SET_ACCESS},${NO_INHERITANCE},0,${NO_MULTIPLE_TRUSTEE},${TRUSTEE_IS_SID},${TRUSTEE_IS_USER},$R9).R6'
  System::Call /NOUNLOAD 'advapi32::SetEntriesInAclA(i 1,i R6,,*i .R5)i.r1'
  ${If} $1 <> 0
    System::Free $R9
    System::Free $R6
    Quit
  ${EndIf}
# Create an empty security descriptor and place it in R4.
  System::Alloc ${NSIS_MAX_STRLEN}
  Pop $R4
  System::Call /NOUNLOAD 'advapi32::InitializeSecurityDescriptor(i R4,i ${SECURITY_DESCRIPTOR_REVISION})i.r1'
  ${If} $1 == 0
    System::Free $R9
    System::Free $R6
    System::Call 'kernel32::LocalFree(i R5)i.r0'
    Quit
  ${EndIf}
# Add the ACL to the security descriptor
  System::Call /NOUNLOAD 'advapi32::SetSecurityDescriptorDacl(i R4,i 1,i R5,i 0)i.r1'
  ${If} $1 == 0
    System::Free $R9
    System::Free $R6
    System::Call 'kernel32::LocalFree(i R5)i.r0'
    Quit
  ${EndIf}
# Generate the structure that holds the share info and place it on $R0
  System::Call /NOUNLOAD '*${strSHARE_INFO_502}("${SHARENAME}",${SHARE_TYPE},"${SHARE_COMMENT}",${SHARE_PERMISSIONS},${MAX_USERS},${CURRENT_USES},"${SHARE_PATH}","${SHARE_PASS}",0,R4).R0'
  System::Call /NOUNLOAD 'netapi32::NetShareAdd(, i 502, i R0, *i .R1) i .r1'
  ${If} $1 <> 0
  ${AndIf} $1 <> 2118
    MessageBox MB_OK|MB_ICONSTOP|MB_TOPMOST 'Erreur � la cr�ation du partage !'
  ${EndIf}
# Cleanup
  System::Free $R9
  System::Free $R6
  System::Call 'kernel32::LocalFree(i R5)i.r0'
  System::Free $R0
!macroend

!macro CreateGroup SERVER_NAME GROUP_NAME GROUP_DESCRIPTION
  # Create a local group
  System::Call '*(w "${GROUP_NAME}",w "${GROUP_DESCRIPTION}")i.R0'
  System::Call 'netapi32::NetLocalGroupAdd(w "${SERVER_NAME}",i 1,i R0,*i r0)i.r1'
  System::Free $R0
!macroend