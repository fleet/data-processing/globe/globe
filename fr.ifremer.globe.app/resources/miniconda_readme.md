#How to configure MINICONDA for Globe

### Solution 1: get configured Miniconda from Ifremer

Retrieve miniconda.zip from '\\meskl1\dev\GLOBE\05 DEV\miniconda' and unzip it in 'fr.ifremer.globe.app/resources/miniconda'.

### Solution 2: download Miniconda, configure it with PyAt requirements

* 1) Download & install miniconda : https://docs.conda.io/en/latest/miniconda.html
 
* 2) Configure miniconda environment with PyAt requierements : 

	conda env update -p #CONDA_ENV_PATH# -f #PYAT_REQUIEREMENT_YML#

CONDA_ENV_PATH = conda default environment path (could be obtain with 'conda info --env' command)

PYAT_REQUIEREMENT_YML = path to the PyAt configuration file (for example: 'requirements_runtime.yml')

* 3) Use conda-pack to zip the environment : https://conda.github.io/conda-pack/

	conda pack -p #CONDA_ENV_PATH# -o #ZIP_PATH#

ZIP_PATH = output zip path (ended with .zip for windows) 

* 4) Unzip it in 'fr.ifremer.globe.app/resources/miniconda'.
