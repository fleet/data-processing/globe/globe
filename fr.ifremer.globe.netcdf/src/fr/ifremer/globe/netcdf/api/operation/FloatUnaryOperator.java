package fr.ifremer.globe.netcdf.api.operation;

import java.util.Objects;
import java.util.function.DoubleUnaryOperator;

/**
 * Represents an operation on a single {@code float}-valued operand that produces
 * a {@code float}-valued result.  This is the primitive type specialization of
 * {@link UnaryOperator} for {@code float}.
 * <p> It is a copy of {@link DoubleUnaryOperator} specialized for float type (missing in jdk)
 *
 * <p> @see {@link java.util.function.DoubleUnaryOperator}
 *
 */
@FunctionalInterface
public interface FloatUnaryOperator {

    /**
     * @see DoubleUnaryOperator
     * */
    float applyAsFloat(float operand);

    /**
     * @see DoubleUnaryOperator
     * */
    default FloatUnaryOperator compose(FloatUnaryOperator before) {
        Objects.requireNonNull(before);
        return (float v) -> applyAsFloat(before.applyAsFloat(v));
    }

    /**
     * @see DoubleUnaryOperator
     * */
    default FloatUnaryOperator andThen(FloatUnaryOperator after) {
        Objects.requireNonNull(after);
        return (float t) -> after.applyAsFloat(applyAsFloat(t));
    }

    /**
     * @see DoubleUnaryOperator
     * */
    static FloatUnaryOperator identity() {
        return t -> t;
    }
}
