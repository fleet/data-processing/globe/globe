package fr.ifremer.globe.netcdf.api.operation;

import java.util.function.DoubleUnaryOperator;

import ucar.units.ConversionException;
import ucar.units.Unit;

public class UnitOperation {

	private UnitOperation() {};
	
	public static  FloatUnaryOperator withFloat(Unit unitSrc, Unit unitDst){
		return value -> {
			try {
				return unitSrc.convertTo(value, unitDst);
			} catch (ConversionException e) {
				return Float.NaN;
			}
		};
	}
	
	public static  DoubleUnaryOperator withDouble(Unit unitSrc, Unit unitDst){
		return value -> {
			try {
				return unitSrc.convertTo(value, unitDst);
			} catch (ConversionException e) {
				return Double.NaN;
			}
		};
	}
}
