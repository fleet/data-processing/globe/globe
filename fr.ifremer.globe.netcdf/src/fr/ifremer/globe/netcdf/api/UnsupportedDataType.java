package fr.ifremer.globe.netcdf.api;

import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;

public class UnsupportedDataType extends NCException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	public UnsupportedDataType(DataType type) {
		this(type, "");
	}
	public UnsupportedDataType(DataType type, String message ) {
		super("Unsuppported data byte mapping (" + type.name()+") " + message);
	}
}
