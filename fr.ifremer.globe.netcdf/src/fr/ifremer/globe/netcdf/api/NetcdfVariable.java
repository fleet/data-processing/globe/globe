package fr.ifremer.globe.netcdf.api;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleUnaryOperator;
import java.util.function.IntConsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.netcdf.api.buffer.ABufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.BufferWrapperProvider;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.api.operation.FloatUnaryOperator;
import fr.ifremer.globe.netcdf.api.operation.UnitOperation;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.netcdf.ucar.NCUnitError;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.utils.array.impl.LargeArray;
import ucar.units.ConversionException;
import ucar.units.PrefixDBException;
import ucar.units.SpecificationException;
import ucar.units.StandardUnitFormat;
import ucar.units.Unit;
import ucar.units.UnitDBException;
import ucar.units.UnitSystemException;

public class NetcdfVariable extends NCVariable {

	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(NetcdfVariable.class);
	/**
	 * Name of the scale factor attribute
	 */
	protected String scaleFactorAttributeName;

	/**
	 * Name of the offset factor attribute
	 */
	protected String offsetFactorAttributeName;

	/**********************************************************************************************************
	 * CONSTRUCTORS AND PUBLIC METHODS
	 **********************************************************************************************************/

	/** Constructor **/
	public NetcdfVariable(int ncid, NCGroup group, String name, int type, List<NCDimension> shape) {
		super(ncid, group, name, type, shape);
	}

	/**
	 * @return dimensions as long array.
	 */
	public long[] getDimensions() {
		List<NCDimension> netcdfDims = getShape();
		long[] result = new long[netcdfDims.size()];
		for (int i = 0; i < result.length; i++)
			result[i] = netcdfDims.get(i).getLength();
		return result;
	}

	/** @return unpacked data type (type after applying scale factor and add offset) */
	public DataType getUnpackedDataType() throws NCException {
		DataType type = getRawDataType();
		if (hasAddOffset())
			type = getAttributeDataType(CFStandardNames.CF_ADD_OFFSET);
		if (type != DataType.DOUBLE && hasScaleFactor())
			type = getAttributeDataType(CFStandardNames.CF_SCALE_FACTOR);
		if (type == null)
			type = DataType.DOUBLE;
		return type;
	}

	/** @return missing value decoded ( value after applying scale factor and add offset) */
	public double getUnpackedMissingValue() throws NCException {
		if(hasMissingValue()) {
			double missingValue = getAttributeDouble(CFStandardNames.CF_MISSING_VALUE);
			if (hasScaleFactor())
				missingValue *= getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
			if (hasAddOffset())
				missingValue += getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
			return missingValue;
		}
		return Double.NaN;
	}

	/**
	 * @return the variable unit, or empty optional if the unit attribute is not defined
	 * @throws NCException
	 */
	private Optional<Unit> getRawUnit() throws NCException {
		if (!hasAttribute(CFStandardNames.CF_UNITS))
			return Optional.empty();

		String srcUnitString = getAttributeText(CFStandardNames.CF_UNITS);
		Unit unitSrc = null;
		try {
			unitSrc = StandardUnitFormat.instance().parse(srcUnitString);
		} catch (NoClassDefFoundError | SpecificationException | UnitDBException | PrefixDBException
				| UnitSystemException e) {
			// unknown src unit
			logger.error("Unit cannot be parsed (unit" + srcUnitString + ")", e);
			// throw new NCUnitError("Unit cannot be parsed (unit" + srcUnitString + ")");
		}
		return (unitSrc != null ? Optional.of(unitSrc) : Optional.empty());
	}

	/**********************************************************************************************************
	 * READING METHODS
	 **********************************************************************************************************/

	/** Functional interface to define read operation **/
	@FunctionalInterface
	private interface IReadOperation {
		void accept(int index, ABufferWrapper src, ABufferWrapper dst);
	}

	/**
	 * Reads data and put them in a buffer
	 * 
	 * @param start : the starting point where to start reading
	 * @param count : define the shape to read
	 * @param destBuffer : buffer to fill with retrieved values
	 * @param askedType : output type
	 * @param unit : output unit (optional)
	 * 
	 * @throws NCException
	 */
	public void read(long[] count, ByteBuffer destBuffer, DataType askedType, Optional<Unit> unit) throws NCException {
		// we clone the count values in order to allocate an array with the same lenght
		long[] start = count.clone();
		// put everything to zero
		for (int i = 0; i < start.length; i++) {
			start[i] = 0;
		}
		// read the full data
		read(start, count, destBuffer, askedType, unit);
	}

	/**
	 * Reads data and put them in a buffer
	 *
	 * @param count : define the shape to read
	 * @param destBuffer : buffer to fill with retrieved values
	 * @param askedType : output type
	 * @param unit : output unit (optional)
	 * 
	 * @throws NCException
	 */
	public void read(long[] start, long[] count, ByteBuffer destBuffer, DataType askedType, Optional<Unit> unit)
			throws NCException {
		checkByteOrder(destBuffer);
		checkBufferSize(destBuffer, count, askedType);
		int valueCount = (int) Arrays.stream(count).reduce(1, (a, b) -> a * b);
		LargeArray tmpArray = null;
		try {
			// get read operation (apply scale factor, add offset, unit or type change...)
			Optional<IReadOperation> operation = getReadOperation(getRawDataType(), askedType, unit);

			if (!operation.isPresent()) { 
				// if no operation necessary: put bytes directly to the destination buffer
				get_byte(start, count, destBuffer);
			} else { 
				// else: put bytes in temporary buffer, and apply operation
				// create temporary buffer
				tmpArray = LargeArray.create(valueCount, getRawDataType().getSize(), "_xsf_writebuffer_");
				ByteBuffer tmpBuffer = tmpArray.getByteBuffer(0);
				tmpBuffer.order(ByteOrder.nativeOrder());

				ABufferWrapper src = BufferWrapperProvider.get(tmpBuffer, getRawDataType());
				ABufferWrapper dst = BufferWrapperProvider.get(destBuffer, askedType);

				// put bytes in temporary buffer
				get_byte(start, count, tmpBuffer);

				// apply read operation
				for (int i = 0; i < valueCount; i++)
					operation.get().accept(i, src, dst);
			}
		} catch (Exception e) {
			throw new NCException(String.format("Error reading '%s' : %s", getName(), e.getMessage()), e);
		} catch (Throwable e) {
			throw new NCException(String.format("Error reading '%s' : %s", getName(), e.getMessage()));
		} finally {
			if (tmpArray != null)
				tmpArray.close();
		}
	}

	/**
	 * @return Returns the read operation or an empty optional if no operation is necessary
	 * 
	 * @param src : source buffer, from where data are extracted
	 * @param dst : destination buffer, where data should be write after treatment
	 * @param dstUnit : unit of the destination buffer
	 */
	private Optional<IReadOperation> getReadOperation(DataType rawType, DataType askedType, Optional<Unit> dstUnit)
			throws NCException {
		boolean unitTransformIsNeeded = false;
		Optional<Unit> srcUnit = null;

		srcUnit = dstUnit.isPresent() ? getRawUnit() : Optional.empty(); 
		unitTransformIsNeeded = dstUnit.isPresent() && srcUnit.isPresent() && srcUnit.get() != dstUnit.get()
				&& checkUnitTransform(srcUnit.get(), dstUnit.get());

		switch (getUnpackedDataType()) {
		case FLOAT:
			Optional<FloatUnaryOperator> floatOperation = Optional.empty();
			if(askedType.isFloatingPoint()) {
				if (hasMissingValue()) { // missing value
					float missingValue = getAttributeFloat(CFStandardNames.CF_MISSING_VALUE);
					if(missingValue != Float.NaN)
						floatOperation = andThen(floatOperation, v -> v == missingValue ? Float.NaN : v);
				} else if(hasFillValue()) { // fill value
					float fillValue = getAttributeFloat(CFStandardNames.CF_FILL_VALUE);
					if(fillValue != Float.NaN)
						floatOperation = andThen(floatOperation, v -> v == fillValue ? Float.NaN : v);
				}
			}
			if (hasScaleFactor()) { // Scale offset
				float scaleFactor = getAttributeFloat(CFStandardNames.CF_SCALE_FACTOR);
				floatOperation = andThen(floatOperation, v -> v * scaleFactor);
			}
			if (hasAddOffset()) {// Add offset
				float addOffset = getAttributeFloat(CFStandardNames.CF_ADD_OFFSET);
				floatOperation = andThen(floatOperation, v -> v + addOffset);
			}
			if (unitTransformIsNeeded) // Unit transform
				floatOperation = andThen(floatOperation, UnitOperation.withFloat(srcUnit.get(), dstUnit.get()));

			if (floatOperation.isPresent()) { // returns: src.readAsFloat() -> floatOperation -> dst.putAsFloat()
				final FloatUnaryOperator op = floatOperation.get();
				return Optional.of((i, src, dst) -> dst.putAsFloat(i, op.applyAsFloat(src.getAsFloat(i))));
			}
			break;

		case DOUBLE:
		default:
			Optional<DoubleUnaryOperator> doubleOperation = Optional.empty();
			if(askedType.isFloatingPoint()) {
				if (hasMissingValue()) { // missing value
					double missingValue = getAttributeDouble(CFStandardNames.CF_MISSING_VALUE);
					if(missingValue != Double.NaN)
						doubleOperation = andThen(doubleOperation, v -> v == missingValue ? Double.NaN : v);
				} else if(hasFillValue()) { // fill value
					double fillValue = getAttributeDouble(CFStandardNames.CF_FILL_VALUE);
					if(fillValue != Double.NaN)
						doubleOperation = andThen(doubleOperation, v -> v == fillValue ? Double.NaN : v);
				}
			}
			if (hasScaleFactor()) { // Scale offset
				double scaleFactor = getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
				doubleOperation = andThen(doubleOperation, v -> v * scaleFactor);
			}
			if (hasAddOffset()) {// Add offset
				double addOffset = getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
				doubleOperation = andThen(doubleOperation, v -> v + addOffset);
			}
			if (unitTransformIsNeeded)// Unit transform
				doubleOperation = andThen(doubleOperation, UnitOperation.withDouble(srcUnit.get(), dstUnit.get()));

			if (doubleOperation.isPresent()) { // returns: src.readAsDouble() -> doubleOperation -> dst.putAsDouble()
				final DoubleUnaryOperator op = doubleOperation.get();
				return Optional.of((i, src, dst) -> dst.putAsDouble(i, op.applyAsDouble(src.getAsDouble(i))));
			}
			break;
		}

		// Returns operation with only type change or nothing if type change is not necessary
		return rawType != askedType ? Optional.of((i, src, dst) -> dst.putFrom(src, i)) : Optional.empty();
	}

	/**********************************************************************************************************
	 * WRINTING METHODS
	 **********************************************************************************************************/

	/**
	 * Write data from a buffer
	 *
	 * <p>
	 * Don't forget to call <b>file.synchronize()</b> after write
	 * </p>
	 *
	 * @param count : define the shape to read
	 * @param buffer : input buffer
	 * @param providedType : input type
	 * @param unit : output unit
	 * 
	 * @throws NCException
	 */
	public void write(long[] count, ByteBuffer buffer, DataType providedType, Optional<Unit> unit) throws NCException {
		long[] start = count.clone();
		for (int i = 0; i < start.length; i++) {
			start[i] = 0;
		}

		checkByteOrder(buffer);

		// Check input buffer size is valid
		checkBufferSize(buffer, count, providedType);

		// Apply scale factor and add offset
		int valueCount = (int) Arrays.stream(count).reduce(1, (a, b) -> {
			return a * b;
		});

		// create a storage buffer
		LargeArray dstArray = null;
		try {
			dstArray = LargeArray.create(valueCount, getRawDataType().getSize(), "_xsf_writebuffer_");
			ByteBuffer tmpBuffer = dstArray.getByteBuffer(0);
			tmpBuffer.order(ByteOrder.nativeOrder());

			// Define write operation
			ABufferWrapper src = BufferWrapperProvider.get(buffer, providedType);
			ABufferWrapper dst = BufferWrapperProvider.get(tmpBuffer, getRawDataType());
			IntConsumer operation = getWriteOperation(src, dst, unit);

			for (int i = 0; i < valueCount; i++)
				operation.accept(i);

			tmpBuffer.position(0);
			put(start, count, tmpBuffer);
		} catch (IOException e) {
			throw new NCException(e.getMessage(), e);
		} finally {
			if (dstArray != null)
				dstArray.close();

		}
	}

	/**
	 * @return Returns the write operation
	 * 
	 * @param src : source buffer, from where data are extracted
	 * @param dst : destination buffer, where data should be write after treatment
	 * @param dstUnit : unit of the destination buffer
	 */
	private IntConsumer getWriteOperation(ABufferWrapper src, ABufferWrapper dst, Optional<Unit> dstUnit)
			throws NCException {

		Optional<Unit> srcUnit = getRawUnit();
		boolean unitTransformIsNeeded = dstUnit.isPresent() && srcUnit.isPresent() && srcUnit.get() != dstUnit.get()
				&& checkUnitTransform(srcUnit.get(), dstUnit.get());

		switch (getUnpackedDataType()) {
		case FLOAT:
			Optional<FloatUnaryOperator> floatOperation = Optional.empty();
			if (unitTransformIsNeeded) // Unit transform
				floatOperation = andThen(floatOperation, UnitOperation.withFloat(srcUnit.get(), dstUnit.get()));

			if (hasAddOffset()) {// Add offset
				float addOffset = getAttributeFloat(CFStandardNames.CF_ADD_OFFSET);
				floatOperation = andThen(floatOperation, v -> v - addOffset);
			}
			if (hasScaleFactor()) { // Scale offset
				float scaleFactor = getAttributeFloat(CFStandardNames.CF_SCALE_FACTOR);
				floatOperation = andThen(floatOperation, v -> v / scaleFactor);
			}

			if (floatOperation.isPresent()) { // returns: src.readAsFloat() -> floatOperation -> dst.putAsFloat()
				final FloatUnaryOperator op = floatOperation.get();
				return i -> dst.putAsFloat(i, op.applyAsFloat(src.getAsFloat(i)));
			}
			break;

		case DOUBLE:
		default:
			Optional<DoubleUnaryOperator> doubleOperation = Optional.empty();
			if (unitTransformIsNeeded)// Unit transform
				doubleOperation = andThen(doubleOperation, UnitOperation.withDouble(srcUnit.get(), dstUnit.get()));

			if (hasAddOffset()) {// Add offset
				double addOffset = getAttributeDouble(CFStandardNames.CF_ADD_OFFSET);
				doubleOperation = andThen(doubleOperation, v -> v - addOffset);
			}
			if (hasScaleFactor()) { // Scale offset
				double scaleFactor = getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR);
				doubleOperation = andThen(doubleOperation, v -> v / scaleFactor);
			}

			if (doubleOperation.isPresent()) { // returns: src.readAsDouble() -> doubleOperation -> dst.putAsDouble()
				final DoubleUnaryOperator op = doubleOperation.get();
				return i -> dst.putAsDouble(i, op.applyAsDouble(src.getAsDouble(i)));
			}
			break;
		}

		return index -> dst.putFrom(src, index); // Returns operation with only type change
	}

	/**********************************************************************************************************
	 * DUPLICATION METHODS
	 **********************************************************************************************************/

	public void duplicate() {
		// TODO Auto-generated method stub
	}

	/**********************************************************************************************************
	 * PRIVATE METHODS
	 **********************************************************************************************************/

	/**
	 * Check buffer byte order is equal to the platform native byte order
	 *
	 * @throws NCException if false
	 */
	private void checkByteOrder(ByteBuffer buffer) throws NCException {
		if (buffer.order() != ByteOrder.nativeOrder())
			throw new NCException("Wrong buffer byte order (" + buffer.order()
					+ "), expected to be the platform native order (" + ByteOrder.nativeOrder() + ").");
	}

	/**
	 * Check if the buffer size is higher or equal of the size defined by the specified dimensions.
	 */
	private void checkBufferSize(ByteBuffer buffer, long[] dim, DataType type) throws NCException {
		// Check destination buffer size is valid
		int valueCount = (int) Arrays.stream(dim).reduce(1, (a, b) -> {
			return a * b;
		});
		int requiredSizeInBytes = valueCount * type.getSize();
		if (buffer.limit() < requiredSizeInBytes) {
			buffer.limit(requiredSizeInBytes);
		}
	}

	/**
	 * indicate if the variable is a vlen variable
	 */
	public boolean isVlen() {
		return false;
	}

	/**
	 * @return true if the variable has a numerical fill value attribute
	 */
	public boolean hasFillValue() throws NCException {
		return hasAttribute(CFStandardNames.CF_FILL_VALUE) && getAttributeDataType(CFStandardNames.CF_FILL_VALUE).isNumeric();
	}

	/**
	 * @return add offset if the variable has a numerical fill value attribute
	 */
	public Optional<Double> getFillValue() throws NCException {
		if (hasFillValue())
			return Optional.of(getAttributeDouble(CFStandardNames.CF_FILL_VALUE));
		return Optional.empty();
	}

	/**
	 * @return true if the variable has a numerical missing value attribute
	 */
	public boolean hasMissingValue() throws NCException {
		return hasAttribute(CFStandardNames.CF_MISSING_VALUE) && getAttributeDataType(CFStandardNames.CF_MISSING_VALUE).isNumeric();
	}

	/**
	 * @return add offset if the variable has a numerical missing value attribute
	 */
	public Optional<Double> getMissingValue() throws NCException {
		if (hasAddOffset())
			return Optional.of(getAttributeDouble(CFStandardNames.CF_MISSING_VALUE));
		return Optional.empty();
	}

	/**
	 * @return true if the variable has a numerical scale factor attribute
	 */
	public boolean hasScaleFactor() throws NCException {
		return hasAttribute(CFStandardNames.CF_SCALE_FACTOR) && getAttributeDataType(CFStandardNames.CF_SCALE_FACTOR).isNumeric();
	}

	/**
	 * @return scale factor if the variable has a numerical scale factor attribute
	 */
	public Optional<Double> getScaleFactor() throws NCException {
		if (hasScaleFactor())
			return Optional.of(getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR));
		return Optional.empty();
	}

	/**
	 * @return true if the variable has a numerical add offset attribute
	 */
	public boolean hasAddOffset() throws NCException {
		return hasAttribute(CFStandardNames.CF_ADD_OFFSET) && getAttributeDataType(CFStandardNames.CF_ADD_OFFSET).isNumeric();
	}

	/**
	 * @return add offset if the variable has a numerical add offset factor attribute
	 */
	public Optional<Double> getAddOffsetFactor() throws NCException {
		if (hasAddOffset())
			return Optional.of(getAttributeDouble(CFStandardNames.CF_ADD_OFFSET));
		return Optional.empty();
	}

	/**
	 * @return true if the variable has a numerical valid_min attribute
	 */
	public boolean hasValidMin() throws NCException {
		return hasAttribute(CFStandardNames.CF_VALID_MIN) && getAttributeDataType(CFStandardNames.CF_VALID_MIN).isNumeric();
	}

	/**
	 * @return scale factor if the variable has a numerical valid_min attribute
	 */
	public Optional<Double> getValidMin() throws NCException {
		if (hasValidMin())
			return Optional.of(getAttributeDouble(CFStandardNames.CF_VALID_MIN));
		return Optional.empty();
	}

	/**
	 * @return true if the variable has a numerical valid_max attribute
	 */
	public boolean hasValidMax() throws NCException {
		return hasAttribute(CFStandardNames.CF_VALID_MAX) && getAttributeDataType(CFStandardNames.CF_VALID_MAX).isNumeric();
	}

	/**
	 * @return scale factor if the variable has a numerical valid_max attribute
	 */
	public Optional<Double> getValidMax() throws NCException {
		if (hasValidMax())
			return Optional.of(getAttributeDouble(CFStandardNames.CF_VALID_MAX));
		return Optional.empty();
	}

	/**
	 * @throws NCUnitError if the two units are not compatibles
	 */
	private boolean checkUnitTransform(Unit unitSrc, Unit unitDst) throws NCUnitError {
		// check for a simple unit conversion
		try {
			unitSrc.convertTo(1, unitDst);
		} catch (ConversionException e1) {
			throw new NCUnitError(
					"Unit requested are not compatibles " + unitSrc.getName() + " (" + unitSrc.getCanonicalString()
							+ ") to " + unitDst.getName() + " (" + unitDst.getCanonicalString() + ")");

		}
		return true;
	}


	/**
	 * Combines two {@link DoubleUnaryOperator} (using optional)
	 */
	private Optional<DoubleUnaryOperator> andThen(Optional<DoubleUnaryOperator> base, DoubleUnaryOperator operation) {
		return Optional.of(base.isPresent() ? base.get().andThen(operation) : operation);
	}

	/**
	 * Combines two {@link FloatUnaryOperator} (using optional)
	 */
	private Optional<FloatUnaryOperator> andThen(Optional<FloatUnaryOperator> base, FloatUnaryOperator operation) {
		return Optional.of(base.isPresent() ? base.get().andThen(operation) : operation);
	}

}
