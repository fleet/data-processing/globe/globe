package fr.ifremer.globe.netcdf.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.jna.SizeT;
import fr.ifremer.globe.netcdf.jna.SizeTByReference;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCGroup;

public class NetcdfGroup extends NCGroup {
	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(NetcdfGroup.class);

	/**
	 * Constructor
	 */
	public NetcdfGroup(int grpid, String name, NCGroup parent) {
		super(grpid, name, parent);
	}

	/**
	 * Get an existing variable
	 * 
	 * @param name
	 * @return
	 * @throws NCException
	 */
	@Override
	public NetcdfVariable getVariable(String name) throws NCException {
		return (NetcdfVariable) super.getVariable(name);
	}

	@Override
	public String getPath() {
		String path = "";
		NetcdfGroup group = this;
		while (group != null && !(group instanceof NetcdfFile)) {
			path = '/' + group.getName() + path;
			group = (NetcdfGroup) group.getParent();
		}
		return path;
	}

	/**
	 * Create a new variable
	 * 
	 * @param name
	 * @param type
	 * @param shape
	 * @return
	 * @throws NCException
	 */
	@Override
	public NetcdfVariable addVariable(String name, int type, List<NCDimension> shape) throws NCException {
		if (variables.containsKey(name)) {
			NetcdfVariable theVar = getVariable(name);

			// check for shape
			if (theVar.getShape().size() != shape.size()) {
				throw new NCException("Trying to re-create the variable " + name + " with incompatible shape");
			}
			Iterator<NCDimension> requestedIt = shape.iterator();
			Iterator<NCDimension> existingIt = theVar.getShape().iterator();
			while (requestedIt.hasNext()) {
				NCDimension thisDim = existingIt.next();
				NCDimension newDim = requestedIt.next();
				if (!thisDim.equals(newDim)) {
					throw new NCException("Trying to create a variable with different dimensions: " + thisDim.getName()
							+ " != " + newDim.getName());
				}
			}

			// check for type
			if (type != theVar.getType()) {
				throw new NCException("Trying to redefine variable " + name + " with different data type");
			}

			return theVar;
		} else {
			int ndims = shape.size();
			int[] dimIds = new int[shape.size()];
			for (int i = 0; i < shape.size(); i++) {
				dimIds[i] = shape.get(i).getId();
			}
			IntByReference varId = new IntByReference();

			check(nc4.nc_def_var(ncid, name, new SizeT(type), ndims, dimIds, varId));
//			check(nc4.nc_def_var_deflate(ncid, varId.getValue(), 0, 1, 5));
			NetcdfVariable var = new NetcdfVariable(varId.getValue(), this, name, type, shape);
			variables.put(name, var);
			return var;
		}
	}

	@Override
	public NetcdfVariable addVariable(String name, DataType type, List<NCDimension> dimensions, String comName,
			String longName) throws NCException {
		return addVariable(name, DataType.getNCType(type), dimensions, comName, longName);
	}

	@Override
	public NetcdfVariable addVariable(String name, DataType type, List<NCDimension> dimensions, String comName,
			String longName, String unit) throws NCException {
		return addVariable(name, DataType.getNCType(type), dimensions, comName, longName, unit);
	}

	@Override
	public NetcdfVariable addVariable(String name, DataType type, List<NCDimension> dimensions) throws NCException {
		return addVariable(name, DataType.getNCType(type), dimensions);
	}

	@Override
	public NetcdfVariable addVariable(String name, int type, List<NCDimension> dimensions, String comName,
			String longName) throws NCException {
		NetcdfVariable v = addVariable(name, type, dimensions);
		v.addAttribute("name", comName);
		v.addAttribute("long_name", longName);
		return v;
	}

	@Override
	public NetcdfVariable addVariable(String name, int type, List<NCDimension> dimensions, String comName,
			String longName, String unit) throws NCException {
		NetcdfVariable v = addVariable(name, type, dimensions, comName, longName);
		v.addAttribute("unit", unit);
		return v;
	}

	/**
	 * read the variables defined in the current group
	 * 
	 * @return
	 * @throws NCException
	 */
	@Override
	public List<NetcdfVariable> getVariables() throws NCException {
		IntByReference nVars = new IntByReference();
		check(nc4.nc_inq_varids(ncid, nVars, null));
		List<NetcdfVariable> ret = new ArrayList<>(nVars.getValue());
		int[] ids = new int[nVars.getValue()];
		check(nc4.nc_inq_varids(ncid, nVars, ids));

		for (int i = 0; i < nVars.getValue(); i++) {
			byte[] name = new byte[Nc4prototypes.NC_MAX_NAME + 1];
			IntByReference nDims = new IntByReference();
			IntByReference type = new IntByReference();
			IntByReference nAttrs = new IntByReference();
			check(nc4.nc_inq_var(ncid, ids[i], name, type, nDims, (Pointer) null, nAttrs));
			int[] dimids = new int[nDims.getValue()];
			check(nc4.nc_inq_var(ncid, ids[i], name, type, nDims, dimids, nAttrs));

			IntByReference numunlimdimids = new IntByReference();
			check(nc4.nc_inq_unlimdims(ncid, numunlimdimids, null));
			int[] unlimdimids = new int[numunlimdimids.getValue()];
			check(nc4.nc_inq_unlimdims(ncid, numunlimdimids, unlimdimids));

			List<NCDimension> shape = new ArrayList<>(nDims.getValue());
			for (int j = 0; j < nDims.getValue(); j++) {
				byte[] dimName = new byte[Nc4prototypes.NC_MAX_NAME + 1];
				boolean isUnlimited = false;
				SizeTByReference dimSize = new SizeTByReference();

				check(nc4.nc_inq_dim(ncid, dimids[j], dimName, dimSize));
				for (int k = 0; k < numunlimdimids.getValue(); k++) {
					if (unlimdimids[k] == dimids[j]) {
						isUnlimited = true;
						break;
					}
				}
				shape.add(new NCDimension(dimids[j], dimSize.getValue().longValue(), makeString(dimName), isUnlimited));
			}
			if (makeString(name).compareToIgnoreCase("backscatter_r") == 0) {
				System.err.print("");
			}
			NetcdfVariable var = null;
			// user define type
			if (type.getValue() >= Nc4prototypes.NC_VLEN) {
				// user defined type, we query its type
				byte[] nameType = new byte[Nc4prototypes.NC_MAX_NAME + 1];
				IntByReference baseType = new IntByReference();
				IntByReference baseClass = new IntByReference();

				check(nc4.nc_inq_user_type(ncid, type.getValue(), nameType, null, baseType, null, baseClass));
				switch (baseClass.getValue()) {
				case Nc4prototypes.NC_VLEN:
					var = new NetcdfVlenVariable(ids[i], this, makeString(name), baseType.getValue(), shape);
					break;
				case Nc4prototypes.NC_ENUM:
					var = new NetcdfVariable(ids[i], this, makeString(name), baseType.getValue(), shape);
					break;
				default:
					logger.debug("Ignore unsupported type" + makeString(nameType));
					break;
				}
			} else {
				// classical variable
				var = new NetcdfVariable(ids[i], this, makeString(name), type.getValue(), shape);
			}
			if (var != null) {
				variables.put(var.getName(), var);
				ret.add(var);
			}
		}
		return ret;
	}

	/**
	 * Retrieve an existing group
	 * 
	 * @param name
	 * @return
	 * @throws NCException
	 */
	@Override
	public NetcdfGroup getGroup(String name) throws NCException {
		return (NetcdfGroup) super.getGroup(name);
	}

	/**
	 * read child groups from the underlying file
	 * 
	 * @return
	 * @throws NCException
	 */
	@Override
	public List<NetcdfGroup> getGroups() throws NCException {
		IntByReference ngrps = new IntByReference();
		check(nc4.nc_inq_grps(ncid, ngrps, (Pointer) null));
		int[] gcids = new int[ngrps.getValue()];
		List<NetcdfGroup> ret = new ArrayList<>(ngrps.getValue());
		check(nc4.nc_inq_grps(ncid, ngrps, gcids));

		for (int gid : gcids) {
			byte[] name = new byte[Nc4prototypes.NC_MAX_NAME + 1];
			check(nc4.nc_inq_grpname(gid, name));
			String grpName = makeString(name);
			NetcdfGroup child = new NetcdfGroup(gid, makeString(name), this);
			groups.put(grpName, child);
			ret.add(child);
		}

		return ret;
	}
}
