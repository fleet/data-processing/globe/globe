package fr.ifremer.globe.netcdf.api.buffer;

import java.nio.ByteBuffer;

import fr.ifremer.globe.netcdf.ucar.DataType;

public class ByteBufferWrapper extends ABufferWrapper {

	public ByteBufferWrapper(ByteBuffer buffer) {
		super(buffer);
	}

	private byte readValue(int index) {
		buffer.position(index * DataType.BYTE.getSize());
		return buffer.get();
	}

	private void writeValue(int index, byte value) {
		buffer.position(index * DataType.BYTE.getSize());
		buffer.put(value);
	}

	@Override
	public float getAsFloat(int index) {
		return readValue(index);
	}

	@Override
	public double getAsDouble(int index) {
		return (double) readValue(index);
	}

	@Override
	public int getAsInt(int index) {
		return (int) readValue(index);
	}

	@Override
	public short getAsShort(int index) {
		return readValue(index);
	}

	@Override
	public byte getAsByte(int index) {
		return readValue(index);
	}

	@Override
	public long getAsLong(int index) {
		return (long) readValue(index);
	}

	@Override
	public void putAsFloat(int index, float value) {
		writeValue(index, (byte) Math.round(value));
	}

	@Override
	public void putAsDouble(int index, double value) {
		writeValue(index, (byte) Math.round(value));
	}

	@Override
	public void putAsInt(int index, int value) {
		writeValue(index, (byte) value);
	}

	@Override
	public void putAsShort(int index, short value) {
		writeValue(index, (byte) value);
	}

	@Override
	public void putAsByte(int index, byte value) {
		writeValue(index, value);
	}

	@Override
	public void putAsLong(int index, long value) {
		writeValue(index, (byte) value);
	}

	@Override
	public void putFrom(ABufferWrapper bufferSrc, int index) {
		writeValue(index, bufferSrc.getAsByte(index));
	}


}
