package fr.ifremer.globe.netcdf.api.buffer;

import java.nio.ByteBuffer;

import fr.ifremer.globe.netcdf.ucar.DataType;

/**
 * Manages unsigned byte, due to java having no type ubyte, data are stored in an short
 */
public class UByteBufferWrapper extends ABufferWrapper {

	public UByteBufferWrapper(ByteBuffer buffer) {
		super(buffer);
	}

	private short readValue(int index) {
		buffer.position(index * DataType.BYTE.getSize());
		return (short) Byte.toUnsignedInt(buffer.get());
	}

	private void writeValue(int index, short value) {
		buffer.position(index * DataType.BYTE.getSize());
		buffer.put((byte) value);
	}

	@Override
	public float getAsFloat(int index) {
		return readValue(index);
	}

	@Override
	public double getAsDouble(int index) {
		return (double) readValue(index);
	}

	@Override
	public int getAsInt(int index) {
		return (int) readValue(index);
	}

	@Override
	public short getAsShort(int index) {
		return readValue(index);
	}

	@Override
	public byte getAsByte(int index) {
		return (byte) readValue(index);
	}

	@Override
	public long getAsLong(int index) {
		return (long) readValue(index);
	}

	@Override
	public void putAsFloat(int index, float value) {
		writeValue(index, (short) Math.round(value));
	}

	@Override
	public void putAsDouble(int index, double value) {
		writeValue(index, (short) Math.round(value));
	}
	
	@Override
	public void putAsInt(int index, int value) {
		writeValue(index, (short) value);
	}

	@Override
	public void putAsShort(int index, short value) {
		writeValue(index, value);
	}

	@Override
	public void putAsByte(int index, byte value) {
		writeValue(index, (short) value);
	}

	@Override
	public void putAsLong(int index, long value) {
		writeValue(index, (short) value);
	}

	@Override
	public void putFrom(ABufferWrapper src, int index) {
		writeValue(index, src.getAsShort(index));
	}
}
