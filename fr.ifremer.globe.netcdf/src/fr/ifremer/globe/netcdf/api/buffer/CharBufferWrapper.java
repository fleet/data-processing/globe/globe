package fr.ifremer.globe.netcdf.api.buffer;

import java.nio.ByteBuffer;

import fr.ifremer.globe.netcdf.ucar.DataType;

public class CharBufferWrapper extends ABufferWrapper {

	public CharBufferWrapper(ByteBuffer buffer) {
		super(buffer);	
	}

	private char readValue(int index) {
		buffer.position(index * DataType.CHAR.getSize());
		return (char) (buffer.get() & 0xff);
	}

	private void writeValue(int index, char value) {
		buffer.position(index * DataType.CHAR.getSize());
		buffer.put((byte) value);
	}

	@Override
	public float getAsFloat(int index) {
		return readValue(index);
	}

	@Override
	public double getAsDouble(int index) {
		return readValue(index);
	}

	@Override
	public int getAsInt(int index) {
		return readValue(index);
	}

	@Override
	public short getAsShort(int index) {
		return (short) readValue(index);
	}

	@Override
	public byte getAsByte(int index) {
		return (byte) readValue(index);
	}

	@Override
	public long getAsLong(int index) {
		return readValue(index);
	}

	@Override
	public void putAsFloat(int index, float value) {
		writeValue(index, (char) value);
	}

	@Override
	public void putAsDouble(int index, double value) {
		writeValue(index, (char) value);
	}

	@Override
	public void putAsInt(int index, int value) {
		writeValue(index, (char) value);
	}

	@Override
	public void putAsShort(int index, short value) {
		writeValue(index, (char) value);
	}

	@Override
	public void putAsByte(int index, byte value) {
		writeValue(index, (char) value);
	}

	@Override
	public void putAsLong(int index, long value) {
		writeValue(index, (char) value);
	}

	@Override
	public void putFrom(ABufferWrapper src, int index) {
		writeValue(index, (char) src.getAsShort(index));
	}
}
