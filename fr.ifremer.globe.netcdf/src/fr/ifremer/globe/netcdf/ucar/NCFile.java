package fr.ifremer.globe.netcdf.ucar;

import static fr.ifremer.globe.netcdf.jna.Nc4prototypes.NC_64BIT_OFFSET;
import static fr.ifremer.globe.netcdf.jna.Nc4prototypes.NC_NETCDF4;
import static fr.ifremer.globe.netcdf.jna.Nc4prototypes.NC_NOWRITE;
import static fr.ifremer.globe.netcdf.jna.Nc4prototypes.NC_WRITE;

import org.gdal.gdal.gdal;

import com.sun.jna.ptr.IntByReference;

public class NCFile extends NCGroup implements AutoCloseable {

	public static enum Version {
		netcdf4, netcdf3_64b
	}

	public static enum Mode {
		readonly, readwrite
	}

	protected boolean withFillValue = false;
	protected boolean compressionDisabled = false;

	/**
	 * only used from static createNew and open methods
	 * 
	 * @param ncid
	 */
	protected NCFile(int ncid, String name, boolean withFillValue) {
		super(ncid, name, null);
		this.withFillValue = withFillValue;
	}

	/**
	 * Create a new netCDF file
	 */
	public static NCFile createNew(Version version, String absolutePath) throws NCException {
		return createNew(version, absolutePath, false);
	}

	/**
	 * Create a new netCDF file
	 */
	public static NCFile createNew(Version version, String absolutePath, boolean withFillValue) throws NCException {
		IntByReference ncidRef = new IntByReference();
		synchronized (gdal.class) {
			switch (version) {
			case netcdf4:
				check(nc4.nc_create(absolutePath, NC_NETCDF4, ncidRef));
				break;
			case netcdf3_64b:
				check(nc4.nc_create(absolutePath, NC_64BIT_OFFSET, ncidRef));
				break;
			}
		}
		int ncid = ncidRef.getValue();
		return new NCFile(ncid, absolutePath, withFillValue);
	}

	/** return NCFile instance with compression disabled */
	public NCFile withCompressionDisabled() {
		compressionDisabled = true;
		return this;
	}
	
	/**
	 * Open an existing netCDF file
	 * 
	 * @param path
	 * @param mode
	 * @return
	 * @throws NCException
	 */
	public static synchronized NCFile open(String path, Mode mode) throws NCException {
		IntByReference ncid = new IntByReference();
		synchronized (gdal.class) {
			switch (mode) {
			case readonly:
				check(nc4.nc_open(path, NC_NOWRITE, ncid));
				break;
			case readwrite:
				check(nc4.nc_open(path, NC_WRITE, ncid));
				break;
			}
		}
		return new NCFile(ncid.getValue(), path, mode == Mode.readwrite);
	}

	/**
	 * Close the file
	 */
	@Override
	public void close() {
		synchronized (gdal.class) {
			nc4.nc_close(ncid);
		}
	}

	/**
	 * Synchronize to disk.
	 */
	public void synchronize() throws NCException {
		check(nc4.nc_sync(ncid));
	}

	@Override
	public String getPath() {
		return "";
	}

	/** @returns true if the file can manage the FillValue attribute */
	@Override
	public boolean isWithFillValue() {
		return withFillValue;
	}

	/**
	 * @param withFillValue the {@link #withFillValue} to set
	 */
	public void setWithFillValue(boolean withFillValue) {
		this.withFillValue = withFillValue;
	}
	
	/** @returns true if compression should be disabled */
	@Override
	public boolean isCompressionDisabled() {
		return compressionDisabled;
	}
}
