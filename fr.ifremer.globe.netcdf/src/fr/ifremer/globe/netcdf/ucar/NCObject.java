package fr.ifremer.globe.netcdf.ucar;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;

import com.sun.jna.ptr.IntByReference;

import fr.ifremer.globe.netcdf.jna.Nc4Iosp;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.jna.SizeT;
import fr.ifremer.globe.netcdf.jna.SizeTByReference;

public abstract class NCObject {

	protected static final Nc4prototypes nc4;
	static {
		nc4 = Nc4Iosp.getCLibrary();
	}

	public NCObject() {
	}

	public NCObject(int ncid) {
		this.ncid = ncid;
	}

	protected int ncid;

	protected static void check(int ret) throws NCException {
		if (ret != 0) {
			String msg = nc4.nc_strerror(ret);
			if (ret > 0 && msg.equals("Unknown error")) {
				ret = 0xFFFFFF00 | ret;
				msg = nc4.nc_strerror(ret);
			}
			throw new NCException(msg + " (error code: " + ret + ")");
		}
	}

	public void redef() throws NCException {
		check(nc4.nc_redef(ncid));
	}

	public void enddef() throws NCException {
		check(nc4.nc_enddef(ncid));
	}

	/**
	 * @return path
	 */
	public abstract String getPath();

	/**
	 * Att a text attribute
	 *
	 * note that a string attribute is used to have multiple strings.
	 *
	 * @param name
	 * @param value
	 * @throws NCException
	 */
	public void addAttribute(String name, String value) throws NCException {
		byte[] val = value.getBytes(StandardCharsets.UTF_8);
		SizeT size = new SizeT(val.length);
		check(nc4.nc_put_att_text(getGroupId(), getVarId(), name, size, val));
	}

	public String getAttributeText(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		byte[] val = new byte[(int) size.getValue().longValue()];
		if (nc4.nc_get_att_text(getGroupId(), getVarId(), name, val) == 0) {
			return makeString(val);
		}
		// Try to read the attribute as a String
		String[] valAsString = new String[(int) size.getValue().longValue()];
		check(nc4.nc_get_att_string(getGroupId(), getVarId(), name, valAsString));
		return valAsString[0];
	}

	public String[] getAttributeTextArray(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		String[] ret = null;

		String[] valAsString = new String[(int) size.getValue().longValue()];
		if (nc4.nc_get_att_string(getGroupId(), getVarId(), name, valAsString) == 0) {
			ret = Arrays.copyOf(valAsString, valAsString.length);
		} else {
			byte[] valAsByte = new byte[(int) size.getValue().longValue()];
			check(nc4.nc_get_att_text(getGroupId(), getVarId(), name, valAsByte));
			ret = new String[] { new String(valAsByte) };
		}

		// check(nc4.nc_free_string(new SizeT(valAsString.length),valAsString));
		// as explained in netcdf documentation allocated string array should be free
		// (Note that I'm not sure how it can really works with string in java but it seems to at least not crash
		return ret;
	}

	public void addAttributeTextArray(String name, String[] values) throws NCException {
		SizeT size = new SizeT(values.length);
		check(nc4.nc_put_att_string(getGroupId(), getVarId(), name, size, values));
	}

	public void deleteAttribute(String name) {
		nc4.nc_del_att(getGroupId(), getVarId(), name);
	}

	// Char attribute

	public void addAttribute(String name, char value) throws NCException {
		// !! do not use nc_put_att_uchar which is not for DataType.CHAR ! (refers to the in-memory C type)
		check(nc4.nc_put_att(getGroupId(), getVarId(), name, DataType.getNCType(DataType.CHAR), new SizeT(1),
				new byte[] { (byte) value }));
	}

	public char getAttributeChar(String name) throws NCException {
		byte[] v = new byte[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() == 0)
			return Character.UNASSIGNED;
		// for char array, use "getAttributeText"
		if (size.getValue().longValue() != 1)
			throw new NCException("Cannot read char array of attribute : " + name);
		check(nc4.nc_get_att(getGroupId(), getVarId(), name, v));
		return StandardCharsets.UTF_8.decode(ByteBuffer.wrap(v)).get();
	}

	// Byte attribute
	public void addAttribute(String name, byte value) throws NCException {
		check(nc4.nc_put_att_schar(getGroupId(), getVarId(), name, DataType.getNCType(DataType.BYTE), new SizeT(1),
				new byte[] { (byte) value }));
	}

	public void addUnsignedAttribute(String name, byte value) throws NCException {
		check(nc4.nc_put_att_ubyte(getGroupId(), getVarId(), name, DataType.getNCType(DataType.UBYTE), new SizeT(1),
				new byte[] { (byte) value }));
	}

	public byte getAttributeByte(String name) throws NCException {
		byte[] v = new byte[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1)
			throw new NCException("Cannot read byte array of attribute : " + name);
		check(nc4.nc_get_att(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	public byte getAttributeUByte(String name) throws NCException {
		byte[] v = new byte[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1)
			throw new NCException("Cannot read byte array of attribute : " + name);
		check(nc4.nc_get_att_ubyte(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	// Short attribute

	public void addAttribute(String name, short value) throws NCException {
		check(nc4.nc_put_att_int(getGroupId(), getVarId(), name, DataType.getNCType(DataType.SHORT), new SizeT(1),
				new int[] { value }));
	}

	public void addUnsignedAttribute(String name, short value) throws NCException {
		check(nc4.nc_put_att_ushort(getGroupId(), getVarId(), name, DataType.getNCType(DataType.USHORT), new SizeT(1),
				new short[] { value }));
	}

	public short getAttributeShort(String name) throws NCException {
		short[] v = new short[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_short(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	public short[] getAttributeShortArray(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size);
		short[] v = new short[(int) size.getValue().longValue()];
		check(nc4.nc_get_att_short(getGroupId(), getVarId(), name, v));
		return v;
	}

	public short getAttributeUShort(String name) throws NCException {
		short[] v = new short[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_ushort(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	// Integer attribute

	public void addAttribute(String name, int value) throws NCException {
		check(nc4.nc_put_att_int(getGroupId(), getVarId(), name, DataType.getNCType(DataType.INT), new SizeT(1),
				new int[] { value }));
	}

	public void addAttribute(String name, int[] value) throws NCException {
		check(nc4.nc_put_att_int(getGroupId(), getVarId(), name, DataType.getNCType(DataType.INT),
				new SizeT(value.length), value));
	}

	public void addUnsignedAttribute(String name, int value) throws NCException {
		check(nc4.nc_put_att_uint(getGroupId(), getVarId(), name, DataType.getNCType(DataType.UINT), new SizeT(1),
				new int[] { value }));
	}
	
	public int getAttributeInt(String name) throws NCException {
		int[] v = new int[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_int(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	public int[] getAttributeIntArray(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size);
		int[] v = new int[(int) size.getValue().longValue()];
		check(nc4.nc_get_att_int(getGroupId(), getVarId(), name, v));
		return v;
	}

	public int getAttributeUInt(String name) throws NCException {
		int[] v = new int[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_uint(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	// Long attribute

	public void addAttribute(String name, long value) throws NCException {
		check(nc4.nc_put_att_longlong(getGroupId(), getVarId(), name, DataType.getNCType(DataType.LONG), new SizeT(1),
				new long[] { value }));
	}

	public long getAttributeLong(String name) throws NCException {
		long[] v = new long[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_longlong(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	public long[] getAttributeLongArray(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size);
		long[] v = new long[(int) size.getValue().longValue()];
		check(nc4.nc_get_att_longlong(getGroupId(), getVarId(), name, v));
		return v;
	}

	public long getAttributeULong(String name) throws NCException {
		long[] v = new long[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_ulonglong(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	// Float attribute
	public void addAttribute(String name, float[] value) throws NCException {
		check(nc4.nc_put_att_float(getGroupId(), getVarId(), name, DataType.getNCType(DataType.FLOAT),
				new SizeT(value.length), value));
	}

	public void addAttribute(String name, float value) throws NCException {
		check(nc4.nc_put_att_float(getGroupId(), getVarId(), name, DataType.getNCType(DataType.FLOAT), new SizeT(1),
				new float[] { value }));
	}

	public float getAttributeFloat(String name) throws NCException {
		float[] v = new float[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_float(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	public float[] getAttributeFloatArray(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size);
		float[] v = new float[(int) size.getValue().longValue()];
		check(nc4.nc_get_att_float(getGroupId(), getVarId(), name, v));
		return v;
	}

	// Double attribute
	public void addAttribute(String name, double[] value) throws NCException {
		check(nc4.nc_put_att_double(getGroupId(), getVarId(), name, DataType.getNCType(DataType.DOUBLE),
				new SizeT(value.length), value));
	}

	public void addAttribute(String name, double value) throws NCException {
		check(nc4.nc_put_att_double(getGroupId(), getVarId(), name, DataType.getNCType(DataType.DOUBLE), new SizeT(1),
				new double[] { value }));
	}

	public double getAttributeDouble(String name) throws NCException {
		double[] v = new double[1];
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		if (size.getValue().longValue() != 1) {
			throw new NCException("Cannot read array attributes");
		}
		check(nc4.nc_get_att_double(getGroupId(), getVarId(), name, v));
		return v[0];
	}

	public double[] getAttributeDoubleArray(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size);
		double[] v = new double[(int) size.getValue().longValue()];
		check(nc4.nc_get_att_double(getGroupId(), getVarId(), name, v));
		return v;
	}

	// Get attribute size

	public int getAttributeSize(String name) throws NCException {
		SizeTByReference size = new SizeTByReference();
		check(nc4.nc_inq_attlen(getGroupId(), getVarId(), name, size));
		return size.getValue().intValue();
	}

	// Get & put attribute as byte arrays

	public byte[] getAttribute(String name) throws NCException {
		int ncType = getAttributeType(name);
		int typeSize = DataType.getDataTypeFromNCType(ncType).getSize();
		byte[] v = new byte[getAttributeSize(name) * typeSize];
		check(nc4.nc_get_att(getGroupId(), getVarId(), name, v));
		return v;
	}

	public void addAttribute(String name, byte[] v, int ncType) throws NCException {
		int dataTypeSize = DataType.getDataTypeFromNCType(ncType).getSize();
		check(nc4.nc_put_att(getGroupId(), getVarId(), name, ncType, new SizeT(v.length / dataTypeSize), v));
	}

	/**
	 * Get the number of attributes of the current object
	 *
	 * @return
	 * @throws NCException
	 */
	public abstract int getAttributeCount() throws NCException;

	/**
	 * Read the list of attribute names
	 *
	 * @return
	 * @throws NCException
	 */
	public String[] getAttributeNames() throws NCException {
		final int nAtts = getAttributeCount();
		String[] ret = new String[nAtts];
		for (int i = 0; i < nAtts; i++) {
			byte[] name = new byte[Nc4prototypes.NC_MAX_NAME + 1];
			check(nc4.nc_inq_attname(getGroupId(), getVarId(), i, name));
			ret[i] = makeString(name);
		}
		return ret;
	}

	/**
	 * Read the type of an attribute given its name
	 *
	 * @param name
	 * @return
	 * @throws NCException
	 */
	public int getAttributeType(String name) throws NCException {
		IntByReference ref = new IntByReference();
		check(nc4.nc_inq_atttype(getGroupId(), getVarId(), name, ref));
		return ref.getValue();
	}

	public DataType getAttributeDataType(String name) throws NCException {
		return DataType.getDataTypeFromNCType(getAttributeType(name));
	}

	/**
	 * Get an attribute and parse its value in String
	 *
	 * @param name attribute name
	 * @return attribute value parsed in a String
	 * @throws NCException
	 */
	public String getAttributeAsString(String name) throws NCException {

		DataType type = getAttributeDataType(name);

		try {
			switch (type) {
			case BYTE:// Nc4prototypes.NC_BYTE:
				return String.valueOf(getAttributeByte(name));
			case UBYTE:// Nc4prototypes.NC_UBYTE:
				return String.valueOf(DataType.unsignedByteToShort(getAttributeUByte(name)));
			case SHORT:// Nc4prototypes.NC_SHORT:
				return String.valueOf(getAttributeShort(name));
			case USHORT:// Nc4prototypes.NC_SHORT:
				return String.valueOf(DataType.unsignedShortToInt(getAttributeUShort(name)));
			case INT:// Nc4prototypes.NC_INT:
			{
				int[] v = getAttributeIntArray(name);
				if (v.length == 1)
					return String.valueOf(v[0]);
				else
					return Arrays.toString(v);
			}
			case UINT:// Nc4prototypes.NC_UINT:
				return String.valueOf(DataType.unsignedIntToLong(getAttributeUInt(name)));
			case LONG: // Nc4prototypes.NC_INT64:
				return String.valueOf(getAttributeLong(name));
			case ULONG: // Nc4prototypes.NC_INT64:
				return DataType.unsignedLongToString(getAttributeULong(name));
			case FLOAT:// Nc4prototypes.NC_FLOAT:
			{
				float[] v = getAttributeFloatArray(name);
				if (v.length == 1)
					return String.valueOf(v[0]);
				else
					return Arrays.toString(v);
			}
			case DOUBLE:// Nc4prototypes.NC_DOUBLE:
			{

				double[] v = getAttributeDoubleArray(name);
				if (v.length == 1)
					return String.valueOf(v[0]);
				else
					return Arrays.toString(v);
			}

			case STRING:// Nc4prototypes.NC_CHAR:
				// if (getAttributeSize(name) > 1)
				return getAttributeText(name);
			case CHAR:// Nc4prototypes.NC_CHAR:
				// getAttributeChar does'nt handle char array (mbg)
				if (getAttributeSize(name) > 1) {
					return getAttributeText(name);
				} else {
					char attValue = getAttributeChar(name);
					return attValue != 0 ? String.valueOf(attValue) : "";
				}
			default:
				return " attribute type (" + type.toString() + ") not handled by the api";
			}

		} catch (

		NCException e) {
			throw new NCException("Error reading attribute : '" + name + "' (type : " + type + "; size : "
					+ getAttributeSize(name) + "; path : " + getPath() + ") : " + e.getMessage(), e);
		}

	}

	/**
	 * Get all attributes in an HasHMap
	 *
	 * @return HasHMap, key = attribute name, value = attribute value as string
	 * @throws NCException
	 */
	public HashMap<String, String> getAttributes() throws NCException {
		HashMap<String, String> result = new HashMap<>();
		for (String attName : getAttributeNames())
			result.put(attName, getAttributeAsString(attName));
		return result;
	}

	/**
	 * Identify presence or not for attribute
	 *
	 * @param name
	 * @return
	 * @throws NCException
	 */
	public boolean hasAttribute(String name) throws NCException {
		for (String strCurrAtt : getAttributeNames()) {
			if (name.equals(strCurrAtt))
				return true;
		}
		return false;
	}

	/**
	 * should return the valid for a variable, and NC_GLOBAL for a group or file
	 */
	protected abstract int getVarId();

	/** Get the group ID for a group, the parents id for a variable. */
	protected abstract int getGroupId();

	protected String makeString(byte[] b) {
		int count = 0;
		while (count < b.length && b[count] != 0) {
			count++;
		}
		// all strings are considered to be UTF-8 unicode.
		return new String(b, 0, count, StandardCharsets.UTF_8);
	}
}
