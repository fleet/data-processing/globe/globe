/*
 * Copyright 1998-2013 University Corporation for Atmospheric Research/Unidata
 *
 * Portions of this software were developed by the Unidata Program at the
 * University Corporation for Atmospheric Research.
 *
 * Access and use of this software shall impose the following obligations
 * and understandings on the user. The user is granted the right, without
 * any fee or cost, to use, copy, modify, alter, enhance and distribute
 * this software, and any derivative works thereof, and its supporting
 * documentation for any purpose whatsoever, provided that this entire
 * notice appears in all copies of the software, derivative works and
 * supporting documentation.  Further, UCAR requests that the user credit
 * UCAR/Unidata in any publications that result from the use of this
 * software or in any product that includes this software. The names UCAR
 * and/or Unidata, however, may not be used in any advertising or publicity
 * to endorse or promote any products or commercial entity unless specific
 * written permission is obtained from UCAR/Unidata. The user also
 * understands that UCAR/Unidata is not obligated to provide the user with
 * any support, consulting, training or assistance of any kind with regard
 * to the use, operation and performance of this software nor to provide
 * the user with any updates, revisions, new versions or "bug fixes."
 *
 * THIS SOFTWARE IS PROVIDED BY UCAR/UNIDATA "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL UCAR/UNIDATA BE LIABLE FOR ANY SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 * WITH THE ACCESS, USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package fr.ifremer.globe.netcdf.jna;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

import com.sun.jna.Library;
import com.sun.jna.Native;

import fr.ifremer.globe.gdal.GdalLibLoader;
import fr.ifremer.globe.netcdf.util.NetcdfLoader;

/**
 * IOSP for reading netcdf files through jni interface to netcdf4 library
 *
 * @author caron
 * @see "http://www.unidata.ucar.edu/software/netcdf/docs/netcdf-c.html"
 * @see "http://earthdata.nasa.gov/sites/default/files/field/document/ESDS-RFC-022v1.pdf"
 * @see "http://www.unidata.ucar.edu/software/netcdf/docs/faq.html#How-can-I-convert-HDF5-files-into-netCDF-4-files"
 * hdf5 features not supported
 * @see "http://www.unidata.ucar.edu/software/netcdf/win_netcdf/"
 * @since Oct 30, 2008
 */
public class Nc4Iosp {

  static public final boolean DEBUG = false;

  static private org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Nc4Iosp.class);
  static private org.slf4j.Logger startupLog = org.slf4j.LoggerFactory.getLogger("serverStartup");
  static private Nc4prototypes nc4 = null;
  static public final String JNA_PATH = "jna.library.path";
  static public final String JNA_PATH_ENV = "JNA_PATH"; // environment var

  static public final String TRANSLATECONTROL = "ucar.translate";
  static public final String TRANSLATE_NONE = "none";
  static public final String TRANSLATE_NC4 = "nc4";

  // Define reserved attributes   (see Nc4DSP)
  static public final String UCARTAGOPAQUE = "_edu.ucar.opaque.size";
  // Not yet implemented
  static public final String UCARTAGVLEN = "_edu.ucar.isvlen";
  static public final String UCARTAGORIGTYPE = "_edu.ucar.orig.type";

  static protected String DEFAULTNETCDF4LIBNAME = "netcdf";

  static private String jnaPath = null;
  static private String libName = DEFAULTNETCDF4LIBNAME;

  /**
   * set the path and name of the netcdf c library.
   * must be called before load() is called.
   *
   * @param jna_path path to shared libraries
   * @param lib_name library name
   */
  static public void setLibraryAndPath(String jna_path, String lib_name) {
    lib_name = nullify(lib_name);

    if (lib_name == null) {
      lib_name = DEFAULTNETCDF4LIBNAME;
    }

    jna_path = nullify(jna_path);

    if (jna_path == null) {
      jna_path = nullify(System.getProperty(JNA_PATH));  // First, try system property (-D flag).
    }
    if (jna_path == null) {
      jna_path = nullify(System.getenv(JNA_PATH_ENV));   // Next, try environment variable.
    }

    if (jna_path != null) {
      System.setProperty(JNA_PATH, jna_path);
    }

    libName = lib_name;
    jnaPath = jna_path;
  }

  static private Nc4prototypes load() {
	  
    if (nc4 == null) {
    	
	  // Add netcdf native libraries to "jna.library.path"
	  NetcdfLoader.initJnaPath();
	  
      if (jnaPath == null) {
        setLibraryAndPath(null, null);
      }
      try {
  		
    	// Loads Hdf5 needed libraries (libhdf5_hl.so and libhdf5.so)  
  		GdalLibLoader.loadNativeLibrary(); //call the Activator.start method of gdal plugin which loads required libraries
  		
        // jna_path may still be null, but try to load anyway;
        // the necessary libs may be on the system PATH or on LD_LIBRARY_PATH
  		nc4 = Native.load(libName, Nc4prototypes.class, Collections.singletonMap(Library.OPTION_STRING_ENCODING, StandardCharsets.UTF_8.toString()));
        // Make the library synchronized
        nc4 = (Nc4prototypes) Native.synchronizedLibrary(nc4);

        startupLog.info("Nc4Iosp: NetCDF-4 C library loaded (jna_path='{}', libname='{}').", jnaPath, libName);
        log.debug("Netcdf nc_inq_libvers='{}' isProtected={}", nc4.nc_inq_libvers(), Native.isProtected());
      } catch (Throwable t) {
    	log.error("Error during netcdf lib loading: " + t.getMessage(),t);
        String message = String.format(
                        "Nc4Iosp: NetCDF-4 C library not present (jna_path='%s', libname='%s').", jnaPath, libName);
        startupLog.warn(message, t);
      }
    }

    return nc4;
  }

  // Shared mutable state. Only read/written in isClibraryPresent().
  private static Boolean isClibraryPresent = null;

  /**
   * Test if the netcdf C library is present and loaded
   *
   * @return true if present
   */
  static public synchronized boolean isClibraryPresent() {
    if (isClibraryPresent == null) {
      isClibraryPresent = load() != null;
    }
    return isClibraryPresent;
  }

  static public synchronized Nc4prototypes getCLibrary()
  {
      return isClibraryPresent() ? nc4 : null;
  }
  

  /**
   * Convert a zero-length string to null
   *
   * @param s the string to check for length
   * @return null if s.length() == 0, s otherwise
   */
  static protected String nullify(String s) {
    if (s != null && s.length() == 0) s = null;
    return s;
  }
}
