package fr.ifremer.globe.netcdf.util;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.netcdf.jna.Nc4Iosp;

public class NetcdfLoader {
	public static final Logger logger = LoggerFactory.getLogger(NetcdfLoader.class);

	private static AtomicBoolean initdone = new AtomicBoolean(false);

	/**
	 * Constructor
	 */
	private NetcdfLoader() {
	}

	/**
	 * Initialize the NetCdf library. ie update environment variable so that the loading of native library can be done.
	 */
	public static void initJnaPath() {
		try {
			if (initdone.compareAndSet(false, true)) {
				// Using GDAL native libraries
				String pluginId = "org.gdal." + Platform.getOS() + "." + Platform.getOSArch();
				Bundle gdalBundle = Platform.getBundle(pluginId);
				String gdalFolder = FilenameUtils.normalize(FileLocator.getBundleFile(gdalBundle).getAbsolutePath());
				logger.debug("ucar.netcdf library path : {}", gdalFolder);
				System.setProperty(Nc4Iosp.JNA_PATH, gdalFolder);
			}
		} catch (Exception e) {
			logger.error("ucar.netcdf library path :", e);
		}
	}
}
