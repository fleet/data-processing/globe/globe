package fr.ifremer.globe.netcdf.util;

import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility class for data type conversion between java and netcdf
 * */
public class TypeMatcher {

	private TypeMatcher() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * find matching datatype for a java raw type
	 * we do not use DataType.getType(javaType, true) because some types are not handled by the model
	 *
	 ** */
	public static DataType javaToNc(Class<?> javaType) throws GIOException
	{
		if(javaType.equals(boolean.class))
		{
			return DataType.BOOLEAN;
		} else if(javaType.equals(int.class))
		{
			return DataType.INT;
		} else if(javaType.equals(long.class))
		{
			return DataType.LONG;
		} else if(javaType.equals(byte.class))
		{
			return DataType.BYTE;
		} else if(javaType.equals(short.class))
		{
			return DataType.SHORT;
		} else if(javaType.equals(double.class))
		{
			return DataType.DOUBLE;
		} else if(javaType.equals(float.class))
		{
			return DataType.FLOAT;
		}
		throw new GIOException("No netcdf mapping found for the java type "+javaType);

	}
}
