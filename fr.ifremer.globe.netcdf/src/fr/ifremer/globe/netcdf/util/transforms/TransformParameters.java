package fr.ifremer.globe.netcdf.util.transforms;

import java.util.Optional;

import fr.ifremer.globe.netcdf.ucar.DataType;
import ucar.units.Unit;

/**
 * Base class handling units and coordinate system transform parameters
 * */
public abstract class TransformParameters {
	TransformParameters()
	{
		
	}
	/**
	 * compute the datatype change when applying a transform from one type to the other
	 * */
	public abstract DataType getDataType(DataType source);
	
	public abstract Optional<Unit> getUnit();
}
