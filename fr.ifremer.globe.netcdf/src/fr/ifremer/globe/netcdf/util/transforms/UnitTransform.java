package fr.ifremer.globe.netcdf.util.transforms;

import java.util.Optional;

import fr.ifremer.globe.netcdf.ucar.DataType;
import ucar.units.Unit;

/**
 * Base class handling units and coordinate system transform parameters
 * */
public class UnitTransform extends TransformParameters {
	private Unit unit;

	public UnitTransform(Unit unit)
	{
		this.unit=unit;
	}



	/**
	 * {@inheritDoc}
	 * <p>When a transform is applied, the computation always return double or float values 
	 * */
	@Override
	public DataType getDataType(DataType source) {
		if(source==DataType.DOUBLE)
			return source;
		if(source==DataType.FLOAT)
			return DataType.FLOAT;
		return DataType.DOUBLE;
	}



	@Override
	public Optional<Unit> getUnit() {
		return Optional.of(unit);
	}


}
