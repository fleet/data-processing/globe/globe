package fr.ifremer.globe.netcdf.util.transforms;

import java.util.Optional;

import fr.ifremer.globe.netcdf.ucar.DataType;
import ucar.units.Unit;

/**
 * Base class handling units and coordinate system transform parameters
 * */
public class EmptyTransform extends TransformParameters {
	
	private static EmptyTransform instance=new EmptyTransform();
	/**
	 * return an instance of empty transform
	 * */
	public static EmptyTransform get() { 
		return instance;
	};
	
	EmptyTransform()
	{
		
	}

	@Override
	public DataType getDataType(DataType source) {
		return source;
	}

	@Override
	public Optional<Unit> getUnit() {
		return Optional.empty();
	}
}
