package fr.ifremer.globe.netcdf.util.comparator;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.buffer.ABufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.BufferWrapperProvider;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCObject;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook.PairValues;
import fr.ifremer.globe.utils.array.impl.LargeArray;
import fr.ifremer.globe.utils.exception.runtime.NotImplementedException;

/**
 * Utility class which provides methods to compare netCDF files and objects.
 */
public class NetcdfComparator {

	private static final Logger LOGGER = LoggerFactory.getLogger(NetcdfComparator.class);

	/** Constructor **/
	private NetcdfComparator() {
		// static utility class
	}

	/**
	 * Compares two netCDF files.
	 * 
	 * @param hook : list of variables (path/to/variable) or attributes (path/to/variable:attribue_name) to ignore.
	 */
	public static Optional<String> compareFiles(String filePathA, String filePathB, NetcdfComparatorHook hook)
			throws NCException, IOException {
		return compareFiles(filePathA, filePathB, Optional.of(hook));
	}

	/**
	 * Compares two netCDF files.
	 * 
	 * @param hook : list of variables (path/to/variable) or attributes (path/to/variable:attribue_name) to ignore.
	 * 
	 * @return an optional string, which contains potentially differences.
	 */
	public static Optional<String> compareFiles(String filePathA, String filePathB, Optional<NetcdfComparatorHook> hook)
			throws NCException, IOException {
		try (NetcdfFile netcdfFileA = NetcdfFile.open(filePathA, Mode.readonly);
				NetcdfFile netcdfFileB = NetcdfFile.open(filePathB, Mode.readonly)) {
			return compareFiles(netcdfFileA, netcdfFileB, hook);
		}
	}

	/**
	 * Compares two {@link NetcdfFile}.
	 * 
	 * @param hook : list of variables (path/to/variable) or attributes (path/to/variable:attribue_name) to ignore.
	 * 
	 * @return an optional string, which contains potentially differences.
	 */
	public static Optional<String> compareFiles(NetcdfFile fileA, NetcdfFile fileB, Optional<NetcdfComparatorHook> hook)
			throws NCException, IOException {
		LOGGER.debug("Compare files : {} vs {} ", fileA.getName(), fileB.getName());
		return compareGroups(fileA, fileB, hook);
	}

	/**
	 * Compares two {@link NetcdfGroup}.
	 * 
	 * @return an optional string, which contains potentially differences.
	 */
	public static Optional<String> compareGroups(NetcdfGroup groupA, NetcdfGroup groupB,
			Optional<NetcdfComparatorHook> hook) throws NCException, IOException {
		// LOGGER.debug("Compare groups : {} ", groupA.getName());
		StringBuilder diffStringBuilder = new StringBuilder();

		// compare sub groups (recursively)
		Map<String, NetcdfGroup> subGroupsA = new HashMap<>();
		groupA.getGroups().forEach(group -> subGroupsA.put(group.getPath(), group));

		Map<String, NetcdfGroup> subGroupsB = new HashMap<>();
		groupB.getGroups().forEach(group -> subGroupsB.put(group.getPath(), group));

		if (!subGroupsA.keySet().equals(subGroupsB.keySet()))
			return Optional.of(groupA.getPath() + " do not contains same subgroups.\n");

		for (NetcdfGroup group : subGroupsA.values())
			compareGroups(group, subGroupsB.get(group.getPath()), hook).ifPresent(diffStringBuilder::append);

		// compare attributes
		compareAttributes(groupA, groupB, hook).ifPresent(diffStringBuilder::append);

		// get variables in map
		Map<String, NetcdfVariable> variablesA = groupA.getVariables().stream()
				.collect(Collectors.toMap(NetcdfVariable::getName, item -> item));
		Map<String, NetcdfVariable> variablesB = groupB.getVariables().stream()
				.collect(Collectors.toMap(NetcdfVariable::getName, item -> item));

		// do not compare variable in hook
		Predicate<String> isIgnored = key -> hook.isPresent() && hook.get().containsVariable(key)
				|| isIgnoredVariableType(variablesA.get(key));

		// compare variable lists
		variablesA.keySet().stream().filter(key -> !isIgnored.test(key) && !variablesB.containsKey(key))
				.forEach(key -> diffStringBuilder.append("Variable not present in second : " + key + "\n"));
		variablesB.keySet().stream().filter(key -> !isIgnored.test(key) && !variablesA.containsKey(key))
				.forEach(key -> diffStringBuilder.append("Variable not present in first : " + key + "\n"));

		for (String variable : variablesA.keySet()) {
			if (!isIgnored.test(variable) && variablesB.containsKey(variable))
				compareVariables(variable, variablesA.get(variable), variablesB.get(variable), hook)
						.ifPresent(diffStringBuilder::append);
		}

		return diffStringBuilder.length() > 0
				? Optional.of("Group[" + groupA.getName() + "]\n" + diffStringBuilder.toString())
				: Optional.empty();
	}

	/**
	 * Compares two {@link NetcdfVariable}.
	 * 
	 * @return an optional string, which contains potentially differences.
	 */
	public static Optional<String> compareVariables(String variableName, NetcdfVariable varA, NetcdfVariable varB,
			Optional<NetcdfComparatorHook> hook) throws NCException, IOException {
		return compareVariables(variableName, varA, varB, hook, 1);
	}

	/**
	 * Compares two {@link NetcdfVariable}.
	 * 
	 * @param varA : reference variable
	 * @param varB : computed variable
	 * @param multiplierFactor: multiplier factor to compare values
	 * @return an optional string, which contains potentially differences.
	 */
	public static Optional<String> compareVariables(String variableName, NetcdfVariable varA, NetcdfVariable varB,
			Optional<NetcdfComparatorHook> hook, float multiplierFactor) throws NCException, IOException {
		StringBuilder diffStringBuilder = new StringBuilder();

		// compare attributes
		compareAttributes(varA, varB, hook).ifPresent(diffStringBuilder::append);

		// compare values (break at the first difference...)
		ABufferWrapper bufferA = readVariable(varA);
		ABufferWrapper bufferB = readVariable(varB);
		Optional<PairValues> defaultValues = Optional.empty();
		if (hook.isPresent()) {
			NetcdfComparatorHook hookInstance = hook.get();
			defaultValues = hook.get().getDefaultValues(variableName);
			if (defaultValues.isPresent()) {
				double valueA = defaultValues.get().valueA.doubleValue();
				Optional<Double> sf = varA.getScaleFactor();
				if (sf.isPresent()) {
					valueA = valueA * sf.get();
				}
				Optional<Double> ao = varA.getAddOffsetFactor();
				if (ao.isPresent())
					valueA = valueA + ao.get();

				double valueB = defaultValues.get().valueB.doubleValue();
				sf = varB.getScaleFactor();
				if (sf.isPresent()) {
					valueB = valueB * sf.get();
				}
				ao = varB.getAddOffsetFactor();
				if (ao.isPresent())
					valueB = valueB + ao.get();
				PairValues p = hookInstance.new PairValues(Double.valueOf(valueA), Double.valueOf(valueB));
				defaultValues = Optional.of(p);

			}

		}
		long[] dims = varA.getDimensions();
		double tolerance = hook.isPresent() ? hook.get().getToleranceForVariable(varA.getName()) : 0;
		switch (dims.length) {
		case 1: // one dimension variable
			compare1DVariableValues(dims[0], bufferA, bufferB, tolerance, multiplierFactor, defaultValues)
					.ifPresent(diffStringBuilder::append);
			break;
		case 2:// two dimension variable
			compare2DVariableValues(dims, bufferA, bufferB, tolerance, multiplierFactor, defaultValues)
					.ifPresent(diffStringBuilder::append);
			break;
		default:
			throw new NotImplementedException();
		}

		// LOGGER.info("Compare variable {}: {}", varA.getPath(),
		// diffStringBuilder.length() == 0 ? "no diff" : diffStringBuilder.toString());
		return diffStringBuilder.length() > 0 ? Optional.of("[" + varA.getPath() + "] " + diffStringBuilder.toString())
				: Optional.empty();// Optional.of("["
		// +
		// varA.getPath()
		// +
		// "]
		// ===>>>
		// OK\n");
	}

	/**
	 * Compares two {@link NCObject}'s attributes.
	 * 
	 * @return an optional string, which contains potentially differences.
	 */
	public static Optional<String> compareAttributes(NCObject varA, NCObject varB, Optional<NetcdfComparatorHook> hook)
			throws NCException {
		StringBuilder diffStringBuilder = new StringBuilder();
		HashMap<String, String> attributesA = varA.getAttributes();
		HashMap<String, String> attributesB = varB.getAttributes();

		// do not compare attributes in hook
		Predicate<String> isIgnored = attributeName -> hook.isPresent()
				&& hook.get().containsAttribute(varA.getPath(), attributeName);

		// compare attribute lists
		attributesA.keySet().stream().filter(key -> !isIgnored.test(key) && !attributesB.containsKey(key))
				.forEach(key -> diffStringBuilder.append("Attribute not present in second : " + key + "\n"));
		attributesB.keySet().stream().filter(key -> !isIgnored.test(key) && !attributesA.containsKey(key))
				.forEach(key -> diffStringBuilder.append("Attribute not present in first : " + key + "\n"));

		// compare attribute values (only if attribute list are equals)
		attributesA.forEach((key, value) -> {

			if (!isIgnored.test(key) && attributesB.containsKey(key)) {

				// compare types
				try {
					DataType typeA = varA.getAttributeDataType(key);
					DataType typeB = varB.getAttributeDataType(key);
					if (!typeA.toString().equals(typeB.toString()))
						diffStringBuilder.append(
								String.format("Attribute '%s' has different types: %s vs %s %n", key, typeA, typeB));
				} catch (NCException e1) {
					e1.printStackTrace();// should not happen
				}

				if (attributesB.get(key).trim().compareTo(value.trim()) != 0) {
					String diff = String.format("Attribute '%s' has different values: %s vs %s", key, value,
							attributesB.get(key));
					try {
						double diffValue = Math
								.abs(Double.parseDouble(value) - Double.parseDouble(attributesB.get(key)));
						if (!hook.isPresent() || diffValue > hook.get().getToleranceForAttribute(key))
							diffStringBuilder.append(diff + "; diff = " + diffValue + "\n");
					} catch (NumberFormatException e) {
						// not a number
						diffStringBuilder.append(diff + "\n");
					}
				}
			}
		});

		return diffStringBuilder.length() > 0 ? Optional.of(diffStringBuilder.toString()) : Optional.empty();
	}

	/**
	 * Reads a {@link NetcdfVariable}.
	 */
	private static ABufferWrapper readVariable(NetcdfVariable netcdfVariable) throws NCException, IOException {
		long[] dims = netcdfVariable.getDimensions();
		int valueCount = (int) Arrays.stream(dims).reduce(1, (a, b) -> a * b);
		DataType dataType = netcdfVariable.getUnpackedDataType();

		// temporary file
		LargeArray largeArray = LargeArray.create(valueCount, dataType.getSize(), "_netcdf_comparator_");
		ByteBuffer byteBuffer = largeArray.getByteBuffer(0);
		byteBuffer.order(ByteOrder.nativeOrder());

		netcdfVariable.read(dims, byteBuffer, dataType, Optional.empty());
		byteBuffer.rewind();

		return BufferWrapperProvider.get(byteBuffer, netcdfVariable.getUnpackedDataType());
	}

	/**
	 * Reads and compares two 1D data buffers.
	 */
	private static Optional<String> compare1DVariableValues(long dim, ABufferWrapper bufferA, ABufferWrapper bufferB,
			double tolerance, float multiplierFactor, Optional<PairValues> defaultValues) {
		StringBuilder diffStringBuilder = new StringBuilder();
		int diffcount = 0;
		double maxDiff = Double.MIN_VALUE;
		for (int i = 0; i < dim; i++) {
			// test is done by considering values as doubles.
			double valueA = bufferA.getAsDouble(i) * multiplierFactor;
			double valueB = bufferB.getAsDouble(i);

			if (valueA == valueB) // values strictly equals, continue
				continue;
			if (defaultValues.isPresent()) {
				if (valueA == defaultValues.get().valueA.doubleValue()
						// We skip the check when valueB is NaN (special case to handle bad long conversion to double)

						&& (Double.isNaN(defaultValues.get().valueB.doubleValue())
								|| valueB == defaultValues.get().valueB.doubleValue())) {
					// values are both equals to default values, thus considered as equals
					// usefull when default values change for mbg
					continue;
				}

			}

			if (Math.abs(valueA - valueB) > tolerance) {
				if (diffStringBuilder.length() == 0)
					diffStringBuilder.append(String.format("Data not equals for index [%d]: %s != %s", i,
							Double.toString(valueA), Double.toString(valueB)));
				diffcount++;
				maxDiff = Math.max(Math.abs(valueA - valueB), maxDiff);
			}
		}
		if (diffcount > 0)
			diffStringBuilder.append(String.format(" (total: %d diff, max: %s)", diffcount, Double.toString(maxDiff)));
		return diffStringBuilder.length() > 0 ? Optional.of(diffStringBuilder.toString() + "\n") : Optional.empty();
	}

	/**
	 * Reads and compares two 2D data buffers.
	 */
	private static Optional<String> compare2DVariableValues(long[] dims, ABufferWrapper bufferA, ABufferWrapper bufferB,
			double tolerance, float multiplierFactor, Optional<PairValues> defaultValues) {
		StringBuilder diffStringBuilder = new StringBuilder();
		int diffcount = 0;
		double maxDiff = Double.MIN_VALUE;

		for (int i = 0; i < dims[0]; i++) {
			for (int j = 0; j < dims[1]; j++) {
				int index = (int) (i * dims[1] + j);
				try {
					double valueA = bufferA.getAsDouble(index) * multiplierFactor;
					double valueB = bufferB.getAsDouble(index);
					if (valueA == valueB) // values strictly equals, continue
						continue;
					if (defaultValues.isPresent()) {
						if (valueA == defaultValues.get().valueA.doubleValue()) {
							// We skip the check when valueB is NaN (special case to handle bad long conversion to
							// double)
							if (Double.isNaN(defaultValues.get().valueB.doubleValue())
									|| valueB == defaultValues.get().valueB.doubleValue()) {
								// values are both equals to default values, thus considered as equals
								// usefull when default values change for mbg
								continue;
							}
						}

					}
					if (Math.abs(valueA - valueB) > tolerance) {
						if (diffStringBuilder.length() == 0)
							diffStringBuilder.append(String.format("Data not equals for index [%d:%d]: %s != %s", i, j,
									Double.toString(valueA), Double.toString(valueB)));
						diffcount++;
						maxDiff = Math.max(Math.abs(valueA - valueB), maxDiff);
					}
				} catch (BufferUnderflowException | IllegalArgumentException e) {
					return Optional.of("Exception : " + e + "\n");
				}
			}
		}

		if (diffcount > 0)
			diffStringBuilder.append(String.format(" (total: %d diff, max: %s)", diffcount, Double.toString(maxDiff)));
		return diffStringBuilder.length() > 0 ? Optional.of(diffStringBuilder.toString() + "\n") : Optional.empty();
	}

	/**
	 * Checks if the variable has to be ignored because of its type.
	 */
	private static boolean isIgnoredVariableType(NetcdfVariable variable) {
		try {
			var type = variable.getRawDataType();
			boolean isIgnored = type == DataType.STRING;
			if (isIgnored)
				LOGGER.warn("Variable '{}' will be ignored, type not handled : {}", variable.getName(), type);
			return isIgnored;
		} catch (NCException e) {
			LOGGER.error("Error while checking type of variable (will be ignored by test) : {}", variable.getName());
			e.printStackTrace();
			return false;
		}
	}
}
