package fr.ifremer.globe.netcdf.util.comparator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class NetcdfComparatorHook {

	/**
	 * Variables to ignore.
	 */
	private final List<String> variables = new ArrayList<>();

	/**
	 * Map of attributes to ignore (sorted by variables).
	 */
	private final Map<String, List<String>> attributes = new HashMap<>();
	private final static String ALL_VARIABLE = "*";

	/**
	 * Tolerances for global attributes
	 */
	private final Map<String, Double> globalAttributesTolerances = new HashMap<>();
	
	/**
	 * Tolerances for variables
	 */
	private final Map<String, Double> variablesTolerances = new HashMap<>();

	/**
	 * Class used to ignore comparaison values when first valueA is encountered and match valueB
	 * 
	 * Due to the test restriction (conversion to double mainly) it is possible to specify valueB as NaN to ignore data when valueA is encountered
	 * */
	public class PairValues
	{
		public final Number valueB;
		public final Number valueA;

		public PairValues(Number valueA,Number valueB)
		{
			this.valueA=valueA;
			this.valueB=valueB;
		}
		
		
	}
	
	/**
	 * Default values for variables, default values (usually invalid values but not necessary) can change between two netcdf 
	 * This allow to ensure comparaison between this changes
	 */
	private final Map<String, PairValues> variablesDefaultlues= new HashMap<>();
	
	
	/**
	 * Constructor.
	 */
	public NetcdfComparatorHook() {
		attributes.put(ALL_VARIABLE, new ArrayList<>());
	}

	/**
	 * Adds a variable to ignore.
	 */
	public void ignoreVariable(String variablePath) {
		variables.add(variablePath);
	}

	/**
	 * Adds an attribute to ignore.
	 */
	public void ignoreAttributeForAllVariables(String attribut) {
		attributes.get(ALL_VARIABLE).add(attribut);
	}

	/**
	 * Adds an attribute to ignore for a specific variable.
	 */
	public void ignoreAttribute(String variablePath, String attribute) {
		if (!attributes.containsKey(variablePath))
			attributes.put(variablePath, new ArrayList<>());
		attributes.get(variablePath).add(attribute);
	}

	/**
	 * Adds a global attribute to ignore.
	 */
	public void ignoreGlobalAttribute(String attribute) {
		ignoreAttribute("", attribute);
	}

	/**
	 * @return true if this hook contains this variable.
	 */
	public boolean containsVariable(String variablePath) {
		return variables.contains(variablePath);
	}

	/**
	 * @return true if this hook contains this attribute.
	 */
	public boolean containsAttribute(String variablePath, String attribute) {
		return attributes.get(ALL_VARIABLE).contains(attribute)
				|| (attributes.containsKey(variablePath) && attributes.get(variablePath).contains(attribute));
	}

	/**
	 * Defines a tolerance delta value for a specified attribute.
	 */
	public void setToleranceForAttribute(String attributeName, double value) {
		globalAttributesTolerances.put(attributeName, value);
	}

	/**
	 * @return the tolerance delta for an attribute.
	 */
	public double getToleranceForAttribute(String attributeName) {
		return globalAttributesTolerances.containsKey(attributeName) ? globalAttributesTolerances.get(attributeName) : 0;
	}
	
	/**
	 * Defines a tolerance delta value for a specified attribute.
	 */
	public void setToleranceForVariable(String variableName, double value) {
		variablesTolerances.put(variableName, value);
	}

	/**
	 * @return the tolerance delta for an attribute.
	 */
	public double getToleranceForVariable(String variableName) {
		return variablesTolerances.containsKey(variableName) ? variablesTolerances.get(variableName) : 0;
	}

	
	/**
	 * Add a new default values mapping (if both numbers are encountered in comparaison, difference is ignored)
	 * */
	public void setDefaultValues(String variableName, Number fileAValue, Number fileBValue)
	{
		this.variablesDefaultlues.put(variableName, new PairValues(fileAValue,fileBValue));
	}
	
	/**
	 * return default values mapping parameter
	 * */
	public Optional<PairValues> getDefaultValues(String variableName)
	{
		PairValues value = variablesDefaultlues.getOrDefault(variableName, null);
		return value==null ? Optional.empty() : Optional.of(value);
	}
}
