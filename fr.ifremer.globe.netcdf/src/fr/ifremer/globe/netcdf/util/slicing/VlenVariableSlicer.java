/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.netcdf.util.slicing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;

import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable;
import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable.VlenWrapper;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Reader of values contained in a variable. <br>
 * Use a list of DimensionSlice to filter the data to load.
 */
class VlenVariableSlicer {

	/** Target type of data in buffer */
	private final DataType dataType;

	/** Variable to slice */
	private final NetcdfVlenVariable ncVariable;

	/** Operation to add a value in the buffer */
	private final FloatBufferConsumer bufferFiller;

	/** Scale factor to apply of each value */
	private final double scaleFactor;
	/** Offset to apply of each value */
	private final double addOffset;

	/**
	 * Constructor
	 */
	protected VlenVariableSlicer(DataType targetType, NetcdfVlenVariable ncVariable) throws NCException {
		dataType = targetType;
		this.ncVariable = ncVariable;

		// Scale/offset
		scaleFactor = ncVariable.hasScaleFactor() ? ncVariable.getAttributeDouble(CFStandardNames.CF_SCALE_FACTOR) : 1d;
		addOffset = ncVariable.hasAddOffset() ? ncVariable.getAttributeDouble(CFStandardNames.CF_ADD_OFFSET) : 0d;

		switch (targetType) {
		case FLOAT:
			bufferFiller = this::putFloatInBuffer;
			break;
		case DOUBLE:
			bufferFiller = this::putDoubleInBuffer;
			break;
		case LONG, ULONG:
			bufferFiller = this::putLongInBuffer;
			break;
		case INT, UINT:
			bufferFiller = this::putIntInBuffer;
			break;
		case BYTE, UBYTE:
			bufferFiller = this::putByteInBuffer;
			break;
		case SHORT, USHORT:
			bufferFiller = this::putShortInBuffer;
			break;
		default:
			throw new NCException("DataType not managed for reading a vlen : " + targetType);
		}
	}

	/** reads the entire contents of the variable and places it in a bytebuffer */
	public ByteBuffer read(List<DimensionSlice> slicing) throws NCException, GIOException {
		Vlen_t[] vlens = null;
		try {
			long mainDimension = slicing.stream().filter(d -> d.count() > 1l).mapToLong(DimensionSlice::count)
					.findFirst().orElse(1l);
			long[] start = slicing.stream().mapToLong(DimensionSlice::start).toArray();
			long[] count = slicing.stream().mapToLong(DimensionSlice::count).toArray();
			// Add the max vlen size as dimension
			vlens = ncVariable.get_vlen(start, count);
			int maxlen = Arrays.stream(vlens).mapToInt(v -> v.len).max().orElse(0);
			if (maxlen == 0) {
				throw new GIOException("No data");
			}

			// Create the buffer
			long bufferCapacity = dataType.getSize() * mainDimension * maxlen;
			ByteBuffer buffer = ByteBuffer.allocate((int) bufferCapacity).order(ByteOrder.nativeOrder());

			// Filling with vlen
			VlenWrapper wrapper = ncVariable.getVlenWrapper();
			for (int row = 0; row < maxlen; row++) {
				for (Vlen_t v : vlens) {
					bufferFiller.accept(row < v.len ? wrapper.get(v, row) : Float.NaN, buffer);
				}
			}
			return buffer;
		} catch (IllegalArgumentException e) {
			throw GIOException.wrap("Not enough memory", e);
		} finally {
			if (vlens != null) {
				ncVariable.free_vlen(vlens);
			}
		}
	}

	/** Filling the buffer with float */
	private void putFloatInBuffer(float value, ByteBuffer buffer) {
		buffer.putFloat(Float.isFinite(value) ? (float) (value * scaleFactor + addOffset) : Float.NaN);
	}

	/** Filling the buffer with double */
	private void putDoubleInBuffer(float value, ByteBuffer buffer) {
		buffer.putDouble(Float.isFinite(value) ? value * scaleFactor + addOffset : Double.NaN);
	}

	/** Filling the buffer with short */
	private void putShortInBuffer(float value, ByteBuffer buffer) {
		buffer.putShort((short) (value * scaleFactor + addOffset));
	}

	/** Filling the buffer with int */
	private void putIntInBuffer(float value, ByteBuffer buffer) {
		buffer.putInt((int) (value * scaleFactor + addOffset));
	}

	/** Filling the buffer with long */
	private void putLongInBuffer(float value, ByteBuffer buffer) {
		buffer.putLong((long) (value * scaleFactor + addOffset));
	}

	/** Filling the buffer with byte */
	private void putByteInBuffer(float value, ByteBuffer buffer) {
		buffer.put((byte) (value * scaleFactor + addOffset));
	}

	/**
	 * How to fill a buffer...
	 */
	@FunctionalInterface
	interface FloatBufferConsumer {
		void accept(float value, ByteBuffer buffer);
	}

}
