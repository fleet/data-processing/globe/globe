/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.netcdf.util.slicing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;

import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Reader of values contained in a variable. <br>
 * Use a list of DimensionSlice to filter the data to load. <br>
 * Resulting values are raw data (without any transformation)
 */
class RawDataVariableSlicer {

	/** Variable to slice */
	private final NetcdfVariable ncVariable;
	/** Variable type */
	private final DataType rawDataType;

	/**
	 * Constructor
	 */
	protected RawDataVariableSlicer(NetcdfVariable ncVariable) {
		this.ncVariable = ncVariable;
		rawDataType = DataType.getDataTypeFromNCType(ncVariable.getType());
	}

	/** reads the entire contents of the variable and places it in a bytebuffer */
	public ByteBuffer read(List<DimensionSlice> slicing) throws NCException, GIOException {
		try {
			long[] start = slicing.stream().mapToLong(DimensionSlice::start).toArray();
			long[] count = slicing.stream().mapToLong(DimensionSlice::count).toArray();
			long bufferCapacity = rawDataType.getSize() * Arrays.stream(count).reduce(1l, (d1, d2) -> d1 * d2);
			ByteBuffer buffer = ByteBuffer.allocate((int) bufferCapacity).order(ByteOrder.nativeOrder());
			ncVariable.get_byte(start, count, buffer);
			return buffer;
		} catch (IllegalArgumentException e) {
			throw GIOException.wrap("Not enough memory", e);
		}
	}

}
