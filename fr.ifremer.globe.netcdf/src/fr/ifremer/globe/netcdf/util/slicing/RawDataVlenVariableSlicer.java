/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.netcdf.util.slicing;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable;
import fr.ifremer.globe.netcdf.jna.Nc4prototypes.Vlen_t;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Reader of values contained in a variable. <br>
 * Use a list of DimensionSlice to filter the data to load.
 */
class RawDataVlenVariableSlicer {

	/** Raw type of data in buffer */
	private final DataType rawDataType;

	/** Variable to slice */
	private final NetcdfVlenVariable ncVariable;

	/**
	 * Constructor
	 */
	protected RawDataVlenVariableSlicer(NetcdfVlenVariable ncVariable) {
		this.ncVariable = ncVariable;
		rawDataType = DataType.getDataTypeFromNCType(ncVariable.getType());
	}

	/** reads the entire contents of the variable and places it in a bytebuffer */
	public List<ByteBuffer> read(List<DimensionSlice> slicing) throws NCException, GIOException {
		List<ByteBuffer> result = new LinkedList<>();
		Vlen_t[] vlens = null;
		try {
			long[] start = slicing.stream().mapToLong(DimensionSlice::start).toArray();
			long[] count = slicing.stream().mapToLong(DimensionSlice::count).toArray();
			vlens = ncVariable.get_vlen(start, count);
			for (Vlen_t vlen : vlens) {
				if (vlen.p != null && vlen.len > 0) {
					result.add(vlen.p.getByteBuffer(0, (long) rawDataType.getSize() * vlen.len));
				}
			}
			return result;
		} catch (IllegalArgumentException e) {
			throw GIOException.wrap("Not enough memory", e);
		} finally {
			if (vlens != null) {
				ncVariable.free_vlen(vlens);
			}
		}
	}
}
