/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.netcdf.util.slicing;

import java.util.List;

import fr.ifremer.globe.netcdf.ucar.NCDimension;

/**
 * Record of a slicing of dimension
 */
public record DimensionSlice(NCDimension dimension, long start, long count, boolean enabled) {

	/**
	 * Constructor
	 */
	public DimensionSlice(NCDimension dimension) {
		this(dimension, 0, dimension.getLength(), false);
	}

	/** Map a list of NCDimension to a list of DimensionSlice */
	public static List<DimensionSlice> fromDimensions(List<NCDimension> dimensions) {
		return dimensions.stream().map(DimensionSlice::new).toList();
	}

}
