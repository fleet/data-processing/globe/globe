/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.netcdf.util.slicing;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Facade of slicing tools
 */
public class VariableSlicerFacade {

	/**
	 * @return a INcVariableSlicer for the variable in the specified data type
	 */
	public Optional<ByteBuffer> sliceVariable(DataType targetType, NetcdfVariable ncVariable,
			List<DimensionSlice> slicing) throws NCException, GIOException {
		if (ncVariable.isVlen()) {
			return sliceVariable(targetType, (NetcdfVlenVariable) ncVariable, slicing);
		}

		return Optional.of(new GenericVariableSlicer(targetType, ncVariable).read(slicing));
	}

	/**
	 * Return a INcVariableSlicer for the variable. <br>
	 * Producted ByteBuffers will contained raw data<br>
	 */
	public List<ByteBuffer> sliceRawVariable(NetcdfVariable ncVariable, List<DimensionSlice> slicing)
			throws NCException, GIOException {
		if (ncVariable.isVlen()) {
			return sliceRawVariable((NetcdfVlenVariable) ncVariable, slicing);
		}
		return List.of(new RawDataVariableSlicer(ncVariable).read(slicing));
	}

	/**
	 * @return a INcVariableSlicer for the VLEN variable in the specified data type
	 */
	public Optional<ByteBuffer> sliceVariable(DataType targetType, NetcdfVlenVariable ncVariable,
			List<DimensionSlice> slicing) throws NCException, GIOException {
		if (targetType == DataType.FLOAT || targetType == DataType.DOUBLE
				|| targetType == ncVariable.getRawDataType()) {
			return Optional.of(new VlenVariableSlicer(targetType, ncVariable).read(slicing));
		}
		return Optional.empty();
	}

	/**
	 * @return a INcVariableSlicer for the VLEN variable. Producted ByteBuffers will contained raw data
	 */
	public List<ByteBuffer> sliceRawVariable(NetcdfVlenVariable ncVariable, List<DimensionSlice> slicing)
			throws NCException, GIOException {
		return new RawDataVlenVariableSlicer(ncVariable).read(slicing);
	}
}
