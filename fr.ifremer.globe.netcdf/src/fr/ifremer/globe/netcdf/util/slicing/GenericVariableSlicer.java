/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.netcdf.util.slicing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Reader of values contained in a variable. <br>
 * Use a list of DimensionSlice to filter the data to load.
 */
class GenericVariableSlicer {

	/** Target type of data in buffer */
	private final DataType dataType;

	/** Variable to slice */
	private final NetcdfVariable ncVariable;

	/** Variable to slice */
	private final Consumer<ByteBuffer> bufferReseter;

	/**
	 * Constructor
	 */
	protected GenericVariableSlicer(DataType targetType, NetcdfVariable ncVariable) {
		dataType = targetType;
		this.ncVariable = ncVariable;

		Number fillValue = getFillValue().orElse(null);
		if (fillValue != null && targetType == DataType.FLOAT) {
			bufferReseter = b -> fillValueToNan(b, fillValue.floatValue());
		} else if (fillValue != null && targetType == DataType.DOUBLE) {
			bufferReseter = b -> fillValueToNan(b, fillValue.doubleValue());
		} else {
			bufferReseter = null;
		}
	}

	/** reads the entire contents of the variable and places it in a bytebuffer */
	public ByteBuffer read(List<DimensionSlice> slicing) throws NCException, GIOException {
		try {
			long[] start = slicing.stream().mapToLong(DimensionSlice::start).toArray();
			long[] count = slicing.stream().mapToLong(DimensionSlice::count).toArray();
			long bufferCapacity = dataType.getSize() * Arrays.stream(count).reduce(1l, (d1, d2) -> d1 * d2);
			ByteBuffer buffer = ByteBuffer.allocate((int) bufferCapacity).order(ByteOrder.nativeOrder());
			ncVariable.read(start, count, buffer, dataType, Optional.empty());

			// Reset FillValue to NaN
			if (bufferReseter != null) {
				bufferReseter.accept(buffer);
			}

			return buffer;
		} catch (IllegalArgumentException e) {
			throw GIOException.wrap("Not enough memory", e);
		}
	}

	private void fillValueToNan(ByteBuffer buffer, float fillValue) {
		buffer.rewind();
		while (buffer.hasRemaining()) {
			buffer.mark();
			if (buffer.getFloat() == fillValue) {
				buffer.reset().putFloat(Float.NaN);
			}
		}
	}

	private void fillValueToNan(ByteBuffer buffer, double fillValue) {
		buffer.rewind();
		while (buffer.hasRemaining()) {
			buffer.mark();
			if (buffer.getDouble() == fillValue) {
				buffer.reset().putDouble(Double.NaN);
			}
		}
	}

	/**
	 * @return the FillValue or Optional.empty if none
	 */
	public Optional<Number> getFillValue() {
		DataType rawDataType = DataType.getDataTypeFromNCType(ncVariable.getType());
		switch (rawDataType) {
		case FLOAT:
			float fResult = ncVariable.getFloatFillValue();
			return Float.isFinite(fResult) ? Optional.of(fResult) : Optional.empty();
		case DOUBLE:
			double dResult = ncVariable.getFloatFillValue();
			return Double.isFinite(dResult) ? Optional.of(dResult) : Optional.empty();
		case INT, UINT:
			return Optional.of(ncVariable.getIntFillValue());
		case LONG, ULONG:
			return Optional.of(ncVariable.getLongFillValue());
		case BYTE, UBYTE:
			return Optional.of(ncVariable.getByteFillValue());
		case SHORT, USHORT:
			return Optional.of(ncVariable.getShortFillValue());
		default:
			return Optional.empty();
		}
	}

}
