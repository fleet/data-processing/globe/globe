package fr.ifremer.globe.editor.line;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.navigation.CompositeNavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.utils.NavigationTurnCutter;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.editor.line.model.CutLine;
import fr.ifremer.globe.editor.line.model.CutLineList;
import fr.ifremer.globe.editor.line.model.CutLineUndoStack;
import fr.ifremer.globe.editor.line.model.LineEditorChartSeries;
import fr.ifremer.globe.editor.line.preference.LinePreferences;
import fr.ifremer.globe.editor.line.view.ChartComposite;
import fr.ifremer.globe.editor.line.view.CutLineTableComposite;
import fr.ifremer.globe.editor.line.view.NavigationCutComposite;
import fr.ifremer.globe.editor.line.view.NavigationCutFromSensorTypeComposite;
import fr.ifremer.globe.editor.line.wizard.ExportToCutProcessWithWizard;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.NavLineTableComposite;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Navigation editor
 */
public class LineEditor {

	private static final Logger LOGGER = LoggerFactory.getLogger(LineEditor.class);

	public static final String LINE_EDITOR_PART_ID = "fr.ifremer.globe.editor.line.part.new";

	private static final int MAX_CUT_LINE_COUNTS = 500;

	private final EPartService partService;
	private final MPart mPart;
	private final LinePreferences preferences;

	// Model
	private final List<INavigationData> inputNavigationData;
	private final List<LineEditorChartSeries> navigationSeriesProxies = new ArrayList<>();
	private final CutLineList lineList;

	private ChartComposite chartComposite;
	private boolean showSensorType;
	private final ColorProvider colorProvider = new ColorProvider();

	private Spinner minDurationSpinner;

	/**
	 * Constructor
	 */
	@Inject
	public LineEditor(EPartService partService, @Named("inputNavigationData") List<INavigationData> inputs,
			@Named("mPart") MPart mPart, LinePreferences preferences) {
		this.mPart = mPart;
		this.lineList = new CutLineList(this);
		this.partService = partService;
		this.inputNavigationData = inputs;
		this.preferences = preferences;

		for (INavigationData navigationData : inputNavigationData) {
			LineEditorChartSeries navSeries = new LineEditorChartSeries(navigationData, new GColor("#616161"), this);
			navigationSeriesProxies.add(navSeries);
		}

		navigationSeriesProxies.sort(
				(s1, s2) -> (int) Math.round(s1.getData(0).getComparisonValue() - s2.getData(0).getComparisonValue()));
	}

	/**
	 * Initialization method: creates UI
	 */
	@PostConstruct
	public void createUI(Composite parent) {
		parent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		parent.setLayout(new GridLayout(2, false));

		chartComposite = new ChartComposite(parent, SWT.NONE, this);
		chartComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		chartComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		var rightComposite = new Composite(parent, SWT.NONE);
		rightComposite.setLayout(new GridLayout(1, false));
		rightComposite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));

		NavLineTableComposite<LineEditorChartSeries> navLineTableComposite = new NavLineTableComposite<LineEditorChartSeries>(
				rightComposite, SWT.FILL, navigationSeriesProxies, this::refreshChart);
		navLineTableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));

		var autoCutComposite = new NavigationCutComposite(rightComposite, SWT.NONE, this::getNavigation,
				this::addCutLinesAfterCompute);
		autoCutComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		var cutFromSensorTypeComposite = new NavigationCutFromSensorTypeComposite(rightComposite, SWT.NONE,
				this::getNavigation, this::addCutLinesAfterCompute);
		cutFromSensorTypeComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		Composite minDurationComposite = new Composite(rightComposite, SWT.NONE);
		minDurationComposite.setLayout(new GridLayout(2, false));
		minDurationComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		Label lblMinimumDuration = new Label(minDurationComposite, SWT.NONE);
		lblMinimumDuration.setText("Minimum duration of computed lines (min)");
		lblMinimumDuration.setToolTipText("Minimum duration of result lines (min)");
		minDurationSpinner = new Spinner(minDurationComposite, SWT.BORDER);
		minDurationSpinner.setMaximum(999999);
		minDurationSpinner.setSelection((int) NavigationTurnCutter.DEFAULT_MIN_LINE_DURATION.toMinutes());

		var cutLineTable = new CutLineTableComposite(rightComposite, SWT.NONE, this);
		GridData gdcutLineTable = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gdcutLineTable.minimumHeight = 250;
		cutLineTable.setLayoutData(gdcutLineTable);

		// run a resetZoom() after the UI created
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				resetZoom();
			}
		}, 500);
	}

	/**
	 * Dispose method
	 */
	@PreDestroy
	public void dispose() {
		if (mPart != null && partService != null)
			partService.hidePart(mPart, true);

		IProcessService.grab().runInForeground("Line editor closing...", (monitor, logger) -> {
			monitor.beginTask("Line editor closing...", inputNavigationData.size());
			for (INavigationData navData : inputNavigationData) {
				monitor.subTask("Close: " + navData.getFileName() + "...");
				try {
					navData.close();
				} catch (IOException e) {
					LOGGER.error("Error while closing file : " + navData.getFileName(), e);
				}
				monitor.worked(1);
			}
			return Status.OK_STATUS;
		});
	}

	/**
	 * Sets the selection mode
	 */
	public void setSelectionMode(SelectionMode mode) {
		chartComposite.setSelectionMode(mode);
	}

	/**
	 * Changes the edition mode
	 */
	public void changeEditionMode() {
		chartComposite.changeEditionMode();
	}

	/**
	 * Enables/disables showSensorType
	 */
	public void showNavigationDataType() {
		showSensorType = !showSensorType;
		chartComposite.refresh();
	}

	/**
	 * @return true if the sensor type has to be displayed
	 */
	public boolean isShowingSensorType() {
		return showSensorType;
	}

	/**
	 * @return a color from preferences
	 */
	public GColor getRandomColor() {
		return colorProvider.get();
	}

	/**
	 * Refreshes chart
	 */
	public void refreshChart() {
		chartComposite.refresh();
	}

	/**
	 * @return minimum and maximum series dates
	 */
	public long[] getMinMaxSeriesDates() {
		long startDate = (long) navigationSeriesProxies.get(0).getData(0).getComparisonValue();
		var lastSeriesProxy = navigationSeriesProxies.get(navigationSeriesProxies.size() - 1);
		long endDate = (long) lastSeriesProxy.getData(lastSeriesProxy.size() - 1).getComparisonValue();
		return new long[] { startDate, endDate };
	}

	/**
	 * Select all points
	 */
	public void selectAll() {
		List<IChartData> allSelection = new ArrayList<>();
		navigationSeriesProxies.forEach(nsp -> allSelection.addAll(nsp.getDataList()));
		lineList.add(allSelection);
	}

	/**
	 * Resets chart zoom
	 */
	public void resetZoom() {
		chartComposite.resetZoom();
	}

	/**
	 * Save method : opens the wizard to export {@link CutLine} in cut file(s)
	 */
	public boolean save() {
		Realm.getDefault().asyncExec(() -> {
			String outputDir = new File(inputNavigationData.get(0).getFileName()).getParent();

			StringPreferenceAttribute linePreference = preferences.getLineNamePattern();
			ExportToCutProcessWithWizard exportToCutProcessWithWizard = new ExportToCutProcessWithWizard(
					lineList.asList(), outputDir, linePreference.getValue());

			if (exportToCutProcessWithWizard.getWizardDialog().open() == Window.OK) {
				var model = exportToCutProcessWithWizard.getModel();
				// Update preferences
				linePreference.setValue(model.getLineNamePattern().get(), false);
				preferences.save();

				exportToCutProcessWithWizard.runInForeground();
				MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Line editor",
						"Cut line(s) exported in : " + model.getOutputDirectory().get().getAbsolutePath());
			}
		});
		return true;
	}

	private void addCutLinesAfterCompute(List<Pair<Instant, Instant>> timeIntervals) {
		getCutLines().getRealm().asyncExec(() -> {
			getCutLines().clear();
			var newCutLines = timeIntervals.stream()
					.filter(timeInterval -> Duration.between(timeInterval.getFirst(), timeInterval.getSecond())
							.compareTo(Duration.ofMinutes(minDurationSpinner.getSelection())) >= 0)
					.map(pair -> new CutLine("tmp", pair.getFirst().toEpochMilli(), pair.getSecond().toEpochMilli(),
							getRandomColor()))
					.toList();
			if (newCutLines.size() <= MAX_CUT_LINE_COUNTS)
				getCutLines().addList(newCutLines);
			else
				Messages.openErrorMessage("Compute cut lines",
						"Error : too many lines to display : " + newCutLines.size());
		});
	}

	public CutLineList getCutLines() {
		return lineList;
	}

	public CutLineUndoStack getUndoStack() {
		return lineList.getUndoStack();
	}

	public List<LineEditorChartSeries> getNavigationSeriesProxies() {
		return navigationSeriesProxies;
	}

	public INavigationData getNavigation() {
		return new CompositeNavigationData(inputNavigationData);
	}

}