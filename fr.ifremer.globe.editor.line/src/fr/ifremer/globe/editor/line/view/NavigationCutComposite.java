package fr.ifremer.globe.editor.line.view;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.utils.NavigationTurnCutter;
import fr.ifremer.globe.core.model.navigation.utils.NavigationTurnCutter.Mode;
import fr.ifremer.globe.core.model.navigation.utils.NavigationTurnCutter.ObservedVariable;
import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.turnfilter.TurnFilterMethods;
import fr.ifremer.globe.core.runtime.gws.param.turnfilter.TurnFilterParams;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.navigation.TurnFilterParametersComposite;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Composite to set parameters and run {@link NavigationTurnCutter}.
 */
public class NavigationCutComposite extends Composite {

	/**
	 * Filter methods
	 */
	public enum TurnFilterAlgos {
		GAP_JAVA(TurnFilterMethods.GAP.label),
		AVERAGE_JAVA(TurnFilterMethods.MEAN.label),
		GAP_PYTHON(TurnFilterMethods.GAP.label + " (PyAT)"),
		MEAN(TurnFilterMethods.MEAN.label + " (PyAT)"),
		STD(TurnFilterMethods.STD.label + " (PyAT)"),
		WEIGHTED_STD(TurnFilterMethods.WEIGHTED_STD.label + " (PyAT)");

		public final String value;

		TurnFilterAlgos(String name) {
			this.value = name;
		}
	}

	@Inject
	private GwsServiceAgent gwsServiceAgent;
	@Inject
	private IProcessService processService;

	/** Input **/
	private final Supplier<INavigationData> navigationSupplier;

	/** Output **/
	private final Consumer<List<Pair<Instant, Instant>>> resultConsumer;

	/** Inner widgets **/
	private Button cutGapCheckButton;
	private TurnFilterParametersComposite turnFilterParametersComposite;

	private ComboViewer methodComboViewer;

	/**
	 * Constructor
	 */
	public NavigationCutComposite(Composite parent, int style, Supplier<INavigationData> navigationSupplier,
			Consumer<List<Pair<Instant, Instant>>> resultConsumer) {
		super(parent, style);
		ContextInitializer.inject(this);
		this.navigationSupplier = navigationSupplier;
		this.resultConsumer = resultConsumer;
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);

		Group grpAutomaticCut = new Group(this, SWT.NONE);
		grpAutomaticCut.setToolTipText(
				"Computes cut lines by rejecting navigation point where values in intervals before and after are over the specified threshold.");
		grpAutomaticCut.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpAutomaticCut.setText("Automatic turns cut");
		grpAutomaticCut.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpAutomaticCut.setLayout(new GridLayout(3, false));

		Group grpParameters = new Group(grpAutomaticCut, SWT.NONE);
		grpParameters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		grpParameters.setLayout(new GridLayout(1, false));
		grpParameters.setText("Observed variables");
		turnFilterParametersComposite = new TurnFilterParametersComposite(grpParameters, SWT.NONE);

		cutGapCheckButton = new Button(grpAutomaticCut, SWT.CHECK);
		cutGapCheckButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		cutGapCheckButton.setText("Cut if interval is empty");
		cutGapCheckButton.setSelection(NavigationTurnCutter.DEFAULT_CUT_GAP);

		Label lblAlgorithm = new Label(grpAutomaticCut, SWT.NONE);
		lblAlgorithm.setToolTipText(
				"For each point, the interval window is divided in two. This option describes how the value of each part is calculated to obtain the difference between before and after the point.");
		lblAlgorithm.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAlgorithm.setText("Compare :");

		methodComboViewer = new ComboViewer(grpAutomaticCut, SWT.READ_ONLY);
		Combo combo = methodComboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		methodComboViewer.setContentProvider(new ArrayContentProvider());
		methodComboViewer.setInput(TurnFilterAlgos.values());
		methodComboViewer.setLabelProvider(LabelProvider.createTextProvider(opt -> ((TurnFilterAlgos) opt).value));
		methodComboViewer.setSelection(new StructuredSelection(TurnFilterAlgos.STD));

		Button btnCompute = new Button(grpAutomaticCut, SWT.NONE);
		btnCompute.setToolTipText("Compute with the furthest value in the intervals.");
		btnCompute.setText("Compute");
		btnCompute.addListener(SWT.Selection, evt -> compute());
		new Label(grpAutomaticCut, SWT.NONE);
		new Label(grpAutomaticCut, SWT.NONE);

	}

	private void compute() {
		switch ((TurnFilterAlgos) methodComboViewer.getStructuredSelection().getFirstElement()) {
		case GAP_JAVA -> compute(Mode.LAST_VALUE_IN_INTERVAL);
		case AVERAGE_JAVA -> compute(Mode.AVERAGE_IN_INTERVAL);
		case GAP_PYTHON -> computeWithService(TurnFilterMethods.GAP);
		case STD -> computeWithService(TurnFilterMethods.STD);
		case WEIGHTED_STD -> computeWithService(TurnFilterMethods.WEIGHTED_STD);
		case MEAN -> computeWithService(TurnFilterMethods.MEAN);
		}
	}

	private void computeWithService(TurnFilterMethods turnFilterMethods) {
		processService.runInForeground(turnFilterMethods.value,
				(monitor, logger) -> computeWithService(turnFilterMethods, monitor, logger));
	}

	private IStatus computeWithService(TurnFilterMethods turnFilterMethods, IProgressMonitor monitor, Logger logger) {
		// use temporary file to send navigation data
		File inputFile = null;
		try {
			inputFile = TemporaryCache.createTemporaryFile("nav_cut", ".nvi");
			INviWriter.grab().write(navigationSupplier.get(), inputFile.getAbsolutePath());
		} catch (Exception e) {
			return Status.error("Can not extract the navigation :\n" + e.getMessage());
		}
		var outputFilePath = new File(inputFile.getAbsolutePath().replace(".nvi", ".cut"));

		var params = new TurnFilterParams(//
				inputFile, //
				outputFilePath, //
				turnFilterMethods, //
				turnFilterParametersComposite.getHeadingParameters(), //
				turnFilterParametersComposite.getSpeedParameters(), //
				Optional.empty());

		// run
		IStatus result;
		try {
			result = gwsServiceAgent.filterTurn(params, monitor, logger);
			if (result.isOK() && outputFilePath.isFile()) {
				List<Pair<Instant, Instant>> cutLines = ProfileUtils.loadProfilesFromFile(outputFilePath) //
						.stream()//
						.map(Profile::toInstantPair)//
						.toList();
				resultConsumer.accept(cutLines);
			}
		} catch (FileFormatException | IOException e) {
			result = Status.error(e.getMessage());
		} catch (GException e) {
			result = Status.error("Service not available.");
		}
		return result;
	}

	/** Computes lines with input parameters **/
	private void compute(Mode mode) {
		try {
			var headingParameters = turnFilterParametersComposite.getHeadingParameters();
			var speedParameters = turnFilterParametersComposite.getSpeedParameters();

			Optional<ObservedVariable> observedVariables = Optional.empty();
			if (headingParameters.isPresent() && speedParameters.isPresent())
				observedVariables = Optional.of(ObservedVariable.HEADING_AND_SPEED);
			else if (headingParameters.isPresent())
				observedVariables = Optional.of(ObservedVariable.HEADING);
			else if (speedParameters.isPresent())
				observedVariables = Optional.of(ObservedVariable.SPEED);
			if (observedVariables.isEmpty())
				return;

			NavigationTurnCutter navigationCut = new NavigationTurnCutter(navigationSupplier.get());

			// set option
			navigationCut.setMode(mode);
			navigationCut.setObservedVariable(observedVariables.get());
			headingParameters.ifPresent(p -> {
				navigationCut.setHeadingInterval(p.interval());
				navigationCut.setHeadingThreshold(p.threshold());
			});
			speedParameters.ifPresent(p -> {
				navigationCut.setSpeedInterval(p.interval());
				navigationCut.setSpeedThreshold(p.threshold());
			});
			navigationCut.setCutGap(cutGapCheckButton.getSelection());

			IProcessService.grab().runInForeground(navigationCut);
			resultConsumer.accept(navigationCut.getCutLines());
		} catch (GIOException e) {
			Messages.openErrorMessage("Error with navigation cut", e);
		}

	}

}
