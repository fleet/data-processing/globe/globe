package fr.ifremer.globe.editor.line.view;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.editor.line.dialogs.NewLineDialog;
import fr.ifremer.globe.editor.line.model.CutLine;
import fr.ifremer.globe.editor.line.model.CutLineList;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.utils.date.DateUtils;

public class CutLineTableComposite extends Composite {

	/** Properties **/
	private final LineEditor lineEditor;
	private final CutLineList cutLines;

	/** Widgets **/
	private TableViewer tableViewer;
	private Table table;
	private TableColumn profileTableColumn;
	private TableColumn startDateTableColumn;
	private TableColumn endDateTableColumn;
	private Button btnRemove;
	private Button btnNewLine;
	private Button btnRemoveAll;
	private Label lblLines;
	private Spinner spinner;
	private Composite composite;
	private Label lblMinimumTimeBetween;
	private Label lblSec;
	private Button btnImport;
	private Label lblFirstIndex;
	private Spinner firstIndexSpinner;

	/**
	 * Constructor
	 */
	public CutLineTableComposite(Composite parent, int style, LineEditor lineEditor) {
		super(parent, style);
		setLayout(new GridLayout(5, false));

		this.lineEditor = lineEditor;
		cutLines = lineEditor.getCutLines();

		lblLines = new Label(this, SWT.NONE);
		lblLines.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 5, 1));
		lblLines.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		lblLines.setText("Cut Lines");
		cutLines.addChangeListener(e -> lblLines.setText("Cut Lines (" + cutLines.size() + ")"));

		composite = new Composite(this, SWT.NONE);
		GridLayout glComposite = new GridLayout(5, false);
		glComposite.marginHeight = 0;
		glComposite.marginWidth = 0;
		composite.setLayout(glComposite);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 5, 1));

		lblFirstIndex = new Label(composite, SWT.NONE);
		lblFirstIndex.setText("First index:");
		firstIndexSpinner = new Spinner(composite, SWT.BORDER);
		firstIndexSpinner.setMaximum(1000);
		@SuppressWarnings("rawtypes")
		IObservableValue target = WidgetProperties.spinnerSelection().observe(firstIndexSpinner);
		(new DataBindingContext()).bindValue(target, cutLines.getFirstCutLineIndex());

		String timeOptionToolTip = "Selected navigation points are splited in multiple cut lines if time between two points is higher than this value.";

		lblMinimumTimeBetween = new Label(composite, SWT.NONE);
		lblMinimumTimeBetween.setToolTipText(timeOptionToolTip);
		lblMinimumTimeBetween.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMinimumTimeBetween.setText("Time between two cut lines :");

		spinner = new Spinner(composite, SWT.BORDER);
		GridData gdSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdSpinner.widthHint = 48;
		spinner.setLayoutData(gdSpinner);
		spinner.setSize(new Point(0, 200));
		spinner.setMinimum(1);
		spinner.setMaximum(1000000);
		spinner.setToolTipText(timeOptionToolTip);
		target = WidgetProperties.spinnerSelection().observe(spinner);
		(new DataBindingContext()).bindValue(target, cutLines.getMinimumTimeBetweenTwoLine());

		lblSec = new Label(composite, SWT.NONE);
		lblSec.setText("sec");

		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		GridData gdTable = new GridData(SWT.FILL, SWT.FILL, false, true);
		gdTable.horizontalSpan = 5;
		gdTable.widthHint = 350;
		table.setLayoutData(gdTable);

		TableViewerColumn profileTableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		profileTableViewerColumn.setLabelProvider(buildColumnLabelProvider(CutLine::getId));
		profileTableColumn = profileTableViewerColumn.getColumn();
		profileTableColumn.setWidth(50);
		profileTableColumn.setText("Index");

		TableViewerColumn startDateTableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		startDateTableViewerColumn
				.setLabelProvider(buildColumnLabelProvider(l -> DateUtils.formatDate(new Date(l.getStartDate()))));
		startDateTableColumn = startDateTableViewerColumn.getColumn();
		startDateTableColumn.setWidth(150);
		startDateTableColumn.setText("Start time");

		String erroMsgTitle = "Cut line edition error";
		String errorDisjoin = "Cut lines must be disjoint.";

		startDateTableViewerColumn.setEditingSupport(new DateCellEditor(tableViewer, CutLine::getStartDate,
				// start date edition
				(cutLine, newDate) -> {
					// check validity
					if (newDate >= cutLine.getEndDate()) {
						Messages.openErrorMessage(erroMsgTitle, "Start date can't be after end date");
						return;
					}
					// check intersection with other lines
					for (CutLine l : cutLines) {
						if (l != cutLine && l.getStartDate() < cutLine.getStartDate() && newDate <= l.getEndDate()) {
							Messages.openErrorMessage(erroMsgTitle, errorDisjoin);
							return;
						}
					}

					cutLine.setStartDate(newDate);
					lineEditor.refreshChart();
					tableViewer.refresh();
				}));

		TableViewerColumn endDateTableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		endDateTableViewerColumn
				.setLabelProvider(buildColumnLabelProvider(l -> DateUtils.formatDate(new Date(l.getEndDate()))));
		endDateTableColumn = endDateTableViewerColumn.getColumn();
		endDateTableColumn.setWidth(150);
		endDateTableColumn.setText("End time");
		endDateTableViewerColumn.setEditingSupport(new DateCellEditor(tableViewer, CutLine::getEndDate,
				// end date edition
				(cutLine, newDate) -> {
					// check validity
					if (newDate <= cutLine.getStartDate()) {
						Messages.openErrorMessage(erroMsgTitle, "End date can't be before start date");
						return;
					}
					// check intersection with other lines
					for (CutLine l : cutLines) {
						if (l != cutLine && l.getStartDate() > cutLine.getStartDate() && newDate >= l.getStartDate()) {
							Messages.openErrorMessage(erroMsgTitle, errorDisjoin);
							return;
						}
					}

					cutLine.setEndDate(newDate);
					lineEditor.refreshChart();
					tableViewer.refresh();
				}));

		tableViewer.setContentProvider(new ObservableListContentProvider());
		tableViewer.setInput(cutLines);

		Listener removeSelection = e -> {
			List<CutLine> linesToRemove = new ArrayList<>();
			for (TableItem item : table.getSelection())
				linesToRemove.add((CutLine) item.getData());
			cutLines.removeAll(linesToRemove);
			cutLines.refresh();
		};

		// Add a listener to remove selected lines with "Delete" key
		table.addListener(SWT.KeyDown, e -> {
			if (e.keyCode == SWT.DEL)
				removeSelection.handleEvent(e);
		});

		table.addListener(SWT.Selection, e -> {
			lineEditor.getCutLines().forEach(cl -> cl.setSelected(false));
			for (TableItem item : table.getSelection()) {
				CutLine cutLine = (CutLine) item.getData();
				cutLine.setSelected(true);
			}
			lineEditor.refreshChart();
		});

		table.addListener(SWT.FocusOut, e -> {
			lineEditor.getCutLines().forEach(cl -> cl.setSelected(false));
			lineEditor.refreshChart();
		});

		btnNewLine = new Button(this, SWT.NONE);
		btnNewLine.setToolTipText("Define a new cut line with start and end dates");
		btnNewLine.setText("New");
		btnNewLine.addListener(SWT.Selection, e -> new NewLineDialog(getShell(), lineEditor).open());

		btnImport = new Button(this, SWT.NONE);
		btnImport.setToolTipText("Import cut lines from cut file(s)");
		btnImport.setText("Import");
		btnImport.addListener(SWT.Selection, e -> showImportDialog());

		btnRemove = new Button(this, SWT.NONE);
		btnRemove.setToolTipText("Remove selected cut lines");
		btnRemove.setText("Remove");
		btnRemove.addListener(SWT.Selection, removeSelection);

		btnRemoveAll = new Button(this, SWT.NONE);
		btnRemoveAll.setToolTipText("Remove all cut lines");
		btnRemoveAll.setText("Remove all");
		new Label(this, SWT.NONE);
		btnRemoveAll.addListener(SWT.Selection, e -> cutLines.clearAllAndSaveUndoAction());

	}

	private ColumnLabelProvider buildColumnLabelProvider(Function<CutLine, String> labelProvider) {
		return new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return labelProvider.apply((CutLine) element);
			}

			@Override
			public Color getForeground(Object element) {
				return ColorUtils.convertGColorToSWT(((CutLine) element).getColor());
			}
		};
	}

	/**
	 * Display a {@link FileDialog} to import ".cut" files
	 */
	private void showImportDialog() {
		FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
		fileDialog.setText("Import cut lines from cut file(s)");
		fileDialog.setFilterExtensions(new String[] { "*.cut" });
		fileDialog.setFilterNames(new String[] { "Cut files (*.cut)" });
		String selected = fileDialog.open();

		if (selected != null) {
			// Get profiles
			String path = fileDialog.getFilterPath();
			String[] names = fileDialog.getFileNames();
			Set<Profile> profiles = new HashSet<>();
			for (String name : names) {
				File currentFile = new File(path + File.separator + name);
				try {
					profiles.addAll(ProfileUtils.loadProfilesFromFile(currentFile));
				} catch (Exception e) {
					Messages.openErrorMessage("Import Error", "Unable to import " + currentFile.getName());
					return;
				}
			}

			// Create cut lines
			var cutlines = profiles.stream()
					.map(p -> new CutLine("import", p.getStartDate(), p.getEndDate(), lineEditor.getRandomColor()))
					.collect(Collectors.toList());
			cutLines.addList(cutlines);
		}
	}
}
