package fr.ifremer.globe.editor.line.view;

import java.util.Date;
import java.util.function.ObjLongConsumer;
import java.util.function.ToLongFunction;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

import fr.ifremer.globe.editor.line.model.CutLine;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * This class defines how to edit cells which display date.
 */
public class DateCellEditor extends EditingSupport {

	private final CellEditor editor;
	private final ToLongFunction<CutLine> dateProvider;
	private final ObjLongConsumer<CutLine> dateSetter;

	/**
	 * Constructor
	 */
	public DateCellEditor(TableViewer viewer, ToLongFunction<CutLine> dateProvider,
			ObjLongConsumer<CutLine> dateSetter) {
		super(viewer);
		this.editor = new TextCellEditor(viewer.getTable());
		this.dateProvider = dateProvider;
		this.dateSetter = dateSetter;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return editor;
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	@Override
	protected Object getValue(Object element) {
		return DateUtils.formatDate(new Date(dateProvider.applyAsLong((CutLine) element)));
	}

	@Override
	protected void setValue(Object element, Object userInputValue) {
		Date inputDate = DateUtils.parseDate((String) userInputValue);
		if (inputDate != null)
			dateSetter.accept((CutLine) element, inputDate.getTime());
	}
}