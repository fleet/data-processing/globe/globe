package fr.ifremer.globe.editor.line.view;

import java.io.IOException;

import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.editor.line.model.CutLineList;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;
import javafx.scene.paint.Color;

public class ChartComposite extends Composite {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartComposite.class);

	public enum EditionMode {
		ADD,
		CLEAN;
	}

	/** Properties **/
	private FXChart chart;
	private EditionMode editonMode = EditionMode.ADD;
	private SelectionMode selectionMode = SelectionMode.LINE_SELECTION;

	/** Constructor **/
	public ChartComposite(Composite parent, int style, LineEditor lineEditor) {
		super(parent, style);
		createUI();
		chart.setData(lineEditor.getNavigationSeriesProxies());
		updateSelectionMode();

		// binding with cutLineList
		CutLineList cutLineList = lineEditor.getCutLines();
		chart.addSelectionListener(selectedData -> {
			if (editonMode == EditionMode.ADD)
				cutLineList.add(selectedData);
			else if (editonMode == EditionMode.CLEAN)
				cutLineList.clean(selectedData);
		});

		chart.maintainAspectRatio();
	}

	/**
	 * Creates user interface
	 */
	private void createUI() {
		try {
			// Create charts
			chart = new FXChart(this, "Position");
			chart.setAxisSynchronisation(true);
			chart.setXAxisLabelFormatter(v -> {
				double longitude = v.doubleValue();
				return FormatLatitudeLongitude.longitudeToString(longitude > 180.0 ? longitude - 360.0 : longitude);
			});
			chart.setYAxisLabelFormatter(v -> FormatLatitudeLongitude.latitudeToString((double) v));
		} catch (IOException | FXChartException e) {
			LOGGER.error("Error during chart creation", e);
		}
	}

	/**
	 * Reloads chart
	 */
	public void refresh() {
		chart.refresh();
	}

	/**
	 * Updates chart selection mode and color
	 */
	private void updateSelectionMode() {
		if (editonMode == EditionMode.ADD)
			chart.setSelectionMode(selectionMode, Color.FORESTGREEN);
		else if (editonMode == EditionMode.CLEAN)
			chart.setSelectionMode(selectionMode, Color.INDIANRED);
	}

	/**
	 * Sets the selection mode
	 */
	public void setSelectionMode(SelectionMode newMode) {
		selectionMode = newMode;
		updateSelectionMode();
	}

	/**
	 * Changes the edition mode
	 */
	public void changeEditionMode() {
		editonMode = editonMode == EditionMode.ADD ? EditionMode.CLEAN : EditionMode.ADD;
		updateSelectionMode();
	}

	/**
	 * Resets chart zoom
	 */
	public void resetZoom() {
		chart.resetZoom();
	}

}
