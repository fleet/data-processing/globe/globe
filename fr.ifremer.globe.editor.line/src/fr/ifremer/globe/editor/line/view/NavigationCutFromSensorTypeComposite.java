package fr.ifremer.globe.editor.line.view;

import java.time.Instant;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.core.model.navigation.utils.SimpleNavigationCutter;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Composite to set parameters and run a {@link SimpleNavigationCutter} with sensor type filter.
 */
public class NavigationCutFromSensorTypeComposite extends Composite {

	/** Input **/
	private final Supplier<INavigationData> navigationSupplier;

	/** Output **/
	private final Consumer<List<Pair<Instant, Instant>>> resultConsumer;

	/** Inner widgets **/
	private ComboViewer comboViewer;

	/**
	 * Constructor
	 */
	public NavigationCutFromSensorTypeComposite(Composite parent, int style,
			Supplier<INavigationData> navigationSupplier, Consumer<List<Pair<Instant, Instant>>> resultConsumer) {
		super(parent, style);
		this.navigationSupplier = navigationSupplier;
		this.resultConsumer = resultConsumer;
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);

		Group grpAutomaticCut = new Group(this, SWT.NONE);
		grpAutomaticCut.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpAutomaticCut.setText("Cut from sensor type");
		grpAutomaticCut.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpAutomaticCut.setLayout(new GridLayout(3, false));

		comboViewer = new ComboViewer(grpAutomaticCut, SWT.READ_ONLY);
		Combo combo = comboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setInput(NmeaSensorType.values());
		comboViewer.setSelection(new StructuredSelection(NmeaSensorType.NOT_DEFINED));

		Button btnCompute = new Button(grpAutomaticCut, SWT.NONE);
		btnCompute.setToolTipText("Computes cut lines keeping only navigation from the selected sensor type.");
		btnCompute.setText("Compute");
		btnCompute.addListener(SWT.Selection, evt -> compute());
	}

	/** Computes lines with input parameters **/
	private void compute() {
		var selection = (NmeaSensorType) comboViewer.getStructuredSelection().getFirstElement();
		try {
			var navigationCutter = new SimpleNavigationCutter(navigationSupplier.get(), (navigationData, index) -> {
				try {
					return navigationData.getMetadataValue(NavigationMetadataType.SENSOR_TYPE, index)
							.filter(sensorType -> sensorType.toNmeaSensorType() == selection).isPresent();
				} catch (GIOException e) {
					e.printStackTrace();
				}
				return false;
			});
			IProcessService.grab().runInForeground(navigationCutter);
			resultConsumer.accept(navigationCutter.getCutLines());
		} catch (GIOException e) {
			Messages.openErrorMessage("Error with navigation cut", e);
		}

	}

}
