package fr.ifremer.globe.editor.line.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.value.WritableValue;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.utils.undo.UndoAction;
import fr.ifremer.globe.editor.line.LineEditor;

public class CutLineList extends WritableList<CutLine> {

	private final WritableValue<Integer> minimumTimeBetweenTwoLine = new WritableValue<>();
	private final WritableValue<Integer> firstCutLineIndex = new WritableValue<>();
	private final LineEditor lineEditor;
	private final CutLineUndoStack undoStack;

	/**
	 * Constructor
	 */
	public CutLineList(LineEditor lineEditor) {
		super();
		this.lineEditor = lineEditor;
		firstCutLineIndex.setValue(1);
		firstCutLineIndex.addValueChangeListener(e -> refresh());
		minimumTimeBetweenTwoLine.setValue(30);
		this.undoStack = new CutLineUndoStack(5);
		new CutLineListUndoAction(lineEditor, this); // Add first undo action
	}

	/**
	 * @return a classic cut line list
	 */
	public List<CutLine> asList() {
		return wrappedList;
	}

	/**
	 * @return local undo stack
	 */
	public CutLineUndoStack getUndoStack() {
		return undoStack;
	}

	/** @ return minimum time between two line parameter **/
	public WritableValue<Integer> getMinimumTimeBetweenTwoLine() {
		return minimumTimeBetweenTwoLine;
	}

	/** @ return first index for cut lines **/
	public WritableValue<Integer> getFirstCutLineIndex() {
		return firstCutLineIndex;
	}

	/**
	 * Reorder lines, set indexes and refreshes UIs.
	 */
	public void refresh() {
		int index = firstCutLineIndex.getValue();
		asList().sort((l1, l2) -> (int) (l1.getStartDate() - l2.getEndDate()));
		List<CutLine> newList = new ArrayList<>();
		for (CutLine line : asList()) {
			line.setId(String.format("%03d", index++));
			newList.add(line);
		}
		clear();
		addAll(newList);
		lineEditor.refreshChart();
		new CutLineListUndoAction(lineEditor, this);
	}

	/**
	 * Computes cut lines for the selected data in function of the parameter "minimumTimeBetweenTwoLine".
	 */
	private List<CutLine> getLineForSelection(List<IChartData> selectedData) {
		List<CutLine> result = new ArrayList<>();
		if (selectedData.isEmpty())
			return result;
		long startDate = Math.round(selectedData.get(0).getComparisonValue());
		long previousDate = startDate;
		long currentDate = 0;
		IChartData previousData = null;

		for (IChartData data : selectedData) {
			currentDate = Math.round(data.getComparisonValue());

			// Check data follow each other & with a minimum time gap
			if (previousData != null
					&& (!(previousData.isLast() && data.isFirst()) && (data.getIndex() - previousData.getIndex() != 1))
					|| currentDate - previousDate > minimumTimeBetweenTwoLine.getValue() * 1000) {
				// Start of a new line
				result.add(new CutLine("tmp", startDate, previousDate, lineEditor.getRandomColor()));
				startDate = currentDate;
			}

			previousDate = currentDate;
			previousData = data;
		}

		result.add(new CutLine("tmp", startDate, currentDate, lineEditor.getRandomColor()));
		return result;
	}

	//// PUBLIC API

	/**
	 * @return an optional with the cut line witch contains this {@link IChartData}, or empty if doesn't exist
	 */
	public Optional<CutLine> contains(IChartData iDataProxy) {
		var dataDatetime = (long) iDataProxy.getComparisonValue();
		for (var cutLine : wrappedList)
			if (cutLine.contains(dataDatetime))
				return Optional.of(cutLine);
		return Optional.empty();
	}

	/**
	 * Adds a new line (merge with the existing lines if possible)
	 */
	@Override
	public boolean add(CutLine newLine) {
		List<CutLine> lineToRemove = new ArrayList<>();
		for (CutLine line : asList()) {
			if (line.intersects(newLine)) {
				newLine.merge(line);
				lineToRemove.add(line);
			}
		}
		super.removeAll(lineToRemove);
		super.add(newLine);
		refresh();
		return true;
	}

	/**
	 * Updates the cut line list with the new selection.
	 */
	public void add(List<IChartData> selectedData) {
		List<CutLine> newCutLineList = getLineForSelection(selectedData);
		for (CutLine newLine : newCutLineList) {
			List<CutLine> lineToRemove = new ArrayList<>();
			for (CutLine line : asList()) {
				if (line.intersects(newLine)) {
					newLine.merge(line);
					lineToRemove.add(line);
				}
			}
			super.removeAll(lineToRemove);
			super.add(newLine);
		}
		refresh();
	}

	/**
	 * Adds a list of {@link CutLine}.
	 */
	public boolean addList(List<CutLine> newCutLineList) {
		for (CutLine newLine : newCutLineList) {
			List<CutLine> lineToRemove = new ArrayList<>();
			for (CutLine line : asList()) {
				if (line.intersects(newLine)) {
					newLine.merge(line);
					lineToRemove.add(line);
				}
			}
			super.removeAll(lineToRemove);
			super.add(newLine);
		}
		refresh();
		return false;
	}

	/**
	 * Overrides remove to save an {@link UndoAction}
	 */
	@Override
	public boolean remove(Object o) {
		boolean result = super.remove(o);
		new CutLineListUndoAction(lineEditor, this);
		refresh();
		return result;
	}

	/**
	 * Cleans the cut line list with the selected data.
	 */
	public void clean(List<IChartData> selectedData) {
		List<CutLine> newCutLineList = asList();
		List<CutLine> cleanLines = getLineForSelection(selectedData);

		for (CutLine lineToRemove : cleanLines) {
			List<CutLine> tmpList = new ArrayList<>();
			for (CutLine line : newCutLineList) {
				tmpList.addAll(line.substract(lineToRemove, lineEditor.getRandomColor()));
			}
			newCutLineList = tmpList;
		}

		clear();
		addAll(newCutLineList);
		refresh();
	}

	/**
	 * Clear (and save the current state for future undo / redo)
	 */
	public void clearAllAndSaveUndoAction() {
		super.clear();
		new CutLineListUndoAction(lineEditor, this);
		refresh();
	}

}
