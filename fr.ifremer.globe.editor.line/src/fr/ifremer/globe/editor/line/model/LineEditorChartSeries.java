package fr.ifremer.globe.editor.line.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.BasicChartSeries;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Implementation of {@link IChartSeries} for Line Editor.
 */
public class LineEditorChartSeries extends BasicChartSeries<LineEditorChartData> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LineEditorChartSeries.class);

	/** Properties **/
	private final List<LineEditorChartData> dataList = new ArrayList<>();

	/**
	 * Constructor
	 */
	public LineEditorChartSeries(INavigationData navigationData, GColor color, LineEditor lineEditor) {
		super(new File(navigationData.getFileName()).getName(), navigationData::getFileName);
		this.color = color;
		int index = 0;
		try {
			boolean spanning180Th = navigationData.computeGeoBox().isSpanning180Th();
			for (int i = 0; i < navigationData.size(); i++) {
				if (!Double.isNaN(navigationData.getLongitude(i)) && !Double.isNaN(navigationData.getLatitude(i))) {
					dataList.add(new LineEditorChartData(this, lineEditor, navigationData, index, i, spanning180Th));
					index++;
				}
			}
		} catch (GIOException e) {
			LOGGER.error("Error while getting latitude/longitude : " + e.getMessage(), e);
		}
	}

	@Override
	public int size() {
		return dataList.size();
	}

	public GColor getTransparentColor() {
		return new GColor(color.getRed(), color.getGreen(), color.getBlue(), (int) (0.2 * 255));
	}

	@Override
	public LineEditorChartData getData(int index) {
		return dataList.get(index);
	}

	public List<LineEditorChartData> getDataList() {
		return dataList;
	}

}
