package fr.ifremer.globe.editor.line.model;

import java.time.Instant;
import java.util.Optional;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Inner class to define {@link IChartData}
 */
public class LineEditorChartData implements IChartData {

	/** Color provider for {@link INavigationSensorType} **/
	private static final NavigationSensorTypeColorProvider NAVIGATION_SENSOR_TYPE_COLOR_PROVIDER = new NavigationSensorTypeColorProvider();

	/**
	 * Properties
	 */
	private final LineEditorChartSeries lineEditorChartSeries;
	private final LineEditor lineEditor;
	private final int index; // index in local list
	private final double lon;
	private final double lat;
	private final long time;
	private final INavigationSensorType sensorType;

	/**
	 * Constructor
	 * 
	 * @param navigationData
	 * @param lineEditor
	 * @throws GIOException
	 */
	protected LineEditorChartData(LineEditorChartSeries lineEditorChartSeries, LineEditor lineEditor,
			INavigationData navigationData, int index, int trueIndex, boolean spanning180Th) throws GIOException {
		this.lineEditorChartSeries = lineEditorChartSeries;
		this.index = index;
		double longitude = navigationData.getLongitude(trueIndex);
		this.lon = spanning180Th && longitude < 0.0 ? longitude + 360.0 : longitude;
		this.lat = navigationData.getLatitude(trueIndex);
		this.time = navigationData.getTime(trueIndex);
		this.sensorType = navigationData.getMetadataValue(NavigationMetadataType.SENSOR_TYPE, trueIndex)
				.orElse(NmeaSensorType.NOT_DEFINED);
		this.lineEditor = lineEditor;
	}

	@Override
	public LineEditorChartSeries getSeries() {
		return this.lineEditorChartSeries;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public double getX() {
		return lon;
	}

	@Override
	public double getY() {
		return lat;
	}

	@Override
	public double getComparisonValue() {
		return time;
	}

	@Override
	public String getTooltip() {
		StringBuilder sb = new StringBuilder();
		// cut line
		Optional<CutLine> cutLineContainer = lineEditor.getCutLines().contains(this);
		if (cutLineContainer.isPresent())
			sb.append(String.format("Cut line %s%n", cutLineContainer.get().getId()));
		// navigation line
		sb.append(lineEditorChartSeries.getTitle() + "\n");
		// date
		sb.append(Instant.ofEpochMilli(time) + "\n");
		// position
		if (lon > 180.0)
			sb.append(FormatLatitudeLongitude.longitudeToString(lon - 360.0));
		else
			sb.append(FormatLatitudeLongitude.longitudeToString(lon));
		sb.append(" : ");
		sb.append(FormatLatitudeLongitude.latitudeToString(lat));
		// sensor type
		if (lineEditor.isShowingSensorType())
			sb.append("\nSensor type : " + sensorType);
		return sb.toString();
	}

	@Override
	public int getWidth() {
		if (lineEditorChartSeries.isEnabled())
			return 4;

		Optional<CutLine> cutLineContainer = lineEditor.getCutLines().contains(this);
		return cutLineContainer.isPresent() && cutLineContainer.get().isSelected() ? 5 : IChartData.super.getWidth();
	}

	@Override
	public GColor getColor() {
		// return navigation data type if isShowingNavigationDataType
		if (lineEditor.isShowingSensorType())
			return sensorType.perform(NAVIGATION_SENSOR_TYPE_COLOR_PROVIDER);

		// return navigation color if selected
		if (lineEditorChartSeries.isEnabled())
			return new GColor("FFFF00");

		// return cut line color
		Optional<CutLine> cutLineContainer = lineEditor.getCutLines().contains(this);
		if (cutLineContainer.isPresent())
			return cutLineContainer.get().getColor();

		// by default return transparent color
		return lineEditorChartSeries.getTransparentColor();
	}

	@Override
	public Optional<String> getSectionId() {
		Optional<CutLine> cutLineContainer = lineEditor.getCutLines().contains(this);
		return cutLineContainer.isPresent() && !lineEditor.isShowingSensorType()
				? Optional.of("CutLine_" + lineEditor.getCutLines().asList().indexOf(cutLineContainer.get()))
				: Optional.empty();
	}

	@Override
	public String toString() {
		return "DataProxy [index=" + index + ", getX()=" + getX() + ", getY()=" + getY() + ", getComparisonValue()="
				+ getComparisonValue() + "]";
	}

}