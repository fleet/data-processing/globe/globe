package fr.ifremer.globe.editor.line.model;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.utils.undo.UndoAction;
import fr.ifremer.globe.editor.line.LineEditor;

/**
 * {@link UndoAction} for cut line list edition.
 */
public class CutLineListUndoAction extends UndoAction {

	private final LineEditor lineEditor;
	private final CutLineList cutLineList;
	private final List<CutLine> cutLines;

	/**
	 * Constructor
	 */
	public CutLineListUndoAction(LineEditor lineEditor, CutLineList cutLineList) {
		this.lineEditor = lineEditor;
		this.cutLineList = cutLineList;
		this.cutLines = new ArrayList<>(cutLineList.asList());
		cutLineList.getUndoStack().push(this);
	}

	@Override
	public String msgInfo() {
		return "line editor";
	}

	/**
	 * Here, undo it's just restore the previous cut line list.
	 */
	@Override
	protected void executeUndo() {
		cutLineList.getUndoStack().getPreviousUndoAction().ifPresent(CutLineListUndoAction::restore);
	}

	@Override
	public void executeRedo() {
		restore();
	}

	public void restore() {
		cutLineList.clear();
		cutLineList.addAll(cutLines);
		lineEditor.refreshChart();
	}

}
