package fr.ifremer.globe.editor.line.model;

import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NviLegacySensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.S7KSensorType;
import fr.ifremer.globe.core.model.navigation.sensortypes.INavigationSensorType.INavigationSensorTypeOperation;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Implementation of {@link INavigationSensorTypeOperation} to provide {@link GColor}.
 */
public class NavigationSensorTypeColorProvider implements INavigationSensorTypeOperation<GColor> {

	/** Default color is GRAY **/
	private static final GColor DEFAULT_COLOR = GColor.GRAY;

	@Override
	public GColor accept(NviLegacySensorType nviSensorType) {
		switch (nviSensorType) {
		case GPS:
			return GColor.DEFAULT_BLUE;
		case GPS_DIFF:
			return GColor.ORANGE;
		case CINEMATIC_GPS:
			return GColor.LIGHT_GREEN;
		case RTK_FLOAT:
			return GColor.DARK_GREEN;
		default:
			return DEFAULT_COLOR;
		}
	}

	@Override
	public GColor accept(S7KSensorType s7kSensorType) {
		switch (s7kSensorType) {
		case GPS:
			return GColor.DEFAULT_BLUE;
		case GPS_DIFF:
			return GColor.ORANGE;
		default:
			return DEFAULT_COLOR;
		}
	}

	@Override
	public GColor accept(NmeaSensorType nmeaSensorType) {
		switch (nmeaSensorType) {
		case GPS:
			return GColor.DEFAULT_BLUE;
		case GPS_DIFF:
			return GColor.ORANGE;
		case PPS:
			return GColor.YELLOW;
		case RTK:
			return GColor.DEFAULT_GREEN;
		case FLOAT_RTK:
			return GColor.DARK_GREEN;
		default:
			return DEFAULT_COLOR;
		}
	}

}
