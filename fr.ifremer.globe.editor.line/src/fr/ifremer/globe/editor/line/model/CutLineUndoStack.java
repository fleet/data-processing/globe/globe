package fr.ifremer.globe.editor.line.model;

import java.util.LinkedList;
import java.util.Optional;

import fr.ifremer.globe.ui.databinding.observable.WritableInt;

/**
 * Local undo stack
 */
public class CutLineUndoStack {

	public static final int MAX_ACTION = 5;
	public static final int INFINITE_ACTION = -1;

	protected WritableInt currentIdx = new WritableInt();

	protected LinkedList<CutLineListUndoAction> stack = new LinkedList<>();

	/** Maximum number of actions that can be put in stack */
	private final int maxActions;

	public CutLineUndoStack(int maxActions) {
		this.maxActions = maxActions;
	}

	public void push(CutLineListUndoAction a) {
		// purge
		while (stack.size() > currentIdx.get()) {
			CutLineListUndoAction last = stack.getLast();
			last.dispose();
			stack.removeLast();
		}
		stack.add(a);
		currentIdx.increment();
		// "cut" the stack if necessary
		if (maxActions != -1 && stack.size() > maxActions) {
			CutLineListUndoAction action = stack.remove(0);
			action.dispose();
			currentIdx.dercrement();
		}

	}

	public void undo() {
		if (currentIdx.get() > 1)
			stack.get(currentIdx.dercrement()).undo();
	}

	public void redo() {
		if (currentIdx.get() < stack.size()) {
			stack.get(currentIdx.get()).redo();
			currentIdx.increment();
		}
	}

	public Optional<CutLineListUndoAction> getPreviousUndoAction() {
		int previousIndex = currentIdx.get() - 1;
		return previousIndex >= 0 ? Optional.of(stack.get(previousIndex)) : Optional.empty();
	}

	/**
	 * @return current index as {@link WritableInt}.
	 */
	public WritableInt getCurrentIdx() {
		return currentIdx;
	}

	/**
	 * Returns true if there isn't any action in the stack yet (ie. no action or all actions already cancelled)
	 */
	public boolean isEmpty() {
		return currentIdx.get() < 2 || stack.isEmpty();
	}

	/**
	 * Returns true if all action in the stack already done (no redo action anymore).
	 */
	public boolean isDone() {
		return currentIdx.get() >= stack.size();
	}

	public void clear() {
		for (CutLineListUndoAction action : stack) {
			action.dispose();
		}
		stack.clear();
		currentIdx.set(0);
	}
}
