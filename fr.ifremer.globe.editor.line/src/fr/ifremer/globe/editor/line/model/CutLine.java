package fr.ifremer.globe.editor.line.model;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import javafx.scene.paint.Color;

public class CutLine {

	public static final GColor DEFAULT_COLOR = ColorUtils.convertJavaFXToGColor(Color.MIDNIGHTBLUE);
	public static final GColor DEFAULT_SELECTED_COLOR = ColorUtils.convertJavaFXToGColor(Color.DEEPSKYBLUE);

	/** Properties **/
	private String id;
	private long startDate;
	private long endDate;
	private GColor color = DEFAULT_COLOR;
	private boolean isSelected = false;

	/** Constructor **/
	public CutLine(String id, long startDate, long endDate, GColor color) {
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.color = color;
	}

	public boolean contains(long date) {
		return startDate <= date && date <= endDate;
	}

	public boolean contains(CutLine otherLine) {
		return startDate <= otherLine.startDate && otherLine.endDate <= endDate;
	}

	public boolean intersects(CutLine otherLine) {
		return otherLine.contains(this) || contains(otherLine.startDate) || contains(otherLine.endDate)
				|| contains(otherLine);
	}

	public void merge(CutLine otherLine) {
		startDate = Math.min(startDate, otherLine.startDate);
		endDate = Math.max(endDate, otherLine.endDate);
		this.color = otherLine.getColor();
	}

	/**
	 * Subtracts an other line (the result may be one line shorter, or two lines)
	 */
	public List<CutLine> substract(CutLine lineToSubstract, GColor otherColor) {
		List<CutLine> result = new ArrayList<>();
		// line to subtract is inside the current line: split the current line in two parts
		if (contains(lineToSubstract)) {
			if (startDate != lineToSubstract.startDate)
				result.add(new CutLine(id + "_start", startDate, lineToSubstract.startDate, color));
			if (endDate != lineToSubstract.endDate)
				result.add(new CutLine(id + "_end", lineToSubstract.endDate, endDate, otherColor));
		}
		// line to subtract contains entirely the current line: remove the current line
		else if (lineToSubstract.contains(this)) {
			return result;
		} else {
			// line to subtract is at the end of the current line : change end date
			if (contains(lineToSubstract.startDate))
				endDate = Math.min(endDate, lineToSubstract.startDate);

			// line to subtract is at the start of the current line : change start date
			if (contains(lineToSubstract.endDate))
				startDate = Math.max(startDate, lineToSubstract.endDate);

			result.add(this);
		}
		return result;
	}

	//// GETTERS AND SETTERS
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public GColor getColor() {
		return color;
	}

	public void setColor(GColor color) {
		this.color = color;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

}
