/**
 * 
 */
package fr.ifremer.globe.editor.line.preference;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

/**
 * Line editor preferences
 */
public class LinePreferences extends PreferenceComposite {

	private final StringPreferenceAttribute lineNamePattern;

	/** Constructor */
	public LinePreferences(PreferenceComposite root) {
		super(root, "Line editor");

		lineNamePattern = new StringPreferenceAttribute("line_name_pattern", "Line name pattern", "line");
		declareAttribute(lineNamePattern);
	}

	public StringPreferenceAttribute getLineNamePattern() {
		return lineNamePattern;
	}

}
