package fr.ifremer.globe.editor.line.wizard;

import java.util.List;

import fr.ifremer.globe.editor.line.model.CutLine;
import fr.ifremer.globe.editor.line.wizard.ExportToCutOptionPage.ExportToCutOptionPageModel;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class ExportToCutWizardModel extends ConvertWizardDefaultModel implements ExportToCutOptionPageModel {

	/** List of input lines */
	private final List<CutLine> inputLines;

	/** Export type */
	private final WritableObject<ExportType> exportType = new WritableObject<>(ExportType.MERGE_IN_SINGLE_FILE);

	/** Line name pattern */
	private final WritableString lineNamePattern;

	/** Default output directory */
	private final String outputDefaultDir;

	/**
	 * Constructor
	 */
	public ExportToCutWizardModel(List<CutLine> lineList, String outputDefaultDir, String lineName) {
		inputLines = lineList;
		outputFormatExtensions.add("cut");
		this.outputDefaultDir = outputDefaultDir;
		lineNamePattern = new WritableString(lineName);
	}

	// GETTERS

	public List<CutLine> getInputLines() {
		return inputLines;
	}

	@Override
	public WritableObject<ExportType> getExportType() {
		return exportType;
	}

	@Override
	public WritableString getLineNamePattern() {
		return lineNamePattern;
	}

	public String getDefaultOutputDir() {
		return outputDefaultDir;
	}
}