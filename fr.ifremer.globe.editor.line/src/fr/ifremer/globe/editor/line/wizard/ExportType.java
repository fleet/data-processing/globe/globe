package fr.ifremer.globe.editor.line.wizard;

public enum ExportType {
	MERGE_IN_SINGLE_FILE, SPLIT_IN_MULTIPLE_FILES;
}
