package fr.ifremer.globe.editor.line.wizard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.editor.line.model.CutLine;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;

/**
 * Wizard page used to define the output format in a sounder files conversion.
 */
public class ExportToCutOptionPage extends WizardPage {

	/** Model in MVC */
	private ExportToCutWizardModel exportToCutWizardModel;

	/** Widgets */
	private Button oneFileOptionButton;
	private Button multiFilesOptionButton;
	private Text lineNamePatternText;

	/** View/model bindings */
	protected DataBindingContext bindingContext;

	/**
	 * Constructor
	 */
	public ExportToCutOptionPage(ExportToCutWizardModel model) {
		super(ExportToCutOptionPage.class.getName(), "Select export option", null);
		this.exportToCutWizardModel = model;
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		setControl(container);
		GridLayout glContainer = new GridLayout(4, false);
		glContainer.verticalSpacing = 15;
		container.setLayout(glContainer);

		Label lblExportMode = new Label(container, SWT.NONE);
		lblExportMode.setText("Export mode:");

		oneFileOptionButton = new Button(container, SWT.RADIO);
		oneFileOptionButton.setText("One file");

		multiFilesOptionButton = new Button(container, SWT.RADIO);
		multiFilesOptionButton.setText("Multiple files (one per line)");
		new Label(container, SWT.NONE);

		Label lblLineName = new Label(container, SWT.NONE);
		lblLineName.setText("Line name pattern:");

		lineNamePatternText = new Text(container, SWT.BORDER);
		lineNamePatternText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		Label lblLineNameDescription = new Label(container, SWT.NONE);
		lblLineNameDescription.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		lblLineNameDescription.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblLineNameDescription.setText("(line name = \"PATTERN_INDEX\")");

		initDataBindings();
		initCheckBoxGroupBindings();
	}

	/** Sets whether this page is complete */
	protected void updatePageComplete() {
		if (oneFileOptionButton.getSelection()) {
			setPageComplete(true);
			WritableObjectList<File> inputFiles = exportToCutWizardModel.getInputFiles();
			inputFiles.clear();
			inputFiles.add(new File(exportToCutWizardModel.getDefaultOutputDir(), "lineEditor.cut"));
		} else if (multiFilesOptionButton.getSelection()) {
			List<File> result = new ArrayList<>();
			for (CutLine cutLine : exportToCutWizardModel.getInputLines())
				result.add(new File(lineNamePatternText.getText() + cutLine.getId()));
			exportToCutWizardModel.getInputFiles().clear();
			exportToCutWizardModel.getInputFiles().addAll(result);
			setPageComplete(true);
		} else {
			setPageComplete(false);
		}
	}

	/**
	 * Model of the OutputFormat wizard page
	 */
	public static interface ExportToCutOptionPageModel {

		/**
		 * Getter of export type
		 */
		public WritableObject<ExportType> getExportType();

		/**
		 * Getter of line name pattern
		 */
		public WritableString getLineNamePattern();
	}

	/** Initialize a specific binding on CheckBoxGroup (not managed by Windows Builder */
	protected void initCheckBoxGroupBindings() {

		// Bind SamplingMode on radio buttons
		var observeSelectionOneFileOption = WidgetProperties.buttonSelection().observe(oneFileOptionButton);
		var observeSelectionMultiFilesOption = WidgetProperties.buttonSelection().observe(multiFilesOptionButton);

		var featureRepoPolicyObservable = new SelectObservableValue<ExportType>(ExportType.class);
		featureRepoPolicyObservable.addOption(ExportType.MERGE_IN_SINGLE_FILE, observeSelectionOneFileOption);
		featureRepoPolicyObservable.addOption(ExportType.SPLIT_IN_MULTIPLE_FILES, observeSelectionMultiFilesOption);
		bindingContext.bindValue(featureRepoPolicyObservable, exportToCutWizardModel.getExportType());
		featureRepoPolicyObservable.addChangeListener(e -> updatePageComplete());
	}

	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			updatePageComplete();
		}
		super.setVisible(visible);
	}

	protected void initDataBindings() {
		bindingContext = new DataBindingContext();

		var target = WidgetProperties.text(SWT.Modify).observe(lineNamePatternText);
		bindingContext.bindValue(target, exportToCutWizardModel.getLineNamePattern());
		target.addChangeListener(e -> updatePageComplete());
	}
}
