package fr.ifremer.globe.editor.line.wizard;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.editor.line.model.CutLine;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * "Save to NVI" process with wizard
 */
public class ExportToCutProcessWithWizard extends DefaultProcessWithWizard<ExportToCutWizardModel> {

	private static final String TITLE = "Export to cut file (.cut)";

	/**
	 * Constructor
	 */
	public ExportToCutProcessWithWizard(List<CutLine> lineList, String outputDefaultDir, String lineName) {
		super(Display.getCurrent().getActiveShell(), TITLE,
				() -> new ExportToCutWizardModel(lineList, outputDefaultDir, lineName), ExportToCutWizard::new);
	}

	/**
	 * @throws GIOException
	 * @see fr.ifremer.globe.utils.process.GProcess#compute(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		ExportType exportType = model.getExportType().get();
		List<CutLine> cutLines = model.getInputLines();
		List<File> outputFiles = model.getOutputFiles().asList();

		// Export in one single file
		if (exportType == ExportType.MERGE_IN_SINGLE_FILE) {
			File outputFile = outputFiles.get(0);
			createCutFile(outputFile, cutLines);
			logger.info("Cut lines exported in a new cut file : {}.", outputFile.getName());
		}

		// Export in several files
		else if (exportType == ExportType.SPLIT_IN_MULTIPLE_FILES) {
			int index = 0;
			for (File outputFile : outputFiles) {
				createCutFile(outputFile, cutLines.get(index++));
				logger.info("Cut lines exported in a new cut file : {}.", outputFile.getName());
			}
		}
		return Status.OK_STATUS;
	}

	/**
	 * Creates a new ".cut" file with the specified list of {@link CutLine}
	 * 
	 * @throws GIOException
	 */
	private void createCutFile(File outputFile, List<CutLine> cutLines) throws GIOException {
		try {
			List<Profile> profiles = new ArrayList<>();
			for (CutLine cutLine : cutLines)
				profiles.add(getProfileFromCutLine(cutLine));
			ProfileUtils.writeProfilesToCutFile(profiles, outputFile);
		} catch (IOException e) {
			throw new GIOException("Error while creating file : " + outputFile.getName(), e);
		}
	}

	/**
	 * Creates a new ".cut" file with the specified {@link CutLine}
	 * 
	 * @throws GIOException
	 */
	private void createCutFile(File outputFile, CutLine cutLine) throws GIOException {
		try {
			Profile profile = getProfileFromCutLine(cutLine);
			ProfileUtils.writeProfilesToCutFile(Arrays.asList(profile), outputFile);
		} catch (IOException e) {
			throw new GIOException("Error while creating file : " + outputFile.getName(), e);
		}
	}

	/**
	 * @return a {@link Profile} from a {@link CutLine}
	 */
	private Profile getProfileFromCutLine(CutLine cutLine) {
		Profile profile = new Profile(model.getLineNamePattern().get() + "_" + cutLine.getId(), cutLine.getStartDate(),
				cutLine.getEndDate());
		return profile;
	}
}
