package fr.ifremer.globe.editor.line.wizard;

import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to convert to NVI files
 */
public class ExportToCutWizard extends Wizard {

	/** Model */
	private ExportToCutWizardModel exportToCutWizardModel;

	/** Pages */
	private SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ExportToCutWizard(ExportToCutWizardModel convertToNviWizardModel) {
		this.exportToCutWizardModel = convertToNviWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		addPage(new ExportToCutOptionPage(exportToCutWizardModel));
		selectOutputParametersPage = new SelectOutputParametersPage(exportToCutWizardModel);
		selectOutputParametersPage.setDescription("Cut lines are saved in cut file (.cut).");
		selectOutputParametersPage.enableLoadFilesAfter(false);
		selectOutputParametersPage.enableOverwriteExistingFiles(false);
		addPage(selectOutputParametersPage);
	}
	
	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return  selectOutputParametersPage.isPageComplete();
	}

}
