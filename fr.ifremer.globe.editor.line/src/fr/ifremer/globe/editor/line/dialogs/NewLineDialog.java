package fr.ifremer.globe.editor.line.dialogs;

import java.util.Calendar;
import java.util.TimeZone;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.editor.line.model.CutLine;

/**
 * Dialog to add a new cut line.
 */
public class NewLineDialog extends Dialog {

	private final LineEditor lineEditor;
	private DateTime startDateWidget;
	private DateTime startTimeWidget;
	private DateTime endDateWidget;
	private DateTime endTimeWidget;

	/**
	 * Constructor
	 */
	public NewLineDialog(Shell parentShell, LineEditor lineEditor) {
		super(parentShell);
		this.lineEditor = lineEditor;
	}

	// overriding this methods allows you to set the
	// title of the custom dialog
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Add new line");
	}

	/**
	 * Creates UI
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		GridLayout parentLayout = new GridLayout(1, false);
		parent.setLayout(parentLayout);

		Composite mainComposite = new Composite(parent, SWT.NONE);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		mainComposite.setLayout(new GridLayout(3, false));

		Label lblNewLabel = new Label(mainComposite, SWT.NONE);
		lblNewLabel.setText("Start:");
		startDateWidget = new DateTime(mainComposite, SWT.DATE | SWT.DROP_DOWN);
		startTimeWidget = new DateTime(mainComposite, SWT.TIME);

		Label lblEnd = new Label(mainComposite, SWT.NONE);
		lblEnd.setText("End:");
		endDateWidget = new DateTime(mainComposite, SWT.DATE | SWT.DROP_DOWN);
		endTimeWidget = new DateTime(mainComposite, SWT.TIME | SWT.LONG);

		initDateTimeWidgets();

		return parent;
	}

	/**
	 * Initializes date/time widgets
	 */
	private void initDateTimeWidgets() {
		long[] startEndDates = lineEditor.getMinMaxSeriesDates();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(startEndDates[0]);
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));

		// Start Date
		startDateWidget.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		// Start Time
		startTimeWidget.setTime(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

		cal.setTimeInMillis(startEndDates[1]);
		// End Date
		endDateWidget.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		// End Time
		endTimeWidget.setTime(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND) + 1);
	}

	/**
	 * Called when "ok" button is pressed
	 */
	@Override
	protected void okPressed() {
		// Start
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.set(Calendar.YEAR, startDateWidget.getYear());
		startCalendar.set(Calendar.MONTH, startDateWidget.getMonth());
		startCalendar.set(Calendar.DAY_OF_MONTH, startDateWidget.getDay());
		startCalendar.set(Calendar.HOUR_OF_DAY, startTimeWidget.getHours());
		startCalendar.set(Calendar.MINUTE, startTimeWidget.getMinutes());
		startCalendar.set(Calendar.SECOND, startTimeWidget.getSeconds());
		startCalendar.set(Calendar.MILLISECOND, 0);
		startCalendar.setTimeZone(TimeZone.getTimeZone("GMT"));

		// End
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.set(Calendar.YEAR, endDateWidget.getYear());
		endCalendar.set(Calendar.MONTH, endDateWidget.getMonth());
		endCalendar.set(Calendar.DAY_OF_MONTH, endDateWidget.getDay());
		endCalendar.set(Calendar.HOUR_OF_DAY, endTimeWidget.getHours());
		endCalendar.set(Calendar.MINUTE, endTimeWidget.getMinutes());
		endCalendar.set(Calendar.SECOND, endTimeWidget.getSeconds());
		endCalendar.set(Calendar.MILLISECOND, 0);
		endCalendar.setTimeZone(TimeZone.getTimeZone("GMT"));

		lineEditor.getCutLines().add(new CutLine("new", startCalendar.getTimeInMillis(), endCalendar.getTimeInMillis(),
				lineEditor.getRandomColor()));
		super.okPressed();
	}
}
