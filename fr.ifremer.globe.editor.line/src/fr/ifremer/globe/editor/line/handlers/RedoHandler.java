
package fr.ifremer.globe.editor.line.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.editor.line.model.CutLineUndoStack;

/**
 * Handler for redo action.
 */
public class RedoHandler {

	private CutLineUndoStack undoStack;

	@CanExecute
	public boolean canExecute(EPartService partService, MItem item) {
		if (undoStack == null && partService.getActivePart().getObject() instanceof LineEditor) {
			undoStack = ((LineEditor) partService.getActivePart().getObject()).getUndoStack();
			undoStack.getCurrentIdx().addChangeListener(evt -> item.setEnabled(!undoStack.isDone()));
			item.setEnabled(!undoStack.isDone());
		}
		return true;
	}

	@Execute
	public void execute(EPartService partService) {
		undoStack.redo();
	}

}
