package fr.ifremer.globe.editor.line.handlers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;

public class OpenLineEditorHandler extends AbstractNodeHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(LineEditor.class);

	@Inject
	INavigationService navigationService;

	@Inject
	IProcessService processService;

	private MItem callerItem;

	/**
	 * @return true if the handler is called from the project explorer
	 */
	private boolean openedFromProjectExplorer() {
		return callerItem.getParent().getElementId().equals("fr.ifremer.globe.ui.projectexplorer.popupmenu.openwith");
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return openedFromProjectExplorer()
				? !getSelectionAsList(getSelection(selectionService), ContentType::hasNavigation).isEmpty()
				: true;
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService,
			@Active MItem item) {
		this.callerItem = item;
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			MApplication application, EModelService modelService, EPartService partService, @Active MItem item)
			throws GIOException {

		// Get input files
		List<INavigationDataSupplier> inputs;
		// Get selected files if launch with "execute with"
		if (openedFromProjectExplorer()) {
			inputs = getSelectionAsList(getSelection(selectionService), ContentType::hasNavigation).stream()
					.map(nodeFileInfo -> navigationService.getNavigationDataSupplier(nodeFileInfo.getFileInfo()))
					.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
		}
		// Else open a FileDialog to select input files
		else {
			inputs = showImportDialog(shell);
		}

		// Import files
		List<INavigationData> inputNavigationData = new ArrayList<>();
		processService.runInForeground("Line editor loading...", (monitor, logger) -> {
			monitor.beginTask("Line editor loading...", 2 * inputs.size());
			for (INavigationDataSupplier navigationDataProvider : inputs) {
				try {
					INavigationData navigationData = navigationDataProvider.getNavigationData(true);
					monitor.subTask("Loading: " + navigationData.getFileName() + "...");
					monitor.worked(1);
					if (navigationData.size() == 0)
						throw new GIOException("Input navigation data empty : " + navigationData.getFileName());
					inputNavigationData.add(navigationData);
					if (monitor.isCanceled()) {
						inputNavigationData.forEach(navData -> {
							try {
								navData.close();
							} catch (IOException e) {
								LOGGER.error("Failed to close file : " + navigationData.getFileName(), e);
							}
						});
						inputNavigationData.clear();
						break;
					}
				} catch (GIOException e) {
					Messages.openErrorMessage("Line editor opening error", e);
					return Status.CANCEL_STATUS;
				}
				monitor.worked(1);
			}
			return Status.OK_STATUS;
		});

		// Open part
		if (!inputNavigationData.isEmpty()) {
			Display.getDefault().asyncExec(() -> {
				MPart part = partService.createPart(LineEditor.LINE_EDITOR_PART_ID);
				IEclipseContext context = application.getContext();
				context.set("inputNavigationData", inputNavigationData);
				context.set("mPart", part);
				PartManager.addEditableBindingContextToPart(part, application);
				PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
				PartManager.showPart(partService, part);
			});
		}
	}

	/**
	 * Display a {@link FileDialog} to import ".cut" files
	 * 
	 * @throws GIOException
	 */
	private List<INavigationDataSupplier> showImportDialog(Shell shell) throws GIOException {
		FileDialog fileDialog = new FileDialog(shell, SWT.OPEN | SWT.MULTI);
		fileDialog.setText("Select file(s) to open in line editor");
		fileDialog.setFilterExtensions(new String[] { "*.mbg;*.nvi;*.xsf;*.nc" });
		fileDialog.setFilterNames(new String[] { "Sounder or navigation files (*.mbg;*.nvi;*.xsf;*.nc)" });
		String selected = fileDialog.open();

		List<INavigationDataSupplier> result = new ArrayList<>();
		if (selected != null) {
			// Get profiles
			String path = fileDialog.getFilterPath();
			String[] names = fileDialog.getFileNames();
			for (String name : names) {
				File file = new File(path, name);
				Optional<INavigationDataSupplier> navigationDataProvider = navigationService
						.getNavigationDataProvider(file.getAbsolutePath());
				if (navigationDataProvider.isPresent()) {
					result.add(navigationDataProvider.get());
				} else {
					throw new GIOException("Convert to NVI error, input file not correct: " + file.getName());
				}
			}
		}
		return result;
	}

}