package fr.ifremer.globe.editor.line.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.utils.exception.runtime.NotImplementedException;

/**
 * Handler to manage shortcuts.
 */
public class KeyHandler {

	@Execute
	protected void execute(EPartService partService,
			@Named("fr.ifremer.globe.editor.line.commandparameter.keyPressed") String action) {
		
		LineEditor lineEditor = (LineEditor) partService.getActivePart().getObject();

		switch (action) {
		case "undo":
			lineEditor.getUndoStack().undo();
			break;
		case "redo":
			lineEditor.getUndoStack().redo();
			break;
		case "save":
			lineEditor.save();
			break;
		default:
			throw new NotImplementedException("Action not managed");
		}
	}
}
