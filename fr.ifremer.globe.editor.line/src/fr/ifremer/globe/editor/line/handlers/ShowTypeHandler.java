package fr.ifremer.globe.editor.line.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.line.LineEditor;

public class ShowTypeHandler {

	@Execute
	public void execute(EPartService partService) {
		LineEditor part = (LineEditor) partService.getActivePart().getObject();
		part.showNavigationDataType();
	}

}
