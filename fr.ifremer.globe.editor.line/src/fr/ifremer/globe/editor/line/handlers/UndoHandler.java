package fr.ifremer.globe.editor.line.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.line.LineEditor;
import fr.ifremer.globe.editor.line.model.CutLineUndoStack;

/**
 * Handler for undo action.
 */
public class UndoHandler {

	private CutLineUndoStack undoStack;

	@CanExecute
	public boolean canExecute(EPartService partService, MItem item) {
		if (undoStack == null && partService.getActivePart().getObject() instanceof LineEditor) {
			undoStack = ((LineEditor) partService.getActivePart().getObject()).getUndoStack();
			undoStack.getCurrentIdx().addChangeListener(evt -> item.setEnabled(!undoStack.isEmpty()));
			item.setEnabled(!undoStack.isEmpty());
		}
		return true;
	}

	@Execute
	public void execute(EPartService partService) {
		undoStack.undo();
	}

}
