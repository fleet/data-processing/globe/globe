package fr.ifremer.globe.editor.navigation;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.function.Consumer;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.editor.navigation.model.ImmersionSeries;
import fr.ifremer.globe.editor.navigation.model.NavigationEditorChartData;
import fr.ifremer.globe.editor.navigation.model.PositionSeries;
import fr.ifremer.globe.editor.navigation.wizard.SaveToNviProcessWithWizard;
import fr.ifremer.globe.editor.navigation.wizard.SaveToNviWizardModel;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.widget.NavLineTableComposite;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import javafx.scene.paint.Color;

/**
 * Navigation editor
 */
public class NavigationEditor implements ISaveable {

	public static final String NAVIGATION_EDITOR_PART_ID = "fr.ifremer.globe.editor.navigation.part.new";
	private static final Logger LOGGER = LoggerFactory.getLogger(NavigationEditor.class);

	@Inject
	private INavigationService navigationService;

	@Inject
	private FileInfoModel fileInfoModel;
	private final Consumer<FileInfoEvent> fileInfoModelListener = this::onFileInfoEvent;

	@Inject
	private IFileLoadingService fileLoadService;
	@Inject
	private IProcessService processService;

	/** Writer of NVI files */
	@Inject
	private INviWriter nviWriter;

	@Inject
	private EPartService partService;
	private MPart mPart;

	private FXChart positionChart;
	private FXChart immersionChart;

	// Model
	private Map<INavigationDataSupplier, Optional<INavigationData>> inputs = new HashMap<>();
	private Map<INavigationDataSupplier, GColor> inputColorMap = new HashMap<>();

	private List<IChartSeries<NavigationEditorChartData>> positionSeries = new ArrayList<>();
	private List<IChartSeries<NavigationEditorChartData>> immersionSeriesModel = new ArrayList<>();
	private NavLineTableComposite<IChartSeries<NavigationEditorChartData>> navLineTable;

	private Map<String, GColor> nviFilesToReload = new HashMap<>();

	// Selection mode
	public enum EditionMode {
		VALIDATE,
		INVALIDATE,
		ZOOM;
	}

	private EditionMode editionMode = EditionMode.INVALIDATE;
	private SelectionMode selectionMode = SelectionMode.RECTANGLE_SELECTION;

	/**
	 * Constructor
	 */
	@Inject
	public NavigationEditor(@Named("navigationDataSuppliers") List<INavigationDataSupplier> navigationDataSuppliers,
			@Named("mPart") MPart mPart) {
		this.mPart = mPart;
		ColorProvider colorProvider = new ColorProvider();

		for (int i = 0; i < navigationDataSuppliers.size() && i < 100; i++) {
			inputs.put(navigationDataSuppliers.get(i), Optional.empty());
			inputColorMap.put(navigationDataSuppliers.get(i), colorProvider.get());
		}
	}

	/**
	 * Initialization method: creates UI and loads input file
	 */
	@PostConstruct
	public void createUI(Composite parent) {
		parent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		parent.setLayout(new GridLayout(2, false));

		SashForm sashForm = new SashForm(parent, SWT.VERTICAL);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		navLineTable = new NavLineTableComposite<>(parent, SWT.FILL, positionSeries, () -> positionChart.refresh());
		navLineTable.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));

		try {
			// Create charts
			positionChart = new FXChart(sashForm, "Position");
			positionChart.setAxisSynchronisation(true);
			positionChart.setXAxisLabelFormatter(v -> {
				double longitude = v.doubleValue();
				return FormatLatitudeLongitude.longitudeToString(longitude > 180.0 ? longitude - 360.0 : longitude);
			});
			positionChart.setYAxisLabelFormatter(v -> FormatLatitudeLongitude.latitudeToString((double) v));

			immersionChart = new FXChart(sashForm, "Immersion (depth negative below surface)");
			DateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:SS");
			format.setTimeZone(TimeZone.getTimeZone("GMT"));

			immersionChart.setXAxisLabelFormatter(d -> format.format(new Date(d.longValue())));
			immersionChart.setVerticalZoomEnabled(false);

			setEditionMode(EditionMode.INVALIDATE);

			// binding with selection tools
			positionChart.addSelectionListener(selectedData -> {
				selectedData.forEach(
						data -> ((NavigationEditorChartData) data).setValidity(editionMode == EditionMode.VALIDATE));
				positionChart.refresh();
				immersionChart.refresh();
				mPart.setDirty(true);
			});

			immersionChart.addSelectionListener(selectedData -> {
				selectedData.forEach(
						data -> ((NavigationEditorChartData) data).setValidity(editionMode == EditionMode.VALIDATE));
				positionChart.refresh();
				immersionChart.refresh();
				mPart.setDirty(true);
			});

			sashForm.setWeights(2, 1);
			reloadChartModel();
			updateChartSelectionMode();

			// initialize model listening
			fileInfoModel.addListener(fileInfoModelListener);

		} catch (IOException | FXChartException e) {
			LOGGER.error("Error during navigation editor opening", e);
		}
	}

	/**
	 * Reloads chart's model from inputs
	 */
	private void reloadChartModel() {
		positionSeries.clear();
		immersionSeriesModel.clear();
		processService.runInForeground("Navigation editor loading...", (monitor, logger) -> {
			monitor.beginTask("Navigation editor loading...", 2 * inputs.size());
			// Create model
			for (INavigationDataSupplier navigationDataProvider : inputs.keySet()) {
				monitor.subTask("Loading navigation data ...");
				monitor.worked(1);
				loadFile(navigationDataProvider);
				monitor.worked(1);
			}
			positionChart.setData(positionSeries);
			immersionChart.setData(immersionSeriesModel);
			UIUtils.asyncExecSafety(navLineTable, navLineTable::refresh);
			return Status.OK_STATUS;
		});
	}

	/**
	 * Loads a {@link INavigationDataSupplier} (gets its {@link INavigationData} if it's not already loaded)
	 */
	private void loadFile(INavigationDataSupplier navigationDataProvider) {
		try {
			if (!inputs.get(navigationDataProvider).isPresent())
				inputs.put(navigationDataProvider, Optional.of(navigationDataProvider.getNavigationData(false)));
		} catch (GIOException e) {
			Messages.openErrorMessage("Navigation editor error", e);
			LOGGER.error("Error during navigation editor opening", e);
			Display.getDefault().asyncExec(this::dispose);
			return;
		}

		INavigationData navigationData = inputs.get(navigationDataProvider).get();
		boolean spanning180Th = false;
		try {
			spanning180Th = navigationData.computeGeoBox().isSpanning180Th();
		} catch (GIOException e) {
			LOGGER.warn("Error during geobox computation in navigation editor opening", e);
		}

		GColor color = inputColorMap.get(navigationDataProvider);

		// Add position and immersion series model
		PositionSeries positionModel = new PositionSeries(navigationData, spanning180Th, color);
		positionSeries.add(positionModel);
		ImmersionSeries immersionModel = new ImmersionSeries(navigationData, color);
		immersionSeriesModel.add(immersionModel);
	}

	/**
	 * Dispose method
	 */
	@PreDestroy
	public void dispose() {
		inputs.values().forEach(optionalDataProxy -> {
			if (optionalDataProxy.isPresent())
				closeNavigationDataProxy(optionalDataProxy.get());
		});
		fileInfoModel.removeListener(fileInfoModelListener);

		if (mPart != null)
			partService.hidePart(mPart, true);
	}

	/**
	 * Closes a {@link INavigationData}.
	 */
	private void closeNavigationDataProxy(INavigationData navDataProxy) {
		try {
			navDataProxy.close();
		} catch (IOException e) {
			LOGGER.error("Error while closing file : " + navDataProxy.getFileName(), e);
		}
	}

	/**
	 * Sets the selection mode
	 */
	public void setEditionMode(EditionMode mode) {
		this.editionMode = mode;
		updateChartSelectionMode();
	}

	public void setSelectionMode(FXChartController.SelectionMode mode) {
		this.selectionMode = mode;
		updateChartSelectionMode();
	}

	private void updateChartSelectionMode() {
		switch (editionMode) {
		case INVALIDATE:
			positionChart.setSelectionMode(selectionMode, Color.INDIANRED);
			immersionChart.setSelectionMode(selectionMode, Color.INDIANRED);
			break;
		case VALIDATE:
			positionChart.setSelectionMode(selectionMode, Color.FORESTGREEN);
			immersionChart.setSelectionMode(selectionMode, Color.FORESTGREEN);
			break;
		case ZOOM:
			positionChart.setSelectionMode(SelectionMode.ZOOM);
			immersionChart.setSelectionMode(SelectionMode.ZOOM);
			break;
		}
	}

	/**
	 * Resets chart zoom
	 */
	public void resetZoom() {
		positionChart.resetZoom();
		immersionChart.resetZoom();
	}

	@Override
	public boolean isDirty() {
		return mPart.isDirty();
	}

	/**
	 * Save method
	 */
	@Override
	@Persist
	public boolean doSave(IProgressMonitor monitor) {
		Display.getDefault().syncExec(() -> {
			monitor.setTaskName("Navigation editor save...");
			if (!exportNewNvi(monitor))
				return;

			List<INavigationDataSupplier> suppliersToReload = new ArrayList<>();
			inputs.entrySet().forEach(entry -> {
				INavigationDataSupplier key = entry.getKey();
				INavigationData value = entry.getValue().orElse(null);
				if (value != null && EnumSet.of(ContentType.NVI_NETCDF_4, ContentType.NVI_V2_NETCDF_4)
						.contains(value.getContentType())) {
					var filePath = value.getFileName();
					LOGGER.debug("Save {}", FilenameUtils.getBaseName(filePath));
					try {
						nviWriter.flush(value);
						suppliersToReload.add(key);

						mPart.setDirty(false);
					} catch (GIOException e) {
						LOGGER.error("Error while saving file : {} ", e.getMessage(), e);
					}
				}
			});

			// Remove NVI form input file list, close their data proxy and reload
			suppliersToReload.forEach(f -> {
				Optional<INavigationData> optDataProxy = inputs.remove(f);
				GColor color = inputColorMap.remove(f);
				if (optDataProxy.isPresent()) {
					var filePath = optDataProxy.get().getFileName();
					if (fileInfoModel.contains(filePath)) {
						closeNavigationDataProxy(optDataProxy.get());
						nviFilesToReload.put(filePath, color);
						fileLoadService.reload(filePath);
					}
				}
			});
			reloadChartModel();
		});
		return true;
	}

	/**
	 * Reacts on {@link FileInfoEvent}.
	 */
	private void onFileInfoEvent(FileInfoEvent event) {
		switch (event.getState()) {
		case LOADED:
			IFileInfo fileInfo = event.getfileInfo();
			if (fileInfo != null) {
				String filePath = fileInfo.getAbsolutePath();
				GColor color = nviFilesToReload.remove(filePath);
				if (color != null) {
					Optional<INavigationDataSupplier> optNavigationData = navigationService
							.getNavigationDataSupplier(fileInfo);
					if (optNavigationData.isPresent()) {
						inputs.put(optNavigationData.get(), Optional.empty());
						inputColorMap.put(optNavigationData.get(), color);
					}
					// All files reloaded. Refresh the editor now
					if (nviFilesToReload.isEmpty())
						UIUtils.asyncExecSafety(navLineTable, this::reloadChartModel);
				}
			}
			break;
		case ERROR, LOADING, RELOADING, REMOVED, UNLOADED:
		default:
			break;
		}
	}

	/**
	 * Creates new NVI files if necessary
	 *
	 * @return false if the process has been canceled
	 */
	public boolean exportNewNvi(IProgressMonitor monitor) {
		// Get input files not already exported as NVI
		Shell shell = Display.getCurrent().getActiveShell();
		SaveToNviProcessWithWizard convertToNviProcessWithWizard = new SaveToNviProcessWithWizard(shell);
		SaveToNviWizardModel convertToNviWizardModel = convertToNviProcessWithWizard.getModel();
		List<INavigationDataSupplier> filesToRemove = new ArrayList<>();

		List<GColor> colors = new ArrayList<>();

		inputs.entrySet().forEach(entry -> {
			INavigationDataSupplier key = entry.getKey();
			INavigationData value = entry.getValue().orElse(null);
			if (value != null && //
					!(EnumSet.of(ContentType.NVI_NETCDF_4, ContentType.NVI_V2_NETCDF_4)
							.contains(value.getContentType()))) {
				convertToNviWizardModel.addInput(new File(value.getFileName()), inputs.get(key).get());
				filesToRemove.add(key);
			}
		});

		// If no file to export in NVI : return
		if (convertToNviWizardModel.getInputFiles().isEmpty())
			return true;

		if (convertToNviProcessWithWizard.getWizardDialog().open() == Window.OK) {
			convertToNviProcessWithWizard.runInForeground();

			// Remove MBG from input file list and close their data proxy
			filesToRemove.forEach(f -> {
				Optional<INavigationData> optDataProxy = inputs.remove(f);
				colors.add(inputColorMap.remove(f));
				if (optDataProxy.isPresent())
					closeNavigationDataProxy(optDataProxy.get());
			});

			// Load new NVI files
			MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Navigation editor",
					"Created navigation file will be loaded into the navigation editor...");

			int i = 0;
			for (File outputFile : convertToNviWizardModel.getOutputFiles()) {
				Optional<INavigationDataSupplier> optNavigationData = navigationService
						.getNavigationDataProvider(outputFile.getAbsolutePath());
				if (optNavigationData.isPresent()) {
					inputs.put(optNavigationData.get(), Optional.empty());
					inputColorMap.put(optNavigationData.get(), colors.get(i));
					i++;
				}
			}

			reloadChartModel();
			monitor.done();
			return true;
		} else {
			return false;
		}
	}

}