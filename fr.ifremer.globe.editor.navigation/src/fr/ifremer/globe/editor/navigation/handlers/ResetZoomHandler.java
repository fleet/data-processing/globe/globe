package fr.ifremer.globe.editor.navigation.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.navigation.NavigationEditor;

public class ResetZoomHandler {

	@Execute
	public void execute(EPartService partService) {
		NavigationEditor part = (NavigationEditor) partService.getActivePart().getObject();
		part.resetZoom();
	}

}
