package fr.ifremer.globe.editor.navigation.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.navigation.NavigationEditor;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;

public class SelectionWithPolygonHandler {
	@Execute
	public void execute(EPartService partService) {
		NavigationEditor part = (NavigationEditor) partService.getActivePart().getObject();
		part.setSelectionMode(SelectionMode.POLYGON_SELECTION);
	}
}
