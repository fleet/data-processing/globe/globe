package fr.ifremer.globe.editor.navigation.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class HelpHandler {

	@Execute
	public void execute(EPartService partService) {
		
		StringBuilder sb = new StringBuilder();

		sb.append("Navigation editor commands :\n\n");
		
		sb.append("Left click : pan\n\n");
		
		sb.append("Right click : select an area with the current mode (zoom, validate or invalidate)\n\n");

		
		sb.append("Zoom : \n");
		sb.append("Ctrl + left click : zoom on area\n");
		sb.append("Double left click : reset zoom and axis bounds\n");
		sb.append("Mouse wheel : zoom\n");
		sb.append("Mouse wheel on X/Y axis: zoom only on one axis\n");
		
		
		// Load new NVI files
		MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Navigation editor help", sb.toString());

	}

}
