package fr.ifremer.globe.editor.navigation.handlers;

import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.editor.navigation.NavigationEditor;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.handler.PartManager;

public class OpenNavigationEditorHandler extends AbstractNodeHandler {

	private EPartService partService;

	@Inject
	INavigationService navigationService;

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return partService.findPart(NavigationEditor.NAVIGATION_EDITOR_PART_ID) == null
				&& !getSelectionAsList(getSelection(selectionService), ContentType::hasNavigation).isEmpty();
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService) {
		this.partService = partService;
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(ESelectionService selectionService, MApplication application, EModelService modelService,
			EPartService partService) {
		// get input navigation data suppliers
		var navigationDataSuppliers = getSelectionAsList(getSelection(selectionService), ContentType::hasNavigation)
				.stream().map(fileInfoNode -> navigationService.getNavigationDataSupplier(fileInfoNode.getFileInfo()))
				.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

		// create part
		Display.getDefault().asyncExec(() -> {
			MPart part = partService.createPart(NavigationEditor.NAVIGATION_EDITOR_PART_ID);
			IEclipseContext context = application.getContext();
			context.set("navigationDataSuppliers", navigationDataSuppliers);
			context.set("mPart", part);
			PartManager.addEditableBindingContextToPart(part, application);
			PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
			PartManager.showPart(partService, part);
		});
	}
}