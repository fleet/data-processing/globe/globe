package fr.ifremer.globe.editor.navigation.model;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.BasicChartSeries;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.exception.GIOException;
import javafx.scene.paint.Color;

/**
 * Implementation of {@link IChartSeries} for position chart.
 */
public class PositionSeries extends BasicChartSeries<NavigationEditorChartData> {

	private static final Logger LOGGER = LoggerFactory.getLogger(PositionSeries.class);

	private final INavigationData navigationData;
	private final boolean spanning180Th;

	/**
	 * Constructor
	 **/
	public PositionSeries(INavigationData navigationData, boolean spanning180Th, GColor color) {
		super(new File(navigationData.getFileName()).getName(), navigationData::getFileName);
		this.navigationData = navigationData;
		this.spanning180Th = spanning180Th;
		this.color = color;
	}

	@Override
	public int size() {
		return navigationData.size();
	}

	@Override
	public NavigationEditorChartData getData(int index) {
		return new NavigationEditorChartData(navigationData, this, index) {

			@Override
			public double getY() {
				try {
					return navigationData.getLatitude(index);
				} catch (GIOException e) {
					LOGGER.error("Error while getting Y value : " + e.getMessage(), e);
					return Double.NaN;
				}
			}

			@Override
			public double getX() {
				try {
					double longitude = navigationData.getLongitude(index);
					return spanning180Th && longitude < 0.0 ? longitude + 360.0 : longitude;
				} catch (GIOException e) {
					LOGGER.error("Error while getting X value : " + e.getMessage(), e);
					return Double.NaN;
				}
			}

			// TODO doesn't work
			/*
			 * @Override public int getWidth() { //if (isSelected()) //return 4; return super.getWidth(); }
			 */

			@Override
			public GColor getColor() {
				return getValidity() ? PositionSeries.this.getColor() : new GColor(Color.LIGHTGRAY.toString());
			}
		};

	}
}
