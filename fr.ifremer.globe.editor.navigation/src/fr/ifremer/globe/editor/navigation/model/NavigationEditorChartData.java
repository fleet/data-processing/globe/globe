package fr.ifremer.globe.editor.navigation.model;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.quality.MeasurandQualifierFlags;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Abstract implementation of {@link IChartData} for navigation data.
 */
public abstract class NavigationEditorChartData implements IChartData {

	private static final Logger LOGGER = LoggerFactory.getLogger(NavigationEditorChartData.class);

	protected final INavigationData navigationData;
	protected final IChartSeries<NavigationEditorChartData> series;
	protected final int index;

	/**
	 * Constructor
	 */
	public NavigationEditorChartData(INavigationData navigationData, IChartSeries<NavigationEditorChartData> series,
			int index) {
		this.navigationData = navigationData;
		this.series = series;
		this.index = index;
	}

	@Override
	public IChartSeries<NavigationEditorChartData> getSeries() {
		return series;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public String getTooltip() {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(DateUtils.formatDate(new Date(navigationData.getTime(index))));

			String lat = FormatLatitudeLongitude.latitudeToString(navigationData.getLatitude(index));
			sb.append("\nLatitude : " + lat);

			sb.append("\nLongitude : ");
			double longitude = navigationData.getLongitude(index);
			if (longitude > 180.0)
				sb.append(FormatLatitudeLongitude.longitudeToString(longitude - 360.0));
			else
				sb.append(FormatLatitudeLongitude.longitudeToString(longitude));

			return sb.toString();
		} catch (GIOException e) {
			return "Tooltip not available";
		}
	}

	@Override
	public double getComparisonValue() {
		try {
			return navigationData.getTime(index);
		} catch (GIOException e) {
			LOGGER.error("Error while getting time: " + e.getMessage(), e);
			return Double.NaN;
		}
	}

	@Override
	public Optional<String> getSectionId() {
		return getValidity() ? Optional.of("VALID_LINE") : IChartData.super.getSectionId();
	}

	// Specification to manage validity settings

	public boolean getValidity() {
		return navigationData.isValid(index);
	}

	public void setValidity(boolean value) {
		navigationData.setQualifierFlag(index, value ? MeasurandQualifierFlags.GOOD : MeasurandQualifierFlags.BAD);
	}

}
