package fr.ifremer.globe.editor.navigation.model;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.BasicChartSeries;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.exception.GIOException;
import javafx.scene.paint.Color;

/**
 * Implementation of {@link IChartSeries} for immersion chart.
 */
public class ImmersionSeries extends BasicChartSeries<NavigationEditorChartData> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImmersionSeries.class);

	protected final INavigationData navigationData;

	/** Constructor **/
	public ImmersionSeries(INavigationData navigationData, GColor color) {
		super(new File(navigationData.getFileName()).getName(), navigationData::getFileName);
		this.navigationData = navigationData;
		this.color = color;
	}

	@Override
	public int size() {
		return navigationData.size();
	}

	@Override
	public NavigationEditorChartData getData(int index) {
		return new NavigationEditorChartData(navigationData, this, index) {

			@Override
			public double getX() {
				try {
					return navigationData.getMetadataValue(NavigationMetadataType.TIME, index).orElseThrow(
							() -> new GIOException("Time not available for :" + navigationData.getFileName()));
				} catch (GIOException e) {
					LOGGER.error("Error while getting X value : " + e.getMessage(), e);
					return Double.NaN;
				}
			}

			@Override
			public double getY() {
				try {
					return navigationData.getMetadataValue(NavigationMetadataType.ELEVATION, index).orElse(Float.NaN);
				} catch (GIOException e) {
					LOGGER.error("Error while getting Y value : " + e.getMessage(), e);
					return Double.NaN;
				}
			}

			@Override
			public GColor getColor() {
				return getValidity() ? ImmersionSeries.this.getColor() : new GColor(Color.LIGHTGRAY.toString());
			}

		};

	}

}
