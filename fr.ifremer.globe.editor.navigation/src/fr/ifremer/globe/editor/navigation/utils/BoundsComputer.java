package fr.ifremer.globe.editor.navigation.utils;

import java.util.Collection;
import java.util.Optional;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides methods to get bounds from specified dimensions to others. For example: bounds in [Latitude x
 * Longitude] can be computed from bounds in [Time x immersion].
 */
public class BoundsComputer {

	/** Constructor **/
	private BoundsComputer() {
		// utility class
	}

	/**
	 * Get immersion bounds from position bounds
	 * 
	 * @throws GIOException
	 */
	public static double[] getImmersionBounds(double[] positionBounds,
			Collection<Optional<INavigationData>> navigationDataProxies) throws GIOException {
		double minX = Double.NaN;
		double maxX = Double.NaN;
		double minY = Double.NaN;
		double maxY = Double.NaN;
		for (Optional<INavigationData> optNavigationDataProxy : navigationDataProxies) {
			if (optNavigationDataProxy.isPresent()) {
				INavigationData navigationData = optNavigationDataProxy.get();
				for (int index : navigationData) {
					double lon = navigationData.getLongitude(index);
					double lat = navigationData.getLatitude(index);
					if (positionBounds[0] <= lon && lon <= positionBounds[1] && positionBounds[2] <= lat
							&& lat <= positionBounds[3]) {
						var time = (double) navigationData.getMetadataValue(NavigationMetadataType.TIME, index).orElseThrow();
						minX = Double.isNaN(minX) ? time : Math.min(minX, time);
						maxX = Double.isNaN(maxX) ? time : Math.max(maxX, time);

						var immersion = -navigationData.getMetadataValue(NavigationMetadataType.ELEVATION, index)
								.orElseThrow();
						minY = Double.isNaN(minY) ? immersion : Math.min(minY, immersion);
						maxY = Double.isNaN(maxY) ? immersion : Math.max(maxY, immersion);
					}

				}
			}
		}
		return new double[] { minX, maxX, minY, maxY };
	}

	/**
	 * Get position bounds from immersion/time bounds
	 * 
	 * @throws GIOException
	 */
	public static double[] getPositionBounds(double[] immertionBounds,
			Collection<Optional<INavigationData>> navigationDataProxies) throws GIOException {
		double minX = Double.NaN;
		double maxX = Double.NaN;
		double minY = Double.NaN;
		double maxY = Double.NaN;
		for (Optional<INavigationData> optNavigationDataProxy : navigationDataProxies) {
			if (optNavigationDataProxy.isPresent()) {
				INavigationData navigationData = optNavigationDataProxy.get();
				for (int index : navigationData) {
					double time = navigationData.getTime(index);
					float immersion = - navigationData.getMetadataValue(NavigationMetadataType.ELEVATION, index).orElseThrow();
					if (immertionBounds[0] <= time && time <= immertionBounds[1] && immertionBounds[2] <= immersion
							&& immersion <= immertionBounds[3]) {
						minX = Double.isNaN(minX) ? navigationData.getLongitude(index)
								: Math.min(minX, navigationData.getLongitude(index));
						maxX = Double.isNaN(maxX) ? navigationData.getLongitude(index)
								: Math.max(maxX, navigationData.getLongitude(index));
						minY = Double.isNaN(minY) ? navigationData.getLatitude(index)
								: Math.min(minY, navigationData.getLatitude(index));
						maxY = Double.isNaN(maxY) ? navigationData.getLatitude(index)
								: Math.max(maxY, navigationData.getLatitude(index));
					}
				}
			}
		}
		return new double[] { minX, maxX, minY, maxY };
	}

}
