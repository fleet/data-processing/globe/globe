package fr.ifremer.globe.editor.navigation.wizard;

import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to convert to NVI files
 */
public class SaveToNviWizard extends Wizard {

	/** Model */
	protected SaveToNviWizardModel convertToNviWizardModel;

	/** Pages */
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public SaveToNviWizard(SaveToNviWizardModel convertToNviWizardModel) {
		this.convertToNviWizardModel = convertToNviWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		NviTypeWizardPage nviTypeWizardPage = new NviTypeWizardPage(convertToNviWizardModel);
		addPage(nviTypeWizardPage);

		selectOutputParametersPage = new SelectOutputParametersPage(convertToNviWizardModel);
		selectOutputParametersPage.setDescription("Navigation data are saved in navigation file (.nvi).");
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectOutputParametersPage.isPageComplete();
	}

}
