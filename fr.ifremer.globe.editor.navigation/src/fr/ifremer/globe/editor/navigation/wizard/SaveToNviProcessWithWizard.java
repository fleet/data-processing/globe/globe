package fr.ifremer.globe.editor.navigation.wizard;

import java.io.File;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.ExportToNviParams;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.exception.GException;

/**
 * "Save to NVI" process with wizard
 */
public class SaveToNviProcessWithWizard extends DefaultProcessWithWizard<SaveToNviWizardModel> {

	@Inject
	private GwsServiceAgent gwsServiceAgent;

	/**
	 * Constructor
	 */
	public SaveToNviProcessWithWizard(Shell shell) {
		super(shell, "Export to navigation file", SaveToNviWizardModel::new, SaveToNviWizard::new);
		ContextInitializer.inject(this);
	}

	/**
	 * @see fr.ifremer.globe.utils.process.GProcess#compute(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) {
		List<File> inputFiles = model.getInputFiles().asList();
		logger.info("Beginning conversion of {} files", inputFiles.size());
		monitor.beginTask("Navigation files creation...", inputFiles.size());

		if (model.getNviType().doGetValue() == ContentType.NVI_NETCDF_4) {
			List<File> ouputFiles = model.getOutputFiles().asList();
			for (int i = 0; i < inputFiles.size(); i++) {
				File file = inputFiles.get(i);
				exportToNvi(file, ouputFiles.get(i), model.getInputs().get(file), monitor, logger);
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
			}
			return Status.OK_STATUS;
		} else {
			// Export with GWS
			try {
				return gwsServiceAgent.exportToNvi(//
						new ExportToNviParams(//
								model.getInputFiles().asList(), // inputFiles
								Optional.of(List.copyOf(model.getInputs().values())), // navigationData
								Optional.empty(), // navPointsInFiles, empty to auto-generates it
								Optional.of(model.getOutputFiles().asList()), // ouputFiles
								Optional.of(model.getOverwriteExistingFiles().doGetValue()), // overwrite
								Optional.ofNullable(
										model.getLoadFilesAfter().isTrue() ? model.getWhereToloadFiles().doGetValue()
												: null) // whereToLoadOutputFiles
						), //
						monitor, logger);
			} catch (GException e) {
				return Status.error("Unable to execute the service exportToNvi", e);
			}
		}
	}

	/**
	 * Execute the conversion to NVI files
	 */
	protected void exportToNvi(File inputFile, File outputFile, INavigationData inputNavDataProxy,
			IProgressMonitor monitor, Logger logger) {
		monitor.subTask("Processing " + inputFile.getName());
		logger.info("Converting {} to NVI ", inputFile.getName());

		if (shouldBeProcessed(outputFile, logger)) {
			try {
				INviWriter.grab().write(inputNavDataProxy, outputFile.getAbsolutePath());
				logger.info("Conversion of {} to {} done", inputFile.getName(), outputFile.getName());
				loadOutputFile(outputFile, monitor);
				monitor.worked(1);
			} catch (Exception e) {
				logger.error("Error while NVI conversion of {} to {} ({})", inputFile.getName(), outputFile.getName(),
						e.getMessage(), e);
				FileUtils.deleteQuietly(outputFile);
			}
		}
	}

}
