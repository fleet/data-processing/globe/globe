package fr.ifremer.globe.editor.navigation.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;

public class NviTypeWizardPage extends WizardPage {

	private final WritableObject<ContentType> nviType;
	private Button btnNviLegacy;

	public NviTypeWizardPage(SaveToNviWizardModel convertToNviWizardModel) {
		super("NVI_Format");
		setTitle("Expected NVI format");
		setDescription("Format of NVI files to export");

		nviType = convertToNviWizardModel.getNviType();
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		setControl(container);
		container.setLayout(new GridLayout(1, false));

		btnNviLegacy = new Button(container, SWT.RADIO);
		btnNviLegacy.setText("NVI legacy (.nvi)");
		btnNviLegacy.setSelection(nviType.getValue() == ContentType.NVI_NETCDF_4);
		btnNviLegacy.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::switchNviType));

		Button btnNviV2 = new Button(container, SWT.RADIO);
		btnNviV2.setText("NVI V2 (.nvi.nc)");
		btnNviV2.setSelection(nviType.getValue() == ContentType.NVI_V2_NETCDF_4);
		btnNviLegacy.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::switchNviType));
	}

	private void switchNviType(SelectionEvent e) {
		nviType.setValue(btnNviLegacy.getSelection() ? ContentType.NVI_NETCDF_4 : ContentType.NVI_V2_NETCDF_4);
	}

}
