package fr.ifremer.globe.editor.navigation.wizard;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class SaveToNviWizardModel extends ConvertWizardDefaultModel {

	/** Map of input files and data proxy */
	private Map<File, INavigationData> inputs = new LinkedHashMap<>();

	private final WritableObject<ContentType> nviType = new WritableObject<>(ContentType.NVI_NETCDF_4);

	/**
	 * Constructor
	 */
	public SaveToNviWizardModel() {
		outputFormatExtensions.add(IFileService.grab().getExtensions(ContentType.NVI_NETCDF_4).get(0));
		nviType.addChangeListener(
				e -> outputFormatExtensions.set(0, IFileService.grab().getExtensions(nviType.getValue()).get(0)));
	}

	/**
	 * Adds an input file and its {@link INavigationData} to the wizard model
	 */
	public void addInput(File file, INavigationData navDataProxy) {
		inputs.put(file, navDataProxy);
		inputFiles.add(file);
	}

	// GETTERS

	public Map<File, INavigationData> getInputs() {
		return inputs;
	}

	/**
	 * @return the {@link #nviType}
	 */
	public WritableObject<ContentType> getNviType() {
		return nviType;
	}

}