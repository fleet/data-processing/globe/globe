package fr.ifremer.globe.editor.navigation;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
	@Override
	public void start(BundleContext context) throws Exception {
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
	}
}
