package fr.ifremer.globe.placa3d.parametersview;

import fr.ifremer.globe.ui.events.BaseEvent;
import fr.ifremer.viewer3d.layers.ContrastShadeModel;

/**
 * Event raised when a {@link ContrastShadeModel} is updated.
 */
public class ParametersViewEvent implements BaseEvent {

	public ParametersViewEvent() {
		super();
	}
}
