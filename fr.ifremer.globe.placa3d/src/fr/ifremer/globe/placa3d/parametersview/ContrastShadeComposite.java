package fr.ifremer.globe.placa3d.parametersview;

import java.beans.PropertyChangeListener;
import java.util.List;

import org.apache.commons.lang3.event.EventListenerSupport;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.ui.widget.color.ColorComposite;
import fr.ifremer.viewer3d.layers.ContrastShadeModel;
import fr.ifremer.viewer3d.layers.xml.parametersview.ApplyAllComposite;
import fr.ifremer.viewer3d.layers.xml.parametersview.AzimuthShadeComposite;
import fr.ifremer.viewer3d.layers.xml.parametersview.ZenithShadeComposite;

/**
 * Composite that changes contrast and shading of ShaderElevationLayer.
 */
public class ContrastShadeComposite extends ApplyAllComposite {

	private static final String reflectivity = "Reflectivity";

	private ContrastShadeModel model;

	// SWT Widget
	protected Button applyAllThresholdButton;
	protected Button applyAllContrastButton;

	@Deprecated // replaced by rasterInfoCombo
	private Combo dataCombo;
	private ComboViewer rasterInfoCombo;
	private Spinner minSpinner;
	private Spinner maxSpinner;
	private Spinner windowSlider;
	private Label sliderLabel;
	private Spinner minTransparencySpinner;
	private Spinner maxTransparencySpinner;
	private Button minMaxButton;
	private Button rehauss5Button;
	private Button rehauss1Button;
	private Button customButton;
	private ColorComposite colorComposite;
	private Button shadeButton;
	private AzimuthShadeComposite azimuthShadeComposite;
	private ZenithShadeComposite zenithShadeComposite;
	private Button shadeLogButton;
	private Button gradientButton;
	private Scale shadeScale;
	private Composite shadeComposite;
	private Button hideAllLayersButton;
	private Label typeLabel;

	private EventListenerSupport<ContrastShadeCompositeListener> listeners;

	private PropertyChangeListener colorCompositeListener;

	private boolean dependsOfColorProfileDialog;

	public ContrastShadeComposite(Composite parent, ContrastShadeModel model, boolean notify,
			boolean dependsOfColorProfileDialog) {

		super(parent, SWT.NONE);

		this.model = model;
		listeners = new EventListenerSupport<>(ContrastShadeCompositeListener.class, getClass().getClassLoader());
		this.dependsOfColorProfileDialog = dependsOfColorProfileDialog;

		initializeComposite(this.dependsOfColorProfileDialog);
		addDisposeListener(e -> onDispose());
	}

	/** {@inheritDoc} */
	protected void onDispose() {
		colorComposite.removePropertyChangeListener(colorCompositeListener);
		ContrastShadeCompositeListener[] listeners = this.listeners.getListeners();
		for (ContrastShadeCompositeListener l : listeners) {
			this.listeners.removeListener(l);
		}
	}

	public void addContrastShadeCompositeListener(ContrastShadeCompositeListener listener) {
		listeners.addListener(listener);
	}

	public void removeContrastShadeCompositeListener(ContrastShadeCompositeListener listener) {
		listeners.removeListener(listener);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void initializeComposite(boolean dependsOkColorProfileDialog) {
		setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite mainComposite = new Composite(this, SWT.NONE);
		mainComposite.setLayout(new GridLayout(1, false));

		if (!dependsOkColorProfileDialog) {
			initializeDataComponents(mainComposite);
		}
		initializeTransparencyComponents(mainComposite);
		initializeContrastComponents(mainComposite);
		initializeColorComponents(mainComposite);
		initializeShadeComponents(mainComposite);
		if (!dependsOfColorProfileDialog) {
			initializeAllLayersComponents(mainComposite);
		}

		updateSpinnersAndLabel();
	}

	/**
	 * Initialize all the graphic components about data management.
	 *
	 * @param mainComposite parent composite of the components to initialize
	 */
	private void initializeDataComponents(Composite mainComposite) {
		List<RasterInfo> rasterInfos = model.getRasterInfos();
		if (rasterInfos.isEmpty()) {
			initializeDataCombo(mainComposite);
		} else {
			initializeRasterComponents(mainComposite, rasterInfos);
		}
	}

	/**
	 * Initialize rasterInfoCombo.
	 */
	private void initializeRasterComponents(Composite mainComposite, List<RasterInfo> rasterInfos) {

		rasterInfoCombo = new ComboViewer(mainComposite, SWT.READ_ONLY);
		rasterInfoCombo.setContentProvider(ArrayContentProvider.getInstance());
		rasterInfoCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				RasterInfo rasterInfo = (RasterInfo) element;
				return rasterInfo.getDataType();
			}
		});
		rasterInfoCombo.setInput(rasterInfos);
		rasterInfoCombo.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		rasterInfoCombo.setSelection(new StructuredSelection(rasterInfos.get(0)));

		String dataType = model.getCurrentLayer().getType();
		RasterInfo editedRasterInfo = rasterInfos.stream().filter(info -> info.getDataType().equals(dataType))
				.findFirst().orElse(rasterInfos.get(0));
		rasterInfoCombo.setSelection(new StructuredSelection(editedRasterInfo));

		// Trick to know if Raster comes from an Editor or not...
		if (editedRasterInfo.getContentType() == ContentType.UNDEFINED) {
			// React to the change of Layer when this Composite is managing a layer of an editor (ie Swath)
			rasterInfoCombo.getCombo().addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
				RasterInfo selectedRasterInfo = (RasterInfo) rasterInfoCombo.getStructuredSelection().getFirstElement();
				// Notify to trigger the change process
				listeners.fire().rasterInfoChanged(selectedRasterInfo);
			}));
		} else {
			// React to the change of Layer when this Composite is not managing a layer of an editor
			rasterInfoCombo.getCombo().addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
				RasterInfo selectedRasterInfo = (RasterInfo) rasterInfoCombo.getStructuredSelection().getFirstElement();
				if (selectedRasterInfo != editedRasterInfo) {
					// Notify to trigger the change process
					listeners.fire().rasterInfoChanged(selectedRasterInfo);

					if (editedRasterInfo.getContentType() != ContentType.UNDEFINED) {
						// In case of DTM, this Composite is dedicated to a layer.
						// The Combo is used to swith to an other layer (with its owm Composite too)
						// So we have to rollBack the selected RasterInfo to the original one
						rasterInfoCombo.getCombo().getDisplay().asyncExec(
								() -> rasterInfoCombo.setSelection(new StructuredSelection(editedRasterInfo)));
					}
				}
			}));
		}
	}

	/**
	 * Initialize all the graphic components about data management.
	 *
	 * @param mainComposite parent composite of the components to initialize
	 */
	@Deprecated // Better to use RasterInfo
	private void initializeDataCombo(Composite mainComposite) {

		dataCombo = new Combo(mainComposite, SWT.READ_ONLY);
		dataCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		for (String data : model.getData()) {
			dataCombo.add(data);
		}

		String type = model.getCurrentLayer().getType();
		dataCombo.select(model.getData().indexOf(type));
		dataCombo.addListener(SWT.Selection, event -> {
			if (!type.equals(dataCombo.getText())) {
				listeners.fire().dataTypeChanged(dataCombo.getText());
				updateSpinnersAndLabel(); // called to keep the suitable layer selected within the combo box
			}
		});
	}

	/**
	 * Initialize all the graphic components about transparency management.
	 *
	 * @param mainComposite parent composite of the components to initialize
	 */
	private void initializeTransparencyComponents(Composite mainComposite) {

		Group transparencyGroup = new Group(mainComposite, SWT.NONE);
		transparencyGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		transparencyGroup.setText("Filtering Threshold");
		transparencyGroup.setLayout(new GridLayout(2, true));
		minTransparencySpinner = new Spinner(transparencyGroup, SWT.NONE);
		minTransparencySpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		maxTransparencySpinner = new Spinner(transparencyGroup, SWT.NONE);
		maxTransparencySpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		minTransparencySpinner.setDigits(2); // allow 2 decimal places
		minTransparencySpinner.setEnabled(true);
		minTransparencySpinner.addListener(SWT.Selection,
				event -> listeners.fire().minTransparencyChanged(minTransparencySpinner.getSelection() / 100.));

		maxTransparencySpinner.setDigits(2); // allow 2 decimal places
		maxTransparencySpinner.setEnabled(true);
		maxTransparencySpinner.addListener(SWT.Selection,
				event -> listeners.fire().maxTransparencyChanged(maxTransparencySpinner.getSelection() / 100.));

		if (!dependsOfColorProfileDialog) {
			// aply for all layer with same type
			applyAllThresholdButton = new Button(transparencyGroup, SWT.CHECK);
			applyAllThresholdButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			applyAllThresholdButton
					.setText("Apply Thresholds for all " + model.getCurrentLayer().getType() + " layers");
			applyAllThresholdButton.addListener(SWT.Selection,
					event -> listeners.fire().synchronizeAllThresholds(applyAllThresholdButton.getSelection()));
		}
	}

	/**
	 * Initialize all the graphic components about contrast management.
	 *
	 * @param mainComposite parent composite of the components to initialize
	 */
	private void initializeContrastComponents(Composite mainComposite) {

		Group contrastGroup = new Group(mainComposite, SWT.NONE);
		contrastGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		contrastGroup.setText("Contrast");
		contrastGroup.setLayout(new GridLayout(5, false));

		String unit = model.getCurrentLayer().getUnit();
		String type = model.getCurrentLayer().getType();

		typeLabel = new Label(contrastGroup, SWT.NONE);
		typeLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		typeLabel.setText(type
				+ (type == null || type.length() == 0 || unit == null || unit.length() == 0 ? "" : " (" + unit + ")"));

		minSpinner = new Spinner(contrastGroup, SWT.BORDER);
		minSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		minSpinner.setDigits(2); // allow 2 decimal places
		minSpinner.setEnabled(model.getContrastLevel() == 3);
		minSpinner.addListener(SWT.Selection,
				event -> listeners.fire().minContrastChanged(minSpinner.getSelection() / 100.));

		maxSpinner = new Spinner(contrastGroup, SWT.BORDER);
		maxSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		maxSpinner.setDigits(2); // allow 2 decimal places
		maxSpinner.setEnabled(model.getContrastLevel() == 3);
		maxSpinner.addListener(SWT.Selection,
				event -> listeners.fire().maxContrastChanged(maxSpinner.getSelection() / 100.));

		sliderLabel = new Label(contrastGroup, SWT.NONE);
		sliderLabel.setText("Spin Both :");
		sliderLabel.setEnabled(false);
		windowSlider = new Spinner(contrastGroup, SWT.READ_ONLY);
		windowSlider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// we try to hide value
		windowSlider.setForeground(windowSlider.getBackground());
		windowSlider.setEnabled(false);
		windowSlider.setMinimum(-1);
		windowSlider.addListener(SWT.Selection, event -> {
			int minSpinnerValue = 0;
			int maxSpinnerValue = 0;

			if (windowSlider.getSelection() > 0) {
				minSpinnerValue = minSpinner.getSelection() + minSpinner.getIncrement();
				maxSpinnerValue = maxSpinner.getSelection() + maxSpinner.getIncrement();
			} else if (windowSlider.getSelection() < 0) {
				minSpinnerValue = minSpinner.getSelection() - minSpinner.getIncrement();
				maxSpinnerValue = maxSpinner.getSelection() - maxSpinner.getIncrement();
			}
			windowSlider.setSelection(0);

			// we update Contrast for both spinners and notify listeners
			minSpinner.setSelection(minSpinnerValue);
			model.setMinContrast(minSpinner.getSelection() / 100.);
			listeners.fire().minContrastChanged(minSpinner.getSelection() / 100d);

			maxSpinner.setSelection(maxSpinnerValue);
			model.setMaxContrast(maxSpinner.getSelection() / 100.);
			listeners.fire().maxContrastChanged(maxSpinner.getSelection() / 100d);
		});

		minMaxButton = new Button(contrastGroup, SWT.RADIO);
		minMaxButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		minMaxButton.setText("Min / Max");
		minMaxButton.setEnabled(true);
		minMaxButton.setSelection(model.getContrastLevel() == 0);
		minMaxButton.addListener(SWT.Selection, event -> {
			if (minMaxButton.getSelection()) {
				minSpinner.setEnabled(false);
				maxSpinner.setEnabled(false);
				listeners.fire().applyContrastMinMax();
			}
		});
		rehauss5Button = new Button(contrastGroup, SWT.RADIO);
		rehauss5Button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		rehauss5Button.setText("0.5%");
		rehauss5Button.setEnabled(true);
		rehauss5Button.setSelection(model.getContrastLevel() == 1);
		rehauss5Button.addListener(SWT.Selection, event -> {
			if (rehauss5Button.getSelection()) {
				minSpinner.setEnabled(false);
				maxSpinner.setEnabled(false);
				listeners.fire().applyContrast05Percent();
			}
		});
		rehauss1Button = new Button(contrastGroup, SWT.RADIO);
		rehauss1Button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		rehauss1Button.setText("1%");
		rehauss1Button.setEnabled(true);
		rehauss1Button.setSelection(model.getContrastLevel() == 2);
		rehauss1Button.addListener(SWT.Selection, event -> {
			if (rehauss1Button.getSelection()) {
				minSpinner.setEnabled(false);
				maxSpinner.setEnabled(false);
				listeners.fire().applyContrast1Percent();
			}
		});
		customButton = new Button(contrastGroup, SWT.RADIO);
		customButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		customButton.setText("Custom");
		customButton.setEnabled(true);
		customButton.setSelection(model.getContrastLevel() == 3);
		customButton.addListener(SWT.Selection, event -> {
			if (customButton.getSelection()) {
				minSpinner.setEnabled(true);
				maxSpinner.setEnabled(true);
				listeners.fire().applyContrastCustom();
			}
		});
	}

	/**
	 * Initialize all the graphic components about color management.
	 *
	 * @param mainComposite parent composite of the components to initialize
	 */
	private void initializeColorComponents(Composite mainComposite) {

		colorComposite = new ColorComposite(mainComposite, SWT.NONE, model.isUseColorMap(), model.getColormap(),
				model.isInverseColorMap());
		colorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		colorCompositeListener = event -> {
			String evtName = event.getPropertyName();
			if (evtName.equals(ColorComposite.PROPERTY_COLORMAP_UPDATE)) {
				listeners.fire().colorMapChanged((int) event.getNewValue());
			} else if (evtName.equals(ColorComposite.PROPERTY_INVERT_UPDATE)) {
				listeners.fire().applyInverseColorMap((boolean) event.getNewValue());
			}
		};

		colorComposite.addPropertyChangeListener(colorCompositeListener);
	}

	/**
	 * Initialize all the graphic components about shade management.
	 *
	 * @param mainComposite parent composite of the components to initialize
	 */
	private void initializeShadeComponents(Composite mainComposite) {

		Group shadeGroup = new Group(mainComposite, SWT.NONE);
		shadeGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		shadeGroup.setText("Shading");
		shadeGroup.setLayout(new GridLayout(2, false));

		shadeComposite = new Composite(shadeGroup, SWT.BORDER | SWT.NONE);
		shadeComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 3));
		shadeComposite.setLayout(new GridLayout(2, true));

		Label azimuthLabel = new Label(shadeComposite, SWT.NONE);
		azimuthLabel.setText("Azimuth");
		Label zenithLabel = new Label(shadeComposite, SWT.NONE);
		zenithLabel.setText("Zenith");

		azimuthShadeComposite = new AzimuthShadeComposite(shadeComposite, SWT.BORDER | SWT.EMBEDDED,
				() -> model.getCurrentLayer());
		azimuthShadeComposite.setAzimuth(Math.toRadians(model.getAzimuth()), true);
		azimuthShadeComposite.setEnabled(model.isUseOmbrage());

		zenithShadeComposite = new ZenithShadeComposite(shadeComposite, SWT.BORDER | SWT.EMBEDDED,
				() -> model.getCurrentLayer());
		zenithShadeComposite.setZenith(Math.toRadians(model.getZenith()), true);
		zenithShadeComposite.setEnabled(model.isUseOmbrage());

		Label exaggerationLabel = new Label(shadeComposite, SWT.NONE);
		exaggerationLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		exaggerationLabel.setText("Exaggeration (log10)");

		// Labels for ticks for exaggeration scale
		Composite tickComposite = new Composite(shadeComposite, SWT.NONE);

		if (model.getCurrentLayer().getType().equalsIgnoreCase(reflectivity)) {
			tickComposite.setLayout(new GridLayout(7, true)); // scale -2...4
		} else {
			tickComposite.setLayout(new GridLayout(6, true)); // scale -1...4
		}
		tickComposite.setLayout(new GridLayout(8, false));
		tickComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		if (model.getCurrentLayer().getType().equalsIgnoreCase(reflectivity)) {
			final Label valueN2Label = new Label(tickComposite, SWT.NONE);
			valueN2Label.setText("-2");
		}

		model.getCurrentLayer().getType().equalsIgnoreCase(reflectivity);

		shadeButton = new Button(shadeGroup, SWT.CHECK);
		shadeButton.setText("Shading");
		shadeButton.setSelection(model.isUseOmbrage());
		shadeButton.addListener(SWT.Selection, event -> {
			boolean useShade = shadeButton.getSelection();
			azimuthShadeComposite.setEnabled(useShade);
			zenithShadeComposite.setEnabled(useShade);
			shadeScale.setEnabled(useShade);
			shadeLogButton.setEnabled(useShade);
			gradientButton.setEnabled(useShade);
			if (!useShade) {
				gradientButton.setSelection(false);
			}
			listeners.fire().applyShading(shadeButton.getSelection());
		});

		final Label valueN1Label = new Label(tickComposite, SWT.NONE);
		valueN1Label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		valueN1Label.setText("-1");

		final Label value0Label = new Label(tickComposite, SWT.NONE);
		value0Label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		value0Label.setText("0");
		final Label value1Label = new Label(tickComposite, SWT.NONE);
		value1Label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		value1Label.setText("1");
		final Label value2Label = new Label(tickComposite, SWT.NONE);
		value2Label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		value2Label.setText("2");
		final Label value3Label = new Label(tickComposite, SWT.NONE);
		value3Label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		value3Label.setText("3");
		final Label value4Label = new Label(tickComposite, SWT.NONE);
		value4Label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		value4Label.setText("4");
		final Label value5Label = new Label(tickComposite, SWT.NONE);
		value5Label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		value5Label.setText("5");

		shadeScale = new Scale(shadeComposite, SWT.NONE);
		shadeScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		if (model.getCurrentLayer().getType().equalsIgnoreCase(reflectivity)) {
			shadeScale.setMinimum(0); // 10^-2
		} else {
			shadeScale.setMinimum(10); // 10^-1
		}
		shadeScale.setMaximum(70); // 10^5
		shadeScale.setSelection((int) (Math.log10(model.getOmbrageExaggeration()) * 10 + 20));
		shadeScale.setEnabled(shadeButton.getSelection());
		shadeScale.setPageIncrement(10);

		shadeLogButton = new Button(shadeGroup, SWT.CHECK);
		shadeLogButton.setText("Log Shade");
		shadeLogButton.setEnabled(model.isUseOmbrage());
		shadeLogButton.setSelection(model.isUseOmbrageLogarithmique());
		shadeLogButton.addListener(SWT.Selection,
				event -> listeners.fire().setUseLogScaleForShading(shadeLogButton.getSelection()));

		gradientButton = new Button(shadeGroup, SWT.CHECK);
		gradientButton.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		gradientButton.setText("Gradient");
		gradientButton.setSelection(model.isUseGradient());
		gradientButton.setEnabled(model.isUseOmbrage());
		gradientButton.addListener(SWT.Selection,
				event -> listeners.fire().setUseGradient(gradientButton.getSelection()));
		shadeScale.addListener(SWT.Selection,
				event -> listeners.fire().applyOmbrageExaggeration(shadeScale.getSelection()));
	}

	/**
	 * Initialize all the graphic components about filter management.
	 *
	 * @param mainComposite parent composite of the components to initialize
	 */
	private void initializeAllLayersComponents(Composite mainComposite) {

		Group allLayersGroup = new Group(mainComposite, SWT.NONE);
		allLayersGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		allLayersGroup.setText("Apply configuration for all layers");
		allLayersGroup.setLayout(new GridLayout(2, false));

		allLayersButton = new Button(allLayersGroup, SWT.CHECK);
		allLayersButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		allLayersButton.setText("Apply for all " + model.getCurrentLayer().getType() + " Layers");
		allLayersButton.addListener(SWT.Selection,
				event -> listeners.fire().synchronizeAllContrasts(allLayersButton.getSelection()));

		hideAllLayersButton = new Button(allLayersGroup, SWT.PUSH);
		hideAllLayersButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		hideAllLayersButton.setText("Display/Hide all Terrain Layers");
		hideAllLayersButton.addListener(SWT.Selection, event -> listeners.fire().displayHideAllTerrains());
	}

	/***
	 * updates IHM after contrast
	 */
	public void refreshCompositeAfterContrast() {
		minSpinner.setEnabled(false);
		maxSpinner.setEnabled(false);
		windowSlider.setEnabled(false);
		minSpinner.setSelection((int) model.layerValue2AbsoluteValue(model.getMinTextureValue()) * 100);
		maxSpinner.setSelection((int) model.layerValue2AbsoluteValue(model.getMaxTextureValue()) * 100);
	}

	public void updateSpinnersAndLabel() {

		String type = model.getCurrentLayer().getType();
		String unit = model.getCurrentLayer().getUnit();

		typeLabel.setText(type
				+ (type != null && type.length() > 0 && unit != null && unit.length() > 0 ? " (" + unit + ")" : ""));

		minTransparencySpinner.setMinimum(Integer.MIN_VALUE);
		minTransparencySpinner.setMaximum(Integer.MAX_VALUE);
		minTransparencySpinner.setSelection((int) (model.getMinTransparency() * 100 - 1));

		maxTransparencySpinner.setMinimum(Integer.MIN_VALUE);
		maxTransparencySpinner.setMaximum(Integer.MAX_VALUE);
		maxTransparencySpinner.setSelection((int) (model.getMaxTransparency() * 100 - 1));

		minSpinner.setMinimum(Integer.MIN_VALUE);
		minSpinner.setMaximum(Integer.MAX_VALUE);
		minSpinner.setSelection((int) (model.getMinContrast() * 100));

		maxSpinner.setMinimum(Integer.MIN_VALUE);
		maxSpinner.setMaximum(Integer.MAX_VALUE);
		maxSpinner.setSelection((int) (model.getMaxContrast() * 100));

		if (dataCombo != null) {
			dataCombo.select(model.getData().indexOf(type));
		}
	}

	public Button getApplyAllThresholdButton() {
		return applyAllThresholdButton;
	}

	public Button getApplyAllContrastButton() {
		return allLayersButton;
	}

	public Spinner getMinSpinner() {
		return minSpinner;
	}

	public Spinner getMaxSpinner() {
		return maxSpinner;
	}

	public Spinner getWindowSlider() {
		return windowSlider;
	}

	public Label getSliderLabel() {
		return sliderLabel;
	}

	public Spinner getMinTransparencySpinner() {
		return minTransparencySpinner;
	}

	public Spinner getMaxTransparencySpinner() {
		return maxTransparencySpinner;
	}

	public Button getMinMaxButton() {
		return minMaxButton;
	}

	public Button getRehauss5Button() {
		return rehauss5Button;
	}

	public Button getRehauss1Button() {
		return rehauss1Button;
	}

	public Button getCustomButton() {
		return customButton;
	}

	public ColorComposite getColorComposite() {
		return colorComposite;
	}

	public Button getShadeButton() {
		return shadeButton;
	}

	public AzimuthShadeComposite getAzimuthShadeComposite() {
		return azimuthShadeComposite;
	}

	public ZenithShadeComposite getZenithShadeComposite() {
		return zenithShadeComposite;
	}

	public Button getShadeLogButton() {
		return shadeLogButton;
	}

	public Button getGradientButton() {
		return gradientButton;
	}

	public Scale getShadeScale() {
		return shadeScale;
	}

	public Composite getShadeComposite() {
		return shadeComposite;
	}

	public Button getHideAllLayersButton() {
		return hideAllLayersButton;
	}

	public Label getTypeLabel() {
		return typeLabel;
	}

	public PropertyChangeListener getColorCompositeListener() {
		return colorCompositeListener;
	}

	/**
	 * @return the model
	 */
	public ContrastShadeModel getModel() {
		return model;
	}

}
