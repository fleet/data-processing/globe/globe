package fr.ifremer.globe.placa3d.parametersview;

import fr.ifremer.globe.core.model.raster.RasterInfo;

public interface ContrastShadeCompositeListener {

	void minTransparencyChanged(double newTransparency);

	void maxTransparencyChanged(double newTransparency);

	void minContrastChanged(double newTextureValue);

	void maxContrastChanged(double newTextureValue);

	void synchronizeAllThresholds(boolean value);

	void synchronizeAllContrasts(boolean value);

	void applyContrastMinMax();

	void applyContrast05Percent();

	void applyContrast1Percent();

	void applyContrastCustom();

	void applyShading(boolean selection);

	void setUseLogScaleForShading(boolean selection);

	void setUseGradient(boolean selection);

	void applyOmbrageExaggeration(int value);

	void displayHideAllTerrains();

	void computeHistogram();

	void colorMapChanged(int newValue);

	void applyInverseColorMap(boolean newValue);

	@Deprecated // Better use rasterInfoChanged()
	void dataTypeChanged(String newDataType);

	/** Called when the specified RasterInfo has been selected */
	void rasterInfoChanged(RasterInfo rasterInfo);
}
