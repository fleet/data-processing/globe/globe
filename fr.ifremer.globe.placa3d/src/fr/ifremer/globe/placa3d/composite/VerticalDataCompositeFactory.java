package fr.ifremer.globe.placa3d.composite;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.ReflectivityLayer;

/***
 * creates a vertical data parameters Composite.
 */
@Component(name = "globe_placa_vertical_data_composite_factory", service = IParametersViewCompositeFactory.class)
public class VerticalDataCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof ReflectivityLayer
				? Optional.of(new VerticalDataComposite(parent, (MyAbstractLayer) selection))
				: Optional.empty();
	}

}
