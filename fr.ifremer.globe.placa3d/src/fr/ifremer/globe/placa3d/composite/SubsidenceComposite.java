/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.composite;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.nebula.widgets.opal.checkboxgroup.CheckBoxGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableNumber;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.widget.FileComposite;
import fr.ifremer.globe.ui.widget.StackLayout;

/** Composite for editing the subsidence parameters */
public class SubsidenceComposite extends CheckBoxGroup {
	protected SubsidenceModel subsidenceModel;

	/** SWT widgets */
	protected Button subsidenceAutoRadioButton;
	protected Button subsidenceManualRadioButton;
	protected StackLayout subsidenceStackLayout;
	protected Composite subsidenceEmptyComposite;
	protected Composite subsidenceAutoComposite;
	protected Composite subsidenceManualComposite;
	protected Button subsidenceFileRadioButton;
	protected Button subsidenceConstantRadioButton;
	protected Spinner currentOffsetSpinner;
	protected FileComposite offsetfileComposite;

	/** View/model bindings */
	protected DataBindingContext dataBindingContext;

	/** Utility instance to observe model changes. */
	protected ObservableSubsidenceModel modelObservable;

	/**
	 * Constructor used by WindowBuilder
	 * 
	 * @wbp.parser.constructor
	 */
	public SubsidenceComposite(Composite parent, int style) {
		super(parent, style);
		setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_Subsidence);
		createContent(getContent());
	}

	/**
	 * Create the composite.
	 */
	public SubsidenceComposite(Composite parent, int style, SubsidenceModel subsidenceModel) {
		super(parent, style);
		this.subsidenceModel = subsidenceModel;
		if (this.subsidenceModel != null) {
			modelObservable = new ObservableSubsidenceModel(subsidenceModel);
		}
		setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_Subsidence);
		createContent(getContent());
	}

	/**
	 * Create the player group of this composite.
	 */
	protected void createContent(Composite parent) {

		addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				subsidenceModel.getOffsetMode().set(isActivated() ? OffsetMode.AUTO : OffsetMode.OFF);
			}
		});

		parent.setLayout(new GridLayout(2, false));
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		subsidenceAutoRadioButton = new Button(parent, SWT.RADIO);
		subsidenceAutoRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (subsidenceAutoRadioButton.getSelection())
					subsidenceModel.getOffsetMode().set(OffsetMode.AUTO);
			}
		});
		subsidenceAutoRadioButton.setSelection(true);
		subsidenceAutoRadioButton.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_Automatic);

		subsidenceManualRadioButton = new Button(parent, SWT.RADIO);
		subsidenceManualRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (subsidenceManualRadioButton.getSelection())
					subsidenceModel.getOffsetMode().set(OffsetMode.MANUAL_FILE);
			}
		});
		subsidenceManualRadioButton.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_Manual);

		Composite autoManualComposite = new Composite(parent, SWT.NONE);
		autoManualComposite.setLayout(new GridLayout(1, false));
		autoManualComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 2, 1));

		Composite subsidenceStackComposite = new Composite(autoManualComposite, SWT.NONE);
		subsidenceStackComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		subsidenceStackComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		subsidenceStackLayout = new StackLayout();
		subsidenceStackComposite.setLayout(subsidenceStackLayout);

		subsidenceEmptyComposite = new Composite(subsidenceStackComposite, SWT.NONE);

		subsidenceAutoComposite = new Composite(subsidenceStackComposite, SWT.NONE);
		subsidenceAutoComposite.setLayout(new GridLayout(1, false));

		Label parsonSclaterLawLabel = new Label(subsidenceAutoComposite, SWT.NONE);
		parsonSclaterLawLabel.setLayoutData(new GridData(SWT.CENTER, SWT.BOTTOM, true, true, 1, 1));
		parsonSclaterLawLabel.setAlignment(SWT.CENTER);
		parsonSclaterLawLabel.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_AutomaticLaw);

		Label parsonSclaterLawFormula = new Label(subsidenceAutoComposite, SWT.NONE);
		parsonSclaterLawFormula.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, true, 1, 1));
		parsonSclaterLawFormula.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.placa3d", "icons/law.png"));

		subsidenceManualComposite = new Composite(subsidenceStackComposite, SWT.NONE);
		subsidenceManualComposite.setLayout(new GridLayout(1, false));

		Group subsidenceManualGroup = new Group(subsidenceManualComposite, SWT.NONE);
		subsidenceManualGroup.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_subsidenceConstant);
		subsidenceManualGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		subsidenceManualGroup.setLayout(new GridLayout(3, false));

		subsidenceFileRadioButton = new Button(subsidenceManualGroup, SWT.RADIO);
		subsidenceFileRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (subsidenceFileRadioButton.getSelection())
					subsidenceModel.getOffsetMode().set(OffsetMode.MANUAL_FILE);
			}
		});
		subsidenceFileRadioButton.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_File);

		offsetfileComposite = new FileComposite(subsidenceManualGroup, SWT.NONE,
				subsidenceModel != null ? subsidenceModel.getOffsetFile() : null,
				FileValidator.EXISTS | FileValidator.FILE);
		offsetfileComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		subsidenceConstantRadioButton = new Button(subsidenceManualGroup, SWT.RADIO);
		subsidenceConstantRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (subsidenceConstantRadioButton.getSelection())
					subsidenceModel.getOffsetMode().set(OffsetMode.MANUAL_VALUE);
			}
		});
		subsidenceConstantRadioButton.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_Constant);

		currentOffsetSpinner = new Spinner(subsidenceManualGroup, SWT.BORDER);
		GridData gdCurrentOffsetSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdCurrentOffsetSpinner.widthHint = 30;
		currentOffsetSpinner.setLayoutData(gdCurrentOffsetSpinner);
		currentOffsetSpinner.setMaximum(10000);

		Label lblM = new Label(subsidenceManualGroup, SWT.NONE);
		lblM.setText("m");

		// Bindings View/Model
		if (subsidenceModel != null) {
			dataBindingContext = initDataBindings();
			initExtraBindings();

			// Update view from model
			dataBindingContext.updateTargets();
			offsetModeUpdated();
		}
	}

	/** Initialize a specific bindings (not managed by Windows Builder) */
	protected void initExtraBindings() {
		subsidenceModel.getOffsetMode().addValueChangeListener(event -> offsetModeUpdated());
		subsidenceModel.getConstantOffset().addValueChangeListener(event -> constantOffsetUpdated());
		subsidenceModel.getOffsetFile().addValueChangeListener(event -> offsetFileUpdated());
	}

	/**
	 * Notify Observers that file has changed
	 */
	protected void offsetFileUpdated() {
		if (subsidenceModel.getOffsetMode().get() == OffsetMode.MANUAL_FILE
				&& subsidenceModel.getOffsetFile().exists()) {
			modelObservable.notifyObservers();
		}
	}

	/**
	 * Notify Observers that manual offset has changed
	 */
	protected void constantOffsetUpdated() {
		if (subsidenceModel.getOffsetMode().get() == OffsetMode.MANUAL_VALUE) {
			modelObservable.notifyObservers();
		}
	}

	/**
	 * Managed new OffsetMode
	 */
	protected void offsetModeUpdated() {
		OffsetMode offsetMode = subsidenceModel.getOffsetMode().get();
		if (!isActivated() && offsetMode != OffsetMode.OFF) {
			activate();
		}

		switch (offsetMode) {
		case AUTO:
			subsidenceAutoRadioButton.setSelection(true);
			subsidenceManualRadioButton.setSelection(false);
			subsidenceFileRadioButton.setSelection(false);
			subsidenceConstantRadioButton.setSelection(false);
			subsidenceStackLayout.setTopControl(subsidenceAutoComposite);
			modelObservable.notifyObservers();
			break;
		case MANUAL_VALUE:
			subsidenceAutoRadioButton.setSelection(false);
			subsidenceManualRadioButton.setSelection(true);
			subsidenceFileRadioButton.setSelection(false);
			subsidenceConstantRadioButton.setSelection(true);
			subsidenceStackLayout.setTopControl(subsidenceManualComposite);
			offsetfileComposite.setEnabled(false);
			currentOffsetSpinner.setEnabled(true);
			constantOffsetUpdated();
			break;
		case MANUAL_FILE:
			subsidenceAutoRadioButton.setSelection(false);
			subsidenceManualRadioButton.setSelection(true);
			subsidenceFileRadioButton.setSelection(true);
			subsidenceConstantRadioButton.setSelection(false);
			subsidenceStackLayout.setTopControl(subsidenceManualComposite);
			offsetfileComposite.setEnabled(true);
			currentOffsetSpinner.setEnabled(false);
			offsetFileUpdated();
			break;
		default:
			if (isActivated()) {
				deactivate();
			}
			modelObservable.notifyObservers();
		}

	}

	/**
	 * @see java.util.Observable#addObserver(java.util.Observer)
	 */
	public void addSubsidenceModelObserver(Observer o) {
		modelObservable.addObserver(o);
	}

	/**
	 * @see java.util.Observable#deleteObserver(java.util.Observer)
	 */
	public void deleteSubsidenceModelObserver(Observer o) {
		modelObservable.deleteObserver(o);
	}

	/**
	 * @see org.eclipse.swt.widgets.Composite#checkSubclass()
	 */
	@Override
	protected void checkSubclass() { // Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Model of subsidence parameters
	 */
	public interface SubsidenceModel {

		/**
		 * Getter of OffsetMode
		 */
		WritableObject<OffsetMode> getOffsetMode();

		/**
		 * Getter of OffsetFile
		 */
		WritableFile getOffsetFile();

		/**
		 * Getter of ConstantOffset
		 */
		WritableNumber getConstantOffset();
	}

	/**
	 * Observable of Model
	 */
	public static class ObservableSubsidenceModel extends Observable {
		protected SubsidenceModel subsidenceModel;

		/**
		 * Constructor
		 */
		public ObservableSubsidenceModel(SubsidenceModel subsidenceModel) {
			this.subsidenceModel = subsidenceModel;
		}

		/**
		 * @see java.util.Observable#notifyObservers(java.lang.Object)
		 */
		@Override
		public void notifyObservers() {
			setChanged();
			super.notifyObservers(subsidenceModel);
		}

	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		var observeSelectionCurrentOffsetSpinnerObserveWidget = WidgetProperties.spinnerSelection()
				.observe(currentOffsetSpinner);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		bindingContext.bindValue(observeSelectionCurrentOffsetSpinnerObserveWidget, subsidenceModel.getConstantOffset(),
				targetToModelStrategy, null);
		//
		return bindingContext;
	}

	/**
	 * Getter of validationStatus of Offsetfile
	 */
	public WritableBoolean getOffsetfileValidationStatus() {
		return offsetfileComposite.getValidationStatus();
	}

}
