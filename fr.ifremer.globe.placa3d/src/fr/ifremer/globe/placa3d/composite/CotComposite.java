package fr.ifremer.globe.placa3d.composite;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.layers.CotLayer;
import fr.ifremer.globe.placa3d.render.cot.CotSegmentRenderer;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.deprecated.CotWriter;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.render.Renderable;

public class CotComposite extends Composite implements MouseListener {

	protected Logger logger = LoggerFactory.getLogger(CotComposite.class);

	/**
	 * Type of pressed button waiting a click of mouse on globe :
	 *
	 * 0 : start 1 : add 2 : insert 3 : delete 4 : move 5 : end
	 *
	 */
	private int pointType;

	/** The cot layer. */
	private CotLayer layer;
	/** The line for cut the cot. */
	protected Polyline line;

	/** The list of pointPlacemark. */
	List<PointPlacemark> pPList;

	boolean isStartSplit = false;

	boolean hasStartPoint = false;
	boolean hasEndPoint = false;
	boolean hasMovedPoint = false;
	int indexOfMovedPoint;

	/** Show the message when a button is selected. */
	boolean showMessage = true;

	/** Cot Format. */
	public final static String[] FORMAT_NAMES = new String[] { "Cot File" };
	public final static String[] FORMAT_EXTENSIONS = new String[] { "*.cot" };

	public CotComposite(Composite parent, int style, final CotLayer layer) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		this.layer = layer;

		final Button startStopSplitButton = new Button(this, SWT.NONE);
		startStopSplitButton.setText("Start Split");

		final Group splitCotGroup = new Group(this, SWT.NONE);
		splitCotGroup.setText("Split cot");
		splitCotGroup.setLayout(new GridLayout(2, true));
		splitCotGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		splitCotGroup.setEnabled(false);
		new Label(this, SWT.NONE);

		final Button showMessageCheckButton = new Button(splitCotGroup, SWT.CHECK);
		showMessageCheckButton.setToolTipText("Show message when a button is selected");
		showMessageCheckButton.setSelection(true);
		showMessageCheckButton.setEnabled(false);
		new Label(splitCotGroup, SWT.NONE);

		Button startButton = new Button(splitCotGroup, SWT.NONE);
		startButton.setText("Start point");
		startButton.setEnabled(false);

		Button endButton = new Button(splitCotGroup, SWT.NONE);
		endButton.setText("End point");
		endButton.setEnabled(false);

		final Button addButton = new Button(splitCotGroup, SWT.RADIO);
		addButton.setText("Add point");
		addButton.setSelection(true);
		addButton.setEnabled(false);
		new Label(splitCotGroup, SWT.NONE);

		final Button insertButton = new Button(splitCotGroup, SWT.RADIO);
		insertButton.setText("Insert point");
		insertButton.setEnabled(false);
		new Label(splitCotGroup, SWT.NONE);

		final Button deleteButton = new Button(splitCotGroup, SWT.RADIO);
		deleteButton.setText("Delete point");
		deleteButton.setEnabled(false);
		new Label(splitCotGroup, SWT.NONE);

		final Button moveButton = new Button(splitCotGroup, SWT.RADIO);
		moveButton.setText("Move point");
		moveButton.setEnabled(false);
		new Label(splitCotGroup, SWT.NONE);

		Button saveButton = new Button(splitCotGroup, SWT.NONE);
		saveButton.setText("Save");
		saveButton.setEnabled(false);

		startStopSplitButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (!isStartSplit) {
					isStartSplit = true;

					// Add the polyline to layer
					line = new Polyline();
					line.setFollowTerrain(true);
					line.setPositions(new ArrayList<Position>(2));
					line.setColor(Color.yellow);
					layer.addRenderable(line);

					pPList = new ArrayList<PointPlacemark>();

					// Add mouse listener to select a position on globe with mousePressed()
					Viewer3D.getWwd().getInputHandler().addMouseListener(CotComposite.this);
					// Set the cursor in crosshair mod
					((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

					// Set text of button
					startStopSplitButton.setText("Stop Split");

					// Reset checkbox
					addButton.setSelection(true);
					insertButton.setSelection(false);
					deleteButton.setSelection(false);
					moveButton.setSelection(false);
					pointType = 2;

				} else {

					if (MessageDialog.openConfirm(Display.getCurrent().getActiveShell(), "Stop Split",
							"Are you sure you want to quite the split tool?. The split line will be deleted")) {

						isStartSplit = false;

						// Remove the line form the layer
						layer.removeRenderable(line);
						// Remove pointplacemark from layer
						for (PointPlacemark pp : pPList) {
							layer.removeRenderable(pp);
						}

						// Remove mouse listener
						Viewer3D.getWwd().getInputHandler().removeMouseListener(CotComposite.this);
						// Set the cursor in default mod
						((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

						// Reset current boolean
						hasStartPoint = false;
						hasEndPoint = false;
						hasMovedPoint = false;

						// Set text of button
						startStopSplitButton.setText("Start Split");
					}

					// Refresh the globe
					Viewer3D.getWwd().redraw();
				}

				// Enable / Disable the button of splitGroup
				splitCotGroup.setEnabled(isStartSplit);
				for (Control child : splitCotGroup.getChildren()) {
					child.setEnabled(isStartSplit);
				}

			}
		});

		showMessageCheckButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				showMessage = showMessageCheckButton.getSelection();
			}
		});
		showMessageCheckButton.setText("Show message");

		startButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (showMessage) {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Start Point",
							"Right click on the globe to select the start point.");
				}

				// start
				pointType = 0;

				// Reset checkbox
				addButton.setSelection(true);
				insertButton.setSelection(false);
				deleteButton.setSelection(false);
				moveButton.setSelection(false);
			}
		});

		endButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (showMessage) {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "End Point",
							"Right click on the globe to select the end point.");
				}

				// end
				pointType = 5;

				// Reset checkbox
				addButton.setSelection(true);
				insertButton.setSelection(false);
				deleteButton.setSelection(false);
				moveButton.setSelection(false);
			}
		});

		// addButton.addSelectionListener(new SelectionAdapter() {
		// public void widgetSelected(SelectionEvent event) {
		// if (addButton.getSelection()) {
		// if (showMessage) {
		// MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Add Point","Right click on the globe to
		// add a point.");
		// }
		//
		// // add
		// pointType = 1;
		// }
		// }
		// });

		insertButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (insertButton.getSelection()) {
					if (showMessage) {
						MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Insert Point",
								"Right click on the globe to insert a point.");
					}

					// insert
					pointType = 2;
				}
			}
		});

		deleteButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (deleteButton.getSelection()) {
					if (showMessage) {
						MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Delete Point",
								"Right click on the globe to delete a point.");
					}

					// delete
					pointType = 3;
				}
			}
		});

		moveButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (moveButton.getSelection()) {
					if (showMessage) {
						MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Move Point",
								"Right click near the line to select a point, and rigth click again to move this point");
					}

					// move
					pointType = 4;
				}
			}
		});

		saveButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (hasStartPoint && hasEndPoint) {
					// Remove separation yellow line and pp for more visibility
					layer.removeRenderable(line);
					for (PointPlacemark pp : pPList) {
						layer.removeRenderable(pp);
					}

					// Calcul sous ensembles
					// Cherche index du starPoint et du EndPoint dans le cot ainsi que l'index du segment dans le cot
					int indexStartPosInCot = -1;
					int indexEndPosInCot = -1;
					int indexRenderableSegmentInCot = -1;
					int indexRenderableSegmentInCotTmp = 0;
					List<Position> positionList = (List<Position>) line.getPositions();
					for (Renderable r : layer.getRenderables()) {
						if (r instanceof CotSegmentRenderer) {
							CotSegmentRenderer segment = (CotSegmentRenderer) r;
							List<? extends LatLon> positionCotList = (List<? extends LatLon>) segment.getLocations();
							for (int i = 0; i < positionCotList.size(); i++) {
								if (LatLon.equals(positionCotList.get(i), positionList.get(0))) {
									indexStartPosInCot = i;
								}
								if (LatLon.equals(positionCotList.get(i), positionList.get(positionList.size() - 1))) {
									indexEndPosInCot = i;
								}
							}

							if (indexStartPosInCot != -1 && indexEndPosInCot != -1) {
								indexRenderableSegmentInCot = indexRenderableSegmentInCotTmp;
								break;
							}

							indexRenderableSegmentInCotTmp++;
						}
					}

					// The first blue polyline
					Polyline firstLine = new Polyline();
					firstLine.setFollowTerrain(true);
					firstLine.setPositions(new ArrayList<Position>(2));
					firstLine.setColor(Color.blue);
					firstLine.setLineWidth(5);
					firstLine.setClosed(true);
					List<Position> firstLinePoList = (List<Position>) firstLine.getPositions();

					// The second red polyline
					Polyline secondLine = new Polyline();
					secondLine.setFollowTerrain(true);
					secondLine.setPositions(new ArrayList<Position>(2));
					secondLine.setColor(Color.red);
					secondLine.setLineWidth(5);
					secondLine.setClosed(true);
					List<Position> secondLinePoList = (List<Position>) secondLine.getPositions();

					if (indexStartPosInCot < indexEndPosInCot) { // 1er cas de séparation

						int indexRenderableSegmentInCotTmp2 = 0;
						for (Renderable r : layer.getRenderables()) {
							if (indexRenderableSegmentInCotTmp2 == indexRenderableSegmentInCot) {
								if (r instanceof CotSegmentRenderer) {
									CotSegmentRenderer segment = (CotSegmentRenderer) r;
									@SuppressWarnings("unchecked")
									List<LatLon> latlonCotList = (List<LatLon>) segment.getLocations();

									// Construction premiere polyline
									// ajout debut cot segment
									for (int i = 0; i < indexStartPosInCot; i++) {
										firstLinePoList.add(
												Position.fromDegrees(latlonCotList.get(i).getLatitude().getDegrees(),
														latlonCotList.get(i).getLongitude().getDegrees()));
									}
									// ajout nouvelle separation
									for (Position p : positionList) {
										firstLinePoList.add(p);
									}
									// ajout fin du cot
									for (int i = indexEndPosInCot + 1; i < latlonCotList.size(); i++) {
										firstLinePoList.add(
												Position.fromDegrees(latlonCotList.get(i).getLatitude().getDegrees(),
														latlonCotList.get(i).getLongitude().getDegrees()));
									}

									// Construction deuxieme polyline
									// ajout nouvelle separation
									for (Position p : positionList) {
										secondLinePoList.add(p);
									}
									// ajout segment cot entre endpoint et startpoint
									for (int i = indexEndPosInCot - 1; i > indexStartPosInCot; i--) {
										secondLinePoList.add(
												Position.fromDegrees(latlonCotList.get(i).getLatitude().getDegrees(),
														latlonCotList.get(i).getLongitude().getDegrees()));
									}
								}
							}
							indexRenderableSegmentInCotTmp2++;
						}

					} else { // 2eme cas de séparation (indexStartPosInCot > indexEndPosInCot)
						int indexRenderableSegmentInCotTmp2 = 0;
						for (Renderable r : layer.getRenderables()) {
							if (indexRenderableSegmentInCotTmp2 == indexRenderableSegmentInCot) {
								if (r instanceof CotSegmentRenderer) {
									CotSegmentRenderer segment = (CotSegmentRenderer) r;
									@SuppressWarnings("unchecked")
									List<LatLon> latlonCotList = (List<LatLon>) segment.getLocations();

									// Construction premiere polyline
									// ajout debut cot segment
									for (int i = 0; i < indexEndPosInCot; i++) {
										firstLinePoList.add(
												Position.fromDegrees(latlonCotList.get(i).getLatitude().getDegrees(),
														latlonCotList.get(i).getLongitude().getDegrees()));
									}
									// ajout nouvelle separation (en sens inverse)
									for (int i = positionList.size() - 1; i >= 0; i--) {
										firstLinePoList.add(positionList.get(i));
									}

									// ajout fin du cot
									for (int i = indexStartPosInCot + 1; i < latlonCotList.size(); i++) {
										firstLinePoList.add(
												Position.fromDegrees(latlonCotList.get(i).getLatitude().getDegrees(),
														latlonCotList.get(i).getLongitude().getDegrees()));
									}

									// Construction deuxieme polyline
									// ajout nouvelle separation
									for (Position p : positionList) {
										secondLinePoList.add(p);
									}

									// ajout segment cot entre endpoint et startpoint
									for (int i = indexEndPosInCot + 1; i < indexStartPosInCot; i++) {
										secondLinePoList.add(
												Position.fromDegrees(latlonCotList.get(i).getLatitude().getDegrees(),
														latlonCotList.get(i).getLongitude().getDegrees()));
									}
								}
							}
							indexRenderableSegmentInCotTmp2++;
						}
					}

					// Save first blue cot
					Shell activeShell = Display.getDefault().getActiveShell();
					FileDialog firstDialog = new FileDialog(activeShell, SWT.SAVE);
					firstDialog.setText("Save the blue cot file");
					firstDialog.setFilterNames(CotComposite.FORMAT_NAMES);
					firstDialog.setFilterExtensions(CotComposite.FORMAT_EXTENSIONS);
					firstDialog.setFileName("blueCot");

					// Add the blue line to layer
					layer.addRenderable(firstLine);
					Viewer3D.getWwd().redraw();

					String firstFilename = firstDialog.open();
					if (firstFilename != null) {

						// Create the exportable class for the CotWriter
						var exportableCotPositions = new ArrayList<Iterable<Position>>();
						exportableCotPositions.add(firstLine.getPositions());

						boolean success = new CotWriter().export(new File(firstFilename), exportableCotPositions);
						if (!success) {
							logger.error("Export failed");
						}
					}

					// Save second red cot
					FileDialog secondDialog = new FileDialog(activeShell, SWT.SAVE);
					secondDialog.setText("Save the red cot file");
					secondDialog.setFilterNames(CotComposite.FORMAT_NAMES);
					secondDialog.setFilterExtensions(CotComposite.FORMAT_EXTENSIONS);
					secondDialog.setFileName("redCot");

					// Add the red line to layer and remove blue line
					layer.removeRenderable(firstLine);
					layer.addRenderable(secondLine);
					Viewer3D.getWwd().redraw();

					String secondFilename = secondDialog.open();
					if (secondFilename != null) {

						// Create the exportable class for the CotWriter
						var exportableCotPositions = new ArrayList<Iterable<Position>>();
						exportableCotPositions.add(secondLine.getPositions());

						CotWriter cotWriter = new CotWriter();
						boolean success = cotWriter.export(new File(secondFilename), exportableCotPositions);
						if (!success) {
							logger.error("Export failed");
						}
					}

					// Remove the first and second lines
					layer.removeRenderable(firstLine);
					layer.removeRenderable(secondLine);
					// Add back the sepration line and pointplacermaks
					layer.addRenderable(line);
					for (PointPlacemark pp : pPList) {
						layer.addRenderable(pp);
					}
				} else {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Save problem",
							"Please select a start point AND an end point before trying to save.");
				}
			}
		});
		// TODO Auto-generated constructor stub
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent mouseEvent) {
		if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
			// Select a position on the globe
			Position currentPosition = Viewer3D.getWwd().getCurrentPosition();
			if (currentPosition != null) {
				currentPosition = new Position(currentPosition, 0); // Set elevation to 0 to avoid bugs
				List<Position> positionList = (List<Position>) line.getPositions();
				switch (pointType) {
				case 0: // 0 : start
					Position nearestPos = searchNearestPositionOnCot(currentPosition);

					if (hasStartPoint) {
						positionList.set(0, nearestPos);
						pPList.get(0).setPosition(nearestPos);
					} else {
						positionList.add(0, nearestPos);
						// add PointPlacemark
						PointPlacemark pp = addPlacermarkToLayer(nearestPos);
						pPList.add(0, pp);
					}

					hasStartPoint = true;

					// Reset point type to insert point
					pointType = 2;

					break;

				case 1: // 1 : add

					positionList = (List<Position>) line.getPositions();

					// add PointPlacemark
					PointPlacemark pp = addPlacermarkToLayer(currentPosition);

					if (!hasEndPoint) {
						positionList.add(currentPosition);
						pPList.add(pp);
					} else if (hasEndPoint) {
						positionList.add(positionList.size() - 1, currentPosition);
						pPList.add(pPList.size() - 1, pp);
					}
					break;

				case 2: // 2 : insert

					insertPoint(currentPosition);

					break;

				case 3: // 3 : delete

					// Search the nearest position on line
					Position positionNearest = searchNearestPositionFromLineExceptStartEnd(currentPosition);

					if (positionNearest != null) {
						positionList = (List<Position>) line.getPositions();
						int indexTodelete = positionList.indexOf(positionNearest);
						positionList.remove(indexTodelete);
						layer.removeRenderable(pPList.get(indexTodelete));
						pPList.remove(indexTodelete);
					}

					break;

				case 4: // 4 : move

					if (!hasMovedPoint) {
						// Search the nearest position on line
						positionNearest = searchNearestPositionFromLineExceptStartEnd(currentPosition);

						if (positionNearest != null) {
							// Change color of the selected point
							positionList = (List<Position>) line.getPositions();
							indexOfMovedPoint = positionList.indexOf(positionNearest);
							pPList.get(indexOfMovedPoint).getAttributes().setLineMaterial(new Material(Color.green));
							hasMovedPoint = true;
						}
					} else {
						// Move the selected position
						positionList = (List<Position>) line.getPositions();
						// Set the new position to the line and the pointplacmark (and change color)
						positionList.set(indexOfMovedPoint, currentPosition);

						pPList.get(indexOfMovedPoint).setPosition(currentPosition);
						pPList.get(indexOfMovedPoint).getAttributes().setLineMaterial(new Material(Color.yellow));

						hasMovedPoint = false;
					}

					break;

				case 5: // 5 : end
					nearestPos = searchNearestPositionOnCot(currentPosition);
					positionList = (List<Position>) line.getPositions();
					if (hasEndPoint) {
						positionList.set(positionList.size() - 1, nearestPos);
						// Set end PointPlacemark
						pPList.get(positionList.size() - 1).setPosition(nearestPos);
					} else {
						positionList.add(nearestPos);

						// add End PointPlacemark
						pp = addPlacermarkToLayer(nearestPos);
						pPList.add(pp);
					}

					hasEndPoint = true;

					// Reset point type to insert point
					pointType = 2;

					break;

				default:
					break;
				}

				// Refresh the sector visibility of line
				line.setPositions(positionList);

				// Refresh the globe
				Viewer3D.getWwd().redraw();

			}

		}

	}

	/**
	 * Add a placemark to the layer.
	 *
	 * @param position
	 * @return
	 */
	private PointPlacemark addPlacermarkToLayer(Position position) {
		// Default PointPlacemarkAttributes for PointPlacemark
		PointPlacemarkAttributes pointAttribute = new PointPlacemarkAttributes();
		pointAttribute.setUsePointAsDefaultImage(true);
		pointAttribute.setLineMaterial(new Material(Color.yellow));

		PointPlacemark pp = new PointPlacemark(position);
		pp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		pp.setLineEnabled(true);
		pp.setValue(AVKey.LAYER, this);
		pp.setAttributes(pointAttribute);
		pp.setVisible(true);
		layer.addRenderable(pp);

		return pp;
	}

	/**
	 * Search the nearest position from Cot layer.
	 *
	 * @param currentPosition
	 * @return the nearest position on cot
	 */
	private Position searchNearestPositionOnCot(Position currentPosition) {

		Angle angleMin = Angle.POS360;
		LatLon latlonNearest = null;
		for (Renderable r : layer.getRenderables()) {
			if (r instanceof CotSegmentRenderer) {
				CotSegmentRenderer segment = (CotSegmentRenderer) r;
				for (LatLon latlon2 : segment.getLocations()) {
					Angle angle = LatLon.greatCircleDistance(currentPosition, latlon2);
					if (angleMin.compareTo(angle) == 1) {
						angleMin = angle;
						latlonNearest = latlon2;
					}
				}
			}
		}

		return Position.fromDegrees(latlonNearest.getLatitude().getDegrees(),
				latlonNearest.getLongitude().getDegrees());
	}

	/**
	 * Search the nearest position on line
	 *
	 * @param currentPosition
	 * @return the nearest position on line
	 */
	private Position searchNearestPositionFromLine(Position currentPosition) {
		Angle angleMin = Angle.POS360;
		Position positionNearest = null;
		for (Position p : line.getPositions()) {
			Angle angle = LatLon.greatCircleDistance(currentPosition, p);
			if (angleMin.compareTo(angle) == 1) {
				angleMin = angle;
				positionNearest = p;
			}
		}

		return positionNearest;

	}

	/**
	 * Search the nearest position on line (except the start and end point)
	 *
	 * @param currentPosition
	 * @return the nearest position on line
	 */
	private Position searchNearestPositionFromLineExceptStartEnd(Position currentPosition) {
		Angle angleMin = Angle.POS360;
		Position positionNearest = null;
		List<Position> positionList = (List<Position>) line.getPositions();

		// Determine a quel index on commence et on finit
		int startPos = 0;
		int endPos = positionList.size();
		if (hasStartPoint)
			startPos = 1;
		if (hasEndPoint)
			endPos = positionList.size() - 1;

		for (int i = startPos; i < endPos; i++) {
			Position p = positionList.get(i);
			Angle angle = LatLon.greatCircleDistance(currentPosition, p);
			if (angleMin.compareTo(angle) == 1) {
				angleMin = angle;
				positionNearest = p;
			}
		}

		return positionNearest;

	}

	/**
	 * Inserts the current position between 2 points from line. Use the algorithm created by Ifremer (Pascal Pelleau)
	 *
	 * @param currentPosition
	 */
	private void insertPoint(Position currentPosition) {

		// Search the nearest position on line
		Position positionNearest = searchNearestPositionFromLine(currentPosition);

		if (positionNearest != null) {
			// add PointPlacemark
			PointPlacemark pp = addPlacermarkToLayer(currentPosition);

			// Search the nearest neighbor
			List<Position> positionList = (List<Position>) line.getPositions();
			int indexOfPositionNearest = positionList.indexOf(positionNearest);

			if (indexOfPositionNearest == 0) { // nearest neighbor is start point
				positionList.add(indexOfPositionNearest + 1, currentPosition);
				pPList.add(indexOfPositionNearest + 1, pp);
			} else if (indexOfPositionNearest == positionList.size() - 1) { // nearest neighbor is last point
				positionList.add(indexOfPositionNearest, currentPosition);
				pPList.add(indexOfPositionNearest, pp);
			} else { // other case
				// Algo Insertion de point (Pascal Pelleau)

				// Soit le point N, le point que l'on veut insérer
				Position N = currentPosition;
				double Nx = N.longitude.degrees;
				double Ny = N.latitude.degrees;

				// Soit les 3 point A B et C avec B le point le plus proche du point inseré et A et C ses voisins :
				Position A = positionList.get(indexOfPositionNearest - 1);
				Position B = positionNearest;
				Position C = positionList.get(indexOfPositionNearest + 1);

				double Ax = A.longitude.degrees;
				double Ay = A.latitude.degrees;
				double Bx = B.longitude.degrees;
				double By = B.latitude.degrees;
				double Cx = C.longitude.degrees;
				double Cy = C.latitude.degrees;

				// Les compteurs pour A et C. Le plus élévé à la fin de la recherche détermine quel voisin choisir
				int countA = 0;
				int countC = 0;

				// Etape 1

				// Hypothèses qui favorisent le point A
				if ((Ax <= Nx) && (Nx <= Bx)) {
					countA++;
				}
				if ((A.latitude.degrees <= N.latitude.degrees) && (N.latitude.degrees <= B.latitude.degrees)) {
					countA++;
				}
				if ((Bx <= Nx) && (Nx <= Ax)) {
					countA++;
				}
				if ((B.latitude.degrees <= N.latitude.degrees) && (N.latitude.degrees <= A.latitude.degrees)) {
					countA++;
				}

				// Hypothèses qui favorisent le point C
				if ((Bx <= Nx) && (Nx <= Cx)) {
					countC++;
				}
				if ((B.latitude.degrees <= N.latitude.degrees) && (N.latitude.degrees <= C.latitude.degrees)) {
					countC++;
				}
				if ((Cx <= Nx) && (Nx <= Bx)) {
					countC++;
				}
				if ((C.latitude.degrees <= N.latitude.degrees) && (N.latitude.degrees <= B.latitude.degrees)) {
					countC++;
				}

				if (countA == 0 && countC == 0) { // cf2eme étape : On n'a pas pu déterminé quel point l'emporte (0
													// point partout) , il faut passer un nouveau test

					if ((Ax < Bx) && (Bx < Cx)) {
						if (Nx < Ax) {
							countA++;
						}
						if (Nx > Cx) {
							countC++;
						}
					} else if ((Cx < Bx) && (Bx < Ax)) {
						if (Nx < Cx) {
							countC++;
						}
						if (Nx > Ax) {
							countA++;
						}

					}
					if ((Ay < By) && (By < Cy)) {
						if (Ny < Ay) {
							countA++;
						}
						if (Ny > Cy) {
							countC++;
						}
					} else if ((Cy < By) && (By < Ay)) {

						if (Ny < Cy) {
							countC++;
						}
						if (Ny > Ay) {
							countA++;
						}
					}

				} else if (countA == 1 && countC == 1) { // cf 4eme étape : On n'a pas pu déterminé quel point l'emporte
															// (1 point partout) , il faut passer un nouveau test

					// Equation de droite AB(y = mx + n)
					double mAB = (By - Ay) / (Bx - Ax);
					double nAB = By - mAB * Bx;
					// Equation de droite BC (y = mx + n)
					double mBC = (Cy - By) / (Cx - Bx);
					double nBC = Cy - mBC * Cx;

					// Distance des points
					double distNAB = Math.abs(mAB * Nx - Ny + nAB) / Math.sqrt(mAB * mAB + 1);
					double distNBC = Math.abs(mBC * Nx - Ny + nBC) / Math.sqrt(mBC * mBC + 1);

					if (distNAB < distNBC) {
						countA++;
					} else {
						countC++;
					}
				} else if (countA == 2 && countC == 2) {// cf3eme étape : On n'a pas pu déterminé quel point l'emporte
														// (2 point partout) , il faut passer un nouveau test
					// Equation de droite AB (y = mx + n)
					double mAB = (By - Ay) / (Bx - Ax);
					double nAB = By - mAB * Bx;
					// Equation de droite BC (y = mx + n)
					double mBC = (Cy - By) / (Cx - Bx);
					double nBC = Cy - mBC * Cx;

					// Distance des points par projection
					double NABpy = mAB * Nx + nAB;
					double NABpx = mAB * Ny + nAB;
					double NBCpy = mBC * Nx + nBC;
					double NBCpx = mBC * Ny + nBC;

					double distNAB = 0;
					double distNBC = 0;

					// verticale
					if (Ay < NABpy && NABpy < By) {
						distNAB = Math.abs(NABpy - Ny);
					}
					if (By < NBCpy && NBCpy < Cy) {
						distNBC = Math.abs(NBCpy - Ny);
					}

					// horizontal
					if (Ax < NABpx && NABpx < Bx) {
						distNAB = Math.abs(NABpx - Nx);
					}
					if (Bx < NBCpx && NBCpx < Cx) {
						distNBC = Math.abs(NBCpy - Nx);
					}

					if (distNAB < distNBC) {
						countA++;
					} else {
						countC++;
					}
				}

				// Comparaison countA et countC
				if (countA != countC) { // Il y a un point A ou C que l'on peut choisir
					if (countA > countC) { // Le point A l'emporte
						positionList.add(indexOfPositionNearest, currentPosition);
						pPList.add(indexOfPositionNearest, pp);
					} else { // Le point C l'emporte
						positionList.add(indexOfPositionNearest + 1, currentPosition);
						pPList.add(indexOfPositionNearest + 1, pp);
					}
				} else {
					// Neighbor before
					Position positionNeigbhorBefore = positionList.get(indexOfPositionNearest - 1);
					Angle angleBefore = LatLon.greatCircleDistance(currentPosition, positionNeigbhorBefore);
					// Neighbor after
					Position positionNeigbhorAfter = positionList.get(indexOfPositionNearest + 1);
					Angle angleAfter = LatLon.greatCircleDistance(currentPosition, positionNeigbhorAfter);

					if (angleBefore.compareTo(angleAfter) == 1) {
						positionList.add(indexOfPositionNearest + 1, currentPosition);
						pPList.add(indexOfPositionNearest + 1, pp);
					} else {
						positionList.add(indexOfPositionNearest, currentPosition);
						pPList.add(indexOfPositionNearest, pp);
					}
				}
			}
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
