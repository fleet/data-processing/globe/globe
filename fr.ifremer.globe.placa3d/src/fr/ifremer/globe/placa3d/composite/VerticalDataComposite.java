package fr.ifremer.globe.placa3d.composite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;

import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.MyAbstractLayerParameter;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;

public class VerticalDataComposite extends Composite {

	private MyAbstractLayer layer;

	public VerticalDataComposite(Composite parent, MyAbstractLayer layer) {
		super(parent, SWT.NONE);
		this.layer = layer;
		init();
	}

	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);

		Group colorInterpolationGroup = new Group(this, SWT.NONE);
		colorInterpolationGroup.setText("Color interpolation");
		colorInterpolationGroup.setLayout(new GridLayout(2, false));
		colorInterpolationGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		Button linearButton = new Button(colorInterpolationGroup, SWT.RADIO);
		linearButton.setText("Mipmaps/Linear");
		linearButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		linearButton.setToolTipText("Better performances for rendering");
		Object defaultValueInterpolation=layer.getValue(MyAbstractLayerParameter.INTERPOLATION);
		if(defaultValueInterpolation==null)
		{
			layer.setValue(MyAbstractLayerParameter.INTERPOLATION, MyAbstractLayerParameter.INTERPOLATION_LINEAR);
			defaultValueInterpolation=MyAbstractLayerParameter.INTERPOLATION_LINEAR;
		}
		boolean interpol=MyAbstractLayerParameter.INTERPOLATION.equals(defaultValueInterpolation);
		linearButton.setSelection(interpol);

		linearButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				VerticalDataComposite.this.layer.setValue(MyAbstractLayerParameter.INTERPOLATION, MyAbstractLayerParameter.INTERPOLATION_LINEAR);
				RefreshOpenGLView.fullRefresh();
			}
		});

		Button nearestButton = new Button(colorInterpolationGroup, SWT.RADIO);
		nearestButton.setSelection(!interpol);
		nearestButton.setText("No interpolation");
		nearestButton.setToolTipText("Exact rendering but requires more memory");
		nearestButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		nearestButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				VerticalDataComposite.this.layer.setValue(MyAbstractLayerParameter.INTERPOLATION, MyAbstractLayerParameter.INTERPOLATION_NEAREST);
				RefreshOpenGLView.fullRefresh();
			}
		});
	}
}
