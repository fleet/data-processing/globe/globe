﻿package fr.ifremer.globe.placa3d.composite;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ffmpeg.FfmpegUtils;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.application.event.LayerStoreAddedEvent;
import fr.ifremer.globe.placa3d.application.event.PlacaEventTopics;
import fr.ifremer.globe.placa3d.composite.SubsidenceComposite.SubsidenceModel;
import fr.ifremer.globe.placa3d.gui.MovableLayerDialog;
import fr.ifremer.globe.placa3d.preferences.Placa3dPreferences;
import fr.ifremer.globe.placa3d.store.LayerStore;
import fr.ifremer.globe.placa3d.tectonic.AgeOffsetFileLoader;
import fr.ifremer.globe.placa3d.tectonic.PoleRotations;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableNumber;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.screenshot.OGLScreenshot;
import fr.ifremer.globe.ui.screenshot.VideoCapture;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.OSUtils;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/***
 * IHM to change tectonic plates date allowing to move plates according to geologic era
 *
 * @author MORVAN
 *
 */
public class PoleRotationComposite extends Composite implements PropertyChangeListener {

	private static final String ICONS_MEDIA_STEP_FORWARD_PNG = OSUtils.isMac() ? "icons/16/media-step-forward.png"
			: "icons/32/media-step-forward.png";

	private static final String ICONS_MEDIA_PLAYBACK_START_PNG = OSUtils.isMac() ? "icons/16/media-playback-start.png"
			: "icons/32/media-playback-start.png";

	private static final String ICONS_MEDIA_REVERSE_START_PNG = OSUtils.isMac() ? "icons/16/media-reverse-start.png"
			: "icons/32/media-reverse-start.png";

	private static final String ICONS_MEDIA_STEP_BACKWARD_PNG = OSUtils.isMac() ? "icons/16/media-step-backward.png"
			: "icons/32/media-step-backward.png";

	protected static final String ICON_MEDIA_PLAYBACK_PAUSE = OSUtils.isMac() ? "/icons/16/media-playback-pause.png"
			: "/icons/32/media-playback-pause.png";

	protected static final String ICON_MEDIA_PLAYBACK_START = OSUtils.isMac() ? "/icons/16/media-playback-start.png"
			: "/icons/32/media-playback-start.png";

	protected static final String ICON_MEDIA_REVERSE_START = OSUtils.isMac() ? "/icons/16/media-reverse-start.png"
			: "/icons/32/media-reverse-start.png";

	protected static final String PLAYER_GROUP_NAME = "Tectonic settings";

	protected static final String ANO_GROUP_NAME = "List of poles";

	protected static final String COLOR_GROUP_NAME = "Color of Poles of Rotation";

	protected static final String SPEED_LABEL = "x";

	protected static final String REMANENT_TEXT = "Remanent";

	protected static final String NEXT_IMAGE_TEXT = "Next image";

	protected static final String PLAY_PAUSE_TEXT = "Play/Pause";

	protected static final String PLAY_REVERSE_TEXT = "Play reverse";

	protected static final String PREVIOUS_IMAGE_TEXT = "Previous image";

	protected static final String CURRENT_INDEX_TEXT = "Current age";

	protected static final String SPEED_TEXT = "Speed";

	protected static final String ANIMATION_CAPTURE_TEXT = "Animation capture";

	protected static final String CAPTURE_TEXT = "Capture";

	protected static final String REFERENTIAL_TEXT = "Referential";

	protected static final String SPEED_VALUES[] = { "0.1", "0.2", "0.5", "1", "2", "4", "8", "16", "32", "64" };

	protected static final String DELETE_LAYER = "Delete";

	protected static final String ADD_LAYER = "Add";

	protected static final String CHANGE_COLOR = "Change color";
	protected static final String CHANGE_COLOR_TOOLTIP = "Change the color of poles of rotation of this layer";

	protected static PoleRotationComposite activeComposite;
	protected static List<PoleRotationComposite> composites = new ArrayList<>();
	protected static boolean playing;
	protected static boolean reverseplaying;

	/** Path for storing screenshot when in video capture mode. */
	protected static String videoCaptureCurrentPath = "";

	/** last speed for all the pole layers */
	protected static float lastSpeed = 1;

	@Inject
	protected WWFileLayerStoreModel fileLayerStoreModel;

	protected Consumer<WWFileLayerStoreEvent> fileLayerStoreModelListener = this::onFileLayerStoreEvent;
	protected TectonicDefinition tectonicDefinition;
	protected TectonicPlayerModel player;

	// SWT widget
	protected Spinner currentIndexSpinner;
	protected Scale indexSlider;
	protected Button stepBackwardButton;
	protected Button reverseButton;
	protected Button playButton;
	protected Button stepForwardButton;
	protected Button remanentButton;
	protected Label speedLabel;
	protected Combo speedCombo;
	protected Button referentielButton;
	protected Button bVideoCapture;
	protected org.eclipse.swt.widgets.List layersList;
	protected Button addButton;
	protected Button deleteButton;
	protected Button colorButton;

	/** maps for associate a layer store and its name in the SWT list. */
	protected Map<String, LayerStore> layersMap;

	// Attributs for videoCapture
	protected boolean videoCaptureEnable = false;
	protected String videoCaptureDirectory = null;
	protected String headerNameFile = "";
	protected int videoImageNb = 1;

	protected Placa3dPreferences preferences;

	/** StyledText for list of total pole */
	StyledText totText;
	/** StyledText for list of int pole */
	StyledText intText;

	/** Model */
	protected PoleRotationModel poleRotationModel = new PoleRotationModel();
	/** Subsidence composite */
	protected SubsidenceComposite subsidenceComposite;

	/** Constructor used with WindowBuilder */
	public PoleRotationComposite(Composite parent, int style) {
		super(parent, style);
		init();
	}

	/**
	 * IHM enabling movement of tectonic plates
	 *
	 * @param parent
	 * @param style
	 * @param tectonicDefinition
	 */
	public PoleRotationComposite(Composite parent, int style, TectonicDefinition tectonicDefinition) {
		super(parent, style);
		preferences = Activator.getPreferences();
		this.tectonicDefinition = tectonicDefinition;
		player = tectonicDefinition.getPlayer();
		layersMap = new HashMap<>();

	}

	@PostConstruct
	public void postConstruct() {
		if (tectonicDefinition != null && player != null) {
			init();
			poleRotationModel.getOffsetMode().set(player.getOffsetMode());
			poleRotationModel.getConstantOffset().set(player.getApproxElevationOffset());
			poleRotationModel.getOffsetFile().set(player.getOffsetFile());
			subsidenceComposite.addSubsidenceModelObserver((observable, subsidenceModel) -> updateOffsetMode());
		}

		composites.add(this);

		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				composites.remove(PoleRotationComposite.this);
				PoleRotationComposite.this.removeDisposeListener(this);
				player.removePropertyChangeListener(PoleRotationComposite.this);
				fileLayerStoreModel.removeListener(fileLayerStoreModelListener);
			}
		});

		addPaintListener(e -> activeComposite = PoleRotationComposite.this);

		player.addPropertyChangeListener(this);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	protected void init() {

		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		createPlayerGroup();
		updateList();
		createSubsidenceGroup();
		createAdditionalGroup();
		createAnomaliesGroup();
		createColorGroup();

		fileLayerStoreModel.addListener(fileLayerStoreModelListener);
	}

	/** Allaw subclasse to add a group */
	protected void createAdditionalGroup() {
	}

	protected boolean isAllOffsets() {
		return TectonicDataset.getInstance().getConfiguration().isSynchronizeOffsets();
	}

	protected boolean isAllPlates() {
		return TectonicDataset.getInstance().getConfiguration().isAllPlates();
	}

	protected void updateOffsetMode() {
		OffsetMode offsetMode = poleRotationModel.getOffsetMode().get();
		double offsetValue = offsetMode == OffsetMode.MANUAL_VALUE ? poleRotationModel.getConstantOffset().doubleValue()
				: 0d;
		File offsetMappingFile = offsetMode == OffsetMode.MANUAL_FILE ? poleRotationModel.getOffsetFile().get() : null;

		if (isAllOffsets()) {
			for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
				p.getPlayer().setOffsetMode(offsetMode, offsetValue, offsetMappingFile);
			}
		} else {
			player.setOffsetMode(offsetMode, offsetValue, offsetMappingFile);
		}
		RefreshOpenGLView.redraw();
	}

	/**
	 * When isAllPlates is active, the offset mode is copied from the active composite
	 */
	public void setSlaveOffsetMode() {

		// In "AllPlates" mode, get the parameters from the active
		// PoleRotationComposite displayed in parameters view
		if (isAllOffsets() && activeComposite != null && activeComposite != this) {
			OffsetMode offsetMode = activeComposite.player.getOffsetMode();
			File offsetFile = activeComposite.player.getOffsetFile();
			int currentOffset = activeComposite.player.getApproxElevationOffset();
			player.setOffsetMode(offsetMode, currentOffset, offsetFile);
		}

	}

	protected boolean isValidOffsetFile(File choosenFile) {
		AgeOffsetFileLoader subsidenceFileLoader = new AgeOffsetFileLoader();
		try {
			return subsidenceFileLoader.loadOffsetFile(choosenFile) != null;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Create the player group of this composite.
	 */
	protected Group createPlayerGroup() {
		// player group
		final Group playerGroup = new Group(this, SWT.NONE);
		playerGroup.setText(PLAYER_GROUP_NAME);
		playerGroup.setLayout(new GridLayout(4, true));
		playerGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		ToolTip tooltip = new ToolTip(getShell(), SWT.BALLOON);
		tooltip.setMessage("Press ENTER to validate or ESC to cancel.");

		Composite ageComposite = new Composite(playerGroup, SWT.NONE);
		ageComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));
		ageComposite.setLayout(new GridLayout(3, false));

		Label label = new Label(ageComposite, SWT.NONE);
		label.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_EarthAge);

		currentIndexSpinner = new Spinner(ageComposite, SWT.BORDER);
		GridData gd_currentIndexSpinner = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		// increase width for mac support
		gd_currentIndexSpinner.widthHint = 40;
		currentIndexSpinner.setLayoutData(gd_currentIndexSpinner);

		currentIndexSpinner.setToolTipText(
				CURRENT_INDEX_TEXT + " [" + player.getMinDateIndex() + " - " + player.getMaxDateIndex() + "]");
		currentIndexSpinner.setMinimum(player.getMinDateIndex());
		currentIndexSpinner.setMaximum(player.getMaxDateIndex());
		currentIndexSpinner.setSelection(player.getDateIndex());
		currentIndexSpinner.addListener(SWT.Selection, event -> {
			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						p.getPlayer().setPlaying(false);
						p.getPlayer().setReversePlaying(false);
						p.getPlayer().setDateIndex(currentIndexSpinner.getSelection());
					}
				}
				for (PoleRotationComposite composite : composites) {
					if (composite != null) {
						composite.currentIndexSpinner.setSelection(currentIndexSpinner.getSelection());
					}
				}
			} else {
				player.setPlaying(false);
				player.setReversePlaying(false);
				player.setDateIndex(currentIndexSpinner.getSelection());
			}
		});

		indexSlider = new Scale(ageComposite, SWT.NONE);
		indexSlider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		indexSlider.setMinimum(player.getMinDateIndex());
		indexSlider.setMaximum(player.getMaxDateIndex());
		indexSlider.setSelection(player.getDateIndex());
		indexSlider.setPageIncrement(player.getMaxDateIndex() - player.getMinDateIndex());
		indexSlider.addListener(SWT.Selection, event -> {
			// reset video capture
			disableCaptureVideo();
			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						p.getPlayer().setPlaying(false);
						p.getPlayer().setReversePlaying(false);
						p.getPlayer().setDateIndex(indexSlider.getSelection());
					}
				}
				for (PoleRotationComposite composite : composites) {
					if (composite != null) {
						composite.indexSlider.setSelection(indexSlider.getSelection());
					}
				}
			} else {
				player.setPlaying(false);
				player.setReversePlaying(false);
				player.setDateIndex(indexSlider.getSelection());
			}
		});
		Composite composite_buttons = new Composite(playerGroup, SWT.NONE);
		composite_buttons.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		composite_buttons.setLayout(new GridLayout(4, false));

		if (OSUtils.isMac()) {

		}

		stepBackwardButton = new Button(composite_buttons, SWT.NONE);
		stepBackwardButton
				.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.placa3d", ICONS_MEDIA_STEP_BACKWARD_PNG));
		reverseButton = new Button(composite_buttons, SWT.TOGGLE);
		reverseButton
				.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.placa3d", ICONS_MEDIA_REVERSE_START_PNG));

		playButton = new Button(composite_buttons, SWT.TOGGLE);
		playButton.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.placa3d", ICONS_MEDIA_PLAYBACK_START_PNG));

		stepForwardButton = new Button(composite_buttons, SWT.NONE);
		stepForwardButton
				.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.placa3d", ICONS_MEDIA_STEP_FORWARD_PNG));

		remanentButton = new Button(playerGroup, SWT.CHECK);
		bVideoCapture = new Button(playerGroup, SWT.CHECK);
		new Label(playerGroup, SWT.NONE);

		Composite composite_1 = new Composite(playerGroup, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		composite_1.setLayout(new GridLayout(2, false));
		speedLabel = new Label(composite_1, SWT.NONE);

		speedLabel.setText(SPEED_LABEL);
		speedCombo = new Combo(composite_1, SWT.READ_ONLY);

		speedCombo.setToolTipText(SPEED_TEXT);
		speedCombo.setItems(SPEED_VALUES);
		speedCombo.select(3);
		speedCombo.addListener(SWT.Selection, event -> {

			float speed = Float.parseFloat(SPEED_VALUES[speedCombo.getSelectionIndex()]);
			lastSpeed = speed;

			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						p.getPlayer().setPlayingSpeed(speed);
					}
				}
				for (PoleRotationComposite composite : composites) {
					if (composite != null) {
						composite.speedCombo.select(speedCombo.getSelectionIndex());
					}
				}
			} else {
				player.setPlayingSpeed(speed);
			}
		});
		referentielButton = new Button(playerGroup, SWT.CHECK);
		new Label(playerGroup, SWT.NONE);
		new Label(playerGroup, SWT.NONE);

		Composite composite_2 = new Composite(playerGroup, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		composite_2.setLayout(new GridLayout(2, false));
		layersList = new org.eclipse.swt.widgets.List(composite_2, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		layersList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4));

		addButton = new Button(composite_2, SWT.PUSH);
		addButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		addButton.setText(ADD_LAYER);
		addButton.setToolTipText(ADD_LAYER);
		addButton.addListener(SWT.Selection, event -> {
			MovableLayerDialog dialog = new MovableLayerDialog(getShell(), tectonicDefinition);
			dialog.open();
			for (PoleRotationComposite composite : composites) {
				composite.updateList();
				composite.updateListOfPoles();
			}
		});
		deleteButton = new Button(composite_2, SWT.PUSH);
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		deleteButton.setText(DELETE_LAYER);
		deleteButton.setToolTipText(DELETE_LAYER);
		deleteButton.addListener(SWT.Selection, event -> {
			String[] sLayerList = layersList.getSelection();
			for (String layerStoreName : sLayerList) {
				tectonicDefinition.removeLinkToLayerStore(layersMap.get(layerStoreName));
			}
			for (PoleRotationComposite composite : composites) {
				composite.updateList();
				composite.updateListOfPoles();
			}
		});
		new Label(composite_2, SWT.NONE);
		new Label(composite_2, SWT.NONE);

		stepBackwardButton.setToolTipText(PREVIOUS_IMAGE_TEXT);
		updateButtonSize(stepBackwardButton);

		stepBackwardButton.addListener(SWT.Selection, event -> {
			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				int imgIndex1 = Math.max(player.getMinDateIndex(),
						player.getDateIndex() - (int) player.getPlayingSpeed());
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						p.getPlayer().setPlaying(false);
						p.getPlayer().setReversePlaying(false);
						p.getPlayer().setDateIndex(imgIndex1);
					}
				}

			} else {
				player.setPlaying(false);
				player.setReversePlaying(false);

				int imgIndex2 = Math.max(player.getMinDateIndex(),
						player.getDateIndex() - (int) player.getPlayingSpeed());
				player.setDateIndex(imgIndex2);
			}
		});
		stepBackwardButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		reverseButton.setToolTipText(PLAY_REVERSE_TEXT);
		updateButtonSize(reverseButton);
		reverseButton.addListener(SWT.Selection, event -> {
			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						if (p.getPlayer().isReversePlaying()) {
							p.getPlayer().setPlaying(false);
							p.getPlayer().setReversePlaying(false);
						} else {
							p.getPlayer().setPlaying(false);
							p.getPlayer().setReversePlaying(true);
						}
					}
				}
			} else {
				if (player.isReversePlaying()) {
					player.setPlaying(false);
					player.setReversePlaying(false);
				} else {
					player.setPlaying(false);
					player.setReversePlaying(true);
				}
			}
		});
		reverseButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		playButton.setToolTipText(PLAY_PAUSE_TEXT);
		updateButtonSize(playButton);

		playButton.addListener(SWT.Selection, event -> {
			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						if (p.getPlayer().isPlaying()) {
							p.getPlayer().setPlaying(false);
							p.getPlayer().setReversePlaying(false);
						} else {
							p.getPlayer().setPlaying(true);
							p.getPlayer().setReversePlaying(false);
						}
					}
				}
			} else {
				if (player.isPlaying()) {
					player.setPlaying(false);
					player.setReversePlaying(false);
				} else {
					player.setPlaying(true);
					player.setReversePlaying(false);
				}
				// sent speed to the
			}
		});
		playButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		stepForwardButton.setToolTipText(NEXT_IMAGE_TEXT);
		updateButtonSize(stepForwardButton);
		stepForwardButton.addListener(SWT.Selection, event -> {
			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				int imgIndex1 = Math.min(player.getMaxDateIndex(),
						player.getDateIndex() + (int) player.getPlayingSpeed());
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						p.getPlayer().setPlaying(false);
						p.getPlayer().setReversePlaying(false);
						p.getPlayer().setDateIndex(imgIndex1);
					}
				}
			} else {
				player.setPlaying(false);
				player.setReversePlaying(false);

				int imgIndex2 = Math.min(player.getMaxDateIndex(),
						player.getDateIndex() + (int) player.getPlayingSpeed());
				player.setDateIndex(imgIndex2);
			}
		});
		stepForwardButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		remanentButton.setText(REMANENT_TEXT);
		remanentButton.setToolTipText(REMANENT_TEXT);
		remanentButton.setSelection(player.isRemanent());
		remanentButton.addListener(SWT.Selection, event -> {
			// case all plates synchronized
			if (isAllPlates()) {
				// case others composite not already created
				for (TectonicDefinition p : TectonicDataset.getInstance().getPlates()) {
					if (p != null) {
						p.getPlayer().setRemanent(remanentButton.getSelection());
						// redraw
						p.remanent();
					}
				}
				for (PoleRotationComposite composite : composites) {
					if (composite != null) {
						composite.remanentButton.setSelection(remanentButton.getSelection());
					}
				}
			} else {
				player.setRemanent(remanentButton.getSelection());
				// redraw
				tectonicDefinition.remanent();
			}
		});
		remanentButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		// video cpature checkbox initialisation and handler
		bVideoCapture.setText(CAPTURE_TEXT);
		bVideoCapture.setToolTipText(ANIMATION_CAPTURE_TEXT);
		bVideoCapture.addListener(SWT.Selection, event -> {

			// get the directory if videoCapture check box checked
			if (bVideoCapture.getSelection() == true) {
				DirectoryDialog directoryDialog = new DirectoryDialog(Display.getDefault().getActiveShell());
				// FfmpegDialog directoryDialog = new
				// FfmpegDialog(Display.getDefault().getActiveShell());
				if (!videoCaptureCurrentPath.isEmpty()) {
					directoryDialog.setFilterPath(videoCaptureCurrentPath);
				}
				videoCaptureDirectory = directoryDialog.open();

				if (videoCaptureDirectory == null) {
					bVideoCapture.setSelection(false);
				} else {
					// Save the current path
					File l_file = new File(videoCaptureDirectory);
					videoCaptureCurrentPath = l_file.getAbsolutePath();
					// Save the current image
					// videoCapture(player.getDateIndex(), 0);
					VideoCapture.start(player.getDateIndex(), videoCaptureDirectory);
				}
				headerNameFile = "";
			}

			// Set videoCapture check box state identic on all composite
			setVideoCaptureCheckboxOnAllComposite();

			// store the state
			videoCaptureEnable = bVideoCapture.getSelection();

		});
		bVideoCapture.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		referentielButton.setText(REFERENTIAL_TEXT);
		referentielButton.setToolTipText(REFERENTIAL_TEXT);
		referentielButton.setSelection(player.isReferentiel());
		referentielButton.addListener(SWT.Selection, event -> {
			// only one plate could be the reference
			for (PoleRotationComposite composite : composites) {
				if (composite != null && composite != PoleRotationComposite.this) {
					composite.referentielButton.setSelection(false);
					composite.player.setReferentiel(false);
				}
			}
			player.setReferentiel(referentielButton.getSelection());
		});
		referentielButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		return playerGroup;
	}

	/**
	 * Create the player group of this composite.
	 */
	protected void createSubsidenceGroup() {
		subsidenceComposite = new SubsidenceComposite(this, SWT.NONE, poleRotationModel);
		subsidenceComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	protected File chooseFile() {
		FileDialog dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
		dialog.setFilterNames(new String[] { "ASCII file with age/offset pairs" });
		dialog.setFilterExtensions(new String[] { "*.*" });

		File result = null;
		String filename = dialog.open();
		if (filename != null) {
			File file = new File(filename);
			if (file.exists() && file.length() > 0) {
				result = file;
			} else {
				displayError(getShell(), "Error", "Empty or invalid file");
			}
		}
		return result;
	}

	protected void displayError(Shell shell, String title, String message) {
		Display.getDefault().asyncExec(() -> {
			MessageDialog.openError(shell, title, message);
		});
	}

	/**
	 * Create the anomalies group of this composite.
	 */
	protected Group createAnomaliesGroup() {
		// Anomalies group
		final Group poleListGroup = new Group(this, SWT.NONE);
		poleListGroup.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		poleListGroup.setText(ANO_GROUP_NAME);
		poleListGroup.setLayout(new GridLayout(1, false));
		poleListGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// tabfolder
		CTabFolder tabfolder = new CTabFolder(poleListGroup, SWT.TOP);
		tabfolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		CTabItem totTab = new CTabItem(tabfolder, SWT.NONE);
		tabfolder.setSelection(totTab);
		totTab.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_totTab_text);
		CTabItem intTab = new CTabItem(tabfolder, SWT.NONE);
		intTab.setText(fr.ifremer.globe.placa3d.i18n.Messages.PoleRotationComposite_intTab_text);

		// tot group
		Composite totComposite = new Composite(tabfolder, SWT.NONE);
		totComposite.setLayout(new GridLayout(5, false));
		totTab.setControl(totComposite);
		totText = new StyledText(totComposite, SWT.READ_ONLY);
		new Label(totComposite, SWT.NONE);
		new Label(totComposite, SWT.NONE);
		new Label(totComposite, SWT.NONE);
		new Label(totComposite, SWT.NONE);

		// int group
		Composite intComposite = new Composite(tabfolder, SWT.NONE);
		intComposite.setLayout(new GridLayout(5, false));
		intTab.setControl(intComposite);
		intText = new StyledText(intComposite, SWT.READ_ONLY);
		new Label(intComposite, SWT.NONE);
		new Label(intComposite, SWT.NONE);
		new Label(intComposite, SWT.NONE);
		new Label(intComposite, SWT.NONE);

		updateListOfPoles();

		return poleListGroup;
	}

	/**
	 * Update the list of pole
	 */
	public void updateListOfPoles() {
		// Format for latitude
		DecimalFormat dfLat = new DecimalFormat("#.#####");
		dfLat.setMinimumFractionDigits(preferences.getNbDigitsLatitude().getValue());
		dfLat.setMaximumFractionDigits(preferences.getNbDigitsLatitude().getValue());
		// Format for longitude
		DecimalFormat dfLon = new DecimalFormat("#.#####");
		dfLon.setMinimumFractionDigits(preferences.getNbDigitsLongitude().getValue());
		dfLon.setMaximumFractionDigits(preferences.getNbDigitsLongitude().getValue());
		// Format for angle
		DecimalFormat dfAngle = new DecimalFormat("#.#####");
		dfAngle.setMinimumFractionDigits(preferences.getNbDigitsAngle().getValue());
		dfAngle.setMaximumFractionDigits(preferences.getNbDigitsAngle().getValue());
		// Format for age
		DecimalFormat dfAge = new DecimalFormat("#.#####");
		dfAge.setMinimumFractionDigits(preferences.getNbDigitsAge().getValue());
		dfAge.setMaximumFractionDigits(preferences.getNbDigitsAge().getValue());

		MagneticAnomalies ma = MagneticAnomalies.getInstance();

		String space = "   ";
		// Première ligne : nom des colonnes : nom_ano, age, lat, long, angle
		String firstLine = "nom_ano" + space + "age" + space + "lat" + space + "long" + space + "angle";

		// TOTAL POLE COMPOSITE

		StringBuilder totSb = new StringBuilder();
		PoleRotations totPole = tectonicDefinition.getPoleRotationLayer().getMovement().getTotalPoleRotation();

		// Première ligne
		totSb.append(firstLine);
		totSb.append(Text.DELIMITER);

		for (int i = 0; i < totPole.getSDates().size(); i++) {
			if (totPole.getSEndDates().get(i) != null) {
				totSb.append(ma.getAnomaliesTreeMap().get(ma.getDoubleAnomaly(totPole.getSEndDates().get(i))));
				totSb.append(space);
				totSb.append(String.valueOf(dfAge.format(ma.getDoubleAnomaly(totPole.getSEndDates().get(i)))));
				totSb.append(space);
				totSb.append(String.valueOf(dfLat.format(totPole.getLatitudes().get(i))));
				totSb.append(space);
				totSb.append(String.valueOf(dfLon.format(totPole.getLongitudes().get(i))));
				totSb.append(space);
				totSb.append(String.valueOf(dfAngle.format(totPole.getAngles().get(i))));
				totSb.append(Text.DELIMITER);
			}
		}
		totText.setText(totSb.toString());

		// INTERMEDIAIRE POLE COMPOSITE
		StringBuilder intSb = new StringBuilder();
		PoleRotations intPole = tectonicDefinition.getPoleRotationLayer().getMovement().getPoleRotation();

		// Première ligne
		intSb.append(firstLine);
		intSb.append(Text.DELIMITER);

		for (int i = 0; i < intPole.getSDates().size(); i++) {
			Double startDate = ma.getDoubleAnomaly(intPole.getSDates().get(i));
			Double endDate = ma.getDoubleAnomaly(intPole.getSEndDates().get(i));

			String startAno = ma.getAnomaliesTreeMap().get(startDate);
			startAno = startAno != null ? startAno : "NoName";
			String endAno = ma.getAnomaliesTreeMap().get(endDate);
			endAno = endAno != null ? endAno : "NoName";

			intSb.append(startAno + "-" + endAno);
			intSb.append(space);
			intSb.append(String.valueOf(dfAge.format(startDate) + "-" + String.valueOf(endDate)));
			intSb.append(space);
			intSb.append(String.valueOf(dfLat.format(intPole.getLatitudes().get(i))));
			intSb.append(space);
			intSb.append(String.valueOf(dfLon.format(intPole.getLongitudes().get(i))));
			intSb.append(space);
			intSb.append(String.valueOf(dfAngle.format(intPole.getAngles().get(i))));
			intSb.append(Text.DELIMITER);
		}
		intText.setText(intSb.toString());

		// Refresh parent layout
		totText.getParent().layout();
	}

	/**
	 * Create the color group of this composite.
	 */
	protected Group createColorGroup() {
		// color group
		final Group colorGroup = new Group(this, SWT.NONE);
		colorGroup.setText(COLOR_GROUP_NAME);
		colorGroup.setLayout(new GridLayout(2, false));
		colorGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Label colorLabel = new Label(colorGroup, SWT.BORDER);
		GridDataFactory.fillDefaults().hint(50, SWT.DEFAULT).applyTo(colorLabel);
		// restore the color of the last save session if exists
		Color color = tectonicDefinition.getPoleRotationLayer().getColor();
		colorLabel.setBackground(ColorUtils.convertAWTToSWT(color));

		colorButton = new Button(colorGroup, SWT.NONE);
		colorButton.setText(CHANGE_COLOR);
		colorButton.setToolTipText(CHANGE_COLOR_TOOLTIP);
		colorButton.addListener(SWT.Selection, event -> {
			ColorDialog colorDialog = new ColorDialog(Display.getDefault().getActiveShell(), SWT.NONE);
			colorDialog.setText("Change color");
			colorDialog.setRGB(colorLabel.getBackground().getRGB());

			RGB rgb1 = colorDialog.open();
			if (rgb1 != null) {
				colorLabel.setBackground(new org.eclipse.swt.graphics.Color(colorLabel.getDisplay(), rgb1));
				tectonicDefinition.getPoleRotationLayer().setColor(new Color(rgb1.red, rgb1.green, rgb1.blue));
				Viewer3D.getWwd().redraw();
			}
		});

		return colorGroup;
	}

	protected void updateList() {

		StringBuffer layerStoreName = null;

		layersList.removeAll();
		layersMap.clear();

		List<LayerStore> layerStoreList = tectonicDefinition.getLayerStoreList();

		for (LayerStore layerStore : layerStoreList) {
			layerStoreName = new StringBuffer();
			for (Pair<Layer, ElevationModel> pair : layerStore.getLayers()) {
				Layer layer = pair.getFirst();
				layerStoreName.append(" " + layer.getName());
			}
			layersList.add(layerStoreName.toString());
			layersMap.put(layerStoreName.toString(), layerStore);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// Object oldValue = event.getOldValue(); //never used

		if (TectonicPlayerModel.PROPERTY_DATEINDEX.equals(event.getPropertyName())) {
			final int imgIndex = (Integer) event.getNewValue();

			// Test if max 200 reached or min O reached to change the button's
			// state
			if (imgIndex == Activator.getPreferences().getMaximumAge().getValue()) {
				if (playing == true) {
					Display.getDefault().asyncExec(() -> {
						playButton.setSelection(false);
						playButton.setImage(ImageResources.getImage(ICON_MEDIA_PLAYBACK_START, getClass()));
					});
				}
			}
			if (imgIndex == 0) {
				if (reverseplaying == true) {
					Display.getDefault().asyncExec(() -> {
						reverseButton.setSelection(false);
						reverseButton.setImage(ImageResources.getImage(ICON_MEDIA_REVERSE_START, getClass()));
					});
				}
			}

			// video capture if requested
			if (videoCaptureEnable == true) {
				VideoCapture.start(imgIndex, videoCaptureDirectory);
				// videoCapture(imgIndex, videoImageNb);
				videoImageNb++;
			}

			// Update components
			Display.getDefault().asyncExec(() -> {
				if (currentIndexSpinner.getSelection() != imgIndex) { // fix
																		// focus
																		// problem
					currentIndexSpinner.setSelection(imgIndex);
				}
				indexSlider.setSelection(imgIndex);
			});

		} else if (TectonicPlayerModel.PROPERTY_OFFSET.equals(event.getPropertyName())) {
			poleRotationModel.getConstantOffset().getRealm().asyncExec(() -> poleRotationModel.getConstantOffset()
					.set(PoleRotationComposite.this.player.getApproxElevationOffset()));
		} else if (TectonicPlayerModel.PROPERTY_PLAYING.equals(event.getPropertyName())) {
			playing = (Boolean) event.getNewValue();
			// reset the snapshot check box
			if (playing == false) {
				Display.getDefault().syncExec(() -> disableCaptureVideo());
			}

			// Update gui compoenets
			Display.getDefault().asyncExec(() -> {
				if (playing) {
					playButton.setSelection(true);
					playButton.setImage(ImageResources.getImage(ICON_MEDIA_PLAYBACK_PAUSE, getClass()));

					reverseButton.setSelection(false);
					reverseButton.setImage(ImageResources.getImage(ICON_MEDIA_REVERSE_START, getClass()));
				} else {
					playButton.setSelection(false);
					playButton.setImage(ImageResources.getImage(ICON_MEDIA_PLAYBACK_START, getClass()));
				}
			});
		} else if (TectonicPlayerModel.PROPERTY_REVERSEPLAYING.equals(event.getPropertyName())) {
			reverseplaying = (Boolean) event.getNewValue();
			// reset the snapshot check box
			if (reverseplaying == false) {
				Display.getDefault().syncExec(() -> {
					disableCaptureVideo();
				});
			}

			Display.getDefault().asyncExec(() -> {
				if (reverseplaying) {
					reverseButton.setSelection(true);
					reverseButton.setImage(ImageResources.getImage(ICON_MEDIA_PLAYBACK_PAUSE, getClass()));

					playButton.setSelection(false);
					playButton.setImage(ImageResources.getImage(ICON_MEDIA_PLAYBACK_START, getClass()));
				} else {
					reverseButton.setSelection(false);
					reverseButton.setImage(ImageResources.getImage(ICON_MEDIA_REVERSE_START, getClass()));
				}
			});
		}
	}

	/** React on a layer model changes */
	protected void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		if (event.getState() != FileLayerStoreState.REMOVED) {
			layersList.getDisplay().asyncExec(this::updateList);
		}
	}

	/**
	 * Make a screen capture.
	 *
	 * @param a_imgIndex Image index in million of years
	 * @return No return.
	 */
	protected void videoCapture(int a_imgIndex, int a_videoImageNb) {

		String l_screenShotFile;
		String l_imageNb;

		// Initialize the header file name to store
		if (headerNameFile == "") {
			final DateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH-mm-ss");
			headerNameFile = formatter.format(new Date());
		}

		// make the capture and store
		l_imageNb = String.format("%03d", a_imgIndex);
		l_screenShotFile = videoCaptureDirectory + "/" + "img-" + l_imageNb + ".png";
		new OGLScreenshot(Viewer3D.getWwd(), l_screenShotFile);
	}

	protected void disableCaptureVideo() {

		if (bVideoCapture.getSelection()) {
			FfmpegUtils.encodeVideo(getShell(), "Placa", new File(videoCaptureDirectory), null);
		}
		bVideoCapture.setSelection(false);
		setVideoCaptureCheckboxOnAllComposite();
		videoCaptureEnable = false;
		videoCaptureDirectory = "";
		headerNameFile = "";
		videoImageNb = 1;
	}

	/**
	 * propagate VideoCapture Checkbox OnAll Composite
	 *
	 * @param a_imgIndex Image index in million of years
	 * @return No return.
	 */
	protected void setVideoCaptureCheckboxOnAllComposite() {
		for (PoleRotationComposite composite : composites) {
			if (composite != null && composite != PoleRotationComposite.this) {
				composite.bVideoCapture.setSelection(bVideoCapture.getSelection());
			}
		}
	}

	/*
	 * Getter Setter
	 */
	public static List<PoleRotationComposite> getPlayersList() {
		return composites;
	}

	public TectonicPlayerModel getPlayer() {
		return player;
	}

	public static float getLastSpeed() {
		return lastSpeed;
	}

	public Button getRepeatButton() {
		return remanentButton;
	}

	public Button getReferentielButton() {
		return referentielButton;
	}

	/**
	 * When a new layer store is added to the TectonicDefinition represented by this composite, this method is invoked
	 * in order to update the list of layers.
	 *
	 * @param event The modification event.
	 */
	@Inject
	@Optional
	protected void onLayerStoreAdded(
			@UIEventTopic(PlacaEventTopics.TOPIC_LAYER_STORE_ADDED) LayerStoreAddedEvent event) {
		if (event.tectonicDefinition() == tectonicDefinition) {
			updateList();
		}
	}

	/**
	 * On mac os button size is badly update when added to an icons, this is a workaround for this behaviour
	 */
	private void updateButtonSize(Button button) {
		if (OSUtils.isMac()) {
			Image img = button.getImage();
			button.setSize(img.getBounds().width, img.getBounds().height);
		}
	}

	/**
	 * Model of subsidence parameters
	 */
	public static class PoleRotationModel implements SubsidenceModel {

		protected WritableObject<OffsetMode> offsetMode = new WritableObject<>(OffsetMode.OFF);
		protected WritableFile offsetFile = new WritableFile();
		protected WritableNumber constantOffset = new WritableNumber(0);

		/**
		 * Getter of offsetMode
		 */
		@Override
		public WritableObject<OffsetMode> getOffsetMode() {
			return offsetMode;
		}

		/**
		 * Getter of offsetFile
		 */
		@Override
		public WritableFile getOffsetFile() {
			return offsetFile;
		}

		/**
		 * Getter of constantOffset
		 */
		@Override
		public WritableNumber getConstantOffset() {
			return constantOffset;
		}
	}

}
