package fr.ifremer.globe.placa3d.composite;

import jakarta.inject.Inject;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.IPoleRotationFile;
import fr.ifremer.globe.placa3d.file.intpol.IntermediatePoleFile;
import fr.ifremer.globe.placa3d.file.totpol.TotalPoleFile;
import fr.ifremer.globe.placa3d.layers.PoleRotationLayer;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;

/***
 * IHM to change tectonic plates date allowing to move plates according to geologic era for pole of rotation of tec
 *
 * @author pmahoudo
 *
 */
public class PoleRotationForTecComposite extends PoleRotationComposite {

	@Inject
	private IFileLoadingService fileLoadingService;

	private final PoleRotationLayer layer;

	public PoleRotationForTecComposite(Composite parent, int style, PoleRotationLayer layer,
			TectonicDefinition tectonicDefinition) {
		super(parent, style, tectonicDefinition);
		this.layer = layer;
	}

	/**
	 * @see fr.ifremer.globe.placa3d.composite.PoleRotationComposite#createAdditionalGroup()
	 */
	@Override
	protected void createAdditionalGroup() {
		Button modifyPoleButton = new Button(this, SWT.TOGGLE);
		modifyPoleButton.setText("Change pole");
		modifyPoleButton.addListener(SWT.Selection, event -> {

			final Shell shell = Display.getCurrent().getActiveShell();
			FileDialog fileDialog = new FileDialog(shell, SWT.SINGLE | SWT.OPEN);
			fileDialog.setText("Load new poles file");
			fileDialog.setFilterNames(new String[] { "*.tot", "*.int" });
			fileDialog.setFilterExtensions(new String[] { "*.tot", "*.int" });

			String fileName = fileDialog.open();

			if (fileName != null) {

				IPoleRotationFile poleFile = null;
				TectonicMovement movement = null;

				if (fileName.toLowerCase().endsWith("tot")) {
					poleFile = new TotalPoleFile(fileName);
				} else if (fileName.toLowerCase().endsWith("int")) {
					poleFile = new IntermediatePoleFile(fileName);
				}

				// Extract pole rotations data
				if (poleFile != null) {
					try {
						movement = poleFile.read();
					} catch (Exception e1) {
						Display.getDefault().asyncExec(() -> MessageDialog.openError(shell, "Reading pole file error",
								"Error: the pole file can not be read"));
					}
				}

				if (movement != null) {
					for (WWFileLayerStore fileLayerStore : fileLayerStoreModel.getAll()) {
						if (fileLayerStore.findLayer(layer.getName(), layer.getClass()).isPresent()) {
							IFileInfo fileInfo = fileLayerStore.getFileInfo();
							// Write pole data in tecfile
							try {
								TectonicModel tectonicModel = tectonicDefinition.getTectonicModel();
								TectonicPlateModel plateModel = tectonicDefinition.getTectonicPlateModel();
								TectonicUtils.writePoleRotationsInTecFile(tectonicModel, plateModel, movement,
										fileInfo.getPath());
							} catch (Exception e2) {
								Display.getDefault().asyncExec(
										() -> MessageDialog.openError(shell, "Error during writing", e2.getMessage()));

							}

							// Reload file
							fileLoadingService.reload(fileInfo.toFile());
						}
					}

				}

			}
		});
	}

}