package fr.ifremer.globe.placa3d.composite;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.placa3d.application.context.ContextInitializer;
import fr.ifremer.globe.placa3d.layers.PoleRotationLayer;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.viewer3d.util.BasicParametersViewCompositeFactory;

/***
 * Creates a Tectonic Composite if survey defines a tectonic plate (associated with a cot file)
 */
@Component(name = "globe_placa_pole_rotation_composite_factory", service = IParametersViewCompositeFactory.class)
public class PoleRotationCompositeFactory extends BasicParametersViewCompositeFactory<PoleRotationLayer> {

	public PoleRotationCompositeFactory() {
		super(ContentType.POLE_TOT, PoleRotationLayer.class);
	}

	@Override
	protected Composite makeComposite(Composite parent, PoleRotationLayer layer) {

		Optional<TectonicDefinition> tectonicDefinition = TectonicDataset.getInstance().getPlates().stream()
				.filter(td -> td.getPoleRotationLayer().equals(layer)).findFirst();
		if (tectonicDefinition.isPresent()) {
			var result = new PoleRotationComposite(parent, SWT.NONE, tectonicDefinition.get());
			ContextInitializer.inject(result);
			return result;
		}
		return null;
	}

}
