package fr.ifremer.globe.placa3d.export;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.export.dialog.AgeSetting;

/**
 * Repository of dates concerned by the MTEC exportation according to the user
 * settings.
 */
public class DatesRepository {

	private AgeSetting ageSetting;
	private int currentAge;
	private int customAge;
	private double startAge;
	private double endAge;
	private int ageStep;

	private ArrayList<Double> realAges;
	private ArrayList<Integer> playerAges;

	/**
	 * Constructor of {@link DatesRepository}.
	 * 
	 * @param ageSetting
	 *            the type of age choosen by the user
	 * @param currentAge
	 *            the current age choosen by the user
	 * @param startAge
	 *            the start age choosen by the user
	 * @param endAge
	 *            the and age choosen by the user
	 * @param customAge
	 *            the custom age choosen by the user
	 * @param ageStep
	 *            the age step choosen by the user
	 */
	public DatesRepository(AgeSetting ageSetting, int currentAge, double startAge, double endAge, int customAge, int ageStep) {

		this.ageSetting = ageSetting;
		if (ageSetting == AgeSetting.Current) {
			this.currentAge = currentAge;
		} else if (ageSetting == AgeSetting.Custom) {
			this.customAge = customAge;
		} else {
			this.startAge = startAge;
			this.endAge = endAge;
			this.ageStep = ageStep;
		}

		this.realAges = new ArrayList<Double>();
		this.playerAges = new ArrayList<Integer>();
	}

	/**
	 * Compute all the dates to export from the user settings.
	 * 
	 * @param tectonicPlateModel
	 *            tectonic plate model which contains differents elevation dates
	 */
	public void initializeAgesToCompute(TectonicPlateModel tectonicPlateModel) {
		// Get all the differents elevation dates
		List<Double> endDates = tectonicPlateModel.getMovement().getTotalPoleRotation().getEndDates();

		if (this.ageSetting == AgeSetting.Current) {
			// Compute the player and real age in case of current date
			// exportation
			for (int i = 1; i < endDates.size(); i++) {
				if (this.currentAge >= endDates.get(i - 1) && this.currentAge <= endDates.get(i)) {
					this.realAges.add(endDates.get(i - 1));
					this.playerAges.add(this.currentAge);
					break;
				}
			}
			if (currentAge > endDates.get(endDates.size() - 1)) {
				this.realAges.add(endDates.get(endDates.size() - 1));
				this.playerAges.add(currentAge);
			}
		} else if (this.ageSetting == AgeSetting.Custom) {
			// Compute the player and real age in case of custom date
			// exportation
			for (int i = 1; i < endDates.size(); i++) {
				if (this.customAge >= endDates.get(i - 1) && this.customAge <= endDates.get(i)) {
					this.realAges.add(endDates.get(i - 1));
					this.playerAges.add(this.customAge);
					break;
				}
			}
			if (customAge > endDates.get(endDates.size() - 1)) {
				this.realAges.add(endDates.get(endDates.size() - 1));
				this.playerAges.add(customAge);
			}
		} else {
			for (int currentDate = (int) this.startAge; currentDate <= Math.ceil(this.endAge); currentDate += this.ageStep) {
				// Compute the player and real ages in case of array of dates
				// exportation
				for (int i = 1; i < endDates.size(); i++) {
					if (currentDate >= endDates.get(i - 1) && currentDate <= endDates.get(i)) {
						this.realAges.add(endDates.get(i - 1));
						this.playerAges.add(currentDate);
						break;
					}
				}
				if (currentDate > endDates.get(endDates.size() - 1)) {
					this.realAges.add(endDates.get(endDates.size() - 1));
					this.playerAges.add(currentDate);
				}
			}
		}
	}

	/**
	 * @return the realAges
	 */
	public List<Double> getRealAges() {
		return this.realAges;
	}

	/**
	 * @return the playerAges
	 */
	public List<Integer> getPlayerAges() {
		return this.playerAges;
	}
}
