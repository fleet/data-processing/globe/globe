/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.export;

import java.text.DecimalFormat;

/**
 * 
 */
public class FileExtensionFormatter {

	/** .texture.tif */
	public static enum EXTENSION {
		/** .texture.tif */
		TEXTURE_EXTENSION("_texture.tif"),
		/** .elevation.tif */
		ELEVATION_EXTENSION("_elevation.tif"),
		/** .xyz */
		XYZ_EXTENSION(".xyz");

		/** Extension value */
		protected String value;

		/**
		 * Constructor
		 */
		private EXTENSION(String value) {
			this.value = value;
		}

		/**
		 * Getter of value
		 */
		public String getValue() {
			return value;
		}
	}

	/** Age formatter */
	protected DecimalFormat formatter = new DecimalFormat("0.###");

	/**
	 * @return the xyz file name for storing the elevation at the specified age.
	 */
	public String format(int age, EXTENSION extension) {
		return formatter.format(age) + extension.getValue();
	}
}
