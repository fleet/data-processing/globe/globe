package fr.ifremer.globe.placa3d.export;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconst;
import org.gdal.osr.osrConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.TectonicLayer;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.export.FileExtensionFormatter.EXTENSION;
import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import fr.ifremer.globe.placa3d.info.MtecInfo;
import fr.ifremer.globe.placa3d.morphing.ElevationMorphingManager;
import fr.ifremer.globe.placa3d.morphing.MorphingManager;
import fr.ifremer.globe.placa3d.reader.MultiLayerTecFileReader;
import fr.ifremer.globe.placa3d.store.TectonicPlateLayerStore;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.ui.utils.image.ImageUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer.OutputFileNameComputerModel;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.viewer3d.data.reader.raster.Raster;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

/**
 * Provide an export feature from MTEC to XYZ file. It results a file which contains latitude, longitude and elevation
 * data saved below an ascii format.
 */
public class MtecElevationExporter {

	/** Logger. */
	protected static Logger logger = LoggerFactory.getLogger(MtecElevationExporter.class);

	/**
	 * Export the {@link FileInfoNode} attribute of {@link MtecElevationExporter}
	 *
	 *
	 * @param parameters all usefull parameters
	 * @param monitor progress monitor
	 * @param nbTicks number of ticks to use for the progression
	 * @throws Exception error occurs
	 */
	public void export(MtecExportParameters parameters, IProgressMonitor monitor) throws Exception {
		// Get the tectonic model contained in the input MTEC file
		TectonicPlateLayerStore tectonicPlateLayerStore = ((MtecInfo) parameters.getFileToTranscode().getFileInfo())
				.getPlateLayerStores().iterator().next();
		parameters.setTectonicPlateLayerStore(tectonicPlateLayerStore);

		initializeElevationData(parameters);
		if (parameters.getElevationData() != null) {
			initializeAgeToExport(parameters);
			doExport(parameters, monitor);
		}
		monitor.done();
	}

	/**
	 * Perform the Export
	 *
	 * @param parameters all usefull parameters
	 * @param monitor progress monitor
	 * @throws IOException error occurs
	 */
	protected void doExport(MtecExportParameters parameters, IProgressMonitor monitor) throws IOException {
		int nbTicks = 20 + 12 * parameters.getDatesRepository().getPlayerAges().size();
		SubMonitor subMonitor = SubMonitor.convert(monitor, nbTicks);

		IArrayFactory arrayFactory = IArrayFactory.grab();

		// Use MorphingManager to extract elevation and compute intermediate
		// grids
		List<ElevationMorphingManager> elevationMorphingManagers = new ArrayList<>();
		try (MorphingManager morphingManager = new MorphingManager(parameters.getTectonicModel(),
				parameters.getTectonicPlateModel())) {
			morphingManager.setCheckNasaCache(false); // Need all files
			morphingManager.setProcessTexture(false); // Don't need texture
			morphingManager.setExtractMaskGrid(true); // Ask to also extract
														// masking grids
			morphingManager.extractGrids(parameters.getElevationData(), Area.AREA_ALL, null, elevationMorphingManagers);
			subMonitor.worked(10);

			if (Activator.getPreferences().getMorphing().getValue().booleanValue()) {
				// Generate morphing data
				morphingManager.computeIntermediateData(parameters.getElevationData(), Area.AREA_ALL, null,
						elevationMorphingManagers, subMonitor.split(10));
			} else {
				subMonitor.worked(10);
			}

			for (Integer age : parameters.getDatesRepository().getPlayerAges()) {
				MaskingGrid maskingGrid = new MaskingGrid();
				File maskingGridFile = searchMaskGridFile(parameters.getTectonicPlateModel(), morphingManager, age);
				if (maskingGridFile != null) {
					maskingGrid.loadMaskValues(maskingGridFile);
				}
				subMonitor.worked(2);

				File elevationFile = searchElevationFile(parameters.getTectonicPlateModel(), morphingManager, age);
				if (elevationFile != null) {
					try (IDoubleArray elevationsGrid = arrayFactory.openDoubleArray(elevationFile, false)) {
						elevationsGrid.setByteOrder(ByteOrder.nativeOrder());

						ElevationSet rotatedElevations = transformGridToAge(elevationsGrid, maskingGrid, parameters,
								age);
						subMonitor.worked(2);

						Dataset newElevationGrid = createGrid(rotatedElevations, parameters, age);
						subMonitor.worked(2);

						if (newElevationGrid != null) {
							if (parameters.isOutXyzFormat()) {
								writeXYZFile(newElevationGrid, parameters, age);
							}
							subMonitor.worked(2);

							if (parameters.isOutTiffFormat()) {
								writeElevationAsTiff(newElevationGrid, parameters, age);
							}
							subMonitor.worked(2);

							if (parameters.isOutTextureFormat()) {
								writeElevationAsTexture(newElevationGrid, parameters, age);
							}
							GdalUtils.asyncDeleteDataset(newElevationGrid);
							subMonitor.worked(2);
						} else {
							subMonitor.worked(6);
						}
					}
				} else {
					subMonitor.worked(10);
				}
			}
		} finally {
			elevationMorphingManagers.forEach(IOUtils::closeQuietly);
		}
	}

	/** Export Dataset as an elevation tiff. */
	protected void writeElevationAsTiff(Dataset dataset, MtecExportParameters parameters, int age) {
		FileExtensionFormatter fileExtensionFormatter = new FileExtensionFormatter();
		File outFile = parameters.getOutputFileNameComputer().compute(parameters.getFileToTranscode().getFile(),
				parameters.getOutputFileNameComputerModel(),
				fileExtensionFormatter.format(age, EXTENSION.ELEVATION_EXTENSION));

		if (!outFile.exists() || parameters.isOverwriteExistingFiles()) {
			GdalUtils.translate(dataset, GdalUtils.TIF_DRIVER_NAME, outFile.getAbsolutePath());
		}
	}

	/** Export Dataset as RGB tiff. */
	protected void writeElevationAsTexture(Dataset dataset, MtecExportParameters parameters, int age)
			throws IOException {
		FileExtensionFormatter fileExtensionFormatter = new FileExtensionFormatter();
		File outFile = parameters.getOutputFileNameComputer().compute(parameters.getFileToTranscode().getFile(),
				parameters.getOutputFileNameComputerModel(),
				fileExtensionFormatter.format(age, EXTENSION.TEXTURE_EXTENSION));

		if (!outFile.exists() || parameters.isOverwriteExistingFiles()) {
			Band band = dataset.GetRasterBand(1);
			DoubleRange minMax = GdalUtils.getMinMaxValues(band, false);
			double[] lineValues = new double[band.getXSize()];
			BufferedImage image = ImageUtils.makeImage(band.getXSize(), band.getYSize(), minMax, Raster.NO_VALUE,
					"GMT_jet", (line, column) -> {
						if (column == 0) {
							// New line : read data
							band.ReadRaster(0, line, lineValues.length, 1, lineValues);
						}
						return lineValues[column];
					});
			band.delete();

			double[] boundingBox = GdalUtils.getBoundingBox(dataset);
			AVList params = new AVListImpl();
			params.setValue(AVKey.SECTOR,
					Sector.fromDegrees(boundingBox[1], boundingBox[0], boundingBox[3], boundingBox[2]));
			ImageUtils.writeAsTiff(image, params, outFile);
		}
	}

	/**
	 * Search the elevation file corresponding to the specified age.
	 */
	protected File searchElevationFile(TectonicPlateModel tectonicPlateModel, MorphingManager morphingManager,
			int age) {
		// Is there an elevation file corresponding to the age
		File elevationFile = morphingManager.getIntermediateElevations(age);
		if (elevationFile == null) {
			// Is there an elevation file corresponding to the TectonicLayer
			TectonicLayer tectonicLayer = tectonicPlateModel.getTectonicLayer(age);
			if (tectonicLayer != null) {
				elevationFile = morphingManager.getIntermediateElevations((int) tectonicLayer.getMinimumDouble());
			}
			if (elevationFile == null) {
				// Take the elevation file to age 0 by default
				elevationFile = morphingManager.getIntermediateElevations(0);
			}
		}
		return elevationFile;
	}

	/**
	 * Search the masking grid file corresponding to the specified age.
	 */
	protected File searchMaskGridFile(TectonicPlateModel tectonicPlateModel, MorphingManager morphingManager, int age) {
		// Is there an elevation file corresponding to the age
		File maskGridFile = morphingManager.getMaskingGrid(age);
		if (maskGridFile == null) {
			// Is there an elevation file corresponding to the TectonicLayer
			TectonicLayer tectonicLayer = tectonicPlateModel.getTectonicLayer(age);
			if (tectonicLayer != null) {
				maskGridFile = morphingManager.getMaskingGrid((int) tectonicLayer.getMinimumDouble());
			}
			if (maskGridFile == null) {
				// Take the elevation file to age 0 by default
				maskGridFile = morphingManager.getMaskingGrid(0);
			}
		}
		return maskGridFile;
	}

	/**
	 * Use grid function of Gdal to create regular grid from the scattered elevations
	 *
	 * @param elevationSet scattered elevations
	 * @param parameters all usefull parameters
	 * @param age age to export
	 * @return Gdal Dataset of the new Grid
	 */
	protected Dataset createGrid(ElevationSet elevationSet, MtecExportParameters parameters, int age) {
		Dataset result = null;

		// Retrieve grid size
		int height = parameters.getTectonicPlateModel().getElevationGridDimension().height;
		int width = parameters.getTectonicPlateModel().getElevationGridDimension().width;
		// Create a new grid corresponding to the rotated position.
		int gridSize = Math.max(height, width);
		// divide as soon as possible to prevent going to negatives values (seen around artic plates)
		int bufferSize = gridSize * gridSize / 8 * gdal.GetDataTypeSize(gdalconst.GDT_Float32);
		ByteBuffer newElevationGrid = ByteBuffer.allocateDirect(bufferSize);
		if (gdal.GridCreate("nearest:radius1=0.1:radius2=0.1:angle=0.0:nodata=" + Raster.NO_VALUE, elevationSet.xyz,
				elevationSet.minLongitude, elevationSet.maxLongitude, elevationSet.maxLatitude,
				elevationSet.minLatitude, width, height, gdalconst.GDT_Float32, newElevationGrid) == GdalUtils.OK) {
			result = GdalUtils.createDataset(gridSize, gridSize, gdalconst.GDT_Float32, gdalconst.GCI_GrayIndex);
			if (result != null) {
				result.SetProjection(osrConstants.SRS_WKT_WGS84_LAT_LONG);
				Band band = result.GetRasterBand(1);
				band.SetNoDataValue(Raster.NO_VALUE);
				GdalUtils.setBoundingBox(result, elevationSet.maxLatitude, elevationSet.minLatitude,
						elevationSet.minLongitude, elevationSet.maxLongitude);
				band.WriteRaster_Direct(0, 0, gridSize, gridSize, width, height, gdalconst.GDT_Float32,
						newElevationGrid);
				double[] min = { 0d };
				double[] max = { 0d };
				double[] mean = { 0d };
				double[] stddev = { 0d };	
				if(band.ComputeStatistics(false, min, max, mean, stddev) == gdalconst.CE_None) {
					band.SetStatistics(min[0], max[0], mean[0], stddev[0]);
				}

				result.FlushCache();
				band.delete();
			} else if (logger.isWarnEnabled()) {
				logger.warn("Error while exporting (Dataset creation failed) : {}", gdal.GetLastErrorMsg());
			}
		} else if (logger.isWarnEnabled()) {
			logger.warn("Error while exporting (gdal.GridCreate failed) : {}", gdal.GetLastErrorMsg());
		}
		return result;
	}

	/**
	 * Apply the movement to the elevation grid at the specified age. Also apply the subsidence on all elevations
	 */
	protected ElevationSet transformGridToAge(IDoubleArray elevationsGrid, MaskingGrid maskingGrid,
			MtecExportParameters parameters, int age) {
		Sector sector = parameters.getTectonicPlateModel().getGeoBox();
		TectonicMovement movement = parameters.getTectonicPlateModel().getMovement();

		// Retrieve grid size
		int height = (int) elevationsGrid.getDouble(0);
		int width = (int) elevationsGrid.getDouble(1);

		ElevationSet result = new ElevationSet(height * width);
		// Convert the elevationsGrid to a XYZ array
		// Age rotation is apply to all XY points
		for (int line = 0; line < height; line++) {
			for (int column = 0; column < width; column++) {
				int pointIndex = column + line * width;
				double elevation = elevationsGrid.getDouble(2 + pointIndex);
				// Ignore nodata value
				if (elevation != TecGeneratorUtil.MISSING_VALUE) {
					// X/Y to Lon/Lat
					double latitude = sector.getMaxLatitude().getDegrees()
							- sector.getDeltaLatDegrees() / height * line;
					double longitude = sector.getMinLongitude().getDegrees()
							+ sector.getDeltaLonDegrees() / width * column;

					// Subsidence
					int maskingAge = maskingGrid.getAge(longitude, latitude);
					// Apply Masking grid

					boolean filter = false;
					if (maskingGrid.toXYTransformation != null) {
						// due to projection and aged grid not of the same resolution, we could have border effect,
						// in such a case we have a No_value returned
						if (maskingAge == Raster.NO_VALUE || age > maskingAge) {
							filter = true;
						}
					}
					if (!filter) {
						double offset = computeOffset(parameters, age, maskingAge);
						elevation += offset;
						LatLon position = movement.rotate(Angle.fromDegrees(latitude), Angle.fromDegrees(longitude),
								false, age);
						// Add elevation
						result.addElevation(position.getLongitude().degrees, position.getLatitude().degrees, elevation);
					}
				}
			}
		}

		// Reduce the array to the number of position
		result.truncate();

		return result;
	}

	/**
	 * Compute all the dates to export in {@link MtecExportParameters#datesRepository}.
	 */
	protected void initializeAgeToExport(MtecExportParameters parameters) {
		parameters.getDatesRepository().initializeAgesToCompute(parameters.getTectonicPlateModel());
	}

	/**
	 * Initialize the elevation data {@link MultiLayerTecImageElevationData} in parameters ie
	 * {@link MtecExportParameters#elevationData}.
	 */
	protected void initializeElevationData(MtecExportParameters parameters) throws Exception {
		File file = new File(parameters.getTectonicPlateLayerStore().getFile().getAbsolutePath());
		try (MultiLayerTecFileReader reader = new MultiLayerTecFileReader(file)) {
			reader.open();
			TectonicModel tectonicModel = new TectonicModel();
			reader.loadMetadata(tectonicModel);
			// Only one plate in a MTEC
			List<TectonicPlateModel> plates = tectonicModel.getPlates();
			if (!plates.isEmpty()) {
				parameters.setElevationData(new MultiLayerTecImageElevationData(file, tectonicModel, plates.get(0)));
				parameters.setTectonicModel(parameters.getElevationData().getTectonicModel());
				parameters.setTectonicPlateModel(parameters.getElevationData().getPlate());
			}
		}
	}

	/**
	 * Serialize the dataset to xyz.
	 */
	protected void writeXYZFile(Dataset dataset, MtecExportParameters parameters, int age) throws IOException {
		FileExtensionFormatter fileExtensionFormatter = new FileExtensionFormatter();
		File outFile = parameters.getOutputFileNameComputer().compute(parameters.getFileToTranscode().getFile(),
				parameters.getOutputFileNameComputerModel(),
				fileExtensionFormatter.format(age, EXTENSION.XYZ_EXTENSION));

		if (!outFile.exists() || parameters.isOverwriteExistingFiles()) {
			FileUtils.deleteQuietly(outFile);
			// Generate the text content of the XYZ file
			writeXYZFile(dataset, outFile);
		}
	}

	/**
	 * Serialize the dataset to xyz. Can't use GDAL because it doesn't filter the noData
	 */
	protected void writeXYZFile(Dataset dataset, File outFile) throws IOException {
		try (BufferedWriter bufferedWriter = Files.newBufferedWriter(outFile.toPath())) {
			Band band = dataset.GetRasterBand(1);
			int lineCount = band.GetYSize();
			int columnCount = band.GetXSize();
			double[] result = new double[columnCount];
			double[] coords = new double[2];
			for (int line = 0; line < lineCount; line++) {
				if (band.ReadRaster(0, line, columnCount, 1, result) == GdalUtils.OK) {
					for (int column = 0; column < columnCount; column++) {
						if (result[column] != Raster.NO_VALUE) {
							coords[0] = column;
							coords[1] = line;
							GdalUtils.transform(coords, dataset);
							bufferedWriter
									.write(String.format("%.17f %.17f %.11f", coords[1], coords[0], result[column]));
							bufferedWriter.newLine();
						}
					}
				}
			}
			band.delete();
		}
	}

	/**
	 * Compute the elevation offset for the specified age.
	 *
	 * @param dateIndex Age (in million years starting from today).
	 */
	protected double computeOffset(MtecExportParameters parameters, int age, int ageCell) {
		double result = 0d;
		switch (parameters.getOffsetMode()) {
		case AUTO:
			result = ageCell != Raster.NO_VALUE && ageCell >= age
					? 350.0 * (Math.sqrt(ageCell) - Math.sqrt(ageCell - age))
					: 0d;
			break;

		case MANUAL_VALUE:
			result = parameters.getOffset();
			break;

		case MANUAL_FILE:
			if (age >= 0 && age < parameters.getOffsetMappingFileValues().length) {
				result = parameters.getOffsetMappingFileValues()[age];
			}
			break;
		case OFF:
		default:
			result = 0d;
		}

		return result;
	}

	/**
	 * Set of parameter to perform an export.
	 */
	public static class MtecExportParameters {

		/** Tool to compute an output file name */
		protected OutputFileNameComputer outputFileNameComputer;
		/** Model expected by OutputFileNameComputer */
		protected OutputFileNameComputerModel outputFileNameComputerModel;

		/** The input infostore where the export need to be done. */
		protected FileInfoNode fileToTranscode;
		/** Contains all dates concerned by the export. */
		protected DatesRepository datesRepository;
		/** Tectonic layer of the plate to export. */
		protected TectonicPlateLayerStore tectonicPlateLayerStore;
		/** Tectonic layer of the plate to export. */
		protected MultiLayerTecImageElevationData elevationData;

		/** Subsidence parameters. */
		protected OffsetMode offsetMode;
		protected int offset;
		protected double[] offsetMappingFileValues;

		/** Ouput format selection. */
		protected boolean outXyzFormat;
		protected boolean outTiffFormat;
		protected boolean outTextureFormat;

		/** The global tectonic model extract from MultiLayerTecImageElevationData. */
		protected TectonicModel tectonicModel;
		/** The plate's model extract from MultiLayerTecImageElevationData. */
		protected TectonicPlateModel tectonicPlateModel;
		/** Overwrite existing files */
		protected boolean overwriteExistingFiles;

		/**
		 * Getter of outputFileNameComputer
		 */
		public OutputFileNameComputer getOutputFileNameComputer() {
			return outputFileNameComputer;
		}

		/**
		 * Setter of outputFileNameComputer
		 */
		public void setOutputFileNameComputer(OutputFileNameComputer outputFileNameComputer) {
			this.outputFileNameComputer = outputFileNameComputer;
		}

		/**
		 * Getter of outputFileNameComputerModel
		 */
		public OutputFileNameComputerModel getOutputFileNameComputerModel() {
			return outputFileNameComputerModel;
		}

		/**
		 * Setter of outputFileNameComputerModel
		 */
		public void setOutputFileNameComputerModel(OutputFileNameComputerModel outputFileNameComputerModel) {
			this.outputFileNameComputerModel = outputFileNameComputerModel;
		}

		/**
		 * Getter of fileToTranscode
		 */
		public FileInfoNode getFileToTranscode() {
			return fileToTranscode;
		}

		/**
		 * Setter of fileToTranscode
		 */
		public void setFileToTranscode(FileInfoNode fileToTranscode) {
			this.fileToTranscode = fileToTranscode;
		}

		/**
		 * Getter of datesRepository
		 */
		public DatesRepository getDatesRepository() {
			return datesRepository;
		}

		/**
		 * Setter of datesRepository
		 */
		public void setDatesRepository(DatesRepository datesRepository) {
			this.datesRepository = datesRepository;
		}

		/**
		 * Getter of tectonicPlateLayerStore
		 */
		public TectonicPlateLayerStore getTectonicPlateLayerStore() {
			return tectonicPlateLayerStore;
		}

		/**
		 * Setter of tectonicPlateLayerStore
		 */
		public void setTectonicPlateLayerStore(TectonicPlateLayerStore tectonicPlateLayerStore) {
			this.tectonicPlateLayerStore = tectonicPlateLayerStore;
		}

		/**
		 * Getter of elevationData
		 */
		public MultiLayerTecImageElevationData getElevationData() {
			return elevationData;
		}

		/**
		 * Setter of elevationData
		 */
		public void setElevationData(MultiLayerTecImageElevationData elevationData) {
			this.elevationData = elevationData;
		}

		/**
		 * Getter of offsetMode
		 */
		public OffsetMode getOffsetMode() {
			return offsetMode;
		}

		/**
		 * Setter of offsetMode
		 */
		public void setOffsetMode(OffsetMode offsetMode) {
			this.offsetMode = offsetMode;
		}

		/**
		 * Getter of offset
		 */
		public int getOffset() {
			return offset;
		}

		/**
		 * Setter of offset
		 */
		public void setOffset(int offset) {
			this.offset = offset;
		}

		/**
		 * Getter of offsetMappingFileValues
		 */
		public double[] getOffsetMappingFileValues() {
			return offsetMappingFileValues;
		}

		/**
		 * Setter of offsetMappingFileValues
		 */
		public void setOffsetMappingFileValues(double[] offsetMappingFileValues) {
			this.offsetMappingFileValues = offsetMappingFileValues;
		}

		/**
		 * Getter of outXyzFormat
		 */
		public boolean isOutXyzFormat() {
			return outXyzFormat;
		}

		/**
		 * Setter of outXyzFormat
		 */
		public void setOutXyzFormat(boolean outXyzFormat) {
			this.outXyzFormat = outXyzFormat;
		}

		/**
		 * Getter of outTiffFormat
		 */
		public boolean isOutTiffFormat() {
			return outTiffFormat;
		}

		/**
		 * Setter of outTiffFormat
		 */
		public void setOutTiffFormat(boolean outTiffFormat) {
			this.outTiffFormat = outTiffFormat;
		}

		/**
		 * Getter of outTextureFormat
		 */
		public boolean isOutTextureFormat() {
			return outTextureFormat;
		}

		/**
		 * Setter of outTextureFormat
		 */
		public void setOutTextureFormat(boolean outTextureFormat) {
			this.outTextureFormat = outTextureFormat;
		}

		/**
		 * Getter of tectonicModel
		 */
		public TectonicModel getTectonicModel() {
			return tectonicModel;
		}

		/**
		 * Setter of tectonicModel
		 */
		public void setTectonicModel(TectonicModel tectonicModel) {
			this.tectonicModel = tectonicModel;
		}

		/**
		 * Getter of tectonicPlateModel
		 */
		public TectonicPlateModel getTectonicPlateModel() {
			return tectonicPlateModel;
		}

		/**
		 * Setter of tectonicPlateModel
		 */
		public void setTectonicPlateModel(TectonicPlateModel tectonicPlateModel) {
			this.tectonicPlateModel = tectonicPlateModel;
		}

		/**
		 * Getter of overwriteExistingFiles
		 */
		public boolean isOverwriteExistingFiles() {
			return overwriteExistingFiles;
		}

		/**
		 * Setter of overwriteExistingFiles
		 */
		public void setOverwriteExistingFiles(boolean overwriteExistingFiles) {
			this.overwriteExistingFiles = overwriteExistingFiles;
		}
	}

	/**
	 * Set of elevations defined in a bounding box.
	 */
	public static class ElevationSet {
		/** Min latitude of the geobox. */
		protected double minLatitude = Double.POSITIVE_INFINITY;
		/** Min longitude of the geobox. */
		protected double minLongitude = Double.POSITIVE_INFINITY;
		/** Max latitude of the geobox. */
		protected double maxLatitude = Double.NEGATIVE_INFINITY;
		/** Max longitude of the geobox. */
		protected double maxLongitude = Double.NEGATIVE_INFINITY;
		/**
		 * <pre>
		 * The grid as [index between 0 and (elevationCount-1)][0=>Longitude | 1=>Latitude | 2=>Elevation]
		 * </pre>
		 */
		protected double[][] xyz;

		/** Number of elevation in the grid. */
		protected int elevationCount = 0;

		/**
		 * Constructor.
		 */
		public ElevationSet(int capacity) {
			xyz = new double[capacity][3];
		}

		/**
		 * Add an elevation.
		 */
		public void addElevation(double longitude, double latitude, double elevation) {
			xyz[elevationCount][0] = longitude;
			xyz[elevationCount][1] = latitude;
			xyz[elevationCount][2] = elevation;

			minLatitude = Math.min(minLatitude, latitude);
			maxLatitude = Math.max(maxLatitude, latitude);
			minLongitude = Math.min(minLongitude, longitude);
			maxLongitude = Math.max(maxLongitude, longitude);

			elevationCount++;
		}

		/**
		 * Reduce the size of the xyz to the number of added elevation.
		 */
		public void truncate() {
			xyz = Arrays.copyOf(xyz, elevationCount);
		}

	}

	/**
	 * Set of parameter to perform an export.
	 */
	public static class MaskingGrid {
		/** Grid of ages. */
		protected double[][] ages;
		/** Grid width. */
		protected int width;
		/** Grid height. */
		protected int height;
		/** Tranformation of Lon/Lat to XY. */
		protected double[] toXYTransformation;

		/**
		 * Load grid.
		 */
		public void loadMaskValues(File maskFile) {
			Dataset maskDataset = gdal.Open(maskFile.getAbsolutePath());
			if (maskDataset != null) {
				height = maskDataset.GetRasterYSize();
				width = maskDataset.GetRasterXSize();
				ages = new double[height][width];

				GdalUtils.readRaster(maskDataset.GetRasterBand(1), (line, column, value) -> ages[line][column] = value);
				toXYTransformation = gdal.InvGeoTransform(maskDataset.GetGeoTransform());
				GdalUtils.asyncDeleteDataset(maskDataset);
			}
		}

		/**
		 * @return the age of the cell for the specified coord.
		 */
		public int getAge(double longitude, double latitude) {
			int result = (int) Raster.NO_VALUE;
			if (toXYTransformation != null) {
				double[] xy = { longitude, latitude };
				GdalUtils.transform(xy, toXYTransformation);
				int column = (int) Math.round(xy[0]);
				int line = (int) Math.round(xy[1]);
				if (column >= 0 && column < width && line >= 0 && line < height) {
					result = (int) ages[line][column];
				}
			}
			return result;
		}
	}

}
