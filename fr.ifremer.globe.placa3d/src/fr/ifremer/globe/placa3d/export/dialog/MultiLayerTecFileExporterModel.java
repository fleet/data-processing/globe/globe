/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.export.dialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.composite.SubsidenceComposite.SubsidenceModel;
import fr.ifremer.globe.placa3d.export.FileExtensionFormatter;
import fr.ifremer.globe.placa3d.export.FileExtensionFormatter.EXTENSION;
import fr.ifremer.globe.placa3d.export.dialog.MultiLayerTecFileExporterSettingsPage.MultiLayerTecFileExporterSettingsModel;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableNumber;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPageModel;

/**
 *
 */
public class MultiLayerTecFileExporterModel
		implements SelectOutputParametersPageModel, MultiLayerTecFileExporterSettingsModel, SubsidenceModel {

	/** Age range */
	private final WritableObject<DoubleRange> ageRange = new WritableObject<>(new DoubleRange(0d));

	/** Age parameters */
	private final WritableObject<AgeSetting> ageSetting = new WritableObject<>(AgeSetting.Current);
	private final WritableNumber currentAge = new WritableNumber(0);
	private final WritableNumber customAge = new WritableNumber(0);
	private final WritableNumber startAge = new WritableNumber(0);
	private final WritableNumber endAge = new WritableNumber(200);
	private final WritableNumber ageStep = new WritableNumber(1);

	/** Subsidence argument */
	private final WritableObject<OffsetMode> offsetMode = new WritableObject<>(OffsetMode.OFF);
	/** Subsidence argument */
	private final WritableFile offsetFile = new WritableFile();
	/** Subsidence argument */
	private final WritableNumber constantOffset = new WritableNumber(0);

	/** Convert to Xyz required */
	private final WritableBoolean outXyzFormat = new WritableBoolean();
	/** Convert to Tiff required */
	private final WritableBoolean outTiffFormat = new WritableBoolean();
	/** Convert to Texture required */
	private final WritableBoolean outTextureFormat = new WritableBoolean();

	/** Output file name */
	private final WritableString singleOutputFilename = new WritableString();
	/** Output directory */
	private final WritableFile outputDirectory = new WritableFile();
	/** Extension of output files */
	private final WritableObjectList<String> outputFormatExtensions = new WritableObjectList<>();
	/** Prefix of output files */
	private final WritableString prefix = new WritableString();
	/** Suffix of output files */
	private final WritableString suffix = new WritableString();
	/** List of output Files */
	private final WritableObjectList<File> outputFiles = new WritableObjectList<>();
	/** Overwrite existing files */
	private final WritableBoolean overwriteExistingFiles = new WritableBoolean();
	/** True to load files at the end of process */
	private final WritableBoolean loadFilesAfter = new WritableBoolean(false);
	/** Where to load the files (in witch group of project explorer) */
	private final WritableString whereToloadFiles = new WritableString();

	/** List of Files */
	private final WritableObjectList<File> inputFiles = new WritableObjectList<>();
	/** List of file Extensions */
	private final List<Pair<String, String>> inputFilesFilterExtensions = new ArrayList<>();

	/**
	 * Constructor
	 */
	public MultiLayerTecFileExporterModel() {
		ageSetting.addChangeListener(e -> ageSetting.getRealm().asyncExec(this::manageExtension));
		outXyzFormat.addChangeListener(e -> outXyzFormat.getRealm().asyncExec(this::manageExtension));
		outTiffFormat.addChangeListener(e -> outTiffFormat.getRealm().asyncExec(this::manageExtension));
		outTextureFormat.addChangeListener(e -> outTextureFormat.getRealm().asyncExec(this::manageExtension));
		customAge.addChangeListener(e -> customAge.getRealm().asyncExec(this::manageExtension));
		startAge.addChangeListener(e -> startAge.getRealm().asyncExec(this::manageExtension));
		endAge.addChangeListener(e -> endAge.getRealm().asyncExec(this::manageExtension));
		ageStep.addChangeListener(e -> ageStep.getRealm().asyncExec(this::manageExtension));
	}

	/**
	 * @return the xyz file name for storing the elevation at the specified age.
	 */
	protected void manageExtension() {
		List<String> computedExtensions = new ArrayList<>();
		FileExtensionFormatter fileExtensionFormatter = new FileExtensionFormatter();
		List<EXTENSION> extensions = new ArrayList<>();
		if (outXyzFormat.isTrue()) {
			extensions.add(EXTENSION.XYZ_EXTENSION);
		}
		if (outTiffFormat.isTrue()) {
			extensions.add(EXTENSION.ELEVATION_EXTENSION);
		}
		if (outTextureFormat.isTrue()) {
			extensions.add(EXTENSION.TEXTURE_EXTENSION);
		}

		for (EXTENSION extension : extensions) {
			switch (ageSetting.get()) {
			case Step:
				for (int age = startAge.intValue(); age <= endAge.intValue(); age += ageStep.intValue()) {
					computedExtensions.add(fileExtensionFormatter.format(age, extension));
				}
				break;
			case Custom:
				computedExtensions.add(fileExtensionFormatter.format(customAge.intValue(), extension));
				break;
			default:
				computedExtensions.add(fileExtensionFormatter.format(currentAge.intValue(), extension));
				break;
			}
			outputFormatExtensions.clear();
			outputFormatExtensions.addAll(computedExtensions);
		}
	}

	/**
	 * Getter of ageSetting
	 */
	public WritableObject<AgeSetting> getAgeSetting() {
		return ageSetting;
	}

	/**
	 * Getter of customAge
	 */
	public WritableNumber getCustomAge() {
		return customAge;
	}

	/**
	 * Getter of startAge
	 */
	public WritableNumber getStartAge() {
		return startAge;
	}

	/**
	 * Getter of endAge
	 */
	public WritableNumber getEndAge() {
		return endAge;
	}

	/**
	 * Getter of ageStep
	 */
	public WritableNumber getAgeStep() {
		return ageStep;
	}

	/**
	 * Getter of offsetMode
	 */
	@Override
	public WritableObject<OffsetMode> getOffsetMode() {
		return offsetMode;
	}

	/**
	 * Getter of offsetFile
	 */
	@Override
	public WritableFile getOffsetFile() {
		return offsetFile;
	}

	/**
	 * Getter of constantOffset
	 */
	@Override
	public WritableNumber getConstantOffset() {
		return constantOffset;
	}

	/**
	 * Getter of outXyzFormat
	 */
	public WritableBoolean getOutXyzFormat() {
		return outXyzFormat;
	}

	/**
	 * Getter of outTiffFormat
	 */
	public WritableBoolean getOutTiffFormat() {
		return outTiffFormat;
	}

	/**
	 * Getter of outTextureFormat
	 */
	public WritableBoolean getOutTextureFormat() {
		return outTextureFormat;
	}

	/**
	 * Getter of outputDirectory
	 */
	@Override
	public WritableFile getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * Getter of outputFormatExtensions
	 */
	@Override
	public WritableObjectList<String> getOutputFormatExtensions() {
		return outputFormatExtensions;
	}

	/**
	 * Getter of prefix
	 */
	@Override
	public WritableString getPrefix() {
		return prefix;
	}

	/**
	 * Getter of suffix
	 */
	@Override
	public WritableString getSuffix() {
		return suffix;
	}

	/**
	 * Getter of outputFiles
	 */
	@Override
	public WritableObjectList<File> getOutputFiles() {
		return outputFiles;
	}

	/**
	 * Getter of overwriteExistingFiles
	 */
	@Override
	public WritableBoolean getOverwriteExistingFiles() {
		return overwriteExistingFiles;
	}

	/**
	 * Getter of loadFilesAfter
	 */
	@Override
	public WritableBoolean getLoadFilesAfter() {
		return loadFilesAfter;
	}

	/**
	 * Getter of ageRange
	 */
	@Override
	public WritableObject<DoubleRange> getAgeRange() {
		return ageRange;
	}

	/**
	 * Getter of inputFiles
	 */
	@Override
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	/**
	 * Getter of inputFilesFilterExtensions
	 */
	public List<Pair<String, String>> getInputFilesFilterExtensions() {
		return inputFilesFilterExtensions;
	}

	/**
	 * Getter of currentAge
	 */
	public WritableNumber getCurrentAge() {
		return currentAge;
	}

	/**
	 * Getter of extensionSeparator
	 */
	@Override
	public String getExtensionSeparator() {
		return "_";
	}

	/** Getter of {@link #singleOutputFilename} */
	@Override
	public WritableString getSingleOutputFilename() {
		return singleOutputFilename;
	}

	/**
	 * @return the {@link #whereToloadFiles}
	 */
	@Override
	public WritableString getWhereToloadFiles() {
		return whereToloadFiles;
	}

}
