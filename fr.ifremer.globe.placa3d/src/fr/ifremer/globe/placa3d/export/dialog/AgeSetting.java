package fr.ifremer.globe.placa3d.export.dialog;

/**
 * Define the type of age selected by the user during a MTEC file export (current, custom or between two limits).
 */
public enum AgeSetting {
	Current,
	Custom,
	Step
}

