package fr.ifremer.globe.placa3d.export.dialog;

import java.io.IOException;

import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.placa3d.tectonic.AgeOffsetFileLoader;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

public class MultiLayerTecFileExporterWizard extends Wizard {

	protected MultiLayerTecFileExporterModel multiLayerTecFileExporterModel = new MultiLayerTecFileExporterModel();

	private MultiLayerTecFileExporterSettingsPage settingsPage;
	private SelectOutputParametersPage selectOutputParametersPage;

	protected double[] offsetMappingFileValues;

	public MultiLayerTecFileExporterWizard(MultiLayerTecFileExporterModel multiLayerTecFileExporterModel) {
		this.multiLayerTecFileExporterModel = multiLayerTecFileExporterModel;
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {
		settingsPage = new MultiLayerTecFileExporterSettingsPage(
				DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_TITLE"),
				DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_DESCRIPTION"), multiLayerTecFileExporterModel);
		addPage(settingsPage);

		selectOutputParametersPage = new SelectOutputParametersPage(
				DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_TITLE"),
				DicoBundle.getString("WIZARD_OUTPUT_FILE_PAGE_TITLE"), multiLayerTecFileExporterModel);
		selectOutputParametersPage.enableLoadFilesAfter(false);
		addPage(selectOutputParametersPage);

	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {

		if (multiLayerTecFileExporterModel.getOffsetMode().get() == OffsetMode.MANUAL_FILE) {
			AgeOffsetFileLoader subsidenceFileLoader = new AgeOffsetFileLoader();
			try {
				this.offsetMappingFileValues = subsidenceFileLoader
						.loadOffsetFile(settingsPage.multiLayerTecFileExporterModel.getOffsetFile().get());
			} catch (IOException e) {
				Messages.openErrorMessage(
						"Unable to read file " + settingsPage.multiLayerTecFileExporterModel.getOffsetFile().getName(),
						e);
				return false;
			}
		}
		return true;
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#canFinish()
	 */
	@Override
	public boolean canFinish() {
		return settingsPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @return the {@link #offsetMappingFileValues}
	 */
	public double[] getOffsetMappingFileValues() {
		return offsetMappingFileValues;
	}

	/**
	 * Getter of multiLayerTecFileExporterModel
	 */
	public MultiLayerTecFileExporterModel getMultiLayerTecFileExporterModel() {
		return multiLayerTecFileExporterModel;
	}

	/**
	 * Getter of outputFileNameComputer
	 */
	public OutputFileNameComputer getOutputFileNameComputer() {
		return selectOutputParametersPage.getOutputFileNameComputer();
	}

}
