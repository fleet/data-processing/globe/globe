package fr.ifremer.globe.placa3d.export.dialog;

/**
 * Define the format type applied during a MTEC file export. <br>
 * <li> XYZ format match to an ascii format which sort data by latitude, longitude and elevation.
 * <li> Globe grid format which contains two layers (one for the elevation and one for the texture).
 * <li> NetCDF format which contains two layers (one for the elevation and one for the texture).
 * <li> GeoTIFF format which produces two GeoTIFF files (one for the elevation and one for the texture).
 */
public enum FormatSetting {
	/** Ascii format which sort data by latitude, longitude and elevation. */
	XYZ,
	/** Produces two GeoTIFF files (one for the elevation and one for the texture). */
	GeoTIFF
}
