package fr.ifremer.globe.placa3d.export.dialog;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.placa3d.composite.SubsidenceComposite;
import fr.ifremer.globe.placa3d.composite.SubsidenceComposite.SubsidenceModel;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * Allow user to manage settings about the MTEC file exportation througth a wizard page.
 */
public class MultiLayerTecFileExporterSettingsPage extends WizardPage {
	private DataBindingContext dataBindingContext;

	private static final int spinnerCoefficient = 100;

	private Spinner customAgeSpinner;
	protected Spinner startAgeSpinner;
	protected Spinner endAgeSpinner;
	protected Spinner stepSpinner;

	/** Age selection buttons */
	protected Button customAgeButton;
	protected Button currentAgeButton;
	protected Button betweenAgeButton;

	/** Ouput format selection. */
	private Button outXyzFormat;
	private Button outTiffFormat;
	private Button outTextureFormat;
	private Composite container;

	/** Model */
	protected MultiLayerTecFileExporterModel multiLayerTecFileExporterModel;

	/** Subsidence composite */
	protected SubsidenceComposite subsidenceComposite;

	/**
	 * Constructor used by WindowBuilder
	 * 
	 * @wbp.parser.constructor
	 */
	public MultiLayerTecFileExporterSettingsPage() {
		super("Export parameters");
		setTitle("Export settings");
		setDescription("Settings about the new MTEC file to export");
	}

	/**
	 * Constructor of {@link MultiLayerTecFileExporterSettingsPage}.
	 * 
	 * @param title title of the wizard page
	 * @param description description of the wizard page
	 * 
	 */
	public MultiLayerTecFileExporterSettingsPage(String title, String description,
			MultiLayerTecFileExporterModel model) {
		super("Export parameters");
		// Configuration of the parent wizard
		setTitle(title);
		setDescription(description);

		this.multiLayerTecFileExporterModel = model;
	}

	/**
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets .Composite)
	 */
	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.NONE);
		setControl(container);
		container.setLayout(new GridLayout(1, false));

		initializeAgeGroup(container);
		initializeSubsidenceGroup(container);
		initializeFormatGroup(container);

		dataBindingContext.updateTargets();
		updatePageComplete();
	}

	/**
	 * Initialize all the components relatives to the age setting.
	 * 
	 * @param parent parent composite of the age components
	 */
	protected void initializeSubsidenceGroup(Composite parent) {
		subsidenceComposite = new SubsidenceComposite(parent, SWT.NONE, multiLayerTecFileExporterModel);
		subsidenceComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
	}

	/**
	 * Initialize all the components relatives to the ouput format setting.
	 * 
	 * @param parent parent composite of the age components
	 */
	protected void initializeFormatGroup(Composite parent) {

		Group formatGroup = new Group(parent, SWT.NONE);
		formatGroup.setLayout(new GridLayout(3, false));
		formatGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		formatGroup.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_FORMAT_GROUP"));

		outXyzFormat = new Button(formatGroup, SWT.CHECK);
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		outXyzFormat.setLayoutData(gridData);
		outXyzFormat.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_FORMAT_XYZ"));
		outXyzFormat.setSelection(true);

		outTiffFormat = new Button(formatGroup, SWT.CHECK);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.horizontalIndent = 10;
		outTiffFormat.setLayoutData(gridData);
		outTiffFormat.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_FORMAT_TIFF"));

		outTextureFormat = new Button(formatGroup, SWT.CHECK);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.horizontalIndent = 10;
		outTextureFormat.setLayoutData(gridData);
		outTextureFormat.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_FORMAT_TEXTURE"));

		dataBindingContext = initDataBindings();
		initSpecificBindings();
	}

	/**
	 * Initialize all the components relatives to the age setting.
	 * 
	 * @param parent parent composite of the age components
	 */
	protected void initializeAgeGroup(Composite parent) {

		Group ageGroup = new Group(parent, SWT.NONE);
		ageGroup.setLayout(new GridLayout(7, false));
		ageGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		ageGroup.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_AGE_GROUP"));

		currentAgeButton = new Button(ageGroup, SWT.RADIO);
		currentAgeButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 7, 1));
		currentAgeButton.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_AGE_CURRENT"));
		currentAgeButton.setSelection(true);

		customAgeButton = new Button(ageGroup, SWT.RADIO);
		customAgeButton.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_AGE_CUSTOM"));

		customAgeSpinner = new Spinner(ageGroup, SWT.BORDER);
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.horizontalSpan = 6;
		gridData.widthHint = 30;
		customAgeSpinner.setLayoutData(gridData);
		customAgeSpinner.setTextLimit(3);
		customAgeSpinner.setMinimum((int) getMinimumAge());
		customAgeSpinner.setMaximum((int) getMaximumAge());
		customAgeSpinner.setEnabled(false);

		betweenAgeButton = new Button(ageGroup, SWT.RADIO);
		betweenAgeButton.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_AGE_BETWEEN"));

		startAgeSpinner = new Spinner(ageGroup, SWT.BORDER);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.widthHint = 30;
		startAgeSpinner.setLayoutData(gridData);
		startAgeSpinner.setMinimum((int) (getMinimumAge() * spinnerCoefficient));
		startAgeSpinner.setMaximum(200);
		startAgeSpinner.setSelection((int) (getMinimumAge() * spinnerCoefficient));
		startAgeSpinner.setEnabled(false);

		Label toLabel = new Label(ageGroup, SWT.NONE);
		toLabel.setText(DicoBundle.getString("WORD_TO"));

		endAgeSpinner = new Spinner(ageGroup, SWT.BORDER);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.widthHint = 30;
		endAgeSpinner.setLayoutData(gridData);
		endAgeSpinner.setMinimum((int) (getMinimumAge() * spinnerCoefficient));
		endAgeSpinner.setMaximum(200);
		endAgeSpinner.setSelection((int) (getMaximumAge() * spinnerCoefficient));
		endAgeSpinner.setEnabled(false);

		Label stepLabel = new Label(ageGroup, SWT.NONE);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.horizontalIndent = 10;
		stepLabel.setLayoutData(gridData);
		stepLabel.setText(DicoBundle.getString("TEC_EXPORT_WIZARD_SETTINGPAGE_AGE_STEP"));

		stepSpinner = new Spinner(ageGroup, SWT.BORDER);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.widthHint = 30;
		stepSpinner.setLayoutData(gridData);
		stepSpinner.setMinimum(1);
		stepSpinner.setMaximum((int) getMaximumAge() - (int) getMinimumAge());
		stepSpinner.setEnabled(false);
		new Label(ageGroup, SWT.NONE);
	}

	/** Sets whether this page is complete */
	protected void updatePageComplete() {
		setPageComplete((multiLayerTecFileExporterModel.getOffsetMode().get() != OffsetMode.MANUAL_FILE
				|| subsidenceComposite.getOffsetfileValidationStatus().isTrue())
				// Out format OK
				&& (getOutTextureFormat() || getOutTiffFormat() || getOutXyzFormat()));
	}

	/**
	 * @return the {@link #outXyzFormat}
	 */
	public boolean getOutXyzFormat() {
		return outXyzFormat.getSelection();
	}

	/**
	 * @return the {@link #outTiffFormat}
	 */
	public boolean getOutTiffFormat() {
		return outTiffFormat.getSelection();
	}

	/**
	 * @return the {@link #outTextureFormat}
	 */
	public boolean getOutTextureFormat() {
		return outTextureFormat.getSelection();
	}

	/** Minimum Age */
	public double getMinimumAge() {
		return multiLayerTecFileExporterModel != null
				? multiLayerTecFileExporterModel.getAgeRange().get().getMinimumDouble()
				: 0d;
	}

	/** Minimum Age */
	public double getMaximumAge() {
		return multiLayerTecFileExporterModel != null
				? multiLayerTecFileExporterModel.getAgeRange().get().getMaximumDouble()
				: 200d;
	}

	/**
	 * Model of this page
	 */
	public static interface MultiLayerTecFileExporterSettingsModel extends SubsidenceModel {
		/** Age range */
		public WritableObject<DoubleRange> getAgeRange();

	}

	/** Initialize a specific bindings (not managed by Windows Builder */
	protected void initSpecificBindings() {

		// Bind AgeSetting on radio buttons
		var observeSelectionNoSamplingButton = WidgetProperties.buttonSelection().observe(customAgeButton);
		var observeSelectionTimeSamplingButton = WidgetProperties.buttonSelection().observe(currentAgeButton);
		var observeSelectionSoundingsSamplingButton = WidgetProperties.buttonSelection().observe(betweenAgeButton);
		var featureRepoPolicyObservable = new SelectObservableValue<AgeSetting>();
		featureRepoPolicyObservable.addOption(AgeSetting.Custom, observeSelectionNoSamplingButton);
		featureRepoPolicyObservable.addOption(AgeSetting.Current, observeSelectionTimeSamplingButton);
		featureRepoPolicyObservable.addOption(AgeSetting.Step, observeSelectionSoundingsSamplingButton);
		dataBindingContext.bindValue(featureRepoPolicyObservable, multiLayerTecFileExporterModel.getAgeSetting());

		multiLayerTecFileExporterModel.getOffsetMode().addValueChangeListener((event) -> updatePageComplete());
		multiLayerTecFileExporterModel.getOffsetFile().addValueChangeListener((event) -> updatePageComplete());
		multiLayerTecFileExporterModel.getOutXyzFormat().addValueChangeListener((event) -> updatePageComplete());
		multiLayerTecFileExporterModel.getOutTiffFormat().addValueChangeListener((event) -> updatePageComplete());
		multiLayerTecFileExporterModel.getOutTextureFormat().addValueChangeListener((event) -> updatePageComplete());
		subsidenceComposite.getOffsetfileValidationStatus().addValueChangeListener((event) -> updatePageComplete());
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		var observeSelectionOutXyzFormatObserveWidget = WidgetProperties.buttonSelection().observe(outXyzFormat);
		bindingContext.bindValue(observeSelectionOutXyzFormatObserveWidget,
				multiLayerTecFileExporterModel.getOutXyzFormat(), null, null);
		//
		var observeSelectionOutTiffFormatObserveWidget = WidgetProperties.buttonSelection().observe(outTiffFormat);
		bindingContext.bindValue(observeSelectionOutTiffFormatObserveWidget,
				multiLayerTecFileExporterModel.getOutTiffFormat(), null, null);
		//
		var observeSelectionOutTextureFormatObserveWidget = WidgetProperties.buttonSelection()
				.observe(outTextureFormat);
		bindingContext.bindValue(observeSelectionOutTextureFormatObserveWidget,
				multiLayerTecFileExporterModel.getOutTextureFormat(), null, null);
		//
		var observeSelectionCustomAgeSpinnerObserveWidget = WidgetProperties.spinnerSelection()
				.observe(customAgeSpinner);
		bindingContext.bindValue(observeSelectionCustomAgeSpinnerObserveWidget,
				multiLayerTecFileExporterModel.getCustomAge(), null, null);
		//
		var observeSelectionStartAgeSpinnerObserveWidget = WidgetProperties.spinnerSelection().observe(startAgeSpinner);
		bindingContext.bindValue(observeSelectionStartAgeSpinnerObserveWidget,
				multiLayerTecFileExporterModel.getStartAge(), null, null);
		//
		var observeSelectionEndAgeSpinnerObserveWidget = WidgetProperties.spinnerSelection().observe(endAgeSpinner);
		bindingContext.bindValue(observeSelectionEndAgeSpinnerObserveWidget, multiLayerTecFileExporterModel.getEndAge(),
				null, null);
		//
		var observeSelectionStepSpinnerObserveWidget = WidgetProperties.spinnerSelection().observe(stepSpinner);
		bindingContext.bindValue(observeSelectionStepSpinnerObserveWidget, multiLayerTecFileExporterModel.getAgeStep(),
				null, null);
		//
		var observeEnabledCustomAgeSpinnerObserveWidget = WidgetProperties.enabled().observe(customAgeSpinner);
		var observeSelectionCustomAgeButtonObserveWidget = WidgetProperties.buttonSelection().observe(customAgeButton);
		bindingContext.bindValue(observeEnabledCustomAgeSpinnerObserveWidget,
				observeSelectionCustomAgeButtonObserveWidget, null, null);
		//
		var observeEnabledStartAgeSpinnerObserveWidget = WidgetProperties.enabled().observe(startAgeSpinner);
		var observeSelectionBetweenAgeButtonObserveWidget = WidgetProperties.buttonSelection()
				.observe(betweenAgeButton);
		bindingContext.bindValue(observeEnabledStartAgeSpinnerObserveWidget,
				observeSelectionBetweenAgeButtonObserveWidget, null, null);
		//
		var observeEnabledEndAgeSpinnerObserveWidget = WidgetProperties.enabled().observe(endAgeSpinner);
		var observeSelectionBetweenAgeButtonObserveWidget_1 = WidgetProperties.buttonSelection()
				.observe(betweenAgeButton);
		bindingContext.bindValue(observeEnabledEndAgeSpinnerObserveWidget,
				observeSelectionBetweenAgeButtonObserveWidget_1, null, null);
		//
		var observeEnabledStepSpinnerObserveWidget = WidgetProperties.enabled().observe(stepSpinner);
		var observeSelectionBetweenAgeButtonObserveWidget_2 = WidgetProperties.buttonSelection()
				.observe(betweenAgeButton);
		bindingContext.bindValue(observeEnabledStepSpinnerObserveWidget,
				observeSelectionBetweenAgeButtonObserveWidget_2, null, null);
		//
		return bindingContext;
	}
}
