package fr.ifremer.globe.placa3d.drivers;

import fr.ifremer.globe.placa3d.file.IPoleRotationFile;
import fr.ifremer.globe.placa3d.file.intpol.IntermediatePoleFile;

/**
 * @author mgaro
 *
 */
public class IntermediatePoleLoader extends PoleRotationLoader {

	@Override
	protected IPoleRotationFile createIPoleRotationFile(String fileName) {
		return new IntermediatePoleFile(fileName);
	}

}
