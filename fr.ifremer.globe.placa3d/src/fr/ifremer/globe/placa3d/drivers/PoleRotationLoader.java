package fr.ifremer.globe.placa3d.drivers;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.placa3d.file.IPoleRotationFile;
import fr.ifremer.globe.placa3d.layers.PoleRotationLayer;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;

/***
 * Loads Rotation Poles if current survey is a TectonicPlate
 *
 * @author MORVAN
 *
 */
public abstract class PoleRotationLoader {

	public PoleRotationLayer load(File file, IProgressMonitor monitor) throws Exception {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		String name = file.getName().substring(0, file.getName().length());

		subMonitor.worked(20);

		// the current survey defines a plate
		// if null, we are in change session mode, the last created survey is
		// used

		String filename = file.getAbsolutePath();

		IPoleRotationFile poleRotationFile = createIPoleRotationFile(filename);
		subMonitor.worked(20);
		TectonicMovement movement = poleRotationFile.read();
		subMonitor.worked(20);
		PoleRotationLayer poleRotationLayer = new PoleRotationLayer(name, movement);
		subMonitor.worked(20);

		TectonicDefinition definition = new TectonicDefinition(poleRotationLayer);
		TectonicDataset.getInstance().addTectonicDefinition(definition);
		subMonitor.worked(20);

		// Restore session
		ISessionService.grab().getPropertiesContainer().monitor(file.getPath(), "Pole_Rotation_Layer_Parameters",
				poleRotationLayer.getLayerParameters(),
				poleRotationLayer.getLayerParameters().gainPropertyChangeSupport());

		return poleRotationLayer;

	}

	protected abstract IPoleRotationFile createIPoleRotationFile(String string);

}
