package fr.ifremer.globe.placa3d.drivers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.TectonicLayer;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.drivers.tile.TileProducerFacade;
import fr.ifremer.globe.placa3d.drivers.tile.TileProduction;
import fr.ifremer.globe.placa3d.drivers.tile.TileProductionParameters;
import fr.ifremer.globe.placa3d.drivers.tile.TileProductionParametersFactory;
import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import fr.ifremer.globe.placa3d.layers.TecShaderElevationLayer;
import fr.ifremer.globe.placa3d.morphing.MorphingManager;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

public class MultiLayerTecImageElevationLoader {

	protected static final Logger logger = LoggerFactory.getLogger(MultiLayerTecImageElevationLoader.class);

	protected MultiLayerTecImageElevationData dataFile;
	protected String type;
	protected String unit;
	protected double globalMin;
	protected double globalMax;
	protected double valMin;
	protected double valMax;

	protected TectonicModel tectonicModel;
	protected TectonicPlateModel plate;

	/** Facade of tile production system */
	protected final TileProducerFacade tileProducerFacade = new TileProducerFacade();

	/** TileProduction on the first image tile */
	protected TileProduction firstImageTileProduction = null;
	/** TileProduction on the first elevation tile */
	protected TileProduction firstElevationTileProduction = null;

	public MultiLayerTecImageElevationLoader(TectonicModel tectonicModel, TectonicPlateModel plate) {
		this.tectonicModel = tectonicModel;
		this.plate = plate;
	}

	/**
	 * This loader is only feeded with plate's metadata. So this method creates a bridge object that will read new data
	 * on-the-fly, depending on the current tectonic model's age index.
	 */
	public MultiLayerTecImageElevationData createImageElevationData(File file) {
		return new MultiLayerTecImageElevationData(file, tectonicModel, plate);
	}

	/**
	 * Creates a list of layers from a generic IImageElevationFile.
	 *
	 * @param dataFile The image file reader.
	 * @return A list of layers created from the image file.
	 */
	protected List<Pair<Layer, ElevationModel>> createLayers(MultiLayerTecImageElevationData tecFile,
			IProgressMonitor monitor) throws Exception {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		// Layers list
		List<Pair<Layer, ElevationModel>> layers = new ArrayList<>(4);

		Sector geoBox = tecFile.getPlate().getMetadata().getGeoBox();

		// test if file is on the dateline (modify test to take in account
		// the poles: cut from 0 to 180 and form -180 to 0)
		if (TecGeneratorUtil.isOnLineDateChange(geoBox.getMinLongitude().degrees, geoBox.getMaxLongitude().degrees)) {
			layers.addAll(createLayers(tecFile, Area.AREA_EAST));
			layers.addAll(createLayers(tecFile, Area.AREA_WEST));
		} else {
			generatePeriodTiles(tecFile, Area.AREA_ALL, subMonitor.split(45));
			subMonitor.subTask("Computing morphing");
			generateMorphing(tecFile, Area.AREA_ALL, subMonitor.split(45));
			subMonitor.subTask("Creating layers");
			layers.addAll(createLayers(tecFile, Area.AREA_ALL));
			subMonitor.done();
		}

		return layers;
	}

	/**
	 * Creates the texture and elevation layers. Generates textures and elevations tiles for each TectonicLayer.
	 */
	protected void generatePeriodTiles(MultiLayerTecImageElevationData tecFile, Area area, IProgressMonitor monitor)
			throws Exception {
		File elevationSource = tecFile.getFile();
		List<TectonicLayer> tectonicLayers = plate.getMetadata().getTectonicLayers();

		SubMonitor subMonitor = SubMonitor.convert(monitor, tectonicLayers.size() * 2);
		for (TectonicLayer interval : tectonicLayers) {
			// Force plate reload
			changeModelAge(tecFile, interval.getMinimumDouble());
			createPeriodImageTiles(tecFile, area, elevationSource, subMonitor.split(1));
			if (plate.hasElevation()) {
				createPeriodElevationTiles(tecFile, area, elevationSource, subMonitor.split(1));
			} else {
				subMonitor.worked(1);
			}
		}
	}

	/**
	 * Change age of the model and force the reloading of it.
	 */
	protected void changeModelAge(MultiLayerTecImageElevationData tecFile, double age) throws IOException {
		tectonicModel.setAge(age);
		tecFile.read();
	}

	/**
	 * Creates the texture layer and generates all texture tiles for all TectonicLayer.
	 */
	protected void createPeriodImageTiles(MultiLayerTecImageElevationData tecFile, Area area, File elevationSource,
			IProgressMonitor monitor) throws Exception {
		monitor.subTask("Generating texture tiles " + elevationSource.getName() + computeCacheDirSuffix());

		TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
				.makeTileImageParameters(tecFile, area, computeCacheDirSuffix());
		TileProduction tileProduction = tileProducerFacade.generateTecImageTiles(tileProductionParameters,
				elevationSource);
		firstImageTileProduction = firstImageTileProduction == null ? tileProduction : firstImageTileProduction;
	}

	/** Create the elevation layer for the specified tec file. */
	protected void createPeriodElevationTiles(MultiLayerTecImageElevationData tecFile, Area area, File elevationSource,
			IProgressMonitor monitor) throws Exception {
		monitor.subTask("Generating elevation tiles " + elevationSource.getName() + computeCacheDirSuffix());

		TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
				.makeTileElevationParameters(tecFile, area, computeCacheDirSuffix());
		TileProduction tileProduction = tileProducerFacade.generateTecElevationTiles(tileProductionParameters,
				elevationSource);
		firstElevationTileProduction = firstElevationTileProduction == null ? tileProduction
				: firstElevationTileProduction;
	}

	/**
	 * Launch morphing computation if enabled
	 *
	 * @param tecFile Mtec model
	 * @param area East / West / ALL
	 * @param monitor the progress monitor
	 * @throws Exception something wrong appends
	 */
	protected void generateMorphing(MultiLayerTecImageElevationData tecFile, Area area, IProgressMonitor monitor)
			throws Exception {
		boolean morphingEnabled = Activator.getPreferences().getMorphing().getValue().booleanValue();
		// If morphing is enabled, compute the intermediate images
		if (morphingEnabled) {
			try (MorphingManager morphingManager = new MorphingManager(tectonicModel, plate)) {
				morphingManager.process(tecFile, area, monitor);
			}
		}
	}

	/**
	 * Creates all layers
	 *
	 * @param tecFile Mtec model
	 * @param area East / West / ALL
	 * @throws Exception something wrong appends
	 */
	protected List<Pair<Layer, ElevationModel>> createLayers(MultiLayerTecImageElevationData tecFile, Area area)
			throws Exception {
		// Come back to the first age
		changeModelAge(tecFile, 0d);

		// Layer list
		List<Pair<Layer, ElevationModel>> ret = new ArrayList<>();

		File elevationSource = dataFile.getFile();

		// Create the elevation layer
		if (tecFile.getMinDepth() != TecGeneratorUtil.getMissingValue()) {
			if (firstElevationTileProduction == null) {
				TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
						.makeTileElevationParameters(tecFile, area,
								TectonicUtils.computeCacheDirSuffix(plate, tectonicModel.getAge(), false));
				firstElevationTileProduction = tileProducerFacade.generateTecElevationTiles(tileProductionParameters,
						elevationSource);
			}
			// Create the layer
			ElevationModel elevationmodel = IWWElevationModelFactory.grab().createMovableInterpolateElevationModel(
					firstElevationTileProduction.getDocument().getDocumentElement(),
					firstElevationTileProduction.getParameters());
			ShaderElevationLayer elevationLayer = new TecShaderElevationLayer(tectonicModel, plate, tecFile.getFile(),
					firstElevationTileProduction.getLevelSet());
			elevationLayer.setUnit(IConstants.MeterUnit);
			elevationLayer.setType("DEPTH");
			elevationLayer.setTextureCacheObsolete();
			ret.add(new Pair<Layer, ElevationModel>(elevationLayer, elevationmodel));
		}

		// Create the image layer to represent the elevation or the texture
		if (firstImageTileProduction == null) {
			TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
					.makeTileImageParameters(tecFile, area,
							TectonicUtils.computeCacheDirSuffix(plate, tectonicModel.getAge(), false));
			firstImageTileProduction = tileProducerFacade.generateTecImageTiles(tileProductionParameters,
					elevationSource);
		}
		// Create the layer
		ShaderElevationLayer imageLayer = new TecShaderElevationLayer(tectonicModel, plate, tecFile.getFile(),
				firstImageTileProduction.getDocument(), firstImageTileProduction.getParameters(), RasterInfo.RASTER_ELEVATION);
		imageLayer.setUnit(IConstants.MeterUnit);
		imageLayer.setIsRGB(plate.hasTexture());

		ret.add(new Pair<Layer, ElevationModel>(imageLayer, null));

		return ret;
	}

	/**
	 * Returns true if cache files already exist.
	 */
	protected boolean checkCacheExists(File cacheIdxFile) {
		if (!cacheIdxFile.exists()) {
			return false;
		}
		File cacheDir = cacheIdxFile.getParentFile();
		for (String child : cacheDir.list()) {
			// check if at least one data directory exists
			if ("0".equals(child)) {
				return true;
			}
		}
		return false;
	}

	/** Compute the tile suffix. */
	protected String computeCacheDirSuffix() {
		return "_" + plate.computeAgeLayerIntervalStringForAge(tectonicModel.getAge());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Pair<Layer, ElevationModel>> load(File file, IProgressMonitor monitor) throws Exception {

		try {
			// Create image elevation file
			dataFile = createImageElevationData(file);

			// Read file
			dataFile.read();
		} catch (IOException e) {
			logger.error("Error during file reading: " + file.getAbsolutePath(), e);
		}

		// Create layers
		return createLayers(dataFile, monitor);
	}

	public MultiLayerTecImageElevationData getLoadedData() {
		return dataFile;
	}

}
