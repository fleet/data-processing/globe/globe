/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.drivers.tile;

import java.io.File;

import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;

/**
 * Parameter used to lead a tile creation. <br>
 * 
 * <pre>
 * Pattern of the tile folder cache : [TileName] _ [TileFormat][TileType] _ [Suffix]
 *  where : TileFormat = "" | "img_texFrom"
 *          TileType = "elevation" | "depth" (MTec file) | "DEPTH" (Tec file)
 *          Suffix = "" | |AgeMin] _ "AgeMax"
 * </pre>
 */
public class TileProductionParameters {

	/** Invoked to update the cache directory suffix */
	protected DirSuffixUpdater dirSuffixUpdater;
	/** Tec or MultiLayerTec data. */
	protected MultiLayerTecImageElevationData imageElevationData;
	/** Area of the tile. */
	protected Area area;
	/** Name of the tile. */
	protected String tileName;
	/** The XML document root to parse for layer configuration elements. */
	protected File configurationFile;
	/** All parameters used by NWW to store the tile. */
	protected AVList params;
	/** Cache location. */
	protected File cacheLocation;

	/**
	 * Constructor.
	 */
	TileProductionParameters() {
		params = new AVListImpl();
	}

	/**
	 * Invoke the DirSuffixUpdater
	 */
	public void updateCacheDirSuffix(String cacheDirSuffix) {
		dirSuffixUpdater.update(cacheDirSuffix);
	}

	/**
	 * Invoked to update the cache directory suffix
	 */
	@FunctionalInterface
	public static interface DirSuffixUpdater {
		void update(String cacheDirSuffix);
	}

	/**
	 * @return the {@link #dirSuffixUpdater}
	 */
	public DirSuffixUpdater getDirSuffixUpdater() {
		return dirSuffixUpdater;
	}

	/**
	 * @param dirSuffixUpdater
	 *            the {@link #dirSuffixUpdater} to set
	 */
	public void setDirSuffixUpdater(DirSuffixUpdater dirSuffixUpdater) {
		this.dirSuffixUpdater = dirSuffixUpdater;
	}

	/**
	 * @return the {@link #imageElevationData}
	 */
	public MultiLayerTecImageElevationData getImageElevationData() {
		return imageElevationData;
	}

	/**
	 * @param imageElevationData
	 *            the {@link #imageElevationData} to set
	 */
	public void setImageElevationData(MultiLayerTecImageElevationData imageElevationData) {
		this.imageElevationData = imageElevationData;
	}

	/**
	 * @return the {@link #area}
	 */
	public Area getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the {@link #area} to set
	 */
	public void setArea(Area area) {
		this.area = area;
	}

	/**
	 * @return the {@link #tileName}
	 */
	public String getTileName() {
		return tileName;
	}

	/**
	 * @param tileName
	 *            the {@link #tileName} to set
	 */
	public void setTileName(String tileName) {
		this.tileName = tileName;
	}

	/**
	 * @return the {@link #configurationFile}
	 */
	public File getConfigurationFile() {
		return configurationFile;
	}

	/**
	 * @param configurationFile
	 *            the {@link #configurationFile} to set
	 */
	public void setConfigurationFile(File configurationFile) {
		this.configurationFile = configurationFile;
	}

	/**
	 * @return the {@link #params}
	 */
	public AVList getParams() {
		return params;
	}

	/**
	 * @param params
	 *            the {@link #params} to set
	 */
	public void setParams(AVList params) {
		this.params = params;
	}

	/**
	 * @return the {@link #cacheLocation}
	 */
	public File getCacheLocation() {
		return cacheLocation;
	}

	/**
	 * @param cacheLocation
	 *            the {@link #cacheLocation} to set
	 */
	public void setCacheLocation(File cacheLocation) {
		this.cacheLocation = cacheLocation;
	}

}
