/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.drivers.tile;

import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.util.LevelSet;

import org.w3c.dom.Document;

/**
 * This class package the result of the production of TiledRasterProducerProxy.
 */
public class TileProduction {

	/** XML description document. */
	protected Document document;
	/** Generated levelSet. */
	protected LevelSet levelSet;
	/** Generated parameters. */
	protected AVList parameters;

	/**
	 * Constructor.
	 */
	public TileProduction() {
	}

	/**
	 * @return the {@link #document}
	 */
	public Document getDocument() {
		return document;
	}

	/**
	 * @param document
	 *            the {@link #document} to set
	 */
	public void setDocument(Document document) {
		this.document = document;
	}

	/**
	 * @return the {@link #levelSet}
	 */
	public LevelSet getLevelSet() {
		return levelSet;
	}

	/**
	 * @param levelSet
	 *            the {@link #levelSet} to set
	 */
	public void setLevelSet(LevelSet levelSet) {
		this.levelSet = levelSet;
	}

	/**
	 * @return the {@link #parameters}
	 */
	public AVList getParameters() {
		return parameters;
	}

	/**
	 * @param parameters
	 *            the {@link #parameters} to set
	 */
	public void setParameters(AVList parameters) {
		this.parameters = parameters;
	}

}
