/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.drivers.tile;

import java.io.File;

import fr.ifremer.globe.nasa.worldwind.WorldWindUtils;
import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import fr.ifremer.globe.ui.utils.CacheDirectory;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.util.WWIO;

/**
 * Factory of TileProductionParameters
 */
public class TileProductionParametersFactory {

	public static final String PROPERTY_DATA_CACHE_NAME_PREFIX = "DataCacheNamePrefix";
	public static final String PROPERTY_DATA_CACHE_NAME_SUFFIX = "DataCacheNameSuffix";

	/**
	 * Constructor.
	 */
	public TileProductionParametersFactory() {
	}

	/**
	 * Build a TileProductionParameters for a image tile
	 */
	protected TileProductionParameters makeTileImageParameters(MultiLayerTecImageElevationData imageElevationData,
			Area area, String displayName, String tileFormat, String cacheNameSuffix) {
		TileProductionParameters result = new TileProductionParameters();
		result.setImageElevationData(imageElevationData);
		result.setArea(area);
		result.setTileName(displayName);
		result.setCacheLocation(WorldWindUtils.getCacheLocation());

		result.getParams().setValue(AVKey.FILE_STORE_LOCATION, result.getCacheLocation().getAbsolutePath());
		String cacheNamePrefix = result.getTileName() + tileFormat;
		result.getParams().setValue(PROPERTY_DATA_CACHE_NAME_PREFIX, cacheNamePrefix);
		result.getParams().setValue(AVKey.DISPLAY_NAME, result.getTileName());
		result.getParams().setValue(AVKey.NAME, result.getTileName());

		assignDirSuffixUpdater(result);
		result.updateCacheDirSuffix(cacheNameSuffix);

		return result;
	}

	/**
	 * Build a TileProductionParameters for a image tile
	 */
	public TileProductionParameters makeTileImageParameters(MultiLayerTecImageElevationData imageElevationData,
			Area area, String cacheNameSuffix) {
		return makeTileImageParameters(imageElevationData, area, imageElevationData.getName(area) + "_img",
				"_img_texFromdepth", cacheNameSuffix);
	}

	/**
	 * Build a TileProductionParameters for a elevation tile
	 */
	public TileProductionParameters makeTileElevationParameters(MultiLayerTecImageElevationData imageElevationData,
			Area area, String cacheNameSuffix) {
		TileProductionParameters result = new TileProductionParameters();
		result.setImageElevationData(imageElevationData);
		result.setArea(area);
		result.setTileName(imageElevationData.getName(area) + "_elevation");
		result.setCacheLocation(WorldWindUtils.getCacheLocation());

		result.getParams().setValue(AVKey.FILE_STORE_LOCATION, result.getCacheLocation().getAbsolutePath());
		result.getParams().setValue(PROPERTY_DATA_CACHE_NAME_PREFIX, result.getTileName());
		result.getParams().setValue(AVKey.DISPLAY_NAME, result.getTileName());
		result.getParams().setValue(AVKey.NAME, result.getTileName());

		assignDirSuffixUpdater(result);
		result.updateCacheDirSuffix(cacheNameSuffix);

		return result;
	}

	/**
	 * Set the DirSuffixUpdater to the TileProductionParameters
	 */
	protected void assignDirSuffixUpdater(TileProductionParameters tileProductionParameters) {
		tileProductionParameters.setDirSuffixUpdater(cacheNameSuffix -> {
			String cacheDirName = WWIO.replaceIllegalFileNameCharacters(
					tileProductionParameters.getParams().getStringValue(PROPERTY_DATA_CACHE_NAME_PREFIX)
							+ cacheNameSuffix);
			String cacheName = CacheDirectory.getCacheSubDirectory() + "/" + cacheDirName;
			tileProductionParameters.setConfigurationFile(new File(tileProductionParameters.getCacheLocation().getPath()
					+ "/" + cacheName + "/" + cacheDirName + ".xml"));

			tileProductionParameters.getParams().setValue(AVKey.DATA_CACHE_NAME, cacheName);
			tileProductionParameters.getParams().setValue(AVKey.DATASET_NAME, cacheDirName);
			tileProductionParameters.getParams().setValue(PROPERTY_DATA_CACHE_NAME_SUFFIX, cacheNameSuffix);
		});
	}
}
