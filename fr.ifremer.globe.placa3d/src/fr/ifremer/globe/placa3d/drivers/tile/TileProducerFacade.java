/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.drivers.tile;

import java.io.File;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.placa3d.morphing.ElevationDataRasterReader;
import fr.ifremer.globe.placa3d.reader.TecElevationRasterReader;
import fr.ifremer.globe.placa3d.reader.TecImageRasterReader;
import fr.ifremer.viewer3d.data.MyTiledElevationProducer;
import fr.ifremer.viewer3d.data.MyTiledImageProducer;
import gov.nasa.worldwind.data.DataRasterReader;
import gov.nasa.worldwind.data.TiledElevationProducer;
import gov.nasa.worldwind.data.TiledImageProducer;

/**
 * Facade of tile production system.
 */
public class TileProducerFacade {

	/**
	 * Constructor.
	 */
	public TileProducerFacade() {
	}

	/**
	 * Creates all texture tiles for the specified tif file.
	 */
	public TileProduction generateImageTiles(TileProductionParameters tileProductionParameters, File tifFile) throws Exception {
		// Preparing
		TiledImageProducer imageProducer = new TiledImageProducer();
		TileProducerProxy producerProxy = new TileProducerProxy();
		// Launch the tile production
		return producerProxy.produceTiles(imageProducer, tileProductionParameters, tifFile);
	}

	/**
	 * Creates all texture tiles for the specified mtec file.
	 */
	public TileProduction generateTecImageTiles(TileProductionParameters tileProductionParameters, File mtecFile) throws Exception {
		// Preparing
		TecImageRasterReader imageReader = new TecImageRasterReader(tileProductionParameters.getImageElevationData(), tileProductionParameters.getArea());
		MyTiledImageProducer imageProducer = new MyTiledImageProducer("", IConstants.MeterUnit, imageReader.getValGlobaleMin(), imageReader.getValGlobaleMax(), imageReader.getValid_min(),
				imageReader.getValid_max());
		imageProducer.addReader(imageReader);
		TileProducerProxy producerProxy = new TileProducerProxy();
		// Launch the tile production
		return producerProxy.produceTiles(imageProducer, tileProductionParameters, mtecFile);
	}

	/**
	 * Creates all elevation tiles for the specified array file.
	 */
	public TileProduction generateElevationTiles(TileProductionParameters tileProductionParameters, File arrayFile) throws Exception {
		// Preparing
		ElevationDataRasterReader evevationDataRasterReader = new ElevationDataRasterReader();
		TiledElevationProducer elevationProducer = new TiledElevationProducer() {
			@Override
			protected DataRasterReader[] getDataRasterReaders() {
				return new DataRasterReader[] { evevationDataRasterReader };
			}
		};

		// Launch the tile production
		TileProducerProxy producerProxy = new TileProducerProxy();
		return producerProxy.produceTiles(elevationProducer, tileProductionParameters, arrayFile);
	}

	/**
	 * Creates all elevation tiles for the specified mtec file.
	 */
	public TileProduction generateTecElevationTiles(TileProductionParameters tileProductionParameters, File mtecFile) throws Exception {
		// Preparing
		MyTiledElevationProducer elevationProducer = new MyTiledElevationProducer(false);
		elevationProducer.addReader(new TecElevationRasterReader(tileProductionParameters.getImageElevationData(), tileProductionParameters.getArea()));
		// Launch the tile production
		TileProducerProxy producerProxy = new TileProducerProxy();
		return producerProxy.produceTiles(elevationProducer, tileProductionParameters, mtecFile);
	}

}
