package fr.ifremer.globe.placa3d.drivers.tile;

import java.awt.image.BufferedImage;
import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.ifremer.globe.ui.utils.image.ImageUtils;
import fr.ifremer.viewer3d.data.MyTiledElevationProducer;
import fr.ifremer.viewer3d.data.MyTiledImageProducer;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.data.TiledElevationProducer;
import gov.nasa.worldwind.data.TiledImageProducer;
import gov.nasa.worldwind.util.DataConfigurationUtils;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.WWXML;

/**
 * Class used to request a NWW tiles production.
 */
public class TileProducerProxy {

	/**
	 * Logger
	 */
	protected static final Logger logger = LoggerFactory.getLogger(TileProducerProxy.class);

	/**
	 * Constructor .
	 */
	public TileProducerProxy() {
	}

	/**
	 * Import the source image into the FileStore by converting it to the World
	 * Wind Java cache format.
	 * 
	 * @return XML description document
	 */
	public TileProduction produceTiles(MyTiledImageProducer imageProducer, TileProductionParameters tileProductionParameters, File mtecFile) throws Exception {
		TileProduction result = new TileProduction();

		// Check if tile cache already exists
		boolean cacheExists = checkCacheExists(tileProductionParameters, result);

		if (!cacheExists) {
			try {
				// Configure the TiledImageProducer with the parameter list and
				// the image source.
				imageProducer.setRecomputeCache(true);
				imageProducer.setStoreParameters(tileProductionParameters.getParams());
				imageProducer.offerDataSource(mtecFile, null);
				imageProducer.startProduction();

				result.setLevelSet(imageProducer.getLevels());
				result.setParameters(imageProducer.getProductionParameters());
				Iterable<?> results = imageProducer.getProductionResults();
				if (results != null && results.iterator().hasNext()) {
					result.setDocument((Document) results.iterator().next());
				}
			} catch (Exception e) {
				imageProducer.removeProductionState();
				throw e;
			}
		}
		return result;
	}

	/**
	 * Import the source image into the FileStore by converting it to the World
	 * Wind Java cache format.
	 * 
	 * @return XML description document
	 */
	public TileProduction produceTiles(TiledImageProducer imageProducer, TileProductionParameters tileProductionParameters, File imageSource) throws Exception {
		TileProduction result = new TileProduction();

		// Check if tile cache already exists
		boolean cacheExists = checkCacheExists(tileProductionParameters, result);

		if (!cacheExists) {
			try {
				// Configure the TiledImageProducer with the parameter list and
				// the image source.
				imageProducer.setStoreParameters(tileProductionParameters.getParams());

				BufferedImage rgbImage = ImageUtil.toCompatibleImage(ImageUtils.readTiff(imageSource));
				tileProductionParameters.getParams().setValue(AVKey.WIDTH, rgbImage.getWidth());
				tileProductionParameters.getParams().setValue(AVKey.HEIGHT, rgbImage.getHeight());
				imageProducer.offerDataSource(BufferedImageRaster.wrap(rgbImage, tileProductionParameters.getParams()), tileProductionParameters.getParams());
				imageProducer.startProduction();

				result.setParameters(imageProducer.getProductionParameters());
				Iterable<?> results = imageProducer.getProductionResults();
				if (results != null && results.iterator().hasNext()) {
					result.setDocument((Document) results.iterator().next());
				}
			} catch (Exception e) {
				imageProducer.removeProductionState();
				throw e;
			}
		}
		return result;
	}

	/**
	 * Import the source elevation data into the FileStore by converting it to
	 * the World Wind Java cache format. Tiles are generated and an XML
	 * description document is also generated to describe this DataStore.
	 * 
	 * @return the resulting production.
	 */
	public TileProduction produceTiles(MyTiledElevationProducer elevationProducer, TileProductionParameters tileProductionParameters, File mtecFile) throws Exception {
		TileProduction result = new TileProduction();

		// Check if tile cache already exists
		boolean cacheExists = checkCacheExists(tileProductionParameters, result);

		if (!cacheExists) {
			try {
				// Configure the TiledElevationProducer with the parameter list
				// and the elevation data source.
				elevationProducer.setRecomputeCache(true);
				elevationProducer.setStoreParameters(tileProductionParameters.getParams());
				elevationProducer.offerDataSource(mtecFile, null);
				elevationProducer.startProduction();

				result.setLevelSet(elevationProducer.getLevels());
				result.setParameters(elevationProducer.getProductionParameters());
				Iterable<?> results = elevationProducer.getProductionResults();
				if (results != null && results.iterator().hasNext()) {
					result.setDocument((Document) results.iterator().next());
				}
			} catch (Exception e) {
				elevationProducer.removeProductionState();
				throw e;
			}
		}
		return result;
	}

	/**
	 * Import the source elevation data into the FileStore by converting it to
	 * the World Wind Java cache format. Tiles are generated and an XML
	 * description document is also generated to describe this DataStore.
	 * 
	 * @return the resulting production.
	 */
	public TileProduction produceTiles(TiledElevationProducer elevationProducer, TileProductionParameters tileProductionParameters, File elevationFile) throws Exception {
		TileProduction result = new TileProduction();

		// Check if tile cache already exists
		boolean cacheExists = checkCacheExists(tileProductionParameters, result);

		if (!cacheExists) {
			try {
				// Configure the TiledElevationProducer with the parameter list
				// and the elevation data source.
				elevationProducer.setStoreParameters(tileProductionParameters.getParams());
				elevationProducer.offerDataSource(elevationFile, tileProductionParameters.getParams());
				elevationProducer.startProduction();

				result.setParameters(elevationProducer.getProductionParameters());
				Iterable<?> results = elevationProducer.getProductionResults();
				if (results != null && results.iterator().hasNext()) {
					result.setDocument((Document) results.iterator().next());
				}
			} catch (Exception e) {
				elevationProducer.removeProductionState();
				throw e;
			}
		}
		return result;
	}

	/**
	 * @return true if tile cache already exists and successfully reloaded
	 */
	protected boolean checkCacheExists(TileProductionParameters tileProductionParameters, TileProduction tileProduction) {
		boolean result = checkCacheExists(tileProductionParameters.getConfigurationFile());
		if (result) {
			try {
				tileProduction.setParameters(tileProductionParameters.getParams());
				Document doc = loadConfigurationFile(tileProductionParameters, tileProduction);
				if (doc != null) {
					tileProduction.setDocument(doc);
					LevelSet levelSet = new LevelSet(tileProduction.getParameters());
					if (tileProductionParameters.getParams().hasKey(AVKey.DISPLAY_NAME)) {
						levelSet.setValue(AVKey.DISPLAY_NAME, tileProductionParameters.getParams().getValue(AVKey.DISPLAY_NAME));
					}
					tileProduction.setLevelSet(levelSet);
				} else {
					result = false;
				}
			} catch (Exception e) {
				// Force cache regenerations
				result = false;
			}
		}
		return result;
	}

	/**
	 * Returns true if cache files already exist.
	 */
	protected boolean checkCacheExists(File cacheIdxFile) {
		if (!cacheIdxFile.exists()) {
			return false;
		}
		File cacheDir = cacheIdxFile.getParentFile();
		for (String child : cacheDir.list()) {
			// check if at least one data directory exists
			if ("0".equals(child)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Reload the configuration file and set the values on the image producer
	 */
	protected Document loadConfigurationFile(TileProductionParameters tileProductionParameters, TileProduction tileProduction) throws Exception {
		Document result = WWXML.openDocument(tileProductionParameters.getConfigurationFile().getAbsolutePath());
		if (result != null) {
			Element domElement = result.getDocumentElement();
			DataConfigurationUtils.getLevelSetConfigParams(domElement, tileProduction.getParameters());
		}
		return result;
	}

	/** @return the value of the tag. */
	protected String getDocumentString(Document result, String tag) {
		NodeList node = result.getElementsByTagName(tag);
		return node.item(0) != null ? (((Element) node.item(0)).getChildNodes()).item(0).getNodeValue() : null;
	}

	/** @return the double value of the tag. */
	protected double getDocumentDouble(Document result, String tag) {
		NodeList node = result.getElementsByTagName(tag);
		return Double.valueOf((((Element) node.item(0)).getChildNodes()).item(0).getNodeValue());
	}

}
