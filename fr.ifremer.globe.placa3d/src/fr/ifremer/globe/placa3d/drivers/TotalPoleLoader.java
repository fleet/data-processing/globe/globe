package fr.ifremer.globe.placa3d.drivers;

import fr.ifremer.globe.placa3d.file.IPoleRotationFile;
import fr.ifremer.globe.placa3d.file.totpol.TotalPoleFile;

/**
 * @author mgaro
 *
 */
public class TotalPoleLoader extends PoleRotationLoader {

	@Override
	protected IPoleRotationFile createIPoleRotationFile(String filename) {
		return new TotalPoleFile(filename);
	}

}
