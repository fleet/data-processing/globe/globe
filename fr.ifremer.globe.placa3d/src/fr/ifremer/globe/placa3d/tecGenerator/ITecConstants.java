package fr.ifremer.globe.placa3d.tecGenerator;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/***
 * Definition des noms de meta-attributs pour un fichier tec
 */
public class ITecConstants {

	// TEC files properties
	public static final String propertyCommCot = "COT comments";
	public static final String propertyCommPole = "Poles comments";
	public static final String propertyX_range = "Longitude range";
	public static final String propertyY_range = "Latitude range";
	public static final String propertyZ_range = "Elevation range";
	public static final String propertyDimension = "Dimension";
	public static final String propertyNumberOfPoles = "Number of poles";

	// Multilayer TEC files properties
	public static final String propertyHasMaskingGrid = "Has masking grid";
	public static final String propertyHasElevation = "Has elevation(s)";
	public static final String propertyHasTexture = "Has texture(s)";
	public static final String propertyMaskingGridDimension = "Masking grid dimensions";
	public static final String propertyElevationGridDimension = "Elevation grid dimensions";
	public static final String propertyTextureGridDimension = "Texture grid dimensions";
	public static final String propertyAgeLayersCount = "Number of age layers";
	public static final String propertyLayers = DicoBundle.getString("WORD_LAYERS");
	public static final String rangePattern = DicoBundle.getString("GENERIC_RANGE");

}
