package fr.ifremer.globe.placa3d.tecGenerator;

import java.util.ArrayList;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

/**
 * Useful method for tecGeneration.
 * 
 * @author D.riou
 */

public class TecGeneratorUtil {

	public final static double MISSING_VALUE = -Double.MAX_VALUE;

	/**
	 * compute difference between a_longMax and a_longMin
	 * 
	 * @param a_longMax
	 *            Longitude max
	 * @param a_longMin
	 *            Longitude min
	 * @return difference between a_longMax and a_longMin
	 */
	public static double diffLongitude(double a_LongMax, double a_LongMin) {
		double l_diff;

		l_diff = ((a_LongMax - a_LongMin) + 360.0) % 360.0;

		return (l_diff);
	}

	/**
	 * compute addition of longitude
	 * 
	 * @param a_LongStart
	 *            Longitude max
	 * @param a_longMin
	 *            Longitude min
	 * @return addition result
	 */
	public static double addLongitude(double a_LongStart, double a_LongToAdd) {
		double l_result;

		l_result = a_LongStart + a_LongToAdd;

		if (l_result > 180.0) {
			l_result = l_result - 360.0;
		}

		return l_result;
	}

	/**
	 * test if a_longToTest is between a_longMax and a_longMin
	 * 
	 * @param a_longMin
	 *            Longitude min
	 * @param a_longMax
	 *            Longitude max
	 * @param a_longToTest
	 *            Longitude to test
	 * @return true if a_longToTest is between a_longMax and a_longMin
	 */
	public static boolean isBetweenTwoLongitudes(double a_LongMin, double a_LongMax, double a_LongToTest) {
		boolean l_result;

		if (isOnLineDateChange(a_LongMin, a_LongMax) == true) {
			// Case on date line change
			if ((a_LongToTest >= -180.0 && a_LongToTest <= a_LongMax) || (a_LongToTest >= a_LongMin && a_LongToTest <= 180.0)) {
				l_result = true;
			} else {
				l_result = false;
			}
		} else {
			// Case not on date line change
			double longMax = Double.NaN;
			if (a_LongMin < 0 && a_LongMax < 0 && (a_LongMax - a_LongMin) < 0) {
				// The max is over 180�
				longMax = (a_LongMax + 360.0) % 360.0;
			} else {
				longMax = a_LongMax;
			}
			if ( a_LongToTest >= a_LongMin && a_LongToTest <= longMax) {
				l_result = true;
			} else {
				l_result = false;
			}
		}

		return l_result;
	}

	/**
	 * test if a_latToTest is between a_latMax and a_latMin
	 * 
	 * @param a_latMin
	 *            Latitude min
	 * @param a_latMax
	 *            Latitude max
	 * @param a_latToTest
	 *            Latitude to test
	 * @return true if a_latToTest is between a_latMax and a_latMin
	 */
	public static boolean isBetweenTwoLatitudes(double a_LatMin, double a_LatMax, double a_LatToTest) {

		if (a_LatToTest < a_LatMin || a_LatToTest > a_LatMax) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * test if two longitudes are on line date change
	 * 
	 * @param a_longMin
	 *            Longitude min
	 * @param a_longMax
	 *            Longitude max
	 * @return true if two longitudes are on line date change
	 */
	public static boolean isOnLineDateChange(double a_LongMin, double a_LongMax) {

		if (a_LongMax < 0 && a_LongMin > 0) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * order longitudes with the shortest distance in direction East to West
	 * 
	 * @param a_firstLong
	 *            First Longitude
	 * @param a_secondLong
	 *            Second Longitude
	 * @return longitues ordered
	 */
	public static double[] longitudeOrdered(double a_firstLong, double a_secondLong) {

		double[] l_result = new double[2];
		double l_diffLong;

		l_diffLong = diffLongitude(a_firstLong, a_secondLong);

		if (l_diffLong > 180.0) {
			l_result[0] = a_firstLong;
			l_result[1] = a_secondLong;
		} else {
			l_result[0] = a_secondLong;
			l_result[1] = a_firstLong;
		}

		return l_result;

	}

	/**
	 * Get the missing value of tec file
	 * 
	 * @return _MissingValue the missing value
	 */
	public static double getMissingValue() {
		return MISSING_VALUE;
	}
	/**
	 * Compute the global bounding box of all the .cot file
	 * 
	 * @param a_segment
	 *            segment of .cot file
	 * 
	 * @return bounding box Sector
	 */
	public static Sector computeGlobalBoundingBox(ArrayList<ArrayList<LatLon>> a_segmentList) {

		Sector l_currentBounding;

		double[] bounding = computeBoundingBox(a_segmentList);
		l_currentBounding = new Sector(Angle.fromDegreesLatitude(bounding[0]), Angle.fromDegreesLatitude(bounding[1]), Angle.fromDegreesLongitude(bounding[2]), Angle.fromDegreesLongitude(bounding[3]));

		return l_currentBounding;
	}
	/**
	 * compute the bounding box of segment list
	 * 
	 * @param a_segmentList
	 *            segments of .cot file
	 * 
	 * @return the bounding box: latmin, longmin, latmax, longmax
	 */
	public static double[] computeBoundingBox(ArrayList<ArrayList<LatLon>> a_segmentList) {
		double[] l_boundingBox = new double[4];
		double l_previousLong;
		double l_currentLong, l_currentLat;
		double l_minLong = 0;
		double l_maxLong = 0;
		double l_minNumericalLong = 0;
		double l_maxNumericalLong = 0;
		double l_minLat = 90;
		double l_maxLat = -90;
		double[] l_longitudeOrdered;
		double[] l_longitudeOrderedMin;
		boolean l_lineDateChange = false;

		// Init the bounding with the first segment which on two differents
		// longitudes
		l_previousLong = a_segmentList.get(0).get(0).longitude.degrees;
		for (int i = 0; i < a_segmentList.size(); i++) {
			for (int j = 0; j < a_segmentList.get(i).size(); j++) {
				l_currentLong = a_segmentList.get(i).get(j).longitude.degrees;
				if (l_previousLong != l_currentLong) {
					l_longitudeOrdered = TecGeneratorUtil.longitudeOrdered(l_previousLong, l_currentLong);
					l_minLong = l_longitudeOrdered[0];
					l_maxLong = l_longitudeOrdered[1];
					l_minNumericalLong = l_longitudeOrdered[0];
					l_maxNumericalLong = l_longitudeOrdered[1];
					break;
				}
				l_previousLong = l_currentLong;
			}
			if (l_minLong != 0) {
				break;
			}
		}

		// Compute the bounding longitude
		for (int i = 0; i < a_segmentList.size(); i++) {
			for (int j = 0; j < a_segmentList.get(i).size(); j++) {
				l_currentLong = a_segmentList.get(i).get(j).longitude.degrees;
				l_currentLat = a_segmentList.get(i).get(j).latitude.degrees;

				// Update the numerical min/max longitude (outside of line date
				// change)
				if (l_currentLong < l_minNumericalLong) {
					l_minNumericalLong = l_currentLong;
				}
				if (l_currentLong > l_maxNumericalLong) {
					l_maxNumericalLong = l_currentLong;
				}

				// Test if coast line in on line date change
				if (Math.abs(l_currentLong - l_previousLong) > 180.0) {
					l_lineDateChange = true;
				}

				// compute the current bounding latitude
				if (l_currentLat < l_minLat) {
					l_minLat = l_currentLat;
				}
				if (l_currentLat > l_maxLat) {
					l_maxLat = l_currentLat;
				}

				// compute the current bounding longitude
				if (TecGeneratorUtil.isBetweenTwoLongitudes(l_minLong, l_maxLong, l_currentLong) == false) {
					l_longitudeOrderedMin = TecGeneratorUtil.longitudeOrdered(l_minLong, l_currentLong);

					if (l_longitudeOrderedMin[0] == l_currentLong) {
						l_minLong = l_currentLong;
					} else {
						l_maxLong = l_currentLong;
					}
				}

				l_previousLong = l_currentLong;
			}
		}

		// Store the result
		l_boundingBox[0] = l_minLat;
		l_boundingBox[1] = l_maxLat;
		if (l_lineDateChange == true) {
			l_boundingBox[2] = l_minLong;
			l_boundingBox[3] = l_maxLong;
		} else {
			l_boundingBox[2] = l_minNumericalLong;
			l_boundingBox[3] = l_maxNumericalLong;
		}

		return l_boundingBox;
	}
}
