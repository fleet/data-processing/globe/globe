package fr.ifremer.globe.placa3d.tecGenerator;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.viewer3d.data.reader.raster.Raster;
import fr.ifremer.viewer3d.data.reader.raster.RasterAttributes;
import fr.ifremer.viewer3d.data.reader.raster.RasterReader;

/**
 * Reader of DTM files. <br>
 * Delegate the parsing of the file to {@link #RasterReader} <br>
 * Only data into a boundingBox are kept.
 * 
 */
public class ElevationReaderAdaptor implements IElevationReaderAdaptor {

	/** bounding box of elevation data: lat min, lat max, long min, long max */
	private double[] boundingBoxToRead;
	/** Read datas. */
	private double[][] z;

	// Size of data matrix extracted
	private int nbLinesExtracted = 0;
	private int nbColumnsExtracted = 0;

	/** All attributes describing a terrain. */
	protected final RasterAttributes rasterAttributes;
	protected final RasterReader rasterReader;

	/**
	 * Constructor.
	 */
	public ElevationReaderAdaptor(RasterReader terrainReader) {
		this.rasterReader = terrainReader;
		rasterAttributes = terrainReader.getRasterAttributes();
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public void setFileName(String a_fileName) {
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.placa3d.tecGenerator.IElevationReaderAdaptor#readElevations(double[])
	 */
	@Override
	public void readElevations(double[] boundingBox) throws Exception {

		boundingBoxToRead = boundingBox;

		// Read the bounding gbox
		GeoBox geoBox = rasterAttributes.getGeoBox();

		// Control that that Bounding box to extract is include into DTM GeoBox
		// boundingBox[2] : Min longitude
		if (TecGeneratorUtil.isBetweenTwoLongitudes(geoBox.getLeft(), geoBox.getRight(), boundingBox[2]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_ELEVATION_FILE_WEST_SIDE"));
		}
		// boundingBox[3] : Max Longitude
		if (TecGeneratorUtil.isBetweenTwoLongitudes(geoBox.getLeft(), geoBox.getRight(), boundingBox[3]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_ELEVATION_FILE_EAST_SIDE"));
		}
		// boundingBox[0] : Min Latitude
		if (TecGeneratorUtil.isBetweenTwoLatitudes(geoBox.getBottom(), geoBox.getTop(), boundingBox[0]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_ELEVATION_FILE_SOUTH_SIDE"));
		}
		// boundingBox[1] : Max Latitude
		if (TecGeneratorUtil.isBetweenTwoLatitudes(geoBox.getBottom(), geoBox.getTop(), boundingBox[1]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_ELEVATION_FILE_NORTH_SIDE"));
		}

		// Extract the data from the raster
		extractData();
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public double getElevation(int a_line, int a_col) {
		return z[a_line][a_col];
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public int getNbOfLines() {
		return nbLinesExtracted;
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public int getNbOfColumns() {
		return nbColumnsExtracted;
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public void clearData() {
		z = null;
	}

	/**
	 * extract the data from DTM read and store into _z attribute
	 * 
	 * @param no parameter
	 * @return no return
	 */
	protected void extractData() throws FileFormatException {
		double l_latMinGeotiff, l_latMaxGeotiff, l_longMinGeotiff, l_longMaxGeotiff;
		double l_latStep, l_longStep;
		double l_diffLong;
		int l_lineStartIndex, l_lineEndIndex, l_columnStartIndex;
		int l_jCurrIndex;
		double l_currentLong;

		// Compute the size of extracted values matrix
		GeoBox geoBox = rasterAttributes.getGeoBox();
		l_latMinGeotiff = geoBox.getBottom();
		l_latMaxGeotiff = geoBox.getTop();
		l_longMinGeotiff = geoBox.getLeft();
		l_longMaxGeotiff = geoBox.getRight();
		l_latStep = (l_latMaxGeotiff - l_latMinGeotiff) / rasterAttributes.getLines();
		l_diffLong = l_longMaxGeotiff - l_longMinGeotiff;
		l_longStep = l_diffLong / rasterAttributes.getColumns();

		// Initialize the extracted data matrix
		nbLinesExtracted = (int) ((boundingBoxToRead[1] - boundingBoxToRead[0]) / l_latStep);
		nbColumnsExtracted = (int) (TecGeneratorUtil.diffLongitude(boundingBoxToRead[3], boundingBoxToRead[2])
				/ l_longStep);
		z = new double[nbLinesExtracted][nbColumnsExtracted];

		Raster terrainBuffer = rasterReader.readFile();

		// Compute the line start index
		l_lineStartIndex = (int) ((l_latMaxGeotiff - boundingBoxToRead[1]) / l_latStep);
		if (l_lineStartIndex < 0) {
			l_lineStartIndex = 0;
		}

		// Compute the column start index
		l_columnStartIndex = (int) ((boundingBoxToRead[2] - l_longMinGeotiff) / l_longStep);
		if (l_columnStartIndex < 0) {
			l_columnStartIndex = 0;
		}

		// Compute the line end index
		l_lineEndIndex = (int) ((boundingBoxToRead[0] - l_latMinGeotiff) / l_latStep);
		if (l_lineEndIndex < 0) {
			l_lineEndIndex = 0;
		}
		l_lineEndIndex = (rasterAttributes.getLines() - l_lineEndIndex) - 1;

		// fill the matrix with datas read
		for (int line = 0; line < z.length; line++) {
			for (int column = 0; column < nbColumnsExtracted; column++) {
				l_jCurrIndex = column + l_columnStartIndex;
				if (l_jCurrIndex > rasterAttributes.getColumns() - 1) {
					l_jCurrIndex = l_jCurrIndex - rasterAttributes.getColumns();
				}
				l_currentLong = TecGeneratorUtil.addLongitude(l_longMinGeotiff,
						(column + l_columnStartIndex) * l_longStep);
				if (TecGeneratorUtil.isBetweenTwoLongitudes(boundingBoxToRead[2], boundingBoxToRead[3],
						l_currentLong) == true) {
					float depth = terrainBuffer.get(column, line);
					z[line][column] = depth != Raster.NO_VALUE ? depth : TecGeneratorUtil.MISSING_VALUE;
				} else {
					z[line][column] = TecGeneratorUtil.MISSING_VALUE;
				}
			}
		}
	}

	/**
	 * @return the {@link #rasterReader}
	 */
	public RasterReader getRasterReader() {
		return rasterReader;
	}

	/**
	 * @param boundingBoxToRead the {@link #boundingBoxToRead} to set
	 */
	public void setBoundingBox(double[] boundingBoxToRead) {
		this.boundingBoxToRead = boundingBoxToRead;
	}

	/**
	 * @return the {@link #rasterAttributes}
	 */
	public RasterAttributes getRasterAttributes() {
		return rasterAttributes;
	}

}
