package fr.ifremer.globe.placa3d.tecGenerator;

import jakarta.inject.Inject;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.programs.GdalWarp;

/**
 * Implementation of IElevationReaderAdaptor for Netcdf file format as defined in detailed conception.
 * 
 * @author D.riou
 */
public class FileGmtReader implements IElevationReaderAdaptor {

	@Inject
	private IFileService fileService;
	@Inject
	private IRasterService rasterService;

	/** File path */
	private String fileName;

	/** RasterInfo extracted in fileName */
	private RasterInfo rasterInfo;
	/** Tmp file containing the result of the warp */
	private String tempFile;
	/** dataset opened on tempFile */
	private Dataset dataset;
	/** first Band of dataset */
	private Band band;

	/** {@inheritDoc} */
	@Override
	public void readElevations(double[] boundingBox) throws Exception {
		fileService.getFileInfo(fileName).ifPresent(fileInfo -> {
			if (fileInfo.getContentType() == ContentType.GMT) {
				// Only one raster in GMT
				readElevations(boundingBox, rasterService.getRasterInfos(fileInfo).get(0));
			}
		});
	}

	/** {@inheritDoc} */
	@Override
	public double getElevation(int line, int col) {
		double[] result = { 0d };
		band.ReadRaster(col, line, 1, 1, result);
		return result[0];
	}

	/** {@inheritDoc} */
	@Override
	public int getNbOfLines() {
		return band != null ? band.getYSize() : 0;
	}

	@Override
	public int getNbOfColumns() {
		return band != null ? band.getXSize() : 0;
	}

	/** {@inheritDoc} */
	@Override
	public void clearData() {
		if (dataset != null) {
			dataset.delete();
			gdal.Unlink(tempFile);
			dataset = null;
		}
	}

	/**
	 * @param snwe, Extent as MinLatitude, MaxLatitude, MinLongitude, MaxLongitude
	 */
	private void readElevations(double[] snwe, RasterInfo rasterInfo) {
		this.rasterInfo = rasterInfo;
		try (GdalDataset gdalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey())) {
			gdalDataset.deleteOnClose(true);
			Dataset rasterDataset = gdalDataset.orElseThrow();
			tempFile = GdalUtils.generateInMemoryFilePath();
			GdalWarp gdalWarp = new GdalWarp(rasterDataset, tempFile);
			gdalWarp.addTargetProjection(GdalOsrUtils.SRS_WGS84);
			gdalWarp.addTargetExtent(new double[] { snwe[2], snwe[0], snwe[3], snwe[1] });
			this.dataset = gdalWarp.run();
			this.band = this.dataset.GetRasterBand(1);
		}
	}

	/**
	 * @return the {@link #fileName}
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the {@link #fileName} to set
	 */
	@Override
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
