package fr.ifremer.globe.placa3d.tecGenerator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.gdal.ogr.Geometry;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

/**
 * Implementation of ICotReaderAdaptor for Netcdf file format as defined in detailed conception.
 * 
 * @author D.riou
 */
public class FileCotReader implements ICotReaderAdaptor {

	/**
	 * Attributes
	 */
	String[] fileNames; // file name

	ArrayList<ArrayList<LatLon>> segmentsList = new ArrayList<ArrayList<LatLon>>(); // Segments
	// list

	int segmentIndex = -1; // Segment index used for read of .cot file list

	/**
	 * Comments of the cot file (with # characters)
	 */
	String comments = new String();

	/**
	 * Implementation of ICotReaderAdaptor method.
	 */
	@Override
	public void setFileNameList(String[] a_fileNames) {
		fileNames = a_fileNames;
	}

	/**
	 * Implementation of ICotReaderAdaptor method.
	 */
	@Override
	public void readCots() throws Exception {

		for (int i = 0; i < fileNames.length; i++) {
			readCot(fileNames[i]);
		}

	}

	/**
	 * read file and compute bounding box
	 * 
	 * @return
	 * @throws Exception
	 */
	public GeoBox computeBoundingBoxWithGDAL() throws Exception {
		// Read .cot files
		readCots();
		// Create polygon
		Geometry polygon = fr.ifremer.globe.nasa.worldwind.GdalUtils.makePolygon(getSegmentsList());
		// Keep only the perimeter of the geometry to eliminate self-loop.
		Geometry polygonSimplified = polygon.Buffer(0.);
		double[] boundingBox = GdalUtils.getBoundingBox(polygon);
		GeoBox box = new GeoBox(boundingBox);
		polygonSimplified.delete();
		polygon.delete();
		return box;
	}

	@Override
	public Sector computeBoundingBox() throws Exception {
		ArrayList<ArrayList<LatLon>> l_Segmentlist = new ArrayList<ArrayList<LatLon>>();
		// Read .cot files
		readCots();
		// Compute the global bounding box of the .cot files
		for (int i = 0; i < getNbOfSegments(); i++) {
			l_Segmentlist.add(getSegment(i));
		}
		Sector l_currentBounding;

		double[] bounding = TecGeneratorUtil.computeBoundingBox(l_Segmentlist);
		l_currentBounding = new Sector(Angle.fromDegreesLatitude(bounding[0]), Angle.fromDegreesLatitude(bounding[1]),
				Angle.fromDegreesLongitude(bounding[2]), Angle.fromDegreesLongitude(bounding[3]));

		return l_currentBounding;
	}

	/**
	 * Implementation of ICotReaderAdaptor method.
	 */
	@Override
	public int getNbOfSegments() {
		return segmentsList.size();
	}

	/**
	 * Implementation of ICotReaderAdaptor method.
	 */
	@Override
	public ArrayList<LatLon> getSegment(int a_segmentindex) {
		return segmentsList.get(a_segmentindex);
	}

	/**
	 * Put the file name of the file to read
	 * 
	 * @param a_fileName File name
	 * @return no return
	 */
	protected void readCot(String a_fileName) throws TecGeneratorException {
		String tmp;
		Scanner scanner;
		String[] tmpTab;
		List<String> tmpList = new ArrayList<String>();
		boolean commentsEnd = false;
		// int l_pointIndex = 0;
		// boolean l_firstLine = true;

		// String builder for comments
		StringBuilder sbComments = new StringBuilder();

		try {
			scanner = new Scanner(new FileInputStream(a_fileName));
			try {
				while (scanner.hasNextLine()) {
					tmp = scanner.nextLine();

					// line interpretation
					if (tmp.contains(">"))// segment begin marker
					{
						commentsEnd = true;

						segmentsList.add(new ArrayList<LatLon>());
						segmentIndex++;
						// l_pointIndex = 0;
					} else if (tmp.contains(TectonicUtils.POUND_CHARACTER))// comments
					{
						if (commentsEnd == false) {
							// String builder for comments
							if (sbComments.toString().isEmpty()) {
								sbComments.append(tmp.substring(1));
							} else {

								sbComments.append(" - " + tmp.substring(1));
							}
						}

					} else if (tmp.isEmpty() == true)// empty line
					{
						// nothing
					} else if (tmp.contains("<"))// plate name
					{
						commentsEnd = true;
						// layer name is plate name
					} else {
						commentsEnd = true;
						// Extract the lat long of the current line
						tmpTab = tmp.split(TectonicUtils.WHITE_CARACTHERS_REGEX);
						tmpList.clear();
						for (String string : tmpTab) {
							if (!string.isEmpty()) {
								tmpList.add(string);
							}
						}
						if (tmpList.size() >= 2) {
							double longitude = Double.parseDouble(tmpList.get(0));
							double latitude = Double.parseDouble(tmpList.get(1));
							segmentsList.get(segmentIndex).add(new LatLon(Angle.fromDegreesLatitude(latitude),
									Angle.fromDegreesLongitude(longitude)));
							// l_pointIndex++;
						}
					}
				}
			} finally {
				if (!sbComments.toString().isEmpty()) {
					sbComments.append("\n");
					comments += sbComments.toString();
				}
				scanner.close();
			}
		} catch (FileNotFoundException e) {
			throw new TecGeneratorException(e);
		}
	}

	public String getComments() {
		return comments;
	}

	/**
	 * @return the {@link #segmentsList}
	 */
	public ArrayList<ArrayList<LatLon>> getSegmentsList() {
		return segmentsList;
	}

}
