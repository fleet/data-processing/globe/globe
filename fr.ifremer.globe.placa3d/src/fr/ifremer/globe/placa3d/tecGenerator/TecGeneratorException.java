package fr.ifremer.globe.placa3d.tecGenerator;

import fr.ifremer.globe.utils.exception.GException;

/**
 * Exception for tec generator.
 * 
 * @author D.riou
 */
public class TecGeneratorException extends GException {

	/** UID. */
	private static final long serialVersionUID = -4129022083222024573L;

	public TecGeneratorException(String message, Throwable cause) {
		super(message, cause);
	}

	public TecGeneratorException(String message) {
		super(message);
	}

	public TecGeneratorException(Throwable cause) {
		super("Error while generating tec file",cause);
	}

	@Override
	public String toString() {
		return getMessage();
	}

}
