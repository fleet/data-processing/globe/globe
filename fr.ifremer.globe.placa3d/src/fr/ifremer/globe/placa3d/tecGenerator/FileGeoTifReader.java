package fr.ifremer.globe.placa3d.tecGenerator;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.utils.OSUtils;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.data.GDALDataRasterReader;
import gov.nasa.worldwind.geom.Sector;

/**
 * Implementation of IElevationReaderAdaptor for GeoTiff file format as defined in detailed conception.
 * 
 * @author D.riou
 */
public class FileGeoTifReader implements IElevationReaderAdaptor {

	/**
	 * logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(FileGeoTifReader.class);

	/**
	 * Attributes
	 */
	String fileName; // File name
	private double[] boundingBoxToRead; // bounding box of elevation data: lat
	// min, lat max, long min, long max
	private String[] gdalEnv = { "GDAL_DATA=C:\\Globe\\org.gdal.win32\\data", "" }; // data
	// path,
	// only
	// for
	// debug/logging
	private String gdalTranslate = "C:\\Temp\\otus_utils\\gdal_translate"; // command
	// name,
	// only
	// for
	// debug/logging
	private double[][] z; // GeoTiff elevation data
	BufferedImage image; // Raster read
	Sector sector; // Bounding box of the geotiff file

	// Size of data matrix read
	int nbLines = 0;
	int nbColumns = 0;

	// Size of data matrix extracted
	int nbLinesExtracted = 0;
	int nbColumnsExtracted = 0;

	/**
	 * Constructor
	 */
	public FileGeoTifReader() throws Exception {
		String root;

		// retrieves the workspace root

		// retrieves GDAL binaries path
		try {
			URL fileUrl = null;
			root = null;
			if (OSUtils.isWindows()) {
				fileUrl = Platform.getBundle("org.gdal.win32.x86_64").getEntry("/");
				root = FileLocator.resolve(fileUrl).getFile();
			} else if (OSUtils.isUnix()) {
				fileUrl = Platform.getBundle("org.gdal.linux.x86_64").getEntry("/");
				// root = FileLocator.resolve(fileUrl).getFile();
				// GDAL_ENV[1] = "LD_LIBRARY_PATH=" + root;
				root = "";
				gdalEnv[1] = System.getProperty("java.library.path");
			} else if (OSUtils.isMac()) {
				fileUrl = Platform.getBundle("org.gdal.macosx.x86_64").getEntry("/");
				// root = FileLocator.resolve(fileUrl).getFile();
				// GDAL_ENV[1] = "LD_LIBRARY_PATH=" + root;
				root = "";
				gdalEnv[1] = System.getProperty("java.library.path");
			}

			LOGGER.debug("gdal binaries root path {}", root);
			if (root != null) {
				gdalTranslate = root + "gdal_translate";
				if (OSUtils.isUnix() || OSUtils.isMac()) {
					File f = new File(gdalTranslate);
					if (f.exists()) {
						f.setExecutable(true);
					}
				}
			}

			// retrieve GDAL parameters files
			fileUrl = Platform.getBundle("org.gdal").getEntry("/");
			if (fileUrl != null) {
				root = FileLocator.resolve(fileUrl).getFile();
				LOGGER.debug("gdal parameters root path {}", root);
				String env = root + "data";
				File envPath = new File(env);
				gdalEnv[0] = "GDAL_DATA=" + envPath.getAbsolutePath();
			}

		} catch (IOException ex) {
			throw new TecGeneratorException("Error while searching for gdal path", ex);
		}
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public void setFileName(String a_fileName) {
		fileName = a_fileName;
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public void readElevations(double[] a_boundingBox) throws Exception {
		boundingBoxToRead = a_boundingBox;
		GDALDataRasterReader reader = new GDALDataRasterReader();

		// Before reading the raster, verify that the file contains imagery.
		AVList metadata = null;
		try {
			metadata = reader.readMetadata(fileName, null);
		} catch (Exception e) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE"));
		}
		if (metadata == null || !AVKey.IMAGE.equals(metadata.getStringValue(AVKey.PIXEL_FORMAT))) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE"));
		}
		// Read the file into the raster. read() returns potentially several
		// rasters if there are multiple
		// files, but in this case there is only one so just use the first
		// element of the returned array.
		DataRaster[] rasters = reader.read(fileName, null);

		if (rasters == null || rasters.length == 0) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE"));
		}

		DataRaster raster = rasters[0];
		// Determine the sector covered by the image. This information is in the
		// GeoTIFF file or auxiliary
		// files associated with the image file.
		sector = (Sector) raster.getValue(AVKey.SECTOR);
		if (sector == null) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_NO_LOCATION_TEXTURE_FILE"));
		}
		double southLatitude = sector.getMinLatitude().degrees;
		double northLatitude = sector.getMaxLatitude().degrees;
		double westLongitude = sector.getMinLongitude().degrees;
		double eastLongitude = sector.getMaxLongitude().degrees;
		// Control that that Bounding box to extract is include into DTM GeoBox
		// boundingBox[2] : Min longitude
		if (TecGeneratorUtil.isBetweenTwoLongitudes(westLongitude, eastLongitude, a_boundingBox[2]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE_WEST_SIDE"));
		}
		// boundingBox[3] : Max Longitude
		if (TecGeneratorUtil.isBetweenTwoLongitudes(westLongitude, eastLongitude, a_boundingBox[3]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE_EAST_SIDE"));
		}
		// boundingBox[0] : Min Latitude
		if (TecGeneratorUtil.isBetweenTwoLatitudes(southLatitude, northLatitude, a_boundingBox[0]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE_SOUTH_SIDE"));
		}
		// boundingBox[1] : Max Latitude
		if (TecGeneratorUtil.isBetweenTwoLatitudes(southLatitude, northLatitude, a_boundingBox[1]) == false) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE_NORTH_SIDE"));
		}

		// Request a sub-raster that contains the whole image. This step is
		// necessary because only sub-rasters
		// are reprojected (if necessary); primary rasters are not.
		int width = raster.getWidth();
		int height = raster.getHeight();

		// getSubRaster() returns a sub-raster of the size specified by width
		// and height for the area indicated
		// by a sector. The width, height and sector need not be the full width,
		// height and sector of the data,
		// but we use the full values of those here because we know the full
		// size isn't huge. If it were huge
		// it would be best to get only sub-regions as needed or install it as a
		// tiled image layer rather than
		// merely import it.
		DataRaster subRaster = raster.getSubRaster(width, height, sector, null);

		// Tne primary raster can be disposed now that we have a sub-raster.
		// Disposal won't affect the
		// sub-raster.
		raster.dispose();

		// Verify that the sub-raster can create a BufferedImage, then create
		// one.
		if (!(subRaster instanceof BufferedImageRaster)) {
			throw new TecGeneratorException(DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE"));
		}
		image = ((BufferedImageRaster) subRaster).getBufferedImage();
		nbLines = image.getHeight();
		nbColumns = image.getWidth();

		// Extract the data from the raster
		extractData();

	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public double getElevation(int a_line, int a_col) {
		return z[a_line][a_col];
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public int getNbOfLines() {
		return nbLinesExtracted;
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public int getNbOfColumns() {
		return nbColumnsExtracted;
	}

	/**
	 * Implementation of ElevationReaderAdaptor method.
	 */
	@Override
	public void clearData() {
		z = null;
		System.gc();
	}

	/**
	 * extract the data from raster read and store into _z attribute
	 * 
	 * @param no parameter
	 * @return no return
	 */
	protected void extractData() {
		int l_val;
		// byte l_val1, l_val2, l_val3;
		double l_latMinGeotiff, l_latMaxGeotiff, l_longMinGeotiff, l_longMaxGeotiff;
		double l_latStep, l_longStep;
		double l_diffLong;
		int l_lineStartIndex, l_lineEndIndex, l_columnStartIndex;
		int l_iCurrIndex, l_jCurrIndex;
		double l_currentLong;

		// Compute the size of extracted values matrix
		l_latMinGeotiff = sector.getMinLatitude().degrees;
		l_latMaxGeotiff = sector.getMaxLatitude().degrees;
		l_longMinGeotiff = sector.getMinLongitude().degrees;
		l_longMaxGeotiff = sector.getMaxLongitude().degrees;
		l_latStep = (l_latMaxGeotiff - l_latMinGeotiff) / nbLines;
		l_diffLong = l_longMaxGeotiff - l_longMinGeotiff;
		l_longStep = l_diffLong / nbColumns;

		// Initialize the extracted data matrix
		nbLinesExtracted = (int) ((boundingBoxToRead[1] - boundingBoxToRead[0]) / l_latStep);
		nbColumnsExtracted = (int) (TecGeneratorUtil.diffLongitude(boundingBoxToRead[3], boundingBoxToRead[2])
				/ l_longStep);
		z = new double[nbLinesExtracted][nbColumnsExtracted];

		// Initialize the _z matrix with 0
		for (int i = 0; i < z.length; i++) {
			for (int j = 0; j < z[0].length; j++) {
				z[i][j] = 0;
			}
		}

		// Compute the line start index
		l_lineStartIndex = (int) ((l_latMaxGeotiff - boundingBoxToRead[1]) / l_latStep);
		if (l_lineStartIndex < 0) {
			l_lineStartIndex = 0;
		}

		// Compute the column start index
		l_columnStartIndex = (int) ((boundingBoxToRead[2] - l_longMinGeotiff) / l_longStep);
		if (l_columnStartIndex < 0) {
			l_columnStartIndex = 0;
		}

		// Compute the line end index
		l_lineEndIndex = (int) ((boundingBoxToRead[0] - l_latMinGeotiff) / l_latStep);
		if (l_lineEndIndex < 0) {
			l_lineEndIndex = 0;
		}
		l_lineEndIndex = (nbLines - l_lineEndIndex) - 1;

		// fill the matrix with datas read
		l_iCurrIndex = l_lineStartIndex;
		for (int i = 0; i < z.length; i++) {
			for (int j = 0; j < nbColumnsExtracted; j++) {
				l_jCurrIndex = j + l_columnStartIndex;
				if (l_jCurrIndex > nbColumns - 1) {
					l_jCurrIndex = l_jCurrIndex - nbColumns;
				}
				l_val = image.getRGB(l_jCurrIndex, l_iCurrIndex); // Blue last
				// byte,
				// Green
				// (last -
				// 1) byte,
				// Blue
				// (last -
				// 2) byte
				l_currentLong = TecGeneratorUtil.addLongitude(l_longMinGeotiff, (j + l_columnStartIndex) * l_longStep);
				if (TecGeneratorUtil.isBetweenTwoLongitudes(boundingBoxToRead[2], boundingBoxToRead[3],
						l_currentLong) == true) {
					z[i][j] = l_val;
				}
			}
			l_iCurrIndex++;
		}
	}

}
