package fr.ifremer.globe.placa3d.tecGenerator;

import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

import java.util.ArrayList;

/**
 * This interface defines the .cot data used for entry of ElevationExtractor
 * class.
 * 
 * @author D.riou
 */
public interface ICotReaderAdaptor {

	/**
	 * Put the file name of the file to read
	 * 
	 * @param a_fileName
	 *            File name
	 * @return no return
	 */
	public void setFileNameList(String[] a_fileNames);

	/**
	 * Read the .cot files
	 * 
	 * @return no return
	 */
	public void readCots() throws Exception;

	/**
	 * Return the number of segments
	 * 
	 * @return number of segments
	 */
	public int getNbOfSegments();

	/**
	 * Return the point list of the segment pointed by index
	 * 
	 * @param a_segmentindex
	 *            Index in the segments list
	 * 
	 * @return list of points in lat long
	 */
	public ArrayList<LatLon> getSegment(int a_segmentindex);

	public Sector computeBoundingBox() throws Exception;
}
