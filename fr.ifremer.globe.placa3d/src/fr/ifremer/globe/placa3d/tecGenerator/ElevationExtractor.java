package fr.ifremer.globe.placa3d.tecGenerator;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

import java.util.ArrayList;
import java.util.stream.DoubleStream;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Class for elevation extractor from elevation data (IElevationReaderAdaptor) inside a polygon (ICotRedaerAdaptor).
 * 
 * @author D.riou
 */
public class ElevationExtractor implements IElevationMatrix {

	protected IElevationReaderAdaptor elevationReader; // Elevation reader
	protected ICotReaderAdaptor cotReader; // Cot reader

	protected Sector boundingBoxNasa; // Bounding box of extracted elevations
	protected double[] boundingBox = new double[4];
	protected double latitudeStep;
	protected double longitudeStep;
	protected double[][] elevationData;

	/**
	 * Constructor
	 * 
	 * @param a_elevationReader
	 *            Elevation reader
	 * @param a_cotReader
	 *            Cot reader
	 * @return no return
	 */
	public ElevationExtractor(IElevationReaderAdaptor a_elevationReader, ICotReaderAdaptor a_cotReader) {
		elevationReader = a_elevationReader;
		cotReader = a_cotReader;
	}

	/**
	 * Compute the extraction
	 * 
	 * @return no return
	 */
	public void computeExtraction(Sector ouputSector, IProgressMonitor monitor) throws Exception {



		boundingBoxNasa = ouputSector;

		// Read the elevation data included in the global bounding box
		boundingBox[0] = boundingBoxNasa.getMinLatitude().degrees;
		boundingBox[1] = boundingBoxNasa.getMaxLatitude().degrees;
		boundingBox[2] = boundingBoxNasa.getMinLongitude().degrees;
		boundingBox[3] = boundingBoxNasa.getMaxLongitude().degrees;

		// manage progress bar
		monitor.subTask("read data from disk");
		monitor.worked(15);

		// if( elevationReader instanceof ElevationReaderAdaptor &&
		// ((ElevationReaderAdaptor)elevationReader).terrainReader.)
		computeExtraction(monitor);

	}

	protected void computeExtraction(IProgressMonitor monitor) throws Exception {
		ArrayList<LatLon> l_currentSegment;

		// Read elevation
		elevationReader.readElevations(boundingBox);

		boolean [][]insideFlagElevation = new boolean[elevationReader.getNbOfLines()][elevationReader.getNbOfColumns()];
		// Compute the latitude step
		latitudeStep = (boundingBoxNasa.getMaxLatitude().degrees - boundingBoxNasa.getMinLatitude().degrees) / elevationReader.getNbOfLines();

		// Compute the longitude step
		longitudeStep = (((boundingBoxNasa.getMaxLongitude().degrees - boundingBoxNasa.getMinLongitude().degrees) + 360.0) % 360.0) / elevationReader.getNbOfColumns();



		// Initialize the _elevationData matrix to missing value
		elevationData = new double[elevationReader.getNbOfLines()][elevationReader.getNbOfColumns()];
		for (int i = 0; i < elevationReader.getNbOfLines(); i++) {
			elevationData[i] = DoubleStream.generate(() -> TecGeneratorUtil.MISSING_VALUE).limit(elevationReader.getNbOfColumns()).toArray();
			// for (int j = 0; j < elevationReader.getNbOfColumns(); j++) {
			// elevationData[i][j] = TecGeneratorUtil.MISSING_VALUE;
			// }
		}

		// manage progress bar
		monitor.subTask("Compute data inside polygons");
		monitor.worked(15);

		// Compute the inside search for each cot segment and set to true the
		// elevation inside
		double l_progressBarStep = 30.0 / cotReader.getNbOfSegments();
		double l_progressOneStep = 0;
		// Compute the inside without memory
		for (int i = 0; i < cotReader.getNbOfSegments(); i++) {
			l_currentSegment = cotReader.getSegment(i);
			// Compute inside for current segment
			computeInside(l_currentSegment,insideFlagElevation);
			// manage progress bar
			l_progressOneStep = l_progressOneStep + l_progressBarStep;
			if (l_progressOneStep >= 1) {
				monitor.worked((int) l_progressOneStep);
				l_progressOneStep = 0;
			}
		}

		// Fill the _elevationData matrix with inside elevations
		for (int line = 0; line < elevationReader.getNbOfLines(); line++) {
			for (int column = 0; column < elevationReader.getNbOfColumns(); column++) {
				if (insideFlagElevation[line][column] == true) {
					double elevation = elevationReader.getElevation(line, column);
					if (elevation == elevation) { // equiv. to Double.isNaN(elevation) == false
						// System.err.println("elevation = " + elevation);
						elevationData[line][column] = elevation;
					}
				}
			}
		}

		// Free the _elevationReader
		elevationReader.clearData();
	}

	@Override
	public double[][] getElevationMatrix() {
		return elevationData;
	}

	@Override
	public double[] getBoundingBox() {
		return boundingBox;
	}

	//	/**
	//	 * Compute the global bounding box of all the .cot file
	//	 * 
	//	 * @param a_segment
	//	 *            segment of .cot file
	//	 * 
	//	 * @return bounding box Sector
	//	 */
	//	protected Sector computeGlobalBoundingBox(ArrayList<ArrayList<LatLon>> a_segmentList) {
	//
	//		Sector l_currentBounding;
	//
	//		double[] bounding = computeBoundingBox(a_segmentList);
	//		l_currentBounding = new Sector(Angle.fromDegreesLatitude(bounding[0]), Angle.fromDegreesLatitude(bounding[1]), Angle.fromDegreesLongitude(bounding[2]), Angle.fromDegreesLongitude(bounding[3]));
	//
	//		return l_currentBounding;
	//	}

	/**
	 * Compute the inside search for cot segment and set to true the elevation inside in the _InsideFlagElevation matrix
	 * 
	 * @param a_segment
	 *            segment of .cot file
	 * 
	 * @return no return
	 */
	protected void computeInside(ArrayList<LatLon> a_segment,boolean [][] insideFlagElevation) {

		Sector l_boundingBoxSegment;
		int l_latitudeStartIndexInMatrix;
		int l_longitudeStartIndexInMatrix;
		int l_latitudeDeltaIndexInMatrix;
		int l_longitudeDeltaIndexInMatrix;
		double l_diffLongitude;
		double l_currentLatitude;
		double l_currentLongitude;
		boolean l_inside;
		ArrayList<ArrayList<LatLon>> l_segmentList = new ArrayList<ArrayList<LatLon>>();
		l_segmentList.add(a_segment);
		boolean l_lineDateChange = false;
		double l_shift;

		// Compute the bounding box of the current segment
		l_boundingBoxSegment = TecGeneratorUtil.computeGlobalBoundingBox(l_segmentList);

		// Compute if line date change
		l_lineDateChange = TecGeneratorUtil.isOnLineDateChange(l_boundingBoxSegment.getMinLongitude().degrees, l_boundingBoxSegment.getMaxLongitude().degrees);
		if (l_lineDateChange == true) {
			// Shift to be out of line date change
			l_shift = 180.0 + l_boundingBoxSegment.getMaxLongitude().degrees;
		} else {
			l_shift = 0.0;
		}

		// Add the first point to the end of segment. This is necessary for
		// isLocationInside method
		a_segment.add(a_segment.get(0));

		// Compute the start line in elevation matrix corresponding to the
		// Latitude Max
		l_latitudeStartIndexInMatrix = (int) ((boundingBoxNasa.getMaxLatitude().degrees - l_boundingBoxSegment.getMaxLatitude().degrees) / latitudeStep);
		if (l_latitudeStartIndexInMatrix < 0) {
			l_latitudeStartIndexInMatrix = 0;
		}

		// Compute the end line in elevation matrix corresponding to the
		// Latitude Min
		l_latitudeDeltaIndexInMatrix = (int) ((l_boundingBoxSegment.getMaxLatitude().degrees - l_boundingBoxSegment.getMinLatitude().degrees) / latitudeStep);

		// Compute the start column in elevation matrix corresponding to the
		// Longitude Min
		l_diffLongitude = TecGeneratorUtil.diffLongitude(l_boundingBoxSegment.getMinLongitude().degrees, boundingBoxNasa.getMinLongitude().degrees);
		l_longitudeStartIndexInMatrix = (int) (l_diffLongitude / longitudeStep);
		if (l_longitudeStartIndexInMatrix > (elevationReader.getNbOfColumns() - 1)) {
			l_longitudeStartIndexInMatrix = elevationReader.getNbOfColumns() - 1;
		}

		// Compute the end column in elevation matrix corresponding to the
		// Longitude Max
		l_diffLongitude = TecGeneratorUtil.diffLongitude(l_boundingBoxSegment.getMaxLongitude().degrees, l_boundingBoxSegment.getMinLongitude().degrees);
		l_longitudeDeltaIndexInMatrix = (int) (l_diffLongitude / longitudeStep);

		// Compute the inside test on bounding box of the segment
		for (int i = 0; i < l_latitudeDeltaIndexInMatrix; i++) {
			for (int j = 0; j < l_longitudeDeltaIndexInMatrix; j++) {
				// Compute current position in matrix
				l_currentLatitude = l_boundingBoxSegment.getMaxLatitude().degrees - (i * latitudeStep);
				l_currentLongitude = TecGeneratorUtil.addLongitude(l_boundingBoxSegment.getMinLongitude().degrees, j * longitudeStep);
				if (l_currentLongitude > 180.0) {
					l_currentLongitude = l_currentLongitude - 360.0;
				}
				LatLon l_currentLatLon = new LatLon(Angle.fromDegreesLatitude(l_currentLatitude), Angle.fromDegreesLongitude(l_currentLongitude));

				// Test if inside
				l_inside = isLocationInside(l_currentLatLon, a_segment, l_shift);

				// Update boolean matrix if inside
				if (l_inside == true && (l_latitudeStartIndexInMatrix + i) < insideFlagElevation.length && (l_longitudeStartIndexInMatrix + j) < insideFlagElevation[0].length) {
					insideFlagElevation[l_latitudeStartIndexInMatrix + i][l_longitudeStartIndexInMatrix + j] = true;
				}
			}
		}

	}

	/**
	 * Test for even/odd number of intersections with a constant latitude line going through the point. From Randolph Franklin see
	 * http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html and http://local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly/ This is only valid for linear path type
	 * 
	 * @param point
	 *            the location
	 * @param positions
	 *            the list of positions describing the polygon. Last one should be the same as the first one.
	 * @return true if the location is inside the polygon.
	 */
	public static boolean isLocationInside(LatLon a_point, ArrayList<? extends LatLon> positions, double a_shift) {

		boolean result = false;

		LatLon p1 = positions.get(0);

		LatLon point = new LatLon(Angle.fromDegreesLatitude(a_point.getLatitude().degrees), Angle.fromDegreesLongitude(a_point.getLongitude().degrees));

		if (a_shift > 0.0) {
			point = new LatLon(Angle.fromDegreesLatitude(point.getLatitude().degrees), Angle.fromDegreesLongitude(((point.getLongitude().degrees - a_shift + 360.0) % 360.0)));
		}

		if (a_shift > 0.0) {
			p1 = new LatLon(Angle.fromDegreesLatitude(p1.getLatitude().degrees), Angle.fromDegreesLongitude(((p1.getLongitude().degrees - a_shift + 360.0) % 360.0)));
		}

		for (int i = 1; i < positions.size(); i++) {
			LatLon p2 = positions.get(i);

			if (a_shift > 0.0) {
				p2 = new LatLon(Angle.fromDegreesLatitude(p2.getLatitude().degrees), Angle.fromDegreesLongitude(((p2.getLongitude().degrees - a_shift + 360.0) % 360.0)));

			}

			// For clarity
			// double lat = point.getLatitude().degrees;
			// double lon = point.getLongitude().degrees;
			// double lat1 = p1.getLatitude().degrees;
			// double lon1 = p1.getLongitude().degrees;
			// double lat2 = p2.getLatitude().degrees;
			// double lon2 = p2.getLongitude().degrees;
			// if ( ((lat2 <= lat && lat < lat1) || (lat1 <= lat && lat < lat2))
			// && (lon < (lon1 - lon2) * (lat - lat2) / (lat1 - lat2) + lon2) )
			// result = !result;

			if (((p2.getLatitude().degrees <= point.getLatitude().degrees && point.getLatitude().degrees < p1.getLatitude().degrees) || (p1.getLatitude().degrees <= point.getLatitude().degrees && point
					.getLatitude().degrees < p2.getLatitude().degrees))
					&& (point.getLongitude().degrees < (p1.getLongitude().degrees - p2.getLongitude().degrees) * (point.getLatitude().degrees - p2.getLatitude().degrees)
							/ (p1.getLatitude().degrees - p2.getLatitude().degrees) + p2.getLongitude().degrees)) {
				result = !result;
			}

			p1 = p2;
		}
		return result;
	}

	/**
	 * Shift the longitude of segment
	 * 
	 * @param a_segment
	 *            segment of .cot file
	 * 
	 * @return no return
	 */
	protected void shiftLongitudeSegment(ArrayList<LatLon> a_segment, double a_longitudeShift) {

		double l_currentLong;

		if (a_longitudeShift == 0.0) {
			return;
		}

		for (int i = 0; i < a_segment.size(); i++) {
			l_currentLong = a_segment.get(i).longitude.degrees;
			l_currentLong = l_currentLong + a_longitudeShift;
			if (l_currentLong < 0.0) {
				l_currentLong = l_currentLong + 360.0;
			}
			a_segment.get(i).longitude.addDegrees(l_currentLong);
		}
	}





	/**
	 * @return the {@link #elevationReader}
	 */
	public IElevationReaderAdaptor getElevationReader() {
		return elevationReader;
	}

	/**
	 * @return the {@link #cotReader}
	 */
	public ICotReaderAdaptor getCotReader() {
		return cotReader;
	}

}
