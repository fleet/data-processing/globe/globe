package fr.ifremer.globe.placa3d.tecGenerator;

/**
 * This interface defines an interface for a writer.
 * 
 * @author D.riou
 */
public interface IElevationMatrix {

	/**
	 * Put the file name of the file to read
	 * 
	 * @return matrix of elevation
	 */
	double[][] getElevationMatrix();

	/**
	 * Put the file name of the file to read
	 * 
	 * @return bounding box latitude min, latitude max, longitude min, longitude
	 *         max
	 */
	double[] getBoundingBox();

}
