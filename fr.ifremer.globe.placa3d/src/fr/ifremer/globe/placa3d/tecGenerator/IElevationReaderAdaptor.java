package fr.ifremer.globe.placa3d.tecGenerator;

/**
 * This interface defines the elevation data used for entry of ElevationExtractor class.
 * 
 * @author D.riou
 */
public interface IElevationReaderAdaptor {
	/**
	 * Put the file name of the file to read
	 * 
	 * @param a_fileName File name
	 * @return no return
	 */
	public void setFileName(String fileName);

	/**
	 * read the Elevation file
	 * 
	 * @param a_boundingBox Bounding box to read. [lat min, lat max, long min, long max]
	 * 
	 * @return no return
	 */
	public void readElevations(double[] boundingBox) throws Exception;

	/**
	 * get an elevation in the matrix
	 * 
	 * @param a_line Line inden
	 * @param a_col Column index
	 * @return elevation value
	 */
	public double getElevation(int a_line, int a_col);

	/**
	 * get number of lines
	 * 
	 * @return number of lines
	 */
	public int getNbOfLines();

	/**
	 * get number of columns
	 * 
	 * @return number of columns
	 */
	public int getNbOfColumns();

	/**
	 * Clear the data to free the memory
	 * 
	 * @return No return
	 */
	public void clearData();

}
