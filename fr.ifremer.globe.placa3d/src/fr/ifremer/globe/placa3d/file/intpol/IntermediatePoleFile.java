package fr.ifremer.globe.placa3d.file.intpol;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.file.IPoleRotationFile;
import fr.ifremer.globe.placa3d.tectonic.Rotation;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;

/**
 * @author mgaro
 * 
 */
public class IntermediatePoleFile extends IPoleRotationFile {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntermediatePoleFile.class);

	/**
	 * Comments of the int file (with # characters)
	 */
	String comments = new String();

	public IntermediatePoleFile(String fileName) {
		super(fileName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ifremer.globe.placa3d.file.IPoleRotationFile#read()
	 */
	@Override
	public TectonicMovement read() throws Exception {
		// reads rotation pole file
		String line;
		Scanner scanner;
		scanner = new Scanner(new FileInputStream(fileName));

		// String builder for comments
		StringBuilder sbComments = new StringBuilder();

		boolean commentsEnd = false;

		try {
			// Add the first total pole rotation
			movement.addTotalRotation(new Rotation(LatLon.ZERO, 0, 0, 0));

			while (scanner.hasNextLine()) {
				line = scanner.nextLine();

				if (line.contains(TectonicUtils.POUND_CHARACTER))// comments
				{
					if (commentsEnd == false) {
						// String builder for comments
						if (sbComments.toString().isEmpty()) {
							sbComments.append(line.substring(1));
						} else {
							sbComments.append(" - " + line.substring(1));
						}
					}

				} else if (line.isEmpty() == true)// empty line
				{
					commentsEnd = true;
					// nothing to do
				} else { // read pole
					commentsEnd = true;
					Rotation intRotation = readLine(line);

					// Add intermediate pole rotation
					movement.addRotation(intRotation);
					// TRACE
					LOGGER.trace("IntermediatepoleFile introt " + intRotation.getBegin() + " " + intRotation.getEnd() + " " + intRotation.getLatLon().toString() + " " + intRotation.getAngle());

					// Construct total pole rotation matrix
					List<Matrix> intMatrixList = new ArrayList<Matrix>();
					for (int i = 0; i < movement.getPoleRotation().getAngles().size(); i++) {
						Matrix m = TectonicUtils.computeMatrix(movement.getLatitudes().get(i), movement.getLongitudes().get(i), -movement.getAngles().get(i));
						intMatrixList.add(m);
					}

					// Construct total pole rotation
					Rotation totRotation = TectonicUtils.convertIntToTot(intMatrixList, 0, intRotation.getEnd());
					// Add total pole rotation
					movement.addTotalRotation(totRotation);
					// TRACE
					LOGGER.trace("IntermediatepoleFile totrot " + totRotation.getBegin() + " " + totRotation.getEnd() + " " + totRotation.getLatLon().toString() + " " + totRotation.getAngle());
				}
			}
		} finally {
			if (!sbComments.toString().isEmpty()) {
				comments += sbComments.toString();
			}
			scanner.close();

			movement.setComments(comments);
		}

		return movement;
	}

	/**
	 * read rotation line.
	 * 
	 * @param tmp
	 * @return a rotation
	 */
	private Rotation readLine(String tmp) {
		Rotation ret = null;

		String[] tmpTab = tmp.split(TectonicUtils.WHITE_CARACTHERS_REGEX);
		String[] dateTab = tmpTab[0].split("-");

		if (tmpTab.length >= 4 && dateTab.length >= 2) {
			try {
				double begin = MagneticAnomalies.getInstance().getDoubleAnomaly(dateTab[0]);
				double end = MagneticAnomalies.getInstance().getDoubleAnomaly(dateTab[1]);

				double latitude = Double.parseDouble(tmpTab[1]);
				double longitude = Double.parseDouble(tmpTab[2]);
				double angle = Double.parseDouble(tmpTab[3]);

				ret = TectonicUtils.getRotation(begin, end, latitude, longitude, angle);
			} catch (NumberFormatException e) {
				LOGGER.error("Impossible to read this line >>> '" + tmp + "'", e);
			}
		} else {
			LOGGER.error("Impossible to read this line >>> " + tmp + "'");
		}
		return ret;
	}
}
