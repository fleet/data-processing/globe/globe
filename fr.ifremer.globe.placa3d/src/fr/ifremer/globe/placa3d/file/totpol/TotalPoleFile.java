package fr.ifremer.globe.placa3d.file.totpol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.file.IPoleRotationFile;
import fr.ifremer.globe.placa3d.gui.MTecParametersDialog;
import fr.ifremer.globe.placa3d.tectonic.Rotation;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.utils.TextFileParser;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.FileFormatException;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;

/**
 * @author mgaro
 * 
 */
public class TotalPoleFile extends IPoleRotationFile {

	private static final Logger LOGGER = LoggerFactory.getLogger(TotalPoleFile.class);

	// Preferences
	protected Preferences preferences = Preferences.userNodeForPackage(MTecParametersDialog.class);
	/** Maximum age defined in preferences. */
	protected int maxAgeInPreference = Activator.getPreferences().getMaximumAge().getValue();
	/** Minimum age defined in preferences. */
	protected int minAgeInPreference = Activator.getPreferences().getMinimumAge().getValue();

	/**
	 * Comments of the tot file (with # characters)
	 */
	String comments = new String();

	public TotalPoleFile(String fileName) {
		super(fileName);
	}

	@Override
	public TectonicMovement read() throws FileFormatException {
		// reads rotation pole file
		List<Double> beginTot = new ArrayList<Double>();
		List<LatLon> latlonTot = new ArrayList<LatLon>();
		List<Double> angleTot = new ArrayList<Double>();

		String line;
		String begin = "";
		double latitude = 0, longitude = 0;
		double angle = 0;

		// String builder for comments
		StringBuilder sbComments = new StringBuilder();

		boolean commentsEnd = false;

		try (Scanner scanner = new Scanner(new FileInputStream(fileName))) {
			try {
				while (scanner.hasNextLine()) {
					line = scanner.nextLine();

					if (line.contains(TectonicUtils.POUND_CHARACTER))// comments
					{
						if (commentsEnd == false) {
							// String builder for comments
							if (sbComments.toString().isEmpty()) {
								sbComments.append(line.substring(1));
							} else {
								// String builder for comments
								sbComments.append(" - " + line.substring(1));
							}
						}

					} else if (line.isEmpty() == true)// empty line
					{
						commentsEnd = true;
					} else { // read pole
						commentsEnd = true;
						String[] tmpTab = line.split(TectonicUtils.WHITE_CARACTHERS_REGEX);

						if (tmpTab.length >= 4) { // begin date, latitude,
													// longitude et angle
							begin = tmpTab[0];
							if (MagneticAnomalies.getInstance().getDoubleAnomaly(begin) == null) {
								throw new FileFormatException(".tot file is inconsistent with the age limits defined in preferences\n(Year '" + begin + "' is out of bounds)");
							}

							try {
								latitude = Double.parseDouble(tmpTab[1]);
								longitude = Double.parseDouble(tmpTab[2]);
								angle = Double.parseDouble(tmpTab[3]);

								beginTot.add(MagneticAnomalies.getInstance().getDoubleAnomaly(begin));
								latlonTot.add(LatLon.fromDegrees(latitude, longitude));
								angleTot.add(angle);

							} catch (NumberFormatException e) {
								throw new FileFormatException(".tot file format is inconsistent (contains bad numbers)");
							}
						} else {
							throw new FileFormatException(".tot file format is inconsistent (not enough column");
						}
					}
				}
			} finally {
				if (!sbComments.toString().isEmpty()) {
					comments += sbComments.toString();
				}
				movement.setComments(comments);
			}
		} catch (FileNotFoundException e) {
			throw new FileFormatException(".tot file not found. Deleted ?");
		}

		// Add a second total pole if only one pole to be able to compute
		// intermediate pole
		if (latlonTot.size() == 1) {
			beginTot.add(MagneticAnomalies.getInstance().getDoubleAnomaly("Fit"));
			latlonTot.add(LatLon.fromDegrees(0, 0));
			angleTot.add(0.001);
		}

		if (latlonTot.size() > 0) {
			// to interpolate rotation pole
			// current time at the begin or the end?
			if (latlonTot.get(0).getLatitude().getDegrees() == 0 && latlonTot.get(0).getLongitude().getDegrees() == 0) {

				// current time at the begin
				addRotations(beginTot, latlonTot, angleTot);

			} else if (latlonTot.get(latlonTot.size() - 1).getLatitude().getDegrees() == 0 && latlonTot.get(latlonTot.size() - 1).getLongitude().getDegrees() == 0) {

				// current time at the end
				Collections.reverse(beginTot);
				Collections.reverse(latlonTot);
				Collections.reverse(angleTot);
				addRotations(beginTot, latlonTot, angleTot);

			} else {
				throw new FileFormatException(".tot file is inconsistent (could not interpolate");
			}
		}
		return movement;
	}

	/**
	 * insert the given couple ano, rotation in the file.
	 * If the ano exist in the given file, it is replaced, or else it is added at the end
	 * @param the anomaly
	 * @param rot the rotation data
	 * @param file the output file, if it does not exists a new file is created
	 * @throws IOException 
	 * */
	public static void insert(String ano, Rotation rot, File file ) throws IOException
	{
		// The anno string to write
		String annoline = ano + " " + rot.getLatLon().getLatitude().getDegrees() + " " + rot.getLatLon().getLongitude().getDegrees() + " "+ -rot.getAngle();

		if (file.exists()) { // edit file

			File tmpFileName = TemporaryCache.createTemporaryFile("temp","tot");
			try (BufferedReader br = new BufferedReader(new FileReader(file));
					BufferedWriter bw = new BufferedWriter(new FileWriter(tmpFileName));	) {
									
				String line = null;							
				double dateBefore = -1;	
				boolean annoWrited = false;
				while ((line = br.readLine()) != null) {
						String parsedLine=TextFileParser.removeComments(line).trim();
						if(!parsedLine.isEmpty())
						{
							double dateCurrent = MagneticAnomalies.getInstance().getDoubleAnomaly(parsedLine.split(TectonicUtils.WHITE_CARACTHERS_REGEX)[0]);
							double dateAnno =  MagneticAnomalies.getInstance().getDoubleAnomaly(ano);
							if (dateAnno == dateCurrent) { // anno trouvée
								line = annoline;
								annoWrited = true;
							} else { // anno non trouvée									
								if (dateBefore == -1 && dateAnno < dateCurrent) { // anno en premier
									line = annoline +"\n"+ line;
									annoWrited = true;
								}

								if (dateBefore != -1) {
									if (dateAnno > dateBefore && dateAnno < dateCurrent) { // anno entre 2
										line = annoline +"\n"+ line;
										annoWrited = true;
									}
								}										
							}

							// Save the date for the next line
							dateBefore = dateCurrent;	
						}

					
					bw.write(line+"\n");
				}

				if (!annoWrited) { // Si l'anno n'a pas été écrite, on l'écrit à la fin
					bw.write(annoline+"\n");
				}
			}
			// Once everything is complete, delete old file.
			if(!file.delete())
			{
				throw new IOException("Cannot delete "+ file.getName() +" check if it is in use in another editor");
			}

			// And rename tmp file's name to old file name
			if(!tmpFileName.renameTo(file))
			{
				throw new IOException("Cannot rename "+ tmpFileName.getPath() + " to "+file.getPath());
			}

		} else { // create file
			try(FileWriter fstream = new FileWriter(file);
					BufferedWriter out = new BufferedWriter(fstream)){
				// Create file 
				out.write("# created by Globe "+LocalDateTime.now().toString()+ "\n"+annoline);
				out.flush();
			}
		}
	}
	
	private void addRotations(List<Double> beginTot, List<LatLon> latlonTot, List<Double> angleTot) {
		double begin;
		double end;
		for (int i = 0; i < latlonTot.size(); i++) {
			begin = beginTot.get(i);

			// Construct total pole rotation
			Rotation totRotation = new Rotation(latlonTot.get(i), angleTot.get(i), beginTot.get(0), begin);
			// Add total pole rotation
			movement.addTotalRotation(totRotation);
			// TRACE
			LOGGER.trace("TotalpoleFile totrot " + totRotation.getBegin() + " " + totRotation.getEnd() + " " + totRotation.getLatLon().toString() + " " + totRotation.getAngle());

			// intermediate poles contains n-1 total poles
			if (i < latlonTot.size() - 1) {

				end = beginTot.get(i + 1);

				// Construct intermediate pole rotation matrix
				Matrix invCurrent = TectonicUtils.computeMatrix(latlonTot.get(i).getLatitude().getDegrees(), latlonTot.get(i).getLongitude().getDegrees(), -angleTot.get(i));
				Matrix next = TectonicUtils.computeMatrix(latlonTot.get(i + 1).getLatitude().getDegrees(), latlonTot.get(i + 1).getLongitude().getDegrees(), angleTot.get(i + 1));

				// Construct intermediate pole rotation
				Rotation intRotation = TectonicUtils.convertTotToInt(invCurrent, next, begin, end);
				// Add int pole rotation
				movement.addRotation(intRotation);
				// TRACE
				LOGGER.trace("TotalpoleFile intRot " + intRotation.getBegin() + " " + intRotation.getEnd() + " " + intRotation.getLatLon().toString() + " " + intRotation.getAngle());
			}
		}
	}

}
