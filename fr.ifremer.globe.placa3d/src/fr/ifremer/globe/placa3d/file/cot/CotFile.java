package fr.ifremer.globe.placa3d.file.cot;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.placa3d.render.cot.CotObject;
import fr.ifremer.globe.placa3d.render.cot.CotPointList;
import fr.ifremer.globe.placa3d.render.cot.CotSegment;

/***
 * File reader : tectonic plates (.cot)
 * 
 * @author MMORVAN
 * 
 */
public class CotFile {

	private static final String POUND_CHARACTER = "#";

	private static final String LESS_CHARACTER = "<";

	private static final String GREATER_CHARACTER = ">";
	
	private static final String DOUBLE_GREATER_CHARACTER = ">>";

	private static final String TRIPLE_GREATER_CHARACTER = ">>>";
		
	/**
	 * Several non-whitespace characters regular expression
	 */
	public final static String WHITE_CARACTHERS_REGEX = "\\s+";

	private static final Logger LOGGER = LoggerFactory.getLogger(CotFile.class);
	
	/**
	 * The list of cot objects (segments or points)
	 */
	private List<CotObject> cotObjectList;
	
	private String fileName = null;
	
	/** The last width */
	private float lastWidth; 
	/** The last color */
	private Color lastColor; 

	/***
	 * reads file
	 * 
	 * @param filePath
	 */
	public CotFile(String filePath) {

		if (filePath != null) {
			cotObjectList = new LinkedList<CotObject>();
			fileName = filePath;
			lastWidth = 3; 
			lastColor = Color.GRAY; 
			read();
		}
	}

	/***
	 * reads file and creates associated polygons in the renderable layer
	 */
	private void read() {

		String line;
		Scanner scanner;
		List<GeoPoint> geoPointList = null;
		List<String> tmpList = new ArrayList<String>();
		String[] tmpTab;
		try {
			scanner = new Scanner(new FileInputStream(fileName));
			try {
				while (scanner.hasNextLine()) {
					line = scanner.nextLine();
					if (line.contains(TRIPLE_GREATER_CHARACTER)) { // point line
						geoPointList = new LinkedList<GeoPoint>();
						CotPointList cotPointList = new CotPointList(geoPointList);
						cotObjectList.add(cotPointList);
						readWidthAndColor(line, cotPointList);	
						
					} else if (line.contains(GREATER_CHARACTER)) {// segment line
						geoPointList = new LinkedList<GeoPoint>();
						CotSegment segment = new CotSegment(geoPointList);
						cotObjectList.add(segment);
						readWidthAndColor(line, segment);
						
						if (line.contains(DOUBLE_GREATER_CHARACTER)) {
							segment.setClosed(false);
						}
					} else if (line.contains(LESS_CHARACTER))// plate name
					{
						// layer name is plate name
					} else if (line.contains(POUND_CHARACTER))// comments
					{
						// nothing
					} else if (line.isEmpty() == true)// empty line
					{
						// nothing
					} else {
						// segment reading
						if (geoPointList != null) {
							tmpTab = line.split(WHITE_CARACTHERS_REGEX);
							tmpList.clear();
							for (String string : tmpTab) {
								if (!string.isEmpty()) {
									tmpList.add(string);
								}
							}
							if (tmpList.size() >= 2) {
								double longitude = Double.parseDouble(tmpList.get(0));
								double latitude = Double.parseDouble(tmpList.get(1));
								geoPointList.add(new GeoPoint(longitude, latitude));
							}

						} else {
							LOGGER.error("Invalid cot file?");
						}

					}
				}
			} finally {
				scanner.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	/**
	 * @return the cot objects list
	 */
	public List<CotObject> getCotObjectList() {
		return cotObjectList;
	}
	
	private void readWidthAndColor(String line, CotObject cotObject) {
		String[] splitLine = line.split(WHITE_CARACTHERS_REGEX);
		boolean widthAndColorSetted = false;
		if (splitLine.length >= 4 ) { //size and color information
			try
			{
				// Width
				float width = Float.valueOf(splitLine[1]);
				cotObject.setWidth(width);

				// Color
				int colorR = Integer.valueOf(splitLine[2]);
				int colorG = Integer.valueOf(splitLine[3]);
				int colorB = Integer.valueOf(splitLine[4]);
				Color color = new Color(colorR, colorG, colorB);
				cotObject.setColor(color);

				// Save color and width
				lastWidth = width;
				lastColor = color;
				widthAndColorSetted = true;
			}
			catch (NumberFormatException nfe)
			{
				LOGGER.trace("NumberFormatException: " + nfe.getMessage());
			}
		}
		if (!widthAndColorSetted) { // if color and width not founded, set last founded 
			cotObject.setWidth(lastWidth);
			cotObject.setColor(lastColor);		
		}
	}
}
