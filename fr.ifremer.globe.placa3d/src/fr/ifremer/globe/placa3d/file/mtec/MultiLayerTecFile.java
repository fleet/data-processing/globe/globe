package fr.ifremer.globe.placa3d.file.mtec;

/**
 *
 */
public class MultiLayerTecFile {

	// File structure :
	// ---------------------------------------------------------------------------------------
	// <HDF5ROOT>
	// .|
	// .GROUP:metadata (attributes = nbPlates:int)
	// .|
	// .GROUP:plates
	// .|___ GROUP:plate<i>
	// .|...|__ GROUP:poles (attributes: )
	// .|...|
	// .|...|__ GROUP:layers (attributes: nbLayers]
	// .|......|
	// .|......|__ GROUP:era<i> (attributes = startAge:double; endAge:double)
	// .|.........|
	// .|.........|__ DATASET:elevation (attributes = min:double ; max:double)
	// .|.........|__ DATASET:texture (attributes = min:double ; max:double)
	// .|.........|__ [DATASET:mask] (attributes = type:int)
	// ---------------------------------------------------------------------------------------

	/** The Multilayer TEC file extension, without "." */
	public static final String FILE_EXTENSION = "mtec";

	public static final String FILE_DESCRIPTION = "TEC File multilayer";

	// Groups

	public static final String GROUPNODE_METADATA = "metadata";
	public static final String GROUPNODE_PLATES = "plates";
	public static final String GROUPNODE_PLATE_PREFIX = "plate";

	public static final String GROUPNODE_PLATE_POLES = "poles";
	public static final String GROUPNODE_PLATE_LAYERS = "layers";

	public static final String GROUPNODE_PLATE_ERA = "era";
	public static final String GROUPNODE_PLATE_ELEVATION = "elevation";
	public static final String GROUPNODE_PLATE_TEXTURE = "texture";
	public static final String GROUPNODE_PLATE_MASK = "mask";

	// Datasets

	public static final String DS_PLATE_ELEV = "elevation";
	public static final String DS_PLATE_TEX = "texture";
	public static final String DS_PLATE_MASK = "mask";

	// Attributes

	public static final String ATT_PLATES_NB_PLATES = "nbPlates";

	public static final String ATT_PLATE_NAME = "name";
	public static final String ATT_PLATE_LONG_RANGE = "long_range";
	public static final String ATT_PLATE_LAT_RANGE = "lat_range";
	public static final String ATT_PLATE_Z_RANGE = "z_range";

	public static final String ATT_LAYERS_NBLAYERS = "nbLayers";

	public static final String ATT_POLES_LATPOLEROTATIONLIST = "latPoleRotationList";
	public static final String ATT_POLES_LONGPOLEROTATIONLIST = "longPoleRotationList";
	public static final String ATT_POLES_ANGLEPOLEROTATIONLIST = "anglePoleRotationList";
	public static final String ATT_POLES_DATEPOLEROTATIONLIST = "datePoleRotationList";
	public static final String ATT_POLES_ENDDATEPOLEROTATIONLIST = "endDatePoleRotationList";

	public static final String ATT_MASK_DIMENSION = "dimensionMask";

	public static final String ATT_LAYER_NAME = "layerName";
	public static final String ATT_ERA_START = "startAge";
	public static final String ATT_ERA_END = "endAge";

	public static final String ATT_GRID_WIDTH = "gridWidth";
	public static final String ATT_GRID_HEIGHT = "gridHeight";
	public static final String ATT_GRID_MIN_VALUE = "min";
	public static final String ATT_GRID_MAX_VALUE = "max";

}
