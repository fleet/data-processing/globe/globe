package fr.ifremer.globe.placa3d.file.elevation;

/** enum for get method */
public enum Area {
	AREA_ALL, /* All area */
	AREA_EAST, /* East area */
	AREA_WEST /* West area */
}