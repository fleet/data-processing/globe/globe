package fr.ifremer.globe.placa3d.file.elevation;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.reader.MultiLayerTecFileReader;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import gov.nasa.worldwind.geom.Sector;

/**
 * This class is intended to feed a tectonic model by reading a tectonic plate's data in a .mtec file on-the-fly.
 * <p>
 * This class uses MultiLayerTecFileReader to decode the ".mtec" file format.
 * <p>
 * Metadata are loaded first and then only a particuliar plate's age interval needs to be loaded (see the
 * {@link #read()} method).
 *
 * @author apside
 */
public class MultiLayerTecImageElevationData {

	/** Data source. */
	private File sourceFile;

	/** The global tectonic model. */
	private TectonicModel tectonicModel;

	/** The plate's model. */
	private TectonicPlateModel plate;

	/** Last date read from the underlying file. */
	private double lastReadAgeIndex = -1;

	private double[][] westElevation;
	private double[][] eastElevation;

	private double[][] westTexture;
	private double[][] eastTexture;

	/**
	 * Constructor.
	 *
	 * @param sourceFile Source file where data can be loaded from.
	 * @param tectonicModel Tectonic model the plate is linked to.
	 * @param plate The plate model to feed with <code>$sourceFile</code> data.
	 */
	public MultiLayerTecImageElevationData(File sourceFile, TectonicModel tectonicModel, TectonicPlateModel plate) {
		super();
		this.sourceFile = sourceFile;
		this.plate = plate;
		this.tectonicModel = tectonicModel;
	}

	/**
	 * Read the plate's data according to the current tectonic age index.
	 */

	public void read() throws IOException {

		double ageIndexToRead = tectonicModel.getAge();
		if (ageIndexToRead != lastReadAgeIndex) {

			try (MultiLayerTecFileReader reader = new MultiLayerTecFileReader(sourceFile)) {
				reader.open();
				reader.loadPlateGrids(plate, ageIndexToRead, true);
				lastReadAgeIndex = ageIndexToRead;

			} catch (IOException e) {
				throw e;
			} catch (Exception e) {
				throw new IOException(e);
			}
		}
	}

	/**
	 * @return The plate's model.
	 */
	public TectonicPlateModel getPlate() {
		return plate;
	}

	/**
	 * @return The data source as a <code>java.io.File</code> object.
	 */

	public File getFile() {
		return sourceFile;
	}

	/**
	 * The data source file's absolute path.
	 *
	 * @return The data source file's absolute path.
	 */

	public String getPathFile() {
		return sourceFile.getAbsolutePath();
	}

	public long getSizeInBytes() {
		return sourceFile.length();
	}

	public double getMinDepth() {
		return plate.getMetadata().getZRange().getMinimumDouble();
	}

	public double getMaxDepth() {
		return plate.getMetadata().getZRange().getMaximumDouble();
	}

	public double getLongitudeMax(Area a_area) {
		double longitude = plate.getMetadata().getGeoBox().getMaxLongitude().degrees;
		if (a_area == Area.AREA_WEST) {
			longitude = 180.0;
		}
		return longitude;
	}

	public double getLongitudeMin(Area a_area) {
		double longitude = plate.getMetadata().getGeoBox().getMinLongitude().degrees;
		if (a_area == Area.AREA_EAST) {
			longitude = -180.0;
		}
		return longitude;
	}

	public double getLatitudeMax() {
		return plate.getMetadata().getGeoBox().getMaxLatitude().degrees;
	}

	public double getLatitudeMin() {
		return plate.getMetadata().getGeoBox().getMinLatitude().degrees;
	}

	public String getDescription() {
		return plate.getMetadata().getPlateName();
	}

	public double[][] getDepthRaster(Area a_area) {

		double[][] result = null;

		if (plate.getMetadata().hasElevation() && plate.getElevationGrid() != null) {
			double[][] data = plate.getElevationGrid().getData();

			if (data.length > 1) {
				if (a_area == Area.AREA_ALL) {
					return data;
				} else {
					if (eastElevation == null) {
						eastElevation = TectonicUtils.extractArea(Area.AREA_EAST, data,
								plate.getMetadata().getGeoBox());
					}
					if (westElevation == null) {
						westElevation = TectonicUtils.extractArea(Area.AREA_WEST, data,
								plate.getMetadata().getGeoBox());
					}
					if (a_area == Area.AREA_EAST) {
						result = eastElevation;
					} else {
						result = westElevation;
					}
				}
			}
		}
		return result;
	}

	public Dimension getDepthRasterDimension(Area a_area) {
		if (plate.getMetadata().hasElevation()) {

			Dimension dim = plate.getMetadata().getElevationGridDimension();
			Sector geoBox = plate.getMetadata().getGeoBox();
			double minLon = geoBox.getMinLongitude().degrees;
			double maxLon = geoBox.getMaxLongitude().degrees;

			if (a_area != Area.AREA_ALL) {
				int l_180index = (int) (dim.width
						* ((180.0 - minLon) / TecGeneratorUtil.diffLongitude(maxLon, minLon)));
				if (a_area == Area.AREA_WEST) {
					dim = new Dimension(l_180index, dim.height);
				} else {
					dim = new Dimension(dim.width - l_180index, dim.height);
				}
			}
			return dim;
		}
		return null;
	}

	public double[][] getTexture(Area a_area) {
		double[][] result = null;

		if (plate.getMetadata().hasTexture() && plate.getTextureGrid() != null) {
			double[][] data = plate.getTextureGrid().getData();

			if (data.length > 1) {
				if (a_area == Area.AREA_ALL) {
					return data;
				} else {
					if (eastTexture == null) {
						eastTexture = TectonicUtils.extractArea(Area.AREA_EAST, data, plate.getMetadata().getGeoBox());
					}
					if (westTexture == null) {
						westTexture = TectonicUtils.extractArea(Area.AREA_WEST, data, plate.getMetadata().getGeoBox());
					}
					if (a_area == Area.AREA_EAST) {
						result = eastTexture;
					} else {
						result = westTexture;
					}
				}
			}
		}
		return result;
	}

	public Dimension getTextureDimension(Area a_area) {
		if (plate.getMetadata().hasElevation()) {

			Dimension dim = plate.getMetadata().getTextureGridDimension();
			Sector geoBox = plate.getMetadata().getGeoBox();
			double minLon = geoBox.getMinLongitude().degrees;
			double maxLon = geoBox.getMaxLongitude().degrees;

			if (a_area != Area.AREA_ALL) {
				int l_180index = (int) (dim.width
						* ((180.0 - minLon) / TecGeneratorUtil.diffLongitude(maxLon, minLon)));
				if (a_area == Area.AREA_WEST) {
					dim = new Dimension(l_180index, dim.height);
				} else {
					dim = new Dimension(dim.width - l_180index, dim.height);
				}
			}
			return dim;
		}
		return null;
	}

	public String[] getOtherRasterName() {
		return null;
	}

	public double[][] getOtherRaster(String a_rasterName, Area a_area) {
		return null;
	}

	public Dimension getOtherRasterDimension(Area a_area) {
		return null;
	}

	public boolean isOnDateLineChange() {
		Sector geoBox = plate.getMetadata().getGeoBox();
		return TecGeneratorUtil.isOnLineDateChange(geoBox.getMinLongitude().degrees, geoBox.getMaxLongitude().degrees);
	}

	public String getName(Area area) {
		StringBuffer sb = new StringBuffer(plate.getMetadata().getPlateName());
		if (area == Area.AREA_ALL) {
			return sb.toString();
		} else if (area == Area.AREA_EAST) {
			return sb.append("_E").toString();
		} else {
			return sb.append("_W").toString();
		}
	}

	/**
	 * @return the {@link #tectonicModel}
	 */
	public TectonicModel getTectonicModel() {
		return tectonicModel;
	}

}