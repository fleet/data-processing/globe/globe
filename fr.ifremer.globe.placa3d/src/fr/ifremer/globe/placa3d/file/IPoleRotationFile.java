package fr.ifremer.globe.placa3d.file;

import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;

public abstract class IPoleRotationFile {

	protected TectonicMovement movement;

	protected String fileName = "";

	/***
	 * reads file
	 * 
	 * @param filePath
	 */
	public IPoleRotationFile(String fileName) {
		super();

		if (fileName != "") {
			movement = new TectonicMovement(fileName);
			this.fileName = fileName;
		}

	}

	public abstract TectonicMovement read() throws Exception;
	
}
