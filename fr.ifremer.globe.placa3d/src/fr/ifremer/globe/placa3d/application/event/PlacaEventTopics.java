/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.application.event;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * All topics managed in the Placa throw the IEventBroker
 */
public class PlacaEventTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/globe/placa3d";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to inform that the dirty flag has changed
	 */
	public static final String TOPIC_MAGNETIC_ANOMALIES_UPDATED = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_MAGNETIC_ANOMALIES_UPDATED";

	/**
	 * Topic to inform that a LayerStore is added to a TectonicDefinition<br>
	 * Event poster place on the event an instance of LayerStoreAddedEvent
	 */
	public static final String TOPIC_LAYER_STORE_ADDED = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_LAYER_STORE_ADDED";

	/**
	 * Constructor
	 */
	private PlacaEventTopics() {
	}
}
