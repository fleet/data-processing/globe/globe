package fr.ifremer.globe.placa3d.application.event;

import fr.ifremer.globe.placa3d.store.LayerStore;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;

/**
 * Event triggered when a LayerStore is added to a TectonicDefinition.
 */
public record LayerStoreAddedEvent(TectonicDefinition tectonicDefinition, LayerStore layerStore) {
}