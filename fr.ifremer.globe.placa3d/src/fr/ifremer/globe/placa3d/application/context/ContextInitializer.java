/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.application.context;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.osgi.service.event.Event;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.placa3d.session.Placa3dSession;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;

/**
 * Application Addon used to initialize the IEclipseContext
 */
public class ContextInitializer {

	/** Static reference of Eclipse context initialized by E4Application */
	protected static IEclipseContext eclipseContext;

	/**
	 * Invoked when Globe application is started.<br>
	 * Fill the context with needed components
	 *
	 * @param injectedEclipseContext Eclipse context initialized by E4Application
	 */
	@Inject
	@Optional
	public void activate(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext injectedEclipseContext) {
		activate(injectedEclipseContext);
	}

	/**
	 * Fill the context with needed components and initialize the context
	 */
	public static void activate(IEclipseContext eclipseContext) {
		setEclipseContext(eclipseContext);
		init();
	}

	/**
	 * Init the singleton instance, create by DI, it registers reference instances of objects used througout the
	 * NavShift application with current context
	 */
	public static void init() {
		initPlaca3dSession();
		inject(MagneticAnomalies.getInstance());
		inject(TectonicDataset.getInstance());
	}

	/**
	 * Getter of {@link #eclipseContext}
	 */
	public static IEclipseContext getEclipseContext() {
		return eclipseContext;
	}

	/**
	 * Setter of {@link #eclipseContext}
	 */
	public static void setEclipseContext(IEclipseContext newEclipseContext) {
		eclipseContext = newEclipseContext;
	}

	/**
	 * Returns the context value associated with the given name
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInContext(String name) {
		return (T) getEclipseContext().get(name);
	}

	public static <T> T getInContext(Class<T> clazz) {
		return getEclipseContext().get(clazz);
	}

	/**
	 * Sets a value to be associated with a given name in this context
	 */
	public static void setInContext(String name, Object value) {
		getEclipseContext().set(name, value);
	}

	/**
	 * Removes the given name and any corresponding value from this context
	 */
	public static void removeFromContext(String name) {
		getEclipseContext().remove(name);
	}

	/**
	 * @return an instance of the specified class and inject it with the context.
	 */
	public static <T> T make(Class<T> clazz) {
		return ContextInjectionFactory.make(clazz, eclipseContext);
	}

	/**
	 * Injects a context into a domain object.
	 */
	public static void inject(Object object) {
		ContextInjectionFactory.inject(object, getEclipseContext());
	}

	private static void initPlaca3dSession() {
		Placa3dSession result = null;
		try {
			File sessionFile = Placa3dSession.getSessionFile();
			if (sessionFile.exists()) {
				Gson gson = new GsonBuilder().create();
				result = gson.fromJson(Files.readString(sessionFile.toPath(), StandardCharsets.UTF_8),
						Placa3dSession.class);
			}
			TectonicDataset.getInstance().postSessionRead(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (result == null) {
			result = ContextInitializer.make(Placa3dSession.class);
		}
		ContextInitializer.getInContext(ISessionService.class).addPreSaveSessionListener(result::write);
		ContextInitializer.getEclipseContext().set(Placa3dSession.class, result);
	}
}
