/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.service.worldwind;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.placa3d.drivers.TotalPoleLoader;
import fr.ifremer.globe.placa3d.info.TotInfoLoader;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;

/**
 *
 */
@Component(name = "globe_placa_tot_layer_provider", service = IWWLayerProvider.class)
public class TotLayerProvider extends BasicWWLayerProvider<BasicFileInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(TotLayerProvider.class);

	/** Loader of tot file info */
	@Reference
	protected TotInfoLoader totInfoLoader;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return totInfoLoader.getContentTypes();
	}

	/**
	 * Create the layers to represent the tot file in the 3D Viewer
	 */
	@Override
	protected void createLayers(BasicFileInfo totInfo, WWFileLayerStore layerStore, IProgressMonitor monitor) {
		TotalPoleLoader loader = new TotalPoleLoader();
		try {
			layerStore.addLayers(loader.load(totInfo.toFile(), monitor));
		} catch (Exception e) {
			// Bad file format
		}
	}

}
