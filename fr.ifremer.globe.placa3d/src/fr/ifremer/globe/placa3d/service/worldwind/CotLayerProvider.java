/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.service.worldwind;

import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.placa3d.file.cot.CotFile;
import fr.ifremer.globe.placa3d.info.CotInfoSupplier;
import fr.ifremer.globe.placa3d.layers.CotLayer;
import fr.ifremer.globe.placa3d.render.cot.CotObject;
import fr.ifremer.globe.placa3d.util.PolygonBuilder;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Sector;

/**
 * Service providing Nasa layer for Mtec files
 */
@Component(name = "globe_placa_cot_layer_provider", service = IWWLayerProvider.class)
public class CotLayerProvider extends BasicWWLayerProvider<BasicFileInfo> {

	/** Loader of mtec file info */
	@Reference
	protected CotInfoSupplier cotInfoSupplier;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return cotInfoSupplier.getContentTypes();
	}

	@Override
	protected void createLayers(BasicFileInfo cotInfo, WWFileLayerStore layerStore, IProgressMonitor monitor) {
		String name = cotInfo.getFilename();

		// reads the .cot file
		CotFile cotFile = new CotFile(cotInfo.getAbsolutePath());

		// add the associated polygons to the renderable layer
		List<CotObject> cotObjectList = cotFile.getCotObjectList();
		CotLayer cotLayer = new CotLayer(name, calculateSector(cotObjectList));
		PolygonBuilder.addCotObjects(cotObjectList, cotLayer);

		// add the renderable layer to NWW layer list
		layerStore.addLayers(cotLayer);
	}

	private Sector calculateSector(List<CotObject> cotObjectList) {
		double maxLat = Double.NaN, maxLon = Double.NaN, minLon = Double.NaN, minLat = Double.NaN;
		double latTmp, lonTmp;
		for (CotObject cotObject : cotObjectList) {
			for (GeoPoint geoPoint : cotObject.getPointList()) {

				latTmp = geoPoint.getLat();
				if (Double.compare(maxLat, Double.NaN) == 0 || latTmp > maxLat) {
					maxLat = latTmp;
				}
				if (Double.compare(minLat, Double.NaN) == 0 || latTmp < minLat) {
					minLat = latTmp;
				}

				lonTmp = geoPoint.getLong();
				if (Double.compare(maxLon, Double.NaN) == 0 || lonTmp > maxLon) {
					maxLon = lonTmp;
				}
				if (Double.compare(minLon, Double.NaN) == 0 || lonTmp < minLon) {
					minLon = lonTmp;
				}
			}
		}

		// creates layer containing the polygon
		return new Sector(Angle.fromDegrees(minLat), Angle.fromDegrees(maxLat), Angle.fromDegrees(minLon),
				Angle.fromDegrees(maxLon));
	}

}
