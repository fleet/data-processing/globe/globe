/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.service.worldwind;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.drivers.MultiLayerTecImageElevationLoader;
import fr.ifremer.globe.placa3d.info.MtecInfo;
import fr.ifremer.globe.placa3d.info.MtecInfoLoader;
import fr.ifremer.globe.placa3d.layers.PoleRotationLayer;
import fr.ifremer.globe.placa3d.store.TectonicPlateLayerStore;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Service providing Nasa layer for Mtec files
 */
@Component(name = "globe_placa_mtec_layer_provider", service = IWWLayerProvider.class)
public class MtecLayerProvider extends BasicWWLayerProvider<MtecInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(MtecLayerProvider.class);

	/** Loader of mtec file info */
	@Reference
	protected MtecInfoLoader mtecInfoLoader;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return mtecInfoLoader.getContentTypes();
	}

	/**
	 * Create the layers to represent the mtec file in the 3D Viewer
	 */
	@Override
	protected void createLayers(MtecInfo mtecInfo, WWFileLayerStore layerStore, IProgressMonitor monitor) {
		List<Pair<IWWLayer, ElevationModel>> layerPairs = new ArrayList<>();
		TectonicModel tectonicModel = mtecInfo.getTectonicModel();
		File file = mtecInfo.toFile();
		int nbPlates = tectonicModel.getPlates().size();
		SubMonitor subMonitor = SubMonitor.convert(monitor, nbPlates);
		if (nbPlates == 1) {
			// Only one plate
			TectonicPlateModel plate = tectonicModel.getPlate(0);
			TectonicPlateLayerStore plateLayerStore = new TectonicPlateLayerStore(file, plate);
			createLayers(plateLayerStore, tectonicModel, plate, subMonitor);
			layerPairs.addAll(TectonicUtils.convertToWWLayers(plateLayerStore.getLayers()));
			mtecInfo.addPlateLayerStore(plateLayerStore);
		} else if (nbPlates > 1) {
			// Multiple plates
			List<TectonicPlateModel> plates = tectonicModel.getPlates();
			for (TectonicPlateModel plate : plates) {
				TectonicPlateLayerStore plateLayerStore = new TectonicPlateLayerStore(file, plate);
				createLayers(plateLayerStore, tectonicModel, plate, subMonitor.split(1));
				layerPairs.addAll(TectonicUtils.convertToWWLayers(plateLayerStore.getLayers()));
				mtecInfo.addPlateLayerStore(plateLayerStore);
			}
		}

		List<IWWLayer> layers = layerPairs.stream().map(Pair::getFirst).collect(Collectors.toList());
		layerStore.addLayers(layers);

		ElevationModel elevationModel = layerPairs.stream().map(Pair::getSecond).filter(Objects::nonNull).findFirst()
				.orElse(null);
		layerStore.setElevationModel(elevationModel);
	}

	protected void createLayers(TectonicPlateLayerStore store, TectonicModel tectonicModel, TectonicPlateModel plate,
			IProgressMonitor progress) {

		// PoleRotation Layer
		TectonicDefinition tectonicDefinition = createPoleRotationLayer(store, tectonicModel, plate);

		// Image and elevation layers, depending on the plate's location
		try {
			MultiLayerTecImageElevationLoader loader = new MultiLayerTecImageElevationLoader(tectonicModel, plate);
			List<Pair<Layer, ElevationModel>> loadedLayers = loader.load(store.getFile(), progress);
			store.addLayers(loadedLayers);
			tectonicDefinition.addLayerStore(store);
			tectonicDefinition.hookListeners();
		} catch (Exception e) {
			logger.error("Unable to load elevation and texture layers : " + e.getMessage(), e);
		}
	}

	protected TectonicDefinition createPoleRotationLayer(TectonicPlateLayerStore store, TectonicModel tectonicModel,
			TectonicPlateModel plate) {
		TectonicDefinition result = null;
		TectonicMovement movement = plate.getMetadata().getMovement();
		if (movement != null) {
			PoleRotationLayer poleRotationLayer = new PoleRotationLayer(plate.getMetadata().getPlateName(), movement);
			result = new TectonicDefinition(poleRotationLayer, tectonicModel, plate);
			TectonicDataset.getInstance().addTectonicDefinition(result);
			store.addLayer(poleRotationLayer, null);
		}
		return result;
	}

}