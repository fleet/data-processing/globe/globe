package fr.ifremer.globe.placa3d.service.parametersview;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.placa3d.composite.CotComposite;
import fr.ifremer.globe.placa3d.layers.CotLayer;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;

@Component(name = "globe_placa_cot_composite_factory", service = IParametersViewCompositeFactory.class)
public class CotCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		return selection instanceof CotLayer ? Optional.of(new CotComposite(parent, SWT.NONE, (CotLayer) selection))
				: Optional.empty();
	}

}
