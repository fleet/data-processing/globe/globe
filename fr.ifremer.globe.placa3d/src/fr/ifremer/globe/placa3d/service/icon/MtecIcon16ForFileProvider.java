/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.service.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a mtec file.
 */
@Component(name = "globe_placa_mtec_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class MtecIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public MtecIcon16ForFileProvider() {
		super("icons/16/tec.png", ContentType.MTEC);
	}
}