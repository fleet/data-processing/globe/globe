/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.service.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 *
 */
@Component(name = "globe_placa_pole_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class PoleIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public PoleIcon16ForFileProvider() {
		super("icons/16/poles.png", ContentType.POLE_INF, ContentType.POLE_TOT);
	}

}
