package fr.ifremer.globe.placa3d.writer;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.ELoadingType;
import fr.ifremer.globe.placa3d.data.TectonicGrid;
import fr.ifremer.globe.placa3d.data.TectonicLayer;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateMetadata;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.mtec.MultiLayerTecFile;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.h5.H5GroupWithMembers;
import fr.ifremer.globe.utils.h5.H5Utils;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.h5.H5File;

/**
 * Writer for Multilayer TEC files.
 *
 * @author apside
 */
public class MultiLayerTecFileWriter implements AutoCloseable {

	private static final Logger logger = LoggerFactory.getLogger(MultiLayerTecFileWriter.class);

	/* Note : We must separate basic File object and H5File objects. */

	/** Regular source file location. */
	protected File destFile;

	/** H5File that will be opened for writing operations. */
	protected H5File h5File;

	/** Temporary file. */
	protected File tempFile;

	/** Flag to know if an error occured. */
	protected boolean isSuccess;

	/**
	 * Constructor.
	 *
	 * @param destinationFile File to be written out.
	 */
	public MultiLayerTecFileWriter(File destinationFile) {
		super();
		isSuccess = true;
		destFile = destinationFile;
	}

	/**
	 * Opens the destination file in write mode.
	 * <p>
	 * <strong>Note :</strong> If the file already exists it will be deleted first.
	 */
	public void open() throws Exception {
		try {
			tempFile = TemporaryCache.createTemporaryFile(destFile.getName(), "");
			h5File = new H5File(tempFile.getAbsolutePath(), FileFormat.CREATE);
			h5File.open();
		} catch (Exception e) {
			isSuccess = false;
			throw e;
		}
	}

	/**
	 * Closes the underlying stream.
	 */
	@Override
	public void close() throws Exception {
		h5File.close();

		if (isSuccess && destFile.exists()) {
			FileUtils.deleteQuietly(destFile);
		}
		if (isSuccess) {
			FileUtils.moveFile(tempFile, destFile);
		} else {
			logger.error("Unable to write file " + destFile.getAbsolutePath()
					+ " (see previous log entries for more details)");
			FileUtils.deleteQuietly(tempFile);
		}
	}

	/**
	 * Writes the model on disk.
	 *
	 * @param model The model to be written out.
	 */
	public void writeMetadata(TectonicModel model) throws Exception {
		try {
			List<? extends TectonicPlateModel> plates = model.getPlates();

			H5GroupWithMembers platesGroup = H5Utils.createGroupWithMembers(h5File, MultiLayerTecFile.GROUPNODE_PLATES);
			H5Utils.addIntegerAttributeToGroup(h5File, platesGroup.getH5Group(), MultiLayerTecFile.ATT_PLATES_NB_PLATES,
					plates.size());

			for (int iPlate = 0; iPlate < plates.size(); iPlate++) {

				TectonicPlateModel plate = plates.get(iPlate);
				TectonicPlateMetadata meta = plate.getMetadata();

				H5GroupWithMembers plateGroup = H5Utils.createGroupWithMembers(h5File,
						MultiLayerTecFile.GROUPNODE_PLATES + "/" + MultiLayerTecFile.GROUPNODE_PLATE_PREFIX + iPlate);

				// Common attributes
				double[] geobox = meta.getGeoBox().asDegreesArray();
				if (geobox != null) {
					H5Utils.addStringAttributeToGroup(h5File, plateGroup.getH5Group(), MultiLayerTecFile.ATT_PLATE_NAME,
							meta.getPlateName());
					H5Utils.addDoubleAttributeToGroup(h5File, plateGroup.getH5Group(),
							MultiLayerTecFile.ATT_PLATE_LONG_RANGE, new double[] { geobox[2], geobox[3] });
					H5Utils.addDoubleAttributeToGroup(h5File, plateGroup.getH5Group(),
							MultiLayerTecFile.ATT_PLATE_LAT_RANGE, new double[] { geobox[0], geobox[1] });
				}
				DoubleRange zRange = meta.getZRange();
				if (zRange != null) {
					H5Utils.addDoubleAttributeToGroup(h5File, plateGroup.getH5Group(),
							MultiLayerTecFile.ATT_PLATE_Z_RANGE,
							new double[] { zRange.getMinimumDouble(), zRange.getMaximumDouble() });
				}

				// Rotation poles
				TectonicMovement movement = meta.getMovement();
				if (movement != null) {

					double[] latPoleRotationList = ArrayUtils
							.toPrimitive(movement.getLatitudes().toArray(new Double[0]));
					double[] longPoleRotationList = ArrayUtils
							.toPrimitive(movement.getLongitudes().toArray(new Double[0]));
					double[] anglePoleRotationList = ArrayUtils
							.toPrimitive(movement.getAngles().toArray(new Double[0]));
					String[] datePoleRotationList = new String[0];
					String[] endDatePoleRotationList = new String[0];

					H5GroupWithMembers polesGroup = H5Utils.createGroupWithMembers(h5File,
							plateGroup.getFullName() + "/" + MultiLayerTecFile.GROUPNODE_PLATE_POLES);

					datePoleRotationList = movement.getSDates().toArray(datePoleRotationList);
					endDatePoleRotationList = movement.getSEndDates().toArray(endDatePoleRotationList);

					H5Utils.addDoubleAttributeToGroup(h5File, polesGroup.getH5Group(),
							MultiLayerTecFile.ATT_POLES_LATPOLEROTATIONLIST, latPoleRotationList);
					H5Utils.addDoubleAttributeToGroup(h5File, polesGroup.getH5Group(),
							MultiLayerTecFile.ATT_POLES_LONGPOLEROTATIONLIST, longPoleRotationList);
					H5Utils.addDoubleAttributeToGroup(h5File, polesGroup.getH5Group(),
							MultiLayerTecFile.ATT_POLES_ANGLEPOLEROTATIONLIST, anglePoleRotationList);
					H5Utils.writeStringDataSet(h5File, polesGroup.getH5Group(),
							MultiLayerTecFile.ATT_POLES_DATEPOLEROTATIONLIST, datePoleRotationList);
					H5Utils.writeStringDataSet(h5File, polesGroup.getH5Group(),
							MultiLayerTecFile.ATT_POLES_ENDDATEPOLEROTATIONLIST, endDatePoleRotationList);
				}

				List<TectonicLayer> tectonicLayers = meta.getTectonicLayers();
				if (tectonicLayers != null) {

					H5GroupWithMembers layersGroup = H5Utils.createGroupWithMembers(h5File,
							plateGroup.getFullName() + "/" + MultiLayerTecFile.GROUPNODE_PLATE_LAYERS);
					H5Utils.addIntegerAttributeToGroup(h5File, layersGroup.getH5Group(),
							MultiLayerTecFile.ATT_LAYERS_NBLAYERS, tectonicLayers.size());

					if (!tectonicLayers.isEmpty()) {
						// Layers
						int i = 0;
						for (TectonicLayer tectonicLayer : tectonicLayers) {
							DoubleRange range = tectonicLayer.getAge();
							H5GroupWithMembers eraGroup = H5Utils.createGroupWithMembers(h5File,
									layersGroup.getFullName() + "/" + MultiLayerTecFile.GROUPNODE_PLATE_ERA + i);

							double ageMin = range.getMinimumDouble();
							double ageMax = range.getMaximumDouble();

							// String sAnomalyMin =
							// MagneticAnomalies.getInstance().getStringAnomaly(ageMin);
							// String sAnomalyMax =
							// MagneticAnomalies.getInstance().getStringAnomaly(ageMax);

							H5Utils.addStringAttributeToGroup(h5File, eraGroup.getH5Group(),
									MultiLayerTecFile.ATT_LAYER_NAME, tectonicLayer.getName());
							H5Utils.addDoubleAttributeToGroup(h5File, eraGroup.getH5Group(),
									MultiLayerTecFile.ATT_ERA_START, ageMin);
							H5Utils.addDoubleAttributeToGroup(h5File, eraGroup.getH5Group(),
									MultiLayerTecFile.ATT_ERA_END, ageMax);

							i++;
						}
					}
				}
			}
		} catch (Exception e) {
			isSuccess = false;
			throw e;
		}
	}

	public void writePlateGrids(TectonicPlateModel plate, DoubleRange range, int ageLayerIndex) throws Exception {
		try {
			H5GroupWithMembers plateGroup = H5Utils.createGroupWithMembers(h5File, MultiLayerTecFile.GROUPNODE_PLATES
					+ "/" + MultiLayerTecFile.GROUPNODE_PLATE_PREFIX + plate.getMetadata().getPlateIndex());
			H5GroupWithMembers layersGroup = H5Utils.createGroupWithMembers(h5File,
					plateGroup.getFullName() + "/" + MultiLayerTecFile.GROUPNODE_PLATE_LAYERS);

			List<TectonicLayer> tectonicLayers = plate.getMetadata().getTectonicLayers();
			for (TectonicLayer era : tectonicLayers) {
				if (era.getAge().equals(range)) {
					H5GroupWithMembers eraGroup = H5Utils.createGroupWithMembers(h5File,
							layersGroup.getFullName() + "/" + MultiLayerTecFile.GROUPNODE_PLATE_ERA + ageLayerIndex);
					for (ELoadingType gridType : ELoadingType.values()) {
						TectonicGrid grid = plate.getGrid(gridType);
						if (grid != null) {
							H5GroupWithMembers gridGroup = H5Utils.createGroupWithMembers(h5File,
									eraGroup.getFullName() + "/" + gridType.name());

							H5Utils.addIntegerAttributeToGroup(h5File, gridGroup.getH5Group(),
									MultiLayerTecFile.ATT_GRID_HEIGHT, grid.getLatitudeCount());
							H5Utils.addIntegerAttributeToGroup(h5File, gridGroup.getH5Group(),
									MultiLayerTecFile.ATT_GRID_WIDTH, grid.getLongitudeCount());
							H5Utils.addDoubleAttributeToGroup(h5File, gridGroup.getH5Group(),
									MultiLayerTecFile.ATT_GRID_MIN_VALUE, grid.getValMin());
							H5Utils.addDoubleAttributeToGroup(h5File, gridGroup.getH5Group(),
									MultiLayerTecFile.ATT_GRID_MAX_VALUE, grid.getValMax());
							H5Utils.writeDoubleDataSet(h5File, gridGroup.getH5Group(), gridType.name(), grid.getData(),
									true);
						}
					}
				}
			}
		} catch (Exception e) {
			isSuccess = false;
			throw e;
		}
	}

	public void writeAll(TectonicModel model) throws Exception {

		try {
			TectonicPlayerModel player = new TectonicPlayerModel();
			player.addPropertyChangeListener(evt -> {
				if (TectonicPlayerModel.PROPERTY_DATEINDEX.equals(evt.getPropertyName())) {
					model.setAge((Integer) evt.getNewValue());
				}
			});

			writeMetadata(model);

			List<? extends TectonicPlateModel> plates = model.getPlates();
			for (TectonicPlateModel plate : plates) {

				writePlateGrids(plate, plate.getMetadata().getTectonicLayers().get(0).getAge(), 0);

				plate.getPropertyChangeSupport().addPropertyChangeListener(evt -> {

					try {
						Integer currentAgeInterval = (Integer) evt.getNewValue();
						if (TectonicPlateModel.PROPERTY_CURRENT_AGE_INTERVAL.equals(evt.getPropertyName())) {
							if (currentAgeInterval.intValue() >= 0) {
								TectonicLayer range = plate.getMetadata().getTectonicLayers()
										.get(currentAgeInterval.intValue());
								writePlateGrids(plate, range.getAge(), currentAgeInterval.intValue());
							}
						}

					} catch (Exception e) {
						logger.error("Error writing plate " + plate.getMetadata().getPlateName(), e);
					}
				});
			}

			player.setMinDateIndex(Activator.getPreferences().getMinimumAge().getValue());
			player.setMaxDateIndex(Activator.getPreferences().getMaximumAge().getValue());
			player.setPlaying(true);

			for (int i = 0; i < MagneticAnomalies.max(); i++) {
				player.jumpToNextDate();
			}
			player.setPlaying(false);

		} catch (Exception e) {
			isSuccess = false;
			throw e;
		}
	}
}
