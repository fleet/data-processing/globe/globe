package fr.ifremer.globe.placa3d.anomalies.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.handler.PartManager;

/**
 * Handler which provides acess to the scale.time file editor.
 */
public class EditScaleTimeHandler {

	private static final String partID = "fr.ifremer.globe.editScaleTime";
	private Logger logger = LoggerFactory.getLogger(EditScaleTimeHandler.class);

	@CanExecute
	public boolean canExecute(ESelectionService selectionService) {
		return true;
	}

	@Execute
	public void execute(ESelectionService selectionService, final EPartService partService, MApplication application, EModelService modelService, Shell shell) {

		logger.info("Scale.time editor handler");

		MPart summaryViewPart = PartManager.createPart(partService, partID);
		PartManager.addPartToStack(summaryViewPart, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, summaryViewPart);
	}
}
