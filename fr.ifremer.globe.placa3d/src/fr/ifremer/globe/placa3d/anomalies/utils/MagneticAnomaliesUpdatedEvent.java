package fr.ifremer.globe.placa3d.anomalies.utils;

import fr.ifremer.globe.ui.events.BaseEvent;

/**
 * Event raised when file of Magnetic Anomalies has changed.
 */
public class MagneticAnomaliesUpdatedEvent implements BaseEvent {
}
