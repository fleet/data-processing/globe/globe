package fr.ifremer.globe.placa3d.anomalies.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.FrameworkUtil;

import fr.ifremer.globe.utils.cache.WorkspaceCache;

/**
 * Some utilities around echelle.temps file
 */
public class AnomaliesUtils {

	/** Path of the scale.time file. */
	private static final String scaleTimeFilePath = "anomalies/echelle.temps";
	/** Path of the scale.time file. */
	private static final File scaleTimeFileCachePath = new File(
			WorkspaceCache.getFrisePath() + File.separator + "echelle.temps");

	/**
	 * The constructor
	 */
	private AnomaliesUtils() {
	}

	/**
	 * @return the echelle.temps file path
	 */
	public static File getAnomaliesFilePath() throws IOException {
		if (!scaleTimeFileCachePath.exists()) {
			URL bundleURL = FileLocator.find(FrameworkUtil.getBundle(AnomaliesUtils.class), new Path(scaleTimeFilePath),
					null);
			String filePath = FileLocator.toFileURL(bundleURL).getPath();
			FileUtils.copyFile(new File(filePath), scaleTimeFileCachePath);
		}
		return scaleTimeFileCachePath;
	}
}
