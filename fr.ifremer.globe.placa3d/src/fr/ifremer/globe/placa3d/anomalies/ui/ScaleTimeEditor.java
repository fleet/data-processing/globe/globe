package fr.ifremer.globe.placa3d.anomalies.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.placa3d.anomalies.utils.AnomaliesUtils;
import fr.ifremer.globe.placa3d.application.event.PlacaEventTopics;

/**
 * Editor of the scale.time file.
 */
public class ScaleTimeEditor implements ISaveable {

	@Inject
	private IEventBroker eventBroker;
	@Inject
	private MPart part;

	// Attributes relative to the SWT components to display
	private Composite parent;
	private Text text;
	// True if unsaved modifications exists in the scale.time file
	private boolean isDirty;

	@PostConstruct
	public void createUI(final Composite parent) {
		// Initialize the SWT graphics components of the editor.
		this.parent = parent;
		this.parent.setLayout(new FillLayout(SWT.HORIZONTAL));
		this.text = new Text(parent, SWT.MULTI);
		this.text.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event event) {
				isDirty = true;
				part.setDirty(true);
			}
		});
		// Fill the editor with the scale.time file content.
		try {
			fillText();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Once the editor is filled, set the unsaved attributes to false.
		this.isDirty = false;
		this.part.setDirty(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ifremer.globe.model.ISaveable#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return isDirty;
	}

	@Persist
	public void save(IProgressMonitor monitor) {
		doSave(monitor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ifremer.globe.model.ISaveable#doSave(org.eclipse.core.runtime. IProgressMonitor)
	 */
	@Override
	public boolean doSave(IProgressMonitor monitor) {
		try {
			// Write the text to save into the scale.time file
			saveText(text.getText());
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Open and reference all the text contained in the scale.color file.
	 * 
	 * @throws IOException
	 */
	private void fillText() throws IOException {
		// Initialize the text references
		StringBuilder fileText = new StringBuilder();
		String tmpLine = "";
		// Read the scale.time file and reference its text in the previous
		// variables
		try (BufferedReader bufferedReader = new BufferedReader(
				new FileReader(AnomaliesUtils.getAnomaliesFilePath()))) {
			while ((tmpLine = bufferedReader.readLine()) != null) {
				fileText.append(tmpLine).append('\n');
			}
		}
		// Apply the captured text in the SWT text field
		this.text.setText(fileText.toString());
	}

	/**
	 * Write all the text to save in the scale.time file.
	 * 
	 * @throws IOException
	 */
	private void saveText(String textToSave) throws IOException {
		// Set the save attributes to false
		File scaleTimeFileCachePath = AnomaliesUtils.getAnomaliesFilePath();
		// Write the user text modifications in the scale.time file
		FileUtils.write(scaleTimeFileCachePath, textToSave, StandardCharsets.UTF_8, false);
		eventBroker.send(PlacaEventTopics.TOPIC_MAGNETIC_ANOMALIES_UPDATED, null);
		this.isDirty = false;
		this.part.setDirty(false);
	}
}
