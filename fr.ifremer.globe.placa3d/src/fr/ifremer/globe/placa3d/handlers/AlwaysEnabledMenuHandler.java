package fr.ifremer.globe.placa3d.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;

public class AlwaysEnabledMenuHandler {

	@CanExecute
	public boolean canExecute() {
		return true;
	}

}
