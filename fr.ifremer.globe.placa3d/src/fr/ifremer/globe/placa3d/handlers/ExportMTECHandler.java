package fr.ifremer.globe.placa3d.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Named;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.export.DatesRepository;
import fr.ifremer.globe.placa3d.export.MtecElevationExporter;
import fr.ifremer.globe.placa3d.export.MtecElevationExporter.MtecExportParameters;
import fr.ifremer.globe.placa3d.export.dialog.MultiLayerTecFileExporterModel;
import fr.ifremer.globe.placa3d.export.dialog.MultiLayerTecFileExporterWizard;
import fr.ifremer.globe.placa3d.info.MtecInfo;
import fr.ifremer.globe.placa3d.layers.PoleRotationLayer;
import fr.ifremer.globe.placa3d.store.TectonicPlateLayerStore;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.StoreUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer;

/**
 * Handler which allows user to export a MTEC file to a new file with the visual transformations applied to it.
 */
public class ExportMTECHandler extends AbstractNodeHandler {

	private Logger logger = LoggerFactory.getLogger(ExportMTECHandler.class);

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {

		Object[] selection = getSelection(selectionService);
		List<FileInfoNode> nodeList = getSelectionAsList(selection, ContentType.MTEC::equals);

		return nodeList.size() == selection.length && !nodeList.isEmpty();
	}

	@Execute
	public void execute(final ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		// Notify handler triggering in the console
		logger.info("Export to MTEC handler");

		// Get the MTEC files selected by the user
		Object[] selection = getSelection(selectionService);
		List<FileInfoNode> listofFileToTranscode = getSelectionAsList(selection, ContentType.MTEC::equals);
		List<String> fileNameList = StoreUtils.getFilePath(StoreUtils.getInfoStoresFromNodes(listofFileToTranscode));
		// Initialize the configuration variables which defines the MTEC export
		// settings which will be manipulated by the user
		double minimumAge = Activator.getPreferences().getMinimumAge().getValue();
		double maximumAge = Activator.getPreferences().getMaximumAge().getValue();
		// Set up the current age from the first loaded file
		TectonicPlateLayerStore tectonicPlateLayerStore = ((MtecInfo) listofFileToTranscode.get(0).getFileInfo())
				.getPlateLayerStores().iterator().next();

		PoleRotationLayer poleRotationLayer = (PoleRotationLayer) tectonicPlateLayerStore.getLayers().get(0).getFirst();
		TectonicMovement movement = poleRotationLayer.getMovement();

		// Initialize the configuration wizard
		MultiLayerTecFileExporterModel multiLayerTecFileExporterModel = new MultiLayerTecFileExporterModel();
		fileNameList.forEach(filename -> multiLayerTecFileExporterModel.getInputFiles().add(new File(filename)));
		multiLayerTecFileExporterModel.getAgeRange().set(new DoubleRange(minimumAge, maximumAge));
		multiLayerTecFileExporterModel.getCurrentAge().set(movement.getDateIndex());

		MultiLayerTecFileExporterWizard multiLayerTecFileExporterWizard = new MultiLayerTecFileExporterWizard(
				multiLayerTecFileExporterModel);

		// Open the configuration wizard
		WizardDialog wizardDialog = new WizardDialog(shell, multiLayerTecFileExporterWizard);
		wizardDialog.setMinimumPageSize(500, 240);
		// Start the export if user validate the process in the wizard
		if (wizardDialog.open() == Window.OK) {
			OutputFileNameComputer outputFileNameComputer = multiLayerTecFileExporterWizard.getOutputFileNameComputer();
			// Initialize a list which references files whose export process have failed
			List<FileInfoNode> filesInError = new ArrayList<>();
			// Initialize all the dates concerned by the exportation as defined by user settings
			DatesRepository datesRepository = new DatesRepository(multiLayerTecFileExporterModel.getAgeSetting().get(),
					movement.getDateIndex(), multiLayerTecFileExporterModel.getStartAge().doubleValue(),
					multiLayerTecFileExporterModel.getEndAge().doubleValue(),
					multiLayerTecFileExporterModel.getCustomAge().intValue(),
					multiLayerTecFileExporterModel.getAgeStep().intValue());

			IProcessService.grab().runInForeground("Export MTEC", (monitor, logger) -> {
				SubMonitor subMonitor = SubMonitor.convert(monitor, "Exporting files...", listofFileToTranscode.size());
				for (int i = 0; i < listofFileToTranscode.size(); i++) {
					try {
						MtecExportParameters parameters = new MtecExportParameters();
						parameters.setDatesRepository(datesRepository);
						parameters.setFileToTranscode(listofFileToTranscode.get(i));
						parameters.setOffsetMode(multiLayerTecFileExporterModel.getOffsetMode().get());
						parameters.setOffset(multiLayerTecFileExporterModel.getConstantOffset().intValue());
						parameters.setOffsetMappingFileValues(
								multiLayerTecFileExporterWizard.getOffsetMappingFileValues());
						parameters.setOutXyzFormat(multiLayerTecFileExporterModel.getOutXyzFormat().isTrue());
						parameters.setOutTiffFormat(multiLayerTecFileExporterModel.getOutTiffFormat().isTrue());
						parameters.setOutTextureFormat(multiLayerTecFileExporterModel.getOutTextureFormat().isTrue());
						parameters.setOutputFileNameComputer(outputFileNameComputer);
						parameters.setOutputFileNameComputerModel(multiLayerTecFileExporterModel);
						parameters.setOverwriteExistingFiles(
								multiLayerTecFileExporterModel.getOverwriteExistingFiles().isTrue());

						MtecElevationExporter xyzExporter = new MtecElevationExporter();
						xyzExporter.export(parameters, subMonitor.split(1));
					} catch (OperationCanceledException e) {
						// Cancel requested
						break;
					} catch (Exception e) {
						logger.error("Error while exporting mtec : ", e);
						filesInError.add(listofFileToTranscode.get(i));
					}
				}

				monitor.done();
				if (!filesInError.isEmpty()) {
					StringBuilder fileList = new StringBuilder();
					for (FileInfoNode f : filesInError) {
						fileList.append(System.getProperty("line.separator")).append(f.getName());
					}
					MessageDialog.openWarning(shell, "Errors while transcoding",
							String.format(
									"The transcoding process has encountered errors with the following files : %s",
									fileList.toString()));
				}

				return filesInError.isEmpty() ? Status.OK_STATUS : Status.error("Error while exporting MTEC");
			});

		}
	}
}