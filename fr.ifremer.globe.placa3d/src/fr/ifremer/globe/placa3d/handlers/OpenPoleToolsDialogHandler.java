package fr.ifremer.globe.placa3d.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.placa3d.gui.PoleToolsDialog;

public class OpenPoleToolsDialogHandler {

	private PoleToolsDialog dlg = null;

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) final Shell shell) {

		dlg = new PoleToolsDialog(shell);

		dlg.open();

	}

}
