package fr.ifremer.globe.placa3d.handlers;


import java.io.File;
import java.io.IOException;

import jakarta.inject.Named;

import org.apache.commons.io.FileUtils;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.placa3d.gui.Frise;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.utils.cache.WorkspaceCache;

/**
 * Open a file chooser to define a new Gradstein scale picture.
 */
public class GradsteinScaleHandler {

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) final Shell shell) {

		FileDialog fileDialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
		fileDialog.setText(DicoBundle.getString("GRADSTEIN_IMAGE_FILE_CHOOSER"));
		String[] filterExt = { "*.png" };
		fileDialog.setFilterExtensions(filterExt);
		String selectedScalePath = fileDialog.open();

		try {
			if(selectedScalePath != null) {
				FileUtils.copyFile(new File(selectedScalePath), new File(WorkspaceCache.getFrisePath() + File.separator + "frise.png"));
				Frise.getInstance().updateLayer();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
