package fr.ifremer.globe.placa3d.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.placa3d.gui.MTecGeneratorWizard;

/**
 * This handler takes a parameter as input : ID="tectonicFileFormatParameter".
 * <p>
 * If tectonicFileFormatParameter = "TEC" ==> opens the TEC generator dialog. <br/>
 * If tectonicFileFormatParameter = "MTEC" ==> opens the Multilayer TEC generator dialog.
 * 
 * @author apside
 */
public class CreateTectonicHandler {

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) final Shell shell, IEclipseContext eclipseContext) {
		MTecGeneratorWizard wizard = new MTecGeneratorWizard(shell);
		ContextInjectionFactory.inject(wizard, eclipseContext);
		wizard.start();
	}
}
