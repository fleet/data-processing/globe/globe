package fr.ifremer.globe.placa3d.preferences;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;

public class Placa3dPreferences extends PreferenceComposite {

	private BooleanPreferenceAttribute morphing;
	private IntegerPreferenceAttribute nbDigitsLatitude;
	private IntegerPreferenceAttribute nbDigitsLongitude;
	private IntegerPreferenceAttribute nbDigitsAngle;
	private IntegerPreferenceAttribute nbDigitsAge;
	private IntegerPreferenceAttribute maximumAge;
	private IntegerPreferenceAttribute minimumAge;
	// hack to display subsidence
	private IntegerPreferenceAttribute subsidenceDisplaySettings;

	public Placa3dPreferences(PreferenceComposite superNode, String nodeName) {
		super(superNode, nodeName);

		morphing = new BooleanPreferenceAttribute("morphing", "Compute Morphing tiles", false);
		declareAttribute(morphing);

		PreferenceComposite numberOfDigit = new PreferenceComposite(this, "Number Of Digits");

		nbDigitsLatitude = new IntegerPreferenceAttribute("nbDigitsLatitude", "nbDigits Latitude", 4);
		nbDigitsLongitude = new IntegerPreferenceAttribute("nbDigitsLongitude", "nbDigits Longitude", 4);
		nbDigitsAngle = new IntegerPreferenceAttribute("nbDigitsAngle", "nbDigits Angle", 4);
		nbDigitsAge = new IntegerPreferenceAttribute("nbDigitsAge", "nbDigits Age", 4);

		numberOfDigit.declareAttribute(nbDigitsLatitude);
		numberOfDigit.declareAttribute(nbDigitsLongitude);
		numberOfDigit.declareAttribute(nbDigitsAngle);
		numberOfDigit.declareAttribute(nbDigitsAge);

		maximumAge = new IntegerPreferenceAttribute("maximumAge", "Age maximum", 200);
		minimumAge = new IntegerPreferenceAttribute("minimumAge", "Age minimum", 0);
		subsidenceDisplaySettings = new IntegerPreferenceAttribute("subsidenceDisplay",
				"hack subsidence display 0=normal, 1=display age, 2= display elevation offset", 0);

		declareAttribute(maximumAge);
		declareAttribute(minimumAge);
		declareAttribute(subsidenceDisplaySettings);

		this.load();

		morphing.addObserver((observable, object) -> updateMorphingNorification());

	}

	/**
	 * Notify user of its parameters application conscequences and update the valid magnetic anolalies dates.
	 */
	private void updateMorphingNorification() {
		if (morphing.getValue().booleanValue()) {
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warning",
					DicoBundle.getString("TEC_PARAMETERS_MORPHING_CHANGE"));
		}
	}

	public IntegerPreferenceAttribute getNbDigitsLatitude() {
		return nbDigitsLatitude;
	}

	public IntegerPreferenceAttribute getNbDigitsLongitude() {
		return nbDigitsLongitude;
	}

	public IntegerPreferenceAttribute getNbDigitsAngle() {
		return nbDigitsAngle;
	}

	public IntegerPreferenceAttribute getNbDigitsAge() {
		return nbDigitsAge;
	}

	public IntegerPreferenceAttribute getMinimumAge() {
		return minimumAge;
	}

	public IntegerPreferenceAttribute getMaximumAge() {
		return maximumAge;
	}

	public IntegerPreferenceAttribute getSubsidenceDisplaySettings() {
		return subsidenceDisplaySettings;
	}

	/**
	 * @return the {@link #morphing}
	 */
	public BooleanPreferenceAttribute getMorphing() {
		return morphing;
	}

	/**
	 * @param morphing the {@link #morphing} to set
	 */
	public void setMorphing(BooleanPreferenceAttribute morphing) {
		this.morphing = morphing;
	}

}
