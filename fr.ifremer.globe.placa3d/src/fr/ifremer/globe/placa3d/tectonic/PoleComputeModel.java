package fr.ifremer.globe.placa3d.tectonic;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.placa3d.util.TectonicUtils;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.layers.LayerList;

public class PoleComputeModel {

	/** The fixed tectonicDefinition. **/
	TectonicDefinition fixedTectonicDefinition;
	/** The moved tectonicDefinition. **/	
	TectonicDefinition movedTectonicDefinition;

	/** The latitude longitude for translation end rotation points. **/	
	private LatLon translationStartLatlon;
	private LatLon translationEndLatlon;
	private LatLon rotationCenterLatlon;
	private LatLon rotationStartLatlon;
	private LatLon rotationEndLatlon;
	
	private List<Rotation> rotationList;
	
	private Rotation computedRotation;
	
	private LayerList originalLayerList;


	public void setComputedRotation(Rotation computedRotation) {
		this.computedRotation = computedRotation;
	}


	public PoleComputeModel() {
		rotationList = new ArrayList<Rotation>();
	}
	
	
	/**
	 * Compute the final pole of rotation with the list of pole then apply if to the moved tectonic plate.
	 */
	public void computeAndApplyPoleRotation() {
		List<Matrix> matrixList = new ArrayList<Matrix>();		

		for (Rotation rotation : getRotationList()) {
			Matrix m = TectonicUtils.computeMatrix(rotation.getLatLon().getLatitude().getDegrees(), rotation.getLatLon().getLongitude().getDegrees(), rotation.getAngle());
			matrixList.add(m);		

		}

		// multiplication must be done in reverse due to none commutativity
		Matrix movement = Matrix.IDENTITY;
		for (int ii = 0; ii < matrixList.size(); ii++) {
			movement = movement.multiply(matrixList.get(matrixList.size() - ii - 1));
		}

		setComputedRotation(TectonicUtils.convertMatrixInRot(movement, 0, 0));

		getMovedTectonicDefinition().getPoleRotationLayer().getMovement().setOffsetMatrix(movement);
	}

	
	public TectonicDefinition getFixedTectonicDefinition() {
		return fixedTectonicDefinition;
	}
	public void setFixedTectonicDefinition(
			TectonicDefinition fixedTectonicDefinition) {
		this.fixedTectonicDefinition = fixedTectonicDefinition;
	}
	public TectonicDefinition getMovedTectonicDefinition() {
		return movedTectonicDefinition;
	}
	public void setMovedTectonicDefinition(
			TectonicDefinition movedTectonicDefinition) {
		this.movedTectonicDefinition = movedTectonicDefinition;
	}

	public LatLon getTranslationStartLatlon() {
		return translationStartLatlon;
	}
	public void setTranslationStartLatlon(LatLon translationStartLatlon) {
		this.translationStartLatlon = translationStartLatlon;
	}
	public LatLon getTranslationEndLatlon() {
		return translationEndLatlon;
	}
	public void setTranslationEndLatlon(LatLon translationEndLatlon) {
		this.translationEndLatlon = translationEndLatlon;
	}
	public LatLon getRotationCenterLatlon() {
		return rotationCenterLatlon;
	}
	public void setRotationCenterLatlon(LatLon rotationCenterLatlon) {
		this.rotationCenterLatlon = rotationCenterLatlon;
	}
	public LatLon getRotationStartLatlon() {
		return rotationStartLatlon;
	}
	public void setRotationStartLatlon(LatLon rotationStartLatlon) {
		this.rotationStartLatlon = rotationStartLatlon;
	}
	public LatLon getRotationEndLatlon() {
		return rotationEndLatlon;
	}
	public void setRotationEndLatlon(LatLon rotationEndLatlon) {
		this.rotationEndLatlon = rotationEndLatlon;
	}
	public List<Rotation> getRotationList() {
		return rotationList;
	}
	public void setRotationList(List<Rotation> rotationList) {
		this.rotationList = rotationList;
	}
	
	public Rotation getComputedRotation() {
		return computedRotation;
	}
	
	public LayerList getOriginalLayerList() {
		return originalLayerList;
	}

	public void setOriginalLayerList(LayerList originalLayerList) {
		this.originalLayerList = originalLayerList;
	}


}
