/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.tectonic;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.slf4j.Logger;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.utils.TextFileParser;

/**
 * Loader of Age/Offset file for simulating the subsidence.
 */
public class AgeOffsetFileLoader {

	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AgeOffsetFileLoader.class);

	/**
	 * Constructor.
	 */
	public AgeOffsetFileLoader() {
	}

	public double[] loadOffsetFile(File f) throws IOException {
		double[] result = null;
		List<Pair<Double, Double>> lines = new ArrayList<>();
		List<String> rawFile = FileUtils.readLines(f, Charset.forName("ASCII"));

		List<String> fileContent = rawFile.stream().map(x -> { // remove comments
			return TextFileParser.removeComments(x);
		}).map(x -> {
			return x.trim();
		}) // remove leading and trailing space
				.filter(x -> !x.isEmpty()) // remove empty lines
				.collect(Collectors.toList()); // collect as a list

		ArrayList<String> errorLines = new ArrayList<>();
		for (String line : fileContent) {

			try (Scanner scanner = new Scanner(line);) {

				double age = scanner.nextDouble();
				double offset = scanner.nextDouble();
				lines.add(new Pair<>(age, offset));

			} catch (Exception e) {
				logger.error("Invalid values found in file " + f.getAbsolutePath() + "line " + line, e);
				errorLines.add(line);
			}

		}

		if (errorLines.size() > 0) {
			throw new IOException("Invalid values found in file" + f.getAbsolutePath() + "\nLines in error " + errorLines.toString());
		}

		// Sort read data by ascending age
		Collections.sort(lines, (o1, o2) -> {
			return o1.getFirst().intValue() - o2.getFirst().intValue();
		});

		if (!lines.isEmpty()) {

			// Verify the max age
			Pair<Double, Double> last = lines.get(lines.size() - 1);

			if (last.getFirst() != (double) Activator.getPreferences().getMaximumAge().getValue()) {
				if (last.getFirst() > Activator.getPreferences().getMaximumAge().getValue()) {
					lines.remove(lines.size() - 1);
				}
				lines.add(new Pair<>((double) Activator.getPreferences().getMaximumAge().getValue(), last.getSecond()));
			}

			result = new double[Activator.getPreferences().getMaximumAge().getValue() + 1];

			// At this point, data are like this :
			//
			// 0 --> 0
			// 50 --> 10
			// 52 --> 5
			// 57 --> 0
			//
			// And the expected result is :
			//
			// - Range 00..50 : offset(n) = offset(n-1) + 0.2
			// - Range 51..52 : offset(n) = offset(n-1) - 2.5
			// - Range 53..58 : offset(n) = offset(n-1) - 1

			// Data interpolation
			Iterator<Pair<Double, Double>> iteratorAgesOffsets = lines.iterator();

			int iOffset = 0;
			double previousAge = 0.0;
			double previousOffset = 0.0;

			while (iOffset < result.length && iteratorAgesOffsets.hasNext()) {

				Pair<Double, Double> ageAndOffset = iteratorAgesOffsets.next();
				double age = ageAndOffset.getFirst();
				double offset = ageAndOffset.getSecond();

				if (age > previousAge) {
					double deltaAge = age - previousAge;
					double deltaOffset = offset - previousOffset;
					double offsetIncr = deltaOffset / deltaAge;

					for (int i = 0; i < deltaAge && iOffset < result.length; i++) {
						previousOffset += offsetIncr;
						result[iOffset] = previousOffset;
						iOffset++;
					}
				} else {
					previousOffset = offset;
					result[iOffset] = previousOffset;
					iOffset++;
				}
				previousAge = age;
			}
		}

		return result;
	}

	/** Open a File chooser to select a age/offset file. */
	public String chooseFile() {
		// Open a FileChooser to choose the offset file
		FileDialog dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
		dialog.setFilterNames(new String[] { "ASCII file with age/offset pairs" });
		dialog.setFilterExtensions(new String[] { "*.*" });
		return dialog.open();
	}

}
