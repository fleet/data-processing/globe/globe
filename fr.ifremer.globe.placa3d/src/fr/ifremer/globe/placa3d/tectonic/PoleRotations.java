package fr.ifremer.globe.placa3d.tectonic;

import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Rotation poles defining tectonic movement Rotation matrix computation is from PlacaM software
 * 
 * @author mgaro
 * 
 */
public class PoleRotations {

	private static final Logger LOGGER = LoggerFactory.getLogger(PoleRotations.class);

	private List<LatLon> latLonList;

	private List<Double> angleList;
	private List<Double> dateList;
	private List<Double> endList;
	private List<Matrix> matrixList;

	/**
	 * defines the list of rotation poles defined by period (begin/end) a rotation pole is defined by a position (latitude,longitude) and an angular speed
	 * 
	 * @param latLon
	 * @param angle
	 * @param begin
	 * @param end
	 */
	public PoleRotations() {
		super();
		this.latLonList = new ArrayList<LatLon>();
		this.angleList = new ArrayList<Double>();
		this.dateList = new ArrayList<Double>();
		this.endList = new ArrayList<Double>();
		this.matrixList = new ArrayList<Matrix>();
	}

	public boolean isNotEmpty() {
		return !latLonList.isEmpty() && !angleList.isEmpty() && !dateList.isEmpty() && !endList.isEmpty() && !matrixList.isEmpty();
	}

	public void addRotation(Rotation rotation) {

		LatLon latLon = rotation.getLatLon();
		double angle = rotation.getAngle();

		latLonList.add(latLon);
		angleList.add(angle);

		Double dateBegin = rotation.getBegin();
		Double dateEnd = rotation.getEnd();
		if (dateBegin != null && dateEnd != null) {

			// pre-computes matrix
			// performance improvement for full anomaly
			matrixList.add(TectonicUtils.computeMatrix(latLon.getLatitude().getDegrees(), latLon.getLongitude().getDegrees(), angle));

			dateList.add(dateBegin);
			endList.add(dateEnd);
		} else {
			LOGGER.error("invalide tectonic pole");
		}
	}

	public List<Double> getDates() {
		return dateList;
	}

	public List<Double> getEndDates() {
		return endList;
	}

	public List<String> getSDates() {
		List<String> ret = new ArrayList<String>();
		for (Double date : dateList) {
			ret.add(MagneticAnomalies.getInstance().getStringAnomaly(date));
		}
		return ret;
	}

	public List<String> getSEndDates() {
		List<String> ret = new ArrayList<String>();
		for (Double date : endList) {
			ret.add(MagneticAnomalies.getInstance().getStringAnomaly(date));
		}
		return ret;
	}

	// Care : do not try to set this list, its a new list created from the latlonList. To modify, use getLatLonList.
	public List<Double> getLongitudes() {
		List<Double> ret = new ArrayList<Double>();
		for (LatLon latLon : latLonList) {
			ret.add(latLon.getLongitude().degrees);
		}
		return ret;
	}

	// Care : do not try to set this list, its a new list created from the latlonList. To modify, use getLatLonList.
	public List<Double> getLatitudes() {
		List<Double> ret = new ArrayList<Double>();
		for (LatLon latLon : latLonList) {
			ret.add(latLon.getLatitude().degrees);
		}
		return ret;
	}

	public List<LatLon> getLatLonList() {
		return latLonList;
	}

	public List<Double> getAngles() {
		return angleList;
	}

	public List<Matrix> getMatrix() {
		return matrixList;
	}

	public int size() {
		return angleList.size();
	}

	/***
	 * return the rotation matrix at a specific date useful if the anomaly is fully included in the time period
	 * 
	 * @param date
	 * @return rotation matrix for a full anomaly
	 */
	public Matrix getMatrix(int date) {

		for (int index = 0; index < dateList.size(); index++) {
			if (dateList.get(index + 1).intValue() >= date) {
				return matrixList.get(index);
			}
		}

		return null;
	}

	/***
	 * return the rotation matrix at a specific date and for a specific duration useful if the anomaly is not fully included in the time period
	 * 
	 * @param index
	 * @param angularPeriod
	 * @return rotation matrix for a part anomaly
	 */
	public Matrix getMatrix(int index, double angularPeriod) {
		// out of range : no movement
		if (index >= latLonList.size() || index >= angleList.size()) {
			return Matrix.IDENTITY;
		}

		double poleLatitude = latLonList.get(index).getLatitude().getDegrees();
		double poleLongitude = latLonList.get(index).getLongitude().getDegrees();
		double begin = dateList.get(index).doubleValue();
		double end = endList.get(index).doubleValue();
		double angle = angleList.get(index).doubleValue();
		double angleSpeed = angle / (end - begin);

		return TectonicUtils.computeMatrix(poleLatitude, poleLongitude, angularPeriod * angleSpeed);

	}

	@Override
	public String toString() {
		return "PoleRotations [latLonList=" + latLonList + ", angleList=" + angleList + ", beginList=" + dateList + ", endList=" + endList + ", matrixList=" + matrixList + "]";
	}

}
