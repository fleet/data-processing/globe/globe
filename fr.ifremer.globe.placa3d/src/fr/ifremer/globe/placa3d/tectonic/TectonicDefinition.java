package fr.ifremer.globe.placa3d.tectonic;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import jakarta.inject.Inject;
import javax.swing.Timer;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.e4.core.services.events.IEventBroker;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.application.context.ContextInitializer;
import fr.ifremer.globe.placa3d.application.event.LayerStoreAddedEvent;
import fr.ifremer.globe.placa3d.application.event.PlacaEventTopics;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.layers.PoleRotationLayer;
import fr.ifremer.globe.placa3d.layers.TecShaderElevationLayer;
import fr.ifremer.globe.placa3d.layers.TectonicRemanentLayer;
import fr.ifremer.globe.placa3d.render.cot.CotSegmentRenderer;
import fr.ifremer.globe.placa3d.session.Placa3dSession;
import fr.ifremer.globe.placa3d.store.LayerStore;
import fr.ifremer.globe.placa3d.store.TectonicPlateLayerStore;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.IMovableLayer;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.terrain.InterpolateElevationModel;
import fr.ifremer.viewer3d.util.RefreshOpenGLView;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Renderable;

/**
 * @author mgaro
 *
 */
public class TectonicDefinition implements Disposable {

	/**
	 * To allow to start only one timer on all TectonicDefinition when 'allplates' is enabled.
	 */
	private transient static boolean timerAlreadyStarted;

	/** Default time interval between two frames in "auto-play" mode. */
	private transient static final int TIMER_DEFAULT_DELAY = 4000;

	// ==================================================================

	@XStreamOmitField
	private transient PropertyChangeSupport pcs;

	private transient List<LayerStore> layerList;
	private List<String> layerName;
	private transient PoleRotationLayer poleRotationLayer;
	private String rotationPoleName;

	/** Model of the tectonic age player. */
	private transient TectonicPlayerModel player;

	/**
	 * Tectonic model containing a set of consistent tectonic plates (but generally only one plate per tectonic model).
	 */
	private transient TectonicModel tectonicModel;

	/** Model of the tectonic plate linked to this TectonicDefinition. */
	private transient TectonicPlateModel tectonicPlateModel;

	/**
	 * Listen to the tectonic model and updates elevation and texture of this plate. See
	 * {@link #refreshElevationAndTexture()} for more details.
	 */
	private PropertyChangeListener ageIntervalListener;

	/** Timer used for auto-play mode. */
	private transient Timer timer;

	private transient HashMap<ShaderElevationLayer, List<TectonicRemanentLayer>> remanentToRender;
	/**
	 * The list of ShaderElevationLayer which allows remanence
	 */
	private transient List<ShaderElevationLayer> remanentLayers;

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	/**
	 * Constructor. Makes a standalone TectonicDefinition (ie. not related to a {@link TectonicPlateModel}).
	 *
	 * @param poleRotationLayer The layer defining rotation poles of the tectonic plate.
	 */
	public TectonicDefinition(PoleRotationLayer poleRotationLayer) {
		super();
		ContextInitializer.inject(this);
		pcs = new PropertyChangeSupport(this);

		layerName = new ArrayList<>();
		layerList = new ArrayList<>();
		remanentToRender = new HashMap<>();
		remanentLayers = new ArrayList<>();

		this.poleRotationLayer = poleRotationLayer;
		rotationPoleName = poleRotationLayer.getMovement().getFileName();

		player = new TectonicPlayerModel();

		player.addPropertyChangeListener(event -> {
			handlePlayerEvent(event);
		});

		timer = new Timer(0, actionEvent -> {
			player.jumpToNextDate();
		});
		timer.setInitialDelay(0);
		timer.setDelay(TIMER_DEFAULT_DELAY);

		// ModelRoot.getInstance().getEventBus().subscribe(this);
	}

	/**
	 * Constructor that builds a TectonicDefinition related to a {@link TectonicPlateModel}.
	 * <p>
	 * When building such a TectonicDefinition (ie. linked to a {@link TectonicPlateModel}), a listener is installed on
	 * the {@link TectonicPlateModel} in order to know when the current age index enters a special era in which
	 * elevation and texture grids exist and must be reloaded from the disk.
	 *
	 * @param poleRotationLayer The layer defining rotation poles of the tectonic plate.
	 * @param tectonicModel The global tectonic model the current plate belongs to.
	 * @param plate The current plate's model that will be animated.
	 */
	public TectonicDefinition(PoleRotationLayer poleRotationLayer, TectonicModel tectonicModel,
			TectonicPlateModel plate) {
		this(poleRotationLayer);

		this.tectonicModel = tectonicModel;
		tectonicPlateModel = plate;

		ageIntervalListener = evt -> {
			if (TectonicPlateModel.PROPERTY_CURRENT_AGE_INTERVAL.equals(evt.getPropertyName())
					&& evt.getSource() == tectonicPlateModel) {
				refreshElevationAndTexture();
			}
		};
	}

	public void hookListeners() {
		tectonicPlateModel.getPropertyChangeSupport().addPropertyChangeListener(ageIntervalListener);
	}

	protected void handlePlayerEvent(PropertyChangeEvent event) {

		String name = event.getPropertyName();
		if (TectonicPlayerModel.PROPERTY_DATEINDEX.equals(name)) {
			// int oldIdx = (Integer) event.getOldValue();
			int newIdx = (Integer) event.getNewValue();
			refresh(newIdx);
		} else if (TectonicPlayerModel.PROPERTY_PLAYING.equals(name)
				|| TectonicPlayerModel.PROPERTY_REVERSEPLAYING.equals(name)) {
			boolean value = (Boolean) event.getNewValue();
			if (value) {
				if (timerAlreadyStarted == false) {
					timer.start();
					timerAlreadyStarted = true;
				}
			} else {
				timer.stop();
				timerAlreadyStarted = false;
			}
		} else if (TectonicPlayerModel.PROPERTY_PLAYINGSPEED.equals(name)) {

			float speed = player.getPlayingSpeed();
			if (speed > 0) {
				float delay = TIMER_DEFAULT_DELAY / speed;
				timer.setDelay((int) delay);
			}
		} else if (TectonicPlayerModel.PROPERTY_CHANGEREFERENTIAL.equals(name)) {
			if (player.isReferentiel()) {
				TectonicMovement.setReferentialMovement(getPoleRotationLayer().getMovement());
			} else {
				TectonicMovement.setReferentialMovement(null);
			}
		} else if (TectonicPlayerModel.PROPERTY_OFFSET.equals(name)) {
			propagateElevationOffset((Double) event.getNewValue());
		} else if (TectonicPlayerModel.PROPERTY_OFFSETMODE.equals(name)) {
			propagateOffsetMode((OffsetMode) event.getNewValue());
		}
	}

	/**
	 * PRopagates the elevation offset to the tectonic elevation model.
	 *
	 * @param offset The current offset value.
	 */
	protected void propagateElevationOffset(double offset) {
		for (LayerStore layerStore : layerList) {
			List<Pair<Layer, ElevationModel>> layers = layerStore.getLayers();
			for (Pair<Layer, ElevationModel> pair : layers) {

				// Propagate offset on layer
				Layer first = pair.getFirst();
				if (first instanceof MyAbstractLayer) {
					((MyAbstractLayer) first).setOffset(offset);
				}
				// and on elevation model
				ElevationModel elevationModel = pair.getSecond();
				if (elevationModel != null && elevationModel instanceof InterpolateElevationModel) {
					InterpolateElevationModel em = (InterpolateElevationModel) elevationModel;
					em.setElevationModelParameters(em.getElevationModelParameters().withOffset(offset));
				}
			}
		}
	}

	/** Alert layers that offset has to be computed */
	protected void propagateOffsetMode(OffsetMode offsetMode) {
		for (LayerStore layerStore : layerList) {
			List<Pair<Layer, ElevationModel>> layers = layerStore.getLayers();
			for (Pair<Layer, ElevationModel> pair : layers) {

				// Propagate offset on layer
				Layer first = pair.getFirst();
				if (first instanceof MyAbstractLayer) {
					((MyAbstractLayer) first).setOffsetAuto(offsetMode == OffsetMode.AUTO);
				}
			}
		}
	}

	public void postSessionRead(Placa3dSession placa3dSession) {

		// Restore the offset mode
		OffsetMode offsetMode = placa3dSession.getOffsetModeByPlate(this);
		File offsetFile = placa3dSession.getOffsetFileByPlate(this);
		player.setOffsetMode(offsetMode, 0.0, offsetFile);

		// Restore associations with other layer stores
		List<String> dependentLayerNames = placa3dSession.getDependentLayersByPlate(this);
		if (dependentLayerNames != null) {
			for (String dependentLayerName : dependentLayerNames) {
				addLayerStore(dependentLayerName);
			}
		}
	}

	/**
	 * This method stores session parameters into the current Placa3DSession.
	 */
	public void preSessionWrite(Placa3dSession placa3dSession) {
		placa3dSession.setDependentLayersByPlate(this, layerName);
		placa3dSession.setOffsetModeByPlate(this, player.getOffsetMode());
		placa3dSession.setOffsetFileByPlate(this, player.getOffsetFile());
	}

	public List<LayerStore> getLayerStoreList() {
		List<LayerStore> ret = new ArrayList<>(layerList);
		return ret;
	}

	public TectonicPlayerModel getPlayer() {
		return player;
	}

	public PoleRotationLayer getPoleRotationLayer() {
		return poleRotationLayer;
	}

	public List<ShaderElevationLayer> getRemanentLayers() {
		return remanentLayers;
	}

	public void setRemanentLayers(List<ShaderElevationLayer> remanentLayers) {
		this.remanentLayers = remanentLayers;
	}

	/**
	 * The hash map of which link each ShaderElevationLayer with his remanent layers
	 */

	/**
	 * Displays or hides remanent layers
	 */
	public void remanent() {
		LayerList layerList = Viewer3D.getModel().getLayers();

		for (ShaderElevationLayer l : remanentLayers) {
			// Displays or hides remanent layers
			List<TectonicRemanentLayer> remanent = remanentToRender.get(l);

			// check if the first remanent layer is in the layerlist
			if (!layerList.contains(remanent.get(0))) {

				// if not, add it before the parent layer
				int indexParent = 0;
				for (Layer p : layerList) {
					if (p == l) {
						break;
					}
					indexParent++;
				}
				for (TectonicRemanentLayer lr : remanent) {
					// add just before parent layer
					// for superposition purpose
					Viewer3D.getModel().getLayers().add(indexParent, lr);
				}

			}

			for (TectonicRemanentLayer lr : remanent) {
				lr.setEnabled(player.isRemanent());
			}

			// hides (remove) or displays (add) remanent parent layers
			if (player.isRemanent()) {
				layerList.remove(l);
			} else {
				layerList.add(l);
			}

		}
	}

	/**
	 * Creates the remanent layers defined for each anomaly era beginnning
	 *
	 * @param parentLayer
	 */
	public void createRemanentLayers(ShaderElevationLayer parentLayer) {

		if (getPoleRotationLayer().getMovement() != null
				&& getPoleRotationLayer().getMovement().getEndDates() != null) {
			TectonicRemanentLayer layer = null;
			for (Double date : getPoleRotationLayer().getMovement().getEndDates()) {
				// add to remanent list for visibility purpose
				List<TectonicRemanentLayer> remanent = remanentToRender.get(parentLayer);
				layer = new TectonicRemanentLayer(parentLayer, this, date.intValue());
				layer.setEnabled(player.isRemanent());
				remanent.add(layer);
			}
		}
	}

	private void refresh(int newIndex) {
		if (TectonicDataset.getInstance().getConfiguration().isAllPlates()) {
			for (TectonicDefinition plate : TectonicDataset.getInstance().getPlates()) {
				plate.player.setDateIndex(newIndex);
				plate.propagateAgeIndex();
			}
		} else {
			propagateAgeIndex();
		}

		RefreshOpenGLView.fullRefresh();
	}

	private void refreshElevationAndTexture() {
		// Make the layer's texture cache obsolete to force its tiles to be
		// reloaded according to the new age index.
		for (LayerStore layerStore : layerList) {
			if (layerStore instanceof TectonicPlateLayerStore plateLayerStore) {
				List<Pair<Layer, ElevationModel>> layers = plateLayerStore.getLayers();
				for (Pair<Layer, ElevationModel> pair : layers) {
					Layer layer = pair.getFirst();
					if (layer instanceof TecShaderElevationLayer shaderLayer) {
						shaderLayer.setTextureCacheObsolete();
					}
				}
			}
		}
	}

	/**
	 * Propagates the current player's age index to the concerned objets.
	 */
	private void propagateAgeIndex() {
		if (tectonicModel != null) {
			tectonicModel.setAge(player.getDateIndex());
			if (Activator.getPreferences().getMorphing().getValue().booleanValue()) {
				refreshElevationAndTexture();
			}
		} else if (getPoleRotationLayer().getMovement() != null) {
			getPoleRotationLayer().getMovement().setDateIndex(player.getDateIndex());
		}
	}

	/**
	 * remove link between a layer store and the tectonic definition.
	 *
	 * @param store layerStore
	 */

	public void removeLinkToLayerStore(LayerStore store) {
		for (Pair<Layer, ElevationModel> pair : store.getLayers()) {
			Layer layer = pair.getFirst();

			if (layer instanceof IMovableLayer) {
				((IMovableLayer) layer).setMovement(null);

				// terrain layer could be remanent
				if (layer instanceof ShaderElevationLayer && ((ShaderElevationLayer) layer).isInt24()) {
					remanentLayers.remove(layer);
					remanentToRender.remove(layer);
				}
			} else if (layer instanceof RenderableLayer) {

				for (Renderable r : ((RenderableLayer) layer).getRenderables()) {
					if (r instanceof CotSegmentRenderer) {
						((CotSegmentRenderer) r).setMovement(null);
					}
				}
			}

			// elevation could move when defined as
			// IMovableLayer
			// should be a MovableInterpolateElevationModel
			ElevationModel em = pair.getSecond();
			if (em instanceof IMovableLayer) {
				((IMovableLayer) em).setMovement(null);
			}
		}

		layerName.remove(store.getPath());
		layerList.remove(store);
	}

	/**
	 * Look for the LayerStore in the "Data" node of the model root and then add the found object in the list of
	 * dependent layer stores.
	 *
	 * @param storeName Name of the LayerStore to add to the list of dependent stores.
	 */
	public void addLayerStore(String storeName) {

		class LayerStoreVisitor implements Consumer<TreeNode> {
			LayerStore found = null;

			@Override
			public void accept(TreeNode node) {
				if (node instanceof FileInfoNode) {
					IFileInfo store = ((FileInfoNode) node).getFileInfo();
					LayerStore ls = null;

					if (store instanceof LayerStore) {
						ls = (LayerStore) store;

						// First time the session is loaded,
						// $dependentLayersByPlate is null or empty
						if (StringUtils.equals(ls.getPath(), storeName)) {
							found = ls;
						}
					}
				}
			}
		}

		LayerStoreVisitor visitor = new LayerStoreVisitor();
		projectExplorerModel.getDataNode().traverseDown(visitor);

		if (visitor.found != null) {
			addLayerStore(visitor.found);
		}
	}

	/**
	 * if the parameter is a LayerStore, associate it to the tectonic definition. If the store was previously associated
	 * to any other TectonicDefinition, it is first unlinked froim this TectonicDefinition before it is associated to
	 * this one.
	 * <p>
	 * Once a LayerStore has been set as dependent on this TectonicDefinition, it is assigned the same TectonicMovement
	 * as the TectonicDefinition.
	 *
	 * @param store LayerStore to add as a dependent LayerStore.
	 */
	public void addLayerStore(final LayerStore store) {

		// First remove any existing link between another
		// TectonicDefinition and the LayerStore
		if (store != null) {
			for (TectonicDefinition td : TectonicDataset.getInstance().getPlates()) {
				if (td != this && td.getLayerStoreList().contains(store)) {
					td.removeLinkToLayerStore(store);
				}
			}

			if (!layerName.contains(store.getPath())) {

				boolean toAdd = false;

				for (Pair<Layer, ElevationModel> pair : store.getLayers()) {
					Layer layer = pair.getFirst();
					if (layer != null && layer instanceof IMovableLayer) {
						toAdd = true;
						break;
					}
				}

				if (toAdd) {
					// Then add this layer store into the internal list
					if (!layerList.contains(store)) {
						layerList.add(store);
					}
					if (!layerName.contains(store.getPath())) {
						layerName.add(store.getPath());
					}

					// Also associate the movement to the new dependent layer
					for (Pair<Layer, ElevationModel> pair : store.getLayers()) {

						Layer layer = pair.getFirst();
						if (layer != null && layer instanceof IMovableLayer) {

							((IMovableLayer) layer).setMovement(getPoleRotationLayer().getMovement());

							// terrain layer could be remanent
							if (layer instanceof ShaderElevationLayer && ((ShaderElevationLayer) layer).isInt24()) {
								remanentLayers.add((ShaderElevationLayer) layer);
								remanentToRender.put((ShaderElevationLayer) layer,
										new ArrayList<TectonicRemanentLayer>());

								createRemanentLayers((ShaderElevationLayer) layer);

								// check if remanent mode
								if (player.isRemanent()) {
									LayerList layerList = Viewer3D.getModel().getLayers();

									// Displays or hides remanent layers
									List<TectonicRemanentLayer> remanent = remanentToRender.get(layer);

									// check if the first remanent layer is in
									// the layerlist
									if (!layerList.contains(remanent.get(0))) {

										// if not, add it before the parent
										// layer
										int indexParent = 0;
										for (Layer p : layerList) {
											if (p == layer) {
												break;
											}
											indexParent++;
										}
										for (TectonicRemanentLayer lr : remanent) {
											// add just before parent layer
											// for superposition purpose
											Viewer3D.getModel().getLayers().add(indexParent, lr);
										}

									}

									for (TectonicRemanentLayer lr : remanent) {
										lr.setEnabled(player.isRemanent());
									}

									// hides (remove) remanent parent layers
									layerList.remove(layer);
								}
							}
						} else if (layer != null && layer instanceof RenderableLayer) {
							for (Renderable r : ((RenderableLayer) layer).getRenderables()) {
								if (r instanceof CotSegmentRenderer) {
									((CotSegmentRenderer) r).setMovement(getPoleRotationLayer().getMovement());
								}
							}
						}

						// elevation could move when defined as
						// IMovableLayer
						// should be a MovableInterpolateElevationModel
						ElevationModel em = pair.getSecond();
						if (em != null && em instanceof IMovableLayer) {
							((IMovableLayer) em).setMovement(getPoleRotationLayer().getMovement());
						}
					}

					ContextInitializer.getInContext(IEventBroker.class).send(PlacaEventTopics.TOPIC_LAYER_STORE_ADDED,
							new LayerStoreAddedEvent(this, store));
				}
			}
		}
	}

	public void resetLayerStoreList() {
		layerList.clear();
		layerName.clear();
	}

	public String getRotationPoleName() {
		return rotationPoleName;
	}

	public TectonicModel getTectonicModel() {
		return tectonicModel;
	}

	public TectonicPlateModel getTectonicPlateModel() {
		return tectonicPlateModel;
	}

	@Override
	public void dispose() {

		// On ne dispose QUE ce que l'on a créé dans cette instance. Le reste
		// n'est pas de notre responsabilité.
		timer.stop();
		tectonicPlateModel.getPropertyChangeSupport().removePropertyChangeListener(ageIntervalListener);

		remanentToRender.values().stream().flatMap(List::stream).forEach(IGeographicViewService.grab()::removeLayer);
		remanentToRender.clear();
		remanentLayers.clear();
		resetLayerStoreList();

		TectonicMovement movement = getPoleRotationLayer().getMovement();
		ISectorMovementProcessing referentialMovement = movement.getReferentialMovement();
		if (movement == referentialMovement) {
			TectonicMovement.setReferentialMovement(null);
		}
	}

}
