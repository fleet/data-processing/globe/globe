package fr.ifremer.globe.placa3d.tectonic;

import gov.nasa.worldwind.geom.LatLon;

/**
 * @author mgaro
 * 
 */
public class Rotation {

	/**
	 * intermediate position
	 */
	private LatLon latLon;
	/**
	 * intermediate angle in degree
	 */
	private double angle;
	/**
	 * begin date
	 */
	private double begin;
	/**
	 * end date
	 */
	private double end;

	/**
	 * @param latLon
	 *            Center of rotation.
	 * @param angle
	 *            Angle of rotation, in degrees.
	 * @param beginDate
	 *            Begin date.
	 * @param endDate
	 *            End date.
	 */
	public Rotation(LatLon latLon, double angle, double beginDate, double endDate) {
		super();
		this.latLon = latLon;
		this.angle = angle;
		this.begin = beginDate;
		this.end = endDate;
	}

	/**
	 * @return Center of rotation.
	 */
	public LatLon getLatLon() {
		return latLon;
	}

	/**
	 * @return Angle of rotation, in degrees.
	 */
	public double getAngle() {
		return angle;
	}

	/**
	 * @return Begin date.
	 */
	public double getBegin() {
		return begin;
	}

	/**
	 * @return End date.
	 */
	public double getEnd() {
		return end;
	}

}
