package fr.ifremer.globe.placa3d.tectonic;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.ui.utils.Messages;

/***
 * Tectonic player selects the date of the tectonic plates
 *
 * @author MORVAN
 *
 */
public final class TectonicPlayerModel {

	/** Property name for playingSpeed change support. */
	public static final String PROPERTY_PLAYINGSPEED = "playingSpeed";

	/** Property name for reversePlaying change support. */
	public static final String PROPERTY_REVERSEPLAYING = "reversePlaying";

	/** Property name for playing change support. */
	public static final String PROPERTY_PLAYING = "playing";

	/** Property name for loop change support. */
	public static final String PROPERTY_REMANENT = "remanent";

	/** Property name for imageIndex change support. */
	public static final String PROPERTY_DATEINDEX = "dateIndex";

	/** Property name for minImageIndex change support. */
	public static final String PROPERTY_MINDATEINDEX = "minDateIndex";

	/** Property name for maxImageIndex change support. */
	public static final String PROPERTY_MAXDATEINDEX = "maxDateIndex";

	/** Property name for offset change support. */
	public static final String PROPERTY_OFFSET = "offset";

	/** Property name for change referential support. */
	public static final String PROPERTY_CHANGEREFERENTIAL = "changeReferential";

	/** Property name for change offset mode. */
	public static final String PROPERTY_OFFSETMODE = "offsetMode";

	/** Property name for change offset values. */
	public static final String PROPERTY_OFFSETVALUES = "offsetValues";

	/**
	 * Enum values for the 'offsetMode' property.
	 */
	public static enum OffsetMode {
		/** No offset. */
		OFF,
		/**
		 * Offset is automatically computed when the current date index changes.
		 */
		AUTO,
		/** Offset is fixed and applied when the current date index changes. */
		MANUAL_VALUE,
		/**
		 * Offset is read from a file and applied when the current date index changes.
		 */
		MANUAL_FILE;
	}

	/**
	 * Constructor.
	 */
	public TectonicPlayerModel() {
		super();
		setMinDateIndex(Activator.getPreferences().getMinimumAge().getValue());
		setMaxDateIndex(Activator.getPreferences().getMaximumAge().getValue());
		setDateIndex(0);
	}

	/** Utility instance for property change support. */
	protected transient PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	/**
	 * Adds a change listener to this object.
	 *
	 * @param listener the listener to add
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Removes a change listener from this object.
	 *
	 * @param listener the listener to remove
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	/**
	 * For Serializable and Externalizable classes, the readResolve method allows a class to replace/resolve the object
	 * read from the stream before it is returned to the caller. By implementing the readResolve method, a class can
	 * directly control the types and instances of its own instances being deserialized.
	 */
	private Object readResolve() {
		pcs = new PropertyChangeSupport(this);
		// Start of user code PlayerBean readResolve

		// End of user code
		return this;
	}

	/** Current min date index. */
	protected int minDateIndex = 0;
	/** Current max date index. */
	protected int maxDateIndex = 0;
	/** Current date index. */
	protected int dateIndex = 0;

	/** The playingSpeed attribute. */
	protected float playingSpeed = 1.0f;

	/** The reversePlaying attribute. */
	protected boolean reversePlaying = false;

	/** True if this player is currently playing. */
	private boolean playing = false;

	/** True if this player should display remanent plates. */
	protected boolean remanent = false;

	private boolean referentiel = false;

	/** Offset computation mode. */
	protected OffsetMode offsetMode = OffsetMode.OFF;

	/**
	 * Value fixed by the PoleComputeComposite when the "Manual" mode is chosen (this is the value in the spinner).
	 */
	protected double offsetFixedValue;

	/** File containing Offset values vs. Dates. */
	protected File offsetMappingFile;
	protected double[] offsetMappingFileValues; // values read from the file.

	/**
	 * Computed offset value after the dateIndex has changed.
	 *
	 * @see #computeOffset(double)
	 */
	protected double offsetValue;

	/**
	 * In player mode, selects the current date according to the min/max period
	 */
	public void jumpToNextDate() {
		// Start of user code for operation jumpToNextImage

		// int delta = Math.max(1, (int) playingSpeed);
		int delta = 1;

		int newIdx = dateIndex;
		if (reversePlaying) {
			newIdx -= delta;
			if (newIdx < minDateIndex) {
				newIdx = minDateIndex;
				setReversePlaying(false);
			}
		} else {
			newIdx += delta;
			if (newIdx > maxDateIndex) {
				newIdx = maxDateIndex;
				setPlaying(false);
			}
		}
		setDateIndex(newIdx);

		// End of user code
	}

	/**
	 * Compute the elevation offset for the specified age.
	 *
	 * @param dateIndex Age (in million years starting from today).
	 */
	protected void computeOffset(double dateIndex) {

		Pair<Boolean, Double> oldValue = new Pair<>(offsetMode == OffsetMode.AUTO, offsetValue);

		switch (offsetMode) {
		case MANUAL_VALUE:
			this.offsetValue = this.offsetFixedValue;
			break;

		case MANUAL_FILE:
			this.offsetValue = getOffsetValueFromFile(dateIndex);
			break;

		case AUTO:
		case OFF:
		default:
			this.offsetValue = 0.0;
			break;
		}

		pcs.firePropertyChange(PROPERTY_OFFSET, oldValue, this.offsetValue);
	}

	protected double getOffsetValueFromFile(double dateIndex) {
		if (dateIndex >= 0 && dateIndex <= Activator.getPreferences().getMaximumAge().getValue()
				&& this.offsetMappingFileValues != null) {
			return this.offsetMappingFileValues[(int) dateIndex];
		}
		return 0;
	}

	public int getApproxElevationOffset() {
		return (int) this.offsetValue;
	}

	/**
	 * The current image index.
	 */
	public void setDateIndex(int imageIndex) {
		if (this.dateIndex != imageIndex) {
			computeOffset(imageIndex);
			pcs.firePropertyChange(PROPERTY_DATEINDEX, this.dateIndex, this.dateIndex = imageIndex);
		}
	}

	/**
	 * The playingSpeed attribute.
	 */
	public float getPlayingSpeed() {
		return this.playingSpeed;
	}

	/**
	 * The playingSpeed attribute.
	 */
	public void setPlayingSpeed(float playingSpeed) {
		if (this.playingSpeed != playingSpeed) {
			pcs.firePropertyChange(PROPERTY_PLAYINGSPEED, this.playingSpeed, this.playingSpeed = playingSpeed);
		}
	}

	/**
	 * The reversePlaying attribute.
	 */
	public boolean isReversePlaying() {
		return this.reversePlaying;
	}

	/**
	 * The reversePlaying attribute.
	 */
	public void setReversePlaying(boolean reversePlaying) {
		if (this.reversePlaying != reversePlaying) {
			// Start of user code custom setter for reversePlaying
			if (reversePlaying == true) {
				setPlaying(false);
			}
			// End of user code
			pcs.firePropertyChange(PROPERTY_REVERSEPLAYING, this.reversePlaying, this.reversePlaying = reversePlaying);
		}
	}

	/**
	 * True if this player is currently playing.
	 */
	public boolean isPlaying() {
		return this.playing;
	}

	/**
	 * True if this player is currently playing.
	 */
	public void setPlaying(boolean playing) {
		if (this.playing != playing) {
			// Start of user code custom setter for playing
			if (playing == true) {
				setReversePlaying(false);
			}
			// End of user code
			pcs.firePropertyChange(PROPERTY_PLAYING, this.playing, this.playing = playing);
		}
	}

	public void setMinDateIndex(int minDateIndex) {
		pcs.firePropertyChange(PROPERTY_MINDATEINDEX, this.minDateIndex, this.minDateIndex = minDateIndex);

	}

	public void setMaxDateIndex(int maxDateIndex) {
		pcs.firePropertyChange(PROPERTY_MAXDATEINDEX, this.maxDateIndex, this.maxDateIndex = maxDateIndex);
	}

	/**
	 * @return min date
	 */
	public int getMinDateIndex() {
		return minDateIndex;
	}

	/**
	 * @return maximum date
	 */
	public int getMaxDateIndex() {
		return maxDateIndex;
	}

	/**
	 * @return current plate position date
	 */
	public int getDateIndex() {
		return dateIndex;
	}

	/**
	 * displays remanent plates?
	 *
	 * @param remanent
	 */
	public void setRemanent(boolean remanent) {
		this.remanent = remanent;
	}

	/**
	 *
	 * @return displays remanent plates?
	 */
	public boolean isRemanent() {
		return this.remanent;
	}

	/**
	 * does the associated plate is the reference? In this case the camera move along the plate's movement
	 *
	 * @param referentiel
	 */
	public void setReferentiel(boolean referentiel) {
		if (this.referentiel != referentiel) {
			pcs.firePropertyChange(PROPERTY_CHANGEREFERENTIAL, this.referentiel, this.referentiel = referentiel);
		}
	}

	/**
	 * does the associated plate is the reference? In this case the camera move along the plate's movement
	 *
	 * @return isReferentiel
	 */
	public boolean isReferentiel() {
		return this.referentiel;
	}

	/**
	 * @return The offset mode.
	 */
	public OffsetMode getOffsetMode() {
		return this.offsetMode;
	}

	/**
	 * Updates the elevation offset computation mode and set the corresponding mode's parameters.
	 *
	 * @param mode Elevation offset computation mode.
	 * @param offsetValue A fixed elevation offset (required only for OffsetMode.MANUAL_FILE).
	 * @param offsetMappingFile [Optionnal] file defining offset values vs date index.
	 */
	public void setOffsetMode(OffsetMode mode, double offsetValue, File offsetMappingFile) {

		boolean triggerEvent = false;
		OffsetMode oldMode = this.offsetMode;

		if (mode != null && mode != this.offsetMode) {
			this.offsetMode = mode;
			triggerEvent = true;
		}

		this.offsetFixedValue = offsetValue;

		if (mode == OffsetMode.MANUAL_FILE) {
			if (offsetMappingFile != null) {
				this.offsetMappingFile = offsetMappingFile;
				AgeOffsetFileLoader subsidenceFileLoader = new AgeOffsetFileLoader();
				try {
					this.offsetMappingFileValues = subsidenceFileLoader.loadOffsetFile(offsetMappingFile);
				} catch (IOException e) {
					Messages.openErrorMessage("Unable to read file " + offsetMappingFile.getAbsolutePath(), e);
				}
			}
		}

		if (triggerEvent) {
			pcs.firePropertyChange(PROPERTY_OFFSETMODE, oldMode, this.offsetMode);
			if (mode == OffsetMode.MANUAL_FILE) {
				pcs.firePropertyChange(PROPERTY_OFFSETVALUES, null, this.offsetMappingFileValues);
			}
		}

		computeOffset(this.dateIndex);
	}

	public File getOffsetFile() {
		return this.offsetMappingFile;
	}

}
