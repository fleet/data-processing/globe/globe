package fr.ifremer.globe.placa3d.tectonic;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;

/**
 * The tectonic configuration (poles of rotation, offsets).
 * 
 * @author pmahoudo
 * 
 */
public class TectonicConfiguration {

	/** True if all plates are rotated together, false else. */
	private boolean allPlates = false;

	/** True if all plates' offsets are changed together, false else. */
	private boolean isSynchronizeOffsets = false;

	/**
	 * Display mode :
	 * 
	 * 0 : display selected rotation poles
	 * 
	 * 1 : display all rotation poles
	 * 
	 * 2 : hide all rotation poles
	 */
	private int displayPoleMode = 0;

	/** True if total poles are displaying. */
	private boolean displayTotPoles = true;

	/** True if intermediate poles are displaying. */
	private boolean displayIntPoles = false;

	/** The latlon origin for the diplay of int poles. */
	private LatLon latlon = new LatLon(new LatLon(Angle.fromDegrees(Double.MIN_VALUE), Angle.fromDegrees(Double.MIN_VALUE)));

	/**
	 * initialisation of the class when deserializing
	 * */
	private Object readResolve() {
		return this;
	}

	public boolean isAllPlates() {
		return allPlates;
	}

	public void setAllPlates(boolean allPlates) {
		this.allPlates = allPlates;
	}

	public boolean isSynchronizeOffsets() {
		return isSynchronizeOffsets;
	}

	public void setSynchronizeOffsets(boolean isSynchronizeOffsets) {
		this.isSynchronizeOffsets = isSynchronizeOffsets;
	}

	public int getDisplayPoleMode() {
		return displayPoleMode;
	}

	public void setDisplayPoleMode(int displayPoleMode) {
		this.displayPoleMode = displayPoleMode;
	}

	public boolean isDisplayTotPoles() {
		return displayTotPoles;
	}

	public void setDisplayTotPoles(boolean displayTotPoles) {
		this.displayTotPoles = displayTotPoles;
	}

	public boolean isDisplayIntPoles() {
		return displayIntPoles;
	}

	public void setDisplayIntPoles(boolean displayIntPoles) {
		this.displayIntPoles = displayIntPoles;
	}

	public LatLon getLatlon() {
		return latlon;
	}

	public void setLatlon(LatLon latlon) {
		this.latlon = latlon;
	}

}
