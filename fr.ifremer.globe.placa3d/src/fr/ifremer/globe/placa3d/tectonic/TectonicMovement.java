package fr.ifremer.globe.placa3d.tectonic;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.viewer3d.geom.MovableSector;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Sector;

/**
 * @author mgaro
 *
 */
public class TectonicMovement implements ISectorMovementProcessing {

	// date in million years from now
	private int dateIndex = Activator.getPreferences().getMinimumAge().getValue();
	private static TectonicMovement referentialMovement = null;

	String fileName = null;

	/** Intermediate Poles of rotation. */
	private PoleRotations intPoleRotations;
	/** Total Poles of rotation. */
	private PoleRotations totalPoleRotations;

	/** Offset matrix used when computing pole. */
	private Matrix offsetMatrix;

	/** Comments of polerotationFile */
	private String comments;

	protected PropertyChangeSupport pcs;

	public TectonicMovement(String fileName) {
		super();
		this.pcs = new PropertyChangeSupport(this);
		this.intPoleRotations = new PoleRotations();
		this.totalPoleRotations = new PoleRotations();
		this.fileName = fileName;
	}

	public void addRotation(Rotation rotation) {
		this.intPoleRotations.addRotation(rotation);
	}

	public void addTotalRotation(Rotation rotation) {
		this.totalPoleRotations.addRotation(rotation);
	}

	@Override
	public Sector move(Sector s) {
		if (hasMovement()) {
			return rotate(s, false, dateIndex);
		} else {
			return s;
		}
	}

	@Override
	public LatLon move(LatLon latLon) {
		if (hasMovement()) {
			return rotate(latLon, false, dateIndex);
		} else {
			return latLon;
		}
	}

	@Override
	public LatLon move(Angle latitude, Angle longitude) {
		LatLon latLon = LatLon.fromDegrees(latitude.getDegrees(), longitude.getDegrees());
		return move(latLon);
	}

	@Override
	public Sector move(Sector s, int indexDate) {
		if (hasMovement()) {
			return rotate(s, false, indexDate);
		} else {
			return s;
		}
	}

	@Override
	public LatLon move(LatLon latLon, int indexDate) {
		return move(latLon);
	}

	@Override
	public Sector moveInv(Sector s) {
		if (hasMovement()) {
			return rotate(s, true, dateIndex);
		} else {
			return s;
		}
	}

	@Override
	public LatLon moveInv(LatLon latLon) {
		if (hasMovement()) {
			return rotate(latLon, true, dateIndex);
		} else {
			return latLon;
		}
	}

	@Override
	public Sector moveInv(Sector s, int indexDate) {
		if (hasMovement()) {
			return rotate(s, true, indexDate);
		} else {
			return s;
		}
	}

	@Override
	public LatLon moveInv(LatLon latLon, int indexDate) {
		if (hasMovement()) {
			return rotate(latLon, true, indexDate);
		} else {
			return latLon;
		}
	}

	@Override
	public Matrix getMovement() {
		return getMovement(dateIndex);
	}

	@Override
	public Matrix getMovement(int indexDate) {
		Matrix movement = Matrix.IDENTITY;

		// no movement defined
		if (!hasMovement()) {
			return movement;
		}

		// Apply an offset matrix if exists (used when "Pole Compute")
		if (offsetMatrix != null) {
			movement = movement.multiply(offsetMatrix);
		}

		List<Matrix> rotations = loadRotationMatrix(indexDate);

		// multiplication must be done in reverse due to none commutativity
		for (int ii = 0; ii < rotations.size(); ii++) {
			movement = movement.multiply(rotations.get(rotations.size() - ii - 1));
		}

		return movement;
	}

	@Override
	public Matrix getMovementInv() {
		return getMovementInv(dateIndex);
	}

	@Override
	public Matrix getMovementInv(int indexDate) {
		Matrix movement = Matrix.IDENTITY;

		// no movement defined
		if (!hasMovement()) {
			return movement;
		}

		List<Matrix> rotations = loadRotationMatrix(indexDate);

		for (int ii = 0; ii < rotations.size(); ii++) {
			movement = movement.multiply(rotations.get(ii).getTranspose());
		}

		// Apply an offset matrix if exists (used when "Pole Compute")
		if (offsetMatrix != null) {
			movement = movement.multiply(offsetMatrix.getTranspose());
		}

		// The inverse movement is the transpose matrix
		return movement;
	}

	@Override
	public int getDateIndex() {
		return dateIndex;
	}

	@Override
	public boolean hasMovement() {
		return intPoleRotations.isNotEmpty();
	}

	private List<Matrix> loadRotationMatrix(int indexDate) {
		double dateComputed = 0;
		int index = 0;

		List<Matrix> rotations = new ArrayList<>();
		for (Double date : intPoleRotations.getDates()) {
			if (index == intPoleRotations.getDates().size() - 1
					|| indexDate < intPoleRotations.getDates().get(index + 1)) {
				break;
			}
			Matrix R = intPoleRotations.getMatrix((int) date.doubleValue() + 1);
			rotations.add(R);

			dateComputed = intPoleRotations.getDates().get(index + 1).doubleValue();
			index++;
		}

		// case of final part anomaly

		// case ano0 not defined
		// case end computation defined by anomaly end date
		if (dateComputed >= intPoleRotations.getDates().get(0).doubleValue() && (indexDate - dateComputed) > 0) {
			double period = indexDate - dateComputed;
			if (indexDate > intPoleRotations.getEndDates().get(intPoleRotations.getEndDates().size() - 1)
					.doubleValue()) {
				period = intPoleRotations.getEndDates().get(intPoleRotations.getEndDates().size() - 1).doubleValue()
						- dateComputed;
			}

			Matrix R = intPoleRotations.getMatrix(index, period);
			rotations.add(R);
		}

		return rotations;
	}

	private Sector rotate(Sector st, boolean inversAngle, int indexDate) {

		Sector borne = st;

		LatLon latlonSW = rotate(borne.getMinLatitude(), borne.getMinLongitude(), inversAngle, indexDate);
		LatLon latlonNE = rotate(borne.getMaxLatitude(), borne.getMaxLongitude(), inversAngle, indexDate);

		// // case no movement
		// if (latlonSW.getLatitude().getDegrees() == borne.getMinLatitude().getDegrees() &&
		// latlonSW.getLongitude().getDegrees() == borne.getMinLongitude().getDegrees()
		// && latlonNE.getLatitude().getDegrees() == borne.getMaxLatitude().getDegrees() &&
		// latlonNE.getLongitude().getDegrees() == borne.getMaxLongitude().getDegrees()) {
		// return borne;
		// }

		LatLon latlonSE = rotate(borne.getMinLatitude(), borne.getMaxLongitude(), inversAngle, indexDate);
		LatLon latlonNW = rotate(borne.getMaxLatitude(), borne.getMinLongitude(), inversAngle, indexDate);

		double sin = Math.sin(latlonSW.getLatitude().radians);
		double scale = (1 + sin * sin);
		MovableSector sector = new MovableSector(latlonSW, latlonSE, latlonNW, latlonNE);

		// computes width, height and heading
		double Hw = 0;
		double Lw = 0;
		double h1 = 90 - TectonicUtils.computeHeading(latlonSW, latlonSE);
		// we compute the width and heading with the equator's closer latitude
		if (latlonSE.getLatitude().getDegrees() > 0) {
			// case north hemisphere
			Hw = latlonSE.getLatitude().getDegrees() - latlonSW.getLatitude().getDegrees();
			Lw = latlonSE.getLongitude().getDegrees() - latlonSW.getLongitude().getDegrees();
			h1 = 90 - TectonicUtils.computeHeading(latlonSW, latlonSE);
		} else {
			// case south hemisphere
			Hw = latlonNE.getLatitude().getDegrees() - latlonNW.getLatitude().getDegrees();
			Lw = latlonNE.getLongitude().getDegrees() - latlonNW.getLongitude().getDegrees();
			h1 = 90 - TectonicUtils.computeHeading(latlonNW, latlonNE);
		}

		double h2 = Math.toDegrees(Math.atan(Hw / Lw)) * scale;
		sector.setHeading(h2);

		double Hh = latlonNW.getLatitude().getDegrees() - latlonSW.getLatitude().getDegrees();
		double Lh = latlonNW.getLongitude().getDegrees() - latlonSW.getLongitude().getDegrees();

		sector.setWidth(Math.sqrt(Lw * Lw + Hw * Hw));
		// 1.01 trick to hide tiles borders
		// must be removed in a later release when computation will be more
		// accurate
		sector.setHeight(Math.sqrt(Lh * Lh + Hh * Hh) /* 1.01 */);

		LatLon parallelSector = computeRotation(latlonSW.getLatitude().getDegrees(),
				latlonSW.getLongitude().getDegrees(), -h1, latlonNE.getLatitude().getDegrees(),
				latlonNE.getLongitude().getDegrees(), inversAngle);

		double width = Math.abs(parallelSector.getLongitude().getDegrees() - latlonSW.getLongitude().getDegrees());
		double height = Math.abs(parallelSector.getLatitude().getDegrees() - latlonSW.getLatitude().getDegrees());

		// special to reconstruct antarctic
		// TODO find a better way to compute width at poles
		if (width > sector.getWidth()) {
			sector.setWidth(width);
		}

		if (10 * height < sector.getHeight()) {
			sector.setHeight(height);
		}

		return sector;
	}

	public LatLon rotate(LatLon latLon, boolean inversAngle, int indexDate) {
		return computeRotation(latLon, inversAngle, indexDate, false);
	}

	public LatLon rotate(Angle latitude, Angle longitude, boolean inversAngle, int indexDate) {
		return computeRotation(new LatLon(latitude, longitude), inversAngle, indexDate, false);
	}

	private LatLon computeRotation(LatLon latlon, boolean inversAngle, int indexDate,
			boolean isinverseMovementForReferential) {
		// case no rotation
		// if (indexDate == 0) {
		// return latlon;
		// }

		// geocentrique
		double[] xyz = TectonicUtils.ptGeoGtoGeoC(latlon);

		// matrix are not commutative we need to perform the inverseAngle case
		// in reverse
		if (inversAngle) {
			xyz = TectonicUtils.performesRotation(xyz, getMovementInv(indexDate), false);
		} else {
			xyz = TectonicUtils.performesRotation(xyz, getMovement(indexDate), inversAngle);
		}

		// geodetic
		if (referentialMovement == null || isinverseMovementForReferential) {
			return TectonicUtils.ptGeoCtoGeoG(xyz);
		} else {
			// apply the inverse movement of the referential plate
			// referential plate is static others move
			return referentialMovement.computeRotation(TectonicUtils.ptGeoCtoGeoG(xyz), !inversAngle, indexDate, true);
		}
	}

	/***
	 *
	 * @param centerLatitude
	 * @param centerLongitude
	 * @param angle
	 * @param latitude
	 * @param longitude
	 * @param inverse
	 * @return the latlon rotated by angle centered at centerLatitude/centerLongitude
	 */
	private static LatLon computeRotation(double centerLatitude, double centerLongitude, double angle, double latitude,
			double longitude, boolean inverse) {
		Matrix R = TectonicUtils.computeMatrix(centerLatitude, centerLongitude, angle);

		// geocentrique
		double[] xyz = TectonicUtils.ptGeoGtoGeoC(LatLon.fromDegrees(latitude, longitude));

		// performs rotation
		xyz = TectonicUtils.performesRotation(xyz, R, inverse);

		// geodetic
		return TectonicUtils.ptGeoCtoGeoG(xyz);
	}

	@Override
	public String toString() {
		return "TectonicMovement [poleRotations=" + intPoleRotations + "]";
	}

	public String getFileName() {
		return fileName;
	}

	// ////////////////////////////////////////////////////////////////////////////////////
	// TODO A REVOIR

	public static void setReferentialMovement(TectonicMovement movement) {
		referentialMovement = movement;
	}

	@Override
	public ISectorMovementProcessing getReferentialMovement() {
		return referentialMovement;
	}

	/***
	 * set current date
	 *
	 * @param newIndex
	 */
	public void setDateIndex(int newIndex) {
		if (newIndex == dateIndex) {
			return;
		}

		// set the new movement
		pcs.firePropertyChange(PROPERTY_DATE_INDEX, dateIndex, dateIndex = newIndex);
	}

	// ////////////////////////////////////////////////////////////////////////////////////

	public void setPoleRotation(PoleRotations rotations) {
		if (rotations != null) {
			this.intPoleRotations = rotations;
		}
	}

	public PoleRotations getPoleRotation() {
		return intPoleRotations;
	}

	public PoleRotations getTotalPoleRotation() {
		return totalPoleRotations;
	}

	public List<Double> getDates() {
		if (intPoleRotations != null) {
			return intPoleRotations.getDates();
		} else {
			return null;
		}
	}

	public List<Double> getEndDates() {
		if (intPoleRotations != null) {
			return intPoleRotations.getEndDates();
		} else {
			return null;
		}
	}

	public List<String> getSDates() {
		if (intPoleRotations != null) {
			return intPoleRotations.getSDates();
		} else {
			return null;
		}
	}

	public List<String> getSEndDates() {
		if (intPoleRotations != null) {
			return intPoleRotations.getSEndDates();
		} else {
			return null;
		}
	}

	public List<Double> getLongitudes() {
		if (intPoleRotations != null) {
			return intPoleRotations.getLongitudes();
		} else {
			return null;
		}
	}

	public List<Double> getLatitudes() {
		if (intPoleRotations != null) {
			return intPoleRotations.getLatitudes();
		} else {
			return null;
		}
	}

	public List<Double> getAngles() {
		if (intPoleRotations != null) {
			return intPoleRotations.getAngles();
		} else {
			return null;
		}
	}

	public Matrix getOffsetMatrix() {
		return offsetMatrix;
	}

	public void setOffsetMatrix(Matrix offsetMatrix) {
		this.offsetMatrix = offsetMatrix;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

}
