package fr.ifremer.globe.placa3d.tectonic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jakarta.inject.Inject;

import fr.ifremer.globe.placa3d.composite.PoleRotationComposite;
import fr.ifremer.globe.placa3d.layers.PoleRotationLayer;
import fr.ifremer.globe.placa3d.session.Placa3dSession;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;

/**
 * Set of TectonicDefinitions.
 */
public final class TectonicDataset {

	/** Singleton. */
	private static TectonicDataset instance;

	/** Tectonic plates. */
	private List<TectonicDefinition> plates;

	@Inject
	private Placa3dSession placa3dSession;

	/**
	 * Constructor.
	 */
	private TectonicDataset() {
		readResolve();
	}

	/**
	 * initialisation of the class when deserializing
	 */
	private Object readResolve() {
		if (instance == null) {
			instance = this;
			plates = new ArrayList<>();
		}
		return instance;
	}

	public static synchronized TectonicDataset getInstance() {
		if (instance == null) {
			instance = new TectonicDataset();
		}
		return instance;
	}

	public List<TectonicDefinition> getPlates() {
		return plates;
	}

	public void addTectonicDefinition(TectonicDefinition definition) {

		plates.add(definition);
		definition.postSessionRead(placa3dSession);

		// Update to display poles in the right mode (total or intermediate)
		updatePoleConfiguration();
	}

	/** Event for new data import. */
	public synchronized void onNodeDelete(List<IWWLayer> layers) {
		for (IWWLayer layer : layers) {
			if (layer instanceof PoleRotationLayer) {
				PoleRotationLayer deletednode = (PoleRotationLayer) layer;
				for (Iterator<TectonicDefinition> iterator = plates.iterator(); iterator.hasNext();) {
					TectonicDefinition definition = iterator.next();
					PoleRotationLayer rotationLayer = definition.getPoleRotationLayer();
					if (deletednode.equals(rotationLayer)) {
						iterator.remove();
						definition.dispose();
					}
				}
			}
		}
	}

	public TectonicConfiguration getConfiguration() {
		return placa3dSession.getTectonicConfiguration();
	}

	/**
	 * Update the configuration of poles roation for each poleRotationLayer.
	 */
	public static void updatePoleConfiguration() {
		// ALL PLATE
		List<PoleRotationComposite> composites = PoleRotationComposite.getPlayersList();
		if (instance.getConfiguration().isAllPlates()) {
			// case others composite not already created
			for (TectonicDefinition definition : TectonicDataset.getInstance().getPlates()) {
				if (definition != null) {
					// synchronize layers
					definition.getPlayer().setRemanent(false);
					definition.getPlayer().setPlayingSpeed(PoleRotationComposite.getLastSpeed());
				}
			}
		}

		// synchronize layers
		if (instance.getConfiguration().isSynchronizeOffsets()) {
			for (PoleRotationComposite composite : composites) {
				// Offset mode must be synchronized
				composite.setSlaveOffsetMode();
			}
		}

		// DISPLAY TOTAL POLES
		if (instance.getConfiguration().isDisplayTotPoles()) {
			for (TectonicDefinition t : TectonicDataset.getInstance().getPlates()) {
				if (t.getPoleRotationLayer() != null) {
					t.getPoleRotationLayer().displayTotalPoles(true);
				}
			}
		}

		// DISPLAY INTERMEDIATE POLES
		if (instance.getConfiguration().isDisplayIntPoles()) {
			for (TectonicDefinition t : TectonicDataset.getInstance().getPlates()) {
				t.getPoleRotationLayer().setLatlonOri(instance.getConfiguration().getLatlon());
				t.getPoleRotationLayer().computeLineForIntPoles();

				t.getPoleRotationLayer().displayIntermediatePoles(true);
			}
		}

	}

	public void postSessionRead(Placa3dSession result) {
		// all data was read, we need to reconstruct all tectonicdefinitions
		// that are partially build
		for (TectonicDefinition def : plates) {
			def.postSessionRead(result);
		}
	}

	public void preSessionWrite(Placa3dSession placa3dSession) {
		for (TectonicDefinition def : plates) {
			def.preSessionWrite(placa3dSession);
		}
	}

	/**
	 * @param fileLayerStoreModel the {@link #fileLayerStoreModel} to set
	 */
	@Inject
	public void fitFileLayerStoreModel(WWFileLayerStoreModel fileLayerStoreModel) {
		fileLayerStoreModel.addListener(this::onFileLayerStoreEvent);
	}

	protected void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		if (event.getState() == FileLayerStoreState.REMOVED) {
			onNodeDelete(event.getFileLayerStore().getLayers());
		}
	}

}
