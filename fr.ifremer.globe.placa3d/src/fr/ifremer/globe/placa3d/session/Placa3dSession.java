package fr.ifremer.globe.placa3d.session;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.ifremer.globe.placa3d.tectonic.TectonicConfiguration;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel.OffsetMode;
import fr.ifremer.globe.ui.workspace.DataSession;
import fr.ifremer.globe.ui.workspace.WorkspaceDirectory;

/**
 * Session parameters for Placa3D plugin. This class manages all tectonic configuration parameters of the user's
 * session. Objects having parameters to store must declare preSessionWrite() and postSessionRead() methods and these
 * methods will be called manually by this class when the SessionControler read or save the current user's session.
 * 
 * @author apside
 */
public class Placa3dSession {

	private static final String PLACA_SESSION_FILE = "Placasession.json";

	/** Session key in the {@link DataSession}. */
	public static final String KEY = "TectonicSessionParameters";

	/** Dependent layers by plate. */
	private Map<String, List<String>> dependentLayersByPlate;

	/** Elevation offset's mode by plate. */
	private Map<String, OffsetMode> offsetModeByPlate;

	/** Elevation offsets by plate. */
	private Map<String, File> offsetFileByPlate;

	/** Configuration of tectonic concerns. */
	private TectonicConfiguration tectonicConfiguration;

	/**
	 * Creates a new object and register it in the model root.
	 * <p>
	 * Note : this constructor is NOT called by XStream deserialization mechanism.
	 */
	public Placa3dSession() {
		readResolve();
	}

	/**
	 * Automatically called by XStream implicit deserialization.
	 * 
	 * @return this
	 */
	private Object readResolve() {
		if (dependentLayersByPlate == null) {
			dependentLayersByPlate = new HashMap<>();
		}
		if (offsetModeByPlate == null) {
			offsetModeByPlate = new HashMap<>();
		}
		if (offsetFileByPlate == null) {
			offsetFileByPlate = new HashMap<>();
		}
		if (tectonicConfiguration == null) {
			tectonicConfiguration = new TectonicConfiguration();
		}
		return this;
	}

	public void write() {
		TectonicDataset.getInstance().preSessionWrite(this);
		// Save
		try {
			File sessionFile = getSessionFile();
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			Files.writeString(sessionFile.toPath(), gson.toJson(this), StandardCharsets.UTF_8,
					StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static File getSessionFile() {
		return new File(WorkspaceDirectory.getInstance().getPath(), PLACA_SESSION_FILE);
	}

	/**
	 * Set the layers that depends on the specified tectonic plate. (See PoleRotationComposite "add" button).
	 * 
	 * @param definition Tectonic plate's definition.
	 * @param dependentLayers Layers that depends on the tetonic plate.
	 */
	public void setDependentLayersByPlate(TectonicDefinition definition, List<String> dependentLayers) {
		dependentLayersByPlate.put(definition.getRotationPoleName(), dependentLayers);
	}

	/**
	 * Get the name of layers that depends on the specified tectonic plate. (See PoleRotationComposite "add" button).
	 * 
	 * @param definition Tectonic plate's definition.
	 * @return dependentLayers Layers that depends on the tetonic plate.
	 */
	public List<String> getDependentLayersByPlate(TectonicDefinition definition) {
		List<String> result = dependentLayersByPlate.get(definition.getRotationPoleName());
		return result;
	}

	/**
	 * Set the elevation offset mode for this tectonic plate.
	 * 
	 * @param definition Tectonic plate's definition.
	 * @paral mode the offset mode for this tectonic plate.
	 */
	public void setOffsetModeByPlate(TectonicDefinition definition, OffsetMode mode) {
		offsetModeByPlate.put(definition.getRotationPoleName(), mode);
	}

	/**
	 * Get the elevation offset mode by tectonic plate.
	 * 
	 * @param definition Tectonic plate's definition.
	 * @return the offset mode for this tectonic plate.
	 */
	public OffsetMode getOffsetModeByPlate(TectonicDefinition definition) {
		return offsetModeByPlate.get(definition.getRotationPoleName());
	}

	/**
	 * Set the elevation offset file for this tectonic plate.
	 * 
	 * @param definition Tectonic plate's definition.
	 * @paral file the offset file for this tectonic plate.
	 */
	public void setOffsetFileByPlate(TectonicDefinition definition, File file) {
		offsetFileByPlate.put(definition.getRotationPoleName(), file);
	}

	/**
	 * Get the elevation offset file by tectonic plate.
	 * 
	 * @param definition Tectonic plate's definition.
	 * @return the offset file for this tectonic plate.
	 */
	public File getOffsetFileByPlate(TectonicDefinition definition) {
		return offsetFileByPlate.get(definition.getRotationPoleName());
	}

	public void setTectonicConfiguration(TectonicConfiguration tectonicConfiguration) {
		this.tectonicConfiguration = tectonicConfiguration;
	}

	public TectonicConfiguration getTectonicConfiguration() {
		return this.tectonicConfiguration;
	}

}
