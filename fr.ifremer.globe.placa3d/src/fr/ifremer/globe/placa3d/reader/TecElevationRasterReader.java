package fr.ifremer.globe.placa3d.reader;

import java.awt.Dimension;

import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.ByteBufferRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.geom.Sector;

/**
 * Class used by TiledElevationProducer to read a .tec file and create DataRaster (ByteBufferRaster).
 * 
 * @author pmahoudo
 */
public class TecElevationRasterReader extends TecRasterReader {

	public TecElevationRasterReader(MultiLayerTecImageElevationData tecFile, Area area) {
		super(tecFile, area);
	}

	@Override
	protected void initMetadata() {
		valid_min = file.getMinDepth();
		valid_max = file.getMaxDepth();
		depth_offset = 0.0; // TODO read on tecfile
		depth_scale = 1.0; // TODO read on tecfile

		// Check if tecfile contains elevation data
		Dimension depthRasterDimension = file.getDepthRasterDimension(area);
		if (depthRasterDimension != null) {
			lines = depthRasterDimension.height;
			columns = depthRasterDimension.width;
		}
		// If not, take the image data
		else {
			Dimension textureDimension = file.getTextureDimension(area);
			if (textureDimension != null) {
				// dimension of the texture array
				lines = textureDimension.height;
				columns = textureDimension.width;
			}
		}

		super.initMetadata();

		if (metadata != null) {
			metadata.setValue(AVKey.ELEVATION_MIN, valid_min);
			metadata.setValue(AVKey.ELEVATION_MAX, valid_max);
			metadata.setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
		}
	}

	/**
	 * compute DataRaster from data matrix
	 * 
	 * @return NWW DataRaster
	 */
	@Override
	protected DataRaster doRead() {
		AVList values = metadata;
		Sector sector = (Sector) values.getValue(AVKey.SECTOR);
		ByteBufferRaster raster = new ByteBufferRaster(columns, lines, sector, values);

		try {
			file.read();
			
			if (logger.isDebugEnabled()) {
				logger.debug("Read new raster " + columns + " x " + lines + ", for sector " + sector);
			}
			
			// Check if tecfile contains elevation data
			double[][] data = file.getDepthRaster(area);
			if (data != null) {
				// get the elevation data from the reader
				
				for (int iCol = 0; iCol < columns; iCol++) {
					for (int iRow = 0; iRow < lines; iRow++) {
						double val = data[iRow][iCol];
						// Set missing values
						if (val < valid_min || val > valid_max || val != val) {
							raster.setDoubleAtPosition(iRow, iCol, nww_missing_value);
						}
						// Set real data
						else {
							double value = val * depth_scale + depth_offset;
							raster.setDoubleAtPosition(iRow, iCol, value);
						}
					}
				}
			}
			// If not, take the texture data as grid
			else {
				// get the image data from the reader
				data = file.getTexture(area);
				double minDepth = file.getMinDepth();
				
				for (int iCol = 0; iCol < columns; iCol++) {
					for (int iRow = 0; iRow < lines; iRow++) {
						double val = data[iRow][iCol];
						// Set missing values
						if (val == TecGeneratorUtil.getMissingValue()) {
							raster.setDoubleAtPosition(iRow, iCol, nww_missing_value);
						} else {
							// Set offset elevation
							raster.setDoubleAtPosition(iRow, iCol, minDepth);
						}
						
					}
				}
			}
			
			raster.setValue(AVKey.MISSING_DATA_SIGNAL, nww_missing_value);
			
		} catch (Exception e) {
			logger.error("", e);
		}

		return raster;

	}

}
