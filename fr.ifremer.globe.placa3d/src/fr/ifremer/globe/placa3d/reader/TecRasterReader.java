package fr.ifremer.globe.placa3d.reader;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.AbstractDataRasterReader;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.formats.worldfile.WorldFile;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.ImageUtil;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;

/**
 * Class used by TiledImageProducer and TiledElevationProducer to read data of .tec file.
 *
 * @author D.riou
 */
public abstract class TecRasterReader extends AbstractDataRasterReader {

	protected Logger logger = LoggerFactory.getLogger(TecRasterReader.class);

	private static final String[] tecMimeTypes = { "image/tec", "image/mtec" };
	private static final String[] tecfSuffixes = { "tec", "mtec" };

	private String currentPath = null;
	protected AVList metadata = null;

	protected MultiLayerTecImageElevationData file;

	protected int lines = 0;
	protected int columns = 0;
	protected double depth_offset = 0.0;
	protected double depth_scale = 1.0;
	protected double valid_min = 0.0;
	protected double valid_max = 0.0;

	protected Area area;

	protected double nww_missing_value = -32768.0;

	/** Constructor */
	public TecRasterReader() {
		super(tecMimeTypes, tecfSuffixes);
		metadata = new AVListImpl();
	}

	public TecRasterReader(MultiLayerTecImageElevationData reader, Area area) {
		this();
		file = reader;
		this.area = area;
		initMetadata();
	}

	protected void initMetadata() {
		metadata = new AVListImpl();

		metadata.setValue(AVKey.FILE_NAME, file.getPathFile());
		// after we read all data, we have evrything as BIG_ENDIAN
		metadata.setValue(AVKey.BYTE_ORDER, AVKey.BIG_ENDIAN);
		metadata.setValue(AVKey.WIDTH, columns);
		metadata.setValue(AVKey.HEIGHT, lines);
		metadata.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
		// Sector
		metadata.setValue(AVKey.ORIGIN, LatLon.fromDegrees(file.getLatitudeMax(), file.getLongitudeMin(area)));

		Sector sector = null;

		sector = new Sector(Angle.fromDegrees(file.getLatitudeMin()), Angle.fromDegrees(file.getLatitudeMax()),
				Angle.fromDegrees(file.getLongitudeMin(area)), Angle.fromDegrees(file.getLongitudeMax(area)));

		metadata.setValue(AVKey.SECTOR, sector);
	}

	@Override
	protected boolean doCanRead(Object source, AVList params) {
		String path = WWIO.getSourcePath(source);
		if (path == null) {
			return false;
		}

		return true;
	}

	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws IOException {
		String path = WWIO.getSourcePath(source);
		if (path != currentPath) {
			currentPath = path;
		}
		if (path == null) {
			String message = Logging.getMessage("DataRaster.CannotRead", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		AVList metadata = new AVListImpl();
		if (null != params) {
			metadata.setValues(params);
		}

		DataRaster[] rasters = null;
		try {
			this.readMetadata(source, metadata);

			rasters = readDataRaster();

			if (null != rasters) {
				String[] keysToCopy = new String[] { AVKey.SECTOR };
				for (DataRaster raster : rasters) {
					WWUtil.copyValues(metadata, raster, keysToCopy, false);
				}
			}
		} catch (IOException e) {
			logger.warn("Error while trying to read raster data : " + e.getMessage());
		}
		return rasters;
	}

	@Override
	protected void doReadMetadata(Object source, AVList params) throws IOException {
		String path = WWIO.getSourcePath(source);

		if (path == null) {
			String message = Logging.getMessage("nullValue.PathIsNull", source);
			Logging.logger().severe(message);
			throw new java.io.IOException(message);
		}

		copyMetadataTo(params);

		if (params.hasKey(AVKey.WIDTH) && params.hasKey(AVKey.HEIGHT)) {
			int[] size = new int[2];

			size[0] = (Integer) params.getValue(AVKey.WIDTH);
			size[1] = (Integer) params.getValue(AVKey.HEIGHT);

			params.setValue(WorldFile.WORLD_FILE_IMAGE_SIZE, size);

			WorldFile.readWorldFiles(source, params);

			Object o = params.getValue(AVKey.SECTOR);
			if (o == null || !(o instanceof Sector)) {
				ImageUtil.calcBoundingBoxForUTM(params);
			}
		}
	}

	/**
	 * Call doRead method and store into table of DataRaster
	 *
	 * @return Table of NWW DataRaster
	 */
	protected DataRaster[] readDataRaster() throws IOException {
		DataRaster[] rasters = new DataRaster[1];
		rasters[0] = doRead();
		return rasters[0] != null ? rasters : null;
	}

	/**
	 * Compute DataRaster from data matrix
	 *
	 * @return NWW DataRaster
	 */
	protected abstract DataRaster doRead() throws IOException;

	/**
	 * Add AVList values to the metadata attribute
	 *
	 * @param values new values to add
	 * @return metadata completed
	 */
	public AVList copyMetadataTo(AVList values) {
		AVList list = this.getMetadata();
		if (null != values) {
			values.setValues(list);
		} else {
			values = list;
		}
		return values;
	}

	/**
	 * setter and getter methods.
	 */
	public AVList getMetadata() {
		AVList values = metadata;
		return null != values ? values.copy() : null;
	}

	public double getValid_min() {
		return valid_min;
	}

	public void setValid_min(double valid_min) {
		this.valid_min = valid_min;
	}

	public double getValid_max() {
		return valid_max;
	}

	public void setValid_max(double valid_max) {
		this.valid_max = valid_max;
	}

	/**
	 * @return the {@link #lines}
	 */
	public int getLines() {
		return lines;
	}

	/**
	 * @param lines the {@link #lines} to set
	 */
	public void setLines(int lines) {
		this.lines = lines;
		if (metadata != null) {
			metadata.setValue(AVKey.HEIGHT, lines);
		}
	}

	/**
	 * @return the {@link #columns}
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * @param columns the {@link #columns} to set
	 */
	public void setColumns(int columns) {
		this.columns = columns;
		if (metadata != null) {
			metadata.setValue(AVKey.WIDTH, columns);
		}
	}

}
