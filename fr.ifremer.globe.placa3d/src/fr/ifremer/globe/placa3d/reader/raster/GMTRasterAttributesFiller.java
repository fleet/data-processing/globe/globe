package fr.ifremer.globe.placa3d.reader.raster;

import java.nio.FloatBuffer;
import java.util.DoubleSummaryStatistics;
import java.util.List;

import org.gdal.gdal.Dataset;
import org.gdal.gdalconst.gdalconstConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.dataset.GdalDataset.BandFeatures;
import fr.ifremer.globe.gdal.dataset.GdalDataset.Resolution;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.viewer3d.data.reader.raster.RasterAttributes;
import gov.nasa.worldwind.geom.Angle;

/**
 * This class aim at filling the TerrainAttributes for a specified GRD file.
 * 
 */
public class GMTRasterAttributesFiller {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(GMTRasterAttributesFiller.class);

	/** All Attributes read in a file. */
	protected final RasterAttributes rasterAttributes;

	/**
	 * Constructor.
	 */
	public GMTRasterAttributesFiller(RasterAttributes rasterAttributes) {
		this.rasterAttributes = rasterAttributes;
	}

	/** Open the file and read all attributes. */
	public void fill(IFileService fileService, IRasterService rasterService) throws FileFormatException {
		rasterAttributes.setReadSuccessfully(false);
		IFileInfo fileInfo = fileService.getFileInfo(rasterAttributes.getFilename()).orElse(null);
		if (fileInfo != null && fileInfo.getContentType() == ContentType.GMT) {
			List<RasterInfo> rasters = rasterService.getRasterInfos(fileInfo);
			if (!rasters.isEmpty()) {
				RasterInfo rasterInfo = rasters.get(0); // Only one raster in GMT
				try (GdalDataset gdalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey())) {
					GdalDataset memDataset = reproject(gdalDataset);
					// Elevations supplied by Globe are positive up.
					// Placa expects elevations in positive down. So, elevations = elevations * -1
					var stats = new DoubleSummaryStatistics();
					memDataset.resample(1, (buffer, noDataValue) -> {
						FloatBuffer floatBuffer = buffer.asFloatBuffer();
						floatBuffer.rewind();
						while (floatBuffer.hasRemaining()) {
							floatBuffer.mark();
							float value = floatBuffer.get();
							if (Float.isFinite(value) && value != noDataValue) {
								floatBuffer.reset();
								stats.accept(-value);
								floatBuffer.put(-value);
							}
						}
					});

					memDataset.setBandFeatures(
							new BandFeatures(gdalDataset.getNoDataValue(), 0d, -1d, gdalconstConstants.GDT_CFloat32));
					Dataset dataset = memDataset.orElseThrow();
					rasterAttributes.setDataset(dataset);

					rasterAttributes.setGeoBox(new GeoBox(GdalUtils.getBoundingBox(dataset)));
					rasterAttributes.setColumns(memDataset.getWidth());
					rasterAttributes.setLines(memDataset.getHeight());

					rasterAttributes.setValidMin(stats.getMin());
					rasterAttributes.setValidMax(stats.getMax());
					Resolution resolution = memDataset.getResolution();
					rasterAttributes.setColumnSpacing(resolution.xRes());
					rasterAttributes.setLineSpacing(resolution.yRes());
					rasterAttributes.setMissingValue(memDataset.getNoDataValue());

					rasterAttributes.setReadSuccessfully(true);
				}
			}
		}
	}

	/**
	 * Set target extents to -180 -90 180 90. Necessary for files like monde_5m.grd
	 */
	protected GdalDataset reproject(GdalDataset dataset) {
		if (dataset.isPresent()) {
			double[] nsew = dataset.getExtent(); // xmin ymin xmax ymax
			double north = NumberUtils.normalizedDegreesLatitude(nsew[3]);
			double south = NumberUtils.normalizedDegreesLatitude(nsew[1]);
			double east = NumberUtils.normalizedDegreesLongitude(nsew[2]);
			double west = NumberUtils.normalizedDegreesLongitude(nsew[0]);
			if ((float) west == (float) east) {
				// Whole world ! Longitude must fit to -180 180
				Dataset reprojectedRaster = GdalUtils.reprojectRaster(dataset.get(), Angle.NEG180.degrees,
						Math.min(north, south), Angle.POS180.degrees, Math.max(north, south));
				if (reprojectedRaster != null) {
					return GdalDataset.of(reprojectedRaster);
				}
			}

		}
		// Simply load in memory
		return dataset.copy(GdalUtils.generateInMemoryFilePath());
	}

}
