/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.reader.raster;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.viewer3d.data.reader.raster.RasterAttributes;
import fr.ifremer.viewer3d.data.reader.raster.RasterReader;

/**
 * Factory of {@link #TerrainReader}
 */
@Creatable
@Singleton
public class RasterReaderFactory {

	@Inject
	private IFileService fileService;
	@Inject
	private IRasterService rasterService;

	/**
	 * Constructor.
	 */
	RasterReaderFactory() {
	}

	/**
	 * @return a {@link #TerrainReader} for parsing a GMT (V4 or V5) file.
	 */
	public RasterReader getGmtReader(String filePath) throws FileFormatException {
		RasterAttributes rasterAttributes = new RasterAttributes(filePath);
		GMTRasterAttributesFiller rasterAttributesFiller = new GMTRasterAttributesFiller(rasterAttributes);
		rasterAttributesFiller.fill(fileService, rasterService);
		return new RasterReader(rasterAttributes);
	}

	/**
	 * @return a {@link #TerrainReader} for parsing a DTM file.
	 */
	public RasterReader getDtmReader(String filePath) throws FileFormatException {
		RasterAttributes rasterAttributes = new RasterAttributes(filePath);
		DTMRasterAttributesFiller rasterAttributesFiller = new DTMRasterAttributesFiller(rasterAttributes);
		rasterAttributesFiller.fill(fileService, rasterService);
		return new RasterReader(rasterAttributes);
	}
}
