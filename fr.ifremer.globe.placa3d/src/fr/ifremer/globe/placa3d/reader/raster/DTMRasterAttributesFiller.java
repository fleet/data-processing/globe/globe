package fr.ifremer.globe.placa3d.reader.raster;

import java.io.IOException;

import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.dataset.GdalDataset.Resolution;
import fr.ifremer.globe.gdal.dataset.GdalDataset.Statistics;
import fr.ifremer.globe.gdal.programs.GdalWarp;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.viewer3d.data.reader.raster.RasterAttributes;
import fr.ifremer.viewer3d.data.reader.raster.Utils;

/**
 * This class aim at filling the RasterAttributes for a specified DTM file.
 * 
 */
public class DTMRasterAttributesFiller {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(DTMRasterAttributesFiller.class);

	/** All Attributes read in a file. */
	protected final RasterAttributes rasterAttributes;

	/**
	 * Constructor.
	 */
	public DTMRasterAttributesFiller(RasterAttributes rasterAttributes) {
		this.rasterAttributes = rasterAttributes;
	}

	/** Open the file and read all attributes. */
	public void fill(IFileService fileService, IRasterService rasterService) throws FileFormatException {
		rasterAttributes.setReadSuccessfully(false);
		IFileInfo fileInfo = fileService.getFileInfo(rasterAttributes.getFilename()).orElse(null);
		if (fileInfo != null && fileInfo.getContentType() == ContentType.DTM_NETCDF_4) {
			RasterInfo rasterInfo = rasterService.getRasterInfos(fileInfo)//
					.stream()//
					.filter(r -> RasterInfo.RASTER_ELEVATION.equals(r.getDataType()))//
					.findFirst()//
					.orElseThrow(() -> new FileFormatException("No Elevation raster found in the file"));

			try (GdalDataset gdalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey())) {

				// Load the raster in memory as a Gdal dataset
				GdalDataset memDataset = null;
				SpatialReference srs = gdalDataset.getSpatialReference();
				if (srs.IsProjected() == 1) {
					GdalWarp warp = GdalWarp.warpToSrsWgs84(gdalDataset.orElseThrow());
					warp.addTargetExtent(rasterInfo.getGeoBox().asGdalExtent());
					warp.addTargetSize(rasterInfo.getXSize(), rasterInfo.getYSize());
					String tempFile = warp.getOutputFile();
					try (var tempDataset = GdalDataset.of(warp.run())) {
						memDataset = tempDataset.copy(GdalUtils.generateInMemoryFilePath());
					}
					GdalUtils.deleteFile(tempFile);
				} else {
					memDataset = gdalDataset.copy(GdalUtils.generateInMemoryFilePath());
				}
				rasterAttributes.setGeoBox(rasterInfo.getGeoBox());
				rasterAttributes.setColumns(memDataset.getWidth());
				rasterAttributes.setLines(memDataset.getHeight());
				rasterAttributes.setMissingValue(memDataset.getNoDataValue());

				Statistics stats = memDataset.computeStatistics(0).orElseThrow();
				rasterAttributes.setValidMin(stats.min());
				rasterAttributes.setValidMax(stats.max());
				Resolution resolution = memDataset.getResolution();
				rasterAttributes.setColumnSpacing(resolution.xRes());
				rasterAttributes.setLineSpacing(resolution.yRes());
				rasterAttributes.setDataset(memDataset.orElseThrow());
				rasterAttributes.setReadSuccessfully(true);
			} catch (IOException e) {
				Utils.throwFileFormatException(rasterAttributes);
			}

		}
	}
}
