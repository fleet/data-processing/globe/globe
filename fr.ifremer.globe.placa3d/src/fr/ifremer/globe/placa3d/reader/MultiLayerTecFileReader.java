package fr.ifremer.globe.placa3d.reader;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.ELoadingType;
import fr.ifremer.globe.placa3d.data.TectonicGrid;
import fr.ifremer.globe.placa3d.data.TectonicLayer;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateMetadata;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.mtec.MultiLayerTecFile;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.tectonic.Rotation;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.h5.H5GroupWithMembers;
import fr.ifremer.globe.utils.h5.H5Utils;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Sector;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5File;

/**
 * Reader for Multilayer TEC file format.
 *
 * @author apside
 */
public class MultiLayerTecFileReader implements AutoCloseable {

	protected static Logger logger = LoggerFactory.getLogger(MultiLayerTecFileReader.class);

	/** The regular File object. */
	protected File file;

	/** The H5File to be read. */
	protected H5File h5File;

	/**
	 * Constructor.
	 *
	 * @param sourceFile The file to be read.
	 */
	public MultiLayerTecFileReader(File sourceFile) {
		super();
		file = sourceFile;
	}

	/**
	 * Get the H5Group of the tectonic plate.
	 *
	 * @param plateIndex The tectonic plate's index.
	 * @return The group found or <code>null</code> if no group found.
	 */
	protected H5GroupWithMembers getPlateGroup(int plateIndex) throws Exception {
		H5GroupWithMembers plateGroup = H5Utils.getGroupWithMembers(h5File,
				MultiLayerTecFile.GROUPNODE_PLATES + "/" + MultiLayerTecFile.GROUPNODE_PLATE_PREFIX + plateIndex);
		return plateGroup;
	}

	/**
	 * Opens the underlying file in READ mode.
	 */
	public void open() throws Exception {
		if (file.exists()) {
			h5File = new H5File(file.getAbsolutePath(), FileFormat.READ);
			h5File.open();
		} else {
			throw new FileNotFoundException(file.getAbsolutePath());
		}
	}

	/**
	 * Closes the underlying stream.
	 */
	@Override
	public void close() throws Exception {
		if (h5File != null) {
			h5File.close();
			h5File = null;
		}
	}

	/**
	 * Loads only the file's metadata. If metadata are already loaded, this method has no effect.
	 *
	 * @param model The tectonic model to feed.
	 */
	public void loadMetadata(TectonicModel model) throws Exception {

		H5GroupWithMembers root = H5Utils.getGroupWithMembers(h5File, MultiLayerTecFile.GROUPNODE_PLATES);

		// Read all plates
		if (root != null) {
			int nbPlates = root.getIntegerAttributeValue(MultiLayerTecFile.ATT_PLATES_NB_PLATES);
			if (nbPlates > 0) {
				for (int iPlate = 0; iPlate < nbPlates; iPlate++) {
					readPlate(model, iPlate, null, false, false);
				}
			}
		} else {
			throw new FileFormatException("No root element '" + MultiLayerTecFile.GROUPNODE_PLATES + "' found in file "
					+ file.getAbsolutePath());
		}
	}

	/**
	 * Reads only file metadata but keep data unloaded. Use {@link #readPlate(int, boolean) readPlate(numPlate, true)}
	 * to load the masking grid and grids of current age.
	 *
	 * @param model The tectonic model to feed.
	 * @param plateNum Plate's number.
	 * @param monitor Monitor for the progression (optionnal).
	 *
	 * @return The plate's model.
	 */
	public TectonicPlateModel readPlate(TectonicModel model, int plateNum, IProgressMonitor monitor) throws Exception {
		return readPlate(model, plateNum, monitor, false, false);
	}

	/**
	 * Reads file metadata and optionnaly load data.
	 *
	 * @param model The tectonic model to feed.
	 * @param plateNum Plate's number.
	 * @param monitor Monitor for the progression (optionnal).
	 * @param readFully <code>true</code> to perform a full data loading.
	 * @param forceReload <code>true</code> to reload data <code>false</code> to skip loading if data already exist.
	 * @return The new TectonicPlateModel.
	 * @throws IOException
	 */
	public TectonicPlateModel readPlate(TectonicModel model, int plateNum, IProgressMonitor monitor, boolean readFully,
			boolean forceReload) throws Exception {

		TectonicPlateModel result = null;

		int size = model.getPlates().size();
		if (plateNum < size) {
			result = model.getPlate(plateNum);
		} else {
			H5GroupWithMembers plateGroup = getPlateGroup(plateNum);

			String plateName = plateGroup.getStringAttributeValue(MultiLayerTecFile.ATT_PLATE_NAME);
			result = model.addPlate(plateName, null);

			// Load the plate's metadata
			loadPlateMetadata(result.getMetadata(), true);
		}

		if (readFully) {
			loadPlateGrids(result, model.getAge(), forceReload);
		}
		return result;
	}

	/**
	 * Reads the metadata from file and loads it into the plate.
	 *
	 * @param plate Plate to be loaded.
	 * @param forceReload Use <code>true</code> to force metadata reload.
	 */
	private void loadPlateMetadata(TectonicPlateMetadata plate, boolean forceReload) throws Exception {
		// Geobox
		loadPlateGeoBox(plate, forceReload);

		// Rotation poles
		loadPlateMovement(plate, forceReload);

		// Other metadata
		H5GroupWithMembers plateGroup = getPlateGroup(plate.getPlateIndex());
		H5GroupWithMembers layersGroup = plateGroup.getGroup(MultiLayerTecFile.GROUPNODE_PLATE_LAYERS);

		// Number of age layers
		int nbLayers = layersGroup.getIntegerAttributeValue(MultiLayerTecFile.ATT_LAYERS_NBLAYERS);
		plate.setAgeLayersCount(nbLayers);

		// Default min/max elevation = invalid value
		plate.setZrange(new DoubleRange(TecGeneratorUtil.getMissingValue(), TecGeneratorUtil.getMissingValue()));

		double[] zRange = plateGroup.getDoubleArrayAttributeValue(MultiLayerTecFile.ATT_PLATE_Z_RANGE);
		if (zRange != null && zRange.length == 2) {
			plate.setZrange(new DoubleRange(zRange[0], zRange[1]));
		}

		double minElevation = Double.MAX_VALUE;
		double maxElevation = Double.MIN_VALUE;

		// Age layers intervals
		List<TectonicLayer> TectonicLayers = new ArrayList<>();
		for (int iLayer = 0; iLayer < nbLayers; iLayer++) {

			H5GroupWithMembers eraGroup = layersGroup.getGroup(MultiLayerTecFile.GROUPNODE_PLATE_ERA + iLayer);

			// Grid's age interval
			// String sStartAge =
			// eraGroup.getStringAttributeValue(MultiLayerTecFile.ATT_ERA_START);
			// String sEndAge =
			// eraGroup.getStringAttributeValue(MultiLayerTecFile.ATT_ERA_END);
			// double startAge =
			// MagneticAnomalies.getInstance().getDoubleAnomaly(sStartAge);
			// double endAge =
			// MagneticAnomalies.getInstance().getDoubleAnomaly(sEndAge);
			String layerName = eraGroup.containsAttribute(MultiLayerTecFile.ATT_LAYER_NAME)
					? eraGroup.getStringAttributeValue(MultiLayerTecFile.ATT_LAYER_NAME)
					: "Layer " + iLayer;
			double startAge = eraGroup.getDoubleAttributeValue(MultiLayerTecFile.ATT_ERA_START);
			double endAge = eraGroup.getDoubleAttributeValue(MultiLayerTecFile.ATT_ERA_END);
			DoubleRange ageLayersInterval = new DoubleRange(startAge, endAge);
			TectonicLayers.add(new TectonicLayer(layerName, ageLayersInterval));

			// Grids availability
			List<HObject> members = eraGroup.getMembers();
			for (HObject hObject : members) {
				H5GroupWithMembers gridGroup = eraGroup.getGroup(hObject.getName());
				ELoadingType type = ELoadingType.quietValueOf(hObject.getName());
				Double min = gridGroup.getDoubleAttributeValue(MultiLayerTecFile.ATT_GRID_MIN_VALUE);
				Double max = gridGroup.getDoubleAttributeValue(MultiLayerTecFile.ATT_GRID_MAX_VALUE);
				Integer w = gridGroup.getIntegerAttributeValue(MultiLayerTecFile.ATT_GRID_WIDTH);
				Integer h = gridGroup.getIntegerAttributeValue(MultiLayerTecFile.ATT_GRID_HEIGHT);

				if (type == ELoadingType.ELEVATION) {
					plate.setHasElevationGrid(true, new Dimension(w, h));
					if (min != null) {
						minElevation = Math.min(min, minElevation);
					}
					if (max != null) {
						maxElevation = Math.max(max, maxElevation);
					}
				}
				if (type == ELoadingType.TEXTURE) {
					plate.setHasTexture(true, new Dimension(w, h));
				}
				if (type == ELoadingType.AGE_GRID) {
					plate.setHasMaskingGrid(true, new Dimension(w, h));
				}
			}
		}

		if (minElevation != Double.MAX_VALUE && maxElevation != Double.MIN_VALUE) {
			plate.setZrange(new DoubleRange(minElevation, maxElevation));
		}

		plate.setTectonicLayers(TectonicLayers);
	}

	public void loadPlateGrids(TectonicPlateModel plate, double age, boolean forceReload) throws Exception {

		if (forceReload || !plate.hasGrids()) {

			TectonicPlateMetadata metadata = plate.getMetadata();
			H5GroupWithMembers plateGroup = getPlateGroup(metadata.getPlateIndex());
			H5GroupWithMembers layersGroup = plateGroup.getGroup(MultiLayerTecFile.GROUPNODE_PLATE_LAYERS);

			boolean isAgeFound = false;

			int nbLayers = layersGroup.getIntegerAttributeValue(MultiLayerTecFile.ATT_LAYERS_NBLAYERS);
			for (int iLayer = 0; iLayer < nbLayers; iLayer++) {

				H5GroupWithMembers eraGroup = layersGroup.getGroup(MultiLayerTecFile.GROUPNODE_PLATE_ERA + iLayer);

				// String sStartAge =
				// eraGroup.getStringAttributeValue(MultiLayerTecFile.ATT_ERA_START);
				// String sEndAge =
				// eraGroup.getStringAttributeValue(MultiLayerTecFile.ATT_ERA_END);

				// double startAge =
				// MagneticAnomalies.getInstance().getDoubleAnomaly(sStartAge);
				// double endAge =
				// MagneticAnomalies.getInstance().getDoubleAnomaly(sEndAge);
				double startAge = eraGroup.getDoubleAttributeValue(MultiLayerTecFile.ATT_ERA_START);
				double endAge = eraGroup.getDoubleAttributeValue(MultiLayerTecFile.ATT_ERA_END);

				if (age >= startAge
						&& (age < endAge || endAge == Activator.getPreferences().getMaximumAge().getValue())) {
					isAgeFound = true;

					List<HObject> members = eraGroup.getMembers();
					for (HObject hObject : members) {
						H5GroupWithMembers gridGroup = eraGroup.getGroup(hObject.getName());

						int height = gridGroup.getIntegerAttributeValue(MultiLayerTecFile.ATT_GRID_HEIGHT);
						int width = gridGroup.getIntegerAttributeValue(MultiLayerTecFile.ATT_GRID_WIDTH);
						double[][] data = gridGroup.getDouble2DDatasetValue(hObject.getName(), width, height);

						TectonicGrid grid = new TectonicGrid(metadata.getGeoBox(), data);

						ELoadingType type = ELoadingType.quietValueOf(hObject.getName().toUpperCase());
						if (type == ELoadingType.ELEVATION) {
							plate.setElevationGrid(grid);
						} else if (type == ELoadingType.TEXTURE) {
							plate.setTextureGrid(grid);
						} else if (type == ELoadingType.AGE_GRID) {
							plate.setMaskingGrid(grid);
						}
					}
					break;
				}
			}
			if (!isAgeFound) {
				if (metadata.hasElevation()) {
					Dimension elevationGridDimension = metadata.getElevationGridDimension();
					plate.setElevationGrid(new TectonicGrid(metadata.getGeoBox(),
							new double[elevationGridDimension.width][elevationGridDimension.height]));
				}
				if (metadata.hasTexture()) {
					Dimension textureGridDimension = metadata.getTextureGridDimension();
					plate.setTextureGrid(new TectonicGrid(metadata.getGeoBox(),
							new double[textureGridDimension.width][textureGridDimension.height]));
				}
				if (metadata.hasMaskingGrid()) {
					Dimension maskingGridDimension = metadata.getMaskingGridDimension();
					plate.setMaskingGrid(new TectonicGrid(metadata.getGeoBox(),
							new double[maskingGridDimension.width][maskingGridDimension.height]));
				}
			}
		}
	}

	public void loadPlateGeoBox(TectonicPlateMetadata plate, boolean forceReload) throws Exception {

		if (forceReload || plate.getGeoBox() == null) {
			H5GroupWithMembers plateGroup = getPlateGroup(plate.getPlateIndex());

			double[] x_range = plateGroup.getDoubleArrayAttributeValue(MultiLayerTecFile.ATT_PLATE_LONG_RANGE);
			double[] y_range = plateGroup.getDoubleArrayAttributeValue(MultiLayerTecFile.ATT_PLATE_LAT_RANGE);

			Sector result = Sector.fromDegrees(y_range[0], y_range[1], x_range[0], x_range[1]);

			plate.setGeoBox(result);
		}
	}

	public void loadPlateMovement(TectonicPlateMetadata plate, boolean forceReload) throws Exception {

		if (forceReload || plate.getMovement() == null) {

			TectonicMovement ret = new TectonicMovement(h5File.getAbsolutePath());

			H5GroupWithMembers plateGroup = getPlateGroup(plate.getPlateIndex());
			H5GroupWithMembers polesGroup = plateGroup.getGroup(MultiLayerTecFile.GROUPNODE_PLATE_POLES);

			if (polesGroup != null) {

				double[] latTab = polesGroup
						.getDoubleArrayAttributeValue(MultiLayerTecFile.ATT_POLES_LATPOLEROTATIONLIST);
				double[] longTab = polesGroup
						.getDoubleArrayAttributeValue(MultiLayerTecFile.ATT_POLES_LONGPOLEROTATIONLIST);
				double[] angleTab = polesGroup
						.getDoubleArrayAttributeValue(MultiLayerTecFile.ATT_POLES_ANGLEPOLEROTATIONLIST);
				String[] dateTab = polesGroup
						.getStringArrayDatasetValue(MultiLayerTecFile.ATT_POLES_DATEPOLEROTATIONLIST);
				String[] endDateTab = polesGroup
						.getStringArrayDatasetValue(MultiLayerTecFile.ATT_POLES_ENDDATEPOLEROTATIONLIST);

				if (latTab != null && longTab != null && angleTab != null && dateTab != null && endDateTab != null) {
					// Add the first total pole rotation
					ret.addTotalRotation(new Rotation(LatLon.ZERO, 0, 0, 0));

					Rotation intRotation = null;
					String missingMagneticAnomalies = null;
					for (int i = 0; i < angleTab.length; i++) {
						if (!dateTab[i].isEmpty()) {

							// Construct intermediate pole rotation
							Double startAno = MagneticAnomalies.getInstance().getDoubleAnomaly(dateTab[i]);
							Double endAno = MagneticAnomalies.getInstance().getDoubleAnomaly(endDateTab[i]);
							if (startAno != null && endAno != null) {
								double begin = MagneticAnomalies.getInstance().getDoubleAnomaly(dateTab[i]);
								double end = MagneticAnomalies.getInstance().getDoubleAnomaly(endDateTab[i]);
								intRotation = TectonicUtils.getRotation(begin, end, latTab[i], longTab[i], angleTab[i]);
								// Add intermediate pole rotation
								ret.addRotation(intRotation);

								// Construct total pole rotation matrix
								List<Matrix> intMatrixList = new ArrayList<>();
								for (int j = 0; j < ret.getPoleRotation().getAngles().size(); j++) {
									Matrix m = TectonicUtils.computeMatrix(ret.getLatitudes().get(j),
											ret.getLongitudes().get(j), -ret.getAngles().get(j));
									intMatrixList.add(m);
								}

								// Construct total pole rotation
								Rotation totRotation = TectonicUtils.convertIntToTot(intMatrixList, 0,
										intRotation.getEnd());
								// Add total pole rotation
								ret.addTotalRotation(totRotation);
							}

							if (startAno == null) {
								missingMagneticAnomalies = missingMagneticAnomalies != null
										? missingMagneticAnomalies + ", " + dateTab[i]
										: dateTab[i];
							}
							if (endAno == null) {
								missingMagneticAnomalies = missingMagneticAnomalies != null
										? missingMagneticAnomalies + ", " + endDateTab[i]
										: endDateTab[i];
							}
						}
					}

					if (missingMagneticAnomalies != null) {
						Messages.openWarningMessage("Loading error", MessageFormat.format(
								"Some ages in the Tec file are not present in echelles.temps ({0}). The correspnding movements will not be assumed",
								missingMagneticAnomalies));
					}
					plate.setMovement(ret);
				}
			}
		}
	}
}
