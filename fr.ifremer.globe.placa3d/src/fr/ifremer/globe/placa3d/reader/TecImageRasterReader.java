﻿package fr.ifremer.globe.placa3d.reader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;

import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.util.ImageUtil;

/**
 * Class used by TiledImageProducer to read a .tec file and create DataRaster
 * (ImageBufferRaster).
 * 
 * @author pmahoudo
 */
public class TecImageRasterReader extends TecRasterReader {

	private double valGlobaleMin = -20000;
	private double valGlobaleMax = 20000;
	private boolean isRGB;

	public TecImageRasterReader(MultiLayerTecImageElevationData tecFile, Area area) {
		super(tecFile, area);
	}

	@Override
	protected void initMetadata() {
		// .tec with texture data

		double[][] texture = file.getTexture(area);
		double[][] elevation = file.getDepthRaster(area);

		if (texture != null) {

			isRGB = true; // image in RGB

			valid_min = -Double.MAX_VALUE;
			valid_max = Double.MAX_VALUE;
			depth_offset = 0.0; // TODO read on tecfile
			depth_scale = 1.0; // TODO read on tecfile

			valGlobaleMin = 0;
			valGlobaleMax = 1;

			// dimension of the texture array
			lines = texture.length;
			if (lines > 0) {
				columns = texture[0].length;
			}

		}
		// .tec with no texture data ==> take elevation data
		else if (elevation != null) {

			isRGB = false; // image in elevation

			valid_min = file.getMinDepth();
			valid_max = file.getMaxDepth();
			depth_offset = 0.0; // TODO read on tecfile
			depth_scale = 1.0; // TODO read on tecfile

			// dimension of the depth array
			lines = elevation.length;
			if (lines > 0) {
				columns = elevation[0].length;
			}
		}

		super.initMetadata();
		if (metadata != null) {
			metadata.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
		}
	}

	/**
	 * compute DataRaster from data matrix
	 * 
	 * @return NWW DataRaster (BufferedImageRaster)
	 */
	@Override
	public BufferedImageRaster doRead() {

		try {
			// Reload data and metadata from disk
			this.file.read();
			this.initMetadata();

			// No image
			if (columns == 0 || lines == 0) {
				return null;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		AVList values = metadata;
		BufferedImage rgbImage = null;

		boolean hasTexture = file.getTexture(area) != null;
		boolean hasElevation = file.getDepthRaster(area) != null;

		// get the data from the reader
		double[][] data = null;
		// Check if tecfile contains texture data
		if (hasTexture) {
			data = file.getTexture(area);
		}
		// If not, take the elevation data
		else if (hasElevation) {
			data = file.getDepthRaster(area);
		}

		final double MISSING = TecGeneratorUtil.getMissingValue();

		rgbImage = new BufferedImage(columns, lines, BufferedImage.TYPE_INT_ARGB);
		WritableRaster wrRaster = rgbImage.getRaster();

		double elevMin = getValGlobaleMin();// en m
		double elevMax = getValGlobaleMax();// en m

		for (int iRow = 0; iRow < lines; iRow++) {
			for (int iCol = 0; iCol < columns; iCol++) {
				// Set missing value

				double dValue = data[iRow][iCol];

				if (dValue < valid_min || dValue > valid_max || dValue == MISSING || dValue != dValue) {
					wrRaster.setSample(iCol, iRow, 0, MISSING);
					wrRaster.setSample(iCol, iRow, 1, MISSING);
					wrRaster.setSample(iCol, iRow, 2, MISSING);
					wrRaster.setSample(iCol, iRow, 3, MISSING);
				} else {
					double val = (dValue + depth_offset) * depth_scale;

					int iValue = 0;
					if (hasTexture) {
						iValue = (int) val;
					} else if (hasElevation) {
						iValue = (int) ((val - elevMin) / (elevMax - elevMin) * 256 * 256 * 256);// elevation
																									// 24bits
					}

					Color color = new Color(iValue);

					wrRaster.setSample(iCol, iRow, 0, color.getRed());
					wrRaster.setSample(iCol, iRow, 1, color.getGreen());
					wrRaster.setSample(iCol, iRow, 2, color.getBlue());
					wrRaster.setSample(iCol, iRow, 3, 255);
				}
			}
		}

		values.setValue(AVKey.DATA_TYPE, AVKey.FLOAT32);
		rgbImage = ImageUtil.toCompatibleImage(rgbImage);
		return (BufferedImageRaster) BufferedImageRaster.wrap(rgbImage, values);

	}

	public double getValGlobaleMin() {
		return valGlobaleMin;
	}

	public void setValGlobaleMin(double valGlobaleMin) {
		this.valGlobaleMin = valGlobaleMin;
	}

	public double getValGlobaleMax() {
		return valGlobaleMax;
	}

	public void setValGlobaleMax(double valGlobaleMax) {
		this.valGlobaleMax = valGlobaleMax;
	}

	public boolean isRGB() {
		return isRGB;
	}

	public void setRGB(boolean isRGB) {
		this.isRGB = isRGB;
	}

}
