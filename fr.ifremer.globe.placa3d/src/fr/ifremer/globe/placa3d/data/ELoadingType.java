package fr.ifremer.globe.placa3d.data;

public enum ELoadingType {

	BATHY,

	BATHY_AND_IMAGING,

	NAVIGATION,

	SPEED,

	NAVIGATION_LINE,

	AGE_GRID,

	TEXTURE,

	ELEVATION,
	
	JOURNEY;

	/**
	 * Same as {@link #valueOf(String)} but does not throws any exception if the specified string does not exists in the enum.
	 * 
	 * @param s
	 *            The searched string.
	 * @return the enum constant or <code>null</code> if no enum constant found.
	 */
	public static ELoadingType quietValueOf(String s) {
		ELoadingType result = null;
		try {
			result = valueOf(s);
		} catch (Exception ignored) {
		}
		return result;
	}
}
