package fr.ifremer.globe.placa3d.data;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;

import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.geom.Sector;

/**
 * A grid is a geographic sector divided into <code>w*h</code> parts.
 */
public class TectonicGrid {

	protected Sector sector;

	protected int nbLonCells; // width
	protected int nbLatCells; // height

	protected ISectorMovementProcessing movement;

	protected double[][] data;

	protected DoubleRange range;

	private double min;
	private double max;

	public TectonicGrid(Sector sector, double[][] data) {
		setBoundingBox(sector);
		setData(data);
		computeMinMax();
	}

	protected void computeMinMax() {
		double[][] data = getData();
		min = Double.POSITIVE_INFINITY;
		max = Double.NEGATIVE_INFINITY;
		double missing = TecGeneratorUtil.getMissingValue();
		for (int i = 0; i < data.length; i++) {
			// (w == w) <==> Double.isNaN(w)
			DoubleSummaryStatistics stat = Arrays.stream(data[i]).filter(w -> (w == w) && (w != missing))
					.summaryStatistics();
			double statMin = stat.getMin();
			double statMax = stat.getMax();
			min = statMin == statMin && statMin != missing ? Math.min(min, statMin) : min;
			max = statMax == statMax && statMax != missing ? Math.max(max, statMax) : max;
		}
		setDataRange(new DoubleRange(min, max));
	}

	/**
	 * Get the data range. This property is not computed automatically. It must be manually set with
	 * {@link #setDataRange(Range)}.
	 * 
	 * @return The range (min and max) of values.
	 */
	public DoubleRange getDataRange() {
		return range;
	}

	public double getValMin() {
		return min;
	}

	public void setValMin(double valMin) {
		this.min = valMin;
	}

	public double getValMax() {
		return max;
	}

	public void setValMax(double valMax) {
		this.max = valMax;
	}

	/**
	 * The geographic bounding sector of the masking grid.
	 * 
	 * @return The geographic bounding sector of the masking grid.
	 */
	public Sector getBoundingBox() {
		return sector;
	}

	/**
	 * Sets the geographic bounding sector of the masking grid.
	 * 
	 * @param boundingBox The geographic bounding sector of the masking grid.
	 */
	public void setBoundingBox(Sector boundingBox) {
		this.sector = boundingBox;
	}

	/**
	 * The tectonic movement associated to the grid.
	 * 
	 * @return The tectonic movement associated to the grid.
	 */
	public ISectorMovementProcessing getMovement() {
		return this.movement;
	}

	/**
	 * Sets the tectonic movement associated to the grid.
	 * 
	 * @param movement The tectonic movement associated to the grid.
	 */
	public void setMovement(ISectorMovementProcessing movement) {
		this.movement = movement;
	}

	/**
	 * Get the grid's data.
	 * 
	 * @return Grid's data.
	 */
	public double[][] getData() {
		return this.data;
	}

	/**
	 * Modify the grid's data.
	 * 
	 * @param data Grid's data.
	 */
	public void setData(double[][] data) {
		this.nbLatCells = Array.getLength(data);
		this.nbLonCells = Array.getLength(Array.get(data, 0));
		this.data = data;
	}

	/**
	 * Number of longitude cells.
	 * 
	 * @return Number of longitude cells.
	 */
	public int getLongitudeCount() {
		return this.nbLonCells;
	}

	/**
	 * Number of latitude cells.
	 * 
	 * @return Number of latitude cells.
	 */
	public int getLatitudeCount() {
		return this.nbLatCells;
	}

	/**
	 * Modify the data range (min andd max values of the grid).
	 * 
	 * @param range New data range.
	 */
	public void setDataRange(DoubleRange range) {
		this.range = range;
	}

}
