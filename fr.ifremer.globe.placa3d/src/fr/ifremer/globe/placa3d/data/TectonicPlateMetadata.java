package fr.ifremer.globe.placa3d.data;

import java.awt.Dimension;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.placa3d.tecGenerator.ITecConstants;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import gov.nasa.worldwind.geom.Sector;

/**
 * Tectonic plate's metadata.<br>
 * Minimal information avalaible for a tectonic plate after this plate is read
 * from a multilayer TEC file (.mtec).
 */
public class TectonicPlateMetadata implements IInfos {

	protected int plateIndex;

	protected String plateName;

	protected Sector geobox;

	protected TectonicMovement movement;

	protected boolean hasMaskingGrid;
	protected boolean hasElevation;
	protected boolean hasTexture;

	private int ageLayersCount;

	protected List<TectonicLayer> tectonicLayers;

	protected int numberOfRotationPoles;

	protected DoubleRange zRange;
	protected String dimension;

	protected String poleComments;
	protected String cotComments;

	// Grids dimensions
	protected Dimension maskingGridDimension;
	protected Dimension elevationGridDimension;
	protected Dimension textureGridDimension;

	public TectonicPlateMetadata() {
		super();
		this.tectonicLayers = new ArrayList<>();
	}

	/**
	 * Plate's index inside its TectonicModel.
	 * 
	 * @return Plate's index inside its TectonicModel.
	 */
	public int getPlateIndex() {
		return plateIndex;
	}

	/**
	 * Set the plate's index. Must be unique inside its tectonic model.
	 * 
	 * @param plateIndex
	 *            The plate's index.
	 */
	public void setPlateIndex(int plateNumber) {
		this.plateIndex = plateNumber;
	}

	/**
	 * Plate's name.
	 * 
	 * @return The plate's name.
	 */
	public String getPlateName() {
		return plateName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Property<?>> getProperties() {
		AbstractInfos infos = new AbstractInfos();

		infos.addInfo(IConstants.Name, plateName);

		infos.addInfo(IConstants.NorthLatitude, FormatLatitudeLongitude
				.latitudeToString(geobox.getMaxLatitude().degrees));
		infos.addInfo(IConstants.SouthLatitude, FormatLatitudeLongitude
				.latitudeToString(geobox.getMinLatitude().degrees));
		infos.addInfo(IConstants.WestLongitude, FormatLatitudeLongitude
				.longitudeToString(geobox.getMinLongitude().degrees));
		infos.addInfo(IConstants.EastLongitude, FormatLatitudeLongitude
				.longitudeToString(geobox.getMaxLongitude().degrees));

		DecimalFormat dfZ_Range = new DecimalFormat("#.##");
		double minZ = zRange.getMinimumDouble();
		double maxZ = zRange.getMaximumDouble();
		double missingValue = TecGeneratorUtil.getMissingValue();
		boolean isMinMissing = minZ == Double.NaN || minZ == missingValue;
		boolean isMaxMissing = maxZ == Double.NaN || maxZ == missingValue;
		infos.addInfo(ITecConstants.propertyZ_range, isMinMissing ? "n.a."
				: dfZ_Range.format(minZ) + " -> "
						+ (isMaxMissing ? "n.a." : dfZ_Range.format(maxZ)));

		infos.addInfo(ITecConstants.propertyNumberOfPoles,
				String.valueOf(getPoleNumber()));
		infos.addInfo(ITecConstants.propertyCommPole, poleComments);

		infos.addInfo(ITecConstants.propertyHasMaskingGrid,
				hasMaskingGrid ? "Yes" : "No");
		if (hasMaskingGrid) {
			infos.addInfo(ITecConstants.propertyMaskingGridDimension,
					maskingGridDimension.width + "x"
							+ maskingGridDimension.height);
		}
		infos.addInfo(ITecConstants.propertyHasElevation, hasElevation ? "Yes"
				: "No");
		if (hasElevation) {
			infos.addInfo(ITecConstants.propertyElevationGridDimension,
					elevationGridDimension.width + "x"
							+ elevationGridDimension.height);
		}
		infos.addInfo(ITecConstants.propertyHasTexture, hasTexture ? "Yes"
				: "No");
		if (hasTexture) {
			infos.addInfo(ITecConstants.propertyTextureGridDimension,
					textureGridDimension.width + "x"
							+ textureGridDimension.height);
		}
		infos.addInfo(ITecConstants.propertyAgeLayersCount, ageLayersCount);

		infos.addInfo(ITecConstants.propertyLayers, "");
		Property<?> layersProperty = infos.getInfo(ITecConstants.propertyLayers);
		Map<Double, String> anomalies = MagneticAnomalies.getInstance().getAnomaliesTreeMap();
		for (TectonicLayer tectonicLayer : tectonicLayers) {
			String minAnomaly = anomalies.get(tectonicLayer.getMinimumDouble());
			String maxAnomaly = anomalies.get(tectonicLayer.getMaximumDouble());
			minAnomaly = minAnomaly != null ? minAnomaly : String.valueOf(tectonicLayer.getMinimumDouble());
			maxAnomaly = maxAnomaly != null ? maxAnomaly : String.valueOf(tectonicLayer.getMaximumDouble());
			layersProperty.add(Property.build(tectonicLayer.getName(), MessageFormat.format(ITecConstants.rangePattern, minAnomaly, maxAnomaly)));
		}

		return infos.getProperties();
	}

	/**
	 * Geographical bounds of the plate.
	 * 
	 * @return Geographical bounds of the plate.
	 */
	public Sector getGeoBox() {
		return this.geobox;
	}

	/**
	 * Set the geographical bounds of the plate.
	 * 
	 * @param box
	 *            the geographical bounds of the plate.
	 */
	public void setGeoBox(GeoBox box) {
		this.geobox = Sector.fromDegrees(box.getTop(),
				box.getBottom(), box.getRight(),
				box.getLeft());

	}

	/**
	 * Set the geographical bounds of the plate.
	 * 
	 * @param box
	 *            the geographical bounds of the plate.
	 */
	public void setGeoBox(Sector sector) {
		this.geobox = sector;

	}

	/**
	 * The tectonic movement if exists.
	 * 
	 * @return The tectonic movement or <code>null</code> if the plate has no
	 *         tectonic movement.
	 */
	public TectonicMovement getMovement() {
		return movement;
	}

	/**
	 * Set the plate's movement.
	 * 
	 * @param movement
	 *            the new plate's movement.
	 */
	public void setMovement(TectonicMovement movement) {
		this.movement = movement;
		if (movement != null) {
			setPoleNumber(movement.getPoleRotation().size());
		}
	}

	/**
	 * @return <code>true</code> if {@link #getMovement()} is not
	 *         <code>null</code>.
	 */
	public boolean hasMovement() {
		return getMovement() != null;
	}

	/**
	 * Returns whether the plate has a masking grid or not. A masking grid
	 * defines for each sector its age of appearance/disappearance.
	 * 
	 * @return <code>true</code> if the plate has a masking grid.
	 */
	public boolean hasMaskingGrid() {
		return hasMaskingGrid;
	}

	/**
	 * Set the plate's 'HasElevation' property.
	 * 
	 * @param hasElevation
	 *            <code>true</code> if the plate has at least one elevation
	 *            grid.
	 * @param dim
	 *            If $hasElevation is <code>true</code>, dimension of the grid.
	 */
	public void setHasMaskingGrid(boolean hasMaskingGrid, Dimension dim) {
		this.hasMaskingGrid = hasMaskingGrid;
		this.maskingGridDimension = dim;
	}

	/**
	 * The number of age layers. A plate may have N layers defining its
	 * elevation and/or texture for each period of time.
	 * <p>
	 * Age layers are used to simulate abductions and subductions, ie. when
	 * geographical sectors become invisible.
	 * 
	 * @return number of age layers.
	 */
	public int getAgeLayersCount() {
		return ageLayersCount;
	}

	/**
	 * Set the number of age layers.
	 * 
	 * @param layerCount
	 *            number of age layers.
	 */
	public void setAgeLayersCount(int layerCount) {
		this.ageLayersCount = layerCount;
	}

	/**
	 * List of time ranges for which the plate has layers. The list is always
	 * ordered ASC.
	 * 
	 * @return time ranges for which the plate has layers.
	 */
	public List<TectonicLayer> getTectonicLayers() {
		return tectonicLayers;
	}

	/**
	 * Set the list of intervals of time for which this plate has elevation
	 * and/or texture layers.
	 * 
	 * @param tectonicLayers
	 *            the list of intervals of time for which this plate has
	 *            elevation and/or texture layers.
	 */
	public void setTectonicLayers(List<TectonicLayer> tectonicLayers) {

		if (tectonicLayers != null) {
			this.tectonicLayers = new ArrayList<>(tectonicLayers);

			// Sort the new list by ascending ages
			Collections.sort(tectonicLayers, (o1, o2) -> {
				return Double.compare(o1.getMinimumDouble(), o2.getMinimumDouble());
			});
			setAgeLayersCount(tectonicLayers.size());
		} else {
			this.tectonicLayers = null;
		}
	}

	/**
	 * Elevation range of this plate.
	 * 
	 * @return Elevation range of this plate.
	 */
	public DoubleRange getZRange() {
		return this.zRange;
	}

	/**
	 * Update the elevation range of this plate.
	 * 
	 * @param range
	 *            New elevation range.
	 */
	public void setZrange(DoubleRange range) {
		this.zRange = range;
	}

	/**
	 * The plate's dimension.
	 * 
	 * @return The plate's dimension.
	 */
	public String getDimension() {
		return dimension;
	}

	/**
	 * Set the plate's dimension.
	 * 
	 * @param dimension
	 *            the plate's dimension.
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	/**
	 * Number of rotation poles.
	 * 
	 * @return the number of rotation poles.
	 */
	public int getPoleNumber() {
		return numberOfRotationPoles;
	}

	/**
	 * Set the number of rotation poles.
	 * 
	 * @param poleNumber
	 *            the number of rotation poles.
	 */
	public void setPoleNumber(int poleNumber) {
		this.numberOfRotationPoles = poleNumber;
	}

	/**
	 * Comments associated with rotations.
	 * 
	 * @param comments
	 *            New comments.
	 */
	public void setCommentsPoles(String comments) {
		this.poleComments = comments;
	}

	public boolean hasElevation() {
		return hasElevation;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setHasElevationGrid(boolean hasElevation, Dimension dim) {
		this.hasElevation = hasElevation;
		this.elevationGridDimension = dim;
	}

	public boolean hasTexture() {
		return hasTexture;
	}

	/**
	 * Set the plate's 'HasTexture' property.
	 * 
	 * @param hasTexture
	 *            <code>true</code> if the plate has at least one texture grid.
	 * @param dim
	 *            If $hasTexture is <code>true</code>, dimension of the grid.
	 */
	public void setHasTexture(boolean hasTexture, Dimension dim) {
		this.hasTexture = hasTexture;
		this.textureGridDimension = dim;
	}

	/**
	 * {@inheritDoc}
	 */
	public Dimension getElevationGridDimension() {
		return this.elevationGridDimension;
	}

	/**
	 * {@inheritDoc}
	 */
	public Dimension getMaskingGridDimension() {
		return this.maskingGridDimension;
	}

	/**
	 * {@inheritDoc}
	 */
	public Dimension getTextureGridDimension() {
		return this.textureGridDimension;
	}

}
