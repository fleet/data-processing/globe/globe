package fr.ifremer.globe.placa3d.data;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;

/**
 * Model for a tectonic plate. A tectonic plate is bounded by a GeoBox and is
 * composed of data grids. Each grid has a visibility range which is the time
 * interval (in million years from today) where data of the grid are applicable.
 * Out the visibility range, data are not applicable.
 */
public class TectonicPlateModel extends TectonicPlateMetadata {

	/**
	 * This property changes when the current age changes AND the age layer's
	 * interval including this age also changes.
	 */
	public static final String PROPERTY_CURRENT_AGE_INTERVAL = "currentAgeInterval";

	/**
	 * Data grids of a tectonic plate : age grid (aka "masking grid"), elevation
	 * grid and texture grid.
	 */
	protected TectonicGrid[] grids = new TectonicGrid[ELoadingType.values().length];

	/** Support for PropertyChangeEvents. */
	protected PropertyChangeSupport pcs;

	/** Listener for TectonicMovement's ageIndex property. */
	private PropertyChangeListener ageListener;

	/**
	 * @param plateIndex
	 *            Plate's index (must be unique inside a tectonic model).
	 * @param plateName
	 *            Plate's name.
	 */
	TectonicPlateModel(int plateIndex, String plateName) {
		this.pcs = new PropertyChangeSupport(this);
		this.plateIndex = plateIndex;
		this.plateName = plateName;

		// Listen to the movement's event
		this.ageListener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (TectonicMovement.PROPERTY_DATE_INDEX.equals(evt.getPropertyName())) {
					handleAgeEvent(evt);
				}
			}
		};
	}

	protected void handleAgeEvent(PropertyChangeEvent evt) {

		int oldIndex = (Integer) evt.getOldValue();
		int newIndex = (Integer) evt.getNewValue();

		int oldAgeIntervalIndex = -1;
		int newAgeIntervalIndex = -1;

		int i = 0;
		List<TectonicLayer> tectonicLayers = getTectonicLayers();
		for (TectonicLayer tectonicLayer : tectonicLayers) {
			DoubleRange interval = tectonicLayer.getAge();
			double min = interval.getMinimumDouble();
			double max = interval.getMaximumDouble();

			if (oldIndex >= min && oldIndex <= max) {
				oldAgeIntervalIndex = i;
			}
			if (newIndex >= min && newIndex <= max) {
				newAgeIntervalIndex = i;
			}
			i++;
		}

		this.pcs.firePropertyChange(PROPERTY_CURRENT_AGE_INTERVAL, oldAgeIntervalIndex, newAgeIntervalIndex);
	}

	/**
	 * The properties manager. You can subscribe to properties changes thanks to
	 * this object.
	 *
	 * @return The PropertyChangeSupport for this object.
	 */
	public PropertyChangeSupport getPropertyChangeSupport() {
		return this.pcs;
	}

	/**
	 * @return The current plate's metadata.
	 */
	public TectonicPlateMetadata getMetadata() {
		return this;
	}

	/**
	 * @return The plat's current age visibility grid.
	 */
	public TectonicGrid getMaskingGrid() {
		return this.grids[ELoadingType.AGE_GRID.ordinal()];
	}

	/**
	 * @return The current plate's elevation grid.
	 */
	public TectonicGrid getElevationGrid() {
		return this.grids[ELoadingType.ELEVATION.ordinal()];
	}

	/**
	 * @return The plate's current texture.
	 */
	public TectonicGrid getTextureGrid() {
		return this.grids[ELoadingType.TEXTURE.ordinal()];
	}

	/**
	 * Set the current plate's masking grid.
	 *
	 * @param grid
	 *            The current masking grid.
	 */
	public void setMaskingGrid(TectonicGrid grid) {
		this.grids[ELoadingType.AGE_GRID.ordinal()] = grid;
		grid.setMovement(this.movement);
	}

	/**
	 * Set the current plate's elevation grid.
	 *
	 * @param grid
	 *            The current elevation grid.
	 */
	public void setElevationGrid(TectonicGrid grid) {
		this.grids[ELoadingType.ELEVATION.ordinal()] = grid;
		grid.setMovement(this.movement);
	}

	/**
	 * Set the current plate's texture grid.
	 *
	 * @param grid
	 *            The current texture grid.
	 */
	public void setTextureGrid(TectonicGrid grid) {
		this.grids[ELoadingType.TEXTURE.ordinal()] = grid;
		grid.setMovement(this.movement);
	}

	/**
	 * Free all in-memory resources associated to elevation, texture and age
	 * grids.
	 */
	public void clearGrids() {
		Arrays.fill(this.grids, null);
	}

	/**
	 * Return the geographic grids.
	 *
	 * @return Pairs name + grid.
	 */
	public TectonicGrid[] getGrids() {
		return this.grids;
	}

	/**
	 * Return the geographic grid.
	 */
	public TectonicGrid getGrid(ELoadingType loadingType) {
		return this.grids[loadingType.ordinal()];
	}

	/**
	 * Return the geographic grids.
	 *
	 * @return Pairs name + grid.
	 */
	public boolean hasGrids() {
		return Arrays.stream(this.grids).filter(Objects::nonNull).findFirst().isPresent();
	}

	/**
	 * Assigns the same movement to all grids.
	 *
	 * @param movement
	 *            The movement to apply to all grids.
	 */
	@Override
	public void setMovement(TectonicMovement movement) {

		if (this.movement != null && this.movement != movement) {
			this.movement.removePropertyChangeListener(this.ageListener);
		}

		super.setMovement(movement);

		Arrays.stream(this.grids).filter(Objects::nonNull).forEach((grid) -> grid.setMovement(movement));

		if (movement != null) {
			movement.addPropertyChangeListener(this.ageListener);
		}
	}

	/**
	 * Set the current tectonic model's age into this tectonic plate.
	 *
	 * @param age
	 *            The current age in million years starting from today.
	 */
	public void setAge(double age) {
		Arrays.stream(this.grids).filter(Objects::nonNull).forEach((grid) -> {
			TectonicMovement movement = (TectonicMovement) grid.getMovement();
			if (movement != null) {
				movement.setDateIndex((int) age);
			}
		});
	}

	/**
	 * Computes a string compound of age layer's interval min/max bounds.
	 * <p>
	 * Example : for age layers interval [4;122] it will return "4_122".
	 *
	 * @param age
	 *            Earth age in million years.
	 * @return The age layer's interval the age belongs to, formatted as a
	 *         string containing the min and max nounds of the age layer's
	 *         interval.
	 */
	public String computeAgeLayerIntervalStringForAge(double age) {
		DoubleRange requiredInterval = null;
		List<TectonicLayer> tectonicLayers = getMetadata().getTectonicLayers();
		for (TectonicLayer interval : tectonicLayers) {
			if (interval.containsDouble(age)) {
				requiredInterval = interval.getAge();
			}
		}
		String sInterval = computeAgeLayerIntervalString(requiredInterval, age);
		return sInterval;
	}

	/**
	 * @return the TectonicLayer containing the specified age
	 */
	public TectonicLayer getTectonicLayer(double age) {
		TectonicLayer result = null;
		for (TectonicLayer interval : getMetadata().getTectonicLayers()) {
			if (interval.containsDouble(age)) {
				result = interval;
			}
		}
		return result;
	}

	private String computeAgeLayerIntervalString(DoubleRange ageLayerInterval, double age) {
		String sInterval = ageLayerInterval == null ? String.valueOf(age) : ageLayerInterval.getMinimumInteger() + "_" + ageLayerInterval.getMaximumInteger();
		return sInterval;
	}

	/**
	 * Computes a string compound of age layer's interval min/max bounds.
	 * <p>
	 * Example : for age layers interval [4;122] it will return "4_122".
	 *
	 * @param age
	 *            Earth age in million years.
	 * @return The age layer's interval the age belongs to, formatted as a
	 *         string containing the min and max nounds of the age layer's
	 *         interval.
	 */
	public String computeAgeLayerIntervalStringForMorphingAge(double age) {
		DoubleRange requiredInterval = null;
		String sInterval = null;
		List<TectonicLayer> tectonicLayers = getMetadata().getTectonicLayers();
		for (int i = 0; i < tectonicLayers.size(); i++) {
			if (tectonicLayers.get(i).containsDouble(age) && i == tectonicLayers.size() - 1) {
				requiredInterval = tectonicLayers.get(i).getAge();
				sInterval = requiredInterval == null ? String.valueOf(age) : requiredInterval.getMinimumInteger() + "_" + requiredInterval.getMaximumInteger();
			} else if (tectonicLayers.get(i).containsDouble(age) && age == tectonicLayers.get(i).getMinimumDouble()) {
				requiredInterval = tectonicLayers.get(i).getAge();
				sInterval = requiredInterval == null ? String.valueOf(age) : requiredInterval.getMinimumInteger() + "_" + requiredInterval.getMaximumInteger();
			} else if (tectonicLayers.get(i).containsDouble(age)) {
				sInterval = String.valueOf((int) age);
			}
		}
		return sInterval;
	}
}
