/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.data;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;

/**
 * Layer defined in a tectonic file.<br>
 * Age range where a plate has some data grids.
 */
public class TectonicLayer {

	/** Name of this layer. */
	protected String name;
	/** Age covered by this layer. */
	protected DoubleRange age;

	/**
	 * Constructor.
	 */
	public TectonicLayer() {
	}

	/**
	 * Constructor.
	 */
	public TectonicLayer(String name, double ageMin, double ageMax) {
		this.name = name;
		this.age = new DoubleRange(ageMin, ageMax);
	}

	/**
	 * Constructor.
	 */
	public TectonicLayer(String name, DoubleRange age) {
		this.name = name;
		this.age = age;
	}

	/**
	 * Delegation.
	 * 
	 * @see org.apache.commons.lang.math.DoubleRange#getMinimumDouble()
	 */
	public double getMinimumDouble() {
		return age.getMinimumDouble();
	}

	/**
	 * Delegation.
	 * 
	 * @see org.apache.commons.lang.math.DoubleRange#getMaximumDouble()
	 */
	public double getMaximumDouble() {
		return age.getMaximumDouble();
	}

	/**
	 * Delegation.
	 * 
	 * @see org.apache.commons.lang.math.DoubleRange#containsDouble(double)
	 */
	public boolean containsDouble(double value) {
		return age.containsDouble(value);
	}

	/**
	 * Delegation.
	 * 
	 * @see org.apache.commons.lang.math.DoubleRange#overlapsRange(org.apache.commons.lang.math.Range)
	 */
	public boolean overlapsRange(Range range) {
		return age.overlapsRange(range);
	}

	/**
	 * @return the {@link #name}
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the {@link #name} to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the {@link #age}
	 */
	public DoubleRange getAge() {
		return age;
	}

	/**
	 * @param age
	 *            the {@link #age} to set
	 */
	public void setAge(DoubleRange age) {
		this.age = age;
	}

}
