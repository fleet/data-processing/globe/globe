package fr.ifremer.globe.placa3d.data;

import gov.nasa.worldwind.geom.Sector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This model contains tectonic plates and maintains the current age.
 */
public class TectonicModel {

	/**
	 * logger
	 */
	protected Logger logger = LoggerFactory.getLogger(TectonicModel.class);

	protected List<TectonicPlateModel> plates;

	protected double age;

	public TectonicModel() {
		this.plates = new ArrayList<>(10);
	}

	/**
	 * Remove all plates and reset the current age.
	 */
	public void clear() {
		this.plates.clear();
	}

	/**
	 * @return the current tectonic age in million years from today.
	 */
	public double getAge() {
		return this.age;
	}

	/**
	 * Set the current age in million years from today. This age is applied to
	 * all grids of all plates. If a movement exists for the MovableGeoGrid at
	 * the specified age, or if the grid becomes visible/invisible, a new
	 * GeoGridChangedEvent will be triggered.
	 *
	 * @param age
	 *            The new current replay age, in million years from today.
	 */
	public void setAge(double age) {
		this.age = age;

		// Modify the date in each movement associated to a visible tectonic plate model
		plates.forEach((plate) -> plate.setAge(age));
	}

	/**
	 * Add a new plate to the model.
	 *
	 * @param plateName
	 *            The plate's name.
	 * @param tectonicLayers
	 *            List of ranges where the plate has some data grids.
	 */
	public TectonicPlateModel addPlate(String plateName, List<TectonicLayer> tectonicLayerss) {
		TectonicPlateModel plate = new TectonicPlateModel(this.plates.size(), plateName);
		plate.setTectonicLayers(tectonicLayerss);
		this.plates.add(plate);
		return plate;
	}

	/**
	 * Add a new plate to the model.
	 *
	 * @param plateName
	 *            The plate's name.
	 * @param ages
	 *            List of ranges where the plate has some data grids.
	 * @param geobox
	 *            Geographical bounds of the tectonic plate.
	 */
	public TectonicPlateModel addPlate(String plateName, List<TectonicLayer> tectonicLayers, Sector geobox) {
		TectonicPlateModel plate = addPlate(plateName, tectonicLayers);
		plate.setGeoBox(geobox);
		return plate;
	}

	/**
	 * Get all plates. The returned list is immutable.
	 *
	 * @return all tectonic plates.
	 */
	public List<TectonicPlateModel> getPlates() {
		return Collections.unmodifiableList(plates);
	}

	/**
	 * Get a plate identified by its number.
	 *
	 * @param plateNum
	 *            Number of the desired plate.
	 */
	public TectonicPlateModel getPlate(int plateNum) {
		if (plateNum < 0 || plateNum >= plates.size()) {
			throw new IndexOutOfBoundsException();
		}
		return plates.get(plateNum);
	}
}
