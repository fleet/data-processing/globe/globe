package fr.ifremer.globe.placa3d.layers;

import java.util.Observer;

import org.eclipse.core.runtime.IProgressMonitor;

import com.jogamp.opengl.GL2;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.placa3d.render.cot.CotSegmentRenderer;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.WWRenderableLayer;
import fr.ifremer.viewer3d.Activator;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.IMovableLayer;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.AbstractSurfaceShape;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.ShapeAttributes;

/**
 * @author mgaro
 *
 */
public class CotLayer extends WWRenderableLayer implements ITarget, IDeletable, IMovableLayer {

	private Sector sector;

	/***
	 * the movement defining the tectonic
	 */
	private ISectorMovementProcessing movement;

	/** Preference observer */
	protected Observer activatorObserver;

	/**
	 * Build a {@link CotLayer}
	 *
	 * @param name Name of this ground overlay
	 * @param sector Lat/lon box for the overlay
	 */
	public CotLayer(String name, Sector sector) {
		if (name == null || sector == null) {
			throw new IllegalArgumentException("Invalid layer name or sector");
		}

		this.sector = sector;
		setName(name);
		setPickEnabled(false);

		// Polygons have to be filled ?
		BooleanPreferenceAttribute fillPolygonPref = Activator.getPluginParameters().getIsFillPolygonAttribute();
		WritableBoolean fillFlag = new WritableBoolean(fillPolygonPref.getValue());
		setValue("WritableBoolean.fillFlag", fillFlag);

		// React when fill polygons preference changed
		activatorObserver = (observable, arg) -> fillFlag.set(fillPolygonPref.getValue());
		fillPolygonPref.addObserver(activatorObserver);

		// React when fill flag changed
		fillFlag.addChangeListener(event -> preferenceChanged(fillFlag.isTrue()));
	}

	@Override
	public void dispose() {
		Activator.getPluginParameters().getIsFillPolygonAttribute().deleteObserver(activatorObserver);
		super.dispose();
	}

	@Override
	public void doRender(DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		gl.glPushAttrib(GL2.GL_CURRENT_BIT);
		gl.glColor3d(1.0, 1.0, 1.0);
		super.doRender(dc);
		gl.glPopAttrib();
	}

	@Override
	public void setOpacity(double opacity) {
		for (Renderable r : this.getRenderables()) {
			if (r instanceof CotSegmentRenderer) {
				CotSegmentRenderer t = (CotSegmentRenderer) r;
				t.setOpacity(opacity);
			}
		}

	}

	@Override
	public double getOpacity() {
		for (Renderable r : this.getRenderables()) {
			if (r instanceof CotSegmentRenderer) {
				CotSegmentRenderer t = (CotSegmentRenderer) r;
				return t.getOpacity();
			}
		}
		return 1;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public Sector getSector() {
		return sector;
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	@Override
	public ISectorMovementProcessing getMovement() {
		return movement;
	}

	@Override
	public void setMovement(ISectorMovementProcessing movement) {
		this.movement = movement;
		for (Renderable renderable : getRenderables()) {
			if (renderable instanceof CotSegmentRenderer) {
				CotSegmentRenderer segment = (CotSegmentRenderer) renderable;
				segment.setMovement(movement);
			}
		}
	}

	@Override
	public Sector move(Sector st) {
		if (movement != null) {
			return movement.move(st);
		}
		return st;
	}

	@Override
	public LatLon move(LatLon latLon) {
		if (movement != null) {
			return movement.move(latLon);
		}
		return latLon;
	}

	@Override
	public Sector moveInv(Sector st) {
		return null;
	}

	@Override
	public LatLon moveInv(LatLon latLon) {
		return null;
	}

	/** Invoked when IsFillPolygon preference changed */
	protected void preferenceChanged(boolean filled) {
		for (Renderable renderable : renderables) {
			if (renderable instanceof AbstractSurfaceShape) {
				ShapeAttributes attributes = ((AbstractSurfaceShape) renderable).getAttributes();
				if (attributes != null) {
					attributes.setDrawInterior(filled);
				}
			}
		}
		Viewer3D.refreshRequired();
	}

}
