package fr.ifremer.globe.placa3d.layers;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.placa3d.tectonic.PoleRotations;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWRenderableLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.WWRenderableLayer;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.viewer3d.layers.ILayerParameters;
import fr.ifremer.viewer3d.layers.LayerParameters;
import fr.ifremer.viewer3d.layers.MyAbstractLayer;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.util.Logging;

public class PoleRotationLayer extends WWRenderableLayer implements IDeletable, ILayerParameters, IWWRenderableLayer {

	/** The movement linked with the tectonic definition. */
	TectonicMovement movement;

	/** List of total poles point placemark. */
	List<PointPlacemark> totalPointList;

	/** Latlon of the origin of the line of the intermediate poles. */
	LatLon latlonOri = TectonicDataset.getInstance().getConfiguration().getLatlon();

	/** Polyline for intermediate poles. */
	Polyline line;

	/**
	 * parameters for this layer
	 */
	PoleRotationLayerParameter layerparameters = new PoleRotationLayerParameter();

	/**
	 * Build a {@link PoleRotationLayer}
	 *
	 * @param name Name of this ground overlay
	 * @param sector Lat/lon box for the overlay
	 */
	public PoleRotationLayer(String name, TectonicMovement movement) {
		if (name == null) {
			throw new IllegalArgumentException("Invalid layer name or sector");
		}

		setName(name);

		this.setEnabled(this.isEnabled());
		this.isEnabled();
		this.setMovement(movement);
		this.setOpacity(MyAbstractLayer.OPACITY_NOT_AVAILABLE);
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean delete(IProgressMonitor monitor) {
		IGeographicViewService.grab().removeLayer(this);
		return true;
	}

	public TectonicMovement getMovement() {
		return movement;
	}

	public void setMovement(TectonicMovement movement) {
		this.movement = movement;
		this.setPickEnabled(false);

		// Total poles rotation
		totalPointList = new ArrayList<>();
		createTotalPolesList();

		// Intermediate poles rotation
		line = new Polyline();
		line.setFollowTerrain(true);
		computeLineForIntPoles();

	}

	/**
	 * Display total poles of rotation
	 *
	 * @param display
	 */
	public void displayTotalPoles(boolean display) {

		if (display) {
			// remove all renderables
			this.setRenderables(null);
			for (PointPlacemark pp : totalPointList) {

				this.addRenderable(pp);
			}
		} else {
			for (PointPlacemark pp : totalPointList) {
				this.removeRenderable(pp);
			}
		}

	}

	/**
	 * Display intermediate poles of rotation
	 *
	 * @param display
	 */
	public void displayIntermediatePoles(boolean display) {
		if (display) {
			// remove all renderables
			this.setRenderables(null);

			this.addRenderable(line);
		} else {
			this.removeRenderable(line);
		}
	}

	public LatLon getLatlonOri() {
		return latlonOri;
	}

	public void setLatlonOri(LatLon latlonOri) {
		this.latlonOri = latlonOri;
	}

	/**
	 * Create the list of total poles of rotation.
	 */
	private void createTotalPolesList() {
		PoleRotations totPoleRot = movement.getTotalPoleRotation();
		for (int i = 0; i < totPoleRot.getAngles().size(); i++) {

			LatLon latlon = new LatLon(Angle.fromDegrees(totPoleRot.getLatitudes().get(i)),
					Angle.fromDegrees(totPoleRot.getLongitudes().get(i)));
			Position position = new Position(latlon, 0);
			PointPlacemark pp = new PointPlacemark(position);
			pp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
			pp.setLineEnabled(true);
			pp.setValue(AVKey.LAYER, this);
			PointPlacemarkAttributes pointAttribute = new PointPlacemarkAttributes();
			pointAttribute.setUsePointAsDefaultImage(true);
			pp.setAttributes(pointAttribute);

			totalPointList.add(pp);
		}
	}

	/**
	 * Compute the polyline of the intermediate poles of rotation.
	 */
	public void computeLineForIntPoles() {
		PoleRotations intPoleRot = movement.getPoleRotation();
		LatLon latlonTmp = new LatLon(latlonOri);

		// The list of latlon for the polyline
		List<LatLon> latlonList = new ArrayList<>();
		latlonList.add(latlonTmp);

		for (int i = 0; i < intPoleRot.getDates().size(); i++) {

			// convert latlon geoG in geoC
			double[] coordOri = TectonicUtils.ptGeoGtoGeoC(latlonTmp);
			// rotate the point with the matrix
			double[] coordRotated = TectonicUtils.performesRotation(coordOri, intPoleRot.getMatrix().get(i), false);
			// convert coord geoC in latlon geoG
			LatLon latlon = TectonicUtils.ptGeoCtoGeoG(coordRotated);

			latlonList.add(latlon);
			latlonTmp = latlon;
		}

		// Set the latlon to the polyline
		line.setPositions(latlonList, 0);
	}

	/** Override AbstractLayer Method. */
	@Override
	public void preRender(DrawContext dc) {
		int mode = TectonicDataset.getInstance().getConfiguration().getDisplayPoleMode();
		if (mode == 0 || mode == 1) {
			if (!this.isEnabled() && mode != 1) {
				// with mode 1
				return; // Don't check for arg errors if we're disabled
			}

			if (null == dc) {
				String message = Logging.getMessage("nullValue.DrawContextIsNull");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (null == dc.getGlobe()) {
				String message = Logging.getMessage("layers.AbstractLayer.NoGlobeSpecifiedInDrawingContext");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (null == dc.getView()) {
				String message = Logging.getMessage("layers.AbstractLayer.NoViewSpecifiedInDrawingContext");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (!this.isLayerActive(dc)) {
				return;
			}

			if (!this.isLayerInView(dc)) {
				return;
			}

			this.doPreRender(dc);
		}

	}

	/** Override AbstractLayer Method. */
	@Override
	public void render(DrawContext dc) {
		int mode = TectonicDataset.getInstance().getConfiguration().getDisplayPoleMode();
		if (mode == 0 || mode == 1) {
			if (!this.isEnabled() && mode != 1) {
				// with mode 1
				return; // Don't check for arg errors if we're disabled
			}

			if (null == dc) {
				String message = Logging.getMessage("nullValue.DrawContextIsNull");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (null == dc.getGlobe()) {
				String message = Logging.getMessage("layers.AbstractLayer.NoGlobeSpecifiedInDrawingContext");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (null == dc.getView()) {
				String message = Logging.getMessage("layers.AbstractLayer.NoViewSpecifiedInDrawingContext");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (!this.isLayerActive(dc)) {
				return;
			}

			if (!this.isLayerInView(dc)) {
				return;
			}

			this.doRender(dc);
		}

	}

	/** Override AbstractLayer Method. */
	@Override
	public void pick(DrawContext dc, java.awt.Point point) {
		int mode = TectonicDataset.getInstance().getConfiguration().getDisplayPoleMode();
		if (mode == 0 || mode == 1) {
			if (!this.isEnabled() && mode != 1) {
				// with mode 1
				return; // Don't check for arg errors if we're disabled
			}

			if (null == dc) {
				String message = Logging.getMessage("nullValue.DrawContextIsNull");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (null == dc.getGlobe()) {
				String message = Logging.getMessage("layers.AbstractLayer.NoGlobeSpecifiedInDrawingContext");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (null == dc.getView()) {
				String message = Logging.getMessage("layers.AbstractLayer.NoViewSpecifiedInDrawingContext");
				Logging.logger().severe(message);
				throw new IllegalStateException(message);
			}

			if (!this.isLayerActive(dc)) {
				return;
			}

			if (!this.isLayerInView(dc)) {
				return;
			}

			this.doPick(dc, point);
		}
	}

	/** Get the color of poles of rotation */
	public Color getColor() {
		return getLayerParameters().getColor();
	}

	/** Set the color of poles of rotation */
	public void setColor(Color color) {
		for (PointPlacemark pp : totalPointList) {
			pp.getAttributes().setLineMaterial(new Material(color));
		}
		line.setColor(color);
		getLayerParameters().setColor(color);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage("icons/16/poles.png", getClass()));
	}

	@Override
	public PoleRotationLayerParameter getLayerParameters() {
		return layerparameters;
	}

	@Override
	public void setLayerParameters(LayerParameters arg) {
		if (arg instanceof PoleRotationLayerParameter) {
			layerparameters.load((PoleRotationLayerParameter) arg);
			setColor(layerparameters.getColor());
		}
	}
}
