package fr.ifremer.globe.placa3d.layers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;

import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.placa3d.render.TectonicRemanentRenderer;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTextureTile;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.PerformanceStatistic;

/***
 * Remanent layer displays previous position of the ShaderElevationLayer defining the plate Number of remanent layer for
 * a same parent layer is defined by the remanence depth field in TectonicPlayer
 * 
 * @author MORVAN
 * 
 */
public class TectonicRemanentLayer extends ShaderElevationLayer {
	protected Logger logger = LoggerFactory.getLogger(TectonicRemanentLayer.class);
	private int dateIndex = 0;
	private TectonicRemanentRenderer render = new TectonicRemanentRenderer(this);
	private ShaderElevationLayer parent = null;
	private static Map<GL, Integer> shaderprograms = new HashMap<>(4);
	private TectonicDefinition plate = null;

	public TectonicRemanentLayer(ShaderElevationLayer parent, TectonicDefinition plate, int dateIndex) {
		super(parent);
		this.dateIndex = dateIndex;
		this.parent = parent;
		this.plate = plate;

		vertex_shader = "/shader/tectonicShader.vert";
		fragment_shader = "/shader/tectonicShader.frag";

		// Constrast and Transparency have to be computed without considering the offset
		getContrastControler().getModel().setLayerValueWithOffset(false);
	}

	@Override
	public boolean isInitialized() {
		return this.isInitialized;
	}

	@Override
	public void initialize(DrawContext dc) {
		// Création Shader
		try {
			GL2 gl = dc.getGL().getGL2();
			if (!shaderprograms.containsKey(gl)) {
				shaderprograms.put(gl, ShaderUtil.createShader(this, gl, vertex_shader, fragment_shader));
			}
		} catch (GIOException e) {
			logger.error("Error reading fragment shader : ", e);
		}

		// init sector
		this.sector = getSector();

		this.isInitialized = true;
	}

	@Override
	public void render(DrawContext dc) {
		// check if parent layer is enabled
		if (parent.isEnabled()) {

			if (!this.isInitialized()) {
				this.initialize(dc);
			}
//
//			if (this.sector != null && this.sector.intersects(dc.getVisibleSector())) {
//				setValue(SSVAVKey.CURRENTLY_VISIBLE, true);
//			} else {
//				setValue(SSVAVKey.CURRENTLY_VISIBLE, false);
//			}

			GL2 gl = dc.getGL().getGL2();
			if (!shaderprograms.containsKey(gl)) {
				initialize(dc);
			}
			int shaderprgm = shaderprograms.get(gl);
			if (shaderprgm != 0) {
				render.setShaderProgram(shaderprgm);

				// date index on 255 scale for color map
				render.setDateIndex(dateIndex);
				render.setDateIndexmax(plate.getPlayer().getMaxDateIndex());
				// opacity is defined by parent opacity
				// remanent layer is not displayed in layer tree
				render.setOpacity(parent.getOpacity());
			}
			super.render(dc);
		}
	}

	@Override
	protected void draw(DrawContext dc) {
		this.checkResources(); // Check whether resources are present, and
		// perform any necessary initialization.

		this.assembleTiles(dc); // Determine the tiles to draw.

		if (this.currentTiles.size() >= 1) {
			if (this.getScreenCredit() != null) {
				dc.addScreenCredit(this.getScreenCredit());
			}

			MyTextureTile[] sortedTiles = new MyTextureTile[this.currentTiles.size()];
			sortedTiles = this.currentTiles.toArray(sortedTiles);
			Arrays.sort(sortedTiles, levelComparer);

			GL2 gl = dc.getGL().getGL2();

			if (this.isUseTransparentTextures() || this.getOpacity() < 1) {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT | GL2.GL_CURRENT_BIT);
				this.setBlendingFunction(dc);
			} else {
				gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL2.GL_POLYGON_BIT);
			}

			gl.glPolygonMode(GL.GL_FRONT, GL2GL3.GL_FILL);
			gl.glEnable(GL.GL_CULL_FACE);
			gl.glCullFace(GL.GL_BACK);

			dc.setPerFrameStatistic(PerformanceStatistic.IMAGE_TILE_COUNT, this.tileCountName,
					this.currentTiles.size());

			// render.setLevel(sortedTiles[0].getLevel().getLevelNumber());
			render.renderTiles(dc, this.currentTiles);

			gl.glPopAttrib();

			if (this.drawTileIDs) {
				this.drawTileIDs(dc, this.currentTiles);
			}

			if (this.drawBoundingVolumes) {
				this.drawBoundingVolumes(dc, this.currentTiles);
			}

			// Check texture expiration. Memory-cached textures are checked for
			// expiration only when an explicit,
			// non-zero expiry time has been set for the layer. If none has been
			// set, the expiry times of the layer's
			// individual levels are used, but only for images in the local file
			// cache, not textures in memory. This is
			// to avoid incurring the overhead of checking expiration of
			// in-memory textures, a very rarely used feature.
			if (this.getExpiryTime() > 0 && this.getExpiryTime() < System.currentTimeMillis()) {
				this.checkTextureExpiration(dc, this.currentTiles);
			}

			this.currentTiles.clear();
		}

		this.sendRequests();
		this.requestQ.clear();
	}

	// /** Only returns a buffered reader from a filename in this classpath. */
	// private BufferedReader getShaderReader(final String shaderFile) {
	// BufferedReader brv;
	// InputStream is =
	// TectonicRemanentLayer.class.getResourceAsStream(shaderFile);
	// if (is == null) {
	// Logging.logger().severe("Cannot read shader file " + shaderFile);
	// return null;
	// }
	// brv = new BufferedReader(new InputStreamReader(is));
	// return brv;
	// }

	@Override
	public Sector move(Sector st) {
		if (movement != null) {
			if (movement instanceof TectonicMovement) {
				st = ((TectonicMovement) movement).move(st, dateIndex);
			} else {
				st = movement.move(st);
			}
		}

		return st;
	}

	@Override
	public LatLon move(LatLon latLon) {
		if (movement != null) {
			if (movement instanceof TectonicMovement) {
				return ((TectonicMovement) movement).move(latLon, dateIndex);
			} else {
				return movement.move(latLon);
			}
		} else {
			return latLon;
		}
	}

	@Override
	public Sector moveInv(Sector st) {
		if (movement != null) {
			if (movement instanceof TectonicMovement) {
				return ((TectonicMovement) movement).moveInv(st, dateIndex);
			} else {
				return movement.moveInv(st);
			}
		} else {
			return st;
		}
	}

	@Override
	public LatLon moveInv(LatLon latLon) {
		if (movement != null) {
			if (movement instanceof TectonicMovement) {
				return ((TectonicMovement) movement).moveInv(latLon, dateIndex);
			} else {
				return movement.moveInv(latLon);
			}
		} else {
			return latLon;
		}
	}
}
