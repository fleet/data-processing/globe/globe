package fr.ifremer.globe.placa3d.layers;

import java.io.File;

import org.w3c.dom.Document;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.drivers.tile.TileProductionParametersFactory;
import fr.ifremer.globe.placa3d.render.TecShaderSurfaceTileRenderer;
import fr.ifremer.globe.ui.utils.CacheDirectory;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTextureTile;
import fr.ifremer.viewer3d.layers.deprecated.dtm.ShaderElevationLayer;
import fr.ifremer.viewer3d.render.ShaderSurfaceTileRenderer;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.LevelSet;
import gov.nasa.worldwind.util.Tile;

/**
 * This layer adds functionnality for masking grids rendering. The masking grid is a double[][] array containing ages
 * when a sector becomes visible. These data are blended with texture data to render transparent sectors if they are not
 * visible yet.
 * 
 * @author apside
 */
public class TecShaderElevationLayer extends ShaderElevationLayer {

	protected TectonicModel tectonicModel;
	protected TectonicPlateModel plate;

	/**
	 * Constructor.
	 * 
	 * @param plate
	 * @param tectonicModel
	 * @param inputFile
	 * @param levelset
	 */
	public TecShaderElevationLayer(TectonicModel tectonicModel, TectonicPlateModel plate, File inputFile,
			LevelSet levelset) {
		super(inputFile, levelset);
		init(tectonicModel, plate, levelset);
	}

	/**
	 * Constructor.
	 */
	public TecShaderElevationLayer(TectonicModel tectonicModel, TectonicPlateModel plate, File sourceFile,
			Document document, AVList parameters, String type) {
		super(sourceFile, document, parameters, type);
		init(tectonicModel, plate, parameters);
	}

	private void init(TectonicModel tectonicModel, TectonicPlateModel plate, AVList params) {
		this.tectonicModel = tectonicModel;
		this.plate = plate;

		// Constrast and Transparency have to be computed without considering the offset
		getContrastControler().getModel().setLayerValueWithOffset(false);

		// Compute the data cache path without the age interval
		String fileStore = CacheDirectory.getCacheSubDirectory();
		String cacheDirPrefix = params.getStringValue(TileProductionParametersFactory.PROPERTY_DATA_CACHE_NAME_PREFIX);
		setValue(TileProductionParametersFactory.PROPERTY_DATA_CACHE_NAME_PREFIX, fileStore + "/" + cacheDirPrefix);
	}

	@Override
	public TecShaderSurfaceTileRenderer getRender() {
		return (TecShaderSurfaceTileRenderer) renderer;
	}

	public TectonicModel getTectonicModel() {
		return tectonicModel;
	}

	public TectonicPlateModel getPlate() {
		return plate;
	}

	@Override
	protected ShaderSurfaceTileRenderer createSurfaceTileRenderer() {
		return new TecShaderSurfaceTileRenderer(this);
	}

	@Override
	protected RequestTask createRequestTask(MyTextureTile tile) {
		return new TecRequestTask(tile, this);
	}

	@Override
	protected void sendRequests() {
		Runnable task = this.requestQ.poll();
		while (task != null) {
			task.run();
			task = this.requestQ.poll();
		}
	}

	/**
	 * Determine the tile's path, according to the layer's specific naming conventions.
	 * 
	 * @param tile Tile whose path is to be computed.
	 * @return The tile's path computed by the layer's naming conventions.
	 */
	public String getTilePath(MyTextureTile tile) {
		String cachePathPrefix = getStringValue(TileProductionParametersFactory.PROPERTY_DATA_CACHE_NAME_PREFIX);
		Level lvl = tile.getLevel();
		String sRow = String.valueOf(tile.getRow());
		String sCol = String.valueOf(tile.getColumn());

		String tilePath = null;
		if (Activator.getPreferences().getMorphing().getValue().booleanValue()) {
			tilePath = cachePathPrefix + "_" + ((int) Math.ceil(tectonicModel.getAge())) + "/" + lvl.getLevelNumber()
					+ "/" + sRow + "/" + sRow + "_" + sCol;
			if (!lvl.isEmpty()) {
				tilePath += lvl.getFormatSuffix();
			}
			if (getDataFileStore().findFile(tilePath, false) == null) {
				tilePath = null;
			}
		}
		if (tilePath == null) {
			String sInterval = plate.computeAgeLayerIntervalStringForAge(tectonicModel.getAge());
			tilePath = cachePathPrefix + "_" + sInterval + "/" + lvl.getLevelNumber() + "/" + sRow + "/" + sRow + "_"
					+ sCol;
			if (!lvl.isEmpty()) {
				tilePath += lvl.getFormatSuffix();
			}
		}

		return tilePath;
	}

	/**
	 * Specific RequestTask to adapt the tiles' path according to the tectonic age. Usually, tiles' path depends on
	 * their level's path on disk but this level's path is immutable. As tectonic concerns, we have decided to have one
	 * PNG cached image by tectonic era/period. But this requires that tiles' path is updated according to this naming
	 * conventions and according to the current tectonic age.
	 */
	protected class TecRequestTask extends RequestTask {

		protected TecRequestTask(MyTextureTile tile, TecShaderElevationLayer layer) {
			super(tile, layer);
		}

		@Override
		public void run() {
			boolean isBilFileFormat = this.layer.getLevels().getFirstLevel().getFormatSuffix().equals(".bil");
			if (!isBilFileFormat) {
				// Compute the new tile path according to the tectonic age
				String tilePath = getTilePath(tile);
				PrivateAccessor.setField(tile, "path", Tile.class, tilePath);
			}
			super.run();
		}
	}

	/** Specialize the shader for Placa */
	@Override
	protected String getFragmentShaderName() {
		return "/shader/tecShaderElevationLayer.frag";
	}

	@Override
	public void listerColorMap(boolean useFullList) {
		super.listerColorMap(useFullList);
		setMinTransparency(-10000d);
		setMaxTransparency(10000d);
	}

}
