package fr.ifremer.globe.placa3d.layers;

import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import fr.ifremer.globe.placa3d.data.TectonicGrid;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.util.ImageUtil;

/**
 * Apply graphical pixels masking on a raster thanks to a IMaskingGrid data.
 */
public class MaskingGridRenderer {

	protected TectonicGrid grid;

	protected BufferedImageRaster raster;
	protected int rasterWidth;
	protected int rasterHeight;
	protected int rasterAgeIndex;

	/**
	 * Constructor.
	 * 
	 * @param grid
	 *            The masking grid.
	 */
	public MaskingGridRenderer(TectonicGrid grid) {
		this.grid = grid;
	}

	public DataRaster render(DataRaster raster) {

		if (rasterAgeIndex != grid.getMovement().getDateIndex()) {
			raster = computeCurrentRaster(rasterWidth, rasterHeight);
			rasterAgeIndex = grid.getMovement().getDateIndex();
		}

		this.raster.drawOnTo(raster);

		return this.raster;
	}

	protected DataRaster computeCurrentRaster(int width, int height) {

		BufferedImage image = ImageUtil.createCompatibleImage(width, height, Transparency.BITMASK);
		WritableRaster output = image.getRaster();

		int age = this.grid.getMovement().getDateIndex();

		int visibilityColor = 255 << 24;

		int[] newArray = new int[width * height];

		double[][] data = grid.getData();

		for (int x = 0; x < data.length; x++) {
			double[] line = data[x];
			for (int y = 0; y < line.length; y++) {
				if (age >= line[y]) {
					newArray[x * width + y] = visibilityColor;
				}
			}
		}
		output.setPixels(0, 0, width, height, newArray);

		AVList params = new AVListImpl();
		params.setValue(AVKey.DATA_TYPE, AVKey.INT32);
		image = ImageUtil.toCompatibleImage(image);
		DataRaster result = BufferedImageRaster.wrap(image, params);
		return result;
	}

}
