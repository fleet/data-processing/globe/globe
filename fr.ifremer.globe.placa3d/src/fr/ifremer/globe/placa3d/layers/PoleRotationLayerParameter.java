package fr.ifremer.globe.placa3d.layers;

import java.awt.Color;

import fr.ifremer.viewer3d.layers.LayerParameters.AbstractLayerParameters;

public class PoleRotationLayerParameter extends AbstractLayerParameters {

	/**
	 * color of the displayed poles
	 */
	private Color color;

	PoleRotationLayerParameter() {
		super();
		color = new Color(255, 255, 255);
	}

	public void load(PoleRotationLayerParameter other) {
		color = other.color;
		visibility = other.visibility;
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		pcs.firePropertyChange("color", this.color, this.color = color);
	}

}
