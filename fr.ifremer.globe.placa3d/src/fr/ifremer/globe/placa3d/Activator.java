package fr.ifremer.globe.placa3d;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.placa3d.preferences.Placa3dPreferences;

public class Activator extends Plugin {

	// The shared instance
	private static Activator INSTANCE;

	private static BundleContext context;

	private static Placa3dPreferences param;

	/**
	 * @return The plugin's preferences.
	 */
	public static Placa3dPreferences getPreferences() {
		return param;
	}

	public static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext )
	 */
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		// create plugin preferences
		PreferenceComposite root = PreferenceRegistry.getInstance().getRootNode();
		param = new Placa3dPreferences(root, "Placa");
		INSTANCE = this;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
		INSTANCE = null;
	}

	/**
	 * @return the {@link #iNSTANCE}
	 */
	public static Activator getInstance() {
		return INSTANCE;
	}

	public static BundleContext getBundleContext() {
		return context;
	}

}
