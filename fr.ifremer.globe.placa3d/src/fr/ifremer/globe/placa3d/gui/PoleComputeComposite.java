package fr.ifremer.globe.placa3d.gui;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.composite.PoleRotationComposite;
import fr.ifremer.globe.placa3d.file.totpol.TotalPoleFile;
import fr.ifremer.globe.placa3d.store.LayerStore;
import fr.ifremer.globe.placa3d.tectonic.PoleComputeModel;
import fr.ifremer.globe.placa3d.tectonic.Rotation;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.placa3d.tectonic.TectonicPlayerModel;
import fr.ifremer.globe.placa3d.util.MagneticAnomalies;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;

public class PoleComputeComposite extends Composite implements MouseListener , PropertyChangeListener {
	protected Logger logger = LoggerFactory.getLogger(PoleComputeComposite.class);

	/**
	 * The model of this composite
	 */
	PoleComputeModel model;

	/**
	 * The player of the moved plate
	 */
	private TectonicPlayerModel player;

	private static final String PLAYER_GROUP_NAME = "Age of moved plate selection";
	private static final String MAX_INDEX_TEXT = "Last image index";
	private static final String CURRENT_INDEX_TEXT = "Current image index";
	private static final String RESET = "Reset";
	private static final String MIN_INDEX_TEXT = "First image index";

	private Spinner minIndexSpinner;
	private Spinner currentIndexSpinner;
	private Spinner maxIndexSpinner;
	private Scale indexSlider;

	/**
	 * The layer which displays the points of start/end rotation/translation.
	 */
	RenderableLayer ppLayer;
	/** The translation start point placemark. */
	PointPlacemark translationStartPp;
	/** The translation end point placemark. */
	PointPlacemark translationEndPp;
	/** The rotation center point placemark. */
	PointPlacemark rotationCenterPp;
	/** The rotation start point placemark. */
	PointPlacemark rotationStartPp;
	/** The rotation end point placemark. */
	PointPlacemark rotationEndPp;

	/** Show the message when a button il selected. */
	boolean showMessage = true;

	/**
	 * Type of pressed button waiting a click of mouse on globe :
	 * 
	 * 0 : translation - start
	 * 1 : translation - end
	 * 2 : rotation - center
	 * 3 : rotation - start
	 * 4 : rotation - end
	 * 
	 */
	private int pointType;

	/** The composite which display the list of computed poles. */
	Composite poleListComposite;
	/** The scrolled composite which displays the composite which display the list of computed poles. */
	ScrolledComposite poleListScrolledComposite;


	/** Int Format. */
	public final static String[] FORMAT_NAMES = new String[] { "Total poles" };
	public final static String[] FORMAT_EXTENSIONS = new String[] { "*.tot" };

	public PoleComputeComposite(Composite parent, int style) {
		super(parent, style);

		model = new PoleComputeModel();

		// initialize the layer which displays the point pointPlacemarks
		initPointPlacemarkLayer();

		// initialize the swt widgets
		initIHM();
	}

	/**
	 * Initialize the layer and his pointPlacemarks
	 */
	private void initPointPlacemarkLayer() {
		ppLayer = new RenderableLayer();
		IGeographicViewService.grab().addLayerBeforeCompass(ppLayer);

		// Default PointPlacemarkAttributes
		PointPlacemarkAttributes pointAttribute = new PointPlacemarkAttributes();
		pointAttribute.setUsePointAsDefaultImage(true);

		// PointPlacemark Translation Start
		translationStartPp = new PointPlacemark(new Position(Angle.ZERO, Angle.ZERO, 0));		
		translationStartPp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		translationStartPp.setLineEnabled(true);
		translationStartPp.setValue(AVKey.LAYER, this);
		translationStartPp.setAttributes(pointAttribute);
		translationStartPp.setVisible(false);
		ppLayer.addRenderable(translationStartPp);

		// PointPlacemark Translation End
		translationEndPp = new PointPlacemark(new Position(Angle.ZERO, Angle.ZERO, 0));		
		translationEndPp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		translationEndPp.setLineEnabled(true);
		translationEndPp.setValue(AVKey.LAYER, this);
		translationEndPp.setAttributes(pointAttribute);
		translationEndPp.setVisible(false);
		ppLayer.addRenderable(translationEndPp);

		// PointPlacemark Rotation Center
		rotationCenterPp = new PointPlacemark(new Position(Angle.ZERO, Angle.ZERO, 0));		
		rotationCenterPp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		rotationCenterPp.setLineEnabled(true);
		rotationCenterPp.setValue(AVKey.LAYER, this);
		rotationCenterPp.setAttributes(pointAttribute);
		rotationCenterPp.setVisible(false);
		ppLayer.addRenderable(rotationCenterPp);

		// PointPlacemark Rotation Start
		rotationStartPp = new PointPlacemark(new Position(Angle.ZERO, Angle.ZERO, 0));		
		rotationStartPp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		rotationStartPp.setLineEnabled(true);
		rotationStartPp.setValue(AVKey.LAYER, this);
		rotationStartPp.setAttributes(pointAttribute);
		rotationStartPp.setVisible(false);
		ppLayer.addRenderable(rotationStartPp);

		// PointPlacemark Rotation End
		rotationEndPp = new PointPlacemark(new Position(Angle.ZERO, Angle.ZERO, 0));		
		rotationEndPp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
		rotationEndPp.setLineEnabled(true);
		rotationEndPp.setValue(AVKey.LAYER, this);
		rotationEndPp.setAttributes(pointAttribute);
		rotationEndPp.setVisible(false);
		ppLayer.addRenderable(rotationEndPp);


	}
	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void initIHM() {

		// create the selection plate group
		createSelectionPlaqueGroup();

		// create the selection point group
		createSelectionPointsAndApplyGroup();

		// create pole list group
		createPoleListGroup();

		// create save pole group
		createSavePoleGroup();

	}

	private void createSelectionPlaqueGroup() {
		setLayout(new GridLayout(1, false));
		Group selectionPlaqueGroup = new Group(this, SWT.NONE);
		selectionPlaqueGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		selectionPlaqueGroup.setText("Fixed plate / moved plate selection");
		selectionPlaqueGroup.setLayout(new GridLayout(2, false));

		Label plaqueFixeLabel = new Label(selectionPlaqueGroup, SWT.NONE);
		plaqueFixeLabel.setText("Fixed plate");

		final Combo plaqueFixeCombo = new Combo(selectionPlaqueGroup, SWT.READ_ONLY);
		plaqueFixeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		plaqueFixeCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				TectonicDefinition fixedTectonicDefinition = TectonicDataset.getInstance().getPlates().get(plaqueFixeCombo.getSelectionIndex());
				model.setFixedTectonicDefinition(fixedTectonicDefinition);

				// only one plate could be the reference
				for (TectonicDefinition td : TectonicDataset.getInstance().getPlates()) {
					td.getPlayer().setReferentiel(false);
				}

				// met a jour les bouton referential  des PoleRotationComposite de la parameter View s'il existant
				for (PoleRotationComposite composite : PoleRotationComposite.getPlayersList()) {
					composite.getReferentielButton().setSelection(false);	
				}

				fixedTectonicDefinition.getPlayer().setReferentiel(true);	
			}

		});


		Label plaqueMobileLabel = new Label(selectionPlaqueGroup, SWT.NONE);
		plaqueMobileLabel.setText("Moved plate");

		final Combo plaqueMobileCombo = new Combo(selectionPlaqueGroup, SWT.READ_ONLY);
		plaqueMobileCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		plaqueMobileCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				// remove the propertychangelistener of the old player if exists
				if (player != null) {
					player.removePropertyChangeListener(PoleComputeComposite.this);

					// Reset the offsetrotation
					model.getMovedTectonicDefinition().getPoleRotationLayer().getMovement().setOffsetMatrix(null);
				}


				TectonicDefinition movedTectonicDefinition = TectonicDataset.getInstance().getPlates().get(plaqueMobileCombo.getSelectionIndex());
				model.setMovedTectonicDefinition(movedTectonicDefinition);

				player = movedTectonicDefinition.getPlayer();
				player.addPropertyChangeListener(PoleComputeComposite.this);

				minIndexSpinner.setMinimum(player.getMinDateIndex());
				minIndexSpinner.setMaximum(player.getMaxDateIndex());
				minIndexSpinner.setSelection(player.getMinDateIndex());

				currentIndexSpinner.setEnabled(true);
				currentIndexSpinner.setMinimum(player.getMinDateIndex());
				currentIndexSpinner.setMaximum(player.getMaxDateIndex());
				currentIndexSpinner.setSelection(player.getDateIndex());

				maxIndexSpinner.setMinimum(player.getMinDateIndex());
				maxIndexSpinner.setMaximum(player.getMaxDateIndex());
				maxIndexSpinner.setSelection(player.getMaxDateIndex());

				indexSlider.setEnabled(true);
				indexSlider.setMinimum(player.getMinDateIndex());
				indexSlider.setMaximum(player.getMaxDateIndex());
				indexSlider.setSelection(player.getDateIndex());
				indexSlider.setPageIncrement((player.getMaxDateIndex() - player.getMinDateIndex()));

			}
		});

		// add pole files to combo
		for (TectonicDefinition tectonicDefinition : TectonicDataset.getInstance().getPlates()) {
			plaqueFixeCombo.add(tectonicDefinition.getPoleRotationLayer().getName());
			plaqueMobileCombo.add(tectonicDefinition.getPoleRotationLayer().getName());
		}



		// creating the player group composite
		createPlayerGroup(selectionPlaqueGroup);	
	}

	/**
	 * Create the player group of this composite.
	 */
	private Group createPlayerGroup(Composite parent) {
		// player group
		final Group playerGroup = new Group(parent, SWT.NONE);
		playerGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		playerGroup.setText(PLAYER_GROUP_NAME);
		playerGroup.setLayout(new GridLayout(4, true));


		minIndexSpinner = new Spinner(playerGroup, SWT.NONE);
		currentIndexSpinner = new Spinner(playerGroup, SWT.NONE);
		maxIndexSpinner = new Spinner(playerGroup, SWT.NONE);
		indexSlider = new Scale(playerGroup, SWT.NONE);

		// Menu reset for minSpinner
		final Menu resetMinMenu = new Menu(Display.getDefault().getActiveShell(), SWT.POP_UP);
		final MenuItem resetMinMenuItem = new MenuItem(resetMinMenu, SWT.PUSH);
		resetMinMenuItem.setText(RESET);
		resetMinMenuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				minIndexSpinner.setSelection(player.getMinDateIndex());
			}
		});

		minIndexSpinner.setToolTipText(MIN_INDEX_TEXT);
		minIndexSpinner.setEnabled(false);
		minIndexSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				player.setPlaying(false);
				player.setReversePlaying(false);
				player.setMinDateIndex(minIndexSpinner.getSelection());

				if (minIndexSpinner.getSelection() > indexSlider.getSelection()) {
					player.setDateIndex(minIndexSpinner.getSelection());
				}

				currentIndexSpinner.setMinimum(minIndexSpinner.getSelection());
				indexSlider.setMinimum(minIndexSpinner.getSelection());
			}
		});
		minIndexSpinner.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

		currentIndexSpinner.setToolTipText(CURRENT_INDEX_TEXT);
		currentIndexSpinner.setEnabled(false);
		currentIndexSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				player.setPlaying(false);
				player.setReversePlaying(false);
				player.setDateIndex(currentIndexSpinner.getSelection());
			}
		});
		GridData gd_currentIndexSpinner = new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1);
		gd_currentIndexSpinner.widthHint = 25;
		currentIndexSpinner.setLayoutData(gd_currentIndexSpinner);

		// Menu reset for maxSpinner
		final Menu resetMaxMenu = new Menu(Display.getDefault().getActiveShell(), SWT.POP_UP);
		final MenuItem resetMaxMenuItem = new MenuItem(resetMaxMenu, SWT.PUSH);
		resetMaxMenuItem.setText(RESET);
		resetMaxMenuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				maxIndexSpinner.setSelection(player.getMaxDateIndex());
			}
		});

		maxIndexSpinner.setToolTipText(MAX_INDEX_TEXT);
		maxIndexSpinner.setEnabled(false);
		maxIndexSpinner.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				player.setPlaying(false);
				player.setReversePlaying(false);
				player.setMaxDateIndex(maxIndexSpinner.getSelection());

				if (maxIndexSpinner.getSelection() < indexSlider.getSelection()) {
					player.setDateIndex(maxIndexSpinner.getSelection());
				}
				currentIndexSpinner.setMaximum(maxIndexSpinner.getSelection());
				indexSlider.setMaximum(maxIndexSpinner.getSelection());
			}
		});
		maxIndexSpinner.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

		indexSlider.setEnabled(false);
		indexSlider.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				player.setPlaying(false);
				player.setReversePlaying(false);
				player.setDateIndex(indexSlider.getSelection());
			}
		});
		indexSlider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		return playerGroup;
	}

	private void createSelectionPointsAndApplyGroup() {

		Group selectionPointsAndApplyGroup = new Group(this, SWT.NONE);
		selectionPointsAndApplyGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		selectionPointsAndApplyGroup.setText("Points selection");
		selectionPointsAndApplyGroup.setLayout(new GridLayout(4, false));

		final Button showMessageCheckButton = new Button(selectionPointsAndApplyGroup, SWT.CHECK);
		showMessageCheckButton.setToolTipText("Show message when a button is selected");
		showMessageCheckButton.setSelection(true);
		showMessageCheckButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				showMessage = showMessageCheckButton.getSelection();
			}
		});
		showMessageCheckButton.setText("Show message");
		new Label(selectionPointsAndApplyGroup, SWT.NONE);
		new Label(selectionPointsAndApplyGroup, SWT.NONE);
		new Label(selectionPointsAndApplyGroup, SWT.NONE);

		Label translationLabel = new Label(selectionPointsAndApplyGroup, SWT.NONE);
		translationLabel.setText("Translation : ");
		new Label(selectionPointsAndApplyGroup, SWT.NONE);
		new Label(selectionPointsAndApplyGroup, SWT.NONE);
		new Label(selectionPointsAndApplyGroup, SWT.NONE);

		// Translation Start Button
		Button translationStartButton = new Button(selectionPointsAndApplyGroup, SWT.NONE);
		translationStartButton.setText("Start point");
		translationStartButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (showMessage) {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Translation Start Location",
							"Right click on the globe to select the latitude and longitude of the start location of translation.");
				}
				((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

				// Add mouse listener to select a position on globe with
				// mousePressed()
				Viewer3D.getWwd().getInputHandler().addMouseListener(PoleComputeComposite.this);

				// translation - start
				pointType = 0;
			}
		});


		// Translation End Button
		Button translationEndButton = new Button(selectionPointsAndApplyGroup, SWT.NONE);
		translationEndButton.setText("End point");
		translationEndButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (showMessage) {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Translation End Location",
							"Right click on the globe to select the latitude and longitude of the end location of translation.");
				}
				((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

				// Add mouse listener to select a position on globe with
				// mousePressed()
				Viewer3D.getWwd().getInputHandler().addMouseListener(PoleComputeComposite.this);

				// translation - end
				pointType = 1;
			}
		});
		new Label(selectionPointsAndApplyGroup, SWT.NONE);

		// Translation Apply Button
		Button translationApplyButton = new Button(selectionPointsAndApplyGroup, SWT.NONE);
		translationApplyButton.setText("Apply");
		translationApplyButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {

				if (model.getFixedTectonicDefinition() == null || model.getMovedTectonicDefinition() == null) {
					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Apply problem",
							"Select a fixed plate and a moved plate.");
				} else if ((model.getTranslationStartLatlon() ==null || model.getTranslationEndLatlon() == null)) {
					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Apply problem",
							"Select start and end of translation.");	
				} else {
					Rotation computedRotation = TectonicUtils.fitTrans(model.getTranslationStartLatlon(), model.getTranslationEndLatlon());
					// Add the computed rotation to the model
					model.getRotationList().add(computedRotation);

					model.computeAndApplyPoleRotation();

					// Refresh the pole list composite
					refreshPoleListComposite();

					//Refresh the globe
					Viewer3D.getWwd().redraw();
				}
			}
		});


		Label rotationLabel = new Label(selectionPointsAndApplyGroup, SWT.NONE);
		rotationLabel.setText("Rotation : ");
		new Label(selectionPointsAndApplyGroup, SWT.NONE);
		new Label(selectionPointsAndApplyGroup, SWT.NONE);
		new Label(selectionPointsAndApplyGroup, SWT.NONE);

		// Rotation Center Button
		Button rotationCenterButton = new Button(selectionPointsAndApplyGroup, SWT.NONE);
		rotationCenterButton.setText("Rotation center");
		rotationCenterButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (showMessage) {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Rotation Center Location",
							"Right click on the globe to select the latitude and longitude of the center location of rotation.");
				}
				((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

				// Add mouse listener to select a position on globe with
				// mousePressed()
				Viewer3D.getWwd().getInputHandler().addMouseListener(PoleComputeComposite.this);

				// rotation - center
				pointType = 2;
			}
		});


		// Rotation Start Button
		Button rotationStartButton = new Button(selectionPointsAndApplyGroup, SWT.NONE);
		rotationStartButton.setText("Start point");
		rotationStartButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (showMessage) {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Rotation Start Location",
							"Right click on the globe to select the latitude and longitude of the start location of rotation.");
				}
				((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

				// Add mouse listener to select a position on globe with
				// mousePressed()
				Viewer3D.getWwd().getInputHandler().addMouseListener(PoleComputeComposite.this);

				//rotation - start
				pointType = 3;
			}
		});


		// Rotation End Button
		Button rotationEndButton = new Button(selectionPointsAndApplyGroup, SWT.NONE);
		rotationEndButton.setText("End point");
		rotationEndButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (showMessage) {
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Rotation End Location",
							"Right click on the globe to select the latitude and longitude of the end location of rotation.");
				}
				((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

				// Add mouse listener to select a position on globe with
				// mousePressed()
				Viewer3D.getWwd().getInputHandler().addMouseListener(PoleComputeComposite.this);

				//rotation - end
				pointType = 4;
			}
		});


		// Rotation Apply Button
		Button rotationApplyButton = new Button(selectionPointsAndApplyGroup, SWT.NONE);
		rotationApplyButton.setText("Apply");
		rotationApplyButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {

				if (model.getFixedTectonicDefinition() == null || model.getMovedTectonicDefinition() == null) {
					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Apply problem",
							"Select a fixed plate and a moved plate.");
				} else if ((model.getRotationCenterLatlon() ==null || model.getRotationStartLatlon() == null ||  model.getRotationEndLatlon() == null)) {
					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Apply problem",
							"Select center, start and end of rotation.");	
				} else {
					Rotation r = TectonicUtils.fitRot(model.getRotationCenterLatlon(), model.getRotationStartLatlon(),  model.getRotationEndLatlon());

					// Add the computed rotation to the model
					model.getRotationList().add(r);

					model.computeAndApplyPoleRotation();

					// Refresh the pole list composite
					refreshPoleListComposite();

					//Refresh the globe
					Viewer3D.getWwd().redraw();
				}
			}
		});

	}

	private void createPoleListGroup() {
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		Label lblListeDesPles = new Label(composite, SWT.CENTER);
		lblListeDesPles.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		lblListeDesPles.setText("List of computed poles");


		// ScrollComposite
		poleListScrolledComposite = new ScrolledComposite(composite, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData gd_poleListScrolledComposite = new GridData(GridData.FILL_BOTH);
		gd_poleListScrolledComposite.heightHint = 80;
		poleListScrolledComposite.setLayoutData(gd_poleListScrolledComposite);

		poleListComposite = new Composite(poleListScrolledComposite, SWT.BORDER);
		GridData gd_totComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_totComposite.heightHint = 107;
		poleListComposite.setLayoutData(gd_totComposite);
		poleListComposite.setLayout(new GridLayout(3, false));

		poleListScrolledComposite.setContent(poleListComposite);
		poleListScrolledComposite.setExpandHorizontal(true);
		poleListScrolledComposite.setExpandVertical(true);

		poleListScrolledComposite.setMinSize(poleListComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));


		Button undoButton = new Button(composite, SWT.NONE);
		undoButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		undoButton.setText("Undo");
		undoButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (!model.getRotationList().isEmpty()) {
					// Delete the last computed rotation of the model
					model.getRotationList().remove(model.getRotationList().size()-1);

					model.computeAndApplyPoleRotation();

					// Refresh the pole list composite
					refreshPoleListComposite();

					//Refresh the globe
					Viewer3D.getWwd().redraw();
				}				
			}
		});

	}



	private void createSavePoleGroup() {
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		composite.setLayout(new GridLayout(4, false));



		final Combo annoCombo = new Combo(composite, SWT.READ_ONLY);
		final Button	saveButton = new Button(composite, SWT.NONE);

		MagneticAnomalies ma = MagneticAnomalies.getInstance();
		for (String anno : ma.getAnomaliesTreeMap().values()) {
			annoCombo.add(anno);			
		}
		// Add the specific "new anno";
		annoCombo.add("New-anno");

		annoCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		annoCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				saveButton.setEnabled(true);	

			}
		});

		saveButton.setText("Save pole");
		saveButton.setEnabled(false);

		saveButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				FileDialog dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.SAVE);
				dialog.setFilterNames(PoleComputeComposite.FORMAT_NAMES);
				dialog.setFilterExtensions(PoleComputeComposite.FORMAT_EXTENSIONS);

				String filename = dialog.open();
				
				if (filename != null ) {
					if(model.getComputedRotation()==null) {
						Messages.openWarningMessage("Cannot save", "cannot save, not computed rotation (click on apply)");
	
					} else if(annoCombo.getText() == null){
						Messages.openWarningMessage("Cannot save", "cannot save, no anomaly setted");
				
					} else {
						String anno = annoCombo.getText(); //anno name
						Rotation rot = TectonicUtils.convertMatrixInRot( model.getMovedTectonicDefinition().getPoleRotationLayer().getMovement().getMovementInv(), 0, 0);
						
						File file = new File(filename);
						try {
							TotalPoleFile.insert(anno, rot, file);
						} catch (IOException e)
						{
							logger.warn("Error while saving tot file "+file.getName(),e);
							Messages.openErrorMessage("Error while saving tot file ", "Unexpected Error while saving tot file "+file.getName() +System.lineSeparator()+ e.getMessage());
						}
					}
				
				}

			}
		});





		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Button fixedPlateAboveButton = new Button(this, SWT.RADIO);
		fixedPlateAboveButton.setText("Fixed plate above moved plate");
		fixedPlateAboveButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TectonicDefinition fixedTd = model.getFixedTectonicDefinition();;
				if (fixedTd != null) {
					LayerList layerList = Viewer3D.getWwd().getModel().getLayers();
					if (model.getOriginalLayerList() == null) { // mémorise la layerlist original si pas encore modifiée
						model.setOriginalLayerList(layerList);
					}

					for (LayerStore ls :fixedTd.getLayerStoreList()) {
						for (Pair<Layer, ElevationModel> p : ls.getLayers()) {

							layerList.remove(p.getFirst());
							layerList.add(p.getFirst());
						}
					}

				}
			}
		});


		Button movedPlateAboveButton = new Button(this, SWT.RADIO);
		movedPlateAboveButton.setText("Moved plate above fixed plate");
		movedPlateAboveButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TectonicDefinition movedTd = model.getMovedTectonicDefinition();
				if (movedTd != null) {
					LayerList layerList = Viewer3D.getWwd().getModel().getLayers();
					if (model.getOriginalLayerList() == null) { // mémorise la layerlist original si pas encore modifiée
						model.setOriginalLayerList(layerList);
					}



					for (LayerStore ls :movedTd.getLayerStoreList()) {
						for (Pair<Layer, ElevationModel> p : ls.getLayers()) {

							layerList.remove(p.getFirst());
							layerList.add(p.getFirst());
						}
					}
				}
			}
		});

	}

	private void refreshPoleListComposite() {
		for (Control child : poleListComposite.getChildren()) {
			child.dispose();
		}
		for (Rotation rotation : model.getRotationList()) {
			Label rotLatLabel = new Label(poleListComposite, SWT.NONE);
			rotLatLabel.setText(rotation.getLatLon().getLatitude().toString());
			Label rotLonLabel = new Label(poleListComposite, SWT.NONE);
			rotLonLabel.setText(rotation.getLatLon().getLongitude().toString());
			Label angleLabel = new Label(poleListComposite, SWT.NONE);
			angleLabel.setText(String.valueOf(rotation.getAngle()));
		}

		poleListScrolledComposite.setMinSize(poleListComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		poleListComposite.layout();	
	}

	/**
	 * This method is called by the parent Dialog. 
	 */
	protected void close() {

		// dispose the layer displaying the points of start/end rotation/translation.
		ppLayer.dispose();

		// remove property change listener to avoid problem of sync
		if (player != null) {
			player.removePropertyChangeListener(PoleComputeComposite.this);

			// Reset the offset rotation
			model.getMovedTectonicDefinition().getPoleRotationLayer().getMovement().setOffsetMatrix(null);
		}


		if (model.getOriginalLayerList() != null) { // reset la layerlist original si pas encore modifiée (gestion dessus dessous)
			Viewer3D.getWwd().getModel().setLayers(model.getOriginalLayerList());
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent mouseEvent) {
		if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
			// Select a position on the globe
			Position currentPosition = Viewer3D.getWwd().getCurrentPosition();
			if (currentPosition != null) {
				switch (pointType) {
				case 0 : // 0 : translation - start
					model.setTranslationStartLatlon(new LatLon(currentPosition));
					translationStartPp.setPosition(currentPosition);
					translationStartPp.setVisible(true);

					break;

				case 1 : // 1 : translation - end
					model.setTranslationEndLatlon(new LatLon(currentPosition));
					translationEndPp.setPosition(currentPosition);
					translationEndPp.setVisible(true);
					break;

				case 2: // 2 : rotation - center
					model.setRotationCenterLatlon(new LatLon(currentPosition));
					rotationCenterPp.setPosition(currentPosition);
					rotationCenterPp.setVisible(true);
					break;

				case 3: // 3 : rotation - start
					model.setRotationStartLatlon(new LatLon(currentPosition));
					rotationStartPp.setPosition(currentPosition);
					rotationStartPp.setVisible(true);
					break;

				case 4: // 4 : rotation - end
					model.setRotationEndLatlon(new LatLon(currentPosition));
					rotationEndPp.setPosition(currentPosition);
					rotationEndPp.setVisible(true);
					break;

				default: 
					break;
				}
			}


			Viewer3D.getWwd().getInputHandler().removeMouseListener(this);
			((Component) Viewer3D.getWwd()).setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}


	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (TectonicPlayerModel.PROPERTY_DATEINDEX.equals(event.getPropertyName())) {
			final int imgIndex = (Integer) event.getNewValue();

			// Update components
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					if (currentIndexSpinner.getSelection() != imgIndex) { // fix
						// focus
						// problem
						currentIndexSpinner.setSelection(imgIndex);
					}
					indexSlider.setSelection(imgIndex);
				}
			});
		}

	}






}
