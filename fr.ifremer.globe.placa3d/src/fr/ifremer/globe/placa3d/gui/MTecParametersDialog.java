package fr.ifremer.globe.placa3d.gui;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.prefs.Preferences;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.file.mtec.MultiLayerTecFile;
import fr.ifremer.globe.placa3d.gui.model.Files;
import fr.ifremer.globe.placa3d.gui.model.Model;
import fr.ifremer.globe.utils.FileUtils;

/**
 * Class for .tec generator dialog box (inspired OtusParametersDialog class)
 *
 * @author D.riou
 */

public class MTecParametersDialog extends Dialog {

	// Preferences Constants
	private static final String _KeyPathCotFile = "KeyPathCotFile";
	private static final String _KeyPathRotationFile = "KeyPathRotationFile";
	private static final String _KeyPathElevFile = "KeyPathElevFile";
	private static final String _KeyPathTextureFile = "KeyPathTextureFile";
	private static final String _KeyPathTecFile = "KeyPathTecFile";
	private static final String _KeyPathAgeFile = "KeyPathAgeFile";

	private static final String _KeyModel = "_KeyModel";

	private static final int nbDigits = 2;

	// Preferences
	protected Preferences _preferences = Preferences.userNodeForPackage(MTecParametersDialog.class);
	/** Maximum age defined in preferences. */
	protected int maxAgeInPreference = Activator.getPreferences().getMaximumAge().getValue();
	/** Minimum age defined in preferences. */
	protected int minAgeInPreference = Activator.getPreferences().getMinimumAge().getValue();

	// Model
	protected Model model;
	protected Files currentFiles;
	protected int currentAgeIndex;

	// Graphical widgets
	protected Text tecFile;
	protected Text rotationFile;
	protected Text elevationFile;
	protected Text textureFile;
	protected Text textureElevation;
	protected Text ageFile;
	protected List cotFiles;

	protected Table ageTable;
	protected Spinner ageSpinner;
	protected Text ageReminder;

	protected IInputValidator integerValidator;

	protected String surveyPath; // ????????

	/**
	 * Constructor.
	 *
	 * @param shell parent shell.
	 */
	public MTecParametersDialog(Shell shell) {
		super(shell);
		restoreModel();
		this.integerValidator = getIntegerValidator();
	}

	/**
	 * Returns the dialog model.
	 *
	 * @return
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * Create an integer Validator.
	 *
	 * @return the integer Validator.
	 */
	private IInputValidator getIntegerValidator() {
		IInputValidator validator = new IInputValidator() {
			@Override
			public String isValid(String newText) {
				try {
					Integer.parseInt(newText);
				} catch (NumberFormatException ex) {
					return "The input must be a integer value";
				}
				return null;
			}
		};
		return validator;
	}

	/**
	 * @return Creates and returns a new number format used to represent ages.
	 */
	protected NumberFormat getAgeFormat() {
		DecimalFormat nf = new DecimalFormat("#,##");
		nf.setMinimumIntegerDigits(1);
		nf.setMinimumFractionDigits(nbDigits);
		return nf;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Multilayer TEC file creation wizard");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void create() {
		super.create();

		// Add the first date.
		if (model.ages.isEmpty()) {
			model.ages.clear();
			addNewDate(minAgeInPreference);
		} else {
			for (Pair<DoubleRange, Files> pair : model.ages) {
				TableItem item = new TableItem(ageTable, SWT.NONE);
				item.setText(0, getAgeFormat().format(pair.getFirst().getMinimumDouble()));
				item.setText(1, "OK");
			}
		}
		setCurrentAgeIndex(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		final Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		Composite globalGroup = new Composite(composite, SWT.BORDER);
		globalGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		globalGroup.setLayout(new GridLayout(2, false));

		Button clearButton = new Button(globalGroup, SWT.NONE);
		clearButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clear();
			}

		});
		clearButton.setText("clear all");

		Label lblEraseAllAges = new Label(globalGroup, SWT.NONE);
		lblEraseAllAges.setText("Erase all ages, rotation files, cot lines, ... ");

		// Create tot file group
		final Group rotationFileGroup = new Group(composite, SWT.NONE);
		rotationFileGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		rotationFileGroup.setText("Rotation file (*.tot | *.int | *.*)");
		rotationFileGroup.setLayout(new GridLayout(2, false));

		Composite ageAndFilesGroup = new Composite(composite, SWT.NONE);
		ageAndFilesGroup.setLayout(new GridLayout(2, false));
		ageAndFilesGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		final Group ageGroup = new Group(ageAndFilesGroup, SWT.NONE);
		GridData gd_ageGroup = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_ageGroup.widthHint = 150;
		ageGroup.setLayoutData(gd_ageGroup);
		ageGroup.setText("Ages");
		ageGroup.setLayout(new GridLayout(1, false));

		Composite tabButtons = new Composite(ageGroup, SWT.NONE);
		tabButtons.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		tabButtons.setLayout(new GridLayout(3, false));

		final Button addDateButton = new Button(tabButtons, SWT.PUSH);
		addDateButton.setText("+");
		addDateButton.setToolTipText("Add a new age");
		addDateButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onAddNewDateEvent();
			}
		});

		final Button deleteDateButton = new Button(tabButtons, SWT.PUSH);
		deleteDateButton.setText("-");
		deleteDateButton.setToolTipText("Remove selected age from the table");
		deleteDateButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onRemoveDateEvent();
			}
		});

		ageSpinner = new Spinner(tabButtons, SWT.BORDER);
		ageSpinner.setValues(minAgeInPreference * 100, minAgeInPreference * 100, maxAgeInPreference * 100, nbDigits,
				100, 1000);
		ageSpinner.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					onAddNewDateEvent();
				}
			}
		});

		ageTable = new Table(ageGroup, SWT.NONE);
		ageTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ageTable.setHeaderVisible(true);
		ageTable.setLinesVisible(true);
		ageTable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onCurrentAgeIndexChanged(e);
			}
		});

		TableColumn column = new TableColumn(ageTable, SWT.NONE);
		column.setText("Dates");
		column.setResizable(false);
		column.setWidth(100);

		TableColumn columnStatus = new TableColumn(ageTable, SWT.NONE);
		columnStatus.setText("OK?");
		columnStatus.setResizable(false);
		columnStatus.setWidth(40);

		// Files super group
		final Group filesGroup = new Group(ageAndFilesGroup, SWT.NONE);
		GridData gd_filesGroup = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_filesGroup.widthHint = 400;
		filesGroup.setLayoutData(gd_filesGroup);
		filesGroup.setText("Files");
		filesGroup.setLayout(new GridLayout(1, false));

		ageReminder = new Text(filesGroup, SWT.NONE);
		ageReminder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ageReminder.setEnabled(true);
		ageReminder.setEditable(false);
		ageReminder.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
		ageReminder.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_INFO_FOREGROUND));

		// Create cot files group
		final Group cotFilesGroup = new Group(filesGroup, SWT.NONE);
		GridData gd_cotFilesGroup = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_cotFilesGroup.widthHint = 400;
		cotFilesGroup.setLayoutData(gd_cotFilesGroup);
		cotFilesGroup.setText("Coast line files (*.cot | *.*)");
		cotFilesGroup.setLayout(new GridLayout(2, false));

		// Create elevation file group
		final Group elevationFileGroup = new Group(filesGroup, SWT.NONE);
		elevationFileGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		elevationFileGroup.setText("Elevation file (*.grd | *.dtm.nc | *.*)");
		elevationFileGroup.setLayout(new GridLayout(2, false));

		// Create texture file group
		final Group textureFileGroup = new Group(filesGroup, SWT.NONE);
		textureFileGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		textureFileGroup.setText("Texture file (*.tiff | *.tif | *.*)");
		textureFileGroup.setLayout(new GridLayout(2, false));

		// Create grid file group
		final Group ageFileGroup = new Group(filesGroup, SWT.NONE);
		ageFileGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ageFileGroup.setText("Age grid file (*.grd | *.*)");
		ageFileGroup.setLayout(new GridLayout(2, false));

		// Create tec file group
		final Group tecFileGroup = new Group(composite, SWT.NONE);
		tecFileGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tecFileGroup.setText("Output file (*.mtec)");
		tecFileGroup.setLayout(new GridLayout(2, false));

		// Create list and buttons of .cot file group
		cotFiles = new List(cotFilesGroup, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		GridData cotFilesData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		cotFilesData.widthHint = 100;
		cotFilesData.heightHint = 5 * cotFiles.getItemHeight();
		cotFiles.setLayoutData(cotFilesData);

		final Button cotAddButton = new Button(cotFilesGroup, SWT.PUSH);
		cotAddButton.setText("Add");
		final Button cotDelButton = new Button(cotFilesGroup, SWT.PUSH);
		cotDelButton.setText("Delete");

		// Listener for add button
		cotAddButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onAddCOTFileEvent();
			}
		});

		// Listener for Del button
		cotDelButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onDeleteCOTFileEvent();
			}
		});

		// Create field and buttons of elevation file group
		final Button rotationAddButton = new Button(rotationFileGroup, SWT.PUSH);
		rotationAddButton.setText("Select ...");

		// Listener for Select button
		rotationAddButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSelectRotationFileEvent();
			}
		});

		rotationFile = new Text(rotationFileGroup, SWT.BORDER | SWT.LEFT);
		rotationFile.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		rotationFile.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				model.sRotationFile = rotationFile.getText();
				updateFilesValidityStatusColumn();
			}
		});
		rotationFile.setText(model.sRotationFile != null ? model.sRotationFile : "");

		// Create field and buttons of elevation file group
		final Button elevationAddButton = new Button(elevationFileGroup, SWT.PUSH);
		elevationAddButton.setText("Select ...");

		// Listener for Select button
		elevationAddButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSelectElevationFileEvent();
			}
		});

		elevationFile = new Text(elevationFileGroup, SWT.BORDER | SWT.LEFT);
		elevationFile.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		elevationFile.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				storeFilesView();
				updateFilesValidityStatusColumn();
			}
		});

		// Create field and buttons of texture file group
		final Button textureAddButton = new Button(textureFileGroup, SWT.PUSH);
		textureAddButton.setText("Select ...");

		// Listener for Select button
		textureAddButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSelectTextureFileEvent();
			}
		});

		textureFile = new Text(textureFileGroup, SWT.BORDER | SWT.LEFT);
		textureFile.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		textureFile.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				storeFilesView();
				updateFilesValidityStatusColumn();
			}
		});

		textureElevation = createTextWidgetForInteger("Elevation (m) :", textureFileGroup);
		textureElevation.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				currentFiles.sTextureElevation = textureElevation.getText();
				updateFilesValidityStatusColumn();
			}
		});

		final Button ageGridAddButton = new Button(ageFileGroup, SWT.PUSH);
		ageGridAddButton.setText("Select ...");

		// Listener for Select button
		ageGridAddButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSelectAgeFileEvent();
			}
		});

		ageFile = new Text(ageFileGroup, SWT.BORDER | SWT.LEFT);
		ageFile.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ageFile.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				storeFilesView();
				updateFilesValidityStatusColumn();
			}
		});

		// Create field and buttons of tec file group
		final Button tecAddButton = new Button(tecFileGroup, SWT.PUSH);
		tecAddButton.setText("Save as ...");

		// Listener for "Save as" button
		tecAddButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onSelectOutputFileEvent();
			}
		});

		tecFile = new Text(tecFileGroup, SWT.BORDER | SWT.LEFT);
		tecFile.setEditable(true);
		tecFile.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tecFile.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				model.sOutputFile = tecFile.getText();
				updateFilesValidityStatusColumn();
			}
		});
		tecFile.setText(model.sOutputFile != null ? model.sOutputFile : "");

		Button autoLoad = new Button(tecFileGroup, SWT.CHECK);
		autoLoad.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		autoLoad.setText("Automatically load after generation");
		autoLoad.setSelection(model.loadAfterWrite);
		autoLoad.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				model.loadAfterWrite = autoLoad.getSelection();
			}
		});

		return composite;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);

		if (cotFiles.getItemCount() == 0) {
			enableOKButton(false);
		} else {
			enableOKButton(true);
		}
	}

	protected void addNewDate(double dateToAdd) {
		Pair<DoubleRange, Files> age1 = null;
		Pair<DoubleRange, Files> age2 = null;

		int inserAtIndex = -1;

		if (!model.ages.isEmpty()) {
			Iterator<Pair<DoubleRange, Files>> it = model.ages.iterator();

			int i = 0;
			for (; it.hasNext();) {

				Pair<DoubleRange, Files> pair = it.next();
				DoubleRange currentAge = pair.getFirst();

				double min = currentAge.getMinimumDouble();
				double max = currentAge.getMaximumDouble();

				if (currentAge.containsDouble(dateToAdd) && dateToAdd != min && dateToAdd != max) {

					age1 = new Pair<>(new DoubleRange(min, dateToAdd), pair.getSecond());
					age2 = new Pair<>(new DoubleRange(dateToAdd, max), new Files());
					it.remove();
					break;

				} else {
					i++;
				}
			}

			if (age1 != null && age2 != null) {
				model.ages.add(i, age2);
				model.ages.add(i, age1);
				inserAtIndex = i + 1;
			}
		} else if (dateToAdd != minAgeInPreference) {
			inserAtIndex = 0;
			currentFiles = new Files();
			age1 = new Pair<>(new DoubleRange(0, dateToAdd), currentFiles);
			age2 = new Pair<>(new DoubleRange(dateToAdd, maxAgeInPreference), new Files());
			model.ages.add(age1);
			model.ages.add(age2);
		} else {
			inserAtIndex = 0;
			currentFiles = new Files();
			model.ages.add(new Pair<>(new DoubleRange(minAgeInPreference, maxAgeInPreference), currentFiles));
		}

		if (inserAtIndex > -1) {
			TableItem item = new TableItem(ageTable, SWT.NONE, inserAtIndex);
			item.setText(0, getAgeFormat().format(dateToAdd));
		}
	}

	private void clear() {
		// clear rotation files
		model.setsRotationFile("");
		rotationFile.setText("");

		// clear last remaining age
		model.ages.clear();
		ageTable.clearAll();
		addNewDate(minAgeInPreference);
		setCurrentAgeIndex(0);
		updateFilesView();
		model.sOutputFile = "";
		tecFile.setText("");
		model.sExtRotationFiles = "";

		enableOKButton(isValidInput());

	}

	protected void onAddNewDateEvent() {

		int tenFactor = 1;
		for (int i = 0; i < nbDigits; i++) {
			tenFactor *= 10;
		}

		double dateToAdd = (double) ageSpinner.getSelection() / (double) tenFactor;

		if (dateToAdd > 0.0 && dateToAdd < maxAgeInPreference) {
			addNewDate(dateToAdd);
		}
	}

	private void removeAge(int ageIndex) {

		int selectionIndex = ageIndex;
		if (selectionIndex > 0) {

			ageTable.remove(selectionIndex);

			// Blend the 2 contiguous ranges
			Pair<DoubleRange, Files> r1 = model.ages.get(selectionIndex - 1);
			Pair<DoubleRange, Files> r2 = model.ages.get(selectionIndex);
			Pair<DoubleRange, Files> r3 = new Pair<>(
					new DoubleRange(r1.getFirst().getMinimumDouble(), r2.getFirst().getMaximumDouble()),
					r1.getSecond());

			model.ages.remove(selectionIndex - 1);
			model.ages.remove(selectionIndex - 1);
			model.ages.add(selectionIndex - 1, r3);

			if (ageIndex == currentAgeIndex) {
				setCurrentAgeIndex(Math.min(currentAgeIndex, model.ages.size() - 1));
			}
		}
	}

	protected void onRemoveDateEvent() {
		removeAge(currentAgeIndex);
	}

	protected void onAddCOTFileEvent() {
		FileDialog fileDialog = new FileDialog(getShell(), SWT.MULTI | SWT.OPEN);
		fileDialog.setFilterNames(new String[] { "*.cot" });
		fileDialog.setFilterExtensions(new String[] { "*.cot", "*.*" });
		fileDialog.setFilterPath(surveyPath);

		// Try to retrive the path
		if (!_preferences.get(_KeyPathCotFile, "").isEmpty()) {
			fileDialog.setFilterPath(_preferences.get(_KeyPathCotFile, null));
		}

		String fileName = fileDialog.open();
		if (fileName != null) {
			File parentDir = new File(fileName).getParentFile();
			String[] fileNames = fileDialog.getFileNames();
			for (String fname : fileNames) {
				// Save the current path
				File file = new File(parentDir, fname);
				_preferences.put(_KeyPathCotFile, parentDir.getAbsolutePath());

				// Update fields
				String[] oldCotFiles = model.ages.get(currentAgeIndex).getSecond().sCotFiles;
				String[] newCotFiles = new String[(oldCotFiles != null ? oldCotFiles.length : 0) + 1];
				if (oldCotFiles != null) {
					System.arraycopy(oldCotFiles, 0, newCotFiles, 0, oldCotFiles.length);
				}
				newCotFiles[newCotFiles.length - 1] = file.getAbsolutePath();
				model.ages.get(currentAgeIndex).getSecond().sCotFiles = newCotFiles;
			}

			updateFilesView();
		}
		enableOKButton(isValidInput());
	}

	protected void onDeleteCOTFileEvent() {

		String[] oldCotFiles = model.ages.get(currentAgeIndex).getSecond().sCotFiles;

		int[] selectionIndices = cotFiles.getSelectionIndices();
		if (selectionIndices != null && selectionIndices.length > 0) {

			String[] newCotFiles = ArrayUtils.removeAll(oldCotFiles, selectionIndices);
			model.ages.get(currentAgeIndex).getSecond().sCotFiles = newCotFiles;

			updateFilesView();
		}
	}

	protected void onSelectRotationFileEvent() {
		String fileName = "";
		FileDialog fileDialog = new FileDialog(getShell(), SWT.SINGLE | SWT.OPEN);
		fileDialog.setFilterNames(new String[] { "*.tot", "*.int" });
		fileDialog.setFilterExtensions(new String[] { "*.tot", "*.int", "*.*" });
		fileDialog.setFilterPath(surveyPath);

		// Try to retrive the path
		if (!_preferences.get(_KeyPathRotationFile, "").isEmpty()) {
			fileDialog.setFilterPath(_preferences.get(_KeyPathRotationFile, null));
		}

		fileName = fileDialog.open();

		if (fileName != null) {
			// Save the current path
			File l_file = new File(fileName);
			String l_name = l_file.getName();
			String currentPathRotationFile = l_file.getAbsolutePath();
			currentPathRotationFile = currentPathRotationFile.substring(0, currentPathRotationFile.indexOf(l_name));
			_preferences.put(_KeyPathRotationFile, currentPathRotationFile);
			// Update fields
			model.sRotationFile = fileName;
			switch (fileDialog.getFilterIndex()) {
			case 0:
				model.sExtRotationFiles = "tot";
				break;
			case 1:
				model.sExtRotationFiles = "int";
				break;
			default:
				model.sExtRotationFiles = "";
			}
			rotationFile.setText(fileName);
		}
		enableOKButton(isValidInput());
	}

	protected void onSelectOutputFileEvent() {

		String fileName = "";
		FileDialog fileDialog = new FileDialog(getShell(), SWT.SAVE);
		fileDialog.setFilterNames(
				new String[] { MultiLayerTecFile.FILE_DESCRIPTION + " (*." + MultiLayerTecFile.FILE_EXTENSION + ")",
						"All files (*.*)" });
		fileDialog.setFilterExtensions(new String[] { "*." + MultiLayerTecFile.FILE_EXTENSION, "*.*" });
		fileDialog.setFilterPath(surveyPath);

		// Try to retrive the path
		if (!_preferences.get(_KeyPathTecFile, "").isEmpty()) {
			fileDialog.setFilterPath(_preferences.get(_KeyPathTecFile, null));
		}

		fileName = fileDialog.open();

		if (fileName != null) {
			// Save the current path
			File l_file = new File(fileName);
			String l_name = l_file.getName();
			String currentPathTecFile = l_file.getAbsolutePath();
			currentPathTecFile = currentPathTecFile.substring(0, currentPathTecFile.indexOf(l_name));
			_preferences.put(_KeyPathTecFile, currentPathTecFile);
			// Update fields
			model.sOutputFile = fileName;
			tecFile.setText(fileName);
		}
		enableOKButton(isValidInput());
	}

	protected void onSelectTextureFileEvent() {

		String fileName = "";
		FileDialog fileDialog = new FileDialog(getShell(), SWT.SINGLE | SWT.OPEN);
		fileDialog.setFilterNames(new String[] { "*.tif" });
		fileDialog.setFilterExtensions(new String[] { "*.tif", "*.*" });
		fileDialog.setFilterPath(surveyPath);

		// Try to retrive the path
		if (!_preferences.get(_KeyPathTextureFile, "").isEmpty()) {
			fileDialog.setFilterPath(_preferences.get(_KeyPathTextureFile, null));
		}

		fileName = fileDialog.open();

		if (fileName != null) {
			// Save current path
			File l_file = new File(fileName);
			String l_name = l_file.getName();
			String currentPathTextureFile = l_file.getAbsolutePath();
			currentPathTextureFile = currentPathTextureFile.substring(0, currentPathTextureFile.indexOf(l_name));
			_preferences.put(_KeyPathTextureFile, currentPathTextureFile);
			// Update fields
			currentFiles.sTextureFile = fileName;
			currentFiles.sTextureElevation = textureElevation.getText();
			if (fileDialog.getFilterIndex() == 0) {
				currentFiles.sExtTexFiles = "tif";
			} else {
				currentFiles.sExtTexFiles = "";
			}
			updateFilesView();
		}
		enableOKButton(isValidInput());
	}

	protected void onSelectElevationFileEvent() {

		String fileName = "";
		FileDialog fileDialog = new FileDialog(getShell(), SWT.SINGLE | SWT.OPEN);
		fileDialog.setFilterNames(new String[] { "*.grd", "*.dtm.nc" });
		fileDialog.setFilterExtensions(new String[] { "*.grd", "*.dtm.nc", "*.*" });
		fileDialog.setFilterPath(surveyPath);

		// Try to retrive the path
		if (!_preferences.get(_KeyPathElevFile, "").isEmpty()) {
			fileDialog.setFilterPath(_preferences.get(_KeyPathElevFile, null));
		}

		fileName = fileDialog.open();

		if (fileName != null) {
			// Save the current path
			File l_file = new File(fileName);
			String l_name = l_file.getName();
			String currentPathElevFile = l_file.getAbsolutePath();
			currentPathElevFile = currentPathElevFile.substring(0, currentPathElevFile.indexOf(l_name));
			_preferences.put(_KeyPathElevFile, currentPathElevFile);
			// Update fields
			currentFiles.sElevationFile = fileName;
			currentFiles.sExtElevFiles = switch (fileDialog.getFilterIndex()) {
			case 0 -> "grd";
			case 1 -> "dtm.nc";
			default -> "";
			};
			updateFilesView();
		}
		enableOKButton(isValidInput());
	}

	protected void onSelectAgeFileEvent() {
		String fileName = "";
		FileDialog fileDialog = new FileDialog(getShell(), SWT.SINGLE | SWT.OPEN);
		fileDialog.setFilterNames(new String[] { "*.grd" });
		fileDialog.setFilterExtensions(new String[] { "*.grd", "*.*" });

		// Try to retrive the path
		if (!_preferences.get(_KeyPathAgeFile, "").isEmpty()) {
			fileDialog.setFilterPath(_preferences.get(_KeyPathAgeFile, null));
		}

		fileName = fileDialog.open();

		if (fileName != null) {
			// Save the current path
			File l_file = new File(fileName);
			String l_name = l_file.getName();
			String currentPathElevFile = l_file.getAbsolutePath();
			currentPathElevFile = currentPathElevFile.substring(0, currentPathElevFile.indexOf(l_name));
			_preferences.put(_KeyPathAgeFile, currentPathElevFile);
			// Update fields
			currentFiles.sAgeFile = fileName;
			switch (fileDialog.getFilterIndex()) {
			case 0:
				currentFiles.sExtAgeFiles = "grd";
				break;
			default:
				currentFiles.sExtAgeFiles = "";
			}
			updateFilesView();
		}
		enableOKButton(isValidInput());
	}

	protected void onCurrentAgeIndexChanged(SelectionEvent e) {
		int selectionIndex = ageTable.getSelectionIndex();
		if (selectionIndex != currentAgeIndex) {
			storeFilesView();
			setCurrentAgeIndex(selectionIndex);
		}
	}

	protected void setCurrentAgeIndex(int selectionIndex) {

		// Update selection in the age table if it is not compliant with the
		// desired selectionIndex.
		if (ageTable.getSelectionIndex() != selectionIndex) {
			ageTable.setSelection(selectionIndex);
		}

		currentAgeIndex = selectionIndex;
		currentFiles = model.ages.get(selectionIndex).getSecond();

		updateFilesView();
	}

	/**
	 * Store input values of the "files" group into the model.
	 */
	private void storeFilesView() {

		// get the .cot file list
		currentFiles.sCotFiles = new String[cotFiles.getItemCount()];
		for (int i = 0; i < cotFiles.getItemCount(); i++) {
			currentFiles.sCotFiles[i] = cotFiles.getItem(i);
		}

		// get the elevation file
		currentFiles.sElevationFile = elevationFile.getText();
		if (StringUtils.isEmpty(currentFiles.sElevationFile)) {
			currentFiles.sElevationFile = null;
			currentFiles.sExtElevFiles = null;
		} else {
			currentFiles.sExtElevFiles = FileUtils.getExtension(currentFiles.sElevationFile);
			if (currentFiles.sExtElevFiles != null) {
				currentFiles.sExtElevFiles = currentFiles.sExtElevFiles.toLowerCase();
			}
		}

		// get the texture file
		currentFiles.sTextureFile = textureFile.getText();
		if (StringUtils.isEmpty(currentFiles.sTextureFile)) {
			currentFiles.sTextureFile = null;
			currentFiles.sExtTexFiles = null;
		} else {
			currentFiles.sExtTexFiles = FilenameUtils.getExtension(currentFiles.sTextureFile);
			if (currentFiles.sExtTexFiles != null) {
				currentFiles.sExtTexFiles = currentFiles.sExtTexFiles.toLowerCase();
			}
		}

		// get the texture elevation
		currentFiles.sTextureElevation = textureElevation.getText();
		if (StringUtils.isEmpty(currentFiles.sTextureElevation)) {
			currentFiles.sTextureElevation = null;
		}

		// get the age file
		currentFiles.sAgeFile = ageFile.getText();
		if (StringUtils.isEmpty(currentFiles.sAgeFile)) {
			currentFiles.sAgeFile = null;
		}
	}

	/**
	 * Update the "files" group input values according to the model values for the current age index.
	 */
	private void updateFilesView() {

		// COT files
		cotFiles.removeAll();
		if (currentFiles.sCotFiles != null) {
			for (String cotFile : currentFiles.sCotFiles) {
				cotFiles.add(cotFile);
			}
		}

		// Elevation file
		if (currentFiles.sElevationFile != null) {
			elevationFile.setText(currentFiles.sElevationFile);
		} else {
			elevationFile.setText("");
		}

		// Texture file
		if (currentFiles.sTextureFile != null) {
			textureFile.setText(currentFiles.sTextureFile);
		} else {
			textureFile.setText("");
		}

		// Texture elevation
		if (currentFiles.sTextureElevation != null && !currentFiles.sTextureElevation.isEmpty()
				&& integerValidator.isValid(currentFiles.sTextureElevation) == null) {
			textureElevation.setText(currentFiles.sTextureElevation);
		} else {
			textureElevation.setText("0");
		}

		// Age grid file
		if (currentFiles.sAgeFile != null) {
			ageFile.setText(currentFiles.sAgeFile);
		} else {
			ageFile.setText("");
		}

		// Update other parts
		updateFilesViewHeader();
		updateFilesValidityStatusColumn();
	}

	/**
	 * Updates the "Files" widgets group header according to the current age selected in the age table.
	 */
	protected void updateFilesViewHeader() {
		NumberFormat nf = getAgeFormat();
		DoubleRange period = model.ages.get(currentAgeIndex).getFirst();
		ageReminder.setText(
				"Period : " + nf.format(period.getMinimumDouble()) + " --> " + nf.format(period.getMaximumDouble()));
	}

	/**
	 * Updates the validity status in the age table column. The status is empty unless all files for the specified age
	 * are valid.
	 * <p>
	 * Furthermore the global "OK" button of the dialog frame is enabled if the status passes to "OK".
	 */
	protected void updateFilesValidityStatusColumn() {

		boolean result = isValidFiles(currentAgeIndex);
		ageTable.getItem(currentAgeIndex).setText(1, result ? "OK" : "-");

		enableOKButton(isValidInput());
	}

	/**
	 * OK Button enabling or desabling
	 */
	protected void enableOKButton(boolean enable) {
		getButton(IDialogConstants.OK_ID).setEnabled(enable);
	}

	/**
	 * Ajoute un widget de type texte ne permettant de saisir que des caractères numériques.
	 *
	 * @param label la label du widget
	 * @param parent le composite parent
	 * @return le widget créé ou <code>null</code> si erreur
	 */
	protected Text createTextWidgetForInteger(final String label, final Composite parent) {
		Label nameLabel = new Label(parent, SWT.NONE);
		nameLabel.setText(label);
		Text text = new Text(parent, SWT.BORDER | SWT.RIGHT);
		text.setEditable(true);
		text.setEnabled(true);

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 70;
		text.setLayoutData(gridData);

		text.addVerifyListener((event) -> {
			event.doit = (event.character >= '0' && event.character <= '9') || event.character == SWT.DEL
					|| event.character == SWT.BS || integerValidator.isValid(event.text) == null || event.text == null
					|| event.text.isEmpty();
		});

		return text;
	}

	/**
	 * Ajoute un widget de type texte ne permettant de saisir que des caractères réels.
	 *
	 * @param label la label du widget
	 * @param parent le composite parent
	 * @return le widget créé ou <code>null</code> si erreur
	 */
	protected Text createTextWidgetForFloat(final String label, final Composite parent) {
		Label nameLabel = new Label(parent, SWT.NONE);
		nameLabel.setText(label);
		Text text = new Text(parent, SWT.BORDER | SWT.RIGHT);
		text.setEditable(true);

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 70;
		text.setLayoutData(gridData);

		return text;
	}

	/**
	 * Ajout un widget de type comboBox.
	 *
	 * @param label le label a afficher
	 * @param parent le composite parent
	 * @param values la liste des valeurs utilis�es dans la comboBox
	 * @return le widget cr��.
	 */
	public Combo createComboWidget(final String label, final Composite parent, final String[] values) {
		Label nameLabel = new Label(parent, SWT.NONE);
		nameLabel.setText(label);
		Combo combo = new Combo(parent, SWT.READ_ONLY);
		combo.setItems(values);
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 80;
		combo.setLayoutData(gridData);

		return combo;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void buttonPressed(int buttonId) {

		storeFilesView();

		boolean allValid = isValidInput();
		boolean okToContinue = true;

		if (buttonId == IDialogConstants.OK_ID) {

			if (allValid) {
				okToContinue = prepareOutputFile(new File(model.sOutputFile));
			}

		} else if (buttonId == IDialogConstants.CANCEL_ID) {

			String message = "Are you sure you want to cancel?";
			MessageBox mb = new MessageBox(this.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			mb.setText("Question");
			mb.setMessage(message);

			int result = mb.open();
			okToContinue = (result == SWT.YES);
		}

		if (okToContinue) {
			super.buttonPressed(buttonId);
		}
	}

	/**
	 * @return <code>true</code> if all input fields are valid.
	 */
	private boolean isValidInput() {
		boolean result = true;

		if (model.sRotationFile != null && !new File(model.sRotationFile).exists()) {
			result = false;
		}

		if (model.sOutputFile == null || model.sOutputFile.isEmpty()
				|| new File(model.sOutputFile).getParentFile() == null
				|| !new File(model.sOutputFile).getParentFile().exists()) {
			result = false;
		}

		if (result) {
			result = isValidFiles();
		}

		return result;
	}

	/**
	 * @return <code>true</code> if the "Files" sections are valid for all ages (ie. all they are all filled and they
	 *         all exist on disk).
	 */
	private boolean isValidFiles() {
		boolean result = true;
		if (model.ages != null) {
			for (int i = 0; i < model.ages.size(); i++) {
				result = result && isValidFiles(i);
			}
		}
		return result;
	}

	/**
	 * Returns whether the files of specified age are valid (ie. all they are all filled and they all exist on disk).
	 *
	 * @param ageIndex The ageIndex to test.
	 * @return <code>true</code> if all files are present.
	 */
	private boolean isValidFiles(int ageIndex) {
		boolean result = true;
		if (model.ages != null && !model.ages.isEmpty()) {

			Pair<DoubleRange, Files> pair = model.ages.get(ageIndex);

			Files files = pair.getSecond();

			if (files.sAgeFile != null && !new File(files.sAgeFile).exists()) {
				result = false;
			}
			if (files.sElevationFile != null && !new File(files.sElevationFile).exists()) {
				result = false;
			}
			if (files.sTextureFile != null && !new File(files.sTextureFile).exists()) {
				result = false;
			}
			if (files.sTextureFile != null
					&& (files.sTextureElevation == null || integerValidator.isValid(files.sTextureElevation) != null)) {
				result = false;
			}
			if (files.sAgeFile == null && files.sElevationFile == null && files.sTextureFile == null) {
				result = false;
			}
			if (files.sCotFiles != null) {
				if (files.sCotFiles.length > 0) {
					for (String cotFileName : files.sCotFiles) {
						if (!new File(cotFileName).exists()) {
							result = false;
							break;
						}
					}
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
		return result;
	}

	/**
	 * If the specified output file already exists, this method asks the user if he/she wants to overwrite it.
	 *
	 * @param outputFile The output file to generate.
	 * @return <code>true</code> if the user
	 */
	protected boolean prepareOutputFile(File outputFile) {

		boolean okToContinue = true;

		if (outputFile.exists()) {
			String message = "Output file '" + outputFile.getName()
					+ "' already exists in destination folder. Overwrite?";
			MessageBox mb = new MessageBox(this.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			mb.setText("Question");
			mb.setMessage(message);

			int result = mb.open();
			if (result == SWT.YES) {
				okToContinue = outputFile.delete();
				if (!okToContinue) {
					displayError(getShell(), "Error", "Unable to delete " + outputFile.getAbsolutePath());
				}
			} else {
				okToContinue = false;
			}
		}
		return okToContinue;
	}

	protected void displayError(Shell shell, String title, String message) {
		Display.getDefault().asyncExec(() -> {
			MessageDialog.openError(shell, title, message);
		});
	}

	/**
	 * Follow the link.
	 *
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		// Save preferences
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(model);
			_preferences.putByteArray(_KeyModel, baos.toByteArray());
		} catch (IOException e) {
			// Unable to serialize the model...
		}

		super.okPressed();
	}

	/**
	 * Restore the preferences.
	 */
	protected void restoreModel() {
		try {
			byte[] serializedModel = _preferences.getByteArray(_KeyModel, null);
			if (serializedModel != null) {
				ByteArrayInputStream bais = new ByteArrayInputStream(serializedModel);
				ObjectInputStream ois = new ObjectInputStream(bais);
				model = (Model) ois.readObject();
			}
		} catch (Exception e) {
			// Unable to deserialize the model...
		}

		// Something wrong with this model ?
		if (model == null || model.getAges() == null || model.getAges().isEmpty()
				|| model.getAges().get(model.getAges().size() - 1).getFirst().getMaximumInteger() != maxAgeInPreference
				|| model.getAges().get(0).getFirst().getMinimumInteger() != minAgeInPreference) {
			model = new Model();
		}
	}

}
