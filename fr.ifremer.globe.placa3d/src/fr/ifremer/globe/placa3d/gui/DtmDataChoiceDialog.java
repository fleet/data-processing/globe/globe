package fr.ifremer.globe.placa3d.gui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

/**
 * Class for DTM data choice generator dialog box
 * 
 * @author D.riou
 */
public class DtmDataChoiceDialog extends Dialog {

	/** The title of the dialog. */
	private String title = "Choose the data to extract from DTM";

	/** The list of data name */
	private List<String> dataList;

	/** The list of buttons linked with each adapter. */
	private List<Button> radioList = new ArrayList<Button>();

	/** The index of the selection. */
	int index;

	public DtmDataChoiceDialog(Shell parentShell, List<String> a_dataList) {
		super(parentShell);

		dataList = a_dataList;
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			for (int i = 0; i < radioList.size(); i++) {
				if (radioList.get(i).getSelection()) {
					index = i;
					break;
				}
			}
		}
		super.buttonPressed(buttonId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (title != null) {
			shell.setText(title);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);

		radioList.get(0).setFocus();
		radioList.get(0).setSelection(true);
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		for (int i = 0; i < dataList.size(); i++) {
			Button radio = new Button(composite, SWT.RADIO);
			radio.setText(dataList.get(i));
			radioList.add(radio);
		}

		applyDialogFont(composite);
		return composite;
	}

	public String getChoice() {
		return dataList.get(index);

	}
}
