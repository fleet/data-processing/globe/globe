package fr.ifremer.globe.placa3d.gui.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.utils.Pair;

public class Model implements Externalizable {
	public boolean loadAfterWrite = true;
	public String sRotationFile;
	public String sExtRotationFiles;
	public List<Pair<DoubleRange, Files>> ages = new ArrayList<>();
	public String sOutputFile;

	/**
	 * Constructor.
	 */
	public Model() {
	}

	/**
	 * @return the {@link #loadAfterWrite}
	 */
	public boolean isLoadAfterWrite() {
		return loadAfterWrite;
	}

	/**
	 * @param loadAfterWrite
	 *            the {@link #loadAfterWrite} to set
	 */
	public void setLoadAfterWrite(boolean loadAfterWrite) {
		this.loadAfterWrite = loadAfterWrite;
	}

	/**
	 * @return the {@link #sRotationFile}
	 */
	public String getsRotationFile() {
		return sRotationFile;
	}

	/**
	 * @param sRotationFile
	 *            the {@link #sRotationFile} to set
	 */
	public void setsRotationFile(String sRotationFile) {
		this.sRotationFile = sRotationFile;
	}

	/**
	 * @return the {@link #sExtRotationFiles}
	 */
	public String getsExtRotationFiles() {
		return sExtRotationFiles;
	}

	/**
	 * @param sExtRotationFiles
	 *            the {@link #sExtRotationFiles} to set
	 */
	public void setsExtRotationFiles(String sExtRotationFiles) {
		this.sExtRotationFiles = sExtRotationFiles;
	}

	/**
	 * @return the {@link #ages}
	 */
	public List<Pair<DoubleRange, Files>> getAges() {
		return ages;
	}

	/**
	 * @param ages
	 *            the {@link #ages} to set
	 */
	public void setAges(List<Pair<DoubleRange, Files>> ages) {
		this.ages = ages;
	}

	/**
	 * @return the {@link #sOutputFile}
	 */
	public String getsOutputFile() {
		return sOutputFile;
	}

	/**
	 * @param sOutputFile
	 *            the {@link #sOutputFile} to set
	 */
	public void setsOutputFile(String sOutputFile) {
		this.sOutputFile = sOutputFile;
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Externalizable#writeExternal(java.io.ObjectOutput)
	 */
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeBoolean(loadAfterWrite);
		out.writeUTF(sRotationFile != null ? sRotationFile : "");
		out.writeUTF(sExtRotationFiles != null ? sExtRotationFiles : "");
		out.writeUTF(sOutputFile != null ? sOutputFile : "");

		if (ages != null && ages.size() > 0) {
			out.writeInt(ages.size());
			for (Pair<DoubleRange, Files> pair : ages) {
				out.writeDouble(pair.getFirst().getMinimumDouble());
				out.writeDouble(pair.getFirst().getMaximumDouble());
				Files files = pair.getSecond();
				out.writeObject(files != null ? files : new Files());
			}
		} else {
			out.writeInt(0);
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Externalizable#readExternal(java.io.ObjectInput)
	 */
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		loadAfterWrite = in.readBoolean();

		sRotationFile = in.readUTF();
		sRotationFile = sRotationFile.isEmpty() ? null : sRotationFile;

		sExtRotationFiles = in.readUTF();
		sExtRotationFiles = sExtRotationFiles.isEmpty() ? null : sExtRotationFiles;

		sOutputFile = in.readUTF();
		sOutputFile = sOutputFile.isEmpty() ? null : sOutputFile;

		int pairCount = in.readInt();
		for (int i = 0; i < pairCount; i++) {
			DoubleRange range = new DoubleRange(in.readDouble(), in.readDouble());
			Files files = (Files) in.readObject();
			Pair<DoubleRange, Files> pair = new Pair<DoubleRange, Files>(range, files);
			ages.add(pair);

		}
	}

}