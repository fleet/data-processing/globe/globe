package fr.ifremer.globe.placa3d.gui.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Files implements Externalizable {

	public String sElevationFile;
	public String sTextureFile;
	public String sTextureElevation;
	public String sAgeFile;
	public String[] sCotFiles;
	public String sExtElevFiles;
	public String sExtTexFiles;
	public String sExtAgeFiles;

	/**
	 * Constructor.
	 */
	public Files() {
	}

	/**
	 * @return the {@link #sElevationFile}
	 */
	public String getsElevationFile() {
		return sElevationFile;
	}

	/**
	 * @param sElevationFile
	 *            the {@link #sElevationFile} to set
	 */
	public void setsElevationFile(String sElevationFile) {
		this.sElevationFile = sElevationFile;
	}

	/**
	 * @return the {@link #sTextureFile}
	 */
	public String getsTextureFile() {
		return sTextureFile;
	}

	/**
	 * @param sTextureFile
	 *            the {@link #sTextureFile} to set
	 */
	public void setsTextureFile(String sTextureFile) {
		this.sTextureFile = sTextureFile;
	}

	/**
	 * @return the {@link #sTextureElevation}
	 */
	public String getsTextureElevation() {
		return sTextureElevation;
	}

	/**
	 * @param sTextureElevation
	 *            the {@link #sTextureElevation} to set
	 */
	public void setsTextureElevation(String sTextureElevation) {
		this.sTextureElevation = sTextureElevation;
	}

	/**
	 * @return the {@link #sAgeFile}
	 */
	public String getsAgeFile() {
		return sAgeFile;
	}

	/**
	 * @param sAgeFile
	 *            the {@link #sAgeFile} to set
	 */
	public void setsAgeFile(String sAgeFile) {
		this.sAgeFile = sAgeFile;
	}

	/**
	 * @return the {@link #sCotFiles}
	 */
	public String[] getsCotFiles() {
		return sCotFiles;
	}

	/**
	 * @param sCotFiles
	 *            the {@link #sCotFiles} to set
	 */
	public void setsCotFiles(String[] sCotFiles) {
		this.sCotFiles = sCotFiles;
	}

	/**
	 * @return the {@link #sExtElevFiles}
	 */
	public String getsExtElevFiles() {
		return sExtElevFiles;
	}

	/**
	 * @param sExtElevFiles
	 *            the {@link #sExtElevFiles} to set
	 */
	public void setsExtElevFiles(String sExtElevFiles) {
		this.sExtElevFiles = sExtElevFiles;
	}

	/**
	 * @return the {@link #sExtTexFiles}
	 */
	public String getsExtTexFiles() {
		return sExtTexFiles;
	}

	/**
	 * @param sExtTexFiles
	 *            the {@link #sExtTexFiles} to set
	 */
	public void setsExtTexFiles(String sExtTexFiles) {
		this.sExtTexFiles = sExtTexFiles;
	}

	/**
	 * @return the {@link #sExtAgeFiles}
	 */
	public String getsExtAgeFiles() {
		return sExtAgeFiles;
	}

	/**
	 * @param sExtAgeFiles
	 *            the {@link #sExtAgeFiles} to set
	 */
	public void setsExtAgeFiles(String sExtAgeFiles) {
		this.sExtAgeFiles = sExtAgeFiles;
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Externalizable#writeExternal(java.io.ObjectOutput)
	 */
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeUTF(sElevationFile != null ? sElevationFile : "");
		out.writeUTF(sTextureFile != null ? sTextureFile : "");
		out.writeUTF(sTextureElevation != null ? sTextureElevation : "");
		out.writeUTF(sAgeFile != null ? sAgeFile : "");
		if (sCotFiles != null && sCotFiles.length > 0) {
			out.writeInt(sCotFiles.length);
			for (String cotFile : sCotFiles) {
				out.writeUTF(cotFile != null ? cotFile : "");
			}
		} else {
			out.writeInt(0);
		}
		out.writeUTF(sExtElevFiles != null ? sExtElevFiles : "");
		out.writeUTF(sExtTexFiles != null ? sExtTexFiles : "");
		out.writeUTF(sExtAgeFiles != null ? sExtAgeFiles : "");
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Externalizable#readExternal(java.io.ObjectInput)
	 */
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		sElevationFile = in.readUTF();
		sElevationFile = sElevationFile.isEmpty() ? null : sElevationFile;

		sTextureFile = in.readUTF();
		sTextureFile = sTextureFile.isEmpty() ? null : sTextureFile;

		sTextureElevation = in.readUTF();
		sTextureElevation = sTextureElevation.isEmpty() ? null : sTextureElevation;

		sAgeFile = in.readUTF();
		sAgeFile = sAgeFile.isEmpty() ? null : sAgeFile;

		sCotFiles = new String[in.readInt()];
		for (int i = 0; i < sCotFiles.length; i++) {
			sCotFiles[i] = in.readUTF();

		}

		sExtElevFiles = in.readUTF();
		sExtElevFiles = sExtElevFiles.isEmpty() ? null : sExtElevFiles;

		sExtTexFiles = in.readUTF();
		sExtTexFiles = sExtTexFiles.isEmpty() ? null : sExtTexFiles;

		sExtAgeFiles = in.readUTF();
		sExtAgeFiles = sExtAgeFiles.isEmpty() ? null : sExtAgeFiles;
	}
}