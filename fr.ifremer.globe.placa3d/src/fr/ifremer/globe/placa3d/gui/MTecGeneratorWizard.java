package fr.ifremer.globe.placa3d.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.imageio.ImageIO;
import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.ogr.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.file.polygon.BasicPolygon;
import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.IPolygonWriter;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.placa3d.application.context.ContextInitializer;
import fr.ifremer.globe.placa3d.data.TectonicGrid;
import fr.ifremer.globe.placa3d.data.TectonicLayer;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.IPoleRotationFile;
import fr.ifremer.globe.placa3d.file.intpol.IntermediatePoleFile;
import fr.ifremer.globe.placa3d.file.totpol.TotalPoleFile;
import fr.ifremer.globe.placa3d.gui.model.Files;
import fr.ifremer.globe.placa3d.gui.model.Model;
import fr.ifremer.globe.placa3d.reader.raster.RasterReaderFactory;
import fr.ifremer.globe.placa3d.tecGenerator.ElevationExtractor;
import fr.ifremer.globe.placa3d.tecGenerator.ElevationReaderAdaptor;
import fr.ifremer.globe.placa3d.tecGenerator.FileCotReader;
import fr.ifremer.globe.placa3d.tecGenerator.FileGeoTifReader;
import fr.ifremer.globe.placa3d.tecGenerator.FileGmtReader;
import fr.ifremer.globe.placa3d.tecGenerator.IElevationReaderAdaptor;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.writer.MultiLayerTecFileWriter;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.viewer3d.data.reader.NetcdfElevationReader;
import fr.ifremer.viewer3d.data.reader.NetcdfReader;
import fr.ifremer.viewer3d.data.reader.raster.RasterAttributes;
import fr.ifremer.viewer3d.data.reader.raster.RasterReader;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

public class MTecGeneratorWizard {

	private final static Logger LOGGER = LoggerFactory.getLogger(MTecGeneratorWizard.class);

	private static class CancelEvent extends Exception {
		private static final long serialVersionUID = -8070330054131342047L;
	}

	@Inject
	RasterReaderFactory rasterReaderFactory;
	@Inject
	private IPolygonWriter polygonWriter;

	protected MTecParametersDialog dlg;
	protected Shell shell;

	protected NetcdfElevationReader lNetCdefreader;

	public MTecGeneratorWizard(Shell shell) {
		this.shell = shell;
		dlg = new MTecParametersDialog(shell);
	}

	public void start() {

		if (dlg.open() == IDialogConstants.OK_ID) {

			Model model = dlg.getModel();

			// Start the tec generation and progress bar
			Display.getDefault().asyncExec(() -> {
				ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
				try {
					dialog.run(true, true, monitor -> {
						try {
							doGenerateFile(monitor, shell, model);
							if (model.loadAfterWrite) {
								doOpenFile(model);
							}
						} catch (CancelEvent e) {
							LOGGER.info("Multilayer TEC generation canceled.");
						} catch (Exception e) {
							LOGGER.error(e.getLocalizedMessage(), e);
							Messages.openErrorMessage("Error",
									"An internal error occured. Please consult logs for more details.\nCause: "
											+ e.getClass().getName());
						} finally {
							monitor.done();
						}
					});
				} catch (Exception e) {
					LOGGER.warn("", e);
				}
			});
		}
	}

	protected ElevationReaderAdaptor newElevationReader(String elevationFile, String fileExtension)
			throws FileFormatException {

		ElevationReaderAdaptor elevReader = null;

		if ("grd".equalsIgnoreCase(fileExtension)) {
			RasterReader terrainReader = rasterReaderFactory.getGmtReader(elevationFile);
			elevReader = new ElevationReaderAdaptor(terrainReader);
		} else if ("dtm.nc".equalsIgnoreCase(fileExtension)) {

			// Get the data choice
			List<String> l_dataFieldList = NetcdfReader.getAvailableData(elevationFile);
			if (l_dataFieldList != null) {
				// Choose the data to read
				AtomicReference<String> l_DtmDataChoice = new AtomicReference<>();
				if (l_dataFieldList.size() > 1) {
					// More than one choice
					DtmDataChoiceDialog lDialogChoice = new DtmDataChoiceDialog(shell, l_dataFieldList);
					Runnable task = () -> {
						int returnCodeAd = lDialogChoice.open();
						if (returnCodeAd == 0) {
							l_DtmDataChoice.set(lDialogChoice.getChoice());
						}
					};
					Display.getDefault().syncExec(task);
				} else {
					// Only one choice - choose dialog box is not usefull
					l_DtmDataChoice.set(l_dataFieldList.get(0));
				}
				if (l_DtmDataChoice.get() != null) {
					RasterReader terrainReader = rasterReaderFactory.getDtmReader(elevationFile);
					elevReader = new ElevationReaderAdaptor(terrainReader);
				}
			}
		}
		return elevReader;
	}

	protected IElevationReaderAdaptor newTexReader(String fileExtension) {

		IElevationReaderAdaptor result = null;

		if ("tif".equalsIgnoreCase(fileExtension) || "tiff".equalsIgnoreCase(fileExtension)) {
			try {
				result = new FileGeoTifReader();
			} catch (Exception e) {
				LOGGER.error("Unable to instantiate a FileGeoTifReader", e);
			}
		}
		return result;
	}

	protected IElevationReaderAdaptor newAgeGridReader(String fileExtension) {

		IElevationReaderAdaptor result = null;

		if ("grd".equalsIgnoreCase(fileExtension)) {
			result = ContextInitializer.make(FileGmtReader.class);
		}
		return result;
	}

	protected void doGenerateFile(IProgressMonitor monitor, Shell shell, Model model) throws Exception {

		checkCancellation(monitor);

		// Log the start date generation
		LOGGER.info("Generation started at " + new Date());

		IPoleRotationFile poleFile = null;
		if ("tot".equalsIgnoreCase(model.sExtRotationFiles)) {
			poleFile = new TotalPoleFile(model.sRotationFile);

		} else if ("int".equalsIgnoreCase(model.sExtRotationFiles)) {
			poleFile = new IntermediatePoleFile(model.sRotationFile);
		}

		monitor.subTask("Extracting pole rotations data...");

		// Extract pole rotations data
		TectonicMovement movement = null;
		if (poleFile != null) {
			try {
				movement = poleFile.read();
			} catch (FileFormatException e) {
				displayError(shell, "Reading pole file error", e.getMessage());
				model.loadAfterWrite = false;
				return;
			} catch (Exception e) {
				LOGGER.error("The pole file can not be read (cause : " + e.getMessage() + ")", e);
				displayError(shell, "Reading pole file error",
						"The pole file can not be read (cause : " + e.getMessage() + ")");
				model.loadAfterWrite = false;
				return;
			}
		}

		// Modèle de données
		TectonicModel tectonicModel = new TectonicModel();
		String plateName = new File(model.sOutputFile).getName();
		List<TectonicLayer> tectonicLayers = new ArrayList<>();

		for (Pair<DoubleRange, Files> pair : model.getAges()) {
			DoubleRange period = pair.getFirst();
			String layerName = computeLayerName(pair.getSecond());
			tectonicLayers.add(new TectonicLayer(layerName, period));
		}

		TectonicPlateModel plate = tectonicModel.addPlate(plateName, tectonicLayers);
		plate.setMovement(movement);

		// Write .mtec file
		monitor.subTask("Writing " + model.sOutputFile + "...");
		File outputFile = new File(model.sOutputFile);

		try (MultiLayerTecFileWriter writer = new MultiLayerTecFileWriter(outputFile)) {
			writer.open();
			boolean isMetadataWritten = false;

			// first of all compute the global bounding box
			for (Pair<DoubleRange, Files> pair : model.getAges()) {
				Files files = pair.getSecond();

				// initialize cot reader
				FileCotReader cotReader = new FileCotReader();
				cotReader.setFileNameList(files.sCotFiles);
				Sector sector = cotReader.computeBoundingBox();

				Sector geoBox = plate.getMetadata().getGeoBox();
				if (geoBox == null) {
					geoBox = sector;
				} else {
					geoBox = geoBox.union(sector);
				}
				plate.getMetadata().setGeoBox(geoBox);
			}
			// Traitement du groupe de fichiers de chaque période
			int iPeriodIndex = 0;
			for (Pair<DoubleRange, Files> pair : model.getAges()) {
				Sector sector = plate.getMetadata().getGeoBox();

				DoubleRange period = pair.getFirst();
				Files files = pair.getSecond();

				// initialize cot reader
				FileCotReader cotReader = new FileCotReader();
				cotReader.setFileNameList(files.sCotFiles);

				// initialize readers
				ElevationReaderAdaptor elevReader = null;
				if (files.sElevationFile != null) {
					elevReader = newElevationReader(files.sElevationFile, files.sExtElevFiles);
					if (elevReader == null) {
						displayError(shell, DicoBundle.getString("TEC_GENERATOR_BAD_ELEVATION_FILE_TITLE"),
								DicoBundle.getString("TEC_GENERATOR_BAD_ELEVATION_FILE") + "\n("
										+ FilenameUtils.getName(files.sElevationFile) + ")");
						model.loadAfterWrite = false;
						return;
					}
				}

				IElevationReaderAdaptor texReader = null;
				if (files.sExtTexFiles != null) {
					texReader = newTexReader(files.sExtTexFiles);
					if (texReader == null) {
						displayError(shell, DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE_TITLE"),
								DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE") + "\n("
										+ FilenameUtils.getName(files.sExtTexFiles) + ")");
						model.loadAfterWrite = false;
						return;
					}
				}

				IElevationReaderAdaptor ageReader = newAgeGridReader(files.sExtAgeFiles);

				checkCancellation(monitor);

				// Init the progress bar
				monitor.beginTask("Generating Multilayer TEC file...", 100);

				// Extract elevation data
				double[][] elevationData = null;
				if (elevReader != null && files.sElevationFile != null && !files.sElevationFile.isEmpty()) {
					monitor.subTask("Extracting elevation data...");
					try {
						elevationData = extractElevation(sector, elevReader, cotReader, files, monitor);
					} catch (Exception e) {
						displayError(shell, DicoBundle.getString("TEC_GENERATOR_BAD_ELEVATION_FILE_TITLE"),
								e.toString() + "\n(" + FilenameUtils.getName(files.sElevationFile) + ")");
						model.loadAfterWrite = false;
						return;
					}
				}

				checkCancellation(monitor);

				// Extract texture data
				double[][] textureData = null;
				if (texReader != null && files.sTextureFile != null && !files.sTextureFile.isEmpty()) {
					monitor.subTask("Extracting texture data...");

					texReader.setFileName(files.sTextureFile);
					ElevationExtractor l_texExtractor = new ElevationExtractor(texReader, cotReader);
					try {
						l_texExtractor.computeExtraction(sector, monitor);
						textureData = l_texExtractor.getElevationMatrix();
						// dumpAsGrayImage(textureData, textureData[0].length,
						// textureData.length,"D:/tex_grid.png");
					} catch (Exception e) {
						displayError(shell, DicoBundle.getString("TEC_GENERATOR_BAD_TEXTURE_FILE_TITLE"),
								e.toString() + "\n(" + FilenameUtils.getName(files.sTextureFile) + ")");

						model.loadAfterWrite = false;
						return;
					}
				}

				// Extract age data
				double[][] ageData = null;
				if (ageReader != null && files.sAgeFile != null && !files.sAgeFile.isEmpty()) {
					monitor.subTask("Extracting age grid data...");

					ageReader.setFileName(files.sAgeFile);
					ElevationExtractor ageExtractor = new ElevationExtractor(ageReader, cotReader);
					try {
						ageExtractor.computeExtraction(sector, monitor);
						ageData = ageExtractor.getElevationMatrix();

						// For debug purpose, uncomment the following lines :
						// dumpAsGrayImage(ageData, ageData[0].length,
						// ageData.length,
						// "D:/age_grid.png");
					} catch (Exception e) {
						LOGGER.debug("Exception while processing age grid file", e);
						displayError(shell, "Age grid data extraction error", e.toString());
						model.loadAfterWrite = false;
						return;
					}
				}

				checkCancellation(monitor);

				tectonicLayers.add(new TectonicLayer(computeLayerName(files), period));
				// Elevation layer
				if (elevationData != null) {
					TectonicGrid elevationGrid = new TectonicGrid(sector, elevationData);
					plate.setElevationGrid(elevationGrid);
					plate.getMetadata().setZrange(elevationGrid.getDataRange());
				}

				// Texture layer
				if (textureData != null) {
					plate.setTextureGrid(new TectonicGrid(sector, textureData));
				}

				// Masking grid
				if (ageData != null) {
					plate.setMaskingGrid(new TectonicGrid(sector, ageData));
				}

				// Commentaires
				StringBuilder comments = new StringBuilder();

				String commCot = cotReader.getComments();
				if (commCot != null && !commCot.isEmpty()) {
					comments.append(commCot);
				}

				String commPole = movement != null ? movement.getComments() : "";
				if (commPole != null && !commPole.isEmpty()) {
					if (comments.length() > 0) {
						comments.append("\n\n");
					}
					comments.append("Rotation poles comments\n--------------------------------");
					comments.append(commPole);
				}

				plate.getMetadata().setCommentsPoles(comments.toString());

				monitor.subTask("Writing " + model.sOutputFile + "...");
				if (!isMetadataWritten) {
					writer.writeMetadata(tectonicModel);
					isMetadataWritten = true;
				}
				writer.writePlateGrids(plate, period, iPeriodIndex);
				iPeriodIndex++;

				checkCancellation(monitor);
			}

		} catch (CancelEvent e) {
			model.loadAfterWrite = false;
		} catch (FileFormatException e) {
			displayError(shell, "Texture extraction error", e.getMessage());
			model.loadAfterWrite = false;
		} catch (Exception e) {
			model.loadAfterWrite = false;
			LOGGER.error("I/O error during writing", e);
			displayError(shell, "Texture extraction error", e.toString());
			throw e;
		}

		// Log the end date generation
		LOGGER.info("Generation ended at " + new Date());
	}

	protected double[][] extractElevation(Sector ouputSector, ElevationReaderAdaptor elevReader,
			FileCotReader cotReader, Files files, IProgressMonitor monitor) throws Exception {
		double[][] result = extractElevationWithGdal(ouputSector, elevReader, cotReader, files, monitor);
		if (result == null) {
			// Unable to extract with gdal. Try in an other way
			elevReader.setFileName(files.sElevationFile);
			ElevationExtractor l_elevationExtractor = new ElevationExtractor(elevReader, cotReader);
			l_elevationExtractor.computeExtraction(ouputSector, monitor);
			result = l_elevationExtractor.getElevationMatrix();
		}
		return result;
	}

	/**
	 * Try to extract elevations with a gdalwarp.
	 */
	protected double[][] extractElevationWithGdal(Sector finalBoundingBox, ElevationReaderAdaptor elevReader,
			FileCotReader cotReader, Files files, IProgressMonitor monitor) throws Exception {
		RasterAttributes rasterAttributes = elevReader.getRasterAttributes();
		// Unable to perform this operation only if the file was loaded with
		// gdal
		if (rasterAttributes.getDataset() != null) {
			File tmpDir = new File(TemporaryCache.getRootPath(), getClass().getSimpleName());
			tmpDir.mkdirs();
			Dataset cutDatasetSource = null;
			Dataset cutDataset = null;
			try {
				// make a ESRI Shapefile
				File shapeFile = convertCotFiles(elevReader, cotReader, tmpDir);
				// Perform a gdalwarp on raster to clip it according the shape
				double nodataValue = Double.isNaN(rasterAttributes.getMissingValue())
						? rasterAttributes.getValidMin() - 1
						: rasterAttributes.getMissingValue();
				File cutFile = new File(tmpDir, "cutfile.tif");
				cutDatasetSource = GdalUtils.clipRaster(rasterAttributes.getDataset(), shapeFile, cutFile,
						StandardProjection.LONGLAT.proj4String(), nodataValue, true);
				// Success ?
				// redimensionner le dataset sur la boite englobante globale des différents layers
				File resizedFile = new File(tmpDir, "expandedfile.tif");

				// create shapefile for global bounding box
				File finalArea = createShapeFile(finalBoundingBox, tmpDir);

				// redimension du dataset pour s'adapter à la boite englobante totale
				cutDataset = GdalUtils.clipRaster(cutDatasetSource, finalArea, resizedFile,
						StandardProjection.LONGLAT.proj4String(), nodataValue, true);

				if (cutDataset != null && cutDataset.GetRasterCount() > 0) {
					Band band = cutDataset.GetRasterBand(1);
					final double[][] result = new double[band.GetYSize()][band.GetXSize()];
					GdalUtils.readRaster(band, (line, column, value) -> {
						if (Double.isNaN(value) || value == nodataValue) {
							result[line][column] = TecGeneratorUtil.getMissingValue();
						} else {
							result[line][column] = value;
						}
					});
					return result;
				}
			} finally {
				if (cutDataset != null) {
					cutDataset.delete();
				}
				if (cutDatasetSource != null) {
					cutDatasetSource.delete();
				}

				FileUtils.deleteDirectory(tmpDir);
			}
		}

		return null;
	}

	protected File createShapeFile(Sector sector, File tmpDir) throws GException {
		File shapeFile = new File(tmpDir, "bb.shp");
		PolygonFileInfo newPolygonFile = new PolygonFileInfo(shapeFile.getPath());
		newPolygonFile.setGdalDriverName(GdalUtils.SHP_DRIVER_NAME);

		Geometry linearRing = IPolygon.makeEmptyGeometry();
		Arrays.stream(sector.getCorners())
				.forEach(p -> linearRing.AddPoint_2D(p.longitude.degrees, p.latitude.degrees));
		var polygon = new BasicPolygon("Polygon");
		polygon.setGeometry(IPolygon.makePolygon(linearRing));
		newPolygonFile.getPolygons().add(polygon);
		polygonWriter.save(newPolygonFile);
		return shapeFile;
	}

	/**
	 * Convert the cot files into one ESRI Shapefile
	 */
	protected File convertCotFiles(ElevationReaderAdaptor elevReader, FileCotReader cotReader, File tmpDir)
			throws Exception {
		// Read .cot files
		cotReader.readCots();

		File shapeFile = new File(tmpDir, "cots.geojson");
		PolygonFileInfo newPolygonFile = new PolygonFileInfo(shapeFile.getPath());
		newPolygonFile.setGdalDriverName(GdalUtils.SHP_DRIVER_NAME);

		for (List<LatLon> latlons : cotReader.getSegmentsList()) {
			Geometry linearRing = IPolygon.makeEmptyGeometry();
			latlons.forEach(p -> linearRing.AddPoint_2D(p.longitude.degrees, p.latitude.degrees));
			var polygon = new BasicPolygon("Polygon");
			polygon.setGeometry(IPolygon.makePolygon(linearRing));
			newPolygonFile.getPolygons().add(polygon);
		}
		polygonWriter.save(newPolygonFile);
		GeoBox geobox = newPolygonFile.getGeoBox();
		elevReader.setBoundingBox(geobox.asGdalExtent());

		return shapeFile;
	}

	protected String computeLayerName(Files files) {
		if (files.getsElevationFile() != null && files.getsTextureFile() != null) {
			return FilenameUtils.getName(files.getsElevationFile()) + " / "
					+ FilenameUtils.getName(files.getsTextureFile());
		}
		if (files.getsElevationFile() != null) {
			return FilenameUtils.getName(files.getsElevationFile());
		}
		if (files.getsTextureFile() != null) {
			return FilenameUtils.getName(files.getsTextureFile());
		}
		if (files.getsAgeFile() != null) {
			return FilenameUtils.getName(files.getsAgeFile());
		}
		return "Layer";
	}

	protected void doOpenFile(Model model) {
		ITreeNodeFactory.grab().createFileInfoNode(new BasicFileInfo(model.sOutputFile));
		IFileLoadingService.grab().load(Collections.singletonList(model.sOutputFile), false);
	}

	private void checkCancellation(IProgressMonitor monitor) throws CancelEvent {
		if (monitor.isCanceled()) {
			throw new CancelEvent();
		}
	}

	protected void displayError(Shell shell, String title, String message) {
		Display.getDefault().asyncExec(() -> {
			MessageDialog.openError(shell, title, message);
		});
	}

	public static void dumpAsRGBImage(double[][] ageData, int width, int height, String filePath) {
		try {
			BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
			for (int i = 0; i < ageData.length; i++) {
				for (int j = 0; j < ageData[0].length; j++) {
					img.setRGB(j, i, (int) ageData[i][j]);
				}
			}
			ImageIO.write(img, "PNG", new File(filePath));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void dumpAsGrayImage(double[][] ageData, int width, int height, String filePath) {
		try {
			BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
			for (int i = 0; i < ageData.length; i++) {
				for (int j = 0; j < ageData[0].length; j++) {
					img.getRaster().setSample(j, i, 0, (int) ageData[i][j]);
				}
			}
			ImageIO.write(img, "PNG", new File(filePath));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
