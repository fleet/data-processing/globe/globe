package fr.ifremer.globe.placa3d.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.store.LayerStore;
import fr.ifremer.globe.placa3d.tectonic.TectonicDefinition;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.viewer3d.layers.IMovableLayer;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * this class creates a dialog box where movable layers can be associated to a pole layer.
 *
 * @author mgaro
 *
 */
public class MovableLayerDialog extends Dialog {

	/**
	 * movable layers list
	 */
	private org.eclipse.swt.widgets.List layersList;

	/**
	 * pole tectonic definition
	 */
	private TectonicDefinition tectonicDefinition;

	/**
	 * maps for associate a layer store and its name in the SWT list
	 */
	private Map<String, LayerStore> layersMap;

	/**
	 * constructor
	 *
	 * @param parentShell parent shell
	 * @param definition pole tectonic definition
	 */
	public MovableLayerDialog(Shell parentShell, TectonicDefinition definition) {
		super(parentShell);
		tectonicDefinition = definition;
		layersMap = new HashMap<>();
	}

	/**
	 * add the selected layer to the pole
	 */
	private void addLayerStore() {
		String[] sLayerList = layersList.getSelection();

		for (String layerStoreName : sLayerList) {
			tectonicDefinition.addLayerStore(layersMap.get(layerStoreName));
		}
	}

	/**
	 * refresh layer list
	 */
	private void updateList() {
		if (layersList != null) {
			layersList.removeAll();
			List<String> names = new ArrayList<>();

			List<LayerStore> layerStores = IWWLayerService.grab().getFileLayerStoreModel().getAll().stream()//
					.map(LayerStore::new) //
					.collect(Collectors.toList());

			for (LayerStore layerStore : layerStores) {
				for (Pair<Layer, ElevationModel> pair : layerStore.getLayers()) {
					Layer layer = pair.getFirst();
					if (layer instanceof IMovableLayer) {
						names.add(layer.getName());
						layersMap.put(layer.getName(), layerStore);
						break;
					}
				}
			}
			for (String name : names) {
				layersList.add(name);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			addLayerStore();
		}
		super.buttonPressed(buttonId);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonBar(org.eclipse.swt.widgets .Composite)
	 */
	@Override
	protected Control createButtonBar(Composite parent) {
		Control c = super.createButtonBar(parent);
		getButton(OK).setEnabled(false);
		return c;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, false));

		Group group = new Group(composite, SWT.NONE);
		group.setText("Layers list");
		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 10;
		layout.verticalSpacing = 10;
		group.setLayout(layout);
		group.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		layersList = new org.eclipse.swt.widgets.List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1);
		gridData.heightHint = 500;
		layersList.setLayoutData(gridData);
		updateList();

		layersList.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				getButton(OK).setEnabled(true);
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
		});

		return composite;
	}

}
