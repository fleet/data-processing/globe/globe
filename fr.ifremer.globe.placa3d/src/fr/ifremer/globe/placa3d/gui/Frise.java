package fr.ifremer.globe.placa3d.gui;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.utils.cache.WorkspaceCache;
import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.BasicWWTexture;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Offset;
import gov.nasa.worldwind.render.ScreenImage;
import gov.nasa.worldwind.render.Size;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;

public class Frise extends ScreenImage {

	private static Frise instance;

	private RenderableLayer layer = null;

	private Frise() {
		updateLayer();
	}

	public void updateLayer() {
		try {
			File friseFile = new File(WorkspaceCache.getFrisePath(), "frise.png");
			if (!friseFile.exists()) {
				URL bundleURL = FileLocator.find(Activator.getInstance().getBundle(), new Path("/icons/frise.png"), null);
				String frisePath = FileLocator.toFileURL(bundleURL).getPath();
				FileUtils.copyFile(new File(frisePath), friseFile);
			}

			// scale image to screen
			InputStream is = new FileInputStream(new File(WorkspaceCache.getFrisePath() + File.separator + "frise.png"));
			BufferedImage src = ImageIO.read(is);
			int destWidth = Viewer3D.getFrame().getWidth();
			// 1.5 to exaggerate height => reading the text
			int destHeight = destWidth * src.getHeight() / src.getWidth();
			BufferedImage dest = new BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = dest.createGraphics();
			AffineTransform at = AffineTransform.getScaleInstance((double) destWidth / src.getWidth(), (double) destHeight / src.getHeight());

			g.drawRenderedImage(src, at);
			setImageSource(dest);
			// -10 is for the panel displaying latitude and longitude at the
			// bottom of the screen
			// TODO find how to have is height
			setScreenLocation(new Point(destWidth / 2, Viewer3D.getFrame().getHeight() - destHeight / 2));
			setSize(new Size(Size.EXPLICIT_DIMENSION, destWidth, AVKey.PIXELS, Size.NATIVE_DIMENSION, destHeight, AVKey.PIXELS));
			this.layer = new RenderableLayer();
			this.layer.setName("Frise");
			this.layer.addRenderable(this);
			// hides frise at first
			this.layer.setEnabled(false);

			Viewer3D.getWwd().getModel().getLayers().add(this.layer);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// for esthetic purpose
		setOpacity(0.8);
	}

	public static Frise getInstance() {
		if (instance == null) {
			instance = new Frise();
		}
		return instance;
	}

	/**
	 * Compute the image size, rotation, and position based on the current
	 * viewport size. This method updates the calculated values for screen
	 * point, rotation point, width, and height. The calculation is not
	 * performed if the values have already been calculated for this frame.
	 *
	 * @param dc
	 *            DrawContext into which the image will be rendered.
	 */
	@Override
	protected void computeOffsets(DrawContext dc) {
		if (dc.getFrameTimeStamp() != this.frameNumber) {
			final BasicWWTexture texture = this.getTexture();

			final int drawableWidth = dc.getDrawableWidth();
			final int drawableHeight = dc.getDrawableHeight();

			// Compute image size
			if (texture != null) {
				this.originalImageWidth = texture.getWidth(dc);
				this.originalImageHeight = texture.getHeight(dc);
			} else if (this.getImageSource() == null) // If no image source is
			// set, draw a rectangle
			{
				this.originalImageWidth = 1;
				this.originalImageHeight = 1;
			} else // If an image source is set, but the image is not available
					// yet, don't draw anything
			{
				this.frameNumber = dc.getFrameTimeStamp();
				return;
			}

			this.width = drawableWidth;
			this.height = drawableWidth * this.originalImageHeight / this.originalImageWidth;

			// Compute rotation
			Offset rotationOffset = this.getRotationOffset();

			// If no rotation offset is set, rotate around the center of the
			// image.
			if (rotationOffset != null) {
				// The KML specification according to both OGC and Google states
				// that the rotation point is specified in
				// a coordinate system with the origin at the lower left corner
				// of the screen (0.5, 0.5 is the center
				// of the screen). But Google Earth interprets the point in a
				// coordinate system with origin at the lower
				// left corner of the image (0.5, 0.5 is the center of the
				// image), so we'll do that too.
				Point.Double pointD = rotationOffset.computeOffset(this.width, this.height, null, null);
				this.rotationPoint = new Point((int) pointD.x, (int) pointD.y);
			} else {
				this.rotationPoint = new Point(this.width, this.height);
			}

			// Compute position
			this.screenLocation = new Point(this.width / 2, this.height / 2);

			// Convert the screen location from OpenGL to AWT coordinates and
			// store the result in awtScreenLocation. The
			// awtScreenLocation property is used in getScreenLocation to
			// indicate the screen location in AWT
			// coordinates.
			this.awtScreenLocation = new Point(this.screenLocation.x, drawableHeight - this.screenLocation.y);

			Point.Double overlayPoint;
			if (this.imageOffset != null) {
				overlayPoint = this.imageOffset.computeOffset(this.width, this.height, null, null);
			} else {
				overlayPoint = new Point.Double(this.originalImageWidth / 2.0, this.originalImageHeight / 2.0);
			}

			this.dx = -overlayPoint.x;
			this.dy = -overlayPoint.y;

			this.frameNumber = dc.getFrameTimeStamp();
		}
	}

	/***
	 * if frise is hidden : display if frise is displayed : hide
	 *
	 * @return
	 */
	public boolean display() {
		if (this.layer != null) {
			this.layer.setEnabled(!this.layer.isEnabled());
			return this.layer.isEnabled();
		}
		return false;
	}

	public boolean isDisplayed() {
		if (this.layer != null) {
			return this.layer.isEnabled();
		}
		return false;
	}
}
