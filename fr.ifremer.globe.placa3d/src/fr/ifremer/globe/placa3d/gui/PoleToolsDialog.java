package fr.ifremer.globe.placa3d.gui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;

//public class PoleToolsDialog extends Dialog implements MouseListener {
public class PoleToolsDialog extends Dialog {

	/** logger */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(PoleToolsDialog.class);

	/**
	 * The pole compute composite of the second tab item
	 */
	PoleComputeComposite poleComputeComposite;

	/**
	 * Constructor
	 */
	public PoleToolsDialog(Shell shell) {
		super(shell);
		// non-modal dialog
		setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Tectonic configuration");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		// create Close button buttons
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CLOSE_LABEL, true);

	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		final Composite composite = (Composite) super.createDialogArea(parent);
		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		composite.setLayoutData(gd);
		GridLayout layout = new GridLayout(1, false);
		layout.horizontalSpacing = 10;
		composite.setLayout(layout);

		// tabfolder
		CTabFolder tabfolder = new CTabFolder(composite, SWT.TOP);
		tabfolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// Pole Configuration Tab
		CTabItem poleConfigurationTab = new CTabItem(tabfolder, SWT.NONE);
		poleConfigurationTab.setText("Poles");
		// Pole Configuration group
		Group poleConfigurationTabGroup = new Group(tabfolder, SWT.NONE);
		poleConfigurationTabGroup.setText("Poles");
		poleConfigurationTab.setControl(new PoleConfigurationComposite(tabfolder, SWT.NONE, this));

		// Offsets. tab
		CTabItem offsetConfigurationTab = new CTabItem(tabfolder, SWT.NONE);
		offsetConfigurationTab.setText("Offsets");

		Composite compositeOffset = new Composite(tabfolder, SWT.NONE);
		compositeOffset.setLayout(new GridLayout());
		compositeOffset.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		Group offsetGroup = new Group(compositeOffset, SWT.NONE);
		offsetGroup.setLayout(new GridLayout());
		offsetGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

		// check box to synchronize all elevation offsets
		Button syncAllButton = new Button(offsetGroup, SWT.CHECK);
		syncAllButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 5, 5));
		syncAllButton.setText("Synchronize all tectonic plates' offsets");
		syncAllButton.setToolTipText("Check this to apply the last tectonic plate's offset to all tectonic plates");
		syncAllButton.setSelection(TectonicDataset.getInstance().getConfiguration().isSynchronizeOffsets());

		Button applyButton = new Button(offsetGroup, SWT.PUSH);
		applyButton.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, false, 5, 5));
		applyButton.setText("Apply");
		applyButton.setToolTipText("Click to apply changes (otherwise changes will be discarded)");
		applyButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				TectonicDataset.getInstance().getConfiguration().setSynchronizeOffsets(syncAllButton.getSelection());
				TectonicDataset.updatePoleConfiguration();
			}
		});

		offsetGroup.setText("Offsets");
		offsetConfigurationTab.setControl(compositeOffset);

		// Pole Compute Tab
		CTabItem poleComputeTab = new CTabItem(tabfolder, SWT.NONE);
		poleComputeTab.setText("Pole tools");
		// Pole Compute group
		Group poleComputeTabGroup = new Group(tabfolder, SWT.NONE);
		poleComputeTabGroup.setText("Pole tools");
		poleComputeComposite = new PoleComputeComposite(tabfolder, SWT.NONE);
		poleComputeTab.setControl(poleComputeComposite);

		applyDialogFont(composite);

		return composite;
	}

	@Override
	public boolean close() {
		// Call the close method of poleComputeComposite
		poleComputeComposite.close();

		return super.close();

	}
}
