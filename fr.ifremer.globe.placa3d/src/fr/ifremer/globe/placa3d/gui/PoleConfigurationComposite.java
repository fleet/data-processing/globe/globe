package fr.ifremer.globe.placa3d.gui;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.placa3d.tectonic.TectonicConfiguration;
import fr.ifremer.globe.placa3d.tectonic.TectonicDataset;
import fr.ifremer.viewer3d.Viewer3D;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

public class PoleConfigurationComposite extends Composite {

	/**
	 * The parent dialog
	 */
	private PoleToolsDialog parentDialog;

	/**
	 * Graphic component declaration
	 */
	private Button allPlatesButton;
	private Combo displayConfigCombo;
	private Button radioTot;
	private Button radioInt;

	/** The configuration for poles of rotation. */
	private TectonicConfiguration poleConfiguration;

	/** True if all plates are rotated together, false else. */
	private boolean allPlates;

	/**
	 * Display mode :
	 * 
	 * 0 : display selected rotation poles
	 * 
	 * 1 : display all rotation poles
	 * 
	 * 2 : hide all rotation poles
	 */
	private int displayPoleMode;

	/** True if total poles are displaying. */
	private boolean displayTotPoles;

	/** True if intermediate poles are displaying. */
	private boolean displayIntPoles;

	public PoleConfigurationComposite(Composite parent, int style, PoleToolsDialog poleToolsDialog) {
		super(parent, style);
		parentDialog = poleToolsDialog;
		init();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		// main layout
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		this.setLayout(layout);

		// init variables
		poleConfiguration = TectonicDataset.getInstance().getConfiguration();
		allPlates = poleConfiguration.isAllPlates();
		displayPoleMode = poleConfiguration.getDisplayPoleMode();
		displayTotPoles = poleConfiguration.isDisplayTotPoles();
		displayIntPoles = poleConfiguration.isDisplayIntPoles();

		createPoleConfiguration();
	}

	private Composite createPoleConfiguration() {
		final Composite composite = new Composite(this, SWT.NONE);
		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = false;
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		composite.setLayoutData(gd);
		GridLayout layout = new GridLayout(1, false);
		layout.horizontalSpacing = 10;
		composite.setLayout(layout);

		// Create all plate group
		final Group allPlateGroup = new Group(composite, SWT.NONE);
		allPlateGroup.setText("Plates animation");
		allPlateGroup.setLayout(new GridLayout(1, false));
		allPlateGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// Create pole configuration group
		final Group poleConfigGroup = new Group(composite, SWT.NONE);
		poleConfigGroup.setText("Rotation poles display");
		poleConfigGroup.setLayout(new GridLayout(1, false));
		poleConfigGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// Create all plates check box
		allPlatesButton = new Button(allPlateGroup, SWT.CHECK);
		allPlatesButton.setText("Move all plates together");
		allPlatesButton.setToolTipText("Check to synchronize all plates when earth age changes");
		// allPlatesButton.setSelection(poleConfiguration.isAllPlates());
		allPlatesButton.setSelection(allPlates);
		allPlatesButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				allPlates = allPlatesButton.getSelection();
			}
		});

		// Create combo configuration
		displayConfigCombo = new Combo(poleConfigGroup, SWT.READ_ONLY);
		final String values[] = { "Display selected rotation poles", "Display all rotation poles", "Hide all rotation poles" };
		displayConfigCombo.setToolTipText("Poles display configuration");
		displayConfigCombo.setItems(values);
		displayConfigCombo.select(poleConfiguration.getDisplayPoleMode());
		displayConfigCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				displayPoleMode = displayConfigCombo.getSelectionIndex();
			}
		});

		// Create radio button total or intermediate
		radioTot = new Button(poleConfigGroup, SWT.RADIO);
		radioTot.setText("Total pole");
		radioTot.setSelection(poleConfiguration.isDisplayTotPoles());
		radioTot.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				displayTotPoles = radioTot.getSelection();
				displayIntPoles = !radioTot.getSelection();

			}
		});
		radioInt = new Button(poleConfigGroup, SWT.RADIO);
		radioInt.setText("Intermediate pole");
		radioInt.setSelection(poleConfiguration.isDisplayIntPoles());
		radioInt.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				displayTotPoles = !radioInt.getSelection();
				displayIntPoles = radioInt.getSelection();
			}
		});

		createButtonsForButtonBar(composite);

		// applyDialogFont(composite);

		return composite;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		// main layout
		// GridLayout layout = new GridLayout(1, true);
		GridLayout layout = new GridLayout();
		// layout.marginHeight = 0;
		// layout.marginWidth = 0;
		composite.setLayout(layout);

		// create OK and Cancel buttons
		createButton(composite, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		// createButton(composite, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		//
		// enableOKButton(true);

	}

	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		// increment the number of columns in the button bar
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		GridData gd_button = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_button.widthHint = 111;
		button.setLayoutData(gd_button);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				buttonPressed(((Integer) event.widget.getData()).intValue());
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		return button;
	}

	protected void buttonPressed(int buttonId) {
		// this.getShell().close();

		if (buttonId == IDialogConstants.OK_ID) {
			// LOGGER.debug("OK");

			// Set the poleconfiguration
			poleConfiguration.setAllPlates(allPlates);
			poleConfiguration.setDisplayPoleMode(displayPoleMode);
			poleConfiguration.setDisplayTotPoles(displayTotPoles);
			poleConfiguration.setDisplayIntPoles(displayIntPoles);

			// If display intermediate poles, the user have to select a position
			// on the globe to display it
			if (poleConfiguration.isDisplayIntPoles() && displayPoleMode != 2) {
				MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Display Intermediate poles of rotation",
						"Right click on the globe to select the latitude and longitude from which will calculate the intermediate rotation.");

				Viewer3D.getWwd().setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

				// Add mouse listener to select a position on globe with
				// mousePressed()
				Viewer3D.getWwd().getInputHandler().addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent mouseEvent) {
						if (mouseEvent.getButton() == MouseEvent.BUTTON3) {

							// Select a position on the globe
							Position currentPosition = Viewer3D.getWwd().getCurrentPosition();
							if (currentPosition != null) {
								poleConfiguration.setLatlon(new LatLon(currentPosition));
							}

							Viewer3D.getWwd().getInputHandler().removeMouseListener(this);
							Viewer3D.getWwd().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						}
						TectonicDataset.updatePoleConfiguration();
					}
				});

			} else {
				TectonicDataset.updatePoleConfiguration();
			}

			parentDialog.close();
		}
	}

}
