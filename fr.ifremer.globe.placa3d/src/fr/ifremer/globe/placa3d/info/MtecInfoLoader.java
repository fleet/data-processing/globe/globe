package fr.ifremer.globe.placa3d.info;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.raster.IRasterInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.mtec.MultiLayerTecFile;
import fr.ifremer.globe.placa3d.reader.MultiLayerTecFileReader;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Loader of MTEC file info. Builder of MtecInfo
 */
@Component(name = "globe_placa_mtec_info_supplier", service = { MtecInfoLoader.class, IFileInfoSupplier.class,
		IRasterInfoSupplier.class })
public class MtecInfoLoader extends BasicFileInfoSupplier<MtecInfo> implements IRasterInfoSupplier {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(MtecInfoLoader.class);

	/** MtecInfo are cached because usefull informations are stored of them */
	protected Map<String, WeakReference<MtecInfo>> infosCache = new HashMap<>();

	/**
	 * Constructor
	 */
	public MtecInfoLoader() {
		super(MultiLayerTecFile.FILE_EXTENSION,
				MultiLayerTecFile.FILE_DESCRIPTION + " (*." + MultiLayerTecFile.FILE_EXTENSION + ")", ContentType.MTEC);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<MtecInfo> getFileInfo(String filePath) {
		MtecInfo result = null;
		if (MultiLayerTecFile.FILE_EXTENSION.equalsIgnoreCase(FilenameUtils.getExtension(filePath))) {
			WeakReference<MtecInfo> reference = infosCache.get(filePath);
			if (reference == null || reference.isEnqueued() || reference.get() == null) {
				try {
					result = loadInfo(filePath);
					infosCache.put(filePath, new WeakReference<>(result));
				} catch (Exception e) {
					logger.warn("Error on {} loading : {}", filePath, e.getMessage());
				}
			} else {
				result = reference.get();
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * @return the GdalRasterInfo of the specified resource. null if filePath is not managed by Gdal
	 * @throws Exception read failed, error occured
	 */
	protected MtecInfo loadInfo(String filePath) throws Exception {
		File file = new File(filePath);
		try (MultiLayerTecFileReader reader = new MultiLayerTecFileReader(file)) {
			MtecInfo mtecInfo = new MtecInfo(filePath);
			TectonicModel tectonicModel = mtecInfo.getTectonicModel();

			logger.debug("Opening file {}", file.getAbsolutePath());
			reader.open();

			logger.debug("Reading {} metadata...", file.getAbsolutePath());
			reader.loadMetadata(tectonicModel);

			return mtecInfo;
		}
	}

	/** {@inheritDoc} */
	@Override
	public List<RasterInfo> supplyRasterInfos(String filepath) throws GIOException {
		Optional<MtecInfo> info = getFileInfo(filepath);
		return info.isPresent() ? supplyRasterInfos(info.get()) : Collections.emptyList();
	}

	/** {@inheritDoc} */
	@Override
	public List<RasterInfo> supplyRasterInfos(IFileInfo fileInfo) {
		List<RasterInfo> result = new ArrayList<>();
		if (fileInfo instanceof MtecInfo) {
			TectonicModel tectonicModel = ((MtecInfo) fileInfo).getTectonicModel();
			for (TectonicPlateModel plate : tectonicModel.getPlates()) {
				if (plate.hasElevation()) {
					RasterInfo rasterInfo = new RasterInfo(this, fileInfo.getPath());
					rasterInfo.setDataType(RasterInfo.RASTER_ELEVATION);
					rasterInfo.setXSize(plate.getElevationGridDimension().width);
					rasterInfo.setYSize(plate.getElevationGridDimension().height);
					if (plate.getZRange() != null) {
						rasterInfo.setMinMaxValues(plate.getZRange());
					}
					result.add(rasterInfo);
				}
			}
		}
		return result;
	}
}