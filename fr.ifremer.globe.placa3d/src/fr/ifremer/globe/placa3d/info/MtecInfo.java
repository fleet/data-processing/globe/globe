package fr.ifremer.globe.placa3d.info;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.mtec.MultiLayerTecFile;
import fr.ifremer.globe.placa3d.store.TectonicPlateLayerStore;

/**
 * IInfoStore dedicated to .mtec file.
 */
public class MtecInfo extends BasicFileInfo {

	/** Model of plates */
	protected TectonicModel tectonicModel = new TectonicModel();
	/** All LayerStores of plates */
	protected Set<TectonicPlateLayerStore> plateLayerStores = new HashSet<>();

	/**
	 * Constructor
	 */
	public MtecInfo(String path) {
		super(path, ContentType.MTEC);
	}

	/**
	 * @see fr.ifremer.globe.model.infostores.IInfos#getProperties()
	 */
	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> result = new ArrayList<>();
		result.add(Property.build("File type", MultiLayerTecFile.FILE_DESCRIPTION));
		result.add(Property.build("File format", "HDF5/" + MultiLayerTecFile.FILE_EXTENSION));
		result.add(
				Property.build("File size", org.apache.commons.io.FileUtils.byteCountToDisplaySize(toFile().length())));

		for (TectonicPlateModel plate : getTectonicModel().getPlates()) {
			List<Property<?>> properties = plate.getProperties();
			if (!properties.isEmpty()) {
				Property<String> plateDesc = Property.build(plate.getPlateName(), "");
				result.add(plateDesc);
				properties.stream().forEach(plateDesc::add);
			}
		}

		return result;
	}

	/**
	 * @return the {@link #tectonicModel}
	 */
	public TectonicModel getTectonicModel() {
		return tectonicModel;
	}

	public void addPlateLayerStore(TectonicPlateLayerStore plateLayerStore) {
		plateLayerStores.add(plateLayerStore);
	}

	/**
	 * @return the {@link #plateLayerStores}
	 */
	public Set<TectonicPlateLayerStore> getPlateLayerStores() {
		return plateLayerStores;
	}

}