/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.info;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;

/**
 * Loader of Cot file info.
 */
@Component(name = "globe_placa_cot_info_supplier", service = { CotInfoSupplier.class, IFileInfoSupplier.class })
public class CotInfoSupplier extends BasicFileInfoSupplier<BasicFileInfo> {

	/**
	 * Constructor
	 */
	public CotInfoSupplier() {
		super("cot", "*.cot", ContentType.COT);
	}
}
