/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.info;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;

/**
 *
 */
@Component(name = "globe_placa_inf_info_supplier", service = { InfInfoLoader.class, IFileInfoSupplier.class })
public class InfInfoLoader extends BasicFileInfoSupplier<BasicFileInfo> {

	/**
	 * Constructor
	 */
	public InfInfoLoader() {
		super("inf", "PolInt", ContentType.POLE_INF);
	}

}
