/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.info;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;

/**
 *
 */
@Component(name = "globe_placa_tot_info_supplier", service = { TotInfoLoader.class, IFileInfoSupplier.class })
public class TotInfoLoader extends BasicFileInfoSupplier<BasicFileInfo> {

	/**
	 * Constructor
	 */
	public TotInfoLoader() {
		super("tot", "PolAbs", ContentType.POLE_TOT);
	}

}
