package fr.ifremer.globe.placa3d.render;

import fr.ifremer.globe.placa3d.layers.TectonicRemanentLayer;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.viewer3d.data.MyRectangularTessellator;
import fr.ifremer.viewer3d.geom.MovableSector;
import fr.ifremer.viewer3d.layers.deprecated.dtm.MyTextureTile;
import fr.ifremer.viewer3d.util.PrivateAccessor;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.GeographicSurfaceTileRenderer;
import gov.nasa.worldwind.render.SurfaceTile;
import gov.nasa.worldwind.terrain.SectorGeometry;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.OGLUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Level;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLProfile;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;

/**
 * Displays remanent layers with a shader defining the color map
 * 
 * @author MORVAN
 * 
 */
public class TectonicRemanentRenderer extends GeographicSurfaceTileRenderer {
	private int _shaderProgram = 0;
	private int _dateIndex = 0;
	private int _dateIndexmax = 0;
	private double opacity;
	private static final int DEFAULT_SHADER_ALPHA_TEXTURE_SIZE = 1024;
	private TectonicRemanentLayer layer = null;

	public TectonicRemanentRenderer(TectonicRemanentLayer layer) {
		this.layer = layer;
	}

	public void setDateIndex(int dateIndex) {
		_dateIndex = dateIndex;
	}

	public void setDateIndexmax(int _dateIndexmax) {
		this._dateIndexmax = _dateIndexmax;
	}

	@Override
	public void renderTiles(DrawContext dc, Iterable<? extends SurfaceTile> tiles) {
		if (tiles == null) {
			String message = Logging.getMessage("nullValue.TileIterableIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		if (dc == null) {
			String message = Logging.getMessage("nullValue.DrawContextIsNull");
			Logging.logger().severe(message);
			throw new IllegalStateException(message);
		}

		GL2 gl = dc.getGL().getGL2();
		int alphaTextureUnit = GL.GL_TEXTURE1;
		boolean showOutlines = this.isShowImageTileOutlines() && dc.getGLRuntimeCapabilities().getNumTextureUnits() > 2;

		gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
				| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for
				// depth
				// func
				| GL2.GL_TRANSFORM_BIT);

		try {
			this.alphaTexture = dc.getTextureCache().getTexture(this);
			if (this.alphaTexture == null) {
				this.initAlphaTexture(dc, DEFAULT_SHADER_ALPHA_TEXTURE_SIZE); // TODO:
				// choose
				// size
				// to
				// match
				// incoming
				// tile
				// sizes?
				dc.getTextureCache().put(this, this.alphaTexture);
			}

			if (showOutlines && this.outlineTexture == null) {
				this.initOutlineTexture(dc, 128);
			}

			gl.glEnable(GL.GL_DEPTH_TEST);
			gl.glDepthFunc(GL.GL_LEQUAL);

			gl.glEnable(GL2ES1.GL_ALPHA_TEST);
			gl.glAlphaFunc(GL.GL_GREATER, 0.01f);

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glEnable(GL.GL_TEXTURE_2D);
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glPushMatrix();
			if (!dc.isPickingMode()) {
				gl.glTexEnvi(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, GL2ES1.GL_MODULATE);
			} else {
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, GL2ES1.GL_COMBINE);
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_SRC0_RGB, GL2ES1.GL_PREVIOUS);
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_COMBINE_RGB, GL.GL_REPLACE);
			}

			int numTexUnitsUsed = 2;
			if (showOutlines) {
				numTexUnitsUsed = 3;
				alphaTextureUnit = GL.GL_TEXTURE2;
				gl.glActiveTexture(GL.GL_TEXTURE1);
				gl.glEnable(GL.GL_TEXTURE_2D);
				gl.glMatrixMode(GL.GL_TEXTURE);
				gl.glPushMatrix();
				gl.glTexEnvi(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, GL2ES1.GL_ADD);
			}

			gl.glActiveTexture(alphaTextureUnit);
			gl.glEnable(GL.GL_TEXTURE_2D);
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glPushMatrix();
			gl.glTexEnvi(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, GL2ES1.GL_MODULATE);

			dc.getSurfaceGeometry().beginRendering(dc);

			// For each current geometry tile, find the intersecting image tiles
			// and render the geometry
			// tile once for each intersecting image tile.
			Transform transform = new Transform();
			for (SectorGeometry sg : dc.getSurfaceGeometry()) {
				Iterable<SurfaceTile> tilesToRender = this.getIntersectingTiles(dc, sg, tiles);
				if (tilesToRender == null) {
					continue;
				}
				try {
					if (sg instanceof MyRectangularTessellator.RectTile) {
						MyRectangularTessellator.RectTile myTile = (MyRectangularTessellator.RectTile) sg;
						myTile.beginRendering(dc, numTexUnitsUsed); // TODO:
						// wrap in
						// try/catch
						// in case
						// of
						// exception
					} else {
						sg.beginRendering(dc, numTexUnitsUsed); // TODO: wrap in
						// try/catch in
						// case of
						// exception
					}
				} catch (Exception e) {
					Logging.logger().log(Level.SEVERE, "SurfaceTileRenderer begin rendering", e);
				}

				// Pre-load info to compute the texture transform
				this.preComputeTextureTransform(dc, sg, transform);

				// For each intersecting tile, establish the texture transform
				// necessary to map the image tile
				// into the geometry tile's texture space. Use an alpha texture
				// as a mask to prevent changing the
				// frame buffer where the image tile does not overlap the
				// geometry tile. Render both the image and
				// alpha textures via multi-texture rendering.
				// TODO: Figure out how to apply multi-texture to more than one
				// tile at a time. Use fragment shader?
				for (SurfaceTile tile : tilesToRender) {
					gl.glActiveTexture(GL.GL_TEXTURE0);

					if (tile.bind(dc)) {
						gl.glMatrixMode(GL.GL_TEXTURE);
						gl.glLoadIdentity();
						tile.applyInternalTransform(dc, true);

						// Determine and apply texture transform to map image
						// tile into geometry tile's texture space
						Sector tileSector = this.computeTextureTransform(dc, tile, transform, sg);
						gl.glScaled(transform.HScale, transform.VScale, 1d);
						gl.glTranslated(transform.HShift, transform.VShift, 0d);

						// Prepare the alpha texture to be used as a mask where
						// texture coords are outside [0,1]
						gl.glActiveTexture(alphaTextureUnit);
						this.alphaTexture.bind(dc.getGL());

						// Apply the same texture transform to the alpha
						// texture. The alpha texture uses a
						// different texture unit than the tile, so the
						// transform made above does not carry over.
						gl.glMatrixMode(GL.GL_TEXTURE);
						gl.glLoadIdentity();
						gl.glScaled(transform.HScale, transform.VScale, 1d);
						gl.glTranslated(transform.HShift, transform.VShift, 0d);

						// Render the geometry tile
						if (tile instanceof MyTextureTile) {
							MyTextureTile textureTile = (MyTextureTile) tile;
							// if(textureTile.getLevel().getLevelNumber() >= 4)
							{
								Texture texture = textureTile.getTexture(dc.getTextureCache());
								// gl.glGetTexImage(arg0, arg1, arg2, arg3,
								// arg4)
								if (texture != null) {

									gl.glUseProgram(_shaderProgram);
									int shader_dateIndex = gl.glGetUniformLocation(_shaderProgram, "dateindex");

									gl.glUniform1f(shader_dateIndex, (float) ((float) _dateIndex / (float) _dateIndexmax * 255.0));

									int shader_opacity = gl.glGetUniformLocation(_shaderProgram, "opacity");

									gl.glUniform1f(shader_opacity, (float) opacity);

									int shader_width = gl.glGetUniformLocation(_shaderProgram, "width");
									int shader_height = gl.glGetUniformLocation(_shaderProgram, "height");

									gl.glUniform1f(shader_width, ((MyTextureTile) tile).getWidth());
									gl.glUniform1f(shader_height, ((MyTextureTile) tile).getHeight());

									shaderParametersForMovement(gl, _shaderProgram, tile.getSector(), tileSector, sg, transform);

									sg.renderMultiTexture(dc, numTexUnitsUsed);

									gl.glUseProgram(0);
								} else {
									sg.renderMultiTexture(dc, numTexUnitsUsed);
								}
							}
						} else {
							sg.renderMultiTexture(dc, numTexUnitsUsed);
						}
					}
				}
				try {
					if (sg instanceof MyRectangularTessellator.RectTile) {
						MyRectangularTessellator.RectTile myTile = (MyRectangularTessellator.RectTile) sg;
						myTile.endRendering(dc); // TODO: wrap in try/catch in
						// case of exception
					} else {
						sg.endRendering(dc); // TODO: wrap in try/catch in case
						// of exception
					}
				} catch (Throwable e) {
					Logging.logger().log(Level.SEVERE, "SurfaceTileRenderer begin rendering", e);
				}

			}
		} catch (Exception e) {
			Logging.logger().log(Level.SEVERE, Logging.getMessage("generic.ExceptionWhileRenderingLayer", this.getClass().getName()), e);
		} finally {
			dc.getSurfaceGeometry().endRendering(dc);

			gl.glActiveTexture(alphaTextureUnit);
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glPopMatrix();
			gl.glDisable(GL.GL_TEXTURE_2D);

			if (showOutlines) {
				gl.glActiveTexture(GL.GL_TEXTURE1);
				gl.glMatrixMode(GL.GL_TEXTURE);
				gl.glPopMatrix();
				gl.glDisable(GL.GL_TEXTURE_2D);
			}

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glMatrixMode(GL.GL_TEXTURE);
			gl.glPopMatrix();
			gl.glDisable(GL.GL_TEXTURE_2D);

			gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_TEXTURE_ENV_MODE, OGLUtil.DEFAULT_TEX_ENV_MODE);
			if (dc.isPickingMode()) {
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_SRC0_RGB, OGLUtil.DEFAULT_SRC0_RGB);
				gl.glTexEnvf(GL2ES1.GL_TEXTURE_ENV, GL2ES1.GL_COMBINE_RGB, OGLUtil.DEFAULT_COMBINE_RGB);
			}

			gl.glPopAttrib();
		}
	}

	public int getShaderProgram() {
		return _shaderProgram;
	}

	public void setShaderProgram(int shaderProgram) {
		_shaderProgram = shaderProgram;
	}

	protected TextureData initTexture(TextureData textureDataOriginale, int width, int height) {
		ByteBuffer textureBytes = Buffers.newDirectByteBuffer(width * height);
		for (int i = 0; i < textureBytes.capacity(); i++) {
			textureBytes.put((byte) 0xff);
		}

		GLProfile glprofile = GLProfile.getDefault();
		TextureData textureData = new TextureData(glprofile, GL.GL_RGBA, width, height, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, false, false, false, textureBytes.rewind(), null);

		return textureData;
	}

	public void setOpacity(double opacity) {
		this.opacity = opacity;
	}

	protected Sector computeTextureTransform(DrawContext dc, SurfaceTile tile, Transform t, SectorGeometry sg) {
		Sector st = tile.getSector();

		// System.out.println("original sector : " + st.toString());
		// rotation for tectonic movement defined in placa3d plugin
		st = layer.move(st);
		// System.out.println("moved sector : " + st.toString());

		Sector s = st;
		if (st instanceof MovableSector) {
			st = sg.getSector();
		}

		// System.out.println("sector geometry : " + sg.getSector().toString());
		// System.out.println("bounding sector : " + st.toString());

		double sgHeight = (Double) PrivateAccessor.getField(this, "sgHeight", GeographicSurfaceTileRenderer.class);
		double sgWidth = (Double) PrivateAccessor.getField(this, "sgWidth", GeographicSurfaceTileRenderer.class);
		double sgMinSN = (Double) PrivateAccessor.getField(this, "sgMinSN", GeographicSurfaceTileRenderer.class);
		double sgMinWE = (Double) PrivateAccessor.getField(this, "sgMinWE", GeographicSurfaceTileRenderer.class);

		double tileWidth = st.getDeltaLonRadians();
		double tileHeight = st.getDeltaLatRadians();

		double minLon = st.getMinLongitude().radians;
		double minLat = st.getMinLatitude().radians;

		double vShift = -(minLat - sgMinSN) / sgHeight;
		double hShift = -(minLon - sgMinWE) / sgWidth;

		t.VScale = tileHeight > 0 ? sgHeight / tileHeight : 1;
		t.HScale = tileWidth > 0 ? sgWidth / tileWidth : 1;

		t.VShift = vShift;
		t.HShift = hShift;

		/*
		 * if(ssd instanceof MovableSector) { double heading =
		 * -Math.toRadians(((MovableSector) ssd).getHeading());// *
		 * (1-(Math.PI/2-oriLon)/(2*Math.PI));
		 * 
		 * t.rotationDegrees = Math.toDegrees(heading); t.HShift =
		 * hShift*Math.cos(heading) - vShift*Math.sin(heading); t.VShift =
		 * hShift*Math.sin(heading) + vShift*Math.cos(heading); }
		 */
		return s;
	}

	@Override
	protected Iterable<SurfaceTile> getIntersectingTiles(DrawContext dc, SectorGeometry sg, Iterable<? extends SurfaceTile> tiles) {
		ArrayList<SurfaceTile> intersectingTiles = null;

		for (SurfaceTile tile : tiles) {
			// sector is rotated by tectonic movement
			Sector s = layer.move(tile.getSector());
			if (s instanceof MovableSector) {
				s = sg.getSector();
				if (!s.intersects(sg.getSector())) {
					continue;
				}
			} else if (!layer.move(tile.getSector()).intersectsInterior(sg.getSector())) {
				continue;
			}

			if (intersectingTiles == null) {
				intersectingTiles = new ArrayList<SurfaceTile>();
			}

			intersectingTiles.add(tile);
		}

		return intersectingTiles; // will be null if no intersecting tiles
	}

	// copy from GeographicSurfaceTileRenderer and SurfaceTileRenderer because
	// of private accessor
	public static class Transform {
		public double HScale;
		public double VScale;
		public double HShift;
		public double VShift;
		public double rotationDegrees;
	}

	protected void preComputeTextureTransform(DrawContext dc, SectorGeometry sg, Transform t) {
		Sector st = sg.getSector();
		PrivateAccessor.setField(this, "sgWidth", GeographicSurfaceTileRenderer.class, st.getDeltaLonRadians());
		PrivateAccessor.setField(this, "sgHeight", GeographicSurfaceTileRenderer.class, st.getDeltaLatRadians());
		PrivateAccessor.setField(this, "sgMinWE", GeographicSurfaceTileRenderer.class, st.getMinLongitude().radians);
		PrivateAccessor.setField(this, "sgMinSN", GeographicSurfaceTileRenderer.class, st.getMinLatitude().radians);
	}

	/***
	 * Transfer parameters to GPU to compute movement</br>
	 * 
	 * @param gl
	 * @param shaderProgram
	 * @param originalSector
	 *            : the beginning position
	 * @param boundingBox
	 *            : the bounding box on which the texture is binded
	 * @param transform
	 */
	void shaderParametersForMovement(GL gl1, int shaderProgram, Sector originalSector, Sector boundingBox, SectorGeometry sg, Transform transform) {
		GL2 gl = gl1.getGL2();
		// Must be a tectonic movement
		TectonicMovement movement = (TectonicMovement) layer.getMovement();

		// TODO add condition : the first position must not be computed
		if (movement != null && boundingBox != null && boundingBox instanceof MovableSector) {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "computeMovement"), (float) 1.0);
		} else {
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "computeMovement"), (float) 0.0);
			// no need to transfer unneeded parameters
			return;
		}

		// System.out.println("enter shader sector : " +
		// boundingBox.toString());
		boundingBox = sg.getSector();
		// System.out.println("bounding shader sector : " +
		// boundingBox.toString()+ "\n");

		Matrix m = movement.getMovementInv(_dateIndex);

		// Check if referential movement exists
		if (movement.getReferentialMovement() != null) {
			m = m.multiply(movement.getReferentialMovement().getMovement(_dateIndex));
		}

		// deplacement Matrix
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m11"), (float) m.m11);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m12"), (float) m.m12);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m13"), (float) m.m13);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m14"), (float) m.m14);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m21"), (float) m.m21);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m22"), (float) m.m22);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m23"), (float) m.m23);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m24"), (float) m.m24);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m31"), (float) m.m31);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m32"), (float) m.m32);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m33"), (float) m.m33);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m34"), (float) m.m34);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m41"), (float) m.m41);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m42"), (float) m.m42);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m43"), (float) m.m43);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "m44"), (float) m.m44);

		// texture bounding box
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "latitudeSouth"), (float) boundingBox.getMinLatitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "latitudeNorth"), (float) boundingBox.getMaxLatitude().degrees);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "longitudeWest"), (float) boundingBox.getMinLongitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "longitudeEast"), (float) boundingBox.getMaxLongitude().degrees);

		// original position
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLatitudeSouth"), (float) originalSector.getMinLatitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLatitudeNorth"), (float) originalSector.getMaxLatitude().degrees);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLongitudeWest"), (float) originalSector.getMinLongitude().degrees);
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "originalLongitudeEast"), (float) originalSector.getMaxLongitude().degrees);

	}
}
