package fr.ifremer.globe.placa3d.render.cot;

import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoPoint;

/**
 * This class describe an object with list of points for cot file.
 * @author pmahoudo
 *
 */
public class CotPointList extends CotObject {

	public CotPointList(List<GeoPoint> pointList) {
		super(pointList);
	}

}
