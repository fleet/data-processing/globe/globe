package fr.ifremer.globe.placa3d.render.cot;

import fr.ifremer.viewer3d.layers.IMovableLayer;
import fr.ifremer.viewer3d.util.processing.ISectorMovementProcessing;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.SurfacePolyline;
import gov.nasa.worldwind.util.SurfaceTileDrawContext;

import java.util.ArrayList;
import java.util.List;

/***
 * CotSegmentRenderer : equivalent to an unmovable SurfacePolyline used to rotate
 * tectonic plates
 * 
 * @author pmahoudo
 * 
 */
public class CotSegmentRenderer extends SurfacePolyline implements IMovableLayer {

	/***
	 * defines a polygon which could be moved through tectonic movement
	 * 
	 * @see TectonicMovement
	 * @see IMovableLayer
	 * @see addLocation to define the polygon boundary
	 */
	public CotSegmentRenderer() {
	}

	/***
	 * points defining the plate nowadays
	 */
	private List<Position> currentTimePosition = null;

	/***
	 * the movement defining the tectonic
	 */
	private ISectorMovementProcessing movement;
	/***
	 * date of the previous rendered movement
	 */
	@SuppressWarnings("unused")
	private int previousDate = -1;
	
	
	/** {@inheritDoc} */
	@Override
	public void render(DrawContext dc) {
		super.render(dc);

		// if movement index not changed, no need to recompute position
		if (currentTimePosition != null && movement != null && movement.hasMovement()) {
				List<LatLon> rotated = new ArrayList<LatLon>();
				for (Position current : currentTimePosition) {
					rotated.add(movement.move(current.getLatitude(), current.getLongitude()));
				}
				this.setLocations(rotated);
				
			previousDate = movement.getDateIndex();
		}
	}
	
	
	/**
	 * Add new location to the location list
	 * 
	 * @param location
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addLocation(Position location) {
		if (location == null) {
			return;
		}

		Iterable<? extends LatLon> b = this.getLocations();
		if (b == null) {
			List<Position> list = new ArrayList<Position>();
			list.add(location);
			this.setLocations(list);
			currentTimePosition = list;
		} else if (b instanceof List<?>) {
			List pts = (List) b;
			pts.add(location);
		}

		this.onShapeChanged();
	}
	
	// refresh even when mouse is not on opengl panel
	@Override
	public void setVisible(boolean enabled) {
		super.setVisible(enabled);
		this.onShapeChanged();
	}
	
	
	@Override
	public ISectorMovementProcessing getMovement() {
		return movement;
	}

	@Override
	public void setMovement(ISectorMovementProcessing movement) {
		this.movement = movement;
	}

	@Override
	public Sector move(Sector st) {
		if (movement != null) {
			return movement.move(st);
		}
		return st;
	}

	@Override
	public LatLon move(LatLon latLon) {
		if (movement != null) {
			return movement.move(latLon);
		}
		return latLon;
	}

	@Override
	public Sector moveInv(Sector st) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LatLon moveInv(LatLon latLon) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// NOTHING TO DO
		return "TectonicSegment";
	}
	
	public void setOpacity(double opacity) {

		ShapeAttributes attributes = this.getAttributes();
		attributes.setInteriorOpacity(opacity);
		this.setAttributes(attributes);
	}

	public double getOpacity() {
		return this.getAttributes().getInteriorOpacity();
	}
	
	@Override
    protected void drawInterior(DrawContext dc, SurfaceTileDrawContext sdc)
    {
		if (this.isClosed()) {
			// draw Interior if area is closed. Code from AbstractSurfaceShape
	        if (this.getActiveGeometry().isEmpty())
	            return;

	        this.applyInteriorState(dc, sdc, this.getActiveAttributes(), this.getInteriorTexture(),
	            this.getReferencePosition());

	        // Tessellate and draw the interior, making no assumptions about the nature or structure of the shape's
	        // vertices. The interior is treated as a potentially complex polygon, and this code will do its best to
	        // rasterize that polygon. The outline is treated as a simple line loop, regardless of whether the shape's
	        // vertices actually define a closed path.
	        this.tessellateInterior(dc);
		} else {
	        // Intentionally left blank; SurfacePolyline does not render an interior. (area not closed)	
		}

    }

}
