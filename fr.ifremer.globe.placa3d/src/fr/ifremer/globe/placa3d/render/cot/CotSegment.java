package fr.ifremer.globe.placa3d.render.cot;

import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoPoint;

/**
 * CotSegment : defines a segment (a list of geoPoints) with a color and a width.
 * The segment can be closed or not.
 * 
 * @author pmahoudo
 *
 */
public class CotSegment extends CotObject {
	
	/** The boolean 'closed' true if the segment is closed, false otherwise */
	private boolean closed = true;

	public CotSegment(List<GeoPoint> pointList) {
		super(pointList);
	}
	
	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

}
