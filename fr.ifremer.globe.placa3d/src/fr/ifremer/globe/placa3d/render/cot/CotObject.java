package fr.ifremer.globe.placa3d.render.cot;

import java.awt.Color;
import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoPoint;

/**
 * Abstract class to describe objects in cots.
 * This object have a list of points, a width and a color.
 * @author pmahoudo
 *
 */
public abstract class CotObject {
	
	/** The list of geopoints */
	private List<GeoPoint> pointList;
	/** The width of this point */
	private float width;
	/** The color of this point */
	private Color color;
	
	public CotObject(List<GeoPoint> pointList) {
		this.pointList = pointList;
	}
	
	public List<GeoPoint> getPointList() {
		return pointList;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
