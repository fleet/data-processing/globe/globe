package fr.ifremer.globe.placa3d.render;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.TectonicGrid;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.layers.TecShaderElevationLayer;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.util.TectonicUtils;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.viewer3d.render.ShaderSurfaceTileRenderer;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.render.SurfaceTile;

/**
 * Class for rendering tiles having a masking grid. Each cell of a masking grid defines a
 * <p>
 * WARNING : This class must be stateless.
 * 
 * @author apside
 */
public class TecShaderSurfaceTileRenderer extends ShaderSurfaceTileRenderer {

	private Map<SurfaceTile, Texture> ageTextures;

	private Map<SurfaceTile, Texture> oldTextures;

	public TecShaderSurfaceTileRenderer(TecShaderElevationLayer shaderElevationLayer) {
		super(shaderElevationLayer);
		ageTextures = new HashMap<>();
	}

	@Override
	public TecShaderElevationLayer getLayer() {
		return (TecShaderElevationLayer) super.getLayer();
	}

	@Override
	public void clearCache() {
		super.clearCache();
		oldTextures = ageTextures;
		ageTextures = new HashMap<>();
	}

	@Override
	protected void shaderParametersForMovement(GL gl1, int shaderProgram, SurfaceTile tile, Sector boundingBox,
			Sector sr, Transform transform) {
		super.shaderParametersForMovement(gl1, shaderProgram, tile, boundingBox, sr, transform);
		shaderParametersForMasking(gl1, shaderProgram, tile);
	}

	@Override
	public void dispose() {
		super.dispose();
		disposeTextures();
	}

	private void disposeTextures() {

		GLContext context = GLContext.getCurrent();
		if (context == null || context.getGL() == null)
			return;

		GL gl = context.getGL();

		for (Entry<SurfaceTile, Texture> entry : oldTextures.entrySet()) {
			entry.getValue().destroy(gl);
		}

		for (Entry<SurfaceTile, Texture> entry : ageTextures.entrySet()) {
			entry.getValue().destroy(gl);
		}

		oldTextures.clear();
		ageTextures.clear();

		oldTextures = null;
		ageTextures = null;
	}

	private void shaderParametersForMasking(GL gl1, int shaderProgram, SurfaceTile tile) {
		GL2 gl2 = gl1.getGL2();

		if (oldTextures != null) {
			Set<Entry<SurfaceTile, Texture>> textures = oldTextures.entrySet();
			for (Entry<SurfaceTile, Texture> entry : textures) {
				entry.getValue().destroy(gl2);
			}
			oldTextures.clear();
			oldTextures = null;
		}

		if (!ageTextures.containsKey(tile)) {
			computeMaskingGrid(gl2, tile);
		}

		Texture texture = ageTextures.get(tile);

		if (texture != null) {

			gl2.glUniform1i(gl2.glGetUniformLocation(shaderProgram, "hasAgeGrid"), 1);
			gl2.glUniform1i(gl2.glGetUniformLocation(shaderProgram, "ageGrid"), 1);
			gl2.glUniform1f(gl2.glGetUniformLocation(shaderProgram, "age"),
					(float) (getLayer().getTectonicModel().getAge()));

			int nbTextures = 1;
			int[] textureId = new int[nbTextures];
			gl2.glGenTextures(nbTextures, textureId, 0);

			gl2.glActiveTexture(GL2.GL_TEXTURE1);
			gl2.glBindTexture(GL2.GL_TEXTURE_2D, textureId[0]);
			texture.enable(gl2);
			texture.bind(gl2);
			texture.disable(gl2);

			// On restaure la précédente texture active
			gl2.glActiveTexture(GL2.GL_TEXTURE0);

		} else {
			gl2.glUniform1i(gl2.glGetUniformLocation(shaderProgram, "hasAgeGrid"), 0);
		}
	}

	/**
	 * Compute the masking grid for the current rendered tile.
	 * 
	 * @param gl2 OpenGL context.
	 * @param tile Rendered tile.
	 */
	protected void computeMaskingGrid(GL2 gl2, SurfaceTile tile) {

		TectonicPlateModel plate = getLayer().getPlate();

		if (plate.getMetadata().hasMaskingGrid()) {

			TectonicGrid maskingGrid = plate.getMaskingGrid();
			Sector box = maskingGrid.getBoundingBox();
			double minLon = box.getMinLongitude().degrees;
			double maxLon = box.getMaxLongitude().degrees;
			boolean isOnLineDateChange = TecGeneratorUtil.isOnLineDateChange(minLon, maxLon);

			int tileWidth = getWidth();
			int tileHeight = getHeight();

			if (tileWidth > 0 && tileHeight > 0) {

				GLProfile glprofile = GLProfile.getDefault();
				BufferedImage bi = new BufferedImage(tileWidth, tileHeight, BufferedImage.TYPE_INT_ARGB);

				WritableRaster raster = bi.getRaster();
				double[][] mask = maskingGrid.getData();
				int maskWidth = maskingGrid.getLongitudeCount();
				int maskHeight = maskingGrid.getLatitudeCount();

				if (isOnLineDateChange) {
					Area area = Area.AREA_ALL;
					if (tile.getSector().getMinLongitude().degrees >= 0
							&& tile.getSector().getMinLongitude().degrees <= 180
							&& tile.getSector().getMaxLongitude().degrees <= 180) {
						area = Area.AREA_WEST;
					} else {
						area = Area.AREA_EAST;
					}
					mask = TectonicUtils.extractArea(area, mask, box);
					maskWidth = mask[0].length;
					maskHeight = mask.length;
				}

				for (int x = 0; x < maskHeight; x++) {
					for (int y = 0; y < maskWidth; y++) {
						// For performance purpose, Should be replaced by calling TectonicGridComputer
						copyAgeToTileArea((int) mask[x][y], y, x, maskWidth, maskHeight, raster, tile);
					}
				}

				Texture tex = AWTTextureIO.newTexture(glprofile, bi, false);

				if (ageTextures.containsKey(tile)) {
					ageTextures.remove(tile).destroy(gl2);
				}
				ageTextures.put(tile, tex);
			}

		} else {
			Texture remove = ageTextures.remove(tile);
			if (remove != null) {
				remove.destroy(gl2);
			}
		}
	}

	/**
	 * Copies the specified age value to the tile's raster matching the {xMaskPixel, yMaskPixel} coordinate of the age
	 * grid.
	 * 
	 * @param age Age value to copy.
	 * @param xMaskPixel X coordinate of the age grid.
	 * @param yMaskPixel Y coordinate of the age grid.
	 * @param maskWidth Width of the age grid.
	 * @param maskHeight Height of the age grid.
	 * @param raster The tile's raster.
	 * @param tile The rendered tile.
	 */
	private void copyAgeToTileArea(int age, int xMaskPixel, int yMaskPixel, int maskWidth, int maskHeight,
			WritableRaster raster, SurfaceTile tile) {

		Sector sector = getLayer().getSector();

		// Taille du layer, en degrés
		double layerWidthInDegrees = sector.getDeltaLonDegrees();
		double layerHeightInDegrees = sector.getDeltaLatDegrees();

		// Coordonnées correspondant à l'emplacement du pixel mask(x,y) dans le layer
		double displayedAreaLon = sector.getMinLongitude().degrees
				+ (double) xMaskPixel / (double) maskWidth * layerWidthInDegrees;
		double displayedAreaLat = sector.getMaxLatitude().degrees
				- (double) yMaskPixel / (double) maskHeight * layerHeightInDegrees;

		// Taille du secteur à masquer (en ratio par rapport à la taille de la tuile)
		double displayedAreaWidth = layerWidthInDegrees / maskWidth;
		double displayedAreaHeight = layerHeightInDegrees / maskHeight;

		// Secteur géographique à afficher
		Sector displayedSector = Sector.fromDegrees(displayedAreaLat - displayedAreaHeight, displayedAreaLat,
				displayedAreaLon, displayedAreaLon + displayedAreaWidth);

		// Si les coordonnées du secteur à afficher intersectent celles de la tuile
		// on convertit ces coordonnées dans un repère orthonormé correspondant aux indices de la matrice de masquage
		// puis on modifie tous les pixels qui correspondent à la région à afficher en leur mettant comme valeur l'age
		// passé en paramètre.
		Sector intersection = displayedSector.intersection(tile.getSector());
		if (intersection != null) {
			copyAgeToRaster(age, tile.getSector(), intersection, raster);
		}
	}

	/**
	 * Convertit une zone géographique de la tuile en un rectangle dont les coordonnées sont relatives à la tuile.
	 * 
	 * @param tileSector
	 * @param displayedSector
	 * @return
	 */
	private void copyAgeToRaster(int age, Sector tileSector, Sector displayedSector, WritableRaster raster) {

		Rectangle rect = new Rectangle();

		double xRatio = displayedSector.getMinLongitude().angularDistanceTo(tileSector.getMinLongitude()).degrees
				/ tileSector.getDeltaLonDegrees();
		double yRatio = displayedSector.getMaxLatitude().angularDistanceTo(tileSector.getMaxLatitude()).degrees
				/ tileSector.getDeltaLatDegrees();

		rect.x = (int) (xRatio * getWidth());
		rect.y = (int) (yRatio * getHeight());
		rect.width = (int) (round(displayedSector.getDeltaLonDegrees() / tileSector.getDeltaLonDegrees(), 5)
				* getWidth());
		rect.height = (int) (round(displayedSector.getDeltaLatDegrees() / tileSector.getDeltaLatDegrees(), 5)
				* getHeight());

		int delta = 1;
		rect.x -= delta;
		rect.y -= delta;
		rect.width += 1 << delta;
		rect.height += 1 << delta;

		rect.x = Math.min(getWidth(), rect.x);
		rect.y = Math.min(getHeight(), rect.y);
		rect.width = Math.min(getWidth(), rect.width);
		rect.height = Math.min(getHeight(), rect.height);

		if (rect.x + rect.width > getWidth()) {
			rect.width = getWidth() - rect.x;
		}
		if (rect.x < 0) {
			rect.x = 0;
		}

		if (rect.y + rect.height > getHeight()) {
			rect.height = getHeight() - rect.y;
		}
		if (rect.y < 0) {
			rect.y = 0;
		}

		// encode age to an int rgba on two bytes
		int major = (age & 0xFF00) >> 8;
		int minor = age & 0xFF;

		int[] data = new int[rect.width * rect.height * 4];
		for (int i = 0; i < rect.width * rect.height * 4; i += 4) {
			data[i] = major; // red
			data[i + 1] = minor; // green
			data[i + 2] = 0; // blue
			data[i + 3] = 255; // alpha need to be set to 255 or it seems cutted by the set pixel
		}
		raster.setPixels(rect.x, rect.y, rect.width, rect.height, data);
	}

	public static double round(double value, int places) {
		return new BigDecimal(value).setScale(places, RoundingMode.HALF_UP).doubleValue();
	}

	/**
	 * @see fr.ifremer.viewer3d.render.ShaderSurfaceTileRenderer#useShader(com.jogamp.opengl.GL)
	 */
	@Override
	protected int useShader(GL gl1) {
		GL2 gl = gl1.getGL2();
		// RGB data => shader disabled
		if (layer == null || layer.getShaderprograms(gl) == 0) {
			return -1;
		}

		int shaderProgram = layer.getShaderprograms(gl);
		gl.glUseProgram(shaderProgram);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "scale"), (float) layer.getScaleFactor());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "offsetAuto"), (layer.isOffsetAuto() ? 1f : 0f));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), (float) layer.getOpacity());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "isRGB"), (layer.isRGB() ? 1f : 0f));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "inverseColormap"),
				(layer.isInverseColorMap() ? 1f : 0f));

		// contrast
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "min"), (float) layer.getMinContrast());
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "max"), (float) layer.getMaxContrast());

		// highlight
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "highlight"), (layer.isHighlighted() ? 1f : 0f));

		// transparency contrast
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minTransparency"), (float) layer.getMinTransparency());
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxTransparency"), (float) layer.getMaxTransparency());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "globalMin"), (float) (layer.getValGlobaleMin()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "globalMax"), (float) (layer.getValGlobaleMax()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "displaySubsidence"),
				Activator.getPreferences().getSubsidenceDisplaySettings().getValue());

		float[][] colormapBuffer = ColorMap.getColormap((int) layer.getColorMap());
		int colorMapLength = 256;
		gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "colorMapLength"), colorMapLength);

		// colormap in one array for effficiency
		// and because shader can't handle to many parameters
		int[] colormap = new int[256];
		for (int index = 0; index < colorMapLength; index++) {
			colormap[index] = (int) (colormapBuffer[ColorMap.red][index] * 255) * 256 * 256
					+ (int) (colormapBuffer[ColorMap.green][index] * 255) * 256
					+ (int) (colormapBuffer[ColorMap.blue][index] * 255);
		}

		gl.glUniform1iv(gl.glGetUniformLocation(shaderProgram, "colormap"), colorMapLength, colormap, 0);

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrage"), (layer.isUseOmbrage() ? 1f : 0f));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "width"), getWidth());
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "height"), getHeight());

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "azimuth"), (float) (layer.getAzimuth()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "zenith"), (float) (layer.getZenith()));
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageExaggeration"),
				(float) (layer.getOmbrageExaggeration()));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "resolution"), (float) (layer.getResolution()));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "toEnergy"), (float) (0.0));

		// log de la color map
		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "logColormap"), (float) (0.0));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "ombrageLogarithmique"),
				(layer.isUseOmbrageLogarithmique() ? 1f : 0f));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "gradient"), (layer.isUseGradient() ? 1f : 0f));

		gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "constantOffset"),
				layer.isOffsetAuto() ? 0f : (float) layer.getOffset());

		return shaderProgram;
	}

}
