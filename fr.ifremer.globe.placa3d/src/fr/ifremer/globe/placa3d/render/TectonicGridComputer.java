package fr.ifremer.globe.placa3d.render;

import java.io.File;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.placa3d.data.TectonicGrid;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import gov.nasa.worldwind.geom.Sector;

/**
 * Class for extract a part of the MaskingGrid according to a sector
 * 
 * @author apside
 */
public class TectonicGridComputer {

	/**
	 * extract a part of the MaskingGrid
	 * 
	 * @param plate TectonicPlateModel containing the
	 * @param result array containing the result
	 * @param sector expected BoundingBox
	 */
	public void computeMaskingGrid(TectonicGrid sourceGrid, double[][] result, Sector sector) {

		int tileHeight = result.length;
		int tileWidth = result[0].length;

		// dumpAsTiff(maskingGrid, "d:/tmp/age/1.tif");

		Sector maskSector = sourceGrid.getBoundingBox();
		double[] geoTransform = GdalUtils.computeGeoTransform(sourceGrid.getLongitudeCount(),
				sourceGrid.getLatitudeCount(), fr.ifremer.globe.nasa.worldwind.GdalUtils.getBoundingBox(maskSector));
		Dataset maskDataset = GdalUtils.generateDataset(sourceGrid.getData(), TecGeneratorUtil.getMissingValue(),
				geoTransform);
		// GdalUtils.translateToTiff(maskDataset, "d:/tmp/age/2.tif");

		Dataset tileMaskDataset = GdalUtils.reprojectRaster(maskDataset, tileWidth, tileHeight,
				TecGeneratorUtil.getMissingValue(), sector.getMinLongitude().degrees, sector.getMinLatitude().degrees,
				sector.getMaxLongitude().degrees, sector.getMaxLatitude().degrees);
		// GdalUtils.translateToTiff(maskDataset, "d:/tmp/age/3.tif");
		maskDataset.delete();

		Band band = tileMaskDataset.GetRasterBand(1);
		GdalUtils.readRaster(band, (line, column, value) -> result[line][column] = value);
		// dumpAsTiff(new TectonicGrid(maskingSector, age), "d:/tmp/age/4.tif");

		band.delete();
		tileMaskDataset.delete();
	}

	/**
	 * Debuging method to visualize a TectonicGrid
	 */
	public void dumpAsTiff(TectonicGrid grid, String filename) {
		GdalUtils.generateFile(grid.getData(), TecGeneratorUtil.getMissingValue(),
				GdalUtils.computeGeoTransform(grid.getLongitudeCount(), grid.getLatitudeCount(),
						fr.ifremer.globe.nasa.worldwind.GdalUtils.getBoundingBox(grid.getBoundingBox())),
				GdalUtils.TIF_DRIVER_NAME, new File(filename));
	}

}