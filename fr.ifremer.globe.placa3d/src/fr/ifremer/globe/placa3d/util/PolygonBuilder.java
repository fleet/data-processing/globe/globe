package fr.ifremer.globe.placa3d.util;

import java.awt.Color;
import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.placa3d.render.cot.CotObject;
import fr.ifremer.globe.placa3d.render.cot.CotPointList;
import fr.ifremer.globe.placa3d.render.cot.CotSegment;
import fr.ifremer.globe.placa3d.render.cot.CotSegmentRenderer;
import fr.ifremer.viewer3d.Activator;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.ShapeAttributes;

/***
 * Creates CotSegmentRenderer and fill the polygon with attributes
 * 
 * @author MORVAN
 * 
 */

public class PolygonBuilder {

	public static void addCotObjects(List<CotObject> cotObjectList, RenderableLayer layer) {
		if (layer != null) {
			for (CotObject cotObject : cotObjectList) {
				if (cotObject instanceof CotSegment) {
					addSegment((CotSegment) cotObject, layer);		
				} else if (cotObject instanceof CotPointList) {
					addPointList((CotPointList) cotObject, layer);
				}
			}

		}
	}

	/***
	 * adds the polygon to the renderable layer initialize the polygon
	 * characteristics (transparency, color,...)
	 * 
	 * @param name
	 * @param esp
	 * @param layer
	 */
	private static void addSegment(CotSegment segment, RenderableLayer layer) {

		CotSegmentRenderer tectonicSegment = new CotSegmentRenderer();

		ShapeAttributes attributes = new BasicShapeAttributes();
		boolean filled=Activator.getPluginParameters().getIsFillPolygon();

		attributes.setDrawInterior(filled);
		attributes.setDrawOutline(true);
		attributes.setInteriorMaterial(new Material(segment.getColor()));
		if(filled)
			attributes.setOutlineMaterial(new Material(makeBrighter(segment.getColor())));
		else
			attributes.setOutlineMaterial(new Material(segment.getColor()));

		attributes.setInteriorOpacity(0.4 * layer.getOpacity());
		attributes.setOutlineOpacity(0.8 * layer.getOpacity());
		attributes.setOutlineWidth(segment.getWidth());

		tectonicSegment.setAttributes(attributes);

		tectonicSegment.setClosed(segment.isClosed());

		layer.addRenderable(tectonicSegment);

		for (GeoPoint geoPoint : segment.getPointList()) {
			tectonicSegment.addLocation(Position.fromDegrees(geoPoint.getLat(), geoPoint.getLong()));
		}

	}

	private static Color makeBrighter(Color color) {
		float[] hsbComponents = new float[3];
		Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsbComponents);
		float hue = hsbComponents[0];
		float saturation = hsbComponents[1];
		float brightness = hsbComponents[2];

		saturation /= 3f;
		brightness *= 3f;

		if (saturation < 0f) {
			saturation = 0f;
		}

		if (brightness > 1f) {
			brightness = 1f;
		}

		int rgbInt = Color.HSBtoRGB(hue, saturation, brightness);

		return new Color(rgbInt);
	}


	private static void addPointList(CotPointList cotPointList, RenderableLayer layer) {
		// Default PointPlacemarkAttributes for PointPlacemark
		PointPlacemarkAttributes pointAttribute = new PointPlacemarkAttributes();
		pointAttribute.setUsePointAsDefaultImage(true);
		pointAttribute.setLineMaterial(new Material(cotPointList.getColor()));
		pointAttribute.setScale((double) cotPointList.getWidth());

		for (GeoPoint geoPoint : cotPointList.getPointList()) {
			//create PointPlacemark for each geopoint
			PointPlacemark pp = new PointPlacemark(Position.fromDegrees(geoPoint.getLat(), geoPoint.getLong()));		
			pp.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
			pp.setLineEnabled(true);
			pp.setValue(AVKey.LAYER, layer);
			pp.setAttributes(pointAttribute);
			pp.setVisible(true);
			layer.addRenderable(pp);
		}
	}
}
