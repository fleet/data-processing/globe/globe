package fr.ifremer.globe.placa3d.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.TreeMap;

import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.anomalies.utils.AnomaliesUtils;
import fr.ifremer.globe.placa3d.application.event.PlacaEventTopics;
import fr.ifremer.globe.utils.TextFileParser;

/***
 * magnetic anomalies are used in geophysics to define time period in the past Those periods are used in placa3D to
 * define the rotation poles periods
 *
 * @author MORVAN
 *
 */
public final class MagneticAnomalies {

	private static final Logger logger = LoggerFactory.getLogger(MagneticAnomalies.class);

	// no final keyword avoid the value to be inlined to external classes
	// whereas the value may evolve in the future. Use a static method instead
	// to retrieve the value.
	private static double MAX = 0.;

	private static MagneticAnomalies instance = null;

	/**
	 * Map magnetic anomalies / dates (Ma)
	 */
	private Map<String, Double> anomalies = new HashMap<String, Double>();

	/**
	 * Map of magnetic anomalies contained in echelle.temps and order by date
	 */
	private Map<Double, String> anomaliesTreeMap = new TreeMap<Double, String>();

	/** Observer of preference changes. */
	protected Observer preferencesObserver;

	private MagneticAnomalies() {
		read();
		this.preferencesObserver = (Observable o, Object arg) -> this
				.updateMagneticAnomaliesAccordingToPReferencesWithNorification();
		Activator.getPreferences().getMinimumAge().addObserver(this.preferencesObserver);
		Activator.getPreferences().getMaximumAge().addObserver(this.preferencesObserver);
	}

	/***
	 * at first call reads the Gradstein.temps file defining anomalies
	 *
	 * @return MagneticAnomalies instance
	 */
	public static MagneticAnomalies getInstance() {
		if (instance == null) {
			instance = new MagneticAnomalies();
		}
		return instance;
	}

	/**
	 * @return The max age value.
	 */
	public static double max() {
		return MAX;
	}

	/***
	 * read file system defining magnetic anomalies
	 */
	private void read() {

		int maxAgeInPreference = Activator.getPreferences().getMaximumAge().getValue();
		int minAgeInPreference = Activator.getPreferences().getMinimumAge().getValue();

		anomaliesTreeMap.clear();
		anomalies.clear();

		try {
			File scaleTimeFileCachePath = AnomaliesUtils.getAnomaliesFilePath();
			InputStream is = new FileInputStream(scaleTimeFileCachePath);
			try (Scanner scanner = new Scanner(is)) {
				while (scanner.hasNextLine()) {
					String tmp = scanner.nextLine();
					tmp = TextFileParser.removeComments(tmp); // remove comments
					if (!tmp.isEmpty()) {
						// last line is invalid
						String[] tmpTab = tmp.split(TectonicUtils.WHITE_CARACTHERS_REGEX);

						if (tmpTab.length != 0) {
							String anomalyName = tmpTab[0];

							double date = Double.parseDouble(tmpTab[1]);
							if (date >= minAgeInPreference && date <= maxAgeInPreference) {
								MAX = Math.max(MAX, date);

								anomaliesTreeMap.put(date, anomalyName);
								anomalies.put(anomalyName, date);
								// time defined 3 ways
								// with N the date the anomaly could be defined
								// as
								// N or anoN or pN
								if (anomalyName.indexOf("ano") == 0) {// anoN
									anomalies.put(anomalyName.substring(3), date);
									anomalies.put("p" + anomalyName.substring(3), date);

								} else if (anomalyName.indexOf("p") == 0) {// pN
									anomalies.put("ano" + anomalyName.substring(1), date);
									anomalies.put(anomalyName.substring(1), date);

								} else {// N
									anomalies.put("ano" + anomalyName, date);
									anomalies.put("p" + anomalyName, date);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.warn("Unable to read anomalies file", e);
		}
	}

	/**
	 * returns the anomaly double value
	 *
	 * @param key the anomaly string value
	 * @return the anomaly double value
	 */
	public Double getDoubleAnomaly(String key) {
		return anomalies.get(key);
	}

	/**
	 * returns the anomaly string value
	 *
	 * @param key the anomaly double value
	 * @return the anomaly string value
	 */
	public String getStringAnomaly(double value) {
		String ret = null;
		for (Entry<String, Double> entry : anomalies.entrySet()) {
			if (entry.getValue().equals(value)) {
				ret = entry.getKey();
				break;
			}
		}
		return ret;
	}

	/**
	 * returns the map of magnetic anomalies ordered by date
	 *
	 * @return
	 */
	public Map<Double, String> getAnomaliesTreeMap() {
		return anomaliesTreeMap;
	}

	/**
	 * Accept the MagneticAnomaliesUpdatedEvent event
	 */
	@Inject
	@Optional
	public void reload(@UIEventTopic(PlacaEventTopics.TOPIC_MAGNETIC_ANOMALIES_UPDATED) Event event) {
		logger.debug("Relaoding magnetic anomalies");
		read();
	}

	/**
	 * Notify user of its parameters application conscequences and update the valid magnetic anolalies dates.
	 */
	private void updateMagneticAnomaliesAccordingToPReferencesWithNorification() {
		MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warning",
				"""
						After the date limits modification, it is recommanded to reload the MTEC files in the project explorer to finish the parameters application.
						Please note that the scale.time file is not impacted by the age limits modification.
						If you want to, please edit it manually with the Globe editor tool.
						""");
		read();
	}

}
