package fr.ifremer.globe.placa3d.util;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.mtec.MultiLayerTecFile;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.placa3d.tectonic.Rotation;
import fr.ifremer.globe.placa3d.tectonic.TectonicMovement;
import fr.ifremer.globe.placa3d.writer.MultiLayerTecFileWriter;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/***
 * 3* Matrix operations
 *
 * @author MORVAN
 *
 */
public final class TectonicUtils {

	/**
	 * Several non-whitespace characters regular expression
	 */
	public final static String WHITE_CARACTHERS_REGEX = "\\s+";

	/**
	 * Comments lines begin with this character
	 */
	public static final String POUND_CHARACTER = "#";

	/***
	 * 3*3 matrix transpose
	 *
	 * @param A
	 * @return A transpose
	 */
	public static Matrix transpose3(Matrix A) {
		return new Matrix(A.m11, A.m21, A.m31, 0, A.m12, A.m22, A.m32, 0, A.m13, A.m23, A.m33, 0, 0, 0, 0, 1);
	}

	/***
	 *
	 * @param xyz
	 * @param rotationMatrix
	 * @param inverse
	 * @return rotated x,y,z position
	 */
	public static double[] performesRotation(double[] xyz, Matrix pRotationMatrix, boolean inverse) {
		// inverse of rotation matrix is transpose matrix
		// use this computation for better accuracy when compution inverse
		// movement

		Matrix rotationMatrix = null;
		if (inverse) {
			rotationMatrix = pRotationMatrix.getTranspose();
		} else {
			rotationMatrix = pRotationMatrix;
		}

		double x = rotationMatrix.m11 * xyz[0] + rotationMatrix.m12 * xyz[1] + rotationMatrix.m13 * xyz[2];
		double y = rotationMatrix.m21 * xyz[0] + rotationMatrix.m22 * xyz[1] + rotationMatrix.m23 * xyz[2];
		double z = rotationMatrix.m31 * xyz[0] + rotationMatrix.m32 * xyz[1] + rotationMatrix.m33 * xyz[2];
		return new double[] { x, y, z };
	}

	/***
	 *
	 * @param latlonSW
	 * @param latlonSE
	 * @return heading
	 */
	public static double computeHeading(LatLon latlonSW, LatLon latlonSE) {
		double lat_depart = latlonSW.getLatitude().getRadians();
		double lat_arrive = latlonSE.getLatitude().getRadians();

		double long_depart = latlonSW.getLongitude().getRadians();
		double long_arrive = latlonSE.getLongitude().getRadians();

		if (lat_depart == lat_arrive && long_depart == long_arrive) {
			return 0;
		}

		double tmp = Math.atan2(2.0 * Math.asin(Math.sin((long_depart - long_arrive) / 2.0)),
				Math.log(Math.tan(Math.PI / 4.0 + lat_arrive / 2.0))
						- Math.log(Math.tan(Math.PI / 4.0 + lat_depart / 2.0)));

		if (Math.asin(Math.sin(long_arrive - long_depart)) >= 0.0) {
			return Math.toDegrees(Math.abs(tmp));
		} else {
			return 360.0 - Math.toDegrees(Math.abs(tmp));
		}
	}

	/***
	 * from placaM
	 *
	 * @param latlon
	 * @return x,y,z position to perform the rotation
	 */
	public static double[] ptGeoGtoGeoC(LatLon latlon) {
		double x = 0, y = 0, z = 0;
		double lat = latlon.getLatitude().getDegrees();
		double lon = latlon.getLongitude().getDegrees();
		if (Math.abs(lat) + Math.abs(lon) == 0) {
			x = 0;
			y = 0;
			z = 0;
		} else if (Math.abs(lat) == 90) {
			x = 0;
			y = 0;
			z = -1;
			if (lat > 0) {
				z = 1;
			}
		} else {
			lat = latlon.getLatitude().getRadians();
			lon = latlon.getLongitude().getRadians();
			double elat = Math.atan(0.9933 * Math.tan(lat));
			x = Math.cos(elat) * Math.cos(lon);
			y = Math.cos(elat) * Math.sin(lon);
			z = Math.sin(elat);
		}
		return new double[] { x, y, z };
	}

	/***
	 * from placaM
	 *
	 * @param x
	 * @param y
	 * @param z
	 * @return the position of the rotated x,y,z position
	 */
	public static LatLon ptGeoCtoGeoG(double[] xyz) {
		double latitude = 0, longitude = 0;
		if (Math.abs(xyz[0]) + Math.abs(xyz[1]) + Math.abs(xyz[2]) == 0) {
			latitude = 0;
			longitude = 0;
		} else if (Math.abs(xyz[2]) == 1) {
			latitude = -90;
			if (xyz[2] > 0) {
				latitude = 90;
			}
		} else {
			latitude = Math.toDegrees(Math.atan(xyz[2] / Math.sqrt(1 - xyz[2] * xyz[2]) / 0.9933));
			if (xyz[0] == 0) {
				longitude = -90;
				if (xyz[1] > 0) {
					longitude = -90;
				}
			} else {
				longitude = Math.toDegrees(Math.atan2(xyz[1], xyz[0]));
			}
		}

		return LatLon.fromDegrees(latitude, longitude);
	}

	/***
	 * computes the rotation matrix associated with the rotation pole and the duration of the movement
	 *
	 * @param poleLongitude
	 * @param poleLatitude
	 * @param angle
	 * @return rotation matrix
	 */
	public static Matrix computeMatrix(double poleLatitude, double poleLongitude, double angle) {
		// Same as in placaM (see PoleRotationComparisonWithPlacaM.java)
		// Initialize longitude
		Matrix R1 = new Matrix(Math.cos(Math.toRadians(poleLongitude)), Math.sin(Math.toRadians(poleLongitude)), 0.0,
				0.0, -Math.sin(Math.toRadians(poleLongitude)), Math.cos(Math.toRadians(poleLongitude)), 0.0, 0.0, 0.0,
				0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0);

		// Initialize latitude
		Matrix R2 = new Matrix(Math.sin(Math.toRadians(poleLatitude)), 0.0, -Math.cos(Math.toRadians(poleLatitude)),
				0.0, 0.0, 1.0, 0.0, 0.0, Math.cos(Math.toRadians(poleLatitude)), 0.0,
				Math.sin(Math.toRadians(poleLatitude)), 0.0, 0.0, 0.0, 0.0, 1.0);

		// Angle computation
		Matrix R = new Matrix(Math.cos(Math.toRadians(angle)), -Math.sin(Math.toRadians(angle)), 0.0, 0.0,
				Math.sin(Math.toRadians(angle)), Math.cos(Math.toRadians(angle)), 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
				0.0, 0.0, 1.0);

		Matrix R3 = R2.multiply(R1);

		R2 = R.multiply(R3);

		// inverse of rotation Matrix is transpose Matrix
		Matrix R4 = TectonicUtils.transpose3(R3);

		R = R4.multiply(R2);

		return R;
	}

	/***
	 * Computes an intermediate pole of rotation from two total poles.
	 *
	 * @param invCurrent
	 * @param next
	 * @param begins
	 * @param ends
	 * @return
	 */
	public static Rotation convertTotToInt(Matrix invCurrent, Matrix next, double begin, double end) {

		Rotation ret = null;

		double angle = Double.NaN;
		LatLon latLon = null;

		if (invCurrent == null || next == null) {
			return null;
		}

		Matrix sum = next.multiply(invCurrent);

		double L = 0.0; // latitude
		double G = 0.0; // longitude
		double A = 0.0; // angle
		if (sum.m11 + sum.m22 + sum.m33 < 3) {
			A = Math.acos((sum.m11 + sum.m22 + sum.m33 - 1.) / 2.0);
			L = Math.asin((sum.m21 - sum.m12) / 2.0 / Math.sin(A));
			G = Math.atan2(sum.m13 - sum.m31, sum.m32 - sum.m23);

			latLon = LatLon.fromRadians(L, G);
			angle = Math.toDegrees(A);
			ret = new Rotation(latLon, angle, begin, end);
		} else {
			latLon = LatLon.fromRadians(0.0, 0.0);
			ret = new Rotation(latLon, 0.0, begin, end);
		}

		return ret;
	}

	/**
	 * Computes a total pole from a list of matrix of intermediate poles of rotation.
	 *
	 * @param intMatrixList
	 * @param begin
	 * @param end
	 * @return
	 */
	public static Rotation convertIntToTot(List<Matrix> intMatrixList, double begin, double end) {

		Rotation ret = null;

		double angle = Double.NaN;
		LatLon latLon = null;

		if (intMatrixList == null || intMatrixList.size() == 0) {
			return null;
		}

		Matrix sum = Matrix.IDENTITY;
		for (Matrix element : intMatrixList) {
			sum = sum.multiply(element);

		}

		double L = 0.0; // latitude
		double G = 0.0; // longitude
		double A = 0.0; // angle
		if (sum.m11 + sum.m22 + sum.m33 < 3) {
			A = Math.acos((sum.m11 + sum.m22 + sum.m33 - 1.) / 2.0);
			L = Math.asin((sum.m21 - sum.m12) / 2.0 / Math.sin(A));
			G = Math.atan2(sum.m13 - sum.m31, sum.m32 - sum.m23);

			latLon = LatLon.fromRadians(L, G);
			angle = Math.toDegrees(-A);
			ret = new Rotation(latLon, angle, begin, end);
		} else {
			latLon = LatLon.fromRadians(0.0, 0.0);
			ret = new Rotation(latLon, 0.0, begin, end);
		}

		return ret;
	}

	public static Rotation convertMatrixInRot(Matrix m, double begin, double end) {
		Rotation ret = null;

		double angle = Double.NaN;
		LatLon latLon = null;

		if (m == null) {
			return null;
		}

		double L = 0.0; // latitude
		double G = 0.0; // longitude
		double A = 0.0; // angle
		if (m.m11 + m.m22 + m.m33 < 3) {
			A = Math.acos((m.m11 + m.m22 + m.m33 - 1.) / 2.0);
			L = Math.asin((m.m21 - m.m12) / 2.0 / Math.sin(A));
			G = Math.atan2(m.m13 - m.m31, m.m32 - m.m23);

			latLon = LatLon.fromRadians(L, G);
			angle = Math.toDegrees(A);
			ret = new Rotation(latLon, angle, begin, end);
		} else {
			latLon = LatLon.fromRadians(0.0, 0.0);
			ret = new Rotation(latLon, 0.0, begin, end);
		}

		return ret;
	}

	/**
	 * Update rotation poles in the destination file.
	 *
	 * @param tectonicModel
	 * @param plate
	 * @param movement
	 * @param filename
	 * @throws Exception
	 */
	public static void writePoleRotationsInTecFile(TectonicModel tectonicModel, TectonicPlateModel plate,
			TectonicMovement movement, String filename) throws Exception {

		if (filename.toLowerCase().endsWith(MultiLayerTecFile.FILE_EXTENSION)) {

			// Update model in memory
			plate.setMovement(movement);

			// Update file
			File outputFile = new File(filename);
			try (MultiLayerTecFileWriter writer = new MultiLayerTecFileWriter(outputFile)) {

				// Open and write model's metadata
				writer.open();
				writer.writeAll(tectonicModel);
			}

		}
	}

	/**
	 * get Rotation.
	 *
	 * @param begin
	 * @param end
	 * @param latitude
	 * @param longitude
	 * @param angle
	 * @return
	 */
	public static Rotation getRotation(double begin, double end, double latitude, double longitude, double angle) {
		LatLon latLon = LatLon.fromDegrees(latitude, longitude);
		return new Rotation(latLon, angle, begin, end);
	}

	/** Prevent instantiation */
	private TectonicUtils() {
	}

	/**
	 * Compute translation with two latlon points. Code from placa.F (Fortran)
	 *
	 * @param r1
	 * @param r2
	 */
	public static Rotation fitTrans(LatLon r1, LatLon r2) {

		double ff = 3.1415927 / 180;

		// Transforme latlon en carthésien
		// double[] a = ptGeoGtoGeoC(r1);
		// double[] b = ptGeoGtoGeoC(r2);
		double[] a = geo2car(r1);
		double[] b = geo2car(r2);

		// Produit vectoriel a*b
		double c[] = new double[3];
		c[0] = a[1] * b[2] - a[2] * b[1];
		c[1] = a[2] * b[0] - a[0] * b[2];
		c[2] = a[0] * b[1] - a[1] * b[0];

		double sintet = Math.sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
		if (sintet == 0) {
			return null;
		}
		c[0] = c[0] / sintet;
		c[1] = c[1] / sintet;
		c[2] = c[2] / sintet;

		// LatLon pfitLatlon = ptGeoCtoGeoG(c);
		LatLon pfitLatlon = car2geo(c);
		double angle = Math.asin(sintet) / ff;

		return new Rotation(pfitLatlon, angle, 0, 0);

	}

	/**
	 * Compute rotation with one center of rotation and two latlon points. Code from placa.F (Fortran)
	 *
	 * @param r1
	 * @param r2
	 * @param r3
	 */
	public static Rotation fitRot(LatLon r1, LatLon r2, LatLon r3) {

		double ff = 3.1415927 / 180;

		// Transforme latlon en carthésien
		// double[] a = ptGeoGtoGeoC(r1);
		// double[] b = ptGeoGtoGeoC(r2);
		// double[] c = ptGeoGtoGeoC(r3);
		double[] a = geo2car(r1);
		double[] b = geo2car(r2);
		double[] c = geo2car(r3);

		// Produit scalaire b*a
		double dd = b[0] * a[0] + b[1] * a[1] + b[2] * a[2];

		double ux = b[0] - dd * a[0];
		double uy = b[1] - dd * a[1];
		double uz = b[2] - dd * a[2];

		// Normalisation
		double uu = Math.sqrt(ux * ux + uy * uy + uz * uz);
		ux = ux / uu;
		uy = uy / uu;
		uz = uz / uu;

		// Produit scalaire c*a
		dd = c[0] * a[0] + c[1] * a[1] + c[2] * a[2];

		double vx = c[0] - dd * a[0];
		double vy = c[1] - dd * a[1];
		double vz = c[2] - dd * a[2];

		// Normalisation
		double vv = Math.sqrt(vx * vx + vy * vy + vz * vz);
		vx = vx / vv;
		vy = vy / vv;
		vz = vz / vv;

		// Produit vectoriel u*v
		double w[] = new double[3];
		w[0] = uy * vz - uz * vy;
		w[1] = uz * vx - ux * vz;
		w[2] = ux * vy - uy * vx;

		double sintet = Math.sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]);
		if (sintet == 0) {
			return null;
		}

		// Produit scalaire w*a
		double ss = w[0] * a[0] + w[1] * a[1] + w[2] * a[2];

		double pfitElev = Math.asin(sintet) / ff;
		if (ss < 0) {
			pfitElev = -pfitElev;
		}
		return new Rotation(r1, pfitElev, 0, 0);
	}

	private static double[] geo2car(LatLon r1) {
		double ff = 3.1415927 / 180;
		double rlat = r1.getLatitude().degrees;
		double rlon = r1.getLongitude().degrees;

		double f1 = rlat * ff;
		double f2 = rlon * ff;
		double cc = Math.cos(f1);
		double x = cc * Math.cos(f2);
		double y = cc * Math.sin(f2);
		double z = Math.sin(f1);

		return new double[] { x, y, z };
	}

	private static LatLon car2geo(double[] coord) {
		double ff = 3.1415927 / 180;

		double x = coord[0];
		double y = coord[1];
		double z = coord[2];

		if (z > 1) {
			z = 1;
		}
		if (z < -1) {
			z = -1;
		}
		double rlat = Math.asin(z) / ff;
		double rlon;
		if (x == 0 && y == 0) {
			rlon = 0;
		} else {
			rlon = atan3(y, x) / ff;
		}

		// return LatLon.fromDegrees(rlat, rlon);
		return limits(rlat, rlon);

	}

	private static double atan3(double x, double y) {
		if (x == 0 && y == 0) {
			return 0;
		} else {
			return Math.atan2(x, y);
		}
	}

	private static LatLon limits(double rlat, double rlon) {
		if (rlat > 90) {
			rlat = rlat - 180;
		}
		if (rlat < -90) {
			rlat = rlat + 180;
		}
		if (rlon < -180) {
			rlon = rlon + 360;
		}
		if (rlon > 180) {
			rlon = rlon - 360;
		}

		return LatLon.fromDegrees(rlat, rlon);
	}

	/**
	 * Extract only the data of the Area side.
	 *
	 * @param a_area area east/west when on date line change or all
	 * @param a_raster input raster for extraction.
	 * @return extracted raster
	 */
	public static double[][] extractArea(Area a_area, double[][] a_raster, Sector geoBox) {
		int indexOf180 = 0;
		double[][] extractData = null;

		double minLon = geoBox.getMinLongitude().degrees;
		double maxLon = geoBox.getMaxLongitude().degrees;

		if (a_area != Area.AREA_ALL) {
			// Compute the index for 180°
			indexOf180 = (int) (a_raster[0].length
					* ((180.0 - minLon) / TecGeneratorUtil.diffLongitude(maxLon, minLon)));
			if (a_area == Area.AREA_WEST) {
				// Extract data at west
				extractData = new double[a_raster.length][indexOf180];
				for (int i = 0; i < a_raster.length; i++) {
					for (int j = 0; j < indexOf180; j++) {
						extractData[i][j] = a_raster[i][j];
					}
				}
			} else {
				// Extract data at east
				int l_currentExtractIndex;
				extractData = new double[a_raster.length][a_raster[0].length - indexOf180];
				for (int i = 0; i < a_raster.length; i++) {
					l_currentExtractIndex = 0;
					for (int j = indexOf180; j < a_raster[0].length; j++) {
						extractData[i][l_currentExtractIndex] = a_raster[i][j];
						l_currentExtractIndex++;
					}
				}
			}
		} else {
			extractData = a_raster;
		}
		return extractData;
	}

	/** Compute the tile suffix. */
	public static String computeCacheDirSuffix(TectonicPlateModel plate, double age, boolean morphingActivated) {
		if (morphingActivated) {
			return "_" + plate.computeAgeLayerIntervalStringForMorphingAge(age);
		} else {
			return "_" + plate.computeAgeLayerIntervalStringForAge(age);
		}
	}

	public static List<Pair<IWWLayer, ElevationModel>> convertToWWLayers(List<Pair<Layer, ElevationModel>> layers) {
		return layers.stream() //
				.filter(pair -> pair.getFirst() instanceof IWWLayer)//
				.map(pair -> new Pair<>((IWWLayer) pair.getFirst(), pair.getSecond())) //
				.collect(Collectors.toList());
	}

}
