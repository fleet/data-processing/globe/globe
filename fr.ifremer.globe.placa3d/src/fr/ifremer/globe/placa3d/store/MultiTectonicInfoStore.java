package fr.ifremer.globe.placa3d.store;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import gov.nasa.worldwind.geom.Sector;

/**
 * Contains multiple tectonic layer stores.
 *
 * @author apside
 */
public class MultiTectonicInfoStore extends AbstractInfos {

	protected File sourceFile;

	protected TectonicModel tectonicModel;

	protected GeoBox geoBox;

	protected List<TectonicPlateLayerStore> layerStores;

	/**
	 * Constructor.
	 *
	 * @param sourceFile The source file.
	 * @param tectonicModel The tectonic model.
	 */
	public MultiTectonicInfoStore(File sourceFile, TectonicModel tectonicModel) {
		layerStores = new ArrayList<>();
		this.sourceFile = sourceFile;
		this.tectonicModel = tectonicModel;

		// Compute the englobing sector of all plates
		Sector globalBounds = null;
		List<? extends TectonicPlateModel> plates = tectonicModel.getPlates();
		for (TectonicPlateModel plate : plates) {
			Sector box = plate.getMetadata().getGeoBox();
			if (globalBounds == null) {
				globalBounds = box;
			} else {
				globalBounds = Sector.union(globalBounds, box);
			}
		}
		if (globalBounds != null) {
			geoBox = convertToGeoBox(globalBounds);
		} else {
			geoBox = new GeoBox();
		}
	}

	private GeoBox convertToGeoBox(Sector sector) {
		GeoBox geobox = new GeoBox();
		geobox.setLeft(sector.getMinLongitude().degrees);
		geobox.setRight(sector.getMaxLongitude().degrees);
		geobox.setBottom(sector.getMinLatitude().degrees);
		geobox.setTop(sector.getMinLongitude().degrees);
		return geobox;
	}

	/**
	 * Adds a new layer store.
	 *
	 * @param store Layer store to add.
	 */
	public void addLayerStore(TectonicPlateLayerStore store) {
		if (!layerStores.contains(store)) {
			layerStores.add(store);

			geoBox = GeoBox.englobe(geoBox, store.getGeoBox());
		}
	}

	public GeoBox getGeoBox() {
		return geoBox;
	}

	public String getPath() {
		return sourceFile.getAbsolutePath();
	}
}
