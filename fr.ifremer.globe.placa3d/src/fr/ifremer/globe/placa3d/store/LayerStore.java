package fr.ifremer.globe.placa3d.store;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.viewer3d.layers.xml.AbstractInfos;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * Implements Globe's data store for map layers.
 */
public class LayerStore extends AbstractInfos {

	protected final File source;
	protected List<Pair<Layer, ElevationModel>> data;
	protected GeoBox geoBox;

	public LayerStore(final File sourcefile, List<Pair<Layer, ElevationModel>> data) {
		source = sourcefile;
		this.data = new ArrayList<>();

		if (data != null) {
			this.data.addAll(data);
		}

		addInfo(IConstants.Name, sourcefile.getName());
	}

	public LayerStore(WWFileLayerStore fileLayerStore) {
		source = fileLayerStore.getFileInfo().toFile();
		geoBox = fileLayerStore.getFileInfo().getGeoBox();
		data = new ArrayList<>();
		for (Layer layer : fileLayerStore.getLayers()) {
			data.add(new Pair<>(layer, data.isEmpty() ? fileLayerStore.getElevationModel() : null));
		}
	}

	public String getPath() {
		return source.getAbsolutePath();
	}

	public GeoBox getGeoBox() {
		return geoBox;
	}

	public List<Pair<Layer, ElevationModel>> getLayers() {
		return data;
	}

}
