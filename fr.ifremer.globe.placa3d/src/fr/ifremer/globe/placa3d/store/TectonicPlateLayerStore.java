package fr.ifremer.globe.placa3d.store;

import java.io.File;
import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;

/**
 * LayerStore for 1 tectonic plate.
 *
 * @author apside
 */
public class TectonicPlateLayerStore extends LayerStore {

	/** Plate's data model. */
	protected TectonicPlateModel plate;

	/**
	 * Constructor.
	 *
	 * @param sourcefile The source file.
	 * @param plate The tectonic plate containing infos.
	 * @param area The specified area.
	 */
	public TectonicPlateLayerStore(File sourcefile, TectonicPlateModel plate) {
		super(sourcefile, null);
		this.plate = plate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/**
	 * {@inheritDoc}
	 */
	public File getFile() {
		return source;
	}

	/**
	 * Adds a new layer with its optionnal elevation model.
	 *
	 * @param layer Layer to add.
	 * @param elevationModel Optionnal elevation model.
	 */
	public void addLayer(Layer layer, ElevationModel elevationModel) {
		data.add(new Pair<>(layer, elevationModel));
	}

	/**
	 * Adds a new layer without elevation model.
	 *
	 * @param layer Layer to add.
	 */
	public void addLayer(Layer layer) {
		addLayer(layer, null);
	}

	/**
	 * Adds a list of layers.
	 *
	 * @param layers List of layers to add, with their optionnal elevation models.
	 */
	public void addLayers(List<Pair<Layer, ElevationModel>> layers) {
		data.addAll(layers);
	}

	/**
	 * Removes all layers.
	 */
	public void clearLayers() {
		data.clear();
	}

}
