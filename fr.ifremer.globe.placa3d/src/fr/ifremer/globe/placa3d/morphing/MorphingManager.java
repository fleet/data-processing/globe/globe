package fr.ifremer.globe.placa3d.morphing;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Dataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.placa3d.Activator;
import fr.ifremer.globe.placa3d.data.TectonicGrid;
import fr.ifremer.globe.placa3d.data.TectonicLayer;
import fr.ifremer.globe.placa3d.data.TectonicModel;
import fr.ifremer.globe.placa3d.data.TectonicPlateModel;
import fr.ifremer.globe.placa3d.drivers.tile.TileProducerFacade;
import fr.ifremer.globe.placa3d.drivers.tile.TileProduction;
import fr.ifremer.globe.placa3d.drivers.tile.TileProductionParameters;
import fr.ifremer.globe.placa3d.drivers.tile.TileProductionParametersFactory;
import fr.ifremer.globe.placa3d.file.elevation.Area;
import fr.ifremer.globe.placa3d.file.elevation.MultiLayerTecImageElevationData;
import fr.ifremer.globe.placa3d.reader.TecImageRasterReader;
import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.ui.utils.image.ImageUtils;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.function.BiIntToFloatFunction;
import fr.ifremer.viewer3d.data.reader.raster.Raster;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.data.BufferedImageRaster;
import gov.nasa.worldwind.geom.Sector;

public class MorphingManager implements Closeable {

	/**
	 * Logger
	 */
	protected static final Logger logger = LoggerFactory.getLogger(MorphingManager.class);

	/** all tectonic plates. */
	protected TectonicModel tectonicModel;
	/** Tectonic plate composed of data grids. */
	protected TectonicPlateModel plate;

	/** Facade of tile production system */
	protected final TileProducerFacade tileProducerFacade = new TileProducerFacade();

	/** Temporary folder */
	protected File tmpDir;

	/** Tiff files produced to represent the texture morphing. */
	protected Map<Integer, File> intermediateTextures = new HashMap<>();
	/** Array files produced to represent the elevation morphing. */
	protected Map<Integer, File> intermediateElevations = new HashMap<>();
	/** Tiff files extracted form MTEC file. */
	protected Map<Integer, File> maskingGrids = new HashMap<>();

	/** Indicates that Textures has to be process if any. */
	protected boolean processTexture = true;
	/** Indicates that masking grid can also be extracted from mtec. */
	protected boolean extractMaskGrid = false;
	/**
	 * Indicates that morphing will not process computation if tiles has already been generated in Nasa cache.
	 */
	protected boolean checkNasaCache = true;

	/**
	 * Constructor.
	 */
	public MorphingManager(TectonicModel tectonicModel, TectonicPlateModel plate) {
		this.tectonicModel = tectonicModel;
		this.plate = plate;

		tmpDir = new File(TemporaryCache.getRootPath(),
				getClass().getSimpleName() + "_" + System.currentTimeMillis());
		tmpDir.mkdirs();
	}

	/**
	 * Launch the morphing computation
	 *
	 * @param tecFile Mtec model
	 * @param area East / West / ALL
	 * @param monitor the progress monitor
	 * @throws Exception something wrong appends
	 */
	public void process(MultiLayerTecImageElevationData tecFile, Area area, IProgressMonitor monitor) throws Exception {
		SubMonitor subMonitor = SubMonitor.convert(monitor, plate.hasElevation() ? 100 : 65);

		File elevationSource = tecFile.getFile();

		// If morphing is enabled, compute the intermediate images
		List<TextureMorphingManager> textureMorphingManagers = new ArrayList<>();
		List<ElevationMorphingManager> elevationMorphingManagers = new ArrayList<>();
		try {
			subMonitor.subTask("Extracting grids");
			extractGrids(tecFile, area, textureMorphingManagers, elevationMorphingManagers);
			subMonitor.worked(10);

			subMonitor.subTask("Computing intermediate data");
			computeIntermediateData(tecFile, area, textureMorphingManagers, elevationMorphingManagers,
					subMonitor.split(20));

			// Create the elevation layer
			if (plate.hasElevation()) {
				subMonitor.subTask("Creating elevation tiles");
				createElevationTiles(tecFile, area, elevationMorphingManagers, subMonitor.split(35));
			}

			subMonitor.subTask("Creating texture tiles");
			createTextureTiles(tecFile, area, elevationSource, textureMorphingManagers, subMonitor.split(35));
		} finally {
			textureMorphingManagers.forEach(IOUtils::closeQuietly);
			elevationMorphingManagers.forEach(IOUtils::closeQuietly);
		}
	}

	/**
	 * Create the texture tiles.
	 */
	protected void createTextureTiles(MultiLayerTecImageElevationData tecFile, Area area, File elevationSource,
			List<TextureMorphingManager> textureMorphingManagers, IProgressMonitor monitor) throws Exception {

		// Compute the progression.
		int totalWork = 0;
		for (TextureMorphingManager textureMorphingManager : textureMorphingManagers) {
			for (int age = textureMorphingManager.getFirstMorphedAge(); age <= textureMorphingManager
					.getLastMorphedAge(); age++) {
				totalWork++;
			}
		}
		SubMonitor subMonitor = SubMonitor.convert(monitor, totalWork);

		for (TextureMorphingManager textureMorphingManager : textureMorphingManagers) {
			tectonicModel.setAge(textureMorphingManager.getStartingAge());
			// Relaod the Nasa configuation file of the first age.
			TileProductionParameters firstAgeTileProductionParameters = new TileProductionParametersFactory()
					.makeTileImageParameters(tecFile, area, computeCacheDirSuffix());
			TileProduction firstAgeProduction = tileProducerFacade
					.generateTecImageTiles(firstAgeTileProductionParameters, elevationSource);

			for (int age = textureMorphingManager.getFirstMorphedAge(); age <= textureMorphingManager
					.getLastMorphedAge(); age++) {
				subMonitor.subTask("Generating texture morphing tile, age " + age);
				if (intermediateTextures.containsKey(age)) {
					TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
							.makeTileImageParameters(tecFile, area, "_" + age);
					tileProductionParameters.getParams().setValue(AVKey.SECTOR,
							firstAgeProduction.getParameters().getValue(AVKey.SECTOR));
					tileProductionParameters.getParams().setValue(AVKey.TILE_ORIGIN,
							firstAgeProduction.getParameters().getValue(AVKey.TILE_ORIGIN));
					tileProductionParameters.getParams().setValue(AVKey.LEVEL_ZERO_TILE_DELTA,
							firstAgeProduction.getParameters().getValue(AVKey.LEVEL_ZERO_TILE_DELTA));
					tileProductionParameters.getParams().setValue(AVKey.NUM_LEVELS,
							firstAgeProduction.getParameters().getValue(AVKey.NUM_LEVELS));
					tileProductionParameters.getParams().setValue(AVKey.TILE_HEIGHT,
							firstAgeProduction.getParameters().getValue(AVKey.TILE_HEIGHT));
					tileProductionParameters.getParams().setValue(AVKey.TILE_WIDTH,
							firstAgeProduction.getParameters().getValue(AVKey.TILE_WIDTH));
					tileProducerFacade.generateImageTiles(tileProductionParameters, intermediateTextures.get(age));
				}
				subMonitor.worked(1);
			}
		}
	}

	/**
	 * Create the elevation tiles.
	 */
	protected void createElevationTiles(MultiLayerTecImageElevationData tecFile, Area area,
			List<ElevationMorphingManager> elevationMorphingManagers, IProgressMonitor monitor) throws Exception {

		// Compute the progression.
		int totalWork = 0;
		for (ElevationMorphingManager elevationMorphingManager : elevationMorphingManagers) {
			for (int age = elevationMorphingManager.getFirstMorphedAge(); age <= elevationMorphingManager
					.getLastMorphedAge(); age++) {
				totalWork++;
			}
		}

		SubMonitor subMonitor = SubMonitor.convert(monitor, totalWork);
		for (ElevationMorphingManager elevationMorphingManager : elevationMorphingManagers) {
			for (int age = elevationMorphingManager.getFirstMorphedAge(); age <= elevationMorphingManager
					.getLastMorphedAge(); age++) {
				if (intermediateElevations.containsKey(age)) {
					subMonitor.subTask("Generating elevation morphing tile, age " + age);
					TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
							.makeTileElevationParameters(tecFile, area, "_" + age);
					tileProductionParameters.getParams().setValue(AVKey.SECTOR, tecFile.getPlate().getGeoBox());
					tileProducerFacade.generateElevationTiles(tileProductionParameters,
							intermediateElevations.get(age));
				}
				subMonitor.worked(1);
			}
		}
	}

	/**
	 * Generate intermediate images.
	 */
	public void computeIntermediateData(MultiLayerTecImageElevationData tecFile, Area area,
			List<TextureMorphingManager> textureMorphingManagers,
			List<ElevationMorphingManager> elevationMorphingManagers, IProgressMonitor monitor) throws IOException {

		// Nothing to do ?
		if ((!isProcessTexture() || textureMorphingManagers.isEmpty()) && elevationMorphingManagers.isEmpty()) {
			monitor.done();
			return;
		}

		int nbTicks = isProcessTexture() ? textureMorphingManagers.size() : 0;
		nbTicks += elevationMorphingManagers.size();
		SubMonitor subMonitor = SubMonitor.convert(monitor, nbTicks);

		String filenamePrefix = tecFile.getFile().getName() + "_";
		if (isProcessTexture() && !textureMorphingManagers.isEmpty()) {
			for (TextureMorphingManager ageMorphingManager : textureMorphingManagers) {
				if (ageMorphingManager.load(computeTextureFileName(filenamePrefix, ageMorphingManager.getStartingAge()),
						computeTextureFileName(filenamePrefix, ageMorphingManager.getStoppingAge()))) {
					int startingAge = ageMorphingManager.getFirstMorphedAge();
					int stoppingAge = ageMorphingManager.getLastMorphedAge();
					for (int age = startingAge; age <= stoppingAge; age++) {
						subMonitor.subTask("Computing texture for age " + age);
						File outFile = computeTextureFileName(filenamePrefix, age);
						ageMorphingManager.produceInterImage(age, outFile);
						intermediateTextures.put(age, outFile);
					}
				}
				ageMorphingManager.close();
				subMonitor.worked(1);
			}
		}

		if (!elevationMorphingManagers.isEmpty()) {
			for (ElevationMorphingManager elevationMorphingManager : elevationMorphingManagers) {
				int startingAge = elevationMorphingManager.getFirstMorphedAge();
				int stoppingAge = elevationMorphingManager.getLastMorphedAge();
				elevationMorphingManager.load(
						computeElevationFileName(filenamePrefix, area, elevationMorphingManager.getStartingAge()),
						computeElevationFileName(filenamePrefix, area, elevationMorphingManager.getStoppingAge()));
				for (int age = startingAge; age <= stoppingAge; age++) {
					subMonitor.subTask("Computing elevation for age " + age);
					File outFile = computeElevationFileName(filenamePrefix, area, age);
					elevationMorphingManager.produceInterElevation(age, outFile);
					intermediateElevations.put(age, outFile);
				}
				elevationMorphingManager.close();
				subMonitor.worked(1);
			}
		}
	}

	/** Extract all grids from MTEC file in Tiff format. */
	public void extractGrids(MultiLayerTecImageElevationData multiLayerTecImageElevationData, Area area,
			List<TextureMorphingManager> textureMorphingManagers,
			List<ElevationMorphingManager> elevationMorphingManagers) throws IOException {
		double ageMax = Activator.getPreferences().getMaximumAge().getValue();

		TecImageRasterReader tecImageRasterReader = new TecImageRasterReader(multiLayerTecImageElevationData, area);
		for (TectonicLayer tectonicLayer : plate.getTectonicLayers()) {
			double startingAge = tectonicLayer.getMinimumDouble();
			double stoppingAge = Math.min(tectonicLayer.getMaximumDouble(), ageMax);

			// Read Data, texture and elevation
			tectonicModel.setAge(tectonicLayer.getMinimumDouble());
			BufferedImageRaster dataRaster = tecImageRasterReader.doRead();

			// Texture may not be required
			if (isProcessTexture()) {
				extractTexture(multiLayerTecImageElevationData, area, ageMax, startingAge, stoppingAge, dataRaster,
						textureMorphingManagers);
			}

			if (plate.hasElevation()) {
				extractElevation(multiLayerTecImageElevationData, area, ageMax, startingAge, stoppingAge,
						elevationMorphingManagers);
			}

			if (plate.hasMaskingGrid() && isExtractMaskGrid()) {
				extractMask(multiLayerTecImageElevationData, startingAge);
			}

			dataRaster.dispose();
		}

	}

	/** Extract all textures from MTEC file in Tiff format. */
	protected void extractTexture(MultiLayerTecImageElevationData multiLayerTecImageElevationData, Area area,
			double ageMax, double startingAge, double stoppingAge, BufferedImageRaster dataRaster,
			List<TextureMorphingManager> textureMorphingManagers) throws IOException {
		// Write texture as tiff file
		File imageFile = computeTextureFileName(multiLayerTecImageElevationData.getFile().getName() + "_", startingAge);
		ImageUtils.writeAsTiff(dataRaster.getBufferedImage(), imageFile);

		if (stoppingAge < ageMax) {
			// Examine NWW cache. Don't need to compute morphing if all tile
			// are already generated.
			TextureMorphingManager textureMorphingManager = new TextureMorphingManager(tmpDir, startingAge,
					stoppingAge);
			boolean processLayer = true;
			if (isCheckNasaCache()) {
				processLayer = false;
				for (int age = textureMorphingManager.getFirstMorphedAge(); !processLayer
						&& age <= textureMorphingManager.getLastMorphedAge(); age++) {
					TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
							.makeTileImageParameters(multiLayerTecImageElevationData, area, "_" + age);
					processLayer = !tileProductionParameters.getConfigurationFile().exists();
				}
			}
			if (processLayer && stoppingAge - startingAge > 1) {
				textureMorphingManagers.add(textureMorphingManager);
			}
		}
	}

	/** Extract all textures from MTEC file in Tiff format. */
	protected void extractElevation(MultiLayerTecImageElevationData multiLayerTecImageElevationData, Area area,
			double ageMax, double startingAge, double stoppingAge,
			List<ElevationMorphingManager> elevationMorphingManagers) throws IOException {
		// Write texture as tiff file
		File imageFile = computeElevationFileName(multiLayerTecImageElevationData.getFile().getName() + "_", area,
				startingAge);
		intermediateElevations.put((int) startingAge, imageFile);
		double[][] elevations = multiLayerTecImageElevationData.getDepthRaster(area);
		int rowCount = elevations.length;
		int columnCount = elevations[0].length;
		// Serialize the elevations in a DoubleArray
		try (IDoubleArray elevationsBuffer = IArrayFactory.grab().openDoubleArray(imageFile, rowCount * columnCount + 2,
				true)) {
			long index = 0L;
			elevationsBuffer.setByteOrder(ByteOrder.nativeOrder());
			elevationsBuffer.putDouble(index++, rowCount);
			elevationsBuffer.putDouble(index++, columnCount);
			for (int row = 0; row < rowCount; row++) {
				elevationsBuffer.putDouble(index, elevations[row]);
				index += elevations[row].length;
			}
		}

		// Examine NWW cache. Don't need to compute morphing if all tiles are
		// already generated.
		if (stoppingAge < ageMax) {
			ElevationMorphingManager elevationMorphingManager = new ElevationMorphingManager(tmpDir, startingAge,
					stoppingAge);
			boolean processLayer = true;
			if (isCheckNasaCache()) {
				processLayer = false;
				for (int age = elevationMorphingManager.getFirstMorphedAge(); !processLayer
						&& age <= elevationMorphingManager.getLastMorphedAge(); age++) {
					TileProductionParameters tileProductionParameters = new TileProductionParametersFactory()
							.makeTileElevationParameters(multiLayerTecImageElevationData, area, "_" + age);
					processLayer = !tileProductionParameters.getConfigurationFile().exists();
				}
			}
			if (processLayer && stoppingAge - startingAge > 1) {
				elevationMorphingManagers.add(elevationMorphingManager);
			}
		}
	}

	/**
	 * Extract the masking grid.
	 */
	protected void extractMask(MultiLayerTecImageElevationData multiLayerTecImageElevationData, double age) {
		// Define the masking grid.
		File imageFile = new File(tmpDir,
				multiLayerTecImageElevationData.getFile().getName() + "_Mask_" + age + ".tiff");
		maskingGrids.put((int) age, imageFile);

		// Write masking grid as tiff file
		TectonicGrid maskingGrid = plate.getMaskingGrid();
		Sector sector = maskingGrid.getBoundingBox();
		double[][] masks = maskingGrid.getData();
		BiIntToFloatFunction rasterValueSupplier = (line, column) -> {
			double value = masks[line][column];
			return value != TecGeneratorUtil.MISSING_VALUE ? (float) value : Raster.NO_VALUE;
		};

		Dataset dataset;
		try {
			dataset = GdalUtils.generateTiffFile(maskingGrid.getLatitudeCount(), maskingGrid.getLongitudeCount(),
					Raster.NO_VALUE, sector.getMaxLatitude().degrees, sector.getMinLatitude().degrees,
					sector.getMinLongitude().degrees, sector.getMaxLongitude().degrees, rasterValueSupplier,
					imageFile.getAbsolutePath(), null);
			GdalUtils.asyncDeleteDataset(dataset);
		} catch (Exception e) {
			logger.error("An exception occurs while generating tiff files", e);
		}
	}

	/** Define the file name for a temporary texture. */
	protected File computeTextureFileName(String filenamePrefix, double age) {
		return new File(tmpDir, filenamePrefix + "Texture_" + age + ".tif");
	}

	/** Define the file name for a temporary elegation grid. */
	protected File computeElevationFileName(String filenamePrefix, Area area, double age) {
		return new File(tmpDir, filenamePrefix + "Elevation_" + area.name() + "_" + age + ".array");
	}

	/**
	 * Follow the link.
	 *
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() {
		// Delete temporary folder
		if (tmpDir != null) {
			try {
				FileUtils.deleteDirectory(tmpDir);
			} catch (Exception e) {
				logger.warn("Unable to delete folder {}", tmpDir.getAbsolutePath());
			}
		}
	}

	/** Compute the tile suffix. */
	protected String computeCacheDirSuffix() {
		return "_" + plate.computeAgeLayerIntervalStringForAge(tectonicModel.getAge());
	}

	/**
	 * @return a file (IDoubleArray) containing all elevations at one age.
	 */
	public File getIntermediateElevations(int age) {
		return intermediateElevations.get(age);
	}

	/**
	 * @return the {@link #processTexture}
	 */
	public boolean isProcessTexture() {
		return processTexture;
	}

	/**
	 * @return the {@link #checkNasaCache}
	 */
	public boolean isCheckNasaCache() {
		return checkNasaCache;
	}

	/**
	 * @param checkNasaCache the {@link #checkNasaCache} to set
	 */
	public void setCheckNasaCache(boolean checkNasaCache) {
		this.checkNasaCache = checkNasaCache;
	}

	/**
	 * @param processTexture the {@link #processTexture} to set
	 */
	public void setProcessTexture(boolean processTexture) {
		this.processTexture = processTexture;
	}

	/**
	 * @return the {@link #extractMaskGrid}
	 */
	public boolean isExtractMaskGrid() {
		return extractMaskGrid;
	}

	/**
	 * @param extractMaskGrid the {@link #extractMaskGrid} to set
	 */
	public void setExtractMaskGrid(boolean extractMaskGrid) {
		this.extractMaskGrid = extractMaskGrid;
	}

	/**
	 * @return the {@link #maskingGrids}
	 */
	public File getMaskingGrid(int age) {
		return maskingGrids.get(age);
	}

}
