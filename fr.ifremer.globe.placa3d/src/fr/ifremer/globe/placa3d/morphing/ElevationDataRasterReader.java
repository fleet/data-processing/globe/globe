/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.morphing;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;

import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.AbstractDataRasterReader;
import gov.nasa.worldwind.data.BufferWrapperRaster;
import gov.nasa.worldwind.data.DataRaster;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.BufferWrapper.DoubleBufferWrapper;
import gov.nasa.worldwind.util.WWBufferUtil;

/**
 *
 */
public class ElevationDataRasterReader extends AbstractDataRasterReader {

	/**
	 * Constructor.
	 */
	public ElevationDataRasterReader() {
		super("", new String[] { "image/array" }, new String[] { "array" });
	}

	/**
	 * Follow the link.
	 *
	 * @see gov.nasa.worldwind.data.AbstractDataRasterReader#doCanRead(java.lang.Object,
	 *      gov.nasa.worldwind.avlist.AVList)
	 */
	@Override
	protected boolean doCanRead(Object source, AVList params) {
		return true;
	}

	/**
	 * Follow the link.
	 *
	 * @see gov.nasa.worldwind.data.AbstractDataRasterReader#doRead(java.lang.Object, gov.nasa.worldwind.avlist.AVList)
	 */
	@Override
	protected DataRaster[] doRead(Object source, AVList params) throws IOException {
		IArrayFactory arrayFactory = IArrayFactory.grab();
		try (IDoubleArray elevationsBuffer = arrayFactory.openDoubleArray((File) source, false)) {
			DoubleBuffer buffer = elevationsBuffer.asDoubleBuffer(2, (int) elevationsBuffer.getElementCount() - 2);
			DoubleBufferWrapper doubleBufferWrapper = (DoubleBufferWrapper) WWBufferUtil
					.newDoubleBufferWrapper((int) elevationsBuffer.getElementCount() - 2, false);
			for (int i = 0; i < doubleBufferWrapper.length(); i++) {
				doubleBufferWrapper.putDouble(i, buffer.get(i + 2));
			}
			BufferWrapperRaster dataRaster = new BufferWrapperRaster((int) elevationsBuffer.getDouble(1),
					(int) elevationsBuffer.getDouble(0), (Sector) params.getValue(AVKey.SECTOR), doubleBufferWrapper,
					params);
			return new DataRaster[] { dataRaster };
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see gov.nasa.worldwind.data.AbstractDataRasterReader#doReadMetadata(java.lang.Object,
	 *      gov.nasa.worldwind.avlist.AVList)
	 */
	@Override
	protected void doReadMetadata(Object source, AVList params) throws IOException {
		IArrayFactory arrayFactory = IArrayFactory.grab();
		try (IDoubleArray elevationsBuffer = arrayFactory.openDoubleArray((File) source, false)) {
			elevationsBuffer.setByteOrder(ByteOrder.nativeOrder());
			int height = (int) elevationsBuffer.getDouble(0);
			int width = (int) elevationsBuffer.getDouble(1);
			setValue(AVKey.HEIGHT, height);
			setValue(AVKey.WIDTH, width);
			setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
			params.setValue(AVKey.HEIGHT, height);
			params.setValue(AVKey.WIDTH, width);
			params.setValue(AVKey.PIXEL_FORMAT, AVKey.ELEVATION);
		}
	}

}
