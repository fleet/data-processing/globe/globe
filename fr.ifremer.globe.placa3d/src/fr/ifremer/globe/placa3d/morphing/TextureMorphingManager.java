/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.morphing;

import java.io.Closeable;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconst;
import org.gdal.ogr.Feature;
import org.gdal.ogr.FieldDefn;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.gdal.GdalUtils;

/**
 * Manager of a texture morphing. <br>
 * Manager has to be initialized with two images. <br>
 * Initialization consists of (load()):
 * <ul>
 * <li>Opening the starting and stopping images with GDAL. This images are
 * managed as Dataset.</li>
 * <li>Polygonize the both images. The goal is to eliminate the transparency
 * areas and then extract the edges as Geometry.</li>
 * <li>Compute the transformation of the first edge to the last one for each
 * age. The result is a set of polygons stored in Shape files.</li>
 * </ul>
 * Morphing at a specific age consists of (produceInterImage) :
 * <ul>
 * <li>Blending the starting and stopping images</li>
 * <li>Clipping the resulting image with the polygon of edge computed in the
 * initialization step.</li>
 * </ul>
 */
public class TextureMorphingManager implements Closeable {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(TextureMorphingManager.class);

	/** Temporary folder. */
	protected File temporaryFolder;
	/** Temporary file prefix. */
	protected String filePrefix = TextureMorphingManager.class.getName() + '_' + System.currentTimeMillis() + '_';

	/** Age of the starting image */
	protected double startingAge;
	/** First image */
	protected Dataset startingImage;
	/** Age of the stopping image */
	protected double stoppingAge;
	/** Last image */
	protected Dataset stoppingImage;

	public TextureMorphingManager(File temporaryFolder, double startingAge, double stoppingAge) {
		this.temporaryFolder = temporaryFolder;
		this.startingAge = startingAge;
		this.stoppingAge = stoppingAge;
	}

	/**
	 * Initialize this manager by loading first and last image.
	 */
	public boolean load(File startingImageFile, File stoppingImageFile) {
		boolean result = false;
		logger.debug("Initialize the morphing manager from age " + startingAge + " to " + stoppingAge);

		logger.debug("Loading starting image file with GDAL : " + startingImageFile.getAbsolutePath());
		startingImage = gdal.Open(startingImageFile.getAbsolutePath());
		if (startingImage != null) {
			logger.debug("Loading stopping image file with GDAL : " + stoppingImageFile.getAbsolutePath());
			stoppingImage = gdal.Open(stoppingImageFile.getAbsolutePath());
			if (stoppingImage != null) {
				result = computeGeometry();
			} else {
				logger.warn("Gdal is not able to load the stopping image file " + stoppingImageFile.getAbsolutePath());
			}
		} else {
			logger.warn("Gdal is not able to load the starting image file " + startingImageFile.getAbsolutePath());
		}
		return result;
	}

	/**
	 * Extract geometries representing the edges of the both images.
	 */
	protected boolean computeGeometry() {
		boolean result = false;
		Geometry edge1 = polygonize(startingImage);
		int rasterXSize=Math.max(startingImage.GetRasterXSize(),stoppingImage.getRasterXSize());
		int rasterYSize=Math.max(startingImage.GetRasterYSize(),stoppingImage.getRasterYSize());

		if (edge1 != null) {
			logger.debug("Computed polygon on starting image file has " + edge1.GetGeometryRef(0).GetPointCount() + " points");
			Geometry edge2 = polygonize(stoppingImage);
			if (edge2 != null) {
				logger.debug("Computed polygon on stopping image file has " + edge2.GetGeometryRef(0).GetPointCount() + " points");
				File shapeFile = new File(temporaryFolder, filePrefix + "edge1" + ".shp");
				GdalUtils.writeShapeFile(edge1, shapeFile, null);
				shapeFile = new File(temporaryFolder, filePrefix + "edge2" + ".shp");
				GdalUtils.writeShapeFile(edge2, shapeFile, null);


				List<Geometry> segments = computeEdgeMorph(edge1, edge2);
				produceEdgesFiles(segments,rasterXSize,rasterYSize);
				edge2.delete();
				result = true;
			} else {
				logger.warn("Unable to extract the edge of stoppingImage");
			}
			edge1.delete();
		} else {
			logger.warn("Unable to extract the edge of startingImage");
		}
		return result;
	}

	/**
	 * Create a Shape file for serializing the specified geometries.
	 */
	protected void produceEdgesFiles(List<Geometry> segments, int rasterXSize, int rasterYSize) {
		int firstAge = getFirstMorphedAge();
		int lastAge = getLastMorphedAge();
		for (int progress = firstAge; progress <= lastAge; progress++) {
			Geometry polygon = new Geometry(ogr.wkbPolygon);
			// One line per segment
			Geometry line = new Geometry(ogr.wkbLinearRing);
			for (Geometry segment : segments) {
				float scalar = (float) ((progress - startingAge) / (stoppingAge - startingAge));
				int pointIndex = Math.min((int) Math.floor(segment.GetPointCount() * scalar), segment.GetPointCount() - 1);
				line.AddPoint(segment.GetX(pointIndex), segment.GetY(pointIndex));
			}
			line.CloseRings();
			polygon.AddGeometry(line);
			ProjectionSettings latlong=StandardProjection.LONGLAT;

			// Keep only the perimeter of the geometry to eliminate self-loop.

			//Geometry polygonSimplified = polygon.Buffer(0.).GetLinearGeometry(); this method is bugged in gdal and sometimes remove thie biggest geometry
			//As a workaround we create a shapefile, convert it to tiff then polygonize it back hoping it is not creating self loop

			// Create the Shape file with the inter polygon
			File outputshapeFile = computeShapeFileName(progress);





			File rawShapeFile= new File(temporaryFolder, filePrefix + progress + "_raw.shp");
			File rawRasterFile=new File(temporaryFolder, filePrefix + progress + "_raw.tif");
			GdalUtils.writeShapeFile(polygon,rawShapeFile, latlong.proj4String());

			//now convert shape file to tif (in order to rasterize it)			
			GdalUtils.rasterizeShapeFile(rawShapeFile, rawRasterFile, rasterXSize, rasterYSize);

			//reconverter to geometry
			Dataset fromRaster=gdal.Open(rawRasterFile.getAbsolutePath());
			Geometry polygonFromRaster = polygonize(fromRaster);
			polygonFromRaster=polygonFromRaster.Buffer(0); //try to remove the last remaining loops
			GdalUtils.writeShapeFile(polygonFromRaster,outputshapeFile, null);
			fromRaster.delete();
			polygonFromRaster.delete();
			polygon.delete();
		}
	}

	/**
	 * Compute the name of the Shape file.
	 */
	protected File computeShapeFileName(int age) {
		return new File(temporaryFolder, filePrefix + age + ".shp");
	}

	/**
	 * Compute a geometry to represent the evolution between first and last
	 * geometry.
	 */
	protected List<Geometry> computeEdgeMorph(Geometry edge1, Geometry edge2) {
		int yOffset = stoppingImage.getRasterYSize() - startingImage.getRasterYSize();

		edge1 = edge1.GetGeometryRef(0);
		edge2 = edge2.GetGeometryRef(0);

		GdalUtils.segmentize(edge1, 100);
		logger.debug("Polygon on starting image has " + edge1.GetPointCount() + " points after segmentation");
		GdalUtils.segmentize(edge2, 100);
		logger.debug("Polygon on stopping image has " + edge2.GetPointCount() + " points after segmentation");

		List<Geometry> lines = new ArrayList<>();
		int firstPoint2 = GdalUtils.getNearestPointIndex(edge2, edge1.GetX(), edge1.GetY());
		int nbPoints1 = edge1.GetPointCount();
		int nbPoints2 = edge2.GetPointCount();
		double[] point1 = new double[2];
		double[] point2 = new double[2];
		int toPointIndex = firstPoint2;
		for (int i = 0; i < nbPoints1; i++) {
			edge1.GetPoint_2D(i, point1);
			edge2.GetPoint_2D(toPointIndex, point2);

			// Origin of image is Bottom Left.
			// So, images will be superompose on their bottom side
			// We have to anticipate here the gap
			if (yOffset < 0) {
				point2[1] -= yOffset;
			}
			Geometry line = GdalUtils.makeSegments(point1, point2);
			GdalUtils.segmentize(line, (int) Math.ceil(stoppingAge - startingAge));
			lines.add(line);

			toPointIndex = (int) (firstPoint2 + (nbPoints2 * ((float) (i + 1) / nbPoints1)));
			toPointIndex = toPointIndex >= nbPoints2 ? toPointIndex - nbPoints2 : toPointIndex;
		}
		logger.debug("Number of segments linking the starting and stopping edges : " + lines.size());

		return lines;
	}

	/**
	 * Extract the geometry representing the edge of the dataset.
	 */
	protected Geometry polygonize(Dataset dataset) {
		Geometry result = null;

		// Extract the alpha band
		Band alphaBand = GdalUtils.getBand(dataset, gdalconst.GCI_AlphaBand, gdalconst.GCI_Undefined);
		if(alphaBand==null) //if alpha band is null, try to get the first band
		{
			alphaBand = dataset.GetRasterBand(1); 
		}
		if (alphaBand != null) {
			// Create a Shape Dataset in memory
			Layer outputLayer = GdalUtils.createDataSource().CreateLayer("Layer");
			int alphaFieldId = outputLayer.CreateField(new FieldDefn("ALPHA_VALUE", ogr.OFTInteger));
			// Ask Gdal to extract polygons
			if (gdal.Polygonize(alphaBand, null, outputLayer, alphaFieldId) == GdalUtils.OK) {
				Feature feature = outputLayer.GetNextFeature();
				while (feature != null) {
					// Select the bigger non transparent shape
					if (feature.GetFieldAsInteger(alphaFieldId) == 255) {
						Geometry polygon = feature.GetGeometryRef();
						if(polygon!=null)
						{
							if(result==null)
							{
								result=polygon;
							}else 
							{
								result=result.Union(polygon); // we need to compute the union, if we try to keep the biggest area, sometimes only the smallest on is kept
							}
						}
					}
					feature = outputLayer.GetNextFeature();
				}

				// All the picture by default
				if (result == null) {
					result = GdalUtils.makePolygon(GdalUtils.makeSquare(0, 0, dataset.GetRasterXSize(), dataset.GetRasterYSize()));
				} 
			}
		} else {
			logger.info("No alpha band found. Abort polygonize.");
			result = GdalUtils.makePolygon(GdalUtils.makeSquare(0, 0, dataset.GetRasterXSize(), dataset.GetRasterYSize()));
		}
		return result.GetLinearGeometry();
	}

	/**
	 * Process the morphing computation at the specified age. The result is
	 * stored in the outImage file.
	 * 
	 * @return the image size
	 */
	public void produceInterImage(int age, File outImage) {
		logger.debug("Apply morphing at age " + age + " in " + outImage.getAbsolutePath());
		Dataset morphedRaster = fading(age);
		GdalUtils.translateToPNG( new File(outImage.getAbsolutePath()+"_notclipped.png"), morphedRaster);
		Dataset clippedImage = GdalUtils.clipRaster(morphedRaster, computeShapeFileName(age), outImage, null, Double.NaN, false);
		morphedRaster.delete();
		GdalUtils.asyncDeleteDataset(clippedImage);
	}

	/**
	 * Blend the first and last image with a fading effect.
	 * 
	 * @return
	 */
	protected Dataset fading(int age) {
		logger.debug("Apply a fading effect between starting and stopping image at age " + age);
		float ratio = (float) ((age - startingAge) / (stoppingAge - startingAge));
		logger.debug("Percent of stopping image injected in starting image : " + (ratio * 100));
		return blendRgbaDataset(startingImage, stoppingImage, ratio);
	}

	/**
	 * Blend two rasters (fading effect).
	 * 
	 * @param ratio
	 *            percent of raster2 to inject to raster1
	 */
	public static Dataset blendRgbaDataset(Dataset raster1, Dataset raster2, float ratio) {

		int xSize = Math.max(raster1.GetRasterXSize(), raster2.GetRasterXSize());
		int ySize = Math.max(raster1.GetRasterYSize(), raster2.GetRasterYSize());

		// Create the resulting dataset
		Dataset result = GdalUtils.createDataset(xSize, ySize, gdalconst.GDT_Byte, gdalconst.GCI_RedBand, gdalconst.GCI_GreenBand, gdalconst.GCI_BlueBand, gdalconst.GCI_AlphaBand);

		// Temporary buffers
		RgbaBuffer buffer1 = new RgbaBuffer(raster1.GetRasterXSize());
		RgbaBuffer buffer2 = new RgbaBuffer(raster2.GetRasterXSize());
		RgbaBuffer resultBuffer = new RgbaBuffer(xSize);

		int line1 = raster1.GetRasterYSize() - 1;
		int line2 = raster2.GetRasterYSize() - 1;
		for (int resultLine = ySize - 1; resultLine >= 0; resultLine--, line1--, line2--) {
			if (line1 >= 0) {
				buffer1.load(raster1, line1);
			} else {
				buffer1.clear();
			}
			if (line2 >= 0) {
				buffer2.load(raster2, line2);
			} else {
				buffer2.clear();
			}
			for (int column = 0; column < xSize; column++) {
				short alpha1 = buffer1.getAlpha(column);
				short alpha2 = buffer2.getAlpha(column);
				// Pixel appeared
				if (alpha1 == GdalUtils.TRANSPARENT && alpha2 != GdalUtils.TRANSPARENT) {
					resultBuffer.setRed(column, buffer2.getRed(column));
					resultBuffer.setGreen(column, buffer2.getGreen(column));
					resultBuffer.setBlue(column, buffer2.getBlue(column));
					resultBuffer.setAlpha(column, GdalUtils.OPAQUE);
				}
				// Pixel appeared
				else if (alpha1 != GdalUtils.TRANSPARENT && alpha2 == GdalUtils.TRANSPARENT) {
					resultBuffer.setRed(column, buffer1.getRed(column));
					resultBuffer.setGreen(column, buffer1.getGreen(column));
					resultBuffer.setBlue(column, buffer1.getBlue(column));
					resultBuffer.setAlpha(column, GdalUtils.OPAQUE);
				}
				// mix colors
				else if (alpha1 != GdalUtils.TRANSPARENT && alpha2 != GdalUtils.TRANSPARENT) {
					resultBuffer.setRed(column, (short) (buffer1.getRed(column) + ((buffer2.getRed(column) - buffer1.getRed(column)) * ratio)));
					resultBuffer.setGreen(column, (short) (buffer1.getGreen(column) + ((buffer2.getGreen(column) - buffer1.getGreen(column)) * ratio)));
					resultBuffer.setBlue(column, (short) (buffer1.getBlue(column) + ((buffer2.getBlue(column) - buffer1.getBlue(column)) * ratio)));
					resultBuffer.setAlpha(column, GdalUtils.OPAQUE);
				}
				// no pixel at this position
				else {
					resultBuffer.setAlpha(column, GdalUtils.TRANSPARENT);
				}
			}
			resultBuffer.save(result, resultLine);
		}

		result.FlushCache();
		return result;
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() {
		if (startingImage != null) {
			startingImage.delete();
			startingImage = null;
		}
		if (stoppingImage != null) {
			stoppingImage.delete();
			stoppingImage = null;
		}
	}

	/**
	 * @return the {@link #startingAge}
	 */
	public double getStartingAge() {
		return startingAge;
	}

	/**
	 * @return the {@link #stoppingAge}
	 */
	public double getStoppingAge() {
		return stoppingAge;
	}

	/**
	 * @return the age of the first intermediate image.
	 */
	public int getFirstMorphedAge() {
		double result = Math.ceil(this.startingAge);
		return (int) (result == this.startingAge ? startingAge + 1 : result);
	}

	/**
	 * @return the age of the last intermediate image.
	 */
	public int getLastMorphedAge() {
		double result = Math.floor(this.stoppingAge);
		return (int) (result == this.stoppingAge ? stoppingAge - 1 : result);
	}

	/** Buffer of pixel in RGBa format. */
	protected static class RgbaBuffer {
		/** Number of column in this buffer. */
		protected final int columnCount;
		/** Red buffer. Using short instead of byte because byte are signed. */
		protected final short[] bufferRed;
		/** Green buffer. */
		protected final short[] bufferGreen;
		/** Blue buffer. */
		protected final short[] bufferBlue;
		/** Alpha buffer. */
		protected final short[] bufferAlpha;

		/**
		 * Constructor.
		 */
		public RgbaBuffer(int columnCount) {
			this.columnCount = columnCount;
			bufferRed = new short[columnCount];
			bufferGreen = new short[columnCount];
			bufferBlue = new short[columnCount];
			bufferAlpha = new short[columnCount];
		}

		/**
		 * Save all pixels of one line.
		 * 
		 * @return true if line has been saved
		 */
		public boolean save(Dataset dataset, int line) {
			boolean result = true;
			if (line < dataset.getRasterYSize())
				for (int bandIndex = 1; result && bandIndex <= dataset.getRasterCount(); bandIndex++) {
					Band band = dataset.GetRasterBand(bandIndex);
					if (band != null) {
						int colorInterpretation = band.GetColorInterpretation();
						if (colorInterpretation == gdalconst.GCI_RedBand) {
							result = band.WriteRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferRed) == GdalUtils.OK;
						} else if (colorInterpretation == gdalconst.GCI_GreenBand) {
							result = band.WriteRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferGreen) == GdalUtils.OK;
						} else if (colorInterpretation == gdalconst.GCI_BlueBand) {
							result = band.WriteRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferBlue) == GdalUtils.OK;
						} else {
							result = band.WriteRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferAlpha) == GdalUtils.OK;
						}
						band.delete();
					}
				}
			return result;
		}

		/**
		 * Load all pixels of one line.
		 * 
		 * @return true if line has been loaded
		 */
		public boolean load(Dataset dataset, int line) {
			boolean result = true;

			// By default, all pixel are considered invisible
			clear();

			if (line < dataset.getRasterXSize())
				for (int bandIndex = 1; result && bandIndex <= dataset.getRasterCount(); bandIndex++) {
					Band band = dataset.GetRasterBand(bandIndex);
					if (band != null) {
						int colorInterpretation = band.GetColorInterpretation();
						if (colorInterpretation == gdalconst.GCI_RedBand) {
							result = band.ReadRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferRed) == GdalUtils.OK;
						} else if (colorInterpretation == gdalconst.GCI_GreenBand) {
							result = band.ReadRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferGreen) == GdalUtils.OK;
						} else if (colorInterpretation == gdalconst.GCI_BlueBand) {
							result = band.ReadRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferBlue) == GdalUtils.OK;
						} else {
							// Alpha by default
							result = band.ReadRaster(0, line, columnCount, 1, gdalconst.GDT_Int16, bufferAlpha) == GdalUtils.OK;
						}
						band.delete();
					}
				}
			return result;
		}

		/** Clean this buffer : all pixels are considered as transparent. */
		public void clear() {
			Arrays.fill(bufferAlpha, GdalUtils.TRANSPARENT);
		}

		/**
		 * @return the {@link #columnCount}
		 */
		public int getNbPixel() {
			return columnCount;
		}

		/**
		 * Get red value to one pixel
		 */
		public short getRed(int column) {
			return column < columnCount ? bufferRed[column] : 0;
		}

		/**
		 * Get green value to one pixel
		 */
		public short getGreen(int column) {
			return column < columnCount ? bufferGreen[column] : 0;
		}

		/**
		 * Get blue value to one pixel
		 */
		public short getBlue(int column) {
			return column < columnCount ? bufferBlue[column] : 0;
		}

		/**
		 * Get alpha value to one pixel
		 */
		public short getAlpha(int column) {
			return column < columnCount ? bufferAlpha[column] : GdalUtils.TRANSPARENT;
		}

		/**
		 * Set red value to one pixel
		 */
		public void setRed(int column, short value) {
			bufferRed[column] = value;
		}

		/**
		 * Set red value to one pixel
		 */
		public void setGreen(int column, short value) {
			bufferGreen[column] = value;
		}

		/**
		 * Set red value to one pixel
		 */
		public void setBlue(int column, short value) {
			bufferBlue[column] = value;
		}

		/**
		 * Set red value to one pixel
		 */
		public void setAlpha(int column, short value) {
			bufferAlpha[column] = value;
		}

	}

}
