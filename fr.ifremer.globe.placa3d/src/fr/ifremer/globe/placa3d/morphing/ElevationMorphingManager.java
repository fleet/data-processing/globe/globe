/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.placa3d.morphing;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.placa3d.tecGenerator.TecGeneratorUtil;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;

/**
 * Manager of a texture morphing. <br>
 * Manager has to be initialized with two elevations. <br>
 * Initialization consists of (load()):
 * <ul>
 * <li>Opening the starting and stopping elevations with GDAL. This elevations are managed as Dataset.</li>
 * <li>Polygonize the both elevations. The goal is to eliminate the transparency areas and then extract the edges as
 * Geometry.</li>
 * <li>Compute the transformation of the first edge to the last one for each age. The result is a set of polygons stored
 * in Shape files.</li>
 * </ul>
 * Morphing at a specific age consists of (produceInterelevation) :
 * <ul>
 * <li>Blending the starting and stopping elevations</li>
 * <li>Clipping the resulting elevation with the polygon of edge computed in the initialization step.</li>
 * </ul>
 */
public class ElevationMorphingManager implements Closeable {

	/** Logger */
	public static final Logger logger = LoggerFactory.getLogger(ElevationMorphingManager.class);

	/** Temporary folder. */
	protected File temporaryFolder;
	/** Temporary file prefix. */
	protected String filePrefix = ElevationMorphingManager.class.getName() + '_' + System.currentTimeMillis() + '_';

	/** Age of the starting Elevation */
	protected double startingAge;
	/** First Elevation */
	protected IDoubleArray startingElevation;
	/** Number of row in startingElevation array */
	protected int startingRowCount = 0;
	/** Number of column in startingElevation array */
	protected int startingColumnCount = 0;

	/** Age of the stopping Elevation */
	protected double stoppingAge;
	/** Last Elevation */
	protected IDoubleArray stoppingElevation;
	/** Number of row in stoppingElevation array */
	protected int stoppingRowCount = 0;
	/** Number of column in stoppingElevation array */
	protected int stoppingColumnCount = 0;

	public ElevationMorphingManager(File temporaryFolder, double startingAge, double stoppingAge) {
		this.temporaryFolder = temporaryFolder;
		this.startingAge = startingAge;
		this.stoppingAge = stoppingAge;
	}

	/**
	 * Initialize this manager by loading first and last elevation.
	 */
	public void load(File startingElevationFile, File stoppingElevationFile) throws IOException {
		logger.debug("Initialize the elevation morphing manager from age " + startingAge + " to " + stoppingAge);
		IArrayFactory arrayFactory = IArrayFactory.grab();

		logger.debug("Loading starting elevation : " + startingElevationFile.getAbsolutePath());
		startingElevation = arrayFactory.openDoubleArray(new File(startingElevationFile.getAbsolutePath()), false);
		startingElevation.setByteOrder(ByteOrder.nativeOrder());
		startingRowCount = (int) startingElevation.getDouble(0);
		startingColumnCount = (int) startingElevation.getDouble(1);

		logger.debug("Loading stopping elevation : " + stoppingElevationFile.getAbsolutePath());
		stoppingElevation = arrayFactory.openDoubleArray(new File(stoppingElevationFile.getAbsolutePath()), false);
		stoppingElevation.setByteOrder(ByteOrder.nativeOrder());
		stoppingRowCount = (int) stoppingElevation.getDouble(0);
		stoppingColumnCount = (int) stoppingElevation.getDouble(1);
	}

	/**
	 * Process the morphing computation at the specified age. The result is stored in the outelevation file.
	 */
	public void produceInterElevation(int age, File outElevation) throws IOException {
		logger.debug("Apply morphing at age " + age + " in " + outElevation.getAbsolutePath());
		double ratio = (age - startingAge) / (stoppingAge - startingAge);
		logger.debug("Percent of stopping elevation injected in starting elevation : " + ratio * 100);

		double noValue = TecGeneratorUtil.MISSING_VALUE;

		int rowCount = Math.max(startingRowCount, stoppingRowCount);
		int columnCount = Math.max(startingColumnCount, stoppingColumnCount);
		IArrayFactory arrayFactory = IArrayFactory.grab();
		try (IDoubleArray elevationsBuffer = arrayFactory.openDoubleArray(outElevation, rowCount * columnCount + 2,
				true)) {
			elevationsBuffer.setByteOrder(ByteOrder.nativeOrder());
			long index = 0L;
			elevationsBuffer.putDouble(index++, rowCount);
			elevationsBuffer.putDouble(index++, columnCount);
			for (int row = 0; row < rowCount; row++) {
				for (int column = 0; column < columnCount; column++) {
					double value1 = row < startingRowCount && column < startingColumnCount
							? startingElevation.getDouble(row * startingColumnCount + column + 2)
							: noValue;
					double value2 = row < stoppingRowCount && column < stoppingColumnCount
							? stoppingElevation.getDouble(row * stoppingColumnCount + column + 2)
							: noValue;
					if (value1 == noValue && value2 == noValue) {
						elevationsBuffer.putDouble(index, noValue);
					} else {
						value1 = value1 == noValue ? 0 : value1;
						value2 = value2 == noValue ? 0 : value2;
						elevationsBuffer.putDouble(index, value1 + (value2 - value1) * ratio);
					}
					index++;
				}
			}
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() {
		IOUtils.closeQuietly(startingElevation);
		IOUtils.closeQuietly(stoppingElevation);
	}

	/**
	 * @return the {@link #startingAge}
	 */
	public double getStartingAge() {
		return startingAge;
	}

	/**
	 * @return the {@link #stoppingAge}
	 */
	public double getStoppingAge() {
		return stoppingAge;
	}

	/**
	 * @return the age of the first intermediate image.
	 */
	public int getFirstMorphedAge() {
		double result = Math.ceil(startingAge);
		return (int) (result == startingAge ? startingAge + 1 : result);
	}

	/**
	 * @return the age of the last intermediate image.
	 */
	public int getLastMorphedAge() {
		double result = Math.floor(stoppingAge);
		return (int) (result == stoppingAge ? stoppingAge - 1 : result);
	}

}
