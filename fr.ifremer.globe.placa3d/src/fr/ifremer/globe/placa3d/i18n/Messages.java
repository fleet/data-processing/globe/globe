package fr.ifremer.globe.placa3d.i18n;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "fr.ifremer.globe.placa3d.i18n.messages"; //$NON-NLS-1$
	public static String PoleRotationComposite_Manual;
	public static String PoleRotationComposite_Constant;
	public static String PoleRotationComposite_Automatic;
	public static String PoleRotationComposite_File;
	public static String PoleRotationComposite_currentOffsetSpinner_toolTipText;
	public static String PoleRotationComposite_TectonicPlayer;
	public static String PoleRotationComposite_intTab_text;
	public static String PoleRotationComposite_EarthAge;
	public static String PoleRotationComposite_Offset;
	public static String PoleRotationComposite_AutomaticLaw;
	public static String PoleRotationComposite_subsidenceConstant;
	public static String PoleRotationComposite_Subsidence;
	public static String PoleRotationComposite_totTab_text;

	////////////////////////////////////////////////////////////////////////////
	//
	// Constructor
	//
	////////////////////////////////////////////////////////////////////////////
	private Messages() {
		// do not instantiate
	}

	////////////////////////////////////////////////////////////////////////////
	//
	// Class initialization
	//
	////////////////////////////////////////////////////////////////////////////
	static {
		// load message values from bundle file
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
}
