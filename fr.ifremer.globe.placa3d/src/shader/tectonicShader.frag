uniform sampler2D tex;
uniform float dateindex;
uniform float opacity;
uniform float width;
uniform float height;

//compute deplacement if needed
uniform float computeMovement;

//deplacement Matrix
uniform float m11;uniform float m12;uniform float m13;uniform float m14;
uniform float m21;uniform float m22;uniform float m23;uniform float m24;
uniform float m31;uniform float m32;uniform float m33;uniform float m34;
uniform float m41;uniform float m42;uniform float m43;uniform float m44;

//texture bounding box
uniform float latitudeNorth;
uniform float latitudeSouth; 
uniform float longitudeWest;
uniform float longitudeEast;

//original position
uniform float originalLatitudeNorth; 
uniform float originalLatitudeSouth; 
uniform float originalLongitudeWest;
uniform float originalLongitudeEast;

///////////  COLORMAPS  ///////////
//age
vec4 ageColorMap(float value)
{
	vec4 result;
	if(value < 20.0) //index entre 0 et 19
	{
		result.r = 249.0;
		result.g = 0.0;
		result.b = 50.0;
	}
	else if(value < 44.0) //index entre 19 et 43
	{
		result.r = 249.0;
		result.g = 129.0;
		result.b = 0.0;
	}
	else if(value < 53.0) //index entre 43 et 52
	{
		result.r = 249.0;
		result.g = 189.0;
		result.b = 0.0;
	}
	else if(value < 73.0) //index entre 52 et 72
	{
		result.r = 249.0;
		result.g = 249.0;
		result.b = 0.0;
	}
	else if(value < 85.0) //index entre 72 et 84
	{
		result.r = 0.0;
		result.g = 99.0;
		result.b = 0.0;
	}
	else if(value < 106.0) //index entre 84 et 105
	{
		result.r = 50.0;
		result.g = 149.0;
		result.b = 0.0;
	}
	else if(value < 126.0) //index entre 105 et 125
	{
		result.r = 99.0;
		result.g = 249.0;
		result.b = 0.0;
	}
	else if(value < 135.0) //index entre 125 et 134
	{
		result.r = 99.0;
		result.g = 199.0;
		result.b = 199.0;
	}
	else if(value < 151.0) //index entre 134 et 150
	{
		result.r = 0.0;
		result.g = 149.0;
		result.b = 249.0;
	}
	else 
	{
		result.r = 0.0;
		result.g = 0.0;
		result.b = 229.0;
	}

	return result/255.0;
}
///////////  COLORMAPS  ///////////

//////////   Movement /////////

//computes the real texCoord from gl_TexCoord[0].st 
//return the coords as vec2
vec2 move()
{
	if(computeMovement == 0.0)
		return gl_TexCoord[0].st; 
	
	float latitude = (1.0-gl_TexCoord[0].t) *(latitudeNorth - latitudeSouth) + latitudeSouth;
	float longitude = gl_TexCoord[0].s *(longitudeEast - longitudeWest) + longitudeWest;
	
	//PtGeoGtoGeoC
	float x=0.0, y=0.0, z=0.0;
	if(abs(latitude) + abs(longitude) == 0.0)
	{
		x=0.0;
		y=0.0;
		z=0.0;
	}
	else if(abs(latitude) == 90.0)
	{
		x=0.0;
		y=0.0;
		z=-1.0;
		if(latitude>0.0)
			z=1.0;
	}
	else
	{
		latitude = radians(latitude);
		longitude = radians(longitude);
		float elat = atan(0.9933*tan(latitude));
		x = cos(elat) * cos(longitude);
		y = cos(elat) * sin(longitude);
		z = sin(elat);
	}
	
	//performesRotation
	float xm = m11 * x  +  m12 * y  +  m13 * z;
	float ym = m21 * x  +  m22 * y  +  m23 * z;
	float zm = m31 * x  +  m32 * y  +  m33 * z;
	
	//PtGeoCtoGeoG
	//latitude = 0.0;
    //longitude = 0.0;
	if(abs(xm) + abs(ym) + abs(zm) == 0.0)
	{
		latitude = 0.0;
		longitude = 0.0;
	}
	else if(abs(zm) == 1.0)
	{
		latitude = -90.0;
		if(zm>0.0)
			latitude = 90.0;
	}
	else
	{
		latitude = atan(zm/sqrt(1.0-zm*zm)/0.9933);
		latitude = degrees(latitude);
		if(xm == 0.0)
		{
			longitude = -90.0;
			if(ym>0.0)
				longitude = -90.0;
		}
		else
		{
			longitude = degrees(atan(ym, xm));
		}
	}
	
	float realS = (longitude - originalLongitudeWest)/(originalLongitudeEast - originalLongitudeWest);
	float realT = 1.0 - (latitude - originalLatitudeSouth)/(originalLatitudeNorth - originalLatitudeSouth);
	return vec2(realS, realT);
}
//////////  End Movement /////////

void main()
{
	vec2 st = move();
	vec4 colorOri = texture2D(tex, st);
	
	if(colorOri.r == 0.0 && colorOri.g == 0.0 && colorOri.b == 0.0)
	{
		gl_FragColor = colorOri;
		gl_FragColor.a = 0.0;
	}
	else if(gl_TexCoord[0].s < 0.0 || gl_TexCoord[0].s >= 1.0 || gl_TexCoord[0].t < 0.0 || gl_TexCoord[0].t >= 1.0 
	|| st.s < 0.0 || st.s >= 1.0 || st.t < 0.0 || st.t >= 1.0 
	|| colorOri.a == 0.0
	)
	{
		gl_FragColor = colorOri;
		gl_FragColor.a = 0.0;
	}
	else
	{
		gl_FragColor = ageColorMap(dateindex);
	
		for(float i = 0.0; i<3.0; i++)
		{
			for(float j = 0.0; j<3.0; j++)
			{
				float x = (i-(3.0/2.0));
				float y = (j-(3.0/2.0));
				
				vec2 off = vec2(x/width, y/height);
	
				vec4 tmp = texture2D(tex, st + off);
				if(tmp.a == 0.0)
				{
					gl_FragColor.r = 0.0;
					gl_FragColor.g = 0.0;
					gl_FragColor.b = 0.0;
				}
			}
		}
		gl_FragColor.a = opacity;
	}
}