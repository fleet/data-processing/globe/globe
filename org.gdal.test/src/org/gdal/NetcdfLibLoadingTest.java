package org.gdal;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.netcdf.jna.Nc4Iosp;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Unit test to check netcdf natives libraries are properly loaded
 */
public class NetcdfLibLoadingTest {

	static {
		System.setProperty("jna.debug_load", "true");
	}

	/**
	 * Loading from the netcdf plugin
	 */
	@Test
	public void loadingByNetCdfPlugin() {

		System.setProperty("jna.debug_load", "true");
		try {
			new NCVariable(0, null, "", 0, null);
			GdalUtils.makePoint(1,1);
			Assert.assertTrue(Nc4Iosp.isClibraryPresent());
		} catch (NoClassDefFoundError e) {
			fail("Loading by netcdf plugin has failed, NoClassDefFoundError: " + e.getMessage());
		}
	}

	/**
	 * Loading from the gdal plugin
	 */
	@Test
	public void loadingByGdalPlugin() {

		System.setProperty("jna.debug_load", "true");
		try {
			GdalUtils.makePoint(1,1);
			new NCVariable(0, null, "", 0, null);
			Assert.assertTrue(Nc4Iosp.isClibraryPresent());

		} catch (NoClassDefFoundError e) {
			fail("Loading by gdal plugin has failed, NoClassDefFoundError: " + e.getMessage());
		}
	}
}
