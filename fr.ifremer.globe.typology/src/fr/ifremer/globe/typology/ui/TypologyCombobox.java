package fr.ifremer.globe.typology.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.typology.model.Typology;

/**
 * {@link ComboViewer} for {@link Typology} selection.
 */
public class TypologyCombobox extends ComboViewer {

	/** Fake {@link Typology} to simulate the selection of no one **/
	private final static Typology NONE_TYPOLOGY = new Typology("NONE", "", "");

	/**
	 * Constructor
	 * 
	 * @param typologyConsumer : will be called when a typology is selected.
	 * @wbp.parser.entryPoint
	 */
	public TypologyCombobox(Composite parent, int style, Consumer<Optional<Typology>> typologyConsumer) {
		super(parent, style);
		setContentProvider(ArrayContentProvider.getInstance());

		// label provider
		setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return element instanceof Typology
						? ((Typology) element).getGroup() + " " + ((Typology) element).getClazz()
						: super.getText(element);
			}
		});

		// fill combobox
		updateTypologies();

		// listener
		addSelectionChangedListener(event -> {
			Typology selectedTypology = (Typology) ((IStructuredSelection) event.getSelection()).getFirstElement();
			typologyConsumer
					.accept(selectedTypology == NONE_TYPOLOGY ? Optional.empty() : Optional.of(selectedTypology));
		});

	}

	public void updateTypologies() {
		// get previous selection
		Typology selected = getSelectedTypology().orElse(NONE_TYPOLOGY);

		// fill combobox
		List<Typology> typologies = new ArrayList<>();
		typologies.add(NONE_TYPOLOGY);
		try { // try cath to allow open in window builder TODO: to fix
			Typologies ts = Typologies.getInstance();
			ts.getChildren().forEach(t -> {
				typologies.add(t);
				t.getChildren().forEach(typologies::add);
			});
		} catch (Exception e) {
			//
		}
		setInput(typologies);
		// by default select "NONE"
		setSelection(new StructuredSelection(
				Optional.ofNullable(Typologies.getInstance().findTypology(selected)).orElse(NONE_TYPOLOGY)));
	}

	public Optional<Typology> getSelectedTypology() {
		return Optional.ofNullable((Typology) ((IStructuredSelection) getStructuredSelection()).getFirstElement());
	}

}
