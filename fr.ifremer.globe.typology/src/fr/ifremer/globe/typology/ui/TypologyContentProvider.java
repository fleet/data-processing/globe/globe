package fr.ifremer.globe.typology.ui;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import fr.ifremer.globe.typology.model.AbstractTypology;
import fr.ifremer.globe.typology.model.Typology;

/**
 * CLass used to get the data from Typologies. The data will be used by XViewer
 * 
 * @author Ghislain CHarrier : gcharrier@itlink.fr
 * 
 */
public class TypologyContentProvider implements ITreeContentProvider {

	private static final Object[] EMPTY_ARRAY = new Object[0];

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof AbstractTypology) {
			return ((AbstractTypology) parentElement).getChildren().toArray();
		}
		return EMPTY_ARRAY;
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof Typology) {
			return ((Typology) element).getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof Typology) {
			return ((Typology) element).getChildren().size() > 0;
		}
		return false;
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}
}
