package fr.ifremer.globe.typology.ui;

import java.io.File;
import java.io.IOException;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeColumn;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.typology.events.TypologiesChangedEvent;
import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.typology.model.Typology;

public class TypologyEditor implements ISaveable {

	protected static String DIRTY = "dirty";

	private Typologies typologies;
	private TreeViewer viewer;

	@Inject
	MDirtyable dirty;

	@Inject
	private IEventBroker eventBroker;

	@PostConstruct
	private void createUI(final Composite parent) throws IOException {
		typologies = new Typologies(Typologies.getInstance());
		viewer = new TreeViewer(parent, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.MULTI);
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);

		viewer.setCellModifier(new TypologyCellModifier(viewer));
		viewer.setColumnProperties(new String[] { "name", "color", "comment" });

		createCellEditors();

		TreeViewerColumn vcol1 = new TreeViewerColumn(viewer, SWT.LEFT);
		TreeColumn col1 = vcol1.getColumn();
		col1.setAlignment(SWT.LEFT);
		col1.setText("Name");
		col1.setWidth(200);
		col1.setMoveable(true);

		TreeViewerColumn vcol2 = new TreeViewerColumn(viewer, SWT.LEFT);
		TreeColumn col2 = vcol2.getColumn();
		col2.setAlignment(SWT.LEFT);
		col2.setText("Color");
		col2.setWidth(200);
		col2.setMoveable(true);

		TreeViewerColumn vcol3 = new TreeViewerColumn(viewer, SWT.LEFT);
		TreeColumn col3 = vcol3.getColumn();
		col3.setAlignment(SWT.LEFT);
		col3.setText("Comment");
		col3.setWidth(200);
		col3.setMoveable(true);

		viewer.setContentProvider(new TypologyContentProvider());
		viewer.setLabelProvider(new TypologyLabelProvider());

		viewer.addDragSupport(DND.DROP_MOVE,
				new Transfer[] { LocalSelectionTransfer.getTransfer() },
				new TypologyDragListener(viewer));
		viewer.addDropSupport(DND.DROP_MOVE,
				new Transfer[] { LocalSelectionTransfer.getTransfer() },
				new TypologyDropListener(viewer));

		viewer.setInput(typologies);
		dirty.setDirty(false);
		viewer.setData(DIRTY, dirty);
		viewer.expandAll();
	}

	private void createCellEditors() {
		DialogCellEditor colorDialog = new TypologyColorSelectionCellEditor(viewer.getTree());

		//Forbid "\" in name: MultiMarkerLayer.exportToCSV don't support "\".
		TextCellEditor tce = new TextCellEditor(viewer.getTree());
		((Text) tce.getControl()).addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				e.doit = !e.text.contains("\\");
			}
		});
		viewer.setCellEditors(new CellEditor[] { tce, colorDialog,
				new TextCellEditor(viewer.getTree()) });
	}

	public void saveTypology() throws IOException {
		typologies.save();
		dirty.setDirty(false);
		// update singleton instance with a copy
		Typologies.updateInstance(new Typologies(typologies));
		eventBroker.post(TypologiesChangedEvent.TOPIC_TYPOLOGIES_CHANGED, new TypologiesChangedEvent());
	}

	public void addTypology() {
		StructuredSelection sel = ((StructuredSelection) viewer.getSelection());
		Typology t = (Typology) sel.getFirstElement();
		Typology newTypology = new Typology("Name", "", new GColor(255, 0, 0).toCSSString());
		if (t != null) {
			t.addChild(newTypology);
		} else {
			typologies.addChild(newTypology);
		}
		dirty.setDirty(true);
		viewer.refresh();
		viewer.expandToLevel(newTypology, AbstractTreeViewer.ALL_LEVELS);
	}

	public void deleteTypology() {
		StructuredSelection sel = ((StructuredSelection) viewer.getSelection());
		for (Object s : sel.toList()) {
			if (s instanceof Typology) {
				((Typology) s).getParent().removeChild(((Typology) s));
				dirty.setDirty(true);
			}
		}
		viewer.refresh();
	}

	public void loadDefaultTypology() throws IOException {
		typologies = Typologies.loadDefault();
		viewer.setInput(typologies);
		viewer.expandAll();
		saveTypology();
	}

	@Override
	public boolean isDirty() {
		return dirty.isDirty();
	}

	@Override
	@Persist
	public boolean doSave(IProgressMonitor monitor) {
		try {
			saveTypology();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void exportTo(File destFile) throws IOException {
		if (destFile.equals(Typologies.getCurrentTypologyFile())) {
			saveTypology();
		} else {
			typologies.save(destFile);
		}
	}

	public void loadTypologies(File file) throws IOException {
		typologies = Typologies.load(file);
		viewer.setInput(typologies);
		viewer.expandAll();
		saveTypology();
	}
}
