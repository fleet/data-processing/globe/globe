package fr.ifremer.globe.typology.ui;

import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.TransferData;

import fr.ifremer.globe.typology.model.AbstractTypology;
import fr.ifremer.globe.typology.model.Typology;

public class TypologyDropListener extends ViewerDropAdapter {

	protected TypologyDropListener(Viewer viewer) {
		super(viewer);
	}

	@Override
	public boolean performDrop(Object data) {
		Typology target = (Typology) getCurrentTarget();
		AbstractTypology parent = target.getParent();
		TreeViewer viewer = (TreeViewer) getViewer();
		MDirtyable dirty = (MDirtyable) viewer.getData(TypologyEditor.DIRTY);
		int pos;

		for (Object s : ((TreeSelection) data).toList()) {
			Typology source = (Typology) s;
			switch (getCurrentLocation()) {
			case LOCATION_BEFORE:
				pos = parent.getChildren().indexOf(target);
				parent.addChild(pos, source);
				dirty.setDirty(true);
				break;
			case LOCATION_AFTER:
				pos = parent.getChildren().indexOf(target);
				parent.addChild(pos + 1, source);
				dirty.setDirty(true);
				break;
			case LOCATION_ON:
				target.addChild(source);
				dirty.setDirty(true);
				break;
			case LOCATION_NONE:
				break;
			}
		}
		viewer.refresh();
		viewer.expandAll();
		return true;
	}

	@Override
	public boolean validateDrop(Object target, int operation,
			TransferData transferType) {
		return (getCurrentLocation() != LOCATION_NONE);
	}

}