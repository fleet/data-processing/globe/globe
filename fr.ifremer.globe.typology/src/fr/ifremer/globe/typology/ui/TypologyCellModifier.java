package fr.ifremer.globe.typology.ui;

import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.TreeItem;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.typology.model.Typology;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.StringValidator;

public class TypologyCellModifier implements ICellModifier {

	private TreeViewer viewer;
	private StringValidator validator = new StringValidator();

	public TypologyCellModifier(TreeViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public boolean canModify(Object element, String property) {
		return true;
	}

	@Override
	public Object getValue(Object element, String property) {
		switch (property) {
		case "name":
			return ((Typology) element).getName();
		case "color":
			return ((Typology) element).getColor();
		case "comment":
			return ((Typology) element).getComment();
		}
		return "";
	}

	@Override
	public void modify(Object element, String property, Object value) {
		Typology t = (Typology) ((TreeItem) element).getData();
		String val = value instanceof GColor ? ((GColor)value).toCSSString() : ((String) value).trim();
		boolean dirty = false;
		switch (property) {
		case "name":
			if (!"".equals(val)) {
				dirty = !t.getName().equals(val);
				if (validator.validate(val))
					t.setName(val);
				else {
					val = validator.filterString(val);
					t.setName(val);
					Messages.openWarningMessage("Forbidden character detected !", "The following characters are not allowed in typology name : " + validator
							+ "\n The name has been changed to match this requierement.");
				}

			}
			break;
		case "color":
			dirty = !t.getColor().equals(val);
			t.setColor(val);
			break;
		case "comment":
			dirty = !t.getComment().equals(val);
			t.setComment(val);
			break;
		}
		if (dirty) {
			((MDirtyable) viewer.getData(TypologyEditor.DIRTY)).setDirty(dirty);
		}
		viewer.refresh(t);
	}

}