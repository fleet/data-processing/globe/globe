package fr.ifremer.globe.typology.ui;

import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.ifremer.globe.ui.utils.color.ColorUtils;

/**
 * This class link TypologyColorSelectionDialog to typology editor cell.
 */
public class TypologyColorSelectionCellEditor extends DialogCellEditor {

	TypologyColorSelectionCellEditor(Composite parent) {
		super(parent);
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		return ColorUtils.openColorSelectorDialog().orElse(null);
	}

}