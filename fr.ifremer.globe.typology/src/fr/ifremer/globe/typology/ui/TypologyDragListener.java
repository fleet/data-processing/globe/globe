package fr.ifremer.globe.typology.ui;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;

public class TypologyDragListener implements DragSourceListener {

	private final TreeViewer viewer;

	public TypologyDragListener(TreeViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		LocalSelectionTransfer transfer = LocalSelectionTransfer
				.getTransfer();
		if (transfer.isSupportedType(event.dataType)) {
			transfer.setSelection(viewer.getSelection());
		}
	}

	@Override
	public void dragStart(DragSourceEvent event) {
	}

}