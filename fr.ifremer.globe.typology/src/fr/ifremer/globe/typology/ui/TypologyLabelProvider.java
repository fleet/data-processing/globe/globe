package fr.ifremer.globe.typology.ui;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.typology.model.Typology;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

public class TypologyLabelProvider implements ITableLabelProvider {

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof Typology) {
			Typology typology = (Typology) element;
			switch (columnIndex) {
			case 0:
				return typology.getName();
			case 1:
				return typology.getColor();
			case 2:
				return typology.getComment();
			}
		}
		return null;
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		if (element instanceof Typology) {
			Typology typology = (Typology) element;
			switch (columnIndex) {
			case 1:
				return ColorUtils.getImageFromColor(typology.getGColor(), 32, 16);
			}
		}
		return null;
	}
}
