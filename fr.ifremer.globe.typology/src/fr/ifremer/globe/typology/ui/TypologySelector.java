package fr.ifremer.globe.typology.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TreeColumn;

import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.typology.model.Typology;

public class TypologySelector extends Dialog {

	private static Map<Shell, TypologySelector> instance = new HashMap<>();
	private TreeViewer viewer;
	private Typologies ts;
	private Typology selection = null;
	private boolean globalInstance = true;

	public static TypologySelector getInstance(Shell parentShell) {
		if (!instance.containsKey(parentShell)) {
			instance.put(parentShell, new TypologySelector(parentShell));
		}
		return instance.get(parentShell);
	}

	private TypologySelector(Shell parentShell) {
		super(parentShell);
		// ModelRoot.getInstance().getEventBus().subscribe(this);
		setShellStyle(SWT.CLOSE | SWT.BORDER | SWT.TITLE);
		setBlockOnOpen(false);
	}

	public TypologySelector(Shell parent, Typology selection) {
		this(parent);
		this.selection = selection;
		this.globalInstance = false;
		setShellStyle(getShellStyle() | SWT.APPLICATION_MODAL);
		setBlockOnOpen(true);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (globalInstance) {
			shell.setText("Select current typology");
		} else {
			shell.setText("Select typology");
		}
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		if (!globalInstance) {
			super.createButtonsForButtonBar(parent);
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		viewer = new TreeViewer(composite, SWT.BORDER | SWT.FULL_SELECTION);
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);

		TreeViewerColumn vcol1 = new TreeViewerColumn(viewer, SWT.LEFT);
		TreeColumn col1 = vcol1.getColumn();
		col1.setAlignment(SWT.LEFT);
		col1.setText("Name");
		col1.setWidth(200);
		col1.setMoveable(true);

		TreeViewerColumn vcol2 = new TreeViewerColumn(viewer, SWT.LEFT);
		TreeColumn col2 = vcol2.getColumn();
		col2.setAlignment(SWT.LEFT);
		col2.setText("Color");
		col2.setWidth(200);
		col2.setMoveable(true);

		TreeViewerColumn vcol3 = new TreeViewerColumn(viewer, SWT.LEFT);
		TreeColumn col3 = vcol3.getColumn();
		col3.setAlignment(SWT.LEFT);
		col3.setText("Comment");
		col3.setWidth(200);
		col3.setMoveable(true);

		viewer.setContentProvider(new TypologyContentProvider());
		viewer.setLabelProvider(new TypologyLabelProvider());

		ts = Typologies.getInstance();
		viewer.setInput(ts);
		viewer.expandAll();
		if (globalInstance) {
			viewer.addSelectionChangedListener(ts);
			selectTypology(ts.getSelectedTypology());
		} else {
			selectTypology(selection);
		}

		return composite;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			if (viewer.getSelection() instanceof TreeSelection) {
				TreeSelection sel = (TreeSelection) viewer.getSelection();
				if (sel.getFirstElement() instanceof Typology) {
					selection = (Typology) sel.getFirstElement();
				}
			}
		}
		super.buttonPressed(buttonId);
	}

	public Typology getSelection() {
		return selection;
	}

	// Injection doen't work on typology selector
	// /**
	// * Handles Eclipse events (see {@link IEventBroker}) of topic {@link TypologiesChangedEvent}.
	// */
	// @Inject
	// @Optional
	// private void onTypologiesChanged(@UIEventTopic(TypologiesChangedEvent.TOPIC_TYPOLOGIES_CHANGED) Object event) {
	// if (globalInstance) {
	// Typology oldSel = ts.getSelectedTypology();
	// viewer.removeSelectionChangedListener(ts);
	//
	// ts = Typologies.getInstance();
	// viewer.setInput(ts);
	// viewer.expandAll();
	// viewer.addSelectionChangedListener(ts);
	// Typology sel = ts.getSelectedTypology();
	// if (sel == null) {
	// sel = ts.findTypology(oldSel);
	// }
	// selectTypology(sel);
	// }
	// }

	private void selectTypology(Typology sel) {
		if (sel != null) {
			viewer.setSelection(new StructuredSelection(sel));
		}
	}

	@Override
	public boolean close() {
		instance.remove(getShell());
		return super.close();
	}

	public void addSelectionChangedListener(ISelectionChangedListener changeListener) {
		this.viewer.addSelectionChangedListener(changeListener);
	}
}
