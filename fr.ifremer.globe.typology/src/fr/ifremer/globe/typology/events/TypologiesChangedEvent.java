package fr.ifremer.globe.typology.events;

import org.eclipse.e4.ui.workbench.UIEvents;

import fr.ifremer.globe.typology.model.Typologies;
import fr.ifremer.globe.ui.events.BaseEvent;

public class TypologiesChangedEvent implements BaseEvent {
	/**
	 * Base to build topic strings.
	 */
	public static final String TOPIC_BASE = "fr/ifremer/globe/typologies";

	/**
	 * Topic to inform that typologies have changed.
	 * 
	 * Associated object : "{@link Typologies}...".
	 */
	public static final String TOPIC_TYPOLOGIES_CHANGED = TOPIC_BASE + UIEvents.TOPIC_SEP + "typologies_changed";
}
