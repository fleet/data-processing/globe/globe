package fr.ifremer.globe.typology.model;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public abstract class AbstractTypology {
	@XStreamImplicit
	protected List<Typology> children = new ArrayList<>();

	public abstract AbstractTypology getParent();

	public abstract void setParent(AbstractTypology parent);

	public List<Typology> getChildren() {
		return children;
	}

	public boolean removeChild(Typology child) {
		return children.remove(child);
	}

	public boolean addChild(int index, Typology c) {
		// Forbid to add a child that is already me or a parent
		if (isAncestor(c)) {
			return false;
		}

		AbstractTypology oldParent = c.getParent();
		if (oldParent == this) {
			int oldIndex = children.indexOf(c);
			if (oldIndex >= 0 && oldIndex < index) {
				--index;
			}
		}
		if (oldParent != null) {
			oldParent.removeChild(c);
		}
		children.add(index, c);
		c.setParent(this);
		return true;
	}

	public boolean addChild(Typology c) {
		return addChild(children.size(), c);
	}

	/** Tells if C is an ancestor of this */
	private boolean isAncestor(Typology c) {
		AbstractTypology tmp = this;
		while (tmp != null) {
			if (c == tmp) {
				return true;
			}
			tmp = tmp.getParent();
		}
		return false;
	}

	protected void recomputeParenthood() {
		if (children == null) {
			children = new ArrayList<>();
		}
		for (Typology typology : children) {
			typology.setParent(this);
			typology.recomputeParenthood();
		}
	}
}
