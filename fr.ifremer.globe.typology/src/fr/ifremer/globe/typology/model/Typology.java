package fr.ifremer.globe.typology.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.core.model.marker.IMarkerTypology;
import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Abstract class representing the typologies
 * 
 * @author Ghislain Charrier
 */
@XStreamAlias("Typology")
public class Typology extends AbstractTypology implements IMarkerTypology {

	@XStreamAlias("Name")
	private String name;

	@XStreamAlias("Comment")
	private String comment;

	@XStreamAlias("Color")
	private String color;

	@XStreamOmitField
	private AbstractTypology parent = null;

	public Typology(String name, String comment, String color) {
		this.name = name;
		this.comment = comment;
		this.color = color;
	}

	public Typology(String name, String comment, String color, AbstractTypology parent) {
		this(name, comment, color);
		this.parent = parent;
	}

	/** Deep copy constructor */
	public Typology(Typology t, AbstractTypology parent) {
		this(t.getName(), t.getComment(), t.getColor(), parent);
		for (Typology typology : t.getChildren()) {
			addChild(new Typology(typology, this));
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		name = n;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String c) {
		comment = c;
	}

	public GColor getGColor() {
		return GColor.fromTypologyColor(color);
	}

	public String getColor() {
		return getGColor().toCSSString();
	}

	public void setColor(String c) {
		color = c;
	}

	@Override
	public AbstractTypology getParent() {
		return parent;
	}

	@Override
	public void setParent(AbstractTypology p) {
		parent = p;
	}

	public boolean isClass() {
		return getChildren().isEmpty();
	}

	public boolean isGroup() {
		return !getChildren().isEmpty();
	}

	@Override
	public String getGroup() {
		if (isGroup()) {
			return getName();
		} else {
			AbstractTypology parent = getParent();
			if (parent instanceof Typology) {
				return ((Typology) parent).getName();
			}
		}
		return "";
	}

	@Override
	public String getClazz() {
		if (isGroup()) {
			return "";
		}
		return getName();
	}

	public Typology findTypology(String group, String clazz) {
		// Check if "this" is the correct typology
		if (clazz.equals(name)) {
			if (group.equals("") && parent instanceof Typologies) {
				// Class in no group
				return this;
			}
			if (parent instanceof Typology && ((Typology) parent).getName().equals(group)) {
				// class and group identical
				return this;
			}
		} else if (clazz.equals("") && group.equals(name)) {
			// Only a group was given
			return this;
		}

		// ask the children to see if they are the searched typology
		for (Typology child : getChildren()) {
			Typology res = child.findTypology(group, clazz);
			if (res != null) {
				return res;
			}
		}

		return null;
	}

}
