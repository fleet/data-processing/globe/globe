package fr.ifremer.globe.typology.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Display;
import org.osgi.framework.Bundle;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.core.model.marker.IMarkerTypology;
import fr.ifremer.globe.typology.ui.TypologySelector;
import fr.ifremer.globe.ui.workspace.WorkspaceDirectory;

/**
 * Class containing all the declared typologies
 * 
 * @author Ghislain Charrier
 */
@XStreamAlias("Typologies")
public class Typologies extends AbstractTypology implements ISelectionChangedListener {

	@XStreamOmitField
	public final static String DEFAULT_TYPOLOGIES_NAME = "etc/default_typology.xml";

	@XStreamOmitField
	public final static String DEFAULT_CURRENT_TYPOLOGY = "current_typology.xml";

	@XStreamOmitField
	public static final Typology UNKNOWN_TYPOLOGY = new Typology("Unknown", "Unknown typology", "");

	private static Typologies instance = null;

	@XStreamOmitField
	private Typology selectedTypology = null;

	// Only there to have a file format version number in exported XML
	@SuppressWarnings("unused")
	private String version = "1.0";

	public Typologies() {
	}

	/** Deep copy constructor */
	public Typologies(Typologies ts) {
		for (Typology t : ts.getChildren()) {
			addChild(new Typology(t, this));
		}
	}

	public static Typologies getInstance() {
		if (instance == null) {
			createInstance();
		}
		return instance;
	}

	private static void createInstance() {
		File f = getInputFile();
		if (f != null) {
			try {
				instance = load(f);
			} catch (IOException e) {
				instance = new Typologies();
			}
		} else {
			instance = new Typologies();
		}
	}

	private static File getInputFile() {
		File f = getCurrentTypologyFile();
		if (f.canRead()) {
			return f;
		}
		f = getDefaultTypologyFile();
		if (f.canRead()) {
			return f;
		}
		return null;
	}

	public static File getCurrentTypologyFile() {
		return new File(WorkspaceDirectory.getInstance().getPath() + "/" + Typologies.DEFAULT_CURRENT_TYPOLOGY);
	}

	private static File getDefaultTypologyFile() {
		Bundle bundle = Platform.getBundle("fr.ifremer.globe.typology");
		URL f = bundle.getResource(Typologies.DEFAULT_TYPOLOGIES_NAME);

		if (f != null) {
			try {
				return new File(FileLocator.toFileURL(f).getPath());
			} catch (IOException e) {
			}
		}

		return null;
	}

	public static void updateInstance(Typologies ts) {
		if (ts != null) {
			instance = ts;
		}
	}

	public void save(File path) throws IOException {
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		Writer writer = new OutputStreamWriter(new FileOutputStream(path), "UTF-8");
		try {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
			xstream.toXML(this, writer);
		} finally {
			writer.close();
		}
	}

	public void save() throws IOException {
		save(getCurrentTypologyFile());
	}

	public static Typologies loadDefault() throws IOException {
		Typologies ts = load(getDefaultTypologyFile());
		return ts;
	}

	public static Typologies load(File inputFile) throws IOException {
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.processAnnotations(AbstractTypology.class);
		xstream.processAnnotations(Typologies.class);
		xstream.processAnnotations(Typology.class);
		xstream.setClassLoader(Typologies.class.getClassLoader());

		InputStream fis = new FileInputStream(inputFile);
		xstream.allowTypes(new Class[] { Typologies.class, Typology.class });
		Typologies result = (Typologies) xstream.fromXML(fis);
		result.recomputeParenthood();
		fis.close();

		return result;
	}

	public static Typologies test() throws IOException {
		Typologies ts = new Typologies();
		Typology poissons = new Typology("poissons", "des poissons", "bleu");
		Typology poissons2 = new Typology("poissons2", "des poissons", "gris");
		Typology c = new Typology("Calamar", "", "");
		poissons.addChild(c);
		Typology d = new Typology("Dauphin", "", "");
		poissons.addChild(d);
		Typology zero = new Typology("0", "", "");
		ts.addChild(zero);
		zero.setColor("rouge");
		Typology one = new Typology("1", "", "");
		ts.addChild(one);
		ts.addChild(poissons);
		poissons.addChild(poissons2);
		ts.save();

		Typologies res = load(getCurrentTypologyFile());
		System.out.println(res);

		return ts;
	}

	public static TypologySelector launchTypologySelector() {
		TypologySelector dialog = TypologySelector.getInstance(Display.getDefault().getActiveShell());
		Display.getDefault().syncExec(dialog::open);
		return dialog;
	}

	@Override
	public AbstractTypology getParent() {
		return null;
	}

	@Override
	public void setParent(AbstractTypology parent) {
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		if (event.getSelection() instanceof TreeSelection) {
			TreeSelection sel = (TreeSelection) event.getSelection();
			if (sel.getFirstElement() instanceof Typology) {
				selectedTypology = (Typology) sel.getFirstElement();
			}
		}
	}

	public Typology getSelectedTypology() {
		return selectedTypology;
	}

	public void setSelectedTypology(Typology selectedTypology) {
		this.selectedTypology = selectedTypology;
	}

	public Typology findTypology(IMarkerTypology markerTypology) {
		return findTypology(markerTypology.getGroup(), markerTypology.getClazz());
	}

	public Typology findTypology(Typology t) {
		String clazz = t.getName();
		String group = "";
		AbstractTypology parent = t.getParent();
		if (parent instanceof Typology) {
			group = ((Typology) parent).getName();
		}
		return findTypology(group, clazz);
	}

	public Typology findTypology(String group, String clazz) {
		for (Typology child : getChildren()) {
			Typology res = child.findTypology(group, clazz);
			if (res != null) {
				return res;
			}
		}

		return null;
	}

}
