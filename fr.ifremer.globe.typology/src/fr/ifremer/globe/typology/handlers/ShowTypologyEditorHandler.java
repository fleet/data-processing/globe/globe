package fr.ifremer.globe.typology.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.PartManager;

public class ShowTypologyEditorHandler {
	public final static String COMMAND_ID = "fr.ifremer.globe.typology.command.show";

	static String partId = "fr.ifremer.globe.typology.typologyeditor";

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application,
			EModelService modelService) {
		MPart part = PartManager.createPart(partService, partId);
		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, part);
	}

}
