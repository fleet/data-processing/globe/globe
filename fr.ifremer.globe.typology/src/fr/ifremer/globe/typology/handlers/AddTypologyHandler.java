package fr.ifremer.globe.typology.handlers;

import java.io.IOException;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.typology.ui.TypologyEditor;

public class AddTypologyHandler {
	@Execute
	protected void execute(EPartService partService) throws IOException {
		MPart part = partService.getActivePart();
		Object obj = part.getObject();
		if (obj instanceof TypologyEditor) {
			TypologyEditor view = (TypologyEditor) obj;
			view.addTypology();
		}
	}
}
