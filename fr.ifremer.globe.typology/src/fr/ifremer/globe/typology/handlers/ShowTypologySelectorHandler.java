package fr.ifremer.globe.typology.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.typology.ui.TypologySelector;

public class ShowTypologySelectorHandler {	
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		TypologySelector dialog = TypologySelector.getInstance(shell);
		dialog.open();
	}
}
