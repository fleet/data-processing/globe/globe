package fr.ifremer.globe.typology.handlers;

import java.io.File;
import java.io.IOException;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.typology.ui.TypologyEditor;
import fr.ifremer.globe.ui.utils.Messages;

public class ImportTypologiesHandler {
	@Execute
	protected void execute(EPartService partService,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell)
			throws IOException {
		MPart part = partService.getActivePart();
		Object obj = part.getObject();
		if (obj instanceof TypologyEditor) {
			TypologyEditor view = (TypologyEditor) obj;
			FileDialog dialog = new FileDialog(shell);
			dialog.setFilterNames(new String[] { "XML File" });
			dialog.setFilterExtensions(new String[] { "*.xml" });
			String filename = dialog.open();
			if (filename != null) {
				if (Messages.openSyncQuestionMessage("Warning!", "Warning : The current typology will be overwritten. Continue?"))
				view.loadTypologies(new File(filename));
			}
		}
	}
}
