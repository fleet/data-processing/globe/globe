#version 140 core

//uniform sampler2D tex;

//void main()
//{

	//gl_FragColor = vec4(1.0, 0.0, 0.0, 0.0); 
//}

 
uniform sampler2D tex;
 
void main()
{
    vec4 color = texture2D(tex,gl_TexCoord[0].st);
    gl_FragColor = color;
    gl_FragColor.r=1.0;
}