import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.sessions.TestSession;
import fr.ifremer.globe.ui.undo.UndoStackTest;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.viewer3d.FileProjectionCacheTest;

/**
 * Test suite for maven , declared test will be launched while compiling with command mvn integration-test
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ TestSession.class, FileProjectionCacheTest.class, UndoStackTest.class })
public class MvnTestSuite {
	@AfterClass
	public static void cleanUp() {
		TemporaryCache.cleanAllFiles();
	}
}
