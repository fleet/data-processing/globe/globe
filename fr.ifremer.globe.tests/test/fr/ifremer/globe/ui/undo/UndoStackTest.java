package fr.ifremer.globe.ui.undo;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.utils.undo.UndoAction;

/** Test class for undostack. */
public class UndoStackTest {

	private static int counter = 0;

	public static class TestAction extends UndoAction {

		@Override
		protected void executeUndo() {
			counter--;
			System.out.println("undone : " + counter);
		}

		@Override
		protected void executeRedo() {
			counter++;
			System.out.println("redone : " + counter);
		}

		@Override
		public String msgInfo() {
			return "test";
		}
	}

	@Test
	public void testGetInstance() {
		UndoStack stack1 = UndoStack.createInstance("testStack", UndoStack.INFINITE_ACTION);
		Assert.assertNotNull(stack1);
		UndoStack stack2 = UndoStack.createInstance("another.stack", UndoStack.INFINITE_ACTION);
		Assert.assertNotNull(stack2);
		Assert.assertTrue(stack1 != stack2);
		UndoStack stack3 = UndoStack.createInstance("testStack", UndoStack.INFINITE_ACTION);
		Assert.assertNotNull(stack3);
		Assert.assertTrue(stack1 == stack3);
	}

	@Test
	public void testUndo() {
		UndoStack stack = UndoStack.createInstance("testUndo", UndoStack.INFINITE_ACTION);
		Assert.assertTrue(stack.isEmpty());
		stack.push(new TestAction());
		Assert.assertFalse(stack.isEmpty());
		stack.undo();
		Assert.assertTrue(stack.isEmpty());
	}

	@Test
	public void testRedo() {
		UndoStack stack = UndoStack.createInstance("testRedo", UndoStack.INFINITE_ACTION);
		counter = 0;
		TestAction action1 = new TestAction();
		TestAction action2 = new TestAction();
		stack.push(action1);
		Assert.assertFalse(stack.isEmpty());
		stack.undo();
		Assert.assertTrue(stack.isEmpty());
		stack.redo();
		Assert.assertFalse(stack.isEmpty());
		stack.push(action2);
		Assert.assertEquals(2, stack.getCurrentIndex());
		stack.undo();
		Assert.assertEquals(1, stack.getCurrentIndex());
		stack.undo();
		Assert.assertEquals(0, stack.getCurrentIndex());
		stack.redo();
		Assert.assertEquals(1, stack.getCurrentIndex());
		stack.undo();
		Assert.assertEquals(0, stack.getCurrentIndex());
		stack.redo();
		Assert.assertEquals(1, stack.getCurrentIndex());
		stack.redo();
		Assert.assertEquals(2, stack.getCurrentIndex());
	}

	@Test
	public void testCancelAction() {
		UndoStack stack = UndoStack.createInstance("testCancelAction", UndoStack.INFINITE_ACTION);
		counter = 0;
		TestAction action1 = new TestAction();
		TestAction action2 = new TestAction();
		TestAction action3 = new TestAction();
		stack.push(action1);
		stack.push(action2);
		stack.push(action3);
		Assert.assertEquals(3, stack.getCurrentIndex());
		stack.undo();
		stack.undo();
		Assert.assertEquals(1, stack.getCurrentIndex());
		Assert.assertEquals(3, stack.size());
		TestAction action4 = new TestAction();
		stack.push(action4);
		Assert.assertEquals(2, stack.getCurrentIndex());
		// only two actions in stack because undone actions are replaced by the
		// new "action4" action
		Assert.assertEquals(2, stack.size());
	}
}
