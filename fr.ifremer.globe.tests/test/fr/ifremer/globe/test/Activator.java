/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.test;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Plugin activator.
 */
public class Activator implements BundleActivator {

	/** Bundle's execution context within the Eclipse Framework. */
	private static BundleContext bundleContext;

	/** Follow the link.
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		bundleContext = context;
	}

	/** Follow the link.
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
	}

	/**
	 * @return the {@link #bundleContext}
	 */
	public static BundleContext getBundleContext() {
		return bundleContext;
	}


}
