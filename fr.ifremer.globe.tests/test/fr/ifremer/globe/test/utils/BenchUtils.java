/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.test.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;

import org.junit.Assert;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * 
 * @author plemarchand
 * @author cguychard (Apside)
 *
 */
public class BenchUtils {

	/** File where initial run times are stored */
	private final static String REFERENCE_TIME_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/referenceTimesApside.txt";

	/** File where updated run times are stored */
	private final static String UPDATED_REFERENCE_TIME_FILE_PATH = GlobeTestUtil.getTestDataPath()
			+ "/file/UpdatedReferenceTimesApside.txt";

	/** Authorized delta (%) relatives to test hardware environment. */
	private final static long delta = 10;

	/**
	 * runs a test method and updates its execution time or fail if elapsed time was superior to
	 * (1+delta/100)referenceExecTime
	 */
	public static void timedPerformanceTest(Method testMethod, Object obj) {
		try {

			String methodName = testMethod.getDeclaringClass().getSimpleName() + "." + testMethod.getName();

			long referenceTime = BenchUtils.getReferenceTime(methodName);

			long total = System.currentTimeMillis();

			testMethod.invoke(obj);

			long elapsed = System.currentTimeMillis() - total;

			BenchUtils.updateReferenceTime(methodName, elapsed, referenceTime);

			if (referenceTime > 0) {
				Assert.assertTrue(
						methodName + ": test creation is slower than its time reference : " + elapsed
								+ "ms. Expected : " + referenceTime + "s.",
						elapsed < referenceTime + (delta * referenceTime) / 100);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Test failed on Exception: [" + e.toString() + "] " + e.getMessage());
		}

	}

	/**
	 * @param name a simple name to get reference time on
	 * @return a long corresponding to the time reference for the given name (Class or Method)
	 * @throws IOException
	 */
	public static long getReferenceTime(String name) throws IOException {

		BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(REFERENCE_TIME_FILE_PATH)));

		String line = "";
		long referenceTime = 0;
		try {
			while ((line = bufferedReader.readLine()) != null) {
				if (line.startsWith(name + "-")) {
					break;
				}
			}
			if (line != null) {
				String[] strs = line.split("-", 0);
				referenceTime = Long.parseLong(strs[1]);
			}

		} finally {
			bufferedReader.close();
		}
		return referenceTime;
	}

	/**
	 * 
	 * updates Measured execution time
	 * 
	 * @param className simple name of the class to get reference times on
	 * @param index index of the test
	 * @param newTime the new value
	 * @param existingTime time found in reference file
	 * @return table of long corresponding to the time references of each test in the order they are called in the main
	 *         test method
	 * @throws IOException
	 */

	public static void updateReferenceTime(String className, long newTime, long existingTime) throws IOException {

		File inputFile = new File(UPDATED_REFERENCE_TIME_FILE_PATH);
		File outputFile = new File(UPDATED_REFERENCE_TIME_FILE_PATH + ".upd");

		BufferedReader bufferedReader = null;
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));

		try {
			long updatedTime = newTime;
			long[] referenceTimes = null;
			if ((existingTime) > 0) {
				updatedTime = (newTime + existingTime) / 2;
			}
			if (inputFile.exists()) {
				bufferedReader = new BufferedReader(new FileReader(inputFile));
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if (line.startsWith(className + "-")) {

						String[] strs = line.split("-", 0);
						referenceTimes = new long[strs.length - 1];
						for (int i = 1; i < strs.length; i++) {
							referenceTimes[i - 1] = Long.parseLong(strs[i]);
						}

						bufferedWriter.write(className + "-" + Long.toString(updatedTime));
						bufferedWriter.newLine();
					} else {
						bufferedWriter.write(line);
						bufferedWriter.newLine();
					}
				}

			}
			if (referenceTimes == null) {
				bufferedWriter.write(className + "-" + Long.toString(updatedTime));
				bufferedWriter.newLine();
			}
		} finally {
			if (bufferedReader != null) {
				bufferedReader.close();
			}
			bufferedWriter.close();
			inputFile.delete();
			outputFile.renameTo(inputFile);

		}
	}

}
