package fr.ifremer.globe.netcdfLibLoading;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.netcdf.jna.Nc4Iosp;
import fr.ifremer.globe.netcdf.ucar.NCVariable;

/**
 * Unit test to check netcdf/gdal natives libraries are properly loaded
 */
public class NetcdfLibLoadingTest {

	/**
	 * Loading from the netcdf plugin first 
	 */
	@Test
	public void loadingByNetCdfPlugin() {

		System.setProperty("jna.debug_load", "true");
		try {
			// Call netcdg first and after gdal
			new NCVariable(0, null, "", 0, null);
			GdalUtils.makePoint(1,1);
			
			// Check library
			Assert.assertTrue(Nc4Iosp.isClibraryPresent());
			
		} catch (NoClassDefFoundError e) {
			fail("Loading by netcdf plugin has failed, NoClassDefFoundError: " + e.getMessage());
		}
	}

	/**
	 * Loading from the gdal plugin first
	 */
	//@Test
	public void loadingByGdalPlugin() {

		System.setProperty("jna.debug_load", "true");
		try {
			// Call gdal first and after netcdf
			GdalUtils.makePoint(1,1);
			new NCVariable(0, null, "", 0, null);
			
			// Check library
			Assert.assertTrue(Nc4Iosp.isClibraryPresent());

		} catch (NoClassDefFoundError e) {
			fail("Loading by gdal plugin has failed, NoClassDefFoundError: " + e.getMessage());
		}
	}
}
