package fr.ifremer.globe.dev;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.draft.NavigationChartSeries;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.utils.exception.GIOException;

public class TestJavaFXChart {

	private final static String TEST_DIR = "F:\\data\\globe\\nvi_charline_02-2019\\NVI";
	// private final static String TEST_DIR = "F:\\data\\globe\\nvi_campagne";
	// private final static String TEST_DIR =
	// "F:\\data\\globe\\mbg_EM2040_Capbreton_2016";
	// data = getDataFromDir("F:\\data\\globe\\mbg_EM2040_Capbreton_2016");
	// data =
	// getDataFromFile("F:\\data\\globe\\test_export_nvi\\application\\0001_20150424_042219_Europe.mbg");
	// data =
	// getDataFromFile("F:\\data\\globe\\test_kmall\\0075_20120525_140214_Thalia.mbg");

	@SuppressWarnings("unused")
	// private final static String TEST_FILE =
	// "F:\\data\\globe\\nvi_test\\201700400055.nvi"; //48mo
	private final static String TEST_FILE = "F:\\data\\globe\\nvi_test\\201700410055.nvi"; // 73mo

	//// DATA PROVIDERS

	public List<IChartSeries> getDataFromDir(String dirpath) throws IOException, GIOException {
		List<IChartSeries> result = new ArrayList<>();
		int maxFile = 800;
		int i = 0;
		for (File file : new File(dirpath).listFiles()) {
			i++;
			if (i > maxFile)
				break;
			if (file.getName().endsWith(".nvi")) {
				try {
					result.add(getDataFile(file.getAbsolutePath()));
				} catch (Exception e) {
					System.err.println("File not read : " + file.getName());
				}
			}
		}
		return result;
	}

	public static IChartSeries<? extends IChartData> getDataFile(String filepath) throws GIOException {
		Optional<INavigationDataSupplier> optNavDataProvider = INavigationService.grab()
				.getNavigationDataProvider(filepath);
		if (!optNavDataProvider.isPresent())
			throw new GIOException("File " + filepath + " doesn't provide navigation data.");
		return new NavigationChartSeries(optNavDataProvider.get().getNavigationData(true));
	}

	@Test
	public void test() throws IOException, GIOException, FXChartException {

		try {
			// JavaFXChartMain test = new JavaFXChartMain();

			final Display display = Display.getDefault();
			final Shell shell = new Shell(display);
			shell.setText("Test");
			shell.setLayout(new FillLayout());
			shell.setSize(shell.computeSize(400, 600));

			FXChart fxChart = new FXChart(shell);

			fxChart.setData(Arrays.asList(getDataFile(TEST_FILE)));
			// fxChart.setData(getDataFromDir(TEST_DIR));

			fxChart.setXAxisLabelFormatter(value -> FormatLatitudeLongitude.longitudeToString((double) value));
			fxChart.setYAxisLabelFormatter(value -> FormatLatitudeLongitude.latitudeToString((double) value));

			shell.open();
			shell.pack();

			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
