package fr.ifremer.globe.dev;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.junit.Test;

public class TestDateTime {
	@Test
	public void testDateTime()
	{
//		Instant today=Instant.now();
//		long second=today.getLong(ChronoField.INSTANT_SECONDS);
//		long nanoToday=today.getLong(ChronoField.NANO_OF_SECOND);
//		long value=second*1_000_000_000+nanoToday;
//		long timeSecond=Long.divideUnsigned(value, 1_000_000_000);
//		long nano=Long.remainderUnsigned(value, 1_000_000_000);
//		
//		Instant t=Instant.ofEpochSecond(timeSecond, nano);
//		LocalDateTime timeEpoch=LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC);
//		LocalDateTime time_1601=LocalDateTime.of(1601,1,1,0,0,0);
//		Duration elapsed=Duration.between(time_1601, timeEpoch);
//		long shiftFrom1601_1970=elapsed.getSeconds()*1_000_000_000+elapsed.getNano();
//		time_1601.plusSeconds(elapsed.getSeconds());
//		
//		long sec=time_1601.atZone(ZoneId.systemDefault()).toInstant().getNano();
		
		long v=1530188759248L*1_000_000L;
		LocalDateTime time1=ofEpoch(v);
		
		LocalDateTime time2=of1601(v);
		System.out.println(time1);
		System.out.println(time2);
	}
	/**
	 * return date as a unsigned long value in ms 
	 * */
	public long toEpoch(LocalDateTime date)
	{
		return date.toEpochSecond(ZoneOffset.UTC)*1_000_000_000+date.getNano();
	}
	
	/**
	 * get LocalDateTime from unsigned long value in nanosecond since 1901
	 * */
	public LocalDateTime ofEpoch(long value)
	{
		long timeSecond=Long.divideUnsigned(value, 1_000_000_000);
		int nano=(int)Long.remainderUnsigned(value, 1_000_000_000);
		LocalDateTime timeEpoch=LocalDateTime.ofEpochSecond(timeSecond, nano, ZoneOffset.UTC);
		return timeEpoch;

	}
	/**
	 * compute a new java Date Time from a long given in nanosecond since 1601
	 * @return 
	 * */
	public LocalDateTime of1601(long value)
	{

		LocalDateTime timeEpoch=LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC);
		LocalDateTime time_1601=LocalDateTime.of(1601,1,1,0,0,0);
		Duration elapsed=Duration.between(time_1601, timeEpoch);
		
		long shiftFrom1601_1970=elapsed.getSeconds()*1_000_000_000+elapsed.getNano();
		long valueFromEpoch=value+shiftFrom1601_1970;
		
		long timeSecond=Long.divideUnsigned(valueFromEpoch, 1_000_000_000);
		long nano=Long.remainderUnsigned(valueFromEpoch, 1_000_000_000);
		
		LocalDateTime time=LocalDateTime.ofEpochSecond(timeSecond,(int) nano, ZoneOffset.UTC);
		return time;
	}
	
}
