package fr.ifremer.globe.dev;

import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.FileSystems;

import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DoublePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.FilePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.ui.utils.preference.PreferenceEditorDialog;

public class TestPref {

	public enum Day {
	    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
	    THURSDAY, FRIDAY, SATURDAY 
	}
	
	
	@Test
	public void test() {
		// on cree des attribut pour le test
		PreferenceComposite rnode = PreferenceRegistry.getInstance().getRootNode();
		final PreferenceComposite compo = new PreferenceComposite(rnode, "pref");
		 
		Display.getCurrent ();
		GColor purple = new GColor (255, 0, 255);

		PreferenceAttribute<?> a0 = new BooleanPreferenceAttribute("1_BOOL","is active", false);
		PreferenceAttribute<?> a1 = new ColorPreferenceAttribute("2_COLOR", "fancy color !", purple);
		PreferenceAttribute<?> a2 = new DirectoryPreferenceAttribute("3_DIR", "work dir",  FileSystems.getDefault().getPath("d:/test/"));
		PreferenceAttribute<?> a3 = new DoublePreferenceAttribute("4_DOUBLE", "double value", 4096.10024);
		PreferenceAttribute<?> a4 = new EnumPreferenceAttribute("5_ENUM", "day", Day.FRIDAY);
		PreferenceAttribute<?> a5 = new FilePreferenceAttribute("6_FILE", "work file", new File("d:/test/out.txt").toPath());
		PreferenceAttribute<?> a6 = new IntegerPreferenceAttribute("7_INT", "version", 10);
		PreferenceAttribute<?> a7 = new StringPreferenceAttribute("8_STRING", "app name", "prefTest");
		
		compo.declareAttribute(a0);
		compo.declareAttribute(a1);
		compo.declareAttribute(a2);
		compo.declareAttribute(a3);
		compo.declareAttribute(a4);
		compo.declareAttribute(a5);
		compo.declareAttribute(a6);
		compo.declareAttribute(a7);
		
		//liste size test
		PreferenceAttribute<String> a;
		for(int i =0; i<16;i++){
			a = new StringPreferenceAttribute("STRING"+i, "chaine "+i, "default");
			compo.declareAttribute(a);
		}

		
		//original pref
		System.out.println("Original Pref ---------------");
		String[] key = compo.keys();
		for (int i = 0; i < key.length; i++) {
			PreferenceAttribute<?> attribute = compo.getAttribute(key[i]);
			System.out.println(attribute.getDisplayedName()+" = "+attribute.getValue());
		}
		
		Assert.assertNotNull(compo.keys());
		final Display display = Display.getDefault();
		final Shell shell = new Shell(display);
		shell.setText(compo.getAbsolutePath());
		shell.setLayout(new FillLayout());
		shell.setSize(shell.computeSize(400, 600));
		shell.open();
		
		new PreferenceEditorDialog(shell).open();
		
		
//		shell.addListener(SWT.Close, new Listener() {
//			@Override
//			public void handleEvent(org.eclipse.swt.widgets.Event event) {
//				shell.setVisible(false);
//				//update and disp pref before close
//				perfpage.performOk();
//				System.out.println("final pref -----------");
//				String[] key = compo.keys();
//				for (int i = 0; i < key.length; i++) {
//					PreferenceAttribute<?> attribute = compo.getAttribute(key[i]);
//					System.out.println(attribute.getDisplayedName()+" = "+attribute.getValue());
//				}
//			}
//		    });
		
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}

		}
		
		display.dispose();
		
		fail("Not yet implemented");
	}

}
