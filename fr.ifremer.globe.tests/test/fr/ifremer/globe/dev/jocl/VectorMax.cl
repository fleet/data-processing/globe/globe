// OpenCL Kernel Function for element by element vector addition
kernel void VectorMax(global const float3* data, global float3* c,int numElements, local float3 *partial_res) {
    
    // get index into global data array
    int gid = get_global_id(0);
    int lid = get_local_id(0);
    int group_size=get_local_size(0);
    
    
    // bound check, equivalent to the limit on a 'for' loop
    if (gid < numElements)
    {
        partial_res[lid]=data[gid];
    } else{
        partial_res[lid]=NAN;
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    
    for(int i=group_size/2;i>0;i>>=1)
    {
        if(!isnan(partial_res[lid+i].x)){
            
            partial_res[lid]=max(partial_res[lid],partial_res[lid+i]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if(lid==0)
    {
        c[get_group_id(0)]=partial_res[0];
    }
    
}
