void gluMultMatrixVecd(global const float* matrix, const float4 in,
                       float *out)
{
    int i;
    
    for (i=0; i<4; i++) {
        out[i] =
        in.x * matrix[0*4+i] +
        in.y * matrix[1*4+i] +
        in.z * matrix[2*4+i] +
        in.w * matrix[3*4+i];
    }
}


/**
 check if a point in contained in the polygon definition passed as parameter
 @see java Polygon.class
 */
bool contains(
              float x, float y, //point coordinates
              int npoints,
              global const int *xpoints,
               global const int *ypoints,
               global const int *boundingboxX, // the two x corners of the bounding box
               global const int *boundingboxY //the two y corners of the bounding box
              )
{
    int hits = 0;
    
    int lastx = xpoints[npoints - 1];
    int lasty = ypoints[npoints - 1];
    int curx, cury;
    
    //bounding box rejection
    bool boundingboxContained;
    boundingboxContained= x>=boundingboxX[0] && y>= boundingboxY[0] && x<boundingboxX[1] && y<boundingboxY[1];
    
    if(!boundingboxContained)
        return false;
    
    // Walk the edges of the polygon
    for (int i = 0; i < npoints; lastx = curx, lasty = cury, i++) {
        curx = xpoints[i];
        cury = ypoints[i];
        
        if (cury == lasty) {
            continue;
        }
        
        int leftx;
        if (curx < lastx) {
            if (x >= lastx) {
                continue;
            }
            leftx = curx;
        } else {
            if (x >= curx) {
                continue;
            }
            leftx = lastx;
        }
        
        float test1, test2;
        if (cury < lasty) {
            if (y < cury || y >= lasty) {
                continue;
            }
            if (x < leftx) {
                hits++;
                continue;
            }
            test1 = x - curx;
            test2 = y - cury;
        } else {
            if (y < lasty || y >= cury) {
                continue;
            }
            if (x < leftx) {
                hits++;
                continue;
            }
            test1 = x - lastx;
            test2 = y - lasty;
        }
        
        if (test1 < (test2 / (lasty - cury) * (lastx - curx))) {
            hits++;
        }
    }
    
    return ((hits % 2) ==1);
}

/*
 // Opencl function projecting a set of vector to world space
 void unproject(global const float4* data, // the vector data in x;y;z coordinates
 int numElements, //number of element to process
 global const float* invertModelViewProjectionMatrix, // invert of ProjectionMatrix*ModelViewMatrix
 global const int* viewport, // viewport
 global float4 *result) // output
 {
 // get index into global data array
 int iGID = get_global_id(0);
 
 // bound check, equivalent to the limit on a 'for' loop
 if (iGID >= numElements)  {
 return;
 }
 float in[4];
 float out[4];
 
 in[0]=data[0].x;
 in[1]=data[0].y;
 in[2]=data[0].z;
 in[3]=1.0f;
 
 //Map x and y from window coordinates
 in[0] = (in[0] - viewport[0]) / viewport[2];
 in[1] = (in[1] - viewport[1]) / viewport[3];
 
 // Map to range -1 to 1
 in[0] = in[0] * 2 - 1.f;
 in[1] = in[1] * 2 - 1.f;
 in[2] = in[2] * 2 - 1.f;
 
 gluMultMatrixVecd(invertModelViewProjectionMatrix, in, out);
 
 if (in[3] == 0.0f) return;
 out[0] /= out[3];
 out[1] /= out[3];
 out[2] /= out[3];
 
 result[iGID].x=out[0];
 result[iGID].y=out[1];
 result[iGID].z=out[2];
 
 }
 
 */

float4 projectPoint(float4 data,
               global const float* projectionmodelviewMatrix, //ProjectionMatrix*ModelViewMatrix
               global const int* viewport)

{
    private float out[4];
    gluMultMatrixVecd(projectionmodelviewMatrix, data, out);
    
    if (out[3] == 0.0f) return (float4)(0.f,0.f,0.f,0.f);
    out[0] /= out[3];
    out[1] /= out[3];
    out[2] /= out[3];
    // Map x, y and z to range 0-1
    out[0] = out[0] * 0.5f + 0.5f;
    out[1] = out[1] * 0.5f + 0.5f;
    out[2] = out[2] * 0.5f + 0.5f;
    
    // Map x,y to viewport
    out[0] = out[0] * viewport[2] + viewport[0];
    out[1] = out[1] * viewport[3] + viewport[1];
    float4 result=(float4)( out[0],out[1],out[2], 1.0f);
    return result;
    
}

// OpenCL Kernel Function for element by element vector addition
kernel void project(global const float* data,// the vector data in x;y;z coordinates
                    const int numElements, //number of element to process
                    global const float* projectionmodelviewMatrix, //ProjectionMatrix*ModelViewMatrix
                    global const int* viewport, //viewport definition
                    const int selectionPointCount,
                    global const int *xpoints, // the x coordinate of selection polygon
                     global const  int *ypoints,  // the y coordinate of selection polygon
                     global const  int *boundingboxX, // the two x corners of the bounding box
                     global const  int *boundingboxY ,//the two y corners of the bounding box
                    global float *result) {
    int gid = get_global_id(0);
    
    // bound check, equivalent to the limit on a 'for' loop
    if (gid >= numElements)  {
        return;
    }
    
    float4 ldata=(float4)(data[gid*3],data[gid*3+1],data[gid*3+2],1.0f);
    
    float4 projected=projectPoint(ldata,projectionmodelviewMatrix,viewport);
    
    bool contained=contains(projected.x, projected.y,selectionPointCount,xpoints,ypoints,boundingboxX, boundingboxY );

    result[gid]=0;
    if(contained==true)
        result[gid]=1;
    
}

