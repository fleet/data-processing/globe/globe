package fr.ifremer.globe.dev.jocl;

import java.util.Arrays;

import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLPlatform;

public class JOCLUtilities {
	public static int roundUp(int groupSize, int globalSize) {
		int r = globalSize % groupSize;
		if (r == 0) {
			return globalSize;
		} else {
			return globalSize + groupSize - r;
		}
	}
	public static void printCapacities()
	{
      	CLPlatform[] availablePlatform=CLPlatform.listCLPlatforms();
    	for(CLPlatform platform:availablePlatform)
    	{
    		System.out.println("----------------------");
    		System.out.println(platform.toString());
    		System.out.println("Name:"+platform.getName());
    		System.out.println("Profile"+platform.getProfile());
    		System.out.println("Vendor"+platform.getVendor());
    		System.out.println("Spec Version"+platform.getSpecVersion());
    		
    		for(String key:platform.getProperties().keySet())
    		{
    			System.out.println("property:"+key+" "+platform.getProperties().get(key));
    		}
    		CLDevice[] devices=platform.listCLDevices();
    		for(CLDevice device:devices)
    		{
        		System.out.println("\t----------------------");

    			System.out.println("\t device:"+device.getName());
    			System.out.println("\t driver version:"+device.getDriverVersion());
    			System.out.println("\t GlobalMemSize:"+device.getGlobalMemSize());
    			System.out.println("\t getLocalMemSize:"+device.getLocalMemSize());
    			System.out.println("\t MaxComputeUnits:"+device.getMaxComputeUnits());
    			System.out.println("\t MaxClockFrequency():"+device.getMaxClockFrequency());
    			System.out.println("\t MaxWorkItemSizes:"+Arrays.toString(device.getMaxWorkItemSizes()));
    			System.out.println("\t getMaxMemAllocSize():"+device.getMaxMemAllocSize());
    			System.out.println("\t MaxWorkGroupSize():"+device.getMaxWorkGroupSize());
    			System.out.println("\t Profile:"+device.getProfile());
    			System.out.println("\t Vendor:"+device.getVendor());
    			System.out.println("\t ID:"+device.getID());
    			System.out.println("\t Type:"+device.getType());

    		}
    		System.out.println("----------------------");
    	}
	}
}
