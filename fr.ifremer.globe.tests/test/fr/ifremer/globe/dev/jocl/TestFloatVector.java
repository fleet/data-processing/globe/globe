package fr.ifremer.globe.dev.jocl;

import static com.jogamp.opencl.CLMemory.Mem.*;
import static java.lang.System.nanoTime;
import java.nio.FloatBuffer;
import org.junit.Assert;
import org.junit.Test;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLKernel;
import com.jogamp.opencl.CLPlatform;
import com.jogamp.opencl.CLProgram;
import com.jogamp.opencl.llb.CL;

import fr.ifremer.globe.ogl.renderer.infrastructure.EmbeddedResources;

import com.jogamp.opencl.CLDevice.Type;

public class TestFloatVector {
	private static final int _VectorSize = 3;

	@Test
	public void main()
	{
		JOCLUtilities.printCapacities();


		int elementCount=2<<6 ;
		run(elementCount).print(elementCount);
		elementCount=2<<14 ;
		run(elementCount).print(elementCount);
		elementCount=2<<16 ;
		run(elementCount).print(elementCount);
		elementCount=2<<18 ;
		run(elementCount).print(elementCount);
		elementCount=2<<20 ;
		run(elementCount).print(elementCount);
		elementCount=2<<22 ;
		run(elementCount).print(elementCount);
	}
	public Time run( int elementCount)
	{
		long time=0;
		long clTime = 0;
		CLContext context = CLContext.create(Type.ALL);
		try{
			CLDevice device = context.getMaxFlopsDevice();
			System.err.println("Choose "+device.getName() + " device");
			// create command queue on device.
			CLCommandQueue queue = device.createCommandQueue();
			int localWorkSize=device.getMaxWorkGroupSize();
			int globalWorkSize = JOCLUtilities.roundUp(localWorkSize, elementCount);   // rounded up to the nearest multiple of the localWorkSize
			int groupCount=globalWorkSize/localWorkSize;
			// load sources, create and build program
			//create Float3 Buffer;
			String code=  EmbeddedResources.getText(this.getClass(),"test/fr/ifremer/globe/dev/jocl/VectorMax.cl");
			CLProgram program = context.createProgram(code).build();

			CLBuffer<FloatBuffer> clBufferA = context.createFloatBuffer(_VectorSize*elementCount, READ_ONLY);

			CL cl = CLPlatform.getLowLevelCLInterface(); 

			CLBuffer<FloatBuffer> clReturnMaxBuffer = context.createFloatBuffer(_VectorSize*groupCount, READ_WRITE);
			for(int i = 0; i < _VectorSize*groupCount; i++)
			{
				clReturnMaxBuffer.getBuffer().put(Float.MIN_VALUE);
			}
			clReturnMaxBuffer.getBuffer().rewind();


			fillBuffer(clBufferA.getBuffer(),elementCount);
			// get a reference to the kernel function with the name 'VectorAdd'
			// and map the buffers to its input parameters.

			CLKernel kernel = program.createCLKernel("VectorMax");
			kernel.putArgs(clBufferA)
			.putArgs(clReturnMaxBuffer)
			.putArg(elementCount);
			cl.clSetKernelArg(kernel.ID, 3, (long)localWorkSize*Float.BYTES*3, null); //allocate local memory

			// asynchronous write of data to GPU device,
			// followed by blocking read to get the computed results back.
			time = nanoTime();
			queue.putWriteBuffer(clBufferA, false)
			.putWriteBuffer(clReturnMaxBuffer, false)
			.put1DRangeKernel(kernel, 0, globalWorkSize, localWorkSize)
			.putReadBuffer(clReturnMaxBuffer, true);
			time = nanoTime() - time;
			clTime=time;
			// print first few elements of the resulting buffer to the console.
			float maxOpenCL=Float.MIN_VALUE;
			clReturnMaxBuffer.getBuffer().rewind();
			//  System.err.print("Min values per group :");
			for(int i = 0; i < _VectorSize*groupCount; i++)
			{
				float value=clReturnMaxBuffer.getBuffer().get();
				//  	System.err.print(value + ", ");
				maxOpenCL=Math.max(maxOpenCL, value);
			}
			clBufferA.getBuffer().rewind();
			float maxx=Float.MIN_VALUE;
			time = nanoTime();
			for(int i = 0; i < elementCount*_VectorSize; i++)
			{
				float value=clBufferA.getBuffer().get();
				maxx=Math.max(maxx, value);
			}
			System.err.println("max results snapshot: "+maxOpenCL);

			time = nanoTime() - time;

			Assert.assertEquals(maxx, maxOpenCL,10e-5);
			//           Assert.assertEquals(maxy, clReturnBuffer.getBuffer().get(),10e-5);
			//          Assert.assertEquals(maxz, clReturnBuffer.getBuffer().get(),10e-5);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		finally{
			// cleanup all resources associated with this context.
			context.release();
		}
		return new Time(clTime, time);
	}
	private static void fillBuffer(FloatBuffer buffer, int elementCount) {
		for(int i=0;i<elementCount*3;i++)
		{

			buffer.put((float)Math.random());
		}
		buffer.rewind();
	}
	
}
