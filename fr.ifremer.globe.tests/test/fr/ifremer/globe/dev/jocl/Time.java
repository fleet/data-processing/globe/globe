package fr.ifremer.globe.dev.jocl;

public class Time {
	Time(long javaTime,long openClTime)
	{
		this.javaTime=javaTime;
		this.openClTime=openClTime;
	}
	public long javaTime;
	public long openClTime;
	public void print(int elementSize)
	{
		System.err.println("element count "+elementSize+" computation took: java "+(javaTime/1000000)+" vs opencl "+openClTime/1000000+" ms");

	}
}
