package fr.ifremer.globe.dev.jocl;

import static com.jogamp.opencl.CLMemory.Mem.READ_ONLY;
import static com.jogamp.opencl.CLMemory.Mem.READ_WRITE;
import static java.lang.System.nanoTime;
import static java.lang.System.out;

import java.awt.Polygon;
import java.awt.geom.Rectangle2D;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.core.matrices.Matrix4D;
import fr.ifremer.globe.ogl.core.matrices.Matrix4F;
import fr.ifremer.globe.ogl.renderer.infrastructure.EmbeddedResources;
import fr.ifremer.globe.ogl.renderer.scene.Camera;
import fr.ifremer.globe.ogl.renderer.scene.CameraVisitor;
import fr.ifremer.globe.ogl.renderer.scene.DefaultCameraCreator;
import fr.ifremer.globe.ogl.renderer.scene.OrthographicCamera;
import fr.ifremer.globe.ogl.renderer.scene.PerspectiveCamera;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.util.ViewProjectionType;

import org.junit.Assert;
import org.junit.Test;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLDevice.Type;
import com.jogamp.opencl.CLKernel;
import com.jogamp.opencl.CLProgram;

public class TestJOCLProjection {

	private Camera camera;
	/**
	 * Set the camera's parameters.
	 */
	class CameraConfigurator implements CameraVisitor {

		/**
		 * Follow the link.
		 * 
		 * @see fr.ifremer.globe.ogl.renderer.scene.CameraVisitor#accept(fr.ifremer.globe.ogl.renderer.scene.OrthographicCamera)
		 */
		@Override
		public void accept(OrthographicCamera camera) {
			camera.setOrthographics(-ViewPointX/2,ViewPointX/2,-ViewPointY/2,ViewPointY/2);
		}

		/**
		 * Follow the link.
		 * 
		 * @see fr.ifremer.globe.ogl.renderer.scene.CameraVisitor#accept(fr.ifremer.globe.ogl.renderer.scene.PerspectiveCamera)
		 */
		@Override
		public void accept(PerspectiveCamera camera) {
			camera.setFieldOfViewY(Math.PI / 10.0);
			camera.setPerspectiveNearPlaneDistance(0.00001); // 0.000001
			camera.setPerspectiveFarPlaneDistance(10000.0);
		}

	}

	private static final int MAX_X=10;
	private static final int MAX_Z=10;
	private static final int MAX_Y=10;

	private static final int _Vector3Size = 3;
	private static final int _Matrix44Size = 16;
	private static final int _ViewPortSize = 4;

	private static int ViewPointX=700;
	private static int ViewPointY=120;
	public Polygon createSelectionPolygon()
	{
		int pointCount=10;
		int xpoints[]=new int[pointCount];
		int ypoints[]=new int[pointCount];
		for(int i=0;i<pointCount;i++)
		{
			int x=(int) (Math.random()*ViewPointX);
			int y=(int) (Math.random()*ViewPointY);
			xpoints[i]=x;
			ypoints[i]=y;
		}
		
		Polygon polygonTemp = new Polygon(xpoints, ypoints, pointCount);

		return polygonTemp;
		
	}
	
	public Time run( int elementCount)
	{		
		GLU glu=new GLU();
		SceneState _sceneState = new SceneState(DefaultCameraCreator.create(ViewProjectionType.ORTHOGRAPHIC));
		this.camera = _sceneState.getCamera();
		this.camera.visit(new CameraConfigurator());
		int viewport[] = {0,0,ViewPointX,ViewPointY};

		Matrix4D modelViewMatrix = _sceneState.getModelViewMatrix();
		Matrix4D projectionMatrix = _sceneState.getProjectionMatrix();
		float mvmatrix[] = modelViewMatrix.toMatrix4F().getReadOnlyColumnMajorValues().clone();
		float projmatrix[] =  projectionMatrix.toMatrix4F().getReadOnlyColumnMajorValues().clone();

		Polygon selection=createSelectionPolygon();
		Rectangle2D bb=selection.getBounds2D();
		
		
		float unprojCoord[] = new float[3];
		CLContext context = CLContext.create(Type.ALL);
		Matrix4F projectionModelViewMatrix=projectionMatrix.multiply(modelViewMatrix).toMatrix4F();
		try{
			CLDevice device = context.getMaxFlopsDevice();
			out.println("using "+device);
			// create command queue on device.
			CLCommandQueue queue = device.createCommandQueue();

			int localWorkSize = device.getMaxWorkGroupSize();//min(device.getMaxWorkGroupSize(), 256);  // Local work size dimensions
			int globalWorkSize = roundUp(localWorkSize, elementCount);   // rounded up to the nearest multiple of the localWorkSize
			// load sources, create and build program
			String code=  EmbeddedResources.getText(this.getClass(),"test/fr/ifremer/globe/dev/jocl/project2screenSpace.cl");

			CLProgram program = context.createProgram(code).build();

			CLBuffer<FloatBuffer> clBufferPoint = createDataBuffer(context,elementCount);

			//create buffers
			CLBuffer<FloatBuffer> clReturnMaxBuffer = context.createFloatBuffer(elementCount, READ_WRITE);
			for(int i = 0; i < elementCount; i++)
			{
				clReturnMaxBuffer.getBuffer().put(Float.MIN_VALUE);
			}
			clReturnMaxBuffer.getBuffer().rewind();


			// create model view buffer
			CLBuffer<FloatBuffer> clInvertProjectionMatrix = context.createFloatBuffer(_Matrix44Size, READ_ONLY);
			clInvertProjectionMatrix.getBuffer().put(projectionModelViewMatrix.getReadOnlyColumnMajorValues());
			clInvertProjectionMatrix.getBuffer().rewind();

			CLBuffer<IntBuffer> clViewport = context.createIntBuffer(_ViewPortSize, READ_ONLY);
			clViewport.getBuffer().put(viewport);
			clViewport.getBuffer().rewind();

			CLBuffer<IntBuffer> clSelectionXPoints = context.createIntBuffer(selection.npoints, READ_ONLY);
			clSelectionXPoints.getBuffer().put(selection.xpoints);
			clSelectionXPoints.getBuffer().rewind();
		
			CLBuffer<IntBuffer> clSelectionYPoints = context.createIntBuffer(selection.npoints, READ_ONLY);
			clSelectionYPoints.getBuffer().put(selection.ypoints);
			clSelectionYPoints.getBuffer().rewind();
			
			CLBuffer<IntBuffer> bbX = context.createIntBuffer(2, READ_ONLY);
			bbX.getBuffer().put((int)bb.getX());
			bbX.getBuffer().put((int)bb.getMaxX());
			bbX.getBuffer().rewind();

			CLBuffer<IntBuffer> bbY = context.createIntBuffer(2, READ_ONLY);
			bbY.getBuffer().put((int)bb.getY());
			bbY.getBuffer().put((int)bb.getMaxY());
			bbY.getBuffer().rewind();
			
			CLKernel kernel = program.createCLKernel("project");
			kernel.putArg(clBufferPoint);
			kernel.putArg(elementCount);
			kernel.putArg(clInvertProjectionMatrix);
			kernel.putArg(clViewport);
			kernel.putArg(selection.npoints);
			kernel.putArg(clSelectionXPoints);
			kernel.putArg(clSelectionYPoints);
			kernel.putArg(bbX);
			kernel.putArg(bbY);
			kernel.putArg(clReturnMaxBuffer);

			long timeOpenCL = nanoTime();

			queue.putWriteBuffer(clBufferPoint, false)
			.putWriteBuffer(clInvertProjectionMatrix, false)
			.putWriteBuffer(clViewport, false)
			.putWriteBuffer(clSelectionXPoints, false)
			.putWriteBuffer(clSelectionYPoints, false)
			.putWriteBuffer(bbX, false)
			.putWriteBuffer(bbY, false)
			.putWriteBuffer(clReturnMaxBuffer, false)	
			.put1DRangeKernel(kernel, 0, globalWorkSize, localWorkSize)
			.putReadBuffer(clReturnMaxBuffer, true);	

			clReturnMaxBuffer.getBuffer().rewind();
			timeOpenCL = nanoTime()-timeOpenCL;

			// project with glu
			clBufferPoint.getBuffer().rewind();
			FloatBuffer javaRet=FloatBuffer.allocate(elementCount);
			long timeJAVA = nanoTime();
			boolean selectedPoint[]=new boolean[elementCount];
			for(int i=0;i<elementCount;i++)
			{

				float lon =clBufferPoint.getBuffer().get();;
				float lat = clBufferPoint.getBuffer().get();;
				float depth =clBufferPoint.getBuffer().get();;

				glu.gluProject(lon, lat, depth, mvmatrix, 0, projmatrix,  0, viewport, 0, unprojCoord, 0);

				selectedPoint[i]=selection.contains(unprojCoord[0],unprojCoord[1]);
				javaRet.put(selectedPoint[i] ? 1:0.f);
			}
			
			timeJAVA = nanoTime()-timeJAVA;
			//COMPARE VALUES
			Time t=new Time(timeJAVA, timeOpenCL);
			t.print(elementCount);
			clReturnMaxBuffer.getBuffer().rewind();
			javaRet.rewind();
			float[] retcl=new float[elementCount];
			float[] retjava=new float[elementCount];

			for(int i=0;i<elementCount;i++)
			{
				retcl[0]=clReturnMaxBuffer.getBuffer().get();
				retjava[0]=javaRet.get();
			}
			Assert.assertArrayEquals("vector "+elementCount,retcl, retjava, 10e-5f);
			return t;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}		
		finally{
			// cleanup all resources associated with this context.
			context.release();
		}
		return null;
	}

	private CLBuffer<FloatBuffer> createDataBuffer(CLContext context, int elementCount) {
		CLBuffer<FloatBuffer> clBufferPoint=context.createFloatBuffer(_Vector3Size*elementCount, READ_ONLY);
		for(int i=0;i<elementCount;i++)
		{
			float a=(float) ((Math.random()-0.5)*MAX_X);
			float b=(float) ((Math.random()-0.5)*MAX_Y);
			float c=(float) ((Math.random()-0.5)*MAX_Z);
			clBufferPoint.getBuffer().put(a); //lat
			clBufferPoint.getBuffer().put(b); //long 
			clBufferPoint.getBuffer().put(c); //depth
		}
		clBufferPoint.getBuffer().rewind();
		return clBufferPoint;

	}

	public void fillDataBuffer()
	{

	}
	private static int roundUp(int groupSize, int globalSize) {
		int r = globalSize % groupSize;
		if (r == 0) {
			return globalSize;
		} else {
			return globalSize + groupSize - r;
		}
	}
	@Test
	public void main(){
		int elementCount=1000000;
		ArrayList<Time> ret=new ArrayList<>();
		for(int i=0;i<20;i++)
		{
			Time t=run(elementCount);
			ret.add(t);
		}
		System.err.println("Performance OpenCL");
		for(Time t:ret)
		{
			System.err.print(t.openClTime/1000000+";");
		}
		System.err.println("Performance JAVA");
		for(Time t:ret)
		{
			System.err.print(t.javaTime/1000000+";");
		}
		System.err.println("");
	}
}
