package fr.ifremer.globe.dev;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.FloatBuffer;

import com.jogamp.opengl.DebugGL2;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import com.jogamp.opengl.util.FPSAnimator;

import fr.ifremer.globe.editor.swath.ui.utils.TransformationUtils;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.textures.TextureFloat;

/**
 * The opengClass
 */
@SuppressWarnings("serial")
class SimpleFloatTextureOpengl extends GLCanvas implements GLEventListener {
	private TextureFloat texture;

	public SimpleFloatTextureOpengl(int width, int height) {
		// super( );

		super(new GLCapabilities(GLProfile.getDefault()));
		setSize(width, height);
		addGLEventListener(this);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		drawable.setGL(new DebugGL2(gl));

		gl.glClearColor(0f, 0f, 0f, 1f);
		int sizeX = 1;
		int sizeY = 1000;

		FloatBuffer pixels = FloatBuffer.allocate(sizeX * sizeY * 4);
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				Color color = TransformationUtils.getColorFromNormalizedElevation((double) j / (double) sizeY);
				pixels.put((i * sizeY + j) * 4, color.getRed() / 255f);
				pixels.put((i * sizeY + j) * 4 + 1, color.getGreen() / 255f);
				pixels.put((i * sizeY + j) * 4 + 2, color.getBlue() / 255f);
				pixels.put((i * sizeY + j) * 4 + 3, 1f);
			}
		}

		texture = Device2x.createTextureFloatRectangle(pixels, sizeX, sizeY, GL.GL_TEXTURE_2D, GL.GL_LINEAR,
				GL.GL_LINEAR, GL.GL_RGB, GL.GL_RGBA, GL.GL_FLOAT);

	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL gl = drawable.getGL();
		gl.glViewport(0, 0, width, height);
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		gl.glClearColor(1.0f, 1.0f, 1.0f, 1f);

		texture.enable(gl);
		texture.bind(gl);

		// Write triangle.
		gl.glBegin(GL.GL_TRIANGLE_FAN);
		gl.glMultiTexCoord2f(GL.GL_TEXTURE0, 0f, 1f);
		gl.glVertex3f(-1, -0.99f, 0);
		gl.glMultiTexCoord2f(GL.GL_TEXTURE0, 0f, 1f);
		gl.glVertex3f(+1, -0.99f, 0);
		gl.glMultiTexCoord2f(GL.GL_TEXTURE0, 0f, 0f);
		gl.glVertex3f(0, 0.99f, 0);
		gl.glEnd();

		texture.disable(gl);
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {

	}
}

/**
 * The openGL launcher
 */
public class TestOpenGLFloatTextureApp {
	@Test
	public void main() {
		final Display display = Display.getCurrent();
		final Shell shell = new Shell(display);
		shell.setText("Lazy Tree");
		shell.setLayout(new FillLayout());
		runJFrame();

		shell.setSize(shell.computeSize(300, 300));
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}

		}
		display.dispose();
		return;
	}

	private void runJFrame() {

		SimpleFloatTextureOpengl ogl = new SimpleFloatTextureOpengl(800, 999);

		// Erstelle neuen Frame und bette die Zeichenfl�che ein
		Frame frame = new Frame("OPEN GL");
		frame.setSize(800, 1000);
		frame.add(ogl);

		// Erstelle einen Window Listener und sorge f�r korrektes
		// schlie�en des Programmes
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
		// Start animator (which should be a field).
		FPSAnimator animator = new FPSAnimator(ogl, 60);
		animator.start();
	}
}
