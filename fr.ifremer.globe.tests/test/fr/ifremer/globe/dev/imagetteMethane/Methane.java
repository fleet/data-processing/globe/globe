package fr.ifremer.globe.dev.imagetteMethane;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

import org.junit.Test;

/**
 * read data from the file and create all necessary buffers
 */
public class Methane {

	private FileReader monFichier;
	private BufferedReader tampon;


	//echos

	private DataOutputStream Energie_Bin = null;
	private String filenameEnergieBin = "D:/TMP_KL2/Energie.bin";

	private DataOutputStream lat_Bin = null;
	private String filenameLatBin = "D:/TMP_KL2/Latitude.bin";

	private DataOutputStream lon_Bin = null;
	private String filenameLonBin = "D:/TMP_KL2/Longitude.bin";

	private DataOutputStream Z_Bin = null;
	private String filenameZBin = "D:/TMP_KL2/Z.bin";

	private DataOutputStream iPingNumber_Bin = null;
	private String filenameiPingNumberBin = "D:/TMP_KL2/iPing.bin";

	private void readFileEcho(String filenameBis) {

		String line = null;
		String[] champs =null;
		String CH4_nM = null;
		String lat = null;
		String lon = null;
		String depth = null;

		float energie_Float = 0;
		float z_Float = 0;
		float iPing_Float = 0;

		double lat_Double = 0.0;
		double lon_Double = 0.0;

		//initialise buffer de lecture

		try {
			monFichier = new FileReader(filenameBis);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		tampon = new BufferedReader(monFichier);

		//initialise fichier binaire

		try {

			Energie_Bin = new DataOutputStream ( new FileOutputStream (filenameEnergieBin));
			lat_Bin = new DataOutputStream ( new FileOutputStream (filenameLatBin));
			lon_Bin = new DataOutputStream ( new FileOutputStream (filenameLonBin));
			Z_Bin =  new DataOutputStream ( new FileOutputStream (filenameZBin)); 
			iPingNumber_Bin = new DataOutputStream ( new FileOutputStream (filenameiPingNumberBin));

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		// passe la première ligne, pas de valeur, c'est les entetes

		try {
			line = tampon.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// on lit le fichier source ligne après ligne

		while(true)
		{

			try {
				line = tampon.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (line==null)
				break;

			//identification des champs de la ligne
			champs = line.split("\t");
			//System.out.println(champs);

			CH4_nM = champs[9];
			depth = champs[6];
			lat = champs[4];
			lon = champs[5];
			

			//conversion string en double
			energie_Float = Float.parseFloat(CH4_nM);
			z_Float = -Math.abs(Float.parseFloat(depth));
			iPing_Float++;

			lat_Double = Double.parseDouble(lat); 
			lon_Double = Double.parseDouble(lon); 

			//remplissage fichier binaire avec les doubles

			// creation de buffer car convention big endian et pas littele endian.......

			ByteBuffer byteEnergie=ByteBuffer.allocate(Float.BYTES*1);
			byteEnergie.order(ByteOrder.LITTLE_ENDIAN);
			FloatBuffer bufferEnergie=byteEnergie.asFloatBuffer();

			ByteBuffer byteLat=ByteBuffer.allocate(Double.BYTES*1);
			byteLat.order(ByteOrder.LITTLE_ENDIAN);
			DoubleBuffer bufferLat=byteLat.asDoubleBuffer();

			ByteBuffer byteLon=ByteBuffer.allocate(Double.BYTES*1);
			byteLon.order(ByteOrder.LITTLE_ENDIAN);
			DoubleBuffer bufferLon=byteLon.asDoubleBuffer();

			ByteBuffer byteZ=ByteBuffer.allocate(Float.BYTES*1);
			byteZ.order(ByteOrder.LITTLE_ENDIAN);
			FloatBuffer bufferZ=byteZ.asFloatBuffer();

			ByteBuffer byteiPing=ByteBuffer.allocate(Float.BYTES*1);
			byteiPing.order(ByteOrder.LITTLE_ENDIAN);
			FloatBuffer bufferiPing=byteiPing.asFloatBuffer();

			bufferEnergie.put(energie_Float);
			bufferLat.put(lat_Double);
			bufferLon.put(lon_Double);
			bufferZ.put(z_Float);
			bufferiPing.put(iPing_Float);

			try {

				Energie_Bin.write(byteEnergie.array());
				lat_Bin.write(byteLat.array());
				lon_Bin.write(byteLon.array());
				Z_Bin.write(byteZ.array());
				iPingNumber_Bin.write(byteiPing.array());

			} catch (IOException e) {
				e.printStackTrace();
			}


		}
		
			//close fichier binaire
			try {
			Energie_Bin.close();
				lat_Bin.close();
				lon_Bin.close();
				Z_Bin.close();
				iPingNumber_Bin.close();

			} catch (IOException e1) {
				e1.printStackTrace();
			}

		//close fichier source
		try {
			tampon.close();
			monFichier.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void main()
	{
		readFileEcho("D:/TMP_KL2/FileEcho.txt");
	}
}