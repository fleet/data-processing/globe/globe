#version 150
 
uniform mat4 viewMatrix, projMatrix;
 
in vec3 position; //w auto set to 1.0
in float reflectivity;
in int validity;

out vec3 Color;
 
void main()
{
    Color = vec3(validity,validity,1);
    vec4 pos=vec4(position,1.0);
    gl_Position = projMatrix * viewMatrix * pos ;
}