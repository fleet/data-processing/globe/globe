#version 120

uniform vec4 u_valid_color;
uniform vec4 u_invalid_color;

attribute vec3 in_position;
attribute float in_validity;

varying vec4 f_color;

void main() {
	// compute position relative to the center
	vec3 position = in_position;
	gl_Position = gl_ModelViewProjectionMatrix * vec4(position, 1.0);
	
	// Color according to validity
	if( in_validity == 1.0 )	
		f_color = u_valid_color;
	else if ( in_validity == 0.0 )
		f_color = u_invalid_color;
    else f_color = vec4(0.0, 0.0, 1.0, 1.0);//color;
    }
    
    
