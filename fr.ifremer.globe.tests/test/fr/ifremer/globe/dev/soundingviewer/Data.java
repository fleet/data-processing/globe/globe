
package fr.ifremer.globe.dev.soundingviewer;

import java.nio.ByteBuffer;
/**
 * Sounding data represent a sounding as it is stored in the grid index.
 * User shall prefer buffer usage to the manipulation of such object as it can be less memory efficient
 * */
public class Data {
	float x;
	float y;
	float z;
	
	float reflectivity;
	byte validity;
	byte detection;
//	byte editable;
	int cycle;
	short beam;
	
	public static int BYTES=Float.BYTES*4+Byte.BYTES*2/*Byte.BYTES*3*/+Integer.BYTES+Short.BYTES;
//	public static int BYTES=Float.BYTES*4;
	
	/**
	 * Parse one sounding from the given {@link ByteBuffer}
	 * @param source the {@link ByteBuffer}
	 * */
	public void parse(ByteBuffer source)
	{
		
		x=source.getFloat();
		y=source.getFloat();
		z=source.getFloat();
		reflectivity=source.getFloat();
		validity=source.get();
		detection=source.get();
		cycle=source.getInt();
		beam=source.getShort();
	}
	/**
	 * write this sounding data to a ByteBuffer
	 * @param dest the destination buffer
	 * */
	public void write(ByteBuffer dest)
	{
		dest.putFloat(x);
		dest.putFloat(y);
		dest.putFloat(z);
		dest.putFloat(reflectivity);
		dest.put(validity);
		dest.put(detection);
		dest.putInt(cycle);
		dest.putShort(beam);
	}
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		this.z = z;
	}
	public float getReflectivity() {
		return reflectivity;
	}
	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
	public byte getValidity() {
		return validity;
	}
	public void setValidity(byte validity) {
		this.validity = validity;
	}
	public byte getDetection() {
		return detection;
	}
	public void setDetection(byte detection) {
		this.detection = detection;
	}
	public int getCycle() {
		return cycle;
	}
	public void setCycle(int cycle) {
		this.cycle = cycle;
	}
	public short getBeam() {
		return beam;
	}
	public void setBeam(short beam) {
		this.beam = beam;
	}
}
