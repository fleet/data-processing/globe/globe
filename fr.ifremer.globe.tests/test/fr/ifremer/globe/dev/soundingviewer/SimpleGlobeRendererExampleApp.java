package fr.ifremer.globe.dev.soundingviewer;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.jogamp.opengl.awt.GLCanvas;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import com.jogamp.opengl.util.FPSAnimator;

import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;

/**
 * The openGL launcher
 * */
public class SimpleGlobeRendererExampleApp {
	
	@SuppressWarnings("unused")
	private SimpleSceneExample scene;
	
	@Test
	public void main()
	{
		final Display display = Display.getCurrent();
		final Shell shell = new Shell(display);
		shell.setText("Lazy Tree");
		shell.setLayout(new FillLayout());
		runJFrame();

		shell.setSize(shell.computeSize(300, 300));
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}

		}
		display.dispose();
		return;
	}

	private void runJFrame() {
	
		final GLCanvas canvas; 
		
		Frame frame = new Frame("OPEN GL");
		frame.setSize(800, 1000);
		
		final GraphicsWindow window = Device2x.createWindow(800, 600);
		
		canvas = (GLCanvas) window.getComponent();
		
		SimpleSceneExample scene = new SimpleSceneExample(canvas);
		
		frame.add(window.getComponent());
		
		new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				scene.dispose();
			}
		};
		
		window.addGraphicsWindowListener(scene);
		
		canvas.addKeyListener(scene);// keyboard keys
		canvas.addMouseListener(scene); // mouse buttons
		canvas.addMouseMotionListener(scene);// mouse moveModments
		canvas.addMouseWheelListener(scene);// mouse wheel
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
        // Start animator (which should be a field).
        FPSAnimator animator = new FPSAnimator(canvas, 60);
        animator.start();
	}
}
