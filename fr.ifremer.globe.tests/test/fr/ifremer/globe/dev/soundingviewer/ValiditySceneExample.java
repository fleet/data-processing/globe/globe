/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.dev.soundingviewer;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Random;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import com.jogamp.opengl.util.FPSAnimator;

import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearState;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.infrastructure.EmbeddedResources;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.shaders.ShaderProgramGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.vertexarray.VertexBufferAttributesGL2x;
import fr.ifremer.globe.ogl.renderer.scene.DefaultCameraCreator;
import fr.ifremer.globe.ogl.renderer.scene.OrthographicCamera;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener;
import fr.ifremer.globe.ogl.scene.OrthographicCameraPan;
import fr.ifremer.globe.ogl.util.ViewProjectionType;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Plugin test to show beam in an OpenGL view.<br>
 * This test is using a VertexShader. <br>
 * Beams are colored according to their validity attribute.
 * 
 * When launching thid test, it is better to check the option
 * "keep junit running after a test run when debugging"
 * 
 */
public class ValiditySceneExample {

	/**
	 * Main method.
	 */
	@Test
	public void main() throws HeadlessException, InvocationTargetException, InterruptedException {
	
		final Display display = Display.getCurrent();
		final Shell shell = new Shell(display);
		shell.setText("Lazy Tree");
		shell.setLayout(new FillLayout());
		runJFrame();
		shell.setSize(shell.computeSize(300, 300));
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}

		}
		display.dispose();
		return;
		
		
	}
	private void runJFrame() {
		
		
	
		
	

		JFrame frame = new JFrame("OPEN GL");
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GraphicsWindow window = Device2x.createWindow(800, 600);
		GLCanvas canvas = window.getComponent();	
		ValiditySceneView scene = new ValiditySceneView(canvas);
		window.addGraphicsWindowListener(scene);
		frame.add(canvas);
		frame.setVisible(true);	

		
		frame.add(window.getComponent());
		
		
		
		window.addGraphicsWindowListener(scene);
	
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
        // Start animator (which should be a field).
        FPSAnimator animator = new FPSAnimator(canvas, 60);
        animator.start();
}
	/**
	 * OpenGl view.
	 */
	class ValiditySceneView implements GraphicsWindowListener {

		private SceneState sceneState;
		private ClearState clearDepth;
		private GraphicsWindow window;
		private ContextGL2x context;
		private GLCanvas canvas;
		private DrawState drawState;

		// Model
		private int verticesCount = 100;
		private FloatBuffer vertices;
		private ByteBuffer validity;

		/**
		 * Constructor.
		 */
		public ValiditySceneView(GLCanvas canvas) {
			this.canvas = canvas;
		}

		/**
		 * Follow the link.
		 * 
		 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#init(com.jogamp.opengl.GLAutoDrawable,
		 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
		 */
		@Override
		public void init(GLAutoDrawable drawable, GraphicsWindow window) {
			this.window = window;

			Device2x.initialize();
			this.context = new ContextGL2x(this.canvas);
			this.context.makeCurrent();

			initializeModel();
			initializeScene();
			initializeCamera();

			try {
				initializeShader();
			} catch (CouldNotCreateVideoCardResourceException e) {
				e.printStackTrace();
			}

			this.context.release();
		}

		/**
		 * Initialize the shader program
		 */
		@SuppressWarnings("unchecked")
		protected void initializeShader() throws CouldNotCreateVideoCardResourceException {
			// ShaderProgram
			ShaderProgramGL2x3x shaderProgram = (ShaderProgramGL2x3x) Device2x.createShaderProgram(
					EmbeddedResources.getText("test/fr/ifremer/globe/dev/soundingviewer/shader/validityVertexShader.glsl"), "",
					EmbeddedResources.getText("test/fr/ifremer/globe/dev/soundingviewer/shader/validityFragmentShader.glsl"), true);

			// Bind Uniforms
			((Uniform<Vector4F>) shaderProgram.getUniforms().get("u_valid_color")).setValue(new Vector4F(0f, 1f, 0f, 1f));
			((Uniform<Vector4F>) shaderProgram.getUniforms().get("u_invalid_color")).setValue(new Vector4F(1f, 0f, 0f, 1f));

			// Bind attributes
			VertexBufferAttributes vertexBufferAttributes = new VertexBufferAttributesGL2x();

			// Validity
			VertexBuffer validityVertexBuffer = new VertexBufferGL2x3x(this.validity, BufferHint.StaticDraw, this.verticesCount * Byte.BYTES);
			VertexBufferAttribute validityVertexBufferAttribute = new VertexBufferAttribute(validityVertexBuffer, ComponentDatatype.Byte, 1);
			int validityAttributeLocation = shaderProgram.getVertexAttributes().get("in_validity").getLocation();
			vertexBufferAttributes.set(validityAttributeLocation, validityVertexBufferAttribute);

			// Coords
			VertexBuffer positionVertexBuffer = new VertexBufferGL2x3x(this.vertices, BufferHint.StaticDraw, this.verticesCount * 3 * Float.BYTES);
			VertexBufferAttribute positionVertexBufferAttribute = new VertexBufferAttribute(positionVertexBuffer, ComponentDatatype.Float, 3);
			int positionAttributeLocation = shaderProgram.getVertexAttributes().get("in_position").getLocation();
			vertexBufferAttributes.set(positionAttributeLocation, positionVertexBufferAttribute);

			VertexArray va = this.context.createVertexArray(vertexBufferAttributes, BufferHint.StaticDraw);
			this.drawState = new DrawState();
			this.drawState.setVertexArray(va);
			this.drawState.setShaderProgram(shaderProgram);
		}

		/** Fill the model. */
		protected void initializeModel() {
			ByteBuffer verticesInByte = ByteBuffer.allocateDirect(Float.BYTES * 3 * this.verticesCount);
			verticesInByte.order(ByteOrder.nativeOrder());
			this.vertices = verticesInByte.asFloatBuffer();

			this.validity = ByteBuffer.allocateDirect(this.verticesCount);
			this.validity.order(ByteOrder.nativeOrder());

			Random random = new Random(123456l);
			for (int i = 0; i < this.verticesCount; i++) {
				// X
				this.vertices.put((random.nextFloat() - 0.5f) * 2f);
				// Y
				this.vertices.put((random.nextFloat() - 0.5f) * 2f);
				// Depth
				this.vertices.put((random.nextFloat() - 0.5f) * 2f);

				// validity
				this.validity.put(NumberUtils.bool2byte(random.nextBoolean()));
			}

			this.vertices.clear();
			this.validity.clear();
		}

		/** Initialize SceneState and SceneDepth. */
		public void initializeScene() {
			this.sceneState = new SceneState(DefaultCameraCreator.create(ViewProjectionType.ORTHOGRAPHIC));
			this.clearDepth = new ClearState();
			this.clearDepth.setColor(new Color(255, 255, 255, 255));
		}

		/** Initialize projection and camera. */
		public void initializeCamera() {
			OrthographicCamera camera = (OrthographicCamera) this.sceneState.getCamera();

			double range = 1d;
			camera.setOrthographics(-range,range,-range, range);
			new OrthographicCameraPan(camera, this.window, range);
		}

		/**
		 * Follow the link.
		 * 
		 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#render(com.jogamp.opengl.GLAutoDrawable,
		 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
		 */
		@Override
		public void render(GLAutoDrawable drawable, GraphicsWindow windows) {
			Context context = this.window.getContext();
			context.clear(this.clearDepth);

			GL2 gl = drawable.getGL().getGL2();

			gl.glPointSize(4f);
			this.context.draw(PrimitiveType.Points, this.drawState, this.sceneState);
		}

	}

}
