package fr.ifremer.globe.dev.soundingviewer.gl2x;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import com.jogamp.common.nio.Buffers;

/**
 * Class representing VBO object
 * 
 * @author bvalliere
 */
public class ByteVBO extends TestVBOBase {

	/** Buffer */
	private ByteBuffer viewBuffer;

	public ByteVBO(int elementSize) {
		super(elementSize);
	}


	/**
	 * Add a value to buffer
	 * 
	 * @param x
	 */
	public void put(byte x) {
		viewBuffer.put(x);
	}

	/**
	 * Add a values to buffer
	 * 
	 * @param values
	 */
	public void put(byte[] values) {
		viewBuffer.put(values);
	}



	@Override
	int getGLElementSize() {
		return Buffers.SIZEOF_INT;
	}

	@Override
	protected ByteBuffer createBuffer(int elementCount2, int elementSize2) {
		ByteBuffer buffer=ByteBuffer.allocateDirect(elementCount2*elementSize2*Byte.BYTES).order(ByteOrder.nativeOrder());
		viewBuffer=buffer;
		return buffer;
	}
}