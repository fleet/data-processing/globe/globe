package fr.ifremer.globe.dev.soundingviewer.gl2x;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import com.jogamp.common.nio.Buffers;

/**
 * Class representing VBO object
 * 
 * @author bvalliere
 */
public class FloatVBO extends TestVBOBase {

	/** Buffer */
	private FloatBuffer floatbuffer;

	public FloatVBO(int elementSize) {
		super(elementSize);
	}


	/**
	 * Add a value to buffer
	 * 
	 * @param x
	 */
	public void put(double x) {
		floatbuffer.put((float) x);
	}

	/**
	 * Add a values to buffer
	 * 
	 * @param values
	 */
	public void put(float[] values) {
		floatbuffer.put(values);
	}



	@Override
	int getGLElementSize() {
		return Buffers.SIZEOF_FLOAT;
	}

	@Override
	protected ByteBuffer createBuffer(int elementCount2, int elementSize2) {
		ByteBuffer buffer=ByteBuffer.allocateDirect(elementCount2*elementSize2*Float.BYTES).order(ByteOrder.nativeOrder());
		floatbuffer=buffer.asFloatBuffer();
		return buffer;
	}
}