package fr.ifremer.globe.dev.soundingviewer.gl2x;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import com.jogamp.common.nio.Buffers;

/**
 * Class representing VBO object
 * 
 * @author bvalliere
 */
public class IntVBO extends TestVBOBase {

	/** Buffer */
	private IntBuffer intBuffer;

	public IntVBO(int elementSize) {
		super(elementSize);
	}


	/**
	 * Add a value to buffer
	 * 
	 * @param x
	 */
	public void put(int x) {
		intBuffer.put(x);
	}

	/**
	 * Add a values to buffer
	 * 
	 * @param values
	 */
	public void put(int[] values) {
		intBuffer.put(values);
	}



	@Override
	int getGLElementSize() {
		return Buffers.SIZEOF_INT;
	}

	@Override
	protected ByteBuffer createBuffer(int elementCount2, int elementSize2) {
		ByteBuffer buffer=ByteBuffer.allocateDirect(elementCount2*elementSize2*Integer.BYTES).order(ByteOrder.nativeOrder());
		intBuffer=buffer.asIntBuffer();
		return buffer;
	}
}