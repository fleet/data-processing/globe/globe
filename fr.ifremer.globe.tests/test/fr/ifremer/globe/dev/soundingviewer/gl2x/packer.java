package fr.ifremer.globe.dev.soundingviewer.gl2x;

import org.junit.Assert;
import org.junit.Test;

public class packer {
	@Test
	public void run()
	{
		Values v = new Values();
		check(v);
		v.valid=1;
		v.detection=0;
		v.beam=1;
		check(v);
		check(v);
		check(v);
	}

	private void check(Values v)
	{
		Values ref = v.clone();
		float packed=pack(v);
		unpack(packed, v);
		Assert.assertTrue(v.equals(ref));
	
	}
	class Values{
		byte valid = 0;
		byte detection = 0;
		short beam=255;
		
		@Override
		public Values clone()
		{
			Values v=new Values();
			v.valid=valid;
			v.detection=detection;
			v.beam=beam;
			return v;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Values other = (Values) obj;
			if(other.valid!=valid)
				return false;
			if(other.detection!=detection)
				return false;
			if(other.beam!=beam )
				return false;
			return true;
		}
	}

	public static float pack(Values v)
	{
		float value= ((int)v.valid) << 24 | ((int)v.detection)<<16 | ((int) v.beam); 

		return value;

	}
	public static void unpack(float values, Values out)
	{

	}
}
