package fr.ifremer.globe.dev.soundingviewer.gl2x;

import java.nio.ByteBuffer;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

/**
 * Class representing VBO object
 * 
 * @author bvalliere
 */
public abstract class TestVBOBase {

	/** Name of the Open GL buffer */
	private int[] bufferName;

	/** Number of elements to render */
	private int elementCount = 0;

	/** Buffer */
	private ByteBuffer buffer;

	/** Size of elements */
	private int elementSize;

	abstract int getGLElementSize();
	
	public TestVBOBase(int elementSize) {
		this.elementSize = elementSize;
	}

	public void allocateBuffer(int elementCount) {
		if (buffer != null) {
			buffer.clear();
		}
		buffer = createBuffer(elementCount , elementSize);
	}

	protected abstract ByteBuffer createBuffer(int elementCount2, int elementSize2);


	/**
	 * Generate and bind the buffer
	 * 
	 * @param gl
	 * @param vbo
	 */
	public void bindAndLoad(GL gl1) {
		GL2 gl = gl1.getGL2();
		elementCount = buffer.capacity() / elementSize;
		buffer.rewind();

		if (bufferName == null) {
			bufferName = new int[1];
		} else {
			gl.glDeleteBuffers(1, bufferName, 0);
		}

		// Get a valid name
		gl.glGenBuffers(1, bufferName, 0);
		// Bind the buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, bufferName[0]);
		// Load the data
		gl.glBufferData(GL.GL_ARRAY_BUFFER, buffer.capacity() , buffer, GL.GL_STATIC_DRAW);
	}

	public void position(int position) {
		buffer.position(position);
	}
	

	public int getElementCount() {
		return elementCount;
	}

	public int getElementSize() {
		return elementSize;
	}

	public int[] getBufferName() {
		return bufferName;
	}

	public void clear() {
		buffer.clear();
	}

	
}