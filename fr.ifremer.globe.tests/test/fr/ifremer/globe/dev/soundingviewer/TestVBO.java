package fr.ifremer.globe.dev.soundingviewer;

import java.nio.FloatBuffer;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;

import com.jogamp.common.nio.Buffers;

/**
 * Class representing VBO object
 * 
 * @author bvalliere
 */
public class TestVBO {

	/** Name of the Open GL buffer */
	private int[] bufferName;

	/** Number of elements to render */
	private int elementCount = 0;

	/** Buffer */
	private FloatBuffer buffer;

	/** Size of elements */
	private int elementSize;

	public TestVBO(int elementSize) {
		this.elementSize = elementSize;
	}

	public void allocateBuffer(int elementCount) {
		if (buffer != null) {
			buffer.clear();
		}
		buffer = Buffers.newDirectFloatBuffer(elementCount * elementSize);
	}

	/**
	 * Add a value to buffer
	 * 
	 * @param x
	 */
	public void put(double x) {
		buffer.put((float) x);
	}

	/**
	 * Add a values to buffer
	 * 
	 * @param values
	 */
	public void put(float[] values) {
		buffer.put(values);
	}

	/**
	 * Generate and bind the buffer
	 * 
	 * @param gl
	 * @param vbo
	 */
	public void bindAndLoad(GL gl1) {
		GL2 gl = gl1.getGL2();
		elementCount = buffer.capacity() / elementSize;
		buffer.rewind();

		if (bufferName == null) {
			bufferName = new int[1];
		} else {
			gl.glDeleteBuffers(1, bufferName, 0);
		}

		// Get a valid name
		gl.glGenBuffers(1, bufferName, 0);
		// Bind the buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, bufferName[0]);
		// Load the data
		gl.glBufferData(GL.GL_ARRAY_BUFFER, buffer.capacity() * Buffers.SIZEOF_FLOAT, buffer, GL.GL_STATIC_DRAW);
	}

	public void drawVertexArray(GL gl1, int glType, int firstIndex, int lastIndex) {

		GL2 gl = gl1.getGL2();
		// Enable vertex arrays
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);

		// Set the vertex pointer to the vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, bufferName[0]);
		gl.glVertexPointer(elementSize, GL.GL_FLOAT, 0, 0);

		// Rendering
		gl.glDrawArrays(glType, firstIndex, lastIndex);

		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
	}
	
	public void position(int position) {
		buffer.position(position);
	}
	
	public void drawVertexArrayWithColorArray(GL gl1, int glType,TestVBO color) {

		GL2 gl = gl1.getGL2();
		// Enable vertex arrays
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GLPointerFunc.GL_COLOR_ARRAY);

		// Set the vertex pointer to the vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, bufferName[0]);
		gl.glVertexPointer(elementSize, GL.GL_FLOAT, 0, 0);
		//Set color pointer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, color.bufferName[0]);
		gl.glColorPointer(elementSize, GL.GL_FLOAT, 0, 0);

		// Rendering
		gl.glDrawArrays(glType, 0, elementCount);
		
		//unbind
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		// Disable vertex arrays
		gl.glDisableClientState(GLPointerFunc.GL_COLOR_ARRAY);
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
	}

	

	public int getElementCount() {
		return elementCount;
	}

	public int getElementSize() {
		return elementSize;
	}

	public int[] getBufferName() {
		return bufferName;
	}

	public void clear() {
		buffer.clear();
	}

	public float get() {
		return buffer.get();
	}
}