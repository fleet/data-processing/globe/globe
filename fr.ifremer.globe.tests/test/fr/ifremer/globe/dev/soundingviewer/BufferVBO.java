package fr.ifremer.globe.dev.soundingviewer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;



public class BufferVBO {
	BufferVBO()
	{
		createData();
	}
	private void createData()
	{
		buffer.clear();
		Data data=new Data();
		data.setX(0.8f);
		data.setY(0.2f);
		data.setZ(0.f);
		data.setReflectivity(0.5f);
		data.setValidity((byte) 1);
		data.write(buffer);
		
		data.setX(0.2f);
		data.setY(0.2f);
		data.setZ(0.f);
		data.setReflectivity(0.5f);
		data.setValidity((byte) 1);

		data.write(buffer);
		
		data.setX(0.5f);
		data.setY(0.8f);
		data.setZ(0.f);
		data.setValidity((byte) 0);

		data.setReflectivity(0.4f);

		data.write(buffer);

		data.setX(0.4f);
		data.setY(0.4f);
		data.setZ(0.f);
		data.setReflectivity(1);
		data.setValidity((byte) 0);

		data.write(buffer);

		buffer.rewind();
	}
	int pointCount=4;
	ByteBuffer buffer=ByteBuffer.allocateDirect(Data.BYTES*pointCount).order(ByteOrder.nativeOrder());
	
	protected int pointSetA;
	// Vertex Attribute Locations
	int vertexLoc=0;
	int reflectivityLoc=1;
	int validityLoc=2;
	int detectionLoc=3;
	int cycleLoc=4;
	int beamLoc=5;

	protected int generateVAOId(GL3 gl) {
		// allocate an array of one element in order to strore 
		// the generated id
		int[] idArray = new int[1];
		// let's generate
		gl.glGenVertexArrays(1, idArray, 0);
		// return the id
		return idArray[0];
	}
	protected int generateBufferId(GL3 gl) {
		// allocate an array of one element in order to strore 
		// the generated id
		int[] idArray = new int[1];
		// let's generate
		gl.glGenBuffers( 1, idArray, 0);

		// return the id
		return idArray[0];
	}
	public void bindAttribuLocation(GL3 gl, int programmID)
	{
		gl.glBindAttribLocation( programmID, vertexLoc,"position");
		gl.glBindAttribLocation( programmID,reflectivityLoc, "reflectivity");
		gl.glBindAttribLocation( programmID,validityLoc, "validity");
		gl.glBindAttribLocation( programmID,detectionLoc, "detection");
		gl.glBindAttribLocation( programmID,cycleLoc, "cycle");
		gl.glBindAttribLocation( programmID,beamLoc, "beam");

	}
	void setupBuffers(GL3 gl) {

		
		// generate the IDs
		this.pointSetA = this.generateVAOId(gl);

		// create the buffer and link the data with the location inside the vertex shader

		// bind the correct VAO id
		gl.glBindVertexArray( pointSetA);
		// Generate two slots for the vertex and color buffers
		int vertexBufferId = this.generateBufferId(gl);

		// bind the two buffer
		this.bindBuffer(gl, vertexBufferId);
		
		gl.glVertexAttribPointer(vertexLoc,3, GL.GL_FLOAT, false,Data.BYTES, 0);
		gl.glEnableVertexAttribArray(vertexLoc);

		gl.glVertexAttribPointer(reflectivityLoc,1, GL.GL_FLOAT, false, Data.BYTES, Float.BYTES*3);
		gl.glEnableVertexAttribArray(reflectivityLoc);

		gl.glVertexAttribPointer(validityLoc,1, GL.GL_BYTE, false, Data.BYTES, Float.BYTES*3+Float.BYTES);
		gl.glEnableVertexAttribArray(validityLoc);


		
		gl.glBindVertexArray(0);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
	}
	void bindBuffer(GL3 gl, int bufferId){
		// bind buffer for vertices and copy data into buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, bufferId);
		
		buffer.rewind();
		gl.glBufferData(GL.GL_ARRAY_BUFFER, buffer.capacity(),buffer, GL.GL_STATIC_DRAW);

	}

}
