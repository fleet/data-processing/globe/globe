/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.dev.gdal;

import java.io.File;

import org.junit.Test;

import fr.ifremer.globe.gdal.GdalUtils;

/**
 * Generates a random GRD file
 */
public class GdalGenerateGrd {

	/** File to test. */
	public static final String FOLDER = "d:/tmp";

	@Test
	public void generate() throws Exception {
		// Expected depths
		double minimum = 0d;
		double maximum = 8000d;

		// Raster size
		int xSize = 1600;
		int ySize = 1300;

		double xDelta = (maximum - minimum) / xSize;
		double yDelta = (maximum - minimum) / ySize;

		int direction = 5;

		int stepCount = 10;
		double stepSize = (int) Math.floor((maximum - minimum) / stepCount);
		double[][] values = new double[xSize][ySize];
		for (int x = 0; x < xSize; x++) {
			for (int y = 0; y < ySize; y++) {
				switch (direction) {
				case 0: // Random
					int step = (int) (Math.random() * stepCount);
					values[x][y] = Math.min((step * stepSize) + minimum, maximum);
					break;
				case 1: // from left to right
					values[x][y] = Math.min((x * xDelta) + minimum, maximum);
					break;
				case 2: // from right to left
					values[x][y] = Math.min((-x * xDelta) + maximum, maximum);
					break;
				case 3: // from left to right
					values[x][y] = Math.min((y * yDelta) + minimum, maximum);
					break;
				case 4: // from right to left
					values[x][y] = Math.min((-y * yDelta) + maximum, maximum);
					break;
				case 5: // Constant
					values[x][y] = 2000d;
					break;
				}
			}
		}

		// India
		double latNorth = 36;
		double lonWest = 63;
		double latSouth = 4;
		double lonEast = 93;

		GdalUtils.generateFile(values, Double.MIN_VALUE,
				new double[] { lonWest, (lonEast - lonWest) / xSize, 0d, latNorth, 0d, (latSouth - latNorth) / ySize },
				"GMT", new File(FOLDER, "India.grd"));
	}
}
