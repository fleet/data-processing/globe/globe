/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.dev.gdal;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconst;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.Driver;
import org.gdal.ogr.Feature;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Extract polygons for all connected regions of pixels in the raster sharing a
 * common pixel value (here, alpha channel)
 */
public class GdalPolygonize {

	/** File to test. */
	public static final String TILE_FILE = GlobeTestUtil.getTestDataPath() + "/file/MultiTiff/Tile_100_200.png";

	@Test
	public void openAndReadGmt5() throws Exception {

		// Open the raster
		Dataset rasterDataset = gdal.Open(TILE_FILE);

		// Extract the alpha band
		Band rasterBand = null;
		for (int bandIndex = 1; bandIndex <= rasterDataset.getRasterCount(); bandIndex++) {
			rasterBand = rasterDataset.GetRasterBand(bandIndex);
			if (rasterBand != null && rasterBand.GetColorInterpretation() == gdalconst.GCI_AlphaBand) {
				break;
			}
			rasterBand = null;
		}
		Assert.assertNotNull(rasterBand);

		// Create a Shape Dataset in memory
		Driver driver = ogr.GetDriverByName("Memory");
		DataSource memoryDataSource = driver.CreateDataSource("Memory");
		Layer outputLayer = memoryDataSource.CreateLayer("layer");

		// Ask Gdal to extract polygons
		gdal.Polygonize(rasterBand, null, outputLayer, -1);
		Feature feature = outputLayer.GetNextFeature();
		Assert.assertNotNull(feature);
		Geometry polygon = feature.GetGeometryRef();
		Assert.assertNotNull(polygon);

		// Ask Geos to simplify the polygon
		polygon = polygon.Simplify(100.);
		Assert.assertNotNull(polygon);

		// View the result with http://mapshaper.org/
		System.err.println(polygon.ExportToJson());
		// Close the dataset
		memoryDataSource.delete();
		rasterDataset.delete();
	}

}
