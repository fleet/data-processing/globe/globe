/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.dev.gdal;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.Driver;
import org.gdal.gdal.gdal;
import org.junit.Test;

import fr.ifremer.globe.utils.test.GlobeTestUtil;
import gov.nasa.worldwind.util.gdal.GDALUtils;

/**
 * WARNING : Need lastest version of GDAL.<br>
 * <br>
 * A class for the GDAL use demonstration For the use of GDAL library, configure
 * the run configurations : <br>
 * - Arguments : -Djava.library.path=<gdal path><br>
 * - Environment variables : <br>
 * PATH = <gdal path>;<gdal path>/projlib;<gdal path>/csharp;<gdal path>/curl <br>
 * GDAL_DATA = <gdal path>/gdal-data<br>
 * GDAL_DRIVER_PATH = <gdal path>/gdalplugins/<br>
 *
 */
public class GdalGMT5 {

	/** File to test. */
	public static final String GMT5_FILE = GlobeTestUtil.getTestDataPath() + "/file/gmt/Contourites-Top-Surface.grd-Cf";

	/**
	 * Open the file and return the dataset
	 */
	public Dataset openGMT5File(String path) throws Exception {
			return GDALUtils.open(path);
	}

	/**
	 * Print file information : - X, Y size, - Projection reference
	 * 
	 * @param band
	 */
	public void printFileInformations(Dataset dataset) {
		// access the dataset raster band/channel
		Band band = dataset.GetRasterBand(1);
		System.err.println("--- File informations :");
		System.err.println("Used driver = " + dataset.GetDriver().getShortName() + " " + dataset.GetDriver().getLongName());
		System.err.println("X size = " + band.getXSize());
		System.err.println("Y size = " + band.getYSize());
		System.err.println("Projection reference = " + dataset.GetProjectionRef());
		System.err.print("GeoTransform = ");
		double[] geoArray = dataset.GetGeoTransform();
		for (double d : geoArray) {
			System.err.print("   " + d);
		}
		System.err.println("");
	}

	/**
	 * Print GDAL informations : - GDAL version, - Available Drivers,
	 */
	public void printGdalInformations() {
		System.err.println("--- GDAL informations :");
		System.err.println("GDAL version : " + gdal.VersionInfo());

		// Get the drivers
		int count = gdal.GetDriverCount();
		System.err.println(count + " available Drivers :");
		for (int i = 0; i < count; i++) {
			try {
				Driver driver = gdal.GetDriver(i);
				System.err.println(" " + driver.getShortName() + " : " + driver.getLongName());
			} catch (Exception e) {
				System.err.println("Error loading driver " + i);
			}
		}
		System.err.println("---");
	}


	/**
	 * Compute X(x, y) x = 0
	 */
	public float getX(Dataset dataset, int x, int y) {
		// access the dataset raster band/channel
		Band band = dataset.GetRasterBand(1);

		// Get geotransform array (matrix to compute X and Y values)
		double[] geoArray = dataset.GetGeoTransform();

		// Compute X value
		// X = (geoArray[0] + y * geoArray[1] + (XSize - x) * geoArray[2])
		float xvalue = (float) (geoArray[0] + y * geoArray[1] + (band.getXSize() - x) * geoArray[2]);

		// Round X value to 0.01
		float xvalueRounded = xvalue * 100;
		xvalueRounded = Math.round(xvalueRounded);
		xvalueRounded = xvalueRounded / 100;

		System.err.println("X(0, " + y + ") => geoArray[0] + " + y + "*geoArray[1] + (" + band.getXSize() + " - " + x + " ) *geoArray[2] = " + xvalueRounded);
		return xvalueRounded;
	}

	/**
	 * Compute Y(x, y) x = 0
	 */
	public float getY(Dataset dataset, int x, int y) {
		// access the dataset raster band/channel
		Band band = dataset.GetRasterBand(1);

		// Get geotransform array (matrix to compute X and Y values)
		double[] geoArray = dataset.GetGeoTransform();

		// Compute Y value
		// Y = (geoArray[3] + x * geoArray[4] + (YSize - y) * geoArray[5])
		float yvalue = (float) (geoArray[3] + x * geoArray[4] + (band.getYSize() - y) * geoArray[5]);

		// Round Y value to 0.01
		float yvalueRounded = yvalue * 100;
		yvalueRounded = Math.round(yvalueRounded);
		yvalueRounded = yvalueRounded / 100;

		System.err.println("Y(" + x + ", " + y + ") => geoArray[3] + " + y + "*geoArray[4] + (" + band.getYSize() + " - " + x + ") *geoArray[5] = " + yvalueRounded);
		return yvalueRounded;
	}

	/**
	 * Get Z value with Z coordinates (x,y) in the array
	 */
	public float getZ(Dataset dataset, int x, int y) {
		// access the dataset raster band/channel
		Band band = dataset.GetRasterBand(1);

		// Get the array of z values
		float[] arr = new float[band.getXSize() * band.getYSize()];
		band.ReadRaster(0, 0, band.getXSize(), band.getYSize(), arr);

		// Compute the position i in the array
		// i = (XSize * (YSize - y))- (XSize - x)
		int i = (band.getXSize() * (band.getYSize() - y)) - (band.getXSize() - x);

		System.err.println("Z(" + x + ", " + y + ") => arr[" + i + "] = " + arr[i]);
		return arr[i];
	}

	/**
	 * Compute the average of Z datas
	 */
	public float computeZAverage(Dataset dataset) {
		// access the dataset raster band/channel
		Band band = dataset.GetRasterBand(1);
		// Get the array of z values
		float[] arr = new float[band.getXSize() * band.getYSize()];
		band.ReadRaster(0, 0, band.getXSize(), band.getYSize(), arr);

		// Calculation of the average
		int cellsNbr = band.getXSize() * band.getYSize();
		float sum = 0;
		int numberCellsNbr = 0;
		for (int i = 0; i < cellsNbr; i++) {
			// if the value of the array not NaN
			if (arr[i] == arr[i]) {
				sum += arr[i];
				numberCellsNbr++;
			}
		}

		System.err.println("Average = " + sum / numberCellsNbr);
		return sum / numberCellsNbr;
	}

	/**
	 * Read a GMT 5 file with GDAL.
	 */
	@Test
	public void openAndReadGmt5() throws Exception {

		printGdalInformations();

		Dataset dataset = GDALUtils.open(GMT5_FILE);
		System.err.println("File can be parsed with " + dataset.GetDriver().getShortName());

		printFileInformations(dataset);
		getX(dataset, 0, 157);
		getY(dataset, 0, 157);
		getZ(dataset, 2216, 24);
		computeZAverage(dataset);

		// Close the dataset
		dataset.delete();
	}
}
