package fr.ifremer.globe.dev.smooth;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartPanel;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import fr.ifremer.globe.core.utils.undo.UndoAction;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Smooth data
 * @author ilepenne
 */

public class Smooth extends UndoAction {

	public static class Struct {

		public static float s_x;
		public static float s_xm1;
		public static float s_xm2;
		public static float s_y;
		public static float s_ym1;
		public static float s_ym2;
	}
	
	
	File fileToTranscode;
	//TechsasDriver driver;
	
	private ChartPanel panel;
	//private List<DateTimeDataRaster> rasters;

	private float FILTER_WEAK_FREQUENCY      =  0.05f;     // Fr�quence de coupure analogique, pour le filtre faible
	private float FILTER_MIDDLE_FREQUENCY    = 0.02f;      // Fr�quence de coupure analogique, pour le filtre moyen
	private float FILTER_STRONG_FREQUENCY    = 0.01f;      // Fr�quence de coupure analogique, pour le filtre fort
	private int DELTA_FACTOR = 3;               		   // facteur de multiplication du temps moyen entre 2 points
	private float COEFF_OVERVOLTAGE = 0.8f;         	   // Coefficient de surtension

	private int delta = 10000000;      // Ecart entre deux points

	//----------------------------------------------------------------------------
	// DONNEES LOCALES
	//----------------------------------------------------------------------------
	private Float                factorA = 0.0f;         	  // Coefficient �quation butterworth d'ordre 2
	private Float                factorB = 0.0f;              // Coefficient �quation butterworth d'ordre 2
	private Float                factorC = 0.0f;              // Coefficient �quation butterworth d'ordre 2
	private Float                factorD = 0.0f;              // Coefficient �quation butterworth d'ordre 2
	private Float                factorE = 0.0f;              // Coefficient �quation butterworth d'ordre 2

	//R�f�rences
	float [] allDataArray = null; //treated data
	int    countAddPoint = 0;     // Indice des points rajout�s

	
	
//	private void getFile(final ESelectionService selectionService, final DataDriverRegistry driverRegistry) {
//		/*if (driverRegistry != null) {
//			Object obj = selectionService.getSelection();
//			if (obj instanceof StructuredSelection) {
//				Object[] elements = ((StructuredSelection) obj).toArray();
//				for (Object e : elements) {
//					fileToTranscode = new File(((InfoStoreNode) e).getInfoStore().getFilename());
//					//driver = driverRegistry.selectDataDriver(fileToTranscode);
//				}
//			}
//		}*/
//	}
//	
	public Smooth(final ChartPanel panel, /*final List<DateTimeDataRaster> selectedRasters, */String frequenceMode, String selectedFrequence, String moduloValue, int selectedData) throws GIOException
	{
		
		//Modified
		//IInfoStore info = driver.initData(fileToTranscode.getAbsolutePath());
		//ITechsasFile file = driver.getTechsasFile(info);
		
	/*	Iterator <NamedDataStore> it = file.getNamedDataStoreList().iterator();
		int size1 = file.getNamedDataStoreList().size();*
		List <Object> objectList = new ArrayList<Object>();
		String [] items = new String [size1];
		
		int x = 0;
		NamedDataStore namedData;

		while (it.hasNext())
		{	
			Object o = it.next();
			namedData = (NamedDataStore)o;
			System.out.println(namedData.getName());
			items[x] = namedData.getName();
			x++;
		}
		
		SmoothDialog dialog = new SmoothDialog(Display.getDefault().getActiveShell(), items, null);

		int returnCode = dialog.open();

		if (returnCode == Window.OK) {
			
		}
		
		//End of modification
		
		
		this.panel = panel;
		this.rasters = selectedRasters;*/

		float frequency = 0.0f;    	  // frequence de coupure
		int count = 0;                // compteur de points

		double   dateFirst; // Date du premier point
		double   dateEnd;   // Date du dernier point

		int    countIdx = 0;          // Nombre de points � rajouter
		int    counterIdx = 0;        // Indice des points a rajouter
		int    addCount = 0;          // Nombre de points � rajouter
		int    idx = 0;               // Indice
		double step = 0.0;            // pas de la valeur des points � rajouter
		double currentValue = 0.0;    // Valeur du point courant
		int    nbPoint = 0;           // Nombre de points

		float  offset = 0.0f;       // decalage
		float  drift = 0.0f;        // derive

		int    l_idx = 0;             // Indice

		int l_count = 0;
		int l_computedCountIdx = 0; 	//Associer aux flags plus tard

		int computedCountIdx = 0;	// Compteur de points de la courbe calcul�e

		List<Integer> l_computedIdx = new ArrayList<Integer>(); // Liste des indices des points r�els 
		List<Integer> idxList = new ArrayList<Integer>();       // Liste des points d'interuption
		List<Integer> countIdxList = new ArrayList<Integer>();  // Liste du nombre de points � rajouter a chaque indice 

		if (frequenceMode.equals("Faible"))
		{
			frequency = FILTER_WEAK_FREQUENCY;
		}
		else if (frequenceMode.equals("Moyen"))
		{
			frequency = FILTER_MIDDLE_FREQUENCY;
		}
		else if (frequenceMode.equals("Fort"))
		{
			frequency = FILTER_STRONG_FREQUENCY; 
		}
		else if (frequenceMode.equals("Autre"))
		{
			frequency = Float.parseFloat(selectedFrequence);
		}
		//FILTRAGE + LISSAGE
		count = 0;

		final TimeSeriesCollection selectedDataset = (TimeSeriesCollection) panel.getChart().getXYPlot().getDataset(selectedData);

		float [] computedDataArray = null;
		float [] allData = null;
		float [] ql_rawData = null;
		long [] dateArray = null;
		int size =0;

		float modValue = 0;

		for (int i = 0; i < selectedDataset.getSeriesCount(); i++) {
			TimeSeries serie = selectedDataset.getSeries(i);
			computedDataArray = new float [serie.getItemCount()];
			allData = new float [serie.getItemCount()];
			dateArray = new long [serie.getItemCount()];
			size = serie.getItemCount();
			for (int j=0 ; j < serie.getItemCount() ; j++)
			{
				allData[j] =  serie.getValue(j).floatValue();
				dateArray[j] = serie.getTimePeriod(j).getFirstMillisecond();
				//l_computedIdx.add(j);
			}
		}

		//System.out.println(moduloValue);
		//System.out.println(Float.parseFloat(moduloValue));

		//************************************************************
		// Si un modulo est choisi, linearisation de la donnee
		//************************************************************
		if (moduloValue != "")
		{
			modValue = Float.parseFloat(moduloValue);
			//**********************************************************
			// Allocation du tableau des donnees brutes
			// Initialisation du tableau de donnees
			//**********************************************************
			ql_rawData = new float[size]; //l_validCount

			//**********************************************************
			// Initialisation du tableau de donn�es brutes : l'algo n'est 
			// pas appliqu� sur les valeurs  invalides ou manquantes
			//**********************************************************
			l_computedCountIdx = 0;
			for (l_count = 0 ; l_count < size ; l_count ++) //Notions de flags a introduire plus tard
			{
				//if ( (*qp_points)[l_count]->GetFlag() >= CIB_MVI_k_VALIDITY_CHECK 
				//		&& (*qp_points)[l_count]->GetDateFlag() >= CIB_MVI_k_VALIDITY_CHECK)
				//{
				ql_rawData[l_computedCountIdx] =  allData[l_count];
				l_computedIdx.add(l_count);
				l_computedCountIdx += 1;
				//}
			}

			//***********************************************************
			// Calcul de la derive et de la courbe linearisee
			//***********************************************************
			computedDataArray[0] = ql_rawData[0];
			int l_validCount;//associer aux flags
			for (l_count = 1; l_count < size; l_count++)
			{
				//************************************************************
				// Calcul de la derive
				//************************************************************
				drift = ql_rawData[l_count] - computedDataArray[l_count-1];

				//************************************************************
				// Calcul de l'offset
				//************************************************************
				if (drift > Math.abs(modValue/2.0))
					offset = -Math.abs(modValue);
				else if (drift < - Math.abs(modValue/2.0))
					offset += Math.abs(modValue);
				else
					offset = 0.0f;

				//************************************************************
				// Calcul de la nouvelle valeur
				//************************************************************
				computedDataArray[l_count] = ql_rawData[l_count] + offset;
			}
		}

		//**********************************************************
		// Sinon on initialise un tableau de donnees pour le calcul
		//**********************************************************
		else
		{
			for (int i = 0; i < selectedDataset.getSeriesCount(); i++) {
				TimeSeries serie = selectedDataset.getSeries(i);
				computedDataArray = new float [serie.getItemCount()];
				for (int j=0 ; j < serie.getItemCount() ; j++)
				{
					computedDataArray[j] = serie.getValue(j).floatValue();
					l_computedIdx.add(j);
				}
			}
		}

		//*********************************************************
		// Definition du seuil maximum entre 2 points
		// Intervalle de temps minimum entre 2 pts
		// sur les 100 premiers points valides		 
		//*********************************************************
		for (int i = 0; i < selectedDataset.getSeriesCount(); i++) {
			TimeSeries serie = selectedDataset.getSeries(i);

			while ( (count < serie.getItemCount() - 1) && (count < 100) )
			{
				dateFirst = serie.getTimePeriod(count).getFirstMillisecond();
				dateEnd = serie.getTimePeriod(count+1).getFirstMillisecond();
				computedDataArray[i] =  serie.getValue(i).floatValue();

				if ( (dateEnd > dateFirst) && (delta > (dateEnd - dateFirst)) )
					delta = (int) (dateEnd - dateFirst);
				count++;
			}

			if (delta == 0)
				delta = 10000;

			//**********************************************************
			// Recuperation de la liste des points d'interruption
			//**********************************************************
			for (count = 0 ; count < serie.getItemCount() ; count ++)
			{
				if (count < serie.getItemCount() - 1)
				{
					dateFirst = serie.getTimePeriod(count).getFirstMillisecond();
					dateEnd = serie.getTimePeriod(count+1).getFirstMillisecond();

					//**********************************************************
					// Si le temps de s�paration de deux points est sup�rieur
					// a facteur * delta, c'est un interruption
					//**********************************************************

					System.out.println(dateEnd - dateFirst);
					if ( (dateEnd > dateFirst) && ((dateEnd - dateFirst) > (DELTA_FACTOR*delta)) )
					{
						idxList.add(count); 
						nbPoint = (int) (((dateEnd - dateFirst) / (delta)) - 1);
						countIdxList.add(nbPoint);
						addCount += nbPoint;
					}
				}
			}
			//INTERPOLATION
			computedCountIdx = 0;

			allDataArray = new float[computedDataArray.length + addCount];

			for (count = 0 ; count < selectedDataset.getSeries(i).getItemCount() ; count ++)
			{   
				//************************************************************
				// Point ajout�s par l'interpolation
				//************************************************************
				if (idxList.contains(count))
				{
					//l_computedIdx.append(l_countAddPoint);
					idx = idxList.indexOf(count);
					countIdx = countIdxList.get(idx);
					step = ((computedDataArray[computedCountIdx+1] - computedDataArray[computedCountIdx]) / countIdx); //Pas delta

					currentValue = computedDataArray[computedCountIdx]; 

					//************************************************************
					// Points ajout�s
					//************************************************************

					for (counterIdx = 0; counterIdx <= countIdx ; counterIdx++)
					{
						allDataArray[countAddPoint] = (float)(currentValue + (step * counterIdx));
						countAddPoint +=1;
					}
					computedCountIdx += 1;
				}
				else
				{
					//***********************************************************
					// Point r��l 
					//***********************************************************
					allDataArray[countAddPoint] = computedDataArray[computedCountIdx];
					l_computedIdx.add(countAddPoint);
					countAddPoint += 1;
					computedCountIdx += 1;
				}
			}
		}

		//************************************************************
		// Filtrage
		//************************************************************

		smooth(frequency, COEFF_OVERVOLTAGE, countAddPoint, allDataArray);

		//Update the serie 

		int counter = 0;
		boolean notFound = false; //Boolean
		long time;

		//**********************************************************
		// Si le modulo est choisi, application du modulo
		//**********************************************************
		if (moduloValue != "")
		{
			
			l_computedCountIdx = 0;
			for ( l_count = 0; l_count < size; l_count++)
			{
				//ql_point = (*qp_points)[l_count];

				//*****************************************************
				// Le calcul n'est pas appliqu� sur les valeurs 
				// invalides ou manquantes
				//****************************************************
				//if ( ql_point->GetFlag() >= CIB_MVI_k_VALIDITY_CHECK		// A associer aux flags plus tard 
				//		&& ql_point->GetDateFlag() >= CIB_MVI_k_VALIDITY_CHECK)
				//{
				//**********************************************************
				// R�cup�ration de l'indice des points r�els
				//**********************************************************
				l_idx = l_computedIdx.get(l_computedCountIdx);

				if (allDataArray[l_idx] < 0.0f)
					allDataArray[l_idx] += Math.abs(modValue);
				else
					allDataArray[l_idx]= ((allDataArray[l_idx]) % Math.abs(modValue));
				//ql_point->SetValue(allData[l_idx]);
				//computedDataArray[l_count]= allDataArray[l_idx];
				l_computedCountIdx += 1;
				//}
			}
		}
		//**********************************************************
		// Sinon, mise a jour de la donnee en parametre
		//**********************************************************
		else
		{
			//TODO --PROBLEM in the algo
		}

		for (int i = 0; i < selectedDataset.getSeriesCount(); i++) {
			TimeSeries serie = selectedDataset.getSeries(i);
			for (int a = 0 ; a < computedDataArray.length; a++)
			{
				for (int b = 0 ; b < idxList.size(); b++)
				{
					notFound = true;
					if (idxList.get(b).equals(a))  //if index in list time = time + delta for nbPoint (added Points)
					{
						notFound = false;  
						int nbPointCounter = 0;

						time = serie.getTimePeriod(counter).getFirstMillisecond();

						while (nbPointCounter <= countIdxList.get(b))
						{
							serie.addOrUpdate(new FixedMillisecond(time), allDataArray[counter]);

							time += delta;  //Add delta to currentTime to compute next Time
							nbPointCounter++;
							counter ++;
						}
						break;
					}
				}
				if (notFound) //Update existing time with new data
				{
					serie.update(counter , allDataArray[counter]);
					counter ++;
				}
			}
		}
	}

	//***************************************************************************
	//
	// SPG : Smooth
	// ROL : Appel de ComputeButherCoef puis de FilterTab
	//	       Simplification des appels dans les programmes utilisateurs
	//
	//**************************************************************************
	
	/**
	 * @param frequency
	 * 			frequence de coupure analogique
	 * @param epsilon	
	 * 			Q factor
	 * @param sampleNb
	 * 			number of samples
	 * @param ql_allData
	 */
	private void smooth(float     frequency,         // I : fr�quence de coupure analogique
			float         epsilon,      // I : coefficient de surtension 
			long  sampleNb,     // I: Nombre d'�chantillons
			float[]  ql_allData
			)
	{

		//----------------------------------------------------------------------------
		// TRAITEMENTS
		//----------------------------------------------------------------------------

		//************************************************************
		// Calcul des coefficients d'un filtre recursif BUTTERWORTH
		// d'ordre 2
		//************************************************************
		computeButherCoef(frequency, epsilon);

		//************************************************************
		// Filtrage de la donn�e valide
		//************************************************************
		filterTab( sampleNb,
				ql_allData,
				factorA,
				factorB,
				factorC,
				factorD,
				factorE);
	}

	//***************************************************************************
	//
	// SPG : ComputeButherCoef
	// ROL : Calcul des coefficients d'un filtre recursif butterworth d'ordre 2
	//	       equation recurante y(k) = A*x(k)+B*x(k-1)+C*x(k-2)-D*y(k-1)-E*y(k-2)
	//**************************************************************************
	
	/**
	 * Compute Butterworth factor
	 * @param frequency
	 * 			analog cutoff frequency
	 * @param epsilon
	 * 			Q factor
	 */
	protected void computeButherCoef(
			Float frequency,                 // I : fr�quence de coupure analogique
			Float epsilon
			)
	{
		//----------------------------------------------------------------------------
		// DONNEES LOCALES
		//----------------------------------------------------------------------------
		float l_deno = 0.0f;           // zone de calcul
		float l_wAC = 0.0f;            // zone de calcul
		float l_wAC2 = 0.0f;           // zone de calcul

		float pi = 3.141592f;  // valeur de PI 

		//----------------------------------------------------------------------------
		// TRAITEMENTS
		//----------------------------------------------------------------------------

		//************************************************************
		// calcul des coefficients
		//************************************************************
		l_wAC = 2.0f * pi * frequency;
		l_wAC2 = l_wAC * l_wAC;
		l_deno = 4.0f + (4.0f * epsilon * l_wAC) + l_wAC2;

		factorA = l_wAC2 / l_deno;
		factorB = 2.0f * factorA;
		factorC = factorA;
		factorD = 2.0f * (l_wAC2 - 4.0f) / l_deno;
		factorE = (l_wAC2 - 4.0f * epsilon * l_wAC + 4.0f) / l_deno;
	}

	//***************************************************************************
	//
	// SPG : FilterTab
	// ROL : Filtrage r�cursif causalement et anti-causalement d'un tableau
	//	       de valeurs
	//**************************************************************************

	protected void filterTab(
			long  p_sampleNb,     // I: Nombre d'�chantillons
			float [] qp_sampleTab,   // I-O: Tableau d'�chantillons
			float p_factorA,      // I : Coefficient �quation butterworth d'ordre 2
			float p_factorB,      // I : Coefficient �quation butterworth d'ordre 2
			float p_factorC,      // I : Coefficient �quation butterworth d'ordre 2
			float p_factorD,      // I : Coefficient �quation butterworth d'ordre 2
			float p_factorE       // I : Coefficient �quation butterworth d'ordre 2
			)
	{
		//----------------------------------------------------------------------------
		// DONNEES LOCALES
		//----------------------------------------------------------------------------
		//Struct Struct = new Struct();  //Mettre struct + tous les attributs en static
		int 						l_sampleNum;        // Compteur d'echantillon

		//----------------------------------------------------------------------------
		// TRAITEMENTS
		//----------------------------------------------------------------------------
		//************************************************************
		// Initialisations
		//************************************************************
		Struct.s_x= qp_sampleTab[0];

		Struct.s_xm2= Struct.s_x;
		Struct.s_xm1= Struct.s_x;
		Struct.s_ym2= Struct.s_x;
		Struct.s_ym1= Struct.s_x;
		Struct.s_y= Struct.s_x;

		//************************************************************
		// Filtrage causal
		//************************************************************
		for ( l_sampleNum= 0; l_sampleNum < p_sampleNb; l_sampleNum++)
		{
			Struct.s_xm2= Struct.s_xm1;
			Struct.s_xm1= Struct.s_x;
			Struct.s_ym2= Struct.s_ym1;
			Struct.s_ym1= Struct.s_y;

			Struct.s_x= qp_sampleTab[l_sampleNum];

			Struct.s_y= p_factorA*Struct.s_x
					+ p_factorB*Struct.s_xm1
					+ p_factorC*Struct.s_xm2
					- p_factorD*Struct.s_ym1
					- p_factorE*Struct.s_ym2;

			qp_sampleTab[l_sampleNum]= Struct.s_y;
		}

		//************************************************************
		// Initialisations
		//************************************************************
		Struct.s_x= qp_sampleTab[(int) (p_sampleNb-1)];

		Struct.s_xm2= Struct.s_x;
		Struct.s_xm1= Struct.s_x;
		Struct.s_ym2= Struct.s_x;
		Struct.s_ym1= Struct.s_x;
		Struct.s_y= Struct.s_x;

		//************************************************************
		// Filtrage anti-causal
		//************************************************************

		for ( l_sampleNum = (int) (p_sampleNb-1); l_sampleNum >= 0; l_sampleNum--)
		{
			Struct.s_xm2= Struct.s_xm1;
			Struct.s_xm1= Struct.s_x;
			Struct.s_ym2= Struct.s_ym1;
			Struct.s_ym1= Struct.s_y;

			Struct.s_x= qp_sampleTab[l_sampleNum];

			Struct.s_y= p_factorA*Struct.s_x
					+ p_factorB*Struct.s_xm1
					+ p_factorC*Struct.s_xm2
					- p_factorD*Struct.s_ym1
					- p_factorE*Struct.s_ym2;

			qp_sampleTab[l_sampleNum]= Struct.s_y;
		}
	}

	@Override
	public String msgInfo() {
		return "Smooth";
	}

	@Override
	protected void executeUndo() {
		// TODO Auto-generated method stub
	}


	@Override
	protected void executeRedo() {
		// TODO Auto-generated method stub
	}
}