package fr.ifremer.globe.dev.smooth;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;


public class SmoothDialog extends Dialog {

	/**
	 * An input dialog for soliciting an input string from the user for the offset
	 * to apply.
	 * 
	 */

	private String dataComboValue = "";
	private String frequenceValue = "";
	private String moduloValue = "";

	/**
	 * The selected data index.
	 */
	private int selectedDataIndex = -1;

	/**
	 * The input validator, or <code>null</code> if none.
	 */
	private IInputValidator validator;

	/**
	 * Data selection widget.
	 */
	private Combo dataCombo;

	/**
	 * Data combo items.
	 */
	private String[] dataComboItems;

	/**
	 * Error message label widget.
	 */
	private Text errorMessageText;

	/**
	 * Error message string.
	 */
	private String errorMessage;
	private Text moduloText;
	private Text dataComboText;
	private Text frequenceText;
	CCombo combo;

	/**
	 * Creates a custom dialog with OK and Cancel buttons. Note that the dialog
	 * will have no visual representation (no widgets) until it is told to open.
	 * <p>
	 * Note that the <code>open</code> method blocks for input dialogs.
	 * </p>
	 * 
	 * @param parentShell
	 *            the parent shell, or <code>null</code> to create a top-level
	 *            shell
	 * @param items
	 *            items of the data combo box
	 * @param validator
	 *            an input validator, or <code>null</code> if none
	 */
	public SmoothDialog(Shell parentShell, String[] items, IInputValidator validator) {
		super(parentShell);
		this.dataComboItems = items;
		this.validator = validator;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			frequenceValue = frequenceText.getText();
			dataComboValue = combo.getText();
			selectedDataIndex = dataCombo.getSelectionIndex();
			moduloValue = moduloText.getText();
		} else {
			frequenceValue = null;
			selectedDataIndex = -1;
		}
		super.buttonPressed(buttonId);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Smooth parameters");
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(null);

		Label filterDegree = new Label(composite, SWT.NONE);
		filterDegree.setBounds(21, 20, 98, 15);
		filterDegree.setText("Degré de filtrage :");


		final Label lblFrquenceDeCoupure = new Label(composite, SWT.NONE);
		lblFrquenceDeCoupure.setBounds(21, 57, 128, 15);
		lblFrquenceDeCoupure.setText("Fréquence de coupure");

		frequenceText = new Text(composite, SWT.BORDER);
		frequenceText.setBounds(217, 54, 76, 21);

		frequenceText.setVisible(false);
		lblFrquenceDeCoupure.setVisible(false);

		combo = new CCombo(composite, SWT.BORDER);
		combo.setEditable(false);
		combo.setBounds(195, 13, 98, 22);
		combo.add("Faible");
		combo.add("Moyen");
		combo.add("Fort");
		combo.add("Autre");
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (combo.getText().equals("Autre"))
				{
					frequenceText.setVisible(true);
					frequenceText.setText("0.0");
					lblFrquenceDeCoupure.setVisible(true);
				}
				else
				{
					frequenceText.setVisible(false);
					lblFrquenceDeCoupure.setVisible(false);
				}
			}
		});

		Label lblModulo = new Label(composite, SWT.NONE);
		lblModulo.setBounds(21, 90, 55, 15);
		lblModulo.setText("Modulo");

		moduloText = new Text(composite, SWT.BORDER);
		moduloText.setBounds(217, 87, 76, 21);
		moduloText.setEnabled(false);


		final Button btnCheckButton = new Button(composite, SWT.CHECK);
		btnCheckButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (btnCheckButton.getSelection())
				{					
					moduloText.setEnabled(true);
					moduloText.setText("0.0");
				}
				else
				{
					moduloText.setEnabled(false);
				}
			}
		});
		btnCheckButton.setBounds(111, 89, 93, 16);

		Label labelData = new Label(composite, SWT.WRAP);
		labelData.setBounds(53, 134, 76, 15);
		labelData.setText("Select data :");

		dataCombo = new Combo(composite, SWT.BORDER);
		dataCombo.setBounds(156, 154, 246, 23);
		dataCombo.setItems(dataComboItems);
		// Select the first item by default
		dataCombo.select(0);

		return composite;
	}

	/**
	 * Returns the validator.
	 * 
	 * @return the validator
	 */
	protected IInputValidator getValidator() {
		return validator;
	}

	public int getSelectedDataIndex() {
		return selectedDataIndex;
	}

	/**
	 * Returns the string typed for offset value.
	 * 
	 * @return the typed string
	 */
	public String getFrequenceValue() {
		return frequenceValue;
	}

	public String getDataComboString()
	{
		return dataComboValue;
	}

	public String getModuloValue()
	{
		return moduloValue;
	}

	/**
	 * Returns the index of the selection in the data combo box.
	 * 
	 * @return the selected index
	 */

	/**
	 * Validates the input.
	 * <p>
	 * The default implementation of this framework method delegates the request
	 * to the supplied input validator object; if it finds the input invalid,
	 * the error message is displayed in the dialog's message line. This hook
	 * method is called whenever the text changes in the input field.
	 * </p>
	 */
	protected void validateInput() {
		String errorMessage = null;
		if (validator != null) {
			errorMessage = validator.isValid(frequenceText.getText());
		}
		setErrorMessage(errorMessage);
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			errorMessageText.setText(errorMessage == null ? " \n " : errorMessage);
			// Disable the error message text control if there is no error, or
			// no error text (empty or whitespace only). Hide it also to avoid
			// color change.
			boolean hasError = errorMessage != null && (StringConverter.removeWhiteSpaces(errorMessage)).length() > 0;
			errorMessageText.setEnabled(hasError);
			errorMessageText.setVisible(hasError);
			errorMessageText.getParent().update();
			Control button = getButton(IDialogConstants.OK_ID);
			if (button != null) {
				button.setEnabled(errorMessage == null);
			}
		}
	}
}