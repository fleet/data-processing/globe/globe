package fr.ifremer.globe.dev.tide;

import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

import org.junit.Test;

import fr.ifremer.globe.core.model.tide.ITideService;
import fr.ifremer.globe.core.model.tide.TideGaugeSource;

public class TideWebServiceTest {

	ITideService tideService = ITideService.grab();

	@Test
	public void test() throws IOException {
		tideService.getTideGauges().forEach(tideGauge -> System.out.println(tideGauge));

		tideService.getObservations("62", TideGaugeSource.RAW_HIGHT_FREQUENCY, Instant.parse("2019-12-01T00:00:00Z"),
				Instant.parse("2019-12-10T00:00:00Z"), Optional.of(10)).forEach(obs -> System.out.println(obs));
	}

}
