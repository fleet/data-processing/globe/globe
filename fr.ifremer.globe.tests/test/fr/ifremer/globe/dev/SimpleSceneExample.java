package fr.ifremer.globe.dev;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;

import fr.ifremer.globe.ogl.Globe3DScene;
import fr.ifremer.globe.ogl.core.RectangleD;
import fr.ifremer.globe.ogl.core.geometry.Ellipsoid;
import fr.ifremer.globe.ogl.core.geometry.Mesh;
import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.core.matrices.Matrix4D;
import fr.ifremer.globe.ogl.core.tessellation.RectangleTessellator;
import fr.ifremer.globe.ogl.core.vectors.Vector2D;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearBuffers;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearState;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.infrastructure.EmbeddedResources;
import fr.ifremer.globe.ogl.renderer.jogl.ContextGL2x;
import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.scene.DefaultCameraCreator;
import fr.ifremer.globe.ogl.renderer.scene.PerspectiveCamera;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttribute;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttributeType;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexLocations;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener;
import fr.ifremer.globe.ogl.scene.CameraBehaviour;
import fr.ifremer.globe.ogl.scene.PerspectiveCameraPan;
import fr.ifremer.globe.ogl.util.ViewProjectionType;

public class SimpleSceneExample extends Globe3DScene implements GraphicsWindowListener, MouseListener, MouseMotionListener, MouseWheelListener, KeyListener, PropertyChangeListener {

	private SceneState _sceneState;
	private ClearState _clearDepth;
	private CameraBehaviour _cameraBehavior;
	private Ellipsoid _ellipsoid;
	private GraphicsWindow window = null;

	private DrawState _drawState;

	private Context _context;

	private GLCanvas _canvas;

	// private ShaderProgramGL3x shaderProgram;

	private Mesh rectangle;

	// private ShaderProgramGL3x _shaderProgram;

	// private long currentTime = 0;

	public SimpleSceneExample(GLCanvas canvas) {
		this._canvas = canvas;
	}

	double distanceCameraEye;

	@Override
	public void init(GLAutoDrawable drawable, GraphicsWindow window) {
		Device2x.initialize();

		this._drawState = new DrawState();

		this._context = new ContextGL2x(_canvas);

		this._context.makeCurrent();

		this.window = window;

		// Initialize ShaderProgram (Generation, Attach shaders, Link shaders,
		// Initialize context)

		try {
			this._drawState.setShaderProgram(Device2x.createShaderProgram(
					EmbeddedResources.getText(this.getClass(),"test/fr/ifremer/globe/dev/shader/shaderVS.glsl"), "",
					EmbeddedResources.getText(this.getClass(),"test/fr/ifremer/globe/dev/shader/shaderFS.glsl"), true));

			this._drawState.setRenderState(new RenderState());

			// Initialize scene (lights, camera, colors, ...)
			initializeScene();

			this._drawState.getShaderProgram().getVertexAttributes().add(new ShaderVertexAttribute("position", VertexLocations.Position, ShaderVertexAttributeType.FloatVector2, 1));

			this.rectangle = RectangleTessellator.compute(new RectangleD(new Vector2D(0.0, 0.0), new Vector2D(10.0, 10.0)), 2, 2);
			this._drawState.setVertexArray(this._context.createVertexArray(rectangle, this._drawState.getShaderProgram().getVertexAttributes(), BufferHint.StaticDraw));
		} catch (CouldNotCreateVideoCardResourceException e) {
			e.printStackTrace();
		}

		this._context.release();
	}

	public void initializeScene() {

		_sceneState = new SceneState(DefaultCameraCreator.create(ViewProjectionType.PERSPECTIVE));
		_sceneState.setDiffuseIntensity(0.90f);
		_sceneState.setSpecularIntensity(0.05f);
		_sceneState.setAmbientIntensity(0.05f);

		_ellipsoid = Ellipsoid.Wgs84;

		initCamera();

		_clearDepth = new ClearState();
		_clearDepth.getBuffers().add(ClearBuffers.DepthBuffer);
		_clearDepth.getBuffers().add(ClearBuffers.StencilBuffer);
		_clearDepth.setColor(new Color(255, 255, 255, 255));
		_clearDepth.getBuffers().add(ClearBuffers.ColorBuffer);
	}

	public void initCamera() {
		if (_cameraBehavior != null) {
			_cameraBehavior.dispose();
		}
		if (_sceneState.getCamera() instanceof PerspectiveCamera) {
			PerspectiveCamera camera = (PerspectiveCamera) _sceneState.getCamera();
			camera.setFieldOfViewY(Math.PI / 10.0);
			camera.setPerspectiveNearPlaneDistance(0.000001 * _ellipsoid.getMaximumRadius()); // 0.000001
			camera.setPerspectiveFarPlaneDistance(10.0 * _ellipsoid.getMaximumRadius());
			_cameraBehavior = new PerspectiveCameraPan(camera, window, 20);
		}
	}

	public void clearGLState(GL2 gl) {
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glLineWidth(1);
		gl.glPointSize(1);
		gl.glColor3d(1, 1, 1);
		gl.glDepthRange(0, 1);
	}

	@Override
	public void postRender(GLAutoDrawable drawable, GraphicsWindow windows) {

	}

	@Override
	public void preRender(GLAutoDrawable drawable, GraphicsWindow windows) {
	}

	@Override
	public void render(GLAutoDrawable drawable, GraphicsWindow windows) {
		Context context = window.getContext();
		context.clear(_clearDepth);
		draw(drawable);
	}

	public void draw(GLAutoDrawable drawable) {

		GL2 gl = drawable.getGL().getGL2();

		clearGLState(gl);

		// Activating shaders (bind)

		// this.shaderProgram.bind();

		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2ES1.GL_ALPHA_TEST);
		gl.glAlphaFunc(GL.GL_GREATER, 0.0f);

		gl.glPushMatrix();

		// Reset the projection matrix
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();

		// Reset the modelview matrix
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();

		// Apply model view matrix
		Matrix4D matrix = getModelViewMatrix();
		double[] values = matrix.getReadOnlyColumnMajorValues().clone();

		// Matrix4D matrix = getProjectionMatrix();

		gl.glLoadMatrixd(values, 0);

		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// Color we use for vertex

		gl.glColor3f(0.0f, 0.0f, 1.0f); // Bleu
		//
		//		gl.glBegin(GL2GL3.GL_QUADS);
		//
		//		gl.glVertex3d(-1.0f, 1.0f, 0.0f);
		//		gl.glVertex3d(1.0f, 1.0f, 0.0f);
		//		gl.glVertex3d(-1.0f, -1.0f, 0.0f);
		//		gl.glVertex3d(1.0f, -1.0f, 0.0f);
		//
		//		gl.glEnd();

		// gl.glVertex3f(43f, -80f, 0);

		this._context.draw(PrimitiveType.Triangles, this._drawState, this._sceneState);

		// Desactivating shaders after displaying (unbind)

		// this.shaderProgram.unBind();

		gl.glPopMatrix();

		clearGLState(gl);
	}

	public Matrix4D getModelViewMatrix() {
		return _sceneState.getModelViewPerspectiveMatrix();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height, GraphicsWindow window) {
	}

	@Override
	public void update(GLAutoDrawable drawable, GraphicsWindow windows) {
	}

	@Override
	public void refresh() {
	}

	public void dispose() {
		// Remove all the components used by the window when the scene is closed

		if (window != null) {
			// GL context
			Context context = window.getContext();
			if (context != null) {
				context.makeCurrent();

				// window
				window.dispose();

				window.removeGraphicsWindowListener(this);
				// this._cameraBehavior.dispose();
				window.removeKeyListener(this);
				window.removeMouseListener(this);
				window.removeMouseMotionListener(this);
				window.removeMouseWheelListener(this);
			}

			context.release();
		}
		System.gc();
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged, GraphicsWindow window) {
	}

}
