Lecture scripts Python netCDF4:

INSTALLATION
- Installation de Anaconda (https://www.continuum.io/downloads)
- Installation du package NetCDF4 avec la commande : conda install -c conda-forge netcdf4=1.2.8 (https://anaconda.org/conda-forge/netcdf4)

EXECUTION
- Avec l'IDLE de Anaconda dans : anaconda\Scripts\idle.exe (https://docs.continuum.io/anaconda/ide_integration#idle)
- Avec Eclipse (https://docs.continuum.io/anaconda/ide_integration#eclipse-and-pydev)