"""
Script for the modification depth datas.
"""
from netCDF4 import Dataset

"""
file = DTM file path
x = column of the value
y = line of the value
new_data = new data value
"""
def modify_depth(file, x, y, new_data):
    # Open the file
    dataset = Dataset(file, 'r+', format="NETCDF4")

    # Modify the file
    dataset.variables['ISOUNDINGS'][x,y] = new_data

    # Close the file
    dataset.close()

