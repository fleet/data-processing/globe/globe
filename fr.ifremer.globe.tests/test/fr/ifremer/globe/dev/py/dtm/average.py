"""
Script for the calculation of the depth average.
"""
from netCDF4 import Dataset
import numpy

"""file = DTM file path"""
def depth_average(file):
    # Open the file
    dataset = Dataset(file, 'r', format="NETCDF4")

    # Get the datas
    depth = dataset.variables['DEPTH']
    depth_data = numpy.array(depth[:,:])

    columns = depth.last_column
    lines = depth.last_line

    cells = columns * lines
    addition = 0

    for x in range(0, lines):
        for y in range(0, columns):
            addition = addition + depth_data[x,y]

    # Close the file
    dataset.close()

    return addition/cells

