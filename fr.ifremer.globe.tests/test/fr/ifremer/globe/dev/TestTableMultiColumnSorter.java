package fr.ifremer.globe.dev;

import static org.junit.Assert.fail;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import fr.ifremer.globe.ui.widget.TableMultiColumnSorter;


public class TestTableMultiColumnSorter {

	@Test
	public void test() {

		final Display display = Display.getDefault();
		final Shell shell = new Shell(display);
		shell.setText("Test");
		shell.setLayout(new FillLayout());
		shell.setSize(shell.computeSize(400, 600));
		
		/*************************** START INPUT SIMULATION *********************/

		class File {

			private String title;
			private String id;
			private String desc;

			public File(String title, String id, String desc) {
				this.title = title;
				this.id = id;
				this.setDesc(desc);
			}

		
			@Override
			public String toString() {
				return title + " - " + id;
			}


			@SuppressWarnings("unused")
			public String getDesc() {
				return desc;
			}


			public void setDesc(String desc) {
				this.desc = desc;
			}
			
			
		}
		

		File[] fileList = { new File("Title Z", "1 ", "address 5464"), new File("Title B", "45 ", "address 11"),
				new File("1 Title Z", "1 ", "address 5464"), new File("Title B", "45 ", "address 21"),
				new File("Title E", "12 ", "address 2211"), new File("Title R", "1 ", "address 1151"),
				new File("Title F", "11 ", "address 1151"), new File("Title A", "12 ", "address 221"),
				new File("Title C", "1 ", "address 0554"), new File("Title G", "11 ", "address 1151"),
				new File("Title M", "32 ", "address 221"), new File("Title C", "35 ", "address 0554"),
				new File("Title F", "21 ", "address 1151"), new File("Title A", "36 ", "address 221"),
				new File("Title P", "15 ", "address 0554"), new File("Title S", "41 ", "address 1151"),
				new File("Title J", "32 ", "address 221"), new File("Title C", "3 ", "address 0554") };

		/*************************** END INPUT SIMULATION *********************/
		
		TableMultiColumnSorter<File> table = new TableMultiColumnSorter<File>(shell,SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);
				
		table.setInputs(Arrays.asList(fileList));
		/*
		table.addColumn("Title", 100, SWT.LEFT, (f) -> f.getTitle());
		table.addColumn("Id", 50,SWT.RIGHT,  (f) -> f.getId());
		table.addColumn("Description", 200,SWT.CENTER,  (f) -> f.getDesc(), (f) -> f.getColor());*/

		
		shell.open();
		shell.pack();
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}

		}

		display.dispose();

		fail("Not yet implemented");
	}

}
