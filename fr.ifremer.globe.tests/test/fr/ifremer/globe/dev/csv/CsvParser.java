package fr.ifremer.globe.dev.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import fr.ifremer.globe.utils.EfficientCounter;
/***
 * sample class used to parse download statistiques for softwares
 * */
public class CsvParser {

	public static void main(String[] args) {
		CsvParser parser=new CsvParser();
		
		try {
			parser.parse("E://DownloadStats2017.csv");
		} catch (IOException e) {
			System.err.println("An error occurs");
			e.printStackTrace();
		}
	}

	class Software {
		protected String name;
		EfficientCounter<String> counter = new EfficientCounter<>();

		public Software(String softName) {
			this.name = softName;
		}

		public EfficientCounter<String> getCounter() {
			return counter;
		}

		/**
		 * check if the given file is linked to the software, aimed to be
		 * overrided by the differents software
		 * 
		 * @param file
		 *            the downloaded file name return true if the file is the
		 *            file is associated with the software, false otherwise
		 */
		public boolean concerns(String file) {
			return file.toLowerCase().contains(name.toLowerCase());
		}

		/**
		 * push a user name and the associated downloaded file
		 * 
		 * @return true if the value was associated with the software, false
		 *         otherwise
		 */
		public boolean push(String downloadedFile, String user) {
			if (concerns(downloadedFile)) {
				counter.put(user);
				return true;
			} else
				return false;

		}

		/**
		 * return the number of entry in the counter, ie match the number of downloaded licences granted for the user
		 * */
		public int getKeyCount() {
			return getCounter().getKeys().size();
		}
	}
	/**
	 * filter by user email 
	 * allow to make stat on ifremer and others 
	 * */
	interface UserFilter
		{
			boolean isFiltered(String user);
		}
	class LineHub {
		ArrayList<Software> list = new ArrayList<>();
		ArrayList<String> ignoredLines = new ArrayList<>();
		private UserFilter filter;
	
		
		LineHub(UserFilter filter) {
			this.filter=filter;
			list.add(new Software("GLOBE"));
			list.add(new Software("CARAIBES") );
			list.add(new Software("CASINO") );
			list.add(new Software("DORIS") );
			list.add(new Software("MOVIES") {
				public boolean concerns(String file) {
					return file.toLowerCase().contains(this.name.toLowerCase()) || file.toLowerCase().contains("HERMES".toLowerCase());
				}
			});
			list.add(new Software("SonarScope"));
			list.add(new Software("drogm"));
			list.add(new Software("dyneco"));
		}

		public void push(String s) {
			// decode csv
			String[] parsed = s.split(";");
			boolean processed = false;
			if (parsed.length >= 4) {
				String user = parsed[0];
				String file = parsed[3];
				if(!filter.isFiltered(user))
				{
					for (Software soft : list) {
						processed |= soft.push(file, user);
					}
				} else 
				{
					processed=true;
				}
			}
			if (!processed) {
				ignoredLines.add(s);
			}
		}

		/**
		 * write to stdout a report of downloaded files
		 * */
		public void report() {
			if(ignoredLines.size()>0)
			{
				System.out.println("Ignored Lines " + ignoredLines.size());
				for(String s:ignoredLines)
				{
					System.out.println(s);
				}
			}
			System.out.println("\tSoftware entries");
			for(Software soft:list)
			{
				System.out.println("\t\t"+soft.name+" "+ soft.getKeyCount());
			}
		};
	}

	public void parse(String fileName) throws IOException {
		LineHub ifremerStats = new LineHub((usermail)-> {
			return !usermail.toLowerCase().contains("ifremer.fr") || 
					// filter nse team
					usermail.toLowerCase().contains("cyrille.poncelet@ifremer.fr") || 
					usermail.toLowerCase().contains("laurent.berger.ifremer.fr") || 
					usermail.toLowerCase().contains("marie.paule.corre@ifremer.fr") || 
					usermail.toLowerCase().contains("gael.billant@ifremer.fr"); 
		});
		LineHub outsideStats = new LineHub((usermail)-> {return usermail.toLowerCase().contains("ifremer.fr");});
		
		
		InputStream is = new FileInputStream(new File(fileName));
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		br.lines().forEach((s) -> {
			ifremerStats.push(s);
			outsideStats.push(s);
		});
		System.out.println("IFREMER");
		ifremerStats.report();
		System.out.println("OutSide IFREMER");
		outsideStats.report();
		br.close();
	}
}
