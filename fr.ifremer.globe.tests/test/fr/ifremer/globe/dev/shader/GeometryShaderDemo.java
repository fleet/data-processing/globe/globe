/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.dev.shader;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import javax.swing.JFrame;

import com.jogamp.common.nio.Buffers;

/**
 * Test class to enforce a Geometry Shader concurrently to OpenGL 2<br>
 * inspired from https://open.gl/geometry<br>
 * <br>
 * Before launching under Windows, define the property java.library.path as
 * -Djava.library.path=[Path to Globe]/jogl.win32.x86_64
 * 
 */
public class GeometryShaderDemo implements GLEventListener {

	/** Vertex shader */
	public static String VERTEX_SHADER_SRC = //
			"#version 110\n" + //
			"attribute vec2 pos;" + //
			"attribute vec3 color;" + //
			"attribute float sides;" + //
			"varying vec3 vColor;" + // Output to geometry shader
			"varying float vSides;" + // Output to geometry shader
			"void main()" + //
			"{ " + //
			"    gl_Position = vec4(pos, 0.0, 1.0);" + //
			"    vColor = color;" + //
			"    vSides = sides;" + //
			"}";

	/** Geometry shader */
	public static String GEOMETRY_SHADER_SRC = //
			"#extension GL_EXT_geometry_shader4: enable\n" + //
			"#extension GL_ARB_enhanced_layouts: enable\n" + //
			"layout(points) in;" + //
			"layout(line_strip, max_vertices = 64) out;" + //
			"in vec3 vColor[];" + // Output from vertex shader for each
									// vertex
			"in float vSides[];" + 
			"out vec3 fColor;" + // Output to fragment shader
			"const float PI = 3.1415926;" + //
			"void main()" + //
			"{" + //
			"   fColor = vColor[0];" + // Point has only one vertex;
			"   for (int i = 0; i <= vSides[0]; i++) {" + //
			"      float ang = PI * 2.0 / vSides[0] * i;" +
			"      vec4 offset = vec4(cos(ang) * 0.3, -sin(ang) * 0.4, 0.0, 0.0);" + //
			"      gl_Position = gl_in[0].gl_Position + offset;" + //
			"      EmitVertex();" + //
			"   }" + //
			"   EndPrimitive();" + //
			"}";

	/** Fragment shader */
	public static String FRAGMENT_SHADER_SRC = //
			"#version 110\n" + //
			"varying vec3 fColor;" + //
			"" + //
			"void main()" + //
			"{" + //
			"    gl_FragColor = vec4(fColor, 1.0);" + //
			"}";

	/** ID of the compiled program. */
	protected int programID;

	/** Retrieves the info log for the shader */
	public String getShaderInfoLog(GL2 gl, int obj) {
		// Otherwise, we'll get the GL info log
		final int logLen = getShaderParameter(gl, obj, GL2.GL_INFO_LOG_LENGTH);
		if (logLen <= 0)
			return "";

		// Get the log
		final int[] retLength = new int[1];
		final byte[] bytes = new byte[logLen + 1];
		gl.glGetShaderInfoLog(obj, logLen, retLength, 0, bytes, 0);
		final String logMessage = new String(bytes);

		return String.format("ShaderLog: %s", logMessage);
	}

	/** Get a shader parameter value. */
	private int getShaderParameter(GL2 gl, int obj, int paramName) {
		final int params[] = new int[1];
		gl.glGetShaderiv(obj, paramName, params, 0);
		return params[0];
	}

	/**
	 * Perform the rendering
	 */
	protected void renderScene(GL2 gl) {
		gl.glClear(GL.GL_COLOR_BUFFER_BIT);
		gl.glUseProgram(programID);
		gl.glDrawArrays(GL.GL_POINTS, 0, 4);
		
		// Check out error
		int error = gl.glGetError();
		if (error != 0) {
			System.err.println("ERROR on render : " + error);
		}
	}

	/** Helper method to generate an id of buffer. */
	protected int generateBufferId(GL2 gl) {
		// allocate an array of one element in order to store the generated id
		int[] idArray = new int[1];
		// let's generate
		gl.glGenBuffers(1, idArray, 0);
		// return the id
		return idArray[0];
	}

	/** Helper method to generate an id of Vertex Array. */
	protected int generateVertexArrayId(GL2 gl) {
		// allocate an array of one element in order to store the generated id
		int[] idArray = new int[1];
		// let's generate
		gl.glGenVertexArrays(1, idArray, 0);
		// return the id
		return idArray[0];
	}

	/** Create a VBO that holds the coordinates of the points. */
	protected void createVbo(GL2 gl) {
		float points[] = { //
				-0.45f, 0.45f, /* Red point */ 1.0f, 0.0f, 0.0f , /* 4 Sides */ 4f,
				0.45f, 0.45f, /* Green point */0.0f, 1.0f, 0.0f, /* 8 Sides */ 8f,
				0.45f, -0.45f, /* Blue point */0.0f, 0.0f, 1.0f, /* 16 Sides */ 16f,
				-0.45f, -0.45f, /* Yellow point */1.0f, 1.0f, 0.0f, /* 32 Sides */ 32f
		};

		// Create a buffer that holds the coordinates of the points
		int vbo = generateBufferId(gl);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo);
		gl.glBufferData(GL.GL_ARRAY_BUFFER, points.length * Float.SIZE / 8, Buffers.newDirectFloatBuffer(points), GL.GL_STATIC_DRAW);

		// create a VAO and set the vertex format specification
		int vaId = generateVertexArrayId(gl);
		gl.glBindVertexArray(vaId);

		// Specify layout of point data
		int posAttrib = gl.glGetAttribLocation(programID, "pos");
		gl.glEnableVertexAttribArray(posAttrib);
		gl.glVertexAttribPointer(posAttrib, 2/* 2 floats per vertex */, GL.GL_FLOAT, false,
				6 * Float.BYTES /* Chunk size */, 0l);

		// Specify layout of color data
		int colorAttrib = gl.glGetAttribLocation(programID, "color");
		gl.glEnableVertexAttribArray(colorAttrib);
		gl.glVertexAttribPointer(colorAttrib, 3/* 3 floats per vertex */, GL.GL_FLOAT, false,
				6 * Float.BYTES /* Chunk size */,
				2 * Float.BYTES /* Jump first two floats */);
		
		// Specify layout of sides data
		int sidesAttrib = gl.glGetAttribLocation(programID, "sides");
		gl.glEnableVertexAttribArray(sidesAttrib);
		gl.glVertexAttribPointer(sidesAttrib, 1/* 1 float per vertex */, GL.GL_FLOAT, false,
				6 * Float.BYTES /* Chunk size */,
				5 * Float.BYTES /* Jump first 5th floats */);
	}

	/** The shaders are compiled and activated. */
	protected int createProgram(GL2 gl) {
		int vertexShader = createShader(gl, GL2.GL_VERTEX_SHADER, VERTEX_SHADER_SRC);
		int geometryShader = createShader(gl, GL3.GL_GEOMETRY_SHADER, GEOMETRY_SHADER_SRC);
		int fragmentShader = createShader(gl, GL2.GL_FRAGMENT_SHADER, FRAGMENT_SHADER_SRC);

		System.out.println(getShaderInfoLog(gl, vertexShader));
		System.out.println(getShaderInfoLog(gl, geometryShader));
		System.out.println(getShaderInfoLog(gl, fragmentShader));

		int result = gl.glCreateProgram();
		gl.glAttachShader(result, vertexShader);
		gl.glAttachShader(result, geometryShader);
		gl.glAttachShader(result, fragmentShader);
		gl.glLinkProgram(result);
		gl.glUseProgram(result);
		return result;
	}

	/** Helper method to create and compile a shader. */
	protected int createShader(GL2 gl, int shaderType, String src) {
		int shader = gl.glCreateShader(shaderType);
		gl.glShaderSource(shader, 1, new String[] { src }, null);
		gl.glCompileShader(shader);
		return shader;
	}

	/** GL Init */
	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		programID = createProgram(gl);
		createVbo(gl);
	}

	/**
	 * Build a JFrame.
	 */
	protected static JFrame buildFrame(String name, GLEventListener eventListener, int x, int y, int width, int height) {
		JFrame result = new JFrame(name);
		result.setBounds(x, y, width, height);
		result.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// OpenGL 2 !
		GLProfile glp = GLProfile.get(GLProfile.GL2);
		GLCapabilities glCapabilities = new GLCapabilities(glp);
		GLCanvas glCanvas = new GLCanvas(glCapabilities);

		glCanvas.addGLEventListener(eventListener);
		result.add(glCanvas);

		return result;
	}

	/** GL Window Reshape */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
	}

	/** GL Render loop */
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		renderScene(gl);
	}

	/** GL Complete */
	@Override
	public void dispose(GLAutoDrawable drawable) {
	}

	/** Main method. */
	public static void main(String[] args) {
		// allocate the openGL application
		GeometryShaderDemo geometryShaderDemo = new GeometryShaderDemo();

		// allocate a frame and display the openGL inside it
		JFrame frame = buildFrame("Geometry Shader demo", geometryShaderDemo, 10, 10, 300, 300);

		// display it and let's go
		frame.setVisible(true);
	}

}