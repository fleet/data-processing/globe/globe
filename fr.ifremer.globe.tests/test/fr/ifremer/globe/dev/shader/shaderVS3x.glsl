#version 330

layout(location = 0)in vec4 position;

uniform mat4 og_modelViewPerspectiveMatrix;

void main()
{
    gl_Position = og_modelViewPerspectiveMatrix *position;
}