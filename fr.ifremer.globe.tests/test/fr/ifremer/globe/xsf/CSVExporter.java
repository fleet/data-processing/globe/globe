package fr.ifremer.globe.xsf;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import fr.ifremer.globe.core.model.wc.LatLonSample;

public class CSVExporter {
	PrintWriter writer;
	public CSVExporter(String fileName) throws IOException {
		 writer = new PrintWriter(
                new BufferedWriter (
                    new FileWriter(fileName)));
	}
	public void printHeader()
	{
		writer.println("latitude;longitude; acrossDistance; alongDistance;depth;range in sample; beam idx;value");
	}
	
	public void dispose()
	{
		writer.close();
	}
	public void toCSV(LatLonSample x)
	{
		writer.println(String.format(Locale.ROOT,"%.10f;%.10f;%.2f;%.2f;%.2f;%d;%d;%.2f", 
				x.latitude,
				x.longitude,
				x.acrossDistance==0 ? 0:x.acrossDistance, //avoid to have -0 written in csv file
				x.alongDistance==0 ? 0:x.alongDistance,//avoid to have -0 written in csv file
				x.elevation==0 ? 0:x.elevation,//avoid to have -0 written in csv file
				x.echo.range,
				x.echo.beam,
				x.echo.value));
	}

}
