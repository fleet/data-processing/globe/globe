
package fr.ifremer.globe.viewer3d.gamepad;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;

public class JoystickWindowsHandler {
	@Execute
	public void execute() {
		JoystickTest joy=new JoystickTest();
		joy.displayTestWindows();
	}


	@CanExecute
	public boolean canExecute() {

		return true;
	}

}