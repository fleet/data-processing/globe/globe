package fr.ifremer.globe.viewer3d.gamepad;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JToggleButton;

import org.junit.Test;

import fr.ifremer.viewer3d.gamepad.JInputJoystick;
import net.java.games.input.Component;
import net.java.games.input.Controller;

/**
 *
 * Joystick Test with JInput
 * 
 * 
 * @author TheUzo007
 *         http://theuzo007.wordpress.com
 * 
 * 
 * Test for joysticks of stick or gamepad type (JInput type), 
 * like Logitech Dual Action which is a stick type or
 * Xbox MadCatz which is a gamepad type.
 * 
 */

public class JoystickTest {

	public void displayTestWindows()
	{
		final JFrameWindow window = new JFrameWindow();

		/*
        JInputJoystickTest jinputJoystickTest = new JInputJoystickTest();
        // Writes (into console) informations of all controllers that are found.
        jinputJoystickTest.getAllControllersInfo();
        // In loop writes (into console) all joystick components and its current values.
        jinputJoystickTest.pollControllerAndItsComponents(Controller.Type.STICK);
        //jinputJoystickTest.pollControllerAndItsComponents(Controller.Type.GAMEPAD);
		 */


		// Test for joystick of stick or gamepad type.
		//stickOrGamepadTypeJoystick_Test(window);
		stickOrGamepadTypeJoystick_Test_Better(window);

		// Test for joystick of stick type.
		//stickTypeJoystick_Test(window);
	}

	@Test
	public void main() {
		JoystickTest joy=new JoystickTest();
		joy.displayTestWindows();
	}


	static int value2Percent(double v)
	{
		return (int)((2 - (1 - v)) * 100) / 2;
	}

	/*
	 * Test for stick and gamepad type of joystick.
	 * The same as stickOrGamepadTypeJoystick_Test but better/easier/cleaner for right controller joystick.
	 */
	private static void stickOrGamepadTypeJoystick_Test_Better(JFrameWindow window)
	{
		// Creates controller
		JInputJoystick joystick;
		try {
			
			joystick = new JInputJoystick(Controller.Type.STICK, Controller.Type.GAMEPAD);


			// Checks if the controller was found.
			if( !joystick.isControllerConnected() ){
				window.setControllerName("No controller found!");
				return;
			}

			// Sets controller name.
			window.setControllerName(joystick.getControllerName());

			// If gamepad, change name for axis.
			if(joystick.getControllerType() == Controller.Type.GAMEPAD)
			{
				window.setProgressBar1Name("X Rotation");
				window.setProgressBar2Name("Y Rotation");
			}
			// Stick type
			else
			{
				window.setProgressBar3Name("");
				window.hideProgresBar3();
			}

			while(true)
			{
				// Gets current state of joystick! And checks, if joystick is disconnected, break while loop.
				if( !joystick.pollController() ) {
					window.setControllerName("Controller disconnected!");
					break;
				}

				// Left controller joystick
				int xValuePercentageLeftJoystick =value2Percent( joystick.getX_LeftJoystick_Value()) ;
				int yValuePercentageLeftJoystick = value2Percent(joystick.getY_LeftJoystick_Value());
				window.setXYAxis(xValuePercentageLeftJoystick, yValuePercentageLeftJoystick);

				// Right controller joystick
				int xValuePercentageRightJoystick =value2Percent(joystick.getX_RightJoystick_Value());
				int yValuePercentageRightJoystick = value2Percent(joystick.getY_RightJoystick_Value());
				window.setZAxis(xValuePercentageRightJoystick);
				window.setZRotation(yValuePercentageRightJoystick);

				// If controller is a gamepad type. 
				if(joystick.getControllerType() == Controller.Type.GAMEPAD)
				{ // Must check if controller is a gamepad, because stick type controller also have Z axis but it's for right controller joystick.
					// If Z Axis exists.
					if(joystick.componentExists(Component.Identifier.Axis.Z)){
						int zAxisValuePercentage = value2Percent(joystick.getZAxisValue());
						window.setZAxisGamepad(zAxisValuePercentage);
					}
				}

				// Sets controller buttons
				JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 1));
				buttonsPanel.setBounds(6, 19, 246, 110);
				ArrayList<Boolean> buttonsValues = joystick.getButtonsValues();
				for(int i=0; i < buttonsValues.size(); i++) {
					JToggleButton aToggleButton = new JToggleButton(""+(i+1), buttonsValues.get(i));
					aToggleButton.setPreferredSize(new Dimension(48, 25));
					aToggleButton.setEnabled(false);
					buttonsPanel.add(aToggleButton);
				}
				window.setControllerButtons(buttonsPanel);

				// Hat Switch
				float hatSwitchPosition = joystick.getHatSwitchPosition();
				window.setHatSwitch(hatSwitchPosition);

				try {
					Thread.sleep(20);
				} catch (InterruptedException ex) {
					Logger.getLogger(JoystickTest.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}






}
