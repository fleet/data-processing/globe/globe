package fr.ifremer.globe.viewer3d;

import java.nio.DoubleBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.perfcounter.PerfoCounter;
import fr.ifremer.viewer3d.data.FileProjectionCache;

public class FileProjectionCacheTest {
	@Test
	public void testFile() throws OutOfMemoryError, Exception {
		PerfoCounter count=new PerfoCounter("Create File");
		count.start();
	    Path tempFile = Paths.get("c:\\tmp/foo");
	    int dimY=100;
	    int dimX=100;
		FileProjectionCache cache=new FileProjectionCache(tempFile, dimX,dimY);
		for(int line =0;line<dimY;line ++)
		{
			DoubleBuffer values=cache.getBuffer();
			
			for(int x=0;x<dimX;x++)
			{
				values.put(x,x+line);
			}
			cache.write(line);
		}
		System.out.println(count.stop());
		//now read the values and check their values
		for(int line =0;line<dimY;line ++)
		{
			DoubleBuffer values=cache.getLine(line);
			for(int x=0;x<dimX;x++)
			{
				Assert.assertEquals("line: "+ line + " x:"+x,values.get(x),x+line,10e-2);
			}
		}
		
		cache.dispose();
		
	}
}
