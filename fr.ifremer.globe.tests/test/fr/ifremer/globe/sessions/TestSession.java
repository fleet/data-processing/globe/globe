package fr.ifremer.globe.sessions;

import java.io.File;
import java.nio.file.Paths;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.internal.workbench.E4Workbench;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.service.session.impl.SessionController;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class TestSession {
	public static String MBG_GLOBE = GlobeTestUtil.getTestDataPath()
			+ "file/Reson/CARAIBES/20130322_204233_PP-7150-24kHz-CIB.mbg";

	public static final String SESSION_FILE = GlobeTestUtil.getTestDataPath() + "/file/generated/test.json";

	@Test
	public void testSession() throws Exception {

		// Init services
		IFileLoadingService fileLoadingService = IFileLoadingService.grab();
		IFileService fileService = IFileService.grab();
		SessionController sessionController = (SessionController) ISessionService.grab();
		ITreeNodeFactory treeNodeFactory = ITreeNodeFactory.grab();

		IEclipseContext testContext = E4Workbench.getServiceContext();
		var projectExplorerModel = ContextInjectionFactory.make(ProjectExplorerModel.class, testContext);
		ContextInjectionFactory.inject(fileService, testContext);
		ContextInjectionFactory.inject(fileLoadingService, testContext);
		ContextInjectionFactory.inject(sessionController, testContext);
		ContextInjectionFactory.inject(treeNodeFactory, testContext);
		ContextInjectionFactory.inject(this, testContext);

		// Init SessionController
		GroupNode dataNode = projectExplorerModel.getDataNode();
		FolderNode parentNode = treeNodeFactory.createFolderNode(dataNode, "my Group", "TEST");
		treeNodeFactory.createFileInfoNode(new BasicFileInfo(MBG_GLOBE), parentNode);
		fileLoadingService.load(MBG_GLOBE);

		waitForLoading(fileLoadingService);

		// Save the session, paths are in relative way
		File sessionFile = new File(SESSION_FILE);
		sessionController.saveSession(true, sessionFile);
		Assert.assertTrue(sessionFile.exists());

		// clean session
		fileLoadingService.delete(MBG_GLOBE);
		Assert.assertNotNull(fileLoadingService.getFileInfoModel().getAll().isEmpty());
		dataNode.removeChildren();

		// Reload session file
		sessionController.loadSession(sessionFile);
		waitForLoading(fileLoadingService);
		Assert.assertEquals(1, fileLoadingService.getFileInfoModel().getAll().size());
		Assert.assertEquals(1, dataNode.getChildren().size());

		// Check folder
		TreeNode folder = dataNode.getChildren().get(0);
		Assert.assertTrue(folder instanceof FolderNode);
		Assert.assertEquals("my Group", ((FolderNode) folder).getName());
		Assert.assertEquals(1, ((FolderNode) folder).getChildren().size());

		TreeNode child = ((FolderNode) folder).getChildren().get(0);
		Assert.assertTrue(child instanceof FileInfoNode);
		Assert.assertEquals(Paths.get(MBG_GLOBE), ((FileInfoNode) child).getFileInfo().toPath());

	}

	protected void waitForLoading(IFileLoadingService fileLoadingService) {
		// Wait the end of the loading
		Job[] jobs = Job.getJobManager().find(fileLoadingService);
		Assert.assertNotNull(jobs);
		for (Job job : jobs) {
			try {
				job.join();
			} catch (InterruptedException e) {
				// Don't care
			}
		}
	}
}
