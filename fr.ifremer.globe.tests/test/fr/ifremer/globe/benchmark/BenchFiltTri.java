package fr.ifremer.globe.benchmark;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.util.Optional;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.processes.filtri.FilTriProcess;
import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;
import fr.ifremer.globe.test.utils.BenchUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class BenchFiltTri {

	/** Reference times (ms) */
	private long referenceTime;
	/** Authorized delta (%) relatives to test hardware environment. */
	private final static long delta = 10;
	/** Input reference file path used for performance tests. */
	private final static String INPUT_MBG_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/FiltTri/MethodHeight_upd_bobgosmf24_0054.mbg";
	public static final String REFERENCE_FOLDER = GlobeTestUtil.getTestDataPath() + "/file/FiltTri";

	protected File tempDir;

	/**
	 * Main method for bias correction tests
	 * 
	 * @throws Exception
	 */
	@Test
	public void tests() throws Exception {
		referenceTime = BenchUtils.getReferenceTime(this.getClass().getSimpleName());
		test1AgainstReference();
	}

	/**
	 * Bias correction time test against reference
	 * 
	 * @throws Exception
	 */
	public void test1AgainstReference() throws Exception {
		long total = System.currentTimeMillis();

		triangulationFiltering();

		long elapsed = System.currentTimeMillis() - total;
		System.out.println("-------elapsed : " + elapsed);
		Assert.assertTrue(
				this.getClass().getSimpleName() + "test creation is slower than its time reference : " + elapsed
						+ "ms. Expected : " + referenceTime + "s.",
				elapsed < referenceTime + (delta * referenceTime) / 100);
	}

	/**
	 * Test de conversion du fichier donné.
	 */
	public void triangulationFiltering() throws ParseException, GException, IOException, InterruptedException {	
		FiltTriParameters parameters = new FiltTriParameters();
		File inputFile = new File(INPUT_MBG_FILE);
		File outputFile = new File(this.tempDir, parameters.getProperty(FiltTriParameters.PROPERTY_NEW_FILES_PREFIX)
				+ new File(INPUT_MBG_FILE).getName());
		
		parameters.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_PREFIX, "MethodNormal_");

		parameters.setIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION,
				FiltTriParameters.FILTERING_OPTION_NORMAL);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_HEIGHT_INVALID_LIMIT, 6d);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_SELECT_LIMIT, 4d);
		parameters.setDoubleProperty(FiltTriParameters.PROPERTY_NORMAL_ANGLE_INVALID_LIMIT, 60d);

		// File configuration
		parameters.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_DIRECTORY, this.tempDir.getAbsolutePath());
		parameters.setBoolProperty(FiltTriParameters.PROPERTY_NEW_FILE_OPTION, true);
		parameters.setIntProperty(FiltTriParameters.PROPERTY_SLICE_SIZE, 250);

		// Projection
		String projection = "+proj=merc +lon_0=0 +lat_ts=46.6 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs ";
		parameters.setProjection(new ProjectionSettings(projection));

		FilTriProcess filtri = new FilTriProcess();
		filtri.process(inputFile, outputFile, parameters, Optional.empty(), new NullProgressMonitor());
	
		Assert.assertTrue("MBG result file not found : " + outputFile.getAbsolutePath(), outputFile.exists());
	}

	@Before
	public void init() throws IOException {
		this.tempDir = Files.createTempDirectory("FilTri_", new FileAttribute[0]).toFile();
	}

	/**
	 * Nettoyage après chaque test.
	 */
	@After
	public void afterConversion() throws IOException {
		if (this.tempDir != null)
			FileUtils.forceDelete(this.tempDir);
	}

}
