package fr.ifremer.globe.benchmark;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import org.junit.Test;

import fr.ifremer.globe.utils.perfcounter.PerfoCounter;

public class MappedFile {

	@Test
	public void create2GMapped() throws Exception
	{
		PerfoCounter perf=new PerfoCounter("create Mapped File");

		File outputFile=new File("howtodoinjava.dat");
		MappedByteBuffer out;
		RandomAccessFile raf = null;
		try{
			perf.start();
			int length = Integer.MAX_VALUE; // 
			raf=new RandomAccessFile(outputFile, "rw");
			out= raf.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, length);
			for (int i = 0; i < length; i++)
			{
				out.put((byte) 'x');
			}
			out.force();
		}
		finally
		{
			out=null;
			raf.close();
			perf.stop().print();
			System.err.println(perf.stop().toString());
			outputFile.delete();
		}
	}
//	@Test
	// non parlant, ce test doit prendre en compte la taille des pages pour pouvoir être significatif, i.e on ne doit pas écrire byte par byte mais plutot par paquet plus importants
	public void create2GRAF() throws Exception
	{
		PerfoCounter perf=new PerfoCounter("create Mapped File");

		File outputFile=new File("RAF.dat");
		RandomAccessFile raf = null;
		try{
			perf.start();
			int length = Integer.MAX_VALUE; // 
			raf=new RandomAccessFile(outputFile, "rw");
			for (int i = 0; i < length; i++)
			{
				raf.writeChar('x');
			}
			
		}
		finally
		{
			raf.close();
			perf.stop().print();
			System.err.println(perf.stop().toString());
			outputFile.delete();
		}
	}
}
