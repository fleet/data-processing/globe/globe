package fr.ifremer.globe.benchmark;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.perfcounter.PerfoCounter;
/**
 * check for iteration cost of object creation vs pool usage
 * */
public class SmallObjectAllocation {
	
	class Data{
		float x,y,z;
		int idx;
		int b;
	}
	private static final int MAX = Integer.MAX_VALUE;//3000*880*30000;
	@Test
	public void object() {
		Assert.assertTrue(MAX>0);
		PerfoCounter counter=new PerfoCounter("Object creation");
		counter.start();
		for(int i=0;i<MAX;i++)
		{
			Data a=new Data();
			a.idx=i;
			process(a);
		}
		System.out.println(counter.stop());
	}
	
	public static void process(Data a)
	{
		
	}
}
