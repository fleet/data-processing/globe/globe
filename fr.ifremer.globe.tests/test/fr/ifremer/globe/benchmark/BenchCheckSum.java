package fr.ifremer.globe.benchmark;

import java.io.File;
import java.io.IOException;
import java.util.zip.Adler32;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.perfcounter.PerfoCounter;

public class BenchCheckSum {
	@Test
	public void testSpeed() throws IOException
	{
		Adler32 adl= new Adler32();

		File file=new File("C:\\Globe\\donnees\\EM2040D\\EM2040\\0006_20130403_102305_Thalia.mbg");
		
		Assert.assertTrue(file.exists());
		double size=file.length()/1024/1024/1024.;
		PerfoCounter perf=new PerfoCounter("chks");

		perf.start();
		long uid=FileUtils.checksumCRC32(file);
		System.err.println("size "+size+" CRC32 "+perf.stop()+ " ("+uid+")");
		
		perf.start();
		uid=FileUtils.checksum(file,adl).getValue();
		System.err.println("size "+size+" Adler32 "+perf.stop()+ " ("+uid+")");
		
		file=new File("C:\\Globe\\donnees\\EM2040D\\EM2040\\0006_20130403_102305_Thalia.all");
		size=file.length();
		
		uid=FileUtils.checksumCRC32(file);
		System.err.println("size "+size+" CRC32 "+perf.stop()+ " ("+uid+")");
		
		perf.start();
		uid=FileUtils.checksum(file,adl).getValue();
		System.err.println("size "+size+" Adler32 "+perf.stop()+ " ("+uid+")");
		
	}

}
