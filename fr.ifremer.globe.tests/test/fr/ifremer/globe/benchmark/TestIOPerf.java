package fr.ifremer.globe.benchmark;

import static java.lang.System.out;

import java.io.File;
import java.io.RandomAccessFile;

import org.junit.Test;

public class TestIOPerf {
	public static final int PAGE_SIZE = 800 * 900*4;
	public static final long lenght= 5000;
	public static final String FILE_NAME = "test.dat";

	@Test
	public void go() throws Exception {
		clear();
		double timeelapsed=testRandomAccessWrite(FILE_NAME);
		double bytesWritePerSec = (lenght*PAGE_SIZE ) / timeelapsed;
		out.format("%s time, rate %s Mbytes/sec\n" ,timeelapsed,bytesWritePerSec/1024./1024.);

	}
	public void clear()
	{
		if(new File(FILE_NAME).exists())
		{
			new File(FILE_NAME).delete();
		}
		
	}
	public double testRandomAccessWrite(final String fileName) throws Exception {
		long start = System.currentTimeMillis();
		RandomAccessFile file = new RandomAccessFile(fileName, "rw");
		final byte[] buffer = new byte[PAGE_SIZE];

		for (long i = 0; i < lenght; i++) {
			file.write(buffer, 0, PAGE_SIZE);
		}

		file.close();
		return (System.currentTimeMillis() - start)/1000.;

	}
}
