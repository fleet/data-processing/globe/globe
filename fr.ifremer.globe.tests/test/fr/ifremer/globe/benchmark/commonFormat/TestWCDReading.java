package fr.ifremer.globe.benchmark.commonFormat;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.System.out;
import static java.nio.channels.FileChannel.MapMode.READ_ONLY;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Assert;
import org.junit.Test;


/**
 * test the read rate of a wcd file, over meskl2
 * */
public class TestWCDReading {
	//static String FILENAME="Z://CARNOT//WC//FichierALL//THALIA//0077_20120525_141950_Thalia.wcd";
	static String DIRSRC="d://tmp//src";
	//static String DIR="d://test";
	static File DESTDIR=new File("d://tmp/random");
	static File DESTDIRSYSTEM=new File("d://tmp/system");
	static File DESTDIRMEMMAP=new File("d://tmp/memmap");
	static File DESTDIRSTATIC=new File("d://tmp/static");
	byte[] datagramBuffer=new byte[(int)64000];
	byte[] staticreadBuffer=new byte[8192];

	@Test
	public void runBench() throws IOException
	{
		out.println("Static buff Copier");
		//		staticreadBuffer=new byte[512];
		//		out.println("buffer size= "+staticreadBuffer.length);
		//		copyDirectory(new Copier() {
		//
		//			@Override
		//			public void run(File source) throws Exception {
		//				copyFileWithRandomFileWithFixedBuffer(source, getDestination());
		//			}
		//
		//			@Override
		//			public File getDestination() {
		//				return DESTDIRSTATIC;
		//			}
		//		});
		//		staticreadBuffer=new byte[8192];
		//		out.println("buffer size= "+staticreadBuffer.length);
		//		copyDirectory(new Copier() {
		//
		//			@Override
		//			public void run(File source) throws Exception {
		//				copyFileWithRandomFileWithFixedBuffer(source, getDestination());
		//			}
		//
		//			@Override
		//			public File getDestination() {
		//				return DESTDIRSTATIC;
		//			}
		//		});
		staticreadBuffer=new byte[8192];
		out.println("buffer size= "+staticreadBuffer.length);
		copyDirectory(new Copier() {

			@Override
			public void run(File source) throws Exception {
				copyFileWithRandomFileWithFixedBuffer(source, getDestination());
			}

			@Override
			public File getDestination() {
				return DESTDIRSTATIC;
			}
		});
		//		staticreadBuffer=new byte[65536];
		//		out.println("buffer size= "+staticreadBuffer.length);
		//		copyDirectory(new Copier() {
		//
		//			@Override
		//			public void run(File source) throws Exception {
		//				copyFileWithRandomFileWithFixedBuffer(source, getDestination());
		//			}
		//
		//			@Override
		//			public File getDestination() {
		//				return DESTDIRSTATIC;
		//			}
		//		});
		//		staticreadBuffer=new byte[131072];
		//		out.println("buffer size= "+staticreadBuffer.length);
		//		copyDirectory(new Copier() {
		//
		//			@Override
		//			public void run(File source) throws Exception {
		//				copyFileWithRandomFileWithFixedBuffer(source, getDestination());
		//			}
		//
		//			@Override
		//			public File getDestination() {
		//				return DESTDIRSTATIC;
		//			}
		//		});
//		out.println("System Copier");
//		copyDirectory(new Copier() {
//
//			@Override
//			public void run(File source) throws Exception {
//				copyWithSystemIO(source, getDestination());
//			}
//
//			@Override
//			public File getDestination() {
//				return DESTDIRSYSTEM;
//			}
//		});
//		//
//		out.println("RandomFile Copier");
//		copyDirectory(new Copier() {
//
//			@Override
//			public void run(File source) throws Exception {
//				copyFileWithRandomFile(source, getDestination());
//			}
//
//			@Override
//			public File getDestination() {
//				return DESTDIR;
//			}
//		});


//		out.println("MemMap Copier");
//		copyDirectory(new Copier() {
//
//			@Override
//			public void run(File source) throws Exception {
//				copyFileWithMemMap(source, getDestination());
//			}
//
//			@Override
//			public File getDestination() {
//				return DESTDIRMEMMAP;
//			}
//		});

	}

	public void copyDirectory(Copier copier) throws IOException
	{
		long fullSize = Files.walk(Paths.get(DIRSRC))
				.filter(Files::isRegularFile)
				.filter(f-> {String file=f.toFile().getName();return file.endsWith(".all") || file.endsWith(".wcd");})
				.mapToLong(p -> p.toFile().length())
				.sum();
		out.format("size % d Mo" ,fullSize/1024/1024);
		out.println();
		//delete previous files
		Files.walk(Paths.get(copier.getDestination().toString()))
		.filter(Files::isRegularFile)
		.forEach((f)-> {
			f.toFile().delete();
		});
		long start = System.currentTimeMillis();

		Files.walk(Paths.get(DIRSRC))
		.filter(Files::isRegularFile)
		.forEach((f)->{
			String file = f.toString();
			if( file.endsWith(".all") || file.endsWith(".wcd"))
				try {
					copier.run(f.toFile());
				} catch (Exception e) {
					e.printStackTrace();
				}
		});
		long time=(System.currentTimeMillis()-start);
		String timeelapsed=new SimpleDateFormat("mm:ss:SSS").format(new Date(time));
		out.format("%d (sec) %s (min:sec:msec), rate %,f Mo/sec\n" ,time/1000,timeelapsed,fullSize/(time/1000.)/1024./1024.);

		//delete files
		Files.walk(Paths.get(copier.getDestination().toString()))
		.filter(Files::isRegularFile)
		.forEach((f)-> {
			f.toFile().delete();
		});

	}
	interface Copier
	{
		public void run(File source) throws Exception;
		public File getDestination();
	}
	public void copyWithSystemIO(File theFileSource, File theFileDest) throws Exception
	{
		Files.copy(theFileSource.toPath(), new File(theFileDest.toString()+"/"+theFileSource.getName()).toPath(),StandardCopyOption.REPLACE_EXISTING);
	}
	public void copyFileWithMemMap(File theFileSource, File theFileDest) throws Exception
	{

		RandomAccessFile raf = null;
		RandomAccessFile rafout=null;
		try {
			raf = new RandomAccessFile(theFileSource.getAbsolutePath(), "r");
			rafout = new RandomAccessFile(theFileDest.getAbsolutePath()+"//"+theFileSource.getName(), "rw");
			FileChannel channel = raf.getChannel();
			MappedByteBuffer buffer = channel.map(READ_ONLY, 0, Math.min(channel.size(), MAX_VALUE));
			buffer.order(ByteOrder.nativeOrder());
			byte[] sizeBu=new byte[4];
			ByteBuffer bufferSize=ByteBuffer.wrap(sizeBu);
			bufferSize.order(ByteOrder.nativeOrder());
			int dgcount=0;
			while(buffer.remaining()>0)
			{
				dgcount++;
				//read header

				buffer.get(sizeBu);
				bufferSize.rewind();
				int sizeRaw=bufferSize.getInt();

				long size=0x00000000FFFFFFFFl & (long)sizeRaw;
				//copy datagram lenght to a buffer
				Assert.assertTrue("size to big for file "+theFileSource + " (datagram "+dgcount+")" +size,size<MAX_VALUE);
				if(datagramBuffer.length<size)
				{
					datagramBuffer=new byte[(int)size];

				}
				buffer.get(datagramBuffer,0,(int)size);
				rafout.write(sizeBu);
				rafout.write(datagramBuffer, 0,(int) size);
				//				parseDatagram(datagram);
			}

			channel.close();
			raf.close();
			rafout.close();
		} catch (Exception e) {
			raf.close();
			rafout.close();
			throw e;
		}

	}
	public void copyFileWithRandomFile(File theFileSource, File theFileDest) throws Exception
	{

		RandomAccessFile raf = null;
		RandomAccessFile rafout=null;
		try {
			raf = new RandomAccessFile(theFileSource.getAbsolutePath(), "r");
			rafout = new RandomAccessFile(theFileDest.getAbsolutePath()+"//"+theFileSource.getName(), "rw");
			rafout.setLength(raf.length());
			byte[] sizeBu=new byte[4];
			ByteBuffer bufferSize=ByteBuffer.wrap(sizeBu);
			bufferSize.order(ByteOrder.nativeOrder());
			ByteBuffer dgByteBuffer=ByteBuffer.wrap(datagramBuffer);
			dgByteBuffer.order(ByteOrder.nativeOrder());

			long fileSize=theFileSource.length();
			int dgcount=0;
			while(raf.getFilePointer()<fileSize)
			{
				dgcount++;
				//read header

				raf.read(sizeBu);
				rafout.write(sizeBu);

				bufferSize.rewind();
				int sizeRaw=bufferSize.getInt();
				long size=0x00000000FFFFFFFFl & (long)sizeRaw;
				//copy datagram lenght to a buffer
				Assert.assertTrue("size to big for file "+theFileSource + " (datagram "+dgcount+")" +size,size<MAX_VALUE);

				raf.read(datagramBuffer,0,(int)size);
				rafout.write(datagramBuffer, 0,(int) size);
				//				parseDatagram(datagram);
			}

			raf.close();
			rafout.close();
		} catch (Exception e) {
			raf.close();
			rafout.close();
			throw e;
		}
		Assert.assertEquals("source and dest files differs ",theFileSource.length(),theFileSource.length());

	}
	public void copyFileWithRandomFileWithFixedBuffer(File theFileSource, File theFileDest) throws Exception
	{

		RandomAccessFile raf = null;
		RandomAccessFile rafout=null;
		try {
			raf = new RandomAccessFile(theFileSource.getAbsolutePath(), "r");
			rafout = new RandomAccessFile(theFileDest.getAbsolutePath()+"//"+theFileSource.getName(), "rw");
			//	rafout.setLength(raf.length());


			//	long fileSize=theFileSource.length();
			int read=0;
			while((read=raf.read(staticreadBuffer)) >0)
			{
				//read header
				rafout.write(staticreadBuffer,0,read);
			}

			raf.close();
			rafout.close();
		} catch (Exception e) {
			raf.close();
			rafout.close();
			throw e;
		}
		Assert.assertEquals("source and dest files differs ",theFileSource.length(),theFileSource.length());

	}
	static byte[] intB=new byte[4];

	long runTest(File file, long fileSize) throws Exception
	{
		long start = System.currentTimeMillis();
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(file.getAbsolutePath(), "r");
			FileChannel channel = raf.getChannel();
			MappedByteBuffer buffer = channel.map(READ_ONLY, 0, Math.min(channel.size(), MAX_VALUE));
			while(buffer.remaining()>0)
			{
				//read header
				long size=0x00000000FFFFFFFFl & (long)Integer.reverseBytes(buffer.getInt());
				//copy datagram lenght to a buffer
				byte[] datagramBuffer=new byte[(int)size];
				buffer.get(datagramBuffer);
				ByteBuffer datagram= ByteBuffer.wrap(datagramBuffer);

				parseDatagram(datagram);
			}

			channel.close();
			raf.close();
		} catch (Exception e) {
			raf.close();
			throw e;
		}

		return System.currentTimeMillis() - start;
	}

	private void parseDatagram(ByteBuffer datagram) {
		byte startSTX= datagram.get();
		Assert.assertTrue(startSTX==2);
		//byte type= datagram.get();
		//int checkSum=Short.toUnsignedInt(Short.reverseBytes(datagram.getShort((int)datagram.limit()-2)));
		byte endETX=datagram.get((int)datagram.limit()-3);
		Assert.assertTrue(endETX==3);
		//		System.err.println("size :"+datagram.limit() + 
		//				" datagram : "+ Integer.toHexString(type) + " checksum "+checkSum) ;

	}

	long readUInt(MappedByteBuffer buffer)
	{
		buffer.get(intB);
		return (intB[3] << 0 | intB[2] << 8 | intB[1] << 16 | intB[0] << 24) & 0xFFFFFFFFL;
	}
}
