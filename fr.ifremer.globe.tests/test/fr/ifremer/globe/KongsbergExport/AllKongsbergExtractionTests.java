package fr.ifremer.globe.KongsbergExport;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ KongsbergTestSoundVelocityExtraction.class })
public class AllKongsbergExtractionTests {

}
