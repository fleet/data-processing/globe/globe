package fr.ifremer.globe.KongsbergExport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.vel.info.VelocityConstants;
import fr.ifremer.globe.core.io.vel.info.VelocityVariables;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityService;
import fr.ifremer.globe.core.model.velocity.IVelWriter;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparator;
import fr.ifremer.globe.netcdf.util.comparator.NetcdfComparatorHook;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class KongsbergTestSoundVelocityExtraction {
	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(KongsbergTestSoundVelocityExtraction.class);

	// not useful; vel format changed % caraibes

	// Input reference file
	public static final String KONGSBERG_REF_INPUT_FILE_NAME = "0372_20140827_195125_AT_EM122";
	public static final String KONGSBERG_REF_INPUT_FILE = GlobeTestUtil.getTestDataPath() + "/file/Kongsberg/"
			+ KONGSBERG_REF_INPUT_FILE_NAME + ".all";

	// Output reference file
	public static final String SOUNDVELOCITY_REF_OUTPUT_FILE = GlobeTestUtil.getTestDataPath()
			// + "/file/Kongsberg/CARAIBES/0372.vel";
			+ "/file/Kongsberg/GLOBE-1.15.22/0372_20140827_195125_AT_EM122-fromOldConverter_to_updateAtt.vel";

	// Output directory
	public static final String SOUNDVELOCITY_TST_DIRECTORY = GlobeTestUtil.getTestDataPath() + "/file/generated";

	// Output file
	public static final String SOUNDVELOCITY_TST_OUTPUT_FILE = SOUNDVELOCITY_TST_DIRECTORY + "/"
			+ KONGSBERG_REF_INPUT_FILE_NAME + ".vel";

	// nbrLvel
	// private int nbLevel = 0;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Check data modified by the process
	 */
	@Test
	public void testSoundVelocityExtraction() throws Exception {
		StringBuilder resume = new StringBuilder("testConvertAllToVel result : \n");
		boolean testFailed = false;

		// Setup process
		List<String> fileNames = new ArrayList<>();
		fileNames.add(KONGSBERG_REF_INPUT_FILE);

		Optional<ISoundVelocityData> src = ISoundVelocityService.grab().getSoundVelocityData(KONGSBERG_REF_INPUT_FILE);
		Assert.assertTrue(src.isPresent());
		IVelWriter.grab().write(src.get(), SOUNDVELOCITY_TST_OUTPUT_FILE);

		// Check if file exists

		Assert.assertTrue(new File(SOUNDVELOCITY_REF_OUTPUT_FILE).exists());

		Assert.assertTrue(new File(SOUNDVELOCITY_TST_OUTPUT_FILE).exists());

		NetcdfComparatorHook hook = new NetcdfComparatorHook();

		// Variables:
		// Attributes values check
		// Data not check
		hook.ignoreVariable(VelocityVariables.histDate);
		hook.ignoreVariable(VelocityVariables.histCode);
		hook.ignoreVariable(VelocityVariables.histTime);
		hook.ignoreVariable(VelocityVariables.histAutor);
		hook.ignoreVariable(VelocityVariables.histModule);
		hook.ignoreVariable(VelocityVariables.histComment);

		// hook.ignoreVariable(VelocityVariables.time);

		hook.ignoreVariable(VelocityVariables.temperature); // 0.0 in GLOBE 1.15.2 (old converter) Default value with
															// new converter
		hook.ignoreVariable(VelocityVariables.salinity); // idem
		hook.ignoreVariable(VelocityVariables.absorption); // idem

		hook.ignoreGlobalAttribute(VelocityConstants.ProfileCounter); // short with old converter, int with new
																		// converter
		Optional<String> errors = NetcdfComparator.compareFiles(SOUNDVELOCITY_REF_OUTPUT_FILE,
				SOUNDVELOCITY_TST_OUTPUT_FILE, Optional.of(hook));

		if (errors.isPresent()) {
			testFailed = true;
			resume.append(SOUNDVELOCITY_TST_OUTPUT_FILE + " : FAILED \n");
			resume.append(errors.get() + "\n");
		} else {
			resume.append(SOUNDVELOCITY_TST_OUTPUT_FILE + " : OK \n");
		}

		LOGGER.debug(resume.toString());
		Assert.assertFalse(testFailed);

	}

}
