package fr.ifremer.globe.utils.signal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SignalUtilsTest {

	@Before
	public void setUp() {

	}

	/**
	 * Test Signal Utils package.
	 * 
	 * 
	 */
	@Test
	public void TestConversion() {
		for (int i = 1; i < 100; i++) {
			double reflectivity = -40 * Math.random();

			double naturalValue = SignalUtils.dBToEnergy(reflectivity);
			double computedReflectivity = SignalUtils.energyTodB(naturalValue);
			Assert.assertTrue(Math.abs(computedReflectivity - reflectivity) < 10e-5);
		}
		for (int i = 1; i < 100; i++) {
			double reflectivity = -40 * Math.random();

			double naturalValue = SignalUtils.dBToAmplitude(reflectivity);
			double computedReflectivity = SignalUtils.amplitudeTodB(naturalValue);
			Assert.assertTrue(Math.abs(computedReflectivity - reflectivity) < 10e-5);
		}
	}

}
