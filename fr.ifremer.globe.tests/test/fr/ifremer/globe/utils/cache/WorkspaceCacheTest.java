package fr.ifremer.globe.utils.cache;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.cache.WorkspaceCache.CacheEntity;
import fr.ifremer.globe.utils.exception.GIOException;

public class WorkspaceCacheTest {

	String testFilesGenerationRepository = "file" + File.separator + "generated" + File.separator;
	String workspaceRepository = WorkspaceCache.getCacheDirectory("UnitTest").getPath() + File.separator;

	/**
	 *  Use case
	 * @throws IOException
	 */
	@Test
	public void useCase() throws GIOException, IOException {
		// Generate input of test.
		List<File> unusedFiles = new ArrayList<File>();
		unusedFiles.add(new File(this.workspaceRepository + generateRandomString()));
		unusedFiles.add(new File(this.workspaceRepository + generateRandomString()));
		unusedFiles.add(new File(this.workspaceRepository + generateRandomString()));

		for(File unusedFile : unusedFiles) {
			unusedFile.mkdirs();
		}

		List<File> files = new ArrayList<File>();
		files.add(new File(generateRandomString()));
		files.add(new File(generateRandomString()));

		List<String> fileNames = new ArrayList<String>();
		for(int i=0; i<files.size(); i++) {
			fileNames.add(files.get(i).getPath());
		}

		byte[] parameters = generateRandomString().getBytes(StandardCharsets.UTF_8);

		String guidDirectory = WorkspaceCache.computeGuid(fileNames, parameters);

		// Test creation of cache repertory.
		WorkspaceCache.create(new File(guidDirectory), parameters, files);

		for(CacheEntity cacheEntity : WorkspaceCache.getCacheEntities()) {
			// Check if cache repository exists and if all input files are saved in a cache entity.
			Assert.assertTrue(cacheEntity.getGuidDirectory().exists());
			Assert.assertEquals("useCase() : wrong size for cacheEntities.", 1, WorkspaceCache.getCacheEntities().size());
			for(File file : files) {
				Assert.assertEquals("useCase() : no association between fileNames and new cache repertory.", true, cacheEntity.getFiles().contains(file));
			}
			Assert.assertEquals("useCase() : no parameter file in new cache repertory.", true, cacheEntity.getParametersFile().exists());
			Assert.assertEquals("useCase() : cache directory is wrong.", 0, guidDirectory.compareTo(WorkspaceCache.getCacheEntities().get(0).getGuidDirectory().getPath()));
		}

		// Check if only cache entities files are found.
		Assert.assertTrue(WorkspaceCache.findCacheEntityByGUID(new File(guidDirectory)) != null);
		for(File unusedFile : unusedFiles) {
			Assert.assertTrue(WorkspaceCache.findCacheEntityByGUID(unusedFile) == null);
		}

		// Check if all unused caches are found on askForUnusedCacheEntity() call.
		Assert.assertEquals("testAskForUnusedCacheEntity() : wrong size for unusedFiles.", unusedFiles.size(), WorkspaceCache.askForUnusedCacheEntity().size());
		// Check if there is no unused files after askForUnusedCacheEntity() call.
		WorkspaceCache.cleanUnusedCacheFiles();
		Assert.assertEquals("testAskForUnusedCacheEntity() : wrong size for unusedFiles.", 0, WorkspaceCache.askForUnusedCacheEntity().size());

		// Check if cache repository deletion succeed.
		Assert.assertTrue(WorkspaceCache.delete(new File(guidDirectory)));

		// Clean workspace cache repository when the test is done.
		cleanAfterTest();
	}


	/**
	 * Test {@link WorkspaceCache#create(java.io.File, byte[], java.util.List)}.
	 *
	 * @throws GIOException
	 */
	@Test
	public void testCreate() throws GIOException {
		// Generate input of test.
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(new File(this.testFilesGenerationRepository + "firstFileNames"));
		fileNames.add(new File(this.testFilesGenerationRepository + "secondFileNames"));
		byte[] parameters = "parameters".getBytes(StandardCharsets.UTF_8);
		File guidDirectory = new File(this.workspaceRepository + "guidDirectory");
		// Call method to test.
		WorkspaceCache.create(guidDirectory, parameters, fileNames);
		// Check if the cache directory exists.
		Assert.assertTrue(guidDirectory.exists());
		// Check number and content of the CacheEntity.
		Assert.assertEquals("testCreate() : wrong size for cacheEntities.", 1, WorkspaceCache.getCacheEntities().size());
		for(File fileName : fileNames) {
			Assert.assertEquals("testCreate() : no association between fileNames and new cache repertory.", true, WorkspaceCache.getCacheEntities().get(0).getFiles().contains(fileName));
		}
		Assert.assertEquals("testCreate() : no parameter file in new cache repertory.", true, WorkspaceCache.getCacheEntities().get(0).getParametersFile().exists());
		Assert.assertEquals("testCreate() : cache directory is wrong.", 0, guidDirectory.getPath().compareTo(WorkspaceCache.getCacheEntities().get(0).getGuidDirectory().getPath()));
		// Clean workspace cache repository when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link WorkspaceCache#create(java.io.File, byte[], java.util.List)}, case when user tries to create two identical cache files.
	 *
	 * @throws GIOException
	 */
	@Test(expected=GIOException.class)
	public void testCreateWithException() throws GIOException {
		// Generate input of test.
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(new File(this.testFilesGenerationRepository + "firstFileNames"));
		fileNames.add(new File(this.testFilesGenerationRepository + "secondFileNames"));
		byte[] parameters = "parameters".getBytes(StandardCharsets.UTF_8);
		File guidDirectory = new File(this.workspaceRepository + "guidDirectory");
		// Call method to test.
		WorkspaceCache.create(guidDirectory, parameters, fileNames);
		// Check if the cache directory exists.
		Assert.assertTrue(guidDirectory.exists());
		// Check number and content of the CacheEntity.
		Assert.assertEquals("testCreateWithException() : wrong size for cacheEntities.", 1, WorkspaceCache.getCacheEntities().size());
		for(File fileName : fileNames) {
			Assert.assertEquals("testCreateWithException() : no association between fileNames and new cache repertory.", true, WorkspaceCache.getCacheEntities().get(0).getFiles().contains(fileName));
		}
		Assert.assertEquals("testCreateWithException() : no parameter file in new cache repertory.", true, WorkspaceCache.getCacheEntities().get(0).getParametersFile().exists());
		Assert.assertEquals("testCreateWithException() : cache directory is wrong.", 0, guidDirectory.getPath().compareTo(WorkspaceCache.getCacheEntities().get(0).getGuidDirectory().getPath()));
		// Call method to test, should raise a GIOException.
		try {
			WorkspaceCache.create(guidDirectory, parameters, fileNames);
		} catch (GIOException e){
			cleanAfterTest();
			throw new GIOException("Can't create two identical workspace cache files.");
		}
		// Clean workspace cache repository when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link WorkspaceCache#askForUnusedCacheEntity()}.
	 *
	 * @throws GIOException
	 */
	@Test
	public void testAskForUnusedCacheEntity() throws GIOException {
		// Generate input of test : two files in use and one unused.
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(new File(this.testFilesGenerationRepository + generateRandomString()));
		byte[] parameters = generateRandomString().getBytes(StandardCharsets.UTF_8);
		File guidDirectory = new File(this.workspaceRepository + generateRandomString());
		WorkspaceCache.create(guidDirectory, parameters, fileNames);
		fileNames = new ArrayList<File>();
		fileNames.add(new File(this.testFilesGenerationRepository + generateRandomString()));
		parameters = generateRandomString().getBytes(StandardCharsets.UTF_8);
		guidDirectory = new File(this.workspaceRepository + generateRandomString());
		WorkspaceCache.create(guidDirectory, parameters, fileNames);
		File unusedFile = new File(this.workspaceRepository + "unusedFile");
		unusedFile.mkdirs();
		// Call method to test.
		List<File> unusedFiles = WorkspaceCache.askForUnusedCacheEntity();
		// Check if all unused caches are found.
		Assert.assertEquals("testAskForUnusedCacheEntity() : wrong size for unusedFiles.", 1, WorkspaceCache.askForUnusedCacheEntity().size());
		Assert.assertEquals("testAskForUnusedCacheEntity() : unusedFile is not in unusedFiles.", true, unusedFiles.contains(unusedFile));
		// Clean workspace cache repository when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link WorkspaceCache#cleanUnusedCacheFiles()}.
	 *
	 * @throws GIOException
	 */
	@Test
	public void testCleanUnusedCacheFiles() throws GIOException {
		// // Generate input of test : two files in use and one unused.
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(new File(generateRandomString()));
		byte[] parameters = generateRandomString().getBytes(StandardCharsets.UTF_8);
		File guidDirectory = new File(this.workspaceRepository + generateRandomString());
		WorkspaceCache.create(guidDirectory, parameters, fileNames);
		fileNames = new ArrayList<File>();
		fileNames.add(new File(generateRandomString()));
		parameters = generateRandomString().getBytes(StandardCharsets.UTF_8);
		guidDirectory = new File(this.workspaceRepository + generateRandomString());
		WorkspaceCache.create(guidDirectory, parameters, fileNames);
		File unusedFile = new File(this.workspaceRepository + "unusedFile");
		unusedFile.mkdirs();
		// Call method to test.
		WorkspaceCache.cleanUnusedCacheFiles();

		Assert.assertFalse(unusedFile.exists());
		for(CacheEntity CacheEntity : WorkspaceCache.getCacheEntities()) {
			Assert.assertTrue(CacheEntity.getGuidDirectory().exists());
		}
		// Clean workspace cache repository when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link WorkspaceCache#delete(java.io.File)}.
	 *
	 * @throws GIOException
	 */
	@Test
	public void testDelete() throws GIOException {
		// Generate input of test.
		List<File> guidDirectory = new ArrayList<File>();
		List<File> files = new ArrayList<File>();
		List<byte[]> parameters = new ArrayList<byte[]>();

		for(int i=0; i<6; i++) {
			guidDirectory.add(new File(this.workspaceRepository + generateRandomString()));
			files.add(new File(this.testFilesGenerationRepository + generateRandomString()));
			parameters.add(generateRandomString().getBytes(StandardCharsets.UTF_8));
			WorkspaceCache.create(guidDirectory.get(i), parameters.get(i), files.subList(i, i));
		}

		int cacheEntitiesSize = WorkspaceCache.getCacheEntities().size();

		while(WorkspaceCache.getCacheEntities().size()>0) {
			// Call method to test.
			WorkspaceCache.delete(WorkspaceCache.getCacheEntities().get(cacheEntitiesSize-1).getGuidDirectory());
			cacheEntitiesSize--;
			Assert.assertTrue(WorkspaceCache.getCacheEntities().size()==cacheEntitiesSize);
		}

		// Clean workspace cache repository when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link WorkspaceCache#findCacheEntityByGUID(java.io.File)}.
	 *
	 * @throws GIOException
	 */
	@Test
	public void testFindCacheEntityByGUID() throws GIOException {
		// Generate input of test.
		File guidDirectory = new File(this.workspaceRepository + "guidDirectory");
		List<File> files = new ArrayList<File>();
		files.add(new File(this.testFilesGenerationRepository + generateRandomString()));
		byte[] parameters = generateRandomString().getBytes(StandardCharsets.UTF_8);
		WorkspaceCache.create(guidDirectory, parameters, files.subList(0, 0));
		// Check if only cache entities files are found.
		Assert.assertTrue(WorkspaceCache.findCacheEntityByGUID(guidDirectory) != null);
		Assert.assertTrue(WorkspaceCache.findCacheEntityByGUID(guidDirectory).getGuidDirectory()==guidDirectory);
		guidDirectory = new File(this.workspaceRepository + "GuidDirectory");
		Assert.assertTrue(WorkspaceCache.findCacheEntityByGUID(guidDirectory) == null);
		// Clean workspace cache repository when the test is done.
		cleanAfterTest();
	}

	/**
	 * Utility function for cleaning workspace repository after a test.
	 */
	private void cleanAfterTest() {

		List<File> files = FileUtils.getSubDirectories(new File(this.testFilesGenerationRepository), false);
		for(File file : files) {
			if(file.isDirectory()) {
				FileUtils.deleteDir(file);
			} else {
				FileUtils.delete(file);
			}
		}

		int cacheEntitiesSize = WorkspaceCache.getCacheEntities().size();

		while(WorkspaceCache.getCacheEntities().size()>0) {
			if(cacheEntitiesSize>0) {
				WorkspaceCache.delete(WorkspaceCache.getCacheEntities().get(cacheEntitiesSize-1).getGuidDirectory());
			}
			cacheEntitiesSize--;
		}

		WorkspaceCache.cleanUnusedCacheFiles();
	}

	/**
	 * Utility function for random string generation in tests.
	 */
	private String generateRandomString() {

		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();

		for (int i = 0; i < 20; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}

		return sb.toString();
	}
}
