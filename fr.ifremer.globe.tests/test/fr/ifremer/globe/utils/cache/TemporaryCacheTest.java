package fr.ifremer.globe.utils.cache;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

public class TemporaryCacheTest {

	/**
	 *  Use case
	 * @throws GIOException
	 * @throws IOException
	 */
	@Test
	public void useCase() throws GIOException, IOException {

		// Generate input : one temporary file and one temporary directory.
		File file = TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX");
		File directory = TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX");

		// Check if the new file exists and is saved in filesInUse list.
		Assert.assertTrue(file.exists());
		Assert.assertTrue(TemporaryCache.isUsed(file));

		// Check if the new file exists, if it's a directory and if it's saved in filesInUse list.
		Assert.assertTrue(directory.exists());
		Assert.assertTrue(directory.isDirectory());
		Assert.assertTrue(TemporaryCache.isUsed(directory));

		// Check if file and directory do not exists after deleteAllFiles() call.
		TemporaryCache.deleteAllFiles("PREFIX", "SUFFIX");
		Assert.assertFalse(file.exists());
		Assert.assertFalse(TemporaryCache.isUsed(file));
		Assert.assertFalse(directory.exists());
		Assert.assertFalse(directory.isDirectory());
		Assert.assertFalse(TemporaryCache.isUsed(directory));

		// Generate input : one temporary file and one temporary directory.
		file = TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX");
		directory = TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX");

		// Check deletion of file with and without a lock.
		RandomAccessFile randomAccessFile = new RandomAccessFile(file.getAbsolutePath(), "rw");
		Assert.assertFalse(TemporaryCache.delete(file));
		randomAccessFile.close();
		Assert.assertTrue(TemporaryCache.delete(file));

		// Check deletion of directory.
		Assert.assertTrue(TemporaryCache.delete(directory));

		// Check if deletion of file and directory succeed.
		Assert.assertFalse(file.exists());
		Assert.assertFalse(TemporaryCache.isUsed(file));
		Assert.assertFalse(directory.exists());
		Assert.assertFalse(directory.isDirectory());
		Assert.assertFalse(TemporaryCache.isUsed(directory));

		// Clean undeletable files when the test is done.
		cleanAfterTest();

	}

	/**
	 * Test {@link TemporaryCache#createTemporaryFile(String, String)}.
	 */
	@Test
	public void testGetTemporaryFile() throws IOException {

		// Call method to test.
		File file = TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX");
		// Check if the new file exists and is saved in filesInUse list.
		Assert.assertTrue(file.exists());
		Assert.assertTrue(TemporaryCache.isUsed(file));
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link TemporaryCache#createTemporaryDirectory(String, String)}.
	 */
	@Test
	public void testGetTemporaryDirectory() throws IOException {

		// Call method to test.
		File file = TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX");
		// Check if the new file exists, if it's a directory and if it's saved in filesInUse list.
		Assert.assertTrue(file.exists());
		Assert.assertTrue(file.isDirectory());
		Assert.assertTrue(TemporaryCache.isUsed(file));
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link TemporaryCache#deleteAllFiles(String, String)}.
	 */
	@Test
	public void deleteAllFiles() throws IOException, GIOException {
		// Generate input of test.
		List<File> files = new ArrayList<File>();
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX"));
		// Call method to test.
		TemporaryCache.deleteAllFiles("PREFIX", "SUFFIX");
		// Test is all files are used.
		for(File file : files) {
			TemporaryCache.isUsed(file);
		}
		// Test if files exist.
		Assert.assertFalse(files.get(0).exists());
		Assert.assertTrue(files.get(1).exists());
		Assert.assertTrue(files.get(2).exists());
		Assert.assertTrue(files.get(3).exists());
		Assert.assertTrue(files.get(4).exists());
		Assert.assertTrue(files.get(5).exists());
		Assert.assertTrue(files.get(6).exists());
		Assert.assertFalse(files.get(7).exists());
		Assert.assertFalse(files.get(8).exists());
		Assert.assertTrue(files.get(9).exists());
		Assert.assertTrue(files.get(10).exists());
		Assert.assertTrue(files.get(11).exists());
		Assert.assertTrue(files.get(12).exists());
		Assert.assertTrue(files.get(13).exists());
		Assert.assertTrue(files.get(14).exists());
		Assert.assertFalse(files.get(15).exists());
		// Test if files are used.
		Assert.assertFalse(TemporaryCache.isUsed(files.get(0)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(1)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(2)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(3)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(4)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(5)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(6)));
		Assert.assertFalse(TemporaryCache.isUsed(files.get(7)));
		Assert.assertFalse(TemporaryCache.isUsed(files.get(8)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(9)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(10)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(11)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(12)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(13)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(14)));
		Assert.assertFalse(TemporaryCache.isUsed(files.get(15)));
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link TemporaryCache#delete(File)}.
	 */
	@Test
	public void testDeleteFile() throws IOException {
		// Generate input of test.
		File file = TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX");
		File directory = TemporaryCache.createTemporaryDirectory("FREPIX", "FUSSIX");
		// Lock file to test if its deletion failed.
		RandomAccessFile randomAccessFile = new RandomAccessFile(file.getAbsolutePath(), "rw");
		// Call method to test for a locked file.
		Assert.assertFalse(TemporaryCache.delete(file));
		// Check if the file exists and is saved in filesInUse list.
		Assert.assertTrue(file.exists());
		Assert.assertTrue(TemporaryCache.isUsed(file));
		// Unlock file.
		randomAccessFile.close();
		// Call method to test for an unlocked file.
		Assert.assertTrue(TemporaryCache.delete(file));
		// Check if the file exists and is saved in filesInUse list.
		Assert.assertFalse(file.exists());
		Assert.assertFalse(TemporaryCache.isUsed(file));
		// Call method to test for a directory.
		Assert.assertTrue(TemporaryCache.delete(directory));
		// Check if the file exists and is saved in filesInUse list.
		Assert.assertFalse(directory.exists());
		Assert.assertFalse(TemporaryCache.isUsed(directory));
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link TemporaryCache#deleteAllFiles(String, String)}.
	 */
	@Test
	public void testDeleteFiles() throws IOException {
		// Generate input of test.
		List<File> files = new ArrayList<File>();
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "FUSSIX"));
		List<File> directories = new ArrayList<File>();
		directories.add(TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX"));
		directories.add(TemporaryCache.createTemporaryDirectory("PREFIX", "FUSSIX"));
		directories.add(TemporaryCache.createTemporaryDirectory("FREPIX", "SUFFIX"));
		directories.add(TemporaryCache.createTemporaryDirectory("FREPIX", "FUSSIX"));
		// Loop for tests on files
		for(File file : files) {
			// Lock file to test if its deletion failed.
			RandomAccessFile randomAccessFile = new RandomAccessFile(file.getAbsolutePath(), "rw");
			// Call method to test for a locked file.
			Assert.assertFalse(TemporaryCache.delete(file));
			// Check if the file exists and is saved in filesInUse list.
			Assert.assertTrue(file.exists());
			Assert.assertTrue(TemporaryCache.isUsed(file));
			// Unlock file.
			randomAccessFile.close();
			// Call method to test for an unlocked file.
			Assert.assertTrue(TemporaryCache.delete(file));
			// Check if the file exists and is saved in filesInUse list.
			Assert.assertFalse(file.exists());
			Assert.assertFalse(TemporaryCache.isUsed(file));
		}
		// Loop for tests on directories
		for(File directory : directories) {
			// Call method to test for a directory.
			Assert.assertTrue(TemporaryCache.delete(directory));
			// Check if the file exists and is saved in filesInUse list.
			Assert.assertFalse(directory.exists());
			Assert.assertFalse(TemporaryCache.isUsed(directory));
		}
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link TemporaryCache#cleanAll()}.
	 */
	@Test
	public void testCleanAll() throws IOException {
		// Generate input of test.
		List<File> files = new ArrayList<File>();
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "FUSSIX"));
		File notGlobeFile = new File(System.getProperty("java.io.tmpdir") + "PREFIX_SUFFIX");
		notGlobeFile.mkdir();
		files.add(notGlobeFile);
		notGlobeFile = new File(System.getProperty("java.io.tmpdir") + "PREFIX_FUSSIX");
		notGlobeFile.mkdir();
		files.add(notGlobeFile);
		notGlobeFile = new File(System.getProperty("java.io.tmpdir") + "FREPIX_SUFFIX");
		notGlobeFile.mkdir();
		files.add(notGlobeFile);
		notGlobeFile = new File(System.getProperty("java.io.tmpdir") + "FREPIX_FUSSIX");
		notGlobeFile.mkdir();
		files.add(notGlobeFile);
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "FUSSIX"));
		// Call method to test.
		TemporaryCache.cleanFilesInUse();
		// Check if the files are not saved in filesInUse list.
		for(File file : files) {
			Assert.assertFalse(TemporaryCache.isUsed(file));
		}
		// Check if the files exist .
		Assert.assertFalse(files.get(0).exists());
		Assert.assertFalse(files.get(1).exists());
		Assert.assertFalse(files.get(2).exists());
		Assert.assertFalse(files.get(3).exists());
		Assert.assertTrue(files.get(4).exists());
		Assert.assertTrue(files.get(5).exists());
		Assert.assertTrue(files.get(6).exists());
		Assert.assertTrue(files.get(7).exists());
		Assert.assertFalse(files.get(8).exists());
		Assert.assertFalse(files.get(9).exists());
		Assert.assertFalse(files.get(10).exists());
		Assert.assertFalse(files.get(11).exists());
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link TemporaryCache#isUsed(File)}.
	 */
	@Test
	public void testIsUsed() throws IOException {
		// Generate input of test.
		List<File> files = new ArrayList<File>();
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryFile("FREPIX", "FUSSIX"));
		files.add(new File(System.getProperty("java.io.tmpdir") + "PREFIX_SUFFIX"));
		files.add(new File(System.getProperty("java.io.tmpdir") + "PREFIX_FUSSIX"));
		files.add(new File(System.getProperty("java.io.tmpdir") + "FREPIX_SUFFIX"));
		files.add(new File(System.getProperty("java.io.tmpdir") + "FREPIX_FUSSIX"));
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("PREFIX", "FUSSIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "SUFFIX"));
		files.add(TemporaryCache.createTemporaryDirectory("FREPIX", "FUSSIX"));
		// Call method to test.
		Assert.assertTrue(TemporaryCache.isUsed(files.get(0)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(1)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(2)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(3)));
		Assert.assertFalse(TemporaryCache.isUsed(files.get(4)));
		Assert.assertFalse(TemporaryCache.isUsed(files.get(5)));
		Assert.assertFalse(TemporaryCache.isUsed(files.get(6)));
		Assert.assertFalse(TemporaryCache.isUsed(files.get(7)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(8)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(9)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(10)));
		Assert.assertTrue(TemporaryCache.isUsed(files.get(11)));
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Test {@link TemporaryCache#getTemporaryCacheFiles()}.
	 */
	@Test
	public void testGetTemporaryCacheFiles() throws IOException {
		// Generate input of test.
		List<File> globeFiles = new ArrayList<File>();
		List<File> notGlobeFiles = new ArrayList<File>();
		File file = new File(System.getProperty("java.io.tmpdir") + "PREFIX_SUFFIX");
		file.mkdir();
		notGlobeFiles.add(file);
		file = new File(System.getProperty("java.io.tmpdir") + "PREFIX_FUSSIX");
		file.mkdir();
		notGlobeFiles.add(file);
		file = new File(System.getProperty("java.io.tmpdir") + "FREPIX_SUFFIX");
		file.mkdir();
		notGlobeFiles.add(file);
		file = new File(System.getProperty("java.io.tmpdir") + "FREPIX_FUSSIX");
		file.mkdir();
		notGlobeFiles.add(file);
		globeFiles.add(TemporaryCache.createTemporaryFile("PREFIX", "SUFFIX"));
		globeFiles.add(TemporaryCache.createTemporaryFile("PREFIX", "FUSSIX"));
		globeFiles.add(TemporaryCache.createTemporaryFile("FREPIX", "SUFFIX"));
		globeFiles.add(TemporaryCache.createTemporaryFile("FREPIX", "FUSSIX"));
		globeFiles.add(TemporaryCache.createTemporaryDirectory("PREFIX", "SUFFIX"));
		globeFiles.add(TemporaryCache.createTemporaryDirectory("PREFIX", "FUSSIX"));
		globeFiles.add(TemporaryCache.createTemporaryDirectory("FREPIX", "SUFFIX"));
		globeFiles.add(TemporaryCache.createTemporaryDirectory("FREPIX", "FUSSIX"));
		// Call method to test.
		List<File> temporaryFiles =TemporaryCache.getTemporaryCacheFiles();
		// Check if function under test returns a right number of files.
		Assert.assertEquals("testGetTemporaryCacheFiles() : files and temporaryFiles have not same size.", globeFiles.size(), temporaryFiles.size());
		// Check if function under test returns the same files as its input.
		for(File globeFile : globeFiles) {
			Assert.assertEquals("testGetTemporaryCacheFiles() : temporaryFile and globeFile have not the same path.", true, temporaryFiles.contains(globeFile));
		}
		// Clean undeletable files when the test is done.
		cleanAfterTest();
	}

	/**
	 * Utility function for cleaning temporary repository after a test.
	 */
	private void cleanAfterTest() {

		File[] files = new File(System.getProperty("java.io.tmpdir")).listFiles();

		for(File file : files) {
			if(file.getName().startsWith("GLOBE_") || file.getName().startsWith("PREFIX") || file.getName().startsWith("FREPIX")) {
				FileUtils.deleteDir(file);
			}
		}
	}
}
