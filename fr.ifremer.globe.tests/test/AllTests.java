import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.KongsbergExport.AllKongsbergExtractionTests;
import fr.ifremer.globe.utils.signal.SignalUtilsTest;

@RunWith(Suite.class)
@SuiteClasses({ SignalUtilsTest.class, AllKongsbergExtractionTests.class })
public class AllTests {

}
