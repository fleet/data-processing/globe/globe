/*******************************************************************************
 * Copyright (c) 2007 SAS Institute. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: SAS Institute - initial API and implementation
 ******************************************************************************/
package swingintegration.example;

import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.JApplet;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.MetalLookAndFeel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Platform {
	private static String platformString = SWT.getPlatform();
	private final static Logger logger = LoggerFactory.getLogger(Platform.class);

	static {



	}

	// prevent instantiation
	private Platform() {
	}

	public static void initLAF()
	{
		EventQueue.invokeLater(new Runnable()
		{




			@Override
			public void run() 
			{
				//freeze on mac, so disable it
				if(isLeopard())
					return;

				if (isGtk()) {
					try {
						UIManager.setLookAndFeel(new MetalLookAndFeel());
					} catch (UnsupportedLookAndFeelException e) {
						logger.error(e.getMessage(),e);

					}
					// System.out.println("Using Java L&F");
				} else {
					try {
						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					} catch (ClassNotFoundException
							| InstantiationException
							| IllegalAccessException
							| UnsupportedLookAndFeelException e) {
						logger.error(e.getMessage(),e);
					}
				}
			}}
				);
	}
	public static boolean isWin32() {
		return "win32".equals(platformString); //$NON-NLS-1$
	}

	public static boolean isGtk() {
		return "gtk".equals(platformString); //$NON-NLS-1$
	}
	public static boolean isMac()
	{
		return isLeopard();
	}


	private static boolean isSunJava6() {
		return System.getProperty("java.version").startsWith("1.6") && System.getProperty("java.vendor").startsWith("Sun ");
	}

	private static boolean isLeopard() {
		String os = System.getProperty("os.name").toLowerCase();
		//String version = System.getProperty("os.version");
		return os.startsWith("mac") ;

	}

	public static Composite createComposite(final Composite container, final Display display, final SwingComponentConstructor swingComponent) {
		if ((!isSunJava6() || !isGtk()) && !isLeopard()) {
			return createEmbeddedComposite(container, display, swingComponent);
		}
		return createBasicComposite(container, display, swingComponent);

	}

	public static Composite createBasicComposite(final Composite container, final Display display, final SwingComponentConstructor swingComponent) {
		Composite composite = new Composite(container, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		composite.setLayout(new FillLayout());
		Frame frame = SWT_AWT.new_Frame(composite);
		JApplet applet = new JApplet();
		applet.add(swingComponent.createSwingComponent());
		frame.add(applet);
		return composite;
	}

	public static Composite createEmbeddedComposite(final Composite container, final Display display, final SwingComponentConstructor swingComponent) {
		final EmbeddedSwingComposite a4Component = new EmbeddedSwingComposite(container, SWT.NONE) {

			@Override
			protected JComponent createSwingComponent() {
				JComponent c = swingComponent.createSwingComponent();
				synchronized (this) {
					notifyAll();
				}
				return c;
			}
		};

		try {
			display.syncExec(new Runnable() {
				@Override
				public void run() {
					a4Component.populate();
					try {
						synchronized (a4Component) {
							a4Component.wait();
						}
					} catch (InterruptedException e) {
						throw new SWTException(e.getMessage());
					}
				}
			});
		} catch (SWTException e) {
			logger.error(e.getMessage(),e);
		}
		return a4Component;
	}
}
