package fr.ifremer.globe.ui.undo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.e4.core.services.events.IEventBroker;

import fr.ifremer.globe.core.utils.undo.UndoAction;
import fr.ifremer.globe.ui.events.BaseEventTopics;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Main contextual stack.
 * 
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */
public class UndoStack {

	public static final int MAX_ACTION = 5;
	public static final int INFINITE_ACTION = -1;

	/** Eclipse event broker service */
	protected IEventBroker eventBroker = OsgiUtils.getService(IEventBroker.class);

	protected int currentIdx = 0;

	protected LinkedList<UndoAction> stack = new LinkedList<>();

	/** Maximum number of actions that can be put in stack */
	private int maxActions = INFINITE_ACTION;

	/** Owner of this stack */
	private Object owner;

	/**
	 * @see #getInstance(Object)
	 */
	private static Map<Object, UndoStack> instances = new HashMap<>();

	public UndoStack(Object owner, int maxActions) {
		this.owner = owner;
		this.maxActions = maxActions;
	}

	/**
	 * Returns an undo actions stack instance from a context. This context may typically be a view part or any GUI
	 * object which needs its own undo stack.
	 * 
	 * @param context context object (cannot be null)
	 */
	public static UndoStack createInstance(Object context, int maxActions) {
		if (context == null) {
			return null;
		}
		UndoStack stack = null;
		synchronized (instances) {
			stack = instances.get(context);
			if (stack == null) {
				stack = new UndoStack(context, maxActions);
				instances.put(context, stack);
			}
		}
		return stack;
	}

	public void push(UndoAction a) {
		// purge
		while (stack.size() > currentIdx) {
			UndoAction last = stack.getLast();
			last.dispose();
			stack.removeLast();
		}
		stack.add(a);
		currentIdx++;
		// "cut" the stack if necessary
		if (maxActions != -1 && stack.size() > maxActions) {
			UndoAction action = stack.remove(0);
			action.dispose();
			currentIdx--;
		}
		if (eventBroker != null) {
			eventBroker.post(BaseEventTopics.UNDO_STACK, new StackEvent());
		}
	}

	public void undo() {
		if (currentIdx > 0) {
			stack.get(currentIdx - 1).undo();
			currentIdx--;
		}
		if (eventBroker != null) {
			eventBroker.post(BaseEventTopics.UNDO_STACK, new StackEvent());
		}
	}

	public void redo() {
		if (currentIdx < stack.size()) {
			stack.get(currentIdx).redo();
			currentIdx++;
		}
		if (eventBroker != null) {
			eventBroker.post(BaseEventTopics.UNDO_STACK, new StackEvent());
		}
	}

	public UndoAction getCurrentUndoAction() {
		if (currentIdx > 0) {
			return stack.get(currentIdx - 1);
		}
		return null;
	}

	public UndoAction getNextUndoAction() {
		if (currentIdx < stack.size()) {
			return stack.get(currentIdx);
		}
		return null;
	}

	public UndoAction getPreviousUndoAction() {
		if (currentIdx - 2 >= 0) {
			return stack.get(currentIdx - 2);
		}
		return null;
	}

	/**
	 * Returns true if there isn't any action in the stack yet (ie. no action or all actions already cancelled)
	 */
	public boolean isEmpty() {
		if (currentIdx == 0 || stack.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true if all action in the stack already done (no redo action anymore).
	 */
	public boolean isDone() {
		if (currentIdx >= stack.size()) {
			return true;
		}
		return false;
	}

	public void clear() {
		for (UndoAction action : stack) {
			action.dispose();
		}
		stack.clear();
		currentIdx = 0;
	}

	/**
	 * return the current index, for test only
	 */
	public int getCurrentIndex() {
		return currentIdx;
	}

	public void dispose() {
		clear();
		instances.remove(owner);
	}

	public int size() {
		return stack.size();
	}
}
