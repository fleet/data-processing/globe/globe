package fr.ifremer.globe.ui;

import java.util.List;

import gov.nasa.worldwind.geom.Position;

public class NavLine extends ANavLine<Position> {
	public NavLine()
	{
		super();
	}
	public void addPoint(Position point) {
		super.addPoint(point);
	}
	public void addPoints(List<Position> points) {
		for (Position point : points) {
			addPoint(point);
		}
	}

}
