package fr.ifremer.globe.ui;

import fr.ifremer.globe.utils.date.DateUtils;
import gov.nasa.worldwind.geom.Position;

/**
 * A navigation line with time values
 * */
public class NavTimedLine extends ANavLine<TimedPosition> {

	public NavTimedLine()
	{
		super();
	}



	/**
	 * Add a points to the points collection.
	 *
	 * @param pointsElt
	 *            Element to add
	 *
	 */
	public void addPointTime(Position point, long time) {
		addPoint(new TimedPosition(point, time));
	}
	/**
	 * Add a points to the points collection.
	 *
	 * @param pointsElt
	 *            Element to add
	 *
	 */
	public void addPointTime(Position point, double date, double hour) {
		long timesElt = DateUtils.getEpochMilliFromJulian((int)Math.round(date), (int) Math.round(hour));
		// JulianDay.fromJulian(date, hour);
		addPoint(new TimedPosition(point, timesElt));
	}



	public void addPointTime(byte validity, Position point, long time) {
		addPoint(new TimedPosition(validity, point, time));
	}


}
