package fr.ifremer.globe.ui.algo;

import java.util.List;
import java.util.stream.Collectors;

import fr.ifremer.globe.utils.algo.DouglasPeucker;
import gov.nasa.worldwind.geom.Position;

public class DouglasPeuckerDecimationAlgorithm {

	/**
	 * Applies decimation with the Douglas Peucker algorithm
	 */
	public List<Position> decimate(int maxPoints, List<? extends Position> inputPoints) {

		List<double[]> inputPointsAsDouble = inputPoints.stream()
				.map(p -> new double[] { p.latitude.degrees, p.longitude.degrees, p.elevation })
				.filter(x -> !Double.isNaN(x[0]) && !Double.isNaN(x[1])).collect(Collectors.toList());

		List<double[]> resultPointsAsDouble = DouglasPeucker.performRecursivly(inputPointsAsDouble, 1e-6, maxPoints);

		return resultPointsAsDouble.stream()
				.map(doubleArray -> Position.fromDegrees(doubleArray[0], doubleArray[1], doubleArray[2]))
				.collect(Collectors.toList());
	}

}
