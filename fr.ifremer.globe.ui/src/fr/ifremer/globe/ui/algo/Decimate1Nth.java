package fr.ifremer.globe.ui.algo;

import gov.nasa.worldwind.geom.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * this is the simpliest algorithm for decimation. Given set of point it will
 * return a subsample of it given the expected number of points and ensuring
 * that first and last points are kept
 * 
 * */
public class Decimate1Nth {

	public static List<Position>  decimate(int maxPoints, List<? extends Position> _points) {

		int pas = _points.size() / maxPoints;
		List<Position> points = new ArrayList<Position>(maxPoints);
		for (int i = 0; i < maxPoints; i++) {
			points.add(_points.get(i * pas));
		}
		// add last navigation point
		Position lastPosition = _points.get(_points.size() - 1);
		if (!points.contains(lastPosition)) {
			points.remove(points.size() - 1);
			points.add(lastPosition);
		}
		return points;
	}
}
