package fr.ifremer.globe.ui.algo;

import gov.nasa.worldwind.geom.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * this is the simpliest algorithm for decimation. Given set of point it will
 * return a subsample of it given the expected number of points and ensuring
 * that first and last points are kept
 * 
 * */
public class DecimateNone {

	public static List<Position>  decimate(List<? extends Position> _points) {

		
		List<Position> points = new ArrayList<Position>(_points.size());
		points.addAll(_points);
		return points;
	}
}
