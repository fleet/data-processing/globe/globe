package fr.ifremer.globe.ui.utils;

import java.util.Locale;

import org.eclipse.nebula.widgets.formattedtext.DoubleFormatter;

/**
 * DoubleFormatter used to display and edit double values in meters
 */
public class SuffixedDoubleFormatter extends DoubleFormatter {

	/**
	 * Constructor
	 */
	public SuffixedDoubleFormatter(String suffix) {
		this("-############0.###", suffix);
	}

	/**
	 * Constructor
	 */
	public SuffixedDoubleFormatter(String editPattern, String suffix) {
		super(editPattern, Locale.US);
		setSuffix(suffix);
	}

	@Override
	protected void clearText(int start, int len) {
		// Don't accept to delete the suffix
		if (start + len <= editValue.length() - suffixLen) {
			super.clearText(start, len);
		}
	}
}
