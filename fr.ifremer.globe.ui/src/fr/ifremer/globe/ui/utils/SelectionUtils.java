package fr.ifremer.globe.ui.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;

public class SelectionUtils {

	private SelectionUtils() {
		// utility class : only static methods
	}

	// FROM SELECTION OBJECT (StructuredSelection)

	public static Stream<Object> stream(StructuredSelection structuredSelection) {
		return Arrays.stream(structuredSelection.toArray());
	}

	public static <T> List<T> getSelection(StructuredSelection structuredSelection, Class<T> expectedClass) {
		return stream(structuredSelection).filter(expectedClass::isInstance).map(expectedClass::cast).toList();
	}

	public static <T> List<T> getSelection(StructuredSelection structuredSelection, Class<T> expectedClass,
			Predicate<T> predicate) {
		return stream(structuredSelection).filter(expectedClass::isInstance).map(expectedClass::cast)
				.filter(predicate::test).toList();
	}

	public static <T> Optional<T> getUniqueSelection(StructuredSelection structuredSelection, Class<T> expectedClass) {
		return stream(structuredSelection).filter(expectedClass::isInstance).map(expectedClass::cast).findFirst();
	}

	public static <T> Optional<T> getUniqueSelection(StructuredSelection structuredSelection, Class<T> expectedClass,
			Predicate<T> predicate) {
		return stream(structuredSelection).filter(expectedClass::isInstance).map(expectedClass::cast)
				.filter(predicate::test).findFirst();
	}

	// FROM SELECTION SERVICE

	private static Stream<Object> streamSelection(ESelectionService selectionService) {
		return selectionService.getSelection() instanceof StructuredSelection structuredSelection
				? Arrays.stream(structuredSelection.toArray())
				: Stream.empty();
	}

	public static <T> List<T> getSelection(ESelectionService selectionService, Class<T> expectedClass) {
		return streamSelection(selectionService).filter(expectedClass::isInstance).map(expectedClass::cast).toList();
	}

	public static <T> List<T> getSelection(ESelectionService selectionService, Class<T> expectedClass,
			Predicate<T> predicate) {
		return streamSelection(selectionService).filter(expectedClass::isInstance).map(expectedClass::cast)
				.filter(predicate::test).toList();
	}

	public static <T> Optional<T> getUniqueSelection(ESelectionService selectionService, Class<T> expectedClass) {
		return streamSelection(selectionService).filter(expectedClass::isInstance).map(expectedClass::cast).findFirst();
	}

	public static <T> Optional<T> getUniqueSelection(ESelectionService selectionService, Class<T> expectedClass,
			Predicate<T> predicate) {
		return streamSelection(selectionService).filter(expectedClass::isInstance).map(expectedClass::cast)
				.filter(predicate::test).findFirst();
	}

}
