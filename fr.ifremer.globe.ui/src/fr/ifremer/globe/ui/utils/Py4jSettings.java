package fr.ifremer.globe.ui.utils;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;

public class Py4jSettings extends PreferenceComposite {
	public enum Py4jStats{
		Unknown,
		Yes,
		No
	}

	private EnumPreferenceAttribute usePy4j;

	public EnumPreferenceAttribute getUsePy4j() {
		return usePy4j;
	}

	public void setSendAuth(EnumPreferenceAttribute usePy4j) {
		this.usePy4j = usePy4j;
	}

	public Py4jSettings(PreferenceComposite father) {
		super(father, "Usage Py4j");

		usePy4j = new EnumPreferenceAttribute("usePy4j", "Use Py4j for Python scripts", Py4jStats.Yes);
		this.declareAttribute(usePy4j);
		this.load();
	}

}
