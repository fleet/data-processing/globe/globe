package fr.ifremer.globe.ui.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.utils.cache.NasaCache;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.cache.FileStore;

public class CacheDirectory {
	private static final Logger logger = LoggerFactory.getLogger(CacheDirectory.class);

	public static String getCacheSubDirectory() {
		return NasaCache.GLOBE_CACHE;
	}

	/**
	 * 
	 * Delete the entries in World Wind cache related to one file
	 * 
	 * 
	 * @param file
	 *            the original file
	 */
	public static void deleteInWWCache(File file) {
		logger.info("Deleting file " + file + " in NW cache");
		// delete entries in World Wind cache
		FileStore fileStore = WorldWind.getDataFileStore();

		// get all the cache locations
		List<? extends File> cacheLocations = fileStore.getLocations();
		for (File cache : cacheLocations) {
			if (cache.exists()) {
				try {
					// get the globe directory in the cache
					File tmpCache = new File(cache.getAbsoluteFile() + File.separator + CacheDirectory.getCacheSubDirectory());

					// PathMatcher of all entries of expression
					// */CACHE/<filename>*
					PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/" + CacheDirectory.getCacheSubDirectory() + "/" + file.getName().toString() + "**");

					if (tmpCache.exists()) {
						File[] listOfFiles = tmpCache.listFiles();

						// for all files in <cache>/tmp
						for (int fc = 0; fc < listOfFiles.length; fc++) {
							// if matches to the pattern, delete
							if (matcher.matches(Paths.get(listOfFiles[fc].getAbsolutePath()))) {
								File toDelete = new File(listOfFiles[fc].getAbsolutePath());

								if (toDelete.isDirectory()) {
									FileUtils.deleteDirectory(toDelete);
								} else {
									toDelete.delete();
								}

							}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
