package fr.ifremer.globe.ui.utils.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import fr.ifremer.globe.utils.FileUtils;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.cache.FileStore;
import gov.nasa.worldwind.util.WWIO;

/**
 * Manage files in C:\ProgramData\WorldWindData\globe_data and C:\ProgramData\WorldWindData\Earth.
 */
public class NasaCache {

	public static final String GLOBE_CACHE = "globe_data_v2";
	public static final int UP_TO_DATE = 1;
	public static final int TO_UPDATE = 0;

	private static final Logger logger = LoggerFactory.getLogger(NasaCache.class);

	private static String historyXmlFileName = "history.xml";

	private static NasaCache instance;

	/**
	 * Object which links a cache file with its associated directory.
	 */
	public class CacheContext {

		private String cacheDirectory;
		private File xmlParametersFile;
		private File xmlHistoryFile;

		public CacheContext(File sourceFile, File fileStoreLocation, String fileName) {

			this.cacheDirectory = NasaCache.getCacheDirectory(sourceFile.getName(), "_elevation");
			this.xmlParametersFile = new File(fileStoreLocation.getPath() + File.separator + this.cacheDirectory
					+ File.separator + fileName + ".xml");
			this.xmlHistoryFile = new File(fileStoreLocation.getPath() + File.separator + this.cacheDirectory
					+ File.separator + historyXmlFileName);
		}

		public String getCacheDirectory() {
			return this.cacheDirectory;
		}

		public File getXmlParametersFile() {
			return this.xmlParametersFile;
		}

		public File getXmlHistoryFile() {
			if (!this.xmlHistoryFile.exists()) {
				this.xmlHistoryFile.getParentFile().mkdirs();
			}
			return this.xmlHistoryFile;
		}
	}

	/**
	 * Create cache file and its associated directory in a {@link CacheContext} object.
	 *
	 * @param sourcefile : file used by Globe
	 * @param fileStoreLocation : cache directory
	 * @param fileName : file name of the imported file
	 */
	public static synchronized CacheContext create(File sourceFile, File fileStoreLocation, String fileName) {

		CacheContext cacheContext = NasaCache.getInstance().new CacheContext(sourceFile, fileStoreLocation, fileName);
		return cacheContext;
	}

	/**
	 * Generate a .xml file which contains the last modification date of a Globe file.
	 *
	 * @param sourcefile : file used by Globe
	 * @param fileName : location of the cache associated to sourcefile
	 */
	public static void createHistoryXMLFile(CacheContext cacheContext, File sourcefile) {

		try {
			XStream xstream = new XStream(new DomDriver());
			File file = cacheContext.getXmlHistoryFile();
			FileOutputStream fileOutputStream = new FileOutputStream(file);

			try {
				xstream.toXML(sourcefile.lastModified(), fileOutputStream);
			} finally {
				fileOutputStream.close();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Returns true if the cache is up to date compared to tmpFile. Verification based on lastModified().
	 *
	 * @param sourceFile : directory of current cache
	 * @param tmpFile : temporary file to compare with sourceFile
	 */
	public static int askForRecomputeCache(File sourceFile, CacheContext cacheContext) {

		long creationDate = 0;

		if (!cacheContext.getXmlParametersFile().exists()) {
			return TO_UPDATE;
		}

		try {
			XStream xstream = new XStream(new DomDriver());

			if (!getHistoryFileByParametersFilePath(cacheContext.getXmlHistoryFile()).exists()) {
				createHistoryXMLFile(cacheContext, sourceFile);
				return TO_UPDATE;
			}
			FileInputStream fileInputStream = new FileInputStream(
					getHistoryFileByParametersFilePath(cacheContext.getXmlHistoryFile()));

			try {
				creationDate = (long) xstream.fromXML(fileInputStream);
			} finally {
				fileInputStream.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		if (sourceFile.lastModified() <= creationDate) {
			return UP_TO_DATE;
		} else {
			return TO_UPDATE;
		}
	}

	/**
	 * Returns true if the cache doesn't exist.
	 *
	 * @param cacheFile : cache file
	 */
	public static boolean cacheIsNull(File cacheFile) {
		if (!cacheFile.exists()) {
			return true;
		}

		File cacheParentDirectory = cacheFile.getParentFile();

		for (String child : cacheParentDirectory.list()) {
			// check if at least one data directory exists
			if ("0".equals(child)) {
				return false;
			}
		}
		return true;
	}

	public static boolean deleteXml(File file) {
		var result = new AtomicBoolean();

		// PathMatcher of all entries of expression */CACHE/<filename>*
		PathMatcher matcher = FileSystems.getDefault()
				.getPathMatcher("glob:**/" + getCacheSubDirectory() + "/" + file.getName().toString() + "**");

		WorldWind.getDataFileStore().getLocations().stream()//
				.filter(File::exists).map(f -> new File(f.getAbsoluteFile() + File.separator + getCacheSubDirectory()))//
				.filter(File::exists).flatMap(f -> Stream.of(f.listFiles()))//
				.filter(f -> matcher.matches(Paths.get(f.getAbsolutePath())))//
				.flatMap(f -> {
					try {
						return Files.list(f.toPath());
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				})//
				.map(Path::toFile)//
				.filter(f -> FileUtils.getExtension(f).equals("xml"))//
				.forEach(f -> result.set(FileUtils.delete(f)));
		return result.get();
	}

	/**
	 * Remove one file in workspace cache directory.
	 *
	 * @return true if deletion succeed
	 */
	public static boolean delete(File file) {
		boolean result = false;
		if (file != null) {
			logger.info("Deleting file " + file + " in Nasa cache");
			// Delete entries in World Wind cache
			FileStore fileStore = WorldWind.getDataFileStore();

			// Get all the cache locations
			List<? extends File> cacheLocations = fileStore.getLocations();
			for (File cache : cacheLocations) {
				if (cache.exists()) {
					// Get the globe directory in the cache
					File tmpCache = new File(cache.getAbsoluteFile() + File.separator + getCacheSubDirectory());

					// PathMatcher of all entries of expression */CACHE/<filename>*
					PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
							"glob:**/" + getCacheSubDirectory() + "/" + file.getName().toString() + "**");

					if (tmpCache.exists()) {
						File[] listOfFiles = tmpCache.listFiles();

						result = true;
						// For all files in <cache>/tmp
						for (int fc = 0; fc < listOfFiles.length; fc++) {
							// If matches to the pattern, delete it.
							if (matcher.matches(Paths.get(listOfFiles[fc].getAbsolutePath()))) {
								// result = false if one delete failed
								result = !FileUtils.deleteDir(new File(listOfFiles[fc].getAbsolutePath())) ? false
										: result;
							}
						}
					}
				}
			}
		}

		return result;
	}

	/**
	 * Singleton pattern for NasaCache.
	 *
	 * @return NasaCache singleton
	 */
	public static NasaCache getInstance() {
		if (instance == null) {
			instance = new NasaCache();
		}
		return instance;
	}

	/**
	 * Returns a sub-directory of cache.
	 *
	 * @return {@link #GLOBE_CACHE}
	 */
	public static String getCacheSubDirectory() {
		return GLOBE_CACHE;
	}

	/**
	 * Returns root path of nasa cache.
	 */
	public static String getRootPath() {
		return WorldWind.getDataFileStore().getLocations().get(0) + File.separator + getCacheSubDirectory()
				+ File.separator;
	}

	/**
	 * @return "Earth" directory path (cache of background nodes)
	 */
	public static String getEarthCachePath() {
		return WorldWind.getDataFileStore().getLocations().get(0) + File.separator + "Earth" + File.separator;
	}

	/**
	 * Returns the cache directory based on its display name and a suffix.
	 *
	 * @param displayName : display name of the imported file
	 * @param suffix : termination of cache file
	 * @return the cache directory based on its display name and a suffix
	 */
	public static String getCacheDirectory(String displayName, String suffix) {
		return getCacheDirectory(displayName, suffix, "");
	}

	/**
	 * Returns the cache directory based on its display name, a suffix and a type.
	 *
	 * @param displayName : display name of the imported file
	 * @param suffix : termination of cache file
	 * @param type : type of file (eg: DEPTH)
	 * @return the cache directory based on its display name, a suffix and a type
	 */
	public static String getCacheDirectory(String displayName, String suffix, String type) {
		return getCacheSubDirectory() + "/" + WWIO.replaceIllegalFileNameCharacters(displayName) + suffix + type;
	}

	/**
	 * Returns the history xml file with its attached parameters file as input.
	 *
	 * @param xmlParametersFile : parameters xml file attached to history xml file
	 * @return a history xml file
	 */
	public static File getHistoryFileByParametersFilePath(File xmlParametersFile) {
		if (!xmlParametersFile.getParentFile().exists()) {
			xmlParametersFile.getParentFile().mkdirs();
		}
		return new File(xmlParametersFile.getParent() + File.separator + historyXmlFileName);
	}
}
