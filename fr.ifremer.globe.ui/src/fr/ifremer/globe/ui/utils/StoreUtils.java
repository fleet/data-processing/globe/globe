package fr.ifremer.globe.ui.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

public class StoreUtils {

	private StoreUtils() {
	}

	public static List<String> getFilePath(List<? extends IFileInfo> stores) {
		ArrayList<String> filenames = new ArrayList<>();
		for (IFileInfo info : stores) {
			filenames.add(info.getPath());
		}
		return filenames;
	}

	public static List<File> getFiles(List<? extends IFileInfo> stores) {
		ArrayList<File> files = new ArrayList<>();
		for (IFileInfo info : stores) {
			files.add(new File(info.getPath()));
		}
		return files;
	}

	public static List<IFileInfo> getInfoStoresFromNodes(List<FileInfoNode> stores) {
		List<IFileInfo> result = new ArrayList<>();
		for (FileInfoNode node : stores) {

			IFileInfo store = node.getFileInfo();
			if (store != null) {
				result.add(store);
			}
		}
		return result;
	}

	/**
	 * Get a {@link ISounderNcInfo} list from a list of {@link FileInfoNode}
	 */
	public static List<ISounderNcInfo> getISounderNcInfoFromNodes(List<FileInfoNode> stores) {
		List<ISounderNcInfo> result = new ArrayList<>();
		for (FileInfoNode node : stores) {
			IFileInfo store = node.getFileInfo();
			if (store instanceof ISounderNcInfo) {
				result.add((ISounderNcInfo) store);
			}
		}
		return result;
	}

}
