package fr.ifremer.globe.ui.utils;

import org.eclipse.nebula.widgets.formattedtext.MaskFormatter;

import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.utils.number.NumberUtils;

public class DegMinMaskFormatter extends MaskFormatter {
	enum CoordinateType {
		LATITUDE,
		LONGITUDE
	}

	private CoordinateType type;
	private ILatLongFormatter formatter;

	public DegMinMaskFormatter(String pattern, CoordinateType type, ILatLongFormatter formatter) {
		super(pattern);
		this.type = type;
		this.formatter = formatter;
	}

	@Override
	public void setValue(Object value) {
		double doubleValue = Double.NaN;
		if(value instanceof Number numberValue)
			doubleValue = numberValue.doubleValue();
		else if(value instanceof String stringValue)
			doubleValue = org.apache.commons.lang3.math.NumberUtils.toDouble(value.toString().replace(",", "."), Double.NaN);
		if (Double.isFinite(doubleValue) ) {
			if (type == CoordinateType.LATITUDE) {
				super.setValue(formatter.formatLatitude(NumberUtils.normalizedDegreesLatitude(doubleValue)));
			} else {
				super.setValue(formatter.formatLongitude(NumberUtils.normalizedDegreesLongitude(doubleValue)));
			}
		}
	}

	@Override
	public Object getValue() {
		return type == CoordinateType.LATITUDE ? formatter.parseLatitude(super.getDisplayString())
				: formatter.parseLongitude(super.getDisplayString());
	}

}
