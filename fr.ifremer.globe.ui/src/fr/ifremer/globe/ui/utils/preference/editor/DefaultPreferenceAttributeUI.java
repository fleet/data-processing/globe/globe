package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.utils.preference.PreferenceAttributeUI;

public class DefaultPreferenceAttributeUI<T> implements PreferenceAttributeUI<T> {

	private PreferenceAttribute<T> attribute;
	private Text widget;

	public DefaultPreferenceAttributeUI(Composite parent, PreferenceAttribute<T> attribute, boolean editable) {
		this.attribute = attribute;
		widget = new Text(parent, SWT.NONE);
		widget.setText(attribute.getValue().toString());
		widget.setEnabled(editable);
	}

	@Override
	public Control getWidgetControl() {
		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValueAsString(widget.getText(), true);
	}

	@Override
	public PreferenceAttribute<T> getAttribute() {
		return attribute;
	}

	@Override
	public void updateWidgetAttribute() {
		widget.setText(attribute.getValue().toString());
		
	}

}
