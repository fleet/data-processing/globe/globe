package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

public class IntegerPreferenceAttributeUI extends AbstractPreferenceAttributeUI<Integer> {

	private Spinner widget;

	public IntegerPreferenceAttributeUI(Composite parent, PreferenceAttribute<Integer> attribute, boolean editable) {
		super(attribute);
		widget = new Spinner(parent, SWT.NONE);
		widget.setEnabled(editable);
		widget.setMinimum(Integer.MIN_VALUE);
		widget.setMaximum(Integer.MAX_VALUE);
		widget.setSelection((Integer) attribute.getValue());
		widget.setIncrement(1);
		widget.setPageIncrement(100);

	}

	@Override
	public Control getWidgetControl() {

		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValue(widget.getSelection(), true);
	}

	@Override
	public void updateWidgetAttribute() {
		widget.setSelection((Integer) attribute.getValue());
		
	}

}
