/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

/** UI widget to edit a number as an int or float, depending of the default value type */
public class NumberPreferenceAttributeUI extends AbstractPreferenceAttributeUI<Number> {

	/** UI control */
	protected Spinner widget;

	/** Constructor */
	public NumberPreferenceAttributeUI(Composite parent, PreferenceAttribute<Number> attribute, boolean editable) {
		super(attribute);
		widget = new Spinner(parent, SWT.NONE);
		widget.setEnabled(editable);

		if (attribute.getDefaultValue() instanceof Integer) {
			widget.setIncrement(1);
			widget.setPageIncrement(100);
		} else {
			widget.setIncrement(100);
			widget.setPageIncrement(1000);
			widget.setDigits(2);
		}
		widget.setMinimum(Integer.MIN_VALUE);
		widget.setMaximum(Integer.MAX_VALUE);
		updateWidgetAttribute();
	}

	/** {@inheritDoc} */
	@Override
	public Control getWidgetControl() {
		return widget;
	}

	/** {@inheritDoc} */
	@Override
	public void updatePreferenceAttribute() {
		if (attribute.getDefaultValue() instanceof Integer) {
			attribute.setValue(widget.getSelection(), true);
		} else {
			attribute.setValue(widget.getSelection() / 100d, true);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void updateWidgetAttribute() {
		if (attribute.getDefaultValue() instanceof Integer) {
			widget.setSelection(attribute.getValue().intValue());
		} else {
			widget.setSelection((int) (attribute.getValue().doubleValue() * 100));
		}
	}
}
