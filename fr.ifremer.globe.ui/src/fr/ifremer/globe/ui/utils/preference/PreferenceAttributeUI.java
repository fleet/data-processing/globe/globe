package fr.ifremer.globe.ui.utils.preference;

import org.eclipse.swt.widgets.Control;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

/**
 * interface used to represent a preference attribute.
 * 
 * @param <T>
 *            Attribute type.
 * @author mguillem
 */
public interface PreferenceAttributeUI<T> {

	/**
	 * return the widget Control of the PreferenceAttributeUI
	 * 
	 * @return
	 */
	Control getWidgetControl();

	/**
	 * update the PreferenceAttribute with GUI value (in case of edition)
	 */
	void updatePreferenceAttribute();

	/**
	 * update the GUI with PreferenceAttribute value (in case of reset or external modification)
	 */
	void updateWidgetAttribute();

	/**
	 * @return The underlying attribute.
	 */
	PreferenceAttribute<T> getAttribute();
}
