package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import swing2swt.layout.BorderLayout;

public class ColorPreferenceAttributeUI extends AbstractPreferenceAttributeUI<GColor> {

	protected static Logger logger = LoggerFactory.getLogger(ColorPreferenceAttributeUI.class);

	private Composite widget;
	private Button choose;

	public ColorPreferenceAttributeUI(Composite parent, PreferenceAttribute<GColor> attribute, boolean editable) {
		super(attribute);
		widget = new Composite(parent, SWT.NONE);
		widget.setLayout(new BorderLayout(0, 0));
		updateWidgetAttribute();

		choose = new Button(widget, SWT.FLAT);
		choose.setEnabled(editable);
		choose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ColorDialog dialog = new ColorDialog(widget.getShell());
				RGB color = dialog.open();
				if (color != null) {
					widget.setBackground(SWTResourceManager.getColor(color));
				}
			}
		});
		choose.setText("...");
		choose.setLayoutData(BorderLayout.EAST);
	}

	@Override
	public Control getWidgetControl() {
		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValue(ColorUtils.convertSWTToGColor(widget.getBackground()), true);
	}

	@Override
	public void updateWidgetAttribute() {
		widget.setBackground(ColorUtils.convertGColorToSWT(((ColorPreferenceAttribute) attribute).getValue()));

	}

}
