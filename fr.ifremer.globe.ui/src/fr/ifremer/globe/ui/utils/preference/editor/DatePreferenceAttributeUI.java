package fr.ifremer.globe.ui.utils.preference.editor;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

public class DatePreferenceAttributeUI extends AbstractPreferenceAttributeUI<Date> {

	private Composite widget;
	private DateTime date, time;

	public DatePreferenceAttributeUI(Composite parent, PreferenceAttribute<Date> attribute, boolean editable) {
		super(attribute);
		widget = new Composite(parent, SWT.NONE);
		widget.setLayout(new FillLayout());
		date = new DateTime(widget, SWT.DATE);
		time = new DateTime(widget, SWT.TIME);
		date.setEnabled(editable);
		time.setEnabled(editable);

		updateWidgetAttribute();
		}

	@Override
	public Control getWidgetControl() {

		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(date.getYear(), date.getMonth(), date.getDay(), time.getHours(), time.getMinutes());
		attribute.setValue(calendar.getTime(), true);
	}

	@Override
	public void updateWidgetAttribute() {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime((Date) attribute.getValue());
		date.setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		time.setTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));		
	}

}
