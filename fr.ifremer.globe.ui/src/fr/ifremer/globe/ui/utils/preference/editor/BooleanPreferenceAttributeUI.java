package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

public class BooleanPreferenceAttributeUI extends AbstractPreferenceAttributeUI<Boolean> {

	private Button widget;

	public BooleanPreferenceAttributeUI(Composite parent, PreferenceAttribute<Boolean> attribute, boolean editable) {
		super(attribute);
		widget = new Button(parent, SWT.CHECK);
		widget.pack();
		widget.setEnabled(editable);
		updateWidgetAttribute();
	}

	@Override
	public Control getWidgetControl() {
		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValue(widget.getSelection(), true);
	}

	@Override
	public void updateWidgetAttribute() {
		widget.setSelection((Boolean) attribute.getValue());
		
	}

}
