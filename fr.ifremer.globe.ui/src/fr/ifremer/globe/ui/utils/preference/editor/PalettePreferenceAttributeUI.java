package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.nebula.jface.tablecomboviewer.TableComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.utils.color.ColorMap;

/** SWT widget to select a palette of color among ColorMap ones */
public class PalettePreferenceAttributeUI extends AbstractPreferenceAttributeUI<String> {

	/** TableComboViewer displaying a list of palette names and a small colored representation of them */
	private final TableComboViewer colormapCombo;

	/** Constructor */
	public PalettePreferenceAttributeUI(Composite parent, PreferenceAttribute<String> attribute) {
		super(attribute);
		// create TableCombo
		colormapCombo = new TableComboViewer(parent, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		colormapCombo.getTableCombo().setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		colormapCombo.setContentProvider(ArrayContentProvider.getInstance());
		// Column width. First column is hidden (width not managed properly)
		colormapCombo.getTableCombo().defineColumns(new int[] { 0, 150, 250 });
		colormapCombo.getTableCombo().setDisplayColumnIndex(1);
		colormapCombo.setLabelProvider(new ColorMapLabelProvider());
		colormapCombo.setInput(ColorMap.getColormapsNames());
		updateWidgetAttribute();
	}

	@Override
	public Control getWidgetControl() {
		return colormapCombo.getControl();
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValue(colormapCombo.getTableCombo().getText(), true);
	}

	@Override
	public void updateWidgetAttribute() {
		colormapCombo.getTableCombo().setText(attribute.getValue());
	}

	/**
	 * Provides label/palette of color for TableComboViewer of colormaps (
	 */
	private class ColorMapLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (columnIndex == 2) {
				Image result = ColorMap.getColormapImage(element.toString(), 250, 14);
				colormapCombo.getControl().addDisposeListener(e -> result.dispose());
				return result;
			}
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			return columnIndex == 1 ? element.toString() : null;
		}
	}

}
