package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MessageBox;

import fr.ifremer.globe.core.utils.preference.attributes.DoublePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.utils.preference.DoubleInputUI;

public class DoublePreferenceAttributeUI extends AbstractPreferenceAttributeUI<Double> {

	private DoubleInputUI widget;

	public DoublePreferenceAttributeUI(Composite parent, PreferenceAttribute<Double> attribute, boolean editable) {
		super(attribute);

		widget = new DoubleInputUI(parent, SWT.NONE);
		updateWidgetAttribute();
		
		widget.setEditable(editable);
	}

	@Override
	public Control getWidgetControl() {
		return widget.getWigetAsControl();
	}

	@Override
	public void updatePreferenceAttribute() {
		try {
			double value = widget.getValue();
			attribute.setValue(value, true);
		} catch (NumberFormatException e) {
			// error popup
			MessageBox error = new MessageBox(widget.getWigetAsControl().getShell(), SWT.ICON_ERROR | SWT.OK);
			error.setText("Propertie change fail");
			error.setMessage("Parse error on " + attribute.getDisplayedName());
			error.open();
		}
	}

	@Override
	public void updateWidgetAttribute() {
		widget.setValue(((DoublePreferenceAttribute) attribute).getValue());
		
	}

}
