package fr.ifremer.globe.ui.utils.preference.editor;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
//import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
//import org.eclipse.swt.widgets.Text;
//import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.wb.swt.SWTResourceManager;
//import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
//import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import swing2swt.layout.BorderLayout;

public class DirectoryPreferenceAttributeUI extends AbstractPreferenceAttributeUI<Path> {

	protected static Logger logger = LoggerFactory.getLogger(DirectoryPreferenceAttributeUI.class);

	private Composite widget;
	private Button choose;
	private Label txt;

	public DirectoryPreferenceAttributeUI(Composite parent, PreferenceAttribute<Path> attribute, boolean editable) {
		super(attribute);
		widget = new Composite(parent, SWT.NONE);
		widget.setLayout(new BorderLayout(0, 0));
		txt = new Label(widget, SWT.NONE);
		txt.setText(((DirectoryPreferenceAttribute) attribute).getValue().toString());


		if (editable)
			txt.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));

		else
			txt.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txt.setLayoutData(BorderLayout.CENTER);

		// tooltip.setVisible(true);

		choose = new Button(widget, SWT.NONE);
		choose.setEnabled(editable);
		choose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(widget.getShell());
				dialog.setFilterPath(txt.getText());
				String path = dialog.open();
				if (path != null) {
					// User may have selected a bad pathname : we test it
					Path p = FileSystems.getDefault().getPath(path);
					if (Files.isDirectory(p)) {
						txt.setText(path);
						txt.pack();
						txt.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
						txt.setToolTipText(path);
					} else {
						// we keep old value in case of error
						txt.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_RED));
						logger.warn("Propertie change fail: \"" + path + "\" is not a valid directory !");
						// error popup
						MessageBox error = new MessageBox(widget.getShell(), SWT.ICON_ERROR | SWT.OK);
						error.setText("Propertie change fail");
						error.setMessage("\"" + path + "\" is not a valid directory !");
						error.open();

					}
				}

			}
		});
		choose.setText("...");
		choose.setLayoutData(BorderLayout.EAST);
	}

	@Override
	public Control getWidgetControl() {
		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValueAsString(txt.getText(), true);
	}

	@Override
	public void updateWidgetAttribute() {
		txt.setText(((DirectoryPreferenceAttribute) attribute).getValue().toString());
		
	}

}
