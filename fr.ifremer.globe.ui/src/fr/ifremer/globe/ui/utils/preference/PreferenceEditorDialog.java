package fr.ifremer.globe.ui.utils.preference;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

/**
 * Dialog window for preference editing. This class manage communication between Preference Tree and Preference Page.
 * 
 * @author mguillem
 * 
 */
public class PreferenceEditorDialog extends Dialog {

	private PreferenceEditorPage page;
	private PreferenceEditorTree tree;

	private PreferenceComposite selectedNode;
	private PreferenceAttribute<?> selectedAttribute;

	public PreferenceEditorDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.APPLICATION_MODAL);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		// UI
		FillLayout layout = new FillLayout(SWT.HORIZONTAL);
		layout.marginWidth = 8;
		layout.marginHeight = 8;
		layout.spacing = 8;
		container.setLayout(layout);
		SashForm form = new SashForm(container, SWT.NONE);
		form.setOrientation(SWT.HORIZONTAL);
		form.setLayout(layout);
		tree = new PreferenceEditorTree(form, SWT.NONE);
		page = new PreferenceEditorPage(form, SWT.NONE, true);
		form.setWeights(new int[] { 35, 65 });

		if (selectedNode != null) {
			tree.setSelectedNode(selectedNode);
			page.populate(selectedNode);
		}
		if (selectedAttribute != null) {
			page.setSelectedAttribute(selectedAttribute);
		}

		// page change:
		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// when selection change, cuttent attributes are set
				page.performOk();
				// then new node is loaded
				page.populate(tree.getSelectedNode());
			}
		});

		return container;
	}

	// overriding this methods allows you to set the
	// title of the custom dialog
	@Override
	protected void configureShell(Shell newShell) {
		newShell.setMinimumSize(new Point(150, 200));
		super.configureShell(newShell);
		newShell.setText("Preferences Editor");
	}

	@Override
	protected Point getInitialSize() {
		return new Point(665, 480);
	}

	@Override
	protected void okPressed() {
		page.performOk();
		// all modification are saved
		PreferenceRegistry.getInstance().getRootNode().save();
		setReturnCode(OK);
		close();
	}

	@Override
	protected void cancelPressed() {
		// all modification are droped on cancel
		PreferenceRegistry.getInstance().getRootNode().load();
		setReturnCode(CANCEL);
		close();
	}

	public int open() {
		return open(null, null);
	}

	public int open(PreferenceComposite selectedNode, PreferenceAttribute<?> selectedAttribute) {

		this.selectedNode = selectedNode;
		this.selectedAttribute = selectedAttribute;
		return super.open();

		// setBlockOnOpen(false);
		// open();
		// page.populate(selectedNode);
		// tree.setSelectedNode(selectedNode);
		// if (selectedAttribute != null) {
		// page.setSelectedAttribute(selectedAttribute);
		// }

		// Display.getCurrent().asyncExec(new Runnable() {
		// public void run() {
		//
		// }
		// });
	}
}
