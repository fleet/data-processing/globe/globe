package fr.ifremer.globe.ui.utils.preference;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * This class is design to provide double / float input method based on Text
 * widget
 * 
 * @author mguillem
 * 
 */
public class DoubleInputUI {

	private double value;
	private Text widget;

	public DoubleInputUI(Composite parent, int style) {
		widget = new Text(parent, style);
		value = 0;
		// set numeric filter
		widget.addListener(SWT.Verify, new DoubleFormat());
	}

	/**
	 * Returns the component value
	 */
	public double getValue() {
		value = Double.parseDouble(widget.getText());
		return value;
	}

	/**
	 * Set the component value
	 */
	public void setValue(double value) {
		this.value = value;
		widget.setText(Double.toString(value));
		showStatus();
	}

	/**
	 * Sets the editable state.
	 */
	public void setEditable(boolean editable) {
		widget.setEditable(editable);
	}

	/**
	 * return the inner text widget as control
	 */
	public Control getWigetAsControl() {
		return widget;
	}

	private void showStatus() {
		try{
			Double.parseDouble(widget.getText());
			widget.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
			}catch(NumberFormatException e){
				widget.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
			}
	}

	// force format for double parsing
	private class DoubleFormat implements Listener {
		@Override
		public void handleEvent(Event event) {
			if(event.character != 0)
			{
				// character typed from keyboard
				String string = event.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);

				// dot
				boolean dotenable = true;
				if (widget.getText().contains("."))
					dotenable = false;
				// minus
				boolean minusEnable = false;
				if ((widget.getCaretPosition() == 0 && !widget.getText().contains("-")))
					minusEnable = true;

				// 0..9 and dot check
				for (int i = 0; i < chars.length; i++) {
					if (!('0' <= chars[i] && chars[i] <= '9')) {
						if (i == 0 && chars[i] == '-' && minusEnable)
							continue;
						if ((chars[i] == '.') && dotenable) {
							dotenable = false;
							continue;
						}
						event.doit = false;
						break;
					}
				}
			}
			showStatus();
		}

	}

}
