package fr.ifremer.globe.ui.utils.preference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

/**
 * This class display all attributes of a preference node.
 * 
 * @author mguillem
 * 
 */
public class PreferenceEditorPage extends Composite {
	private Label name;
	private Label message;
	private Table table;
	private List<Control> editors;
	// editing the 2nd column (value)
	private final int EDITABLECOLUMN = 1;
	private boolean editable = true;
	private Button btnReset;
	private Composite composite;
	private Label lblResetAllValues;

	/**
	 * @param parent s parent
	 * @param style SWT style
	 * @param editable allow (or disable) the edition of attributes
	 */
	public PreferenceEditorPage(Composite parent, int style, boolean editable) {
		super(parent, style);
		this.editable = editable;
		editors = new ArrayList<>();
		setLayout(new GridLayout(1, false));
		name = new Label(this, SWT.NONE);
		name.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		message = new Label(this, SWT.NONE);
		message.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		message.setText("Please check sub-node for content (This node has no attribute).");
		FontData[] fD = name.getFont().getFontData();
		fD[0].setStyle(SWT.BOLD);
		fD[0].setHeight(12);
		Font font = new Font(name.getDisplay(), fD[0]);
		name.setFont(font);
		name.addDisposeListener(e -> font.dispose());

		table = new Table(this, SWT.BORDER | SWT.FULL_SELECTION);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		table.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem tableItem = table.getSelection()[0];
				PreferenceAttributeUI<?> ui = (PreferenceAttributeUI<?>) tableItem.getData();
				ui.getWidgetControl().setFocus();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		// resize the row height using a MeasureItem listener
		table.addListener(SWT.MeasureItem, new Listener() {
			@Override
			public void handleEvent(Event event) {
				event.height = 23;
			}
		});

		TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(240);
		tblclmnName.setText("Name");
		TableColumn tblclmnValue = new TableColumn(table, SWT.NONE);
		tblclmnValue.setWidth(155);
		tblclmnValue.setText("Value");

		composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));

		btnReset = new Button(composite, SWT.NONE);
		btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem[] items = table.getItems();
				for (int i = 0; i < items.length; i++) {
					((PreferenceAttributeUI<?>) items[i].getData()).getAttribute().resetToDefault();
					PreferenceAttributeUI<?> ui = (PreferenceAttributeUI<?>) items[i].getData();
					ui.updateWidgetAttribute();
				}
			}
		});
		btnReset.setText("Reset");

		lblResetAllValues = new Label(composite, SWT.NONE);
		lblResetAllValues.setText("Reset all values to default");
	}

	/**
	 * populate the page with attributes of baseNode
	 * 
	 * @param baseNode
	 */
	public void populate(PreferenceComposite node) {
		// we must dispose editors to clean table
		if (!editors.isEmpty()) {
			for (Control editor : editors) {
				if (editor != null)
					editor.dispose();
			}
			editors.clear();
		}
		table.removeAll();

		if (node == null)
			return;
		name.setText(node.getNodeName());
		name.pack();
		PreferenceAttributeUIAdapteur adapteur = new PreferenceAttributeUIAdapteur();
		String[] key = node.keys();
		if (key.length == 0) {
			// the node has no attribute
			table.setVisible(false);
			if (!node.getChildList().isEmpty())
				message.setVisible(true);
		} else {
			message.setVisible(false);
			table.setVisible(true);
		}
		Arrays.sort(key);
		// we list all attribute from node in table
		for (int i = 0; i < key.length; i++) {
			PreferenceAttribute<?> attribute = node.getAttribute(key[i]);
			if (attribute.getDisplayedName() != null) {
				TableItem item = new TableItem(table, SWT.NONE);
				TableEditor tableEditor = new TableEditor(table);
				tableEditor.grabHorizontal = true;
				tableEditor.horizontalAlignment = SWT.LEFT;
				item.setText(attribute.getDisplayedName());
				PreferenceAttributeUI<?> ui = adapteur.getUI(table, attribute, editable);
				item.setData(ui);
				tableEditor.setEditor(ui.getWidgetControl(), item, EDITABLECOLUMN);
				editors.add(tableEditor.getEditor());
			}
		}
	}

	/**
	 * Selects a particular line of the table.
	 * 
	 * @param attribute The attribute to select.
	 */
	public void setSelectedAttribute(PreferenceAttribute<?> attribute) {
		TableItem[] items = table.getItems();
		for (TableItem tableItem : items) {
			PreferenceAttributeUI<?> ui = (PreferenceAttributeUI<?>) tableItem.getData();
			if (ui.getAttribute().equals(attribute)) {
				table.setSelection(tableItem);
				ui.getWidgetControl().setFocus();
				break;
			}
		}
	}

	/**
	 * save the changes to the PreferenceComposite node
	 * 
	 * @return false if page is not editable
	 */
	public boolean performOk() {
		if (editable == false || table.getItems().length == 0) {
			return false;
		}
		TableItem[] items = table.getItems();
		for (int i = 0; i < items.length; i++) {
			((PreferenceAttributeUI<?>) items[i].getData()).updatePreferenceAttribute();
		}
		return true;
	}
}
