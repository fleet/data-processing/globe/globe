package fr.ifremer.globe.ui.utils.preference;

import java.nio.file.Path;
import java.util.Date;

import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DatePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DoublePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.FilePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.NumberPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.OptionalBooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PalettePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.ui.utils.preference.editor.BooleanPreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.ColorPreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.DatePreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.DefaultPreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.DirectoryPreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.DoublePreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.EnumPreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.FilePreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.IntegerPreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.NumberPreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.PalettePreferenceAttributeUI;
import fr.ifremer.globe.ui.utils.preference.editor.StringPreferenceAttributeUI;

public class PreferenceAttributeUIAdapteur {

	/**
	 * select the correct GUI for the passed attribute
	 *
	 * @param parent parent swt composite
	 * @param attribute
	 * @param editable Indicate if the PreferenceAttribute could be edited
	 * @return PreferenceAttributeUI
	 */
	@SuppressWarnings("unchecked")
	public PreferenceAttributeUI<?> getUI(Composite parent, PreferenceAttribute<?> attribute, boolean editable) {
		if (attribute.getClass() == BooleanPreferenceAttribute.class) {
			return new BooleanPreferenceAttributeUI(parent, (PreferenceAttribute<Boolean>) attribute, editable);
		} else if (attribute.getClass() == ColorPreferenceAttribute.class) {
			return new ColorPreferenceAttributeUI(parent, (PreferenceAttribute<GColor>) attribute, editable);
		} else if (attribute.getClass() == DirectoryPreferenceAttribute.class) {
			return new DirectoryPreferenceAttributeUI(parent, (PreferenceAttribute<Path>) attribute, editable);
		} else if (attribute.getClass() == DoublePreferenceAttribute.class) {
			return new DoublePreferenceAttributeUI(parent, (PreferenceAttribute<Double>) attribute, editable);
		} else if (attribute.getClass() == EnumPreferenceAttribute.class) {
			return new EnumPreferenceAttributeUI(parent, (PreferenceAttribute<Enum<?>>) attribute, editable);
		} else if (attribute.getClass() == OptionalBooleanPreferenceAttribute.class) {
			return new EnumPreferenceAttributeUI(parent, (PreferenceAttribute<Enum<?>>) attribute, editable);
		} else if (attribute.getClass() == FilePreferenceAttribute.class) {
			return new FilePreferenceAttributeUI(parent, (PreferenceAttribute<Path>) attribute, editable);
		} else if (attribute.getClass() == IntegerPreferenceAttribute.class) {
			return new IntegerPreferenceAttributeUI(parent, (PreferenceAttribute<Integer>) attribute, editable);
		} else if (attribute.getClass() == StringPreferenceAttribute.class) {
			return new StringPreferenceAttributeUI(parent, (PreferenceAttribute<String>) attribute, editable);
		} else if (attribute.getClass() == DatePreferenceAttribute.class) {
			return new DatePreferenceAttributeUI(parent, (PreferenceAttribute<Date>) attribute, editable);
		} else if (attribute.getClass() == NumberPreferenceAttribute.class) {
			return new NumberPreferenceAttributeUI(parent, (NumberPreferenceAttribute) attribute, editable);
		} else if (attribute.getClass() == PalettePreferenceAttribute.class) {
			return new PalettePreferenceAttributeUI(parent, (PalettePreferenceAttribute) attribute);
		}
		return new DefaultPreferenceAttributeUI(parent, attribute, editable);
	}
}
