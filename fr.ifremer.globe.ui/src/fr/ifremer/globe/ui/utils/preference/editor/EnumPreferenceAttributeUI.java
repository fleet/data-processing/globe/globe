package fr.ifremer.globe.ui.utils.preference.editor;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.ifremer.globe.core.utils.preference.attributes.EnumPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

/**
 * This GUI show the attribute Enum as a SWT Combo
 * 
 * @author mguillem
 *
 */
public class EnumPreferenceAttributeUI extends AbstractPreferenceAttributeUI<Enum<?>> {

	private Combo widget;
	private String combovalues[];
	private Enum<?> enumvalue;

	public EnumPreferenceAttributeUI(Composite parent, PreferenceAttribute<Enum<?>> attribute, boolean editable) {
		super(attribute);
		widget = new Combo(parent, SWT.READ_ONLY);
		enumvalue = (((EnumPreferenceAttribute) attribute).getValue());
		Set<?> set = EnumSet.allOf(enumvalue.getDeclaringClass());
		combovalues = new String[set.size()];
		Iterator<?> it = set.iterator();
		int index = 0;

		widget.setEnabled(editable);
		for (int i = 0; i < set.size(); i++) {
			combovalues[i] = it.next().toString();
			if (combovalues[i].equals(enumvalue.toString()))
				index = i;
		}
		widget.setItems(combovalues);
		widget.select(index);

	}

	@Override
	public Control getWidgetControl() {
		return widget;

	}

	@Override
	public void updatePreferenceAttribute() {
		Enum<?> e = Enum.valueOf(enumvalue.getDeclaringClass(), combovalues[widget.getSelectionIndex()]);
		attribute.setValue(e, true);
	}

	@Override
	public void updateWidgetAttribute() {
		int index = 0;
		for (int i = 0; i < combovalues.length; i++) {
			if (combovalues[i].equals(attribute.getValue().toString()))
				index = i;
		}
		widget.select(index);
		
	}

}
