package fr.ifremer.globe.ui.utils.preference.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;

public class StringPreferenceAttributeUI extends AbstractPreferenceAttributeUI<String> {

	private Text widget;

	public StringPreferenceAttributeUI(Composite parent, PreferenceAttribute<String> attribute, boolean editable) {
		super(attribute);
		widget = new Text(parent, SWT.NONE);
		widget.setEnabled(editable);
		widget.setText(attribute.getValue().toString());
	}

	@Override
	public Control getWidgetControl() {
		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValue(widget.getText(), true);
	}

	@Override
	public void updateWidgetAttribute() {
		widget.setText(attribute.getValue().toString());
		
	}

}
