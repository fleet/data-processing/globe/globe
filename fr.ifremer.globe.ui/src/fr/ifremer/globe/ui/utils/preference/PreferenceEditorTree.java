package fr.ifremer.globe.ui.utils.preference;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;

public class PreferenceEditorTree extends Composite {

	private Tree nodeTree;

	public PreferenceEditorTree(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new FillLayout(SWT.VERTICAL));
		PreferenceComposite rootNode = PreferenceRegistry.getInstance().getRootNode();
		nodeTree = new Tree(this, SWT.SINGLE | SWT.BORDER);
		nodeTree.setData(rootNode);
		// The tree is based on root node:
		populateTree(rootNode, null);
		nodeTree.setSize(150,200);

	}

	private void populateTree(PreferenceComposite node, TreeItem parent) {
		List<PreferenceComposite> childList = node.getChildList();
		for (PreferenceComposite child : childList) {
			TreeItem item;
			if (parent == null)
				item = new TreeItem(nodeTree, SWT.NONE);
			else
				item = new TreeItem(parent, SWT.NONE);
			item.setText(child.getNodeName());
			item.setData(child);
			populateTree(child, item);
			item.setExpanded(true);
		}
	}

	/**
	 * return selected node as PreferenceComposite
	 * 
	 * @return PreferenceComposite
	 */
	public PreferenceComposite getSelectedNode() {
		if (nodeTree.getSelection().length > 0)
			return (PreferenceComposite) nodeTree.getSelection()[0].getData();
		else
			return null;
	}

	/**
	 * Look for a PreferenceComposite into the hierachy of a TreeItem.
	 * 
	 * @param parent
	 *            Node where to look for $nodeToFind.
	 * @param nodeToFind
	 *            The node to find.
	 * @return Found node or <code>null</code> if the node doesn't exist in the TreeItem's hierarchy.
	 */
	protected TreeItem findItem(TreeItem parent, PreferenceComposite nodeToFind) {
		TreeItem result = null;

		if (parent.getData() != null && parent.getData().equals(nodeToFind)) {
			result = parent;
		} else {
			TreeItem[] children = parent.getItems();
			for (TreeItem child : children) {
				TreeItem findItem = findItem(child, nodeToFind);
				if (findItem != null) {
					result = findItem;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Selects a node.
	 * 
	 * @param node
	 *            The node to select.
	 */
	public void setSelectedNode(PreferenceComposite node) {

		TreeItem[] items = nodeTree.getItems();
		for (TreeItem child : items) {
			TreeItem find = findItem(child, node);
			if (find != null) {
				nodeTree.setSelection(find);
				break;
			}
		}
	}

	public void addSelectionListener(SelectionListener l) {
		nodeTree.addSelectionListener(l);
	}

	public void removeSelectionListener(SelectionListener l) {
		nodeTree.removeSelectionListener(l);
	}
}
