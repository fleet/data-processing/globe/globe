package fr.ifremer.globe.ui.utils.preference.editor;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.utils.preference.PreferenceAttributeUI;

public abstract class AbstractPreferenceAttributeUI<T> implements PreferenceAttributeUI<T> {

	protected PreferenceAttribute<T> attribute;

	public AbstractPreferenceAttributeUI(PreferenceAttribute<T> attribute) {
		this.attribute = attribute;
	}

	@Override
	public PreferenceAttribute<T> getAttribute() {
		return this.attribute;
	}

}
