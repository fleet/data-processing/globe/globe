package fr.ifremer.globe.ui.utils.preference.editor;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.preference.attributes.FilePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import swing2swt.layout.BorderLayout;

public class FilePreferenceAttributeUI extends AbstractPreferenceAttributeUI<Path> {

	protected static Logger logger = LoggerFactory.getLogger(FilePreferenceAttributeUI.class);

	private Composite widget;
	private Button choose;
	private Label txt;

	public FilePreferenceAttributeUI(Composite parent, PreferenceAttribute<Path> attribute, boolean editable) {
		super(attribute);
		widget = new Composite(parent, SWT.NONE);
		widget.setLayout(new BorderLayout(0, 0));
		txt = new Label(widget, SWT.NONE);
		if (editable)
			txt.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		else
			txt.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));

		txt.setText(((FilePreferenceAttribute) attribute).getValue().toAbsolutePath().toString());
		txt.setLayoutData(BorderLayout.CENTER);

		choose = new Button(widget, SWT.FLAT);
		choose.setEnabled(editable);
		choose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(widget.getShell());
				String file = dialog.open();
				if (file != null) {
					// Test here are limited, file may not exist but usable
					Path p = FileSystems.getDefault().getPath(file);
					if (!Files.isDirectory(p)) {
						txt.setText(file);
						txt.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
					} else {
						// we keep old value in case of error
						txt.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_RED));
						logger.warn("Propertie change fail: \"" + file + "\" is not a valid file !");
						// error popup
						MessageBox error = new MessageBox(widget.getShell(), SWT.ICON_ERROR | SWT.OK);
						error.setText("Propertie change fail");
						error.setMessage("\"" + file + "\" is not a valid file !");
						error.open();
					}
				}

			}
		});
		choose.setText("...");
		choose.setLayoutData(BorderLayout.EAST);
	}

	@Override
	public Control getWidgetControl() {
		return widget;
	}

	@Override
	public void updatePreferenceAttribute() {
		attribute.setValueAsString(txt.getText(), true);
	}

	@Override
	public void updateWidgetAttribute() {
		txt.setText(((FilePreferenceAttribute) attribute).getValue().toAbsolutePath().toString());

	}

}
