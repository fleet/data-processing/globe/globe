package fr.ifremer.globe.ui.utils;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.util.Optional;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.operation.ModalContext;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UI utilities class
 *
 * @author bvalliere
 */
public class UIUtils {
	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(UIUtils.class);

	/**
	 * Creates a float number validator
	 *
	 * @return the validator
	 */
	public static IInputValidator getFloatValidator() {
		IInputValidator validator = newText -> {
			try {
				Float.parseFloat(newText);
			} catch (NumberFormatException ex) {
				return "The input must be a float value";
			}
			return null;
		};
		return validator;
	}

	/**
	 * Creates a double number validator
	 *
	 * @return the validator
	 */
	public static IInputValidator getDoubleValidator() {
		IInputValidator validator = newText -> {
			try {
				Double.parseDouble(newText);
			} catch (NumberFormatException ex) {
				return "The input must be a double value";
			}
			return null;
		};
		return validator;
	}

	public static IInputValidator getIntegerValidator() {
		IInputValidator validator = newText -> {
			try {
				Integer.parseInt(newText);
			} catch (NumberFormatException ex) {
				return "The input must be an integer value";
			}
			return null;
		};
		return validator;
	}

	public static final FormAttachment FORM_ATTACHMENT_100_0 = new FormAttachment(100, 0);
	public static final FormAttachment FORM_ATTACHMENT_0_0 = new FormAttachment(0, 0);

	public static void addFileSelectionListener(Button button, final Text text, final String[] extensions,
			final String[] filterNames) {
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Shell shell = Display.getDefault().getActiveShell();
				FileDialog dialog = new FileDialog(shell, SWT.MULTI);
				dialog.setFilterExtensions(extensions);
				dialog.setFilterNames(filterNames);
				dialog.open();
				String[] files = dialog.getFileNames();
				String result = "";
				if (files != null) {
					for (String file : files) {
						result = result + dialog.getFilterPath() + File.separator + file + ";";
					}
				}
				text.setText(result);
			}
		});
	}

	/**
	 * Give the focus to the SWT container when AWT canvas take the focus
	 */
	public static void bindFocus(final Component component, final EPartService partService, final MPart part) {
		component.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
			}

			@Override
			public void focusGained(FocusEvent e) {
				Display.getDefault().syncExec(() -> partService.activate(part, true));
			}
		});
	}

	public static void bindFocus(final Component component, final Composite composite) {
		component.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
			}

			@Override
			public void focusGained(FocusEvent e) {
				Display.getDefault().syncExec(() -> {
					if (!composite.isDisposed()) {
						composite.forceFocus();
					}
				});
			}
		});
	}

	public static IInputValidator getFileNameValidator() {
		return newText -> null;
	}

	/**
	 * Return display.
	 *
	 * @return return current display or default if no current
	 */
	public static Display getDisplay() {
		Display display = Display.getCurrent();
		if (display == null) {
			display = Display.getDefault();
		}
		return display;
	}

	/**
	 * @return Current shell
	 */
	public static Shell getActiveShell() {
		Display display = getDisplay();
		Shell result = display.getActiveShell();

		if (result == null) {
			Shell[] shells = display.getShells();
			for (Shell shell : shells) {
				if (shell.getShells().length == 0) {
					result = shell;
				}
			}
		}

		return result;
	}

	/**
	 * @return Current shell
	 */
	public static IProgressMonitor getCurrentProgressMonitor() {
		IProgressMonitor result = null;
		Thread currentThread = Thread.currentThread();
		if (ModalContext.isModalContextThread(currentThread)) {
			try {
				result = (IProgressMonitor) FieldUtils.readDeclaredField(currentThread, "progressMonitor", true);
			} catch (IllegalAccessException e) {
				logger.debug(e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Manage the visible flag for the specified control
	 */
	public static void setVisible(Control control, boolean visible) {
		if (control == null || control.isDisposed() || control.getVisible() == visible) {
			return;
		}

		control.setVisible(visible);
		Object layoutData = control.getLayoutData();
		if (layoutData instanceof GridData) {
			GridData gridData = (GridData) layoutData;
			if (visible) {
				Integer heightHint = (Integer) control.getData("SAVED_HEIGHTHINT");
				gridData.heightHint = heightHint != null ? heightHint.intValue() : SWT.DEFAULT;
				Integer widthHint = (Integer) control.getData("SAVED_WIDTHHINT");
				gridData.widthHint = widthHint != null ? widthHint.intValue() : SWT.DEFAULT;
			} else {
				control.setData("SAVED_HEIGHTHINT", gridData.heightHint);
				control.setData("SAVED_WIDTHHINT", gridData.widthHint);
				gridData.heightHint = 0;
				gridData.widthHint = 0;
			}
			control.requestLayout();
		}
	}

	/**
	 * @return The first sub-window of Globe if any
	 */
	public static Optional<Shell> getSubShell(Shell globeMainShell) {
		if (globeMainShell == null) {
			return Optional.empty();
		}
		Shell[] shells = globeMainShell.getDisplay().getShells();
		for (Shell oneShell : shells) {
			if (oneShell.getParent() == globeMainShell) {
				return Optional.of(oneShell);
			}
		}
		return Optional.empty();
	}

	/**
	 * Runs the {@link Runnable} in Main thread only if control is not disposed.
	 */
	public static void asyncExecSafety(Control control, Runnable runnable) {
		if (control != null && !control.isDisposed())
			control.getDisplay().asyncExec(() -> {
				// shortly after first check, so have to check again...
				if (control != null && !control.isDisposed()) {
					runnable.run();
				}
			});
	}

	/**
	 * Runs the {@link Runnable} in Main thread only if JFace viewer is not disposed.
	 */
	public static void asyncExecSafety(Viewer viewer, Runnable runnable) {
		if (viewer != null) {
			asyncExecSafety(viewer.getControl(), runnable);
		}
	}

	/**
	 * Enable all widgets of a control
	 *
	 * @param control control to enable/disable
	 * @param enable <code>true</code> to enable, <code>false</code> to disable
	 */
	public static void enableAllChildrenWidgets(final Control control, boolean enable) {
		if (control instanceof Composite composite) {
			for (final Control c : composite.getChildren()) {
				enableAllChildrenWidgets(c, enable);
			}
		}
		control.setEnabled(enable);
	}

	/**
	 * Invoke a setEnable on the control and on all embedded one
	 */
	public static void recursiveSetEnabled(Control control, boolean enabled) {
		if (control instanceof Composite composite) {
			for (Control c : composite.getChildren())
				recursiveSetEnabled(c, enabled);
		}
		control.setEnabled(enabled);
	}
}
