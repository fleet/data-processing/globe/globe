package fr.ifremer.globe.ui.utils.dico;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/** Bathyscope's i18n helper class. */
public class DicoBundle {
	/**
	 * This method is used to read a texte string in a .properties file.
	 */
	private static final String BUNDLE_NAME = "fr.ifremer.globe.ui.utils.dico.messages"; //$NON-NLS-1$

	public static ResourceBundle RESOURCE_BUNDLE;

	private DicoBundle() {
	}

	/**
	 * 
	 * @param key
	 * @return the text string associated to the key.
	 */
	public static String getString(String key) {
		try {
			if (RESOURCE_BUNDLE == null) {
				init();
			}
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	/**
	 * @throws MissingResourceException
	 *             when bundle {@link #BUNDLE_NAME} isn't found
	 */
	public static void init() throws MissingResourceException {
		RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, Locale.ROOT);
	}
}
