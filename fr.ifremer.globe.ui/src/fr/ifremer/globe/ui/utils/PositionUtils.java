package fr.ifremer.globe.ui.utils;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.utils.number.NumberUtils;
import gov.nasa.worldwind.geom.Position;

/**
 * Utility class to handle {@link Position}.
 */
public class PositionUtils {

	/**
	 * Private constructor.
	 */
	private PositionUtils() {
		// utility class
	}

	/**
	 * @return a {@link Position} from a {@link IMarker}.
	 */
	public static Position getPosition(IMarker marker) {
		return Position.fromDegrees(marker.getLat(), marker.getLon(), marker.getElevation());
	}

	/**
	 * Sets latitude/longitude/elevation of a {@link IMarker} with a {@link Position}.
	 */
	public static void setPosition(IMarker marker, Position position) {
		marker.setLatitude(position.getLatitude().degrees);
		marker.setLongitude(position.getLongitude().degrees);
		marker.setElevation(position.elevation);
	}

	public static String positionToString(Position position) {
		return FormatLatitudeLongitude.latitudeToString(position.getLatitude().degrees) + " : "
				+ FormatLatitudeLongitude.longitudeToString(position.getLongitude().degrees) + " : "
				+ NumberUtils.getStringMetersDouble(position.getElevation()) + " m";
	}

}
