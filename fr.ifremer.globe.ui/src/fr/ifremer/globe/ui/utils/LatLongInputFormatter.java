package fr.ifremer.globe.ui.utils;

import java.util.Locale;

import org.eclipse.nebula.widgets.formattedtext.AbstractFormatter;
import org.eclipse.nebula.widgets.formattedtext.DoubleFormatter;

import fr.ifremer.globe.core.model.ModelPreferences;
import fr.ifremer.globe.core.utils.latlon.CoordinateDisplayType;
import fr.ifremer.globe.core.utils.latlon.DegMinDecFormatter;
import fr.ifremer.globe.core.utils.latlon.DegMinSecFormatter;
import fr.ifremer.globe.ui.utils.DegMinMaskFormatter.CoordinateType;

public class LatLongInputFormatter {
	private LatLongInputFormatter() {
	}

	public static AbstractFormatter getLatitudeFormatter() {
		CoordinateDisplayType display = (CoordinateDisplayType) ModelPreferences.getInstance().getGridCoordinate()
				.getValue();
		switch (display) {
		case DEG_MIN_DEC:
			return new DegMinMaskFormatter("U ## ##.###", CoordinateType.LATITUDE, DegMinDecFormatter.getInstance());

		case DEG_MIN_SEC:
			return new DegMinMaskFormatter("U ##°##'##''", CoordinateType.LATITUDE, DegMinSecFormatter.getInstance());
		default:
		case DECIMAL:
			return new DoubleFormatter("-#0.######", Locale.US);
		}
	}

	public static AbstractFormatter getLongitudeFormatter() {
		CoordinateDisplayType display = (CoordinateDisplayType) ModelPreferences.getInstance().getGridCoordinate()
				.getValue();
		switch (display) {
		case DEG_MIN_DEC:
			return new DegMinMaskFormatter("U ### ##.###", CoordinateType.LONGITUDE, DegMinDecFormatter.getInstance());

		case DEG_MIN_SEC:
			return new DegMinMaskFormatter("U ###°##'##''", CoordinateType.LONGITUDE, DegMinSecFormatter.getInstance());

		default:
		case DECIMAL:
			return new DoubleFormatter("-##0.######", Locale.US);
		}
	}
}
