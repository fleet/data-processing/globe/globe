package fr.ifremer.globe.ui.utils.color;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.color.GColor;

/***
 * List of colormaps available in shaders
 * 
 * @author MORVAN
 * 
 */
public class ColorMap {
	protected static Logger logger = LoggerFactory.getLogger(ColorMap.class);

	public enum ColorMaps {
		GMT_JET("GMT_jet"), //
		UDIV_CMOCEAN_DELTA("udiv_cmocean_delta"), //
		U_CMOCEAN_THERMAL("u_cmocean_thermal"), //
		GRAY("GRAY");

		public final String label;

		ColorMaps(String label) {
			this.label = label;
		}

		public int getIndex() {
			return colormapsNames.stream().filter(colormap -> colormap.equalsIgnoreCase(label))
					.map(colormapsNames::indexOf).findFirst().orElse(0);
		}
	}

	/***
	 * ColorMaps dir path
	 */

	/***
	 * list of colormaps
	 */
	private static List<float[][]> colormaps = new ArrayList<>();

	/***
	 * list of colormaps names
	 */
	private static List<String> colormapsNames = new ArrayList<>();

	public static int red = 0;
	public static int green = 1;
	public static int blue = 2;

	/***
	 * load Available ColorMaps
	 * 
	 * @throws IOException
	 */
	public static void load() {
		// test if colormaps already loaded
		if (colormaps.size() > 0)
			return;

		// create graylevel
		float[][] gray = new float[3][256];
		for (int color = 0; color < 3; color++) {
			for (int index = 0; index < 256; index++) {
				gray[color][index] = (float) (index / 255.0);
			}
		}
		colormapsNames.add("GRAY");
		colormaps.add(gray);

		try {
			var dir = new File(
					FileLocator.resolve(FrameworkUtil.getBundle(ColorMap.class).getEntry("/colormap")).getPath());
			if (dir.exists() && dir.isDirectory()) {
				String[] children = dir.list();
				for (String child : children) {
					if (child.endsWith("cpt")) {
						try (var file = new FileInputStream(dir.getPath() + "/" + child);) {
							DataInputStream in = new DataInputStream(file);
							BufferedReader buffer = new BufferedReader(new InputStreamReader(in));
							readFile(buffer, child.substring(0, child.indexOf(".")));
						} catch (Exception e) {
							logger.error("Could not open colormap : " + child + " : " + e.getMessage(), e);
						}
					}
				}
			}
		} catch (IOException e) {
			logger.error("Could not open colormap dir : " + e.getMessage(), e);
		}
	}

	/***
	 * read the colormap file
	 * 
	 * @param file
	 * @param name
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void readFile(BufferedReader buffer, String name) throws IOException, InterruptedException {
		if (buffer == null) {
			return;
		}

		// categorical or regular file?
		if (!regular(buffer, name)) {
			categorical(buffer, name);
		}
	}

	/***
	 * read regular cpt file
	 * 
	 * @param file
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static boolean regular(BufferedReader buffer, String name) throws IOException, InterruptedException {
		// Get the object of DataInputStream

		String strLine;

		int index = 0;
		float colorMap[][] = new float[3][256];

		double val;

		// Read File Line By Line
		while ((strLine = buffer.readLine()) != null && index < 256) {
			// if first element is "#"
			// not a regular file
			if (strLine.contains("#")) {
				return false;
			}

			// first element must be skipped
			strLine = strLine.substring(strLine.indexOf(" ") + 1);

			// second is r
			val = Double.parseDouble(strLine.substring(0, strLine.indexOf(" ")));
			colorMap[red][index] = (float) val;
			strLine = strLine.substring(strLine.indexOf(" ") + 1);

			// third is g
			val = Double.parseDouble(strLine.substring(0, strLine.indexOf(" ")));
			colorMap[green][index] = (float) val;
			strLine = strLine.substring(strLine.indexOf(" ") + 1);

			// fourth is b
			val = Double.parseDouble(strLine.substring(0, strLine.indexOf(" ")));
			colorMap[blue][index] = (float) val;

			index++;
		}

		// add colormap
		colormaps.add(fullScale(colorMap, index));

		// add name
		colormapsNames.add(name);
		return true;
	}

	/***
	 * read categorical cpt file
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	private static void categorical(BufferedReader buffer, String name) throws NumberFormatException, IOException {
		String strLine;

		boolean isCategorical = false;

		int index = 0;
		float colorMap[][] = new float[3][256];

		double val = 0;
		int ii;
		// Read File Line By Line
		while ((strLine = buffer.readLine()) != null && index < 256) {
			// if first element is "#"
			// not a regular file
			if (strLine.contains("#")) {
				isCategorical = true;
				continue;
			}

			if (strLine.length() == 0 || strLine.contains("B") || strLine.contains("F") || strLine.contains("N")) {
				continue;
			}

			if (!isCategorical) {
				break;
			}

			// 5 first elements must be skipped
			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			if (strLine.indexOf("\t") != -1) {
				ii = strLine.indexOf("\t");
			} else {
				ii = strLine.indexOf(" ");
			}
			strLine = strLine.substring(ii + 1);

			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			if (strLine.indexOf("\t") != -1) {
				ii = strLine.indexOf("\t");
			} else {
				ii = strLine.indexOf(" ");
			}
			strLine = strLine.substring(ii + 1);

			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			if (strLine.indexOf("\t") != -1) {
				ii = strLine.indexOf("\t");
			} else {
				ii = strLine.indexOf(" ");
			}
			strLine = strLine.substring(ii + 1);

			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			if (strLine.indexOf("\t") != -1) {
				ii = strLine.indexOf("\t");
			} else {
				ii = strLine.indexOf(" ");
			}
			strLine = strLine.substring(ii + 1);

			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			if (strLine.indexOf("\t") != -1) {
				ii = strLine.indexOf("\t");
			} else {
				ii = strLine.indexOf(" ");
			}
			strLine = strLine.substring(ii + 1);

			// second is r
			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			if (strLine.indexOf("\t") != -1) {
				ii = strLine.indexOf("\t");
			} else {
				ii = strLine.indexOf(" ");
			}
			try {
				val = Integer.parseInt(strLine.substring(0, ii));
			} catch (IndexOutOfBoundsException ex) {
				ex.printStackTrace();
				System.err.println("LINE ===> " + index + " : " + ex.getMessage());
				throw ex;
			}
			colorMap[red][index] = (float) (val / 255.0);
			strLine = strLine.substring(ii + 1);

			// third is g
			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			if (strLine.indexOf("\t") != -1) {
				ii = strLine.indexOf("\t");
			} else {
				ii = strLine.indexOf(" ");
			}
			val = Integer.parseInt(strLine.substring(0, ii));
			colorMap[green][index] = (float) (val / 255.0);
			strLine = strLine.substring(ii + 1);

			// fourth is b
			while (strLine.indexOf(" ") == 0) {
				strLine = strLine.substring(strLine.indexOf(" ") + 1);
			}
			while (strLine.indexOf("\t") == 0) {
				strLine = strLine.substring(strLine.indexOf("\t") + 1);
			}
			val = Integer.parseInt(strLine.substring(0));
			colorMap[blue][index] = (float) (val / 255.0);

			index++;
		}

		// add colormap
		colormaps.add(fullScale(colorMap, index));

		// add name
		colormapsNames.add(name);
	}

	/****
	 * 
	 * @param colorMap
	 * @param index
	 * @return scaled colormap on 256 values
	 */
	private static float[][] fullScale(float[][] colorMap, int width) {
		if (width == 256) {
			return colorMap;
		}
		float[][] scaled = new float[3][256];
		double scale = (width - 1) / 255.0;
		double interp;
		int unScaledIndex;
		for (int indexScale = 0; indexScale < 256; indexScale++) {
			unScaledIndex = (int) Math.floor(scale * indexScale);
			interp = scale * indexScale - unScaledIndex;

			for (int color = 0; color < 3; color++) {
				if (unScaledIndex + 1 < width) {
					scaled[color][indexScale] = (float) ((1.0 - interp) * colorMap[color][unScaledIndex]
							+ interp * colorMap[color][unScaledIndex + 1]);
				} else {
					scaled[color][indexScale] = colorMap[color][unScaledIndex];
				}
			}
		}

		return scaled;
	}

	/***
	 * 
	 * @param index
	 * @return the colormap
	 */
	public static float[][] getColormap(int index) {
		if (index < colormaps.size()) {
			return colormaps.get(index);
		}
		return null;
	}

	/***
	 * 
	 * @param index
	 * @return the colormap
	 */
	public static ArrayList<Color> getColormapAwtValues(int index) {
		ArrayList<Color> ret = new ArrayList<>(256);
		if (index < colormaps.size()) {
			float[][] colors = colormaps.get(index);

			for (int i = 0; i < colors[0].length; i++) {
				ret.add(new Color(colors[0][i], colors[1][i], colors[2][i]));
			}
		}
		return ret;
	}

	public static List<String> getColormapsNames() {
		return colormapsNames;
	}

	public static int getColorMapIntIndex(String name) {
		int index = 0;
		for (String colormap : colormapsNames) {
			if (colormap.equalsIgnoreCase(name)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	/***
	 * 
	 * @param name
	 * @return the index of the colormap
	 */
	public static float getColorMapIndex(String name) {
		return getColorMapIntIndex(name);
	}

	public static Image getColormapImage(String name, int width, int height) {
		float[][] colors = ColorMap.getColormap(getColorMapIntIndex(name));
		Image image = new Image(Display.getDefault(), width, height);
		GC gc = new GC(image);
		for (int i = 0; i < width; i++) {
			int index = Math.round(i / (float) width * 256);
			int r = (int) (colors[ColorMap.red][index] * 255);
			int g = (int) (colors[ColorMap.green][index] * 255);
			int b = (int) (colors[ColorMap.blue][index] * 255);
			gc.setForeground(new org.eclipse.swt.graphics.Color(Display.getDefault(), r, g, b));
			gc.drawLine(i, 0, i, image.getBounds().height);
		}
		gc.dispose();

		return image;
	}

	/**
	 * @return GColor build from parameters.
	 */
	public static GColor getColor(int colorMapIndex, boolean isColorMapInverted, float minContrast, float maxContrast,
			double value) {
		float[][] colors = ColorMap.getColormap(colorMapIndex);
		value = Math.max(Math.min(value, maxContrast), minContrast);
		int index = (int) Math.round(255 * (value - minContrast) / Math.abs(maxContrast - minContrast));
		if (isColorMapInverted)
			index = 255 - index;
		return new GColor((int) (colors[ColorMap.red][index] * 255), (int) (colors[ColorMap.green][index] * 255),
				(int) (colors[ColorMap.blue][index] * 255));
	}

}
