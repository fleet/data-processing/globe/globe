package fr.ifremer.globe.ui.utils.color;

import java.awt.Color;
import java.util.Optional;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.color.GColor;

public class ColorUtils {

	protected static Logger logger = LoggerFactory.getLogger(ColorUtils.class);

	// list of valid pin colors
	private static String pinColorNames[] = { "Yellow", "Red", "Black", "Brown", "Teal", "Purple", "Blue", "Gray",
			"White", "Orange", "Green" };
	private static Color pinColors[] = { Color.yellow, Color.red, Color.black, new Color(165, 42, 42),
			new Color(0, 128, 128), new Color(128, 0, 128), Color.BLUE, Color.gray, Color.white, Color.orange,
			new Color(0, 128, 0) };

	/**
	 * Give the list of color names available for Geographic View placemark.
	 * 
	 * @return
	 */
	public static String[] getPinColorNames() {
		return pinColorNames;
	}

	/**
	 * return the color corresponding to given name
	 * 
	 * @param colorName
	 * @return
	 */
	public static java.awt.Color getPinColor(String colorName) {
		for (int i = 0; i < pinColorNames.length; i++) {
			if (pinColorNames[i].equalsIgnoreCase(colorName))
				return pinColors[i];
		}
		logger.warn("Color " + colorName + " not found, replaced by " + pinColors[0]);
		return pinColors[0];
	}

	/**
	 * Converts AWT {@link Color} to Eclipse {@link RGB}.
	 */
	public static java.awt.Color convertRGBToAWT(RGB rgb) {
		return new Color(rgb.red, rgb.green, rgb.blue);
	}

	/**
	 * Converts Eclipse {@link RGB} to AWT {@link Color}.
	 */
	public static RGB convertAWTToRGB(Color color) {
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		return new RGB(red, green, blue);
	}

	/**
	 * Converts Eclipse {@link RGB} to AWT {@link Color}.
	 */
	public static GColor convertRGB2GColor(RGB rgb) {
		return new GColor(rgb.red, rgb.green, rgb.blue);
	}

	/**
	 * Convert a AWT color to a SWT color.<br>
	 * WARNING : Must be called in the user interface thread.<br>
	 * CAUTION : You must dispose the color when it is no longer required.
	 */
	public static org.eclipse.swt.graphics.Color convertAWTToSWT(Color color) {
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		return new org.eclipse.swt.graphics.Color(Display.getCurrent(), new RGB(red, green, blue));
	}

	/**
	 * Convert a AWT color to a SWT color.<br>
	 * WARNING : Must be called in the user interface thread.<br>
	 * CAUTION : You must dispose the color when it is no longer required.
	 */
	public static Color cconvertSWTToAWT(org.eclipse.swt.graphics.Color color) {
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		return new Color(red, green, blue);
	}

	public static org.eclipse.swt.graphics.Color convertGColorToSWT(GColor c) {
		return SWTResourceManager.getColor(c.getRed(), c.getGreen(), c.getBlue());
	}

	public static GColor convertSWTToGColor(org.eclipse.swt.graphics.Color c) {
		return new GColor(c.getRed(), c.getGreen(), c.getBlue());
	}

	public static GColor convertJavaFXToGColor(javafx.scene.paint.Color javaFXColor) {
		return new GColor((int) (Math.round(javaFXColor.getRed() * 255f)),
				(int) (Math.round(javaFXColor.getGreen() * 255f)), (int) (Math.round(javaFXColor.getBlue() * 255f)));
	}

	public static javafx.scene.paint.Color convertGColorToJavaFXColor(GColor c) {
		return new javafx.scene.paint.Color(c.getFloatRed(), c.getFloatGreen(), c.getFloatBlue(), 1);
	}

	public static org.eclipse.swt.graphics.Color convertJavaFXToSWT(javafx.scene.paint.Color javaFXColor) {
		return SWTResourceManager.getColor((int) (Math.round(javaFXColor.getRed() * 255f)),
				(int) (Math.round(javaFXColor.getGreen() * 255f)), (int) (Math.round(javaFXColor.getBlue() * 255f)));
	}

	public static javafx.scene.paint.Color convertSWTToJavaFX(org.eclipse.swt.graphics.Color color) {
		return new javafx.scene.paint.Color(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f,
				color.getAlpha() / 255f);
	}

	public static GColor convertAWTtoGColor(Color c) {
		return new GColor(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}

	public static Color convertGColorToAWT(GColor c) {
		return new Color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}

	/**
	 * Opens a color selector dialog.
	 * 
	 * @return optional with the selected color, or empty if dialog has been canceled.
	 */
	public static Optional<GColor> openColorSelectorDialog() {
		ColorDialog colorDialog = new ColorDialog(Display.getDefault().getActiveShell());
		RGB rgb = colorDialog.open();
		return rgb != null ? Optional.of(ColorUtils.convertRGB2GColor(rgb)) : Optional.empty();
	}

	/**
	 * Get image from color
	 */
	public static Image getImageFromColor(GColor color, int width, int height) {
		Image image = new Image(Display.getDefault(), width, height);
		GC gc = new GC(image);
		gc.setBackground(convertGColorToSWT(color));
		gc.fillRectangle(image.getBounds());
		gc.dispose();
		return image;
	}

	@Deprecated
	public static String convertToABGR(String color) {
		// TODO to remove
		return null;
	}
}
