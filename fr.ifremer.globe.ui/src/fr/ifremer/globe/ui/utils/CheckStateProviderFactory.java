package fr.ifremer.globe.ui.utils;

import java.util.function.Function;

import org.eclipse.jface.viewers.ICheckStateProvider;

/**
 * Utility class to build {@link ICheckStateProvider}
 */
public class CheckStateProviderFactory {

	public static <T> ICheckStateProvider build(Class<T> clazz, Function<T, Boolean> function) {
		return new ICheckStateProvider() {

			@Override
			public boolean isGrayed(Object element) {
				return false;
			}

			@SuppressWarnings("unchecked") // assume to know the expected class
			@Override
			public boolean isChecked(Object element) {
				return function.apply(((T) element));
			}
		};
	}

}
