package fr.ifremer.globe.ui.utils;

import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolTip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Messages {
	/**
	 * logger.
	 */
	private final static Logger logger = LoggerFactory.getLogger(Messages.class);

	/**
	 * Opens an information dialog box.
	 */
	public static void openInfoMessage(String title, String msg) {
		Display.getDefault()
				.asyncExec(() -> MessageDialog.openInformation(Display.getCurrent().getActiveShell(), title, msg));
	}

	public static void openErrorMessage(final String title, Throwable e) {
		if (e.getMessage() != null) {
			openErrorMessage(title, e.getMessage());
		} else {
			openErrorMessage(title, e.toString());
		}
		logger.error("Message " + title + " " + e.getMessage(), e);
	}

	public static void openErrorMessage(Logger myLogger, final String title, Exception e) {
		if (e.getMessage() != null) {
			openErrorMessage(title, e.getMessage());
		} else {
			openErrorMessage(title, e.toString());
		}
		myLogger.error("Message " + title + " " + e.getMessage(), e);
	}

	public static void openErrorMessage(final String title, final String message, Throwable e) {
		openErrorMessage(title, message + System.getProperty("line.separator") + "(" + e.getMessage() + ")");
		logger.error("Message " + title + " " + message, e);
	}

	public static void openErrorMessage(Logger myLogger, final String title, final String message, Exception e) {
		openErrorMessage(title, message, e);
		myLogger.error("Message " + title + " " + message, e);
	}

	/**
	 * Open an error message
	 * 
	 * @param title
	 * @param message
	 */
	public static void openErrorMessage(final String title, final String message) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				MessageDialog.openError(Display.getCurrent().getActiveShell(), title, message);
				logger.error(message);

			}
		});
	}

	/**
	 * Open an warning message
	 * 
	 * @param title
	 * @param message
	 */
	public static void openWarningMessage(final String title, final String message) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				MessageDialog.openWarning(Display.getCurrent().getActiveShell(), title, message);
				logger.warn("Message " + title + " " + message);

			}
		});
	}

	/**
	 * Open synch warning message
	 */
	public static void openSyncWarningMessage(final String title, final String message) {
		Display.getDefault().syncExec(() -> {
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), title, message);
			logger.warn("Message {} : {}", title, message);
		});
	}

	/**
	 * Opens a question (sync). Note : use the method which return an integer to manage a click and close button.
	 * 
	 * @return <code>0</code> if the user presses the Yes button, <code>1</code> No, <code>- 1</code> cancel
	 */
	public static boolean openSyncQuestionMessage(String title, String message) {
		return openSyncQuestionMessage(Display.getDefault(), title, message, IDialogConstants.YES_LABEL,
				IDialogConstants.NO_LABEL) == 0;
	}

	public static int openSyncQuestionMessageWithCancel(String title, String message) {
		return openSyncQuestionMessage(Display.getDefault(), title, message, IDialogConstants.YES_LABEL,
				IDialogConstants.NO_LABEL, true);
	}

	/**
	 * Opens a question (sync).
	 * 
	 * @return <code>0</code> if the user presses the Yes button, <code>1</code> No, <code>- 1</code> cancel
	 */
	public static int openSyncQuestionMessage(String title, String message, String yesLabel, String noLabel) {
		return openSyncQuestionMessage(Display.getDefault(), title, message, yesLabel, noLabel);
	}

	/**
	 * Opens a question (sync) with yes, no and.
	 * 
	 * @return <code>0</code> if the user presses the Yes button, <code>1</code> No, <code>- 1</code> cancel
	 */
	public static int openSyncQuestionMessage(Display display, String title, String message, String yesLabel,
			String noLabel) {
		return openSyncQuestionMessage(display, title, message, yesLabel, noLabel, false);
	}

	/**
	 * Open a question (sync)
	 * 
	 * @param title the dialog's title, or <code>null</code> if none
	 * @param message the message
	 * @param defaultIndex the index in the button label array of the default button
	 * @return <code>0</code> if the user presses the Yes button, <code>1</code> No, <code>- 1</code> or <code>2</code> cancel.
	 */
	public static int openSyncQuestionMessage(String title, String message, String yesLabel, String noLabel,
			boolean displayCancel) {
		return openSyncQuestionMessage(Display.getDefault(), title, message, yesLabel, noLabel, displayCancel);
	}

	/**
	 * Open a question (sync)
	 * 
	 * @param title the dialog's title, or <code>null</code> if none
	 * @param message the message
	 * @param defaultIndex the index in the button label array of the default button
	 * @return <code>0</code> if the user presses the Yes button, <code>1</code> No,  <code>- 1</code> or <code>2</code> cancel.
	 */
	public static int openSyncQuestionMessage(Display display, String title, String message, String yesLabel,
			String noLabel, boolean displayCancel) {
		AtomicInteger status = new AtomicInteger(-1);
		var labels = displayCancel ? new String[] { yesLabel, noLabel, IDialogConstants.CANCEL_LABEL }
				: new String[] { yesLabel, noLabel };
		display.syncExec(() -> {
			MessageDialog dialog = new MessageDialog(Display.getCurrent().getActiveShell(), title, null, message,
					MessageDialog.QUESTION, labels, 0);
			status.set(dialog.open());
		});
		return status.get();
	}

	/**
	 * Open a ToolTip under the specified Control to display a warning message
	 */
	public static ToolTip openToolTip(Control control, String message) {
		ToolTip result = new ToolTip(control.getShell(), SWT.BALLOON);
		result.setLocation(control.toDisplay(10, control.getSize().y));
		result.setAutoHide(true);
		result.setMessage(message);
		result.setVisible(true);
		return result;
	}

}
