/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.gdal.ogr.Geometry;
import org.gdal.ogr.ogrConstants;
import org.gdal.osr.SpatialReference;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;

/**
 * Utility class for Geobox, Sector and Geometry
 */
public class GeoBoxUtils {

	/**
	 * Constructor
	 */
	private GeoBoxUtils() {
	}

	/** Create a new GeoBox with the sector data */
	public static GeoBox convert(Sector sector) {
		return sector != null //
				? new GeoBox(//
						sector.getMaxLatitude().degrees, //
						sector.getMinLatitude().degrees, //
						sector.getMaxLongitude().degrees, //
						sector.getMinLongitude().degrees//
				) //
				: null;
	}

	/** Create a new GeoBox with the sector data */
	public static Sector convert(GeoBox geoBox) {
		if (geoBox == null) {
			return null;
		}
		try {
			geoBox = geoBox.asLonLat();
		} catch (ProjectionException e) {
			// Unable to unproject the geobox
			return null;
		}
		return Sector.fromDegrees( //
				geoBox.getBottom(), //
				geoBox.getTop(), //
				geoBox.getLeft(), //
				geoBox.getRight()//
		);
	}

	/** Create a new Geometry of type ogrConstants.wkbPolygon from the specified geoBox */
	public static Geometry toPolygon(GeoBox geoBox) {

		Geometry geometry = new Geometry(ogrConstants.wkbLinearRing);
		// Assign SpatialReference
		if (geoBox != null && geoBox.getProjection() != null) {
			SpatialReference srs = new SpatialReference();
			if (srs.ImportFromProj4(geoBox.getProjection().proj4String()) == GdalUtils.OK) {
				geometry.AssignSpatialReference(srs);
			}
		}
		if (geometry.GetSpatialReference() == null) {
			geometry.AssignSpatialReference(GdalOsrUtils.SRS_WGS84);
		}

		if (geoBox != null) {
			geometry.AddPoint_2D(geoBox.getLeft(), geoBox.getTop());
			geometry.AddPoint_2D(geoBox.getRight(), geoBox.getTop());
			geometry.AddPoint_2D(geoBox.getRight(), geoBox.getBottom());
			geometry.AddPoint_2D(geoBox.getLeft(), geoBox.getBottom());
			geometry.CloseRings();
		}

		Geometry result = new Geometry(ogrConstants.wkbPolygon);
		result.AddGeometry(geometry);
		return result;
	}

	/**
	 * @return true when this sector is spanning the 180th meridian
	 */
	public static boolean isSpanning180Th(Sector sector) {
		return sector.getMinLongitude().degrees > 0 && sector.getMaxLongitude().degrees < 0;
	}

	/**
	 * @return the delta between min and max longitude
	 */
	public static double getDeltaLonDegrees(Sector sector) {
		return isSpanning180Th(sector) //
				? 360d - sector.getMinLongitude().degrees + sector.getMaxLongitude().degrees //
				: sector.getDeltaLonDegrees();
	}

	/**
	 * @return the average of the two longitude angles
	 */
	public static Angle averageLongitudes(Sector sector) {
		Angle result = Angle.average(sector.getMinLongitude(), sector.getMaxLongitude());
		return isSpanning180Th(sector) ? result.add(Angle.POS180) : result;
	}

	/**
	 * @return the latitude and longitude of the sector's angular center
	 */
	public static LatLon getCentroid(Sector sector) {
		LatLon result = sector.getCentroid();
		return isSpanning180Th(sector) ? new LatLon(result.latitude, averageLongitudes(sector)) : result;
	}

	/**
	 * @return a sector with only positive longitude if specified sector span the 180th meridian
	 */
	public static Sector adaptIfSpanning180Th(Sector sector) {
		Sector result = sector;
		if (isSpanning180Th(sector)) {
			result = new Sector(sector.getMinLatitude(), sector.getMaxLatitude(), sector.getMinLongitude(),
					Angle.fromDegrees(sector.getMinLongitude().degrees + GeoBoxUtils.getDeltaLonDegrees(sector)));
		}
		return result;
	}

	/**
	 * @return a sector with all angles normalized
	 */
	public static Sector normalizedDegrees(Sector sector) {
		Angle minLatitude = sector.getMinLatitude().normalizedLatitude();
		Angle maxLatitude = sector.getMaxLatitude().normalizedLatitude();
		Angle minLongitude = sector.getMinLongitude().normalizedLongitude();
		if (minLongitude.degrees == 180.0) {
			minLongitude = Angle.NEG180;
		}
		Angle maxLongitude = sector.getMaxLongitude().normalizedLongitude();
		if (maxLongitude.degrees == -180.0) {
			maxLongitude = Angle.POS180;
		}
		return new Sector(minLatitude, maxLatitude, minLongitude, maxLongitude);
	}

	/**
	 * Determines whether the sector1 intersects the other sector's range of latitude and longitude.
	 */
	public static boolean intersects(Sector sector1, Sector sector2) {
		if (Objects.equals(sector1, sector2)) {
			return true;
		}
		if (sector1 == null || sector2 == null) {
			return false;
		}

		if (isSpanning180Th(sector1)) {
			Sector west = clampTo180th(sector1, Angle.POS180);
			if (intersects(west, sector2)) {
				return true;
			}
			Sector east = clampTo180th(sector1, Angle.NEG180);
			return east.intersects(sector2);
		} else if (isSpanning180Th(sector2)) {
			Sector west = clampTo180th(sector2, Angle.POS180);
			if (intersects(sector1, west)) {
				return true;
			}
			Sector east = clampTo180th(sector2, Angle.NEG180);
			return intersects(sector1, east);
		}
		return sector1.intersects(sector2);
	}

	/**
	 * Determines whether the sector1 intersects the other sector's range of latitude and longitude.
	 */
	public static Sector union(Sector sector1, Sector sector2) {
		if (Objects.equals(sector1, sector2)) {
			return sector1;
		}
		if (sector1 == null) {
			return sector2;
		}
		if (sector2 == null) {
			return sector1;
		}

		Sector result = sector1.union(sector2);
		if (isSpanning180Th(sector1) != isSpanning180Th(sector2)) {
			Sector spanningSector = sector1;
			if (!isSpanning180Th(sector1)) {
				spanningSector = sector2;
				sector2 = sector1;
			}
			if (sector2.getMaxLongitude().degrees > 0) {
				result = new Sector(result.getMinLatitude(), result.getMaxLatitude(), sector2.getMinLongitude(),
						spanningSector.getMaxLongitude());
			} else {
				result = new Sector(result.getMinLatitude(), result.getMaxLatitude(), spanningSector.getMinLongitude(),
						sector2.getMaxLongitude());
			}

		}
		return result;
	}

	/**
	 * Determines whether the sector1 intersects the other sector's range of latitude and longitude.
	 */
	public static Sector clampTo180th(Sector sector, Angle longitude) {
		return longitude.degrees == 180d
				? new Sector(sector.getMinLatitude(), sector.getMaxLatitude(), sector.getMinLongitude(), Angle.POS180)// West
				: new Sector(sector.getMinLatitude(), sector.getMaxLatitude(), Angle.NEG180, sector.getMaxLongitude()); // East
	}

	/** @return the sector as an extent expected by gdal (xmin ymin xmax ymax) */
	public static double[] asGdalExtent(Sector sector) {
		return new double[] { //
				sector.getMinLongitude().degrees - (isSpanning180Th(sector) ? 360d : 0d), //
				sector.getMinLatitude().degrees, sector.getMaxLongitude().degrees, sector.getMaxLatitude().degrees };
	}

	/** @return the union sector of all specified sectors */
	public static Sector union(List<Sector> sectors) {
		if (sectors == null || sectors.isEmpty()) {
			return Sector.EMPTY_SECTOR;
		}

		Sector result = null;
		for (Sector sector : sectors) {
			result = union(result, sector);
		}
		return result;
	}

	/**
	 * @return a list of position covering the specified sector param density Size of the grid in latitude and longitude
	 *         to cover a sector with Positions
	 */
	public static List<LatLon> generateGridOfPosition(Sector sector, int density) {
		var result = new ArrayList<LatLon>((density + 1) * (density + 1));

		Angle deltaLat = sector.getDeltaLat().divide(density);
		Angle deltaLon = sector.getDeltaLon().divide(density);
		Angle latitude = sector.getMinLatitude();
		for (int iLatitude = 0; iLatitude <= density; iLatitude++) {
			Angle longitude = sector.getMinLongitude();
			for (int iLongitude = 0; iLongitude <= density; iLongitude++) {
				result.add(new LatLon(latitude, longitude));
				longitude = iLongitude == density ? sector.getMaxLongitude() : longitude.add(deltaLon);
				longitude = (longitude.degrees < -180) ? Angle.NEG180 : longitude;
				longitude = (longitude.degrees > 180) ? Angle.POS180 : longitude;
			}
			latitude = iLatitude == density ? sector.getMaxLatitude() : latitude.add(deltaLat);
		}

		return result;
	}

}
