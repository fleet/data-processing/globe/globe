package fr.ifremer.globe.ui.utils.image;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.data.BufferWrapperRaster;
import gov.nasa.worldwind.formats.tiff.GeotiffReader;
import gov.nasa.worldwind.formats.tiff.GeotiffWriter;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.BufferWrapper;
import gov.nasa.worldwind.util.WWBufferUtil;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;

import fr.ifremer.globe.ui.utils.color.ColorMap;

public class ImageUtils {

	/**
	 * Converts a buffered image to SWT <code>ImageData</code>.
	 *
	 * @param bufferedImage
	 *            the buffered image (<code>null</code> not permitted).
	 *
	 * @return The image data.
	 */
	public static ImageData convertToSWT(BufferedImage bufferedImage) {
		if (bufferedImage.getColorModel() instanceof DirectColorModel) {
			DirectColorModel colorModel = (DirectColorModel) bufferedImage.getColorModel();
			PaletteData palette = new PaletteData(colorModel.getRedMask(), colorModel.getGreenMask(), colorModel.getBlueMask());
			ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(), colorModel.getPixelSize(), palette);
			WritableRaster raster = bufferedImage.getRaster();
			int[] pixelArray = new int[4];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					raster.getPixel(x, y, pixelArray);
					int pixel = palette.getPixel(new RGB(pixelArray[0], pixelArray[1], pixelArray[2]));
					data.setAlpha(x, y, pixelArray[3]);
					data.setPixel(x, y, pixel);
				}
			}
			return data;

		} else if (bufferedImage.getColorModel() instanceof IndexColorModel) {
			IndexColorModel colorModel = (IndexColorModel) bufferedImage.getColorModel();
			int size = colorModel.getMapSize();
			byte[] reds = new byte[size];
			byte[] greens = new byte[size];
			byte[] blues = new byte[size];
			colorModel.getReds(reds);
			colorModel.getGreens(greens);
			colorModel.getBlues(blues);
			RGB[] rgbs = new RGB[size];
			for (int i = 0; i < rgbs.length; i++) {
				rgbs[i] = new RGB(reds[i] & 0xFF, greens[i] & 0xFF, blues[i] & 0xFF);
			}
			PaletteData palette = new PaletteData(rgbs);
			ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(), colorModel.getPixelSize(), palette);
			data.transparentPixel = colorModel.getTransparentPixel();
			WritableRaster raster = bufferedImage.getRaster();
			int[] pixelArray = new int[1];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					raster.getPixel(x, y, pixelArray);
					data.setPixel(x, y, pixelArray[0]);
				}
			}
			return data;
		}
		return null;
	}

	/**
	 * Serialize the bufferedImage in a Tiff format
	 */
	public static void writeAsTiff(BufferedImage bufferedImage, AVList params, File outFile) throws IOException {
		GeotiffWriter geotiffWriter = null;
		try {
			params.setValue(AVKey.PIXEL_FORMAT, AVKey.IMAGE);
			geotiffWriter = new GeotiffWriter(outFile);
			geotiffWriter.write(bufferedImage, params);
		} finally {
			if (geotiffWriter != null) {
				geotiffWriter.close();
			}
		}
	}

	/**
	 * Serialize the bufferedImage in a Tiff format
	 */
	public static void writeAsTiff(BufferedImage bufferedImage, File outFile) throws IOException {
		GeotiffWriter geotiffWriter = null;
		try {
			geotiffWriter = new GeotiffWriter(outFile);
			geotiffWriter.write(bufferedImage);
		} finally {
			if (geotiffWriter != null) {
				geotiffWriter.close();
			}
		}
	}

	/**
	 * Serialize the DataRaster in a Tiff format
	 */
	public static void writeAsTiff(double[][] dataRaster, Sector sector, File outFile) throws IOException {
		GeotiffWriter geotiffWriter = null;
		try {
			int nbLatCells = dataRaster.length;
			int nbLonCells = dataRaster[0].length;
			BufferWrapper bufferWrapper = WWBufferUtil.newDoubleBufferWrapper(nbLatCells * nbLonCells, false);
			for (int x = 0; x < nbLatCells; x++) {
				bufferWrapper.putDouble(x * nbLonCells, dataRaster[x], 0, nbLonCells);
			}
			geotiffWriter = new GeotiffWriter(outFile);
			geotiffWriter.write(new BufferWrapperRaster(nbLatCells, nbLonCells, sector, bufferWrapper));
		} finally {
			if (geotiffWriter != null) {
				geotiffWriter.close();
			}
		}
	}

	/**
	 * Read a Tiff file and load it in a BufferedImage
	 */
	public static BufferedImage readTiff(File tiffFile) throws IOException {
		GeotiffReader geotiffReader = null;
		try {
			geotiffReader = new GeotiffReader(tiffFile);
			return geotiffReader.read();
		} finally {
			if (geotiffReader != null) {
				geotiffReader.close();
			}
		}
	}

	/**
	 * Resize image.
	 */
	public static BufferedImage resize(BufferedImage imageSource, double ratio) {
		BufferedImage result = new BufferedImage((int) (ratio * imageSource.getWidth()), (int) (ratio * imageSource.getHeight()), imageSource.getType());
		Graphics2D g = result.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(imageSource, 0, 0, result.getWidth(), result.getHeight(), null);
		g.dispose();
		return result;
	}

	/**
	 * Supplier of double
	 */
	@FunctionalInterface
	public interface DoubleValueSupplier {
		double getValue(int line, int column);
	}

	/**
	 * Produce a BufferedImage from a set of double according to a color map.
	 */
	public static BufferedImage makeImage(int columnCount, int lineCount, DoubleRange minMaxValue, double missingValue, String colorName, DoubleValueSupplier supplier) {
		BufferedImage result = new BufferedImage(columnCount, lineCount, BufferedImage.TYPE_INT_ARGB);
		WritableRaster raster = result.getRaster();

		List<Color> awtColors = ColorMap.getColormapAwtValues(ColorMap.getColorMapIntIndex(colorName));
		for (int line = 0; line < lineCount; line++) {
			for (int column = 0; column < columnCount; column++) {
				double value = supplier.getValue(line, column);
				if (Double.compare(value, missingValue) != 0) {
					int colorIndex = (int) (((value - minMaxValue.getMinimumDouble()) / (minMaxValue.getMaximumDouble() - minMaxValue.getMinimumDouble())) * (awtColors.size() - 1));
					Color color = awtColors.get(Math.max(colorIndex, 0));
					raster.setSample(column, line, 0, color.getRed());
					raster.setSample(column, line, 1, color.getGreen());
					raster.setSample(column, line, 2, color.getBlue());
					raster.setSample(column, line, 3, 255);
				} else {
					// Missing value : transparent
					raster.setSample(column, line, 0, 0); // Red
					raster.setSample(column, line, 1, 0); // Green
					raster.setSample(column, line, 2, 0); // Blue
					raster.setSample(column, line, 3, 0);
				}
			}
		}

		return result;
	}

	/**
	 * Produce a BufferedImage from a set of double.
	 */
	public static BufferedImage makeImage(int columnCount, int lineCount, DoubleRange minMaxValue, double missingValue, DoubleValueSupplier supplier) {
		BufferedImage result = new BufferedImage(columnCount, lineCount, BufferedImage.TYPE_INT_ARGB);
		WritableRaster raster = result.getRaster();

		for (int line = 0; line < lineCount; line++) {
			for (int column = 0; column < columnCount; column++) {
				double value = supplier.getValue(line, column);
				if (Double.compare(value, missingValue) != 0) {
					int color = (int) ((value - minMaxValue.getMinimumDouble()) / (minMaxValue.getMaximumDouble() - minMaxValue.getMinimumDouble()) * 0xFFFFFF);
					raster.setSample(column, line, 0, (color >> 16) & 0xFF); // Red
					raster.setSample(column, line, 1, (color >> 8) & 0xFF); // Green
					raster.setSample(column, line, 2, (color >> 0) & 0xFF); // Blue
					raster.setSample(column, line, 3, 255);
				} else {
					// Missing value : transparent
					raster.setSample(column, line, 0, 0); // Red
					raster.setSample(column, line, 1, 0); // Green
					raster.setSample(column, line, 2, 0); // Blue
					raster.setSample(column, line, 3, 0);
				}
			}
		}

		return result;
	}

}
