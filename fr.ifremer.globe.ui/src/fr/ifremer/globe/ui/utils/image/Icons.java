package fr.ifremer.globe.ui.utils.image;

import org.eclipse.swt.graphics.Image;

public enum Icons {
	ANNOTATION(ImageResources.ICON_FOLDER + "annotation.png"), //
	ARROW_DOWN(ImageResources.ICON_FOLDER + "arrow_down.png"), //
	BACKGROUND(ImageResources.ICON_FOLDER + "background.png"), //
	BROKEN(ImageResources.ICON_FOLDER + "fileNotFound.png"), //
	CHART_LINE(ImageResources.ICON_FOLDER + "chart-line.png"), //
	CANCEL(ImageResources.ICON_FOLDER + "cancel.png"), //
	COLORS(ImageResources.ICON_FOLDER + "colors.png"), //
	DATABASE(ImageResources.ICON_FOLDER + "db.png"), //
	DELETE(ImageResources.ICON_FOLDER + "delete.png"), //
	DTM(ImageResources.ICON_FOLDER + "dtm.png"), //
	EDIT(ImageResources.ICON_FOLDER + "edit.png"), //
	ELEVATION(ImageResources.ICON_FOLDER + "elevation.png"), //
	EXPORT(ImageResources.ICON_FOLDER + "document_export.png"), //
	FILE(ImageResources.ICON_FOLDER + "defaultFile.png"), //
	FOLDER(ImageResources.ICON_FOLDER + "folder-survey.png"), //
	IMPORT(ImageResources.ICON_FOLDER + "document_import.png"), //
	ISOBATHS(ImageResources.ICON_FOLDER + "isobaths.png"), //
	LABEL(ImageResources.ICON_FOLDER + "label.png"), //
	LAYER(ImageResources.ICON_FOLDER + "layers.png"), //
	LOADED(ImageResources.ICON_FOLDER + "db.png"), //
	LOADING(ImageResources.ICON_FOLDER + "sand-timer.png"), //
	MARKER(ImageResources.ICON_FOLDER + "marker.png"), //
	NAVIGATION(ImageResources.ICON_FOLDER + "navigation.png"), //
	OK(ImageResources.ICON_FOLDER + "dialog_ok_apply.png"), //
	PLAY(ImageResources.ICON_FOLDER + "play.png"), //
	POLYGON(ImageResources.ICON_FOLDER + "polygon.png"), //
	PYTHON(ImageResources.ICON_FOLDER + "python.png"), //
	RASTER(ImageResources.ICON_FOLDER + "raster.png"), //
	REFRESH(ImageResources.ICON_FOLDER + "view_refresh.png"), //
	READING_ERROR(ImageResources.ICON_FOLDER + "loadingError.png"), //
	RUN(ImageResources.ICON_FOLDER + "run_exc.png"), //
	SAND_TIMER(ImageResources.ICON_FOLDER + "sand-timer.png"), //
	SERVICE(ImageResources.ICON_FOLDER + "service.png"), //
	SWATH(ImageResources.ICON_FOLDER + "swath.png"), //
	TEXTURE(ImageResources.ICON_FOLDER + "texture.png"), //
	UNAVAILABLE(ImageResources.ICON_FOLDER + "dialog_cancel.png"), //
	UNLOADED(ImageResources.ICON_FOLDER + "unloaded.png"), //
	USBL(ImageResources.ICON_FOLDER + "usbl.png"), //
	WC(ImageResources.ICON_FOLDER + "polarEcho.png"), //
	VIEW_MENU(ImageResources.ICON_FOLDER + "view_menu.png"), //
	WC_SLICE(ImageResources.ICON_FOLDER + "echogramLong.png"), //
	WMS(ImageResources.ICON_FOLDER + "wms.png");

	final String path;

	Icons(String path) {
		this.path = path;
	}

	public Image toImage() {
		return ImageResources.getImage(path);
	}
}