/**
 *
 */
package fr.ifremer.globe.ui.utils.image;

import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Gather SWT image resources.
 */
public class ImageResources {

	public static final String ICON_FOLDER = "icons/16/";

	public static Image getImage(String src, Class<?> callerClass) {
		return ResourceManager.getPluginImage(OsgiUtils.getBundleName(callerClass), src);
	}

	public static Image getImage(String src, Object callerObject) {
		return getImage(src, callerObject.getClass());
	}

	public static Image getImage(String src) {
		return getImage(src, ImageResources.class);
	}

	/**
	 * Constructor
	 */
	private ImageResources() {
	}

}
