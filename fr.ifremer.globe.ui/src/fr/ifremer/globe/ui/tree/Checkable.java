package fr.ifremer.globe.ui.tree;

/**
 * Interface for checkable elements, such as checkable tree nodes.
 */
public interface Checkable {

	/** Property name for checkable object state. */
	public final static String CHECKED = "checked";

	/** Returns true is currently checked. */
	public boolean isChecked();

	/**
	 * Returns true is currently grayed, ie. partially checked. This is relevant
	 * for parent nodes when only some of their children are checked.
	 */
	public boolean isGrayed();

	public void setChecked(boolean checked);
}
