//package fr.ifremer.globe.ui.tree;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.ListIterator;
//import java.util.regex.Pattern;
//
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.events.ModifyEvent;
//import org.eclipse.swt.events.ModifyListener;
//import org.eclipse.swt.events.SelectionEvent;
//import org.eclipse.swt.events.SelectionListener;
//import org.eclipse.swt.layout.GridData;
//import org.eclipse.swt.widgets.Button;
//import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.Text;
//
//import fr.ifremer.globe.model.IExplorer;
//import fr.ifremer.globe.ui.ImageResources;
//
///**
// * A search tool which allows to search a TreeNode in a TreeViewer.
// * 
// * @author pmahoudo
// * 
// */
//public class SearchTool {
//
//	/**
//	 * Create the widgets of the tool.
//	 * 
//	 * @param parent
//	 * @param explorer
//	 */
//	public static void createSearchTool(Composite parent, final IExplorer explorer) {
//
//		// Input Area
//		final Text inputText = new Text(parent, SWT.BORDER | SWT.SINGLE);
//		inputText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
//		inputText.setText("");
//
//		// Reset Button
//		final Button resetButton = new Button(parent, SWT.NONE);
//		resetButton.setLayoutData(new GridData(SWT.END, SWT.FILL, false, false));
//		resetButton.setToolTipText("Search");
//		resetButton.setImage(ImageResources.getImage("/icons/16/filefind.png", new SearchTool().getClass()));
//
//		// action on input area
//		inputText.addModifyListener(new ModifyListener() {
//			@Override
//			public void modifyText(ModifyEvent e) {
//				String regex = inputText.getText();
//				SearchTool.search(explorer, regex);
//
//				if (!regex.equals("")) {
//					resetButton.setToolTipText("Reset Search");
//					resetButton.setImage(ImageResources.getImage("/icons/16/edit-delete.png", new SearchTool().getClass()));
//					resetButton.setEnabled(true);
//				} else {
//					resetButton.setToolTipText("Search");
//					resetButton.setImage(ImageResources.getImage("/icons/16/filefind.png", new SearchTool().getClass()));
//
//				}
//			}
//		});
//
//		// action on reset button
//		resetButton.addSelectionListener(new SelectionListener() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				inputText.setText("");
//				inputText.forceFocus();
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//			}
//		});
//
//	}
//
//	/**
//	 * Search a regex in a IExplorer
//	 * 
//	 * @param explorer
//	 * @param regex
//	 *            The regular expression to use
//	 */
//	private static void search(IExplorer explorer, String regex) {
//
//		// explorer.refresh();
//		//
//		// TreeViewer viewer = explorer.getViewer();
//		//
//		// if (regex != "") {
//		//
//		// for (TreeItem treeItem : viewer.getTree().getItems()) {
//		//
//		// List<String> listName = new ArrayList<String>();
//		// List<ITreeNode> toDelete = new ArrayList<ITreeNode>();
//		//
//		// // create the list of name of children
//		// for (ITreeNode treeNode : (((ITreeNode)
//		// treeItem.getData()).getChildren())) {
//		// listName.add(treeNode.getName());
//		// }
//		//
//		// // create the list of indexex of name found
//		// List<Integer> listIndexes = SearchTool.getMatchingIndexes(listName,
//		// regex);
//		//
//		// // create the list of treenode to delete
//		// int i = 0;
//		// for (ITreeNode treeNode : ((ITreeNode)
//		// treeItem.getData()).getChildren()) {
//		// if (!listIndexes.contains(i)) {
//		// toDelete.add(treeNode);
//		// }
//		//
//		// i++;
//		// }
//		//
//		// // delete the treenodes
//		// ((ITreeNode) treeItem.getData()).getChildren().removeAll(toDelete);
//		//
//		// }
//		// viewer.expandAll();
//		// }
//
//	}
//
////	/**
////	 * Finds the index of all entries in the list that matches the regex
////	 * 
////	 * @param list
////	 *            The list of strings to check
////	 * @param regex
////	 *            The regular expression to use
////	 * @return list containing the indexes of all matching entries
////	 */
////	private static List<Integer> getMatchingIndexes(List<String> list, String regex) {
////		ListIterator<String> li = list.listIterator();
////
////		List<Integer> indexes = new ArrayList<Integer>();
////
////		// not a real regex
////		if (!regex.contains("*") && !regex.contains("?")) {
////			while (li.hasNext()) {
////				int i = li.nextIndex();
////				String next = li.next();
////				if (next.toLowerCase().contains(regex.toLowerCase())) {
////					indexes.add(i);
////				}
////			}
////		} else {
////			// Search String
////			regex = regex.replace("*", ".*"); // any string
////			regex = regex.replace("?", ".?"); // any character
////
////			Pattern p = null;
////
////			p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.COMMENTS | Pattern.MULTILINE | Pattern.DOTALL);
////
////			while (li.hasNext()) {
////				int i = li.nextIndex();
////				String next = li.next();
////				if (p.matcher(next).matches()) {
////					indexes.add(i);
////				}
////			}
////		}
////
////		return indexes;
////	}
//
// }
