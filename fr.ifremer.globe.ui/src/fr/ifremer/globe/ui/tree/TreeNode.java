/*
 * @License@ 
 */
package fr.ifremer.globe.ui.tree;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Adapter node for tree views.
 * 
 * @author Guillaume Bourel &lt;guillaume.bourel@altran.com&gt;
 */
public abstract class TreeNode<T> implements ITreeNode, Checkable, PropertyChangeListener {

	protected Logger logger = LoggerFactory.getLogger(TreeNode.class);

	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	/**
	 * Parent node.
	 */
	protected ITreeNode parent;

	/**
	 * Children nodes list.
	 */
	protected List<ITreeNode> children;

	/**
	 * JFace viewer associated to this node.
	 */
	private final TreeViewer viewer;

	/**
	 * The model object.
	 */
	protected T model;

	/**
	 * Ctor pour noeud fils.
	 * 
	 * @param parent le noeud parent (ne peut �tre <code>null</code> {@link TreeNode#TreeNode(TreeViewer)}
	 * @param model l'instance du mod�le associ�e � cet �l�ment (ne doit pas �tre <code>null</code>)
	 * @throws DauradeTreeException si le parent ou le module pass� en param�tre est <code>null</code>
	 */
	public TreeNode(ITreeNode parent, T model) {
		if (parent == null) {
			throw new IllegalArgumentException("Parent null");
		}
		if (model == null) {
			throw new IllegalArgumentException("Model null");
		}

		this.parent = parent;
		this.viewer = parent.getViewer();
		this.model = model;

		// parent.addChild(this);
		// mmodel.addPropertyChangeListener(this);
	}

	/**
	 * Ctor pour un noeud racine.
	 * 
	 * @param viewer le viewer associ� � ce noeud (ne peut �tre <code>null</code>)
	 * @param model l'instance du mod�le associ�e � cet �l�ment (ne doit pas �tre <code>null</code>)
	 * @throws IllegalArgumentException si le viewer ou le module pass� en param�tre est <code>null</code>
	 */
	public TreeNode(TreeViewer viewer, T model) {
		if (viewer == null || model == null) {
			throw new IllegalArgumentException();
		}

		// noeud racine
		this.parent = null;
		this.viewer = viewer;
		this.model = model;
		// mmodel.addPropertyChangeListener(this);
	}

	/**
	 * Permet de lib�rer les ressources associ�es au noeud lors de sa destruction.
	 */
	public void dispose() {
		// mmodel.removePropertyChangeListener(this);
	}

	/** {@inheritDoc} */
	@Override
	public Image getImage() {
		return null; // par defaut : aucune image
	}

	/** {@inheritDoc} */
	@Override
	public void decorate(Image img) {
	}

	/** {@inheritDoc} */
	@Override
	public Font getFont() {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public Color getForeground() {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public Color getBackground() {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public boolean hasChildren() {
		if (children != null) {
			return (children.size() > 0);
		} else {
			return false;
		}
	}

	/** {@inheritDoc} */
	@Override
	public ITreeNode getParent() {
		return parent;
	}

	public void setParent(ITreeNode parent) {
		this.parent = parent;
	}

	/** {@inheritDoc} */
	@Override
	public List<ITreeNode> getChildren() {
		if (children != null) {
			return children;
		} else {
			children = new ArrayList<ITreeNode>();
		}
		createChildren();
		return children;
	}

	/** {@inheritDoc} */
	@Override
	public TreeViewer getViewer() {
		return viewer;
	}

	/**
	 * Refresh this node.
	 */
	public void refresh(final boolean updateLabels) {
		// children = null;

		createChildren();
		// si le thread courant est le uithread alors on rafraichi directement
		if (Display.getCurrent() != null) {
			getViewer().refresh(this, updateLabels);
		} else {
			// Il faut un asyncexec pour eviter les problemes d'invalid thread
			// access car l'evenement peut avoir ete leve par un thread autre
			// que celui de l'ihm
			Display.getDefault().asyncExec(new Runnable() {

				@Override
				public void run() {

					try {
						getViewer().refresh(TreeNode.this, updateLabels);
					} catch (Exception e) {
						logger.error("Refresh error", e);
					}
				}
			});
		}
	}

	/**
	 * Cette methode permet l'initialisation des noeuds fils.
	 */
	protected abstract void createChildren();

	@Override
	public void addChild(ITreeNode node) {
		if (children == null) {
			children = new ArrayList<ITreeNode>();
		}
		children.add(node);
	}

	@Override
	public void removeChild(ITreeNode node) {
		children.remove(node);
	}

	/**
	 * L'instance modele associee a ce noeud.
	 * 
	 * @return l'instance modele associee a ce noeud.
	 */
	public T getModel() {
		return model;
	}

	/**
	 * Default behavior just refreshs this node.
	 * 
	 * Attention le listener n'est enregistre que si le noeud pere a ete deploye car c'est a ce moment la que les
	 * treenode filles sont créées
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		refresh(true);
	}

	@Override
	public boolean isChecked() {
		// checked if at least one child node is checked
		List<ITreeNode> children = getChildren();
		if (children != null) {
			for (ITreeNode tn : children) {
				if (tn instanceof Checkable) {
					if (((Checkable) tn).isChecked()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean isGrayed() {
		// if at least one child node is unchecked, this node should be grayed
		List<ITreeNode> children = getChildren();
		if (children != null) {
			for (ITreeNode tn : children) {
				if (tn instanceof Checkable) {
					if (!((Checkable) tn).isChecked()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/** Default behavior : propagate check. */
	@Override
	public void setChecked(boolean checked) {
		List<ITreeNode> children = getChildren();
		if (children != null) {
			for (ITreeNode child : children) {
				if (child instanceof Checkable) {
					((Checkable) child).setChecked(checked);
				}
			}
		}
	}

	public synchronized void addListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public synchronized void removeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	@Override
	public ITreeNode findNode(String name) {

		if (this.getName().equals(name)) {
			return this;
		}
		List<ITreeNode> children = this.getChildren();
		ITreeNode res = null;
		for (ITreeNode child : children) {
			res = child.findNode(name);
			if (res != null) {
				return res;
			}
		}
		return null;
	}

	@Override
	public ITreeNode findRootNode() {
		if (this.getParent() == null) {
			return this;
		} else {
			return this.getParent().findRootNode();
		}
	}

	/**
	 * @param model the {@link #model} to set
	 */
	public void setModel(T model) {
		this.model = model;
	}

}
