// Structure d'accueil Daurade
// 
// Application.java - V1.0 - mai.03 , 2009
// 
// Copyright (c) 2009 SHOM
// @Address@
// All rights reserved.
// -------------------------------------------------------------------------------------------

package fr.ifremer.globe.ui.tree;

import java.util.List;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;

/**
 * Interface representant un objet de l'arborescence.
 */
public interface ITreeNode {

	/**
	 * Recuperation du nom (label) de l'objet.
	 * 
	 * @return Renvoie le nom de l'objet
	 */
	public String getName();

	/**
	 * Renvoi l'image associee a l'objet.
	 * 
	 * @return l'image associee a l'objet
	 */
	public Image getImage();

	/**
	 * Add decoration (overlay) on top of provided image. Default implementation
	 * does nothing.
	 */
	public void decorate(Image img);

	/**
	 * Recuperation du parent de l'objet.
	 * 
	 * @return Renvoie le parent de l'objet
	 */
	public ITreeNode getParent();

	/**
	 * Renvoie la liste des elements fils du noeud ou une liste vide si aucun.
	 * 
	 * @return la liste des elements fils du noeud ou une liste vide si aucun.
	 */
	public List<ITreeNode> getChildren();

	/**
	 * Add a child to the node
	 * */
	public void addChild(ITreeNode node);

	/**
	 * Remove a child from the node
	 * */
	public void removeChild(ITreeNode node);

	/**
	 * Renvoie vrai si l'element e au moins un fils.
	 * 
	 * @return vrai si l'element e au moins un fils.
	 */
	public boolean hasChildren();

	/**
	 * Renvoie le treeviewer JFace associe e ce noeud.
	 * 
	 * @return le treeviewer JFace associe e ce noeud.
	 */
	public TreeViewer getViewer();

	/**
	 * Recuperation de la police associee e ce noeud.
	 * 
	 * @return la police associee e ce noeud
	 */
	public Font getFont();

	/**
	 * Recuperation de la couleur de premier plan pour ce noeud.
	 * 
	 * @return la couleur de premier plan pour ce noeud.
	 */
	public Color getForeground();

	/**
	 * Recuperation de la couleur d'arriere plan pour ce noeud.
	 * 
	 * @return la couleur d'arriere plan pour ce noeud.
	 */
	public Color getBackground();

	public ITreeNode findNode(String name);

	public ITreeNode findRootNode();

}
