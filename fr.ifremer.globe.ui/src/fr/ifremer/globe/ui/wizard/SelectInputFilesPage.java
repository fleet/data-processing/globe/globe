package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.typed.PojoProperties;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.conversion.NotNullToBooleanConverter;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.dnd.FileListDropListener;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.viewersorter.FilenameViewerSorter;

/**
 * Wizard page used to select the input files
 */
public class SelectInputFilesPage extends WizardPage implements ISummarizableWizardPage {

	/** Model in MVC */
	protected SelectInputFilesPageModel selectInputFilesPageModel;

	/** Widgets */
	protected ListViewer inputFilesListViewer;
	protected Button addSomeFilesButton;
	protected Button removeSomeFilesButton;
	protected Button removeAllFilesButton;
	protected List inputFilesList;

	/** Directory path that the FileDialog will use */
	protected File startingDirectory;

	/** List of files in the ListViewer */
	protected WritableList<File> writableInputFileList;

	/**
	 * do we allow to drop a list of file in the wizard page
	 */
	protected boolean enableFileListDrop;

	/**
	 * Constructor
	 */
	public SelectInputFilesPage(SelectInputFilesPageModel model) {
		super(SelectInputFilesPage.class.getName(), "Select input files", null);
		selectInputFilesPageModel = model;
		writableInputFileList = model.getInputFiles();

		java.util.List<String> filterNames = new ArrayList<>();
		selectInputFilesPageModel.getInputFilesFilterExtensions().stream()
				.forEach(ext -> filterNames.add(ext.getSecond()));
		setDescription(StringUtils.abbreviate(String.format("Accept files : %s",
				StringUtils.join(filterNames.toArray(new String[filterNames.size()]), " - ")), 120));
		// disable file list drop if any of input extension is txt or *
		enableFileListDrop = selectInputFilesPageModel.getInputFilesFilterExtensions().stream()
				.noneMatch(x -> x.getSecond().contains(".txt") || x.getSecond().contains("*.*"));

		updatePageComplete();
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(3, false));
		setControl(container);

		inputFilesListViewer = new ListViewer(container);
		inputFilesListViewer.setComparator(new FilenameViewerSorter());
		inputFilesList = inputFilesListViewer.getList();
		inputFilesList.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.DEL) {
					removeFiles();
				}
			}
		});
		GridData gdList = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 4);
		gdList.horizontalIndent = 10;
		inputFilesList.setLayoutData(gdList);

		addSomeFilesButton = new Button(container, SWT.NONE);
		addSomeFilesButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addFiles();
			}
		});
		addSomeFilesButton.setAlignment(SWT.LEFT);
		addSomeFilesButton.setImage(ImageResources.getImage("/icons/16/add.png", SelectInputFilesPage.class));
		GridData gdBtnAdd = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gdBtnAdd.horizontalIndent = 10;
		addSomeFilesButton.setLayoutData(gdBtnAdd);
		addSomeFilesButton.setText("Add");

		removeSomeFilesButton = new Button(container, SWT.NONE);
		removeSomeFilesButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeFiles();
			}
		});
		removeSomeFilesButton.setEnabled(false);
		removeSomeFilesButton.setAlignment(SWT.LEFT);
		removeSomeFilesButton.setImage(ImageResources.getImage("icons/16/delete.png", SelectInputFilesPage.class));
		GridData gdBtnRemove = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gdBtnRemove.horizontalIndent = 10;
		removeSomeFilesButton.setLayoutData(gdBtnRemove);
		removeSomeFilesButton.setText("Remove");

		removeAllFilesButton = new Button(container, SWT.NONE);
		removeAllFilesButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeAllFiles();
			}
		});
		removeAllFilesButton.setAlignment(SWT.LEFT);
		GridData gdBtnRemoveAll = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gdBtnRemoveAll.horizontalIndent = 10;
		removeAllFilesButton.setLayoutData(gdBtnRemoveAll);
		removeAllFilesButton.setText("Remove all");
		removeAllFilesButton.setImage(ImageResources.getImage("icons/16/delete.png", SelectInputFilesPage.class));
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
		lblNewLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		if (enableFileListDrop) {
			lblNewLabel.setText("Drop a .txt file which contains a list of absolute paths");

			lblNewLabel.setToolTipText(
					"File example:\nC:\\Users\\user\\Documents\\file1.all\nC:\\Users\\user\\Documents\\file2.all\nC:\\Users\\user\\Documents\\file3.all\n...");
		}

		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		// View/model bindings
		initDataBindings();

		initializeDropSupport();
	}

	/** Adds support for dropping files into this viewer */
	protected void initializeDropSupport() {
		int operations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_DEFAULT;
		Transfer[] transferTypes = new Transfer[] { FileTransfer.getInstance() };
		inputFilesListViewer.addDropSupport(operations, transferTypes, new FileListDropListener(inputFilesListViewer) {

			/**
			 * @see fr.ifremer.globe.ui.dnd.FileListDropListener#performDrop(java.util.List)
			 */
			@Override
			public boolean performDrop(java.util.List<File> files) {
				addFiles(files);
				return true;
			}
		});
	}

	/** Sets whether this page is complete */
	protected void updatePageComplete() {
		setPageComplete(
				!selectInputFilesPageModel.isMandatory() || !selectInputFilesPageModel.getInputFiles().isEmpty());
	}

	/**
	 * removes a file from the selection
	 */
	protected void removeFiles() {
		inputFilesList.setRedraw(false);
		java.util.List<?> selectedFiles = inputFilesListViewer.getStructuredSelection().toList();
		// Disconnect view from model to avoid the update processing when removing files
		inputFilesListViewer.setInput(null);
		writableInputFileList.removeAll(selectedFiles);
		inputFilesListViewer.setInput(writableInputFileList);
		updatePageComplete();
		inputFilesList.setRedraw(true);
	}

	/**
	 * removes all files
	 */
	protected void removeAllFiles() {
		inputFilesList.setRedraw(false);
		inputFilesListViewer.setSelection(null, false);
		// Disconnect view from model to avoid the update processing when removing files
		inputFilesListViewer.setInput(null);
		writableInputFileList.clear();
		inputFilesListViewer.setInput(writableInputFileList);
		updatePageComplete();
		inputFilesList.setRedraw(true);
	}

	/**
	 * adds a file to the selection
	 */
	protected void addFiles() {
		addFiles(chooseFile());
	}

	/**
	 * adds some files to the selection
	 */
	protected void addFiles(java.util.List<File> files) {
		if (files != null) {
			if (files.size() == 1) {
				File file = files.get(0);
				if (file.exists() && enableFileListDrop
						&& "txt".equals(FilenameUtils.getExtension(file.getName().toLowerCase()))) {
					addListOfFiles(file);
				} else {
					addSpecifiedfFiles(files);
				}
			} else {
				addSpecifiedfFiles(files);
			}
		}
	}

	/**
	 * adds some files to the selection
	 *
	 * @param file text file containing the list of file
	 */
	protected void addListOfFiles(File file) {
		try {
			java.util.List<String> filenames = Files.readAllLines(file.toPath());
			java.util.List<File> addedFiles = new ArrayList<>();
			filenames.forEach(filename -> addedFiles.add(new File(filename)));
			addSpecifiedfFiles(addedFiles);
		} catch (IOException e) {
			Messages.openErrorMessage("Selecting file error",
					"Unable to add files contained in " + file.getName() + "\n" + e.getMessage());
		}
	}

	/**
	 * adds some files to the selection
	 */
	protected void addSpecifiedfFiles(java.util.List<File> files) {

		inputFilesList.setRedraw(false);
		// Disconnect view from model to avoid the update processing when removing files
		inputFilesListViewer.setInput(null);

		java.util.List<File> addedFiles = new ArrayList<>();
		if (files != null && !files.isEmpty()) {
			java.util.List<String> filterExtensions = new ArrayList<>();
			for (Pair<String, String> pair : selectInputFilesPageModel.getInputFilesFilterExtensions()) {
				Arrays.stream(pair.getSecond().split(";"))
						.forEach(ext -> filterExtensions.add(FilenameUtils.getExtension(ext)));
			}

			// Extract unknown files
			for (File file : files) {
				boolean extensionAcceptable = filterExtensions.contains("*") // all extension accepted
						|| filterExtensions.contains(FilenameUtils.getExtension(file.getName()));
				if (extensionAcceptable && !writableInputFileList.contains(file)) {
					addedFiles.add(file);
				}
			}

			// Select new files
			writableInputFileList.addAll(addedFiles);
		}
		// Connect view from model
		inputFilesListViewer.setInput(writableInputFileList);

		Display.getDefault().asyncExec(() -> {
			// Update selection
			inputFilesListViewer.setSelection(new StructuredSelection(addedFiles), false);
			// Focus to allow the suppression of the new files with Del key.
			inputFilesList.setFocus();
			inputFilesList.setRedraw(true);
			updatePageComplete();
		});
	}

	/**
	 * Helper to open the file chooser dialog.
	 */
	protected java.util.List<File> chooseFile() {
		java.util.List<File> result = new ArrayList<>();
		FileDialog dialog = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
		if (startingDirectory != null) {
			dialog.setFilterPath(startingDirectory.getPath());
		}

		java.util.List<String> filterExtensions = new ArrayList<>();
		selectInputFilesPageModel.getInputFilesFilterExtensions().stream()
				.forEach(ext -> filterExtensions.add(ext.getSecond()));
		filterExtensions.add("*.txt");
		dialog.setFilterExtensions(new String[] { String.join(";", filterExtensions) });

		java.util.List<String> filterNames = new ArrayList<>();
		selectInputFilesPageModel.getInputFilesFilterExtensions().stream()
				.forEach(ext -> filterNames.add(ext.getFirst()));
		filterNames.add("List of file (*.txt)");
		dialog.setFilterNames(new String[] { String.join(";", filterNames) });

		if (dialog.open() != null) {
			String[] fileNames = dialog.getFileNames();
			for (String fileName : fileNames) {
				File file = new File(dialog.getFilterPath(), fileName);
				result.add(file);
				startingDirectory = file.getParentFile();
			}
		}
		return result;
	}

	/**
	 * Model of the input file selection wizard page
	 */
	public interface SelectInputFilesPageModel {

		/**
		 * @return the files
		 */
		WritableObjectList<File> getInputFiles();

		/**
		 * @return True when at leat one file must be selected
		 */
		default boolean isMandatory() {
			return true;
		}

		/**
		 * Getter of filterExtensions
		 */
		java.util.List<Pair<String, String>> getInputFilesFilterExtensions();

	}

	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			updatePageComplete();
		}
		super.setVisible(visible);
	}

	@SuppressWarnings("unchecked")
	protected void initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		var listContentProvider = new ObservableListContentProvider<File>();
		var observeMap = PojoProperties.value(File.class, "absolutePath")
				.observeDetail(listContentProvider.getKnownElements());
		inputFilesListViewer.setLabelProvider(new ObservableMapLabelProvider(observeMap));
		inputFilesListViewer.setContentProvider(listContentProvider);
		//
		inputFilesListViewer.setInput(writableInputFileList);
		//
		var observeSingleSelectionInputFilesListViewer = ViewerProperties.singleSelection()
				.observe(inputFilesListViewer);
		var enabledBtnRemoveObserveValue = WidgetProperties.enabled().observe(removeSomeFilesButton);
		var strategy = UpdateValueStrategy.create(new NotNullToBooleanConverter());
		bindingContext.bindValue(observeSingleSelectionInputFilesListViewer, enabledBtnRemoveObserveValue, strategy,
				new UpdateValueStrategy<>(UpdateValueStrategy.POLICY_NEVER));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		StringBuilder sb = new StringBuilder();
		for (File file : selectInputFilesPageModel.getInputFiles()) {
			if (sb.length() > 0) {
				sb.append('\n');
			}
			sb.append(file.getAbsolutePath());
		}
		return sb.toString();
	}
}
