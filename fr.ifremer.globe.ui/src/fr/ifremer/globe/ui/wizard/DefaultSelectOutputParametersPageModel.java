package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.util.Optional;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;

/**
 * Default implementation of {@link SelectOutputParametersPageModel}.
 */
public class DefaultSelectOutputParametersPageModel implements SelectOutputParametersPageModel {

	/** Output file name */
	private final WritableString singleOutputFilename = new WritableString();

	/** Output directory */
	private final WritableFile outputDirectory = new WritableFile();

	/** List of input Files */
	private final WritableObjectList<File> inputFiles = new WritableObjectList<>();

	/** List of output Files */
	private final WritableObjectList<File> outputFiles = new WritableObjectList<>();

	/** Extension of output files */
	private final WritableObjectList<String> outputFormatExtensions = new WritableObjectList<>();

	/** Prefix of output files */
	private final WritableString prefix = new WritableString();

	/** Suffix of output files */
	private final WritableString suffix = new WritableString();

	/** Overwrite existing files */
	private final WritableBoolean overwriteExistingFiles = new WritableBoolean(true);

	/** True to load files at the end of process */
	private final WritableBoolean loadFilesAfter = new WritableBoolean(false);
	/** Where to load the files (in witch group of project explorer) */
	private final WritableString whereToloadFiles = new WritableString();

	/** is merged in a single file */
	private final Optional<WritableBoolean> mergeOutputFiles = Optional.empty();

	// GETTERS

	@Override
	public WritableFile getOutputDirectory() {
		return outputDirectory;
	}

	@Override
	public WritableString getPrefix() {
		return prefix;
	}

	@Override
	public WritableString getSuffix() {
		return suffix;
	}

	@Override
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	@Override
	public WritableObjectList<String> getOutputFormatExtensions() {
		return outputFormatExtensions;
	}

	@Override
	public WritableObjectList<File> getOutputFiles() {
		return outputFiles;
	}

	@Override
	public WritableBoolean getOverwriteExistingFiles() {
		return overwriteExistingFiles;
	}

	@Override
	public WritableBoolean getLoadFilesAfter() {
		return loadFilesAfter;
	}

	/**
	 * Getter of mergeOutputFiles
	 */
	@Override
	public Optional<WritableBoolean> getMergeOutputFiles() {
		return mergeOutputFiles;
	}

	/** Getter of {@link #singleOutputFilename} */
	@Override
	public WritableString getSingleOutputFilename() {
		return singleOutputFilename;
	}

	/**
	 * @return the {@link #whereToloadFiles}
	 */
	@Override
	public WritableString getWhereToloadFiles() {
		return whereToloadFiles;
	}

}