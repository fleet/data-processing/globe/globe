package fr.ifremer.globe.ui.wizard;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

public class CustomWizardDialog extends WizardDialog {

	private Button convertButton;
	private Button finishButton;
	private static final int PROCEED_ID = 10;
	private static final String PROCEED_LABEL = "Convert";
	private Runnable runnable;


	public CustomWizardDialog(Shell parentShell, IWizard newWizard, Runnable runnable) {
		super(parentShell, newWizard);
		this.runnable=runnable;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);

		// Add convert buttons to the Wizard Dialog
		convertButton = super.createButton(parent, PROCEED_ID, PROCEED_LABEL, false);
		setButtonLayoutData(convertButton);

		// Get the existing Finish button
		finishButton = getButton(IDialogConstants.FINISH_ID);

		// Move created buttons above the Finish button
		convertButton.moveAbove(finishButton);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.PROCEED_ID) {
			runnable.run();
		}

	}

	@Override
	public void updateButtons() {

		boolean canFinish = getWizard().canFinish();

		finishButton.setEnabled(canFinish);
		convertButton.setEnabled(canFinish);

		// finish is default unless it is disabled and next is enabled
		if (!canFinish) {
			getShell().setDefaultButton(finishButton);
			getShell().setDefaultButton(convertButton);
		}

	}

}