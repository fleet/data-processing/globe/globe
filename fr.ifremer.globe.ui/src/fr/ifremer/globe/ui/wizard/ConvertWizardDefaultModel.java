package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage.SelectInputFilesPageModel;

/**
 * Default model for convert wizard : contains "input files" and "output parameters" page models.
 */
public class ConvertWizardDefaultModel implements SelectInputFilesPageModel, SelectOutputParametersPageModel {

	/** List of Files */
	protected final WritableObjectList<File> inputFiles = new WritableObjectList<>();
	/** List of file Extensions */
	protected final List<Pair<String, String>> inputFilesFilterExtensions = new ArrayList<>();

	/** Output file name */
	protected final WritableString singleOutputFilename = new WritableString();
	/** Output directory */
	protected final WritableFile outputDirectory = new WritableFile();
	/** Extension of output files */
	protected final WritableObjectList<String> outputFormatExtensions = new WritableObjectList<>();
	/** Prefix of output files */
	protected final WritableString prefix = new WritableString();
	/** Suffix of output files */
	protected final WritableString suffix = new WritableString();
	/** List of output Files */
	protected final WritableObjectList<File> outputFiles = new WritableObjectList<>();
	/** Overwrite existing files */
	protected final WritableBoolean overwriteExistingFiles = new WritableBoolean();
	/** True to load files at the end of process */
	protected final WritableBoolean loadFilesAfter = new WritableBoolean(false);
	/** Where to load the files (in witch group of project explorer) */
	protected final WritableString whereToloadFiles = new WritableString();

	/**
	 * Getter of inputFiles
	 */
	@Override
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	/**
	 * Getter of inputFilesFilterExtensions
	 */
	@Override
	public List<Pair<String, String>> getInputFilesFilterExtensions() {
		return inputFilesFilterExtensions;
	}

	/**
	 * Getter of outputDirectory
	 */
	@Override
	public WritableFile getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * Getter of outputFormatExtensions
	 */
	@Override
	public WritableObjectList<String> getOutputFormatExtensions() {
		return outputFormatExtensions;
	}

	/**
	 * Getter of prefix
	 */
	@Override
	public WritableString getPrefix() {
		return prefix;
	}

	/**
	 * Getter of suffix
	 */
	@Override
	public WritableString getSuffix() {
		return suffix;
	}

	/**
	 * Getter of outputFiles
	 */
	@Override
	public WritableObjectList<File> getOutputFiles() {
		return outputFiles;
	}

	/**
	 * Getter of overwriteExistingFiles
	 */
	@Override
	public WritableBoolean getOverwriteExistingFiles() {
		return overwriteExistingFiles;
	}

	/**
	 * Getter of loadFilesAfter
	 */
	@Override
	public WritableBoolean getLoadFilesAfter() {
		return loadFilesAfter;
	}

	/** Getter of {@link #singleOutputFilename} */
	@Override
	public WritableString getSingleOutputFilename() {
		return singleOutputFilename;
	}

	/**
	 * @return the {@link #whereToloadFiles}
	 */
	@Override
	public WritableString getWhereToloadFiles() {
		return whereToloadFiles;
	}

}