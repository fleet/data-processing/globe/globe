package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.wizard.WizardPageSupport;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.application.prefs.Preferences;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.dialogs.FolderNodeChooser;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.viewersorter.FilenameViewerSorter;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.widget.FileComposite;
import fr.ifremer.globe.utils.FileUtils;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Wizard page used to select the input files
 */
public class SelectOutputParametersPage extends WizardPage implements ISummarizableWizardPage {

	protected Logger logger = LoggerFactory.getLogger(SelectOutputParametersPage.class);

	/** Preferences of the bundle. */
	@Inject
	private Preferences preferences;

	public static final String WARNING_TEXT = "* file already exists ( %d )";

	/** Model in MVC */
	protected final SelectOutputParametersPageModel selectOutputParametersPageModel;

	/** Widgets */
	protected FileComposite outDirComposite;
	protected TableViewer outputFilesTableViewer;
	protected Table ouputFilesTable;
	protected Label prefixLabel;
	protected Text prefixText;
	protected Label suffixLabel;
	protected Text suffixText;
	protected Button btnOverwriteExistingFiles;
	protected Composite loadFilesAfterComposite;
	protected Button btnLoadFilesAfter;
	protected Button btnChooseFolder;
	protected Label nbOfExistingFileLabel;
	protected Label singleFilenameLabel;
	protected Text singleFilenameText;
	protected Button singleFilenameBtn;
	protected Button mergeOutputFilesButton;

	/** View/model bindings */
	protected DataBindingContext dataBindingContext = new DataBindingContext();

	/** Tool to compute an output file name */
	protected OutputFileNameComputer outputFileNameComputer = new OutputFileNameComputer();

	/** Define if check boxes are available or not */
	protected boolean enableLoadFilesAfter = true;
	protected boolean enableMergeOutputFiles = true;
	protected boolean enableOverwriteExistingFiles = true;

	/** Subject that emits a request to compute output files */
	protected PublishSubject<SelectOutputParametersPageModel> computeOutputFilesSubject = PublishSubject.create();
	protected Disposable computeOutputFilesSubscription;

	/**
	 * Constructor
	 */
	public SelectOutputParametersPage(String title, String description, SelectOutputParametersPageModel model) {
		super(SelectOutputParametersPage.class.getName(), title, null);
		ContextInitializer.inject(this);

		setDescription(description);
		selectOutputParametersPageModel = model;
		computeOutputFilesSubscription = computeOutputFilesSubject //
				.toFlowable(BackpressureStrategy.LATEST) //
				.observeOn(Schedulers.computation()) //
				.subscribe(event -> computeOutputFiles(),
						error -> logger.warn("Error while computing output files", error));
	}

	/**
	 * Constructor
	 *
	 * @wbp.parser.constructor
	 */
	public SelectOutputParametersPage(SelectOutputParametersPageModel model) {
		this("Select output parameters", "", model);
	}

	/** Compute model attributes */
	protected void bindModel() {
		IChangeListener resetOutputFiles = e -> resetOutputFiles();
		selectOutputParametersPageModel.getInputFiles().addChangeListener(resetOutputFiles);

		IChangeListener computeOutputFiles = e -> requestOutputFilesComputation();
		selectOutputParametersPageModel.getInputFiles().addChangeListener(computeOutputFiles);
		selectOutputParametersPageModel.getSingleOutputFilename().addChangeListener(computeOutputFiles);
		selectOutputParametersPageModel.getOutputFormatExtensions().addChangeListener(computeOutputFiles);
		selectOutputParametersPageModel.getOutputDirectory().addChangeListener(computeOutputFiles);
		selectOutputParametersPageModel.getPrefix().addChangeListener(computeOutputFiles);
		selectOutputParametersPageModel.getSuffix().addChangeListener(computeOutputFiles);

		Optional<WritableBoolean> optionalMergeOutputFiles = selectOutputParametersPageModel.getMergeOutputFiles();
		if (optionalMergeOutputFiles.isPresent()) {
			optionalMergeOutputFiles.get().addChangeListener(computeOutputFiles);
		}
	}

	/**
	 * Displays suffix/prefix or single file input.
	 */
	protected void updateInputFileWidgets() {
		boolean oneOutputFile = selectOutputParametersPageModel.getOutputFiles().size() == 1;
		Optional<WritableBoolean> optionalMergeOutputFiles = selectOutputParametersPageModel.getMergeOutputFiles();
		boolean mergeOuputEnabled = optionalMergeOutputFiles.isPresent() && optionalMergeOutputFiles.get().isTrue();

		if (oneOutputFile || mergeOuputEnabled) {
			UIUtils.setVisible(prefixLabel, false);
			UIUtils.setVisible(prefixText, false);
			UIUtils.setVisible(suffixLabel, false);
			UIUtils.setVisible(suffixText, false);
			if (enableMergeOutputFiles) {
				UIUtils.setVisible(mergeOutputFilesButton, !oneOutputFile || mergeOuputEnabled);
			}
			UIUtils.setVisible(singleFilenameLabel, true);
			UIUtils.setVisible(singleFilenameText.getParent(), true);
		} else {
			UIUtils.setVisible(prefixLabel, true);
			UIUtils.setVisible(prefixText, true);
			UIUtils.setVisible(suffixLabel, true);
			UIUtils.setVisible(suffixText, true);
			if (enableMergeOutputFiles) {
				UIUtils.setVisible(mergeOutputFilesButton,
						selectOutputParametersPageModel.getMergeOutputFiles().isPresent());
			}
			UIUtils.setVisible(singleFilenameLabel, false);
			UIUtils.setVisible(singleFilenameText.getParent(), false);
		}
	}

	/** Compute all output files */
	protected void resetOutputFiles() {
		selectOutputParametersPageModel.getOutputFiles().clear();
		selectOutputParametersPageModel.getSingleOutputFilename().set(null);
	}

	/** Compute all output files */
	protected void computeOutputFiles() {
		Display.getDefault().syncExec(() -> {
			WritableObjectList<File> inputFiles = selectOutputParametersPageModel.getInputFiles();
			WritableFile outputDirectory = selectOutputParametersPageModel.getOutputDirectory();
			Optional<WritableBoolean> optionalMergeOutputFiles = selectOutputParametersPageModel.getMergeOutputFiles();
			boolean mergeOuputEnabled = optionalMergeOutputFiles.isPresent() && optionalMergeOutputFiles.get().isTrue();

			// set OutputDirectory with input file directory if null
			if (!inputFiles.isEmpty())
				outputDirectory.setIfNull(inputFiles.get(0)::getParentFile);

			// Computing output files
			if (outputDirectory.isNotNull()) {
				// Has to compute only one output file when merging
				boolean computingSingleOutputFile = mergeOuputEnabled;
				// Or when only one file in input list produces only one output file
				computingSingleOutputFile |= inputFiles.size() == 1
						&& selectOutputParametersPageModel.getOutputFormatExtensions(inputFiles.get(0)).size() <= 1;
				// Or when a single input file produces only one output file
				String singleOutputFilename = selectOutputParametersPageModel.getSingleOutputFilename().getValue();
				computingSingleOutputFile |= inputFiles.isEmpty() && singleOutputFilename != null
						&& selectOutputParametersPageModel.getOutputFormatExtensions(singleOutputFilename).size() <= 1;

				if (computingSingleOutputFile) {
					selectOutputParametersPageModel.getOutputFiles().clear();
					computeSingleOutputFile();
				} else {
					selectOutputParametersPageModel.getSingleOutputFilename().set(null);
					computeSeveralOutputFiles();
				}
			}

			updateExistingFileLabel();
			updateInputFileWidgets();
		});
	}

	/** Compute all output files */
	protected void computeSeveralOutputFiles() {
		List<File> inputFiles = selectOutputParametersPageModel.getInputFiles().asList();

		Optional<WritableBoolean> optionalMergeOutputFiles = selectOutputParametersPageModel.getMergeOutputFiles();

		List<File> outputFileaList = new ArrayList<>();
		if (!inputFiles.isEmpty() && selectOutputParametersPageModel.getOutputDirectory().isNotNull()) {
			for (File file : inputFiles) {
				List<String> outputFormatExtensions = selectOutputParametersPageModel.getOutputFormatExtensions(file);
				if (!outputFormatExtensions.isEmpty()) {
					for (String extension : outputFormatExtensions) {
						File outputFile = outputFileNameComputer.compute(file, selectOutputParametersPageModel,
								extension);
						outputFileaList.add(outputFile);
					}
				} else {
					// Same extension as input file
					File outputFile = outputFileNameComputer.compute(file, selectOutputParametersPageModel,
							FileUtils.getExtension(file));
					outputFileaList.add(outputFile);
				}
				if (optionalMergeOutputFiles.isPresent() && optionalMergeOutputFiles.get().isTrue()) {
					break;
				}
			}

		}

		ouputFilesTable.setRedraw(false);
		WritableObjectList<File> outputFiles = selectOutputParametersPageModel.getOutputFiles();
		outputFiles.clear();
		outputFiles.addAll(outputFileaList);
		ouputFilesTable.setRedraw(true);
	}

	/** Compute all output files */
	protected void updateExistingFileLabel() {
		if (nbOfExistingFileLabel != null && !nbOfExistingFileLabel.isDisposed()) {
			int nbOfFile = (int) selectOutputParametersPageModel.getOutputFiles().stream().filter(File::exists).count();
			String nbOfFileToSet = nbOfFile > 0 ? String.format(WARNING_TEXT, nbOfFile) : "";
			nbOfExistingFileLabel.setText(nbOfFileToSet);
			nbOfExistingFileLabel.getParent().layout();
		}
	}

	/** Compute single output file name */
	protected void computeSingleOutputFile() {
		WritableString singleOutputFilename = selectOutputParametersPageModel.getSingleOutputFilename();
		if (singleOutputFilename.isEmpty() && !selectOutputParametersPageModel.getInputFiles().isEmpty()) {
			// Set the output filename for the first time
			File inputFile = selectOutputParametersPageModel.getInputFiles().get(0);
			List<String> outputFormatExtensions = selectOutputParametersPageModel.getOutputFormatExtensions(inputFile);
			// Same extension as input file ?
			String extension = outputFormatExtensions.isEmpty() ? FileUtils.getExtension(inputFile)
					: outputFormatExtensions.get(0);
			// Compute output file
			File outputFile = outputFileNameComputer.compute(inputFile, selectOutputParametersPageModel, extension);
			singleOutputFilename.set(outputFile.getName());
		} else if (!singleOutputFilename.isEmpty()
				&& !selectOutputParametersPageModel.getOutputFormatExtensions().isEmpty()) {
			// Change only the extension if need be
			var expectedExtensions = selectOutputParametersPageModel.getOutputFormatExtensions();
			if (expectedExtensions.stream().noneMatch(singleOutputFilename.get()::endsWith)) {
				String newName = FileUtils.getBaseName(singleOutputFilename.get());
				newName += newName.endsWith(".") ? expectedExtensions.get(0) : "." + expectedExtensions.get(0);
				singleOutputFilename.set(newName);
			}
		}

		if (!singleOutputFilename.isEmpty() && selectOutputParametersPageModel.getOutputDirectory().isNotNull()) {
			WritableObjectList<File> outputFiles = selectOutputParametersPageModel.getOutputFiles();
			outputFiles.add(
					new File(selectOutputParametersPageModel.getOutputDirectory().get(), singleOutputFilename.get()));
		}
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite mainContainer = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(mainContainer);

		GridLayoutFactory.fillDefaults().numColumns(3).extendedMargins(10, 10, 10, 0).applyTo(mainContainer);
		setControl(mainContainer);

		Label lblNewLabel = new Label(mainContainer, SWT.NONE);
		lblNewLabel.setText("Output directory");
		GridDataFactory.defaultsFor(lblNewLabel).applyTo(lblNewLabel);

		outDirComposite = new FileComposite(mainContainer, SWT.NONE,
				selectOutputParametersPageModel.getOutputDirectory(), FileValidator.EXISTS | FileValidator.FOLDER,
				dataBindingContext);
		GridLayout gridLayout = (GridLayout) outDirComposite.getLayout();
		gridLayout.verticalSpacing = 0;
		gridLayout.marginHeight = 0;
		gridLayout.marginLeft = 5;
		GridData gdFileComposite = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gdFileComposite.horizontalSpan = 2;
		outDirComposite.setLayoutData(gdFileComposite);

		singleFilenameLabel = new Label(mainContainer, SWT.NONE);
		singleFilenameLabel.setText("File name");
		GridDataFactory.defaultsFor(singleFilenameLabel).applyTo(singleFilenameLabel);

		Composite singleFilenameContainer = new Composite(mainContainer, SWT.NONE);
		GridData gdSingleFilenameContainer = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		singleFilenameContainer.setLayoutData(gdSingleFilenameContainer);
		GridLayout gl_singleFilenameContainer = new GridLayout(2, false);
		gl_singleFilenameContainer.verticalSpacing = 0;
		gl_singleFilenameContainer.marginWidth = 0;
		gl_singleFilenameContainer.marginHeight = 0;
		singleFilenameContainer.setLayout(gl_singleFilenameContainer);

		singleFilenameText = new Text(singleFilenameContainer, SWT.BORDER);
		GridData gdFilename = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdFilename.horizontalIndent = 5;
		singleFilenameText.setLayoutData(gdFilename);

		singleFilenameBtn = new Button(singleFilenameContainer, SWT.PUSH);
		singleFilenameBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectSingleFilename();
			}
		});
		GridData gdSingleFilenameBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdSingleFilenameBtn.horizontalIndent = 5;
		singleFilenameBtn.setLayoutData(gdSingleFilenameBtn);
		singleFilenameBtn.setText(" ... ");

		prefixLabel = new Label(mainContainer, SWT.NONE);
		prefixLabel.setText("Prefix");
		GridDataFactory.defaultsFor(prefixLabel).applyTo(prefixLabel);

		prefixText = new Text(mainContainer, SWT.BORDER);
		GridData gdPrefixText = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gdPrefixText.horizontalIndent = 5;
		prefixText.setLayoutData(gdPrefixText);

		suffixLabel = new Label(mainContainer, SWT.NONE);
		suffixLabel.setText("Suffix");
		GridDataFactory.defaultsFor(suffixLabel).applyTo(suffixLabel);

		suffixText = new Text(mainContainer, SWT.BORDER);
		GridData gdSuffixText = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gdSuffixText.horizontalIndent = 5;
		suffixText.setLayoutData(gdSuffixText);

		Label lblOutputFiles = new Label(mainContainer, SWT.NONE);
		GridDataFactory.defaultsFor(singleFilenameLabel).indent(0, 15).applyTo(lblOutputFiles);
		lblOutputFiles.setText("Output files :");

		mergeOutputFilesButton = new Button(mainContainer, SWT.CHECK);
		GridData gdMergeOutputFilesButton = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false, 1, 1);
		gdMergeOutputFilesButton.verticalIndent = 20;
		mergeOutputFilesButton.setLayoutData(gdMergeOutputFilesButton);
		mergeOutputFilesButton.setText("Merge in one file");
		UIUtils.setVisible(mergeOutputFilesButton,
				enableMergeOutputFiles && selectOutputParametersPageModel.getMergeOutputFiles().isPresent());

		nbOfExistingFileLabel = new Label(mainContainer, SWT.NONE);
		nbOfExistingFileLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		nbOfExistingFileLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, true, false));
		GridDataFactory.swtDefaults().align(SWT.RIGHT, SWT.BOTTOM).grab(true, false).applyTo(nbOfExistingFileLabel);

		outputFilesTableViewer = new TableViewer(mainContainer, SWT.BORDER | SWT.HIDE_SELECTION);
		outputFilesTableViewer.setComparator(new FilenameViewerSorter());
		ouputFilesTable = outputFilesTableViewer.getTable();
		ouputFilesTable.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		ouputFilesTable.addListener(SWT.EraseItem, event -> {
			// Disable selection !
			event.detail &= ~SWT.SELECTED;
			event.detail &= ~SWT.HOT;
		});
		GridData gdOuputFilesList = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		gdOuputFilesList.widthHint = 559;
		ouputFilesTable.setLayoutData(gdOuputFilesList);

		btnOverwriteExistingFiles = new Button(mainContainer, SWT.CHECK);
		GridData gdBtnOverwriteExistingFiles = new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1);
		gdBtnOverwriteExistingFiles.verticalIndent = 10;
		btnOverwriteExistingFiles.setLayoutData(gdBtnOverwriteExistingFiles);
		btnOverwriteExistingFiles.setText("Overwrite existing files (If not selected, existing file will passed.)");
		UIUtils.setVisible(btnOverwriteExistingFiles, enableOverwriteExistingFiles);

		loadFilesAfterComposite = new Composite(mainContainer, SWT.NONE);
		GridLayout loadFilesAfterGridLayout = new GridLayout(2, false);
		loadFilesAfterGridLayout.marginWidth = 0;
		loadFilesAfterGridLayout.horizontalSpacing = 0;
		loadFilesAfterComposite.setLayout(loadFilesAfterGridLayout);
		loadFilesAfterComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		btnLoadFilesAfter = new Button(loadFilesAfterComposite, SWT.CHECK);
		btnLoadFilesAfter.setLayoutData(new GridData(SWT.LEFT, SWT.END, false, false, 1, 1));
		btnLoadFilesAfter.setText("Load files after conversion into folder :");

		btnChooseFolder = new Button(loadFilesAfterComposite, SWT.PUSH);
		btnChooseFolder.setLayoutData(new GridData(SWT.LEFT, SWT.END, true, false, 1, 1));
		String whereToloadFiles = selectOutputParametersPageModel.getWhereToloadFiles().get();
		btnChooseFolder.setText(whereToloadFiles != null && !whereToloadFiles.isEmpty() ? whereToloadFiles : "/Data");
		btnChooseFolder.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> chooseFolder()));
		enableLoadFilesAfter(enableLoadFilesAfter);

		// Data binding
		initDataBindings();
		bindModel();
		bindOutputFiles();

		// Preferences
		selectOutputParametersPageModel.getLoadFilesAfter().set(preferences.getLoadFilesAfterConversion().getValue());
		// Send model to view
		dataBindingContext.updateTargets();

		computeOutputFiles();

		// PageComplete flag is automatically manage by observing Validators
		WizardPageSupport.create(this, dataBindingContext);
	}

	private void chooseFolder() {
		FolderNodeChooser dialog = new FolderNodeChooser(getShell(), "Project Explorer", folder -> {
			String path = folder.getName();
			Optional<GroupNode> parentFolder = folder.getParent();
			while (parentFolder.isPresent()) {
				path = parentFolder.get().getName() + "/" + path;
				parentFolder = parentFolder.get().getParent();
			}
			btnChooseFolder.setText(path);
			btnChooseFolder.requestLayout();
			selectOutputParametersPageModel.getWhereToloadFiles().set(path);
		});
		dialog.open();
	}

	/**
	 * Model of the input file selection wizard page
	 */
	public static class OutputFilesLabelProvider extends LabelProvider implements IColorProvider {

		/**
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(Object element) {
			return ((File) element).getAbsolutePath();
		}

		@Override
		public Color getForeground(Object element) {
			return ((File) element).exists() ? SWTResourceManager.getColor(SWT.COLOR_RED) : null;
		}

		@Override
		public Color getBackground(Object element) {
			return null;
		}

	}

	/** Initialize a specific binding on CheckBoxGroup (not managed by Windows Builder */
	protected void bindOutputFiles() {
		outputFilesTableViewer.setLabelProvider(new OutputFilesLabelProvider());
		outputFilesTableViewer.setContentProvider(new ObservableListContentProvider<File>());
		outputFilesTableViewer.setInput(selectOutputParametersPageModel.getOutputFiles());
	}

	/**
	 * Getter of outputFileNameComputer
	 */
	public OutputFileNameComputer getOutputFileNameComputer() {
		return outputFileNameComputer;
	}

	/**
	 * Setter of outputFileNameComputer
	 */
	public void setOutputFileNameComputer(OutputFileNameComputer outputFileNameComputer) {
		this.outputFileNameComputer = outputFileNameComputer;
	}

	/**
	 * Enables btnOverwriteExistingFiles
	 */
	public void enableOverwriteExistingFiles(boolean enabled) {
		enableOverwriteExistingFiles = enabled;
		if (btnOverwriteExistingFiles != null) {
			btnOverwriteExistingFiles.getDisplay().asyncExec(() -> {
				UIUtils.setVisible(btnOverwriteExistingFiles, enabled);
			});
		}
	}

	/**
	 * Enables btnLoadFilesAfter
	 */
	public void enableLoadFilesAfter(boolean enabled) {
		enableLoadFilesAfter = enabled;
		UIUtils.asyncExecSafety(loadFilesAfterComposite, () -> UIUtils.setVisible(loadFilesAfterComposite, enabled));
	}

	/**
	 * @param enableMergeOutputFiles the {@link #enableMergeOutputFiles} to set
	 */
	public void enableMergeOutputFiles(boolean enabled) {
		enableMergeOutputFiles = enabled;
		if (mergeOutputFilesButton != null) {
			mergeOutputFilesButton.getDisplay().asyncExec(() -> {
				UIUtils.setVisible(mergeOutputFilesButton, enabled);
			});
		}
	}

	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#dispose()
	 */
	@Override
	public void dispose() {
		// Save preference
		preferences.getLoadFilesAfterConversion().setValue(selectOutputParametersPageModel.getLoadFilesAfter().get(),
				false);
		preferences.save();

		super.dispose();
		computeOutputFilesSubscription.dispose();
		computeOutputFilesSubject.unsubscribeOn(Schedulers.computation());
	}

	protected void initDataBindings() {
		//
		IObservableValue<?> observeTextSuffixTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(suffixText);
		dataBindingContext.bindValue(observeTextSuffixTextObserveWidget, selectOutputParametersPageModel.getSuffix(),
				null, null);
		//
		IObservableValue<?> observeTextPrefixTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(prefixText);
		dataBindingContext.bindValue(observeTextPrefixTextObserveWidget, selectOutputParametersPageModel.getPrefix(),
				null, null);
		//
		IObservableValue<?> observeSelectionBtnOverwriteExistingFilesObserveWidget = WidgetProperties.widgetSelection()
				.observe(btnOverwriteExistingFiles);
		dataBindingContext.bindValue(observeSelectionBtnOverwriteExistingFilesObserveWidget,
				selectOutputParametersPageModel.getOverwriteExistingFiles(), null, null);
		//
		IObservableValue<?> observeSelectionBtnLoadFilesAfterObserveWidget = WidgetProperties.widgetSelection()
				.observe(btnLoadFilesAfter);
		dataBindingContext.bindValue(observeSelectionBtnLoadFilesAfterObserveWidget,
				selectOutputParametersPageModel.getLoadFilesAfter(), null, null);
		// WhereToloadFiles : bind enable
		IObservableValue<?> observeEnableWhereToloadFilesObserveWidget = WidgetProperties.enabled()
				.observe(btnChooseFolder);
		dataBindingContext.bindValue(observeEnableWhereToloadFilesObserveWidget,
				selectOutputParametersPageModel.getLoadFilesAfter(), null, null);
		//
		Optional<WritableBoolean> optionalMergeOutputFiles = selectOutputParametersPageModel.getMergeOutputFiles();
		if (optionalMergeOutputFiles.isPresent()) {
			IObservableValue<?> observeSelectionMergeOutputFilesButtonObserveWidget = WidgetProperties.widgetSelection()
					.observe(mergeOutputFilesButton);
			dataBindingContext.bindValue(observeSelectionMergeOutputFilesButtonObserveWidget,
					optionalMergeOutputFiles.get(), null, null);
		}
		//

		UpdateValueStrategy<String, String> targetToModel = new UpdateValueStrategy<>();
		targetToModel.setAfterConvertValidator(new FileNameValidator());

		IObservableValue<String> observeTextSingleFilenameObserveWidget = WidgetProperties.text(SWT.Modify)
				.observe(singleFilenameText);
		Binding textFileBinding = dataBindingContext.bindValue(observeTextSingleFilenameObserveWidget,
				selectOutputParametersPageModel.getSingleOutputFilename(), targetToModel, null);

		ControlDecorationSupport.create(textFileBinding, SWT.TOP | SWT.RIGHT);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		StringBuilder sb = new StringBuilder();
		for (File file : selectOutputParametersPageModel.getOutputFiles()) {
			if (sb.length() > 0) {
				sb.append('\n');
			}
			sb.append(file.getAbsolutePath());
		}

		if (selectOutputParametersPageModel.getOverwriteExistingFiles().isTrue()) {
			sb.append("\n\nExisting files are : overwritten");
		} else {
			sb.append("\n\nExisting files are : passed");
		}
		return sb.toString();
	}

	public class FileNameValidator implements IValidator<String> {

		@Override
		public IStatus validate(String value) {
			if (value == null || value.isEmpty()) {
				return ValidationStatus.ok();
			}

			List<String> extensions = selectOutputParametersPageModel.getOutputFormatExtensions();
			extensions = new ArrayList<>(extensions);
			extensions.removeIf(x -> x.isBlank() || x.isEmpty());
			if (extensions.isEmpty()) {
				return ValidationStatus.ok();
			}

			// Useful for composite extensions like xsf.nc where xsf.nc and .nc are acceptable
			String extension = FilenameUtils.getExtension(value);
			if (extensions.stream()
					.anyMatch(expectedExtension -> FilenameUtils.getExtension(expectedExtension).equals(extension))) {
				return ValidationStatus.ok();
			}

			if (extensions.contains(extension) || extensions.contains("*." + extension)) {
				return ValidationStatus.ok();
			}

			if (extensions.size() == 1) {
				return ValidationStatus.error("Wrong file extension (expecting " + extensions.get(0) + ")");
			}
			return ValidationStatus.error("Wrong file extension (expecting one of "
					+ extensions.stream().collect(Collectors.joining(", ")) + ")");
		}
	}

	/** Trigger the computation of the output files */
	public void requestOutputFilesComputation() {
		computeOutputFilesSubject.onNext(selectOutputParametersPageModel);
	}

	/** Open a FileDialog to select the single file (folder and filename) */
	private void selectSingleFilename() {
		FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);

		String[] extensions = selectOutputParametersPageModel.getOutputFormatExtensions().stream().map(e -> "*." + e)
				.toArray(String[]::new);
		dialog.setFilterExtensions(extensions);

		WritableFile outputDir = selectOutputParametersPageModel.getOutputDirectory();
		if (outputDir.isDirectory())
			dialog.setFilterPath(outputDir.get().getAbsolutePath());

		WritableString singleOutputFilename = selectOutputParametersPageModel.getSingleOutputFilename();
		if (singleOutputFilename.isNotNull())
			dialog.setFileName(singleOutputFilename.get());

		String file = dialog.open();
		if (file != null) {
			File newSingleFile = new File(file);
			outputDir.set(newSingleFile.getParentFile());
			selectOutputParametersPageModel.getSingleOutputFilename().set(newSingleFile.getName());
		}
	}

}
