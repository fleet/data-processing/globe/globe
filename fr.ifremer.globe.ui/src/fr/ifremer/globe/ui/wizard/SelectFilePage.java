/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.databinding.Binding;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.widget.FileComposite;
import fr.ifremer.globe.ui.widget.FileComposite.SelectFileCompositeModel;

/**
 * Wizard page used to select one input file
 */
public class SelectFilePage extends WizardPage implements ISummarizableWizardPage {

	/** Model in MVC */
	protected SelectFileCompositeModel selectInputFilesPageModel;

	/** Widgets */
	protected FileComposite fileSelector;

	/** Directory path that the FileDialog will use */
	protected File startingDirectory;

	/** Selection binding. This binding is managed manually for performance purpose. */
	protected Binding selectModelBinding;
	protected Label lblNewLabel;

	/**
	 * Constructor
	 *
	 * @wbp.parser.constructor
	 */

	public SelectFilePage() {
		super(SelectFilePage.class.getName(), "Select file", null);
		selectInputFilesPageModel = null;
	}

	public SelectFilePage(SelectFileCompositeModel model) {
		super(SelectFilePage.class.getName(), "Select file", null);
		selectInputFilesPageModel = model;

		if (model.getSelectedFileFilterExtension() != null) {
			setDescription(String.format("Accept files : %s", model.getSelectedFileFilterExtension()));
		}

		updatePageComplete();
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		setControl(container);

		fileSelector = new FileComposite(container, SWT.NORMAL, selectInputFilesPageModel);

		fileSelector.getFileBinding().getValidationStatus().addValueChangeListener(handler -> updatePageComplete());

		lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
		lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		if (selectInputFilesPageModel.getSelectedFileFilterExtensions() != null
				&& !selectInputFilesPageModel.getSelectedFileFilterExtensions().isEmpty()) {
			StringBuilder sb = new StringBuilder();
			selectInputFilesPageModel.getSelectedFileFilterExtensions().stream()
					.forEach(x -> sb.append(x.getSecond() + ","));

			lblNewLabel.setToolTipText(
					"You can drop any file with extension in:  " + sb.toString() + ", if it already exists ");
			lblNewLabel.setText("You can drop any file with extension in:  " + StringUtils.abbreviate(sb.toString(), 80)
					+ ", if it already exists ");
		} else if (selectInputFilesPageModel.getSelectedFileFilterExtension() != null) {
			lblNewLabel.setText("You can drop a ." + selectInputFilesPageModel.getSelectedFileFilterExtension()
					+ " File, if it already exists ");
		} else {
			lblNewLabel.setText("You can drop any existing file.");
		}

	}

	/** Sets whether this page is complete */
	protected void updatePageComplete() {
		WritableFile selectedFile = selectInputFilesPageModel.getSelectedFile();
		if (selectedFile != null && fileSelector != null) {
			setPageComplete(fileSelector.getValidationStatus().isTrue());
		} else {
			setPageComplete(false);
		}
	}

	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			updatePageComplete();
		}
		super.setVisible(visible);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		String result = "";
		File selectedFile = selectInputFilesPageModel.getSelectedFile().get();
		if (selectedFile != null && !selectedFile.getPath().isEmpty()) {
			result = selectedFile.getAbsolutePath();
		}
		return result;
	}

}
