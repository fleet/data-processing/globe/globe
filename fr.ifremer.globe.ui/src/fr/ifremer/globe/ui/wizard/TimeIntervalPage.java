/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.widget.FileComposite;
import fr.ifremer.globe.ui.widget.datetime.InstantWidget;
import fr.ifremer.globe.ui.wizard.model.GenericSelectFileCompositeModel;

public class TimeIntervalPage extends WizardPage implements ISummarizableWizardPage {

	/** Model to edit */
	protected final TimeIntervalModel model;
	/**
	 * True when start or end date were manually changed.<br>
	 * When false, this dates are automatically updated with the union of all input files.<br>
	 * As soon as the user has changed one of the date, his input is privileged. The dates are no longer updated even if
	 * the input files change
	 */
	private final AtomicBoolean manual = new AtomicBoolean(false);

	/** Context for bindings */
	protected DataBindingContext bindingContext = new DataBindingContext();

	/**
	 * Constructor <br>
	 *
	 * @wbp.parser.constructor
	 */
	protected TimeIntervalPage() {
		this(new TimeIntervalModel(new WritableObject<>(new Pair<>(Instant.now(), Instant.now())), // referenceTimeInterval
				new WritableBoolean(), // simple merge
				new WritableBoolean(), new WritableFile(new File("Test")), // isCutFile, cutFile
				new WritableBoolean(true), new WritableObject<>(Instant.now()), new WritableObject<>(Instant.now()), // manual
				new WritableBoolean(), new WritableFile(new File("Test")), new WritableBoolean()) // geo mask
		);
	}

	/**
	 * Constructor
	 */
	public TimeIntervalPage(TimeIntervalModel model) {
		super(TimeIntervalPage.class.getSimpleName());
		this.model = model;
		setTitle("Cut / Merge options");
		setDescription("Select time interval(s) or a geographic mask(s) to filter the input data.");
	}

	/** {@inheritDoc} */
	@Override
	public void createControl(Composite parent) {
		var container = new Composite(parent, SWT.NULL);
		setControl(container);
		container.setLayout(new GridLayout(1, false));

		// Option 1 : simple merge
		var noIntervalOptionButton = new Button(container, SWT.RADIO);
		noIntervalOptionButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		noIntervalOptionButton.setText("Simple merge");
		noIntervalOptionButton.addListener(SWT.Selection, evt -> refreshErrorMessage());
		bindRadioButtonSelection(noIntervalOptionButton, model.isSimpleMerge);

		var simpleMergeComposite = new Composite(container, SWT.BORDER);
		simpleMergeComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		simpleMergeComposite.setLayout(new GridLayout(1, false));

		var simpleMergeLabel = new Label(simpleMergeComposite, SWT.NONE);
		simpleMergeLabel.setText("Merges all files in one.");
		simpleMergeLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
		bindEnabled(simpleMergeLabel, model.isSimpleMerge);

		// Option 2 : with cut file
		var cutFileOptionButton = new Button(container, SWT.RADIO);
		cutFileOptionButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		cutFileOptionButton.setText("Cut and merge with time interval(s) from cut file");
		bindRadioButtonSelection(cutFileOptionButton, model.isCutFile);

		var cutFileGrpComposite = new Composite(container, SWT.BORDER);
		cutFileGrpComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		cutFileGrpComposite.setLayout(new GridLayout(2, false));

		var cutFileLabel = new Label(cutFileGrpComposite, SWT.NONE);
		cutFileLabel.setText("Cut file (.cut) :");
		bindEnabled(cutFileLabel, model.isCutFile);

		var cutFileCompositeModel = new GenericSelectFileCompositeModel(
				List.of(new Pair<>("Cut file (*.cut)", "*.cut")), model.cutFile,
				FileValidator.FILE | FileValidator.NULL);
		var cutFileComposite = new FileComposite(cutFileGrpComposite, SWT.NONE, cutFileCompositeModel);
		cutFileComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		bindEnabled(cutFileComposite, model.isCutFile);

		// Option 3 : custom interval
		var customIntervalOptionButton = new Button(container, SWT.RADIO);
		customIntervalOptionButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		customIntervalOptionButton.setText("Cut and merge with a custom time interval");
		bindRadioButtonSelection(customIntervalOptionButton, model.isManually);

		var manuallyComposite = new Composite(container, SWT.BORDER);
		manuallyComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		manuallyComposite.setLayout(new GridLayout(4, false));

		var startLabel = new Label(manuallyComposite, SWT.NONE);
		startLabel.setText("Start :");
		bindEnabled(startLabel, model.isManually);

		var startDateWidget = new InstantWidget(manuallyComposite, SWT.NONE,
				model.startDate.isNotNull() ? model.startDate.get() : Instant.now(), this::changeStartDate);
		bindEnabled(startDateWidget, model.isManually);

		var endLabel = new Label(manuallyComposite, SWT.NONE);
		endLabel.setText("end :");
		bindEnabled(endLabel, model.isManually);

		var endDateWidget = new InstantWidget(manuallyComposite, SWT.NONE,
				model.endDate.isNotNull() ? model.endDate.get() : Instant.now(), this::changeEndDate);
		bindEnabled(endDateWidget, model.isManually);

		startDateWidget.setInstant(model.startDate.get());
		endDateWidget.setInstant(model.endDate.get());
		startDateWidget.setInstant(model.startDate.get());
		endDateWidget.setInstant(model.endDate.get());

		// Option 4 : geographic mask
		var geoMaskFileOptionButton = new Button(container, SWT.RADIO);
		geoMaskFileOptionButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		geoMaskFileOptionButton.setText("Cut with geographic mask");
		bindRadioButtonSelection(geoMaskFileOptionButton, model.isGeoMaskFile);

		var geoMaskParamsComposite = new Composite(container, SWT.BORDER);
		geoMaskParamsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		geoMaskParamsComposite.setLayout(new GridLayout(2, false));

		// geographic mask file
		var geoMaskFileLabel = new Label(geoMaskParamsComposite, SWT.NONE);
		geoMaskFileLabel.setText("Geomask file (.shp, .kml, .geojson) :");
		bindEnabled(geoMaskFileLabel, model.isGeoMaskFile);

		var geoMaskFileCompositeModel = new GenericSelectFileCompositeModel(
				IFileService.grab().getFileFilters(ContentType.POLYGON_GDAL), model.geoMaskFile,
				FileValidator.FILE | FileValidator.NULL);
		var geoMaskFileComposite = new FileComposite(geoMaskParamsComposite, SWT.NONE, geoMaskFileCompositeModel);
		bindEnabled(geoMaskFileComposite, model.isGeoMaskFile);

		// geographic mask reverse
		var reverseGeoMaskCheckButton = new Button(geoMaskParamsComposite, SWT.CHECK);
		reverseGeoMaskCheckButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		reverseGeoMaskCheckButton.setText("Reverse geographic mask (keep data outside area(s))");
		bindEnabled(reverseGeoMaskCheckButton, model.isGeoMaskFile);
		bindingContext.bindValue(WidgetProperties.buttonSelection().observe(reverseGeoMaskCheckButton),
				model.reverseGeoMask);

		var lblGeoMaskInfo = new Label(geoMaskParamsComposite, SWT.NONE);
		lblGeoMaskInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblGeoMaskInfo.setText("Cuts each file with provided polygons (no merge).");
		lblGeoMaskInfo.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
		bindEnabled(lblGeoMaskInfo, model.isGeoMaskFile);

		// Compute status page when model changed
		model.isCutFile.addChangeListener(e -> refreshErrorMessage());
		model.cutFile.addChangeListener(e -> refreshErrorMessage());
		model.isGeoMaskFile.addChangeListener(e -> refreshErrorMessage());
		model.geoMaskFile.addChangeListener(e -> refreshErrorMessage());
		model.isManually.addChangeListener(e -> refreshErrorMessage());
		model.referenceTimeInterval.addChangeListener(e -> {
			if (!manual.get()) {
				model.startDate.set(model.referenceTimeInterval.get().getFirst());
				model.endDate.set(model.referenceTimeInterval.get().getSecond());
			}
		});

		bindingContext.updateTargets();
	}

	/**
	 * User decides to change the value of the start date
	 */
	protected void changeStartDate(Instant value) {
		model.startDate.set(value);
		manual.set(true);
	}

	/**
	 * User decides to change the value of the end date
	 */
	protected void changeEndDate(Instant value) {
		model.endDate.set(value);
		manual.set(true);
	}

	private void bindRadioButtonSelection(Button button, WritableBoolean model) {
		bindingContext.bindValue(WidgetProperties.buttonSelection().observe(button), model);
	}

	private void bindEnabled(Widget widget, WritableBoolean flag) {
		bindingContext.bindValue(//
				WidgetProperties.enabled().observe(widget), flag, UpdateValueStrategy.never(), null);
	}

	protected void refreshErrorMessage() {
		setErrorMessage(null);
		setPageComplete(true);

		if (model.isCutFile.isTrue()) {
			if (model.cutFile.isNull()) {
				setErrorMessage("Cut file (.cut) is required.");
				setPageComplete(false);
			} else if (!model.cutFile.exists()) {
				setErrorMessage("Cut file doesn't exist.");
				setPageComplete(false);
			} else {
				try {
					ProfileUtils.loadProfilesFromFile(model.cutFile.get());
				} catch (Exception e) {
					setErrorMessage("Bad cut file format");
					setPageComplete(false);
				}
			}
		} else if (model.isGeoMaskFile.isTrue()) {
			if (model.geoMaskFile.isNull()) {
				setErrorMessage("Geographic mask file is required.");
				setPageComplete(false);
			} else if (!model.geoMaskFile.exists()) {
				setErrorMessage("Geographic mask file doesn't exist.");
				setPageComplete(false);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			refreshErrorMessage();
		}
	}

	/** {@inheritDoc} */
	@Override
	public String summarize() {
		if (model.isCutFile.isTrue()) {
			return "Cut file : " + model.cutFile.getValue().getPath();
		}
		if (model.isManually.isTrue()) {
			var formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.US)
					.withZone(ZoneId.from(ZoneOffset.UTC));
			return "From : " + formatter.format(model.startDate.get()) + "\nto : "
					+ formatter.format(model.endDate.get());
		}
		if (model.isGeoMaskFile.isTrue()) {
			return "Cut with geographic mask : " + model.geoMaskFile.getValue().getPath() + "\nReverse : "
					+ model.reverseGeoMask.getValue();
		}
		return "No time interval";
	}

	/**
	 * Model of the page
	 */
	public record TimeIntervalModel(WritableObject<Pair<Instant, Instant>> referenceTimeInterval,
			WritableBoolean isSimpleMerge, //
			WritableBoolean isCutFile, WritableFile cutFile, //
			WritableBoolean isManually, WritableObject<Instant> startDate, WritableObject<Instant> endDate, //
			WritableBoolean isGeoMaskFile, WritableFile geoMaskFile, WritableBoolean reverseGeoMask) {
	}

}
