package fr.ifremer.globe.ui.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class DescriptionPage extends WizardPage {
	private Text text;
	private String description;

	/**
	 * Create the wizard.
	 */
	public DescriptionPage(String description) {
		super("wizardPage");
		setTitle("Description");
		setDescription("Process description");
		this.description=description;
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		setControl(container);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));

		text = new Text(container, SWT.BORDER | SWT.MULTI);
		text.setEditable(false);
		text.setText(description);
	}
}
