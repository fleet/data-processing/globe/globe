/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.wizard.generic;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.Range;
import org.eclipse.core.databinding.observable.IObservableCollection;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.model.dtm.filter.FilteringOperator;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.ICdiService;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableDuration;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableInstant;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.service.wizard.IWizardBuilder;
import fr.ifremer.globe.ui.widget.cdi.CDIWidgetParameters;
import fr.ifremer.globe.ui.wizard.AsciiDelimiterPage;
import fr.ifremer.globe.ui.wizard.MaskPage;
import fr.ifremer.globe.ui.wizard.ProjectionWizardPage;
import fr.ifremer.globe.ui.wizard.SelectFilePage;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;
import fr.ifremer.globe.ui.wizard.SelectPolygonMaskFilesPage;
import fr.ifremer.globe.ui.wizard.TimeIntervalPage;
import fr.ifremer.globe.ui.wizard.TimeIntervalPage.TimeIntervalModel;
import fr.ifremer.globe.ui.wizard.WCFilterParametersPage;
import fr.ifremer.globe.ui.wizard.model.DepthCorrectionModel;
import fr.ifremer.globe.ui.wizard.model.GenericGeoboxPageModel;
import fr.ifremer.globe.ui.wizard.model.GenericMaskPageModel;
import fr.ifremer.globe.ui.wizard.model.GenericProjectionWizardPageModel;
import fr.ifremer.globe.ui.wizard.model.GenericSelectFileCompositeModel;
import fr.ifremer.globe.ui.wizard.model.GenericSelectInputFilesPageModel;
import fr.ifremer.globe.ui.wizard.model.GenericSelectOutputParametersPageModel;
import fr.ifremer.globe.ui.wizard.model.GenericSpatialResolutionModel;
import fr.ifremer.globe.ui.wizard.page.GenericCheckListWizardPage;
import fr.ifremer.globe.ui.wizard.page.GenericGeoboxPage;
import fr.ifremer.globe.ui.wizard.page.GenericSpatialResolutionPage;
import fr.ifremer.globe.ui.wizard.page.GenericWizardPage;
import fr.ifremer.globe.ui.wizard.page.SortFilesByCdiPage;
import fr.ifremer.globe.ui.wizard.page.tide.DepthCorrectionWizardPage;
import fr.ifremer.globe.ui.wizard.setCDI.ModifyCDIPage2;
import fr.ifremer.globe.ui.wizard.setCDI.MultiFilterWizardPage;
import fr.ifremer.globe.ui.wizard.setCDI.SetCDIPage;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;

/**
 *
 */
public class WizardBuilder implements IWizardBuilder {
	/** The title of the Wizard to build */
	String windowTitle = "";
	/** Pages to add to the wizard */
	List<ISummarizableWizardPage> wizardPages = new ArrayList<>();
	/** True to add the summary page */
	boolean needSummary = false;
	/** First display page is the summary one */
	boolean firstPageIsSummary = false;
	/** Generated generic pages */
	Map<String, GenericWizardPage> genericWizardPages = new HashMap<>();

	/**
	 * Constructor
	 */
	public WizardBuilder() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder setWindowTitle(String windowTitle) {
		this.windowTitle = windowTitle;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSelectFilePage(String pageTitle, boolean mandatory, List<Pair<String, String>> fileFilter,
			WritableFile selectedFile, BooleanSupplier enabledSupplier) {
		int fileRequirements = mandatory ? FileValidator.FILE : FileValidator.FILE | FileValidator.NULL;
		GenericSelectFileCompositeModel pageModel = new GenericSelectFileCompositeModel(fileFilter, selectedFile,
				fileRequirements);
		SelectFilePage page = new SelectFilePage(pageModel) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSelectFolderPage(String pageTitle, boolean mandatory, WritableFile selectedFolder,
			BooleanSupplier enabledSupplier) {
		int fileRequirements = mandatory ? FileValidator.FOLDER : FileValidator.FOLDER | FileValidator.NULL;
		GenericSelectFileCompositeModel pageModel = new GenericSelectFileCompositeModel(null, selectedFolder,
				fileRequirements);
		SelectFilePage page = new SelectFilePage(pageModel) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSelectInputFilesPage(String pageTitle, List<Pair<String, String>> filterExtensions,
			boolean mandatory, WritableObjectList<File> selectedFiles, BooleanSupplier enabledSupplier) {
		GenericSelectInputFilesPageModel pageModel = new GenericSelectInputFilesPageModel(selectedFiles, mandatory);
		pageModel.setInputFilesFilterExtensions(filterExtensions);
		SelectInputFilesPage page = new SelectInputFilesPage(pageModel) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};

		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSortFilesByCdiPage(String pageTitle, WritableObjectList<File> selectedFiles,
			BooleanSupplier enabledSupplier) {
		SortFilesByCdiPage page = new SortFilesByCdiPage(selectedFiles) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addTimeIntervalPage(String pageTitle,
			WritableObject<Pair<Instant, Instant>> referenceTimeInterval, //
			WritableBoolean isMergeSimple, //
			WritableBoolean isCutFile, WritableFile cutFile, //
			WritableBoolean isManually, WritableObject<Instant> startDate, WritableObject<Instant> endDate, //
			WritableBoolean isGeoMaskFile, WritableFile geoMaskFile, WritableBoolean reverseGeoMask) {
		var model = new TimeIntervalModel(referenceTimeInterval, isMergeSimple, isCutFile, cutFile, isManually,
				startDate, endDate, isGeoMaskFile, geoMaskFile, reverseGeoMask);
		var page = new TimeIntervalPage(model);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addDepthCorrectionPage(String pageTitle, DepthCorrectionModel model) {
		var page = new DepthCorrectionWizardPage(model);
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSelectOutputFilesPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableObjectList<String> outputFileExtensions, WritableObjectList<File> outputFiles,
			WritableBoolean loadFilesAfter, WritableString whereToloadFiles, WritableBoolean overwriteExistingFiles,
			WritableBoolean mergeOutputFiles, WritableString prefix, WritableString suffix,
			WritableString singleOutputFilename, boolean mergeAllowed, BooleanSupplier enabledSupplier) {
		GenericSelectOutputParametersPageModel pageModel = new GenericSelectOutputParametersPageModel(inputFiles,
				outputFileExtensions, outputFiles, loadFilesAfter, whereToloadFiles, overwriteExistingFiles,
				mergeOutputFiles, prefix, suffix, singleOutputFilename);
		SelectOutputParametersPage page = new SelectOutputParametersPage(pageModel) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		page.enableMergeOutputFiles(mergeAllowed);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addGeoBoxWizardPage(String pageTitle, WritableList<GeoBox> geoboxes,
			WritableObject<GeoBox> customGeobox, WritableObject<Projection> projection,
			Optional<WritableDouble> spatialResolution,
			Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator,
			Optional<Consumer<MapOfDoubleConsumer>> geoBoxEvaluator, BooleanSupplier enabledSupplier) {

		GenericGeoboxPageModel pageModel = new GenericGeoboxPageModel(geoboxes, customGeobox, projection,
				spatialResolution, spatialResolutionEvaluator, geoBoxEvaluator);
		GenericGeoboxPage page = new GenericGeoboxPage(pageModel) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSpatialResolutionWizardPage(String pageTitle, WritableObject<Projection> projection,
			Optional<WritableDouble> spatialResolution,
			Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator) {

		GenericSpatialResolutionModel pageModel = new GenericSpatialResolutionModel(projection, spatialResolution,
				spatialResolutionEvaluator);
		GenericSpatialResolutionPage page = new GenericSpatialResolutionPage(pageModel);
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IWizardBuilder addAsciiDelimiterPage(String pageTitle, IxyzFile file, BooleanSupplier enabledSupplier) {

		AsciiDelimiterPage page = new AsciiDelimiterPage(file, true) {

			/** {@inheritDoc} */
			@Override
			public IWizardPage getNextPage() {
				// Update file before swapping to the next page
				file.setDelimiter(getDelimiter());
				file.setDepthPositiveBelowSurface(isDepthPositiveBelowSurface());
				file.setHeaderIndex(getHeaderIndex());
				file.setStartingLine(getStartingLine());
				file.setDecimalPointLabel(getDecimalPointLabel());
				return super.getNextPage();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSetCDIPage(String pageTitle, WritableObjectList<File> files, WritableList<String> cdis,
			String defaultValue, BooleanSupplier enabledSupplier) {
		CDIWidgetParameters param = new CDIWidgetParameters(files, cdis, defaultValue);
		SetCDIPage page = new SetCDIPage(param) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addModifyCDIPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableList<String> originalCdis, WritableList<String> cdis, BooleanSupplier enabledSupplier) {

		inputFiles.addListChangeListener(event -> retrieveCdis(inputFiles, originalCdis, cdis));
		if (originalCdis.isEmpty()) {
			retrieveCdis(inputFiles, originalCdis, cdis);
		}

		CDIWidgetParameters param = new CDIWidgetParameters(originalCdis, cdis);
		ModifyCDIPage2 page = new ModifyCDIPage2(param) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addProjectionPage(String pageTitle, WritableObject<ProjectionSettings> projection,
			WritableObject<GeoBox> geobox, BooleanSupplier enabledSupplier) {
		GenericProjectionWizardPageModel pageModel = new GenericProjectionWizardPageModel(projection, geobox);
		ProjectionWizardPage page = new ProjectionWizardPage("Choose projection, ellipsoid, parameters", pageModel) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addMultiFilterWizardPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableList<String> filters, WritableObject<FilteringOperator> operator, BooleanSupplier enabledSupplier) {
		WritableList<String> writableCdis = new WritableList<>();
		WritableList<String> writableLayers = new WritableList<>();
		inputFiles.addChangeListener(
				event -> updateMultiFilterWizardPageParameters(inputFiles, writableCdis, writableLayers));
		updateMultiFilterWizardPageParameters(inputFiles, writableCdis, writableLayers);

		MultiFilterWizardPage page = new MultiFilterWizardPage(writableCdis, writableLayers, filters, operator) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableFile value, Supplier<File> evaluator, List<String> extensions, BooleanSupplier enabledSupplier) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator, extensions);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableBoolean value, Supplier<Boolean> evaluator, BooleanSupplier enabledSupplier) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableDouble value, Supplier<Double> evaluator, Range doubleRange, BooleanSupplier enabledSupplier) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator, doubleRange);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableInt value, Supplier<Integer> evaluator, Range intRange, BooleanSupplier enabledSupplier) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator, intRange);
		return this;
	}

	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableString value, Supplier<String> evaluator, BooleanSupplier enabledSupplier, String... choices) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator, choices);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableInt value, Supplier<Integer> evaluator, BooleanSupplier enabledSupplier, int... choices) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator, choices);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableDuration value, Supplier<Duration> evaluator, BooleanSupplier enabledSupplier) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableInstant value, Supplier<Instant> evaluator, BooleanSupplier enabledSupplier) {
		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, value, evaluator);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addGenericCheckListPage(String pageTitle, String pageDescription,
			WritableSet<String> availableValues, WritableSet<String> selectedValues, BooleanSupplier enabledSupplier) {
		GenericCheckListWizardPage page = new GenericCheckListWizardPage(pageTitle, availableValues, selectedValues) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setDescription(pageDescription);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addGenericCheckListPage(String pageTitle, String pageDescription,
			WritableList<String> availableValues, WritableList<String> selectedValues,
			BooleanSupplier enabledSupplier) {
		GenericCheckListWizardPage page = new GenericCheckListWizardPage(pageTitle, availableValues, selectedValues) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		page.setDescription(pageDescription);
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addLayersPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableSet<String> selectedLayers, BooleanSupplier enabledSupplier) {
		WritableSet<String> layers = new WritableSet<>();
		inputFiles.addChangeListener(event -> retrieveLayers(inputFiles, layers, selectedLayers, layer -> true));
		retrieveLayers(inputFiles, layers, selectedLayers, layer -> true);
		GenericCheckListWizardPage page = new GenericCheckListWizardPage(pageTitle, layers, selectedLayers) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		wizardPages.add(page);

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSensorsCheckListPage(String pageTitle, String pageDescription,
			WritableObjectList<File> inputFiles, WritableList<String> selectedSensors,
			WritableSet<String> sensorFilters, BooleanSupplier enabledSupplier) {
		WritableList<String> sensors = new WritableList<>();
		Predicate<File> pred = !sensorFilters.isEmpty() ? //
				sensor -> sensorFilters.stream().anyMatch(filter -> sensor.getName().contains(filter)) : //
				sensor -> true;

		inputFiles.addChangeListener(event -> retrieveSensors(inputFiles, sensors, pred));
		retrieveSensors(inputFiles, sensors, pred);
		GenericCheckListWizardPage page = new GenericCheckListWizardPage(pageTitle, sensors, selectedSensors) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};

		page.setDescription(pageDescription);
		wizardPages.add(page);

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSensorToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableObjectList<File> inputFiles, WritableString selectedSensor, WritableSet<String> sensorFilters,
			BooleanSupplier enabledSupplier) {
		WritableList<String> sensors = new WritableList<>();
		Predicate<File> pred = !sensorFilters.isEmpty() ? //
				sensor -> sensorFilters.stream().anyMatch(filter -> sensor.getName().contains(filter)) : //
				sensor -> true;

		inputFiles.addChangeListener(event -> retrieveSensors(inputFiles, sensors, pred));
		retrieveSensors(inputFiles, sensors, pred);

		GenericWizardPage genericWizardPage = getGenericPage(pageTitle, pageDescription, enabledSupplier);
		genericWizardPage.add(labelArgument, selectedSensor, null, sensors);

		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IWizardBuilder addMaskWizardPage(String pageTitle, WritableBoolean maskEnable, WritableInt maskSize,
			WritableObjectList<File> maskFiles, BooleanSupplier enabledSupplier) {

		IFileService fileService = IFileService.grab();
		GenericMaskPageModel pageModel = new GenericMaskPageModel(maskEnable, maskSize, maskFiles,
				fileService.getFileFilters(ContentType.KML_XML, ContentType.SHAPEFILE));

		MaskPage page = new MaskPage(pageTitle, pageModel) {
			@Override
			public boolean isEnabled() {
				return enabledSupplier.getAsBoolean();
			}
		};
		wizardPages.add(page);

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addWcFiltersPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableString filters, BooleanSupplier enabledSupplier) {
		var page = new WCFilterParametersPage(filters, inputFiles);
		page.setTitle(pageTitle);
		wizardPages.add(page);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IWizardBuilder addCustomPage(ISummarizableWizardPage page) {
		wizardPages.add(page);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSummaryPage() {
		needSummary = true;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardBuilder addSelectPolygonMaskPage(WritableObjectList<File> polygonMask,
			WritableBoolean reversePolygonMask) {
		GenericSelectInputFilesPageModel pageModel = new GenericSelectInputFilesPageModel(polygonMask, false);
		pageModel.setInputFilesFilterExtensions(List.of(new Pair<>("kml", "*.kml"), new Pair<>("shp", "*.shp")));
		SelectPolygonMaskFilesPage page = new SelectPolygonMaskFilesPage(pageModel, reversePolygonMask);
		wizardPages.add(page);
		return this;
	}

	/**
	 * @return the generic page identified by its title
	 */
	public GenericWizardPage getGenericPage(String pageTitle, String description, BooleanSupplier enabledSupplier) {
		return genericWizardPages.computeIfAbsent(pageTitle, title -> {
			GenericWizardPage result = new GenericWizardPage(title) {
				@Override
				public boolean isEnabled() {
					return enabledSupplier.getAsBoolean();
				}
			};
			result.setDescription(description);
			wizardPages.add(result);
			return result;
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Wizard build() {
		GenericWizard result = new GenericWizard(windowTitle, needSummary, firstPageIsSummary);
		wizardPages.forEach(result::addPage);
		windowTitle = "";
		wizardPages.clear();
		return result;
	}

	protected void updateMultiFilterWizardPageParameters(WritableObjectList<File> inputFiles,
			WritableList<String> writableCdis, IObservableCollection<String> writableLayers) {
		retrieveCdis(inputFiles, writableCdis, null);
		// All layers but CDI
		retrieveLayers(inputFiles, writableLayers, null, layer -> !layer.toUpperCase().contains("CDI"));
	}

	/**
	 * Retrieves all CDIs contained in the files list and fill the output collection mainting old mapping
	 */
	protected void retrieveCdis(List<File> filePaths, WritableList<String> writableCdis,
			WritableList<String> mappedCdis) {
		ICdiService cdiService = ICdiService.grab();

		SortedSet<String> newCdis = new TreeSet<>();
		for (File file : filePaths) {
			newCdis.addAll(cdiService.getCdis(file.getAbsolutePath()));
		}

		if (mappedCdis != null) {
			TreeMap<String, String> oldCdis = new TreeMap<>();
			if (writableCdis.size() == mappedCdis.size()) {
				for (int i = 0; i < writableCdis.size(); i++) {
					oldCdis.put(writableCdis.get(i), mappedCdis.get(i));
				}
			}
			mappedCdis.clear();
			mappedCdis.addAll(newCdis.stream().map(cdi -> oldCdis.getOrDefault(cdi, cdi)).collect(Collectors.toList()));
		}

		writableCdis.clear();
		writableCdis.addAll(newCdis);
	}

	/**
	 * Retrieves all layers contained in the files list and fill the output collection
	 */
	protected void retrieveLayers(List<File> filePaths, IObservableCollection<String> writableLayers,
			WritableSet<String> selectedLayers, Predicate<String> layersFilter) {
		IFileService fileService = IFileService.grab();
		Set<String> layers = new HashSet<>();
		for (File file : filePaths) {
			List<String> fileLayers = fileService.getLayers(file.getAbsolutePath());
			fileLayers.stream().filter(layersFilter).forEach(layers::add);
		}

		writableLayers.clear();
		writableLayers.addAll(layers);
		if (selectedLayers != null && selectedLayers.isEmpty()) {
			selectedLayers.clear();
			selectedLayers.addAll(layers); // All layers are selectde by default
		}
	}

	/**
	 * Retrieves all sensors contained in the input directory and fill the output collection
	 */
	protected void retrieveSensors(List<File> filePaths, IObservableCollection<String> writableSensors,
			Predicate<File> sensorsFilter) {
		HashSet<String> sensorSet = new HashSet<>();
		ArrayList<File> files = new ArrayList<>();
		for (File filePath : filePaths) {
			if (filePath.isDirectory()) {
				files.addAll(FileUtils.listFiles(filePath, null, true));
			} else {
				files.add(filePath);
			}
		}

		files.stream().filter(sensorsFilter).forEach(file -> {
			String[] parts = file.toString().split("-");
			if (parts.length == 4) {
				sensorSet.add(parts[2] + "-" + parts[3]);
			}
		});
		// sort sensors by alphabetical order (isn't enough because of set)
		ArrayList<String> sensorList = new ArrayList<>();
		sensorList.addAll(sensorSet);
		sensorList.sort((l1, l2) -> l1.compareTo(l2));
		writableSensors.clear();
		if (!sensorList.isEmpty()) {
			writableSensors.addAll(sensorList);
		} else {
			writableSensors.add("No sensor found");
		}
	}

	@Override
	public IWizardBuilder firstPageIsSummary() {
		this.firstPageIsSummary = true;
		return this;
	}

}
