package fr.ifremer.globe.ui.wizard.generic;

import java.util.Optional;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.application.prefs.Preferences;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.wizard.ProjectionWizardPage;
import fr.ifremer.globe.ui.wizard.page.SummarytWizardPage;

/**
 * Implementation of the generic Wizard
 */
public class GenericWizard extends Wizard {

	private final Optional<SummarytWizardPage> summarytWizardPage;
	private final boolean firstPageIsSummary;

	/** Projection settings get from ProjectionWizardPage if any */
	private Optional<WritableObject<ProjectionSettings>> projectionSettings = Optional.empty();

	/**
	 * Constructor
	 */
	public GenericWizard(String title) {
		this(title, false, false);
	}

	/**
	 * Constructor
	 */
	public GenericWizard(String title, boolean withSummaryPage, boolean firstPageIsSummary) {
		setWindowTitle(title);
		this.firstPageIsSummary = firstPageIsSummary;
		summarytWizardPage = withSummaryPage ? Optional.of(new SummarytWizardPage()) : Optional.empty();
	}

	/** Adds a new page to this wizard */
	@Override
	public void addPage(IWizardPage page) {
		super.addPage(page);
		if (page instanceof ProjectionWizardPage projectionWizardPage)
			setPreferredProjection(projectionWizardPage);
	}

	/** Add extra pages */
	@Override
	public void addPages() {
		super.addPages();
		summarytWizardPage.ifPresent(this::addPage);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performFinish() {
		savePreferredProjection();
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		IWizardPage result = super.getNextPage(page);
		if (result instanceof ISummarizableWizardPage summarizableWizardPage && !summarizableWizardPage.isEnabled()) {
			// Page is not enabled. Skip it.
			result = getNextPage(result);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardPage getPreviousPage(IWizardPage page) {
		IWizardPage result = super.getPreviousPage(page);
		if (result instanceof ISummarizableWizardPage summarizableWizardPage && !summarizableWizardPage.isEnabled()) {
			// Page is not enabled. Skip it.
			result = getPreviousPage(result);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWizardPage getStartingPage() {
		return firstPageIsSummary && summarytWizardPage.isPresent() ? summarytWizardPage.get()
				: super.getStartingPage();
	}

	/** {@inheritDoc} */
	@Override
	public boolean canFinish() {
		return firstPageIsSummary || super.canFinish();
	}

	/** Check if a projection is selected. If not, set the preferred one as default */
	private void setPreferredProjection(ProjectionWizardPage projectionWizardPage) {
		WritableObject<ProjectionSettings> ps = projectionWizardPage.getModel().getProjectionSettings();
		this.projectionSettings = Optional.of(ps);

		if (ps.isNull() || ps.getValue().isLongLatProjection()) {
			// Default projection in process is always LongLat
			// In this case, replace it by preferred one
			Preferences preferences = ContextInitializer.getInContext(Preferences.class);
			ps.set(preferences.getPreferredProjection());
		}
	}

	/** Save the value of projection in preferences */
	private void savePreferredProjection() {
		this.projectionSettings.ifPresent(ps -> {
			Preferences preferences = ContextInitializer.getInContext(Preferences.class);
			preferences.setPreferredProjection(ps.doGetValue());
		});
	}

}
