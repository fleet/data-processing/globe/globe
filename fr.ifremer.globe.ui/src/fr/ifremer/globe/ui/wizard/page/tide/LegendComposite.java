package fr.ifremer.globe.ui.wizard.page.tide;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.model.chart.impl.SimpleChartSeries;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.widget.color.ColorPicker;

public class LegendComposite extends Composite {

	/** Linked chart */
	private FXChart chart;

	/** Observable list of linked series **/
	private WritableObjectList<SimpleChartSeries> linkedSeries = new WritableObjectList<>();

	/** Widget **/
	private ColorPicker colorPicker;

	private Button checkbox;

	private Label label;

	private static GColor TRANSPARENT_COLOR = new GColor("0xFFFFFF00");

	/**
	 * Constructor
	 */
	public LegendComposite(Composite parent, String legend, GColor color, FXChart chart) {
		super(parent, SWT.NONE);
		ContextInitializer.inject(this);
		this.chart = chart;
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.verticalSpacing = 1;
		setLayout(gridLayout);

		// checkbox & label
		checkbox = new Button(this, SWT.CHECK);
		checkbox.setSelection(true);
		checkbox.addListener(SWT.Selection, e -> {
			linkedSeries.forEach(series -> {
				series.setEnabled(checkbox.getSelection());
				series.setColor(getColor());
			});
			chart.refresh();
		});

		// combo box
		label = new Label(this, SWT.READ_ONLY);
		label.setText(legend);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// color picker
		colorPicker = new ColorPicker(this, SWT.None, color, newColor -> {
			linkedSeries.forEach(series -> series.setColor(newColor));
			chart.refresh();
		});
		GridData gdColorPicker = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gdColorPicker.heightHint = 6;
		colorPicker.setLayoutData(gdColorPicker);

	}

	private GColor getColor() {
		return isCheckboxEnabled() ? colorPicker.getColor() : TRANSPARENT_COLOR;
	}

	private boolean isCheckboxEnabled() {
		return checkbox.getSelection();
	}

	public void setSeries(List<SimpleChartSeries> series) {
		linkedSeries.clear();
		// select all series with the selected title
		linkedSeries.addAll(series);
		linkedSeries.forEach(serie -> serie.setColor(getColor()));
		chart.refresh();
	}
}
