/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard.page;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.IObservableCollection;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.core.databinding.property.Properties;
import org.eclipse.jface.databinding.viewers.IViewerObservableSet;
import org.eclipse.jface.databinding.viewers.ViewerSupport;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;

import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;

/**
 * Generic Wizard page for selecting some Strings
 */
public class GenericCheckListWizardPage extends WizardPage implements ISummarizableWizardPage {

	/** The model */
	protected IObservableCollection<String> selectedValues;
	protected IStructuredSelection currentSelection;
	/** The view */
	protected CheckboxTableViewer tableViewer;
	private Runnable initBindingsMethod;

	/** Constructor */
	public GenericCheckListWizardPage(String title, IObservableSet<String> availableValues,
			IObservableSet<String> selectedValues) {
		super(GenericCheckListWizardPage.class.getSimpleName(), title, null);
		this.selectedValues = selectedValues;
		initBindingsMethod = () -> initDataBindings(availableValues, selectedValues);
	}

	/** Constructor */
	public GenericCheckListWizardPage(String title, IObservableList<String> availableValues,
			IObservableList<String> selectedValues) {
		super(GenericCheckListWizardPage.class.getSimpleName(), title, null);
		this.selectedValues = selectedValues;
		initBindingsMethod = () -> initDataBindings(availableValues, selectedValues);
	}

	/** {@inheritDoc} */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout glContainer = new GridLayout(1, false);
		glContainer.marginHeight = 10;
		glContainer.marginWidth = 10;
		glContainer.verticalSpacing = 10;
		glContainer.horizontalSpacing = 10;
		container.setLayout(glContainer);
		setControl(container);

		tableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		tableViewer.setCheckStateProvider(new GenericCheckListStateProvider());

		tableViewer.addDoubleClickListener(e -> {
			boolean checked = tableViewer.getChecked(((IStructuredSelection) e.getSelection()).getFirstElement());
			for (Object o : tableViewer.getStructuredSelection()) {
				tableViewer.setChecked(o, !checked);
			}
			updateSelection();
		});

		tableViewer.addSelectionChangedListener(e -> {
			if (!tableViewer.getStructuredSelection().equals(currentSelection)) {
				currentSelection = tableViewer.getStructuredSelection();
				for (Object o : currentSelection) {
					tableViewer.setChecked(o, true);
				}
				updateSelection();
			}
		});

		Table table = tableViewer.getTable();
		GridDataFactory.fillDefaults().grab(true, true).applyTo(table);

		initBindingsMethod.run();
	}

	private void updateSelection() {
		selectedValues.clear();
		for(Object elem : tableViewer.getCheckedElements())
		{
			selectedValues.add((String) elem);
		}
	}

	/**
	 * Binds the viewer's input and selection
	 */
	protected void initDataBindings(IObservableList<String> availableValues, IObservableList<String> selectedValues) {
		// Bind available values as input for the tableViewer
		ViewerSupport.bind(tableViewer, availableValues, Properties.selfValue(availableValues));

		// Set expected instead of list
		WritableSet<String> selectedSet = new WritableSet<>(selectedValues, String.class);
		selectedSet.addChangeListener(e -> updateSelection());

		// Binding selection
		IViewerObservableSet<String> targetObservableSet = ViewerProperties.checkedElements(String.class)
				.observe((Viewer) tableViewer);
		DataBindingContext bindingContext = new DataBindingContext();
		bindingContext.bindSet(targetObservableSet, selectedSet);
	}

	/**
	 * Binds the viewer's input and selection
	 */
	protected void initDataBindings(IObservableSet<String> availableValues, IObservableSet<String> selectedValues) {
		// Bind available values as input for the tableViewer
		ViewerSupport.bind(tableViewer, availableValues, Properties.selfValue(availableValues));

		// Binding selection
		IViewerObservableSet<String> targetObservableSet = ViewerProperties.checkedElements(String.class)
				.observe((Viewer) tableViewer);
		DataBindingContext bindingContext = new DataBindingContext();
		bindingContext.bindSet(targetObservableSet, selectedValues);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		String result = "";
		if (!selectedValues.isEmpty()) {
			result = "Selection : " + StringUtils.join(selectedValues, ", ");
		}
		return result;
	}

	/**
	 * Checked and grayed state information about data in the table
	 */
	class GenericCheckListStateProvider implements ICheckStateProvider {

		@Override
		public boolean isChecked(Object element) {
			return selectedValues.contains(element);
		}

		@Override
		public boolean isGrayed(Object element) {
			return false;
		}
	}
}
