/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard.page;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.Range;
import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.text.NumberToStringConverter;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import com.ibm.icu.text.NumberFormat;

import fr.ifremer.globe.ui.databinding.conversion.FileToStringConverter;
import fr.ifremer.globe.ui.databinding.conversion.StringToDoubleConverter;
import fr.ifremer.globe.ui.databinding.conversion.StringToFileConverter;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableDuration;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableInstant;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.databinding.validation.NumberFormatValidator;
import fr.ifremer.globe.ui.databinding.validation.NumberValidator;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.widget.datetime.DurationWidget;
import fr.ifremer.globe.ui.widget.datetime.InstantWidget;

/**
 * Generic Wizard page
 */
public class GenericWizardPage extends WizardPage implements ISummarizableWizardPage {

	public static final int MAX_WIDTH = 600;

	/** Context for bindings */
	protected DataBindingContext bindingContext = new DataBindingContext();

	protected List<IValue> values = new ArrayList<>();
	protected List<Binding> bindings = new ArrayList<>();

	/** Constructor */
	public GenericWizardPage(String title) {
		super(GenericWizardPage.class.getSimpleName() + '_' + title, title, null);
	}

	/** {@inheritDoc} */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(false).margins(10, 10).spacing(10, 10)
				.applyTo(container);
		setControl(container);

		IValueOperation createWidget = new IValueOperation() {
			@Override
			public void visit(BooleanValue booleanValue) {
				createWidget(container, booleanValue);
			}

			@Override
			public void visit(DoubleValue numberValue) {
				createWidget(container, numberValue);
			}

			@Override
			public void visit(IntValue intValue) {
				createWidget(container, intValue);
			}

			@Override
			public void visit(StringValue stringValue) {
				createWidget(container, stringValue);
			}

			@Override
			public void visit(DurationValue durationValue) {
				createWidget(container, durationValue);
			}

			@Override
			public void visit(InstantValue instantValue) {
				createWidget(container, instantValue);
			}

			@Override
			public void visit(FileValue filesValue) {
				createWidget(container, filesValue);
			}

		};

		values.forEach(value -> value.accept(createWidget));

		updatePageComplete();
	}

	/** Add a boolean arguments */
	public void add(String label, WritableBoolean value, Supplier<Boolean> evaluator) {
		values.add(new BooleanValue(label, value, evaluator));
	}

	/** Add a String arguments */
	public void add(String label, WritableString value, Supplier<String> evaluator, WritableList<String> choices) {
		values.add(new StringValue(label, value, evaluator, choices));
	}

	/** Add a String arguments */
	public void add(String label, WritableString value, Supplier<String> evaluator, String... choices) {
		values.add(new StringValue(label, value, evaluator, choices));
	}

	/**
	 * Add a double arguments
	 */
	public void add(String label, WritableDouble value, Supplier<Double> evaluator, Range range) {
		values.add(new DoubleValue(label, value, evaluator, range));
	}

	/**
	 * Add a int arguments
	 */
	public void add(String label, WritableInt value, Supplier<Integer> evaluator, Range range) {
		values.add(new IntValue(label, value, evaluator, range));
	}

	/**
	 * Add a int arguments
	 */
	public void add(String label, WritableInt value, Supplier<Integer> evaluator, int... choices) {
		values.add(new IntValue(label, value, evaluator, null, choices));
	}

	/**
	 * Add a duration arguments
	 */
	public void add(String label, WritableDuration value, Supplier<Duration> evaluator) {
		values.add(new DurationValue(label, value, evaluator));
	}

	/**
	 * Add an instant arguments
	 */
	public void add(String label, WritableInstant value, Supplier<Instant> evaluator) {
		values.add(new InstantValue(label, value, evaluator));
	}

	/**
	 * Add an list of files arguments
	 */
	public void add(String label, WritableFile value, Supplier<File> evaluator, List<String> extensions) {
		values.add(new FileValue(label, value, evaluator, extensions.toArray(new String[extensions.size()])));
	}

	/** Add the Compute button if necessary */
	protected void addComputeButton(Composite container, Object evaluator, Runnable evaluationProcess) {
		// Compute button
		if (evaluator != null) {
			Button compute = new Button(container, SWT.PUSH);
			compute.setText("Compute");
			compute.setImage(ImageResources.getImage("icons/16/python.png", this));
			compute.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> evaluationProcess.run()));
		} else {
			new Label(container, SWT.NONE);
		}
	}

	/**
	 * Create a Button to represent a boolean
	 */
	protected void createWidget(Composite container, BooleanValue booleanValue) {
		createLabel(container, booleanValue.label);

		Button checkBox = new Button(container, SWT.CHECK);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.TOP).applyTo(checkBox);

		IObservableValue<?> observeWidget = WidgetProperties.widgetSelection().observe(checkBox);
		bindingContext.bindValue(observeWidget, booleanValue.value, null, null);

		addComputeButton(container, booleanValue.getEvaluator(), booleanValue::evaluate);
	}

	/**
	 * Create a Button to represent a File
	 */
	protected void createWidget(Composite container, FileValue fileValue) {
		var fileContainer = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.TOP).grab(true, false).span(2, 1).applyTo(fileContainer);

		var fileEditor = new FileFieldEditor(fileValue.label, fileValue.label, true,
				StringFieldEditor.VALIDATE_ON_FOCUS_LOST, fileContainer);
		fileEditor.setChangeButtonText("...");
		fileEditor.setFileExtensions(fileValue.extensions);

		var targetToModel = new UpdateValueStrategy<String, File>();
		targetToModel.setConverter(new StringToFileConverter());
		var modelToTarget = new UpdateValueStrategy<File, String>();
		modelToTarget.setConverter(new FileToStringConverter());

		var observeWidget = WidgetProperties.text(SWT.Modify).observe(fileEditor.getTextControl(fileContainer));
		bindingContext.bindValue(observeWidget, fileValue.value, targetToModel, modelToTarget);

		addComputeButton(container, fileValue.getEvaluator(), fileValue::evaluate);
	}

	/**
	 * Create a Button to represent a string
	 */
	protected void createWidget(Composite container, StringValue stringValue) {
		if (stringValue.choices != null && !stringValue.choices.isEmpty()) {
			createStringCombo(container, stringValue);
		} else {
			createStringText(container, stringValue);
		}
	}

	/**
	 * Create a Combo to represent an String
	 */
	protected void createStringCombo(Composite container, StringValue stringValue) {
		createLabel(container, stringValue.label);

		Combo combo = new Combo(container, SWT.READ_ONLY);
		stringValue.choices.forEach(combo::add);

		stringValue.choices.addChangeListener(e -> {
			combo.removeAll();
			stringValue.choices.forEach(combo::add);
			container.layout();
		});
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.TOP).grab(true, false).applyTo(combo);

		IObservableValue<?> observeWidget = WidgetProperties.widgetSelection().observe(combo);
		bindingContext.bindValue(observeWidget, stringValue.value, null, null);

		addComputeButton(container, stringValue.getEvaluator(), stringValue::evaluate);
	}

	/**
	 * Create a Text to represent a string
	 */
	protected void createStringText(Composite container, StringValue stringValue) {
		createLabel(container, stringValue.label);

		Text text = new Text(container, SWT.BORDER | SWT.WRAP);
		text.addTraverseListener(e -> {
			if (e.detail == SWT.TRAVERSE_TAB_NEXT || e.detail == SWT.TRAVERSE_TAB_PREVIOUS) {
				e.doit = true;
			}
		});
		GridDataFactory.fillDefaults().grab(true, false).applyTo(text);
		IObservableValue<?> observeWidget = WidgetProperties.text(SWT.Modify).observe(text);
		bindingContext.bindValue(observeWidget, stringValue.value, null, null);

		addComputeButton(container, stringValue.getEvaluator(), stringValue::evaluate);
	}

	/**
	 * Create a Text to represent a double
	 */
	protected void createWidget(Composite container, DoubleValue doubleValue) {
		createLabel(container, doubleValue.label);

		Text text = new Text(container, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.TOP).hint(100, SWT.DEFAULT).applyTo(text);

		IObservableValue<String> observeWidget = WidgetProperties.text(SWT.Modify).observe(text);
		UpdateValueStrategy<String, Double> targetToModel = new UpdateValueStrategy<>();
		var converter = new StringToDoubleConverter(null);
		targetToModel.setConverter(converter);

		var numberValidator = doubleValue.range != null ? new NumberValidator(doubleValue.range)
				: new NumberValidator();
		numberValidator.setNullAllowed(true);
		targetToModel.setAfterConvertValidator(numberValidator);
		targetToModel.setAfterGetValidator(new NumberFormatValidator());

		UpdateValueStrategy<Double, String> modelToTarget = new UpdateValueStrategy<>();
		modelToTarget.setConverter(NumberToStringConverter.fromDouble(NumberFormat.getInstance(Locale.US), false));

		Binding binding = bindingContext.bindValue(observeWidget, doubleValue.value, targetToModel, modelToTarget);
		bindings.add(binding);

		ControlDecorationSupport.create(binding, SWT.TOP | SWT.LEFT);
		IObservableValue<?> validationStatus = binding.getValidationStatus();
		validationStatus.addValueChangeListener(changeEvent -> updatePageComplete());

		addComputeButton(container, doubleValue.getEvaluator(), doubleValue::evaluate);
	}

	/**
	 * Create a Button to represent an int
	 */
	protected void createWidget(Composite container, IntValue intValue) {
		if (intValue.choices != null && intValue.choices.length > 0) {
			createIntCombo(container, intValue);
		} else {
			createIntSpinner(container, intValue);
		}
	}

	/**
	 * Create a Spinner to represent an int
	 */
	protected void createIntSpinner(Composite container, IntValue intValue) {
		createLabel(container, intValue.label);

		Spinner spinner = new Spinner(container, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.TOP).hint(100, SWT.DEFAULT).applyTo(spinner);
		if (intValue.range != null) {
			spinner.setMinimum(intValue.range.getMinimumInteger());
			spinner.setMaximum(intValue.range.getMaximumInteger());
		} else {
			spinner.setMinimum(Integer.MIN_VALUE);
			spinner.setMaximum(Integer.MAX_VALUE);
		}
		IObservableValue<?> observeWidget = WidgetProperties.widgetSelection().observe(spinner);
		Binding binding = bindingContext.bindValue(observeWidget, intValue.value);
		bindings.add(binding);

		addComputeButton(container, intValue.getEvaluator(), intValue::evaluate);
	}

	/**
	 * Create a Combo to represent an int
	 */
	protected void createIntCombo(Composite container, IntValue intValue) {
		createLabel(container, intValue.label);

		Combo combo = new Combo(container, SWT.READ_ONLY);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.TOP).applyTo(combo);

		IntStream.of(intValue.choices).forEach(choice -> combo.add(String.valueOf(choice)));

		IObservableValue<?> observeWidget = WidgetProperties.widgetSelection().observe(combo);
		UpdateValueStrategy targetToModel = new UpdateValueStrategy();
		bindingContext.bindValue(observeWidget, intValue.value, targetToModel, null);

		addComputeButton(container, intValue.getEvaluator(), intValue::evaluate);
	}

	/**
	 * Create a Button to represent an Duration
	 */
	@SuppressWarnings("unchecked")
	protected void createWidget(Composite container, DurationValue durationValue) {
		createLabel(container, durationValue.label);
		DurationWidget durationTime = new DurationWidget(container, SWT.BORDER, durationValue.value.get(),
				durationValue.value::set);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.TOP).grab(true, false).applyTo(durationTime);

		addComputeButton(container, durationValue.getEvaluator(), durationValue::evaluate);
	}

	/**
	 * Create a Button to represent an Instant
	 */
	protected void createWidget(Composite container, InstantValue instantValue) {
		createLabel(container, instantValue.label);

		InstantWidget instantDateTime = new InstantWidget(container, SWT.BORDER, instantValue.value.get(),
				instantValue.value::set);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.TOP).grab(true, false).applyTo(instantDateTime);

		addComputeButton(container, instantValue.getEvaluator(), instantValue::evaluate);
	}

	protected void updatePageComplete() {
		boolean status = true;
		for (Binding binding : bindings) {
			status &= binding.getValidationStatus().getValue().isOK();
		}
		setPageComplete(status);
	}

	/**
	 * Create a plain label
	 */
	protected void createLabel(Composite container, String string) {
		Label labelWidget = new Label(container, SWT.WRAP);
		if (string != null)
			labelWidget.setText(string);
		Point point = labelWidget.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER)
				.hint(point.x > MAX_WIDTH ? MAX_WIDTH : SWT.DEFAULT, SWT.DEFAULT).applyTo(labelWidget);
	}

	/** Trucate a label if necessary */
	protected String truncateLabel(String label) {
		String result = label;
		if (label.length() > 120) {
			int dot = StringUtils.indexOfAny(label, new char[] { '.', ':' });
			if (dot > 0 && dot < 120) {
				result = StringUtils.substring(label, 0, dot);
			}
		}

		if (result.length() > 120) {
			result = StringUtils.abbreviate(result, 120);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		StringBuilder sb = new StringBuilder();
		IValueOperation summarizeOperation = new IValueOperation() {

			@Override
			public void visit(BooleanValue value) {
				if (value.value.isNotNull()) {
					sb.append(truncateLabel(value.label)).append(" : ").append(value.value.isTrue());
				}
			}

			@Override
			public void visit(DoubleValue value) {
				if (value.value.isNotNull()) {
					sb.append(truncateLabel(value.label)).append(" : ").append(value.value.get());
				}
			}

			@Override
			public void visit(IntValue value) {
				if (value.value.isNotNull()) {
					sb.append(truncateLabel(value.label)).append(" : ").append(value.value.get());
				}
			}

			@Override
			public void visit(StringValue value) {
				if (value.value.isNotNull()) {
					sb.append(truncateLabel(value.label)).append(" : ").append(value.value.get());
				}
			}

			@Override
			public void visit(DurationValue value) {
				if (value.value.isNotNull()) {
					sb.append(truncateLabel(value.label)).append(" : ")
							.append(String.valueOf(value.value.get().toSeconds())).append(" seconds");
				}
			}

			@Override
			public void visit(InstantValue value) {
				if (value.value.isNotNull()) {
					sb.append(truncateLabel(value.label)).append(" : ").append(value.value.get());
				}
			}

			@Override
			public void visit(FileValue value) {
				if (value.value.isNotNull()) {
					sb.append(truncateLabel(value.label)).append(" : ").append(value.value.get().getPath());
				}
			}

		};

		int sbLength = 0;
		for (IValue oneValue : values) {
			if (sb.length() > sbLength) {
				sb.append('\n');
				sbLength = sb.length();
			}
			oneValue.accept(summarizeOperation);
		}
		return sb.toString();
	}

	/* Implementation of visitor pattern. Abstract operation to visit a value */
	public interface IValueOperation {
		void visit(BooleanValue booleanValue);

		void visit(DoubleValue numberValue);

		void visit(IntValue intValue);

		void visit(StringValue stringValue);

		void visit(DurationValue durationValue);

		void visit(InstantValue instantValue);

		void visit(FileValue filesValue);
	}

	/** Generic value which can be visited by any operation. Implementation of visitor pattern */
	public interface IValue {
		void accept(IValueOperation operation);
	}

	/** Boolean value */
	public static class BooleanValue implements IValue {
		String label;
		WritableBoolean value;
		Supplier<Boolean> evaluator;

		/**
		 * Constructor
		 */
		public BooleanValue(String label, WritableBoolean value, Supplier<Boolean> evaluator) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void accept(IValueOperation operation) {
			operation.visit(this);
		}

		/**
		 * Invoke the evaluator and set the result to the Writable
		 */
		public void evaluate() {
			var newValue = evaluator.get();
			if (newValue != null) {
				value.getRealm().asyncExec(() -> value.setValue(newValue));
			}
		}

		/**
		 * @return the {@link #evaluator}
		 */
		public Supplier<Boolean> getEvaluator() {
			return evaluator;
		}
	}

	/** String value */
	public static class StringValue implements IValue {
		String label;
		WritableString value;
		Supplier<String> evaluator;
		WritableList<String> choices;

		/**
		 * Constructor
		 */
		public StringValue(String label, WritableString value, Supplier<String> evaluator, String... choices) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
			this.choices = new WritableList<>();
			if (choices != null) {
				Arrays.stream(choices).forEach(this.choices::add);
			}
		}

		/**
		 * Constructor
		 */
		public StringValue(String label, WritableString value, Supplier<String> evaluator,
				WritableList<String> choices) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
			this.choices = choices;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void accept(IValueOperation operation) {
			operation.visit(this);
		}

		/**
		 * Invoke the evaluator and set the result to the Writable
		 */
		public void evaluate() {
			var newValue = evaluator.get();
			if (newValue != null) {
				value.getRealm().asyncExec(() -> value.setValue(newValue));
			}
		}

		/**
		 * @return the {@link #evaluator}
		 */
		public Supplier<String> getEvaluator() {
			return evaluator;
		}
	}

	/** Double value */
	public static class DoubleValue implements IValue {
		String label;
		WritableDouble value;
		Supplier<Double> evaluator;
		Range range;

		/**
		 * Constructor
		 */
		public DoubleValue(String label, WritableDouble value, Supplier<Double> evaluator, Range range) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
			this.range = range;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void accept(IValueOperation operation) {
			operation.visit(this);
		}

		/**
		 * Invoke the evaluator and set the result to the Writable
		 */
		public void evaluate() {
			var newValue = evaluator.get();
			if (newValue != null) {
				value.getRealm().asyncExec(() -> value.setValue(newValue));
			}
		}

		/**
		 * @return the {@link #evaluator}
		 */
		public Supplier<Double> getEvaluator() {
			return evaluator;
		}
	}

	/** Integer value */
	public static class IntValue implements IValue {
		String label;
		WritableInt value;
		Supplier<Integer> evaluator;
		Range range;
		int[] choices;

		/**
		 * Constructor
		 */
		public IntValue(String label, WritableInt value, Supplier<Integer> evaluator, Range range, int... choices) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
			this.range = range;
			this.choices = choices;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void accept(IValueOperation operation) {
			operation.visit(this);
		}

		/**
		 * Invoke the evaluator and set the result to the Writable
		 */
		public void evaluate() {
			var newValue = evaluator.get();
			if (newValue != null) {
				value.getRealm().asyncExec(() -> value.setValue(newValue));
			}
		}

		/**
		 * @return the {@link #evaluator}
		 */
		public Supplier<Integer> getEvaluator() {
			return evaluator;
		}
	}

	/** List of files */
	public static class FileValue implements IValue {
		String label;
		WritableFile value;
		Supplier<File> evaluator;
		String[] extensions;

		/**
		 * Constructor
		 */
		public FileValue(String label, WritableFile value, Supplier<File> evaluator, String[] extensions) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
			this.extensions = extensions;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void accept(IValueOperation operation) {
			operation.visit(this);
		}

		/**
		 * Invoke the evaluator and set the result to the Writable
		 */
		public void evaluate() {
			var newValue = evaluator.get();
			if (newValue != null) {
				value.getRealm().asyncExec(() -> value.setValue(newValue));
			}
		}

		/**
		 * @return the {@link #evaluator}
		 */
		public Supplier<File> getEvaluator() {
			return evaluator;
		}
	}

	/** Duration value */
	public static class DurationValue implements IValue {
		String label;
		WritableDuration value;
		Supplier<Duration> evaluator;

		/**
		 * Constructor
		 */
		public DurationValue(String label, WritableDuration value, Supplier<Duration> evaluator) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void accept(IValueOperation operation) {
			operation.visit(this);
		}

		/**
		 * Invoke the evaluator and set the result to the Writable
		 */
		public void evaluate() {
			var newValue = evaluator.get();
			if (newValue != null) {
				value.getRealm().asyncExec(() -> value.setValue(newValue));
			}
		}

		/**
		 * @return the {@link #evaluator}
		 */
		public Supplier<Duration> getEvaluator() {
			return evaluator;
		}
	}

	/** Instant value */
	public static class InstantValue implements IValue {
		String label;
		WritableInstant value;
		Supplier<Instant> evaluator;

		/**
		 * Constructor
		 */
		public InstantValue(String label, WritableInstant value, Supplier<Instant> evaluator) {
			this.label = label;
			this.value = value;
			this.evaluator = evaluator;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void accept(IValueOperation operation) {
			operation.visit(this);
		}

		/**
		 * Invoke the evaluator and set the result to the Writable
		 */
		public void evaluate() {
			var newValue = evaluator.get();
			if (newValue != null) {
				value.getRealm().asyncExec(() -> value.setValue(newValue));
			}
		}

		/**
		 * @return the {@link #evaluator}
		 */
		public Supplier<Instant> getEvaluator() {
			return evaluator;
		}
	}

}
