/**
 * 
 */
package fr.ifremer.globe.ui.wizard.page;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.wizard.page.GenericSpatialResolutionComposite.IGenericSpatialResolutionModel;

/**
 * Wizard page to display and edit a the spatial resolution
 */
public class GenericSpatialResolutionPage extends WizardPage implements ISummarizableWizardPage {

	private final IGenericSpatialResolutionModel model;
	private Button btnEnable;
	private GenericSpatialResolutionComposite resolutionComposite;

	/**
	 * Constructor
	 */
	public GenericSpatialResolutionPage(IGenericSpatialResolutionModel model) {
		super(GenericSpatialResolutionPage.class.getSimpleName());
		setTitle("Spatial resolution");
		this.model = model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.BORDER);
		setControl(container);
		container.setLayout(new GridLayout(1, false));

		btnEnable = new Button(container, SWT.CHECK);
		btnEnable.setText("Enabled");
		if (model.getSpatialResolution().isPresent()) {
			WritableDouble res = model.getSpatialResolution().get();
			btnEnable.setSelection(res.isValid() && res.get() != 0d);
		} else
			btnEnable.setSelection(false);

		btnEnable.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		resolutionComposite = new GenericSpatialResolutionComposite(container, model);
		GridData rcGridData = new GridData(SWT.FILL, SWT.TOP, true, true);
		rcGridData.horizontalIndent = 10;
		resolutionComposite.setLayoutData(rcGridData);

		btnEnable.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onEnableChanged()));
		btnEnable.addPaintListener(e -> onEnableChanged());
	}

	private void onEnableChanged() {
		this.resolutionComposite.setEnabled(btnEnable.getSelection());
		if (!btnEnable.getSelection()) {
			model.getSpatialResolution().ifPresent(res -> res.setToNull());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		var optSpatialResolution = model.getSpatialResolution();
		if (optSpatialResolution.isPresent()) {
			WritableDouble spatialResolution = optSpatialResolution.get();
			if (spatialResolution.isValid())
				return "Spatial resolution : " + spatialResolution.get();
		}
		return "";
	}

}
