package fr.ifremer.globe.ui.wizard.page;

import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.nebula.widgets.formattedtext.DoubleFormatter;
import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.nebula.widgets.formattedtext.FormattedTextObservableValue;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.globes.Earth;

/**
 * Creates controls to display and edit a the spatial resolution
 */
public class GenericSpatialResolutionComposite extends Composite {
	private static Logger logger = LoggerFactory.getLogger(GenericSpatialResolutionComposite.class);

	private static final double DEFAULT_RES_DEGREE = 0.0001;

	/** Key in json file produced by pyat */
	private static final String FUNC_DEGREE = "degree";
	private static final String FUNC_METER = "meter";

	/** Format used to edit a double */
	private static final String DOUBLE_FORMAT = "-##############0.##########";
	/** Format used to edit a meter field */
	private static final String METER_FORMAT = "-##############0.##";

	/** Formatter */
	private static final String METER_UNIT = "m";
	private static final String ARCMIN_UNIT = "arcminute";
	private static final String ARCSEC_UNIT = "arcsecond";

	private final IGenericSpatialResolutionModel model;
	/** Spatial resolution */
	private final Composite spatialResolutionComposite;
	private Composite spatialResolutionMeterComposite;
	private Composite spatialResolutionDegComposite;
	private Combo comboSrsArcmin;

	private FormattedText formattedTextArcsec;
	private FormattedText formattedTextDeg;
	private FormattedText formattedTextMeter;
	private FormattedText formattedTextMeterLatEstimate;
	private FormattedText formattedTextMeterLonEstimate;

	/** Used fonts */
	private final Font fontItalic;

	/**
	 * Points of Geobox used in conversion (degree <-> meter) to estimate the distance
	 */
	private final WritableDouble north;
	private final WritableDouble south;
	private final WritableDouble west;

	private final WritableDouble spatialResDeg = new WritableDouble(null);
	private final WritableDouble spatialResMeter = new WritableDouble(null);

	/**
	 * Constructor with Geobox coordinates
	 * 
	 * @wbp.parser.constructor
	 * 
	 */
	public GenericSpatialResolutionComposite(Composite parent, IGenericSpatialResolutionModel model) {
		// North, south and west are set to 0 : By default, distance estimates are based on the equator
		this(parent, model, new WritableDouble(0.1), new WritableDouble(-0.1), new WritableDouble(0d));
	}

	/**
	 * Constructor. <br>
	 * north, south,west are the some values of the geobox used in conversion to estimate the distance
	 */
	@SuppressWarnings("unchecked")
	public GenericSpatialResolutionComposite(Composite parent, IGenericSpatialResolutionModel model,
			WritableDouble north, WritableDouble south, WritableDouble west) {
		super(parent, SWT.NONE);
		this.model = model;
		this.north = north;
		this.south = south;
		this.west = west;

		FontData fontData = parent.getDisplay().getSystemFont().getFontData()[0];
		fontItalic = SWTResourceManager.getFont(fontData.getName(), fontData.getHeight(), SWT.ITALIC);

		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);
		setLayoutData(new GridData(SWT.BEGINNING, SWT.FILL, true, true));

		spatialResolutionComposite = new Composite(this, SWT.NONE);
		spatialResolutionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		StackLayout stackLayout = new StackLayout();
		spatialResolutionComposite.setLayout(stackLayout);
		createControlSpatialResolutionDeg();
		createControlSpatialResolutionMeter();

		if (model.getSpatialResolutionEvaluator().isPresent()) {
			Button btnSpatialResolution = new Button(this, SWT.PUSH | SWT.FLAT);
			btnSpatialResolution.addSelectionListener(
					SelectionListener.widgetSelectedAdapter(event -> evaluateSpatialResolution()));
			btnSpatialResolution.setImage(ImageResources.getImage("icons/16/python.png", this));
			btnSpatialResolution.setText("Compute");
			btnSpatialResolution.setFont(fontItalic);
			btnSpatialResolution.setToolTipText("Request the evaluation of the spatial resolution");
			GridDataFactory.swtDefaults().align(SWT.BEGINNING, SWT.BEGINNING).applyTo(btnSpatialResolution);
		} else {
			new Label(this, SWT.NONE);
		}

		DataBindingContext bindingContext = new DataBindingContext();

		// Conversion Arsec <-> degree
		UpdateValueStrategy<Double, Double> targetToModelArcsec = UpdateValueStrategy
				.create(IConverter.create(value -> value != null ? value / 3600.0 : 1.0));
		UpdateValueStrategy<Double, Double> modelToTargetArcsec = UpdateValueStrategy
				.create(IConverter.create(value -> value != null ? value * 3600.0 : 1.0));
		bindingContext.bindValue(new FormattedTextObservableValue(formattedTextArcsec), spatialResDeg,
				targetToModelArcsec, modelToTargetArcsec);

		// Conversion meters <-> degree
		UpdateValueStrategy<Double, Double> targetToModelLatMeter = UpdateValueStrategy
				.create(IConverter.create(value -> value != null ? meterToDegLatResolution(value) : 1.0));
		UpdateValueStrategy<Double, Double> modelToTargetLatMeter = UpdateValueStrategy
				.create(IConverter.create(value -> value != null ? degToMeterLatResolution(value) : 1.0));
		bindingContext.bindValue(new FormattedTextObservableValue(formattedTextMeterLatEstimate), spatialResDeg,
				targetToModelLatMeter, modelToTargetLatMeter);

		UpdateValueStrategy<Double, Double> targetToModelLonMeter = UpdateValueStrategy
				.create(IConverter.create(value -> value != null ? meterToDegLonResolution(value) : 1.0));
		UpdateValueStrategy<Double, Double> modelToTargetLonMeter = UpdateValueStrategy
				.create(IConverter.create(value -> value != null ? degToMeterLonResolution(value) : 1.0));
		bindingContext.bindValue(new FormattedTextObservableValue(formattedTextMeterLonEstimate), spatialResDeg,
				targetToModelLonMeter, modelToTargetLonMeter);

		// Bindings
		bindingContext.bindValue(new FormattedTextObservableValue(formattedTextDeg), spatialResDeg);
		new Label(spatialResolutionDegComposite, SWT.NONE);
		new Label(spatialResolutionDegComposite, SWT.NONE);
		bindingContext.bindValue(new FormattedTextObservableValue(formattedTextMeter), spatialResMeter);
		new Label(this, SWT.NONE);

		// Force first update
		updateSpatialResolutionLabel();
		initDataBindings();
	}

	/**
	 * Bind model to widget or local variables
	 */
	private void initDataBindings() {

		// Bind spatial resolution
		spatialResMeter.addChangeListener(event -> onSpatialResMeter(spatialResMeter.get()));
		spatialResDeg.addChangeListener(event -> onSpatialResDeg(spatialResDeg.get()));
		model.getSpatialResolution().ifPresent(spatialResolution -> {
			if (hasProjection()) {
				spatialResMeter.set(spatialResolution.get());
			} else {
				spatialResDeg.set(spatialResolution.get());
			}
		});

		// Update when projection changed
		model.getProjection().addValueChangeListener(event -> updateSpatialResolutionLabel());
	}

	/**
	 * React on the change of spatial resolution in degree
	 */
	private void onSpatialResDeg(double valueDeg) {
		if (Double.isFinite(valueDeg)) {
			model.getSpatialResolution().ifPresent(spatialResolution -> spatialResolution.doSetValue(valueDeg));
		}
	}

	/**
	 * React on the change of spatial resolution in meter
	 */
	private void onSpatialResMeter(double valueMeter) {
		if (Double.isFinite(valueMeter)) {
			model.getSpatialResolution().ifPresent(spatialResolution -> spatialResolution.doSetValue(valueMeter));
		}
	}

	private double degToMeterLatResolution(double valueDeg) {
		var point1 = LatLon.fromDegrees((north.get() + south.get()) / 2d, west.get());
		var point2 = LatLon.fromDegrees(point1.latitude.degrees + valueDeg, point1.longitude.degrees);
		return LatLon.ellipsoidalDistance(point1, point2, Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS);
	}

	private double degToMeterLonResolution(double valueDeg) {
		var point1 = LatLon.fromDegrees((north.get() + south.get()) / 2d, west.get());
		var point2 = LatLon.fromDegrees(point1.latitude.degrees, point1.longitude.degrees + valueDeg);
		return LatLon.ellipsoidalDistance(point1, point2, Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS);
	}

	private double meterToDegLatResolution(double valueMeter) {
		double resDeg = spatialResDeg.isValid() && spatialResDeg.get() > 0 ? spatialResDeg.get() : DEFAULT_RES_DEGREE;
		double deltaMeterLat = degToMeterLatResolution(resDeg);
		return valueMeter * resDeg / deltaMeterLat;
	}

	private double meterToDegLonResolution(double valueMeter) {
		double resDeg = spatialResDeg.isValid() && spatialResDeg.get() > 0 ? spatialResDeg.get() : DEFAULT_RES_DEGREE;
		double deltaMeterLon = degToMeterLonResolution(resDeg);
		return valueMeter * resDeg / deltaMeterLon;
	}

	private void createControlSpatialResolutionDeg() {
		spatialResolutionDegComposite = new Composite(spatialResolutionComposite, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(spatialResolutionDegComposite);
		GridLayout glSpatialResolutionDegComposite = new GridLayout(2, false);
		glSpatialResolutionDegComposite.marginHeight = 0;
		glSpatialResolutionDegComposite.marginWidth = 0;
		spatialResolutionDegComposite.setLayout(glSpatialResolutionDegComposite);

		// Arc minute
		comboSrsArcmin = new Combo(spatialResolutionDegComposite, SWT.BORDER | SWT.READ_ONLY);
		comboSrsArcmin.setItems("1/4", "1/8", "1/16", "1/32", "1/64", "1/128", "1/256", "1/512", "1/1024");
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(150, SWT.DEFAULT).applyTo(comboSrsArcmin);

		var lblArcMinUnit = new Label(spatialResolutionDegComposite, SWT.NONE);
		lblArcMinUnit.setText(ARCMIN_UNIT);
		lblArcMinUnit.setFont(fontItalic);

		// Arc second
		var txtSrsArcsec = new Text(spatialResolutionDegComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(150, SWT.DEFAULT).applyTo(txtSrsArcsec);
		formattedTextArcsec = new FormattedText(txtSrsArcsec);
		formattedTextArcsec.setFormatter(new FiniteDoubleFormatter(DOUBLE_FORMAT));

		// Synchronize arcminute/second
		AtomicBoolean isUpdating = new AtomicBoolean(false);
		txtSrsArcsec.addModifyListener(e -> {
			if (isUpdating.compareAndSet(false, true)) {
				comboSrsArcmin.deselectAll();
				isUpdating.set(false);
			}
		});
		comboSrsArcmin.addModifyListener(e -> {
			if (isUpdating.compareAndSet(false, true)) {
				int selection = comboSrsArcmin.getSelectionIndex();
				if (selection >= 0) {
					spatialResDeg.set(60d / (2 << selection + 1) / 3600d);
				}
				isUpdating.set(false);
			}
		});

		var lblSrsArcsecUnit = new Label(spatialResolutionDegComposite, SWT.NONE);
		lblSrsArcsecUnit.setText(ARCSEC_UNIT);
		lblSrsArcsecUnit.setFont(fontItalic);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).applyTo(lblSrsArcsecUnit);

		// Degree
		var txtSrsDeg = new Text(spatialResolutionDegComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(150, SWT.DEFAULT).applyTo(txtSrsDeg);
		formattedTextDeg = new FormattedText(txtSrsDeg);
		formattedTextDeg.setFormatter(new FiniteDoubleFormatter(DOUBLE_FORMAT));

		var lblSrsDegUnit = new Label(spatialResolutionDegComposite, SWT.NONE);
		lblSrsDegUnit.setText("\u00b0");
		lblSrsDegUnit.setFont(fontItalic);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).applyTo(lblSrsDegUnit);

		// Meter
		var txtSrsEstimateMeterLat = new Text(spatialResolutionDegComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(150, SWT.DEFAULT)
				.applyTo(txtSrsEstimateMeterLat);
		formattedTextMeterLatEstimate = new FormattedText(txtSrsEstimateMeterLat);
		formattedTextMeterLatEstimate.setFormatter(new FiniteDoubleFormatter(METER_FORMAT));

		var lblSrsEstimateMeterUnitLat = new Label(spatialResolutionDegComposite, SWT.NONE);
		lblSrsEstimateMeterUnitLat.setText(METER_UNIT + " (N-S estimation)");
		lblSrsEstimateMeterUnitLat.setFont(fontItalic);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).applyTo(lblSrsEstimateMeterUnitLat);

		var txtSrsEstimateMeterLon = new Text(spatialResolutionDegComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(150, SWT.DEFAULT)
				.applyTo(txtSrsEstimateMeterLon);
		formattedTextMeterLonEstimate = new FormattedText(txtSrsEstimateMeterLon);
		formattedTextMeterLonEstimate.setFormatter(new FiniteDoubleFormatter(METER_FORMAT));

		var lblSrsEstimateMeterUnitLon = new Label(spatialResolutionDegComposite, SWT.NONE);
		lblSrsEstimateMeterUnitLon.setText(METER_UNIT + " (W-E estimation)");
		lblSrsEstimateMeterUnitLon.setFont(fontItalic);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).applyTo(lblSrsEstimateMeterUnitLon);

	}

	private void createControlSpatialResolutionMeter() {
		spatialResolutionMeterComposite = new Composite(spatialResolutionComposite, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(spatialResolutionMeterComposite);
		GridLayout glSpatialResolutionMeterComposite = new GridLayout(2, false);
		glSpatialResolutionMeterComposite.marginWidth = 0;
		glSpatialResolutionMeterComposite.marginHeight = 0;
		spatialResolutionMeterComposite.setLayout(glSpatialResolutionMeterComposite);

		// Meter
		var txtSrsMeter = new Text(spatialResolutionMeterComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(150, SWT.DEFAULT).applyTo(txtSrsMeter);
		formattedTextMeter = new FormattedText(txtSrsMeter);
		formattedTextMeter.setFormatter(new FiniteDoubleFormatter(DOUBLE_FORMAT));

		var lblSrsMeterUnit = new Label(spatialResolutionMeterComposite, SWT.NONE);
		lblSrsMeterUnit.setText(METER_UNIT);
		lblSrsMeterUnit.setFont(fontItalic);
		GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).applyTo(lblSrsMeterUnit);
	}

	/**
	 * Invoked when spatial resolution changed in the model.
	 */
	private void updateSpatialResolutionLabel() {
		// Show meter only when projection is present
		if (METER_UNIT.equals(getSpatialResolutionUnit())) {
			((StackLayout) spatialResolutionComposite.getLayout()).topControl = spatialResolutionMeterComposite;
		} else {
			((StackLayout) spatialResolutionComposite.getLayout()).topControl = spatialResolutionDegComposite;
		}

		spatialResolutionComposite.requestLayout();
	}

	/**
	 * Launch the evaluation of the spatial resolution
	 */
	private void evaluateSpatialResolution() {
		model.getSpatialResolutionEvaluator().ifPresent(evaluator -> evaluator.accept(result -> {
			if (hasProjection() && result.containsKey(FUNC_METER)) {
				double spatialResolution = result.get(FUNC_METER);
				logger.debug("SpatialResolution evaluated to {}", spatialResolution);
				spatialResMeter.getRealm().asyncExec(() -> spatialResMeter.set(spatialResolution));
			}
			if (!hasProjection() && result.containsKey(FUNC_DEGREE)) {
				double spatialResolution = result.get(FUNC_DEGREE);
				logger.debug("SpatialResolution evaluated to {}", spatialResolution);
				spatialResDeg.getRealm().asyncExec(() -> {
					if (!comboSrsArcmin.isDisposed()) {
						comboSrsArcmin.deselectAll();
					}
					spatialResDeg.set(spatialResolution);
				});
			}
		}));
	}

	/**
	 * @return true if model has a projection defined
	 */
	private boolean hasProjection() {
		return model.getProjection().isNotNull() && !getProjection().isLongLatProjection();
	}

	/**
	 * @return the projection
	 */
	private Projection getProjection() {
		return model.getProjection().get();
	}

	/**
	 * @return the unit of the spatial resolution
	 */
	private String getSpatialResolutionUnit() {
		return model.getProjection().isNull() || getProjection().isLongLatProjection() ? ARCSEC_UNIT : METER_UNIT;
	}

	/** Model of this page */
	public static interface IGenericSpatialResolutionModel {

		/** @return the used Projection. Mercator by default */
		WritableObject<Projection> getProjection();

		/**
		 * @return the spatial resolution. Value may be Nan when no grid computation is not required
		 */
		Optional<WritableDouble> getSpatialResolution();

		/**
		 * @return the function to call to request an evaluation of the spatial resolution. <br>
		 *         The Consumer will be called with the spatial resolution in meter and degree
		 */
		Optional<Consumer<MapOfDoubleConsumer>> getSpatialResolutionEvaluator();
	}

	/**
	 * @return the {@link #spatialResDeg}
	 */
	public WritableDouble getSpatialResDeg() {
		return spatialResDeg;
	}

	/**
	 * @return the {@link #spatialResMeter}
	 */
	public WritableDouble getSpatialResMeter() {
		return spatialResMeter;
	}

	/**
	 * @return the {@link #comboSrsArcmin}
	 */
	public Combo getComboSrsArcmin() {
		return comboSrsArcmin;
	}

	/** Redefines DoubleFormatter to accept only finite double */
	public static class FiniteDoubleFormatter extends DoubleFormatter {
		public FiniteDoubleFormatter(String editPattern) {
			super(editPattern, Locale.US);
		}

		@Override
		public void setValue(Object value) {
			if (value instanceof Double doubleValue && !doubleValue.isInfinite() && !doubleValue.isNaN())
				super.setValue(value);
			else {
				super.setValue(null);
			}
		}

		/**
		 * @return current number value if valid, <code>null</code> else
		 */
		@Override
		public Object getValue() {
			if (super.getValue() instanceof Double doubleValue && !doubleValue.isInfinite() && !doubleValue.isNaN())
				return doubleValue;
			return null;
		}
	}

}
