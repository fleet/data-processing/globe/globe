/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.wizard.page;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.utils.UIUtils;

/**
 * Generic wizard summary page. <br>
 * Browse all AbstractWizardPage of the wizard and claim theirs summary.
 */
public class SummarytWizardPage extends WizardPage {

	/** Title of the page */
	public static final String TITLE = "Summary";

	/** Tag to identify the beginning of a value in a summary */
	public static final String VALUE_TAG = ": ";

	/** Default summary */
	public static final String NO_SUMMARY = "No specific option";

	/** Key used to find a StyledText among data of AbstractWizardPage */
	public static final String STYLEDTEXT_KEY = SummarytWizardPage.class.getName();

	/** Composite containing all Groups */
	protected Composite mainComposite;

	/**
	 * Constructor
	 */
	public SummarytWizardPage() {
		super(SummarytWizardPage.class.getSimpleName());
		this.setTitle(TITLE);
		this.setMessage("You are ready to start the operation ! Use Finish to validate and proceed.", INFORMATION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createControl(Composite parent) {
		FontData fontData = parent.getDisplay().getSystemFont().getFontData()[0];
		Font boldFont = SWTResourceManager.getFont(fontData.getName(), 9, SWT.BOLD);

		ScrolledComposite scrolled = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(scrolled);
		scrolled.setShowFocusedControl(false);
		scrolled.setExpandHorizontal(true);
		scrolled.setExpandVertical(true);
		setControl(scrolled);
		mainComposite = new Composite(scrolled, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(mainComposite);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(mainComposite);
		scrolled.setContent(mainComposite);

		// Create one Group and Label per AbstractWizardPage
		IWizardPage[] pages = getWizard().getPages();
		for (IWizardPage wizardPage : pages) {
			if (wizardPage != this && wizardPage instanceof ISummarizableWizardPage page) {
				Group group = new Group(mainComposite, SWT.NONE);
				group.setLayout(new GridLayout(1, false));
				GridDataFactory.fillDefaults().grab(true, false).applyTo(group);
				group.setFont(boldFont);
				group.setText(wizardPage.getTitle());

				StyledText text = new StyledText(group, SWT.WRAP | SWT.READ_ONLY);
				GridDataFactory.fillDefaults().grab(true, false).applyTo(text);
				text.setBackground(text.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));

				// Remember which Label is yours
				page.getControl().setData(STYLEDTEXT_KEY, text);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			update();
		}
		super.setVisible(visible);
	}

	/**
	 * Update this page. Grab all other pages summary
	 */
	protected void update() {
		IWizardPage[] pages = getWizard().getPages();
		for (IWizardPage wizardPage : pages) {
			if (wizardPage != this && wizardPage instanceof ISummarizableWizardPage page) {
				StyledText text = (StyledText) page.getControl().getData(STYLEDTEXT_KEY);
				if (page.isEnabled() && text != null) {
					String summary = page.summarize();
					manageVisibility(text, summary);
					if (summary != null) {
						summary = summary.isEmpty() ? NO_SUMMARY : summary;
						text.setText(summary);
						if (summary.contains(VALUE_TAG)) {
							// Update summary and highlight contained values
							highlightValues(text);
						} else {
							// Default summary
							text.setStyleRange(getDefaultStyleRange(summary));
						}
					}
				} else {
					manageVisibility(text, null);
				}
			}
		}
		mainComposite.layout();
		((ScrolledComposite) mainComposite.getParent()).setMinSize(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	protected void manageVisibility(StyledText text, String summary) {
		if (text.getParent().getLayoutData() instanceof GridData gridData) {
			UIUtils.setVisible(text.getParent(), summary != null);
			gridData.exclude = summary == null;
			mainComposite.requestLayout();
		}
	}

	/**
	 * Apply styles to the summary to highlight all values.<br>
	 * Values are the sequence of characters between a colon and the end of the line, tabulation or end of text
	 */
	protected void highlightValues(StyledText text) {
		List<StyleRange> styles = new ArrayList<>();

		// Loop on each tag
		Pattern pattern = Pattern.compile("[\n\t$]");
		String[] chuncks = pattern.split(text.getText());
		int from = 0;
		for (String chunck : chuncks) {
			int tagFrom = chunck.indexOf(VALUE_TAG);
			if (tagFrom > 0) {
				tagFrom += 1;
				styles.add(getDefaultStyleRange(from, from + tagFrom));
				styles.add(getStyleRange(from + tagFrom, from + chunck.length(), SWT.BOLD));
			} else {
				styles.add(getDefaultStyleRange(from, from + chunck.length()));
			}
			from += chunck.length() + 1;
		}

		if (!styles.isEmpty()) {
			text.setStyleRanges(styles.toArray(new StyleRange[styles.size()]));
		} else {
			text.setStyleRange(getDefaultStyleRange(text.getText()));
		}
	}

	/**
	 * @return the default style for the specified summary
	 */
	protected StyleRange getDefaultStyleRange(String summary) {
		return getDefaultStyleRange(0, summary.length());
	}

	/**
	 * @return the a default StyleRange
	 */
	protected StyleRange getDefaultStyleRange(int from, int to) {
		return getStyleRange(from, to, SWT.ITALIC);
	}

	/**
	 * @return the a StyleRange
	 */
	protected StyleRange getStyleRange(int from, int to, int fontStyle) {
		StyleRange result = new StyleRange();
		result.start = from;
		result.length = to - from;
		result.fontStyle = fontStyle;
		return result;
	}
}
