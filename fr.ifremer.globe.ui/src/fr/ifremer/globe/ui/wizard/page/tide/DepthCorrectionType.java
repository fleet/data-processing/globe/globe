package fr.ifremer.globe.ui.wizard.page.tide;

/**
 * Enumerator to specify depth correction types.
 */
public enum DepthCorrectionType {
	
	TIDE("tide correction"), DRAUGHT("draught correction"), NOMINAL_WATERLINE("nominal waterline correction");

	private String name;

	DepthCorrectionType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
