/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard.page;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Path;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoUtils;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.utils.latlon.DegMinSecFormatter;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.dialogs.GeoBoxChooser;
import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerModel;
import fr.ifremer.globe.ui.service.geobox.IGeoBoxLayerService;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.utils.LatLongInputFormatter;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.SuffixedDoubleFormatter;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.wizard.page.GenericSpatialResolutionComposite.IGenericSpatialResolutionModel;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;
import fr.ifremer.globe.utils.number.NumberUtils;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Generic Wizard page to edit a Geobox
 */
public class GenericGeoboxPage extends WizardPage implements ISummarizableWizardPage {

	private static final Logger logger = LoggerFactory.getLogger(GenericGeoboxPage.class);
	private static final ILatLongFormatter DMS = DegMinSecFormatter.getInstance();
	private static final int DTM_PRECISION_DECIMAL_COUNT = 10;

	/** Key in json file produced by pyat */
	private static final String FUNC_SPATIAL_RESOLUTION = "spatial_resolution";

	/** Formatter */
	private static final String GRID_FORMAT = "Grid size : %,d cols x %,d rows = %,d cells";

	/** Constants computed from the image size */
	private int widgetHeight;
	private int widgetWidth;

	/** Used colors */
	private final Color foreground = SWTResourceManager.getColor(SWT.COLOR_WIDGET_FOREGROUND);
	private final Color background = SWTResourceManager.getColor(SWT.COLOR_WHITE);
	private final Color darkColor = SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY);
	private final Color red = SWTResourceManager.getColor(SWT.COLOR_RED);

	/** Used fonts */
	private Font font13;
	private Font fontItalic;
	private Font fontBold;

	/** Central widget containing the map */
	private Label lblMap;
	private Image map;

	/** West widgets */
	private Text txtWest;
	private Text txtLeft;

	/** East widgets */
	private Text txtEast;
	private Text txtRight;

	/** North widgets */
	private Text txtNorth;
	private Text txtTop;

	/** South widgets */
	private Text txtSouth;
	private Text txtBottom;

	/** Computed north widgets */
	private Label lblComputedNorthTag;
	private Text txtComputedNorth;
	private Text txtComputedTop;

	/** Computed east widgets */
	private Label lblComputedEastTag;
	private Text txtComputedEast;
	private Text txtComputedRight;

	/** Arrows widgets */
	private Canvas canvasNorthArrow;
	private Canvas canvasCArrow;
	private Canvas canvasEastArrow;
	private Label lblHelp;

	/** Label to display the grid size */
	private Label lblGridSize;

	/** Model to edit */
	private final IGenericGeoboxPageModel model;
	/**
	 * True when geobox was manually changed.<br>
	 * When false, geobox is automatically updated with the union of all input files.<br>
	 * As soon as the user has changed one of the coordinates, his input is privileged. The geobox is no longer updated
	 * even if the input files change
	 */
	private final AtomicBoolean manual = new AtomicBoolean(false);

	/** MVC model */
	private final WritableDouble north = new WritableDouble(null);
	private final WritableDouble top = new WritableDouble(null);
	private final WritableDouble computedNorth = new WritableDouble(null);
	private final WritableDouble computedTop = new WritableDouble(null);

	private final WritableDouble south = new WritableDouble(null);
	private final WritableDouble bottom = new WritableDouble(null);

	private final WritableDouble west = new WritableDouble(null);
	private final WritableDouble left = new WritableDouble(null);

	private final WritableDouble east = new WritableDouble(null);
	private final WritableDouble right = new WritableDouble(null);
	private final WritableDouble computedEast = new WritableDouble(null);
	private final WritableDouble computedRight = new WritableDouble(null);

	// Grid size
	private int gridCol = 0;
	private int gridRow = 0;

	/** Used to avoid concurrent modifications */
	private final AtomicBoolean updatingCoords = new AtomicBoolean(false);

	/** Last selected file */
	private File lastSelectedFile = null;

	/** Widgets */
	private GenericSpatialResolutionComposite spatialResolutionCmp;
	private Label lblProjection;
	private Button btnCrop;
	private Button expandAction;

	/** Nasa layer model */
	private IGeoBoxLayerModel layerModel;

	/**
	 * Constructor
	 */
	public GenericGeoboxPage(IGenericGeoboxPageModel model) {
		super(GenericGeoboxPage.class.getSimpleName());
		setTitle("Geographic bounds");
		setDescription("Define geographic bounds");

		this.model = model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createControl(Composite parent) {
		loadMap();

		FontData fontData = parent.getDisplay().getSystemFont().getFontData()[0];
		font13 = SWTResourceManager.getFont(fontData.getName(), fontData.getHeight() + 3, SWT.NORMAL);
		fontItalic = SWTResourceManager.getFont(fontData.getName(), fontData.getHeight(), SWT.ITALIC);
		fontBold = SWTResourceManager.getFont(fontData.getName(), fontData.getHeight(), SWT.BOLD);

		Composite container = new Composite(parent, SWT.NONE);
		setControl(container);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(container);
		GridLayoutFactory.fillDefaults().numColumns(8).spacing(10, 10).margins(10, 10).applyTo(container);

		createMenu(container);
		createLabel(container, SWT.LEFT, "", 6, 1);
		lblHelp = createLabel(container, SWT.LEFT, """
				This values are the true resulting geographic
				bounds, computed from the spatial resolution. (The
				provided north and east values do not match
				exactly with a potential grid respecting the south,
				west coordinates and the spatial resolution)""", 2, 3);

		createLabel(container, "");
		createLabel(container, "North");
		createLabel(container, ""); // above arrow
		lblComputedNorthTag = createLabel(container, SWT.CENTER, "Computed north", 2, 1);
		createLabel(container, "");

		createLabel(container, "");
		createControlNorth(container);
		canvasNorthArrow = createHorizontalArrow(container);
		createControlComputedNorth(container);
		canvasCArrow = createCurveArrow(container);

		createControlWest(container);
		createControlMap(container);
		createControlEast(container);
		canvasEastArrow = createHorizontalArrow(container);
		createControlComputedEast(container);
		createLabel(container, "");

		createLabel(container, "West");
		createControlSouth(container);
		createLabel(container, SWT.CENTER, "East", 2, 1);
		createLabel(container, "");
		lblComputedEastTag = createLabel(container, SWT.CENTER, "Computed east", 2, 1);
		createLabel(container, "");

		createLabel(container, "");
		createLabel(container, "South");
		createLabel(container, SWT.NONE, "", 6, 1);

		createControlProjection(container);

		Composite resolutionCmp = new Composite(container, SWT.NONE);
		GridData resolutionCmpGridData = new GridData(SWT.FILL, SWT.FILL, true, false, 8, 1);
		// Mask all when model is not valid (spatial resolution not required)
		resolutionCmpGridData.exclude = model.getSpatialResolution().isEmpty();
		resolutionCmp.setLayoutData(resolutionCmpGridData);
		resolutionCmp.setLayout(new GridLayout(2, false));

		Label spLabel = new Label(resolutionCmp, SWT.NONE);
		spLabel.setText("Spatial resolution :");
		spLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false));

		spatialResolutionCmp = new GenericSpatialResolutionComposite(resolutionCmp, model, north, south, west);
		spatialResolutionCmp.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false));

		createGridSize(container);
		createGridWarning(container);

		initFormatters();
		initDataBindings();
		updateComputedVisibility();
	}

	/** Load the earth image */
	private void loadMap() {
		map = ResourceManager.getPluginImage(OsgiUtils.getBundleName(this), "icons/Eath.png");
		widgetWidth = map.getImageData().width;
		widgetHeight = map.getImageData().height / 2;
	}

	/**
	 * Creates controls to render the menu
	 */
	private void createMenu(Composite parent) {
		Composite toolBarComposite = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(4).applyTo(toolBarComposite);
		GridDataFactory.fillDefaults().span(8, 1).grab(true, false).applyTo(toolBarComposite);

		Button boundsMenu = new Button(toolBarComposite, SWT.PUSH);
		GridDataFactory.fillDefaults().applyTo(boundsMenu);
		boundsMenu.setText("Set geographic bounds");
		boundsMenu.setImage(ResourceManager.getPluginImage(OsgiUtils.getBundleName(this), "icons/16/arrow_down.png"));

		Menu menu = new Menu(parent);
		MenuItem firstItem = new MenuItem(menu, SWT.PUSH);
		firstItem.setText("First input file");
		firstItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> onFirstInputFile()));

		MenuItem unionItem = new MenuItem(menu, SWT.PUSH);
		unionItem.setText("Union of all input files");
		unionItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> onUnionInputFiles()));

		MenuItem loadedItem = new MenuItem(menu, SWT.CASCADE);
		loadedItem.setText("File loaded in Globe...");
		loadedItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> onFileLoaded()));

		MenuItem otherItem = new MenuItem(menu, SWT.CASCADE);
		otherItem.setText("Other file...");
		otherItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> onOtherFile()));

		boundsMenu.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> menu.setVisible(true)));

		expandAction = new Button(toolBarComposite, SWT.PUSH);
		expandAction.setImage(ResourceManager.getPluginImage(OsgiUtils.getBundleName(this), "icons/16/expand.png"));
		GridDataFactory.fillDefaults().applyTo(expandAction);
		updateExpandButtonText();
		expandAction.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> onAlignAndExpandGeobox()));

		if (model.getGeoBoxEvaluator().isPresent()) {
			Button evaluateAction = new Button(toolBarComposite, SWT.PUSH);
			GridDataFactory.fillDefaults().applyTo(expandAction);
			evaluateAction.setText("Evaluate from input files");
			evaluateAction.setImage(ImageResources.getImage("icons/16/python.png", this));
			evaluateAction.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> evaluateGeoBox()));
			evaluateAction.setToolTipText("Request the evaluation of GeoBox and spatial resolution");
		} else {
			new Label(toolBarComposite, SWT.NONE);
		}

		btnCrop = new Button(toolBarComposite, SWT.PUSH);
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.RIGHT, SWT.CENTER).applyTo(btnCrop);
		btnCrop.setText("Set resulting North/East");
		btnCrop.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> onCropRequired()));
	}

	/** Wrap Texts with formatters */
	private void initFormatters() {
		new FormattedText(txtNorth).setFormatter(LatLongInputFormatter.getLatitudeFormatter());
		new FormattedText(txtSouth).setFormatter(LatLongInputFormatter.getLatitudeFormatter());
		new FormattedText(txtComputedNorth).setFormatter(LatLongInputFormatter.getLatitudeFormatter());
		new FormattedText(txtEast).setFormatter(LatLongInputFormatter.getLongitudeFormatter());
		new FormattedText(txtWest).setFormatter(LatLongInputFormatter.getLongitudeFormatter());
		new FormattedText(txtComputedEast).setFormatter(LatLongInputFormatter.getLongitudeFormatter());

		new FormattedText(txtTop).setFormatter(new SuffixedDoubleFormatter(" m"));
		new FormattedText(txtBottom).setFormatter(new SuffixedDoubleFormatter(" m"));
		new FormattedText(txtRight).setFormatter(new SuffixedDoubleFormatter(" m"));
		new FormattedText(txtLeft).setFormatter(new SuffixedDoubleFormatter(" m"));
		new FormattedText(txtComputedRight).setFormatter(new SuffixedDoubleFormatter(" m"));
		new FormattedText(txtComputedTop).setFormatter(new SuffixedDoubleFormatter(" m"));
	}

	/**
	 * Creates controls to render the map
	 */
	private void createControlMap(Composite container) {
		Composite cmpMap = new Composite(container, SWT.BORDER);
		GridLayoutFactory.fillDefaults().applyTo(cmpMap);
		cmpMap.setBackground(background);
		GridDataFactory.fillDefaults().applyTo(cmpMap);

		lblMap = new Label(cmpMap, SWT.NONE);
		lblMap.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		lblMap.setImage(map);

		lblMap.addPaintListener(this::drawGeoBox);
	}

	/**
	 * Creates controls to edit the north latitude
	 */
	private void createControlNorth(Composite container) {
		Composite cmpProjNorth = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(cmpProjNorth);
		GridLayoutFactory.fillDefaults().spacing(0, 4).applyTo(cmpProjNorth);
		txtNorth = createText(cmpProjNorth);
		txtTop = createText(cmpProjNorth);
	}

	/**
	 * Creates controls to edit the south latitude
	 */
	private void createControlSouth(Composite container) {
		Composite cmpSouth = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(cmpSouth);
		GridLayoutFactory.fillDefaults().spacing(0, 4).applyTo(cmpSouth);
		txtSouth = createText(cmpSouth);
		txtBottom = createText(cmpSouth);
	}

	/**
	 * Creates controls to edit the west longitude
	 */
	private void createControlWest(Composite container) {
		Composite cmpWest = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(cmpWest);
		GridLayoutFactory.fillDefaults().spacing(0, 4).applyTo(cmpWest);
		txtWest = createText(cmpWest);
		txtLeft = createText(cmpWest);
	}

	/**
	 * Creates controls to edit the west longitude
	 */
	private void createControlEast(Composite container) {
		Composite cmpEast = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().span(2, 1).applyTo(cmpEast);
		GridLayoutFactory.fillDefaults().spacing(0, 4).applyTo(cmpEast);
		txtEast = createText(cmpEast);
		txtRight = createText(cmpEast);
	}

	/**
	 * Creates controls to edit the north latitude
	 */
	private void createControlComputedNorth(Composite container) {
		// Computed Latitude
		Composite cmpProjBorder = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().span(2, 1).applyTo(cmpProjBorder);
		GridLayoutFactory.fillDefaults().spacing(0, 4).applyTo(cmpProjBorder);

		txtComputedNorth = createComputedLabel(cmpProjBorder);
		txtComputedTop = createComputedLabel(cmpProjBorder);
	}

	/**
	 * Creates controls to edit the north latitude
	 */
	private void createControlComputedEast(Composite container) {
		// Computed longitude
		Composite cmpComputedBorder = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().span(2, 1).applyTo(cmpComputedBorder);
		GridLayoutFactory.fillDefaults().spacing(0, 4).applyTo(cmpComputedBorder);

		txtComputedEast = createComputedLabel(cmpComputedBorder);
		txtComputedRight = createComputedLabel(cmpComputedBorder);
	}

	/**
	 * Creates controls to display the projection
	 */
	private void createControlProjection(Composite container) {
		lblProjection = new Label(container, SWT.NONE);
		lblProjection.setFont(fontItalic);
		GridDataFactory.fillDefaults().span(8, 1).applyTo(lblProjection);
		updateProjectionLabel();
	}

	/**
	 * Update controls to display the projection
	 */
	private void updateProjectionLabel() {
		if (hasProjection()) {
			lblProjection.setText("Projection : " + getProjection().getProj4ProjectionString());
		}
		UIUtils.setVisible(lblProjection, hasProjection());

		updateExpandButtonText();
	}

	/**
	 * Update text to display in the expand button
	 */
	private void updateExpandButtonText() {
		if (hasProjection()) {
			expandAction.setText("Expand to an integer number of spatial resolution");
		} else {
			expandAction.setText("Expand to an integer number of angle minute");
		}
		expandAction.getParent().layout();
	}

	/**
	 * Creates controls to display the spatial resolution
	 */
	private void createGridSize(Composite container) {
		Composite parent = new Composite(container, SWT.NONE);
		GridDataFactory.fillDefaults().span(8, 1).grab(true, false).applyTo(parent);
		GridLayoutFactory.fillDefaults().numColumns(3).applyTo(parent);

		// Grid
		lblGridSize = new Label(parent, SWT.NONE);
		lblGridSize.setFont(fontItalic);
		GridDataFactory.fillDefaults().grab(true, false).span(3, 1).applyTo(lblGridSize);
	}

	/**
	 * Creates controls to display the warning
	 */
	private void createGridWarning(Composite container) {
		var warning = new Composite(container, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(2).applyTo(warning);
		GridDataFactory.fillDefaults().span(8, 1).applyTo(warning);

		Label icon = new Label(warning, SWT.NONE);
		icon.setImage(ResourceManager.getPluginImage(OsgiUtils.getBundleName(this), "icons/16/warning.png"));

		Label lblWarning = new Label(warning, SWT.NONE);
		lblWarning.setFont(fontBold);
		lblWarning.setText(
				"Warning : grid coordinates are not exactly integer numbers of angle minute (precision ckecked : 10^10) ! Use \u00ab Expand to integer of minutes \u00bb to fix them.");
		UIUtils.setVisible(warning, false);
	}

	/**
	 * Creates controls to display a label
	 */
	private Label createLabel(Composite container, String label) {
		return createLabel(container, SWT.CENTER, label, 1, 1);
	}

	/**
	 * Creates controls to display a label
	 */
	private Label createLabel(Composite container, int style, String label, int hSpan, int vSpan) {
		Label result = new Label(container, style);
		GridDataFactory.fillDefaults().align(SWT.FILL, vSpan == 1 ? SWT.FILL : SWT.CENTER).span(hSpan, vSpan)
				.grab(false, false).applyTo(result);
		result.setFont(fontItalic);
		result.setText(label);
		return result;
	}

	/**
	 * Creates controls to edit a coordinate
	 */
	private Text createText(Composite container) {
		Composite wrappedComposite = new Composite(container, SWT.BORDER);
		GridDataFactory.fillDefaults().hint(widgetWidth, widgetHeight).align(SWT.FILL, SWT.BOTTOM).grab(true, true)
				.applyTo(wrappedComposite);
		GridLayoutFactory.fillDefaults().applyTo(wrappedComposite);

		Text result = new Text(wrappedComposite, SWT.CENTER);
		wrappedComposite.setBackground(background);
		result.setBackground(result.getBackground());
		GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, true).applyTo(result);
		result.setFont(font13);
		return result;
	}

	/**
	 * Creates controls to display a computed coordinate
	 */
	private Text createComputedLabel(Composite container) {
		Composite wrappedComposite = new Composite(container, SWT.BORDER);
		GridDataFactory.fillDefaults().hint(widgetWidth, widgetHeight).align(SWT.FILL, SWT.BOTTOM).grab(true, true)
				.applyTo(wrappedComposite);
		GridLayoutFactory.fillDefaults().applyTo(wrappedComposite);

		Text result = new Text(wrappedComposite, SWT.CENTER | SWT.READ_ONLY);
		result.setBackground(SWTResourceManager.getColor(SWT.COLOR_TRANSPARENT));
		GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, true).applyTo(result);
		result.setFont(font13);
		return result;
	}

	/**
	 * Creates Canvas to draw an horizontal arrow
	 */
	private Canvas createHorizontalArrow(Composite container) {
		Canvas result = new Canvas(container, SWT.NONE);
		GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).applyTo(result);
		result.addPaintListener(this::drawHorizontalArrow);
		return result;
	}

	/**
	 * Draw an horizontal arrow at the middle of the widget
	 */
	private void drawHorizontalArrow(PaintEvent event) {
		event.gc.setForeground(darkColor);
		event.gc.setLineWidth(2);

		Path path = new Path(event.display);

		path.moveTo(0f, event.height / 2f);
		path.lineTo(event.width - 1f, event.height / 2f);
		path.lineTo(event.width - 6f, event.height / 2f - 3f);
		path.lineTo(event.width - 6f, event.height / 2f + 3f);
		path.lineTo(event.width - 1f, event.height / 2f);

		event.gc.drawPath(path);
		path.dispose();
	}

	/**
	 * Creates Canvas to draw a curve arrow
	 */
	private Canvas createCurveArrow(Composite container) {
		Canvas result = new Canvas(container, SWT.NONE);
		GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).applyTo(result);
		result.addPaintListener(this::drawCurveArrow);
		return result;
	}

	/**
	 * Draw a curve arrow at the bottom left of the widget
	 */
	private void drawCurveArrow(PaintEvent event) {
		event.gc.setAntialias(SWT.ON);
		event.gc.setForeground(darkColor);
		event.gc.setLineWidth(2);

		Point fromPoint = new Point(0, event.height / 2);
		Point toPoint = new Point(event.width - 4, event.height - 1);

		Path path = new Path(event.display);
		path.moveTo(fromPoint.x, fromPoint.y);
		path.lineTo(fromPoint.x + 6f, fromPoint.y - 3f);
		path.lineTo(fromPoint.x + 6f, fromPoint.y + 3f);
		path.lineTo(fromPoint.x, fromPoint.y);

		path.quadTo(toPoint.x + 2f, fromPoint.y - 2f, toPoint.x, toPoint.y);
		path.lineTo(toPoint.x - 3f, toPoint.y - 6f);
		path.lineTo(toPoint.x + 3f, toPoint.y - 6f);
		path.lineTo(toPoint.x, toPoint.y);

		event.gc.drawPath(path);
		path.dispose();
	}

	/**
	 * Draw an horizontal arrow at the middle of the widget
	 */
	private void drawGeoBox(PaintEvent event) {
		GeoBox geoBox = getGeoBox();
		if (geoBox != null) {
			double[] geoTransform = { 81.0, 0.45, 0.0, 40.50, 0.0, -0.45 };

			double[] coordsWN = { west.get(), north.get() };
			GdalUtils.transform(coordsWN, geoTransform);
			double[] coordsES = { east.get(), south.get() };
			GdalUtils.transform(coordsES, geoTransform);

			float maxX = map.getImageData().width - 1f;
			float maxY = map.getImageData().height - 1f;
			var up = (float) Math.max(1f, Math.min(coordsWN[1], maxY));
			var down = (float) Math.max(1f, Math.min(coordsES[1], maxY));
			if (down > up) {
				var mapLeft = (float) Math.max(1f, Math.min(coordsWN[0], maxX));
				var mapRight = (float) Math.max(1f, Math.min(coordsES[0], maxX));
				event.gc.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
				event.gc.setLineWidth(4);
				Path path = new Path(event.display);
				if (mapRight > mapLeft) {
					path.addRectangle(mapLeft, up, mapRight - mapLeft, down - up);
				} else {
					// East part
					path.moveTo(0f, up);
					path.lineTo(mapRight, up);
					path.lineTo(mapRight, down);
					path.lineTo(0f, down);
					// West part
					path.moveTo(maxX, up);
					path.lineTo(mapLeft, up);
					path.lineTo(mapLeft, down);
					path.lineTo(maxX, down);
				}
				event.gc.drawPath(path);
				path.dispose();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		StringBuilder sb = new StringBuilder();
		if (model.getGeoBox().isNotNull()) {
			GeoBox geobox = getGeoBox();
			if (hasProjection()) {
				sb.append("Projection : ").append(getProjection().getProjectionSettings().proj4String()).append("\n");
				sb //
						.append("Top : ").append(NumberUtils.getStringMetersDouble(geobox.getTop()))//
						.append("\t Left :  ").append(NumberUtils.getStringMetersDouble(geobox.getLeft()))//
						.append("\t Bottom : ").append(NumberUtils.getStringMetersDouble(geobox.getBottom()))//
						.append("\t Right : ").append(NumberUtils.getStringMetersDouble(geobox.getRight()));
			} else {
				sb //
						.append("North : ").append(FormatLatitudeLongitude.latitudeToString(geobox.getTop()))//
						.append("\t West :  ").append(FormatLatitudeLongitude.longitudeToString(geobox.getLeft()))//
						.append("\t South : ").append(FormatLatitudeLongitude.latitudeToString(geobox.getBottom()))//
						.append("\t East : ").append(FormatLatitudeLongitude.longitudeToString(geobox.getRight()));
			}
			model.getSpatialResolution().ifPresent(
					spatialResolution -> sb.append("\nSpatial resolution : ").append(spatialResolution.get()));
		}

		return sb.toString();
	}

	/**
	 * @return true if model has a projection defined
	 */
	private boolean hasProjection() {
		return model.getProjection().isNotNull() && !getProjection().isLongLatProjection();
	}

	/**
	 * @return the projection
	 */
	private Projection getProjection() {
		return model.getProjection().get();
	}

	/**
	 * @return true if model has a spatial resolution defined
	 */
	private boolean hasSpatialResolution() {
		return model.getSpatialResolution().isPresent();
	}

	/**
	 * Bind model to widget or local variables
	 */
	private void initDataBindings() {

		// Bind spatial resolution
		getSpatialResMeter().addChangeListener(event -> computeProjectedGrid());
		getSpatialResDeg().addChangeListener(event -> computeLatlonGrid());

		// Bind geobox components to local variables
		WritableObject<GeoBox> geoBox = model.getGeoBox();
		geoBox.addChangeListener(event -> onGeoboxChanged(geoBox.get()));
		onGeoboxChanged(geoBox.get());
		onProjectionChanged(null);

		model.getGeoboxes().addChangeListener(e -> {
			// Recomputed geobox when any input file changed
			if (!manual.get()) {
				onUnionInputFiles();
			}
		});

		// Bind spatial resolution
		model.getSpatialResolution().ifPresent(spatialResolution -> {
			if (hasProjection())
				computeProjectedGrid();
			else
				computeLatlonGrid();
		});

		north.addChangeListener(event -> onLatLonGeoboxChanged());
		west.addChangeListener(event -> onLatLonGeoboxChanged());
		south.addChangeListener(event -> onLatLonGeoboxChanged());
		east.addChangeListener(event -> onLatLonGeoboxChanged());

		top.addChangeListener(event -> onProjectedGeoboxChanged());
		left.addChangeListener(event -> onProjectedGeoboxChanged());
		bottom.addChangeListener(event -> onProjectedGeoboxChanged());
		right.addChangeListener(event -> onProjectedGeoboxChanged());

		// Bind geobox local variables to widgets
		bindCoordToModel(txtNorth, north);
		bindCoordToModel(txtSouth, south);
		bindCoordToModel(txtWest, west);
		bindCoordToModel(txtEast, east);

		bindCoordToModel(txtTop, top);
		bindCoordToModel(txtBottom, bottom);
		bindCoordToModel(txtLeft, left);
		bindCoordToModel(txtRight, right);

		bindCoordToModel(txtComputedEast, computedEast);
		bindCoordToModel(txtComputedNorth, computedNorth);
		bindCoordToModel(txtComputedRight, computedRight);
		bindCoordToModel(txtComputedTop, computedTop);

		model.getProjection().addValueChangeListener(event -> onProjectionChanged(event.diff.getOldValue()));
	}

	/**
	 * Project geographic coords <br>
	 * Avoid Gdal error "tolerance condition error"<br>
	 * on longitude equals to 180 or -180<br>
	 * on latitude equals to 90 or -90
	 *
	 * @return projected values
	 */
	private double[] project(double lon, double lat) {
		double lon2 = NumberUtils.normalizedDegreesLongitude(lon); // 180>=lon>=-180
		lon2 = Math.max(lon2, Math.nextUp(-180.0)); // lon2 > -180
		lon2 = Math.min(lon2, Math.nextDown(180.0)); // lon2 > -180

		double lat2 = NumberUtils.normalizedDegreesLatitude(lat);// 90>=lat>=-90
		lat2 = Math.max(lat2, Math.nextUp(-90.0)); // lon > -180
		lat2 = Math.min(lat2, Math.nextDown(90.0)); // lon > -180
		try {
			return getProjection().project(lon2, lat2);
		} catch (ProjectionException e) {
			return new double[] { Double.NaN, Double.NaN };
		}
	}

	private GeoBox getLatLonGeoBox() {
		return new GeoBox(north.get(), south.get(), east.get(), west.get());
	}

	/**
	 * Reacts on lat/lon changes
	 */
	private void onLatLonGeoboxChanged() {
		if (!updatingCoords.get())
			setGeoBox(getLatLonGeoBox());
	}

	private GeoBox getProjectedGeoBox() {
		return new GeoBox(top.get(), bottom.get(), left.get(), right.get(), getProjection().getProjectionSettings());
	}

	/**
	 * Reacts on metric coordinates changes
	 */
	private void onProjectedGeoboxChanged() {
		if (!updatingCoords.get())
			setGeoBox(getProjectedGeoBox());
	}

	/**
	 * React on the geobox value changes
	 */
	private void onGeoboxChanged(GeoBox geoBox) {
		if (geoBox != null && updatingCoords.compareAndSet(false, true)) {
			logger.debug("Geobox changed : {}", geoBox);
			if (geoBox.getProjection().isLongLatProjection()) {
				north.set(geoBox.getTop());
				south.set(geoBox.getBottom());
				west.set(geoBox.getLeft());
				east.set(geoBox.getRight());
				// projection
				if (hasProjection()) {
					var projectedGeoBox = geoBox.projectQuietlyAndRound(getProjection());
					top.set(projectedGeoBox.getTop());
					bottom.set(projectedGeoBox.getBottom());
					left.set(projectedGeoBox.getLeft());
					right.set(projectedGeoBox.getRight());
				}
			} else {
				top.set(geoBox.getTop());
				bottom.set(geoBox.getBottom());
				left.set(geoBox.getLeft());
				right.set(geoBox.getRight());
				// unproject
				var latlonGeoBox = geoBox.asLonLatQuietly();
				north.set(latlonGeoBox.getTop());
				south.set(latlonGeoBox.getBottom());
				west.set(latlonGeoBox.getLeft());
				east.set(latlonGeoBox.getRight());
			}
			computeGrid();
			lblMap.redraw();
			model.getGeoBox().set(geoBox);
			updatingCoords.set(false);
		}
	}

	/**
	 * React on the Projection value changes
	 */
	private void onProjectionChanged(Projection oldProjection) {
		Projection projection = model.getProjection().get();
		if (logger.isDebugEnabled()) {
			logger.debug("Projection changed : {}",
					projection != null ? projection.getProj4ProjectionString() : "null");
		}

		updateProjectionLabel();

		boolean visible = hasProjection();
		UIUtils.setVisible(txtRight.getParent(), visible);
		UIUtils.setVisible(txtLeft.getParent(), visible);
		UIUtils.setVisible(txtTop, visible);
		UIUtils.setVisible(txtTop.getParent(), visible);
		UIUtils.setVisible(txtBottom.getParent(), visible);

		// Projection ?
		if (getSpatialResDeg().isValid() && hasProjection() && updatingCoords.compareAndSet(false, true)) {
			// Compute spatial resolution in meter. Try to keep the same grid size
			double x1 = project(west.get(), north.get())[0];
			double x2 = project(west.get() + getSpatialResDeg().get(), north.get())[0];
			getSpatialResMeter().set(Math.abs(x2 - x1));
			updatingCoords.set(false);
		}

		// Lonlat ?
		if (getSpatialResMeter().isValid() && !getSpatialResDeg().isValid() && oldProjection != null
				&& updatingCoords.compareAndSet(false, true)) {
			// Compute spatial resolution in °. Try to keep the same grid size
			double lon1 = oldProjection.unprojectQuietly(left.get(), top.get())[0];
			double lon2 = oldProjection.unprojectQuietly(left.get() + getSpatialResMeter().get(), top.get())[0];
			getSpatialResDeg().set(Math.abs(lon2 - lon1));
			updatingCoords.set(false);
		}

		// Invoke setGeoBox to force the Geobox initalization with the new projection
		setGeoBox(getGeoBox());

		updateComputedVisibility();
	}

	/** Compute NE according to the spatial resolution. */
	private void computeGrid() {
		if (hasSpatialResolution()) {
			if (hasProjection()) {
				computeProjectedGrid();
			} else {
				computeLatlonGrid();
			}
		}
	}

	/** Compute NE according to the spatial resolution in meters. */
	private void computeProjectedGrid() {
		updatePageComplete();
		logger.debug("Computing coords");
		double meterResolution = getSpatialResMeter().get();
		if (!getSpatialResMeter().isValid() || meterResolution == 0d) {
			return;
		}
		var geoBox = getProjectedGeoBox();
		double width = geoBox.getWidth();
		double height = geoBox.getHeight();

		if (Double.isFinite(width) && Double.isFinite(height)) {
			gridCol = estimateCellSize(width, meterResolution);
			gridRow = estimateCellSize(height, meterResolution);
			displayGrid();

			double newWidth = gridCol * meterResolution;
			double newHeight = gridRow * meterResolution;
			computedRight.set(right.get() + newWidth - width);
			computedTop.set(bottom.get() + newHeight);

			double[] lonlat = getProjection().unprojectQuietly(computedRight.get(), computedTop.get());
			if (lonlat != null) {
				if (Double.isFinite(lonlat[0])) {
					if (logger.isDebugEnabled()) {
						logger.debug("East => Original:{} computed:{}", east.get(), lonlat[0]);
						logger.debug("East => Original:{} computed:{}", DMS.formatLongitude(east.get()),
								DMS.formatLongitude(lonlat[0]));
					}
					computedEast.set(lonlat[0]);
				}
				if (Double.isFinite(lonlat[1])) {
					if (logger.isDebugEnabled()) {
						logger.debug("North => Original:{} computed:{}", north.get(), lonlat[1]);
						logger.debug("North => Original:{} computed:{}", DMS.formatLatitude(north.get()),
								DMS.formatLatitude(lonlat[1]));
					}
					computedNorth.set(lonlat[1]);
				}
			}
		} else {
			computedNorth.setToNull();
			computedTop.setToNull();
			computedEast.setToNull();
			computedRight.setToNull();
		}

		updateComputedVisibility();
	}

	/**
	 * Display the grid label
	 */
	private void displayGrid() {
		long gridSize = (long) gridCol * gridRow;
		lblGridSize.setText(String.format(GRID_FORMAT, gridCol, gridRow, gridSize));
		if (gridSize > 0l && gridSize < 1_000_000l) {
			lblGridSize.setForeground(foreground);
		} else {
			lblGridSize.setForeground(red);
		}
	}

	/** Estimate cell size given two limits and a spatial_resolution */
	private int estimateCellSize(double size, double spatialResolution) {
		var result = BigDecimal.valueOf(size / spatialResolution);
		// we estimated resolution, but to prevent adding a row or col due to precision
		// issues, first compute size with
		// a 10 decimal resolution
		result = result.setScale(DTM_PRECISION_DECIMAL_COUNT, RoundingMode.HALF_UP);
		result = result.abs();
		// then use a ceil to include border cell if there is still any
		result = result.setScale(0, RoundingMode.CEILING);
		return result.intValue();
	}

	/** Compute NE according to the spatial resolution in arc second */
	private void computeLatlonGrid() {
		updatePageComplete();

		logger.debug("Computing LonLat coords");
		double degreeResolution = getSpatialResDeg().get();
		if (!getSpatialResDeg().isValid() || degreeResolution == 0d) {
			return;
		}

		var geoBox = getLatLonGeoBox();
		double width = geoBox.getWidth();
		double height = geoBox.getHeight();

		if (Double.isFinite(width) && Double.isFinite(height)) {
			gridCol = estimateCellSize(width, degreeResolution);
			gridRow = estimateCellSize(height, degreeResolution);
			displayGrid();

			double newWidth = gridCol * degreeResolution;
			double newHeight = gridRow * degreeResolution;
			computedEast.set(east.get() + newWidth - width);
			computedNorth.set(south.get() + newHeight);

			updateComputedVisibility();
		}
	}

	/** Hide computed NE when it is equals to the edited NE */
	private void updateComputedVisibility() {
		txtComputedEast.getDisplay().asyncExec(() -> {
			boolean same = true;
			if (hasSpatialResolution()) {
				if (hasProjection()) {
					if (!txtComputedRight.getText().isEmpty() && !txtComputedTop.getText().isEmpty()) {
						same = txtComputedRight.getText().equals(txtRight.getText())
								&& txtComputedTop.getText().equals(txtTop.getText());
					}
				} else {
					if (!txtComputedEast.getText().isEmpty() && !txtComputedNorth.getText().isEmpty()) {
						same = txtComputedEast.getText().equals(txtEast.getText())
								&& txtComputedNorth.getText().equals(txtNorth.getText());
					}
				}
			}
			updateComputedVisibility(!same);
		});
	}

	/** Hide computed NE when it is equals to the edited NE */
	private void updateComputedVisibility(boolean visible) {
		logger.debug("Show computed values : {}", visible);

		UIUtils.setVisible(lblComputedNorthTag, visible);
		UIUtils.setVisible(txtComputedNorth.getParent(), visible);
		UIUtils.setVisible(txtComputedTop.getParent(), visible && hasProjection());

		UIUtils.setVisible(canvasNorthArrow, visible);

		UIUtils.setVisible(lblComputedEastTag, visible);
		UIUtils.setVisible(txtComputedEast.getParent(), visible);
		UIUtils.setVisible(txtComputedRight.getParent(), visible && hasProjection());
		UIUtils.setVisible(canvasEastArrow, visible);

		UIUtils.setVisible(canvasCArrow, visible);
		UIUtils.setVisible(lblHelp, visible);
		UIUtils.setVisible(btnCrop, visible);
	}

	/** Sets whether this page is complete. */
	private void updatePageComplete() {
		boolean complete = true;
		if (hasSpatialResolution()) {
			if (hasProjection()) {
				complete = getSpatialResMeter().isValid() && getSpatialResMeter().get() > 0d;
			} else {
				complete = getSpatialResDeg().isValid() && getSpatialResDeg().get() > 0d;
			}
		}
		complete = complete && !getGeoBox().isEmpty();
		setPageComplete(complete);
	}

	/** React on the selection of the menu item "First input file" */
	private void onFirstInputFile() {
		if (!model.getGeoboxes().isEmpty()) {
			logger.debug("Set geobox from first file");
			setGeoBox(new GeoBox(model.getGeoboxes().get(0)));
			IGeoBoxLayerService.grab().revealLayer(layerModel);
		}
	}

	/** React on the selection of the menu item "Union input files" */
	private void onUnionInputFiles() {
		if (!model.getGeoboxes().isEmpty()) {
			logger.debug("Set geobox from union files");
			manual.set(false);
			setGeoBox(GeoBox.englobe(model.getGeoboxes()));
			IGeoBoxLayerService.grab().revealLayer(layerModel);
		}
	}

	/** React on the selection of the menu item "File loaded in Globe" */
	private void onFileLoaded() {
		PopupDialog dialog = new GeoBoxChooser(getShell(), "File loaded in Globe", fileGeoBox -> {
			setGeoBox(fileGeoBox);
			IGeoBoxLayerService.grab().revealLayer(layerModel);
		});
		dialog.open();
	}

	/** React on the selection of the menu item "Other file" */
	private void onOtherFile() {
		FileDialog dialog = new FileDialog(getShell());
		dialog.setText("Extract geographic bounds from...");
		if (lastSelectedFile != null) {
			dialog.setFilterPath(lastSelectedFile.getAbsolutePath());
			dialog.setFileName(lastSelectedFile.getName());
		}
		String selectedFile = dialog.open();
		if (selectedFile != null) {
			lastSelectedFile = new File(selectedFile);

			IFileService fileService = IFileService.grab();
			Optional<IFileInfo> infoStore = fileService.getFileInfo(selectedFile);
			if (infoStore.isPresent()) {
				GeoBox geoBox = infoStore.get().getGeoBox();
				if (geoBox != null) {
					logger.debug("Set geobox from other file");
					setGeoBox(geoBox);
					IGeoBoxLayerService.grab().revealLayer(layerModel);
				} else {
					Messages.openWarningMessage("Unsupported file", "Unable to extract the geobox from the file\n"
							+ lastSelectedFile.getName() + " is not supported or reading error");
				}
			} else {
				Messages.openWarningMessage("Unsupported file",
						"The file " + lastSelectedFile.getName() + " is not supported or reading error");
			}
		}
	}

	/** React on the selection of the menu item "Computed North/East" */
	private void onCropRequired() {
		var croppedGeoBox = hasProjection()
				? new GeoBox(computedTop.get(), bottom.get(), left.get(), computedRight.get(),
						getProjection().getProjectionSettings())
				: new GeoBox(computedNorth.get(), south.get(), computedEast.get(), west.get());
		setGeoBox(croppedGeoBox);
	}

	/** Set the new Geobox */
	private void setGeoBox(GeoBox geoBox) {

		// Ensure the geobox to set is suitable with the projection
		var geoboxProjection = geoBox.getProjection();
		if (hasProjection()) {
			if (geoboxProjection == null || geoboxProjection.isLongLatProjection())
				// From LonLat geobox to projected
				geoBox = geoBox.projectQuietlyAndRound(getProjection());
			else if (!geoboxProjection.isSame(getProjection().getProjectionSettings())) {
				// From projected geobox to an other projection
				geoBox = geoBox.asLonLatQuietly().projectQuietlyAndRound(getProjection());
			}
		} else {
			if (geoboxProjection != null && !geoboxProjection.isLongLatProjection())
				// From projected geobox to LonLat
				geoBox = geoBox.asLonLatQuietly();
		}

		model.getGeoBox().set(geoBox);
	}

	/** get the Geobox */
	private GeoBox getGeoBox() {
		return Optional.ofNullable(model.getGeoBox().get()).orElse(GeoBox.getEmptyGeobox());
	}

	/** React on the selection of the menu item "Expand" */
	private void onAlignAndExpandGeobox() {
		logger.debug("Aligning and expanding the geobox");
		if (!hasProjection()) {
			setGeoBox(GeoUtils.realign(getGeoBox(), 1.0 / 60.0)); // Aligning to integer of arcmin
			return;
		}

		if (getSpatialResMeter().isValid() && getSpatialResMeter().get() > 0d) {
			var geoBox = new GeoBox(top.get(), bottom.get(), left.get(), right.get(),
					getProjection().getProjectionSettings());
			setGeoBox(GeoUtils.realign(geoBox, getSpatialResMeter().get()));
		} else
			Messages.openErrorMessage("Miscalculation",
					"Invalid spatial resolution \nEnter a correct value and relaunch the calculation");
	}

	/** Bind model variable to Text widget */
	private void bindCoordToModel(Text source, WritableDouble modelValue) {
		FormattedText formattedText = (FormattedText) source.getData(FormattedText.TEXT_DATA_KEY);
		formattedText.setValue(modelValue.getValue());

		// From Text widget to model
		formattedText.getControl().addModifyListener(e -> {
			manual.set(true);
			Double newValue = (Double) formattedText.getValue();
			if (newValue != null && !newValue.equals(modelValue.getValue()))
				modelValue.setValue(newValue);
		});

		// From model to Text widget
		modelValue.addChangeListener(e -> {
			Double newValue = modelValue.getValue();
			if (newValue != null && !newValue.equals(formattedText.getValue()))
				formattedText.setValue(newValue);
		});
	}

	/**
	 * Launch the evaluation of the evaluateGeoBox
	 */
	private void evaluateGeoBox() {
		var geoBoxEvaluator = model.getGeoBoxEvaluator();
		if (geoBoxEvaluator.isPresent()) {
			geoBoxEvaluator.get().accept(result -> {
				try {
					model.getGeoBox().getRealm()
							.asyncExec(() -> setGeoBox(new GeoBox(result.get("top"), result.get("bottom"),
									result.get("left"), result.get("right"), //
									getProjection() != null ? getProjection().getProjectionSettings()
											: StandardProjection.LONGLAT)));
					if (hasProjection() && result.containsKey(FUNC_SPATIAL_RESOLUTION)) {
						getSpatialResMeter().getRealm()
								.asyncExec(() -> getSpatialResMeter().set(result.get(FUNC_SPATIAL_RESOLUTION)));
					}
					if (!hasProjection() && result.containsKey(FUNC_SPATIAL_RESOLUTION)) {
						getSpatialResDeg().getRealm().asyncExec(() -> {
							if (!getComboSrsArcmin().isDisposed()) {
								getComboSrsArcmin().deselectAll();
							}
							getSpatialResDeg().set(result.get(FUNC_SPATIAL_RESOLUTION));
						});
					}
				} catch (Exception e) {
					Messages.openErrorMessage("Evaluations", "Evaluation failed\n" + e.getMessage());
				}
			});
		}
	}

	/** Model of this page */
	public interface IGenericGeoboxPageModel extends IGenericSpatialResolutionModel {
		/** @return all reference geobox */
		WritableList<GeoBox> getGeoboxes();

		/** @return the edited geobox */
		WritableObject<GeoBox> getGeoBox();

		/**
		 * @return the function to call to request an evaluation of the GeoBox and spatial resolution. <br>
		 *         The Consumer will be called with the computed GeoBox
		 */
		Optional<Consumer<MapOfDoubleConsumer>> getGeoBoxEvaluator();
	}

	/** {@inheritDoc} */
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);

		// Manage the Geobox layer in geographic view
		if (visible && layerModel == null) {
			layerModel = IGeoBoxLayerService.grab().createLayer();
			layerModel.getGeoBox().set(getGeoBox());
			model.getGeoBox().addChangeListener(event -> {
				if (layerModel != null) {
					layerModel.getGeoBox().set(getGeoBox());
				}
			});
		}
		if (!visible && layerModel != null) {
			IGeoBoxLayerService.grab().removeLayer(layerModel);
			layerModel = null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		IGeoBoxLayerService.grab().removeLayer(layerModel);
	}

	private WritableDouble getSpatialResDeg() {
		return spatialResolutionCmp.getSpatialResDeg();
	}

	private WritableDouble getSpatialResMeter() {
		return spatialResolutionCmp.getSpatialResMeter();
	}

	private Combo getComboSrsArcmin() {
		return spatialResolutionCmp.getComboSrsArcmin();
	}

}
