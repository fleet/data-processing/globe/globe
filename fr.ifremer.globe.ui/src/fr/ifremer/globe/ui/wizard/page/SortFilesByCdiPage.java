package fr.ifremer.globe.ui.wizard.page;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.observable.list.ListChangeEvent;
import org.eclipse.core.databinding.observable.list.ListDiffVisitor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceId;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceIdBuilder;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceMetadata;
import fr.ifremer.globe.core.model.file.ICdiService;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.widget.TableMultiColumnSorter;
import fr.ifremer.globe.ui.widget.TableMultiColumnSorter.ICheckable;
import fr.ifremer.globe.ui.widget.TableMultiColumnSorter.ITableMultColumnSorterListener;

public class SortFilesByCdiPage extends WizardPage implements ISummarizableWizardPage, ITableMultColumnSorterListener {

	/** Private attributes **/
	private WritableObjectList<File> model;
	private TableMultiColumnSorter<FileInfo> tableMultiColSort;
	private List<FileInfo> inputFiles = new ArrayList<>();

	/** True when input files in model changed dans the table is currently updated */
	private AtomicBoolean isDealingWithTheModelModification = new AtomicBoolean(true);

	/**
	 * Constructor
	 */
	protected SortFilesByCdiPage(WritableObjectList<File> model) {
		super(SortFilesByCdiPage.class.getSimpleName());
		this.model = model;
		setTitle("Selected files will be used from top to bottom to fill the resulting DTM");
		setDescription("Selected files will be used from top to bottom to fill the resulting DTM");

		model.stream().map(FileInfo::new).forEach(inputFiles::add);
		model.addListChangeListener(this::onFilesChanged);
	}

	/**
	 * Build the page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		setControl(container);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));

		// Create the table
		tableMultiColSort = new TableMultiColumnSorter<>(container, SWT.MULTI | SWT.FULL_SELECTION);
		// Set input files
		tableMultiColSort.setInputs(inputFiles);

		// Add columns with a column builder
		// Title
		tableMultiColSort.new ColumnBuilder().setTitle("Title").setBound(250).setLabelProvider(FileInfo::getFilename)
				.build();

		// CDI
		tableMultiColSort.new ColumnBuilder().setTitle("Identifier").setBound(170)
				.setLabelProvider(FileInfo::getCdiValue).build();

		// State
		tableMultiColSort.new ColumnBuilder().setTitle("Status").setBound(50).setLabelProvider(FileInfo::getState)
				.setColorProvider(FileInfo::getStateColor).build();

		// Age
		tableMultiColSort.new ColumnBuilder().setTitle("Age").setBound(50).setAlignment(SWT.RIGHT)
				.setLabelProvider(FileInfo::getQIAge).build();

		// Survey type
		tableMultiColSort.new ColumnBuilder().setTitle("Survey type").setBound(80).setAlignment(SWT.RIGHT)
				.setLabelProvider(FileInfo::getQISurveyType).build();

		// Hor. accuracy
		tableMultiColSort.new ColumnBuilder().setTitle("Hor. accuracy").setBound(80).setAlignment(SWT.RIGHT)
				.setLabelProvider(FileInfo::getQIHorizAccuracy).build();

		// Vert. accuracy
		tableMultiColSort.new ColumnBuilder().setTitle("Vert. accuracy").setBound(80).setAlignment(SWT.RIGHT)
				.setLabelProvider(FileInfo::getQIVertAccuracy).build();

		// Add default sort levels
		tableMultiColSort.addSortLevel("Age", TableMultiColumnSorter.DECREASING);
		tableMultiColSort.addSortLevel("Survey type", TableMultiColumnSorter.DECREASING);
		tableMultiColSort.addSortLevel("Hor. accuracy", TableMultiColumnSorter.DECREASING);
		tableMultiColSort.addSortLevel("Vert. accuracy", TableMultiColumnSorter.DECREASING);

		tableMultiColSort.addRefreshListeners(this);

		// tableMultiColSort is now synchronized with the model.
		isDealingWithTheModelModification.set(false);
	}

	/** React to the model changes */
	protected <T extends File> void onFilesChanged(ListChangeEvent<T> event) {
		// Going to synchronize the table with the input files of the model
		if (isDealingWithTheModelModification.compareAndSet(false, true)) {
			event.diff.accept(new ListDiffVisitor<T>() {

				@Override
				public void handleRemove(int index, File file) {
					Optional<FileInfo> option = inputFiles.stream().filter(fileInfo -> fileInfo.getFile().equals(file))
							.findAny();
					if (option.isPresent()) {
						option.get().setChecked(false);
					}
				}

				@Override
				public void handleAdd(int index, File file) {
					Optional<FileInfo> option = inputFiles.stream().filter(fileInfo -> fileInfo.getFile().equals(file))
							.findAny();
					if (option.isPresent()) {
						option.get().setChecked(true);
					} else {
						inputFiles.add(new FileInfo(file));
					}
				}
			});
			tableMultiColSort.refresh();
			isDealingWithTheModelModification.set(false);
		}
	}

	/** React to the files sort */
	@Override
	public void onRefresh() {
		// Ignore the refresh when synchronization of the table is currently running.
		if (isDealingWithTheModelModification.compareAndSet(false, true)) {
			// Compute the new list of files
			List<File> newInputFiles = inputFiles.stream().filter(FileInfo::isChecked).map(FileInfo::getFile)
					.collect(Collectors.toList());

			// Set the new list in the model.
			model.updateWrappedList(newInputFiles);

			isDealingWithTheModelModification.set(false);
		}
	}

	@Override
	public String summarize() {
		return null; // Mask summary
	}

	/**
	 * FileInfo class: manages informations provided by DtmInfo object and CDI meta data
	 */
	private class FileInfo implements ICheckable {

		/** Properties **/
		private File file;
		private boolean checked = true;
		private String cdi;
		private DtmSourceMetadata metaData;

		/** Constructor **/
		public FileInfo(File file) {
			this.file = file;
			retrieveMetaData();
		}

		/** Download meta data */
		public void retrieveMetaData() {
			ICdiService cdiService = ICdiService.grab();
			List<String> cdis = cdiService.getCdis(file.getAbsolutePath());
			if (!cdis.isEmpty()) {
				cdi = cdis.get(0);
				Executors.newSingleThreadExecutor().submit(() -> {
					DtmSourceId url = DtmSourceIdBuilder.build(cdi);
					metaData = url.retrieve();
					tableMultiColSort.refresh();
				});
			}
		}

		@Override
		public String toString() {
			return getFilename();
		}

		public String getFilename() {
			String result = null;
			if (file != null) {
				result = file.getName();
				if (!inputFiles.isEmpty() && inputFiles.get(0).getFile().equals(file)) {
					result += " (Reference)";
				}
			}
			return result;
		}

		public String getCdiValue() {
			return cdi;
		}

		public String getQIAge() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return Integer.toString(metaData.qualityIndicator().getAge());
			}
		}

		public String getQISurveyType() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return Integer.toString(metaData.qualityIndicator().getSurveyType());
			}
		}

		public String getQIHorizAccuracy() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return Integer.toString(metaData.qualityIndicator().getHorizontalAccuracy());
			}
		}

		public String getQIVertAccuracy() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return Integer.toString(metaData.qualityIndicator().getVerticalAccuracy());
			}
		}

		public String getState() {
			String state;
			if (metaData == null) {
				return null;
			}

			switch (metaData.state()) {
			case eExists:
				state = "valid";
				break;
			case eNotFound:
				state = "notFound";
				break;
			case eConnectionError:
			case eURLParsingError:
			default:
				state = "error";
				break;
			}
			return state;
		}

		public Color getStateColor() {
			if (getState() != null && getState().equals("valid")) {
				return SWTResourceManager.getColor(76, 175, 80);
			} else {
				return SWTResourceManager.getColor(SWT.COLOR_RED);
			}
		}

		/**
		 * Return the check state ( {@link ICheckable} interface)
		 */
		@Override
		public boolean isChecked() {
			return checked;
		}

		/**
		 * Set the check state ( {@link ICheckable} interface)
		 */
		@Override
		public void setChecked(boolean checked) {
			this.checked = checked;
		}

		/**
		 * @return the {@link #file}
		 */
		public File getFile() {
			return file;
		}
	}
}
