/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage.SelectInputFilesPageModel;

/**
 * Wizard page to edit masking parameters (ie fill gap process)<br>
 * Delegates the files edition to a SelectInputFilesPage
 */
public class MaskPage extends WizardPage implements ISummarizableWizardPage {
	protected DataBindingContext dataBindingContext;

	/** Model */
	protected MaskPageModel maskPageModel;

	/** Widgets */
	protected SelectInputFilesPage selectInputFilesPage;
	protected Button btnEnable;
	protected Spinner spnMaskSize;
	protected Composite fileComposite;

	/**
	 * Constructor
	 */
	public MaskPage(String title, MaskPageModel maskPageModel) {
		super(SelectInputFilesPage.class.getName(), title, null);
		this.maskPageModel = maskPageModel;

		SelectInputFilesPageModel selectInputFilesPageModel = new SelectInputFilesPageModel() {
			@Override
			public List<Pair<String, String>> getInputFilesFilterExtensions() {
				return maskPageModel.getMaskFilesFilterExtensions();
			}

			@Override
			public WritableObjectList<File> getInputFiles() {
				return maskPageModel.getMaskFiles();
			}

			@Override
			public boolean isMandatory() {
				return false;
			}

		};
		selectInputFilesPage = new SelectInputFilesPage(selectInputFilesPageModel) {
			/** {@inheritDoc} */
			@Override
			public Shell getShell() {
				return MaskPage.this.getShell();
			}
		};
	}

	/** {@inheritDoc} */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		setControl(container);

		container.setLayout(new GridLayout(1, false));

		btnEnable = new Button(container, SWT.CHECK);
		btnEnable.setText("Enabled");
		btnEnable.setSelection(false);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(btnEnable);

		Composite detailComposite = new Composite(container, SWT.NONE);
		GridData gdDetailComposite = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
		gdDetailComposite.heightHint = 101;
		detailComposite.setLayoutData(gdDetailComposite);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(detailComposite);
		GridLayoutFactory.fillDefaults().extendedMargins(20, 0, 20, 0).applyTo(detailComposite);
		GridLayout glDetailComposite = new GridLayout(1, false);
		glDetailComposite.verticalSpacing = 0;
		detailComposite.setLayout(glDetailComposite);

		Composite sizeComposite = new Composite(detailComposite, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(sizeComposite);
		GridLayoutFactory.fillDefaults().numColumns(2).applyTo(sizeComposite);
		GridLayout glSizeComposite = new GridLayout(2, false);
		glSizeComposite.marginWidth = 0;
		glSizeComposite.marginHeight = 0;
		sizeComposite.setLayout(glSizeComposite);

		Label lblSize = new Label(sizeComposite, SWT.NONE);
		GridData gdLblSize = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblSize.horizontalIndent = 15;
		lblSize.setLayoutData(gdLblSize);
		lblSize.setText("Size of the mask : ");

		spnMaskSize = new Spinner(sizeComposite, SWT.BORDER);
		spnMaskSize.setMaximum(31);
		spnMaskSize.setMinimum(3);
		spnMaskSize.setSelection(3);

		Label lblMaskFileList = new Label(detailComposite, SWT.NONE);
		GridData gdLblMaskFileList = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblMaskFileList.horizontalIndent = 15;
		gdLblMaskFileList.verticalIndent = 10;
		lblMaskFileList.setLayoutData(gdLblMaskFileList);
		lblMaskFileList.setText("Mask file list (*.kml or *.shp)");

		fileComposite = new Composite(detailComposite, SWT.NONE);
		fileComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		GridDataFactory.fillDefaults().grab(true, true).applyTo(fileComposite);
		selectInputFilesPage.createControl(fileComposite);

		dataBindingContext = initDataBindings();
		dataBindingContext.updateTargets();
	}

	/** {@inheritDoc} */
	@Override
	public String summarize() {
		if (maskPageModel.getMaskEnable().isFalse()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Mask size : ").append(maskPageModel.getMaskSize().get());

		String filesSummary = selectInputFilesPage.summarize();
		if (filesSummary != null && !filesSummary.isEmpty()) {
			sb.append("\nFiles :\n").append(filesSummary);
		}
		return sb.toString();
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		var enableObservableValue = WidgetProperties.buttonSelection().observe(btnEnable);
		bindingContext.bindValue(enableObservableValue, maskPageModel.getMaskEnable(), null, null);
		//
		var spnMaskObservableEnabled = WidgetProperties.enabled().observe(spnMaskSize);
		bindingContext.bindValue(spnMaskObservableEnabled, enableObservableValue, null, null);
		//
		var spnMaskObservableValue = WidgetProperties.spinnerSelection().observe(spnMaskSize);
		bindingContext.bindValue(spnMaskObservableValue, maskPageModel.getMaskSize(), null, null);
		//
		var fileObservableValue = WidgetProperties.enabled().observe(fileComposite);
		bindingContext.bindValue(fileObservableValue, enableObservableValue, null, null);
		
		return bindingContext;
	}

	/**
	 * Model of the input file selection wizard page
	 */
	public interface MaskPageModel {

		/**
		 * @return the boolean indicating the mask page is activated
		 */
		WritableBoolean getMaskEnable();

		/**
		 * @return the number of cell composing a mask
		 */
		WritableInt getMaskSize();

		/**
		 * @return the masking files
		 */
		WritableObjectList<File> getMaskFiles();

		/**
		 * Type of files allowed for masking (KML_XML, SHAPEFILE...)
		 */
		java.util.List<Pair<String, String>> getMaskFilesFilterExtensions();

	}
}