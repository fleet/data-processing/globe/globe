/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class HeaderDialog extends Dialog {
	private List<ColumnName> columnNames;
	private Table table;
	private TableViewer tableViewer;

	private static final List<String> STR_TYPES = List.of("Not numeric", "Float", "Int");
	private static final List<Class<?>> CLASS_TYPES = List.of(String.class, Double.class, Integer.class);

	/**
	 * Create the dialog.
	 *
	 * @wbp.parser.constructor
	 */
	protected HeaderDialog(Shell parentShell) {
		super(parentShell);
		columnNames = new ArrayList<>();
	}

	/**
	 * Create the dialog.
	 */
	public HeaderDialog(Shell parentShell, List<ColumnName> columnNames) {
		super(parentShell);
		this.columnNames = columnNames;
	}

	/**
	 * Create contents of the dialog.
	 *
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		getShell().setText("Editing the column names");
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite composite = new Composite(container, SWT.NONE);
		TableColumnLayout tcl_composite = new TableColumnLayout();
		composite.setLayout(tcl_composite);

		tableViewer = new TableViewer(composite, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableViewerColumn headerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		headerColumn.setEditingSupport(new EditingSupport(tableViewer) {
			@Override
			protected boolean canEdit(Object element) {
				return ((ColumnName) element).canEdit();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return new TextCellEditor(table);
			}

			@Override
			protected Object getValue(Object element) {
				return ((ColumnName) element).getName() != null ? ((ColumnName) element).getName() : "";
			}

			@Override
			protected void setValue(Object element, Object value) {
				if (value != null && !value.toString().isBlank()) {
					ColumnName columnName = (ColumnName) element;
					if (columnName.getName() != null) {
						columnName.setName(value.toString());
						tableViewer.refresh(element);
					} else {
						// Adding a new header
						columnName.setName(value.toString());
						columnName.setType(Double.class);
						columnNames.add(new ColumnName());
						tableViewer.refresh();
					}
				}
			}
		});
		headerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((ColumnName) element).getName() == null ? "New..." : ((ColumnName) element).getName();
			}
		});
		TableColumn tblclmnHeader = headerColumn.getColumn();
		tcl_composite.setColumnData(tblclmnHeader, new ColumnWeightData(80, 300));
		tblclmnHeader.setText("Header");

		TableViewerColumn numericColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		numericColumn.setEditingSupport(new EditingSupport(tableViewer) {
			@Override
			protected boolean canEdit(Object element) {
				return ((ColumnName) element).canEdit();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return new ComboBoxCellEditor(table, STR_TYPES.toArray(new String[STR_TYPES.size()]), SWT.READ_ONLY);
			}

			@Override
			protected Object getValue(Object element) {
				Class<?> type = ((ColumnName) element).getType();
				return type != null ? CLASS_TYPES.indexOf(type) : -1;
			}

			@Override
			protected void setValue(Object element, Object value) {
				int index = (Integer) value;
				if (index >= 0) {
					((ColumnName) element).setType(CLASS_TYPES.get(index));
					tableViewer.refresh(element);
				}
			}
		});
		numericColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Class<?> type = ((ColumnName) element).getType();
				return type != null ? STR_TYPES.get(CLASS_TYPES.indexOf(type)) : "";
			}
		});
		TableColumn tblclmnNumeric = numericColumn.getColumn();
		tcl_composite.setColumnData(tblclmnNumeric, new ColumnWeightData(20, 100));
		tblclmnNumeric.setText("Numeric");

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(columnNames);
		return container;
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

}
