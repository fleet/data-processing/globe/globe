package fr.ifremer.globe.ui.wizard;

import java.util.Map;

import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang.math.NumberUtils;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;

/**
 * Wizard page to define driver parameters for reading a {@link fr.ifremer.globe.PointCloudFileInfoSettings.RawFile}<br>
 * <br>
 * Takes in account :<br>
 * - ASCII delimiter<br>
 * - depth sign<br>
 * <br>
 * Show the pre-parsed first three lines in a table
 *
 *
 * @author Gael.Quemener@ifremer.fr
 *
 */
public class AsciiDelimiterPage extends WizardPage implements ISummarizableWizardPage {

	protected static final String[] PREDIFINED_DELIMITERS_LABELS = { "Tab", "Semi-column \";\"", "Space", "Comma \",\"",
			"Whitespaces (combination of space, tab…)", "Other" };
	protected static final char[] PREDIFINED_DELIMITERS = { '\t', ';', ' ', ',', '…', ' ' };

	// Widgets
	private Button[] delimiterButtons = new Button[PREDIFINED_DELIMITERS_LABELS.length];
	private Text otherDelimiter;
	private CsvComposite csvComposite;
	private Text decimalPointText;
	private Button btnEnableRowSkipping;
	private Text rowCountToSkip;
	private Button positiveDepthButton;
	private Button negativeDepthButton;

	// Listeners
	private final SelectionListener delimiterButtonListener = SelectionListener
			.widgetSelectedAdapter(this::delimiterChanged);
	private final ModifyListener otherDelimiterListener = e -> otherDelimiterChanged();

	// Constructor parameters
	private IxyzFile file;
	private boolean editable;

	// Model
	private char delimiter = ' ';
	private boolean depthPositiveBelowSurface;
	private char decimalPoint;

	/**
	 *
	 * Constructor<br>
	 * sets name, title, description, RawFile for which the wizard page is intended<br>
	 *
	 * @param editable true to allow column names edition
	 */
	public AsciiDelimiterPage(IxyzFile file, boolean editable) {
		super(AsciiDelimiterPage.class.getName());
		this.file = file;
		this.editable = editable;

		setPageComplete(false);
	}

	/**
	 * React of the delimiter change
	 */
	protected void delimiterChanged(SelectionEvent e) {
		for (int i = 0; i < PREDIFINED_DELIMITERS_LABELS.length; i++) {

			if (!PREDIFINED_DELIMITERS_LABELS[i].equals(((Button) e.getSource()).getText())) {
				// set all others check buttons to "not selected"
				delimiterButtons[i].setSelection(false);
			} else {
				if (!((Button) e.getSource()).getSelection()) {
					delimiterButtons[i].setSelection(true);
				}

				delimiter = PREDIFINED_DELIMITERS[i];

				// if "Other" is selected, set the corresponding text
				// box to enable
				if (PREDIFINED_DELIMITERS_LABELS[i].equals("Other")) {
					otherDelimiter.setEnabled(true);
					var oneDelimiter = otherDelimiter.getText();
					csvComposite.setDelimiter(oneDelimiter.isEmpty() ? ' ' : oneDelimiter.charAt(0));
				} else {
					otherDelimiter.setEnabled(false);
					csvComposite.setDelimiter(PREDIFINED_DELIMITERS[i]);
				}
				update();
			}
		}
	}

	/**
	 * Called when the user change the other listener Text
	 */
	protected void otherDelimiterChanged() {
		var oneDelimiter = otherDelimiter.getText();
		delimiter = oneDelimiter.isEmpty() ? ' ' : oneDelimiter.charAt(0);
		csvComposite.setDelimiter(delimiter);
		update();
	}

	public void updateStatus() {
		setPageComplete(
				csvComposite.isDelimiterValid() && csvComposite.isHeaderValid() && csvComposite.isDecimalPointValid());
		decimalPoint = csvComposite.getDecimalPoint();
		updateDecimalPointText();
	}

	public void updateDecimalPointText() {
		decimalPointText.setText(decimalPoint == ',' ? "Comma \",\"" : "Dot \".\"");
	}

	@Override
	public void createControl(Composite parent) {
		Composite container1 = new Composite(parent, SWT.NULL);
		GridLayout glContainer = new GridLayout(1, false);
		glContainer.verticalSpacing = 10;
		container1.setLayout(glContainer);

		setControl(container1);

		Label label = new Label(container1, SWT.NONE);
		label.setText("This screen lets you set the delimiters your data contains");
		label.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1));

		// Delimiters
		Group delimiterGroup = new Group(container1, SWT.NONE);
		delimiterGroup.setText("Delimiters");
		delimiterGroup.setLayout(new GridLayout(2, false));
		delimiterGroup.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1));

		for (int i = 0; i < PREDIFINED_DELIMITERS_LABELS.length; i++) {
			delimiterButtons[i] = new Button(delimiterGroup, SWT.CHECK);
			delimiterButtons[i].setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			delimiterButtons[i].addSelectionListener(delimiterButtonListener);
			delimiterButtons[i].setText(PREDIFINED_DELIMITERS_LABELS[i]);
			delimiterButtons[i].setSelection(false);

			if (PREDIFINED_DELIMITERS_LABELS[i].equals("Other")) {
				otherDelimiter = new Text(delimiterGroup, SWT.NONE);
				otherDelimiter.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
				otherDelimiter.addModifyListener(otherDelimiterListener);
			} else {
				Label l1 = new Label(delimiterGroup, SWT.NONE);
				l1.setText("");
				l1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			}
		}

		// Depth
		Group depthSignGroup = new Group(container1, SWT.NONE);
		depthSignGroup.setText("Depth");
		depthSignGroup.setLayout(new GridLayout(1, false));
		depthSignGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		negativeDepthButton = new Button(depthSignGroup, SWT.CHECK);
		negativeDepthButton.setText("Depths are negative below surface");
		positiveDepthButton = new Button(depthSignGroup, SWT.CHECK);
		negativeDepthButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				positiveDepthButton.setSelection(false);
				depthPositiveBelowSurface = false;

			}
		});
		positiveDepthButton.setText("Depths are positive below surface");
		positiveDepthButton.setSelection(true);
		positiveDepthButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				negativeDepthButton.setSelection(false);
				depthPositiveBelowSurface = true;

			}
		});

		// Options
		Group grpOptions = new Group(container1, SWT.NONE);
		grpOptions.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpOptions.setText("Options");
		grpOptions.setLayout(new GridLayout(3, false));

		// for lines to skip
		btnEnableRowSkipping = new Button(grpOptions, SWT.CHECK);
		btnEnableRowSkipping.setText("Skip the ");
		btnEnableRowSkipping.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateRowToSkipOption();
			}
		});

		rowCountToSkip = new Text(grpOptions, SWT.BORDER);
		rowCountToSkip.addModifyListener(e -> updateRowToSkipOption());

		GridData gdRowCountToSkip = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdRowCountToSkip.widthHint = 59;
		rowCountToSkip.setLayoutData(gdRowCountToSkip);

		Label lblFirstRows = new Label(grpOptions, SWT.NONE);
		lblFirstRows.setText("first row(s)");

		Label decimalPointChoiceLabel = new Label(grpOptions, SWT.NULL);
		decimalPointChoiceLabel.setText("Decimal point : ");
		decimalPointText = new Text(grpOptions, SWT.READ_ONLY);
		GridDataFactory.fillDefaults().applyTo(decimalPointText);

		// CSV Composite
		csvComposite = new CsvComposite(container1, file, this::updateStatus, editable);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(csvComposite);
		configureWithFileParameters();
		update();

	}

	/** Initialize by Defaults values from Preferences */
	protected void configureWithFileParameters() {

		rowCountToSkip.setText(Integer.toString(file.getStartingLine()));

		boolean found = false;
		for (int i = 0; i < PREDIFINED_DELIMITERS.length - 1; i++) {
			if (PREDIFINED_DELIMITERS[i] == file.getDelimiter()) {
				delimiter = file.getDelimiter();
				delimiterButtons[i].setSelection(true);
				otherDelimiter.setEnabled(false);
				csvComposite.setDelimiter(PREDIFINED_DELIMITERS[i]);
				found = true;
			} else {
				delimiterButtons[i].setSelection(false);
			}

		}
		if (!found) {
			delimiterButtons[PREDIFINED_DELIMITERS.length - 1].setSelection(true);
			csvComposite.setDelimiter(file.getDelimiter());
			otherDelimiter.setText(String.valueOf(file.getDelimiter()));
		}

		if (file.isDepthPositiveBelowSurface()) {
			positiveDepthButton.setSelection(true);
			negativeDepthButton.setSelection(false);
			depthPositiveBelowSurface = true;
		} else {
			negativeDepthButton.setSelection(true);
			positiveDepthButton.setSelection(false);
			depthPositiveBelowSurface = false;
		}
		// Set columns names by default
		csvComposite.setHeaderIndexMap(file.getHeaderIndex());

		if (file.getStartingLine() != 0) {
			btnEnableRowSkipping.setSelection(true);
			updateRowToSkipOption();
		}

		// Set decimal point by default
		decimalPoint = file.getDecimalPoint();
		updateDecimalPointText();
		csvComposite.setDecimalPoint(decimalPoint);
	}

	private void update() {
		csvComposite.updateTable();
		decimalPoint = csvComposite.getDecimalPoint();
		updateDecimalPointText();
	}

	public char getDelimiter() {
		return delimiter;
	}

	/**
	 * returns sign of depths
	 *
	 * @return true if depths are positive below surface, false else
	 */
	public boolean isDepthPositiveBelowSurface() {
		return depthPositiveBelowSurface;
	}

	public int getStartingLine() {
		return csvComposite.getLineToSkipCount();
	}

	public Map<String, Integer> getHeaderIndex() {
		return csvComposite.getHeaderIndex();
	}

	public IntList getValueIndexes() {
		return csvComposite.getValueIndexes();
	}

	public Character getDecimalPointLabel() {
		return decimalPoint;
	}

	/**
	 * Update the "line to skip" option
	 */
	private void updateRowToSkipOption() {
		if (btnEnableRowSkipping.getSelection() && NumberUtils.isNumber(rowCountToSkip.getText())
				&& Integer.parseInt(rowCountToSkip.getText()) > 0) {
			csvComposite.setLineToSkip(Integer.parseInt(rowCountToSkip.getText()));
		} else {
			csvComposite.setLineToSkip(0);
		}
		update();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		int delimiterIndex = 0;
		while (delimiterIndex < PREDIFINED_DELIMITERS.length
				&& PREDIFINED_DELIMITERS[delimiterIndex] != file.getDelimiter()) {
			delimiterIndex++;
		}
		StringBuilder sb = new StringBuilder();
		sb //
				.append("Delimiter : ").append(PREDIFINED_DELIMITERS_LABELS[delimiterIndex]).append("\n")//
				.append("Decimal point : \"").append(file.getDecimalPoint()).append("\"\n")//
				.append("Starting line : ").append(file.getStartingLine()).append("\n")//
				.append("Depth positive below surface : ").append(file.isDepthPositiveBelowSurface()).append("\n")//
				.append("Columns : \n");
		for (var entry : file.getHeaderIndex().entrySet()) {
			sb.append("\t").append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
		}
		return sb.toString();
	}

	@Override
	public void setVisible(boolean visible) {

		if (visible && !getControl().isVisible()) {
			// Recall configuration when ever input file has changed
			configureWithFileParameters();
		}
		if (visible) {
			update();
		}
		super.setVisible(visible);
	}
}
