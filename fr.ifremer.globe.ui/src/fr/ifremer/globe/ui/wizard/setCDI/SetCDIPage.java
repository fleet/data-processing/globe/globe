package fr.ifremer.globe.ui.wizard.setCDI;

import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.dtm.cdi.CDIValue;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.widget.cdi.CDIKeyValueWidget;
import fr.ifremer.globe.ui.widget.cdi.CDIWidgetParameters;

public class SetCDIPage extends WizardPage implements ISummarizableWizardPage {

	private CDIWidgetParameters param;

	public SetCDIPage(CDIWidgetParameters param) {
		super("Set Identifier values");
		this.param = param;
	}

	private Composite container;
	private CDIKeyValueWidget widget;

	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.NULL);
		container.setLayout(new FillLayout());
		param.setCDIEditable(true);
		widget = new CDIKeyValueWidget(container, SWT.NONE, param);

		widget.populate();
		if (param.getFiles() != null) {
			param.getFiles().addChangeListener(event -> {
				param.linkCdiToFiles();
				widget.populate();
			});
		}

		setControl(container);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < param.getFiles().size(); i++) {
			String file = param.getFiles().get(i).getName();
			String cdi = param.getCdis().get(i);
			if (cdi != null && !cdi.isBlank()) {
				sb.append("\n\tFile : ").append(file).append("\t set to : ").append(cdi);
			}
		}
		return sb.length() > 0 ? "CDI modifications :" + sb.toString() : null;
	}

	public void setFocus() {
		widget.setFocus();
	}

	public List<CDIValue> getCDIValues() {
		return widget.getValues();
	}

}
