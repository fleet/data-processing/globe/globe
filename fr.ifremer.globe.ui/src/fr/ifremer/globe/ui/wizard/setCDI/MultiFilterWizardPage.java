package fr.ifremer.globe.ui.wizard.setCDI;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.model.dtm.filter.FilterDtm;
import fr.ifremer.globe.core.model.dtm.filter.FilteringOperator;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.widget.cdi.FilterDtmUI;

public class MultiFilterWizardPage extends WizardPage implements ISummarizableWizardPage {
	private static final String ALL_CONDITIONS_ARE_TRUE = "all conditions are true";
	private static final String AT_LEAST_ONE_CONDITION_IS_TRUE = "at least one condition is true";

	private Composite filtersComposite;
	private Composite addFilter;

	int index = 0;
	private String[] cdis;
	private String[] layers;
	List<FilterDtmUI> filterComposites = new ArrayList<>();

	/** Context for bindings */
	private WritableList<String> writableCdis;
	private WritableList<String> writableLayers;
	private WritableList<String> filters;
	private WritableObject<FilteringOperator> operator;
	private ModifyListener filterModifyListener = event -> filterChanged();
	private ScrolledComposite scrolledComposite;

	/**
	 * Constructor
	 * 
	 * @wbp.parser.constructor
	 */
	private MultiFilterWizardPage() {
		super("Filter Page");
		filterComposites = new ArrayList<>();
		setTitle("Set Filters");
		setMessage("Select Filters for resetting cell (selected cell will be reset)");
	}

	/**
	 * Constructor
	 */
	public MultiFilterWizardPage(WritableList<String> writableCdis, WritableList<String> writableLayers,
			WritableList<String> filters, WritableObject<FilteringOperator> operator) {
		this();

		this.writableCdis = writableCdis;
		this.writableLayers = writableLayers;
		this.filters = filters;
		this.operator = operator;

		this.cdis = writableCdis.toArray(new String[writableCdis.size()]);
		this.layers = writableLayers.toArray(new String[writableLayers.size()]);

		writableCdis.addChangeListener(event -> resetFilter());
		writableLayers.addChangeListener(event -> resetFilter());
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		setControl(container);
		GridLayout glContainer = new GridLayout(1, false);
		glContainer.verticalSpacing = 0;
		glContainer.marginWidth = 0;
		glContainer.marginHeight = 0;
		container.setLayout(glContainer);

		Group andOrGroup = new Group(container, SWT.NONE);
		GridLayout glAndOrGroup = new GridLayout(3, false);
		glAndOrGroup.marginHeight = 0;
		glAndOrGroup.marginWidth = 0;
		andOrGroup.setLayout(glAndOrGroup);
		GridData gdAndOrGroup = new GridData(GridData.FILL_HORIZONTAL);
		gdAndOrGroup.verticalAlignment = SWT.TOP;
		andOrGroup.setLayoutData(gdAndOrGroup);

		Label lblResetIf = new Label(andOrGroup, SWT.NONE);
		lblResetIf.setText("Reset if :");

		Button btnAtLeastOne = new Button(andOrGroup, SWT.RADIO);
		btnAtLeastOne.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				operator.set(btnAtLeastOne.getSelection() ? FilteringOperator.OR : FilteringOperator.AND);
			}
		});
		btnAtLeastOne.setText(AT_LEAST_ONE_CONDITION_IS_TRUE);

		Button btnAllConditions = new Button(andOrGroup, SWT.RADIO);
		btnAllConditions.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				operator.set(btnAllConditions.getSelection() ? FilteringOperator.AND : FilteringOperator.OR);
			}
		});
		btnAllConditions.setText(ALL_CONDITIONS_ARE_TRUE);

		scrolledComposite = new ScrolledComposite(container, SWT.V_SCROLL | SWT.BORDER);
		scrolledComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		scrolledComposite.setAlwaysShowScrollBars(true);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		filtersComposite = new Composite(scrolledComposite, SWT.NONE);
		filtersComposite.setLayout(new GridLayout(1, false));
		filtersComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		scrolledComposite.setContent(filtersComposite);

		// Add filter
		addFilter = new Composite(filtersComposite, SWT.NONE);
		addFilter.setLayout(new GridLayout(1, false));
		addFilter.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true, false, 1, 1));
		Button btnNewButton = new Button(addFilter, SWT.NONE);
		btnNewButton.addListener(SWT.Selection, event -> addOneFilter(null));
		btnNewButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, true, false, 0, 0));
		btnNewButton.setText("+ Add Filter");

		// update scroll pane when filter is removed
		container.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				super.controlResized(e);
				scrolledComposite.setMinSize(filtersComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}
		});

		if (filters != null)
			new ArrayList<>(filters).forEach(this::addOneFilter);

		btnAtLeastOne.setSelection(operator.get() == FilteringOperator.OR);
		btnAllConditions.setSelection(operator.get() != FilteringOperator.OR);

		container.pack();
		setPageComplete(true);
	}

	private void addOneFilter(String filterAsString) {
		FilterDtmUI filter = new FilterDtmUI(filtersComposite, cdis, layers, filterAsString);
		filter.addModifyListener(filterModifyListener);
		filterComposites.add(filter);
		filterChanged();
		// move add button, resize and scroll
		addFilter.moveBelow(filter);
		filtersComposite.layout(); // does not trigger ControlListener
		scrolledComposite.setMinSize(filtersComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		scrolledComposite.setOrigin(0, scrolledComposite.getBounds().height);
	}

	public List<FilterDtm> getFilters() {
		return filterComposites.stream()//
				.filter(filterUI -> !filterUI.isDisposed())//
				.map(FilterDtmUI::getFilter)//
				.toList();
	}

	private void resetFilter() {
		for (FilterDtmUI filterUI : filterComposites) {
			if (!filterUI.isDisposed()) {
				filterUI.removeModifyListener(filterModifyListener);
				filterUI.setVisible(false);
				filterUI.dispose();
			}
		}
		filterComposites.clear();
		filtersComposite.layout();

		this.cdis = writableCdis.toArray(new String[writableCdis.size()]);
		this.layers = writableLayers.toArray(new String[writableLayers.size()]);

		filterChanged();
	}

	private void filterChanged() {
		if (filters != null) {
			filters.clear();
			filters.addAll(getFilters().stream().map(FilterDtm::toString).toList());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		StringBuilder sb = new StringBuilder();

		List<FilterDtm> filterDtms = getFilters();
		if (!filterDtms.isEmpty()) {
			sb.append("Filters :");
			if (operator.get() == FilteringOperator.OR)
				sb.append("\n\tReset cells if ").append(AT_LEAST_ONE_CONDITION_IS_TRUE);
			else
				sb.append("\n\tReset cells if '").append(ALL_CONDITIONS_ARE_TRUE);

			for (FilterDtm filterDtm : filterDtms) {
				sb//
						.append("\n\tReset cells of '").append(filterDtm.resetLayerName()).append("'")//
						.append(" when '").append(filterDtm.filterLayerName()).append("' is");
				switch (filterDtm.type()) {
				case equal -> sb.append(" = ");
				case not_equal -> sb.append(" \u2260 ");
				case lessThan -> sb.append(" \u2264 ");
				case moreThan -> sb.append(" \u2265 ");
				case between -> sb.append(" between ");
				}

				if (filterDtm.valueCDI().isPresent())
					sb.append(filterDtm.valueCDI().get());
				else if (filterDtm.valueA().isPresent()) {
					sb.append(filterDtm.valueA().getAsDouble());
					if (filterDtm.valueB().isPresent())
						sb.append(" and ").append(filterDtm.valueB().getAsDouble());
				}
			}
		}
		return sb.toString();
	}
}
