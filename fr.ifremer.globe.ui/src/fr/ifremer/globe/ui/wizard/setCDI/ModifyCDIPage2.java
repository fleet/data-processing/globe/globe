package fr.ifremer.globe.ui.wizard.setCDI;

import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.dtm.cdi.CDIValue;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.widget.cdi.CDIKeyValueWidget;
import fr.ifremer.globe.ui.widget.cdi.CDIWidgetParameters;

public class ModifyCDIPage2 extends WizardPage implements ISummarizableWizardPage {

	private CDIWidgetParameters param;

	public ModifyCDIPage2(CDIWidgetParameters param) {
		super("Modify Identifier values");
		setDescription("Modify the Identifier values in dtm files");
		this.param = param;
	}

	private CDIKeyValueWidget widget;
	private Composite container;

	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.NULL);
		container.setLayout(new FillLayout());
		param.setCDIEditable(true);
		widget = new CDIKeyValueWidget(container, SWT.NONE, param);
		widget.populate();
		if (param.getOriginalCdis() != null) {
			param.getOriginalCdis().addChangeListener(event -> {
				param.linkCdiFromOriginals();
				widget.populate();
			});
		}

		setControl(container);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		StringBuilder sb = new StringBuilder("CDI modifications :");
		for (int i = 0; i < param.getOriginalCdis().size(); i++) {
			String oldCdi = param.getOriginalCdis().get(i);
			String newCdi = param.getCdis().get(i);
			if (!oldCdi.equals(newCdi)) {
				sb.append("\n\tFrom : ").append(oldCdi).append("\tto : ").append(newCdi);
			}
		}
		return sb.toString();
	}

	public List<CDIValue> getCDIValues() {
		return widget.getValues();
	}

}
