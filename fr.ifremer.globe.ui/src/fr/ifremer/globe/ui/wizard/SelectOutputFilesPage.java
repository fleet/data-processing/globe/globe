package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.utils.FileUtils;

/**
 * 
 * Wizard page to configure multi output file names for a process
 * 
 * 
 * @author Gael Quemener <Gael.Quemener@ifremer.fr>
 * 
 * @deprecated use {@link SelectOutputParametersPage} 
 * 
 */
public class SelectOutputFilesPage extends WizardPage {

	protected List<String> inputFiles = new ArrayList<String>();

	protected List<String> outputFiles = new ArrayList<String>();

	private Composite container1;
	private Group group1;
	private Group group2;
	private Label[] outputFileLabel;
	private Text[] outputFileText;
	private Button[] outputFileButton;
	private SelectionListener[] outputFileListener;
	private ModifyListener[] outputFileTextListener;

	private String prefixString;
	private Label prefixLabel;
	private Text prefixText;

	private String suffixString;
	private Label suffixLabel;
	private Text suffixText;
	private ModifyListener prefixAndSuffixListener;

	private Label outputDirectoryLabel;
	private Text outputDirectoryText;
	private Button outputDirectoryButton;
	private SelectionListener outputDirectoryListener;

	private String[] outputFileStrings;

	private static int index;
	private static int nbOfInputFiles;
	private String outputFileExtension;

	private int windowsIndex;
	private int unixIndex;

	private FileDialog fileDialog;

	/**
	 * Constructor
	 * 
	 * @param inputF
	 *            list of input files (absolute paths)
	 * @param outputF
	 *            list of output files
	 * @param title
	 *            title of the page
	 * @param description
	 *            description of the page
	 * @param ofe
	 *            extension of output files
	 * @param initPrefix
	 *            initial value of prefix to add to output files
	 * @param initSuffix
	 *            initial value of suffix to add to output files
	 */

	public SelectOutputFilesPage(List<String> inputF, List<String> outputF, String title, String description, String ofe, String initPrefix, String initSuffix, FileDialog fileDialog) {
		super("");
		this.fileDialog = fileDialog;
		initialize(inputF, outputF, title, description, ofe, initPrefix, initSuffix);
	}

	public SelectOutputFilesPage(List<String> inputF, List<String> outputF, String title, String description, String ofe, String initPrefix, String initSuffix) {
		super("");
		initialize(inputF, outputF, title, description, ofe, initPrefix, initSuffix);
	}

	public void initialize(List<String> inputF, List<String> outputF, String title, String description, String ofe, String initPrefix, String initSuffix) {
		setTitle(title);
		setDescription(description);
		this.suffixString = initSuffix;
		this.prefixString = initPrefix;
		inputFiles = inputF;
		outputFiles = outputF;
		outputFileExtension = ofe;
		nbOfInputFiles = inputFiles.size();
		outputFileListener = new SelectionAdapter[nbOfInputFiles];
		outputFileTextListener = new ModifyListener[nbOfInputFiles];
		outputFileStrings = new String[nbOfInputFiles];

		for (int i = 0; i < nbOfInputFiles; i++) {

			index = i;
			int WindowsIndex = inputFiles.get(index).lastIndexOf("\\");
			int unixIndex = inputFiles.get(index).lastIndexOf("/");

			outputFileStrings[index] = inputFiles.get(index).substring(Math.max(WindowsIndex, unixIndex), inputFiles.get(index).lastIndexOf("."));

			// listeners called when the user changes one output file
			outputFileListener[i] = new SelectionAdapter() {
				private int listenerIndex = index;

				@Override
				public void widgetSelected(SelectionEvent e) {
					if(fileDialog == null) {
						Shell shell = Display.getCurrent().getActiveShell();
						fileDialog = new FileDialog(shell, SWT.SAVE);
						fileDialog.setText("Choose output DTM file");
						fileDialog.setFilterExtensions(new String[] { "*" + outputFileExtension });
						String type  = outputFileExtension.substring(1) + " files (*" + outputFileExtension + ")";
						fileDialog.setFilterNames(new String[] { type });
					}
					String file = outputFiles.get(listenerIndex);
					String path = new File(file).getParent();

					fileDialog.setFilterPath(path);
					fileDialog.setFileName(FileUtils.getFileShortName(file));

					String fileName = fileDialog.open();
					if (fileName != null) {
						outputFileText[listenerIndex].setText(fileName);
					}

					updateOutputFiles();
				}
			};

			// listener called when the user change the output file name in the
			// Text widget
			outputFileTextListener[i] = new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent e) {
					updateOutputFiles();

				}
			};

		}

		// listener called when the user change the output directory with the
		// File Dialog button
		outputDirectoryListener = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Shell shell = Display.getCurrent().getActiveShell();
				DirectoryDialog fileDialog = new DirectoryDialog(shell);
				fileDialog.setText("Choose output directory");

				String file = SelectOutputFilesPage.this.inputFiles.get(0);
				String path = new File(file).getParent();
				file.split("[.]");
				fileDialog.setFilterPath(path);

				// when directory is changed
				// refresh all the output files Text
				int WindowsIndex = outputFileText[0].getText().lastIndexOf("\\");
				int unixIndex = outputFileText[0].getText().lastIndexOf("/");
				fileDialog.setText(inputFiles.get(0).substring(0, Math.max(WindowsIndex, unixIndex)));

				String fileName = fileDialog.open();
				if (fileName != null) {
					outputDirectoryText.setText(fileName);
					for (int i = 0; i < nbOfInputFiles; i++) {
						outputFileText[i].setText(fileName + outputFileText[i].getText().substring(Math.max(WindowsIndex, unixIndex)));
					}
				}
				updateOutputFiles();
			}

		};

		// listener called when the user change the suffix or the prefix
		prefixAndSuffixListener = new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				// when prefix or suffix is changed
				// refresh all the output files Text
				for (int i = 0; i < nbOfInputFiles; i++) {
					int WindowsIndex = outputFileStrings[index].lastIndexOf("\\");
					int unixIndex = outputFileStrings[index].lastIndexOf("/");
					outputFileText[i].setText(outputDirectoryText.getText() + outputFileStrings[i].substring(0, Math.max(unixIndex, WindowsIndex) + 1) + prefixText.getText()
					+ outputFileStrings[i].substring(Math.max(unixIndex, WindowsIndex) + 1) + suffixText.getText() + outputFileExtension);
				}
				updateOutputFiles();
			}
		};

	}

	@Override
	public void createControl(Composite parent) {
		container1 = new Composite(parent, SWT.NULL);
		GridLayout gl_container = new GridLayout(1, false);
		gl_container.verticalSpacing = 10;
		container1.setLayout(gl_container);

		setControl(container1);

		// one group is created for:
		// - output directory
		// - prefix
		// - suffix
		group1 = new Group(container1, SWT.NONE);
		group1.setText("Global parameters");
		group1.setLayout(new GridLayout(3, false));
		group1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		outputDirectoryLabel = new Label(group1, SWT.NONE);
		outputDirectoryLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		outputDirectoryLabel.setText("Output directory");

		// Text to edit the output directory
		outputDirectoryText = new Text(group1, SWT.BORDER);
		outputDirectoryText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		windowsIndex = inputFiles.get(0).lastIndexOf("\\");
		unixIndex = inputFiles.get(0).lastIndexOf("/");
		outputDirectoryText.setText(inputFiles.get(0).substring(0, Math.max(windowsIndex, unixIndex)));

		// button connected to a File Dialog to edit the output directory
		outputDirectoryButton = new Button(group1, SWT.NONE);
		outputDirectoryButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		outputDirectoryButton.addSelectionListener(outputDirectoryListener);
		outputDirectoryButton.setText("...");

		// Text widget to edit the prefix
		prefixLabel = new Label(group1, SWT.NONE);
		prefixLabel.setLayoutData(new GridData(SWT.LEFT, SWT.LEFT, false, false, 1, 1));
		prefixLabel.setText("Prefix");

		prefixText = new Text(group1, SWT.NONE);
		prefixText.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, false, false, 1, 1));
		prefixText.setText(prefixString);
		prefixText.addModifyListener(prefixAndSuffixListener);

		// fill the gap on the third column
		new Label(group1, SWT.NONE);

		// Text widget to edit the suffix
		suffixLabel = new Label(group1, SWT.NONE);
		suffixLabel.setLayoutData(new GridData(SWT.LEFT, SWT.LEFT, false, false, 1, 1));
		suffixLabel.setText("Suffix");

		suffixText = new Text(group1, SWT.NONE);
		suffixText.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, false, false, 1, 1));
		suffixText.setText(suffixString);
		suffixText.addModifyListener(prefixAndSuffixListener);

		// fill the gap on the third column
		new Label(group1, SWT.NONE);

		// group for editing the list of output files
		group2 = new Group(container1, SWT.NONE);
		group2.setText("Output files");
		group2.setLayout(new GridLayout(3, false));
		group2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		outputFileLabel = new Label[nbOfInputFiles];
		outputFileText = new Text[nbOfInputFiles];
		outputFileButton = new Button[nbOfInputFiles];

		for (int i = 0; i < nbOfInputFiles; i++) {
			// simple label
			outputFileLabel[i] = new Label(group2, SWT.NONE);			
			outputFileLabel[i].setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

			outputFileText[i] = new Text(group2, SWT.BORDER);
			outputFileText[i].setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			outputFileText[i].addModifyListener(outputFileTextListener[i]);

			outputFileButton[i] = new Button(group2, SWT.NONE);
			outputFileButton[i].setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			outputFileButton[i].addSelectionListener(outputFileListener[i]);

		}

		updateOutputFilesWidgets();

		updateOutputFiles();

	}

	/**
	 * Update the content of SWT components relatives to output file path.
	 */
	public void updateOutputFilesWidgets() {
		for (int i = 0; i < nbOfInputFiles; i++) {
			outputFileLabel[i].setText("Output file " + (i + 1));

			// Text widget to edit the output file name
			windowsIndex = outputFileStrings[i].lastIndexOf("\\");
			unixIndex = outputFileStrings[i].lastIndexOf("/");
			outputFileText[i].setText(outputDirectoryText.getText() + outputFileStrings[i].substring(0, Math.max(unixIndex, windowsIndex) + 1) + prefixText.getText()
			+ outputFileStrings[i].substring(Math.max(unixIndex, windowsIndex) + 1) + suffixText.getText() + outputFileExtension);

			// Button connected to a File Dialog to edit the output file name
			outputFileButton[i].setText("...");
		}
	}

	@Override
	protected void initializeDialogUnits(Control testControl) {

	}

	protected void updateOutputFiles() {
		outputFiles = new ArrayList<String>();
		for (int i = 0; i < nbOfInputFiles; i++) {
			outputFiles.add(outputFileText[i].getText());
		}
	}

	public List<String> getOutputFiles() {
		return outputFiles;
	}

	public void setOutputFiles(List<String> outputFiles) {
		this.outputFiles = outputFiles;
	}

	/**
	 * @param outputFileExtension the outputFileExtension to set
	 */
	public void setOutputFileExtension(String outputFileExtension) {
		this.outputFileExtension = "." + outputFileExtension;
	}

}
