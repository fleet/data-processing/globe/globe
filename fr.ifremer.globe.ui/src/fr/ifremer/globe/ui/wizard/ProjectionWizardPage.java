package fr.ifremer.globe.ui.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.projection.ProjectionDialog;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;

/**
 * Wizard page to select a projection
 */
public class ProjectionWizardPage extends WizardPage implements ISummarizableWizardPage {

	private final IProjectionWizardPageModel projectionWizardPageModel;

	private ProjectionDialog projectionDialog;

	/** Constructor */
	public ProjectionWizardPage(String title, IProjectionWizardPageModel projectionWizardPageModel) {
		super(ProjectionWizardPage.class.getSimpleName(), title, null);
		this.projectionWizardPageModel = projectionWizardPageModel;
	}

	/** {@inheritDoc} */
	@Override
	public void createControl(Composite parent) {

		Composite container = new Composite(parent, SWT.NULL);
		GridLayout glContainer = new GridLayout(2, false);
		glContainer.marginHeight = 10;
		glContainer.marginWidth = 10;
		glContainer.verticalSpacing = 10;
		glContainer.horizontalSpacing = 10;
		container.setLayout(glContainer);

		Composite projectionComposite = new Composite(container, SWT.NONE);
		projectionComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		projectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		projectionDialog = new ProjectionDialog(projectionComposite);

		// Inform ProjectionDialog when geobox changed
		var geobox = projectionWizardPageModel.getGeobox();
		if (geobox != null) {
			projectionDialog.setGeobox(geobox.get());
			geobox.addChangeListener(e -> projectionDialog.setGeobox(geobox.get()));
		}
		// Map edited settings to model
		projectionDialog.addPropertyChangeListener(evt -> projectionChanged());
		projectionDialog.setProjectionSettings(projectionWizardPageModel.getProjectionSettings().get());

		setPageComplete(projectionWizardPageModel.getProjectionSettings().isNotNull());

		setControl(container);
	}

	/** User changed the projection */
	private void projectionChanged() {
		var projectionSettings = projectionDialog.getProjectionSettings();
		if (projectionSettings != null) {
			String message = projectionDialog.validateProj4Chain();
			if (message != null) {
				setErrorMessage(message);
				setPageComplete(false);
			} else {
				projectionWizardPageModel.getProjectionSettings().set(projectionSettings);
				setErrorMessage(null);
				setPageComplete(true);
				setMessage(projectionDialog.getWarning(), WARNING);
			}
		} else {
			setErrorMessage(null);
			setPageComplete(false);
		}
	}

	/** Model of this page */
	public interface IProjectionWizardPageModel {
		/** Selected projection settings */
		WritableObject<ProjectionSettings> getProjectionSettings();

		/** Geobox used to guess some projection settings (latitude of true scale, UTM zone... */
		WritableObject<GeoBox> getGeobox();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String summarize() {
		String result = "";
		if (projectionWizardPageModel.getProjectionSettings().isNotNull()) {
			result = "Projection : "
					+ projectionWizardPageModel.getProjectionSettings().get().proj4String();
		}
		return result;
	}

	public IProjectionWizardPageModel getModel() {
		return projectionWizardPageModel;
	}

}
