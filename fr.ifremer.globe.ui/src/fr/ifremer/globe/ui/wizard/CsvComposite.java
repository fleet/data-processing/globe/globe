package fr.ifremer.globe.ui.wizard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.io.input.BOMInputStream;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.utils.csv.CsvUtils;

/**
 * Composite which displays a table with csv file content and a warning label about it.
 */
public class CsvComposite extends Composite {

	protected final Logger logger = LoggerFactory.getLogger(CsvComposite.class);

	/** true to allow column names edition */
	private boolean editable = false;
	private Table table;
	private Label warningLabel;

	private final IxyzFile xyzFile;
	/** Mandatory headers. These headers must have an assigned index. */
	private final Set<String> mandatoryHeaders;
	/** Optional headers. These headers may have an assigned index. */
	private final Set<String> optionalHeaders;
	private final Runnable completionCallback;

	private char delimiter = ';';
	private char decimalPoint = '.';
	private int defaultColumnCount = 100;

	private boolean isDelimiterValid;
	private boolean isHeaderValid;
	private boolean isDecimalPointValid = true;
	private boolean isMaxColumnCountReached = false;

	private static final String EMPTY_HEADER = "- ? -";
	private static final String VALUE_HEADER = "- Value -";

	/** All columns in the table with their index (first column has index 1 because of the dummy column). */
	private Map<TableColumn, Integer> tableColumnIndexMap = new HashMap<>();

	/** Map a column index to a column name selected by the operator */
	private Map<Integer, String> headerIndexMap = new HashMap<>();
	private int lineToSkipCount = 0;

	/**
	 * Contructor of {@link CsvComposite}.
	 *
	 * @param parent parent composite of {@link CsvComposite}
	 * @param xyzFile csv file to display in the {@link CsvComposite} table
	 * @param editable true to allow column names edition
	 * @param mandatoryHeaders mandatory header required by the csv file
	 */
	public CsvComposite(Composite parent, IxyzFile xyzFile, Runnable completionCallback, boolean editable) {
		super(parent, SWT.NONE);

		mandatoryHeaders = Set.copyOf(xyzFile.getMandatoryHeader());
		optionalHeaders = new HashSet<>(xyzFile.getOptionalHeader());
		this.xyzFile = xyzFile;
		this.editable = editable;
		this.completionCallback = completionCallback;

		initialize();
	}

	/**
	 * Initialize the {@link CsvComposite}.
	 */
	private void initialize() {

		GridLayout layout = new GridLayout();
		layout.marginWidth = 0;
		setLayout(layout);

		// Delimiters
		Group tableGroup = new Group(this, SWT.NONE);
		tableGroup.setText("Visualization of file first lines");
		tableGroup.setLayout(new GridLayout(1, false));
		tableGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		var buttonComposite = new Composite(tableGroup, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(buttonComposite);
		GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(true).applyTo(buttonComposite);

		Button refreshButton = new Button(buttonComposite, SWT.NONE);
		refreshButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> updateTable()));
		refreshButton.setText("Refresh");
		refreshButton.setToolTipText("Re-parsing the file");
		GridDataFactory.fillDefaults().applyTo(refreshButton);

		Button resetColumnsNames = new Button(buttonComposite, SWT.NONE);
		resetColumnsNames.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> setResetHeaderIndexMap()));
		resetColumnsNames.setText("Reset Columns Names");
		resetColumnsNames.setToolTipText("Reset column's selection");
		GridDataFactory.fillDefaults().applyTo(resetColumnsNames);

		if (editable) {
			Button editColumnsNames = new Button(buttonComposite, SWT.NONE);
			editColumnsNames.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> editHeaders()));
			editColumnsNames.setText("Edit Columns Names");
			editColumnsNames.setToolTipText("Modify column names and type");
			GridDataFactory.fillDefaults().applyTo(editColumnsNames);
		}

		table = new Table(tableGroup, SWT.H_SCROLL);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableViewer tableViewer = new TableViewer(table);

		// Add a dummy column for avoid selection bug in SWT table
		TableColumn dummyColumn = new TableViewerColumn(tableViewer, SWT.NONE).getColumn();
		dummyColumn.setText(EMPTY_HEADER);
		dummyColumn.setWidth(0);
		dummyColumn.setResizable(false);
		tableColumnIndexMap.put(dummyColumn, -1);

		// Add column header
		for (int tableIndex = 1; tableIndex < defaultColumnCount; tableIndex++) {
			TableViewerColumn columnViewer = new TableViewerColumn(tableViewer, SWT.NONE);
			TableColumn tableColumn = columnViewer.getColumn();
			tableColumn.setText(EMPTY_HEADER);
			tableColumnIndexMap.put(tableColumn, tableIndex);
			tableColumn.addSelectionListener(
					SelectionListener.widgetSelectedAdapter(e -> showHeaderPopupMenu((TableColumn) e.widget)));
		}

		// Add label
		warningLabel = new Label(tableGroup, SWT.LEFT);
		warningLabel.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1));

		Display display = Display.getCurrent();
		warningLabel.setForeground(new Color(display, 250, 50, 50));
		FontData fontData = warningLabel.getFont().getFontData()[0];
		Font font = new Font(display, new FontData(fontData.getName(), fontData.getHeight(), SWT.ITALIC));
		warningLabel.setFont(font);
		warningLabel.addDisposeListener(e -> font.dispose());

		// Update table
		updateTable();

		// Ensure table is not too large
		Point bestSize = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		bestSize.x = Math.min(bestSize.x, 600);
		GridDataFactory.fillDefaults().grab(true, true).hint(bestSize).minSize(bestSize).applyTo(table);
	}

	/**
	 * Update the table of the {@link CsvComposite}.
	 */
	public void updateTable() {
		// Remove all datas
		table.removeAll();

		// Read ten first line
		List<String[]> firstLines = parseFirstLines();

		boolean firstLine = true;
		isDelimiterValid = true;
		// Add a table item for each line
		int maxCol = 1;
		for (String[] fields : firstLines) {
			TableItem item = new TableItem(table, SWT.NONE);

			maxCol = Math.max(maxCol, fields.length);

			for (int i = 0; i < fields.length; i++) {
				item.setText(i + 1, fields[i]);
			}

			if (firstLine) {
				firstLine = false;

				// If the second field is empty, it's because delimiter is wrong
				if (fields.length < 2) {
					item.setBackground(new Color(Display.getCurrent(), 255, 0, 0));
					isDelimiterValid = false;
				}

				// Add specific color for header
				item.setBackground(new Color(Display.getCurrent(), 150, 170, 150));
			}

			if (!isDelimiterValid) {
				item.setBackground(new Color(Display.getCurrent(), 255, 0, 0));
			}
		}
		table.setRedraw(false);
		
		isMaxColumnCountReached = false;
		if(maxCol > defaultColumnCount) {
			isMaxColumnCountReached = true;
			maxCol = defaultColumnCount;
		}
		
		for (Entry<TableColumn, Integer> entry : tableColumnIndexMap.entrySet()) {
			if (entry.getValue() > maxCol || entry.getValue() < 0) {
				entry.getKey().setWidth(0);
			} else {
				entry.getKey().pack();
			}
		}

		// Checking of decimal point
		isDecimalPointValid = true;
		if (!firstLines.isEmpty()) {
			// Check values of mandatory and optional columns
			isHeaderValid = getMissingHeaders().isEmpty();
			for (String columnName : getAllHeaders()) {
				int columnIndex = getHeaderIndex().getOrDefault(columnName, -1);
				if (columnIndex < 0) {
					// Not set yet
					continue;
				}
				checkDecimalPointInColumn(firstLines, columnName, columnIndex);
			}
			// Check values of value columns
			for (var iter = getValueIndexes().iterator(); iter.hasNext();) {
				checkDecimalPointInColumn(firstLines, VALUE_HEADER, iter.next());
			}
		}

		table.setRedraw(true);
		updateWarningLabelText();
		updatePageCompletion();

	}

	/**
	 * Browse values in of the specified column in first lines of the file and check the decimal point
	 */
	protected void checkDecimalPointInColumn(List<String[]> firstLines, String columnName, int columnIndex) {
		for (String[] fields : firstLines) {
			if (columnIndex < fields.length) {
				try {
					if (columnName.equals(VALUE_HEADER) || xyzFile.getType(columnName) == Double.class) {
						String token = fields[columnIndex];
						if (!token.isBlank()) {
							if (token.contains(",")) {
								token = token.replace(",", ".");
								decimalPoint = ',';
							} else if (token.contains(".")) {
								decimalPoint = '.';
							}
							Double.parseDouble(token);
						}
					}
				} catch (NumberFormatException e) {
					isDecimalPointValid = false;
					break;
				}
			} else {
				isHeaderValid = false;
			}
		}
	}

	/**
	 * Update the warning label contained by the {@link CsvComposite}.
	 */
	public void updateWarningLabelText() {
		if (warningLabel != null) {
			if (isDelimiterValid == false) {
				warningLabel.setText("Selected delimiter is invalid");
			} else if (isDecimalPointValid == false) {
				warningLabel.setText("Selected decimal point is invalid or decimal point in a field is invalid");
			} else if (isMaxColumnCountReached == true) {
				warningLabel.setText("Max number of columns reached. Columns index greather than " + defaultColumnCount + " will be truncated.");
			} else if (isHeaderValid == false) {
				warningLabel.setText("Click on table headers to select data's position");
			} else {
				warningLabel.setText("");
			}
		}
	}

	/**
	 * Initialize a menu which is displayed on a column header and allow to select the data to show.
	 *
	 * @param column column where the menu is to displayed
	 */
	protected void showHeaderPopupMenu(TableColumn column) {
		Integer columnIndex = tableColumnIndexMap.get(column);

		// Show PopupMenu
		Menu menu = new Menu(this);
		setMenu(menu);

		for (String headerText : getAvailableHeadersText()) {
			MenuItem newItem = new MenuItem(menu, SWT.NONE);
			if (EMPTY_HEADER.equals(headerText) || mandatoryHeaders.contains(headerText)) {
				newItem.setText(headerText);
			} else {
				newItem.setText(headerText + " *");
			}
			newItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
				// Unset previous column
				if (!EMPTY_HEADER.equals(headerText) && !VALUE_HEADER.equals(headerText)) {
					for (var entry : tableColumnIndexMap.entrySet()) {
						if (entry.getKey().getText().equals(headerText)) {
							entry.getKey().setText(EMPTY_HEADER);
							headerIndexMap.remove(entry.getValue());
						}
					}
				}

				// Add new text to storage map
				headerIndexMap.put(columnIndex, headerText);

				// Update header text
				column.setText(headerText);
				updateTable();

				// Headers valid?
				checkHeadersCompletion();

				// Page ok?
				updatePageCompletion();
			}));
		}

		menu.setLocation(column.getDisplay().getCursorLocation());
		menu.setVisible(true);
	}

	/**
	 * Set the {@link CsvComposite#isHeaderValid} attribute to true if no mandatory header left.
	 */
	private void checkHeadersCompletion() {
		// Header is valid if there is no mandatory header left
		isHeaderValid = getMissingHeaders().isEmpty();
	}

	/**
	 * Return a list with the mandatory header which are not selected yet.
	 */
	private Set<String> getMissingHeaders() {
		var result = new HashSet<>(mandatoryHeaders);
		result.removeAll(headerIndexMap.values());
		return result;
	}

	/**
	 * @return mandatoryHeaders + optionalHeaders
	 */
	private Set<String> getAllHeaders() {
		var result = new HashSet<>(mandatoryHeaders);
		result.addAll(optionalHeaders);
		return result;
	}

	private List<String> getAvailableHeadersText() {
		List<String> headersText = new ArrayList<>();

		// Add empty header
		headersText.add(EMPTY_HEADER);

		// Add needed headers
		headersText.addAll(getAllHeaders());
		Collections.sort(headersText);

		// Add value header
		if (xyzFile.allowsMultipleValues()) {
			headersText.add(VALUE_HEADER);
		}
		return headersText;
	}

	/**
	 * Raises an event if the table is completed and updates the warning label.
	 */
	public void updatePageCompletion() {
		getDisplay().asyncExec(completionCallback);

		// Update warning text
		updateWarningLabelText();
	}

	/**
	 * Reads the ten first lines of the csv file.
	 */
	protected List<String[]> parseFirstLines() {
		List<String[]> firstLines = new ArrayList<>();

		try {
			if (xyzFile.getPath() != null) {
				try (FileInputStream fis = new FileInputStream(new File(xyzFile.getPath()));
						BOMInputStream bis = new BOMInputStream(fis);
						InputStreamReader isr = new InputStreamReader(bis);
						BufferedReader bufferedReader = new BufferedReader(isr);) {
					// Read first 10 lines from file
					String line = bufferedReader.readLine();

					for (int i = 0; i < 9 + lineToSkipCount && line != null; i++) {
						if (delimiter == ' ') {
							line = line.trim().replaceAll(" +", " ");
						} else if (delimiter == '…') {
							line = line.trim().replaceAll("\\s+", "…");
						}

						// split line with separator
						String[] splittedLine = CsvUtils.parseLine(line, delimiter);

						// Store current line (if it's not a line to skip)
						if (i >= lineToSkipCount) {
							firstLines.add(splittedLine);
						}

						// Read next line
						line = bufferedReader.readLine();
					}
				}
			}
		} catch (Exception t) {
			logger.warn("Error while reading first lines of file {} ", xyzFile.getPath());

			// Add dummy line
			firstLines.add(new String[0]);
		}

		return firstLines;
	}

	/**
	 * @return the delimiter
	 */
	public char getDelimiter() {
		return delimiter;
	}

	/**
	 * @param delimiter the delimiter to set
	 */
	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
		isDelimiterValid = true;
		updateWarningLabelText();

	}

	/**
	 * @return the isDelimiterValid
	 */
	public boolean isDelimiterValid() {
		return isDelimiterValid;
	}

	/**
	 * @return the isHeaderValid
	 */
	public boolean isHeaderValid() {
		return isHeaderValid;
	}

	public void setDecimalPoint(char decimalPoint) {
		this.decimalPoint = decimalPoint;

	}

	public char getDecimalPoint() {
		return decimalPoint;

	}

	/**
	 * @return the isDecimalPointValid
	 */
	public boolean isDecimalPointValid() {
		return isDecimalPointValid;
	}

	/**
	 * Initialize columns names of the table
	 *
	 * @param headerIndexMap: List of default columns names Index
	 */
	public void setHeaderIndexMap(Map<String, Integer> headerIndexMap) {
		this.headerIndexMap.clear();
		Set<String> allHeaders = getAllHeaders();
		for (Entry<String, Integer> entry : headerIndexMap.entrySet()) {
			if (entry.getValue().intValue() > 0 && allHeaders.contains(entry.getKey())) {
				this.headerIndexMap.put(entry.getValue(), entry.getKey());
			}
		}
		for (Entry<TableColumn, Integer> entry : tableColumnIndexMap.entrySet()) {
			TableColumn tableColumn = entry.getKey();
			Integer indexColumn = entry.getValue();
			if (this.headerIndexMap.get(indexColumn) != null) {
				tableColumn.setText(this.headerIndexMap.get(indexColumn));
			} else {
				tableColumn.setText(EMPTY_HEADER);
			}
		}
		// update
		checkHeadersCompletion();
		updatePageCompletion();
	}

	/**
	 * Initialize columns names of the table
	 *
	 * @param headerIndexMap: List of default columns names Index
	 */
	public void setResetHeaderIndexMap() {
		headerIndexMap.clear();
		for (Entry<TableColumn, Integer> entry : tableColumnIndexMap.entrySet()) {
			if (entry.getValue() > -1) {
				TableColumn tableColumn = entry.getKey();
				tableColumn.setText(EMPTY_HEADER);
			}
		}
		updateTable();
		checkHeadersCompletion();
		updatePageCompletion();
	}

	/**
	 * create a new map containing header index
	 */
	public Map<String, Integer> getHeaderIndex() {
		Map<String, Integer> headerIndex = new HashMap<>();
		for (Entry<Integer, String> entry : headerIndexMap.entrySet()) {
			if (!EMPTY_HEADER.equals(entry.getValue()) && !VALUE_HEADER.equals(entry.getValue())) {
				headerIndex.put(entry.getValue(), entry.getKey() - 1);
			}
		}

		return headerIndex;
	}

	/**
	 * create a list of column index containing values
	 */
	public IntList getValueIndexes() {
		var result = new ArrayIntList();
		for (Entry<Integer, String> entry : headerIndexMap.entrySet()) {
			if (VALUE_HEADER.equals(entry.getValue())) {
				result.add(entry.getKey() - 1);
			}
		}
		return result;
	}

	/** lineToSkipCount setter **/
	public void setLineToSkip(int i) {
		lineToSkipCount = i;
	}

	/** lineToSkipCount getter **/
	public int getLineToSkipCount() {
		return lineToSkipCount;
	}

	/**
	 * Edit the column names
	 */
	public void editHeaders() {
		var columnNames = new ArrayList<ColumnName>();
		for (String header : optionalHeaders) {
			var columnName = new ColumnName();
			columnName.setName(header);
			columnName.setType(xyzFile.getType(header));
			columnNames.add(columnName);
		}
		// New one
		columnNames.add(new ColumnName());

		HeaderDialog dialog = new HeaderDialog(getShell(), columnNames);
		if (dialog.open() == Window.OK) {
			optionalHeaders.clear();
			for (ColumnName columnName : columnNames) {
				if (columnName.getName() != null) {
					optionalHeaders.add(columnName.getName());
					xyzFile.setType(columnName.getName(), columnName.getType());
				}
			}
		}

	}

}
