package fr.ifremer.globe.ui.wizard.pref;

import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

/** Preferences used by wizards */
public class WizardPreferences {

	/** Parent preference node */
	private final PreferenceComposite processesSettingsNode;
	/** Preferences attributes **/
	private final StringPreferenceAttribute defaultProjectionName;

	/**
	 * Constructor
	 */
	public WizardPreferences(PreferenceComposite processesSettingsNode) {
		this.processesSettingsNode = processesSettingsNode;

		defaultProjectionName = new StringPreferenceAttribute("defaultProjectionName", "Preferred projection name",
				StandardProjection.LONGLAT.name());
		processesSettingsNode.declareAttribute(defaultProjectionName);
		processesSettingsNode.load();
	}

}
