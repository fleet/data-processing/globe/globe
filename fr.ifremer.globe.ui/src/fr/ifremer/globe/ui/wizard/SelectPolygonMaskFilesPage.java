package fr.ifremer.globe.ui.wizard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;

/**
 * Wizard page to select a list of polygon files
 */
public class SelectPolygonMaskFilesPage extends SelectInputFilesPage {

	private final WritableBoolean reversePolygonMask;

	/**
	 * @param model
	 */
	public SelectPolygonMaskFilesPage(SelectInputFilesPageModel model, WritableBoolean reversePolygonMask) {
		super(model);
		this.reversePolygonMask = reversePolygonMask;
	}

	@Override
	public void createControl(Composite parent) {
		super.createControl(parent);

		setTitle("Mask file list (*.kml or *.shp)");
		setDescription("Define the area to process with files containing geographic polygons.");

		// Reverse mask available ?
		if (reversePolygonMask.isNotNull()) {
			var reverseButton = new Button((Composite) getControl(), SWT.CHECK);
			reverseButton.setSelection(reversePolygonMask.isTrue());
			reverseButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			reverseButton.setText("Process data outside of the provided polygons");
			reverseButton.addSelectionListener(
					SelectionListener.widgetSelectedAdapter(e -> reversePolygonMask.set(reverseButton.getSelection())));
		}
	}

	@Override
	public String summarize() {
		String result = super.summarize();
		if (!result.isEmpty() && reversePolygonMask.isNotNull())
			result = result + "\nProcess data outside of the provided polygons : " + reversePolygonMask.isTrue();
		return result;
	}
}
