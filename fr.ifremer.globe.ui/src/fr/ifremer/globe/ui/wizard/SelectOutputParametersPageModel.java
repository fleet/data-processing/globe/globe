package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer.OutputFileNameComputerModel;

/**
 * Model of the input file selection wizard page
 */
public interface SelectOutputParametersPageModel extends OutputFileNameComputerModel {

	/**
	 * Getter of inputFiles
	 */
	WritableObjectList<File> getInputFiles();

	/**
	 * Getter of outputFormatExtensions
	 */
	WritableObjectList<String> getOutputFormatExtensions();

	/**
	 * Getter of outputFormatExtensions
	 */
	default List<String> getOutputFormatExtensions(File file) {
		return getOutputFormatExtensions(file.getName());
	}

	/**
	 * Getter of outputFormatExtensions
	 */
	default List<String> getOutputFormatExtensions(String filename) {
		return getOutputFormatExtensions().asList();
	}

	/**
	 * Getter of outputFiles
	 */
	WritableObjectList<File> getOutputFiles();

	/**
	 * Getter of overwriteExistingFiles
	 */
	WritableBoolean getOverwriteExistingFiles();

	/**
	 * Getter of loadFilesAfter. True to load files at the end of process
	 */
	WritableBoolean getLoadFilesAfter();

	/**
	 * Getter of whereToloadFiles. Where to load the files (in witch group of project explorer)
	 */
	WritableString getWhereToloadFiles();

	/**
	 * Getter of mergeOutputFiles
	 */
	default Optional<WritableBoolean> getMergeOutputFiles() {
		return Optional.empty();

	}

	/**
	 * Getter of mergeOutputFiles
	 */
	default boolean canMergeOutputFiles() {
		boolean result = false;
		Optional<WritableBoolean> mergeOutputFiles = getMergeOutputFiles();
		if (mergeOutputFiles.isPresent()) {
			result = mergeOutputFiles.get().isTrue();
		}
		return result;
	}
}