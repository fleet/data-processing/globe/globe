package fr.ifremer.globe.ui.wizard.saveseriesascsv;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;
import fr.ifremer.globe.utils.exception.GIOException;

public class SaveWizardModel extends ConvertWizardDefaultModel {

	/** Input series **/
	private final Map<? extends IChartSeries<? extends IChartData>, AtomicBoolean> selectableChartSeries;

	/**
	 * Constructor
	 * 
	 * @throws GIOException
	 **/
	public SaveWizardModel(List<? extends IChartSeries<? extends IChartData>> inputSeries) throws GIOException {
		super();
		ContextInitializer.inject(this);
		selectableChartSeries = inputSeries.stream()
				.collect(Collectors.toMap(Function.identity(), k -> new AtomicBoolean(true)));

		List<File> inputFile = selectableChartSeries.entrySet().stream()//
				.filter(e -> e.getValue().get()) //
				.map(Entry::getKey)//
				.map(IChartSeries::getSource)//
				.distinct()//
				.map(IChartSeriesDataSource::getFilePath).filter(Optional::isPresent).map(Optional::get)
				.filter(Files::exists).map(Path::toFile).toList();
		if (inputFile.isEmpty())
			throw new GIOException("no valid input file.");

		getInputFiles().addAll(inputFile);
		getOutputFormatExtensions().clear();
		getOutputFormatExtensions().add("csv");
		getOutputDirectory().doSetValue(inputFile.get(0).getParentFile());
	}

	public Map<? extends IChartSeries<? extends IChartData>, AtomicBoolean> getSelectableCharSeries() {
		return selectableChartSeries;
	}

	/**
	 * @return the list of selected series.
	 */
	public List<IChartSeries<? extends IChartData>> getSeriesToSave() {
		return selectableChartSeries.entrySet().stream().filter(e -> e.getValue().get()).map(Entry::getKey)
				.collect(Collectors.toList());
	}

	public void selectSeries(String title, boolean selection) {
		selectableChartSeries.entrySet().stream() //
				.filter(entry -> entry.getKey().getTitle().equals(title))
				.forEach(entry -> entry.getValue().set(selection));
	}

}
