package fr.ifremer.globe.ui.wizard;

import java.io.File;

/**
 * File element to be processed by a {@link @DefaultProcessWithWizard}
 */
public class FilesToProcessEntry {

	private final File inputFile;
	private final File outputFile;
	private final File tempFile;

	/**
	 * Constructor
	 * 
	 * @param inputFile : input file to process
	 * @param outputFile : generated output file if process is successful
	 * @param tempFile : temporary file used to work with
	 */
	public FilesToProcessEntry(File inputFile, File outputFile, File tempFile) {
		super();
		this.inputFile = inputFile;
		this.outputFile = outputFile;
		this.tempFile = tempFile;
	}

	public File getInputFile() {
		return inputFile;
	}

	public File getOutputFile() {
		return outputFile;
	}

	public File getTempFile() {
		return tempFile;
	}
}