package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.ISoundVelocityService;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Default process with wizard : supply a {@link WizardDialog} and implements {@link IProcessFunction}.
 */
public abstract class DefaultProcessWithWizard<T extends SelectOutputParametersPageModel> extends GProcess {

	/** Wizard dialog **/
	protected final WizardDialog wizardDialog;

	/** Model of the input file selection wizard page */
	protected final T model;

	/** Tool to compute an output file name */
	protected OutputFileNameComputer outputFileNameComputer = new OutputFileNameComputer();

	/**
	 * Constructor from a {@link Wizard}.
	 */
	protected DefaultProcessWithWizard(Shell shell, String name, Supplier<T> modelConstructor,
			Function<T, Wizard> wizardConstructor) {
		this(name, modelConstructor, wizardConstructor, (m, w) -> new WizardDialog(shell, w));
	}

	/**
	 * Constructor from a {@link WizardDialog}.
	 */
	protected DefaultProcessWithWizard(String name, Supplier<T> modelConstructor, Function<T, Wizard> wizardConstructor,
			BiFunction<T, Wizard, WizardDialog> wizardDialogConstructor) {
		super(name);
		this.model = modelConstructor.get();
		// Creates the wizard
		Wizard wizard = wizardConstructor.apply(model);
		wizard.setWindowTitle(name);
		this.wizardDialog = wizardDialogConstructor.apply(model, wizard);
		this.wizardDialog.setMinimumPageSize(300, 510);
	}

	/**
	 * @return {@link WizardDialog}.
	 */
	public WizardDialog getWizardDialog() {
		return wizardDialog;
	}

	/**
	 * @return the model of this wizard.
	 */
	public T getModel() {
		return model;
	}

	/**
	 * Retrieves the {@link INavigationDataSupplier} for the given file.
	 */
	protected INavigationDataSupplier getNavigationDataProxy(File file) throws GIOException {
		Optional<INavigationDataSupplier> result = INavigationService.grab()
				.getNavigationDataProvider(file.getAbsolutePath());
		if (!result.isPresent()) {
			throw new GIOException("Convert to NVI error, input file not correct: " + file.getName());
		}
		return result.get();
	}

	/**
	 * Retrieves the {@link ISoundVelocityData} for the given file.
	 */
	protected ISoundVelocityData getSoundVelocityDataProxy(File file) throws GIOException {
		Optional<ISoundVelocityData> result = ISoundVelocityService.grab().getSoundVelocityData(file.getAbsolutePath());
		if (!result.isPresent()) {
			throw new GIOException("Convert to VEL error, input file not correct: " + file.getName());
		}
		return result.get();
	}

	/**
	 * Loads output file.
	 */
	protected void loadOutputFile(File outputFile, IProgressMonitor monitor) {
		if (outputFile != null && outputFile.exists() && !monitor.isCanceled() && (model.getLoadFilesAfter().isTrue()
				|| IFileLoadingService.grab().getFileInfoModel().contains(outputFile.getAbsolutePath()))) {
			ITreeNodeFactory.grab().createFileInfoNode(outputFile.getPath(), model.getWhereToloadFiles().get());
			IFileLoadingService.grab().reload(outputFile);
		}
	}

	/**
	 * @return true when file does not exists or can be overwritten
	 */
	protected boolean shouldBeProcessed(File file, Logger logger) {
		if (!file.exists() || model.getOverwriteExistingFiles().isTrue()) {
			return true;
		} else {
			logger.warn("Output file {} already exists, skipping (overwrite option disabled)", file.getName());
			return false;
		}
	}

	/**
	 * Gets a list of input, output, temporary files.
	 */
	protected List<FilesToProcessEntry> getFilesToProcessWithTemp(Logger logger) throws GIOException {
		List<FilesToProcessEntry> filesToProcess = new LinkedList<>();
		List<File> originFiles = model.getInputFiles().asList();
		List<File> outputFiles = model.getOutputFiles().asList();

		for (int i = 0; i < originFiles.size(); i++) {
			File originFile = originFiles.get(i);
			File processedFile = outputFiles.get(i);
			if (shouldBeProcessed(processedFile, logger)) {
				// If it's the origin file which will be processed: just add it to the list
				if (processedFile.getAbsolutePath().equals(originFile.getAbsolutePath())) {
					filesToProcess.add(new FilesToProcessEntry(originFile, originFile, originFile));
				}
				// Else : copy origin file with the new name
				else {
					try {
						File tempFile = TemporaryCache.createTemporaryFile(
								FilenameUtils.getBaseName(processedFile.getName()),
								"." + FilenameUtils.getExtension((processedFile.getName())));
						FileUtils.copyFile(originFile, tempFile);
						filesToProcess.add(new FilesToProcessEntry(originFile, processedFile, tempFile));
					} catch (IOException e) {
						throw new GIOException("Error creating temp file.", e);
					}
				}
			}
		}
		return filesToProcess;
	}

	/**
	 * Writes output file.
	 * 
	 * @throws GIOException
	 */
	protected void writeOutputFileFromTemp(FilesToProcessEntry entry) throws GIOException {
		File inputFile = entry.getInputFile();
		File outputFile = entry.getOutputFile();
		File tempFile = entry.getTempFile();

		// if the processed file is in project explorer: reload it
		if (!inputFile.getAbsolutePath().equals(outputFile.getAbsolutePath())) {
			// move temp file to destination
			try {
				FileUtils.moveFile(tempFile, outputFile);
			} catch (FileExistsException e) {
				// force copy/delete
				try {
					FileUtils.copyFile(tempFile, outputFile);
					FileUtils.deleteQuietly(tempFile);
				} catch (IOException e1) {
					throw new GIOException("Error copying temp file to destination.", e);
				}
			} catch (IOException e) {
				throw new GIOException("Error moving temp file.", e);
			}
		}
		logger.info("Copying result file to " + outputFile.getAbsolutePath());
	}

	/**
	 * Loads output files if asked.
	 */
	protected void loadOutputFiles(List<FilesToProcessEntry> filesToProcess, IProgressMonitor monitor) {
		for (FilesToProcessEntry entry : filesToProcess) {
			loadOutputFile(entry.getOutputFile(), monitor);
		}
	}

}