package fr.ifremer.globe.ui.wizard;

import java.io.File;
import java.util.Optional;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWaterColumnVolumicLayer;
import fr.ifremer.globe.ui.widget.wc.WCFilterParametersComposite;

public class WCFilterParametersPage extends WizardPage implements ISummarizableWizardPage {

	private WritableString filterDescription;
	private FilterParameters filterParameters;
	private WCFilterParametersComposite wcFilterContainer;
	/**
	 * Input files specified in the Wizard.
	 */
	private final WritableObjectList<File> inputFiles;

	private IFileService fileService;
	private IWWLayerService layerService;

	/**
	 * @wbp.parser.constructor
	 */
	public WCFilterParametersPage(WritableString parameters) {
		this(parameters, null);
	}

	public WCFilterParametersPage(WritableString parameters, WritableObjectList<File> inputFiles) {
		super("");
		fileService = IFileService.grab();
		layerService = IWWLayerService.grab();
		filterDescription = parameters;
		filterParameters = FilterParameters.fromJson(filterDescription.get());
		// initialize filter parameters values from previous launch, but globally disabled
		filterParameters = FilterParameters.getDefaultFrom(filterParameters).fromEnabled(false);
		this.inputFiles = inputFiles;
		setTitle("Setup wc filter parameters");
		setDescription("Setup wc filter parameters.");
	}

	@Override
	public void createControl(Composite parent) {
		var scrollContainer = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrollContainer.setExpandVertical(true);
		scrollContainer.setExpandHorizontal(true);
		scrollContainer.setLayout(GridLayoutFactory.fillDefaults().create());
		setControl(scrollContainer);

		wcFilterContainer = new WCFilterParametersComposite(scrollContainer, SWT.NONE, filterParameters);
		wcFilterContainer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		wcFilterContainer.getPublisher().subscribe(model -> filterParameters = model);
		scrollContainer.setMinSize(wcFilterContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		scrollContainer.setSize(300,300);
		scrollContainer.setContent(wcFilterContainer);

		this.inputFiles.addChangeListener(event -> onInputFilesChanged());
		onInputFilesChanged();
	}

	private void onInputFilesChanged() {
		this.inputFiles.stream()//
				.filter(File::exists)// File must exist
				.map(File::getPath)//
				.map(fileService::getFileInfoSilently)//
				.filter(Optional::isPresent)//
				.map(Optional::get)//
				.map(fileInfo -> layerService.getFileLayerStoreModel().get(fileInfo))//
				.filter(Optional::isPresent)//
				.map(Optional::get)//
				.flatMap(layerStore -> layerStore.getLayers(IWWWaterColumnVolumicLayer.class) //
						.map(IWWWaterColumnVolumicLayer::getFilterParameters))//
				.findAny() //
				.ifPresent(params -> wcFilterContainer.updateParams(params));
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible)
			onEnterPage();
		else
			onExitPage();
	}

	private void onEnterPage() {
		wcFilterContainer.updateParams(filterParameters);
	}

	private void onExitPage() {
		if (filterParameters.enabled) {
			filterDescription.set(filterParameters.toJson());
		} else {
			filterDescription.set("");
		}
	}

	/** {@inheritDoc} */
	@Override
	public String summarize() {
		if (filterParameters.enabled) {
			return filterDescription.get();
		}
		return "No filter parameters";
	}
}
