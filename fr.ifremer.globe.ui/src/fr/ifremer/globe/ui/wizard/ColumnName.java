/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard;

/**
 *
 */
public class ColumnName {

	/** Name of the column */
	protected String name;

	/**
	 * {@link Integer#getClass()}, {@link Double#getClass()} or {@link String#getClass()} depending on the content type
	 * of the column
	 */
	protected Class<?> type;

	/**
	 * @return true when this column name can be edided
	 */
	public boolean canEdit() {
		return !"latitude".equalsIgnoreCase(getName()) && !"longitude".equalsIgnoreCase(getName())
				&& !"cdi".equalsIgnoreCase(getName());
	}

	/**
	 * @return the {@link #name}
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the {@link #name} to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the {@link #type}
	 */
	public Class<?> getType() {
		return type;
	}

	/**
	 * @param type the {@link #type} to set
	 */
	public void setType(Class<?> type) {
		this.type = type;
	}

}
