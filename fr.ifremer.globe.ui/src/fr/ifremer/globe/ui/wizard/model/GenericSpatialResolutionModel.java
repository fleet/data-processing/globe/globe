package fr.ifremer.globe.ui.wizard.model;

import java.util.Optional;
import java.util.function.Consumer;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.wizard.page.GenericSpatialResolutionComposite.IGenericSpatialResolutionModel;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;

/**
 * Implementation of IGenericSpatialResolutionModel for a generated wizard
 */
public class GenericSpatialResolutionModel implements IGenericSpatialResolutionModel {

	/** The used Projection. Mercator by default */
	private final WritableObject<Projection> projection;

	/** The spatial resolution. Empty when no grid computation is not required */
	private final Optional<WritableDouble> spatialResolution;

	/** Function called to request an evaluation of the spatial resolution */
	private final Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator;

	/**
	 * Constructor
	 */
	public GenericSpatialResolutionModel(WritableObject<Projection> projection,
			Optional<WritableDouble> spatialResolution,
			Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator) {
		this.projection = projection;
		this.spatialResolution = spatialResolution;
		this.spatialResolutionEvaluator = spatialResolutionEvaluator;
	}

	/**
	 * @return the {@link #projection}
	 */
	@Override
	public WritableObject<Projection> getProjection() {
		return projection;
	}

	/**
	 * @return the {@link #spatialResolution}
	 */
	@Override
	public Optional<WritableDouble> getSpatialResolution() {
		return spatialResolution;
	}

	/**
	 * @return the {@link #spatialResolutionEvaluator}
	 */
	@Override
	public Optional<Consumer<MapOfDoubleConsumer>> getSpatialResolutionEvaluator() {
		return spatialResolutionEvaluator;
	}

}
