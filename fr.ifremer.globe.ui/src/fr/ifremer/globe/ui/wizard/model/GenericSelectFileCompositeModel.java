package fr.ifremer.globe.ui.wizard.model;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.widget.FileComposite.SelectFileCompositeModel;

/**
 * Implementation of SelectFileCompositeModel for a generated wizard
 */
public class GenericSelectFileCompositeModel implements SelectFileCompositeModel {

	protected WritableFile inputFile;
	protected int fileRequirements;
	protected String selectedFileFilterExtension;
	protected List<Pair<String, String>> selectedFileFilterExtensions = new ArrayList<>();

	public GenericSelectFileCompositeModel(List<Pair<String, String>> fileFilter, WritableFile inputFile,
			int fileRequirements) {
		this.inputFile = inputFile;
		this.fileRequirements = fileRequirements;
		if (fileFilter != null) {
			selectedFileFilterExtensions.addAll(fileFilter);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableFile getSelectedFile() {
		return inputFile;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSelectedFileFilterExtension() {
		return selectedFileFilterExtension;
	}

	@Override
	public int getFileRequirements() {
		return fileRequirements;
	}

	@Override
	public List<Pair<String, String>> getSelectedFileFilterExtensions() {
		return selectedFileFilterExtensions;
	}
}
