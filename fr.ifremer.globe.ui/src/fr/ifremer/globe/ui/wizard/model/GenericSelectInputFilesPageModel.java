/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.wizard.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage.SelectInputFilesPageModel;

/**
 * Implementation of SelectInputFilesPageModel for a generated wizard
 */
public class GenericSelectInputFilesPageModel implements SelectInputFilesPageModel {

	/** List of Files */
	protected WritableObjectList<File> inputFiles = new WritableObjectList<>();
	/** List of file Extensions */
	protected List<Pair<String, String>> inputFilesFilterExtensions = new ArrayList<>();
	/** True when at least one file must be selected */
	protected boolean mandatory;

	/**
	 * Constructor
	 */
	public GenericSelectInputFilesPageModel(WritableObjectList<File> selectedFiles) {
		this(selectedFiles, true);
	}

	/**
	 * Constructor
	 */
	public GenericSelectInputFilesPageModel(WritableObjectList<File> selectedFiles, boolean mandatory) {
		this.inputFiles = selectedFiles;
		this.mandatory = mandatory;
	}

	/**
	 * @return the {@link #inputFiles}
	 */
	@Override
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	/**
	 * @param inputFiles the {@link #inputFiles} to set
	 */
	public void setInputFiles(WritableObjectList<File> inputFiles) {
		this.inputFiles = inputFiles;
	}

	/**
	 * @return the {@link #inputFilesFilterExtensions}
	 */
	@Override
	public List<Pair<String, String>> getInputFilesFilterExtensions() {
		return inputFilesFilterExtensions;
	}

	/**
	 * @param inputFilesFilterExtensions the {@link #inputFilesFilterExtensions} to set
	 */
	public void setInputFilesFilterExtensions(List<Pair<String, String>> inputFilesFilterExtensions) {
		this.inputFilesFilterExtensions = inputFilesFilterExtensions;
	}

	/**
	 * @return the {@link #mandatory}
	 */
	@Override
	public boolean isMandatory() {
		return mandatory;
	}
}
