/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.wizard.model;

import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.core.databinding.observable.list.WritableList;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.wizard.page.GenericGeoboxPage.IGenericGeoboxPageModel;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;

/**
 * Implementation of IGenericGeoboxPageModel for a generated wizard
 */
public class GenericGeoboxPageModel extends GenericSpatialResolutionModel implements IGenericGeoboxPageModel {

	/** All reference geobox */
	private final WritableList<GeoBox> geoboxes;

	/** Edited geobox */
	private final WritableObject<GeoBox> geoBox;

	/** Function called to request an evaluation of the geobox */
	private final Optional<Consumer<MapOfDoubleConsumer>> geoBoxEvaluator;

	/**
	 * Constructor
	 */
	public GenericGeoboxPageModel(WritableList<GeoBox> geoboxes, WritableObject<GeoBox> geoBox,
			WritableObject<Projection> projection, Optional<WritableDouble> spatialResolution,
			Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator,
			Optional<Consumer<MapOfDoubleConsumer>> geoBoxEvaluator) {
		super(projection, spatialResolution, spatialResolutionEvaluator);
		this.geoboxes = geoboxes;
		this.geoBox = geoBox;
		this.geoBoxEvaluator = geoBoxEvaluator;
	}

	/**
	 * @return the {@link #geoBox}
	 */
	@Override
	public WritableObject<GeoBox> getGeoBox() {
		return geoBox;
	}

	/**
	 * @return the {@link #geoboxes}
	 */
	@Override
	public WritableList<GeoBox> getGeoboxes() {
		return geoboxes;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Consumer<MapOfDoubleConsumer>> getGeoBoxEvaluator() {
		return geoBoxEvaluator;
	}

}
