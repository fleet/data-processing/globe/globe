/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.wizard.model;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPageModel;
import fr.ifremer.globe.utils.FileUtils;

/**
 * 
 */
public class GenericSelectOutputParametersPageModel implements SelectOutputParametersPageModel {

	private final WritableObjectList<File> inputFiles;
	private final WritableObjectList<File> outputFiles;

	/** True to load files at the end of process */
	private final WritableBoolean loadFilesAfter;
	/** Where to load the files (in witch group of project explorer) */
	private final WritableString whereToloadFiles;

	private final WritableBoolean overwriteExistingFiles;
	private final WritableBoolean mergeOutputFiles;
	private final WritableString prefix;
	private final WritableString suffix;
	private final WritableString singleOutputFilename;

	private final WritableFile outputDirectory = new WritableFile();
	private final WritableObjectList<String> outputFormatExtensions;

	/**
	 * Constructor
	 */
	public GenericSelectOutputParametersPageModel(WritableObjectList<File> inputFiles,
			WritableObjectList<String> outputFileExtensions, WritableObjectList<File> outputFiles,
			WritableBoolean loadFilesAfter, WritableString whereToloadFiles, WritableBoolean overwriteExistingFiles,
			WritableBoolean mergeOutputFiles, WritableString prefix, WritableString suffix,
			WritableString singleOutputFilename) {
		this.inputFiles = inputFiles;
		this.outputFiles = outputFiles;
		this.loadFilesAfter = loadFilesAfter;
		this.whereToloadFiles = whereToloadFiles;
		this.overwriteExistingFiles = overwriteExistingFiles;
		this.mergeOutputFiles = mergeOutputFiles;
		this.prefix = prefix;
		this.suffix = suffix;
		this.singleOutputFilename = singleOutputFilename;
		this.outputFormatExtensions = outputFileExtensions;

		// Extract original info from output files
		if (!outputFiles.isEmpty()) {
			File firstFile = outputFiles.get(0);
			outputDirectory.set(firstFile.getParentFile());

			// Only one file
			if (outputFiles.size() == 1) {
				singleOutputFilename.set(firstFile.getName());
			}

			// At least 2 files...
			if (outputFiles.size() >= 2) {
				String baseName1 = FileUtils.getBaseName(firstFile);
				String baseName2 = FileUtils.getBaseName(outputFiles.get(1));
				// Search suffix
				if (suffix.isEmpty()) {
					int index = baseName1.lastIndexOf('_');
					if (index > 0 && baseName2.endsWith(baseName1.substring(index))) {
						suffix.set(baseName1.substring(index + 1));
					}
				}
				if (prefix.isEmpty()) {
					// Search prefix
					int index = baseName1.indexOf('_');
					if (index > 0 && baseName2.startsWith(baseName1.substring(0, index))) {
						prefix.set(baseName1.substring(0, index));
					}
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableFile getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableString getSingleOutputFilename() {
		return singleOutputFilename;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableString getPrefix() {
		return prefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableString getSuffix() {
		return suffix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableObjectList<String> getOutputFormatExtensions() {
		return outputFormatExtensions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getOutputFormatExtensions(String filename) {
		// find best output format extension
		if (outputFormatExtensions.isEmpty()) {
			// return input file extension
			return Arrays.asList(FileUtils.getExtension(filename));
		} else if (outputFormatExtensions.size() == 1) {
			// return configured extension
			return outputFormatExtensions;
		} else {
			// return exact match if exist or output format which begins like input extension
			String outputFileExtension;
			String inputFileExtension = FileUtils.getExtension(filename);
			if (outputFormatExtensions.stream().anyMatch(e -> e.equalsIgnoreCase(inputFileExtension))) {
				outputFileExtension = inputFileExtension;
			} else {
				outputFileExtension = outputFormatExtensions.stream().filter(e -> e.startsWith(inputFileExtension))
						.findFirst().orElse(outputFormatExtensions.get(0));
			}
			return Arrays.asList(outputFileExtension);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableObjectList<File> getOutputFiles() {
		return outputFiles;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableBoolean getOverwriteExistingFiles() {
		return overwriteExistingFiles;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WritableBoolean getLoadFilesAfter() {
		return loadFilesAfter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<WritableBoolean> getMergeOutputFiles() {
		return Optional.of(mergeOutputFiles);
	}

	/**
	 * @return the {@link #whereToloadFiles}
	 */
	@Override
	public WritableString getWhereToloadFiles() {
		return whereToloadFiles;
	}

}
