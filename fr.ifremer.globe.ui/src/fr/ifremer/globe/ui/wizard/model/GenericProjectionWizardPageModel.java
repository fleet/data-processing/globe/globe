/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.wizard.model;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.wizard.ProjectionWizardPage.IProjectionWizardPageModel;

/**
 * Implementation of IProjectionWizardPageModel for a generated wizard
 */
public class GenericProjectionWizardPageModel implements IProjectionWizardPageModel {

	protected WritableObject<ProjectionSettings> projectionSettings;

	/**
	 * Geobox used to guess some projection settings (latitude of true scale, UTM
	 * zone...
	 */
	protected WritableObject<GeoBox> geobox;

	/**
	 * Constructor
	 */
	public GenericProjectionWizardPageModel(WritableObject<ProjectionSettings> projectionSettings,
			WritableObject<GeoBox> geobox) {
		this.projectionSettings = projectionSettings;
		this.geobox = geobox;
	}

	/**
	 * @return the {@link #projectionSettings}
	 */
	@Override
	public WritableObject<ProjectionSettings> getProjectionSettings() {
		return projectionSettings;
	}

	/**
	 * @return the {@link #geobox}
	 */
	@Override
	public WritableObject<GeoBox> getGeobox() {
		return geobox;
	}

}
