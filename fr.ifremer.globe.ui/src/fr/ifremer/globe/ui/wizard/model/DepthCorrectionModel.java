package fr.ifremer.globe.ui.wizard.model;

import java.io.File;

import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionSource;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableFloat;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;

public record DepthCorrectionModel(//
		WritableObjectList<File> inputFiles, //
		WritableObject<TideType> tideType, //
		WritableObject<DepthCorrectionSource> tideCorrectionSource, //
		WritableFile tideTtb, //
		WritableFile platformElevationTtb, //
		WritableFloat nominalWaterLine, //
		WritableObject<DepthCorrectionSource> draughtCorrectionSource, //
		WritableFile draughtTtb//
) {

	public static DepthCorrectionModel instance(WritableObjectList<File> inputFiles) {
		return new DepthCorrectionModel(//
				inputFiles, //
				new WritableObject<>(TideType.MEASURED), //
				new WritableObject<>(DepthCorrectionSource.NONE), //
				new WritableFile(), //
				new WritableFile(), //
				new WritableFloat(-1.0f), //
				new WritableObject<>(DepthCorrectionSource.NONE), //
				new WritableFile() //
		);

	}

	public TideType getTideType() {
		return tideType.get();
	}

	public void setTideType(TideType tideType) {
		this.tideType.set(tideType);
	}

	public DepthCorrectionSource getTideCorrectionSource() {
		return tideCorrectionSource.get();
	}

	public void setTideCorrectionSource(DepthCorrectionSource tideCorrectionSource) {
		this.tideCorrectionSource.set(tideCorrectionSource);
	}

	public DepthCorrectionSource getDraughtCorrectionSource() {
		return draughtCorrectionSource.get();
	}

	public void setDraughtCorrectionSource(DepthCorrectionSource draughtCorrectionSource) {
		this.draughtCorrectionSource.set(draughtCorrectionSource);
	}
}
