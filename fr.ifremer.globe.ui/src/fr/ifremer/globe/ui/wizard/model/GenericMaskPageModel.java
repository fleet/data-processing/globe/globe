/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard.model;

import java.io.File;
import java.util.List;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.wizard.MaskPage.MaskPageModel;

/**
 * Implementation of MaskPageModel for a generated wizard
 */
public class GenericMaskPageModel implements MaskPageModel {

	/**
	 * @return the boolean indicating the mask page is activated
	 */
	WritableBoolean maskEnable;

	/**
	 * @return the number of cell composing a mask
	 */
	WritableInt maskSize;

	/**
	 * @return the masking files
	 */
	WritableObjectList<File> maskFiles;

	/**
	 * Type of files allowed for masking (KML_XML, SHAPEFILE...)
	 */
	List<Pair<String, String>> maskFilesFilterExtensions;

	/**
	 * Constructor
	 */
	public GenericMaskPageModel(WritableBoolean maskEnable, WritableInt maskSize, WritableObjectList<File> maskFiles,
			List<Pair<String, String>> maskFilesFilterExtensions) {
		this.maskEnable = maskEnable;
		this.maskSize = maskSize;
		this.maskFiles = maskFiles;
		this.maskFilesFilterExtensions = maskFilesFilterExtensions;
	}

	/**
	 * @return the {@link #maskEnable}
	 */
	@Override
	public WritableBoolean getMaskEnable() {
		return maskEnable;
	}

	/**
	 * @return the {@link #maskSize}
	 */
	@Override
	public WritableInt getMaskSize() {
		return maskSize;
	}

	/**
	 * @return the {@link #maskFiles}
	 */
	@Override
	public WritableObjectList<File> getMaskFiles() {
		return maskFiles;
	}

	/**
	 * @return the {@link #maskFilesFilterExtensions}
	 */
	@Override
	public java.util.List<Pair<String, String>> getMaskFilesFilterExtensions() {
		return maskFilesFilterExtensions;
	}

}
