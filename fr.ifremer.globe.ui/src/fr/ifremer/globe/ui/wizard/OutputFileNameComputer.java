/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.wizard;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableString;

/**
 * Tool to compute an output file name
 */
public class OutputFileNameComputer {

	/** compute an output file name */
	public File compute(File file, OutputFileNameComputerModel outputFileNameComputerModel, String extension) {
		String singleOutputFilename = outputFileNameComputerModel.getSingleOutputFilename().get();
		if (singleOutputFilename == null || singleOutputFilename.isEmpty()) {
			return computeOutputFilename(file, outputFileNameComputerModel, extension);
		} else {
			return new File(outputFileNameComputerModel.getOutputDirectory().get(), singleOutputFilename);
		}
	}

	/**
	 * @return the output filename, a concatenation of prefix, input filename, suffix and extension
	 */
	protected File computeOutputFilename(File file, OutputFileNameComputerModel outputFileNameComputerModel,
			String extension) {
		String prefix = outputFileNameComputerModel.getPrefix().isEmpty() ? ""
				: outputFileNameComputerModel.getPrefix().get() + '_';
		String suffix = outputFileNameComputerModel.getSuffix().isEmpty() ? ""
				: '_' + outputFileNameComputerModel.getSuffix().get();
		String fileNameFormat = "%s%s%s%s%s";

		// Prevent extension like .dtm.nc
		String basename = FilenameUtils.getBaseName(file.getName());
		while (FilenameUtils.indexOfExtension(basename) > 0) {
			basename = FilenameUtils.removeExtension(basename);
		}

		return new File(outputFileNameComputerModel.getOutputDirectory().get(), String.format(fileNameFormat, prefix,
				basename, suffix, outputFileNameComputerModel.getExtensionSeparator(), extension));
	}

	/**
	 * Model expected by OutputFileNameComputer
	 */
	public interface OutputFileNameComputerModel {

		/**
		 * Getter of outputDirectory
		 */
		WritableFile getOutputDirectory();

		/**
		 * Getter of singleOutputFile
		 */
		WritableString getSingleOutputFilename();

		/**
		 * Getter of extensionSeparator
		 */
		default String getExtensionSeparator() {
			return ".";
		}

		/**
		 * Getter of prefix
		 */
		WritableString getPrefix();

		/**
		 * Getter of suffix
		 */
		WritableString getSuffix();
	}
}
