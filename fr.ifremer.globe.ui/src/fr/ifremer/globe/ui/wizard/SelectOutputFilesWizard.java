package fr.ifremer.globe.ui.wizard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.Wizard;

/**
 * 
 * Wizard to configure multi output file names for a process
 * 
 * 
 * @author Gael Quemener <Gael.Quemener@ifremer.fr>
 * 
 */
public class SelectOutputFilesWizard extends Wizard {

	protected List<String> inputFiles = new ArrayList<String>();

	protected List<String> outputFiles = new ArrayList<String>();
	private SelectOutputFilesPage page;
	private String outputFileExtension;
	private String prefixString;
	private String suffixString;

	/**
	 * @param inputF
	 *            list of input files (absolute paths)
	 * @param title
	 *            title of the wizard
	 * @param ofe
	 *            extension of output files
	 */
	public SelectOutputFilesWizard(List<String> inputF, String title, String ofe) {
		super();
		this.inputFiles = inputF;
		outputFileExtension = ofe;
		prefixString = "";
		suffixString = "";
		outputFiles = new ArrayList<String>();
		setNeedsProgressMonitor(true);
		setWindowTitle(title);
	}

	/**
	 * Constructor
	 * 
	 * @param inputF
	 *            list of input files (absolute paths)
	 * @param title
	 *            title of the wizard
	 * @param ofe
	 *            extension of output files
	 * @param prefix
	 *            initial value of prefix to add to output files
	 * @param suffix
	 *            initial value of suffix to add to output files
	 */
	public SelectOutputFilesWizard(List<String> inputF, String title, String ofe, String prefix, String suffix) {
		super();
		this.inputFiles = inputF;
		outputFileExtension = ofe;
		prefixString = prefix;
		suffixString = suffix;
		outputFiles = new ArrayList<String>();
		setNeedsProgressMonitor(true);
		setWindowTitle(title);
	}

	@Override
	public boolean performFinish() {
		outputFiles = page.outputFiles;
		return true;
	}

	@Override
	public void addPages() {
		page = new SelectOutputFilesPage(inputFiles, outputFiles, "Select output files", "Select ouput directory, add a suffix and a prefix", outputFileExtension, prefixString, suffixString);
		addPage(page);
	}

	public List<String> getOutputFiles() {
		return outputFiles;
	}
}
