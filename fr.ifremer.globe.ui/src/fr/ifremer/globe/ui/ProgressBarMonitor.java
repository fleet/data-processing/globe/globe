package fr.ifremer.globe.ui;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ProgressBar;

import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.utils.exception.GException;

public class ProgressBarMonitor extends Composite  {
	private ProgressBar progressBar;
	UISynchronize sync;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ProgressBarMonitor(Composite parent, int style, UISynchronize sync, IEclipseContext context) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));

		this.sync=sync;

		progressBar = new ProgressBar(this, SWT.INDETERMINATE);
		progressBar.setVisible(false);
		progressBar.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {

			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				try {
					CommandUtils.executeCommand(context, "org.eclipse.e4.ui.views.progress.commands.openProgressView");
				} catch (GException ex) {
					ex.printStackTrace();
				}
			}
		});
	}

	public void start() {
		sync.asyncExec(new Runnable() {
			@Override
			public void run() {
				if(!progressBar.isDisposed())
					progressBar.setVisible(true);
			}
		});	
	}

	public void stop() {
		sync.asyncExec(new Runnable() {
			@Override
			public void run() {
				if(!progressBar.isDisposed())
					progressBar.setVisible(false);
			}
		});		
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}


}
