package fr.ifremer.globe.ui.handlers;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.utils.preference.PreferenceEditorDialog;

public class ShowGenericPreferenceDialogHandler {

	@Inject
	@Named(IServiceConstants.ACTIVE_SHELL)
	protected Shell shell;

	@Execute
	public void execute() {
		new PreferenceEditorDialog(shell).open();
	}
}