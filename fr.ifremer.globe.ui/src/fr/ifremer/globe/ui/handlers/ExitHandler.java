/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.globe.ui.handlers;

import jakarta.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.e4.ui.workbench.modeling.IWindowCloseHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handler for about dialog.
 *
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */

public class ExitHandler implements IWindowCloseHandler {

	private Logger logger = LoggerFactory.getLogger(ExitHandler.class);
	@Inject
	IWorkbench workbench;

	@Override
	public boolean close(MWindow window) {
		logger.info("Exit handler");

		if (MessageDialog.openConfirm(Display.getDefault().getActiveShell(), "Confirmation", "Do you want to exit?")) {
			//ModelRoot.getInstance().getEventBus().publish(new AppLifeEvent(LifeState.ePreCloseViews));
			return true;
		} else {
			return false;
		}
	}

}
