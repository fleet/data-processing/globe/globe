/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.globe.ui.handlers;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;

import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.dialogs.PickWorkspaceDialog;
import fr.ifremer.globe.ui.utils.Messages;

public class QuitHandler {
	private Logger logger = LoggerFactory.getLogger(QuitHandler.class);
	protected final static String TOOLBOX_K_TMP_PATH = "Toolbox/CIB_Temp";

	@Execute
	public void execute(IWorkbench workbench, IEclipseContext context,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) throws InvocationTargetException, InterruptedException {
		logger.info("Quit handler");

		if (MessageDialog.openConfirm(Display.getDefault().getActiveShell(), "Confirmation", "Do you want to exit?")) {
			// delete the contents of the toolbox temporary directory (Caraibes modules)
			java.nio.file.Path tmpPath = Paths
					.get(PickWorkspaceDialog.getLastSetWorkspaceDirectory() + "/" + TOOLBOX_K_TMP_PATH);
			if (tmpPath.toFile().exists()) {
				try {
					org.apache.commons.io.FileUtils.cleanDirectory(tmpPath.toFile());
				} catch (IOException e) {
					Messages.openErrorMessage("Toolbox temporary diectory",
							"Impossible to delete files in : " + TOOLBOX_K_TMP_PATH + "\n" + e.toString());
				}
			}
			workbench.close();
		}
	}
}
