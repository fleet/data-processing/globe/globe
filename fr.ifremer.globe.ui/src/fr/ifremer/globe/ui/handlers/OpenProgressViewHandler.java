package fr.ifremer.globe.ui.handlers;


import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.PartManager;

public class OpenProgressViewHandler {

	static String partId = "org.eclipse.e4.ui.progress.ProgressView";

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application, EModelService modelService) {

		MPart part = partService.findPart(partId);
		if(part == null) {
			part = partService.createPart(partId);
			PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		}
		partService.showPart(part, PartState.ACTIVATE);
		partService.bringToTop(part);
	}
}