package fr.ifremer.globe.ui.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.handler.PartManager;

public class OpenToolboxExplorerHandler {

	private Logger logger = LoggerFactory.getLogger(OpenToolboxExplorerHandler.class);
	private static String partId = "fr.ifremer.globe.toolboxExplorer";

	@CanExecute
	public boolean canExecute(ESelectionService selectionService) {
		return true;
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, EPartService partService, MApplication application, EModelService modelService) {

		logger.info("OpenToolPanel handler");

		MPart summaryViewPart = PartManager.createPart(partService, partId);
		PartManager.addPartToStack(summaryViewPart, application, modelService, PartManager.LEFT_TOP_STACK_ID);
		PartManager.showPart(partService, summaryViewPart);
	}
}
