package fr.ifremer.globe.ui.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.PartUtil;

/**
 * Shows a part identified by its ID.
 */
public class ShowViewHandler {
	private Logger logger = LoggerFactory.getLogger(ShowViewHandler.class);

	@Execute
	public void execute(@Named("fr.ifremer.globe.showViewCommand.param.viewId") String viewId, EPartService partService,
			MApplication application) {
		logger.info("Show view {}", viewId);

		MPart part = PartUtil.findPart(application, partService, viewId);
		if (part == null)
			part = partService.createPart(viewId);

		if (part != null) {
			PartUtil.showPartSashContainer(part);
			part.setVisible(true);
			PartManager.showPart(partService, part);
		} else
			logger.warn("Part {} not found.", viewId);
	}
}
