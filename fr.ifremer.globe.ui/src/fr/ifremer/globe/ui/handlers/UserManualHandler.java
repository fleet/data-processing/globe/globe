package fr.ifremer.globe.ui.handlers;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jakarta.inject.Named;

import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserManualHandler {

	private Logger logger = LoggerFactory.getLogger(UserManualHandler.class);

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		logger.info("User Manual handler");

		String pdfName = Platform.getInstallLocation().getURL().getPath() + "/UserManual.pdf";

		// reading and writing pdf file...
		File pdfFile = new File(pdfName);
		if (!pdfFile.exists()) {
			try {
				InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/help/UserManual.pdf");
				OutputStream out = new FileOutputStream(pdfFile);
				byte buf[] = new byte[1024];
				int len;
				while ((len = inputStream.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.close();
				inputStream.close();
			} catch (IOException e) {
				logger.error("Can't read or write user manual");
			}
		}

		// opening pdf file...
		if (pdfName != null) {
			try {
				if (pdfFile.exists()) {
					if (Desktop.isDesktopSupported()) {
						Desktop.getDesktop().open(pdfFile);
					} else {
						logger.debug("Awt Desktop is not supported!");
					}
				} else {
					logger.debug("PDF File is not exists!");
				}
			} catch (Exception ex) {
				try {
					if (Desktop.isDesktopSupported()) {
						Desktop.getDesktop().open(new File(Platform.getInstallLocation().getURL().getPath()));
					} else {
						logger.debug("Awt Desktop is not supported!");
					}
				} catch (Exception ex2) {
					logger.error("Can't open user manual " + ex2);
				}
			}
		}
	}
}
