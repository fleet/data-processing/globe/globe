/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.globe.ui.handlers;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.tree.TreeNode;

public class RefreshHandler {

	private Logger logger = LoggerFactory.getLogger(RefreshHandler.class);

	@Execute
	public void execute(ESelectionService selectionService, IProgressMonitor monitor) {
		logger.info("Refresh handler");

		Object obj = selectionService.getSelection();
		if (obj instanceof StructuredSelection) {
			Object[] elements = ((StructuredSelection) obj).toArray();
			for (Object e : elements) {
				if (e instanceof TreeNode) {
					((TreeNode<?>) e).refresh(true);
				}
			}
		}
	}
}
