package fr.ifremer.globe.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwitchToDefaultPerspective {
	private Logger logger = LoggerFactory.getLogger(SwitchToDefaultPerspective.class);

	@CanExecute
	public boolean canExecute() {
		return true;
	}

	@Execute
	public void execute(MApplication app, EPartService partService, EModelService modelService) {
		logger.info("SwitchToDefaultPerspective handler");

		MPerspective element = (MPerspective) modelService.find("fr.ifremer.globe.perspective.default", app);
		// now switch perspective
		partService.switchPerspective(element);

	}
}
