/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.globe.ui.handlers;

import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MContribution;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.core.runtime.job.IProcessService;

public class SaveHandler {
	private Logger logger = LoggerFactory.getLogger(SaveHandler.class);

	@CanExecute
	public boolean canExecute(@Named(IServiceConstants.ACTIVE_PART) final MContribution activePart, MWindow window) {
		return activePart != null && activePart.getObject() instanceof ISaveable saveable && saveable.isDirty();
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_PART) final MContribution activePart) {
		logger.info("Save handler");
		if (activePart != null && activePart.getObject() instanceof ISaveable saveable) {
			IProcessService.grab().runInBackground("Save", (monitor, logger) -> {
				saveable.doSave(monitor);
				return Status.OK_STATUS;
			});
		}
	}
}