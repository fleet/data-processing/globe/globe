/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.globe.ui.handlers;

import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.impl.HandledMenuItemImpl;
import org.eclipse.e4.ui.model.application.ui.menu.impl.HandledToolItemImpl;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.IDataUndoable;
import fr.ifremer.globe.ui.events.BaseEventTopics;
import fr.ifremer.globe.ui.undo.StackEvent;
import fr.ifremer.globe.ui.undo.UndoStack;

/**
 * Handler for redo action.
 * 
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */

@SuppressWarnings("restriction")
public class RedoHandler {

	private Logger logger = LoggerFactory.getLogger(RedoHandler.class);
	private List<MHandledToolItem> toolItems = new ArrayList<MHandledToolItem>();
	private List<MHandledMenuItem> menuItems = new ArrayList<MHandledMenuItem>();
	private boolean firstTime = true;
	@Inject
	IEventBroker eventBroker;

	RedoHandler() {
	}

	@Inject
	@Optional
	void stackChanged(@UIEventTopic(BaseEventTopics.UNDO_STACK) StackEvent e) {
		if (eventBroker != null) {
			eventBroker.send(UIEvents.REQUEST_ENABLEMENT_UPDATE_TOPIC, UIEvents.ALL_ELEMENT_ID);
		}
	}

	@CanExecute
	public boolean canExecute(EPartService partService, MApplication application, EModelService modelService,
			MWindow window) {
		if (firstTime) {
			initItems(application, modelService, window);
			firstTime = false;
		}
		MPart part = partService.getActivePart();
		String msgInfo = "Redo";

		if (part != null) {
			Object object = part.getObject();
			UndoStack stack = null;
			if (object != null && object instanceof IDataUndoable) {
				stack = ((IDataUndoable) object).getUndoStack();
			}
			if (stack != null) {
				if (!stack.isDone()) {
					// update menu label
					msgInfo = msgInfo + " " + stack.getNextUndoAction().msgInfo();
					updateToolItems(msgInfo);
					updateMenuItems(msgInfo);
					return true;
				}
			}
		}
		updateToolItems(msgInfo);
		updateMenuItems(msgInfo);
		return false;
	}

	@Execute
	public void execute(EPartService partService) {
		logger.info("Redo handler");
		MPart part = partService.getActivePart();
		if (part != null) {

			Object object = part.getObject();
			UndoStack stack = null;
			if (object != null && object instanceof IDataUndoable) {
				IDataUndoable o = ((IDataUndoable) object);
				stack = o.getUndoStack();
				if (stack != null) {
					stack.redo();
					o.setDirty(true);
				}
			}
		}
	}

	private void initItems(MApplication application, EModelService modelService, MWindow window) {
		this.toolItems = modelService.findElements(application, "fr.ifremer.globe.redotoolitem", null, null);
		MMenu menu = window.getMainMenu();
		retrieveMenuItems(menu);
	}

	private void retrieveMenuItems(MMenu menu) {
		for (MMenuElement menuElement : menu.getChildren()) {
			if (menuElement instanceof MMenu) {
				retrieveMenuItems((MMenu) menuElement);
			} else if (menuElement instanceof MHandledMenuItem) {
				if ((menuElement.getElementId() != null)
						&& (menuElement.getElementId().equals("fr.ifremer.globe.redomenuitem"))) {
					menuItems.add((MHandledMenuItem) menuElement);
				}
			}
		}
	}

	private void updateToolItems(String msgInfo) {
		if ((toolItems != null) && (!toolItems.isEmpty())) {
			for (MHandledToolItem item : toolItems) {
				((HandledToolItemImpl) item).setTooltip(msgInfo);
			}
		}
	}

	private void updateMenuItems(String msgInfo) {
		if ((menuItems != null) && (!menuItems.isEmpty())) {
			for (MHandledMenuItem item : menuItems) {
				((HandledMenuItemImpl) item).setLabel(msgInfo);
			}
		}
	}
}
