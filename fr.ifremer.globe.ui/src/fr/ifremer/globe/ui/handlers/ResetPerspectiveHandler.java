package fr.ifremer.globe.ui.handlers;

import java.util.prefs.Preferences;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;

public class ResetPerspectiveHandler {

	private Logger logger = LoggerFactory.getLogger(ResetPerspectiveHandler.class);

	@Execute
	void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		logger.info("Reset Perspective handler");

		// pref save
		Preferences prefs = Preferences.userRoot().node("fr.ifremer.globe.resetPerspectiveHandler");
		prefs.putBoolean("clearPerspective", true);

		// close shell
		shell.close();
		if (shell.isDisposed()) {
			// asyncExec to avoid invalid thread access error with swt widget
			// disposed
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					// restart properties
					System.setProperty("osgi.forcedRestart", Boolean.toString(true));
					try {
						// shutdown
						Activator.getContext().getBundle(0).stop(org.osgi.framework.Bundle.STOP_TRANSIENT);
					} catch (BundleException e) {
						logger.error("Unable to restart system.bundle", e);
					}
				}
			});
		}

	}

}
