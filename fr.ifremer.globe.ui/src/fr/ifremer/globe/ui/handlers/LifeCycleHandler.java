package fr.ifremer.globe.ui.handlers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.prefs.Preferences;

import jakarta.inject.Inject;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;
import org.eclipse.e4.ui.workbench.lifecycle.PreSave;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessAdditions;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.ISaveHandler;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.gws.GwsBootstrapService;
import fr.ifremer.globe.core.utils.GlobeVersion;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.ui.dialogs.NewVersionAvailableDialog;
import fr.ifremer.globe.ui.dialogs.PickWorkspaceDialog;
import fr.ifremer.globe.ui.service.gws.impl.MockGwsBootstrapService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.cache.TemporaryCache;

public class LifeCycleHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(LifeCycleHandler.class);

	private IEclipseContext context;

	/** Clean cache at startup (useful if the application has not been properly closed before) */
	@ProcessAdditions
	void addObjectToContext(IEclipseContext context) {
		TemporaryCache.cleanAllFiles();
	}

	@PostContextCreate
	void init(IEclipseContext context, final IEventBroker eventBroker, EModelService modelService) {

		this.context = context;

		// first : ask user to select workspace
		if (!openWorkspaceSelectionDialog()) {
			// cancelled by user or invalid workspace : exit
			try {
				PlatformUI.getWorkbench().close();
			} catch (Exception e) {
				//
			}
			System.exit(0);
		}

		// clearPerspective
		Preferences prefs = Preferences.userRoot().node("fr.ifremer.globe.resetPerspectiveHandler");
		// Load generic preferences
		PreferenceRegistry.getInstance().getRootNode().load();
		boolean clearPerspective = prefs.getBoolean("clearPerspective", false);
		if (clearPerspective) {
			System.setProperty(IWorkbench.CLEAR_PERSISTED_STATE, Boolean.toString(true));
			prefs.putBoolean("clearPerspective", false);
		}

		try {
			// because of a bug in eclipse branding, we need to load the defaults icons in java
			Window.setDefaultImages(new Image[] {
					ImageDescriptor.createFromURL(FileLocator.find(FrameworkUtil.getBundle(this.getClass()),
							new Path("icons/app/IconeGlobe16.png"), null)).createImage(),
					ImageDescriptor.createFromURL(FileLocator.find(FrameworkUtil.getBundle(this.getClass()),
							new Path("icons/app/IconeGlobe24.png"), null)).createImage(),
					ImageDescriptor.createFromURL(FileLocator.find(FrameworkUtil.getBundle(this.getClass()),
							new Path("icons/app/IconeGlobe32.png"), null)).createImage(),
					ImageDescriptor.createFromURL(FileLocator.find(FrameworkUtil.getBundle(this.getClass()),
							new Path("icons/app/IconeGlobe48.png"), null)).createImage(),
					ImageDescriptor.createFromURL(FileLocator.find(FrameworkUtil.getBundle(this.getClass()),
							new Path("icons/app/IconeGlobe64.png"), null)).createImage(),
					ImageDescriptor.createFromURL(FileLocator.find(FrameworkUtil.getBundle(this.getClass()),
							new Path("icons/app/IconeGlobe128.png"), null)).createImage(),
					ImageDescriptor.createFromURL(FileLocator.find(FrameworkUtil.getBundle(this.getClass()),
							new Path("icons/app/IconeGlobe256.png"), null)).createImage(),

			});
		} catch (Exception e) {
			LOGGER.error("Error while loading reference images : " + e.getMessage(), e);
		}

	}

	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onStartupComplete(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext context, EModelService modelService) {

		// Starting GWS
		GwsBootstrapService gwsBootstrapService = context.get(GwsBootstrapService.class);
		if (gwsBootstrapService == null)
			// No GwsBootstrapService in context : GWS plugin absent
			context.set(GwsBootstrapService.class, new MockGwsBootstrapService());
		else
			gwsBootstrapService.startServing();

		// override save handler (called when a part or a window is closing and there is some dirty part...)
		var theWindow = (MWindow) modelService.find("fr.ifremer.globe.mainwindow", context.get(MApplication.class));
		theWindow.getContext().set(ISaveHandler.class, new SaveOnExitHandler());

		updateWindowsTitle();
		checkIfNewVersionAvailable();
	}

	private boolean openWorkspaceSelectionDialog() {
		Location instanceLoc = Platform.getInstanceLocation();
		// isSet is true when workspace already set (globe launch with -data CLI option)
		if (instanceLoc.isSet())
			return true;

		var pickWorkspaceDialog = new PickWorkspaceDialog(false, null);
		if (pickWorkspaceDialog.open() != Window.CANCEL) {
			// clean up workspace directory, this will reset all configuration to default one
			File file = new File(pickWorkspaceDialog.getSelectedWorkspaceLocation() + "/.metadata/.plugins");
			if (file.exists() && file.isDirectory()) {
				FileUtils.deleteDir(file);
			}
			try {
				// tell Eclipse what the selected location was and continue
				instanceLoc.set(new URL("file", null, pickWorkspaceDialog.getSelectedWorkspaceLocation()), false);
				return true;
			} catch (IllegalStateException | IOException e) {
				Messages.openErrorMessage("Invalid workspace", "Error with selected wokscpace : " + e.getMessage(), e);
			}
		}
		return false;
	}

	/**
	 * Update windows title
	 */
	private void updateWindowsTitle() {
		Shell shell = (Shell) context.get(IServiceConstants.ACTIVE_SHELL);
		if (shell != null) {
			String workspace = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
			shell.setText("Globe " + GlobeVersion.VERSION + " - " + workspace);
		}
	}

	/**
	 * Displays a dialog if a new version of Globe is available. (The dialog is only opened once when a new version has
	 * been retrieved)
	 */
	private void checkIfNewVersionAvailable() {
		Shell shell = (Shell) context.get(IServiceConstants.ACTIVE_SHELL);
		new Thread(() -> GlobeVersion.checkIfNewVersionAvailable().ifPresent(newVersion -> {
			// Check is the version saved in preferences is different (the dialog is only
			// opened once)
			Optional<String> optVersionSavedInPref = GlobeVersion.getAvailableVersionSavedInPreferences();
			if (!optVersionSavedInPref.isPresent() || !optVersionSavedInPref.get().equals(newVersion)) {
				GlobeVersion.setAvailableVersionSavedInPreferences(newVersion);
				shell.getDisplay().asyncExec(() -> new NewVersionAvailableDialog(shell, newVersion).open());
			}
		})).start();
	}

	@PreSave
	void cleanupModel(IEclipseContext context) {
		// Stopping GWS
		GwsBootstrapService gwsBootstrapService = context.get(GwsBootstrapService.class);
		if (gwsBootstrapService != null)
			gwsBootstrapService.stopServing();

		// ModelRoot.getInstance().getEventBus().publish(new AppLifeEvent(LifeState.eClosing));

		// Delete Toolbox console views
		// if (application.getContext().getActiveChild() != null) {
		// MPart part = partService.findPart(ToolboxConsole.TOOLBOX_CONSOLE_ID);
		// while (part != null) {
		// part.getParent().getChildren().remove(part);
		// // part =
		// // partService.findPart(ToolboxConsole.TOOLBOX_CONSOLE_ID);
		// }
		// }

		// // Remove all swathEditor and time editor parts on close
		// String[] partsToClose = { "fr.ifremer.globe.swatheditor.partId",
		// "fr.ifremer.globe.timeeditor.partId" };

		// for (String id : partsToClose) {
		// MPart part = partService.findPart(id);
		// while (part != null) {
		// part.getParent().getChildren().remove(part);
		// part = partService.findPart(id);
		// }
		// }
	}

}