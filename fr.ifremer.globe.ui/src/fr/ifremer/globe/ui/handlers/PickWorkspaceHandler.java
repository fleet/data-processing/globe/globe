package fr.ifremer.globe.ui.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.ui.dialogs.PickWorkspaceDialog;

public class PickWorkspaceHandler {

	private Logger logger = LoggerFactory.getLogger(PickWorkspaceHandler.class);

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		PickWorkspaceDialog pwd = new PickWorkspaceDialog(true, null);
		int pick = pwd.open();
		if (pick == Window.CANCEL) {
			return;
		}

		MessageDialog.openInformation(Display.getDefault().getActiveShell(), "Switch Workspace", "The client will now restart with the new workspace");

		// restart client
		try {
			shell.close();
			System.setProperty("osgi.forcedRestart", Boolean.toString(true));
			Activator.getContext().getBundle(0).stop(org.osgi.framework.Bundle.STOP_TRANSIENT);
		} catch (BundleException e) {
			logger.error("Unable to restart system.bundle", e);
		}
	}
}
