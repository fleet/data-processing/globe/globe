package fr.ifremer.globe.ui.handlers;

import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.views.performances.MemoryView;

public class ShowMemoryUsageHandler {

	@CanExecute
	public boolean canExecute(ESelectionService selectionService) {

		return true;
	}

	static String partId = "fr.ifremer.globe.partdescriptor.memoryView";

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application, EModelService modelService) {

		MPart imagePart = partService.createPart(partId);

		if (imagePart != null) {
			List<MPartStack> stacks = modelService.findElements(application, null, MPartStack.class, null);
			if (!(stacks == null || stacks.size() == 0)) {

				stacks.get(0).getChildren().add(imagePart);
			}

			partService.showPart(imagePart, PartState.ACTIVATE);
			// Set selection

			MemoryView view = (MemoryView) imagePart.getObject();
			if (view != null) {
				view.setPart(imagePart);
			}

		}
	}

}
