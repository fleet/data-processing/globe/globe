package fr.ifremer.globe.ui.handlers;

import java.util.Arrays;
import java.util.Collection;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.InjectionException;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISaveHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.utils.Messages;

/**
 * Handler called after an exit event while a part is dirty.
 */
public class SaveOnExitHandler implements ISaveHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaveOnExitHandler.class);

	@Override
	public boolean save(MPart dirtyPart, boolean confirm) {
		if (confirm) {
			switch (promptToSave(dirtyPart)) {
			default:
			case NO:
				return true;
			case CANCEL:
				return false; // cancel the exit
			case YES:
				break;
			}
		}
		try {
			ContextInjectionFactory.invoke(dirtyPart.getObject(), Persist.class, dirtyPart.getContext());
		} catch (final InjectionException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		return true;
	}

	@Override
	public boolean saveParts(Collection<MPart> dirtyParts, boolean confirm) {
		return true; // the save of several part is not handled (issue with hidden dirty part!!)
	}

	@Override
	public Save promptToSave(MPart dirtyPart) {
		return switch (Messages.openSyncQuestionMessageWithCancel("Save file",
				"Data have been modified. Save changes?")) {
		case 0 -> Save.YES;
		case 1 -> Save.NO;
		default -> Save.CANCEL;
		};
	}

	@Override
	public Save[] promptToSave(Collection<MPart> dirtyParts) {
		Save[] rc = new Save[dirtyParts.size()];
		Arrays.fill(rc, Save.YES);
		return rc;
	}

}
