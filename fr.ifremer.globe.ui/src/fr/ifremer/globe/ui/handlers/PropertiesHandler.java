package fr.ifremer.globe.ui.handlers;

import java.util.List;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.properties.IPropertySource;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

public class PropertiesHandler {
	private Logger logger = LoggerFactory.getLogger(PropertiesHandler.class);

	@CanExecute
	public boolean canExecute(ESelectionService selectionService) {
		Object obj = selectionService.getSelection();
		if (obj instanceof StructuredSelection) {
			Object[] elements = ((StructuredSelection) obj).toArray();

			for (Object e : elements) {
				if (e instanceof FileInfoNode) {
					return true;
				}
			}
		}
		return false;
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService) {
		logger.info("Properties handler");

		Object obj = selectionService.getSelection();
		if (obj instanceof StructuredSelection) {
			Object[] elements = ((StructuredSelection) obj).toArray();

			for (Object e : elements) {
				if (e instanceof FileInfoNode) {
					String message = createMessage((IPropertySource) e);
					String title = "Properties for " + ((IPropertySource) e).getName();

					MessageDialog.openInformation(shell, title, message);
				}
			}
		}
	}

	private String createMessage(IPropertySource e) {
		List<Property<?>> propertiesList = e.getProperties();
		StringBuilder sb = new StringBuilder();
		if (propertiesList != null && !propertiesList.isEmpty()) {
			for (Property<?> property : propertiesList) {
				propertyMsg(sb, property, "");
			}
		} else {
			sb.append("No property available.");
		}
		return sb.toString();
	}

	private void propertyMsg(StringBuilder sb, Property<?> property, String prefix) {
		if (property.getKey() != null) {
			if (prefix != null && !prefix.isEmpty()) {
				sb.append(prefix);
			}
			sb.append(property.getKey());
			sb.append(" : \t");
			if (property.getValue() != null) {
				sb.append(property.getValue());
			}
			sb.append("\n");

			List<Property<?>> subProperties = property.getSubProperties();
			if (subProperties != null) {
				for (Property<?> p : subProperties) {
					propertyMsg(sb, p, prefix + "\t");
				}
			}
		}
	}

}
