package fr.ifremer.globe.ui.views.summary;

import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;

public class SummaryHandler extends AbstractNodeHandler {

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return getSelectionAsList(selectionService, ISounderNcInfo.class)
				.size() == getSelection(selectionService).length;
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application,
			EModelService modelService) {
		List<ISounderNcInfo> inputFiles = getSelectionAsList(selectionService, ISounderNcInfo.class);
		SummaryController summaryControler = new SummaryController();
		summaryControler.initializeController(inputFiles, selectionService, partService, application, modelService);
	}
}