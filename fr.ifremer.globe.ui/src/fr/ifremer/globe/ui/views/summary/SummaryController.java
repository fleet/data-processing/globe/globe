package fr.ifremer.globe.ui.views.summary;

import java.util.List;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MBasicFactory;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.ui.handler.PartLabel;
import fr.ifremer.globe.ui.handler.PartManager;

/**
 * Controller of summary, as defined in a model/view/controller pattern.
 */
public class SummaryController {

	private SummaryModel summaryModel;
	private SummaryView summaryView;

	private MPart summaryPart;

	/**
	 * Initialize all components of the controller (view and model)
	 *
	 * @param filesToOpen list of input files
	 * @param selectionService {@link ESelectionService}
	 * @param partService {@link EPartService}
	 * @param application {@link MApplication}
	 * @param modelService {@link EModelService}
	 */
	public void initializeController(List<ISounderNcInfo> filesToOpen, ESelectionService selectionService,
			EPartService partService, MApplication application, EModelService modelService) {
		initializeModel(filesToOpen);
		initializePart(filesToOpen, selectionService, partService, application, modelService);
		initializeView();
	}

	/**
	 * Initialize a model which contains all data used for the {@link SummaryView}.
	 *
	 * @param filesToOpen list of input files
	 */
	private void initializeModel(List<ISounderNcInfo> filesToOpen) {
		this.summaryModel = new SummaryModel(this, filesToOpen);
	}

	/**
	 * Initialize the part which will contain the chart view.
	 *
	 * @param filesToOpen list of input files
	 * @param selectionService {@link ESelectionService}
	 * @param partService {@link EPartService}
	 * @param application {@link MApplication}
	 * @param modelService {@link EModelService}
	 */
	private void initializePart(List<ISounderNcInfo> filesToOpen, ESelectionService selectionService,
			EPartService partService, MApplication application, EModelService modelService) {

		this.summaryPart = MBasicFactory.INSTANCE.createPart();
		this.summaryPart.setElementId("fr.ifremer.globe.summaryview");
		this.summaryPart.setLabel("Summary View of " + PartLabel.computeLabelI(filesToOpen));
		this.summaryPart.setCloseable(true);
		this.summaryPart
				.setContributionURI("bundleclass://fr.ifremer.globe.ui/fr.ifremer.globe.ui.views.summary.SummaryView");
		this.summaryPart.setIconURI("platform:/plugin/fr.ifremer.globe.ui/icons/16/properties.png");

		PartManager.addPartToStack(this.summaryPart, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, this.summaryPart);
	}

	/**
	 * Initialize the view which contains the summary table.
	 */
	public void initializeView() {

		this.summaryView = (SummaryView) this.summaryPart.getObject();
		this.summaryView.setSummaryControler(this);
		this.summaryView.updateTable();
	}

	/**
	 * @return the summaryModel
	 */
	public SummaryModel getSummaryModel() {
		return this.summaryModel;
	}

	/**
	 * @return the summaryView
	 */
	public SummaryView getSummaryView() {
		return this.summaryView;
	}
}
