package fr.ifremer.globe.ui.views.summary;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.views.summary.SummaryModel.SummaryOfFile;

public class SummaryProvider extends LabelProvider implements ITableLabelProvider {

	public SummaryProvider() {
		super();
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof SummaryOfFile) {
			SummaryOfFile summaryOfFile = (SummaryOfFile) element;
			switch (columnIndex) {
			case 0:
				return summaryOfFile.getName();
			case 1:
				return summaryOfFile.getStartDate();
			case 2:
				return summaryOfFile.getEndDate();
			case 3:
				return summaryOfFile.getAutomaticCleaning();
			case 4:
				return summaryOfFile.getManualCleaning();
			case 5:
				return summaryOfFile.getVelocityCorrection();
			case 6:
				return summaryOfFile.getBiasCorrection();
			case 7:
				return summaryOfFile.getTideCorrection();
			case 8:
				return summaryOfFile.getDraughtCorrection();
			default:
				return "";
			}
		}
		return "";
	}
}
