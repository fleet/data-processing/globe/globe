package fr.ifremer.globe.ui.views.summary;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.TableItem;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo.ProcessAssessor;

/**
 * Model of summary, as defined in a model/view/controller pattern.
 */
public class SummaryModel {

	/** List of all data required to build a summary of input files. */
	private List<SummaryOfFile> summariesOfFile;
	private SummaryController summaryController;

	/**
	 * Constructor of {@link SummaryModel}.
	 *
	 * @param summariesOfFile contains usefull data of a input globe file
	 * @param summaryController {@link SummaryController}
	 */
	public SummaryModel(SummaryController summaryController, List<ISounderNcInfo> filesToOpen) {
		this.summariesOfFile = new ArrayList<>();
		this.summaryController = summaryController;

		setUpModel(filesToOpen);
	}

	/**
	 * Builds {@link SummaryModel} with input files data.
	 *
	 * @param filesToOpen files to summerize in the {@link SummaryView}
	 */
	public void setUpModel(List<ISounderNcInfo> filesToOpen) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

		for (ISounderNcInfo fileToOpen : filesToOpen) {
			ProcessAssessor processAssessor = fileToOpen.getProcessAssessor().orElse(null);
			String name = fileToOpen.getFilename();
			String startDate = dateFormat.format(fileToOpen.getFirstPingDate());
			String endDate = dateFormat.format(fileToOpen.getLastPingDate());
			boolean velocityCorrection = false;
			boolean biasCorrection = false;
			boolean tideCorrection = false;
			boolean draughtCorrection = false;
			boolean automaticCleaning = false;
			boolean manualCleaning = false;
			if (processAssessor != null) {
				velocityCorrection = processAssessor.velocityCorrectionEnforced();
				biasCorrection = processAssessor.biasCorrectionEnforced();
				tideCorrection = processAssessor.tideCorrectionEnforced();
				draughtCorrection = processAssessor.draughtCorrectionEnforced();
				automaticCleaning = processAssessor.automaticCleaningEnforced();
				manualCleaning = processAssessor.manualCleaningEnforced();
			}
			SummaryOfFile summaryOfFile = new SummaryOfFile(name, startDate, endDate, automaticCleaning, manualCleaning,
					velocityCorrection, biasCorrection, tideCorrection, draughtCorrection);
			this.summariesOfFile.add(summaryOfFile);
		}
	}

	/**
	 * Sort {@link SummaryModel} as {@link SummaryView} table display.
	 */
	public void sortModelAsView() {

		this.summariesOfFile = new ArrayList<>();

		for (TableItem tableItem : this.getSummaryControler().getSummaryView().getTableViewer().getTable().getItems()) {
			String name = tableItem.getText(0);
			String startDate = tableItem.getText(1);
			String endDate = tableItem.getText(2);
			String automaticCleaning = tableItem.getText(3);
			String manualCleaning = tableItem.getText(4);
			String velocityCorrection = tableItem.getText(5);
			String biasCorrection = tableItem.getText(6);
			String tideCorrection = tableItem.getText(7);
			String soundingsCorrection = tableItem.getText(8);

			SummaryOfFile summaryOfFile = new SummaryOfFile(name, startDate, endDate, "Yes".equals(automaticCleaning),
					"Yes".equals(manualCleaning), "Yes".equals(velocityCorrection), "Yes".equals(biasCorrection),
					"Yes".equals(tideCorrection), "Yes".equals(soundingsCorrection));
			this.summariesOfFile.add(summaryOfFile);
		}
	}

	/**
	 * Selects all data of the {@link SummmaryModel} and puts them into a String in .csv format.
	 */
	public String convertModelToCSVFormat() {

		String buffer = "Name,Start Date,End Date,Automatic Cleaning,Manual Cleaning,Velocity Correction,Bias Correction,Tide Correction,Draught Correction"
				+ System.lineSeparator();

		for (SummaryOfFile summaryOfFile : this.summariesOfFile) {
			String line = summaryOfFile.getName() + "," + summaryOfFile.getStartDate() + ","
					+ summaryOfFile.getEndDate() + "," + summaryOfFile.getAutomaticCleaning() + ","
					+ summaryOfFile.getManualCleaning() + "," + summaryOfFile.getVelocityCorrection() + ","
					+ summaryOfFile.getBiasCorrection() + "," + summaryOfFile.getTideCorrection() + ","
					+ summaryOfFile.getDraughtCorrection();
			buffer += line + System.lineSeparator();
		}

		return buffer;
	}

	/**
	 * @return the summaryOfFiles
	 */
	public List<SummaryOfFile> getSummaryOfFiles() {
		return this.summariesOfFile;
	}

	/**
	 * @return the summaryControler
	 */
	public SummaryController getSummaryControler() {
		return this.summaryController;
	}

	/**
	 * Object which contains useful data to display in a {@link SummaryView}.
	 */
	public class SummaryOfFile {

		private String name;
		private String startDate;
		private String endDate;
		private String automaticCleaning;
		private String manualCleaning;
		private String velocityCorrection;
		private String biasCorrection;
		private String tideCorrection;
		private String draughtCorrection;

		public SummaryOfFile(String name, String startDate, String endDate, boolean automaticCleaning,
				boolean manualCleaning, boolean velocityCorrection, boolean biasCorrection, boolean tideCorrection,
				boolean draughtCorrection) {
			this.name = name;
			this.startDate = startDate;
			this.endDate = endDate;
			this.automaticCleaning = format(automaticCleaning);
			this.manualCleaning = format(manualCleaning);
			this.velocityCorrection = format(velocityCorrection);
			this.biasCorrection = format(biasCorrection);
			this.tideCorrection = format(tideCorrection);
			this.draughtCorrection = format(draughtCorrection);
		}

		/**
		 * Replace some textual expression to improve lisibility of summary.
		 */
		public String format(boolean data) {
			return data ? "Yes" : "No";
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return this.name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the startDate
		 */
		public String getStartDate() {
			return this.startDate;
		}

		/**
		 * @param startDate the startDate to set
		 */
		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		/**
		 * @return the endDate
		 */
		public String getEndDate() {
			return this.endDate;
		}

		/**
		 * @param endDate the endDate to set
		 */
		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		/**
		 * @return the automaticCleaning
		 */
		public String getAutomaticCleaning() {
			return this.automaticCleaning;
		}

		/**
		 * @param automaticCleaning the automaticCleaning to set
		 */
		public void setAutomaticCleaning(String automaticCleaning) {
			this.automaticCleaning = automaticCleaning;
		}

		/**
		 * @return the manualCleaning
		 */
		public String getManualCleaning() {
			return this.manualCleaning;
		}

		/**
		 * @param manualCleaning the manualCleaning to set
		 */
		public void setManualCleaning(String manualCleaning) {
			this.manualCleaning = manualCleaning;
		}

		/**
		 * @return the velocityCorrection
		 */
		public String getVelocityCorrection() {
			return this.velocityCorrection;
		}

		/**
		 * @param velocityCorrection the velocityCorrection to set
		 */
		public void setVelocityCorrection(String velocityCorrection) {
			this.velocityCorrection = velocityCorrection;
		}

		/**
		 * @return the biasCorrection
		 */
		public String getBiasCorrection() {
			return this.biasCorrection;
		}

		/**
		 * @param biasCorrection the biasCorrection to set
		 */
		public void setBiasCorrection(String biasCorrection) {
			this.biasCorrection = biasCorrection;
		}

		/**
		 * @return the tideCorrection
		 */
		public String getTideCorrection() {
			return this.tideCorrection;
		}

		/**
		 * @param tideCorrection the tideCorrection to set
		 */
		public void setTideCorrection(String tideCorrection) {
			this.tideCorrection = tideCorrection;
		}

		/**
		 * @return the draughtCorrection
		 */
		public String getDraughtCorrection() {
			return this.draughtCorrection;
		}

		/**
		 * @param draughtCorrection the draughtCorrection to set
		 */
		public void setDraughtCorrection(String draughtCorrection) {
			this.draughtCorrection = draughtCorrection;
		}
	}
}
