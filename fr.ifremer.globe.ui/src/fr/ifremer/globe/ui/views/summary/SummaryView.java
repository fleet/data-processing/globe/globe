package fr.ifremer.globe.ui.views.summary;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import fr.ifremer.globe.ui.TableViewerColumnSorter;
import fr.ifremer.globe.ui.views.summary.SummaryModel.SummaryOfFile;
import swing2swt.layout.BorderLayout;

/**
 * View of summary, as defined in a model/view/controller pattern.
 */
public class SummaryView extends Composite {

	/** Table which displays interesting data of input files */
	private TableViewer tableViewer;

	private Composite tableComposite;
	private Composite exportationComposite;

	private SummaryController summaryController;

	public String NAME_COLUMN_NAME = "Name";
	public String START_DATE_COLUMN_NAME = "Start Date";
	public String END_DATE_COLUMN_NAME = "End Date";
	public String AUTOMATIC_CLEANING_COLUMN_NAME = "Automatic Cleaning";
	public String MANUAL_CLEANING_COLUMN_NAME = "Manual Cleaning";
	public String VELOCITY_CORRECTION_COLUMN_NAME = "Velocity Correction";
	public String BIAS_CORRECTION_COLUMN_NAME = "Bias Correction";
	public String TIDE_CORRECTION_COLUMN_NAME = "Tide Correction";
	public String DRAUGHT_CORRECTION_COLUMN_NAME = "Draught Correction";

	@Inject
	public SummaryView(Composite parent) {

		super(parent, SWT.NONE);
		setLayout(new BorderLayout());

		initializeComposites();
		initializeComponents();
	}

	/**
	 * Initialize composites of {@link SummaryView}.
	 */
	public void initializeComposites() {

		this.tableComposite = new Composite(this, SWT.NONE);
		this.tableComposite.setLayoutData(BorderLayout.CENTER);
		this.tableComposite.setLayout(new FillLayout(SWT.HORIZONTAL));

		this.exportationComposite = new Composite(this, SWT.NONE);
		this.exportationComposite.setLayoutData(BorderLayout.NORTH);
		this.exportationComposite.setLayout(new GridLayout(1, false));
	}

	/**
	 * Initialize components of {@link SummaryView}.
	 */
	public void initializeComponents() {

		this.tableViewer = new TableViewer(this.tableComposite, SWT.BORDER | SWT.HIDE_SELECTION);
		Table table = this.tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableViewerColumn nameColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		nameColumn.getColumn().setText(this.NAME_COLUMN_NAME);
		addTableViewerColumnSorter(nameColumn);

		TableViewerColumn startDateColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		startDateColumn.getColumn().setText(this.START_DATE_COLUMN_NAME);
		addTableViewerColumnSorter(startDateColumn);

		TableViewerColumn endDateColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		endDateColumn.getColumn().setText(this.END_DATE_COLUMN_NAME);
		addTableViewerColumnSorter(endDateColumn);

		TableViewerColumn automaticCleaningColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		automaticCleaningColumn.getColumn().setText(this.AUTOMATIC_CLEANING_COLUMN_NAME);
		addTableViewerColumnSorter(automaticCleaningColumn);

		TableViewerColumn manualCleaningColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		manualCleaningColumn.getColumn().setText(this.MANUAL_CLEANING_COLUMN_NAME);
		addTableViewerColumnSorter(manualCleaningColumn);

		TableViewerColumn velocityCorrectionColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		velocityCorrectionColumn.getColumn().setText(this.VELOCITY_CORRECTION_COLUMN_NAME);
		addTableViewerColumnSorter(velocityCorrectionColumn);

		TableViewerColumn biasCorrectionColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		biasCorrectionColumn.getColumn().setText(this.BIAS_CORRECTION_COLUMN_NAME);
		addTableViewerColumnSorter(biasCorrectionColumn);

		TableViewerColumn tideCorrectionColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		tideCorrectionColumn.getColumn().setText(this.TIDE_CORRECTION_COLUMN_NAME);
		addTableViewerColumnSorter(tideCorrectionColumn);

		TableViewerColumn draughtCorrectionColumn = new TableViewerColumn(this.tableViewer, SWT.NONE);
		draughtCorrectionColumn.getColumn().setText(this.DRAUGHT_CORRECTION_COLUMN_NAME);
		addTableViewerColumnSorter(draughtCorrectionColumn);

		Button exportButton = new Button(this.exportationComposite, SWT.NONE);
		exportButton.setText("Export table into a new file...");
		exportButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				switch (e.type) {
				case SWT.Selection:

					exportTableToCSVFile();
					break;
				}
			}
		});
	}

	/**
	 * Fill the {@link SummaryView#tableViewer} with {@link SummaryModel} data.
	 */
	public void updateTable() {

		this.tableViewer.setLabelProvider(new SummaryProvider());
		this.tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		this.tableViewer.setInput(this.summaryController.getSummaryModel().getSummaryOfFiles());

		for (TableColumn tableColumn : this.tableViewer.getTable().getColumns()) {
			tableColumn.pack();
		}
	}

	/**
	 * Converts data of {@link SummaryModel} onto a .csv file.
	 */
	public void exportTableToCSVFile() {

		FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);
		dialog.setFilterNames(new String[] { ".csv" });
		dialog.setFilterExtensions(new String[] { "*.csv", });
		String savePath = dialog.open();

		if (savePath != null) {
			this.summaryController.getSummaryModel().sortModelAsView();
			try {
				PrintWriter writer = new PrintWriter(savePath, "UTF-8");
				writer.print(this.summaryController.getSummaryModel().convertModelToCSVFormat());
				writer.close();
			} catch (IOException e) {

			}
		}
	}

	/**
	 * Add a sorter to a table column.
	 *
	 * @param tableViewerColumn
	 *            column which a sorter has to be added
	 */
	public void addTableViewerColumnSorter(TableViewerColumn tableViewerColumn) {
		new TableViewerColumnSorter(tableViewerColumn) {
			@Override
			protected int doCompare(Viewer viewer, Object e1, Object e2) {
				if (e1 instanceof SummaryOfFile && e2 instanceof SummaryOfFile) {
					return getDataByParameter((SummaryOfFile) e1, tableViewerColumn)
							.compareTo(getDataByParameter((SummaryOfFile) e2, tableViewerColumn));
				} else {
					return super.doCompare(viewer, e1, e2);
				}
			}
		};
	}

	/**
	 * Returns a parameter of a {@link SummaryOfFile} object, according to a
	 * selected column.
	 *
	 * @param tableViewerColumn
	 *            column where the desired data is
	 * @param summaryOfFile
	 *            object where the desired data is
	 * @return
	 */
	public String getDataByParameter(SummaryOfFile summaryOfFile, TableViewerColumn tableViewerColumn) {

		if (tableViewerColumn.getColumn().getText().compareTo(this.NAME_COLUMN_NAME) == 0) {
			return summaryOfFile.getName();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.START_DATE_COLUMN_NAME) == 0) {
			return summaryOfFile.getStartDate();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.END_DATE_COLUMN_NAME) == 0) {
			return summaryOfFile.getEndDate();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.AUTOMATIC_CLEANING_COLUMN_NAME) == 0) {
			return summaryOfFile.getAutomaticCleaning();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.MANUAL_CLEANING_COLUMN_NAME) == 0) {
			return summaryOfFile.getManualCleaning();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.VELOCITY_CORRECTION_COLUMN_NAME) == 0) {
			return summaryOfFile.getVelocityCorrection();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.BIAS_CORRECTION_COLUMN_NAME) == 0) {
			return summaryOfFile.getBiasCorrection();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.TIDE_CORRECTION_COLUMN_NAME) == 0) {
			return summaryOfFile.getTideCorrection();
		} else if (tableViewerColumn.getColumn().getText().compareTo(this.DRAUGHT_CORRECTION_COLUMN_NAME) == 0) {
			return summaryOfFile.getDraughtCorrection();
		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.ifremer.globe.model.DataEditor#onClose()
	 */
	@PostConstruct
	public void createUI() {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.ifremer.globe.model.DataEditor#onClose()
	 */
	@PreDestroy
	public void onClose() {

	}

	/**
	 * @param summaryControler
	 *            the summaryControler to set
	 */
	public void setSummaryControler(SummaryController summaryControler) {
		this.summaryController = summaryControler;
	}

	/**
	 * @return the tableViewer
	 */
	public TableViewer getTableViewer() {
		return this.tableViewer;
	}
}
