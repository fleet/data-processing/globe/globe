package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.utils.image.Icons;
import gov.nasa.worldwind.layers.Layer;

/** Tree node for a NWW layer. */
public class LayerNode extends LayerBaseNode {

	/** Layer node display a specific short name **/
	private String shortName;

	/**
	 * Layer link to the node. <br>
	 * This link must be maintained even after the {@link #dispose} invocation
	 */
	private WeakReference<Layer> layer = new WeakReference<>(null);

	/**
	 * Listener for "Enabled" property of layer : shares the received event with node listeners to notify the update of
	 * check property.
	 **/
	private static final String LAYER_ENABLED_PROPERTY = "Enabled";
	private PropertyChangeListener enabledPropertyChangeListener = event -> pcs.firePropertyChange(PROPERTY_CHECKED,
			event.getOldValue(), event.getNewValue());

	/**
	 * Constructors
	 */
	public LayerNode(Layer layer) {
		super(layer.getName(), Icons.LAYER.toImage());
		setLayer(layer);
	}

	@Override
	public void delete() {
		getLayer().ifPresent(l -> {
			l.removePropertyChangeListener(enabledPropertyChangeListener);
			l.dispose();
		});
	}

	@Override
	public String getShortName() {
		return shortName;
	}

	/**
	 * @return an optional wrapping the layer associated to this node (could be empty: layer is not strongly referenced
	 *         by the node)
	 */
	public Optional<Layer> getLayer() {
		return Optional.ofNullable(layer != null ? layer.get() : null);
	}

	public void setLayer(Layer layer) {
		getLayer().ifPresent(l -> l.removePropertyChangeListener(enabledPropertyChangeListener));
		if (this.layer == null || this.layer.get() != layer) {
			this.layer = new WeakReference<>(layer);
			setName(layer.getName());
			if (layer instanceof IWWLayer iWWLayer) {
				this.shortName = iWWLayer.getShortName();
				this.image = iWWLayer.getIcon().orElse(Icons.LAYER.toImage());
			} else {
				this.shortName = name;
				this.image = Icons.LAYER.toImage();
			}
			setSavedCheckedState(layer.isEnabled());
			layer.addPropertyChangeListener(LAYER_ENABLED_PROPERTY, enabledPropertyChangeListener);
		}
	}

	@Override
	public CheckState getState() {
		return getLayer().filter(Layer::isEnabled).map(l -> CheckState.TRUE).orElse(CheckState.FALSE);
	}

	@Override
	public void setLayerEnabled(boolean enabled) {
		getLayer().ifPresent(l -> l.setEnabled(enabled));
	}

	@Override
	public List<Property<?>> getProperties() {
		var properties = new ArrayList<Property<?>>(super.getProperties());
		properties.add(Property.build("Layer type", getLayer().map(l -> l.getClass().getSimpleName()) //
				.orElse("No layer")));
		getLayer().filter(IInfos.class::isInstance).map(IInfos.class::cast)
				.ifPresent(info -> properties.addAll(info.getProperties()));
		return properties;
	}

}
