package fr.ifremer.globe.ui.views.projectexplorer;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import gov.nasa.worldwind.layers.Layer;

@Component(name = "globe_ui_worldwind_layer_node_parameters_composite_factory", service = IParametersViewCompositeFactory.class)
public class LayerNodeParamViewCompFactory implements IParametersViewCompositeFactory {

	/**
	 * If selection is {@link LayerNode}, @returns composites for the associated {@link Layer}.
	 */
	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		if (selection instanceof LayerNode layerNode) {
			var optLayer = layerNode.getLayer();
			if (optLayer.isPresent()) {
				var layerParameterComposite = IParametersViewService.grab().getComposites(layerNode.getLayer().get(),
						parent);
				if (!layerParameterComposite.isEmpty())
					return Optional.of(layerParameterComposite.get(0));
			}
		}
		return Optional.empty();
	}

}
