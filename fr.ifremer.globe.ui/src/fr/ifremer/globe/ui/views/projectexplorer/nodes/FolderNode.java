package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.utils.image.Icons;

public class FolderNode extends GroupNode {

	protected String comments;

	public FolderNode(String name, String comments) {
		super(name, Icons.FOLDER.toImage());
		this.comments = comments;
	}

	public FolderNode(String name, Image icon) {
		super(name, icon);
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public List<Property<?>> getProperties() {
		var properties = new ArrayList<>(super.getProperties());
		properties.add(Property.build("Group Name", name));
		Optional.ofNullable(comments).ifPresent(n -> properties.add(Property.build("Comments", comments)));
		return properties;
	}

}
