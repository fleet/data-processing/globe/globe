package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import gov.nasa.worldwind.globes.ElevationModel;

/**
 * Class for TreeNode accepting child
 *
 */
public class GroupNode extends TreeNode {

	public static final String PROPERTY_CHILDREN = "TREE_NODE_PROPERTY_CHILDREN";

	/*** Children nodes list. */
	private final CopyOnWriteArrayList<TreeNode> children = new CopyOnWriteArrayList<>();

	public GroupNode(String name, Image icon) {
		super(name, icon);
	}

	/**
	 * Add a child to the given node
	 *
	 * @return the child added
	 */
	public TreeNode addChild(TreeNode node) {
		return insertChild(node, getChildren().size());
	}

	/**
	 * Add a child to the given node at the given index
	 *
	 * @return the child added
	 */
	public TreeNode insertChild(TreeNode node, int index) {
		if (!getChildren().contains(node)) {
			var oldChildren = new ArrayList<>(children);
			if (index < getChildren().size() && index >= 0)
				children.add(index, node);
			else
				children.add(node);
			node.setParent(this);
			pcs.firePropertyChange(PROPERTY_CHILDREN, oldChildren, children);
		}
		return node;
	}

	/**
	 * Simply remove a child from the child list.
	 */
	public void removeChild(TreeNode node) {
		var oldChildren = new ArrayList<>(children);
		children.remove(node);
		pcs.firePropertyChange(PROPERTY_CHILDREN, oldChildren, children);
	}

	/**
	 * Remove all children from the child list.
	 */
	public void removeChildren() {
		var oldChildren = new ArrayList<>(children);
		children.clear();
		pcs.firePropertyChange(PROPERTY_CHILDREN, oldChildren, children);
	}

	public void sortChildren() {
		if (!children.isEmpty()) {
			var oldChildren = new ArrayList<>(children);
			Collections.sort(getChildren(), (o1, o2) -> o1.getName().compareTo(o2.getName()));
			pcs.firePropertyChange(PROPERTY_CHILDREN, oldChildren, children);
		}
	}

	@Override
	public void traverseDown(Consumer<TreeNode> visitor) {
		getChildren().forEach(child -> child.traverseDown(visitor));
		visitor.accept(this);
	}

	private boolean allChildStateMatch(CheckState triState) {
		return getChildren().stream().map(TreeNode::getState).allMatch(triState::equals);
	}

	/**
	 * @return true if all children are true, false if all children are false, else return partial.
	 */
	@Override
	public CheckState getState() {
		return getChildren().isEmpty() || allChildStateMatch(CheckState.FALSE) ? CheckState.FALSE
				: allChildStateMatch(CheckState.TRUE) ? CheckState.TRUE //
						: CheckState.PARTIAL;
	}

	@Override
	public void setChecked(boolean enabled, boolean directCall, Optional<Predicate<TreeNode>> filter) {
		var noneChildrenSelected = new AtomicBoolean(true);
		traverseDown(node -> noneChildrenSelected.compareAndSet(true, !node.isChecked()));

		// if set enabled, and none of children is selected or if only one child ==> force selection
		var forceSelection = (enabled && noneChildrenSelected.get()) || getChildren().size() == 1;

		getChildren().forEach(c -> c.setChecked(enabled, forceSelection, filter));
	}

	/**
	 * @return the {@link #children}
	 */
	public List<TreeNode> getChildren() {
		return children;
	}

	public boolean hasChildren() {
		return !children.isEmpty();
	}

	/** @return the first child {@link TreeNode} of the specified {@link Class} which passes the test. */
	public <T extends TreeNode> Optional<T> getChildNode(Class<T> clazz, Predicate<T> filter) {
		return getChildren().stream().filter(clazz::isInstance).map(clazz::cast).filter(filter::test).findFirst();
	}

	/** Searches the LayerNode holding the specified layer */
	public Optional<LayerNode> getLayerNode(IWWLayer wwLayer) {
		return getChildNode(LayerNode.class,
				layerNode -> layerNode.getLayer().isPresent() && (layerNode.getLayer().get() == wwLayer
						|| layerNode.getLayer().get().getName().equals(wwLayer.getName())));
	}

	/** Searches the ElevationNode holding the specified ElevationModel */
	public Optional<ElevationNode> getElevationNode(ElevationModel elevationModel) {
		return getChildNode(ElevationNode.class,
				elevationNode -> elevationNode.getElevationModel() != null
						&& (elevationModel == elevationNode.getElevationModel()
								|| elevationModel.getName().equals(elevationNode.getElevationModel().getName())));
	}

}
