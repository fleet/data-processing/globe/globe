package fr.ifremer.globe.ui.views.projectexplorer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;

import fr.ifremer.globe.core.model.AbstractModelPublisher;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel.ProjectExplorerEvent;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

/**
 * This class is the model of the {@link ProjectExplorer} view.
 */
@Creatable
@Singleton
public class ProjectExplorerModel extends AbstractModelPublisher<String, TreeNode, ProjectExplorerEvent> {

	public static final String PROJECT_EXPLORER_PART_ID = "fr.ifremer.globe.ui.projectExplorer";

	@Inject
	@org.eclipse.e4.core.di.annotations.Optional // Optional for test purpose
	private ESelectionService selectionService;

	// Root nodes
	private final GroupNode backGroundGroupNode = new GroupNode("Background", Icons.BACKGROUND.toImage());
	private final GroupNode wMSGroupNode = new GroupNode("WMS", Icons.WMS.toImage());
	private final FolderNode dataGroupNode = new FolderNode("Data", Icons.DATABASE.toImage());

	@PostConstruct
	private void init() {
		add(backGroundGroupNode);
		add(wMSGroupNode);
		dataGroupNode.setExpanded(true); // always expand "Data" node
		add(dataGroupNode);
	}

	@Override
	protected String getKey(TreeNode treeNode) {
		return treeNode.getPath();
	}

	@Override
	public boolean add(String key, TreeNode treeNode) {
		var result = super.add(key, treeNode);
		if (result) {
			// add child node & set property change listeners
			Optional.ofNullable(elements.get(key)).ifPresent(newNode -> {
				addPropertyChangeListener(newNode);
				newNode.traverseDown(this::add);
			});
		}
		return result;
	}

	@Override
	public Optional<TreeNode> remove(String key) {
		var optRemovedNode = super.remove(key);
		// remove child nodes & property change listeners
		optRemovedNode.ifPresent(removedNode -> {
			removePropertyChangeListener(removedNode);
			removedNode.traverseDown(this::removeElement);
		});
		return optRemovedNode;
	}

	/**
	 * @return root nodes (= node without parents)
	 */
	public Set<TreeNode> getRootNodes() {
		// build the set with a specific order
		var result = new LinkedHashSet<TreeNode>();
		result.add(backGroundGroupNode);
		result.add(wMSGroupNode);
		elements.values().stream().filter(node -> node.getParent().isEmpty()) // root == no parent
				.filter(node -> node != backGroundGroupNode && node != wMSGroupNode && node != dataGroupNode)
				.forEach(result::add);
		result.add(dataGroupNode);
		return result;
	}

	public GroupNode getBackgroundNode() {
		return backGroundGroupNode;
	}

	public void addBackgroundNode(TreeNode node) {
		backGroundGroupNode.addChild(node);
		notifyUpdate(backGroundGroupNode);
	}

	public GroupNode getwMSGroupNode() {
		return wMSGroupNode;
	}

	public GroupNode getDataNode() {
		return dataGroupNode;
	}

	public List<TreeNode> getSelectedNodes() {
		return selectionService.getSelection(PROJECT_EXPLORER_PART_ID) instanceof StructuredSelection selection ? Arrays
				.stream(selection.toArray()).filter(TreeNode.class::isInstance).map(TreeNode.class::cast).toList()
				: Collections.emptyList();
	}

	/**
	 * @return the TreeNode with the provided {@link UUID} if exists in "Data" {@link GroupNode}.
	 */
	public Optional<TreeNode> getNode(UUID uid) {
		return getAll().stream().filter(treeNode -> treeNode.getUid().equals(uid)).findFirst();
	}

	/**
	 * @return the {@link FileInfoNode} associated with the provided {@link IFileInfo} if exists.
	 */
	public Optional<FileInfoNode> getFileInfoNode(IFileInfo fileInfo) {
		return getAll().stream().filter(FileInfoNode.class::isInstance).map(FileInfoNode.class::cast)
				.filter(node -> node.getFileInfo().equals(fileInfo)
						|| node.getFileInfo().getAbsolutePath().equals(fileInfo.getAbsolutePath()))
				.findAny();
	}

	/**
	 * @return the {@link FileInfoNode} associated with the provided file path if exists.
	 */
	public Optional<FileInfoNode> getFileInfoNode(String filePath) {
		return getAll().stream().filter(FileInfoNode.class::isInstance).map(FileInfoNode.class::cast)
				.filter(node -> node.getFileInfo().getAbsolutePath().equals(filePath)).findAny();
	}

	public Optional<TreeNode> findNode(String path) {
		return find(node -> node.getPath().equals(path));
	}

	public Optional<GroupNode> findFolderNode(String path) {
		return findNode(path).filter(GroupNode.class::isInstance).map(GroupNode.class::cast);
	}

	public void moveNodes(List<? extends TreeNode> nodes, GroupNode target, boolean onTop) {
		nodes.stream().filter(node -> node != target && !node.getParent().equals(Optional.of(target))
				&& !node.testDown(childNode -> childNode == target)).forEach(node -> {
					// remove from previous parent
					node.getParent().ifPresent(p -> p.removeChild(node));
					// insert or add to new parent
					if (onTop)
						target.insertChild(node, 0);
					else
						target.addChild(node);
				});
		notifyUpdate();
	}

	public void moveBeforeAfterNode(List<GroupNode> groupNodes, TreeNode target, boolean before) {
		int index = target.getParent().map(p -> p.getChildren().indexOf(target)).orElse(-1);
		if (index == -1)
			return;
		groupNodes.stream().filter(node -> node != target && !node.getParent().equals(Optional.of(target)))
				.forEach(node -> {
					// remove from previous parent
					node.getParent().ifPresent(p -> p.removeChild(node));
					// insert or add to new parent
					target.getParent().get().insertChild(node, before ? index : index + 1);
				});
		notifyUpdate();
	}

	// Events

	/** Event types **/
	public enum ProjectExplorerEventType {
		ADD, UPDATE, SELECTION, REMOVE
	}

	/**
	 * Event is a record defined by a {@link ProjectExplorerEventType} and a list of {@link TreeNode}
	 */
	public record ProjectExplorerEvent(ProjectExplorerEventType type, List<TreeNode> element) {
	}

	public void notifyUpdate() {
		submit(new ProjectExplorerEvent(ProjectExplorerEventType.UPDATE, Collections.emptyList()));
	}

	public void notifyUpdate(TreeNode node) {
		newUpdateEvent(node).ifPresent(this::submit);
	}

	public void setSelection(TreeNode node) {
		setSelection(List.of(node));
	}

	public void setSelection(List<TreeNode> node) {
		newSelectionEvent(node).ifPresent(this::submit);
	}

	@Override
	protected Optional<ProjectExplorerEvent> newAddEvent(TreeNode element) {
		return Optional.of(new ProjectExplorerEvent(ProjectExplorerEventType.ADD, List.of(element)));
	}

	@Override
	protected Optional<ProjectExplorerEvent> newUpdateEvent(TreeNode element) {
		return Optional.of(new ProjectExplorerEvent(ProjectExplorerEventType.UPDATE, List.of(element)));
	}

	protected Optional<ProjectExplorerEvent> newSelectionEvent(List<TreeNode> elements) {
		return Optional.of(new ProjectExplorerEvent(ProjectExplorerEventType.SELECTION, elements));
	}

	@Override
	protected Optional<ProjectExplorerEvent> newRemovedEvent(TreeNode element) {
		return Optional.of(new ProjectExplorerEvent(ProjectExplorerEventType.REMOVE, List.of(element)));
	}

	/// PROPERTY CHANGE LISTENERS (used to observe nodes properties)

	private PropertyChangeListener treeNodeListener = event -> notifyUpdate((TreeNode) event.getSource());
	private PropertyChangeListener groupNodeListener = this::onGroupNodeChanged;

	private void addPropertyChangeListener(TreeNode node) {
		node.addPropertyChangeListener(TreeNode.PROPERTY_NAME, treeNodeListener);
		node.addPropertyChangeListener(TreeNode.PROPERTY_CHECKED, treeNodeListener);
		if (node instanceof GroupNode)
			node.addPropertyChangeListener(GroupNode.PROPERTY_CHILDREN, groupNodeListener);
	}

	private void removePropertyChangeListener(TreeNode node) {
		node.removePropertyChangeListener(TreeNode.PROPERTY_NAME, treeNodeListener);
		node.removePropertyChangeListener(TreeNode.PROPERTY_CHECKED, treeNodeListener);
		if (node instanceof GroupNode)
			node.removePropertyChangeListener(GroupNode.PROPERTY_CHILDREN, groupNodeListener);
	}

	/**
	 * Called when group's children change.
	 */
	@SuppressWarnings("unchecked") // should not happen
	private void onGroupNodeChanged(PropertyChangeEvent evt) {
		var oldChildren = (List<TreeNode>) evt.getOldValue();
		var newChildren = (List<TreeNode>) evt.getNewValue();

		// remove obsolete child from model
		for (var child : oldChildren)
			if (!newChildren.contains(child))
				removeElement(child);

		// add new child to model
		for (var child : newChildren)
			if (!oldChildren.contains(child))
				add(child);
		
		// notify update (useful if only the order has changed)
		notifyUpdate((TreeNode) evt.getSource());
	}

}