package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.util.Optional;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.utils.image.Icons;

public class TreeNodeLabelProvider extends CellLabelProvider {

	public String getText(Object element) {
		return element instanceof TreeNode treeNode ? treeNode.getShortName() : "unkonwn";
	}

	public Image getImage(Object element) {
		return Optional.of(element).filter(TreeNode.class::isInstance).map(TreeNode.class::cast).map(TreeNode::getImage)
				.filter(image -> !image.isDisposed()).orElse(Icons.READING_ERROR.toImage());
	}

	@Override
	public String getToolTipText(Object element) {
		return element instanceof TreeNode treeNode ? treeNode.getToolTip().orElse(null) : null;
	}

	@Override
	public int getToolTipDisplayDelayTime(Object object) {
		return 2000;
	}

	@Override
	public void update(ViewerCell cell) {
		cell.setText(getText(cell.getElement()));
		cell.setImage(getImage(cell.getElement()));
	}

}
