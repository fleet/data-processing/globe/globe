package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import fr.ifremer.globe.ui.utils.image.Icons;
import gov.nasa.worldwind.globes.ElevationModel;

/** Tree node for a NWW layer. */
public class ElevationNode extends LayerBaseNode {

	private ElevationModel elevation;

	public ElevationNode(ElevationModel elevation) {
		super(elevation.getName(), Icons.ELEVATION.toImage());
		setElevationModel(elevation);
	}

	@Override
	public String getShortName() {
		return "Elevation model";
	}

	public ElevationModel getElevationModel() {
		return elevation;
	}

	public void setElevationModel(ElevationModel elevationModel) {
		elevation = elevationModel;
		setSavedCheckedState(elevation.isEnabled());
	}

	@Override
	public CheckState getState() {
		return elevation != null && elevation.isEnabled() ? CheckState.TRUE : CheckState.FALSE;
	}

	@Override
	public void setLayerEnabled(boolean enabled) {
		elevation.setEnabled(enabled);
	}

}
