package fr.ifremer.globe.ui.views.projectexplorer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.e4.ui.di.UIEventTopic;

import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoEvent.FileInfoState;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.event.WWLayerEvent;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode.LoadedState;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

/**
 * This class updates {@link ProjectExplorerModel} with events coming from other models ({@link FileInfoModel},
 * {@link WWFileLayerStoreModel}...).
 */
public class ProjectExplorerController {

	@Inject
	private ProjectExplorerModel model;
	@Inject
	private FileInfoModel fileInfoModel;
	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;
	@Inject
	private ITreeNodeFactory treeNodeFactory;

	@PostConstruct
	public void init() {
		fileInfoModel.addListener(this::onFileInfoEvent);
		fileLayerStoreModel.addListener(this::onFileLayerStoreEvent);
	}

	/**
	 * Reacts on {@link FileInfoEvent}.
	 */
	private void onFileInfoEvent(FileInfoEvent event) {
		var fileInfo = event.getfileInfo();
		var optFileInfoNode = model.getFileInfoNode(fileInfo);

		if (optFileInfoNode.isEmpty() && event.getState() == FileInfoState.LOADING)
			optFileInfoNode = Optional.of(ITreeNodeFactory.grab().createFileInfoNode(event.getfileInfo()));

		optFileInfoNode.ifPresent(node -> {
			// update node FileInfo if needed (an obsolete FileInfo (associated to the same) file may have to be
			// removed by new one)
			if (node.getFileInfo() != event.getfileInfo())
				node.setFileInfo(fileInfo);

			switch (event.getState()) {
			case LOADING -> node.setLoadStatus(LoadedState.LOADING);
			case LOADED -> node.setLoadStatus(LoadedState.LOADED);
			case ERROR -> node.setLoadStatus(LoadedState.READING_ERROR);
			case REMOVED -> model.removeElement(node);
			case RELOADING, UNLOADED -> unloadFileInfoNode(node, fileInfo);
			}
			model.notifyUpdate(node);
		});
	}

	/**
	 * Reacts on {@link WWFileLayerStoreEvent}.
	 */
	protected void onFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		var fileLayerStore = event.getFileLayerStore();
		var fileInfo = fileLayerStore.getFileInfo();

		model.getFileInfoNode(fileInfo).ifPresent(node -> {
			switch (event.getState()) {
			case ADDED -> treeNodeFactory.createOrUpdateLayerNodes(node, fileLayerStore);
			case REMOVED -> unloadFileInfoNode(node, fileInfo);
			case UPDATED -> {
				var updatedLayers = event.getUpdatedLayers();
				if (updatedLayers != null) {
					// if updated layer is not more contained in layer store: remove its node
					updatedLayers.stream()//
							.filter(layer -> !fileLayerStore.getLayers().contains(layer))//
							.forEach(wwLayer -> node.getLayerNode(wwLayer).ifPresent(node::removeChild));

					var layerAdded = treeNodeFactory.createOrUpdateLayerNodes(node, fileLayerStore);
					if (!layerAdded && !updatedLayers.isEmpty()) // no node created : a layer has been replaced
						node.getLayerNode(updatedLayers.get(0)).ifPresent(model::setSelection);
				}
			}
			}
			model.notifyUpdate(node);
		});
	}

	private void unloadFileInfoNode(FileInfoNode fileInfoNode, IFileInfo fileInfo) {
		fileInfoNode.setFileInfo(new BasicFileInfo(fileInfo.getPath())); // release FileInfo
		fileInfoNode.setLoadStatus(LoadedState.UNLOADED);
	}

	/**
	 * Handles layer selection events.
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onLayerSelected(@UIEventTopic(WWLayerEvent.TOPIC_LAYER_SELECTED) IWWLayer... layers) {
		// compute node to select
		var nodeToSelect = List.of(layers).stream()
				.map(layer -> model.find(
						node -> node instanceof LayerNode layerNode && layerNode.getLayer().equals(Optional.of(layer))))
				.filter(Optional::isPresent).map(Optional::get) //
				.map(TreeNode::getParent).filter(Optional::isPresent).map(Optional::get).map(TreeNode.class::cast)
				.toList();

		// select nodes & refresh tree
		model.setSelection(nodeToSelect);
	}

	/**
	 * Handles layer selection events.
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onFileInfosSelected(@UIEventTopic(FileInfoEvent.EVENT_UI_FILES_SELECTED) IFileInfo... fileInfos) {
		// compute node to select
		var nodeToSelect = new ArrayList<TreeNode>();
		model.getDataNode().traverseDown(node -> {
			if (node instanceof FileInfoNode fileInfoNode
					&& Arrays.stream(fileInfos).anyMatch(fileInfo -> fileInfo.equals(fileInfoNode.getFileInfo()))) {
				nodeToSelect.add(node);
			}
		});
		// select nodes & refresh tree
		model.setSelection(nodeToSelect);
	}

}
