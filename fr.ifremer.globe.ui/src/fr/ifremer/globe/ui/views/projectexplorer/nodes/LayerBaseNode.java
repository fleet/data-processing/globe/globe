package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.util.Optional;
import java.util.function.Predicate;

import org.eclipse.swt.graphics.Image;

/**
 * Base class for nodes associated to a worldwind layer.
 */
public abstract class LayerBaseNode extends TreeNode {

	/**
	 * Property to save the node state ; allows to react differently if check is direct or inherited
	 */
	private boolean savedCheckedState;

	/**
	 * Constructor
	 */
	protected LayerBaseNode(String name, Image icon) {
		super(name, icon);
	}

	protected void setSavedCheckedState(boolean b) {
		this.savedCheckedState = b;
	}

	@Override
	public boolean isChecked() {
		return savedCheckedState || this.getState() == CheckState.TRUE;
	}

	/**
	 * Set layer enabled if this node and its parents are both selected.
	 */
	@Override
	public void setChecked(boolean checked, boolean directCall, Optional<Predicate<TreeNode>> filter) {
		// apply filter : if false, do nothing
		if (filter.isPresent() && !filter.get().test(this))
			return;

		// do not enable layer if indirect selection (inherited from parent) and previous direct selection false
		if (!directCall && !isChecked())
			return;

		var previousSavedCheckedState = savedCheckedState;

		setLayerEnabled(checked);

		// direct selection : update saved checked state, else keep the previous state
		savedCheckedState = directCall ? checked : previousSavedCheckedState;
	}

	protected abstract void setLayerEnabled(boolean enabled);
}
