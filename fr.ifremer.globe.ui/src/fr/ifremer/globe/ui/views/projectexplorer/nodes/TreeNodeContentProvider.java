package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import org.eclipse.jface.viewers.ITreeContentProvider;

public class TreeNodeContentProvider implements ITreeContentProvider {

	@Override
	public Object getParent(Object element) {
		return ((TreeNode) element).getParent().orElse(null);
	}

	@Override
	public boolean hasChildren(Object element) {
		return element instanceof GroupNode groupNode && groupNode.hasChildren();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof GroupNode groupNode)
			return groupNode.getChildren().toArray();
		else if (parentElement instanceof Object[] objects)
			return objects;
		return null;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

}
