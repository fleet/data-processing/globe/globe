/*
 * @License@
 */
package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.IDeletable;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;

public abstract class TreeNode implements IInfos, IDeletable {

	public enum CheckState {
		TRUE, FALSE, PARTIAL
	}

	/** Node properties **/
	private final UUID uid;
	protected String name;
	protected Image image;
	protected boolean expanded = false;

	/** List of properties for IInfos */
	protected List<Property<?>> properties = new ArrayList<>();

	/** Image of this node */

	/** Parent node. */
	protected Optional<GroupNode> parent = Optional.empty();

	/**
	 * Constructor
	 */
	protected TreeNode(String name, Image icon) {
		this.uid = UUID.randomUUID();
		this.name = name;
		this.image = icon;
	}

	// GETTERS & SETTERS

	public UUID getUid() {
		return uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		var oldName = this.name;
		this.name = name;
		pcs.firePropertyChange(PROPERTY_NAME, oldName, this.name);
	}

	public String getShortName() {
		return getName();
	}

	public Image getImage() {
		return image;
	}

	public Optional<String> getToolTip() {
		return Optional.empty();
	}

	public Optional<GroupNode> getParent() {
		return parent;
	}

	public void setParent(GroupNode parent) {
		this.parent = Optional.of(parent);
	}

	/**
	 * Check if the given node visible status
	 * <p>
	 * {@link CheckState#TRUE} node is visible (and all its descendants)
	 * <p>
	 * {@link CheckState#FALSE} node is not visible (and all its descendants)
	 * <p>
	 * {@link CheckState#PARTIAL} at least one of the descendant is not loaded
	 */
	public abstract CheckState getState();

	/**
	 * @return true if this node is enabled, by default equals to {@link CheckState} == TRUE (! not for LayerBaseNode)
	 */
	public boolean isChecked() {
		return getState() == CheckState.TRUE;
	}

	/**
	 * Set the node checked
	 */
	public void setChecked(boolean checked) {
		boolean oldValue = isChecked();
		setChecked(checked, true, Optional.empty());
		pcs.firePropertyChange(PROPERTY_CHECKED, oldValue, isChecked());
	}

	/**
	 * Sets this node checked.
	 * 
	 * @param enabled : new enabled state
	 * @param directCall : true if this method is directly called from user selection, false if it's from recursive call
	 * @param filter : filter to apply on node to determinate if it have to be updated
	 */
	public abstract void setChecked(boolean checked, boolean directCall, Optional<Predicate<TreeNode>> filter);

	/**
	 * Visitor pattern, visit this node and all its children (if there is some)
	 */
	public void traverseDown(Consumer<TreeNode> visitor) {
		visitor.accept(this);
	}

	/**
	 * Apply a test by traversing down.
	 * 
	 * @return true if one of child nodes passed the test.
	 */
	public boolean testDown(Predicate<TreeNode> predicate) {
		AtomicBoolean result = new AtomicBoolean();
		traverseDown(node -> result.compareAndSet(false, predicate.test(node)));
		return result.get();
	}

	/**
	 * Visitor pattern, visit this node and all its parents (if there is some)
	 */
	public void traverseUp(Consumer<TreeNode> visitor) {
		visitor.accept(this);
		parent.ifPresent(p -> p.traverseUp(visitor));
	}

	/**
	 * Visitor pattern, visit this node and all its children and parents (if there is some)
	 */
	public void traverseUpAndDown(Consumer<TreeNode> visitor) {
		traverseUp(visitor);
		traverseDown(visitor);
	}

	public TreeNode findRootNode() {
		return parent.map(TreeNode::findRootNode).orElse(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final boolean delete(IProgressMonitor monitor) {
		delete();
		return true;
	}

	/**
	 * delete the given node and its subtree
	 */
	public void delete() {
		parent.ifPresent(p -> p.removeChild(this));
	}

	/**
	 * @return the {@link #properties}
	 */
	@Override
	public List<Property<?>> getProperties() {
		return properties;
	}

	/**
	 * @param properties the {@link #properties} to set
	 */
	public void setProperties(List<Property<?>> properties) {
		this.properties = properties;
	}

	/**
	 * @return the path of this node like "root_name/parent_name/" ... "/parent_name/name_of_this"
	 */
	public String getPath() {
		return parent.map(GroupNode::getPath).map(parentPath -> parentPath + '/' + name).orElse(name);
	}

	/**
	 * @return the {@link #expanded}
	 */
	public boolean isExpanded() {
		return expanded;
	}

	/**
	 * @param expanded the {@link #expanded} to set
	 */
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	// PROPERTY CHANGE LISTERNER

	public static final String PROPERTY_NAME = "TREE_NODE_PROPERTY_NAME";
	public static final String PROPERTY_CHECKED = "TREE_NODE_PROPERTY_CHECKED";

	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(propertyName, listener);
	}

}
