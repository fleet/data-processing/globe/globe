package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.eclipse.swt.graphics.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.service.icon.IImageService;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Node matching an FileInfoNode
 */
public class FileInfoNode extends GroupNode {

	protected static Logger logger = LoggerFactory.getLogger(FileInfoNode.class);

	/** The file */
	protected IFileInfo fileInfo;

	/** Loading status */
	public enum LoadedState {
		LOADED, UNLOADED, LOADING, NOT_FOUND, READING_ERROR;

		@Override
		public String toString() {
			return switch (this) {
			case LOADED -> "Loaded";
			case UNLOADED -> "Not loaded";
			case LOADING -> "Loading";
			case READING_ERROR -> "Reading error";
			case NOT_FOUND -> "File not found";
			};
		}
	}

	private LoadedState loadedState = LoadedState.UNLOADED;

	/**
	 * Constructor
	 */
	public FileInfoNode(IFileInfo fileInfo) {
		super(fileInfo.getFilename(), Icons.FILE.toImage());
		setFileInfo(fileInfo);
	}

	/**
	 * Constructor
	 */
	public FileInfoNode(String filePath) {
		this(new BasicFileInfo(filePath));
	}

	public IFileInfo getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(IFileInfo fileInfo) {
		this.fileInfo = fileInfo;
		setName(fileInfo.getFilename());
	}

	public File getFile() {
		return fileInfo.toFile();
	}

	@Override
	public Image getImage() {
		return switch (loadedState) {
		case LOADED -> Optional.ofNullable(fileInfo).flatMap(IImageService.grab()::getIcon16ForFile)
				.orElse(Icons.BROKEN.toImage());
		case UNLOADED -> Icons.UNLOADED.toImage();
		case LOADING -> Icons.LOADING.toImage();
		case READING_ERROR -> Icons.READING_ERROR.toImage();
		case NOT_FOUND -> Icons.BROKEN.toImage();
		};
	}

	@Override
	public List<Property<?>> getProperties() {
		List<Property<?>> myproperties = new LinkedList<>();
		myproperties.addAll(super.getProperties());
		myproperties.add(Property.build("Load status",
				getLoadedState().toString() + " (" + fileInfo.getClass().getSimpleName() + "/" + fileInfo.getContentType() + ")"));
		myproperties.add(Property.build("File", getFile().getPath()));
		if (!getFile().exists()) {
			myproperties.add(Property.build("Error", "File does not exist"));
		}
		List<Property<?>> listProperties = fileInfo.getProperties();
		if (listProperties != null) {
			myproperties.addAll(listProperties);
		}
		return myproperties;
	}

	public LoadedState getLoadedState() {
		return loadedState;
	}

	/**
	 * @param loadStatus the {@link #loadedState} to set
	 */
	public void setLoadStatus(LoadedState loadStatus) {
		loadedState = loadStatus;
		if (loadStatus == LoadedState.UNLOADED || loadStatus == LoadedState.NOT_FOUND)
			removeChildren();
	}

	/** Return true when underlying data have to be saved */
	public boolean isDirty() {
		return false;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) || //
				obj instanceof FileInfoNode fileInfoNode
						&& fileInfoNode.getFileInfo().toFile().equals(getFileInfo().toFile());
	}

}
