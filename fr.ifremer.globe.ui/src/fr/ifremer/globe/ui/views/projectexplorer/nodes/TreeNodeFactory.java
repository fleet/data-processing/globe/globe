/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.views.projectexplorer.nodes;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import jakarta.inject.Inject;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;

/** Visitor of IWWLayer to create an instance of LayerNode according the layer type */
@Component(name = "globe_ui_treenode_factory", service = ITreeNodeFactory.class)
public class TreeNodeFactory implements ITreeNodeFactory {

	@Inject
	protected ProjectExplorerModel projectExplorerModel;

	/// FOLDER NODES
	@Override
	public FolderNode createOrphanFolderNode(String name, String comments) {
		return new FolderNode(name, comments);
	}

	@Override
	public FolderNode createFolderNode(GroupNode parentNode, String name, String comments) {
		FolderNode result = createOrphanFolderNode(name, comments);
		parentNode.addChild(result);
		return result;
	}

	/// FILE NODES

	@Override
	public FileInfoNode createFileInfoNode(String filePath, String targetNodePath) {
		return createFileInfoNode(new BasicFileInfo(filePath), projectExplorerModel.findNode(targetNodePath),
				Optional.empty());
	}

	@Override
	public FileInfoNode createFileInfoNode(IFileInfo fileInfo, Optional<? extends TreeNode> optTargetNode,
			Optional<Integer> optTargetIndex) {
		// If node already exist for this file info : return it.
		var existingFileInfoNode = projectExplorerModel.getFileInfoNode(fileInfo);
		if (existingFileInfoNode.isPresent())
			return existingFileInfoNode.get();

		var fileInfoNode = new FileInfoNode(fileInfo);
		if (optTargetNode.isEmpty())
			// No target node : add the new FileInfoNode in default data group.
			projectExplorerModel.getDataNode().addChild(fileInfoNode);
		else {
			if (optTargetNode.get() instanceof FolderNode folderNode) {
				// Add the new FileInfoNode into the target FolderNode.
				optTargetIndex.ifPresentOrElse(index -> folderNode.insertChild(fileInfoNode, index),
						() -> folderNode.addChild(fileInfoNode));
			} else if (optTargetNode.get() instanceof FileInfoNode targetFileInfoNode) {
				// Add the new FileInfoNode after the target FileInfoNode.
				var parentNode = targetFileInfoNode.getParent().get();
				int index = parentNode.getChildren().indexOf(fileInfoNode);
				parentNode.insertChild(fileInfoNode, index);
			}
		}
		return fileInfoNode;
	}

	/// LAYER NODES

	@Override
	public LayerNode createNode(IWWLayer layer) {
		return new LayerNode(layer);
	}

	@Override
	public boolean createOrUpdateLayerNodes(GroupNode groupNode, WWFileLayerStore fileLayerStore) {
		var newLayerAdded = new AtomicBoolean();
		var elevationModelAdded = false;
		for (IWWLayer wwLayer : fileLayerStore.getLayers()) {
			if (wwLayer != null) {
				groupNode.getLayerNode(wwLayer).ifPresentOrElse(
						// same node exists (from the session restoration)
						sameLayerNode -> sameLayerNode.setLayer(wwLayer),
						// or create new node
						() -> {
							LayerNode createdLayerNode = createNode(wwLayer);
							groupNode.addChild(createdLayerNode);
							newLayerAdded.set(true);
						});

				// add elevation model (in second place)
				if (!elevationModelAdded) {
					var elevationModel = fileLayerStore.getElevationModel();
					if (elevationModel != null)
						groupNode.getElevationNode(elevationModel).ifPresentOrElse(
								// same node exists (from the session restoration)
								sameElevationNode -> sameElevationNode.setElevationModel(elevationModel),
								// or create new node
								() -> {
									ElevationNode elevationNode = new ElevationNode(elevationModel);
									groupNode.addChild(elevationNode).setChecked(elevationModel.isEnabled());
								});
					elevationModelAdded = true;
				}
			}
		}
		return newLayerAdded.get();
	}

}
