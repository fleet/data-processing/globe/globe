/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.views.projectexplorer.tester;

import org.eclipse.core.expressions.PropertyTester;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode.LoadedState;

/**
 * PropertyTester for testing some Property on FileInfoNode using the eclipse.core.expressions api
 */
public class FileInfoNodeTester extends PropertyTester {

	/**
	 * @see org.eclipse.core.expressions.IPropertyTester#test(java.lang.Object, java.lang.String, java.lang.Object[],
	 *      java.lang.Object)
	 */
	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		switch (property) {
		case "LoadedState":
			LoadedState expectedState = LoadedState.valueOf(expectedValue.toString());
			return ((FileInfoNode) receiver).getLoadedState() == expectedState;
		case "contentType":
			ContentType expectedContentType = ContentType.valueOf(expectedValue.toString());
			return ((FileInfoNode) receiver).getFileInfo().getContentType() == expectedContentType;
		default:
			return false;
		}
	}

}
