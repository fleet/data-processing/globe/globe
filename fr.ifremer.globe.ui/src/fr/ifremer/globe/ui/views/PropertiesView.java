package fr.ifremer.globe.ui.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.nebula.widgets.xviewer.XViewer;
import org.eclipse.nebula.widgets.xviewer.XViewerFactory;
import org.eclipse.nebula.widgets.xviewer.XViewerLabelProvider;
import org.eclipse.nebula.widgets.xviewer.core.model.SortDataType;
import org.eclipse.nebula.widgets.xviewer.core.model.XViewerAlign;
import org.eclipse.nebula.widgets.xviewer.core.model.XViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IInfos;
import fr.ifremer.globe.core.model.properties.Property;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

public class PropertiesView {

	protected Logger logger = LoggerFactory.getLogger(PropertiesView.class);

	private final Composite parent;

	private XViewer viewer;

	public static final int COLUMN_COUNT = 10;
	public static final String PROPERTY_COLUMN_NAME = "Property";
	public static final String VALUE_COLUMN_NAME = "Values";
	public final static String COLUMN_NAMESPACE = PropertiesView.class.getName();
	public final static Object[] EMPTY_ARRAY = new Object[0];

	protected final static XViewerColumn PROPERTY_COLUMN = new XViewerColumn(PROPERTY_COLUMN_NAME, PROPERTY_COLUMN_NAME,
			80, XViewerAlign.Left, true, SortDataType.String, false, null);
	protected final static XViewerColumn VALUES_COLUMN = new XViewerColumn(VALUE_COLUMN_NAME, VALUE_COLUMN_NAME, 80,
			XViewerAlign.Left, true, SortDataType.String, false, null);

	/** Possible columns in the wiewer. */
	protected List<TreeViewerColumn> columns = new ArrayList<>();

	/** PropertiesXViewerFactory dedicated to the viewer. */
	protected PropertiesXViewerFactory propertiesXViewerFactory;

	@Inject
	FileInfoModel fileInfoModel;

	/** Current selection **/
	private Optional<IInfos> currentSelection;

	@Inject
	public PropertiesView(Composite parent) {
		this.parent = parent;
	}

	@PostConstruct
	public void createUI() {
		final FillLayout layout = new FillLayout();
		parent.setLayout(layout);

		propertiesXViewerFactory = new PropertiesXViewerFactory();
		viewer = new XViewer(parent, SWT.FULL_SELECTION, propertiesXViewerFactory);
		viewer.setContentProvider(new PropertiesContentProvider());
		viewer.setLabelProvider(new PropertiesLabelProvider(viewer));
		Tree tree = viewer.getTree();
		tree.setLayoutData(new GridData(GridData.FILL_BOTH));

		fileInfoModel.addListener(this::onFileInfoEvent);
	}

	/**
	 * Compute column's width.
	 */
	private void repack() {
		Tree tree = (Tree) viewer.getControl();
		parent.getDisplay().asyncExec(() -> Arrays.stream(tree.getColumns()).filter(column -> column.getWidth() > 0)
				.forEach(TreeColumn::pack));
	}

	/**
	 * Invoked when selection in Project Explorer has changed
	 */
	@Inject
	public void onSelection(@Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection selection) {
		this.currentSelection = Optional.ofNullable(selection).map(StructuredSelection::getFirstElement)
				.filter(IInfos.class::isInstance).map(IInfos.class::cast);
		refresh();
	}

	/**
	 * Asks to refresh if current selection has been updated.
	 */
	private void onFileInfoEvent(FileInfoEvent event) {
		currentSelection.filter(selection -> selection == event.getfileInfo() || (selection instanceof FileInfoNode
				&& ((FileInfoNode) selection).getFileInfo() == event.getfileInfo())).ifPresent(s -> refresh());
	}

	/**
	 * Refreshes the view
	 */
	private void refresh() {
		if (viewer != null)
			currentSelection.map(IInfos::getProperties)
					.ifPresentOrElse(propertiesList -> Display.getDefault().asyncExec(() -> {
						viewer.setInput(propertiesList.toArray());
						repack();
					}), /* else : remove contents */ () -> Display.getDefault().asyncExec(() -> viewer.setInput(null)));
	}

	/**
	 * LabelProvider of the view.
	 */
	class PropertiesLabelProvider extends XViewerLabelProvider {

		public PropertiesLabelProvider(XViewer viewer) {
			super(viewer);
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
		 */
		@Override
		public void addListener(ILabelProviderListener listener) {

		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
		 */
		@Override
		public void dispose() {
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
		 */
		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
		 */
		@Override
		public void removeListener(ILabelProviderListener listener) {
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerLabelProvider#getColumnImage(java.lang.Object,
		 *      org.eclipse.nebula.widgets.xviewer.core.model.XViewerColumn, int)
		 */
		@Override
		public Image getColumnImage(Object element, XViewerColumn xCol, int columnIndex) throws Exception {
			return null;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerLabelProvider#getColumnText(java.lang.Object,
		 *      org.eclipse.nebula.widgets.xviewer.core.model.XViewerColumn, int)
		 */
		@Override
		public String getColumnText(Object element, XViewerColumn xColumn, int columnIndex) throws Exception {
			if (!(element instanceof Property<?>)) {
				return "";
			}
			Property<?> property = (Property<?>) element;
			if (xColumn.equals(PROPERTY_COLUMN)) {
				return property.getKey();
			}
			return property.getValueAsString();
		}
	}

	/**
	 * Viewer configuration.
	 */
	class PropertiesXViewerFactory extends XViewerFactory {

		/**
		 * Constructor.
		 */
		public PropertiesXViewerFactory() {
			super(COLUMN_NAMESPACE);
			registerColumns(PROPERTY_COLUMN, VALUES_COLUMN);
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerFactory#getCustomizeDialog(org.eclipse.nebula.widgets.xviewer.XViewer)
		 */
		@Override
		public Dialog getCustomizeDialog(XViewer xViewer) {
			return null;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.IXViewerFactory#isAdmin()
		 */
		@Override
		public boolean isAdmin() {
			return false;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerFactory#isSearhTop()
		 */
		@Override
		public boolean isSearhTop() {
			return false;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerFactory#isFilterUiAvailable()
		 */
		@Override
		public boolean isFilterUiAvailable() {
			return false;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerFactory#isHeaderBarAvailable()
		 */
		@Override
		public boolean isHeaderBarAvailable() {
			return false;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerFactory#isLoadedStatusLabelAvailable()
		 */
		@Override
		public boolean isLoadedStatusLabelAvailable() {
			return false;
		}

		/**
		 * Follow the link.
		 *
		 * @see org.eclipse.nebula.widgets.xviewer.XViewerFactory#isSearchUiAvailable()
		 */
		@Override
		public boolean isSearchUiAvailable() {
			return false;
		}

	}

	/**
	 * The ITreeContentProvider of wiewer.
	 */
	class PropertiesContentProvider implements ITreeContentProvider {

		@Override
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof String) {
				return new Object[] { inputElement };
			}
			return getChildren(inputElement);
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			if (parentElement instanceof Object[]) {
				return (Object[]) parentElement;
			}
			if (parentElement instanceof Collection) {
				return ((Collection<?>) parentElement).toArray();
			}
			if (parentElement instanceof Property<?>) {
				Property<?> property = (Property<?>) parentElement;
				return property.getSubProperties() != null ? property.getSubProperties().toArray() : EMPTY_ARRAY;
			}
			return EMPTY_ARRAY;
		}

		@Override
		public Object getParent(Object element) {
			return null;
		}

		@Override
		public boolean hasChildren(Object parentElement) {
			if (!(parentElement instanceof Property<?>)) {
				return false;
			}
			Property<?> property = (Property<?>) parentElement;
			return property.getSubProperties() != null && !property.getSubProperties().isEmpty();
		}

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

	}

}