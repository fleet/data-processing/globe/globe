package fr.ifremer.globe.ui.views.datetime;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectToolItem;

/** Handler of "Synchronize texture on time" toolbar item */
public class SyncTexturePlayersOnTimeHandler {
	@Execute
	public void execute(@Active MPart currentPart, @Active MDirectToolItem item) {
		if (DateTimePlayerView.PART_ID.equals(currentPart.getElementId())) {
			((DateTimePlayerView) currentPart.getObject()).syncTexturePlayersOnTime(item.isSelected());
		}
	}

}