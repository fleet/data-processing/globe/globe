/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.views.datetime;

import java.time.Duration;
import java.time.Instant;
import java.util.EnumSet;
import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoEvent.FileInfoState;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.application.prefs.Preferences;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerComposite;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerData;
import fr.ifremer.globe.utils.date.DateUtils;
import io.reactivex.disposables.Disposable;

/**
 * View of Datetime player
 */
public class DateTimePlayerView {

	/** Part properties **/
	public static final String PART_ID = "fr.ifremer.globe.ui.part.datetimeplayer";

	@Inject
	private IGlobalCursorService globalCursorService;
	private final Consumer<GlobalCursor> globalCursorListener = this::onGlobalCursor;
	@Inject
	private FileInfoModel fileInfoModel;
	private final Consumer<FileInfoEvent> fileInfoEventListener = this::onFileInfoEvent;
	@Inject
	private Preferences preferences;

	/** Min date encountered in FileInfo */
	private Instant playerMinDate = Instant.MAX;
	/** Min date encountered in FileInfo */
	private Instant playerCurrentDate = Instant.MAX;
	/** Max date encountered in FileInfo */
	private Instant playerMaxDate = Instant.MIN;

	private final IndexedPlayer player = new IndexedPlayer(PART_ID, 0, 1000);
	private Disposable playerListener;

	private IndexedPlayerComposite playerComposite;

	/**
	 * Create contents of the view part.
	 */
	@PostConstruct
	public void postConstruct(Composite parent, MPart part) {
		// Init min, max and current date
		computeMinMaxDatetime();

		// Init view
		player.setLabelProvider(this::formatDatetimeForIndex);
		createControls(parent);
		onGlobalCursor(globalCursorService.getLastSubmitted());
		UIUtils.recursiveSetEnabled(playerComposite, !Instant.MAX.equals(playerMinDate));

		hookListener();

		initToolBar(part);
	}

	/**
	 * Listen player and required services
	 */
	private void hookListener() {
		playerListener = player.getIndexPublisher().subscribe(this::onPlayerIndexChanged);
		fileInfoModel.addListener(fileInfoEventListener);
		globalCursorService.addListener(globalCursorListener);
	}

	/**
	 * Creates contents of the view part.
	 */
	public void createControls(Composite parent) {
		parent.setLayout(new GridLayout(1, true));
		playerComposite = new IndexedPlayerComposite(parent, SWT.NONE, player);
		playerComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
	}

	/**
	 * Extend the interval of dates to include the specified one
	 */
	private void extendGlobalDates(Instant oneDate) {
		if (oneDate != null) {
			if (oneDate.isBefore(playerMinDate))
				playerMinDate = oneDate;
			if (oneDate.isAfter(playerMaxDate))
				playerMaxDate = oneDate;
		}
	}

	/**
	 * Cleaning
	 */
	@PreDestroy
	public void dispose() {
		if (playerListener != null) {
			playerListener.dispose();
			playerListener = null;
		}
		fileInfoModel.removeListener(fileInfoEventListener);
		globalCursorService.removeListener(globalCursorListener);
	}

	@Focus
	public void setFocus() {
		if (Instant.MAX.equals(playerMinDate)) {
			computeMinMaxDatetime();
		}
		UIUtils.recursiveSetEnabled(playerComposite, !Instant.MAX.equals(playerMinDate));
	}

	/** FileInfoModel notification. The file list has change, recompute min and max date coverring all files */
	private void onFileInfoEvent(FileInfoEvent event) {
		if (EnumSet.of(FileInfoState.LOADED, FileInfoState.REMOVED, FileInfoState.UNLOADED)
				.contains(event.getState())) {
			computeMinMaxDatetime();
			onGlobalCursor(globalCursorService.getLastSubmitted());
			playerComposite.refreshGroupTitles();
			UIUtils.asyncExecSafety(playerComposite,
					() -> UIUtils.recursiveSetEnabled(playerComposite, !Instant.MAX.equals(playerMinDate)));
		}
	}

	/** IGlobalCursorService notification. A new Datetime may have been designated */
	private void onGlobalCursor(GlobalCursor globalCursor) {
		globalCursor.time().ifPresent(datetime -> {
			if (DateUtils.between(datetime, playerMinDate, playerMaxDate) && !datetime.equals(playerCurrentDate)) {
				// To avoid the onPlayerIndexChanged event after calling player.setCurrent
				if (playerListener != null) {
					playerListener.dispose();
					playerListener = null;
				}

				playerCurrentDate = datetime;
				int index = (int) Math
						.round(Duration.between(playerMinDate, datetime).toMillis() / computeStepDuration());
				if (index != player.getCurrent())
					player.setCurrent(index);

				playerListener = player.getIndexPublisher().subscribe(this::onPlayerIndexChanged);
			}
		});
	}

	/** Player notification. Index has changed */
	private void onPlayerIndexChanged(IndexedPlayerData playerData) {
		Instant newCurrentDate = computeDateAtIndex(playerData.currentValue());
		if (!newCurrentDate.equals(playerCurrentDate)) {
			playerCurrentDate = newCurrentDate;
			var globalCursor = new GlobalCursor()//
					.withTime(newCurrentDate)//
					.withSource(this)//
					.withSyncTexturePlayer(preferences.getSyncTexturePlayersOnTime().getValue())//
					.withSyncWcPayer(preferences.getSyncWcPayersOnTime().getValue());
			globalCursorService.submit(globalCursor);
		}
	}

	/** Returns the amount of ms for each step index of the player */
	private double computeStepDuration() {
		long globalDuration = Duration.between(playerMinDate, playerMaxDate).toMillis();
		return globalDuration / (double) (player.getData().maxLimit() - player.getData().minLimit() + 1);
	}

	/** Format a date (specified by the index) to String */
	private String formatDatetimeForIndex(int index) {
		if (index == player.getData().minLimit() && !Instant.MAX.equals(playerMinDate))
			return DateUtils.getDateTimeString(playerMinDate);
		if (index == player.getData().maxLimit() && !Instant.MIN.equals(playerMaxDate))
			return DateUtils.getDateTimeString(playerMaxDate);
		if (index == player.getCurrent() && !Instant.MAX.equals(playerCurrentDate))
			return DateUtils.getDateTimeString(playerCurrentDate);

		Instant dateAtIndex = computeDateAtIndex(index);
		if (!Instant.MIN.equals(dateAtIndex) && !Instant.MAX.equals(dateAtIndex))
			return DateUtils.getDateTimeString(dateAtIndex);
		return "";
	}

	/** Return the Instant corresponding to the specified index */
	private Instant computeDateAtIndex(int index) {
		if (index == player.getData().minLimit())
			return playerMinDate;
		if (index == player.getData().maxLimit())
			return playerMaxDate;

		double stepDuration = computeStepDuration();
		return playerMinDate.plusMillis((long) (stepDuration * index));
	}

	/**
	 * Browse all loaded files and compute interval of dates
	 */
	private void computeMinMaxDatetime() {
		playerMinDate = Instant.MAX;
		playerMaxDate = Instant.MIN;
		for (var fileInfo : fileInfoModel.getAll()) {
			Pair<Instant, Instant> startEndDate = fileInfo.getStartEndDate().orElse(null);
			if (startEndDate != null) {
				extendGlobalDates(startEndDate.getFirst());
				extendGlobalDates(startEndDate.getSecond());
			}
		}

		// Reset current
		if (Instant.MAX.equals(playerMinDate) //
				|| !DateUtils.between(playerCurrentDate, playerMinDate, playerMaxDate)) {
			playerCurrentDate = Instant.MAX;
			player.setCurrent(0);
		}
	}

	/** Initialize toolbar items with preferences */
	private void initToolBar(MPart part) {
		for (var toolBarElement : part.getToolbar().getChildren()) {
			switch (toolBarElement.getElementId()) {
			case "fr.ifremer.globe.ui.part.datetimeplayer.synchronizeLayersOnTime" -> //
					((MItem) toolBarElement).setSelected(preferences.getSyncTexturePlayersOnTime().getValue());
			case "fr.ifremer.globe.ui.part.datetimeplayer.syncWcPlayersOnTime" -> //
					((MItem) toolBarElement).setSelected(preferences.getSyncWcPayersOnTime().getValue());
			}
		}
	}

	/**
	 * Change the texture synchronization behaviour
	 */
	public void syncTexturePlayersOnTime(boolean syncTexturePlayersOnTime) {
		preferences.getSyncTexturePlayersOnTime().setValue(syncTexturePlayersOnTime, false);
		preferences.save();

		// Submit last cursor with synchronize info
		if (syncTexturePlayersOnTime && !Instant.MAX.equals(playerCurrentDate)) {
			globalCursorService.submit(//
					new GlobalCursor()//
							.withTime(playerCurrentDate)//
							.withSyncTexturePlayer(true)//
							.withSource(this));
		}
	}

	/**
	 * Change the WC synchronization behaviour
	 */
	public void syncWcPlayersOnTime(boolean syncWcPlayersOnTime) {
		preferences.getSyncWcPayersOnTime().setValue(syncWcPlayersOnTime, false);
		preferences.save();

		// Submit last cursor with synchronize info
		if (syncWcPlayersOnTime && !Instant.MAX.equals(playerCurrentDate)) {
			globalCursorService.submit(//
					new GlobalCursor()//
							.withTime(playerCurrentDate)//
							.withSyncWcPayer(true)//
							.withSource(this));
		}
	}
}
