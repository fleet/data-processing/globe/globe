package fr.ifremer.globe.ui.views.performances;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PerformanceCounterView {

	protected Logger logger = LoggerFactory.getLogger(PerformanceCounterView.class);

	private Composite parent;
	private PerfCounterEditor composite;

	public PerformanceCounterView() {

	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@PostConstruct
	public void createPartControl(Composite parent) {

		this.parent = parent;
		createUI();
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Focus
	public void setFocus() {
	}


	public void createUI() {
		composite=new PerfCounterEditor(parent,SWT.NONE) ;

	}
	@PreDestroy
	public void close()
	{
		composite.dispose();
	}
	public void setPart(MPart imagePart) {
	}


}