package fr.ifremer.globe.ui.views.performances;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.Timer;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import gov.nasa.worldwind.util.PerformanceStatistic;

@SuppressWarnings("serial")
public class GPUUsage extends JPanel {
	private class DataGenerator extends Timer implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent actionevent) {
			Collection<PerformanceStatistic> perf = IGeographicViewService.grab().getWwd().getPerFrameStatistics();
			for (PerformanceStatistic performance : perf) {
				if (performance.getKey().compareTo(PerformanceStatistic.FRAME_RATE) == 0) {
					Integer v = (Integer) performance.getValue();
					addFPS(v);
				}
			}
		}

		DataGenerator(int i) {
			super(i, null);
			addActionListener(this);
		}
	}

	// timer each seconds
	private Timer timer = new DataGenerator(500);
	private TimeSeries fps;

	/**
	 * Create a memory usage point
	 * 
	 * @param i
	 *            the maximum lenght of data in millisecond
	 * */
	public GPUUsage(int i) {
		super(new BorderLayout());
		Set<String> set = new HashSet<String>(1);
		set.add(PerformanceStatistic.FRAME_RATE);

		IGeographicViewService.grab().activatePerformance();
		fps = new TimeSeries("Frame per seconds");
		fps.setMaximumItemAge(i);
		Date time = new Date();
		for (int t = 0; t < i / 500; t++) {
			time.setTime(System.currentTimeMillis() - t * 1000);
			fps.add(new Millisecond(time), 0);
		}

		TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
		timeseriescollection.addSeries(fps);
		DateAxis dateaxis = new DateAxis("Time");
		NumberAxis numberaxis = new NumberAxis("Frame rate (Hz)");
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer(true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.red);

		XYPlot xyplot = new XYPlot(timeseriescollection, dateaxis, numberaxis, xylineandshaperenderer);
		dateaxis.setAutoRange(true);
		dateaxis.setLowerMargin(0.0D);
		dateaxis.setUpperMargin(0.0D);
		dateaxis.setTickLabelsVisible(true);
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		JFreeChart jfreechart = new JFreeChart(null, null, xyplot, true);
		ChartUtilities.applyCurrentTheme(jfreechart);
		ChartPanel chartpanel = new ChartPanel(jfreechart, true);
		// chartpanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(4,
		// 4, 4, 4), BorderFactory.createLineBorder(Color.black)));
		add(chartpanel);
		xyplot.setRangeGridlinePaint(Color.black);
		xyplot.setBackgroundPaint(Color.LIGHT_GRAY);
		xyplot.getRenderer().setSeriesPaint(0, Color.BLUE);

		jfreechart.setBackgroundPaint(Color.LIGHT_GRAY);

	}

	private void addFPS(double d) {
		fps.add(new Millisecond(), d);
	}

	public void start() {
		this.timer.start();
	}

	public void stop() {
		this.timer.stop();
	}

}
