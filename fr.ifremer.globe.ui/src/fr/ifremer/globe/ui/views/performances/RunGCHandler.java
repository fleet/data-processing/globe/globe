package fr.ifremer.globe.ui.views.performances;

import org.eclipse.e4.core.di.annotations.Execute;

public class RunGCHandler {
	@Execute
	public void execute() {
		System.gc();
	}
}
