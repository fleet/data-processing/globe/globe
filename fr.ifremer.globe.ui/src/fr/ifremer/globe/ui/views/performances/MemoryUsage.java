package fr.ifremer.globe.ui.views.performances;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.Timer;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

@SuppressWarnings("serial")
public class MemoryUsage extends JPanel {
	private class DataGenerator extends Timer implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent actionevent) {
			long l = Runtime.getRuntime().freeMemory();
			long l1 = Runtime.getRuntime().totalMemory();
			addTotalObservation(l1 / 1024 / 1024);
			addFreeObservation(l / 1024 / 1024);
			addUsedMemory((l1 - l) / 1024 / 1024);
		}

		DataGenerator(int i) {
			super(i, null);
			addActionListener(this);
		}
	}

	// timer each seconds
	private Timer timer = new DataGenerator(500);
	private TimeSeries total;
	private TimeSeries free;
	private TimeSeries used;

	/**
	 * Create a memory usage point
	 * 
	 * @param i
	 *            the maximum lenght of data in millisecond
	 * */
	public MemoryUsage(int i) {
		super(new BorderLayout());
		total = new TimeSeries("Total Memory");
		total.setMaximumItemAge(i);
		free = new TimeSeries("Free Memory");
		free.setMaximumItemAge(i);
		used = new TimeSeries("Used Memory");
		used.setMaximumItemAge(i);

		Date time = new Date();
		for (int t = 0; t < i / 500; t++) {
			time.setTime(System.currentTimeMillis() - t * 1000);
			used.add(new Millisecond(time), 0);
		}

		TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
		timeseriescollection.addSeries(total);
		timeseriescollection.addSeries(used);
		//		timeseriescollection.addSeries(free);
		DateAxis dateaxis = new DateAxis("Time");
		NumberAxis numberaxis = new NumberAxis("Memory (M)");
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer(true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.blue);
		xylineandshaperenderer.setSeriesPaint(1, Color.red);
		xylineandshaperenderer.setSeriesPaint(0, Color.green);

		XYPlot xyplot = new XYPlot(timeseriescollection, dateaxis, numberaxis, xylineandshaperenderer);
		dateaxis.setAutoRange(true);
		dateaxis.setLowerMargin(0.0D);
		dateaxis.setUpperMargin(0.0D);
		dateaxis.setTickLabelsVisible(true);
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		JFreeChart jfreechart = new JFreeChart(null, null, xyplot, true);
		ChartUtilities.applyCurrentTheme(jfreechart);
		ChartPanel chartpanel = new ChartPanel(jfreechart, true);
		// chartpanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(4,
		// 4, 4, 4), BorderFactory.createLineBorder(Color.black)));
		add(chartpanel);
		xyplot.setRangeGridlinePaint(Color.black);
		xyplot.setBackgroundPaint(Color.LIGHT_GRAY);
		xyplot.getRenderer().setSeriesPaint(0, Color.DARK_GRAY);

		jfreechart.setBackgroundPaint(Color.LIGHT_GRAY);

	}

	private void addTotalObservation(double d) {
		total.add(new Millisecond(), d);
	}

	private void addFreeObservation(double d) {
		free.add(new Millisecond(), d);
	}

	private void addUsedMemory(double d) {
		used.add(new Millisecond(), d);

	}

	public void start() {
		this.timer.start();
	}

	public void stop() {
		this.timer.stop();
	}

}
