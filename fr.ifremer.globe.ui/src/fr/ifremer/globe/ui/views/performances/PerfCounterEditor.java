package fr.ifremer.globe.ui.views.performances;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.util.Date;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import fr.ifremer.globe.utils.perfcounter.PerfoCounterEntry;
import fr.ifremer.globe.utils.perfcounter.PerfoCounterEntry.Measure;
import fr.ifremer.globe.utils.perfcounter.PerfoCounterManager;

public class PerfCounterEditor extends Composite implements Observer {
	private class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			PerfoCounterEntry p = (PerfoCounterEntry) element;
			return p.getKey();
		}
	}

	private Table table;
	CheckboxTableViewer checkboxTableViewer;
	PerfGraph perfG;

	private class NewClearAction extends Action {
		public NewClearAction() {
			super("Clear");
		}

		@Override
		public void run() {
			TableItem[] selected = table.getSelection();
			for (TableItem item : selected) {
				PerfoCounterEntry entry = (PerfoCounterEntry) item.getData();
				entry.clear();
			}
		}
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PerfCounterEditor(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));

		TabFolder tabFolder = new TabFolder(composite, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		TabItem tbtmCounter = new TabItem(tabFolder, SWT.NONE);
		tbtmCounter.setText("Counter");

		Composite composite_1 = new Composite(tabFolder, SWT.NONE);
		tbtmCounter.setControl(composite_1);
		composite_1.setLayout(new FillLayout(SWT.HORIZONTAL));

		checkboxTableViewer = CheckboxTableViewer.newCheckList(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
		table = checkboxTableViewer.getTable();
		table.setLinesVisible(true);

		TableViewerColumn tableViewerColumn = new TableViewerColumn(checkboxTableViewer, SWT.NONE);
		TableColumn tblclmnNewColumn = tableViewerColumn.getColumn();
		tblclmnNewColumn.setWidth(300);
		tblclmnNewColumn.setText("New Column");
		checkboxTableViewer.setContentProvider(ArrayContentProvider.getInstance());

		checkboxTableViewer.setLabelProvider(new TableLabelProvider());

		checkboxTableViewer.setInput(PerfoCounterManager.getInstance().getPerfoCounterAsList());
		TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("Graph");

		// add a popup for new rows
		MenuManager popManager = new MenuManager();
		IAction menuAction = new NewClearAction();
		popManager.add(menuAction);
		Menu menu = popManager.createContextMenu(table);
		table.setMenu(menu);

		Composite top = new Composite(tabFolder, SWT.EMBEDDED);
		tabItem.setControl(top);
		top.setLayoutData(new GridData(GridData.FILL_BOTH));
		// Swing Frame and Panel
		Frame frame = SWT_AWT.new_Frame(top);
		frame.setLayout(new java.awt.GridLayout(1, 1));
		perfG = new PerfGraph();
		frame.add(perfG);

		checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {

			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {

				PerfoCounterEntry p = (PerfoCounterEntry) event.getElement();
				if (p != null) {
					if (event.getChecked())

						perfG.addDataSet(p);
					else
						perfG.removeDataSet(p);
				}

			}
		});
		PerfoCounterManager.getInstance().addObserver(this);
	}

	@Override
	public void update(final Observable paramObservable, Object paramObject) {
		if (paramObservable instanceof PerfoCounterManager perfoCounterManager)
			checkboxTableViewer.getTable().getDisplay()
					.asyncExec(() -> checkboxTableViewer.setInput(perfoCounterManager.getPerfoCounterAsList()));
	};

	@Override
	public void dispose() {
		// remove observer in perfG
		perfG.dispose();
		PerfoCounterManager.getInstance().deleteObserver(this);
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@SuppressWarnings("serial")
	public class PerfGraph extends JPanel {
		private TimeSeriesCollection timeseriescollection;
		private XYPlot xyplot;

		class MyDataSet implements Observer {
			private PerfoCounterEntry entry;
			private Date time;

			public MyDataSet(TimeSeries dataset, PerfoCounterEntry entry) {
				this.series = dataset;
				this.entry = entry;
				this.time = new Date();
				for (Measure data : entry.getMeasures()) {
					time.setTime(data.date);
					dataset.addOrUpdate(new Millisecond(time), data.timeElapsed);
				}
				entry.addObserver(this);
			}

			TimeSeries series;

			@Override
			public void update(Observable paramObservable, Object paramObject) {
				this.time = new Date();
				if (entry.getMeasures().size() == 0) {
					series.clear();
					return;
				}
				Measure data = entry.getMeasures().get(entry.getMeasures().size() - 1);
				time.setTime(data.date);
				series.addOrUpdate(new Millisecond(time), data.timeElapsed);

			}

			public void dispose() {
				entry.deleteObserver(this);
			}
		}

		HashMap<String, MyDataSet> map;

		PerfGraph() {
			super(new BorderLayout());
			map = new HashMap<String, MyDataSet>();
			timeseriescollection = new TimeSeriesCollection();
			DateAxis dateaxis = new DateAxis("Time");
			NumberAxis numberaxis = new NumberAxis("Time spent");
			XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer(true, true);
			// xylineandshaperenderer.setSeriesPaint(0, Color.blue);
			// xylineandshaperenderer.setSeriesPaint(1, Color.green);
			// xylineandshaperenderer.setSeriesPaint(2, Color.red);
			xyplot = new XYPlot(timeseriescollection, dateaxis, numberaxis, xylineandshaperenderer);
			dateaxis.setAutoRange(true);
			dateaxis.setLowerMargin(0.0D);
			dateaxis.setUpperMargin(0.0D);
			dateaxis.setTickLabelsVisible(true);
			JFreeChart jfreechart = new JFreeChart(null, null, xyplot, true);
			ChartUtilities.applyCurrentTheme(jfreechart);
			ChartPanel chartpanel = new ChartPanel(jfreechart, true);
			// chartpanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(4,
			// 4, 4, 4), BorderFactory.createLineBorder(Color.black)));
			add(chartpanel);
			xyplot.setRangeGridlinePaint(Color.black);
			xyplot.setBackgroundPaint(Color.LIGHT_GRAY);
			// xyplot.getRenderer().setSeriesPaint(0, Color.DARK_GRAY);

			jfreechart.setBackgroundPaint(Color.LIGHT_GRAY);
		}

		public void removeDataSet(PerfoCounterEntry p) {
			MyDataSet dst = map.get(p.getKey());
			timeseriescollection.removeSeries(dst.series);
			dst.dispose();
			map.remove(p.getKey());
		}

		public void addDataSet(PerfoCounterEntry p) {
			TimeSeries dataset = new TimeSeries(p.getKey());

			timeseriescollection.addSeries(dataset);
			map.put(p.getKey(), new MyDataSet(dataset, p));
		}

		public void dispose() {
			for (MyDataSet dst : map.values()) {
				dst.dispose();
			}
			map.clear();
		}
	}

}
