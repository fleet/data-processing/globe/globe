package fr.ifremer.globe.ui.views.performances;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemoryView {

	protected Logger logger = LoggerFactory.getLogger(MemoryView.class);

	private Composite parent;
	private Frame frame;
	private MemoryUsage memoryusagedemo;
	private GPUUsage gpuUsage;
	//private MPart part;

	public MemoryView() {

	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@PostConstruct
	public void createPartControl(Composite parent) {

		this.parent = parent;
		createUI();
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Focus
	public void setFocus() {
	}

	@PreDestroy
	public void onClose() {
		memoryusagedemo.stop();
	}

	public void setPart(MPart value) {
		//	this.part = value;
	}

	public void createUI() {
		final FillLayout layout = new FillLayout();
		parent.setLayout(layout);

		Composite top = new Composite(parent, SWT.EMBEDDED);
		top.setLayoutData(new GridData(GridData.FILL_BOTH));
		// Swing Frame and Panel
		frame = SWT_AWT.new_Frame(top);

		memoryusagedemo = new MemoryUsage(1000 * 60);
		gpuUsage = new GPUUsage(1000 * 60);
		frame.setLayout(new GridLayout(0, 2));
		frame.add(memoryusagedemo);
		frame.add(gpuUsage);
		// frame.setBounds(200, 120, 600, 280);
		frame.setVisible(true);
		memoryusagedemo.start();
		gpuUsage.start();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				memoryusagedemo.stop();
				gpuUsage.stop();
			}

			@Override
			public void windowClosed(WindowEvent e) {
				memoryusagedemo.stop();
				gpuUsage.stop();
			}

		});
	}

}