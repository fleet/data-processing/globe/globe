package fr.ifremer.globe.ui.views.layerlist;

import gov.nasa.worldwind.layers.Layer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Listener for drop operations in Layer List view part. */
public class LayerListDragListener implements DragSourceListener {

	protected Logger logger = LoggerFactory.getLogger(LayerListDragListener.class);

	private final Viewer viewer;
	/** Implements superclass of {@link LayerListDragListener}. */
	public LayerListDragListener(Viewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

		Object[] elements = ((StructuredSelection) selection).toArray();

		if (elements.length == 0) {
			event.detail = DND.DROP_NONE;
			return;
		}
		List<String> nameList = new ArrayList<String>();
		String listofData = "";

		for (Object e : elements) {
			if (e instanceof Layer) {

				Layer layer = (Layer) e;
				nameList.add(layer.getName());
				listofData += (layer.getName()) + "#";// NodeConverter.SEPARATOR;
			}
		}
		if (event.dataType != null && listofData != "") {

			if (TextTransfer.getInstance().isSupportedType(event.dataType)) {
				event.data = listofData;
			}
		}
	}

	@Override
	public void dragStart(DragSourceEvent event) {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

		Object[] elements = ((StructuredSelection) selection).toArray();
		event.doit = false;

		if (elements.length == 0) {
			event.detail = DND.DROP_NONE;
			return;
		}

		for (Object e : elements) {
			if (e instanceof Layer) {
				event.doit = true;
			}
		}
	}

	@Override
	public void dragFinished(DragSourceEvent event) {

	}
}
