package fr.ifremer.globe.ui.views.layerlist;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;

/** Listener for drop operations in Layer List view part. */
public class LayerListDropListener extends ViewerDropAdapter {

	private Logger logger = LoggerFactory.getLogger(LayerListDropListener.class);

	/** Implements superclass of {@link LayerListDropListener}. */
	public LayerListDropListener(Viewer viewer) {
		super(viewer);
	}

	/**
	 * Function which select where user drop its item regarding the target
	 * 
	 * @param event : {@link DropTargetEvent}
	 */
	@Override
	public void drop(DropTargetEvent event) {

		super.drop(event);

		int location = this.determineLocation(event);
		String translatedLocation = "";
		String sourceLayerName = (String) event.data;
		Layer target = (Layer) determineTarget(event);

		switch (location) {
		case ViewerDropAdapter.LOCATION_BEFORE: // Case where user drop its item just before the item target.
			translatedLocation = "Dropped " + sourceLayerName + " before the target " + target.getName();
			moveItemBefore(true, sourceLayerName, target);
			break;
		case ViewerDropAdapter.LOCATION_AFTER: // Case where user drop its item just after the item target.
			translatedLocation = "Dropped " + sourceLayerName + " after the target " + target.getName();
			moveItemBefore(false, sourceLayerName, target);
			break;
		case ViewerDropAdapter.LOCATION_ON: // Case where user drop its item on the item target.
			translatedLocation = "Dropped " + sourceLayerName + " into nothing";
			target = null;
			break;
		case ViewerDropAdapter.LOCATION_NONE: // Case where user drop its item on the item target
			translatedLocation = "Dropped " + sourceLayerName + " into nothing";
			target = null;
			break;
		}

		logger.debug(translatedLocation);
	}

	@Override
	public boolean performDrop(Object data) {
		return false;
	}

	@Override
	public boolean validateDrop(Object target, int operation, TransferData transferType) {

		if (target instanceof Layer && !(target instanceof FileInfoNode)) {

			Layer s1 = (Layer) target;
			Layer s2 = (Layer) getSelectedObject();
			if (s1 == s2) {
				return true;
			}
		}

		if (target instanceof Layer && getCurrentLocation() != LOCATION_ON) {
			return true;
		}

		return false;
	}

	@Override
	public void dropAccept(DropTargetEvent event) {
		if (event.data == null && event.dataTypes == null) {
			event.detail = DND.DROP_NONE;
		}
	}

	/**
	 * Function which perform graphical movements according to {@link LayerListDropListener:drop()} instructions.
	 * 
	 * @param before : indicates if itemNameToMove moves before or after target.
	 * @param itemNameToMove : name of item to drop.
	 * @param targetName : name of drop target.
	 */
	public void moveItemBefore(boolean before, String sourceLayerName, Layer target) {

		// Creation of a new layer list where moving operations will be done.
		LayerList newlayerList = IGeographicViewService.grab().getLayers();
		// Find the itemToMove and the target in the layer List of Viewer3D.
		Layer newItemToMove = newlayerList.getLayerByName(sourceLayerName.substring(0, sourceLayerName.length() - 1));
		Layer newTarget = null;
		for (Layer layer : newlayerList) {
			if (target == layer) {
				newTarget = layer;
			}
		}
		// Find the indexes of itemToMove and the target in the layer List of Viewer3D.
		int itemToMoveIndex = newlayerList.indexOf(newItemToMove, 0);
		int targetIndex = newlayerList.indexOf(target, 0);
		// Process moving operations.
		if (newItemToMove != null & newTarget != null) {
			if (targetIndex >= 0) {
				if (itemToMoveIndex < targetIndex) {

					newlayerList.remove(newItemToMove);

					if (before) {
						newlayerList.add(targetIndex - 1, newItemToMove);
					} else {
						newlayerList.add(targetIndex, newItemToMove);
					}
				} else {
					newlayerList.remove(newItemToMove);

					if (before) {
						newlayerList.add(targetIndex, newItemToMove);
					} else {
						newlayerList.add(targetIndex + 1, newItemToMove);
					}
				}
				// Update the Viewer3D with updated layer list.
				IGeographicViewService.grab().getWwd().getModel().setLayers(newlayerList);
				getViewer().refresh();
			}
		}
	}
}
