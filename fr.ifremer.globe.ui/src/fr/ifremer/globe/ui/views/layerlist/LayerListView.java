package fr.ifremer.globe.ui.views.layerlist;

import java.beans.PropertyChangeListener;
import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.CheckStateProviderFactory;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.Layer;

/** Layer List view part */
public class LayerListView {

	private CheckboxTableViewer tableViewer;

	@PostConstruct
	protected void createUI(Composite parent, IGeographicViewService geographicViewService,
			WWFileLayerStoreModel fileLayerStoreModel) {
		parent.setLayout(new FillLayout());

		tableViewer = CheckboxTableViewer.newCheckList(parent, SWT.CHECK);
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setLabelProvider(
				LabelProvider.createTextProvider(obj -> obj instanceof Layer layer ? layer.getName() : ""));

		// check
		tableViewer.setCheckStateProvider(CheckStateProviderFactory.build(Layer.class, Layer::isEnabled));
		tableViewer.getTable().addListener(SWT.Selection, event -> {
			if (event.detail == SWT.CHECK) {
				var selectedItem = (TableItem) event.item;
				((Layer) selectedItem.getData()).setEnabled(selectedItem.getChecked());
			}
		});

		// DND
		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[] { TextTransfer.getInstance() };
		tableViewer.addDragSupport(operations, transferTypes, new LayerListDragListener(tableViewer));
		tableViewer.addDropSupport(operations, transferTypes, new LayerListDropListener(tableViewer));

		// bind with layer list
		var layerList = geographicViewService.getLayers();
		tableViewer.setInput(layerList);

		// observe layers "enabled" property
		PropertyChangeListener layerListChangeListener = event -> {
			// filter events to avoid useless refresh
			if ((event.getPropertyName().equals(AVKey.LAYERS) && event.getSource() == layerList)
					|| event.getPropertyName().equals("Enabled"))
				refresh();
		};
		layerList.addPropertyChangeListener(layerListChangeListener);
		parent.addDisposeListener(evt -> layerList.removePropertyChangeListener(layerListChangeListener));

		// observe WWFileLayerStoreModel (if layer has been removed or added)
		Consumer<WWFileLayerStoreEvent> fileLayerStoreEventConsumer = event -> refresh();
		fileLayerStoreModel.addListener(fileLayerStoreEventConsumer);
		parent.addDisposeListener(evt -> fileLayerStoreModel.removeListener(fileLayerStoreEventConsumer));
	}

	private void refresh() {
		if (!tableViewer.getTable().isDisposed())
			tableViewer.getTable().getDisplay().asyncExec(tableViewer::refresh);
	}

}