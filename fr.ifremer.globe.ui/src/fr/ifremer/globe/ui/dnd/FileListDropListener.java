package fr.ifremer.globe.ui.dnd;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TransferData;

/**
 * Listener for the drag n drop of files in the mergeTool tableviewer
 */
public abstract class FileListDropListener extends ViewerDropAdapter {

	/**
	 * Constructor
	 */
	public FileListDropListener(Viewer viewer) {
		super(viewer);
	}

	/**
	 * @see org.eclipse.jface.viewers.ViewerDropAdapter#performDrop(java.lang.Object)
	 */
	@Override
	public boolean performDrop(Object data) {
		if (data instanceof String[]) {
			List<File> files = new ArrayList<>();
			Arrays.stream((String[]) data).forEach((filename) -> files.add(new File(filename)));
			return performDrop(files);
		}
		return false;
	}

	/** Performs any work associated with the files drop. */
	public abstract boolean performDrop(List<File> files);

	/**
	 * @see org.eclipse.jface.viewers.ViewerDropAdapter#validateDrop(java.lang.Object, int,
	 *      org.eclipse.swt.dnd.TransferData)
	 */
	@Override
	public boolean validateDrop(Object target, int operation, TransferData transferType) {
		FileTransfer fileTransfer = FileTransfer.getInstance();
		return fileTransfer.isSupportedType(transferType);
	}
}
