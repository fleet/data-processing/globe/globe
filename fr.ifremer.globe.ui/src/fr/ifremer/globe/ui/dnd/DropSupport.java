/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.dnd;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Control;

/**
 * Adds support for dropping objects into the controls
 */
public class DropSupport {

	/**
	 * Constructor
	 */
	public DropSupport() {
	}

	/** Adds support for dropping files into the control */
	public static void initializeFileDropSupport(Control control, Consumer<List<File>> consumer) {
		DropTarget target = new DropTarget(control, DND.DROP_DEFAULT | DND.DROP_MOVE | DND.DROP_COPY);
		target.setTransfer(new Transfer[] { FileTransfer.getInstance() });
		target.addDropListener(new DropTargetAdapter() {
			@Override
			public void dragEnter(DropTargetEvent e) {
				if (e.detail == DND.DROP_DEFAULT)
					e.detail = DND.DROP_COPY;
			}

			@Override
			public void dragOperationChanged(DropTargetEvent e) {
				if (e.detail == DND.DROP_DEFAULT)
					e.detail = DND.DROP_COPY;
			}

			@Override
			public void drop(DropTargetEvent e) {
				if (e.data instanceof String[]) {
					String[] paths = (String[]) e.data;
					if (paths.length > 0) {
						List<File> result = new ArrayList<>(paths.length);
						Arrays.stream(paths).forEach((path) -> result.add(new File(path)));
						consumer.accept(result);
					}
				}
			}
		});
	}

	/** Adds support for dropping a directory into the control */
	public static void initializeOneDirectoryDropSupport(Control control, Consumer<File> consumer) {
		initializeFileDropSupport(control, (files) -> {
			File outputDirectory = files.get(0);
			if (outputDirectory.exists()) {
				consumer.accept(outputDirectory.isFile() ? outputDirectory.getParentFile() : outputDirectory);
			}
		});
	}

	/** Adds support for dropping a file into the control */
	public static void initializeOneFileDropSupport(Control control, Consumer<File> consumer) {
		initializeFileDropSupport(control, (files) -> {
			File outputDirectory = files.get(0);
			consumer.accept(outputDirectory);
		});
	}
}
