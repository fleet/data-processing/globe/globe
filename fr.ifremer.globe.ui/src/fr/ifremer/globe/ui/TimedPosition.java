package fr.ifremer.globe.ui;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;

public class TimedPosition extends Position
{
	long time;
	private double validity;

	public TimedPosition(Angle latitude, Angle longitude, double elevation, long time) {
		super(latitude, longitude, elevation);
		this.time=time;
	}
	public TimedPosition(Position pos, long time) {
		super(pos.latitude, pos.longitude, pos.elevation);
		this.time=time;
		validity = 0;
	}
	public TimedPosition(byte validity, Position pos, long time) {
		super(pos.latitude, pos.longitude, pos.elevation);
		this.time=time;
		this.validity = validity;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}

	public double getValidity() {
		return validity;
	}

	@Override
	public String toString() {
		return "(" + latitude.toString() + ", " + longitude.toString() + ", " + elevation + ", " + validity + ")";
	}
}
