/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.texture;

import java.nio.IntBuffer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

import fr.ifremer.globe.ogl.renderer.jogl.buffers.FloatVertexBuffer;
import fr.ifremer.globe.ogl.util.GLUtils;
import fr.ifremer.globe.ogl.util.PaletteTexture;
import fr.ifremer.globe.ogl.util.ShaderStore;
import fr.ifremer.globe.ogl.util.ShaderUtil;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.util.WWMath;

/**
 * This class handles the rendering of a texture.
 */
public class TextureRenderer<C extends ITextureCoordinate> {
	private static final Logger LOGGER = LoggerFactory.getLogger(TextureRenderer.class);

	/**
	 * Render shader program. One shader program instance needed for each OGL context.
	 */
	public static final String TEXTURE_VERT_SHADER = TextureRenderer.class.getResource("texture.vert").getFile();
	public static final String TEXTURE_FRAG_SHADER = TextureRenderer.class.getResource("texture.frag").getFile();
	public static final String TEXTURE_REDIRECT_FRAG_SHADER = TextureRenderer.class.getResource("textureRedirect.frag").getFile();

	private final ShaderStore shaderStore;
	private final ShaderStore shaderStoreRedirect;

	/** OpenGL texture */
	private Texture texture = null;
	private boolean isLoaded = false;

	/** Color palette **/
	private PaletteTexture palette = new PaletteTexture();

	/** Points buffer (x,y,z) */
	protected FloatVertexBuffer viewCoordinatesBuffer = new FloatVertexBuffer(3);

	/** Texture vectors buffer (s,t) */
	protected FloatVertexBuffer textureCoordinatesBuffer = new FloatVertexBuffer(2);

	/** Index buffer */
	protected IntBuffer indexBuffer;

	/** Model */
	private ITextureData<C> data;

	/** Supplier of settings **/
	private final Supplier<TextureDataParameters> settingsSupplier;

	/**
	 * Current GLContext
	 */
	private GL currentGL;

	/**
	 * Constructor
	 */
	public TextureRenderer(Supplier<TextureDataParameters> settingsSupplier) {
		this.settingsSupplier = settingsSupplier;

		shaderStore = new ShaderStore(
				gl -> ShaderUtil.createShader(this, gl, TEXTURE_VERT_SHADER, TEXTURE_FRAG_SHADER));
		shaderStoreRedirect = new ShaderStore(
				gl -> ShaderUtil.createShader(this, gl, TEXTURE_VERT_SHADER, TEXTURE_REDIRECT_FRAG_SHADER));
	}

	/**
	 * Dispose
	 */
	public void dispose() {
		shaderStore.dispose();
		shaderStoreRedirect.dispose();
		palette.dispose();
		SwingUtilities.invokeLater(() -> {
			if (currentGL != null && currentGL.getContext().makeCurrent() != GLContext.CONTEXT_NOT_CURRENT) {
				if (texture != null) {
					texture.destroy(currentGL);
				}
				viewCoordinatesBuffer.delete(currentGL);
				textureCoordinatesBuffer.delete(currentGL);
			}
		});
	}

	/**
	 * Receive new data
	 *
	 * Apportion the coordinates all along a straight line
	 *
	 * @param xSupplier : provides X position for {@link IWWTextureCoordinate}.
	 *
	 * @return the buffer containing the x/y/z of all points describing the texture
	 */
	public void acceptData(ITextureData<C> data, Function<C, float[]> position2DSupplier) {
		if (this.data == data)
			return;
		this.data = data;

		isLoaded = false; // texture will be refreshed at the next rendering loop

		// clear coordinate buffers
		viewCoordinatesBuffer.clear();
		textureCoordinatesBuffer.clear();

		// Associates 3D position with a position in texture
		var coordinates = data.getCoordinates();
		viewCoordinatesBuffer.allocate(coordinates.size());
		textureCoordinatesBuffer.allocate(coordinates.size());

		for (C coord : coordinates) {
			// 2D view position
			viewCoordinatesBuffer.put(position2DSupplier.apply(coord));
			// position in texture
			textureCoordinatesBuffer.put(new float[] { coord.getS(), coord.getT() });
		}

		indexBuffer = WWMath.computeIndicesForGridInterior(data.getCoordGridHeight(), data.getCoordGridWidth());
	}

	/**
	 * Computes OpenGL texture and coordinate buffers.
	 */
	protected void load(GL2 gl) {
		// refresh texture
		if (texture != null) {
			texture.destroy(currentGL);
		}
		data.getBuffer().rewind();
		TextureData textureData = new TextureData(gl.getGLProfile(), GL.GL_ALPHA32F, data.getWidth(), data.getHeight(),
				0, GL.GL_ALPHA, GL.GL_FLOAT, false, false, false, data.getBuffer(), null);

		currentGL = gl;
		texture = TextureIO.newTexture(textureData);
		viewCoordinatesBuffer.sendToGpu(gl);
		textureCoordinatesBuffer.sendToGpu(gl);
		isLoaded = true;
	}

	public void draw(GL2 gl) {
		// Data available ?
		if (data == null)
			return;

		if (!isLoaded)
			load(gl);

		try {
			gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
					| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for depth func
					| GL2.GL_TEXTURE_BIT // for texture env
					| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

			// Enable shader
			int shaderProgram = shaderStore.getShaderProgram(gl);
			gl.glUseProgram(shaderProgram);

			// Enable blending (transparency)
			gl.glEnable(GL.GL_BLEND);
			gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

			// Set shader parameters (opacity, contrast, threshold...)
			TextureDataParameters settings = settingsSupplier.get();
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "opacity"), settings.opacity);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minContrast"), settings.minContrast);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxContrast"), settings.maxContrast);
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "isThresholdEnabled"),
					settings.isThesholdEnabled ? 1 : 0);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minThreshold"), settings.minThreshold);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxThreshold"), settings.maxThreshold);

			// Palette texture is created here to avoid a bug at the first rendering: to explain...
			Texture paletteTexture = palette.getColorPalette(gl.getGL2(), settings.colorMapIndex,
					settings.isColorMapReversed);

			// Bind texture
			gl.glEnable(GL.GL_TEXTURE_2D);

			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "texture"), 0);
			if (texture != null)
				texture.bind(gl);

			// Texture filtering parameters (define interpolation)
			int filtering = settings.isInterpolationEnabled ? GL.GL_LINEAR : GL.GL_NEAREST;
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, filtering);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, filtering);

			// Texture wrap parameters : do not display texture outside bounds
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL2GL3.GL_CLAMP_TO_BORDER);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL2GL3.GL_CLAMP_TO_BORDER);

			// Bind color palette texture
			gl.glActiveTexture(GL.GL_TEXTURE1);
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "palette"), 1);
			paletteTexture.bind(gl);

			// Palette texture warp parameters (use limit color outside contrast limits)
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

			GLUtils.checkGLError(gl).ifPresent(
					error -> LOGGER.debug("Error with {} render method: {} ", getClass().getSimpleName(), error));

			doDraw(gl);

			// Disable shaders
			if (shaderProgram != 0) {
				gl.glUseProgram(0);
			}

			gl.glPopAttrib();
		} catch (GIOException e) {
			LOGGER.debug("Error while rendering texture : " + e.getMessage(), e);
		}
	}

	protected void doDraw(GL2 gl) {
		// Enable vertex arrays
		gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

		// Set the vertex pointer to the vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, viewCoordinatesBuffer.getBufferName());
		gl.glVertexPointer(viewCoordinatesBuffer.getVerticeSize(), GL.GL_FLOAT, 0, 0);

		// texture
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, textureCoordinatesBuffer.getBufferName());
		gl.glTexCoordPointer(textureCoordinatesBuffer.getVerticeSize(), GL.GL_FLOAT, 0, 0);

		// Rendering
		gl.glDrawElements(GL.GL_TRIANGLE_STRIP, indexBuffer.remaining(), GL.GL_UNSIGNED_INT, indexBuffer);

		// Clean context
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		gl.glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

		GLUtils.checkGLError(gl)
				.ifPresent(error -> LOGGER.debug("Error with {} draw method: {} ", getClass().getSimpleName(), error));
	}
	
	public void redirect(GL2 gl, float resolution, float minValue) {
		// Data available ?
		if (data == null)
			return;

		if (!isLoaded)
			load(gl);

		try {
			gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT // for alpha func
					| GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT | GL.GL_DEPTH_BUFFER_BIT // for depth func
					| GL2.GL_TEXTURE_BIT // for texture env
					| GL2.GL_TRANSFORM_BIT | GL2.GL_POLYGON_BIT);

			// Enable shader
			int shaderProgram = shaderStoreRedirect.getShaderProgram(gl);
			gl.glUseProgram(shaderProgram);

			// Enable blending (transparency)
			gl.glDisable(GL.GL_BLEND);

			// Set shader parameters (resolution, minValue, threshold...)
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "resolution"), resolution);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minValue"), minValue);
			
			TextureDataParameters settings = settingsSupplier.get();
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "isThresholdEnabled"),
					settings.isThesholdEnabled ? 1 : 0);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "minThreshold"), settings.minThreshold);
			gl.glUniform1f(gl.glGetUniformLocation(shaderProgram, "maxThreshold"), settings.maxThreshold);
			
			// Bind texture
			gl.glEnable(GL.GL_TEXTURE_2D);
			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glUniform1i(gl.glGetUniformLocation(shaderProgram, "texture"), 0);
			if (texture != null)
				texture.bind(gl);

			// Texture filtering parameters (define interpolation)
			int filtering = settings.isInterpolationEnabled ? GL.GL_LINEAR : GL.GL_NEAREST;
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, filtering);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, filtering);

			// Texture wrap parameters : do not display texture outside bounds
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL2GL3.GL_CLAMP_TO_BORDER);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL2GL3.GL_CLAMP_TO_BORDER);

			
			GLUtils.checkGLError(gl).ifPresent(
					error -> LOGGER.debug("Error with {} render method: {} ", getClass().getSimpleName(), error));

			doDraw(gl);

			// Disable shaders
			if (shaderProgram != 0) {
				gl.glUseProgram(0);
			}

			gl.glPopAttrib();
		} catch (GIOException e) {
			LOGGER.debug("Error while rendering texture : " + e.getMessage(), e);
		}
	}
}