/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.texture;

import fr.ifremer.globe.ui.texture.basic.BasicTextureCoordinate;

/**
 * Coordinates in texture corresponding to the vec4 vTexCoord passed to the shader. <br>
 * This relates the pixel on the screen to its position in the image
 */
public interface ITextureCoordinate {

	/** OpenGL convention for the abscissa in a texture. From 0f to 1f */
	float getS();

	/** OpenGL convention for the ordinate in a texture. From 0f to 1f */
	float getT();

	/** Abscissa of this coordinate in the real world. */
	float getX();

	/** Ordinate of this coordinate in the real world. */
	float getY();

	/** Return an instance of ITextureCoordinate */
	static ITextureCoordinate of(float s, float t, float x, float y) {
		return new BasicTextureCoordinate(s, t, x, y);
	}

}
