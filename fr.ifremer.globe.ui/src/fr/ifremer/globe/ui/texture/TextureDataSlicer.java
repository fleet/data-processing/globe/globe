/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.texture;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;

import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.ui.texture.basic.BasicTextureData;
import fr.ifremer.globe.ui.texture.basic.IBasicTextureData;

/**
 * Utility class for slicing ITextureData in smaller one. <br>
 * This template method may be sub-classed to instanciate desired implementation of ITextureData and ITextureCoordinate
 */
public class TextureDataSlicer {

	/**
	 * Splits the provided {@link ITextureData} in several smaller {@link ITextureData}.
	 *
	 * @param src : input {@link ITextureData}, will not be modified.
	 * @param sliceWidth : width of each sub {@link TextureData}
	 * @param sliceHeight : height of each sub {@link TextureData}
	 *
	 * @return a matrix of {@link ITextureData}, resulting of the split. (ITextureData[row][column])
	 */
	public IBasicTextureData[][] slice(IBasicTextureData src, int sliceWidth, int sliceHeight) {
		int colCount = (int) Math.ceil((double) src.getWidth() / sliceWidth);
		int rowCount = (int) Math.ceil((double) src.getHeight() / sliceHeight);

		var result = (IBasicTextureData[][]) Array.newInstance(IBasicTextureData.class, rowCount, colCount);
		// no split required
		if (colCount == 1 && rowCount == 1) {
			result[0][0] = src;
			return result;
		}

		for (int row = 0; row < rowCount; row++) {
			int rowStart = row * sliceHeight;
			int height = Math.min(sliceHeight, src.getHeight() - rowStart);
			for (int col = 0; col < colCount; col++) {
				int colStart = col * sliceWidth;
				int width = Math.min(sliceWidth, src.getWidth() - colStart);
				result[row][col] = slice(src, colStart, rowStart, width, height);
			}
		}

		return result;
	}

	/**
	 * Cut a part of the provided {@link ITextureData}.
	 *
	 * @return new {@link ITextureData}
	 */
	private IBasicTextureData slice(IBasicTextureData src, int colStart, int rowStart, int width, int height) {
		// create a new buffer (a slice of the source buffer)
		ByteBuffer buffer = src.getBuffer();
		var previousPosition = buffer.position();
		ByteBuffer newBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(ByteOrder.nativeOrder());
		int rowEnd = rowStart + height;
		for (int row = rowStart; row < rowEnd; row++) {
			byte[] dst = new byte[width * Float.BYTES];
			int readPosition = (row * src.getWidth() + colStart) * Float.BYTES;
			buffer.position(readPosition);
			buffer.get(dst, 0, width * Float.BYTES);
			newBuffer.put(dst);
		}
		buffer.position(previousPosition);
		newBuffer.rewind();

		// compute slice texture coordinates (s & t)
		Range sRange = new DoubleRange((double) colStart / src.getWidth(),
				(double) (colStart + width) / src.getWidth());
		Range tRange = new DoubleRange((double) rowStart / src.getHeight(),
				(double) (rowStart + height) / src.getHeight());

		List<ITextureCoordinate> newCoordinates = new ArrayList<>();
		float minX = (float) (sRange.getMinimumDouble() * (src.getWidth() - 1));
		float maxX = (float) (sRange.getMaximumDouble() * (src.getWidth() - 1));
		float minY = (float) (tRange.getMinimumDouble() * (src.getHeight() - 1));
		float maxY = (float) (tRange.getMaximumDouble() * (src.getHeight() - 1));
		newCoordinates.add(ITextureCoordinate.of(0f, 0f, minX, minY));
		newCoordinates.add(ITextureCoordinate.of(0f, 1f, minX, maxY));
		newCoordinates.add(ITextureCoordinate.of(1f, 0f, maxX, minY));
		newCoordinates.add(ITextureCoordinate.of(1f, 1f, maxX, maxY));

		return new BasicTextureData(width, height, newBuffer, newCoordinates);
	}
}
