package fr.ifremer.globe.ui.texture.basic;

import fr.ifremer.globe.ui.texture.ITextureCoordinate;

/**
 * Plain implementation of ITextureCoordinate
 */
public class BasicTextureCoordinate implements ITextureCoordinate {

	private final float s;
	private final float t;
	private final float x;
	private final float y;

	/**
	 * Constructor
	 */
	public BasicTextureCoordinate(float s, float t, float x, float y) {
		this.s = s;
		this.t = t;
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the {@link #s}
	 */
	@Override
	public float getS() {
		return s;
	}

	/**
	 * @return the {@link #t}
	 */
	@Override
	public float getT() {
		return t;
	}

	/**
	 * @return the {@link #x}
	 */
	@Override
	public float getX() {
		return x;
	}

	/**
	 * @return the {@link #y}
	 */
	@Override
	public float getY() {
		return y;
	}
}
