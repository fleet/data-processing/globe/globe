/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.texture.basic;

import fr.ifremer.globe.ui.texture.ITextureCoordinate;
import fr.ifremer.globe.ui.texture.ITextureData;

/** Useful interface for writing simplifications */
public interface IBasicTextureData extends ITextureData<ITextureCoordinate> {
}
