/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.texture.basic;

import java.nio.ByteBuffer;
import java.util.List;

import fr.ifremer.globe.ui.texture.ITextureCoordinate;

/**
 * Basic implementation of IBasicTextureData
 */
public class BasicTextureData implements IBasicTextureData {

	private final int width;
	private final int height;
	private final ByteBuffer buffer;
	private final List<ITextureCoordinate> coordinates;

	/**
	 * Constructor
	 */
	public BasicTextureData(int width, int height, ByteBuffer buffer, List<ITextureCoordinate> coordinates) {
		this.width = width;
		this.height = height;
		this.buffer = buffer;
		this.coordinates = coordinates;
	}

	/**
	 * Creates an instance of BasicTextureData for a rendering with the first pixel at the top left
	 */
	public BasicTextureData(int width, int height, ByteBuffer buffer) {
		this(width, height, buffer, List.of(//
				ITextureCoordinate.of(0f, 0f, 0f, 0f), //
				ITextureCoordinate.of(0f, 1f, 0f, height), //
				ITextureCoordinate.of(1f, 0f, width, 0f), //
				ITextureCoordinate.of(1f, 1f, width, height)));
	}

	/**
	 * @return the {@link #width}
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/**
	 * @return the {@link #height}
	 */
	@Override
	public int getHeight() {
		return height;
	}

	/**
	 * @return the {@link #buffer}
	 */
	@Override
	public ByteBuffer getBuffer() {
		return buffer;
	}

	/**
	 * @return the {@link #coordinates}
	 */
	@Override
	public List<ITextureCoordinate> getCoordinates() {
		return coordinates;
	}

	@Override
	public int getCoordGridHeight() {
		return 2;
	}

	@Override
	public int getCoordGridWidth() {
		return 2;
	}

}
