#version 400 compatibility

uniform sampler2D texture;
uniform int isThresholdEnabled;
uniform float minThreshold;
uniform float maxThreshold;

uniform float resolution;
uniform float minValue;


in vec4 vTexCoord;

out vec4 out_Color;

// float to uint
uint encode(float value)
{
	return uint((value - minValue)/resolution);
}

// rgba8Unorm to vec4
vec4 unpack(uint value)
{
	return unpackUnorm4x8(value);
}

void main()
{
	// Get value from the texture
	float value = texture2D(texture, vTexCoord.st).a;

	// Set opactiy (0 if value == NaN)
	float opacity = isnan(value) ? 0.0 : 1.0;
	
	// Apply threshold filter
	if(isThresholdEnabled == 1)
		opacity = value < minThreshold || value > maxThreshold ? 0 : opacity;
	
	// Discard the fragment if opactity == 0 (avoid issue with transparency & NaN values...Quick fix, not the best solution)
	if (opacity == 0)
		discard; 

	// Apply this color to the fragment
	out_Color = unpack(encode(value));
}
