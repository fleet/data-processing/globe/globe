/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.texture;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Represents a texture object that can be rendered with a TextureRenderer
 *
 * @param <T> type of the coordinates in texture
 */
public interface ITextureData<C extends ITextureCoordinate> {

	/**
	 * @return texture width.
	 */
	int getWidth();

	/**
	 * @return texture height.
	 */
	int getHeight();

	/**
	 * Texture data directly used to initialize a com.jogamp.opengl.util.texture.TextureData
	 */
	ByteBuffer getBuffer();

	/** Returns a float value peeked in the buffer */
	default float getFloat(int col, int row) {
		int position = (row * getWidth() + col) * Float.BYTES;
		if (position >= 0 && position < getBuffer().capacity() - Float.BYTES) {
			return getBuffer().getFloat(position);
		}
		return Float.NaN;
	}

	/**
	 * @return coordinates grid height.
	 */
	int getCoordGridHeight();

	/**
	 * @return coordinates grid width.
	 */
	int getCoordGridWidth();

	/**
	 * List of coordinates in texture.<br>
	 * The first 2 coordinates correspond to the top and bottom points of the leftmost pixel column of the texture.<br>
	 * The last 2 coordinates correspond to the top and bottom points of the rightmost pixel column of the texture.<br>
	 */
	List<C> getCoordinates();
}
