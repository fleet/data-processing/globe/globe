/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.texture;

import java.nio.ByteBuffer;
import java.util.DoubleSummaryStatistics;

import org.apache.commons.lang.math.FloatRange;

import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;

/**
 * Rendering parameters to display a Texture <br>
 * An instance of this class is immutable
 */
public class TextureDataParameters {

	/** Display parameters **/
	public final float opacity;
	public final float minContrast;
	public final float maxContrast;
	public final boolean isThesholdEnabled;
	public final float maxThreshold;
	public final float minThreshold;
	public final int colorMapIndex;
	public final boolean isColorMapReversed;
	public final boolean isInterpolationEnabled;

	/**
	 * Constructor
	 */
	public TextureDataParameters(float minContrast, float maxContrast) {
		this(minContrast, maxContrast, ColorMaps.UDIV_CMOCEAN_DELTA.getIndex());
	}

	/**
	 * Constructor
	 */
	public TextureDataParameters(float minContrast, float maxContrast, int colorMapindex) {
		this(1, minContrast, maxContrast, colorMapindex, false);
	}

	/**
	 * Constructor
	 */
	public TextureDataParameters(float opacity, float minContrast, float maxContrast, int colorMapindex,
			boolean isInverseColorMap) {
		this(opacity, minContrast, maxContrast, minContrast, maxContrast, colorMapindex, isInverseColorMap, false,
				false);
	}

	/**
	 * Constructor
	 */
	public TextureDataParameters(float opacity, float minContrast, float maxContrast, float minThreshold,
			float maxThreshold, int colorMapIndex, boolean isColorMapReversed, boolean isThesholdEnabled,
			boolean isInterpolationEnabled) {
		this.opacity = opacity;
		this.minContrast = minContrast;
		this.maxContrast = maxContrast;
		this.colorMapIndex = colorMapIndex;
		this.isColorMapReversed = isColorMapReversed;
		this.isThesholdEnabled = isThesholdEnabled;
		this.minThreshold = minThreshold;
		this.maxThreshold = maxThreshold;
		this.isInterpolationEnabled = isInterpolationEnabled;
	}

	/** Duplicates this TextureRenderSettings with an other value of opacity */
	public TextureDataParameters withOpacity(float opacity) {
		return new TextureDataParameters(opacity, minContrast, maxContrast, minThreshold, maxThreshold, colorMapIndex,
				isColorMapReversed, isThesholdEnabled, isInterpolationEnabled);
	}

	/** Duplicates this TextureRenderSettings with an other color definition */
	public TextureDataParameters withColor(boolean isColorMapReversed, int colorMapIndex, float minContrast,
			float maxContrast, float opacity) {
		return new TextureDataParameters(opacity, minContrast, maxContrast, minThreshold, maxThreshold, colorMapIndex,
				isColorMapReversed, isThesholdEnabled, isInterpolationEnabled);
	}

	/**
	 * Creates a TextureDataParameters for the specified texture. Min and max contrast are set to the min and max values
	 * of the texture
	 */
	public static TextureDataParameters defaultFor(ITextureData<?> texture) {
		DoubleSummaryStatistics stats = computeStats(texture);
		return new TextureDataParameters((float) stats.getMin(), (float) stats.getMax());
	}

	/**
	 * Adapts this TextureDataParameters to the specified texture. Min and max contrast are set to the min and max
	 * values of the texture
	 */
	public TextureDataParameters adaptTo(ITextureData<?> texture) {
		DoubleSummaryStatistics stats = computeStats(texture);
		if (minContrast == minThreshold && maxContrast == maxThreshold) {
			return new TextureDataParameters(opacity, (float) stats.getMin(), (float) stats.getMax(),
					(float) stats.getMin(), (float) stats.getMax(), colorMapIndex, isColorMapReversed,
					isThesholdEnabled, isInterpolationEnabled);
		}
		// Keep Threshold if changed by user
		FloatRange contrast = new FloatRange((float) stats.getMin(), (float) stats.getMax());
		if (contrast.containsFloat(minContrast)) {
			contrast = new FloatRange(minContrast, contrast.getMaximumFloat());
		}
		if (contrast.containsFloat(maxContrast)) {
			contrast = new FloatRange(contrast.getMinimumFloat(), maxContrast);
		}
		return new TextureDataParameters(opacity, contrast.getMinimumFloat(), contrast.getMaximumFloat(),
				(float) stats.getMin(), (float) stats.getMax(), colorMapIndex, isColorMapReversed, isThesholdEnabled,
				isInterpolationEnabled);
	}

	/**
	 * @return the stats of the texture float buffer
	 */
	private static DoubleSummaryStatistics computeStats(ITextureData<?> texture) {
		DoubleSummaryStatistics result = new DoubleSummaryStatistics();
		ByteBuffer buffer = texture.getBuffer();
		buffer.rewind();
		while (buffer.hasRemaining()) {
			float value = buffer.getFloat();
			if (Float.isFinite(value)) {
				result.accept(value);
			}
		}

		return result;
	}

}
