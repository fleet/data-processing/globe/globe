/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewers;

import java.util.function.BiConsumer;
import java.util.function.Function;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * EditingSupport to edit String value of a cell
 */
public class StringEditingSupport<T> extends EditingSupport {

	private final Function<T, String> valueSupplier;
	private final BiConsumer<T, String> valueSetter;

	/**
	 * Constructor
	 */
	public StringEditingSupport(ColumnViewer viewer, Function<T, String> valueSupplier,
			BiConsumer<T, String> valueSetter) {
		super(viewer);
		this.valueSupplier = valueSupplier;
		this.valueSetter = valueSetter;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		Control control = getViewer().getControl();
		return new TextCellEditor(control instanceof Composite ? (Composite) control : control.getParent());
	}

	@Override
	protected boolean canEdit(Object element) {
		return element != null;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Object getValue(Object element) {
		return valueSupplier.apply((T) element);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setValue(Object element, Object value) {
		valueSetter.accept((T) element, value.toString());
	}

}
