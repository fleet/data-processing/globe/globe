/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewers;

import java.util.function.Predicate;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.ResourceManager;

/**
 * CellLabelProvider used to render a Chekbox for representing a boolean
 */
public class CheckBoxCellLabelProvider<T> extends CellLabelProvider {

	private static final Image CHECK_ICON = ResourceManager.getPluginImage("fr.ifremer.3dviewer", "icons/16/check.png");
	private static final Image UNCHECK_ICON = ResourceManager.getPluginImage("fr.ifremer.3dviewer",
			"icons/16/uncheck.png");

	/** How to get the boolean value for a cell */
	private final Predicate<T> getter;

	/**
	 * Constructor
	 */
	public CheckBoxCellLabelProvider(Predicate<T> getter) {
		this.getter = getter;
	}

	/** {@inheritDoc} */
	@Override
	@SuppressWarnings("unchecked")
	public void update(ViewerCell cell) {
		cell.setImage(getter.test((T) cell.getElement()) ? CHECK_ICON : UNCHECK_ICON);
	}
}
