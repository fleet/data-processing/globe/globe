/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewers;

import java.util.function.Function;

import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

/**
 * CellLabelProvider used to render a GColor
 */
public class ColorCellLabelProvider<T> extends StyledCellLabelProvider {

	/** How to get the boolean value for a cell */
	private final Function<T, GColor> getter;

	/**
	 * Constructor
	 */
	public ColorCellLabelProvider(Function<T, GColor> getter) {
		this.getter = getter;
	}

	/** {@inheritDoc} */
	@Override
	@SuppressWarnings("unchecked")
	public void update(ViewerCell cell) {
		cell.setBackground(ColorUtils.convertGColorToSWT(getter.apply((T) cell.getElement())));
	}

	/**
	 * Override to make sur the color is always visible
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void erase(Event event, Object element) {
		event.detail &= ~SWT.HOT;
		if ((event.detail & SWT.SELECTED) != 0) {
			event.detail &= ~SWT.SELECTED;

			Rectangle bounds = event.getBounds();
			event.gc.setBackground(ColorUtils.convertGColorToSWT(getter.apply((T) element)));
			event.gc.fillRectangle(bounds);
		}

		super.erase(event, element);
	}

	/**
	 * Override to make sur the color is always visible
	 */
	@Override
	protected void paint(Event event, Object element) {
		event.detail &= ~SWT.SELECTED;
		event.detail &= ~SWT.HOT;
		super.paint(event, element);
	}

	/**
	 * Override to make sur the color is always visible
	 */
	@Override
	protected void measure(Event event, Object element) {
		event.detail &= ~SWT.SELECTED;
		event.detail &= ~SWT.HOT;
		super.measure(event, element);
	}

}
