/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewers;

import java.util.function.BiConsumer;
import java.util.function.Function;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColorCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * EditingSupport used to render and modify a GColor
 */
public class ColorEditingSupport<T> extends EditingSupport {

	/** How to get the color from the object */
	private final Function<T, GColor> getter;
	/** How to set the color to the object */
	private final BiConsumer<T, GColor> setter;

	/**
	 * Constructor
	 */
	public ColorEditingSupport(ColumnViewer viewer, Function<T, GColor> getter, BiConsumer<T, GColor> setter) {
		super(viewer);
		this.getter = getter;
		this.setter = setter;
	}

	/** {@inheritDoc} */
	@Override
	protected CellEditor getCellEditor(Object element) {
		return new ColorCellEditor((Composite) getViewer().getControl());
	}

	/** {@inheritDoc} */
	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	protected Object getValue(Object element) {
		GColor color = getter.apply((T) element);
		return new RGB(color.getRed(), color.getGreen(), color.getBlue());
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	protected void setValue(Object element, Object rgb) {
		setter.accept((T) element, new GColor(((RGB) rgb).red, ((RGB) rgb).green, ((RGB) rgb).blue));
	}

}
