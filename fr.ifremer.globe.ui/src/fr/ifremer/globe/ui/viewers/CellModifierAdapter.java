/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewers;

import org.eclipse.jface.viewers.ICellModifier;

/**
 * This adapter class provides default implementations for the methods described by the <code>ICellModifier</code>
 * interface.
 */
public class CellModifierAdapter implements ICellModifier {

	/**
	 * Constructor
	 */
	public CellModifierAdapter() {
	}

	/**
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean canModify(Object element, String property) {
		return true;
	}

	/**
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object getValue(Object element, String property) {
		return null;
	}

	/**
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
	 */
	@Override
	public void modify(Object element, String property, Object value) {
	}

}
