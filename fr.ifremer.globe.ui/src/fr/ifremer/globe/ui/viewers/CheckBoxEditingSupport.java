/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewers;

import java.util.function.Predicate;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.swt.SWT;

import fr.ifremer.globe.utils.function.ObjBooleanConsumer;

/**
 * EditingSupport used to render a Chekbox for modifying a boolean value
 */
public class CheckBoxEditingSupport<T> extends EditingSupport {

	/** How to get the boolean value from the objec */
	private final Predicate<T> getter;
	/** How to set the boolean value to the object */
	private final ObjBooleanConsumer<T> setter;

	/**
	 * Constructor
	 */
	public CheckBoxEditingSupport(ColumnViewer viewer, Predicate<T> getter, ObjBooleanConsumer<T> setter) {
		super(viewer);
		this.getter = getter;
		this.setter = setter;
	}

	/** {@inheritDoc} */
	@Override
	protected CellEditor getCellEditor(Object element) {
		return new CheckboxCellEditor(getViewer().getControl().getParent(), SWT.CHECK);
	}

	/** {@inheritDoc} */
	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	protected Object getValue(Object element) {
		return getter.test((T) element);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	protected void setValue(Object element, Object value) {
		setter.accept((T) element, value == Boolean.TRUE);
	}

}
