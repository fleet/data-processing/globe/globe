/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.parts;

import java.util.function.Consumer;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.IPresentationEngine;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.event.EventHandler;

import fr.ifremer.globe.ui.handler.PartManager;

/**
 * Utility class to show parts
 */
public class PartUtil {
	/**
	 * This class is not meant to be instantiated.
	 */
	private PartUtil() {
	}

	/** Part should be made visible and activated */
	public static void activatePart(MApplication application, EPartService partService, String partId) {
		if (application != null && partService != null) {
			Display.getDefault().asyncExec(() -> {
				// Workaround: Application does not have an active window exception...
				MWindow mWindow = application.getChildren().get(0);
				if (mWindow != null && mWindow.getContext() != null) {
					mWindow.getContext().activate();

					partService.showPart(partId, PartState.ACTIVATE);
				}
			});
		}
	}

	/** Part should be made visible and activated */
	public static void activatePart(MApplication application, EPartService partService, MPart part) {
		if (application != null && partService != null) {
			Display.getDefault().asyncExec(() -> {
				// Workaround: Application does not have an active window exception...
				MWindow mWindow = application.getChildren().get(0);
				if (mWindow != null && mWindow.getContext() != null) {
					mWindow.getContext().activate();

					partService.showPart(part, PartState.ACTIVATE);
				}
			});
		}
	}

	/** Part should be made visible and activated */
	public static void showPart(MApplication application, EPartService partService, String partId) {
		if (application != null && partService != null) {
			Display.getDefault().asyncExec(() -> {
				// Workaround: Application does not have an active window exception...
				MWindow mWindow = application.getChildren().get(0);
				if (mWindow != null && mWindow.getContext() != null) {
					mWindow.getContext().activate();

					partService.showPart(partId, PartState.VISIBLE);
				}
			});
		}
	}

	/** Part should be made visible and activated */
	public static void showPart(MApplication application, EPartService partService, MPart part) {
		if (application != null && partService != null) {
			Display.getDefault().asyncExec(() -> {
				// Workaround: Application does not have an active window exception...
				MWindow mWindow = application.getChildren().get(0);
				if (mWindow != null && mWindow.getContext() != null) {
					mWindow.getContext().activate();

					partService.showPart(part, PartState.VISIBLE);
				}
			});
		}
	}

	/** Hides the given part */
	public static void hidePart(MApplication application, EPartService partService, String partId) {
		if (application != null && partService != null) {
			Display.getDefault().asyncExec(() -> {
				// Workaround: Application does not have an active window exception...
				MWindow mWindow = application.getChildren().get(0);
				if (mWindow != null && mWindow.getContext() != null) {
					mWindow.getContext().activate();

					MPart part = partService.findPart(partId);
					if (part != null) {
						partService.hidePart(part, true);
					}
				}
			});
		}
	}

	/** Hides the given part */
	public static void hidePart(MApplication application, EPartService partService, MPart part) {
		if (application != null && partService != null) {
			Display.getDefault().asyncExec(() -> {
				// Workaround: Application does not have an active window exception...
				MWindow mWindow = application.getChildren().get(0);
				if (mWindow != null && mWindow.getContext() != null) {
					mWindow.getContext().activate();

					partService.hidePart(part);
				}
			});
		}
	}

	/** Hides the given part */
	public static MPart findPart(MApplication application, EPartService partService, String partId) {
		if (application != null && partService != null) {
			// Workaround: Application does not have an active window exception...
			MWindow mWindow = application.getChildren().get(0);
			if (mWindow != null && mWindow.getContext() != null) {
				mWindow.getContext().activate();

				return partService.findPart(partId);
			}
		}
		return null;
	}

	/** Creates a new part of the given id */
	public static MPart createPart(MApplication application, EPartService partService, String partId) {
		if (application != null && partService != null) {
			// Workaround: Application does not have an active window exception...
			MWindow mWindow = application.getChildren().get(0);
			if (mWindow != null && mWindow.getContext() != null) {
				mWindow.getContext().activate();
				return partService.createPart(partId);
			}
		}
		return null;
	}

	/** Opens part in a detached window **/
	public static MPart openDetachedPart(EPartService partService, EModelService modelService, String partId, int width,
			int height) {
		MPart part = PartManager.createPart(partService, partId);
		PartManager.showPart(partService, part);
		if (!part.getTags().contains("active")) {
			Point cursorLocation = Display.getDefault().getCursorLocation();
			modelService.detach(part, cursorLocation.x - Math.round(1.25f * width),
					cursorLocation.y - Math.round(0.7f * height), width, height);
		}
		return part;
	}

	/** Prepares the PartSashContainer to receive a view */
	public static void showPartSashContainer(MPart part) {
		MElementContainer<MUIElement> parent = part.getParent();
		if (parent != null) {
			// In case the PlaceHolder has never been displayed before
			parent.setVisible(true);
			// In case the PlaceHolder is minimized
			parent.getTags().remove(IPresentationEngine.MINIMIZED);
		}

	}

	/**
	 * Subscribe to EventBroker and reacts when a view is added to a PartStack.
	 * 
	 * @return created EventHandler. Must un-subscribe it to IEventBroker when job done.
	 */
	public static EventHandler onPartStackChanged(String partId, IEventBroker eventBroker,
			Consumer<MPartStack> partStackConsumer) {
		EventHandler eventHandler = event -> {
			if (UIEvents.isADD(event) // Part add to a partStack
					&& event.getProperty(UIEvents.EventTags.ELEMENT) instanceof MPartStack partStack
					&& event.getProperty(UIEvents.EventTags.NEW_VALUE) instanceof MPart part
					&& partId.equals(part.getElementId())) {
				partStackConsumer.accept(partStack);
			}
		};
		eventBroker.subscribe(UIEvents.ElementContainer.TOPIC_CHILDREN, eventHandler);
		return eventHandler;
	}

}
