package fr.ifremer.globe.ui.parts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.ui.IDataEditor;
import fr.ifremer.globe.utils.FileUtils;

public class TextEditorView implements ISaveable, IDataEditor {

	protected Logger logger = LoggerFactory.getLogger(TextEditorView.class);

	private static final List<String> acceptedExtensions = List.of("txt", "ttb", "xml", "geojson", "csv", "json", "cot", "emo", "xyz");

	private MPart part;

	private File editedFile;

	private boolean dirty = false;

	private ModifyListener modListener;

	private Text editor;

	public TextEditorView() {

		modListener = new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent event) {
				if (!dirty) {
					dirty = true;
					part.setLabel("* " + part.getLabel());
				}
			}
		};
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@PostConstruct
	public void createPartControl(Composite parent) {

		editor = new Text(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		editor.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Focus
	public void setFocus() {
	}

	@Override
	public List<String> getFileNameList() {
		List<String> filenameList = new ArrayList<String>();
		filenameList.add(editedFile.getAbsolutePath());
		return filenameList;
	}

	@Override
	@PreDestroy
	public void onClose() {
		if (isDirty()) {
			if (Display.getDefault().getActiveShell() != null) {
				MessageBox messageBox = new MessageBox(Display.getDefault().getActiveShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
				messageBox.setText("Save Resource");
				messageBox.setMessage('"' + part.getLabel().substring(2) + '"' + " has been modified. Save changes?");
				int answer = messageBox.open();
				switch (answer) {
				case SWT.YES:
					ProgressMonitorDialog dialog = new ProgressMonitorDialog(Display.getDefault().getActiveShell());
					try {
						dialog.run(true, true, new IRunnableWithProgress() {
							@Override
							public void run(IProgressMonitor monitor) {
								monitor.beginTask("Saving Text Editor...", 100);
								TextEditorView.this.doSave(monitor);
							}
						});
					} catch (InvocationTargetException e) {
						logger.error("Can't save files on TextEditor close");
					} catch (InterruptedException e) {
						logger.error("Can't save files on TextEditor close");
					}
					break;
				case SWT.NO:
					break;

				}
			}
		}
		dirty = false;
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	public boolean doSave(IProgressMonitor monitor) {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter(editedFile));
			output.write(editor.getText());
			output.close();
			dirty = false;
			if (part.getLabel().startsWith("*")) {
				part.setLabel(part.getLabel().substring(2));
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e);

		}
		return true;
	}

	public void setPart(MPart value) {
		this.part = value;
	}

	public static boolean acceptFile(File pFile) {
		return pFile.length() < 20_000_000l && acceptedExtensions.contains(FileUtils.getFileExtension(pFile).toLowerCase());
	}

	public void openFile(File pFile) {
		editedFile = pFile;
		editor.setText("");
		editor.removeModifyListener(modListener);
		dirty = false;
		try {
			BufferedReader input = new BufferedReader(new FileReader(editedFile));
			try {
				StringBuffer strBuff = new StringBuffer((int) editedFile.length());
				String line = null;
				while ((line = input.readLine()) != null) {
					strBuff.append(line + "\n");
				}
				editor.setText(strBuff.toString());
			} finally {
				input.close();
			}
		} catch (IOException ex) {
			logger.error(ex.getMessage());
		}
		editor.addModifyListener(modListener);
	}

}
