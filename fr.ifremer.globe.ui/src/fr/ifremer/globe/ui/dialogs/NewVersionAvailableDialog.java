package fr.ifremer.globe.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.utils.GlobeVersion;

public class NewVersionAvailableDialog extends Dialog {

	private final String newVersion;

	public NewVersionAvailableDialog(Shell parentShell, String newVersion) {
		super(parentShell);
		this.newVersion = newVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Globe : new version available!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = (Composite) super.createDialogArea(parent);
		GridLayout glComposite = new GridLayout(2, false);
		glComposite.marginWidth = 10;
		glComposite.marginHeight = 10;
		glComposite.verticalSpacing = 10;
		glComposite.horizontalSpacing = 10;
		composite.setLayout(glComposite);

		applyDialogFont(composite);

		Label labelImage = new Label(composite, SWT.NONE);
		labelImage.setImage(Display.getDefault().getSystemImage(SWT.ICON_INFORMATION));
		labelImage.setFont(parent.getFont());

		Label lblANewVersion = new Label(composite, SWT.NONE);
		lblANewVersion.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblANewVersion.setText("A new version of Globe is available : " + newVersion);
		new Label(composite, SWT.NONE);

		// New version available link
		Link lvlNewVersionAvailble = new Link(composite, SWT.NONE);
		lvlNewVersionAvailble.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		lvlNewVersionAvailble.setText("Download at : <A HREF=\"" + GlobeVersion.GLOBE_DOWNLOAD_WEBSITE + "\">"
				+ GlobeVersion.GLOBE_DOWNLOAD_WEBSITE + "</A>");

		lvlNewVersionAvailble.addListener(SWT.Selection,
				e -> org.eclipse.swt.program.Program.launch(GlobeVersion.GLOBE_DOWNLOAD_WEBSITE));

		return composite;
	}

}
