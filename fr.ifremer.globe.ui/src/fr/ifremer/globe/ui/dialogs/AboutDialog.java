package fr.ifremer.globe.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.utils.GlobeVersion;
import swing2swt.layout.BorderLayout;

public class AboutDialog extends Dialog {

	public AboutDialog(Shell parentShell) {
		super(parentShell);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("About Globe");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new BorderLayout());

		Composite imageComposite = new Composite(composite, SWT.NONE);
		imageComposite.setLayoutData(BorderLayout.WEST);
		imageComposite.setLayout(new GridLayout(1, false));

		Composite textComposite = new Composite(composite, SWT.NONE);
		textComposite.setLayoutData(BorderLayout.CENTER);
		textComposite.setLayout(new GridLayout(2, false));

		Label labelImage = new Label(imageComposite, SWT.NONE);
		labelImage.setImage(Display.getDefault().getSystemImage(SWT.ICON_INFORMATION));
		labelImage.setFont(parent.getFont());

		// GLOBE
		// Title label
		Label labelTitle = new Label(textComposite, SWT.NONE);
		labelTitle.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		labelTitle.setText("Globe - 3D Viewer");
		labelTitle.setFont(parent.getFont());
		labelTitle.setFont(new Font(Display.getDefault(), "Arial", 14, SWT.NONE));
		labelTitle.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_BLUE));

		// Version label
		Label labelVersion = new Label(textComposite, SWT.NONE);
		labelVersion.setText(GlobeVersion.VERSION);
		labelVersion.setFont(parent.getFont());

		// New version available link
		Link lvlNewVersionAvailble = new Link(textComposite, SWT.NONE);
		GlobeVersion.checkIfNewVersionAvailable().ifPresent(newVersion -> {

			lvlNewVersionAvailble.setText("(Version " + newVersion + " available on : <A HREF=\""
					+ GlobeVersion.GLOBE_DOWNLOAD_WEBSITE + "\">" + GlobeVersion.GLOBE_DOWNLOAD_WEBSITE + "</A>)");

			lvlNewVersionAvailble.addListener(SWT.Selection,
					e -> org.eclipse.swt.program.Program.launch(GlobeVersion.GLOBE_DOWNLOAD_WEBSITE));
		});

		// IfremerContact label
		Label labelIfremerContact = new Label(textComposite, SWT.NONE);
		labelIfremerContact.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		labelIfremerContact.setText("IFREMER");
		labelIfremerContact.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));

		Link labelIfremerContact2 = new Link(textComposite, SWT.NONE);
		labelIfremerContact2.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		labelIfremerContact2.setText(
				"For more information, please contact <A HREF=\"mailto:globe-assistance@ifremer.fr\">globe-assistance@ifremer.fr</A>");
		labelIfremerContact2.setFont(parent.getFont());
		labelIfremerContact2.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				org.eclipse.swt.program.Program.launch("mailto:globe-assistance@ifremer.fr");
			}
		});

		// NASAContribution label
		Link labelNASAContribution = new Link(textComposite, SWT.NONE);
		labelNASAContribution.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		labelNASAContribution.setText(
				"Software based on NasaWorldWind. \nFor more information on its licence : <A HREF=\"http://ti.arc.nasa.gov/opensource/nosa/\">http://ti.arc.nasa.gov/opensource/nosa/</A> ");
		labelNASAContribution.setFont(parent.getFont());
		labelNASAContribution.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				org.eclipse.swt.program.Program.launch("http://ti.arc.nasa.gov/opensource/nosa/");
			}
		});

		// Seismicio label
		Link labelSeismicioContribution = new Link(textComposite, SWT.NONE);
		labelSeismicioContribution.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		labelSeismicioContribution.setText(
				"Using Seismic I/O - Seismic access library for parsing data from SEG-Y format. \nFor more information on its licence : <A HREF=\"https://petroware.no/html/seismicio.html\">https://petroware.no/html/seismicio.html</A> ");
		labelSeismicioContribution.setFont(parent.getFont());
		labelSeismicioContribution.addListener(SWT.Selection,
				e -> org.eclipse.swt.program.Program.launch("https://petroware.no/html/seismicio.html"));

		// Icons label
		Link labelIconsContribution = new Link(textComposite, SWT.NONE);
		labelIconsContribution.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		labelIconsContribution.setText(
				"Icons of time editor are made by : Situ Herrera, Gregor Cresnar, Eleonor Wang, Freepik and Madebyoliver. \nFor more informations on its licence : <A HREF=\"https://creativecommons.org/licenses/by/3.0/\">https://creativecommons.org/licenses/by/3.0/</A>");
		labelIconsContribution.setFont(parent.getFont());
		labelIconsContribution.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				org.eclipse.swt.program.Program.launch("https://creativecommons.org/licenses/by/3.0/");
			}
		});

		applyDialogFont(composite);

		return composite;
	}
}
