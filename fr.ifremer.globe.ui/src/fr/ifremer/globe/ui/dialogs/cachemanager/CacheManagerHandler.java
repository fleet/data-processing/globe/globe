package fr.ifremer.globe.ui.dialogs.cachemanager;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class CacheManagerHandler {

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		CacheManagerDialog dialog = new CacheManagerDialog(Display.getCurrent().getActiveShell());
		dialog.open();
	}
}
