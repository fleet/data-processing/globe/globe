package fr.ifremer.globe.ui.dialogs.cachemanager;

import java.io.File;
import java.util.List;
import java.util.function.Supplier;

import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.cache.WorkspaceCache;

/**
 * List of cache directories.
 */
public enum Cache {

	NASA_CACHE("Worlwind", () -> FileUtils.getSubDirectories(new File(NasaCache.getRootPath()), false)),

	NASA_BACKGROUND_CACHE("Worlwind background",
			() -> FileUtils.getSubDirectories(new File(NasaCache.getEarthCachePath()), false)),

	WORKSPACE_CACHE("Workspace", () -> FileUtils.getSubDirectories(new File(WorkspaceCache.getRootPath()), true)),

	TEMPORARY_CACHE("Temporary files", TemporaryCache::getTemporaryCacheFiles);

	public final String title;
	public final Supplier<List<File>> fileSupplier;

	Cache(String title, Supplier<List<File>> fileSupplier) {
		this.title = title;
		this.fileSupplier = fileSupplier;
	}
}
