package fr.ifremer.globe.ui.dialogs.cachemanager;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.TableViewerColumnSorter;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;

/**
 * Contains all components displays in a tab of {@link CacheManagerDialog}.
 */
public class CacheComposite extends Composite {

	private static final Logger LOGGER = LoggerFactory.getLogger(CacheComposite.class);

	/**
	 * Inner class to define an item of the table
	 */
	private class CacheCompositeItem {
		final File file;
		String status = "";

		public CacheCompositeItem(File file) {
			this.file = file;
		}
	}

	/** Model **/
	private final Cache cacheModel;
	public final WritableObjectList<CacheCompositeItem> items = new WritableObjectList<>();

	/** Inner composites */
	private TableViewer tableViewer;
	private Button btnSelectAll;
	private Button btnUnselectAll;
	private Button btnDelete;

	/** Scheduler to update items **/
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	/**
	 * Constructor
	 */
	public CacheComposite(Composite parent, Cache cacheModel) {
		super(parent, SWT.NONE);
		this.setLayout(new GridLayout(3, false));
		this.cacheModel = cacheModel;
		initializeTable();
		initializeButtons();
		scheduler.scheduleAtFixedRate(this::updateItems, 0, 1, TimeUnit.SECONDS);
	}

	@Override
	public void dispose() {
		scheduler.shutdown();
	}

	/**
	 * Initializes the table.
	 */
	protected void initializeTable() {
		tableViewer = CheckboxTableViewer.newCheckList(this, SWT.BORDER | SWT.FULL_SELECTION);
		tableViewer.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		tableViewer.getTable().setHeaderVisible(true);
		tableViewer.getTable().addListener(SWT.MouseDown, e -> updateButtons());
		tableViewer.setContentProvider(new ObservableListContentProvider<CacheCompositeItem>());

		// column "Path"
		var tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		tableViewerColumn.setLabelProvider(
				ColumnLabelProvider.createTextProvider(i -> ((CacheCompositeItem) i).file.getAbsolutePath()));
		new TableViewerColumnSorter(tableViewerColumn);
		var tableColumn = tableViewerColumn.getColumn();
		tableColumn.setText("Path");
		tableColumn.setWidth(600);

		// column "Status"
		tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		tableViewerColumn
				.setLabelProvider(ColumnLabelProvider.createTextProvider(i -> ((CacheCompositeItem) i).status));
		new TableViewerColumnSorter(tableViewerColumn);
		tableColumn = tableViewerColumn.getColumn();
		tableColumn.setText("Status");
		tableColumn.pack();
		tableColumn.setWidth(150);

		// set input
		tableViewer.setInput(items);

		// add menu to open system explorer
		var menu = new Menu(tableViewer.getTable());
		tableViewer.getTable().setMenu(menu);
		var pathItem = new MenuItem(menu, SWT.NONE);
		pathItem.setText("Open with system explorer");
		pathItem.addListener(SWT.Selection,
				ev -> Optional.ofNullable(tableViewer.getStructuredSelection().getFirstElement())
						.map(CacheCompositeItem.class::cast).ifPresent(item -> {
							try {
								Desktop.getDesktop()
										.open(item.file.isDirectory() ? item.file : item.file.getParentFile());
							} catch (IOException e1) {
								LOGGER.error(e1.getMessage(), e1);
							}
						}));
	}

	/**
	 * Initialize buttons of {@link WorkspaceCacheComposite}
	 */
	protected void initializeButtons() {
		btnSelectAll = new Button(this, SWT.NONE);
		btnSelectAll.setText("Select All");
		btnSelectAll.addListener(SWT.Selection, event -> {
			((CheckboxTableViewer) tableViewer).setAllChecked(true);
			updateButtons();
		});

		btnUnselectAll = new Button(this, SWT.NONE);
		btnUnselectAll.setText("Unselect all");
		btnUnselectAll.addListener(SWT.Selection, event -> {
			((CheckboxTableViewer) tableViewer).setAllChecked(false);
			updateButtons();
		});

		btnDelete = new Button(this, SWT.NONE);
		btnDelete.setText("Delete Selection");
		btnDelete.addListener(SWT.Selection, event -> {
			getSelectedItems().forEach(item -> {
				try {
					if (item.file.isDirectory())
						FileUtils.deleteDirectory(item.file);
					else
						Files.delete(item.file.toPath());
				} catch (IOException | IllegalArgumentException e) {
					LOGGER.error("Error while deleting file {} : {}", item.file.getName(), e.getMessage(), e);
					item.status = "Undelatable";
				}
			});
			updateItems();
		});

	}

	/**
	 * Reload the list of {@link CacheCompositeItem} to display.
	 */
	private void updateItems() {
		Display.getDefault().syncExec(() -> {
			List<File> files = cacheModel.fileSupplier.get();

			// remove obsolete items (file does not longer exist)
			items.removeAll(items.stream().filter(item -> files.stream().noneMatch(item.file::equals))
					.collect(Collectors.toList()));

			// create new items for new files
			files.stream().filter(file -> items.stream().noneMatch(i -> file.equals(i.file)))
					.forEach(newFile -> items.add(new CacheCompositeItem(newFile)));

			if (!tableViewer.getTable().isDisposed()) {
				updateButtons();
				tableViewer.refresh();
			}
		});
	}

	/**
	 * @return selected {@link CacheCompositeItem}.
	 */
	private List<CacheCompositeItem> getSelectedItems() {
		return Stream.of(tableViewer.getTable().getItems()).filter(TableItem::getChecked)
				.map(tableItem -> (CacheCompositeItem) tableItem.getData()).collect(Collectors.toList());
	}

	/**
	 * Update status of buttons according to checkboxes selection.
	 */
	private void updateButtons() {
		Display.getDefault().syncExec(() -> {
			btnSelectAll.setEnabled(!items.isEmpty() && items.size() != getSelectedItems().size());
			btnUnselectAll.setEnabled(!items.isEmpty() && !getSelectedItems().isEmpty());
			btnDelete.setEnabled(!getSelectedItems().isEmpty());
		});
	}

}
