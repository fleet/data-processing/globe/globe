package fr.ifremer.globe.ui.dialogs.cachemanager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

/**
 * Dialog which allows to manage cache files created by Globe.
 */
public class CacheManagerDialog extends Dialog {

	private final List<CacheComposite> cacheComposites = new ArrayList<>();

	/**
	 * Constructor of {@link CacheManagerDialog}.
	 */
	public CacheManagerDialog(Shell shell) {
		super(shell);
		setShellStyle(SWT.RESIZE | SWT.TITLE | SWT.CLOSE);
		setBlockOnOpen(false);
	}

	@Override
	protected void configureShell(Shell shell) {
		shell.setSize(800, 500);
		shell.setText("Cache Manager");
		super.configureShell(shell);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID, "Close", false);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		cacheComposites.forEach(Composite::dispose);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		var tabFolder = new TabFolder(parent, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		Stream.of(Cache.values()).forEach(cache -> addNewCacheItem(tabFolder, cache));
		return parent;
	}

	private void addNewCacheItem(TabFolder tabFolder, Cache cache) {
		var tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText(cache.title);
		var cacheComposite = new CacheComposite(tabFolder, cache);
		tabItem.setControl(cacheComposite);
		cacheComposites.add(cacheComposite);
	}
}
