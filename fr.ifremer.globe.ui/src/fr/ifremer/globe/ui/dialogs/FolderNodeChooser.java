/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.dialogs;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import jakarta.inject.Inject;

import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNodeLabelProvider;

/**
 * Plain PopupDialog to select a FolderNode
 */
public class FolderNodeChooser extends PopupDialog {

	/** Consumer of the selected FolderNode */
	private final Consumer<FolderNode> consumer;

	/** Used to igore the first selection (a selection is automatically set on the first node when dialog is opened) */
	private final AtomicBoolean ignoreSelection = new AtomicBoolean(true);

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	/**
	 * Constructor
	 */
	public FolderNodeChooser(Shell parent, String titleText, Consumer<FolderNode> consumer) {
		super(parent, PopupDialog.INFOPOPUP_SHELLSTYLE, true, false, false, false, false, titleText, null);
		ContextInitializer.inject(this);
		this.consumer = consumer;
	}

	/** Show popup relative to cursor */
	@Override
	protected Point getInitialLocation(Point initialSize) {
		return getShell().getDisplay().getCursorLocation();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Point getInitialSize() {
		return getShell().computeSize(SWT.DEFAULT, 200, true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.addFocusListener(FocusListener.focusLostAdapter(event -> close()));
		TreeViewer viewer = new TreeViewer(composite, SWT.SINGLE | SWT.FULL_SELECTION | SWT.V_SCROLL);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(viewer.getControl());
		viewer.setAutoExpandLevel(AbstractTreeViewer.ALL_LEVELS);
		viewer.getTree().setHeaderVisible(false);
		viewer.setContentProvider(new FolderNodeContentProvider());

		TreeViewerColumn mainColumn = new TreeViewerColumn(viewer, SWT.NONE);
		mainColumn.setLabelProvider(new TreeNodeLabelProvider());

		// Populates the model
		viewer.setInput(new GroupNode[] { projectExplorerModel.getDataNode() });

		mainColumn.getColumn().pack();

		viewer.addSelectionChangedListener(event -> selectionChanged(event.getStructuredSelection()));
		return composite;
	}

	/**
	 * A node has been selected in the tree
	 */
	protected void selectionChanged(IStructuredSelection selection) {
		if (selection.getFirstElement() instanceof FolderNode selectedFolderNode) {
			if (ignoreSelection.compareAndSet(true, false))
				return;
			consumer.accept(selectedFolderNode);
			close();
		}
	}

	/** ContentProvider to render FolderNode */
	public static class FolderNodeContentProvider implements ITreeContentProvider {
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getChildren(Object parentElement) {
			Object[] result = null;
			if (parentElement instanceof FolderNode parentFolderNode) {
				List<TreeNode> children = parentFolderNode.getChildren();
				if (children != null) {
					children = children.stream().//
							filter(FolderNode.class::isInstance).toList();
					result = children.toArray();
				}
			} else if (parentElement instanceof Object[]) {
				result = (Object[]) parentElement;
			}
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object getParent(Object element) {
			return ((TreeNode) element).getParent();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasChildren(Object parentElement) {
			if (parentElement instanceof FolderNode parentFolderNode) {
				List<TreeNode> children = parentFolderNode.getChildren();
				return children != null && children.stream().anyMatch(FolderNode.class::isInstance);
			}
			return false;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getElements(Object inputElement) {
			return getChildren(inputElement);
		}
	}

}
