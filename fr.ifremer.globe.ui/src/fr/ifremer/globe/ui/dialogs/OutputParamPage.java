package fr.ifremer.globe.ui.dialogs;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.processes.OutputParameters;

public class OutputParamPage extends WizardPage implements Listener {

	final IStatus statusOK = new Status(IStatus.OK, "not_used", 0, null, null);

	private Composite m_parent;

	private OutputParameters m_parameters;
	private boolean m_suffixe = true;
	private DirectoryFieldEditor m_directoryFieldEditor;
	private Text m_directoryText;
	private Label suffixeLabel;
	private Text suffixeText;
	private Button m_restoreDefault;

	private IStatus m_directoryTextStatus = statusOK;
	private IStatus m_suffixeTextStatus = statusOK;

	public OutputParamPage(OutputParameters parameters, boolean suffixe) {
		super("");
		setDescription("Enter output parameters");
		m_parameters = parameters;
		m_suffixe = suffixe;
	}

	@Override
	public void createControl(Composite parent) {
		m_parent = parent;
		Composite container = new Composite(parent, SWT.NONE);
		createMain(container);
		getShell().pack();
		setControl(container);
		setPageComplete(false);
		loadData();
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);

		if (visible) {
			onEnterPage();
		}
	}

	private void onEnterPage() {
		m_parent.layout();
	}

	protected void createMain(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.verticalSpacing = 10;
		parent.setLayout(layout);
		layout.numColumns = 3;

		m_directoryFieldEditor = new DirectoryFieldEditor(OutputParameters.PROPERTY_OUTPUT_DIRECTORY_NAME,
				"Output directory", parent);
		m_directoryText = m_directoryFieldEditor.getTextControl(parent);
		m_directoryText.addListener(SWT.Modify, this);

		if (m_suffixe) {
			suffixeLabel = new Label(parent, SWT.NONE);
			suffixeLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			suffixeLabel.setText("Suffixe results :");

			suffixeText = new Text(parent, SWT.BORDER);
			GridData gd_suffixeLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
			gd_suffixeLabel.widthHint = 341;
			suffixeText.setLayoutData(gd_suffixeLabel);
		} else {
			m_suffixeTextStatus = statusOK;
		}

		m_restoreDefault = new Button(parent, SWT.PUSH);
		m_restoreDefault.setText("Restore Default");
		GridData gd2 = new GridData(SWT.RIGHT, SWT.BOTTOM, true, true);
		gd2.horizontalSpan = 3;
		gd2.verticalIndent = 20;
		m_restoreDefault.setLayoutData(gd2);
		m_restoreDefault.addListener(SWT.Selection, this);

	}

	@Override
	public void handleEvent(Event event) {
		if (event.widget == m_restoreDefault) {
			m_parameters.restoreDefault();
			loadData();
		} else if (event.widget == m_directoryText) {
			if (m_directoryText.getText().isEmpty()) {
				m_directoryTextStatus = new Status(IStatus.ERROR, "not_used", 0, "Output directory can't be empty",
						null);

			} else {
				m_directoryTextStatus = statusOK;
			}
		} else if (event.widget == suffixeText) {
			if (suffixeText.getText().isEmpty()) {
				m_suffixeTextStatus = new Status(IStatus.ERROR, "not_used", 0, "Suffixe results can't be empty", null);
			} else {
				m_suffixeTextStatus = statusOK;
			}
		}
		applyMostSevere();
	}

	private void applyMostSevere() {
		IStatus status = new Status(IStatus.OK, "not_used", 0, null, null);
		if (m_directoryTextStatus.matches(IStatus.ERROR)) {
			status = m_directoryTextStatus;
		} else if (m_suffixeTextStatus.matches(IStatus.ERROR)) {
			status = m_suffixeTextStatus;
		}

		if (status.getSeverity() == IStatus.ERROR) {
			setErrorMessage(status.getMessage());
			setPageComplete(false);
		} else {
			setErrorMessage(null);
			setPageComplete(true);
		}
	}

	public void loadData() {

		File path = null;
		String fileName = m_parameters.getProperty(OutputParameters.PROPERTY_OUTPUT_DIRECTORY_NAME);
		if (fileName != null && !fileName.isEmpty()) {
			path = new File(fileName);
		} else {
			try {
				Platform.getInstanceLocation().getURL();
				URL locationUrl = Platform.getInstanceLocation().getURL();
				URL fileUrl = FileLocator.toFileURL(locationUrl);
				path = new File(fileUrl.getPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		m_directoryFieldEditor.setStringValue(path.getAbsolutePath());
		m_directoryFieldEditor.setFilterPath(path);

		if (m_suffixe) {
			if (m_parameters.getProperty(OutputParameters.PROPERTY_SUFFIXE_RESULTS) != null) {
				suffixeText.setText(m_parameters.getProperty(OutputParameters.PROPERTY_SUFFIXE_RESULTS));
			} else {
				suffixeText.setText("");
			}
		}
	}

	public void saveData() {
		m_parameters.setProperty(OutputParameters.PROPERTY_OUTPUT_DIRECTORY_NAME,
				m_directoryFieldEditor.getStringValue());
		if (m_suffixe) {
			m_parameters.setProperty(OutputParameters.PROPERTY_SUFFIXE_RESULTS, suffixeText.getText());
		}
	}

}
