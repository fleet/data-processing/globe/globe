package fr.ifremer.globe.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

public class ShortcutsDialog extends Dialog {

	public ShortcutsDialog(Shell parentShell) {
		super(parentShell);

		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE);
		setBlockOnOpen(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Shortcuts");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		FontData fd = new FontData();
		fd.setStyle(SWT.BOLD);
		Font boldFont = new Font(getShell().getDisplay(), fd);

		applyDialogFont(composite);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));

		TabFolder tabFolder = new TabFolder(composite, SWT.NONE);

		// KeybordTab
		{
			TabItem keyboardtbtm = new TabItem(tabFolder, SWT.NONE);
			keyboardtbtm.setText("Keyboard");

			Table table = new Table(tabFolder, SWT.BORDER);
			keyboardtbtm.setControl(table);
			TableColumn column1 = new TableColumn(table, SWT.LEFT);
			column1.setText("Key");
			column1.setWidth(189);
			TableColumn column2 = new TableColumn(table, SWT.LEFT);
			column2.setText("Action");
			column2.setWidth(200);
			table.setHeaderVisible(true);
			table.setLinesVisible(true);

			//Edition
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Edition shortcuts", "" });
			item.setFont(boldFont);
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Ctrl+Z", "Undo last action" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Ctrl+Y", "Redo last action" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Ctrl+S", "Save current modifications" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "", "" });
			item = new TableItem(table, SWT.NONE);

			//World View
			item.setText(new String[] { "World View", "" });
			item.setFont(boldFont);
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Alt+F4", "Exit application" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "m", "Contrast min/max" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "c", "Contrast 0.5%" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "C", "Contrast 1.0%" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "$", "Hight/Normal resolution" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "t", "Hide terrain" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "i", "Interpolation Nearest/Bilinear" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "n", "set view to north" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "s", "show all data (dtm) intersecting the current displayed area" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "h", "hide all data (dtm) intersecting the current displayed area" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "arrows,+,-,pagedown,pageup", "move view point" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "w", "wireframe mode" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "space", "play/pause current WC player" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "p", "previous on current WC player" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "o", "next on current WC player" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "", "" });
			item = new TableItem(table, SWT.NONE);

			//Swath Editor
			item.setText(new String[] { "Swath Editor", "" });
			item.setFont(boldFont);
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Arrows", "Move selection" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Enter", "Validate selection" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Del/Suppr", "Invalidate selection" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Crtl+Z", "Undo" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Ctrl+Y", "Redo" });

			column1.pack();
			column2.pack();
		}

		// GamePadTab
		{
			TabItem gamePadtbtm = new TabItem(tabFolder, SWT.NONE);
			gamePadtbtm.setText("GamePad");

			Composite gamePadComposite = new Composite(tabFolder, SWT.NONE);
			gamePadtbtm.setControl(gamePadComposite);
			gamePadComposite.setLayout(new GridLayout(1, false));

			Label lblNewLabel = new Label(gamePadComposite, SWT.CENTER);
			lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			lblNewLabel.setAlignment(SWT.CENTER);
			lblNewLabel.setImage(ResourceManager.getPluginImage("fr.ifremer.3dviewer", "icons/360_controller.svg.png"));

			Label lblDoNotForget = new Label(gamePadComposite, SWT.NONE);
			lblDoNotForget.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
			lblDoNotForget.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			lblDoNotForget.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
			lblDoNotForget.setText("Do not forget to enable gamepad detection in viewer3D preferences");

			Table table = new Table(gamePadComposite, SWT.BORDER);
			table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			table.setLinesVisible(true);
			table.setHeaderVisible(true);

			TableColumn column1 = new TableColumn(table, SWT.LEFT);
			column1.setWidth(189);
			column1.setText("Key");

			TableColumn column2 = new TableColumn(table, SWT.LEFT);
			column2.setWidth(200);
			column2.setText("Action");

			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Left stick", "Panning" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Right stick", "Attitude (Heading and Roll)" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Left bumper ", "Decrease panning increment" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Right bumper ", "Increase panning increment" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Left trigger", "Increase altitude" });
			item = new TableItem(table, SWT.NONE);
			item.setText(new String[] { "Right trigger", "Decrease altitude" });

			column1.pack();
			column2.pack();
		}

		return composite;
	}
}
