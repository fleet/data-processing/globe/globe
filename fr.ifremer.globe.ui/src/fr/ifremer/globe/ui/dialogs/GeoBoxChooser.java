/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.themes.ColorUtil;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.providers.GeoBoxLabelProvider;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNodeLabelProvider;

/**
 * Plain PopupDialog to select a InfoStore
 */
public class GeoBoxChooser extends PopupDialog {

	/** Consumer of the selected GeoBox */
	protected Consumer<GeoBox> consumer;

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	/**
	 * Constructor
	 */
	public GeoBoxChooser(Shell parent, String titleText, Consumer<GeoBox> consumer) {
		super(parent, PopupDialog.INFOPOPUP_SHELLSTYLE, true, false, false, false, false, titleText, null);
		ContextInitializer.inject(this);
		this.consumer = consumer;
	}

	/** Show popup relative to cursor */
	@Override
	protected Point getInitialLocation(Point initialSize) {
		return getShell().getDisplay().getCursorLocation();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Point getInitialSize() {
		return getShell().computeSize(SWT.DEFAULT, 200, true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.addFocusListener(FocusListener.focusLostAdapter(event -> close()));
		TreeViewer viewer = new TreeViewer(composite,
				SWT.VIRTUAL | SWT.SINGLE | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(viewer.getControl());
		viewer.setAutoExpandLevel(AbstractTreeViewer.ALL_LEVELS);
		viewer.getTree().setHeaderVisible(true);
		viewer.setContentProvider(new OpenFileContentProvider());

		TreeViewerColumn mainColumn = new TreeViewerColumn(viewer, SWT.NONE);
		mainColumn.getColumn().setText("File");
		mainColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewLabelProvider()));

		TreeViewerColumn northColumn = new TreeViewerColumn(viewer, SWT.NONE);
		northColumn.getColumn().setText("North");
		northColumn.setLabelProvider(GeoBoxLabelProvider.getNorthLabelProvider(GeoBoxChooser::getGeoBoxFromStoreNode));

		TreeViewerColumn southColumn = new TreeViewerColumn(viewer, SWT.NONE);
		southColumn.getColumn().setText("South");
		southColumn.setLabelProvider(GeoBoxLabelProvider.getSouthLabelProvider(GeoBoxChooser::getGeoBoxFromStoreNode));

		TreeViewerColumn westColumn = new TreeViewerColumn(viewer, SWT.NONE);
		westColumn.getColumn().setText("West");
		westColumn.setLabelProvider(GeoBoxLabelProvider.getWestLabelProvider(GeoBoxChooser::getGeoBoxFromStoreNode));

		TreeViewerColumn eastColumn = new TreeViewerColumn(viewer, SWT.NONE);
		eastColumn.getColumn().setText("East");
		eastColumn.setLabelProvider(GeoBoxLabelProvider.getEastLabelProvider(GeoBoxChooser::getGeoBoxFromStoreNode));

		// Populates the model
		List<Object> treeInput = new ArrayList<>();
		treeInput.add(projectExplorerModel.getDataNode());
		viewer.setInput(treeInput.toArray());

		mainColumn.getColumn().pack();
		northColumn.getColumn().pack();
		southColumn.getColumn().pack();
		westColumn.getColumn().pack();
		eastColumn.getColumn().pack();

		// No selection
		parent.getDisplay().asyncExec(() -> viewer.setSelection(TreeSelection.EMPTY));

		viewer.addSelectionChangedListener(event -> selectionChanged(event.getStructuredSelection()));
		return composite;
	}

	/**
	 * A node has been selected in the tree
	 */
	protected void selectionChanged(IStructuredSelection selection) {
		GeoBox geoBox = getGeoBoxFromStoreNode(selection.getFirstElement());
		if (geoBox != null) {
			consumer.accept(geoBox);
			close();
		}
	}

	/** Return the GeoBox stored on the spetified object. */
	static GeoBox getGeoBoxFromStoreNode(Object element) {
		GeoBox result = null;
		if (element instanceof FileInfoNode) {
			IFileInfo fileInfo = ((FileInfoNode) element).getFileInfo();
			if (fileInfo != null) {
				result = fileInfo.getGeoBox();
			}
		}
		return result;
	}

	/** LabelProvider to render IFileInfoNode as a clickable link */
	public static class ViewLabelProvider extends TreeNodeLabelProvider implements IStyledLabelProvider {
		/**
		 * Constructor
		 */
		public ViewLabelProvider() {
			JFaceResources.getColorRegistry().put(getClass().getSimpleName() + "_Link",
					ColorUtil.getColorValue("COLOR_BLACK"));
			JFaceResources.getColorRegistry().put(getClass().getSimpleName() + "_Folder",
					ColorUtil.getColorValue("COLOR_GRAY"));
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public StyledString getStyledText(Object element) {
			StyledString result = null;
			String text = getText(element);
			if (text != null) {
				Styler style = StyledString.createColorRegistryStyler(
						getClass().getSimpleName() + (element instanceof FileInfoNode ? "_Link" : "_Folder"), null);
				result = new StyledString(text, style);
			}
			return result;
		}
	}

	/** LabelProvider to render IFileInfoNode as a clickable link */
	public static class OpenFileContentProvider implements ITreeContentProvider {
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getChildren(Object parentElement) {
			Object[] result = null;
			if (parentElement instanceof GroupNode) {
				List<TreeNode> children = ((GroupNode) parentElement).getChildren();
				if (children != null) {
					children = children.stream().//
							filter(treeNode -> treeNode instanceof FolderNode
									|| getGeoBoxFromStoreNode(treeNode) != null)
							.collect(Collectors.toList());
					result = children.toArray();
				}
			} else if (parentElement instanceof Object[]) {
				result = (Object[]) parentElement;
			}
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object getParent(Object element) {
			return ((TreeNode) element).getParent();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof GroupNode) {
				return ((GroupNode) element).hasChildren();
			}
			return false;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getElements(Object inputElement) {
			return getChildren(inputElement);
		}
	}

}
