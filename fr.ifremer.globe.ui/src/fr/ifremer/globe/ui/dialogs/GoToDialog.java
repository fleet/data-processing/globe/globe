package fr.ifremer.globe.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import gov.nasa.worldwind.geom.LatLon;

public class GoToDialog extends Dialog {

	/**
	 * The latitude coordinate input value.
	 */
	Text latText;

	/**
	 * The longitude coordinate input value.
	 */
	Text lonText;

	/**
	 * The input validator, or <code>null</code> if none.
	 */
	private IInputValidator validator;

	/**
	 * Error message label widget.
	 */
	private Text errorMessageText;

	/**
	 * Error message string.
	 */
	private String errorMessage;

	public GoToDialog(Shell parentShell) {
		super(parentShell);
		this.validator = this.getDoubleValidator();
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			double lat = Double.parseDouble(latText.getText());
			double lon = Double.parseDouble(lonText.getText());
			IGeographicViewService.grab().zoomTo(LatLon.fromDegrees(lat, lon));
		}
		super.buttonPressed(buttonId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Go to coordinates");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(2, false));

		Label labelImage = new Label(composite, SWT.NONE);
		labelImage.setImage(ImageResources.getImage("/icons/goto.png", getClass()));
		labelImage.setLayoutData(new GridData(GridData.FILL_HORIZONTAL, GridData.CENTER, true, false, 1, 1));
		labelImage.setFont(parent.getFont());

		Group coordGroup = new Group(composite, SWT.NONE);
		coordGroup.setText("Coordinates");
		GridLayout layout = new GridLayout(2, true);
		layout.marginHeight = 10;
		layout.verticalSpacing = 10;
		coordGroup.setLayout(layout);
		coordGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		Label latLabel = new Label(coordGroup, SWT.NONE);
		latLabel.setText("Decimal latitude : ");
		latLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		latText = new Text(coordGroup, SWT.BORDER);
		latText.setText("0");
		latText.setFocus();
		latText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				validateInput();
			}
		});
		latText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		Label lonLabel = new Label(coordGroup, SWT.NONE);
		lonLabel.setText("Decimal longitude : ");
		lonLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		lonText = new Text(coordGroup, SWT.BORDER);
		lonText.setText("0");
		lonText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				validateInput();
			}
		});
		lonText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		errorMessageText = new Text(composite, SWT.READ_ONLY | SWT.WRAP);
		errorMessageText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 2, 1));
		errorMessageText.setBackground(errorMessageText.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		setErrorMessage(errorMessage);

		return composite;
	}

	/**
	 * Validates the input.
	 * <p>
	 * The default implementation of this framework method delegates the request
	 * to the supplied input validator object; if it finds the input invalid,
	 * the error message is displayed in the dialog's message line. This hook
	 * method is called whenever the text changes in the input field.
	 * </p>
	 */
	protected void validateInput() {
		String errorMessage = null;
		if (validator != null) {
			errorMessage = validator.isValid(latText.getText());
			if (errorMessage == null) {
				errorMessage = validator.isValid(lonText.getText());
			}
		}
		setErrorMessage(errorMessage);
	}

	/**
	 * Create the double validator
	 * 
	 * @return the validator
	 */
	private IInputValidator getDoubleValidator() {
		IInputValidator validator = new IInputValidator() {
			@Override
			public String isValid(String newText) {
				try {
					Double.parseDouble(newText);
				} catch (NumberFormatException ex) {
					return "Coordinates must be double values";
				}
				return null;
			}
		};
		return validator;
	}

	/**
	 * Sets or clears the error message. If not <code>null</code>, the OK button
	 * is disabled.
	 * 
	 * @param errorMessage
	 *            the error message, or <code>null</code> to clear
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			errorMessageText.setText(errorMessage == null ? " \n " : errorMessage);
			// Disable the error message text control if there is no error, or
			// no error text (empty or whitespace only).
			boolean hasError = errorMessage != null && (StringConverter.removeWhiteSpaces(errorMessage)).length() > 0;
			errorMessageText.setEnabled(hasError);
			errorMessageText.setVisible(hasError);
			errorMessageText.getParent().update();
			Control button = getButton(IDialogConstants.OK_ID);
			if (button != null) {
				button.setEnabled(errorMessage == null);
			}
		}
	}

}
