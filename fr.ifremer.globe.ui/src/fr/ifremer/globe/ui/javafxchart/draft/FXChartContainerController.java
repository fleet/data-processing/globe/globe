package fr.ifremer.globe.ui.javafxchart.draft;

import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

public class FXChartContainerController {

	/**
	 * FXML Components
	 */
	@FXML
	public TextField maxPoints;
	@FXML
	private Label outputLabel;
	@FXML
	private CheckBox autoReload;
	@FXML
	private CheckBox showPoints;
	@FXML
	private CheckBox showInvalid;
	@FXML
	public TextField reductionFactor;
	@FXML
	public TextField fakeDataSize;
	@FXML
	public RadioButton rectangleSelectionMode;
	@FXML
	public RadioButton polygonSelectionMode;
	@FXML
	public RadioButton lineSelectionMode;
	@FXML
	private FXChartController fxChartController;

	@FXML
	private void initialize() {

		autoReload.selectedProperty().addListener((ov, old, newVal) -> fxChartController.setAutoRefresh(newVal));

		showPoints.selectedProperty().addListener((ov, old, newVal) -> fxChartController.setShowSymbols(newVal));

		// Selection mode
		fxChartController.setSelectionMode(SelectionMode.LINE_SELECTION);
		fxChartController.addSelectionListener(selectedData -> {
			System.err.println("Selected data : " + selectedData.size());
			selectedData.forEach(data -> data.setEnabled(!data.isEnabled()));
			fxChartController.refresh();
		});
		lineSelectionMode.selectedProperty()
				.addListener((ov, old, newVal) -> fxChartController.setSelectionMode(SelectionMode.LINE_SELECTION));
		rectangleSelectionMode.selectedProperty().addListener(
				(ov, old, newVal) -> fxChartController.setSelectionMode(SelectionMode.RECTANGLE_SELECTION));
		polygonSelectionMode.selectedProperty()
				.addListener((ov, old, newVal) -> fxChartController.setSelectionMode(SelectionMode.POLYGON_SELECTION));

		maxPoints.textProperty()
				.addListener((ov, old, newVal) -> fxChartController.setMaxDisplayPoints(Integer.parseInt(newVal)));

		reductionFactor.textProperty()
				.addListener((ov, old, newVal) -> fxChartController.setReductionFactor(Double.parseDouble(newVal)));

	}

	@FXML
	void autoZoom() {
		fxChartController.resetZoom();
	}

	@FXML
	public void onRefreshChart() {
		fxChartController.refresh();
	}

	@FXML
	public void reloadFakeData() {
		// fxChartController.setSeriesModel(Arrays.asList(new
		// FakeFXChartSeriesProxy(Integer.parseInt(fakeDataSize.getText()))))
	}

	public FXChartController getChartController() {
		return fxChartController;
	}
}
