package fr.ifremer.globe.ui.javafxchart.draft;

import java.util.Date;
import java.util.HashMap;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.exception.GIOException;
import javafx.scene.paint.Color;

public class NavigationChartSeries implements IChartSeries {

	private final INavigationData navigationData;
	private HashMap<Integer, Boolean> invalidIndexes = new HashMap<>();
	private static ColorProvider colorProvider = new ColorProvider();
	private GColor color = colorProvider.get();

	public NavigationChartSeries(INavigationData navigationData) {
		this.navigationData = navigationData;
	}

	@Override
	public IChartData getData(int index) {
		
		return new IChartData() {			

			@Override
			public IChartSeries getSeries() {
				return NavigationChartSeries.this;
			}
			
			@Override 
			public int getIndex() {
				return index;
			}

			@Override
			public double getY() {
				try {
					return navigationData.getLatitude(index);
				} catch (GIOException e) {
					e.printStackTrace();
					return index;
				}
			}

			@Override
			public double getX() {
				try {
					return navigationData.getLongitude(index);
				} catch (GIOException e) {
					e.printStackTrace();
					return index;
				}
			}
			
			@Override
			public String getTooltip() {
				try {
					return new Date(navigationData.getTime(index)).toString() + "\tn°" + index;
				} catch (GIOException e) {
					return "Date not available";
				}
			}

			@Override
			public double getComparisonValue() {
				try {
					return navigationData.getTime(index);
				} catch (GIOException e) {
					e.printStackTrace();
					return index;
				}
			}

			@Override
			public GColor getColor() {
				boolean isValid = invalidIndexes.containsKey(index) ? invalidIndexes.get(index) : true;
				return isValid ? NavigationChartSeries.this.getColor() : new GColor(Color.GRAY.toString());
			}

		};
	}

	@Override
	public int size() {
		return navigationData.size();
	}

	@Override
	public String getTitle() {
		return navigationData.getFileName();
	}

	@Override
	public GColor getColor() {
		return color;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setEnabled(boolean enabled) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setColor(GColor color) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IChartSeriesDataSource getSource() {
		// TODO Auto-generated method stub
		return null;
	}

}
