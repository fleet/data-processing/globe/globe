package fr.ifremer.globe.ui.javafxchart.draft;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FXChartApplication extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("./../view/FXChartContainer.fxml"));
		primaryStage.setTitle("FXChart");
		primaryStage.setScene(new Scene(fxmlLoader.load(), 1200, 500));
		primaryStage.show();
	
		//FXChartController chartController = ((FXChartContainerController) fxmlLoader.getController()).getChartController()
		//chartController.setSeriesModel(Arrays.asList(new FakeFXChartSeriesProxy(1000)))
	}

	public static void main(String[] args) {
		launch(args);
	}

}
