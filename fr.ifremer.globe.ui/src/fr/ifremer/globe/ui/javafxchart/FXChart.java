package fr.ifremer.globe.ui.javafxchart;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Consumer;
import java.util.function.Function;

import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.gillius.jfxutils.chart.AxisConstraint;
import org.osgi.framework.Bundle;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.ui.javafxchart.draft.FXChartContainerController;
import fr.ifremer.globe.ui.javafxchart.tools.ASelectionManager.SelectionListener;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.ChartCursor;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;
import javafx.embed.swt.FXCanvas;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.paint.Color;

/**
 * FXChart : chart widget based on JavaFx {@link LineChart}.
 */
public class FXChart {

	static {
		// Avoid to delete JavaFX Thread when a view is closed (the thread is only initialized once, in static part
		// of FXCanvas, so we need to keep it alive to be able to open another view later).
		javafx.application.Platform.setImplicitExit(false);
	}

	private FXChartController chartController;

	/**
	 * Constructor (loads a new JavaFXChart in a SWT composite)
	 *
	 * @param parent chart container
	 * @param title chart title
	 * @throws FXChartException
	 */
	public FXChart(Composite parent, String title) throws IOException, FXChartException {
		// Load fxml
		Bundle bundle = Platform.getBundle("fr.ifremer.globe.ui");
		URL urlToFile = bundle.findEntries("/", "FXChart.fxml", true).nextElement();
		FXMLLoader fxmlLoader = new FXMLLoader(urlToFile);

		// Use FXCanvas to integrate JavaFX content into an SWT component
		FXCanvas fxCanvas = new FXCanvas(parent, SWT.FILL);
		fxCanvas.setScene(new Scene(fxmlLoader.load()));

		chartController = fxmlLoader.getController();
		chartController.setTitle(title);

		parent.addDisposeListener(e -> chartController.dispose());
	}

	/**
	 * Code to display FXCHART within a container which provides buttons for debug...
	 */
	public FXChart(Composite parent) throws IOException {

		FXCanvas fxCanvas = new FXCanvas(parent, SWT.FILL);
		URL url = getClass().getResource("./draft/FXChartContainer.fxml");

		FXMLLoader fxmlLoader = new FXMLLoader(url);
		fxCanvas.setScene(new Scene(fxmlLoader.load()));

		FXChartContainerController containerController = fxmlLoader.getController();
		chartController = containerController.getChartController();

	}

	//// PUBLIC API

	/**
	 * @return chart bounds
	 */
	public double[] getBoundsAsDouble() {
		return chartController.getBoundsAsDouble();
	}

	/**
	 * Sets the chart title
	 */
	public void setTitle(String title) {
		chartController.setTitle(title);
	}

	/**
	 * Set chart model: a list of object implementing the {@link IChartSeries} interface
	 */
	public void setData(List<? extends IChartSeries<? extends IChartData>> series) {
		chartController.setSeriesModel(series);
	}

	/**
	 * Set x axis label to display date.
	 */
	public void setXAxisTimeLabel() {
		DateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		chartController.setXAxisLabelFormatter(d -> format.format(new Date(d.longValue())));
	}

	/**
	 * Defines how the x axis label are formatted
	 */
	public void setXAxisLabelFormatter(Function<Number, String> fomatter) {
		chartController.setXAxisLabelFormatter(fomatter);
	}

	/**
	 * Defines how the y axis label are formatted
	 */
	public void setYAxisLabelFormatter(Function<Number, String> fomatter) {
		chartController.setYAxisLabelFormatter(fomatter);
	}

	/**
	 * Set x axis label to display date.
	 */
	public void setYAxisTimeLabel() {
		DateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		chartController.setYAxisLabelFormatter(d -> format.format(new Date(d.longValue())));
	}

	/**
	 * Defines the selection mode
	 */
	public void setSelectionMode(SelectionMode mode) {
		chartController.setSelectionMode(mode);
	}

	/**
	 * Defines the selection mode and its color
	 */
	public void setSelectionMode(SelectionMode mode, Color selectorColor) {
		chartController.setSelectionMode(mode, selectorColor);
	}

	/**
	 * @return current {@link ChartCursor}
	 */
	public ChartCursor getCursor() {
		return chartController.getCursor();
	}

	/**
	 * Defines the cursor type to display.
	 */
	public void setCursor(ChartCursor cursor) {
		chartController.setCursor(cursor);
	}

	/**
	 * Moves cursor at a specific position.
	 */
	public void moveCursor(double x, double y) {
		chartController.moveCursor(new Point2D(x, y));
	}

	/**
	 * Sets the axis synchronization (if true, X and Y axis will have the same scale range)
	 */
	public void setAxisSynchronisation(boolean b) {
		chartController.setAxisSynchronisation(b);
	}

	/**
	 * Disables vertical zoom
	 */
	public void setVerticalZoomEnabled(boolean enabled) {
		chartController.setZoomConstraintAxis(enabled ? AxisConstraint.Both : AxisConstraint.Horizontal);
	}

	/**
	 * Refresh
	 */
	public void refresh() {
		chartController.refresh();
	}

	/**
	 * Resets zoom
	 */
	public void resetZoom() {
		chartController.resetZoom();
	}

	/** Ask the controller to maintain the aspect ration of the chart on resize. */
	public void maintainAspectRatio() {
		chartController.maintainAspectRatio();
	}

	/**
	 * Enables / disables automatic reset of zoom when data change
	 */
	public void setAutoZoom(boolean autoZoom) {
		chartController.setAutoZoom(autoZoom);
	}

	/**
	 * Refresh with specific axis bounds
	 *
	 * @param bounds double array with values : [minX, maxX, minY, maxY]
	 */
	public void setAxisBounds(double[] bounds) {
		chartController.setAxisBounds(bounds[0], bounds[1], bounds[2], bounds[3]);
	}

	/**
	 * Refresh with specific axis bounds
	 */
	public void setAxisBounds(double minX, double maxX, double minY, double maxY) {
		chartController.setAxisBounds(minX, maxX, minY, maxY);
	}

	/**
	 * Adds a new {@link SelectionListener}
	 */
	public void addSelectionListener(SelectionListener listener) {
		chartController.addSelectionListener(listener);
	}

	/**
	 * Removes a {@link SelectionListener}
	 */
	public void removeSelectionListener(SelectionListener listener) {
		chartController.removeSelectionListener(listener);
	}

	/**
	 * Adds a new cursor listener
	 */
	public boolean addCursorListener(Consumer<IChartData> listener) {
		return chartController.addCursorListener(listener);
	}

	/**
	 * Displays or not the disabled {@link IChartData}.
	 */
	public void setShowDisabledData(boolean b) {
		chartController.setShowDisabledData(b);
	}
}
