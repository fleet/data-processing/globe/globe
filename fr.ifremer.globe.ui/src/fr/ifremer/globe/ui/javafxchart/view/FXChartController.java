package fr.ifremer.globe.ui.javafxchart.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

import org.gillius.jfxutils.chart.AxisConstraint;
import org.gillius.jfxutils.chart.AxisTickFormatter;
import org.gillius.jfxutils.chart.ChartPanManager;
import org.gillius.jfxutils.chart.ChartZoomManager;
import org.gillius.jfxutils.chart.StableTicksAxis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.ui.javafxchart.model.FXChartSeries;
import fr.ifremer.globe.ui.javafxchart.tools.ASelectionManager.SelectionListener;
import fr.ifremer.globe.ui.javafxchart.tools.LineCursorManager;
import fr.ifremer.globe.ui.javafxchart.tools.LineSelectionManager;
import fr.ifremer.globe.ui.javafxchart.tools.PointCursorManager;
import fr.ifremer.globe.ui.javafxchart.tools.PolygonSelectionManager;
import fr.ifremer.globe.ui.javafxchart.tools.RectSelectionManager;
import io.reactivex.disposables.Disposable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class FXChartController {

	public static final String REFRESH_INFO_TEST = "refreshing...";

	private static final Logger LOGGER = LoggerFactory.getLogger(FXChartController.class);

	/** FXML Components **/
	@FXML
	private StackPane chartPane;
	@FXML
	private GlobeChart chart;
	@FXML
	private StableTicksAxis xAxis;
	@FXML
	private StableTicksAxis yAxis;
	@FXML
	private Label infoText;

	/** Chart properties **/
	// Max number of points that can be displayed at a time in the whole chart. Limitation found experimentally to have
	// good performance.
	private int maxDisplayPoints = 5000;
	// Factor used as a part of the displayBoundingBox to initialize the douglasPeucker reduction algorithm. A new point
	// will be added in the first step if the distance between the real and reduced data is larger than
	// reductionFactor*displayBoundingBox
	private double reductionFactor = 0.0001;
	private boolean autoRefresh = true;
	private boolean autoZoom = true;
	private boolean showSymbols = false;
	private boolean showDisabledData = true;
	private boolean axisSynchronisationEnabled = false;
	private Function<Number, String> xFomatter;
	private Function<Number, String> yFomatter;
	// Area where points are displayed (larger than the real chart size to get a "cache")
	private BoundingBox displayBoundingBox;
	/** Current window ratio computed on zoow reset */
	private Optional<Bounds> layoutBounds = Optional.empty();

	/** Tools **/
	private ChartZoomManager zoomManager;
	private LineSelectionManager lineSelectionManager;
	private RectSelectionManager rectangleSelectionManager;
	private PolygonSelectionManager polygonSelectionManager;
	private PointCursorManager pointCursorManager;
	private LineCursorManager lineCursorManager;

	/** Selection mode (validate, invalidate...) */
	public enum SelectionMode {
		RECTANGLE_SELECTION,
		POLYGON_SELECTION,
		LINE_SELECTION,
		ZOOM
	}

	/** Cursor : point or line **/
	public enum ChartCursor {
		POINT_CURSOR,
		LINE_CURSOR
	}

	private SelectionMode selectionMode = SelectionMode.RECTANGLE_SELECTION;

	/** Series **/
	private List<FXChartSeries> seriesList = new ArrayList<>();

	/** RxJava {@link PublishProcessor} (manages refresh requests with back pressure strategy) **/
	private PublishProcessor<Integer> refreshPublishProcessor = PublishProcessor.create();
	private final Disposable rxDisposable = refreshPublishProcessor.observeOn(Schedulers.computation())
			.debounce(100, TimeUnit.MILLISECONDS).onBackpressureLatest().subscribe(o -> doRefresh());

	/**
	 * Initialization method
	 */
	@FXML
	private void initialize() {
		chart.setAnimated(false);
		chart.setLegendVisible(false);
		chart.setAxisSortingPolicy(LineChart.SortingPolicy.NONE);
		xAxis.setAutoRanging(false);
		yAxis.setAutoRanging(false);
		xAxis.setForceZeroInRange(false);
		yAxis.setForceZeroInRange(false);

		// Initialize tools
		intializePanManager();
		initializeZoomManager();
		initializeSelectionManager();
		pointCursorManager = new PointCursorManager(this);
		lineCursorManager = new LineCursorManager(this);

		// Activate point cursor by default
		pointCursorManager.setEnabled(true);

		// Automatic refresh when panning and/or zooming
		refreshAndUpdateBoundingBox();
		xAxis.upperBoundProperty().addListener((observable, oldValue, newValue) -> {
			double xStart = xAxis.getLowerBound();
			double xEnd = xAxis.getUpperBound();
			double width = xAxis.getUpperBound() - xStart;
			double displayXStart = displayBoundingBox.getMinX();
			double displayXEnd = displayBoundingBox.getMaxX();
			double displayWidth = displayBoundingBox.getWidth();

			if (width < 0.3 * displayWidth || width > 0.9 * displayWidth
					|| (xStart - displayXStart) < 0.1 * displayWidth || (displayXEnd - xEnd) < 0.1 * displayWidth) {
				refreshAndUpdateBoundingBox();
			}
		});
		yAxis.upperBoundProperty().addListener((observable, oldValue, newValue) -> {
			double yStart = yAxis.getLowerBound();
			double yEnd = yAxis.getUpperBound();
			double height = yAxis.getUpperBound() - yStart;
			double displayYStart = displayBoundingBox.getMinY();
			double displayYEnd = displayBoundingBox.getMaxY();
			double displayHeight = displayBoundingBox.getHeight();

			if (height < 0.3 * displayHeight || height > 0.9 * displayHeight
					|| (yStart - displayYStart) < 0.1 * displayHeight || (displayYEnd - yEnd) < 0.1 * displayHeight) {
				refreshAndUpdateBoundingBox();
			}
		});
	}

	/**
	 * Dispose managers (avoid memory leek)
	 */
	public void dispose() {
		rxDisposable.dispose();
		lineCursorManager.dispose();
		lineSelectionManager.dispose();
		pointCursorManager.dispose();
	}

	/**
	 * Initialize the pan functionality : works via either secondary (right) mouse or primary with ctrl held down
	 */
	private void intializePanManager() {
		ChartPanManager panner = new ChartPanManager(chart);
		panner.setMouseFilter(mouseEvent -> {
			if (mouseEvent.getButton() != MouseButton.PRIMARY || mouseEvent.isShortcutDown())
				mouseEvent.consume();
		});
		panner.start();
	}

	/** When chart is resized, recompute the axis bounds to maintain the aspect ration */
	public void maintainAspectRatio() {
		chart.needsLayoutProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.booleanValue())
				updateZoom();
		});
	}

	/**
	 * Initialize the zoom functionality
	 */
	private void initializeZoomManager() {
		// Creates zoom rectangle (selectRect not used because of zoom with short cut down)
		Rectangle zoomRect = new Rectangle(0, 0, 0, 0);
		zoomRect.setFill(Color.DODGERBLUE);
		zoomRect.setMouseTransparent(true);
		zoomRect.setOpacity(0.2);
		StackPane.setAlignment(zoomRect, Pos.TOP_LEFT);
		chartPane.getChildren().add(zoomRect);

		zoomManager = new ChartZoomManager(chartPane, zoomRect, chart);
		zoomManager.setZoomAnimated(false);
		zoomManager.setMouseFilter(mouseEvent -> {
			if (mouseEvent.getButton() == MouseButton.SECONDARY
					&& (mouseEvent.isShortcutDown() || selectionMode == SelectionMode.ZOOM))
				return; // let it through
			mouseEvent.consume();
		});
		zoomManager.start();

		// Reset zoom on double click
		chart.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
			if (event.getClickCount() == 2 && event.getButton() == MouseButton.PRIMARY
					&& selectionMode != SelectionMode.POLYGON_SELECTION && !lineSelectionManager.isSelecting()) {
				resetZoom();
				refresh();
			}
		});
	}

	/**
	 * Initialize the validate/invalidate selection functionality
	 */
	private void initializeSelectionManager() {
		rectangleSelectionManager = new RectSelectionManager(this);
		rectangleSelectionManager.setMouseFilter(mouseEvent -> {
			if (mouseEvent.getButton() == MouseButton.SECONDARY && !mouseEvent.isShortcutDown()
					&& selectionMode != SelectionMode.ZOOM)
				return; // let it through
			mouseEvent.consume();
		});
		lineSelectionManager = new LineSelectionManager(this);
		lineSelectionManager.setMouseFilter(mouseEvent -> {
			if (mouseEvent.isStillSincePress()) // check is not dragging
				return; // let it through
			mouseEvent.consume();
		});
		polygonSelectionManager = new PolygonSelectionManager(this);
		polygonSelectionManager.setMouseFilter(mouseEvent -> {
			if (mouseEvent.isStillSincePress()) // check is not dragging
				return; // let it through
			mouseEvent.consume();
		});
	}

	/**
	 * Resets zoom
	 */
	public void resetZoom() {
		resetZoom(true);
	}

	/**
	 * Reset zoom
	 */
	public void resetZoom(boolean resetXAxis) {
		if (seriesList.isEmpty())
			return;

		// Get minimum and maximum values
		double minX = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		for (FXChartSeries series : seriesList) {
			var bounds = series.computeBounds(!showDisabledData);
			if (!Double.isInfinite(bounds.getWidth())) {
				minX = Math.min(minX, bounds.getMinX());
				maxX = Math.max(maxX, bounds.getMaxX());
			}
			if (!Double.isInfinite(bounds.getHeight())) {
				minY = Math.min(minY, bounds.getMinY());
				maxY = Math.max(maxY, bounds.getMaxY());
			}
		}

		// avoid same min & max values
		if (minX == maxX) {
			maxX = maxX == 0 ? 1 : maxX + 0.1 * Math.abs(maxX);
			minX = minX == 0 ? -1 : minX - 0.1 * Math.abs(minX);
		}
		if (minY == maxY) {
			maxY = maxY == 0 ? 1 : maxY + 0.1 * Math.abs(maxY);
			minY = minY == 0 ? -1 : minY - 0.1 * Math.abs(minY);
		}

		// avoid invalid ranges
		if (minX > maxX || minY > maxY)
			return;

		// If axis are synchronized : the x and y axis steps are equals
		if (axisSynchronisationEnabled) {
			float ratio = (float) (chart.getWidth() / chart.getHeight()); // chart window ratio
			double rangeX = maxX - minX;
			double rangeY = maxY - minY;
			double middleX = (maxX + minX) / 2d;
			double middleY = (maxY + minY) / 2d;
			double offset = Math.max(rangeX / ratio, rangeY) / 1.9d;

			minX = middleX - ratio * offset;
			maxX = middleX + ratio * offset;
			minY = middleY - offset;
			maxY = middleY + offset;
		} else {
			double rangeX = maxX - minX;
			double rangeY = maxY - minY;
			minX = minX - 0.05 * rangeX;
			maxX = maxX + 0.05 * rangeX;
			minY = minY - 0.15 * rangeY; // let space to display info text
			maxY = maxY + 0.05 * rangeY;
		}

		// reset X axis only if asked or necessary
		if (resetXAxis || minX > xAxis.getUpperBound() || maxX < xAxis.getLowerBound()) {
			xAxis.setLowerBound(minX);
			xAxis.setUpperBound(maxX);
		}
		yAxis.setLowerBound(minY);
		yAxis.setUpperBound(maxY);
	}

	/**
	 * Update axis bounds
	 */
	public void updateZoom() {
		Bounds newChartBounds = chart.getLayoutBounds();
		layoutBounds.ifPresent(oldChartBounds -> {
			BoundingBox bounds = getBounds();
			if (newChartBounds.getWidth() != oldChartBounds.getWidth()) {
				double offset = newChartBounds.getWidth() * bounds.getWidth() / oldChartBounds.getWidth() / 2d;
				xAxis.setLowerBound(bounds.getCenterX() - offset);
				xAxis.setUpperBound(bounds.getCenterX() + offset);
			}
			if (newChartBounds.getHeight() != oldChartBounds.getHeight()) {
				double offset = newChartBounds.getHeight() * bounds.getHeight() / oldChartBounds.getHeight() / 2d;
				yAxis.setLowerBound(bounds.getCenterY() - offset);
				yAxis.setUpperBound(bounds.getCenterY() + offset);
			}
		});
		layoutBounds = Optional.of(newChartBounds);
	}

	public void refreshAndUpdateBoundingBox() {
		var bounds = getBounds();
		// chart bounds extended to get a "cache" around the real bounds.
		displayBoundingBox = new BoundingBox(bounds.getMinX() - bounds.getWidth(),
				bounds.getMinY() - bounds.getHeight(), 3 * bounds.getWidth(), 3 * bounds.getHeight());
		refresh();
	}

	/**
	 * Request a refresh (see javarx debounce)
	 */
	public void refresh() {
		refreshPublishProcessor.onNext(0);
	}

	/**
	 * Refresh the chart
	 */
	private void doRefresh() {
		try {
			Platform.runLater(() -> infoText.setText(REFRESH_INFO_TEST));

			// computes number of points per series
			List<FXChartSeries> seriesInBounds = getSeriesInBounds();
			if (seriesInBounds.isEmpty()) {
				Platform.runLater(() -> infoText.setText("no data to display"));
				return;
			}

			// reload series (apply reduction if necessary)
			int maxPointsBySeries = Math.max(20, maxDisplayPoints / seriesInBounds.size());
			boolean reductionHasBeenApplied = false;
			for (FXChartSeries series : seriesList) {
				if (series.reload(displayBoundingBox, reductionFactor, maxPointsBySeries, showDisabledData))
					reductionHasBeenApplied = true;
			}

			// refresh chart
			final boolean fShowSymbols = showSymbols || !reductionHasBeenApplied;
			Platform.runLater(() -> {
				try {
					chart.setCreateSymbols(fShowSymbols);
					seriesList.forEach(FXChartSeries::refreshChartSeries);
					infoText.setText("");
				} catch (Exception e) {
					LOGGER.error("Error during refresh process (display) : " + e.getMessage(), e);
				}
			});
		} catch (Exception e) {
			LOGGER.error("Error during refresh process : " + e.getMessage(), e);
		}
	}

	//// PUBLIC API

	public GlobeChart getChart() {
		return chart;
	}

	public StackPane getChartPane() {
		return chartPane;
	}

	public List<FXChartSeries> getSeries() {
		return seriesList;
	}

	public BoundingBox getBounds() {
		double minX = xAxis.getLowerBound();
		double maxX = xAxis.getUpperBound();
		double minY = yAxis.getLowerBound();
		double maxY = yAxis.getUpperBound();
		return new BoundingBox(minX, minY, Math.abs(minX - maxX), Math.abs(minY - maxY));
	}

	public double[] getBoundsAsDouble() {
		return new double[] { xAxis.getLowerBound(), xAxis.getUpperBound(), yAxis.getLowerBound(),
				yAxis.getUpperBound() };
	}

	private boolean allSeriesContainedInBounds() {
		var currentBounds = getBounds();
		return seriesList.stream().allMatch(series -> currentBounds.contains(series.computeBounds(!showDisabledData)));
	}

	private List<FXChartSeries> getSeriesInBounds() {
		var currentBounds = getBounds();
		return seriesList.stream().filter(series -> currentBounds.intersects(series.computeBounds(!showDisabledData)))
				.toList();
	}

	/**
	 * Sets model
	 */
	public void setSeriesModel(List<? extends IChartSeries<? extends IChartData>> seriesProxies) {
		// Compute the new series list
		List<FXChartSeries> newSeriesList = new ArrayList<>();

		for (var seriesProxy : seriesProxies) {
			boolean oldSeriesFound = false;
			// If a series matching with the proxy already exists: keep it
			for (FXChartSeries oldSeries : seriesList) {
				if (oldSeries.getProxy() == seriesProxy) {
					newSeriesList.add(oldSeries);
					oldSeriesFound = true;
					break;
				}
			}
			// Else: create a new one
			if (!oldSeriesFound)
				newSeriesList.add(new FXChartSeries(chart, seriesProxy));
		}

		// Remove obsolete series
		for (FXChartSeries oldSeries : seriesList) {
			if (!newSeriesList.contains(oldSeries))
				if (!Platform.isFxApplicationThread())
					Platform.runLater(oldSeries::dispose);
				else
					oldSeries.dispose();
		}

		seriesList = newSeriesList;

		if (autoZoom || getSeriesInBounds().size() != seriesList.size() || allSeriesContainedInBounds())
			resetZoom(autoZoom);

		refreshAndUpdateBoundingBox();
	}

	/**
	 * Updates information label
	 */
	public void updateInfoText(String text) {
		if (!infoText.getText().equals(REFRESH_INFO_TEST))
			infoText.setText(text);
	}

	/**
	 * Sets axis bounds
	 */
	public void setAxisBounds(double minX, double maxX, double minY, double maxY) {
		// avoid same min & max values
		if (minX == maxX) {
			maxX = maxX == 0 ? 1 : maxX + 0.1 * Math.abs(maxX);
			minX = minX == 0 ? -1 : minX - 0.1 * Math.abs(minX);
		}
		if (minY == maxY) {
			maxY = maxY == 0 ? 1 : maxY + 0.1 * Math.abs(maxY);
			minY = minY == 0 ? -1 : minY - 0.1 * Math.abs(minY);
		}

		// avoid invalid ranges
		if (minX > maxX || minY > maxY)
			return;

		xAxis.setLowerBound(minX);
		xAxis.setUpperBound(maxX);
		yAxis.setLowerBound(minY);
		yAxis.setUpperBound(maxY);
	}

	public void setTitle(String title) {
		chart.setTitle(title);
	}

	public void setAutoRefresh(boolean b) {
		this.autoRefresh = b;
	}

	public void setAutoZoom(boolean autoZoom) {
		this.autoZoom = autoZoom;
	}

	public void setShowSymbols(boolean b) {
		this.showSymbols = b;
		if (autoRefresh)
			refresh();
	}

	public void setShowDisabledData(boolean b) {
		this.showDisabledData = b;
	}

	public void setMaxDisplayPoints(int maxDisplayPoints) {
		this.maxDisplayPoints = maxDisplayPoints;
	}

	public void setReductionFactor(double reductionFactor) {
		this.reductionFactor = reductionFactor;
	}

	public void setSelectionMode(SelectionMode selectionMode) {
		this.selectionMode = selectionMode;
		switch (selectionMode) {
		case LINE_SELECTION:
			lineSelectionManager.start();
			rectangleSelectionManager.stop();
			polygonSelectionManager.stop();
			break;
		case RECTANGLE_SELECTION:
			lineSelectionManager.stop();
			rectangleSelectionManager.start();
			polygonSelectionManager.stop();
			break;
		case POLYGON_SELECTION:
			lineSelectionManager.stop();
			rectangleSelectionManager.stop();
			polygonSelectionManager.start();
			break;
		case ZOOM:
			lineSelectionManager.stop();
			rectangleSelectionManager.stop();
			polygonSelectionManager.stop();
			break;
		default:
			break;
		}
	}

	public boolean isSelecting() {
		return lineSelectionManager.isSelecting() || polygonSelectionManager.isSelecting()
				|| rectangleSelectionManager.isSelecting();
	}

	/**
	 * Sets the selection tool and associated color.
	 */
	public void setSelectionMode(SelectionMode mode, Color selectorColor) {
		setSelectionMode(mode);
		lineSelectionManager.setColor(selectorColor);
		rectangleSelectionManager.setColor(selectorColor);
		polygonSelectionManager.setColor(selectorColor);
	}

	/**
	 * @return current {@link ChartCursor}
	 */
	public ChartCursor getCursor() {
		return lineCursorManager.isEnabled() ? ChartCursor.LINE_CURSOR : ChartCursor.POINT_CURSOR;
	}

	/**
	 * Defines which cursor to display.
	 */
	public void setCursor(ChartCursor cursor) {
		lineCursorManager.setEnabled(cursor == ChartCursor.LINE_CURSOR);
		pointCursorManager.setEnabled(cursor == ChartCursor.POINT_CURSOR);
	}

	/**
	 * Moves cursor to a specific position.
	 */
	public void moveCursor(Point2D point) {
		lineCursorManager.moveTo(point);
		pointCursorManager.moveTo(point);
	}

	public void setAxisSynchronisation(boolean b) {
		axisSynchronisationEnabled = b;
	}

	/**
	 * Set zoom constraint
	 */
	public void setZoomConstraintAxis(AxisConstraint constraint) {
		if (zoomManager != null)
			// specific constraint strategy to keep zoom in axis enabled
			zoomManager.setMouseWheelAxisConstraintStrategy(context -> {
				if (context.isInXAxis())
					return AxisConstraint.Horizontal;
				else if (context.isInYAxis())
					return AxisConstraint.Vertical;
				return constraint;
			});
	}

	/// EVENT LISTENERS

	public void addSelectionListener(SelectionListener listener) {
		lineSelectionManager.addListener(listener);
		rectangleSelectionManager.addListener(listener);
		polygonSelectionManager.addListener(listener);
	}

	public void removeSelectionListener(SelectionListener listener) {
		lineSelectionManager.removeListener(listener);
		rectangleSelectionManager.removeListener(listener);
		polygonSelectionManager.removeListener(listener);
	}

	public boolean addCursorListener(Consumer<IChartData> listener) {
		return pointCursorManager.addListener(listener) && lineCursorManager.addListener(listener);
	}

	/// AXIS LABEL FORMATTERS

	/**
	 * Defines how the x axis label are formatted
	 */
	public void setXAxisLabelFormatter(Function<Number, String> formatter) {
		this.xFomatter = formatter;
		xAxis.setAxisTickFormatter(new AxisTickFormatter() {

			@Override
			public void setRange(double low, double high, double tickSpacing) {
				// not used
			}

			@Override
			public String format(Number value) {
				return formatter.apply(value);
			}
		});
	}

	public Function<Number, String> getXAxisLabelFormatter() {
		return xFomatter;
	}

	/**
	 * Defines how the y axis label are formatted
	 */
	public void setYAxisLabelFormatter(Function<Number, String> formatter) {
		this.yFomatter = formatter;
		yAxis.setAxisTickFormatter(new AxisTickFormatter() {

			@Override
			public void setRange(double low, double high, double tickSpacing) {
				// not used
			}

			@Override
			public String format(Number value) {
				return formatter.apply(value);
			}
		});
	}

	public Function<Number, String> getYAxisLabelFormatter() {
		return yFomatter;
	}

}
