package fr.ifremer.globe.ui.javafxchart.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javafx.beans.NamedArg;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javafx.scene.text.Text;

/**
 * Specific {@link LineChart} for Globe.
 */
public class GlobeChart extends LineChart<Double, Double> {

	private Optional<List<double[]>> optCustomLine = Optional.empty();
	private Path customLinePath = new Path();
	private Map<Node, Point2D> customNodes = new HashMap<>();

	/**
	 * Constructor
	 */
	public GlobeChart(@NamedArg("xAxis") Axis<Double> xAxis, @NamedArg("yAxis") Axis<Double> yAxis) {
		super(xAxis, yAxis, FXCollections.<Series<Double, Double>> observableArrayList());
	}

	/**
	 * Constructor
	 */
	public GlobeChart(@NamedArg("xAxis") Axis<Double> xAxis, @NamedArg("yAxis") Axis<Double> yAxis,
			@NamedArg("data") ObservableList<Series<Double, Double>> data) {
		super(xAxis, yAxis, data);
	}

	@Override
	protected void updateLegend() {
		/**
		 * !! Very important overriding !! Legends are not displayed, so the updateLegend method is useless.... However,
		 * this methods causes a lot of performance problems when a lot of series are loaded!
		 */
	}

	@Override
	public ObservableList<Node> getPlotChildren() {
		return super.getPlotChildren();
	}

	/**
	 * Specific layoutPlotChildren method to draw custom shapes. (could be overridden to define how series and points
	 * are designed)
	 */
	@Override
	protected void layoutPlotChildren() {
		super.layoutPlotChildren();
		// Custom elements are redrawn here (to manage pan or zoom events)
		drawCustomLine();
		relocateCustomNodes();
	}

	/// CUSTOM LINE

	private void drawCustomLine() {
		if (optCustomLine.isPresent()) {
			List<double[]> points = optCustomLine.get();

			if (getPlotChildren().contains(customLinePath))
				getPlotChildren().remove(customLinePath);

			if (points.size() < 2)
				return;

			ObservableList<PathElement> elements = customLinePath.getElements();
			elements.clear();

			double x = getXAxis().getDisplayPosition(points.get(0)[0]);
			double y = getYAxis().getDisplayPosition(points.get(0)[1]);
			elements.add(new MoveTo(x, y));

			for (int i = 1; i < points.size(); i++) {
				x = getXAxis().getDisplayPosition(points.get(i)[0]);
				y = getYAxis().getDisplayPosition(points.get(i)[1]);
				elements.add(new LineTo(x, y));
			}

			if (!getPlotChildren().contains(customLinePath) && optCustomLine.isPresent())
				getPlotChildren().add(customLinePath);
		}
	}

	public void setCustomLine(List<double[]> line, Color color, int strokeWidth) {
		optCustomLine = Optional.of(line);
		customLinePath.setStrokeWidth(strokeWidth);
		customLinePath.setStroke(color);
		drawCustomLine();
	}

	public void removeCustomLine() {
		optCustomLine = Optional.empty();
		if (getPlotChildren().contains(customLinePath))
			getPlotChildren().remove(customLinePath);
	}

	//// CUSTOM NODES

	private void relocateCustomNodes() {
		for (Entry<Node, Point2D> entry : customNodes.entrySet()) {
			Point2D position = entry.getValue();
			double x = getXAxis().getDisplayPosition(position.getX());
			double y = getYAxis().getDisplayPosition(position.getY());
			Node node = entry.getKey();

			if (node instanceof Text)
				x += 10;
			node.setTranslateX(x);
			node.setTranslateY(y);
		}
	}

	public void addCustomNode(Node node, Point2D position) {
		customNodes.put(node, position);
		if (!getPlotChildren().contains(node))
			getPlotChildren().add(node);
		relocateCustomNodes();
	}

	public void removeCustomNode(Node node) {
		customNodes.remove(node);
		if (getPlotChildren().contains(node))
			getPlotChildren().remove(node);
	}

	//// UTILS

	/**
	 * Model -> chart
	 */
	public Point2D getPositionInChart(Point2D point) {
		double xChart = getXAxis().getDisplayPosition(point.getX());
		double yChart = getYAxis().getDisplayPosition(point.getY());
		Point2D chartpoint = new Point2D(xChart, yChart);
		double x = getXAxis().localToScene(chartpoint).getX();
		double y = getYAxis().localToScene(chartpoint).getY();
		return new Point2D(x, y);
	}

	/**
	 * Chart -> model
	 */
	public Point2D getPositionInModel(MouseEvent mouseEvent) {
		Point2D mouseSceneCoords = new Point2D(mouseEvent.getSceneX(), mouseEvent.getSceneY());
		double x = getXAxis().getValueForDisplay(getXAxis().sceneToLocal(mouseSceneCoords).getX());
		double y = getYAxis().getValueForDisplay(getYAxis().sceneToLocal(mouseSceneCoords).getY());
		return new Point2D(x, y);
	}

}
