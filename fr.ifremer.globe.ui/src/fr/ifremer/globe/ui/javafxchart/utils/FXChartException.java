package fr.ifremer.globe.ui.javafxchart.utils;

import fr.ifremer.globe.utils.exception.GException;

public class FXChartException extends GException {
	
	private static final long serialVersionUID = -1291049933715540358L;

	/** Constructor **/
	public FXChartException(String string) {
		super("FXChart error : " + string);
	}
	
	/** Constructor **/
	public FXChartException(String string, Throwable t) {
		super("FXChart error : " + string, t);
	}
}