package fr.ifremer.globe.ui.javafxchart.utils;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;

/**
 * This class provides methods to apply the Douglas Peucker reduction algorithm.
 */
public class DouglasPeucker {

	/** Maximum deep level for recursive call (avoid to exception because of too much calls) **/
	private static final int MAX_DEEP_LEVEL = 100;

	/** Constructor **/
	private DouglasPeucker() {
		// utility class
	}

	/**
	 * Applies the "Douglas Peucker" algorithm while the reduced list size is over the target size. Each time, the
	 * reduction distance is grown to get a larger reduction. the reduction distance is the maximum allowed distance
	 * between real and reduced data
	 * 
	 */
	public static List<Integer> performRecursivly(IChartSeries<? extends IChartData> dataProxy, List<Integer> list,
			double reductionDistanceX, double scaleFactorXY, int targetSize) {
		// Apply "Douglas Peucker" algorithm while it's necessary
		while (list.size() > targetSize) {
			list = DouglasPeucker.performClassic(dataProxy, list, reductionDistanceX * reductionDistanceX,
					scaleFactorXY * scaleFactorXY);
			reductionDistanceX *= 1.5; // grow the reduction distance for the next loop
		}
		return list;
	}

	public static List<Integer> performClassic(IChartSeries<? extends IChartData> dataProxy, List<Integer> inputList,
			double reductionDistanceX, double scaleFactorXY) {
		return performClassic(dataProxy, inputList, reductionDistanceX, scaleFactorXY, 0);
	}

	/**
	 * Classic Douglas Peucker algorithm with {@link List}
	 * <p>
	 * Be careful with reduction distance (should be squared distance : see dist method) !
	 * <p>
	 */
	private static List<Integer> performClassic(IChartSeries<? extends IChartData> dataProxy, List<Integer> inputList,
			double reductionDistanceX, double scaleFactorXY, int deepLevel) {
		List<Integer> resultList = new ArrayList<>();

		// Get the most distant point
		double dmax = 0;
		int index = 0;

		int firstIndex = inputList.get(0);
		double[] firstPoint = { dataProxy.getData(firstIndex).getX(), dataProxy.getData(firstIndex).getY() };

		int lastIndex = inputList.get(inputList.size() - 1);
		double[] lastPoint = { dataProxy.getData(lastIndex).getX(), dataProxy.getData(lastIndex).getY() };

		for (int i = 1; i < inputList.size() - 1; i++) {
			double[] currentPoint = { dataProxy.getData(inputList.get(i)).getX(),
					dataProxy.getData(inputList.get(i)).getY() };
			double d = dist(currentPoint, firstPoint, lastPoint, scaleFactorXY);
			if (d > dmax) {
				index = i;
				dmax = d;
			}
		}

		if (deepLevel < MAX_DEEP_LEVEL && dmax > reductionDistanceX) {
			// Two new data list to process
			List<Integer> subList1 = performClassic(dataProxy, inputList.subList(0, index + 1), reductionDistanceX,
					scaleFactorXY, deepLevel + 1);
			List<Integer> subList2 = performClassic(dataProxy, inputList.subList(index, inputList.size()),
					reductionDistanceX, scaleFactorXY, deepLevel + 1);

			resultList.addAll(subList1.subList(0, subList1.size() - 1));
			resultList.addAll(subList2);
		} else {
			resultList.add(firstIndex);
			resultList.add(lastIndex);
		}
		return resultList;
	}

	/**
	 * @return square distance between a point and a segment
	 */
	private static double dist(double[] point, double[] startLine, double[] endLine, double squareScaleFactorXY) {
		double x1 = startLine[0];
		double y1 = startLine[1];

		double x2 = endLine[0];
		double y2 = endLine[1];

		double x3 = point[0];
		double y3 = point[1];

		double px = x2 - x1;
		double py = y2 - y1;

		double something = px * px + py * py * squareScaleFactorXY;

		double u = ((x3 - x1) * px + (y3 - y1) * py * squareScaleFactorXY) / something;

		if (u > 1) {
			u = 1;
		} else if (u < 0) {
			u = 0;
		}
		double x = x1 + u * px;
		double y = y1 + u * py;

		double dx = x - x3;
		double dy = y - y3;

		// Note: If the actual distance does not matter,
		// if you only want to compare what this function
		// returns to other results of this function, you
		// can just return the squared distance instead
		// (i.e. remove the sqrt) to gain a little performance
		//
		// Avoid use of Math.hypot()... bad performances!

		return (dx * dx + dy * dy * squareScaleFactorXY); // instead of "Math.sqrt(dx * dx + dy * dy)"
	}
}
