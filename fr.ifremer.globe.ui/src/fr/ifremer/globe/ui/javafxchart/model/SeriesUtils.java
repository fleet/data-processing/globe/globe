package fr.ifremer.globe.ui.javafxchart.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.shape.Shape;

/**
 * Series utility class.
 */
public class SeriesUtils {

	/** Constructor **/
	private SeriesUtils() {
		// no public constructor: utility class
	}

	public static Map<FXChartSeries, Double> getInterpolatedYValues(List<FXChartSeries> seriesList, double x) {
		var result = new HashMap<FXChartSeries, Double>();
		for (var series : seriesList) {
			// get points before and after 'x'
			var nearestPoint = getNearestPoint(List.of(series), x);
			var pointBefore = nearestPoint.getX() < x ? nearestPoint
					: series.getProxy().getData(Math.max(0, nearestPoint.getIndex() - 1));
			var pointAfter = nearestPoint.getX() > x ? nearestPoint
					: series.getProxy().getData(Math.min(series.getProxy().size() - 1, nearestPoint.getIndex() + 1));

			// compute interpolated value
			if (pointAfter.getX() != pointBefore.getX()) {
				var interpolatedValue = pointBefore.getY()
						+ ((x - pointBefore.getX()) / (pointAfter.getX() - pointBefore.getX()))
								* (pointAfter.getY() - pointBefore.getY());
				result.put(series, interpolatedValue);
			}
		}
		return result;
	}

	/**
	 * @return the nearest data in series from a specified position.
	 */
	public static IChartData getNearestPoint(List<FXChartSeries> seriesList, Point2D fromPosition) {
		return getNearestPoint(seriesList,
				chartData -> new Point2D(chartData.getX(), chartData.getY()).distance(fromPosition));
	}

	/**
	 * @return the nearest data in series from a specified position.
	 */
	public static IChartData getNearestPoint(List<FXChartSeries> seriesList, double x) {
		return getNearestPoint(seriesList, chartData -> Math.abs(x - chartData.getX()));
	}

	/**
	 * @return the nearest data in series from a specified position.
	 */
	public static IChartData getNearestPoint(List<FXChartSeries> seriesList,
			ToDoubleFunction<IChartData> distanceSupplier) {
		IChartData neareastData = null;
		double distance = Double.MAX_VALUE;
		FXChartSeries researhSeries = null;
		int indexStartResearh = 0;
		int indexEndResearh = 0;

		// First step: research the nearest point in display points (faster)
		for (FXChartSeries series : seriesList) {
			var dataList = series.getDisplayedData();
			for (int i = 0; i < dataList.size(); i++) {
				var currentDistance = distanceSupplier.applyAsDouble(dataList.get(i));
				if (currentDistance < distance) {
					distance = currentDistance;
					researhSeries = series;
					indexStartResearh = i > 0 ? dataList.get(i - 1).getIndex() : 0;
					indexEndResearh = (i == dataList.size() - 1) ? dataList.get(i).getIndex()
							: dataList.get(i + 1).getIndex();
				}
			}
		}

		// Second step: research the nearest point in real data (between limits defined by the first step)
		distance = Double.MAX_VALUE;
		if (researhSeries != null) {
			for (int i = indexStartResearh; i <= indexEndResearh; i++) {
				IChartData data = researhSeries.getProxy().getData(i);
				var currentDistance = distanceSupplier.applyAsDouble(data);
				if (currentDistance < distance) {
					distance = currentDistance;
					neareastData = data;
				}
			}
		}

		return neareastData;
	}

	/**
	 * @return the list of displayed point inside an interval.
	 */
	public static List<double[]> getDisplayDataInInterval(List<FXChartSeries> seriesList, IChartData start,
			IChartData end) {
		List<double[]> dataList = new ArrayList<>();

		// Swap start and end if needed
		if (start.compareTo(end) > 0) {
			IChartData tmp = start;
			start = end;
			end = tmp;
		}

		List<IChartData> selectedData = new ArrayList<>();
		for (FXChartSeries series : seriesList) {
			for (IChartData currentData : series.getDisplayedData()) {
				if (0 <= currentData.compareTo(start) && currentData.compareTo(end) <= 0) {
					selectedData.add(currentData);
				}
			}
		}
		// Sort data
		Collections.sort(selectedData);
		selectedData.forEach(dataProxy -> dataList.add(new double[] { dataProxy.getX(), dataProxy.getY() }));
		return dataList;
	}

	/**
	 * @return all points inside an interval.
	 */
	public static List<IChartData> getAllDataInInterval(List<FXChartSeries> seriesList, IChartData start,
			IChartData end) {
		// Swap start and end if needed
		if (start.compareTo(end) > 0) {
			IChartData tmp = start;
			start = end;
			end = tmp;
		}
		final IChartData fStart = start;
		final IChartData fEnd = end;

		// Sort series
		seriesList.sort((seriesA, seriesB) -> seriesA.getProxy().getData(0).compareTo(seriesB.getProxy().getData(0)));
		return seriesList.stream().map(FXChartSeries::getProxy)
				// Filter series to keep only those intersect the interval
				.filter(proxy -> proxy.compareTo(fStart) <= 0 && proxy.compareTo(fEnd) >= 0) //
				.flatMap(IChartSeries::stream)
				// Get data in interval
				.filter(data -> 0 <= data.compareTo(fStart) && data.compareTo(fEnd) <= 0).collect(Collectors.toList());
	}

	/**
	 * @return {@link IChartData} inside the specified shape (polygon, rectangle...).
	 */
	public static List<IChartData> getDataInShape(List<FXChartSeries> seriesList, Shape shape) {
		List<IChartData> dataList = new ArrayList<>();

		Stream<IChartSeries<? extends IChartData>> seriesProxies = seriesList.stream()
				.filter(series -> shape.intersects(series.getBounds())).map(FXChartSeries::getProxy);

		seriesProxies.forEach(seriesProxy -> {
			for (IChartData data : seriesProxy) {
				// /!\ BE CAREFUL: DOESN'T WORK WITH LARGE NUMBER /!\
				// "contains()" method cast double as float
				if (shape.contains(data.getX(), data.getY())) {
					dataList.add(data);
				}
			}
		});

		return dataList;
	}

	/**
	 * @return {@link IChartData} inside the specified rectangle.
	 */
	public static List<IChartData> getDataInShape(List<FXChartSeries> seriesList, Rectangle2D rectangle) {
		List<IChartData> dataList = new ArrayList<>();

		Bounds rectBounds = new BoundingBox(rectangle.getMinX(), rectangle.getMinY(), rectangle.getWidth(),
				rectangle.getHeight());
		Stream<IChartSeries<? extends IChartData>> seriesProxies = seriesList.stream()
				.filter(series -> series.getBounds().intersects(rectBounds)).map(FXChartSeries::getProxy);

		seriesProxies.forEach(seriesProxy -> {
			for (IChartData data : seriesProxy) {
				if (rectangle.contains(data.getX(), data.getY())) {
					dataList.add(data);
				}
			}
		});

		return dataList;
	}

}
