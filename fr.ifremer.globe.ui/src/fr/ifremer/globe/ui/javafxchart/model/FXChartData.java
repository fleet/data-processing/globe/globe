package fr.ifremer.globe.ui.javafxchart.model;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.ui.javafxchart.utils.TooltipStartTimingHacker;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Tooltip;

public class FXChartData {

	// Properties
	private final IChartData proxy;
	private final Data<Double, Double> validData;

	/** Constructor */
	public FXChartData(IChartData proxy) {
		this.proxy = proxy;
		this.validData = new XYChart.Data<>(proxy.getX(), proxy.getY());
	}

	/**
	 * Sets CSS (color) and tooltip
	 */
	protected void setCSSAndTooltip() {
		// Valid point tooltip and color
		if (validData.getNode() != null) {
			Tooltip t = new Tooltip(proxy.getTooltip());
			TooltipStartTimingHacker.hack(t);
			Tooltip.install(validData.getNode(), t);

			String colorString = proxy.getColor().toString().replaceAll("0x", "#");
			String lineStyle = String.format("-fx-background-color: %s;", colorString);
			validData.getNode().lookup(".chart-line-symbol").setStyle(lineStyle);
			
			//StackPane p = (StackPane) validData.getNode();
			//p.getBackground().getFills().clear();
			//p.setPrefSize(5, 5);
		    //p.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
		}
	}

	// GETTERS
	
	public  IChartData getDataProxy() {
		return proxy;
	}
	
	public Data<Double, Double> getSeriesData() {
		return validData;
	}

}
