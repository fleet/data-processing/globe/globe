package fr.ifremer.globe.ui.javafxchart.model;

import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.ui.javafxchart.utils.DouglasPeucker;
import javafx.geometry.BoundingBox;

public class SeriesReducer {

	/** Constants to define how many points will be mandatory **/
	private static final float MANDATORY_POINTS_PART = 0.3f;
	private static final float OTHER_POINTS_PART = 0.7f;

	/** Properties **/
	private final IChartSeries<? extends IChartData> seriesProxy;
	private double previousReductionFactor = -1;
	private int previousMaxPointNumber = -1;
	private TreeSet<Integer> permanentList;
	private boolean douglasPeuckerApplied = false;
	private BoundingBox previousBounds;
	private TreeSet<Integer> previousResult;

	/**
	 * Constructor
	 */
	public SeriesReducer(IChartSeries<? extends IChartData> seriesProxy) {
		this.seriesProxy = seriesProxy;
	}

	/**
	 * Keeps some mandatory points
	 * 
	 * @param reductionDistanceX : maximum allowed distance between real and reduced data on the X axis
	 * @param scaleFactorXY : scale between X and Y displayed data
	 * @param maxPointNumber : maximum allowed resulting number of points
	 */
	private void initMandatoryPoints(double reductionDistanceX, double scaleFactorXY, int maxPointNumber) {
		List<Integer> reducedList = IntStream.rangeClosed(0, seriesProxy.size() - 1).boxed()
				.collect(Collectors.toList());
		permanentList = new TreeSet<>(DouglasPeucker.performRecursivly(seriesProxy, reducedList, reductionDistanceX,
				scaleFactorXY, (int) (MANDATORY_POINTS_PART * maxPointNumber)));
	}

	/**
	 * Get reduced list of points in the given bounding box
	 * 
	 * @param bounds : input bounding box
	 * @param reductionFactor : factor of the bounding box used to initialize DouglasPeuker algorithm
	 * @param maxPointNumber : maximum allowed resulting number of points
	 *
	 * @return selected indexes after apply a reduction on its {@link IChartSeries}
	 */
	public TreeSet<Integer> get(BoundingBox bounds, double reductionFactor, int maxPointNumber) {
		TreeSet<Integer> result = new TreeSet<>();

		// Recompute reduction only if necessary
		if (reductionFactor != previousReductionFactor || maxPointNumber != previousMaxPointNumber
				|| !bounds.equals(previousBounds)) {

			// Get data inside specified intersection
			List<Integer> reducedList = getDataInside(bounds).map(IChartData::getIndex).collect(Collectors.toList());
			if (reducedList.isEmpty()) {
				douglasPeuckerApplied = false;
				return result;
			}

			// Apply "Douglas Peucker" reduction algorithm
			int sizeAfterReduction = (int) (OTHER_POINTS_PART * maxPointNumber);
			douglasPeuckerApplied = reducedList.size() > sizeAfterReduction;
			if (douglasPeuckerApplied) {
				reducedList = DouglasPeucker.performRecursivly(seriesProxy, reducedList,
						reductionFactor * bounds.getWidth(), bounds.getWidth() / bounds.getHeight(),
						sizeAfterReduction);
			}
			result.addAll(reducedList);

			// Add permanent indexes (to keep the global form and avoids artificial lines)
			if (permanentList == null || reductionFactor != previousReductionFactor
					|| maxPointNumber != previousMaxPointNumber) {
				initMandatoryPoints(reductionFactor * bounds.getWidth(), bounds.getWidth() / bounds.getHeight(),
						maxPointNumber);
			}
			result.addAll(permanentList);
		} else {
			result = previousResult;
		}

		// Add points when color or section change
		IChartData data1;
		IChartData data2 = seriesProxy.getData(0);
		for (int i = 1; i < seriesProxy.size(); i++) {
			data1 = data2;
			data2 = seriesProxy.getData(i);
			if (!data1.getSectionId().equals(data2.getSectionId()) || !data1.getColor().equals(data2.getColor())) {
				result.add(i - 1);
				result.add(i);
			}
		}

		previousResult = result;
		previousBounds = bounds;
		previousReductionFactor = reductionFactor;
		previousMaxPointNumber = maxPointNumber;
		return result;
	}

	/**
	 * @return true if the reduction algorithm has been applied
	 */
	public boolean isDouglasPeuckerApplied() {
		return douglasPeuckerApplied;
	}

	/**
	 * @return {@link IChartData} contained inside the specified bounds
	 */
	private Stream<? extends IChartData> getDataInside(BoundingBox bounds) {
		return StreamSupport.stream(seriesProxy.spliterator(), false)
				.filter(data -> bounds.contains(data.getX(), data.getY()));
	}
}
