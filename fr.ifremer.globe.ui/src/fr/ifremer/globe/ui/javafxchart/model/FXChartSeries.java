package fr.ifremer.globe.ui.javafxchart.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.javafxchart.view.GlobeChart;
import javafx.geometry.BoundingBox;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

/**
 * This class builds and displays chart {@link Series} from a {@link IChartSeries}.
 */
public class FXChartSeries {

	/** Properties **/
	private final IChartSeries<? extends IChartData> seriesProxy;
	private final SeriesReducer seriesReducer;
	private final BoundingBox bounds;
	private List<? extends IChartData> dataList = Collections.unmodifiableList(new ArrayList<>());
	private final GlobeChart chart;
	private final Map<String, Series<Double, Double>> seriesMap = new HashMap<>();
	private Map<String, Section> sectionMap = new HashMap<>();
	private Map<String, Section> previousSectionMap = new HashMap<>();

	/**
	 * Constructor
	 */
	public FXChartSeries(GlobeChart chart, IChartSeries<? extends IChartData> proxy) {
		this.seriesProxy = proxy;
		this.seriesReducer = new SeriesReducer(proxy);
		this.chart = chart;
		this.bounds = computeBounds(false);
	}

	public void dispose() {
		chart.getData().removeAll(seriesMap.values());
	}

	/**
	 * @return {@link BoundingBox} computed from {@link IChartData}.
	 */
	public BoundingBox computeBounds(boolean keepOnlyValid) {
		var resultingBoundingBox = new BoundingBox(0, 0, 0, 0);
		if (seriesProxy.size() > 0) {
			double minX = Double.POSITIVE_INFINITY;
			double maxX = Double.NEGATIVE_INFINITY;
			double minY = Double.POSITIVE_INFINITY;
			double maxY = Double.NEGATIVE_INFINITY;
			for (var dataP : seriesProxy) {
				if (keepOnlyValid && !dataP.isEnabled())
					continue;
				if (!Double.isNaN(dataP.getX())) {
					minX = Math.min(minX, dataP.getX());
					maxX = Math.max(maxX, dataP.getX());
				}
				if (!Double.isNaN(dataP.getY())) {
					minY = Math.min(minY, dataP.getY());
					maxY = Math.max(maxY, dataP.getY());
				}
			}
			resultingBoundingBox = new BoundingBox(minX, minY, Math.abs(minX - maxX), Math.abs(minY - maxY));
		}
		return resultingBoundingBox;
	}

	//// GETTERS

	public IChartSeries<? extends IChartData> getProxy() {
		return seriesProxy;
	}

	public BoundingBox getBounds() {
		return bounds;
	}

	public List<? extends IChartData> getDisplayedData() {
		return dataList;
	}

	/**
	 * Reloads with reduction if necessary : computes points to display
	 * 
	 * @return true if the reduction algorithm has been applied
	 */
	public boolean reload(BoundingBox intersection, double reductionFactor, int maxPointNumber,
			boolean keepDisabledData) {
		Set<Integer> reducedSeriesIndexes = seriesReducer.get(intersection, reductionFactor, maxPointNumber);
		sectionMap = computeSections(reducedSeriesIndexes, keepDisabledData);
		return seriesReducer.isDouglasPeuckerApplied();
	}

	/**
	 * Inner class to group a list of {@link IChartData} to add to the same JavaFX {@link Series}. It allows to split a
	 * {@link IChartSeries} is in several segments.
	 */
	class Section {
		private static final String DEFAULT_ID = "DEFAULT_";
		private final String iD;
		private final GColor color;
		private final int width;
		private final List<IChartData> dataList = new ArrayList<>();

		public Section(IChartData chartData) {
			// the id is either the current data proxy ID or a default computed ID
			Optional<String> optDataSectionId = chartData.getSectionId();
			this.iD = optDataSectionId.isPresent() ? optDataSectionId.get() : DEFAULT_ID + chartData.getIndex();
			this.color = chartData.getColor();
			this.width = chartData.getWidth();
		}

		public String getID() {
			return iD;
		}

		public GColor getColor() {
			return color;
		}

		public boolean isDefault() {
			return iD.contains(DEFAULT_ID);
		}

		public void addData(IChartData dataProxy) {
			dataList.add(dataProxy);
		}

		public List<IChartData> getDataList() {
			return dataList;
		}

		public String getCSSLineStyle() {
			String colorString = getColor().toCSSString();
			String lineStyle = String.format("-fx-stroke: %s;", colorString);
			lineStyle += String.format("-fx-stroke-width: %d;", width);
			return lineStyle;
		}

		public boolean isIdentical(Section otherSection) {
			if (!otherSection.getColor().equals(color))
				return false;
			if (otherSection.getDataList().size() != getDataList().size())
				return false;
			if (otherSection.dataList.stream().anyMatch(data -> !dataList.contains(data)))
				return false;
			return true;
		}

		public boolean isIdentical(Series<Double, Double> value) {
			if (value.getData().size() != dataList.size())
				return false;
			// compare each data coordinates
			for (int i = 0; i < dataList.size(); i++) {
				var sectionData = dataList.get(i);
				var seriesData = value.getData().get(i);
				if (sectionData.getX() != seriesData.getXValue() || sectionData.getY() != seriesData.getYValue())
					return false;
			}
			return true;
		}
	}

	/**
	 * Computes {@link Section} with {@link IChartData} to display.
	 */
	private Map<String, Section> computeSections(Collection<Integer> reducedSeriesIndexes, boolean keepDisabledData) {
		Map<String, Section> result = new HashMap<>();
		Section currentSection = null;

		for (int index : reducedSeriesIndexes) {
			IChartData data = seriesProxy.getData(index);
			Optional<String> optDataSectionId = data.getSectionId();

			// find the section where data should be added, if necessary creates a new one
			if (optDataSectionId.isPresent()) {
				if (result.containsKey(optDataSectionId.get())) {
					currentSection = result.get(optDataSectionId.get());
				} else {
					currentSection = new Section(data);
					result.put(currentSection.getID(), currentSection);
				}
			} else if (currentSection == null || !currentSection.isDefault()
					|| !currentSection.getColor().equals(data.getColor())) {
				currentSection = new Section(data);
				result.put(currentSection.getID(), currentSection);
			}

			// Add data to the current section
			currentSection.addData(data);
		}

		List<IChartData> newDataList = new ArrayList<>();
		result.values().forEach(section -> newDataList.addAll(section.getDataList()));
		Collections.sort(newDataList);
		dataList = Collections.unmodifiableList(newDataList);

		// filter disabled data if necessary
		if (!keepDisabledData)
			result = result.entrySet().stream()
					.filter(entry -> entry.getValue().getDataList().stream().anyMatch(IChartData::isEnabled))
					.collect(Collectors.toMap(Entry::getKey, Entry::getValue));

		return result;
	}

	/**
	 * Refreshes chart {@link Series} (to call in graphical thread).
	 * 
	 * (Note: the update of existing Series is faster than create new ones.)
	 */
	public void refreshChartSeries() {
		// all series are removed and after added to chart to conserve the order (from background to front) if there is
		// several FXChartSeries (performance lost...)
		chart.getData().removeAll(seriesMap.values());

		// remove obsolete series (compare section map with the previous to know which section disappeared)
		var seriesToRemove = seriesMap.entrySet().stream().filter(entry ->
		// first, check if section has not changed
		!sectionMap.containsKey(entry.getKey()) || !previousSectionMap.containsKey(entry.getKey())
				|| !previousSectionMap.get(entry.getKey()).isIdentical(sectionMap.get(entry.getKey()))
				// after, check if value of each data has not changed (compare to displayed series)
				|| !previousSectionMap.get(entry.getKey()).isIdentical(entry.getValue()))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		previousSectionMap = sectionMap;
		seriesToRemove.forEach(seriesMap::remove);

		// creates new series
		var seriesToAdd = sectionMap.entrySet().stream().filter(e -> !seriesMap.containsKey(e.getKey())).map(e -> {
			Series<Double, Double> series = new Series<>();
			var data = e.getValue().getDataList().stream().map(d -> new XYChart.Data<>(d.getX(), d.getY())).toList();
			series.getData().setAll(data);
			return Map.entry(e.getKey(), series);
		}).collect(Collectors.toMap(Entry::getKey, Entry::getValue));

		seriesMap.putAll(seriesToAdd);
		chart.getData().addAll(seriesMap.values());

		// set CSS style (for all, because they have been all re-added (see first comment of this method))
		seriesMap.forEach((key, series) -> {
			var section = sectionMap.get(key);
			series.getNode().lookup(".chart-series-line").setStyle(section.getCSSLineStyle());
			series.getData().forEach(chartData -> {
				if (chartData.getNode() != null) {
					String colorString = section.getColor().toCSSString();
					String lineStyle = String.format("-fx-background-color: %s;", colorString);
					chartData.getNode().lookup(".chart-line-symbol").setStyle(lineStyle);
				}
			});
		});
	}

}
