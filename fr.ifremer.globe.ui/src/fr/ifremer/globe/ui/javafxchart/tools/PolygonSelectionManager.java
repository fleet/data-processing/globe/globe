package fr.ifremer.globe.ui.javafxchart.tools;

import java.util.List;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.ui.javafxchart.model.SeriesUtils;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import fr.ifremer.globe.ui.javafxchart.view.GlobeChart;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

/**
 * This class provides a manager for the line selection functionality.
 */
public class PolygonSelectionManager extends ASelectionManager {

	/** Properties **/
	private final GlobeChart chart;
	private final Polygon polygon;

	/**
	 * Constructor
	 */
	public PolygonSelectionManager(FXChartController chartController) {
		super(chartController);
		this.chart = chartController.getChart();

		// Creates selection polygon
		polygon = new Polygon();
		polygon.setId("selectionPolygon");
		setColor(Color.BLACK);

		// Add event handlers
		handlerManager.addEventHandler(false, MouseEvent.MOUSE_CLICKED, this::onMousePressed);
		handlerManager.addEventHandler(false, MouseEvent.MOUSE_MOVED, this::onMouseMouved);
	}
	
	/**
	 * @return true if a selection is happening
	 */
	public boolean isSelecting() {
		return polygon.getPoints().size() > 0;
	}
	
	@Override
	public void start() {
		super.start();
		if (!chart.getPlotChildren().contains(polygon))
			chart.getPlotChildren().add(polygon);
	}
	
	@Override
	public void stop() {
		super.stop();
		polygon.getPoints().clear();
		if (chart.getPlotChildren().contains(polygon))
			chart.getPlotChildren().remove(polygon);
	}

	/**
	 * Sets the polygon color
	 */
	public void setColor(Color selectorColor) {
		polygon.setStroke(selectorColor);
		polygon.setFill(new Color(selectorColor.getRed(), selectorColor.getGreen(), selectorColor.getBlue(), 0.2));
	}

	//// PRIVATE METHODS TO MANAGE MOUSE EVENTS

	private void onMousePressed(MouseEvent mouseEvent) {
		if (passesFilter(mouseEvent)) {
			if (mouseEvent.getClickCount() == 1) {
				Point2D mouseCoords = getMouseCoordsInChart(mouseEvent);
				polygon.getPoints().addAll(mouseCoords.getX(), mouseCoords.getY());
			}
			// double click : end of selection
			else if (mouseEvent.getClickCount() == 2) {
				Polygon modelPolygon = new Polygon();
				for (int i = 0; i < polygon.getPoints().size() - 1; i += 2) {
					double chartPointX = polygon.getPoints().get(i);
					double chartPointY = polygon.getPoints().get(i + 1);

					modelPolygon.getPoints().add((Double) chart.getXAxis().getValueForDisplay(chartPointX));
					modelPolygon.getPoints().add((Double) chart.getYAxis().getValueForDisplay(chartPointY));
				}
	
				List<IChartData> selectedData = SeriesUtils.getDataInShape(chartController.getSeries(), modelPolygon);
				if (!selectedData.isEmpty())
					listeners.forEach(l -> l.onSelection(selectedData));
				
				polygon.getPoints().clear();
			}
		}
	}

	private void onMouseMouved(MouseEvent mouseEvent) {
		ObservableList<Double> points = polygon.getPoints();
		if (!points.isEmpty()) {
			Point2D mouseCoords = getMouseCoordsInChart(mouseEvent);
			if (points.size() < 4) {
				points.addAll(mouseCoords.getX(), mouseCoords.getY());
			} else {
				points.set(points.size() - 2, mouseCoords.getX());
				points.set(points.size() - 1, mouseCoords.getY());
			}
		}
	}
	
	/**
	 * Provides mouse position in chart
	 */
	private Point2D getMouseCoordsInChart(MouseEvent mouseEvent) {
		Point2D mouseSceneCoords = new Point2D(mouseEvent.getSceneX(), mouseEvent.getSceneY());
		double x = chart.getXAxis().sceneToLocal(mouseSceneCoords).getX();
		double y = chart.getYAxis().sceneToLocal(mouseSceneCoords).getY();
		return new Point2D(x, y);
	}

}
