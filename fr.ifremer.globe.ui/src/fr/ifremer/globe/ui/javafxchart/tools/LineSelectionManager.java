package fr.ifremer.globe.ui.javafxchart.tools;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.ui.javafxchart.model.SeriesUtils;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import io.reactivex.disposables.Disposable;
import io.reactivex.processors.PublishProcessor;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * This class provides a manager for the line selection functionality.
 */
public class LineSelectionManager extends ASelectionManager {

	/** Properties **/
	private AtomicBoolean isSelecting = new AtomicBoolean();
	private Optional<IChartData> startData = Optional.empty();
	private Color lineColor = Color.BLACK;

	// Use of debounce JavaRx functionality
	private final PublishProcessor<MouseEvent> publishProcessor = PublishProcessor.create();
	private final Disposable disposable;

	/**
	 * Constructor
	 */
	public <X, Y> LineSelectionManager(FXChartController chartController) {
		super(chartController);
		handlerManager.addEventHandler(false, MouseEvent.MOUSE_CLICKED, this::onMousePressed);
		handlerManager.addEventHandler(false, MouseEvent.MOUSE_MOVED, this::onMouseMouved);
		handlerManager.addEventHandler(false, KeyEvent.KEY_PRESSED, this::onKeyPressed);// TODO : doesn't work!

		disposable = publishProcessor.sample(20, TimeUnit.MILLISECONDS).subscribe(this::updateSelectionSeries);
	}

	public void dispose() {
		disposable.dispose();
	}

	/**
	 * Sets the line color
	 */
	public void setColor(Color selectorColor) {
		lineColor = selectorColor;
	}

	public boolean isSelecting() {
		return isSelecting.get();
	}

	@Override
	public void stop() {
		super.stop();
		stopSelection();
	}

	private void stopSelection() {
		startData = Optional.empty();
		chartController.getChart().removeCustomLine();
	}

	//// PRIVATE METHODS TO MANAGE EVENTS

	private void onMousePressed(MouseEvent mouseEvent) {
		if (passesFilter(mouseEvent)) {
			if (!startData.isPresent()) {
				// Start selection
				Point2D point = chartInfo.getDataCoordinates(mouseEvent.getX(), mouseEvent.getY());
				startData = Optional.of(SeriesUtils.getNearestPoint(chartController.getSeries(), point));
				isSelecting.set(true);
			} else {
				// End selection
				isSelecting.set(false);
				Point2D point = chartInfo.getDataCoordinates(mouseEvent.getX(), mouseEvent.getY());
				IChartData endData = SeriesUtils.getNearestPoint(chartController.getSeries(), point);
				List<IChartData> selectedData = SeriesUtils.getAllDataInInterval(chartController.getSeries(),
						startData.get(), endData);
				if (!selectedData.isEmpty())
					listeners.forEach(l -> l.onSelection(selectedData));

				stopSelection();
			}
		}
	}

	private void onMouseMouved(MouseEvent mouseEvent) {
		publishProcessor.onNext(mouseEvent);
	}

	private void onKeyPressed(KeyEvent keyEvent) {
		if (keyEvent.getCode() == KeyCode.ESCAPE)
			stopSelection();
	}

	/**
	 * Updates the selection line
	 */
	private void updateSelectionSeries(MouseEvent mouseEvent) {
		if (isSelecting.get()) {

			Point2D currentPoint = chartInfo.getDataCoordinates(mouseEvent.getX(), mouseEvent.getY());
			IChartData currentData = SeriesUtils.getNearestPoint(chartController.getSeries(), currentPoint);

			try {
				List<double[]> points = SeriesUtils.getDisplayDataInInterval(chartController.getSeries(),
						startData.get(), currentData);

				Platform.runLater(() -> {
					if (isSelecting.get()) {
						chartController.getChart().setCustomLine(points, lineColor, 4);
						chartController.updateInfoText(
								"Selection from " + startData.get().getTooltip() + "\nto " + currentData.getTooltip());
					}
				});
			} catch (NullPointerException | NoSuchElementException | ConcurrentModificationException e) {
				// displayed data list has been modified : ignore this update (no real impact)
			}
		}
	}

}
