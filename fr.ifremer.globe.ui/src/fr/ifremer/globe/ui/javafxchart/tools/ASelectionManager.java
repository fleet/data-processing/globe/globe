package fr.ifremer.globe.ui.javafxchart.tools;

import java.util.ArrayList;
import java.util.List;

import org.gillius.jfxutils.EventHandlerManager;
import org.gillius.jfxutils.chart.XYChartInfo;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * Abstract class for the selection functionalities.
 */
public abstract class ASelectionManager {

	/**
	 * Interface implement by the listener of selection
	 */
	@FunctionalInterface
	public interface SelectionListener {
		void onSelection(List<IChartData> selectedData);
	}

	/**
	 * The default mouse filter for the filters events unless only primary mouse button (usually left) is depressed.
	 */
	public static final EventHandler<MouseEvent> DEFAULT_FILTER = mouseEvent -> {
		// The ChartPanManager uses this reference, so if behavior changes, copy to users first.
		if (mouseEvent.getButton() != MouseButton.PRIMARY)
			mouseEvent.consume();
	};

	/** Properties **/
	protected final FXChartController chartController;
	protected final XYChartInfo chartInfo;
	protected final EventHandlerManager handlerManager;
	private EventHandler<? super MouseEvent> mouseFilter = DEFAULT_FILTER;
	protected final List<SelectionListener> listeners = new ArrayList<>();

	/**
	 * Constructor
	 */
	public <X, Y> ASelectionManager(FXChartController chartController) {
		this.chartController = chartController;
		this.chartInfo = new XYChartInfo(chartController.getChart(), chartController.getChart());
		this.handlerManager = new EventHandlerManager(chartController.getChart());
	}

	/**
	 * Returns the mouse filter.
	 *
	 * @see #setMouseFilter(EventHandler)
	 */
	public EventHandler<? super MouseEvent> getMouseFilter() {
		return mouseFilter;
	}

	/**
	 * Sets the mouse filter for starting the action. If the filter consumes the event with {@link Event#consume()},
	 * then the event is ignored. If the filter is null, all events are passed through. The default filter is
	 * {@link #DEFAULT_FILTER}.
	 */
	public void setMouseFilter(EventHandler<? super MouseEvent> mouseFilter) {
		this.mouseFilter = mouseFilter;
	}

	/**
	 * Start management by adding event handlers
	 */
	public void start() {
		handlerManager.addAllHandlers();
	}

	/**
	 * Stop management by removing all event handlers and bindings
	 */
	public void stop() {
		handlerManager.removeAllHandlers();
	}

	/**
	 * Filters the events
	 */
	protected boolean passesFilter(MouseEvent event) {
		if (mouseFilter != null) {
			MouseEvent cloned = (MouseEvent) event.clone();
			mouseFilter.handle(cloned);
			if (cloned.isConsumed())
				return false;
		}
		return true;
	}

	/**
	 * Adds a new {@link SelectionListener}
	 */
	public boolean addListener(SelectionListener newListener) {
		return listeners.add(newListener);
	}

	/**
	 * Removes a {@link SelectionListener}
	 */
	public boolean removeListener(SelectionListener newListener) {
		return listeners.remove(newListener);
	}

}
