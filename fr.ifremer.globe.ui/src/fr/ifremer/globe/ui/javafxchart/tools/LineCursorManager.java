package fr.ifremer.globe.ui.javafxchart.tools;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.stream.Collectors;

import fr.ifremer.globe.ui.javafxchart.model.SeriesUtils;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import fr.ifremer.globe.utils.number.NumberUtils;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * This class handles the display of a line cursor.
 */
public class LineCursorManager extends AbstractCursorManager {

	/** Properties **/
	private Line line = new Line();
	private final List<Circle> circles = new ArrayList<>();

	/**
	 * Constructor
	 */
	public <X, Y> LineCursorManager(FXChartController chartController) {
		super(chartController);
		line.getStyleClass().add("cursor-line");
	}

	@Override
	protected void draw(Point2D modelPosition, Point2D scenePosition) {
		drawLine(scenePosition);
		drawCirclesAndUpdateToolTip(modelPosition);
	}

	@Override
	protected void hide() {
		hideCircles();
		hideLine();
	}

	// LINE

	private void drawLine(Point2D positionInScene) {
		if (!chart.getPlotChildren().contains(line))
			chart.getPlotChildren().add(line);
		var x = chart.getXAxis().sceneToLocal(positionInScene).getX();
		line.setStartX(x);
		line.setEndX(x);
		line.setEndY(chart.getHeight());
	}

	private void hideLine() {
		chart.getPlotChildren().remove(line);
	}

	// CIRCLES

	private void drawCircles(List<Point2D> positionInModel) {
		hideCircles();
		// if (circles.size() < 100)
		for (var point : positionInModel) {
			var circle = new Circle(3);
			circle.getStyleClass().add("cursor-circle");
			circles.add(circle);
			chart.addCustomNode(circle, point);
		}
	}

	private void hideCircles() {
		circles.forEach(chart::removeCustomNode);
		circles.clear();
	}

	// TOOLTIP

	private void drawCirclesAndUpdateToolTip(Point2D modelPosition) {
		try {
			var valuesBySeries = SeriesUtils.getInterpolatedYValues(chartController.getSeries(), modelPosition.getX());
			// refresh interface
			Platform.runLater(() -> {
				// draw circles around data
				List<Point2D> neasestDataPoints = valuesBySeries.values().stream()
						.map(y -> new Point2D(modelPosition.getX(), y)).collect(Collectors.toList());
				drawCircles(neasestDataPoints);

				// update info text
				var infoText = valuesBySeries.entrySet().stream().map(entry -> entry.getKey().getProxy().getTitle()
						+ " = " + NumberUtils.getStringDouble(entry.getValue())).collect(Collectors.joining("; "));
				if (!infoText.isEmpty()) {
					String xStringValue = chartController.getXAxisLabelFormatter() != null
							? chartController.getXAxisLabelFormatter().apply(modelPosition.getX())
							: NumberUtils.getStringDouble(modelPosition.getX());
					chartController.updateInfoText(String.format("%s", xStringValue + "; " + infoText));
				} else
					chartController.updateInfoText("");
			});
		} catch (NullPointerException | ConcurrentModificationException e) {
			// displayed data list has been modified : ignore this update (no real impact)
		}
	}

}
