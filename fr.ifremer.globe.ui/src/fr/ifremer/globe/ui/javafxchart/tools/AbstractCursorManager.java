package fr.ifremer.globe.ui.javafxchart.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.ui.javafxchart.model.SeriesUtils;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import fr.ifremer.globe.ui.javafxchart.view.GlobeChart;
import fr.ifremer.globe.utils.number.NumberUtils;
import io.reactivex.disposables.Disposable;
import io.reactivex.processors.PublishProcessor;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;

/**
 * Abstract class to handle and display cursor.
 */
public abstract class AbstractCursorManager {

	/** Properties **/
	private final AtomicBoolean enabled = new AtomicBoolean();
	protected final FXChartController chartController;
	protected final GlobeChart chart;

	/** Listener of {@link MouseEvent} **/
	private final EventHandler<? super MouseEvent> mouseEventHandler = this::moveTo;

	/**
	 * Handle of cursor listeners : notify when {@link IChartData} are selected
	 */
	private final List<Consumer<IChartData>> listeners = new ArrayList<>();
	// Use of sampling JavaRx functionality
	private final PublishProcessor<Point2D> modelPositionPublishProcessor = PublishProcessor.create();
	private final Disposable disposable;
	{
		disposable = modelPositionPublishProcessor.sample(20, TimeUnit.MILLISECONDS).subscribe(this::doNotifyListeners);
	}

	/**
	 * Constructor
	 */
	public AbstractCursorManager(FXChartController chartController) {
		this.chartController = chartController;
		this.chart = chartController.getChart();
	}

	public void dispose() {
		disposable.dispose();
	}

	public boolean isEnabled() {
		return enabled.get();
	}

	/**
	 * Set enabled or not this cursor.
	 */
	public void setEnabled(boolean b) {
		this.enabled.set(b);
		if (this.enabled.get()) {
			chart.setOnMouseMoved(mouseEventHandler);
		} else {
			if (chart.getOnMouseMoved() == mouseEventHandler)
				chart.setOnMouseMoved(null);
			hide();
		}
	}

	public boolean addListener(Consumer<IChartData> listener) {
		return listeners.add(listener);
	}

	protected void notifyListeners(Point2D modelPosition) {
		modelPositionPublishProcessor.onNext(modelPosition);
	}

	/**
	 * Called by modelPositionPublishProcessor : notifies listeners with the closest data.
	 */
	private void doNotifyListeners(Point2D modelPosition) {
		var nearestChartData = SeriesUtils.getNearestPoint(chartController.getSeries(), modelPosition.getX());
		listeners.forEach(l -> l.accept(nearestChartData));
	}

	/**
	 * Moves the cursor with a {@link MouseEvent};
	 */
	private void moveTo(MouseEvent mouseEvent) {
		moveTo(chart.getPositionInModel(mouseEvent), new Point2D(mouseEvent.getSceneX(), mouseEvent.getSceneY()), true);
	}

	/**
	 * Moves the cursor to specific position;
	 */
	public void moveTo(Point2D modelPosition) {
		moveTo(modelPosition, chart.getPositionInChart(modelPosition), false);
	}

	/**
	 * @return true if provided position allows to display the cursor
	 */
	protected boolean accept(Point2D modelPosition, Point2D scenePosition) {
		return true; // accepts all position by default
	}

	/**
	 * Draws or hides cursor.
	 */
	private void moveTo(Point2D modelPosition, Point2D scenePosition, boolean notifyListeners) {
		if (enabled.get() && !chartController.isSelecting()) {
			if (accept(modelPosition, scenePosition)) {
				// draw cursor
				Platform.runLater(() -> draw(modelPosition, scenePosition));
				if (notifyListeners)
					notifyListeners(modelPosition);
			} else {
				Platform.runLater(() -> {
					hide();
					// display default tooltip
					String xStringValue = chartController.getXAxisLabelFormatter() != null
							? chartController.getXAxisLabelFormatter().apply(modelPosition.getX())
							: NumberUtils.getStringDouble(modelPosition.getX());
					String yStringValue = chartController.getYAxisLabelFormatter() != null
							? chartController.getYAxisLabelFormatter().apply(modelPosition.getY())
							: NumberUtils.getStringDouble(modelPosition.getY());
					chartController.updateInfoText(String.format("%s : %s ", xStringValue, yStringValue));
				});
			}
		}
	}

	// PRIVATE

	protected abstract void draw(Point2D modelPosition, Point2D scenePosition);

	protected abstract void hide();

}
