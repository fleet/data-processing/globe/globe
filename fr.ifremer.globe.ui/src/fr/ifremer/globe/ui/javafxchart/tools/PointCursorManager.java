package fr.ifremer.globe.ui.javafxchart.tools;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.ui.javafxchart.model.SeriesUtils;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import javafx.geometry.Point2D;
import javafx.scene.shape.Circle;

/**
 * This class handles the display of a cursor on selected point (and updates information label).
 */
public class PointCursorManager extends AbstractCursorManager {

	private final static int DISTANCE_TOLERENCE = 15; // in pixel

	/** Properties **/
	private final Circle circleOnNearestPoint = new Circle(3);
	private IChartData neareastPoint;
	private Point2D nearseastPointPosition;

	/**
	 * Constructor
	 */
	public <X, Y> PointCursorManager(FXChartController chartController) {
		super(chartController);
		circleOnNearestPoint.getStyleClass().add("cursor-circle");
	}

	/**
	 * Accepts only position close enough from a {@link IChartData}.
	 */
	@Override
	protected boolean accept(Point2D modelPosition, Point2D scenePosition) {
		neareastPoint = SeriesUtils.getNearestPoint(chartController.getSeries(), modelPosition);
		if (neareastPoint == null)
			return false;

		// accept position if the nearest data if close enough
		nearseastPointPosition = new Point2D(neareastPoint.getX(), neareastPoint.getY());
		return scenePosition.distance(chart.getPositionInChart(nearseastPointPosition)) < DISTANCE_TOLERENCE;
	}

	@Override
	protected void draw(Point2D modelPosition, Point2D scenePosition) {
		chart.addCustomNode(circleOnNearestPoint, nearseastPointPosition);
		chartController.updateInfoText(neareastPoint.getTooltip());
	}

	@Override
	protected void hide() {
		chart.removeCustomNode(circleOnNearestPoint);
	}

}
