package fr.ifremer.globe.ui.javafxchart.tools;

import java.util.List;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.ui.javafxchart.model.SeriesUtils;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * This class provides a manager for the selection functionality.
 */
public class RectSelectionManager extends ASelectionManager {

	/** Properties **/
	private final SimpleDoubleProperty rectX = new SimpleDoubleProperty();
	private final SimpleDoubleProperty rectY = new SimpleDoubleProperty();
	private final Rectangle selectRect;
	private final SimpleBooleanProperty selecting = new SimpleBooleanProperty(false);

	/**
	 * Constructor
	 */
	public <X, Y> RectSelectionManager(FXChartController chartController) {
		super(chartController);

		// Creates selection rectangle
		selectRect = new Rectangle(0, 0, 0, 0);
		selectRect.setMouseTransparent(true);
		setColor(Color.BLACK);
		StackPane.setAlignment(selectRect, Pos.TOP_LEFT);
		chartController.getChartPane().getChildren().add(selectRect);

		handlerManager.addEventHandler(false, MouseEvent.MOUSE_PRESSED, mouseEvent -> {
			if (passesFilter(mouseEvent))
				onMousePressed(mouseEvent);
		});
		handlerManager.addEventHandler(false, MouseEvent.DRAG_DETECTED, mouseEvent -> {
			if (passesFilter(mouseEvent))
				onDragStart();
		});
		handlerManager.addEventHandler(false, MouseEvent.MOUSE_DRAGGED, this::onMouseDragged);
		handlerManager.addEventHandler(false, MouseEvent.MOUSE_RELEASED, e -> onMouseReleased());
	}

	/**
	 * Sets the polygon color
	 */
	public void setColor(Color selectorColor) {
		selectRect.setStroke(selectorColor);
		selectRect.setFill(new Color(selectorColor.getRed(), selectorColor.getGreen(), selectorColor.getBlue(), 0.2));
	}
	
	/**
	 * @return true if a selection is happening
	 */
	public boolean isSelecting() {
		return selecting.get();
	}

	/**
	 * Start management by adding event handlers and bindings as appropriate.
	 */
	@Override
	public void start() {
		super.start();
		selectRect.widthProperty().bind(rectX.subtract(selectRect.translateXProperty()));
		selectRect.heightProperty().bind(rectY.subtract(selectRect.translateYProperty()));
		selectRect.visibleProperty().bind(selecting);
	}

	/**
	 * Stop management by removing all event handlers and bindings, and hiding the rectangle.
	 */
	@Override
	public void stop() {
		super.stop();
		selecting.set(false);
		selectRect.widthProperty().unbind();
		selectRect.heightProperty().unbind();
		selectRect.visibleProperty().unbind();
	}

	//// PRIVATE METHODS TO MANAGE MOUSE EVENTS

	private void onMousePressed(MouseEvent mouseEvent) {
		double x = mouseEvent.getX();
		double y = mouseEvent.getY();
		selectRect.setTranslateX(x);
		selectRect.setTranslateY(y);
		rectX.set(x);
		rectY.set(y);
	}

	private void onDragStart() {
		selecting.set(true);
	}

	private void onMouseDragged(MouseEvent mouseEvent) {
		if (!selecting.get())
			return;

		Rectangle2D plotArea = chartInfo.getPlotArea();

		double x = mouseEvent.getX();
		x = Math.max(x, selectRect.getTranslateX());
		x = Math.min(x, plotArea.getMaxX());
		rectX.set(x);

		double y = mouseEvent.getY();
		y = Math.max(y, selectRect.getTranslateY());
		y = Math.min(y, plotArea.getMaxY());
		rectY.set(y);
	}

	private void onMouseReleased() {
		if (!selecting.get())
			return;

		if (selectRect.getWidth() == 0.0 || selectRect.getHeight() == 0.0) {
			selecting.set(false);
			return;
		}
		
		Rectangle2D selectionWindow = chartInfo.getDataCoordinates(selectRect.getTranslateX(),
				selectRect.getTranslateY(), rectX.get(), rectY.get());

		List<IChartData> selectedData = SeriesUtils.getDataInShape(chartController.getSeries(), selectionWindow);
		if (!selectedData.isEmpty())
			listeners.forEach(l -> l.onSelection(selectedData));
		
		selecting.set(false);
	}



}
