package fr.ifremer.globe.ui.databinding.observable;

import java.time.Instant;

import org.eclipse.core.databinding.observable.value.IObservableValue;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a String and fire change events when
 * the value changes.
 */
public class WritableInstant extends WritableObject<Instant> {

	public WritableInstant(Instant initialValue) {
		super(initialValue);
	}
}
