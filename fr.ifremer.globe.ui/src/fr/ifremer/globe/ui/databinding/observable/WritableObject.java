package fr.ifremer.globe.ui.databinding.observable;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.DisplayRealm;
import org.eclipse.swt.widgets.Display;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a String and fire change events when
 * the value changes.
 */
public class WritableObject<T> extends WritableValue<T> {

	/**
	 * Constructs a new instance
	 */
	public WritableObject(T initialValue) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue,
				initialValue != null ? initialValue.getClass() : null);
	}

	/**
	 * Constructs a new instance
	 */
	protected WritableObject(T initialValue, Class<T> valueType) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue, valueType);
	}

	public T get() {
		return doGetValue();
	}

	public void set(T value) {
		if (doGetValue() == null && value != null || doGetValue() != null && !doGetValue().equals(value)) {
			doSetValue(value);
		}
	}

	/** @return true when value is null */
	public boolean isNull() {
		return doGetValue() == null;
	}

	/** @return true when value is not null */
	public boolean isNotNull() {
		return doGetValue() != null;
	}

	/**
	 * If the value is null, invoke the specified consumer
	 */
	public void setIfNull(Supplier<T> supplier) {
		if (isNull()) {
			set(supplier.get());
		}
	}

	/**
	 * If the value is not null, invoke the specified consumer
	 */
	public void ifNotNull(Consumer<? super T> consumer) {
		if (isNotNull()) {
			consumer.accept(get());
		}
	}

	/**
	 * @return the value, if not null, otherwise {@code other}
	 */
	public T orElse(T other) {
		return isNotNull() ? get() : other;
	}

	/**
	 * @return the value, if not null, otherwise supply it
	 */
	public T orElse(Supplier<T> supplier) {
		return isNotNull() ? get() : supplier.get();
	}

	/** {@inheritDoc} */
	@Override
	public void doSetValue(T value) {
		if (getRealm().isCurrent()) {
			super.doSetValue(value);
		} else {
			getRealm().exec(() -> super.doSetValue(value));
		}
	}

}
