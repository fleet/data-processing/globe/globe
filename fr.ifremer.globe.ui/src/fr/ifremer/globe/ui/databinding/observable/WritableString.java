package fr.ifremer.globe.ui.databinding.observable;

import org.eclipse.core.databinding.observable.value.IObservableValue;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a String and fire change events when
 * the value changes.
 */
public class WritableString extends WritableObject<String> {

	/**
	 * Constructs a new instance.
	 */
	public WritableString() {
		this("");
	}

	/**
	 * Constructs a new instance
	 */
	public WritableString(String initialValue) {
		super(initialValue, String.class);
	}

	public boolean isEmpty() {
		return isNull() || get().isEmpty();
	}

}
