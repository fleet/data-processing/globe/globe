package fr.ifremer.globe.ui.databinding.observable;

import org.eclipse.core.databinding.observable.value.IObservableValue;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a Number and fire change events when
 * the value changes.
 */
public class WritableNumber extends WritableObject<Number> {

	/**
	 * Constructs a new instance.
	 */
	public WritableNumber() {
		this(null);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableNumber(Number initialValue) {
		super(initialValue, Number.class);
	}

	/**
	 * Returns the value of the specified number as an {@code int}, which may involve rounding or truncation.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code int}.
	 */
	public int intValue() {
		return isNull() ? 0 : get().intValue();
	}

	/**
	 * Returns the value of the specified number as a {@code long}, which may involve rounding or truncation.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code long}.
	 */
	public long longValue() {
		return isNull() ? 0 : get().longValue();
	}

	/**
	 * Returns the value of the specified number as a {@code float}, which may involve rounding.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code float}.
	 */
	public float floatValue() {
		return isNull() ? 0 : get().floatValue();
	}

	/**
	 * Returns the value of the specified number as a {@code double}, which may involve rounding.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code double}.
	 */
	public double doubleValue() {
		return isNull() ? 0 : get().doubleValue();
	}

	/**
	 * Returns the value of the specified number as a {@code byte}, which may involve rounding or truncation.
	 *
	 * <p>
	 * This implementation returns the result of {@link #intValue} cast to a {@code byte}.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code byte}.
	 */
	public byte byteValue() {
		return (byte) intValue();
	}

	/**
	 * Returns the value of the specified number as a {@code short}, which may involve rounding or truncation.
	 *
	 * <p>
	 * This implementation returns the result of {@link #intValue} cast to a {@code short}.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code short}.
	 */
	public short shortValue() {
		return (short) intValue();
	}

}
