/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.observable;

import java.util.function.DoubleConsumer;
import java.util.function.DoubleSupplier;

import org.eclipse.core.databinding.observable.Diffs;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;

/**
 * Wrapper of GeoBox. <br>
 * Notification is raised of any GeoBox value change.
 */
public class WritableGeoBox extends WritableObject<GeoBox> {

	/** Used to generated a ValueDiff when notifying */
	protected GeoBox oldValue;

	/**
	 * Constructor
	 */
	public WritableGeoBox() {
		super(null);
	}

	/**
	 * Constructor
	 */
	public WritableGeoBox(GeoBox initialValue) {
		super(initialValue);
		oldValue = initialValue;
	}

	/** {@inheritDoc} */
	public boolean contains(GeoBox box) {
		return doGetValue().contains(box);
	}

	/** {@inheritDoc} */
	public boolean isEmpty() {
		return doGetValue().isEmpty();
	}

	/** {@inheritDoc} */
	public double getWidth() {
		return doGetValue().getWidth();
	}

	/** {@inheritDoc} */
	public double getHeight() {
		return doGetValue().getHeight();
	}

	/** {@inheritDoc} */
	public double getNorthLatitude() {
		return doGetValue().getTop();
	}

	/** {@inheritDoc} */
	public double getLong() {
		return doGetValue().getCenterX();
	}

	/** {@inheritDoc} */
	public void setLong(double lon) {
		doGetValue().setLong(lon);
		fireValueChange(Diffs.createValueDiff(oldValue, doGetValue()));
		oldValue = doGetValue();
	}

	/** {@inheritDoc} */
	public double getLat() {
		return doGetValue().getCenterY();
	}

	/** {@inheritDoc} */
	public void setLat(double lat) {
		doGetValue().setLat(lat);
		fireValueChange(Diffs.createValueDiff(oldValue, doGetValue()));
		oldValue = doGetValue();
	}

	/** {@inheritDoc} */
	public void setWidth(double width) {
		doGetValue().setWidth(width);
		fireValueChange(Diffs.createValueDiff(oldValue, doGetValue()));
		oldValue = doGetValue();
	}

	/** {@inheritDoc} */
	public void setHeight(double height) {
		doGetValue().setHeight(height);
		fireValueChange(Diffs.createValueDiff(oldValue, doGetValue()));
		oldValue = doGetValue();
	}

	/** {@inheritDoc} */
	public void extend(GeoBox other) {
		if (other != null) {
			doGetValue().extend(other);
			fireValueChange(Diffs.createValueDiff(oldValue, doGetValue()));
			oldValue = doGetValue();
		}
	}

	/** {@inheritDoc} */
	public double getSouthLatitude() {
		return doGetValue().getBottom();
	}

	/** {@inheritDoc} */
	public double getEastLongitude() {
		return doGetValue().getRight();
	}

	/** {@inheritDoc} */
	public double getWestLongitude() {
		return doGetValue().getLeft();
	}

	/** {@inheritDoc} */
	public void setSouthLatitude(double southLatitude) {
		setOneValue(oldValue::getBottom, value -> doGetValue().setBottom(value), southLatitude);
	}

	/** {@inheritDoc} */
	public void setEastLongitude(double eastLongitude) {
		setOneValue(oldValue::getRight, value -> doGetValue().setRight(value), eastLongitude);
	}

	/** {@inheritDoc} */
	public void setWestLongitude(double westLongitude) {
		setOneValue(oldValue::getLeft, value -> doGetValue().setLeft(value), westLongitude);
	}

	/** {@inheritDoc} */
	public void setNorthLatitude(double northLatitude) {
		setOneValue(oldValue::getTop, value -> doGetValue().setTop(value), northLatitude);
	}

	/** Generic setter of one GeoBox value */
	protected void setOneValue(DoubleSupplier oldValueGetter, DoubleConsumer valueSetter, double value) {
		if (oldValue != null && oldValueGetter.getAsDouble() != value) {
			valueSetter.accept(value);
			fireValueChange(Diffs.createValueDiff(oldValue, doGetValue()));
			oldValue = doGetValue();
		}
	}

	/** {@inheritDoc} */
	public GeoPoint getCenter() {
		return doGetValue().getCenter();
	}

}
