package fr.ifremer.globe.ui.databinding.observable;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.DisplayRealm;
import org.eclipse.swt.widgets.Display;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a Byte and fire change events when
 * the value changes.
 */
public class WritableByte extends WritableValue<Byte> {

	/**
	 * Constructs a new instance.
	 */
	public WritableByte() {
		this((byte) 0);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableByte(byte initialValue) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue, Byte.class);
	}

	/**
	 * Returns the value of the specified number as an {@code byte}.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code byte}.
	 */
	public byte value() {
		return doGetValue() == null ? 0 : doGetValue().byteValue();
	}

	/** @return true when value is null */
	public boolean isNull() {
		return doGetValue() == null;
	}

	/** @return true when value is not null */
	public boolean isNotNull() {
		return doGetValue() != null;
	}

	/** {@inheritDoc} */
	@Override
	public void doSetValue(Byte value) {
		if (getRealm().isCurrent()) {
			super.doSetValue(value);
		} else {
			getRealm().exec(() -> super.doSetValue(value));
		}
	}

}
