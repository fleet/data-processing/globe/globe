package fr.ifremer.globe.ui.databinding.observable;

import java.time.Duration;

import org.eclipse.core.databinding.observable.value.IObservableValue;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a Duration and fire change events
 * when the value changes.
 */
public class WritableDuration extends WritableObject<Duration> {
	/**
	 * Constructs a new instance
	 */
	public WritableDuration(Duration initialValue) {
		super(initialValue);
	}
}
