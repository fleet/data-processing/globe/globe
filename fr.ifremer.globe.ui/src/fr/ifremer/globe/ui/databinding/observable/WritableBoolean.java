package fr.ifremer.globe.ui.databinding.observable;

import org.eclipse.core.databinding.observable.value.IObservableValue;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a Boolean and fire change events
 * when the value changes.
 */
public class WritableBoolean extends WritableObject<Boolean> {

	/**
	 * Constructs a new instance.
	 */
	public WritableBoolean() {
		this(false);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableBoolean(Boolean initialValue) {
		super(initialValue, Boolean.class);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableBoolean(boolean initialValue) {
		super(Boolean.valueOf(initialValue), Boolean.class);
	}

	public boolean isTrue() {
		return Boolean.TRUE.equals(doGetValue());
	}

	public boolean isFalse() {
		return Boolean.FALSE.equals(doGetValue());
	}

}
