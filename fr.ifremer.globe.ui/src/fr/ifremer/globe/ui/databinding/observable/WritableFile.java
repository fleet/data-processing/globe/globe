package fr.ifremer.globe.ui.databinding.observable;

import java.io.File;

import org.eclipse.core.databinding.observable.value.IObservableValue;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a File and fire change events when
 * the value changes.
 */
public class WritableFile extends WritableObject<File> {

	/**
	 * Constructs a new instance.
	 */
	public WritableFile() {
		this(null);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableFile(File initialValue) {
		super(initialValue, File.class);
	}

	/**
	 * @see fr.ifremer.globe.ui.databinding.observable.WritableObject#isNotNull()
	 */
	@Override
	public boolean isNotNull() {
		return doGetValue() != null && !get().getAbsolutePath().isEmpty();
	}

	/**
	 * @return true if file is not null and exists
	 */
	public boolean exists() {
		return doGetValue() != null && get().exists();
	}

	/** Returns the name of the file or directory denoted by this WritableFile */
	public String getName() {
		return doGetValue() != null ? get().getName() : null;
	}

	/** Tests whether the file is a directory */
	public boolean isDirectory() {
		return doGetValue() != null && doGetValue().isDirectory();
	}

	/** Tests whether the file is a normal file */
	public boolean isFile() {
		return doGetValue() != null ? get().isFile() : false;
	}

}
