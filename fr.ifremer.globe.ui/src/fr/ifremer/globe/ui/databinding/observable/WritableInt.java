package fr.ifremer.globe.ui.databinding.observable;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.DisplayRealm;
import org.eclipse.swt.widgets.Display;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a Integer and fire change events
 * when the value changes.
 */
public class WritableInt extends WritableValue<Integer> {

	/**
	 * Constructs a new instance.
	 */
	public WritableInt() {
		this(0);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableInt(Integer initialValue) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue, Integer.class);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableInt(int initialValue) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue, Integer.class);
	}

	/**
	 * Returns the value of the specified number as an {@code int}, which may involve rounding or truncation.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code int}.
	 */
	public int get() {
		return isNull() ? 0 : doGetValue().intValue();
	}

	/** Set a new value */
	public void set(int value) {
		setValue(Integer.valueOf(value));
	}

	/** Set the maximum value an {@code int} can have */
	public void setToMax() {
		set(Integer.MAX_VALUE);
	}

	/** Set the minimum value an {@code int} can have */
	public void setToMin() {
		set(Integer.MIN_VALUE);
	}

	/**
	 * Increment the current value (++)
	 * 
	 * @return current value
	 */
	public int increment() {
		set(get() + 1);
		return get();
	}

	/**
	 * Decrement the current value (++)
	 * 
	 * @return current value
	 */
	public int dercrement() {
		set(get() - 1);
		return get();
	}

	/** @return true when value is null */
	public boolean isNull() {
		return doGetValue() == null;
	}

	/** Set the value to null */
	public void setToNull() {
		setValue(null);
	}

	/** @return true when value is not null */
	public boolean isNotNull() {
		return doGetValue() != null;
	}

	/** {@inheritDoc} */
	@Override
	public void doSetValue(Integer value) {
		if (getRealm().isCurrent()) {
			super.doSetValue(value);
		} else {
			getRealm().exec(() -> super.doSetValue(value));
		}
	}

}
