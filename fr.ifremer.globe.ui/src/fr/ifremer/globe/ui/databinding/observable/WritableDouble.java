package fr.ifremer.globe.ui.databinding.observable;

import java.util.function.DoubleConsumer;
import java.util.function.DoubleSupplier;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.DisplayRealm;
import org.eclipse.swt.widgets.Display;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a Double and fire change events when
 * the value changes.
 */
public class WritableDouble extends WritableValue<Double> {

	/**
	 * Constructs a new instance.
	 */
	public WritableDouble() {
		this(0d);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableDouble(double initialValue) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue, Double.class);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableDouble(Double initialValue) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue, Double.class);
	}

	/**
	 * Returns the value of the specified number as an {@code double}, which may involve rounding or truncation.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code double} or Double.NaN if
	 *         null.
	 */
	public double get() {
		return isNull() ? Double.NaN : doGetValue().doubleValue();
	}

	/** Set a new value */
	public void set(double value) {
		setValue(Double.valueOf(value));
	}

	/** Set value to {@link Double#NaN} */
	public void setNaN() {
		set(Double.NaN);
	}

	/** Set value to {@link Double#NEGATIVE_INFINITY} */
	public void setNegativeInfinity() {
		set(Double.NEGATIVE_INFINITY);
	}

	/** Set value to {@link Double#POSITIVE_INFINITY} */
	public void setPositiveInfinity() {
		set(Double.POSITIVE_INFINITY);
	}

	/** @return true when value is null */
	public boolean isNull() {
		return doGetValue() == null;
	}

	/** @return true when value is equals to {@link Double#NaN} */
	public boolean isNaN() {
		return Double.isNaN(doGetValue());
	}

	/** @return true when value is equals to {@link Double#NEGATIVE_INFINITY} or {@link Double#POSITIVE_INFINITY} */
	public boolean isInfinite() {
		return Double.isInfinite(doGetValue());
	}

	/** @return true when value is not null */
	public boolean isNotNull() {
		return doGetValue() != null;
	}

	/** Set the value to null */
	public void setToNull() {
		setValue(null);
	}

	/** @return true when value is not null, not infinite nor NaN */
	public boolean isValid() {
		return isNotNull() && !isInfinite() && !isNaN();
	}

	/**
	 * If the value is null, invoke the specified consumer
	 */
	public void setIfNull(DoubleSupplier supplier) {
		if (isNull()) {
			set(supplier.getAsDouble());
		}
	}

	/**
	 * If the value is null, invoke the specified consumer
	 */
	public void setIfNotValid(DoubleSupplier supplier) {
		if (!isValid()) {
			set(supplier.getAsDouble());
		}
	}

	/**
	 * If the value is not null, invoke the specified consumer
	 */
	public void ifNotNull(DoubleConsumer consumer) {
		if (isNotNull()) {
			consumer.accept(get());
		}
	}

	/**
	 * If the value is not null, invoke the specified consumer
	 */
	public void ifValid(DoubleConsumer consumer) {
		if (isValid()) {
			consumer.accept(get());
		}
	}

	/**
	 * @return the value, if valid, otherwise {@code other}
	 */
	public double orElse(double other) {
		return isValid() ? get() : other;
	}

	/**
	 * @return the value, if valid, otherwise supply it
	 */
	public double orElse(DoubleSupplier supplier) {
		return isValid() ? get() : supplier.getAsDouble();
	}

	/** {@inheritDoc} */
	@Override
	public void doSetValue(Double value) {
		if (getRealm().isCurrent()) {
			super.doSetValue(value);
		} else {
			getRealm().exec(() -> super.doSetValue(value));
		}
	}

}
