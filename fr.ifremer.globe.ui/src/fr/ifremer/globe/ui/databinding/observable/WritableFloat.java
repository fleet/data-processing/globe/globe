package fr.ifremer.globe.ui.databinding.observable;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.DisplayRealm;
import org.eclipse.swt.widgets.Display;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a Float and fire change events when
 * the value changes.
 */
public class WritableFloat extends WritableValue<Float> {

	/**
	 * Constructs a new instance.
	 */
	public WritableFloat() {
		this(0f);
	}

	/**
	 * Constructs a new instance
	 */
	public WritableFloat(float initialValue) {
		super(DisplayRealm.getRealm(Display.getDefault()), initialValue, Float.class);
	}

	/**
	 * Returns the value of the specified number as an {@code float}, which may involve rounding or truncation.
	 *
	 * @return the numeric value represented by this object after conversion to type {@code float} or Float.NaN if null.
	 */
	public float get() {
		return isNull() ? Float.NaN : doGetValue().floatValue();
	}

	/** Set a new value */
	public void set(float value) {
		setValue(Float.valueOf(value));
	}

	/** Set value to {@link Float#NaN} */
	public void setNaN() {
		set(Float.NaN);
	}

	/** Set value to {@link Float#NEGATIVE_INFINITY} */
	public void setNegativeInfinity() {
		set(Float.NEGATIVE_INFINITY);
	}

	/** Set value to {@link Float#POSITIVE_INFINITY} */
	public void setPositiveInfinity() {
		set(Float.POSITIVE_INFINITY);
	}

	/** @return true when value is equals to {@link Float#NaN} */
	public boolean isNaN() {
		return Float.isNaN(doGetValue());
	}

	/** @return true when value is equals to {@link Float#NEGATIVE_INFINITY} or {@link Float#POSITIVE_INFINITY} */
	public boolean isInfinite() {
		return Float.isInfinite(doGetValue());
	}

	/** @return true when value is null */
	public boolean isNull() {
		return doGetValue() == null;
	}

	/** @return true when value is not null */
	public boolean isNotNull() {
		return doGetValue() != null;
	}

	/** Set the value to null */
	public void setToNull() {
		setValue(null);
	}

	/** {@inheritDoc} */
	@Override
	public void doSetValue(Float value) {
		if (getRealm().isCurrent()) {
			super.doSetValue(value);
		} else {
			getRealm().exec(() -> super.doSetValue(value));
		}
	}

}
