package fr.ifremer.globe.ui.databinding.observable;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.DisplayRealm;
import org.eclipse.swt.widgets.Display;

/**
 * Mutable (writable) implementation of {@link IObservableValue} that will maintain a String and fire change events when
 * the value changes.
 */
public class WritableObjectList<T> extends WritableList<T> {

	/**
	 * Constructs a new instance
	 */
	public WritableObjectList() {
		super(DisplayRealm.getRealm(Display.getDefault()));
	}

	/**
	 * Constructs a new instance
	 */
	public WritableObjectList(Collection<T> toWrap) {
		super(DisplayRealm.getRealm(Display.getDefault()), toWrap, null);
	}

	public List<T> asList() {
		return wrappedList;
	}

	public Stream<T> stream() {
		return wrappedList.stream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateWrappedList(List<T> newList) {
		super.updateWrappedList(newList);
	}

	/**
	 * Ugly method to force the notification of listener, even if there is no changes.
	 * 
	 * TODO: find a better solution...
	 */
	public void forceFireChange() {
		List<T> tmpList = List.copyOf(asList());
		clear();
		addAll(tmpList);
	}
}
