/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.validation;

import java.io.File;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;

/**
 * Check a file according some requirements<br>
 * Listen {@link #validationStatus} to be notify of validation changes
 */
public class FileValidator implements IValidator<Object> {

	/** File OK even if it is empty */
	public static final int NULL = 1 << 1;
	/** File OK even if it is a folder */
	public static final int FOLDER = 1 << 2;
	/** File OK even if it is a file */
	public static final int FILE = 1 << 3;
	/** File OK even if it is read only */
	public static final int READ_ONLY = 1 << 4;
	/** File OK if it exists */
	public static final int EXISTS = 1 << 5;

	/** Requirements on expected file */
	protected int requirements;
	/** External predicate to complete the number validation. May be null */
	protected Predicate<File> predicate;
	/** Validation status */
	protected WritableBoolean validationStatus = new WritableBoolean(true);
	/** Validator enabled or not */
	protected boolean enabled = true;

	/**
	 * Constructor
	 */
	public FileValidator() {
		this.requirements = NULL | FILE | FOLDER | READ_ONLY;
	}

	/**
	 * Constructor
	 */
	public FileValidator(int requirements) {
		this.requirements = requirements;
	}

	/**
	 * Constructor
	 */
	public FileValidator(Predicate<File> predicate) {
		this();
		this.predicate = predicate;
	}

	/**
	 * Constructor
	 */
	public FileValidator(int requirements, Predicate<File> predicate) {
		this.requirements = requirements;
		this.predicate = predicate;
	}

	/**
	 * @see org.eclipse.core.databinding.validation.IValidator#validate(java.lang.Object)
	 */
	@Override
	public IStatus validate(Object value) {
		if (!enabled) {
			validationStatus.set(true);
			return ValidationStatus.ok();
		}

		// No file ?
		if (value == null || value.toString().isEmpty()) {
			// Mandatory ?
			if ((requirements & NULL) == 0) {
				validationStatus.set(false);
				return (requirements & FOLDER) != 0 ? ValidationStatus.error("Directory is mandatory")
						: ValidationStatus.error("File is mandatory");
			} else {
				validationStatus.set(true);
				return ValidationStatus.ok();
			}

		}
		File file = null;
		if (value instanceof File)
			file = (File) value;
		if (value instanceof String)
			file = new File((String) value);

		if (file != null) {
			if ((file.getPath() == null || file.getPath().isEmpty()) && (requirements & NULL) == 0) {
				validationStatus.set(false);
				return ValidationStatus.error("File is mandatory");
			}

			if (!file.exists() && (requirements & EXISTS) != 0) {
				validationStatus.set(false);
				return ValidationStatus.error("File does not exist");
			}
			if (file.isDirectory() && (requirements & FOLDER) == 0) {
				validationStatus.set(false);
				return ValidationStatus.error("Directory is not allowed");
			}
			if (file.isFile() && (requirements & FILE) == 0) {
				validationStatus.set(false);
				return ValidationStatus.error("File is not allowed");
			}
			if (predicate != null) {
				String message = predicate.test(file);
				if (message != null) {
					validationStatus.set(false);
					return ValidationStatus.error(message);
				}
			}
		}

		validationStatus.set(true);
		return ValidationStatus.ok();
	}

	/**
	 * Getter of enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Setter of enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Getter of validationStatus
	 */
	public WritableBoolean getValidationStatus() {
		return validationStatus;
	}

}
