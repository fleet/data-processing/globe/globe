/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.validation;

import java.text.ParsePosition;
import java.util.Locale;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

import com.ibm.icu.text.NumberFormat;

/**
 * Implementation of IValidator using a NumberFormat to validate a String
 *
 */
public class NumberFormatValidator implements IValidator<String> {

	/** Formatter used to validate a String */
	private final NumberFormat numberFormat;

	/**
	 * Constructor
	 */
	public NumberFormatValidator() {
		this(NumberFormat.getInstance(Locale.US));
	}

	/**
	 * Constructor
	 */
	public NumberFormatValidator(NumberFormat numberFormat) {
		this.numberFormat = numberFormat;
	}

	@Override
	public IStatus validate(String value) {
		if (value != null && !value.isEmpty()) {
			ParsePosition position = new ParsePosition(0);
			numberFormat.parseObject(value, position);
			if (position.getIndex() != value.length() || position.getErrorIndex() > -1) {
				return ValidationStatus.error("Not a number");
			}
		}
		return ValidationStatus.ok();

	}

}
