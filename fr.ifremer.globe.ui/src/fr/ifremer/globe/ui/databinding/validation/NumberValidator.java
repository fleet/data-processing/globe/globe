/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.validation;

import org.apache.commons.lang.math.Range;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

/**
 * Check a Number according to a Range<br>
 * Listen {@link #validationStatus} to be notify of validation changes
 */
public class NumberValidator implements IValidator {

	/** Range of numbers. May be null */
	protected Range range;
	/** External predicate to complete the number validation. May be null */
	protected Predicate<Number> predicate;
	/** Null allowed or not */
	protected boolean nullAllowed = false;
	/** Validator enabled or not */
	protected boolean enabled = true;

	/**
	 * Constructor
	 */
	public NumberValidator() {
		this(null, null);
	}

	/**
	 * Constructor
	 */
	public NumberValidator(Range range) {
		this(range, null);
	}

	/**
	 * Constructor
	 */
	public NumberValidator(Predicate<Number> predicate) {
		this(null, predicate);
	}

	/**
	 * Constructor
	 */
	public NumberValidator(Range range, Predicate<Number> predicate) {
		this.range = range;
		this.predicate = predicate;
	}

	/**
	 * @see org.eclipse.core.databinding.validation.IValidator#validate(java.lang.Object)
	 */
	@Override
	public IStatus validate(Object value) {
		if (!enabled) {
			return ValidationStatus.ok();
		}
		if (nullAllowed && value == null) {
			return ValidationStatus.ok();
		}
		if (!(value instanceof Number)) {
			return ValidationStatus.error("Not a number");
		}

		Number number = (Number) value;
		if (Double.isNaN(number.doubleValue())) {
			return ValidationStatus.error("Not a number");
		}
		if (range != null && !range.containsNumber(number)) {
			return ValidationStatus.error("Out of range " + range.toString());
		}
		if (predicate != null) {
			String message = predicate.test(number);
			if (message != null && !message.isEmpty()) {
				return ValidationStatus.error(message);
			}
		}

		return ValidationStatus.ok();
	}

	/**
	 * Getter of enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Setter of enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the {@link #nullAllowed}
	 */
	public boolean isNullAllowed() {
		return nullAllowed;
	}

	/**
	 * @param nullAllowed the {@link #nullAllowed} to set
	 */
	public void setNullAllowed(boolean nullAllowed) {
		this.nullAllowed = nullAllowed;
	}
}
