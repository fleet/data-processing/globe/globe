package fr.ifremer.globe.ui.databinding.validation;

/**
 * Represents a predicate of one argument.
 *
 * @param <T> the type of the input to the predicate
 * 
 */
@FunctionalInterface
public interface Predicate<T> {

	/**
	 * Evaluates this predicate on the given argument.
	 *
	 * @param t the input argument
	 * @return {@code null} if the input argument matches the predicate, otherwise a message error
	 */
	String test(T t);
}