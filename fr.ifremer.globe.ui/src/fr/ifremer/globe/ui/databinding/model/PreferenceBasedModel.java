/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.model;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;

/**
 * Base class for a model containing WritableValue synchronized with preferences
 */
public abstract class PreferenceBasedModel {

	/** Sync with preferences */
	private List<PreferenceBridge<?>> bridges = new ArrayList<>();

	/** Sync value of a model attribute with the given preference */
	public <T> void sync(PreferenceAttribute<T> preference, WritableObject<T> modelAttribute) {
		bridges.add(new PreferenceBridge<>(preference, modelAttribute));
	}

	/** Sync value of a model attribute with the given preferences */
	public <T> void sync(List<PreferenceAttribute<T>> preferences, WritableObject<T> modelAttribute) {
		preferences.forEach(preference -> sync(preference, modelAttribute));
	}

	/** Free */
	public void dispose() {
		bridges.forEach(PreferenceBridge::dispose);
		bridges.clear();
	}
}
