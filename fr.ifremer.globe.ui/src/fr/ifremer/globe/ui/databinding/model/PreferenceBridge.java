package fr.ifremer.globe.ui.databinding.model;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;

import fr.ifremer.globe.core.utils.preference.attributes.PreferenceAttribute;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;

/** Sync a Writable with a preference */
public class PreferenceBridge<T> implements Observer, IChangeListener {

	protected PreferenceAttribute<T> preference;
	protected WritableObject<T> attribute;

	/** Constructor */
	public PreferenceBridge(PreferenceAttribute<T> preference, WritableObject<T> attribute) {
		this.preference = preference;
		this.attribute = attribute;

		this.attribute.set(this.preference.getValue());
		this.preference.addObserver(this);
		this.attribute.addChangeListener(this);
	}

	/** Attribute changed */
	@Override
	public void handleChange(ChangeEvent event) {
		if (attribute.get() != preference.getValue()) {
			preference.setValue(attribute.get(), true);
		}
	}

	/** Preference changed */
	@Override
	public void update(Observable o, Object arg) {
		if (attribute.get() != preference.getValue()) {
			attribute.set(preference.getValue());
		}
	}

	/** Free */
	public void dispose() {
		this.preference.deleteObserver(this);
		this.attribute.removeChangeListener(this);
	}
}
