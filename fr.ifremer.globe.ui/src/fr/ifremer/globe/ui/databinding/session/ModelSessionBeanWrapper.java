/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.session;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyDescriptor;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.LazyDynaClass;
import org.apache.commons.beanutils.PropertyUtils;
import org.eclipse.core.databinding.observable.value.WritableValue;

/**
 * Wrapper of model instance (using WritableValue as attributes). <br>
 * This wrapper allows to save such a model in the session
 */
@SuppressWarnings("unchecked")
public class ModelSessionBeanWrapper extends BasicDynaBean {

	/** No serialization */
	private static final long serialVersionUID = 1L;

	/** All wrapped attributes */
	protected Map<String, WritableValue<Object>> attributes = new HashMap<>();
	/** All wrapped attributes */
	protected PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	/**
	 * Constructor
	 */
	public ModelSessionBeanWrapper(Object modelBean) {
		super(new LazyDynaClass());
		LazyDynaClass lazyDynaClass = (LazyDynaClass) getDynaClass();

		try {
			PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(modelBean);
			for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
				Class<?> type = propertyDescriptor.getPropertyType();
				if (WritableValue.class.isAssignableFrom(type)) {
					WritableValue<Object> writableValue = (WritableValue<Object>) propertyDescriptor.getReadMethod()
							.invoke(modelBean);
					attributes.put(propertyDescriptor.getName(), writableValue);
					lazyDynaClass.add(propertyDescriptor.getName(), (Class<?>) writableValue.getValueType());
					writableValue.addChangeListener(event -> propertyChanged(propertyDescriptor.getName()));
				}
			}
		} catch (Exception e) {
			// For debugging purpose
			throw new UnsupportedOperationException("Can't create a " + ModelSessionBeanWrapper.class.getCanonicalName()
					+ " for " + modelBean.getClass().getName(), e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void set(String name, Object value) {
		WritableValue<Object> writableValue = attributes.get(name);
		if (writableValue != null) {
			Object oldValue = writableValue.doGetValue();
			writableValue.doSetValue(value);
			propertyChangeSupport.firePropertyChange(name, oldValue, value);
		} else {
			// For debugging purpose
			throw new UnsupportedOperationException("Unexpected property " + name);
		}
	}

	/** {@inheritDoc} */
	@Override
	public Object get(String name) {
		WritableValue<?> writableValue = attributes.get(name);
		if (writableValue != null) {
			return writableValue.doGetValue();
		}
		// For debugging purpose
		throw new UnsupportedOperationException("Unexpected property " + name);
	}

	/** Fire a PropertyChange event when a value changed on a WritableValue */
	protected void propertyChanged(String name) {
		WritableValue<Object> writableValue = attributes.get(name);
		if (writableValue != null) {
			propertyChangeSupport.firePropertyChange(name, null, writableValue.doGetValue());
		} else {
			// For debugging purpose
			throw new UnsupportedOperationException("Unexpected property " + name);
		}
	}

	/**
	 * @return the {@link #propertyChangeSupport}
	 */
	public PropertyChangeSupport getPropertyChangeSupport() {
		return propertyChangeSupport;
	}

}