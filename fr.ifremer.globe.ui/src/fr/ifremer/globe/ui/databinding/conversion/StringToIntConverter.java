/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import org.apache.commons.lang.math.NumberUtils;
import org.eclipse.core.databinding.conversion.Converter;

/**
 * Converts a String to a Double.
 */
public class StringToIntConverter extends Converter {

	/**
	 * Constructor
	 */
	public StringToIntConverter() {
		super(String.class, Integer.class);
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object string) {
		return string != null ? NumberUtils.toInt(string.toString(), 0) : 0;
	}

}
