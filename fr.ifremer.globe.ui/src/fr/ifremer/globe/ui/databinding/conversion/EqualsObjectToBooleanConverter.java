package fr.ifremer.globe.ui.databinding.conversion;

/**
 * Converts an object value into Boolean.TRUE when equals.
 */
public class EqualsObjectToBooleanConverter<F> extends EqualsObjectToValueConverter<F, Boolean> {

	/**
	 * Constructor. <br>
	 * Converts expectedValue into Boolean.TRUE when equals
	 */
	public EqualsObjectToBooleanConverter(F expectedValue) {
		this(expectedValue, true);
	}

	/**
	 * Constructor<br>
	 * Converts expectedValue into specified Boolean when equals
	 */
	public EqualsObjectToBooleanConverter(F expectedValue, boolean equalsValue) {
		super(expectedValue, Boolean.valueOf(equalsValue), Boolean.valueOf(!equalsValue));
	}

}
