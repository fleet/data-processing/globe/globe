/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.databinding.conversion.Converter;

/**
 * Test if an object has one of the specified values. <br>
 * if true, returns containsValue, notContainsValue otherwise
 */
public class ContainsObjectToValueConverter extends Converter {

	protected List<?> expectedValues;
	protected Object containsValue;
	protected Object notContainsValue;

	/**
	 * Constructor. <br>
	 * Test if an object has one of the specified values. <br>
	 * if true, returns Boolean.TRUE, Boolean.FALSE otherwise
	 */
	public ContainsObjectToValueConverter(Object... expectedValue) {
		this(expectedValue, true);
	}

	/**
	 * Constructor<br>
	 * Test if an object has one of the specified values. <br>
	 * if true, returns {@code #containsValue}, !{@code #containsValue} otherwise
	 */
	public ContainsObjectToValueConverter(Object[] expectedValue, boolean containsValue) {
		this(expectedValue, Boolean.valueOf(containsValue), Boolean.valueOf(!containsValue));
	}

	/**
	 * Constructor<br>
	 * Test if an object has one of the specified values. <br>
	 * if true, returns {@code #containsValue}, {@code #notContainsValue} otherwise
	 */
	public ContainsObjectToValueConverter(Object[] expectedValue, Object containsValue, Object notContainsValue) {
		this(Arrays.asList(expectedValue), containsValue, notContainsValue);
	}

	/**
	 * Constructor<br>
	 * Test if an object has one of the specified values. <br>
	 * if true, returns {@code #containsValue}, {@code #notContainsValue} otherwise
	 */
	public ContainsObjectToValueConverter(List<?> expectedValue, Object containsValue, Object notContainsValue) {
		super(expectedValue.get(0).getClass(),
				containsValue != null ? containsValue.getClass() : notContainsValue.getClass());
		this.expectedValues = expectedValue;
		this.containsValue = containsValue;
		this.notContainsValue = notContainsValue;
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object value) {
		return expectedValues.contains(value) ? containsValue : notContainsValue;
	}

}
