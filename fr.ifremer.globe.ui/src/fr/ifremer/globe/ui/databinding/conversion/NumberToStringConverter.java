/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import java.text.NumberFormat;

import org.eclipse.core.databinding.conversion.Converter;

/**
 * Converts a Number to a String.
 */
public class NumberToStringConverter extends Converter {

	/** Text format to apply to the number. May be null. */
	protected NumberFormat numberFormat;

	/**
	 * Constructor
	 */
	public NumberToStringConverter() {
		super(Number.class, String.class);
	}

	/**
	 * Constructor
	 */
	public NumberToStringConverter(NumberFormat numberFormat) {
		this();
		this.numberFormat = numberFormat;
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object number) {
		Object result = "";
		if (number instanceof Double) {
			double value = ((Number) number).doubleValue();
			if (!Double.isNaN(value)) {
				result = numberFormat != null ? numberFormat.format(value) : String.valueOf(value);
			}
		} else if (number instanceof Float) {
			float value = ((Number) number).floatValue();
			if (!Float.isNaN(value)) {
				result = numberFormat != null ? numberFormat.format(value) : String.valueOf(value);
			}
		} else {
			long value = ((Number) number).longValue();
			result = numberFormat != null ? numberFormat.format(value) : String.valueOf(value);
		}
		return result;
	}

}
