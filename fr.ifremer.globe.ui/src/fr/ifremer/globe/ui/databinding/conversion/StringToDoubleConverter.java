/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import org.eclipse.core.databinding.conversion.Converter;

/**
 * Converts a String to a Double.
 */
public class StringToDoubleConverter extends Converter<String, Double> {

	protected Double defaultValue = Double.NaN;

	/**
	 * Constructor
	 */
	public StringToDoubleConverter() {
		super(String.class, Double.class);
	}

	/**
	 * Constructor
	 */
	public StringToDoubleConverter(Double defaultValue) {
		this();
		this.defaultValue = defaultValue;
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Double convert(String value) {
		try {
			return value != null ? Double.valueOf(value) : defaultValue;
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

}
