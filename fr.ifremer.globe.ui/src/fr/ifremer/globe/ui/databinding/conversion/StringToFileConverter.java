/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import java.io.File;

import org.eclipse.core.databinding.conversion.Converter;

/**
 * Converts a String into a File.
 */
public class StringToFileConverter extends Converter<String, File> {

	/**
	 * Constructor
	 */
	public StringToFileConverter() {
		super(String.class, File.class);
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public File convert(String string) {
		return string != null ? new File(string) : null;
	}

}
