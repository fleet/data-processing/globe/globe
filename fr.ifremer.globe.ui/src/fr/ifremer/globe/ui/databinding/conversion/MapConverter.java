/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.eclipse.core.databinding.conversion.Converter;

/**
 * Test if an object has one of the specified values. <br>
 * if true, returns containsValue, notContainsValue otherwise
 */
public class MapConverter<K, V> extends Converter {

	protected Map<K, V> values;

	/**
	 * Constructor. <br>
	 * Test if an object has one of the specified values. <br>
	 * if true, returns Boolean.TRUE, Boolean.FALSE otherwise
	 */
	public MapConverter(K key, V value) {
		super(key.getClass(), value.getClass());
		values = new HashedMap<>();
		values.put(key, value);
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object key) {
		return values.get(key);
	}

	/**
	 * @return
	 * @see java.util.Map#size()
	 */
	public int size() {
		return values.size();
	}

	/**
	 * @param key
	 * @return
	 * @see java.util.Map#get(java.lang.Object)
	 */
	public V get(Object key) {
		return values.get(key);
	}

	/**
	 * @param key
	 * @param value
	 * @return
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	public V put(K key, V value) {
		return values.put(key, value);
	}

	/**
	 * @param key
	 * @return
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	public V remove(Object key) {
		return values.remove(key);
	}

	/**
	 * 
	 * @see java.util.Map#clear()
	 */
	public void clear() {
		values.clear();
	}

	/**
	 * @return
	 * @see java.util.Map#values()
	 */
	public Collection<V> values() {
		return values.values();
	}

}
