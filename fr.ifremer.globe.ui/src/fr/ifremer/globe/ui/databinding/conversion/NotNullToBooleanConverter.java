/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import org.eclipse.core.databinding.conversion.Converter;

/**
 * Converts any object to a Boolean (true when not null).
 */
public class NotNullToBooleanConverter extends Converter {

	/**
	 * Constructor
	 */
	public NotNullToBooleanConverter() {
		super(Object.class, Boolean.class);
	}

	/**
	 * @see org.eclipse.core.databinding.UpdateValueStrategy#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object value) {
		return value != null ? Boolean.TRUE : Boolean.FALSE;
	}
}
