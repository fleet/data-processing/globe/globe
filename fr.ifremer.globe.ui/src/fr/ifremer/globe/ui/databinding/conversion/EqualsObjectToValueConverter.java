package fr.ifremer.globe.ui.databinding.conversion;

import org.eclipse.core.databinding.conversion.Converter;

/**
 * Converts an object value into a specified value. (Boolean.TRUE when equals by default).
 */
public class EqualsObjectToValueConverter<F, T> extends Converter<F, T> {

	protected F expectedValue;
	protected T equalsValue;
	protected T notEqualsValue;

	/**
	 * Constructor<br>
	 * Converts expectedValue into specified value when equals
	 */
	public EqualsObjectToValueConverter(F expectedValue, T equalsValue, T notEqualsValue) {
		super(expectedValue.getClass(), equalsValue != null ? equalsValue.getClass() : notEqualsValue.getClass());
		this.expectedValue = expectedValue;
		this.equalsValue = equalsValue;
		this.notEqualsValue = notEqualsValue;
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public T convert(F value) {
		return expectedValue.equals(value) ? equalsValue : notEqualsValue;
	}

}
