/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.databinding.conversion;

import java.io.File;

import org.eclipse.core.databinding.conversion.Converter;

/**
 * Converts a File into a String (AbsolutePath).
 */
public class FileToStringConverter extends Converter<File, String> {

	/**
	 * Constructor
	 */
	public FileToStringConverter() {
		super(File.class, String.class);
	}

	/**
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public String convert(File file) {
		return file != null ? file.getAbsolutePath() : null;
	}

}
