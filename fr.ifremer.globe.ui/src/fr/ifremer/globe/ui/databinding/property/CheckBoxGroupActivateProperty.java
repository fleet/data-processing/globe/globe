package fr.ifremer.globe.ui.databinding.property;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.value.ValueDiff;
import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.SimplePropertyEvent;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;
import org.eclipse.nebula.widgets.opal.checkboxgroup.CheckBoxGroup;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

/**
 * Value property implementation for {@link CheckBoxGroup} activate property
 * 
 */
public class CheckBoxGroupActivateProperty extends SimpleValueProperty<CheckBoxGroup, Boolean> {

	/**
	 * @see org.eclipse.jface.databinding.swt.WidgetValueProperty#getValueType()
	 */
	@Override
	public Object getValueType() {
		return Boolean.TYPE;
	}

	/**
	 * @see org.eclipse.core.databinding.property.value.SimpleValueProperty#doGetValue(java.lang.Object)
	 */
	@Override
	protected Boolean doGetValue(CheckBoxGroup source) {
		return source.isActivated();
	}

	/**
	 * @see org.eclipse.core.databinding.property.value.SimpleValueProperty#doSetValue(java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	protected void doSetValue(CheckBoxGroup source, Boolean value) {
		if (Boolean.TRUE.equals(value)) {
			source.activate();
		} else {
			source.deactivate();

		}
	}

	/** {@inheritDoc} */
	@Override
	public INativePropertyListener<CheckBoxGroup> adaptListener(
			ISimplePropertyListener<CheckBoxGroup, ValueDiff<? extends Boolean>> propertyListener) {
		return new NativePropertyListener<CheckBoxGroup, ValueDiff<? extends Boolean>>(this, propertyListener) {

			SelectionListener selectionListener;

			@Override
			protected void doRemoveFrom(CheckBoxGroup source) {
				source.removeSelectionListener(selectionListener);
			}

			@Override
			protected void doAddTo(CheckBoxGroup source) {
				selectionListener = new SelectionAdapter() {
					/**
					 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
					 */
					@Override
					public void widgetSelected(SelectionEvent e) {
						propertyListener
								.handleEvent(new SimplePropertyEvent<CheckBoxGroup, ValueDiff<? extends Boolean>>(
										SimplePropertyEvent.CHANGE, source, CheckBoxGroupActivateProperty.this,
										Diffs.createValueDiff(Boolean.FALSE, Boolean.TRUE)));
					}
				};
				source.addSelectionListener(selectionListener);
			}
		};
	}
}