package fr.ifremer.globe.ui.databinding.property;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.value.ValueDiff;
import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.SimplePropertyEvent;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;
import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.widgets.Text;

/**
 * Value property implementation for {@link FormattedText} activate property
 * 
 * @param <T> type of the value of the property
 */
public class FormattedTextProperty<T extends Number> extends SimpleValueProperty<Text, T> {

	protected Class<T> valueType;

	/** Constructor */
	public FormattedTextProperty(Class<T> valueType) {
		this.valueType = valueType;
	}

	/**
	 * @see org.eclipse.jface.databinding.swt.WidgetValueProperty#getValueType()
	 */
	@Override
	public Object getValueType() {
		return valueType;
	}

	/**
	 * @see org.eclipse.core.databinding.property.value.SimpleValueProperty#doGetValue(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T doGetValue(Text source) {
		return !source.isDisposed() ? (T) getFormattedText(source).getValue() : null;
	}

	protected FormattedText getFormattedText(Text source) {
		return (FormattedText) source.getData(FormattedText.TEXT_DATA_KEY);
	}

	/**
	 * @see org.eclipse.core.databinding.property.value.SimpleValueProperty#doSetValue(java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	protected void doSetValue(Text source, T value) {
		if (!source.isDisposed()) {
			getFormattedText(source).setValue(value);
		}
	}

	/** {@inheritDoc} */
	@Override
	public INativePropertyListener<Text> adaptListener(
			ISimplePropertyListener<Text, ValueDiff<? extends T>> propertyListener) {
		return new NativePropertyListener<Text, ValueDiff<? extends T>>(this, propertyListener) {

			FocusListener focusListener;

			@Override
			protected void doRemoveFrom(Text source) {
				source.removeFocusListener(focusListener);
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void doAddTo(Text source) {
				focusListener = FocusListener.focusLostAdapter(event -> {
					propertyListener.handleEvent(new SimplePropertyEvent<Text, ValueDiff<? extends T>>(
							SimplePropertyEvent.CHANGE, source, FormattedTextProperty.this,
							Diffs.createValueDiff(null, (T) getFormattedText(source).getValue())));
				});
				source.addFocusListener(focusListener);
			}
		};
	}
}