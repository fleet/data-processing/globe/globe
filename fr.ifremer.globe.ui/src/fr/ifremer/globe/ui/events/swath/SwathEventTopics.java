/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.events.swath;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * All topics managed in the navigation shift editor throw the IEventBroker
 */
public class SwathEventTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/globe/model/swath";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to inform that the dirty flag has have changed<br>
	 * Event poster place on the event an instance of Boolean
	 */
	public static final String IS_DIRTY = TOPIC_BASE + UIEvents.TOPIC_SEP + "EDITOR_IS_DIRTY";

	/**
	 * Topic to required the DTM refreshing<br>
	 * Event poster place on the event an instance of itself
	 */
	public static final String REFRESH_DTM = TOPIC_BASE + UIEvents.TOPIC_SEP + "REFRESH_DTM";

	/**
	 * Topic to required the shifting of the selection in the DTM<br>
	 * Event poster place on the event an instance of ShiftingDirection
	 */
	public static final String SHIFT_DTM_SELECTION = TOPIC_BASE + UIEvents.TOPIC_SEP + "SHIFT_DTM_SELECTION";

	/**
	 * Constructor
	 */
	private SwathEventTopics() {
		// private constructor to hide the implicit public one.
	}
}
