/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.events.swath;

public enum ShiftingDirection {
	NORTH, WEST, EAST, SOUTH;
}
