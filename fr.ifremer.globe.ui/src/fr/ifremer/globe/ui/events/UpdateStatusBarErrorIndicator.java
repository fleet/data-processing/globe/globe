/**
 * 
 */
package fr.ifremer.globe.ui.events;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * Event send to update the status bar error indicator
 * shall be reworked in order to check for multithread access
 */
public class UpdateStatusBarErrorIndicator implements BaseEvent {
	
	public static final String TOPIC_BASE = "fr/ifremer/globe/ui/events";
	public static final String EVENT = TOPIC_BASE + UIEvents.TOPIC_SEP + "selection";
	
	private boolean error;

	public UpdateStatusBarErrorIndicator(boolean showError)
	{
		this.error=showError;
	}

	/**
	 * @return the error
	 */
	public boolean isError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(boolean error) {
		this.error = error;
	}
}
