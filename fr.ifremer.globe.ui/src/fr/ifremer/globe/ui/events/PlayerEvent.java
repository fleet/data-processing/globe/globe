package fr.ifremer.globe.ui.events;

/**
 * Event raised when user request to perform a player action.
 */
public class PlayerEvent implements BaseEvent {

	public enum EPlayerAction {
		PLAY_PAUSE, REVERT_PLAY_PAUSE, STEP_BACKWARD, STEP_FORWARD
	}

	public PlayerEvent(EPlayerAction action) {
		this.action = action;
	}

	public EPlayerAction getAction() {
		return action;
	}

	private EPlayerAction action;

}
