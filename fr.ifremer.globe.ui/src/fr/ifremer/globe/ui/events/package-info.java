/**
 * This package gathers POJO classes used to publish events containing 
 * references to more than one class. 
 * <p>
 * Eg. when using a property change listener the old and new values could be 
 * specific events classes in order to collect all informations relevant to 
 * this event.
 * </p>   
 */
package fr.ifremer.globe.ui.events;

