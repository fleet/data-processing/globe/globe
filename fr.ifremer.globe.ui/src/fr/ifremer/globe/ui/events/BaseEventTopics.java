/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.events;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * All topics managed in WW layer processing
 */
public class BaseEventTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/globe/ui";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to alert of an undostack modification
	 */
	public static final String UNDO_STACK = TOPIC_BASE + UIEvents.TOPIC_SEP + "UNDO_STACK";

	/**
	 * Constructor
	 */
	private BaseEventTopics() {
		// private constructor to hide the implicit public one.
	}

}
