/**
 * 
 */
package fr.ifremer.globe.ui.workspace;

import java.io.File;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Globe's specific access for workspace informations.
 */
public class WorkspaceDirectory {

	private static Logger logger = LoggerFactory.getLogger(WorkspaceDirectory.class);

	private static WorkspaceDirectory instance = null;

	/** @see #getRoot() */
	private File root;
	/** Root's absolute path. */
	private String path;

	/** Returns this singleton instance. */
	public static WorkspaceDirectory getInstance() {
		if (instance == null) {
			instance = new WorkspaceDirectory();
		}
		return instance;
	}

	/** @see #getInstance() */
	public WorkspaceDirectory() {
		String path = null;
		try {
			IWorkspace ws = ResourcesPlugin.getWorkspace();
			path = ws.getRoot().getLocation().toOSString();
		} catch (IllegalStateException e) {
			logger.error("Workspace cannot be found," + " using directory \"ws\" instead", e);
			path = "ws";
		}
		root = new File(path);
		if (!root.exists()) {
			root.mkdirs();
		}

	}

	/**
	 * Returns this workspace file tree root. May be either a directory or a
	 * file depending on current behavior (see {@link WorkspaceDirectory}).
	 */
	public File getRoot() {
		return root;
	}

	/** Returns this workspace file tree root absolute path. */
	public String getPath() {
		if (path == null) {
			path = root.getAbsolutePath();
		}
		return path;
	}

}
