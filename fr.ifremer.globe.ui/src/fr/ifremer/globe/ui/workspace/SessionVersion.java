package fr.ifremer.globe.ui.workspace;

public class SessionVersion {
	int version;

	SessionVersion() {
		this.version = 2;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
