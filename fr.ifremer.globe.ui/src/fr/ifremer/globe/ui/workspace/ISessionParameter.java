package fr.ifremer.globe.ui.workspace;

/**
 * Interface for data which can be saved/restored into/from the user's XML
 * session file.
 * <p>
 * <strong>IMPORTANT :</strong> when the user's session is deserialized by
 * XStream, constructor of ISessionParameter implementors is NOT CALLED.
 * Instead, XStream calls a private readResolve() method.
 * <p>
 * So it is *not recommended* that your business model implements directly
 * ISessionParameter. Instead, consider using a basic POJO/JavaBean object
 * and load/save your business parameters from this singleton. Thus, you
 * will only have to deal with XStream subtilities in your ISessionParameter 
 * POJO instead of having to implement complex loading functions in your 
 * business model.
 */
public interface ISessionParameter {

	/**
	 * Called when session was read and all tree parameters updated
	 * */
	public void postSessionRead();

	/**
	 * Called before the current session is written on disk.
	 */
	public void preSessionWrite();

}
