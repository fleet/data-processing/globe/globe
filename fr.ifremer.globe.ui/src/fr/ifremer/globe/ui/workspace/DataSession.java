package fr.ifremer.globe.ui.workspace;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.LinkedList;

import javax.xml.bind.annotation.XmlRootElement;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.core.util.CompositeClassLoader;

import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode.CheckState;

/**
 * Session Object containing all data associated to a user session file will be serialized as xml
 *
 */
@XmlRootElement
public class DataSession {

	/** Format names of configuration file */
	@XStreamOmitField
	public final static String[] CONFIGURATION_FORMAT_NAMES = new String[] { "Extensible Markup Language", "XML" };
	@XStreamOmitField
	/** Format extension of configuration file */
	public final static String[] CONFIGURATION_FORMAT_EXTENSIONS = new String[] { "*.xml" };

	@XStreamOmitField
	public final static String DEFAULT_SESSION_NAME = "defaultsession.xml";

	private SessionVersion version;
	private GroupNode dataNode = new GroupNode("Data", Icons.DATABASE.toImage());

	// @XStreamOmitField
	// private BackGroundGroupNode Node;
	@XStreamOmitField
	private GroupNode wmsNode = new GroupNode("WMS", Icons.WMS.toImage());

	@XStreamOmitField
	private static LinkedList<Class<?>> knownObjects = new LinkedList<>();

	/**
	 * list of session object a parameters to be serialized/deserialized
	 */
	private HashMap<String, ISessionParameter> parametersmap;

	public DataSession() {
		version = new SessionVersion();
		// globeNode = new BackGroundGroupNode();

		parametersmap = new HashMap<>();
		//
		DataSession.declareObject(SessionVersion.class);
		DataSession.declareObject(GroupNode.class);
		// DataSession.declareObject(BackGroundGroupNode.class);
		DataSession.declareObject(FolderNode.class);
		DataSession.declareObject(TreeNode.class);
		DataSession.declareObject(CheckState.class);

	}

	public GroupNode getDataNode() {
		return dataNode;
	}

	/// public BackGroundGroupNode getGlobeNode() {
	// return globeNode;
	// }

	public GroupNode getWmsNode() {
		return wmsNode;
	}

	public void save(boolean relativePath, File destination) throws FileNotFoundException, IOException {

		XStream xstream = initOutputStream(relativePath, destination);
		xstream.autodetectAnnotations(true);

		ObjectOutputStream out = xstream.createObjectOutputStream(new FileWriter(destination), "Session");
		out.writeObject(getVersion());
		out.writeObject(getDataNode());
		out.writeObject(parametersmap);
		out.close();
	}

	@SuppressWarnings("unchecked")
	public void load(File sessionFile) throws IOException, ClassNotFoundException, XStreamException {
		XStream xstream = initInputStream(sessionFile);
		xstream.ignoreUnknownElements();
		// DataSession obj=JAXB.unmarshal(new File(filename),
		// DataSession.class);
		// return obj;
		ObjectInputStream in = xstream.createObjectInputStream(new FileReader(sessionFile));

		version = (SessionVersion) in.readObject();
		dataNode = (GroupNode) in.readObject();
		if (version.getVersion() >= 2) {
			parametersmap = (HashMap<String, ISessionParameter>) in.readObject();
		}
	}

	public static void declareObject(Class<?> cls) {
		knownObjects.add(cls);
	}

	public static CompositeClassLoader getClassLoader() {
		CompositeClassLoader classLoader = new CompositeClassLoader();

		for (Class<?> t : knownObjects) {
			ClassLoader cl = t.getClassLoader();
			if (cl != null) {
				classLoader.add(cl);
			}
		}
		// for (IDataDriver driver : DataDriverRegistry.getInstance().getDrivers()) {
		// ClassLoader cl = driver.getClass().getClassLoader();
		// if (cl != null) {
		// classLoader.add(cl);
		// }
		// }
		return classLoader;
	}

	private XStream initInputStream(File sessionFile) {
		XStream xstream = new XStream();
		xstream.registerConverter(new FilePathConverter(false, sessionFile.getParentFile()));

		xstream.setClassLoader(getClassLoader());
		return xstream;

	}

	private XStream initOutputStream(boolean relativePath, File sessionFile) {
		XStream xstream = new XStream();
		xstream.registerConverter(new FilePathConverter(relativePath, sessionFile.getParentFile()));
		xstream.autodetectAnnotations(true);
		return xstream;
	}

	public SessionVersion getVersion() {
		return version;
	}

	/**
	 * @return the parametersmap
	 */
	public HashMap<String, ISessionParameter> getParametersmap() {
		return parametersmap;
	}

	/**
	 * @param parametersmap the parametersmap to set
	 */
	public void setParametersmap(HashMap<String, ISessionParameter> parametersmap) {
		this.parametersmap = parametersmap;
	}

}
