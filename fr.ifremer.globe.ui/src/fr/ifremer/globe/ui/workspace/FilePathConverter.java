/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.workspace;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;

import com.thoughtworks.xstream.converters.SingleValueConverter;

/**
 * Converter of file path (relative or absolute)
 */
public class FilePathConverter implements SingleValueConverter {

	/** Indicates if path must be written in a relative or absolute way */
	protected boolean relativePath;
	/** Folder of the session file */
	protected File sessionFolder;
	
	/**
	 * Constructor.
	 */
	public FilePathConverter(boolean relativePath, File sessionFolder) {
		this.relativePath = relativePath;
		this.sessionFolder = sessionFolder;
	}

	/**
	 * Follow the link.
	 * 
	 * @see com.thoughtworks.xstream.converters.ConverterMatcher#canConvert(java.lang.Class)
	 */
	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class paramClass) {
		return paramClass.equals(File.class);
	}


	/**
	 * Follow the link.
	 * 
	 * @see com.thoughtworks.xstream.converters.SingleValueConverter#toString(java.lang.Object)
	 */
	@Override
	public String toString(Object paramObject) {
		File file = (File) paramObject;
		String result = file.getAbsolutePath();
		if (relativePath) {
			Path filePath = file.toPath();
			Path sessionFolderPath = sessionFolder.toPath();
			if (filePath.getRoot() != null && sessionFolderPath.getRoot() != null && filePath.getRoot().equals(sessionFolderPath.getRoot())) {
				result = sessionFolderPath.relativize(filePath).toString();
			}
		}
		return result;
	}

	/**
	 * Follow the link.
	 * 
	 * @see com.thoughtworks.xstream.converters.SingleValueConverter#fromString(java.lang.String)
	 */
	@Override
	public Object fromString(String paramString) {
		Path filePath = Paths.get(FilenameUtils.separatorsToSystem(paramString));
		if (!filePath.isAbsolute()) {
			filePath = sessionFolder.toPath().resolve(filePath);
		}
		return filePath.normalize().toFile();
	}

}
