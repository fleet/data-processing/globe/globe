package fr.ifremer.globe.ui;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;

/**
 * Validate a longitude (in degrees)
 * 
 * @author ctphanle
 * 
 */
@Creatable
public class LongitudeValidator implements IValidator {

	public static final IValidator INSTANCE = new LongitudeValidator();

	@Override
	public IStatus validate(Object value) {
		if (value instanceof String) {
			ILatLongFormatter formatter = LatLongFormater.getFormatter();
			String lonString = (String) value;
			Double longitude = formatter.parseLongitude(lonString);
			if (longitude != null) {
				if (Double.compare(longitude, 180.0) > 0 || Double.compare(longitude, -180.0) < 0) {
					return ValidationStatus.error("Longitude must be between 0 and 180");
				}
			} else {
				return ValidationStatus.error("Invalid longitude");
			}
			return ValidationStatus.info(Double.toString(longitude));
		}
		return ValidationStatus.error("Invalid longitude");
	}

}