package fr.ifremer.globe.ui.service.process.impl;

import java.util.Optional;

import jakarta.annotation.PreDestroy;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;

import fr.ifremer.globe.core.runtime.job.GProcess;

/** Handler to manage a cancel request of a running process */
public class CancelProcessHandler {

	/** Listener to be notified when job finishes */
	private Optional<JobChangeAdapter> processListener = Optional.empty();

	/** Evaluate if cancel widget may be enabled */
	@CanExecute
	public boolean evaluate(MPart part, MItem toolItem) {
		if (part.getObject() instanceof ProcessConsole processConsole) {
			GProcess process = processConsole.getProcess();
			if (process != null) {
				if (processListener.isEmpty()) {
					// Attach a listener to a job to be notified when it is completed
					JobChangeAdapter doneListener = new JobChangeAdapter() {
						@Override
						public void done(IJobChangeEvent event) {
							toolItem.setEnabled(false);
							process.removeJobChangeListener(this);
						}
					};
					process.addJobChangeListener(doneListener);
					processListener = Optional.of(doneListener);
				}

				// Can cancel job if not yet finished
				return process.getState() != Job.NONE;
			}
		}
		return false;
	}

	/** Invoke cancel on process */
	@Execute
	public void execute(MPart part, MItem toolItem) {
		if (part.getObject() instanceof ProcessConsole processConsole) {
			GProcess process = processConsole.getProcess();
			if (process != null) {
				process.cancel();
				toolItem.setEnabled(false);
			}
		}
	}

	/** Widget disposed, ProcessConsole was closed */
	@PreDestroy
	public void dispose(MPart part) {
		processListener.ifPresent(doneListener -> {
			if (part.getObject() instanceof ProcessConsole processConsole) {
				GProcess process = processConsole.getProcess();
				if (process != null)
					process.removeJobChangeListener(doneListener);
			}
		});
		processListener = Optional.empty();
	}
}
