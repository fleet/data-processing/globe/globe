package fr.ifremer.globe.ui.service.process.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.application.prefs.Preferences;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GException;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

/**
 * Implementation of {@link IProcessService}.
 */
@Component(name = "globe_ui_service_process_service", service = { IProcessService.class })
public class ProcessService implements IProcessService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessService.class);

	/** Useful Eclipse components, retrieved after inject in {@link ContextInitializer} init method **/
	@Inject
	EModelService modelService;
	@Inject
	MApplication application;
	@Inject
	private Preferences preferences;

	/** Id of the partStack where to open a console view */
	private StringPreferenceAttribute consoleViewContainerId;

	/** What to do after this component injection */
	@PostConstruct
	private void postConstruct(IEventBroker eventBroker) {
		initConsoleViewContainerId();
		PartUtil.onPartStackChanged(ProcessConsole.PART_ID, eventBroker, this::onPartStackChanged);
	}

	@Override
	public GProcess createProcess(String name, IProcessFunction function) {
		return new GProcess(name) {
			@Override
			protected IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
				return function.apply(monitor, logger);
			}
		};
	}

	@Override
	public void runInForeground(GProcess process) {
		// This solution should be used, but the Dialog has few bugs (details button...) &
		// the process will not run synchronized (which can be necessary, for Navshift opening...)
		// process.setUser(true);
		// process.setJobGroup(jobGroup);
		// process.schedule();

		ProgressMonitorDialog dialog = new ProgressMonitorDialog(Display.getDefault().getActiveShell()) {

			/** {@inheritDoc} */
			@Override
			protected void configureShell(Shell shell) {
				super.configureShell(shell);
				shell.setText(process.getName());
			}
		};
		process.whenException(exception -> Messages
				.openErrorMessage("Error while running process  : " + process.getName(), exception));
		try {
			dialog.run(true, true, process::run);
		} catch (InvocationTargetException | InterruptedException e) {
			Messages.openErrorMessage("Process " + process.getName() + " interrupted", e);
			Thread.currentThread().interrupt(); // Restore interrupted state...
		}
	}

	@Override
	public void runInBackground(GProcess process, boolean openConsoleView) {
		if (openConsoleView) {
			try {
				MTrimmedWindow window = (MTrimmedWindow) application.getChildren().get(0);
				EPartService partService = window.getContext().get(EPartService.class);
				MPart consolePart = PartManager.forceCreatePart(partService, ProcessConsole.PART_ID);
				consolePart.setLabel(process.getName());
				PartManager.addPartToStack(consolePart, application, modelService, consoleViewContainerId.getValue());
				PartUtil.showPartSashContainer(consolePart);
				PartManager.showPart(partService, consolePart);
				((ProcessConsole) consolePart.getObject()).bind(process);
			} catch (Exception e) {
				LOGGER.error("Error while opening console view : " + e.getMessage(), e);
			}
		}
		process.schedule();
	}

	/**
	 * Initialize attribute consoleViewContainerId. <br>
	 * Check the favorite PartStack id of the console views. <br>
	 * Set to default (PartManager.RIGHT_STACK_ID) if unknown
	 */
	private void initConsoleViewContainerId() {
		consoleViewContainerId = preferences.getConsoleViewContainerId();
		List<MPartStack> partStacks = modelService.findElements(application, consoleViewContainerId.getValue(),
				MPartStack.class);
		if (partStacks.isEmpty())
			consoleViewContainerId.setValue(PartManager.RIGHT_STACK_ID, false);
	}

	/** Subscribe to EventBroker and reacts when a Console view is added to a PartStack */
	private void onPartStackChanged(MPartStack partStack) {
		consoleViewContainerId.setValue(partStack.getElementId(), false);
		preferences.save();
	}
}
