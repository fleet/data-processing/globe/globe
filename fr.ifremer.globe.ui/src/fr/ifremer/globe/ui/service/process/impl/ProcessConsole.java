package fr.ifremer.globe.ui.service.process.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.runtime.job.GProcess;

/**
 * Composite widget that display output of a system process
 */
public class ProcessConsole {

	/** Constants **/
	public static final String PART_ID = "fr.ifremer.globe.process.console";
	public static final String ERROR_TAG = "ERROR";
	public static final String WARNING_TAG = "WARN";

	/** Properties **/
	private GProcess process;
	protected Logger logger;
	private AppenderSkeleton appender;

	/** Inner composite to display logs **/
	private StyledText text;

	/**
	 * Constructor
	 */
	@Inject
	public ProcessConsole(Composite parent) {
		this.text = new StyledText(parent, SWT.READ_ONLY | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		FontRegistry fontRegistry = new FontRegistry(parent.getDisplay());
		// fontRegistry.put("console", new FontData[] { new FontData("Courier New", 8, SWT.NORMAL) });
		this.text.setFont(fontRegistry.get("console"));
		this.text.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		Color errorColor = parent.getDisplay().getSystemColor(SWT.COLOR_RED);
		Color warnColor = SWTResourceManager.getColor(255, 127, 80);

		// colors lines
		text.addLineStyleListener(event -> {
			if (event.lineText.contains(ERROR_TAG)) {
				event.styles = new StyleRange[] {
						new StyleRange(event.lineOffset, event.lineText.length(), errorColor, null) };
			} else if (event.lineText.contains(WARNING_TAG)) {
				event.styles = new StyleRange[] {
						new StyleRange(event.lineOffset, event.lineText.length(), warnColor, null) };
			}
		});
	}

	@PreDestroy
	public void onClose() {
		if (process != null)
			process.cancel();
		unbind();
	}

	private void unbind() {
		if (process != null) {
			process = null;
		}
		// this code locks the UI...
		// if (appender != null) {
		// appender.close();
		// }
		// if (logger != null) {
		// logger.removeAppender(appender);
		// }
	}

	/**
	 * Displays logs of the provided {@link GProcess}.
	 */
	public void bind(GProcess process) {
		unbind();
		appender = new AppenderSkeleton() {

			@Override
			public boolean requiresLayout() {
				return false;
			}

			@Override
			public void close() { // nothing to do
			}

			@Override
			protected void append(LoggingEvent paramLoggingEvent) {
				Display.getDefault().syncExec(() -> {
					if (!text.isDisposed()) {
						final String logMessage = paramLoggingEvent.getRenderedMessage();
						final String level = paramLoggingEvent.getLevel().toString();
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
						String date = sdf.format(new Date(paramLoggingEvent.getTimeStamp()));
						ProcessConsole.this.text.append(date + "|" + level + "| " + logMessage + "\n");
						ProcessConsole.this.text.setTopIndex(ProcessConsole.this.text.getLineCount() - 1);
					}
				});
			}
		};
		this.process = process;
		logger = Logger.getLogger(process.getLogger().getName());
		logger.addAppender(appender);
	}

	/**
	 * @return the {@link #process}
	 */
	public GProcess getProcess() {
		return process;
	}
}
