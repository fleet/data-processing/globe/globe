/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.geographicview;

/**
 * All parameters applied to an elevation model to extend it
 */
public class ExtensionParameters {

	/** True when extension process is enabled on the elevation model */
	private final boolean extensionEnabled;

	/** % of extra space around the sector when extension will be computed */
	private final float extensionPercent;

	/**
	 * Constructor
	 */
	public ExtensionParameters() {
		this(false, 10f);
	}

	/**
	 * Constructor
	 */
	public ExtensionParameters(boolean extensionEnabled, float extensionPercent) {
		this.extensionEnabled = extensionEnabled;
		this.extensionPercent = extensionPercent;
	}

	/**
	 * @return the {@link #extensionEnabled}
	 */
	public boolean extensionEnabled() {
		return extensionEnabled;
	}

	/**
	 * @return the {@link #extensionPercent}
	 */
	public float extensionPercent() {
		return extensionPercent;
	}

}
