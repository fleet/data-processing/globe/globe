/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.geographicview;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.terrain.CompoundElevationModel;

/**
 * Service used to access the Nasa view
 */
public interface IGeographicViewService {

	/**
	 * Returns the service implementation of IGeographicViewService.
	 */
	static IGeographicViewService grab() {
		return OsgiUtils.getService(IGeographicViewService.class);
	}

	/**
	 * @return the AWT component for displaying World Wind
	 */
	WorldWindowGLCanvas getWwd();

	/**
	 * @return List of layers in this model.
	 */
	LayerList getLayers();

	/**
	 * Predict layers order. (Useful while the session restoration<br>
	 * Pair is <Layer name, Layer enabled>
	 */
	void setPredictedLayers(List<Pair<String, Boolean>> predictedLayers);

	/**
	 * Return true if specified layer belongs to the list of predicted layers<br>
	 * Useful to know if layer was present in the last session file
	 */
	boolean isPredictedLayer(String layerName);

	/**
	 * Remove the layer from the predicted ones layers order. <br>
	 * Useful when session state don't have to be restored
	 */
	void unPredictedLayer(String layerName);

	/**
	 * Add a new Layer to display in World Wind.
	 */
	void addLayer(Layer layer);

	/**
	 * Add a new {@link Layer} just over background layers.
	 */
	void addLayerBeforeCompass(Layer layer);

	/** @return true if a layer already exists with the spetified name */
	boolean hasLayer(String layerName);

	/**
	 * Replace a Layer by an other one.<br>
	 * If specified layer does not exist in WW, it is simply added
	 */
	void replaceLayer(Layer layer);

	/**
	 * Removed the specified layer from World Wind
	 */
	void removeLayer(Layer layer);

	/** @return the globe's elevation model. */
	CompoundElevationModel getElevationModel();

	/**
	 * Add a new ElevationModel to display in World Wind
	 *
	 * @return the parent model
	 */
	void addElevationModel(ElevationModel elevationModel);

	/**
	 * Removed the specified ElevationModel from World Wind
	 */
	void removeElevationModel(ElevationModel elevationModel);

	/**
	 * Required the refreshing of the geographic view
	 */
	void refresh();

	/**
	 * Adds a new placemark with a label on the geographic view.
	 */
	PointPlacemark addPlacemark(Position position, String label);

	/**
	 * Removes the placemark from the geographic view.
	 */
	void removePlacemark(PointPlacemark placemark);

	void waitForClickByUser(Consumer<Position> resultConsumer);

	/**
	 * Moves the view to see the provided {@link LatLon}.
	 */
	public void zoomTo(LatLon latlon);

	/**
	 * Moves the view to see the provided {@link Sector}.
	 * 
	 * @param elevation the maximum elevation of the objects to be zoomed in on
	 */
	void zoomToSector(Sector sector, float elevation);

	/**
	 * Moves the view to see the provided list of {@link Sector}
	 * 
	 * @param elevation the maximum elevation of the objects to be zoomed in on
	 */
	void zoomToSector(List<Sector> sectors, float elevation);

	/**
	 * Moves the view to see the provided {@link GeoBox}.
	 */
	void zoomToGeobox(GeoBox geobox);

	/**
	 * Moves the view to see the provided list of {@link GeoBox}.
	 */
	void zoomToGeobox(List<GeoBox> geoboxes);

	/** Replace extra information in the status bar */
	void setExtraStatusBarInfo(String info);

	/**
	 * Provides the extension parameters currently applied to the elevation model
	 */
	Optional<ExtensionParameters> getExtensionParameters(ElevationModel elevationModel);

	/** Changes the extension parameters of the elevation model */
	void applyExtensionParameters(ElevationModel elevationModel, ExtensionParameters flatteningParameters);

	/** Set marker mode as active */
	void setMarkersEnabled(boolean enable);

	/** Check if marker mode is active */
	boolean isMarkersEnabled();

	/** Set tooltip mode as active */
	void setTooltipEnabled(boolean enable);

	/** Check if marker mode is active */
	boolean isTooltipEnabled();

	/** Check if detailed picking is enabled */
	boolean isDetailPickEnabled();

	/** Active the MPart of the geographique view */
	void requestFocus();

	/** Activate performance monitoring mode **/
	void activatePerformance();

	/**
	 * Moves the specified {@link IWWLayer} to background (but over "background" layers).
	 */
	void moveLayerToBackground(IWWLayer layer);

	/**
	 * Moves the specified {@link IWWLayer} to front.
	 */
	void moveLayerToFront(IWWLayer layer);

	/**
	 * Moves the specified {@link ElevationModel} to back.
	 */
	void moveElevationToBack(ElevationModel layer);

	/**
	 * Moves the specified {@link ElevationModel} to front.
	 */
	void moveElevationToFront(ElevationModel layer);

	default Vec4 computePointFromPosition(Position position) {
		return getWwd().getModel().getGlobe().computePointFromPosition(position);
	}

	default Vec4 computePointFromPosition(Position position, double d) {
		return getWwd().getModel().getGlobe().computePointFromPosition(position, d);
	}

	/***
	 *
	 * @return true if texture are interpolated
	 */
	boolean isInterpolate();

	/***
	 * Change interpolation mode Nearest / Linear
	 */
	public void interpolateTerrain();

	/***
	 *
	 * @return true if high resolution is activated
	 */
	boolean isHighResolution();

	/***
	 * Change high resolution state
	 */
	void setHighResolution();

	/**
	 * Enables synchronization with selection.
	 */
	void setSyncViewSelection(boolean b);

	/**
	 * @return true if synchronizaiton with selection is enabled.
	 */
	boolean isSyncViewSelection();

}
