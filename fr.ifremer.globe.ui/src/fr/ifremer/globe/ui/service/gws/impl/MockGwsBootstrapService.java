package fr.ifremer.globe.ui.service.gws.impl;

import fr.ifremer.globe.core.runtime.gws.GwsBootstrapService;
import fr.ifremer.globe.core.runtime.gws.event.GwsState;

/**
 * Mock of GwsBootstrapService when GWS plugin is absent
 */
public class MockGwsBootstrapService implements GwsBootstrapService {

	/** Server can not run : GWS plugin absent */
	@Override
	public GwsState getServerState() {
		return GwsState.NOT_AVAILABLE;
	}

	/** Server can not start : GWS plugin absent */
	@Override
	public void startServing() {
		// Never called, GWS not running
	}

	@Override
	public void startDriverServices() {
		// Never called, GWS not running
	}

	@Override
	public void stopServing() {
		// Never called, GWS not running
	}
}
