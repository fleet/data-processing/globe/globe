/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.icon;

import java.util.Optional;
import java.util.Set;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 *
 */
public interface IIcon16ForFileProvider {

	/**
	 * Returns the service implementation of IIcon16ForFileProvider.
	 */
	static IIcon16ForFileProvider grab() {
		return OsgiUtils.getService(IIcon16ForFileProvider.class);
	}

	/** @return the list of managed file type */
	Set<ContentType> getContentTypes();

	/**
	 * @return the icon for representing a file
	 */
	Optional<Image> getIcon();

	/**
	 * @return the icon for representing the specified IFileInfo
	 */
	default Optional<Image> getIcon(IFileInfo fileInfo) {
		return getIcon();
	}

}
