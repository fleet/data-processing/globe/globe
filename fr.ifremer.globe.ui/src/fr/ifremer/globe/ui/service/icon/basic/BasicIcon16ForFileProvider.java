/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.icon.basic;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.image.ImageResources;

/**
 * Basic implementation of IIcon16ForFileProvider.
 */
public class BasicIcon16ForFileProvider implements IIcon16ForFileProvider {

	/** Icon **/
	protected String iconPath;
	/** ContentTypes **/
	protected Set<ContentType> contentTypes;

	/**
	 * Constructor
	 */
	public BasicIcon16ForFileProvider(String iconPath, ContentType... contentType) {
		this.iconPath = iconPath;
		contentTypes = EnumSet.copyOf(Arrays.asList(contentType));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<ContentType> getContentTypes() {
		return contentTypes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage(iconPath, getClass()));
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon(IFileInfo fileInfo) {
		return getIcon();
	}

}