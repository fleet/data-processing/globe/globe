/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.icon;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Osgi service providing Icons and images
 */
public interface IImageService {

	/**
	 * Returns the service implementation of IImageService.
	 */
	static IImageService grab() {
		return OsgiUtils.getService(IImageService.class);
	}

	/**
	 * @return the 16x16 icon for representing a file
	 */
	Optional<Image> getIcon16ForFile(ContentType contentType);

	/**
	 * @return the 16x16 icon for representing the specified IFileInfo
	 */
	Optional<Image> getIcon16ForFile(IFileInfo fileInfo);

}
