/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.icon.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.IImageService;

/**
 *
 */
@Component(name = "globe_ui_service_image", service = IImageService.class)
public class ImageService implements IImageService {

	/** All registered providers */
	protected List<IIcon16ForFileProvider> icon16ForFileProviders = new ArrayList<>();

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon16ForFile(ContentType contentType) {
		return icon16ForFileProviders.stream().//
				filter(provider -> provider.getContentTypes().contains(contentType)).//
				map(IIcon16ForFileProvider::getIcon).//
				findFirst().//
				orElse(Optional.empty());
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon16ForFile(IFileInfo fileInfo) {
		return icon16ForFileProviders.stream().//
				filter(provider -> provider.getContentTypes().contains(fileInfo.getContentType())).//
				map(provider -> provider.getIcon(fileInfo)).//
				findFirst().//
				orElse(Optional.empty());
	}

	/** Register a new provider */
	@Reference(name = "Icon16ForFileProvider", cardinality = ReferenceCardinality.MULTIPLE)
	public void addIcon16ForFileProvider(IIcon16ForFileProvider provider) {
		icon16ForFileProviders.add(provider);
	}

}
