/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.geobox;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Service used to display some GeoBox in the Nasa view
 */
public interface IGeoBoxLayerService {

	/**
	 * Returns the service implementation of IGeoBoxLayerService.
	 */
	static IGeoBoxLayerService grab() {
		return OsgiUtils.getService(IGeoBoxLayerService.class);
	}

	/**
	 * Create a Nasa Layer to display a GeoBox
	 *
	 * @return the model to customize the rendering
	 */
	IGeoBoxLayerModel createLayer();

	/**
	 * Remove the Nasa layer used to display the specified GeoBox model. <br>
	 * After removing the layer, the model is disposed and is no more usable
	 */
	void removeLayer(IGeoBoxLayerModel model);

	/**
	 * Turn the globe to reveal the geobox
	 */
	void revealLayer(IGeoBoxLayerModel model);

}
