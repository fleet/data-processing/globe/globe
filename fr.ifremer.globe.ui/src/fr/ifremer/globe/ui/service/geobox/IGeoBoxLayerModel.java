package fr.ifremer.globe.ui.service.geobox;

import java.awt.Color;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableGeoBox;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;

/** Model used by a Nasa layer to display a GeoBox */
public interface IGeoBoxLayerModel {

	/**
	 * @return the GeoBox to display
	 */
	WritableGeoBox getGeoBox();

	/**
	 * @return if the interior is filled
	 */
	WritableBoolean getFilled();

	/**
	 * @return the interior color
	 */
	WritableObject<Color> getInteriorColor();

	/**
	 * @return the interior opacity
	 */
	WritableDouble getInteriorOpacity();

	/**
	 * @return the outline color
	 */
	WritableObject<Color> getOutlineColor();

	/**
	 * @return the outline opacity
	 */
	WritableDouble getOutlineOpacity();

	/**
	 * @return the outline width
	 */
	WritableDouble getOutlineWidth();

}