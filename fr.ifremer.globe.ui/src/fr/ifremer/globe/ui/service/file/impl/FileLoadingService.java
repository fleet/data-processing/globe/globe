/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.file.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobGroup;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.xsf.info.DeprecatedXsfInfo;
import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoEvent.FileInfoState;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.runtime.ipc.message.IpcConstants;
import fr.ifremer.globe.core.runtime.ipc.message.IpcMessage;
import fr.ifremer.globe.core.runtime.ipc.message.IpcMessageType;
import fr.ifremer.globe.core.runtime.ipc.service.IGlobeIpcServer;
import fr.ifremer.globe.core.runtime.ipc.service.IpcMessageReceiver;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import jakarta.annotation.PostConstruct;

/**
 * Implementation of IFileLoadingService.
 */
@Component(name = "globe_ui_service_file_loading_service", service = IFileLoadingService.class)
public class FileLoadingService implements IFileLoadingService, IpcMessageReceiver {

	protected Logger logger = LoggerFactory.getLogger(FileLoadingService.class);

	/** Services **/

	@Reference
	private ISessionService sessionService;

	@Reference
	private IWWLayerService layerService;

	@Reference
	private IFileService fileService;

	@Reference
	private IWWTilesService tilesService;

	// Injected components
	private FileInfoModel fileInfoModel;
	private WWFileLayerStoreModel fileLayerStoreModel;
	private ProjectExplorerModel projectExplorerModel;

	/** Loading threads **/
	private static final int LOADING_THREADS = 8;

	/**
	 * Initialization after injection
	 */
	@PostConstruct
	private void initialize(FileInfoModel fileInfoModel, WWFileLayerStoreModel fileLayerStoreModel,
			ProjectExplorerModel projectExplorerModel) {
		this.fileInfoModel = fileInfoModel;
		this.fileLayerStoreModel = fileLayerStoreModel;
		this.projectExplorerModel = projectExplorerModel;
	}

	@Override
	public synchronized void load(List<String> filePaths, boolean silently, boolean reload) {
		// get two list of files : files to load and files already present
		var fileNamesInModel = fileInfoModel.getAll().stream().map(IFileInfo::getFilename).toList();
		var filesToLoad = new ArrayList<String>();
		var filesAlreadyLoaded = new ArrayList<String>();
		for (var filePath : filePaths) {
			var path = Paths.get(filePath).getFileName();
			if (path == null)
				continue;
			var newFileName = path.toString();
			if (!fileNamesInModel.contains(newFileName))
				filesToLoad.add(filePath);
			else
				filesAlreadyLoaded.add(newFileName);
		}

		// create JobGroup & launch LoadingFileJob
		var jobGroup = new JobGroup(FileLoadingService.class.getName(), LOADING_THREADS, 0);
		filesToLoad.stream().map(filePath -> new LoadingFileJob(jobGroup, filePath, silently, reload))
				.forEach(Job::schedule);

		// monitor jobs
		IProcessService.grab().runInBackground("File loading (" + filesToLoad.size() + ")", (monitor, logger) -> {
			try {
				jobGroup.join(0, monitor);
			} catch (OperationCanceledException | InterruptedException e) {
				logger.warn("File loading interrupted : " + e.getMessage(), e);
				jobGroup.cancel();
			}
			var errors = Optional.ofNullable(jobGroup.getResult()) // result can be null
					.map(MultiStatus::getChildren).stream().flatMap(Arrays::stream) // stream result status
					.filter(status -> !status.isOK() && status.getSeverity() != IStatus.CANCEL) // keep error status
					.map(IStatus::getMessage).collect(Collectors.joining("\n")); // build message
			if (!errors.isEmpty()) {
				// display error in dialog
				Messages.openErrorMessage("File loading error", errors);
				return Status.error("File loading error : " + errors);
			} else
				return Status.OK_STATUS;
		});

		// alert about files already loaded
		if (!filesAlreadyLoaded.isEmpty())
			Messages.openWarningMessage("Load data files",
					"File(s) already loaded : \n" + filesAlreadyLoaded.stream().collect(Collectors.joining("\n")));
	}

	@Override
	public void unload(List<String> filePaths) {
		Job.createSystem("Unload", monitor -> {
			sessionService.saveFilesParameters(filePaths);
			for (String filePath : filePaths) {
				fileLayerStoreModel.remove(filePath);
				fileInfoModel.unload(filePath);
			}
		}).schedule();
	}

	@Override
	public void reload(List<String> filePaths, boolean cleanSession) {
		Job.createSystem("Reload", monitor -> {
			if (cleanSession)
				sessionService.removeFilesParameters(filePaths);
			for (String filePath : filePaths) {
				fileLayerStoreModel.remove(filePath);
				fileInfoModel.reload(filePath, cleanSession ? FileInfoState.RELOADING : FileInfoState.LOADING);
				// clean Nasa cache to force the tile computation
				tilesService.clearCache(new File(filePath));
			}
			load(filePaths, !cleanSession, true);
		}).schedule();
	}

	@Override
	public void delete(List<String> filePaths) {
		filePaths.forEach(filePath -> {
			fileInfoModel.remove(filePath);
			fileLayerStoreModel.remove(filePath);
			// clean Nasa cache to force the tile computation
			tilesService.clearCache(new File(filePath));
		});
	}

	@Override
	public void cancel(List<String> filePaths) {
		Stream.of(Job.getJobManager().find(this))//
				.filter(LoadingFileJob.class::isInstance)//
				.map(LoadingFileJob.class::cast)//
				.filter(loadingJob -> filePaths.stream().anyMatch(loadingJob.filePath::equals))//
				.forEach(LoadingFileJob::cancel);
	}

	/** Eclipse job to load one file asynchronously */
	public class LoadingFileJob extends Job {

		/** IFileInfo corresponding to the file to load */
		protected final String filePath;
		protected boolean silently;
		protected boolean reload;

		/**
		 * Constructor
		 */
		public LoadingFileJob(JobGroup jobGroup, String filePath, boolean silently, boolean reload) {
			super("Loading file " + filePath);
			this.filePath = filePath;
			this.silently = silently;
			this.reload = reload;
			setUser(false);
			setSystem(false);
			setPriority(Job.LONG);
			setJobGroup(jobGroup);
			// change icon to "Loading icon"
			fileInfoModel.submit(new FileInfoEvent(new BasicFileInfo(filePath), FileInfoState.LOADING));
		}

		/** Run the job */
		@Override
		protected IStatus run(IProgressMonitor monitor) {
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			var subMonitor = SubMonitor.convert(monitor, 100);
			var start = Instant.now();
			try {
				// Check if file exists
				if (!Files.exists(Paths.get(filePath)))
					return error(String.format("File does not exist: %s ", filePath),
							new FileNotFoundException(filePath));

				// get file info
				subMonitor.setTaskName("Loading file...");
				var optFileInfo = (silently ? fileService.getFileInfoSilently(filePath)
						: fileService.getFileInfo(filePath));
				if (optFileInfo.isEmpty()) {
					// check ascii path
					// TODO : find a better way to load such files with netcdf Library
					if (!StandardCharsets.US_ASCII.newEncoder().canEncode(filePath))
						return error(String.format("File path contains non ASCII characters: %s ", filePath),
								new FileNotFoundException(filePath));
					return error(String.format("File '%s' not loadable : unknown file type.",
							FilenameUtils.getName(filePath)));
				}
				var fileInfo = optFileInfo.get();
				subMonitor.worked(35);

				// if not already loaded : load layers
				if (monitor.isCanceled())
					throw new OperationCanceledException();
				subMonitor.setTaskName("Creating layers...");
				if (!fileLayerStoreModel.contains(fileInfo))
					layerService.getLayers(fileInfo, silently, subMonitor.split(65))
							.ifPresent(layerStore -> fileLayerStoreModel.add(fileInfo.getPath(), layerStore));
				logger.info("Loading of {} ({}) finished in {}.", FilenameUtils.getName(filePath),
						fileInfo.getClass().getSimpleName(), Duration.between(start, Instant.now()));
				fileInfoModel.update(fileInfo, FileInfoState.LOADED);

				// warning for deprecated XSF
				if (fileInfo instanceof DeprecatedXsfInfo)
					return error(String.format("Loading of '%s' failed. Deprecated Xsf version.", filePath));
			} catch (CancellationException e) {
				// Click on Cancel in a dialog before the file parsing
				logger.warn("Loading of '{}' aborted.", FilenameUtils.getName(filePath));
				fileInfoModel.unload(filePath);
				if (!reload)
					// Not a reload. Remove the file from the project explorer
					projectExplorerModel.getFileInfoNode(filePath).ifPresent(FileInfoNode::delete);
				return Status.CANCEL_STATUS;
			} catch (OperationCanceledException e) {
				// Click on Cancel button during the file parsing
				logger.warn("Loading of '{}' canceled.", FilenameUtils.getName(filePath));
				fileInfoModel.unload(filePath);
				return Status.CANCEL_STATUS;
			} catch (Exception e) {
				return error(
						String.format("Loading of '%s' failed : %s", FilenameUtils.getName(filePath), e.getMessage()),
						e);
			}
			return Status.OK_STATUS;
		}

		/** {@inheritDoc} */
		@Override
		public boolean belongsTo(Object family) {
			return family == FileLoadingService.this;
		}

		/** {@inheritDoc} */
		@Override
		protected void canceling() {
			// Cancel happened when user
			// - stop the job in the progress view
			// - delete a the node of a loading file
			// - clic on the cancel button in a progress dialog
			fileInfoModel.unload(filePath);
			logger.info("Canceling the loading of {}", filePath);
			super.canceling();
		}

		private IStatus error(String message) {
			return error(message, Optional.empty());
		}

		private IStatus error(String message, Throwable ex) {
			return error(message, Optional.of(ex));
		}

		private IStatus error(String message, Optional<Throwable> ex) {
			logger.error(message);
			fileInfoModel.update(new BasicFileInfo(filePath), FileInfoState.ERROR);
			fileLayerStoreModel.remove(filePath);
			// return only warning and not error to avoid the all the group being stopped
			return ex.isPresent() ? Status.warning(message, ex.get()) : Status.warning(message);
		}
	}

	/**
	 * @return the {@link #fileInfoModel}
	 */
	@Override
	public FileInfoModel getFileInfoModel() {
		return fileInfoModel;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<String> accept(IpcMessage message) {
		if (message.type == IpcMessageType.LOAD_FILE) {
			File fileToLoad = new File(message.argument);
			if (fileToLoad.exists()) {
				reload(fileToLoad);
				return Optional.of(IpcConstants.REPLY_OK);
			}
			return Optional.of("File not found. Loading aborted");
		}
		// Not a load message
		return Optional.empty();
	}

	@Activate
	private void osgiActivation(BundleContext bundleContext) {
		IGlobeIpcServer ipcServer = OsgiUtils.getService(bundleContext, IGlobeIpcServer.class);
		ipcServer.addIpcMessageReceiver(this);
	}

}
