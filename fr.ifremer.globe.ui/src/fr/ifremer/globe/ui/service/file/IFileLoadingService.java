/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.file;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Service to load files in Globe
 */
public interface IFileLoadingService {

	/**
	 * @return the service implementation of IImportService.
	 */
	static IFileLoadingService grab() {
		return OsgiUtils.getService(IFileLoadingService.class);
	}

	/**
	 * Loads some files : creates nodes and loads associated WW layers.
	 * 
	 * @param silently : false to allow user interactions. true to prevent dialog messages.
	 * @param reload : true when file exits in Globe and a reload has been required.
	 */
	void load(List<String> filePaths, boolean silently, boolean reload);

	/**
	 * Loads some files : creates nodes and loads associated WW layers.
	 * 
	 * @param silently : false to allow user interactions. true to prevent dialog messages.
	 */
	default void load(List<String> filePaths, boolean silently) {
		load(filePaths, silently, false);
	}

	/** Load some files asynchronously and hook resulting WW layers to the root data node. */
	default void load(List<String> filePaths) {
		load(filePaths, false);
	}

	/** Load some files asynchronously and hook resulting WW layers to the root data node. */
	default void load(String... filePaths) {
		load(List.of(filePaths), false);
	}

	/**
	 * Load some files asynchronously and hook resulting WW layers to the root data node.
	 *
	 * @param silently false to allow user interactions. true to prevent dialog messages
	 */
	default void loadFiles(List<File> files, boolean silently) {
		load(files.stream().map(File::getAbsolutePath).toList(), silently);
	}

	/** Load some files asynchronously and hook resulting WW layers to the root data node. */
	default void loadFiles(List<File> files) {
		loadFiles(files, false);
	}

	/** Loads some files and associated WW layers. **/
	default void load(File... files) {
		loadFiles(List.of(files), false);
	}

	/*--------------------- UNLOAD ---------------------*/

	/** Closes and unbooks the files */
	void unload(List<String> filepaths);

	/** Closes and unbooks the files */
	default void unload(String... filePaths) {
		unload(List.of(filePaths));
	}

	/** Closes and unbooks the files */
	default void unloadFiles(List<File> files) {
		unload(files.stream().map(File::getAbsolutePath).toList());
	}

	/** Closes and unbooks the files */
	default void unload(File... files) {
		unload(Arrays.stream(files).map(File::getAbsolutePath).toList());
	}

	/*--------------------- RELOAD ---------------------*/

	/**
	 * Closes the files, unbook, clear cache and reload them
	 * 
	 * @param cleanSession true to remove session parameters of the files
	 */
	void reload(List<String> filepaths, boolean cleanSession);

	/** Closes the files, unbook, clear cache and reload them */
	default void reload(String... filePaths) {
		reload(List.of(filePaths), true);
	}

	/** Closes the files, unbook, clear cache and reload them */
	default void reloadFiles(List<File> files) {
		reload(files.stream().map(File::getAbsolutePath).toList(), true);
	}

	/** Closes the files, unbook, clear cache and reload them */
	default void reload(File... files) {
		reload(Arrays.stream(files).map(File::getAbsolutePath).toList(), true);
	}

	/*--------------------- DELETE ---------------------*/

	/** Unloads the files and clear cache */
	void delete(List<String> filepaths);

	/** Unloads the files and clear cache */
	default void delete(String... filePaths) {
		delete(List.of(filePaths));
	}

	/** Unloads the files and clear cache */
	default void deleteFiles(List<File> files) {
		delete(files.stream().map(File::getAbsolutePath).toList());
	}

	/** Unloads the files and clear cache */
	default void delete(File... files) {
		delete(Arrays.stream(files).map(File::getAbsolutePath).toList());
	}

	/*--------------------- CANCEL ---------------------*/

	/** Cancels the loading of some files */
	void cancel(List<String> filepaths);

	/** Cancels the loading of some files */
	default void cancel(String... filePaths) {
		cancel(List.of(filePaths));
	}

	/** Cancels the loading of some files */
	default void cancelFiles(List<File> files) {
		cancel(files.stream().map(File::getAbsolutePath).toList());
	}

	/** Cancels the loading of some files */
	default void cancel(File... files) {
		cancel(Arrays.stream(files).map(File::getAbsolutePath).toList());
	}

	/** @return the loaded files model */
	FileInfoModel getFileInfoModel();

}
