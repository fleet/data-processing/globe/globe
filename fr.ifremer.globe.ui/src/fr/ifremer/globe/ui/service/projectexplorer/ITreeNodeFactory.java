/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.projectexplorer;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Service used to create TreeNodes
 */
public interface ITreeNodeFactory {

	/**
	 * Returns the service implementation of ITreeNodeFactory.
	 */
	static ITreeNodeFactory grab() {
		return OsgiUtils.getService(ITreeNodeFactory.class);
	}

	// FILE NODES

	/**
	 * Create a FileInfoNode for the specified file info. <br>
	 * The resulting node is hooked to the current selected GroupNode (Root node when selection is empty)
	 */
	default FileInfoNode createFileInfoNode(IFileInfo fileInfo) {
		return createFileInfoNode(fileInfo, Optional.empty(), Optional.empty());
	}

	/**
	 * Create a FileInfoNode for the specified file info. <br>
	 * The resulting node is added to given parent node.
	 */
	default FileInfoNode createFileInfoNode(IFileInfo fileInfo, GroupNode parentNode) {
		return createFileInfoNode(fileInfo, Optional.of(parentNode), Optional.empty());
	}

	/**
	 * Create a FileInfoNode for the specified file info. <br>
	 * The resulting node is added to given parent at the specified index.
	 */
	default FileInfoNode createFileInfoNode(IFileInfo fileInfo, GroupNode parentNode, int index) {
		return createFileInfoNode(fileInfo, Optional.of(parentNode), Optional.of(index));
	}

	/**
	 * Create a FileInfoNode for the specified file path. <br>
	 * The resulting node is added to given parent at the specified index.
	 */
	default FileInfoNode createFileInfoNode(String filePath, Optional<? extends TreeNode> optTargetNode,
			Optional<Integer> optTargetIndex) {
		return createFileInfoNode(new BasicFileInfo(filePath), optTargetNode, optTargetIndex);
	}

	/**
	 * Create a FileInfoNode for the specified file info. <br>
	 * The resulting node is hooked to given parent (or root node when parentNode is null)
	 */
	FileInfoNode createFileInfoNode(String filePath, String parentNodePath);

	/**
	 * Creates new file node for each file (if does not already exist)
	 *
	 * @param targetNodePath : node path where file will be added.
	 */
	default void createFileInfoNode(List<String> filePaths, String targetNodePath) {
		filePaths.forEach(filePath -> createFileInfoNode(filePath, targetNodePath));
	}

	/**
	 * Creates new file node for each file (if does not already exist)
	 *
	 * @param optTargetNode : optional node where will be added
	 */
	default void createFileInfoNode(List<String> filePaths, Optional<? extends TreeNode> optTargetNode) {
		filePaths.forEach(filePath -> createFileInfoNode(filePath, optTargetNode, Optional.empty()));
	}

	/**
	 * Creates {@link FileInfoNode} node if not already exists.
	 * 
	 * @param fileInfo : {@link IFileInfo} to bind with node.
	 * @param optTargetNode : optional node where the new {@link FileInfoNode} should be added.
	 * @param optTargetIndex : optional index to define where the new {@link FileInfoNode} should be added.
	 * 
	 * @return new {@link FileInfoNode} or one already exist for the specified {@link IFileInfo}.
	 */
	FileInfoNode createFileInfoNode(IFileInfo fileInfo, Optional<? extends TreeNode> optTargetNode,
			Optional<Integer> optTargetIndex);

	// FOLDER NODES

	/**
	 * Create a FolderNode with the specified name and comments.<br>
	 * The resulting node is hooked to given parent (or root node when parentNode is null)
	 */
	FolderNode createFolderNode(GroupNode parentNode, String name, String comments);

	/**
	 * Create a FileInfoNode for the specified file info. <br>
	 * The resulting node has no parent.
	 */
	FolderNode createOrphanFolderNode(String name, String comments);

	/// LAYER NODES

	/** Create a TreeNode for the specified layer. This TreeNode can safely be cast into a LayerNode. */
	LayerNode createNode(IWWLayer layer);

	/** Creates or updates {@link LayerNode} under the specified {@link GroupNode}. **/
	boolean createOrUpdateLayerNodes(GroupNode groupNode, WWFileLayerStore fileLayerStore);

}
