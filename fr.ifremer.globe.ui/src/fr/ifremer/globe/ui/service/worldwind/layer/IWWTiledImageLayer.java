/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing an image (ie BasicTiledImageLayer)
 */
public interface IWWTiledImageLayer extends IWWLayer {

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.RASTER.toImage()));
	}

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
