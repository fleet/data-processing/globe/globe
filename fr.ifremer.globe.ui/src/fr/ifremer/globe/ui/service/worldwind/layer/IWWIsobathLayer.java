/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing a color scale (ie ColorScaleLayer)
 */
public interface IWWIsobathLayer extends IWWLayer {

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	@Override
	default String getShortName() {
		return Optional.ofNullable((String) getValue(SHORT_NAME)).orElse("Isobaths");
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.ISOBATHS.toImage()));
	}

}
