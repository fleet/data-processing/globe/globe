/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.basic;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWGeoBoxLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWIsobathLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWOldMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWQuadSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWRenderableLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWShapefileLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWTiledImageLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWVerticalImageLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWallLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWWaterColumnVolumicLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;

/**
 * Basic IWWLayerOperation with no effect
 */
public abstract class BasicWwLayerOperation implements IWWLayerOperation {

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWTerrainLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWTerrainWithPaletteLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWColorScaleLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWTiledImageLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWIsobathLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWNavigationLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWWaterColumnVolumicLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWShapefileLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWTextureLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWComputableTextureLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWMultiTextureLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWPlayableTextureLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWPlayableMultiTextureLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWRenderableLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWVerticalImageLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWGeoBoxLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWWallLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWMarkerLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWPointCloudLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public BasicWwLayerOperation accept(IWWOldMultiTextureLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IWWLayerOperation accept(IWWQuadSelectionLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IWWLayerOperation accept(IWWSwathSelectionLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IWWLayerOperation accept(IWWPolygonLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IWWLayerOperation accept(IWWCurrentLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	@Override
	public IWWLayerOperation accept(IWWAnnotationLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	@Override
	public IWWLayerOperation accept(IWWLabelLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	@Override
	public IWWLayerOperation accept(IWWUsblLayer layer) {
		performDefaultOperation(layer);
		return this;
	}

	/** Default behavior when no specific operation is required for the layer */
	public void performDefaultOperation(IWWLayer layer) {
	}

}
