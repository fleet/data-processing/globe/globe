/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.tile;

import java.io.File;
import java.util.List;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Osgi service used to manage tiled pyramid images of raster files
 */
public interface IWWTilesService {

	/**
	 * Returns the service implementation of IWWTilesService.
	 */
	static IWWTilesService grab() {
		return OsgiUtils.getService(IWWTilesService.class);
	}

	/** @return how to present the file name in Nasa */
	String computeDisplayName(File file);

	/**
	 * @param displayName Name of this image
	 * @return the list of folders containing the Nasa cache for the specified name
	 */
	List<File> getCacheFolders(String displayName);

	/**
	 * Compute the XML configuration file discribing the pyramid tiles levels of a rgb file
	 *
	 * @param displayName Name of this image
	 * @return The configuration file path
	 */
	File computeImageCacheDocument(String displayName);

	/**
	 * @return a builder of producter of image tiles for the specified image tiff file
	 */
	ITilesProducerBuilder makeImageProducerBuilder(File rgbFile);

	/**
	 * Compute the XML configuration file discribing the pyramid tiles levels of a DEM file
	 *
	 * @param displayName Name of this image
	 * @return The configuration file path
	 */
	@Deprecated(forRemoval = true)
	File computeElevationCacheDocument(String displayName);

	/**
	 * @return a builder of producer of elevation tiles for the specified raster tiff file
	 */
	@Deprecated(forRemoval = true)
	ITilesProducerBuilder makeElevationProducerBuilder(File rgbFile);

	/**
	 * Compute the XML configuration files describing the pyramid tiles levels of a DEM file<br>
	 * At least, one XML configuration file is returned.<br>
	 * 2 files can be returned if geobox is spanning the 180 meridian
	 *
	 * @param displayName Name of this image
	 * @param geoBox of the file * @return The configuration file path
	 */
	List<File> computeRasterCacheDocument(String displayName, GeoBox geoBox);

	/**
	 * @return a builder of producer of raster tiles for the specified Gdal dataset
	 */
	default ITilesProducerBuilder makeTerrainProducerBuilder(GdalDataset gdalDataset) {
		return makeTerrainProducerBuilder(gdalDataset, new GdalDataset(null));
	}

	/**
	 * @return a builder of producer of raster tiles for the specified Gdal dataset and
	 */
	ITilesProducerBuilder makeTerrainProducerBuilder(GdalDataset gdalDataset, GdalDataset elevationDataset);

	/** Clear cache for the specified file */
	void clearCache(File file);
}
