/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.elevation;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.terrain.BasicElevationModel;

/**
 * Osgi service used to produce WorldWind ElevationModel
 */
public interface IWWElevationModelFactory {

	/**
	 * Returns the service implementation of IWWElevationFactory.
	 */
	static IWWElevationModelFactory grab() {
		return OsgiUtils.getService(IWWElevationModelFactory.class);
	}

	/**
	 * @param domElement the XML document root to parse for ElevationModel configuration parameters. See
	 *            {@link IWWTilesService} to produce the XML document
	 * @return the ElevationModel with the interpolation functionalities
	 */
	IWWElevationModel createInterpolateElevationModel(Element domElement);

	/**
	 * @param geoBox Geobox covering all tilesPyramids
	 * @param tilesPyramids all XML document roots to parse for ElevationModel configuration parameters. See
	 *            {@link IWWTilesService} to produce the XML document
	 * @return the ElevationModel with the interpolation functionalities
	 */
	IWWElevationModel createInterpolateElevationModel(String name, GeoBox geoBox, List<Document> tilesPyramids);

	/**
	 * @param domElement the XML document root to parse for ElevationModel configuration parameters. See
	 *            {@link IWWTilesService} to produce the XML document
	 * @param params the output key-value pairs which recieve the BasicElevationModel configuration parameters. A null
	 *            reference is permitted.
	 *
	 * @return the ElevationModel with the interpolation functionalities
	 */
	IWWElevationModel createInterpolateElevationModel(Element domElement, AVList params);

	/**
	 * @param domElement the XML document root to parse for ElevationModel configuration parameters. See
	 *            {@link IWWTilesService} to produce the XML document
	 * @return the movable ElevationModel with the interpolation functionalities
	 */
	BasicElevationModel createMovableInterpolateElevationModel(Element domElement);

	/**
	 * @param domElement the XML document root to parse for ElevationModel configuration parameters. See
	 *            {@link IWWTilesService} to produce the XML document
	 * @param params the output key-value pairs which recieve the BasicElevationModel configuration parameters. A null
	 *            reference is permitted.
	 * @return the movable ElevationModel with the interpolation functionalities
	 */
	BasicElevationModel createMovableInterpolateElevationModel(Element domElement, AVList params);

}
