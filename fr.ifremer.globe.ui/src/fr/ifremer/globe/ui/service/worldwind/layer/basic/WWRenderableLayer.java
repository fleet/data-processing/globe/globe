package fr.ifremer.globe.ui.service.worldwind.layer.basic;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWRenderableLayer;
import gov.nasa.worldwind.layers.RenderableLayer;

/**
 * Implementation of IWWRenderableLayer
 */
public class WWRenderableLayer extends RenderableLayer implements IWWRenderableLayer {

	/** All recorded listener */
	protected List<Runnable> disposeListeners = new ArrayList<>();

	/**
	 * Constructor
	 */
	public WWRenderableLayer() {
		setPickEnabled(false);
	}

	/** {@inheritDoc} */
	@Override
	public <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		super.dispose();
		disposeListeners.forEach(Runnable::run);
	}

	/** {@inheritDoc} */
	@Override
	public void addDisposeListener(Runnable listener) {
		disposeListeners.add(listener);
	}

	/** {@inheritDoc} */
	@Override
	public void removeDisposeListener(Runnable listener) {
		disposeListeners.remove(listener);
	}

	/** {@inheritDoc} */
	@Override
	public void render() {
		IGeographicViewService.grab().refresh();
	}
}
