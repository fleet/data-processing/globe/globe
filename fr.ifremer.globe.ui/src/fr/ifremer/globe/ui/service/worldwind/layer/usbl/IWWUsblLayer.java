package fr.ifremer.globe.ui.service.worldwind.layer.usbl;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.io.usbl.UsblSettings;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing a vertical image.
 */
public interface IWWUsblLayer extends IWWLayer {

	@Override
	default String getShortName() {
		return "Target and landing areas";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.USBL.toImage()));
	}

	/** @return field used to feed the layer */
	UsblFileInfo getFieldInfo();

	/** @return the parameters used to render the layer */
	UsblSettings getParameters();

	/** Set the parameters used to render the layer */
	void setParameters(UsblSettings parameters);
	
	/** Refreshes the layer. **/
	void refresh();

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

}
