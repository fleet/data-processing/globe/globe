package fr.ifremer.globe.ui.service.worldwind.layer.marker;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

public class /* Records */ WWMarkerLayerParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<WWMarkerLayerParameters> SESSION_KEY = new TypedKey<>("Marker_parameters");

	public final float opacity;
	public final boolean applyProjectionOnTerrain;
	public final boolean resizingWithZoom;

	public final boolean applyVerticalOffset;
	public final float verticalOffset;

	public final boolean applySize;
	public final float size;

	public final boolean applyColor;
	public final GColor color;

	public WWMarkerLayerParameters() {
		this(1.0f, false, false, false, 0f, false, 15f, false, GColor.RED);
	}

	public WWMarkerLayerParameters(float opacity, boolean projectMarkerOnTerrain, boolean resizingWithZoom,
			boolean applyVerticalOffset, float verticalOffset, boolean applySize, float size, boolean applyColor,
			GColor color) {
		super();
		this.opacity = opacity;
		this.applyProjectionOnTerrain = projectMarkerOnTerrain;
		this.resizingWithZoom = resizingWithZoom;
		this.applyVerticalOffset = applyVerticalOffset;
		this.verticalOffset = verticalOffset;
		this.applySize = applySize;
		this.size = size;
		this.applyColor = applyColor;
		this.color = color;
	}

	public WWMarkerLayerParameters withOptacity(float opacity) {
		return new WWMarkerLayerParameters(opacity, applyProjectionOnTerrain, resizingWithZoom, applyVerticalOffset,
				verticalOffset, applySize, size, applyColor, color);
	}

	public WWMarkerLayerParameters withProjectMarkerOnTerrain(boolean applyProjectionOnTerrain) {
		return new WWMarkerLayerParameters(opacity, applyProjectionOnTerrain, resizingWithZoom, applyVerticalOffset,
				verticalOffset, applySize, size, applyColor, color);
	}

	public WWMarkerLayerParameters withResizingWithZoom(boolean resizingWithZoom) {
		return new WWMarkerLayerParameters(opacity, applyProjectionOnTerrain, resizingWithZoom, applyVerticalOffset,
				verticalOffset, applySize, size, applyColor, color);
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<WWMarkerLayerParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

}
