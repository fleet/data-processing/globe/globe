/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer;

import java.awt.Color;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;

import org.eclipse.core.runtime.IProgressMonitor;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.IPointCloudFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.core.model.marker.IMarkerFileInfo;
import fr.ifremer.globe.core.model.marker.IWCMarker;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer.ComputableTextureParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.WWPlayableTextureInitParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.BiIntToDoubleFunction;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Factory of worldwind layers
 */
public interface IWWLayerFactory {

	/**
	 * Returns the service implementation of IWWLayerFactory.
	 */
	static IWWLayerFactory grab() {
		return OsgiUtils.getService(IWWLayerFactory.class);
	}

	/**
	 * Creates an instance of IWWDtmLayer (ie ShaderElevationLayer) for DEPTH layer...
	 *
	 * @param sourceFile Source file.
	 * @param document XML document describing the layer.
	 * @param kindOfData type of data as Elevation min, Elevation max...
	 */
	IWWTerrainLayer createElevationLayer(File sourceFile, Document document, String kindOfData);

	/**
	 * Generates layers to render a raster <br>
	 * The raster is generated on the fly using rasterValueSupplier
	 */
	Optional<WWRasterLayerStore> createRasterLayers(RasterFeatures rasterFeatures,
			BiIntToDoubleFunction rasterValueSupplier, IProgressMonitor monitor) throws GIOException;

	/**
	 * Creates an instance of IWWDtmLayer (ie ShaderElevationLayer) for layer such as Reflectivity...
	 *
	 * @param sourceFile Source file.
	 * @param document XML document describing the layer.
	 * @param kindOfData type of data as STDEV, interpolation flag...
	 * @deprecated Use {@link #createTerrainLayer}
	 */
	@Deprecated(forRemoval = true)
	IWWTerrainLayer createDtmLayer(File sourceFile, Document document, String kindOfData);

	/**
	 * Creates an instance of IWWTerrainLayer for layer such as Elevation, Reflectivity...
	 *
	 * @param document all XML documents describing the layer.
	 * @param kindOfData type of data as Elevation, Reflectivity...
	 */
	IWWTerrainLayer createTerrainLayer(List<Document> documents, String kindOfData, String name);

	/**
	 * Creates an instance of IWWTerrainWithPaletteLayer for layer such as Bitfield...
	 *
	 * @param document all XML documents describing the layer.
	 * @param palette the initial palette...
	 */
	IWWTerrainWithPaletteLayer createTerrainWithPaletteLayer(List<Document> documents, String name,
			AColorPalette palette);

	/**
	 * Creates an instance of IWWColorScaleLayer (ie ColorScaleLayer)
	 *
	 * @param source : provides the {@link ColorContrastModel} used to build the color scale.
	 * @param label : label of the color scale.
	 */
	IWWColorScaleLayer createColorScaleLayer(Supplier<ColorContrastModel> source, String label);

	/**
	 * Creates an instance of IWWColorScaleLayer from {@link IWWTerrainLayer}
	 */
	IWWColorScaleLayer createColorScaleLayer(IWWTerrainLayer terrainLayer);

	/**
	 * Creates an instance of IWWTiledImageLayer (ie BasicTiledImageLayer)
	 *
	 * @param document XML document describing the layer.
	 */
	IWWTiledImageLayer createTiledImageLayer(Document document);

	/**
	 * Creates an instance of IWWColorScaleLayer (ie ColorScaleLayer)
	 */
	IWWIsobathLayer createIsobathLayer(String name, GeoBox geoBox, Supplier<File> demFileSupplier);

	/**
	 * Creates an instance of IWWSwathLayer (ie SwathLayer)
	 * 
	 * @param swathCountInSelection Nb of swath expected by selection
	 */
	IWWSwathSelectionLayer createSwathLayer(String name, ProjectionSettings projection, int swathCountInSelection,
			GeoBox geoBox);

	/**
	 * Creates an instance of {@link IWWNavigationLayer} (ie NavigationLayer).
	 */
	IWWNavigationLayer createNavigationLayer(INavigationDataSupplier navigationDataSupplier);

	/**
	 * Creates an instance of {@link IWWNavigationLayer} with {@link WWNavigationLayerParameters}.
	 */
	IWWNavigationLayer createNavigationLayer(INavigationDataSupplier navigationDataSupplier,
			WWNavigationLayerParameters parameters);

	/**
	 * Creates an instance of IWWWcVolumicLayer (ie WCVolumicLayer)
	 */
	IWWWaterColumnVolumicLayer createWCLayer(ISounderNcInfo info);

	/**
	 * Creates an instance of IWWWcVolumicLayer (ie WCVolumicLayer)
	 */
	Optional<IWWWaterColumnVolumicLayer> createNativeWCLayer(ISounderNcInfo info);

	/**
	 * Creates an instance of {@link IWWTextureLayer}
	 */
	IWWTextureLayer createWWTextureLayer(String name, IWWTextureData textureData,
			WWTextureDisplayParameters textureDisplayParameters);

	/**
	 * Creates an instance of {@link IWWComputableTextureLayer} with a {@link IWWTextureData} supplier (texture will be
	 * compute only on demand).
	 */
	IWWComputableTextureLayer createWWComputableTextureLayer(String name,
			Function<ComputableTextureParameters, IWWTextureData> textureDataSupplier,
			WWTextureDisplayParameters textureDisplayParameters, ComputableTextureParameters defaultComputeParameters);

	/**
	 * Creates an instance of {@link IWWMultiTextureLayer}
	 */
	IWWMultiTextureLayer createWWMultiTextureLayer(String name, Function<String, IWWTextureData> textureDataByLayer,
			Map<String, WWTextureDisplayParameters> textureDisplayParametersByLayer);

	/**
	 * Creates an instance of {@link IWWPlayableTextureLayer}
	 */
	IWWPlayableTextureLayer createWWPlayableTextureLayer(WWPlayableTextureInitParameters playableTextureInitParameters,
			IntFunction<IWWTextureData> textureDataByIndex, WWTextureDisplayParameters textureDisplayParameters);

	/**
	 * Creates an instance of {@link IWWPlayableMultiTextureLayer}
	 */
	IWWPlayableMultiTextureLayer createWWPlayableMultiTextureLayer(String name, int sliceCount,
			BiFunction<String, Integer, IWWTextureData> textureDataByLayerAndIndex,
			Map<String, WWTextureDisplayParameters> textureDisplayParametersByLayer);

	/**
	 * Creates an instance of IWWShapefileLayer (ie ShpLayer)
	 */
	IWWShapefileLayer createShapefileLayer(IWWShapefileLayer.Data data);

	/**
	 * Creates an instance of IWWVerticalImageLayer (ie Layer for SEG-Y files)
	 */
	IWWVerticalImageLayer createVerticalImageLayer(IWWVerticalImageLayer.Data data);

	/**
	 * Creates an instance of IWWRenderableLayer
	 */
	IWWRenderableLayer createRenderableLayer();

	/**
	 * Creates an instance of IWWRenderableLayer
	 */
	IWWGeoBoxLayer createGeoBoxLayer(GeoBox geoBox);

	/**
	 * Creates an instance of IWWMarkerLayer
	 */
	<T extends IMarker> IWWMarkerLayer createMarkerLayer(IMarkerFileInfo<T> markerFileInfo);

	/**
	 * Creates an instance of IWWMarkerLayer
	 */
	IWWMarkerLayer createWaterColumnMarkerLayer(IMarkerFileInfo<IWCMarker> markerFileInfo);

	/**
	 * Creates an instance of IWWMarkerLayer
	 */
	IWWPointCloudLayer createPointCloudLayer(IPointCloudFileInfo pointCloudFileInfo, PointCloudFieldInfo fieldInfo);

	/**
	 * Creates an instance of IWWQuadSelectionLayer
	 */
	IWWQuadSelectionLayer createQuadSelectionLayer(Projection projection, Color selectionColor, int selectionLineWidth);

	/**
	 * Creates an instance of IWWPolygonLayer
	 */
	IWWPolygonLayer createPolygonLayer(PolygonFileInfo polygon);

	/**
	 * Creates an instance of {@link IWWCurrentLayer} .
	 */
	IWWCurrentLayer createCurrentLayer(ICurrentData currentData);

	/**
	 * Creates an instance of IWWAnnotationLayer containing textual annotations
	 */
	IWWAnnotationLayer createAnnotationLayer(IFileInfo fileInfo);

	/**
	 * Creates an instance of IWWLabelLayer containing textual annotations
	 */
	IWWLabelLayer createLabelLayer(IFileInfo fileInfo);

	/**
	 * Creates an instance of {@link IWWUsblLayer}.
	 */
	IWWUsblLayer createUsblTargetLayer(UsblFileInfo usblFileInfo);

	/** Dtm characteristics */
	static class RasterFeatures {

		/** Where to generate the file */
		public final File filePath;
		/** Kind of DTM. See RasterInfo.RASTER_* */
		public final String rasterDataType;
		/** Mercator GeoBox using projection */
		public final GeoBox geoBox;

		/** number of rows */
		public final int rowCount;
		/** number of columns */
		public final int columnCount;
		/** Missing value */
		public final double noDataValue;

		/**
		 * Constructor
		 */
		public RasterFeatures(File filePath, String rasterDataType, GeoBox geoBox, int rowCount, int columnCount,
				double noDataValue) {
			this.filePath = filePath;
			this.rasterDataType = rasterDataType;
			this.geoBox = geoBox;
			this.rowCount = rowCount;
			this.columnCount = columnCount;
			this.noDataValue = noDataValue;
		}
	}
}
