package fr.ifremer.globe.ui.service.worldwind.layer.annotation;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.file.annotation.Annotation;
import fr.ifremer.globe.core.model.file.pointcloud.IPoint;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWRenderableLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing shape annotations.
 */
public interface IWWAnnotationLayer extends IWWRenderableLayer {

	/** Accept an new annotation to render */
	void acceptAnnotation(Annotation annotation);

	/** Accept a point to render as an annotation */
	void acceptLabelAnnotation(IPoint point);

	/** @return the parameters used to render the layer */
	AnnotationParameters getParameters();

	/** Set the parameters used to render the layer */
	void setParameters(AnnotationParameters parameters);

	/** Returns the layer of labels */
	IWWLabelLayer getLabelLayer();

	/** Returns the layer of markers if any */
	Optional<IWWMarkerLayer> getMarkerLayer();

	@Override
	default String getShortName() {
		return "Annotations";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.of(Icons.ANNOTATION.toImage());
	}

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

}
