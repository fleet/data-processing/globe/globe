/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.basic;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWVerticalImageLayer.Data;
import gov.nasa.worldwind.geom.Position;

/**
 * Basic vertical image layer data
 */
public class WWVerticalImageLayerData implements Data {

	protected final String name;
	protected BufferedImage image;
	protected final DoubleRange minMaxValues;
	protected final List<Position> topPoints = new ArrayList<>();
	protected final List<Position> bottomPoints = new ArrayList<>();

	/**
	 * Constructor
	 */
	public WWVerticalImageLayerData(String name, BufferedImage image, DoubleRange minMaxValues) {
		this.name = name;
		this.image = image;
		this.minMaxValues = minMaxValues;
	}

	/**
	 * @return the {@link #name}
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @return the {@link #image}
	 */
	@Override
	public BufferedImage getImage() {
		return image;
	}

	/**
	 * @param image the {@link #image} to set
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
	}

	/**
	 * @return the {@link #minMaxValues}
	 */
	@Override
	public DoubleRange getMinMaxValues() {
		return minMaxValues;
	}

	/**
	 * @return the {@link #topPoints}
	 */
	@Override
	public List<Position> getTopPoints() {
		return topPoints;
	}

	/**
	 * @return the {@link #bottomPoints}
	 */
	@Override
	public List<Position> getBottomPoints() {
		return bottomPoints;
	}
}
