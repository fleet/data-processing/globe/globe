package fr.ifremer.globe.ui.service.worldwind.tile;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.math.Range;
import org.eclipse.core.runtime.IProgressMonitor;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Builder of a producer of tiles
 */
public interface ITilesProducerBuilder {

	/** Folder of Nasa cache */
	ITilesProducerBuilder setTilesLocation(File file);

	/** Folder's name of tiles */
	ITilesProducerBuilder setDatasetName(String datasetName);

	/** Number of level. Minimum 1. When > 1, generates a pyramid */
	ITilesProducerBuilder setNumLevels(int numLevels);

	/** Min and max values in this original raster (for computing palette) */
	ITilesProducerBuilder setMinMax(Range minMax);

	/** Unit of min and max values */
	ITilesProducerBuilder setUnit(String unit);

	/** Set the geobox to prevent its computation */
	ITilesProducerBuilder setGeoBox(GeoBox geoBox);

	/**
	 * Launch...
	 */
	List<Document> run(IProgressMonitor progress) throws GIOException;

}