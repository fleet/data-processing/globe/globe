package fr.ifremer.globe.ui.service.worldwind.layer.navigation;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;

/**
 * Instance of layer for representing a navigation line.
 */
public interface IWWNavigationLayer extends IWWLayer, Supplier<ColorContrastModel> {

	@Override
	default String getShortName() {
		return Optional.ofNullable((String) getValue(SHORT_NAME)).orElse("Navigation");
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.NAVIGATION.toImage()));
	}

	/** How to render the navigation */
	public enum DisplayMode {
		LINE, POINT
	}

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	@Override
	default ColorContrastModel get() {
		return getDisplayParameters().colorContrastModel();
	}

	/**
	 * Recomputes the navigation path from {@link INavigationDataSupplier}.
	 */
	void reload();

	/**
	 * @return the current {@link WWNavigationLayerParameters} of this layer.
	 */
	WWNavigationLayerParameters getDisplayParameters();

	/**
	 * Applies new {@link WWNavigationLayerParameters}.
	 */
	void setDisplayParameters(WWNavigationLayerParameters displayParameters);

	/**
	 * @return {@link List} of displayable {@link NavigationMetadataType}
	 */
	default List<NavigationMetadataType<?>> getMetadataTypes() {
		return getColorContrastModelByMetadataType().keySet().stream().toList();
	}

	/**
	 * @return {@link ColorContrastModel} for the specified {@link NavigationMetadataType}.
	 */
	ColorContrastModel getColorContrastModel(NavigationMetadataType<?> metadataType);

	/**
	 * @return {@link Map} of {@link ColorContrastModel} by {@link NavigationMetaData}.
	 */
	Map<NavigationMetadataType<?>, ColorContrastModel> getColorContrastModelByMetadataType();


	/**
	 * @return associated {@link IWWColorScaleLayer} (used to display scale of colored navigation line)
	 */
	Optional<IWWColorScaleLayer> getColorScaleLayer();

}
