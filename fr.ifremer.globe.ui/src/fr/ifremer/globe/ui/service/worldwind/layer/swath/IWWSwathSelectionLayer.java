/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer.swath;

import java.util.List;
import java.util.function.Consumer;

import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;

/**
 * Instance of layer for representing swaths
 */
public interface IWWSwathSelectionLayer extends IWWLayer {

	/** Generates swath lines for all the specified SounderDataContainers */
	void addSwathsFrom(List<SounderDataContainer> dataContainers);

	/** Ask layer to increment the swath idx of all selections */
	void shiftSelection(int increment);

	/**
	 * @return the {@link #parameters}
	 */
	WWSwathLayerParameters getParameters();

	/**
	 * @param parameters the {@link #parameters} to set
	 */
	void setParameters(WWSwathLayerParameters parameters);

	/** Set the listener to catch new selections */
	void setListener(Consumer<WWSwathSelection> listener);

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

}
