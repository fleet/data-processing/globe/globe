package fr.ifremer.globe.ui.service.worldwind.texture.impl;

import com.google.gson.annotations.JsonAdapter;

import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import gov.nasa.worldwind.geom.Position;

/**
 * Simple implementation of IWWTextureCoordinate
 */
@JsonAdapter(WWTextureCoordinateAdapter.class)
public record WWTextureCoordinate(float s, float t, Position position) implements IWWTextureCoordinate {

	@Override
	public float getS() {
		return s;
	}

	@Override
	public float getT() {
		return t;
	}

	@Override
	public float getX() {
		return 0;
	}

	@Override
	public float getY() {
		return 0;
	}

	@Override
	public Position getPosition() {
		return position;
	}

	@Override
	public Position getPosition(double verticalOffset, double verticalExageration) {
		return Position.fromDegrees(position.getLatitude().degrees, position.getLongitude().degrees,
				verticalExageration * (position.getElevation() + verticalOffset));
	}

}
