package fr.ifremer.globe.ui.service.worldwind.layer.navigation;

import java.util.Optional;

import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer.DisplayMode;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.globe.utils.map.TypedMap;

/**
 * This record contains parameters used to display a {@link IWWNavigationLayer}.
 */
public record WWNavigationLayerParameters(DisplayMode displayMode, //
		boolean displayGroundPath, //
		int lineWidth, //
		boolean displayPoints, //
		float opacity, boolean isVariableColor, //
		GColor absolutePathColor, //
		Optional<NavigationMetadataType<?>> metadataUsedToGetColor, //
		ColorContrastModel colorContrastModel, //
		/** Decimation **/
		boolean decimationEnabled, int decimationPercentage,
		/** Vertical offset model */
		boolean verticalOffsetEnabled, float verticalOffset) {

	/**
	 * Default values
	 */
	public WWNavigationLayerParameters() {
		this(DisplayMode.LINE, false, 2, false, 1, false, GColor.RED, Optional.empty(),
				new ColorContrastModelBuilder().build(), true, 30, false, 0f);
	}

	/** Key used to save this bean in session */
	public static final TypedKey<WWNavigationLayerParameters> SESSION_KEY = new TypedKey<>(
			"Navigation_Layer_Display_Parameters");
	public static final TypedKey<String> SESSION_KEY_METADATA = new TypedKey<>(
			"Navigation_Layer_Display_Parameters_Metadata");

	/** Duplicates this WWNavigationLayerParameters with an other value of displayMode */
	public WWNavigationLayerParameters withDisplayMode(DisplayMode displayMode) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				isVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercentage, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWNavigationLayerParameters with an other value of displayPoints */
	public WWNavigationLayerParameters withDisplayPoints(boolean displayPoints) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				isVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercentage, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWNavigationLayerParameters with an other value of lineWidth */
	public WWNavigationLayerParameters withLineWidth(int lineWidth) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				isVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercentage, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWNavigationLayerParameters with an other value of displayGroundPath */
	public WWNavigationLayerParameters withDisplayGroundPath(boolean displayGroundPath) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				isVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercentage, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWNavigationLayerParameters with an other value of variable color */
	public WWNavigationLayerParameters withVariableColor(boolean enableVariableColor,
			Optional<NavigationMetadataType<?>> metadataUsedToGetColor) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				enableVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercentage, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWNavigationLayerParameters with an other value of NavigationMetadataType */
	public WWNavigationLayerParameters withNavigationMetadataType(
			Optional<NavigationMetadataType<?>> metadataUsedToGetColor) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				isVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercentage, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWNavigationLayerParameters with an other value of ColorContrastModel */
	public WWNavigationLayerParameters withColorContrastModel(ColorContrastModel colorContrastModel) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				isVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercentage, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWNavigationLayerParameters with an other value of decimation */
	public WWNavigationLayerParameters withDecimation(boolean decimationEnabled, int decimationPercent) {
		return new WWNavigationLayerParameters(displayMode, displayGroundPath, lineWidth, displayPoints, opacity,
				isVariableColor, absolutePathColor, metadataUsedToGetColor, colorContrastModel, decimationEnabled,
				decimationPercent, verticalOffsetEnabled, verticalOffset);
	}

	/** Wrap this bean to a TypedMap with SESSION_KEY as key and SESSION_KEY_METADATA as key for metadata part */
	public TypedMap toTypedMap() {
		return TypedMap.of(SESSION_KEY.bindValue(this.withNavigationMetadataType(Optional.empty())), //
				SESSION_KEY_METADATA.bindValue(metadataUsedToGetColor.map(meta -> meta.name).orElse("")));
	}
}
