/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.List;
import java.util.function.Consumer;

import fr.ifremer.globe.core.model.geo.GeoPoint;

/**
 * Instance of layer managing a quad selection.
 */
public interface IWWQuadSelectionLayer extends IWWRenderableLayer {

	/** True when selection is started */
	public boolean isSelectionArmed();

	/** Set the quad selection */
	public void setSelection(GeoPoint geoPoint1, GeoPoint geoPoint2, GeoPoint geoPoint3, GeoPoint geoPoint4);

	/** Set the listener to catch new selections */
	void setListener(Consumer<List<GeoPoint>> listener);

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
