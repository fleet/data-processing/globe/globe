/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.pointcloud;

import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget.Model;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Parameters of a IWWPointCloudLayer
 */
public record PointCloudParameters(
		/** How to render the points */
		DisplayMode displayMode,

		/** Color & contrast parameters **/
		boolean colorMapInverted, //
		int colorMapIndex, //
		float minContrast, //
		float maxContrast, //

		/** Point size */
		PointSizeMode pointSizeMode, //
		int constantSize, //
		Model variablePointSizeModel, //

		float opacity) {

	/** Key used to save this bean in session */
	public static final TypedKey<PointCloudParameters> SESSION_KEY = new TypedKey<>("Point_cloud_parameters");

	/**
	 * New instance of PointCloudParameters with an other DisplayMode
	 */
	public PointCloudParameters withDisplayMode(DisplayMode displayMode) {
		return new PointCloudParameters(displayMode, colorMapInverted, colorMapIndex, minContrast, maxContrast,
				pointSizeMode, constantSize, variablePointSizeModel, opacity);
	}

	/**
	 * New instance of PointCloudParameters with an other color parameters
	 */
	public PointCloudParameters withColor(boolean colorMapInverted, int colorMapIndex, float minContrast,
			float maxContrast, float opacity) {
		return new PointCloudParameters(displayMode, colorMapInverted, colorMapIndex, minContrast, maxContrast,
				pointSizeMode, constantSize, variablePointSizeModel, opacity);
	}

	/**
	 * New instance of PointCloudParameters with an other marker size
	 */
	public PointCloudParameters withPointSizeMode(PointSizeMode pointSizeMode) {
		return new PointCloudParameters(displayMode, colorMapInverted, colorMapIndex, minContrast, maxContrast,
				pointSizeMode, constantSize, variablePointSizeModel, opacity);
	}

	/**
	 * New instance of PointCloudParameters with an other marker size
	 */
	public PointCloudParameters withConstantSize(int constantSize) {
		return new PointCloudParameters(displayMode, colorMapInverted, colorMapIndex, minContrast, maxContrast,
				pointSizeMode, constantSize, variablePointSizeModel, opacity);
	}

	/**
	 * New instance of PointCloudParameters with an other variablePointSizeModel value
	 */
	public PointCloudParameters withVariablePointSizeModel(Model variablePointSizeModel) {
		return new PointCloudParameters(displayMode, colorMapInverted, colorMapIndex, minContrast, maxContrast,
				pointSizeMode, constantSize, variablePointSizeModel, opacity);
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<PointCloudParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

}
