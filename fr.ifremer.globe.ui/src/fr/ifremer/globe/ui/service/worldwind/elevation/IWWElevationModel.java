/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.elevation;

import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.globe.utils.map.TypedMap;
import gov.nasa.worldwind.globes.ElevationModel;

/**
 * WW ElevationModel with interpolation feature
 */
public interface IWWElevationModel extends ElevationModel {

	/**
	 * Called by the session to retrieve all the savable parameters of this ElevationModel<br>
	 *
	 * A layer implementing this method must also implement setSessionParameter to be able to retrieve the values of the
	 * parameters saved in session.
	 */
	TypedMap describeParametersForSession();

	/**
	 * Called by the session when the parameters have been restored.
	 */
	void setSessionParameter(TypedMap parameters);

	/** @return the parameters */
	ElevationModelParameters getElevationModelParameters();

	/** Set the parameters */
	void setElevationModelParameters(ElevationModelParameters parameters);

	/**
	 * Parameters of a IWWElevationModel
	 */
	public class ElevationModelParameters {
		/** Key used to save this bean in session */
		public static final TypedKey<ElevationModelParameters> SESSION_KEY = new TypedKey<>(
				"Elevation_Model_Parameters");

		public final boolean interpolate;
		public final double offset;

		/**
		 * Constructor
		 */
		public ElevationModelParameters(boolean interpolate, double offset) {
			super();
			this.interpolate = interpolate;
			this.offset = offset;
		}

		public ElevationModelParameters withOffset(double offset) {
			return new ElevationModelParameters(interpolate, offset);
		}

		/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
		public TypedEntry<ElevationModelParameters> toTypedEntry() {
			return SESSION_KEY.bindValue(this);
		}

	}
}
