package fr.ifremer.globe.ui.service.worldwind.layer.annotation;

import fr.ifremer.globe.core.utils.color.GColor;
import gov.nasa.worldwind.geom.Position;

public interface ILabelProperties {

	/** Getter of {@link #label} */
	String getLabel();

	/** Getter of {@link #position} */
	Position getPosition();

	/** Getter of {@link #color} */
	GColor getColor();

	/** Getter of {@link #userFacing} */
	boolean isUserFacing();

	/** Setter of {@link #label} */
	void setLabel(String label);

	/** Setter of {@link #position} */
	void setPosition(Position position);

	/** Setter of {@link #color} */
	void setColor(GColor color);

	/** Setter of {@link #userFacing} */
	void setUserFacing(boolean userFacing);

}