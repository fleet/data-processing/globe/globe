/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer;

import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.current.IWWCurrentLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.swath.IWWSwathSelectionLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;

/**
 * Operation to visit a IWWLayer
 */
public interface IWWLayerOperation {

	/** Perform the operation for an instance of IWWTerrainLayer */
	IWWLayerOperation accept(IWWTerrainLayer layer);

	/** Perform the operation for an instance of IWWColorScaleLayer (ie ColorScaleLayer) */
	IWWLayerOperation accept(IWWColorScaleLayer layer);

	/** Perform the operation for an instance of IWWTiledImageLayer (ie BasicTiledImageLayer) */
	IWWLayerOperation accept(IWWTiledImageLayer layer);

	/** Perform the operation for an instance of IWWIsobathLayer (ie IsobathLayer) */
	IWWLayerOperation accept(IWWIsobathLayer layer);

	/** Perform the operation for an instance of IWWQuadSelectionLayer */
	IWWLayerOperation accept(IWWQuadSelectionLayer layer);

	/** Perform the operation for an instance of IWWSwathLayer */
	IWWLayerOperation accept(IWWSwathSelectionLayer layer);

	/** Perform the operation for an instance of IWWNavigationLayer (ie NavigationLayer) */
	IWWLayerOperation accept(IWWNavigationLayer layer);

	/** Perform the operation for an instance of IWWWcVolumicLayer (ie WCVolumicLayer) */
	IWWLayerOperation accept(IWWWaterColumnVolumicLayer layer);

	/** Perform the operation for an instance of IWWShapefileLayer (ie ShpLayer) */
	IWWLayerOperation accept(IWWShapefileLayer layer);

	/** Perform the operation for an instance of {@link IWWTextureLayer} */
	IWWLayerOperation accept(IWWTextureLayer layer);

	/** Perform the operation for an instance of {@link IWWComputableTextureLayer} */
	IWWLayerOperation accept(IWWComputableTextureLayer layer);

	/** Perform the operation for an instance of {@link IWWMultiTextureLayer} */
	IWWLayerOperation accept(IWWMultiTextureLayer layer);

	/** Perform the operation for an instance of {@link IWWPlayableTextureLayer} */
	IWWLayerOperation accept(IWWPlayableTextureLayer layer);

	/** Perform the operation for an instance of {@link IWWPlayableMultiTextureLayer} */
	IWWLayerOperation accept(IWWPlayableMultiTextureLayer layer);

	/** Perform the operation for an instance of {@link IWWRenderableLayer} */
	IWWLayerOperation accept(IWWRenderableLayer layer);

	/** Perform the operation for an instance of {@link IWWTerrainWithPaletteLayer} */
	IWWLayerOperation accept(IWWTerrainWithPaletteLayer layer);

	/**
	 * Perform the operation for an instance of IWWVerticalImageLayer (ie ReflectivityLayer or layer for SEG-Y files)
	 */
	IWWLayerOperation accept(IWWVerticalImageLayer layer);

	/** Perform the operation for an instance of IWWGeoBoxLayer */
	IWWLayerOperation accept(IWWGeoBoxLayer layer);

	/** Perform the operation for an instance of IWWDtmLayer (ie ReflectivityLayer) */
	IWWLayerOperation accept(IWWWallLayer layer);

	/** Perform the operation for an instance of IWWMarkerLayer (ie MultiMarkerLayer) */
	IWWLayerOperation accept(IWWMarkerLayer layer);

	/** Perform the operation for an instance of {@link IWWPointCloudLayer} */
	IWWLayerOperation accept(IWWPointCloudLayer iwwPointCloudLayer);

	/** Perform the operation for an instance of {@link IWWOldMultiTextureLayer} */
	IWWLayerOperation accept(IWWOldMultiTextureLayer layer);

	/** Perform the operation for an instance of {@link IWWPolygonLayer} */
	IWWLayerOperation accept(IWWPolygonLayer layer);

	/** Perform the operation for an instance of IWWCurrentLayer */
	IWWLayerOperation accept(IWWCurrentLayer layer);

	/** Perform the operation for an instance of IWWAnnotationLayer */
	IWWLayerOperation accept(IWWAnnotationLayer layer);

	/** Perform the operation for an instance of IWWLabelLayer */
	IWWLayerOperation accept(IWWLabelLayer layer);

	/** Perform the operation for an instance of IWWUsblTargetLayer */
	IWWLayerOperation accept(IWWUsblLayer layer);

}
