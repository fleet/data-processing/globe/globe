package fr.ifremer.globe.ui.service.worldwind.layer;

/**
 * Instance of layer for representing a Wall layer.
 */
public interface IWWWallLayer extends IWWLayer {

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
