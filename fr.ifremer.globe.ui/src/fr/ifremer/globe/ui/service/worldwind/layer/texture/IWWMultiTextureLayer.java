/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.texture;

import java.util.Set;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;

/**
 * Instance of layer for representing a fly texture.
 */
public interface IWWMultiTextureLayer extends IWWTextureLayer {

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	Set<String> getAvailableDataLayers();

	String getSelectedDataLayer();

	void setSelectedDataLayer(String selectedDataLayer);
}
