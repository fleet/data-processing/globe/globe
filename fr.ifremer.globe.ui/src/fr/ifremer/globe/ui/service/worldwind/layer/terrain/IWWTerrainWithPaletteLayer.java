/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer.terrain;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Layer instance for rendering a raster associated with a color palette
 */
public interface IWWTerrainWithPaletteLayer extends IWWLayer {

	@Override
	default Optional<Image> getIcon() {
		return Optional.of(Icons.RASTER.toImage());
	}

	/** @return the parameters used to render the raster */
	TerrainWithPaletteParameters getTextureParameters();

	/** Set the parameters used to render the rater */
	void setTextureParameters(TerrainWithPaletteParameters parameters);

	/** Set to true when tiles have been regenerated for example */
	void setClearTextureCache(boolean clearFlag);

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
