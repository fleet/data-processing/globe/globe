/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.texture.playable;

import java.util.function.IntFunction;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;

/**
 * Instance of layer for representing a fly texture.
 */
public interface IWWPlayableTextureLayer extends IWWTextureLayer {

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	/**
	 * Creates and initializes a {@link IndexedPlayer}.
	 * 
	 * @return
	 */
	default IndexedPlayer initPlayer(int sliceCount, IntFunction<String> sliceLabelProvider) {
		// initialize player
		IndexedPlayer player = new IndexedPlayer("Player of " + getName(), 0, sliceCount - 1);
		player.subscribe(indexedPlayerData -> buildTextureRenderable());
		player.setLabelProvider(sliceLabelProvider);
		return player;
	}

	/**
	 * @return the {@link IndexedPlayer} to move on this texture slices
	 */
	IndexedPlayer getPlayer();
}
