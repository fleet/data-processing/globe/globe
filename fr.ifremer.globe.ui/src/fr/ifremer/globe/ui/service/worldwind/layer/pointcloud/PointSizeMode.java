package fr.ifremer.globe.ui.service.worldwind.layer.pointcloud;

/** How to compute point size */
public enum PointSizeMode {
	CONSTANT,
	VARIABLE
}
