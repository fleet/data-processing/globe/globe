package fr.ifremer.globe.ui.service.worldwind.layer.texture.playable;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Parameters to initialize a WW Texture layer with a Player <br>
 * An instance of this class is immutable
 */
public record WWPlayableTextureInitParameters(//
		String name, //
		int sliceCount, //
		List<Instant> sliceTime, // Date time of slice
		Optional<String> syncKey) {
}