/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer.marker;

import java.util.List;
import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing markers
 */
public interface IWWMarkerLayer extends IWWLayer {

	@Override
	default String getShortName() {
		return "Markers";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.MARKER.toImage()));
	}

	/**
	 * @return all markers managed by this layer
	 */
	<M extends IMarker> List<M> getMarkers(Class<M> typeOfMarkers);

	/**
	 * @return {@link WWMarkerLayerParameters} of this texture.
	 */
	WWMarkerLayerParameters getParameters();

	/**
	 * Set a new {@link WWMarkerLayerParameters} for this texture.
	 */
	void setParameters(WWMarkerLayerParameters parameters);

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
