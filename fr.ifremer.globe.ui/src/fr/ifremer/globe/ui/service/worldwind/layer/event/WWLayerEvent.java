package fr.ifremer.globe.ui.service.worldwind.layer.event;

import org.eclipse.e4.ui.workbench.UIEvents;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;

/**
 * This class provides event around {@link IWWLayer}.
 */
public class WWLayerEvent {

	/**
	 * Base to build topic strings.
	 */
	public static final String TOPIC_BASE = "fr/ifremer/globe/ui/service/worldwind/layer";

	/**
	 * Topic to inform that a layer has been selected.
	 * 
	 * Associated object : "{@link IWWLayer}...".
	 */
	public static final String TOPIC_LAYER_SELECTED = TOPIC_BASE + UIEvents.TOPIC_SEP + "layer_selected";

}
