package fr.ifremer.globe.ui.service.worldwind.texture.impl;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.utils.date.DateUtils;

public class WWTextureData implements IWWTextureData {

	/** Texture properties & data **/
	private final int width;
	private final int height;
	private final ByteBuffer buffer;
	private int gridHeight = 2;
	private final List<IWWTextureCoordinate> coordinates;
	private final boolean isVertical;

	/** Optional metadata **/
	private Map<String, String> metadata = Collections.emptyMap();

	/**
	 * Map of metadata providers (provides a value "String" for each cell)
	 */
	private Map<String, BiFunction<Integer, Integer, String>> cellMetadataProviders;

	/**
	 * Constructors
	 *
	 * @param width : texture width
	 * @param height : texture height
	 * @param buffer : data buffer
	 * @param coordinates : texture coordinates
	 * @param isVertical : contains a vertical/longitudinal texture
	 */
	public WWTextureData(int width, int height, ByteBuffer buffer, List<IWWTextureCoordinate> coordinates,
			boolean isVertical) {
		this.width = width;
		this.height = height;
		this.buffer = buffer;
		this.coordinates = coordinates;
		this.isVertical = isVertical;
	}

	/**
	 * Constructors
	 *
	 * @param width : texture width
	 * @param height : texture height
	 * @param gridHeight : coordinates grid height
	 * @param buffer : data buffer
	 * @param coordinates : texture coordinates
	 * @param isVertical : contains a vertical/longitudinal texture
	 */
	public WWTextureData(int width, int height, int gridHeight, ByteBuffer buffer,
			List<IWWTextureCoordinate> coordinates, boolean isVertical) {
		this.width = width;
		this.height = height;
		this.gridHeight = gridHeight;
		this.buffer = buffer;
		this.coordinates = coordinates;
		this.isVertical = isVertical;
	}

	@Override
	public IWWTextureData buildSubTextureData(int width, int height, int gridHeight, ByteBuffer buffer,
			List<IWWTextureCoordinate> coordinates) {
		var subTexture = new WWTextureData(width, height, gridHeight, buffer, coordinates, this.isVerticalTexture());
		subTexture.setMetadata(this.getMetadata());
		return subTexture;
	}

	// GETTERS & SETTERS

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public ByteBuffer getBuffer() {
		return buffer;
	}

	@Override
	public int getCoordGridHeight() {
		return gridHeight;
	}

	@Override
	public int getCoordGridWidth() {
		return getCoordinates().size() / gridHeight;
	}

	@Override
	public List<IWWTextureCoordinate> getCoordinates() {
		return coordinates;
	}

	@Override
	public float getValue(int row, int col) {
		return getBuffer().getFloat((row * getWidth() + col) * Float.BYTES);
	}

	@Override
	public Optional<Instant> getDate(int row, int col) {
		Optional<Instant> result = Optional.empty();

		Optional<String> optStringDate = Optional.ofNullable(getMetadata().get(IWWTextureData.DATE_METADATA_KEY));
		if (optStringDate.isEmpty())
			optStringDate = Optional.ofNullable(getCellMetadata(row, col).get(IWWTextureData.DATE_METADATA_KEY));

		if (optStringDate.isPresent())
			result = DateUtils.parseInstant(optStringDate.get());

		return result;
	}

	@Override
	public boolean isVerticalTexture() {
		return isVertical;
	}

	@Override
	public double getVerticalOffset() {
		return Double.valueOf(getMetadata().getOrDefault(IWWTextureData.VERTICAL_OFFSET_METADATA_KEY, "0"));
	}

	@Override
	public double getLongitudinalOffset() {
		return Double.valueOf(getMetadata().getOrDefault(IWWTextureData.LONGITUDINAL_OFFSET_METADATA_KEY, "0"));
	}

	@Override
	public Map<String, String> getMetadata() {
		return metadata;
	}

	@Override
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}

	@Override
	public Map<String, String> getCellMetadata(int row, int col) {
		Map<String, String> cellMetadata = new HashMap<>();
		if (cellMetadataProviders != null) {
			cellMetadataProviders.forEach((key, valueProvider) -> {
				cellMetadata.put(key, valueProvider.apply(row, col));
			});
		}
		return cellMetadata;
	}

	@Override
	public void setMetadataProviders(Map<String, BiFunction<Integer, Integer, String>> metadataProviders) {
		cellMetadataProviders = metadataProviders;
	}

	@Override
	public String getSynchronizationKey() {
		return getMetadata().getOrDefault(IWWTextureData.SYNCHRONIZATION_KEY_METADATA_KEY, "");
	}

	@Override
	public String getSynchronizationLabel() {
		return getMetadata().getOrDefault(IWWTextureData.SYNCHRONIZATION_LABEL_METADATA_KEY, "");
	}

}
