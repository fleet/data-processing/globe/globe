/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer.terrain;

import java.util.Optional;
import java.util.function.Supplier;

import org.apache.commons.lang.math.Range;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;

/**
 * Instance of layer for representing an elevation layer (ie ShaderElevationLayer)
 */
public interface IWWTerrainLayer extends IWWLayer, Supplier<ColorContrastModel> {

	@Override
	default String getShortName() {
		return getType();
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.of(Icons.RASTER.toImage());
	}

	/** @return the parameters used to render the DTM */
	TerrainParameters getTextureParameters();

	/** Set the parameters used to render the DTM */
	void setTextureParameters(TerrainParameters parameters);

	/** Set to true when tiles have been regenerated for example */
	void setClearTextureCache(boolean clearFlag);

	/** Kind of data. May be one of RasterInfo.RASTER_* */
	String getType();

	/** @return data cache name if available. **/
	default Optional<String> getDataCacheName() {
		return Optional.empty();
	}

	/** Min and max values currently visible in the Geographic view */
	Optional<Range> getVisibleValueRange();

	/** Return true when this layer is currently visible in the Geographic view, completely or partially */
	boolean isVisibleInView();

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	@Override
	default ColorContrastModel get() {
		return new ColorContrastModelBuilder(this, false, new org.apache.commons.collections.primitives.ArrayIntList(0))
				.build();
	}
}
