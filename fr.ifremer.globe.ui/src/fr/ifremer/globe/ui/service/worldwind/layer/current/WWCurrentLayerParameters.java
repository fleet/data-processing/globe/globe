package fr.ifremer.globe.ui.service.worldwind.layer.current;

import java.util.Optional;

import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastModelBuilder;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.globe.utils.map.TypedMap;

/**
 * This record contains parameters used to display a {@link IWWCurrentLayer}.
 */
public record WWCurrentLayerParameters(int width, //
		float opacity, //
		GColor absoluteColor, //
		boolean isVariableColor, Optional<NavigationMetadataType<?>> metadataUsedToGetColor, //
		ColorContrastModel colorContrastModel, //
		int scaleFactor,
		/** Vertical offset model */
		boolean verticalOffsetEnabled, //
		float verticalOffset) {

	/** Key used to save this bean in session */
	public static final TypedKey<WWCurrentLayerParameters> SESSION_KEY = new TypedKey<>(
			"Current_Layer_Display_Parameters");
	public static final TypedKey<String> SESSION_KEY_METADATA = new TypedKey<>(
			"Current_Layer_Display_Parameters_Metadata");

	/**
	 * Default values
	 */
	public WWCurrentLayerParameters() {
		this(2, 1, GColor.ORANGE, false, Optional.empty(), new ColorContrastModelBuilder().build(), 30, false, 0f);
	}

	/** Creates a new instance with the specified metadataUsedToGetColor */
	public WWCurrentLayerParameters withColorContrastModel(ColorContrastModel colorContrastModel) {
		return new WWCurrentLayerParameters(width, opacity, absoluteColor, isVariableColor, metadataUsedToGetColor,
				colorContrastModel, scaleFactor, verticalOffsetEnabled, verticalOffset);
	}

	/** Creates a new instance with the specified metadataUsedToGetColor */
	public WWCurrentLayerParameters withMetadataUsedToGetColor(
			Optional<NavigationMetadataType<?>> metadataUsedToGetColor) {
		return new WWCurrentLayerParameters(width, opacity, absoluteColor, isVariableColor, metadataUsedToGetColor,
				colorContrastModel, scaleFactor, verticalOffsetEnabled, verticalOffset);
	}

	/** Wrap this bean to a TypedMap with SESSION_KEY as key and SESSION_KEY_METADATA as key for metadata part */
	public TypedMap toTypedMap() {
		return TypedMap.of(SESSION_KEY.bindValue(this.withMetadataUsedToGetColor(Optional.empty())), //
				SESSION_KEY_METADATA.bindValue(metadataUsedToGetColor.map(meta -> meta.name).orElse("")));
	}

}
