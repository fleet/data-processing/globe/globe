/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.polygon;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing a polygon.
 */
public interface IWWPolygonLayer extends IWWLayer {

	@Override
	default String getShortName() {
		return "Polygon";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.of(Icons.POLYGON.toImage());
	}

	/** @return the PolygonFileInfo edited in the layer */
	PolygonFileInfo getPolygonFileInfo();

	/** @return the parameters used to render the layer */
	PolygonParameters getParameters();

	/** Set the parameters used to render the layer */
	void setParameters(PolygonParameters parameters);

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
