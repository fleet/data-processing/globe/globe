package fr.ifremer.globe.ui.service.worldwind.layer.texture;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;

/**
 * Instance of layer for representing a computable texture.
 */
public interface IWWComputableTextureLayer extends IWWTextureLayer {

	/**
	 * Immutable class for parameters.
	 */
	record ComputableTextureParameters(boolean isEnabled, int min, int max) {
	}

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	/**
	 * @return current {@link ComputableTextureParameters}.
	 */
	ComputableTextureParameters getComputeParameters();

	/**
	 * Sets current {@link ComputableTextureParameters}.
	 */
	void setComputeParameters(ComputableTextureParameters computeParameters);

}
