/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.pointcloud;

import java.util.Optional;
import java.util.function.Supplier;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;

/**
 * Instance of layer for representing a point cloud.
 */
public interface IWWPointCloudLayer extends IWWLayer, Supplier<ColorContrastModel> {

	@Override
	default String getShortName() {
		return "Point cloud";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.SWATH.toImage()));
	}

	/** @return the parameters used to render the layer */
	PointCloudParameters getParameters();

	/** @return field used to feed the layer */
	PointCloudFieldInfo getFieldInfo();

	/** Set the parameters used to render the layer */
	void setParameters(PointCloudParameters parameters);

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
