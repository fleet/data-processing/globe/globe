/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer;

/**
 * Status of IWWLayerProvider.
 */
public class WWLayerProviderStatus {

	/** Provider produces new experimental layers */
	public static final String EXPERIMENTAL = "EXPERIMENTAL";
	/** Provider produces deprecated layers, will soon be replaced by experimental layers */
	public static final String DEPRECATED = "DEPRECATED";
	/** Provider produces nominal and stable layers */
	public static final String NOMINAL = "NOMINAL";

	/** Property use to declare the WWLayerProviderStatus in services */
	public static final String SERVICE_PROPERTY = "LAYER_PROVIDER_STATUS";

	/** Property use to declare the WWLayerProviderStatus in services */
	public static final String EXPERIMENTAL_PROPERTY = SERVICE_PROPERTY + "=" + EXPERIMENTAL;
	/** Property use to declare the WWLayerProviderStatus in services */
	public static final String DEPRECATED_PROPERTY = SERVICE_PROPERTY + "=" + DEPRECATED;

	/**
	 * Constructor
	 */
	private WWLayerProviderStatus() {
	}
}
