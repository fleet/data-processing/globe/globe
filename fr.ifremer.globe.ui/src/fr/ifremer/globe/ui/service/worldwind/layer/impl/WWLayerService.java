/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics.PointCloudRequestInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWLayerPreferences;
import fr.ifremer.globe.ui.service.worldwind.layer.WWLayerProviderStatus;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 *
 */
@Component(name = "globe_ui_service_worldwind_layer", service = IWWLayerService.class)
public class WWLayerService implements IWWLayerService {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(WWLayerService.class);

	@Reference
	IProcessService processService;

	@Reference
	IFileService fileService;

	@Inject
	WWFileLayerStoreModel fileLayerStoreModel;

	/** All registered providers */
	protected List<IWWLayerProvider> providers = new ArrayList<>();

	/** {@inheritDoc} */
	@Override
	public boolean isManaged(ContentType contentType) {
		return providers.stream().anyMatch(layerProvider -> layerProvider.getContentType().contains(contentType));
	}

	/** {@inheritDoc} */
	@Override
	public Optional<WWFileLayerStore> getLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		return providers.stream().filter(provider -> provider.getContentType().contains(fileInfo.getContentType()))
				.map(provider -> {
					Optional<WWFileLayerStore> optLayerStore = Optional.empty();
					try {
						optLayerStore = provider.getFileLayers(fileInfo, silently, monitor);
					} catch (GIOException e) {
						logger.error("Error while getting layer from provider " + provider.getClass().getSimpleName()
								+ " with file '" + fileInfo.getFilename() + "' : " + e.getMessage(), e);
					}
					return optLayerStore;
				}).filter(Optional::isPresent).map(Optional::get).reduce((layerStore1, layerStore2) -> {
					layerStore1.addLayers(layerStore2.getLayers());
					if (layerStore1.getElevationModel() == null && layerStore2.getElevationModel() != null)
						layerStore1.setElevationModel(layerStore2.getElevationModel());
					return layerStore1;
				});
	}

	/** {@inheritDoc} */
	@Override
	public Optional<WWFileLayerStore> getLayers(String filepath, IProgressMonitor monitor) throws GIOException {
		var opFileInfo = fileService.getFileInfo(filepath);
		return opFileInfo.isPresent() ? getLayers(opFileInfo.get(), false, monitor) : Optional.empty();
	}

	/**
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService#getRasterLayers(fr.ifremer.globe.model.services.raster.RasterInfo,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWRasterLayerStore> getRasterLayers(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		return providers.stream().map(provider -> {
			Optional<WWRasterLayerStore> optRasterLayerStore = Optional.empty();
			try {
				optRasterLayerStore = provider.getRasterLayers(rasterInfo, monitor);
			} catch (GIOException e) {
				logger.error("Error while getting layer from provider " + provider.getClass().getSimpleName()
						+ " with file '" + rasterInfo.getFilePath() + "' : " + e.getMessage(), e);
			}
			return optRasterLayerStore;
		}).filter(Optional::isPresent).map(Optional::get).findFirst();
	}

	/** Register a new provider */
	@Reference(name = "WWLayerProvider", cardinality = ReferenceCardinality.MULTIPLE)
	public void add(IWWLayerProvider provider, Map<String, Object> properties) {
		WWLayerPreferences layerPreferences = OsgiUtils.getService(WWLayerPreferences.class);

		String providerStatus = properties
				.getOrDefault(WWLayerProviderStatus.SERVICE_PROPERTY, WWLayerProviderStatus.NOMINAL).toString();
		if (layerPreferences.isUsingExperimentalLayers() && WWLayerProviderStatus.DEPRECATED.equals(providerStatus)) {
			logger.info("Deprecated {} is not registered : {}", IWWLayerProvider.class.getSimpleName(),
					provider.getClass().getName());
			return;
		}
		if (!layerPreferences.isUsingExperimentalLayers()
				&& WWLayerProviderStatus.EXPERIMENTAL.equals(providerStatus)) {
			logger.info("Experimental {} is not registered : {}", IWWLayerProvider.class.getSimpleName(),
					provider.getClass().getName());
			return;
		}

		logger.info("{} registered : {}", IWWLayerProvider.class.getSimpleName(), provider.getClass().getName());
		// Is it the ultimate Gdal InfoSupplier ?
		if (provider.getContentType().contains(ContentType.RASTER_GDAL)) {
			// This is the default supplier.
			// Put it at the end of the list to let the priority of more specific supplier
			providers.add(provider);
		} else {
			providers.add(0, provider);
		}
	}

	/**
	 * Event handler for TOPIC_WW_LAYERS_REQUESTED
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onLayerRequired(@UIEventTopic(UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED) RasterInfo rasterInfo) {
		if (rasterInfo.getContentType() != ContentType.UNDEFINED) { // Undefined in case of Swath Editor
			processService.runInForeground("Layer creation for " + rasterInfo.getDataType(), (monitor, l) -> {
				getRasterLayers(rasterInfo, monitor).ifPresent(fileLayerStoreModel::update);
				return Status.OK_STATUS;
			});
		}
	}

	/**
	 * Event handler for TOPIC_WW_LAYERS_REQUESTED for a PointCloudFieldInfo
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onPointCloudLayerRequired(
			@UIEventTopic(UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED) PointCloudRequestInfo info) {
		processService.runInForeground("Layer creation for " + info.fileInfo().getFilename(), (monitor, l) -> {
			for (IWWLayerProvider layerProvider : providers) {
				List<IWWLayer> result = layerProvider.getPointCloudLayers(info.fileInfo(), info.pointCloudFieldInfo(),
						monitor);
				if (!result.isEmpty()) {
					fileLayerStoreModel.addLayersToStore(info.fileInfo(), result.toArray(new IWWLayer[result.size()]));
					break;
				}
			}
			return Status.OK_STATUS;
		});
	}

	/** {@inheritDoc} */
	@Override
	public WWFileLayerStoreModel getFileLayerStoreModel() {
		return fileLayerStoreModel;
	}

}
