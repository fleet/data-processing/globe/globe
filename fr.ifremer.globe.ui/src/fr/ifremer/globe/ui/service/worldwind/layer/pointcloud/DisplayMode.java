package fr.ifremer.globe.ui.service.worldwind.layer.pointcloud;

/** How to render the points */
public enum DisplayMode {
	MARKER, POINT
}
