/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.application.event.WWLayerDescription;
import gov.nasa.worldwind.globes.ElevationModel;

/**
 * Set of Wordwind layers to render a IFileInfo
 */
public class WWFileLayerStore {

	/** IInfoStore used to generate the ww layers */
	private final IFileInfo fileInfo;

	/** WW layers */
	private final List<IWWLayer> layers = new ArrayList<>();

	/**
	 * Optional layers, which can be loaded and then be visible in the Geographic view and Project explorer. <br>
	 * Once loaded, an optional layer goes into the list of layers. <br>
	 * An optional layer can be required by posting the event UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED
	 */
	private final List<WWLayerDescription> optionalLayerDescription = new ArrayList<>();

	/** Elevation model if any */
	private ElevationModel elevationModel = null;

	/**
	 * Constructor
	 */
	public WWFileLayerStore(IFileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}

	/**
	 * Constructor
	 */
	public WWFileLayerStore(IFileInfo fileInfo, IWWLayer... layers) {
		this(fileInfo);
		addLayers(layers);
	}

	/**
	 * Constructor
	 */
	public WWFileLayerStore(IFileInfo fileInfo, List<IWWLayer> layers) {
		this(fileInfo);
		addLayers(layers);
	}

	/**
	 * @return the {@link #fileInfo}
	 */
	public IFileInfo getFileInfo() {
		return fileInfo;
	}

	/**
	 * @return true when contains any layer
	 */
	public boolean hasLayer() {
		return !layers.isEmpty();
	}

	/**
	 * @return true when contains an ElevationModel
	 */
	public boolean hasElevationModel() {
		return elevationModel != null;
	}

	/**
	 * @return the {@link #layers}
	 */
	public List<IWWLayer> getLayers() {
		return List.copyOf(layers); // Avoid concurrent modification
	}

	/**
	 * @return the {@link #layers}
	 */
	public List<WWLayerDescription> getOptionalLayers() {
		return List.copyOf(optionalLayerDescription); // Avoid concurrent modification
	}

	/**
	 * @return stream of specified class layers
	 */
	public <L extends IWWLayer> Stream<L> getLayers(Class<L> layerKind) {
		return layers.stream().filter(layerKind::isInstance).map(layerKind::cast);
	}

	/**
	 * @return the layer of the specified class and name
	 */
	public <L extends IWWLayer> Optional<L> findLayer(String layerName, Class<L> layerKind) {
		return getLayers(layerKind).filter(layer -> layer.getName().equals(layerName)).findFirst();
	}

	public <T extends IWWLayer> void addLayers(Collection<T> layers) {
		this.layers.addAll(layers);
	}

	public void addLayers(IWWLayer... layers) {
		this.layers.addAll(List.of(layers));
	}

	public void addOptionalLayers(WWLayerDescription... layerDescs) {
		this.optionalLayerDescription.addAll(List.of(layerDescs));
	}

	public void removeLayers(IWWLayer... layers) {
		this.layers.removeAll(List.of(layers));
	}

	public boolean removeOptionalLayer(WWLayerDescription layerDescs) {
		return this.optionalLayerDescription.remove(layerDescs);
	}

	/**
	 * @return the {@link #elevationModel}
	 */
	public ElevationModel getElevationModel() {
		return elevationModel;
	}

	/** Add layers from a WWRasterLayerStore */
	public void addLayers(WWRasterLayerStore createLayersForRaster) {
		layers.addAll(createLayersForRaster.getLayers());
		elevationModel = createLayersForRaster.hasElevationModel() ? createLayersForRaster.getElevationModel()
				: elevationModel;
	}

	/**
	 * @param elevationModel the {@link #elevationModel} to set
	 */
	public void setElevationModel(ElevationModel elevationModel) {
		this.elevationModel = elevationModel;
	}

}
