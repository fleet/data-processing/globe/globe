/**
 * 
 */
package fr.ifremer.globe.ui.service.worldwind.texture.impl;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import gov.nasa.worldwind.geom.Position;

/**
 * GSon adapter to save and restore a IWWTextureCoordinate
 */
public class WWTextureCoordinateAdapter extends TypeAdapter<IWWTextureCoordinate> {

	@Override
	public void write(JsonWriter out, IWWTextureCoordinate value) throws IOException {
		out.beginArray()//
				.value(value.getS())//
				.value(value.getT())//
				.value(value.getPosition().latitude.degrees)//
				.value(value.getPosition().longitude.degrees)//
				.value(value.getPosition().elevation)//
				.endArray();

	}

	@Override
	public WWTextureCoordinate read(JsonReader in) throws IOException {
		in.beginArray();
		var result = new WWTextureCoordinate(//
				(float) in.nextDouble(), //
				(float) in.nextDouble(), //
				Position.fromDegrees(in.nextDouble(), in.nextDouble(), in.nextDouble()));
		in.endArray();
		return result;
	}

}
