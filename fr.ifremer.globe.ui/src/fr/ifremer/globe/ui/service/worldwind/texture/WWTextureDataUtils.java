package fr.ifremer.globe.ui.service.worldwind.texture;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.jogamp.opengl.util.texture.TextureData;

import fr.ifremer.globe.ui.service.worldwind.texture.impl.WWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.impl.WWTextureData;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

/**
 * Utility methods to build {@link IWWTextureData}.
 */
public class WWTextureDataUtils {

	/**
	 * Private constructor : utility class, only public static methods.
	 */
	private WWTextureDataUtils() {
		// private constructor
	}

	public static IWWTextureCoordinate buildTopTextureCoordinate(double lat, double lon, double elevation, float s) {
		return buildTextureCoordinate(Position.fromDegrees(lat, lon, elevation), s, IWWTextureCoordinate.T_TOP);
	}

	public static IWWTextureCoordinate buildBottomTextureCoordinate(double lat, double lon, double elevation, float s) {
		return buildTextureCoordinate(Position.fromDegrees(lat, lon, elevation), s, IWWTextureCoordinate.T_BOTTOM);
	}

	/**
	 * @return a new {@link IWWTextureCoordinate}.
	 *
	 * @param lat       : coordinate's latitude
	 * @param lon       : coordinate's longitude
	 * @param elevation : coodinate's elevation
	 * @param s         : 's' texture vector (define position in texture)
	 * @param t         : 't' texture vector (define position in texture)
	 */
	public static IWWTextureCoordinate buildTextureCoordinate(double lat, double lon, double elevation, float s,
			float t) {
		return buildTextureCoordinate(Position.fromDegrees(lat, lon, elevation), s, t);
	}

	/**
	 * @return a new {@link IWWTextureCoordinate}.
	 */
	public static IWWTextureCoordinate buildTextureCoordinate(Position position, float s, float t) {
		return new WWTextureCoordinate(s, t, position);
	}

	/**
	 * @return a new {@link IWWTextureData} for vertical/longitudinal texture.
	 *
	 * @param width       : texture width
	 * @param height      : texture height
	 * @param dataBuffer  : texture data
	 * @param coordinates : texture coordinates (see {@link IWWTextureCoordinate})
	 */
	public static IWWTextureData buildVerticalTextureData(int width, int height, ByteBuffer dataBuffer,
			List<IWWTextureCoordinate> coordinates) {
		return new WWTextureData(width, height, dataBuffer, coordinates, true);
	}

	/**
	 * @return the {@link IWWTextureData} swath index, if exists.
	 */
	public static Optional<Integer> getSwathIndex(IWWTextureData data) {
		if (data.getMetadata().containsKey(IWWTextureData.NAME_METADATA_KEY)) {
			String textureName = data.getMetadata().get(IWWTextureData.NAME_METADATA_KEY);
			if (textureName.contains("Ping")) {
				try {
					return Optional.of(Integer.parseInt(textureName.replace("Ping", "").trim()));
				} catch (NumberFormatException ex) {
					// invalid index : do nothing
				}
			}
		}
		return Optional.empty();
	}

	/**
	 * @return an int array with 2 values : row & column computed from provided
	 *         {@link TextureData} & {@link Position}. (if not found, values = -1)
	 */
	public static int[] getRowCol(IWWTextureData textureData, Position pos) {
		float[] texCoords;
		if (textureData.isVerticalTexture()) {
			texCoords = getLongitudinalTexCoords(textureData, pos);
		} else {
			texCoords = getGroundTexCoords(textureData, pos);
		}

		float s = texCoords[0];
		float t = texCoords[1];
		if (0 <= t && t < 1 && 0 <= s && s < 1) {
			int row = (int) (t * textureData.getHeight());
			int col = (int) (s * textureData.getWidth());
			return new int[] { row, col };
		}
		return new int[] { -1, -1 };
	}

	/**
	 * @return a float array with 2 values : s & t computed from provided
	 *         {@link TextureData} & {@link Position}. (if not found, values = -1)
	 */
	public static float[] getLongitudinalTexCoords(IWWTextureData textureData, Position pos) {
		Angle distTop1 = Angle.POS360;
		IWWTextureCoordinate coordTop1 = null;
		Angle distTop2 = Angle.POS360;
		IWWTextureCoordinate coordTop2 = null;
		Angle minDistBottom = Angle.POS360;
		IWWTextureCoordinate coordBottom = null;

		for (IWWTextureCoordinate coord : textureData.getCoordinates()) {
			Angle dist = LatLon.linearDistance(pos, coord.getPosition());
			if (coord.getT() == 1) {
				if (dist.degrees < distTop1.degrees) {
					// replace top2 by top1
					distTop2 = distTop1;
					coordTop2 = coordTop1;
					// update top1
					distTop1 = dist;
					coordTop1 = coord;
				} else if (dist.degrees < distTop2.degrees) {
					distTop2 = dist;
					coordTop2 = coord;
				}
			}
			if (coord.getT() == 0 && dist.degrees < minDistBottom.degrees) {
				minDistBottom = dist;
				coordBottom = coord;
			}
		}

		if (coordTop1 != null && coordTop2 != null && coordBottom != null) {
			float t = (float) ((pos.getElevation() - coordBottom.getPosition().getElevation())
					/ (coordTop1.getPosition().getElevation() - coordBottom.getPosition().getElevation()));

			// TODO : find a way to precisely compute this coord. The commented code doesn't
			// work
//			float s = (float) (LatLon.linearDistance(coordTop1.getPosition(), pos).degrees
//					/ LatLon.linearDistance(coordTop1.getPosition(), coordTop2.getPosition()).degrees);
//			s = coordTop1.getS() + s * (coordTop2.getS() - coordTop1.getS());
			float s = coordTop1.getS();

			return new float[] { s, t };
		}
		return new float[] { -1, -1 };
	}

	/**
	 * @return a float array with 2 values : s & t computed from provided
	 *         {@link TextureData} & {@link Position}. (if not found, values = -1)
	 */
	public static float[] getGroundTexCoords(IWWTextureData textureData, Position pos) {
		var bottomLeft = textureData.getCoordinates().get(0);
		var topRight = textureData.getCoordinates().get(textureData.getCoordinates().size() - 1);

		var s = (pos.longitude.degrees - bottomLeft.getPosition().longitude.degrees)
				/ (topRight.getPosition().longitude.degrees - bottomLeft.getPosition().longitude.degrees);
		var t = (pos.latitude.degrees - bottomLeft.getPosition().latitude.degrees)
				/ (topRight.getPosition().latitude.degrees - bottomLeft.getPosition().latitude.degrees);

		return new float[] { (float) s, (float) t };
	}

	/**
	 * Splits the provided {@link IWWTextureData} in several smaller
	 * {@link IWWTextureData}.
	 *
	 * @param src         : input {@link WWTextureData}, will not be modified.
	 * @param sliceWidth  : width of each sub {@link TextureData}
	 * @param sliceHeight : height of each sub {@link TextureData}
	 *
	 * @return a matrix of {@link IWWTextureData}, resulting of the split.
	 *         (IWWTextureData[row][column])
	 */
	public static IWWTextureData[][] split(IWWTextureData src, int sliceWidth, int sliceHeight) {
		int colCount = (int) Math.ceil((double) src.getWidth() / sliceWidth);
		int rowCount = (int) Math.ceil((double) src.getHeight() / sliceHeight);

		// no split required
		if (colCount == 1 && rowCount == 1)
			return new IWWTextureData[][] { { src } };

		var result = new IWWTextureData[rowCount][colCount];
		for (int row = 0; row < rowCount; row++) {
			int rowStart = row * sliceHeight;
			int height = Math.min(sliceHeight, src.getHeight() - rowStart);
			for (int col = 0; col < colCount; col++) {
				int colStart = col * sliceWidth;
				int width = Math.min(sliceWidth, src.getWidth() - colStart);
				result[row][col] = slice(src, colStart, rowStart, width, height);
			}
		}

		return result;
	}

	/**
	 * Cut a part of the provided {@link IWWTextureData}.
	 *
	 * @return new {@link IWWTextureData}
	 */
	public static IWWTextureData slice(IWWTextureData src, int colStart, int rowStart, int width, int height) {
		// create a new buffer (a slice of the source buffer)
		var buffer = src.getBuffer();
		var previousPosition = buffer.position();
		var newBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(ByteOrder.nativeOrder());
		int rowEnd = rowStart + height;
		for (int row = rowStart; row < rowEnd; row++) {
			byte[] dstRowBuffer = new byte[width * Float.BYTES];
			int readPosition = (row * src.getWidth() + colStart) * Float.BYTES;
			buffer.position(readPosition);
			buffer.get(dstRowBuffer, 0, width * Float.BYTES);
			newBuffer.put(dstRowBuffer);
		}
		buffer.position(previousPosition);
		newBuffer.rewind();

		// compute slice texture coordinates (s & t)
		var newCoordinates = new ArrayList<IWWTextureCoordinate>();
		double sStart = (double) colStart / src.getWidth();
		double sEnd = (double) (colStart + width) / src.getWidth();
		double tStart = (double) rowStart / src.getHeight();
		double tEnd = (double) (rowStart + height) / src.getHeight();
		int gridHeight = src.getCoordGridHeight();
		int gridWidth = src.getCoordGridWidth();

		double xStartIndex = 0.0;
		double yStartIndex = 0.0;
		double xEndIndex = gridWidth - 1.0;
		double yEndIndex = gridHeight - 1.0;

		// find x grid indices
		float previousS = 0.f;
		int colGridIndex = 0;
		if (sStart > 0.0) {
			for (; colGridIndex < gridWidth; colGridIndex++) {
				float s = src.getCoordinates().get(colGridIndex * gridHeight).getS();
				if (s >= sStart) {
					double coeff = (sStart - previousS) / (s - previousS);
					xStartIndex = colGridIndex + coeff - 1.0;
					break;
				}
				previousS = s;
			}
		}
		if (sEnd < 1.0) {
			for (; colGridIndex < gridWidth; colGridIndex++) {
				float s = src.getCoordinates().get(colGridIndex * gridHeight).getS();
				if (s >= sEnd) {
					double coeff = (sEnd - previousS) / (s - previousS);
					xEndIndex = colGridIndex + coeff - 1.0;
					break;
				}
				previousS = s;
			}
		}

		// find y grid indices
		float previousT = 0.f;
		int rowGridIndex = 0;
		if (tStart > 0.0) {
			for (; rowGridIndex < gridHeight; rowGridIndex++) {
				float t = src.getCoordinates().get(rowGridIndex).getT();
				if (t >= tStart) {
					double coeff = (tStart - previousT) / (t - previousT);
					yStartIndex = rowGridIndex + coeff - 1.0;
					break;
				}
				previousT = t;
			}
		}
		if (tEnd < 1.0) {
			for (; rowGridIndex < gridHeight; rowGridIndex++) {
				float t = src.getCoordinates().get(rowGridIndex).getT();
				if (t >= tEnd) {
					double coeff = (tEnd - previousT) / (t - previousT);
					yEndIndex = rowGridIndex + coeff - 1.0;
					break;
				}
				previousT = t;
			}
		}
		// fill new Grid coordinates
		for (int colIndex = (int) Math.floor(xStartIndex); colIndex <= Math.ceil(xEndIndex); colIndex++) {
			for (int rowIndex = (int) Math.floor(yStartIndex); rowIndex <= Math.ceil(yEndIndex); rowIndex++) {
				int bufIndex = colIndex * gridHeight + rowIndex;
				var coordinate = src.getCoordinates().get(bufIndex);
				var s = coordinate.getS();
				var t = coordinate.getT();
				float newS = (float) ((s - sStart) / (sEnd - sStart));
				float newT = (float) ((t - tStart) / (tEnd - tStart));

				var coordinatePosition = coordinate.getPosition();
				newCoordinates.add(buildTextureCoordinate(coordinatePosition, newS, newT));
			}
		}

		// Fit new Grid coordinates
		int newGridWidth = (int) (Math.ceil(xEndIndex) - Math.floor(xStartIndex) + 1);
		int newGridHeight = (int) (Math.ceil(yEndIndex) - Math.floor(yStartIndex) + 1);

		// Adjust left column
		var leftColumnPositions = new ArrayList<Position>();
		var leftStartIndex = Math.floor(xStartIndex);
		if (leftStartIndex < xStartIndex) {
			double coef = xStartIndex - leftStartIndex;
			float newS = 0.f;
			for (int rowIndex = 0; rowIndex < newGridHeight; rowIndex++) {
				int bufIndex = rowIndex;
				var leftCoordinate = newCoordinates.get(bufIndex);
				var leftPosition = leftCoordinate.getPosition();
				var rightPosition = newCoordinates.get(bufIndex + newGridHeight).getPosition();
				leftColumnPositions.add(leftPosition);
				var newPosition = Position.interpolate(coef, leftPosition, rightPosition);
				newCoordinates.set(bufIndex, buildTextureCoordinate(newPosition, newS, leftCoordinate.getT()));
			}
		}
		// Adjust right column
		var leftEndIndex = Math.floor(xEndIndex);
		if (leftEndIndex < xEndIndex) {
			double coef = xEndIndex - leftEndIndex;
			float newS = 1.f;
			for (int rowIndex = 0; rowIndex < newGridHeight; rowIndex++) {
				int bufIndex = newGridHeight * (newGridWidth - 1) + rowIndex;
				var rightCoordinate = newCoordinates.get(bufIndex);
				var rightPosition = rightCoordinate.getPosition();
				var leftPosition = newCoordinates.get(bufIndex - newGridHeight).getPosition();
				if (leftEndIndex == leftStartIndex && !leftColumnPositions.isEmpty()) {
					// left column could have shifted : use fixed left positions
					leftPosition = leftColumnPositions.get(rowIndex);
				}
				var newPosition = Position.interpolate(coef, leftPosition, rightPosition);
				newCoordinates.set(bufIndex, buildTextureCoordinate(newPosition, newS, rightCoordinate.getT()));
			}
		}
		// Adjust bottom row
		var bottomRowPositions = new ArrayList<Position>();
		var bottomStartIndex = Math.floor(yStartIndex);
		if (bottomStartIndex < yStartIndex) {
			double coef = yStartIndex - bottomStartIndex;
			float newT = 0.f;
			for (int colIndex = 0; colIndex < newGridWidth; colIndex++) {
				int bufIndex = colIndex * newGridHeight;
				var bottomCoordinate = newCoordinates.get(bufIndex);
				var bottomPosition = bottomCoordinate.getPosition();
				bottomRowPositions.add(bottomPosition);
				var topPosition = newCoordinates.get(bufIndex + 1).getPosition();
				var newPosition = Position.interpolate(coef, bottomPosition, topPosition);
				newCoordinates.set(bufIndex, buildTextureCoordinate(newPosition, bottomCoordinate.getS(), newT));
			}
		}
		// Adjust top row
		var bottomEndIndex = Math.floor(yEndIndex);
		if (bottomEndIndex < yEndIndex) {
			double coef = yEndIndex - bottomEndIndex;
			float newT = 1.f;
			for (int colIndex = 0; colIndex < newGridWidth; colIndex++) {
				int bufIndex = (colIndex + 1) * newGridHeight - 1;
				var topCoordinate = newCoordinates.get(bufIndex);
				var topPosition = topCoordinate.getPosition();
				var bottomPosition = newCoordinates.get(bufIndex - 1).getPosition();
				if (bottomEndIndex == bottomStartIndex && !bottomRowPositions.isEmpty()) {
					// bottom row could have shifted : use fixed bottom positions
					bottomPosition = bottomRowPositions.get(colIndex);
				}
				var newPosition = Position.interpolate(coef, bottomPosition, topPosition);
				newCoordinates.set(bufIndex, buildTextureCoordinate(newPosition, topCoordinate.getS(), newT));
			}
		}
		return src.buildSubTextureData(width, height, newGridHeight, newBuffer, newCoordinates);
	}

}
