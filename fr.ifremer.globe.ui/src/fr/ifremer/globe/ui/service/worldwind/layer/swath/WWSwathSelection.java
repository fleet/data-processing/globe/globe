/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.swath;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;

/**
 * Pojo representing a selected swath in the IWWSwathLayer
 */
public record WWSwathSelection(ISounderNcInfo info, int firstSwath, int lastSwath, GeoBox geobox) {
}
