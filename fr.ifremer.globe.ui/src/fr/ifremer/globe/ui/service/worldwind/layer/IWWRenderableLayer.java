/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer;

import gov.nasa.worldwind.render.Renderable;

/**
 * A WW layer wrapping a RenderableLayer. It can be visited by any IWWLayerOperation
 */
public interface IWWRenderableLayer extends IWWLayer {

	/**
	 * Adds the specified <code>renderable</code> to the end of this layer's internal collection.
	 */
	void addRenderable(Renderable renderable);

	/**
	 * gov.nasa.worldwind.layers.RenderableLayer delegation.
	 */
	void addRenderable(int index, Renderable renderable);

	/**
	 * gov.nasa.worldwind.layers.RenderableLayer delegation.
	 */
	void addRenderables(Iterable<? extends Renderable> renderables);

	/**
	 * gov.nasa.worldwind.layers.RenderableLayer delegation.
	 */
	void removeRenderable(Renderable renderable);

	/**
	 * gov.nasa.worldwind.layers.RenderableLayer delegation.
	 */
	void removeAllRenderables();

	/**
	 * gov.nasa.worldwind.layers.RenderableLayer delegation.
	 */
	int getNumRenderables();

	/**
	 * gov.nasa.worldwind.layers.RenderableLayer delegation.
	 */
	Iterable<Renderable> getRenderables();

	/**
	 * gov.nasa.worldwind.layers.RenderableLayer delegation.
	 */
	void setRenderables(Iterable<Renderable> renderableIterable);

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	/** Add a operation to perform when this layer will be disposed */
	void addDisposeListener(Runnable listener);

	/** Remove a operation to perform when this layer will be disposed */
	void removeDisposeListener(Runnable listener);

	/** Force the rendering of this layer */
	void render();
}
