package fr.ifremer.globe.ui.service.worldwind.layer;

import java.awt.image.BufferedImage;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;

import gov.nasa.worldwind.geom.Position;

/**
 * Instance of layer for representing a vertical image.
 */
public interface IWWVerticalImageLayer extends IWWLayer {

	/** Necessary data to display a vertical image */
	interface Data {
		/** Name of the layer */
		String getName();

		/** Image to display vertically */
		BufferedImage getImage();

		/** Min and max values in the image */
		DoubleRange getMinMaxValues();

		/** top points list */
		List<Position> getTopPoints();

		/** bottom points list */
		List<Position> getBottomPoints();
	}

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

}
