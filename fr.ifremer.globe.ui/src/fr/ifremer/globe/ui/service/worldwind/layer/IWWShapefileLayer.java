/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import gov.nasa.worldwind.layers.Layer;

/**
 * Instance of layer for representing an shp (ie ShpLayer)
 */
public interface IWWShapefileLayer extends IWWLayer, Layer {

	@FunctionalInterface
	interface Data {
		/** @return the Shp file. */
		File getShapeFile();

		/** @return the name of the layer. */
		default String getName() {
			return "Shapes";
		}

		/** @return the fields on which manage the coloration. */
		default Map<String, Set<String>> getFields() {
			return Collections.emptyMap();
		}
	}

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

}
