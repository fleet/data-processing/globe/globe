package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.Observable;
import java.util.Observer;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.ui.utils.Messages;

/**
 * Preferences for WW layers
 */
@Component(name = "globe_ui_service_layer_preferences", service = WWLayerPreferences.class)
public class WWLayerPreferences extends PreferenceComposite {
	protected BooleanPreferenceAttribute useExperimentalTerrainLayers;
	protected BooleanPreferenceAttribute useLegacyWcLayers;

	public WWLayerPreferences() {
		super(PreferenceRegistry.getInstance().getViewsSettingsNode(), "Experimental");

		useExperimentalTerrainLayers = new BooleanPreferenceAttribute("useExperimentalLayers",
				"use experimental terrain layers", true);
		declareAttribute(useExperimentalTerrainLayers);

		useLegacyWcLayers = new BooleanPreferenceAttribute("useLegacyWcLayers", "display legacy WC layers", false);
		declareAttribute(useLegacyWcLayers);

		load();

		useExperimentalTerrainLayers.addObserver(new Observer() {
			@Override
			public void update(Observable o, Object arg) {
				Messages.openInfoMessage("Advice",
						"You should restart Globe and then reload all DTMs to take this option into account");
			}
		});

		useLegacyWcLayers.addObserver(new Observer() {
			@Override
			public void update(Observable o, Object arg) {
				Messages.openInfoMessage("Advice", "You should reload all files containing Water columns");
			}
		});
	}

	/** @return true When Globe use experimental layers rather than deprecated */
	public boolean isUsingExperimentalLayers() {
		return useExperimentalTerrainLayers.getValue().booleanValue();
	}

	/** @return true When Globe use experimental WC layers */
	public boolean isUsingWcLegacyLayers() {
		return useLegacyWcLayers.getValue().booleanValue();
	}
}
