package fr.ifremer.globe.ui.service.worldwind.layer.annotation;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing textual labels.
 */

public interface IWWLabelLayer extends IWWLayer {
	/** Add one label to render */
	ILabelProperties acceptLabel(String label, double longitude, double latitude, double elevation);

	ILabelProperties acceptLabel(String label, double longitude, double latitude, GColor color, boolean userFacing,
			String fontName, int fontSize);

	/** Clear all */
	void removeAllLabels();

	/** Remove one label */
	void removelLabel(ILabelProperties oneLabelProperties);

	/** Getter of {@link #visibilityDistance} */
	double getVisibilityDistance();

	/** Setter of {@link #visibilityDistance} */
	void setVisibilityDistance(double visibilityDistance);

	@Override
	default String getShortName() {
		return "Labels";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.of(Icons.LABEL.toImage());
	}

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

}
