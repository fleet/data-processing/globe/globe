/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.Optional;
import java.util.function.Supplier;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Instance of layer for representing a color scale (ie ColorScaleLayer)
 */
public interface IWWColorScaleLayer extends IWWLayer {

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	@Override
	default String getShortName() {
		return getScaleLabel() + " color scale";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.COLORS.toImage()));
	}

	/**
	 * @return true if a label is displayed.
	 */
	boolean isLabelEnabled();

	/**
	 * Specifies if a label is displayed or not.
	 */
	void setLabelEnabled(boolean selection);

	/**
	 * @return the relative viewport location to display the scalebar. Can be one of AVKey.NORTHEAST, AVKey.NORTHWEST,
	 *         AVKey.SOUTHEAST (the default), or AVKey.SOUTHWEST. These indicate the corner of the viewport.
	 */
	String getPosition();

	/**
	 * Sets the relative viewport location to display the scalebar. Can be one of AVKey.NORTHEAST, AVKey.NORTHWEST,
	 * AVKey.SOUTHEAST (the default), or AVKey.SOUTHWEST. These indicate the corner of the viewport.
	 *
	 * @param position the desired scalebar position
	 */
	void setPosition(String key);

	/**
	 * Returns the current location offset. See #setLocationOffset for a description of the offset and its values.
	 *
	 * @return the location offset. Will be null if no offset has been specified.
	 */
	Vec4 getLocationOffset();

	/**
	 * Specifies a placement offset from the scalebar's position on the screen.
	 *
	 * @param locationOffset the number of pixels to shift the scalebar from its specified screen position. A positive X
	 *            value shifts the image to the right. A positive Y value shifts the image up. If null, no offset is
	 *            applied. The default offset is null.
	 *
	 * @see #setLocationCenter
	 * @see #setPosition
	 */
	void setLocationOffset(Vec4 vec4);

	/**
	 * @return the offset of label position.
	 */
	int getLabelPositionOffset();

	/**
	 * Sets an offset to compute position of label from scale.
	 */
	void setLabelPositionOffset(int offset);

	/**
	 * @return the {@link #scaleLabel}
	 */
	String getScaleLabel();

	/**
	 * @param scaleLabel the {@link #scaleLabel} to set
	 */
	void setScaleLabel(String scaleLabel);

	/** Return the source of the colors */
	Supplier<ColorContrastModel> getSource();

}
