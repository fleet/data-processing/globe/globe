package fr.ifremer.globe.ui.service.worldwind.texture.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import fr.ifremer.globe.core.model.file.FileInfoEvent.FileInfoState;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.utils.gson.GsonByteBufferTypeAdapter;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureDataCache;
import fr.ifremer.globe.utils.cache.WorkspaceCache;

/**
 * Implementation of a cache of IWWTextureData. <br>
 * Metatdata of a texture are saved in a JSon file (size, positions...) <br>
 * Data of a texture (ByteBuffer of floats) are saved in a binary files (extension .buf)
 */
@Component(name = "globe_ui_service_texture_cache", service = IWWTextureDataCache.class)
public class WWTextureDataCache implements IWWTextureDataCache {

	/** Logger */
	protected static final Logger LOGGER = LoggerFactory.getLogger(WWTextureDataCache.class);

	@Inject
	private FileInfoModel fileInfoModel;

	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/** Folder of the files cache */
	public static final File CACHE_FOLDER = WorkspaceCache.getCacheDirectory(WWTextureDataCache.class.getSimpleName());

	public WWTextureDataCache() {
		CACHE_FOLDER.mkdirs();
	}

	@PostConstruct
	void postConstruct() {
		fileInfoModel.addListener(e -> {
			if (e.getState() == FileInfoState.REMOVED || e.getState() == FileInfoState.RELOADING) {
				delete(e.getfileInfo());
			}
		});
	}

	/**
	 * Return true if a texture is available in the cache for the layer
	 */
	@Override
	public boolean isInCache(IFileInfo fileInfo, String textureKey) {
		File folder = computeFolder(fileInfo);
		File jsonFile = computeJsonFile(folder, textureKey);
		return jsonFile.exists();
	}

	/**
	 * Return true if a texture is available in the cache for the layer
	 */
	@Override
	public boolean isInCache(IWWTextureLayer textureLayer) {
		boolean result = false;
		IFileInfo fileInfo = fileLayerStoreModel.get(textureLayer).map(WWFileLayerStore::getFileInfo).orElse(null);
		if (fileInfo != null) {
			result = isInCache(fileInfo, textureLayer.getName());
		}
		return result;
	}

	@Override
	public void save(IFileInfo fileInfo, IWWTextureData textureData, String textureKey) {
		long time = System.currentTimeMillis();
		File folder = computeFolder(fileInfo);
		folder.mkdirs();
		File jsonFile = computeJsonFile(folder, textureKey);
		File bufferFile = computeBufferFile(folder, textureKey);
		try (var fileWriter = new FileWriter(jsonFile, StandardCharsets.UTF_8, false)) {
			Gson gson = new GsonBuilder() //
					.registerTypeHierarchyAdapter(ByteBuffer.class,
							new GsonByteBufferTypeAdapter(bufferFile).nullSafe())
					.registerTypeAdapter(IWWTextureCoordinate.class, new WWTextureCoordinateAdapter().nullSafe())
					.create();
			gson.toJson(textureData, fileWriter);
		} catch (IOException | JsonIOException e) {
			LOGGER.warn("Error while saving the texture {} in cache : {}", textureKey, e.getMessage());
			delete(fileInfo, textureKey);
		}
		LOGGER.debug("{} ms for saving the texture data {} ", (System.currentTimeMillis() - time), textureKey);
	}

	@Override
	public Optional<IWWTextureData> restore(IFileInfo fileInfo, String textureKey) {
		long time = System.currentTimeMillis();
		File folder = computeFolder(fileInfo);
		File jsonFile = computeJsonFile(folder, textureKey);
		File bufferFile = computeBufferFile(folder, textureKey);

		try (var fileReader = new FileReader(jsonFile, StandardCharsets.UTF_8)) {
			Gson gson = makeGson(bufferFile);
			IWWTextureData result = gson.fromJson(fileReader, WWTextureData.class);
			LOGGER.debug("{} ms for restoring the texture data {} ", (System.currentTimeMillis() - time), textureKey);
			return Optional.of(result);
		} catch (FileNotFoundException e) {
			return Optional.empty();
		} catch (IOException | JsonIOException | JsonSyntaxException e) {
			LOGGER.warn("Error while restoring the texture {} from cache : {}", textureKey, e.getMessage());
			delete(fileInfo, textureKey);
			return Optional.empty();
		}
	}

	/**
	 * Delete a texture from the cache
	 */
	@Override
	public void delete(IFileInfo fileInfo, String textureKey) {
		File folder = computeFolder(fileInfo);
		File jsonFile = computeJsonFile(folder, textureKey);
		File bufferFile = computeBufferFile(folder, textureKey);
		if (FileUtils.deleteQuietly(jsonFile) || FileUtils.deleteQuietly(bufferFile)) {
			LOGGER.info("Removing {} from texture cache", textureKey);
			folder.delete();
		}
	}

	/**
	 * Delete the texture of the specified layer from the cache
	 */
	@Override
	public void delete(IWWTextureLayer textureLayer) {
		fileLayerStoreModel.get(textureLayer).ifPresent(store -> delete(store.getFileInfo(), textureLayer.getName()));
	}

	/** Creates an instance of Gson to manage the serialization of IWWTextureData */
	private Gson makeGson(File bufferFile) {
		return new GsonBuilder().setPrettyPrinting()
				.registerTypeHierarchyAdapter(ByteBuffer.class, new GsonByteBufferTypeAdapter(bufferFile).nullSafe())
				.registerTypeAdapter(IWWTextureCoordinate.class, new WWTextureCoordinateAdapter().nullSafe())
				.create();
	}

	/**
	 * Delete all textures of the file from the cache
	 */
	private void delete(IFileInfo fileInfo) {
		File folder = computeFolder(fileInfo);
		if (FileUtils.deleteQuietly(folder)) {
			LOGGER.info("Removing all textures from cache for {}", fileInfo.getFilename());
		}
	}

	private File computeFolder(IFileInfo fileInfo) {
		return new File(CACHE_FOLDER,
				fileInfo.getBaseName() + "_" + String.valueOf(fileInfo.getAbsolutePath().hashCode()));
	}

	private File computeBufferFile(File folder, String textureKey) {
		return new File(folder, textureKey + ".buf");
	}

	private File computeJsonFile(File folder, String textureKey) {
		return new File(folder, textureKey + ".json");
	}

}
