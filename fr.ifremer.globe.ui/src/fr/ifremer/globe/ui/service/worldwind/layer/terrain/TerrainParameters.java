package fr.ifremer.globe.ui.service.worldwind.layer.terrain;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/** Simple Record to hold all terrain parameters */
public /* Record */class TerrainParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<TerrainParameters> SESSION_KEY = new TypedKey<>("Terrain_parameters");

	// color contrast parameters
	public final double opacity;
	public final double minContrast;
	public final double maxContrast;
	public final int colorMap;
	public final boolean reverseColorMap;

	// Shading
	public final boolean useShading;
	public final boolean useGradient;
	public final boolean useLogarithmicShading;
	public final double shadingExaggeration;
	public final double azimuth;
	public final double zenith;

	// Filtering
	public final boolean transparencyEnabled;
	public final double minTransparency;
	public final double maxTransparency;
	public final double minTextureValue;
	public final double maxTextureValue;

	/**
	 * Constructor
	 */
	public TerrainParameters(double opacity, double minContrast, double maxContrast, int colorMap,
			boolean reverseColorMap, boolean useShading, boolean useGradient, boolean useLogarithmicShading,
			double shadingExaggeration, double azimuth, double zenith, boolean transparencyEnabled,
			double minTransparency, double maxTransparency, double minTextureValue, double maxTextureValue) {

		this.opacity = opacity;
		this.minContrast = minContrast;
		this.maxContrast = maxContrast;
		this.colorMap = colorMap;
		this.reverseColorMap = reverseColorMap;

		this.useShading = useShading;
		this.useGradient = useGradient;
		this.useLogarithmicShading = useLogarithmicShading;
		this.shadingExaggeration = shadingExaggeration;
		this.azimuth = azimuth;
		this.zenith = zenith;

		this.transparencyEnabled = transparencyEnabled;
		this.minTransparency = minTransparency;
		this.maxTransparency = maxTransparency;
		this.minTextureValue = minTextureValue;
		this.maxTextureValue = maxTextureValue;
	}

	/**
	 * Creates a new instance by changing the shading
	 */
	public TerrainParameters withShading(boolean useShading, boolean useGradient, boolean useLogarithmicShading,
			double shadingExaggeration, double azimuth, double zenith) {
		return new TerrainParameters(opacity, minContrast, maxContrast, colorMap, reverseColorMap, useShading,
				useGradient, useLogarithmicShading, shadingExaggeration, azimuth, zenith, transparencyEnabled,
				minTransparency, maxTransparency, minTextureValue, maxTextureValue);
	}

	/**
	 * Creates a new instance by changing the color contrast
	 */
	public TerrainParameters withColorContrast(double opacity, double minContrast, double maxContrast, int colorMap,
			boolean reverseColorMap) {
		return new TerrainParameters(opacity, minContrast, maxContrast, colorMap, reverseColorMap, useShading,
				useGradient, useLogarithmicShading, shadingExaggeration, azimuth, zenith, transparencyEnabled,
				minTransparency, maxTransparency, minTextureValue, maxTextureValue);
	}

	/**
	 * Creates a new instance by changing the color
	 */
	public TerrainParameters withColor(int colorMap) {
		return new TerrainParameters(opacity, minContrast, maxContrast, colorMap, reverseColorMap, useShading,
				useGradient, useLogarithmicShading, shadingExaggeration, azimuth, zenith, transparencyEnabled,
				minTransparency, maxTransparency, minTextureValue, maxTextureValue);
	}

	/**
	 * Creates a new instance by changing the color contrast
	 */
	public TerrainParameters withContrast(double minContrast, double maxContrast) {
		return new TerrainParameters(opacity, minContrast, maxContrast, colorMap, reverseColorMap, useShading,
				useGradient, useLogarithmicShading, shadingExaggeration, azimuth, zenith, transparencyEnabled,
				minTransparency, maxTransparency, minTextureValue, maxTextureValue);
	}

	/**
	 * Creates a new instance by changing the color contrast
	 */
	public TerrainParameters withFiltering(boolean transparencyEnabled, double minTransparency, double maxTransparency,
			double minTextureValue, double maxTextureValue) {
		return new TerrainParameters(opacity, minContrast, maxContrast, colorMap, reverseColorMap, useShading,
				useGradient, useLogarithmicShading, shadingExaggeration, azimuth, zenith, transparencyEnabled,
				minTransparency, maxTransparency, minTextureValue, maxTextureValue);
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<TerrainParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

	/**
	 * @return default texture parameters
	 */
	public static TerrainParameters defaultInstance(DoubleRange minMaxTextureValues) {
		return new TerrainParameters(1d, // opacity
				minMaxTextureValues.getMinimumDouble(), // minContrast
				minMaxTextureValues.getMaximumDouble(), // maxContrast
				ColorMaps.GMT_JET.getIndex(), //
				false, // reverseColorMap
				true, // useShading
				false, // useGradient
				false, // useLogarithmicShading
				1d, // shadingExaggeration
				315d, // azimuth,
				45d, // zenith,
				false, // transparencyEnabled
				minMaxTextureValues.getMinimumDouble(), // minTransparency
				minMaxTextureValues.getMaximumDouble(), // maxTransparency
				minMaxTextureValues.getMinimumDouble(), // minTextureValue
				minMaxTextureValues.getMaximumDouble() // maxTextureValue
		);
	}
}
