/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Osgi service used to produce WorldWind Layers
 */
public interface IWWLayerService {

	/**
	 * Returns the service implementation of IWWLayerService.
	 */
	static IWWLayerService grab() {
		return OsgiUtils.getService(IWWLayerService.class);
	}

	/**
	 * @return true when WW layers can be produced to represent this kind of file
	 */
	boolean isManaged(ContentType contentType);

	/**
	 * @param filepath considered file to produce the layers
	 * @param monitor progress monitor.
	 *
	 * @return the WW layers to represent the file
	 */
	Optional<WWFileLayerStore> getLayers(String filepath, IProgressMonitor monitor) throws GIOException;

	/**
	 * Creates a WWFileLayerStore to hold the WW layers to represent the file<br>
	 * User may be solicited for providing extra informations
	 *
	 * @param fileInfo considered IFileInfo to produce the layers
	 * @param silently false to allow user interactions. true to prevent dialog messages
	 * @param monitor progress monitor.
	 * @return the WW layers to represent the file
	 */
	Optional<WWFileLayerStore> getLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException;

	/** @return the WW layers to represent the specified RasterInfo */
	Optional<WWRasterLayerStore> getRasterLayers(RasterInfo rasterInfo, IProgressMonitor monitor) throws GIOException;

	/**
	 * @return model of loaded WWFileLayerStore
	 */
	WWFileLayerStoreModel getFileLayerStoreModel();

}
