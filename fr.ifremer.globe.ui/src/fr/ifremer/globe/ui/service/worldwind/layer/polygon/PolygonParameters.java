/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.polygon;

import java.awt.Color;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Parameters of a IWWPolygonLayer
 */
public class PolygonParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<PolygonParameters> SESSION_KEY = new TypedKey<>("Polygon_parameters");

	public final boolean drawOutline;
	public final GColor lineColor;
	public final int lineWidth;
	public final float opacity;
	public final boolean fill;
	public final GColor fillColor;

	/**
	 * Constructor
	 */
	public PolygonParameters() {
		drawOutline = true;
		lineColor = ColorUtils.convertAWTtoGColor(Color.BLUE);
		lineWidth = 2;
		opacity = 1f;
		fill = false;
		fillColor = ColorUtils.convertAWTtoGColor(Color.RED);
	}

	/**
	 * Constructor
	 */
	public PolygonParameters(boolean drawOutline, GColor lineColor, int lineWidth, float opacity, boolean fill,
			GColor fillColor) {
		this.drawOutline = drawOutline;
		this.lineColor = lineColor;
		this.lineWidth = lineWidth;
		this.opacity = opacity;
		this.fill = fill;
		this.fillColor = fillColor;
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<PolygonParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

	public PolygonParameters withOutLine(boolean drawOutline, GColor lineColor, int lineWidth) {
		return new PolygonParameters(drawOutline, lineColor, lineWidth, opacity, fill, fillColor);
	}

	public PolygonParameters withFill(boolean fill, GColor fillColor) {
		return new PolygonParameters(drawOutline, lineColor, lineWidth, opacity, fill, fillColor);
	}

	public PolygonParameters withOpacity(float opacity) {
		return new PolygonParameters(drawOutline, lineColor, lineWidth, opacity, fill, fillColor);
	}

}
