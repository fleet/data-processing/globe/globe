/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.Optional;
import java.util.function.Function;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.utils.map.TypedMap;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.layers.Layer;

/**
 * A basic WW layer. It can be visited by any IWWLayerOperation
 */
public interface IWWLayer extends Layer, AVList {

	static final String ICON_KEY = "icon_key";
	static final String SHORT_NAME = "short_name";
	/** Key for storing a double to specify maximum layer elevation */
	static final String MAX_ELEVATION_KEY = "max elevation";

	/**
	 * Called by the session to retrieve all the savable parameters of this layer<br>
	 *
	 * A layer implementing this method must also implement setSessionParameter to be able to retrieve the values of the
	 * parameters saved in session.
	 */
	default TypedMap describeParametersForSession() {
		return TypedMap.of();
	}

	/**
	 * Called by the session when the parameters have been restored.
	 */
	default void setSessionParameter(TypedMap parameters) {
	}

	/**
	 * @return the function used to duplicate this WW layer. <br>
	 *         By default, a IWWLayer is not duplicable and the result is Optional.empty()
	 */
	default Optional<Function<String, IWWLayer>> getDuplicationProvider() {
		return Optional.empty();
	}

	/** Design pattern visitor */
	<T extends IWWLayerOperation> T perform(T operation);

	/**
	 * @return a short name for this layer (displayed in Project Explorer for example)
	 */
	default String getShortName() {
		return Optional.ofNullable((String) getValue(SHORT_NAME)).orElse(getName());
	}

	/**
	 * Set the icon to represent the layer
	 */
	default void setShortName(String shortName) {
		setValue(SHORT_NAME, shortName);
	}

	/**
	 * @return the icon to represent the layer
	 */
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY));
	}

	/**
	 * Set the icon to represent the layer
	 */
	default void setIcon(Image image) {
		setValue(ICON_KEY, image);
	}

}
