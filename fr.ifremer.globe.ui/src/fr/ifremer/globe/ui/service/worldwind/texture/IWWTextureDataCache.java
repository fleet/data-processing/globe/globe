package fr.ifremer.globe.ui.service.worldwind.texture;

import java.util.Optional;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;

/**
 * Cache of IWWTextureData. <br>
 * Metatdata of a texture are saved in a JSon file (size, positions...) <br>
 * Data of a texture (ByteBuffer of floats) are saved in a binary files (extension .buf)
 */
public interface IWWTextureDataCache {

	/**
	 * Return true if a texture is available in the cache for the layer
	 */
	boolean isInCache(IFileInfo fileInfo, String textureKey);

	/**
	 * For a layer present in WWFileLayerStoreModel, return true if a texture is available in the cache for the layer
	 */
	boolean isInCache(IWWTextureLayer textureLayer);

	/**
	 * Save the specified textureData in the cache
	 * 
	 * @param textureKey id of the textureData (may be the name of the WW layer)
	 */
	void save(IFileInfo fileInfo, IWWTextureData textureData, String textureKey);

	/**
	 * Restore a IWWTextureData from the cache
	 * 
	 * @return Optional.empty() if error occurs
	 */
	Optional<IWWTextureData> restore(IFileInfo fileInfo, String textureKey);

	/**
	 * Delete a texture from the cache
	 */
	void delete(IFileInfo fileInfo, String textureKey);

	/**
	 * For a layer present in WWFileLayerStoreModel, delete the texture of the specified layer from the cache
	 */
	void delete(IWWTextureLayer textureLayer);

}
