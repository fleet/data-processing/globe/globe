package fr.ifremer.globe.ui.service.worldwind.layer;

/**
 * Interface to handle deprecated MultiTextureLayer (from .xml files)
 */
public interface IWWOldMultiTextureLayer extends IWWLayer {
	
	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}