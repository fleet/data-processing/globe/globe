/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.terrain;

import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.core.utils.color.BitColorPalette;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/** Simple Record to hold all terrain parameters */
public record TerrainWithPaletteParameters(float opacity, AColorPalette palette) {

	/** Key used to save this bean in session */
	public static final TypedKey<TerrainWithPaletteParameters> SESSION_KEY = new TypedKey<>(
			"Terrain_with_palette_parameters");

	/** New instance with a new palette */
	public TerrainWithPaletteParameters withPalette(BitColorPalette palette) {
		return new TerrainWithPaletteParameters(opacity, palette);
	}

	/** New instance with a new opacity */
	public TerrainWithPaletteParameters withOpacity(float opacity) {
		return new TerrainWithPaletteParameters(opacity, palette);
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<TerrainWithPaletteParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

}
