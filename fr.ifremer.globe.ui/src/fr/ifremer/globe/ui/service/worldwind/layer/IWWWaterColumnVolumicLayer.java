/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing a water column.
 */
public interface IWWWaterColumnVolumicLayer extends IWWLayer {

	@Override
	default String getShortName() {
		return "Water column";
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.WC.toImage()));
	}

	/** @return the parameters used to render the DTM */
	public FilterParameters getFilterParameters();

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}
}
