/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.swath;

import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Parameters used by WWSwathLayer
 */
public record WWSwathLayerParameters(ProjectionSettings projection, double opacity, GColor highlightColor,
		GColor selectionColor, int swathsCountInSelection) {

	/** Key used to save this bean in session */
	public static final TypedKey<WWSwathLayerParameters> SESSION_KEY = new TypedKey<>("Swath_Layer_Parameters");

	/**
	 * Constructor
	 */
	public WWSwathLayerParameters() {
		this(StandardProjection.MERCATOR, 1.0, GColor.LIGHT_GRAY, new GColor(0, 127, 255, 255), 1);
	}

	/** Duplicates this WWSwathLayerParameters with an other value of swathsCountInSelection */
	public WWSwathLayerParameters withSwathsCountInSelection(int swathsCountInSelection) {
		return new WWSwathLayerParameters(projection, opacity, highlightColor, selectionColor, swathsCountInSelection);
	}

	/** Duplicates this WWSwathLayerParameters with an other value of Projection */
	public WWSwathLayerParameters withProjection(ProjectionSettings otherProjection) {
		return new WWSwathLayerParameters(otherProjection, opacity, highlightColor, selectionColor,
				swathsCountInSelection);
	}

	/** Duplicates this WWSwathLayerParameters with an other value of opacity */
	public WWSwathLayerParameters withOpacity(double otherOpacity) {
		return new WWSwathLayerParameters(projection, otherOpacity, highlightColor, selectionColor,
				swathsCountInSelection);
	}

	/** Duplicates this WWSwathLayerParameters with an other value of opacity */
	public WWSwathLayerParameters withColor(GColor otherHighlightColor, GColor otherSelectionColor) {
		return new WWSwathLayerParameters(projection, opacity, otherHighlightColor, otherSelectionColor,
				swathsCountInSelection);
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<WWSwathLayerParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}
}
