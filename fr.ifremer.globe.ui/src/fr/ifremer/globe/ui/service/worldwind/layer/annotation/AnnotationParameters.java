package fr.ifremer.globe.ui.service.worldwind.layer.annotation;

import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Parameters of a IWWAnnotationLayer
 */
public record AnnotationParameters(boolean drawOutline, boolean fill, boolean label, boolean marker, float opacity) {

	/** Key used to save this bean in session */
	public static final TypedKey<AnnotationParameters> SESSION_KEY = new TypedKey<>("Annotation_parameters");

	public AnnotationParameters withDrawOutline(boolean drawOutline) {
		return new AnnotationParameters(drawOutline, fill, label, marker, opacity);
	}

	public AnnotationParameters withFill(boolean fill) {
		return new AnnotationParameters(drawOutline, fill, label, marker, opacity);
	}

	public AnnotationParameters withLabel(boolean label) {
		return new AnnotationParameters(drawOutline, fill, label, marker, opacity);
	}

	public AnnotationParameters withMarker(boolean marker) {
		return new AnnotationParameters(drawOutline, fill, label, marker, opacity);
	}

	public AnnotationParameters withOpacity(float opacity) {
		return new AnnotationParameters(drawOutline, fill, label, marker, opacity);
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<AnnotationParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

}
