/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.worldwind.layer.texture;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.service.worldwind.texture.impl.WWTextureData;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Instance of layer for representing a fly texture.
 */
public interface IWWTextureLayer extends IWWLayer {

	String DISPLAY_PARAMETERS_PROPERTY = "DISPLAY_PARAMETERS_PROPERTY";

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	@Override
	default Optional<Image> getIcon() {
		return Optional.ofNullable((Image) getValue(ICON_KEY)).or(() -> Optional.of(Icons.WC_SLICE.toImage()));
	}

	/**
	 * @return {@link WWTextureData} of this layer.
	 */
	IWWTextureData getTextureData();

	/**
	 * @return true if {@link WWTextureData} is not null. Avoids calling the getTextureData() and triggering the
	 *         computation of the texture.
	 */
	boolean hasTextureData();

	/**
	 * @return {@link IWWTextureDisplayParameters} of this texture.
	 */
	WWTextureDisplayParameters getDisplayParameters();

	/**
	 * Set a new {@link IWWTextureDisplayParameters} for this texture.
	 */
	void setDisplayParameters(WWTextureDisplayParameters parameters);

	/**
	 * Builds the texture nasa object.
	 */
	void buildTextureRenderable();

}
