package fr.ifremer.globe.ui.service.worldwind.layer.basic;

import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Basic IWWLayerProvider associated to a IFileInfoSupplier
 */
public abstract class BasicWWLayerProvider<I extends IFileInfo> implements IWWLayerProvider {

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor) throws GIOException {
		WWFileLayerStore result = null;
		if (getContentType().contains(fileInfo.getContentType())) {
			WWFileLayerStore layerStore = new WWFileLayerStore(fileInfo);
			createLayers((I) fileInfo, layerStore, monitor);
			// Something loaded ?
			if (layerStore.hasLayer()) {
				result = layerStore;
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * Required the layers creation
	 *
	 * @param layerStore to fill
	 */
	protected abstract void createLayers(I i, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException;

}
