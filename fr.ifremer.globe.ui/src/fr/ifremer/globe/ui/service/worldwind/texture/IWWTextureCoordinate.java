package fr.ifremer.globe.ui.service.worldwind.texture;

import fr.ifremer.globe.ui.texture.ITextureCoordinate;
import gov.nasa.worldwind.geom.Position;

/**
 * Provides information about a texture coordinate, define by its position in 3D viewer and texture vectors (s (along
 * width) & t (along height)).
 */
public interface IWWTextureCoordinate extends ITextureCoordinate {

	// Top and bottom T values
	public static final float T_TOP = 0;
	public static final float T_BOTTOM = 1;

	Position getPosition();

	Position getPosition(double verticalOffset, double verticalExageration);

}