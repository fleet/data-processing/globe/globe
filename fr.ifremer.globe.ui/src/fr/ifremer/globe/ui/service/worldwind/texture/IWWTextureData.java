package fr.ifremer.globe.ui.service.worldwind.texture;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import fr.ifremer.globe.ui.texture.ITextureData;

/**
 * Provides information about a texture.
 */
public interface IWWTextureData extends ITextureData<IWWTextureCoordinate> {

	/** Metadata keys **/
	public static final String NAME_METADATA_KEY = "NAME";
	public static final String DATA_TYPE_METADATA_KEY = "DATA_TYPE";
	public static final String DATE_METADATA_KEY = "DATE";
	public static final String UNITS_METADATA_KEY = "UNITS";
	public static final String VERTICAL_OFFSET_METADATA_KEY = "VERTICAL_OFFSET";
	public static final String LONGITUDINAL_OFFSET_METADATA_KEY = "LONGITUDINAL_OFFSET";

	/**
	 * Metadata of the synchronization key. <br>
	 * All textures of the same key can share the same WWTextureDisplayParameters.<br>
	 * All textures of the same key can also be selected (in the project explorer).
	 */
	public static final String SYNCHRONIZATION_KEY_METADATA_KEY = "SYNCHRONIZATION_KEY";

	/**
	 * Metadata describing the SYNCHRONIZATION_KEY value
	 */
	public static final String SYNCHRONIZATION_LABEL_METADATA_KEY = "SYNCHRONIZATION_LABEL";

	/**
	 * Minimum float value encountered in the texture
	 */
	public static final String MIN_VALUE_METADATA_KEY = "MIN_VALUE";

	/**
	 * Maximum float value encountered in the texture
	 */
	public static final String MAX_VALUE_METADATA_KEY = "MAX_VALUE";

	/**
	 * @return buffer value for specified row and column.
	 */
	float getValue(int row, int col);

	/**
	 * @return a global metadata for this texture (optional).
	 */
	Map<String, String> getMetadata();

	/**
	 * set global metadata for this texture (optional).
	 */
	void setMetadata(Map<String, String> metadata);

	/**
	 * @return metadata for a specific cell, by default return row/col indexes.
	 */
	Map<String, String> getCellMetadata(int row, int col);

	/**
	 * set metadata provider to feed cellMetadata
	 */
	void setMetadataProviders(Map<String, BiFunction<Integer, Integer, String>> metadataProviders);

	/**
	 * @return the date for a specific cell if exists.
	 */
	Optional<Instant> getDate(int row, int col);

	/**
	 * @return true if this is a vertical texture.
	 */
	boolean isVerticalTexture();

	/**
	 * @return vertical offset as defined in layer.
	 */
	double getVerticalOffset();

	/**
	 * @return the key used by WWTextureSynchronizer to identify which layer to synchronize to.
	 */
	String getSynchronizationKey();

	/**
	 * @return a word to describe the synchronizatior key.
	 */
	String getSynchronizationLabel();

	/**
	 * @return longitudinal offset as defined in layer.
	 */
	double getLongitudinalOffset();

	/**
	 * @return new instance of IWWtextureData with provided buffer and coordinates
	 */
	public IWWTextureData buildSubTextureData(int width, int height, int gridHeight, ByteBuffer buffer,
			List<IWWTextureCoordinate> coordinates);

}
