/*
 * @License@ 
 */
package fr.ifremer.globe.ui.service.worldwind;

import gov.nasa.worldwind.geom.Sector;

/**
 * Interface used by "go to" action.
 * <p>
 * Classes implementing this interface provides a sector for zoom to (ie. go to)
 * action.
 * </p>
 * 
 * @author Guillaume &lt;guillaume.bourel@altran.com&gt;
 */
public interface ITarget {

	/**
	 * Returns the sector value for this ITarget instance for zoom to (ie. go
	 * to) action.
	 * 
	 * @return the sector value for this ITarget instance for zoom to (ie. go
	 *         to) action.
	 */
	public Sector getSector();
}
