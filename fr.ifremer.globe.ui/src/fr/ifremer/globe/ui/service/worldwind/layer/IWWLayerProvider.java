/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Specific WorldWind layers provider. Implemented by bundles to contribute to IWWLayerService.
 */
public interface IWWLayerProvider {

	/** @return the content types managed by this provider */
	Set<ContentType> getContentType();

	/**
	 * Creates a WWFileLayerStore to hold the WW layers to represent the file<br>
	 * User may be solicited for providing extra informations
	 *
	 * @param fileInfo considered IFileInfo to produce the layers
	 * @param silently false to allow user interactions. true to prevent dialog messages
	 * @param monitor progress monitor.
	 * @return the WW layers to represent the file
	 */
	Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException;

	/** @return the WW layers to represent the specified RasterInfo */
	default Optional<WWRasterLayerStore> getRasterLayers(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		return Optional.empty();
	}

	/** @return the WW layers to represent the specified PointCloudFieldInfo */
	default List<IWWLayer> getPointCloudLayers(IFileInfo fileInfo, PointCloudFieldInfo fieldInfo,
			IProgressMonitor monitor) throws GIOException {
		return List.of();
	}
}
