package fr.ifremer.globe.ui.service.worldwind.layer.impl;

import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class builds {@link IWWNavigationLayer} for files which can provide a {@link INavigationDataSupplier}.
 */
@Component(name = "globe_ui_service_worldwind_navigation_layer_provider", service = IWWLayerProvider.class)
public class WWNavigationLayerProvider implements IWWLayerProvider {

	/** Navigation service **/
	@Reference
	protected INavigationService navigationService;

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		var acceptedContentTypes = ContentType.getNavigationContentTypes();
		// Content types to ignore (maybe they have their own navigation layer provider)
		acceptedContentTypes.removeIf(ContentType.getTechsasContentTypes()::contains);
		acceptedContentTypes.remove(ContentType.MGD77);
		acceptedContentTypes.remove(ContentType.USBL);
		return acceptedContentTypes;
	}

	/**
	 * @return an {@link WWFileLayerStore} which contains a {@link IWWNavigationLayer} for files which can provide a
	 *         {@link INavigationDataSupplier}.
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		return navigationService.getNavigationDataSupplier(fileInfo)
				.map(navigationDataSupplier -> new WWFileLayerStore(fileInfo,
						layerFactory.createNavigationLayer(navigationDataSupplier)));
	}

}
