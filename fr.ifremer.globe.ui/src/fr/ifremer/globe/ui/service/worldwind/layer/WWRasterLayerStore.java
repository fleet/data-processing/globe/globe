/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.worldwind.layer;

import java.util.List;

import fr.ifremer.globe.core.model.raster.RasterInfo;
import gov.nasa.worldwind.globes.ElevationModel;

/**
 * Set of Wordwind layers to render a RasterInfo
 */
public class WWRasterLayerStore {

	/** RasterInfo used to generate the ww layers */
	protected final RasterInfo rasterInfo;

	/** WW layers */
	protected final List<IWWLayer> layers;

	/** Elevation model if any */
	protected final ElevationModel elevationModel;

	/**
	 * Constructor
	 */
	public WWRasterLayerStore(RasterInfo rasterInfo, List<IWWLayer> layers, ElevationModel elevationModel) {
		this.rasterInfo = rasterInfo;
		this.layers = layers;
		this.elevationModel = elevationModel;
	}

	/** Creates a new instance with an other ElevationModel */
	public WWRasterLayerStore duplicatesWithElevationModel(ElevationModel otherElevationModel) {
		return new WWRasterLayerStore(rasterInfo, layers, otherElevationModel);
	}

	/**
	 * @return the {@link #rasterInfo}
	 */
	public RasterInfo getRasterInfo() {
		return rasterInfo;
	}

	/**
	 * @return true when contains any layer
	 */
	public boolean hasLayer() {
		return !layers.isEmpty();
	}

	/**
	 * @return true when contains an ElevationModel
	 */
	public boolean hasElevationModel() {
		return elevationModel != null;
	}

	/**
	 * @return the {@link #layers}
	 */
	public List<IWWLayer> getLayers() {
		return layers;
	}

	/**
	 * @return the {@link #elevationModel}
	 */
	public ElevationModel getElevationModel() {
		return elevationModel;
	}
}
