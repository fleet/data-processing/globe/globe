package fr.ifremer.globe.ui.service.worldwind.texture;

import fr.ifremer.globe.ui.texture.TextureDataParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;
import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Parameters to display a WW Texture layer <br>
 * An instance of this class is immutable
 */
public class WWTextureDisplayParameters extends TextureDataParameters {

	/** Key used to save this bean in session */
	public static final TypedKey<WWTextureDisplayParameters> SESSION_KEY = new TypedKey<>("Display_Parameters");

	/** Display parameters **/
	private final float initialMinContrast;
	private final float initialMaxContrast;

	/** Vertical offset model */
	private final boolean verticalOffsetEnabled;
	private final float verticalOffset;

	/**
	 * Constructor
	 */
	public WWTextureDisplayParameters(float minContrast, float maxContrast) {
		this(1, minContrast, maxContrast, ColorMaps.UDIV_CMOCEAN_DELTA.getIndex(), false);
	}

	/**
	 * Constructor
	 */
	public WWTextureDisplayParameters(float minContrast, float maxContrast, int colorMapindex) {
		this(1, minContrast, maxContrast, colorMapindex, false);
	}

	/**
	 * Constructor
	 */
	public WWTextureDisplayParameters(float opacity, float minContrast, float maxContrast, int colorMapindex,
			boolean isInverseColorMap) {
		this(opacity, minContrast, maxContrast, minContrast, maxContrast, minContrast, maxContrast, colorMapindex,
				isInverseColorMap, false, false, false, 0f);
	}

	/**
	 * Constructor
	 */
	public WWTextureDisplayParameters(float opacity, float minContrast, float maxContrast, float initialMinContrast,
			float initialMaxContrast, float minThreshold, float maxThreshold, int colorMapIndex,
			boolean isColorMapReversed, boolean isInterpolationEnabled, boolean isThesholdEnabled,
			boolean verticalOffsetEnabled, float verticalOffset) {
		super(opacity, minContrast, maxContrast, minThreshold, maxThreshold, colorMapIndex, isColorMapReversed,
				isThesholdEnabled, isInterpolationEnabled);
		this.initialMinContrast = initialMinContrast;
		this.initialMaxContrast = initialMaxContrast;
		this.verticalOffsetEnabled = verticalOffsetEnabled;
		this.verticalOffset = verticalOffset;
	}

	/** Duplicates this WWTextureDisplayParameters with an other value of initial Min/Max Contrast */
	public WWTextureDisplayParameters withInitialMinContrast(float initialMinContrast, float initialMaxContrast) {
		return new WWTextureDisplayParameters(opacity, minContrast, maxContrast, initialMinContrast, initialMaxContrast,
				minThreshold, maxThreshold, colorMapIndex, isColorMapReversed, isInterpolationEnabled,
				isThesholdEnabled, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWTextureDisplayParameters with an other value of verticalOffset */
	public WWTextureDisplayParameters withVerticalOffset(boolean verticalOffsetEnabled, float verticalOffset) {
		return new WWTextureDisplayParameters(opacity, minContrast, maxContrast, initialMinContrast, initialMaxContrast,
				minThreshold, maxThreshold, colorMapIndex, isColorMapReversed, isInterpolationEnabled,
				isThesholdEnabled, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWTextureDisplayParameters with an other value of opacity */
	@Override
	public WWTextureDisplayParameters withOpacity(float opacity) {
		return new WWTextureDisplayParameters(opacity, minContrast, maxContrast, initialMinContrast, initialMaxContrast,
				minThreshold, maxThreshold, colorMapIndex, isColorMapReversed, isInterpolationEnabled,
				isThesholdEnabled, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWTextureDisplayParameters with other Contrast and Color values */
	public WWTextureDisplayParameters withContrastAndColor(float minContrast, float maxContrast, int colorMapIndex,
			boolean isColorMapReversed, boolean isInterpolationEnabled, float opacity) {
		return new WWTextureDisplayParameters(opacity, minContrast, maxContrast, initialMinContrast, initialMaxContrast,
				minThreshold, maxThreshold, colorMapIndex, isColorMapReversed, isInterpolationEnabled,
				isThesholdEnabled, verticalOffsetEnabled, verticalOffset);
	}

	/** Duplicates this WWTextureDisplayParameters with other Threshold values */
	public WWTextureDisplayParameters withThreshold(float minThreshold, float maxThreshold, boolean isThesholdEnabled) {
		return new WWTextureDisplayParameters(opacity, minContrast, maxContrast, initialMinContrast, initialMaxContrast,
				minThreshold, maxThreshold, colorMapIndex, isColorMapReversed, isInterpolationEnabled,
				isThesholdEnabled, verticalOffsetEnabled, verticalOffset);
	}

	// GETTERS & SETTERS

	public float getOpacity() {
		return opacity;
	}

	public float getMinContrast() {
		return minContrast;
	}

	public float getInitialMinContrast() {
		return initialMinContrast;
	}

	public float getMaxContrast() {
		return maxContrast;
	}

	public float getInitialMaxContrast() {
		return initialMaxContrast;
	}

	public int getColorMapIndex() {
		return colorMapIndex;
	}

	public boolean isColorMapReversed() {
		return isColorMapReversed;
	}

	public boolean isInterpolationEnabled() {
		return isInterpolationEnabled;
	}

	public boolean isThesholdEnabled() {
		return isThesholdEnabled;
	}

	public float getMaxThreshold() {
		return maxThreshold;
	}

	public float getMinThreshold() {
		return minThreshold;
	}

	/** Wrap this bean to a TypedEntry with SESSION_KEY as key */
	public TypedEntry<WWTextureDisplayParameters> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

	/**
	 * @return the {@link #verticalOffset}
	 */
	public float verticalOffset() {
		return verticalOffset;
	}

	/**
	 * @return the {@link #verticalOffsetEnabled}
	 */
	public boolean verticalOffsetEnabled() {
		return verticalOffsetEnabled;
	}

}
