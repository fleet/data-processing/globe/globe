package fr.ifremer.globe.ui.service.worldwind.layer.current;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerOperation;
import fr.ifremer.globe.ui.widget.color.ColorContrastModel;

/**
 * Specific layer drawing current vectors
 */
public interface IWWCurrentLayer extends IWWLayer, Supplier<ColorContrastModel> {

	@Override
	default String getShortName() {
		return "Current";
	}

	/** @return the parameters used to render the layer */
	WWCurrentLayerParameters getParameters();

	/** Set the parameters used to render the layer */
	void setParameters(WWCurrentLayerParameters parameters);

	/**
	 * @return {@link ColorContrastModel} for the specified {@link NavigationMetadataType}.
	 */
	ColorContrastModel getColorContrastModel(NavigationMetadataType<?> metadataType);

	/**
	 * @return {@link Map} of {@link ColorContrastModel} by {@link NavigationMetaData}.
	 */
	Map<NavigationMetadataType<?>, ColorContrastModel> getColorContrastModelByMetadataType();

	/** Process the operation for this kind of IWWLayer */
	@Override
	default <T extends IWWLayerOperation> T perform(T operation) {
		operation.accept(this);
		return operation;
	}

	/**
	 * @return associated {@link IWWColorScaleLayer} (used to display scale of colored navigation line)
	 */
	Optional<IWWColorScaleLayer> getColorScaleLayer();

	@Override
	default ColorContrastModel get() {
		return getParameters().colorContrastModel();
	}

	/** Data currently displayed */
	ICurrentData getCurrentData();

	/** Apply a filter */
	void filter(CurrentFiltering filtering);

	/** Last applyed filter */
	CurrentFiltering getCurrentFiltering();

}
