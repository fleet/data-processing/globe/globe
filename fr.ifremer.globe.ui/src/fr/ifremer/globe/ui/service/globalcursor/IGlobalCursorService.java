package fr.ifremer.globe.ui.service.globalcursor;

import java.util.function.Consumer;

import fr.ifremer.globe.ui.application.context.ContextInitializer;

/**
 * This services provides methods to share a cursor between several views.
 */
public interface IGlobalCursorService {

	/**
	 * @return the service implementation.
	 */
	static IGlobalCursorService grab() {
		return ContextInitializer.getInContext(IGlobalCursorService.class);
	}

	/**
	 * Submits an empty cursor (useful to hide the previous).
	 */
	void submitEmpty();

	/**
	 * @return the last submitted GlobalCursor or an empty one if none
	 */
	GlobalCursor getLastSubmitted();

	/**
	 * Submits a new cursor.
	 */
	void submit(GlobalCursor globalCursor);

	/**
	 * Accepts a new listener.
	 */
	void addListener(Consumer<GlobalCursor> listener);

	/**
	 * Removes a listener.
	 */
	void removeListener(Consumer<GlobalCursor> listener);
}
