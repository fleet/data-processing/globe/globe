package fr.ifremer.globe.ui.service.globalcursor;

import java.time.Instant;
import java.util.Optional;

import gov.nasa.worldwind.geom.Position;

/**
 * Model of cursor which could be used by any view.
 */
public record GlobalCursor(
		// Emitter of the cursor
		Optional<Object> source,
		// Datetime
		Optional<Instant> time,
		// Synchronisation of Texture players is required
		boolean syncTexturePlayers,
		// Synchronisation of WC players is required
		boolean syncWcPayers,
		// Geo position
		Optional<Position> position,
		// True when cursor must lay on the ground
		boolean clampToGround,
		// Description of the cursor (ie tooltip)
		Optional<String> description,
		// True to render the cursor in GlobalCursorLayer
		boolean visible) {

	/** Key useful for {@link AVList} objects **/
	public static final String AV_KEY = "fr.ifremer.globe.ui.service.globalcursor";

	/**
	 * Default constructor for an empty cursor
	 */
	public GlobalCursor() {
		this(Optional.empty(), Optional.empty(), false, false, Optional.empty(), false, Optional.empty(), true);
	}

	public GlobalCursor withSource(Object source) {
		return new GlobalCursor(Optional.of(source), time, syncTexturePlayers, syncWcPayers, position, clampToGround,
				description, visible);
	}

	public GlobalCursor withTime(Instant time) {
		return new GlobalCursor(source, Optional.of(time), syncTexturePlayers, syncWcPayers, position, clampToGround,
				description, visible);
	}

	public GlobalCursor withSyncTexturePlayer(boolean syncTexturePlayers) {
		return new GlobalCursor(source, time, syncTexturePlayers, syncWcPayers, position, clampToGround, description,
				visible);
	}

	public GlobalCursor withSyncWcPayer(boolean syncWcPayers) {
		return new GlobalCursor(source, time, syncTexturePlayers, syncWcPayers, position, clampToGround, description,
				visible);
	}

	public GlobalCursor withPosition(Position position) {
		return new GlobalCursor(source, time, syncTexturePlayers, syncWcPayers, Optional.of(position), clampToGround,
				description, visible);
	}

	public GlobalCursor withClampToGround(boolean clampToGround) {
		return new GlobalCursor(source, time, syncTexturePlayers, syncWcPayers, position, clampToGround, description,
				visible);
	}

	public GlobalCursor withPosition(double lat, double lon, double elevation, boolean clampToGround) {
		return new GlobalCursor(source, time, syncTexturePlayers, syncWcPayers,
				Optional.of(Position.fromDegrees(lat, lon, elevation)), clampToGround, description, visible);
	}

	public GlobalCursor withDescription(String description) {
		return new GlobalCursor(source, time, syncTexturePlayers, syncWcPayers, position, clampToGround,
				Optional.of(description), visible);
	}

	public GlobalCursor withVisible(boolean visible) {
		return new GlobalCursor(source, time, syncTexturePlayers, syncWcPayers, position, clampToGround, description,
				visible);
	}

}