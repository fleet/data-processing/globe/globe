package fr.ifremer.globe.ui.service.globalcursor.impl;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Consumer;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;

/**
 * Implementation of {@link IGlobalCursorService}.
 */
@Component(name = "globe_ui_service_global_cursor_service", service = { IGlobalCursorService.class })
public class GlobalCursorService implements IGlobalCursorService {

	private GlobalCursor lastEmittedCursor;

	/** All listeners */
	protected Set<Consumer<GlobalCursor>> listeners = new CopyOnWriteArraySet<>();

	@Override
	public void submit(GlobalCursor globalCursor) {
		// do not emit twice same cursor
		if (!Objects.equals(globalCursor, lastEmittedCursor)) {
			lastEmittedCursor = globalCursor;
			listeners.forEach(consumer -> consumer.accept(globalCursor));
		}
	}

	@Override
	public void addListener(Consumer<GlobalCursor> listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(Consumer<GlobalCursor> listener) {
		listeners.remove(listener);
	}

	/** {@inheritDoc} */
	@Override
	public GlobalCursor getLastSubmitted() {
		return lastEmittedCursor != null ? lastEmittedCursor : new GlobalCursor();
	}

	/** {@inheritDoc} */
	@Override
	public void submitEmpty() {
		submit(new GlobalCursor());
	}

}
