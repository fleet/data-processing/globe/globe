/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.wizard;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.commons.lang.math.Range;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.model.dtm.filter.FilteringOperator;
import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableDuration;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableInstant;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.model.DepthCorrectionModel;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;

/**
 * Wizard builder
 */
public interface IWizardBuilder {

	/**
	 * Set the window title string for this wizard.
	 *
	 * @param windowTitle : the window title string, or <code>null</code> for no title
	 */
	IWizardBuilder setWindowTitle(String windowTitle);

	/**
	 * Add a SelectFilePage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param fileExtension : the expected file extension
	 * @param selectedFile : the initial and resulting selected file
	 */
	default IWizardBuilder addSelectFilePage(String pageTitle, boolean mandatory, List<Pair<String, String>> fileFilter,
			WritableFile selectedFile) {
		return addSelectFilePage(pageTitle, mandatory, fileFilter, selectedFile, () -> true);
	}

	/**
	 * Add a SelectFilePage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param fileExtension : the expected file extension
	 * @param selectedFile : the initial and resulting selected file
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSelectFilePage(String pageTitle, boolean mandatory, List<Pair<String, String>> fileFilter,
			WritableFile selectedFile, BooleanSupplier enabledSupplier);

	/**
	 * Add a SelectFilePage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param selectedFile : the initial and resulting selected folder
	 */
	default IWizardBuilder addSelectFolderPage(String pageTitle, boolean mandatory, WritableFile selectedFolder) {
		return addSelectFolderPage(pageTitle, mandatory, selectedFolder, () -> true);
	}

	/**
	 * Add a SelectFilePage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param selectedFile : the initial and resulting selected folder
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSelectFolderPage(String pageTitle, boolean mandatory, WritableFile selectedFolder,
			BooleanSupplier enabledSupplier);

	/**
	 * Add a SelectInputFilesPage wizard page.
	 *
	 * @param filterExtensions : file filter required by the page model
	 * @param mandatory : true when at least one file must be selected
	 * @param selectedFiles : the initial and resulting selected files
	 */
	default IWizardBuilder addSelectInputFilesPage(String pageTitle, List<Pair<String, String>> filterExtensions,
			boolean mandatory, WritableObjectList<File> selectedFiles) {
		return addSelectInputFilesPage(pageTitle, filterExtensions, mandatory, selectedFiles, () -> true);
	}

	/**
	 * Add a SelectInputFilesPage wizard page.
	 *
	 * @param filterExtensions : file filter required by the page model
	 * @param mandatory : true when at least one file must be selected
	 * @param selectedFiles : the initial and resulting selected files
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSelectInputFilesPage(String pageTitle, List<Pair<String, String>> filterExtensions,
			boolean mandatory, WritableObjectList<File> selectedFiles, BooleanSupplier enabledSupplier);

	/**
	 * Add a SortFilesByCdiPage wizard page.
	 *
	 * @param selectedFiles : the initial and resulting selected files
	 */
	default IWizardBuilder addSortFilesByCdiPage(String pageTitle, WritableObjectList<File> selectedFiles) {
		return addSortFilesByCdiPage(pageTitle, selectedFiles, () -> true);
	}

	/**
	 * Add a SortFilesByCdiPage wizard page.
	 *
	 * @param selectedFiles : the initial and resulting selected files
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSortFilesByCdiPage(String pageTitle, WritableObjectList<File> selectedFiles,
			BooleanSupplier enabledSupplier);

	/**
	 * Add a TimeIntervalPage wizard page.
	 *
	 * @param referenceTimeInterval : reference time interval representing the interval covering all input files
	 */
	IWizardBuilder addTimeIntervalPage(String pageTitle, WritableObject<Pair<Instant, Instant>> referenceTimeInterval,
			WritableBoolean isMergeSimple, //
			WritableBoolean isCutFile, WritableFile cutFile, //
			WritableBoolean isManually, WritableObject<Instant> startDate, WritableObject<Instant> endDate, //
			WritableBoolean isGeoMaskFile, WritableFile geoMaskFile, WritableBoolean reverseGeoMask);

	/**
	 * Add a TimeIntervalPage wizard page.
	 */
	IWizardBuilder addDepthCorrectionPage(String pageTitle, DepthCorrectionModel model);

	/**
	 * Add a SelectOutputParametersPage wizard page for some output files.
	 *
	 * @param inputFiles : file in input used to compute the merged outputFile
	 * @param outputFileExtension : the expected file extension for the output file
	 * @param outputFiles : the output file paths
	 * @param loadFilesAfter : allow the loading of output files
	 * @param whereToloadFiles : Where to load the files (in witch group of project explorer)
	 * @param overwriteExistingFiles : If true, output files are overwriting when exist
	 * @param mergeOutputFiles : true to have only one output file
	 * @param prefix : prefix of output file
	 * @param suffix : suffix of output file
	 * @param singleOutputFilename : default output file name
	 */
	default IWizardBuilder addSelectOutputFilesPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableObjectList<String> outputFileExtensions, WritableObjectList<File> outputFiles,
			WritableBoolean loadFilesAfter, WritableString whereToloadFiles, WritableBoolean overwriteExistingFiles,
			WritableBoolean mergeOutputFiles, WritableString prefix, WritableString suffix,
			WritableString singleOutputFilename, boolean mergeAllowed) {
		return addSelectOutputFilesPage(pageTitle, inputFiles, outputFileExtensions, outputFiles, loadFilesAfter,
				whereToloadFiles, overwriteExistingFiles, mergeOutputFiles, prefix, suffix, singleOutputFilename,
				mergeAllowed, () -> true);
	}

	/**
	 * Add a SelectOutputParametersPage wizard page for some output files.
	 *
	 * @param inputFiles : file in input used to compute the merged outputFile
	 * @param outputFileExtension : the expected file extension for the output file
	 * @param outputFiles : the generated output file paths
	 * @param loadFilesAfter : allow the loading of output files
	 * @param whereToloadFiles : Where to load the files (in witch group of project explorer)
	 * @param overwriteExistingFiles : If true, output files are overwriting when exist
	 */
	default IWizardBuilder addSelectOutputFilesPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableObjectList<String> outputFileExtensions, WritableObjectList<File> outputFiles,
			WritableBoolean loadFilesAfter, WritableString whereToloadFiles, WritableBoolean overwriteExistingFiles) {
		return addSelectOutputFilesPage(pageTitle, //
				inputFiles, outputFileExtensions, //
				outputFiles, //
				loadFilesAfter, //
				whereToloadFiles, //
				overwriteExistingFiles, //
				new WritableBoolean(false), // mergeOutputFiles,
				new WritableString(""), // prefix
				new WritableString(""), // suffix
				new WritableString(""), // singleOutputFilename
				false, // mergeAllowed
				() -> true);
	}

	/**
	 * Add a SelectOutputParametersPage wizard page for some output files.
	 *
	 * @param inputFiles : file in input used to compute the merged outputFile
	 * @param outputFileExtensions : the expected file extensions for the output file
	 * @param outputFiles : the output file paths
	 * @param loadFilesAfter : allow the loading of output files
	 * @param whereToloadFiles : Where to load the files (in witch group of project explorer)
	 * @param overwriteExistingFiles : If true, output files are overwriting when exist
	 * @param mergeOutputFiles : true to have only one output file
	 * @param prefix : prefix of output file
	 * @param suffix : suffix of output file
	 * @param singleOutputFilename : default output file name
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSelectOutputFilesPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableObjectList<String> outputFileExtensions, WritableObjectList<File> outputFiles,
			WritableBoolean loadFilesAfter, WritableString whereToloadFiles, WritableBoolean overwriteExistingFiles,
			WritableBoolean mergeOutputFiles, WritableString prefix, WritableString suffix,
			WritableString singleOutputFilename, boolean mergeAllowed, BooleanSupplier enabledSupplier);

	/**
	 * Add a GenericGeoboxPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param geoboxes : all reference geobox
	 * @param customGeobox : the edited geobox
	 */
	default IWizardBuilder addGeoBoxWizardPage(String pageTitle, //
			WritableList<GeoBox> geoboxes, //
			WritableObject<GeoBox> customGeobox, //
			WritableObject<Projection> projection, //
			Optional<WritableDouble> spatialResolution, //
			Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator, //
			Optional<Consumer<MapOfDoubleConsumer>> geoBoxEvaluator) {
		return addGeoBoxWizardPage(pageTitle, geoboxes, customGeobox, projection, spatialResolution,
				spatialResolutionEvaluator, geoBoxEvaluator, () -> true);
	}

	/**
	 * Add a GenericGeoboxPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param geoboxes : all reference geobox
	 * @param customGeobox : the edited geobox
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addGeoBoxWizardPage(String pageTitle, //
			WritableList<GeoBox> geoboxes, //
			WritableObject<GeoBox> customGeobox, //
			WritableObject<Projection> projection, //
			Optional<WritableDouble> spatialResolution, //
			Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator, //
			Optional<Consumer<MapOfDoubleConsumer>> geoBoxEvaluator, //
			BooleanSupplier enabledSupplier);

	/**
	 * Add a GenericSpatialResolutionPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param projection : projection, to get the unit
	 * @param spatialResolution : spatial resolution to edit
	 * @param spatialResolutionEvaluator : to request a computation of the spatial resolution
	 */
	IWizardBuilder addSpatialResolutionWizardPage(String pageTitle, //
			WritableObject<Projection> projection, //
			Optional<WritableDouble> spatialResolution, //
			Optional<Consumer<MapOfDoubleConsumer>> spatialResolutionEvaluator);

	/**
	 * Add a AsciiDelimiterPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param IxyzFile : parsing parameters
	 */
	default IWizardBuilder addAsciiDelimiterPage(String pageTitle, IxyzFile file) {
		return addAsciiDelimiterPage(pageTitle, file, () -> true);
	}

	/**
	 * Add a AsciiDelimiterPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param IxyzFile : parsing parameters
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addAsciiDelimiterPage(String pageTitle, IxyzFile file, BooleanSupplier enabledSupplier);

	/**
	 * Add a ProjectionWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param projection : projection configuration
	 */
	default IWizardBuilder addProjectionPage(String pageTitle, WritableObject<ProjectionSettings> projection,
			WritableObject<GeoBox> geobox) {
		return addProjectionPage(pageTitle, projection, geobox, () -> true);
	}

	/**
	 * Add a ProjectionWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param projection : projection configuration
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addProjectionPage(String pageTitle, WritableObject<ProjectionSettings> projection,
			WritableObject<GeoBox> geobox, BooleanSupplier enabledSupplier);

	/**
	 * Add a MultiFilterWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param inputFiles : file in input
	 * @param filters : edited filter (see FilterDtm.toString)
	 */
	default IWizardBuilder addMultiFilterWizardPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableList<String> filters, WritableObject<FilteringOperator> operator) {
		return addMultiFilterWizardPage(pageTitle, inputFiles, filters, operator, () -> true);
	}

	/**
	 * Add a MultiFilterWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page
	 * @param inputFiles : file in input
	 * @param filters : edited filter (see FilterDtm.toString)
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addMultiFilterWizardPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableList<String> filters, WritableObject<FilteringOperator> operator, BooleanSupplier enabledSupplier);

	/**
	 * Add one File argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : File argument
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableFile value, Supplier<File> evaluator, List<String> extensions) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, extensions, () -> true);
	}

	/**
	 * Add one File argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : File argument
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument, WritableFile value,
			Supplier<File> evaluator, List<String> extensions, BooleanSupplier enabledSupplier);

	/**
	 * Add one boolean argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : boolean argument
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableBoolean value, Supplier<Boolean> evaluator) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, () -> true);
	}

	/**
	 * Add one boolean argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : boolean argument
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableBoolean value, Supplier<Boolean> evaluator, BooleanSupplier enabledSupplier);

	/**
	 * Add one double argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : double argument
	 * @param doubleRange : range of valid values. may be null
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableDouble value, Supplier<Double> evaluator, Range doubleRange) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, doubleRange, () -> true);
	}

	/**
	 * Add one double argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : double argument
	 * @param doubleRange : range of valid values. may be null
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableDouble value, Supplier<Double> evaluator, Range doubleRange, BooleanSupplier enabledSupplier);

	/**
	 * Add one int argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : int argument
	 * @param intRange : range of valid values
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableInt value, Supplier<Integer> evaluator, Range intRange) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, intRange, () -> true);
	}

	/**
	 * Add one int argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : int argument
	 * @param intRange : range of valid values
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument, WritableInt value,
			Supplier<Integer> evaluator, Range intRange, BooleanSupplier enabledSupplier);

	/**
	 * Add one int argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : int argument
	 * @param choices : possible values
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableInt value, Supplier<Integer> evaluator, int... choices) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, () -> true, choices);
	}

	/**
	 * Add one int argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : int argument
	 * @param choices : possible values
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument, WritableInt value,
			Supplier<Integer> evaluator, BooleanSupplier enabledSupplier, int... choices);

	/**
	 * Add one string argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : string argument
	 * @param choices : possible values
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableString value, Supplier<String> evaluator, String... choices) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, () -> true, choices);
	}

	/**
	 * Add one string argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : string argument
	 * @param choices : possible values
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableString value, Supplier<String> evaluator, BooleanSupplier enabledSupplier, String... choices);

	/**
	 * Add one duration argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : duration argument
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableDuration value, Supplier<Duration> evaluator) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, () -> true);
	}

	/**
	 * Add one duration argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : duration argument
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableDuration value, Supplier<Duration> evaluator, BooleanSupplier enabledSupplier);

	/**
	 * Add one instant argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : instant argument
	 */
	default IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableInstant value, Supplier<Instant> evaluator) {
		return addToGenericPage(pageTitle, pageDescription, labelArgument, value, evaluator, () -> true);
	}

	/**
	 * Add one instant argument to a Generic page.
	 *
	 * @param pageTitle : Title of the page
	 * @param labelArgument : label of widget
	 * @param value : instant argument
	 */
	IWizardBuilder addToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableInstant value, Supplier<Instant> evaluator, BooleanSupplier enabledSupplier);

	/**
	 * Add a SetCDIPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param files : target files
	 * @param cdis : edited CDIs
	 * @param defaultValue : default cdi value
	 */
	default IWizardBuilder addSetCDIPage(String pageTitle, WritableObjectList<File> files, WritableList<String> cdis,
			String defaultValue) {
		return addSetCDIPage(pageTitle, files, cdis, defaultValue, () -> true);
	}

	/**
	 * Add a SetCDIPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param files : target files
	 * @param cdis : edited CDIs
	 * @param defaultValue : default cdi value
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSetCDIPage(String pageTitle, WritableObjectList<File> files, WritableList<String> cdis,
			String defaultValue, BooleanSupplier enabledSupplier);

	/**
	 * Add a ModifyCDIPage2 wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param inputFiles : files in input
	 * @param originalCdis : original CDIs
	 * @param cdis : edited CDIs
	 */
	default IWizardBuilder addModifyCDIPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableList<String> originalCdis, WritableList<String> cdis) {
		return addModifyCDIPage(pageTitle, inputFiles, originalCdis, cdis, () -> true);
	}

	/**
	 * Add a ModifyCDIPage2 wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param inputFiles : files in input
	 * @param originalCdis : original CDIs
	 * @param cdis : edited CDIs
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addModifyCDIPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableList<String> originalCdis, WritableList<String> cdis, BooleanSupplier enabledSupplier);

	/**
	 * Add a MaskPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param maskEnable : the boolean indicating the mask page is activated
	 * @param maskSize : the number of cell composing a mask
	 * @param maskFiles : the masking files
	 */
	default IWizardBuilder addMaskWizardPage(String pageTitle, WritableBoolean maskEnable, WritableInt maskSize,
			WritableObjectList<File> maskFiles) {
		return addMaskWizardPage(pageTitle, maskEnable, maskSize, maskFiles, () -> true);
	}

	/**
	 * Add a MaskPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param maskEnable : the boolean indicating the mask page is activated
	 * @param maskSize : the number of cell composing a mask
	 * @param maskFiles : the masking files
	 */
	IWizardBuilder addMaskWizardPage(String pageTitle, WritableBoolean maskEnable, WritableInt maskSize,
			WritableObjectList<File> maskFiles, BooleanSupplier enabledSupplier);

	/**
	 * Add a GenericCheckListWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param availableValues : all possibles values
	 * @param selectedValues : resulting checked values
	 */
	default IWizardBuilder addGenericCheckListPage(String pageTitle, String pageDescription,
			WritableSet<String> availableValues, WritableSet<String> selectedValues) {
		return addGenericCheckListPage(pageTitle, pageDescription, availableValues, selectedValues, () -> true);
	}

	/**
	 * Add a GenericCheckListWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param availableValues : all possibles values
	 * @param selectedValues : resulting checked values
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addGenericCheckListPage(String pageTitle, String pageDescription,
			WritableSet<String> availableValues, WritableSet<String> selectedValues, BooleanSupplier enabledSupplier);

	/**
	 * Add a GenericCheckListWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param availableValues : all possibles values
	 * @param selectedValues : resulting checked values
	 */
	default IWizardBuilder addGenericCheckListPage(String pageTitle, String pageDescription,
			WritableList<String> availableValues, WritableList<String> selectedValues) {
		return addGenericCheckListPage(pageTitle, pageDescription, availableValues, selectedValues, () -> true);
	}

	/**
	 * Add a GenericCheckListWizardPage wizard page.
	 *
	 * @param pageTitle : Title of the page.
	 * @param availableValues : all possibles values
	 * @param selectedValues : resulting checked values
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addGenericCheckListPage(String pageTitle, String pageDescription,
			WritableList<String> availableValues, WritableList<String> selectedValues, BooleanSupplier enabledSupplier);

	/**
	 * Add a GenericCheckListWizardPage wizard page for the layers contains in the input files.
	 *
	 * @param pageTitle : Title of the page.
	 * @param inputFiles : files in input
	 * @param selectedLayers : resulting checked layers
	 */
	default IWizardBuilder addLayersPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableSet<String> selectedLayers) {
		return addLayersPage(pageTitle, inputFiles, selectedLayers, () -> true);
	}

	/**
	 * Add a GenericCheckListWizardPage wizard page for the layers contains in the input files.
	 *
	 * @param pageTitle : Title of the page.
	 * @param inputFiles : files in input
	 * @param selectedLayers : resulting checked layers
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addLayersPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableSet<String> selectedLayers, BooleanSupplier enabledSupplier);

	/**
	 * Add a GenericCheckListWizardPage wizard page for the sensors contains in the input directory.
	 *
	 * @param pageTitle : Title of the page.
	 * @param pageDescription : Description of the page
	 * @param inputFiles : root directory for data or files to parse
	 * @param selectedSensors : resulting checked sensors
	 * @param sensorFilters : sensor types to search for
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSensorsCheckListPage(String pageTitle, String pageDescription,
			WritableObjectList<File> inputFiles, WritableList<String> selectedSensors,
			WritableSet<String> sensorFilters, BooleanSupplier enabledSupplier);

	/**
	 * Add a Sensor string picker to generic page
	 *
	 * @param pageTitle : Title of the page
	 * @param pageDescription : Description of the page
	 * @param labelArgument : label of widget
	 * @param value : double argument
	 * @param inputFiles : root directory for data or files to parse
	 * @param selectedSensor : resulting picked sensor
	 * @param sensorFilters : sensor types to search for
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addSensorToGenericPage(String pageTitle, String pageDescription, String labelArgument,
			WritableObjectList<File> inputFiles, WritableString selectedSensor, WritableSet<String> sensorFilters,
			BooleanSupplier enabledSupplier);

	/**
	 * Add a GenericCheckListWizardPage wizard page for the layers contains in the input files.
	 *
	 * @param pageTitle : Title of the page.
	 * @param inputFiles : files in input
	 * @param filters : resulting filters
	 */
	default IWizardBuilder addWcFiltersPage(String pageTitle, WritableObjectList<File> inputFiles,
			WritableString filters) {
		return addWcFiltersPage(pageTitle, inputFiles, filters, () -> true);
	}

	/**
	 * Add a GenericCheckListWizardPage wizard page for the layers contains in the input files.
	 *
	 * @param pageTitle : Title of the page.
	 * @param inputFiles : files in input
	 * @param filters : resulting filters
	 * @param enabledSupplier : Supply false to skip the page
	 */
	IWizardBuilder addWcFiltersPage(String pageTitle, WritableObjectList<File> inputFiles, WritableString filters,
			BooleanSupplier enabledSupplier);

	/**
	 * Add a SelectPolygonMaskFilesPage wizard page for defining a polygons mask
	 * 
	 * @param polygonMask polygon files mask
	 * @param reversePolygonMask true when mask is reversed
	 */
	IWizardBuilder addSelectPolygonMaskPage(WritableObjectList<File> polygonMask, WritableBoolean reversePolygonMask);

	/**
	 * Add a ISummarizableWizardPage wizard page.
	 *
	 * @param page : the custom page.
	 */
	IWizardBuilder addCustomPage(ISummarizableWizardPage page);

	/**
	 * Add a summary page.
	 */
	IWizardBuilder addSummaryPage();

	/** First display page is the summary one */
	IWizardBuilder firstPageIsSummary();

	/** Build the wizard */
	Wizard build();

}
