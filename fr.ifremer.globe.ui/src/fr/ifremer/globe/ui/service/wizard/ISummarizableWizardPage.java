/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.wizard;

import org.eclipse.jface.wizard.IWizardPage;

/**
 * WizardPage page which can be added to generated wi
 */
public interface ISummarizableWizardPage extends IWizardPage {

	/**
	 * Ask the page to produce a summary of the edition.<br>
	 * This summary will be displayed at the end of the wizard, in a final summary page.
	 * 
	 * @return the summary. Null to mask it completly
	 */
	String summarize();

	/**
	 * @return true to enable the page. When false, the page is skipped by the wizard.
	 */
	default boolean isEnabled() {
		return true;
	}
}
