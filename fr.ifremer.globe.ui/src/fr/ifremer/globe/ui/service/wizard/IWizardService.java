/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.service.wizard;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Wizard services
 */
public interface IWizardService {

	/**
	 * Returns the service implementation of IFileService.
	 */
	static IWizardService grab() {
		return OsgiUtils.getService(IWizardService.class);
	}

	/** Creates a new Wizard builder */
	IWizardBuilder newWizardBuilder();
}
