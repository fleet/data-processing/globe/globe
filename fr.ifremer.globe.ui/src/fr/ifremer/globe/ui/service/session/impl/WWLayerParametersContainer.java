/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.session.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.primitives.IntList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import fr.ifremer.globe.core.utils.gson.IntListTypeAdapter;

/**
 * Container to store any Json bean of WWLayers parameters.
 */
@SuppressWarnings("serial")
public class WWLayerParametersContainer extends //
		HashMap<String /* filePath */, //
				Map<String /* Layer Name */, //
						Map<String /* Parameter name */ , Object/* Parameter */ >>> {

	protected static final Logger logger = LoggerFactory.getLogger(WWLayerParametersContainer.class);

	/** GSon utility */
	protected final transient Gson gson;

	/**
	 * Constructor
	 */
	public WWLayerParametersContainer() {
		gson = new GsonBuilder() //
				.setPrettyPrinting() //
				.registerTypeHierarchyAdapter(IntList.class, new IntListTypeAdapter().nullSafe())//
				.create();
	}

	/** Put the parameters of a WW layer */
	public void put(String filePath, String wwLayerName, Map<String, Object> wwParams) {
		var layersMap = computeIfAbsent(filePath, key -> new HashMap<>());
		layersMap.put(wwLayerName, wwParams);
	}

	/** Restore the properties of a WW layer parameter */
	@SuppressWarnings("unchecked")
	public <T> T restore(String filePath, String wwLayerName, String wwParamName, T wwParam) {

		// In case of error, return the incoming bean
		T result = wwParam;

		// Any layers in session for this file ?
		var layerMap = get(filePath);
		if (layerMap != null) {
			// Any parameter in session for this layer ?
			var parameterMap = layerMap.get(wwLayerName);
			if (parameterMap != null) {
				// Is this param in session ?
				var parameter = parameterMap.remove(wwParamName);

				// Cleaning when all parameters have been processed
				if (parameterMap.isEmpty()) {
					layerMap.remove(wwLayerName);
					if (layerMap.isEmpty()) {
						remove(filePath);
					}
				}

				if (parameter instanceof Map) { // Instance of Map means that parameters come from the session file
					try {
						// Trick to create a new instance of the parameter using json deserialization
						result = (T) gson.fromJson(gson.toJson(parameter), wwParam.getClass());
					} catch (JsonSyntaxException e) {
						logger.debug("Unable to restore the parameter '{}' of layer '{}' : {}", wwParamName,
								wwLayerName, e.getMessage());
					}
				} else if (wwParam.getClass().isInstance(parameter)) {
					result = (T) wwParam.getClass().cast(parameter);
				}
			}
		}

		return result;
	}

	/**
	 * @return true when parameters exist for the specified file/layer
	 */
	public boolean contains(String filePath, String wwLayerName) {
		boolean result = false;
		// Any layers in session for this file ?
		var layerMap = get(filePath);
		if (layerMap != null) {
			result = layerMap.containsKey(wwLayerName);
		}
		return result;
	}
}
