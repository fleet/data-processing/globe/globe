package fr.ifremer.globe.ui.service.session.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoEvent.FileInfoState;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.session.IFilePropertiesContainer;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.session.impl.XmlSessionSerializer.XmlSessionData;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.workspace.WorkspaceDirectory;
import fr.ifremer.globe.utils.map.TypedMap;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Class handling session operations sur as load/unload, save and reaction on app life events
 */
@Component(name = "globe_ui_service_session_service", service = ISessionService.class)
public class SessionController implements ISessionService {
	protected static final Logger logger = LoggerFactory.getLogger(SessionController.class);

	/** Default json session file */
	public static final String DEFAULT_SESSION_NAME = "defaultsession.json";
	/** Old format xml session file */
	public static final String DEFAULT_XML_SESSION_NAME = "defaultsession.xml";

	protected FilePropertiesContainer propertiesContainer;

	/**
	 * Initial layer parameters restored from the session file <br>
	 * Contains also all parameters of closed files
	 */
	protected WWLayerParametersContainer wwLayerParametersContainer;

	@Inject
	protected FileInfoModel fileInfoModel;
	@Inject
	protected WWFileLayerStoreModel wwfileLayerStoreModel;
	@Inject
	protected IFileLoadingService fileLoadingService;
	@Inject
	protected IGeographicViewService geographicViewService;

	@Inject
	protected ProjectExplorerModel projectExplorerModel;

	/** Subject that emits a save session request */
	protected PublishSubject<Boolean> savePublishSubject = PublishSubject.create();

	/** List of listeners to notify when the session is about to be saved */
	private final List<Runnable> preSaveSessionListeners = new LinkedList<>();

	/**
	 * Constructor
	 */
	public SessionController() {
		propertiesContainer = new FilePropertiesContainer(this);
		wwLayerParametersContainer = new WWLayerParametersContainer();
		initialize();
	}

	/** Initialize this service when model becomes available */
	public void initialize() {
		// Save the file every 10 seconds if needed
		savePublishSubject.sample(10, TimeUnit.SECONDS)//
				.observeOn(Schedulers.io())//
				.subscribe(//
						bool -> saveDefaultSession(), //
						error -> logger.warn("Error while saving session file", error));
	}

	@PostConstruct
	public void postConstruct() {
		fileInfoModel.addListener(this::onFileInfoEvent);
		wwfileLayerStoreModel.addListener(this::onWwFileLayerStoreEvent);
	}

	/**
	 * Saves session at closing.
	 */
	@Inject
	@Optional
	private void onAppShutdownStarted(@UIEventTopic(UIEvents.UILifeCycle.APP_SHUTDOWN_STARTED) Event event) {
		saveDefaultSession();
	}

	@Override
	public void saveSession(boolean relativePath, File destination) throws FileNotFoundException, IOException {

		cleanContainers();

		WWLayerParametersContainer beanStore = new WWLayerParametersContainer();
		beanStore.putAll(wwLayerParametersContainer); // Usefull to save parameters of closed files
		recordWwLayerParameters(beanStore);

		JsonSessionSerializer sessionSerializer = new JsonSessionSerializer(destination);
		sessionSerializer.setRelativePath(relativePath);
		sessionSerializer.write(fileInfoModel, projectExplorerModel.getDataNode(), geographicViewService.getLayers(),
				propertiesContainer, beanStore);

	}

	@Override
	public void saveSession() {
		savePublishSubject.onNext(Boolean.TRUE);
	}

	public void saveDefaultSession() {
		try {
			preSaveSessionListeners.forEach(Runnable::run);
			File sessionFile = new File(WorkspaceDirectory.getInstance().getPath(), DEFAULT_SESSION_NAME);
			logger.debug("Saving session {}", sessionFile.getPath());
			saveSession(false, sessionFile);
		} catch (IOException e) {
			logger.error("Error saving " + DEFAULT_SESSION_NAME, e);
		}
	}

	@Override
	public boolean loadSession() {
		// compute the session path
		File sessionFile = new File(WorkspaceDirectory.getInstance().getPath(), DEFAULT_SESSION_NAME);
		if (sessionFile.exists()) {
			try {
				JsonSessionSerializer sessionSerializer = new JsonSessionSerializer(sessionFile);
				sessionSerializer.read(propertiesContainer, c -> wwLayerParametersContainer = c);
				return true;
			} catch (Exception e) {
				// save session file to a backup_file
				LocalDateTime now = LocalDateTime.now();
				File sessionCopy = new File(WorkspaceDirectory.getInstance().getPath(),
						"session_" + now.getYear() + "_" + now.getMonth() + "_" + now.getDayOfMonth() + "_"
								+ now.getHour() + "_" + now.getMinute() + "_" + now.getSecond() + ".json");
				try {
					FileUtils.copyFile(sessionFile, sessionCopy);
				} catch (IOException e1) {
					logger.error("Error while copying old session file to " + DEFAULT_SESSION_NAME, e);
				}
				Messages.openErrorMessage("Error while loading session", "Error while loading " + DEFAULT_SESSION_NAME
						+ " a copy of the source file was created under the name " + sessionCopy, e);
			}
		} else {
			loadOldXmlDefaultSession();
		}
		// no default session => no errors
		return true;
	}

	private void loadOldXmlDefaultSession() {
		File sessionFile = new File(WorkspaceDirectory.getInstance().getPath(), DEFAULT_XML_SESSION_NAME);
		if (sessionFile.exists()) {
			try {
				var xmlSessionSerializer = new XmlSessionSerializer(sessionFile);
				var xmlSessionData = xmlSessionSerializer.read();
				// Something ?
				if (xmlSessionData != null) {
					var jsonSessionData = new JsonSessionData();
					xmlToJson(jsonSessionData, xmlSessionData.dataGroupNode);
					JsonSessionSerializer sessionSerializer = new JsonSessionSerializer(sessionFile);
					sessionSerializer.parseSession(jsonSessionData);
				} else {
					logger.warn("Unable to parse the Xml session file {}", DEFAULT_XML_SESSION_NAME);
				}
			} catch (Exception e) {
				logger.error("Error while parsing xml session " + DEFAULT_XML_SESSION_NAME, e);
			}
		}
	}

	private void loadOldXmlSession(File sessionFile) {
		if (sessionFile.exists()) {
			try {
				var xmlSessionSerializer = new XmlSessionSerializer(sessionFile);
				var xmlSessionData = xmlSessionSerializer.read();
				// Something ?
				if (xmlSessionData != null) {
					var jsonSessionData = new JsonSessionData();
					xmlToJson(jsonSessionData, xmlSessionData.dataGroupNode);
					JsonSessionSerializer sessionSerializer = new JsonSessionSerializer(sessionFile);
					sessionSerializer.parseSession(jsonSessionData);
				} else {
					logger.warn("Unable to parse the Xml session file {}", DEFAULT_XML_SESSION_NAME);
				}
			} catch (Exception e) {
				logger.error("Error while parsing xml session " + DEFAULT_XML_SESSION_NAME, e);
			}
		}
	}

	protected void xmlToJson(JsonSessionData jsonSessionData, XmlSessionData.XmlGroupNode groupNode) {
		if (groupNode != null && groupNode != null && groupNode.children != null) {
			for (XmlSessionData.XmlNode treeNode : groupNode.children) {
				if (treeNode instanceof XmlSessionData.FolderNode) {
					// XML folder => JSON folder
					var xmlFolder = (XmlSessionData.FolderNode) treeNode;
					var jsonGroupNode = new JsonSessionData.JsonGroupNode();
					jsonGroupNode.id = xmlFolder.uid;
					jsonGroupNode.name = xmlFolder.name;
					jsonGroupNode.comments = xmlFolder.comments;
					jsonGroupNode.parentId = groupNode.uid;
					jsonGroupNode.index = groupNode.children.indexOf(treeNode);
					jsonSessionData.projectExplorerGroupNodes.add(jsonGroupNode);
					xmlToJson(jsonSessionData, xmlFolder);
				} else if (treeNode instanceof XmlSessionData.FileNode) {
					// XML file => JSON file
					var xmlFile = (XmlSessionData.FileNode) treeNode;
					var jsonFileInfoNode = new JsonSessionData.JsonFileInfoNode();
					jsonFileInfoNode.name = xmlFile.name;
					jsonFileInfoNode.path = xmlFile.file;
					jsonFileInfoNode.parentId = groupNode.uid;
					jsonFileInfoNode.index = groupNode.children.indexOf(treeNode);
					jsonSessionData.projectExplorerFileInfoNode.add(jsonFileInfoNode);
					if (xmlFile.loaded) {
						jsonSessionData.fileInfoModel.add(jsonFileInfoNode.path);
					}
				}
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void loadSession(File sessionFile) throws IOException {
		projectExplorerModel.getDataNode().removeChildren();

		// clean current session
		List<String> loadedFiles = fileInfoModel.getAll().stream()//
				.map(IFileInfo::getPath)//
				.collect(Collectors.toList());
		if (!loadedFiles.isEmpty()) {
			fileLoadingService.unload(loadedFiles);
		}

		if (sessionFile.getName().endsWith(".json")) {
			JsonSessionSerializer sessionSerializer = new JsonSessionSerializer(sessionFile);
			sessionSerializer.read(propertiesContainer, c -> wwLayerParametersContainer = c);
		} else {
			loadOldXmlSession(sessionFile);
		}
	}

	/** Clean all known FileSettings when file is deleted */
	protected void onFileInfoEvent(FileInfoEvent event) {
		if (event.getState() == FileInfoState.RELOADING || event.getState() == FileInfoState.REMOVED
				|| event.getState() == FileInfoState.ERROR) {
			propertiesContainer.remove(event.getfileInfo().getPath());
			wwLayerParametersContainer.remove(event.getfileInfo().getPath());
		}
		if (event.getState() != FileInfoState.LOADING) {
			saveSession();
		}
	}

	/**
	 * When some IWWLayer are added to the model, try to restore their parameters from the session <br>
	 * When a file is closed, keep the layer parameters in session
	 */
	protected void onWwFileLayerStoreEvent(WWFileLayerStoreEvent event) {
		if (event.getState() == FileLayerStoreState.ADDED) {
			restoreWwLayerParameters(event.getFileLayerStore());
		}
	}

	/** {@inheritDoc} */
	@Override
	public void saveFilesParameters(List<String> filePaths) {
		for (String filePath : filePaths) {
			wwfileLayerStoreModel.get(filePath).ifPresent(this::saveClosedWwLayerParameters);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void removeFilesParameters(List<String> filePaths) {
		for (String filePath : filePaths) {
			propertiesContainer.remove(filePath);
			wwLayerParametersContainer.remove(filePath);
		}
	}

	/**
	 * Clean propertiesContainer. Remove properties of unknown files
	 */
	protected void cleanContainers() {
		Set<String> fileNames = new HashSet<>(propertiesContainer.getProperties().keySet());
		fileNames.addAll(wwLayerParametersContainer.keySet());
		for (String filePath : fileNames) {
			if (projectExplorerModel.getFileInfoNode(filePath).isEmpty()) {
				propertiesContainer.remove(filePath);
				wwLayerParametersContainer.remove(filePath);
			}
		}
	}

	/** Browse all IWWLayer and save all parameters in the container */
	protected void recordWwLayerParameters(WWLayerParametersContainer beanStore) {
		wwfileLayerStoreModel.getAll().forEach(fileLayerStore -> recordWwLayerParameters(beanStore, fileLayerStore));
	}

	/** Browse all IWWLayer of a layerStore and save all parameters in the container */
	protected void recordWwLayerParameters(WWLayerParametersContainer beanStore, WWFileLayerStore fileLayerStore) {
		String filePath = fileLayerStore.getFileInfo().getPath();
		for (IWWLayer wwLayer : fileLayerStore.getLayers()) {
			TypedMap paramEntry = wwLayer.describeParametersForSession();
			if (!paramEntry.isEmpty()) {
				beanStore.put(filePath, wwLayer.getName(), paramEntry.toMap());
			}
		}
		if (fileLayerStore.getElevationModel() instanceof IWWElevationModel) {
			var elevationModel = (IWWElevationModel) fileLayerStore.getElevationModel();
			TypedMap paramEntry = elevationModel.describeParametersForSession();
			beanStore.put(filePath, "ElevationModel_" + elevationModel.getName(), paramEntry.toMap());
		}
	}

	/** Restore all parameters of IWWLayers contained in the specified store */
	protected void restoreWwLayerParameters(WWFileLayerStore fileLayerStore) {
		String filePath = fileLayerStore.getFileInfo().getPath();
		var allLayers = fileLayerStore.getLayers();
		for (IWWLayer wwLayer : allLayers) {
			restoreWwLayerParameters(filePath, wwLayer);

			wwLayer.getDuplicationProvider().ifPresent(provider -> {
				// Search some duplicated layer in session : same layer name with a suffix
				for (int suffix = 1; suffix < Integer.MAX_VALUE; suffix++) {
					String clonedLayerName = wwLayer.getName() + "_" + suffix;
					if (wwLayerParametersContainer.contains(filePath, clonedLayerName)) {
						// Some parameters exist for a clone
						IWWLayer newLayer = provider.apply(clonedLayerName);
						wwLayer.getIcon().ifPresent(newLayer::setIcon);
						wwfileLayerStoreModel.addLayersToStore(fileLayerStore.getFileInfo(), newLayer);
						restoreWwLayerParameters(filePath, newLayer);
					} else {
						break;
					}
				}

			});
		}
		if (fileLayerStore.getElevationModel() instanceof IWWElevationModel) {
			var elevationModel = (IWWElevationModel) fileLayerStore.getElevationModel();
			restoreWWElevationModelParameters(filePath, elevationModel);
		}
	}

	/** Restore all parameters of IWWLayers contained in the specified store */
	protected void saveClosedWwLayerParameters(WWFileLayerStore fileLayerStore) {
		recordWwLayerParameters(wwLayerParametersContainer, fileLayerStore);
		saveSession();
	}

	/** Restore all parameters of one IWWLayer */
	protected void restoreWwLayerParameters(String filePath, IWWLayer wwLayer) {
		TypedMap wwParams = wwLayer.describeParametersForSession();
		for (var paramEntry : wwParams.entrySet()) {
			Object propertyBean = wwLayerParametersContainer.restore(filePath, wwLayer.getName(),
					paramEntry.key().getName(), paramEntry.value());
			wwParams.put(paramEntry.bindObject(propertyBean));
		}

		// Restore the parameters. (
		// Don't asyncExec, to make sure that the session restoration happens before the layers synchronisation process
		wwLayer.setSessionParameter(wwParams);
	}

	/** Restore all parameters of one IWWLayer */
	protected void restoreWWElevationModelParameters(String filePath, IWWElevationModel elevationModel) {
		TypedMap wwParams = elevationModel.describeParametersForSession();
		for (var paramEntry : wwParams.entrySet()) {
			Object propertyBean = wwLayerParametersContainer.restore(filePath,
					"ElevationModel_" + elevationModel.getName(), paramEntry.key().getName(), paramEntry.value());
			wwParams.put(paramEntry.bindObject(propertyBean));
		}

		// Informs the layer that its parameters have changed
		Display.getDefault().asyncExec(() -> {
			try {
				elevationModel.setSessionParameter(wwParams);
			} catch (Exception e) {
				logger.debug("Unable to restore the parameters of IWWElevationModel '{}' : {}",
						elevationModel.getName(), e.getMessage());
			}
		});
	}

	/**
	 * @return the {@link #propertiesContainer}
	 */
	@Override
	public IFilePropertiesContainer getPropertiesContainer() {
		return propertiesContainer;
	}

	/** {@inheritDoc} */
	@Override
	public void addPreSaveSessionListener(Runnable listener) {
		preSaveSessionListeners.add(listener);
	}

	/** Return true if the session contains parameters for the specified layer */
	@Override
	public boolean doesSessionContainsLayer(String filePath, String wwLayerName) {
		return wwLayerParametersContainer.contains(filePath, wwLayerName);
	}

}
