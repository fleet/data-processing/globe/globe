/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.session.impl;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.session.IFilePropertiesContainer;
import fr.ifremer.globe.utils.bean.AwtColorTypeConverter;
import fr.ifremer.globe.utils.bean.StringTypeConverter;

/**
 * A generic container to store any properties grouped by realm.
 */
public class FilePropertiesContainer implements IFilePropertiesContainer, PropertyChangeListener {

	private static final Logger logger = LoggerFactory.getLogger(FilePropertiesContainer.class);

	protected SessionController sessionController;
	protected BeanUtilsBean bub;

	/** Properties container */
	protected Map<String, // filePath
			Map<String, // realm
					// Property / Value
					Map<String, String>>> properties = new HashMap<>();

	/** All beans on which a addPropertyChangeListener has been set */
	protected List<ListenBean> listenBeans = new LinkedList<>();

	/**
	 * Constructor
	 */
	public FilePropertiesContainer(SessionController sessionController) {
		this.sessionController = sessionController;

		ConvertUtilsBean cub = new ConvertUtilsBean();
		cub.deregister(String.class);
		cub.register(new AwtColorTypeConverter(), Color.class);
		cub.register(new StringTypeConverter(), String.class);
		bub = new BeanUtilsBean(cub, new PropertyUtilsBean());
	}

	/** {@inheritDoc} */
	@Override
	public Map<String, String> get(String filePath, String realm) {
		return Collections.unmodifiableMap(//
				properties.getOrDefault(filePath, Collections.emptyMap()) // Get or create the Realm map
						.getOrDefault(realm, new HashMap<>()));// Get or create the properties map
	}

	/** {@inheritDoc} */
	@Override
	public void add(String filePath, String realm, Map<String, String> properties) {
		this.properties.computeIfAbsent(filePath, key -> new HashMap<>()).put(realm, new HashMap<>(properties));
		sessionController.saveSession();
	}

	/**
	 * {@inheritDoc} <br>
	 * Method synchronized to avoid ConcurrentModificationException on {@link #listenBeans}
	 */
	@Override
	public synchronized void monitor(String filePath, String realm, Object bean, PropertyChangeSupport pcs) {
		removePropertyChangeListener(filePath, realm);

		try {
			Map<String, String> props = properties.computeIfAbsent(filePath, key -> new HashMap<>()).get(realm);
			// Properties exists for this bean. Restore them
			if (props != null) {
				bub.populate(bean, props);
			} else {
				props = bub.describe(bean);
				if (props != null && !props.isEmpty()) {
					add(filePath, realm, props);
				}
			}
			addPropertyChangeListener(filePath, realm, bean, pcs);
		} catch (Exception e) {
			logger.debug("Unable to store the bean into the session", e);
		}
	}

	/**
	 * {@inheritDoc} <br>
	 * Method synchronized to avoid ConcurrentModificationException on {@link #listenBeans}
	 */
	@Override
	public synchronized void remove(String filePath) {
		removePropertyChangeListener(filePath);
		if (properties.remove(filePath) != null) {
			sessionController.saveSession();
		}
	}

	/**
	 * @return the {@link #properties}
	 */
	public Map<String, Map<String, Map<String, String>>> getProperties() {
		return properties;
	}

	/**
	 * @param properties the {@link #properties} to set
	 */
	public void setProperties(Map<String, Map<String, Map<String, String>>> properties) {
		this.properties = properties;
	}

	/** Stop listening the property changes for a file */
	protected void removePropertyChangeListener(String filePath) {
		List<ListenBean> listeners = listenBeans.stream() //
				.filter(listenBean -> listenBean.filePath.equals(filePath))//
				.peek(listenBean -> listenBean.pcs.removePropertyChangeListener(this)) //
				.collect(Collectors.toList());
		listenBeans.removeAll(listeners);
	}

	/** Stop listening the property changes for a file/realm */
	protected void removePropertyChangeListener(String filePath, String realm) {
		listenBeans.stream() //
				.filter(listenBean -> listenBean.filePath.equals(filePath))//
				.filter(listenBean -> listenBean.realm.equals(realm))//
				.forEach(listenBean -> listenBean.pcs.removePropertyChangeListener(this));
	}

	/** Add or replace this as PropertyChangeListener to the specified PropertyChangeSupport */
	protected void addPropertyChangeListener(String filePath, String realm, Object bean, PropertyChangeSupport pcs) {
		removePropertyChangeListener(filePath, realm);

		ListenBean listenBean = new ListenBean();
		listenBean.filePath = filePath;
		listenBean.realm = realm;
		listenBean.bean = bean;
		listenBean.pcs = pcs;
		listenBeans.add(0, listenBean);

		pcs.addPropertyChangeListener(this);
	}

	/**
	 * {@inheritDoc} <br>
	 * Method synchronized to avoid ConcurrentModificationException on {@link #listenBeans}
	 */
	@Override
	public synchronized void propertyChange(PropertyChangeEvent evt) {
		synchronized (listenBeans) { // Avoid ConcurrentModificationException
			ListenBean listenBean = listenBeans.stream() //
					.filter(listener -> listener.bean == evt.getSource())//
					.findFirst().orElse(null);
			if (listenBean != null) {

				try {
					Map<String, String> props = bub.describe(listenBean.bean);
					if (props != null && !props.isEmpty()) {
						add(listenBean.filePath, listenBean.realm, props);
						sessionController.saveSession();
					}
				} catch (Exception e) {
					logger.debug("Unable to store the bean into the session", e);
				}
			}
		}
	}

	static class ListenBean {
		public String filePath;
		public String realm;
		public Object bean;
		public PropertyChangeSupport pcs;
	}

	/** {@inheritDoc} */
	@Override
	public void addConverter(Converter converter, Class<?> clazz) {
		bub.getConvertUtils().register(converter, clazz);
	}

	/**
	 * Allows SessionController to change the property Bean linked to a file and a realm without triggering the save of
	 * the session
	 */
	void put(String filePath, String realm, Object bean) {
		try {
			Map<String, String> props = bub.describe(bean);

			if (props != null && !props.isEmpty()) {
				properties.computeIfAbsent(filePath, key -> new HashMap<>()).put(realm, props);
			}
		} catch (Exception e) {
			logger.debug("Unable to store the bean from the session", e);
		}
	}

}
