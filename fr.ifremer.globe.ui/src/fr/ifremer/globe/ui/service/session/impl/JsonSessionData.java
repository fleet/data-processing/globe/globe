/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.session.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

/**
 * JSON Persistant model of the session
 */
public class JsonSessionData {

	/** All files contained if the FileInfoModel */
	public List<String> fileInfoModel = new ArrayList<>();
	/** All created GroupNodes in the project explorer even DataGroup */
	public List<JsonGroupNode> projectExplorerGroupNodes = new ArrayList<>();
	/** All FileInfoNode in the project explorer */
	public List<JsonFileInfoNode> projectExplorerFileInfoNode = new ArrayList<>();
	/** All WW layers in the layer list view, keeping order */
	public List<JsonWwLayer> wwLayersOrder = new ArrayList<>();

	/** Properties container */
	public Map<String /* filePath */, Map<String /* realm */ , Map<String, String> /* Propeties */>> properties = new HashMap<>();
	/** Container of layer parameters */
	@SerializedName("wwLayersParameters")
	public WWLayerParametersContainer wwLayerParametersContainer = new WWLayerParametersContainer();

	public static class JsonFileInfoNode {
		public String name;
		public String path;
		public String parentId;
		public int index;
		public boolean expanded;
	}

	public static class JsonGroupNode {
		public String id;
		public String name;
		public String comments;
		public String parentId;
		public int index;
		public boolean expanded;
	}

	public static class JsonWwLayer {
		public String name;
		public boolean enabled;
	}
}
