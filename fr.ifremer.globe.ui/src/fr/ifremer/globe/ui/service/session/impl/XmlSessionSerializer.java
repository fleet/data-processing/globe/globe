/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.session.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamInclude;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import fr.ifremer.globe.ui.service.session.impl.XmlSessionSerializer.XmlSessionData.XmlNode;

/**
 * Utility class to convert an old xml session file
 */
public class XmlSessionSerializer {

	protected static final Logger logger = LoggerFactory.getLogger(XmlSessionSerializer.class);

	/** File to read */
	protected File sessionXmlFile;

	/** To to transform all paths in as relative to the session file */
	protected boolean relativePath = false;

	/**
	 * Constructor
	 */
	public XmlSessionSerializer(File sessionJsonFile) {
		sessionXmlFile = sessionJsonFile;
	}

	/** Launch the reading of the session */
	public XmlSessionData read() {
		XmlSessionData result = null;
		try {
			XStream xstream = new XStream() {
				@Override
				protected MapperWrapper wrapMapper(MapperWrapper next) {
					return new MapperWrapper(next) {
						@Override
						public Class<?> realClass(String elementName) {
							try {
								return super.realClass(elementName);
							} catch (CannotResolveClassException e) {
								// Ignore unknown elements (xstream.ignoreUnknownElements does not work in all cases)
								return XmlNode.class;
							}
						}
					};
				}
			};
			xstream.processAnnotations(XmlSessionData.class);
			xstream.setClassLoader(XmlSessionData.DataGroupNode.class.getClassLoader());
			xstream.ignoreUnknownElements();
			result = (XmlSessionData) xstream.fromXML(sessionXmlFile);
		} catch (Exception e) {
			logger.warn("Unable to parse the xml file ", sessionXmlFile.getName(), e);
		}
		return result;

	}

	/**
	 * Old XML Persistant model of the session
	 */
	@XStreamAlias("Session")
	public static class XmlSessionData {

		@XStreamAlias("fr.ifremer.globe.model.infostores.DataGroupNode")
		public DataGroupNode dataGroupNode;

		@XStreamInclude({ FolderNode.class, FileNode.class, MarkerFileNode.class })
		public static class XmlNode {
			public String uid;
			public String name;
		}

		public static class XmlGroupNode extends XmlNode {
			public List<XmlNode> children;
		}

		@XStreamAlias("fr.ifremer.globe.model.infostores.FolderNode")
		public static class FolderNode extends XmlGroupNode {
			public String comments;
		}

		@XStreamAlias("fr.ifremer.globe.model.infostores.InfoStoreNode")
		public static class FileNode extends XmlNode {
			public boolean loaded;
			public String file;
		}

		@XStreamAlias("fr.ifremer.viewer3d.layernodes.MarkerStoreNode")
		public static class MarkerFileNode extends FileNode {
		}

		public static class DataGroupNode extends XmlGroupNode {
		}

		public static class FakeNode {
		}

	}

}
