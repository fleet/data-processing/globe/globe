/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.service.session.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import jakarta.inject.Inject;

import org.apache.commons.collections.primitives.IntList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.gson.IntListTypeAdapter;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.service.session.impl.JsonSessionData.JsonGroupNode;
import fr.ifremer.globe.ui.service.session.impl.JsonSessionData.JsonWwLayer;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode.LoadedState;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;

/**
 * Utility class to save and reload a session in JSON format
 */
public class JsonSessionSerializer {

	/** File to read or save */
	protected File sessionJsonFile;

	/** To to transform all paths in as relative to the session file */
	protected boolean relativePath = false;

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	/**
	 * Constructor
	 */
	public JsonSessionSerializer(File sessionJsonFile) {
		ContextInitializer.inject(this);
		this.sessionJsonFile = sessionJsonFile;
	}

	/** Launch the reading of the session */
	public void read(FilePropertiesContainer propertiesContainer,
			Consumer<WWLayerParametersContainer> layerParametrsConsumer) throws IOException {
		String jsonString = Files.readString(sessionJsonFile.toPath(), StandardCharsets.UTF_8);
		if (jsonString != null && !jsonString.isBlank()) {
			Gson gson = makeGson();
			JsonSessionData sessionData = gson.fromJson(jsonString, JsonSessionData.class);
			if (sessionData != null) {
				// First restore properties (including all "file_settings")
				var properties = new HashMap<String, Map<String, Map<String, String>>>();
				sessionData.properties.entrySet().stream()
						.forEach(entry -> properties.put(toAbsolutePath(entry.getKey()), entry.getValue()));
				propertiesContainer.setProperties(properties);

				// Transmit the ww layer parameters
				layerParametrsConsumer.accept(sessionData.wwLayerParametersContainer);
				// Restore the session and load files
				parseSession(sessionData);
			}
		}
	}

	/** Write the session */
	void write(FileInfoModel fileInfoModel, GroupNode dataGroupNode, LayerList layerList,
			FilePropertiesContainer propertiesContainer, WWLayerParametersContainer beanStore) throws IOException {
		if (!sessionJsonFile.exists() || sessionJsonFile.delete()) {
			JsonSessionData sessionData = new JsonSessionData();

			// Nodes
			for (TreeNode child : dataGroupNode.getChildren()) {
				if (child instanceof FolderNode folderNode) {
					writeNode(sessionData, folderNode, dataGroupNode);
				} else if (child instanceof FileInfoNode fileInfoNode) {
					writeNode(sessionData, fileInfoNode, dataGroupNode);
				}
			}

			// Properties
			sessionData.properties = new HashMap<>();
			propertiesContainer.properties.entrySet().stream().forEach(
					entry -> sessionData.properties.put(transformPath(new File(entry.getKey())), entry.getValue()));

			// Beans
			sessionData.wwLayerParametersContainer = beanStore;

			// Loaded files
			sessionData.fileInfoModel = fileInfoModel.getAll().stream()//
					.map(IFileInfo::toFile)//
					.map(this::transformPath)//
					.toList();

			for (Layer wwLayer : layerList) {
				JsonWwLayer jsonWwLayer = new JsonWwLayer();
				jsonWwLayer.name = wwLayer.getName();
				jsonWwLayer.enabled = wwLayer.isEnabled();
				sessionData.wwLayersOrder.add(jsonWwLayer);
			}

			// Save
			Gson gson = makeGson();
			sessionJsonFile.getParentFile().mkdirs();
			Files.writeString(sessionJsonFile.toPath(), gson.toJson(sessionData), StandardCharsets.UTF_8,
					StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

		}

	}

	private void writeNode(JsonSessionData sessionData, FolderNode node, GroupNode parentNode) {
		JsonSessionData.JsonGroupNode jsonGroupNode = new JsonSessionData.JsonGroupNode();
		jsonGroupNode.id = node.getUid().toString();
		jsonGroupNode.name = node.getName();
		jsonGroupNode.expanded = node.isExpanded();
		jsonGroupNode.comments = node.getComments();
		if (parentNode != null) {
			jsonGroupNode.parentId = parentNode.getUid().toString();
			jsonGroupNode.index = parentNode.getChildren().indexOf(node);
		} else {
			jsonGroupNode.parentId = null;
			jsonGroupNode.index = 0;
		}
		sessionData.projectExplorerGroupNodes.add(jsonGroupNode);

		for (TreeNode child : node.getChildren()) {
			if (child instanceof FolderNode folderNode) {
				writeNode(sessionData, folderNode, node);
			} else if (child instanceof FileInfoNode fileInfoNode) {
				writeNode(sessionData, fileInfoNode, node);
			}
		}
	}

	private void writeNode(JsonSessionData sessionData, FileInfoNode node, GroupNode parentNode) {
		JsonSessionData.JsonFileInfoNode jsonFileInfoNode = new JsonSessionData.JsonFileInfoNode();
		jsonFileInfoNode.name = node.getName();
		jsonFileInfoNode.path = transformPath(node.getFileInfo().toFile());
		jsonFileInfoNode.expanded = node.isExpanded();
		if (parentNode != null) {
			jsonFileInfoNode.parentId = parentNode.getUid().toString();
			jsonFileInfoNode.index = parentNode.getChildren().indexOf(node);
		} else {
			jsonFileInfoNode.parentId = null;
			jsonFileInfoNode.index = 0;
		}
		sessionData.projectExplorerFileInfoNode.add(jsonFileInfoNode);

	}

	protected String transformPath(File file) {
		String result = file.getAbsolutePath();
		if (relativePath) {
			Path filePath = file.toPath();
			Path sessionFolderPath = sessionJsonFile.getParentFile().toPath();
			if (filePath.getRoot() != null && sessionFolderPath.getRoot() != null
					&& filePath.getRoot().equals(sessionFolderPath.getRoot())) {
				result = sessionFolderPath.relativize(filePath).toString();
			}
		}
		return result;
	}

	protected String toAbsolutePath(String filePath) {
		Path path = Paths.get(filePath);
		if (!path.isAbsolute()) {
			path = Paths.get(sessionJsonFile.getParent(), filePath);
		}
		return path.normalize().toAbsolutePath().toString();
	}

	/** Restore the session and load the files */
	public void parseSession(JsonSessionData sessionData) {
		Map<String, GroupNode> groupNodes = new HashMap<>();

		groupNodes.put(null, projectExplorerModel.getDataNode());

		// First creates folders.
		for (JsonGroupNode jsonGroupNode : sessionData.projectExplorerGroupNodes) {
			GroupNode groupNode = ITreeNodeFactory.grab().createOrphanFolderNode(jsonGroupNode.name,
					jsonGroupNode.comments);
			groupNode.setExpanded(jsonGroupNode.expanded);
			groupNodes.put(jsonGroupNode.id, groupNode);
		}

		// Hook folders.
		for (JsonGroupNode jsonGroupNode : sessionData.projectExplorerGroupNodes) {
			var parent = groupNodes.getOrDefault(jsonGroupNode.parentId, groupNodes.get(null));
			if (parent != null) {
				parent.addChild(groupNodes.get(jsonGroupNode.id));
			}
		}

		for (JsonSessionData.JsonFileInfoNode jsonFileInfoNode : sessionData.projectExplorerFileInfoNode) {
			var parent = groupNodes.getOrDefault(jsonFileInfoNode.parentId, groupNodes.get(null));
			if (parent != null) {
				BasicFileInfo fileInfo = new BasicFileInfo(toAbsolutePath(jsonFileInfoNode.path));
				FileInfoNode node = ITreeNodeFactory.grab().createFileInfoNode(fileInfo, parent,
						jsonFileInfoNode.index);
				node.setExpanded(jsonFileInfoNode.expanded);
				// Check that file already exists
				if (!fileInfo.toFile().exists()) {
					node.setLoadStatus(LoadedState.NOT_FOUND);
				}
			}
		}

		// Predicted layers
		IGeographicViewService.grab().setPredictedLayers(//
				sessionData.wwLayersOrder.stream()//
						.map(layer -> new Pair<>(layer.name, layer.enabled))//
						.toList()//
		);

		// Load files...
		IFileLoadingService.grab().load(sessionData.fileInfoModel.stream().map(this::toAbsolutePath).toList(), true);
	}

	/** Creates an instance of Gson */
	private Gson makeGson() {
		return new GsonBuilder() //
				.setPrettyPrinting() //
				.registerTypeHierarchyAdapter(IntList.class, new IntListTypeAdapter().nullSafe()) //
				.serializeSpecialFloatingPointValues() //
				.create();
	}

	/**
	 * @return the {@link #relativePath}
	 */
	public boolean isRelativePath() {
		return relativePath;
	}

	/**
	 * @param relativePath the {@link #relativePath} to set
	 */
	public void setRelativePath(boolean relativePath) {
		this.relativePath = relativePath;
	}
}
