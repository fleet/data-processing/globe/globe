package fr.ifremer.globe.ui.service.parametersview.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import jakarta.inject.Inject;

/**
 * This service retrieves all {@link IParametersViewCompositeFactory} and uses them to build the parameter view.
 */
@Component(name = "globe_parameters_view_service", service = IParametersViewService.class)
public class ParametersViewService implements IParametersViewService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParametersViewService.class);
	@Inject
	EPartService partService;

	/** Constructor **/
	public ParametersViewService() {
		ContextInitializer.inject(this);
	}

	/** All registered {@link IParametersViewCompositeFactory} */
	protected List<IParametersViewCompositeFactory> compositeFactories = new ArrayList<>();

	/** Register a new composite factory */
	@Reference(name = "IParametersViewCompositeFactory", cardinality = ReferenceCardinality.MULTIPLE)
	public void addParametersViewCompositeFactory(IParametersViewCompositeFactory provider) {
		compositeFactories.add(provider);
		compositeFactories.sort((f1, f2) -> f1.getPriority() > f2.getPriority() ? 1 : -1);
	}

	/**
	 * Gets children composites from available factories.
	 */
	@Override
	public List<Composite> getComposites(Object selection, Composite parent) {
		return compositeFactories.stream() //
				.map(factory -> {
					Optional<Composite> optComposite = Optional.empty();
					try {
						optComposite = factory.getComposite(selection, parent);
					} catch (Exception e) {
						// avoid crash if an exception occurs in factory
						LOGGER.error("Paramaters view : error while getting composite from factory : "
								+ factory.getClass().getName(), e);
					}
					if (optComposite == null)
						System.err.println();
					return optComposite;
				}) //
				.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
	}

	/**
	 * Builds the composite to display in parameter view.
	 */
	@Override
	public Composite buildComposite(Object selection, Composite parent) {
		// create the result composite
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(1, true));

		// get composites to display
		List<Composite> composites = getComposites(selection, composite);

		// if selection is FileInfoNode, search composite for first children
		if (composites.isEmpty() && selection instanceof FileInfoNode fileInfoNode
				&& !fileInfoNode.getChildren().isEmpty())
			composites = getComposites(fileInfoNode.getChildren().get(0), composite);

		// apply layout
		composites.forEach(c -> c.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1)));

		return composite;
	}

	@Override
	public void refresh() {
		MPart part = null;
		try {
			part = getPart();
		} catch (IllegalStateException ex) {
			// part not available
		}
		Optional.ofNullable(part).map(MPart::getObject).map(ParametersView.class::cast)
				.ifPresent(parametersView -> Display.getDefault().asyncExec(parametersView::refresh));
	}

	@Override
	public void resize() {
		MPart part = null;
		try {
			part = getPart();
		} catch (IllegalStateException ex) {
			// part not available
		}
		Optional.ofNullable(part).map(MPart::getObject).map(ParametersView.class::cast)
				.ifPresent(parametersView -> Display.getDefault().asyncExec(parametersView::resize));
	}

	@Override
	public MPart getPart() {
		return partService.findPart(ParametersView.ID);
	}

}
