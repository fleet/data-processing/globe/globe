package fr.ifremer.globe.ui.service.parametersview;

import java.util.List;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Service used to build the parameter view composite.
 */
public interface IParametersViewService {

	/**
	 * Returns the service implementation of IGeographicViewService.
	 */
	static IParametersViewService grab() {
		return OsgiUtils.getService(IParametersViewService.class);
	}

	/**
	 * @return list of children composites for the specified selection.
	 */
	List<Composite> getComposites(Object selection, Composite parent);

	/**
	 * Builds the composite to display in parameter view.
	 */
	Composite buildComposite(Object selection, Composite parent);

	/**
	 * Refreshes the parameter view
	 */
	void refresh();

	/**
	 * Resizes the parameter view
	 */
	void resize();

	/**
	 * @return {@link MPart} for Parameters view
	 */
	MPart getPart();

}
