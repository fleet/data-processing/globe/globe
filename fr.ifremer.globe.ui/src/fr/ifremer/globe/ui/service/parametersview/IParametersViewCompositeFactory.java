package fr.ifremer.globe.ui.service.parametersview;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;

/**
 * Parameter view composite factory.
 */
public interface IParametersViewCompositeFactory {

	/**
	 * @return a composite to display for the current selection, if no one, returns an empty optional.
	 */
	public Optional<Composite> getComposite(Object selection, Composite parent);

	/**
	 * @return priority of this factory (define the order of composites). By default = 10. Higher priority is 1.
	 */
	public default int getPriority() {
		return 10;
	}

}
