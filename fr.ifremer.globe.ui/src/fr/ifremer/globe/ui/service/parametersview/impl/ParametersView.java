package fr.ifremer.globe.ui.service.parametersview.impl;

import java.util.List;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.event.WWLayerEvent;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Parameters view.
 */
public class ParametersView implements ISaveable {

	public static final String ID = "fr.ifremer.globe.parametersView";

	/** Injected {@link IParametersViewService} **/
	@Inject
	IParametersViewService parametersViewService;

	@Inject
	FileInfoModel fileInfoModel;

	@Inject
	WWFileLayerStoreModel fileLayerStoreModel;

	/** Scrolled Composite */
	private ScrolledComposite scrolledComposite;

	/** The current composite */
	private Composite composite;

	/** The last selection */
	private Optional<Object> selection = Optional.empty();

	/**
	 * Creates UI : builds a {@link ScrolledComposite} which will contains parameter composites.
	 */
	@PostConstruct
	public void createUI(Composite parent) {
		parent.setLayout(new FillLayout());

		// create scrolledComposite
		scrolledComposite = new ScrolledComposite(parent, SWT.V_SCROLL | SWT.H_SCROLL);
		scrolledComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		scrolledComposite.setBackgroundMode(SWT.INHERIT_DEFAULT);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.addListener(SWT.Resize, event -> resize());

		fileInfoModel.addListener(this::onFileInfoEvent);

		refresh();
	}

	/**
	 * Sets the selected element whose the parameters have to be display.
	 */
	private void setSelection(Optional<Object> newSelection) {
		if (!newSelection.isEmpty() && !selection.equals(newSelection)) { // 
			selection = newSelection;
			asyncRefresh();
		}
	}

	/**
	 * Refreshes on {@link Display} thread.
	 */
	private void asyncRefresh() {
		Display.getDefault().asyncExec(this::refresh);
	}

	/**
	 * Re-builds the composite to display.
	 */
	void refresh() {
		if (scrolledComposite == null || scrolledComposite.isDisposed())
			return;

		if (composite != null) {
			composite.dispose();
			scrolledComposite.setContent(null);
		}
		if (selection.isPresent()) {
			composite = parametersViewService.buildComposite(selection.get(), scrolledComposite);
			scrolledComposite.setContent(composite);
			resize();
		}
	}

	/**
	 * Resizes composite depending on scroll width and child composites
	 */
	void resize() {
		if (composite != null && scrolledComposite != null && !composite.isDisposed()) {
			composite.setSize(composite.computeSize(
					scrolledComposite.getParent().getSize().x - scrolledComposite.getVerticalBar().getSize().x,
					SWT.DEFAULT));
		}
	}

	/**
	 * Handles event of a new selection
	 */
	@Inject
	public void onSelection(
			@org.eclipse.e4.core.di.annotations.Optional @Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection strSelection) {
		if (strSelection != null && SelectionUtils.stream(strSelection).map(Optional::of).anyMatch(selection::equals))
			return; // current selection is contained in ACTIVE_SELECTION : nothing to do
		setSelection(strSelection != null ? Optional.ofNullable(strSelection.getFirstElement()) : Optional.empty());
	}

	/**
	 * Handles Eclipse events (see {@link IEventBroker}) of topic {@link WWLayerEvent}.
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onLayerSelected(@UIEventTopic(WWLayerEvent.TOPIC_LAYER_SELECTED) IWWLayer... layers) {
		var layerList = List.of(layers);
		// do nothing if one of the selected layers is the current selection
		if (selection.filter(layerList::contains).isPresent()
				// check also if the current selection is not a FileInfo which contains one of the selected layers
				|| (selection.isPresent() && selection.get() instanceof FileInfoNode fileInfoNode)
						&& (fileLayerStoreModel.get(fileInfoNode.getFileInfo())
								.filter(layerStore -> layerStore.getLayers().stream().anyMatch(layerList::contains))
								.isPresent()))
			return;
		setSelection(layers.length > 0 ? Optional.ofNullable(layers[0]) : Optional.empty());
	}

	/**
	 * Asks to refresh if current selection is a file which has been updated.
	 */
	private void onFileInfoEvent(FileInfoEvent event) {
		// get optional current selected IFileInfo (if directly selected or from FileInfoNode)
		Optional<IFileInfo> optCurrentFileInfo = selection.filter(IFileInfo.class::isInstance)
				.map(IFileInfo.class::cast).or(() -> selection.filter(FileInfoNode.class::isInstance)
						.map(FileInfoNode.class::cast).map(FileInfoNode::getFileInfo));

		// compare FileInfo from selection and event (compare also path)
		if (optCurrentFileInfo.isPresent() && (optCurrentFileInfo.get().equals(event.getfileInfo())
				|| optCurrentFileInfo.get().getAbsolutePath().equals(event.getfileInfo().getAbsolutePath())))
			asyncRefresh();
	}

	@Focus
	public void setFocus() {
		scrolledComposite.setFocus();
	}

	/**
	 * @see fr.ifremer.globe.model.ISaveable#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return composite instanceof ISaveable saveable && saveable.isDirty();
	}

	/**
	 * @see fr.ifremer.globe.model.ISaveable#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	@Persist
	public boolean doSave(IProgressMonitor monitor) {
		return composite instanceof ISaveable saveable && saveable.doSave(monitor);
	}
}