package fr.ifremer.globe.ui.providers;

/*
 * @License@ 
 */

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import fr.ifremer.globe.ui.tree.ITreeNode;

/**
 * Content provider pour la vue arborescente.
 * 
 * @author Guillaume Bourel &lt;guillaume.bourel@altran.com&gt;
 * 
 */
public class TreeContentProvider implements ITreeContentProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof ITreeNode) {
			List<ITreeNode> children = ((ITreeNode) parentElement).getChildren();
			if (children != null) {
				return children.toArray();
			}
		} else if (parentElement instanceof Object[]) {
			return (Object[]) parentElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getParent(Object element) {
		return ((ITreeNode) element).getParent();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasChildren(Object element) {
		return ((ITreeNode) element).hasChildren();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
