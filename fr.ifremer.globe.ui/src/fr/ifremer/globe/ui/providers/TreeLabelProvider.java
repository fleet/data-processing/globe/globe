package fr.ifremer.globe.ui.providers;

/*
 * @License@ 
 */

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.ui.tree.ITreeNode;

/**
 * @author Guillaume Bourel &lt;guillaume.bourel@altran.com&gt;
 * 
 */
public class TreeLabelProvider extends LabelProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText(Object element) {
		return ((ITreeNode) element).getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Image getImage(Object element) {
		return ((ITreeNode) element).getImage();
	}

}
