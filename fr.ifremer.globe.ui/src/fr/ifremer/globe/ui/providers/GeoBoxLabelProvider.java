/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.providers;

import java.util.function.Function;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;

/**
 * ColumnLabelProvider to map any GeoBox coord to optional text representation
 */
public class GeoBoxLabelProvider extends ColumnLabelProvider {

	/**
	 * Make a LabelProvider to map the north latitude to its optional text representation
	 * 
	 * @param geoBoxSupplier supplier of GeoGox. GeoGox may be null
	 * @return the LabelProvider
	 */
	public static GeoBoxLabelProvider getNorthLabelProvider(Function<Object, GeoBox> geoBoxSupplier) {
		return new GeoBoxLabelProvider(geoBoxSupplier,
				geobox -> LatLongFormater.getFormatter().formatLatitude(geobox.getTop()));
	}

	/**
	 * Make a LabelProvider to map the south latitude to its optional text representation
	 * 
	 * @param geoBoxSupplier supplier of GeoGox. GeoGox may be null
	 * @return the LabelProvider
	 */
	public static GeoBoxLabelProvider getSouthLabelProvider(Function<Object, GeoBox> geoBoxSupplier) {
		return new GeoBoxLabelProvider(geoBoxSupplier,
				geobox -> LatLongFormater.getFormatter().formatLatitude(geobox.getBottom()));
	}

	/**
	 * Make a LabelProvider to map the west longitude to its optional text representation
	 * 
	 * @param geoBoxSupplier supplier of GeoGox. GeoGox may be null
	 * @return the LabelProvider
	 */
	public static GeoBoxLabelProvider getWestLabelProvider(Function<Object, GeoBox> geoBoxSupplier) {
		return new GeoBoxLabelProvider(geoBoxSupplier,
				geobox -> LatLongFormater.getFormatter().formatLongitude(geobox.getLeft()));
	}

	/**
	 * Make a LabelProvider to map the east longitude to its optional text representation
	 * 
	 * @param geoBoxSupplier supplier of GeoGox. GeoGox may be null
	 * @return the LabelProvider
	 */
	public static GeoBoxLabelProvider getEastLabelProvider(Function<Object, GeoBox> geoBoxSupplier) {
		return new GeoBoxLabelProvider(geoBoxSupplier,
				geobox -> LatLongFormater.getFormatter().formatLatitude(geobox.getRight()));
	}

	/** Supplier of GeoGox. GeoGox may be null */
	protected Function<Object, GeoBox> supplier;
	/** Map the GeoBox to its String represintation */
	protected Function<GeoBox, String> formatter;

	/**
	 * Constructor
	 */
	public GeoBoxLabelProvider(Function<Object, GeoBox> supplier, Function<GeoBox, String> formatter) {
		this.supplier = supplier;
		this.formatter = formatter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText(Object element) {
		GeoBox geoBox = supplier.apply(element);
		return geoBox != null ? formatter.apply(geoBox) : null;
	}
}
