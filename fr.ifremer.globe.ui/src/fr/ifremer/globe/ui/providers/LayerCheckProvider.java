package fr.ifremer.globe.ui.providers;

import org.eclipse.jface.viewers.ICheckStateProvider;

import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode.CheckState;

public class LayerCheckProvider implements ICheckStateProvider {

	@Override
	public boolean isChecked(Object element) {
		return element instanceof TreeNode treeNode
				&& (treeNode.getState() == CheckState.TRUE || treeNode.getState() == CheckState.PARTIAL);
	}

	@Override
	public boolean isGrayed(Object element) {
		return element instanceof TreeNode treeNode && treeNode.getState() == CheckState.PARTIAL;
	}

}
