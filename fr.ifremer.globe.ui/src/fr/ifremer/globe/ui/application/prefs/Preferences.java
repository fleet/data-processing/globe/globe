/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.application.prefs;

import java.util.Optional;

import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;
import fr.ifremer.globe.ui.handler.PartManager;

/**
 * Preferences used in this bundle.
 */
public class Preferences {

	// Processes preferences
	private final BooleanPreferenceAttribute loadFilesAfterConversion;
	private final StringPreferenceAttribute consoleViewContainerId;
	private final StringPreferenceAttribute defaultProjectionName;

	// View preferences
	private final BooleanPreferenceAttribute showHistogramInColorContrast;
	private final BooleanPreferenceAttribute syncTexturePlayersOnTime;
	private final BooleanPreferenceAttribute syncWcPayersOnTime;

	/**
	 * Constructor
	 */
	public Preferences() {
		var processesSettingsNode = PreferenceRegistry.getInstance().getProcessesSettingsNode();
		loadFilesAfterConversion = new BooleanPreferenceAttribute("loadFilesAfterConversion",
				"Load files after conversion", true);
		processesSettingsNode.declareAttribute(loadFilesAfterConversion);
		consoleViewContainerId = new StringPreferenceAttribute("consoleViewContainerId",
				"Favorite container ID of the console views", PartManager.RIGHT_STACK_ID);
		processesSettingsNode.declareAttribute(consoleViewContainerId);
		defaultProjectionName = new StringPreferenceAttribute("defaultProjectionName", "Preferred projection name",
				StandardProjection.LONGLAT.name());
		processesSettingsNode.declareAttribute(defaultProjectionName);
		processesSettingsNode.load();

		var viewsSettingsNode = PreferenceRegistry.getInstance().getViewsSettingsNode();
		showHistogramInColorContrast = new BooleanPreferenceAttribute("showHistogramInContrastColor",
				"Show histogram in color contrast panel", true);
		viewsSettingsNode.declareAttribute(showHistogramInColorContrast);
		syncTexturePlayersOnTime = new BooleanPreferenceAttribute("syncTexturePlayersOnTime",
				"Synchronize texture players on time", true);
		viewsSettingsNode.declareAttribute(syncTexturePlayersOnTime);
		syncWcPayersOnTime = new BooleanPreferenceAttribute("syncWcPayersOnTime", "Synchronize WC players on time",
				true);
		viewsSettingsNode.declareAttribute(syncWcPayersOnTime);
		viewsSettingsNode.load();
	}

	/** Save preferences */
	public void save() {
		PreferenceRegistry.getInstance().getRootNode().save();
	}

	/**
	 * @return the {@link #loadFilesAfterConversion}
	 */
	public BooleanPreferenceAttribute getLoadFilesAfterConversion() {
		return loadFilesAfterConversion;
	}

	/**
	 * @return the {@link #showHistogramInColorContrast}
	 */
	public BooleanPreferenceAttribute getShowHistogramInColorContrast() {
		return showHistogramInColorContrast;
	}

	/**
	 * @return the {@link #syncTexturePlayersOnTime}
	 */
	public BooleanPreferenceAttribute getSyncTexturePlayersOnTime() {
		return syncTexturePlayersOnTime;
	}

	/**
	 * @return the {@link #syncWcPayersOnTime}
	 */
	public BooleanPreferenceAttribute getSyncWcPayersOnTime() {
		return syncWcPayersOnTime;
	}

	/**
	 * @return the {@link #consoleViewContainerId}
	 */
	public StringPreferenceAttribute getConsoleViewContainerId() {
		return consoleViewContainerId;
	}

	/** Return the ProjectionSettings stored in preferences */
	public ProjectionSettings getPreferredProjection() {
		Optional<ProjectionSettings> result = StandardProjection.fromFriendlyName(defaultProjectionName.getValue());
		return result.orElse(StandardProjection.LONGLAT);
	}

	/** Return the ProjectionSettings stored in preferences */
	public void setPreferredProjection(ProjectionSettings projectionSettings) {
		defaultProjectionName.setValue(projectionSettings.name(), false);
		PreferenceRegistry.getInstance().getProcessesSettingsNode().save();
	}

}
