/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.application.event;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;

/**
 * All topics managed in WW layer processing
 */
public class UILayerEventTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/globe/model/ui/layer";

	/**
	 * Topic to inform that a component claims the generation of WW layer<br>
	 * The event is associated to an instance of RasterInfo, LayerRequestInfo or PointCloudRequestInfo
	 */
	public static final String TOPIC_WW_LAYERS_REQUESTED = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_WW_LAYERS_REQUESTED";

	/** Object associated to a TOPIC_WW_LAYERS_REQUESTED for requesting a WW layer of IFileInfo */
	public record LayerRequestInfo(IFileInfo fileInfo, WWLayerDescription layerDesc, IProgressMonitor monitor,
			Logger logger) {
	}

	/** Object associated to a TOPIC_WW_LAYERS_REQUESTED for requesting a PointCloudTexture */
	public record PointCloudRequestInfo(IFileInfo fileInfo, PointCloudFieldInfo pointCloudFieldInfo) {
	}

	/**
	 * Constructor
	 */
	private UILayerEventTopics() {
		// private constructor to hide the implicit public one.
	}

}
