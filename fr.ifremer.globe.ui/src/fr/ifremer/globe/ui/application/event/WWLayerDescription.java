/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.application.event;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;

/**
 * Description of a IWWLayer witch can be loaded by sending a UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED
 * 
 * @param name futur name of the layer
 * @param type futur type of the layer
 * @param syncKey synchonization key of the layer. This the id used to identify the same layers. Layers of same key may
 *            have their parameters synchronized
 */
public record WWLayerDescription(String name, Class<? extends IWWLayer> type, String syncKey) {

}
