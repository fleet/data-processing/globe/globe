/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.application.context;

import jakarta.inject.Inject;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.application.prefs.Preferences;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureDataCache;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerController;

/**
 * Application Addon used to initialize the IEclipseContext
 */
public class ContextInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContextInitializer.class);

	/** Static reference of Eclipse context initialized by E4Application */
	protected static IEclipseContext eclipseContext;

	/**
	 * Invoked when Globe application is started.<br>
	 * Fill the context with needed components
	 *
	 * @param injectedEclipseContext Eclipse context initialized by E4Application
	 */
	@Inject
	@Optional
	public void activate(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext injectedEclipseContext) {
		activate(injectedEclipseContext);
	}

	/**
	 * Fill the context with needed components and initialize the context
	 */
	public static void activate(IEclipseContext eclipseContext) {
		setEclipseContext(eclipseContext);
		init();
	}

	/**
	 * Init the singleton instance, create by DI, it registers reference instances of objects used througout the
	 * NavShift application with current context
	 */
	protected static void init() {
		// Init preferences
		getEclipseContext().set(Preferences.class, make(Preferences.class));

		// Useful to substribe by annotation to IEventBroker and inject E4 components
		IFileLoadingService fileLoadingService = getInContext(IFileLoadingService.class);
		inject(fileLoadingService);
		inject(getInContext(IWWLayerService.class));
		inject(getInContext(ITreeNodeFactory.class));
		inject(getInContext(IProcessService.class));
		inject(getInContext(ISessionService.class));
		inject(getInContext(IFileService.class));
		inject(getInContext(IWWTextureDataCache.class));

		ColorMap.load();

		// request the loading of file in command line
		try {
			CommandLine commandLine = Activator.getCommandLine();
			String fileToLoad = commandLine.getOptionValue("load");
			if (fileToLoad != null)
				fileLoadingService.reload(fileToLoad);
		} catch (ParseException e) {
			LOGGER.warn("Unable to parse the command line", e);
		}

		make(ProjectExplorerController.class);

	}

	/**
	 * Getter of {@link #eclipseContext}
	 */
	public static IEclipseContext getEclipseContext() {
		return eclipseContext;
	}

	/**
	 * Setter of {@link #eclipseContext}
	 */
	public static void setEclipseContext(IEclipseContext newEclipseContext) {
		eclipseContext = newEclipseContext;
	}

	/**
	 * Returns the context value associated with the given name
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInContext(String name) {
		return (T) getEclipseContext().get(name);
	}

	public static <T> T getInContext(Class<T> clazz) {
		return getEclipseContext().get(clazz);
	}

	/**
	 * Sets a value to be associated with a given name in this context
	 */
	public static void setInContext(String name, Object value) {
		getEclipseContext().set(name, value);
	}

	/**
	 * Removes the given name and any corresponding value from this context
	 */
	public static void removeFromContext(String name) {
		getEclipseContext().remove(name);
	}

	/**
	 * @return an instance of the specified class and inject it with the context.
	 */
	public static <T> T make(Class<T> clazz) {
		return ContextInjectionFactory.make(clazz, eclipseContext);
	}

	/**
	 * Injects a context into a domain object.
	 */
	public static void inject(Object object) {
		ContextInjectionFactory.inject(object, getEclipseContext());
	}

}
