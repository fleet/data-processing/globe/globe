package fr.ifremer.globe.ui;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;

/**
 * Formatted text cell editor with an error decoration.
 * 
 * @author ctphanle
 * 
 */
public class DecoratedFormattedText extends FormattedText {

	private static final FieldDecoration INFO = FieldDecorationRegistry.getDefault().getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION);

	private static final FieldDecoration ERROR = FieldDecorationRegistry.getDefault().getFieldDecoration(FieldDecorationRegistry.DEC_ERROR);

	private ControlDecoration controlDecoration;

	private IValidator validator;

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            the composite parent
	 * @param returnDisplayString
	 *            <code>true</code> return the display string as a value
	 */
	public DecoratedFormattedText(Text parent, IValidator validator) {
		super(parent);
		this.validator = validator;
		controlDecoration = new ControlDecoration(getControl(), SWT.TOP | SWT.RIGHT);
		controlDecoration.hide();
		text.addModifyListener(new ModifyCellEditorListener());
	}

	@Override
	public Object getValue() {
		return getFormatter().getValue();
	}

	@Override
	public boolean isValid() {
		IStatus status = validator.validate(text.getText());
		return !status.matches(IStatus.ERROR);
	}

	private class ModifyCellEditorListener implements ModifyListener {

		@Override
		public void modifyText(ModifyEvent e) {
			IStatus status = validator.validate(text.getText());
			if (status.matches(IStatus.OK)) {
				controlDecoration.hide();
			} else if (status.matches(IStatus.INFO)) {
				controlDecoration.setImage(INFO.getImage());
				if( text.isVisible())
				controlDecoration.show();
			} else if (status.matches(IStatus.ERROR)) {
				controlDecoration.setImage(ERROR.getImage());
				if( text.isVisible())
				controlDecoration.show();
			}
			controlDecoration.setDescriptionText(status.getMessage());
		}
	}
}
