package fr.ifremer.globe.ui.handler;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

public abstract class AbstractNodeHandler {

	/** tell if menu shall be hidden or not when can execute return false */
	protected boolean hideMenu;

	public AbstractNodeHandler() {
		hideMenu = true;
	}

	public AbstractNodeHandler(boolean hideMenu) {
		this.hideMenu = hideMenu;
	}

	public boolean checkExecution(ESelectionService selectionService, MUIElement uiElement) {
		boolean canEx = computeCanExecute(selectionService);

		updateComponentVisibility(canEx, uiElement);

		return canEx;

	}

	protected void updateComponentVisibility(boolean visible, MUIElement uiElement) {
		/**
		 * show or hide execution
		 */
		if (hideMenu) {
			uiElement.setVisible(visible);
		} else {
			uiElement.setVisible(true);
		}

	}

	public Object[] getSelection(ESelectionService selectionService) {
		Object obj = selectionService.getSelection();
		if (obj instanceof StructuredSelection) {
			return ((StructuredSelection) obj).toArray();
		}
		return new Object[] {};
	}

	public List<Object> getSelection(ESelectionService selectionService, Predicate<Object> test) {
		return Arrays.stream(getSelection(selectionService)).filter(test).collect(Collectors.toList());
	}

	/**
	 * @return list of selected instance of the specified class
	 */
	public <T> List<T> getSelection(ESelectionService selectionService, Class<T> expectedClass) {
		return Arrays.stream(getSelection(selectionService)).filter(expectedClass::isInstance).map(expectedClass::cast)
				.toList();
	}

	/**
	 * @return an Optional containing the selected {@link FileInfoNode} only if one is selected.
	 */
	public Optional<FileInfoNode> getUniqueFileInfoNode(ESelectionService selectionService) {
		Object[] selection = getSelection(selectionService);
		return selection != null && selection.length == 1 && selection[0] instanceof FileInfoNode
				? Optional.of(((FileInfoNode) selection[0]))
				: Optional.empty();
	}

	/**
	 * @return the selection list filtered by class (the provided class must implemented by the selected
	 *         {@link IFileInfo})
	 */
	public <T> List<T> getSelectionAsList(ESelectionService selectionService, Class<T> fileInfoClass) {
		Object[] selection = getSelection(selectionService);
		return Arrays.stream(selection). //
				filter(FileInfoNode.class::isInstance).//
				map(FileInfoNode.class::cast). //
				filter(node -> node.getFileInfo() != null && fileInfoClass.isInstance(node.getFileInfo())). //
				map(FileInfoNode::getFileInfo).//
				map(fileInfoClass::cast).//
				collect(Collectors.toList());
	}

	/**
	 * @return the selection as a list of FileInfoNode filtered of a contendType test
	 */
	public List<FileInfoNode> getSelectionAsList(Object[] selectedElements, Predicate<ContentType> predicate) {
		return Arrays.stream(selectedElements). //
				filter(FileInfoNode.class::isInstance).//
				map(FileInfoNode.class::cast). //
				filter(node -> node.getFileInfo() != null && predicate.test(node.getFileInfo().getContentType())). //
				collect(Collectors.toList());
	}

	/**
	 * return the selection as a list of infostore, and check if the given driver accept selected data
	 *
	 * @param selectedElements the selection
	 * @param expectedClass the driver class
	 */
	// public List<FileInfoNode> getSelectionForDriver(Object[] selectedElements,
	// Class<? extends IDataDriver> expectedClass) {
	//
	// List<FileInfoNode> ret = new ArrayList<>();
	// DataDriverRegistry ddRegistry = DataDriverRegistry.getInstance();
	// if (ddRegistry != null) {
	// IDataDriver driver = ddRegistry.getDriver(expectedClass);
	// for (Object e : selectedElements) {
	// if (e instanceof FileInfoNode) {
	//
	// if (driver != null && ((FileInfoNode) e).getLoadedState() == LoadedState.eLoaded
	// && driver.accept(((FileInfoNode) e).getFileInfo().getPath())) {
	// ret.add((FileInfoNode) e);
	// }
	// }
	// }
	// }
	//
	// return ret;
	//
	// }

	protected abstract boolean computeCanExecute(ESelectionService selectionService);
}