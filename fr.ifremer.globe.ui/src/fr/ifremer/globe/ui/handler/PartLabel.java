package fr.ifremer.globe.ui.handler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Utility class used to compute labels and tooltips for Globe parts
 */
public class PartLabel {
	/**
	 * Compute labels given a list of files
	 */
	public static String computeLabel(List<File> files) {
		String label = "";
		if (files != null) {
			if (files.size() > 0) {
				label = files.get(0).getName();
			}
			if (files.size() > 1) {
				label = label + " ...";
				label = label + " " + files.get(files.size() - 1).getName();
			}
		}
		return label;
	}

	/**
	 * Compute labels given a list of FileInfoNode
	 */
	public static String computeLabelID(List<SounderDataContainer> dataContainers) {
		// retrive the file list
		ArrayList<String> filelist = new ArrayList<>();
		for (SounderDataContainer dc : dataContainers) {
			filelist.add(dc.getInfo().getPath());
		}
		return computeLabel(pathNames2Files(filelist));
	}

	/**
	 * Compute labels given a list of FileInfoNode
	 */
	public static String computeLabelI(List<ISounderNcInfo> nodes) {

		// retrive the file list
		List<File> filelist = nodes.stream().//
				map(ISounderNcInfo::getPath).//
				map(File::new).//
				collect(Collectors.toList());
		return computeLabel(filelist);
	}

	/**
	 * Compute labels given a list of pathnames
	 */
	public static String computeLabelP(List<String> filenames) {
		return computeLabel(pathNames2Files(filenames));
	}

	/**
	 * Compute tooltips given a list of pathnames
	 */
	public static String computeToolTipP(List<String> filenames) {

		return computeToolTip(pathNames2Files(filenames));
	}

	/**
	 * Compute tooltips given a list of pathnames
	 */
	public static String computeToolTipID(List<SounderDataContainer> dataContainers) {
		ArrayList<String> files = new ArrayList<>(dataContainers.size());
		for (SounderDataContainer dataContainer : dataContainers) {
			files.add(dataContainer.getInfo().getPath());
		}
		return computeToolTip(pathNames2Files(files));
	}

	/**
	 * Compute tooltips given a list of pathnames
	 */
	public static String computeToolTipI(List<FileInfoNode> nodes) {
		ArrayList<File> files = new ArrayList<>(nodes.size());
		for (FileInfoNode node : nodes) {
			files.add(node.getFile());
		}
		return computeToolTip(files);
	}

	/**
	 * Compute tooltips given a list of files
	 */
	public static String computeToolTip(List<File> files) {
		String label = "";
		if (files != null) {
			for (File file : files) {
				label += file.getName() + String.format("%n");
			}
		}
		return label;
	}

	protected static List<File> pathNames2Files(List<String> filenames) {
		ArrayList<File> files = new ArrayList<>();
		for (String file : filenames) {
			files.add(new File(file));
		}
		return files;
	}
}
