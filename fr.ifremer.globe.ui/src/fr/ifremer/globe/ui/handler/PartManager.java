package fr.ifremer.globe.ui.handler;

import java.util.List;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.commands.MBindingContext;
import org.eclipse.e4.ui.model.application.commands.MBindingTable;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MStackElement;
import org.eclipse.e4.ui.workbench.UIEvents.BindingContext;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.IPartListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PartManager {
	private static final Logger logger = LoggerFactory.getLogger(PartManager.class);

	public final static String LEFT_TOP_STACK_ID = "fr.ifremer.globe.partstack.topLeft";
	public final static String LEFT_BOTTOM_STACK_ID = "fr.ifremer.globe.partstack.bottomLeft";
	public final static String RIGHT_STACK_ID = "fr.ifremer.globe.partstack.right";
	public final static String RIGHT_BOTTOM_STACK_ID = "fr.ifremer.globe.partstack.rightbottom";

	public final static String RIGHT_RIGHT_STACK_ID = "fr.ifremer.globe.partstack.right.right";

	public final static String TIME_EDITOR_TOOLBOX_PART_ID = "fr.ifremer.globe.timeeditor.toolbox";
	public final static String TIME_EDITOR_TOOLBOX_STACK_ID = "fr.ifremer.globe.timeeditor.partstack";

	/**
	 * return a part if it exists, or create it if not existing
	 * */
	public static MPart createPart(EPartService partService, String partId) {
		MPart part = partService.findPart(partId);
		if (part == null) {
			part = partService.createPart(partId);
		}
		return part;
	}

	/**
	 * call create part
	 * */
	public static MPart forceCreatePart(EPartService partService, String partId) {
		return  partService.createPart(partId);
	}

	/**
	 * Adds a the undo/redo biding context to a part.
	 * @param part part where a biding context need to be added
	 * @param application application which contains the {@link BindingContext}
	 */
	public static void addEditableBindingContextToPart(MPart part, MApplication application) {
		for(MBindingTable bindingTable : application.getBindingTables()) {
			MBindingContext bindingContext = bindingTable.getBindingContext();
			if(bindingContext.getElementId().compareTo("fr.ifremer.globe.bindingcontext.editableParts")==0) {
				part.getBindingContexts().add(bindingContext);	
				return;
			}
		}
	}

	/**
	 * Add a part to a part stack.
	 * @param part {@link MStackElement} to add in the part stack
	 * @param application {@link MApplication}
	 * @param modelService {@link EModelService}
	 * @param partStackId id of the {@link MPartStack} where you want to add a new part.
	 */
	public static void addPartToStack(MStackElement part, MApplication application, EModelService modelService, String partStackId) {
		addToContainer(MPartStack.class, part, application, modelService, partStackId);
	}

	/**
	 * Add a part to the given container, if exits
	 * @param <TContainer>
	 * */
	protected static <TElement extends MUIElement, TContainer extends MElementContainer<TElement>> void addToContainer
	(Class<TContainer> containerClass,TElement element, MApplication application, EModelService modelService, String preferedContainerId) {

		if (element == null) {
			return;
		}

		// MUIElement a = modelService.find(preferedContainerId, application);
		List<TContainer> stacks = modelService.findElements(application, preferedContainerId, containerClass, null);
		if (!(stacks == null || stacks.size() == 0)) {

			stacks.get(0).getChildren().add(element);
		} else {
			// try to find a container
			stacks = modelService.findElements(application, null, containerClass, null);
			if (!(stacks == null || stacks.size() == 0)) {

				stacks.get(0).getChildren().add(element);
			} else {
				logger.warn("Cannot find stack " + preferedContainerId);
			}
		}

	}
	/**
	 * Add a part to the given container, if exits
	 * */
	public static void addToMPartSashContainer(MPartSashContainer sash, MApplication application, EModelService modelService, String preferedContainerId) {
		addToContainer(MPartSashContainer.class, sash, application, modelService, preferedContainerId);
	}
	public static void showPart(EPartService partService, MPart part) {
		partService.showPart(part, PartState.CREATE);
		partService.showPart(part, PartState.VISIBLE);
		partService.bringToTop(part);
	}

	/**
	 * Hides the part identified by its id.
	 */
	public static void hidePart(EPartService partService, String partId, boolean force) {
		MPart part = partService.findPart(partId);
		if( part != null) {
			partService.hidePart(part, force);	
		}
	}

	/**
	 * Empty IPartListener implementation.
	 */
	public static class PartAdapter implements IPartListener {

		@Override
		public void partActivated(MPart part) {
		}

		@Override
		public void partBroughtToTop(MPart part) {
		}

		@Override
		public void partDeactivated(MPart part) {
		}

		@Override
		public void partHidden(MPart part) {
		}

		@Override
		public void partVisible(MPart part) {
		}

	}
}
