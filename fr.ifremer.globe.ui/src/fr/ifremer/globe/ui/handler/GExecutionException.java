package fr.ifremer.globe.ui.handler;

import org.eclipse.core.commands.common.CommandException;

import fr.ifremer.globe.utils.exception.GException;

@SuppressWarnings("serial")
public class GExecutionException extends GException {

	public GExecutionException(String string, CommandException e) {
		super(string,e);
	}

}
