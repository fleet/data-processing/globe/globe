package fr.ifremer.globe.ui.handler;

import java.util.Collections;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.CommandManager;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GException;

@SuppressWarnings("restriction")
public class CommandUtils {

	private static Logger logger = LoggerFactory.getLogger(CommandUtils.class);

	public static void executeCommand(IEclipseContext context, String commandId) throws GException {
		// Obtain IServiceLocator implementer, e.g. from PlatformUI.getWorkbench():
		// IServiceLocator serviceLocator = PlatformUI.getWorkbench();
		// or a site from within a editor or view:
		// IServiceLocator serviceLocator = getSite();

		ECommandService commandService = context.get(ECommandService.class);

		// Lookup commmand with its ID
		Command command = commandService.getCommand(commandId);

		// Optionally pass a ExecutionEvent instance, default no-param arg creates blank event
		try {
			if (command == null)
				throw new BadParameterException("Command not found" + commandId);
			ExecutionEvent event = new ExecutionEvent(null, Collections.EMPTY_MAP, null, context);
			command.executeWithChecks(event);
		} catch (ExecutionException | NotDefinedException | NotEnabledException | NotHandledException e) {
			throw new GExecutionException("Cannot execute command" + commandId, e);
		}

	}

	/** Executes the command. On error, log a message */
	public static void executeCommand(CommandManager commandManager, String commandId) {
		Command command = commandManager.getCommand(commandId);
		try {
			if (command == null)
				throw new BadParameterException("Command not found" + commandId);
			command.executeWithChecks(new ExecutionEvent());
		} catch (Exception e) {
			logger.warn("Unable to launch the command {} : {}", commandId, e.getMessage());
		}
	}

}
