package fr.ifremer.globe.ui;

import fr.ifremer.globe.ui.undo.UndoStack;

/**
 * implements an undo redo stack
 * */
public interface IDataUndoable {

	/**
	 * Calls if app or plugin is closed.
	 */
	public abstract UndoStack getUndoStack();

	public void setDirty(boolean dirty);
}