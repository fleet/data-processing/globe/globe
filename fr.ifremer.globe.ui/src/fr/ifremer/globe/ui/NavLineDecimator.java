package fr.ifremer.globe.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import fr.ifremer.globe.core.model.ModelPreferences;
import fr.ifremer.globe.ui.algo.Decimate1Nth;
import fr.ifremer.globe.ui.algo.DecimateNone;
import fr.ifremer.globe.ui.algo.DouglasPeuckerDecimationAlgorithm;
import fr.ifremer.globe.utils.perfcounter.PerfoCounter;
import gov.nasa.worldwind.geom.Position;

public class NavLineDecimator{
	// the algorithm to use
	private static enum DecimationAlgorithm {
		eNth, eDouglasPeuckerAlgorithm, eNone
	}

	public static Integer DEFAULT_DECIMATION=255;

	// algorithm to use for decimation
	DecimationAlgorithm algorithm = DecimationAlgorithm.eDouglasPeuckerAlgorithm;
	/** Returns number of points for {@link #getReducedPoints()}. */
	private int expectedReducedPointCount() {
		return ModelPreferences.getInstance().getNavigationDecimation().getValue();
	}

	public NavLineDecimator()
	{

	}
	/**
	 * Returns a list of navigation points location with
	 * {@link #expectedReducedPointCount()} points at most.
	 */
	public List<Position> getReducedPoints(ANavLine<? extends Position> source)  {

		boolean dump2file = false;
		List<Position> points = null;
		if (dump2file) {
			dumpPoints2file(source.points, new File("c:\\navbrute.csv"));
		}
		this.algorithm=DecimationAlgorithm.eDouglasPeuckerAlgorithm;
		switch (this.algorithm) {
		
		case eDouglasPeuckerAlgorithm:
			PerfoCounter counter = new PerfoCounter("Decimation LinearityDecimationAlgorithm");
			counter.start();
			points = new DouglasPeuckerDecimationAlgorithm().decimate(expectedReducedPointCount(), source.points);
			counter.stop();
			if (dump2file) {
				dumpPoints2file(points, new File("c:\\navlisseeRDP.csv"));
			}
			break;
		case eNth:
			PerfoCounter counter1 = new PerfoCounter("Decimation Decimate1Nth");
			counter1.start();
			points = Decimate1Nth.decimate(expectedReducedPointCount(), source.points);
			if (dump2file) {
				dumpPoints2file(points, new File("c:\\navlissee1nth.csv"));
			}
			counter1.stop();
			break;
		case eNone:
			counter1 = new PerfoCounter("Decimation None");
			counter1.start();
			points = DecimateNone.decimate( source.points);
			if (dump2file) {
				dumpPoints2file(points, new File("c:\\navlissee1nth.csv"));
			}
			counter1.stop();
			break;
		default:

		}

		return points;
	}

	/**
	 * dumps to the file given in parameters (for test and validation)
	 * */
	private void dumpPoints2file(List<? extends Position> pointlist, File file) {
		if (file.exists()) {
			file.delete();
		}
		try {
			FileWriter out = new FileWriter(file);
			out.write("latitude;longitude;elevation\r\n");

			for (Position pos : pointlist) {
				out.write(pos.latitude.degrees + ";" + pos.longitude.degrees + ";" + pos.elevation + "\r\n");
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
