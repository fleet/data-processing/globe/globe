/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.layer;

import java.util.List;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import gov.nasa.worldwind.globes.ElevationModel;

/**
 *
 */
public class WWFileLayerStoreEvent {

	protected WWFileLayerStore fileLayerStore;
	protected FileLayerStoreState state;

	/** Indicates the list of updated layers */
	protected List<IWWLayer> updatedLayers;
	/** Elevation model if any in an update event */
	protected ElevationModel elevationModel;

	/**
	 * Constructor
	 */
	public WWFileLayerStoreEvent(WWFileLayerStore fileLayerStore, FileLayerStoreState state) {
		this.fileLayerStore = fileLayerStore;
		this.state = state;
	}

	/** Loading State of the file */
	public enum FileLayerStoreState {
		ADDED, REMOVED, UPDATED
	}

	/**
	 * @return the {@link #fileLayerStore}
	 */
	public WWFileLayerStore getFileLayerStore() {
		return fileLayerStore;
	}

	/**
	 * @return the {@link #state}
	 */
	public FileLayerStoreState getState() {
		return state;
	}

	/**
	 * @return the {@link #updatedLayers}
	 */
	public List<IWWLayer> getUpdatedLayers() {
		return updatedLayers;
	}

	/**
	 * @param updatedLayers the {@link #updatedLayers} to set
	 */
	public void setUpdatedLayers(List<IWWLayer> updatedLayers) {
		this.updatedLayers = updatedLayers;
	}

	/**
	 * @return the {@link #elevationModel}
	 */
	public ElevationModel getElevationModel() {
		return elevationModel;
	}

	/**
	 * @param elevationModel the {@link #elevationModel} to set
	 */
	public void setElevationModel(ElevationModel elevationModel) {
		this.elevationModel = elevationModel;
	}

}
