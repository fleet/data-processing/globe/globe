/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.layer;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.core.model.AbstractModelPublisher;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoUtils;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreEvent.FileLayerStoreState;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import jakarta.inject.Singleton;

/**
 * Model of loaded WWFileLayerStore in the geographic view<br>
 * This the cache of all Worldwind layer displayed in Globe
 */
@Creatable
@Singleton
public class WWFileLayerStoreModel extends AbstractModelPublisher<String, WWFileLayerStore, WWFileLayerStoreEvent> {

	@Override
	protected String getKey(WWFileLayerStore wWFileLayerStore) {
		return wWFileLayerStore.getFileInfo().getPath();
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<WWFileLayerStoreEvent> newAddEvent(WWFileLayerStore store) {
		return Optional.of(new WWFileLayerStoreEvent(store, FileLayerStoreState.ADDED));
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<WWFileLayerStoreEvent> newUpdateEvent(WWFileLayerStore store) {
		return Optional.of(new WWFileLayerStoreEvent(store, FileLayerStoreState.UPDATED));
	}

	/** {@inheritDoc} */
	@Override
	protected Optional<WWFileLayerStoreEvent> newRemovedEvent(WWFileLayerStore store) {
		return Optional.of(new WWFileLayerStoreEvent(store, FileLayerStoreState.REMOVED));
	}

	public boolean contains(IFileInfo fileInfo) {
		return contains(fileInfo.getPath());
	}

	public Optional<WWFileLayerStore> get(IFileInfo fileInfo) {
		return get(fileInfo.getPath());
	}

	/** Search and return the IFileInfo from which the layer was created */
	public Optional<IFileInfo> getFileInfo(IWWLayer layer) {
		return get(layer).map(WWFileLayerStore::getFileInfo);
	}

	public Optional<WWFileLayerStore> get(IWWLayer layer) {
		return getAll().stream().filter(wWFileLayerStore -> wWFileLayerStore.getLayers().contains(layer)).findFirst();
	}

	/** Update the model by adding or replacing raster layer on a WWFileLayerStore */
	public void update(WWRasterLayerStore rasterLayerStore) {
		WWFileLayerStore fileLayerStore = get(rasterLayerStore.getRasterInfo().getFilePath()).orElse(null);
		if (fileLayerStore != null) {
			fileLayerStore.addLayers(rasterLayerStore.getLayers());
			WWFileLayerStoreEvent event = newUpdateEvent(fileLayerStore).get();
			event.setUpdatedLayers(rasterLayerStore.getLayers());
			event.setElevationModel(rasterLayerStore.getElevationModel());
			submit(event);
		}
	}

	/** Update the model by adding layers on a WWFileLayerStore */
	public void addLayersToStore(IFileInfo fileInfo, IWWLayer... newLayers) {
		get(fileInfo).ifPresent(fileLayerStore -> addLayersToStore(fileLayerStore, newLayers));
	}

	/** Update the model by adding layers on a WWFileLayerStore */
	public void addLayersToStore(WWFileLayerStore fileLayerStore, IWWLayer... newLayers) {
		fileLayerStore.addLayers(newLayers);
		WWFileLayerStoreEvent event = newUpdateEvent(fileLayerStore).get();
		event.setUpdatedLayers(List.of(newLayers));
		submit(event);
	}

	/** Update the model by removing layers on a WWFileLayerStore */
	public void removeLayersToStore(WWFileLayerStore fileLayerStore, IWWLayer... newLayers) {
		fileLayerStore.removeLayers(newLayers);
		WWFileLayerStoreEvent event = newUpdateEvent(fileLayerStore).get();
		event.setUpdatedLayers(List.of(newLayers));
		submit(event);
	}

	/**
	 * @return all layers accepted by the specified {@link Predicate}.
	 */
	public List<IWWLayer> getLayers(Predicate<IWWLayer> test) {
		return getAll().stream().flatMap(wWFileLayerStore -> wWFileLayerStore.getLayers().stream()).filter(test)
				.toList();
	}

	/**
	 * @return all layers into the specified {@link GeoBox}.
	 */
	public List<IWWLayer> getLayersWithinGeoBox(GeoBox geoBox) {
		return getAll().stream() //
				.filter(wwFileLayerStore -> wwFileLayerStore.getFileInfo().getGeoBox() != null
						&& GeoUtils.overlaps(geoBox, wwFileLayerStore.getFileInfo().getGeoBox()))
				.flatMap(wWFileLayerStore -> wWFileLayerStore.getLayers().stream()).toList();
	}

	/**
	 * @return all layers of the specified type.
	 */
	public <T extends IWWLayer> Stream<T> getLayers(Class<T> clazz) {
		return getAll().stream() //
				// flatMap => get all layer in one stream
				.flatMap(wWFileLayerStore -> wWFileLayerStore.getLayers().stream()) //
				.filter(clazz::isInstance)//
				.map(clazz::cast);//
	}

	/** Add a listener and react on a layer action */
	public <T extends IWWLayer> void addListener(//
			Class<T> layerClass, //
			Consumer<T> onWcLayerAdded, //
			Consumer<T> onWcLayerUpdated, Consumer<T> onWcLayerDeleted) {
		addListener(event -> {
			for (IWWLayer layer : event.getFileLayerStore().getLayers()) {
				if (layerClass.isInstance(layer)) {
					if (event.getState() == FileLayerStoreState.ADDED) {
						onWcLayerAdded.accept(layerClass.cast(layer));
					} else if (event.getState() == FileLayerStoreState.UPDATED
							&& event.getUpdatedLayers().contains(layer)) {
						onWcLayerUpdated.accept(layerClass.cast(layer));
					} else if (event.getState() == FileLayerStoreState.REMOVED) {
						onWcLayerDeleted.accept(layerClass.cast(layer));
					}
				}
			}
		});
	}

}
