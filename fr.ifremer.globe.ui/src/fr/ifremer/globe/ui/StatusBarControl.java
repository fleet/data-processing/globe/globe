package fr.ifremer.globe.ui;

import java.util.LinkedList;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.ui.events.UpdateStatusBarErrorIndicator;
import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.utils.exception.GException;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

public class StatusBarControl {

	@Inject
	IEclipseContext context;
	@Inject
	UISynchronize sync;

	private IJobChangeListener myJobListener;
	private LinkedList<Job> runningJobs = new LinkedList<Job>();

	private Composite parent;
	private Label posLabel;
	private Label consoleLabel;
	private ProgressBarMonitor progressBar;

	boolean error = false;

	@PreDestroy
	public void dispose() {
		IJobManager manager = Job.getJobManager();
		manager.removeJobChangeListener(myJobListener);
	}

	/**
	 * Event for change in error count
	 */
	@Inject
	@Optional
	public void onErrorIndicatorChange(
			@UIEventTopic(UpdateStatusBarErrorIndicator.EVENT) UpdateStatusBarErrorIndicator evt) {
		final boolean error = evt.isError();
		UIUtils.asyncExecSafety(parent, () -> {
			setError(error);
			parent.layout(true);
		});
	}

	@PostConstruct
	public void postConstruct(Composite parent) {
		this.parent = parent;
		parent.setLayout(new GridLayout(8, false));

		posLabel = new Label(parent, SWT.NONE);
		posLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		posLabel.setText("");

		Label firstSeparator = new Label(parent, SWT.SEPARATOR | SWT.VERTICAL);
		GridData gd_firstSeparator = new GridData(SWT.CENTER, SWT.FILL, false, true, 1, 1);
		gd_firstSeparator.heightHint = 10;
		firstSeparator.setLayoutData(gd_firstSeparator);

		consoleLabel = new Label(parent, SWT.NONE);
		consoleLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		consoleLabel.setText("No Error");
		consoleLabel.addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(MouseEvent e) {

			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				try {
					CommandUtils.executeCommand(context, "fr.ifremer.globe.trimtop.toolbar.consolelog.commands.show");
				} catch (GException ex) {

				}
			}
		});

		Label secondSeparator = new Label(parent, SWT.SEPARATOR | SWT.VERTICAL);
		GridData gd_secondSeparator = new GridData(SWT.CENTER, SWT.FILL, false, true, 1, 1);
		gd_secondSeparator.heightHint = 10;
		secondSeparator.setLayoutData(gd_secondSeparator);

		progressBar = new ProgressBarMonitor(parent, SWT.NONE, sync, context);
		progressBar.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		progressBar.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {

			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				try {
					CommandUtils.executeCommand(context, "org.eclipse.e4.ui.views.progress.commands.openProgressView");
				} catch (GException ex) {
					ex.printStackTrace();
				}
			}
		});

		updateJobManager();
	}

	/**
	 * Update the {@link IJobManager} with a new {@link IJobChangeListener}.
	 */
	public void updateJobManager() {

		IJobManager manager = Job.getJobManager();
		myJobListener = new IJobChangeListener() {
			@Override
			public void sleeping(IJobChangeEvent event) {

			}

			@Override
			public void scheduled(IJobChangeEvent event) {

			}

			@Override
			public void running(IJobChangeEvent event) {

			}

			@Override
			public void done(IJobChangeEvent event) {
				synchronized (runningJobs) {
					runningJobs.remove(event.getJob());
				}
				refreshActivityBar();
			}

			@Override
			public void awake(IJobChangeEvent event) {
			}

			@Override
			public void aboutToRun(IJobChangeEvent event) {
				synchronized (runningJobs) {
					runningJobs.add(event.getJob());
				}
				refreshActivityBar();
			}
		};

		manager.addJobChangeListener(myJobListener);
	}

	/**
	 * Refresh progress bar if running jobs status are updated.
	 */
	public void refreshActivityBar() {
		synchronized (runningJobs) {
			if (progressBar.isDisposed()) {
				return;
			}
			if (runningJobs.size() > 0) {
				progressBar.start();
			} else {
				progressBar.stop();
			}
		}
	}

	/**
	 * @return the error
	 */
	boolean isError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	void setError(boolean error) {
		this.error = error;
		if (!consoleLabel.isDisposed()) {
			if (error) {
				consoleLabel.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/erroricon.png"));
				consoleLabel.setToolTipText("An error occured, see console view for detailed information");
			} else {
				consoleLabel.setImage(null);
				consoleLabel.setText("No Error");
				consoleLabel.setToolTipText("No Error");
			}
		}
	}
}