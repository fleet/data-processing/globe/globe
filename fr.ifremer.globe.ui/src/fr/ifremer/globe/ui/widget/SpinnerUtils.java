package fr.ifremer.globe.ui.widget;

import org.eclipse.swt.widgets.Spinner;

public class SpinnerUtils {
	/**
	 * Utility class
	 * */
	private SpinnerUtils(){}
	/**
	 * Update the custom step for a couple of min and max spinners.
	 * 
	 * @param min the min spinner
	 * @param max the max spinner
	 */
	public static void updateCustomStep(Spinner min, Spinner max, int scalefactor) {
		int customStep = Math.abs(max.getSelection() - min.getSelection()) / scalefactor;
		min.setIncrement( customStep);
		max.setIncrement( customStep);
	}
}
