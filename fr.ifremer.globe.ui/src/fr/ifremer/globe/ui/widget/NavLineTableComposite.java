package fr.ifremer.globe.ui.widget;

import java.util.List;
import java.util.function.Function;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

public class NavLineTableComposite<T extends IChartSeries<? extends IChartData>> extends Composite {

	private TableViewer tableViewer;
	private Table table;
	private TableColumn profileTableColumn;
	private Label title;
	private List<T> navigationLines;

	/**
	 * Constructor
	 */
	public NavLineTableComposite(Composite parent, int style, List<T> navigationLines, Runnable selectedCallback) {
		super(parent, style);
		setLayout(new GridLayout(4, false));

		this.navigationLines = navigationLines;

		title = new Label(this, SWT.NONE);
		title.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		title.setText("Navigation Lines (" + navigationLines.size() + ")");
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_table.horizontalSpan = 4;
		gd_table.widthHint = 350;
		table.setLayoutData(gd_table);

		TableViewerColumn profileTableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		profileTableViewerColumn.setLabelProvider(buildColumnLabelProvider(IChartSeries::getTitle));
		profileTableColumn = profileTableViewerColumn.getColumn();
		profileTableColumn.setWidth(350);
		profileTableColumn.setText("Name");

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(navigationLines);

		table.addListener(SWT.Selection, e -> {
			navigationLines.forEach(line -> line.setEnabled(false));
			for (TableItem item : table.getSelection()) {
				((IChartSeries<?>) item.getData()).setEnabled(true);
			}
			selectedCallback.run();
		});

		table.addListener(SWT.FocusOut, e -> {
			navigationLines.forEach(line -> line.setEnabled(false));
			selectedCallback.run();
		});
	}

	public void refresh() {
		if (!isDisposed()) {
			title.setText("Navigation Lines (" + navigationLines.size() + ")");
			tableViewer.refresh();
		}
	}

	@SuppressWarnings("unchecked")
	private ColumnLabelProvider buildColumnLabelProvider(Function<T, String> labelProvider) {

		return new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return labelProvider.apply((T) element);
			}

			@Override
			public Color getForeground(Object element) {
				return ColorUtils.convertGColorToSWT(((T) element).getColor());
			}
		};
	}
}
