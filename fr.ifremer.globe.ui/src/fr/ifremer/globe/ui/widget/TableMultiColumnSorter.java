package fr.ifremer.globe.ui.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * TableMultiColumnSorter: composite which contains a ViewerTable and tools to
 * reorder and to sort it (mutlicolumn sort).
 * 
 * @param <T>
 *            Type of the ViewerTable's item. This type can implement the
 *            interfaces {@link ICheckable} and {@link ISelectable} to manage
 *            check and selection events.
 */
public class TableMultiColumnSorter<T> extends Composite {

	static String plugingRessources = "fr.ifremer.globe.ui";

	/** Sort orders **/
	public static final String ASCENDING = "Ascending";
	public static final String DECREASING = "Decreasing";

	/** UI properties **/
	private Table table;
	private TableViewer tableViewer;
	private ExpandBar sortExpandBar;
	private ExpandItem sortExpanditem;
	private Composite sortComposite;
	private Composite sortLevelListComposite;
	private Button topButton;
	private Button upButton;
	private Button downButton;
	private Button botButton;

	/** Model properties **/
	private List<T> inputDataList;
	private ArrayList<ColumnModel> columnList = new ArrayList<>();
	private ArrayList<SortLevel> sortLevelList = new ArrayList<>();
	private ArrayList<ITableMultColumnSorterListener> refreshListeners = new ArrayList<>();

	/**
	 * ColumnModel class: represents a column in the table viewer. A ColumnModel
	 * instance provides also a function which can be used in the comparator to
	 * sort the table.
	 */
	private class ColumnModel {
		private String title;
		private Function<T, String> getValueFunction;

		public ColumnModel(String title, Function<T, String> fct) {
			this.title = title;
			this.getValueFunction = fct;
		}

		public String getTitle() {
			return title;
		}

		public Function<T, String> getGetValueFunction() {
			return getValueFunction;
		}
	}

	/**
	 * SortLevel class: provides tools to define the algorithm used to sort the
	 * table. It's composed of 3 elements: - a combo box to select a column (the
	 * selected column provides a function used to sort the list) - a combo box
	 * to select an sort order (ascending or decreasing) - a button to delete
	 * itself
	 */
	private class SortLevel {
		/** Properties **/
		private String sortOrder = ASCENDING; // default sort order
		private Function<T, String> getValueToCompareFct;
		private ComboViewer columnViewer;
		private Combo comboOrder;
		private Button btnDelete;
		private int columnIndex;

		/** Constructor **/
		public SortLevel() {
			createComposite();
			sortLevelList.add(this);
			resizeSortComposite();
		}

		/** Constructor **/
		public SortLevel(String columnTitle, String order) {
			this();

			// Select column
			columnIndex = -1;
			columnList.forEach(c -> {
				if (c.getTitle().equals(columnTitle))
					columnIndex = columnList.indexOf(c);
			});
			if (columnIndex != -1) {
				columnViewer.getCombo().select(columnIndex);
				getValueToCompareFct = columnList.get(columnIndex).getGetValueFunction();
			}

			// Select order
			sortOrder = order;
			int orderIndex = Arrays.asList((comboOrder.getItems())).indexOf(sortOrder);
			if (orderIndex > -1)
				comboOrder.select(orderIndex);
		}

		/** Build the composite **/
		private void createComposite() {
			// Column selection combo box
			columnViewer = new ComboViewer(sortLevelListComposite, SWT.READ_ONLY);
			GridData gdCol = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
			gdCol.widthHint = -1;
			columnViewer.getCombo().setLayoutData(gdCol);
			columnViewer.setContentProvider(ArrayContentProvider.getInstance());
			columnViewer.setLabelProvider(new LabelProvider() {
				@SuppressWarnings("unchecked")
				@Override
				public String getText(Object element) {
					return ((ColumnModel) element).getTitle();
				}
			});
			columnViewer.addSelectionChangedListener(new ISelectionChangedListener() {
				@SuppressWarnings("unchecked")
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					IStructuredSelection selection = (IStructuredSelection) event.getSelection();
					getValueToCompareFct = ((ColumnModel) selection.getFirstElement()).getGetValueFunction();
				}
			});
			columnViewer.setInput(columnList);

			// Order selection combo box
			comboOrder = new Combo(sortLevelListComposite, SWT.READ_ONLY);
			GridData gdComboOrder = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
			gdComboOrder.widthHint = -1;
			comboOrder.setLayoutData(gdComboOrder);
			comboOrder.setItems(new String[] { ASCENDING, DECREASING });
			comboOrder.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					sortOrder = comboOrder.getItem(comboOrder.getSelectionIndex());
				}
			});
			comboOrder.select(Arrays.asList(comboOrder.getItems()).indexOf(sortOrder));

			// Delete button
			btnDelete = new Button(sortLevelListComposite, SWT.NONE);
			btnDelete.setText("Remove");
			GridData gdBtnDelete = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
			btnDelete.setLayoutData(gdBtnDelete);
			btnDelete.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					destroy();
				}
			});

		}

		/** Destroy method: called when the sort level is removed **/
		public void destroy() {
			columnViewer.getCombo().dispose();
			comboOrder.dispose();
			btnDelete.dispose();
			sortLevelList.remove(this);
			resizeSortComposite();

		}

		/** Refresh the column list (column comboBox) **/
		public void refreshColumnList() {
			columnViewer.setInput(columnList);
		}
	}

	/**
	 * Constructor: create the TableMultiColumnSorter composite.
	 */
	public TableMultiColumnSorter(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		// Sort expand bar
		sortExpandBar = new ExpandBar(this, SWT.NONE);
		sortExpandBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		sortExpandBar.addMouseListener(new MouseAdapter() {
			// Reset layout when the sortExpanditem is expanded or collapsed
			@Override
			public void mouseUp(MouseEvent e) {
				TableMultiColumnSorter.this.layout();
			}
		});
		sortExpanditem = new ExpandItem(sortExpandBar, SWT.NONE);
		sortExpanditem.setText("Multi column sort");

		sortComposite = new Composite(sortExpandBar, SWT.NONE);
		sortExpanditem.setControl(sortComposite);
		sortComposite.setLayout(new GridLayout(2, false));

		// Sort level list
		sortLevelListComposite = new Composite(sortComposite, SWT.NONE);
		sortLevelListComposite.setLayout(new GridLayout(3, false));
		sortLevelListComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		Label lblColumn = new Label(sortLevelListComposite, SWT.NONE);
		GridData gd_lblColumn = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblColumn.widthHint = 120;
		lblColumn.setLayoutData(gd_lblColumn);
		lblColumn.setText("Column");

		Label lblOrder = new Label(sortLevelListComposite, SWT.NONE);
		GridData gd_lblOrder = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblOrder.widthHint = 120;
		lblOrder.setLayoutData(gd_lblOrder);
		lblOrder.setText("Order");

		new Label(sortLevelListComposite, SWT.NONE);

		Button btnAddSortLevel = new Button(sortComposite, SWT.NONE);
		btnAddSortLevel.setText("Add sort level");
		btnAddSortLevel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				new SortLevel();
			}
		});

		Button btnDoSort = new Button(sortComposite, SWT.NONE);
		btnDoSort.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		btnDoSort.setText("Sort table");
		btnDoSort.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSort();
			}
		});

		new Label(this, SWT.NONE);

		// Table viewer
		tableViewer = new TableViewer(this, SWT.BORDER | SWT.CHECK | SWT.FULL_SELECTION);
		tableViewer.setContentProvider(new ArrayContentProvider());
		table = tableViewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		table.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				TableItem item = (TableItem) event.item;
				// Item checked
				if (event.detail == SWT.CHECK && item.getData() instanceof ICheckable) {
					((ICheckable) item.getData()).setChecked(item.getChecked());
					refresh();
				}
				// Item selected
				else if (item.getData() instanceof ISelectable) {
					((ISelectable) item.getData()).onSelect();
				}
			}
		});

		// Reorder buttons
		Composite reorderButtonsComposite = new Composite(this, SWT.NONE);
		reorderButtonsComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		reorderButtonsComposite.setLayout(new GridLayout(1, false));

		topButton = new Button(reorderButtonsComposite, SWT.NONE);
		topButton.setImage(ResourceManager.getPluginImage(plugingRessources, "icons/double_up.png"));
		topButton.addSelectionListener(reorderListener);

		upButton = new Button(reorderButtonsComposite, SWT.NONE);
		upButton.setImage(ResourceManager.getPluginImage(plugingRessources, "icons/arrow_up.png"));
		upButton.addSelectionListener(reorderListener);

		downButton = new Button(reorderButtonsComposite, SWT.NONE);
		downButton.setImage(ResourceManager.getPluginImage(plugingRessources, "icons/arrow_down.png"));
		downButton.addSelectionListener(reorderListener);

		botButton = new Button(reorderButtonsComposite, SWT.NONE);
		botButton.setImage(ResourceManager.getPluginImage(plugingRessources, "icons/double_down.png"));
		botButton.addSelectionListener(reorderListener);
	}

	/**
	 * Sort the table (with the algorithm define in
	 */
	public void doSort() {
		synchronized (inputDataList) {
			if (inputDataList != null && inputDataList.size() > 0) {
				Collections.sort(inputDataList, new Comparator<T>() {
					@Override
					public int compare(T o1, T o2) {
						int result = 0;
						// describes the algorithm used to sort the input list
						for (SortLevel sortLvl : sortLevelList) {
							if (sortLvl.getValueToCompareFct != null) {
								String value1 = sortLvl.getValueToCompareFct.apply(o1);
								String value2 = sortLvl.getValueToCompareFct.apply(o2);
								if (value1 == null || value2 == null)
									return result;
								result = value1.compareTo(value2);
								if (sortLvl.sortOrder.equals(DECREASING))
									result = -result;
								if (result != 0)
									break;
							}
						}
						return result;
					}
				});
				refresh();
			}
		}
	}

	/** Resize sort graphic components **/
	private void resizeSortComposite() {
		// Resize "sortLevelListComposite"
		sortComposite.layout();
		// Resize "sortComposite"
		sortExpanditem.setHeight(sortLevelListComposite.getSize().y + 40);
		// Resize "expandBar"
		this.layout();
	}

	/**
	 * Functional interface for listeners which need to be notified when the
	 * table is refreshed
	 */
	@FunctionalInterface
	public interface ITableMultColumnSorterListener {
		void onRefresh();
	}

	public void addRefreshListeners(ITableMultColumnSorterListener newListener) {
		refreshListeners.add(newListener);
	}

	public void removeRefreshListener(ITableMultColumnSorterListener listenerToRemove) {
		refreshListeners.remove(listenerToRemove);
	}

	/**
	 * Interface for checkable items
	 */
	public interface ICheckable {
		void setChecked(boolean isChecked);

		boolean isChecked();
	}

	/**
	 * Interface for selectable items
	 */
	public interface ISelectable {
		void onSelect();
	}

	/**
	 * Reorder listener: called when the buttons ("Up"/"Down"/...) to move the
	 * selected element are called
	 **/
	SelectionListener reorderListener = new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {

			int selectedIndex = table.getSelectionIndex();
			int futurIndex = selectedIndex;
			if (e.getSource() == downButton)
				futurIndex++;
			else if (e.getSource() == upButton)
				futurIndex--;
			else if (e.getSource() == botButton)
				futurIndex = inputDataList.size() - 1;
			else if (e.getSource() == topButton)
				futurIndex = 0;

			if (selectedIndex < 0 || futurIndex < 0 || futurIndex > inputDataList.size() - 1)
				return;

			synchronized (inputDataList) {
				while (selectedIndex != futurIndex) {
					if (futurIndex < selectedIndex) {
						Collections.swap(inputDataList, selectedIndex, selectedIndex - 1);
						selectedIndex--;
					} else {
						Collections.swap(inputDataList, selectedIndex, selectedIndex + 1);
						selectedIndex++;
					}
				}
			}
			refresh();
			tableViewer.getTable().setSelection(futurIndex);
		}
	};

	/**
	 * ColmunBuilder: this class provides help to build a new column. Column's
	 * parameters and providers can be freely added. (Builder pattern)
	 * 
	 * ".build()" method must be called to add the new column in the table.
	 * 
	 * <p>
	 * Example: tableMultiColSort.new ColumnBuilder().setTitle("Title") ...
	 * .build()
	 *
	 */
	public class ColumnBuilder {
		String title;
		int bound;
		int alignment;
		Function<T, String> labelProvider;
		Function<T, Font> fontProvider;
		Function<T, Color> colorProvider;
		Function<T, Image> imageProvider;

		public ColumnBuilder setTitle(String title) {
			this.title = title;
			return this;
		}

		public ColumnBuilder setBound(int bound) {
			this.bound = bound;
			return this;
		}

		public ColumnBuilder setAlignment(int alignment) {
			this.alignment = alignment;
			return this;
		}

		public ColumnBuilder setLabelProvider(Function<T, String> labelProvider) {
			this.labelProvider = labelProvider;
			return this;
		}

		public ColumnBuilder setFontProvider(Function<T, Font> fontProvider) {
			this.fontProvider = fontProvider;
			return this;
		}

		public ColumnBuilder setColorProvider(Function<T, Color> colorProvider) {
			this.colorProvider = colorProvider;
			return this;
		}

		public ColumnBuilder setImageProvider(Function<T, Image> imageProvider) {
			this.imageProvider = imageProvider;
			return this;
		}

		public void build() {
			addColumn(title, bound, alignment, labelProvider, fontProvider, colorProvider, imageProvider);
		}

	}

	/**
	 * Add new column (heavy method: it's easier to use {@link ColumnBuilder})
	 */
	@SuppressWarnings("unchecked") //
	public void addColumn(String title, int bound, int alignment, Function<T, String> labelProvider,
			Function<T, Font> fontProvider, Function<T, Color> colorProvider, Function<T, Image> imageProvider) {

		ColumnLabelProvider columnLabelProvider = new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (labelProvider != null)
					return labelProvider.apply((T) element);
				else
					return super.getText(element);
			}

			@Override
			public Color getForeground(Object element) {
				// Unchecked elements are grayed
				if (element instanceof ICheckable && !((ICheckable) element).isChecked())
					return SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY);
				else if (colorProvider != null)
					return colorProvider.apply((T) element);
				else
					return super.getForeground(element);
			}

			@Override
			public Font getFont(Object element) {
				if (fontProvider != null)
					return fontProvider.apply((T) element);
				else
					return super.getFont(element);
			}

			@Override
			public Image getImage(Object element) {
				if (imageProvider != null)
					return imageProvider.apply((T) element);
				else
					return super.getImage(element);
			}
		};

		final TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		viewerColumn.setLabelProvider(columnLabelProvider);

		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setAlignment(alignment);
		column.setResizable(true);
		column.setMoveable(true);

		refresh();

		columnList.add(new ColumnModel(title, labelProvider));
		sortLevelList.forEach(sLL -> sLL.refreshColumnList());
	}

	/**
	 * Add a sort level
	 */
	public void addSortLevel() {
		new SortLevel();
	}

	/**
	 * Add a sort level
	 * 
	 * @param columnTitle:
	 *            selected column
	 * @param selectedOrder:
	 *            selected order
	 * @return result: true, if the input parameters were correct, else false
	 */
	public boolean addSortLevel(String columnTitle, String selectedOrder) {
		// Check the column exists and the selected order is correct
		if (columnList.stream().filter(c -> c.getTitle().equals(columnTitle)).toArray().length < 1
				|| (!selectedOrder.equals(ASCENDING) && !selectedOrder.equals(DECREASING)))
			return false;

		new SortLevel(columnTitle, selectedOrder);
		return true;
	}

	/**
	 * Set the data model
	 * 
	 * @param inputs:
	 *            list of elements to display in the table
	 */
	public void setInputs(List<T> inputs) {
		inputDataList = Collections.synchronizedList(inputs);
		tableViewer.setInput(inputDataList);
		refresh();
	}

	/**
	 * Do a refresh process.
	 */
	private void refreshProcess() {
		tableViewer.refresh();

		Arrays.asList(table.getItems()).stream().forEach(tableItem -> {
			if (tableItem.getData() instanceof ICheckable)
				tableItem.setChecked(((ICheckable) tableItem.getData()).isChecked());
		});

		refreshListeners.forEach(listener -> listener.onRefresh());
	}

	/**
	 * Refresh the TableMultiColumnSorter composite. (Manage call from other
	 * thread)
	 */
	public void refresh() {
		if (Display.getDefault().getThread() != Thread.currentThread()) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					if (!isDisposed())
						refreshProcess();
				}
			});
		} else {
			refreshProcess();
		}
	}

	/**
	 * Get the checked elements.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<T> getCheckedElements() {
		ArrayList<T> result = new ArrayList<>();
		Arrays.asList(table.getItems()).stream().filter(t -> t.getChecked())
		.forEach(tableItem -> result.add((T) tableItem.getData()));
		return result;
	}

	public TableViewer getTableViewer() {
		return this.tableViewer;
	}
}