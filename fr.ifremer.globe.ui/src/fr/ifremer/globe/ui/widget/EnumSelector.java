package fr.ifremer.globe.ui.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import io.reactivex.subjects.PublishSubject;

public class EnumSelector<T extends Enum<T>> extends Composite {
	public class EnumSelectorModel
	{
		EnumSelectorModel(T value)
		{
			this.selection=value;
		}
		public final T selection;
	}
	/**
	 * {@link PublisherSubject} for event in this composite
	 * */
	PublishSubject<EnumSelectorModel> publisher = PublishSubject.create();
	private Combo combo;
	private T defaultValue;

	private void publish() {
		int selection=combo.getSelectionIndex();
		if(selection>-1)
		{
			String text=combo.getItem(selection);
			T finalValue=T.valueOf(defaultValue.getDeclaringClass(), text);
			publisher.onNext(new EnumSelectorModel(finalValue));
		}

	}
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public EnumSelector(Composite parent, int style, T defaultValue, String label) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		this.defaultValue=defaultValue;
		combo = new Combo(this, style);
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}


		});
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		for(T v:defaultValue.getDeclaringClass().getEnumConstants())
		{
			combo.add(v.name());
		}
		for(int i=0;i<combo.getItemCount();i++)
		{
			if (combo.getItem(i).compareTo(defaultValue.name()) == 0)
			{
				combo.select(i);
				break;
			}
		}
		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblNewLabel.setText(label);

	}

	@Override
	protected void checkSubclass() {
	}
	
	/**
	 * Retrieve the event publisher
	 * */
	public PublishSubject<EnumSelectorModel> getPublisher() {
		return publisher;
	}
}
