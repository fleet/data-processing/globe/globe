/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.widget;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

/**
 * Instances of this class provide an etched border with a title and a checkbox.
 */
public class CheckBoxGroup extends org.eclipse.nebula.widgets.opal.checkboxgroup.CheckBoxGroup {

	/**
	 * Constructor
	 */
	public CheckBoxGroup(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * Constructor
	 */
	public CheckBoxGroup(Composite parent, int horizontalIndent, int style) {
		super(parent, style);
		((GridData) button.getLayoutData()).horizontalIndent = horizontalIndent;
	}

}
