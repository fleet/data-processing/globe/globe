package fr.ifremer.globe.ui.widget.shading;

import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;

/**
 * A immutable class for ColorAndConstrast settings
 */
public class ShadingWidgetModel {

	private boolean enabled;
	private boolean isGradientEnabled;
	private boolean isLogaritmicEnabled;
	private double azimtuh;
	private double zenith;
	private double shadingExageration;

	/**
	 * Constructor with a {@link TerrainParameters}
	 */
	public ShadingWidgetModel(TerrainParameters parameters) {
		this(parameters.useShading, parameters.useGradient, parameters.useLogarithmicShading, parameters.azimuth,
				parameters.zenith, parameters.shadingExaggeration);
	}

	/**
	 * Constructor
	 */
	public ShadingWidgetModel(boolean enabled, boolean isGradientEnabled, boolean isLogaritmicEnabled, double azimtuh,
			double zenith, double shadingExageration) {
		super();
		this.enabled = enabled;
		this.isGradientEnabled = isGradientEnabled;
		this.isLogaritmicEnabled = isLogaritmicEnabled;
		this.azimtuh = azimtuh;
		this.zenith = zenith;
		this.shadingExageration = shadingExageration;
	}

	// Getter & setters

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isGradientEnabled() {
		return isGradientEnabled;
	}

	public boolean isLogaritmicEnabled() {
		return isLogaritmicEnabled;
	}

	public double getAzimuth() {
		return azimtuh;
	}

	public double getZenith() {
		return zenith;
	}

	public double getShadingExageration() {
		return shadingExageration;
	}

	@Override
	public String toString() {
		return "ShadingWidgetModel [enabled=" + enabled + ", isGradientEnabled=" + isGradientEnabled
				+ ", isLogaritmicEnabled=" + isLogaritmicEnabled + ", azimtuh=" + azimtuh + ", zenith=" + zenith
				+ ", shadingExageration=" + shadingExageration + "]";
	}

}
