package fr.ifremer.globe.ui.widget.shading;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.function.Consumer;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;

/**
 * Composite that changes azimuth of shading of ShaderElevationLayer
 */
public class AzimuthShadeComposite extends Composite implements MouseListener, MouseMotionListener {

	protected Canvas canvas;
	protected static final int radius = 30;
	protected static final int spotRadius = 4;
	protected static final int cornerx = 0;
	protected static final int cornery = 0;
	protected double theta;
	protected Color circleColor;
	protected Color circleColor2;
	protected Color sunColor;
	protected Color spotColor;
	protected boolean pressedOnSpot;
	protected boolean enabled;
	private double viewHeading = 0;

	private final Consumer<Double> azimuthConsumer;

	public AzimuthShadeComposite(Composite parent, int style, Consumer<Double> azimuthConsumer) {
		super(parent, style);
		init();
		this.azimuthConsumer = azimuthConsumer;
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void init() {
		theta = Math.PI * 7 / 4;
		pressedOnSpot = false;
		circleColor = Color.lightGray;
		circleColor2 = Color.gray;
		sunColor = Color.gray;
		spotColor = Color.lightGray;

		Frame frame = SWT_AWT.new_Frame(this);
		canvas = new Canvas() {
			private static final long serialVersionUID = 1L;

			/**
			 * Paint the JKnob on the graphics context given. The knob is a filled circle with a small filled circle
			 * offset within it to show the current angular position of the knob.
			 * 
			 * @param g The graphics context on which to paint the knob.
			 */
			@Override
			public void paint(Graphics g) {
				g.clearRect(0, 0, 2 * radius + spotRadius, 2 * radius + spotRadius);

				// Draw the knob.
				// Cercle interieur principal
				g.setColor(circleColor);

				g.fillOval(cornerx, cornery, 2 * radius, 2 * radius);

				// Cercle contour principal
				g.setColor(circleColor2);
				g.drawOval(cornerx, cornery, 2 * radius, 2 * radius);

				// Find the center of the spot.
				Point pt = getSpotCenter();
				int xc = (int) pt.getX();
				int yc = (int) pt.getY();

				// Fleche
				g.setColor(Color.gray);
				g.drawLine(cornerx + radius, cornery + radius, xc, yc);

				// Rayons de soleil
				drawSunRays(g, xc, yc);

				// Cercle centre
				g.setColor(circleColor2);
				g.fillOval(cornerx + radius - spotRadius, cornery + radius - spotRadius, 2 * spotRadius,
						2 * spotRadius);

				// Draw the spot.
				g.setColor(spotColor);
				g.fillOval(xc - spotRadius, yc - spotRadius, 2 * spotRadius, 2 * spotRadius);

				g.setColor(sunColor);
				g.drawOval(xc - spotRadius, yc - spotRadius, 2 * spotRadius, 2 * spotRadius);
			}
		};
		frame.add(canvas);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
	}

	public void drawSunRays(Graphics g, int xc, int yc) {
		g.setColor(sunColor);
		g.drawLine(xc, yc, xc - 2 * spotRadius, yc);
		g.drawLine(xc, yc, xc + 2 * spotRadius, yc);
		g.drawLine(xc, yc, xc, yc - 2 * spotRadius);
		g.drawLine(xc, yc, xc, yc + 2 * spotRadius);

		g.drawLine(xc, yc, (int) (xc - 1.5 * spotRadius), (int) (yc - 1.5 * spotRadius));
		g.drawLine(xc, yc, (int) (xc - 1.5 * spotRadius), (int) (yc + 1.5 * spotRadius));
		g.drawLine(xc, yc, (int) (xc + 1.5 * spotRadius), (int) (yc - 1.5 * spotRadius));
		g.drawLine(xc, yc, (int) (xc + 1.5 * spotRadius), (int) (yc + 1.5 * spotRadius));

	}

	/**
	 * Return the ideal size that the knob would like to be.
	 * 
	 * @return the preferred size of the JKnob.
	 */
	public Dimension getPreferredSize() {
		return new Dimension(2 * radius + 2 * cornerx, 2 * radius + 2 * cornery);
	}

	/**
	 * Return the minimum size that the knob would like to be. This is the same size as the preferred size so the knob
	 * will be of a fixed size.
	 * 
	 * @return the minimum size of the JKnob.
	 */
	public Dimension getMinimumSize() {
		return new Dimension(2 * radius + 2 * cornerx, 2 * radius + 2 * cornery);
	}

	/**
	 * Calculate the x, y coordinates of the center of the spot.
	 * 
	 * @return a Point containing the x,y position of the center of the spot.
	 */
	protected Point getSpotCenter() {
		// Calculate the center point of the spot RELATIVE to the center of the of the circle.
		// int r = radius - spotRadius; // spot à l'interieur
		// int r = radius - 0; // spot sur le cercle
		int xcp = (int) (radius * Math.sin(theta - viewHeading));
		int ycp = (int) (radius * Math.cos(theta - viewHeading));

		// Adjust the center point of the spot so that it is offset from the center of the circle. This is necessary
		// because 0,0 is not actually the center of the circle, it is the upper left corner of the component!
		int xc = radius + xcp + cornerx;
		int yc = radius - ycp + cornery;

		// Create a new Point to return since we can't return 2 values!
		return new Point(xc, yc);
	}

	/**
	 * Determine if the mouse click was on the spot or not. If it was return true, otherwise return false.
	 * 
	 * @return true if x,y is on the spot and false if not.
	 */
	protected boolean isOnSpot(Point pt) {
		if (enabled) {
			if (pt.distance(getSpotCenter()) <= spotRadius + 2
					|| pt.distance(new Point(cornerx + radius, cornery + radius)) <= radius) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public void setAzimuth(final double theta, boolean notify) {
		if (this.theta != theta) {
			this.theta = theta;
			Double theta_reel;
			if (theta >= 0 && theta < 2 * Math.PI) {
				theta_reel = theta;
			} else if (theta >= 2 * Math.PI) {
				theta_reel = theta - 2 * Math.PI;
			} else {
				theta_reel = theta + 2 * Math.PI;
			}
			if (theta_reel >= 2 * Math.PI) {
				theta_reel = theta_reel - 2 * Math.PI;
			} else if (theta_reel < 0) {
				theta_reel = theta_reel + 2 * Math.PI;
			}
			theta_reel = Math.toDegrees(theta_reel);
			if (notify) {
				azimuthConsumer.accept(theta);
			} else {
				canvas.repaint();
			}
		}
	}

	public double getAzimuth() {
		return theta;
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.enabled = enabled;
		if (!enabled) {
			circleColor = Color.lightGray;
			circleColor2 = Color.gray;
			sunColor = Color.gray;
			spotColor = Color.lightGray;
		} else {
			circleColor = Color.white;
			circleColor2 = Color.orange;
			sunColor = new Color(255, 127, 0);
			spotColor = Color.yellow;
		}

		setAzimuth(theta, true);
		canvas.repaint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (pressedOnSpot) {
			computeAngle(e);
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	private void computeAngle(MouseEvent e) {
		int mx = e.getX();
		int my = e.getY();

		// Compute the x, y position of the mouse RELATIVE to the center of the knob.
		int mxp = mx - (radius + cornerx);
		int myp = (radius + cornery) - my;

		// Compute the new angle of the knob from the new x and y position of the mouse.
		// Math.atan2(...) computes the angle at which x,y lies from the positive y axis with cw rotations
		// being positive and ccw being negative.
		setAzimuth(Math.atan2(mxp, myp) + viewHeading, true);
		canvas.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Point mouseLoc = e.getPoint();
		pressedOnSpot = isOnSpot(mouseLoc);
		if (pressedOnSpot) {
			computeAngle(e);
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point mouseLoc = e.getPoint();
		pressedOnSpot = isOnSpot(mouseLoc);
		canvas.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		pressedOnSpot = false;
	}

	public void setViewHeading(double newHeading) {
		viewHeading = Math.toRadians(newHeading);
		canvas.repaint();
	}

}
