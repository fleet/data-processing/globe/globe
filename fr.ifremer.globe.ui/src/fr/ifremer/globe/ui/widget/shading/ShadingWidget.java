package fr.ifremer.globe.ui.widget.shading;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import io.reactivex.subjects.PublishSubject;

/**
 * A widget allowing to display basic color and contrast setting
 */
public class ShadingWidget extends Composite {

	/** Inner widgets **/
	private AzimuthShadeComposite azimuthShadeComposite;
	private ZenithShadeComposite zenithShadeComposite;
	private Button shadeLogButton;
	private Button gradientButton;
	private Button enableButton;
	private SliderComposite exagerationSliderComposite;

	/** {@link PublisherSubject} for event in this composite */
	private PublishSubject<ShadingWidgetModel> publisher = PublishSubject.create();

	/**
	 * Constructor
	 */
	public ShadingWidget(Composite parent, ShadingWidgetModel initialParameters) {
		super(parent, SWT.NONE);
		setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 3));
		GridLayout gridLayout = new GridLayout(3, true);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));
		enableButton = new Button(composite, SWT.CHECK);
		enableButton.setText("Enabled");
		enableButton.addListener(SWT.Selection, event -> publish());

		// log
		shadeLogButton = new Button(composite, SWT.CHECK);
		shadeLogButton.setText("Log Shade");

		// gradient
		gradientButton = new Button(composite, SWT.CHECK);
		gradientButton.setText("Gradient");
		gradientButton.addListener(SWT.Selection, event -> publish());
		shadeLogButton.addListener(SWT.Selection, event -> publish());

		// zenith
		Group grpZenith = new org.eclipse.swt.widgets.Group(this, SWT.NONE);
		grpZenith.setLayout(new GridLayout(1, false));
		grpZenith.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpZenith.setText("Zenith");
		zenithShadeComposite = new ZenithShadeComposite(grpZenith, SWT.EMBEDDED,
				zenith -> Display.getDefault().syncExec(this::publish));
		zenithShadeComposite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));

		// azimuth
		Group grpAzimuth = new org.eclipse.swt.widgets.Group(this, SWT.NONE);
		grpAzimuth.setLayout(new GridLayout(1, false));
		grpAzimuth.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpAzimuth.setText("Azimuth");
		azimuthShadeComposite = new AzimuthShadeComposite(grpAzimuth, SWT.EMBEDDED,
				azimuth -> Display.getDefault().syncExec(this::publish));
		azimuthShadeComposite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));

		// exaggeration
		Group grpExageration = new Group(this, SWT.NONE);
		grpExageration.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		grpExageration.setText("Exaggeration");
		grpExageration.setLayout(new GridLayout(1, false));
		exagerationSliderComposite = new SliderComposite(grpExageration, SWT.NONE, -1, 5, 1, true, value -> publish());
		exagerationSliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		setModel(initialParameters);
	}

	public void setModel(ShadingWidgetModel model) {
		enableButton.setSelection(model.isEnabled());
		azimuthShadeComposite.setEnabled(model.isEnabled());
		zenithShadeComposite.setEnabled(model.isEnabled());
		shadeLogButton.setEnabled(model.isEnabled());
		gradientButton.setEnabled(model.isEnabled());
		exagerationSliderComposite.setEnabled(model.isEnabled());

		shadeLogButton.setSelection(model.isLogaritmicEnabled());
		gradientButton.setSelection(model.isGradientEnabled());
		azimuthShadeComposite.setAzimuth(Math.toRadians(model.getAzimuth()), false);
		zenithShadeComposite.setZenith(Math.toRadians(model.getZenith()), false);
		exagerationSliderComposite.setValue((float) Math.log10(model.getShadingExageration()));
	}

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<ShadingWidgetModel> getPublisher() {
		return publisher;
	}

	/**
	 * Publishes a new model after a selection.
	 */
	private void publish() {
		shadeLogButton.setEnabled(enableButton.getSelection());
		gradientButton.setEnabled(enableButton.getSelection());
		azimuthShadeComposite.setEnabled(enableButton.getSelection());
		zenithShadeComposite.setEnabled(enableButton.getSelection());
		exagerationSliderComposite.setEnabled(enableButton.getSelection());

		publisher.onNext(new ShadingWidgetModel(//
				enableButton.getSelection(), //
				gradientButton.getSelection(), //
				shadeLogButton.getSelection(), //
				Math.toDegrees(azimuthShadeComposite.getAzimuth()), //
				Math.toDegrees(zenithShadeComposite.getZenith()), //
				Math.pow(10, exagerationSliderComposite.getValue())));
	}
}
