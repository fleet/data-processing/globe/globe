package fr.ifremer.globe.ui.widget.navigation;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.model.navigation.utils.NavigationTurnCutter;
import fr.ifremer.globe.core.runtime.gws.param.turnfilter.ThresholdWithInterval;

/**
 * Composite to set parameters : heading/speed period and threshold.
 */
public class TurnFilterParametersComposite extends Composite {

	/** Records to wrap parameters **/

	/** Inner widgets **/
	private Spinner headingThresholdSpinner;
	private Spinner headingIntervalSpinner;
	private Spinner speedThresholdSpinner;
	private Spinner speedIntervalSpinner;
	private Button speedCheckbox;
	private Button headingCheckbox;

	/**
	 * Constructor
	 */
	public TurnFilterParametersComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(4, false));
		new Label(this, SWT.NONE);

		Label lblInterval = new Label(this, SWT.NONE);
		GridData gdLblInterval = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblInterval.widthHint = 103;
		lblInterval.setLayoutData(gdLblInterval);
		lblInterval.setToolTipText(
				"Defines the size of intervals before and after each point, where values are computed.");
		lblInterval.setText("Intervals (sec)");

		Label lblThreshold = new Label(this, SWT.NONE);
		lblThreshold.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblThreshold.setToolTipText(
				"For each point, if difference between before and after intervals are over this threshold, the navigation is cut.");
		lblThreshold.setText("Variation threshold");

		headingCheckbox = new Button(this, SWT.CHECK);
		GridData gdHeadingCheckbox = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdHeadingCheckbox.widthHint = 98;
		headingCheckbox.setLayoutData(gdHeadingCheckbox);
		headingCheckbox.setText("Heading");
		headingIntervalSpinner = new Spinner(this, SWT.BORDER);
		headingIntervalSpinner.setMaximum(999999);
		headingIntervalSpinner.setSelection((int) Math.round(NavigationTurnCutter.DEFAULT_HEADING_INTERVAL.getSeconds()
				* Math.pow(10, headingIntervalSpinner.getDigits())));
		headingThresholdSpinner = new Spinner(this, SWT.BORDER);
		headingThresholdSpinner.setIncrement(10);
		headingThresholdSpinner.setMaximum(9999999);
		headingThresholdSpinner.setDigits(2);
		headingThresholdSpinner.setSelection((int) Math.round(
				NavigationTurnCutter.DEFAULT_HEADING_THRESHOLD * Math.pow(10, headingThresholdSpinner.getDigits())));

		Label label = new Label(this, SWT.NONE);
		label.setText("°");

		speedCheckbox = new Button(this, SWT.CHECK);
		speedCheckbox.setText("Speed");

		speedIntervalSpinner = new Spinner(this, SWT.BORDER);
		speedIntervalSpinner.setMaximum(999999);
		speedIntervalSpinner.setSelection((int) Math.round(NavigationTurnCutter.DEFAULT_SPEED_INTERVAL.getSeconds()
				* Math.pow(10, speedIntervalSpinner.getDigits())));

		speedThresholdSpinner = new Spinner(this, SWT.BORDER);
		speedThresholdSpinner.setMaximum(9999999);
		speedThresholdSpinner.setSelection(1500);
		speedThresholdSpinner.setIncrement(10);
		speedThresholdSpinner.setDigits(2);
		speedThresholdSpinner.setSelection((int) Math
				.round(NavigationTurnCutter.DEFAULT_SPEED_THRESHOLD * Math.pow(10, speedThresholdSpinner.getDigits())));

		Label lblMs = new Label(this, SWT.NONE);
		lblMs.setText("m/s");

		// hook listeners
		headingCheckbox.addListener(SWT.Selection, e -> {
			headingIntervalSpinner.setEnabled(headingCheckbox.getSelection());
			headingThresholdSpinner.setEnabled(headingCheckbox.getSelection());
		});
		headingCheckbox.setSelection(true);
		speedCheckbox.addListener(SWT.Selection, e -> {
			speedIntervalSpinner.setEnabled(speedCheckbox.getSelection());
			speedThresholdSpinner.setEnabled(speedCheckbox.getSelection());
		});
		speedCheckbox.setSelection(false);
		speedIntervalSpinner.setEnabled(false);
		speedThresholdSpinner.setEnabled(false);
	}

	public Optional<ThresholdWithInterval> getHeadingParameters() {
		AtomicReference<Optional<ThresholdWithInterval>> headingParameters = new AtomicReference<>(Optional.empty());
		getDisplay().syncExec(() -> { // access widget in suitable display thread
			if (headingCheckbox.getSelection()) {
				var headingInterval = Duration.ofSeconds(headingIntervalSpinner.getSelection());
				var headingThreshold = headingThresholdSpinner.getSelection()
						/ Math.pow(10, headingThresholdSpinner.getDigits());
				headingParameters.set(Optional.of(new ThresholdWithInterval(headingInterval, headingThreshold)));
			}
		});
		return headingParameters.get();
	}

	public Optional<ThresholdWithInterval> getSpeedParameters() {
		AtomicReference<Optional<ThresholdWithInterval>> speedParameters = new AtomicReference<>(Optional.empty());
		getDisplay().syncExec(() -> { // access widget in suitable display thread
			if (speedCheckbox.getSelection()) {
				var speedInterval = Duration.ofSeconds(speedIntervalSpinner.getSelection());
				var speedThreshold = speedThresholdSpinner.getSelection()
						/ Math.pow(10, speedThresholdSpinner.getDigits());
				speedParameters.set(Optional.of(new ThresholdWithInterval(speedInterval, speedThreshold)));
			}
		});
		return speedParameters.get();
	}

}
