package fr.ifremer.globe.ui.widget.threshold;

import org.eclipse.swt.widgets.Composite;

public class FloatThresholdWidget extends AbstractThresholdWidget<FloatThresholdModel> {

	public FloatThresholdWidget(String title, Composite parent, FloatThresholdModel initialParameters) {
		super(title, parent, initialParameters, 1);

	}

	/**
	 * Update the view according to current display parameters.
	 */
	public void update(FloatThresholdModel model) {
		sliderAndSpinners.update(model.minValue, model.maxValue, model.minLimit, model.maxLimit);
		btnEnable.setSelection(model.enable);
		setCustomStep();
	}

	@Override
	public FloatThresholdModel buildModelFromWidget() {
		return new FloatThresholdModel(sliderAndSpinners.getMin(), sliderAndSpinners.getMax(), btnEnable.getSelection(),
				settings.minLimit, settings.maxLimit);
	}

}
