package fr.ifremer.globe.ui.widget.threshold;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget;
import fr.ifremer.globe.ui.widget.SpinnerUtils;
import io.reactivex.subjects.PublishSubject;

public abstract class AbstractThresholdWidget<T> extends Composite {

	/**
	 * {@link PublisherSubject} for event in this composite
	 */
	PublishSubject<T> publisher = PublishSubject.create();
	protected T settings;
	protected Button btnEnable;
	protected SliderAndSpinnersWidget sliderAndSpinners;

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<T> getPublisher() {
		return publisher;
	}

	/**
	 * Get enable button composite
	 */
	public Button getEnableButton() {
		return btnEnable;
	}

	public AbstractThresholdWidget(String title, Composite parent, T initialParameters, int digits) {
		super(parent, SWT.NONE);
		this.settings = initialParameters;

		setLayout(new GridLayout(2, false));

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		btnEnable.setText(title);

		Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		sliderAndSpinners = new SliderAndSpinnersWidget(this);
		sliderAndSpinners.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		sliderAndSpinners.setDigits(digits);
		sliderAndSpinners.getPublisher().subscribe(contrastModel -> publish());

		update(settings);
	}

	/**
	 * Override this method to change step increment when a value changed
	 * 
	 * @see SpinnerUtils#updateCustomStep(Spinner, Spinner, int)
	 */
	protected void setCustomStep() {
	}

	/**
	 * Update the view according to current display parameters.
	 */
	public abstract void update(T model);

	protected void publish() {
		publisher.onNext(buildModelFromWidget());
	}

	protected abstract T buildModelFromWidget();

}
