package fr.ifremer.globe.ui.widget.threshold;

import org.eclipse.swt.widgets.Composite;

public class IntThresholdWidget extends AbstractThresholdWidget<IntThresholdModel> {

	public IntThresholdWidget(String title, Composite parent, IntThresholdModel initialParameters) {
		super(title, parent, initialParameters, 0);
	}

	@Override
	public IntThresholdModel buildModelFromWidget() {
		return new IntThresholdModel(Math.round(sliderAndSpinners.getMin()), Math.round(sliderAndSpinners.getMax()),
				btnEnable.getSelection(), settings.minLimit, settings.maxLimit);
	}

	@Override
	public void update(IntThresholdModel model) {
		sliderAndSpinners.update(model.minValue, model.maxValue, model.minLimit, model.maxLimit);
		btnEnable.setSelection(model.enable);
	}
}
