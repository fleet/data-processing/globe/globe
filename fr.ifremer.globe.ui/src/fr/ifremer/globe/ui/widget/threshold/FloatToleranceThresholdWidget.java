package fr.ifremer.globe.ui.widget.threshold;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.model.wc.filters.ThresholdParameter;
import fr.ifremer.globe.core.model.wc.filters.ThresholdToleranceParameter;

public class FloatToleranceThresholdWidget extends AbstractThresholdWidget<ThresholdToleranceParameter> {

	protected Spinner toleranceSpinner;

	public FloatToleranceThresholdWidget(String title, Composite parent,
			ThresholdToleranceParameter initialParameters) {
		super(title, parent, initialParameters, 1);
		this.settings = initialParameters;

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		GridLayout glComposite = new GridLayout(2, false);
		glComposite.marginHeight = 0;
		composite.setLayout(glComposite);

		Label lblTolerance = new Label(composite, SWT.NONE);
		lblTolerance.setText("Tolerance");

		toleranceSpinner = new Spinner(composite, SWT.BORDER);
		toleranceSpinner.setDigits(0);
		toleranceSpinner.setIncrement(1);
		toleranceSpinner.addListener(SWT.Selection, event -> {
			setCustomStep();
			publish();
		});

		update(settings);
	}

	/**
	 * Update the view according to current display parameters.
	 */
	@Override
	public void update(ThresholdToleranceParameter model) {
		sliderAndSpinners.update(model.getMinValue(), model.getMaxValue(), ThresholdParameter.minValidValue,
				ThresholdParameter.maxValidValue);

		if (toleranceSpinner != null) {
			toleranceSpinner.setMinimum((int) (0));
			toleranceSpinner.setMaximum((int) (Integer.MAX_VALUE));
			toleranceSpinner.setSelection(model.getTolerance());
		}

		btnEnable.setSelection(model.isEnable());

		setCustomStep();
	}

	@Override
	protected ThresholdToleranceParameter buildModelFromWidget() {
		return new ThresholdToleranceParameter(toleranceSpinner.getSelection(), sliderAndSpinners.getMin(),
				sliderAndSpinners.getMax(), btnEnable.getSelection());
	}
}
