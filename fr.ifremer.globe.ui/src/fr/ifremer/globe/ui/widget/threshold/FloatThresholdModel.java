package fr.ifremer.globe.ui.widget.threshold;

/**
 * Data model for {@link FloatThresholdWidget}
 */
public class FloatThresholdModel {

	/** Properties **/
	public final boolean enable;
	public final float minValue;
	public final float maxValue;
	public final float minLimit;
	public final float maxLimit;

	/**
	 * Constructor
	 */
	public FloatThresholdModel(float minValue, float maxValue) {
		this(minValue, maxValue, false, minValue, maxValue);
	}

	public FloatThresholdModel(FloatThresholdModel model) {
		this(model.minValue, model.maxValue, model.enable, model.minLimit, model.maxLimit);
	}
	
	/**
	 * Constructor
	 */
	public FloatThresholdModel(float minValue, float maxValue, boolean enable, float minLimit, float maxLimit) {
		super();
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.enable = enable;
		this.maxLimit = maxLimit;
		this.minLimit = minLimit;
	}

}
