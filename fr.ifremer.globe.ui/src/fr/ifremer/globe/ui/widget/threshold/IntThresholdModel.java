package fr.ifremer.globe.ui.widget.threshold;

/**
 * Data model for {@link FloatThresholdWidget}
 * */
public class IntThresholdModel {
	/**
	 * min value
	 * */
	public final int minValue;
	/**
	 * max value
	 * */
	public final int maxValue;
	/**
	 * is threshold enabled 
	 * */
	public final boolean enable;
	
	public final int minLimit;
	public final int maxLimit;
	public IntThresholdModel(int minValue, int maxValue, boolean enable,  int minLimit,int maxLimit) {
		super();
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.enable = enable;
		this.maxLimit = maxLimit;
		this.minLimit = minLimit;
	}
	
	
	
}
