package fr.ifremer.globe.ui.widget;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * StackLayout redefinition to allow WindowBuilder to access topControl attribute
 */
public class StackLayout extends org.eclipse.swt.custom.StackLayout {

	protected Composite composite;

	/**
	 * Constructor
	 */
	public StackLayout() {
		super();
	}

	/**
	 * Getter of topControl
	 */
	public Control getTopControl() {
		return topControl;
	}

	/**
	 * Setter of topControl
	 */
	public void setTopControl(Control topControl) {
		topControl.getDisplay().syncExec(() -> {
			if (this.topControl != topControl) {
				this.topControl = topControl;
				topControl.getParent().layout();
			}
		});
	}

}