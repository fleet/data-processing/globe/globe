package fr.ifremer.globe.ui.widget.slider;

import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;

/**
 * Simple slider composite which handles float values and provides options like tick labels...
 */
public class SliderComposite extends Composite {

	/** Properties **/
	private final int incrementCount;
	private final float min;
	private final float max;

	/** Inner widget **/
	private final Scale scale;

	/**
	 * Default constructor: builds a slider with value from 0 to 1.
	 * 
	 * @wbp.parser.constructor
	 */
	public SliderComposite(Composite parent, int style, boolean showTickLabels, Consumer<Float> valueConsumer) {
		this(parent, style, 0, 1, 0.25f, showTickLabels, valueConsumer);
	}

	/**
	 * Constructor
	 */
	public SliderComposite(Composite parent, int style, float min, float max, float tickIncrement,
			boolean showTickLabels, Consumer<Float> valueConsumer) {
		super(parent, style);
		this.min = min;
		this.max = max;

		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);

		int tickCount = Math.min(11, Math.round((max - min) / tickIncrement) + 1);
		incrementCount = 10 * (tickCount - 1);

		// labels for ticks for exaggeration scale
		if (showTickLabels) {
			Composite tickComposite = new Composite(this, SWT.NONE);
			tickComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			GridLayout glTickComposite = new GridLayout(tickCount, false);
			glTickComposite.marginRight = 8;
			glTickComposite.marginBottom = -8;
			glTickComposite.marginLeft = 4;
			tickComposite.setLayout(glTickComposite);

			for (int i = 0; i < tickCount; i++) {
				Label tickLabel = new Label(tickComposite, SWT.NONE);
				if (i != tickCount - 1) // let the last cell right align
					tickLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

				float value = min + i * tickIncrement;
				tickLabel.setText(value % 1 == 0 ? Integer.toString((int) value) : Float.toString(value));
			}
		}

		// scale
		scale = new Scale(this, SWT.NONE);
		scale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		scale.setMinimum(0);
		scale.setMaximum(incrementCount);
		scale.addListener(SWT.Selection, e -> valueConsumer.accept(getValue()));
		scale.setIncrement(1);
		scale.setPageIncrement(10);
	}

	/**
	 * @return current selection value.
	 */
	public float getValue() {
		// scale value is between 0 & INCREMENT_COUNT => transform to value between min & max
		var value = min + (max - min) * scale.getSelection() / incrementCount;
		// round value
		value = (float) (Math.round(value * Math.pow(10, 2)) / Math.pow(10, 2));
		return value;
	}

	/**
	 * Sets the {@link Scale} selection.
	 */
	public void setValue(float value) {
		// scale value is between 0 & INCREMENT_COUNT => normalize value
		int newSelection = Math.round(incrementCount * (value - min) / (max - min));
		if (scale.getSelection() != newSelection)
			scale.setSelection(newSelection);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		scale.setEnabled(enabled);
	}

}
