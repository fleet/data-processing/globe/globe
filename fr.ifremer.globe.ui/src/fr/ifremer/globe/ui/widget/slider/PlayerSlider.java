package fr.ifremer.globe.ui.widget.slider;

import org.eclipse.nebula.widgets.opal.commons.SelectionListenerUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

/**
 * Horizontal slider used in player composites
 */
public class PlayerSlider extends Canvas {

	private static final int SELECTOR_WIDTH = 10;
	private static final int BAR_HEIGHT = 4;
	private static final int SELECTOR_HEIGHT = 20;

	private final Color barColor;
	private final Color selectorColor;

	private int minimum;
	private int maximum;
	/** Current value between minimum and maximum */
	private int value;
	/** False to hide selector */
	private boolean selectorVisible = true;

	/** Current selector X position */
	private int xPosition;
	/** X Delta between selector position and last mouse down event */
	private int mouseDeltaX;
	/** True when selector is currently moved */
	private boolean moving = false;

	/**
	 * Constructor.
	 */
	public PlayerSlider(Composite parent) {
		super(parent, SWT.DOUBLE_BUFFERED);

		barColor = parent.getDisplay().getSystemColor(SWT.COLOR_GRAY);
		selectorColor = parent.getDisplay().getSystemColor(SWT.COLOR_WIDGET_DISABLED_FOREGROUND);

		minimum = Integer.MIN_VALUE;
		maximum = Integer.MAX_VALUE;
		value = 0;
		xPosition = -1;

		addPaintListener(e -> paintControl(e.gc));
		addMouseListeners();
	}

	private void paintControl(GC gc) {
		int originalWidth = getClientArea().width - SELECTOR_WIDTH;
		float coeff = (float) value / (maximum - minimum);
		xPosition = (int) (coeff * originalWidth);

		drawBar(gc);
		if (selectorVisible)
			drawSelector(gc);
	}

	private void drawBar(GC gc) {
		Rectangle rect = getClientArea();
		gc.setForeground(barColor);

		int x = SELECTOR_WIDTH / 2;
		int y = (rect.height - BAR_HEIGHT) / 2;
		int width = rect.width - SELECTOR_WIDTH;

		gc.drawRoundRectangle(x, y, width, BAR_HEIGHT, BAR_HEIGHT, BAR_HEIGHT);
	}

	private void drawSelector(GC gc) {
		Rectangle rect = getClientArea();
		gc.setBackground(selectorColor);

		int y = (rect.height - SELECTOR_HEIGHT) / 2;
		gc.fillRectangle(xPosition, y, SELECTOR_WIDTH, SELECTOR_HEIGHT);
	}

	private void addMouseListeners() {
		addListener(SWT.MouseDown, e -> {
			int y = (getClientArea().height - SELECTOR_HEIGHT) / 2;
			Rectangle rect = new Rectangle(xPosition, y, SELECTOR_WIDTH, SELECTOR_HEIGHT);
			if (rect.contains(e.x, e.y)) {
				moving = true;
				mouseDeltaX = xPosition - e.x;
			} else {
				mouseDeltaX = -(SELECTOR_WIDTH / 2);
				updateValue(e);
			}
		});

		addListener(SWT.MouseUp, e -> {
			moving = false;
			mouseDeltaX = 0;
			redraw();
		});

		addListener(SWT.MouseMove, e -> {
			if (!moving) {
				return;
			}
			updateValue(e);
		});
	}

	private void updateValue(Event e) {
		xPosition = e.x + mouseDeltaX;
		if (xPosition < 0) {
			xPosition = 0;
		}
		int originalWidth = getClientArea().width - SELECTOR_WIDTH;

		if (xPosition > originalWidth) {
			xPosition = originalWidth;
		}

		// Update value
		float ratio = (float) xPosition / originalWidth;
		int newValue = (int) Math.floor(ratio * (maximum - minimum));
		if (newValue != value) {
			value = newValue;
			SelectionListenerUtil.fireSelectionListeners(this, e);
		}
		redraw();
	}

	/**
	 * Adds the listener to the collection of listeners who will be notified when the control is selected by the user,
	 * by sending it one of the messages defined in the <code>SelectionListener</code> interface.
	 */
	public void addSelectionListener(SelectionListener listener) {
		checkWidget();
		SelectionListenerUtil.addSelectionListener(this, listener);
	}

	/**
	 * @see org.eclipse.swt.widgets.Control#computeSize(int, int, boolean)
	 */
	@Override
	public Point computeSize(int wHint, int hHint, boolean changed) {
		return new Point(Math.max(300, wHint), Math.max(40, hHint));
	}

	/**
	 * Removes the listener from the collection of listeners who will be notified when the control is selected by the
	 * user.
	 */
	public void removeSelectionListener(SelectionListener listener) {
		checkWidget();
		SelectionListenerUtil.removeSelectionListener(this, listener);
	}

	/**
	 * Returns the minimum value which the receiver will allow.
	 */
	public int getMinimum() {
		return minimum;
	}

	/**
	 * Sets the minimum value. If this value is greater than the maximum, an exception is thrown
	 */
	public void setMinimum(int minimum) {
		checkWidget();
		if (minimum < maximum) {
			this.minimum = minimum;
			redraw();
			update();
		}
	}

	/**
	 * Returns the maximum value which the receiver will allow.
	 */
	public int getMaximum() {
		return maximum;
	}

	/**
	 * Sets the maximum value. If this value is lower than the minimum, an exception is thrown
	 */
	public void setMaximum(int maximum) {
		checkWidget();
		if (maximum > minimum) {
			this.maximum = maximum;
			redraw();
			update();
		}
	}

	/**
	 * Returns the receiver's value.
	 */
	public int getSelection() {
		return value;
	}

	/**
	 * Sets the receiver's value. If the value is lower to minimum or greater than the maximum, an exception is thrown
	 */
	public void setSelection(int value) {
		checkWidget();
		if (value >= minimum && value <= maximum) {
			this.value = value;
			redraw();
			update();
		}
	}

	/**
	 * @return the {@link #selectorVisible}
	 */
	public boolean isSelectorVisible() {
		return selectorVisible;
	}

	/**
	 * @param selectorVisible the {@link #selectorVisible} to set
	 */
	public void setSelectorVisible(boolean selectorVisible) {
		if (this.selectorVisible != selectorVisible) {
			this.selectorVisible = selectorVisible;
			redraw();
			update();
		}
	}

}
