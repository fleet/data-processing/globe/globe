package fr.ifremer.globe.ui.widget;
/*******************************************************************************
 * Copyright (c) 2000, 2013 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Tom Eicher <eclipse@tom.eicher.name> - fix minimum width
 *******************************************************************************/

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Spinner;

/**
 * A cell editor that manages a integer entry field in a spinner.
 */
public class SpinnerCellEditor extends CellEditor {

	/**
	 * The Spinner control
	 */
	protected Spinner spinner;

	/**
	 * Constructor
	 */
	public SpinnerCellEditor(Composite parent, int minimum, int maximum, int increment) {
		this(parent, SWT.RIGHT, minimum, maximum, increment, 0);
	}

	/**
	 * Constructor
	 */
	public SpinnerCellEditor(Composite parent, int minimum, int maximum, int increment, int digits) {
		this(parent, SWT.RIGHT, minimum, maximum, increment, digits);
	}

	/**
	 * Constructor
	 */
	public SpinnerCellEditor(Composite parent, int style, int minimum, int maximum, int increment, int digits ) {
		super(parent, style);
		spinner.setMinimum(minimum);
		spinner.setMaximum(maximum);
		spinner.setIncrement(increment);
		spinner.setDigits(digits);
		spinner.setTextDirection(SWT.LEFT_TO_RIGHT);

	}

	/**
	 * @see org.eclipse.jface.viewers.CellEditor#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createControl(Composite parent) {
		spinner = new Spinner(parent, getStyle());
		spinner.addTraverseListener(new TraverseListener() {
			@Override
			public void keyTraversed(TraverseEvent e) {
				if (e.detail == SWT.TRAVERSE_ESCAPE || e.detail == SWT.TRAVERSE_RETURN) {
					e.doit = false;
					keyReleaseOccured(e);
				}
			}
		});
		spinner.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				SpinnerCellEditor.this.focusLost();
			}
		});
		spinner.setFont(parent.getFont());
		spinner.setBackground(parent.getBackground());
		return spinner;
	}


	/**
	 * @see org.eclipse.jface.viewers.CellEditor#doGetValue()
	 */
	@Override
	protected Object doGetValue() {
		return spinner.getSelection();
	}

	/**
	 * @see org.eclipse.jface.viewers.CellEditor#doSetFocus()
	 */
	@Override
	protected void doSetFocus() {
		if (spinner != null) {
			spinner.setFocus();
		}
	}

	/**
	 * @see org.eclipse.jface.viewers.CellEditor#doSetValue(java.lang.Object)
	 */
	@Override
	protected void doSetValue(Object value) {
		spinner.setSelection(((Number) value).intValue());
	}

	/**
	 * @see org.eclipse.jface.viewers.CellEditor#getLayoutData()
	 */
	@Override
	public LayoutData getLayoutData() {
		LayoutData data = new LayoutData();
		data.minimumWidth = 0;
		return data;
	}

}