/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.widget.usbl;

import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.io.usbl.UsblSettings;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;
import fr.ifremer.globe.ui.utils.LatLongInputFormatter;
import fr.ifremer.globe.ui.widget.datetime.InstantWidget;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;

public class UsblLayerParameterComposite extends Composite {

	/** Layer parameters */
	private UsblSettings parameters;

	/** Parameters consumer **/
	private final Consumer<UsblSettings> paramsConsumer;

	/** Inner widgets */
	private Spinner targetRaidusSpinner;
	private Spinner landingRadiusSpinner;
	private FormattedText longitudeformatedText;
	private Button btnDisplayTarget;
	private Button btnDisplayLanding;
	private Button btnDisplayAllPointsBeforeLanding;
	private FormattedText latitdueformatedText;
	private InstantWidget landingDateWidget;
	private SliderComposite opacitySliderComposite;
	private Text latitudeInputField;
	private Text longitudeInputField;
	private Label lblLandingDatetime;
	private Label lblLandingRadius;
	private Label lblLatitude;
	private Label lblLongitude;
	private Label lblRadiusm;

	/**
	 * @wbp.parser.constructor
	 */
	public UsblLayerParameterComposite(Composite parent, IWWUsblLayer layer) {
		this(parent, layer.getParameters(), params -> {
			layer.setParameters(params);
			IGeographicViewService.grab().refresh();
		}, true);
	}

	/**
	 * Create the composite.
	 */
	public UsblLayerParameterComposite(Composite parent, UsblSettings initParameters,
			Consumer<UsblSettings> paramsConsumer) {
		this(parent, initParameters, paramsConsumer, false);
	}

	/**
	 * Create the composite.
	 */
	public UsblLayerParameterComposite(Composite parent, UsblSettings initParameters,
			Consumer<UsblSettings> paramsConsumer, boolean displayOpacityWidget) {
		super(parent, SWT.NONE);
		this.parameters = initParameters;
		this.paramsConsumer = paramsConsumer;
		initializeComposite(displayOpacityWidget);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void initializeComposite(boolean withOpacity) {
		setLayout(new GridLayout(1, true));

		Label lblAttribute = new Label(this, SWT.NONE);
		lblAttribute.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblAttribute.setText("USBL display parameters");

		Group grpLanding = new Group(this, SWT.NONE);
		grpLanding.setLayout(new GridLayout(3, false));
		grpLanding.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpLanding.setText("Landing");

		btnDisplayLanding = new Button(grpLanding, SWT.CHECK);
		btnDisplayLanding.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		btnDisplayLanding.setText("Display USBL average position before landing");
		btnDisplayLanding.setSelection(parameters.displayLanding());
		btnDisplayLanding.addListener(SWT.Selection, e -> updateLayer());

		// Landing datetime
		lblLandingDatetime = new Label(grpLanding, SWT.NONE);
		lblLandingDatetime.setText("Landing datetime :");
		landingDateWidget = new InstantWidget(grpLanding, SWT.NONE, parameters.landingDateTime(), t -> updateLayer());
		landingDateWidget.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		// Duration before landing
		lblLandingRadius = new Label(grpLanding, SWT.NONE);
		lblLandingRadius.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblLandingRadius.setText("Duration before (sec) :");
		landingRadiusSpinner = new Spinner(grpLanding, SWT.BORDER);
		landingRadiusSpinner.setMinimum(1);
		landingRadiusSpinner.setMaximum(Integer.MAX_VALUE);
		landingRadiusSpinner.setSelection(parameters.durationBeforeLandingInSeconds());
		landingRadiusSpinner.addListener(SWT.Selection, e -> updateLayer());

		// Display all points before landing
		btnDisplayAllPointsBeforeLanding = new Button(grpLanding, SWT.CHECK);
		btnDisplayAllPointsBeforeLanding.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		btnDisplayAllPointsBeforeLanding.setText("Display data used to compute average");
		btnDisplayAllPointsBeforeLanding.setSelection(parameters.displayAllPointsBeforeLanding());
		btnDisplayAllPointsBeforeLanding.addListener(SWT.Selection, e -> updateLayer());

		// Target group

		Group grpTarget = new Group(this, SWT.NONE);
		grpTarget.setLayout(new GridLayout(2, false));
		grpTarget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpTarget.setText("Target");

		btnDisplayTarget = new Button(grpTarget, SWT.CHECK);
		btnDisplayTarget.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnDisplayTarget.setText("Display target");
		btnDisplayTarget.setSelection(parameters.displayTarget());
		btnDisplayTarget.addListener(SWT.Selection, e -> updateLayer());

		// Target latitude
		lblLatitude = new Label(grpTarget, SWT.NONE);
		lblLatitude.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLatitude.setText("Latitude :");

		latitudeInputField = new Text(grpTarget, SWT.BORDER);
		latitudeInputField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		latitdueformatedText = new FormattedText(latitudeInputField);
		latitdueformatedText.setFormatter(LatLongInputFormatter.getLatitudeFormatter());
		latitdueformatedText.setValue(parameters.targetLatitude());
		latitudeInputField.addModifyListener((e) -> updateLayer());

		// Target longitude
		lblLongitude = new Label(grpTarget, SWT.NONE);
		lblLongitude.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLongitude.setText("Longitude :");

		longitudeInputField = new Text(grpTarget, SWT.BORDER);
		longitudeInputField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		longitudeformatedText = new FormattedText(longitudeInputField);
		longitudeformatedText.setFormatter(LatLongInputFormatter.getLongitudeFormatter());
		longitudeformatedText.setValue(parameters.targetLongitude());
		longitudeInputField.addModifyListener((e) -> updateLayer());

		// Target radius
		lblRadiusm = new Label(grpTarget, SWT.NONE);
		lblRadiusm.setText("Radius (m) :");

		targetRaidusSpinner = new Spinner(grpTarget, SWT.BORDER);
		targetRaidusSpinner.setMinimum(1);
		targetRaidusSpinner.setMaximum(Integer.MAX_VALUE);
		targetRaidusSpinner.setSelection(parameters.targetRadius());
		targetRaidusSpinner.addListener(SWT.Selection, e -> updateLayer());

		// Opacity
		if (withOpacity) {
			Group grpOpacity = new Group(this, SWT.NONE);
			grpOpacity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			grpOpacity.setText("Opacity");
			grpOpacity.setLayout(GridLayoutFactory.fillDefaults().create());
			opacitySliderComposite = new SliderComposite(grpOpacity, SWT.NONE, false, t -> updateLayer());
			opacitySliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			opacitySliderComposite.setValue(parameters.opacity());
		}
	}

	/**
	 * Builds new {@link WWNavigationLayerParameters} from user selection and updates layer(s).
	 */
	private void updateLayer() {
		// update widget states
		boolean isLandingEnabled = btnDisplayLanding.getSelection();
		lblLandingDatetime.setEnabled(isLandingEnabled);
		landingDateWidget.setEnabled(isLandingEnabled);
		lblLandingRadius.setEnabled(isLandingEnabled);
		landingRadiusSpinner.setEnabled(isLandingEnabled);
		btnDisplayAllPointsBeforeLanding.setEnabled(isLandingEnabled);

		boolean isTargetEnabled = btnDisplayTarget.getSelection();
		lblLatitude.setEnabled(isTargetEnabled);
		latitudeInputField.setEnabled(isTargetEnabled);
		lblLongitude.setEnabled(isTargetEnabled);
		longitudeInputField.setEnabled(isTargetEnabled);
		lblRadiusm.setEnabled(isTargetEnabled);
		targetRaidusSpinner.setEnabled(isTargetEnabled);

		// build new parameters
		parameters = parameters.copyWith( //
				// target
				isTargetEnabled,
				Optional.ofNullable(latitdueformatedText.getValue()).map(Double.class::cast).orElse(Double.NaN),
				Optional.ofNullable(longitudeformatedText.getValue()).map(Double.class::cast).orElse(Double.NaN),
				targetRaidusSpinner.getSelection(),
				// landing
				isLandingEnabled, btnDisplayAllPointsBeforeLanding.getSelection(),
				landingDateWidget.getInstant().toEpochMilli(), landingRadiusSpinner.getSelection(),
				// opacity
				Optional.ofNullable(opacitySliderComposite).map(SliderComposite::getValue)
						.orElse(parameters.opacity()));

		paramsConsumer.accept(parameters);
	}
}
