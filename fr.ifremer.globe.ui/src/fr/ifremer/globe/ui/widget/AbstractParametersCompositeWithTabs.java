package fr.ifremer.globe.ui.widget;

import java.util.Optional;
import java.util.function.Consumer;

import org.apache.commons.lang.math.Range;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.widget.color.ColorContrastModel;
import fr.ifremer.globe.ui.widget.color.ColorContrastWidget;
import fr.ifremer.globe.ui.widget.player.IndexedPlayer;
import fr.ifremer.globe.ui.widget.player.IndexedPlayerComposite;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget.SpinnerWidgetLayout;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdWidget;

/**
 * Composite which contains tabs.
 */
public class AbstractParametersCompositeWithTabs extends Composite {

	protected CTabFolder tabFolder;

	/**
	 * Root constructor : initializes the composite.
	 */
	protected AbstractParametersCompositeWithTabs(Composite parent) {
		super(parent, SWT.NONE);
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);
	}

	/**
	 * Creates a new tab.
	 *
	 * @return the composite contained into the new tab.
	 */
	protected Composite createTab(String title) {

		// if first tab: init tab folder
		if (tabFolder == null) {
			tabFolder = new CTabFolder(this, SWT.TOP);
			tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			tabFolder.addListener(SWT.Selection, e -> onSelectedTab(tabFolder.getSelectionIndex()));
		}

		CTabItem playerTab = new CTabItem(tabFolder, SWT.NONE);
		playerTab.setText(title);
		Composite container = new Composite(tabFolder, SWT.NONE);
		container.setLayout(new GridLayout(1, true));
		playerTab.setControl(container);

		tabFolder.setSelection(0);
		tabFolder.layout(true, true);
		tabFolder.pack(true);

		return container;
	}

	/**
	 * Called when a tab in selected
	 *
	 * @param selectedTab : index of the selected tab
	 */
	protected void onSelectedTab(int selectedTab) {
		// usefull to child classes to save the selected tab
	}

	/**
	 * Creates a tab which contains all tools relatives to color and contrast features.
	 *
	 * @return the built {@link ColorContrastWidget}.
	 */
	protected ColorContrastWidget createContrastTab(ColorContrastModel initialParameters,
			Optional<Range> minMaxSyncValues, io.reactivex.functions.Consumer<? super ColorContrastModel> onNext) {
		Composite container = createTab("Contrast and color");

		ColorContrastWidget colorComposite = new ColorContrastWidget(container, initialParameters,
				minMaxSyncValues);
		colorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		colorComposite.subscribe(onNext);

		return colorComposite;
	}

	/**
	 * Creates a tab with a player.
	 */
	protected Composite createPlayerTab(IndexedPlayer player) {
		Composite container = createTab("Player");
		IndexedPlayerComposite playerComposite = new IndexedPlayerComposite(container, SWT.NONE, player);
		playerComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		return container;
	}

	/**
	 * Creates a tab containing all tools relatives to filter features.
	 *
	 * @return the built {@link FloatThresholdWidget}.
	 */
	protected FloatThresholdWidget createFilterTab(FloatThresholdModel initialParameters,
			io.reactivex.functions.Consumer<? super FloatThresholdModel> onNext) {
		Composite container = createTab("Filter");

		// create threshold value widget
		FloatThresholdWidget valueThreshold = new FloatThresholdWidget("Value threshold", container, initialParameters);
		valueThreshold.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// response to event
		valueThreshold.getPublisher().subscribe(onNext);
		return valueThreshold;
	}

	/**
	 * Creates a tab containing all tools relatives to potition features.
	 *
	 * @return the built {@link FloatSpinnerWidget}.
	 */
	protected Composite createPositionTab(EnabledNumberModel model, Consumer<EnabledNumberModel> onNext) {
		Composite container = createTab("Position");

		// create offset widget
		var offsetWidget = new SpinnerWidget("Vertical offset : ", container, model, 2, SpinnerWidgetLayout.HORIZONTAL);
		offsetWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// response to event
		offsetWidget.subscribe(onNext);
		return container;
	}

}
