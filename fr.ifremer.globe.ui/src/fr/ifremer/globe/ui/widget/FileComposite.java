package fr.ifremer.globe.ui.widget;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.ISWTObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.conversion.FileToStringConverter;
import fr.ifremer.globe.ui.databinding.conversion.StringToFileConverter;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.databinding.validation.Predicate;
import fr.ifremer.globe.ui.dnd.DropSupport;

public class FileComposite extends Composite {

	/** Selected File */
	protected WritableFile selectedFile;

	/** Requirements on expected file */
	protected int requirements = 0;

	/** Widgets */
	protected Text offsetFileText;
	protected Button browseFileButton;

	protected FileValidator fileValidator;

	/** Data binding */
	protected DataBindingContext bindingContext;

	/** File Validation binding */
	protected Binding fileBinding;

	/** Model to be updated by Composite */
	private SelectFileCompositeModel model;

	/**
	 * Create the composite used by WindowBuilder.
	 *
	 * @param requirements @see {@link fr.ifremer.globe.ui.databinding.validation.FileValidator}
	 */
	public FileComposite(Composite parent, int style) {
		super(parent, style);
		createContent();
	}

	/**
	 * Create the composite.
	 *
	 * Filename will be considered valid if it matches given requirements
	 *
	 * @param requirements @see {@link fr.ifremer.globe.ui.databinding.validation.FileValidator}
	 */
	public FileComposite(Composite parent, int style, WritableFile writableFile, int requirements) {
		this(parent, style, writableFile, requirements, null);
	}

	/**
	 * Constructor
	 */
	public FileComposite(Composite parent, int style, WritableFile writableFile, int requirements,
			DataBindingContext dataBindingContext) {
		this(parent, style);
		bindingContext = dataBindingContext;
		selectedFile = writableFile;
		this.requirements = requirements;

		fileValidator = new FileValidator(requirements);

		if (writableFile != null) {
			initializeDropSupport();
			initDataBindings();
		}
	}

	/**
	 * Create the composite.
	 *
	 * Given the content of the {@link SelectFileCompositeModel}, Filename will be considered valid if, either:
	 * <ul>
	 * <li>it matches the given requirements and ends with one of the provided inputFileFilterExtensions</li>
	 * <li>it matches the given requirements and ends with the provided validFileExtension</li>
	 * <li>it matches the given requirements (if no extension is given</li>
	 * </ul>
	 *
	 * @param requirements @see {@link fr.ifremer.globe.ui.databinding.validation.FileValidator}
	 * @param validFileExtension
	 */
	public FileComposite(Composite container, int style, SelectFileCompositeModel selectFileCompositeModel) {
		this(container, style);
		selectedFile = selectFileCompositeModel.getSelectedFile();
		Predicate<File> predicate = null;
		model = selectFileCompositeModel;
		requirements = model.getFileRequirements();

		if (model.getSelectedFileFilterExtensions() != null && !model.getSelectedFileFilterExtensions().isEmpty()) {
			List<String> extensionList = model.getSelectedFileFilterExtensions().stream()//
					.flatMap(pair -> Arrays.stream(pair.getSecond().split(";"))) //
					.map(extension -> extension.replace("*.", "")).collect(Collectors.toList());

			predicate = t -> {
				String filename = t.getName();
				if(extensionList.stream().anyMatch("*"::equals))
				{
					return null;
				}
				boolean result = extensionList.stream().anyMatch(filename::endsWith);
				if (result) {
					return null;
				} else {
					return "Filename does not match any required extension";
				}
			};
		} else if (model.getSelectedFileFilterExtension() != null) {
			predicate = t -> {
				if (t.getName().endsWith(model.getSelectedFileFilterExtension())) {
					return null;
				} else {
					return "Filename does not match required extension";
				}
			};
		}

		if (predicate != null) {
			fileValidator = new FileValidator(requirements, predicate);
		} else {
			fileValidator = new FileValidator(requirements);
		}

		if (selectedFile != null) {
			initializeDropSupport();
			initDataBindings();
		}
	}

	/**
	 * Create the player group of this composite.
	 */
	protected void createContent() {
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);
		setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		offsetFileText = new Text(this, SWT.BORDER);
		offsetFileText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		browseFileButton = new Button(this, SWT.NONE);
		browseFileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				browseFile();
			}
		});
		GridData gd_browseFileButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_browseFileButton.horizontalIndent = 5;
		browseFileButton.setLayoutData(gd_browseFileButton);
		browseFileButton.setText(" ... ");
	}

	/**
	 * Navigates the file system and select or enter a file name
	 */
	protected void browseFile() {
		// Expected only a folder ?
		if (onlyFolderExpected()) {
			DirectoryDialog dialog = new DirectoryDialog(getShell());
			if (selectedFile.isNotNull()) {
				dialog.setFilterPath(selectedFile.get().getAbsolutePath());
			}
			String file = dialog.open();
			if (file != null) {
				setFile(new File(file));
			}
		} else {
			FileDialog dialog = new FileDialog(getShell());
			if (selectedFile.isNotNull()) {
				dialog.setFilterPath(selectedFile.get().getAbsolutePath());
			}
			if (model != null && model.getSelectedFileFilterExtensions() != null) {
				List<String> filterExtensions = new ArrayList<>();
				model.getSelectedFileFilterExtensions().stream().forEach(ext -> filterExtensions.add(ext.getSecond()));
				dialog.setFilterExtensions(filterExtensions.toArray(new String[filterExtensions.size()]));

				java.util.List<String> filterNames = new ArrayList<>();
				model.getSelectedFileFilterExtensions().stream().forEach(ext -> filterNames.add(ext.getFirst()));
				filterNames.add("List of file");
				dialog.setFilterNames(filterNames.toArray(new String[filterNames.size()]));
			}
			String file = dialog.open();
			if (file != null) {
				setFile(new File(file));
			}
		}
	}

	protected boolean onlyFolderExpected() {
		return (requirements & FileValidator.FOLDER) != 0 && (requirements & FileValidator.FILE) == 0;
	}

	/** Initialize View/Model bindings */
	protected void initDataBindings() {
		if (bindingContext == null) {
			bindingContext = new DataBindingContext();
		}

		//
		// Bind OffsetFile to text widget
		ISWTObservableValue<String> observeTextOffsetFileTextObserveWidget = WidgetProperties.text(SWT.Modify)
				.observe(offsetFileText);
		UpdateValueStrategy<String, File> targetToModel = new UpdateValueStrategy<>();
		targetToModel.setAfterConvertValidator(fileValidator);
		targetToModel.setConverter(new StringToFileConverter());
		UpdateValueStrategy<File, String> modelToTarget = new UpdateValueStrategy<>();
		modelToTarget.setConverter(new FileToStringConverter());
		modelToTarget.setAfterConvertValidator(fileValidator);
		fileBinding = bindingContext.bindValue(observeTextOffsetFileTextObserveWidget, selectedFile, targetToModel,
				modelToTarget);
		//
		ControlDecorationSupport.create(fileBinding, SWT.TOP | SWT.RIGHT, this);
	}

	/** Adds support for dropping files into the control */
	protected void initializeDropSupport() {
		DropSupport.initializeOneFileDropSupport(offsetFileText, this::setFile);
	}

	/**
	 * Set the file value
	 */
	public void setFile(File file) {
		offsetFileText.setText(!onlyFolderExpected() || file.isDirectory() ? file.getAbsolutePath() : file.getParent());
	}

	/**
	 * @see org.eclipse.swt.widgets.Composite#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * @see org.eclipse.swt.widgets.Control#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		offsetFileText.setEnabled(enabled);
		browseFileButton.setEnabled(enabled);

		fileValidator.setEnabled(enabled);
		fileBinding.updateModelToTarget();
	}

	/**
	 * Getter of validationStatus
	 */
	public WritableBoolean getValidationStatus() {
		return fileValidator.getValidationStatus();
	}

	public Binding getFileBinding() {
		return fileBinding;
	}

	/**
	 * Model of the input file selection Composite
	 */
	public interface SelectFileCompositeModel {

		/**
		 * @return the files
		 */
		WritableFile getSelectedFile();

		/**
		 * Getter of required extension
		 *
		 * @see {@link fr.ifremer.globe.ui.databinding.validation.FileValidator}
		 */
		int getFileRequirements();

		/**
		 * Getter of required file extension
		 */
		String getSelectedFileFilterExtension();

		/**
		 * Getter of filterExtensions
		 */
		List<Pair<String, String>> getSelectedFileFilterExtensions();
	}

}
