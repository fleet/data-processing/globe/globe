package fr.ifremer.globe.ui.widget.inputs;

import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.text.NumberToStringConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.ibm.icu.text.NumberFormat;

import fr.ifremer.globe.ui.databinding.conversion.StringToDoubleConverter;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.validation.NumberFormatValidator;
import fr.ifremer.globe.ui.databinding.validation.NumberValidator;

/**
 * Text composite with binding and validation checking to set Double.
 */
public class DoubleWidget extends Composite {

	/** Context for bindings */
	private final DataBindingContext bindingContext = new DataBindingContext();

	private final WritableDouble value = new WritableDouble();

	private Binding binding;

	public DoubleWidget(Composite parent, int style, double initValue) {
		this(parent, style);
		value.set(initValue);
	}

	public DoubleWidget(Composite parent, int style) {
		this(parent, style, Optional.empty());
	}

	@SuppressWarnings("unchecked")
	public DoubleWidget(Composite parent, int style, Optional<DoubleRange> range) {
		super(parent, style);
		setLayout(new FillLayout());

		Text text = new Text(this, SWT.BORDER);

		IObservableValue<String> observeWidget = WidgetProperties.text(SWT.Modify).observe(text);
		UpdateValueStrategy<String, Double> targetToModel = new UpdateValueStrategy<>();
		var converter = new StringToDoubleConverter(null);
		targetToModel.setConverter(converter);

		NumberValidator numberValidator = range.map(NumberValidator::new).orElse(new NumberValidator());
		numberValidator.setNullAllowed(true);
		targetToModel.setAfterConvertValidator(numberValidator);

		targetToModel.setAfterGetValidator(new NumberFormatValidator());

		UpdateValueStrategy<Double, String> modelToTarget = new UpdateValueStrategy<>();
		modelToTarget.setConverter(NumberToStringConverter.fromDouble(NumberFormat.getInstance(Locale.US), false));

		binding = bindingContext.bindValue(observeWidget, value, targetToModel, modelToTarget);
		ControlDecorationSupport.create(binding, SWT.TOP | SWT.LEFT);
	}

	public WritableDouble get() {
		return value;
	}

	public IObservableValue<IStatus> getValidationStatus() {
		return binding.getValidationStatus();
	}

}
