package fr.ifremer.globe.ui.widget.color;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.nebula.jface.tablecomboviewer.TableComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

public class ColorComposite extends Composite {

	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	public static final String PROPERTY_COLORMAP_UPDATE = "COLORMAP_UPDATE";
	public static final String PROPERTY_INVERT_UPDATE = "INVERT_UPDATE";
	public static final String PROPERTY_INTERPOLATION_UPDATE = "INTERPOLATION_UPDATE";

	protected TableComboViewer colormapCombo;
	protected Button invertButton;
	protected ColormapComposite colormapComposite;
	private Button interpolationButton;

	public ColorComposite(Composite parent, int style) {
		this(parent, style, true, 0, false);
	}

	public ColorComposite(Composite parent, int style, boolean enabled, int colormapIndex, boolean inverted) {
		this(parent, style, enabled, ColorMap.getColormapsNames(), colormapIndex, inverted);
	}

	public ColorComposite(Composite parent, int style, boolean enabled, List<String> colormapsNames, int colormapIndex,
			boolean inverted) {
		super(parent, style);
		init(enabled, //
				colormapsNames != null && !colormapsNames.isEmpty() ? colormapsNames : ColorMap.getColormapsNames(), //
				colormapIndex, //
				inverted);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	private void init(boolean enabled, List<String> colormapsNames, int colormapIndex, boolean inverted) {
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		Group colorGroup = new Group(this, SWT.NONE);
		colorGroup.setText("Color");
		colorGroup.setLayout(new GridLayout(2, false));
		colorGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// create TableCombo
		colormapCombo = new TableComboViewer(colorGroup, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		colormapCombo.getTableCombo().setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		colormapCombo.setContentProvider(ArrayContentProvider.getInstance());
		// Column width. First column is hidden (width not managed properly)
		colormapCombo.getTableCombo().defineColumns(new int[] { 0, 150, 250 });
		colormapCombo.getTableCombo().setDisplayColumnIndex(1);
		colormapCombo.setLabelProvider(new ColorMapLabelProvider());
		colormapCombo.setInput(colormapsNames);

		// add listener
		colormapCombo.addSelectionChangedListener(e -> {
			int selectedColorMapIndex = getColormapSelectionIndex();
			if (selectedColorMapIndex >= 0 && selectedColorMapIndex != colormapComposite.getIndexColormap()) {
				colormapComposite.setIndexColormap(selectedColorMapIndex);
				pcs.firePropertyChange(PROPERTY_COLORMAP_UPDATE, null, selectedColorMapIndex);
			}
		});

		// invert button
		invertButton = new Button(colorGroup, SWT.CHECK);
		invertButton.setText("Invert");
		invertButton.setEnabled(enabled);
		invertButton.setSelection(inverted);
		invertButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				pcs.firePropertyChange(PROPERTY_INVERT_UPDATE, null, invertButton.getSelection());
				colormapComposite.setInvertColormap(invertButton.getSelection());
			}
		});
		invertButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false, 1, 1));

		colormapComposite = new ColormapComposite(colorGroup, SWT.BORDER | SWT.EMBEDDED, colormapIndex, inverted);
		GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, true);
		gridData.heightHint = 15;
		colormapComposite.setLayoutData(gridData);

		// interpolation button
		interpolationButton = new Button(colorGroup, SWT.CHECK);
		interpolationButton.setText("Interpolation");
		interpolationButton.addListener(SWT.Selection,
				evt -> pcs.firePropertyChange(PROPERTY_INTERPOLATION_UPDATE, null, interpolationButton.getSelection()));

		setColormapSelection(colormapIndex);
	}

	// Getter and setters

	public int getColormapSelectionIndex() {
		return getColormaps().indexOf(colormapCombo.getTableCombo().getText());
	}

	public List<?> getColormaps() {
		return ((List<?>) colormapCombo.getInput());
	}

	public void setColormapSelection(int selection) {
		if (getColormapSelectionIndex() != selection) {
			colormapCombo.getTableCombo().setText(getColormaps().get(selection).toString());
			colormapComposite.setIndexColormap(selection);
			pcs.firePropertyChange(PROPERTY_COLORMAP_UPDATE, null, selection);
		}
	}

	public boolean getInvertSelection() {
		return invertButton.getSelection();
	}

	public void setInvertSelection(boolean selection) {
		if (invertButton.getSelection() != selection) {
			invertButton.setSelection(selection);
			pcs.firePropertyChange(PROPERTY_INVERT_UPDATE, null, selection);
			colormapComposite.setInvertColormap(selection);
		}
	}

	public void setInvertEnabled(boolean b) {
		invertButton.setEnabled(b);
	}

	public boolean getInterpolationSelection() {
		return interpolationButton.getSelection();
	}

	public void setInterpolationSelection(boolean selection) {
		if (interpolationButton.getSelection() != selection) {
			interpolationButton.setSelection(selection);
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		invertButton.setEnabled(enabled);
		interpolationButton.setEnabled(enabled);
		colormapCombo.getTableCombo().setEnabled(enabled);
		if (!enabled)
			colormapComposite.setBackground(ColorUtils.convertGColorToSWT(GColor.GRAY));
		else if (getColormapSelectionIndex() >= 0)
			colormapComposite.setIndexColormap(getColormapSelectionIndex());
	}

	/**
	 * Provides label/palette of color for TableComboViewer of colormaps (
	 */
	private class ColorMapLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (columnIndex == 2) {
				Image result = ColorMap.getColormapImage(element.toString(), 250, 14);
				colormapCombo.getControl().addDisposeListener(e -> result.dispose());
				return result;
			}
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			return columnIndex == 1 ? element.toString() : null;
		}
	}
}
