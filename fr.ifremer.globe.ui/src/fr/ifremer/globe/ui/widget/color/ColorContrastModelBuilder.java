package fr.ifremer.globe.ui.widget.color;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;

import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.TerrainParameters;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;

public class ColorContrastModelBuilder {

	/** Color properties **/
	private int colorMapIndex = ColorMaps.GMT_JET.getIndex();
	private boolean invertColor = false;
	private boolean isInterpolated = false;

	/** Contrast properties */
	private boolean isInitialized = false;
	private float contrastMin = Float.NEGATIVE_INFINITY;
	private float contrastMax = Float.POSITIVE_INFINITY;
	private float limitMin = Float.NEGATIVE_INFINITY;
	private float limitMax = Float.POSITIVE_INFINITY;
	private float resetMin = Float.NEGATIVE_INFINITY;
	private float resetMax = Float.POSITIVE_INFINITY;

	/** Opacity **/
	private float opacity = 1;

	/** Histogram */
	private IntList histogram;

	public ColorContrastModelBuilder() {

	}

	public ColorContrastModelBuilder(ColorContrastModel src) {
		super();
		colorMapIndex = src.colorMapIndex();
		invertColor = src.invertColor();
		contrastMin = src.contrastMin();
		contrastMax = src.contrastMax();
		limitMin = src.limitMin();
		limitMax = src.limitMax();
		resetMin = src.resetMin();
		resetMax = src.resetMax();
		isInterpolated = src.interpolated();
		opacity = src.opacity();
	}

	public ColorContrastModelBuilder(WWTextureDisplayParameters src) {
		super();
		colorMapIndex = src.getColorMapIndex();
		invertColor = src.isColorMapReversed();
		contrastMin = src.getMinContrast();
		contrastMax = src.getMaxContrast();
		limitMin = src.getInitialMinContrast();
		limitMax = src.getInitialMaxContrast();
		resetMin = src.getInitialMinContrast();
		resetMax = src.getInitialMaxContrast();
		isInterpolated = src.isInterpolationEnabled();
		opacity = src.getOpacity();
	}

	/**
	 * Constructor with a {@link IWWTerrainLayer}
	 */
	public ColorContrastModelBuilder(IWWTerrainLayer layer, boolean isInterpolated, IntList histogram) {
		TerrainParameters textureParameters = layer.getTextureParameters();
		colorMapIndex = textureParameters.colorMap;
		invertColor = textureParameters.reverseColorMap;
		contrastMin = (float) textureParameters.minContrast;
		contrastMax = (float) textureParameters.maxContrast;
		limitMin = (float) textureParameters.minTextureValue;
		limitMax = (float) textureParameters.maxTextureValue;
		resetMin = (float) textureParameters.minTextureValue;
		resetMax = (float) textureParameters.maxTextureValue;
		this.isInterpolated = isInterpolated;
		opacity = (float) textureParameters.opacity;
		this.histogram = histogram;
	}

	public ColorContrastModel build() {
		return new ColorContrastModel(colorMapIndex, invertColor, contrastMin, contrastMax, limitMin, limitMax,
				resetMin, resetMax, isInterpolated, opacity, histogram != null ? histogram : new ArrayIntList(0));
	}

	public ColorContrastModelBuilder setColorMapIndex(int colorMapIndex) {
		this.colorMapIndex = colorMapIndex;
		return this;
	}

	public ColorContrastModelBuilder setInvertColor(boolean invertColor) {
		this.invertColor = invertColor;
		return this;
	}

	public ColorContrastModelBuilder setInterpolated(boolean isInterpolated) {
		this.isInterpolated = isInterpolated;
		return this;
	}

	public ColorContrastModelBuilder setContrastMin(float contrastMin) {
		this.contrastMin = contrastMin;
		this.resetMin = contrastMin;
		return this;
	}

	public ColorContrastModelBuilder setContrastMax(float contrastMax) {
		this.contrastMax = contrastMax;
		this.resetMax = contrastMax;
		return this;
	}

	public ColorContrastModelBuilder setLimitMin(float limitMin) {
		this.limitMin = limitMin;
		return this;
	}

	public ColorContrastModelBuilder setLimitMax(float limitMax) {
		this.limitMax = limitMax;
		return this;
	}

	public ColorContrastModelBuilder setResetMin(float resetMin) {
		this.resetMin = resetMin;
		return this;
	}

	public ColorContrastModelBuilder setResetMax(float resetMax) {
		this.resetMax = resetMax;
		return this;
	}

	public ColorContrastModelBuilder setOpacity(float opacity) {
		this.opacity = opacity;
		return this;
	}

}
