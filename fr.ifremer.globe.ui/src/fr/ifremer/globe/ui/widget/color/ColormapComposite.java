package fr.ifremer.globe.ui.widget.color;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

/**
 * This composite displays the color map on the parameters View of the layer.
 */
public class ColormapComposite extends CLabel {

	/** The color map index */
	private int indexColormap = 0;

	/** True if the color map is invert */
	private boolean invertColormap;

	/**
	 * @param parent
	 * @param style
	 * @param colormap
	 */
	public ColormapComposite(Composite parent, int style, int colormap) {
		this(parent, style, colormap, false);
	}

	/**
	 * @param parent
	 * @param style
	 * @param colormap
	 * @param invertColormap
	 */
	public ColormapComposite(Composite parent, int style, int colormap, boolean invertColormap) {
		super(parent, style| SWT.EMBEDDED);

		this.invertColormap = invertColormap;
		indexColormap = colormap;
		computeImage();
	}

	protected void computeImage() {
		ArrayList<java.awt.Color> awtColors = ColorMap.getColormapAwtValues(indexColormap);

		Color[] colors = new Color[awtColors.size()];
		int[] percents = new int[colors.length - 1];
		double percent = 100.0 / colors.length;
		for (int i = 0; i < colors.length; i++) {
			if( invertColormap) {
				colors[colors.length - i -1] = ColorUtils.convertAWTToSWT(awtColors.get(i));
			} else {
				colors[i] = ColorUtils.convertAWTToSWT(awtColors.get(i));				
			}
			if (i > 0) {
				percents[i - 1] = (int) percent;
				percent = Math.min(100.0, percent + (100.0 / colors.length));
			}
		}

		setBackground(colors, percents);
	}

	/**
	 * @return the index of the current color map.
	 */
	public int getIndexColormap() {
		return indexColormap;
	}

	/**
	 * Set the index of the current color map.
	 * 
	 * @param indexColormap
	 */
	public void setIndexColormap(int indexColormap) {
		this.indexColormap = indexColormap;
		computeImage();
	}

	public void setInvertColormap(boolean invertColormap) {
		this.invertColormap = invertColormap;
		computeImage();
	}

}