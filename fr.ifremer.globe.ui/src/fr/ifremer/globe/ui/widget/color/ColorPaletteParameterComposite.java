/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.widget.color;

import java.util.function.Consumer;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;

import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.core.utils.color.AColorPalette.Color;
import fr.ifremer.globe.ui.utils.CheckStateProviderFactory;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.ui.viewers.ColorCellLabelProvider;
import fr.ifremer.globe.ui.viewers.ColorEditingSupport;

public class ColorPaletteParameterComposite extends Composite {

	/** Model */
	private AColorPalette palette;
	private final Consumer<AColorPalette> onPaletteChanged;

	private Table colorTable;
	private CheckboxTableViewer colorTableViewer;

	/**
	 * Create the composite.
	 */
	public ColorPaletteParameterComposite(Composite parent, int style, AColorPalette palette,
			Consumer<AColorPalette> onPaletteChanged) {
		super(parent, style);
		this.palette = palette;
		this.onPaletteChanged = onPaletteChanged;

		initializeComposite();

		// Bindings
		bindAllAttributeNamesCombo();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 */
	private void initializeComposite() {
		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		Label lblAttribute = new Label(this, SWT.NONE);
		lblAttribute.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAttribute.setText("Palette");

		Composite tableComposite = new Composite(this, SWT.NONE);
		tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		colorTableViewer = CheckboxTableViewer.newCheckList(tableComposite,
				SWT.BORDER | SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
		colorTable = colorTableViewer.getTable();
		colorTable.setHeaderVisible(true);
		colorTable.setLinesVisible(true);

		// Check management
		colorTableViewer.setCheckStateProvider(CheckStateProviderFactory.build(Color.class, Color::active));
		colorTableViewer.addCheckStateListener(e -> {
			palette.activateColor((Color) e.getElement(), e.getChecked());
			colorTableViewer.setInput(palette.getColors());
			onPaletteChanged.accept(palette);
		});

		colorTable.addListener(SWT.EraseItem, event -> {
			// Highlight the row with the same color than the current Color
			Color coloredValue = (Color) event.item.getData();
			event.gc.setBackground(ColorUtils.convertGColorToSWT(coloredValue.gColor()));
			event.gc.fillRectangle(colorTable.getColumn(0).getWidth(), event.y, colorTable.getColumn(1).getWidth() - 2,
					event.height);
			event.detail &= ~SWT.HOT;
			event.detail &= ~SWT.SELECTED;
		});

		// Define columns
		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		tableComposite.setLayout(tableColumnLayout);
		defineValueColumn(tableColumnLayout);
		defineColorColumn(tableColumnLayout);
	}

	/** Defines the Value column of the TableViewer */
	private void defineValueColumn(TableColumnLayout tableColumnLayout) {
		TableViewerColumn valueTableViewerColumn = new TableViewerColumn(colorTableViewer, SWT.NONE);
		valueTableViewerColumn.getColumn().setText("Value");
		tableColumnLayout.setColumnData(valueTableViewerColumn.getColumn(), new ColumnWeightData(70, 50, true));
		valueTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				String result = ((Color) element).name();
				return result != null ? result : "<Empty>";
			}
		});
	}

	/** Defines the Color column of the TableViewer */
	private void defineColorColumn(TableColumnLayout tableColumnLayout) {
		TableViewerColumn colorTableViewerColumn = new TableViewerColumn(colorTableViewer, SWT.NONE);
		colorTableViewerColumn.getColumn().setText("Color");
		tableColumnLayout.setColumnData(colorTableViewerColumn.getColumn(), new ColumnWeightData(30, 20, true));
		colorTableViewerColumn.setLabelProvider(new ColorCellLabelProvider<Color>(Color::gColor));
		colorTableViewerColumn.setEditingSupport(new ColorEditingSupport<Color>(colorTableViewer, //
				Color::gColor, //
				(color, gColor) -> {
					palette.changeColor(color, gColor);
					colorTableViewer.setInput(palette.getColors());
					onPaletteChanged.accept(palette);
				}));

	}

	/** Initialize a specific binding on ComboViewer (not managed by Windows Builder) */
	private void bindAllAttributeNamesCombo() {
		colorTableViewer.setContentProvider(ArrayContentProvider.getInstance());
		colorTableViewer.setInput(palette.getColors());
	}

	/** {@inheritDoc} */
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
