package fr.ifremer.globe.ui.widget.color;

import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;

/**
 * A immutable class for ColorAndConstrast settings
 */
public record ColorContrastModel(int colorMapIndex, boolean invertColor, float contrastMin, float contrastMax,
		float limitMin, float limitMax, float resetMin, float resetMax, boolean interpolated, float opacity,
		IntList histogram) {

	/**
	 * Redefinition of the Record constructor
	 */
	public ColorContrastModel(int colorMapIndex, boolean invertColor, float contrastMin, float contrastMax,
			float limitMin, float limitMax, float resetMin, float resetMax, boolean interpolated, float opacity,
			IntList histogram) {
		this.colorMapIndex = colorMapIndex;
		this.invertColor = invertColor;
		this.contrastMin = contrastMin;
		this.contrastMax = contrastMax;
		this.limitMin = Math.min(contrastMin, limitMin);
		this.limitMax = Math.max(contrastMax, limitMax);
		this.resetMin = resetMin;
		this.resetMax = resetMax;
		this.interpolated = interpolated;
		this.opacity = opacity;
		this.histogram = histogram != null ? histogram : new ArrayIntList(0);
	}


	/**
	 * Constructor without histogram
	 */
	public ColorContrastModel(int colorMapIndex, boolean invertColor, float contrastMin, float contrastMax,
			float limitMin, float limitMax, float resetMin, float resetMax, boolean interpolated, float opacity) {
		this(colorMapIndex, invertColor, contrastMin, contrastMax, limitMin, limitMax, resetMin, resetMax, interpolated,
				opacity, new ArrayIntList());
	}


	/** Duplicates this ColorContrastModel with other values of limitMin and max */
	public ColorContrastModel withHistogram(IntList histogram) {
		return new ColorContrastModel(colorMapIndex, invertColor, contrastMin, contrastMax, limitMin, limitMax,
				resetMin, resetMax, interpolated, opacity, histogram);
	}

	/** Duplicates this ColorContrastModel with other values of limitMin and max */
	public ColorContrastModel withLimitMinMax(float limitMin, float limitMax) {
		return new ColorContrastModel(colorMapIndex, invertColor, contrastMin, contrastMax, limitMin, limitMax,
				resetMin, resetMax, interpolated, opacity, histogram);
	}

	/** Duplicates this ColorContrastModel with other values of contrastMin and max */
	public ColorContrastModel withContrastMinMax(float contrastMin, float contrastMax) {
		return new ColorContrastModel(colorMapIndex, invertColor, contrastMin, contrastMax, limitMin, limitMax,
				resetMin, resetMax, interpolated, opacity, histogram);
	}
	
	/** Duplicates this ColorContrastModel with other values of resetMin and max */
	public ColorContrastModel withResetMinMax(float resetMin, float resetMax) {
		return new ColorContrastModel(colorMapIndex, invertColor, contrastMin, contrastMax, limitMin, limitMax,
				resetMin, resetMax, interpolated, opacity, histogram);
	}
	
	public boolean isValid() {
		return Float.isFinite(contrastMin) && Float.isFinite(contrastMax);
	}

}
