package fr.ifremer.globe.ui.widget.color;

import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

/**
 * Composite to select a color.
 */
public class ColorPicker extends Composite {

	private GColor color = GColor.GRAY;
	private final Consumer<GColor> colorConsumer;

	/**
	 * Constructor
	 */
	public ColorPicker(Composite parent, int style, GColor initColor, Consumer<GColor> colorConsumer) {
		super(parent, style);
		this.colorConsumer = colorConsumer;
		addListener(SWT.MouseDown, event -> changeColor());
		setCursor(this.getDisplay().getSystemCursor(SWT.CURSOR_HAND));
		setColor(initColor != null ? initColor : color);
	}

	private void changeColor() {
		ColorDialog colorDialog = new ColorDialog(getShell());
		Color oldColor = getBackground();
		if (oldColor != null)
			colorDialog.setRGB(oldColor.getRGB());

		RGB rgb = colorDialog.open();
		if (rgb != null) {
			GColor newColor = ColorUtils.convertSWTToGColor(new Color(getDisplay(), rgb));
			setColor(newColor);
			colorConsumer.accept(newColor);
		}
	}

	public GColor getColor() {
		return color;
	}

	public void setColor(GColor color) {
		if (color != null) {
			this.color = color;
			setBackground(ColorUtils.convertGColorToSWT(color));
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		setBackground(ColorUtils.convertGColorToSWT(enabled ? color : GColor.GRAY));
	}

}