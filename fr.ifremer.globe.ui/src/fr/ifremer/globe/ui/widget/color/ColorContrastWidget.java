package fr.ifremer.globe.ui.widget.color;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.RowDataFactory;
import org.eclipse.jface.layout.RowLayoutFactory;
import org.eclipse.nebula.visualization.xygraph.dataprovider.CircularBufferDataProvider;
import org.eclipse.nebula.visualization.xygraph.figures.Axis;
import org.eclipse.nebula.visualization.xygraph.figures.IAxesFactory;
import org.eclipse.nebula.visualization.xygraph.figures.IXYGraph;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.TraceType;
import org.eclipse.nebula.visualization.xygraph.figures.XYGraph;
import org.eclipse.nebula.visualization.xygraph.util.XYGraphMediaFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.application.prefs.Preferences;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewService;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.widget.SliderAndSpinnersWidget;
import fr.ifremer.globe.ui.widget.slider.SliderComposite;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

/**
 * A widget allowing to display basic color and contrast setting
 */
public class ColorContrastWidget extends Composite implements PropertyChangeListener {

	/** Preferences of the bundle. */
	private Preferences preferences = ContextInitializer.getInContext(Preferences.class);

	// Widgets relatives to the contrast filter
	private SliderAndSpinnersWidget contrastSliderAndSpinners;
	// Widgets relatives to the color filter
	private ColorComposite colorComposite;

	/** Model to apply after a "reset" */
	private Range resetModel;
	/** Model to apply for a global "reset" */
	private Optional<Range> minMaxSyncValues = Optional.empty();
	/** Runnable to launch for an "adjust" contrast */
	private Optional<Runnable> autoContrast = Optional.empty();

	/** {@link PublisherSubject} for event in this composite */
	private PublishSubject<ColorContrastModel> publisher = PublishSubject.create();
	private SliderComposite opacitySliderComposite;
	private Button btnResetToMinmax;
	private Button btnResetToGlobalMinmax;
	private Button btnAutoContrast;

	// Histogram
	private boolean enableHistogram;
	private Canvas histogramCanvas;
	private Trace histogramTrace;
	private Trace contrastTrace;
	private Button btnEnableHistogram;

	/**
	 * Constructor
	 */
	public ColorContrastWidget(Composite parent, ColorContrastModel initialParameters,
			boolean createOpacityGroup) {
		super(parent, SWT.NONE);
		enableHistogram = preferences.getShowHistogramInColorContrast().getValue();

		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);

		createContrastGroup();
		createColorGroup();
		if (createOpacityGroup)
			createOpacityGroup();

		setModel(initialParameters, minMaxSyncValues);
	}

	public ColorContrastWidget(Composite parent, ColorContrastModel initialParameters,
			Optional<Range> minMaxSyncValues) {
		this(parent, initialParameters, true);
		setMinMaxSyncValues(minMaxSyncValues);
	}

	// UI Private methods

	private void createContrastGroup() {
		var contrastGroup = new Group(this, SWT.NONE);
		contrastGroup.setText("Contrast");
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.verticalSpacing = 0;
		contrastGroup.setLayout(gridLayout);
		contrastGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		createHistogramWidget(contrastGroup);

		contrastSliderAndSpinners = new SliderAndSpinnersWidget(contrastGroup);
		contrastSliderAndSpinners.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		contrastSliderAndSpinners.getPublisher().subscribe(contrastModel -> publish());

		var buttonGroup = new Composite(contrastGroup, SWT.NONE);
		RowLayoutFactory.fillDefaults().applyTo(buttonGroup);
		GridDataFactory.fillDefaults().applyTo(buttonGroup);

		btnResetToMinmax = new Button(buttonGroup, SWT.NONE);
		RowDataFactory.swtDefaults().applyTo(btnResetToMinmax);
		btnResetToMinmax.setText("Reset to min/max");
		btnResetToMinmax.setToolTipText("Reset range to min and max values of this layer");
		btnResetToMinmax.addListener(SWT.Selection, event -> {
			// reset contrast widget
			contrastSliderAndSpinners.update(resetModel.getMinimumFloat(), resetModel.getMaximumFloat(),
					Math.min(resetModel.getMinimumFloat(), contrastSliderAndSpinners.getMinLimit()),
					Math.max(resetModel.getMaximumFloat(), contrastSliderAndSpinners.getMaxLimit()));
			publish();
		});

		btnResetToGlobalMinmax = new Button(buttonGroup, SWT.NONE);
		RowDataFactory.swtDefaults().applyTo(btnResetToGlobalMinmax);
		btnResetToGlobalMinmax.setText("Reset to min/max");
		btnResetToGlobalMinmax.setToolTipText("Reset range to min and max values of all synchronized layer");
		btnResetToGlobalMinmax.addListener(SWT.Selection, //
				event -> minMaxSyncValues.ifPresent(minMax -> {
					contrastSliderAndSpinners.update(minMax.getMinimumFloat(), minMax.getMaximumFloat(),
							Math.min(minMax.getMinimumFloat(), contrastSliderAndSpinners.getMinLimit()),
							Math.max(minMax.getMaximumFloat(), contrastSliderAndSpinners.getMaxLimit()));
					publish();
				}));

		btnAutoContrast = new Button(buttonGroup, SWT.NONE);
		RowDataFactory.swtDefaults().applyTo(btnAutoContrast);
		btnAutoContrast.setText("Auto");
		btnAutoContrast.setToolTipText("Adjust contrast to fit displayed data");
		btnAutoContrast.setVisible(false);
		((RowData) btnAutoContrast.getLayoutData()).exclude = true;
		btnAutoContrast.addListener(SWT.Selection, //
				event -> autoContrast.ifPresent(runnable -> {
					runnable.run();
					publish();
				}));

		btnEnableHistogram = new Button(buttonGroup, SWT.NONE);
		RowDataFactory.swtDefaults().applyTo(btnEnableHistogram);
		btnEnableHistogram.setToolTipText("Show or hide histogram");
		btnEnableHistogram
				.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> revertHistogramVisibility()));
		btnEnableHistogram.setImage(ImageResources.getImage("/icons/16/histogram.png", getClass()));

	}

	private void revertHistogramVisibility() {
		enableHistogram = !enableHistogram;
		UIUtils.setVisible(histogramCanvas, enableHistogram);
		preferences.getShowHistogramInColorContrast().setValue(enableHistogram, false);
		preferences.save();

		ContextInitializer.getInContext(IParametersViewService.class).resize();
	}

	private void createHistogramWidget(Group contrastGroup) {
		histogramCanvas = new Canvas(contrastGroup, SWT.DOUBLE_BUFFERED);
		var gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 64;
		histogramCanvas.setLayoutData(gridData);
		UIUtils.setVisible(histogramCanvas, enableHistogram);

		// create a new XY Graph.
		LightweightSystem lws = new LightweightSystem(histogramCanvas);
		XYGraph histogramGraph = new XYGraph(new HistogramAxesFactory());
		lws.setContents(histogramGraph);

		// Configure XYGraph
		histogramGraph.setShowLegend(false);
		histogramGraph.setShowTitle(false);

		// create a trace for histogram
		CircularBufferDataProvider histogramTraceDataProvider = new CircularBufferDataProvider(false);
		histogramTrace = new Trace("Histo Plot", histogramGraph.getPrimaryXAxis(), histogramGraph.getPrimaryYAxis(),
				histogramTraceDataProvider);
		histogramTrace.setTraceType(TraceType.LINE_AREA);
		histogramTrace.setOpaque(true);
		histogramGraph.addTrace(histogramTrace);

		// create a trace for contrast
		CircularBufferDataProvider contrastTraceDataProvider = new CircularBufferDataProvider(false);
		contrastTrace = new Trace("Contrast Plot", histogramGraph.getPrimaryXAxis(), histogramGraph.getPrimaryYAxis(),
				contrastTraceDataProvider);
		contrastTrace.setTraceType(TraceType.LINE_AREA);
		contrastTrace.setLineWidth(2);
		contrastTrace.setAreaAlpha(200);
		contrastTrace.setTraceColor(XYGraphMediaFactory.getInstance().getColor(XYGraphMediaFactory.COLOR_GRAY));
		histogramGraph.addTrace(contrastTrace);

		UIUtils.setVisible(histogramCanvas, enableHistogram);
	}

	private void createColorGroup() {
		colorComposite = new ColorComposite(this, SWT.NONE, true, 0, false);
		colorComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		colorComposite.addPropertyChangeListener(this);
	}

	private void createOpacityGroup() {
		Group grpOpacity = new Group(this, SWT.NONE);
		grpOpacity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpOpacity.setText("Opacity");
		grpOpacity.setLayout(new GridLayout(1, false));
		opacitySliderComposite = new SliderComposite(grpOpacity, SWT.NONE, false, value -> publish());
		opacitySliderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	}

	/**
	 * @return current {@link ColorContrastModel}.
	 */
	public ColorContrastModel getModel() {
		return new ColorContrastModel(colorComposite.getColormapSelectionIndex(), colorComposite.getInvertSelection(),
				contrastSliderAndSpinners.getMin(), contrastSliderAndSpinners.getMax(),
				contrastSliderAndSpinners.getMinLimit(), contrastSliderAndSpinners.getMaxLimit(),
				resetModel.getMinimumFloat(), resetModel.getMaximumFloat(),
				colorComposite.getInterpolationSelection(),
				opacitySliderComposite != null ? opacitySliderComposite.getValue() : 1);
	}

	/**
	 * Updates the composite with a {@link ColorContrastModel}.
	 */
	public void setModel(ColorContrastModel model, Optional<Range> minMaxSyncValues) {
		// update the model to apply after a reset
		setResetModel(model);
		setModel(model);
		setMinMaxSyncValues(minMaxSyncValues);
	}

	/**
	 * Updates the composite with a {@link ColorContrastModel}.
	 */
	public void setModel(ColorContrastModel model) {
		if (!isDisposed()) {
			// check validity
			setEnabled(model.isValid());
			
			setHistogramModel(model);

			// update contrast widgets
			contrastSliderAndSpinners.update(model.contrastMin(), model.contrastMax(), model.limitMin(),
					model.limitMax());

			// update color widgets
			colorComposite.setColormapSelection(model.colorMapIndex());
			colorComposite.setInvertSelection(model.invertColor());
			colorComposite.setInterpolationSelection(model.interpolated());

			// update opacity
			if (opacitySliderComposite != null)
				opacitySliderComposite.setValue(model.opacity());
		}
	}

	/**
	 * Updates the histogram according the new model.
	 */
	public void setHistogramModel(ColorContrastModel model) {
		UIUtils.setVisible(histogramCanvas, enableHistogram && !model.histogram().isEmpty());
		btnEnableHistogram.setVisible(!model.histogram().isEmpty());

		if (!model.histogram().isEmpty()) {
			int maxHist = 0;

			int binCount = model.histogram().size();
			double binSize = (resetModel.getMaximumDouble() - resetModel.getMinimumDouble()) / binCount;
			double[] xDataArray = new double[binCount * 2 + 2];
			double[] yDataArray = new double[binCount * 2 + 2];
			xDataArray[0] = resetModel.getMinimumDouble();
			yDataArray[0] = 0d;
			for (int i = 0; i < binCount; i++) {
				xDataArray[i * 2 + 1] = xDataArray[i * 2];
				xDataArray[i * 2 + 2] = xDataArray[i * 2 + 1] + binSize;

				int histogramValue = model.histogram().get(i);
				yDataArray[i * 2 + 1] = histogramValue;
				yDataArray[i * 2 + 2] = histogramValue;
				maxHist = Math.max(maxHist, histogramValue);
			}
			xDataArray[xDataArray.length - 1] = resetModel.getMaximumDouble();
			yDataArray[xDataArray.length - 1] = 0d;

			CircularBufferDataProvider histogramTraceDataProvider = new CircularBufferDataProvider(false);
			histogramTraceDataProvider.setBufferSize(xDataArray.length);
			histogramTraceDataProvider.setCurrentXDataArray(xDataArray);
			histogramTraceDataProvider.setCurrentYDataArray(yDataArray);
			histogramTrace.setDataProvider(histogramTraceDataProvider);

			double minX = Math.min(model.contrastMin(), resetModel.getMinimumDouble());
			double maxX = Math.max(model.contrastMax(), resetModel.getMaximumDouble());

			CircularBufferDataProvider contrastTraceDataProvider = new CircularBufferDataProvider(false);
			contrastTraceDataProvider.setBufferSize(8);
			contrastTraceDataProvider.setCurrentXDataArray(new double[] { //
					minX, minX, model.contrastMin(), model.contrastMin(), //
					model.contrastMax(), model.contrastMax(), maxX, maxX });
			contrastTraceDataProvider.setCurrentYDataArray(new double[] { //
					0, maxHist, maxHist, 0, //
					0, maxHist, maxHist, 0 });
			contrastTrace.setDataProvider(contrastTraceDataProvider);

			// Adapt axis limits
			histogramTrace.getXAxis().setRange(minX, maxX);
			histogramTrace.getYAxis().setRange(0d, maxHist);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String evtName = event.getPropertyName();
		if (evtName.equals(ColorComposite.PROPERTY_COLORMAP_UPDATE)
				|| evtName.equals(ColorComposite.PROPERTY_INVERT_UPDATE)
				|| evtName.equals(ColorComposite.PROPERTY_INTERPOLATION_UPDATE)) {
			publish();
		}
	}

	/**
	 * Subscribes to model update.
	 */
	public Disposable subscribe(Consumer<? super ColorContrastModel> modelConsumer) {
		var disposable = publisher.subscribe(modelConsumer);
		addDisposeListener(e -> disposable.dispose());
		return disposable;
	}

	private void publish() {
		publisher.onNext(getModel());
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		contrastSliderAndSpinners.setEnabled(enabled);
		btnResetToMinmax.setEnabled(enabled);
		btnResetToGlobalMinmax.setEnabled(enabled);
		btnAutoContrast.setEnabled(enabled);
		btnEnableHistogram.setEnabled(enabled);
		colorComposite.setEnabled(enabled);
	}


	/** Change the value of the minMaxSync. Update UI */
	public void setMinMaxSyncValues(Optional<Range> minMaxSyncValues) {
		this.minMaxSyncValues = minMaxSyncValues;

		if (minMaxSyncValues.isPresent()) {
			btnResetToGlobalMinmax.setText(String.format("Reset to %.1f/%.1f", minMaxSyncValues.get().getMinimumFloat(),
					minMaxSyncValues.get().getMaximumFloat()));
		}
		btnResetToGlobalMinmax.setVisible(minMaxSyncValues.isPresent());
		((RowData) btnResetToGlobalMinmax.getLayoutData()).exclude = !minMaxSyncValues.isPresent();

		btnResetToGlobalMinmax.getParent().layout();
	}

	/** Change the value of the resetModel. Update UI */
	public void setResetModel(ColorContrastModel model) {
		this.resetModel = new FloatRange(model.resetMin(), model.resetMax());
		btnResetToMinmax.setText(
				String.format("Reset to %.1f/%.1f", resetModel.getMinimumFloat(), resetModel.getMaximumFloat()));
	}

	/** Set the runnable for auto contrast action */
	public void setAutoContrast(Optional<Runnable> autoContrast) {
		this.autoContrast = autoContrast;
		btnAutoContrast.setVisible(autoContrast.isPresent());
		((RowData) btnAutoContrast.getLayoutData()).exclude = !autoContrast.isPresent();
		btnAutoContrast.getParent().layout();
	}

	public static class HistogramAxesFactory implements IAxesFactory {

		@Override
		public Axis createXAxis() {
			return createAxis(false);
		}

		@Override
		public Axis createYAxis() {
			return createAxis(true);
		}

		public Axis createAxis(boolean yAxis) {
			Axis result = new Axis(yAxis ? IXYGraph.Y_AXIS : IXYGraph.X_AXIS, yAxis) {

				/**
				 * Redefined to avoid the computation of margins (No empty space around the plotting)
				 */
				@Override
				public void updateTick() {
					if (isDirty()) {
						setDirty(false);
						setMargin(0);
						setLength(isHorizontal() ? getClientArea().width : getClientArea().height);
					}
				}
			};
			result.setAutoScale(false);
			result.setVisible(false);
			return result;
		}
	}
}
