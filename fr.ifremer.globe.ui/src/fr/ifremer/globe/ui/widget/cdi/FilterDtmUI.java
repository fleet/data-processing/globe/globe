package fr.ifremer.globe.ui.widget.cdi;

import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalDouble;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.model.dtm.filter.FilterDtm;
import fr.ifremer.globe.core.model.dtm.filter.FilteringType;

public class FilterDtmUI extends Composite {

	private final Combo resetLayerCombo;
	private final Combo ifLayerCombo;
	private final Combo conditionCombo;
	private final Spinner valueA;
	private final Spinner valueB;
	private final Button btnDelete;
	private Listener deleteBtnListener;
	private Composite conditionComposite;

	// Conditions widgets
	private final StackLayout conditionStack;
	private final Composite numberComposite;
	private final Combo cdiCombo;

	/**
	 * Constructor
	 * 
	 * @wbp.parser.constructor
	 */
	FilterDtmUI(Composite parent) {
		this(parent, new String[] { "Cdi1", "Cdi2" }, new String[] { "elevation" },
				"All elevation between -137.0 -87.0");
	}

	public FilterDtmUI(Composite parent, String[] cdis, String[] layers, String filterAsString) {
		super(parent, SWT.NONE);

		GridLayout gridLayout = new GridLayout(5, false);
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);
		setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label lbl1 = new Label(this, SWT.NONE);
		lbl1.setEnabled(false);
		lbl1.setText("Reset layer");

		Label lbl2 = new Label(this, SWT.NONE);
		lbl2.setEnabled(false);
		lbl2.setText("If layer");

		Label lbl3 = new Label(this, SWT.NONE);
		lbl3.setEnabled(false);
		lbl3.setText("Condition");
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		resetLayerCombo = new Combo(this, SWT.READ_ONLY);
		resetLayerCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		resetLayerCombo.setToolTipText("Name of layer on which to apply cell reset\nChoose '" + FilterDtm.ALL_LAYERS
				+ "' to reset cells on all layers");

		ifLayerCombo = new Combo(this, SWT.READ_ONLY);
		ifLayerCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onIfLayerChanged();
			}
		});
		ifLayerCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		ifLayerCombo.setToolTipText("Name of the layer on which to apply the filter condition");

		conditionCombo = new Combo(this, SWT.READ_ONLY);
		conditionCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		conditionCombo.setToolTipText("Filter condition");
		conditionCombo.setItems(FilteringType.getDisplayNames());
		conditionCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FilteringType selection = FilteringType.resolveName(conditionCombo.getText());
				
				cdiCombo.setEnabled(selection.argsNumber() > 0);				
				valueA.setEnabled(selection.argsNumber() > 0);
				if (!valueA.isEnabled())
					valueA.setSelection(0);
				valueB.setEnabled(selection.argsNumber() > 1);
				if (!valueB.isEnabled())
					valueB.setSelection(0);
			}
		});

		conditionComposite = new Composite(this, SWT.NONE);
		conditionStack = new StackLayout();
		conditionComposite.setLayout(conditionStack);
		conditionComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		numberComposite = new Composite(conditionComposite, SWT.NONE);
		conditionStack.topControl = numberComposite;
		GridLayout glNumberComposite = new GridLayout(4, false);
		glNumberComposite.marginWidth = 0;
		glNumberComposite.marginHeight = 0;
		numberComposite.setLayout(glNumberComposite);

		Label a = new Label(numberComposite, SWT.NONE);
		a.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		a.setText("a:");

		valueA = new Spinner(numberComposite, SWT.BORDER);
		valueA.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		valueA.setMaximum(999999);
		valueA.setMinimum(-999999);
		valueA.setDigits(2);
		valueA.setIncrement(100);

		Label b = new Label(numberComposite, SWT.NONE);
		b.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		b.setText("b:");

		valueB = new Spinner(numberComposite, SWT.BORDER);
		valueB.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		valueB.setMaximum(999999);
		valueB.setMinimum(-999999);
		valueB.setDigits(2);
		valueB.setIncrement(100);

		cdiCombo = new Combo(conditionComposite, SWT.READ_ONLY);
		cdiCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		btnDelete = new Button(this, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setVisible(false);
				dispose();
				parent.pack();
			}
		});
		btnDelete.setText("-");

		initWidgets(cdis, layers, filterAsString);

	}

	/** Initialize widgets according to the filter */
	private void initWidgets(String[] cdis, String[] layers, String filterAsString) {
		// Fill combo "Layers to reset"
		String[] filterLayers = new String[layers.length + 1];
		filterLayers[0] = FilterDtm.ALL_LAYERS;
		System.arraycopy(layers, 0, filterLayers, 1, layers.length);
		resetLayerCombo.setItems(filterLayers);

		// Fill combo "If layer"
		String[] ifLayers = layers;
		if (cdis.length > 0) {
			// Add CDI filter
			ifLayers = Arrays.copyOf(layers, layers.length + 1);
			ifLayers[layers.length] = FilterDtm.CDI_LAYER;
		}
		ifLayerCombo.setItems(ifLayers);

		// Fill CDI combo
		if (cdis.length > 0) {
			cdiCombo.setItems(cdis);
			cdiCombo.select(0);
		}

		FilterDtm filterDtm = new FilterDtm(filterLayers[0], ifLayers[0], FilteringType.equal, Optional.empty(),
				OptionalDouble.of(0d), OptionalDouble.of(0d));
		try {
			filterDtm = FilterDtm.parseString(filterAsString);
		} catch (Exception e) {
			// Bad string format
		}

		resetLayerCombo.select(Math.max(Arrays.asList(filterLayers).indexOf(filterDtm.resetLayerName()), 0));
		ifLayerCombo.select(Math.max(Arrays.asList(ifLayers).indexOf(filterDtm.filterLayerName()), 0));
		onIfLayerChanged();

		conditionCombo.select(
				Math.max(Arrays.asList(conditionCombo.getItems()).indexOf(filterDtm.type().getDisplayName()), 0));

		filterDtm.valueCDI().ifPresent(cdi -> cdiCombo.select(Math.max(Arrays.asList(cdis).indexOf(cdi), 0)));
		filterDtm.valueA().ifPresent(a -> valueA.setSelection((int) (a * 100d)));
		filterDtm.valueB().ifPresent(b -> valueB.setSelection((int) (b * 100d)));
		cdiCombo.setEnabled(filterDtm.type().argsNumber() > 0);
		valueA.setEnabled(filterDtm.type().argsNumber() > 0);
		valueB.setEnabled(filterDtm.type().argsNumber() > 1);
	}

	/** Reacts to the change of the ifLayer value */
	private void onIfLayerChanged() {
		String selectedLayer = ifLayerCombo.getText();
		if (FilterDtm.CDI_LAYER.equals(selectedLayer)) {
			if (conditionStack.topControl != cdiCombo) {
				conditionStack.topControl = cdiCombo;
				cdiCombo.requestLayout();
				conditionCombo.setItems(FilteringType.equal.getDisplayName(), FilteringType.not_equal.getDisplayName(), FilteringType.missing.getDisplayName(), FilteringType.notMissing.getDisplayName());
				conditionCombo.select(0);
				FilteringType selection = FilteringType.resolveName(conditionCombo.getText());
				cdiCombo.setEnabled(selection.argsNumber() > 0);
			}
		} else if (conditionStack.topControl != numberComposite) {
			conditionStack.topControl = numberComposite;
			numberComposite.requestLayout();
			conditionCombo.setItems(FilteringType.getDisplayNames());
			conditionCombo.select(0);
			FilteringType selection = FilteringType.resolveName(conditionCombo.getText());
			valueA.setEnabled(selection.argsNumber() > 0);
			valueB.setEnabled(selection.argsNumber() > 1);
		}
	}

	public FilterDtm getFilter() {
		String resetLayerName = resetLayerCombo.getText();
		String filterLayerName = ifLayerCombo.getText();
		FilteringType type = FilteringType.resolveName(conditionCombo.getText());
		String cdi = cdiCombo.getText();
		double a = (double) valueA.getSelection() / 100;
		double b = (double) valueB.getSelection() / 100;

		if (type == FilteringType.between)
			return FilterDtm.makeForNumericBetweenCondition(resetLayerName, filterLayerName, a, b);
		if (FilterDtm.CDI_LAYER.equals(filterLayerName))
			return FilterDtm.makeForCDICondition(resetLayerName, type, cdi);
		return FilterDtm.makeForNumericCondition(resetLayerName, filterLayerName, type, a);
	}

	/**
	 * Adds the listener to the collection of listeners who will be notified when the receiver's values are modified
	 */
	public void addModifyListener(ModifyListener listener) {
		valueA.addModifyListener(listener);
		valueB.addModifyListener(listener);
		resetLayerCombo.addModifyListener(listener);
		ifLayerCombo.addModifyListener(listener);
		cdiCombo.addModifyListener(listener);
		conditionCombo.addModifyListener(listener);
		deleteBtnListener = e -> listener.modifyText(new ModifyEvent(e));
		btnDelete.addListener(SWT.Selection, deleteBtnListener);
	}

	/**
	 * Removes the listener to the collection of listeners who will be notified when the receiver's values are modified
	 */
	public void removeModifyListener(ModifyListener listener) {
		valueA.removeModifyListener(listener);
		valueB.removeModifyListener(listener);
		ifLayerCombo.removeModifyListener(listener);
		conditionCombo.removeModifyListener(listener);
		btnDelete.removeListener(SWT.Selection, deleteBtnListener);
	}
}
