package fr.ifremer.globe.ui.widget.cdi;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

import fr.ifremer.globe.core.model.dtm.cdi.CDIChecker;
import fr.ifremer.globe.core.model.dtm.cdi.CDIValue;

public class SecondColumnEditingSupport extends EditingSupport {

	private final TableViewer viewer;
	private final CellEditor editor;
	private final CDIChecker checker;

	public SecondColumnEditingSupport(TableViewer viewer, CDIChecker checker) {
		super(viewer);
		this.editor = new TextCellEditor(viewer.getTable());
		this.viewer=viewer;
		this.checker=checker;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return editor;
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	@Override
	protected Object getValue(Object element) {
		return ((CDIValue) element).getValue();
	}

	@Override
	protected void setValue(Object element, Object value) {
		((CDIValue) element).setValue(String.valueOf(value));
		viewer.update(element, null);
		checker.checkCDI((CDIValue) element);
	}

}
