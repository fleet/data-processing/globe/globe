package fr.ifremer.globe.ui.widget.cdi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.observable.list.WritableList;

import fr.ifremer.globe.core.model.dtm.cdi.CDIValue;

public class CDIWidgetParameters {
	public static String DEFAULT_CDI = "SDN:CDI:LOCAL:486_12345";

	public String firstColumnName;
	ArrayList<CDIValue> values = new ArrayList<CDIValue>();
	boolean CDIEditable = false;

	WritableList<File> files;
	WritableList<String> cdis;
	WritableList<String> originalCdis;
	String defaultValue = DEFAULT_CDI;

	/**
	 * Constructor
	 */
	public CDIWidgetParameters(String firstColumnName) {
		this.firstColumnName = firstColumnName;
	}

	/**
	 * Constructor to edit one CDI per file (Set CDI)
	 */
	public CDIWidgetParameters(WritableList<File> files, WritableList<String> cdis, String defaultValue) {
		this("File");
		this.files = files;
		this.cdis = cdis;
		this.defaultValue = defaultValue;
		linkCdiToFiles();
	}

	/**
	 * Constructor to modify CDIs
	 */
	public CDIWidgetParameters(WritableList<String> originalCdis, WritableList<String> cdis) {
		this("Source Identifier");
		this.originalCdis = originalCdis;
		this.cdis = cdis;
		linkCdiFromOriginals();
	}

	/**
	 * Build the "values" model by linking a cdi to each file
	 */
	public void linkCdiToFiles() {
		values.clear();
		for (int i = 0; i < files.size(); i++) {
			if (i >= cdis.size()) {
				cdis.add(defaultValue);
			}
			values.add(new CdiEntry(files.get(i).getAbsolutePath(), cdis.get(i)));
		}
	}

	/**
	 * Build the "values" model by linking a cdi to original one
	 */
	public void linkCdiFromOriginals() {
		values.clear();
		if (cdis.isEmpty()) {
			cdis.addAll(originalCdis);
			values.addAll(originalCdis.stream().map(cdi -> new CdiEntry(cdi, cdi)).collect(Collectors.toList()));
		} else {
			if (!cdis.isEmpty()) {
				for (int i = 0; i < originalCdis.size(); i++) {
					if (i >= cdis.size()) {
						cdis.add(originalCdis.get(i));
					}
					values.add(new CdiEntry(originalCdis.get(i), cdis.get(i)));
				}
			}
		}
	}

	public boolean isCDIEditable() {
		return CDIEditable;
	}

	public void setCDIEditable(boolean cDIEditable) {
		CDIEditable = cDIEditable;
	}

	public String getFirstColumnName() {
		return firstColumnName;
	}

	public void setFirstColumnName(String firstColumnName) {
		this.firstColumnName = firstColumnName;
	}

	public void add(String key, String value) {
		values.add(new CDIValue(key, value));
	}

	/**
	 * return the values
	 */
	public List<CDIValue> get() {
		return values;
	}

	/**
	 * @return the {@link #files}
	 */
	public WritableList<File> getFiles() {
		return files;
	}

	/**
	 * @return the {@link #originalCdis}
	 */
	public WritableList<String> getOriginalCdis() {
		return originalCdis;
	}

	/** CDIValue redefinition to maintain consistency between "values" list and the "cdis" WritableList */
	class CdiEntry extends CDIValue {

		/**
		 * Constructor
		 */
		public CdiEntry(String input, String modified) {
			super(input, modified);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setValue(String modifiedValue) {
			super.setValue(modifiedValue);
			cdis.clear();
			cdis.addAll(values.stream().map(CDIValue::getValue).collect(Collectors.toList()));
		}

	}

	/**
	 * @return the {@link #cdis}
	 */
	public WritableList<String> getCdis() {
		return cdis;
	}

}