package fr.ifremer.globe.ui.widget.cdi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.collections4.map.ListOrderedMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.dtm.cdi.CDIChecker;
import fr.ifremer.globe.core.model.dtm.cdi.CDIFileListParser;
import fr.ifremer.globe.core.model.dtm.cdi.CDIValue;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceId;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceIdBuilder;
import fr.ifremer.globe.ui.TableViewerColumnSorter;
import fr.ifremer.globe.ui.dnd.DropSupport;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.FileUtils;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.Functions;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * A widget displaying a list of key and associated CDI values in a table this widget is able to check is CDI exists in
 * databases on geoseas and seadatanet
 */
public class CDIKeyValueWidget extends Composite implements CDIChecker {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(CDIKeyValueWidget.class);

	protected Table table;

	/** empty parameter by default */
	CDIWidgetParameters parameters = new CDIWidgetParameters("default");

	enum Status {
		UNKNOWN, LOADING, INVALID, VALID, ERROR, UNSET;

		@Override
		public String toString() {
			return StringUtils.capitalize(name().toLowerCase());
		}
	}

	/** A map containing key and status */
	private ListOrderedMap<CDIValue, Status> statusMap = new ListOrderedMap<>();
	/** Id of the current checking pass */
	private int pass = 0;
	/** Button to check all CDI in error manually */
	private Button btnRefresh;

	/** Button to reset cdi modifications */
	private Button btnReset;

	private TableViewer tableViewer;

	/** Observer subscribed to mouseEventPublisher */
	protected Disposable onCheckCdiSubscriber;

	/**
	 * Create the composite.
	 */
	public CDIKeyValueWidget(Composite parent, int style, CDIWidgetParameters parameters) {
		super(parent, style);
		this.parameters = parameters;
		int numLayoutColumns = 3;
		GridLayoutFactory.fillDefaults().numColumns(numLayoutColumns).applyTo(this);

		createRefresh();
		createReset();
		ViewerFilter filter = createFilter();

		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		GridDataFactory.fillDefaults().grab(true, true).span(numLayoutColumns, 1).applyTo(table);
		tableViewer.setFilters(filter);
		TableViewerColumn firstColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		firstColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public Image getImage(Object element) {
				return null;
			}

			@Override
			public String getText(Object element) {
				return element == null ? "" : ((CDIValue) element).getInput();
			}
		});
		new TableViewerColumnSorter(firstColumn);
		TableColumn keyColumn = firstColumn.getColumn();
		keyColumn.setWidth(200);
		keyColumn.setText(parameters.firstColumnName);

		TableViewerColumn secondColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		secondColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public Image getImage(Object element) {
				return null;
			}

			@Override
			public String getText(Object element) {
				return element == null ? "" : ((CDIValue) element).getValue();
			}

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();
				int color = SWT.COLOR_GRAY;
				boolean modified = !((CDIValue) element).getValue().equals(((CDIValue) element).getInput());
				if (modified) {
					color = SWT.COLOR_BLACK;
				}
				cell.setText(((CDIValue) element).getValue());
				cell.setForeground(Display.getDefault().getSystemColor(color));
			}
		});
		new TableViewerColumnSorter(secondColumn);
		TableColumn cdiColumn = secondColumn.getColumn();
		cdiColumn.setWidth(200);
		cdiColumn.setText("CDI");

		TableViewerColumn thirdColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		var statusLabelProvider = new ColumnLabelProvider() {
			@Override
			public Image getImage(Object element) {
				return null;
			}

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();
				int color = SWT.COLOR_BLACK;
				Status status = statusMap.get(element);
				if (status == Status.INVALID || status == Status.ERROR) {
					color = SWT.COLOR_RED;
				} else if (status == Status.VALID) {
					color = SWT.COLOR_GREEN;
				}
				cell.setText(getLabel(status));
				cell.setForeground(Display.getDefault().getSystemColor(color));
			}

			/** {@inheritDoc} */
			@Override
			public String getText(Object element) {
				return getLabel(statusMap.get(element));
			}

			/** {@inheritDoc} */
			protected String getLabel(Status status) {
				return status != null ? status.toString() : "Unkown";
			}
		};
		thirdColumn.setLabelProvider(statusLabelProvider);
		new TableViewerColumnSorter(thirdColumn) {

			/** {@inheritDoc} */
			@Override
			protected Object getValue(Object element) {
				return statusLabelProvider.getText(element);
			}
		};
		TableColumn statusColumn = thirdColumn.getColumn();
		statusColumn.setWidth(200);
		statusColumn.setText("Status");

		// fill the table
		tableViewer.setContentProvider(new ArrayContentProvider());

		if (parameters.isCDIEditable()) {
			secondColumn.setEditingSupport(new SecondColumnEditingSupport(tableViewer, this));
		}
		addDisposeListener(e -> stop());

		if (parameters.getFiles() != null || parameters.getOriginalCdis() != null) {
			DropSupport.initializeOneFileDropSupport(table, this::setCdiTxtFile);
			Label lblNewLabel = new Label(this, SWT.NONE);
			lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
			GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.BEGINNING).grab(true, false).span(numLayoutColumns, 1)
					.applyTo(lblNewLabel);
			if (parameters.getFiles() != null) {
				lblNewLabel.setText("Drop a .txt file which contains a list of [File / CDI]");
			} else {
				lblNewLabel.setText("Drop a .txt file which contains a list of [CDI / CDI]");
			}
		}
	}

	/**
	 * Create the status filtering
	 */
	protected ViewerFilter createFilter() {
		Composite filterComposite = new Composite(this, SWT.NONE);
		GridLayoutFactory.fillDefaults().equalWidth(false).numColumns(7).applyTo(filterComposite);
		GridDataFactory.fillDefaults().grab(true, false).align(GridData.END, GridData.CENTER).applyTo(filterComposite);
		Label filterLabel = new Label(filterComposite, SWT.NONE);
		filterLabel.setText("Show");
		Button btnOnlyValid = new Button(filterComposite, SWT.RADIO);
		btnOnlyValid.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> tableViewer.refresh()));
		btnOnlyValid.setText("Valid");
		Button btnOnlyInvalid = new Button(filterComposite, SWT.RADIO);
		btnOnlyInvalid.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> tableViewer.refresh()));
		btnOnlyInvalid.setText("Invalid");
		Button btnOnlyInError = new Button(filterComposite, SWT.RADIO);
		btnOnlyInError.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> tableViewer.refresh()));
		btnOnlyInError.setText("In error");
		Button btnOnlyUnknown = new Button(filterComposite, SWT.RADIO);
		btnOnlyUnknown.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> tableViewer.refresh()));
		btnOnlyUnknown.setText("Unknown");
		Button btnOnlyLoading = new Button(filterComposite, SWT.RADIO);
		btnOnlyLoading.setText("Loading");
		Button btnBoth = new Button(filterComposite, SWT.RADIO);
		btnBoth.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> tableViewer.refresh()));
		btnBoth.setText("All");
		btnBoth.setSelection(true);

		return new ViewerFilter() {
			/** {@inheritDoc} */
			@Override
			public boolean select(Viewer viewer, Object parentElement, Object element) {
				if (btnBoth.getSelection()) {
					return true;
				}
				Status s = statusMap.get(element);
				if (s == Status.VALID && btnOnlyValid.getSelection()) {
					return true;
				}
				if (s == Status.INVALID && btnOnlyInvalid.getSelection()) {
					return true;
				}
				if (s == Status.ERROR && btnOnlyInError.getSelection()) {
					return true;
				}
				if (s == Status.UNKNOWN && btnOnlyUnknown.getSelection()) {
					return true;
				}
				return s == Status.LOADING && btnOnlyLoading.getSelection();
			}
		};
	}

	/**
	 * Create the refresh button
	 */
	protected void createRefresh() {
		btnRefresh = new Button(this, SWT.PUSH);
		btnRefresh.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			btnRefresh.setEnabled(false);
			relaunchCheckCDI();
		}));
		btnRefresh.setText("Check CDI");
		btnRefresh.setToolTipText("Check again all invalid or in error CDI");
		btnRefresh.setEnabled(false);
	}

	/**
	 * Create the reset button
	 */
	protected void createReset() {
		btnReset = new Button(this, SWT.PUSH);
		btnReset.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			resetCDI();
		}));
		btnReset.setText("Reset CDI");
		btnReset.setToolTipText("Reset all modified CDI");
		btnReset.setEnabled(true);
	}

	/**
	 * return the list of values, with respect to their insertion order
	 */
	public List<CDIValue> getValues() {
		ArrayList<CDIValue> list = new ArrayList<>();
		list.addAll(statusMap.keySet());
		return list;
	}

	public void populate() {
		statusMap.clear();
		parameters.get().forEach(cdi -> statusMap.put(cdi, Status.UNKNOWN));
		tableViewer.setInput(statusMap.asList());
		relaunchCheckCDI();
	}

	public void stop() {
		onCheckCdiSubscriber.dispose();
		onCheckCdiSubscriber = null;
	}

	@Override
	protected void checkSubclass() {
	}

	private void setStatus(CDIValue v, Status s) {
		synchronized (statusMap) {
			statusMap.put(v, s);
		}
	}

	private void refreshStatus(boolean async) {
		if (async || Display.getDefault().getThread() != Thread.currentThread()) {
			Display disp = Display.getDefault();
			disp.asyncExec(() -> {
				if (!tableViewer.getTable().isDisposed() && !tableViewer.isCellEditorActive()) {
					tableViewer.refresh(true);
				}
			});
		} else {
			if (!tableViewer.getTable().isDisposed() && !tableViewer.isCellEditorActive()) {
				tableViewer.refresh(true);
			}
		}
	}

	/** Called when CDI has been edited */
	@Override
	public void checkCDI(CDIValue value) {
		logger.debug("Check CDI {}", value.getValue());
		if (value.getValue() == null || value.getValue().isBlank()) {
			setStatus(value, Status.UNSET);
			refreshStatus(false);
			return;
		}

		setStatus(value, Status.LOADING);
		refreshStatus(false);

		DtmSourceId url = DtmSourceIdBuilder.build(value.getValue());
		var result = url.retrieve().state();
		switch (result) {
		case eExists:
			setStatus(value, Status.VALID);
			break;
		case eNotFound:
			setStatus(value, Status.INVALID);
			break;
		default:
			setStatus(value, Status.ERROR);
			break;
		}
		refreshStatus(true);
	}

	/**
	 * Set the file value
	 */
	public void setCdiTxtFile(File file) {
		// Txt file ?
		if (file != null && file.isFile() && "txt".equalsIgnoreCase(FilenameUtils.getExtension(file.getName()))) {
			boolean isFileKey = (parameters.getFiles() != null);
			CDIFileListParser parser = new CDIFileListParser();
			var cdiValuesFromFile = parser.readFile(file.getPath());
			if (!cdiValuesFromFile.isEmpty()) {
				BusyIndicator.showWhile(getDisplay(), () -> {
					var modifCount = new AtomicInteger();
					statusMap.entrySet().stream().forEach(entry -> {
						CDIValue currentCdiValue = entry.getKey();
						String currentCdiKey = isFileKey ? FileUtils.getBaseName(currentCdiValue.getInput())
								: currentCdiValue.getInput();
						// Current edited file present if txt file ?
						var option = cdiValuesFromFile.stream()//
								.filter(cdiValueFromFile -> currentCdiKey
										.equals(isFileKey ? FileUtils.getBaseName(cdiValueFromFile.getInput())
												: cdiValueFromFile.getInput()))//
								.findFirst();
						if (option.isPresent()) {
							// Update CDI
							currentCdiValue.setValue(option.get().getValue());
							entry.setValue(Status.UNKNOWN);
							modifCount.incrementAndGet();
						}
					});
					if (modifCount.get() > 0) {
						refreshStatus(true);
						Messages.openInfoMessage("CDI updated", "" + modifCount.get() + " CDI modified");
						relaunchCheckCDI();
					} else {
						Messages.openInfoMessage("CDI updated", isFileKey ? "No matching file" : "No matching CDI");
					}
				});
			}
		} else {
			Messages.openErrorMessage("Bad file", "Only .txt files are suitable");
		}
	}

	/** Relaunch the check of all CDI with status other than Valid */
	private void relaunchCheckCDI() {
		if (onCheckCdiSubscriber != null) {
			onCheckCdiSubscriber.dispose();
		}
		// A Subject that emits CDIValue for onCheckCdiSubscriber
		PublishSubject<CDIValue> publisher = PublishSubject.create();
		onCheckCdiSubscriber = publisher//
				.toFlowable(BackpressureStrategy.BUFFER)//
				.observeOn(Schedulers.io())//
				.subscribe(this::checkCDI, Functions.ON_ERROR_MISSING, this::onAllCdiChecked);

		// Increment the pass. Previous CdiToCheck for less pass will be discarded
		pass++;
		statusMap.entrySet().stream()//
				.filter(entry -> entry.getValue() != Status.VALID)//
				.map(Entry::getKey) //
				.forEach(publisher::onNext);
		publisher.onComplete();
	}

	/** Call when all CDI have been checked */
	private void onAllCdiChecked() {
		// Pass is odd => this is the first pass.
		if (pass % 2 == 1) {
			// Relaunch a second pass
			relaunchCheckCDI();
		} else {
			getDisplay().asyncExec(() -> btnRefresh.setEnabled(true));
		}
	}

	/** Reset tall CDI modifications */
	private void resetCDI() {
		this.parameters.getCdis().clear();
		if (this.parameters.getOriginalCdis() != null) {
			this.parameters.linkCdiFromOriginals();
		} else if (this.parameters.getFiles() != null) {
			this.parameters.linkCdiToFiles();
		}
		populate();
	}

}
