package fr.ifremer.globe.ui.widget.datetime;

import java.time.Instant;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;

/**
 * Composite to select an instant.
 */
public class InstantWidget extends Composite {

	private static final TimeZone UTC = TimeZone.getTimeZone("UTC");

	private DateTime date;
	private DateTime time;

	/**
	 * Constructor
	 */
	public InstantWidget(Composite parent, int style, Instant initInstant) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		date = new DateTime(this, SWT.BORDER | SWT.DATE | SWT.DROP_DOWN);
		time = new DateTime(this, SWT.BORDER | SWT.TIME);

		setInstant(initInstant);
	}

	/**
	 * Constructor
	 */
	public InstantWidget(Composite parent, int style, Instant initInstant, Consumer<Instant> instantConsumer) {
		this(parent, style, initInstant);

		date.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> instantConsumer.accept(getInstant())));
		time.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> instantConsumer.accept(getInstant())));

		setInstant(initInstant);
	}

	/**
	 * @return current date as {@link Instant}.
	 */
	public Instant getInstant() {
		Calendar calendar = Calendar.getInstance(UTC);
		calendar.clear();
		calendar.set(date.getYear(), date.getMonth(), date.getDay(), time.getHours(), time.getMinutes(),
				time.getSeconds());
		return calendar.toInstant();
	}

	/**
	 * Sets current date with {@link Instant}.
	 */
	public void setInstant(Instant instant) {
		Calendar calendar = Calendar.getInstance(UTC);
		calendar.setTimeInMillis(instant.toEpochMilli());
		date.setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		time.setTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		date.setEnabled(enabled);
		time.setEnabled(enabled);
	}

}