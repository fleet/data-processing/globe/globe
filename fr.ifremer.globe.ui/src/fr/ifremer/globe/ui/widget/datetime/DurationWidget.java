package fr.ifremer.globe.ui.widget.datetime;

import java.time.Duration;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;

/**
 * Composite to select an instant.
 */
public class DurationWidget extends Composite {

	private final Consumer<Duration> durationConsumer;
	private DateTime time;

	private static final TimeZone UTC = TimeZone.getTimeZone("UTC");

	/**
	 * Constructor
	 */
	public DurationWidget(Composite parent, int style, Duration initDuration, Consumer<Duration> durationConsumer) {
		super(parent, style);
		this.durationConsumer = durationConsumer;

		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		time = new DateTime(this, SWT.BORDER | SWT.TIME);
		time.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> updateDuration()));

		setDuration(initDuration);
	}

	public void updateDuration() {
		// get time interval
		Duration duration = Duration.ofSeconds(time.getSeconds());
		duration = duration.plusMinutes(time.getMinutes());
		duration = duration.plusHours(time.getHours());
		durationConsumer.accept(duration);
	}

	public void setDuration(Duration duration) {
		Calendar calendar = Calendar.getInstance(UTC);
		calendar.setTimeInMillis(duration.toMillis());
		time.setTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
	}

}