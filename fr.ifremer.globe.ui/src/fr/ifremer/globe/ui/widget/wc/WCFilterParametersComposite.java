package fr.ifremer.globe.ui.widget.wc;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.DefaultToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.model.wc.FilterParameters;
import fr.ifremer.globe.core.model.wc.SamplingParameter;
import fr.ifremer.globe.core.model.wc.filters.AcrossDistanceParameter;
import fr.ifremer.globe.core.model.wc.filters.BeamParameter;
import fr.ifremer.globe.core.model.wc.filters.BottomFilterParameter;
import fr.ifremer.globe.core.model.wc.filters.MultipingParameter;
import fr.ifremer.globe.core.model.wc.filters.DepthParameter;
import fr.ifremer.globe.core.model.wc.filters.SampleParameter;
import fr.ifremer.globe.core.model.wc.filters.SideLobeFilterParameter;
import fr.ifremer.globe.core.model.wc.filters.SpecularFilterParameter;
import fr.ifremer.globe.core.model.wc.filters.ThresholdParameter;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdModel;
import fr.ifremer.globe.ui.widget.threshold.FloatThresholdWidget;
import fr.ifremer.globe.ui.widget.threshold.IntThresholdModel;
import fr.ifremer.globe.ui.widget.threshold.IntThresholdWidget;
import fr.ifremer.globe.utils.exception.GIOException;
import io.reactivex.BackpressureStrategy;
import io.reactivex.subjects.PublishSubject;

public class WCFilterParametersComposite extends Composite {

	PublishSubject<FilterParameters> publisher = PublishSubject.create();

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<FilterParameters> getPublisher() {
		return publisher;
	}

	private FilterParameters filterParameters;
	private Runnable update;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public WCFilterParametersComposite(Composite parent, int style, FilterParameters defaultValues) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);
		if (defaultValues == null) {
			defaultValues = FilterParameters.getDefault();
		}
		this.filterParameters = FilterParameters.getDefaultFrom(defaultValues);

		Button enabledBtn = new Button(this, SWT.CHECK);
		enabledBtn.setText("Enable filtering");
		enabledBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		enabledBtn.setSelection(filterParameters.enabled);

		Button btnLoad = new Button(this, SWT.NONE);
		btnLoad.setToolTipText("Import");
		btnLoad.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/import_wiz.png"));
		btnLoad.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Button btnSave = new Button(this, SWT.NONE);
		btnSave.setToolTipText("Export");
		btnSave.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/export_wiz.png"));
		btnSave.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Composite container = new Composite(this, SWT.NONE);
		GridLayout gl_container = new GridLayout(1, false);
		gl_container.verticalSpacing = 0;
		container.setLayout(gl_container);
		container.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		// Sampling
		WCSamplingWidget sampling = new WCSamplingWidget(container, SWT.NONE,
				new WCSamplingModel(filterParameters.sampling.getSampling()));
		sampling.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// response to event
		sampling.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters.from(new SamplingParameter(model.threshold));
					publish();
				});

		// create threshold value widget
		FloatThresholdWidget threshold = new FloatThresholdWidget("Value", container,
				new FloatThresholdModel(filterParameters.threshold.getMinValue(),
						filterParameters.threshold.getMaxValue(), filterParameters.threshold.isEnable(), -128, 64));
		threshold.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// response to event
		threshold.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters
							.from(new ThresholdParameter(model.minValue, model.maxValue, model.enable));
					publish();
				});

		// Specular filter
		SpecularFilterComposite specular = new SpecularFilterComposite(container, SWT.NONE,
				new SpecularFilterModel(filterParameters.specular.isEnable(),
						filterParameters.specular.getFilterBelow(), filterParameters.specular.getTolerance()));
		specular.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		specular.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters
							.from(new SpecularFilterParameter(model.enable, model.filterBelow, model.tolerance));
					publish();
				});
		
		// Bottom filter
		BottomFilterComposite bottom = new BottomFilterComposite(container, SWT.NONE,
				new BottomFilterModel(filterParameters.bottom.isEnable(),
						filterParameters.bottom.getToleranceAbsolute(), filterParameters.bottom.getTolerancePercent(),
						filterParameters.bottom.getType(), filterParameters.bottom.getAngleCoefficient()));
		bottom.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		bottom.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters.from(new BottomFilterParameter(model.enable,
							model.toleranceAbsolute, model.tolerancePercent, model.type, model.angleCoefficient));
					publish();
				});

		// Side lobe filter
		WCSideLobeFilter sidelobe = new WCSideLobeFilter(container, SWT.NONE,
				new WCSideLobeModel(filterParameters.sidelobe.getThreshold(), filterParameters.sidelobe.isEnable()));
		sidelobe.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		sidelobe.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters
							.from(new SideLobeFilterParameter(model.enable, model.threshold));
					publish();
				});
		// create beam index widget
		BeamParameter beamParameter = filterParameters.beam;
		IntThresholdWidget beamFilter = new IntThresholdWidget("Beam index", container,
				new IntThresholdModel(beamParameter.getMinValue(), beamParameter.getMaxValue(),
						beamParameter.isEnable(), BeamParameter.MINDEFAULTVALUE, BeamParameter.MAXDEFAULTVALUE));
		beamFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		beamFilter.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters
							.from(new BeamParameter(model.minValue, model.maxValue, model.enable));
					publish();
				});
		DefaultToolTip beamTooltip = new DefaultToolTip(beamFilter.getEnableButton());
		beamTooltip.setImage(ImageResources.getImage("/icons/wc/wc_beam.png", getClass()));

		// create sample index widget
		SampleParameter sampleParameter = filterParameters.sample;
		IntThresholdWidget sampleFilter = new IntThresholdWidget("Sample index", container,
				new IntThresholdModel(sampleParameter.getMinValue(), sampleParameter.getMaxValue(),
						sampleParameter.isEnable(), SampleParameter.MINDEFAULTVALUE, SampleParameter.MAXDEFAULTVALUE));
		sampleFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		sampleFilter.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters
							.from(new SampleParameter(model.minValue, model.maxValue, model.enable));
					publish();
				});
		DefaultToolTip sampleTooltip = new DefaultToolTip(sampleFilter.getEnableButton());
		sampleTooltip.setImage(ImageResources.getImage("/icons/wc/wc_sample.png", getClass()));

		// create depth threshold value widget
		DepthParameter depthParameter = filterParameters.depth;
		FloatThresholdWidget depthFilter = new FloatThresholdWidget("Depth", container,
				new FloatThresholdModel(depthParameter.getMinValue(), depthParameter.getMaxValue(),
						depthParameter.isEnable(), DepthParameter.minDefaultValue, DepthParameter.maxDefaultValue));
		depthFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		depthFilter.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters
							.from(new DepthParameter(model.minValue, model.maxValue, model.enable));
					publish();
				});
		DefaultToolTip depthTooltip = new DefaultToolTip(depthFilter.getEnableButton());
		depthTooltip.setImage(ImageResources.getImage("/icons/wc/wc_depth.png", getClass()));

		// Across distance filter
		AcrossDistanceParameter acrossDistanceParameter = filterParameters.acrossDistance;
		FloatThresholdWidget filterAcrossDistance = new FloatThresholdWidget("Across distance", container,
				new FloatThresholdModel(acrossDistanceParameter.getMinValue(), acrossDistanceParameter.getMaxValue(),
						acrossDistanceParameter.isEnable(), AcrossDistanceParameter.minDefaultValue,
						AcrossDistanceParameter.maxDefaultValue));
		filterAcrossDistance.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		filterAcrossDistance.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters
							.fromAcrossDist(new AcrossDistanceParameter(model.minValue, model.maxValue, model.enable));
					publish();
				});
		DefaultToolTip acrossTooltip = new DefaultToolTip(filterAcrossDistance.getEnableButton());
		acrossTooltip.setImage(ImageResources.getImage("/icons/wc/wc_across.png", getClass()));

		// multiping sequence
		WCMultipingWidget multiping = new WCMultipingWidget(container, SWT.NONE,
				new EnabledNumberModel(filterParameters.multiping.isEnable(), filterParameters.multiping.getMultipingIndex()));
		multiping.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// response to event
		multiping.getPublisher().debounce(800, TimeUnit.MILLISECONDS).toFlowable(BackpressureStrategy.LATEST)
				.subscribe(model -> {
					filterParameters = filterParameters.from(new MultipingParameter(model.enable, (int) model.value));
					publish();
				});

		
		update = () -> {
			UIUtils.enableAllChildrenWidgets(container, enabledBtn.getSelection());
			filterParameters = filterParameters.fromEnabled(enabledBtn.getSelection());
			sampling.update(new WCSamplingModel(filterParameters.sampling.getSampling()));
			var thresholdModel = threshold.buildModelFromWidget();
			threshold.update(new FloatThresholdModel(filterParameters.threshold.getMinValue(),
					filterParameters.threshold.getMaxValue(), filterParameters.threshold.isEnable(),
					thresholdModel.minLimit, thresholdModel.maxLimit));
			specular.update(new SpecularFilterModel(filterParameters.specular.isEnable(),
					filterParameters.specular.getFilterBelow(), filterParameters.specular.getTolerance()));
			beamFilter.update(new IntThresholdModel(filterParameters.beam.getMinValue(),
					filterParameters.beam.getMaxValue(), filterParameters.beam.isEnable(),
					BeamParameter.MINDEFAULTVALUE, BeamParameter.MAXDEFAULTVALUE));
			sampleFilter.update(new IntThresholdModel(filterParameters.sample.getMinValue(),
					filterParameters.sample.getMaxValue(), filterParameters.sample.isEnable(),
					SampleParameter.MINDEFAULTVALUE, SampleParameter.MAXDEFAULTVALUE));
			depthFilter.update(new FloatThresholdModel(filterParameters.depth.getMinValue(),
					filterParameters.depth.getMaxValue(), filterParameters.depth.isEnable(),
					DepthParameter.minDefaultValue, DepthParameter.maxDefaultValue));
			sidelobe.update(new WCSideLobeModel(filterParameters.sidelobe.getThreshold(),
					filterParameters.sidelobe.isEnable()));
			bottom.update(new BottomFilterModel(filterParameters.bottom.isEnable(),
					filterParameters.bottom.getToleranceAbsolute(), filterParameters.bottom.getTolerancePercent(),
					filterParameters.bottom.getType(), filterParameters.bottom.getAngleCoefficient()));
			multiping.update(new EnabledNumberModel(filterParameters.multiping.isEnable(),filterParameters.multiping.getMultipingIndex()));
		};

		enabledBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				update.run();
				publish();
			}
		});
		update.run();

		Runnable save = () -> {
			FileDialog dialog = new FileDialog(container.getShell(), SWT.SAVE);

			dialog.setFilterNames(FilterParameters.CONFIGURATION_FORMAT_NAMES);
			dialog.setFilterExtensions(FilterParameters.CONFIGURATION_FORMAT_EXTENSIONS);

			final String path = dialog.open();

			// Create survey from xml session file
			if (path != null) {
				File outputFile = new File(path);
				try {
					filterParameters.save(outputFile);
				} catch (GIOException e1) {
					MessageDialog.openError(container.getShell(), "Error saving file" + outputFile,
							"Error saving file : (error message:" + e1.getMessage() + ")");
				}
			} else {
				return;
			}

		};

		Runnable load = () -> {
			FileDialog dialog = new FileDialog(container.getShell(), SWT.OPEN);

			dialog.setFilterNames(FilterParameters.CONFIGURATION_FORMAT_NAMES);
			dialog.setFilterExtensions(FilterParameters.CONFIGURATION_FORMAT_EXTENSIONS);

			final String path = dialog.open();

			// Create survey from xml session file
			if (path != null) {
				File outputFile = new File(path);
				try {
					var loadedFilters = FilterParameters.load(outputFile);
					filterParameters = FilterParameters.getDefaultFrom(loadedFilters);
					// refresh composites
					update.run();
					publish();
				} catch (GIOException e1) {
					MessageDialog.openError(container.getShell(), "Error loading file" + outputFile,
							"Error loading file : (error message:" + e1.getMessage() + ")");
				}
			} else {
				return;
			}
		};

		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				save.run();
			}
		});

		btnLoad.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				load.run();
			}
		});

	}

	public void updateParams(FilterParameters params) {
		filterParameters = FilterParameters.getDefaultFrom(params);
		update.run();
		publish();
	}

	private void publish() {
		publisher.onNext(filterParameters);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
