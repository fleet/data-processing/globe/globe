package fr.ifremer.globe.ui.widget.wc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import io.reactivex.subjects.PublishSubject;

public class WCMultipingWidget extends Composite {

	PublishSubject<EnabledNumberModel> publisher = PublishSubject.create();

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<EnabledNumberModel> getPublisher() {
		return publisher;
	}

	private Button btnEnable;
	private Spinner spinner;
	private EnabledNumberModel defaultValues;
	
	public void update(EnabledNumberModel model) {
		defaultValues = model;
		spinner.setSelection((int) (defaultValues.value));
		btnEnable.setSelection(defaultValues.enable);
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public WCMultipingWidget(Composite parent, int style, EnabledNumberModel defaultValues) {
		super(parent, style);
		setLayout(new GridLayout(4, false));
		this.defaultValues = defaultValues;

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		btnEnable.setText("Multiping sequence");
		btnEnable.setSelection(defaultValues.enable);

		var label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));

		var txtRemoveAllData = new Text(this, SWT.WRAP | SWT.MULTI);
		txtRemoveAllData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		txtRemoveAllData.setEditable(false);
		txtRemoveAllData.setText("Retains pings with multiping id");

		spinner = new Spinner(this, SWT.BORDER);
		spinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		spinner.setMaximum(15);
		spinner.setMinimum(0);
		spinner.setSelection((int) (defaultValues.value));
		spinner.setDigits(0);
	}

	private void publish() {
		publisher.onNext(new EnabledNumberModel(btnEnable.getSelection(), spinner.getSelection()));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
