package fr.ifremer.globe.ui.widget.wc;

import org.eclipse.jface.window.DefaultToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.utils.image.ImageResources;
import io.reactivex.subjects.PublishSubject;

public class SpecularFilterComposite extends Composite {

	PublishSubject<SpecularFilterModel> publisher = PublishSubject.create();

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<SpecularFilterModel> getPublisher() {
		return publisher;
	}

	private Button btnEnable;
	private Combo comboBellow;
	private Spinner spinner;
	private SpecularFilterModel defaultValues;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public SpecularFilterComposite(Composite parent, int style, SpecularFilterModel defaultValues) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		// hack for swt designer
		if (defaultValues == null) {
			defaultValues = new SpecularFilterModel(true, true, 3);
		}
		this.defaultValues = defaultValues;

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		btnEnable.setText("Specular");
		DefaultToolTip tip = new DefaultToolTip(btnEnable);
		tip.setImage(ImageResources.getImage("/icons/wc/wc_specular.png", getClass()));
		
		Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Composite grp = new Composite(this, SWT.NONE);//
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grp.setLayout(new GridLayout(3, false));

		Text txtRemoveAllDataLeft = new Text(grp, SWT.WRAP | SWT.MULTI);
		txtRemoveAllDataLeft.setEditable(false);
		txtRemoveAllDataLeft.setText("Remove all data");
		txtRemoveAllDataLeft.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		comboBellow = new Combo(grp, SWT.READ_ONLY);
		comboBellow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		comboBellow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		comboBellow.setItems("BELOW", "ON");

		Text txtRemoveAllDataRight = new Text(grp, SWT.WRAP | SWT.MULTI);
		txtRemoveAllDataRight.setEditable(false);
		txtRemoveAllDataRight.setText("specular sphere");
		txtRemoveAllDataRight.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblTolerance = new Label(grp, SWT.NONE);
		lblTolerance.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblTolerance.setText("Tolerance");

		spinner = new Spinner(grp, SWT.BORDER);
		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		spinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(grp, SWT.NONE);
		update(defaultValues);
	}

	public void update(SpecularFilterModel model) {
		this.defaultValues = model;
		btnEnable.setSelection(defaultValues.enable);
		comboBellow.select(defaultValues.filterBelow ? 0 : 1);
		updateSpinner();
	}

	/***
	 * Update spinner depending on type of tolerance
	 */
	private void updateSpinner() {
		spinner.setDigits(0);
		spinner.setMaximum(Integer.MAX_VALUE);
		spinner.setMinimum(Integer.MIN_VALUE);
		spinner.setSelection(defaultValues.tolerance);
		spinner.setIncrement((int) (1 * Math.pow(10, spinner.getDigits())));
	}

	private void publish() {
		SpecularFilterModel model = new SpecularFilterModel(btnEnable.getSelection(), //
				comboBellow.getSelectionIndex() == 0, //
				spinner.getSelection());
		// keep as default values
		defaultValues = model;
		publisher.onNext(model);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
