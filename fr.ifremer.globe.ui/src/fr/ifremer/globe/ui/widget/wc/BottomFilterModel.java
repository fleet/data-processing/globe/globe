package fr.ifremer.globe.ui.widget.wc;

import fr.ifremer.globe.core.model.wc.filters.BottomFilterParameter.ToleranceType;

public class BottomFilterModel {
	
	
	public final boolean enable;
	
	public final int toleranceAbsolute;
	public final double tolerancePercent;
	public final double angleCoefficient;
	public final ToleranceType type;

	public BottomFilterModel(boolean enable, int toleranceAbsolute, double tolerancePercent, ToleranceType type, double angleCoef) {
		super();
		this.enable = enable;
		this.toleranceAbsolute = toleranceAbsolute;
		this.tolerancePercent = tolerancePercent;
		this.type = type;
		this.angleCoefficient=angleCoef;
	}

	
}
