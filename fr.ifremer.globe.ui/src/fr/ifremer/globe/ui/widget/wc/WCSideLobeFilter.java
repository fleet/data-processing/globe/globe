package fr.ifremer.globe.ui.widget.wc;

import org.eclipse.jface.window.DefaultToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.utils.image.ImageResources;
import io.reactivex.subjects.PublishSubject;

public class WCSideLobeFilter extends Composite {

	PublishSubject<WCSideLobeModel> publisher = PublishSubject.create();

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<WCSideLobeModel> getPublisher() {
		return publisher;
	}

	private Text txtRemoveAllData;
	private Button btnEnable;
	private Spinner spinner;
	private WCSideLobeModel defaultValues;

	/**
	 * update widget
	 * 
	 * @param mode
	 */
	public void update(WCSideLobeModel model) {
		defaultValues = model;
		spinner.setSelection((int) (defaultValues.threshold * 100));
		btnEnable.setSelection(defaultValues.enable);

	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public WCSideLobeFilter(Composite parent, int style, WCSideLobeModel defaultValues) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		this.defaultValues = defaultValues;

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.setText("Side lobe");
		btnEnable.setSelection(defaultValues.enable);
		DefaultToolTip tip = new DefaultToolTip(btnEnable);
		tip.setImage(ImageResources.getImage("/icons/wc/wc_sidelobe.png", getClass()));
		
		Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Composite innerComposite = new Composite(this, SWT.NONE);
		innerComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		innerComposite.setLayout(new GridLayout(2, false));
		
		txtRemoveAllData = new Text(innerComposite, SWT.WRAP | SWT.MULTI);
		txtRemoveAllData.setEditable(false);
		txtRemoveAllData.setText(
				"Remove all data having value less than the median value for a given range plus the given threshold");
		txtRemoveAllData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		Label lblSideLobeFiltering = new Label(innerComposite, SWT.NONE);
		lblSideLobeFiltering.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblSideLobeFiltering.setText("Threshold");

		spinner = new Spinner(innerComposite, SWT.BORDER);
		spinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		spinner.setMaximum(128000);
		spinner.setMinimum(-128000);
		spinner.setDigits(2);
		spinner.setIncrement((int)Math.pow(10, spinner.getDigits()));

		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		btnEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		update(defaultValues);

	}

	private void publish() {
		publisher.onNext(new WCSideLobeModel((float) spinner.getSelection() / 100.f, btnEnable.getSelection()));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
