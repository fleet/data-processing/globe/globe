package fr.ifremer.globe.ui.widget.wc;

public class WCSideLobeModel{
	public final float threshold;
	public final boolean enable;
	public WCSideLobeModel(float threshold, boolean enable) {
		super();
		this.threshold = threshold;
		this.enable = enable;
	}
}