package fr.ifremer.globe.ui.widget.wc;

import org.eclipse.jface.window.DefaultToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.model.wc.filters.BottomFilterParameter;
import fr.ifremer.globe.core.model.wc.filters.BottomFilterParameter.ToleranceType;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import io.reactivex.subjects.PublishSubject;

public class BottomFilterComposite extends Composite {

	PublishSubject<BottomFilterModel> publisher = PublishSubject.create();

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<BottomFilterModel> getPublisher() {
		return publisher;
	}

	private Text txtRemoveAllData;
	private Button btnEnable;
	private Spinner spinner;
	private BottomFilterModel defaultValues;
	private Combo combo;
	private Spinner spinnerAngle;
	private Label lblcosangle;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public BottomFilterComposite(Composite parent, int style, BottomFilterModel defaultValues) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		// hack for swt designer
		if (defaultValues == null) {
			defaultValues = new BottomFilterModel(true, 5, 0.7, BottomFilterParameter.ToleranceType.RANGEPERCENT, 0);
		}
		this.defaultValues = defaultValues;

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		btnEnable.setText("Bottom detection");
		DefaultToolTip tip = new DefaultToolTip(btnEnable);
		tip.setImage(ImageResources.getImage("/icons/wc/wc_bottom.png", getClass()));
		
		Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Composite grp = new Composite(this, SWT.NONE);//
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grp.setLayout(new GridLayout(3, false));

		txtRemoveAllData = new Text(grp, SWT.WRAP | SWT.MULTI);
		txtRemoveAllData.setEditable(false);
		txtRemoveAllData.setText(
				"Remove all data after bottom detection minus the given tolerance.\r\nTolerance is either a percent of the bottom range or a number of samples plus an across angle dependant part.");
		txtRemoveAllData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		Label lblTolerance = new Label(grp, SWT.NONE);
		lblTolerance.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblTolerance.setText("Tolerance");

		spinner = new Spinner(grp, SWT.BORDER);
		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		spinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		combo = new Combo(grp, SWT.READ_ONLY);
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ToleranceType selection = ToleranceType.valueOf(combo.getItem(combo.getSelectionIndex()));
				updateSpinner(selection);
				publish();
			}
		});
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		new Label(grp, SWT.NONE);

		lblcosangle = new Label(grp, SWT.NONE);
		lblcosangle.setText(" + (1/cos(angle) - 1) x");
		lblcosangle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		spinnerAngle = new Spinner(grp, SWT.BORDER);
		spinnerAngle.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, false, false, 1, 1));
		spinnerAngle.setDigits(2);
		spinnerAngle.setMaximum(Integer.MAX_VALUE);
		spinnerAngle.setMinimum(Integer.MIN_VALUE);
		spinnerAngle.setIncrement((int) (1 * Math.pow(10, spinnerAngle.getDigits())));
		spinnerAngle.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});

		for (ToleranceType v : ToleranceType.values()) {
			combo.add(v.name());
		}
		update(defaultValues);
	}

	public void update(BottomFilterModel model) {
		this.defaultValues = model;
		btnEnable.setSelection(defaultValues.enable);
		for (int i = 0; i < combo.getItemCount(); i++) {
			if (combo.getItem(i).compareTo(defaultValues.type.name()) == 0) {
				combo.select(i);
				break;
			}
		}
		updateSpinner(defaultValues.type);
	}

	/***
	 * Update spinner depending on type of tolerance
	 */
	private void updateSpinner(ToleranceType type) {
		switch (type) {
		case SAMPLE:
			spinner.setDigits(0);
			spinner.setMaximum(Integer.MAX_VALUE);
			spinner.setMinimum(Integer.MIN_VALUE);
			spinner.setSelection(defaultValues.toleranceAbsolute);
			break;
		case RANGEPERCENT:
			spinner.setMaximum(Integer.MAX_VALUE);
			spinner.setMinimum(Integer.MIN_VALUE);
			spinner.setDigits(2);
			spinner.setSelection((int) (defaultValues.tolerancePercent * Math.pow(10, spinner.getDigits())));
			break;
		default:
			break;
		}
		spinner.setIncrement((int) (1 * Math.pow(10, spinner.getDigits())));
		
		spinnerAngle.setSelection((int)(defaultValues.angleCoefficient * Math.pow(10, spinnerAngle.getDigits())));
	}

	private void publish() {
		ToleranceType selection = ToleranceType.valueOf(combo.getItem(combo.getSelectionIndex()));
		BottomFilterModel model;
		switch (selection) {
		case SAMPLE:

			model = new BottomFilterModel(btnEnable.getSelection(),
					(int) (spinner.getSelection() / Math.pow(10, spinner.getDigits())), defaultValues.tolerancePercent,
					selection, (double) (spinnerAngle.getSelection() / Math.pow(10, spinnerAngle.getDigits())));
			break;
		case RANGEPERCENT:
		default:
			model = new BottomFilterModel(btnEnable.getSelection(), defaultValues.toleranceAbsolute,
					(spinner.getSelection() / Math.pow(10, spinner.getDigits())), selection,
					(double) (spinnerAngle.getSelection() / Math.pow(10, spinnerAngle.getDigits())));
			break;

		}
		// keep as default values
		defaultValues = model;

		publisher.onNext(model);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
