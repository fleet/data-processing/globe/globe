package fr.ifremer.globe.ui.widget.wc;

public class SpecularFilterModel {
	
	
	public final boolean enable;
	
	public final int tolerance;
	public final boolean filterBelow;

	public SpecularFilterModel(boolean enable, boolean filterBellow, int tolerance) {
		super();
		this.enable = enable;
		this.tolerance = tolerance;
		this.filterBelow = filterBellow;
	}	
}
