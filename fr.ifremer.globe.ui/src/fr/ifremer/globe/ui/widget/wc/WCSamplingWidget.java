package fr.ifremer.globe.ui.widget.wc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import io.reactivex.subjects.PublishSubject;

public class WCSamplingWidget extends Composite {

	PublishSubject<WCSamplingModel> publisher = PublishSubject.create();

	/**
	 * Get the publisher for event in this composite
	 */
	public PublishSubject<WCSamplingModel> getPublisher() {
		return publisher;
	}

	private Text txtRemoveAllData;
	private Button btnEnable;
	private Spinner spinner;
	private WCSamplingModel defaultValues;
	private Label label;
	private Text txtValues;

	public void update(WCSamplingModel model) {
		defaultValues = model;
		spinner.setSelection((int) (defaultValues.threshold));
		btnEnable.setSelection(defaultValues.threshold > 1);

	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public WCSamplingWidget(Composite parent, int style, WCSamplingModel defaValues) {
		super(parent, style);
		setLayout(new GridLayout(4, false));
		this.defaultValues = defaValues;

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		btnEnable.setText("Subsampling");
		btnEnable.setSelection(defaultValues.threshold > 1);

		label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));

		txtRemoveAllData = new Text(this, SWT.WRAP | SWT.MULTI);
		txtRemoveAllData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		txtRemoveAllData.setEditable(false);
		txtRemoveAllData.setText("Retains a value every");

		spinner = new Spinner(this, SWT.BORDER);
		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				publish();
			}
		});
		spinner.setMaximum(128000);
		spinner.setMinimum(1);
		spinner.setSelection((int) (defaultValues.threshold));
		spinner.setDigits(0);

		txtValues = new Text(this, SWT.MULTI);
		txtValues.setText("value");
		txtValues.setEditable(false);
		txtValues.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

	}

	private void publish() {
		if (btnEnable.getSelection()) {
			publisher.onNext(new WCSamplingModel(spinner.getSelection()));
		} else {
			publisher.onNext(new WCSamplingModel(1));
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
