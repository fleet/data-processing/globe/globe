package fr.ifremer.globe.ui.widget;

import java.util.Objects;
import java.util.Optional;

import org.eclipse.nebula.widgets.opal.rangeslider.RangeSlider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import io.reactivex.subjects.PublishSubject;

public class SliderAndSpinnersWidget extends Composite {

	/**
	 * Inner class which represents the model of this composite.
	 */
	public record Model(float min, float max, float minLimit, float maxLimit) {

		public Model withMax(float value) {
			return new Model(min, value, minLimit, value > maxLimit ? value : maxLimit);
		}

		public Model withMin(float value) {
			return new Model(value, max, value < minLimit ? value : minLimit, maxLimit);
		}

	}

	/** Slider max constant **/
	private static final int SLIDER_MAX = 1000;

	/** Instance of {@link Model} **/
	private Model model;

	/** {@link PublisherSubject} for event in this composite */
	private PublishSubject<Model> publisher = PublishSubject.create();

	/** UI components */
	private Spinner minSpinner;
	private Spinner maxSpinner;
	private RangeSlider rangeSlider;
	private int rangerSliderLowValue = Integer.MAX_VALUE;
	private int rangerSliderUpperValue = Integer.MIN_VALUE;

	private Optional<Integer> optDigits = Optional.empty();
	private int scaling;

	/**
	 * Constructor
	 */
	public SliderAndSpinnersWidget(Composite parent) {
		super(parent, SWT.NONE);
		setLayout(new GridLayout(3, false));

		// min spinner
		minSpinner = new Spinner(this, SWT.BORDER);
		GridData gdMinSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdMinSpinner.widthHint = 50;
		minSpinner.setLayoutData(gdMinSpinner);
		minSpinner.setMinimum(Integer.MIN_VALUE);
		minSpinner.setMaximum(Integer.MAX_VALUE);
		minSpinner.addListener(SWT.Selection, event -> {
			model = model.withMin(minSpinner.getSelection() / (float) scaling);
			updateRangeSlider();
			publisher.onNext(model);
		});

		// range slider
		rangeSlider = new RangeSlider(this, SWT.ON);
		rangeSlider.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		rangeSlider.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		rangeSlider.setMinimum(0);
		rangeSlider.setMaximum(SLIDER_MAX);
		rangeSlider.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				onRangeSliderChange();
			}
		});

		// max spinner
		maxSpinner = new Spinner(this, SWT.BORDER);
		GridData gdMaxSpinner = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gdMaxSpinner.widthHint = 50;
		maxSpinner.setLayoutData(gdMaxSpinner);
		maxSpinner.setMinimum(Integer.MIN_VALUE);
		maxSpinner.setMaximum(Integer.MAX_VALUE);
		maxSpinner.addListener(SWT.Selection, event -> {
			model = model.withMax(maxSpinner.getSelection() / (float) scaling);
			updateRangeSlider();
			publisher.onNext(model);
		});

	}

	private void onRangeSliderChange() {
		if (rangerSliderLowValue == rangeSlider.getLowerValue()
				&& rangerSliderUpperValue == rangeSlider.getUpperValue())
			return;
		rangerSliderLowValue = rangeSlider.getLowerValue();
		rangerSliderUpperValue = rangeSlider.getUpperValue();

		model = model
				.withMin((rangeSlider.getLowerValue() / (float) SLIDER_MAX) * (model.maxLimit - model.minLimit)
						+ model.minLimit)
				.withMax((rangeSlider.getUpperValue() / (float) SLIDER_MAX) * (model.maxLimit - model.minLimit)
						+ model.minLimit);
		updateSpinners();
		publisher.onNext(model);
	}

	private void updateRangeSlider() {
		int min = Math.round((SLIDER_MAX * (model.min - model.minLimit) / (model.maxLimit - model.minLimit)));
		int max = (int) Math.ceil((SLIDER_MAX * (model.max - model.minLimit) / (model.maxLimit - model.minLimit)));
		rangerSliderLowValue = Math.max(min, 0);
		rangerSliderUpperValue = Math.min(max, SLIDER_MAX);
		rangeSlider.setSelection(rangerSliderLowValue, rangerSliderUpperValue);
	}

	private void updateSpinners() {
		minSpinner.setSelection(Math.round(model.min * scaling));
		maxSpinner.setSelection(Math.round(model.max * scaling));
	}

	//// PUBLIC API

	/**
	 * Sets the number of digits to display.
	 */
	public void setDigits(int digits) {
		optDigits = Optional.of(digits);
	}

	/**
	 * Updates the view according to current display parameters.
	 */
	public void update(float min, float max, float minLimit, float maxLimit) {
		// Update ignored when it comes from one of the widget of this composite
		if (minSpinner.isFocusControl() || maxSpinner.isFocusControl() || rangeSlider.isFocusControl())
			return;

		var newModel = new Model(min, max, minLimit, maxLimit);
		if (!Objects.equals(model, newModel)) {
			this.model = newModel;
			// get or compute digits in function of range limits
			float range = maxLimit - minLimit;
			int digits = optDigits.orElseGet(() -> range < 1 ? 2 : range < 1000 ? 1 : 0);

			scaling = (int) Math.pow(10, digits);
			minSpinner.setDigits(digits);
			maxSpinner.setDigits(digits);
			updateSpinners();

			updateRangeSlider();
		}
	}

	/**
	 * Gets the publisher for event in this composite.
	 */
	public PublishSubject<Model> getPublisher() {
		return publisher;
	}

	@Override
	public void setEnabled(boolean enabled) {
		minSpinner.setEnabled(enabled);
		maxSpinner.setEnabled(enabled);
		rangeSlider.setEnabled(enabled);
		rangeSlider.redraw();
	}

	// GETTERS

	public float getMax() {
		return model.max;
	}

	public float getMaxLimit() {
		return model.maxLimit;
	}

	public float getMin() {
		return model.min;
	}

	public float getMinLimit() {
		return model.minLimit;
	}

	/**
	 * @return the {@link #model}
	 */
	public Model getModel() {
		return model;
	}
}
