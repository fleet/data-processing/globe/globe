package fr.ifremer.globe.ui.widget.player;

/**
 * Immutable class of the current status for this player
 * 
 */
public class IndexedPlayerStatus {
	/** The playingSpeed attribute. */
	public final float playingSpeed;
	/** The reversePlaying attribute true if the player is playing in a reverse (decreasing) way. */
	public final boolean reversePlaying;
	/** True if this player is currently playing. */
	public final boolean playing;
	/**
	 * True if this player should replay after last index has been played (loop).
	 */
	public final boolean loop;

	public IndexedPlayerStatus(float playingSpeed, boolean reversePlaying, boolean playing, boolean loop) {
		this.playingSpeed = playingSpeed;
		this.reversePlaying = reversePlaying;
		this.playing = playing;
		this.loop = loop;
	}
	/**
	 * @see #playingSpeed
	 * */
	public float getPlayingSpeed() {
		return playingSpeed;
	}

	/**
	 * @see #reversePlaying
	 * */
	public boolean isReversePlaying() {
		return reversePlaying;
	}

	/**
	 * @see #playing
	 * */
	public boolean isPlaying() {
		return playing;
	}

	/**
	 * @see #loop
	 * */
	public boolean isLoop() {
		return loop;
	}

	/**
	 * return a new immutable {@link IndexedPlayerStatus} as a copy of this and modifying the given parameter
	 * */
	public IndexedPlayerStatus fromLoop(boolean loop)
	{
		return new IndexedPlayerStatus(playingSpeed, reversePlaying, playing, loop);
	}

	/**
	 * return a new immutable {@link IndexedPlayerStatus} as a copy of this and modifying the given parameter
	 * */
	public IndexedPlayerStatus fromPlaying(boolean playing)
	{
		return new IndexedPlayerStatus(playingSpeed, reversePlaying, playing, loop);
	}

	/**
	 * return a new immutable {@link IndexedPlayerStatus} as a copy of this and modifying the given parameter
	 * */
	public IndexedPlayerStatus fromReversePlaying(boolean reversePlaying)
	{
		return new IndexedPlayerStatus(playingSpeed, reversePlaying, playing, loop);
	}
	/**
	 * return a new immutable {@link IndexedPlayerStatus} as a copy of this and modifying the given parameter
	 * */
	public IndexedPlayerStatus fromSpeed(float playingSpeed)
	{
		return new IndexedPlayerStatus(playingSpeed, reversePlaying, playing, loop);
	}
}