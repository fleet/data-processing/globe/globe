package fr.ifremer.globe.ui.widget.player;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.events.BaseEventTopics;
import fr.ifremer.globe.ui.events.PlayerEvent;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.widget.slider.PlayerSlider;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;

/**
 * Composite for {@link IndexedPlayer}.
 */
public class IndexedPlayerComposite extends Composite {

	private static final String[] SPEED_VALUES = { "0.1", "0.2", "0.5", "1.0", "2.0", "4.0", "8.0" };

	private static final String BACKWARD_ICON = "/icons/32/media-step-backward.png";
	private static final String FORWARD_ICON = "/icons/32/media-step-forward.png";
	private static final String PLAY_ICON = "/icons/32/media-playback-start.png";
	private static final String BACK_ICON = "/icons/32/media-reverse-start.png";
	private static final String PAUSE_ICON = "/icons/32/media-playback-pause.png";

	/** Associated {@link IndexedPlayer} **/
	private final IndexedPlayer player;

	/** Inner composites **/
	private Spinner minIndexSpinner;
	private Spinner currentIndexSpinner;
	private Spinner maxIndexSpinner;
	private PlayerSlider indexSlider;
	private Button reverseButton;
	private Button playButton;
	private Button repeatButton;
	private Combo speedCombo;
	private Spinner incrementSpinner;
	private Group grpMinValue;
	private Group grpCurrentValue;
	private Group grpMaxValue;

	private final CompositeDisposable disposable = new CompositeDisposable();

	/**
	 * Constructor
	 */
	public IndexedPlayerComposite(Composite parent, int style, IndexedPlayer player) {
		super(parent, style);
		this.player = player;

		// force injection for IEventBroker
		ContextInitializer.inject(this);

		// subscribe for data
		disposable.add(player.subscribe(data -> UIUtils.asyncExecSafety(this, this::refreshComposite)));

		// subscribe for status
		disposable.add(player.getStatusPublisher()
				.subscribe(status -> UIUtils.asyncExecSafety(this, this::refreshPlayingButtons)));

		createUI();
		initComposite();

		// release subscribers on dispose
		addDisposeListener(evt -> disposable.clear());
	}

	@Inject
	@Optional
	private void onPlayerEvent(@UIEventTopic(BaseEventTopics.TOPIC_BASE) PlayerEvent event) {
		if (!isDisposed()) {
			switch (event.getAction()) {
			case PLAY_PAUSE:
				player.playPause();
				break;
			case REVERT_PLAY_PAUSE:
				player.revertPlayPause();
				break;
			case STEP_BACKWARD:
				player.stepBackward();
				break;
			case STEP_FORWARD:
				player.stepForward();
				break;
			}
		}
	}

	private void refreshComposite() {
		if (!currentIndexSpinner.isDisposed())
			currentIndexSpinner.setSelection(player.getCurrent());
		if (!indexSlider.isDisposed()) {
			indexSlider.setSelectorVisible(player.hasCurrent());
			indexSlider.setSelection(player.getCurrent());
		}
		if (!grpCurrentValue.isDisposed())
			updateGroupTitle(grpCurrentValue, player.getCurrent());
	}

	private void refreshPlayingButtons() {
		if (!isDisposed() && !playButton.isDisposed() && !reverseButton.isDisposed()) {
			boolean playing = player.getStatus().isPlaying();
			boolean reversePlaying = player.getStatus().isReversePlaying();
			playButton.setSelection(playing);
			reverseButton.setSelection(reversePlaying);

			if (reversePlaying) {
				playButton.setImage(ImageResources.getImage(PLAY_ICON, getClass()));
				reverseButton.setImage(ImageResources.getImage(PAUSE_ICON, getClass()));
			} else if (playing) {
				playButton.setImage(ImageResources.getImage(PAUSE_ICON, getClass()));
				reverseButton.setImage(ImageResources.getImage(BACK_ICON, getClass()));
			} else { // pause
				playButton.setImage(ImageResources.getImage(PLAY_ICON, getClass()));
				reverseButton.setImage(ImageResources.getImage(BACK_ICON, getClass()));
			}
		}
	}

	private void initComposite() {
		minIndexSpinner.setMinimum(player.getData().minLimit());
		minIndexSpinner.setMaximum(player.getData().maxLimit());
		minIndexSpinner.setSelection(player.getMinPlayIndex());

		updateGroupTitle(grpCurrentValue, player.getCurrent());
		currentIndexSpinner.setMaximum(player.getMaxPlayIndex());
		currentIndexSpinner.setSelection(player.getCurrent());

		maxIndexSpinner.setMinimum(player.getData().minLimit());
		maxIndexSpinner.setMaximum(player.getData().maxLimit());
		maxIndexSpinner.setSelection(player.getMaxPlayIndex());

		indexSlider.setMinimum(player.getMinPlayIndex());
		indexSlider.setMaximum(player.getMaxPlayIndex());
		indexSlider.setSelectorVisible(player.hasCurrent());
		indexSlider.setSelection(player.getCurrent());

		repeatButton.setSelection(player.getLoop());
		speedCombo.select(Arrays.asList(SPEED_VALUES).indexOf(Float.toString(player.getPlayingSpeed())));
		incrementSpinner.setSelection(player.getReadIncrement());

		refreshPlayingButtons();
	}

	private void updateGroupTitle(Group group, int index) {
		String prefix = "current index";
		if (group == grpMinValue)
			prefix = "min";
		else if (group == grpMaxValue)
			prefix = "max";

		String title = player.getLabelProvider().apply(index);
		group.setText(title.isEmpty() ? prefix : prefix + " (" + title + ")");
	}

	private void setMaxPlayerSelection(int value) {
		player.setPlaying(false);
		player.setReversePlaying(false);
		player.setMaxPlayIndex(value);

		if (value < indexSlider.getSelection()) {
			player.setCurrent(maxIndexSpinner.getSelection());
		}
		currentIndexSpinner.setMaximum(value);
		indexSlider.setMaximum(value);
		if (indexSlider.getSelection() != player.getCurrent()) {
			indexSlider.setSelection(player.getCurrent());
		}

		updateGroupTitle(grpMaxValue, value);
	}

	private void setMinPlayerSelection(int value) {
		player.setPlaying(false);
		player.setReversePlaying(false);
		player.setMinPlayIndex(value);

		if (value > indexSlider.getSelection()) {
			player.setCurrent(value);
		}

		currentIndexSpinner.setMinimum(value);
		indexSlider.setMinimum(value);
		if (indexSlider.getSelection() != player.getCurrent()) {
			indexSlider.setSelection(player.getCurrent());
		}

		updateGroupTitle(grpMinValue, value);
	}

	/**
	 * Creates the interface and inner composites.
	 */
	private void createUI() {
		// main layout
		GridLayout layout = new GridLayout(3, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		setLayout(layout);
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		// min index
		grpMinValue = new Group(this, SWT.NONE);
		updateGroupTitle(grpMinValue, player.getData().minLimit());
		grpMinValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpMinValue.setLayout(new GridLayout(2, false));
		Button buttonEraseMin = new Button(grpMinValue, SWT.FLAT);
		buttonEraseMin.addListener(SWT.Selection, event -> {
			minIndexSpinner.setSelection(player.getData().minLimit());
			setMinPlayerSelection(player.getData().minLimit());
		});
		buttonEraseMin.setImage(ImageResources.getImage("icons/16/eraser.png", getClass()));
		buttonEraseMin.setToolTipText("reset min value");
		minIndexSpinner = new Spinner(grpMinValue, SWT.NONE);
		minIndexSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		minIndexSpinner.addListener(SWT.Selection, event -> setMinPlayerSelection(minIndexSpinner.getSelection()));

		// current index
		grpCurrentValue = new Group(this, SWT.NONE);
		updateGroupTitle(grpCurrentValue, player.getCurrent());
		grpCurrentValue.setLayout(new GridLayout(1, false));
		grpCurrentValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		currentIndexSpinner = new Spinner(grpCurrentValue, SWT.NONE);
		currentIndexSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));

		// use of reactiveRx publisher for current index spinner to handle correctly manual input
		PublishSubject<Integer> currentIndexSpinnerPublisher = PublishSubject.create();
		disposable.add(currentIndexSpinnerPublisher.debounce(800, TimeUnit.MILLISECONDS)
				.toFlowable(BackpressureStrategy.LATEST).subscribe(index -> {
					player.setPlaying(false);
					player.setReversePlaying(false);
					player.setCurrent(index);
				}));
		currentIndexSpinner.addListener(SWT.Selection,
				event -> currentIndexSpinnerPublisher.onNext(currentIndexSpinner.getSelection()));

		// max index
		grpMaxValue = new Group(this, SWT.NONE);
		grpMaxValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		updateGroupTitle(grpMaxValue, player.getData().maxLimit());
		grpMaxValue.setLayout(new GridLayout(2, false));
		Button buttonEraseMax = new Button(grpMaxValue, SWT.FLAT);
		buttonEraseMax.addListener(SWT.Selection, event -> {
			maxIndexSpinner.setSelection(player.getData().maxLimit());
			setMaxPlayerSelection(player.getData().maxLimit());
		});
		buttonEraseMax.setImage(ImageResources.getImage("icons/16/eraser.png", getClass()));
		buttonEraseMax.setToolTipText("reset max value");
		maxIndexSpinner = new Spinner(grpMaxValue, SWT.NONE);
		maxIndexSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		maxIndexSpinner.addListener(SWT.Selection, event -> setMaxPlayerSelection(maxIndexSpinner.getSelection()));

		// slider
		indexSlider = new PlayerSlider(this);
		indexSlider.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> {
			player.setReversePlaying(false);
			player.setCurrent(indexSlider.getSelection());
			event.doit = false;
		}));
		indexSlider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		Composite composite1 = new Composite(this, SWT.NONE);
		composite1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		GridLayout glComposite1 = new GridLayout(4, false);
		glComposite1.marginWidth = 0;
		glComposite1.marginHeight = 0;
		composite1.setLayout(glComposite1);

		// previous
		Button stepBackwardButton = new Button(composite1, SWT.NONE);
		stepBackwardButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		stepBackwardButton.setToolTipText("Previous");
		stepBackwardButton.setImage(ImageResources.getImage(BACKWARD_ICON, getClass()));

		// play / pause reverse
		reverseButton = new Button(composite1, SWT.TOGGLE);
		reverseButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		reverseButton.setToolTipText("Play reverse");
		reverseButton.setImage(ImageResources.getImage(BACK_ICON, getClass()));

		// play / pause
		playButton = new Button(composite1, SWT.TOGGLE);
		playButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		playButton.setToolTipText("Play/Pause");
		playButton.setImage(ImageResources.getImage(PLAY_ICON, getClass()));
		// next
		Button stepForwardButton = new Button(composite1, SWT.NONE);
		stepForwardButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		stepForwardButton.setToolTipText("Next");
		stepForwardButton.setImage(ImageResources.getImage(FORWARD_ICON, getClass()));
		stepForwardButton.addListener(SWT.Selection, event -> player.stepForward());
		playButton.addListener(SWT.Selection, event -> player.playPause());
		reverseButton.addListener(SWT.Selection, event -> player.revertPlayPause());
		stepBackwardButton.addListener(SWT.Selection, event -> player.stepBackward());

		Composite composite2 = new Composite(this, SWT.NONE);
		composite2.setLayout(new GridLayout(3, false));
		GridData gdComposite2 = new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1);
		gdComposite2.widthHint = 456;
		composite2.setLayoutData(gdComposite2);

		// repeat
		repeatButton = new Button(composite2, SWT.CHECK);
		repeatButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		repeatButton.setText("repeat");
		repeatButton.setToolTipText("Repeat");

		// increment
		Composite incrementComposite = new Composite(composite2, SWT.NONE);
		incrementComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		incrementComposite.setLayout(new GridLayout(2, false));
		Label lblNewLabel = new Label(incrementComposite, SWT.NONE);
		lblNewLabel.setText("increment");
		incrementSpinner = new Spinner(incrementComposite, SWT.BORDER);
		incrementSpinner.setMinimum(1);
		incrementSpinner.setSelection(1);

		// speed
		Composite composite = new Composite(composite2, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite.setLayout(new GridLayout(2, false));
		Label speedLabel = new Label(composite, SWT.NONE);
		speedLabel.setText("speed x");
		speedCombo = new Combo(composite, SWT.READ_ONLY);
		speedCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		speedCombo.setItems(SPEED_VALUES);
		speedCombo.select(3);
		speedCombo.addListener(SWT.Selection,
				event -> player.setPlayingSpeed(Float.parseFloat(SPEED_VALUES[speedCombo.getSelectionIndex()])));
		incrementSpinner.addListener(SWT.Selection, event -> player.setReadIncrement(incrementSpinner.getSelection()));
		repeatButton.addListener(SWT.Selection, event -> player.setLoop(repeatButton.getSelection()));
	}

	/** Updates group titles. Useful when LabelProvider changed on player */
	public void refreshGroupTitles() {
		UIUtils.asyncExecSafety(this, () -> {
			updateGroupTitle(grpMinValue, player.getData().minLimit());
			updateGroupTitle(grpCurrentValue, player.getCurrent());
			updateGroupTitle(grpMaxValue, player.getData().maxLimit());
		});
	}
}
