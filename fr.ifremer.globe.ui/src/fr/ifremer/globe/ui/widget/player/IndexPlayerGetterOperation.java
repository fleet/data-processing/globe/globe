/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.widget.player;

import java.util.Optional;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWwLayerOperation;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableMultiTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.IWWPlayableTextureLayer;

/**
 * IWWLayerOperation to grab the IndexedPlayer on any Layer
 */
public class IndexPlayerGetterOperation extends BasicWwLayerOperation {

	/** The resulting IndexedPlayer */
	protected Optional<IndexedPlayer> result = Optional.empty();

	/** {@inheritDoc} */
	@Override
	public IndexPlayerGetterOperation accept(IWWPlayableTextureLayer layer) {
		result = Optional.of(layer.getPlayer());
		return this;
	}

	/** {@inheritDoc} */
	@Override
	public IndexPlayerGetterOperation accept(IWWPlayableMultiTextureLayer layer) {
		result = Optional.of(layer.getPlayer());
		return this;
	}

	@Override
	public void performDefaultOperation(IWWLayer layer) {
		result = Optional.empty();
	}

	/**
	 * @return the {@link #result}
	 */
	public Optional<IndexedPlayer> getResult() {
		return result;
	}

}
