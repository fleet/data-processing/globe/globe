package fr.ifremer.globe.ui.widget.player;

import fr.ifremer.globe.utils.map.TypedEntry;
import fr.ifremer.globe.utils.map.TypedKey;

/**
 * Record for IndexPlayer data
 */
public record IndexedPlayerData(
		/** max possible value . */
		int maxLimit,
		/** Default min play number. */
		int minLimit,
		/** The current play index. */
		int currentValue,
		/** max play index, after this value player will either stop or loop. */
		int maxPlayIndex,
		/** max play index, after this value player will either stop or loop. */
		int minPlayIndex) {

	/** Key used to save this bean in session */
	public static final TypedKey<IndexedPlayerData> SESSION_KEY = new TypedKey<>("Player_Index");

	/**
	 * create a new {@link IndexedPlayerData} with a new currentValue
	 *
	 * @param currentValue the new current value
	 */
	public IndexedPlayerData withCurrentValue(int currentValue) {
		return new IndexedPlayerData(maxLimit, minLimit, currentValue, maxPlayIndex, minPlayIndex);
	}

	/**
	 * create a new {@link IndexedPlayerData} with a new minPlayIndex
	 */
	public IndexedPlayerData withMinPlayValue(int minPlayIndex) {
		return new IndexedPlayerData(maxLimit, minLimit, currentValue, maxPlayIndex, minPlayIndex);
	}

	/**
	 * create a new {@link IndexedPlayerData} with a new maxPlayIndex
	 */
	public IndexedPlayerData withMaxPlayValue(int maxPlayIndex) {
		return new IndexedPlayerData(maxLimit, minLimit, currentValue, maxPlayIndex, minPlayIndex);
	}

	/**
	 * create a new {@link IndexedPlayerData} with a new currentValue, minPlayIndex and maxPlayIndex
	 */
	public IndexedPlayerData fromOther(IndexedPlayerData other) {
		int newMinPlayIndex = Math.max(minLimit, other.minPlayIndex);
		int newMaxPlayIndex = Math.min(maxLimit, other.maxPlayIndex);
		int newCurrentValue = Math.max(newMinPlayIndex, Math.min(newMaxPlayIndex, other.currentValue));
		return new IndexedPlayerData(maxLimit, minLimit, newCurrentValue, newMaxPlayIndex, newMinPlayIndex);
	}

	@Override
	public String toString() {
		return "IndexedPlayerData [maxLimit=" + maxLimit + ", minLimit=" + minLimit + ", currentValue=" + currentValue
				+ ", maxPlayIndex=" + maxPlayIndex + ", minPlayIndex=" + minPlayIndex + "]";
	}

	/**
	 * Wrap this bean to a TypedEntry with SESSION_KEY as key
	 */
	public TypedEntry<IndexedPlayerData> toTypedEntry() {
		return SESSION_KEY.bindValue(this);
	}

}