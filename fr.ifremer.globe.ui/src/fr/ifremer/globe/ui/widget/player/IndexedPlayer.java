package fr.ifremer.globe.ui.widget.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.function.IntFunction;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

/**
 * Allow to set a timer to play on a regular basis among a set of continuous integer values This class will emulate
 * function of a player like looping, backward/forward, play rate,
 *
 */
public class IndexedPlayer {

	/** ID used to distinct multiple playerBeans */
	private String name;

	/** {@link IndexedPlayerStatus} & publisher **/
	private IndexedPlayerStatus status = new IndexedPlayerStatus(1.0f, false, false, false);
	private PublishSubject<IndexedPlayer> statusPublisher = PublishSubject.create();

	/** {@link IndexedPlayerData} & publisher **/
	private IndexedPlayerData data = new IndexedPlayerData(0, 0, -1, 0, 0);
	private PublishSubject<IndexedPlayerData> indexPublisher = PublishSubject.create();

	/** Default {@link Flowable} for {@link IndexedPlayerData} **/
	private static final int DEFAUT_SAMPLING_PERIOD = 10;
	private Flowable<IndexedPlayerData> flowable = indexPublisher.sample(DEFAUT_SAMPLING_PERIOD, TimeUnit.MILLISECONDS)
			.toFlowable(BackpressureStrategy.LATEST);

	/** data increment is the increment in data when going the next value (1 = we read next value) */
	private int readIncrement = 1;
	private int lastActualIncrement = 0;

	/** Timer used for autoplay mode */
	private static final int DEFAULT_TIMER_DELAY = 500;
	private Timer timer = null;

	/** Label provider by index **/
	private IntFunction<String> labelProvider = i -> "";

	private final List<Disposable> disposables = new ArrayList<>();

	/* Key used by the Synchronizer to identify the sync group of this player */
	private Optional<String> syncKey = Optional.empty();

	/**
	 * Constructor
	 */
	public IndexedPlayer(String name, int minValue, int maxValue) {
		this.name = name;
		data = new IndexedPlayerData(maxValue, minValue, minValue, maxValue, minValue);
		notifyIndex();
	}

	/**
	 * Disposes the {@link IndexedPlayer} subscribers (avoid memory leaks).
	 */
	public void dispose() {
		disposables.forEach(Disposable::dispose);
	}

	/**
	 * @return the status of this player
	 */
	public IndexedPlayerStatus getStatus() {
		return status;
	}

	/** getter for data increment is the increment in data when going the next value (1 = we read next value) */
	public int getReadIncrement() {
		return readIncrement;
	}

	/** setter for data increment is the increment in data when going the next value (1 = we read next value) */
	public void setReadIncrement(int readIncrement) {
		this.readIncrement = readIncrement;
	}

	/** getter for the last data increment actually applied (0 = index has been explicitly loaded) */
	public int getLastActualIncrement() {
		return lastActualIncrement;
	}

	public IntFunction<String> getLabelProvider() {
		return labelProvider;
	}

	public void setLabelProvider(IntFunction<String> labelProvider) {
		this.labelProvider = labelProvider;
	}

	/**
	 * Return the player data value
	 */
	public IndexedPlayerData getData() {
		return data;
	}

	/** Set all player data values at once in case of session restore for example */
	public void setData(IndexedPlayerData data) {
		if (!this.data.equals(data)) {
			if (this.data.currentValue() != data.currentValue()) {
				this.data = data;
				notifyIndex();
			} else {
				this.data = data;
			}
			notifyStatus();
		}
	}

	/**
	 * Default subscribtion for {@link IndexedPlayerData}.
	 */
	public Disposable subscribe(Consumer<? super IndexedPlayerData> onNext) {
		var disposable = flowable.subscribe(onNext);
		disposables.add(disposable);
		return disposable;
	}

	public PublishSubject<IndexedPlayerData> getIndexPublisher() {
		return indexPublisher;
	}

	public PublishSubject<IndexedPlayer> getStatusPublisher() {
		return statusPublisher;
	}

	private void notifyStatus() {
		statusPublisher.onNext(this);
	}

	public void notifyIndex() {
		indexPublisher.onNext(data);
	}

	public float getPlayingSpeed() {
		return status.getPlayingSpeed();
	}

	public void setPlayingSpeed(float playingSpeed) {
		if (status.getPlayingSpeed() != playingSpeed) {
			status = status.fromSpeed(playingSpeed);
			notifyStatus();
			if (timer != null)
				startTimer(); // update current speed if playing
		}
	}

	/**
	 * The reversePlaying attribute.
	 */
	public void setReversePlaying(boolean reversePlaying) {
		status = status.fromReversePlaying(reversePlaying);
		notifyStatus();
		if (reversePlaying) {
			startTimer();
		} else if (!status.playing) {
			stopTimer();
		}
	}

	/**
	 * True if this player is currently playing.
	 */
	public void setPlaying(boolean playing) {
		if (status.isPlaying() != playing) {
			status = status.fromPlaying(playing);
			notifyStatus();
			if (playing) {
				startTimer();
			} else {
				stopTimer();
			}
		}
	}

	private void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	private void jumpToNextPlayIndex() {
		int delta = getReadIncrement();
		int newIdx = data.currentValue();
		if (status.isReversePlaying()) {
			lastActualIncrement = -delta;
			newIdx -= delta;
		} else {
			lastActualIncrement = delta;
			newIdx += delta;
		}
		if (newIdx > data.maxPlayIndex()) {
			if (status.isLoop())
				newIdx = data.minPlayIndex();
			else {
				newIdx = data.maxPlayIndex();
				setPlaying(false);
			}
		} else if (newIdx < data.minPlayIndex()) {
			if (status.isLoop()) {
				newIdx = data.maxPlayIndex();
			} else {
				newIdx = data.minPlayIndex();
				setPlaying(false);
			}
		}
		updateCurrentValue(newIdx);
	}

	private void startTimer() {
		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer(name);
		long period = (long) (DEFAULT_TIMER_DELAY / status.getPlayingSpeed());

		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				jumpToNextPlayIndex();
			}
		}, 0, period);
	}

	/**
	 * True if this player should replay at first index after last index has been played (loop).
	 */
	public boolean getLoop() {
		return status.isLoop();
	}

	/**
	 * True if this player should replay at first index after last index has been played (loop).
	 */
	public void setLoop(boolean loop) {
		status = status.fromLoop(loop);
		notifyStatus();
	}

	public String getName() {
		return name;
	}

	/**
	 * The current play index.
	 */
	public int getCurrent() {
		return data.currentValue();
	}

	/**
	 * set the current value stop reading if it is running
	 */
	public void setCurrent(int value) {
		// public api to set the current value
		setPlaying(false);
		lastActualIncrement = 0;
		updateCurrentValue(value);
	}

	/** Set the flag no current value for the player */
	public void noCurrent() {
		setCurrent(-1);
	}

	/** @return true when the flag no current value for the player has been set */
	public boolean hasCurrent() {
		return getCurrent() != -1;
	}

	/**
	 * Set the current play index.
	 */
	private void updateCurrentValue(int playIndex) {
		if (data.currentValue() != playIndex) {
			data = data.withCurrentValue(playIndex);
			notifyIndex();
		}
	}

	/**
	 * The start play index.
	 */
	public int getMinPlayIndex() {
		return data.minPlayIndex();
	}

	/**
	 * The start play index.
	 */
	public void setMinPlayIndex(int minPlayIndex) {
		IndexedPlayerData oldData = data;
		if (minPlayIndex < getMinLimit() || minPlayIndex > getMaxPlayIndex()) {
			data = data.withMinPlayValue(getMinLimit());
		} else {
			data = data.withMinPlayValue(minPlayIndex);
		}
		notifyStatus();
		if (!oldData.equals(data)) {
			notifyStatus();
		}
	}

	/**
	 * The last play index.
	 */
	public int getMaxPlayIndex() {
		return data.maxPlayIndex();
	}

	/**
	 * The last play index.
	 */
	public int getIndexCount() {
		return data.maxPlayIndex() - data.minPlayIndex();
	}

	/**
	 * set the last play index. if the value is above {@link #getMaxLimit()}, it will be truncated
	 */
	public void setMaxPlayIndex(int maxPlayIndex) {
		IndexedPlayerData oldData = data;
		if (maxPlayIndex > getMaxLimit() || maxPlayIndex < getMinPlayIndex()) {
			data = data.withMaxPlayValue(getMaxLimit());
		} else {
			data = data.withMaxPlayValue(maxPlayIndex);
		}
		if (!oldData.equals(data)) {
			notifyStatus();
		}
	}

	private int getMinLimit() {
		return data.minLimit();
	}

	private int getMaxLimit() {
		return data.maxLimit();
	}

	public void stepForward() {
		setPlaying(false);
		setReversePlaying(false);
		jumpToNextPlayIndex();
	}

	public void stepBackward() {
		setPlaying(false);
		// to go backward, the reverse play needs to be enabled...
		setReversePlaying(true);
		jumpToNextPlayIndex();
		// but the player is paused...
		setReversePlaying(false);
	}

	public void skipIndex() {
		if (status.isPlaying() || status.isReversePlaying()) {
			jumpToNextPlayIndex();
		} else if (lastActualIncrement == readIncrement) {
			stepForward();
		} else if (lastActualIncrement == -readIncrement) {
			stepBackward();
		}
	}

	public void playPause() {
		setPlaying(!status.playing);
		setReversePlaying(false);
	}

	public void revertPlayPause() {
		setPlaying(false);
		setReversePlaying(!status.reversePlaying);
	}

	/**
	 * @param status the {@link #status} to set
	 */
	public void syncWith(IndexedPlayerStatus status, IndexedPlayerData data) {
		this.status = status;
		if (!status.playing) {
			stopTimer();
			notifyStatus();
		}

		this.data = this.data.fromOther(data);
		notifyIndex();
	}

	/**
	 * @return the {@link #flowable}
	 */
	public Flowable<IndexedPlayerData> getFlowable() {
		return flowable;
	}

	/**
	 * @return the {@link #syncKey}
	 */
	public Optional<String> getSyncKey() {
		return syncKey;
	}

	/**
	 * @param syncKey the {@link #syncKey} to set
	 */
	public void setSyncKey(Optional<String> syncKey) {
		this.syncKey = syncKey;
	}

}
