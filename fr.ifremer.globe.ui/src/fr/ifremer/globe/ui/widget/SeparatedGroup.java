/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.widget;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.widgets.CompositeFactory;
import org.eclipse.jface.widgets.LabelFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

/**
 * Simple composite with a title and separator
 */
public class SeparatedGroup extends Composite {

	/** Wrapped composite */
	private final Composite innerComposite;

	/**
	 * Create the composite.
	 */
	public SeparatedGroup(Composite parent, int style, String title) {
		super(parent, style);
		GridLayoutFactory.fillDefaults().numColumns(2).applyTo(this);

		LabelFactory.newLabel(SWT.NONE)//
				.text(title)//
				.layoutData(GridDataFactory.swtDefaults().create()) //
				.create(this);
		LabelFactory.newLabel(SWT.SEPARATOR | SWT.HORIZONTAL)//
				.layoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(SWT.DEFAULT, 1)
						.grab(true, false).create())//
				.create(this);

		innerComposite = CompositeFactory.newComposite(SWT.NONE)//
				.layoutData(GridDataFactory.fillDefaults().grab(true, true).span(2, 1).create()) //
				.create(this);
	}

	/**
	 * Sets the layout data associated with this composite.
	 */
	public SeparatedGroup layoutData(Object layoutData) {
		setLayoutData(layoutData);
		return this;
	}

	/**
	 * @return the {@link #innerComposite}
	 */
	public Composite getInnerComposite() {
		return innerComposite;
	}

	/** {@inheritDoc} */
	@Override
	protected void checkSubclass() { // Disable the check that prevents subclassing of SWT components
	}

}
