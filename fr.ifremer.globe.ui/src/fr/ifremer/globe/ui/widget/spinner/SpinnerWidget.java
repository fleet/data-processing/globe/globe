/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.widget.spinner;

import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;

/**
 * Widget to edit a Number in a spinner
 */
public class SpinnerWidget extends Composite {

	public static enum SpinnerWidgetLayout {
		HORIZONTAL, VERTICAL
	}

	/** Widgets */
	protected Button btnEnable;
	protected Spinner spinner;
	private Label lblSpinner;

	/**
	 * {@link PublisherSubject} for event in this composite
	 */
	private PublishSubject<EnabledNumberModel> publisher = PublishSubject.create();
	private Flowable<EnabledNumberModel> modelFlow = publisher.toFlowable(BackpressureStrategy.LATEST);
	private CompositeDisposable disposables = new CompositeDisposable();
	private EnabledNumberModel model;

	/**
	 * Constructor
	 *
	 * @wbp.parser.constructor
	 */
	protected SpinnerWidget(Composite parent) {
		this("title", "spinner", parent, new EnabledNumberModel(true, 12.456f), 2, SpinnerWidgetLayout.VERTICAL);
	}

	/**
	 * Constructor without spinnerLabel
	 */
	public SpinnerWidget(String title, Composite parent, EnabledNumberModel initialModel, int digits) {
		this(title, "", parent, initialModel, digits, SpinnerWidgetLayout.VERTICAL);
	}

	/**
	 * Constructor without spinnerLabel
	 */
	public SpinnerWidget(String title, Composite parent, EnabledNumberModel initialModel, int digits,
			SpinnerWidgetLayout layout) {
		this(title, "", parent, initialModel, digits, layout);
	}

	/**
	 * Constructor
	 */
	public SpinnerWidget(String checkBoxLabel, String spinnerLabel, Composite parent, EnabledNumberModel initialModel,
			int digits, SpinnerWidgetLayout layout) {
		super(parent, SWT.NONE);

		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		btnEnable = new Button(this, SWT.CHECK);
		btnEnable.addListener(SWT.Selection, event -> onEnabledChanged());

		Label lblTitleLabel = new Label(this, SWT.NONE);
		lblTitleLabel.setText(checkBoxLabel);
		lblTitleLabel.addMouseListener(MouseListener.mouseUpAdapter(e -> onLabelClicked()));

		if (layout == SpinnerWidgetLayout.VERTICAL) {
			Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		}

		Composite cmpSpinner = new Composite(this, SWT.NONE);
		RowLayout rlSpinner = new RowLayout(SWT.HORIZONTAL);
		rlSpinner.wrap = false;
		rlSpinner.center = true;
		rlSpinner.spacing = 10;
		cmpSpinner.setLayout(rlSpinner);
		cmpSpinner.setLayoutData(
				new GridData(SWT.FILL, SWT.FILL, false, false, layout == SpinnerWidgetLayout.HORIZONTAL ? 1 : 3, 1));

		if (spinnerLabel != null && !spinnerLabel.isEmpty()) {
			lblSpinner = new Label(cmpSpinner, SWT.NONE);
			lblSpinner.setText(spinnerLabel);
		}
		spinner = new Spinner(cmpSpinner, SWT.BORDER);
		spinner.setLayoutData(new RowData(70, SWT.DEFAULT));
		spinner.setMinimum(Integer.MIN_VALUE);
		spinner.setMaximum(Integer.MAX_VALUE);
		spinner.setDigits(digits);
		spinner.setIncrement((int) Math.pow(10, digits));
		spinner.addModifyListener(event -> publishModel());

		setModel(initialModel);

		addDisposeListener(e -> onDispose());
	}

	/** Sets the minimum and maximum values that the spinner will allow */
	public void setMinMaxValues(Number min, Number max) {
		double scale = Math.pow(10, spinner.getDigits());
		spinner.setMinimum((int) Math.round(min.doubleValue() * scale));
		spinner.setMaximum((int) Math.round(max.doubleValue() * scale));
	}

	/**
	 * Create and publish a model based on the edition
	 */
	private void publishModel() {
		double scale = Math.pow(10, spinner.getDigits());
		model = new EnabledNumberModel(btnEnable.getSelection(), spinner.getSelection() / scale);
		publisher.onNext(new EnabledNumberModel(btnEnable.getSelection(), spinner.getSelection() / scale));
	}

	/**
	 * Update the view with the model
	 */
	public void setModel(EnabledNumberModel inputModel) {
		model = inputModel;
		btnEnable.setSelection(model.enable);
		spinner.setEnabled(model.enable);
		double scale = Math.pow(10, spinner.getDigits());
		spinner.setSelection((int) Math.round(model.value.doubleValue() * scale));
	}

	/** Click on enabled button */
	protected void onEnabledChanged() {
		spinner.setEnabled(btnEnable.getSelection());
		publishModel();
	}

	/** Click on label samo than enabled button */
	protected void onLabelClicked() {
		btnEnable.setSelection(!btnEnable.getSelection());
		onEnabledChanged();
	}

	/**
	 * Return a Flowable to receive the NumberModel
	 */
	public void subscribe(Consumer<EnabledNumberModel> consumer) {
		disposables.add(modelFlow.subscribe(consumer::accept));
	}

	/** Widget is disposed */
	private void onDispose() {
		disposables.dispose();
	}

	/**
	 * @return the {@link #btnEnable}
	 */
	public Button getBtnEnable() {
		return btnEnable;
	}

	/**
	 * @return the {@link #spinner}
	 */
	public Spinner getSpinner() {
		return spinner;
	}

	public EnabledNumberModel getModel() {
		return model;
	}

}
