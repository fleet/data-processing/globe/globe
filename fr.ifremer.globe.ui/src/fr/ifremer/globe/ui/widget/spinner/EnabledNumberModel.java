/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.widget.spinner;

/**
 * Data model for {@link SpinnerWidget}
 */
public class EnabledNumberModel {

	/** Properties **/
	public final boolean enable;
	public final Number value;

	/**
	 * Constructor
	 */
	public EnabledNumberModel(boolean enable, Number value) {
		this.enable = enable;
		this.value = value;
	}

}
