package fr.ifremer.globe.ui.widget.jfreechart.events;

import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.EventObject;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;

/**
 * A selection event for a chart that is displayed in a {@link ChartPanel}.
 * 
 * @see ChartSelectionListener
 */
public class ChartSelectionEvent extends EventObject implements Serializable {

	/** For serialization. */
	private static final long serialVersionUID = -1;

	/** The chart that the mouse event relates to. */
	private JFreeChart chart;

	/** The Java mouse event that triggered this event. */
	private MouseEvent trigger;

	private Rectangle2D.Double selectedArea;

	private XYPlot subplot;

	private int rendererIndex;

	/**
	 * Constructs a new event.
	 * 
	 * @param chart
	 *            the source chart (<code>null</code> not permitted).
	 * @param trigger
	 *            the mouse event that triggered this event (<code>null</code>
	 *            not permitted).
	 * 
	 */
	public ChartSelectionEvent(JFreeChart chart, MouseEvent trigger, Rectangle2D.Double selRectangle2d, XYPlot subplot, int rendererIndex) {
		super(chart);
		this.chart = chart;
		this.trigger = trigger;
		this.selectedArea = selRectangle2d;
		this.subplot = subplot;
		this.rendererIndex = rendererIndex;
	}

	/**
	 * Returns the chart that the mouse event relates to.
	 * 
	 * @return The chart (never <code>null</code>).
	 */
	public JFreeChart getChart() {
		return this.chart;
	}

	/**
	 * Returns the mouse event that triggered this event.
	 * 
	 * @return The event (never <code>null</code>).
	 */
	public MouseEvent getTrigger() {
		return this.trigger;
	}

	/**
	 * Returns the selected Area in dataset coordinates.
	 * 
	 * @return The selected Area
	 */
	public Rectangle2D.Double getSelectedArea() {
		return this.selectedArea;
	}

	/**
	 * Returns the selected plot or subplot
	 * 
	 * @return The subplot
	 */
	public XYPlot getXYPlot() {
		return subplot;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ":" + getXYPlot().toString() + " " + getSelectedArea().toString();
	}

	/**
	 * @return the rendererIndex
	 */
	public int getRendererIndex() {
		return rendererIndex;
	}
}
