package fr.ifremer.globe.ui.widget.jfreechart;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.EventListener;

import javax.swing.event.EventListenerList;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;

import fr.ifremer.globe.ui.widget.jfreechart.events.ChartSelectionEvent;
import fr.ifremer.globe.ui.widget.jfreechart.events.ChartSelectionListener;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Extends chart panel to add a selection capacity * 
 **/
@SuppressWarnings("serial")
public class SelectChartPanel extends ChartPanel {
	/**
	 * The select rectangle starting point (selected by the user with a mouse
	 * click). This is a point on the screen, not the chart (which may have been
	 * scaled up or down to fit the panel).
	 */
	private Point2D selectionStartingPoint;

	/** The select rectangle (selected by the user with the mouse). */
	private Rectangle2D selectRectangle;
	/** True if selection is done after a edition point action (move, remove or validate). */
	private boolean selectionForPointEdition;

	/**
	 * Flag indication that the selection interactor is enabled
	 * */
	private boolean selectable = false;

	/** Storage for registered (chart) selection listeners. */
	private transient EventListenerList chartSelectionListeners;

	public SelectChartPanel(JFreeChart chart, boolean selectionEnabled) {
		super(chart);
		setMinimumDrawWidth(0);
		setMinimumDrawHeight(0);
		setFillZoomRectangle(false);
		setMouseWheelEnabled(true);
		setMouseZoomable(true);
		setPopupMenu(null);
		this.setSelectable(selectionEnabled);

		this.chartSelectionListeners = new EventListenerList();
		this.selectionStartingPoint = null;
		this.selectRectangle = null;
		this.selectionForPointEdition = false;
	}

	/**
	 * Flag indicating that the selectable interactor is enabled
	 * */
	public boolean isSelectable() {
		return selectable;
	}

	/**
	 * Flag setting selectable interactor capacity
	 * */
	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
		if (!selectable) {
			resetSelection();
		}
	}

	/**
	 * Start selection mode. Will disable zoom mode
	 * */
	public void startSelection(MouseEvent e) {

		setMouseZoomable(false, getFillZoomRectangle());
		if (this.selectRectangle == null) {
			this.selectRectangle = null;
		}
		Rectangle2D screenDataArea = getScreenDataArea(e.getX(), e.getY());
		if (screenDataArea != null) {
			this.selectionStartingPoint = getPointInRectangle(e.getX(), e.getY(), screenDataArea);
		} else {
			resetSelection();
		}
	}

	protected void clearSelectionRectangle() {
		Graphics2D g2 = (Graphics2D) getGraphics();
		repaint();
		g2.dispose();
	}

	/**
	 * Update the selection rectangle
	 * */
	public void updateSelection(MouseEvent e) {

		Rectangle2D scaledDataArea = getScreenDataArea((int) this.selectionStartingPoint.getX(), (int) this.selectionStartingPoint.getY());

		// selected rectangle shouldn't extend outside the data area...
		double xmax = Math.min(e.getX(), scaledDataArea.getMaxX());
		double ymax = Math.min(e.getY(), scaledDataArea.getMaxY());

		double x = this.selectionStartingPoint.getX();
		double y = this.selectionStartingPoint.getY();
		double w;
		double h;

		if(this.selectionStartingPoint.getX() > 0) {
			w = xmax - this.selectionStartingPoint.getX();
		} else {
			w = xmax + this.selectionStartingPoint.getX();
		}
		if(this.selectionStartingPoint.getY() > 0) {
			h = ymax - this.selectionStartingPoint.getY();
		} else {
			h = ymax + this.selectionStartingPoint.getY();
		}

		if(this.selectionForPointEdition) {
			this.selectRectangle = new Rectangle2D.Double(x, y, w, h);
		}

		repaint();
	}

	/**
	 * reset selection mode will enable zoom mode and reset selection data
	 * */
	public void resetSelection() {
		selectionStartingPoint = null;
		selectRectangle = null;
		setMouseZoomable(true, getFillZoomRectangle());
	}

	/**
	 * Handles a 'mouse dragged' event.
	 * 
	 * @param e
	 *            the mouse event.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		// if the popup menu has already been triggered, then ignore dragging...
		if (this.getPopupMenu() != null && this.getPopupMenu().isShowing()) {
			return;
		}
		// selection is disable, call standard behavior

		if (selectable == false) {
			super.mouseDragged(e);
			return;
		}
		// / call super treatment if we are not in a select mode
		// if no initial selection point was set, ignore dragging...
		if (this.selectionStartingPoint == null) {
			// first check if we are in a selection mode
			if (this.selectionForPointEdition) {
				// act if mouse was pressed
				startSelection(e);
				updateSelection(e);
			} else {
				super.mouseDragged(e);
			}
		} else {
			// selection was already started, update the selection rectangle
			updateSelection(e);

		}

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// selection is disable, call standard behavior
		if (this.selectable == false) {
			super.mousePressed(e);
			return;
		}
		boolean domainZoomFlag = isDomainZoomable();
		boolean rangeZoomFlag = isRangeZoomable();

		// check if we are in selection mode
		if (this.selectionForPointEdition) {
			startSelection(e);
		} else {
			resetSelection();
			// call mouse pressed
			super.mousePressed(e);
		}
		setDomainZoomable(domainZoomFlag);
		setRangeZoomable(rangeZoomFlag);
	}

	/**
	 * Handles a 'mouse released' event.
	 * 
	 * @param e
	 *            information about the event.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {

		super.mouseReleased(e);

		if (selectionStartingPoint == null) {
			startSelection(e);
			selectionStartingPoint.setLocation(selectionStartingPoint.getX(), selectionStartingPoint.getY());
		}

		if(this.selectRectangle != null) {
			Rectangle2D selectArea = relativeToAbsoluteRectangle();
			/** Move to dataset space coordinate */
			PlotRenderingInfo plotInfo = getChartRenderingInfo().getPlotInfo();
			PlotRenderingInfo info = plotInfo;
			XYPlot xyplot = getChart().getXYPlot();
			if (plotInfo.getSubplotCount() != 0) {
				Plot plot = getChart().getPlot();
				if (plot instanceof CombinedDomainXYPlot) {
					CombinedDomainXYPlot combinedPlot = (CombinedDomainXYPlot) getChart().getPlot();

					// get the origin of the zoom selection in the Java2D
					// space
					// used for
					// drawing the chart (that is, before any scaling to fit
					// the
					// panel)
					Point2D selectOrigin = translateScreenToJava2D(new Point((int) selectArea.getX(), (int) selectArea.getY()));
					int subplotIndex = plotInfo.getSubplotIndex(selectOrigin);
					if (subplotIndex == -1) {
						return;
					}

					xyplot = (XYPlot) combinedPlot.getSubplots().get(subplotIndex);
					info = plotInfo.getSubplotInfo(subplotIndex);

				}
			} else {
				xyplot = getChart().getXYPlot();
				info = plotInfo;
			}

			for (int i = 0; i < xyplot.getRangeAxisCount(); i++) {
				if (xyplot.getRangeAxis(i).isVisible()) {
					Point2D.Double upperLeft = getDatasetCoord(new Point((int) selectArea.getMinX(), (int) selectArea.getMinY()), xyplot, info, i);
					Point2D.Double lowerRight = getDatasetCoord(new Point((int) selectArea.getMaxX(), (int) selectArea.getMaxY()), xyplot, info, i);

					Rectangle2D.Double rec = new Rectangle2D.Double(upperLeft.getX(), lowerRight.getY(), lowerRight.getX() - upperLeft.getX(), upperLeft.getY() - lowerRight.getY());

					ChartSelectionEvent chartEvent = new ChartSelectionEvent(getChart(), e, rec, xyplot, i);
					try {
						notifySelectionEvent(chartEvent);
					} catch (GIOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			clearSelectionRectangle();
			consumeSelectionEvents();

			this.selectionForPointEdition = false;
		}
		resetSelection();
	}

	private void notifySelectionEvent(ChartSelectionEvent chartEvent) throws GIOException {
		System.out.println("Notify selection on " + chartEvent.toString());
		// notify the listeners
		Object[] listeners = this.chartSelectionListeners.getListeners(ChartSelectionListener.class);

		if (listeners.length != 0) {
			for (int i = listeners.length - 1; i >= 0; i -= 1) {
				((ChartSelectionListener) listeners[i]).chartAreaSelected(chartEvent);
			}
		}
	}

	private void consumeSelectionEvents() {
		Object[] listeners = this.chartSelectionListeners.getListeners(ChartSelectionListener.class);

		if (listeners.length != 0) {
			for (int i = listeners.length - 1; i >= 0; i -= 1) {
				removeChartSelectionListener((ChartSelectionListener) listeners[i]);
			}
		}
	}

	public Point2D.Double getDatasetCoord(Point point, XYPlot subPlot, PlotRenderingInfo subplotInfo, int axisnumber) {

		Point2D point2d = translateScreenToJava2D(point);
		java.awt.geom.Rectangle2D rectangle2d = subplotInfo.getDataArea();
		ValueAxis valueaxis = subPlot.getDomainAxis();
		org.jfree.ui.RectangleEdge rectangleedgeDomain = subPlot.getDomainAxisEdge();
		ValueAxis valueaxis1 = subPlot.getRangeAxis(axisnumber);
		org.jfree.ui.RectangleEdge rectangleedgeRange = subPlot.getRangeAxisEdge();
		double dX = valueaxis.java2DToValue(point2d.getX(), rectangle2d, rectangleedgeDomain);
		double dY = valueaxis1.java2DToValue(point2d.getY(), rectangle2d, rectangleedgeRange);
		return new Point2D.Double(dX, dY);
	}



	/**
	 * Convert a rectangle with relative height and width to an absolute rectangle.
	 * @return an absolute rectangle with a positive width and a positive height
	 */
	private Rectangle2D relativeToAbsoluteRectangle() {

		Rectangle2D rectangle2d = new Rectangle2D.Double();

		if (this.selectRectangle.getWidth() > 0 && this.selectRectangle.getHeight() > 0) {
			rectangle2d = this.selectRectangle;
		} else if (this.selectRectangle.getWidth() > 0 && this.selectRectangle.getHeight() < 0) {
			rectangle2d.setRect(this.selectRectangle.getX(), Math.min(this.selectRectangle.getY(), this.selectRectangle.getY() + this.selectRectangle.getHeight()), this.selectRectangle.getWidth(), Math.abs(this.selectRectangle.getY() - this.selectRectangle.getY() + this.selectRectangle.getHeight()));
		} else if (this.selectRectangle.getWidth() < 0 && this.selectRectangle.getHeight() > 0) {
			rectangle2d.setRect(Math.min(this.selectRectangle.getX(), this.selectRectangle.getX() + this.selectRectangle.getWidth()), this.selectRectangle.getY(), Math.abs(this.selectRectangle.getX() - this.selectRectangle.getX() + this.selectRectangle.getWidth()), Math.abs(this.selectRectangle.getY() - this.selectRectangle.getY() + this.selectRectangle.getHeight()));
		} else {
			rectangle2d.setRect(Math.min(this.selectRectangle.getX(), this.selectRectangle.getX() + this.selectRectangle.getWidth()), Math.min(this.selectRectangle.getY(), this.selectRectangle.getY() + this.selectRectangle.getHeight()), Math.abs(this.selectRectangle.getX() - this.selectRectangle.getX() + this.selectRectangle.getWidth()), Math.abs(this.selectRectangle.getY() - this.selectRectangle.getY() + this.selectRectangle.getHeight()));
		}

		return rectangle2d;
	}

	/**
	 * Draws selection rectangle (if present). The drawing is performed in XOR
	 * mode, therefore when this method is called twice in a row, the second
	 * call will completely restore the state of the canvas.
	 * 
	 * @param g2
	 *            the graphics device.
	 * @param xor
	 *            use XOR for drawing?
	 */
	private void drawSelectionRectangle(Graphics2D g2, boolean xor) {
		if (this.selectRectangle != null) {
			if (xor) {
				// Set XOR mode to draw the zoom rectangle
				g2.setXORMode(Color.gray);
			}
			g2.setPaint(new Color(0, 125, 125, 63));			
			g2.fill(relativeToAbsoluteRectangle());
			if (xor) {
				// Reset to the default 'overwrite' mode
				g2.setPaintMode();
			}
		}
	}

	/**
	 * Returns a point based on (x, y) but constrained to be within the bounds
	 * of the given rectangle. This method could be moved to JCommon.
	 * 
	 * @param x
	 *            the x-coordinate.
	 * @param y
	 *            the y-coordinate.
	 * @param area
	 *            the rectangle (<code>null</code> not permitted).
	 * 
	 * @return A point within the rectangle.
	 */
	private Point2D getPointInRectangle(int x, int y, Rectangle2D area) {
		double xx = Math.max(area.getMinX(), Math.min(x, area.getMaxX()));
		double yy = Math.max(area.getMinY(), Math.min(y, area.getMaxY()));
		return new Point2D.Double(xx, yy);
	}

	/**
	 * Paints the component by drawing the chart to fill the entire component,
	 * but allowing for the insets (which will be non-zero if a border has been
	 * set for this component). To increase performance (at the expense of
	 * memory), an off-screen buffer image can be used.
	 * 
	 * @param g
	 *            the graphics device for drawing on.
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (this.getChart() == null) {
			return;
		}
		Graphics2D g2 = (Graphics2D) g.create();
		drawSelectionRectangle(g2, false);
		g2.dispose();

	}

	/**
	 * Adds a listener to the list of objects listening for chart selection.
	 * 
	 * @param listener
	 *            the listener (<code>null</code> not permitted).
	 */
	public void addChartSelectionListener(ChartSelectionListener listener) {
		if (listener == null) {
			throw new IllegalArgumentException("Null 'listener' argument.");
		}
		this.chartSelectionListeners.add(ChartSelectionListener.class, listener);
	}

	/**
	 * Removes a listener from the list of objects listening for chart mouse
	 * events.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public void removeChartSelectionListener(ChartSelectionListener listener) {
		this.chartSelectionListeners.remove(ChartSelectionListener.class, listener);
	}

	/**
	 * Returns an array of the listeners of the given type registered with the
	 * panel.
	 * 
	 * @param listenerType
	 *            the listener type.
	 * 
	 * @return An array of listeners.
	 */
	@Override
	public EventListener[] getListeners(@SuppressWarnings("rawtypes") Class listenerType) {
		if (listenerType == ChartSelectionListener.class) {
			// fetch listeners from local storage
			return this.chartSelectionListeners.getListeners(ChartSelectionListener.class);
		} else {
			return super.getListeners(listenerType);
		}
	}

	/**
	 * @param selectionForPointEdition the selectionForPointEdition to set
	 */
	public void setSelectionForPointEdition(boolean selectionForPointEdition) {
		this.selectionForPointEdition = selectionForPointEdition;
	}
}
