package fr.ifremer.globe.ui.widget.jfreechart.events;

import java.util.EventListener;

import org.jfree.chart.ChartPanel;

import fr.ifremer.globe.ui.widget.jfreechart.SelectChartPanel;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * The interface that must be implemented by classes that wish to receive
 * {@link ChartSelectionEvent} notifications from a {@link SelectChartPanel}.
 * 
 * @see ChartPanel#addChartSelectionListener(ChartSelectionListener)
 */
public interface ChartSelectionListener extends EventListener {

	/**
	 * Callback method for receiving notification of a mouse selection on a
	 * chart.
	 * 
	 * @param event
	 *            information about the event.
	 * @throws GIOException 
	 */
	void chartAreaSelected(ChartSelectionEvent event) throws GIOException;

}
