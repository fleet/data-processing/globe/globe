package fr.ifremer.globe.ui.widget.jfreechart;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;

import fr.ifremer.globe.ui.widget.jfreechart.events.ChartSelectionEvent;
import fr.ifremer.globe.ui.widget.jfreechart.events.ChartSelectionListener;

/**
 * Class implementing basic behaviour for an interactor on a
 * {@link SelectChartPanel}
 * */
public abstract class AbstractInteractor implements ChartSelectionListener, ChartMouseListener {
	protected SelectChartPanel chartPanel;

	/**
	 * Constructor
	 * */
	public AbstractInteractor(SelectChartPanel chartPanel) {
		this.chartPanel = chartPanel;
		chartPanel.addChartMouseListener(this);
		chartPanel.addChartSelectionListener(this);
	}

	/**
	 * remove listener and reset cursor to default cursor
	 * */
	public void dispose() {
		chartPanel.removeChartMouseListener(this);
		chartPanel.removeChartSelectionListener(this);
		changeCursor(Cursor.DEFAULT_CURSOR);
	}

	/**
	 * Change cursor to the given type. Type is extracted from {@link Cursor
	 * constant field values}
	 * */
	public void changeCursor(int cursor) {
		Cursor lightPenCursor = new Cursor(cursor);
		chartPanel.setCursor(lightPenCursor);
	}

	/**
	 * change the cursor to the given one
	 * */
	public void changeCursor(Cursor cursor) {
		chartPanel.setCursor(cursor);
	}

	/**
	 * convert event coordinate to data space coordinate
	 * */
	public Point2D.Double getDatasetCoord(ChartMouseEvent event) {
		int i = event.getTrigger().getX();
		int j = event.getTrigger().getY();
		return event2Data(i, j);
	}

	/**
	 * return the associated Plot or Subplot
	 * */
	public abstract XYPlot getXYPlot();

	/**
	 * return the dataArea associated with the plot or subplot
	 * */
	public abstract Rectangle2D getDataArea();

	protected Point2D.Double event2Data(MouseEvent e) {
		return event2Data(e.getX(), e.getY());
	}

	/**
	 * convert coordinate from event (ie java) space to dataspace
	 * */
	protected Point2D.Double event2Data(int i, int j) {
		XYPlot subPlot = getXYPlot();
		Rectangle2D rectangle2d = getDataArea();
		Point2D point2d = chartPanel.translateScreenToJava2D(new Point(i, j));
		ValueAxis valueaxis = subPlot.getDomainAxis();
		org.jfree.ui.RectangleEdge rectangleedgeDomain = subPlot.getDomainAxisEdge();
		ValueAxis valueaxis1 = subPlot.getRangeAxis();
		org.jfree.ui.RectangleEdge rectangleedgeRange = subPlot.getRangeAxisEdge();
		double dX = valueaxis.java2DToValue(point2d.getX(), rectangle2d, rectangleedgeDomain);
		double dY = valueaxis1.java2DToValue(point2d.getY(), rectangle2d, rectangleedgeRange);
		return new Point2D.Double(dX, dY);
	}

	public void chartAreaSelected(ChartSelectionEvent event, boolean displayMovePointDialog) {
		// TODO Auto-generated method stub
	}
}
