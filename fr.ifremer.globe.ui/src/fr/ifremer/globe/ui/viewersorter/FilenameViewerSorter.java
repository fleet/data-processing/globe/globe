/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.viewersorter;

import java.io.File;
import java.text.Collator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

/**
 * A viewer sorter  to reorder the Files provided by its content provider.
 */
public class FilenameViewerSorter extends ViewerSorter {

	/**
	 * Constructor
	 */
	public FilenameViewerSorter() {
	}

	/**
	 * Constructor
	 */
	public FilenameViewerSorter(Collator collator) {
		super(collator);

	}

	/**
	 * @see org.eclipse.jface.viewers.ViewerComparator#compare(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		return ((File)e1).getName().compareToIgnoreCase(((File)e2).getName());
	}

}
