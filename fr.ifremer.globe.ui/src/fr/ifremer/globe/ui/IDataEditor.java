package fr.ifremer.globe.ui;

import java.util.List;

import jakarta.annotation.PreDestroy;

public interface IDataEditor {

	public abstract List<String> getFileNameList();

	/**
	 * Calls if app or plugin is closed.
	 */
	@PreDestroy
	public abstract void onClose();

}