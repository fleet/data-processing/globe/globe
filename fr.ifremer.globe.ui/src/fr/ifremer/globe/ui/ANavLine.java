/*
 * @License@
 */
package fr.ifremer.globe.ui;

import java.util.ArrayList;
import java.util.List;

import gov.nasa.worldwind.geom.Position;

/**
 * Defines a navigation "line". It contains latitude, longitude and time stamp
 * for each point.
 *
 */
public abstract class ANavLine<T extends Position> {

	/**
	 * Default constructor.
	 */
	public ANavLine() {
		super();
		this.points = new ArrayList<T>();
	}

	/**
	 * For Serializable and Externalizable classes, the readResolve method
	 * allows a class to replace/resolve the object read from the stream before
	 * it is returned to the caller. By implementing the readResolve method, a
	 * class can directly control the types and instances of its own instances
	 * being deserialized.
	 *
	 *
	 */
	private Object readResolve() {
		return this;
	}

	/**
	 * The name attribute.
	 *
	 *
	 */
	private String _name = "";

	/**
	 * The points attribute.
	 *
	 *
	 */
	protected List<T> points;



	private String _collada;

	/**
	 * The name attribute.
	 *
	 *
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * * The name attribute.
	 *
	 *
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * Add a point to the points collection.
	 *
	 * @param pointsElt
	 *            Element to add
	 *
	 */
	protected void addPoint(T pointsElt) {
		this.points.add(pointsElt);
	}
	public T getFirstPoint() {
		return this.points.get(0);
	}
	public T getLastPoint() {
		return this.points.get(size()-1);
	}

	public int size()
	{
		return this.points.size();
	}


	public void setCollada(String collada) {
		this._collada = collada;

	}

	public String getCollada() {
		return this._collada;

	}

	public List<T> getPoints() {
		return this.points;
	}


}
