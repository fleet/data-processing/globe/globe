package fr.ifremer.globe.ui.projection;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.gdal.osr.SpatialReference;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Datum;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.utils.mbes.Ellipsoid;

public class ProjectionDialog extends Composite {

	public static final String UTM_ZONE_PARAM = "+zone";
	public static final String UTM_SOUTH_FLAG = "+south";

	/** Available projections */
	private static final ProjectionSettings[] AVAILABLE_PROJECTIONS = { StandardProjection.MERCATOR_PATTERN,
			StandardProjection.UTM_PATTERN, StandardProjection.LONGLAT, StandardProjection.EQUIDISTANT,
			StandardProjection.LAMBERT_EURO, StandardProjection.LAMBERT_PARIS, StandardProjection.LAMBERT_93 };

	/** Available ellipsoids */
	private static final Ellipsoid[] AVAILABLE_ELLIPSOIDS = new Ellipsoid[] { Ellipsoid.WGS84, Ellipsoid.WGS72,
			Ellipsoid.HAYFORD, Ellipsoid.GRS80 };

	/** Available datum */
	private static final Datum[] AVAILABLE_DATUMS = new Datum[] { Datum.WGS84, Datum.NAD83 };

	private final Table parametersTable;
	private final Combo projectionCombo;

	/** Last LonLat geobox set */
	private GeoBox geoBox = null;

	/** Map holding parameter/value entries of the selected projection */
	private final Map<String, String> proj4Map = new LinkedHashMap<>();

	/** Map holding parameters for UTM projections */
	private final Map<String, String> utmParamsMap = new LinkedHashMap<>();

	/** Map holding parameters for Mercator projection */
	private final Map<String, String> mercatorParamsMap = new LinkedHashMap<>();

	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	List<String> projectionList = new ArrayList<>();

	public ProjectionDialog(Composite parent) {
		super(parent, SWT.NONE);

		Composite composite = this;
		GridLayout glComposite = new GridLayout(3, false);
		glComposite.marginHeight = 10;
		glComposite.marginWidth = 10;
		glComposite.verticalSpacing = 8;
		glComposite.horizontalSpacing = 8;
		composite.setLayout(glComposite);

		// projections
		Label projectionLabel = new Label(composite, SWT.NONE);
		projectionLabel.setText("Projection :");
		projectionCombo = new Combo(composite, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
		GridData gdProjectionCombo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdProjectionCombo.widthHint = 224;
		projectionCombo.setLayoutData(gdProjectionCombo);

		for (ProjectionSettings projectionSettings : AVAILABLE_PROJECTIONS) {
			if (!projectionSettings.proj4String().isEmpty()) {
				projectionCombo.add(projectionSettings.name());
				projectionList.add(projectionSettings.name());
			}
		}

		projectionCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			int index = projectionCombo.getSelectionIndex();
			ProjectionSettings projectionSettings = findProjection(projectionCombo.getText());
			setProj4Chain(projectionSettings.proj4String());
			selectProjection(index);
		}));

		// EPSG code input
		Button epsgCodeButton = new Button(composite, SWT.NONE);
		epsgCodeButton.setText("Use EPSG code ...");
		epsgCodeButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		epsgCodeButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(e ->

		inputEPSGCode()));

		Label parametersLabel = new Label(composite, SWT.NONE);
		parametersLabel.setText("Parameters :");
		new Label(this, SWT.NONE);
		new Label(composite, SWT.NONE);

		parametersTable = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		parametersTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		parametersTable.setHeaderVisible(true);
		parametersTable.setLinesVisible(true);

		final TableEditor editor = new TableEditor(parametersTable);
		parametersTable.addListener(SWT.MouseDown, event -> {
			Point pt = new Point(event.x, event.y);
			int index = parametersTable.getTopIndex();
			while (index < parametersTable.getItemCount()) {
				final TableItem item = parametersTable.getItem(index);
				Rectangle rect0 = item.getBounds(0);
				Rectangle rect1 = item.getBounds(1);
				if (rect0.contains(pt)) {
					// edition of 1st column
					if (item.getText().equals("")) {
						createProj4ParameterComponent(editor, item, PROJ4_PARAMETERS);
						return;
					}
				} else if (rect1.contains(pt)) {
					// edition of 2nd column
					if (item.getText().equals("+ellps")) {
						createEllipsoidComboComponent(editor, item, 1, AVAILABLE_ELLIPSOIDS);
						return;
					} else if (item.getText().equals("+datum")) {
						createDatumComboComponent(editor, item, 1, AVAILABLE_DATUMS);
						return;
					} else if (!item.getText().isEmpty()) {
						createEditableTextComponent(editor, item, 1);
						return;
					}
				}

				index++;
			}
		});
		parametersTable.addKeyListener(KeyListener.keyReleasedAdapter(e -> {
			if (e.doit && e.keyCode == SWT.DEL) {
				int index = parametersTable.getSelectionIndex();
				if (index != -1) {
					parametersTable.remove(index);
					modifyProjection();
				}
			}
		}));
		String[] titles = new String[] { "Parameter", "Value", "Description" };
		for (String title : titles) {
			TableColumn column = new TableColumn(parametersTable, SWT.NONE);
			column.setText(title);
		}
		parametersTable.getColumn(0).setWidth(100);
		parametersTable.getColumn(1).setWidth(200);
		parametersTable.getColumn(2).setWidth(300);

		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;
	}

	private void inputEPSGCode() {
		final SpatialReference srs = new SpatialReference();
		InputDialog dialog = new InputDialog(getShell(), "Enter EPSG code", "EPSG Code", "", newText -> {
			String message = null;
			try {
				Integer.parseInt(newText);
				srs.ImportFromEPSG(Integer.valueOf(newText));
			} catch (NumberFormatException e1) {
				message = "Invalid code";
			} catch (RuntimeException e2) {
				message = "Unknown code";
			}
			return message;
		});
		int res = dialog.open();
		if (res == 0) {
			String codeStr = dialog.getValue();
			srs.ImportFromEPSG(Integer.valueOf(codeStr));
			String proj4String = srs.ExportToProj4();

			setProj4Chain(proj4String);
			setProjectionWidget(proj4String);
		}
	}

	public void setProjectionWidget(String proj4String) {
		int index = 0;
		String name = ProjectionSettings.getProjName(proj4String);
		index = projectionList.indexOf(name);
		selectProjection(index);
	}

	protected void modifyProjection() {

		proj4Map.clear();
		for (int index = 0; index < parametersTable.getItemCount(); index++) {
			final TableItem item = parametersTable.getItem(index);
			String parameter = item.getText(0);
			if (!parameter.isEmpty()) {
				String value = item.getText(1);
				proj4Map.put(parameter, value);
			}
		}
		setProj4Chain(getProj4Chain());
		pcs.firePropertyChange(Projection.PROJECTION_CHANGED, false, true);

		// Force focus to table, not the default button of the wizard...
		parametersTable.forceFocus();
	}

	protected void selectProjection(int index) {
		if (index >= 0) {
			projectionCombo.select(index);
			String friendlyName = projectionList.get(index);
			// Check if an error exists and try to fix it automatically
			if (validateProj4Chain() != null) {
				if (ProjectionSettings.UTM.equalsIgnoreCase(friendlyName)) {
					computeUTMZone();
					proj4Map.putAll(utmParamsMap);
				} else if (ProjectionSettings.MERCATOR.equalsIgnoreCase(friendlyName)) {
					computeMercatorOptions();
					proj4Map.putAll(mercatorParamsMap);
				}
			}
		} else {
			projectionCombo.clearSelection();
		}

		parametersTable.removeAll();
		for (Entry<String, String> entry : proj4Map.entrySet()) {
			if (entry.getKey().isEmpty()) {
				break;
			} else {
				TableItem item = new TableItem(parametersTable, SWT.NONE);
				item.setText(0, entry.getKey());
				item.setText(1, entry.getValue());
				item.setText(2, PROJ4_PARAMETERS.get(entry.getKey()));
			}
		}
		// last item is empty (used to add parameters)
		new TableItem(parametersTable, SWT.NONE);

		if (parametersTable.getItemCount() > 1) {
			pcs.firePropertyChange(Projection.PROJECTION_CHANGED, false, true);
		}
	}

	/**
	 * Analyse given string and put its parameters and value into the map
	 */
	private Map<String, String> setProj4Chain(String proj4Projection) {
		convertProj4StringToMap(proj4Projection, proj4Map);
		return proj4Map;
	}

	public void convertProj4StringToMap(String proj4Projection, Map<String, String> proj4Map) {
		proj4Map.clear();
		String[] parametersAndValues = proj4Projection.split(" ");
		for (String parameters : parametersAndValues) {
			String[] parameterAndValue = parameters.split("=");
			String parameter = parameterAndValue[0];
			String value = "";
			if (parameterAndValue.length == 2) {
				value = parameterAndValue[1];
			}
			proj4Map.put(parameter, value);
		}
	}

	public String validateProj4Map() {
		if (proj4Map.containsKey(UTM_ZONE_PARAM)) {
			var zone = proj4Map.get(UTM_ZONE_PARAM);
			if (zone.isBlank() || NumberUtils.toInt(zone) <= 0 && NumberUtils.toInt(zone) > 60) {
				return "Bad value for +zone";
			}
		}
		if (proj4Map.containsKey(StandardProjection.MERCATOR_LAT)) {
			var lat = proj4Map.get(StandardProjection.MERCATOR_LAT);
			if (lat.isBlank() || NumberUtils.toFloat(lat) < -90f && NumberUtils.toFloat(lat) > 90f) {
				return "Bad value for +lat_ts";
			}
		}
		if (proj4Map.containsKey("+lon_0")) {
			var lon = proj4Map.get("+lon_0");
			if (lon.isBlank() || NumberUtils.toFloat(lon) < -190f && NumberUtils.toFloat(lon) > 180f) {
				return "Bad value for +lon_0";
			}
		}
		return null;
	}

	/**
	 * Build proj4 string from the map containing parameters and values
	 */
	private String getProj4Chain() {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, String> entry : proj4Map.entrySet()) {
			sb.append(entry.getKey());
			if (entry.getValue().length() > 0) {
				sb.append("=");
				sb.append(entry.getValue());
			}
			sb.append(" ");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	public void setProjectionSettings(ProjectionSettings projectionSettings) {
		if (projectionSettings != null) {
			int index = projectionList.indexOf(projectionSettings.name());
			setProj4Chain(projectionSettings.proj4String());
			selectProjection(index);
		} else {
			setProj4Chain("");
			selectProjection(-1);
		}
	}

	private void createEditableTextComponent(final TableEditor editor, final TableItem item, final int column) {
		final Text text = new Text(parametersTable, SWT.NONE);
		Listener listener = e -> {
			if (e.type == SWT.FocusOut) {
				item.setText(column, text.getText());
				text.dispose();
				modifyProjection();
			} else if (e.type == SWT.Traverse) {
				if (e.detail == SWT.TRAVERSE_RETURN) {
					item.setText(column, text.getText());
					modifyProjection();
				} else if (e.detail == SWT.TRAVERSE_ESCAPE) {
					text.dispose();
					e.doit = false;
				}
			}
		};
		text.addListener(SWT.FocusOut, listener);
		text.addListener(SWT.Traverse, listener);
		editor.setEditor(text, item, column);
		text.setText(item.getText(column));
		text.selectAll();
		text.setFocus();
	}

	private void createEllipsoidComboComponent(final TableEditor editor, final TableItem item, final int column,
			final Ellipsoid[] ellipsoids) {
		final Combo combo = new Combo(parametersTable, SWT.READ_ONLY);
		Ellipsoid e0 = null;
		for (Ellipsoid e : ellipsoids) {
			combo.add(e.getEllipsName());
			if (e0 == null && e.getProj4Name().equals(item.getText(1))) {
				e0 = e;
			}
		}
		if (e0 == null) {
			e0 = Ellipsoid.WGS84;
		}
		final Ellipsoid ellipsoid0 = e0;
		Listener listener = e -> {
			String proj4name = ellipsoids[combo.getSelectionIndex()].getProj4Name();
			if (e.type == SWT.FocusOut) {
				item.setText(column, proj4name);
				modifyProjection();
				combo.dispose();
			} else if (e.type == SWT.Traverse) {
				if (e.detail == SWT.TRAVERSE_RETURN) {
					item.setText(column, proj4name);
					modifyProjection();
					combo.dispose();
				} else if (e.detail == SWT.TRAVERSE_ESCAPE) {
					item.setText(column, ellipsoid0.getProj4Name());
					combo.dispose();
					e.doit = false;
				}
			}
		};
		combo.addListener(SWT.FocusOut, listener);
		combo.addListener(SWT.Traverse, listener);
		editor.setEditor(combo, item, column);
		combo.setText(e0.getEllipsName());
		combo.setFocus();
	}

	private void createDatumComboComponent(final TableEditor editor, final TableItem item, final int column,
			final Datum[] datums) {
		final Combo combo = new Combo(parametersTable, SWT.READ_ONLY);
		Datum d0 = null;
		for (Datum d : datums) {
			combo.add(d.toString());
			if (d0 == null && d.toString().equals(item.getText(1))) {
				d0 = d;
			}
		}
		if (d0 == null) {
			d0 = Datum.WGS84;
		}
		final Datum datum0 = d0;
		Listener listener = e -> {
			Datum datum = datums[combo.getSelectionIndex()];
			TableItem ellipsoidItem = findEllipsoidItem();
			if (e.type == SWT.FocusOut) {
				item.setText(column, datum.toString());
				if (ellipsoidItem != null) {
					ellipsoidItem.setText(column, datum.ellipsoid.getProj4Name());
				}
				modifyProjection();
				combo.dispose();
			} else if (e.type == SWT.Traverse) {
				if (e.detail == SWT.TRAVERSE_RETURN) {
					item.setText(column, datum.toString());
					if (ellipsoidItem != null) {
						ellipsoidItem.setText(column, datum.ellipsoid.getProj4Name());
					}
					modifyProjection();
					combo.dispose();
				} else if (e.detail == SWT.TRAVERSE_ESCAPE) {
					item.setText(column, datum0.toString());
					combo.dispose();
					e.doit = false;
				}
			}
		};
		combo.addListener(SWT.FocusOut, listener);
		combo.addListener(SWT.Traverse, listener);
		editor.setEditor(combo, item, column);
		combo.setText(d0.toString());
		combo.setFocus();
	}

	private void createProj4ParameterComponent(final TableEditor editor, final TableItem item,
			final Map<String, String> proj4Parameters) {
		final Combo combo = new Combo(parametersTable, SWT.READ_ONLY);
		for (String parameter : proj4Parameters.keySet()) {
			if (!proj4Map.containsKey(parameter)) {
				combo.add(parameter);
			}
		}
		Listener listener = e -> {
			String param = combo.getText();
			String description = proj4Parameters.get(combo.getText());
			if (!param.isEmpty()) {
				if (e.type == SWT.FocusOut) {
					item.setText(0, param);
					item.setText(2, description);
					modifyProjection();
					combo.dispose();
					// new empty item to add parameters
					new TableItem(parametersTable, SWT.NONE);
				} else if (e.type == SWT.Traverse) {
					if (e.detail == SWT.TRAVERSE_RETURN) {
						item.setText(0, param);
						item.setText(2, description);
						modifyProjection();
						// new empty item to add parameters
						new TableItem(parametersTable, SWT.NONE);
					} else if (e.detail == SWT.TRAVERSE_ESCAPE) {
						combo.dispose();
						e.doit = false;
					}
				}
			}
		};
		if (combo.getItemCount() > 0) {
			combo.addListener(SWT.FocusOut, listener);
			combo.addListener(SWT.Traverse, listener);
			editor.setEditor(combo, item, 0);
			combo.select(0);
			combo.setFocus();
		}
	}

	private TableItem findEllipsoidItem() {
		TableItem result = null;
		for (TableItem item : parametersTable.getItems()) {
			if (item.getText(0).equals("+ellps")) {
				result = item;
				break;
			}
		}
		return result;
	}

	public String validateProj4Chain() {
		String message = null;

		// get selected projection
		ProjectionSettings projectionSettings = findProjection(projectionCombo.getText());
		if (projectionSettings != null) {
			String proj4Projection = getProj4Chain();

			// test proj4 projection string
			SpatialReference srs = new SpatialReference();
			try {
				int code = srs.ImportFromProj4(proj4Projection);
				if (code != 0) {
					message = "Proj4 projection chain is not valid";
				} else {
					message = validateProj4Map();
				}
			} catch (RuntimeException e) {
				message = "Proj4 projection chain is not valid";
			}
		}
		return message;
	}

	public void setGeobox(GeoBox geoBox) {
		if (geoBox != null) {
			try {
				this.geoBox = geoBox.asLonLat();
				computeUTMZone();
				computeMercatorOptions();
			} catch (ProjectionException e) {
				// GeoBox ignored
			}
		}
	}

	/** Computes UTM zone corresponding to surrounding geo box */
	private void computeUTMZone() {
		utmParamsMap.clear();
		if (geoBox != null && geoBox.getProjection().isLongLatProjection() && !geoBox.isEmpty()) {
			double lat = geoBox.getCenterY();
			double lon = geoBox.getCenterX();
			if (-80 < lat && lat < 0) {
				utmParamsMap.put(UTM_SOUTH_FLAG, "");
			}
			if (-180 < lon && lon <= 180) {
				int zone = (int) ((lon + 180) / 6) + 1;
				utmParamsMap.put(UTM_ZONE_PARAM, Integer.toString(zone));
			}
		}
	}

	/** Computes Mercator options according to the geobox */
	private void computeMercatorOptions() {
		mercatorParamsMap.clear();
		if (geoBox != null && geoBox.getProjection().isLongLatProjection() && !geoBox.isEmpty()) {
			double lat = Math.round(geoBox.getCenterY() * 10.) / 10.;
			if (-90d < lat && lat < 90d)
				mercatorParamsMap.put(StandardProjection.MERCATOR_LAT, Double.toString(lat));
		}
	}

	public ProjectionSettings getProjectionSettings() {
		if (!projectionCombo.getText().isEmpty()) {
			return new ProjectionSettings(getProj4Chain());
		}
		return null;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	protected ProjectionSettings findProjection(String friendlyName) {
		return Arrays.stream(AVAILABLE_PROJECTIONS)//
				.filter(proj -> friendlyName.equals(proj.name()))//
				.findFirst()//
				.orElse(StandardProjection.LONGLAT);
	}

	public String getWarning() {
		// get selected projection
		String friendlyName = projectionCombo.getText();
		if (ProjectionSettings.UTM.equalsIgnoreCase(friendlyName)) {
			if (!proj4Map.containsKey(UTM_SOUTH_FLAG)) {
				return "Add the " + UTM_SOUTH_FLAG + " if necessary";
			}
		}
		return null;
	}

	private static final Map<String, String> PROJ4_PARAMETERS;
	static {
		PROJ4_PARAMETERS = new TreeMap<>();
		PROJ4_PARAMETERS.put("+a", "Semimajor radius of the ellipsoid axis");
		PROJ4_PARAMETERS.put("+alpha", "? Used with Oblique Mercator and possibly a few others");
		PROJ4_PARAMETERS.put("+axis", "Axis orientation (new in 4.8.0)");
		PROJ4_PARAMETERS.put("+b", "Semiminor radius of the ellipsoid axis");
		PROJ4_PARAMETERS.put("+datum", "Datum name (see `proj -ld`)");
		PROJ4_PARAMETERS.put("+ellps", "Ellipsoid name (see `proj -le`)");
		PROJ4_PARAMETERS.put("+k", "Scaling factor (old name)");
		PROJ4_PARAMETERS.put("+k_0", "Scaling factor (new name)");
		PROJ4_PARAMETERS.put("+lat_0", "Latitude of origin");
		PROJ4_PARAMETERS.put("+lat_1", "Latitude of first standard parallel");
		PROJ4_PARAMETERS.put("+lat_2", "Latitude of second standard parallel");
		PROJ4_PARAMETERS.put("+lat_ts", "Latitude of true scale");
		PROJ4_PARAMETERS.put("+lon_0", "Central meridian");
		PROJ4_PARAMETERS.put("+lonc", "? Longitude used with Oblique Mercator and possibly a few others");
		PROJ4_PARAMETERS.put("+lon_wrap", "Center longitude to use for wrapping (see below)");
		PROJ4_PARAMETERS.put("+nadgrids", "Filename of NTv2 grid file to use for datum transforms (see below)");
		PROJ4_PARAMETERS.put("+no_defs", "Don't use the /usr/share/proj/proj_def.dat defaults file");
		PROJ4_PARAMETERS.put("+over",
				"Allow longitude output outside -180 to 180 range, disables wrapping (see below)");
		PROJ4_PARAMETERS.put("+pm", "Alternate prime meridian (typically a city name, see below)");
		PROJ4_PARAMETERS.put("+proj", "Projection name (see `proj -l`)");
		PROJ4_PARAMETERS.put("+south", "Denotes southern hemisphere UTM zone");
		PROJ4_PARAMETERS.put("+to_meter", "Multiplier to convert map units to 1.0m");
		PROJ4_PARAMETERS.put("+towgs84", "3 or 7 term datum transform parameters (see below)");
		PROJ4_PARAMETERS.put("+units", "meters, US survey feet, etc.");
		PROJ4_PARAMETERS.put("+vto_meter", "vertical conversion to meters.");
		PROJ4_PARAMETERS.put("+vunits", "vertical units.");
		PROJ4_PARAMETERS.put("+x_0", "False easting");
		PROJ4_PARAMETERS.put("+y_0", "False northing");
		PROJ4_PARAMETERS.put("+zone", "UTM zone");
	}

}
