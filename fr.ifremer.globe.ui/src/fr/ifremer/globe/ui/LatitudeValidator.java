package fr.ifremer.globe.ui;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;

/**
 * Validate a latitude in degrees.
 * 
 * @author bvalliere
 * 
 */
@Creatable
public class LatitudeValidator implements IValidator {

	public static final IValidator INSTANCE = new LatitudeValidator();

	@Override
	public IStatus validate(Object value) {
		if (value instanceof String) {
			ILatLongFormatter formatter = LatLongFormater.getFormatter();
			String latString = (String) value;
			Double latitude = formatter.parseLatitude(latString);
			if (latitude != null) {
				if (Double.compare(latitude, 90.0) > 0 || Double.compare(latitude, -90.0) < 0) {
					return ValidationStatus.error("Latitude must be between 0 and 90");
				}
			} else {
				return ValidationStatus.error("Invalid latitude");
			}
			return ValidationStatus.info(Double.toString(latitude));
		}
		return ValidationStatus.error("Invalid latitude");
	}
}
