package fr.ifremer.globe.ui.screenshot;

import java.io.File;

import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;

public class VideoCapture {

	/**
	 * Make a screen capture.
	 */
	public static void start(int a_imgIndex, String videoCaptureDirectory) {
		new OGLScreenshot(IGeographicViewService.grab().getWwd(),
				getScreenShotFilePath(a_imgIndex, new File(videoCaptureDirectory)).getAbsolutePath());
	}

	/** @return The path to a specified screenshot */
	public static File getScreenShotFilePath(int imgageIndex, File videoCaptureDirectory) {
		return new File(videoCaptureDirectory, String.format("img-%d.png", imgageIndex));
	}

}
