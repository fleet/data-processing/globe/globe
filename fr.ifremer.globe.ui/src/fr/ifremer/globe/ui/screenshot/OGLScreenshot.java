package fr.ifremer.globe.ui.screenshot;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.event.RenderingEvent;
import gov.nasa.worldwind.event.RenderingListener;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.concurrent.CompletableFuture;

import javax.imageio.ImageIO;
import com.jogamp.opengl.GLAutoDrawable;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;

/**
 * Take a screenshot using an OpenGL render to file processing. The destination
 * image file format is inferred from destination file extension (cf.
 * {@link #FORMAT_EXTENSIONS}). If file extension cannot be recognized the
 * default PNG is used.
 * 
 * @author gbourel
 */
public class OGLScreenshot {

	public final static String[] FORMAT_NAMES = new String[] { "Portable Network Graphics", "JPEG" };
	public final static String[] FORMAT_EXTENSIONS = new String[] { "*.png", "*.jpg" };
	public final static String DEFAULT_EXTENSION = ".png";

	private Logger logger = LoggerFactory.getLogger(OGLScreenshot.class);

	private final WorldWindow wwd;
	private File destFile = null;

	public OGLScreenshot(WorldWindow wwd, String filename) {
		if (wwd == null) {
			throw new InvalidParameterException("Cannot take screenshot using a NULL WorldWindow");
		}

		this.wwd = wwd;

		boolean knownFormat = false;
		for (String ext : FORMAT_EXTENSIONS) {
			if (filename.endsWith(ext.substring(1))) {
				knownFormat = true;
			}
		}
		if (!knownFormat) {
			filename = filename + DEFAULT_EXTENSION;
		}

		destFile = new File(filename);

		ScreenShotListener listener = new ScreenShotListener();
		wwd.removeRenderingListener(listener); // ensure not to add a
		// duplicate
		wwd.addRenderingListener(listener);
	}

	public class ScreenShotListener implements RenderingListener {

		@Override
		public void stageChanged(RenderingEvent event) {
			if (event.getStage().equals(RenderingEvent.AFTER_BUFFER_SWAP) && destFile != null) {
				try {
					GLAutoDrawable drawable = (GLAutoDrawable) event.getSource();
					AWTGLReadBufferUtil glReadBufferUtil = new AWTGLReadBufferUtil(drawable.getGLProfile(), false);
					BufferedImage image = glReadBufferUtil.readPixelsToBufferedImage(drawable.getGL(), true);
					CompletableFuture.runAsync(() -> {
						try {
							ImageIO.write(image, FilenameUtils.getExtension(destFile.getName()), destFile);
						} catch (IOException e) {
							logger.error("Error during screenshot.", e);
						}

						logger.info("Image saved to " + destFile.getPath());
					});
				} finally {
					wwd.removeRenderingListener(this);
				}
			}
		}

	}

}
