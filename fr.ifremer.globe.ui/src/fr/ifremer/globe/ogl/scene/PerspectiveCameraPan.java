package fr.ifremer.globe.ogl.scene;

import java.awt.Dimension;

import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.scene.Camera;
import fr.ifremer.globe.ogl.renderer.scene.PerspectiveCamera;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;

/**
 * 
 * <p>
 * Description : A camera that always faces a particular point. The location of
 * the camera, from which it views the point, is specified by an Azimuth,
 * Elevation, and Range. FixedToLocalRotation defines the transformation from
 * the Camera's axes to a local set of axes in which the azimuth, elevation, and
 * range are defined.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 28 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class PerspectiveCameraPan extends AbstractCameraPan {

	private ZoomFactorComputer zoomComputer = new ZoomFactorComputer();

	/**
	 * Initializes a new instance.
	 * 
	 * @param camera
	 *            The renderer camera that is to be manipulated by the new
	 *            instance.
	 * @param window
	 *            The window in which the scene is drawn.
	 * @param ellipsoid
	 *            The ellipsoid defining the shape of the globe.
	 */
	public PerspectiveCameraPan(PerspectiveCamera camera, GraphicsWindow window, double initialRange) {
		super(window, initialRange);
		if (camera == null) {
			throw new IllegalArgumentException("camera");
		}
		if (window == null) {
			throw new IllegalArgumentException("window");
		}
		_camera = camera;
		setMouseEnabled(true);
		// Reset view
		resetView();
	}

	/**
	 * Gets the renderer camera that is manipulated by this instance.
	 */
	public Camera getCamera() {
		return _camera;
	}

	/**
	 * Updates Azimuth, Elevation and Range properties based on the current
	 * position of the renderer Camera.
	 */
	@Override
	protected void updateParametersFromCamera() {
		_eye = _camera.getEye();
		_target = _camera.getTargetOnXYPlane();
		_up = _camera.getUp();
		_range = _camera.getDistanceFromTargetOnXYPlane();
	}

	@Override
	protected void updateCameraFromParameters() {
		_camera.setTarget(_target);
		_camera.setEye(_eye);
		_camera.setUp(_up);
	}

	@Override
	protected void rotate(Dimension movement) {
		checkDisposed();

		// Compute current angle with the target
		double distance3DFromTarget = _target.subtract(_eye).getMagnitude();

		// Compute angle offset
		double angleXOffset = movement.height / 400.0;
		double angleZOffset = -movement.width / 400.0;

		// Rotate up axis
		_up = _up.rotateAroundAxis(Vector3D.getUnitZ(), angleZOffset);
		_up = _up.rotateAroundAxis(_camera.getRight(), angleXOffset);

		// If up.z < 0.0, we cancel the rotation
		if (_up.getZ() < 0) {
			_up = _up.rotateAroundAxis(_camera.getRight(), -angleXOffset);
			angleXOffset = 0.0;
		}

		// Rotate around the target
		Vector3D eyeToTarget = _target.subtract(_eye);
		eyeToTarget = eyeToTarget.normalize();


		eyeToTarget = eyeToTarget.rotateAroundAxis(Vector3D.getUnitZ(), angleZOffset);
		eyeToTarget = eyeToTarget.rotateAroundAxis(_camera.getRight(), angleXOffset);
		eyeToTarget = eyeToTarget.normalize();
		eyeToTarget = eyeToTarget.multiply(distance3DFromTarget);
		eyeToTarget = eyeToTarget.negate();
		eyeToTarget = _target.add(eyeToTarget);
		_eye = eyeToTarget;

		// Correct up axis imprecisions : right camera vector must have z=0.0
		Vector3D cameraForward = (_target.subtract(_eye)).normalize();
		Vector3D cameraRight = cameraForward.cross(_up).normalize();
		cameraRight = new Vector3D(cameraRight.getX(), cameraRight.getY(), 0.0).normalize();
		_up = cameraRight.cross(cameraForward);
	}

	@Override
	protected void translate(Dimension movement) {
		checkDisposed();

		double translateRate = _range;

		double max = Math.min(_window.getWidth(), _window.getHeight());
		double xWindowRatio = movement.width / max;
		double yWindowRatio = movement.height / max;

		Vector3D forwardAxis = _camera.getRight().cross(new Vector3D(0, 0, -1)).normalize();

		// Special case : if forward doesn't fit then take up instead
		if (forwardAxis.isUndefined()) {
			forwardAxis = new Vector3D(_up.getX(), _up.getY(), 0.0).normalize();
		}

		Vector3D rightAxis = new Vector3D(_camera.getRight().getX(), _camera.getRight().getY(), 0.0).normalize();

		Vector3D eyeOffsetX = rightAxis.multiply(-translateRate * xWindowRatio);
		Vector3D eyeOffsetY = forwardAxis.multiply(translateRate * yWindowRatio);

		_eye = _eye.add(eyeOffsetX).add(eyeOffsetY);

		Vector3D targetOffsetX = rightAxis.multiply(-translateRate * xWindowRatio);
		Vector3D targetOffsetY = forwardAxis.multiply(translateRate * yWindowRatio);

		_target = _target.add(targetOffsetX).add(targetOffsetY);
	}

	@Override
	protected void zoom(double click) {
		checkDisposed();
		// Compute new range
		_range -= zoomComputer.computeZoomRate(_range) * click;

		// Update eye position with new range
		Vector3D toEye = _eye.subtract(_target).normalize();
		_eye = _target.add(toEye.multiply(_range));
	}

	private PerspectiveCamera _camera;

}