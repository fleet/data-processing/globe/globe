package fr.ifremer.globe.ogl.scene;

import java.awt.Dimension;

import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.scene.OrthographicCamera;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;

public class OrthographicCameraPan extends AbstractCameraPan {

	private ZoomFactorComputer zoomComputer = new ZoomFactorComputer();

	private OrthographicCamera _camera;

	private double m_initialOrthographicLeft = 0;
	private double m_initialOrthographicRight = 0;
	private double m_initialOrthographicBottom = 0;
	private double m_initialOrthographicTop = 0;

	public OrthographicCameraPan(OrthographicCamera camera, GraphicsWindow window, double initialRange) {
		super(window, initialRange);
		_camera = camera;
		setMouseEnabled(true);
		// Reset view
		m_initialOrthographicLeft = _camera.getOrthographicLeft();
		m_initialOrthographicRight = _camera.getOrthographicRight();
		m_initialOrthographicBottom = _camera.getOrthographicBottom();
		m_initialOrthographicTop = _camera.getOrthographicTop();
		resetView();

		updateCameraFromParameters();
	}

	/**
	 * Updates Azimuth, Elevation and Range properties based on the current
	 * position of the renderer Camera.
	 */
	@Override
	protected void updateParametersFromCamera() {
		_eye = _camera.getEye();
		_target = _camera.getTarget();
		_up = _camera.getUp();

	}

	@Override
	public void resetView() {
		_range = _initialRange;
		updateParametersFromCamera();

		// Reset view
		_camera.setOrthographics(m_initialOrthographicLeft,m_initialOrthographicRight,m_initialOrthographicBottom,m_initialOrthographicTop);

		// Default values
		updateCameraFromParameters();
	};

	@Override
	protected void updateCameraFromParameters() {
		_camera.setTarget(_target);
		_camera.setEye(_eye);
		_camera.setUp(_up);

		_camera.setOrthographicNearPlaneDistance(0.01);
		double distance3DFromTarget = _target.subtract(_eye).getMagnitude();
		if(distance3DFromTarget==0 || Double.isNaN(distance3DFromTarget))
			distance3DFromTarget=1;
		_camera.setOrthographicFarPlaneDistance(distance3DFromTarget + _initialRange);
	}

	@Override
	protected void rotate(Dimension movement) {
		checkDisposed();

		// Compute current angle with the target
		double distance3DFromTarget = _target.subtract(_eye).getMagnitude();

		// Compute angle offset
		double angleXOffset = -movement.height / 400.0;
		double angleZOffset = -movement.width / 400.0;

		// Rotate up axis
		_up = _up.rotateAroundAxis(Vector3D.getUnitZ(), angleZOffset);
		_up = _up.rotateAroundAxis(_camera.getRight(), angleXOffset);

		// If up.z < 0.0, we cancel the rotation
		if (_up.getZ() < 0) {
			_up = _up.rotateAroundAxis(_camera.getRight(), -angleXOffset);
			angleXOffset = 0.0;
		}

		// Rotate around the target
		Vector3D eyeToTarget = _target.subtract(_eye);
		eyeToTarget = eyeToTarget.normalize();
		// eyeToTarget = new Vector3D(eyeToTarget.getX(), eyeToTarget.getY(),
		// eyeToTarget.getZ() + Math.sin(angleOffset));
		eyeToTarget = eyeToTarget.rotateAroundAxis(Vector3D.getUnitZ(), angleZOffset);
		eyeToTarget = eyeToTarget.rotateAroundAxis(_camera.getRight(), angleXOffset);
		eyeToTarget = eyeToTarget.normalize();
		eyeToTarget = eyeToTarget.multiply(distance3DFromTarget);
		eyeToTarget = eyeToTarget.negate();
		eyeToTarget = _target.add(eyeToTarget);
		_eye = eyeToTarget;

		// Correct up axis imprecisions : right camera vector must have z=0.0
		Vector3D cameraForward = (_target.subtract(_eye)).normalize();
		Vector3D cameraRight = cameraForward.cross(_up).normalize();
		cameraRight = new Vector3D(cameraRight.getX(), cameraRight.getY(), 0.0).normalize();
		_up = cameraRight.cross(cameraForward);
	}

	@Override
	protected void translate(Dimension movement) {
		checkDisposed();

		double translateRate = _range;

		double max = Math.min(_window.getWidth(), _window.getHeight());
		double xWindowRatio = movement.width / max;
		double yWindowRatio = movement.height / max;

		Vector3D forwardAxis = _camera.getRight().cross(new Vector3D(0, 0, -1)).normalize();

		// Special case : if forward doesn't fit then take up instead
		if (forwardAxis.isUndefined()) {
			forwardAxis = new Vector3D(_up.getX(), _up.getY(), 0.0).normalize();
		}

		Vector3D rightAxis = new Vector3D(_camera.getRight().getX(), _camera.getRight().getY(), 0.0).normalize();

		Vector3D eyeOffsetX = rightAxis.multiply(-translateRate * xWindowRatio);
		Vector3D eyeOffsetY = forwardAxis.multiply(translateRate * yWindowRatio);

		_eye = _eye.add(eyeOffsetX).add(eyeOffsetY);

		Vector3D targetOffsetX = rightAxis.multiply(-translateRate * xWindowRatio);
		Vector3D targetOffsetY = forwardAxis.multiply(translateRate * yWindowRatio);

		_target = _target.add(targetOffsetX).add(targetOffsetY);
	}

	@Override
	protected void zoom(double click) {
		checkDisposed();

		_range -= zoomComputer.computeZoomRate(_range) * click;

		// compute a reduction factor as a fraction of the initial range and the
		// computed range
		double factor = _range / _initialRange;

		// update camera size
		this._camera.setOrthographics(m_initialOrthographicLeft * factor,m_initialOrthographicRight * factor,m_initialOrthographicBottom * factor,m_initialOrthographicTop * factor);

	}

}
