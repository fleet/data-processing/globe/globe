package fr.ifremer.globe.ogl.scene;

/**
 * Simple utility class for zoom computation
 * */
public class ZoomFactorComputer {
	ZoomFactorComputer() {
		_zoomFactor = 0.01;
		_maximumZoomRate = Double.MAX_VALUE;
		_minimumZoomRate = 0.01;
	}

	private double _zoomFactor;
	private double _maximumZoomRate;
	private double _minimumZoomRate;

	/**
	 * compute the zoom rate The zoom rate is the distance by which the Range is
	 * adjusted. It is computed as follows:
	 * <code>ZoomRate = ZoomFactor * (Range )</code>.
	 * */
	public double computeZoomRate(double range) {
		_zoomFactor = 0.02;
		double zoomRate = _zoomFactor * (range);
		if (zoomRate > _maximumZoomRate) {
			zoomRate = _maximumZoomRate;
		}
		if (zoomRate < _minimumZoomRate) {
			zoomRate = _minimumZoomRate;
		}
		return zoomRate;
	}

	/**
	 * Gets the factor used to compute the rate at which the camera zooms in
	 * response to mouse events. The zoom rate is the distance by which the
	 * Range is adjusted when the mouse moves a distance equal to the
	 * GraphicsWindow.Height of the Window. It is computed as follows:
	 * <code>ZoomRate = ZoomFactor * (Range )</code>. When the mouse is moved
	 * across a fraction of the window, the range is adjusted by the
	 * corresponding fraction of the zoom rate.
	 */
	public double getZoomFactor() {
		return _zoomFactor;
	}

	/**
	 * Gets the maximum rate at which the camera zooms in response to mouse
	 * events, regardless of the current Range. See ZoomFactor and
	 * ZoomRateRangeAdjustment for an explanation of how the zoom rate is
	 * computed.
	 */
	public double getMaximumZoomRate() {
		return _maximumZoomRate;
	}

	/**
	 * Sets the maximum rate at which the camera zooms in response to mouse
	 * events, regardless of the current Range. See ZoomFactor and
	 * ZoomRateRangeAdjustment for an explanation of how the zoom rate is
	 * computed.
	 */
	public void setMaximumZoomRate(double value) {
		_maximumZoomRate = value;
	}

	/**
	 * Gets the minimum rate at which the camera zooms in response to mouse
	 * events, regardless of the current Range. See ZoomFactor and
	 * ZoomRateRangeAdjustment for an explanation of how the zoom rate is
	 * computed.
	 */
	public double getMinimumZoomRate() {
		return _minimumZoomRate;
	}

	/**
	 * Sets the minimum rate at which the camera zooms in response to mouse
	 * events, regardless of the current Range. See ZoomFactor and
	 * ZoomRateRangeAdjustment for an explanation of how the zoom rate is
	 * computed.
	 */
	public void setMinimumZoomRate(double value) {
		_minimumZoomRate = value;
	}

}
