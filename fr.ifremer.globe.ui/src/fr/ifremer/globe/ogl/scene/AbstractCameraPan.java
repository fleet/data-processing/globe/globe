package fr.ifremer.globe.ogl.scene;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import javax.swing.SwingUtilities;

import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;

public abstract class AbstractCameraPan implements CameraBehaviour {
	private boolean _mouseEnabled;

	protected boolean _leftButtonDown;
	protected boolean _rightButtonDown;
	protected boolean _middleButtonDown;
	protected double _initialRange;

	protected Vector3D _up = new Vector3D(0, 1, 0);
	protected Vector3D _eye;
	protected Vector3D _target;
	protected double _range;

	/**
	 * Last view parameters
	 * */
	private Vector3D _lasteye;
	private Vector3D _lastup;
	private Vector3D _lasttarget;

	private Point _lastPoint;

	public boolean is_leftButtonDown() {
		return _leftButtonDown;
	}

	public boolean is_rightButtonDown() {
		return _rightButtonDown;
	}

	public boolean is_middleButtonDown() {
		return _middleButtonDown;
	}

	AbstractCameraPan(GraphicsWindow window, double initialRange) {
		_window = window;
		_initialRange = initialRange;
		_range = initialRange;

	}

	protected void updateKeyStatus(MouseEvent e) {
		_leftButtonDown = false;
		_rightButtonDown = false;
		_middleButtonDown = false;
		if (SwingUtilities.isLeftMouseButton(e)) {
			_leftButtonDown = true;
		} else if (SwingUtilities.isRightMouseButton(e)) {
			_rightButtonDown = true;
		} else if (SwingUtilities.isMiddleMouseButton(e)) {
			_middleButtonDown = true;
		}
	}

	private class MouseBehavior extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			onMouseDown(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			onMouseUp(e);
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			onMouseMove(e);
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			onMouseWheelMove(e);
		}
	}

	@Override
	public void resetView() {
		_eye = new Vector3D(0, 0, _initialRange);
		_target = new Vector3D(0, 0, 0);
		_up = new Vector3D(0, 1, 0);
		_range = _initialRange;
		// Default values
		updateCameraFromParameters();
	}

	/**
	 * 
	 * */
	@Override
	public void setViewLowAngle() {
		Vector3D dist = new Vector3D(_eye.getX() - _target.getX(), _eye.getY() - _target.getY(), _eye.getZ() - _target.getZ());

		double distance2 = dist.getMagnitude();
		Vector3D pos = new Vector3D(1, -1, 1);
		pos = pos.normalize();
		pos = pos.multiply(distance2);
		pos.add(_target);

		_eye = pos;
		_up = new Vector3D(0, 0, 1);
		updateCameraFromParameters();
	}

	/**
	 * Set camera to facing to north
	 * */
	@Override
	public void setViewNorth() {
		Vector3D dist = new Vector3D(_eye.getX() - _target.getX(), _eye.getY() - _target.getY(), _eye.getZ() - _target.getZ());
		double distance2 = dist.getMagnitudeSquared();

		double x = Math.sqrt(distance2 - Math.pow((_eye.getZ() - _target.getZ()), 2));
		_eye = new Vector3D(0, -x, _eye.getZ());
		_up = new Vector3D(0, 0, 1);
		updateCameraFromParameters();
	}

	/**
	 * Set camera to the very last view
	 * */
	@Override
	public void setLastView() {
		if (_lasteye != null && _lastup != null && _lasttarget != null) {
			_eye = _lasteye;
			_up = _lastup;
			_target = _lasttarget;
		}
		updateCameraFromParameters();
	}

	/***
	 * save the last known position
	 * */
	protected void saveLastView() {
		// save last position
		_lasteye = _eye;
		_lastup = _up;
		_lasttarget = _target;
	}

	/**
	 * Set camera to see data from up in a pseudo 2D
	 * */
	@Override
	public void setViewUp() {
		_eye = new Vector3D(0, 0, _range);
		updateCameraFromParameters();
	}

	private MouseBehavior m_MouseBehavior = new MouseBehavior();

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.CameraBehaviour#
	 * enableDefaultAdaptor(boolean)
	 */
	@Override
	public void enableDefaultAdaptor(boolean enable) {
		setMouseEnabled(enable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.CameraBehaviour#
	 * isMouseEnabled()
	 */
	@Override
	public boolean isMouseEnabled() {
		return _mouseEnabled;
	}

	protected void checkDisposed() {
		if (_window == null) {
			throw new IllegalStateException(PerspectiveCameraPan.class.getName());
		}
	}

	protected GraphicsWindow _window;

	public void setMouseEnabled(boolean value) {
		checkDisposed();

		if (value != _mouseEnabled) {
			_mouseEnabled = value;
			if (_mouseEnabled) {
				_window.addMouseListener(m_MouseBehavior);
				_window.addMouseMotionListener(m_MouseBehavior);
				_window.addMouseWheelListener(m_MouseBehavior);
			} else {
				_window.removeMouseListener(m_MouseBehavior);
				_window.removeMouseMotionListener(m_MouseBehavior);
				_window.removeMouseWheelListener(m_MouseBehavior);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.CameraBehaviour#dispose
	 * ()
	 */
	@Override
	public void dispose() {
		if (_window != null) {
			setMouseEnabled(false);
			_window = null;
		}
	}

	/**
	 * Updates internal properties based on the current position of the renderer
	 * Camera.
	 */
	protected abstract void updateParametersFromCamera();

	/**
	 * Updates camera based on the current internal properties Camera.
	 */
	protected abstract void updateCameraFromParameters();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.ICameraPan#onMouseWheelMove
	 * (java.awt.event.MouseWheelEvent)
	 */
	@Override
	public void onMouseWheelMove(MouseWheelEvent e) {
		int wheelRotation = e.getWheelRotation();
		onZoom(wheelRotation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.ICameraPan#onMouseWheelMove
	 * (org.eclipse.swt.events.MouseEvent)
	 */
	@Override
	public void onMouseWheelMove(org.eclipse.swt.events.MouseEvent e) {
		int wheelRotation = -e.count / 3;
		onZoom(wheelRotation);
	}

	protected void onZoom(int wheelRotation) {

		updateParametersFromCamera();

		zoom(-wheelRotation);

		updateCameraFromParameters();
		this._window.display();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.ICameraPan#onMouseUp
	 * (java .awt.event.MouseEvent)
	 */
	@Override
	public void onMouseUp(MouseEvent e) {
		updateKeyStatus(e);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.ICameraPan#onMouseMove
	 * (java.awt.event.MouseEvent)
	 */
	@Override
	public void onMouseMove(MouseEvent e) {
		if (!is_middleButtonDown() && !is_leftButtonDown() && !is_rightButtonDown()) {
			return;
		}

		updateParametersFromCamera();

		Dimension movement = new Dimension(e.getPoint().x - _lastPoint.x, e.getPoint().y - _lastPoint.y);
		if (is_leftButtonDown()) {
			// translate(movement);
		}

		if (is_middleButtonDown()) {
			// zoom(movement);
			translate(movement);

		}

		if (is_rightButtonDown()) {
			rotate(movement);
		}

		this._window.display();
		updateCameraFromParameters();

		_lastPoint = e.getPoint();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.ifremer.globe.editor.dtmviewer.clipmap.scene.cameras.ICameraPan#onMouseDown
	 * (java.awt.event.MouseEvent)
	 */
	@Override
	public void onMouseDown(MouseEvent e) {
		updateKeyStatus(e);
		if (is_rightButtonDown() == true || is_middleButtonDown() == true) {
			saveLastView();
		}
		_lastPoint = e.getPoint();
	}

	protected abstract void rotate(Dimension movement);

	protected abstract void translate(Dimension movement);

	/**
	 * zoom from a certain amount (the number of click of the middle roll of the
	 * mouse
	 * */
	protected abstract void zoom(double click);
}
