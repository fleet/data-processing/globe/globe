package fr.ifremer.globe.ogl.scene;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

/**
 * 
 * <p>
 * Description : Interface to delegate handling mouseEvent to another class
 * without break Camera features and expose CameraPan class. It's needed when
 * using DtmScene3D in a well builded Eclipse 4 plugin (with fragment and
 * inserting DtmScene directly in editor)
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 20 fev. 2013
 * </p>
 * 
 * @author Guillaume FOUILLET
 * @version 1.0
 */
public interface MouseAdapted {

	/**
	 * Enable or disable default adaptator, if exists
	 * 
	 * @param enable
	 *            true if we want enable adaptator, false otherwise
	 */
	void enableDefaultAdaptor(boolean enable);

	/**
	 * Handle mouse down event.
	 * 
	 * @param e
	 *            mouse event
	 */
	void onMouseDown(MouseEvent e);

	/**
	 * Handle mouse up event.
	 * 
	 * @param e
	 *            mouse event
	 */
	void onMouseUp(MouseEvent e);

	/**
	 * Handle mouse wheel event (AWT)
	 * 
	 * @param e
	 *            mouse event
	 */
	void onMouseWheelMove(MouseWheelEvent e);

	/**
	 * Handle mouse wheel event (SWT)
	 * 
	 * @param e
	 *            mouse event
	 */
	void onMouseWheelMove(org.eclipse.swt.events.MouseEvent e);

	/**
	 * Handle mouse movee event.
	 * 
	 * @param e
	 *            mouse event
	 */
	void onMouseMove(MouseEvent e);

}