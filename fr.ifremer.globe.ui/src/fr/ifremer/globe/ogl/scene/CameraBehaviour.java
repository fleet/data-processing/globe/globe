package fr.ifremer.globe.ogl.scene;


public interface CameraBehaviour extends MouseAdapted {

	/**
	 * Disposes the camera. After it is disposed, the camera should not be used.
	 */
	public abstract void dispose();

	/**
	 * Gets a value indicating if the mouse is enabled or disabled. If the value
	 * of this property is true, the camera will respond to mouse events. If it
	 * is false, mouse events will be ignored.
	 */
	public abstract boolean isMouseEnabled();

	@Override
	public abstract void enableDefaultAdaptor(boolean enable);

	public abstract void resetView();

	public abstract void setViewLowAngle();

	/**
	 * Set camera to see data from up in a pseudo 2D
	 * */
	public abstract void setViewUp();

	/**
	 * Set camera to facing to north
	 * */
	public abstract void setViewNorth();

	/**
	 * set the camera to the last view
	 * */
	public abstract void setLastView();

}