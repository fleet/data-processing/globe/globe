package fr.ifremer.globe.ogl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import com.jogamp.opengl.GLAutoDrawable;

import org.eclipse.jface.viewers.CheckStateChangedEvent;


/**
 * This class tries to unify formerly DtmScene3D and DtmScene2D (=>
 * SoundingsScene). All new commons behaviors should be added in this class
 * 
 * This 3D scene contains mechanisms used by @see MouseViewControl
 * 
 * @author bvalliere
 */
public abstract class Globe3DScene {

	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	protected List<GLComponent> components = new ArrayList<GLComponent>();

	public abstract void refresh();

	public void addGLComponent(GLComponent component) {
		components.add(component);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void fireEvent(String propertyName) {
		pcs.firePropertyChange(propertyName, false, true);
	}

	public void fireEvent(String propertyName, Object newValue) {
		pcs.firePropertyChange(propertyName, false, newValue);
	}

	public void fireEvent(String propertyName, Object oldValue, Object newValue) {
		pcs.firePropertyChange(propertyName, oldValue, newValue);
	}

	public void display(GLAutoDrawable glad, CheckStateChangedEvent event) {
		// TODO Auto-generated method stub
		
	}
}
