package fr.ifremer.globe.ogl.core.matrices;

import java.util.Locale;

/**
 * <p>
 * Description : Matrix4F. 4x4 matrix - 4 columns and 4 rows.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Matrix4F {

	public Matrix4F() {
		_values = new float[getNumberOfComponents()];
	}

	public Matrix4F(float value) {
		_values = new float[] { value, value, value, value, value, value, value, value, value, value, value, value, value, value, value, value };
	}

	public Matrix4F(float column0row0, float column1row0, float column2row0, float column3row0, float column0row1, float column1row1, float column2row1, float column3row1, float column0row2,
			float column1row2, float column2row2, float column3row2, float column0row3, float column1row3, float column2row3, float column3row3) {
		_values = new float[] { column0row0, column0row1, column0row2, column0row3, column1row0, column1row1, column1row2, column1row3, column2row0, column2row1, column2row2, column2row3,
				column3row0, column3row1, column3row2, column3row3 };
	}

	public int getNumberOfComponents() {
		return 16;
	}

	public float getColumn0Row0() {
		return _values[0];
	}

	public float getColumn0Row1() {
		return _values[1];
	}

	public float getColumn0Row2() {
		return _values[2];
	}

	public float getColumn0Row3() {
		return _values[3];
	}

	public float getColumn1Row0() {
		return _values[4];
	}

	public float getColumn1Row1() {
		return _values[5];
	}

	public float getColumn1Row2() {
		return _values[6];
	}

	public float getColumn1Row3() {
		return _values[7];
	}

	public float getColumn2Row0() {
		return _values[8];
	}

	public float getColumn2Row1() {
		return _values[9];
	}

	public float getColumn2Row2() {
		return _values[10];
	}

	public float getColumn2Row3() {
		return _values[11];
	}

	public float getColumn3Row0() {
		return _values[12];
	}

	public float getColumn3Row1() {
		return _values[13];
	}

	public float getColumn3Row2() {
		return _values[14];
	}

	public float getColumn3Row3() {
		return _values[15];
	}

	public Matrix4D toMatrix4D() {
		return new Matrix4D(getColumn0Row0(), getColumn1Row0(), getColumn2Row0(), getColumn3Row0(), getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn3Row1(), getColumn0Row2(),
				getColumn1Row2(), getColumn2Row2(), getColumn3Row2(), getColumn0Row3(), getColumn1Row3(), getColumn2Row3(), getColumn3Row3());
	}

	public Matrix4F transpose() {
		return new Matrix4F(getColumn0Row0(), getColumn0Row1(), getColumn0Row2(), getColumn0Row3(), getColumn1Row0(), getColumn1Row1(), getColumn1Row2(), getColumn1Row3(), getColumn2Row0(),
				getColumn2Row1(), getColumn2Row2(), getColumn2Row3(), getColumn3Row0(), getColumn3Row1(), getColumn3Row2(), getColumn3Row3());
	}

	public boolean equals(Matrix4F other) {
		if (other == null) {
			return false;
		}

		for (int i = 0; i < _values.length; ++i) {
			if (_values[i] != other._values[i]) {
				return false;
			}
		}

		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Matrix4F) {
			return equals((Matrix4F) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "Rows: \n(%1$f, %2$f, %3$f, %4$f) \n(%5$f, %6$f, %7$f, %8$f) \n(%9$f, %10$f, %11$f, %12$f) \n(%13$f, %14$f, %15$f, %16$f)", getColumn0Row0(),
				getColumn1Row0(), getColumn2Row0(), getColumn3Row0(), getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn3Row1(), getColumn0Row2(), getColumn1Row2(), getColumn2Row2(),
				getColumn3Row2(), getColumn0Row3(), getColumn1Row3(), getColumn2Row3(), getColumn3Row3());
	}

	@Override
	public int hashCode() {
		int hashCode = Double.valueOf(_values[0]).intValue();

		for (int i = 1; i < _values.length; ++i) {
			hashCode = hashCode ^ Double.valueOf(_values[1]).intValue();
		}

		return hashCode;
	}

	/**
	 * Shame on you if you change elements in this array. This type is supposed
	 * to be immutable.
	 */
	public float[] getReadOnlyColumnMajorValues() {
		return _values;
	}

	public static Matrix4F Identity = new Matrix4F(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);

	// Column major
	private float[] _values;
}