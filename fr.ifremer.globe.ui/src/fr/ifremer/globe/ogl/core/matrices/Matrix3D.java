package fr.ifremer.globe.ogl.core.matrices;

import java.util.Locale;

import fr.ifremer.globe.ogl.core.vectors.Vector3D;

/**
 * <p>
 * Description : 3x3 matrix - 3 columns and 3 rows.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 28 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Matrix3D {
	public Matrix3D() {
		_values = new double[getNumberOfComponents()];
	}

	public Matrix3D(double value) {
		_values = new double[] { value, value, value, value, value, value, value, value, value };
	}

	public Matrix3D(double column0Row0, double column1Row0, double column2Row0, double column0Row1, double column1Row1, double column2Row1, double column0Row2, double column1Row2, double column2Row2) {
		_values = new double[] { column0Row0, column0Row1, column0Row2, column1Row0, column1Row1, column1Row2, column2Row0, column2Row1, column2Row2, };
	}

	public Matrix3D invert() {
		// Source : http://www.songho.ca/opengl/gl_matrix.html

		Matrix3D result = null;

		double determinant, invDeterminant;
		double[] tmp = new double[9];

		tmp[0] = getColumn1Row1() * getColumn2Row2() - getColumn2Row1() * getColumn1Row2();
		tmp[1] = getColumn2Row0() * getColumn1Row2() - getColumn1Row0() * getColumn2Row2();
		tmp[2] = getColumn1Row0() * getColumn2Row1() - getColumn2Row0() * getColumn1Row1();
		tmp[3] = getColumn2Row1() * getColumn0Row2() - getColumn0Row1() * getColumn2Row2();
		tmp[4] = getColumn0Row0() * getColumn2Row2() - getColumn2Row0() * getColumn0Row2();
		tmp[5] = getColumn2Row0() * getColumn0Row1() - getColumn0Row0() * getColumn2Row1();
		tmp[6] = getColumn0Row1() * getColumn1Row2() - getColumn1Row1() * getColumn0Row2();
		tmp[7] = getColumn1Row0() * getColumn0Row2() - getColumn0Row0() * getColumn1Row2();
		tmp[8] = getColumn0Row0() * getColumn1Row1() - getColumn1Row0() * getColumn0Row1();

		// check determinant if it is 0
		determinant = getColumn0Row0() * tmp[0] + getColumn1Row0() * tmp[3] + getColumn2Row0() * tmp[6];

		if (Math.abs(determinant) >= 0.00001) {
			// Divide by the determinant
			invDeterminant = 1.0 / determinant;

			double c0r0 = invDeterminant * tmp[0];
			double c1r0 = invDeterminant * tmp[1];
			double c2r0 = invDeterminant * tmp[2];
			double c0r1 = invDeterminant * tmp[3];
			double c1r1 = invDeterminant * tmp[4];
			double c2r1 = invDeterminant * tmp[5];
			double c0r2 = invDeterminant * tmp[6];
			double c1r2 = invDeterminant * tmp[7];
			double c2r2 = invDeterminant * tmp[8];
			result = new Matrix3D(c0r0, c1r0, c2r0, c0r1, c1r1, c2r1, c0r2, c1r2, c2r2);
		}
		// Else, cannot invert

		return result;
	}

	public int getNumberOfComponents() {
		return 9;
	}

	public double getColumn0Row0() {
		return _values[0];
	}

	public double getColumn0Row1() {
		return _values[1];
	}

	public double getColumn0Row2() {
		return _values[2];
	}

	public double getColumn1Row0() {
		return _values[3];
	}

	public double getColumn1Row1() {
		return _values[4];
	}

	public double getColumn1Row2() {
		return _values[5];
	}

	public double getColumn2Row0() {
		return _values[6];
	}

	public double getColumn2Row1() {
		return _values[7];
	}

	public double getColumn2Row2() {
		return _values[8];
	}

	public Matrix3D transpose() {
		return new Matrix3D(getColumn0Row0(), getColumn0Row1(), getColumn0Row2(), getColumn1Row0(), getColumn1Row1(), getColumn1Row2(), getColumn2Row0(), getColumn2Row1(), getColumn2Row2());
	}

	public Vector3D multiply(Vector3D vector) {

		double[] values = getReadOnlyColumnMajorValues();

		double x = values[0] * vector.getX() + values[3] * vector.getY() + values[6] * vector.getZ();
		double y = values[1] * vector.getX() + values[4] * vector.getY() + values[7] * vector.getZ();
		double z = values[2] * vector.getX() + values[5] * vector.getY() + values[8] * vector.getZ();

		return new Vector3D(x, y, z);
	}

	public boolean equals(Matrix3D other) {
		if (other == null) {
			return false;
		}

		for (int i = 0; i < _values.length; ++i) {
			if (_values[i] != other._values[i]) {
				return false;
			}
		}

		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Matrix3D) {
			return equals((Matrix3D) obj);
		}

		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "Rows: \n(%1$f, %2$f, %3$f) \n(%4$f, %5$f, %6$f) \n(%7$f, %8$f, %9$f) \n(%10$f, %11$f, %12$f)", getColumn0Row0(), getColumn1Row0(), getColumn2Row0(),
				getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn0Row2(), getColumn1Row2(), getColumn2Row2());
	}

	@Override
	public int hashCode() {
		int hashCode = Double.valueOf(_values[0]).intValue();

		for (int i = 1; i < _values.length; ++i) {
			hashCode = hashCode ^ Double.valueOf(_values[i]).intValue();
		}

		return hashCode;
	}

	/**
	 * Shame on you if you change elements in this array. This type is supposed
	 * to be immutable.
	 */
	public double[] getReadOnlyColumnMajorValues() {
		return _values;
	}

	public static Matrix3D Identity = new Matrix3D(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);

	private double[] _values; // Column major
}