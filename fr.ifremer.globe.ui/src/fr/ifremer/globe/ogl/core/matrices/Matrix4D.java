package fr.ifremer.globe.ogl.core.matrices;

import java.util.Locale;

import fr.ifremer.globe.ogl.core.Transform3D;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.core.vectors.Vector4D;

/**
 * <p>
 * Description : Matrix4D. 4x4 matrix - 4 columns and 4 rows.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Matrix4D {
	public Matrix4D() {
		_values = new double[getNumberOfComponents()];
	}

	public Matrix4D(double value) {
		_values = new double[] { value, value, value, value, value, value, value, value, value, value, value, value, value, value, value, value };
	}

	public Matrix4D(Matrix3D matrix) {
		this(matrix.getColumn0Row0(), matrix.getColumn1Row0(), matrix.getColumn2Row0(), 0, matrix.getColumn0Row1(), matrix.getColumn1Row1(), matrix.getColumn2Row1(), 0, matrix.getColumn0Row2(),
				matrix.getColumn1Row2(), matrix.getColumn2Row2(), 0, 0, 0, 0, 1);
	}

	public Matrix4D(double column0row0, double column1row0, double column2row0, double column3row0, double column0row1, double column1row1, double column2row1, double column3row1, double column0row2,
			double column1row2, double column2row2, double column3row2, double column0row3, double column1row3, double column2row3, double column3row3) {
		_values = new double[] { column0row0, column0row1, column0row2, column0row3, column1row0, column1row1, column1row2, column1row3, column2row0, column2row1, column2row2, column2row3,
				column3row0, column3row1, column3row2, column3row3 };
	}

	public Matrix4D getRotationMatrix() {
		Transform3D t = new Transform3D(this);
		Matrix3D matrix = t.getRotationMatrix();
		Matrix4D result = new Matrix4D(matrix);
		return result;
	}

	public int getNumberOfComponents() {
		return 16;
	}

	public double getColumn0Row0() {
		return _values[0];
	}

	public double getColumn0Row1() {
		return _values[1];
	}

	public double getColumn0Row2() {
		return _values[2];
	}

	public double getColumn0Row3() {
		return _values[3];
	}

	public double getColumn1Row0() {
		return _values[4];
	}

	public double getColumn1Row1() {
		return _values[5];
	}

	public double getColumn1Row2() {
		return _values[6];
	}

	public double getColumn1Row3() {
		return _values[7];
	}

	public double getColumn2Row0() {
		return _values[8];
	}

	public double getColumn2Row1() {
		return _values[9];
	}

	public double getColumn2Row2() {
		return _values[10];
	}

	public double getColumn2Row3() {
		return _values[11];
	}

	public double getColumn3Row0() {
		return _values[12];
	}

	public double getColumn3Row1() {
		return _values[13];
	}

	public double getColumn3Row2() {
		return _values[14];
	}

	public double getColumn3Row3() {
		return _values[15];
	}

	public Matrix4F toMatrix4F() {
		return new Matrix4F((float) getColumn0Row0(), (float) getColumn1Row0(), (float) getColumn2Row0(), (float) getColumn3Row0(), (float) getColumn0Row1(), (float) getColumn1Row1(),
				(float) getColumn2Row1(), (float) getColumn3Row1(), (float) getColumn0Row2(), (float) getColumn1Row2(), (float) getColumn2Row2(), (float) getColumn3Row2(), (float) getColumn0Row3(),
				(float) getColumn1Row3(), (float) getColumn2Row3(), (float) getColumn3Row3());
	}

	public Matrix4D invert() {
		// Source : http://www.songho.ca/opengl/gl_matrix.html

		Matrix4D result = null;

		// If the 4th row is [0,0,0,1] then it is affine matrix and
		// it has no projective transformation.
		if (getColumn0Row3() == 0 && getColumn1Row3() == 0 && getColumn2Row3() == 0 && getColumn3Row3() == 1) {
			result = invertAffine();
		} else {
			result = invertGeneral();
		}

		return result;
	}

	public Matrix4D invertGeneral() {
		// Source : http://www.songho.ca/opengl/gl_matrix.html

		Matrix4D result = null;

		// Get cofactors of minor matrices
		double cofactor0 = getCofactor(getColumn1Row1(), getColumn2Row1(), getColumn3Row1(), getColumn1Row2(), getColumn2Row2(), getColumn3Row2(), getColumn1Row3(), getColumn2Row3(), getColumn3Row3());
		double cofactor1 = getCofactor(getColumn0Row1(), getColumn2Row1(), getColumn3Row1(), getColumn0Row2(), getColumn2Row2(), getColumn3Row2(), getColumn0Row3(), getColumn2Row3(), getColumn3Row3());
		double cofactor2 = getCofactor(getColumn0Row1(), getColumn1Row1(), getColumn3Row1(), getColumn0Row2(), getColumn1Row2(), getColumn3Row2(), getColumn0Row3(), getColumn1Row3(), getColumn3Row3());
		double cofactor3 = getCofactor(getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn0Row2(), getColumn1Row2(), getColumn2Row2(), getColumn0Row3(), getColumn1Row3(), getColumn2Row3());

		// get determinant
		double determinant = getColumn0Row0() * cofactor0 - getColumn1Row0() * cofactor1 + getColumn2Row0() * cofactor2 - getColumn3Row0() * cofactor3;
		if (Math.abs(determinant) > 0.00001f) {

			// get rest of cofactors for adj(M)
			double cofactor4 = getCofactor(getColumn1Row0(), getColumn2Row0(), getColumn3Row0(), getColumn1Row2(), getColumn2Row2(), getColumn3Row2(), getColumn1Row3(), getColumn2Row3(),
					getColumn3Row3());
			double cofactor5 = getCofactor(getColumn0Row0(), getColumn2Row0(), getColumn3Row0(), getColumn0Row2(), getColumn2Row2(), getColumn3Row2(), getColumn0Row3(), getColumn2Row3(),
					getColumn3Row3());
			double cofactor6 = getCofactor(getColumn0Row0(), getColumn1Row0(), getColumn3Row0(), getColumn0Row2(), getColumn1Row2(), getColumn3Row2(), getColumn0Row3(), getColumn1Row3(),
					getColumn3Row3());
			double cofactor7 = getCofactor(getColumn0Row0(), getColumn1Row0(), getColumn2Row0(), getColumn0Row2(), getColumn1Row2(), getColumn2Row2(), getColumn0Row3(), getColumn1Row3(),
					getColumn2Row3());

			double cofactor8 = getCofactor(getColumn1Row0(), getColumn2Row0(), getColumn3Row0(), getColumn1Row1(), getColumn2Row1(), getColumn3Row1(), getColumn1Row3(), getColumn2Row3(),
					getColumn3Row3());
			double cofactor9 = getCofactor(getColumn0Row0(), getColumn2Row0(), getColumn3Row0(), getColumn0Row1(), getColumn2Row1(), getColumn3Row1(), getColumn0Row3(), getColumn2Row3(),
					getColumn3Row3());
			double cofactor10 = getCofactor(getColumn0Row0(), getColumn1Row0(), getColumn3Row0(), getColumn0Row1(), getColumn1Row1(), getColumn3Row1(), getColumn0Row3(), getColumn1Row3(),
					getColumn3Row3());
			double cofactor11 = getCofactor(getColumn0Row0(), getColumn1Row0(), getColumn2Row0(), getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn0Row3(), getColumn1Row3(),
					getColumn2Row3());

			double cofactor12 = getCofactor(getColumn1Row0(), getColumn2Row0(), getColumn3Row0(), getColumn1Row1(), getColumn2Row1(), getColumn3Row1(), getColumn1Row2(), getColumn2Row2(),
					getColumn3Row2());
			double cofactor13 = getCofactor(getColumn0Row0(), getColumn2Row0(), getColumn3Row0(), getColumn0Row1(), getColumn2Row1(), getColumn3Row1(), getColumn0Row2(), getColumn2Row2(),
					getColumn3Row2());
			double cofactor14 = getCofactor(getColumn0Row0(), getColumn1Row0(), getColumn3Row0(), getColumn0Row1(), getColumn1Row1(), getColumn3Row1(), getColumn0Row2(), getColumn1Row2(),
					getColumn3Row2());
			double cofactor15 = getCofactor(getColumn0Row0(), getColumn1Row0(), getColumn2Row0(), getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn0Row2(), getColumn1Row2(),
					getColumn2Row2());

			// Build inverse matrix = adj(M) / det(M)
			// adjugate of M is the transpose of the cofactor matrix of M
			double invDeterminant = 1.0 / determinant;
			double c0r0 = invDeterminant * cofactor0;
			double c1r0 = -invDeterminant * cofactor4;
			double c2r0 = invDeterminant * cofactor8;
			double c3r0 = -invDeterminant * cofactor12;

			double c0r1 = -invDeterminant * cofactor1;
			double c1r1 = invDeterminant * cofactor5;
			double c2r1 = -invDeterminant * cofactor9;
			double c3r1 = invDeterminant * cofactor13;

			double c0r2 = invDeterminant * cofactor2;
			double c1r2 = -invDeterminant * cofactor6;
			double c2r2 = invDeterminant * cofactor10;
			double c3r2 = -invDeterminant * cofactor14;

			double c0r3 = -invDeterminant * cofactor3;
			double c1r3 = invDeterminant * cofactor7;
			double c2r3 = -invDeterminant * cofactor11;
			double c3r3 = invDeterminant * cofactor15;

			result = new Matrix4D(c0r0, c1r0, c2r0, c3r0, c0r1, c1r1, c2r1, c3r1, c0r2, c1r2, c2r2, c3r2, c0r3, c1r3, c2r3, c3r3);
		}

		return result;
	}

	/**
	 * Compute cofactor of 3x3 minor matrix without sign input params are 9
	 * elements of the minor matrix
	 * 
	 * NOTE: The caller must know its sign.
	 */
	private double getCofactor(double m0, double m1, double m2, double m3, double m4, double m5, double m6, double m7, double m8) {
		// Source : http://www.songho.ca/opengl/gl_matrix.html

		return m0 * (m4 * m8 - m5 * m7) - m1 * (m3 * m8 - m5 * m6) + m2 * (m3 * m7 - m4 * m6);
	}

	public Matrix4D invertAffine() {
		// Source : http://www.songho.ca/opengl/gl_matrix.html

		Matrix4D result = null;

		// R^-1
		Matrix3D r = new Matrix3D(getColumn0Row0(), getColumn1Row0(), getColumn2Row0(), getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn0Row2(), getColumn1Row2(), getColumn2Row2());
		r = r.invert();

		// -R^-1 * T
		double x = getColumn3Row0();
		double y = getColumn3Row1();
		double z = getColumn3Row2();
		double c3r0 = -(r.getColumn0Row0() * x + r.getColumn1Row0() * y + r.getColumn2Row0() * z);
		double c3r1 = -(r.getColumn0Row1() * x + r.getColumn1Row1() * y + r.getColumn2Row1() * z);
		double c3r2 = -(r.getColumn0Row2() * x + r.getColumn1Row2() * y + r.getColumn2Row2() * z);

		// Last row should be unchanged (0,0,0,1)
		double c0r3 = 0.0;
		double c1r3 = 0.0;
		double c2r3 = 0.0;
		double c3r3 = 1.0;

		result = new Matrix4D(r.getColumn0Row0(), r.getColumn1Row0(), r.getColumn2Row0(), c3r0, r.getColumn0Row1(), r.getColumn1Row1(), r.getColumn2Row1(), c3r1, r.getColumn0Row2(),
				r.getColumn1Row2(), r.getColumn2Row2(), c3r2, c0r3, c1r3, c2r3, c3r3);

		return result;
	}

	public Matrix4D transpose() {
		return new Matrix4D(getColumn0Row0(), getColumn0Row1(), getColumn0Row2(), getColumn0Row3(), getColumn1Row0(), getColumn1Row1(), getColumn1Row2(), getColumn1Row3(), getColumn2Row0(),
				getColumn2Row1(), getColumn2Row2(), getColumn2Row3(), getColumn3Row0(), getColumn3Row1(), getColumn3Row2(), getColumn3Row3());
	}

	public static Matrix4D createPerspectiveFieldOfView(double fovy, double aspect, double zNear, double zFar) {
		if (fovy <= 0.0 || fovy > Math.PI) {
			throw new IllegalArgumentException("fovy must be in [0, PI).");
		}

		if (aspect <= 0.0) {
			throw new IllegalArgumentException("aspect must be greater than zero.");
		}

		if (zNear <= 0.0) {
			throw new IllegalArgumentException("zNear must be greater than zero.");
		}

		if (zFar <= 0.0) {
			throw new IllegalArgumentException("zFar must be greater than zero.");
		}

		double bottom = Math.tan(fovy * 0.5);
		double f = 1.0 / bottom;

		return new Matrix4D(f / aspect, 0.0, 0.0, 0.0, 0.0, f, 0.0, 0.0, 0.0, 0.0, (zFar + zNear) / (zNear - zFar), (2.0 * zFar * zNear) / (zNear - zFar), 0.0, 0.0, -1.0, 0.0);
	}

	/** 
	 * In computer graphics, one of the most common matrices used for orthographic projection can be defined by a 6-tuple, (left, right, bottom, top, near, far), which defines the clipping planes. 
	 * These planes form a box with the minimum corner at (left, bottom, -near) and the maximum corner at (right, top, -far).
	 * 
	 * The box is translated so that its center is at the origin, then it is scaled to the unit cube which is defined by having a minimum corner at (-1,-1,-1) and a maximum corner at (1,1,1).
	 */
	public static Matrix4D createOrthographicOffCenter(double left, double right, double bottom, double top, double zNear, double zFar) {
		double a = 1.0 / (right - left);
		double b = 1.0 / (top - bottom);
		double c = 1.0 / (zFar - zNear);

		double tx = -(right + left) * a;
		double ty = -(top + bottom) * b;
		double tz = -(zFar + zNear) * c;

		return new Matrix4D(2.0 * a, 0.0, 0.0, tx, 0.0, 2.0 * b, 0.0, ty, 0.0, 0.0, -2.0 * c, tz, 0.0, 0.0, 0.0, 1.0);
	}

	public static Matrix4D lookAt(Vector3D eye, Vector3D target, Vector3D up) {
		Vector3D f = (target.subtract(eye)).normalize();
		Vector3D s = f.cross(up).normalize();
		Vector3D u = s.cross(f).normalize();

		Matrix4D rotation = new Matrix4D(s.getX(), s.getY(), s.getZ(), 0.0, u.getX(), u.getY(), u.getZ(), 0.0, -f.getX(), -f.getY(), -f.getZ(), 0.0, 0.0, 0.0, 0.0, 1.0);
		Matrix4D translation = new Matrix4D(1.0, 0.0, 0.0, -eye.getX(), 0.0, 1.0, 0.0, -eye.getY(), 0.0, 0.0, 1.0, -eye.getZ(), 0.0, 0.0, 0.0, 1.0);
		return rotation.multiply(translation);
	}

	public Matrix4D multiply(Matrix4D other) {
		if (other == null) {
			throw new IllegalArgumentException("other");
		}

		double[] leftValues = getReadOnlyColumnMajorValues();
		double[] rightValues = other.getReadOnlyColumnMajorValues();

		double col0row0 = leftValues[0] * rightValues[0] + leftValues[4] * rightValues[1] + leftValues[8] * rightValues[2] + leftValues[12] * rightValues[3];
		double col0row1 = leftValues[1] * rightValues[0] + leftValues[5] * rightValues[1] + leftValues[9] * rightValues[2] + leftValues[13] * rightValues[3];
		double col0row2 = leftValues[2] * rightValues[0] + leftValues[6] * rightValues[1] + leftValues[10] * rightValues[2] + leftValues[14] * rightValues[3];
		double col0row3 = leftValues[3] * rightValues[0] + leftValues[7] * rightValues[1] + leftValues[11] * rightValues[2] + leftValues[15] * rightValues[3];

		double col1row0 = leftValues[0] * rightValues[4] + leftValues[4] * rightValues[5] + leftValues[8] * rightValues[6] + leftValues[12] * rightValues[7];
		double col1row1 = leftValues[1] * rightValues[4] + leftValues[5] * rightValues[5] + leftValues[9] * rightValues[6] + leftValues[13] * rightValues[7];
		double col1row2 = leftValues[2] * rightValues[4] + leftValues[6] * rightValues[5] + leftValues[10] * rightValues[6] + leftValues[14] * rightValues[7];
		double col1row3 = leftValues[3] * rightValues[4] + leftValues[7] * rightValues[5] + leftValues[11] * rightValues[6] + leftValues[15] * rightValues[7];

		double col2row0 = leftValues[0] * rightValues[8] + leftValues[4] * rightValues[9] + leftValues[8] * rightValues[10] + leftValues[12] * rightValues[11];
		double col2row1 = leftValues[1] * rightValues[8] + leftValues[5] * rightValues[9] + leftValues[9] * rightValues[10] + leftValues[13] * rightValues[11];
		double col2row2 = leftValues[2] * rightValues[8] + leftValues[6] * rightValues[9] + leftValues[10] * rightValues[10] + leftValues[14] * rightValues[11];
		double col2row3 = leftValues[3] * rightValues[8] + leftValues[7] * rightValues[9] + leftValues[11] * rightValues[10] + leftValues[15] * rightValues[11];

		double col3row0 = leftValues[0] * rightValues[12] + leftValues[4] * rightValues[13] + leftValues[8] * rightValues[14] + leftValues[12] * rightValues[15];
		double col3row1 = leftValues[1] * rightValues[12] + leftValues[5] * rightValues[13] + leftValues[9] * rightValues[14] + leftValues[13] * rightValues[15];
		double col3row2 = leftValues[2] * rightValues[12] + leftValues[6] * rightValues[13] + leftValues[10] * rightValues[14] + leftValues[14] * rightValues[15];
		double col3row3 = leftValues[3] * rightValues[12] + leftValues[7] * rightValues[13] + leftValues[11] * rightValues[14] + leftValues[15] * rightValues[15];

		return new Matrix4D(col0row0, col1row0, col2row0, col3row0, col0row1, col1row1, col2row1, col3row1, col0row2, col1row2, col2row2, col3row2, col0row3, col1row3, col2row3, col3row3);
	}

	public Vector4D multiply(Vector4D vector) {

		double[] values = getReadOnlyColumnMajorValues();

		double x = values[0] * vector.getX() + values[4] * vector.getY() + values[8] * vector.getZ() + values[12] * vector.getW();
		double y = values[1] * vector.getX() + values[5] * vector.getY() + values[9] * vector.getZ() + values[13] * vector.getW();
		double z = values[2] * vector.getX() + values[6] * vector.getY() + values[10] * vector.getZ() + values[14] * vector.getW();
		double w = values[3] * vector.getX() + values[7] * vector.getY() + values[11] * vector.getZ() + values[15] * vector.getW();

		return new Vector4D(x, y, z, w);
	}

	public boolean equals(Matrix4D other) {
		if (other == null) {
			return false;
		}

		for (int i = 0; i < _values.length; ++i) {
			if (_values[i] != other._values[i]) {
				return false;
			}
		}

		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Matrix4D) {
			return equals((Matrix4D) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "Rows: \n(%1$f, %2$f, %3$f, %4$f) \n(%5$f, %6$f, %7$f, %8$f) \n(%9$f, %10$f, %11$f, %12$f) \n(%13$f, %14$f, %15$f, %16$f)", getColumn0Row0(),
				getColumn1Row0(), getColumn2Row0(), getColumn3Row0(), getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn3Row1(), getColumn0Row2(), getColumn1Row2(), getColumn2Row2(),
				getColumn3Row2(), getColumn0Row3(), getColumn1Row3(), getColumn2Row3(), getColumn3Row3());
	}

	@Override
	public int hashCode() {
		int hashCode = Double.valueOf(_values[0]).intValue();

		for (int i = 1; i < _values.length; ++i) {
			hashCode = hashCode ^ Double.valueOf(_values[i]).intValue();
		}

		return hashCode;
	}

	/**
	 * Shame on you if you change elements in this array. This type is supposed
	 * to be immutable.
	 */
	public double[] getReadOnlyColumnMajorValues() {
		return _values;
	}

	public static Matrix4D Identity = new Matrix4D(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0);

	// Column major
	private double[] _values;
}