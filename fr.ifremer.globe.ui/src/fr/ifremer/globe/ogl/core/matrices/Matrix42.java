package fr.ifremer.globe.ogl.core.matrices;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;

public class Matrix42<T extends Number> {

	public static Matrix42<Float> DoubleToFloat(Matrix42<Double> value) {
		return new Matrix42<Float>(Float.class, value.getColumn0Row0().floatValue(), value.getColumn1Row0().floatValue(), value.getColumn2Row0().floatValue(), value.getColumn3Row0().floatValue(),
				value.getColumn0Row1().floatValue(), value.getColumn1Row1().floatValue(), value.getColumn2Row1().floatValue(), value.getColumn3Row1().floatValue());
	}

	@SuppressWarnings("unchecked")
	public Matrix42(Class<T> clazz) {
		_values = (T[]) Array.newInstance(clazz, getNumberOfComponents());
	}

	@SuppressWarnings("unchecked")
	public Matrix42(Class<T> clazz, T value) {
		_values = (T[]) Array.newInstance(clazz, getNumberOfComponents());
		Arrays.fill(_values, value);
	}

	@SuppressWarnings("unchecked")
	public Matrix42(Class<T> clazz, T column0Row0, T column1Row0, T column2Row0, T column3Row0, T column0Row1, T column1Row1, T column2Row1, T column3Row1) {
		_values = (T[]) Array.newInstance(clazz, getNumberOfComponents());
		_values[0] = column0Row0;
		_values[1] = column0Row1;
		_values[2] = column1Row0;
		_values[3] = column1Row1;
		_values[4] = column2Row0;
		_values[5] = column2Row1;
		_values[6] = column3Row0;
		_values[7] = column3Row1;
	}

	public boolean equals(Matrix42<T> other) {
		if (other == null) {
			return false;
		}

		for (int i = 0; i < _values.length; ++i) {
			if (!_values[i].equals(other._values[i])) {
				return false;
			}
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Matrix42<?>) {
			return equals((Matrix42<T>) obj);
		}
		return false;
	}

	public T getColumn0Row0() {
		return _values[0];
	}

	public T getColumn0Row1() {
		return _values[1];
	}

	public T getColumn1Row0() {
		return _values[2];
	}

	public T getColumn1Row1() {
		return _values[3];
	}

	public T getColumn2Row0() {
		return _values[4];
	}

	public T getColumn2Row1() {
		return _values[5];
	}

	public T getColumn3Row0() {
		return _values[6];
	}

	public T getColumn3Row1() {
		return _values[7];
	}

	public int getNumberOfComponents() {
		return 8;
	}

	/**
	 * Shame on you if you change elements in this array. This type is supposed
	 * to be immutable.
	 */
	public T[] getReadOnlyColumnMajorValues() {
		return _values;
	}

	@Override
	public int hashCode() {
		int hashCode = _values[0].intValue();

		for (int i = 1; i < _values.length; ++i) {
			hashCode = hashCode ^ _values[i].intValue();
		}

		return hashCode;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "Rows: \n({0:n}, {1:n}, {2:n}, {3:n}) \n({4:n}, {5:n}, {6:n}, {7:n})", getColumn0Row0(), getColumn1Row0(), getColumn2Row0(), getColumn3Row0(),
				getColumn0Row1(), getColumn1Row1(), getColumn2Row1(), getColumn3Row1());
	}

	// Column major
	private T[] _values;
}