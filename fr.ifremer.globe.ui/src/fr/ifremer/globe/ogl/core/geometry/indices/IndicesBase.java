package fr.ifremer.globe.ogl.core.geometry.indices;

/**
 * <p>
 * Description : IndicesBase.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class IndicesBase {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER = Logger.getLogger(IndicesBase.class);

	protected IndicesBase(IndicesType type) {
		_type = type;
	}

	public IndicesType getDatatype() {
		return _type;
	}

	private IndicesType _type;
}
