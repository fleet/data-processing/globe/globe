package fr.ifremer.globe.ogl.core.geometry.vertexattributes;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import com.jogamp.common.nio.Buffers;

import fr.ifremer.globe.ogl.core.vectors.Vector3F;

public class VertexAttributeFloatVector3 extends VertexAttribute<Vector3F> {

	public VertexAttributeFloatVector3(String name) {
		super(name, VertexAttributeType.FloatVector3);
	}

	public VertexAttributeFloatVector3(String name, int capacity) {
		super(name, VertexAttributeType.FloatVector3, capacity);
	}

	@Override
	public ByteBuffer getValuesBuffer() {
		int size = getValues().size();
		FloatBuffer buffer = Buffers.newDirectFloatBuffer(size * 3);

		Vector3F v;
		for (int i = 0; i < size; i++) {
			v = getValues().get(i);
			buffer.put(v.getX());
			buffer.put(v.getY());
		}

		buffer.rewind();
		ByteBuffer result = Buffers.copyFloatBufferAsByteBuffer(buffer);
		return result;
	}
}