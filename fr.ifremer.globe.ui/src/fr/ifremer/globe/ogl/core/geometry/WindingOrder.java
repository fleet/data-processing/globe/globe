package fr.ifremer.globe.ogl.core.geometry;

/**
 * <p>
 * Description : WindingOrder.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public enum WindingOrder {
	Clockwise, Counterclockwise
}
