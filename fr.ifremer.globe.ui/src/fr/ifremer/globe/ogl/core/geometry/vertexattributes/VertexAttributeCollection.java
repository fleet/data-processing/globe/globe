package fr.ifremer.globe.ogl.core.geometry.vertexattributes;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * Description : VertexAttributeCollection.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class VertexAttributeCollection implements Collection<VertexAttribute<?>> {

	@Override
	public boolean add(VertexAttribute<?> vertexAttribute) {
		m_collection.put(vertexAttribute.getName(), vertexAttribute);
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends VertexAttribute<?>> c) {
		boolean result = true;

		for (VertexAttribute<?> vertexAttribute : c) {
			result &= add(vertexAttribute);
		}

		return result;
	}

	@Override
	public void clear() {
		m_collection.clear();
	}

	@Override
	public boolean contains(Object o) {
		boolean result = false;

		if (o instanceof VertexAttribute<?>) {
			result = m_collection.containsValue(o);
		} else {
			result = m_collection.containsKey(o);
		}

		return result;
	}

	public boolean contains(String vertexAttributeName) {
		return m_collection.containsKey(vertexAttributeName);
	}

	public boolean contains(VertexAttribute<?> vertexAttribute) {
		return contains(vertexAttribute.getName());
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		boolean result = true;

		for (Object object : c) {
			result &= contains(object);
		}

		return result;
	}

	public void copyTo(VertexAttribute<?>[] array) {
		Collection<VertexAttribute<?>> values = m_collection.values();
		values.toArray(array);
	}

	public VertexAttribute<?> get(String vertexAttributeName) {
		return m_collection.get(vertexAttributeName);
	}

	@Override
	public boolean isEmpty() {
		return m_collection.isEmpty();
	}

	@Override
	public Iterator<VertexAttribute<?>> iterator() {
		return m_collection.values().iterator();
	}

	@Override
	public boolean remove(Object o) {
		boolean result = false;

		if (o instanceof VertexAttribute<?>) {
			result = (m_collection.remove(((VertexAttribute<?>) o).getName()) != null);
		} else {
			result = (m_collection.remove(o) != null);
		}

		return result;
	}

	public boolean remove(String vertexAttributeName) {
		return (m_collection.remove(vertexAttributeName) != null);
	}

	public boolean remove(VertexAttribute<?> item) {
		return remove(item.getName());
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean result = false;

		for (Object object : c) {
			result |= remove(object);
		}

		return result;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		boolean result = false;

		Set<String> keys = m_collection.keySet();
		for (String key : keys) {
			if (!contains(key)) {
				result |= remove(key);
			}
		}
		Collection<VertexAttribute<?>> values = m_collection.values();
		for (VertexAttribute<?> value : values) {
			if (!contains(value)) {
				result |= remove(value);
			}
		}

		return result;
	}

	@Override
	public int size() {
		return m_collection.size();
	}

	@Override
	public Object[] toArray() {
		return m_collection.values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return m_collection.values().toArray(a);
	}

	private Map<String, VertexAttribute<?>> m_collection = new HashMap<String, VertexAttribute<?>>();

}
