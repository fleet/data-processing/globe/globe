package fr.ifremer.globe.ogl.core.geometry.vertexattributes;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import com.jogamp.common.nio.Buffers;

import fr.ifremer.globe.ogl.core.vectors.Vector2F;

public class VertexAttributeFloatVector2 extends VertexAttribute<Vector2F> {

	public VertexAttributeFloatVector2(String name) {
		super(name, VertexAttributeType.FloatVector2);
	}

	public VertexAttributeFloatVector2(String name, int capacity) {
		super(name, VertexAttributeType.FloatVector2, capacity);
	}

	@Override
	public ByteBuffer getValuesBuffer() {
		int size = getValues().size();
		FloatBuffer buffer = Buffers.newDirectFloatBuffer(size * 2);

		Vector2F v;
		for (int i = 0; i < size; i++) {
			v = getValues().get(i);
			buffer.put(v.getX());
			buffer.put(v.getY());
		}

		buffer.rewind();
		ByteBuffer result = Buffers.copyFloatBufferAsByteBuffer(buffer);
		return result;
	}
}