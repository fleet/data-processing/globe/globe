package fr.ifremer.globe.ogl.core.geometry.indices;

import java.util.ArrayList;
import java.util.List;

public class IndicesInt extends IndicesBase {
	public IndicesInt() {
		super(IndicesType.Int);
		_values = new ArrayList<Integer>();
	}

	public IndicesInt(int capacity) {
		super(IndicesType.Int);
		_values = new ArrayList<Integer>(capacity);
	}

	public List<Integer> getValues() {
		return _values;
	}

	public void addTriangle(TriangleIndicesInt triangle) {
		_values.add(triangle.getI0());
		_values.add(triangle.getI1());
		_values.add(triangle.getI2());
	}

	private List<Integer> _values;
}