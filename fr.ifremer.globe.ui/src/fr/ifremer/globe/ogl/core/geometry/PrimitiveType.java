package fr.ifremer.globe.ogl.core.geometry;

/**
 * <p>
 * Description : PrimitiveType.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public enum PrimitiveType {
	Points, Lines, LineLoop, LineStrip, Triangles, TriangleStrip, TriangleFan, LinesAdjacency, LineStripAdjacency, TrianglesAdjacency, TriangleStripAdjacency, Quads
}
