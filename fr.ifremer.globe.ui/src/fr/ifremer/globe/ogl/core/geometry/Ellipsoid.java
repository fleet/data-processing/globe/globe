package fr.ifremer.globe.ogl.core.geometry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.ifremer.globe.ogl.core.Geodetic2D;
import fr.ifremer.globe.ogl.core.Geodetic3D;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;

/**
 * <p>
 * Description :
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Ellipsoid {

	public static Ellipsoid Wgs84 = new Ellipsoid(6378137.0, 6378137.0, 6356752.314245);
	public static Ellipsoid ScaledWgs84 = new Ellipsoid(1.0, 1.0, 6356752.314245 / 6378137.0);
	public static Ellipsoid UnitSphere = new Ellipsoid(1.0, 1.0, 1.0);

	public Ellipsoid(double x, double y, double z) {
		this(new Vector3D(x, y, z));
	}

	public Ellipsoid(Vector3D radii) {
		if ((radii.getX() <= 0.0) || (radii.getY() <= 0.0) || (radii.getZ() <= 0.0)) {
			throw new IllegalArgumentException("radii");
		}

		_radii = radii;
		_radiiSquared = new Vector3D(radii.getX() * radii.getX(), radii.getY() * radii.getY(), radii.getZ() * radii.getZ());
		_radiiToTheFourth = new Vector3D(_radiiSquared.getX() * _radiiSquared.getX(), _radiiSquared.getY() * _radiiSquared.getY(), _radiiSquared.getZ() * _radiiSquared.getZ());
		_oneOverRadiiSquared = new Vector3D(1.0 / (radii.getX() * radii.getX()), 1.0 / (radii.getY() * radii.getY()), 1.0 / (radii.getZ() * radii.getZ()));
	}

	public static Vector3D getCentricSurfaceNormal(Vector3D positionOnEllipsoid) {
		return positionOnEllipsoid.normalize();
	}

	public Vector3D getGeodeticSurfaceNormal(Vector3D positionOnEllipsoid) {
		return (positionOnEllipsoid.multiplyComponents(_oneOverRadiiSquared)).normalize();
	}

	public Vector3D getGeodeticSurfaceNormal(Geodetic3D geodetic) {
		double cosLatitude = Math.cos(geodetic.getLatitude());

		return new Vector3D(cosLatitude * Math.cos(geodetic.getLongitude()), cosLatitude * Math.sin(geodetic.getLongitude()), Math.sin(geodetic.getLatitude()));
	}

	public Vector3D getRadii() {
		return _radii;
	}

	public Vector3D getRadiiSquared() {
		return _radiiSquared;
	}

	public Vector3D getOneOverRadiiSquared() {
		return _oneOverRadiiSquared;
	}

	public double getMinimumRadius() {
		return Math.min(_radii.getX(), Math.min(_radii.getY(), _radii.getZ()));
	}

	public double getMaximumRadius() {
		return Math.max(_radii.getX(), Math.max(_radii.getY(), _radii.getZ()));
	}

	public double[] getIntersections(Vector3D origin, Vector3D direction) {
		direction.normalize();

		// By laborious algebraic manipulation....
		double a = direction.getX() * direction.getX() * _oneOverRadiiSquared.getX() + direction.getY() * direction.getY() * _oneOverRadiiSquared.getY() + direction.getZ() * direction.getZ()
				* _oneOverRadiiSquared.getZ();
		double b = 2.0 * (origin.getX() * direction.getX() * _oneOverRadiiSquared.getX() + origin.getY() * direction.getY() * _oneOverRadiiSquared.getY() + origin.getZ() * direction.getZ()
				* _oneOverRadiiSquared.getZ());
		double c = origin.getX() * origin.getX() * _oneOverRadiiSquared.getX() + origin.getY() * origin.getY() * _oneOverRadiiSquared.getY() + origin.getZ() * origin.getZ()
				* _oneOverRadiiSquared.getZ() - 1.0;

		// Solve the quadratic equation: ax^2 + bx + c = 0.
		// Algorithm is from Wikipedia's "Quadratic equation" topic, and
		// Wikipedia
		// credits
		// Numerical Recipes in C, section 5.6: "Quadratic and Cubic Equations"
		double discriminant = b * b - 4 * a * c;
		if (discriminant < 0.0) {
			// no intersections
			return new double[0];
		} else if (discriminant == 0.0) {
			// one intersection at a tangent point
			return new double[] { -0.5 * b / a };
		}

		double t = -0.5 * (b + (b > 0.0 ? 1.0 : -1.0) * Math.sqrt(discriminant));
		double root1 = t / a;
		double root2 = c / t;

		// Two intersections - return the smallest first.
		if (root1 < root2) {
			return new double[] { root1, root2 };
		} else {
			return new double[] { root2, root1 };
		}
	}

	public Vector3D toVector3D(Geodetic2D geodetic) {
		return toVector3D(new Geodetic3D(geodetic.getLongitude(), geodetic.getLatitude(), 0.0));
	}

	public Vector3D toVector3D(Geodetic3D geodetic) {
		Vector3D n = getGeodeticSurfaceNormal(geodetic);
		Vector3D k = _radiiSquared.multiplyComponents(n);
		double gamma = Math.sqrt((k.getX() * n.getX()) + (k.getY() * n.getY()) + (k.getZ() * n.getZ()));

		Vector3D rSurface = k.divide(gamma);
		return rSurface.add(n.multiply(geodetic.getHeight()));
	}

	public Collection<Geodetic3D> toGeodetic3D(Iterable<Vector3D> positions) {
		if (positions == null) {
			throw new IllegalArgumentException("positions");
		}

		List<Geodetic3D> geodetics = new ArrayList<Geodetic3D>();

		for (Vector3D position : positions) {
			geodetics.add(toGeodetic3D(position));
		}

		return geodetics;
	}

	public Collection<Geodetic2D> toGeodetic2D(Iterable<Vector3D> positions) {
		if (positions == null) {
			throw new IllegalArgumentException("positions");
		}

		List<Geodetic2D> geodetics = new ArrayList<Geodetic2D>();

		for (Vector3D position : positions) {
			geodetics.add(toGeodetic2D(position));
		}

		return geodetics;
	}

	public Geodetic2D toGeodetic2D(Vector3D positionOnEllipsoid) {
		Vector3D n = getGeodeticSurfaceNormal(positionOnEllipsoid);
		return new Geodetic2D(Math.atan2(n.getY(), n.getX()), Math.asin(n.getZ() / n.getMagnitude()));
	}

	public Geodetic3D toGeodetic3D(Vector3D position) {
		Vector3D p = scaleToGeodeticSurface(position);
		Vector3D h = position.subtract(p);
		double height = Math.signum(h.dot(position)) * h.getMagnitude();
		return new Geodetic3D(toGeodetic2D(p), height);
	}

	public Vector3D scaleToGeodeticSurface(Vector3D position) {
		double beta = 1.0 / Math.sqrt((position.getX() * position.getX()) * _oneOverRadiiSquared.getX() + (position.getY() * position.getY()) * _oneOverRadiiSquared.getY()
				+ (position.getZ() * position.getZ()) * _oneOverRadiiSquared.getZ());
		double n = new Vector3D(beta * position.getX() * _oneOverRadiiSquared.getX(), beta * position.getY() * _oneOverRadiiSquared.getY(), beta * position.getZ() * _oneOverRadiiSquared.getZ())
				.getMagnitude();
		double alpha = (1.0 - beta) * (position.getMagnitude() / n);

		double x2 = position.getX() * position.getX();
		double y2 = position.getY() * position.getY();
		double z2 = position.getZ() * position.getZ();

		double da = 0.0;
		double db = 0.0;
		double dc = 0.0;

		double s = 0.0;
		double dSdA = 1.0;

		do {
			alpha -= (s / dSdA);

			da = 1.0 + (alpha * _oneOverRadiiSquared.getX());
			db = 1.0 + (alpha * _oneOverRadiiSquared.getY());
			dc = 1.0 + (alpha * _oneOverRadiiSquared.getZ());

			double da2 = da * da;
			double db2 = db * db;
			double dc2 = dc * dc;

			double da3 = da * da2;
			double db3 = db * db2;
			double dc3 = dc * dc2;

			s = x2 / (_radiiSquared.getX() * da2) + y2 / (_radiiSquared.getY() * db2) + z2 / (_radiiSquared.getZ() * dc2) - 1.0;

			dSdA = -2.0 * (x2 / (_radiiToTheFourth.getX() * da3) + y2 / (_radiiToTheFourth.getY() * db3) + z2 / (_radiiToTheFourth.getZ() * dc3));
		} while (Math.abs(s) > 1e-10);

		return new Vector3D(position.getX() / da, position.getY() / db, position.getZ() / dc);
	}

	public Vector3D scaleToGeocentricSurface(Vector3D position) {
		double beta = 1.0 / Math.sqrt((position.getX() * position.getX()) * _oneOverRadiiSquared.getX() + (position.getY() * position.getY()) * _oneOverRadiiSquared.getY()
				+ (position.getZ() * position.getZ()) * _oneOverRadiiSquared.getZ());

		return position.multiply(beta);
	}

	public List<Vector3D> computeCurve(Vector3D start, Vector3D stop, double granularity) {
		if (granularity <= 0.0) {
			throw new IllegalArgumentException("Granularity must be greater than zero.");
		}

		Vector3D normal = start.cross(stop).normalize();
		double theta = start.getAngleBetween(stop);
		int n = Math.max((int) (theta / granularity) - 1, 0);

		List<Vector3D> positions = new ArrayList<Vector3D>(2 + n);

		positions.add(start);

		for (int i = 1; i <= n; ++i) {
			double phi = (i * granularity);

			positions.add(scaleToGeocentricSurface(start.rotateAroundAxis(normal, phi)));
		}

		positions.add(stop);

		return positions;
	}

	private Vector3D _radii;
	private Vector3D _radiiSquared;
	private Vector3D _radiiToTheFourth;
	private Vector3D _oneOverRadiiSquared;
}
