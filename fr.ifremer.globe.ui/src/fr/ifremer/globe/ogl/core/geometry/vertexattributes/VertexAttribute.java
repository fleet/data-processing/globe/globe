package fr.ifremer.globe.ogl.core.geometry.vertexattributes;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Description : VertexAttribute.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class VertexAttribute<T> {

	protected VertexAttribute(String name, VertexAttributeType type) {
		_name = name;
		_type = type;
		_values = new ArrayList<T>();
	}

	public String getName() {
		return _name;
	}

	public VertexAttributeType getDatatype() {
		return _type;
	}

	protected VertexAttribute(String name, VertexAttributeType type, int capacity) {
		this(name, type);
		_values = new ArrayList<T>(capacity);
	}

	public List<T> getValues() {
		return _values;
	}

	public abstract ByteBuffer getValuesBuffer();

	private List<T> _values;

	private String _name;
	private VertexAttributeType _type;
}
