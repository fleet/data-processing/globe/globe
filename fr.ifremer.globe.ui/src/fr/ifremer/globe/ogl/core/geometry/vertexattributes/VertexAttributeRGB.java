package fr.ifremer.globe.ogl.core.geometry.vertexattributes;

import java.awt.Color;
import java.nio.ByteBuffer;

import com.jogamp.common.nio.Buffers;

public class VertexAttributeRGB extends VertexAttribute<Byte> {
	public VertexAttributeRGB(String name) {
		super(name, VertexAttributeType.UnsignedByte);
	}

	public VertexAttributeRGB(String name, int capacity) {
		super(name, VertexAttributeType.UnsignedByte, capacity * 3);
		if (capacity > Integer.MAX_VALUE / 3) {
			throw new IllegalArgumentException("capacity causes int overflow.");
		}
	}

	@Override
	public ByteBuffer getValuesBuffer() {
		int size = getValues().size();
		ByteBuffer result = Buffers.newDirectByteBuffer(size);

		for (int i = 0; i < size; i++) {
			result.put(getValues().get(i));
		}

		return result;
	}

	public void addColor(Color color) {
		getValues().add((byte) color.getRed());
		getValues().add((byte) color.getGreen());
		getValues().add((byte) color.getBlue());
	}
}