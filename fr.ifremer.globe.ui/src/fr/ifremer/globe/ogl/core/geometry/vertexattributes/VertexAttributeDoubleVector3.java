package fr.ifremer.globe.ogl.core.geometry.vertexattributes;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

import com.jogamp.common.nio.Buffers;

import fr.ifremer.globe.ogl.core.vectors.Vector3D;

public class VertexAttributeDoubleVector3 extends VertexAttribute<Vector3D> {
	public VertexAttributeDoubleVector3(String name) {
		super(name, VertexAttributeType.EmulatedDoubleVector3);
	}

	public VertexAttributeDoubleVector3(String name, int capacity) {
		super(name, VertexAttributeType.EmulatedDoubleVector3, capacity);
	}

	@Override
	public ByteBuffer getValuesBuffer() {
		int size = getValues().size();
		DoubleBuffer buffer = Buffers.newDirectDoubleBuffer(size * 3);

		Vector3D v;
		for (int i = 0; i < size; i++) {
			v = getValues().get(i);
			buffer.put(v.getX());
			buffer.put(v.getY());
			buffer.put(v.getZ());
		}

		buffer.rewind();
		// TODO verifier ce code de migration
		// avant migration
		// ByteBuffer result = Buffers.copyDoubleBufferAsByteBuffer(buffer);

		FloatBuffer fbuffer = Buffers.newDirectFloatBuffer(size * 3);
		Buffers.getFloatBuffer(buffer, fbuffer);
		ByteBuffer result = Buffers.copyFloatBufferAsByteBuffer(fbuffer);

		return result;
	}
}