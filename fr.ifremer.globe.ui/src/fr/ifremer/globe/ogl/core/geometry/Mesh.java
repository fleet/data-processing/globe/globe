package fr.ifremer.globe.ogl.core.geometry;

import fr.ifremer.globe.ogl.core.geometry.indices.IndicesBase;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttributeCollection;

/**
 * <p>
 * Description : Mesh.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Mesh {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER = Logger.getLogger(Mesh.class);

	public Mesh() {
		_attributes = new VertexAttributeCollection();
	}

	public VertexAttributeCollection getAttributes() {
		return _attributes;
	}

	public WindingOrder getFrontFaceWindingOrder() {
		return m_FrontFaceWindingOrder;
	}

	public IndicesBase getIndices() {
		return m_Indices;
	}

	public PrimitiveType getPrimitiveType() {
		return m_PrimitiveType;
	}

	public void setFrontFaceWindingOrder(WindingOrder frontFaceWindingOrder) {
		m_FrontFaceWindingOrder = frontFaceWindingOrder;
	}

	public void setIndices(IndicesBase indices) {
		m_Indices = indices;
	}

	public void setPrimitiveType(PrimitiveType primitiveType) {
		m_PrimitiveType = primitiveType;
	}

	private VertexAttributeCollection _attributes;
	private WindingOrder m_FrontFaceWindingOrder;
	private IndicesBase m_Indices;
	private PrimitiveType m_PrimitiveType;

}
