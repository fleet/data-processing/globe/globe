package fr.ifremer.globe.ogl.core.geometry.indices;

import java.util.Locale;

/**
 * <p>
 * Description : TriangleIndicesInt.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 18 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class TriangleIndicesInt {

	public TriangleIndicesInt(int i0, int i1, int i2) {
		_i0 = i0;
		_i1 = i1;
		_i2 = i2;
	}

	public TriangleIndicesInt(TriangleIndicesInt other) {
		_i0 = other.getI0();
		_i1 = other.getI1();
		_i2 = other.getI2();
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "i0: {0} i1: {1} i2: {2}", _i0, _i1, _i2);
	}

	@Override
	public int hashCode() {
		return Integer.valueOf(_i0).hashCode() ^ Integer.valueOf(_i1).hashCode() ^ Integer.valueOf(_i2).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof TriangleIndicesInt)) {
			return false;
		}

		return equals((TriangleIndicesInt) obj);
	}

	public boolean equals(TriangleIndicesInt other) {
		return (_i0 == other.getI0()) && (_i1 == other.getI1()) && (_i2 == other.getI2());
	}

	public int getI0() {
		return _i0;
	}

	public int getI1() {
		return _i1;
	}

	public int getI2() {
		return _i2;
	}

	private int _i0;
	private int _i1;
	private int _i2;
}
