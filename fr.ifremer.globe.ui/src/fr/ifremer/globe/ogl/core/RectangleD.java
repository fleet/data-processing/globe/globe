package fr.ifremer.globe.ogl.core;

import java.util.Locale;

import fr.ifremer.globe.ogl.core.vectors.Vector2D;

/**
 * <p>
 * Description : RectangleD
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 18 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class RectangleD {

	public RectangleD(Vector2D lowerLeft, Vector2D upperRight) {
		_lowerLeft = lowerLeft;
		_upperRight = upperRight;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RectangleD)) {
			return false;
		}

		return isEqual((RectangleD) obj);
	}

	public boolean isEqual(RectangleD other) {
		return (_lowerLeft == other._lowerLeft) && (_upperRight == other._upperRight);
	}

	public Vector2D getLowerLeft() {
		return _lowerLeft;
	}

	public Vector2D getUpperRight() {
		return _upperRight;
	}

	@Override
	public int hashCode() {
		return _lowerLeft.hashCode() ^ _upperRight.hashCode();
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1})", _lowerLeft.toString(), _upperRight.toString());
	}

	private Vector2D _lowerLeft;
	private Vector2D _upperRight;
}
