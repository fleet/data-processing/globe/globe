package fr.ifremer.globe.ogl.core.message;

public class MessageInfo {
	public MessageInfo(Action<Object> callback, Object message, Object done) {
		m_Callback = callback;
		m_Message = message;
		m_Done = done;
	}

	public Action<Object> getCallback() {
		return m_Callback;
	}

	public void setCallback(Action<Object> callback) {
		m_Callback = callback;
	}

	public Object getMessage() {
		return m_Message;
	}

	public void setMessage(Object message) {
		m_Message = message;
	}

	public Object getDone() {
		return m_Done;
	}

	public void setDone(Object done) {
		m_Done = done;
	}

	private Action<Object> m_Callback = null;
	private Object m_Message = null;
	private Object m_Done = null;
}