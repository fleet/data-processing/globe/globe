package fr.ifremer.globe.ogl.core.message;

/**
 * <p>
 * Description : Action.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 22 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public interface Action<T> {
	void actionPerformed(T obj);
}
