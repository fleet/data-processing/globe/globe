package fr.ifremer.globe.ogl.core.message;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * A message queue for passing messages between threads. Messages can be simple
 * objects that are processed by the MessageReceived event, or they can be
 * delegates which are invoked in the thread that is processing the queue. The
 * queue can be processed in a dedicated thread by calling StartInAnotherThread,
 * or in an existing thread by calling StartInCurrentThread. Finally, the
 * current contents of the queue can be executed just once by calling
 * ProcessQueue.
 */
public class MessageQueue implements Runnable {
	private final class StopQueueAction implements Action<Object> {

		@Override
		public void actionPerformed(Object obj) {
			stopQueue(obj);
		}

	}

	/**
	 * Calls Terminate.
	 */
	public void dispose() {
		terminate();
	}

	/**
	 * Posts a delegate to the queue. This method returns immediately without
	 * waiting for the delegate to be invoked.
	 * 
	 * @param callback
	 *            The callback to invoke when the message is processed.
	 * @param userData
	 *            Optional data to pass to the callback when it is invoked.
	 */
	public void post(Action<Object> callback, Object userData) {
		synchronized (_queue) {
			_queue.add(new MessageInfo(callback, userData, null));
			_queue.notify();
		}
	}

	/**
	 * Posts a message to the queue. This method returns immediately without
	 * waiting for the message to be processed.
	 * 
	 * @param message
	 *            The message to post to the queue.
	 */
	public void post(Object message) {
		synchronized (_queue) {
			_queue.add(new MessageInfo(null, message, null));
			_queue.notify();
		}
	}

	public void addListener(MessageQueueListener listener) {
		m_Listeners.add(listener);
	}

	public void removeListener(MessageQueueListener listener) {
		m_Listeners.remove(listener);
	}

	/**
	 * Processes messages currently in the queue using the calling thread. This
	 * method returns as soon as all messages currently in the queue have been
	 * processed.
	 */
	public void processQueue() {
		List<MessageInfo> current = null;

		synchronized (_queue) {
			if (_state != State.Stopped) {
				throw new IllegalStateException("The MessageQueue is already running.");
			}

			if (_queue.size() > 0) {
				_state = State.Running;
				current = new ArrayList<MessageInfo>(_queue);
				_queue.clear();
			}
		}

		if (current != null) {
			try {
				processCurrentQueue(current);
			} finally {
				synchronized (_queue) {
					_state = State.Stopped;
				}
			}
		}
	}

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(MessageQueue.class);

	@Override
	public void run() {
		try {
			List<MessageInfo> current = new ArrayList<MessageInfo>();

			do {
				synchronized (_queue) {
					if (_queue.size() > 0) {
						current.addAll(_queue);
						_queue.clear();
					} else {
						try {
							_queue.wait();
						} catch (InterruptedException e) {
							LOGGER.error(e, e);
						}

						current.addAll(_queue);
						_queue.clear();
					}
				}

				processCurrentQueue(current);
				current.clear();
			} while (_state == State.Running);
		} finally {
			synchronized (_queue) {
				_state = State.Stopped;
			}
		}
	}

	/**
	 * Sends a delegate to the queue. This method waits for the delegate to be
	 * invoked in the queue thread before returning. Calling this message from
	 * the queue thread itself will result in a deadlock.
	 * 
	 * @param callback
	 *            The callback to invoke when the message is processed.
	 * @param userData
	 *            Optional data to pass to the callback when it is invoked.
	 */
	public void send(Action<Object> callback, Object userData) {
		MessageInfo messageInfo = new MessageInfo(callback, userData, new Object());
		synchronized (messageInfo.getDone()) {
			synchronized (_queue) {
				_queue.add(messageInfo);
				_queue.notify();
			}
			try {
				messageInfo.getDone().wait();
			} catch (InterruptedException e) {
				LOGGER.error(e, e);
			}
		}
	}

	/**
	 * Sends a message to the queue. This method waits for the delegate to be
	 * invoked in the queue thread before returning. Calling this message from
	 * the queue thread itself will result in a deadlock.
	 * 
	 * @param message
	 *            The message to post to the queue.
	 */
	public void send(Object message) {
		MessageInfo messageInfo = new MessageInfo(null, message, new Object());
		synchronized (messageInfo.getDone()) {
			synchronized (_queue) {
				_queue.add(messageInfo);
				_queue.notify();
			}
			try {
				messageInfo.getDone().wait();
			} catch (InterruptedException e) {
				LOGGER.error(e, e);
			}
		}
	}

	/**
	 * Starts processing the queue in a separate thread. This method returns
	 * immediately. The thread created by this method will continue running
	 * until Terminate is called.
	 */
	public void startInAnotherThread() {
		synchronized (_queue) {
			if (_state != State.Stopped) {
				throw new IllegalStateException("The MessageQueue is already running.");
			}
			_state = State.Running;
		}

		Thread thread = new Thread(this, "Message Queue Thread");
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();
	}

	/**
	 * Starts processing the queue in the current thread. This method does not
	 * return until Terminate is called.
	 */
	public void startInCurrentThread() {
		synchronized (_queue) {
			if (_state != State.Stopped) {
				throw new IllegalStateException("The MessageQueue is already running.");
			}
			_state = State.Running;
		}

		run();
	}

	/**
	 * Stops queue processing started by StartInCurrentThread
	 * StartInAnotherThread, or ProcessQueue. This method returns immediately
	 * without waiting for the message queue to stop. To wait for the message
	 * queue to stop, call TerminateAndWait instead. If the message queue is not
	 * running when this method is called, a "stop" message will be queued such
	 * that the message queue will be stopped when it starts processing messages
	 * again.
	 */
	public void terminate() {
		post(new StopQueueAction(), null);
	}

	/**
	 * Stops queue processing started by StartInCurrentThread,
	 * StartInAnotherThread, or ProcessQueue. This method does not return until
	 * the message queue has stopped. To signal the message queue to terminate
	 * without waiting, call Terminate instead. If the message queue is not
	 * running when this method is called, a "stop" message will be queued such
	 * that the message queue will be stopped when it starts processing messages
	 * again, and the calling thread will be blocked until that happens.
	 */
	public void terminateAndWait() {
		send(new StopQueueAction(), null);
	}

	/**
	 * Blocks the calling thread until a message is waiting in the queue. This
	 * message should only be called on queues for which messages are processed
	 * explicitly with a call to ProcessQueue.
	 */
	public void waitForMessage() {
		synchronized (_queue) {
			while (_queue.size() == 0) {
				try {
					_queue.wait();
				} catch (InterruptedException e) {
					LOGGER.error(e, e);
				}
			}
		}
	}

	protected void fireMessageReceived(Object message) {
		for (MessageQueueListener listener : m_Listeners) {
			listener.messageReceived(this, message);
		}
	}

	private void processCurrentQueue(List<MessageInfo> currentQueue) {
		for (int i = 0; i < currentQueue.size(); ++i) {
			if (_state == State.Stopping) {
				// Push the remainder of 'current' back into '_queue'.
				synchronized (_queue) {

					for (int j = i; j < currentQueue.size(); j++) {
						currentQueue.set(j - i, currentQueue.get(j));
					}
					for (int j = 0; j < i; j++) {
						currentQueue.remove(currentQueue.size() - 1);
					}

					_queue.addAll(0, currentQueue);
				}
				break;
			}
			processMessage(currentQueue.get(i));
		}
	}

	private void processMessage(MessageInfo message) {
		if (message.getCallback() != null) {
			message.getCallback().actionPerformed(message.getMessage());
		} else {

			fireMessageReceived(message.getMessage());
		}

		if (message.getDone() != null) {
			synchronized (message.getDone()) {
				message.getDone().notify();
			}
		}
	}

	private void stopQueue(Object userData) {
		_state = State.Stopping;
	}

	private List<MessageInfo> _queue = new ArrayList<MessageInfo>();

	private State _state = State.Stopped;
	private Set<MessageQueueListener> m_Listeners = new HashSet<MessageQueueListener>();
}