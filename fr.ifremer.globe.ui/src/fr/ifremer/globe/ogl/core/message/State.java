package fr.ifremer.globe.ogl.core.message;

public enum State {
	Stopped, Running, Stopping
}