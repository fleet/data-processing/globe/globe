package fr.ifremer.globe.ogl.core.message;

/**
 * <p>
 * Description : MessageQueueListener.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 22 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public interface MessageQueueListener {

	/**
	 * Raised when a message is received by the message queue. This event is
	 * raised in the thread that is handling messages for the queue.
	 */
	void messageReceived(Object sender, Object message);

}
