package fr.ifremer.globe.ogl.core;

/**
 * <p>
 * Description : Geodetic coordinates in lat/lon.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Geodetic2D {

	public Geodetic2D(double longitude, double latitude) {
		_longitude = longitude;
		_latitude = latitude;
	}

	public Geodetic2D(Geodetic3D geodetic3D) {
		_longitude = geodetic3D.getLongitude();
		_latitude = geodetic3D.getLatitude();
	}

	public boolean equals(Geodetic2D other) {
		return _longitude == other._longitude && _latitude == other._latitude;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Geodetic2D) {
			return equals((Geodetic2D) obj);
		}

		return false;
	}

	public boolean equalsEpsilon(Geodetic2D other, double epsilon) {
		return (Math.abs(_longitude - other._longitude) <= epsilon) && (Math.abs(_latitude - other._latitude) <= epsilon);
	}

	public double getLatitude() {
		return _latitude;
	}

	public double getLongitude() {
		return _longitude;
	}

	@Override
	public int hashCode() {
		return Double.valueOf(_longitude).hashCode() ^ Double.valueOf(_latitude).hashCode();
	}

	private double _latitude;
	private double _longitude;
}
