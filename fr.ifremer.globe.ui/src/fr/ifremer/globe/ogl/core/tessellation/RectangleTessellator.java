package fr.ifremer.globe.ogl.core.tessellation;

import java.util.List;

import fr.ifremer.globe.ogl.core.RectangleD;
import fr.ifremer.globe.ogl.core.geometry.Mesh;
import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.core.geometry.WindingOrder;
import fr.ifremer.globe.ogl.core.geometry.indices.IndicesInt;
import fr.ifremer.globe.ogl.core.geometry.indices.TriangleIndicesInt;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttributeFloatVector2;
import fr.ifremer.globe.ogl.core.vectors.Vector2D;
import fr.ifremer.globe.ogl.core.vectors.Vector2F;

public class RectangleTessellator {

	public static Mesh compute(RectangleD rectangle, int numberOfPartitionsX, int numberOfPartitionsY) {
		if (numberOfPartitionsX < 0) {
			throw new IllegalArgumentException("numberOfPartitionsX");
		}

		if (numberOfPartitionsY < 0) {
			throw new IllegalArgumentException("numberOfPartitionsY");
		}

		Mesh mesh = new Mesh();
		mesh.setPrimitiveType(PrimitiveType.Triangles);
		mesh.setFrontFaceWindingOrder(WindingOrder.Counterclockwise);

		int numberOfPositions = (numberOfPartitionsX + 1) * (numberOfPartitionsY + 1);
		VertexAttributeFloatVector2 positionsAttribute = new VertexAttributeFloatVector2("position", numberOfPositions);
		List<Vector2F> positions = positionsAttribute.getValues();
		mesh.getAttributes().add(positionsAttribute);

		int numberOfIndices = (numberOfPartitionsX * numberOfPartitionsY) * 6;
		IndicesInt indices = new IndicesInt(numberOfIndices);
		mesh.setIndices(indices);

		//
		// Positions
		//
		Vector2D lowerLeft = rectangle.getLowerLeft();
		Vector2D toUpperRight = rectangle.getUpperRight().subtract(lowerLeft);

		for (int y = 0; y <= numberOfPartitionsY; ++y) {
			double deltaY = y / (double) numberOfPartitionsY;
			double currentY = lowerLeft.getY() + (deltaY * toUpperRight.getY());

			for (int x = 0; x <= numberOfPartitionsX; ++x) {
				double deltaX = x / (double) numberOfPartitionsX;
				double currentX = lowerLeft.getX() + (deltaX * toUpperRight.getX());
				positions.add(new Vector2D(currentX, currentY).toVector2F());
			}
		}

		//
		// Indices
		//
		int rowDelta = numberOfPartitionsX + 1;
		int i = 0;
		for (int y = 0; y < numberOfPartitionsY; ++y) {
			for (int x = 0; x < numberOfPartitionsX; ++x) {
				indices.addTriangle(new TriangleIndicesInt(i, i + 1, rowDelta + (i + 1)));
				indices.addTriangle(new TriangleIndicesInt(i, rowDelta + (i + 1), rowDelta + i));
				i += 1;
			}
			i += 1;
		}

		return mesh;
	}
}