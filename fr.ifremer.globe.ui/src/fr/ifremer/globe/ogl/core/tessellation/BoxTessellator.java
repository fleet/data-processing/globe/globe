package fr.ifremer.globe.ogl.core.tessellation;

import java.util.List;

import fr.ifremer.globe.ogl.core.geometry.Mesh;
import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.core.geometry.WindingOrder;
import fr.ifremer.globe.ogl.core.geometry.indices.IndicesInt;
import fr.ifremer.globe.ogl.core.geometry.indices.TriangleIndicesInt;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttributeDoubleVector3;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;

public class BoxTessellator {
	public static Mesh compute(Vector3D length) {
		if (length.getX() < 0 || length.getY() < 0 || length.getZ() < 0) {
			throw new IllegalArgumentException("length");
		}

		Mesh mesh = new Mesh();
		mesh.setPrimitiveType(PrimitiveType.Triangles);
		mesh.setFrontFaceWindingOrder(WindingOrder.Counterclockwise);

		VertexAttributeDoubleVector3 positionsAttribute = new VertexAttributeDoubleVector3("position", 8);
		mesh.getAttributes().add(positionsAttribute);

		IndicesInt indices = new IndicesInt(36);
		mesh.setIndices(indices);

		//
		// 8 corner points
		//
		List<Vector3D> positions = positionsAttribute.getValues();

		Vector3D corner = length.multiply(0.5);
		positions.add(new Vector3D(-corner.getX(), -corner.getY(), -corner.getZ()));
		positions.add(new Vector3D(corner.getX(), -corner.getY(), -corner.getZ()));
		positions.add(new Vector3D(corner.getX(), corner.getY(), -corner.getZ()));
		positions.add(new Vector3D(-corner.getX(), corner.getY(), -corner.getZ()));
		positions.add(new Vector3D(-corner.getX(), -corner.getY(), corner.getZ()));
		positions.add(new Vector3D(corner.getX(), -corner.getY(), corner.getZ()));
		positions.add(new Vector3D(corner.getX(), corner.getY(), corner.getZ()));
		positions.add(new Vector3D(-corner.getX(), corner.getY(), corner.getZ()));

		//
		// 6 faces, 2 triangles each
		//
		indices.addTriangle(new TriangleIndicesInt(4, 5, 6)); // Top:
																// plane z =
																// corner.Z
		indices.addTriangle(new TriangleIndicesInt(4, 6, 7));
		indices.addTriangle(new TriangleIndicesInt(1, 0, 3)); // Bottom:
																// plane z =
																// -corner.Z
		indices.addTriangle(new TriangleIndicesInt(1, 3, 2));
		indices.addTriangle(new TriangleIndicesInt(1, 6, 5)); // Side:
																// plane x =
																// corner.X
		indices.addTriangle(new TriangleIndicesInt(1, 2, 6));
		indices.addTriangle(new TriangleIndicesInt(2, 3, 7)); // Side:
																// plane y =
																// corner.Y
		indices.addTriangle(new TriangleIndicesInt(2, 7, 6));
		indices.addTriangle(new TriangleIndicesInt(3, 0, 4)); // Side:
																// plane x =
																// -corner.X
		indices.addTriangle(new TriangleIndicesInt(3, 4, 7));
		indices.addTriangle(new TriangleIndicesInt(0, 1, 5)); // Side:
																// plane y =
																// -corner.Y
		indices.addTriangle(new TriangleIndicesInt(0, 5, 4));

		return mesh;
	}
}