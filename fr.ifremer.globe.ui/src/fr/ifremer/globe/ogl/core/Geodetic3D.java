package fr.ifremer.globe.ogl.core;

/**
 * <p>
 * Description : Geodetic coordinates in lat/lon/height.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Geodetic3D {

	public Geodetic3D(double longitude, double latitude, double height) {
		_longitude = longitude;
		_latitude = latitude;
		_height = height;
	}

	public Geodetic3D(double longitude, double latitude) {
		_longitude = longitude;
		_latitude = latitude;
		_height = 0;
	}

	public Geodetic3D(Geodetic2D geodetic2D) {
		_longitude = geodetic2D.getLongitude();
		_latitude = geodetic2D.getLatitude();
		_height = 0;
	}

	public Geodetic3D(Geodetic2D geodetic2D, double height) {
		_longitude = geodetic2D.getLongitude();
		_latitude = geodetic2D.getLatitude();
		_height = height;
	}

	public double getLongitude() {
		return _longitude;
	}

	public double getLatitude() {
		return _latitude;
	}

	public double getHeight() {
		return _height;
	}

	public boolean equals(Geodetic3D other) {
		return _longitude == other._longitude && _latitude == other._latitude && _height == other._height;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Geodetic3D) {
			return equals((Geodetic3D) obj);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return Double.valueOf(_longitude).hashCode() ^ Double.valueOf(_latitude).hashCode() ^ Double.valueOf(_height).hashCode();
	}

	private double _longitude;
	private double _latitude;
	private double _height;
}
