package fr.ifremer.globe.ogl.core;

/**
 * <p>
 * Description : Geodetic extent.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class GeodeticExtent {

	public GeodeticExtent(double west, double south, double east, double north) {
		_west = west;
		_south = south;
		_east = east;
		_north = north;
	}

	public GeodeticExtent(Geodetic2D bottomLeft, Geodetic2D topRight) {
		_west = bottomLeft.getLongitude();
		_south = bottomLeft.getLatitude();
		_east = topRight.getLongitude();
		_north = topRight.getLatitude();
	}

	public double getWest() {
		return _west;
	}

	public double getSouth() {
		return _south;
	}

	public double getEast() {
		return _east;
	}

	public double getNorth() {
		return _north;
	}

	public double getWidth() {
		return _east - _west;
	}

	public double getHeight() {
		return _north - _south;
	}

	public boolean equals(GeodeticExtent other) {
		return _west == other._west && _south == other._south && _east == other._east && _north == other._north;
	}

	public boolean overlap(GeodeticExtent other) {
		if (_north < other._south) {
			return false;
		}
		if (other._north < _south) {
			return false;
		}
		if (_east < other._west) {
			return false;
		}
		if (other._east < _west) {
			return false;
		}
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GeodeticExtent) {
			return equals((GeodeticExtent) obj);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return Double.valueOf(_west).hashCode() ^ Double.valueOf(_south).hashCode() ^ Double.valueOf(_east).hashCode() ^ Double.valueOf(_north).hashCode();
	}

	private double _west;
	private double _south;
	private double _east;
	private double _north;

}
