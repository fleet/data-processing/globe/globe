package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : A set of 4-dimensional cartesian coordinates where the four
 * components X, Y, Z and W are represented as single-precision (32-bit)
 * floating point numbers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector4F {

	public static Vector4F getZero() {
		return new Vector4F(0.0f, 0.0f, 0.0f, 0.0f);
	}

	public static Vector4F getUnitX() {
		return new Vector4F(1.0f, 0.0f, 0.0f, 0.0f);
	}

	public static Vector4F getUnitY() {
		return new Vector4F(0.0f, 1.0f, 0.0f, 0.0f);
	}

	public static Vector4F getUnitZ() {
		return new Vector4F(0.0f, 0.0f, 1.0f, 0.0f);
	}

	public static Vector4F getUnitW() {
		return new Vector4F(0.0f, 0.0f, 0.0f, 1.0f);
	}

	public static Vector4F getUndefined() {
		return new Vector4F(Float.NaN, Float.NaN, Float.NaN, Float.NaN);
	}

	public Vector4F(float x, float y, float z, float w) {
		_x = x;
		_y = y;
		_z = z;
		_w = w;
	}

	public Vector4F(Vector3F v, float w) {
		_x = v.getX();
		_y = v.getY();
		_z = v.getZ();
		_w = w;
	}

	public Vector4F(Vector2F v, float z, float w) {
		_x = v.getX();
		_y = v.getY();
		_z = z;
		_w = w;
	}

	public float getX() {
		return _x;
	}

	public float getY() {
		return _y;
	}

	public float getZ() {
		return _z;
	}

	public float getW() {
		return _w;
	}

	public Vector2F getXY() {
		return new Vector2F(_x, _y);
	}

	public Vector3F getXYZ() {
		return new Vector3F(_x, _y, _z);
	}

	public float getMagnitudeSquared() {
		return _x * _x + _y * _y + _z * _z + _w * _w;
	}

	public float getMagnitude() {
		return (float) Math.sqrt(getMagnitudeSquared());
	}

	public boolean isUndefined() {
		return Float.isNaN(_x);
	}

	public Vector4F normalize() {
		return this.divide(getMagnitude());
	}

	public float dot(Vector4F other) {
		return getX() * other.getX() + getY() * other.getY() + getZ() * other.getZ() + getW() * other.getW();
	}

	public Vector4F add(Vector4F addend) {
		return new Vector4F(_x + addend._x, _y + addend._y, _z + addend._z, _w + addend._w);
	}

	public Vector4F subtract(Vector4F subtrahend) {
		return new Vector4F(_x - subtrahend._x, _y - subtrahend._y, _z - subtrahend._z, _w - subtrahend._w);
	}

	public Vector4F multiply(float scalar) {
		return new Vector4F(_x * scalar, _y * scalar, _z * scalar, _w * scalar);
	}

	public Vector4F multiplyComponents(Vector4F scale) {
		return new Vector4F(getX() * scale.getX(), getY() * scale.getY(), getZ() * scale.getZ(), getW() * scale.getW());
	}

	public Vector4F divide(float scalar) {
		return new Vector4F(_x / scalar, _y / scalar, _z / scalar, _w / scalar);
	}

	public Vector4F getMostOrthogonalAxis() {
		float x = Math.abs(_x);
		float y = Math.abs(_y);
		float z = Math.abs(_z);
		float w = Math.abs(_w);

		if ((x < y) && (x < z) && (x < w)) {
			return getUnitX();
		} else if ((y < x) && (y < z) && (y < w)) {
			return getUnitY();
		} else if ((z < x) && (z < y) && (z < w)) {
			return getUnitZ();
		} else {
			return getUnitW();
		}
	}

	public Vector4F negate() {
		return new Vector4F(-_x, -_y, -_z, -_w);
	}

	public boolean equalsEpsilon(Vector4F other, float epsilon) {
		return (Math.abs(_x - other._x) <= epsilon) && (Math.abs(_y - other._y) <= epsilon) && (Math.abs(_z - other._z) <= epsilon) && (Math.abs(_w - other._w) <= epsilon);
	}

	public boolean isEquals(Vector4F other) {
		return _x == other._x && _y == other._y && _z == other._z && _w == other._w;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector4F) {
			return isEquals((Vector4F) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1}, {2}, {3})", _x, _y, _z, _w);
	}

	@Override
	public int hashCode() {
		return Float.valueOf(_x).hashCode() ^ Float.valueOf(_y).hashCode() ^ Float.valueOf(_z).hashCode() ^ Float.valueOf(_w).hashCode();
	}

	public Vector4D toVector4D() {
		return new Vector4D(_x, _y, _z, _w);
	}

	private float _x;
	private float _y;
	private float _z;
	private float _w;
}
