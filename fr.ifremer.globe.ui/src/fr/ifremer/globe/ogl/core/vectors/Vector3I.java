package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : A set of 3-dimensional cartesian coordinates where the three
 * components X, Y and Z are represented as 32-bit integers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector3I {

	public static Vector3I getZero() {
		return new Vector3I(0, 0, 0);
	}

	public static Vector3I getUnitX() {
		return new Vector3I(1, 0, 0);
	}

	public static Vector3I getUnitY() {
		return new Vector3I(0, 1, 0);
	}

	public static Vector3I getUnitZ() {
		return new Vector3I(0, 0, 1);
	}

	public Vector3I(int x, int y, int z) {
		_x = x;
		_y = y;
		_z = z;
	}

	public Vector3I(Vector2I v, int z) {
		_x = v.getX();
		_y = v.getY();
		_z = z;
	}

	public int getX() {
		return _x;
	}

	public int getY() {
		return _y;
	}

	public int getZ() {
		return _z;
	}

	public Vector2I getXY() {
		return new Vector2I(_x, _y);
	}

	public int getMagnitudeSquared() {
		return _x * _x + _y * _y + _z * _z;
	}

	public double getMagnitude() {
		return Math.sqrt(getMagnitudeSquared());
	}

	public boolean isUndefined() {
		return Float.isNaN(_x);
	}

	public Vector3I cross(Vector3I other) {
		return new Vector3I(_y * other.getZ() - _z * other.getY(), _z * other.getX() - _x * other.getZ(), _x * other.getY() - _y * other.getX());
	}

	public float dot(Vector3I other) {
		return _x * other.getX() + _y * other.getY() + _z * other.getZ();
	}

	public Vector3I add(Vector3I addend) {
		return new Vector3I(_x + addend._x, _y + addend._y, _z + addend._z);
	}

	public Vector3I subtract(Vector3I subtrahend) {
		return new Vector3I(_x - subtrahend._x, _y - subtrahend._y, _z - subtrahend._z);
	}

	public Vector3I multiply(int scalar) {
		return new Vector3I(_x * scalar, _y * scalar, _z * scalar);
	}

	public Vector3I multiplyComponents(Vector3I scale) {
		return new Vector3I(_x * scale.getX(), _y * scale.getY(), _z * scale.getZ());
	}

	public Vector3I divide(int scalar) {
		return new Vector3I(_x / scalar, _y / scalar, _z / scalar);
	}

	public int getMaximumComponent() {
		return Math.max(Math.max(_x, _y), _z);
	}

	public int getMinimumComponent() {
		return Math.min(Math.min(_x, _y), _z);
	}

	public Vector3I getMostOrthogonalAxis() {
		int x = Math.abs(_x);
		int y = Math.abs(_y);
		int z = Math.abs(_z);

		if ((x < y) && (x < z)) {
			return getUnitX();
		} else if ((y < x) && (y < z)) {
			return getUnitY();
		} else {
			return getUnitZ();
		}
	}

	public Vector3I negate() {
		return new Vector3I(-_x, -_y, -_z);
	}

	public boolean equalsEpsilon(Vector3I other, int epsilon) {
		return (Math.abs(_x - other._x) <= epsilon) && (Math.abs(_y - other._y) <= epsilon) && (Math.abs(_z - other._z) <= epsilon);
	}

	public boolean equals(Vector3I other) {
		return _x == other._x && _y == other._y && _z == other._z;
	}

	public boolean isSuperior(Vector3I other) {
		return (_x > other.getX()) && (_y > other.getY()) && (_z > other.getZ());
	}

	public boolean isSuperiorOrEqual(Vector3I other) {
		return (_x >= other.getX()) && (_y >= other.getY()) && (_z >= other.getZ());
	}

	public boolean isInferior(Vector3I other) {
		return (_x < other.getX()) && (_y < other.getY()) && (_z < other.getZ());
	}

	public boolean isInferiorOrEqual(Vector3I other) {
		return (_x <= other.getX()) && (_y <= other.getY()) && (_z <= other.getZ());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector3I) {
			return equals((Vector3I) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1}, {2})", _x, _y, _z);
	}

	@Override
	public int hashCode() {
		return Integer.valueOf(_x).hashCode() ^ Integer.valueOf(_y).hashCode() ^ Integer.valueOf(_z).hashCode();
	}

	public Vector3D toVector3D() {
		return new Vector3D(_x, _y, _z);
	}

	private int _x;
	private int _y;
	private int _z;
}
