package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : A set of 2-dimensional cartesian coordinates where the two
 * components X, Y and Z are represented as double-precision (64-bit) floating
 * point numbers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector2D {

	public static Vector2D getZero() {
		return new Vector2D(0.0, 0.0);
	}

	public static Vector2D getUnitX() {
		return new Vector2D(1.0, 0.0);
	}

	public static Vector2D getUnitY() {
		return new Vector2D(0.0, 1.0);
	}

	public static Vector2D getUndefined() {
		return new Vector2D(Double.NaN, Double.NaN);
	}

	public Vector2D(double x, double y) {
		_x = x;
		_y = y;
	}

	public double getX() {
		return _x;
	}

	public double getY() {
		return _y;
	}

	public double getMagnitudeSquared() {
		return _x * _x + _y * _y;
	}

	public double getMagnitude() {
		return Math.sqrt(getMagnitudeSquared());
	}

	public boolean isUndefined() {
		return Double.isNaN(_x);
	}

	public Vector2D normalize() {
		return this.divide(getMagnitude());
	}

	public double dot(Vector2D other) {
		return _x * other.getX() + _y * other.getY();
	}

	public Vector2D add(Vector2D addend) {
		return new Vector2D(_x + addend._x, _y + addend._y);
	}

	public Vector2D subtract(Vector2D subtrahend) {
		return new Vector2D(_x - subtrahend._x, _y - subtrahend._y);
	}

	public Vector2D multiply(double scalar) {
		return new Vector2D(_x * scalar, _y * scalar);
	}

	public Vector2D multiplyComponents(Vector2D scale) {
		return new Vector2D(_x * scale.getX(), _y * scale.getY());
	}

	public Vector2D divide(double scalar) {
		return new Vector2D(_x / scalar, _y / scalar);
	}

	public double getMaximumComponent() {
		return Math.max(_x, _y);
	}

	public double getMinimumComponent() {
		return Math.min(_x, _y);
	}

	public Vector2D getMostOrthogonalAxis() {
		double x = Math.abs(_x);
		double y = Math.abs(_y);

		if (x < y) {
			return getUnitX();
		} else {
			return getUnitY();
		}
	}

	public double getAngleBetween(Vector2D other) {
		return Math.acos(normalize().dot(other.normalize()));
	}

	public Vector2D negate() {
		return new Vector2D(-_x, -_y);
	}

	public boolean equalsEpsilon(Vector2D other, double epsilon) {
		return (Math.abs(_x - other._x) <= epsilon) && (Math.abs(_y - other._y) <= epsilon);
	}

	public boolean equals(Vector2D other) {
		return _x == other._x && _y == other._y;
	}

	public boolean isSuperior(Vector2D other) {
		return (_x > other.getX()) && (_y > other.getY());
	}

	public boolean isSuperiorOrEqual(Vector2D other) {
		return (_x >= other.getX()) && (_y >= other.getY());
	}

	public boolean isInferior(Vector2D other) {
		return (_x < other.getX()) && (_y < other.getY());
	}

	public boolean isInferiorOrEqual(Vector2D other) {
		return (_x <= other.getX()) && (_y <= other.getY());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector2D) {
			return equals((Vector2D) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({%f}, {%f})", _x, _y);
	}

	@Override
	public int hashCode() {
		return Double.valueOf(_x).hashCode() ^ Double.valueOf(_y).hashCode();
	}

	public Vector2F toVector2F() {
		return new Vector2F((float) _x, (float) _y);
	}

	private double _x;
	private double _y;
}
