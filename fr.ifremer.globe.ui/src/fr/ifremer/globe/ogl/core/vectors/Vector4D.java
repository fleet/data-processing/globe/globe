package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : A set of 4-dimensional cartesian coordinates where the four
 * components X, Y, Z and W are represented as double-precision (64-bit)
 * floating point numbers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector4D {

	public static Vector4D getZero() {
		return new Vector4D(0.0, 0.0, 0.0, 0.0);
	}

	public static Vector4D getUnitX() {
		return new Vector4D(1.0, 0.0, 0.0, 0.0);
	}

	public static Vector4D getUnitY() {
		return new Vector4D(0.0, 1.0, 0.0, 0.0);
	}

	public static Vector4D getUnitZ() {
		return new Vector4D(0.0, 0.0, 1.0, 0.0);
	}

	public static Vector4D getUnitW() {
		return new Vector4D(0.0, 0.0, 0.0, 1.0);
	}

	public static Vector4D getUndefined() {
		return new Vector4D(Double.NaN, Double.NaN, Double.NaN, Double.NaN);
	}

	public Vector4D(double x, double y, double z, double w) {
		_x = x;
		_y = y;
		_z = z;
		_w = w;
	}

	public Vector4D(Vector3D v, double w) {
		_x = v.getX();
		_y = v.getY();
		_z = v.getZ();
		_w = w;
	}

	public Vector4D(Vector2D v, double z, double w) {
		_x = v.getX();
		_y = v.getY();
		_z = z;
		_w = w;
	}

	public double getX() {
		return _x;
	}

	public double getY() {
		return _y;
	}

	public double getZ() {
		return _z;
	}

	public double getW() {
		return _w;
	}

	public Vector2D getXY() {
		return new Vector2D(_x, _y);
	}

	public Vector3D getXYZ() {
		return new Vector3D(_x, _y, _z);
	}

	public double getMagnitudeSquared() {
		return _x * _x + _y * _y + _z * _z + _w * _w;
	}

	public double getMagnitude() {
		return Math.sqrt(getMagnitudeSquared());
	}

	public boolean isUndefined() {
		return Double.isNaN(_x);
	}

	public Vector4D normalize() {
		return this.divide(getMagnitude());
	}

	public double dot(Vector4D other) {
		return getX() * other.getX() + getY() * other.getY() + getZ() * other.getZ() + getW() * other.getW();
	}

	public Vector4D add(Vector4D addend) {
		return new Vector4D(_x + addend._x, _y + addend._y, _z + addend._z, _w + addend._w);
	}

	public Vector4D subtract(Vector4D subtrahend) {
		return new Vector4D(_x - subtrahend._x, _y - subtrahend._y, _z - subtrahend._z, _w - subtrahend._w);
	}

	public Vector4D multiply(double scalar) {
		return new Vector4D(_x * scalar, _y * scalar, _z * scalar, _w * scalar);
	}

	public Vector4D multiplyComponents(Vector4D scale) {
		return new Vector4D(getX() * scale.getX(), getY() * scale.getY(), getZ() * scale.getZ(), getW() * scale.getW());
	}

	public Vector4D divide(double scalar) {
		return new Vector4D(_x / scalar, _y / scalar, _z / scalar, _w / scalar);
	}

	public Vector4D getMostOrthogonalAxis() {
		double x = Math.abs(_x);
		double y = Math.abs(_y);
		double z = Math.abs(_z);
		double w = Math.abs(_w);

		if ((x < y) && (x < z) && (x < w)) {
			return getUnitX();
		} else if ((y < x) && (y < z) && (y < w)) {
			return getUnitY();
		} else if ((z < x) && (z < y) && (z < w)) {
			return getUnitZ();
		} else {
			return getUnitW();
		}
	}

	public Vector4D negate() {
		return new Vector4D(-_x, -_y, -_z, -_w);
	}

	public boolean equalsEpsilon(Vector4D other, double epsilon) {
		return (Math.abs(_x - other._x) <= epsilon) && (Math.abs(_y - other._y) <= epsilon) && (Math.abs(_z - other._z) <= epsilon) && (Math.abs(_w - other._w) <= epsilon);
	}

	public boolean equals(Vector4D other) {
		return _x == other._x && _y == other._y && _z == other._z && _w == other._w;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector4D) {
			return equals((Vector4D) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1}, {2}, {3})", _x, _y, _z, _w);
	}

	@Override
	public int hashCode() {
		return Double.valueOf(_x).hashCode() ^ Double.valueOf(_y).hashCode() ^ Double.valueOf(_z).hashCode() ^ Double.valueOf(_w).hashCode();
	}

	public Vector4F toVector4F() {
		return new Vector4F((float) _x, (float) _y, (float) _z, (float) _w);
	}

	private double _x;
	private double _y;
	private double _z;
	private double _w;
}
