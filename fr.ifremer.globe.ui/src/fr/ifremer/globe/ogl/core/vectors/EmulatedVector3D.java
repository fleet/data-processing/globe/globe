package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : EmulatedVector3D.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 18 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class EmulatedVector3D {

	public EmulatedVector3D(Vector3D v) {
		_high = v.toVector3F();
		_low = (v.subtract(_high.toVector3D())).toVector3F();
	}

	public Vector3F getHigh() {
		return _high;
	}

	public Vector3F getLow() {
		return _low;
	}

	public boolean equals(EmulatedVector3D other) {
		return _high == other._high && _low == other._low;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EmulatedVector3D) {
			return equals((EmulatedVector3D) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1})", getHigh(), getLow());
	}

	@Override
	public int hashCode() {
		return _high.hashCode() ^ _low.hashCode();
	}

	private Vector3F _high;
	private Vector3F _low;
}
