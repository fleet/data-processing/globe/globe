package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : A set of 2-dimensional cartesian coordinates where the two
 * components X, Y and Z are represented as single-precision (32-bit) floating
 * point numbers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector2F {

	public static Vector2F getZero() {
		return new Vector2F(0.0f, 0.0f);
	}

	public static Vector2F getUnitX() {
		return new Vector2F(1.0f, 0.0f);
	}

	public static Vector2F getUnitY() {
		return new Vector2F(0.0f, 1.0f);
	}

	public static Vector2F getUndefined() {
		return new Vector2F(Float.NaN, Float.NaN);
	}

	public Vector2F(float x, float y) {
		_x = x;
		_y = y;
	}

	public float getX() {
		return _x;
	}

	public float getY() {
		return _y;
	}

	public float getMagnitudeSquared() {
		return _x * _x + _y * _y;
	}

	public float getMagnitude() {
		return (float) Math.sqrt(getMagnitudeSquared());
	}

	public boolean isUndefined() {
		return Float.isNaN(_x);
	}

	public Vector2F normalize() {
		return this.divide(getMagnitude());
	}

	public float dot(Vector2F other) {
		return _x * other.getX() + _y * other.getY();
	}

	public Vector2F add(Vector2F addend) {
		return new Vector2F(_x + addend._x, _y + addend._y);
	}

	public Vector2F subtract(Vector2F subtrahend) {
		return new Vector2F(_x - subtrahend._x, _y - subtrahend._y);
	}

	public Vector2F multiply(float scalar) {
		return new Vector2F(_x * scalar, _y * scalar);
	}

	public Vector2F multiplyComponents(Vector2F scale) {
		return new Vector2F(_x * scale.getX(), _y * scale.getY());
	}

	public Vector2F divide(float scalar) {
		return new Vector2F(_x / scalar, _y / scalar);
	}

	public float getMaximumComponent() {
		return Math.max(_x, _y);
	}

	public float getMinimumComponent() {
		return Math.min(_x, _y);
	}

	public Vector2F getMostOrthogonalAxis() {
		float x = Math.abs(_x);
		float y = Math.abs(_y);

		if (x < y) {
			return getUnitX();
		} else {
			return getUnitY();
		}
	}

	public Vector2F negate() {
		return new Vector2F(-_x, -_y);
	}

	public boolean equalsEpsilon(Vector2F other, float epsilon) {
		return (Math.abs(_x - other._x) <= epsilon) && (Math.abs(_y - other._y) <= epsilon);
	}

	public boolean equals(Vector2F other) {
		return _x == other._x && _y == other._y;
	}

	public boolean isSuperior(Vector2F other) {
		return (_x > other.getX()) && (_y > other.getY());
	}

	public boolean isSuperiorOrEqual(Vector2F other) {
		return (_x >= other.getX()) && (_y >= other.getY());
	}

	public boolean isInferior(Vector2F other) {
		return (_x < other.getX()) && (_y < other.getY());
	}

	public boolean isInferiorOrEqual(Vector2F other) {
		return (_x <= other.getX()) && (_y <= other.getY());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector2F) {
			return equals((Vector2F) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1})", _x, _y);
	}

	@Override
	public int hashCode() {
		return Float.valueOf(_x).hashCode() ^ Float.valueOf(_y).hashCode();
	}

	public Vector2D toVector2D() {
		return new Vector2D(_x, _y);
	}

	private float _x;
	private float _y;
}
