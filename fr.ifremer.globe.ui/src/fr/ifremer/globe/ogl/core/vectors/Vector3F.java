package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : A set of 3-dimensional cartesian coordinates where the three
 * components X, Y and Z are represented as single-precision (32-bit) floating
 * point numbers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector3F {

	public static Vector3F getZero() {
		return new Vector3F(0.0f, 0.0f, 0.0f);
	}

	public static Vector3F getUnitX() {
		return new Vector3F(1.0f, 0.0f, 0.0f);
	}

	public static Vector3F getUnitY() {
		return new Vector3F(0.0f, 1.0f, 0.0f);
	}

	public static Vector3F getUnitZ() {
		return new Vector3F(0.0f, 0.0f, 1.0f);
	}

	public static Vector3F getUndefined() {
		return new Vector3F(Float.NaN, Float.NaN, Float.NaN);
	}

	public Vector3F(float x, float y, float z) {
		_x = x;
		_y = y;
		_z = z;
	}

	public Vector3F(Vector2F v, float z) {
		_x = v.getX();
		_y = v.getY();
		_z = z;
	}

	public float getX() {
		return _x;
	}

	public float getY() {
		return _y;
	}

	public float getZ() {
		return _z;
	}

	public Vector2F getXY() {
		return new Vector2F(_x, _y);
	}

	public float getMagnitudeSquared() {
		return _x * _x + _y * _y + _z * _z;
	}

	public float getMagnitude() {
		return (float) Math.sqrt(getMagnitudeSquared());
	}

	public boolean isUndefined() {
		return Float.isNaN(_x);
	}

	public Vector3F normalize() {
		return this.divide(getMagnitude());
	}

	public Vector3F cross(Vector3F other) {
		return new Vector3F(_y * other.getZ() - _z * other.getY(), _z * other.getX() - _x * other.getZ(), _x * other.getY() - _y * other.getX());
	}

	public float dot(Vector3F other) {
		return _x * other.getX() + _y * other.getY() + _z * other.getZ();
	}

	public Vector3F add(Vector3F addend) {
		return new Vector3F(_x + addend._x, _y + addend._y, _z + addend._z);
	}

	public Vector3F subtract(Vector3F subtrahend) {
		return new Vector3F(_x - subtrahend._x, _y - subtrahend._y, _z - subtrahend._z);
	}

	public Vector3F multiply(float scalar) {
		return new Vector3F(_x * scalar, _y * scalar, _z * scalar);
	}

	public Vector3F multiplyComponents(Vector3F scale) {
		return new Vector3F(_x * scale.getX(), _y * scale.getY(), _z * scale.getZ());
	}

	public Vector3F divide(float scalar) {
		return new Vector3F(_x / scalar, _y / scalar, _z / scalar);
	}

	public float getMaximumComponent() {
		return Math.max(Math.max(_x, _y), _z);
	}

	public float getMinimumComponent() {
		return Math.min(Math.min(_x, _y), _z);
	}

	public Vector3F getMostOrthogonalAxis() {
		float x = Math.abs(_x);
		float y = Math.abs(_y);
		float z = Math.abs(_z);

		if ((x < y) && (x < z)) {
			return getUnitX();
		} else if ((y < x) && (y < z)) {
			return getUnitY();
		} else {
			return getUnitZ();
		}
	}

	public Vector3F negate() {
		return new Vector3F(-_x, -_y, -_z);
	}

	public boolean equalsEpsilon(Vector3F other, float epsilon) {
		return (Math.abs(_x - other._x) <= epsilon) && (Math.abs(_y - other._y) <= epsilon) && (Math.abs(_z - other._z) <= epsilon);
	}

	public boolean equals(Vector3F other) {
		return _x == other._x && _y == other._y && _z == other._z;
	}

	public boolean isSuperior(Vector3F other) {
		return (_x > other.getX()) && (_y > other.getY()) && (_z > other.getZ());
	}

	public boolean isSuperiorOrEqual(Vector3F other) {
		return (_x >= other.getX()) && (_y >= other.getY()) && (_z >= other.getZ());
	}

	public boolean isInferior(Vector3F other) {
		return (_x < other.getX()) && (_y < other.getY()) && (_z < other.getZ());
	}

	public boolean isInferiorOrEqual(Vector3F other) {
		return (_x <= other.getX()) && (_y <= other.getY()) && (_z <= other.getZ());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector3F) {
			return equals((Vector3F) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1}, {2})", _x, _y, _z);
	}

	@Override
	public int hashCode() {
		return Float.valueOf(_x).hashCode() ^ Float.valueOf(_y).hashCode() ^ Float.valueOf(_z).hashCode();
	}

	public Vector3D toVector3D() {
		return new Vector3D(_x, _y, _z);
	}

	private float _x;
	private float _y;
	private float _z;
}
