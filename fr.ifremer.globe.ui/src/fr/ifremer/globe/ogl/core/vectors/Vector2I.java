package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

/**
 * <p>
 * Description : A set of 2-dimensional cartesian coordinates where the two
 * components X, Y and Z are represented as double-precision (64-bit) floating
 * point numbers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector2I {

	public static Vector2I getZero() {
		return new Vector2I(0, 0);
	}

	public static Vector2I getUnitX() {
		return new Vector2I(1, 0);
	}

	public static Vector2I getUnitY() {
		return new Vector2I(0, 1);
	}

	public Vector2I(int x, int y) {
		_x = x;
		_y = y;
	}

	public int getX() {
		return _x;
	}

	public int getY() {
		return _y;
	}

	public Vector2I add(Vector2I addend) {
		return new Vector2I(_x + addend._x, _y + addend._y);
	}

	public Vector2I subtract(Vector2I subtrahend) {
		return new Vector2I(_x - subtrahend._x, _y - subtrahend._y);
	}

	public Vector2I multiply(int scalar) {
		return new Vector2I(_x * scalar, _y * scalar);
	}

	public Vector2I multiplyComponents(Vector2I scale) {
		return new Vector2I(_x * scale.getX(), _y * scale.getY());
	}

	public int getMaximumComponent() {
		return Math.max(_x, _y);
	}

	public int getMinimumComponent() {
		return Math.min(_x, _y);
	}

	public Vector2I negate() {
		return new Vector2I(-_x, -_y);
	}

	public boolean equals(Vector2I other) {
		return _x == other._x && _y == other._y;
	}

	public boolean isSuperior(Vector2I other) {
		return (_x > other.getX()) && (_y > other.getY());
	}

	public boolean isSuperiorOrEqual(Vector2I other) {
		return (_x >= other.getX()) && (_y >= other.getY());
	}

	public boolean isInferior(Vector2I other) {
		return (_x < other.getX()) && (_y < other.getY());
	}

	public boolean isInferiorOrEqual(Vector2I other) {
		return (_x <= other.getX()) && (_y <= other.getY());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector2I) {
			return equals((Vector2I) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "({0}, {1})", _x, _y);
	}

	@Override
	public int hashCode() {
		return Integer.valueOf(_x).hashCode() ^ Integer.valueOf(_y).hashCode();
	}

	public Vector2D toVector2D() {
		return new Vector2D(_x, _y);
	}

	public Vector2F toVector2F() {
		return new Vector2F(_x, _y);
	}

	private int _x;
	private int _y;
}
