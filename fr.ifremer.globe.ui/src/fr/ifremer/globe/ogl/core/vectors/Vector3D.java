package fr.ifremer.globe.ogl.core.vectors;

import java.util.Locale;

import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * <p>
 * Description : A set of 3-dimensional cartesian coordinates where the three
 * components X, Y and Z are represented as double-precision (64-bit) floating
 * point numbers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 15 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Vector3D {

	public static Vector3D getZero() {
		return new Vector3D(0.0, 0.0, 0.0);
	}

	public static Vector3D getUnitX() {
		return new Vector3D(1.0, 0.0, 0.0);
	}

	public static Vector3D getUnitY() {
		return new Vector3D(0.0, 1.0, 0.0);
	}

	public static Vector3D getUnitZ() {
		return new Vector3D(0.0, 0.0, 1.0);
	}

	public static Vector3D getUndefined() {
		return new Vector3D(Double.NaN, Double.NaN, Double.NaN);
	}

	public Vector3D(double x, double y, double z) {
		_x = x;
		_y = y;
		_z = z;
	}

	public Vector3D(Vector2D v, double z) {
		_x = v.getX();
		_y = v.getY();
		_z = z;
	}

	public double getX() {
		return _x;
	}

	public double getY() {
		return _y;
	}

	public double getZ() {
		return _z;
	}

	public Vector2D getXY() {
		return new Vector2D(_x, _y);
	}

	public double getMagnitudeSquared() {
		return _x * _x + _y * _y + _z * _z;
	}

	public double getMagnitude() {
		return Math.sqrt(getMagnitudeSquared());
	}

	public boolean isUndefined() {
		return Double.isNaN(_x);
	}

	public Vector3D normalize() {
		return this.divide(getMagnitude());
	}

	public Vector3D cross(Vector3D other) {
		return new Vector3D(_y * other.getZ() - _z * other.getY(), _z * other.getX() - _x * other.getZ(), _x * other.getY() - _y * other.getX());
	}

	public double dot(Vector3D other) {
		return _x * other.getX() + _y * other.getY() + _z * other.getZ();
	}

	public double distance(Vector3D other) {
		return Math.sqrt(NumberUtils.square(_x - other.getX()) + NumberUtils.square(_y - other.getX()) + NumberUtils.square(_z - other.getX()));
	}

	public float distance(float[] other) {
		return (float) Math.sqrt(NumberUtils.square(_x - other[0]) + NumberUtils.square(_y - other[1]) + NumberUtils.square(_z - other[2]));
	}

	public Vector3D add(Vector3D addend) {
		return new Vector3D(_x + addend._x, _y + addend._y, _z + addend._z);
	}

	public Vector3D subtract(Vector3D subtrahend) {
		return new Vector3D(_x - subtrahend._x, _y - subtrahend._y, _z - subtrahend._z);
	}

	public Vector3D multiply(double scalar) {
		return new Vector3D(_x * scalar, _y * scalar, _z * scalar);
	}

	public Vector3D multiplyComponents(Vector3D scale) {
		return new Vector3D(_x * scale.getX(), _y * scale.getY(), _z * scale.getZ());
	}

	public Vector3D divide(double scalar) {
		return new Vector3D(_x / scalar, _y / scalar, _z / scalar);
	}

	public double getMaximumComponent() {
		return Math.max(Math.max(_x, _y), _z);
	}

	public double getMinimumComponent() {
		return Math.min(Math.min(_x, _y), _z);
	}

	public Vector3D getMostOrthogonalAxis() {
		double x = Math.abs(_x);
		double y = Math.abs(_y);
		double z = Math.abs(_z);

		if ((x < y) && (x < z)) {
			return getUnitX();
		} else if ((y < x) && (y < z)) {
			return getUnitY();
		} else {
			return getUnitZ();
		}
	}

	public double getAngleBetween(Vector3D other) {
		return Math.acos(normalize().dot(other.normalize()));
	}

	public Vector3D rotateAroundAxis(Vector3D axis, double theta) {
		double u = axis.getX();
		double v = axis.getY();
		double w = axis.getZ();

		double cosTheta = Math.cos(theta);
		double sinTheta = Math.sin(theta);

		double ms = axis.getMagnitudeSquared();
		double m = Math.sqrt(ms);

		return new Vector3D(((u * (u * _x + v * _y + w * _z)) + (((_x * (v * v + w * w)) - (u * (v * _y + w * _z))) * cosTheta) + (m * ((-w * _y) + (v * _z)) * sinTheta)) / ms,

				((v * (u * _x + v * _y + w * _z)) + (((_y * (u * u + w * w)) - (v * (u * _x + w * _z))) * cosTheta) + (m * ((w * _x) - (u * _z)) * sinTheta)) / ms,

				((w * (u * _x + v * _y + w * _z)) + (((_z * (u * u + v * v)) - (w * (u * _x + v * _y))) * cosTheta) + (m * (-(v * _x) + (u * _y)) * sinTheta)) / ms);
	}

	public Vector3D negate() {
		return new Vector3D(-_x, -_y, -_z);
	}

	public boolean equalsEpsilon(Vector3D other, double epsilon) {
		return (Math.abs(_x - other._x) <= epsilon) && (Math.abs(_y - other._y) <= epsilon) && (Math.abs(_z - other._z) <= epsilon);
	}

	public boolean equals(Vector3D other) {
		return _x == other._x && _y == other._y && _z == other._z;
	}

	public boolean isSuperior(Vector3D other) {
		return (_x > other.getX()) && (_y > other.getY()) && (_z > other.getZ());
	}

	public boolean isSuperiorOrEqual(Vector3D other) {
		return (_x >= other.getX()) && (_y >= other.getY()) && (_z >= other.getZ());
	}

	public boolean isInferior(Vector3D other) {
		return (_x < other.getX()) && (_y < other.getY()) && (_z < other.getZ());
	}

	public boolean isInferiorOrEqual(Vector3D other) {
		return (_x <= other.getX()) && (_y <= other.getY()) && (_z <= other.getZ());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector3D) {
			return equals((Vector3D) obj);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "(%1$f, %2$f, %3$f)", _x, _y, _z);
	}

	@Override
	public int hashCode() {
		return Double.valueOf(_x).hashCode() ^ Double.valueOf(_y).hashCode() ^ Double.valueOf(_z).hashCode();
	}

	public Vector3F toVector3F() {
		return new Vector3F((float) _x, (float) _y, (float) _z);
	}

	private double _x;
	private double _y;
	private double _z;
}
