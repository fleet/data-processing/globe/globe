package fr.ifremer.globe.ogl;

import com.jogamp.opengl.GLAutoDrawable;

/**
 * Defines an object which will be draw on the Scene3D
 * 
 * @author G.Fouillet, &lt;gfouillet@ipsis.com&gt;
 * 
 */
public interface GLComponent {
	
	/**
	 * Dispose OpenGL resources
	 * 
	 * @param glad
	 */
	void dispose(GLAutoDrawable glad);

	/**
	 * Draw the component to the drawable.
	 * 
	 * @param glad
	 */
	void draw(GLAutoDrawable glad);

	/**
	 * Redirect the component to the drawable for value picking with the following formaula: 
	 * targetValue = (sourceValue - minValue) / resolution
	 * 
	 * @param glad
	 * @param resolution
	 * @param minValue
	 */
	default void redirect(GLAutoDrawable glad, float resolution, float minValue) {
		// Do nothing by default
	}

	/**
	 * Component visible ?
	 */
	boolean isVisible();
}
