package fr.ifremer.globe.ogl.util;

/**
 * Extracts an immutable key for this object.
 * 
 * It is important for the key to not be mutable and that the key value itself
 * is not changed in the object. If this happens any AutoIndex, or Map using
 * this key will break.
 */
public interface KeyExtractor<K, V> {
	/**
	 * Gets the immutable key for the specified value.
	 * 
	 * @return gets the immutable key for the specified value
	 */
	K getKey(V value);
}